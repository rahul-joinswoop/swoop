#!/usr/bin/env bash
set -e
REPO=joinswoop/circleci-ruby
RUBY_VERSION=$(grep "^ruby " ../../../Gemfile|head -1| awk {'print $2'}|sed "s/'//g")
BUNDLER_VERSION=$(grep -a1 "BUNDLED WITH" ../../../Gemfile.lock |tail -1|awk {'print $1'})
PREV_LATEST_ID=$(docker images $REPO:latest -q)
docker build --build-arg BUNDLER_VERSION=$BUNDLER_VERSION \
             --build-arg RUBY_VERSION=$RUBY_VERSION \
             --tag $REPO:latest \
             .

# make sure the tags in config.yml are correct
PREV_IMAGE_TAG=$(docker images $REPO| sort -n -k 2| tail -1|awk {'print $2'})
sed -E -i '' 's'"@$REPO"':[0-9]+@'"$REPO"':'"$PREV_IMAGE_TAG"'@g' ../../config.yml

LATEST_ID=$(docker images $REPO:latest -q)

if [[ "$PREV_LATEST_ID" != "$LATEST_ID" ]]; then
  # bump the tag version up by one
  IMAGE_TAG=$((PREV_IMAGE_TAG + 1))
  sed -E -i '' 's'"@$REPO"':[0-9]+@'"$REPO"':'"$IMAGE_TAG"'@g' ../../config.yml
  echo "adding new tag $REPO:${IMAGE_TAG:=0}"
  docker tag $REPO:latest $REPO:${IMAGE_TAG:=0}
  docker push $REPO:${IMAGE_TAG:=0}
  docker push $REPO:latest
fi
