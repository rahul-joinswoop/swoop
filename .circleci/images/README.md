# Docker images for CircleCI

## Overview
This directory contains custom docker images for our circleci builds. We customize them to make them smaller and to add extra software in the image build step that we'd normally do each time a test is run. Each image is based on the circleci version with the following changes:

### all
1. install `cc-test-reporter`
2. install `apt-utils` and `apt-transport-https`
3. don't install any of the `docker` packages - they are huge and we don't use them.

### circleci-node
1. allow `ENV` variables to specify the versions of `node`, `npm`, and `yarn` and set these from our `package.json`

### circleci-ruby
1. allow `ENV` variable to specify the version of `bundler` we use and set this from `Gemfile.lock`
2. `bundle config build.nokogiri --use-system-libraries` so we don't try to build our own version whenever we do a `bundle install`

### circleci-ruby-browser
1. everything from `circleci-ruby`
2. install `chrome` and `chromedriver`

## Build
If you need to make changes to these images you'll need to build and push them out so that our circleci test process picks them up.
1. [install docker](https://docs.docker.com/docker-for-mac/install/)
2. login to [dockerhub](https://hub.docker.com) and make sure you're a member of the [joinswoop org](https://hub.docker.com/u/joinswoop/dashboard/) (ask jon for help here)
3. run the build:
````
$ cd .circle/images/
$ ./build.sh
```
