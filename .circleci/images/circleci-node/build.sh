#!/usr/bin/env bash
set -e
export REPO=joinswoop/circleci-node
NODE_VERSION=$(grep '"node":' ../../../package.json|awk {'print $2'}|sed 's/[",]//g')
YARN_VERSION=$(grep '"yarn":' ../../../package.json|awk {'print $2'}|sed 's/[",]//g')
NPM_VERSION=$(grep '"npm":' ../../../package.json|awk {'print $2'}|sed 's/[",]//g')
PREV_LATEST_ID=$(docker images $REPO:latest -q)
docker build --build-arg NODE_VERSION=$NODE_VERSION \
             --build-arg YARN_VERSION=$YARN_VERSION \
             --build-arg NPM_VERSION=$NPM_VERSION \
             --tag $REPO:latest \
             .


# make sure the tags in config.yml are correct
PREV_IMAGE_TAG=$(docker images $REPO| sort -n -k 2| tail -1|awk {'print $2'})
sed -E -i '' 's'"@$REPO"':[0-9]+@'"$REPO"':'"$PREV_IMAGE_TAG"'@g' ../../config.yml

LATEST_ID=$(docker images $REPO:latest -q)

if [[ "$PREV_LATEST_ID" != "$LATEST_ID" ]]; then
  # bump the tag version up by one
  IMAGE_TAG=$((PREV_IMAGE_TAG + 1))
  sed -E -i '' 's'"@$REPO"':[0-9]+@'"$REPO"':'"$IMAGE_TAG"'@g' ../../config.yml
  echo "adding new tag $REPO:${IMAGE_TAG:=0}"
  docker tag $REPO:latest $REPO:${IMAGE_TAG:=0}
  docker push $REPO:${IMAGE_TAG:=0}
  docker push $REPO:latest
fi
