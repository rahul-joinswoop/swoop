#!/usr/bin/env bash
set -e
for i in circleci-*; do
  cd $i
  ./build.sh
  cd ..
done
