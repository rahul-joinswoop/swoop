# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.5.6'

# load up our .env file before anything else in test and dev
gem 'dotenv-rails', '2.7.5', require: ENV.key?('DOCKER_RUN') ? false : 'dotenv/rails-now', groups: %i(development test)

#======
# CORE
#====

gem 'aasm',                         '5.0.6'
gem 'eventmachine',                 '1.2.7'
gem 'openssl',                      '2.1.2'
gem 'pg',                           '1.1.4'
gem 'activerecord-postgis-adapter', '5.2.2'
gem 'fx',                           '0.5.0'
gem 'scenic',                       '1.5.1'
gem 'puma',                         '4.2.0'
gem 'rails',                        '5.2.3'

#============
# API & JSON
#==========

# graphql subscriptions
gem 'ably-rest', '1.1.2'
# DO NOT UPDATE THIS GEM - 6.2.1 breaks CSRF tokens for us!
gem 'gon',                       '6.2.0'
gem 'graphql',                   '1.9.12'
gem 'graphql-batch',             '0.4.1'
gem 'graphql-errors',            '0.3.0'
gem 'graphql-preload',           '2.1.0'
gem 'jbuilder',                  '2.9.1'
gem 'keen',                      '1.1.1'
gem 'rack-cors',                 '1.0.3'

# graphql-pro - make sure you have BUNDLE_GEMS__GRAPHQL__PRO set in your env
# or this won't work
source "https://gems.graphql.pro/" do
  gem "graphql-pro", '1.10.7'
end

#===============
# 3rd party API
#=============

# PDW use carrierwave-aws when needed gem 'carrierwave', '0.10.0'
# gem 'amazon-ecs',              '2.3.0'  # this and carrierwave
# affect the open call to tomtom somehow
gem 'aws-sdk-s3',                '1.48.0'
gem 'edmunds',                   '0.0.8'
gem 'geocoder',                  '1.5.1'
gem 'fast-polylines',            '1.0.0'
gem 'looker-sdk'
gem 'newrelic_rpm',              '6.6.0.358'
gem 'pagerduty',                 '2.1.2'
gem 'twilio-ruby',               '4.13.0'

#==================
# Background tasks
#================

gem 'clockwork',                 '2.0.4'
gem 'delayed_job_active_record', '4.1.4'
gem 'redis',                     '~> 4.1.3'
gem 'sidekiq',                   '~> 6.0.0', require: ['sidekiq', 'sidekiq/web']
gem 'sidekiq-transaction_guard', '~> 1.0.0'
gem 'sidekiq-unique-jobs',       '~> 6.0.13'

#============
# Auth & Aut
#==========

gem 'bcrypt',                    '3.1.13'
gem 'devise',                    '4.7.1'
gem 'doorkeeper',                '5.2.1'
gem 'rolify',                    '5.2.0'

#===================
# Async and sockets
#=================

gem 'faye-websocket',            '0.10.9'
# gem 'pusher',                    '1.3.0' #PDW: ios Notifications not used atm

# PDW this is just to resolve a dependency of rail 5.0.6 on ~>0.6.1
gem 'websocket-driver', '0.7.1'

#=========
# Caching
#=======

# optional, for faster hashing (C-Ruby only)
gem 'cityhash',                  '0.9.0'
gem 'identity_cache',            '0.5.1'
gem 'redis-rails', '5.0.2'

#========================
# Documentation & alerts
#======================

gem 'slack-api', '1.6.1'

#=================
# File generation
#===============

gem 'wicked_pdf', '1.1'

#=============
# i18n / l10n
#===========
gem 'money-rails', '1.13.2'

#=============
# Convenience
#===========
gem 'activerecord-import',     '1.0.2'
gem 'base62',                  '1.0.0'
gem 'browser',                 '2.6.1'
gem 'full-name-splitter',      '0.1.2'
gem 'has_scope',               '0.7.2'
gem 'hashie',                  '~>3.6.0'
gem 'http',                    '~> 4.1.1'
gem 'http-2',                  '0.10.1'
gem 'httparty',                '0.17.1'
gem 'interactor',              '3.1.1'
gem 'lograge',                 '0.11.2'
gem 'phonelib',                '0.6.37'
gem 'request_store',           '1.4.1'
gem 'savon',                   '~> 2.12.0'
gem 'StreetAddress',           '1.0.6'
gem 'will_paginate',           '3.1.8' # must occur before elasticsearch
gem 'xxhash',                  '0.4.0'
#========
# Heroku
#======

gem 'em-hiredis',              '0.3.1'
gem 'hirefire-resource',       '0.7.2'
gem 'platform-api',            '2.2.0'

#===========
# Analytics
#=========

gem 'analytics-ruby',          '2.2.7', require: 'segment/analytics'
# gem 'oink',                    '0.10.1'
# gem 'skylight',                  '1.1.0'
gem 'splitclient-rb',          '7.0.0'

#========
# Search
#======

gem 'addressable',             '2.7.0'
gem 'elasticsearch-model', '~> 5.0', '>= 5.0.2'
gem 'elasticsearch-rails', '~> 6.1'

#========
# Logging
#======
gem 'logstash-logger',         '0.26.1'

#============
# Middleware
#==========

gem 'rack-rails-logger',       '1.0.3'
# enable Server-Timing headers everywhere except production
gem 'server_timing_middleware', git: 'https://github.com/joinswoop/server_timing_middleware',
                                require: ENV['SWOOP_ENV'] != 'production'

#============
# Others
#==========

gem 'active_model_serializers', '0.10.10'
gem 'active_record_union', '1.3.0'
gem 'after_commit_everywhere', '0.1.4'
gem 'ansi' # PDW used to get progress bars in ES imports
gem 'bcrypt_pbkdf', '~> 1.0.1' # required for ssh key auth with net-sftp
gem 'concurrent-ruby-ext', '1.1.5'
gem 'connection_pool', '2.2.2'
gem 'ed25519', '~> 1.2.4' # required for ssh key auth with net-sftp
gem 'fast_blank', '~> 1.0.0'
gem 'flipper', '0.16.2'
gem 'flipper-redis', '0.16.2'
gem 'jwt', '1.5.6'
gem 'net-sftp', '~> 2.1.2'
gem 'nokogiri', '1.10.4'
gem 'oj', '3.9.1'
gem 'rest-client', '2.1.0'
gem 'roda', '3.24.0'
gem 'rollbar', '2.22.1'
gem 'seed_migration', git: "https://github.com/joinswoop/seed_migration.git", branch: "import"
gem 'shrine', '2.19.3'
gem 'simple_thread_pool', '~> 1.0.0'
gem 'simple_throttle', '~> 1.0.1'
gem 'strip_attributes', '~> 1.9.0'
gem 'stripe', '4.21.3'
gem 'terminal-table', '1.8.0'
gem 'policy_coverage', '0.1.4', git: "https://github.com/joinswoop/policy_coverage.git"
gem 'us_geo', '~> 1.0.3'
gem 'rgeo-geojson', '2.1.1'
# gem 'policy_coverage', :path => "../coverage"

gem 'strong_migrations', '0.4.1'
gem 'migration-lock-timeout', '~> 1.2.0'

#
# Test Data
#
gem 'faker',                 '2.4.0'

gem 'bootsnap',              '1.4.5', require: false

group :development do
  gem 'pry-byebug'
  gem 'pry-remote'
  # as of today this is the newest version supported by codeclimate:
  #
  # https://github.com/codeclimate/codeclimate-rubocop/branches/all?utf8=%E2%9C%93&query=channel%2Frubocop
  #
  # if you want to update this you'll need to do it here and in .codeclimate.yml
  #
  # also right now airbnb's style guide is locked at 0.57.2:
  #
  # https://github.com/airbnb/ruby/issues/146#issuecomment-425995871
  #
  # to update:
  #
  # $ echo > .rubocop_todo.yml
  # $ rubocop -a
  # $ rubocop --auto-gen-config
  gem 'rubocop', '~> 0.74.0', require: false
  # use locally by ides, see https://github.com/castwide/solargraph
  gem 'solargraph', '0.37.2', require: false
end

group :development, :test do
  gem 'active_record_query_trace'
  gem 'bump', '0.8.0'
  gem 'byebug', '11.0.1'
  gem 'climate_control', '0.2.0'
  gem 'derailed'
  gem 'ejson', '1.2.1'
  gem 'fakes3', '1.2.1', require: false
  # used by derailed, factory_doctor, factory_profiler
  gem 'flamegraph', '0.9.5', require: false
  gem 'foreman',               '0.85.0'
  gem 'gem-release',           '2.0.3'
  gem 'letter_opener',         '1.7.0'
  gem 'parallel_tests'
  gem 'pry-rails'
  gem 'rake'
  gem 'redis-rails-instrumentation', '1.0.1', require: false
  gem 'rspec-collection_matchers', '~> 1.2.0'
  gem 'rspec-rails',           '3.8.2'
  gem 'selenium-webdriver',    '3.142.4'
  gem 'shrine-memory',         '0.3.1'
  # needed for flamegraph
  gem 'stackprof', '0.2.12', require: false
  # this provides binaries for osx and our docker images used in tests. we need
  # to use wkhtmltopdf-heroku in production for the same thing
  gem 'wkhtmltopdf-binary-edge', git: 'https://github.com/joinswoop/wkhtmltopdf-binary-edge'
  gem 'rspec-json_expectations'
end

group :test do
  gem 'capybara',              '3.29.0'
  gem 'capybara-selenium',     '0.0.6'
  gem 'database_cleaner',      '1.7.0'
  gem 'factory_bot_any_instance', '1.0.0'
  gem 'factory_bot_rails',     '5.0.2'
  gem 'rails-controller-testing'
  gem 'rails-dom-testing',     '2.0.3'
  gem 'rspec-instafail', require: false
  gem 'rspec-sidekiq'
  gem 'rspec_junit_formatter', '0.4.1'
  gem 'rubocop-performance', '1.4.1', require: false
  gem 'rubocop-rails', '2.3.2', require: false
  gem 'rubocop-rspec', '1.35.0', require: false
  gem 'shoulda-matchers', '4.1.2'
  gem 'simplecov', require: false
  gem 'single_test'
  gem 'timecop', '0.9.1'
  gem 'vcr'
  gem 'webmock', '3.7.5'
end

group :production do
  # used for pdf generation in production (heroku), wkhtmltopdf-binary-edge is
  # used for dev/test
  gem 'wkhtmltopdf-heroku', '2.12.5.0-alpha'
end
