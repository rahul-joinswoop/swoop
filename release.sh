#!/bin/bash

set -o errexit
set -o xtrace

bump patch --tag
git push
git push --tags
