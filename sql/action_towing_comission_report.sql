SELECT    accounts.NAME                                                            AS "accounts.name",
          jobs.id                                                                  AS "jobs.id", 
          Date((jobs.created_at )::timestamptz at time zone 'America/Los_Angeles') AS "jobs.created_date",
          driver.first_name 
                    || ' ' 
                    || driver.last_name AS "driver.full_name", 
          COALESCE(COALESCE( ( Sum(DISTINCT (Cast(Floor(COALESCE(invoicing_line_items.net_amount ,0)*(1000000*1.0)) AS DECIMAL(65,0))) + ('x' 
                    || Md5(invoicing_line_items.id ::                                                                  varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "invoicing_line_items.sum_net_amount",
          COALESCE(COALESCE( ( sum(DISTINCT (cast(floor(COALESCE(invoicing_line_items.net_amount*(.3) ,0)*(1000000*1.0)) AS decimal(65,0))) + ('x' 
                    || md5(invoicing_line_items.id ::                                                                       varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "invoicing_line_items.sum_net_amount_30commish",
          COALESCE(COALESCE( ( sum(DISTINCT (cast(floor(COALESCE(invoicing_line_items.tax_amount ,0)*(1000000*1.0)) AS decimal(65,0))) + ('x' 
                    || md5(invoicing_line_items.id ::                                                                  varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "invoicing_line_items.sum_tax_amount"
FROM      PUBLIC.jobs                                                                                                                AS jobs
LEFT JOIN PUBLIC.accounts                                                                                                            AS accounts
ON        jobs.account_id = accounts.id 
LEFT JOIN PUBLIC.invoicing_ledger_items AS invoicing_ledger_items 
ON        jobs.id = invoicing_ledger_items.job_id 
AND       invoicing_ledger_items.sender_id = jobs.rescue_company_id 
AND       invoicing_ledger_items.deleted_at IS NULL 
LEFT JOIN PUBLIC.invoicing_line_items AS invoicing_line_items 
ON        invoicing_ledger_items.id = invoicing_line_items.ledger_item_id 
AND       invoicing_line_items.deleted_at IS NULL 
LEFT JOIN PUBLIC.companies AS rescue_company 
ON        jobs.rescue_company_id = rescue_company.id 
LEFT JOIN PUBLIC.users AS driver 
ON        jobs.rescue_driver_id = driver.id 
WHERE     (((( 
                                                  jobs.created_at ) >= ((timestamp '2017-07-01' at time zone 'America/Los_Angeles')) 
                              AND       ( 
                                                  jobs.created_at ) < ((timestamp '2017-07-17' at time zone 'America/Los_Angeles'))))) 
AND       ( 
                    jobs.status = 'Completed' 
          OR        jobs.status = 'GOA' 
          OR        jobs.status = 'Released') 
AND       ( 
                    invoicing_line_items.description NOT LIKE '%storage%' 
          AND       invoicing_line_items.description NOT LIKE '%fuel%' 
          OR        invoicing_line_items.description IS NULL) 
AND       ( 
                    rescue_company.NAME = 'Action Towing - Tampa') 
GROUP BY  1, 
          2, 
          3, 
          4 
ORDER BY  4 limit 500

-- sql for creating the total
SELECT    COALESCE(COALESCE( ( Sum(DISTINCT (Cast(Floor(COALESCE(invoicing_line_items.net_amount ,0)*(1000000*1.0)) AS DECIMAL(65,0))) + ('x' 
                    || Md5(invoicing_line_items.id ::                                                                  varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "invoicing_line_items.sum_net_amount",
          COALESCE(COALESCE( ( sum(DISTINCT (cast(floor(COALESCE(invoicing_line_items.net_amount*(.3) ,0)*(1000000*1.0)) AS decimal(65,0))) + ('x' 
                    || md5(invoicing_line_items.id ::                                                                       varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "invoicing_line_items.sum_net_amount_30commish",
          COALESCE(COALESCE( ( sum(DISTINCT (cast(floor(COALESCE(invoicing_line_items.tax_amount ,0)*(1000000*1.0)) AS decimal(65,0))) + ('x' 
                    || md5(invoicing_line_items.id ::                                                                  varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "invoicing_line_items.sum_tax_amount"
FROM      PUBLIC.jobs                                                                                                                AS jobs
LEFT JOIN PUBLIC.accounts                                                                                                            AS accounts
ON        jobs.account_id = accounts.id 
LEFT JOIN PUBLIC.invoicing_ledger_items AS invoicing_ledger_items 
ON        jobs.id = invoicing_ledger_items.job_id 
AND       invoicing_ledger_items.sender_id = jobs.rescue_company_id 
AND       invoicing_ledger_items.deleted_at IS NULL 
LEFT JOIN PUBLIC.invoicing_line_items AS invoicing_line_items 
ON        invoicing_ledger_items.id = invoicing_line_items.ledger_item_id 
AND       invoicing_line_items.deleted_at IS NULL 
LEFT JOIN PUBLIC.companies AS rescue_company 
ON        jobs.rescue_company_id = rescue_company.id 
LEFT JOIN PUBLIC.users AS driver 
ON        jobs.rescue_driver_id = driver.id 
WHERE     (((( 
                                                  jobs.created_at ) >= ((timestamp '2017-07-01' at time zone 'America/Los_Angeles')) 
                              AND       ( 
                                                  jobs.created_at ) < ((timestamp '2017-07-17' at time zone 'America/Los_Angeles'))))) 
AND       ( 
                    jobs.status = 'Completed' 
          OR        jobs.status = 'GOA' 
          OR        jobs.status = 'Released') 
AND       ( 
                    invoicing_line_items.description NOT LIKE '%storage%' 
          AND       invoicing_line_items.description NOT LIKE '%fuel%' 
          OR        invoicing_line_items.description IS NULL) 
AND       ( 
                    rescue_company.NAME = 'Action Towing - Tampa') limit 1
