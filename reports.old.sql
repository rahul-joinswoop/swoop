-- **** DEPRECATED ****
--
-- This file is no longer used. Reports are now defined in db/reports.yml with the SQL in db/reports/*
-- Leaving this around for awhile just in case we need to refer to it or someone is looking
-- for where the reports have moved to.

-- Want feed back on whats happening at this point, unfortunately you cant change return type without dropping
-- the function.
DROP FUNCTION IF EXISTS merge_reports(character varying,character varying,text,json,json,json,json,json);
DROP FUNCTION IF EXISTS merge_reports(character varying,character varying,text,json,json,json,json,json,integer);

CREATE OR REPLACE FUNCTION merge_reports(key VARCHAR, c_type VARCHAR, report_sql TEXT, def_params JSON, a_params JSON, filter JSON, m_params JSON, avail_params JSON, display_ord INTEGER DEFAULT NULL, display_nm VARCHAR DEFAULT NULL) RETURNS VARCHAR AS
$$
DECLARE report_version VARCHAR;
BEGIN
    LOOP
        SELECT SUBSTR(MD5(report_sql),0,7) into report_version;

        UPDATE reports
            SET sql = report_sql,
                default_params = def_params,
                all_params = a_params,
                filter_sql = filter,
                company_type = c_type,
                mandatory_params = m_params,
                available_params = avail_params,
                updated_at       = now(),
                version          = report_version,
                display_order    = display_ord,
                display_name     = display_nm
            WHERE name = key;
        IF found THEN
            -- We updated a report go ahead and let the consumer know.
            RETURN CONCAT(key,
                    CONCAT(' ', CONCAT('Updated to ', report_version)));
        END IF;
        -- not there, so try to insert the key
        -- if someone else inserts the same key concurrently,
        -- we could get a unique-key failure
        BEGIN
            INSERT INTO reports(name, sql, default_params, all_params, filter_sql, mandatory_params, available_params, company_type, created_at, updated_at, version, display_order, display_name)
            VALUES (key, report_sql, def_params, a_params, filter, m_params, avail_params, c_type, now(), now(), SUBSTR(MD5(report_sql),0,7), display_ord, display_nm);
            -- Start providing Feed back of the report being instered
            RETURN CONCAT(key,
                      CONCAT(' ', CONCAT('Inserted at ', report_version)));
        EXCEPTION WHEN unique_violation THEN
            -- do nothing, and loop to try the UPDATE again
        END;
    END LOOP;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_report_name(old_name VARCHAR, new_name VARCHAR) RETURNS VOID AS
$$
BEGIN
  UPDATE reports
      SET name = new_name
      WHERE name = old_name;
  IF found <> TRUE THEN
      RETURN;
  END IF;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_pdf_to_report(old_name VARCHAR) RETURNS VOID AS
$$
BEGIN
  UPDATE reports
      SET pdf = TRUE
      WHERE name = old_name;
  IF found <> TRUE THEN
      RETURN;
  END IF;
END;
$$
LANGUAGE plpgsql;



/* Used from qbd.sql as well */
CREATE OR REPLACE FUNCTION adjusted_ajs(jobid int, status_id int)
  RETURNS timestamp
  AS $function$
    BEGIN
      RETURN
        CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END
        FROM audit_job_statuses
        where job_status_id=status_id
          and audit_job_statuses.job_id=jobid
          and audit_job_statuses.deleted_at is null
        order by id desc limit 1;
    END;
  $function$
LANGUAGE 'plpgsql' IMMUTABLE;




CREATE OR REPLACE FUNCTION US_time(ts timestamp, tz text)
  RETURNS text
  AS $function$
    BEGIN
      RETURN to_char(ts at time zone 'UTC' AT TIME ZONE COALESCE(tz, 'America/Los_Angeles'),'MM/DD/YYYY HH24:MI');
    END;
  $function$
LANGUAGE 'plpgsql' IMMUTABLE;




CREATE OR REPLACE FUNCTION hms(i interval)
  RETURNS text
  AS $function$
    BEGIN
      RETURN to_char(i,'HH24:MI:SS');
    END;
  $function$
LANGUAGE 'plpgsql' IMMUTABLE;




-- Merge Reports Params Method
-- SELECT merge_reports(name, company_type, sql, default_params, all_params, filter_sql, mandatory_params, available_params)



-- Farmers ETA Variance Report
-- signed off PDW , Sameer 2:42 fri 2th oct
-- Farmers ETA Variance Report Updated to 3a4307
-- Farmers ETA Variance Report Updated to dfb98e: filter out deleted
-- Farmers ETA Variance Report Updated to 94266f:Remove GOA canceled
SELECT merge_reports('Farmers ETA Variance Report' , 'FleetCompany', $$select
 "Job ID",
 "Date (Central)",
 "Time (Central)",
 "Service Requested",
 "Policy Number",
 "Claim Number",
 "Status",
 "Pickup City",
 "ETA",
 "ATA",
 "ATA"-"ETA" as "ATA-ETA",
 CASE WHEN ("ATA"-"ETA") < 0 THEN 1 ELSE 0 END as "ATA-ETA < 0",
 CASE WHEN ("ATA"-"ETA") <= 10 THEN 1 ELSE 0 END as "ATA-ETA <= 10"
from
(
select
  jobs.id as "Job ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  service_codes.name as "Service Requested",
  jobs.status as "Status",
  pickup.city as "Pickup City",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ETA",
  round(cast((Extract(EPOCH FROM
  (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
  (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ATA",
  '' as "Variance",

    -- Policy Addr
  jobs.policy_number   as "Policy Number",
  policy_addr.street  as "Policy Street",
  policy_addr.city    as "Policy City",
  policy_addr.state   as "Policy State",
  policy_addr.zip     as "Policy Zip",

  pcc_claim.claim_number AS "Claim Number"

from jobs
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join locations policy_addr on policy_addr.id = jobs.policy_location_id
  LEFT JOIN pcc_claims pcc_claim on pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.status in ('Completed','Released') and
  jobs.scheduled_for is null and
  fleet_company_id = (SELECT id FROM companies WHERE name = 'Farmers Insurance') and
  jobs.created_at >= ? and
  jobs.created_at < ?
order by jobs.id desc) as t1$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{} ',
'{   "start_date":{"type":"start_date"},   "end_date":{"type":"end_date"}} ');





-- 21st Century ETA Variance Report
SELECT merge_reports('21st Century Billing File' , 'SuperCompany', $$SELECT
  '' AS "BatchCreated",
  'InboundPaymentRequestCCAS' AS "TransType",
  'CCAS' AS "ReceivedBy",
  pcc_claim.data->'table'->>'claimUnitId' AS "EntityID",
  '' AS "ReceivedDate",
  pcc_claim.data->'table'->>'claimUnitNumber' AS "ClaimUnitNumber",
  'Security Advantage Towing' AS "PaymentClass",
  CASE
    WHEN symptom.name = 'Accident' THEN 'ACCDT'
    WHEN symptom.name = 'Flat tire' THEN 'FLTSP'
    WHEN symptom.name = 'Out of fuel' THEN 'GAS'
    WHEN symptom.name = 'Locked out' THEN 'LCKSI'
    WHEN (symptom.name = 'Inoperable Problem' OR symptom.name = 'Specialty Vehicle/RV Disablement') THEN 'VINOP'
    WHEN symptom.name = 'Stuck' THEN 'WINCH'
    WHEN symptom.name = 'Would not Start' THEN 'WNTST'
  END AS "ProblemType",
  'N' AS "Accident",
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN NULL
        WHEN pcc_coverage.covered IS TRUE THEN 'Y'
    ELSE 'N'
  END AS "TowCoverageStatus", -- override CASE
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN 'Y'
    ELSE 'N'
  END AS "SupervisorApproval", -- override CASE
  '' AS "CCASBatch Number",
  '57.56' AS "Dispatch Fee",
  '0' AS "Excess Claim Amount",
  '1' AS "TaskNumber",
  LPAD(job.id::text, 10, '0') AS "CaseNumber", -- leading 0
  job.id AS "CCASPONumber",
  TO_CHAR((SELECT
      CASE
          WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
          ELSE last_set_dttm
      END
   FROM (
     SELECT last_set_dttm, adjusted_dttm
     FROM audit_job_statuses
     WHERE deleted_at IS NULL
     AND job_status_id = 9 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY') AS "ServiceStartDate", -- AJS created date
  CASE
    WHEN job.status = 'Completed' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 9 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'GOA' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 21 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'Canceled' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 8 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'Released' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 18 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     ELSE ''
     END AS "ServiceEndDate", -- AJS end date
  CASE
    WHEN job.status = 'goa' THEN 'GOA'
    WHEN job.status = 'canceled' THEN 'CAN'
    WHEN service.name = 'Tow' OR service.name = 'Tow if Jump Fails' OR service.name = 'Tow if Tire Change Fails' THEN 'TOW'
    WHEN service.name = 'Battery Jump' OR service.name = 'Tire Change' OR service.name = 'Fuel Delivery' OR service.name = 'Winch Out' THEN 'ROAD'
    WHEN service.name = 'Lock Out' THEN 'LOCK'
    WHEN service.name = 'Accident Tow' THEN 'ACCDT'
  END AS "TypeOfService",
  'Final Payment' AS "PaymentType",
  job.rescue_company_id AS "ServiceProviderID",
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN 'HP'
    ELSE NULL
  END AS "CoverageVerificationSource", -- override CASE
  'N' AS "Resubmitted",
  '21' AS "IGI",
  '' AS "VehicleType"
FROM jobs job
JOIN companies fleet_company ON job.fleet_company_id = fleet_company.id
LEFT JOIN pcc_claims pcc_claim ON job.pcc_claim_id = pcc_claim.id
LEFT JOIN pcc_coverages pcc_coverage ON pcc_claim.pcc_coverage_id = pcc_coverage.id
LEFT JOIN service_codes service ON service.id = job.service_code_id
LEFT JOIN symptoms symptom ON symptom.id = job.symptom_id
WHERE fleet_company.name = '21st Century Insurance'
AND job.status IN ('Completed', 'Released', 'Canceled', 'GOA')
AND job.created_at >= ?
AND job.created_at < ?
order by job.id desc $$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{} ',
'{   "start_date":{"type":"start_date"},   "end_date":{"type":"end_date"}} ');






-- 21st Century ETA Variance Report
SELECT merge_reports('21st Century ETA Variance Report' , 'FleetCompany', $$select
 "Job ID",
 "Date (Central)",
 "Time (Central)",
 "Service Requested",
 "Policy Number",
 "Claim Number",
 "Status",
 "Pickup City",
 "ETA",
 "ATA",
 "ATA"-"ETA" as "ATA-ETA",
 CASE WHEN ("ATA"-"ETA") < 0 THEN 1 ELSE 0 END as "ATA-ETA < 0",
 CASE WHEN ("ATA"-"ETA") <= 10 THEN 1 ELSE 0 END as "ATA-ETA <= 10"
from
(
select
  jobs.id as "Job ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  service_codes.name as "Service Requested",
  jobs.status as "Status",
  pickup.city as "Pickup City",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ETA",
  round(cast((Extract(EPOCH FROM
  (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
  (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ATA",
  '' as "Variance",

    -- Policy Addr
  jobs.policy_number   as "Policy Number",
  policy_addr.street  as "Policy Street",
  policy_addr.city    as "Policy City",
  policy_addr.state   as "Policy State",
  policy_addr.zip     as "Policy Zip",

  pcc_claim.claim_number AS "Claim Number"

from jobs
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join locations policy_addr on policy_addr.id = jobs.policy_location_id
  LEFT JOIN pcc_claims pcc_claim on pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.status in ('Completed','Released') and
  jobs.scheduled_for is null and
  fleet_company_id = (SELECT id FROM companies WHERE name = '21st Century Insurance') and
  jobs.created_at >= ? and
  jobs.created_at < ?
order by jobs.id desc) as t1$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{} ',
'{   "start_date":{"type":"start_date"},   "end_date":{"type":"end_date"}} ');


-- Driver Report Report
-- Driver Report Updated to ff7677: limit job states per Sameer
-- Driver Report Updated to 8e9b58: unlimit jobs pre state, convert where join on invoice to on clause in join, exclude deleted jobs, order on dispatched datetime
-- Driver Report Updated to 8e9b58: Update available params to support start/end date defaults
-- Driver Report Updated to a20787: Users users timezone
-- Driver Report Updated to 2b6647: Date trunc and coalesce 0 Drive Times
-- Driver Report Updated to 743d62: Remove filter fallback to created_at per Sammer & Rob
-- Driver Report Updated to 2a6672 join with Invoice
SELECT merge_reports('Driver Report' , 'RescueCompany', $$
SELECT
  "Swoop ID",
  "Dispatched Date",
  "Status",
  "Driver",
  "Truck",
  "Account",
  "Dispatched",
  "En Route",
  "On Site",
  "Completed",
  CONCAT(COALESCE((EXTRACT(DAY FROM total_time) * 24) + (EXTRACT(HOUR FROM total_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM total_time)::TEXT, '0'), 2, '0'), ':00') AS "Total Time",
  CONCAT(COALESCE((EXTRACT(DAY FROM drive_time) * 24) + (EXTRACT(HOUR FROM drive_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM drive_time)::TEXT, '0'), 2, '0'), ':00') AS "Drive Time",
  "Total Miles",
  "Payment Type",
  "Invoice Total"
FROM (
  Select
        jobs.id as "Swoop ID",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) as "Dispatched Date",
        jobs.status as "Status",
        CONCAT(driver.first_name, ' ', driver.last_name) as "Driver",
        truck.name as "Truck",
        accounts.name as "Account",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) as "Dispatched",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1) as "En Route",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc  limit 1) as "On Site",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) as "Completed",
        ((select CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) -
        (select CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1)) as total_time,
        (
           (coalesce(((select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=6 and job_id=jobs.id order by id desc limit 1) -
           (select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=5 and job_id=jobs.id order by id desc  limit 1)),interval '0')
         +
           ((select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) -
           (select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1)))
        ) as drive_time,
        ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) AS "Total Miles",
        customer_types.name AS "Payment Type",
         CAST(
           (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
              from invoicing_line_items
              where invoices.id=invoicing_line_items.ledger_item_id
               and invoicing_line_items.description!='Storage'
               and invoicing_line_items.deleted_at is null
           group by invoices.id) AS DECIMAL(14, 2)
          ) as "Invoice Total"

        from jobs
        left join users as customer on jobs.user_id=customer.id
        left join users as driver on jobs.rescue_driver_id=driver.id
        left join vehicles as truck on jobs.rescue_vehicle_id=truck.id
        left join invoicing_ledger_items as invoices on invoices.job_id=jobs.id and invoices.deleted_at is NULL and invoices.sender_id=? and invoices.type='Invoice'
        left join accounts on accounts.id=jobs.account_id
        left join customer_types on jobs.customer_type_id = customer_types.id
        left join estimates ON estimates.id = jobs.estimate_id
        left join users on users.id=?

        Where
          jobs.rescue_company_id=?
          AND jobs.deleted_at is null
          AND (select (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) >= ?
          AND (select (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) < ?
          AND jobs.status != 'Reassigned'
    {{driver}}
    {{vehicle}}
    {{account}}
    {{payment_type}}
    ORDER BY (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1)
  ) AS results;
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'[ "sender_id", "api_user.id", "rescue_company_id", "start_date", "end_date", "driver", "vehicle", "account", "payment_type"]',
'{
  "driver": "and rescue_driver_id = ?",
  "vehicle": "and rescue_vehicle_id = ?",
  "account": "and account_id = ?",
  "payment_type": "and jobs.customer_type_id = ?"
}',
'{ "api_user.id": "api_user.id", "rescue_company_id": "api_company.id", "sender_id": "api_company.id" }',
' {
  "start_date": { "type": "eval", "method": "start_date_yesterday_beginning_of_day" },
  "end_date": { "type": "eval", "method": "end_date_yesterday_end_of_day" },
  "driver": { "type":"eval", "method": "collatable_drivers" },
  "vehicle": { "type":"eval", "method": "customize_vehicle_label" },
  "account": { "type": "account" },
  "payment_type": { "type": "eval", "method": "payment_types" }
}',
1,
'Driver Jobs Report');

SELECT add_pdf_to_report('Driver Report');

--EHI Driver Report Updated ENG-2315
--EHI Driver Report Updated to 3559e0: ENG-1173
--EHI Driver Report Updated to a6cb0c : coalesce jobs.adjusted_created_at
--EHI Driver Report Updated to 9c1d5b: Join with Invoice
SELECT merge_reports('EHI Driver Report' , 'FleetInHouseCompany', $$Select
        jobs.id as "Swoop ID",
        jobs.status as "Status",
        to_char(jobs.adjusted_created_at at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY') as "Created Date",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) as "Dispatched Date",
        CONCAT(driver.first_name, ' ', driver.last_name) as "Driver",
        truck.name as "Truck",
        accounts.name as "Account Name",
        to_char(jobs.adjusted_created_at at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') as "Created",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) as "Dispatched",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1) as "En Route",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc  limit 1) as "On Site",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) as "Completed",


          to_char(
            ((select CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) -
            (select CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1)),
          'DD:HH24:MI') as "Total Time (min)",

          -- (Towdest - towing) + (On - En Rout)
          to_char(
                 (coalesce(((select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=6 and job_id=jobs.id order by id desc limit 1) -
                 (select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=5 and job_id=jobs.id order by id desc  limit 1)),interval '0')
               +
                 ((select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) -
                 (select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1))),
          'DD:HH24:MI') as "Drive Time (min)",



         CAST(
           (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
              from invoicing_line_items
              where invoices.id=invoicing_line_items.ledger_item_id
               and invoicing_line_items.deleted_at is null
               and invoicing_line_items.description != 'Storage'
           group by invoices.id) AS DECIMAL(14, 2)
          ) as "Amount"


        from jobs
        left join users as customer on jobs.user_id=customer.id
        left join users as driver on jobs.rescue_driver_id=driver.id
        left join vehicles as truck on jobs.rescue_vehicle_id=truck.id
        left join invoicing_ledger_items as invoices on invoices.job_id=jobs.id and invoices.deleted_at is NULL and invoices.sender_id=(select id from companies where name='EHI Truck') and invoices.type='Invoice'
        left join accounts on accounts.id=jobs.account_id
        left join users on users.id=?

        Where
          jobs.fleet_company_id=(select id from companies where name='Enterprise')
          AND jobs.rescue_company_id=(select id from companies where name='EHI Truck')
          AND jobs.deleted_at is null
          AND coalesce((select (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1),jobs.adjusted_created_at) >= ?
          AND coalesce((select (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1),jobs.adjusted_created_at) < ?
          AND jobs.status != 'Reassigned'
    ORDER BY coalesce((SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1),jobs.adjusted_created_at)
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'[ "api_user.id","start_date","end_date"]',
'{}',
'{  "api_user.id": "api_user.id" }',
'{ "start_date":{"type":"eval", "method": "start_date_yesterday_beginning_of_day"},  "end_date":{"type":"eval", "method": "end_date_yesterday_end_of_day"}}');



-- Reviews Report
SELECT merge_reports('Reviews' , 'SuperCompany', $$select
  to_char(reviews.created_at at time zone 'UTC' at time zone 'America/Los_Angeles', 'YYYY/MM/DD HH24:MI:SS') as "Date PDT",
  creating_company.name as "Fleet Company",
  rescue_company.name as "Rescue Company",
  job_id as "JobID",
  reviews.type,
  reviews.rating,
  reviews.nps_question1 as "NPS Recommend",
  reviews.nps_question2 as "NPS Informed",
  reviews.nps_question3 as "NPS ETA",
  reviews.nps_feedback,
  (select array_agg(body) as sms_replies  from reviews left join twilio_replies on reviews.id=twilio_replies.target_id and twilio_replies.target_type='Review' where reviews.job_id=jobs.id group by reviews.job_id),
  reviews.id as "ReviewID"
from jobs
    inner join reviews on jobs.id=reviews.job_id
    left join companies as creating_company on jobs.fleet_company_id=creating_company.id
    left join companies as rescue_company on jobs.rescue_company_id=rescue_company.id
where
  not (reviews.nps_question1 is null and reviews.nps_question2 is null and reviews.nps_question3 is null) and
  jobs.created_at >= ? and
  jobs.created_at < ?
  {{rescue_company}}
  {{fleet_company}}
order by reviews.created_at desc;$$,
'{ "start_date" : "2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date", "rescue_company", "fleet_company"]',
'{"rescue_company":"and rescue_company_id=?", "fleet_company":"and fleet_company_id=?"}',
'{}',
'{  "start_date":{"type":"start_date"}, "end_date":{"type":"end_date"}, "rescue_company":{"type":"partner"}, "fleet_company":{"type":"fleet"} }');


-- Farmers CSAT Report
-- Signed off PDW SB 2:58 Fri 10/28
-- Farmers CSAT Updated to 9744d3
-- Farmers CSAT Updated to 6f1355 Updated for deleted_at
-- Farmers CSAT Updated to 5c2716: disambiguate reviews.job_id
-- Farmers CSAT Updated to 4c5560: Add Review Type and Fleet Dispatcher
-- Farmers CSAT Updated to 031fd9: Update Fleet Dispatcher to email
-- Farmers CSAT Updated to 21cfb9: Include nps_feedback if review type typeform
-- Farmers CSAT Updated to 0fc1fb: Farmers extra ENG-175
-- Farmers CSAT Updated to 192529: Add branding "New Link"
-- Farmers CSAT Updated to 6653d2: Add status and scheduled cols, dont show ATA/ETA for scheduled jobs
-- Farmers CSAT Updated to d723a6: Exclude GOA and Canceled - reviewed PDW SB 01/10/17
SELECT merge_reports('Farmers CSAT' , 'FleetCompany', $$
select
  jobs.id as "Dispatch ID",
  /*jobs.status as "Status",
  jobs.parent_id,
  parent.status "Paretn Status",
  to_char(jobs.created_at  at time zone 'UTC' at time zone 'US/Central' , 'MM/DD/YYYY HH24:MI') as "Date (Central)", */
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  jobs.policy_number    as "Policy Number",
  pcc_claim.claim_number as "Claim Number",
  case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
  /*pickup.state as "Pickup State",
  original_pickup.state as "Original Pickup State",
  provider_location.state as "Provider State", */
  CONCAT(customer.first_name,
    CONCAT(' ', customer.last_name)) as "Customer",
  customer.phone        as "Phone",
  jobs.status as "Status",
  case when jobs.scheduled_for is not null then 'Scheduled' else null end as "Scheduled",
  case when jobs.scheduled_for is not null then null else
    round(cast((Extract(EPOCH FROM
      (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric),0)
  end as "ETA",
  case when jobs.scheduled_for is not null then null else
    round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric),0)
  end  as "ATA",
  (case when (reviews.type='Reviews::Typeform' and reviews.branding is true) then 'Branded Link' else
     (case when (reviews.type='Reviews::Typeform') then 'Web Link' else
       (case when (reviews.type='NpsReview') then 'SMS' else null end)
      end)
   end) as "Review Type",
  case when (fleet_dispatchers.email like '%farmersinsurance.com') then 'Farmers' else 'Swoop' end as "Call Taker",
  fleet_dispatchers.email as "Fleet Dispatcher",
  reviews.nps_question1 as "Question 1",
  reviews.nps_question2 as "Question 2",
  reviews.nps_question3 as "Question 3",
  CASE
  WHEN (reviews.type='Reviews::Typeform') THEN reviews.nps_feedback
  WHEN (reviews.type='NpsReview')
    THEN (SELECT string_agg(body,', ')
      FROM (SELECT body FROM reviews
        LEFT JOIN twilio_replies ON reviews.id = twilio_replies.target_id AND twilio_replies.target_type='Review'
        WHERE reviews.job_id=jobs.id
        ORDER BY twilio_replies.id) AS reply_bodies)
  WHEN (reviews.type = 'Reviews::SurveyReview') THEN reviews.nps_feedback
  ELSE NULL
  END AS "Feedback/Comments"
from jobs
    join reviews   /* ONLY include jobs with reviews */
      ON jobs.id = reviews.job_id
    left join drives    ON drives.id = jobs.driver_id
    left join users  customer ON customer.id=drives.user_id
    left join users fleet_dispatchers on jobs.fleet_dispatcher_id=fleet_dispatchers.id
    left join locations as pickup on pickup.id=jobs.service_location_id
    left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
    left join companies provider on jobs.rescue_company_id=provider.id
    left join locations provider_location on provider.location_id=provider_location.id
    left join jobs parent on jobs.parent_id=parent.id
    LEFT JOIN pcc_claims pcc_claim on pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.created_at >= ? and
  jobs.created_at < ? and
  jobs.fleet_company_id = (SELECT id FROM companies WHERE name = 'Farmers Insurance')
  and jobs.status!='GOA'
  and jobs.status !='Canceled'
order by reviews.created_at desc;
$$,
'{ "start_date" : "2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{}',
'{  "start_date":{"type":"start_date"}, "end_date":{"type":"end_date"} }');


-- 21st Century CSAT Report
SELECT merge_reports('21st Century CSAT' , 'FleetCompany', $$
select
  jobs.id as "Dispatch ID",
  /*jobs.status as "Status",
  jobs.parent_id,
  parent.status "Paretn Status",
  to_char(jobs.created_at  at time zone 'UTC' at time zone 'US/Central' , 'MM/DD/YYYY HH24:MI') as "Date (Central)", */
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  jobs.policy_number    as "Policy Number",
  pcc_claim.claim_number as "Claim Number",
  case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
  /*pickup.state as "Pickup State",
  original_pickup.state as "Original Pickup State",
  provider_location.state as "Provider State", */
  CONCAT(customer.first_name,
    CONCAT(' ', customer.last_name)) as "Customer",
  customer.phone        as "Phone",
  jobs.status as "Status",
  case when jobs.scheduled_for is not null then 'Scheduled' else null end as "Scheduled",
  case when jobs.scheduled_for is not null then null else
    round(cast((Extract(EPOCH FROM
      (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric),0)
  end as "ETA",
  case when jobs.scheduled_for is not null then null else
    round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric),0)
  end  as "ATA",
  (case when (reviews.type='Reviews::Typeform' and reviews.branding is true) then 'Branded Link' else
     (case when (reviews.type='Reviews::Typeform') then 'Web Link' else
       (case when (reviews.type='NpsReview') then 'SMS' else null end)
      end)
   end) as "Review Type",
  case when (fleet_dispatchers.email like '%21st.com') then '21st Century' else 'Swoop' end as "Call Taker",
  fleet_dispatchers.email as "Fleet Dispatcher",
  reviews.nps_question1 as "Question 1",
  reviews.nps_question2 as "Question 2",
  reviews.nps_question3 as "Question 3",
  CASE
  WHEN (reviews.type='Reviews::Typeform') THEN reviews.nps_feedback
  WHEN (reviews.type='NpsReview')
    THEN (SELECT string_agg(body,', ')
      FROM (SELECT body FROM reviews
        LEFT JOIN twilio_replies ON reviews.id = twilio_replies.target_id AND twilio_replies.target_type='Review'
        WHERE reviews.job_id=jobs.id
        ORDER BY twilio_replies.id) AS reply_bodies)
  WHEN (reviews.type = 'Reviews::SurveyReview') THEN reviews.nps_feedback
  ELSE NULL
  END AS "Feedback/Comments"
from jobs
    join reviews   /* ONLY include jobs with reviews */
      ON jobs.id = reviews.job_id
    left join drives    ON drives.id = jobs.driver_id
    left join users  customer ON customer.id=drives.user_id
    left join users fleet_dispatchers on jobs.fleet_dispatcher_id=fleet_dispatchers.id
    left join locations as pickup on pickup.id=jobs.service_location_id
    left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
    left join companies provider on jobs.rescue_company_id=provider.id
    left join locations provider_location on provider.location_id=provider_location.id
    left join jobs parent on jobs.parent_id=parent.id
    LEFT JOIN pcc_claims pcc_claim on pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.created_at >= ? and
  jobs.created_at < ? and
  jobs.fleet_company_id = (SELECT id FROM companies WHERE name = '21st Century Insurance')
  and jobs.status!='GOA'
  and jobs.status !='Canceled'
order by reviews.created_at desc;
$$,
'{ "start_date" : "2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{}',
'{  "start_date":{"type":"start_date"}, "end_date":{"type":"end_date"} }');



-- Farmers CSAT Comment Report
-- Farmers CSAT Comment Updated to 8ffbdb
-- Farmers CSAT Comment Updated to 254330:Remove fields add filter for empty review
SELECT merge_reports('Farmers CSAT Comment' , 'FleetCompany', $$
select
  jobs.id as "Dispatch ID",
  reviews.nps_question1 as "Question 1 - How well informed",
  reviews.nps_question2 as "Question 2 - How on time",
  reviews.nps_question3 as "Question 3 - NPS",
  case when (reviews.type='Reviews::Typeform') then reviews.nps_feedback else (case when (reviews.type='NpsReview') then
    (select string_agg(body,', ')
     from (select body
           from reviews
           left join twilio_replies on reviews.id=twilio_replies.target_id and twilio_replies.target_type='Review'
           where reviews.job_id=jobs.id order by twilio_replies.id) as reply_bodies)
   else null end) end as "Feedback/Comments"
from jobs
    join reviews
      ON jobs.id = reviews.job_id
where
  jobs.deleted_at is null and
  reviews.nps_feedback is not null and
  reviews.nps_feedback != '' and
  jobs.created_at >= ? and
  jobs.created_at < ? and
    jobs.fleet_company_id = (SELECT id FROM companies WHERE name = 'Farmers Insurance')
order by reviews.created_at desc;
$$,
'{ "start_date" : "2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{}',
'{  "start_date":{"type":"start_date"}, "end_date":{"type":"end_date"} }');


-- All Jobs Report
-- All Jobs Updated to ae1c79: Added Handle Time
-- All Jobs Updated to 465398: Added state
-- All Jobs Updated to 827a10: Added Reference Number
-- All Jobs Updated to 15e8a5: Cleaned up invoice join and removed garbage jobs
-- All Jobs Updated to 6b47fc: moved invoicing_ledger_items.deleted at to where clause
-- All Jobs Updated to 81d42f: Add city,zip and Pickup Location Exact
-- All Jobs Updated to 8667dd
-- All Jobs Updated to 066382: Add autoassign columns
-- All Jobs Updated to d8af44: Add jobs not assigned to partner, Include jobs with no invoice
-- All Jobs Updated to 0c24ed: Remove unnecessary company id check as covered by company.demo
-- All Jobs Updated to 87a55e: Remove formula for zip
-- All Jobs Updated to b84538: Rearrange joins
-- All Jobs Updated to 5c8849: Added deleted_at
SELECT merge_reports('All Jobs' , 'SuperCompany', $$select
  jobs.id,
  TO_CHAR((
    WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
        SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
        FROM jobs AS inner_jobs
        WHERE inner_jobs.id = jobs.id
      UNION
        SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
        FROM jobs AS inner_jobs, parents
        WHERE inner_jobs.id = parents.parent_id
    )
    SELECT adjusted_created_at
    FROM parents
    ORDER BY id ASC
    LIMIT 1
  ) AT TIME ZONE 'UTC' AT TIME ZONE 'America/Los_Angeles', 'MM/DD/YYYY') AS "Date (Pacific)",
  TO_CHAR((
    WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
        SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
        FROM jobs AS inner_jobs
        WHERE inner_jobs.id = jobs.id
      UNION
        SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
        FROM jobs AS inner_jobs, parents
        WHERE inner_jobs.id = parents.parent_id
    )
    SELECT adjusted_created_at
    FROM parents
    ORDER BY id ASC
    LIMIT 1
  ) AT TIME ZONE 'UTC' AT TIME ZONE 'America/Los_Angeles', 'HH24:MI') AS "Time (Pacific)",
  jobs.status,
  rescue_company.name as "Rescue",
  fleet_company.name as "Fleet",
  CONCAT(customer.first_name, ' ', customer.last_name) as "Customer",
  customer.phone as "Phone",
  pickup_location_type.name as "Location Type",
  pickup.address as "pickup location",
  case when (pickup.city is not null) then pickup.city else (case when original_pickup.city is not null then original_pickup.city else (provider_location.city) end) end as "City",
  case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
  case when (pickup.zip is not null) then pickup.zip else (case when original_pickup.zip is not null then original_pickup.zip else provider_location.zip end) end as "Zip",
  case when (pickup.state is null) and (original_pickup.state is null) then false else true end as "Pickup Location Exact",
  CONCAT(pickup.lat, ',', pickup.lng) as "Pickup",
  CONCAT(dropoff.lat, ',', dropoff.lng) as "Dropoff",
  service_codes.name as "Service",
  customer_types.name as "Payment Type",
  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
  round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",
  round((estimates.meters_ab+(CASE when estimates.drop_location_id is not null THEN estimates.meters_bc+estimates.meters_ca else estimates.meters_ba END))*0.000621371,1) as "P2P Miles",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ETA",
  round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ATA",
  round(
    cast((Extract(EPOCH FROM
      (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
      (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric)
    -
    cast((Extract(EPOCH FROM
      (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric)
  ,0) as "ATA - ETA",
  case when (jobs.track_technician_sent_at is not null) then 1 else 0 end as "Tracking Link Sent",
   (select array_agg(body) as sms_replies  from reviews left join twilio_replies on reviews.id=twilio_replies.target_id and twilio_replies.target_type='Review' where reviews.job_id=jobs.id group by reviews.job_id),
   invoicing_ledger_items.state,
  CAST(
    (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
     from invoicing_line_items
     where invoicing_ledger_items.id=invoicing_line_items.ledger_item_id
       and invoicing_line_items.deleted_at is null
     group by invoicing_ledger_items.id)
  AS DECIMAL(14, 2)) as "Invoiced",
   jobs.partner_live,
   jobs.partner_open,
   case when (jobs.partner_live and jobs.partner_open) then 1 else 0 end as "Live and Open",
   jobs.platform,
   vehicles.vin,
   jobs.scheduled_for,
   jobs.accident,
   symptoms.name as "Symptom",
  CONCAT_WS(' ',fleet_dispatchers.first_name, fleet_dispatchers.last_name) as "Fleet Dispatcher",
  CONCAT_WS(' ',swoop_dispatchers.first_name, swoop_dispatchers.last_name) as "Swoop Dispatcher",
  CONCAT_WS(' ',partner_dispatchers.first_name, partner_dispatchers.last_name) as "Partner Dispatcher",
  US_time(adjusted_ajs(jobs.id,9), 'America/Los_Angeles') AS "Created",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=10 and job_id=jobs.id order by id desc limit 1) as assigned_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "Assigned (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=11 and job_id=jobs.id order by id desc limit 1) as accepted_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "Accepted (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=2 and job_id=jobs.id order by id desc limit 1) as dispatch_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "Dispatched (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=3 and job_id=jobs.id order by id desc limit 1) as enroute_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "En Route (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "On Site (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=7 and job_id=jobs.id order by id desc limit 1) as completed_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "Completed (Pacific)",
  to_char(((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=11 and job_id=jobs.id order by id desc limit 1) as accepted_ajs) - jobs.created_at),'HH24:MI:SS') as "Digital Dispatch Time",

to_char(((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=3 and job_id=jobs.id order by id desc limit 1) as accepted_ajs) - jobs.created_at),'HH24:MI:SS') as "Digital En Route",

to_char(((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from audit_job_statuses where deleted_at is null and job_status_id=7 and job_id=jobs.id order by id desc limit 1) -
  (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from audit_job_statuses where deleted_at is null and job_status_id=3 and job_id=jobs.id order by id desc limit 1)),'HH24:MI') as "enroute->completed",

TO_CHAR((
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1) -
    (
      WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs
          WHERE inner_jobs.id = jobs.id
        UNION
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs, parents
          WHERE inner_jobs.id = parents.parent_id
      )
      SELECT adjusted_created_at
      FROM parents
      ORDER BY id ASC
      LIMIT 1
    )
  ),'HH24:MI:SS') as "Handle Time",
  /* Track Driver Funnel */
  to_char(track_driver_audit_sms.created_at   at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Text Sent",
  to_char(track_driver_audit_sms.clicked_dttm at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Link Clicked",
  null as "Track Driver Page Viewed",
  null as "Track Driver Page Session",

  reviews.rating as "5 Star Review",
  reviews.nps_question1 as "NPS Informed ",
  reviews.nps_question2 as "NPS ETA",
  reviews.nps_question3 as "NPS Recommend",
  reviews.nps_feedback as "NPS: Comments",
  jobs.policy_number as "Policy Number",
  jobs.ref_number as "Reference Number",
  customers.member_number as "Member Number",
  jobs.notes,
  jobs.fleet_notes,
  job_reject_reasons.text as "Reject Reason",
  jobs.reject_reason_info as "Reject Comment",
  rescue_vehicle.name as "Rescue Vehicle Name",
  case when (auto_assign.status = 'assigned') then true else false end as "Auto Assigned",
  auto_assign.auto_dispatched as "Dedicated Dispatched",
  jobs.deleted_at as "Deleted At",
  TO_CHAR(audit_sms.clicked_dttm AT TIME ZONE 'UTC' AT TIME ZONE 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') AS "Link Clicked"
from jobs
  left join companies as rescue_company on rescue_company.id=jobs.rescue_company_id
  left join companies as fleet_company on fleet_company.id=jobs.fleet_company_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join location_types as pickup_location_type on pickup_location_type.id = pickup.location_type_id
  left join locations as dropoff on dropoff.id=jobs.drop_location_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join estimates on estimates.id=jobs.estimate_id
  left join drives on jobs.driver_id=drives.id
  left join vehicles on drives.vehicle_id=vehicles.id
  left join users as fleet_dispatchers on jobs.fleet_dispatcher_id = fleet_dispatchers.id
  left join users as swoop_dispatchers on jobs.root_dispatcher_id = swoop_dispatchers.id
  left join users as partner_dispatchers on jobs.partner_dispatcher_id = partner_dispatchers.id
  left join symptoms on jobs.symptom_id=symptoms.id
  left join users as customer on customer.id=drives.user_id
  left join customers on customer.customer_id=customers.id
  left join customer_types on customer_types.id=jobs.customer_type_id
  left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
  left join locations provider_location on rescue_company.location_id=provider_location.id
  left join job_reject_reasons on jobs.reject_reason_id=job_reject_reasons.id
  left join vehicles as rescue_vehicle on jobs.rescue_vehicle_id = rescue_vehicle.id
  /* the following joins have the potential to cause duplicates */
  left join audit_sms as track_driver_audit_sms on track_driver_audit_sms.job_id = jobs.id and track_driver_audit_sms.type='EtaAuditSms'
  left join reviews on jobs.id=reviews.job_id
  left join invoicing_ledger_items on (jobs.id=invoicing_ledger_items.job_id and invoicing_ledger_items.sender_id=jobs.rescue_company_id and invoicing_ledger_items.deleted_at is null and invoicing_ledger_items.type='Invoice')
  left join auto_assign_assignments as auto_assign on jobs.id=auto_assign.job_id
  left join jobs parent ON jobs.parent_id = parent.id
  LEFT JOIN audit_sms ON audit_sms.job_id = jobs.id AND audit_sms.type = 'Sms::BrandedReviewLink'
where
   jobs.status != 'Reassigned' and
   (
    reviews.id = (SELECT MAX(id) FROM reviews WHERE reviews.job_id = jobs.id) or
    reviews.id is NULL
   ) and
   (rescue_company.demo is not true or jobs.rescue_company_id is null) and
   fleet_company.demo is not true
   and jobs.created_at >= ?
   and jobs.created_at < ?
   {{rescue_company}}
   {{fleet_company}}
   {{service}}
order by id desc;
$$,
'{"start_date":"2000-01-01","end_date":"2050-09-01"}',
'["start_date","end_date", "rescue_company", "fleet_company", "service"]',
'{"rescue_company":"and jobs.rescue_company_id=?", "fleet_company":"and jobs.fleet_company_id=?", "service":"and jobs.service_code_id=?"}',
'{}',
'{  "start_date":{"type":"start_date"},  "end_date":{"type":"end_date"},  "rescue_company":{"type":"partner"},  "fleet_company":{"type":"fleet"},  "service":{"type":"service"}}');




/* PDW: broken? */
-- Fleet Managed Swoop Jobs Report (Completed, Released) Report
-- Fleet Managed Swoop Jobs Report (Completed, Released, Canceled, GOA) Updated to 5ad17c: removing duplicate invoice deleted at check
SELECT merge_reports('Fleet Managed Swoop Jobs Report (Completed, Released, Canceled, GOA)' , 'SuperCompany', $$select
  jobs.id,
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY') as "Date (Pacific)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','HH24:MI') as "Time (Pacific)",
  rescue_company.name as "Rescue",
  fleet_company.name as "Fleet",
  pickup_location_type.name as "Location Type",
  pickup.address as "pickup location",
  CONCAT(pickup.lat, ',', pickup.lng) as "Pickup",
  CONCAT(dropoff.lat, ',', dropoff.lng) as "Dropoff",
  service_codes.name as "Service",
  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
  round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",
    round((estimates.meters_ab+(CASE when estimates.drop_location_id is not null THEN estimates.meters_bc+estimates.meters_ca else estimates.meters_ba END))*0.000621371,1) as "P2P Miles",
  round(cast((Extract(EPOCH FROM
  (select last_set_dttm from audit_job_statuses where job_status_id=7 and audit_job_statuses.job_id=jobs.id order by id desc limit 1) -
  (select last_set_dttm from audit_job_statuses where job_status_id=9 and audit_job_statuses.job_id=jobs.id order by id desc  limit 1)
)/60) as numeric),0) as "Created To Completed",
   round(cast((Extract(EPOCH FROM
  time_of_arrivals.time - (select last_set_dttm from audit_job_statuses where job_status_id=10 and audit_job_statuses.job_id=jobs.id order by id desc limit 1)
)/60) as numeric),0) as "ETA",
   round(cast((Extract(EPOCH FROM
  (select last_set_dttm from audit_job_statuses where job_status_id=4 and audit_job_statuses.job_id=jobs.id order by id desc limit 1) -
  (select last_set_dttm from audit_job_statuses where job_status_id=10 and audit_job_statuses.job_id=jobs.id order by id desc limit 1)
)/60) as numeric),0) as "ATA",
   jobs.status,

   to_char(audit_job_statuses.created_at at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI'),

   reviews.rating,
   (select array_agg(body) as sms_replies  from reviews left join twilio_replies on reviews.id=twilio_replies.target_id and twilio_replies.target_type='Review' where reviews.job_id=jobs.id group by reviews.job_id),
   invoicing_ledger_items.state,
   CAST(
           (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
      from invoicing_line_items
      where invoicing_ledger_items.id=invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
   group by invoicing_ledger_items.id)

          AS DECIMAL(14, 2)) as "Invoiced",

   fleet_invoice.state,
   CAST(
           (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
      from invoicing_line_items
      where fleet_invoice.id=invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
   group by fleet_invoice.id)
          AS DECIMAL(14, 2)) as "Fleet Invoiced",
   partner_live,
   partner_open,
   vehicles.vin,
   jobs.scheduled_for,
   symptoms.name as "Symptom",
  CONCAT_WS(' ',fleet_dispatchers.first_name, fleet_dispatchers.last_name) as "Fleet Dispatcher",
  CONCAT_WS(' ',partner_dispatchers.first_name, partner_dispatchers.last_name) as "Partner Dispatcher",
  to_char(((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) -
  (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc limit 1)),'HH24:MI') as "enroute->completed",
  notes,
  fleet_notes
from jobs

  left join audit_job_statuses
    ON audit_job_statuses.job_id = jobs.id
      AND audit_job_statuses.job_status_id IN (SELECT id FROM job_statuses WHERE name IN ('Completed', 'GOA', 'Released', 'Canceled')) /* PDW: this looks wrong */


  left join companies as rescue_company on rescue_company.id=jobs.rescue_company_id
  left join companies as fleet_company on fleet_company.id=jobs.fleet_company_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join location_types as pickup_location_type on pickup_location_type.id = pickup.location_type_id
  left join locations as dropoff on dropoff.id=jobs.drop_location_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join estimates on estimates.id=jobs.estimate_id
  left join time_of_arrivals on time_of_arrivals.job_id=jobs.id
  left join reviews on jobs.id = reviews.job_id

  left join invoicing_ledger_items
    ON jobs.id=invoicing_ledger_items.job_id
    AND invoicing_ledger_items.recipient_id =(SELECT id FROM companies WHERE name = 'Swoop' LIMIT 1)
    AND invoicing_ledger_items.type='Invoice'

  left join invoicing_ledger_items fleet_invoice
    ON jobs.id=fleet_invoice.job_id
       AND fleet_invoice.deleted_at IS NULL
       AND fleet_invoice.sender_id =(SELECT id FROM companies WHERE name = 'Swoop' LIMIT 1)
       AND fleet_invoice.type='Invoice'

  left join drives on jobs.driver_id=drives.id
  left join vehicles on drives.vehicle_id=vehicles.id
  left join users as fleet_dispatchers on jobs.fleet_dispatcher_id = fleet_dispatchers.id
  left join users as partner_dispatchers on jobs.partner_dispatcher_id = partner_dispatchers.id
  left join symptoms on jobs.symptom_id = symptoms.id
  where
   jobs.status in ('Completed', 'Released', 'Canceled', 'GOA') and
   jobs.type = 'FleetManagedJob' and
   (
         time_of_arrivals.id= (SELECT MAX(id) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type = 0) or
         (
           (SELECT MAX(id) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type = 0) is NULL and
           time_of_arrivals.id= (SELECT MAX(id) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id)
         ) or
         time_of_arrivals.id is NULL
   ) and
   (
     reviews.id = (SELECT MAX(id) FROM reviews WHERE reviews.job_id = jobs.id) or
     reviews.id is NULL
   ) and
   invoicing_ledger_items.deleted_at is null and
   fleet_company_id not in (1,6,41,45,46,47,48,49,472,473,474) and
   rescue_company_id not in (1,6,41,45,46,47,48,49,472,473,474) and
   rescue_company.demo is not true and
   fleet_company.demo is not true and
   jobs.created_at >= ? and
   jobs.created_at < ?
   {{rescue_company}}
   {{fleet_company}}
   {{service}}
order by id desc;$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date", "rescue_company", "fleet_company", "service"]',
'{"rescue_company":"and rescue_company_id=?",  "fleet_company":"and fleet_company_id=?",  "service":"and service_code_id=?"}',
'{}',
'{   "start_date":{"type":"start_date"},   "end_date":{"type":"end_date"},   "rescue_company":{"type":"partner"},   "fleet_company":{"type":"fleet"},   "service":{"type":"service"} } ');


-- Fleet Managed Field Responses Report (Sameer,Cody,Tim)
SELECT merge_reports('Fleet Managed Field Responses' , 'SuperCompany', $$
          select jobs.id as "ID",
                 to_char(jobs.created_at  at time zone 'UTC' at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Created time (LA)",
                 fleet_company.name as "Fleet Company",
                 service.name as "Service",
                 (select array_agg(questions.question || ': ' || answers.answer) from question_results
                    left join questions on questions.id=question_results.question_id
                    left join answers on answers.id=question_results.answer_id
                    where question_results.job_id = jobs.id
                    group by job_id) as "All Question Answers",
                 symptoms.name as "Symptom",
                 jobs.policy_number as "Policy Number",
                 (CASE WHEN policy_location.street IS NOT NULL THEN CONCAT(policy_location.street, ', ',policy_location.city, ', ', policy_location.state, ', ', policy_location.zip) ELSE null END) as "Policy Address",
                 customer_type.name as "Payment Type"
          from jobs
            left join symptoms on jobs.symptom_id=symptoms.id
            left join companies  as fleet_company on fleet_company.id=jobs.fleet_company_id
            left join service_codes as service on jobs.service_code_id=service.id
            left join locations as policy_location on policy_location.id=jobs.policy_location_id
            left join customer_types as customer_type on jobs.customer_type_id=customer_type.id
          where
            jobs.type='FleetManagedJob' and
            jobs.created_at >= ? and
            jobs.created_at < ?
           order by jobs.created_at desc$$,
'{"start_date":"2000-01-01","end_date":"2050-09-01"}',
'["start_date","end_date"]',
'{}',
'{}',
'{"start_date":{"type":"start_date"},"end_date":{"type":"end_date"}}');


-- Swoop Partner Invoice Report
-- Swoop Partner Invoice Updated to a39313: Fix Fleet name
-- Swoop Partner Invoice Updated to a82570: Add sender_id as filter
SELECT merge_reports('Swoop Partner Invoice' , 'SuperCompany', $$
SELECT
  *,
  "Invoice Amount"-"Payments" as "Balance Due"
FROM
(
 SELECT
 concat('SW', job.id) as "Invoice Number",
 to_char(job.created_at  at time zone 'UTC' at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Date/Time",

 service.name as "Service",
 job.status as "Status",

 fleet.name   as "Fleet",
 partner.name as "Partner",

 '' as "Payment Type",

  CAST((select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
     from invoicing_line_items
         where inv.id=invoicing_line_items.ledger_item_id
         and invoicing_line_items.deleted_at is null
       group by inv.id)
     AS DECIMAL(14, 2)) as "Invoice Amount",

 round(estimate.meters_ab*0.000621371,1) as "EN Route Miles",
 round(estimate.meters_bc*0.000621371,1) as "Towed Miles",
 round((CASE when estimate.drop_location_id is not null THEN estimate.meters_ca else estimate.meters_ba END)*0.000621371,1) as "Return Miles",

 inv.id as "Invoice Id",

 CAST (
 (SELECT -1*COALESCE(SUM(total_amount), 0) AS "Payments"
  FROM invoicing_ledger_items AS "payments_or_credits"
  WHERE payments_or_credits.invoice_id = inv.id
  AND payments_or_credits.deleted_at IS NULL
  AND payments_or_credits.type IN ('Payment', 'Discount', 'Refund', 'WriteOff'))
 AS DECIMAL(14, 2))

 FROM invoicing_ledger_items inv

 LEFT JOIN jobs    job           ON job.id        = inv.job_id
 LEFT JOIN drives  cdrive        ON job.driver_id = cdrive.id
 LEFT JOIN users   customer      ON customer.id   = cdrive.user_id
 LEFT JOIN companies  partner    ON inv.sender_id = partner.id
 LEFT JOIN companies  fleet      ON job.fleet_company_id = fleet.id
 LEFT JOIN vehicles vehicle      ON vehicle.id    = cdrive.vehicle_id
 LEFT JOIN locations policy_addr ON job.service_location_id = policy_addr.id
 LEFT JOIN service_codes service ON job.service_code_id     = service.id
 LEFT JOIN estimates   estimate  ON job.id = estimate.job_id

 LEFT JOIN rescue_providers AS provider
  ON    provider.site_id = job.site_id
    AND provider.company_id  = (Select id from companies where name='Swoop')
    AND provider.deleted_at IS NULL

 LEFT JOIN companies  company ON company.id = job.rescue_company_id

 WHERE inv.type IN ('Invoice')
   AND job.type = 'FleetManagedJob'
   AND inv.created_at BETWEEN ? AND ?
   AND inv.recipient_company_id =  ?
   AND inv.state = ?
   {{provider_id}}
   {{sender_id}}
) as t1
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01", "state" : "swoop_approved_partner" }',
'["start_date", "end_date", "recipient_company_id", "state", "provider_id","sender_id"]',
'{"provider_id":"and provider.id=?", "sender_id":"and sender_id=?"}',
'{ "recipient_company_id": "api_company.id" }',
'{}');


-- Swoop Fleet Invoice Report
SELECT merge_reports('Swoop Fleet Invoice' , 'SuperCompany', $$
  SELECT
    invoice.job_id AS "Job Id",
    invoice.id AS "Invoice Id",
    to_char((CASE WHEN (job.scheduled_for is not null) THEN job.scheduled_for ELSE job.created_at END) AT TIME ZONE (COALESCE((SELECT timezone FROM users WHERE id = ?), 'America/Los_Angeles')), 'MM/DD/YY') as "Service Date",
    (to_char(
      (CASE WHEN (job.scheduled_for IS NOT null)
        THEN job.scheduled_for
        ELSE job.created_at END
      ) AT TIME ZONE (COALESCE((SELECT timezone FROM users WHERE id = ?), 'America/Los_Angeles')), 'HH24:MI')) || ' (' || (COALESCE((SELECT timezone FROM users WHERE id = ?), 'America/Los_Angeles')) || ')' AS "Time",
    client_company.name AS "Company",
    partner_company.name AS "Partner",
    service_location.address AS "Pickup Address",
    drop_location.address AS "Drop Off Address",
    job.policy_number AS "Policy Number",
    job.ref_number AS "Reference Number",
    customers.member_number as "Member Number",
    CONCAT_WS(' ', vehicle.year, vehicle.make, vehicle.model) AS "Vehicle Info",
    service_code.name AS "Service",
    job.status AS "Status",
    customer_type.name AS "Payment Type",
    CAST(invoice.total_amount AS DECIMAL(14, 2)) AS "Invoice Amount"
  FROM invoicing_ledger_items invoice
  LEFT JOIN jobs job ON job.id = invoice.job_id
  LEFT JOIN companies client_company ON client_company.id = invoice.recipient_company_id
  LEFT JOIN companies partner_company ON partner_company.id = job.rescue_company_id
  LEFT JOIN locations service_location ON service_location.id = job.service_location_id
  LEFT JOIN locations drop_location ON drop_location.id = job.drop_location_id
  LEFT JOIN drives driver ON driver.id = job.driver_id
  LEFT JOIN users customer ON customer.id=driver.user_id
  LEFT JOIN customers ON customer.customer_id=customers.id
  LEFT JOIN vehicles vehicle ON vehicle.id = driver.vehicle_id
  LEFT JOIN service_codes service_code ON service_code.id = job.service_code_id
  LEFT JOIN customer_types customer_type ON customer_type.id = job.customer_type_id
  WHERE
    invoice.type IN ('Invoice')
    AND invoice.deleted_at IS NULL
    AND job.type = 'FleetManagedJob'
    AND invoice.created_at BETWEEN ? AND ?
    AND invoice.sender_id = ?
    {{invoice_ids}}
    {{client_company_id}}
$$,
'{"start_date":"2000-01-01", "end_date":"2050-09-01"}',
'["swoop_user_id", "swoop_user_id", "swoop_user_id", "start_date", "end_date", "sender_id", "invoice_ids", "client_company_id"]',
'{"invoice_ids":"AND invoice.id = ANY(string_to_array(?, '','')::int[])" , "client_company_id":"AND invoice.recipient_company_id =  ?"}',
'{"swoop_user_id":"api_user.id", "sender_id":"api_company.id"}',
'{}');


-- Tesla Invoice Report
-- Tesla Invoice Updated to cafbc3: check for deleted line items
-- Tesla Invoice Updated to 92b6dd: Fix rescue company filter ENG-2209
SELECT merge_reports('Tesla Invoice' , 'FleetCompany', $$select
invoices.id as "Invoice Id",
provider.vendor_code as "Vendor",
to_char((CASE WHEN (jobs.scheduled_for is not null) THEN jobs.scheduled_for ELSE jobs.created_at END) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YY') as "Doc Date",
to_char(now() AT TIME ZONE 'America/Los_Angeles','MM/DD/YY') as "Posting Date",
concat('SW', jobs.id) as "Invoice Number",

CAST((select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
  from invoicing_line_items
      where invoices.id=invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
    group by invoices.id)
  AS DECIMAL(14, 2)) as "Invoice Amount",
(CASE WHEN (select count(*) as cnt
                from companies_features
                left join features on companies_features.feature_id=features.id
                where companies_features.company_id=jobs.rescue_company_id
                and features.name='Canadian AP')>0
   THEN 'CAD' else 'USD' END) as "Currency",
-- purposley like this.
'' as "Tax Code",
'0.00' as "Tax Amount",
'' as "Text field",
'' as "Payment Reference",
(CASE
  WHEN departments.name = 'Roadside' AND customer_types.name = 'Body Shop' THEN '30150300'
  WHEN departments.name = 'Logistics' THEN '30167000'
  WHEN departments.name = 'Service Center' THEN '30150100'
  WHEN departments.name = 'Other' THEN '30153000'
  WHEN customer_types.name IN ('Warranty', 'Goodwill') THEN '30153000'
  WHEN customer_types.name = 'Body Shop' THEN '30150001'
  WHEN customer_types.name =  'P Card' THEN 'COGSSVR'
  ELSE '30150000'
END) AS "Cost Center",
(CASE
  WHEN customer_types.name in ('Warranty', 'P Card') THEN '224520'
  WHEN customer_types.name = 'Goodwill' THEN '640001'
  WHEN customer_types.name = 'Body Shop' THEN '685150'
  ELSE '610600'
END) AS "GL Account",
(CASE WHEN customer_types.name in ('Warranty', 'P Card', 'Body Shop') THEN 'S5200' ELSE 'C9000' END) as "SIO",
(CASE
  WHEN customer_types.name = 'Body Shop' THEN '787'
  WHEN sites.site_code is NULL THEN provider.location_code
  ELSE sites.site_code
END) as "Location",
vehicles.vin as "VIN Number",
'' as "Reservation Number",
(CASE
  WHEN customer_types.name = 'Body Shop' THEN 'AUBNW'
  ELSE departments.code
END) AS "Budget Code"
from
 invoicing_ledger_items as invoices
LEFT JOIN jobs ON invoices.job_id=jobs.id

LEFT JOIN rescue_providers AS provider
     ON provider.site_id = jobs.site_id
     AND provider.company_id = jobs.fleet_company_id
     AND provider.deleted_at is NULL
LEFT JOIN drives ON jobs.driver_id=drives.id
LEFT JOIN vehicles ON drives.vehicle_id=vehicles.id
LEFT JOIN departments ON jobs.department_id = departments.id
LEFT JOIN sites ON jobs.fleet_site_id = sites.id
LEFT JOIN customer_types ON customer_types.id = jobs.customer_type_id
WHERE
  invoices.state = 'fleet_approved'
  AND invoices.type='Invoice'
  AND invoices.deleted_at IS NULL
  AND invoices.created_at BETWEEN ? AND ?
  AND jobs.fleet_company_id = ?
  {{rescue_company}}
order by jobs.id
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date", "end_date", "recipient_company_id", "rescue_company"]',
'{"rescue_company":"and rescue_company_id=?"}',
'{ "recipient_company_id": "api_company.id" }',
'{}');

-- Enterprise Invoice Report
-- Enterprise Invoice Updated to cafbc3: check for deleted line items
-- Enterprise Invoice Inserted at 3a6c6f
-- Enterprise Invoice Updated to 4319b6: Fix rescue company filter ENG-2209
-- Enterprise Invoice Updated to 7c8616: Convert to requested TOW TEMPLATE.xlsx
-- Enterprise Invoice Updated to d48bce: Add accounting code
SELECT merge_reports('Enterprise Invoice' , 'FleetCompany', $$select
invoices.id as "Invoice Id",
partner.name as "TOW VENDOR",
jobs.id as "INVOICE NUMBER",
to_char((CASE WHEN (jobs.scheduled_for is not null) THEN jobs.scheduled_for ELSE jobs.created_at END) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YY') as "DATE OF TOW",
CONCAT_WS(' ', vehicles.year, vehicles.make, vehicles.model) as "VEHICLE DESCRIPTION",

jobs.unit_number as "UNIT",
vehicles.license as "LICENSE",
vehicles.vin as "VIN",
jobs.ref_number as "CLAIM",
CAST((select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
  from invoicing_line_items
      where invoices.id=invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
    group by invoices.id)
  AS DECIMAL(14, 2)) as "INVOICE TOTAL",
jobs.accounting_code as "ACCOUNTING CODE"
from
 invoicing_ledger_items as invoices
left join jobs on invoices.job_id=jobs.id
left join drives on jobs.driver_id=drives.id
left join vehicles on drives.vehicle_id=vehicles.id
left join companies AS partner on jobs.rescue_company_id = partner.id
left join customer_types as claims on jobs.customer_type_id = claims.id
where
  invoices.state = 'fleet_approved'
  AND invoices.type='Invoice'
  AND invoices.deleted_at is null
  AND invoices.created_at BETWEEN ? AND ?
  AND jobs.fleet_company_id = ?
  {{rescue_company}}
order by jobs.id
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date", "end_date", "recipient_company_id", "rescue_company"]',
'{"rescue_company":"and rescue_company_id=?"}',
'{ "recipient_company_id": "api_company.id" }',
'{}');

-- MTS Invoice Report
SELECT merge_reports('Motorcycle Towing Services, L.C. Invoice' , 'FleetCompany', $$SELECT
invoices.id as "Invoice Id",
jobs.id AS "Job Id",
to_char((CASE WHEN (jobs.scheduled_for IS NOT null) THEN jobs.scheduled_for ELSE jobs.created_at END) AT TIME zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YY') AS "Date",
service_codes.name AS "Service",
jobs.policy_number AS "Policy Number",
departments.name AS "Department",
CONCAT(customer.first_name, ' ', customer.last_name) AS "Customer Name",
pickup_location.address AS "Pickup Location",
rescue_company.name AS "Partner",
jobs.po_number AS "PO#",
jobs.ref_number AS "Reference Number",
customers.member_number AS "Member Number",
(CASE WHEN (dropoff_place.type = 'PoiSite' AND dropoff_place.name IS NOT NULL) THEN (dropoff_place.name || ' - ' || dropoff_location.address) ELSE dropoff_location.address END) AS "Drop Location",
CAST((SELECT sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) AS sum_amount
  FROM invoicing_line_items
      WHERE invoices.id=invoicing_line_items.ledger_item_id
        AND invoicing_line_items.deleted_at IS NULL
    GROUP BY invoices.id)
  AS DECIMAL(14, 2)) AS "Invoice Amount",
payment_type.name AS "Payment Type"
FROM
invoicing_ledger_items AS invoices
LEFT JOIN jobs ON invoices.job_id=jobs.id
LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
LEFT JOIN departments ON jobs.department_id = departments.id
LEFT JOIN locations AS pickup_location ON pickup_location.id=jobs.service_location_id
LEFT JOIN locations AS dropoff_location ON dropoff_location.id=jobs.drop_location_id
LEFT JOIN sites AS dropoff_place ON dropoff_place.id = dropoff_location.site_id
LEFT JOIN rescue_providers AS provider
  ON provider.site_id = jobs.site_id
  AND provider.company_id  = jobs.fleet_company_id
  AND provider.deleted_at IS NULL
LEFT JOIN companies AS rescue_company ON rescue_company.id = provider.provider_id
LEFT JOIN drives ON jobs.driver_id=drives.id
LEFT JOIN users AS customer ON customer.id = drives.user_id
LEFT JOIN customers ON customer.customer_id=customers.id
LEFT JOIN customer_types AS payment_type ON payment_type.id = jobs.customer_type_id
where
  invoices.state = 'fleet_approved'
  AND invoices.type='Invoice'
  AND invoices.deleted_at is null
  AND invoices.created_at BETWEEN ? AND ?
  AND jobs.fleet_company_id = ?
  {{rescue_company}}
order by jobs.id
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date", "end_date", "recipient_company_id", "rescue_company"]',
'{"rescue_company":"and rescue_company_id=?"}',
'{ "recipient_company_id": "api_company.id" }',
'{}');


-- 21st Invoice Report - this is based on 21st Century Billing File report
SELECT merge_reports('21st Century Invoice' , 'SuperCompany', $$SELECT
  '' AS "BatchCreated",
  'InboundPaymentRequestCCAS' AS "TransType",
  'CCAS' AS "ReceivedBy",
  pcc_claim.data->'table'->>'claimUnitId' AS "EntityID",
  '' AS "ReceivedDate",
  pcc_claim.data->'table'->>'claimUnitNumber' AS "ClaimUnitNumber",
  'Security Advantage Towing' AS "PaymentClass",
  CASE
    WHEN symptom.name = 'Accident' THEN 'ACCDT'
    WHEN symptom.name = 'Flat tire' THEN 'FLTSP'
    WHEN symptom.name = 'Out of fuel' THEN 'GAS'
    WHEN symptom.name = 'Locked out' THEN 'LCKSI'
    WHEN (symptom.name = 'Inoperable Problem' OR symptom.name = 'Specialty Vehicle/RV Disablement') THEN 'VINOP'
    WHEN symptom.name = 'Stuck' THEN 'WINCH'
    WHEN symptom.name = 'Would not Start' THEN 'WNTST'
  END AS "ProblemType",
  'N' AS "Accident",
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN NULL
        WHEN pcc_coverage.covered IS TRUE THEN 'Y'
    ELSE 'N'
  END AS "TowCoverageStatus", -- override CASE
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN 'Y'
    ELSE 'N'
  END AS "SupervisorApproval", -- override CASE
  '' AS "CCASBatch Number",
  '57.56' AS "Dispatch Fee",
  '0' AS "Excess Claim Amount",
  '1' AS "TaskNumber",
  LPAD(job.id::text, 10, '0') AS "CaseNumber", -- leading 0
  job.id AS "CCASPONumber",
  TO_CHAR((SELECT
      CASE
          WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
          ELSE last_set_dttm
      END
   FROM (
     SELECT last_set_dttm, adjusted_dttm
     FROM audit_job_statuses
     WHERE deleted_at IS NULL
     AND job_status_id = 9 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY') AS "ServiceStartDate", -- AJS created date
  CASE
    WHEN job.status = 'Completed' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 9 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'GOA' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 21 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'Canceled' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 8 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'Released' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 18 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     ELSE ''
     END AS "ServiceEndDate", -- AJS end date
  CASE
    WHEN job.status = 'goa' THEN 'GOA'
    WHEN job.status = 'canceled' THEN 'CAN'
    WHEN service.name = 'Tow' OR service.name = 'Tow if Jump Fails' OR service.name = 'Tow if Tire Change Fails' THEN 'TOW'
    WHEN service.name = 'Battery Jump' OR service.name = 'Tire Change' OR service.name = 'Fuel Delivery' OR service.name = 'Winch Out' THEN 'ROAD'
    WHEN service.name = 'Lock Out' THEN 'LOCK'
    WHEN service.name = 'Accident Tow' THEN 'ACCDT'
  END AS "TypeOfService",
  'Final Payment' AS "PaymentType",
  job.rescue_company_id AS "ServiceProviderID",
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN 'HP'
    ELSE NULL
  END AS "CoverageVerificationSource", -- override CASE
  'N' AS "Resubmitted",
  '21' AS "IGI",
  '' AS "VehicleType"
FROM invoicing_ledger_items invoice
JOIN jobs job ON job.id = invoice.job_id
JOIN companies fleet_company ON job.fleet_company_id = fleet_company.id
LEFT JOIN pcc_claims pcc_claim ON job.pcc_claim_id = pcc_claim.id
LEFT JOIN pcc_coverages pcc_coverage ON pcc_claim.pcc_coverage_id = pcc_coverage.id
LEFT JOIN service_codes service ON service.id = job.service_code_id
LEFT JOIN symptoms symptom ON symptom.id = job.symptom_id
WHERE fleet_company.name = '21st Century Insurance'
AND invoice.deleted_at IS NULL
AND invoice.type IN ('Invoice')
AND job.status IN ('Completed', 'Released', 'Canceled', 'GOA')
AND job.created_at >= ?
AND job.created_at < ?
{{invoice_ids}}
order by job.id desc $$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date","invoice_ids"]',
'{"invoice_ids":"AND invoice.id = ANY(string_to_array(?, '','')::int[])"}',
'{}',
'{"start_date":{"type":"start_date"}, "end_date":{"type":"end_date"}}');


-- Farmers Activity Report Report
-- Signed off PDW SB 2:27 10/28
-- Farmers Activity Report Updated to c358b6
-- Farmers Activity Report Updated to fd3e0f: filter out deleted jobs
SELECT merge_reports('Farmers Activity Report' , 'FleetCompany', $$
select
  DISTINCT(jobs.id),
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  pickup.address as "Full Address",

  -- Policy Addr
  jobs.policy_number   as "Policy Number",
  pcc_claim.claim_number AS "Claim Number",

  CONCAT_WS(' ',vehicles.make,vehicles.model,vehicles.year) as "Vehicle Info",
  service_codes.name as "Service Requested",
  jobs.status as "Status",


  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
  round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",

  ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) as "P2P Miles"
from jobs
  LEFT JOIN locations as pickup
    ON pickup.id=jobs.service_location_id
  LEFT JOIN service_codes
    ON service_codes.id=jobs.service_code_id
  LEFT JOIN drives
    ON jobs.driver_id=drives.id
  LEFT JOIN vehicles
    ON drives.vehicle_id=vehicles.id
  LEFT JOIN estimates
    ON estimates.id = jobs.estimate_id
  LEFT JOIN pcc_claims pcc_claim
    ON pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.status IN ('Completed', 'Released', 'Canceled', 'GOA') and
  jobs.fleet_company_id=(SELECT id FROM companies WHERE name = 'Farmers Insurance')
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
order by jobs.id desc;$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{} ',
'{   "start_date":{"type":"start_date"},   "end_date":{"type":"end_date"}} ');


-- 21st Century Activity Report
SELECT merge_reports('21st Century Activity Report' , 'FleetCompany', $$
select
  DISTINCT(jobs.id),
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  pickup.address as "Full Address",

  -- Policy Addr
  jobs.policy_number   as "Policy Number",
  pcc_claim.claim_number AS "Claim Number",

  CONCAT_WS(' ',vehicles.make,vehicles.model,vehicles.year) as "Vehicle Info",
  service_codes.name as "Service Requested",
  jobs.status as "Status",


  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
  round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",

  ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) as "P2P Miles"
from jobs
  LEFT JOIN locations as pickup
    ON pickup.id=jobs.service_location_id
  LEFT JOIN service_codes
    ON service_codes.id=jobs.service_code_id
  LEFT JOIN drives
    ON jobs.driver_id=drives.id
  LEFT JOIN vehicles
    ON drives.vehicle_id=vehicles.id
  LEFT JOIN estimates
    ON estimates.id = jobs.estimate_id
  LEFT JOIN pcc_claims pcc_claim
    ON pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.status IN ('Completed', 'Released', 'Canceled', 'GOA') and
  jobs.fleet_company_id=(SELECT id FROM companies WHERE name = '21st Century Insurance')
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
order by jobs.id desc;$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{} ',
'{   "start_date":{"type":"start_date"},   "end_date":{"type":"end_date"}} ');


SELECT merge_reports('Request Location Metrics', 'SuperCompany', $$
   SELECT jobs.id as "ID",

  to_char(jobs.created_at  at time zone 'UTC' at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Created At",

  fleet_company.name as "Creating Company",

  to_char(sms.created_at   at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "SMS Sent At",
  to_char(sms.clicked_dttm  at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Link Clicked",


  '' as "Rejected Location",
  '' as "10 Second Limit",

  to_char(command.created_at  at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Location Updated At",
  to_char(metrics.created_at  at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Page Viewed At"


  FROM jobs
  JOIN companies fleet_company
    ON fleet_company.id = jobs.fleet_company_id

  JOIN event_operations eo
     ON eo.name = 'update'

  JOIN audit_sms sms
    ON sms.job_id = jobs.id AND sms.type='LocationAuditSms'

  LEFT JOIN drives
    ON jobs.driver_id = drives.id

  LEFT JOIN users customer
     ON drives.user_id = customer.id

 LEFT JOIN commands command
    ON command.event_operation_id = eo.id AND command.user_id = customer.id

  LEFT JOIN metrics
    ON metrics.target_id = jobs.id
        AND metrics.target_type = 'Job'
        AND metrics.action      = 'viewed'
  {{company}}
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["company"]',
'{"company":"AND ? IN (fleet_company_id, rescue_company_id)"}',
'{}',
'{ "start_date":{"type":"start_date"},  "end_date":{"type":"end_date"}, "company":{"type":"company"} }');



SELECT merge_reports('Time spent creating job', 'SuperCompany', $$
   select to_char(metrics.created_at  at time zone 'UTC' at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Created At",
       companies.type,
       companies.name,
       metrics.value
from metrics
left join companies on companies.id=metrics.company_id
where
  action='complete'
  and label='job_form'
  and metrics.created_at >= ?
  and metrics.created_at < ?
  {{company_id}};
$$,
'{"start_date":"2000-01-01","end_date":"2050-09-01"}',
'["start_date","end_date", "company_id"]',
'{"company_id":"and company_id=?"}',
'{}',
'{ "start_date":{"type":"start_date"},  "end_date":{"type":"end_date"}, "company_id":{"type":"company"} }');



/* PDW: This report is depricated and partially merged into All Jobs */
SELECT merge_reports('Track Driver', 'SuperCompany', $$SELECT
  jobs.id as "Job ID",

  to_char(created_audit.created_at at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Job Created Date/Time",

  CASE
    WHEN rescue_company.id = fleet_company.id THEN ''
    ELSE fleet_company.name
  END as "Fleet Company",
  rescue_company.name as "Partner Company",

  to_char(audit_sms.created_at   at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Text Sent",
  to_char(audit_sms.clicked_dttm at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Link Clicked",
  to_char(metrics.created_at     at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Page Viewed",

  -- We dont have a way to track this at the moment so leave blank
  null as "Track Driver Page Session"
from jobs
  LEFT JOIN audit_sms
  ON audit_sms.job_id = jobs.id and audit_sms.type='EtaAuditSms'

LEFT JOIN metrics
  ON metrics.target_type = 'Job'
     AND metrics.target_id = audit_sms.job_id
     AND metrics.action    = 'viewed'
     AND metrics.label     = 'track_driver_page_view' /* PDW: this doesn't exist */

JOIN job_statuses created
 ON created.name = 'Created'

LEFT JOIN audit_job_statuses created_audit
 ON created.id = created_audit.job_status_id
    AND created_audit.job_id = audit_sms.job_id

JOIN companies as rescue_company ON rescue_company.id = jobs.rescue_company_id
JOIN companies as fleet_company  ON fleet_company.id   = jobs.fleet_company_id

where
  audit_sms.created_at >= ?
  and audit_sms.created_at < ?
  {{company_id}};
$$,
'{"start_date":"2000-01-01","end_date":"2050-09-01"}',
'["start_date","end_date", "company_id"]',
'{"company_id":"and metrics.company_id=?"}',
'{}',
'{ "start_date":{"type":"start_date"},  "end_date":{"type":"end_date"}, "company_id":{"type":"company"} }');


/* Fleet Managed Detail */
/* Old Digital Dispatch report */
-- Fleet Managed Detail Inserted at 52ed31
-- Fleet Managed Detail Updated to bf7511 - update to .in_house
SELECT merge_reports('Fleet Managed Detail', 'FleetCompany', $$
select
  jobs.id as "Job ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY') as "Date(Pacific)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','HH24:MI') as "Time(Pacific)",
  fleet_company.name as "Fleet Company",
  pickup.city as "City",
  case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
  trim(both ' ' from concat(provider.name, ' ', sites.name)) as "Towing Company",
  to_char(((SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
 - jobs.created_at),'HH24:MI:SS') as "Handle Time",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ETA",
  round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ATA",
  service_codes.name as "Service",
  jobs.status,
  CAST(
    (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
     from invoicing_line_items
     where invoicing_ledger_items.id=invoicing_line_items.ledger_item_id
       and invoicing_line_items.deleted_at is null
     group by invoicing_ledger_items.id)
  AS DECIMAL(14, 2)) as "Invoiced",
  reviews.rating as "5 Star",
  reviews.nps_question1 as "Customer review: Q1",
  reviews.nps_question2 as "Customer review: Q2",
  reviews.nps_question3 as "Customer review: Q3",
  reviews.nps_feedback as "Customer review comments"
from jobs
  left join reviews on jobs.id=reviews.job_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join companies as provider on provider.id=jobs.rescue_company_id
  left join companies as fleet_company on fleet_company.id=jobs.fleet_company_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
  left join locations provider_location on provider.location_id=provider_location.id
  left join invoicing_ledger_items on (jobs.id=invoicing_ledger_items.job_id and invoicing_ledger_items.sender_id=jobs.rescue_company_id AND invoicing_ledger_items.type='Invoice')
  left join sites on sites.company_id=provider.id and sites.name is not null
where
  invoicing_ledger_items.deleted_at is null and
  jobs.status in ('Completed','Canceled','GOA','Released') and
  fleet_company.type='FleetCompany' and
  fleet_company.in_house is null and
  fleet_company_id not in (1,6,9,41,45,46,47,48,49,472,473,474)
and
  jobs.created_at >= ? and
  jobs.created_at < ?
order by jobs.id desc;
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{}',
'{"start_date":{"type":"start_date"},   "end_date":{"type":"end_date"}} ');


-- Storage Report Updated to bb1738
SELECT merge_reports('Storage Report', 'RescueCompany', $$
  SELECT
    jobs.id as "Swoop ID",
    to_char(inventory_items.stored_at  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')  as "Date",
    FLOOR(invoicing_line_items.quantity) as "Days",
    sites.name as "Site",
    storage_types.name as "Type",
    (CASE WHEN jobs.type='RescueJob' THEN COALESCE(account_company.name, accounts.name, 'Cash Call') ELSE COALESCE(fleet_company.name, accounts.name) END) as "Account",
    vehicles.year as "Year",
    vehicles.make as "Make",
    vehicles.model as "Model",
    vehicles.color as "Color",
    vehicles.license as "License",
    vehicles.vin as "VIN"

    FROM jobs
    LEFT JOIN inventory_items
      ON jobs.id = inventory_items.job_id AND inventory_items.item_type = 'Vehicle'
    LEFT JOIN sites
      ON inventory_items.site_id = sites.id
    LEFT JOIN storage_types
      ON storage_types.id = jobs.storage_type_id
    LEFT JOIN accounts
      ON accounts.id = jobs.account_id
    LEFT JOIN companies fleet_company
      ON jobs.fleet_company_id = fleet_company.id
    LEFT JOIN companies account_company
      ON accounts.client_company_id = account_company.id
    LEFT JOIN drives
      ON jobs.driver_id = drives.id
    LEFT JOIN vehicles
      ON vehicles.id = drives.vehicle_id
    LEFT JOIN invoicing_ledger_items
      ON invoicing_ledger_items.job_id = jobs.id
        AND invoicing_ledger_items.sender_id = jobs.rescue_company_id
    LEFT JOIN invoicing_line_items
      ON invoicing_line_items.ledger_item_id = invoicing_ledger_items.id
        AND invoicing_line_items.description = 'Storage'
        AND invoicing_line_items.deleted_at IS NULL
    LEFT JOIN users
      ON users.id = ?
    WHERE inventory_items.deleted_at IS NULL
      AND (invoicing_ledger_items.type is null or invoicing_ledger_items.type = 'Invoice')
      AND invoicing_ledger_items.deleted_at IS NULL
      AND jobs.deleted_at IS NULL
      AND jobs.status = 'Stored'
      AND inventory_items.item_type='Vehicle'
      AND inventory_items.released_at IS NULL
      AND jobs.rescue_company_id = ?
      {{account}}
      {{type}}
      {{site}}
    ORDER BY jobs.id ASC
$$,
'{"api_user.id": "api_user.id"}',
'["api_user.id", "api_company.id", "account", "type", "site"]',
'{ "account": "and jobs.account_id=?", "type": "AND jobs.storage_type_id = ?", "site": "AND inventory_items.site_id = ?"}',
'{"api_user.id": "api_user.id", "api_company.id": "api_company.id"}',
'{"account":{"type":"account"}, "type":{"type":"eval", "method":"storage_types"}, "site": { "type":"eval", "method":"live_sites" }}');

SELECT add_pdf_to_report('Storage Report');

/* Tesla Provider rejection */
-- Tesla Provider Rejection Updated to 1f88f0: Approved SB,PDW 11/12/2016
SELECT merge_reports('Tesla Provider Rejection', 'FleetCompany', $$
select
  jobs.id as "Swoop ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY') as "Date (Pacific)",
  trim(both ' ' from concat(provider.name, ' ', sites.name)) as "Partner",
  service_codes.name as "Service",
  jobs.status as "Status",
  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  CONCAT(pickup.lat, ',', pickup.lng) as "Lat/Lng Pickup",
  job_reject_reasons.text as "Reject Reason",
  reject_reason_info as "Reject Comment"
from jobs
  left join companies provider on jobs.rescue_company_id=provider.id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join sites on sites.company_id=provider.id and sites.name is not null
  left join estimates on estimates.id=jobs.estimate_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join job_reject_reasons on jobs.reject_reason_id=job_reject_reasons.id
where
  status='Rejected'
  and jobs.fleet_company_id=(SELECT id FROM companies WHERE name = 'Tesla') and
  jobs.created_at >= ? and
  jobs.created_at < ?
  {{rescue_company}}
order by jobs.id DESC;
$$,
'{ "start_date" : "2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date", "rescue_company"]',
'{"rescue_company":"and rescue_company_id=?"}',
'{}',
'{"start_date":{"type":"start_date"}, "end_date":{"type":"end_date"}, "rescue_company":{"type":"partner"}}');



drop view if exists fleet_all_data_view;
create view "fleet_all_data_view" as
select
  jobs.id as "Job ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY') as "Date (Pacific)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','HH24:MI') as "Time (Pacific)",
  partner.name as "Partner",
  CONCAT(customer.first_name, ' ', customer.last_name) as "Customer",
  customer.phone as "Phone",
  pickup_location_type.name as "Location Type",
  vehicles.make  as "Make",
  vehicles.model as "Model",
  pickup.address as "pickup location",
  pickup.zip,
  case when (pickup.city is not null) then pickup.city else (case when original_pickup.city is not null then original_pickup.city else (provider_location.city) end) end as "City",
  case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
  case when (pickup.zip is not null) then pickup.zip else (case when original_pickup.zip is not null then original_pickup.zip else provider_location.zip end) end as "Zip",
  CONCAT(pickup.lat, ',', pickup.lng) as "Pickup",
  CONCAT(dropoff.lat, ',', dropoff.lng) as "Dropoff",
  service_codes.name as "Service",
  jobs.scheduled_for as "Scheduled",
  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
  round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",
  round((estimates.meters_ab+(CASE when estimates.drop_location_id is not null THEN estimates.meters_bc+estimates.meters_ca else estimates.meters_ba END))*0.000621371,1) as "P2P Miles",


round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3,4) order by id asc limit 1)
)/60) as numeric),0) as "ETA",

round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3,4) order by id asc limit 1)
)/60) as numeric),0) as "ATA",

  round(
    cast((Extract(EPOCH FROM
      (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
      (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3,4) order by id asc limit 1)
    )/60) as numeric)
    -
    cast((Extract(EPOCH FROM
      (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3,4) order by id asc limit 1)
    )/60) as numeric)
  ,0) as "ATA - ETA",

TO_CHAR((
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0,3,4) ORDER BY id ASC LIMIT 1) -
    (
      WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs
          WHERE inner_jobs.id = jobs.id
        UNION
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs, parents
          WHERE inner_jobs.id = parents.parent_id
      )
      SELECT adjusted_created_at
      FROM parents
      ORDER BY id ASC
      LIMIT 1
    )
  ),'HH24:MI:SS') as "Handle Time",

    jobs.status as "Status",
    customer_types.name AS "Payment Type",
  CAST(
    (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
     from invoicing_line_items
     where invoicing_ledger_items.id=invoicing_line_items.ledger_item_id
       and invoicing_line_items.deleted_at is null
     group by invoicing_ledger_items.id)
  AS DECIMAL(14, 2)) as "Invoiced",
   jobs.partner_live as "Partner Live",
   jobs.partner_open as "Partner Open",
   case when (jobs.partner_live and jobs.partner_open) then true else null end as "Partner Live and Open",
   jobs.platform as "Platform",

   invoicing_ledger_items.state as "Invoice Status",
   vehicles.vin,
   jobs.unit_number as "Unit Number",
   jobs.ref_number as "Claim Number",
   symptoms.name as "Symptom",

  case when (jobs.drop_location_id is not null) then
    (estimates.seconds_ab +
    25*60 +
    estimates.seconds_bc +
    25*60 +
    estimates.seconds_ca)/60
  else
    (estimates.seconds_ab +
    25*60 +
    estimates.seconds_ba)/60
  end as "P2P Job Time Estimate",
  case when (jobs.drop_location_id is not null) then
    (estimates.seconds_ab +
    25*60 +
    estimates.seconds_bc +
    25*60)/60
  else
    (estimates.seconds_ab +
    25*60)/60
  end as "Port-to-Complete Job Time Estimate",
  departments.name as "Department",
  CONCAT_WS(' ',fleet_dispatchers.first_name, fleet_dispatchers.last_name) as "Fleet Dispatcher",
  CONCAT_WS(' ',partner_dispatchers.first_name, partner_dispatchers.last_name) as "Partner Dispatcher",

  US_time(adjusted_ajs(jobs.id,9), 'America/Los_Angeles') AS "Created",
  US_time(adjusted_ajs(jobs.id,10), 'America/Los_Angeles') AS "Assigned",
  US_time(adjusted_ajs(jobs.id,11), 'America/Los_Angeles') AS "Accepted",
  US_time(adjusted_ajs(jobs.id,2), 'America/Los_Angeles') AS "Dispatched",
  US_time(adjusted_ajs(jobs.id,3), 'America/Los_Angeles') AS "En Route",
  US_time(adjusted_ajs(jobs.id,4), 'America/Los_Angeles') AS "On Site",
  US_time(adjusted_ajs(jobs.id,5), 'America/Los_Angeles') AS "Towing",
  US_time(adjusted_ajs(jobs.id,6), 'America/Los_Angeles') AS "Tow Destination",
  US_time(adjusted_ajs(jobs.id,7), 'America/Los_Angeles') AS "Completed",
  US_time(adjusted_ajs(jobs.id,21), 'America/Los_Angeles') AS "GOA",
  US_time(adjusted_ajs(jobs.id,18), 'America/Los_Angeles') AS "Released",
  US_time(adjusted_ajs(jobs.id,8), 'America/Los_Angeles') AS "Canceled",

  hms(adjusted_ajs(jobs.id,11)-adjusted_ajs(jobs.id,9))  as "Digital Dispatch Time",
  hms(adjusted_ajs(jobs.id,3)-adjusted_ajs(jobs.id,9))  as "Digital En Route",
  hms(adjusted_ajs(jobs.id,7)-adjusted_ajs(jobs.id,3))  as "En Route -> Completed",

  jobs.notes as "Job Details",
  jobs.fleet_notes as "Fleet Notes",
  jobs.driver_notes as "Invoice Notes",
  job_reject_reasons.text as "Reject Reason",
  jobs.reject_reason_info as "Reject Comment",
  rescue_vehicle.name as "Rescue Vehicle Name",

/* These are for filtering on the actual reports */
  jobs.created_at,
  jobs.fleet_company_id,
  jobs.rescue_company_id

FROM   jobs
  left join estimates on jobs.estimate_id=estimates.id
  left join companies partner on partner.id = jobs.rescue_company_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join locations as dropoff on dropoff.id=jobs.drop_location_id
  left join location_types as pickup_location_type on pickup_location_type.id = pickup.location_type_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join symptoms on jobs.symptom_id=symptoms.id
  left join invoicing_ledger_items on (jobs.id=invoicing_ledger_items.job_id and invoicing_ledger_items.sender_id=jobs.rescue_company_id and invoicing_ledger_items.deleted_at is null AND invoicing_ledger_items.type='Invoice')
  left join drives on jobs.driver_id=drives.id
  left join vehicles on drives.vehicle_id=vehicles.id
  left join departments on jobs.department_id=departments.id
  left join users as customer on customer.id=drives.user_id
  left join customer_types on customer_types.id=jobs.customer_type_id
  left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
  left join locations provider_location on partner.location_id=provider_location.id
  left join users as fleet_dispatchers on jobs.fleet_dispatcher_id = fleet_dispatchers.id
  left join users as partner_dispatchers on jobs.partner_dispatcher_id = partner_dispatchers.id
  left join job_reject_reasons on jobs.reject_reason_id=job_reject_reasons.id
  left join vehicles as rescue_vehicle on jobs.rescue_vehicle_id = rescue_vehicle.id

order by jobs.id DESC;




-- Tesla Data Inserted at c4990e
-- Tesla All Data Report Updated to 799aa0: added zip
-- Tesla All Data Report Updated to 793cbc: Add invoicing_ledger_items.deleted_at is null
-- Tesla All Data Report Updated to 6b632d: Add Department
-- Tesla All Data Report Updated to 9079b2: migrated to view and added 'Payment Type' and 'Dispatcher Name'
-- Tesla All Data Report Updated to d7100c: Add Time and Invoice Status
-- Tesla All Data Report Updated to e14508: Add Cost Center, GL Account, SIO, Budget Code, Location code, Drop Off Location Type, Drop Off Location Type
SELECT merge_reports('Tesla All Data Report' , 'FleetCompany', $$
SELECT
  "Job ID",
  "Date (Pacific)",
  "Time (Pacific)",
  "Customer",
  "Phone",
  "Location Type",
  "Partner",
  "Make",
  "Model",
  "pickup location",
  "City",
  "State",
  "Zip",
  "Pickup",
  "Dropoff",
  dropoff_location_type.name AS "Drop Off Location Type",
  "Service",
  "Scheduled",
  "EN Route Miles",
  "Pick Up To Drop Off",
  "Return Miles",
  "P2P Miles",
  "ETA",
  "ATA",
  "ATA - ETA",
  "Handle Time",
  "Status",
  "Payment Type",
  "Invoiced",
  "Partner Live",
  "Partner Open",
  "Partner Live and Open",
  "Platform",
  "Invoice Status",
  vin,
  "Symptom",
  "P2P Job Time Estimate",
  "Port-to-Complete Job Time Estimate",
  "Department",
  "Fleet Dispatcher",
  "Partner Dispatcher",
  "Created",
  "Assigned",
  "Accepted",
  "Dispatched",
  "En Route",
  "On Site",
  "Towing",
  "Tow Destination",
  "Completed",
  "GOA",
  "Released",
  "Canceled",
  "Digital Dispatch Time",
  "Digital En Route",
  "En Route -> Completed",
  "Job Details",
  "Fleet Notes",
  "Invoice Notes",
  "Reject Reason",
  "Reject Comment",
  "Rescue Vehicle Name",
  (CASE
    WHEN departments.name = 'Roadside' AND customer_types.name = 'Body Shop' THEN '30150300'
    WHEN departments.name = 'Logistics' THEN '30167000'
    WHEN departments.name = 'Service Center' THEN '30150100'
    WHEN departments.name = 'Other' THEN '30153000'
    WHEN customer_types.name IN ('Warranty', 'Goodwill') THEN '30153000'
    WHEN customer_types.name = 'Body Shop' THEN '30150001'
    WHEN customer_types.name = 'P Card' THEN 'COGSSVR'
    ELSE '30150000'
  END) AS "Cost Center",
  (CASE
    WHEN customer_types.name IN ('Warranty', 'P Card') THEN '224520'
    WHEN customer_types.name = 'Goodwill' THEN '640001'
    WHEN customer_types.name = 'Body Shop' THEN '685150'
    ELSE '610600'
  END) AS "GL Account",
  (CASE WHEN customer_types.name in ('Warranty', 'P Card', 'Body Shop') THEN 'S5200' ELSE 'C9000' END) as "SIO",
  (CASE
    WHEN customer_types.name = 'Body Shop' THEN 'AUBNW'
    ELSE departments.code
  END) AS "Budget Code",
  (CASE
    WHEN customer_types.name = 'Body Shop' THEN '787'
    WHEN sites.site_code is NULL THEN provider.location_code
    ELSE sites.site_code
  END) AS "Location"
FROM fleet_all_data_view
LEFT JOIN jobs ON jobs.id = "Job ID"
LEFT JOIN departments ON departments.id = jobs.department_id
LEFT JOIN customer_types ON customer_types.id = jobs.customer_type_id
LEFT JOIN rescue_providers AS provider
  ON  provider.site_id = jobs.site_id
  AND provider.company_id = jobs.fleet_company_id
  AND provider.deleted_at IS NULL
LEFT JOIN sites ON jobs.fleet_site_id = sites.id
LEFT JOIN locations AS dropoff_location ON jobs.drop_location_id = dropoff_location.id
LEFT JOIN location_types AS dropoff_location_type ON dropoff_location.location_type_id = dropoff_location_type.id
WHERE "Status" in ('Completed', 'GOA', 'Released', 'Canceled')
  AND fleet_all_data_view.created_at >= ?
  AND fleet_all_data_view.created_at < ?
  AND fleet_all_data_view.fleet_company_id = ?
  {{rescue_company}};
$$,
'{ "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'["start_date", "end_date", "api_company.id", "rescue_company"]',
'{"rescue_company": "AND fleet_all_data_view.rescue_company_id=?"}',
'{"api_company.id": "api_company.id"}',
'{"start_date": {"type":"start_date"}, "end_date": {"type": "end_date"}, "rescue_company": {"type": "partner"}}');


-- Enterprise All Data Report Updated to 793cbc: Copy of Tesla
-- Enterprise All Data Report Updated to e12238: Add Unit and claim numbers
-- Enterprise All Data Report Updated to 97923c migrated to view and added 'Payment Type' and 'Dispatcher Name'
-- Enterprise All Data Report Updated to 97fa7f Add Time and Invoice Status
SELECT merge_reports('Enterprise All Data Report' , 'FleetCompany', $$
select
  "Job ID",
  "Date (Pacific)",
  "Time (Pacific)",
  "Partner",
  "Make",
  "Model",
  "pickup location",
  zip,
  "Pickup",
  "Dropoff",
  "Service",
  "EN Route Miles",
  "Pick Up To Drop Off",
  "Return Miles",
  "P2P Miles",
  "ETA",
  "ATA",
  "Status",
  "Payment Type",
  "Invoiced",
  "Invoice Status",
  vin,
  "Unit Number",
  "Claim Number",
  "Symptom" as "Tow Category",
  "P2P Job Time Estimate",
  "Port-to-Complete Job Time Estimate",
  "Fleet Dispatcher"
FROM fleet_all_data_view
where
  "Status" in ('Completed', 'GOA', 'Released', 'Canceled')
  and created_at >= ?
  and created_at < ?
  and fleet_company_id=?
  {{rescue_company}};
$$,
'{ "start_date" : "2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date", "api_company.id", "rescue_company"]',
'{"rescue_company":"and rescue_company_id=?"}',
'{"api_company.id": "api_company.id"}',
'{  "start_date":{"type":"start_date"}, "end_date":{"type":"end_date"}, "rescue_company":{"type":"partner"}}');


-- All Data Report (FleetInHouse) Copied from Tesla All Data
-- All Data Report (FleetInHouse) Inserted at 9079b2
-- All Data Report (FleetInHouse) Inserted at d7100c Add Time and Invoice Status
SELECT merge_reports('All Data Report (FleetInHouse)' , 'FleetCompany', $$
select
  "Job ID",
  "Date (Pacific)",
  "Time (Pacific)",
  "Partner",
  "Make",
  "Model",
  "pickup location",
  zip,
  "Pickup",
  "Dropoff",
  "Service",
  "EN Route Miles",
  "Pick Up To Drop Off",
  "Return Miles",
  "P2P Miles",
  "ETA",
  "ATA",
  "Status",
  "Payment Type",
  "Invoiced",
  "Invoice Status",
  vin,
  "Symptom",
  "P2P Job Time Estimate",
  "Port-to-Complete Job Time Estimate",
  "Department",
  "Fleet Dispatcher"
FROM   fleet_all_data_view
where
  "Status" in ('Completed', 'GOA', 'Released', 'Canceled')
  and created_at >= ?
  and created_at < ?
  and fleet_company_id=?
  {{rescue_company}};
$$,
'{ "start_date" : "2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date", "api_company.id", "rescue_company"]',
'{"rescue_company":"and rescue_company_id=?"}',
'{"api_company.id": "api_company.id"}',
'{  "start_date":{"type":"start_date"}, "end_date":{"type":"end_date"}, "rescue_company":{"type":"partner"}}');

-- Jobs Report
SELECT merge_reports('Jobs Report' , 'RescueCompany', $$
  SELECT
    jobs.id AS "Swoop ID",
    (
      SELECT TO_CHAR(COALESCE(adjusted_dttm, last_set_dttm) AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
        AND deleted_at IS NULL
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatch Date",
    sites.name AS "Site",
    accounts.name AS "Account",
    jobs.po_number AS "PO",
    jobs.status AS "Status",
    CONCAT(drivers.first_name, ' ', drivers.last_name) AS "Driver",
    trucks.name AS "Truck",
    vehicle_categories.name AS "Class Type",
    CONCAT_WS(' ', partner_dispatchers.first_name, partner_dispatchers.last_name) AS "Dispatcher",
    service_codes.name AS "Service",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
        AND deleted_at IS NULL
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatched",
    TO_CHAR(
      ((SELECT COALESCE(adjusted_dttm, last_set_dttm) FROM audit_job_statuses WHERE job_status_id = 7 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1) -
      (SELECT COALESCE(adjusted_dttm, last_set_dttm) FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1)),
      'DD:HH24:MI'
    ) AS "Total Job Time",
    ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) AS "Total Miles",
    CAST(COALESCE((
      SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount) AS sum_amount
      FROM invoicing_line_items
      WHERE
        invoices.id = invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
     GROUP BY invoices.id), 0) AS DECIMAL(14, 2)
    ) AS "Invoice Total",
    job_reject_reasons.text as "Reject Reason",
    jobs.reject_reason_info as "Reject Comment"
  FROM jobs
  LEFT JOIN users ON users.id = ?
  LEFT JOIN accounts ON accounts.id = jobs.account_id
  LEFT JOIN users AS drivers ON jobs.rescue_driver_id = drivers.id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN users AS partner_dispatchers ON jobs.partner_dispatcher_id = partner_dispatchers.id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN invoicing_ledger_items AS invoices ON invoices.job_id = jobs.id AND invoices.deleted_at IS NULL AND invoices.sender_id = ? AND invoices.type='Invoice'
  LEFT JOIN job_reject_reasons on jobs.reject_reason_id = job_reject_reasons.id
  LEFT JOIN sites on jobs.site_id = sites.id
  LEFT JOIN vehicle_categories on jobs.invoice_vehicle_category_id = vehicle_categories.id
  WHERE
    jobs.rescue_company_id=?
    AND jobs.deleted_at IS NULL
    AND jobs.status != 'Reassigned'
    AND (
      (
        SELECT COALESCE(adjusted_dttm, last_set_dttm, jobs.created_at)
        FROM audit_job_statuses
        WHERE
          (job_status_id = 2 OR (jobs.status = 'Rejected' AND job_status_id = 10))
          AND job_id = jobs.id
          AND deleted_at IS NULL
        ORDER BY id DESC
        LIMIT 1
      ) >= ?
      AND (
        SELECT COALESCE(adjusted_dttm, last_set_dttm, jobs.created_at)
        FROM audit_job_statuses
        WHERE
          (job_status_id = 2 OR (jobs.status = 'Rejected' AND job_status_id = 10))
          AND job_id = jobs.id
          AND deleted_at IS NULL
        ORDER BY id DESC
        LIMIT 1
      ) < ?
    )
    {{driver}}
    {{vehicle}}
    {{account}}
    {{service}}
    {{dispatcher}}
    {{site}}
    {{vehicle_category}}
ORDER BY jobs.id DESC;
$$,
'{ "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "sender_id", "rescue_company_id", "start_date", "end_date", "driver", "vehicle", "account", "service", "dispatcher", "site", "vehicle_category"]',
'{
  "driver": "and rescue_driver_id = ?",
  "vehicle": "and rescue_vehicle_id = ?",
  "account": "and account_id = ?",
  "service": "and service_code_id = ?",
  "dispatcher": "and partner_dispatcher_id = ?",
  "site": "and site_id = ?",
  "vehicle_category": "and invoice_vehicle_category_id = ?"
}',
'{ "api_user.id": "api_user.id", "rescue_company_id": "api_company.id", "sender_id": "api_company.id" }',
'{
  "start_date": { "type":"eval", "method": "start_date_yesterday_beginning_of_day" },
  "end_date": { "type":"eval", "method": "end_date_yesterday_end_of_day" },
  "account": { "type":"account" },
  "vehicle": { "type":"eval", "method": "customize_vehicle_label" },
  "driver": { "type":"driver" },
  "service": { "type":"service" },
  "dispatcher": { "type":"eval", "method":"dispatchers" },
  "site": { "type":"eval", "method":"partner_sites" },
  "vehicle_category": { "type":"eval", "method":"vehicle_categories" }
}',
2);

-- Account Summary Report
SELECT merge_reports('Account Summary' , 'RescueCompany', $$
  WITH jobs AS (
    SELECT
      jobs.id,
      jobs.account_id,
      invoices.id AS invoice_id,
      invoices.state AS invoice_state,
      invoices.paid AS invoice_paid,
      CAST((
        SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
        FROM invoicing_line_items
        WHERE
          invoices.id = invoicing_line_items.ledger_item_id
          and invoicing_line_items.deleted_at is null
        GROUP BY invoices.id
      ) AS DECIMAL(14, 2)) as amount,
      ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) AS total_miles
    FROM jobs
    LEFT JOIN estimates ON
      jobs.estimate_id = estimates.id
    LEFT JOIN invoicing_ledger_items AS invoices ON
      jobs.id = invoices.job_id
      AND invoices.deleted_at IS NULL
      AND invoices.sender_id = ?
      AND invoices.type='Invoice'
    WHERE
      jobs.deleted_at IS NULL
      AND jobs.rescue_company_id = ?
      AND jobs.status != 'Reassigned'
      AND (
        SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
        FROM audit_job_statuses
        WHERE
          job_status_id = 2
          AND job_id = jobs.id
        ORDER BY id DESC
        LIMIT 1) >= ?
      AND (
        SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
        FROM audit_job_statuses
        WHERE
          job_status_id = 2
          AND job_id = jobs.id
        ORDER BY id DESC
        LIMIT 1) < ?
  )
  SELECT
    accounts.name AS "Account",
    accounts.contact_phone AS "Phone",
    (
      SELECT COUNT(*)
      FROM jobs
      WHERE jobs.account_id = accounts.id
    ) AS "Total Jobs",
    (
      SELECT COALESCE(SUM(total_miles), 0)
      FROM jobs
      WHERE jobs.account_id = accounts.id
    ) AS "Total Miles",
    (
      SELECT COUNT(*)
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_state NOT IN ('partner_sent', 'fleet_approved', 'fleet_downloaded', 'on_site_payment', 'swoop_downloaded_partner', 'swoop_approved_partner')
    ) AS "Unsent Invoices",
    (
      SELECT CAST(COALESCE(SUM(amount), 0) AS DECIMAL(14, 2))
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_state NOT IN ('partner_sent', 'fleet_approved', 'fleet_downloaded', 'on_site_payment', 'swoop_downloaded_partner', 'swoop_approved_partner')
    ) AS "Unsent Total",
    (
      SELECT COUNT(*)
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_state IN ('partner_sent', 'fleet_approved', 'fleet_downloaded', 'on_site_payment', 'swoop_downloaded_partner', 'swoop_approved_partner')
        AND jobs.invoice_paid = FALSE
    ) AS "Outstanding Invoices",
    (
      SELECT CAST(COALESCE(SUM(amount), 0) AS DECIMAL(14, 2))
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_state IN ('partner_sent', 'fleet_approved', 'fleet_downloaded', 'on_site_payment', 'swoop_downloaded_partner', 'swoop_approved_partner')
        AND jobs.invoice_paid = FALSE
    ) AS "Outstanding Total",
    (
      SELECT COUNT(*)
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_paid = TRUE
    ) AS "Paid Invoices",
    (
      SELECT CAST(COALESCE(SUM(amount), 0) AS DECIMAL(14, 2))
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_paid = TRUE
    ) AS "Paid Total",
    (
      SELECT CAST(AVG(amount) AS DECIMAL(14, 2))
      FROM jobs
      WHERE jobs.account_id = accounts.id
    ) AS "Avg. Revenue per Job",
    (
      SELECT CAST(COALESCE(SUM(amount), 0) AS DECIMAL(14, 2))
      FROM jobs
      WHERE jobs.account_id = accounts.id
    ) AS "Total Revenue"
  FROM accounts
  WHERE accounts.company_id = ?
  {{account}}
  ORDER BY accounts.name ASC;
$$,
'{ "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_company.id", "api_company.id", "start_date", "end_date", "api_company.id", "account"]',
'{ "account": "and accounts.id = ?" }',
'{ "api_company.id": "api_company.id" }',
'{
  "start_date": { "type": "eval", "method": "start_date_yesterday_beginning_of_day" },
  "end_date": { "type": "eval", "method": "end_date_yesterday_end_of_day" },
  "account": { "type": "account" }
}',
3);

-- Driver Summary Report
SELECT merge_reports('Driver Summary' , 'RescueCompany', $$
  SELECT
    "Driver Name",
    "Total Jobs",
    CONCAT(COALESCE((EXTRACT(DAY FROM total_time) * 24) + (EXTRACT(HOUR FROM total_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM total_time)::TEXT, '0'), 2, '0'), ':00') AS "Total Time",
    CONCAT(COALESCE((EXTRACT(DAY FROM drive_time) * 24) + (EXTRACT(HOUR FROM drive_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM drive_time)::TEXT, '0'), 2, '0'), ':00') AS "Drive Time",
    "Total Miles",
    "Avg. ETA",
    "Avg. ATA",
    "Avg. Revenue per Job",
    "Total Revenue"
  FROM (
    SELECT
      CONCAT(drivers.first_name, ' ', drivers.last_name) AS "Driver Name",
      COUNT(jobs.id) AS "Total Jobs",
      JUSTIFY_HOURS(COALESCE(SUM(
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM audit_job_statuses WHERE job_status_id = 7 AND job_id = jobs.id ORDER BY id DESC limit 1) -
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id ORDER BY id DESC limit 1)
      ), INTERVAL '0')) AS total_time,
      JUSTIFY_HOURS(COALESCE(SUM(
        COALESCE((
          (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN DATE_TRUNC('minute', adjusted_dttm) ELSE DATE_TRUNC('minute', last_set_dttm) END FROM audit_job_statuses WHERE job_status_id = 6 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) -
          (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN DATE_TRUNC('minute', adjusted_dttm) ELSE DATE_TRUNC('minute', last_set_dttm) END FROM audit_job_statuses WHERE job_status_id = 5 AND job_id = jobs.id ORDER BY id DESC LIMIT 1)
        ), INTERVAL '0') + (
          (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN DATE_TRUNC('minute', adjusted_dttm) ELSE DATE_TRUNC('minute', last_set_dttm) END FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) -
          (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN DATE_TRUNC('minute', adjusted_dttm) ELSE DATE_TRUNC('minute', last_set_dttm) END FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id ORDER BY id DESC LIMIT 1)
        )
      ), INTERVAL '0')) AS drive_time,
      COALESCE(SUM(ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1)), 0) AS "Total Miles",
      ROUND(AVG(CAST((EXTRACT(EPOCH FROM
        (SELECT (time - created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS numeric)), 0) AS "Avg. ETA",
      ROUND(AVG(CAST((EXTRACT(EPOCH FROM
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
        (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS numeric)), 0) AS "Avg. ATA",
      CAST(COALESCE(AVG((
        SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
        FROM invoicing_line_items
        WHERE
          invoices.id = invoicing_line_items.ledger_item_id
          AND invoicing_line_items.description != 'Storage'
          and invoicing_line_items.deleted_at is null
        GROUP BY invoices.id
      )), 0) AS DECIMAL(14, 2)) as "Avg. Revenue per Job",
      CAST(
        COALESCE(
          SUM((
            SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
            FROM invoicing_line_items
            WHERE
              invoices.id = invoicing_line_items.ledger_item_id
              AND invoicing_line_items.description != 'Storage'
              and invoicing_line_items.deleted_at is null
            GROUP BY invoices.id
          ))
        , 0)
      AS DECIMAL(14, 2)) as "Total Revenue"
    FROM jobs
    RIGHT JOIN users AS drivers ON jobs.rescue_driver_id = drivers.id
    LEFT JOIN companies ON drivers.company_id = companies.id
    LEFT JOIN users_roles ON drivers.id = users_roles.user_id
    LEFT JOIN roles ON users_roles.role_id = roles.id
    LEFT JOIN estimates ON jobs.estimate_id = estimates.id
    LEFT JOIN invoicing_ledger_items AS invoices ON
      jobs.id = invoices.job_id
      AND invoices.deleted_at IS NULL
      AND invoices.sender_id = ?
      AND invoices.type='Invoice'
    WHERE
      jobs.deleted_at IS NULL
      AND roles.name = 'driver'
      AND drivers.company_id = ?
      AND (jobs.status != 'Reassigned' OR jobs.status IS NULL)
      AND (
        (
          SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses
          WHERE
            job_status_id = 2
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) >= ?
        OR (
          SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses
          WHERE
            job_status_id = 2
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) IS NULL
      )
      AND (
        (
          SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses
          WHERE
            job_status_id = 2
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) < ?
        OR (
          SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses
          WHERE
            job_status_id = 2
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) IS NULL
      )
    {{driver}}
    GROUP BY drivers.id
    ORDER BY drivers.last_name ASC
  ) AS results;
$$,
'{ "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_company.id", "api_company.id", "start_date", "end_date", "driver"]',
'{ "driver": "and drivers.id = ?" }',
'{ "api_company.id": "api_company.id" }',
'{
  "start_date": { "type":"eval", "method": "start_date_yesterday_beginning_of_day" },
  "end_date": { "type":"eval", "method": "end_date_yesterday_end_of_day" },
  "driver": { "type": "driver" }
}',
4);


-- Partner Commissions Report
-- Based on Commissions report used on Looker
SELECT merge_reports('Commissions' , 'RescueCompany', $$
SELECT
accounts.name  AS "Account",
jobs.id  AS "ID",
DATE((jobs.created_at )::timestamptz AT TIME ZONE 'America/Los_Angeles') AS "Date",
driver.first_name || ' ' || driver.last_name  AS "Driver",
service.name AS "Primary Service",
COALESCE(COALESCE(CAST((SUM (DISTINCT (CAST(FLOOR(COALESCE(invoicing_line_items.net_amount ,0)*(1000000*1.0)) AS DECIMAL(65,0))) + ('x'
  || MD5(invoicing_line_items.id ::varchar))::bit(64)::bigint::DECIMAL(65,0)  *18446744073709551616 + ('x'
  || SUBSTR(MD5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::DECIMAL(65,0) ) - SUM(DISTINCT ('x'
  || MD5(invoicing_line_items.id ::varchar))::bit(64)::bigint::DECIMAL(65,0)  *18446744073709551616 + ('x'
  || SUBSTR(MD5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::DECIMAL(65,0)) )  AS DOUBLE PRECISION) / CAST((1000000*1.0) AS DOUBLE PRECISION), 0), 0) AS "Net Amount",
CASE WHEN COUNT(service_exclusion.id) > 0 THEN '0.00' ELSE (
  COALESCE(COALESCE(CAST((SUM (DISTINCT (CAST(FLOOR(COALESCE(invoicing_line_items_minus_exclusions.net_amount*(CAST((SELECT default_pct FROM company_commissions WHERE company_commissions.company_id = rescue_company.id) AS DECIMAL) / 100), 0)*(1000000*1.0)) AS DECIMAL(65,0))) + ('x'
    || MD5(invoicing_line_items_minus_exclusions.id ::varchar))::bit(64)::bigint::DECIMAL(65,0)  *18446744073709551616 + ('x'
    || SUBSTR(MD5(invoicing_line_items_minus_exclusions.id ::varchar),17))::bit(64)::bigint::DECIMAL(65,0) ) - SUM(DISTINCT ('x'
    || MD5(invoicing_line_items_minus_exclusions.id ::varchar))::bit(64)::bigint::DECIMAL(65,0)  *18446744073709551616 + ('x'
    || SUBSTR(MD5(invoicing_line_items_minus_exclusions.id ::varchar),17))::bit(64)::bigint::DECIMAL(65,0)) )  AS DOUBLE PRECISION) / CAST((1000000*1.0) AS DOUBLE PRECISION), 0), 0)
  ) END AS "Commission",
COALESCE(CAST((
    SELECT sum(invoicing_tax_items.tax_amount) AS tax_amount
    FROM invoicing_line_items AS invoicing_tax_items
    WHERE invoicing_tax_items.ledger_item_id = jobs.invoice_id
    AND invoicing_tax_items.description!='Storage') AS DECIMAL(14, 2)), '0.00') AS "Tax"
FROM jobs  AS jobs
LEFT JOIN accounts AS accounts ON jobs.account_id = accounts.id
LEFT JOIN companies AS rescue_company ON jobs.rescue_company_id = rescue_company.id
LEFT JOIN users AS driver ON jobs.rescue_driver_id = driver.id
LEFT JOIN service_codes AS service ON jobs.service_code_id = service.id
LEFT JOIN service_commissions_exclusions AS service_exclusion ON jobs.service_code_id = service_exclusion.service_code_id
      AND rescue_company.id = service_exclusion.company_id
LEFT JOIN invoicing_ledger_items  AS invoicing_ledger_items ON jobs.id = invoicing_ledger_items.job_id
      AND invoicing_ledger_items.sender_id = jobs.rescue_company_id
      AND invoicing_ledger_items.deleted_at IS NULL
      AND invoicing_ledger_items.type = 'Invoice'
LEFT JOIN invoicing_line_items  AS invoicing_line_items ON invoicing_ledger_items.id = invoicing_line_items.ledger_item_id
      AND invoicing_line_items.deleted_at IS NULL
LEFT JOIN invoicing_line_items  AS invoicing_line_items_minus_exclusions ON invoicing_ledger_items.id = invoicing_line_items_minus_exclusions.ledger_item_id
      AND invoicing_line_items_minus_exclusions.deleted_at IS NULL
      AND
        (
          (CASE WHEN EXISTS (SELECT id from service_commissions_exclusions WHERE company_id = rescue_company.id AND service_code_id = (SELECT id FROM service_codes where name = 'Storage' AND is_standard = true))
            THEN invoicing_line_items_minus_exclusions.description != 'Storage' AND (lower(invoicing_line_items_minus_exclusions.description) NOT IN (SELECT lower(commission_exclusions.item) FROM commission_exclusions WHERE company_id=invoicing_ledger_items.sender_id))
            ELSE (lower(invoicing_line_items_minus_exclusions.description) NOT IN (SELECT lower(commission_exclusions.item) FROM commission_exclusions WHERE company_id=invoicing_ledger_items.sender_id))
          END)
        )
WHERE (jobs.status = 'Completed' OR jobs.status = 'GOA' OR jobs.status = 'Stored' OR jobs.status = 'Released')
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
  AND rescue_company.id = ?
  {{driver}}
  {{account}}
GROUP BY 1,2,3,4,5
ORDER BY 1
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'[ "start_date", "end_date", "rescue_company", "driver", "account" ]',
'{ "driver": "and driver.id = ?", "account": "and accounts.id = ?"}',
'{ "rescue_company": "api_company.id" }',
'{
    "start_date": { "type": "eval", "method": "start_date_yesterday_beginning_of_day" },
    "end_date": { "type": "eval", "method": "end_date_yesterday_end_of_day" },
    "driver": { "type":"driver" },
    "account": { "type": "account" }
 }',
 5);

SELECT add_pdf_to_report('Commissions');


-- Truck Summary Report
SELECT merge_reports('Truck Summary' , 'RescueCompany', $$
  SELECT
    "Truck",
    "Total Jobs",
    CONCAT(COALESCE((EXTRACT(DAY FROM total_time) * 24) + (EXTRACT(HOUR FROM total_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM total_time)::TEXT, '0'), 2, '0'), ':00') AS "Total Time",
    CONCAT(COALESCE((EXTRACT(DAY FROM drive_time) * 24) + (EXTRACT(HOUR FROM drive_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM drive_time)::TEXT, '0'), 2, '0'), ':00') AS "Drive Time",
    "Total Miles",
    "En Route Miles",
    "Towing Miles",
    "Deadhead Miles",
    CAST((CASE WHEN "Total Miles" = 0 THEN NULL ELSE ("Total Revenue" / "Total Miles") END) AS DECIMAL(14, 2)) AS "Avg. per Mile",
    "Avg. Revenue per Job",
    "Total Revenue"
  FROM (
    SELECT
      trucks.name AS "Truck",
      COUNT(jobs.id) AS "Total Jobs",
      JUSTIFY_HOURS(COALESCE(SUM(
        (SELECT COALESCE(adjusted_dttm, last_set_dttm) FROM audit_job_statuses WHERE job_status_id = 7 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC limit 1) -
        (SELECT COALESCE(adjusted_dttm, last_set_dttm) FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC limit 1)
      ), INTERVAL '0')) AS total_time,
      JUSTIFY_HOURS(COALESCE(SUM(
        COALESCE((
          (SELECT DATE_TRUNC('minute', COALESCE(adjusted_dttm, last_set_dttm)) FROM audit_job_statuses WHERE job_status_id = 6 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1) -
          (SELECT DATE_TRUNC('minute', COALESCE(adjusted_dttm, last_set_dttm)) FROM audit_job_statuses WHERE job_status_id = 5 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1)
        ), INTERVAL '0') + (
          (SELECT DATE_TRUNC('minute', COALESCE(adjusted_dttm, last_set_dttm)) FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1) -
          (SELECT DATE_TRUNC('minute', COALESCE(adjusted_dttm, last_set_dttm)) FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1)
        )
      ), INTERVAL '0')) AS drive_time,
      COALESCE(SUM(ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1)), 0) AS "Total Miles",
      COALESCE(SUM(ROUND(estimates.meters_ab * 0.000621371, 1)), 0) AS "En Route Miles",
      COALESCE(SUM(ROUND(estimates.meters_bc * 0.000621371, 1)), 0) AS "Towing Miles",
      COALESCE(SUM(ROUND((CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_ca ELSE estimates.meters_ba END) * 0.000621371, 1)), 0) AS "Deadhead Miles",
      CAST(AVG((
        SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
        FROM invoicing_line_items
        WHERE
          invoices.id = invoicing_line_items.ledger_item_id
          AND invoicing_line_items.description != 'Storage'
          and invoicing_line_items.deleted_at is null
        GROUP BY invoices.id
      )) AS DECIMAL(14, 2)) as "Avg. Revenue per Job",
      CAST(COALESCE(SUM((
        SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
        FROM invoicing_line_items
        WHERE
          invoices.id = invoicing_line_items.ledger_item_id
          AND invoicing_line_items.description != 'Storage'
          and invoicing_line_items.deleted_at is null
        GROUP BY invoices.id
      )), 0) AS DECIMAL(14, 2)) as "Total Revenue"
    FROM vehicles AS trucks
    LEFT OUTER JOIN jobs ON jobs.rescue_vehicle_id = trucks.id
    LEFT JOIN estimates ON jobs.estimate_id = estimates.id
    LEFT JOIN invoicing_ledger_items AS invoices ON
      jobs.id = invoices.job_id
      AND invoices.deleted_at IS NULL
      AND invoices.sender_id = ?
      AND invoices.type = 'Invoice'
    WHERE
      jobs.deleted_at IS NULL
      AND trucks.company_id = ?
      AND (jobs.status != 'Reassigned' OR jobs.status IS NULL)
      AND (
        (
          jobs.id IS NULL AND trucks.deleted_at IS NULL
        ) OR (
          (
            SELECT COALESCE(adjusted_dttm, last_set_dttm, jobs.created_at)
            FROM audit_job_statuses
            WHERE
              (job_status_id = 2 OR (jobs.status = 'Rejected' AND job_status_id = 10))
              AND job_id = jobs.id
              AND deleted_at IS NULL
            ORDER BY id DESC
            LIMIT 1
          ) >= ?
          AND (
            SELECT COALESCE(adjusted_dttm, last_set_dttm, jobs.created_at)
            FROM audit_job_statuses
            WHERE
              (job_status_id = 2 OR (jobs.status = 'Rejected' AND job_status_id = 10))
              AND job_id = jobs.id
              AND deleted_at IS NULL
            ORDER BY id DESC
            LIMIT 1
          ) < ?
        )
      )
      {{vehicle}}
    GROUP BY trucks.id
    ORDER BY trucks.name ASC
  ) AS results;
$$,
'{ "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_company.id", "api_company.id", "start_date", "end_date", "vehicle"]',
'{ "vehicle": "and trucks.id = ?" }',
'{ "api_company.id": "api_company.id" }',
'{
  "start_date": { "type":"eval", "method": "start_date_yesterday_beginning_of_day" },
  "end_date": { "type":"eval", "method": "end_date_yesterday_end_of_day" },
  "vehicle": { "type":"eval", "method": "customize_vehicle_label" }
}',
6);

-- Statement Report
SELECT merge_reports('Statement' , 'RescueCompany', $$
  SELECT
    jobs.id AS "Swoop ID",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY')
      FROM audit_job_statuses
      WHERE
        job_status_id = CASE
          WHEN jobs.status = 'Completed' THEN 7
          WHEN jobs.status = 'Canceled' THEN 8
          WHEN jobs.status = 'Released' THEN 18
          WHEN jobs.status = 'GOA' THEN 21
        END
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Service Date",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = CASE
          WHEN jobs.status = 'Completed' THEN 7
          WHEN jobs.status = 'Canceled' THEN 8
          WHEN jobs.status = 'Released' THEN 18
          WHEN jobs.status = 'GOA' THEN 21
        END
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Time",
    accounts.name AS "Account",
    jobs.po_number AS "PO",
    service_codes.name AS "Service",
    CONCAT_WS(' ', vehicles.make, vehicles.model, vehicles.year) as "Vehicle",
    vehicles.vin as "Vin",
    jobs.status AS "Status",
    ( /* PDW: If you modify this then you also need to modify the filter clauses */
      CASE
        WHEN invoices.state IN ('created', 'partner_new', 'partner_approved') AND invoices.paid != 't' THEN 'Unsent'
        WHEN ((
          (COALESCE(invoices.total_amount, 0) + (
            SELECT COALESCE(SUM(total_amount), 0) AS "payments_or_credits_sum"
            FROM invoicing_ledger_items AS "payments_or_credits"
            WHERE payments_or_credits.invoice_id = invoices.id
            AND payments_or_credits.deleted_at IS NULL
            AND payments_or_credits.type IN ('Payment', 'Discount', 'Refund', 'WriteOff')
          )) > 0) OR (invoices.paid != TRUE AND invoices.state != 'on_site_payment')) THEN 'Outstanding'
        WHEN invoices.state = 'on_site_payment' OR invoices.paid = TRUE THEN 'Paid'
        ELSE NULL
      END
    ) AS "Invoice Status",
    CAST((
      SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount) AS sum_amount
      FROM invoicing_line_items
      WHERE invoices.id = invoicing_line_items.ledger_item_id
        AND invoicing_line_items.deleted_at IS NULL
     GROUP BY invoices.id) AS DECIMAL(14, 2)
    ) AS "Invoice Total",
    (
      SELECT
      (
        CASE
          WHEN MAX(payments.total) > 1 THEN 'Multiple'
          ELSE MAX(payments.payment_method)
        END
      )
      FROM (
        SELECT COUNT(sub_payments.payment_method) AS "total", MAX(payment_method) AS "payment_method"
        FROM invoicing_ledger_items AS "sub_payments"
        WHERE sub_payments.invoice_id = invoices.id
        AND sub_payments.deleted_at IS NULL
        AND sub_payments.type = 'Payment'
      ) AS "payments"
    ) AS "Payment Method",
    CAST((
        SELECT ABS(COALESCE(SUM(total_amount), 0))
        FROM invoicing_ledger_items AS "payments"
        WHERE payments.invoice_id = invoices.id
        AND payments.deleted_at IS NULL
        AND payments.type IN ('Payment')
      ) AS DECIMAL(14, 2)
    ) AS "Payment",
    CAST((
      COALESCE(invoices.total_amount, 0) + (
        SELECT COALESCE(SUM(total_amount), 0) AS "payments_or_credits_sum"
        FROM invoicing_ledger_items AS "payments_or_credits"
        WHERE payments_or_credits.invoice_id = invoices.id
        AND payments_or_credits.deleted_at IS NULL
        AND payments_or_credits.type IN ('Payment', 'Discount', 'Refund', 'WriteOff')
      )) AS DECIMAL(14, 2)
    ) AS "Balance Due"
  FROM jobs
  LEFT JOIN users ON users.id = ?
  LEFT JOIN accounts ON accounts.id = jobs.account_id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN users AS customers ON drives.user_id = customers.id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN invoicing_ledger_items AS invoices ON invoices.job_id = jobs.id AND invoices.deleted_at IS NULL AND invoices.sender_id = ? AND invoices.type='Invoice' AND invoices.deleted_at IS NULL
  WHERE
    jobs.rescue_company_id=?
    AND jobs.deleted_at IS NULL
    AND jobs.status IN ('Completed', 'GOA', 'Canceled', 'Released')
    AND (SELECT
          (CASE
            WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
            FROM audit_job_statuses WHERE job_status_id = CASE
              WHEN jobs.status = 'Completed' THEN 7
              WHEN jobs.status = 'Canceled' THEN 8
              WHEN jobs.status = 'Released' THEN 18
              WHEN jobs.status = 'GOA' THEN 21
          END
          AND job_id = jobs.id ORDER BY id DESC LIMIT 1) >= ?
    AND (SELECT
          (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses WHERE job_status_id = CASE
            WHEN jobs.status = 'Completed' THEN 7
            WHEN jobs.status = 'Canceled' THEN 8
            WHEN jobs.status = 'Released' THEN 18
            WHEN jobs.status = 'GOA' THEN 21
          END
          AND job_id = jobs.id ORDER BY id DESC LIMIT 1) < ?
    {{account}}
    {{unsent}}
    {{outstanding}}
    {{paid}}
    {{invoice_payment_method}}
ORDER BY jobs.id DESC;
$$,
'{ "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "sender_id", "rescue_company_id", "start_date", "end_date", "account", "invoice_payment_method"]',
'{
   "account": "and account_id = ?",
   "unsent": "and state IN (''created'', ''partner_new'', ''partner_approved'') and paid != ''t''",
   "outstanding": "and invoices.state NOT IN (''created'', ''partner_new'', ''partner_approved'') and (((COALESCE(invoices.total_amount, 0) + (SELECT COALESCE(SUM(total_amount), 0) AS payments_or_credits_sum FROM invoicing_ledger_items AS payments_or_credits WHERE payments_or_credits.invoice_id = invoices.id AND payments_or_credits.deleted_at IS NULL AND payments_or_credits.type IN (''Payment'', ''Discount'', ''Refund'', ''WriteOff''))) > 0) OR (invoices.paid != TRUE AND invoices.state != ''on_site_payment''))",
   "paid": "AND (((COALESCE(invoices.total_amount, 0) + (SELECT COALESCE(SUM(total_amount), 0) AS payments_or_credits_sum FROM invoicing_ledger_items AS payments_or_credits WHERE payments_or_credits.invoice_id = invoices.id AND payments_or_credits.deleted_at IS NULL AND payments_or_credits.type IN (''Payment'', ''Discount'', ''Refund'', ''WriteOff''))) <= 0) AND (invoices.paid = TRUE OR invoices.state = ''on_site_payment''))",
   "invoice_payment_method": "and invoices.id IN (SELECT sub_payments.invoice_id FROM invoicing_ledger_items AS sub_payments WHERE sub_payments.invoice_id = invoices.id AND sub_payments.deleted_at IS NULL AND sub_payments.type = ''Payment'' AND sub_payments.payment_method = (SELECT name FROM payment_methods WHERE id = ? LIMIT 1))" }',
'{ "api_user.id": "api_user.id", "rescue_company_id": "api_company.id", "sender_id": "api_company.id" }',
'{ "start_date": { "type": "eval", "method": "start_date_yesterday_beginning_of_day" },
   "end_date": { "type": "eval", "method": "end_date_yesterday_end_of_day" },
   "account": { "type": "account" },
   "status": { "type": "eval", "method": "invoice_status"},
   "invoice_payment_method": { "type": "eval", "method": "invoice_payment_methods" }
 }',
7);

SELECT add_pdf_to_report('Statement');


-- Payments report added ENG-6321
SELECT merge_reports('Payments Report' , 'RescueCompany', $$
  SELECT
    job.id AS "Job ID",
    to_char(job.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE((SELECT timezone FROM users WHERE id = ? LIMIT 1), 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Job Created",
    account.name "Account",
    service.name "Service",
    CASE
      WHEN (payment_or_credit.type = 'Refund' AND charge.refund_id IS NOT NULL) THEN CONCAT(refund_user.first_name, ' ', refund_user.last_name)
      WHEN charge.payment_id IS NOT NULL THEN CONCAT(charge_user.first_name, ' ', charge_user.last_name)
      ELSE ''
    END AS "User",
    to_char(payment_or_credit.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE((SELECT timezone FROM users WHERE id = ? LIMIT 1), 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Payment Date",
    payment_or_credit.type AS "Transaction Type",
    payment_or_credit.payment_method AS "Payment Method",
    CAST((- payment_or_credit.total_amount) AS DECIMAL(14, 2)) AS "Amount",
    CAST((
      CASE
        WHEN payment_or_credit.type = 'Refund' AND charge.refund_id IS NOT NULL THEN (- COALESCE(charge.fee, 0))
        WHEN charge.payment_id IS NOT NULL THEN COALESCE(charge.fee, 0)
        ELSE 0
      END
    ) AS DECIMAL(14, 2)) AS "Fee",
    CAST((
      CASE
        WHEN payment_or_credit.type = 'Refund' THEN (-(payment_or_credit.total_amount - COALESCE(charge.fee, 0)))
        ELSE ((-payment_or_credit.total_amount) - COALESCE(charge.fee, 0))
      END
    ) AS DECIMAL(14, 2)) AS "Net",
    CASE
      WHEN payment_or_credit.type = 'Refund' AND charge.refund_id IS NOT NULL THEN 'Refunded'
      WHEN charge.payment_id IS NOT NULL THEN 'Charged'
    END AS "Charge Status",
    to_char(charge.upstream_payout_created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE((SELECT timezone FROM users WHERE id = ? LIMIT 1), 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Payout Date",
    charge.upstream_charge_id AS "Charge ID",
    charge.upstream_payment_id AS "Payment ID",
    charge.upstream_payout_id AS "Payout ID"
    FROM invoicing_ledger_items payment_or_credit
    LEFT JOIN invoicing_ledger_items invoice
      ON invoice.id = payment_or_credit.invoice_id
      AND invoice.deleted_at IS NULL
    LEFT JOIN invoice_payment_charges charge
      ON (charge.payment_id = payment_or_credit.id OR charge.refund_id = payment_or_credit.id)
    LEFT JOIN jobs job ON job.id = invoice.job_id
    LEFT JOIN accounts account ON account.id = job.account_id
    LEFT JOIN service_codes service ON service.id = job.service_code_id
    LEFT JOIN users charge_user ON charge_user.id = charge.charge_user_id
    LEFT JOIN users refund_user ON refund_user.id = charge.refund_user_id
    WHERE payment_or_credit.type IN ('Payment', 'Discount', 'Refund', 'WriteOff')
    AND payment_or_credit.deleted_at IS NULL
    AND invoice.deleted_at IS NULL
    AND job.deleted_at IS NULL
    AND invoice.sender_id = ?
    AND job.created_at >= ?
    AND job.created_at < ?
    {{account}}
    {{only_payments}}
    {{only_discounts}}
    {{only_refunds}}
    {{only_writeoffs}}
    {{only_swoop_charges}}
    {{invoice_payment_method}}
    {{charged}}
    {{refunded}}
    ORDER BY payment_or_credit.created_at;
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }', -- defaults
'[ "api_user.id", "api_user.id", "api_user.id", "sender.id", "start_date", "end_date", "account", "invoice_payment_method"]', -- all params
'{
   "account": "and job.account_id = ?",
   "only_payments": "and payment_or_credit.type = ''Payment''",
   "only_discounts": "and payment_or_credit.type = ''Discount''",
   "only_refunds": "and payment_or_credit.type = ''Refund''",
   "only_writeoffs": "and payment_or_credit.type = ''WriteOff''",
   "only_swoop_charges": "and charge.id IS NOT NULL",
   "invoice_payment_method": "and payment_or_credit.payment_method = (SELECT name FROM payment_methods WHERE id = ? LIMIT 1)",
   "charged": "and charge.payment_id IS NOT NULL and charge.refund_id IS NULL",
   "refunded": "and charge.refund_id IS NOT NULL and payment_or_credit.type = ''Refund''"
 }', -- filter_sql
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "sender.id": "api_company.id" }', -- mandatory_params
'{
  "start_date": { "type": "eval", "method": "start_date_yesterday_beginning_of_day" },
  "end_date": { "type": "eval", "method": "end_date_yesterday_end_of_day" },
  "account": { "type": "account" },
  "transaction_type": { "type": "eval", "method": "transaction_type"},
  "invoice_payment_method": { "type": "eval", "method": "invoice_payment_methods" },
  "payment_status": { "type": "eval", "method": "payment_status" }
}', 8);

-- PO Required Report
SELECT merge_reports('PO Required' , 'RescueCompany', $$
  SELECT
    jobs.id AS "Swoop ID",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatch Date",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatched",
    accounts.name AS "Account",
    accounts.contact_phone AS "Phone",
    service_codes.name AS "Service",
    CONCAT(customers.first_name, ' ', customers.last_name) AS "Customer",
    CONCAT_WS(' ', vehicles.make, vehicles.model, vehicles.year) as "Vehicle",
    jobs.status AS "Status",
    jobs.po_number AS "PO",
    CAST((
      SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount) AS sum_amount
      FROM invoicing_line_items
      WHERE
        invoices.id = invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
     GROUP BY invoices.id) AS DECIMAL(14, 2)
    ) AS "Invoice Total"
  FROM jobs
  LEFT JOIN users ON users.id = ?
  LEFT JOIN users AS customers ON jobs.user_id = customers.id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN accounts ON accounts.id = jobs.account_id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN invoicing_ledger_items AS invoices ON
    invoices.job_id = jobs.id
    AND invoices.deleted_at IS NULL
    AND invoices.sender_id = ?
    AND invoices.type='Invoice'
  WHERE
    jobs.rescue_company_id=?
    AND jobs.deleted_at IS NULL
    AND jobs.status != 'Reassigned'
    AND accounts.po_required_before_sending_invoice = TRUE
    AND (SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) FROM audit_job_statuses WHERE job_status_id = 2 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) >= ?
    AND (SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) FROM audit_job_statuses WHERE job_status_id = 2 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) < ?
    {{account}}
ORDER BY jobs.id DESC;
$$,
'{ "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "sender_id", "rescue_company_id", "start_date", "end_date", "account"]',
'{ "account": "and account_id = ?" }',
'{ "api_user.id": "api_user.id", "rescue_company_id": "api_company.id", "sender_id": "api_company.id" }',
'{
  "start_date": { "type":"eval", "method": "start_date_yesterday_beginning_of_day" },
  "end_date": { "type":"eval", "method": "end_date_yesterday_end_of_day" },
  "account": { "type":"account" }
}',
9);

-- ATA/ETA Report
SELECT merge_reports('ETA ATA Report' , 'RescueCompany', $$
  SELECT
    jobs.id AS "Swoop ID",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatch Date",
    jobs.status AS "Status",
    CONCAT(drivers.first_name, ' ', drivers.last_name) AS "Driver",
    trucks.name AS "Truck",
    accounts.name AS "Account",
    CONCAT_WS(' ', partner_dispatchers.first_name, partner_dispatchers.last_name) AS "Dispatcher",
    service_codes.name AS "Service",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatched",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 3
        AND job_id = jobs.id
      ORDER BY id DESC
      LIMIT 1
    ) as "En Route",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 4
        AND job_id = jobs.id
      ORDER BY id DESC
      LIMIT 1
    ) as "On Site",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (
        SELECT (time - created_at)
        FROM time_of_arrivals
        WHERE
          time_of_arrivals.job_id = jobs.id
          AND time_of_arrivals.eba_type IN (0, 3)
        ORDER BY id ASC
        LIMIT 1
      )
    ) / 60) AS numeric), 0) AS "ETA",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (
        SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END
        FROM (
          SELECT last_set_dttm, adjusted_dttm
          FROM audit_job_statuses
          WHERE
            job_status_id = 4
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) AS onsite_ajs
      ) -
      (
        CASE WHEN (
          SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1
        ) IS NULL THEN (
          SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END
          FROM (
            SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 9 AND job_id = jobs.id ORDER BY id DESC LIMIT 1
          ) AS created_ajs
        ) ELSE (
          SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1
        ) END
      )
    ) / 60) AS numeric), 0) AS "ATA",
    CAST((
      SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount) AS sum_amount
      FROM invoicing_line_items
      WHERE
        invoices.id = invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
     GROUP BY invoices.id) AS DECIMAL(14, 2)
    ) AS "Invoice Total"
  FROM jobs
  LEFT JOIN users ON users.id = ?
  LEFT JOIN accounts ON accounts.id = jobs.account_id
  LEFT JOIN users AS drivers ON jobs.rescue_driver_id = drivers.id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN users AS partner_dispatchers ON jobs.partner_dispatcher_id = partner_dispatchers.id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN invoicing_ledger_items AS invoices ON invoices.job_id = jobs.id AND invoices.deleted_at IS NULL AND invoices.sender_id = ? AND invoices.type='Invoice'
  WHERE
    jobs.rescue_company_id=?
    AND jobs.deleted_at IS NULL
    AND jobs.status != 'Reassigned'
    AND jobs.scheduled_for IS NULL
    AND (SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) FROM audit_job_statuses WHERE job_status_id = 2 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) >= ?
    AND (SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) FROM audit_job_statuses WHERE job_status_id = 2 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) < ?
    {{driver}}
    {{vehicle}}
    {{account}}
ORDER BY jobs.id DESC;
$$,
'{ "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "sender_id", "rescue_company_id", "start_date", "end_date", "driver", "vehicle", "account"]',
'{
  "driver": "and rescue_driver_id = ?",
  "vehicle": "and rescue_vehicle_id = ?",
  "account": "and account_id = ?"
}',
'{ "api_user.id": "api_user.id", "rescue_company_id": "api_company.id", "sender_id": "api_company.id" }',
'{
  "start_date": { "type":"eval", "method": "start_date_yesterday_beginning_of_day" },
  "end_date": { "type":"eval", "method": "end_date_yesterday_end_of_day" },
  "driver": { "type":"driver" },
  "vehicle": { "type":"eval", "method": "customize_vehicle_label" },
  "account": { "type":"account" }
}',
10,
'ETA / ATA Report');

-- ETA Variance Report Report (Copied from BMW)
-- ETA Variance Report Inserted at 433cbd
SELECT merge_reports('Fleet Managed ETA Variance Report' , 'FleetCompany', $$
SELECT
  "Job ID",
  "Date",
  "Time",
  "Site",
  "Service Requested",
  "Status",
  "Pickup City",
  "ETA",
  "ATA",
  "ATA" - "ETA" AS "ATA-ETA",
  CASE WHEN ("ATA" - "ETA") < 0 THEN 1 ELSE 0 END as "ATA-ETA < 0",
  CASE WHEN ("ATA" - "ETA") <= 10 THEN 1 ELSE 0 END as "ATA-ETA <= 10"
FROM (
  SELECT
    jobs.id AS "Job ID",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
    sites.name AS "Site",
    service_codes.name AS "Service Requested",
    jobs.status AS "Status",
    pickup.city AS "Pickup City",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (SELECT (time - created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
    ) / 60) AS NUMERIC), 0) AS "ETA",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
      (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
    ) / 60) AS NUMERIC), 0) as "ATA",
    '' as "Variance"
  FROM jobs
    LEFT JOIN users ON users.id = ?
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
    LEFT JOIN sites ON sites.id = jobs.fleet_site_id
  WHERE
    jobs.deleted_at IS NULL
    AND (
      jobs.fleet_site_id IS NULL
      OR (
        jobs.fleet_site_id = ANY (
          CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
            ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
          ELSE
            ARRAY(SELECT id FROM sites WHERE company_id = ?)
          END
        )
      )
    )
    AND jobs.status IN ('Completed','Released')
    AND jobs.scheduled_for IS NULL
    AND fleet_company_id = ?
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    {{site}}
  ORDER BY jobs.id DESC
) as t1
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "api_company.id", "start_date", "end_date", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');


-- ETA Variance Report Report (Copied from Fleet Managed ETA Variance Report) - Class Type column added
SELECT merge_reports('Enterprise Fleet Management ETA Variance Report' , 'FleetCompany', $$
SELECT
  "Job ID",
  "Date",
  "Time",
  "Site",
  "Class Type",
  "Service Requested",
  "Status",
  "Pickup City",
  "ETA",
  "ATA",
  "ATA" - "ETA" AS "ATA-ETA",
  CASE WHEN ("ATA" - "ETA") < 0 THEN 1 ELSE 0 END as "ATA-ETA < 0",
  CASE WHEN ("ATA" - "ETA") <= 10 THEN 1 ELSE 0 END as "ATA-ETA <= 10"
FROM (
  SELECT
    jobs.id AS "Job ID",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
    sites.name AS "Site",
    service_codes.name AS "Service Requested",
    jobs.status AS "Status",
    pickup.city AS "Pickup City",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (SELECT (time - created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
    ) / 60) AS NUMERIC), 0) AS "ETA",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
      (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
    ) / 60) AS NUMERIC), 0) as "ATA",
    '' as "Variance",
    vehicle_categories.name AS "Class Type"
  FROM jobs
    LEFT JOIN users ON users.id = ?
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
    LEFT JOIN sites ON sites.id = jobs.fleet_site_id
    LEFT JOIN vehicle_categories on vehicle_categories.id = jobs.invoice_vehicle_category_id
  WHERE
    jobs.deleted_at IS NULL
    AND (
      jobs.fleet_site_id IS NULL
      OR (
        jobs.fleet_site_id = ANY (
          CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
            ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
          ELSE
            ARRAY(SELECT id FROM sites WHERE company_id = ?)
          END
        )
      )
    )
    AND jobs.status IN ('Completed','Released')
    AND jobs.scheduled_for IS NULL
    AND fleet_company_id = ?
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    {{site}}
  ORDER BY jobs.id DESC
) as t1
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "api_company.id", "start_date", "end_date", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');


-- ETA Variance Report Report (Copied from Fleet Managed ETA Variance Report) - Class Type column added
SELECT merge_reports('Fleet Response ETA Variance Report' , 'FleetCompany', $$
SELECT
  "Job ID",
  "Date",
  "Time",
  "Site",
  "Class Type",
  "Service Requested",
  "Status",
  "Pickup City",
  "ETA",
  "ATA",
  "ATA" - "ETA" AS "ATA-ETA",
  CASE WHEN ("ATA" - "ETA") < 0 THEN 1 ELSE 0 END as "ATA-ETA < 0",
  CASE WHEN ("ATA" - "ETA") <= 10 THEN 1 ELSE 0 END as "ATA-ETA <= 10"
FROM (
  SELECT
    jobs.id AS "Job ID",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
    sites.name AS "Site",
    service_codes.name AS "Service Requested",
    jobs.status AS "Status",
    pickup.city AS "Pickup City",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (SELECT (time - created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
    ) / 60) AS NUMERIC), 0) AS "ETA",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
      (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
    ) / 60) AS NUMERIC), 0) as "ATA",
    '' as "Variance",
    vehicle_categories.name AS "Class Type"
  FROM jobs
    LEFT JOIN users ON users.id = ?
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
    LEFT JOIN sites ON sites.id = jobs.fleet_site_id
    LEFT JOIN vehicle_categories on vehicle_categories.id = jobs.invoice_vehicle_category_id
  WHERE
    jobs.deleted_at IS NULL
    AND (
      jobs.fleet_site_id IS NULL
      OR (
        jobs.fleet_site_id = ANY (
          CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
            ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
          ELSE
            ARRAY(SELECT id FROM sites WHERE company_id = ?)
          END
        )
      )
    )
    AND jobs.status IN ('Completed','Released')
    AND jobs.scheduled_for IS NULL
    AND fleet_company_id = ?
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    {{site}}
  ORDER BY jobs.id DESC
) as t1
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "api_company.id", "start_date", "end_date", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');




-- Activity Report Report (copied from BMW Report)
SELECT merge_reports('Fleet Managed Activity Report', 'FleetCompany', $$
SELECT
  DISTINCT(jobs.id),
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
  jobs.email AS "Customer Email",
  sites.name AS "Site",
  pickup.address AS "Pickup Address",
  dropoff.address AS "Drop off Address",
  vehicles.make AS "Make",
  vehicles.model AS "Model",
  vehicles.year AS "Year",
  vehicles.vin AS "VIN",
  service_codes.name AS "Service Requested",
  symptoms.name AS "Symptom",
  partner.name AS "Partner",
  (CASE WHEN issues.key = 'redispatched' THEN 'Y' ELSE 'N' END) AS "Redispatched",
  vehicle_categories.name AS "Truck Type",
  jobs.status AS "Status",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 10
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Assigned",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 11
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Accepted",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 2
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Dispatched",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 3
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "En Route",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 4
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "On Site",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 6
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Tow Destination",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 7
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Completed",
  ROUND(estimates.meters_ab * 0.000621371, 1) AS "EN Route Miles",
  ROUND(estimates.meters_bc * 0.000621371, 1) AS "Pick Up To Drop Off",
  ROUND((CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_ca ELSE estimates.meters_ba END) * 0.000621371, 1) AS "Return Miles",
  ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) as "P2P Miles"
FROM jobs
  LEFT JOIN locations as pickup ON pickup.id = jobs.service_location_id
  LEFT JOIN locations as dropoff ON dropoff.id = jobs.drop_location_id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN locations policy_addr ON policy_addr.id = jobs.policy_location_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN symptoms ON jobs.symptom_id = symptoms.id
  LEFT JOIN companies partner ON partner.id = jobs.rescue_company_id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN vehicle_categories ON trucks.vehicle_category_id = vehicle_categories.id
  LEFT JOIN explanations ON explanations.job_id = jobs.id
  LEFT JOIN reasons ON reasons.id = explanations.reason_id
  LEFT JOIN issues ON issues.id = reasons.issue_id
  LEFT JOIN sites ON sites.id = jobs.fleet_site_id
  LEFT JOIN users ON users.id = ?
WHERE
  jobs.deleted_at IS NULL
  AND (
    jobs.fleet_site_id IS NULL
    OR (
      jobs.fleet_site_id = ANY (
        CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
          ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
        ELSE
          ARRAY(SELECT id FROM sites WHERE company_id = ?)
        END
      )
    )
  )
  AND jobs.status IN ('Completed', 'Released', 'Canceled', 'GOA')
  AND jobs.fleet_company_id = ?
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
  {{site}}
ORDER BY jobs.id DESC;
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "api_company.id", "start_date", "end_date", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');




SELECT merge_reports('Turo Activity Report', 'FleetCompany', $$
SELECT
  DISTINCT(jobs.id),
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
  jobs.email AS "Customer Email",
  jobs.ref_number AS "Reservation Number",
  sites.name AS "Site",
  pickup.address AS "Pickup Address",
  dropoff.address AS "Drop off Address",
  vehicles.make AS "Make",
  vehicles.model AS "Model",
  vehicles.year AS "Year",
  vehicles.vin AS "VIN",
  service_codes.name AS "Service Requested",
  symptoms.name AS "Symptom",
  partner.name AS "Partner",
  (CASE WHEN issues.key = 'redispatched' THEN 'Y' ELSE 'N' END) AS "Redispatched",
  vehicle_categories.name AS "Truck Type",
  jobs.status AS "Status",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 10
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Assigned",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 11
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Accepted",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 2
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Dispatched",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 3
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "En Route",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 4
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "On Site",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 6
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Tow Destination",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 7
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Completed",
  ROUND(estimates.meters_ab * 0.000621371, 1) AS "EN Route Miles",
  ROUND(estimates.meters_bc * 0.000621371, 1) AS "Pick Up To Drop Off",
  ROUND((CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_ca ELSE estimates.meters_ba END) * 0.000621371, 1) AS "Return Miles",
  ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) as "P2P Miles"
FROM jobs
  LEFT JOIN locations as pickup ON pickup.id = jobs.service_location_id
  LEFT JOIN locations as dropoff ON dropoff.id = jobs.drop_location_id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN locations policy_addr ON policy_addr.id = jobs.policy_location_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN symptoms ON jobs.symptom_id = symptoms.id
  LEFT JOIN companies partner ON partner.id = jobs.rescue_company_id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN vehicle_categories ON trucks.vehicle_category_id = vehicle_categories.id
  LEFT JOIN explanations ON explanations.job_id = jobs.id
  LEFT JOIN reasons ON reasons.id = explanations.reason_id
  LEFT JOIN issues ON issues.id = reasons.issue_id
  LEFT JOIN sites ON sites.id = jobs.fleet_site_id
  LEFT JOIN users ON users.id = ?
WHERE
  jobs.deleted_at IS NULL
  AND (
    jobs.fleet_site_id IS NULL
    OR (
      jobs.fleet_site_id = ANY (
        CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
          ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
        ELSE
          ARRAY(SELECT id FROM sites WHERE company_id = (SELECT id FROM companies WHERE name = 'Turo'))
        END
      )
    )
  )
  AND jobs.status IN ('Completed', 'Released', 'Canceled', 'GOA')
  AND jobs.fleet_company_id=(SELECT id FROM companies WHERE name = 'Turo')
  AND jobs.created_at > ?
  AND jobs.created_at < ?
  {{site}}
ORDER BY jobs.id DESC;
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "start_date", "end_date", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');




-- Activity Report Report (copied from BMW Report)
SELECT merge_reports('Fleet Response Activity Report', 'FleetCompany', $$
SELECT
  DISTINCT(jobs.id),
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
  jobs.email AS "Customer Email",
  sites.name AS "Site",
  class_type.name AS "Class Type",
  pickup.address AS "Pickup Address",
  dropoff.address AS "Drop off Address",
  vehicles.make AS "Make",
  vehicles.model AS "Model",
  vehicles.year AS "Year",
  vehicles.vin AS "VIN",
  service_codes.name AS "Service Requested",
  symptoms.name AS "Symptom",
  partner.name AS "Partner",
  (CASE WHEN issues.key = 'redispatched' THEN 'Y' ELSE 'N' END) AS "Redispatched",
  vehicle_categories.name AS "Truck Type",
  jobs.status AS "Status",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 10
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Assigned",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 11
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Accepted",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 2
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Dispatched",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 3
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "En Route",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 4
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "On Site",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 6
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Tow Destination",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 7
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Completed",
  ROUND(estimates.meters_ab * 0.000621371, 1) AS "EN Route Miles",
  ROUND(estimates.meters_bc * 0.000621371, 1) AS "Pick Up To Drop Off",
  ROUND((CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_ca ELSE estimates.meters_ba END) * 0.000621371, 1) AS "Return Miles",
  ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) as "P2P Miles"
FROM jobs
  LEFT JOIN locations as pickup ON pickup.id = jobs.service_location_id
  LEFT JOIN locations as dropoff ON dropoff.id = jobs.drop_location_id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN locations policy_addr ON policy_addr.id = jobs.policy_location_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN symptoms ON jobs.symptom_id = symptoms.id
  LEFT JOIN companies partner ON partner.id = jobs.rescue_company_id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN vehicle_categories ON trucks.vehicle_category_id = vehicle_categories.id
  LEFT JOIN vehicle_categories class_type on class_type.id = jobs.invoice_vehicle_category_id
  LEFT JOIN explanations ON explanations.job_id = jobs.id
  LEFT JOIN reasons ON reasons.id = explanations.reason_id
  LEFT JOIN issues ON issues.id = reasons.issue_id
  LEFT JOIN sites ON sites.id = jobs.fleet_site_id
  LEFT JOIN users ON users.id = ?
WHERE
  jobs.deleted_at IS NULL
  AND (
    jobs.fleet_site_id IS NULL
    OR (
      jobs.fleet_site_id = ANY (
        CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
          ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
        ELSE
          ARRAY(SELECT id FROM sites WHERE company_id = ?)
        END
      )
    )
  )
  AND jobs.status IN ('Completed', 'Released', 'Canceled', 'GOA')
  AND jobs.fleet_company_id = ?
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
  {{site}}
ORDER BY jobs.id DESC;
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "api_company.id", "start_date", "end_date", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');

-- Activity Report Report (copied from BMW Report)
SELECT merge_reports('Enterprise Fleet Management Activity Report', 'FleetCompany', $$
SELECT
  DISTINCT(jobs.id),
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
  jobs.email AS "Customer Email",
  sites.name AS "Site",
  class_type.name AS "Class Type",
  pickup.address AS "Pickup Address",
  dropoff.address AS "Drop off Address",
  vehicles.make AS "Make",
  vehicles.model AS "Model",
  vehicles.year AS "Year",
  vehicles.vin AS "VIN",
  service_codes.name AS "Service Requested",
  symptoms.name AS "Symptom",
  partner.name AS "Partner",
  (CASE WHEN issues.key = 'redispatched' THEN 'Y' ELSE 'N' END) AS "Redispatched",
  vehicle_categories.name AS "Truck Type",
  jobs.status AS "Status",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 10
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Assigned",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 11
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Accepted",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 2
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Dispatched",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 3
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "En Route",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 4
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "On Site",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 6
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Tow Destination",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 7
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Completed",
  ROUND(estimates.meters_ab * 0.000621371, 1) AS "EN Route Miles",
  ROUND(estimates.meters_bc * 0.000621371, 1) AS "Pick Up To Drop Off",
  ROUND((CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_ca ELSE estimates.meters_ba END) * 0.000621371, 1) AS "Return Miles",
  ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) as "P2P Miles"
FROM jobs
  LEFT JOIN locations as pickup ON pickup.id = jobs.service_location_id
  LEFT JOIN locations as dropoff ON dropoff.id = jobs.drop_location_id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN locations policy_addr ON policy_addr.id = jobs.policy_location_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN symptoms ON jobs.symptom_id = symptoms.id
  LEFT JOIN companies partner ON partner.id = jobs.rescue_company_id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN vehicle_categories ON trucks.vehicle_category_id = vehicle_categories.id
  LEFT JOIN vehicle_categories class_type on class_type.id = jobs.invoice_vehicle_category_id
  LEFT JOIN explanations ON explanations.job_id = jobs.id
  LEFT JOIN reasons ON reasons.id = explanations.reason_id
  LEFT JOIN issues ON issues.id = reasons.issue_id
  LEFT JOIN sites ON sites.id = jobs.fleet_site_id
  LEFT JOIN users ON users.id = ?
WHERE
  jobs.deleted_at IS NULL
  AND (
    jobs.fleet_site_id IS NULL
    OR (
      jobs.fleet_site_id = ANY (
        CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
          ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
        ELSE
          ARRAY(SELECT id FROM sites WHERE company_id = ?)
        END
      )
    )
  )
  AND jobs.status IN ('Completed', 'Released', 'Canceled', 'GOA')
  AND jobs.fleet_company_id = ?
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
  {{site}}
ORDER BY jobs.id DESC;
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "api_company.id", "start_date", "end_date", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');







-- Fleet Managed CSAT Report (copied from BMW)
-- Fleet Managed CSAT Report Inserted at 447047
-- Fleet Managed CSAT Report Updated to ac05f4 - remove BMW specific questions
-- Fleet Managed CSAT Report Updated to 5a8e92 - remove hardcoded Bosh ref and fleet dispatcher
-- Fleet Managed CSAT Report Updated to 43dae3 - Added 5* review
SELECT merge_reports('Fleet Managed CSAT Report' , 'FleetCompany', $$
  SELECT
    jobs.id AS "Dispatch ID",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
    sites.name AS "Site",
    CASE WHEN (pickup.state IS NOT NULL) THEN pickup.state ELSE (CASE WHEN original_pickup.state IS NOT NULL THEN original_pickup.state ELSE (provider_location.state) END) END AS "State",
    CONCAT(customer.first_name, CONCAT(' ', customer.last_name)) AS "Customer",
    customer.phone AS "Phone",
    jobs.status AS "Status",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN 'Scheduled' ELSE NULL END AS "Scheduled",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC limit 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ETA",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
        (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ATA",
    (CASE WHEN (reviews.type = 'Reviews::Typeform' AND reviews.branding IS TRUE) THEN 'Branded Link' ELSE
      (CASE WHEN (reviews.type = 'Reviews::Typeform') THEN 'Web Link' ELSE
        (CASE WHEN (reviews.type = 'NpsReview') THEN 'SMS' ELSE NULL END)
      END)
    END) AS "Review Type",
/*    fleet_dispatchers.email AS "Fleet Dispatcher", */
    reviews.rating as "5* Rating",
    reviews.nps_question1 AS "Question 1",
    reviews.nps_question2 AS "Question 2",
    reviews.nps_question3 AS "Question 3",
    reviews.nps_question4 AS "Question 4",
    CASE
    WHEN (reviews.type = 'Reviews::Typeform') THEN reviews.nps_feedback
    WHEN (reviews.type = 'Reviews::SurveyReview') THEN reviews.nps_feedback
    ELSE (CASE WHEN (reviews.type = 'NpsReview') THEN
      (SELECT string_agg(body, ', ')
      FROM (
       SELECT body
       FROM reviews
       LEFT JOIN twilio_replies ON reviews.id = twilio_replies.target_id AND twilio_replies.target_type = 'Review'
       WHERE reviews.job_id = jobs.id
       ORDER BY twilio_replies.id
      ) AS reply_bodies)
    ELSE NULL END) END AS "Feedback/Comments",
    TO_CHAR(audit_sms.clicked_dttm AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Link Clicked"
  FROM jobs
    JOIN reviews ON jobs.id = reviews.job_id
    LEFT JOIN users ON users.id = ?
    LEFT JOIN drives ON drives.id = jobs.driver_id
    LEFT JOIN users customer ON customer.id = drives.user_id
    LEFT JOIN users fleet_dispatchers ON jobs.fleet_dispatcher_id = fleet_dispatchers.id
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN locations AS original_pickup ON original_pickup.id = jobs.original_service_location_id
    LEFT JOIN companies provider ON jobs.rescue_company_id = provider.id
    LEFT JOIN locations provider_location ON provider.location_id = provider_location.id
    LEFT JOIN jobs parent ON jobs.parent_id = parent.id
    LEFT JOIN sites ON sites.id = jobs.fleet_site_id
    LEFT JOIN audit_sms ON audit_sms.job_id = jobs.id AND audit_sms.type = 'Sms::BrandedReviewLink'
  WHERE
    jobs.deleted_at IS NULL
    AND (
      jobs.fleet_site_id IS NULL
      OR (
        jobs.fleet_site_id = ANY (
          CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
            ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
          ELSE
            ARRAY(SELECT id FROM sites WHERE company_id = ?)
          END
        )
      )
    )
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    AND jobs.fleet_company_id = ?
    AND jobs.status != 'GOA'
    AND jobs.status != 'Canceled'
    {{site}}
  ORDER BY reviews.created_at DESC;
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "start_date", "end_date", "api_company.id", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');

-- Fleet Response CSAT Report (copied from Fleet Managed CSAT Report)
SELECT merge_reports('Fleet Response CSAT Report' , 'FleetCompany', $$
  SELECT
    jobs.id AS "Dispatch ID",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
    sites.name AS "Site",
    vehicle_categories.name AS "Class Type",
    CASE WHEN (pickup.state IS NOT NULL) THEN pickup.state ELSE (CASE WHEN original_pickup.state IS NOT NULL THEN original_pickup.state ELSE (provider_location.state) END) END AS "State",
    CONCAT(customer.first_name, CONCAT(' ', customer.last_name)) AS "Customer",
    customer.phone AS "Phone",
    jobs.status AS "Status",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN 'Scheduled' ELSE NULL END AS "Scheduled",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC limit 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ETA",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
        (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ATA",
    (CASE WHEN (reviews.type = 'Reviews::Typeform' AND reviews.branding IS TRUE) THEN 'Branded Link' ELSE
      (CASE WHEN (reviews.type = 'Reviews::Typeform') THEN 'Web Link' ELSE
        (CASE WHEN (reviews.type = 'NpsReview') THEN 'SMS' ELSE NULL END)
      END)
    END) AS "Review Type",
/*    fleet_dispatchers.email AS "Fleet Dispatcher", */
    reviews.rating as "5* Rating",
    reviews.nps_question1 AS "Question 1",
    reviews.nps_question2 AS "Question 2",
    reviews.nps_question3 AS "Question 3",
    reviews.nps_question4 AS "Question 4",
    CASE
    WHEN (reviews.type = 'Reviews::Typeform') THEN reviews.nps_feedback
    WHEN (reviews.type = 'Reviews::SurveyReview') THEN reviews.nps_feedback
    ELSE (CASE WHEN (reviews.type = 'NpsReview') THEN
      (SELECT string_agg(body, ', ')
      FROM (
       SELECT body
       FROM reviews
       LEFT JOIN twilio_replies ON reviews.id = twilio_replies.target_id AND twilio_replies.target_type = 'Review'
       WHERE reviews.job_id = jobs.id
       ORDER BY twilio_replies.id
      ) AS reply_bodies)
    ELSE NULL END) END AS "Feedback/Comments",
    TO_CHAR(audit_sms.clicked_dttm AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Link Clicked"
  FROM jobs
    JOIN reviews ON jobs.id = reviews.job_id
    LEFT JOIN users ON users.id = ?
    LEFT JOIN drives ON drives.id = jobs.driver_id
    LEFT JOIN users customer ON customer.id = drives.user_id
    LEFT JOIN users fleet_dispatchers ON jobs.fleet_dispatcher_id = fleet_dispatchers.id
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN locations AS original_pickup ON original_pickup.id = jobs.original_service_location_id
    LEFT JOIN companies provider ON jobs.rescue_company_id = provider.id
    LEFT JOIN locations provider_location ON provider.location_id = provider_location.id
    LEFT JOIN jobs parent ON jobs.parent_id = parent.id
    LEFT JOIN sites ON sites.id = jobs.fleet_site_id
    LEFT JOIN audit_sms ON audit_sms.job_id = jobs.id AND audit_sms.type = 'Sms::BrandedReviewLink'
    LEFT JOIN vehicle_categories on vehicle_categories.id = jobs.invoice_vehicle_category_id
  WHERE
    jobs.deleted_at IS NULL
    AND (
      jobs.fleet_site_id IS NULL
      OR (
        jobs.fleet_site_id = ANY (
          CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
            ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
          ELSE
            ARRAY(SELECT id FROM sites WHERE company_id = ?)
          END
        )
      )
    )
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    AND jobs.fleet_company_id = ?
    AND jobs.status != 'GOA'
    AND jobs.status != 'Canceled'
    {{site}}
  ORDER BY reviews.created_at DESC;
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "start_date", "end_date", "api_company.id", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');


-- Fleet Response CSAT Report (copied from Fleet Managed CSAT Report)
SELECT merge_reports('Enterprise Fleet Management CSAT Report' , 'FleetCompany', $$
  SELECT
    jobs.id AS "Dispatch ID",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
    sites.name AS "Site",
    vehicle_categories.name AS "Class Type",
    CASE WHEN (pickup.state IS NOT NULL) THEN pickup.state ELSE (CASE WHEN original_pickup.state IS NOT NULL THEN original_pickup.state ELSE (provider_location.state) END) END AS "State",
    CONCAT(customer.first_name, CONCAT(' ', customer.last_name)) AS "Customer",
    customer.phone AS "Phone",
    jobs.status AS "Status",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN 'Scheduled' ELSE NULL END AS "Scheduled",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC limit 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ETA",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
        (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ATA",
    (CASE WHEN (reviews.type = 'Reviews::Typeform' AND reviews.branding IS TRUE) THEN 'Branded Link' ELSE
      (CASE WHEN (reviews.type = 'Reviews::Typeform') THEN 'Web Link' ELSE
        (CASE WHEN (reviews.type = 'NpsReview') THEN 'SMS' ELSE NULL END)
      END)
    END) AS "Review Type",
/*    fleet_dispatchers.email AS "Fleet Dispatcher", */
    reviews.rating as "5* Rating",
    reviews.nps_question1 AS "Question 1",
    reviews.nps_question2 AS "Question 2",
    reviews.nps_question3 AS "Question 3",
    reviews.nps_question4 AS "Question 4",
    CASE
    WHEN (reviews.type = 'Reviews::Typeform') THEN reviews.nps_feedback
    WHEN (reviews.type = 'Reviews::SurveyReview') THEN reviews.nps_feedback
    ELSE (CASE WHEN (reviews.type = 'NpsReview') THEN
      (SELECT string_agg(body, ', ')
      FROM (
       SELECT body
       FROM reviews
       LEFT JOIN twilio_replies ON reviews.id = twilio_replies.target_id AND twilio_replies.target_type = 'Review'
       WHERE reviews.job_id = jobs.id
       ORDER BY twilio_replies.id
      ) AS reply_bodies)
    ELSE NULL END) END AS "Feedback/Comments",
    TO_CHAR(audit_sms.clicked_dttm AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Link Clicked"
  FROM jobs
    JOIN reviews ON jobs.id = reviews.job_id
    LEFT JOIN users ON users.id = ?
    LEFT JOIN drives ON drives.id = jobs.driver_id
    LEFT JOIN users customer ON customer.id = drives.user_id
    LEFT JOIN users fleet_dispatchers ON jobs.fleet_dispatcher_id = fleet_dispatchers.id
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN locations AS original_pickup ON original_pickup.id = jobs.original_service_location_id
    LEFT JOIN companies provider ON jobs.rescue_company_id = provider.id
    LEFT JOIN locations provider_location ON provider.location_id = provider_location.id
    LEFT JOIN jobs parent ON jobs.parent_id = parent.id
    LEFT JOIN sites ON sites.id = jobs.fleet_site_id
    LEFT JOIN audit_sms ON audit_sms.job_id = jobs.id AND audit_sms.type = 'Sms::BrandedReviewLink'
    LEFT JOIN vehicle_categories on vehicle_categories.id = jobs.invoice_vehicle_category_id
  WHERE
    jobs.deleted_at IS NULL
    AND (
      jobs.fleet_site_id IS NULL
      OR (
        jobs.fleet_site_id = ANY (
          CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
            ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
          ELSE
            ARRAY(SELECT id FROM sites WHERE company_id = ?)
          END
        )
      )
    )
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    AND jobs.fleet_company_id = ?
    AND jobs.status != 'GOA'
    AND jobs.status != 'Canceled'
    {{site}}
  ORDER BY reviews.created_at DESC;
$$,
'{ "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_user.id": "api_user.id", "api_company.id": "api_company.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "api_user.id", "api_user.id", "api_company.id", "start_date", "end_date", "api_company.id", "site" ]',
'{ "site": "AND jobs.fleet_site_id = ?" }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }, "site": { "type": "eval", "method": "user_sites" } }');




-- Partner Commission Report
-- Commission Report Inserted at dc3fc9
-- Commission Report Updated to 6009aa - column titles
SELECT merge_reports('Commission Report' , 'RescueCompany', $$
SELECT    accounts.NAME                                                            AS "Account",
          jobs.id                                                                  AS "ID",
          Date((jobs.created_at )::timestamptz at time zone COALESCE(users.timezone, 'America/Los_Angeles')) AS "Date",
          driver.first_name
                    || ' '
                    || driver.last_name AS "Driver",
          COALESCE(COALESCE( ( Sum(DISTINCT (Cast(Floor(COALESCE(invoicing_line_items.net_amount ,0)*(1000000*1.0)) AS DECIMAL(65,0))) + ('x'
                    || Md5(invoicing_line_items.id ::                                                                  varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "Invoice Amt (ex. Storage, Fuel)",
          COALESCE(COALESCE( ( sum(DISTINCT (cast(floor(COALESCE(invoicing_line_items.net_amount*(.3) ,0)*(1000000*1.0)) AS decimal(65,0))) + ('x'
                    || md5(invoicing_line_items.id ::                                                                       varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "Commission(30%)",
          COALESCE(COALESCE( ( sum(DISTINCT (cast(floor(COALESCE(invoicing_line_items.tax_amount ,0)*(1000000*1.0)) AS decimal(65,0))) + ('x'
                    || md5(invoicing_line_items.id ::                                                                  varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0) ) - sum(DISTINCT ('x'
                    || md5(invoicing_line_items.id ::varchar))::bit(64)::bigint::decimal(65,0) *18446744073709551616 + ('x'
                    || substr(md5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::decimal(65,0)) ) / (1000000*1.0), 0), 0) AS "Tax Amount"
FROM      PUBLIC.jobs                                                                                                                AS jobs
LEFT JOIN PUBLIC.accounts                                                                                                            AS accounts
ON        jobs.account_id = accounts.id
LEFT JOIN PUBLIC.invoicing_ledger_items AS invoicing_ledger_items
ON        jobs.id = invoicing_ledger_items.job_id
AND       invoicing_ledger_items.sender_id = jobs.rescue_company_id
AND       invoicing_ledger_items.deleted_at IS NULL
AND       invoicing_ledger_items.type='Invoice'
LEFT JOIN PUBLIC.invoicing_line_items AS invoicing_line_items
ON        invoicing_ledger_items.id = invoicing_line_items.ledger_item_id
AND       invoicing_line_items.deleted_at IS NULL
LEFT JOIN PUBLIC.companies AS rescue_company
ON        jobs.rescue_company_id = rescue_company.id
LEFT JOIN PUBLIC.users AS driver
ON        jobs.rescue_driver_id = driver.id
LEFT JOIN users ON users.id = ?
WHERE     ((((
                                                  jobs.created_at ) >= ?
                              AND       (
                                                  jobs.created_at ) < ? )))
AND       (
                    jobs.status = 'Completed'
          OR        jobs.status = 'GOA'
          OR        jobs.status = 'Released')
AND       (
                    invoicing_line_items.description NOT LIKE '%storage%'
          AND       invoicing_line_items.description NOT LIKE '%fuel%'
          OR        invoicing_line_items.description IS NULL)
AND       (
                    rescue_company.id = ?)
GROUP BY  1,
          2,
          3,
          4
ORDER BY  4
$$,
'{ "api_user.id": "api_user.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "start_date", "end_date", "api_company.id"]',
'{}',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" }}');

-- USAA CSAT Report
-- Copied from BMW CSAT Report
SELECT merge_reports('USAA CSAT' , 'FleetCompany', $$
  SELECT
    jobs.id AS "Dispatch ID",
    to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
    to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
    jobs.policy_number    as "Policy Number",
    CASE WHEN (pickup.state IS NOT NULL) THEN pickup.state ELSE (CASE WHEN original_pickup.state IS NOT NULL THEN original_pickup.state ELSE (provider_location.state) END) END AS "State",
    CONCAT(customer.first_name, CONCAT(' ', customer.last_name)) AS "Customer",
    customer.phone AS "Phone",
    jobs.status AS "Status",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN 'Scheduled' ELSE NULL END AS "Scheduled",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC limit 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ETA",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
        (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ATA",
    'Web Link'  AS "Review Type",
    fleet_dispatchers.email as "Fleet Dispatcher",
    TO_CHAR(audit_sms.clicked_dttm AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'US/Central'), 'MM/DD/YYYY HH24:MI') AS "Link Clicked",
    (
      SELECT int_value
      FROM survey_results
      JOIN survey_questions ON survey_results.survey_question_id = survey_questions.id
      WHERE
        survey_questions.order=1
        AND survey_results.job_id = jobs.id
        AND survey_questions.survey_id = reviews.survey_id
      ORDER BY survey_results.created_at DESC
      LIMIT 1
    ) AS "Question 1 - Phone Rep",
    (
      SELECT int_value
      FROM survey_results
      JOIN survey_questions ON survey_results.survey_question_id = survey_questions.id
      WHERE
        survey_questions.order=2
        AND survey_results.job_id = jobs.id
        AND survey_questions.survey_id = reviews.survey_id
      ORDER BY survey_results.created_at DESC
      LIMIT 1
    ) AS "Question 2 - Provider Courtesy",
    (
      SELECT int_value
      FROM survey_results
      JOIN survey_questions ON survey_results.survey_question_id = survey_questions.id
      WHERE
        survey_questions.order=3
        AND survey_results.job_id = jobs.id
        AND survey_questions.survey_id = reviews.survey_id
      ORDER BY survey_results.created_at DESC
      LIMIT 1
    ) AS "Question 3 - Satisfaction",
    (
      SELECT string_value
      FROM survey_results JOIN survey_questions ON survey_results.survey_question_id = survey_questions.id
      WHERE
        survey_questions.order=4
        AND survey_results.job_id = jobs.id
        AND survey_questions.survey_id = reviews.survey_id
      ORDER BY survey_results.created_at DESC
      LIMIT 1
    ) AS "Question 4 - Feedback/Comments",
    (
      SELECT
        STRING_AGG(issues.name, E'\n')
      FROM explanations
        JOIN reasons ON reasons.id = explanations.reason_id
        JOIN issues ON issues.id = reasons.issue_id
      WHERE explanations.job_id = jobs.id
      GROUP BY explanations.job_id
    ) AS "Issues",
    (
      SELECT
        STRING_AGG(CASE WHEN explanations.reason_info IS NOT NULL THEN CONCAT(reasons.name, ': ', explanations.reason_info) ELSE reasons.name END, E'\n')
      FROM explanations
        JOIN reasons ON reasons.id = explanations.reason_id
        JOIN issues ON issues.id = reasons.issue_id
      WHERE explanations.job_id = jobs.id
      GROUP BY explanations.job_id
    ) AS "Reasons"
  FROM jobs
    JOIN reviews ON jobs.id = reviews.job_id
    LEFT JOIN users ON users.id = ?
    LEFT JOIN drives ON drives.id = jobs.driver_id
    LEFT JOIN users customer ON customer.id = drives.user_id
    LEFT JOIN users fleet_dispatchers ON jobs.fleet_dispatcher_id = fleet_dispatchers.id
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN locations AS original_pickup ON original_pickup.id = jobs.original_service_location_id
    LEFT JOIN companies provider ON jobs.rescue_company_id = provider.id
    LEFT JOIN locations provider_location ON provider.location_id = provider_location.id
    LEFT JOIN jobs parent ON jobs.parent_id = parent.id
    LEFT JOIN audit_sms ON audit_sms.job_id = jobs.id AND audit_sms.type = 'Sms::BrandedReviewLink'
  WHERE
    jobs.deleted_at IS NULL
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    AND jobs.fleet_company_id = (SELECT id FROM companies WHERE name = 'USAA')
    AND jobs.status != 'GOA'
    AND jobs.status != 'Canceled'
  ORDER BY reviews.created_at DESC;
$$,
'{ "api_user.id": "api_user.id", "start_date": "2000-01-01", "end_date": "2050-09-01" }',
'[ "api_user.id", "start_date", "end_date", "site" ]',
'{ }',
'{ "api_user.id": "api_user.id", "api_company.id": "api_company.id" }',
'{ "start_date": { "type": "start_date" }, "end_date": { "type": "end_date" } }');










-- PDW: took looker report and removed / 86400.0 from onsite to completed

SELECT merge_reports('USAA Activity Report' , 'FleetCompany', $$


WITH audit_job_status_latest_pivot AS (SELECT *
      FROM crosstab(
            '
      WITH  status_name as ( SELECT id, name FROM job_statuses group by 1 ),
      audit_job_status_latest as (
            select job_id, job_status_id, status_name.name, MAX(audit_job_statuses.id) as max_id
            FROM status_name
            LEFT JOIN public.audit_job_statuses on audit_job_statuses.job_status_id = status_name.id
            GROUP BY 1, 2, 3
            order by 4 DESC ),

      audit_2 as (SELECT audit_job_statuses.*, audit_job_status_latest.name as status_name
            , CASE WHEN adjusted_dttm is not null THEN adjusted_dttm      ELSE last_set_dttm END as dtmm
            FROM audit_job_statuses
            LEFT JOIN audit_job_status_latest ON max_id = audit_job_statuses.id
             where deleted_at is null
      )

      SELECT job_id, status_name, dtmm FROM audit_2 ORDER BY 1,2 '
            , 'VALUES (''Pending''), (''Dispatched''), (''En Route''), (''On Site''), (''Towing''), (''Tow Destination''), (''Completed''), (''Canceled''), (''Created''), (''Assigned''), (''Accepted''), (''Rejected''), (''Reassign''), (''Initial''), (''Unassigned''), (''Reassigned''), (''Stored''), (''Released''), (''Submitted''), (''ETA Rejected''), (''GOA''), (''Expired''), (''Auto Assigning'')')
      as dttm ("job_id" int, "pending" timestamp, "dispatched" timestamp, "en_route" timestamp, "on_site" timestamp, "towing" timestamp, "tow_Destination" timestamp, "completed" timestamp, "canceled" timestamp, "created" timestamp, "assigned" timestamp, "accepted" timestamp, "rejected" timestamp, "reassign" timestamp, "initial" timestamp, "unassigned" timestamp, "reassigned" timestamp, "stored" timestamp, "released" timestamp, "submitted" timestamp, "eta_rejected" timestamp, "goa" timestamp, "expired" timestamp, "auto_assigning" text)
       )
  ,  survey_scores AS (SELECT *
FROM crosstab( 'select distinct on (job_id,survey_questions.order) job_id, survey_questions.order, int_value from survey_results left join survey_questions on survey_results.survey_question_id=survey_questions.id order by job_id,survey_questions.order,survey_results.id DESC')
     AS final_result(job_id integer, "q1" integer,"q2"  integer, "q3" integer,"q4" integer,"q5" integer,"q6" integer)
       )
SELECT
  jobs.id  AS "Swoop ID",
  TO_CHAR((audit_job_status_latest_pivot.created )::timestamptz AT TIME ZONE 'US/Central', 'YYYY-MM-DD HH24:MI:SS') AS "Created (Central)",
  jobs.policy_number  AS "Policy Number",
  customer.first_name || ' ' || customer.last_name  AS "Customer Name",
  customer.phone  AS "Customer Phone",
  pickup.state  AS "Pickup State",
  pickup.city  AS "Pickup City",
  pickup.zip  AS "Pickup Zip",
        pickup_location_type.name as "Location Type",
  COALESCE(estimates.meters_bc,0)*0.000621371  AS "Pickup To Drop Off Mileage",
  vehicles.make  AS "Vehicle Make",
  vehicles.model  AS "Vehicle Model",
  vehicles.year  AS "Vehicle Year",
  service_codes.name  AS "Service Type",
  symptoms.name  AS "Symptom",
  jobs.status  AS "Job Status",
  rescue_company.name  AS "Service Provider",
  CASE WHEN (select case when count(*) > 0 then true else false end as cnt from explanations
left join reasons on explanations.reason_id=reasons.id
where
  job_id = jobs.id
  and issue_id=(select id from issues where key = 'redispatched'))  THEN 'Yes' ELSE 'No' END
 AS "Re-Dispatched",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0)  AS "Partner ETA",
  round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0)  AS "Partner ATA",
TO_CHAR((
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1) -
    (
      WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs
          WHERE inner_jobs.id = jobs.id
        UNION
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs, parents
          WHERE inner_jobs.id = parents.parent_id
      )
      SELECT adjusted_created_at
      FROM parents
      ORDER BY id ASC
      LIMIT 1
    )
  ),'HH24:MI:SS') as "Handle Time",
TO_CHAR
((
(select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=7 and job_id=jobs.id order by id desc limit 1) as completed_ajs)
-
(select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs)
)::interval,
'HH24:MI:SS')
 AS "Service Time",
  survey_scores.q1  AS "Q1 Customer Review",
  survey_scores.q2  AS "Q2 Customer Review",
  survey_scores.q3  AS "Q3 Customer Review"
FROM public.jobs  AS jobs
LEFT JOIN public.companies  AS fleet_companies ON jobs.fleet_company_id = fleet_companies.id
LEFT JOIN public.companies  AS rescue_company ON jobs.rescue_company_id = rescue_company.id
LEFT JOIN public.locations  AS pickup ON jobs.service_location_id = pickup.id
left join location_types as pickup_location_type on pickup_location_type.id = pickup.location_type_id
LEFT JOIN public.service_codes  AS service_codes ON jobs.service_code_id = service_codes.id
LEFT JOIN public.estimates  AS estimates ON jobs.estimate_id = estimates.id
LEFT JOIN public.drives  AS drives ON jobs.driver_id = drives.id
LEFT JOIN public.vehicles  AS vehicles ON drives.vehicle_id = vehicles.id
LEFT JOIN public.symptoms  AS symptoms ON jobs.symptom_id = symptoms.id
LEFT JOIN public.users  AS customer ON drives.user_id = customer.id
LEFT JOIN audit_job_status_latest_pivot ON jobs.id = audit_job_status_latest_pivot.job_id
LEFT JOIN survey_scores ON jobs.id=survey_scores.job_id

WHERE (jobs.status = 'Completed' OR jobs.status = 'Released' OR jobs.status = 'GOA' OR jobs.status = 'Canceled') AND (fleet_companies.name = 'USAA')
ORDER by jobs.id
$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{} ',
'{   "start_date":{"type":"start_date"},   "end_date":{"type":"end_date"}} ');




SELECT merge_reports('USAA ETA Variance Report' , 'FleetCompany', $$select
 "Job ID",
 "Date (Central)",
 "Time (Central)",
 "Service Requested",
 "Policy Number",
 "Status",
 "Pickup City",
 "ETA",
 "ATA",
 "ATA"-"ETA" as "ATA-ETA",
 CASE WHEN ("ATA"-"ETA") < 0 THEN 1 ELSE 0 END as "ATA-ETA < 0",
 CASE WHEN ("ATA"-"ETA") <= 10 THEN 1 ELSE 0 END as "ATA-ETA <= 10"
from
(
select
  jobs.id as "Job ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  service_codes.name as "Service Requested",
  jobs.status as "Status",
  pickup.city as "Pickup City",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ETA",
  round(cast((Extract(EPOCH FROM
  (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
  (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ATA",
  '' as "Variance",

    -- Policy Addr
  jobs.policy_number   as "Policy Number",
  policy_addr.street  as "Policy Street",
  policy_addr.city    as "Policy City",
  policy_addr.state   as "Policy State",
  policy_addr.zip     as "Policy Zip"

from jobs
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join locations policy_addr on policy_addr.id = jobs.policy_location_id
where
  jobs.deleted_at is null and
  jobs.status in ('Completed','Released') and
  jobs.scheduled_for is null and
  fleet_company_id = (SELECT id FROM companies WHERE name = 'USAA') and
  jobs.created_at >= ? and
  jobs.created_at < ?
order by jobs.id desc) as t1$$,
'{ "start_date":"2000-01-01", "end_date":"2050-09-01" }',
'["start_date","end_date"]',
'{}',
'{} ',
'{   "start_date":{"type":"start_date"},   "end_date":{"type":"end_date"}} ');
