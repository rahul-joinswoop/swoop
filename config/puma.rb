# frozen_string_literal: true

require_relative "../lib/runtime_settings"

if ENV['RAILS_ENV'] != 'production'
  # only pull this in for puma, if we pull it in for sidekiq we just get spammed
  require 'redis-rails-instrumentation'
end

workers Integer(ENV['WEB_CONCURRENCY'] || 2)
threads_count = RuntimeSettings.puma_thread_count
threads threads_count, threads_count

preload_app!

if ENV['SWOOP_HOSTNAME']
  bind    "tcp://#{ENV['SWOOP_HOSTNAME']}:#{ENV['PORT'] || 3000}"
else
  port    ENV['PORT'] || 3000
end
rackup      DefaultRackup
environment ENV['RACK_ENV'] || 'development'

on_worker_boot do
  # Worker specific setup for Rails 4.1+
  # See: https://devcenter.heroku.com/articles/deploying-rails-applications-with-the-puma-web-server#on-worker-boot
  ActiveRecord::Base.establish_connection
end
