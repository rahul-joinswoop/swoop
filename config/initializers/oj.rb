# frozen_string_literal: true

# TODO - we could also setup Oj to be the default json serializer for rails, see
# https://github.com/ohler55/oj/blob/master/pages/Rails.md

# set the default oj behavior to mimic rails json serialization. specifically
# we want to make sure we serialize { foo: 1 } to {"foo": 1} instead of {":foo": 1 }
# but this also has implications for time/dates, see the above for more.
Oj.default_options = { mode: :rails }

# The :rails mode mimics the ActiveSupport version 5 encoder. Rails and ActiveSupport are built around the use of the
# as_json(*) method defined for a class. Oj attempts to provide the same functionality by being a drop in replacement
# with a few exceptions.
Oj.optimize_rails
