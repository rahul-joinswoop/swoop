# frozen_string_literal: true

# from https://stackoverflow.com/questions/27327134/coerce-to-boolean
class Object

  def to_bool
    !!self
  end

end

module Kernel

  def Boolean(val)
    val.to_bool
  end

end
