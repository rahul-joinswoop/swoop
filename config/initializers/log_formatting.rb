# frozen_string_literal: true

# Override the ActiveSupport SimpleFormatter. Should probably create a completely different
# formatter, but I'm not about to mess with that (we should really use a framework like lumberjack, imo)
class ActiveSupport::Logger::SimpleFormatter

  OMITTED_TEXT = "... [TRUNCATED]"
  MAX_LOG_STRING = ENV.fetch('MAX_LOG_STRING', 1000).to_i

  def call(severity, time, progname, msg)
    formatted_severity = sprintf("-%-5s-", severity)

    if Rails.env.production?
      "#{formatted_severity} #{untagged_log_identifiers} #{format_message(msg)}\n"
    else
      formatted_time = time.strftime("%FT%T.%L")
      "#{formatted_time} #{formatted_severity} #{untagged_log_identifiers} #{msg}\n"
    end
  end

  private

  def format_message(message, max_length = MAX_LOG_STRING)
    escape_message(message).truncate(max_length + OMITTED_TEXT.length, omission: OMITTED_TEXT)
  end

  def escape_message(message)
    if message.is_a?(String)
      # ensure that String subclasses are converted to a string
      message = message.to_s
      # Always escape the message [Security requirement - see PDW]
      message.inspect
    elsif message.is_a?(Exception)
      # Log exceptions in a particular format so we can get the backtrace
      output = "Exception! #{message.class.name}: #{message.message}"
      output << " | backtrace=#{message.backtrace.inspect}"
    else
      # Otherwise (Hashes, Arrays, etc.) just inspect the object
      message.inspect
    end
  end

  def untagged_log_identifiers
    # There are very specific cases in which none of the middleware to track the request_id
    # exists (websockets do weird things with threads), so as a hack to get tracking on these
    # special logs, we're going to generate a UUIDv5 based on the current pid and thread id
    if Rails.logger.formatter.current_tags.blank?
      namespace = "#{ENV['HEROKU_DYNO_ID']}#{Process.pid}"
      uuid = Digest::UUID.uuid_v5(namespace, Thread.current.object_id.to_s)
      "[#{uuid}]"
    end
  end

end
