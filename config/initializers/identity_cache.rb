# frozen_string_literal: true

IdentityCache.cache_backend = ActiveSupport::Cache.lookup_store(*Rails.configuration.identity_cache_store)
IdentityCache.logger = Logger.new(nil)
