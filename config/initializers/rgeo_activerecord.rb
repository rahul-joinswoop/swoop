# frozen_string_literal: true

RGeo::ActiveRecord::SpatialFactoryStore.instance.tap do |config|
  # DO NOT SET A DEFAULT HERE! if you do the factory below won't ever get used?
  # see https://github.com/rgeo/rgeo-activerecord/issues/53

  # Use a geographic implementation for point columns.
  config.register(RGeo::Geographic.spherical_factory(srid: 4326), geo_type: "point")
end
