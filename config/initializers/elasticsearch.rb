# frozen_string_literal: true

Elasticsearch::Model.client = Elasticsearch::Client.new(
  url: ENV['ELASTICSEARCH_URL'] || ENV[ENV['ELASTICSEARCH_PROVIDER'].to_s] || 'http://localhost:9200',
  retry_on_failure: true,
  log: true,
)

Elasticsearch::Model.client.transport.logger.level = Logger::WARN
