# frozen_string_literal: true

Flipper.configure do |config|
  config.default do
    adapter = Flipper::Adapters::Redis.new(RedisClient::WrappedRedis.new(:default))
    Flipper.new(adapter)
  end
end
