# frozen_string_literal: true

module Doorkeeper

  # hard code these here so we can access them outside of our configuration
  # without hackery
  OAUTH_APPLICATION_SCOPES = [:fleet, :super_fleet_managed, :rescue, :customer, :company].freeze

end

Doorkeeper.configure do
  # Change the ORM that doorkeeper will use (needs plugins)
  orm :active_record

  allow_blank_redirect_uri false

  # This block will be called to check whether the resource owner is authenticated or not.
  resource_owner_authenticator do
    SwoopAuthenticator.for(request).user
  end

  resource_owner_from_credentials do |routes|
    SwoopAuthenticator.authenticate_user(request)
  end

  # Doorkeeper admin routes do not currently work with our setup.
  # admin_authenticator do
  #   nil
  # end

  # Authorization Code expiration time (default 10 minutes).
  authorization_code_expires_in 10.minutes

  # Access token expiration time (default 2 hours).
  # If you want to disable expiration, set this to nil.
  access_token_expires_in 1.year

  custom_access_token_expires_in do |context|
    scopes = context.client&.scopes
    # Partner API applications get tokens that do not expire
    if scopes && scopes.include?("company")
      Float::INFINITY
    else
      # Otherwise use the default
      nil
    end
  end

  # Assign a custom TTL for implicit grants.
  # custom_access_token_expires_in do |oauth_client|
  #   oauth_client.application.additional_settings.implicit_oauth_expiration
  # end

  # Use a custom class for generating the access token.
  # https://github.com/doorkeeper-gem/doorkeeper#custom-access-token-generator
  # access_token_generator "::Doorkeeper::JWT"

  # Reuse access token for the same resource owner within an application (disabled by default)
  # Rationale: https://github.com/doorkeeper-gem/doorkeeper/issues/383
  # reuse_access_token

  # Issue access tokens with refresh token (disabled by default)
  use_refresh_token

  # Provide support for an owner to be assigned to each registered application (disabled by default)
  # Optional parameter :confirmation => true (default false) if you want to enforce ownership of
  # a registered application
  # Note: you must also run the rails g doorkeeper:application_owner generator to provide the necessary support
  enable_application_owner confirmation: false

  # Define access token scopes for your provider
  # For more information go to
  # https://github.com/doorkeeper-gem/doorkeeper/wiki/Using-Scopes
  # default_scopes  :public
  # optional_scopes :write, :update
  #
  # OAUTH_APPLICATION_SCOPES are used for our public (graphql) api
  optional_scopes(*Doorkeeper::OAUTH_APPLICATION_SCOPES)

  # Define scopes_by_grant_type to restrict only certain scopes for grant_type
  # By default, all the scopes will be available for all the grant types.
  #
  # Keys to this hash should be the name of grant_type and
  # values should be the array of scopes for that grant type.
  # Note: scopes should be from configured_scopes(i.e. deafult or optional)
  #
  # scopes_by_grant_type password: [:write], client_credentials: [:update]

  # Forbids creating/updating applications with arbitrary scopes that are
  # not in configuration, i.e. `default_scopes` or `optional_scopes`.
  # (disabled by default)
  #
  # enforce_configured_scopes

  # Change the way client credentials are retrieved from the request object.
  # By default it retrieves first from the `HTTP_AUTHORIZATION` header, then
  # falls back to the `:client_id` and `:client_secret` params from the `params` object.
  # Check out the wiki for more information on customization
  # client_credentials :from_basic, :from_params

  # Change the way access token is authenticated from the request object.
  # By default it retrieves first from the `HTTP_AUTHORIZATION` header, then
  # falls back to the `:access_token` or `:bearer_token` params from the `params` object.
  # Check out the wiki for more information on customization
  # Add custom method to retrieve the access token from the session.
  access_token_methods(
    :from_bearer_authorization,
    :from_access_token_param,
    :from_bearer_param,
    lambda { |request| request.session[:access_token] if request.session.present? },
  )

  # Change the native redirect uri for client apps
  # When clients register with the following redirect uri, they won't be redirected to any server and the authorization code will be displayed within the provider
  # The value can be any string. Use nil to disable this feature. When disabled, clients must provide a valid URL
  # (Similar behaviour: https://developers.google.com/accounts/docs/OAuth2InstalledApp#choosingredirecturi)
  #
  # native_redirect_uri 'urn:ietf:wg:oauth:2.0:oob'

  # Forces the usage of the HTTPS protocol in non-native redirect uris (enabled
  # by default in non-development environments). OAuth2 delegates security in
  # communication to the HTTPS protocol so it is wise to keep this enabled.
  #
  # force_ssl_in_redirect_uri !Rails.env.development?

  # Specify what grant flows are enabled in array of Strings. The valid
  # strings and the flows they enable are:
  #
  # "authorization_code" => Authorization Code Grant Flow
  # "implicit"           => Implicit Grant Flow
  # "password"           => Resource Owner Password Credentials Grant Flow
  # "client_credentials" => Client Credentials Grant Flow
  #
  # If not specified, Doorkeeper enables authorization_code and
  # client_credentials.
  #
  # implicit and password grant flows have risks that you should understand
  # before enabling:
  #   http://tools.ietf.org/html/rfc6819#section-4.4.2
  #   http://tools.ietf.org/html/rfc6819#section-4.4.3
  #
  grant_flows %w(client_credentials password authorization_code)

  after_successful_authorization do |ctx|
    # LOLWUT - if we're coming from partner_api_email_verification then fire off
    # the Authentication::ResetPasswordForPartnerAPI service

    # TODO - should we delete these values from session at this point?
    if ctx.session.delete :partner_api_email_verification
      user_id = ctx.session.delete :user_id
      application_uid = ctx.session.delete :application_uid
      user = User.find_by(id: user_id)
      client = Doorkeeper::Application.find_by(uid: application_uid)
      result = ::Authentication::ResetPasswordForPartnerAPI.call user: user, client: client
      if !result.success?
        ::Rails.logger.error "Doorkeeper: failed to run ResetPasswordForPartnerAPI in after_successful_authorization: #{result.error}"
      end
    end
  rescue StandardError => e
    Rollbar.error "Doorkeeper: error in after_successful_authorization", e
    ::Rails.logger.error "Doorkeeper: error in after_successful_authorization: #{e.message}"
  end

  # Under some circumstances you might want to have applications auto-approved,
  # so that the user skips the authorization step.
  # For example if dealing with a trusted application.
  # skip_authorization do |resource_owner, client|
  #   client.superapp? or resource_owner.admin?
  # end

  # WWW-Authenticate Realm (default "Doorkeeper").
  realm "Swoop"
end
