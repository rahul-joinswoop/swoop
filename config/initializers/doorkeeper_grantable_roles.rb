# frozen_string_literal: true

# add Doorkeeper::Application#grantable_roles, analog to User#grantable_roles but
# for applications (duh)

require_relative "../../app/lib/swoop_authenticator"

module GrantableRoles

  def grantable_roles
    Role.where(name: SwoopAuthenticator.scope_roles(scopes.to_a, owner))
  end

end

ActiveSupport.on_load(:active_record) do
  Doorkeeper::Application.include(GrantableRoles)
end
