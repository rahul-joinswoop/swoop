# frozen_string_literal: true

# based on https://github.com/jaredbeck/rails-callback_log/blob/master/lib/rails-callback_log.rb
#
# set RAILS_CALLBACK_LOG_FILTER=1 in .env to enable this. it's super useful for
# debugging specific issues in (say) job creation but it's way too slow to use
# on a daily basis.

# Make sure `ActiveSupport::Callbacks` is loaded before we continue.
require "active_support/all"

module RailsCallbackLog

  # Filtering is very expensive. It makes my test suite more than 50%
  # slower. So, it's off by default.
  FILTER = ENV["RAILS_CALLBACK_LOG_FILTER"].present?.freeze

  # In rails 5.1, we extend `CallTemplate`.
  module CallTemplateExtension

    # Returns a lambda that wraps `super`, adding logging.
    def make_lambda
      original_lambda = super
      lambda { |*args, &block|
        klass = args[0].class
        if ::RailsCallbackLog::FILTER &&
          # we get a bunch of calls to #call - not sure exactly what it is but
          # it's not terribly useful to debugging so we strip them out here
          @method_name != :call &&
          # make sure we're only logging for ActiveRecord models - otherwise we'll
          # get a bunch of worker/sidekiq/etc
          klass.ancestors.include?(::ApplicationRecord)
          ::Rails.logger.debug("Callback: #{klass.name}##{@method_name}")
        end
        original_lambda.call(*args, &block)
      }
    end

  end

end

# Install our `CallbackExtension` using module prepend.
if Rails.env.development?
  ActiveSupport::Callbacks::CallTemplate.prepend ::RailsCallbackLog::CallTemplateExtension
end
