# frozen_string_literal: true

SeedMigration::Migrator.logger.level = Logger::ERROR
SeedMigration.config do |c|
  c.use_activerecord_import = true
end

# ignored attributes in our seeds
EXCLUDED_ATTRS = [:created_at, :updated_at].freeze

# all the model classes we want to manage with SeedMigration
MODELS = [
  Group,
  ServiceCode,
  EventOperation,
  Feature,
  VehicleCategory,
  Symptom,
  SubscriptionStatus,
  CustomerType,
  JobStatus,
  JobEtaExplanation,
  JobRejectReason,
  TireType,
  LocationType,
  ModelType,
  JobStatusReasonType,
  PaymentMethod,
  Role,
  Referer,
].freeze

# configure them all
MODELS.each do |klass|
  SeedMigration.register klass do
    exclude(*EXCLUDED_ATTRS)
  end
end
