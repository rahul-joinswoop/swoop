# frozen_string_literal: true

# like deep_transform_keys but for values
module DeepTransformValues

  def deep_transform_values(&block)
    reduce({}) do |result, (k, v)|
      v = v.to_h if !v.nil? && v.respond_to?(:to_h) && !v.is_a?(Array)
      result[k] = v.is_a?(Hash) ? v.deep_transform_values(&block) : yield(v)
      result
    end
  end

end

Hash.include DeepTransformValues
