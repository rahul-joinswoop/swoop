# frozen_string_literal: true

module Swoop

  class << self

    def graphql_redis_client
      @graphql_redis_client ||= RedisClient::WrappedRedis.new(:default)
    end

  end

end
