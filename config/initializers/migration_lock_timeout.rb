# frozen_string_literal: true

# Specify the number of seconds that migrations will wait for a lock before they
# timeout. This helps protect against long running transactions that may hold row
# level locks from effectively turning into table level locks when a migration is waiting on
MigrationLockTimeout.configure do |config|
  config.default_timeout = 5
end
