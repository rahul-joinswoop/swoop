# frozen_string_literal: true

# Set the default for generated routes to use the values from the SITE_URL environment variable.
site_uri = URI.parse(Rails.env.test? ? "http://test.host" : ENV['SITE_URL'])
host = site_uri.host.freeze
unless site_uri.port == site_uri.default_port
  host = "#{host}:#{site_uri.port}"
end
Rails.application.routes.default_url_options[:host] = host
Rails.application.routes.default_url_options[:protocol] = site_uri.scheme.freeze
