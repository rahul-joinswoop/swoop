# frozen_string_literal: true

require_relative "../../app/models/concerns/graphql_subscriber"
require_relative "../../app/models/concerns/graphql_stats"

ActiveSupport.on_load(:active_record) do
  # add has_many :graphql_subscriptions
  Doorkeeper::Application.include(GraphQLSubscriber)
  # add graphql_deprecated_fields and graphql_requests
  Doorkeeper::Application.include(GraphQLStats)
end
