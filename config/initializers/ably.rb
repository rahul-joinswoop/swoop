# frozen_string_literal: true

module Swoop

  if Rails.env.test?
    # mock our ably rest client here because the actual client doesn't provide a stub mode
    # and there's no simple / global way to do this in rspec (at least that i could find?)

    class AblyRestStub

      class StubHash < Hash

        include Hashie::Extensions::MethodAccess

      end

      def options
        { key: "asdf.asdf:asdf_asdf" }
      end

      def channel
        channel = StubHash.new
        channel.presence = StubHash.new
        channel.presence.get = StubHash.new
        channel.presence.get.items = []
        channel
      end

      def auth
        # noop
      end

    end
  end

  class << self

    def ably_rest_client
      @ably_rest_client ||= Rails.env.test? ? AblyRestStub.new : Ably::Rest.new(key: ENV['ABLY_SERVER_API_KEY'])
    end

    def ably_encryption_key
      @ably_encryption_key ||= [ENV['ABLY_ENCRYPTION_KEY']].pack("H*")
    end

  end

end
