# frozen_string_literal: true

Warden::Strategies.add(:phone) do
  def valid?
    params[:phone] && params[:password]
  end

  def authenticate!
    Rails.logger.debug "warden phone authenticate! called #{params}"
    user = User.find_by(phone: params[:phone])
    if !(user && user.valid_password?(params[:password]))
      fail!("Couldn't log in")
    else
      success!(user)
    end
  end
end
