# frozen_string_literal: true

Stripe.open_timeout = Configuration.stripe.open_timeout
Stripe.read_timeout = Configuration.stripe.read_timeout
Stripe.api_key = Configuration.stripe.private_key
Stripe.api_version = Configuration.stripe.api_version
