# frozen_string_literal: true

# It will Initialize the LogDeadLockTransactions Module to log the Deadlock Errors that comes from Database Transactions.
require 'log_deadlock_transactions'
