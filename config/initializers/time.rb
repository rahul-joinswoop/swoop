# frozen_string_literal: true

# Make Ruby 2.4 preserve the timezone of the receiver when calling `to_time`.
# Previous versions had false.
ActiveSupport.to_time_preserves_timezone = false

# Custom formats for `to_formatted_s`

# Redefining iso8601 formats because the default implementation on different ruby classes
# uses slightly different logic for timezone formatting and we want a consistent format for GraphQL.
Time::DATE_FORMATS[:iso8601] = lambda { |time| time.strftime(time.utc? ? '%Y-%m-%dT%H:%M:%SZ' : '%Y-%m-%dT%H:%M:%S%z') }
Date::DATE_FORMATS[:iso8601] = '%Y-%m-%d'
