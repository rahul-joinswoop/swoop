# frozen_string_literal: true

# Sanity check for application configuration files. All files loaded using
# the `Configuration` class should be referenced here so the app can verify
# that it has valid configuration when starting up.

%w(
  agero_farmers_api
  agero_rsa_api
  fleet_response_invoice_endpoint
  rsc_api
  s3
  stripe
).each do |name|
  unless Configuration[name] && Configuration[name].config.present?
    raise ArgumentError, "Missing #{Rails.env} configuration for #{name.inspect}"
  end
end

JobSimulator.configuration
