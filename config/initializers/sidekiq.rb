# frozen_string_literal: true

require "sidekiq/active_record_query_cache_control_middleware"
require "sidekiq/log_job_info_middleware"
require "sidekiq/pausable"
require "sidekiq/request_context_middleware"
require "sidekiq/worker_uuid_middleware"

Sidekiq::Pausable.install!
Sidekiq::Pausable::Web.install!

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [ENV["SIDEKIQ_USER"], ENV["SIDEKIQ_PASSWORD"]]
end

Sidekiq.configure_server do |config|
  # Let the sidekiq server manage the redis connection using the raw connection options
  config.redis = RedisClient.options(:default)

  config.average_scheduled_poll_interval = 1

  Sidekiq.options[:fetch] = Sidekiq::Pausable::Fetch

  config.server_middleware do |chain|
    chain.add Sidekiq::RequestContextMiddleware::Server
    chain.add Sidekiq::WorkerUuidMiddleware
    chain.add Sidekiq::LogJobInfoMiddleware
    chain.add Sidekiq::ActiveRecordQueryCacheControlMiddleware
  end

  # Cleanup unique jobs lock if a worker dies.
  config.death_handlers << ->(job, _ex) do
    SidekiqUniqueJobs::Digests.del(digest: job['unique_digest']) if job['unique_digest']
  end
end

Sidekiq.configure_client do |config|
  # Set the client to use a configured connection pool client
  config.redis = RedisClient::WrappedConnectionPool.new(:default)

  config.client_middleware do |chain|
    chain.add(Sidekiq::TransactionGuard::Middleware)
    chain.add Sidekiq::RequestContextMiddleware::Client
  end
end

if Rails.env.test?
  Sidekiq::TransactionGuard.mode = :error
else
  Sidekiq::TransactionGuard.mode = :warn
end

# Log what middleware is actually being used in case it's needed for troubleshooting purposes
if Sidekiq.server?
  Rails.logger.info("Sidekiq server middleware: #{Sidekiq.server_middleware.map(&:klass).join(', ')}")
else
  Rails.logger.info("Sidekiq client middleware: #{Sidekiq.client_middleware.map(&:klass).join(', ')}")
end

unless ENV['DB_ADAPTER'] == 'nulldb'
  # Verify that we can actually connect to the sidekiq redis instance
  # so deploys will fail if there's a misconfiguration instead of potentially
  # losing sidekiq jobs that can't be enqueued.
  Sidekiq.redis { |conn| conn.ping }
end

# disable arg logging for rollbar
Rollbar::Delay::Sidekiq.sidekiq_options logging: :without_args
