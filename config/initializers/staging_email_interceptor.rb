# frozen_string_literal: true

# Register an email interceptor for the staging environment.
#
# See: http://guides.rubyonrails.org/v4.2/action_mailer_basics.html#intercepting-emails
if ENV["SWOOP_ENV"] == "staging"
  ActionMailer::Base.register_interceptor(StagingEmailInterceptor)
end
