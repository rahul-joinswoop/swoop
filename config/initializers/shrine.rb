# frozen_string_literal: true

require "shrine/storage/s3"

s3_options = {
  access_key_id: Configuration.s3.access_key,
  secret_access_key: Configuration.s3.secret_key,
  region: Configuration.s3.region,
  bucket: Configuration.s3.document_bucket,
}

# For local development with fakes3
s3_options[:endpoint] = Configuration.s3.endpoint if Configuration.s3.endpoint.present?

Shrine.storages = if Configuration.s3.region.blank?
                    require "shrine/storage/memory"
                    {
                      cache: Shrine::Storage::Memory.new,
                      store: Shrine::Storage::Memory.new,
                    }
                  else
                    {
                      cache: Shrine::Storage::S3.new(prefix: "cache", **s3_options),
                      store: Shrine::Storage::S3.new(prefix: "store", **s3_options),
                    }
                  end
