# frozen_string_literal: true

Logging::SqlLogSubscriber.attach_to(:active_record)
