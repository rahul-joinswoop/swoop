# frozen_string_literal: true

# monkeypatch our pg connection adapters to work around several issues, see
# ApplicationRecord for more info
ActiveSupport.on_load(:active_record) do
  # add transaction_id support
  require 'swoop/postgresql_transaction_id'
  ActiveRecord::ConnectionAdapters::AbstractAdapter.include Swoop::PostgreSQLTransactionId::AbstractAdapter
  ActiveRecord::ConnectionAdapters::PostgreSQLAdapter.include Swoop::PostgreSQLTransactionId::PostgreSQLAdapter

  # gracefully handle undefined columns after destructive migrations
  require 'swoop/postgresql_undefined_column'
  ActiveRecord::ConnectionAdapters::PostgreSQLAdapter.prepend Swoop::PostgreSQLUndefinedColumn
end
