# frozen_string_literal: true

# Monkey patch the fix how we're parsing our custom swoop ios/android user agents

Browser::Platform.matchers.insert(0, Swoop::SwoopNativeBrowser)
