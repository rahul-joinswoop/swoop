# frozen_string_literal: true

Analytics = Segment::Analytics.new({
  write_key: Rails.env.production? ? ENV["SEGMENT_PRIVATE_KEY"] : 'fake_since_calls_are_stubbed',
  on_error: Proc.new { |status, msg| Rails.logger.debug msg },
  stub: !Rails.env.production?,
})
