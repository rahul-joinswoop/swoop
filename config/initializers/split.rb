# frozen_string_literal: true

if ENV["SPLIT_API_KEY"]
  split_factory = SplitIoClient::SplitFactory.new(ENV["SPLIT_API_KEY"])
  Rails.configuration.split_client = split_factory.client
else
  Rails.configuration.split_client = nil
end
