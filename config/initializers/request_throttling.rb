# frozen_string_literal: true

# Configure request throttling by IP address. These values are set with environment variables:
#
# * RATE_PERIOD = the number of seconds to measure requests over
# * RATE_LIMIT = the number of requests allowed by an IP address over the period measured.
# * WHITLISTED_IPS = list of IP addresses that are exempt from throttling (localhost is always exempt)
# * BLACKLISTED_IPS = list of IP addresses to always deny access to.
# * THROTTLE_ENABLED = requests will only actually be throttled if this is set to true
#
# If an IP address exceeds the allowed limit in the allowed period, it will be logged an reported
# to Rollbar. Additionally, if THROTTLE_ENABLED=true, then a 429 response will be returned
# that will include a Retry-After header containing the number of seconds until the IP will no longer
# be throttled.
#
# The average throughput allowed will be RATE_LIMIT / RATE_PERIOD requests per second.
# It is a best practice, though, to use a lower value for RATE_PERIOD so that clients
# don't bunch up their request at the start of the period. For instance, if we want to
# cap clients at 600 requests per minute, it is usually better to specify this as something
# like 10 requests per second to prevent the client from sending 600 requests at once,
# overloading the server, and then having to wait a minute to make the next request.
#
# Whitelisted and blacklisted IP addresses can be specified as ranges using standard
# CIDR notation.
#
# Request throttling can be disabled entirely by setting either RATE_LIMIT or RATE_PERIOD
# to "0".

rate_limit = Integer(ENV.fetch('RATE_LIMIT', '100'))
rate_period = Integer(ENV.fetch('RATE_PERIOD', '5'))
whitelisted_ips = ENV['WHITLISTED_IPS'].to_s.split(/[ ,]+/)
blacklisted_ips = ENV['BLACKLISTED_IPS'].to_s.split(/[ ,]+/)
enabled = (ENV['THROTTLE_ENABLED'] == 'true')

if RedisClient[:cache]
  SimpleThrottle.set_redis { RedisClient[:cache] }

  Rails.application.config.middleware.use(
    ThrottleIpAddressMiddleware,
    {
      rate_limit: rate_limit,
      rate_period: rate_period,
      whitelisted_ips: whitelisted_ips,
      blacklisted_ips: blacklisted_ips,
      enabled: enabled,
    }
  )
end
