# frozen_string_literal: true

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    # we (currently) don't depend on cors for our site to work. these defaults
    # should be fine, at least until we start hosting stuff on a cdn.
    origins ENV.fetch('SITE_URL', 'http://localhost:3000')
    resource '*',
             headers: :any,
             methods: %i(get post put patch delete options head)
  end
end
