# frozen_string_literal: true

require 'aws-sdk-s3'

opts = {
  access_key_id: Configuration.s3.access_key,
  secret_access_key: Configuration.s3.secret_key,
  region: Configuration.s3.region,
}
Aws.config.update(opts)

# 20 minutes
S3_SIGNED_URL_EXPIRATION = 1200

S3, S3_CLIENT = if Configuration.s3.endpoint.present?
                  [
                    Aws::S3::Resource.new({ endpoint: Configuration.s3.endpoint }),
                    Aws::S3::Client.new({ endpoint: Configuration.s3.endpoint }),
                  ]
                else
                  [Aws::S3::Resource.new, Aws::S3::Client.new]
                end
