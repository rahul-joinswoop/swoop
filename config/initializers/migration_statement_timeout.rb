# frozen_string_literal: true

# Monkey patch migrations to turn off statement timeouts so that indexes, etc.
# can be created on large tables without timing out.
module MigrationStatementTimeout

  def migrate(direction)
    statement_timeout = safety_assured { select_one("SHOW statement_timeout")["statement_timeout"] }
    begin
      safety_assured { execute "SET SESSION statement_timeout = 0" }
      super
    ensure
      safety_assured { execute "SET SESSION statement_timeout = '#{statement_timeout}'" }
    end
  end

end

unless ::ActiveRecord::Migration.include?(MigrationStatementTimeout)
  ::ActiveRecord::Migration.prepend(MigrationStatementTimeout)
end
