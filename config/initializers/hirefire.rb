# frozen_string_literal: true

HireFire::Resource.configure do |config|
  config.dyno(:sidekiq) do
    HireFire::Macro::Sidekiq.queue
  end
  config.dyno(:worker) do
    HireFire::Macro::Delayed::Job.queue(mapper: :active_record)
  end
end
