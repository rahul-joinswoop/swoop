# frozen_string_literal: true

Geocoder.configure(
  lookup: :google_premier,
  google_premier: {
    api_key: [
      ENV['GOOGLE_PREMIUM_SECRET_KEY'],
      ENV['GOOGLE_PREMIUM_CLIENT_ID'],
      ENV['GOOGLE_PREMIUM_CHANNEL'],
    ],
  }
)
