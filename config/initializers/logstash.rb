# frozen_string_literal: true

if ENV['LOGSTASH_TOKEN']

  require 'logstash-logger'

  LogStashLogger.configure do |config|
    config.customize_event do |event|
      event["token"] = ENV['LOGSTASH_TOKEN']
    end
  end

  Stash = LogStashLogger.new(host: 'listener.logz.io', port: 5050)

end
