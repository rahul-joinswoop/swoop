# frozen_string_literal: true

module DeepOrganized

  # recurse through organiers in the organized array and find out which interactors _they_ organize too
  def deep_organized
    organized.map do |i|
      if i.included_modules.include?(Interactor::Organizer)
        i.deep_organized
      else
        i
      end
    end.flatten
  end

end

Interactor::Organizer::ClassMethods.include DeepOrganized
