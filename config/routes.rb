# frozen_string_literal: true

using GraphQL::Pro::Routes

Rails.application.routes.draw do
  post '/ageroapi/authcode' => 'agero_webhook#auth_code'

  post "/graphql", to: "graphql#execute"
  post "/graphql/batch", to: "graphql#batch"

  if Rails.env.development?
    post "/graphql/webhook", to: "graphql#fake_webhook"
  end
  graphql_dashboard = Rack::Builder.new do
    use(Rack::Auth::Basic) do |username, password|
      username == ENV.fetch("GRAPHQL_DASHBOARD_USERNAME") && password == ENV.fetch("GRAPHQL_DASHBOARD_PASSWORD")
    end
    run SwoopSchema.dashboard
  end

  mount graphql_dashboard, at: "/graphql/dashboard"
  mount SwoopSchema.operation_store_sync, at: "/graphql/sync"
  # Handle webhooks for subscriptions:
  mount SwoopSchema.ably_webhooks_client, at: "/ably/webhooks"
  post '/ably/auth' => 'ably#auth'

  require 'sidekiq_unique_jobs/web'
  # fix sessions in sidekiq so that the web interface doesn't throw csrf errors, see
  # https://github.com/mperham/sidekiq/wiki/Monitoring#forbidden
  Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]
  mount Sidekiq::Web => "/sidekiq"

  # QBD

  get '/t/:uuid' => 'callbacks#txt'

  constraints(host: /farmerssurvey.com/) do
    match "/(*path)" => redirect { |params, req| "http://farmers.com" }, :via => [:get, :post]
  end

  # Redirect www -> naked
  constraints(host: /^www\./i) do
    match '(*any)' => redirect { |params, request|
      URI.parse(request.url).tap { |uri| uri.host.sub!(/^www\./i, '') }.to_s
    }, :via => [:get, :post]
  end

  use_doorkeeper

  get '/stats(.json)' => "health_check#index"

  get '/txt/:uuid' => 'callbacks#txt'

  post '/twilio_status/:audit_id' => 'callbacks#twilio_status'

  # begin react endpoints
  root to: 'react#application'

  if Rails.env.development?
    get '/dev(/*dev)' => 'react#dev'
    get '/survey' => 'react#dev_survey'
    get '/pdf' => 'invoice_pdfs#pdf'
  end
  # end react endpoints

  controller :consumer_mobile_web do
    get '/get_location/:uuid', action: :get_location
    get '/show_eta/:uuid', action: :show_eta
    get '/s/:uuid', action: :survey
  end

  get '/ticket/:invoice_uuid(/:id)', to: 'invoice_pdfs#show', as: "invoice_pdf"

  # TODO why do we expose this?
  get '/time' => 'welcome#time'

  get '/apple-app-site-association', to: 'welcome#apple_app_site_association'
  get '/.well-known/apple-app-site-association', to: 'welcome#apple_app_site_association'
  get '/.well-known/assetlinks', to: 'welcome#assetlinks'

  post '/review/callback' => 'callbacks#review_callback'
  get '/review/:uuid' => 'callbacks#review_redirect'
  get '/r/:fleet/:uuid' => 'callbacks#review_redirect'

  # API
  namespace :api, constraints: { format: 'json' }, defaults: { format: :json } do
    post 'issc/event' => 'issc_webhook#create'

    controller :digital_dispatch_webhooks do
      post 'digital_dispatch/handle_rsc_event', action: :handle_rsc_event
    end

    namespace :v1 do
      post '/sites/search' => 'sites#search'

      get 'service_aliases' => 'service_aliases#index'

      resources :roles, only: [:index]

      get '/departments/batch' => 'departments#batch'
      resources :departments

      get '/tags/batch' => 'tags#batch'
      resources :tags

      get '/poi_sites/batch' => 'poi_sites#batch'
      resources :poi_sites

      get 'tire_types/batch' => 'tire_types#batch'
      get 'symptoms/standard' => 'symptoms#standard'
      get 'symptoms/batch' => 'symptoms#batch'
      post 'symptoms/change' => 'symptoms#change'

      get '/customer_types/batch' => 'customer_types#batch'

      post '/jobs/:job_id/audit_job_statuses' => 'audit_job_statuses#create'

      get 'job_status_reason_types' => 'job_status_reason_types#index'

      resources :oauth_applications, only: [:index, :create, :destroy]

      get 'referers' => 'referers#index'

      namespace :fleet do
        get 'jobs/active' => 'jobs#active'
        get 'jobs/map' => 'jobs#map'
        get 'jobs/done' => 'jobs#done'
        get 'jobs/:id/recommended_providers' => 'jobs#recommended_providers'
        get 'jobs/:id/recommended_providers/:provider_id' => 'jobs#recommended_provider'
        post 'jobs/:id/copy' => 'jobs#copy'
        patch 'jobs/:id/status' => 'jobs_status#update'
        patch 'jobs/:id/choose_partner' => 'jobs_choose_partner#update'
        post 'jobs/:id/send_details_email' => 'jobs#send_details_email'

        get '/jobs/search_new' => 'job_search#index'
        get '/users/search' => 'user_search#index'
        get '/providers/search' => '/api/v1/fleet/search/provider_search#index'

        get '/issues' => 'issues#index'

        get 'jobs/vin/:vin' => 'jobs#show'
        resources :jobs do
          patch 'change_provider_to/:new_rescue_provider_id', to: 'jobs#change_provider_to'
          patch 'change_payment_type_to/:new_payment_type_id', to: 'jobs#change_payment_type_to'

          resources :attached_documents, only: [:create, :update] do
            collection do
              get 'presign'
            end
          end

          resources :explanations, only: [:create, :update]
        end

        get 'partial_jobs' => 'partial_jobs#index'

        resources :sites

        get 'symptoms' => 'symptoms#index'
        get 'rescue_vehicles' => 'vehicles#index'
        get 'rescue_vehicles/batch' => 'vehicles#batch'

        post 'inventory_items' => 'inventory_items#create'
        delete 'inventory_items/:id' => 'inventory_items#destroy'
        get 'inventory_items/tires' => 'inventory_items#tires'
        get 'inventory_items/tire_quantity_by_site_and_type' => 'inventory_items#tire_quantity_by_site_and_type'
        patch 'inventory_items/decrease_tire_quantity' => 'inventory_items#decrease_tire_quantity'
        get 'inventory_items/:id' => 'inventory_items#show'
        patch 'inventory_items/:id' => 'inventory_items#update'

        ### DEPRECATED: These routes are deprecated and should be moved over to the below rescue_companies routes
        post 'companies' => 'rescue_companies#create'
        get 'companies/batch' => 'rescue_companies#batch'
        get 'company/logo/presign' => 'rescue_companies#presign'
        post 'company/logo' => 'rescue_companies#create_logo'
        delete 'company/logo' => 'rescue_companies#delete_logo'
        get "companies/:id" => "rescue_companies#show"
        post 'companies/site' => 'rescue_companies#add_site_to_existent_partner'
        post "companies/existing_site" => "rescue_companies#add_existing_site"
        ### End deprecated routes

        get '/search/rescue_companies/:term' => 'rescue_companies#search_rescue_companies'
        post 'rescue_companies' => 'rescue_companies#create'
        get 'rescue_companies/batch' => 'rescue_companies#batch'
        get 'rescue_company/logo/presign' => 'rescue_companies#presign'
        post 'rescue_company/logo' => 'rescue_companies#create_logo'
        delete 'rescue_company/logo' => 'rescue_companies#delete_logo'
        get "rescue_companies/:id" => "rescue_companies#show"
        post 'rescue_companies/site' => 'rescue_companies#add_site_to_existent_partner'
        post "rescue_companies/existing_site" => "rescue_companies#add_existing_site"

        get 'customer_types' => 'customer_types#index'

        post 'invoices/download' => 'invoices#download'

        get 'invoices' => 'invoices#index'
        get 'invoices_search' => 'invoices#search'
        patch 'invoices/:id' => 'invoices#update'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # use rates/fetch_by_filters route instead
        get 'rates/provider/:rescue_provider_id/service/:service_code_id' => 'rates#provider'
        get 'rates/fetch_by_filters' => 'rates/fetch_by_filters'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # route is deprecated, use 'rates/defaults' instead
        post "rates/provider/:rescue_provider_id/services/:service_code_id" => "rates#create_service"
        post 'rates/defaults' => 'rates#create_based_on_defaults'

        post '/rates/live' => 'rates#live'
        patch 'rates' => 'rates#update_multi'
        post 'rates' => 'rates#create'
        delete 'rates' => 'rates#delete'
        # get 'rates/provider/:rescue_provider_id/addons' => 'rates#addons'

        get 'reports' => 'reports#index'
        post 'reports/:id/run(.:report_format)' => 'reports#run_report'

        get 'report_results/:id' => 'report_results#show'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # use services/by_rates_filter route instead
        get 'services/with_rates/provider/:rescue_provider_id' => 'services#with_rates_by_provider'
        get 'services/by_rates_filter' => 'services#index_by_rates_filter'

        get 'services/partner/:provider_id' => 'services#partner'
        get 'services/standard' => 'services#standard'
        post "sites/search" => "sites#search"

        resources :invoice_rejections, only: [:index, :create] do
          collection do
            get :reasons
          end
        end

        get "vehicle_categories/partner/:rescue_provider_id" => "vehicle_categories#partner"

        post 'look' => 'looker#look'
        get 'looker/dashboards' => 'looker#dashboards'

        post 'autocomplete/rescue_companies_by_provider' => 'autocomplete#rescue_companies_by_provider'
        post 'autocomplete/rescue_providers' => 'autocomplete#rescue_providers'
        post 'autocomplete/recommended_providers' => 'autocomplete#recommended_providers'

        # Depricated, use /policy/search
        post '/policy' => 'pcc/policy#create'

        # New Policy Search routes
        # { job_id:<job.id>, search: {policy_number:<policy_num>}}
        post '/policy/search' => 'pcc/policy#search'

        get  '/policy/:id' => 'pcc/policy#show'
        get  '/policy/:policy_id/pcc_vehicle/:pcc_vehicle_id/pcc_driver/:pcc_driver_id/coverage' => 'pcc/policy#show_coverage'
        post '/policy/coverage' => 'pcc/policy#coverage'

        post 'dropoff_sites' => 'dropoff_sites#dropoff_sites'

        post 'jobs/survey_results' => 'survey/results#create'
      end

      namespace :partner do
        post 'stripe_webhooks' => 'stripe_webhooks#account_event'
        post 'stripe_webhooks/connect' => 'stripe_webhooks#connect_event'

        resource :custom_accounts, only: [:create, :show, :update]

        resources :integrations, only: [:destroy]

        post 'signup' => 'signup#create'

        patch 'company/commission' => 'companies#update_commission'
        patch 'company/role_permissions' => 'companies#update_role_permissions'
        post 'company' => 'companies#update'
        get 'companies' => 'companies#index'
        get 'companies/batch' => 'companies#batch'
        get 'company/logo/presign' => 'companies#presign'
        post 'company/logo' => 'companies#create_logo'
        delete 'company/logo' => 'companies#delete_logo'
        get 'companies/:id' => 'companies#show'
        post 'companies' => 'companies#create'
        patch 'companies/:id' => 'companies#update_subcompany'
        delete 'companies/:id' => 'companies#destroy'

        resources :companies, only: [] do
          resources :territories,
                    shallow: true,
                    only: [:create, :index, :show, :destroy] do
            collection do
              delete :destroy_all
            end
          end
        end

        get 'storage_types' => 'storage_types#index'
        get 'storage_types/:id' => 'storage_types#show'
        post 'storage_types' => 'storage_types#create'
        patch 'storage_types/:id' => 'storage_types#update'
        delete 'storage_types/:id' => 'storage_types#destroy'

        post 'commands/' => 'commands#command'
        get 'vehicles/with_driver' => 'vehicles#with_driver'
        get 'vehicles/batch' => 'vehicles#batch'
        resources :vehicles
        get 'vehicles/:id/jobs' => 'vehicles#jobs'
        post 'vehicles/:id/logout' => 'vehicles#logout'
        # Background Geolocation Library requires a put or post
        put 'vehicles/:id/geolocation' => 'vehicles#update'
        patch 'vehicles/:id/location' => 'vehicles#location_update'

        resources :trailers

        get 'jobs/active' => 'jobs#active'
        get 'jobs/map' => 'jobs#map'
        get 'jobs/done' => 'jobs#done'
        get '/jobs/search_new' => 'job_search#index'

        get '/users/search' => 'user_search#index'
        get '/accounts/search' => 'search/account_search#index'

        post '/jobs/:id/bids' => 'jobs_bid#bid'
        post '/jobs/:id/bid/reject' => 'jobs_bid#reject'

        get 'jobs/storage' => 'jobs#storage'
        resources :jobs do
          resources :attached_documents, only: [:create, :update] do
            collection do
              get 'presign'
            end
          end
        end

        post 'jobs/demo' => 'jobs#create_demo'
        post 'jobs/:id/get_location' => 'jobs#get_location'
        post 'jobs/:id/copy' => 'jobs#copy'
        post 'jobs/:id/add_job' => 'jobs#add_job'
        patch 'jobs/:id/status' => 'jobs_status#update'
        post 'jobs/:id/request_callback' => 'request_callback#create'
        post 'jobs/:id/reject' => 'reject_job#create'
        post 'jobs/:id/send_details_email' => 'jobs#send_details_email'
        patch 'jobs/:id/release' => 'jobs#release_vehicle'
        post 'jobs/:id/request_more_time' => 'jobs#request_more_time'

        get 'partial_jobs' => 'partial_jobs#index'

        get 'users/me/jobs' => 'users#jobs'
        get 'users/:driver/jobs/done/' => 'jobs#done'
        get 'companies/drivers' => 'companies#drivers'

        get 'accounts/batch' => 'accounts#batch'
        get 'accounts/motorclub' => 'accounts#motorclub'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # use accounts/structured_for_rates route instead
        get 'accounts/with_rates' => 'accounts#with_rates'
        get 'accounts/structured_for_rates' => 'accounts#index_structured_for_rates'

        get 'accounts/without_rates' => 'accounts#without_rates'
        get 'accounts' => 'accounts#index'
        post 'accounts' => 'accounts#create'
        get 'accounts/:id' => 'accounts#show'
        patch 'accounts/:id' => 'accounts#update'

        post 'inventory_items' => 'inventory_items#create'
        delete 'inventory_items/:id' => 'inventory_items#destroy'
        get 'inventory_items/tire_quantity_by_site_and_type' => 'inventory_items#tire_quantity_by_site_and_type'
        patch 'inventory_items/decrease_tire_quantity' => 'inventory_items#decrease_tire_quantity'
        get 'inventory_items/tires' => 'inventory_items#tires'
        get 'inventory_items/:id' => 'inventory_items#show'
        patch 'inventory_items/:id' => 'inventory_items#update'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # use services/by_rates_filter route instead
        get 'services/with_rates/accounts/:account_id' => 'services#with_rates_by_account'
        get 'services/by_rates_filter' => 'services#index_by_rates_filter'

        get 'services/standard' => 'services#standard'

        # PDW deprecated in favour of /partner/accounts company
        get 'customer_types' => 'customer_types#index'

        get 'invoices' => 'invoices#index'
        get 'invoices_search' => 'invoices#search'
        post 'invoices' => 'invoices#create'
        get 'invoices/:id' => 'invoices#show'
        patch 'invoices/:id' => 'invoices#update'
        post 'invoices/:id/samplerate' => 'invoices#samplerate'
        post 'invoices/:id/resend' => 'invoices#resend'
        post 'invoices/:id/paid' => 'invoices#paid'
        post 'invoices/:id/charge' => 'invoices#charge'
        post 'invoices/:id/payments/:payment_id/refund' => 'invoices#refund'

        get 'rates' => 'rates#index'
        patch 'rates/:id' => 'rates#update'
        post 'rates' => 'rates#create'
        patch 'rates' => 'rates#update_multi'
        delete 'rates' => 'rates#delete'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # use rates/fetch_by_filters route instead
        get 'rates/accounts/:account_id' => 'rates#account'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # use rates/defaults route instead
        post 'rates/accounts/:account_id' => 'rates#create_account'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # use rates/fetch_by_filters route instead
        get 'rates/accounts/:account_id/services/:service_id' => 'rates#account_service'

        # DEPRICATED (can be removed after VendorID Rates FE is shipped)
        # use rates/defaults route instead
        post 'rates/accounts/:account_id/services/:service_id' => 'rates#create_service'

        post 'rates/defaults' => 'rates#create_based_on_defaults'
        get 'rates/fetch_by_filters' => 'rates/fetch_by_filters'

        post '/rates/live' => 'rates#live'

        get 'sites' => 'sites#index'
        get 'sites/dispatchable' => 'sites#dispatchable'
        get 'sites/tire_program' => 'sites#tire_program'
        patch 'sites/:id' => 'sites#update'
        post 'sites' => 'sites#create'

        # get '/reports/driver' => 'reports#driver'
        get 'reports' => 'reports#index'
        post 'reports/:id/run(.:report_format)' => 'reports#run_report'
        get 'report_results/:id' => 'report_results#show'
        post 'reports/storage_list'

        get 'invoice_rejections' => 'invoice_rejections#index'

        get 'auctions' => 'auctions#index'

        get 'looker/dashboards' => 'looker#dashboards'
        post 'look' => 'looker#look'

        post 'autocomplete/accounts' => 'autocomplete#accounts'

        get 'commissions/exclusions' => 'commissions#index_exclusions'
        patch 'commissions/addons/exclusions' => 'commissions#update_addons_exclusions'
        patch 'commissions/services/exclusions' => 'commissions#update_services_exclusions'

        get 'qb/token' => 'qb#token'
      end

      namespace :qbd do
        get 'accounts' => 'v0#accounts'
        get 'invoices' => 'v0#invoices'
        get 'payments' => 'v0#payments'
        get 'lineitems' => 'v0#lineitems'
        get 'items'    => 'v0#items'
        get 'user'     => 'v0#user'
        put 'imported' => 'v0#imported'
        put 'onboard'  => 'v0#onboard'
      end

      post 'get_location' => 'get_location#location'

      get '/jobs/uuid/:uuid' => 'customer_jobs#show'

      get 'vehicles/makes' => 'vehicles#makes'
      get 'vehicles/extra_types' => 'vehicles#extra_types'

      # Leaving route under users for now
      get 'users/lookup' => 'user_lookup#by_encoded_email_or_username'

      get 'users/me' => 'users#me'
      get 'users' => 'users#index'
      get '/users/batch' => 'users#batch'
      get '/sites/batch' => 'sites#batch'
      get '/users/dispatchers' => 'users#dispatchers'
      get '/users/drivers' => 'users#drivers'
      get 'users/:id' => 'users#show'
      post 'users' => 'users#create'
      patch 'users/change_password' => 'users#change_password'
      patch 'users/:id' => 'users#update'
      post 'users/search' => 'users#search'
      post 'users/password_request' => 'users#password_request'
      get 'users/password_request/status' => 'users#password_request_status'

      get 'tire_types' => 'tire_types#index'

      post 'metrics' => 'metrics#create'

      get 'settings' => 'settings#index'
      get 'settings/:id' => 'settings#show'
      post 'settings' => 'settings#create'
      patch 'settings/:id' => 'settings#update'
      delete 'settings/:id' => 'settings#destroy'

      get 'user/settings' => 'user_settings#index'
      get 'user/settings/:id' => 'user_settings#show'
      post 'user/settings' => 'user_settings#create'
      patch 'user/settings/:id' => 'user_settings#update'
      delete 'user/settings/:id' => 'user_settings#destroy'

      # PDW: use RescueProvider      get 'companies/rescue' => 'companies#rescue_index'
      # PDW:not used?      get 'companies/:id' => 'companies#show'

      get 'questions' => 'questions#index'

      get 'services' => 'services#index'
      get 'reject_reasons' => 'reject_reasons#index'
      get 'eta_explanations' => 'eta_explanations#index'

      get 'invoices/uuid/:uuid' => 'invoices#show_uuid'

      get 'accounts' => 'accounts#index'
      patch 'status_history/:id' => 'status_history#update'

      get 'versions' => 'versions#index'
      get 'versions/:name/latest' => 'versions#latest'

      post 'location_types/search' => 'location_types#search'
      get 'location_types/:id' => 'location_types#show'

      get 'features' => 'features#index'

      post '/invoices/:uuid/pdfs', to: 'invoices#create_pdf_asynchronously'
      get  '/invoices/:uuid/pdfs/:pdf_id', to: 'invoices#show_pdf'

      # TODO - depricated
      resources :vehicles
      get 'vehicles/:id/jobs' => 'vehicles#jobs'
      post 'vehicles/:id/logout' => 'vehicles#logout'
      # end depriccated

      get 'providers' => 'providers#index'
      # do via invite?        post 'providers' => 'providers#create'

      get 'providers/batch' => 'providers#batch'
      get 'providers/search_batch' => 'providers#search_batch'
      get 'providers/tire_program' => 'providers#tire_program'
      get 'providers/grouped_by_partner' => 'providers#grouped_by_partner'
      get 'providers/:id' => 'providers#show'
      patch 'providers/:id' => 'providers#update'

      get 'vehicle_categories' => 'vehicle_categories#index'
      get 'vehicle_categories/standard' => 'vehicle_categories#standard'

      get  'services/batch' => 'services#batch'
      get  'services/batch_by_name' => 'services#batch_by_name'
      post 'services/change' => 'services#change'
      get  'vehicle_categories/batch' => 'vehicle_categories#batch'
      post 'vehicle_categories/change' => 'vehicle_categories#change'

      namespace :root do
        delete 'custom_accounts' => 'custom_accounts#destroy'
        get 'rates/accounts/:account_id/services/:service_id' => 'rates#account_service'

        post 'customer_types' => 'customer_types#create'
        get 'customer_types' => 'customer_types#index'

        get 'jobs/active' => 'jobs#active'
        get 'jobs/map' => 'jobs#map'
        get 'jobs/done' => 'jobs#done'
        post 'jobs/:id/copy' => 'jobs#copy'
        get 'jobs/:id/recommended_providers' => 'jobs#recommended_providers'
        get 'jobs/:id/recommended_providers/:provider_id' => 'jobs#recommended_provider'
        get 'jobs/:id/billing_info' => 'jobs#billing_info'

        # PDW Depricated in favour of partial_jobs
        get '/jobs/search_new' => 'job_search#index'

        get '/users/search' => 'user_search#index'
        get '/providers/search' => '/api/v1/root/search/provider_search#index'

        patch '/users/restore' => 'users#restore'

        resources :jobs do
          patch 'change_provider_to/:new_rescue_provider_id', to: 'jobs#change_provider_to'
          patch 'restore', to: 'job_restoration#create'

          resources :explanations, only: [:create, :update]
          resources :attached_documents, only: [:create, :update] do
            collection do
              get 'presign'
            end
          end
        end

        patch 'jobs/:id/status' => 'jobs_status#update'
        patch 'jobs/:id/choose_partner' => 'jobs_choose_partner#update'

        get 'partial_jobs' => 'partial_jobs#index'

        get 'rescue_vehicles' => 'vehicles#index'
        get 'rescue_vehicles/batch' => 'vehicles#batch'
        patch 'rescue_vehicles/:id' => "vehicles#update"

        post 'versions' => 'versions#create'
        patch 'versions/:id' => 'versions#update'
        get 'users' => 'users#index'
        get '/users/batch' => 'batch_users#batch'

        post 'company' => 'companies#update_swoop'
        post 'jobs/:id/send_details_email' => 'jobs#send_details_email'
        get '/companies/search' => 'company_search#index'
        get '/companies/init' => 'company_init#index'
        get 'companies' => 'companies#index'
        get 'companies/batch' => 'companies#batch'
        get 'companies/:id' => 'companies#show'
        post 'companies' => 'companies#create'
        patch 'companies/:id' => 'companies#update'

        post 'invoices/download/fleet' => 'invoices#download_fleet'
        post 'invoices/export/fleet' => 'invoices#export_fleet'

        # DEPRICATED - should use invoices/download_all/partner instead
        post 'invoices/download/partner' => 'invoices#download_partner'
        post 'invoices/download_all/partner' => 'invoices#download_all_partner'

        get 'invoices/download' => 'invoices#download'
        get 'invoices/incoming' => 'invoices#incoming'
        get 'invoices/outgoing' => 'invoices#outgoing'
        get 'invoices' => 'invoices#incoming_or_outgoing'
        get 'invoices_search' => 'invoices#search'
        get 'invoices/:id' => 'invoices#show'
        post 'invoices' => 'invoices#create'
        patch 'invoices/:id' => 'invoices#update'
        post 'invoices/:id/samplerate' => 'invoices#samplerate'
        post 'invoices/:id/paid' => 'invoices#paid'

        get 'features' => 'features#index'
        post 'authenticate_as' => 'users#authenticate_as'

        get 'services/standard' => 'services#standard'

        # DEPRECATED (can be removed after VendorID Rates FE is shipped)
        # route is deprecated, use 'api/v1/tags' (base route) instead
        get 'tags' => 'tags#index'

        # get '/reports/jobs' => 'reports#jobs'
        get 'reports' => 'reports#index'
        post 'reports/:id/run(.:report_format)' => 'reports#run_report'

        get 'report_results/:id' => 'report_results#show'

        get 'dataclips/results/:token/:filename.csv', to: 'dataclip_results#show', constraints: { format: :csv }, as: :dataclip_results
        resources :dataclips, only: [:index, :show]

        resources :invoice_rejections, only: [:index, :create] do
          collection do
            get :reasons
          end
        end

        get 'auctions' => 'auctions#index'

        get 'looker/dashboards' => 'looker#dashboards'
        post 'look' => 'looker#look'

        post 'autocomplete/company' => 'autocomplete#company'
        post 'autocomplete/rescue_companies_by_provider' => 'autocomplete#rescue_companies_by_provider'
        post 'autocomplete/rescue_providers' => 'autocomplete#rescue_providers'
        post 'autocomplete/recommended_providers' => 'autocomplete#recommended_providers'
        post 'autocomplete/accounts' => 'autocomplete#accounts'
        post 'autocomplete/root_users' => 'autocomplete#root_users'

        post 'fake_objects' => 'fake_objects#create'

        get '/issues' => 'issues#index'

        # Deprecated, use /policy/search
        post '/policy' => 'pcc/policy#create'

        # New Policy Search routes
        # { job_id:<job.id>, search: {policy_number:<policy_num>}}
        post '/policy/search' => 'pcc/policy#search'

        get  '/policy/:id' => 'pcc/policy#show'
        get  '/policy/:policy_id/pcc_vehicle/:pcc_vehicle_id/pcc_driver/:pcc_driver_id/coverage' => 'pcc/policy#show_coverage'
        post '/policy/coverage' => 'pcc/policy#coverage'

        post 'dropoff_sites' => 'dropoff_sites#dropoff_sites'
      end
    end
  end

  resource :auth, controller: :auth, only: [] do
    get 'agero', action: :agero_login
    get 'agero/callback', action: :agero_callback
    match 'partner', action: :partner_authentication, via: [:get, :post]
    resources :partner_api_email_verifications, path: 'email_verifications', only: [:create, :show], param: :uuid
    resources :partner_api_password_requests, path: 'password_requests', only: [:create, :show, :update], param: :uuid
  end

  resources :twilio_replies, :issc_webhook
  post 'twilio_replies/review' => 'twilio_replies#create'
  post 'twilio_replies/inbound' => 'twilio_replies#create'

  get '/revision', to: proc { |env| [200, { "Content-Type" => "text/plain; charset=UTF-8" }, [Rails.application.revision || 'undefined']] }

  # Must come at end!

  get '*path', to: 'react#application', constraints: lambda { |req| req.format == :html }

  # if we're getting an options request send a 204 back, rack-cors will handle the
  # headers for us
  match '*path', via: [:options], to: lambda { |_| [204, { 'Content-Type' => 'text/plain' }, []] }

  # otherwise 404
  # TODO - we should handle json 404's differently?
  match "*path", to: "welcome#render_404", via: :all
end
