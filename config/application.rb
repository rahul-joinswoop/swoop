# frozen_string_literal: true

class CannotBootSwoopAPIWithoutGoogleMapsKeysError < StandardError; end

require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"

# without this wickedpdf blows up
require "sprockets/railtie"

# this is required because running rake needs to list the test tasks even when
# you're not in RAILS_ENV=test. this is weird because we don't even use test_unit
# but whatever
require "rails/test_unit/railtie"

require 'elasticsearch/rails/instrumentation'
require 'elasticsearch/rails/lograge'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# need this railtie loaded before our configuration - if this was a gem it would
# happen above in the bundler call.
require_relative '../lib/somewhat_secure_id/railtie'

# For some reason, core_ext misses requiring this. Required for Digest::UUID
require "active_support/core_ext/digest/uuid"

# Force require code used in initializers so autoloading doesn't mess up development
# TODO we should remove autoloading the lib directory and move anything that should
# be autoloaded to app/lib
require_relative "../lib/runtime_settings"
require_relative "../lib/redis_client"
require_relative "../lib/log_long_transactions"
require_relative "../lib/logging"
require_relative "../lib/logging/sql_log_subscriber"

module Swoop
  class Application < Rails::Application

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.default_locale = :de

    # loads locale files in nested dictionaries
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    # disabled for now, ENG-5224
    # config.action_controller.raise_on_unfiltered_parameters = true

    # see ENG-5138
    config.active_record.time_zone_aware_types = [:datetime, :time]

    # TODO - this would be nice to enable?
    config.active_record.belongs_to_required_by_default = false

    config.active_record.schema_format = (ENV["ACTIVE_RECORD_SCHEMA_FORMAT"] == "sql" ? :sql : :ruby)

    # disable assets pipeline
    config.assets.enabled = false

    # and disable assets generators
    config.generators do |g|
      g.assets false
    end

    # Return the current short git SHA. This method can return nil.
    def revision
      RuntimeSettings.revision
    end

    config.permissions = config_for(:permissions)
    config.roles = config_for(:roles)

    # Ensure Google Maps API environment variables exist before booting Rails
    raise CannotBootSwoopAPIWithoutGoogleMapsKeysError unless ENV['GOOGLE_PREMIUM_CLIENT_ID'] && ENV['GOOGLE_PREMIUM_SECRET_KEY'] && ENV['GOOGLE_PREMIUM_CHANNEL']

  end
end

Rails.application.configure do
  config.middleware.delete ActionDispatch::Session::CookieStore
  config.middleware.delete ActionDispatch::Cookies

  require_relative "../app/middleware/report_long_running_request_middleware"
  config.middleware.use ReportLongRunningRequestMiddleware

  require_relative "../app/middleware/stateful_requests"
  config.middleware.use StatefulRequests,
                        '/auth/agero',
                        '/auth/agero/callback',
                        '/auth/partner',
                        '/oauth/authorize',
                        '/auth/email_verifications',
                        Regexp.new('/auth/email_verifications/.*')

  # config.middleware.use "UseRailsLogger"
  # autoload / eager_load doesn't work inside of this initializer so we have to
  # explicitly pull this file in first
  require_relative "../app/middleware/websockets"
  config.middleware.use Websockets
  # Add the request_id formatting middleware
  require_relative "../app/middleware/log_request_id"
  config.middleware.insert_after ActionDispatch::RequestId, LogRequestId

  config.to_prepare do
    Devise::Mailer.layout "mailer"
    Doorkeeper::AuthorizationsController.layout "react_application"

    Doorkeeper::AuthorizationsController.before_action only: :new do
      @version = Swoop::VERSION
      @js = WebpackAssets.instance.js('application')
      @css = WebpackAssets.instance.css('application')

      # all of these come from app/views/doorkeeper/authorizations/new.html.erb
      gon.pre_auth = {
        client_id: pre_auth.client.uid,
        redirect_uri: pre_auth.redirect_uri,
        state: pre_auth.state,
        response_type: pre_auth.response_type,
        scope: pre_auth.scope,
        code_challenge: pre_auth.code_challenge,
        code_challenge_method: pre_auth.code_challenge_method,
        # except this one which we need to provide
        authenticity_token: form_authenticity_token,
      }

      # this is the url we post to
      gon.oauth_authorization_path = oauth_authorization_path

      # our client name
      gon.client_name = pre_auth.client.name
    end
  end

  config.lograge.enabled = true

  config.eager_load_paths << Rails.root.join('lib')

  #  config.logger = ::Logger.new(STDOUT)
  #  config.log_tags = [ :uuid ]

  config.autoload_paths += Dir[Rails.root.join('app', 'lib')]
  config.autoload_paths += Dir[Rails.root.join('app', 'models')]
  # config.autoload_paths += Dir[Rails.root.join('lib')]
  config.autoload_paths += Dir[Rails.root.join('app', 'services')]
  config.autoload_paths += Dir[Rails.root.join('app', 'workers')]
end
