# frozen_string_literal: true

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Attempt to read encrypted secrets from `config/secrets.yml.enc`
  # Requires an encryption key in `ENV["RAILS_MASTER_KEY"]` or
  # `config/secrets.yml.key`.
  # config.read_encrypted_secrets = true

  # Heroku needs us to handle serving static assets
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like
  # NGINX, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  # config.log_level = :warn
  config.log_level = ENV['LOG_LEVEL'] || 'debug'

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Replace the logger from rails_12factor
  STDOUT.sync = true
  logger = ActiveSupport::Logger.new(STDOUT)
  config.logger = ActiveSupport::TaggedLogging.new(logger)

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # Use a real queuing backend for Active Job (and separate queues per environment)
  # config.action_controller.asset_host = 'http://assets.example.com'
  # config.active_job.queue_adapter     = :resque
  # config.active_job.queue_name_prefix = "swoop_#{Rails.env}"
  # config.action_mailer.perform_caching = false

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  # PDW added for devise
  config.action_mailer.asset_host = ENV['SITE_URL']

  # mailer
  config.action_mailer.smtp_settings = {
    address: ENV['MAIL_SERVER'],
    port: 587, # ports 587 and 2525 are also supported with STARTTLS
    enable_starttls_auto: true, # detects and uses STARTTLS
    user_name: ENV['MAIL_USERNAME'],
    password: ENV['MAIL_PASSWORD'],
    authentication: ENV['MAIL_AUTHENTICATION'], # Mandrill supports 'plain' or 'login'
    domain: ENV['MAIL_HOST'], # your domain to identify your server when connecting
  }
  config.action_mailer.default_url_options = { host: ENV['MAIL_HOST'] }

  config.colorize_logging = false

  #  config.active_record.logger = nil #PDW: see ActiveSupport monkey patch below

  if RedisClient.url(:cache)
    config.cache_store =
      :redis_store, RedisClient.url(:cache), {
        namespace: "swc1@#{Rails.application.revision}",
        pool_size: RuntimeSettings.max_db_connections,
        expires_in: Integer(ENV['REDIS_CACHE_EXPIRATION_SECS'] || 600),
      }
  else
    config.cache_store = :null_store
  end
  config.identity_cache_store = config.cache_store
end

module ActiveSupport
  class LogSubscriber

    def debug(*args, &block)
    end

  end
end
