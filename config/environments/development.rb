# frozen_string_literal: true

require 'elasticsearch/rails/instrumentation'
require 'elasticsearch/rails/lograge'

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.
  config.log_level = ENV['LOG_LEVEL'] || 'debug'

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = ENV['DEV_CACHE_CLASSES'].present?

  # Do not eager load code on boot in dev
  # This is recommended rails 5  default setting, do not change without consulting PDW
  # https://github.com/joinswoop/swoop/wiki/Rails-Reloading
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  # config.active_record.verbose_query_logs = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # PDW added for devise
  config.action_mailer.default_url_options = { host: 'localhost', port: 5000 }

  # config.middleware.insert_after ActionDispatch::Static, Rack::LiveReload, :no_swf => true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  # config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  # see https://github.com/rails/rails/issues/24990
  config.file_watcher = ActiveSupport::FileUpdateChecker

  # mailer previews live under spec/
  config.action_mailer.preview_path = "#{Rails.root}/spec/mailers/previews"

  if ENV['TEST_EMAIL']
    config.action_mailer.delivery_method = :letter_opener
  elsif ENV['USE_MAILCATCHER']
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = { # @see https://mailcatcher.me/
      address: ENV['MAIL_SERVER'],
      port: 1025,
    }
  elsif ENV['DELIVER_IN_DEVELOPMENT']
    config.action_mailer.smtp_settings = {
      address: ENV['MAIL_SERVER'],
      port: ENV['MAIL_PORT'] || 587, # ports 587 and 2525 are also supported with STARTTLS
      enable_starttls_auto: true, # detects and uses STARTTLS
      user_name: ENV['MAIL_USERNAME'],
      password: ENV['MAIL_PASSWORD'],
      authentication: ENV['MAIL_AUTHENTICATION'],
      domain: ENV['MAIL_HOST'], # your domain to identify your server when connecting
    }
    config.action_mailer.default_url_options = { host: ENV['MAIL_HOST'] }
  else
    config.action_mailer.perform_deliveries = false
  end

  if RedisClient.url(:cache)
    config.cache_store =
      :redis_store, RedisClient.url(:cache), {
        namespace: "swc1_dev@#{Rails.application.revision}",
        pool_size: RuntimeSettings.max_db_connections,
        expires_in: Integer(ENV['REDIS_CACHE_EXPIRATION_SECS'] || 600),
      }
  else
    config.cache_store = :null_store
  end
  config.identity_cache_store = config.cache_store

  config.middleware.insert_before Rack::Sendfile, ActionDispatch::DebugLocks
  config.middleware.insert_after ActiveRecord::Migration::CheckPending, CheckPendingSeedMigrations
end
