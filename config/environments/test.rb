# frozen_string_literal: true

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.
  # First check `TEST_LOG_LEVEL` (.env.test isn't loaded by `rspec`), then fall through
  # to general `LOG_LEVEL` and ultimately default to `warn`
  config.log_level = ENV.fetch('TEST_LOG_LEVEL', ENV.fetch('LOG_LEVEL', 'warn'))

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure public file server for tests with Cache-Control for performance.
  config.public_file_server.enabled = true
  config.public_file_server.headers = {
    'Cache-Control' => "public, max-age=#{1.hour.to_i}",
  }

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  # Store uploaded files on the local file system in a temporary directory
  # config.active_storage.service = :test

  config.action_mailer.perform_caching = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  if ENV['DELIVER_IN_TEST']
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.perform_deliveries = true
    config.action_mailer.smtp_settings = {
      address: ENV['MAIL_SERVER'],
      port: ENV['MAIL_PORT'] || 587, # ports 587 and 2525 are also supported with STARTTLS
      enable_starttls_auto: true, # detects and uses STARTTLS
      user_name: ENV['MAIL_USERNAME'],
      password: ENV['MAIL_PASSWORD'],
      authentication: ENV['MAIL_AUTHENTICATION'], # Mandrill supports 'plain' or 'login'
      domain: ENV['MAIL_HOST'], # your domain to identify your server when connecting
    }
    config.action_mailer.default_url_options = { host: ENV['MAIL_HOST'] }
  end

  if RedisClient.url(:cache) && ENV["ENABLE_TEST_CACHE"].present?
    config.cache_store =
      :redis_store, RedisClient.url(:cache), {
        namespace: "swc1_test@#{Rails.application.revision}",
        pool_size: RuntimeSettings.max_db_connections,
        expires_in: Integer(ENV['REDIS_CACHE_EXPIRATION_SECS'] || 600),
      }
  else
    config.cache_store = :null_store
  end
  config.identity_cache_store = config.cache_store

  # disable colors on circleci
  config.colorize_logging = false if ENV['CIRCLECI']
end
