# frozen_string_literal: true

json.id vehicle.id
json.name vehicle.description
json.company_id vehicle.company_id
json.lat vehicle.lat
json.lng vehicle.lng
json.driver_id vehicle.driver.try(:id)
json.location_updated_at vehicle.location_updated_at
