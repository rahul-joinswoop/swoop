# frozen_string_literal: true

json.id vehicle.id
json.eta nil
json.name vehicle.name
