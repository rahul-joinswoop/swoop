# frozen_string_literal: true

json.id vehicle.id
json.name vehicle.description
json.lat vehicle.lat
json.lng vehicle.lng
