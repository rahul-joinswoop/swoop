# frozen_string_literal: true

json.array! @makes do |make|
  json.id make.id
  json.name make.name
  json.models make.vehicle_models do |model|
    json.id model.id
    json.name model.name
    json.years model.years
    if model.vehicle_type
      json.vehicle_type model.vehicle_type.name
    end
  end
end
