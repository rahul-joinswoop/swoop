# frozen_string_literal: true

json.id vehicle.id
json.name vehicle.description
json.make vehicle.make
json.model vehicle.model
json.year vehicle.year
json.color vehicle.color
json.tire_size vehicle.tire_size
json.vehicle_category_id vehicle.vehicle_category_id
json.company_id vehicle.company_id
json.lat vehicle.lat
json.lng vehicle.lng
json.company vehicle.company.try(:name)
json.active vehicle.active
json.driver_name vehicle.driver.try(:full_name)
json.driver_id vehicle.driver.try(:id)
json.sequence vehicle.number
json.deleted_at vehicle.deleted_at
json.updated_at vehicle.updated_at
json.location_updated_at vehicle.location_updated_at
if vehicle.dedicated_to_swoop
  json.dedicated_to_swoop vehicle.dedicated_to_swoop
end
