# frozen_string_literal: true

json.array! @eta_explanations, partial: 'api/v1/eta_explanations/eta_explanation', as: :eta_explanation
