# frozen_string_literal: true

json.id event.id
json.channel channel
json.operation event.event_operation.name

if event.target
  json.target do
    json.partial! event.target.show_template, job: event.target
  end
end
