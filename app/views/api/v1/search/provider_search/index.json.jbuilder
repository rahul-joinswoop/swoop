# frozen_string_literal: true

json.meta do
  json.total @total
end

json.providers @providers do |provider|
  json.partial! 'api/v1/providers/provider', provider: provider
end
