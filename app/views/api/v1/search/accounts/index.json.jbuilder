# frozen_string_literal: true

json.meta do
  json.total @total
end

json.accounts @accounts do |account|
  json.partial! 'api/v1/accounts/account', account: account
end
