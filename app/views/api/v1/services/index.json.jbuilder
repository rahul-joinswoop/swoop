# frozen_string_literal: true

json.array! @services, partial: 'api/v1/services/service', as: :service
