# frozen_string_literal: true

if service
  json.id service.id
  json.name service.name
  json.addon service.addon
  json.variable service.variable
  json.is_standard service.is_standard
  json.support_storage service.is_standard

else
  json.id nil
end
