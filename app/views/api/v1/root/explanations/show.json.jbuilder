# frozen_string_literal: true

json.partial! 'api/v1/explanations/explanation', explanation: @explanation
