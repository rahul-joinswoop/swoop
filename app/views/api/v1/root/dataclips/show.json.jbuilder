# frozen_string_literal: true

json.key_format! camelize: :lower

json.dataclip do
  json.call(@dataclip, :id, :created_at, :sql, :values, :executed_in, :last_result_created_at)

  json.name         @dataclip.name.titleize
  json.fields       @dataclip.fields.map(&:titleize)

  json.values       @rows
  json.values_count @rows.total_entries
  json.page_count   @rows.total_pages

  json.authenticated_export_url authenticated_export_url(@dataclip)
end
