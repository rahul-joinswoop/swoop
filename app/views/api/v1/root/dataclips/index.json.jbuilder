# frozen_string_literal: true

json.key_format! camelize: :lower
json.dataclips @dataclips do |dataclip|
  json.call(dataclip, :id, :created_at, :last_result_created_at)
  json.name     dataclip.name.titleize
  json.running  dataclip.running?
  json.authenticated_export_url authenticated_export_url(dataclip)
end
