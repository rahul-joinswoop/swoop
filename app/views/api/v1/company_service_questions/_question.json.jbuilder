# frozen_string_literal: true

json.service_id  question.service_code_id
json.question    question.question.question
json.question_id question.question.id
json.name question.question.name
json.order_num question.question.order_num

json.answers question.question.answers do |answer|
  json.id answer.id
  json.answer answer.answer
end
