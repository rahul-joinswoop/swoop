# frozen_string_literal: true

json.array! @questions, partial: 'api/v1/company_service_questions/question', as: :question
