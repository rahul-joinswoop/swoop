# frozen_string_literal: true

json.assigning_company_name @invoice_rejection.assigning_company_name
json.reject_reason @invoice_rejection.reject_reason
json.notes @invoice_rejection.notes
json.invoice_id @invoice_rejection.invoice_id
