# frozen_string_literal: true

json.array! @reasons, partial: 'api/v1/invoice_rejections/reason', as: :reason
