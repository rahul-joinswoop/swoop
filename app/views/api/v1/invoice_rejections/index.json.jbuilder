# frozen_string_literal: true

json.array! @invoice_rejections,
            partial: 'api/v1/invoice_rejections/invoice_rejection', as: :invoice_rejection
