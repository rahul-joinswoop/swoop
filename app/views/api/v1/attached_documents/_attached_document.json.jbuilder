# frozen_string_literal: true

json.id attached_document.id
json.original_filename attached_document.original_filename
json.url attached_document.url
json.job_id attached_document.job_id if attached_document.job_id
json.company_id attached_document.company_id if attached_document.company_id
json.deleted_at attached_document.deleted_at
json.created_at attached_document.created_at
json.type attached_document.type
json.container attached_document.container
json.invoice_attachment attached_document.invoice_attachment
if attached_document.location
  json.location do
    json.partial! 'api/v1/locations/location', location: attached_document.location
  end
end
