# frozen_string_literal: true

json.partial!('api/v1/attached_documents/attached_document', {
  attached_document: @attached_document,
})
