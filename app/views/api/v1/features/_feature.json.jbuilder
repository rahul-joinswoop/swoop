# frozen_string_literal: true

json.id feature.id
json.name feature.name
json.description feature.description
