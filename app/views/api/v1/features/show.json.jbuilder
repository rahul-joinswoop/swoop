# frozen_string_literal: true

json.partial! 'api/v1/features/feature', feature: @feature
