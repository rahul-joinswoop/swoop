# frozen_string_literal: true

json.array! @features, partial: 'api/v1/features/feature', as: :feature
