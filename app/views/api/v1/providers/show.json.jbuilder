# frozen_string_literal: true

json.partial! 'api/v1/providers/provider', { provider: @provider }
