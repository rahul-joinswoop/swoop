# frozen_string_literal: true

json.id provider.id
json.name provider.name
json.site_id provider.site_id
if provider.site
  json.tire_program provider.site.tire_program
else
  json.tire_program false
end

json.phone provider.phone_or_site_phone

json.accounting_email provider.accounting_email
json.dispatch_email provider.dispatch_email

json.primary_contact provider.primary_contact
json.primary_email provider.primary_email
json.primary_phone provider.primary_phone

json.notes provider.notes
json.status provider.status

json.tags provider.tags.collect { |t| t.id }

if provider.location
  json.location do
    json.partial! 'api/v1/locations/location', location: provider.location
  end
elsif provider.site.location
  json.location do
    json.partial! 'api/v1/locations/location', location: provider.site.location
  end
else
  json.location nil
end
json.deleted_at provider.deleted_at

json.company do
  json.partial! 'api/v1/companies/public_company', { company: provider.provider }
end

if provider.job_distance
  json.job_distance provider.job_distance
end

if provider.provider.features.select { |f| f.name == "Canadian AP" }.any?
  json.canadian true
end

json.fax provider.fax
json.live provider.live
json.location_code provider.location_code
json.vendor_code provider.vendor_code
json.auto_assign_distance provider.auto_assign_distance
json.billing_type provider.provider.billing_type
