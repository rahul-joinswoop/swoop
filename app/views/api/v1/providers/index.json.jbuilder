# frozen_string_literal: true

json.array! @providers do |provider|
  json.partial! 'api/v1/providers/provider', provider: provider
end
