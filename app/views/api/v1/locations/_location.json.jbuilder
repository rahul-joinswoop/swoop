# frozen_string_literal: true

json.id location.id
json.address location.address
json.lat location.lat
json.lng location.lng
json.street location.street
json.street_number location.street_number
json.street_name location.street_name
json.city location.city
json.state location.state
json.zip location.zip
json.exact location.exact
json.updated_at location.updated_at

json.place_id location.place_id

json.site_id location.site_id

json.location_type_id location.location_type_id
