# frozen_string_literal: true

json.partial! 'api/v1/trailers/trailer', trailer: @trailer
