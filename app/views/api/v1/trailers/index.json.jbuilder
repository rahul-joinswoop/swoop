# frozen_string_literal: true

json.array! @trailers, partial: 'api/v1/trailers/trailer', as: :trailer
