# frozen_string_literal: true

json.id trailer.id
json.name trailer.description
json.make trailer.make
json.model trailer.model
json.year trailer.year
json.vehicle_category_id trailer.vehicle_category_id
json.company_id trailer.company_id
json.company trailer.company.try(:name)
json.deleted_at trailer.deleted_at
