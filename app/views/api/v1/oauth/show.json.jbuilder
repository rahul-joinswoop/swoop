# frozen_string_literal: true

json.partial! 'api/v1/oauth/oauth', oauth_access_token: @oauth_access_token
