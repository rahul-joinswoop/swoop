# frozen_string_literal: true

json.access_token oauth_access_token.token
json.token_type "bearer"
json.expires_in oauth_access_token.expires_in
