# frozen_string_literal: true

json.id invoice.job.id
json.customer_type_name invoice.job.customer_type.try(:name)
json.name invoice.job.customer.full_name
if invoice.job.customer.location
  json.location do
    json.partial! 'api/v1/locations/location', location: job.customer.location
  end
else
  json.location nil
end
json.driver invoice.job.rescue_driver.try(:full_name)

json.make invoice.job.stranded_vehicle.make
json.model invoice.job.stranded_vehicle.model
json.vehicle_type invoice.job.stranded_vehicle.vehicle_type
json.serial_number invoice.job.stranded_vehicle.serial_number
json.year invoice.job.stranded_vehicle.year
json.color invoice.job.stranded_vehicle.color
json.vin invoice.job.stranded_vehicle.vin
json.license invoice.job.stranded_vehicle.license
json.po_number invoice.job.po_number
json.unit_number invoice.job.unit_number
json.ref_number invoice.job.ref_number
if invoice.job.odometer
  json.odometer invoice.job.odometer.round
else
  json.odometer nil
end

if invoice.job.service_location
  json.service_location do
    json.partial! 'api/v1/locations/location', location: invoice.job.service_location
  end
end
if invoice.job.drop_location
  json.drop_location do
    json.partial! 'api/v1/locations/location', location: invoice.job.drop_location
  end
end

if invoice.job.rescue_vehicle
  json.rescue_vehicle do
    json.partial! 'api/v1/vehicles/job_rescue_vehicle', vehicle: invoice.job.rescue_vehicle
  end
end

if invoice.job.rescue_driver
  json.rescue_driver do
    json.partial! 'api/v1/users/user', user: invoice.job.rescue_driver
  end
end

json.po_number invoice.job.po_number

json.history do
  invoice.job.audit_job_statuses_as_hash.each do |k, v|
    json.set! k do
      json.partial! 'api/v1/status_history/status_history', audit_job_status: v
    end
  end
end

json.notes invoice.job.notes
json.driver_notes invoice.job.driver_notes
json.service invoice.job.service_name

json.invoice_vehicle_category_id invoice.job.invoice_vehicle_category_id

if invoice.job.priority_response
  json.priority_response invoice.job.priority_response
end

json.invoice_restrictions job.invoice_restrictions?
