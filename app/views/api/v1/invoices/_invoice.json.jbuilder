# frozen_string_literal: true

# TODO move this to an ActiveModel Serializer
json.id invoice.id
json.partner_alert invoice.partner_alert
json.fleet_alert invoice.fleet_alert
json.state invoice.state

if invoice.paid
  json.paid invoice.paid
end

if invoice.rate_type
  json.rate_type invoice.rate_type
else
  if invoice.sender_id != Company.swoop_id
    json.rate_type invoice.rate_type
  end
end

if invoice.billing_type
  json.billing_type invoice.billing_type
end

json.total_amount number_with_precision(invoice.total_amount, precision: 2)
json.tax_amount number_with_precision(invoice.tax_amount, precision: 2)

json.sender do
  json.partial! 'api/v1/companies/company', company: invoice.sender, accounts: false, sites: false, territories: false
end

if invoice.recipient
  json.recipient_id invoice.recipient_id
  json.recipient_type invoice.recipient_type
else
  json.recipient_id nil
  json.recipient_type nil
end

json.recipient do
  json.recipient_type invoice.recipient_type

  if invoice.is_enterprise?
    json.site_name invoice.job.fleet_site_name
  end

  if invoice.recipient_type == "Account"
    # TODO: pass the account_id through instead and lazy load
    json.partial! 'api/v1/accounts/account', account: invoice.account_recipient
  elsif invoice.recipient_type == "User"
    recipient = invoice.user_recipient
    json.id recipient.id
    json.full_name recipient.full_name
    json.phone recipient.phone
    json.email invoice.job.email # warn this is not the recipeints email. it's the jobs
    if recipient.location
      json.location do
        json.partial! 'api/v1/locations/location', location: recipient.location
      end
    end
  end
end

json.job_id invoice.job.id
if @inline_jobs
  json.job do
    json.partial! 'api/v1/invoices/job', invoice: invoice
  end
end

if invoice.job.site
  json.site do
    json.partial! 'api/v1/sites/site', site: invoice.job.site
  end
end

if !invoice.invoice_rejections.empty?
  json.invoice_rejection do
    json.reject_reason invoice.invoice_rejections.last.reject_reason
    json.notes invoice.invoice_rejections.last.notes
  end
end

json.line_items do
  json.array!(invoice.line_items_include_original_deleted(@include_original_deleted_line_items)) do |line_item|
    json.id line_item.id
    if line_item.calc_type
      json.calc_type line_item.calc_type
    end
    json.quantity line_item.quantity
    json.currency line_item.currency
    json.unit_price number_with_precision(line_item.unit_price, precision: 2)
    json.net_amount number_with_precision(line_item.net_amount, precision: 2)
    json.free line_item.free
    json.original_quantity line_item.original_quantity
    json.original_unit_price number_with_precision(line_item.original_unit_price, precision: 2)
    json.user_created line_item.user_created
    json.description line_item.description
    # json.notes line_item.notes
    json.tax_amount number_with_precision(line_item.tax_amount, precision: 2)
    json.estimated line_item.estimated
    json.job_id line_item.job_id
    json.rate_id line_item.rate_id
    json.deleted_at line_item.deleted_at
    json.auto_calculated_quantity line_item.auto_calculated_quantity
  end
end

if invoice.email_to
  json.email_to invoice.email_to
end

if invoice.email_cc
  json.email_cc invoice.email_cc
end

if invoice.email_bcc
  json.email_bcc invoice.email_bcc
end

json.driver_notes invoice.job.driver_notes
json.uuid invoice.uuid
json.sent_at invoice.sent_at
json.error invoice.error
json.deleted_at invoice.deleted_at
json.created_at invoice.created_at
json.updated_at invoice.updated_at

json.fleet_managed_invoice_approved_or_sent invoice.fleet_managed_invoice_approved_or_sent?

json.qb_imported_at invoice.qb_imported_at
json.qb_pending_import invoice.qb_pending_import

json.payments ActiveModelSerializers::SerializableResource.new(
  invoice.payments_or_credits
).serializable_hash

if invoice.export_error_msg
  json.export_error_msg invoice.export_error_msg
end

json.balance invoice.balance
json.currency invoice.currency
