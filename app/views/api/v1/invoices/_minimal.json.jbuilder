# frozen_string_literal: true

json.id invoice.id
json.state invoice.state
json.currency invoice.currency

# this is needed because invoice.rate_type will not be set by default,
# only when user change its rate type on the UI
# TODO rate system refactoring!
json.rate_type invoice.rate_type

json.total_amount number_with_precision(invoice.total_amount, precision: 2)
json.tax_amount number_with_precision(invoice.tax_amount, precision: 2)
json.uuid invoice.uuid
# if invoice.sender
json.sender_id invoice.sender_id
# end
# if invoice.recipient
json.recipient_company_id invoice.recipient_id
# end
