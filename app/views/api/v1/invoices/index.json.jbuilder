# frozen_string_literal: true

json.total @invoices_total
json.invoices @invoices do |invoice|
  json.partial! 'api/v1/invoices/invoice', invoice: invoice
end
