# frozen_string_literal: true

json.id toa.id
json.eba_type toa.eba_type
json.job_id toa.job_id
json.time toa.time
