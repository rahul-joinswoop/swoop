# frozen_string_literal: true

json.array! @issues, partial: 'api/v1/issues/issue', as: :issue
