# frozen_string_literal: true

json.partial! 'api/v1/issues/issue_attributes', issue: issue
json.reasons do
  json.array! issue.reasons, partial: 'api/v1/reasons/reason_attributes', as: :reason
end
