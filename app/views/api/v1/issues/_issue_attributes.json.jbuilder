# frozen_string_literal: true

json.id issue.id
json.name issue.name
json.key issue.key
