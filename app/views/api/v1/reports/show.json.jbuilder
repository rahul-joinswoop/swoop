# frozen_string_literal: true

json.partial! 'api/v1/reports/report', company_report: @company_report
