# frozen_string_literal: true

json.array! @company_reports do |company_report|
  json.partial! 'api/v1/reports/report', company_report: company_report.handle_global(@api_company)
end
