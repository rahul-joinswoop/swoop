# frozen_string_literal: true

json.id company_report.report.id
if company_report.report.display_name
  json.title         company_report.report.display_name
else
  json.title         company_report.report.name
end
json.filters       company_report.report.processed_available_params(current_company_report: company_report, user: @api_user)
json.pdf           company_report.report.pdf
if company_report.report.display_order
  json.display_order company_report.report.display_order
end
