# frozen_string_literal: true

json.territories @territories, partial: 'api/v1/partner/territories/territory', as: :territory
