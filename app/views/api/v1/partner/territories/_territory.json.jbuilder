# frozen_string_literal: true

json.id    territory.id
json.paths territory.coordinates do |coordinates|
  json.lat coordinates[:lat]
  json.lng coordinates[:lng]
end
json.deleted_at territory.deleted_at
