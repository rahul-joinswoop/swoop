# frozen_string_literal: true

json.territory @territory, partial: 'api/v1/partner/territories/territory', as: :territory
