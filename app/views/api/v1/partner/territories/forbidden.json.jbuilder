# frozen_string_literal: true

json.message "You are authenticated, but unauthorized to access this resource"
