# frozen_string_literal: true

json.partial! 'api/v1/invoices/invoice', invoice: @invoice
