# frozen_string_literal: true

json.partial! 'api/v1/companies/company', { company: @company, accounts: true }
json.user_id @user.id
