# frozen_string_literal: true

json.id setting.id
json.user_id setting.user_id
json.key setting.key
json.value setting.value
