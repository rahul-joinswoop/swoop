# frozen_string_literal: true

json.partial! 'api/v1/user_settings/setting', { setting: @setting }
