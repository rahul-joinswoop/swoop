# frozen_string_literal: true

json.array! @services, partial: 'api/v1/rate_additions/rate_addition', as: :rate_addition
