# frozen_string_literal: true

json.id rate_addition.id
json.name rate_addition.name
json.calc_type rate_addition.calc_type
json.currency rate_addition.currency
if rate_addition.calc_type == "flat"
  json.amount number_with_precision(rate_addition.amount, precision: 2)
else
  json.amount number_with_precision(rate_addition.amount, strip_insignificant_zeros: true)
end
if rate_addition.deleted_at
  json.deleted_at rate_addition.deleted_at
end
