# frozen_string_literal: true

json.id setting.id
json.key setting.key
json.value setting.value
