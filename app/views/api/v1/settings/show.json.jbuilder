# frozen_string_literal: true

json.partial! 'api/v1/settings/setting', { setting: @setting }
