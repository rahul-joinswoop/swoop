# frozen_string_literal: true

json.array! @settings do |setting|
  json.partial! 'api/v1/settings/setting', setting: setting
end
