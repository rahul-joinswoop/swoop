# frozen_string_literal: true

json.id account.id
json.name account.name
json.accounting_email account.accounting_email
json.primary_email account.primary_email
json.notes account.notes
json.phone account.phone
json.fax account.fax
json.contact_name account.contact_name
json.contact_phone account.contact_phone
json.contact_email account.contact_email
json.accounting_email_cc account.accounting_email_cc
json.accounting_email_bcc account.accounting_email_bcc
json.disable_qb_import account.disable_qb_import
json.hide_timestamps_on_invoice account.hide_timestamps_on_invoice

if account.location
  json.location do
    json.partial! 'api/v1/locations/location', location: account.location
  end
else
  json.location nil
end

if account.physical_location
  json.physical_location do
    json.partial! 'api/v1/locations/location', location: account.physical_location
  end
else
  json.physical_location nil
end

if account.client_company
  json.company_id account.client_company.id
  # Deprecated#
  json.company do
    json.partial! 'api/v1/companies/public_company', { company: account.client_company }
  end
  ###########
else
  json.company nil
end

json.color account.color
if account.client_company
  if account.rsc_enabled?
    json.accountIntegrations(account.company.api_access_tokens) do |access_token|
      json.vendorId access_token.vendorid
      json.status access_token.current_status
      json.allAuthorizedLocations access_token.all_associated_location_ids
    end
  end

  json.dispatchable_sites(account.company.partner_sites) do |site|
    json.site_id site.id
    json.site_dispatchable site.dispatchable
    json.enabled site.dispatchable_by?(account.client_company)
    if account.rsc_enabled?
      json.locationid site.facility(account.client_company)&.facility_id
    else
      json.locationid site.issc_locationid(account.client_company)
    end
    json.contractorids site.contractorids(account.client_company)
    if site.issc_login(account.client_company)
      json.username site.issc_login(account.client_company)[:username]
      json.password site.issc_login(account.client_company)[:password]
      json.incorrect site.issc_login(account.client_company)[:incorrect] == true
    end

    isscs = site.isscs_by_fleet(account.client_company).group_by(&:contractorid).values.collect(&:first)

    json.isscs(isscs) do |issc|
      json.clientid issc.clientid
      json.contractorid issc.contractorid

      if account.rsc_enabled? && issc.api_access_token.present? && !issc.api_access_token.connected?
        json.status "disconnected"
      else
        json.status issc.status
      end

      json.logged_in_at issc.logged_in_at
      json.system issc.system
      if issc.delete_pending_at
        json.delete_pending_at issc.delete_pending_at
      end
    end
  end
end

if account.rsc_enabled?
  json.unconfigured_vendor_ids account.company.api_access_tokens
    .select { |token| token.location_ids_with_site.blank? }
    .map(&:vendorid)
end

if account.po_required_at_job_creation
  json.po_required_at_job_creation account.po_required_at_job_creation
end
if account.po_required_before_sending_invoice
  json.po_required_before_sending_invoice account.po_required_before_sending_invoice
end

json.auto_accept account.auto_accept
json.auto_eta account.auto_eta
if account.department_id
  json.department_id account.department_id
end
json.deleted_at account.deleted_at
