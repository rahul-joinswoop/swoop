# frozen_string_literal: true

json.array! @accounts do |account|
  json.partial! 'api/v1/accounts/account', account: account
end
