# frozen_string_literal: true

json.array! @rates do |rate|
  json.partial! 'api/v1/rates/rate', rate: rate
end
