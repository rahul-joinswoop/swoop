# frozen_string_literal: true

json.id rate.id
json.type rate.type
json.company_id rate.company_id
json.currency rate.currency
json.service_code_id rate.service_code_id
if @include_service_name
  json.service_code do
    json.partial! 'api/v1/services/service', service: rate.service_code
  end
end
json.site_id rate.site_id
json.vendor_id rate.vendor_id
json.account_id rate.account_id
if rate.account
  json.fleet_company_id rate.account.client_company_id
end
json.vehicle_category_id rate.vehicle_category_id
json.live rate.live
json.service_group_id rate.service_group_id
json.account_group_id rate.account_group_id

if rate.has_attribute?(:priority)
  json.priority rate.priority
end

# json.prices do
# json.array! rate.prices, partial: 'api/v1/rates/price', as: :price
# end

json.hourly number_with_precision(rate.hourly, precision: 2)
json.flat number_with_precision(rate.flat, precision: 2)
json.hookup number_with_precision(rate.hookup, precision: 2)
json.miles_towed number_with_precision(rate.miles_towed, precision: 2)
json.miles_towed_free number_with_precision(rate.miles_towed_free, precision: 2)
json.miles_enroute number_with_precision(rate.miles_enroute, precision: 2)
json.miles_enroute_free number_with_precision(rate.miles_enroute_free, precision: 2)
json.miles_p2p number_with_precision(rate.miles_p2p, precision: 2)
json.miles_p2p_free number_with_precision(rate.miles_p2p_free, precision: 2)
json.miles_deadhead number_with_precision(rate.miles_deadhead, precision: 2)
json.miles_deadhead_free number_with_precision(rate.miles_deadhead_free, precision: 2)
json.special_dolly number_with_precision(rate.special_dolly, precision: 2)
json.gone number_with_precision(rate.gone, precision: 2)
json.storage_daily number_with_precision(rate.storage_daily, precision: 2)

json.additions do
  json.array! rate.additions, partial: 'api/v1/rate_additions/rate_addition', as: :rate_addition
end

if rate.has_attribute?(:second_day_charge)
  json.second_day_charge rate.second_day_charge
end
if rate.has_attribute?(:beyond_charge)
  json.beyond_charge rate.beyond_charge
end
if rate.has_attribute?(:seconds_free_window)
  json.seconds_free_window rate.seconds_free_window
end
if rate.has_attribute?(:round)
  json.round rate.round
end
if rate.has_attribute?(:nearest)
  json.nearest rate.nearest
end

json.created_at rate.created_at
json.updated_at rate.updated_at
json.deleted_at rate.deleted_at
