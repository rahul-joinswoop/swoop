# frozen_string_literal: true

json.id price.id
json.type price.type
json.cost price.cost
