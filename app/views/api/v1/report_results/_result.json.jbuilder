# frozen_string_literal: true

json.id report_result.id
json.report_id report_result.report_id
json.state report_result.state
json.s3_filename report_result.s3_filename
json.empty report_result.empty
