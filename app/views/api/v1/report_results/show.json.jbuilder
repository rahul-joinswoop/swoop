# frozen_string_literal: true

json.partial! 'api/v1/report_results/result', report_result: @report_result
