# frozen_string_literal: true

json.array! @job_summary do |line|
  json.action line.action
end
