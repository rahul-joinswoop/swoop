# frozen_string_literal: true

json.partial! 'api/v1/metrics/metric', { metric: @metric }
