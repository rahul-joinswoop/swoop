# frozen_string_literal: true

json.id image.id
json.url image.url
json.lat image.lat
json.lng image.lng
json.user_id image.user_id
json.job_id image.job_id
json.creted_at image.created_at
json.updated_at image.updated_at
json.deleted_at image.deleted_at
