# frozen_string_literal: true

json.id reply.id
json.body reply.body

if reply.target
  json.job_id(reply.target.is_a?(Job) ? reply.target.id : reply.target.job_id)
end
