# frozen_string_literal: true

json.id user.id
json.first_name user.first_name
json.last_name user.last_name
json.full_name user.full_name
json.phone user.phone
json.pager user.pager
json.email user.email
json.language user.language
json.last_sign_in_at user.last_sign_in_at
json.license_state user.license_state
json.license_id user.license_id
json.timezone user.timezone
json.username user.username
json.deleted_at user.deleted_at
json.custom_account_visible user.custom_account_visible?
json.member_number user.member_number

if !local_assigns.fetch :customer_data_only, false
  json.on_duty user.on_duty
  json.site_ids user.site_ids
  json.job_status_email_frequency user.job_status_email_frequency
  json.job_status_email_preferences user.job_status_email_status_names
  if user.receive_system_alerts
    json.receive_system_alerts user.receive_system_alerts
  end
  if user.company
    json.company_id user.company.id
    json.company_name user.company.name
  else
    json.company_id nil
    json.company_name nil
  end

  json.roles user.human_roles
end

if user.location
  json.location do
    json.partial! 'api/v1/locations/location', location: user.location
  end
end

if user.vehicle
  json.driving_vehicle_id user.vehicle_id
else
  json.driving_vehicle_id nil
end

if user.storage_relationship
  json.storage_relationship user.storage_relationship
end

if user.system_email_filters.present?
  json.system_email_filtered_companies user.system_email_filters.collect(&:company_id)
end

json.mobile_platform user.platform
json.mobile_version user.version
json.mobile_build user.build

json.mobile_user_agent_human user.user_agent_human

# only load notification_settings if include_notification_settings
if defined?(include_notification_settings) && include_notification_settings
  json.notification_settings ActiveModelSerializers::SerializableResource.new(
    user.notification_settings,
    each_serializer: UserNotificationSettingSerializer
  ).serializable_hash
else
  # otherwise, empty array
  json.notification_settings []
end
