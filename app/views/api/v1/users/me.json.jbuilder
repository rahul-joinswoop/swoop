# frozen_string_literal: true

json.id user.id
json.first_name user.first_name
json.last_name user.last_name
json.full_name user.full_name
json.phone user.phone
json.email user.email
json.language user.language
json.last_sign_in_at user.last_sign_in_at
json.on_duty user.on_duty
json.timezone user.timezone
json.deleted_at user.deleted_at
json.site_ids user.site_ids
json.job_status_email_frequency user.job_status_email_frequency
json.job_status_email_preferences user.job_status_email_status_names
json.custom_account_visible user.custom_account_visible?

if user.company
  json.roles user.human_roles
  json.username user.username
  json.company_id user.company.id
  json.role_ids user.roles.map(&:id)

  json.company do
    json.partial! 'api/v1/companies/company', { company: user.company, accounts: false }
  end

  if user.location
    json.location do
      json.partial! 'api/v1/locations/location', location: user.location
    end
  end
else
  json.company_id nil
end

if user.vehicle
  json.driving_vehicle_id user.vehicle_id
else
  json.driving_vehicle_id nil
end

if defined?(include_notification_settings) && include_notification_settings
  json.notification_settings ActiveModelSerializers::SerializableResource.new(
    user.notification_settings,
    each_serializer: UserNotificationSettingSerializer
  ).serializable_hash
else
  # otherwise, empty array
  json.notification_settings []
end
