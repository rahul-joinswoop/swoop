# frozen_string_literal: true

json.uuid password_request.uuid

if password_request.user
  json.username password_request.user.username
end
