# frozen_string_literal: true

json.array! @users, partial: 'api/v1/users/user', as: :user, include_notification_settings: @include_notification_settings
