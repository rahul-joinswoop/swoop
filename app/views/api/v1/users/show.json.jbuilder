# frozen_string_literal: true

json.partial! 'api/v1/users/user', user: @user, include_notification_settings: @include_notification_settings
