# frozen_string_literal: true

json.user_input password_request.user_input

if password_request.user.present?
  json.user_id password_request.user.id
  json.email password_request.user.email
  json.phone password_request.user.phone
end
