# frozen_string_literal: true

json.id @custom_account.id
json.complete @custom_account.complete?
json.verification_failure @custom_account.verification_failure?

if @custom_account.complete?
  json.upstream_account_id @custom_account.upstream_account_id
end

if @bank_account
  json.linked true
  json.bank_account do
    json.bank_name @bank_account.bank_name
    json.last4 @bank_account.last4
  end
else
  json.linked false
end

json.deleted_at @custom_account.deleted_at
