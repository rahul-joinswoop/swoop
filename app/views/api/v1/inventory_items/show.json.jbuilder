# frozen_string_literal: true

json.partial! 'api/v1/inventory_items/inventory_item', inventory_item: @inventory_item, controller: @controller
