# frozen_string_literal: true

json.array! @inventory_items do |inventory_item|
  json.partial! 'api/v1/inventory_items/inventory_item', inventory_item: inventory_item
end
