# frozen_string_literal: true

json.id inventory_item.id
json.site_id inventory_item.site_id
json.job_id inventory_item.job_id
json.deleted_at inventory_item.deleted_at
if inventory_item.item_type == "Tire"
  json.item do
    json.partial! 'api/v1/tires/tire', tire: inventory_item.item
    json.type inventory_item.item_type
  end

elsif inventory_item.item_type == "Vehicle"
  json.item do
    json.partial! 'api/v1/vehicles/vehicle', vehicle: inventory_item.item
    json.type inventory_item.item_type
  end
  json.stored_at inventory_item.stored_at
  json.released_at inventory_item.released_at
  json.released_to_account_id inventory_item.released_to_account_id
  json.replace_invoice_info inventory_item.replace_invoice_info

  json.released_to_user_id inventory_item.released_to_user_id
  if inventory_item.released_to_user
    json.released_to_user do
      json.partial! 'api/v1/users/user', user: inventory_item.released_to_user
    end
  else
    json.released_to_user nil
  end
  json.released_to_email inventory_item.released_to_email
end
