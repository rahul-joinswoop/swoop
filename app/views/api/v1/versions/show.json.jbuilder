# frozen_string_literal: true

json.partial! 'api/v1/versions/version', version: @version
