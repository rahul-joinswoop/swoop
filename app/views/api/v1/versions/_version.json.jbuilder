# frozen_string_literal: true

if version
  json.id version.id
  json.name version.name
  json.version version.version
  json.description version.description
  json.forced version.forced
  json.delay version.delay
  json.created_at version.created_at
  if ENV['SWOOP_ENV']
    if ENV['SWOOP_ENV'] == "production"
      json.swoop_env ''
    else
      json.swoop_env ENV['SWOOP_ENV']
    end
  else
    json.swoop_env 'Local'
  end
end
