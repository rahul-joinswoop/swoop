# frozen_string_literal: true

show_company_json = defined?(show_company) ? show_company : true
company = site.company
fleet_rescue_provider = site.rescue_provider_for_company(fleet_company)

json.id site.id
json.type site.type
json.name site.name
json.name_with_company site.name_with_company

if show_company_json
  json.company do
    json.id company.id
    json.name company.name
    json.type company.type
    json.dispatch_email company.dispatch_email
    json.phone company.phone
    json.accounting_email company.accounting_email
    json.fax company.fax

    if company.location
      json.location do
        json.partial! 'api/v1/locations/location', location: company.location
      end
    else
      json.location nil
    end
  end
end

json.location_id site.location_id
json.tz site.tz
json.phone site.phone

if site.location
  json.location do
    json.partial! 'api/v1/locations/location', location: site.location
  end
else
  json.location nil
end

if site.site_code
  json.site_code site.site_code
end

json.dispatchable site.dispatchable
json.tire_program site.tire_program

json.dispatchable_by_fleet dispatchable_by_fleet(fleet_rescue_provider)

if fleet_rescue_provider
  json.fleet_rescue_provider do
    json.id fleet_rescue_provider.id
    json.phone fleet_rescue_provider.phone

    json.accounting_email fleet_rescue_provider.accounting_email
    json.dispatch_email fleet_rescue_provider.dispatch_email

    json.primary_contact fleet_rescue_provider.primary_contact
    json.primary_email fleet_rescue_provider.primary_email
    json.primary_phone fleet_rescue_provider.primary_phone

    json.notes fleet_rescue_provider.notes
    json.status fleet_rescue_provider.status

    json.deleted_at fleet_rescue_provider.deleted_at

    json.location_code fleet_rescue_provider.location_code
    json.vendor_code fleet_rescue_provider.vendor_code
  end
else
  json.fleet_rescue_provider nil
end
