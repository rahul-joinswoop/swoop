# frozen_string_literal: true

json.array! @sites do |site|
  json.partial! "api/v1/sites/partner_site",
                site: site, fleet_company: @fleet_company
end
