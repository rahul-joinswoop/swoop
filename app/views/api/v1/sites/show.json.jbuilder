# frozen_string_literal: true

json.partial! 'api/v1/sites/site', site: @site
