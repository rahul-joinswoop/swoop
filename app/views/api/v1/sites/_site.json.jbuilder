# frozen_string_literal: true

json.id site.id
json.type site.type
json.name site.name
json.company_id site.company_id
json.location_id site.location_id
json.tz site.tz
json.phone site.phone
if site.location
  json.location do
    json.partial! 'api/v1/locations/location', location: site.location
  end
else
  json.location nil
end
json.providerCompanies do
  json.array! site.providerCompanyNames.map do |provider|
    json.id provider.id
    json.name provider.name
    json.in_house provider.in_house
  end
end

if site.open_time
  json.open_time site.open_time.to_s(:time)
else
  json.open_time nil
end

if site.close_time
  json.close_time site.close_time.to_s(:time)
else
  json.close_time nil
end

if site.open_time_sat
  json.open_time_sat site.open_time_sat.to_s(:time)
else
  json.open_time_sat nil
end

if site.close_time_sat
  json.close_time_sat site.close_time_sat.to_s(:time)
else
  json.close_time_sat nil
end

if site.open_time_sun
  json.open_time_sun site.open_time_sun.to_s(:time)
else
  json.open_time_sun nil
end

if site.close_time_sun
  json.close_time_sun site.close_time_sun.to_s(:time)
else
  json.close_time_sun nil
end

if site.site_code
  json.site_code site.site_code
end

json.open site.within_hours
json.dispatchable site.dispatchable
json.deleted_at site.deleted_at
json.tire_program site.tire_program
json.storage_lot site.storage_lot
json.always_open site.always_open
json.owner_company_id site.owner_company_id
json.force_open site.force_open

current_user = controller.api_user? if defined?(controller.api_user?)
json.user_site(current_user ? current_user.user_site?(site) : true)
