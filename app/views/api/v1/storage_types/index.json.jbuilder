# frozen_string_literal: true

json.array! @storage_types do |storage_type|
  json.partial! 'api/v1/storage_types/storage_type', storage_type: storage_type
end
