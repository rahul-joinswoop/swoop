# frozen_string_literal: true

json.id storage_type.id
json.name storage_type.name
json.deleted_at storage_type.deleted_at
