# frozen_string_literal: true

json.partial! 'api/v1/storage_types/storage_type', { storage_type: @storage_type }
