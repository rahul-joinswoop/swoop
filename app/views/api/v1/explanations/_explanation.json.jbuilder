# frozen_string_literal: true

json.id explanation.id
json.job_id explanation.job_id
json.reason_info explanation.reason_info
json.deleted_at explanation.deleted_at
json.reason do
  json.partial! 'api/v1/reasons/reason', reason: explanation.reason
end
