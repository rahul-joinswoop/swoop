# frozen_string_literal: true

json.array! @subscription_statuses, partial: 'api/v1/subscription_statuses/status', as: :status
