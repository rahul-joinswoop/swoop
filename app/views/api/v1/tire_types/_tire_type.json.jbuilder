# frozen_string_literal: true

json.id tire_type.id
json.car tire_type.car
json.position tire_type.position
json.extras tire_type.extras
json.size tire_type.size
