# frozen_string_literal: true

json.array! @tire_types, partial: 'api/v1/tire_types/tire_type', as: :tire_type
