# frozen_string_literal: true

json.array! @tags, partial: 'api/v1/tags/tag', as: :tag
