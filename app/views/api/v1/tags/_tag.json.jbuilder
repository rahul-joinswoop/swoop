# frozen_string_literal: true

json.id tag.id
json.name tag.name
json.color tag.color
