# frozen_string_literal: true

json.total @total
json.users @users do |user|
  json.partial! 'api/v1/users/user', user: user, include_notification_settings: @include_notification_settings
end
