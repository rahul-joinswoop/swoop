# frozen_string_literal: true

json.partial! 'api/v1/jobs/job', job: @job, controller: @controller
