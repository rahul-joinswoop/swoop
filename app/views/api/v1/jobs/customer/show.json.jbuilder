# frozen_string_literal: true

json.partial! 'api/v1/jobs/customer/job', job: @job
