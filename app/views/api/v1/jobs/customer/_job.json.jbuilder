# frozen_string_literal: true

json.id job.id
if job.service_location
  json.service_location do
    json.partial! 'api/v1/locations/location', location: job.service_location
  end
end
if job.drop_location
  json.drop_location do
    json.partial! 'api/v1/locations/location', location: job.drop_location
  end
end

if job.send_sms_to_pickup
  if job.pickup_contact
    json.recipient do
      json.partial! 'api/v1/users/user', { user: job.pickup_contact, customer_data_only: true }
    end
  end
else
  if job.customer
    json.recipient do
      json.partial! 'api/v1/users/user', { user: job.customer, customer_data_only: true }
    end
  end
end
json.status job.human_status
json.type job.type
if job.fleet_company
  json.owner_company do
    json.name job.fleet_company.name
    json.logo_url job.fleet_company.logo_url
    json.phone job.fleet_company.phone
  end
else
  json.owner_company nil
end

if job.site && job.site.location
  json.site do
    json.lat job.site.location.lat
    json.lng job.site.location.lng
  end
else
  json.site nil
end

if job.rescue_vehicle && job.rescue_vehicle.location_updated_at && (job.rescue_vehicle.location_updated_at > 5.minute.ago)
  json.rescue_vehicle do
    json.partial! 'api/v1/vehicles/customer_vehicle', vehicle: job.rescue_vehicle
  end
else
  json.rescue_vehicle nil
end

if job.latest_toa
  json.toa job.eta_hash
else
  json.toa nil
end
