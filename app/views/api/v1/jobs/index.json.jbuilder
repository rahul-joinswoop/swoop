# frozen_string_literal: true

json.total @jobs_total
json.jobs @jobs do |job|
  json.partial! 'api/v1/jobs/job', job: job, controller: @controller
end
