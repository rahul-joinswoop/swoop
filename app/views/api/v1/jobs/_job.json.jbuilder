# frozen_string_literal: true

json.id job.id
# HACK: created_at is needed until we swith off android native
json.created_at job.adjusted_created_at
json.type job.type
json.status job.human_status

if job.service_alias_id
  json.service_alias_id job.service_alias_id
end

invoice_partial = @show_full_invoice ? 'api/v1/invoices/invoice' : 'api/v1/invoices/minimal'
json.invoices do
  json.array! job.invoices_by_viewer(@api_company), partial: invoice_partial, as: :invoice
end

if job.storage
  json.storage do
    json.partial! 'api/v1/inventory_items/inventory_item', inventory_item: job.storage
  end
end

json.email job.email

if job.customer
  json.customer do
    json.partial! 'api/v1/users/user', { user: job.customer, customer_data_only: true }
  end
else
  json.customer nil
end

json.customer_type_id job.customer_type_id
json.customer_type_name job.customer_type.try(:name)

if job.caller
  json.caller do
    json.partial! 'api/v1/users/user', user: job.caller
  end
else
  json.caller nil
end

if job.fleet_dispatcher
  json.fleet_dispatcher do
    json.partial! 'api/v1/users/user', user: job.fleet_dispatcher
  end
else
  json.fleet_dispatcher nil
end
if job.root_dispatcher
  json.root_dispatcher do
    json.partial! 'api/v1/users/user', user: job.root_dispatcher
  end
else
  json.root_dispatcher nil
end

if job.accounting_code
  json.accounting_code job.accounting_code
end

if job.issc_dispatch_request.present?
  json.issc_dispatch_request do
    json.max_eta job.issc_dispatch_request.max_eta
    json.preferred_eta job.issc_dispatch_request.preferred_eta
    if job.issc_dispatch_request.present? && job.issc_dispatch_request.issc
      json.client_id job.issc_dispatch_request.issc.clientid
      json.contractor_id job.issc_dispatch_request.issc.contractorid
      json.club_job_type job.issc_dispatch_request.club_job_type
      json.system job.issc_dispatch_request.issc.system
      json.more_time_requested job.issc_dispatch_request.more_time_requested
    end
  end
end
if job.user
  json.dispatcher do
    json.partial! 'api/v1/users/user', user: job.user
  end
else
  json.dispatcher nil
end

json.fleet_dispatcher_id job.fleet_dispatcher_id
json.partner_dispatcher_id job.partner_dispatcher_id
json.root_dispatcher_id job.root_dispatcher_id

if job.pickup_contact
  json.pickup_contact do
    json.partial! 'api/v1/users/user', user: job.pickup_contact
  end
else
  json.pickup_contact nil
end

if job.dropoff_contact
  json.dropoff_contact do
    json.partial! 'api/v1/users/user', user: job.dropoff_contact
  end
else
  json.dropoff_contact nil
end

if job.last_touched_by
  json.last_touched_by do
    json.id job.last_touched_by.id
    json.full_name job.last_touched_by.full_name
    json.email job.last_touched_by.email
    json.phone job.last_touched_by.phone
    json.company_id job.last_touched_by.company_id
  end
else
  json.last_touched_by nil
end

if job.trailer_vehicle_id
  json.trailer_vehicle_id job.trailer_vehicle_id
  json.trailer_name job.trailer_vehicle.name
end

if job.rescue_vehicle
  json.rescue_vehicle_id job.rescue_vehicle_id
  json.rescue_vehicle do
    json.partial! 'api/v1/vehicles/job_rescue_vehicle', vehicle: job.rescue_vehicle
  end
else
  json.rescue_vehicle nil
  json.rescue_vehicle_id nil
end

if job.rescue_driver
  json.rescue_driver do
    json.partial! 'api/v1/users/user', user: job.rescue_driver
  end
  json.rescue_driver_id job.rescue_driver_id
else
  json.rescue_driver nil
  json.rescue_driver_id nil
end

json.service job.service_code.try(:name)
json.service_code_id job.service_code_id

if job.stranded_vehicle
  json.make job.stranded_vehicle.make
  json.model job.stranded_vehicle.model
  json.vehicle_type job.stranded_vehicle.vehicle_type
  json.serial_number job.stranded_vehicle.serial_number
  json.year job.stranded_vehicle.year
  json.color job.stranded_vehicle.color
  json.vin job.stranded_vehicle.vin
  json.tire_size job.stranded_vehicle.tire_size
  json.license job.stranded_vehicle.license
end

if job.parent_id && job.parent
  json.parent_id job.parent_id
end

if job.service_location
  json.service_location do
    json.partial! 'api/v1/locations/location', location: job.service_location
  end
end

if job.original_service_location
  json.original_service_location do
    json.partial! 'api/v1/locations/location', location: job.original_service_location
  end
end

if job.drop_location
  json.drop_location do
    json.partial! 'api/v1/locations/location', location: job.drop_location
  end
end

if job.question_results.present?
  json.question_results do
    json.array! job.question_results do |c|
      json.answer_id c.answer_id
      json.question_id c.question_id
      json.question c.question.question
      json.question_name c.question.name
      if c.answer
        json.answer c.answer.answer
        json.answer_info c.answer_info
      end
    end
  end
end

json.notes job.notes
json.partner_notes job.partner_notes
json.dispatch_notes job.dispatch_notes

if job.fleet_job?
  json.fleet_notes job.fleet_notes
end

json.driver_notes job.driver_notes
json.swoop_notes job.swoop_notes

# PDW - can't do this becuase it's a helper method on the controller, which doesnt exist when doing ws publishes
# Need:
#  user based auth on ws
#  policy based object access (https://labs.kollegorna.se/blog/2014/11/rails-api/)
#  conversion of all jbuilder templates to code
# job_notes.each do |note|
#  json.set!(note, job[note])
# end

if job.bta
  json.bta job.bta.time
else
  json.bta nil
end

# json.toa job.toa_hash
json.toa job.eta_hash

if job.account
  json.account do
    json.id job.account.id
    json.name job.account.name
  end
else
  json.account nil
end
json.account_id job.account_id

if job.fleet_company
  json.owner_company_id job.fleet_company_id
  json.owner_company do
    json.id job.fleet_company.id
    json.name job.fleet_company.name

    json.customer_types job.fleet_company.companies_customer_types do |company_customer_type|
      if company_customer_type.deleted_at
        json.deleted_at company_customer_type.deleted_at
      end
      json.id company_customer_type.customer_type.id
      json.name company_customer_type.customer_type.name
      # TODO: Move to use partial here
    end
  end
else
  json.owner_company nil
end

if job.rescue_company
  json.rescue_company do
    json.id job.rescue_company.id
    json.name job.rescue_company.name
  end
else
  json.rescue_company nil
end
if job.deleted_at
  json.deleted_at job.deleted_at
end
json.get_location_attempts job.get_location_attempts
json.uuid job.uuid

if job.review
  json.set! :review, ReviewSerializer.new(job.review).serializable_hash
end

json.history do
  job.job_history_hash.each do |k, v|
    json.set! k do
      json.partial! 'api/v1/status_history/status_history', audit_job_status: v
    end
  end
end

if job.estimate
  json.distance job.estimate.miles_bc
else
  json.distance nil
end

json.scheduled_for job.scheduled_for
json.po_number job.po_number
json.unit_number job.unit_number
json.ref_number job.ref_number

if job.odometer
  json.odometer job.odometer.round
else
  json.odometer nil
end
json.vip job.vip
json.uber job.uber
json.alternate_transportation job.alternate_transportation

json.send_sms_to_pickup job.send_sms_to_pickup

json.location_type job.location_type
json.dropoff_location_type job.dropoff_location_type

json.no_keys job.no_keys
json.keys_in_trunk job.keys_in_trunk
json.four_wheel_drive job.four_wheel_drive
json.blocking_traffic job.blocking_traffic
json.accident job.accident
json.unattended job.unattended

json.reject_reason_id job.reject_reason_id
json.reject_reason_info job.reject_reason_info

if job.invoice && job.estimate
  if job.invoice.rate_type == "HoursP2PRate"
    if job.drop_location
      json.mins_ab job.mins_ab
      json.mins_on_site job.mins_on_site
      json.mins_bc job.mins_bc
      json.mins_at_dropoff job.mins_at_dropoff
      json.mins_ca job.mins_ca
    else
      json.mins_ab job.mins_ab
      json.mins_on_site job.mins_on_site
      json.mins_ba job.mins_ba
    end
  else
    if job.drop_location
      json.miles_ab job.estimate.miles_ab
      json.miles_bc job.estimate.miles_bc
      json.miles_ca job.estimate.miles_ca
    else
      json.miles_ab job.estimate.miles_ab
      json.miles_ba job.estimate.miles_ba
    end
  end
end

json.images do
  json.array! job.images, partial: 'api/v1/images/image', as: :image
end

if job.original_job_id
  json.original_job_id job.original_job_id
end
if job.child_jobs.present?
  json.child_job job.child_jobs[0].id
end

json.fleet_manual job.fleet_manual

json.partner_live job.partner_live
json.partner_open job.partner_open

json.eta_mins job.eta_mins
json.ata_mins job.ata_mins

json.total_miles job.miles_p2p
json.total_minutes job.mins_p2p
json.invoice_vehicle_category_id job.invoice_vehicle_category_id
json.symptom_id job.symptom_id
# TODO: only expose this to fleet and rename to fleet
if job.department_id
  json.department_id job.department_id
end
# TODO: Only expose this if partner
if job.partner_department_id
  json.partner_department_id job.partner_department_id
end
if job.symptom
  json.symptom job.symptom.name
end
json.tire_type_id job.tire_type_id
json.get_location_sms job.get_location_sms
json.text_eta_updates job.text_eta_updates
json.review_sms job.review_sms
json.partner_sms_track_driver job.partner_sms_track_driver
json.track_technician_sent_at job.track_technician_sent_at

json.updated_at job.updated_at

json.will_store job.will_store

json.site_id job.site_id
if job.fleet_site_id
  json.fleet_site_id job.fleet_site_id
end
json.storage_type_id job.storage_type_id

# TODO: Remove once mobile no longer depends on it
json.answerBy job.answer_by
###

json.answer_by job.answer_by

if job.priority_response
  json.priority_response job.priority_response
end

# depricated
json.first_name job.customer&.first_name
json.last_name job.customer&.last_name
json.phone job.customer&.phone

json.last_status_changed_at job.last_status_changed_at

json.extract! job, :policy_number if job.policy_number?

if job.policy_location_id.present?
  json.policy_location do
    json.partial! 'api/v1/locations/location', location: job.policy_location
  end
end

if job.review_delay
  json.review_delay job.review_delay
end

if job.attached_documents.present?
  json.attached_documents do
    json.array! job.attached_documents, partial: 'api/v1/attached_documents/attached_document', as: :attached_document
  end
end

if job.explanations.present?
  json.explanations do
    json.array! job.explanations, partial: 'api/v1/explanations/explanation', as: :explanation
  end
end

json.customized_rescue_company_name job.customized_rescue_company_name

json.customer_lookup_type job.customer_lookup_type

if job.view_alert
  json.customer_says_not_on_site job.view_alert.customer_says_not_on_site
  json.customer_says_not_complete job.view_alert.customer_says_not_complete
else
  json.customer_says_not_on_site nil
  json.customer_says_not_complete nil
end

if @api_company&.id == Company.swoop_id
  json.experiments ActiveModelSerializers::SerializableResource.new(
    TargetVariant.where(target: job)
  ).serializable_hash
end

if job.candidate_assignment
  json.set! :assignment, Job::Candidate::AssignmentSerializer.new(job.candidate_assignment).serializable_hash
end

if @api_company
  if (@api_company == job.rescue_company) || job.bids.any? { |bid| bid.company_id == @api_company&.id }
    json.candidates ActiveModelSerializers::SerializableResource.new(
      job.candidates.select { |candidate| candidate.company_id == @api_company.id },
      namespace: Job
    ).serializable_hash

  elsif (@api_company&.id == Company.swoop_id) || (@api_company == job.fleet_company)
    json.candidates ActiveModelSerializers::SerializableResource.new(
      job.candidates,
      namespace: Job
    ).serializable_hash

  end

  if job.auction
    json.auction ActiveModelSerializers::SerializableResource.new(
      job.auction, scope: @api_company
    ).serializable_hash
  end

end

if job.pcc_coverage_id.present?
  json.pcc_coverage_id job.pcc_coverage_id
  json.covered job.pcc_coverage.covered
  json.exceeded_service_count_limit job.pcc_coverage.exceeded_service_count_limit
  if (@api_company&.id == Company.swoop_id) || (@api_company == job.fleet_company)
    json.pcc_coverage ActiveModelSerializers::SerializableResource.new(
      job.pcc_coverage, scope: @api_company
    ).serializable_hash

    if job.pcc_coverage.pcc_policy_id
      json.pcc_policy ActiveModelSerializers::SerializableResource.new(
        job.pcc_coverage.pcc_policy
      ).serializable_hash
    end
  end
end

if job.pcc_claim.present?
  json.pcc_claim_number  job.pcc_claim.claim_number
  json.pcc_policy_number job.pcc_claim.pcc_policy.policy_number
end

json.invoice_restrictions job.invoice_restrictions?
