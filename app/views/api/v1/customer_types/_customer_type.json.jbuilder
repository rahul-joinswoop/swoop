# frozen_string_literal: true

json.id customer_type.id
json.name customer_type.name
