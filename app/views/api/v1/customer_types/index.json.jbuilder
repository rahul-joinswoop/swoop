# frozen_string_literal: true

json.array! @customer_types, partial: 'api/v1/customer_types/customer_type', as: :customer_type
