# frozen_string_literal: true

json.id symptom.id
json.name symptom.name
json.deleted_at symptom.deleted_at
json.is_standard symptom.is_standard
