# frozen_string_literal: true

json.partial! 'api/v1/location_types/location_type', location_type: @location_type
