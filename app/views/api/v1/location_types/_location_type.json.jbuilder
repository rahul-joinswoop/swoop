# frozen_string_literal: true

json.id location_type.id
json.name location_type.name
json.deleted_at location_type.deleted_at
