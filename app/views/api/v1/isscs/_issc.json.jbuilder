# frozen_string_literal: true

json.id issc.id
json.site_id issc.site_id
if issc.site
  json.site do
    json.partial! 'api/v1/sites/site', site: issc.site
  end
end

json.company_id issc.company_id
if issc.company
  json.company do
    json.partial! 'api/v1/companies/company', { company: issc.company, accounts: false }
  end
end

if issc.issc_location
  json.locationid issc.issc_location.locationid
end

json.contractorid issc.contractorid
json.clientid issc.clientid
json.token issc.token
json.status issc.status

json.created_at issc.created_at
json.updated_at issc.updated_at
