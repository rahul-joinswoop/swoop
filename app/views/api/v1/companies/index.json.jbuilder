# frozen_string_literal: true

json.array! @companies, partial: 'api/v1/companies/company', locals: { accounts: accounts }, as: :company
