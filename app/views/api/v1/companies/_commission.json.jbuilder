# frozen_string_literal: true

json.id commission.id
json.default_pct commission.default_pct
