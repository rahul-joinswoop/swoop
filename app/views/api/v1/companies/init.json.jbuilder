# frozen_string_literal: true

json.services do
  json.array! @services, partial: 'api/v1/services/service', as: :service
end

json.customer_types do
  json.array! @customer_types, partial: 'api/v1/customer_types/customer_type', as: :customer_type
end

json.service_aliases do
  json.array! @service_aliases, partial: 'api/v1/service_aliases/service_alias', as: :service_alias
end

json.sites do
  json.array! @sites, partial: 'api/v1/sites/site', as: :site
end

json.vehicle_categories ActiveModelSerializers::SerializableResource.new(
  @company.vehicle_categories
)

# render json: departments

# render json: users, each_serializer: MinimalUserSerializer
json.users ActiveModelSerializers::SerializableResource.new(
  @users,
  each_serializer: MinimalUserSerializer
).serializable_hash

json.company do
  json.partial! 'api/v1/companies/company', { company: @company, accounts: false }
end

json.settings do
  json.array! @settings, partial: 'api/v1/settings/setting', as: :setting
end

json.symptoms do
  json.array! @symptoms, partial: 'api/v1/symptoms/symptom', as: :symptom
end

json.departments ActiveModelSerializers::SerializableResource.new(
  @departments
)
