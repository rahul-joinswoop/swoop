# frozen_string_literal: true

json.id company.id
json.name company.name
json.type company.type
json.dispatch_email company.dispatch_email
json.phone company.phone
json.accounting_email company.accounting_email
json.fax company.fax

if company.location
  json.location do
    json.partial! 'api/v1/locations/location', location: company.location
  end
else
  json.location nil
end

json.sites do
  json.array! company.partner_sites, partial: 'api/v1/sites/partner_site', locals: { show_company: false, fleet_company: fleet_company }, as: :site
end
