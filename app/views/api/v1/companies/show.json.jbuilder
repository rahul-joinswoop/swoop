# frozen_string_literal: true

json.partial! 'api/v1/companies/company', { company: @company, accounts: accounts }
