# frozen_string_literal: true

json.id company.id
json.agero_vendor_id company.agero_vendor_id
json.name company.name
json.currency company.currency
json.live company.live
if company.location
  json.location do
    json.partial! 'api/v1/locations/location', location: company.location
  end
end
json.logo_url company.logo_url
json.phone company.phone
json.accounting_email company.accounting_email
json.fax company.fax
json.in_house company.in_house
if company.primary_contact
  json.primary_contact do
    json.full_name company.primary_contact.full_name
    json.email company.primary_contact.email
    json.phone company.primary_contact.phone
  end
else
  json.primary_contact nil
end

if company.rescue?
  json.dispatch_email company.dispatch_email
end

if company.fleet?
  json.customer_types company.companies_customer_types do |company_customer_type|
    if company_customer_type.deleted_at
      json.deleted_at company_customer_type.deleted_at
    end
    json.id company_customer_type.customer_type.id
    json.name company_customer_type.customer_type.name
    # TODO: Move to use partial here
  end
end

json.service_ids do
  json.array! company.service_ids
end

json.addon_ids do
  json.array! company.addon_ids
end
