# frozen_string_literal: true

sites = true unless defined?(sites) && sites == false
questions = true unless defined?(questions) && questions == false
territories = true unless defined?(territories) && territories == false

json.id company.id
json.name company.name
json.type company.type
if company.location
  json.location do
    json.partial! 'api/v1/locations/location', location: company.location
  end
else
  json.location nil
end
json.logo_url company.logo_url
json.phone company.phone
json.accounting_email company.accounting_email
json.fax company.fax
json.tax_id company.tax_id
json.agero_vendor_id company.agero_vendor_id
if accounts
  json.accounts company.accounts do |account|
    json.id account.id
    json.name account.name
    if account.deleted_at
      json.deleted_at account.deleted_at
    end
  end
end

json.features company.features.collect { |c| c.id }
json.customer_type_ids company.customer_types.collect { |c| c.id }
json.symptoms company.symptoms.collect { |c| c.id }
json.departments company.departments.collect { |c| c.id }
# json.features do
#  json.array! company.features, partial: 'api/v1/features/feature', as: :feature
# end

json.vehicle_categories company.vehicle_categories.collect { |t| t.id }

json.channel_name company.channel_name
json.in_house company.in_house
json.parent_company_id company.parent_company_id

json.extract! company, :auto_assign_distance

json.subscription_status_id company.subscription_status_id

if company.primary_contact
  json.primary_contact do
    json.full_name company.primary_contact.full_name
    json.email company.primary_contact.email
    json.phone company.primary_contact.phone
  end
else
  json.primary_contact nil
end

if company.rescue?
  json.dispatch_email company.dispatch_email
end

if company.rescue?
  if company.network_manager
    json.network_manager do
      json.id company.network_manager.id
      json.name company.network_manager.name
    end
    json.network_manager_id company.network_manager_id
  else
    json.network_manager nil
  end
end

if company.rescue? && company.commission.present?
  json.commission do
    json.partial! 'api/v1/companies/commission', commission: company.commission
  end
end

if company.rescue?
  json.commission_exclusions_services_ids do
    json.array! company.commission_exclusions_services_ids
  end

  json.commission_exclusions_addons_ids do
    json.array! company.commission_exclusions_addons_ids
  end

  if company.invoice_charge_fee.present?
    json.invoice_charge_fee ActiveModelSerializers::SerializableResource.new(
      company.invoice_charge_fee
    ).serializable_hash
  end

  json.billing_type company.billing_type

  if territories
    json.territories company.territories, partial: 'api/v1/partner/territories/territory', as: :territory
  end
end

if company.fleet? || company.super?
  json.support_email company.support_email
end

json.customer_types company.companies_customer_types do |company_customer_type|
  if company_customer_type.deleted_at
    json.deleted_at company_customer_type.deleted_at
  end
  json.id company_customer_type.customer_type.id
  json.name company_customer_type.customer_type.name
  # TODO: Move to use partial here
end

# This is deprecated and should be removed for next forced mobile release
json.service_codes do
  json.array! company.service_codes, partial: 'api/v1/services/service', as: :service
end

json.service_ids do
  json.array! company.service_ids
end

json.addon_ids do
  json.array! company.addon_ids
end

if sites
  json.sites do
    if !company.has_feature(Feature::JOB_FORM_RECOMMENDED_DROPOFF)
      json.array! company.live_sites, partial: 'api/v1/sites/site', as: :site
    else
      json.array! []
    end
  end
end

if questions && company.fleet?
  json.questions do
    json.array! company.company_service_questions, partial: 'api/v1/company_service_questions/question', as: :question
  end
end

json.location_types do
  json.array! company.location_types, partial: 'api/v1/location_types/location_type', as: :location_type
end

# TODO: we only need to send the waiver properties if the receiving user is in the company
if !defined?(hide_waivers) || hide_waivers != true || company.super?
  json.pickup_waiver company.inherited_pickup_waiver
  json.dropoff_waiver company.inherited_dropoff_waiver
  json.invoice_waiver company.inherited_invoice_waiver
end

json.live company.live

json.invoice_minimum_mins company.invoice_minimum_mins
json.invoice_increment_mins company.invoice_increment_mins
json.invoice_roundup_mins company.invoice_roundup_mins
json.invoice_mileage_rounding company.invoice_mileage_rounding
json.invoice_mileage_minimum company.invoice_mileage_minimum

json.issc_status company.issc_status

json.fleet_in_house_client_ids company.fleet_in_house_client_ids
json.fleet_managed_client_ids company.fleet_managed_client_ids

json.deleted_at company.deleted_at

if company.fleet?
  json.auction_max_bidders company.auction_max_bidders
  json.auction_max_distance company.auction_max_distance
  json.auction_max_eta company.auction_max_eta
  if company.fleet_managed? && company.ranker
    json.autoassignment_ranker do
      json.id company.ranker.id
      json.type company.ranker.type
      json.weights do
        json.speed company.ranker.weights['speed'].to_f
        json.quality company.ranker.weights['quality'].to_f
        json.cost company.ranker.weights['cost'].to_f
      end
      json.constraints do
        json.min_eta company.ranker.constraints['min_eta'].to_i
        json.max_eta company.ranker.constraints['max_eta'].to_i
        json.min_cost company.ranker.constraints['min_cost'].to_i
        json.max_cost company.ranker.constraints['max_cost'].to_i
        json.min_rating company.ranker.constraints['min_rating'].to_i
        json.max_rating company.ranker.constraints['max_rating'].to_i
      end
    end
  end
end

json.review_email company.review_email
json.review_email_recipient_key company.review_email_recipient_key
json.disable_review_email company.disable_review_emails?
json.distance_unit company.distance_unit
json.currency company.currency
json.language company.language

json.referer_id company.referer_id
json.dispatch_to_all_partners company.dispatch_to_all_partners
json.issc_client_id company.issc_client_id

json.role_permissions company.role_permissions_by_type_hash

json.available_user_notification_settings company.available_user_notification_settings
