# frozen_string_literal: true

json.id reject_reason.id
json.text reject_reason.text
