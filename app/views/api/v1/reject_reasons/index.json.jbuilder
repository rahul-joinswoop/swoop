# frozen_string_literal: true

json.array! @reject_reasons, partial: 'api/v1/reject_reasons/reject_reason', as: :reject_reason
