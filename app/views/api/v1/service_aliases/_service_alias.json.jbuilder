# frozen_string_literal: true

json.id service_alias.id
json.company_id service_alias.company_id
json.service_code_id service_alias.service_code_id
json.alias service_alias.alias
