# frozen_string_literal: true

json.array! @service_aliases, partial: 'api/v1/service_aliases/service_alias', as: :service_alias
