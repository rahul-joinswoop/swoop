# frozen_string_literal: true

json.id audit_job_status.id

json.last_set audit_job_status.last_set_dttm # Deprecated

json.last_set_dttm audit_job_status.last_set_dttm
json.adjusted_dttm audit_job_status.adjusted_dttm

json.type audit_job_status.ui_type

if audit_job_status.respond_to?(:name)
  json.name audit_job_status.name
else
  json.name JobStatus::ID_MAP[audit_job_status.job_status_id]
end

json.sort_date audit_job_status.adjusted_dttm || audit_job_status.last_set_dttm

# this check is needed, otherwise this explodes with
# ServerError: "undefined method `ui_label' for #<OpenStruct:0x00007fe5093b5380>
if audit_job_status.is_a?(AuditJobStatus) || audit_job_status.is_a?(HistoryItem) || audit_job_status.is_a?(TimeOfArrival) || audit_job_status.is_a?(AuditSms) || audit_job_status.is_a?(ParentJobHistoryItem)
  json.ui_label audit_job_status.ui_label(viewing_company: @api_company)
end
