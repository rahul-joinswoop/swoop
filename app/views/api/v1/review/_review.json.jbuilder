# frozen_string_literal: true

json.id review.id
json.created_at review.created_at
if review.rating
  json.rating review.rating
end
if review.nps_question1
  json.nps_question1 review.nps_question1
end
if review.nps_question2
  json.nps_question2 review.nps_question2
end
if review.nps_question3
  json.nps_question3 review.nps_question3
end

if review.nps_feedback
  json.nps_feedback review.nps_feedback
end
json.job_id review.job_id
if review.replies
  json.replies review.replies do |reply|
    json.id reply.id
    json.body reply.body
  end
end

if review.survey
  json.survey do
    json.id review.survey.id
    json.name review.survey.name
    json.responses review.most_recent_results do |result|
      json.partial! 'api/v1/survey/result', result: result
    end
  end
end
