# frozen_string_literal: true

json.partial! 'api/v1/reasons/reason_attributes', reason: reason
json.issue do
  json.partial! 'api/v1/issues/issue_attributes', issue: reason.issue
end
