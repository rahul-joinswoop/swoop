# frozen_string_literal: true

json.id vehicle_category.id
json.name vehicle_category.name
json.order_num vehicle_category.order_num
