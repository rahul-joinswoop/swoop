# frozen_string_literal: true

json.total @total
json.companies @companies do |company|
  json.partial! 'api/v1/companies/company', company: company, accounts: nil
end
