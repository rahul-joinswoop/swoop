# frozen_string_literal: true

class TerritoriesPolicy

  attr_accessor :context

  def initialize(context)
    self.context = context
  end

  def can_create?
    context.user.has_role?(:root) || context.user.company == context.company
  end

  def can_destroy_all?
    context.user.has_role?(:root) || context.user.company == context.company
  end

  def can_index?
    context.user.has_role?(:root) || context.user.company == context.company
  end

  def can_destroy?
    context.user.has_role?(:root) || context.user.company == context.territory.company
  end

  def can_show?
    context.user.has_role?(:root) || context.user.company == context.territory.company
  end

end
