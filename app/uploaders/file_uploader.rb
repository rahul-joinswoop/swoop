# frozen_string_literal: true

class FileUploader < Shrine

  GB = 1024 * 1024 * 1024

  plugin :activerecord, callbacks: false
  plugin :determine_mime_type
  plugin :logging, logger: Rails.logger
  plugin :validation_helpers
  plugin :direct_upload # Uses Roda

  Attacher.validate { validate_max_size GB }

end
