# frozen_string_literal: true

class SurveySerializer < ActiveModel::Serializer

  attributes(
    :id,
    :name
  )
  has_many :reponses, serializer: SurveyResultSerializer do
    object.survey_results
  end

end
