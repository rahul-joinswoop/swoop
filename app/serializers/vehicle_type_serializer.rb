# frozen_string_literal: true

class VehicleTypeSerializer < ActiveModel::Serializer

  attribute :name

end
