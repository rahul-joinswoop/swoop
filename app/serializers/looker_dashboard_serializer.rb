# frozen_string_literal: true

class LookerDashboardSerializer < ActiveModel::Serializer

  attributes :id, :name

end
