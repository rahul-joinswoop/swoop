# frozen_string_literal: true

class RefererSerializer < ActiveModel::Serializer

  attributes :id, :name

end
