# frozen_string_literal: true

class ServiceCodeSerializer < ActiveModel::Serializer

  attributes :id, :name, :addon, :variable, :is_standard, :support_storage

end
