# frozen_string_literal: true

class ReportResultSerializer < ActiveModel::Serializer

  attributes :id, :report_id, :state, :s3_filename, :empty

end
