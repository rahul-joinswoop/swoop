# frozen_string_literal: true

class CustomerTypeSerializer < ActiveModel::Serializer

  attributes :id, :name

end
