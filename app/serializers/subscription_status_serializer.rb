# frozen_string_literal: true

class SubscriptionStatusSerializer < ActiveModel::Serializer

  attributes(
    :id,
    :name
  )

end
