# frozen_string_literal: true

class PrimaryContactSerializer < ActiveModel::Serializer

  attributes :full_name, :email, :phone

end
