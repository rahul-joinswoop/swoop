# frozen_string_literal: true

class ReasonSerializer < ActiveModel::Serializer

  attributes :id, :name

  belongs_to :issue

end
