# frozen_string_literal: true

class RescueCompanySerializer < ActiveModel::Serializer

  attributes :id, :name

  has_one :commission

  has_many :commission_exclusions_services_ids
  has_many :commission_exclusions_addons_ids

end
