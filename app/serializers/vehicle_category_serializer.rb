# frozen_string_literal: true

class VehicleCategorySerializer < ActiveModel::Serializer

  attributes :id, :name, :order_num, :is_standard

end
