# frozen_string_literal: true

class InvoicePdfSerializer < ActiveModel::Serializer

  attributes :id, :invoice_id, :created_at, :updated_at, :public_url

  def public_url
    if object.public_url.present?
      object.url_for(
        :read,
        response_content_disposition: scope.fetch(:response_content_disposition, 'attachment')
      ).to_s
    else
      nil
    end
  end

end
