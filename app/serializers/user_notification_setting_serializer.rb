# frozen_string_literal: true

class UserNotificationSettingSerializer < ActiveModel::Serializer

  attribute :notification_code
  attribute :notification_channels

end
