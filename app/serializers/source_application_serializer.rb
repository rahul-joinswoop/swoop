# frozen_string_literal: true

class SourceApplicationSerializer < ActiveModel::Serializer

  attributes :platform, :source, :is_api

  def is_api
    object.api?
  end

end
