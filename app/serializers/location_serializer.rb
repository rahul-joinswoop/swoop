# frozen_string_literal: true

class LocationSerializer < ActiveModel::Serializer

  attributes(
    :id,
    :address,
    :street_number, # this is just a subset of `street` field
    :street_name, # this is just a subset of `street` field
    :lat,
    :lng,
    :street,
    :city,
    :state,
    :zip,
    :exact,
    :updated_at
  )

  attribute :place_id
  attribute :site_id
  attribute :location_type_id

end
