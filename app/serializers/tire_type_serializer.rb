# frozen_string_literal: true

class TireTypeSerializer < ActiveModel::Serializer

  attributes(:id, :car, :position, :extras, :size, :deleted_at)

end
