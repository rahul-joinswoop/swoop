# frozen_string_literal: true

class CommissionExclusionSerializer < ActiveModel::Serializer

  attributes :id, :company_id, :service_code_id

end
