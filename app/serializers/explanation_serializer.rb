# frozen_string_literal: true

class ExplanationSerializer < ActiveModel::Serializer

  attributes :id, :job_id, :reason_info, :deleted_at

  has_one :reason

end
