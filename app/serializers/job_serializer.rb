# frozen_string_literal: true

class JobSerializer < ActiveModel::Serializer

  attribute :id
  attribute :created_at
  attribute :type
  attribute :scheduled_for
  attribute :rescue_vehicle_id
  attribute :deleted_at
  attribute :service_code_id
  attribute :service
  attribute :status
  attribute :customer_id
  attribute :customer

  attribute :service_location_id
  attribute :service_location

  attribute :drop_location_id
  attribute :drop_location

  attribute :fleet_company_id, { key: :owner_company_id }
  attribute :owner_company

  attribute :rescue_company_id
  attribute :rescue_company

  attribute :toa
  attribute :odometer
  attribute :root_dispatcher_id
  attribute :fleet_dispatcher_id
  attribute :partner_dispatcher_id
  attribute :rescue_driver_id
  attribute :rescue_vehicle_id
  attribute :account_id
  attribute :po_number
  attribute :year
  attribute :make
  attribute :model
  attribute :color
  attribute :license
  attribute :vin
  attribute :vehicle_type
  attribute :last_status_changed_at
  attribute :partner_live_invoices
  attribute :child_job
  has_one :storage
  attribute :storage_type_id
  attribute :service_alias_id
  attribute :customer_type_id
  attribute :department_id
  attribute :phone
  attribute :customized_rescue_company_name
  attribute :ref_number
  attribute :unit_number
  attribute :fleet_manual
  attribute :invoice_vehicle_category_id
  attribute :pcc_coverage_id
  attribute :covered
  attribute :exceeded_service_count_limit
  attribute :pcc_claim_number
  attribute :pcc_policy_number
  attribute :will_store
  attribute :fleet_site_id

  has_one :review

  def storage
    if object.storage
      StorageSerializer.new(object.storage).serializable_hash
    end
  end

  def review
    if object.review
      ReviewSerializer.new(object.review).serializable_hash
    end
  end

  def drop_location
    if object.drop_location
      LocationSerializer.new(object.drop_location).serializable_hash
    end
  end

  def service_location
    if object.service_location
      LocationSerializer.new(object.service_location).serializable_hash
    end
  end

  def status
    object.human_status
  end

  def service
    object.service_code&.name
  end

  def owner_company
    { id: object.fleet_company.id, name: object.fleet_company.name } if object.fleet_company
  end

  def rescue_company
    { id: object.rescue_company.id, name: object.rescue_company.name } if object.rescue_company
  end

  def customer_id
    object.customer&.id
  end

  def customer
    object&.customer&.slice(:id, :full_name, :member_number, :phone) if object.customer
  end

  def toa
    object.eta_hash
  end

  def odometer
    object.odometer&.round
  end

  def phone
    object.customer&.phone
  end

  def year
    object.stranded_vehicle&.year
  end

  def make
    object.stranded_vehicle&.make
  end

  def model
    object.stranded_vehicle&.model
  end

  def color
    object.stranded_vehicle&.color
  end

  def license
    object.stranded_vehicle&.license
  end

  def vin
    object.stranded_vehicle&.vin
  end

  def vehicle_type
    object.stranded_vehicle&.vehicle_type
  end

  def partner_live_invoices
    ret = []
    object.partner_live_invoices.each do |invoice|
      ret << {
        id: invoice.id,
      }
    end
    ret
  end

  def child_job
    object.child_jobs[0]&.id
  end

  def customized_rescue_company_name
    object.customized_rescue_company_name
  end

  def pcc_claim_number
    if object.pcc_claim.present?
      object.pcc_claim.claim_number
    end
  end

  def pcc_policy_number
    if object.pcc_claim.present?
      object.pcc_claim.pcc_policy.policy_number
    end
  end

  def covered
    if object.pcc_coverage.present?
      object.pcc_coverage.covered
    end
  end

  def exceeded_service_count_limit
    if object.pcc_coverage.present?
      object.pcc_coverage.exceeded_service_count_limit
    end
  end

end

# This keeps the association as an association and not an , but then it's inconsistent as you then need to do include:[service_location]
#  If they're s you can just use fields:[:service_location] and also can order them properly
#  has_one :service_location, class_name: 'Location' do|serializer|
#    {id:object.service_location.id,
#     address:object.service_location.address} if object.service_location
#  end
