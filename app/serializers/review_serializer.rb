# frozen_string_literal: true

class ReviewSerializer < ActiveModel::Serializer

  attributes(
    :id,
    :created_at,
    :actual_toa,
    :feedback,
    :rating,
    :job_id,
    :survey_results,
    :estimated_toa,
    :survey,
  )

  attribute :nps_question1, if: -> { object.nps_question1 }
  attribute :nps_question2, if: -> { object.nps_question2 }
  attribute :nps_question3, if: -> { object.nps_question3 }
  attribute :nps_feedback, if: -> { object.nps_feedback }

  has_many :replies, serializer: TwilioReplySerializer, if: -> { object.replies }
  has_one :survey, serializer: SurveySerializer, if: -> { object.survey }
  # has_many :survey_results, serializer: SurveyResultSerializer, if: -> {object.survey_results}

  def survey_results
    object.most_recent_results.map do |result|
      SurveyResultSerializer.new(result, scope: scope, root: false)
    end
  end

  def actual_toa
    result = AtaCalculator.new(object.job_id).call
    return '-' if result.nil?

    result.round
  end

  def estimated_toa
    result = EtaCalculator.new(object.job_id).call
    return '-' if result.nil?

    result.round
  end

  # PDW going forwards object.nps_feedback should be set, but historically it's not
  def feedback
    ret = ''
    if object.nps_feedback.present?
      ret = object.nps_feedback
    elsif object.rating.present? && object.replies.count > 1
      ret = object.replies.second.body
    end
    ret
  end

end
