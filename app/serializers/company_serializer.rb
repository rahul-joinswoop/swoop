# frozen_string_literal: true

class CompanySerializer < ActiveModel::Serializer

  attributes(
    :id,
    :name,
    :type,
    :logo_url,
    :phone,
    :accounting_email,
    :fax,
    :tax_id,
    :channel_name,
    :in_house,
    :parent_company_id,
    :live,
    :invoice_minimum_mins,
    :invoice_increment_mins,
    :invoice_roundup_mins,
    :invoice_mileage_rounding,
    :invoice_mileage_minimum,
    :issc_status,
    :features,
    :vehicle_categories,
    :service_ids,
    :addon_ids,
    :referer_id,
    :dispatch_to_all_partners,
    :issc_client_id,
    :role_permissions
  )

  attribute :dispatch_email, if: -> { object.rescue? }
  attribute :support_email, if: -> { object.fleet? }

  has_one :location
  has_one :primary_contact, serializer: PrimaryContactSerializer
  has_many :customer_types do
    object.companies_customer_types
  end
  has_many :service_codes
  has_many :questions, if: -> { object.fleet? } do
    object.company_service_questions
  end
  has_many :accounts
  has_many :sites, include: ['location', 'providerCompanies'] do
    object.live_sites
  end

  has_many :customer_type_ids do
    object.companies_customer_types.collect { |c| c.id }
  end

  has_one :commission, if: -> { object.rescue? }
  has_one :invoice_charge_fee, if: -> { object.rescue? }

  has_many :commission_exclusions_services_ids, if: -> { object.fleet? }
  has_many :commission_exclusions_addons_ids,   if: -> { object.rescue? }

  # call with 'render json: company,  pretty: true'
  def features
    if @instance_options[:pretty]
      object.features.collect { |c| c.name }
    else
      object.features.collect { |c| c.id }
    end
  end

  def vehicle_categories
    if @instance_options[:pretty]
      object.vehicle_categories.collect { |t| t.name }
    else
      object.vehicle_categories.collect { |t| t.id }
    end
  end

  def role_permissions
    object.role_permissions_by_type_hash
  end

end
