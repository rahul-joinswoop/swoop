# frozen_string_literal: true

module PCC
  class PolicyDriversAndVehiclesSerializer < ActiveModel::Serializer

    attributes :policy_id, :pcc_vehicles, :pcc_drivers

    def policy_id
      object.id
    end

    def pcc_vehicles
      object.vehicles.map do |vehicle|
        {
          id: vehicle.id,
          make: vehicle.make,
          model: vehicle.model,
          year: vehicle.year,
          vin: vehicle.vin,
        }
      end
    end

    def pcc_drivers
      object.drivers.map do |driver|
        {
          id: driver.id,
          name: driver.full_name,
        }
      end
    end

  end
end
