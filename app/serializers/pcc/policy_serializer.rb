# frozen_string_literal: true

module PCC
  class PolicySerializer < ActiveModel::Serializer

    attributes :id, :policy_number, :full_name, :address

    attribute :pcc_vehicles, if: -> { object.vehicles.length > 0 }
    attribute :pcc_drivers, if: -> { object.drivers.length > 0 }

    def pcc_vehicles
      object.vehicles&.map do |vehicle|
        {
          id: vehicle.id,
          make: vehicle.make,
          model: vehicle.model,
          year: vehicle.year,
          vin: vehicle.vin,
        }
      end
    end

    def pcc_drivers
      object.drivers&.map do |driver|
        {
          id: driver.id,
          name: driver.full_name,
        }
      end
    end

  end
end
