# frozen_string_literal: true

module PCC
  class CoverageSerializer < ActiveModel::Serializer

    attributes :id,
               :pcc_policy_id,
               :pcc_vehicle_id,
               :pcc_driver_id,
               :covered,
               :exceeded_service_count_limit,
               :data

  end
end
