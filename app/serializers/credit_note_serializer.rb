# frozen_string_literal: true

class CreditNoteSerializer < ActiveModel::Serializer

  attributes :id, :type, :memo, :payment_method, :mark_paid_at, :amount, :status, :deleted_at

  def type
    object.display_name
  end

  def memo
    object.description
  end

  def amount
    object.total_amount
  end

  attribute :deleted_at, if: -> { object.deleted_at }

end
