# frozen_string_literal: true

class AttachedDocumentSerializer < ActiveModel::Serializer

  attributes(
    :id,
    :original_filename,
    :url,
    :deleted_at,
    :created_at,
    :type,
    :container,
    :invoice_attachment
  )

  attribute :job_id, if: -> { object.job_id }
  attribute :company_id, if: -> { object.company_id }

  belongs_to :location

end
