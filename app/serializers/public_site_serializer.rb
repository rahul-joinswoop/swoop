# frozen_string_literal: true

class PublicSiteSerializer < ActiveModel::Serializer

  attributes(
    :name,
    :phone,
  )

  belongs_to :location
  has_one :company, serializer: PublicCompanySerializer

end
