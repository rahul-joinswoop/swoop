# frozen_string_literal: true

class TwilioReplySerializer < ActiveModel::Serializer

  attributes(
    :id,
    :body
  )

end
