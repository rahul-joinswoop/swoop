# frozen_string_literal: true

class AutocompleteCompanySerializer < ActiveModel::Serializer

  attributes(
    :id,
    :name,
  )

end
