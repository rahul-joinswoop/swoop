# frozen_string_literal: true

class AuctionSerializer < ActiveModel::Serializer

  attributes :id,
             :job_id,
             :status,
             :result,
             :target_eta,
             :min_target_eta,
             :max_target_eta,
             :expires_at,
             :manual_dttm,
             :duration_secs

  has_many :bids do
    if Company.swoop?(scope)
      object.bids
    else
      object.bids.where(company: scope)
    end
  end

  def target_eta
    eta_mins = object.bids.map(&:eta_mins).compact.sort.first

    if eta_mins && (eta_mins <= object.max_target_eta)
      [eta_mins, object.min_target_eta].compact.max
    else
      nil
    end
  end

end
