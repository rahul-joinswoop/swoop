# frozen_string_literal: true

class CustomAccountSerializer < ActiveModel::Serializer

  attributes :id, :complete, :verification_failure, :upstream_account_id, :deleted_at, :linked

  attribute :bank_account, if: :linked_stripe_bank_account?
  attribute :upstream_account_id, if: :complete_custom_account?

  def complete
    object.complete?
  end

  def verification_failure
    object.verification_failure?
  end

  def linked
    linked_stripe_bank_account?
  end

  def bank_account
    {
      bank_name: stripe_bank_account.bank_name,
      last4: stripe_bank_account.last4,
    }
  end

  def stripe_bank_account
    @stripe_bank_account ||= scope[:stripe_bank_account]
  end

  def complete_custom_account?
    object.complete?
  end

  def linked_stripe_bank_account?
    stripe_bank_account.present?
  end

end
