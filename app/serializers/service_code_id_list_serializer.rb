# frozen_string_literal: true

class ServiceCodeIdListSerializer < ActiveModel::Serializer

  attributes :ids, :addon, :company_id

  def ids
    object.service_code_ids
  end

end
