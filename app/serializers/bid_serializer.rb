# frozen_string_literal: true

class BidSerializer < ActiveModel::Serializer

  attributes :id,
             :status,
             :company_id,
             :company_name,
             :auction_id
  attribute :eta_mins, key: :submitted_eta
  attribute :cost
  attribute :rating
  attribute :score, key: :swoop_score

  def company_name
    object.company.try(:name)
  end

end
