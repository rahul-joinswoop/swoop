# frozen_string_literal: true

class AccountSerializer < ActiveModel::Serializer

  attributes :id, :name
  attribute :deleted_at, if: -> { object.deleted_at }

end
