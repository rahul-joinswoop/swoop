# frozen_string_literal: true

class PublicCompanySerializer < ActiveModel::Serializer

  attributes(
    :name,
    :phone,
  )

  has_one :location

end
