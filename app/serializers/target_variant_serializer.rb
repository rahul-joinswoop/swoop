# frozen_string_literal: true

class TargetVariantSerializer < ActiveModel::Serializer

  attributes :experiment_name, :variant_name

  def experiment_name
    object.variant.experiment.try(:name)
  end

  def variant_name
    object.variant.try(:name)
  end

end
