# frozen_string_literal: true

class PoiSiteSerializer < ActiveModel::Serializer

  attributes(
    :id,
    :name,
    :phone
  )

  attribute :deleted_at, if: -> { object.deleted_at }

  belongs_to :location
  belongs_to :manager,
             if: -> { object.manager }, serializer: SimpleUserSerializer

end
