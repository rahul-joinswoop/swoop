# frozen_string_literal: true

module QB
  class TokenSerializer < ActiveModel::Serializer

    attributes :id, :company_id, :token, :created_by_user_id

  end
end
