# frozen_string_literal: true

class SymptomSerializer < ActiveModel::Serializer

  attributes :id, :name, :created_at, :deleted_at, :is_standard

end
