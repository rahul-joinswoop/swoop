# frozen_string_literal: true

module Agero::RSA
  # Serializes an Ageor-readable customer hash from a user object
  class CustomerSerializer < ActiveModel::Serializer

    attributes :name, :callbackNumber

    def name
      {
        firstName: object.first_name,
        lastName: object.last_name,
        middleInitial: "",
      }
    end

    def callbackNumber
      object.phone
    end

  end
end
