# frozen_string_literal: true

module Agero::RSA
  # Serializes an Agero-readable hash from a Swoop Job. Designed to provide
  # Agero with all information necessary to create a job in their system.
  class SubmitRequestSerializer < ActiveModel::Serializer

    attributes :requestor, :disablementLocation, :towDestination, :vehicle,
               :notificationPreferences, :serviceDetails

    def requestor
      Agero::RSA::CustomerSerializer.new(object.customer).serializable_hash
    end

    def disablementLocation
      {
        address: serialize_address(object.service_location),
        geographicalCoordinates: serialize_coordinates(object.service_location),
        customerAtLocation: customer_at_location?,
        locationType: map_location_type(object.service_location),
      }
    end

    def towDestination
      {
        destinationBusinessName: nil, # unknown
        destinationType: map_location_type(object.drop_location),
        address: serialize_address(object.drop_location),
        geographicalCoordinates: serialize_coordinates(object.drop_location),
      }
    end

    def vehicle
      Agero::RSA::VehicleSerializer.new(object).serializable_hash
    end

    def notificationPreferences
      {
        preferredMode: "SMS",
        emailId: nil,
        textMessageNumber: object.customer.phone,
        primaryPhoneNumber: nil,
      }
    end

    def serviceDetails
      {
        disablementReason: object.symptom.name,
        comments: [
          { value: object.notes },
        ],
      }
    end

    private

    def customer_at_location?
      QuestionServices::FindAnswer.call(
        job: object, question: Question::CUSTOMER_WITH_VEHICLE
      ).result&.answer
    end

    def map_location_type(location)
      Agero::RSA::MapLocationType.call(
        location_type: location.location_type.name
      ).result
    end

    def serialize_address(location)
      Agero::RSA::AddressSerializer.new(location).serializable_hash
    end

    def serialize_coordinates(location)
      Agero::RSA::CoordinateSerializer.new(location).serializable_hash
    end

  end
end
