# frozen_string_literal: true

module Agero::RSA
  # Serializes an Agero-readable address hash from a location object
  class AddressSerializer < ActiveModel::Serializer

    attributes :streetAddress1, :streetAddress2, :city, :state, :country,
               :zip

    def streetAddress1
      object.address
    end

    def streetAddress2
      ""
    end

  end
end
