# frozen_string_literal: true

module Agero::RSA
  # Serializes an Agero-readable vehicle hash from a Swoop Job object.
  class VehicleSerializer < ActiveModel::Serializer

    attributes :make, :model, :year, :color, :licensePlate, :mileage, :vin,
               :fuelType

    def initialize(job)
      @odometer = job.odometer

      super(job.driver.vehicle)
    end

    def licensePlate
      object.license
    end

    def mileage
      @odometer
    end

    def fuelType
      nil
    end

  end
end
