# frozen_string_literal: true

module Agero::RSA
  # Serializes an Agero-readable coordinate hash from a location object
  class CoordinateSerializer < ActiveModel::Serializer

    attributes :latitude, :longitude

    def latitude
      object.lat
    end

    def longitude
      object.lng
    end

  end
end
