# frozen_string_literal: true

class CompanyServiceQuestionSerializer < ActiveModel::Serializer

  attributes(
    :service_id,
    :question,
    :question_id,
    :name,
    :order_num
  )

  has_many :answers do
    object.question.answers
  end

  def service_id
    object.service_code_id
  end

  def question
    object.question.question
  end

  def question_id
    object.question.id
  end

  def name
    object.question.name
  end

  def order_num
    object.question.order_num
  end

end
