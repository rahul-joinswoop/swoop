# frozen_string_literal: true

class RefundSerializer < PaymentsOrCreditsSerializer

  attribute :refunded_by_external_provider

  def refunded_by_external_provider
    object.type == Payment::REFUND &&
    object.charge.present?
  end

end
