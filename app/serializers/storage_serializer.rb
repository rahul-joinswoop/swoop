# frozen_string_literal: true

class StorageSerializer < ActiveModel::Serializer

  attributes :id, :stored_at, :site_id, :released_to_account_id, :released_to_email, :replace_invoice_info
  attribute :released_to_user, serializer: ReleasedToUserSerializer, if: -> { object.released_to_user }

end
