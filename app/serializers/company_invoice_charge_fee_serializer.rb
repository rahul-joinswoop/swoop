# frozen_string_literal: true

class CompanyInvoiceChargeFeeSerializer < ActiveModel::Serializer

  attributes :id, :percentage, :fixed_value, :payout_interval

end
