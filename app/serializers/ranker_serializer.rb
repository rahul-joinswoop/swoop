# frozen_string_literal: true

class RankerSerializer < ActiveModel::Serializer

  attributes :id, :type
  attribute(:company_id) { object.client_id }
  attribute(:speed) { object.weights['speed'].to_f }
  attribute(:quality) { object.weights['quality'].to_f }
  attribute(:cost) { object.weights['cost'].to_f }
  attribute(:max_eta) { object.constraints['max_eta'].to_i }
  attribute(:max_cost) { object.constraints['max_cost'].to_i }
  attribute(:min_rating) { object.constraints['min_rating'].to_i }

end
