# frozen_string_literal: true

class RecommendedProviderSerializer < ActiveModel::Serializer

  # TODO: most of these should be moved to a BATCH provider fetch call
  attributes(
    :id,
    :open,
    :provider_id,
    :site_id,
    :tire_program,
    :name,
    :job_distance,
    :company_live,
    :phone,
    :location,
    :tags,
    :dispatch_email,
    :rescue_provider_dispatch_email,
    :billing_type,
  )

  # call with 'render json: company,  pretty: true'
  def job_distance
    if object.job_distance
      object.job_distance.round(1)
    end
  end

  def location
    {
      lat: object.site.location.lat,
      lng: object.site.location.lng,
    }
  end

  def phone
    object.phone || object.site.phone
  end

  def company_live
    object.provider.live
  end

  def open
    object.open_for_business
  end

  def tags
    object.tags.pluck(:id)
  end

  def rescue_provider_dispatch_email
    object.provider.dispatch_email
  end

  def tire_program
    object.site.tire_program
  end

  def billing_type
    object.provider&.billing_type
  end

end
