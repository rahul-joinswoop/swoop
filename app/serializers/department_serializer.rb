# frozen_string_literal: true

class DepartmentSerializer < ActiveModel::Serializer

  attributes :id, :name, :code, :created_at, :deleted_at, :company_id, :sort_order

end
