# frozen_string_literal: true

class VehicleSerializer < ActiveModel::Serializer

  attribute :id
  attribute :description, key: :name
  attribute :company_id
  attribute :lat
  attribute :lng
  attribute :location_updated_at

  def driver_id
    vehicle.driver.try(:id)
  end

end
# json.id vehicle.id
# json.name vehicle.description
# json.company_id vehicle.company_id
# json.lat vehicle.lat
# json.lng vehicle.lng
# json.driver_id vehicle.driver.try(:id)
# json.location_updated_at vehicle.location_updated_at
