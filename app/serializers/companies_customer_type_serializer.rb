# frozen_string_literal: true

class CompaniesCustomerTypeSerializer < ActiveModel::Serializer

  attributes :id, :name
  attribute :deleted_at, if: -> { object.deleted_at }

  def id
    object.customer_type.id
  end

  def name
    object.customer_type.name
  end

end
