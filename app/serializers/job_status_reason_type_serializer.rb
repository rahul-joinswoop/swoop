# frozen_string_literal: true

class JobStatusReasonTypeSerializer < ActiveModel::Serializer

  attributes :id, :key, :text

  attribute :job_status_id
  attribute :job_status_name

  def job_status_id
    object.job_status.id
  end

  def job_status_name
    object.job_status.name
  end

end
