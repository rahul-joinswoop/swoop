# frozen_string_literal: true

class ReleasedToUserSerializer < ActiveModel::Serializer

  attributes :storage_relationship, :full_name, :location, :phone, :license_state, :license_id

end
