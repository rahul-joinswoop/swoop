# frozen_string_literal: true

class SiteSerializer < ActiveModel::Serializer

  attributes(
    :id,
    :type,
    :name,
    :company_id,
    :location_id,
    :tz,
    :phone,
    :within_hours,
    :dispatchable,
    :deleted_at,
    :tire_program,
    :storage_lot,
    :always_open,
    :owner_company_id,
    :force_open,
    :open_time,
    :close_time,
    :open_time_sat,
    :close_time_sat,
    :open_time_sun,
    :close_time_sun,
    :user_site,
  )

  attribute :site_code, if: -> { object.site_code }

  belongs_to :location
  has_many :providerCompanies, serializer: RescueCompanySerializer do
    object.providerCompanyNames
  end

  def open_time
    object.open_time&.to_s(:time)
  end

  def close_time
    object.close_time&.to_s(:time)
  end

  def open_time_sat
    object.open_time_sat&.to_s(:time)
  end

  def close_time_sat
    object.close_time_sat&.to_s(:time)
  end

  def open_time_sun
    object.open_time_sun&.to_s(:time)
  end

  def close_time_sun
    object.close_time_sun&.to_s(:time)
  end

  def user_site
    if scope && scope.respond_to?(:user_site?)
      scope.user_site?(object)
    else
      true
    end
  end

end
