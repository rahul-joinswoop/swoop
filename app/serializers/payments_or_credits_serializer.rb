# frozen_string_literal: true

class PaymentsOrCreditsSerializer < ActiveModel::Serializer

  attributes :id, :type, :memo, :payment_method, :mark_paid_at, :amount, :status, :deleted_at,
             :currency, :qb_imported_at, :qb_pending_import, :updated_at

  attribute :deleted_at, if: -> { object.deleted_at }
  attribute :card_charge_last_4_digits, if: -> { object.charge.present? }
  attribute :card_brand, if: -> { object.charge.present? }

  def type
    object.display_name
  end

  def memo
    object.description
  end

  def amount
    object.total_amount
  end

  def card_charge_last_4_digits
    object.charge.last_4
  end

  def card_brand
    object.charge.card_brand
  end

end
