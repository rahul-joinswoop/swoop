# frozen_string_literal: true

class Job::CandidateSerializer < ActiveModel::Serializer

  attributes :id, :rescue_company_id, :company_name, :type, :vehicle_eligible
  attribute :google_eta, key: :eta
  attribute :cost
  attribute :rating
  attribute :score, key: :swoop_score

  # if object.is_a?(Truck)
  #  attribute :vehicle_eligible
  #  has_one :driver, serializer: SimpleUserSerializer
  #  has_one :vehicle
  # end

  def rescue_company_id
    object.company.try(:id)
  end

  def company_name
    object.company.try(:name)
  end

end
