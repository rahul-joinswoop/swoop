# frozen_string_literal: true

class Job::Candidate::SiteSerializer < Job::CandidateSerializer

  attribute :site

end
