# frozen_string_literal: true

class Job::Candidate::AssignmentSerializer < ActiveModel::Serializer

  attributes :id,
             :status,
             :result,
             :eta,
             :auto_assigned,
             :auto_dispatched,
             :company_name,
             :truck_name,

             def company_name
               object.chosen_company.try(:name)
             end

  def truck_name
    object.vehicle.try(:name)
  end

end
