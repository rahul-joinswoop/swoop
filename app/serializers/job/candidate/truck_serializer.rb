# frozen_string_literal: true

class Job::Candidate::TruckSerializer < Job::CandidateSerializer

  attribute :vehicle_eligible
  has_one :driver, serializer: SimpleUserSerializer
  has_one :vehicle

end
