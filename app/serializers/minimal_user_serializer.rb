# frozen_string_literal: true

class MinimalUserSerializer < ActiveModel::Serializer

  attributes(
    :id,
    :full_name,
    :first_name,
    :phone,
    :deleted_at,
    :on_duty,
    :company_id,
    :roles
  )
  def roles
    object.human_roles
  end

end
