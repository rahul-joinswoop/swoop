# frozen_string_literal: true

class PaymentSerializer < PaymentsOrCreditsSerializer

  attribute :charged_by_external_provider
  attribute :refund_id, if: -> { object.charge.present? }

  def charged_by_external_provider
    object.type == Payment::PAYMENT &&
    object.charge.present?
  end

  def refund_id
    object.charge.refund_id
  end

end
