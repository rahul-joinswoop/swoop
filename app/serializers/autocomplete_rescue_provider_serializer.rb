# frozen_string_literal: true

class AutocompleteRescueProviderSerializer < ActiveModel::Serializer

  attributes(
    :id,
    :rescue_company_id,
    :name
  )

  def rescue_company_id
    object.provider_id
  end

end
