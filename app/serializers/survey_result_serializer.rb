# frozen_string_literal: true

class SurveyResultSerializer < ActiveModel::Serializer

  attributes(
    :int_value,
    :string_value,
    :question,
    :description,
    :type,
    :order,
  )
  def order
    object.survey_question.order
  end

  def question
    object.survey_question.question
  end

  def description
    object.survey_question.description
  end

  def type
    object.survey_question.type
  end

end
