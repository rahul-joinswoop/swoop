# frozen_string_literal: true

# Serialize a User model with only available data, no associations or complex
# behaviour. To be used for serializers like PoiSiteSerializer when we only need
# the full name for example.
class SimpleUserSerializer < ActiveModel::Serializer

  attributes :id, :full_name

end
