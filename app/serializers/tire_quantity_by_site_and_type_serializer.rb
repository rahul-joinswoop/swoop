# frozen_string_literal: true

class TireQuantityBySiteAndTypeSerializer < ActiveModel::Serializer

  attributes(:site_id, :tire_type_id, :tire_quantity)

end
