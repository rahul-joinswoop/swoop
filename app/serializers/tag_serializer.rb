# frozen_string_literal: true

class TagSerializer < ActiveModel::Serializer

  attributes :id, :name, :color, :company_id, :created_at, :updated_at, :deleted_at

end
