# frozen_string_literal: true

class SlackWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'slack', retry: 2

  def perform(msg)
    localtime = Time.now.in_time_zone("America/Los_Angeles")
    hms = localtime.strftime("%H:%M:%S")

    SlackChannel.message("#{hms} #{msg}")
  end

end
