# frozen_string_literal: true

class IsscDispatchResponseReject < IsscDispatchResponse

  include LoggableJob

  USAC = 'USAC'
  NSD = 'NSD'
  QUEST = 'QUEST'
  AGO = 'AGO'
  TSLA = 'TSLA'

  REASON_MAP = {
    USAC => {
      JobRejectReason::OUT_OF_SERVICE_AREA => 'OutOfArea',
      JobRejectReason::EQUIPMENT_NOT_AVAILABLE => 'EquipmentNotAvailable', # Equipment not available
      JobRejectReason::RESTRICTED_ROADWAY => 'RestrictedRoadway', # Restricted Roadway
      JobRejectReason::PAYMENT_ISSUE => 'PaymentIssue', # Refused - Payment Issue
    },
    NSD => {
      JobRejectReason::OUT_OF_SERVICE_AREA => 'OutOfArea',
      JobRejectReason::EQUIPMENT_NOT_AVAILABLE => 'EquipmentNotAvailable', # Equipment not available
      JobRejectReason::RESTRICTED_ROADWAY => 'RestrictedRoadway', # Restricted Roadway
      JobRejectReason::PAYMENT_ISSUE => 'PaymentIssue', # Refused - Payment Issue
    },
    QUEST => {
      JobRejectReason::OUT_OF_SERVICE_AREA => 'OutOfArea',
      JobRejectReason::EQUIPMENT_NOT_AVAILABLE => 'EquipmentNotAvailable', # Equipment not available
      JobRejectReason::WEATHER => 'Weather', # Restricted Roadway
      JobRejectReason::SERVICE_NOT_AVAILABLE => 'ServiceNotAvailable', # Refused - Payment Issue
    },
    TSLA => {
      JobRejectReason::EQUIPMENT_NOT_AVAILABLE => 'EquipmentNotAvailable', # Refused - Payment Issue
      JobRejectReason::OTHER => 'Other',
      JobRejectReason::OUT_OF_SERVICE_AREA => 'OutOfArea',
      JobRejectReason::TRAFFIC => 'Traffic',
      JobRejectReason::WEATHER => 'Weather', # Restricted Roadway
    },
    AGO => {
      JobRejectReason::PAYMENT_ISSUE => 'PaymentIssue', # Refused - Payment Issue
      JobRejectReason::EQUIPMENT_NOT_AVAILABLE => 'EquipmentNotAvailable', # Equipment not available
      JobRejectReason::OUT_OF_SERVICE_AREA => 'OutOfArea',
      JobRejectReason::OTHER => 'Other',
    },
  }.freeze

  def perform(job_id, source = :Manual)
    job = Job.find(job_id)

    clientid = job.issc_dispatch_request.issc.clientid

    raise ArgumentError, log_msg("Unable to find clientid", job: job_id) unless clientid

    motorclub = REASON_MAP[clientid]
    raise ArgumentError, log_msg("No reject reasons for clientid #{clientid}", job: job_id) unless motorclub
    @reason = motorclub[job.reject_reason.try(:text)]
    if !@reason
      @reason = motorclub[JobRejectReason::OTHER] || motorclub.first[1]

      # is this hack still valid?
      log :debug, "ISSC IsscDispatchResponseReject MAJOR REJECT_HACK, setting reject reason to #{@reason} because iphone", job: job.id
    end
    log :debug, "#{clientid} #{@reason}", isscreject: true
    @job_id = job.id

    @issccon.ensure_connected do |client|
      Job.transaction do
        job = Job.find(@job_id)

        raise ArgumentError, "ISSC(#{@job_id}) IsscDispatchResponseReject - invalid state for Issc update (#{job.status}) must be: rejected" unless ["rejected"].include?(job.status)

        payload = {
          DispatchID: job.issc_dispatch_request.dispatchid,
          JobID: job.issc_dispatch_request.jobid,
          ServiceProviderResponse: 'Rejected',
          Source: source,
          ClientID: job.issc_dispatch_request.issc.clientid,
          ContractorID: job.issc_dispatch_request.issc.contractorid,
          ProviderContactName: provider_name(job),
          ProviderPhoneNumber: provider_phone_number(job),
          RejectDescription: @reason,
        }
        add_locationid(job, payload)

        log :debug, "ISSCDispatchResponse #{payload}"
        client.dispatchresponse(job, payload)
      end
    end
  end

end
