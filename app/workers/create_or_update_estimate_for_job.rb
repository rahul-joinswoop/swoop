# frozen_string_literal: true

# CREATE OR UPDATE AN ESTIMATE FOR A JOB
#
# Compute an Estimate for a Job, and generate an invoice if appropriate
#
# Using sidekiq-unique-jobs, we ensure we're only creating or updating 1
# Estimate per Job at any given time
#
# N.B. This worker does not calculate estimates for autoassign candidates
#
class CreateOrUpdateEstimateForJob

  include LoggableJob
  include Sidekiq::Worker

  sidekiq_options queue: 'create_or_update_estimate',
                  retry: false,
                  unique_across_queues: true,
                  lock: :while_executing,
                  unique_args: :unique_args,
                  log_duplicate_payload: true

  def self.unique_args(args)
    [args[0]]
  end

  def perform(job_id)
    # Step 1: Find the job we want to estimate
    job = Job.find_by(id: job_id)
    return unless job

    # Step 2: Find or create an estimate for the job and run it
    job.estimate ||= Estimate.new(job: job, start_at: job.scheduled_for)
    we_are_creating_or_updating_estimate = job.estimate.persisted? ? :updating : :creating
    job.estimate.service_location = job.service_location
    job.estimate.drop_location = job.drop_location
    job.estimate.site_location = job.site.try(:location)
    job.estimate.run
    Job.transaction do
      job.estimate.save!
      job.save!
    end

    # We can't calculate the invoice if job is storage and still a draft.
    # This case is achieved when the job is duplicated from a Storage job.
    #
    # (UI Dashboard / select a Storage job / click job menu / duplicate)
    return if job.draft_and_storage?

    # Step 3: Since we re-ran the estimate, let's generate a fresh invoice
    if (we_are_creating_or_updating_estimate == :creating && job.account) || (we_are_creating_or_updating_estimate == :updating && !job.invoice && job.allowed_to_create_invoice?)
      log :debug, "calling CreateOrUpdateInvoice for job_id #{job_id}", job: job_id

      CreateOrUpdateInvoice.perform_async(job_id)
    elsif we_are_creating_or_updating_estimate == :updating && job.invoice&.new_state?
      log :debug, "calling CreateOrUpdateInvoice for job_id #{job_id}", job: job_id

      CreateOrUpdateInvoice.perform_async(job_id)
    else
      log :debug, "did not create or update an invoice for job_id #{job_id}", job: job_id
    end
  end

end
