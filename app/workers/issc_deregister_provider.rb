# frozen_string_literal: true

require 'issc'

class IsscDeregisterProvider

  include Utils
  include Sidekiq::Worker
  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform(issc_ids)
    issc_ids = Array(issc_ids)

    @issccon.ensure_connected do |client|
      Issc.transaction do
        raise ArgumentError, "Must have at least 1 id" if issc_ids.empty?

        isscs = Issc.find(issc_ids)
        isscs.reject { |issc| issc.status == 'unregistered' }.each do |issc|
          client.deregister({
            ClientID: issc.clientid,
            ContractorID: issc.contractorid,
            Token: issc.token,
            LocationID: issc.locationid,
          }.compact)
        end
      end
    end
  end

end
