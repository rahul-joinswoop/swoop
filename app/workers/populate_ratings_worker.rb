# frozen_string_literal: true

# This is called regularly from clock process
#
class PopulateRatingsWorker

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'low', retry: false

  def perform
    CompanyRatingSetting.all.each do |crs|
      Auction::PopulateRatings.new(crs.company.id, crs.settings).call
    end
  end

end
