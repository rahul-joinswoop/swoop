# frozen_string_literal: true

require 'issc'

class IsscServiceRequesterStatus

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform
    @issccon.ensure_connected do |client|
      response = client.servicerequesters
      if response.code == 200
        requesters = response["ServiceRequesters"]
        requesters.each do |requester|
          company = Company.find_by(issc_client_id: requester["ClientID"])
          if company && (company.issc_status != requester["Status"])
            company.issc_status = requester["Status"]
            company.save!
          end
        end

      else
        logger.warn "ISSC Service Requester status returned: #{response.code}"
      end
    end
  end

end
