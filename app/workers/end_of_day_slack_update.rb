# frozen_string_literal: true

# end_of_day_slack_update
class EndOfDaySlackUpdate

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'low'

  def tz
    ActiveSupport::TimeZone['America/Los_Angeles']
  end

  def perform(time = Time.now)
    start_time  = 1.day.ago
    end_time    = Time.now

    SlackChannel.message("Status Update for #{tz.local_to_utc(start_time)} - #{tz.local_to_utc(end_time)}", tags: ['root', 'summary'])

    records = Job.connection.send(:select, update_sql(start_time, end_time))
    records.each do |record|
      message = "Summary For #{record['company']}: #{record['completed']} completed, "\
                "#{record['goa']} goa, #{record['released']} released, #{record['stored']} stored, "\
                "#{record['active']} active, "\
                "#{record['canceled']} canceled, "\
                "#{record['total']} total"
      SlackChannel.message(message, tags: [record['company'], 'summary'])
    end
  end

  # JM ENG-777 jobs.created_at used here intentionally.
  def update_sql(start_time, end_time)
    <<-SQL
      SELECT
          fleet_company.name as "company",

          SUM(CASE WHEN jobs.status='Completed' THEN 1 ELSE 0 END) as "completed",
          SUM(CASE WHEN jobs.status='GOA' THEN 1 ELSE 0 END) as "goa",
          SUM(CASE WHEN jobs.status='Canceled' THEN 1 ELSE 0 END) as "canceled",
          SUM(CASE WHEN jobs.status='Released' THEN 1 ELSE 0 END) as "released",
          SUM(CASE WHEN jobs.status='Stored' THEN 1 ELSE 0 END) as "stored",
          SUM(CASE WHEN jobs.status NOT IN ('Completed', 'GOA', 'Released', 'Canceled', 'Stored') THEN 1 ELSE 0 END ) as "active",
          COUNT(0) as "total"

         FROM jobs
      LEFT JOIN companies fleet_company
        ON fleet_company.id = jobs.fleet_company_id

      LEFT JOIN audit_job_statuses
        ON audit_job_statuses.job_id = jobs.id
          AND audit_job_statuses.job_status_id IN (SELECT id FROM job_statuses WHERE name IN ('Completed', 'GOA', 'Released', 'Canceled', 'Stored'))

      WHERE
        jobs.type = 'FleetManagedJob'
        AND jobs.status <> 'Reassigned'
        AND jobs.deleted_at IS NULL
        AND ( audit_job_statuses.created_at BETWEEN '#{start_time.utc}' AND '#{end_time.utc}'
            OR jobs.created_at BETWEEN '#{start_time.utc}' AND '#{end_time.utc}')
      GROUP BY fleet_company.name
    SQL
  end

end
