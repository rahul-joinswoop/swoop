# frozen_string_literal: true

class GraphQL::AuctionBidStatusChangedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :graphql

  # this is a list of bid statuses that would make a job appear active, on maps, or done from the bidding companies POV
  # note that there are separate subscriptions on a job's status to really track this stuff for all other status changes -
  # this is solely to handle auction-related changes (where the "state change" actually happens on the Auction::Bid and not
  # the Job)
  STATUS_MAP = {
    active_states: [::Auction::Bid::REQUESTED, ::Auction::Bid::SUBMITTED, ::Auction::Bid::WON],
    map_states: [::Auction::Bid::WON],
    done_states: [],
  }.freeze

  def perform(bid_id, delta = {})
    delta.deep_symbolize_keys!

    # bail unless we have a status change
    return unless delta.key? :status

    # load up our bid
    bid = ::Auction::Bid.find(bid_id)

    # our lists of statuses are the human readable form so we also need to
    # convert our old and new status values the same way
    prev_status = delta.dig(:status, 0)
    cur_status = delta.dig(:status, 1)

    # currently this is a Set with a single entry (bid.company)
    subscribers = bid.subscribers

    # notify our subscribers to bid.job that this bid was updated - we do this here
    # because we're not updating the _job's_ subscribers, but the _bid's_ subscribers
    # (ie, the partner company)
    subscribers.each do |l|
      SwoopSchema.subscriptions.trigger('jobUpdated', { id: bid.job.to_ssid }, bid.job, scope: l)
      SwoopSchema.subscriptions.trigger('jobUpdated', { id: bid.job_ssid }, bid.job, scope: l)
      # notify activeJobsFeed when a bid changes - this removes the need to do any notifications below
      SwoopSchema.subscriptions.trigger('activeJobsFeed', {}, bid.job, scope: l)
    end

    # code adopted from JobStatusChangedWorker
    Job.job_status_groups.without(:draft_states).each do |jsg|
      states = STATUS_MAP[jsg]

      if states.include?(cur_status) && !states.include?(prev_status)
        # edge, entering a new job_status_group
        subscribers.each do |l|
          SwoopSchema.subscriptions.trigger('jobStatusEntered', { states: jsg }, bid.job, scope: l)
        end
      elsif !states.include?(cur_status) && states.include?(prev_status)
        # edge, leaving an existing job_status_group
        subscribers .each do |l|
          SwoopSchema.subscriptions.trigger('jobStatusLeft', { states: jsg }, bid.job, scope: l)
        end
      end
    end
  end

end
