# frozen_string_literal: true

class GraphQL::PartnerVehicleChangedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :graphql

  def perform(id)
    partner_vehicle = ::RescueVehicle.find id

    partner_vehicle.subscribers.each do |subscriber|
      SwoopSchema.subscriptions.trigger(
        'partnerVehicleChanged',
        {},
        partner_vehicle,
        scope: subscriber
      )
    end
  end

end
