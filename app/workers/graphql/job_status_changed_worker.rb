# frozen_string_literal: true

class GraphQL::JobStatusChangedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :graphql

  def perform(job_id, delta = {})
    delta.deep_symbolize_keys!

    # load up our job
    job = Job.find(job_id)

    # our lists of statuses are the human readable form so we also need to
    # convert our old and new status values the same way
    if delta.key? :status
      prev_status = Job.statuses[delta.dig(:status, 0)]
      cur_status = Job.statuses[delta.dig(:status, 1)]
    else
      # if status didn't explicitly change just set
      # prev + cur to the actual job status so the jsg code
      # will work
      prev_status = cur_status = Job.statuses[job.status]
    end
    # we always want to notify when the rescue_driver changes
    rescue_driver_removed = delta.dig(:rescue_driver_id, 0)
    rescue_driver_added = delta.dig(:rescue_driver_id, 1)

    # we only notify when rescue_company_id goes from nil -> a company.
    # afaik we do not reassign jobs to different rescue companies but even it we did we would not want to
    # send a notification to the old company with the new company, hence this logic
    rescue_company_added = !delta.dig(:rescue_company_id, 0) && delta.dig(:rescue_company_id, 1)

    # do this only once and exclude the job customer from our subscribers since they don't have access to any of
    # these subscriptions
    subscribers = job.subscribers.reject { |s| s == job.customer }

    Job.job_status_groups.without(:draft_states).each do |jsg|
      states = Job.send jsg

      # if our jsg is :active_states then we also (potentially) call activeJobsFeed with subscription updates
      if states.include?(cur_status)
        if !states.include?(prev_status)
          # edge, going from one job_status_group to another
          subscribers.each do |l|
            SwoopSchema.subscriptions.trigger('jobStatusEntered', { states: jsg }, job, scope: l)
            if jsg == :active_states
              # notify the activeJobsFeed if a job is going into active
              SwoopSchema.subscriptions.trigger('activeJobsFeed', {}, job, scope: l)
            end
          end
        else
          if rescue_company_added
            # if the rescue_company was added it's functionally an edge. we need to let the new company
            # know the job has entered whatever status they are subscribing to. we don't have to notify
            # the current company if we've already sent one above for the status change since it will
            # be identitical
            job.rescue_company.dispatchers.each do |l|
              SwoopSchema.subscriptions.trigger('jobStatusEntered', { states: jsg }, job, scope: l)
              # notify the activeJobsFeed when a rescue company is added
              if jsg == :active_states
                SwoopSchema.subscriptions.trigger('activeJobsFeed', {}, job, scope: l)
              end
            end
          end

          if rescue_driver_added
            # this is also an edge with similar logic to above. we only need to fire this off if there was no
            # status change above.
            scope = job.rescue_driver
            SwoopSchema.subscriptions.trigger('jobStatusEntered', { states: jsg }, job, scope: scope)
            # notify the activeJobsFeed as when a driver is added
            if jsg == :active_states
              SwoopSchema.subscriptions.trigger('activeJobsFeed', {}, job, scope: scope)
            end
          end
        end

        if rescue_driver_removed
          # regardless of any of the above we have to send notifications out when a driver is removed from
          # a job. fyi - this means they may get an api notification which lists the new driver (although they
          # already have access to this job if they query directly).
          rescue_driver = User.find_by_id_in_cache(rescue_driver_removed)
          scope = rescue_driver
          SwoopSchema.subscriptions.trigger('jobStatusLeft', { states: jsg }, job, scope: scope)
          # notify the activeJobsFeed when a driver is deassigned
          if jsg == :active_states
            SwoopSchema.subscriptions.trigger('activeJobsFeed', {}, job, scope: scope)
          end
        end
      elsif !states.include?(cur_status) && states.include?(prev_status)
        subscribers.each do |l|
          SwoopSchema.subscriptions.trigger('jobStatusLeft', { states: jsg }, job, scope: l)
          if jsg == :active_states
            # # notify the activeJobsFeed if a job is leaving active
            SwoopSchema.subscriptions.trigger('activeJobsFeed', {}, job, scope: l)
          end
        end
      end
    end
  end

end
