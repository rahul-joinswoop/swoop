# frozen_string_literal: true

class GraphQL::JobDocumentChangedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :graphql

  def perform(document_id)
    document = AttachedDocument.find document_id

    document.job.subscribers.each do |subscriber|
      SwoopSchema.subscriptions.trigger(
        'jobDocumentChanged',
        { type: document.type, container: document.container, job_id: document.job.to_ssid },
        document,
        scope: subscriber
      )
    end
  end

end
