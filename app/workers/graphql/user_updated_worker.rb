# frozen_string_literal: true

class GraphQL::UserUpdatedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :graphql

  def perform(*user_ids)
    # load up our users
    User.find(user_ids).each do |user|
      # TODO - in the future we could subscribe by user or job instead. would it
      # make more sense to send to an entire company here and then filter the
      # actual subscription payload based on role? or should we (say) iterate through
      # a company, lookup all the dispatchers and notify each of them here
      # individually? need to test this.

      user.subscribers.each do |l|
        SwoopSchema.subscriptions.trigger('userUpdated', { id: user.to_ssid }, user, scope: l)
      end
    end
  end

end
