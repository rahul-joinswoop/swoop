# frozen_string_literal: true

class GraphQL::JobUpdatedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :graphql

  def perform(opts = {})
    opts.deep_symbolize_keys!
    ids = Array(opts[:ids] || [])
    delta = opts[:delta] || {}
    customer_only = opts[:customer_only]

    Rails.logger.debug("#{self.class.name}: sending updates for #{ids.size} jobs")
    # TODO this guard is here because we know from painful experience that passing 100's of ids
    # to this worker will back up sidekiq real good with jobs taking over 20 minutes to complete.
    if ids.size > 25
      Rollbar.warn("#{self.class.name}: too many ids (#{ids.size}) passed")
    end

    # load up our jobs
    ids.each do |job_id|
      job = Job.find(job_id)
      # TODO - in the future we could subscribe by user or job instead. would it
      # make more sense to send to an entire company here and then filter the
      # actual subscription payload based on role? or should we (say) iterate through
      # a company, lookup all the dispatchers and notify each of them here
      # individually? need to test this.

      # on the map/eta page we want to send jobUpdated subscriptions with the rescue vehicle's location
      # out to the customer but we don't want to send to anyone else else (mobile app users don't need
      # the updates which are frequent). so if customer_only is set we're only notifying the customer
      if customer_only
        subscribers = [job.customer]
      else
        subscribers = job.subscribers

        # TODO - could optimize this slightly and leave out rescue_driver if rescue_driver is also
        # a dispatcher
        prev_rescue_driver_id = delta&.dig(:rescue_driver_id, 0)
        (subscribers << User.find_by_id_in_cache(prev_rescue_driver_id)) if prev_rescue_driver_id

        prev_rescue_company_id = delta&.dig(:rescue_company_id, 0)
        (subscribers << Company.find_by_id_in_cache(prev_rescue_company_id)) if prev_rescue_company_id
      end
      subscribers.each do |l|
        # check our subscriber and figure out which job id to use
        id = case l
             when RescueCompany
               bid = job.bids.find_by(company: l)
               bid ? bid.job_ssid : job.to_ssid
             when User
               bid = job.bids.find_by(company: l.company)
               bid ? bid.job_ssid : job.to_ssid
             else
               job.to_ssid
             end

        SwoopSchema.subscriptions.trigger('jobUpdated', { id: id }, job, scope: l)

        # this is only for the transitional time when we release this so that existing jobs continue
        # to update on the mobile app. after we deploy we can remove this.
        if id != job.to_ssid
          SwoopSchema.subscriptions.trigger('jobUpdated', { id: job.to_ssid }, job, scope: l)
        end

        # also notify the activeJobsFeed for everyone except the customer since they don't have access
        # to that subscription
        SwoopSchema.subscriptions.trigger('activeJobsFeed', {}, job, scope: l) if l != job.customer
      end
    end
  end

end
