# frozen_string_literal: true

require 'issc'

class IsscDispatchStatus

  include LoggableJob
  include Sidekiq::Worker
  sidekiq_options queue: 'issc', retry: 5

  # Maybe make this an enum
  STATUS_MAP = {
    accepted: 'DispatchAccepted',
    reassigned: 'DispatchRejected',
    canceled: 'DispatchCanceled',
  }.freeze

  class << self

    def schedule_for(job, job_status_id)
      time_now = Time.now
      issc_status = FleetMotorClubJob::ISSC_STATUS_MAPPING.dig(job_status_id)

      IsscDispatchStatus.perform_async(job.id, issc_status, :Manual, time_now, time_now)
    end

  end

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform(job_id, status, source, event_at, sent_at, **others)
    @job_id = job_id
    @status = status
    @source = source
    @event_at = DateTime.parse(event_at)
    @sent_at = DateTime.parse(sent_at)
    @others = others

    @issccon.ensure_connected do |client|
      Job.transaction do
        job = Job.find(@job_id)
        payload = status_payload(job, @event_at, @sent_at)

        # PDW: why is this duplicated with IsscDispatchStatusGps ?
        if payload
          if @status == 'GPS'
            payload[:Latitude] = @others[:lat]
            payload[:Longitude] = @others[:lng]
            payload[:GPSLastUpdateTimeUTC] = @others[:gps_updated_at]
          elsif job&.rescue_vehicle&.location_updated_at? && (job.rescue_vehicle.location_updated_at > 1.hour.ago)
            payload[:Latitude] = job.rescue_vehicle[:lat]
            payload[:Longitude] = job.rescue_vehicle[:lng]
            payload[:GPSLastUpdateTimeUTC] = job.rescue_vehicle[:location_updated_at]
          end
          log :debug, "IsscDispatchStatus driver:#{job.rescue_driver_id}", issclog: true
          payload[:Driver] = job.rescue_driver_id.to_s if job.rescue_driver_id
          client.dispatchstatus(job, payload)
        end
      end
    end
  end

  def status_payload(job, event_at, sent_at)
    if job.issc_dispatch_request
      clientid = job.issc_dispatch_request.issc.clientid
      payload = {
        ClientID: clientid,
        DispatchID: job.issc_dispatch_request.dispatchid,
        ContractorID: job.issc_dispatch_request.issc.contractorid,
        EventTimeUTC: event_at.utc, # When did it happen
        SentTimeUTC: sent_at.utc, # when was it sent to us
        StatusType: @status,
        Source: @source,
      }
      if job.issc_dispatch_request.issc.issc_location && job.issc_dispatch_request.issc.issc_location.locationid && Issc.provider_supports_location?(clientid)
        payload[:LocationID] = job.issc_dispatch_request.issc.issc_location.locationid
      end

      payload
    else
      log :warn, "Unable to find issc_dispatch_request for #{job.id}", job: job.id
      nil
    end
  end

end
