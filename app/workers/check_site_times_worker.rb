# frozen_string_literal: true

class CheckSiteTimesWorker

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options query_cache: false, retry: false

  def perform
    Site::OpenChecker.update_open_businesses!
  end

end
