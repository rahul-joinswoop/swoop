# frozen_string_literal: true

class UpdateCustomServiceCodesToPartnersWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'custom_service_code_to_partners', retry: 1, query_cache: false

  UPDATE_SERVICES_TO_PARTNER_BATCH_SIZE = ENV['UPDATE_SERVICES_TO_PARTNER_BATCH_SIZE']&.to_i || 500

  def perform(client_company_id, custom_services_ids_added = [], custom_services_ids_removed = [], addon = false)
    @client_company_id = client_company_id
    @custom_services_ids_added = custom_services_ids_added
    @custom_services_ids_removed = custom_services_ids_removed

    company = Company.find(@client_company_id)

    if company.fleet? || company.super?
      Rails.logger.debug(
        "AddServiceCodesAsStandardToPartnersWorker will add custom services to all <id #{company.id}> #{company.name}'s partners"
      )

      if custom_services_ids_added.present?
        add_service_codes_as_standard_to_partners
      end

      if custom_services_ids_removed.present?
        remove_custom_service_codes_from_partners
      end

      PublishServiceCodeIdsToPartnersWorker.perform_async(@client_company_id, addon)
    else
      Rails.logger.debug(
        "AddServiceCodesAsStandardToPartnersWorker called for a #{company.class.name}: <id #{company.id}> #{company.name}"
      )
    end
  end

  private

  def add_service_codes_as_standard_to_partners
    rescue_company_ids = RescueProvider.where(company_id: @client_company_id)
      .joins('INNER JOIN companies ON companies.id = rescue_providers.provider_id')
      .select('DISTINCT provider_id').map(&:provider_id)

    RescueCompany.where(id: rescue_company_ids)
      .find_in_batches(batch_size: UPDATE_SERVICES_TO_PARTNER_BATCH_SIZE) do |rescue_companies|
      rescue_companies.each do |rescue_company|
        ServiceCodeServices::Change.new(
          api_company: rescue_company,
          standard_services_ids_to_add: @custom_services_ids_added,
          should_publish_company: false
        ).call
      end
    end
  end

  def remove_custom_service_codes_from_partners
    rescue_company_ids = RescueProvider.where(company_id: @client_company_id)
      .joins('INNER JOIN companies ON companies.id = rescue_providers.provider_id')
      .select('DISTINCT provider_id').map(&:provider_id)

    RescueCompany.where(id: rescue_company_ids)
      .find_in_batches(batch_size: UPDATE_SERVICES_TO_PARTNER_BATCH_SIZE) do |rescue_companies|
      rescue_companies.each do |rescue_company|
        # checks if partner has any other rescue_providers using the same service_code, and filter it out
        ids_to_be_removed_from_partner =
          @custom_services_ids_removed.select do |id_removed|
            RescueProvider.where(provider_id: rescue_company.id).joins(company: [:service_codes])
              .where.not(companies: { id: @client_company_id }).where(service_codes: { id: id_removed })
          end

        if ids_to_be_removed_from_partner.present?
          ServiceCodeServices::Change.new(
            api_company: rescue_company,
            services_ids_to_remove: ids_to_be_removed_from_partner,
            should_publish_company: false
          ).call
        end
      end
    end
  end

end
