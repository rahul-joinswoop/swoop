# frozen_string_literal: true

class FakeWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'low', retry: 0

  def perform(company_id, type, count)
    company = Company.find(company_id) if company_id
    Fake::Generator.new(company,
                        type,
                        Integer(count)).call
  end

end
