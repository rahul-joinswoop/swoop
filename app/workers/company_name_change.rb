# frozen_string_literal: true

class CompanyNameChange

  include Sidekiq::Worker
  include AfterCommit::Worker

  sidekiq_options queue: 'low', retry: 5

  def perform(company_id, old_name)
    @company = Company.find(company_id)
    @old_name = old_name

    reindex_users!

    return unless @company.fleet?

    update_and_reindex_accounts!
  end

  private

  def update_and_reindex_accounts!
    Account.where(client_company_id: @company.id).each do |account|
      # Something has overridden the account name
      next if account.name != @old_name

      account.update!(name: @company.name)

      Indexer.new.perform('index', 'Account', account.id)
    end
  end

  def reindex_users!
    User.where(company: @company).pluck(:id).each do |user_id|
      Indexer.new.perform('index', 'User', user_id)
    end
  end

end
