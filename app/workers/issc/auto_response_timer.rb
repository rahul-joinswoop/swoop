# frozen_string_literal: true

module ISSC
  class AutoResponseTimer

    include LoggableJob
    include Sidekiq::Worker

    sidekiq_options queue: 'issc', retry: 0

    attr_reader :job

    def perform(job_id)
      @job = Job.find(job_id)

      log :debug, "Auto response timer expired, checking", job: job_id

      unless job.can_auto_respond?
        log :debug, "Cannot Auto Respond -- Skipping", job: job_id
        return
      end

      DigitalDispatch.strategy_for(
        job_digital_dispatcher: @job.digital_dispatcher,
        action: :auto_accept
      ).perform_async(@job.id)
    end

  end
end
