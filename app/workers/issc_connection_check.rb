# frozen_string_literal: true

require 'issc'

class IsscConnectionCheck

  include LoggableJob
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform
    if ENV['ISSC_AUTOCONNECT'] == 'true'
      begin
        @issccon.ensure_connected do |client|
          if ENV['ISSC_UPDATE_GPS_LOCATIONS'] == 'true'
            IsscDispatchStatusGps.new.perform
          end
        end
      rescue ISSC::NotConnected
        log :debug, "Did not connect", issclog: true
      rescue StandardError => e
        log :error, "Exception connecting #{e}", issclog: true
        log :error, e.backtrace.join("\n")
        Rollbar.error(e)
      end
    end
  end

end
