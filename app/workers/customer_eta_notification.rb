# frozen_string_literal: true

class CustomerEtaNotification

  include Sidekiq::Worker

  sidekiq_options queue: 'high', retry: 5

  def perform(toa_id, revision)
    toa = TimeOfArrival.find(toa_id)
    toa.send_customer_eta_notification(revision)
  end

end
