# frozen_string_literal: true

require 'street_address'

# Use the google places api to resolve a place_id to a full address.
# This background worker checks for a specific location and address details and fill in missing parts of the address.
# In case of missing location specifics, the location gets resolved by generating missing components of address (address,
# street, street_name, street_number, city, state, zip and country), else, it returns a nil.
# The address gets resolved by using place_id, lat/lng's of the location or by looking up the address fields using
# StreetAddress library.
class PlacesResolver

  include Sidekiq::Worker
  sidekiq_options queue: 'places', retry: 2

  class LocationDetailsMissingError < ArgumentError; end

  def perform(location_id)
    Rails.logger.debug "PlacesResolver called with #{location_id}"
    raise LocationDetailsMissingError, "location_id missing" if location_id.blank?
    # Step 1: Retrieves the location details.
    location = Location.find(location_id)
    raise LocationDetailsMissingError, "location missing" if location.blank?

    # Step 2: Checks if any of the address components are missing?
    return unless location.missing_address_component?

    # Step 3: Based on place_id if available, tries to resolve the location updates.
    if location.place_id.present?
      updates = geocoder_updates(location)
      Rails.logger.debug "PlacesResolver::perform updating from geocoder places search with: #{updates}"
      incremental_update_of_location_attributes(location, updates) if updates.size > 0
    end

    # Step 4: Checks if any of the address components are missing?
    return if !location.missing_address_component?

    # Step 5: Based on lat/lng if available, tries to resolve the location updates.
    if location.lat.present? && location.lng.present?
      updates = Geocode::LatLngSearch.new(location.lat, location.lng).call
      Rails.logger.debug "PlacesResolver::perform updating from reverse geocoder search with: #{updates}"
      incremental_update_of_location_attributes(location, updates) if updates.size > 0
    end

    # Step 6: Checks if any of the address components are missing?
    return if !location.missing_address_component?

    # Step 7: Check if address field of a location is nil. If its not empty, resolve the remaining fields using the
    # address. This could be accomplished by `lookup_blanks` function.
    if location.address.present?
      updates = lookup_blanks(location)
      Rails.logger.debug "PlacesResolver::perform updating from StreetAddress with: #{updates}"
      location.update(updates) if updates.size > 0
    else
      Rails.logger.warn "PlacesResolver::perform got `location.address` as nil. No further operation could made."
    end

    if location.zip.blank?
      Rails.logger.warn "PlacesResolver: zip_missing for location: #{location.id}"
    end
  end

  private

  # Use place_id if available to reverse geocode the location attributes using the Geocoder library and returns a hash
  # of all the location attributes.
  def geocoder_updates(location)
    place_id = location.place_id
    results = Geocoder.search(place_id, google_place_id: true)
    return {} if results.size == 0
    Geocode::Extractor.new(results).call
  end

  # Only updates null fields when invoked either by Step 3 or Step 5.
  def incremental_update_of_location_attributes(location, args)
    [:street, :street_number, :street_name, :city, :state, :zip, :country, :lat, :lng, :address].each do |field|
      location[field] = args[field] unless location[field]
    end
    location.save!
  end

  # Checks if a location has any NULL fields and tries to update these fields by splitting the street address.
  def lookup_blanks(location)
    sa = street_address(location)
    return {} if !sa

    populate_components(location, sa)
  end

  # Parses either an address or intersection and returns an instance of StreetAddress::US::Address or nil if the
  # location cannot be parsed.
  def street_address(location)
    addr = location.address.gsub(', USA', '')

    sa = StreetAddress::US.parse(addr)
    Rails.logger.debug "PlacesResolver Looking up '#{addr}', got StreetAddress:#{sa.inspect}"
    sa
  end

  # Generates the updates necessary for missing location attributes from the provided street address.
  def populate_components(location, sa)
    updates = {}

    if location.street.nil? && sa.street
      updates[:street] = location.address.split(',')[0]
    end

    if location.street_number.nil? && sa.number
      updates[:street_number] = sa.number
    end

    if location.street_name.nil? && sa.street
      updates[:street_name] = "#{sa.prefix} #{sa.street} #{sa.street_type}".strip
    end

    if location.state.nil? && sa.state
      updates[:state] = sa.state
    end

    if location.city.nil? && sa.city
      updates[:city] = sa.city
    end

    if location.zip.nil? && sa.postal_code
      updates[:zip] = sa.postal_code
    end

    if location.country.nil? && location.address.end_with?(', USA')
      location.country = 'US'
    end

    updates[:autosplit] = true
    updates
  end

end
