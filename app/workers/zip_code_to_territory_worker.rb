# frozen_string_literal: true

# Initializes download of GEOJSON datafiles and feeds to GeojsonDataIngestorWorker asynchronously.
class ZipCodeToTerritoryWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'territoryimport', retry: 0

  def perform
    downloader = S3Downloader.new
    local_geojson_data_file_paths = downloader.download
    local_geojson_data_file_paths.each { |file_object| GeojsonDataIngestorWorker.perform_async(file_object) }
  end

end
