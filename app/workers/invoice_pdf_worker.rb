# frozen_string_literal: true

class InvoicePdfWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'low', retry: 5

  def perform(invoice_id, invoice_pdf_id)
    invoice = Invoice.find(invoice_id)
    invoice_pdf = InvoicePdf.find(invoice_pdf_id)

    generator = InvoicePdfGenerator.new(invoice)
    pdf = generator.generate

    uploader = S3Uploader.new(invoice_pdf.s3_object)
    uploader.upload(pdf)

    invoice_pdf.populate_public_url
    invoice_pdf.save!
  end

end
