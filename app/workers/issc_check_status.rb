# frozen_string_literal: true

class IsscCheckStatus

  include LoggableJob
  include Utils
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: 5

  def enqueue()
  end

  def perform
    # Possible to miss iscss if they are in transitionary periods, this is called a lot though, so that should take care of it

    isscs = Issc.find_by_sql(
      <<~SQL
        select * from isscs
        where
          isscs.status in ('LoggingIn', 'LoggingOut', 'Registering', 'Deregistering') and
          isscs.deleted_at IS NULL and
          isscs.updated_at < NOW() - INTERVAL '2 minutes';
      SQL
    )

    isscs.each do |issc|
      log :debug, "in #{issc.status} for over 2 mins", issc: issc.id
      case issc.status
      when 'logging_in'
        log :debug, "fixing logging_in", issc: issc.id
        IsscLoginProvider.new.perform([issc.id])
      when 'logging_out'
        log :debug, "fixing logging_out", issc: issc.id
        IsscLogoutProvider.new.perform([issc.id])
      else
        log :debug, "leaving", issc: issc.id
      end
    end

    isscs = Issc.find_by_sql(
      <<~SQL
        select * from isscs
        where
          isscs.status = 'LoggingIn' and
          isscs.deleted_at IS NULL and
          isscs.updated_at < NOW() - INTERVAL '5 minutes' and
          isscs.logged_in_at is not null;
      SQL
    )

    if isscs.length > 0
      msg = "There are #{isscs.length} isscs trying to log in for over 5 minutes"
      log :debug, msg
      # Rollbar.error(StandardError.new(msg))
    end
  end

  def success()
  end

end
