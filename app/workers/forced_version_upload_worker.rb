# frozen_string_literal: true

class ForcedVersionUploadWorker

  include LoggableJob
  include Sidekiq::Worker

  sidekiq_options queue: 'high', retry: 5

  def perform(version_id)
    version = Version.find(version_id)

    uploader = S3Uploader.new(version.s3_object)
    uploader.upload(version.new_forced_version_hash.to_json)

    log :info, "New forced version #{version.new_forced_version_hash} published to S3 #{version.s3_object.public_url}"
  end

end
