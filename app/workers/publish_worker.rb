# frozen_string_literal: true

class PublishWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'high', retry: 2

  def perform(target_class, target_id, operation, changes, user_id, request, txn_id)
    clz = target_class.constantize
    obj = if clz <= Company
            Rails.logger.debug "Looking in cache #{ENV['REDIS_CACHE_URL']}"
            Company.find_in_cache(target_id)
          else
            clz.find(target_id)
          end
    obj.base_publish(operation, changes, user_id, request, txn_id)
  end

end
