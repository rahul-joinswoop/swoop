# frozen_string_literal: true

class PublishCompanyWorker

  include Sidekiq::Worker
  include AfterCommit::Worker

  # TODO this should be a unique job with a lock: :until_executing, but need https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379 fixed first
  sidekiq_options queue: 'ws', retry: 1

  # TODO the class_name argument is not used and can be removed.
  def perform(company_id, class_name)
    # We can only publish the Company if it exists in the DB obvisously.
    #
    # CompanyService::Create is a service that runs under transactional context, and it can
    # potentially trigger this async PublishCompanyWorker depending on some specific cases.
    # When it happens, we'll not have a company stored yet on the DB until the transaction finishes
    # (and the company is commited to DB).
    #
    # Since the purpose of this Publisher is to update current logged in users with last
    # company changes, there's no need to publish it while it doesn't exist yet (as there will be no
    # user logged in yet). So we guard it from happen in this case.
    #
    # Otherwise, ActiveRecord::RecordNotFound errors would pop up on Sidekiq logs.
    company = Company.includes(
      :location,
      :features,
      :service_codes,
      :accounts,
      :features,
      :vehicle_categories,
      :company_service_questions,
      live_sites: [:location],
      companies_customer_types: [:customer_type]
    ).find_by(id: company_id)

    if company
      company.base_publish('update')
    end
  end

end
