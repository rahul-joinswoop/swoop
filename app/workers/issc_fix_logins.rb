# frozen_string_literal: true

require 'issc'

class IsscFixLogins

  include Utils
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: false

  def enqueue()
  end

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform
    # Possible to miss iscss if they are in transitionary periods, this is called a lot though, so that should take care of it
    return logout_all if !@issccon.connected?
    isscs = Issc.get_logout_candidates(Issc::ISSC_SYSTEM)
    return if isscs.empty?

    grouped = isscs.group_by { |issc| issc.clientid == 'AGO' ? :ago : :other }
    Rails.logger.debug("IsscFixLogins grouped:#{grouped}")
    logout(grouped[:other]) if grouped[:other].present?
    logout_agero(grouped[:ago]) if grouped[:ago].present?
  end

  def success()
  end

  private

  def logout_all
    # Log out everything that's logged in
    # This is a catch for transition states on logout as well
    # TODO: move this into a better named spot
    Issc.where(system: nil, status: ['LoggedIn', 'LoggingIn', 'LoggingOut']).each do |issc|
      issc.update(status: "Registered")
    end
    Issc.where(system: nil, status: ['Registering']).each do |issc|
      issc.update(status: "Unregistered")
    end
  end

  def logout(isscs)
    Rails.logger.debug "IsscFixLogins Logging out: #{isscs}"
    Issc.transaction do
      isscs.each do |issc|
        Rails.logger.debug("IsscFixLogins::logout - logging out #{issc.id}")
        issc.provider_logout
        issc.save!
      end
      IsscLogoutProvider.perform_async(isscs.map(&:id))
    end
  end

  def logout_agero(isscs)
    Rails.logger.debug "IsscFixLogins Logging out Agero: #{isscs.map(&:id)}"
    agero = Company.find_by(issc_client_id: 'AGO')
    groups = Issc.joins(:site).where(
      clientid: 'AGO',
      status: 'LoggedIn',
      contractorid: isscs.map(&:contractorid).uniq,
      deleted_at: nil,
      delete_pending_at: nil,
      sites: { dispatchable: true },
      system: nil
    ).order("isscs.id").group_by(&:contractorid)

    Rails.logger.debug "IsscFixLogins AGO groups: #{groups}"

    locations = groups.values.select do |_isscs|
      sites = _isscs.map(&:site)
      _isscs.all? { |issc| RescueProvider.find_by(company: agero, provider: issc.company, site: issc.site)&.deleted_at } || sites.any? { |s| !s.within_hours } && sites.none?(&:force_open)
    end.flatten

    Rails.logger.debug "IsscFixLogins AGO locations: #{locations}"

    logout(locations) if locations.present?
  end

end
