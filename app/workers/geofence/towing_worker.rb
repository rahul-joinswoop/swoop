# frozen_string_literal: true

module Geofence
  class TowingWorker < WorkerBase

    # Must be #call to support SidekiqUniqueScheduledWorkerManagement
    def call(job_id)
      @job = find_job(job_id)
      debug "Job found...Starting"

      if !job.onsite?
        debug "Job not in valid status for transition #{job.status}. Exiting.."
        return
      end

      # Currently RequesetContext.current will return the current context (if we are inside a .use
      # block). If not, each call will instantiate a new context which is not persisted anywhere.
      # If this process did not originate from somewhere with a context explicitly set, setting the
      # platform to gps will succeed but will not persist to be written into the job_status_audit.
      #
      # Setting platform to GPS is something of an override of the platform column which indicates
      # the status was changed as part of the geofencing workflow. That said, the location update
      # could have initially come from Web, Android etc so we don't want to destroy that
      # information.
      RequestContext.use(RequestContext.current) do
        set_platform_gps
        advance_to_towing
      end

      if job.towing?
        debug "Job successfully change to status 'towing', updating digital dispatch"
        job.update_digital_dispatcher_status
        debug 'Job#update_digital_dispatcher_status complete'
        Notifiable.trigger_geofence_towing(job)
      else
        debug "Something went wrong. Job failed to update to 'towing'"
      end
    end

    def advance_to_towing
      PartnerUpdateJobState.new(job, JobStatus::NAMES[:TOWING], api_user: job.rescue_driver).call
      debug "#advance_to_towing complete"
    end

  end
end
