# frozen_string_literal: true

module Geofence
  class WorkerBase

    include Sidekiq::Worker
    include SidekiqUniqueScheduledWorkerManagement

    attr_reader :job
    sidekiq_options retry: 0

    def find_job(job_id)
      Job.find(job_id)
    end

    # Set context to platform GPS
    def set_platform_gps
      context = RequestContext.current
      context.source_application_id = SourceApplication.gps_application.id
    end

    def debug(msg)
      Rails.logger.debug("[ #{self.class} ] JobID: #{job&.id} - #{msg}")
    end

  end
end
