# frozen_string_literal: true

module Geofence
  class TowDestinationWorker < WorkerBase

    # Must be #call to support SidekiqUniqueScheduledWorkerManagement
    def call(job_id)
      @job = find_job(job_id)
      debug "Job found...Starting"

      if !job.onsite? && !job.towing?
        debug "Job not in valid status for transition #{job.status}. Exiting.."
        return
      end

      # Currently RequesetContext.current will return the current context (if we are inside a .use
      # block). If not, each call will instantiate a new context which is not persisted anywhere.
      # If this process did not originate from somewhere with a context explicitly set, setting the
      # platform to gps will succeed but will not persist to be written into the job_status_audit.
      #
      # Setting platform to GPS is something of an override of the platform column which indicates
      # the status was changed as part of the geofencing workflow. That said, the location update
      # could have initially come from Web, Android etc so we don't want to destroy that
      # information.
      RequestContext.use(RequestContext.current) do
        set_platform_gps
        advance_to_tow_destination
      end

      if job.towdestination?
        debug "Job successfully changed to status 'towdestination', updating digital dispatch"
        job.update_digital_dispatcher_status
        debug 'Job#update_digital_dispatcher_status complete'
        Notifiable.trigger_geofence_tow_destination(job)
      else
        debug "Something went wrong. Job failed to update to 'towdestination'"
      end
    end

    def advance_to_tow_destination
      status_to_be = JobStatus::NAMES[:TOW_DESTINATION]
      PartnerUpdateJobState.new(job, status_to_be, api_user: job.rescue_driver).call
      debug "#advance_to_tow_destination complete"
    end

  end
end
