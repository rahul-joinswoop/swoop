# frozen_string_literal: true

module Geofence
  class CompletedWorker < WorkerBase

    # Must be #call to support SidekiqUniqueScheduledWorkerManagement
    def call(job_id)
      @job = find_job(job_id)
      debug "Job found...Starting"

      if !(job.onsite? || job.towdestination?)
        debug "Job not in valid status for transition #{job.status}. Exiting.."
        return
      end

      # Currently RequesetContext.current will return the current context (if we are inside a .use
      # block). If not, each call will instantiate a new context which is not persisted anywhere.
      # If this process did not originate from somewhere with a context explicitly set, setting the
      # platform to gps will succeed but will not persist to be written into the job_status_audit.
      #
      # Setting platform to GPS is something of an override of the platform column which indicates
      # the status was changed as part of the geofencing workflow. That said, the location update
      # could have initially come from Web, Android etc so we don't want to destroy that
      # information.
      RequestContext.use(RequestContext.current) do
        set_platform_gps
        advance_to_completed
      end

      if job.completed?
        debug "Job successfully changed to status 'completed', updating digital dispatch"
        job.update_digital_dispatcher_status
        debug 'Job#update_digital_dispatcher_status complete'

        send_notification
      else
        debug "Something went wrong. Job failed to update to 'completed'"
      end
    end

    def advance_to_completed
      PartnerUpdateJobState.new(job, JobStatus::NAMES[:COMPLETED]).call
      debug "#advance_to_completed complete"
    end

    def send_notification
      if job.drop_location
        Notifiable.trigger_geofence_completed_with_dropoff(job)
      else
        Notifiable.trigger_geofence_completed_without_dropoff(job)
      end
    end

  end
end
