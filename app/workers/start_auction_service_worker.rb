# frozen_string_literal: true

class StartAuctionServiceWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'high', retry: 5

  def perform(job_id)
    job = Job.find(job_id)

    auction_service = Auction::StartAuctionService.new(job)
    auction_service.call
  end

end
AutoDispatchWorker = StartAuctionServiceWorker
