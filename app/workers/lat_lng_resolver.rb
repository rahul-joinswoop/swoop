# frozen_string_literal: true

require_dependency 'geocoder'

# Use the google places api to resolve LAT and LNG from an address.
class LatLngResolver

  include Sidekiq::Worker

  sidekiq_options queue: 'places', retry: 2

  def perform(location_id)
    location = Location.find_by!(id: location_id)

    location.lat, location.lng = Geocoder.coordinates(location.address)

    Rails.logger.debug("(LOCATION #{location_id}) LatLngResolver::perform setting lat / lng from GooglePremier API")

    location.save!
  end

end
