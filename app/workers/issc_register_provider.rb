# frozen_string_literal: true

require 'issc'

class IsscRegisterProvider

  include Utils
  include Sidekiq::Worker
  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform(issc_id)
    @issccon.ensure_connected do |client|
      Issc.transaction do
        issc = Issc.find(issc_id)

        raise ArgumentError, "Issc must exist Register, #{issc_id} not found" if issc.nil?
        raise ArgumentError, "Issc must be in Registering state, currently is #{issc.status}" unless issc.status == 'registering'
        raise ArgumentError, "Issc must not be deleted Register, currently is #{issc.deleted_at}" if issc.deleted_at.present?
        raise ArgumentError, "Issc site must have contact name and phone" unless issc.site.name && issc.site.phone

        args = {
          ClientID: issc.clientid,

          ContractorID: issc.contractorid,
          TaxID: issc.site.taxid,
          ContactName: issc.site.name,
          ContactPhone: ISSC.phone10(issc.site.phone),
          ProviderName: issc.company.name,
          ProviderPhone: ISSC.phone10(issc.company.base_phone),
        }
        if Issc.provider_supports_location? issc.clientid
          if issc.issc_location.blank? || issc.issc_location.locationid.blank?
            # TODO - is this still necessary in a post-GCO world?
            # This catches the case where it's actually a GCO register and we want to fail first time.
            args[:LocationID] = '5555'
            args[:ClientID] = 'FAIL'
          else
            args[:LocationID] = issc.issc_location.locationid
          end
        end

        if Issc::PROVIDERS_WITH_USERNAME_SUPPORT.include?(issc.clientid) && !issc.issc_login.nil?
          args[:UserName] = issc.issc_login.username
          args[:Password] = issc.issc_login.password
        end

        if Issc::PROVIDERS_WHICH_REQUIRE_ADDRESS.include?(issc.clientid)
          args[:Address] = issc.site.location.address
          args[:City] = issc.site.location.city
          args[:State] = issc.site.location.state
          args[:Zip] = issc.site.location.zip
        end

        Rails.logger.debug("Calling Register on client with args #{args.inspect}")
        client.register(args.reject { |k, v| v.nil? })

        # No changes are made to the issc
        # issc.save!
      end
    end
  end

end
