# frozen_string_literal: true

class KeenWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'low'

  def perform(batch)
    Keen.publish_batch batch.deep_symbolize_keys!
  end

end
