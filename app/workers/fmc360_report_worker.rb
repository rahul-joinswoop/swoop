# frozen_string_literal: true

# Generate the FMC360 report for Lincoln jobs and upload it to an SFTP server.
class Fmc360ReportWorker

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  # sidekiq_options lock: :until_executed

  REPORT_TIME_ZONE = "America/New_York"
  REPORT_JOB_STATUSES = ["Completed", "GOA", "Released"].freeze
  UNIQUE_TOW_TIME_WINDOW = 72.hours
  REPORT_DIRECTORY = "incoming"
  REPORT_BASENAME = "Agero-LincolnRoadSideAssistance"
  LINCOLN_WARRANTY_PROGRAM_CODE = "lincoln_warranty"
  LINCOLN_ESP_PROGRAM_PREFIX = "lincoln_esp_"
  VENDOR_NAME = "SWOOP"

  def perform(date)
    date = Date.parse(date) if date.is_a?(String)
    sftp_server = SftpServer.new(ENV["LINCOLN_SFTP_SERVER_URI"], password: ENV["LINCOLN_SFTP_PASSWORD"], ssh_key: ENV["LINCOLN_SFTP_SSH_KEY"])
    generate_report(date) do |stream|
      sftp_server.upload(stream, "#{REPORT_DIRECTORY}/#{REPORT_BASENAME}.#{date.iso8601}.txt")
    end
  end

  # Generate the fixed width report and yield it as a stream.
  def generate_report(date)
    file = Tempfile.new("fmc360", encoding: "ISO-8859-1")
    begin
      finder = Job.where(fleet_company_id: Company.lincoln_id)
      finder = finder.where(status: REPORT_JOB_STATUSES)
      finder = finder.joins(:pcc_coverage).where(pcc_coverages: { result: [PCC::Coverage::COVERED_RESULT, PCC::Coverage::PARTIAL_COVERAGE_RESULT] })
      finder = finder.includes(:service_code, :symptom, { pcc_coverage: [:client_program, :pcc_policy] })

      timezone = ActiveSupport::TimeZone[REPORT_TIME_ZONE]
      start_time = timezone.local(date.year, date.month, date.day)
      # Calculate end time on date + 1 so that daylight saving time doesn't mess up the reports.
      next_day = date + 1
      end_time = timezone.local(next_day.year, next_day.month, next_day.day)
      finder = finder.where("jobs.adjusted_created_at >= ? AND jobs.adjusted_created_at < ?", start_time, end_time)

      # Header
      timestamp = Time.now.in_time_zone(REPORT_TIME_ZONE)
      date = timestamp.strftime("%Y-%m-%d")
      time = timestamp.strftime("%H:%M:%S")
      file.puts("H#{fixed_width(VENDOR_NAME, 20)}#{fixed_width(date, 10)}#{fixed_width(time, 8)}#{fixed_width(' ', 2)}#{fixed_width(1, 10, left: '0')}")

      job_count = 0
      finder.find_each(batch_size: 100) do |job|
        service_code = service_code_id(job)
        next unless service_code
        job_count += 1
        line = String.new
        line << fixed_width(job.id, 10)
        line << fixed_width(task_id(job), 2, left: '0')
        line << fixed_width(job.stranded_vehicle&.vin, 17)
        line << fixed_width(program_code_id(job), 5)
        line << fixed_width(job.drop_location&.site&.site_code, 5, left: '0')
        line << fixed_width(job.odometer&.round, 6, left: '0')
        line << fixed_width(service_code, 6)
        line << fixed_width(job.human_status, 50)
        line << "N"
        line << fixed_width(job.customer&.name, 50)
        line << fixed_width(Phonelib.parse(job.customer&.phone).national(false), 20)
        file.puts(line)
      end

      file.puts("T#{fixed_width(VENDOR_NAME, 20)}#{fixed_width(job_count, 10, left: '0')}")

      file.flush
      File.open(file.path, encoding: file.external_encoding) do |f|
        file.unlink
        yield f
      end
    ensure
      file.close unless file.closed?
    end
  end

  # Translate program code to the values expected in the report that
  # distinguish ESP coverage from original warranty coverage.
  def program_code_id(job)
    if esp_job?(job)
      "1416"
    else
      "1415"
    end
  end

  # Translate Swoop service codes to the values expected in the report.
  def service_code_id(job)
    name = job.service_code&.name
    if name == ServiceCode::TOW || name == ServiceCode::ACCIDENT_TOW
      vehicle_catetory = job.rescue_vehicle&.vehicle_category
      if vehicle_catetory&.name != VehicleCategory::WHEEL_LIFT
        "CCM001"
      else
        "CCM002"
      end
    elsif name == ServiceCode::LOCK_OUT
      "CCM003"
    elsif name == ServiceCode::BATTERY_JUMP
      "CCM004"
    elsif name == ServiceCode::TIRE_CHANGE
      "CCM005"
    elsif name == ServiceCode::FUEL_DELIVERY
      "CCM006"
    elsif name == ServiceCode::WINCH_OUT
      "CCM007"
    else
      nil
    end
  end

  private

  # Helper method to truncate/pad out a value to fit in a fixed width format.
  def fixed_width(value, length, left: nil, right: nil)
    value = value.to_s[0, length]
    if left
      value = value.rjust(length, left)
    else
      value = value.ljust(length, right || ' ')
    end
    value
  end

  def task_id(job)
    # TODO implement correct logic once jobs get a universal case ID
    1
  end

  def esp_job?(job)
    program_code(job).to_s.start_with?(LINCOLN_ESP_PROGRAM_PREFIX)
  end

  def mechanical_issue?(job)
    job.symptom&.name == Symptom::MECHANICAL_ISSUE
  end

  def program_code(job)
    job.pcc_coverage&.client_program&.code
  end

  def vehicle_warranty_start_date(job)
    program_info = policy_program(job, LINCOLN_WARRANTY_PROGRAM_CODE)
    if program_info.present? && program_info["start_date"].present?
      Date.iso8601(program_info["start_date"])
    else
      nil
    end
  end

  def policy_program(job, program_code)
    policy_data = job.pcc_coverage&.pcc_policy&.data
    return nil unless policy_data.is_a?(Hash)
    Array(policy_data["programs"]).detect do |program|
      next unless program.is_a?(Hash)
      program["program_code"] == program_code
    end
  end

end
