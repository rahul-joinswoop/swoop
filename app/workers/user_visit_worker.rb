# frozen_string_literal: true

# Record or increment the session count for a user visit.
class UserVisitWorker

  include Sidekiq::Worker

  sidekiq_options retry: false

  def perform(iso_date, user_id, source_application_id)
    date = Date.parse(iso_date)
    attributes = { visit_date: date, user_id: user_id, source_application_id: source_application_id }
    first_attempt = true

    User::Visit.transaction do
      visit = User::Visit.find_by(attributes)
      if visit
        User::Visit.increment_counter(:session_count, visit.id)
      else
        User::Visit.create!(attributes.merge(session_count: 1))
      end
    rescue ActiveRecord::RecordNotUnique
      # Race condition inserting a unique visit record. Retry to increment the session count instead.
      raise unless first_attempt
      first_attempt = false
      retry
    end
  end

end
