# frozen_string_literal: true

require 'pagerduty_alert'

class RescueVehicleLocationAlert

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'low', retry: 0

  def perform
    trucks = RescueVehicle.stale_auto_dispatch_location
    trucks.map do |truck|
      mins = ((Time.zone.now - truck.location_updated_at) / 60).floor
      msg = "Auto Dispatch Truck #{truck.id} from company #{truck.company.name} is enabled but hasn't received an update for #{mins} minutes"
      if truck.driver && truck.driver.on_duty?
        SlackChannel.message(msg, tags: 'truck_locations')
      end
    end
  end

end
