# frozen_string_literal: true

class AuditChangedCompanySubscriptionStatusWorker

  include Sidekiq::Worker

  def perform(company_id)
    @company = Company.find_by(id: company_id)
    return if @company.nil?
    return if @company.subscription_status_id.nil?

    @audit_record = latest_audit_record

    if @audit_record.blank? || subscription_status_changed?
      AuditCompanySubscriptionStatus
        .create!(
          company_id: @company.id,
          subscription_status_id: @company.subscription_status_id
        )
    end
  end

  private

  def latest_audit_record
    AuditCompanySubscriptionStatus
      .where(company_id: @company.id)
      .order(created_at: :desc)
      .first
  end

  def subscription_status_changed?
    @audit_record.subscription_status_id != @company.subscription_status_id
  end

end
