# frozen_string_literal: true

# Update rescue vehicle location
class UpdateVehicleLocationWorker

  include Sidekiq::Worker
  # TODO won't need unique args if moved out of the database and we could retry, but will have to deal with the ArgumentErrors.
  sidekiq_options queue: 'vehicle_location', retry: false, lock: :until_executed, on_conflict: :replace, unique_args: :unique_args

  class << self

    # Limit the number of location updates in the queue to at most one per job every
    # 5 minutes. Location updates can be very frequent and when there are system
    # issues that cause queuing backlog, these jobs can prevent a speedy recovery of
    # backlogged queues.
    def unique_args(args)
      company_id, vehicle_id, _lat, _lng, timestamp = args
      time_bucket = timestamp.to_time.to_i / 5.minutes.to_i
      [company_id, vehicle_id, time_bucket]
    end

  end

  def perform(company_id, vehicle_id, lat, lng, timestamp)
    vehicle = RescueVehicle.find_by(id: vehicle_id)
    return if vehicle.nil?

    if vehicle.company_id != company_id.to_i
      # malicious update; this is checked here rather than in the controller to keep that endpoint as light as possible
      error = ArgumentError.new("Illegal vehicle location update: company_id: #{company_id}, vehicle.company_id: #{vehicle.company_id}")
      Rollbar.error(error)
      return
    end

    vehicle.update_location!(lat: lat, lng: lng, timestamp: timestamp)

    run_geofencing(vehicle)
  end

  def run_geofencing(vehicle)
    return unless vehicle.company
    return unless vehicle.is_available?
    return unless geofencing_enabled?(vehicle)

    interactor_context = Geofence::EligibilityEvaluator.call(
      rescue_vehicle: vehicle,
      jobs: vehicle.current_jobs.eager_load(:drop_location, :service_location),
    )
    if interactor_context.failure?
      msg = interactor_context.error
      Rails.logger.warn("[ #{self.class} ] - EligibilityEvaluator failure: #{msg}")
    end
  rescue => e
    Rollbar.error(e)
  end

  def geofencing_enabled?(vehicle)
    !!vehicle.company&.setting(Setting::GEOFENCE_PROGRESS_STATUS)
  end

end
