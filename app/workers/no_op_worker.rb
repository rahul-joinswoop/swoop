# frozen_string_literal: true

# This crazy class is a Sidekiq worker that does nothing.
# It is provided for cases where logic determines a worker to execute
# based on some runtime criteria to fill any holes where some criteria
# don't have an implementation.
class NoOpWorker

  include Sidekiq::Worker

  class << self

    # Don't even both scheduling this guy.
    def perform_async(*args)
    end

    alias_method :perform_in, :perform_async
    alias_method :perform_at, :perform_async

  end

  def perform(*args)
    # Do nothing
  end

end
