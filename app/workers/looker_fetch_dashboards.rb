# frozen_string_literal: true

require 'issc'

class LookerFetchDashboards

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: 1

  def initialize()
  end

  def perform
    Looker::FetchDashboards.new.call
  end

end
