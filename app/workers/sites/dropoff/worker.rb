# frozen_string_literal: true

module Sites
  module Dropoff
    class Worker

      include LoggableJob
      include Sidekiq::Worker

      sidekiq_options queue: 'high', retry: false

      #
      # The number of miles to add and subtract around the pickup location to quickly eliminate
      # sites that won't be used in the calculation (before sending to google for accurate miles)
      #
      DROPOFF_SITES_BOUNDING_BOX_MAX_DISTANCE_DEFAULT = ENV.fetch('DROPOFF_SITES_BOUNDING_BOX_MAX_DISTANCE_DEFAULT', 100).to_i

      #
      # Max dropoff_sites returned.
      #
      DROPOFF_SITES_MAX_RESULTS_DEFAULT = ENV.fetch('DROPOFF_SITES_MAX_RESULTS_DEFAULT', 5)

      # TODO move Lincoln specific settings to a table
      FORD_MOTOR_COMPANY_VEHICLE = 'lincoln_ford_company_vehicle'

      def perform(async_request_id, job_id, drop_sites_params)
        request = API::AsyncRequest.find(async_request_id)
        job = Job.find(job_id)

        log :debug, "Sites::DropoffWorker - called with\n request:#{request},\n drop_sites_params:#{drop_sites_params}", job: job.id

        program_code = drop_sites_params[:program_code]

        # TODO move this logic to a table; it's currently only used by Lincoln,
        # but it should be extended to all clients
        if job.fleet_company.name == 'Lincoln'
          if program_code == FORD_MOTOR_COMPANY_VEHICLE
            location_type_names = ['Dealership', 'HQ']
          else
            location_type_names = ['Dealership']
          end
        end

        service_location_drop_sites_params = drop_sites_params.dig('job', 'location', 'service_location')
        origin = Location.new(lat: service_location_drop_sites_params['lat'],
                              lng: service_location_drop_sites_params['lng'])

        nearest_sites = ::SiteService::Nearest.new(
          FleetSite,
          origin,
          DROPOFF_SITES_BOUNDING_BOX_MAX_DISTANCE_DEFAULT,
          location_type_names,
          job.fleet_company,
          job.id
        ).call

        dropoff_sites = SiteService::DistanceMatrix.new(nearest_sites, origin).call

        results = dropoff_sites[0..(DROPOFF_SITES_MAX_RESULTS_DEFAULT - 1)]

        dropoff_sites_hash = ActiveModelSerializers::SerializableResource.new(
          results.map { |site_hash| site_hash[:site] }
        ).serializable_hash

        ret = []
        dropoff_sites_hash.zip(results) do |site, distance_hash|
          ret << {
            site: site,
            miles: distance_hash[:miles],
          }
        end

        output = { dropoff_sites: ret }

        PCC::Publishers::Websocket.call(
          input: output,
          async_request_id: async_request_id
        )
      end

    end
  end
end
