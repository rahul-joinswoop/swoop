# frozen_string_literal: true

# TODO safely delete this worker after its generic version Sites::Dropoff::Worker is released in PROD
# https://swoopme.atlassian.net/browse/ENG-12487
module Sites
  module Dropoff
    module Lincoln
      class Worker

        include LoggableJob

        FORD_MOTOR_COMPANY_VEHICLE = 'lincoln_ford_company_vehicle'

        # The number of miles to add and subtract around the pickup location to quickly eliminate
        # sites that won't be used in the calculation (before sending to google for accurate miles)
        DROPOFF_SITES_BOUNDING_BOX_MAX_DISTANCE = (ENV['DROPOFF_SITES_BOUNDING_BOX_MAX_DISTANCE'] || 75).to_i

        include Sidekiq::Worker
        sidekiq_options queue: 'high', retry: false

        def perform(async_request_id, job_id, args)
          request = API::AsyncRequest.find(async_request_id)
          job = Job.find(job_id)
          log :debug, "Sites::DropoffWorker - called with\n request:#{request},\n args:#{args}", job: job.id

          program_code = args[:program_code]

          if program_code == FORD_MOTOR_COMPANY_VEHICLE
            location_type_names = ['Dealership', 'HQ']
          else
            location_type_names = ['Dealership']
          end
          service_location_args = args.dig('job', 'location', 'service_location')
          origin = Location.new(lat: service_location_args['lat'],
                                lng: service_location_args['lng'])
          nearest_sites = ::SiteService::Nearest.new(
            FleetSite,
            origin,
            DROPOFF_SITES_BOUNDING_BOX_MAX_DISTANCE,
            location_type_names,
            job.fleet_company,
            job.id
          ).call

          dropoff_sites = SiteService::DistanceMatrix.new(nearest_sites, origin).call

          top5 = dropoff_sites[0..4]

          dropoff_sites_hash = ActiveModelSerializers::SerializableResource.new(
            top5.map { |site_hash| site_hash[:site] }
          ).serializable_hash

          ret = []
          dropoff_sites_hash.zip(top5) do |site, distance_hash|
            ret << {
              site: site,
              miles: distance_hash[:miles],
            }
          end

          output = { dropoff_sites: ret }

          PCC::Publishers::Websocket.call(
            input: output,
            async_request_id: async_request_id
          )
        end

      end
    end
  end
end
