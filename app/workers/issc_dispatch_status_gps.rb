# frozen_string_literal: true

require 'issc'

class IsscDispatchStatusGps < IsscDispatchStatus

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: false

  def perform
    @status = 'GPS'
    @source = 'Automated'
    @issccon.ensure_connected do |client|
      statuses = []
      query.each do |job|
        location = job.rescue_vehicle&.current_location
        if location && location.timestamp >= 5.minutes.ago
          payload = status_payload(job, job.rescue_vehicle.updated_at, location.timestamp)
          payload[:Latitude] = location.lat
          payload[:Longitude] = location.lng
          payload[:GPSLastUpdateTimeUTC] = location.timestamp
          payload[:Driver] = job.rescue_driver_id.to_s if job.rescue_driver_id
          statuses << payload
        end
      end

      client.dispatchstatus(nil, statuses)
    end
  end

  private

  def query
    FleetMotorClubJob.includes(:rescue_vehicle)
      .where(status: ['En Route', 'Towing'], deleted_at: nil, parent_id: nil)
      .joins(issc_dispatch_request: :issc)
      .where(isscs: { system: [:issc, nil] })
      .order(:id)
  end

end
