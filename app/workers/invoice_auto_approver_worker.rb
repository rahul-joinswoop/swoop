# frozen_string_literal: true

class InvoiceAutoApproverWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'low'

  def perform(invoice_id)
    InvoiceAutoApprover.call(invoice: Invoice.find(invoice_id))
  end

end
