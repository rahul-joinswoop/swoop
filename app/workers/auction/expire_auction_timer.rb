# frozen_string_literal: true

module Auction
  class ExpireAuctionTimer

    include Sidekiq::Worker

    sidekiq_options queue: 'auction', retry: 5

    def perform(auction_id)
      service = ExpireAuctionService.new(auction_id)
      service.call
    end

  end
end
