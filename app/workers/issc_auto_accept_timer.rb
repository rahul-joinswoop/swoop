# frozen_string_literal: true

class IsscAutoAcceptTimer

  include LoggableJob
  include Sidekiq::Worker
  sidekiq_options queue: 'issc', retry: 1

  def perform(job_id)
    log :debug, "Auto accept timer expired, DUMMY to stop errors, not doing anything", job: job_id
  end

end
