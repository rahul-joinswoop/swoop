# frozen_string_literal: true

class IsscDispatchResponseAccept < IsscDispatchResponse

  # TODO should we config sidekiq options here?

  def perform(job_id, source = :Manual)
    @issccon.ensure_connected do |client|
      Job.transaction do
        job = Job.find(job_id)

        unless ["submitted"].include?(job.status)
          raise(
            ArgumentError,
            "ISSC(#{job_id}) IsscDispatchResponse - invalid state for Issc " \
            " update (#{job.status}) must be Job::STATUS_SUBMITTED"
          )
        end

        # Convert our time-based ETA into a rough number of minutes
        status = JobStatus::NAMES[:ACCEPTED]

        phone_number = provider_phone_number(job)
        provider_name = provider_name(job)

        payload = {
          DispatchID: job.issc_dispatch_request.dispatchid,
          JobID: job.issc_dispatch_request.jobid,
          ServiceProviderResponse: status,
          Source: source,
          ClientID: job.issc_dispatch_request.issc.clientid,
          ContractorID: job.issc_dispatch_request.issc.contractorid,
          ProviderContactName: provider_name,
          ProviderPhoneNumber: phone_number,
        }
        add_locationid(job, payload)

        if job.scheduled_for
          eta = ((job.scheduled_for - Time.now) / 60.0).round
        else
          eta = job.bta.duration
        end

        payload[:ETA] = eta

        explanation =
          ISSC::JobEtaExplanationMapper.new(
            job_id: job.id, eta: eta
          ).call.explanation

        if explanation
          payload[:ETAExplanation] = explanation
        end

        if ENV['ISSC_SIMULATE_RANDOM_FAILURES']
          if rand > 0.5
            payload[:SimulateResponse] = ['Rejected', 'Canceled', 'Expired'].sample
          end
        end

        if ENV['ISSC_SIMULATE_FAILURE']
          payload[:SimulateResponse] = ENV['ISSC_SIMULATE_FAILURE']
        end

        if ENV['ISSC_SIMULATE_RESPONSE']
          payload[:SimulateResponse] = ENV['ISSC_SIMULATE_RESPONSE']
        end

        Rails.logger.debug "ISSCDispatchResponseAccepted #{payload}"
        client.dispatchresponse(job, payload)
      end
    end
  end

end
