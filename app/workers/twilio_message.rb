# frozen_string_literal: true

require 'swoop_twilio'

class TwilioMessage

  include Sidekiq::Worker
  include TwilioConcerns

  sidekiq_options queue: 'twilio'

  def perform(phone, body, extra = {})
    if phone
      Rails.logger.debug "Twilio sending SMS: #{phone}:#{body}, extra:#{extra}"
      msg = { to: phone, body: body }

      if !ENV['TWILIO_REVIEW_SID'].nil?
        msg[:messaging_service_sid] = ENV['TWILIO_REVIEW_SID']
      else
        msg[:from] = sticky_sms_number(phone)
      end

      audit = AuditSms.create!(extra.merge(msg))
      msg[:status_callback] = audit.status_callback_url

      Rails.logger.debug "Twilio msg: #{msg}"
      SwoopTwilio.new.message_create(msg)

      audit
    end
  rescue StandardError => e
    Rails.logger.error "TWILIO MESSAGE: Exception connecting #{e}"
    Rails.logger.error e.backtrace.join("\n")
    Rollbar.error(e)
  end

end
