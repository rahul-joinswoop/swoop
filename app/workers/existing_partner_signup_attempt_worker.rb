# frozen_string_literal: true

class ExistingPartnerSignupAttemptWorker

  include Sidekiq::Worker
  include Utils

  attr_reader :company

  SUBJECT = 'Attempted Sign Up'

  def perform(company_id)
    load_company(company_id)
    send_email_notification(company, email, name)
  end

  private

  def email
    if @company.primary_contact.nil?
      @email ||= @company.dispatch_email
    else
      @email = @company.primary_contact.email
    end
  end

  def name
    if @company.primary_contact.nil?
      @name = 'Unknown'
    else
      @name = @company.primary_contact.name
    end
  end

  def load_company(company_id)
    @company ||= Company.find(company_id)
  end

  def send_email_notification(company, email, name)
    SystemMailer.send_mail(
      {
        template_name: 'existing_partner_signup',
        to: 'accountcreated@joinswoop.com',
        from: SWOOP_OPERATIONS_EMAIL,
        subject: SUBJECT,
        locals: { :@company => company, :@name => name, :@email => email },
      }, nil, nil, company, SUBJECT, company
    ).deliver_now
  end

end
