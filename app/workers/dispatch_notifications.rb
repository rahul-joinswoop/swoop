# frozen_string_literal: true

class DispatchNotifications

  include LoggableJob
  include Sidekiq::Worker

  sidekiq_options queue: 'high', retry: 5

  def perform
    stagnent_jobs.each do |job|
      next unless should_notify_again?(job)

      send_notification(job)
    end
  end

  # private
  def stagnent_jobs
    Job.where(status: %w(Pending Created Unassigned Assigned), type: 'FleetManagedJob')
      .where("audit_job_statuses.created_at < ? ", 5.minutes.ago)
      .where("job_statuses.name = jobs.status")
      .where("jobs.deleted_at IS NULL")
      .where("jobs.scheduled_for IS NULL")
      .joins(audit_job_statuses: :job_status)
  end

  def send_notification(job)
    message = message_for(job)

    job.slack_notification ":exclamation: #{message}"

    Notification.create!(message: message, job: job, timeout: 300000)

    calc = SystemEmailRecipientCalculator.new(job)
    users = calc.calculate
    users.each do |user|
      Job.delay.send_sms(user.phone, message, {
        user_id: user.id,
        job_id: job.id,
        type: 'JobAuditSms',
      })
    end
  end

  def message_for(job)
    status = job.human_status

    log :debug, "Creating message_for #{job.id} #{job.status}", job: job.id

    if status.eql?("Assigned")
      "Job Not Accepted "
    else
      "Job Still #{status} "
    end + job.swoop_summary_name
  end

  def should_notify_again?(job)
    notifs = job.notifications.where("created_at > ?", 5.minutes.ago)
    notifs.blank?
  end

end
