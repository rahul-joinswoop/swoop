# frozen_string_literal: true

class TruckSimulatorWorker

  include Sidekiq::Worker

  sidekiq_options retry: 0

  def perform(truck_id, directions_id)
    truck = RescueVehicle.find truck_id
    redis = RedisClient[:default]
    data = redis.lpop(directions_id)
    if data.present?
      (lat, lng) = MultiJson.load(data)
      UpdateVehicleLocationWorker.perform_async(truck.company_id, truck.id, lat, lng, Time.current)
      self.class.perform_in(TruckSimulator::UPDATE_FREQUENCY_SECONDS, truck_id, directions_id)
    else
      redis.del(directions_id)
    end
  end

end
