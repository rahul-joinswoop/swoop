# frozen_string_literal: true

class DisableStorageAutoCalculationWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'low'

  NUM_DAYS = 90

  # find storage job invoices that are 90 days old where auto_calculated_quantity is enabled and disable it,
  # see ENG-6249 for more info
  def perform(date)
    date = DateTime.parse(date.to_s)
    to = date - NUM_DAYS
    from = to - 1

    InvoicingLineItem
      .active_storage
      .where(auto_calculated_quantity: true)
      .where(jobs: { created_at: from..to })
      .in_batches(of: 100) do |items|
        items.each do |item|
          # disable auto_calculating
          item.update! auto_calculated_quantity: false
        end
      end
  end

end
