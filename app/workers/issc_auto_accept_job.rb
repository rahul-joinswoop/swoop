# frozen_string_literal: true

class IsscAutoAcceptJob

  include LoggableJob
  include Sidekiq::Worker

  sidekiq_options queue: 'issc', retry: 0

  def perform(job_id)
    @job = Job.find(job_id)

    if service_obj.nil?
      log :debug, "IsscAutoAcceptJob lacks available handler, exiting"
      return
    end

    ApplicationRecord.transaction do
      service_obj.execute
    end

    service_obj.after_execution
  end

  private

  def service_obj
    if @job.rescue_company.has_feature?(Feature::MC_JOB_AUTO_REJECT_OVERRIDE)
      ISSC::RejectJob.new(@job)
    elsif @job.account.auto_accept
      ISSC::AcceptJob.new(@job)
    end
  end

end
