# frozen_string_literal: true

require 'issc'
class IsscDisconnect

  include Sidekiq::Worker
  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform
    raise ArgumentError, "Must have ISSC Auth token set using ENV - ISSC_AUTH_TOKEN" unless ENV['ISSC_AUTH_TOKEN']
    raise ArgumentError, "Must have ISSC Webhook URL set using ENV - ISSC_WEBHOOK_URL" unless ENV['ISSC_WEBHOOK_URL']
    @issccon.issc_client.disconnect
  end

end
