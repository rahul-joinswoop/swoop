# frozen_string_literal: true

class Job::Candidate::StatusCheck

  include Sidekiq::Worker

  sidekiq_options queue: 'auction', retry: 5

  def perform(job_id)
    assigner = Job::Candidate::Assignment.for_job(job_id)

    # We have been rejected but our old job is still running,
    # so just bail there is no need to update the target.
    return unless assigner.status.eql?(Job::Candidate::Assignment::ASSIGNED)

    if assigner.job.assigned?
      assigner.slack_notification ":exclamation: Job has not been accepted after #{AutoAssignService::STATUS_CHECK_TIME}minutes"
      calc = SystemEmailRecipientCalculator.new(assigner.job)
      users = calc.calculate
      users.each do |user|
        Job.delay.send_sms(user.phone,
                           "No action taken on job - #{assigner.job.swoop_summary_name}", {
                             user_id: user.id,
                             job_id: job_id,
                             type: 'JobAuditSms',
                           })
      end
    end
  end

end
