# frozen_string_literal: true

class Email::PartnerAPIPasswordRequestWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'low'

  def perform(password_request_id)
    password_request = PartnerAPIPasswordRequest.find password_request_id

    # we don't have to check for password_request.user_email because that's a validation requirement in the
    # model class
    if password_request.valid_request?
      args = {
        template_name: 'partner_api_password_request',
        to: password_request.user_email,
        from: 'Swoop <hello@joinswoop.com>',
        subject: "Invoicing Agero (Swoop) Jobs",
        locals: {
          :@ageroSwoopLogo => true,
          :@password_request => password_request,
          :@app => password_request.application,
          :@smtp_api_values => { "filters.clicktrack.settings.enable" => 0 },
        },
      }
      SystemMailer.send_mail(args, nil, nil, nil, 'Password Request', password_request).deliver_now
    end
  end

end
