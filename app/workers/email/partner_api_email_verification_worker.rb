# frozen_string_literal: true

class Email::PartnerAPIEmailVerificationWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'low'

  def perform(email_verification_id)
    email_verification = PartnerAPIEmailVerification.find email_verification_id

    if email_verification.user_email && email_verification.valid_request?
      # TODO - need to update the template with the correct url
      args = {
        template_name: 'partner_api_email_verification',
        to: email_verification.user_email,
        from: 'Swoop <hello@joinswoop.com>',
        subject: "Verify Swoop Email",
        locals: {
          :@ageroSwoopLogo => true,
          :@email_verification => email_verification,
          :@app => email_verification.application,
          :@smtp_api_values => { "filters.clicktrack.settings.enable" => 0 },
        },
      }
      SystemMailer.send_mail(args, nil, nil, nil, 'Email Verification', email_verification).deliver_now
    end
  end

end
