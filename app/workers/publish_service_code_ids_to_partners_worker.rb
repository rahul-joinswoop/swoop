# frozen_string_literal: true

class PublishServiceCodeIdsToPartnersWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'ws', retry: 1

  def perform(client_company_id, addon, should_publish_partner_service_codes = true)
    client_company = Company.select(:id, :name).find(client_company_id)

    Rails.logger.debug(
      "PublishServiceCodeIdsToPartnersWorker calling ServiceCodeIdList#base_publish to all <id #{client_company.id}> #{client_company.name}'s partners"
    )

    partner_ids = Account.where(client_company_id: client_company_id).pluck(:company_id)

    fleet_service_code_ids = client_company.service_codes.where(addon: addon).pluck(:id)

    partner_ids.each do |partner_id|
      if should_publish_partner_service_codes
        partner_service_code_ids =
          CompaniesService
            .joins(:service_code).where(service_codes: { addon: addon })
            .where(company_id: partner_id).pluck(:service_code_id)

        ServiceCodeIdList.new(
          company_id_to_publish_to: partner_id,
          company_id_to_be_added_to_json: partner_id,
          service_code_ids: partner_service_code_ids,
          addon: addon
        ).base_publish('update')
      end

      ServiceCodeIdList.new(
        company_id_to_publish_to: partner_id,
        company_id_to_be_added_to_json: client_company_id,
        service_code_ids: fleet_service_code_ids,
        addon: addon
      ).base_publish('update')
    end
  end

end
