# frozen_string_literal: true

require 'csv'

# Generate the RDA report for Lincoln jobs and upload it to an SFTP server.
# The time frame for the report is the week ending with the date passed in (inclusive).
class LincolnRdaReportWorker

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  # sidekiq_options lock: :until_executed

  REPORT_TIME_ZONE = "America/New_York"
  REPORT_JOB_STATUSES = ["Completed", "Released"].freeze
  REPORT_DIRECTORY = "incoming"
  REPORT_BASENAME = "LincolnRDAReport"

  CSV_HEADERS = [
    "caller_last_nm",
    "caller_first_nm",
    "home_phone_nbr",
    "first_nm",
    "last_nm",
    "mail_strt_addr_1",
    "mail_strt_addr_2",
    "mail_city",
    "mail_st",
    "mail_zip",
    "vehicle_id_nbr",
    "vehicle_year_nbr",
    "vehicle_model",
    "vehicle_make",
    "service_type_cd",
    "contact_time",
    "dispatch_time",
    "vndr_id",
    "vndr_st",
    "eta",
    "purch_order_id",
    "Dealer tow to name 668",
    "csr_user_id",
    "Email Address",
  ].freeze

  def perform(date)
    date = Date.parse(date) if date.is_a?(String)
    sftp_server = SftpServer.new(ENV["LINCOLN_SFTP_SERVER_URI"], password: ENV["LINCOLN_SFTP_PASSWORD"], ssh_key: ENV["LINCOLN_SFTP_SSH_KEY"])
    generate_report(date) do |stream|
      sftp_server.upload(stream, "#{REPORT_DIRECTORY}/#{REPORT_BASENAME}.#{date.iso8601}.csv")
    end
  end

  # Generate the fixed width report and yield it as a stream.
  def generate_report(date)
    file = Tempfile.new("lincoln_rda", encoding: "UTF-8")
    begin
      finder = Job.where(fleet_company_id: Company.lincoln_id)
      finder = finder.where(status: REPORT_JOB_STATUSES)
      finder = finder.joins(:pcc_coverage).where(pcc_coverages: { result: [PCC::Coverage::COVERED_RESULT, PCC::Coverage::PARTIAL_COVERAGE_RESULT] })
      finder = finder.includes([:user, :service_code, :service_location, :drop_location, :pickup_contact, { driver: [:user, :vehicle], pcc_coverage: :pcc_policy }])

      timezone = ActiveSupport::TimeZone[REPORT_TIME_ZONE]
      start_date = date - 6
      start_time = timezone.local(start_date.year, start_date.month, start_date.day)
      end_date = date + 1
      end_time = timezone.local(end_date.year, end_date.month, end_date.day)
      finder = finder.where("jobs.adjusted_created_at >= ? AND jobs.adjusted_created_at < ?", start_time, end_time)

      csv = CSV.new(file, headers: CSV_HEADERS)
      csv << CSV_HEADERS
      finder.find_each(batch_size: 100) do |job|
        next if reimbursement?(job)

        policy_customer = lookup_policy_customer(job.pcc_coverage.pcc_policy)
        customer = (job.pickup_contact || job.customer)
        next if job.email.blank? && (policy_customer.nil? || policy_customer["street_address"].blank?)

        row = HashWithIndifferentAccess.new
        row[:caller_last_nm] = customer&.last_name
        row[:caller_first_nm] = customer&.first_name
        row[:home_phone_nbr] = Phonelib.parse(job.customer&.phone).international(false)
        row[:first_nm] = job.customer&.first_name
        row[:last_nm] = job.customer&.last_name
        if policy_customer
          row[:mail_strt_addr_1] = policy_customer["street_address"]
          row[:mail_strt_addr_2] = nil
          row[:mail_city] = policy_customer["city"]
          row[:mail_st] = policy_customer["region"]
          row[:mail_zip] = policy_customer["postal_code"]
        end
        row[:vehicle_id_nbr] = job.stranded_vehicle&.vin
        row[:vehicle_year_nbr] = job.stranded_vehicle&.year
        row[:vehicle_model] = job.stranded_vehicle&.model
        row[:vehicle_make] = job.stranded_vehicle&.make
        row[:service_type_cd] = job.service_code&.name
        row[:contact_time] = job.adjusted_created_at.in_time_zone(REPORT_TIME_ZONE).strftime("%Y-%m-%d %H:%M:%S")
        dispatched_at = (job.dispatched_at || job.adjusted_created_at)
        if dispatched_at
          row[:dispatch_time] = dispatched_at.in_time_zone(REPORT_TIME_ZONE).strftime("%Y-%m-%d %H:%M:%S")
        end
        row[:vndr_id] = job.rescue_company_id
        row[:vndr_st] = job.service_location&.state
        row[:eta] = job.orig_eta_mins
        row[:purch_order_id] = job.id
        row[:"Dealer tow to name 668"] = job.drop_location&.site_id
        row[:csr_user_id] = job.user&.name
        row[:"Email Address"] = job.email

        row_array = CSV_HEADERS.inject([]) { |array, name| array << row[name] }
        csv << row_array
      end

      file.flush
      File.open(file.path, encoding: file.external_encoding) do |f|
        file.unlink
        yield f
      end
    ensure
      file.close unless file.closed?
    end
  end

  private

  def reimbursement?(job)
    job.service_code.present? && job.service_code.name.match(/reimbursement/i)
  end

  def lookup_policy_customer(pcc_policy)
    policy_uuid = pcc_policy&.uuid
    return unless policy_uuid
    client = PolicyLookupService::Client.new(PCC::PolicyLookup::Lincoln::CLIENT)
    policy = client.policy(policy_uuid)
    policy["customers"].first if policy
  end

end
