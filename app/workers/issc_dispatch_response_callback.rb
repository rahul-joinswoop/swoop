# frozen_string_literal: true

class IsscDispatchResponseCallback < IsscDispatchResponse

  def perform(job_id, source = :Manual)
    @issccon.ensure_connected do |client|
      Job.transaction do
        job = Job.find(job_id)
        job.issc_dispatch_request.club_job_type = 'Callback Requested'
        job.save!

        payload = {
          DispatchID: job.issc_dispatch_request.dispatchid,
          JobID: job.issc_dispatch_request.jobid,
          ServiceProviderResponse: 'PhoneCallRequested',
          Source: source,
          ClientID: job.issc_dispatch_request.issc.clientid,
          ContractorID: job.issc_dispatch_request.issc.contractorid,
          ProviderContactName: provider_name(job),
          ProviderPhoneNumber: provider_phone_number(job),
        }
        add_locationid(job, payload)
        Rails.logger.debug "ISSCDispatchResponseCallback #{payload}"
        client.dispatchresponse(job, payload)

        # Logout if this is the last item we've responded to
        # IsscFixLogins.new.perform
      end
    end
  end

end
