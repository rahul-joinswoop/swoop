# frozen_string_literal: true

require 'pagerduty_alert'

class JobStatusAlert

  include Utils
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'low', retry: false

  @@lock = Mutex.new

  def perform
    count = jobs_in_created.count

    case count
    when 0
      resolve_last_incident
    else
      create_incident_for_count(count)
    end
  end

  def incident
    self.class.incident
  end

  def incident=(inci)
    self.class.incident = inci
  end

  private

  def create_incident_for_count(count)
    return if incident.present? # We already have an incident created
    self.incident = PagerdutyAlert.create("Too many jobs #{count} in created status on #{hostname}", details: {})
  end

  def resolve_last_incident
    return if incident.blank?
    PagerdutyAlert.resolve(incident)
  end

  def hostname
    URI(ENV['SITE_URL']).hostname
  end

  def jobs_in_created(time = 1.minute)
    Job.where(status: [Job::CREATED, Job::SUBMITTED])
      .where("audit_job_statuses.created_at < ?", time.ago)
      .where("job_statuses.name = jobs.status")
      .joins(audit_job_statuses: [:job_status])
  end

  class << self

    def incident
      # Just incase someone else is writing to us
      @@lock.synchronize do
        @incident
      end
    end

    def incident=(inci)
      @@lock.synchronize do
        @incident = inci
      end
    end

  end

end
