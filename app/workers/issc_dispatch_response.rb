# frozen_string_literal: true

require 'issc'

class IsscDispatchResponse

  include Sidekiq::Worker
  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def add_locationid(job, payload)
    clientid = job.issc_dispatch_request.issc.clientid

    if Issc.provider_supports_location? clientid
      if job.issc_dispatch_request.issc.issc_location && job.issc_dispatch_request.issc.issc_location.locationid
        payload[:LocationID] = job.issc_dispatch_request.issc.issc_location.locationid
      end
    end
  end

  def provider_phone_number(job)
    if job.partner_dispatcher.try(:phone)
      phone_number = job.partner_dispatcher.phone
    elsif job.rescue_company.try(:phone)
      phone_number = job.rescue_company.phone
    else
      raise ArgumentError, "ISSC(#{job.id}) IsscDispatchResponse - either partrner_dispatcher or resuce_company need a phone number"
    end

    phone = Phonelib.parse(phone_number)
    phone.national(false)
  end

  def provider_name(job)
    if job.partner_dispatcher
      job.partner_dispatcher.full_name
    elsif job.site.manager
      job.site.manager.full_name
    else
      "Dispatcher"
    end
  end

end
