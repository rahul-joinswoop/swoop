# frozen_string_literal: true

class DelayedSlackSurveyWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'slack', retry: 2

  def perform(review_id)
    SurveyService::Slack.new(review_id).call
  end

end
