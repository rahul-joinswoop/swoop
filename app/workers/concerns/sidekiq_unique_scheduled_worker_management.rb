# frozen_string_literal: true

module SidekiqUniqueScheduledWorkerManagement

  extend ActiveSupport::Concern

  included do
    sidekiq_options lock: :until_and_while_executing, on_conflict: :null
  end

  module ClassMethods

    def schedule(*args)
      redis_client.del(key(*args[1..-1]))

      perform_in(*args)
    end

    def deschedule(*args)
      expiration = args.shift + 10
      _key = key(*args)
      redis_client.set(_key, _key, ex: expiration)
    end

    def redis_client
      RedisClient[:default]
    end

    def no_op?(*args)
      redis_client.get(key(*args)).present?
    end

    def key(*args)
      "#{name}__#{args.join('__')}"
    end

  end

  def perform(*args)
    call(*args) unless self.class.no_op?(*args)
  end

end
