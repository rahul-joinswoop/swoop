# frozen_string_literal: true

module Stripe
  class CheckChargesInIndeterminateStatusWorker

    include LoggableJob
    include Sidekiq::Worker

    # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
    sidekiq_options queue: 'low', retry: false

    def perform
      charges = InvoicePaymentCharge.where(
        alert_sent: false,
        status: InvoicePaymentCharge::CHARGE_STATUS_RUNNING
      ).where(
        'created_at < ?', 2.minutes.ago
      )

      log :debug, "charge_ids found: #{charges.map(&:id)}"

      charges.each do |charge|
        # alert through email
        email = Stripe::EmailChargeInIndeterminateStatus.new(charge.id)
        Delayed::Job.enqueue(email)

        # update alert flag so we don't send more than once
        charge.update!(alert_sent: true)

        log :debug, "charge.alert_sent: #{charge.alert_sent}"
      end
    end

  end
end
