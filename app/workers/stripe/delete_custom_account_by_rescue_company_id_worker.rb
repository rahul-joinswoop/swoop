# frozen_string_literal: true

module Stripe
  class DeleteCustomAccountByRescueCompanyIdWorker

    include LoggableJob
    include Sidekiq::Worker
    include AsyncRequest::TreatError

    sidekiq_options queue: 'high', retry: false

    NO_STRIPE_CONNECT_ACCOUNT_ERROR = 'No Stripe Connect account exists for this company'
    GENERIC_ERROR = 'Stripe Connect account deletion failed'

    def perform(async_request_id, rescue_company_id)
      @async_request_id  = async_request_id
      @rescue_company_id = rescue_company_id

      # keep track of the request
      log_async_request(
        external_request_body: {
          upstream_account_id: custom_account.upstream_account_id,
        }.to_json
      )

      destroy_account_on_stripe!

      # keep track of the request
      log_async_request(external_response_body: @stripe_response)

      custom_account.update!(deleted_at: Time.current)

      publish_async_request_through_ws
    rescue ActiveRecord::RecordNotFound => e
      treat_error(e, :custom_account, [NO_STRIPE_CONNECT_ACCOUNT_ERROR], async_request.company)
    rescue StandardError => e
      treat_error(e, :custom_account, [GENERIC_ERROR], async_request.company)
    end

    private

    def log_async_request(async_request_attrs)
      async_request.update!(async_request_attrs)
    end

    def async_request
      @async_request ||= API::AsyncRequest.find(@async_request_id)
    end

    def custom_account
      @custom_account ||= CustomAccount.not_deleted.find_by!(company_id: @rescue_company_id)
    end

    def destroy_account_on_stripe!
      @stripe_response = StripeIntegration::AccountDestroyer.call(
        input: { custom_account: custom_account }
      ).output
    end

    def publish_async_request_through_ws
      log :debug, "about to publish the result throught WS"

      Publishers::AsyncRequestWebsocketMessage.call(
        input: {
          custom_account: {
            status: 'success',
          },
        },
        id: @async_request_id
      )
    end

  end
end
