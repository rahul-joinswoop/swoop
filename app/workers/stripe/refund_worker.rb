# frozen_string_literal: true

module Stripe
  class RefundWorker

    include LoggableJob
    include Sidekiq::Worker

    sidekiq_options queue: 'invoice_charge_card', retry: false

    ASYNC_REQUEST_SUCCESS = 'success'
    REFUND_ATTR = 'refund'
    GENERIC_ERROR = 'Refund failed. Please try again.'

    def perform(async_request_id, invoice_id, payment_id)
      @async_request_id = async_request_id
      @invoice_id = invoice_id
      @payment_id = payment_id

      # keep track of the request
      async_request.update!(external_request_body: stripe_refund_hash.to_json)

      @output = InvoiceService::ChargeProviders::Stripe::RefundService.call({
        input: {
          sender_id: async_request.company_id,
          invoice_id: invoice_id,
          user_id: async_request.user_id,
          payment_id: payment_id,
        },
      }).output

      # keep track of the request
      async_request.update!(external_response_body: @output[:stripe_refund].to_s)

      publish_async_request_through_ws
    rescue InvoiceService::ChargeProviders::Stripe::InvalidBankAccountError,
           InvoiceService::ChargeProviders::Stripe::UserNotAuthorizedError,
           InvoiceService::ChargeProviders::Stripe::PaymentAlreadyRefundedError,
           StandardError => e

      treat_error(e, REFUND_ATTR, [GENERIC_ERROR])
    end

    private

    def async_request
      @async_request ||= API::AsyncRequest.find(@async_request_id)
    end

    def stripe_refund_hash
      @stripe_refund_hash ||= {
        charge: payment_charge.upstream_charge_id,
      }
    end

    def treat_error(exception, attribute, errors)
      log :error, "error occured", invoice: @invoice_id

      log :error, exception.message

      publish_async_request_with_error(attribute, errors)

      raise exception
    end

    def invoice
      @invoice ||= ::Invoice.find_by!(id: @invoice_id, sender_id: async_request.company_id)
    end

    def payment_charge
      payment.charge
    end

    def payment
      @payment ||= invoice.payments.find(@payment_id)
    end

    def publish_async_request_through_ws
      log :debug, "about to publish the send result throught WS", invoice: @invoice_id

      refund_hash = RefundSerializer.new(@output[:refund]).serializable_hash

      Publishers::AsyncRequestWebsocketMessage.call(
        input: {
          refund: refund_hash,
        },
        id: @async_request_id
      )
    end

    def publish_async_request_with_error(attribute, errors)
      log :debug,
          "there was an error on the charge process for the " \
          "async_request(#{@async_request_id}), " \
          "about to publish the error throught WS",
          invoice_id: @invoice_id

      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: @async_request_id,
            error: {
              attribute => errors,
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )
    end

  end
end
