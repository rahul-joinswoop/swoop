# frozen_string_literal: true

module Stripe
  class UpdatePayoutScheduleWorker

    include Sidekiq::Worker
    include AfterCommit::Worker

    sidekiq_options queue: 'high', retry: 5

    def perform(custom_account_id, payout_interval)
      StripeIntegration::PayoutScheduleUpdater.call({
        input: {
          custom_account_id: custom_account_id,
          payout_interval: payout_interval,
        },
      }).output
    end

  end
end
