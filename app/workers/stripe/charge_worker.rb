# frozen_string_literal: true

module Stripe
  class ChargeWorker

    include LoggableJob
    include Sidekiq::Worker
    include InvoiceService::ChargeProviders::Stripe::ChargeHelper

    sidekiq_options queue: 'invoice_charge_card', retry: false

    GENERIC_ERROR = 'Charge failed. Please try again.'
    AMOUNT_CANNOT_EXCEED_BALANCE_ERROR = 'Amount cannot exceed balance'
    BANK_ACCOUNT_NOT_LINKED_ERROR = 'Bank account not linked'
    CARD_ATTR   = 'card'
    AMOUNT_ATTR = 'amount'
    CHARGE_ATTR = 'charge'

    ASYNC_REQUEST_SUCCESS = 'success'

    EXPIRED_CARD_ERROR = 'expired_card'
    FRAUDULENT_CARD_ERROR = 'fraudulent'
    INCORRECT_NUMBER = 'incorrect_number'
    INCORRECT_CVC = 'incorrect_cvc'
    INCORRECT_ZIP = 'incorrect_zip'
    INSUFFICIENT_FUNDS = 'insufficient_funds'
    INVALID_CVC = 'invalid_cvc'
    INVALID_EXPIRY_YEAR = 'invalid_expiry_year'
    INVALID_NUMBER = 'invalid_number'
    PROCESSING_ERROR = 'processing_error'
    REENTER_TRANSACTION = 'reenter_transaction'
    TRY_AGAIN_LATER = 'try_again_later'

    CARD_DECLINED_ERROR_MESSAGES = {
      EXPIRED_CARD_ERROR => 'Card expired',
      FRAUDULENT_CARD_ERROR =>
        'Blocked for fraud risk. Contact Swoop for approval: (866) 219-8136.',
      INCORRECT_NUMBER => 'Incorrect card number',
      INCORRECT_CVC => 'Incorrect CVC',
      INCORRECT_ZIP => 'Incorrect Zip',
      INSUFFICIENT_FUNDS => 'Insufficient funds. Contact card issuer.',
      INVALID_CVC => 'Invalid CVC',
      INVALID_EXPIRY_YEAR => 'Invalid expiration date',
      INVALID_NUMBER => 'Invalid card number',
      PROCESSING_ERROR => 'Processing error. Please try again.',
      REENTER_TRANSACTION => 'Processing error. Please try again.',
      TRY_AGAIN_LATER => 'Processing error. Please try again.',
    }.freeze

    CARD_DECLINED_GENERIC_ERROR_MESSAGE = 'Card declined. Contact card issuer.'

    def perform(async_request_id, invoice_charge_attributes, invoice_id)
      @async_request_id = async_request_id
      @invoice_charge_attributes = invoice_charge_attributes.to_h.deep_symbolize_keys
      @invoice_id = invoice_id

      log :debug,
          "perform triggered for async_request_id #{@async_request_id}",
          invoice_id: @invoice_id

      charge!
    end

    private

    def charge!
      # keep track of the request
      async_request.update!(external_request_body: stripe_charge_hash.to_json)

      charge_service_call = InvoiceService::ChargeProviders::Stripe::ChargeService.call({
        input: {
          sender_id: invoice.sender_id,
          user_id: async_request.user_id,
          invoice_id: invoice.id,
          invoice_charge_attributes: @invoice_charge_attributes,
        },
      })

      if charge_service_call.failure?
        treat_service_failure(charge_service_call.error)
      else
        @output = charge_service_call.output

        # keep track of the response
        async_request.update!(external_response_body: @output[:stripe_charge].to_s)

        # publish the async_request
        publish_async_request_through_ws

        log :debug,
            "charge! - well succeeded!",
            invoice_id: @invoice_id
      end
    end

    def invoice
      @invoice ||= ::Invoice.lock.find_by!(id: @invoice_id, sender_id: async_request.company_id)
    end

    def async_request
      @async_request ||= API::AsyncRequest.find(@async_request_id)
    end

    def treat_service_failure(error)
      case error
      when ::Stripe::CardError
        error_code = stripe_card_error_code_from_exception(error)

        friendly_error_message = friendly_error_message_for(error_code)

        treat_error(CARD_ATTR, [friendly_error_message])
      when InvoiceService::ChargeProviders::Stripe::InvalidAmountError
        treat_error(AMOUNT_ATTR, [error.message])
      when InvoiceService::ChargeProviders::Stripe::InvalidBankAccountError,
           InvoiceService::ChargeProviders::Stripe::UserNotAuthorizedError,
           StandardError
        treat_error(CHARGE_ATTR, [GENERIC_ERROR])
      else
        treat_error(CHARGE_ATTR, [GENERIC_ERROR])
      end
    end

    def friendly_error_message_for(decline_code)
      CARD_DECLINED_ERROR_MESSAGES[decline_code] || CARD_DECLINED_GENERIC_ERROR_MESSAGE
    end

    def treat_error(attribute, errors)
      log :info,
          errors

      publish_async_request_with_error(attribute, errors)
    end

    def publish_async_request_through_ws
      log :debug,
          "about to publish the send result throught WS",
          invoice_id: @invoice_id

      Publishers::AsyncRequestWebsocketMessage.call(
        input: {
          charge: {
            status: ASYNC_REQUEST_SUCCESS,
          },
        },
        id: @async_request_id
      )
    end

    def publish_async_request_with_error(attribute, errors)
      log :debug,
          "there was an error on the charge process for the " \
          "async_request(#{@async_request_id}), " \
          "about to publish the error throught WS",
          invoice_id: @invoice_id

      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: @async_request_id,
            error: {
              attribute => errors,
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )
    end

  end
end
