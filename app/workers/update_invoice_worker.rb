# frozen_string_literal: true

# Updates missing invoice section for swoop jobs. This is a temporary workaround.
# The fix for this will be addressed in https://swoopme.atlassian.net/browse/ENG-11223.
class UpdateInvoiceWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'low', retry: 0

  def perform
    Job.where("created_at > ?", DateTime.now - 1).where(status: ["Completed", "Stored"]).preload(:invoice).find_each do |j|
      unless j.invoice
        begin
          j.update_invoice
        rescue => e
          Rollbar.error(e)
        end
      end
    end
  end

end
