# frozen_string_literal: true

# Update indexes that reference an account so that they can stay in sync with
# changes to the company record.
module SearchIndex
  class SiteDependentIndexUpdateWorker

    include Sidekiq::Worker
    sidekiq_options queue: 'elasticsearch'

    def perform(id)
      site = Site.find_by(id: id)
      return if site.nil?

      RescueProvider.where(site_id: site.id).each do |rescue_provider|
        Indexer.perform_async(:index, RescueProvider.name, rescue_provider.id)
      end
    end

  end
end
