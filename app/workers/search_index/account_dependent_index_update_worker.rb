# frozen_string_literal: true

# Update indexes that reference an account so that they can stay in sync with
# changes to the company record.
module SearchIndex
  class AccountDependentIndexUpdateWorker

    include Sidekiq::Worker
    sidekiq_options queue: 'elasticsearch'

    def perform(id)
      account = Account.find_by(id: id)
      return if account.nil?

      Job.where(account_id: account.id).select(:id).find_each do |job|
        Indexer.perform_async(:index, Job.name, job.id)
      end
    end

  end
end
