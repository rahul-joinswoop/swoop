# frozen_string_literal: true

# Update indexes that reference a company so that they can stay in sync with
# changes to the company record.
module SearchIndex
  class CompanyDependentIndexUpdateWorker

    include Sidekiq::Worker
    sidekiq_options queue: 'elasticsearch'

    def perform(id)
      company = Company.find_by(id: id)
      return if company.nil?

      company.users.each do |user|
        Indexer.perform_async(:index, User.name, user.id)
      end

      RescueProvider.where(company_id: company.id).or(RescueProvider.where(provider_id: company.id)).each do |rescue_provider|
        Indexer.perform_async(:index, RescueProvider.name, rescue_provider.id)
      end

      Job.where(rescue_company_id: company.id).or(Job.where(fleet_company_id: company.id)).select(:id).find_each do |job|
        Indexer.perform_async(:index, Job.name, job.id)
      end
    end

  end
end
