# frozen_string_literal: true

require 'issc'

# Only meant for non production environments to simulate job expiration,
# and to make sure jobs in bad states don't pile up on the front end

class IsscCancelOverdueJobs

  include LoggableJob
  include Utils
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: false

  def initialize
  end

  def enqueue()
  end

  def perform
    if ENV['ISSC_CANCEL_OVERDUE_JOBS']
      jobs = FleetMotorClubJob.find_by_sql <<~SQL
        select * from jobs
        where
          type = 'FleetMotorClubJob' AND (
              (status = 'Assigned' and answer_by < NOW() - INTERVAL '4 minutes')
                OR
              (status = 'Submitted' and updated_at < NOW() - INTERVAL '4 minutes')
            )
      SQL

      if jobs.length > 0
        jobs.each do |job|
          log :info, "IsscCancelOverdueJobs::canceling job #{job.id}", job: job.id
          job.issc_expired
          job.save!
        rescue => e
          log :error, e
          Rollbar.error(e)
        end
      end
    end
  end

  def success()
  end

end
