# frozen_string_literal: true

# PDW Possible other attributes to move to sms_alerts table
#
# t.integer  "get_location_attempts",                   default: 0, null: false
# t.date     "track_technician_sent_at"
# t.boolean  "get_location_sms"
# t.boolean  "partner_sms_track_driver"
# t.boolean  "send_sms_to_pickup"

class SmsAlertWorker

  include LoggableJob
  include Utils
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'low', retry: false

  def perform
    process_confirm_partner_on_site_alert
    process_confirm_partner_on_site_follow_up_alert
    process_confirm_partner_completed_alert
    nil
  end

  private

  # TODO - why not do this first before the other queries? then they load up fewer results
  # also - i don't see any callbacks on SmsAlert so we could potentially turn this into an #update_all and make
  # it much faster?
  def delete_or_cancel_alert_if_necessary(alert)
    job = alert.job
    changed = false
    if !alert.class::VALID_FROM_STATES.include?(job.status)
      log :debug, "SmsAlertWorker::delete_or_cancel_if_necessary - canceling because status:#{job.status}", job: alert.job_id
      alert.canceled_at = Time.current
      changed = true
    elsif job.deleted_at
      log :debug, "SmsAlertWorker::delete_or_cancel_if_necessary - canceling because job deleted", job: alert.job_id
      alert.canceled_at = Time.current
      changed = true
    end

    if changed
      alert.save!
    end

    changed
  end

  def process_confirm_partner_on_site_alert
    # i think the subquery can be improved via one of the techniques here:
    # https://www.periscopedata.com/blog/4-ways-to-join-only-the-first-row-in-sql
    ConfirmPartnerOnSiteSmsAlert
      .eager_load(:job)
      .where(sent_at: nil, canceled_at: nil)
      .where("(SELECT time_of_arrivals.time
               FROM time_of_arrivals
               WHERE time_of_arrivals.job_id=sms_alerts.job_id
               ORDER BY id DESC
               LIMIT 1) < ?", 1.minutes.ago)
      .find_each(batch_size: 100) do |alert|
      log :debug, "SmsAlertWorker::process_confirm_partner_on_site_alert ETA > 1.min overdue", job: alert.job_id
      job = alert.job
      if !delete_or_cancel_alert_if_necessary(alert)
        log :debug, "SmsAlertWorker::process_confirm_partner_on_site_alert - sending ConfirmPartnerOnSite", job: alert.job_id
        alert.audit_sms = Sms::ConfirmPartnerOnSite.send_notification(job)
        alert.sent_at = Time.current
        alert.save!
      end
    end
  end

  def process_confirm_partner_on_site_follow_up_alert
    ConfirmPartnerOnSiteSmsAlert
      .eager_load(:job, :audit_sms)
      .where(canceled_at: nil)
      .where.not(sent_at: nil)
      .joins(:audit_sms)
      .where(audit_sms: { response: nil, followed_up_at: nil })
      .where(AuditSms.arel_table[:created_at].lt(5.minutes.ago))
      .find_each(batch_size: 100) do |alert|
      if !delete_or_cancel_alert_if_necessary(alert)
        log :debug, "SmsAlertWorker::process_confirm_partner_on_site_follow_up_alert - sending ConfirmPartnerOnSiteFollowUp"
        Sms::ConfirmPartnerOnSiteFollowUp.send_notification(alert.job)
        alert.audit_sms.followed_up_at = Time.current
        alert.audit_sms.save!
      end
    end
  end

  def process_confirm_partner_completed_alert
    ConfirmPartnerCompletedSmsAlert
      .eager_load(:job)
      .where(sent_at: nil, canceled_at: nil)
      .where(ConfirmPartnerCompletedSmsAlert.arel_table[:send_at].lt(Time.current))
      .find_each(batch_size: 100) do |alert|
      log :debug, "SmsAlertWorker::process_confirm_partner_completed_alert need to send", job: alert.job_id
      job = alert.job
      if !delete_or_cancel_alert_if_necessary(alert)
        log :debug, "SmsAlertWorker::process_confirm_partner_completed_alert - sending ConfirmPartnerCompleted", job: alert.job_id
        alert.audit_sms = Sms::ConfirmPartnerCompleted.send_notification(job)
        alert.sent_at = Time.current
        alert.save!
      end
    end
  end

end
