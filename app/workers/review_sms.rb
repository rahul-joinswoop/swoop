# frozen_string_literal: true

class ReviewSMS

  include LoggableJob
  include Sidekiq::Worker

  sidekiq_options queue: 'low', retry: 2

  def perform(job_id)
    begin
      job = Job.find(job_id)

      if !(job.review_sms && job.rescue_company)
        log :debug, "Not sending review as review_sms not set", consumer_sms: job.id
      elsif job.review
        log :debug, "Not sending review as review already exists", consumer_sms: job.id
      elsif job.fleet_company.is_tesla?
        log :debug, "Review Not sending because Tesla", consumer_sms: job.id
      else
        phone, user = job.sms_phone
        log :debug, "Sending sms review to #{phone}", consumer_sms: job.id
        review_args = { phone: phone, user: user, job: job, company: job.fleet_company }
        review = feature_based_review(job, review_args)
        review.save!
        review.send_review_sms
        job.track(Metric::SENT, "sms_review", 1)
      end
    rescue StandardError => e
      log :error, "ReviewSMS: Exception #{e}"
      log :error, e.backtrace.join("\n")
      Rollbar.error(e)
    end

    log :debug, "ReviewSMS Finished", job: job_id
  end

  def feature_based_review(job, review_args)
    log :debug, "feature based", job: job.id
    if job.fleet_company.has_feature(Feature::REVIEW_4_TEXTS)
      review = NpsReview.new(review_args)
    elsif job.fleet_company.has_feature(Feature::REVIEW_CHOOSE)
      review = NpsChooseReview.new(review_args)
    elsif job.fleet_company.has_feature(Feature::REVIEW_NPS_SURVEYS)
      review = create_survey(job, review_args)
    elsif job.fleet_company.has_feature(Feature::REVIEW_PURE_NPS)
      review = NpsPureReview.new(review_args)
    else
      review = Review.new(review_args)
    end
    review
  end

  def create_survey(job, review_args)
    review = Reviews::SurveyReview.new(review_args)
    review.survey = Survey.where(
      company: job.fleet_company
    ).last
    review
  end

end
