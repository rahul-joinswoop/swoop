# frozen_string_literal: true

class BidAlertWorker

  include LoggableJob
  include Sidekiq::Worker
  sidekiq_options queue: 'high', retry: 5

  def alert_company(bid)
    log :debug, "Bid::alert_company - Alerting #{bid.company.name}", alerts: bid.job_id
    phone = bid.company.hq&.phone
    if phone
      Job.new_auction_call_if_enabled(bid.company, bid.job_id, phone)
    else
      log :warn, "Bid::alert_company - no phone, unable to alert #{bid.company.name}", alerts: bid.job_id
    end
  end

  def perform(bid_id)
    bid = Auction::Bid.find(bid_id)
    log :debug, "Alerting Bid for #{bid.company.name}", alerts: bid.job_id
    if bid.notifiable?
      alert_company(bid)
    else
      log :debug, "Bid::alert_company - not Alerting #{bid.company.name}. Failed one of: live, bid requested or not ending soon", alerts: bid.job_id
    end
    if bid.status == Auction::Bid::NEW
      bid.status = Auction::Bid::REQUESTED
      bid.save!
    end
  end

end
