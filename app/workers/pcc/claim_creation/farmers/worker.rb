# frozen_string_literal: true

# PCC::ClaimCreation::Farmers::Worker.new.perform(<job_id>)
module PCC
  module ClaimCreation
    module Farmers
      class Worker

        include LoggableJob
        include Sidekiq::Worker

        # Farmers has 8 hours downtime every Sunday, according to Agero Team.
        # We need to ensure that the Claim Creation will retry after 8 hours when this occurs.
        #
        # Following the Sidekiq formula will result in 11 as the retry count:
        #
        #   (retry_count ** 4) + 15 + (rand(30) * (retry_count + 1))
        #
        #  10 will give us 7h  total (less than 8h)
        #  11 will give us 11h total (covers 8h, bingo)
        #  12 will give us 17h total (too much)
        #
        # Use this to calc (result is in secs):

        # def recalc(retry_count)
        #   total_hours = 0
        #
        #   (0..retry_count).each do |rc|
        #     total_hours += (rc ** 4) + 15 + (rand(30) * (rc + 1))
        #   end
        #
        #   total_hours
        # end
        sidekiq_options queue: 'pcc', retry: 11

        def perform(job_id)
          job = Job.find(job_id)

          return if job.pcc_claim

          if job.service_location&.zip.blank?
            raise ArgumentError, LoggableJob.log_msg(" - PCC::ClaimCreation::Farmers::Worker - can't create a Claim as the job.service_location has no zip code yet", job: job.id)
          end

          ctx = Organizer.call(input: {
            job_id: job_id,
          })

          if ctx.failure?
            log :debug, "PCC::ClaimCreation::Farmers::Worker - PCC::Coverage not created #{ctx.output.id}"

            raise RuntimeError
          else
            log :debug, "PCC::ClaimCreation::Farmers::Worker - PCC::Coverage successfully created #{ctx.output.id}"
          end
        end

      end
    end
  end
end
