# frozen_string_literal: true

# PCC::CoverageDetails::Farmers::Worker.new.perform(async_request_id, policy_id, pcc_vehicle_id, pcc_driver_id)
module PCC
  module CoverageDetails
    module Farmers
      class Worker

        include LoggableJob
        include Sidekiq::Worker

        sidekiq_options queue: 'pcc', retry: false

        def perform(async_request_id, policy_id, pcc_vehicle_id, pcc_driver_id)
          request = API::AsyncRequest.find(async_request_id)

          ctx = Organizer.call(input: {
            policy_id: policy_id,
            pcc_vehicle_id: pcc_vehicle_id,
            pcc_driver_id: pcc_driver_id,
          }, company: request.company)

          if ctx.failure?
            PCC::Publishers::ErrorWorker.perform_in(1.seconds, async_request_id)
          else
            PCC::Publishers::Websocket.call(
              input: {
                pcc_coverage: ctx.output,
              },
              async_request_id: async_request_id
            )
          end
        rescue StandardError => e
          log :debug, "PCC::CoverageDetails::Farmers::Worker PCC::Policy(#{policy_id}) error - #{e.class.name} #{e.message}"

          PCC::Publishers::ErrorWorker.perform_in(1.seconds, async_request_id)
        end

      end
    end
  end
end
