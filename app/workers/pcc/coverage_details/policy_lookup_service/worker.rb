# frozen_string_literal: true

# PCC::CoverageDetails::PolicyLookupService:Worker.new.perform(async_request_id, policy_id, pcc_vehicle_id, pcc_driver_id)
module PCC
  module CoverageDetails
    module PolicyLookupService
      class Worker

        include LoggableJob
        include Sidekiq::Worker

        sidekiq_options queue: 'pcc', retry: false

        def perform(async_request_id, job_id, policy_id, args)
          request = API::AsyncRequest.find(async_request_id)

          policy = nil
          if policy_id
            policy = PCC::Policy.find(policy_id)
          end

          job = Job.find(job_id)
          log :debug, "called with\n request:#{request},\n args:#{args}", job: job.id

          # request_args:
          # { "policy_uuid"=>"823b8fe3080e910a1b019677565550fe",
          #  "job"=>{"id":1234,
          #          "service"=>{"name"=>"tow"},
          #          "vehicle"=>{"year"=>1980, "odometer"=>55000},
          #          "location"=>{"service_location"=>{"lat"=>34.5, "lng"=>-90.5},
          #                       "dropoff_location"=>{"site_id"=>22}}
          # }

          ctx = Organizer.call(input: {
            job: job,
            policy: policy,
            request_args: args,
          }, company: request.company)

          if ctx.failure?
            PCC::Publishers::Websocket.call(
              input: {
                programs: ctx.error,
              },
              async_request_id: async_request_id
            )
          else
            PCC::Publishers::Websocket.call(
              input: {
                pcc_coverage_id: ctx.output[:pcc_coverage_id],
                pcc_policy_id: ctx.output[:pcc_policy_id],
                pcc_vehicle_id: ctx.output[:pcc_vehicle_id],
                pcc_driver_id: ctx.output[:pcc_driver_id],
                programs: ctx.output[:program_results],
              },
              async_request_id: async_request_id
            )
          end
        end

      end
    end
  end
end
