# frozen_string_literal: true

module PCC
  module Publishers
    class ErrorWorker

      include Sidekiq::Worker
      sidekiq_options queue: 'pcc_error', retry: false

      def perform(async_request_id)
        async_request = API::AsyncRequest.find(async_request_id)

        ::Publishers::GenericWebsocketMessage.call(
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              error: {
                code: 500,
                message: 'AgeroAPI has failed, please see the logs',
              },
              id: async_request_id,
            },
            company: async_request.user.company,
          }
        )
      end

    end
  end
end
