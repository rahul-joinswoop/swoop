# frozen_string_literal: true

# PCC::PolicyDetails::Farmers::Worker.new.perform(<async_request_id>, <policy_id>)
module PCC
  module PolicyDetails
    module Farmers
      class Worker

        include LoggableJob
        include Sidekiq::Worker

        sidekiq_options queue: 'pcc', retry: false

        def perform(async_request_id, policy_id)
          request = API::AsyncRequest.find(async_request_id)

          ctx = Organizer.call(input: { policy_id: policy_id }, company: request.company)

          if ctx.failure?
            PCC::Publishers::ErrorWorker.perform_in(1.seconds, async_request_id)
          else
            PCC::Publishers::Websocket.call(
              input: {
                pcc_drivers_and_cars: ctx.output,
              },
              async_request_id: async_request_id
            )
          end

        rescue StandardError => e
          log :debug, "PCC::PolicyDetails::Farmers::Worker error - #{e.exception.class.name} #{e.message}"

          PCC::Publishers::ErrorWorker.perform_in(1.seconds, async_request_id)
        end

      end
    end
  end
end
