# frozen_string_literal: true

# PCC::PolicyLookup::PolicyLookupService::Worker.new.perform(<async_request_id>,<job_id>,{vin:<last 8>})
module PCC
  module PolicyLookup
    module PolicyLookupService
      class Worker < ::PCC::PolicyLookup::WorkerBase

        def call_organizer(request, job_id, search_params)
          Organizer.call(input: {
            search_params: search_params,
            job_id: job_id,
          }, company: request.company)
        end

      end
    end
  end
end
