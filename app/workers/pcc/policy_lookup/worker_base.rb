# frozen_string_literal: true

module PCC
  module PolicyLookup
    class WorkerBase

      include LoggableJob
      include Sidekiq::Worker

      sidekiq_options queue: 'pcc', retry: false

      def perform(async_request_id, job_id, search_terms)
        search_terms.deep_symbolize_keys!

        log :debug, "PCC::PolicyLookup::WorkerBase called with #{search_terms}"

        request = API::AsyncRequest.find(async_request_id)

        ctx = call_organizer(request, job_id, search_terms)

        if ctx.failure?
          PCC::Publishers::ErrorWorker.perform_in(1.seconds, async_request_id)
        else
          PCC::Publishers::Websocket.call(
            input: {
              policies: ctx.output,
            },
            async_request_id: async_request_id
          )
        end
      rescue StandardError => e
        log :debug, "PCC::PolicyLookup::WorkerBase error - #{e.class.name} #{e.message}"
        log :debug, e.backtrace.join('\n')

        PCC::Publishers::ErrorWorker.perform_in(1.seconds, async_request_id)
      end

    end
  end
end
