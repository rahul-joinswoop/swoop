# frozen_string_literal: true

# PCC::PolicyLookup::Farmers::Worker.new.perform(13,1234567890)
module PCC
  module PolicyLookup
    module Farmers
      class Worker < ::PCC::PolicyLookup::WorkerBase

        def call_organizer(request, job_id, search_params)
          Organizer.call(input:
                           {
                             policy_number: search_params[:policy_number],
                             job_id: job_id,
                           }, company: request.company)
        end

      end
    end
  end
end
