# frozen_string_literal: true

class Indexer

  include LoggableJob
  include Sidekiq::Worker
  sidekiq_options queue: 'elasticsearch', retry: 5

  def perform(operation, class_name, id)
    operation = operation.to_s
    klass = class_name.constantize
    index_name = klass.index_name
    document_type = klass.document_type
    client = klass.__elasticsearch__.client

    if operation == "index"
      record = klass.find_by(id: id)
      if record
        body = record.as_indexed_json
        client.index(index: index_name, type: document_type, id: record.id, body: body)
      else
        log :debug, "#{class_name}.#{id} could not be found"
      end
    elsif operation == "delete"
      client.delete(index: index_name, type: document_type, id: id)
    else
      raise ArgumentError, "Unknown operation #{operation.inspect}"
    end
  end

end
