# frozen_string_literal: true

# This is a periodic task called to handle signing in all RSC issc's
# that should be signed in.
module Agero
  module Rsc
    class LoginWorker

      include Sidekiq::Worker

      # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
      sidekiq_options queue: 'rsc_vendor', retry: false

      def perform
        isscs = Issc.get_login_candidates(Issc::RSC_SYSTEM)
        return if isscs.empty?
        Agero::Rsc::SignInVendorIds.new.perform(isscs.group_by(&:contractorid))
      end

    end
  end
end
