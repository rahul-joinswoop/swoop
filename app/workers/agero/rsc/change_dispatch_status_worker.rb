# frozen_string_literal: true

# This receives a job_id and a rsc_status and pushes the job status to RSC.
module Agero::Rsc
  class ChangeDispatchStatusWorker

    include LoggableJob
    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: 3

    class << self

      def schedule_for(job, job_status_id)
        status_code = Agero::Rsc::Data.job_status_code(job_status_id)
        if status_code
          rsc_status = Agero::Rsc::Status.new(job, code: status_code)
          Agero::Rsc::ChangeDispatchStatusWorker.perform_async(job.id, rsc_status.to_hash)
        end
      end

    end

    def perform(job_id, rsc_status)
      job = Job.not_deleted.find_by(id: job_id)
      return unless job

      rsc_api = Agero::Rsc::API.for_job(job_id)
      dispatch_request_number = job.issc_dispatch_request.dispatchid
      return unless rsc_api && dispatch_request_number

      rsc_api.change_status(dispatch_request_number, status: rsc_status)
    rescue Agero::Rsc::ForbiddenError => e
      handle_forbidden_error(e, job_id)
    rescue Agero::Rsc::API::MissingAccessTokenError => e
      log :warn, "Partner company on job #{job_id} is disconnected from RSC", job: job_id
      Rollbar.warning(e)
    end

    private

    # 403 is when Agero explicitly says it rejects it:
    #
    # https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
    # @see 6.4 Change Status
    #
    # - 403 - Cancellation requested for this job, cannot update it
    #
    # Given the case above, we don't raise the error for 403. All other will raise,
    # so Sidekiq job will retry.
    def handle_forbidden_error(error, job_id)
      log :warn, error.message, job: job_id
      # TODO Need to correct internal state by refreshing from RSC
    end

  end
end
