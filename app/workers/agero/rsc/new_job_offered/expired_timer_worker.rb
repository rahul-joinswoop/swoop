# frozen_string_literal: true

# This worker is called in anticipation of an RSC job being on the verge of expiring.
# Rescue companies can be configured to auto accept or reject jobs at this point.
# Otherwise, we'll pre-emptively mark the job as expired so that we don't wind up
# in a race condition with trying a accept jobs too close to when they expire on the
# system of record.
module Agero
  module Rsc
    module NewJobOffered
      class ExpiredTimerWorker

        include Sidekiq::Worker
        sidekiq_options queue: 'high', retry: 0

        def perform(job_id)
          job = Job.not_deleted.find_by(id: job_id)
          return unless job && job.assigned?

          # Reschedule if the answer_by time has been advanced
          if job.answer_by && job.answer_by > Time.current
            self.class.perform_at(job.answer_by, job_id)
            return
          end

          if auto_reject?(job)
            Agero::Rsc::RejectDispatchWorker.perform_async(job.id)
          elsif auto_accept?(job)
            accept_time = job.account.auto_eta
            job.bid(Time.now + accept_time.minutes)
            job.partner_accepted
            job.save!
            Agero::Rsc::AcceptDispatch.call(job: job)
          else
            job.issc_expired
            job.save!
            TrackEvent.call(event: "RSC Job Expired", target: job)
          end
        end

        private

        def auto_accept_enabled?
          ENV["ISSC_AUTORESPONSE"] == "true"
        end

        def auto_reject?(job)
          job.rescue_company.has_feature?(Feature::MC_JOB_AUTO_REJECT_OVERRIDE) && auto_accept_enabled?
        end

        def auto_accept?(job)
          job.account&.auto_accept? && auto_accept_enabled?
        end

      end
    end
  end
end
