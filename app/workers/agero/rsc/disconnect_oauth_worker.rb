# frozen_string_literal: true

# Worker to fully disconnect an access token from RSC.
module Agero
  module Rsc
    class DisconnectOauthWorker

      include Sidekiq::Worker

      sidekiq_options queue: 'high', retry: 3

      def perform(company_id, vendorid)
        api_access_token = APIAccessToken.find_by(company_id: company_id, vendorid: vendorid, deleted_at: nil)
        return unless api_access_token

        api_access_token.logout!

        agero_account = api_access_token.company.base_company.accounts.find_by(client_company_id: Company.agero_id)
        agero_account.base_publish('update') if agero_account
      end

    end
  end
end
