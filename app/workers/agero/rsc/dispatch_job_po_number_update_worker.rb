# frozen_string_literal: true

module Agero::Rsc
  class DispatchJobPoNumberUpdateWorker

    include Sidekiq::Worker

    sidekiq_options queue: 'low', retry: 2, unique_args: :unique_args

    def perform(job_id)
      job = Job.find_by(id: job_id)
      return unless job && job.po_number.blank?

      result = DispatchJobPoNumberUpdate.call(job: job)

      unless result.success?
        Rails.logger.warn "Warning: #{result.errors}"
      end
    end

  end
end
