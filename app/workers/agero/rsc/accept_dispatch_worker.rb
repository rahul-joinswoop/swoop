# frozen_string_literal: true

# This receives a job_id and pushes the job acceptance to RSC.
module Agero::Rsc
  class AcceptDispatchWorker

    include LoggableJob
    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: 3

    def perform(job_id)
      job = Job.not_deleted.find_by(id: job_id)
      return unless job

      begin
        Agero::Rsc::AcceptDispatch.call(job: job)
      rescue Agero::Rsc::ForbiddenError => e
        handle_forbidden_error(e, job)
      rescue Agero::Rsc::API::MissingAccessTokenError => e
        log :warn, "Partner company on job #{job_id} is disconnected from RSC", job: job_id
        Rollbar.warning(e)
      end
    end

    private

    # 403 is when Agero explicitly says it rejects it:
    #
    # https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
    # @see 5.4 Submit Dispatch Acceptance:
    #
    # - 403 - The job you are tyring to retrive is not allowed by the current user
    #   (should not be the case)
    #
    # - 403 - The job has expired, it cannot be accepted
    #   in this case we expect a notification to expire the job in our side.
    #
    # - 403 - Another dispatcher has already refused/accepted the job
    #   job should be updated by notification to match the refused/accepted, so nothing to do
    #
    # Given the cases above, we don't raise the error for 403. All other will raise,
    # so Sidekiq job will retry.
    def handle_forbidden_error(error, job)
      log :warn, "#{error.message}; expiring job #{job.id}", job: job.id
      job.issc_expired
      job.save!
    end

  end
end
