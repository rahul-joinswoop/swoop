# frozen_string_literal: true

# This receives a job_id, a rsc_status and a rcs_reason and
# pushes the job cancel status and reason to RSC.
module Agero::Rsc
  class CancelDispatchWorker

    include LoggableJob
    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: 3

    class << self

      def schedule_for(job, job_status_id)
        rsc_status_and_reason_hash =
          Agero::Rsc::Data.rsc_status_and_reason_code_hash(
            job.cancel_or_goa_status_reason&.key, job_status_id
          )

        Agero::Rsc::CancelDispatchWorker.perform_async(
          job.id,
          rsc_status_and_reason_hash[:status_code],
          rsc_status_and_reason_hash[:reason_code]
        )
      end

    end

    def perform(job_id, rsc_status, rsc_reason)
      job = Job.not_deleted.find_by(id: job_id)
      return unless job

      rsc_api = Agero::Rsc::API.for_job(job_id)
      dispatch_request_number = job.issc_dispatch_request.dispatchid
      begin
        rsc_api.cancel_dispatch(dispatch_request_number, status: rsc_status, reason: rsc_reason)
      rescue Agero::Rsc::ForbiddenError
        # This is returned if the dispatch was already cancelled by Agero which
        # can easily happen if the cancel was initiated on RSC in first place.
        log :info, "Forbidden response canceling RSC dispatch #{dispatch_request_number} for Job #{job.id}; likely already canceled", job: job.id
      rescue Agero::Rsc::API::MissingAccessTokenError => e
        log :warn, "Partner company on job #{job.id} is disconnected from RSC", job: job.id
        Rollbar.warning(e)
      end
    end

  end
end
