# frozen_string_literal: true

# This will call the services that will create the
# a Job in Swoop from the RSC NewJobOffered notification message.
module Agero::Rsc
  class NewJobOfferedWorker

    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: 3

    def perform(callback_token, event)
      Agero::Rsc::NewJobOffered::Service.call(
        input: { callback_token: callback_token, event: event }
      )
    end

  end
end
