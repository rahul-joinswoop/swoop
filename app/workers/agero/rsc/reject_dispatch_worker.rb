# frozen_string_literal: true

# This receives a job_id and pushes the job rejection to RSC.
module Agero::Rsc
  class RejectDispatchWorker

    include LoggableJob
    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: 3

    def perform(job_id)
      job = Job.not_deleted.find_by(id: job_id)
      return unless job

      rsc_api = Agero::Rsc::API.for_job(job_id)
      dispatch_request_number = job.issc_dispatch_request.dispatchid
      return unless rsc_api && dispatch_request_number

      rsc_api.refuse_dispatch(dispatch_request_number, reason: reject_reason(job))
    rescue Agero::Rsc::ForbiddenError => e
      handle_forbidden_error(e, job_id)
    rescue Agero::Rsc::API::MissingAccessTokenError => e
      log :warn, "Partner company on job #{job_id} is disconnected from RSC", job: job_id
      Rollbar.warning(e)
    end

    private

    # 403 is when Agero explicitly says it rejects it:
    #
    # https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
    # @see 5.5 Submit Dispatch Refusal:
    #
    # - 403 - The resource is not allowed by the the current user
    #   (should not be the case)
    #
    # - 403 - The job has expired, it cannot be accepted
    #   (it's in their docs, though we are currently REJECTING a job...)
    #   in this case we expect a notification to expire the job in our side.
    #
    # - 403 - Another dispatcher has already refused/accepted the job
    #   job should be already updated to match the refused/accepted, so nothing to do
    #
    # - 403 - Error: eDispatch unavailable. Callback requested
    #   If "request callback" is made on new job offer, only agent can accept/refuse.
    #
    # Given the cases above, we don't raise the error for 403. All other will raise,
    # so Sidekiq job will retry.
    def handle_forbidden_error(error, job_id)
      log :warn, error.message, job: job_id
      # TODO Need to correct internal state by refreshing from RSC
    end

    def reject_reason(job)
      log(:warn, "is missing reject_reason_id", job: job.id) unless job.reject_reason_id
      Agero::Rsc::Data.job_refusal_explanation_code(job.reject_reason_id)
    end

  end
end
