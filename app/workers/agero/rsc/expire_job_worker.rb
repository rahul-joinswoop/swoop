# frozen_string_literal: true

# The worker is called when we receive an expire job notification from RSC.
module Agero::Rsc
  class ExpireJobWorker

    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: 3

    def perform(job_id)
      job = Job.not_deleted.find_by(id: job_id)
      return if job.nil? || job.expired?

      job.issc_expired
      job.save!
    end

  end
end
