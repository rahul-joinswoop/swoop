# frozen_string_literal: true

# This will call the service that will move the RSC job to status canceled
module Agero::Rsc
  class AgeroCanceledWorker

    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: 3

    def perform(callback_token, event)
      Agero::Rsc::AgeroCanceled::Service.call(
        input: { callback_token: callback_token, event: event }
      )
    end

  end
end
