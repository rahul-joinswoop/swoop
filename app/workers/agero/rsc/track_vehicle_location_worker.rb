# frozen_string_literal: true

# This class will update a vehicles location with RSC. It is invoked from
# Agero::Rsc::TrackActiveJobLocationsWorker
class Agero::Rsc::TrackVehicleLocationWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'rsc_lbt', retry: false, lock: :until_executed, on_conflict: :replace, unique_args: :unique_args

  class << self

    # Only the latest update for the job will be kept. This is a safety
    # mechanism to protect our systems when degredations occur so that we
    # don't end up with a long backlog of "real time" location updates that
    # cascade or prevent speedy recovery.
    def unique_args(args)
      [args[0]]
    end

  end

  def perform(job_id, driver_id, latitude, longitude, timestamp)
    api = Agero::Rsc::API.for_job(job_id)
    location = { driver_id: driver_id, latitude: latitude, longitude: longitude, timestamp: timestamp.to_time }
    api.update_driver_locations(location)
  end

end
