# frozen_string_literal: true

# This is a periodic task called to handle signing out all RSC issc's
# that should be signed out.
module Agero
  module Rsc
    class LogoutWorker

      include Sidekiq::Worker

      # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
      sidekiq_options queue: 'rsc_vendor', retry: false

      def perform
        isscs = Issc.get_logout_candidates(Issc::RSC_SYSTEM)
        return if isscs.empty?
        Agero::Rsc::SignOutVendorIds.new.perform(isscs.group_by(&:contractorid))
      end

    end
  end
end
