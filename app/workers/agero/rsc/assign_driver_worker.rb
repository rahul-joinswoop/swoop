# frozen_string_literal: true

module Agero
  module Rsc
    class AssignDriverWorker

      include LoggableJob
      include Sidekiq::Worker

      sidekiq_options queue: 'high', retry: 3

      def perform(job_id)
        job = Job.not_deleted.find_by(id: job_id)
        return if job.nil? || job.rescue_driver.nil? || job.done?

        rsc_api = Agero::Rsc::API.for_job(job_id)
        dispatch_request_number = job.issc_dispatch_request.dispatchid
        return unless rsc_api && dispatch_request_number

        begin
          rsc_api.assign_external_driver(
            dispatch_request_number,
            driver: job.rescue_driver,
            vehicle: job.rescue_vehicle,
            site: job.site,
          )
        rescue Agero::Rsc::ForbiddenError => e
          # This should only be the case if the job is now expired or cancelled in RSC but not synced yet to Swoop.
          log :warn, e.message, job: job.id
        rescue Agero::Rsc::API::MissingAccessTokenError => e
          log :warn, "Partner company on job #{job_id} is disconnected from RSC", job: job_id
          Rollbar.warning(e)
        end
      end

    end
  end
end
