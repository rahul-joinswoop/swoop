# frozen_string_literal: true

# This pulls the RSC locations from Agero and attempts to match them with a
# corresponding Swoop site

class Agero::Rsc::PullLocationsWorker

  include LoggableJob
  include Sidekiq::Worker

  sidekiq_options queue: 'high', retry: 3

  def perform(api_access_token_id)
    @api_access_token = APIAccessToken.find(api_access_token_id)
    @company = @api_access_token.company

    api_client = Agero::Rsc::API.new(
      @api_access_token.access_token
    )

    ensure_agero_is_account!

    facilities = api_client.facility_list(@api_access_token.vendorid)

    log :debug, "Agero::Rsc::PullLocationsWorker retrieved facilities: #{facilities}"

    return if facilities.blank?

    facilities["results"].each do |facility|
      ApplicationRecord.transaction do
        issc = Issc.find_live_issc(@api_access_token.vendorid, Issc::AGERO_CLIENTID, facility["id"], "rsc", @company.id)
        update_or_create_issc_from_facility!(issc, facility)
      end
    end

    IsscFixLogouts.perform_async

  ensure
    @company.base_company.accounts.find_by(client_company: agero)
      .base_publish('update')
  end

  private

  def site_already_associated_with_issc_location?(site)
    IsscLocation.where(site: site,
                       fleet_company: agero,
                       deleted_at: nil,
                       company: @company).size > 0
  end

  def update_or_create_issc_from_facility!(issc, facility)
    log :debug, "Agero::Rsc::PullLocationsWorker - update_or_create_issc_from_facility! called with facility:#{facility}"
    if issc
      log :debug, "Agero::Rsc::PullLocationsWorker - Found existing ISSC #{issc.inspect}, attempting to update site"
      update_issc_if_matched_site!(issc, facility) if issc.site.nil?
    else
      create_issc_from_facility!(facility)
    end
  end

  def ensure_agero_is_account!
    agero_exists = @company.base_company.accounts
      .where(client_company: agero).exists?

    return if agero_exists

    @company.base_company.accounts.create!(name: "Agero", client_company: agero)
  end

  def agero
    @agero ||= Company.agero
  end

  def create_issc_from_facility!(facility)
    log :debug, "Agero::Rsc::PullLocationsWorker Creating ISSC for facility: #{facility}"

    mapping = find_or_create_mapping_from_facility!(@company, agero, facility)

    issc_location = IsscLocation.new(
      locationid: facility["id"],
      fleet_company: agero,
      deleted_at: nil,
      company: @company,
      location: mapping.facility,
    )

    log :debug, "Agero::Rsc::PullLocationsWorker using issc_location: #{issc_location.inspect}"

    issc = Issc.new(
      system: :rsc,
      company_id: @company.id,
      contractorid: @api_access_token.vendorid,
      clientid: Issc::AGERO_CLIENTID,
      issc_location: issc_location,
      api_access_token_id: @api_access_token.id,
      status: "Registered"
    )

    matched_site = match_site_to_facility(facility)

    if matched_site
      unless mapping.site
        mapping.site = matched_site
        mapping.save!
      end
      issc.site = matched_site
    end

    issc.save!

    log :debug, "Agero::Rsc::PullLocationsWorker creating Issc #{issc.id} for vendorid: #{@api_access_token.vendorid} and locationid: #{facility["id"]}"
  end

  def update_issc_if_matched_site!(issc, facility)
    matched_site = match_site_to_facility(facility)
    if matched_site
      issc.update!(site: matched_site)
    end
  end

  def match_site_to_facility(facility)
    Agero::Rsc::MatchLocationToSite.call(
      company: @company,
      facility: facility
    ).result
  end

  def find_or_create_mapping_from_facility!(company, fleet_company, facility)
    existing = SiteFacilityMap.where(company: company,
                                     fleet_company: fleet_company,
                                     deleted_at: nil)

    existing.each do |mapping|
      location = mapping.facility
      if location.matches?(latitude: facility["lat"], longitude: facility["lon"])
        return mapping
      end
    end

    loc = Location.create! do |location|
      location.lat = facility["lat"]
      location.lng = facility["lon"]
      location.state = facility["state"]
      location.city = facility["city"]
      location.street = facility["address"]
      location.zip = facility["zipcode"]
    end

    SiteFacilityMap.create!(company: @company, fleet_company: agero, facility: loc)
  end

end
