# frozen_string_literal: true

# This receives a job_id and requests more time on Agero API.
# It will sync the result back to FE through WS message.
module Agero::Rsc
  class RequestMoreTimeWorker

    include LoggableJob
    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: false

    ASYNC_REQUEST_SUCCESS = 'success'
    ASYNC_REQUEST_SUCCESS_MESSAGE = 'More Time Request Accepted by RSC'
    ASYNC_REQUEST_403_ERROR = 'More Time Request Rejected by Agero'
    ASYNC_REQUEST_GENERIC_ERROR = 'More time request failed, please try again'

    def perform(job_id, async_request_id)
      job = Job.not_deleted.find_by(id: job_id)
      return unless job
      async_request = ::API::AsyncRequest.find_by(id: async_request_id)

      rsc_api = Agero::Rsc::API.for_job(job_id)
      dispatch_request_number = job.issc_dispatch_request.dispatchid
      return unless rsc_api && dispatch_request_number

      rsc_api.request_more_time(dispatch_request_number)
      update_job_answer_by(job, async_request)
      publish_async_request_through_ws(job, async_request)
    rescue Agero::Rsc::ForbiddenError => e
      handle_forbidden_error(e, job, async_request)
    rescue Agero::Rsc::API::MissingAccessTokenError => e
      log :warn, "Partner company on job #{job_id} is disconnected from RSC", job: job_id
      Rollbar.warning(e)
    rescue => e
      handle_error(e, job, async_request)
    end

    private

    def update_job_answer_by(job, async_request)
      job.with_lock do
        job.issc_dispatch_request.update!(more_time_requested: true)
        job.update!(answer_by: job.answer_by + 1.minute)
        async_request.update!(external_response_body: ASYNC_REQUEST_SUCCESS_MESSAGE)
      end
    end

    def handle_forbidden_error(error, job, async_request)
      log :warn, error.message, job: job.id

      job.with_lock do
        job.issc_dispatch_request.update!(more_time_requested: true)
        async_request.update!(external_response_body: ASYNC_REQUEST_403_ERROR)
        job.save! # so it pushes the job by WS message
      end

      publish_async_request_with_error(job, async_request, [ASYNC_REQUEST_403_ERROR])
    end

    def handle_error(error, job, async_request)
      log :warn, error.message, job: job.id
      # On unexpected error, we push WS with error but allow the partner to try it again
      job.issc_dispatch_request.update!(more_time_requested: false)
      async_request.update!(external_response_body: "More Time Request Error - #{error.message}")
      publish_async_request_with_error(job, async_request, [ASYNC_REQUEST_GENERIC_ERROR])
    end

    def publish_async_request_through_ws(job, async_request)
      log :debug, "about to publish the AsyncRequest throught WS", job: job.id

      Publishers::AsyncRequestWebsocketMessage.call(
        input: {
          charge: {
            status: ASYNC_REQUEST_SUCCESS,
          },
        },
        id: async_request.id
      )
    end

    def publish_async_request_with_error(job, async_request, errors)
      log :debug, "Failed: about to publish the AsyncRequest throught WS", job: job.id

      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              request_more_time: errors,
            },
          },
          company: Company.find(job.rescue_company.id),
        }
      )
    end

  end
end
