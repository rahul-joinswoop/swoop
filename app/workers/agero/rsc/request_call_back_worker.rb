# frozen_string_literal: true

# This requests a call back to RSC
module Agero::Rsc
  class RequestCallBackWorker

    include LoggableJob
    include Sidekiq::Worker
    include Utils

    sidekiq_options queue: 'high', retry: 3

    def perform(job_id, call_back_hash)
      job = Job.not_deleted.find_by(id: job_id)
      return unless job

      call_back_hash = call_back_hash.with_indifferent_access
      dispatch_request_number = job.issc_dispatch_request.dispatchid

      rsc_api = Agero::Rsc::API.for_job(job_id)

      dispatch_number = call_back_hash[:dispatch_number] || job.site.phone
      formatted_dispatch_number = format_us_phone_number(dispatch_number)

      formatted_alternate_phone = format_us_phone_number(call_back_hash[:secondary_number])

      rsc_api.request_phone_call(
        dispatch_request_number,
        callback_phone_number: formatted_dispatch_number,
        name: job.partner_dispatcher.full_name,
        alternate_phone: formatted_alternate_phone,
      )

      CreateHistoryItem.call(
        job: job,
        target: job,
        user: job.last_touched_by,
        event: HistoryItem::CALL_BACK_REQUESTED,
      )

      job.base_publish("update")
    rescue Agero::Rsc::ForbiddenError => e
      handle_forbidden_error(e, job_id)
    rescue Agero::Rsc::API::MissingAccessTokenError => e
      log :warn, "Partner company on job #{job_id} is disconnected from RSC", job: job_id
      Rollbar.warning(e)
    end

    private

    # 403 is when Agero explicitly says it rejects it:
    #
    # https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
    # @see 5.19 Request Phone Call
    #
    # - 403 - Error: duplicate callback request (nothing to do)
    #
    # - 403 - The job has expired, it cannot be accepted
    #   in this case we expect a notification to expire the job in our side.
    #
    # - 403 - Error: Callback request allowed for New Job offers only (nothing to do)
    #
    # Given the cases above, we don't raise the error for 403. All other will raise,
    # so Sidekiq job will retry.
    def handle_forbidden_error(error, job_id)
      log :warn, error.message, job: job_id
      # TODO Need to correct internal state by refreshing from RSC
    end

  end
end
