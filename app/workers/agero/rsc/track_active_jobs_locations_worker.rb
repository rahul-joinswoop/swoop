# frozen_string_literal: true

# This class will query for and fork a Agero::Rsc::TrackVehicleLocationWorker for
# each job being managed from RSC that requires a location update.
#
# This worker is kicked off from a periodic process rather than from location updates
# because the location updates endpoint is the most frequently called in the system
class Agero::Rsc::TrackActiveJobsLocationsWorker

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'rsc_lbt', retry: false, query_cache: false

  TRACK_JOB_STATUSES = ['En Route', 'On Site', 'Towing', 'Tow Destination'].freeze

  def perform
    job_finder = Job.not_deleted.joins(issc_dispatch_request: { issc: :api_access_token })
      .where(isscs: { system: :rsc })
      .where(status: TRACK_JOB_STATUSES)
      .where.not(rescue_driver_id: nil)
      .where.not(rescue_vehicle_id: nil)
      .where("jobs.updated_at > ?", 24.hours.ago)
      .where(api_access_tokens: { deleted_at: nil })

    job_finder.preload(:rescue_vehicle).find_each do |job|
      vehicle = job.rescue_vehicle
      next if vehicle.nil? || vehicle.location_updated_at.nil? || vehicle.lat.nil? || vehicle.lng.nil?
      Agero::Rsc::TrackVehicleLocationWorker.perform_async(job.id, job.rescue_driver_id, vehicle.lat, vehicle.lng, vehicle.location_updated_at.iso8601)
    end
  end

end
