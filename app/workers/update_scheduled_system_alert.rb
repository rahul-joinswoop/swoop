# frozen_string_literal: true

class UpdateScheduledSystemAlert

  include LoggableJob
  include Sidekiq::Worker

  sidekiq_options queue: 'low', retry: 2

  def perform(job_id)
    job = Job.find(job_id)
    alerts = ScheduledSystemAlert.where(job: job, deleted_at: nil, sent_at: nil)
    if alerts.length == 0
      if job.scheduled_for_over_reminder_time_away
        log :debug, "SCHEDULED_SYSTEM_ALERT creating", job: job_id
        ScheduledSystemAlert.create!(job: job, send_at: job.scheduled_for_reminder_time)
      end
    elsif alerts.length == 1
      alert = alerts.first
      if job.scheduled_for_over_reminder_time_away
        log :debug, "SCHEDULED_SYSTEM_ALERT updating", job: job_id
        alert.update!(send_at: job.scheduled_for_reminder_time)
      else
        log :debug, "SCHEDULED_SYSTEM_ALERT deleting", job: job_id
        alert.update!(deleted_at: Time.now)
      end
    else
      log :error, "SCHEDULED_SYSTEM_ALERT Found more than one ScheduledSystemAlert", job: job_id
    end
  end

end
