# frozen_string_literal: true

class CancelOldJobsWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'low'

  def perform
    unless ENV.key?("CANCEL_OLD_JOBS") && (ENV['SWOOP_ENV'] != 'production')
      return
    end

    Job.where('adjusted_created_at < ?', 24.hours.ago)
      .where.not(status: Job.closed_states)
      .find_each do |job|
        service_klass = job.respond_to?(:swoop_canceled) ? SwoopUpdateJobState : PartnerUpdateJobState

        service_klass.new(job, 'Canceled').execute
      end
  end

end
