# frozen_string_literal: true

class FixAccountlessRescueProviders

  include Sidekiq::Worker
  sidekiq_options queue: 'low', retry: 5

  def perform
    ApplicationRecord.connection.execute <<~SQL
      UPDATE
        rescue_providers
      SET
        deleted_at = NOW()
      WHERE
        id in (
          SELECT
            rescue_providers.id
            FROM
              rescue_providers
              LEFT JOIN accounts ON
                rescue_providers.company_id = accounts.client_company_id
                AND rescue_providers.provider_id = accounts.company_id
              LEFT JOIN companies ON
                rescue_providers.company_id = companies.id
            WHERE
              accounts.deleted_at is not null
              AND rescue_providers.deleted_at is null
              AND companies.in_house is true
          );
    SQL
  end

end
