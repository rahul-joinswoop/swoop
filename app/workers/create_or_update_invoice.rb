# frozen_string_literal: true

# CREATE OR UPDATE AN INVOICE
#
# Compute an Invoice for a Job
#
# Using sidekiq-unique-jobs, we ensure we're only creating or updating 1
# Invoice per Job at any given time
#
# TODO there is a race condition here that results in duplicate invoices.
# Need to add a unique constraint (if partner invoices must be unique),
# and handle duplicate record errors, or otherwise ensure that we are
# creating and using the correct invoice.
class CreateOrUpdateInvoice

  include LoggableJob
  include Sidekiq::Worker

  sidekiq_options queue: 'create_and_update_invoice',
                  retry: false,
                  unique_across_queues: true,
                  lock: :while_executing,
                  unique_args: :unique_args,
                  log_duplicate_payload: true

  def self.unique_args(args)
    [args[0]]
  end

  def perform(job_id)
    Job.transaction do
      # Step 1: Find the job we want to invoice
      job = Job.find_by(id: job_id)
      return unless job

      # Step 2: If no invoice exists for this job, create one
      if !job.invoice && job.account
        JobError.with_job_errors(job, JobError::CREATE_OR_UPDATE_INVOICE) do |job_errors|
          # Create the invoice with status 'created'
          job.invoice = CreateInvoiceForJob.new(job).call
          job.save!
          # Populate the invoice & line items
          UpdateInvoiceForJob.new(job.invoice, job, job.get_rate).call
          # Update the status from 'cretaed' to 'partner_new'
          job.invoice.partner_new
          job.save!
        rescue ArgumentError => e
          raise JobError::JobException.new("CreateOrUpdateInvoice::perform unable to create/update invoice for job_id #{job_id} due to #{e}", true)
        end

      # Step 3: Or, if an invoice exists and needs refreshing, update it instead
      elsif job.invoice&.new_state?
        UpdateInvoiceForJob.new(job.invoice, job, job.get_rate).call
        job.invoice.save!

      # Step 4: And, if no invoice could be created or updated, make a note of it
      else
        log :debug, "did not generate or update an invoice for job_id #{job_id}", job: job_id
      end

      # Step 5: Finally, if, since starting this Sidekiq job, or since generating the foregoing
      # invoice, this job has become cancelled, cancel the invoice as well
      # PDW: this seems wrong, won't it cancel the invoice every time this is called if the job is in canceled state?
      if job.canceled? && job.invoice
        log :debug, "discovered the job was cancelled, and so the the invoice was cancelled for job_id #{job_id}", job: job_id
        InvoiceService::Cancel::CancelPartnerInvoice.new(job).call
        job.save!
      end
    end
  end

end
