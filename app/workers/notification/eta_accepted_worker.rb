# frozen_string_literal: true

class Notification::EtaAcceptedWorker

  include Sidekiq::Worker
  include Notifiable::SendNotification

  sidekiq_options queue: :notification

  def perform(job_id)
    # these are the relationships we need to put our message together
    job = Job.includes(:rescue_company, :service_code, :account).find_by(id: job_id)

    return unless job

    send_push_notification(job)
    send_phone_notification(job)
  end

  private

  def send_push_notification(job)
    user_ids = [*job&.rescue_company&.dispatchers&.select(:id)].reject(&:blank?).map(&:to_ssid).sort
    return if user_ids.blank?

    title = Notifiable::EtaAccepted.build_title(job)
    msg = Notifiable::EtaAccepted.build_msg
    link = Notifiable::EtaAccepted.build_link(job)

    External::Pushwoosh.create_message title: title, msg: msg, link: link, user_ids: user_ids
  end

  def send_phone_notification(job)
    supported_roles = UserNotificationSetting.supported_roles_by(notification_code: :eta_accepted)
    sms_msg = Notifiable::EtaAccepted.build_sms_msg(job)

    filter_users_for(rescue_company: job.rescue_company, supported_roles: supported_roles).find_in_batches(batch_size: 100) do |users|
      send_notification_to(job: job, sms_msg: sms_msg, users: users, job_status_name: :accepted)
    end
  end

end
