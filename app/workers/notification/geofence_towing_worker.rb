# frozen_string_literal: true

class Notification::GeofenceTowingWorker

  include Sidekiq::Worker

  sidekiq_options queue: :notification

  def perform(job_id)
    job = Job.includes(:rescue_company, :account).find(job_id)
    user_ids = [job.rescue_driver].reject(&:blank?).map(&:to_ssid).sort
    return if user_ids.blank?

    title = Notifiable::GeofenceTowing.build_title(job)
    msg = Notifiable::GeofenceTowing.build_msg
    link = Notifiable::GeofenceTowing.build_link(job)

    External::Pushwoosh.create_message title: title, link: link, msg: msg, user_ids: user_ids
  end

end
