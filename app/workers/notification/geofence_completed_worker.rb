# frozen_string_literal: true

class Notification::GeofenceCompletedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :notification

  def perform(job_id, location_departed)
    job = Job.includes(:rescue_company, :account).find(job_id)
    user_ids = [job.rescue_driver].compact.map(&:to_ssid)
    return if user_ids.blank? || location_departed.blank?

    title = Notifiable::GeofenceCompleted.build_title(job)
    msg = Notifiable::GeofenceCompleted.build_msg(location_departed)
    link = Notifiable::GeofenceCompleted.build_link(job)

    External::Pushwoosh.create_message title: title, link: link, msg: msg, user_ids: user_ids
  end

end
