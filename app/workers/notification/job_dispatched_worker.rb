# frozen_string_literal: true

class Notification::JobDispatchedWorker

  include LoggableJob
  include Sidekiq::Worker
  include Notifiable::SendNotification

  sidekiq_options queue: :notification

  def perform(job_id)
    # these are the relationships we need to put our message together
    job = Job.includes(:rescue_company, :account, :service_code).find(job_id)

    send_push_notification(job)
    send_phone_notification(job)
  end

  private

  def send_push_notification(job)
    user_ids = [job.rescue_driver].reject(&:blank?).map(&:to_ssid).sort
    return if user_ids.blank?

    title = Notifiable::JobDispatched.build_title(job)
    msg = Notifiable::JobDispatched.build_msg
    link = Notifiable::JobDispatched.build_link(job)

    External::Pushwoosh.create_message title: title, msg: msg, link: link, user_ids: user_ids
  end

  def send_phone_notification(job)
    if job.rescue_driver
      sms_msg = Notifiable::JobDispatched.build_sms_msg(job)

      send_notification_to(job: job, sms_msg: sms_msg, users: [job.rescue_driver], job_status_name: :dispatched, also_send_to_secondary_phone: true)
    else
      log :debug, "job has no rescue_driver", job: job.id
    end
  end

end
