# frozen_string_literal: true

class Notification::JobReassignedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :notification

  def perform(job_id, user_id)
    # these are the relationships we need to put our message together
    job = Job.includes(:rescue_company, :account).find(job_id)
    user = User.select(:id).find(user_id)
    user_ids = [user].reject(&:blank?).map(&:to_ssid).sort
    # we won't ever get here because the #find above should explode first?
    return if user_ids.blank?

    title = Notifiable::JobReassigned.build_title(job)
    msg = Notifiable::JobReassigned.build_msg
    link = Notifiable::JobReassigned.build_link(job)

    External::Pushwoosh.create_message title: title, msg: msg, link: link, user_ids: user_ids
  end

end
