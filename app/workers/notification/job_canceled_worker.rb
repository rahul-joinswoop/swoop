# frozen_string_literal: true

class Notification::JobCanceledWorker

  include Sidekiq::Worker

  sidekiq_options queue: :notification

  def perform(job_id)
    # these are the relationships we need to put our message together
    job = Job.includes(:rescue_company, :account).find(job_id)

    dispatchers = Array(job.rescue_company&.dispatchers&.select(:id)) - [job.rescue_driver]
    build_and_send_push(job, job.rescue_driver, :driver)
    build_and_send_push(job, dispatchers, :dispatcher)
  end

  private

  def build_and_send_push(job, users, role)
    user_ssids = Array(users).reject(&:blank?).map(&:to_ssid).sort
    return if user_ssids.empty?

    title = Notifiable::JobCanceled.build_title(job)
    msg = Notifiable::JobCanceled.build_msg
    link = Notifiable::JobCanceled.build_link(job, role)

    External::Pushwoosh.create_message(
      title: title, msg: msg, link: link, user_ids: user_ssids
    )
  end

end
