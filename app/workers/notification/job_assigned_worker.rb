# frozen_string_literal: true

# Kicks notification messages related to Job assigned transtion.
class Notification::JobAssignedWorker

  include Sidekiq::Worker
  include Notifiable::SendNotification

  sidekiq_options queue: :high, retry: 5

  def perform(job_id)
    # these are the relationships we need for logic and to put our message together
    job = Job.includes(:account, :service_code, :rescue_company).find_by(id: job_id)

    return unless job
    return unless job.assigned? && job.rescue_company

    supported_roles = UserNotificationSetting.supported_roles_by(notification_code: :new_job)
    sms_msg = Notifiable::JobAssigned.build_sms_msg(job)

    filter_users_for(rescue_company: job.rescue_company, supported_roles: supported_roles).find_in_batches(batch_size: 100) do |users|
      send_notification_to(job: job, sms_msg: sms_msg, users: users, job_status_name: :assigned)
    end
  end

end
