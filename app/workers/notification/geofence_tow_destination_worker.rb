# frozen_string_literal: true

class Notification::GeofenceTowDestinationWorker

  include Sidekiq::Worker

  sidekiq_options queue: :notification

  def perform(job_id)
    job = Job.includes(:rescue_company, :account).find(job_id)
    user_ssids = [job.rescue_driver].compact.map(&:to_ssid).sort
    return if user_ssids.blank?

    title = Notifiable::GeofenceTowDestination.build_title(job)
    msg = Notifiable::GeofenceTowDestination.build_msg
    link = Notifiable::GeofenceTowDestination.build_link(job)

    External::Pushwoosh.create_message title: title, link: link, msg: msg, user_ids: user_ssids
  end

end
