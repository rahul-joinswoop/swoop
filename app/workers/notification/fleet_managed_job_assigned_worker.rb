# frozen_string_literal: true

class Notification::FleetManagedJobAssignedWorker

  include Sidekiq::Worker

  sidekiq_options queue: :notification

  def perform(job_id)
    # these are the relationships we need to put our message together
    job = Job.includes(:rescue_company, :account).find(job_id)
    user_ids = [*job&.rescue_company&.dispatchers&.select(:id)].reject(&:blank?).map(&:to_ssid).sort
    return if user_ids.blank?

    title = Notifiable::FleetManagedJobAssigned.build_title(job)
    msg = Notifiable::FleetManagedJobAssigned.build_msg
    link = Notifiable::FleetManagedJobAssigned.build_link(job)

    External::Pushwoosh.create_message title: title, msg: msg, link: link, user_ids: user_ids
  end

end
