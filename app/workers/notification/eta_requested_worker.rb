# frozen_string_literal: true

class Notification::EtaRequestedWorker

  include LoggableJob
  include Sidekiq::Worker
  include Notifiable::SendNotification

  sidekiq_options queue: :notification

  def perform(job_id = nil, bid_id = nil)
    send_push_notification(job_id, bid_id)
    send_phone_notification(job_id, bid_id)
  end

  def send_push_notification(job_id, bid_id)
    if job_id
      job = Job.includes(:rescue_company, :account).find(job_id)
      user_ids = [*job&.rescue_company&.dispatchers&.select(:id)]
    elsif bid_id
      bid = Auction::Bid.find(bid_id)
      job = Job.includes(:rescue_company, :account).find(bid.job_id)
      if job.auction_live
        user_ids = [*bid&.company&.dispatchers&.select(:id)]
      end
    end

    return if user_ids.blank?

    user_ids = user_ids.reject(&:blank?).map(&:to_ssid).sort

    title = Notifiable::EtaRequested.build_title(job)
    msg = Notifiable::EtaRequested.build_msg
    link = Notifiable::EtaRequested.build_link(job)

    External::Pushwoosh.create_message title: title, msg: msg, link: link, user_ids: user_ids
  end

  def send_phone_notification(job_id, bid_id)
    if job_id
      job = Job.includes(:account, :service_code, :rescue_company).find_by(id: job_id)

      if job
        filter_company_users_and_send_notification(job, job.rescue_company)
      else
        log :debug, "Job not found for id #{job_id}", job: job_id
      end
    elsif bid_id
      bid = Auction::Bid.includes(job: [:account, :service_code, :rescue_company]).new_or_requested.find_by(id: bid_id)

      if bid
        filter_company_users_and_send_notification(bid.job, bid.company)
      else
        log :debug, "Bid not found for id #{bid_id} and status [new, requested]"
      end
    end
  end

  def filter_company_users_and_send_notification(job, rescue_company)
    # build sms_msg
    sms_msg = Notifiable::EtaRequested.build_sms_msg(job)

    supported_roles = UserNotificationSetting.supported_roles_by(notification_code: :new_job)

    # filter company users
    filter_users_for(rescue_company: rescue_company, supported_roles: supported_roles).find_in_batches(batch_size: 100) do |users|
      # send notification for each user
      send_notification_to(job: job, sms_msg: sms_msg, users: users, job_status_name: :auto_assigning)
    end
  end

end
