# frozen_string_literal: true

class JobStatusNotifications

  include LoggableJob
  include Utils
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'low', retry: false

  def perform
    # First check all the jobs that are dispatched
    five_min_from_eta

    # Alert users with all jobs that are scheduled if it's ninety minutes to the job's scheduled time
    JobAlerts::NinetyMinutesFromSchedule.new.call
  end

  private

  def five_min_from_eta
    jobs = Job.where(status: %w(Dispatched Assigned), type: 'FleetManagedJob').includes(:time_of_arrivals)
    jobs.each do |job|
      toa = job.time_of_arrivals.non_live.last # Latest ETA thats not live
      next if !toa

      eta = toa.minutes_until(Time.now)
      next if !eta.between?(4, 5)

      send_to_root_users job, "ETA Approaching (#{eta}min) - #{job.swoop_summary_name}"
    end
  end

  def send_to_root_users(job, notif_text)
    calc = SystemEmailRecipientCalculator.new(job)
    users = calc.calculate
    users.each do |user|
      Job.delay.send_sms(user.phone,
                         notif_text, {
                           user_id: user.id,
                           job_id: job.id,
                           type: 'JobAuditSms',
                         })
    end
  end

end
