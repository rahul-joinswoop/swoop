# frozen_string_literal: true

# Triggers GeojsonDataIngestor for every downloaded GEOJSON data file.
class GeojsonDataIngestorWorker

  include Sidekiq::Worker

  sidekiq_options queue: 'territoryimport', retry: 0

  def perform(geojson_data_file_obj)
    ZipCodes::IngestGeojsonData.new(geojson_data_file_obj).call
  end

end
