# frozen_string_literal: true

require 'issc'

class IsscFixDeleted

  include Sidekiq::Worker
  include AfterCommit::Worker

  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform
    if !@issccon.connected?
      return
    end

    # This deregsiteres all deleted isscs

    isscs = Issc.find_by_sql(
      <<~SQL
        select isscs.* from isscs
        where
          isscs.status in ('LoggedIn', 'LoggingIn','Registered','Registering') and (
          deleted_at IS NOT NULL
            or
          delete_pending_at IS NOT NULL
          )
          and system is null
      SQL
    )

    if isscs.length > 0
      isscs.each do |issc|
        Issc.transaction do
          IsscDeregister.new(issc).call
          Rails.logger.debug("Done with issc")
        end
      end
    end
  end

end
