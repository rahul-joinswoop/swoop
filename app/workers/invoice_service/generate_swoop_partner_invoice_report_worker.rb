# frozen_string_literal: true

# # #
# This worker is primarily called by InvoiceService::SwoopDownloadAll.
#
# It will generate a report with the given invoice ids.
# Then move the invoices.state to swoop_downloaded_partner.
#
# If well succeeded, it will push 2 WS messages with the async_request as its root class:
#   - one containing all invoices, so that UI will know that they got moved to
#     swoop_downloaded_partner
#   - the other containing the report_result generated, so UI can kick the file download straight
#     from Amazon.
#
# If error:
#  - it will move invoices.state back to swoop_approved_partner
#  - will kick one WS message containing all invoices, so that UI will know that they got moved back
#    to swoop_approved_partner
module InvoiceService
  class GenerateSwoopPartnerInvoiceReportWorker < SwoopReportWorker

    INVOICE_STATE_ON_SUCCESS = Invoice::SWOOP_DOWNLOADED_PARTNER
    INVOICE_STATE_ON_FAIL = Invoice::SWOOP_APPROVED_PARTNER

    protected

    def find_swoop_report
      Report.swoop_partner_invoice
    end

    def specific_report_params
      @report_params
    end

  end
end
