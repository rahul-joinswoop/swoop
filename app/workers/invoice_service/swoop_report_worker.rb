# frozen_string_literal: true

# Super class used by GenerateSwoopPartnerInvoiceReportWorker and
# GenerateSwoopFleetInvoiceReportWorker.
#
# See more details on these ^ subclasses.
module InvoiceService

  class ReportResultFailed < StandardError; end

  class SwoopReportWorker

    include LoggableJob
    include Sidekiq::Worker

    sidekiq_options queue: 'high', retry: false

    def perform(report_params, api_user_id, invoice_ids, async_request_id)
      @report_params     = report_params
      @api_user_id       = api_user_id
      @invoice_ids       = invoice_ids
      @async_request_id  = async_request_id

      begin
        @report = find_swoop_report
        @company_report_result = generate_report

        change_invoices_state_on_success!

        log :debug, "finished executing successfully"

        # publish results
        publish_partial_invoices_through_ws
        publish_report_result_through_ws
      rescue StandardError => e
        change_invoices_state_on_fail!
        publish_partial_invoices_with_error_through_ws
        publish_report_result_error_through_ws

        raise e
      end
    end

    protected

    def find_swoop_report
      raise NotImplementedError("Subclasses must implement this")
    end

    def specific_report_params
      raise NotImplementedError("Subclasses must implement this")
    end

    private

    def company_report
      @company_report ||= CompanyReport.new(company_id: Company.swoop_id, report: @report)
    end

    # This will generate the report synchronously
    def generate_report
      company_report_result = company_report.run(
        api_user, specific_report_params, nil, nil, nil, true
      )

      log :debug,
          "ReportResult(#{company_report_result}) " \
          "finished generating report, file url: #{company_report_result.s3_filename}, " \
          "result is #{company_report_result.state}"

      unless company_report_result.state == ReportResult::FINISHED
        raise(
          ReportResultFailed,
          "ReportResult(#{company_report_result}) #{self.class.name}##{__method__} " \
          "report processing has error, result is #{company_report_result.state}"
        )
      end

      company_report_result
    end

    def client_company
      @client_company ||= Company.find(@report_params["client_company_id"])
    end

    def api_user
      @api_user ||= User.find(@api_user_id)
    end

    # # #
    #
    # http://guides.rubyonrails.org/active_record_callbacks.html#skipping-callbacks
    #
    # invoices#update_all will skip callbacks, and that's right here since we don't
    # need any callbacks to be kicked, and mostly we don't want each invoice to be
    # pushed by WS on this (WS update msg is kicked by a callback on Publishable concern).
    #
    def change_invoices_state_on_success!
      log :debug,
          "ReportResult(#{@company_report_result})" \
          "moving invoices to #{Invoice::SWOOP_SENT_FLEET}"

      Invoice.where(id: @invoice_ids).update_all(
        state: self.class::INVOICE_STATE_ON_SUCCESS, updated_at: Time.now
      )
    end

    # This will be called in case any error occurs,
    # so all invoices will be back to swoop_approved_fleet
    def change_invoices_state_on_fail!
      log :debug,
          "about to move invoices back to #{Invoice::SWOOP_APPROVED_FLEET}"

      Invoice.where(id: @invoice_ids).update_all(
        state: self.class::INVOICE_STATE_ON_FAIL, updated_at: Time.now
      )
    end

    def invoices_with_state_array
      Invoice.where(id: @invoice_ids).order(:id).pluck(:id, :state).map do |id, state|
        { id: id, state: state }
      end
    end

    def publish_partial_invoices_through_ws
      log :debug,
          "ReportResult(#{@company_report_result})" \
          "publishing partial invoices"

      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'PartialObjectArray',
          target_data: {
            partial_invoices: invoices_with_state_array,
          },
          company: Company.swoop,
        }
      )
    end

    def publish_partial_invoices_with_error_through_ws
      log :debug,
          "about to publish partial invoices with error throught WS"

      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'PartialObjectArray',
          target_data: {
            partial_invoices: invoices_with_state_array,
            error: {
              message: 'There was a problem with the report generation. ' \
                       'Please try again or call a system admin.',
            },
          },
          company: Company.swoop,
        }
      )
    end

    def publish_report_result_through_ws
      log :debug,
          "ReportResult(#{@company_report_result}) publishing report"

      Publishers::AsyncRequestWebsocketMessage.call(
        input: {
          report_result: ReportResultSerializer.new(@company_report_result).serializable_hash,
        },
        id: @async_request_id
      )
    end

    def publish_report_result_error_through_ws
      log :debug,
          "ReportResult(#{@company_report_result}) " \
          "there was an error on report, about to publish the report result error throught WS"

      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            error: {
              code: 500,
              message: 'There was a problem with the report generation. ' \
                       'Please try again or call a system admin.',
            },
            id: @async_request_id,
          },
          company: Company.swoop,
        }
      )
    end

  end

end
