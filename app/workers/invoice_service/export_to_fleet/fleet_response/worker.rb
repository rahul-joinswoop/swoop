# frozen_string_literal: true

module InvoiceService
  module ExportToFleet
    module FleetResponse
      class Worker

        include LoggableJob
        include Sidekiq::Worker

        sidekiq_options queue: 'send_invoice', retry: false

        GENERIC_ERROR_MESSAGE = 'Error with the export invoice process. Please try again or report to an admin.'

        def perform(swoop_invoice_id, api_async_request_id)
          @swoop_invoice_id = swoop_invoice_id
          @api_async_request_id = api_async_request_id

          # step 1 - Find invoice
          begin
            @swoop_invoice = find_invoice!
          rescue ActiveRecord::RecordNotFound
            log :warn,
                "invoice not found id:#{@swoop_invoice_id}, api_async_request_id:#{@api_async_request_id}. " \
                "The invoice probably updated in another api_async_request_id. Cancelling this job.",
                invoice_id: @swoop_invoice_id

            return
          end

          @api_async_request_changes_hash = {}
          @invoice_changes_hash = {}
          hashes_with_changes = {}

          begin
            # step 2 - Create soap_request_values
            # step 3 - Generate a soap_request and update api_async_request with its values
            # step 4 - POST request Fleet Response endpoint
            # step 5 - Parse soap response and update api_async_request its details
            hashes_with_changes = Organizer.call(input: {
              swoop_invoice: @swoop_invoice,
              invoice_changes_hash: @invoice_changes_hash,
              api_async_request_changes_hash: @api_async_request_changes_hash,
            }).output
          rescue StandardError => e
            log :warn,
                "exception: #{e.message}. Setting state to #{Invoice::SWOOP_SEND_FLEET_ERROR}",
                invoice_id: @swoop_invoice_id

            # alternative step: general failure treatment
            hashes_with_changes[:invoice_changes_hash] = { state_transition: Invoice::SWOOP_SEND_FLEET_ERROR }
            hashes_with_changes[:invoice_changes_hash][:export_error_msg] = GENERIC_ERROR_MESSAGE
          end

          Invoice.transaction do
            # step 6 - refetch the invoice again, under transactional context now
            @swoop_invoice = find_invoice!

            # step 7 - update the invoice and its respective api_async_request
            update_invoice_attributes_with(hashes_with_changes[:invoice_changes_hash])

            # in case general failure occur, this may not be available.
            if hashes_with_changes[:api_async_request_changes_hash].present?
              update_api_async_request_attributes_with(hashes_with_changes[:api_async_request_changes_hash])
            end

            # step 8 - save all
            @swoop_invoice.api_async_request.save!
            @swoop_invoice.save!

            # end of transaction, all will be commited
          end
        end

        private

        def find_invoice!
          @swoop_invoice = Invoice.find_by!(
            id: @swoop_invoice_id,
            sender_id: Company.swoop_id,
            api_async_request_id: @api_async_request_id
          )
        end

        def update_invoice_attributes_with(invoice_changes_hash)
          state_transition = invoice_changes_hash[:state_transition]

          # change invoice status accordingly
          @swoop_invoice.send("#{state_transition}") if @swoop_invoice.send("may_#{state_transition}?")

          log :debug,
              "state changed to #{@swoop_invoice.state}",
              invoice_id: @swoop_invoice_id

          # adds export error msg to invoice in case it exists
          @swoop_invoice.export_error_msg = invoice_changes_hash[:export_error_msg] if invoice_changes_hash[:export_error_msg].present?
        end

        def update_api_async_request_attributes_with(api_async_request_changes_hash)
          @swoop_invoice.api_async_request.assign_attributes(api_async_request_changes_hash)
        end

      end
    end
  end
end
