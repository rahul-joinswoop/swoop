# frozen_string_literal: true

# # #
#
# This async worker will export all given invoice_ids to FleetResponse endpoint.
# It receives the invoice_ids and also the async_request_id that was created to track the whole operation.
#
# For each invoice it will create a new async_request too, but it is just to audit the message exchanged
# between us and FleetResponse; to make it easier to read them and to differ from the original
# async_request, they are called invoice_async_request in this worker.
#
# Invoices will be updated using update_columns to not trigger WS updates for each of them.
# At the end of the process, the worker will send a WS containing all invoice ids and their respective states (and error if any).
#
# It will also push the original async_request through WS so UI will know when it finished.
#
module InvoiceService
  module ExportToFleet
    module FleetResponse
      class ExportAllWorker

        include LoggableJob
        include Sidekiq::Worker

        sidekiq_options queue: 'send_invoice', retry: false

        GENERIC_ERROR_MESSAGE = 'Error with the export invoice process. Please try again or report to an admin.'
        SUCCESS = 'Success'

        def perform(invoice_ids, async_request_id)
          @invoice_ids = invoice_ids

          @async_request_id = async_request_id
          @async_request    = API::AsyncRequest.find(@async_request_id)

          @api_company_id   = @async_request.company_id
          @api_user_id      = @async_request.user_id

          @processed_invoice_ids = []

          begin
            # export invoices and update them on DB
            process_invoices

            # push WS changes to FE
            publish_partial_invoices_through_ws
            publish_export_async_request_through_ws
          rescue StandardError => e
            log :error, e.message

            Rollbar.error(e)

            move_not_processed_invoices_back_to_swoop_approved_fleet!

            # push WS changes to FE
            publish_partial_invoices_through_ws
            publish_export_async_request_error_through_ws
          end
        end

        private

        def process_invoices
          @invoice_ids.each do |invoice_id|
            # Find invoice and create its respective api_request to track the message exchange
            begin
              @swoop_invoice = find_invoice(invoice_id)

              create_audit_async_request_for_invoice!
            rescue ActiveRecord::RecordNotFound
              log :warn,
                  "Swoop invoice not found",
                  invoice_id: invoice_id
              next
            end

            # Export the invoice (using an Organizer), and build hashes with its updates
            hashes_with_changes = export_invoice_and_build_invoice_changes_hash(invoice_id)

            # Update the invoice with the result of the export process; it will either
            # send it to swoop_sent_fleet or back to swoop_approved_fleet in case of error
            update_invoice_with!(invoice_id, hashes_with_changes)
          end
        end

        # Export all invoices (one at a time) and build hashes with their updates
        def export_invoice_and_build_invoice_changes_hash(invoice_id)
          hashes_with_changes = {}

          begin
            # step 1 - Create soap_request_values
            # step 2 - Generate a soap_request and update invoice_async_request with its values
            # step 3 - POST request Fleet Response endpoint
            # step 4 - Parse soap response and update invoice_async_request with its details
            hashes_with_changes = Organizer.call(input: {
              swoop_invoice: @swoop_invoice,
              invoice_changes_hash: {},
              api_async_request_changes_hash: {},
            }).output

            @processed_invoice_ids << invoice_id

            hashes_with_changes
          rescue StandardError => e
            log :error,
                "exception: #{e.message}. Setting state to #{Invoice::SWOOP_SEND_FLEET_ERROR}",
                invoice_id: @swoop_invoice.id

            Rollbar.error(e)

            # alternative step: general failure treatment
            hashes_with_changes[:invoice_changes_hash] = { state_transition: Invoice::SWOOP_SEND_FLEET_ERROR }
            hashes_with_changes[:invoice_changes_hash][:export_error_msg] = GENERIC_ERROR_MESSAGE

            @processed_invoice_ids << invoice_id

            hashes_with_changes
          end
        end

        # Update the invoice with the result of the export process; it will either
        # send it to swoop_sent_fleet or back to swoop_approved_fleet in case of error
        def update_invoice_with!(invoice_id, hashes_with_changes)
          # we rescue errors by only logging because the loop over @invoices_ids must continue

          Invoice.transaction do
            # step 5 - update the invoice and its respective invoice_async_request
            fetch_invoice_and_update_it!(invoice_id, hashes_with_changes)
          end
        rescue StandardError => e
          log :error,
              "error while trying to update_columns of the invoice: #{e.message}, " \
              "invoice.state=#{@swoop_invoice.state} - it will probably stay with this current state and needs to be treated manually!",
              invoice_id: @swoop_invoice.id

          Rollbar.error(e)
        end

        def find_invoice(invoice_id)
          @swoop_invoice = Invoice.find_by!(
            id: invoice_id,
            sender_id: Company.swoop_id
          )
        end

        def create_audit_async_request_for_invoice!
          invoice_async_request = API::AsyncRequest.create!(
            target_id: @swoop_invoice.id,
            company_id: @api_company_id,
            user_id: @api_user_id,
            system: API::AsyncRequest::FLEET_RESPONSE_INVOICE
          )

          @swoop_invoice.update!(api_async_request_id: invoice_async_request.id)

          invoice_async_request
        end

        # This will be called in case any error occurs, so all not processed invoices will be back to swoop_approved_fleet
        def move_not_processed_invoices_back_to_swoop_approved_fleet!
          log :debug, "about to move invoices back to #{Invoice::SWOOP_APPROVED_FLEET}"

          not_processed_ids = @invoice_ids - @processed_invoice_ids

          if not_processed_ids.present?
            Invoice.where(id: not_processed_ids).update_all(state: Invoice::SWOOP_APPROVED_FLEET, updated_at: Time.now)
          end
        end

        def fetch_invoice_and_update_it!(invoice_id, hashes_with_changes)
          log :debug,
              "found, about to update it with #{hashes_with_changes[:invoice_changes_hash]}",
              invoice_id: @swoop_invoice.id

          @swoop_invoice = find_invoice(invoice_id)
          update_invoice_columns!(hashes_with_changes[:invoice_changes_hash])

          # in case general failure occur, invoice_async_request_changes_hash may not be available, so we check it.
          if hashes_with_changes[:api_async_request_changes_hash].present?
            log :debug,
                "about to update its async_request with #{hashes_with_changes[:invoice_changes_hash]}",
                invoice_id: @swoop_invoice.id

            update_invoice_async_request_columns!(hashes_with_changes[:api_async_request_changes_hash])
          end
        end

        def update_invoice_columns!(invoice_changes_hash)
          if invoice_changes_hash[:state_transition] == Invoice::SWOOP_SENT_FLEET
            state = Invoice::SWOOP_SENT_FLEET
          else
            state = Invoice::SWOOP_APPROVED_FLEET
          end

          export_error_msg = invoice_changes_hash[:export_error_msg]

          log :debug,
              "updating state:#{state}, export_error_msg:#{export_error_msg}",
              invoice_id: @swoop_invoice.id

          @swoop_invoice.update_columns(state: state, export_error_msg: export_error_msg)
          log :debug,
              "invoice state changed to #{@swoop_invoice.state}",
              invoice_id: @swoop_invoice.id
        end

        def update_invoice_async_request_columns!(invoice_async_request_changes_hash)
          log :debug,
              "about to do an update. If you don't see a log indicating the update, be very, very scared",
              invoice_id: @swoop_invoice.id

          @swoop_invoice.api_async_request.update_columns(invoice_async_request_changes_hash)
          log :debug,
              "invoice.api_async_request(#{@swoop_invoice.api_async_request.id}) updated",
              invoice_id: @swoop_invoice.id
        end

        def invoices_with_state_array
          invoices = Invoice.where(id: @invoice_ids).order(:id).pluck(:id, :state, :export_error_msg)
          invoices.map do |id, state, export_error_msg|
            { id: id, state: state, export_error_msg: export_error_msg }
          end
        end

        def publish_partial_invoices_through_ws
          Publishers::GenericWebsocketMessage.call(
            input: {
              class_name: 'PartialObjectArray',
              target_data: {
                partial_invoices: invoices_with_state_array,
              },
              company: Company.find(@api_company_id),
            }
          )
        end

        def publish_export_async_request_through_ws
          log :debug, "publishing send result"
          Publishers::AsyncRequestWebsocketMessage.call(
            input: {
              export_all_invoices_result: {
                status: SUCCESS,
              },
            },
            id: @async_request_id
          )
        end

        def publish_export_async_request_error_through_ws
          log :debug, "publishing error on export process for async_request(#{@async_request_id})"

          Publishers::GenericWebsocketMessage.call(
            input: {
              class_name: 'AsyncRequest',
              target_data: {
                error: {
                  code: 500,
                  message: 'There was a problem with the export invoices flow. Please try again or call a system admin.',
                },
                id: @async_request_id,
              },
              company: Company.find(@api_company_id),
            }
          )
        end

      end
    end
  end
end
