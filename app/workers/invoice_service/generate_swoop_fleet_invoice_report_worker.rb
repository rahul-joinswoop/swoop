# frozen_string_literal: true

# # #
# This worker is primarily called by InvoiceService::SwoopDownloadAll.
#
# It will generate a report with the given invoice ids.
# Then move the invoices.state to swoop_sent_fleet.
#
# It treats the case for 21st Century: it uses a specific report called 21st Century Invoice
# (which is based on 21st Century Billing File Report).
# If any other FleetManaged, it uses the default Swoop Invoice Fleet report.
#
# If well succeeded, it will push 2 WS messages with the async_request as its root class:
#   - one containing all invoices, so that UI will know that they got moved to swoop_sent_fleet
#   - the other containing the report_result generated, so UI can kick the file download straight
#     from Amazon.
#
# If error:
#  - it will move invoices.state back to swoop_approved_fleet
#  - will kick one WS message containing all invoices, so that UI will know that they got moved back
#    to swoop_approved_fleet
module InvoiceService
  class GenerateSwoopFleetInvoiceReportWorker < SwoopReportWorker

    include LoggableJob

    INVOICE_STATE_ON_SUCCESS = Invoice::SWOOP_SENT_FLEET
    INVOICE_STATE_ON_FAIL = Invoice::SWOOP_APPROVED_FLEET

    protected

    def find_swoop_report
      if client_company.is_century?
        log :debug,
            "InvoiceService::GenerateSwoopFleetInvoiceReportWorker#find_swoop_fleet_report " \
            "will use 21st Century Billing File report for " \
            "FleetManaged(#{client_company.id}) #{client_company.name}"

        report = Report.century_invoice
      else
        log :debug,
            "InvoiceService::GenerateSwoopFleetInvoiceReportWorker#find_swoop_fleet_report " \
            "will use Swoop Fleet Invoice report for FleetManaged(#{client_company.id}) " \
            "#{client_company.name}"

        report = Report.swoop_fleet_invoice
      end

      log :debug, "InvoiceService::GenerateSwoopFleetInvoiceReportWorker report_id: #{report.id}"

      report
    end

    def specific_report_params
      case client_company.name
      when Company::CENTURY
        @specific_report_params = { invoice_ids: @invoice_ids.join(",") }
      else
        @specific_report_params ||= @report_params.merge(
          client_company_id: client_company.id,
          invoice_ids: @invoice_ids.join(",")
        )
      end

      log :debug, @specific_report_params

      @specific_report_params
    end

  end
end
