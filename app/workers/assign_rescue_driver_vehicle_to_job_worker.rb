# frozen_string_literal: true

class AssignRescueDriverVehicleToJobWorker

  include Sidekiq::Worker
  sidekiq_options queue: 'low'

  def perform(ids)
    ids = Array(ids)
    ids.each do |id|
      AssignRescueDriverVehicleToJob.call job: Job.find(id)
    end
  end

end
