# frozen_string_literal: true

require 'issc'

class IsscFixLogouts

  include Utils
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: false

  def enqueue()
  end

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform
    if !@issccon.connected?
      Rails.logger.debug "IsscFixLogouts not connected, returning"
      return
    end

    if @issccon.logging_in_lock
      Rails.logger.debug "Dropping IsscFixLogouts check because logging_in_lock set"
      return
    end

    @issccon.set_logging_in_lock

    # Possible to miss iscss if they are in transitionary periods, this is called a lot though, so that should take care of it
    isscs = Issc.get_login_candidates(Issc::ISSC_SYSTEM)
    return if isscs.empty?

    ISSC::LoginService.new(isscs).call
  end

end
