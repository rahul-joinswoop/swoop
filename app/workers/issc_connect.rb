# frozen_string_literal: true

require 'issc'

class IsscConnect

  include LoggableJob
  include Sidekiq::Worker
  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def perform
    raise ArgumentError, "Must have ISSC Auth token set using ENV - ISSC_AUTH_TOKEN" unless ENV['ISSC_AUTH_TOKEN']
    raise ArgumentError, "Must have ISSC Webhook URL set using ENV - ISSC_WEBHOOK_URL" unless ENV['ISSC_WEBHOOK_URL']

    webhook = ENV['ISSC_WEBHOOK_URL']

    log :debug, "IsscConnect::perform relogin", issclog: true
    # This will force a re-login
    # NOTE: this should be needed as if we were disconnected all providers should already be in this state
    Issc.transaction do
      isscs = Issc.where(system: nil, status: 'LoggedIn')
      isscs.each do |i|
        i.provider_logout
        i.provider_logged_out
        i.save!
      end
    end
    log :debug, "IsscConnect::perform connect", issclog: true

    # production
    # Count > 7 < 10
    # Retry Interval 60
    response = @issccon.issc_client.connect({
      Webhook: webhook,
      RetryCount: ENV.fetch('ISSC_RETRY_COUNT', 8).to_i,
      RetryInterval: ENV.fetch('ISSC_RETRY_INTERVAL', 60).to_i,
      HeartbeatInterval: ENV.fetch('ISSC_HEARTBEAT_INTERVAL', 60).to_i,
      WebhookBasicAuth: "#{ENV['ISSC_SWOOP_USERNAME']}:#{ENV['ISSC_SWOOP_PASSWORD']}",
    })

    if response.code != 200
      @issccon.set_connection_status ISSC::ConnectionManager::DISCONNECTED
    end
  end

end
