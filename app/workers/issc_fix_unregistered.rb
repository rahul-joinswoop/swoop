# frozen_string_literal: true

require 'issc'

class IsscFixUnregistered

  include Utils
  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'issc', retry: 5

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    @issccon = connection_manager
  end

  def enqueue()
  end

  def perform
    if !@issccon.connected?
      msg = "ISSC NOT CONNECTED"
      Rails.logger.debug(msg)
      return
    end

    # TODO: race condition here if issc becomes disconnected

    # This registers all unregistered ISSC's that aren't deleted

    isscs = Issc.find_by_sql(
      <<~SQL
        SELECT isscs.* FROM isscs
        WHERE
          isscs.status IN ('Unregistered')
            AND
          site_id IS NOT NULL
            AND
          deleted_at IS NULL
      SQL
    )

    # TODO: I feel like this should be moved into IsscRegisterProvider
    if isscs.length > 0

      Rails.logger.debug("Registering amount: #{isscs.length}")
      isscs.each do |issc|
        Rails.logger.debug("Registering #{issc.id}")
        issc.provider_register
        issc.save!
        issc.register
      end
    end
  end

  def success()
  end

end
