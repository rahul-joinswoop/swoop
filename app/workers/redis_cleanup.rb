# frozen_string_literal: true

class RedisCleanup

  include Sidekiq::Worker

  # TODO should include `lock: :until_executed` but there's a bug in the gem https://github.com/mhenrixon/sidekiq-unique-jobs/issues/379
  sidekiq_options queue: 'low', retry: false

  def perform
    cleanup = External::Redis::Cleanup.new(:default)
    cleanup.call
  end

end
