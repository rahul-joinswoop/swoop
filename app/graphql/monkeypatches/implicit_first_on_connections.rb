# frozen_string_literal: true

# monkeypatch for graphql until https://github.com/rmosolgo/graphql-ruby/issues/1109
# is resolved
module ImplicitFirstOnConnections

  module ArrayConnection

    def has_next_page
      if first
        # There are more items after these items
        sliced_nodes.count > first
      elsif max_page_size && !first && !last
        # paginating without a first/last so we pretend we got first: max_page_size
        sliced_nodes.count > max_page_size
      elsif GraphQL::Relay::ConnectionType.bidirectional_pagination && before
        # The original array is longer than the `before` index
        index_from_cursor(before) < nodes.length
      else
        false
      end
    end

    def has_previous_page
      if last
        # There are items preceding the ones in this result
        sliced_nodes.count > last
      elsif max_page_size && after && !first && !last
        index_from_cursor(after) > 0
      elsif GraphQL::Relay::ConnectionType.bidirectional_pagination && after
        # We've paginated into the Array a bit, there are some behind us
        index_from_cursor(after) > 0
      else
        false
      end
    end

  end

  GraphQL::Relay::ArrayConnection.prepend ArrayConnection

  module RelationConnection

    def has_next_page
      if first
        paged_nodes.length >= first && sliced_nodes_count > first
      elsif max_page_size && !first && !last
        # paginating without a first/last so we pretend we got first: max_page_size
        paged_nodes.length >= max_page_size && sliced_nodes_count > max_page_size
      elsif GraphQL::Relay::ConnectionType.bidirectional_pagination && last
        sliced_nodes_count >= last
      else
        false
      end
    end

    def has_previous_page
      if last
        paged_nodes.length >= last && sliced_nodes_count > last
      elsif max_page_size && after && !first && !last
        # paginating without a first/last so we pretend we got first: max_page_size
        offset_from_cursor(after) > 0
      elsif GraphQL::Relay::ConnectionType.bidirectional_pagination && after
        # We've already paginated through the collection a bit,
        # there are nodes behind us
        offset_from_cursor(after) > 0
      else
        false
      end
    end

  end

  GraphQL::Relay::RelationConnection.prepend RelationConnection

  module ColumnValueConnection

    def has_next_page
      if first || (max_page_size && !last)
        next_page_and_nodes
        @has_next_page
      elsif before && GraphQL::Pro::RelationConnection.bidirectional_pagination?
        # Is there an item _after_ this cursor?
        relation = nodes.filter_relation(
          after: @decoded_before,
          allow_eq: true,
        )
        relation.limit(1).length == 1
      else
        false
      end
    end

    def has_previous_page
      if last && !first
        prev_page_and_nodes
        @has_previous_page
      elsif after && GraphQL::Pro::RelationConnection.bidirectional_pagination?
        # Is there an item _before_ this cursor?
        has_item_before_after_cursor?
      else
        # You're on the first page!
        false
      end
    end

  end

  GraphQL::Pro::OrderedRelation::ColumnValueConnection.prepend ColumnValueConnection

end
