# frozen_string_literal: true

# see https://github.com/rmosolgo/graphql-ruby/issues/1596
#
# workarounds so that we don't have to set totalCount on every connection.
# requires two patches since we're (currently) using the dsl- and class-based
# api.

# original workaround, from
# https://github.com/rmosolgo/graphql-ruby/issues/1596#issuecomment-401774706
module ConnectionsWithCount

  # class-based api workaround
  module ClassBasedConnection

    def self.prepended(base)
      base.field :totalCount, GraphQL::Types::Int, 'Total count', null: false

      class << base

        prepend ClassMethods

      end
    end

    module ClassMethods

      # this gets prepended only so we can curry the default value of nodes_field: false
      def edge_type(edge_type_class, edge_class: GraphQL::Relay::Edge, node_type: edge_type_class.node_type, nodes_field: false)
        super
      end

    end

    # these methods are added to correspond with our fields from above
    def total_count
      # object.nodes could be a hash or an AR relation so we use this logic from
      # Graphql::Relay::RelationConnection#relation_count
      count_or_hash = if defined?(ActiveRecord::Relation) && object.nodes.is_a?(ActiveRecord::Relation)
                        object.nodes.count(:all)
                      else # eg, Sequel::Dataset, don't mess up others
                        object.nodes.count
                      end
      count_or_hash.is_a?(Integer) ? count_or_hash : count_or_hash.length
    end

  end

  GraphQL::Types::Relay::BaseConnection.prepend ClassBasedConnection

end
