# frozen_string_literal: true

# workaround for https://github.com/rmosolgo/graphql-ruby/issues/1464
module EdgesFix

  private def build_edges(items, connection)
    items.respond_to?(:map) ? super : items
  end

end

GraphQL::Relay::EdgesInstrumentation::EdgesResolve.prepend EdgesFix
