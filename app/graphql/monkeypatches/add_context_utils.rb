# frozen_string_literal: true

# add our various extensions to the context object
GraphQL::Query::Context.include Utils::Context
