# frozen_string_literal: true

module BidirectionalPagination

  def self.included(base)
    base.bidirectional_pagination = true
  end

end

# enable bidirectional_pagination for GraphQL::Relay::ConnectionType
GraphQL::Relay::ConnectionType.include BidirectionalPagination
