# frozen_string_literal: true

module PublicGlobalIdField

  # this code is identical to https://github.com/rmosolgo/graphql-ruby/blob/7eb62c2fe79d1cc45f58f7f0431571cf6dceea9e/lib/graphql/schema/member/has_fields.rb#L63
  # but adds custom required_roles logic so that ids are always public fields
  def global_id_field(field_name)
    id_resolver = GraphQL::Relay::GlobalIdResolve.new(type: self)
    field field_name, "ID", null: false, required_roles: :any
    define_method(field_name) do
      id_resolver.call(object, {}, context)
    end
  end

end

GraphQL::Schema::Mutation.extend PublicGlobalIdField
GraphQL::Schema::Subscription.extend PublicGlobalIdField
GraphQL::Schema::Object.extend PublicGlobalIdField
GraphQL::Schema::Interface.include PublicGlobalIdField
