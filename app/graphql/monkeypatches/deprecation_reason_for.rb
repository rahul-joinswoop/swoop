# frozen_string_literal: true

module DeprecationReasonFor

  extend ActiveSupport::Concern

  class_methods do
    # TODO - eventually want to support input params too
    # TODO - is there any reason to memoize this? i think the only cost is fully loading
    # the schema, which we probably already have done in production?
    def deprecation_reason_for(path)
      (type, field) = path.split('.')
      reason = types[type]&.fields&.dig(field)&.deprecation_reason
      unless reason
        Rollbar.error "DeprecationReasonFor: couldn't find deprecation_reason for #{path.inspect}"
      end
      reason
    end
  end

end

GraphQL::Schema.include DeprecationReasonFor
