# frozen_string_literal: true

class Mutations::EditJob < Mutations::UpdateJob

  graphql_name "EditJob"

  # Arguments
  argument :job, Types::EditJob::JobInputType, required: true

end
