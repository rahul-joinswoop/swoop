# frozen_string_literal: true

class Mutations::UpdateJobStatus < Mutations::BaseMutation

  include GraphQLConcerns
  include Utils::GraphQLHeaders

  graphql_name "UpdateJobStatus"

  field :job, Types::JobType, null: true

  class UpdateJobStatusBiddedEtaType < Types::BaseInputObject

    argument :bidded, Types::DateTimeType, required: false, required_roles: :rescue do
      description "Bidded ETA for JobStatus Accepted only"
    end

  end

  class UpdateJobStatusNotesType < Types::BaseInputObject

    argument :rejected, String, required: false, required_roles: :rescue do
      description "Additional notes to be used for Rejected status"
    end

  end

  class UpdateJobStatusJobInputType < Types::BaseInputObject

    graphql_name "UpdateJobStatusJobInput"

    argument :id, ID, required: true
    argument :status, Types::JobStatusEnumType, required: true do
      description 'Job Status.'
    end
    argument :eta, UpdateJobStatusBiddedEtaType, required: false, required_roles: :rescue
    # TODO - let's just call this reason?
    # TODO - coerce this here rather than in our analyzer?
    argument :reason, String, required: false, required_roles: :rescue do
      description "Reason, in case required by the Status this Job is changed into"
    end
    argument :notes, UpdateJobStatusNotesType, required: false, required_roles: :rescue

  end

  # Arguments
  argument :job, UpdateJobStatusJobInputType, required: false

  def resolve(args = {})
    args = Utils::VariablesConverter.convert(args)

    params = args.dig(:job).to_h.slice(:id, :status, :eta, :reason, :notes)
    job = Resolvers::Job.new(object: object, context: context).resolve(args)
    return if job.blank?

    # JobStatuses is an EnumType which doesn't allow spaces. we accept snakecase
    # status, ie "en_route", "dispatched", etc but FleetUpdateJobState wants "En
    # Route", "Dispatched", etc, so we have to do a conversion here.
    status = Job.statuses[params[:status]]

    # if we have a draft job and we're going to pending then we have to use
    # the same code path Mutations::UpdateJob follows)
    #
    # TODO - what about predraft?
    if job.draft? && params[:status] == "pending"
      return Resolvers::UpdateJob.new(object: object, context: context).resolve(args)
    # It's actually a job update when 'Accepted', and it will be processed by Job::UpdateByRescueCompany.
    # TODO - we shouldn't need to check for job.eta.bidded here? doesn't our analyzer already do this
    # but removing this breaks specs?
    elsif job.assigned? && params[:status] == "accepted" && params.dig(:eta, :bidded).present?
      ret = Resolvers::UpdateJob.new(object: object, context: context).resolve(args)
      if ret[:job]
        # tried doing this inside of Resolvers::UpdateJob but it required a ton of changes because eventually it hooked
        # into API::Jobs::Edit and we had to change the fingerprint of that method. plus this code path _should_ be
        # inside of EditStatus instead anyway (there's a ticket on this somewhere). so anyway, that's why this is here.
        AnalyticsData::JobStatus.new(
          job: ret[:job],
          # we have to pass this in because it's possible ret[:job].status is actually submitted and not accepted
          status: "Accepted",
          api_user: context[:api_user],
          api_application: context[:api_application],
          user_agent_hash: { platform: swoop_custom_platform, version: client_or_swoop_version },
          referer: referer,
        ).push_message
      end
      return ret
    elsif job.assigned? && params[:status] == "rejected"
      return Resolvers::RejectJob.new(object: object, context: context).resolve(args)
    elsif job.auto_assigning?
      # no need for an else here, we're verifying the statuses are valid already in our analyzer
      if params[:status] == "accepted"
        Auction::SubmitBidService.new(
          job,
          context[:api_company],
          params.dig(:eta, :bidded)
        ).call

        AnalyticsData::JobStatus.new(
          job: job,
          # we have to pass this in because job's status doesn't actually change
          status: "Accepted",
          api_user: context[:api_user],
          api_application: context[:api_application],
          user_agent_hash: { platform: swoop_custom_platform, version: client_or_swoop_version },
          referer: referer,
        ).push_message

      elsif params[:status] == "rejected"
        Auction::RejectBidService.new(
          job,
          context[:api_company],
          JobRejectReason.find_by!(text: params[:reason]).id,
          params.dig(:notes, :rejected)
        ).call

        AnalyticsData::JobStatus.new(
          job: job,
          # we have to pass this in because job's status doesn't actually change
          status: "Rejected",
          api_user: context[:api_user],
          api_application: context[:api_application],
          user_agent_hash: { platform: swoop_custom_platform, version: client_or_swoop_version },
          referer: referer,
        ).push_message

      end
    else
      job_status_reason_type_id = if params[:reason]
                                    ::JobStatusReasonType.find_by!(
                                      text: params[:reason],
                                      job_status_id: JobStatus::NAME_MAP[status.to_sym]
                                    ).id
                                  else
                                    nil
                                  end

      service_klass = context[:api_company].rescue? ? PartnerUpdateJobState : FleetUpdateJobState

      service_klass.new(
        job, status,
        api_user: context[:api_user],
        api_application: context[:api_application],
        user_agent_hash: { platform: swoop_custom_platform, version: client_or_swoop_version },
        referer: referer,
        job_status_reason_type_id: job_status_reason_type_id,
      ).call_with_transaction
      job.update_digital_dispatcher_status if job.save
    end
    # TODO - should this be #save! ? then we could catch this up higher and DRY
    # our error behavior? or do we even need the save call since we've already done it
    # above?
    if job && job.save
      { job: job }
    else
      # TODO - this doesn't work, see ENG-8957
      { errors: job&.errors&.to_a }
    end
  rescue StandardError => e
    Rails.logger.error e
    Rollbar.error e
    if e.is_a?(API::Jobs::UpdateJobStatus::CustomActionError) || e.is_a?(AASM::InvalidTransition) || e.is_a?(ArgumentError)
      raise GraphQL::ExecutionError, Analyzers::Validations::UpdateJobStatus.error_message(context[:api_company], job.human_status, status)
    else
      raise GraphQL::ExecutionError, e.message
    end
  end

end
