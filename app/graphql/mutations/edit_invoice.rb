# frozen_string_literal: true

module Mutations
  class EditInvoice < Mutations::UpdateInvoice
  end
end
