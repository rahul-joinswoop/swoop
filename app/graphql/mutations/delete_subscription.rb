# frozen_string_literal: true

module Mutations
  class DeleteSubscription < Mutations::BaseMutation

    description <<~DESCRIPTION
      Delete Subscription
    DESCRIPTION

    # Return
    field :subscription, Types::WebhookSubscriptionType, null: true

    # Arguments

    class DeleteSubscriptionSubscriptionInputType < Types::BaseInputObject

      argument :id, ID, required: true

    end

    argument :subscription, DeleteSubscriptionSubscriptionInputType, required: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)
      subscription_uuid = args.dig(:subscription).to_h.dig(:id)
      subscription = SomewhatSecureID.load!(subscription_uuid)

      # TODO - should we also send the EOM subscription message
      # (ie, { result: null, more: false}) to any active subscriptions before we delete
      # them?

      # you can only delete your own subscriptions
      if subscription.viewer == context[:viewer]
        SwoopSchema.subscriptions.delete_subscription(subscription.id)
      end
      { subscription: nil }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
