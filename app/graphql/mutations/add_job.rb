# frozen_string_literal: true

class Mutations::AddJob < Mutations::CreateJob

  # Arguments
  argument :job, Types::AddJob::JobInputType, required: false

end
