# frozen_string_literal: true

module Mutations
  class SampleInvoice < Mutations::BaseMutation

    description <<~DESCRIPTION
      Builds a sample invoice for recalculation.
      It does not store the sample. It takes an Invoice as the input
      and triggers a new calc based on values passed. These are the values
      that can trigger a new Invoice calc when changed:
      - invoice.rate_type
      - invoice.class_type
      - invoice.line_items
    DESCRIPTION

    graphql_name "SampleInvoice"

    # Receive
    argument :invoice, Types::InvoiceInputType, required: true

    # Return
    field :invoice, Types::InvoiceType, null: false

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)

      # step 1 - parse all arguments that we care about, and load stuff
      invoice_arg = args.dig(:invoice)

      job_uuid = invoice_arg.dig(:job, :id)
      job = SomewhatSecureID.load! job_uuid

      original_invoice = Invoice.find_by!(
        job_id: job.id,
        sender_id: context[:api_company].id,
        deleted_at: nil
      )

      rate_type = RateType.find_by_friendly_type(invoice_arg.dig(:rate_type, :type))
      class_type = invoice_arg[:class_type]

      line_items_hashes = invoice_arg.dig(:line_items).map do |li|
        db_li = SomewhatSecureID.load li[:id]

        if db_li
          li[:id] = db_li.id
        else
          li[:id] = nil
        end

        li[:rate_id] = SomewhatSecureID.load(li.dig(:rate, :id))&.id
        li.delete :rate

        li[:job_id] = job.id
        li.delete :job

        li
      end

      previous_rate_type = nil
      previous_class_type = nil

      if invoice_arg.dig(:previous_rate_type, :type)
        previous_rate_type = RateType.find_by_friendly_type(invoice_arg.dig(:previous_rate_type, :type))
      end

      if invoice_arg.dig(:previous_class_type)
        previous_class_type = invoice_arg.dig(:previous_class_type)
      end

      edit_invoice_metadata = {
        previous_rate_type: previous_rate_type,
        previous_vehicle_category: previous_class_type,
      }

      # step 2 - build a sample Invoice
      sample_invoice = InvoiceService::BuildSample.new(
        original_invoice: original_invoice,
        rate_type: rate_type,
        vehicle_category: class_type,
        line_items_hashes: line_items_hashes,
        edit_invoice_metadata: edit_invoice_metadata,
      ).call.sample_invoice

      # step 3 - store notes into job.driver_notes, this is a value that does not
      #          require invoice recalc, but user can change it in the UI so we need
      #          to store it temporarily until user hits save.
      sample_invoice.job.driver_notes = invoice_arg.dig(:notes)

      # step 4 - respond back to client
      { invoice: sample_invoice }
    rescue StandardError => e
      Rollbar.error e

      raise GraphQL::ExecutionError, e.message
    end

  end
end
