# frozen_string_literal: true

class Mutations::UpdateJob < Mutations::BaseMutation

  graphql_name "UpdateJob"
  # Return
  field :job, Types::JobType, null: true

  # Arguments
  argument :job, Types::UpdateJob::JobInputType, required: true

  def resolve(**args)
    args = Utils::VariablesConverter.convert(args)
    Resolvers::UpdateJob.new(object: object, context: context).resolve(args)
  rescue StandardError => e
    Rollbar.error e
    raise GraphQL::ExecutionError, e.message
  end

end
