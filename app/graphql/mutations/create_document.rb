# frozen_string_literal: true

module Mutations
  class CreateDocument < Mutations::BaseMutation

    include Utils::GraphQLHeaders

    description <<~DESCRIPTION
      Create Document
    DESCRIPTION

    # Return
    field :document, Types::DocumentType, null: true

    # Arguments
    argument :document, Types::DocumentInputType, required: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)
      document_hash = args.dig(:document).to_h

      if document_hash[:location]
        document_hash[:location_attributes] = document_hash.delete :location
      end

      job_hash = document_hash.delete :job
      job = SomewhatSecureID.load! job_hash[:id]

      document_hash[:job_id] = job.id
      if context[:api_user]
        document_hash[:user_id] = context[:api_user].id
      end

      # this comes in as a json structure, but we store it as a string i guess?
      document_hash[:document] = document_hash[:document].to_json
      document = ::AttachedDocument.create! document_hash
      document.finalize_document

      AnalyticsData::Document.new(
        document: document,
        user_agent_hash: {
          platform: swoop_custom_platform,
          version: client_or_swoop_version,
        },
        referer: referer,
        api_user: context[:api_user],
        api_application: context[:api_application],
      ).push_message

      { document: document }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
