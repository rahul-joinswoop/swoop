# frozen_string_literal: true

module Mutations
  class CreatePartnerVehicle < Mutations::BaseMutation

    description <<~DESCRIPTION
      Create PartnerVehicles.
    DESCRIPTION

    # Return
    field :partner_vehicle, Types::PartnerVehicleType, null: true

    # Arguments
    argument :partner_vehicle, Types::PartnerVehicleInputType, required: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)

      partner_vehicle_hash = args.dig(:partner_vehicle).to_h

      partner_vehicle = ::RescueVehicle.create!(
        partner_vehicle_hash.merge(company: context[:api_company])
      )

      { partner_vehicle: partner_vehicle }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
