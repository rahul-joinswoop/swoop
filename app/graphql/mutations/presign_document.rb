# frozen_string_literal: true

module Mutations
  class PresignDocument < Mutations::BaseMutation

    include API::V1::Shared::Storage

    description <<~DESCRIPTION
      Presign a Document Upload
    DESCRIPTION

    # Return

    field :presignedDocument, Types::PresignedDocumentType, null: true

    def resolve(**args)
      key = SecureRandom.hex
      url = presign_url(key)

      presigned_document = {
        id: key,
        storage: 'cache',
        url: url,
      }

      { presigned_document: presigned_document }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
