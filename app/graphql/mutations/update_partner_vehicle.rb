# frozen_string_literal: true

module Mutations
  class UpdatePartnerVehicle < Mutations::BaseMutation

    description <<~DESCRIPTION
      Update PartnerVehicles.
    DESCRIPTION

    graphql_name "UpdatePartnerVehicle"

    # Return
    field :partner_vehicle, Types::PartnerVehicleType, null: true

    # Arguments
    argument :partner_vehicle, Types::UpdatePartnerVehicle::InputType, required: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)
      partner_vehicle_hash = args.dig(:partner_vehicle).to_h
      partner_vehicle_uuid = partner_vehicle_hash.dig(:id)
      partner_vehicle = SomewhatSecureID.load!(partner_vehicle_uuid)

      # handle location updates as input[:partner_vehicle][:location][:lat|:lng] or input[:partner_vehicle][:lat|:lng]
      location_params = if partner_vehicle_hash.key?(:location)
                          partner_vehicle_hash[:location]
                        else
                          partner_vehicle_hash
                        end.slice(:lat, :lng)

      if (location_params.keys & [:lat, :lng]).present?
        # we do this async to avoid killing the db
        UpdateVehicleLocationWorker.perform_async(
          context[:api_company].id, partner_vehicle.id, location_params[:lat], location_params[:lng], Time.now.utc.iso8601
        )
      end

      # handle driver updates
      driver_params = partner_vehicle_hash.slice(:driver)
      partner_vehicle.update!(driver_params) if driver_params.present?

      { partner_vehicle: partner_vehicle }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
