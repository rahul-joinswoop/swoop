# frozen_string_literal: true

module Mutations
  class DeleteDocument < Mutations::BaseMutation

    description <<~DESCRIPTION
      Delete Document
    DESCRIPTION

    # Return
    field :document, Types::DocumentType, null: true

    # Arguments

    class DeleteDocumentDocumentInputType < Types::BaseInputObject

      argument :id, ID, required: true

    end

    argument :document, DeleteDocumentDocumentInputType, required: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)
      document_uuid = args.dig(:document).to_h.dig(:id)
      document = SomewhatSecureID.load!(document_uuid)

      # for now you can only delete a document if you're in the partner company on the associated job
      if document.job&.rescue_company == context[:api_company]
        document.update! deleted_at: Time.now
      end
      { document: document }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
