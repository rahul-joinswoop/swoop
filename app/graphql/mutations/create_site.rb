# frozen_string_literal: true

module Mutations
  class CreateSite < Mutations::BaseMutation

    include Utils::GraphQLHeaders

    description <<~DESCRIPTION
      Create Site
    DESCRIPTION

    # Return
    field :site, Types::SiteType, null: true

    # Arguments
    argument :site, Types::SiteInputType, required: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)
      site_params = args.dig(:site).to_h
      location_params = site_params.delete :location

      if location_params.present?
        place_id = location_params.delete :google_place_id
        location_params[:place_id] = place_id if place_id.present?
        site_params[:location_attributes] = location_params
      end

      site_service_create = SiteService::Create.new(
        site_params: site_params,
        site_type: context.is_partner? ? ::PartnerSite : ::FleetSite,
        company: context[:api_company],
      )

      site = ApplicationRecord.transaction do
        site_service_create.call.site
      end

      { site: site }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
