# frozen_string_literal: true

module Mutations
  class CreateUser < Mutations::BaseMutation

    include Utils::GraphQLHeaders

    description <<~DESCRIPTION
      Create User
    DESCRIPTION

    # Return
    field :user, Types::UserType, null: true

    # Arguments
    argument :user, Types::UserInputType, required: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)
      user_attributes = args.dig(:user).to_h
        .slice(:name, :phone, :email, :roles)
        .merge(job_status_email_frequency: "none") # not sure why we have to set this as a default but we do all over the place?

      # User actually has a #full_name attribute which we map #name to
      if user_attributes.key?(:name)
        user_attributes[:full_name] = user_attributes.delete :name
      end

      # we have to set a company_id in order to use BuildCompanyUser
      user_attributes[:company_id] = context[:api_company].id
      # a password is required - we don't actually allow a user to set one though, we just
      # user a random string
      user_attributes[:password] = SecureRandom.uuid

      new_roles = user_attributes.delete :roles

      user = Users::BuildCompanyUser.new(
        user_attributes: user_attributes,
        new_roles: new_roles,
        api_user: context[:api_user],
        api_company: context[:api_company],
        api_roles: context[:api_roles]
      ).call.user

      user.save!

      { user: user }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
