# frozen_string_literal: true

module Mutations
  class RequestCallback < Mutations::BaseMutation

    description <<~DESCRIPTION
      Request a callback on a Job
    DESCRIPTION

    graphql_name "RequestCallback"

    # Return
    field :job, Types::JobType, null: true

    # Arguments
    class RequestCallbackJobInputType < Types::BaseInputObject

      argument :id, ID, required: false

    end

    argument :job, RequestCallbackJobInputType, required: true
    argument :dispatch_phone, Types::E164PhoneNumberType, required: false
    argument :secondary_phone, Types::E164PhoneNumberType, required: false

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)
      job = Resolvers::Job.new(object: object, context: context).resolve(args)
      return if job.blank?

      dispatch_number = args.dig(:dispatch_phone) || job.site_or_rescue_company_phone
      secondary_number = args.dig(:secondary_phone)

      DigitalDispatch.strategy_for(
        job_digital_dispatcher: job.digital_dispatcher,
        action: :request_call_back
      ).new(
        job: job,
        api_user: context[:api_user],
        call_back_hash: { dispatch_number: dispatch_number, secondary_number: secondary_number },
      ).call

      { job: job }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
