# frozen_string_literal: true

module Mutations
  class CreateDemoJob < Mutations::BaseMutation

    include Utils::GraphQLHeaders

    description <<~DESCRIPTION
      Create Demo Job
    DESCRIPTION

    # Return
    field :job, Types::JobType, null: true

    def resolve(**args)
      # TODO - add support for client demo jobs?
      # TODO - add support for RSC demo jobs?
      job = API::Jobs::CreateDemoJob.new(rescue_company_id: context[:api_company].id).call.job
      { job: job }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
