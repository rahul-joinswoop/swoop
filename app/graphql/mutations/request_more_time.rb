# frozen_string_literal: true

module Mutations
  class RequestMoreTime < Mutations::BaseMutation

    description <<~DESCRIPTION
      Request more time on a Job
    DESCRIPTION

    graphql_name "RequestMoreTime"

    # Return
    field :job, Types::JobType, null: true

    # Arguments
    class RequestMoreTimeJobInputType < Types::BaseInputObject

      argument :id, ID, required: false

    end

    argument :job, RequestMoreTimeJobInputType, required: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)
      job = Resolvers::Job.new(object: object, context: context).resolve(args)
      return if job.blank?

      DigitalDispatch::RequestMoreTime.new(
        api_company_id: context[:api_company].id,
        api_user_id: context[:api_user].id,
        job_id: job.id,
      ).call

      # this will eventually happen in RequestMoreTimeWorker (and it will be unset if there's
      # an error there but we do it here now so that the fe doesn't think it can request more time again.
      job.issc_dispatch_request.update! more_time_requested: true

      { job: job }
    rescue StandardError => e
      Rollbar.error e
      raise GraphQL::ExecutionError, e.message
    end

  end
end
