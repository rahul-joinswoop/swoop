# frozen_string_literal: true

class Mutations::CreateDraftJob < Mutations::BaseMutation

  include Utils::GraphQLHeaders

  # Return
  field :job, Types::JobType, null: true

  # Arguments
  argument :job, Types::CreateDraftJob::JobInputType, required: false

  def resolve(**args)
    # we add job.status = 'draft' here so that we create a draft job
    args = Utils::VariablesConverter.convert(args).tap do |a|
      a[:job] ||= {}
      a[:job][:status] = "draft"
    end
    Resolvers::CreateJob.new(object: object, context: context).resolve(args)
  rescue StandardError => e
    Rollbar.error e
    raise GraphQL::ExecutionError, e.message
  end

end
