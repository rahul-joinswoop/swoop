# frozen_string_literal: true

# Update PartnerDrivers.
#
# Uses Analyzers::UpdateUserAnalyzer
module Mutations
  class UpdateUser < Mutations::BaseMutation

    description <<~DESCRIPTION
      Update PartnerDrivers.
    DESCRIPTION

    # Arguments

    class UpdateUserVehicleInputType < Types::BaseInputObject

      argument :id, ID, required: false
      argument :name, String, required: false

    end

    class UpdateUserUserInputType < Types::BaseInputObject

      argument :id, ID, required: true
      argument :on_duty, Boolean, required: false
      argument :vehicle, UpdateUserVehicleInputType, required: false

    end

    argument :user, UpdateUserUserInputType, required: true

    # Return
    field :user, Types::UserType, null: true

    def resolve(**args)
      args = Utils::VariablesConverter.convert(args)

      user_hash = args.dig(:user).to_h
      user_uuid = user_hash.dig(:id)
      user_hash.delete(:id)

      user = SomewhatSecureID.load! user_uuid

      vehicle_hash = user_hash.delete(:vehicle).to_h

      if vehicle_hash.present?
        if vehicle_hash.key?(:id)
          # we allow an explicitly blank vehicle id - this means that we're clearing
          # a previous assignment
          vehicle_uuid = vehicle_hash.dig(:id)
          begin
            user_hash[:vehicle_id] = vehicle_uuid ? user.company.vehicles.find(SomewhatSecureID.load(vehicle_uuid).id).id : nil
          rescue ActiveRecord::RecordNotFound
            raise GraphQL::ExecutionError, "Couldn't find PartnerVehicle with id: #{vehicle_uuid.inspect}"
          end
        elsif vehicle_hash.key?(:name)
          vehicle_name = vehicle_hash.dig(:name)
          begin
            user_hash[:vehicle_id] = vehicle_name ? user.company.vehicles.find_by!(name: vehicle_name).id : nil
          rescue ActiveRecord::RecordNotFound
            raise GraphQL::ExecutionError, "Couldn't find PartnerVehicle with name: #{vehicle_name.inspect}"
          end
        end
      end

      user.update! user_hash

      { user: user }
    rescue StandardError => e
      Rollbar.error e
      # TODO - what other errors are we potentially raising here? for now not much but we don't really want to
      # show our clients actual rails-level errors :/
      raise GraphQL::ExecutionError, e.message
    end

  end
end
