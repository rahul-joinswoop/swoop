# frozen_string_literal: true

class Mutations::CreateJob < Mutations::BaseMutation

  include Utils::GraphQLHeaders

  # Return
  field :job, Types::JobType, null: true

  # Arguments
  argument :job, Types::CreateJob::JobInputType, required: false

  def resolve(**args)
    args = Utils::VariablesConverter.convert(args)
    Resolvers::CreateJob.new(object: object, context: context).resolve(args)
  rescue StandardError => e
    Rollbar.error e
    raise GraphQL::ExecutionError, e.message
  end

end
