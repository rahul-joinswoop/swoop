# frozen_string_literal: true

class Mutations::EditJobStatus < Mutations::UpdateJobStatus

  graphql_name "EditJobStatus"

  class EditJobStatusBiddedEtaType < Mutations::UpdateJobStatus::UpdateJobStatusBiddedEtaType; end
  class EditJobStatusNotesType < Mutations::UpdateJobStatus::UpdateJobStatusNotesType; end

  class EditJobStatusJobInputType < Mutations::UpdateJobStatus::UpdateJobStatusJobInputType

    graphql_name "EditJobStatusJobInput"

    argument :eta, EditJobStatusBiddedEtaType, required: false, required_roles: :rescue
    argument :notes, EditJobStatusNotesType, required: false, required_roles: :rescue

  end

  # Arguments
  argument :job, EditJobStatusJobInputType, required: false

end
