# frozen_string_literal: true

module Resolvers
  class Symptoms < MyCompanyOnly

    def resolve(**args)
      case object
      when FleetCompany
        object.symptoms
      else
        SymptomServices::FetchAllStandard.new.call
      end
    end

  end
end
