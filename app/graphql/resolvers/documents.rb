# frozen_string_literal: true

module Resolvers
  class Documents < Base

    def resolve(**args)
      # default where
      where = { deleted_at: nil }

      # add our filter args
      where[:type] = args[:type] if args[:type]
      where[:container] = args[:container] if args[:container]

      if context[:api_company].id == object.rescue_company_id ||
        context[:api_company].id == object.fleet_company_id
        object.attached_documents
      else
        AttachedDocument.none
      end.where(where)
    end

  end
end
