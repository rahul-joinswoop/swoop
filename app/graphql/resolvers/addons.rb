# frozen_string_literal: true

module Resolvers
  class Addons < Resolvers::Base

    def resolve(**args)
      Resolvers::Company.new(object: object, context: context).resolve(args)
        .companies_services
        .joins(:service_code)
        .where(service_codes: { addon: true })
    end

  end
end
