# frozen_string_literal: true

module Resolvers
  module PermissionChecks

    def object_is_api_application
      object == context[:api_application]
    end

    def object_is_api_company
      object == context[:api_company]
    end

    def object_is_api_user
      object == context[:api_user]
    end

    def object_is_api_company_user
      object.is_a?(::User) && (object.company == context[:api_company])
    end

    # if object is a driver on a fleet job then the fleet company should be
    # able to view it.
    def object_is_a_driver_for_api_company_fleet_job
      object.is_a?(::User) && object.jobs.where(fleet_company: context[:api_company]).exists?
    end

  end
end
