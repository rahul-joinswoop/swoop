# frozen_string_literal: true

module Resolvers
  class VehicleModels < Resolvers::Base

    include Utils::Query

    type [Types::VehicleModel], null: false

    def resolve(**args)
      # we need name and id in our order here or graphql pro's cursor code won't
      # do the right thing, see http://graphql-ruby.org/pro/cursors#grouped-relations
      models = object.vehicle_models

      # not doing this as a separate service (yet) because we'd just end up duplicating
      # the code from Resolvers::VehicleMakes in a confusing way
      query = args.dig(:query)
      return ::VehicleModel.none if present_but_blank?(query)

      if query.present?
        models = models.where('vehicle_models.name ILIKE ?', "%#{query}%")
      end

      models.order("vehicle_models.name, vehicle_models.id")
    end

  end
end
