# frozen_string_literal: true

module Resolvers
  class Services < Resolvers::Base

    type [Types::CompanyServiceType], null: false

    def resolve(**args)
      Resolvers::Company.new(object: object, context: context).resolve(args).services
    end

  end
end
