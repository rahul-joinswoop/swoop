# frozen_string_literal: true

module Resolvers
  class VehicleModel < Resolvers::Base

    def resolve(**args)
      models = Resolvers::VehicleModels.new(object: object, context: context).resolve(args)
      if args[:id]
        models.find(args[:id])
      elsif args[:name]
        models.find_by!(name: args[:name])
      end
    end

  end
end
