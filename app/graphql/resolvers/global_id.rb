# frozen_string_literal: true

module Resolvers
  class GlobalId

    def call(global_id, ctx)
      _type_name, item_id = SomewhatSecureID.decode(global_id)
      item_id
    rescue
      # if we blow up here then the id is invalid. we just want to trap the
      # exception here and return nil (just like we do when we rescue
      # ActiveRecord::RecordNotFound
    end

  end
end
