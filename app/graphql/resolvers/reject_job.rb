# frozen_string_literal: true

module Resolvers
  class RejectJob < Resolvers::Base

    include Utils::GraphQLHeaders

    def resolve(**args)
      job = Resolvers::Job.new(object: object, context: context).resolve(args)
      return if job.blank?

      reject_params = {}.tap do |r|
        reason = args.dig(:job, :reason)
        job_status_reason = ::JobStatusReasonType.find_by(text: reason, job_status_id: JobStatus::REJECTED)
        if job_status_reason
          r[:reject_reason_id] = job_status_reason.find_legacy.id
        end
        rejected_notes = args.dig(:job, :notes, :rejected)
        if rejected_notes
          r[:reject_reason_info] = rejected_notes
        end
      end

      API::Jobs::RejectByPartner.new(
        job_id: job.id,
        api_company_id: context[:api_company],
        api_user_id: context[:api_user]&.id,
        reject_params: reject_params,
      ).call.job

      if job.valid? && job.reload
        # ping analytics about this status change
        AnalyticsData::JobStatus.new(
          job: job,
          user_agent_hash: { platform: swoop_custom_platform, version: client_or_swoop_version },
          referer: referer,
          api_user: context[:api_user],
          api_application: context[:api_application],
        ).push_message

        { job: job }
      else
        { errors: job&.errors&.to_a }
      end
    end

  end
end
