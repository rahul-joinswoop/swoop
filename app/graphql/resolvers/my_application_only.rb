# frozen_string_literal: true

module Resolvers
  class MyApplicationOnly < Resolvers::Base

    include Resolvers::Buildable
    include Resolvers::PermissionChecks

    def authorized?(**args)
      super && object_is_api_application
    end

  end
end
