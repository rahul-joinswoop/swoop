# frozen_string_literal: true

module Resolvers
  class Users < MyCompanyOnly

    include Utils::Query

    ALL_ROLES = [Role::DRIVER, Role::DISPATCHER, Role::ADMIN].freeze

    def resolve(**args)
      query = args.dig(:query)

      return User.none if present_but_blank?(query)

      case object
      when ::RescueCompany
        all_or_query_by_name(args, object)
      else
        ::User.none
      end
    end

    def all_or_query_by_name(args, company)
      roles_args_array = args[:roles].to_a.map(&:to_sym)
      roles_filter = roles_args_array.present? ? (roles_args_array & ALL_ROLES) : ALL_ROLES

      if args[:query]
        API::AutoComplete::CompanyUsers.new(args[:query], company_id: company.id, limit: false).call
      else
        company.users.not_deleted
      end.joins(:roles).where(roles: { name: roles_filter }).distinct
    end

  end
end
