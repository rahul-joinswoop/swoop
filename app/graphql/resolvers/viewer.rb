# frozen_string_literal: true

module Resolvers
  class Viewer < Resolvers::Base

    type [Types::ViewerUnionType], null: false

    def resolve(**args)
      context[:viewer]
    end

  end
end
