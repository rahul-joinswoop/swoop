# frozen_string_literal: true

module Resolvers
  module Buildable

    extend ActiveSupport::Concern

    module ClassMethods

      def build(_method_name)
        klass = Class.new(self) do
          cattr_accessor :method_name
          def resolve(**args)
            object.send(method_name)
          end
        end
        klass.method_name = _method_name
        klass
      end

    end

  end
end
