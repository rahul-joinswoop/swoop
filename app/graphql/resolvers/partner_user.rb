# frozen_string_literal: true

module Resolvers
  class PartnerUser < Resolvers::Base

    include Resolvers::Buildable
    include Resolvers::PermissionChecks

    def authorized?(**args)
      super && [
        object.is_a?(::Job),
        object.is_a?(::RescueVehicle),
        object.is_a?(::InventoryItem),
        object_is_api_company,
        object_is_api_user,
        object_is_api_company_user,
        object_is_a_driver_for_api_company_fleet_job,
      ].any?
    end

  end
end
