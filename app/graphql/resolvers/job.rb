# frozen_string_literal: true

module Resolvers
  class Job < Resolvers::Base

    def resolve(**args)
      # TODO - logic is moved to JobType#authorized? method - how do we dedupe
      # that and Resolvers::Job?
      id = args[:id]
      job_id = args.dig(:job, :id)
      if job_id
        # called from mutations with {job: { id:  NNNN } } - here we fail with an
        # error
        begin
          SomewhatSecureID.load!(job_id)
        rescue SomewhatSecureID::Error, ActiveRecord::RecordNotFound
          context.add_error GraphQL::ExecutionError.new("invalid Job #{job_id.inspect}")
        end
      elsif id
        # called from node() or job()
        ::Job.find_by(id: id)
      else
        nil
      end
    end

  end
end
