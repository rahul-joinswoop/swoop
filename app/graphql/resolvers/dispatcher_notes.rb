# frozen_string_literal: true

module Resolvers
  class DispatcherNotes < Base

    def authorized?(**args)
      super && context.is_partner? && context.has_any_role?(:admin, :dispatcher)
    end

    def resolve(**args)
      object.dispatch_notes
    end

  end
end
