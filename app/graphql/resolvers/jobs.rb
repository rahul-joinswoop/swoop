# frozen_string_literal: true

module Resolvers
  class Jobs < MyselfOrMyCompanyOnly

    def resolve(**args)
      # see also JobType
      # just an initial stab at how we can do authorization.
      # TODO - we could do something like this but RescueDriver isn't actually a
      # thing. i think we need to check for who our api_user is and based on that
      # we can filter on Job#user / Job#driver.user / Job#rescue_driver. so what
      # are those all exactly?
      # if ctx[:api_user].is_a? RescueDriver
      #   return jobs.where(rescue_driver: ctx[:api_user])
      # end
      # TODO - any other company types here?
      company = context[:api_company]

      # default where
      where = { deleted_at: nil }

      # array of wheres to apply later
      wheres = [where]

      results = case company
                when ::FleetCompany
                  if context.is_super_client_managed?
                    # return all fleet jobs. TODO is this right?
                    ::FleetJob.all
                  else
                    company.fleet_jobs
                  end
                when ::RescueCompany
                  # if we're querying user.jobs off of a user then limit the scope of jobs to those that
                  # the user is a driver of
                  if object.is_a?(User)
                    if object == context[:api_user] || context.has_any_role?(:admin, :dispatcher)
                      where[:rescue_driver_id] = object.id
                    else
                      return ::Job.none
                    end
                  end

                  # ***********************************************************************
                  # This logic should be kept in sync or more restrictive than .authorized?
                  # logic in JobType so that node are not nullified later in the request
                  displayable_bid_jobs = ::Job.displayable_bids_for_partners(company)
                  recently_bid_jobs = ::Jobs::RecentlyBid.call(context, displayable_bid_jobs)
                  company.rescue_jobs.union(recently_bid_jobs)
                  # ***********************************************************************
                when ::SuperCompany
                  # there are currently two SuperCompanies
                  return ::Job.none unless company.is_swoop?
                  if context.has_role?(:root)
                    ::Job.all
                  else
                    # swoop dispatcher
                    where[:type] = 'FleetManagedJob'
                    company.fleet_jobs
                  end
                else
                  ::Job.none
                end

      # parse our args and setup our queries / filters
      filters = args.dig(:filters) || {}

      # handle states from :filters and the deprecated root :states argument
      states = args.dig(:states) || filters.dig(:states)
      case states
      when :active_states
        where[:status] = ::Job.active_states
      when :draft_states
        where[:status] = ::Job.draft_states
      when :done_states
        where[:status] = ::Job.done_states
      when :map_states
        where[:status] = ::Job.map_states
      when :all_states
        # noop since it's cleaner not to specify any job_states than for us to
        # specify all of them
      end

      # build / handle our where clauses

      # handle created_at from/to, see https://stackoverflow.com/a/35186805
      # TODO - break this out into a util somewhere as soon as we
      # filter something else by date range
      created_at_range = filters.dig(:created_at) || {}
      created_at_from = created_at_range.dig(:from)
      created_at_to = created_at_range.dig(:to)

      created_at = ::Job.arel_table[:created_at]

      if created_at_from
        wheres << created_at.gteq(created_at_from)
      end

      if created_at_to
        wheres << created_at.lteq(created_at_to)
      end

      # handle client_company.id
      client = filters.dig(:client) || {}
      client_company_id = client.dig(:company, :id)
      if client_company_id
        client_company = SomewhatSecureID.load(client_company_id)
        if client_company
          where[:fleet_company_id] = client_company.id
        else
          # if a bogus client_company id was provided just return an empty list
          return ::Job.none
        end
      end

      # apply all of our where conditions
      results = wheres.reduce(results, :where)

      # build / apply our order
      order = if states == :done_states
                # TODO - why don't we use :id here as well?
                { last_status_changed_at: :desc }
              else
                [{ adjusted_created_at: :desc }, :id]
              end

      results.order(order)
    end

  end
end
