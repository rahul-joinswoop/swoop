# frozen_string_literal: true

module Resolvers
  class FieldLabels < Base

    ENTERPRISE = {
      poNumber: 'Branch/RA #',
      refNumber: 'Claim Number',
      paymentType: 'Claim Type',
      customer: {
        name: 'Branch',
        phone: 'Branch Contact Phone',
      },
    }.freeze

    ENTERPRISE_FLEET_MANAGEMENT = {
      refNumber: 'Claim Number/RO',
    }.freeze

    TURO = {
      refNumber: 'Reservation',
    }.freeze

    def resolve(**args)
      if object.fleet_company.is_enterprise?
        ENTERPRISE
      elsif object.fleet_company.is_enterprise_fleet_management?
        ENTERPRISE_FLEET_MANAGEMENT
      elsif object.fleet_company.is_turo?
        TURO
      end
    end

  end
end
