# frozen_string_literal: true

module Resolvers
  class VehicleYears < Resolvers::Base

    include Utils::Query

    type [Types::VehicleYearType], null: false

    def resolve(**args)
      # we need name and id in our order here or graphql pro's cursor code won't
      # do the right thing, see http://graphql-ruby.org/pro/cursors#grouped-relations
      years = object.years

      # not doing this as a separate service (yet) because we'd just end up duplicating
      # the code from Resolvers::VehicleMakes in a confusing way
      query = args.dig(:query)
      return [] if present_but_blank?(query)

      if query.present?
        # yuck - can't figure out a sane way to do this in the db right now :/
        years = years.select { |y| y.to_s.start_with?(query.to_s) }
      end

      years
    end

  end
end
