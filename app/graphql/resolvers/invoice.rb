# frozen_string_literal: true

module Resolvers
  class Invoice < Resolvers::Base

    def resolve(**args)
      invoice_id = args[:id]

      Resolvers::Invoices.new(object: object, context: context)
        .resolve(args).find(invoice_id)
    end

  end
end
