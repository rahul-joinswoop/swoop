# frozen_string_literal: true

module Resolvers
  class CompaniesServices < MyCompanyOnly

    def resolve(**args)
      # TODO - would be better to sort by service_codes.sort but
      # that field is nullable and it breaks cursor generation
      filter = args.dig(:filter)

      results = object.companies_services_excluding_addons

      if filter == :create
        results.where.not(service_codes: { name: ['Storage', 'GOA'] })
      elsif filter == :storage
        results.where(service_codes: { support_storage: true })
      else
        results
      end
    end

  end
end
