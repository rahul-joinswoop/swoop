# frozen_string_literal: true

module Resolvers
  class Editable < Resolvers::Base

    include Utils::GraphQLHeaders
    def resolve(args: {}, **opts)
      case context[:api_company]
      when ::RescueCompany
        context[:api_user].present? && object_in_valid_state? && !disable_for_feature?(args)
      else
        true
      end
    end

    private

    def disable_for_feature?(args)
      # if a rescue user is only updating the customer email we allow it through
      return false if only_updating_customer_email?(args)

      disable_for_app_edit_jobs_feature?(args) || disable_for_app_drivers_edit_jobs_feature?(args)
    end

    # When a job has one of the statuses it can't be edited: Submitted, AutoAssigning, Assigned
    def object_in_valid_state?
      !(object.submitted? || object.auto_assigning? || object.assigned?)
    end

    def only_updating_customer_email?(args)
      args.dig(:customer, :email) && !has_other_keys?(args, { id: true, customer: { email: true } })
    end

    # When the feature "Disable App Drivers Edit Jobs" is set on the users company
    # and the user is not a dispatcher or admin the job can't be edited by the mobile app
    def disable_for_app_drivers_edit_jobs_feature?(args)
      mobile_app? &&
      context[:api_company]&.has_feature?(Feature::DISABLE_APP_DRIVERS_EDIT_JOBS) &&
      context[:api_user]&.driver_only?
    end

    # otherwise check the various features to see if it's editable
    # When the feature "Disable App Edit Jobs" is set on the users company the job
    # can't be edited by the mobile app
    def disable_for_app_edit_jobs_feature?(args)
      return false unless mobile_app?

      if args.dig(:partner, :driver, :name)
        return false unless has_other_keys?(args, {
          id: true,
          partner: {
            driver: { name: true },
            vehicle: { name: true },
          },
        })
      end

      context[:api_company]&.has_feature?(Feature::DISABLE_APP_EDIT_JOBS)
    end

    def has_other_keys?(search_hash, filter_hash)
      return search_hash.present? unless filter_hash.is_a?(Hash)
      search_hash.each do |k, v|
        return true unless filter_hash.include?(k)

        if v.is_a?(Hash) && has_other_keys?(v, filter_hash[k])
          return true
        end
      end

      false
    end

  end
end
