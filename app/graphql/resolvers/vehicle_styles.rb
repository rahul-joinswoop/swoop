# frozen_string_literal: true

module Resolvers
  class VehicleStyles < Resolvers::Base

    include Utils::Query

    def resolve(**args)
      query = args.dig(:query)
      return ::VehicleType.none if present_but_blank?(query)
      if query
        API::AutoComplete::VehicleTypes.new(term: query.strip, limit: false).call
      else
        ::VehicleType.all
      end.select(:id, :name).order(:name)
    end

  end
end
