# frozen_string_literal: true

# Here for compatibility reasons only, and should not be used.
# It will hopefully be removed soon.
# @see https://swoopme.atlassian.net/browse/ENG-9406
module Resolvers
  class EndUserInvoices < Resolvers::Base

    def resolve(**args)
      company = Resolvers::Company.new(object: object, context: context).resolve(args)
      invoices = case company
                 when FleetCompany
                   # TODO
                   company.end_user_invoices
                 when RescueCompany
                   # TODO
                   Invoice.none
                 when SuperCompany
                   case company.id
                   when ::Company.swoop_id
                     if context.has_role?(:root)
                       # for swoop root - return any invoice being sent to a user, is this right?
                       Invoice.where(recipient_type: :User)
                     else
                       # swoop dispatcher, no end user invoices
                       Invoice.none
                     end
                   else
                     Invoice.none
                   end
                 else
                   Invoice.none
                 end

      if object.is_a?(::Job)
        invoices.find_by(job_id: object.id)
      else
        invoices.order(id: :desc)
      end
    end

  end
end
