# frozen_string_literal: true

module Resolvers
  class CreateJob < Resolvers::Base

    include Utils::GraphQLHeaders

    def resolve(**args)
      # the below methods handle going from job params -> internal api params
      service_params = API::MapJobInput.new(
        api_company: context[:api_company],
        api_user: context[:api_user],
        params: args,
      ).call

      service_klass = context[:api_company].rescue? ? API::Jobs::CreateRescue : API::Jobs::CreateFleet
      job = service_klass.new(service_params).call.job

      if job && job.save
        # this doesn't seem to get called from the after_commit hook on Job so we'll call it manually
        # here
        GraphQL::JobStatusChangedWorker.perform_async(job.id, status: [nil, job.status])

        AnalyticsData::CreateJob.new(
          job: job,
          user_agent_hash: {
            platform: swoop_custom_platform,
            version: client_or_swoop_version,
          },
          api_company: context[:api_company],
        ).push_message

        { job: job }
      else
        { errors: job&.errors&.to_a }
      end
    end

  end
end
