# frozen_string_literal: true

module Resolvers
  class UpdateJob < Resolvers::Base

    include Utils::GraphQLHeaders

    def resolve(**args)
      job = Resolvers::Job.new(object: object, context: context).resolve(args)
      return if job.blank?

      # convert from graphql input -> job service params.
      service_params = API::MapJobInput.new(
        api_company: context[:api_company],
        api_user: context[:api_user],
        job: job,
        params: args,
      ).call

      # convert from job service params -> internal api params - UpdateFleet doesn't do this
      # for us :/

      params = API::MapJobParams.new(
        service_params.slice(:api_company, :api_user, :job, :params)
      ).call

      service_klass = context[:api_company].rescue? ? API::Jobs::UpdateByRescueCompany : API::Jobs::UpdateFleet

      prev_status = job.status

      job = service_klass.new(
        company: service_params[:api_company],
        api_user: service_params[:api_user],
        save_as_draft: service_params[:save_as_draft],
        job_id: job.id,
        params: params,
        # TODO - this isn't exposed yet, should it be?
        job_history: params&.dig(:job, :history),
        job_storage: params&.dig(:job, :storage),
      ).call.job

      if job.dispatched? && job.status != prev_status
        # it's possible that we've caused a status change inside of our update. example: if you set a
        # rescue_driver_id and a rescue_vehicle_id you'll also cause the status to go to dispatched.
        # anyway, this ugly hack makes sure we send analytics data in that case
        AnalyticsData::JobStatus.new(
          job: job,
          api_user: context[:api_user],
          api_application: context[:api_application],
          user_agent_hash: { platform: swoop_custom_platform, version: client_or_swoop_version },
          referer: referer,
        ).push_message
      end

      # TODO - should this be #save! ? then we could catch this up higher and DRY
      # our error behavior
      if job && job.save
        # send an analytics message if we successfully changed will_store
        if service_params.dig(:params).to_h.key?(:will_store)
          AnalyticsData::Storage.new(
            job: job,
            api_user: context[:api_user],
            api_application: context[:api_application],
            user_agent_hash: { platform: swoop_custom_platform, version: client_or_swoop_version },
            referer: referer,
          ).push_message
        end

        { job: job }
      else
        # TODO we're not exposing validation errors to clients with this unfortunately.
        # should we refactor it to `raise GraphQL::ExecutionError, job&.errors&.to_a`?
        # https://swoopme.atlassian.net/browse/ENG-10260
        { error: job&.errors&.to_a }
      end
    end

  end
end
