# frozen_string_literal: true

module Resolvers
  class Subscriptions

    def resolve(**args)
      context[:viewer].graphql_subscriptions
    end

  end
end
