# frozen_string_literal: true

module Resolvers
  class Invoices < Resolvers::Base

    def resolve(**args)
      company = Resolvers::Company.new(object: object, context: context).resolve(args)

      invoices = case company
                 when RescueCompany
                   if context[:api_user]&.driver_only? && context[:api_company].has_feature?(Feature::DISABLE_DRIVER_VIEW_INVOICE)
                     # If api_user is (rescue) driver only and feature 'Disable driver view invoice' is enabled for api_company,
                     # then don't expose the invoices to the logged in (rescue) driver.
                     ::Invoice.none
                   else
                     # partners can only be the sender of the invoice
                     company.outgoing_invoices.not_deleted
                   end
                 else
                   # This is the default case for FleetManaged: they don't see invoices.
                   # Also for now FleetInHouse and Swoop will fall into this case, and we can
                   # implement behaviour in the future when required.
                   ::Invoice.none
                 end

      if object.is_a?(::Job) # when querying through job.invoices
        invoices.where(job_id: object.id)
      else # when querying through company.invoices field
        invoices
      end
    end

  end
end
