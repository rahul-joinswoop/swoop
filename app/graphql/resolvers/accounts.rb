# frozen_string_literal: true

module Resolvers
  class Accounts < MyCompanyOnly

    include Utils::Query
    def resolve(**args)
      query = args.dig(:query)
      filter = args.dig(:filter)

      return ::Account.none if present_but_blank?(query)

      results = case object
                when ::RescueCompany
                  all_acccounts_or_query_by_name(args, object)
                else
                  ::Account.none
                end.not_deleted

      if filter == :create
        # LOLWUT - in pg != does not match NULL so we have to explicitly check for that case too.
        results.where("accounts.client_company_id != ? OR accounts.client_company_id IS NULL", ::Company.swoop_id)
      else
        results
      end
    end

    def all_acccounts_or_query_by_name(args, company)
      if args[:query]
        API::AutoComplete::Accounts.new(term: args[:query].strip, api_company: company, limit: false).call
      else
        company.accounts
      end
    end

  end
end
