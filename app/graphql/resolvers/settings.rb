# frozen_string_literal: true

module Resolvers
  class Settings < MyCompanyOnly

    def resolve(**args)
      object.settings
    end

  end
end
