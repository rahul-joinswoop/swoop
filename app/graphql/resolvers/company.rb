# frozen_string_literal: true

module Resolvers
  class Company < Resolvers::Base

    def resolve(**args)
      # for now, always resolve a company to the api_company
      context[:api_company]
      # TODO only allow a supercompany to specify a company other than themselves
      # case ctx[:api_company]
      # when SuperCompany
      #   obj.is_a? Company ? obj : ctx[:api_company]
      # else
      #   ctx[:api_company]
      # end
    end

  end
end
