# frozen_string_literal: true

# Return class types (aka vehicle categories) for the context company.
module Resolvers
  class VehicleClassTypes < MyCompanyOnly

    def resolve(**args)
      object.vehicle_class_type_enabled? ? object.vehicle_categories : nil
    end

  end
end
