# frozen_string_literal: true

module Resolvers
  class Trailers < MyCompanyOnly

    include Utils::Query
    type [Types::TrailerType], null: false

    def resolve(**args)
      query = args.dig(:query)

      return ::TrailerVehicle.none if present_but_blank?(query)

      # default where - we don't use SoftDelete because we need to be able to
      # remove this in the SwoopRoot case
      where = { deleted_at: nil }

      trailers = case object
                 when ::FleetCompany
                   ::TrailerVehicle.none
                 when ::RescueCompany
                   object.trailers
                 when ::SuperCompany
                   case object.id
                   when ::Company.swoop_id
                     if context.has_role?(:root)
                       # root user, can query any vehicles (including deleted ones)
                       where = {}
                       ::TrailerVehicle.all
                     else
                       ::TrailerVehicle.none
                     end
                   else
                     ::TrailerVehicle.none
                   end
                 else
                   ::TrailerVehicle.none
                 end
        .where(where)
        .order({ created_at: :desc }, :id)

      if args[:id]
        trailers.find args[:id]
      else
        if query
          API::AutoComplete::TrailerVehicles.new(term: query, api_company: object, scope: trailers, limit: false).call
        else
          trailers
        end
      end
    end

  end
end
