# frozen_string_literal: true

# Return sites for the context company.
module Resolvers
  class Sites < MyCompanyOnly

    include Utils::Query

    def resolve(**args)
      query = args.dig(:query)

      return ::Site.none if present_but_blank?(query)
      all_sites_or_query_by_name(args, object).not_deleted
    end

    def all_sites_or_query_by_name(args, company)
      if args[:query]
        query_term = args[:query].downcase
        API::AutoComplete::Sites.new(term: query_term.strip, api_company: object, limit: false).call
      else
        object.sites
      end
    end

  end
end
