# frozen_string_literal: true

module Resolvers
  class Account < Base

    def authorized?(**args)
      # TODO - do client companies need to see accounts? probably not? or should they see accounts that they
      # are the client_company on?
      # we allow authorized? to pass through if we don't have an account object, this is because we may
      # substitue one in below
      super && (object.account.blank? || object.account&.company_id == context[:api_company].id)
    end

    def resolve(**args)
      # TODO - see Job#account_name for the logic we'll use for other job types
      if object.is_a?(::FleetManagedJob) && object.account_id.blank? && context.is_partner?
        return context[:api_company].accounts.find_by(name: "Swoop")
      end
      object.account
    end

  end
end
