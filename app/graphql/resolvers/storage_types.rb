# frozen_string_literal: true

module Resolvers
  class StorageTypes < MyCompanyOnly

    def resolve(**args)
      object.storage_types.not_deleted
    end

  end
end
