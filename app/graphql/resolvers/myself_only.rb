# frozen_string_literal: true

module Resolvers
  class MyselfOnly < Resolvers::Base

    include Resolvers::Buildable
    include Resolvers::PermissionChecks

    def authorized?(**args)
      super && object_is_api_user
    end

  end
end
