# frozen_string_literal: true

module Resolvers
  class LocationTypes < MyCompanyOnly

    include Utils::Query

    def resolve(**args)
      query = args.dig(:query)

      return ::LocationType.none if present_but_blank?(query)
      all_location_types_or_query_by_name(args, object).not_deleted
    end

    def all_location_types_or_query_by_name(args, company)
      if args[:query]
        query_term = args[:query].downcase
        API::AutoComplete::LocationTypes.new(term: query_term.strip, api_company: object, limit: false).call
      else
        object.location_types
      end
    end

  end
end
