# frozen_string_literal: true

# Return departments for the context company.
module Resolvers
  class Departments < MyCompanyOnly

    def resolve(**args)
      object.departments
    end

  end
end
