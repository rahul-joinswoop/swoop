# frozen_string_literal: true

module Resolvers
  class PartnerVehicles < MyCompanyOnly

    include Utils::Query

    def resolve(**args)
      query = args.dig(:query)
      return ::RescueVehicle.none if present_but_blank?(query)

      # default where - we don't use SoftDelete because we need to be able to
      # remove this in the SwoopRoot case
      where = { deleted_at: nil }

      vehicles = case object
                 when ::FleetCompany
                   object.fleet_rescue_vehicles
                 when ::RescueCompany
                   object.vehicles
                 when ::SuperCompany
                   case object.id
                   when ::Company.swoop_id
                     if context.has_role?(:root)
                       # root user, can query any vehicles (including deleted ones)
                       where = {}
                       ::RescueVehicle.all
                     else
                       # TODO is there a role for this?
                       # dispatcher - return back the same as FleetCompany
                       object.fleet_rescue_vehicles
                     end
                   else
                     ::RescueVehicle.none
                   end
                 else
                   ::RescueVehicle.none
                 end
        .where(where)
        .order({ created_at: :desc }, :id)

      results = if args[:id]
                  vehicles.find args[:id]
                elsif query
                  API::AutoComplete::RescueVehicles.new(term: query, api_company: object, scope: vehicles, limit: false).call
                else
                  vehicles
                end

      hide_off_duty_drivers = args.dig(:filters, :offDutyDriver)
      hide_vehicles_without_drivers = args.dig(:filters, :noDriver)

      if hide_vehicles_without_drivers && !hide_off_duty_drivers
        results = results.joins(:driver).distinct
      elsif !hide_vehicles_without_drivers && hide_off_duty_drivers
        results = results.left_outer_joins(:driver).where("users.id IS NULL OR users.on_duty = TRUE").distinct
      elsif hide_vehicles_without_drivers && hide_off_duty_drivers
        results = results.joins(:driver).where(users: { on_duty: true }).distinct
      end

      area = args.dig(:area)
      if area.present?
        results.where("vehicles.lat BETWEEN ? AND ? AND vehicles.lng BETWEEN ? AND ?",
                      area[:from][:lat], area[:to][:lat], area[:from][:lng], area[:to][:lng])
      else
        results
      end
    end

  end
end
