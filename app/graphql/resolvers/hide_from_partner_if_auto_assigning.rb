# frozen_string_literal: true

module Resolvers
  class HideFromPartnerIfAutoAssigning < Resolvers::Base

    include Resolvers::Buildable
    include Resolvers::PermissionChecks

    def authorized?(**args)
      super &&
      object.is_a?(::Job) &&
      !(context.is_partner? && object.auto_assigning?)
    end

  end
end
