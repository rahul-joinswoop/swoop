# frozen_string_literal: true

module Resolvers
  class CompanyVehicleType < MyCompanyOnly

    def resolve(**args)
      object.vehicles
    end

  end
end
