# frozen_string_literal: true

module Resolvers
  class VehicleMakes < Resolvers::Base

    include Utils::Query

    def resolve(**args)
      # very similar to VehicleMakeService except:
      #
      # - we use #joins instead of #includes to keep graphql-pro happy for some
      #   reason that i don't understand
      # - we don't bother joining against vehicle_types because we don't need
      #   them
      # - if we don't have Feature::HEAVY_DUTY_EQUIPMENT then we have to add
      #   a #group statement or else we end up with duplicates
      # we need name and id in our order here or graphql pro's cursor code won't
      # do the right thing, see http://graphql-ruby.org/pro/cursors#grouped-relations
      makes = ::VehicleMake.where({ original: true })

      if !context[:api_company].has_feature(Feature::HEAVY_DUTY_EQUIPMENT)
        makes = makes.joins(:vehicle_models)
          .where(vehicle_models: { heavy_duty: [false, nil] })
          .group(:id)
      end

      # not doing this as a separate service (yet) because we'd just end up duplicating
      # the above code in a confusing manner
      query = args.dig(:query)
      return ::VehicleMake.none if present_but_blank?(query)

      if query.present?
        makes = makes.where('vehicle_makes.name ILIKE ?', "%#{query}%")
      end

      # we need name and id in our order here or graphql pro's cursor code won't
      # do the right thing, see http://graphql-ruby.org/pro/cursors#grouped-relations
      makes.order(:name, :id)
    end

  end
end
