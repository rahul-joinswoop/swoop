# frozen_string_literal: true

module Resolvers
  class ClientInvoices < Resolvers::Base

    # TODO - this needs to be refactored, see Resolvers::EndUserInvoices and
    # https://github.com/joinswoop/swoop/pull/3917#discussion_r226772232
    def resolve(**args)
      company = Resolvers::Company.new(object: object, context: context).resolve(args)
      case company
      when FleetCompany
        company.incoming_invoices
      when RescueCompany
        # TODO - is this right?
        Invoice.none
      when SuperCompany
        # for swoop - return any client invoice, is this right?
        Invoice.where.not(recipient_company: nil)
      else
        Invoice.none
      end.order(id: :desc)
    end

  end
end
