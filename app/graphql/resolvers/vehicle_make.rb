# frozen_string_literal: true

module Resolvers
  class VehicleMake < Resolvers::Base

    def resolve(**args)
      makes = Resolvers::VehicleMakes.new(object: object, context: context).resolve(args)
      if args[:id]
        makes.find(args[:id])
      elsif args[:name]
        makes.find_by!(name: args[:name])
      end
    end

  end
end
