# frozen_string_literal: true

module Resolvers
  class MyCompanyOnly < Resolvers::Base

    include Resolvers::Buildable
    include Resolvers::PermissionChecks

    def authorized?(**args)
      super && [
        object_is_api_company,
        object_is_api_company_user,
      ].any?
    end

  end
end
