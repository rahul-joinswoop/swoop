# frozen_string_literal: true

# Return payment types for the context company.
module Resolvers
  class PaymentTypes < MyCompanyOnly

    def resolve(**args)
      object.payment_types
    end

  end
end
