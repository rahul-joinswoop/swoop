# frozen_string_literal: true

module Resolvers
  class JobPartnerVehicle < Base

    def resolve(**args)
      # this resolver is only for job.partner_vehicle
      if object.is_a?(::Job)
        if context.is_client?
          # clients can only see the driver object if a job is enroute or farther and not in a terminal state
          # (completed, goa, cancelled)
          return nil unless ::Job.statuses[object.status].in?(::Job.fleet_show_rescue_vehicle_states)
          if object.rescue_vehicle.driver.present?
            # and even if they can see the driver, they don't get the driver's last name
            object.rescue_vehicle.driver.full_name = object.rescue_vehicle.driver.first_name
          end
        end
        object.rescue_vehicle
      end
    end

  end
end
