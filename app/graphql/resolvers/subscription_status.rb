# frozen_string_literal: true

module Resolvers
  class SubscriptionStatus < MyCompanyOnly

    def resolve(**args)
      object&.subscription_status&.name
    end

  end
end
