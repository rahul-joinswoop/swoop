# frozen_string_literal: true

module Resolvers
  class JobStatusReasonTypes < Resolvers::Base

    def resolve(**args)
      friendly_name = Types::JobStatusEnumType.friendly_name(args.dig(:status))

      job_status_id = JobStatus.find_by!(name: friendly_name).id

      object.status_reason_types_by(job_status_id)
    end

  end
end
