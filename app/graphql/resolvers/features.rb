# frozen_string_literal: true

module Resolvers
  class Features < MyCompanyOnly

    def resolve(**args)
      object.features
    end

  end
end
