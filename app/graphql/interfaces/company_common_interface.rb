# frozen_string_literal: true

module Interfaces::CompanyCommonInterface

  include Types::BaseInterface
  include Interfaces::JobsInterface

  graphql_name "CompanyCommon"
  description "Common fields on Company"

  field :accounts,
        Types::AccountType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::Accounts,
        required_roles: :rescue,
        description: "Fetch Accounts list paginated, can be searched by account name through query argument" do
    argument :query,
             String,
             "Search String",
             required: false
    argument :filter,
             Types::AccountFilterEnumType,
             required: true,
             default_value: :all
  end

  field :class_types,
        Types::VehicleClassTypeType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::VehicleClassTypes,
        if_company: :vehicle_class_type_enabled?,
        description: 'Vehicle class types available for the Company.'

  field :departments,
        Types::DepartmentType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::Departments,
        required_features: Feature::DEPARTMENTS,
        description: 'Departments available for the Company.'

  field :feature,
        Types::FeatureType,
        null: true,
        deprecation_reason: "Please use the node interface instead" do
    argument :id,
             ID,
             "FeatureType ID",
             prepare: Resolvers::GlobalId.new,
             required: true
  end

  def feature(**args)
    Resolvers::Features.new(object: object, context: context).resolve(args).find(args[:id])
  rescue ActiveRecord::RecordNotFound
    # noop - catching this error in our main schema is broken for now, see
    # https://github.com/rmosolgo/graphql-ruby/issues/1598
    nil
  end

  field :features,
        Types::FeatureType.connection_type,
        null: true,
        max_page_size: 50,
        connection: true,
        resolver: Resolvers::Features,
        description: 'Features array for the Company.'

  field :invoices,
        Types::InvoiceType.connection_type,
        resolver: Resolvers::Invoices,
        description: 'Invoices that are available for the Company'

  field :invoice,
        Types::InvoiceType,
        null: true,
        description: 'Fetch single invoice',
        required_roles: :rescue do
    argument :id,
             ID,
             "Invoice ID",
             prepare: Resolvers::GlobalId.new,
             required: true
  end

  def invoice(**args)
    Resolvers::Invoice.new(object: object, context: context).resolve(args)
  rescue ActiveRecord::RecordNotFound
    # noop - catching this error in our main schema is broken for now, see
    # https://github.com/rmosolgo/graphql-ruby/issues/1598
    nil
  end

  field :job,
        Types::JobType,
        null: true,
        required_roles: :any,
        description: 'Fetch single job' do
    argument :id,
             ID,
             "Job ID",
             prepare: Resolvers::GlobalId.new,
             required: true,
             required_roles: :any
  end

  def job(**args)
    Resolvers::Job.new(object: object, context: context).resolve(args)
  rescue ActiveRecord::RecordNotFound
    # noop - catching this error in our main schema is broken for now, see
    # https://github.com/rmosolgo/graphql-ruby/issues/1598
    nil
  end

  field :location_types,
        Types::LocationTypeType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::LocationTypes do
    argument :query,
             String,
             "Search String",
             required: false
  end

  field :location_type,
        Types::LocationTypeType,
        null: true,
        deprecation_reason: "Please use the node interface instead" do
    argument :id,
             ID,
             "LocationType ID",
             prepare: Resolvers::GlobalId.new,
             required: true
  end

  def location_type(**args)
    Resolvers::LocationTypes.new(object: object, context: context).resolve(args).find(args[:id])
  rescue ActiveRecord::RecordNotFound
    # noop - catching this error in our main schema is broken for now, see
    # https://github.com/rmosolgo/graphql-ruby/issues/1598
    nil
  end

  field :mobile_app,
        Types::ApplicationType,
        null: false,
        if_company: :is_swoop?

  def mobile_app
    SuperCompany::IGNORE_ME_I_AM_A_FAKE_MOBILE_APP
  end

  field :partner_vehicles,
        Types::PartnerVehicleType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::PartnerVehicles,
        description: "Fetch PartnerVehicle list paginated" do
    argument :area,
             Types::AreaInputType,
             required: false
    argument :filters,
             Types::PartnerVehicleHideType,
             required: false
    argument :query,
             String,
             "Search String",
             required: false
  end

  field :paymentTypes,
        Types::PaymentType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::PaymentTypes,
        description: 'Payment types available for the Company.'

  field :rescue_vehicles,
        Types::PartnerVehicleType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::PartnerVehicles,
        deprecation_reason: "Please use partnerVehicles instead",
        description: "Fetch PartnerVehicle list paginated" do
    argument :query,
             String,
             "Search String",
             required: false
  end

  field :services,
        Types::CompanyServiceType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::CompaniesServices,
        description: 'Services available for the Company, they are used on Job.' do
    argument :filter,
             Types::ServiceFilterEnumType,
             required: true,
             default_value: :all
  end

  field :service,
        Types::CompanyServiceType,
        null: true,
        description: 'Service available for the Company',
        deprecation_reason: "Please use the node interface instead" do
    argument :id,
             ID,
             "Service ID",
             prepare: Resolvers::GlobalId.new,
             required: true
  end

  def service(**args)
    Resolvers::CompaniesServices.new(object: object, context: context).resolve(args).find(args[:id])
  rescue ActiveRecord::RecordNotFound
    # noop - catching this error in our main schema is broken for now, see
    # https://github.com/rmosolgo/graphql-ruby/issues/1598
    nil
  end

  field :settings,
        Types::SettingType.connection_type,
        null: true,
        max_page_size: 50,
        connection: true,
        resolver: Resolvers::Settings,
        description: 'Settings array for the Company.'

  field :sites,
        Types::SiteType.connection_type,
        null: false,
        connection: true,
        resolver: Resolvers::Sites,
        if_company: :sites_enabled?,
        description: 'Sites available for the Company.' do
    argument :query,
             String,
             "Search String",
             required: false
  end

  field :storage_types,
        Types::StorageTypeType.connection_type,
        null: false,
        connection: true,
        resolver: Resolvers::StorageTypes,
        description: 'Storage Types available for the Company.'

  field :symptoms,
        Types::SymptomType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::Symptoms,
        required_features: Feature::SHOW_SYMPTOMS,
        description: 'Symptoms available for the Company, they can be used on Job when Symptoms feature is enabled.'

  field :symptom,
        Types::SymptomType,
        null: true,
        required_features: Feature::SHOW_SYMPTOMS,
        description: 'Symptom available for the Company',
        deprecation_reason: "Please use the node interface instead" do
    argument :id,
             ID,
             "Symptom ID",
             prepare: Resolvers::GlobalId.new,
             required: true
  end

  def symptom(**args)
    Resolvers::Symptoms.new(object: object, context: context).resolve(args).find(args[:id])
  rescue ActiveRecord::RecordNotFound
    # noop - catching this error in our main schema is broken for now, see
    # https://github.com/rmosolgo/graphql-ruby/issues/1598
    nil
  end

  field :trailers,
        Types::TrailerType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::Trailers,
        description: "Fetch Trailer list paginated",
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::TRAILERS)
        } do
    argument :query,
             String,
             "Search String",
             required: false
  end

  field :users,
        Types::UserType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::Users,
        required_roles: :rescue,
        description: "Fetch Users list paginated, can be searched by query name through query argument" do
    argument :query,
             String,
             "Search String",
             required: false
    argument :roles,
             [Types::RoleEnumType],
             required: false,
             description: <<~DESCRIPTION
      Filter by user roles.

      Available Swoop roles are: Driver, Dispatcher and Admin. It's not required.

      Examples:
        - if ['Driver'] is passed, it will filter by drivers.
        - if ['Driver', 'Dispatcher'] is passed as filter, it will filter by drivers and dispatchers.
        - if ['Driver', 'Dispatcher', 'Admin'] is passed as filter, it will filter by drivers, dispatchers and admins.
        - if ['Dispatcher', 'Admin'] is passed as filter, it will filter by dispatchers and admins.
    DESCRIPTION
  end

  field :vehicle,
        Types::CompanyVehicleType,
        null: true,
        resolver: Resolvers::CompanyVehicleType

end
