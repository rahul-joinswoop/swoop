# frozen_string_literal: true

# common fields for all vehicles
module Interfaces::VehicleInterface

  include Types::BaseInterface
  include Interfaces::VehicleMinimalInterface
  description "A Vehicle"

  field :color, String, null: true
  field :license, String, null: true
  field :location, Types::VehicleLocationType, null: true, required_roles: :any

  def location
    object
  end

  field :sequence, String, null: false

  def sequence
    object.number
  end

  field :vin, String, null: true
  field :year, Integer, null: true

end
