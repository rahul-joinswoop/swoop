# frozen_string_literal: true

module Interfaces::JobsInterface

  include Types::BaseInterface

  field :jobs,
        Types::JobType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::Jobs,
        description: "Fetch job list paginated" do
    argument :states,
             Types::JobStatusGroupEnumType,
             required: false,
             description: "Deprecated: Please use filters.states instead"
    argument :filters,
             Types::JobsFiltersInputType,
             required: false,
             description: "Filters for Job list",
             # LOLWUT this has to be in sync with the default value set on the
             # states field in JobInputType because reasons, see
             #  https://github.com/rmosolgo/graphql-ruby/issues/1374
             default_value: { states: :active_states }
  end

end
