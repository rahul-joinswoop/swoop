# frozen_string_literal: true

# common fields for all vehicles
module Interfaces::VehicleMinimalInterface

  include Types::BaseInterface
  include Interfaces::TimestampsInterface

  description "A Minimal Vehicle"

  global_id_field :id

  field :category,
        Types::VehicleClassTypeType,
        null: true,
        preload: :vehicle_category,
        method: :vehicle_category,
        deprecation_reason: 'Please use classType instead.'

  field :class_type,
        Types::VehicleClassTypeType,
        null: true,
        preload: :vehicle_category,
        method: :vehicle_category

  field :make,
        String,
        null: true,
        description: "The make of the Vehicle."

  field :model,
        String,
        null: true,
        description: "The model of the Vehicle."

  field :name,
        String,
        null: true,
        description: "The name of the Vehicle."

  definition_methods do
    # TODO - this doesn't work, have to duplicate it in our main schema :/
    # Determine what object type to use for `object`
    def resolve_type(object, context)
      if object.is_a?(::RescueVehicle)
        Types::PartnerVehicleType
      elsif object.is_a?(::StrandedVehicle)
        Types::StrandedVehicleType
      elsif object.is_a?(::TrailerVehicle)
        Types::TrailerType
      else
        raise "Unexpected Vehicle: #{object.inspect}"
      end
    end
  end

end
