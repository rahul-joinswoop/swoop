# frozen_string_literal: true

module Interfaces::TimestampsInterface

  include Types::BaseInterface

  field :created_at, Types::DateTimeType, null: true, description: "Created At"
  field :deleted_at, Types::DateTimeType, null: true, description: "Deleted At"
  field :updated_at, Types::DateTimeType, null: true, description: "Updated At"

end
