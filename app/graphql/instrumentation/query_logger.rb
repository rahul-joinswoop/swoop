# frozen_string_literal: true

module Instrumentation
  module QueryLogger

    module_function

    def before_query(query)
      @start = DateTime.current
    end

    def after_query(query)
      elapsed_ms = ((DateTime.current - @start) * 1.days).in_milliseconds.to_f.round(1)
      Utils::QueryLogger.log_query query: query, elapsed_ms: elapsed_ms
    end

  end
end
