# frozen_string_literal: true

module Instrumentation
  module AuditLogger

    class << self

      include Utils::QueryErrors

      def before_query(query)
        return unless query.selected_operation&.operation_type == 'mutation'
        variables = query.provided_variables
        variables = variables.to_unsafe_h if variables.respond_to?(:to_unsafe_h)

        json = {
          query: (query.query_string || query&.document&.to_query_string),
          variables: variables,
        }.to_json

        # we don't sepecify a target here because there's not necessarily a single
        # model being targeted - we could figure _something_ out - (ie, job id, user
        # id, etc) but i don't understand what we're using them for.

        # TODO - why aren't all of these bang methods?
        query.context[:event_operation] = Command.new(
          event_operation: EventOperation.fetch_by_name(EventOperation::MUTATION),
          user: query.context[:api_user],
          company: query.context[:api_company],
          json: json,
          request: query.context[:request_id]
        )
        query.context[:event_operation].save!
      end

      def after_query(query)
        return unless query.selected_operation&.operation_type == 'mutation'
        return if query.context[:event_operation].blank?
        query.context[:event_operation].update!(applied: query_errors(query).blank?)
      end

    end

  end
end
