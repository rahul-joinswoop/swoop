# frozen_string_literal: true

module Instrumentation
  module MultiplexLogger

    module_function

    # TODO - this is always called but should only be called when someone hits #batch
    def before_multiplex(multiplex)
      @start = DateTime.current
    end

    def after_multiplex(multiplex)
      elapsed_ms = ((DateTime.current - @start) * 1.days).in_milliseconds.to_f.round(1)
      Utils::QueryLogger.log_multiplex multiplex: multiplex, elapsed_ms: elapsed_ms
    end

  end
end
