# frozen_string_literal: true

Dir[Rails.root.join("app/graphql/monkeypatches/**/*.rb")].sort.each { |f| require f }

class SwoopSchema < GraphQL::Schema

  use GraphQL::Batch
  use GraphQL::Pro::OperationStore

  enable_preloading

  # disable error bubbling by default - we'll reenable it in GraphQLController for customers with
  # legacy graphql errors enabled
  error_bubbling false

  if ::Rails.env.production?
    use(GraphQL::Tracing::NewRelicTracing, set_transaction_name: true)
  end

  instrument :multiplex, ::Instrumentation::MultiplexLogger

  instrument :query, ::Instrumentation::QueryLogger
  instrument :query, ::Instrumentation::AuditLogger

  tracer GraphQL::Tracing::ActiveSupportNotificationsTracing

  # Load all relevant Analyzers into ObjectSpace
  Dir[Rails.root.join("app/graphql/analyzers/*.rb")].sort.each { |f| require f }
  # Traverse MutationAnalyzer heirarchy down to leaves to avoid initializing abstract classes.
  Analyzers::MutationAnalyzer.descendants.each do |klass|
    query_analyzer klass.new
  end

  # build our field usage analyzer - we do it like this because our block is for
  # GraphQL::Analysis::FieldUsage.new and not query_analyzer so we can't do it all in one line.
  FIELD_USAGE_ANALYZER = GraphQL::Analysis::FieldUsage.new do |query, used_fields, used_deprecated_fields|
    Utils::Stats.call query: query, used_fields: used_fields, used_deprecated_fields: used_deprecated_fields
  end

  query_analyzer FIELD_USAGE_ANALYZER

  mutation Types::MutationType

  query Types::QueryType

  subscription Types::SubscriptionType
  use Subscriptions::SwoopMultiSubscriptions,
      redis: Swoop.graphql_redis_client,
      ably: Swoop.ably_rest_client

  # use our customer cursor encoder
  cursor_encoder Encoders::SecureEncoder

  default_max_page_size 20

  # disable this in development since sometimes our initial loads take more than 5 seconds and we'll end up in an
  # endless error loop
  unless Rails.env.development?
    middleware GraphQL::Schema::TimeoutMiddleware.new(max_seconds: 5) do |err, query|
      Rails.logger.info("GraphQL Timeout: #{query.query_string}")
    end
  end

  def self.id_from_object(object, type_definition, context)
    # we return unique job ids for rescue companies that are involved in auctions.
    # this simplifies life for 3rd party partner api consumers
    if object.is_a?(::Job) && context.is_partner?
      bid = object.bids.find_by(company_id: context[:api_company].id)
      return SomewhatSecureID.encode(object, bid) if bid.present?
    end

    SomewhatSecureID.encode(object)
  end

  def self.object_from_id(id, query_ctx)
    # we don't use the bang method here so we just end up returning nil if this
    # blows up (this matches the behavior from #authorized?)
    SomewhatSecureID.load(id)
  end

  class UnexpectedObject < StandardError

    attr_accessor :object

  end

  def self.resolve_type(type, obj, ctx)
    case obj
    # when AuditJobStatus
    #   Types::History
    when AttachedDocument
      ::Types::DocumentType
    when CompaniesService
      ::Types::CompanyServiceType
    when ::Feature
      Types::FeatureType
    when ::GraphQLSubscription
      Types::WebhookSubscriptionType
    when Invoice
      Types::InvoiceType
    when ::Auction::Auction
      Types::AuctionType
    when ::TrailerVehicle
      Types::TrailerType
    when ::RescueVehicle
      Types::PartnerVehicleType
    when ::StrandedVehicle
      Types::StrandedVehicle
    when ::Job
      Types::JobType
    when ::Company
      Types::CompanyType
    when LocationType
      Types::LocationTypeType
    when PCC::Coverage
      Types::CoverageType
    when PCC::Policy
      Types::PolicyType
    # when ServiceCode
    #   Types::ServiceType
    when Symptom
      Types::SymptomType
    when VehicleMake
      Types::VehicleMakeType
    when VehicleModel
      Types::VehicleModelType
    # when VehicleType
    #   Types::VehicleTypeType
    when ::User
      Types::UserType
    else
      e = UnexpectedObject.new "Unexpected object"
      e.object = obj
      raise e
    end
  end

  # this doesn't work if we put it below, why is that?
  rescue_from UnexpectedObject do |e|
    Rollbar.error e, object: e.object
    # we can't return the actual type here because we don't have a 1:1 mapping
    # of all rails models -> types (only ones that we support above)
    "node interface not implemented for #{Rails.env.development? ? e.object.class.name : 'object'}"
  end

end

GraphQL::Errors.configure(SwoopSchema) do
  rescue_from ActiveRecord::RecordNotFound do |exception, obj, args, ctx|
    nil
  end

  rescue_from ActiveRecord::RecordInvalid do |exception, obj, args, ctx|
    ctx.add_error GraphQL::ExecutionError.new(exception.record.errors.full_messages.join("\n"))
  end

  rescue_from StandardError do |exception, obj, args, ctx|
    raise exception if exception.is_a?(GraphQL::ExecutionError)

    # any other error we want to send to rollbar
    Rollbar.error(exception)
    Rails.logger.error("#{exception.message}\n#{exception.backtrace.join("\n")}")
    # if we're in production we just return back a generic error. otherwise we
    # give the actual error
    ctx.add_error GraphQL::ExecutionError.new(
      Rails.env.development? ? "#{exception.message}\n#{exception.backtrace.join("\n")}" : GraphQLController::UNKNOWN_ERROR
    )
  end
end

# workaround for https://github.com/rmosolgo/graphql-ruby/issues/1505
#
# This processes the schema members and makes some caches on the schema,
# maybe it's subject to a race condition?
# So eagerly warm that cache at load time:
SwoopSchema.graphql_definition
