# frozen_string_literal: true

module Utils
  class Stats

    include Interactor::Organizer

    around do |interactor|
      # set the date we're using when we start to avoid any edge cases
      context.date = Date.current.strftime('%Y%m%d')

      # wrap all redis commands in a single pipeline call
      begin
        ::Swoop.graphql_redis_client.pipelined do |redis_pipeline|
          context.redis_pipeline = redis_pipeline
          interactor.call
        end
      rescue StandardError => e
        Rails.logger.error "#{self.class.name}: error in organizer (#{e.message})"
        Rollbar.error e
      end
    end

    organize Stats::Queries,
             Stats::DeprecatedFields

  end
end
