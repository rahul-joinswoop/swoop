# frozen_string_literal: true

module Utils::Stats::Common

  extend ActiveSupport::Concern

  # hack - mobile doesn't have an application_id so we use this one instead
  # (we can use -2 for web whenever that happens)
  MOBILE_APP_ID = -1

  # default ttl for our stats in redis
  TTL = 30.days.to_i

  included do
    include Interactor

    # the base redis key for our stats is based on the module name, ends up being something like:
    #
    # graphql:stats:fields
    #
    # we have set it this way because it's dynamically defined once this concern is included,
    # and we use self.class::KEY below to access it because KEY would refer to Utils::Stats::Common::KEY
    # which isn't defined.
    const_set :KEY, ['graphql', *(name.snakecase.split('/')[1..-1])].join(':')
  end

  def build_all_key(date)
    "#{self.class::KEY}:all:#{date}"
  end

  def build_application_key(date, id)
    "#{self.class::KEY}:application:#{id}:#{date}"
  end

  def application_id
    @application_id ||= (context.query.context[:api_application]&.id || MOBILE_APP_ID)
  end

end
