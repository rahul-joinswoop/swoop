# frozen_string_literal: true

module Utils
  class Stats::DeprecatedFields

    include Stats::Common

    def call
      # no need to do anything if we don't have any deprecated fields
      return if context.used_deprecated_fields.blank?

      # NOTE - context.used_deprecated_fields is de-duped, we don't know how many times
      # the current query used a deprecated field, we just know it was used at least once.

      # our redis key will look something like:
      #
      # graphql:stats:deprecated_fields:(all|application_id):YYYMMDD
      #
      # it will be a sorted set - this way we can quickly increment a given deprecated
      # field inside of it and easily get a list of all deprecated fields used on a given
      # day ordered by the usage count.
      #
      # we set a 30 day expiration on the key so that we don't keep old data around

      # always increment the all key
      all_key = build_all_key(context.date)
      context.used_deprecated_fields.each do |field|
        context.redis_pipeline.zincrby all_key, 1, field
      end
      context.redis_pipeline.expire all_key, TTL

      # if we have an application, increment its count as well, otherwise assume it's from mobile (for now)
      # TODO - need to handle web case as well whenever that happens
      application_key = build_application_key(context.date, application_id)

      context.used_deprecated_fields.each do |field|
        context.redis_pipeline.zincrby application_key, 1, field
      end
      context.redis_pipeline.expire application_key, TTL
    end

  end
end
