# frozen_string_literal: true

module Utils
  class Stats::Queries

    include Stats::Common

    def call
      # our redis key will look something like:
      #
      # graphql:stats:queries:(all|application_id):YYYMMDD
      #
      # it's just a basic key - we increment it every time an applicable request comes in.
      #
      # we set a 30 day expiration on the key so that we don't keep old data around

      # always increment the all key
      all_key = build_all_key(context.date)
      context.redis_pipeline.incr all_key
      context.redis_pipeline.expire all_key, TTL

      # if we have an application, increment its count as well, otherwise assume it's from mobile (for now)
      # TODO - need to handle web case as well whenever that happens
      application_key = build_application_key(context.date, application_id)
      context.redis_pipeline.incr application_key
      context.redis_pipeline.expire application_key, TTL
    end

  end
end
