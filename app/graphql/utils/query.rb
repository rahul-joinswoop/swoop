# frozen_string_literal: true

module Utils
  module Query

    def present_but_blank?(query)
      # provided a query but it's blank
      !query.nil? && query.strip.blank?
    end

  end
end
