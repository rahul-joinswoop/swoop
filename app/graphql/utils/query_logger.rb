# frozen_string_literal: true

module Utils
  module QueryLogger

    class << self

      include Utils::QueryErrors

      GENERATE_OPTS = { space: ' ', object_nl: ' ', array_nl: ' ' }.freeze

      def log_query(query:, elapsed_ms:)
        operation_name = query.selected_operation_name
        operation_type = query.selected_operation&.operation_type
        # TODO - should we deep_dup first?
        variables = query.provided_variables
        variables = variables.to_unsafe_h if variables.respond_to?(:to_unsafe_h)
        errors = query_errors(query).uniq.map do |error|
          error.to_h.tap do |e|
            # don't log the full stack trace if we have one
            e["message"] = e["message"].split("\n").first
          end
        end
        msg = []
        msg << [
          "GraphQL#{' Error' if errors.present?}:",
          viewer_string(query.context[:viewer]),
          operation_type,
          operation_name,
          "(#{elapsed_ms}ms)",
        ].reject(&:blank?).join(' ')

        graphql_req = {}.tap do |r|
          # don't log any of this stuff for IntrospectionQueries
          # TODO - this isn't right - we should look at the actual query and not the name
          if operation_name != "IntrospectionQuery"
            r[:operationName] = operation_name if operation_name.present?

            # handle a query string _OR_ a parsed graphql document
            query_string = (query.query_string || query&.document&.to_query_string).to_s.gsub(/\n(\s+)?/, ' ')
            r[:query] = query_string if query_string.present?

            variables = query.provided_variables
            variables = variables.to_unsafe_h if variables.respond_to?(:to_unsafe_h)
            r[:variables] = variables if variables.present?
          end
        end

        (msg << JSON.generate(graphql_req, GENERATE_OPTS)) if graphql_req.present?
        (msg << "errors: #{JSON.generate(errors)}") if errors.present?
        Rails.logger.debug msg.join(' ')
      end

      # TODO - fix this
      def log_multiplex(multiplex:, elapsed_ms:)
        operation_names = multiplex.queries.map(&:selected_operation_name)
        msg = "GraphQL: batched #{operation_names.inspect} (#{elapsed_ms}ms)"
        Rails.logger.debug msg
      end

      VIEWER_STRING = "viewer %s(id: %d)"
      def viewer_string(viewer)
        type = case viewer
               when Doorkeeper::Application
                 'Application'
               when ::User
                 'User'
               when ::Company
                 'Company'
               else
                 ''
               end
        id = viewer&.id

        if type.blank? || id.blank?
          ''
        else
          VIEWER_STRING % [type, id]
        end
      end

    end

  end
end
