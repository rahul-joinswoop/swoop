# frozen_string_literal: true

module Utils
  module VariablesConverter

    # TODO - would be cool to do this as a middleware?
    def self.convert(params = {})
      params.to_h.deeper_dup.deep_transform_values { |v| v }.deep_transform_keys! { |k| k.to_s.underscore.to_sym }
    end

  end
end
