# frozen_string_literal: true

# Helper methods for pulling data out of the GraphQL context. these are included into
# GraphQL::Query::Context via monkeypatches/add_context_utils.rb  and run in the
# context's context (heh).
module Utils::Context

  # Return true if the context has the specified role.
  def has_role?(role)
    api_roles.include?(role.to_s)
  end

  # Return true if the context has any of the specifies roles.
  def has_any_role?(*roles)
    return false if api_roles.blank?
    (api_roles & roles.map(&:to_s)).present?
  end

  # Return true if the context has all of the specified roles.
  def has_all_roles?(*roles)
    return false if api_roles.blank?
    (api_roles & roles.map(&:to_s)).size == roles.size
  end

  def has_feature?(feature)
    !!(api_company&.has_feature? feature)
  end

  def is_super_client_managed?
    !!(api_company&.is_super_client_managed? && has_role?(:super_fleet_managed))
  end

  def is_client?
    !!api_company&.is_client?
  end

  def is_partner?
    !!api_company&.is_partner?
  end

  def is_swoop?
    !!api_company&.is_swoop?
  end

  def is_super?
    !!api_company&.is_super?
  end

  # if we come in with an api_application that's not owned by our api_company
  # then we're a third party api client. this is how towbook and beacon work -
  # the api_application belongs to them, but the api_company / api_user are from
  # the company they are acting on behalf of.
  def is_third_party_api_client?
    api_company.present? && api_application.present? && api_application.owner != api_company
  end

  def is_valid_subscription_viewer?
    if is_third_party_api_client?
      viewer.is_a?(::Company)
    else
      # TODO - we may want to update this?
      true
    end
  end

  def is_valid_mutation_viewer?
    if is_third_party_api_client?
      # for requests via 3rd party apis a user has to be specified
      viewer.is_a?(::User)
    else
      # TODO - we may want to update this?
      true
    end
  end

  def fleet_in_house?
    !!api_company&.fleet_in_house?
  end

  def motor_club?
    !!api_company&.motor_club?
  end

  def api_roles
    Array(self[:api_roles])
  end

  def api_application
    self[:api_application]
  end

  def api_company
    self[:api_company]
  end

  def viewer
    self[:viewer]
  end

end
