# frozen_string_literal: true

module Utils
  module QueryErrors

    def query_errors(query)
      Array(query.static_errors) + Array(query.analysis_errors) + Array(query.context.errors)
    end

  end
end
