# frozen_string_literal: true

module Utils
  module Currency

    def currency_value_or_zero(value, decimal_places: 2)
      currency_value_or_nil((value || 0), decimal_places: decimal_places)
    end

    def currency_value_or_nil(value, decimal_places: 2)
      if value
        "%.#{decimal_places}f" % value.truncate(2)
      else
        nil
      end
    end

  end
end
