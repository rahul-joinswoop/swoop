# frozen_string_literal: true

module Utils
  module Auth

    RESCUE_ADMIN_OR_DISPATCHER_ONLY = -> (roles) do
      roles.include?(:rescue) && (roles.include?(:dispatcher) || roles.include?(:admin))
    end

    NON_CUSTOMER = -> (roles) do
      roles.present? && !roles.include?(:customer)
    end

  end
end
