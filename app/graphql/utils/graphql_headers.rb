# frozen_string_literal: true

module Utils
  module GraphQLHeaders

    VERSION_NOT_PROVIDED  = 'API - version not provided by client'
    PLATFORM_NOT_PROVIDED = 'API - platform not provided by client'
    REFERER_NOT_PROVIDED = 'API - referer not provided by client'

    def user_agent(context)
      return @user_agent if @user_agent

      http_user_agent = headers(context)['HTTP_USER_AGENT']

      if http_user_agent.blank?
        return @user_agent = {}
      end

      @user_agent = UserAgentParser.new(http_user_agent, platform: :api).call
    end

    def headers(context)
      return @headers if @headers
      return @headers = {} unless context

      @headers = context.dig(:headers) || {}
    end

    def platform
      user_agent(context)[:platform] || PLATFORM_NOT_PROVIDED
    end

    def version
      user_agent(context)[:version] || VERSION_NOT_PROVIDED
    end

    def client_or_swoop_version
      user_agent(context)[:client_or_swoop_version] || VERSION_NOT_PROVIDED
    end

    def swoop_custom_platform
      user_agent(context)[:swoop_custom_platform] || PLATFORM_NOT_PROVIDED
    end

    def referer
      headers(context)['HTTP_REFERER'] || REFERER_NOT_PROVIDED
    end

    def mobile_app?
      [UserAgentParser::REACT_IOS, UserAgentParser::REACT_ANDROID].include?(swoop_custom_platform)
    end

  end
end
