# frozen_string_literal: true

class Arguments::AuthorizedArgument < GraphQL::Schema::Argument

  def initialize(*args,
                 required_roles: Utils::Auth::NON_CUSTOMER,
                 required_features: nil,
                 if_company: nil,
                 if_context: nil,
                 **kwargs)
    @required_roles = required_roles
    @required_features = required_features
    @if_company = if_company
    @if_context = if_context
    # Pass on the default args:
    super(*args, **kwargs)
  end

  def to_graphql
    arg_defn = super
    arg_defn.metadata.merge!(
      required_roles: @required_roles,
      required_features: @required_features,
      if_company: @if_company,
      if_context: @if_context
    )
    arg_defn
  end

end
