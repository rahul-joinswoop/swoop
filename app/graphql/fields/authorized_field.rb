# frozen_string_literal: true

class Fields::AuthorizedField < GraphQL::Schema::Field

  argument_class Arguments::AuthorizedArgument

  # Override #initialize to take new arguments:
  def initialize(*args,
                 required_roles: Utils::Auth::NON_CUSTOMER,
                 required_features: nil,
                 if_company: nil,
                 if_context: nil,
                 **kwargs,
                 &block)
    @required_roles = required_roles
    @required_features = required_features
    @if_company = if_company
    @if_context = if_context
    # Pass on the default args:
    super(*args, **kwargs, &block)
  end

  def to_graphql
    field_defn = super # Returns a GraphQL::Field
    field_defn.metadata.merge!(
      required_roles: @required_roles,
      required_features: @required_features,
      if_company: @if_company,
      if_context: @if_context,
    )
    field_defn
  end

  # hack to easily build a new version of this class with different defaults, used by
  # Types::MutationType and Types::SubscriptionType
  def self.with_defaults(_defaults = {})
    klass = Class.new(self) do
      cattr_accessor :defaults
      def initialize(*args, **kwargs, &block)
        defaults.each do |k, v|
          kwargs[k] ||= v
        end
        super(*args, **kwargs, &block)
      end
    end
    klass.defaults = _defaults
    klass
  end

end
