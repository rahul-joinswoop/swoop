# frozen_string_literal: true

module Analyzers
  module AnalysisError

    def analysis_error(message:, problems: [], extensions: {}, irep_node:, path:)
      problems = [problems] unless problems.is_a?(::Array)
      if problems.present?
        extensions[:problems] = problems
      end

      e = GraphQL::AnalysisError.new(
        message,
        ast_node: irep_node.ast_node,
        options: extensions.present? ? { extensions: extensions }.deep_stringify_keys : {},
      )
      e.instance_variable_set :@path, path
      e
    end

    def base_path(irep_node)
      [[irep_node.owner_type.name.downcase, irep_node.parent.name].reject(&:blank?).join(" "), irep_node.name]
    end

  end
end
