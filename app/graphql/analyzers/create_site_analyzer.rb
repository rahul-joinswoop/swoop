# frozen_string_literal: true

# @see https://github.com/rmosolgo/graphql-ruby/blob/master/guides/queries/analysis.md

# Used by CreateSiteMutation
module Analyzers
  class CreateSiteAnalyzer < MutationAnalyzer

    LAT_LNG_OR_GOOGLE_PLACE_ID_OR_ADDRESS_IS_REQUIRED = "One of lat/lng, googlePlaceId or address is required"

    # This is like the `reduce` callback.
    # The return value is passed to the next call as `memo`
    def call_on_subject_node(memo, visit_type, irep_node)
      location = irep_node.arguments.dig(:input, :site, :location)

      unless (location[:lat].present? && location[:lng].present?) ||
            location[:googlePlaceId].present? ||
            location[:address].present?
        memo[:errors].push analysis_error(
          message: LAT_LNG_OR_GOOGLE_PLACE_ID_OR_ADDRESS_IS_REQUIRED,
          problems: { 'explanation' => LAT_LNG_OR_GOOGLE_PLACE_ID_OR_ADDRESS_IS_REQUIRED },
          irep_node: irep_node,
          path: [*base_path(irep_node), 'input', 'site', 'location'],
        )
      end

      memo
    end

  end
end
