# frozen_string_literal: true

module Analyzers
  # When subclassing, name your class following the pattern
  # [MutationName|QueryName]Analyzer so that mutation may be inferred.
  # Additionally you should either implement #call_on_subject_node or
  # override #call
  class Base

    include AnalysisError
    # Called before initializing the Analyzer.
    # This should be false unless the analyzer is applicable to the query.
    # If true, #call will be visited by every node in the query.
    def analyze?(query)
      # for now always true, see ENG-11483
      true
    end

    # Called before the visit.
    # Returns the initial value for `memo`
    def initial_value(query)
      { query: query, context: query.context, errors: [] }
    end

    # Called when we're done the whole visit.
    # The return value may be a GraphQL::AnalysisError (or an array of them).
    # Or, you can use this hook to write to a log, etc
    def final_value(memo)
      return memo[:errors] if memo[:errors].present?
    end

    def call(memo, visit_type, irep_node)
      if !respond_to?(:call_on_subject_node)
        memo
      elsif visiting_subject_node?(visit_type, irep_node)
        call_on_subject_node(memo, visit_type, irep_node)
      else
        memo
      end
    end

    private

    def visiting_subject_node?(visit_type, irep_node)
      return false unless visit_type == :enter

      irep_node&.definition&.name == subject_name.camelize(:lower)
    end

    def subject_name
      self.class.name.demodulize.sub(/Analyzer\z/, '')
    end

  end
end
