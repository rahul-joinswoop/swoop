# frozen_string_literal: true

# @see https://github.com/rmosolgo/graphql-ruby/blob/master/guides/queries/analysis.md

# Used by UpdatePartnerVehicle mutation
module Analyzers
  class UpdatePartnerVehicleAnalyzer < MutationAnalyzer

    NOT_FOUND_ERROR_MESSAGE = "PartnerVehicle not found"
    DRIVER_NOT_AUTHORIZED_ERROR_MESSAGE = "Viewer not authorized to update this PartnerVehicle"

    # This is like the `reduce` callback.
    # The return value is passed to the next call as `memo`
    def call_on_subject_node(memo, visit_type, irep_node)
      partner_vehicle_args = irep_node.arguments.dig(:input, :partner_vehicle)&.to_h || {}

      api_user = memo.dig(:context, :api_user)
      api_company = memo.dig(:context, :api_company)

      begin
        partner_vehicle = api_company.vehicles.find(SomewhatSecureID.load!(partner_vehicle_args[:id]).id)
      rescue SomewhatSecureID::Error, ActiveRecord::RecordNotFound
        problems = {
          'explanation' => NOT_FOUND_ERROR_MESSAGE,
        }
        memo[:errors].push analysis_error(
          message: NOT_FOUND_ERROR_MESSAGE,
          problems: problems,
          irep_node: irep_node,
          path: [*base_path(irep_node), 'input', 'partnerVehicle', 'id'],
        )
        return memo
      end

      # skip this validation if we don't have an api_user - in that case we're coming in as either
      # a company or application through the api and we don't want these restrictions.
      if api_user && api_user.vehicle != partner_vehicle
        problems = {
          'explanation' => DRIVER_NOT_AUTHORIZED_ERROR_MESSAGE,
        }
        memo[:errors].push analysis_error(
          message: DRIVER_NOT_AUTHORIZED_ERROR_MESSAGE,
          problems: problems,
          irep_node: irep_node,
          path: [*base_path(irep_node), 'input', 'partnerVehicle', 'id'],
        )
        return memo
      end

      # If it reaches this point, it means that either:
      # - api_user is a driver and partner_vehicle.driver.id == api_user.to_ssid, or
      # - no api_user but api_company has our partner_vehicle
      #
      # but either way we can let it pass.
      #
      # note: as per business rule drivers can re-assign PartnerVehicles to themselves even if
      # it's already assigned to another Driver.
      memo
    end

  end
end
