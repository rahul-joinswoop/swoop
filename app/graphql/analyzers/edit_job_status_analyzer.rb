# frozen_string_literal: true

module Analyzers
  class EditJobStatusAnalyzer < UpdateJobStatusAnalyzer
  end
end
