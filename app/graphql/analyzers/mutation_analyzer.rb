# frozen_string_literal: true

module Analyzers
  # When subclassing, name your class following the pattern
  # [MutationName]Analyzer so that mutation may be inferred.
  # Additionally you should either implement #call_on_subject_node or
  # override #call
  class MutationAnalyzer < Base

    def analyze?(query)
      query.mutation? && super
    end

  end
end
