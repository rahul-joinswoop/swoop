# frozen_string_literal: true

module Analyzers
  module Validations
    class JobStatusReasonType

      include Interactor
      include AnalysisError
      include ContextMethods

      REASON_REQUIRED_ERROR_MESSAGE = "A reason is required when transitioning a Job to %s"
      INVALID_REASON_ERROR_MESSAGE = "Invalid reason for transitioning a Job to %s"
      INVALID_STATUS_REASON_FOR_JOB_MESSAGE = "This Job does not support this reason"

      def call
        # LOLWTF, there has to be a faster way than this.
        job_status_id = JobStatus.const_get ::Job::STATUSES[irep_node.arguments.dig(:input, :job, :status).to_sym]
        status_reason_type_text = irep_node.arguments.dig(:input, :job, :reason)

        # a - validate that status_reason_type_text is not blank
        if status_reason_type_text.blank?
          msg = REASON_REQUIRED_ERROR_MESSAGE % [JobStatus::ID_MAP[job_status_id]]
          memo[:errors].push analysis_error(
            message: msg,
            problems: {
              'explanation' => msg,
            },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'reason'],
          )
          return
        end

        # if we got a status_reason_type then verify it's valid
        begin
          status_reason_type = ::JobStatusReasonType.find_by!(text: status_reason_type_text, job_status_id: job_status_id)
        rescue ActiveRecord::RecordNotFound
          # b - validate that status_reason_type.job_status is job_status
          msg = INVALID_REASON_ERROR_MESSAGE % [JobStatus::ID_MAP[job_status_id]]
          memo[:errors].push analysis_error(
            message: msg,
            problems: {
              'explanation' => msg,
            },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'reason'],
          )
          return
        end

        # c - validate that the job supports the given status reason
        if !context.job.status_reason_types_by(job_status_id).include?(status_reason_type)
          msg = INVALID_STATUS_REASON_FOR_JOB_MESSAGE
          memo[:errors].push analysis_error(
            message: msg,
            problems: {
              'explanation' => msg,
            },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'reason'],
          )
          return
        end
      end

    end
  end
end
