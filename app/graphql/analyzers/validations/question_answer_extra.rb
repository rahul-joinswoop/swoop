# frozen_string_literal: true

module Analyzers
  module Validations
    class QuestionAnswerExtra

      include Interactor::Organizer

      organize Validations::Question,
               Validations::Answer,
               Validations::Extra

    end
  end
end
