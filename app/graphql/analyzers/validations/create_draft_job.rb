# frozen_string_literal: true

module Analyzers
  module Validations
    class CreateDraftJob

      include Interactor::Organizer

      # we want to use the same validations as CreateJob but with some changes.
      # rather than maintain a separate duplicate list we pull in CreateJob's validations
      # and modify the list for our own usage

      # these are validations we want to skip when creating a draft job
      SKIP_FOR_DRAFT = [
        # we don't use Locations here because we want
        # to skip DropLocation and ServiceLocationType
        Locations,
        # CustomerInfo requires job.customer.[name|phone]
        CustomerInfo,
        # ServiceQuestions will require certain questions based on our service
        ServiceQuestions,
        # ServiceSymptom requires job.service.symptom in certain circumstances
        ServiceSymptom,
        # Vehicle requires various fields on job.vehicle
        Validations::Vehicle,
        # Texts complains if job.texts.sendTextsToPickup is present but job.pickupContact.phone isn't
        Texts,
      ].freeze

      # these are validations we want to add when creating a draft job
      ADD_FOR_DRAFT = [
        # we skip Locations above (and Locations is an organizer) but we do want one
        # interactor from it, LocationInfo, which requires a lat/lng or googlePlaceid for
        # a location entry (TODO - do we really want this? can't we just lookup from the address?)
        LocationInfo,
      ].freeze

      organize Analyzers::Validations::CreateJob.organized
        .reject { |klass| klass.in? SKIP_FOR_DRAFT }
        .concat(ADD_FOR_DRAFT)
        .freeze

    end
  end
end
