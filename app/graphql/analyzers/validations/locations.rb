# frozen_string_literal: true

module Analyzers
  module Validations
    class Locations

      include Interactor::Organizer

      organize LocationInfo,
               DropLocation,
               ServiceLocationType

    end
  end
end
