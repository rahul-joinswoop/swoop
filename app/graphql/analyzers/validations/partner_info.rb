# frozen_string_literal: true

module Analyzers
  module Validations
    class PartnerInfo

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "Partner %s not found"

      # TODO - this probably makes more sense as an Organizer - for the first pass i just wanted to
      # get the initial conversion done.
      def call
        partner = irep_node.arguments.dig(:input, :job, :partner).to_h
        return if partner.blank?

        company = memo.dig(:context, :api_company)

        driver_name = partner.dig(:driver, :name)
        if driver_name
          user = User.search_by_full_name(driver_name)
            .where(company: company)
            .with_role(:driver)
            .not_deleted.first
          if user.blank?
            memo[:errors].push analysis_error(
              message: ERROR_MESSAGE % ['driver'],
              problems: {
                'explanation' => ERROR_MESSAGE % ['driver'],
              },
              irep_node: irep_node,
              path: [*base_path(irep_node), 'input', 'job', 'partner', 'driver'],
            )
          end
        end

        department_name = partner.dig(:department, :name)
        if department_name
          department = ::Department.find_by({ name: department_name, company: company })
          if department.blank?
            memo[:errors].push analysis_error(
              message: ERROR_MESSAGE % ['department'],
              problems: {
                'explanation' => ERROR_MESSAGE % ['department'],
              },
              irep_node: irep_node,
              path: [*base_path(irep_node), 'input', 'job', 'partner', 'department'],
            )
          end
        end

        vehicle_name = partner.dig(:vehicle, :name)
        if vehicle_name
          vehicle = ::RescueVehicle.find_by({ name: vehicle_name, company: company })
          if vehicle.blank?
            memo[:errors].push analysis_error(
              message: ERROR_MESSAGE % ['vehicle'],
              problems: {
                'explanation' => ERROR_MESSAGE % ['vehicle'],
              },
              irep_node: irep_node,
              path: [*base_path(irep_node), 'input', 'job', 'partner', 'vehicle'],
            )
          end
        end

        trailer_name = partner.dig(:trailer, :name)
        if trailer_name
          trailer = ::TrailerVehicle.find_by({ name: trailer_name, company: company })
          if trailer.blank?
            memo[:errors].push analysis_error(
              message: ERROR_MESSAGE % ['trailer'],
              problems: {
                'explanation' => ERROR_MESSAGE % ['trailer'],
              },
              irep_node: irep_node,
              path: [*base_path(irep_node), 'input', 'job', 'partner', 'trailer'],
            )
          end
        end
      end

    end
  end
end
