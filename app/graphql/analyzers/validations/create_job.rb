# frozen_string_literal: true

module Analyzers
  module Validations
    class CreateJob

      include Interactor::Organizer

      organize AccountName,
               Locations,
               PartnerInfo,
               QuestionResults,
               # ServiceQuestions has to come after QuestionResults
               ServiceQuestions,
               CustomerInfo,
               ServiceSymptom,
               Validations::Vehicle,
               ServiceScheduledFor,
               Texts,
               TireChangeService

    end
  end
end
