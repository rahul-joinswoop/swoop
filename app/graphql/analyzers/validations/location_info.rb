# frozen_string_literal: true

module Analyzers
  module Validations
    class LocationInfo

      include Interactor
      include AnalysisError
      include ContextMethods

      EITHER_OR_ERROR_MESSAGE = "Either lat/lng or googlePlaceId is required for %s"
      FLEET_MANAGED_ERROR_MESSAGE = "lat/lng is required"
      def call
        [:dropoffLocation, :serviceLocation].each do |loc|
          location = irep_node.arguments.dig(:input, :job, :location, loc)
          return if location.blank?
          # this is all because of FleetManageJob#validate_drop_location_isnt_empty
          if loc == :dropoffLocation && memo[:context].is_client? && !memo[:context].fleet_in_house? && !memo[:context].motor_club? &&
             location[:lat].blank? && location[:lng].blank?
            memo[:errors].push analysis_error(
              message: FLEET_MANAGED_ERROR_MESSAGE,
              problems: { 'explanation' => FLEET_MANAGED_ERROR_MESSAGE },
              irep_node: irep_node,
              path: [*base_path(irep_node), 'input', 'job', 'location', loc.to_s],
            )
          elsif location[:lat].blank? && location[:lng].blank? && location[:googlePlaceId].blank?
            memo[:errors].push analysis_error(
              message: EITHER_OR_ERROR_MESSAGE % [loc],
              problems: { 'explanation' => EITHER_OR_ERROR_MESSAGE % [loc] },
              irep_node: irep_node,
              path: [*base_path(irep_node), 'input', 'job', 'location', loc.to_s],
            )
          end
        end
      end

    end
  end
end
