# frozen_string_literal: true

module Analyzers
  module Validations
    class JobStatusAccepted

      include Interactor
      include AnalysisError
      include ContextMethods

      ACCEPTED_WITHOUT_BTA_ERROR_MSG = "Accepting a Job should have a bidded ETA, see UpdateJobStatus.eta.bidded"
      BTA_WITHOUT_REASON_ERROR_MSG = "Job requires a reason for the given bidded ETA"

      def call
        # we don't need an eta or status_reason_type_text if the job already has a bidded
        # eta - this means that we've already accepted the job previously
        return if context.job.bta.present?

        # we also don't need an eta or status_reason_type_text if the job has already been in
        # accepted
        return if context.job.audit_job_status_accepted.present?

        # and we don't need an eta or status_reason_type_text is a job is scheduled
        return if context.job.scheduled_for.present?

        bidded_eta = irep_node.arguments.dig(:input, :job, :eta, :bidded)

        # bidded_eta should not be blank
        if bidded_eta.blank?
          msg = ACCEPTED_WITHOUT_BTA_ERROR_MSG
          memo[:errors].push analysis_error(
            message: msg,
            problems: {
              'explanation' => msg,
            },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'eta', 'bidded'],
          )
          return
        end

        status_reason_type_text = irep_node.arguments.dig(:input, :job, :reason)

        if context.job.max_bidded_eta_without_explanation
          # if there's a max eta without explanation then we require an explanation
          bidded_eta_datetime = bidded_eta.to_datetime
          bidded_eta_in_minutes = ((bidded_eta_datetime - DateTime.current) * 24 * 60).to_i

          if bidded_eta_in_minutes > context.job.max_bidded_eta_without_explanation &&
            context.job.status_reason_type_required?(JobStatus::ACCEPTED)

            # a - validate that status_reason_type_text is not blank
            if status_reason_type_text.blank?
              msg = BTA_WITHOUT_REASON_ERROR_MSG
              memo[:errors].push analysis_error(
                message: msg,
                problems: {
                  'explanation' => msg,
                },
                irep_node: irep_node,
                path: [*base_path(irep_node), 'input', 'job', 'reason'],
              )

              return
            end
          end
        end

        Validations::JobStatusReasonType.call(context) if status_reason_type_text
      end

    end
  end
end
