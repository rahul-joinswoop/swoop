# frozen_string_literal: true

module Analyzers
  module Validations
    class ServiceQuestions

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      ERROR_MESSAGE = "Missing required %s for Service %s"
      ERROR_EXPLANATION = "Question %s is required"

      def call
        # make sure that all the questions required for a selected service
        # have been answered

        # skip in certain circumstances
        return if AnswersEditable.call(context.to_h).failure?

        # TODO - this is not working right! it only checks to see if theres a question_result entry for
        # all required questions - it doesn't verify that there's an actual answer. not sure if it's
        # possible to end up in this state by using the graphql api or not, need to investigate

        if existing_job?
          service_code = job&.service_code
        else
          # this is the service input object - this is a required field so
          # if it's not there validation would have already exploded and we
          # wouldn't have gotten here.
          service = irep_node.arguments.dig(:input, :job, :service)

          # and yet, let's be paranoid and verify this exists
          return if service.blank?

          # this is our ServiceCode model
          service_code = service.dig(:name)
        end

        # can't do anything without a service code
        return if service_code.blank?

        # make sure we have all the required questions for our service_code
        # and add errors if we don't

        # TODO - if the find_by! fails we raise an exception, what happens?

        ids = context.question_results.map do |qr|
          if qr.is_a?(Hash)
            qr[:question]
          else
            qr.question
          end.id
        end

        problems = Resolvers::CompaniesServices
          .new(object: memo[:context][:api_company], context: memo[:context])
          .resolve
          .find_by!(service_code: service_code)
          .questions
          .where.not(id: ids).
          # using string keys on purpose here because that's what the graphql
          # gem does internally for validation exceptions and i'm trying to
          # duplicate that interface
          map { |q| { 'explanation' => ERROR_EXPLANATION % [q.question.inspect] } }

        if problems.present?
          path = [*base_path(irep_node), 'input', 'job', 'service', 'answers']
          path.delete('input') if existing_job?

          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE % ["Question".pluralize(problems.length), service_code.name.inspect],
            problems: problems,
            irep_node: irep_node,
            path: path,
          )

        end
      end

    end
  end
end
