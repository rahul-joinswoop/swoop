# frozen_string_literal: true

module Analyzers
  module Validations
    class UpdateJob

      include Interactor::Organizer

      organize Validations::Job, JobEditable, Validations::AccountName, PartnerInfo

    end
  end
end
