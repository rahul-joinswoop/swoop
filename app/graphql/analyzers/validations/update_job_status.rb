# frozen_string_literal: true

module Analyzers
  module Validations
    class UpdateJobStatus

      include Interactor
      include ContextMethods

      ERROR_MESSAGE = "A %s cannot transition a Job from %s to %s"
      def self.error_message(company, from, to)
        ERROR_MESSAGE % [company.graphql_type, from.inspect, to.inspect]
      end

      def call
        # if any of these failed we bail because there's no reason to go on,
        # hence the #call!
        Validations::Job.call! context
        # TODO - enable this once specs are fixed
        # Validations::JobEditable.call! context

        if context.job.auto_assigning?
          JobStatusAutoAssigning.call!(context)
        else
          JobStatusNotAutoAssigning.call!(context)
        end

        # otherwise we're allowed (in theory) to make the status change we're attempting
        # but there may be other reasons below to prevent it.
        case irep_node.arguments.dig(:input, :job, :status)
        when 'pending'
          JobStatusPending.call(context)
        when 'accepted'
          JobStatusAccepted.call(context)
        when 'rejected'
          JobStatusRejected.call(context)
        when 'canceled'
          JobStatusCanceled.call(context)
        when 'goa'
          JobStatusGoa.call(context)
        end
      end

    end
  end
end
