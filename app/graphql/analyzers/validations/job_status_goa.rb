# frozen_string_literal: true

module Analyzers
  module Validations
    class JobStatusGoa

      include Interactor
      include AnalysisError
      include ContextMethods

      PARTNER_CANNOT_GOA_FLEET_MANAGED_JOB_MESSAGE = "This Job cannot be transitioned to GOA via the API. Instead, please contact Swoop: #{::Job::DEFAULT_SUPPORT_PHONE}"

      def call
        # clients can goa a job without a reason, even if a reason is required.
        # specifically - clients don't have access to the reason input field so we
        # short-circuit here
        return if memo.dig(:context).is_client?

        if context.job.is_a?(::FleetManagedJob) && memo.dig(:context).is_partner?
          msg = PARTNER_CANNOT_GOA_FLEET_MANAGED_JOB_MESSAGE
          memo[:errors].push analysis_error(
            message: msg,
            problems: {
              'explanation' => msg,
            },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'status'],
          )
          return
        end

        if context.job.status_reason_type_required?(JobStatus::GOA) ||
           irep_node.arguments.dig(:input, :job, :reason)
          Validations::JobStatusReasonType.call(context)
        end
      end

    end
  end
end
