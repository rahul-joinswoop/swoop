# frozen_string_literal: true

module Analyzers
  module Validations
    module ContextMethods

      extend Forwardable

      def_delegators :@context, :irep_node, :memo, :visit_type

    end
  end
end
