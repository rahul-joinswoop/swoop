# frozen_string_literal: true

module Analyzers
  module Validations
    class ServiceLocationType

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      # TODO - this error message makes no sense?
      ERROR_MESSAGE = "Argument 'locationType' on InputObject 'LocationInput' is required"

      def call
        return unless memo.dig(:context).is_client?
        service_location_type = begin
          if existing_job?
            job&.service_location&.location_type
          else
            irep_node.arguments.dig(:input, :job, :location, :serviceLocation, :locationType)
          end
        end

        if service_location_type.blank?
          path = [*base_path(irep_node), 'input', 'job', 'location', 'serviceLocation']
          path.delete('input') if existing_job?

          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE,
            problems: { 'explanation' => ERROR_MESSAGE },
            irep_node: irep_node,
            path: path,
          )
        end
      end

    end
  end
end
