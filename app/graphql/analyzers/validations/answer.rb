# frozen_string_literal: true

module Analyzers
  module Validations
    class Answer

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "%s is not a valid Answer"

      def call
        # make sure our answer is valid
        context.answer = context.question.fetch_answer!(context.input[:answer])
      rescue ActiveRecord::RecordNotFound
        path = [*base_path(irep_node), 'input', 'job', 'service', 'answers', context.index]
        memo[:errors].push analysis_error(
          message: ERROR_MESSAGE % context.input[:answer].inspect,
          irep_node: irep_node,
          path: path,
          extensions: {
            code: "argumentLiteralsIncompatible",
            typeName: "CoercionError",
          },
        )
        context.fail! error: "invalid answer"
      end

    end
  end
end
