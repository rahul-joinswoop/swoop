# frozen_string_literal: true

module Analyzers
  module Validations
    class ServiceName

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      ERROR_MESSAGE = 'Service Name is required'

      def call
        # this is only ever called on an existing job - for new jobs we catch this in the schema.
        if existing_job? && job.service_code.blank?
          msg = ERROR_MESSAGE
          memo[:errors].push analysis_error(
            message: msg,
            problems: { 'explanation' => msg },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'job', 'service'],
          )
        end
      end

    end
  end
end
