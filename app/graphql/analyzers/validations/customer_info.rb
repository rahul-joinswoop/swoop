# frozen_string_literal: true

module Analyzers
  module Validations
    class CustomerInfo

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      CUSTOMER_NAME_ERROR_MESSAGE = "Customer name is required"
      CUSTOMER_PHONE_ERROR_MESSAGE = "Customer phone is required"

      # TODO - should this be an organizer?
      def call
        return if memo[:context].has_feature?(Feature::DONT_REQUIRE_CUSTOMER_NAME_AND_PHONE)

        customer_name = begin
          if existing_job?
            job&.customer&.name
          else
            # handle deprecated :fullName input
            irep_node.arguments.dig(:input, :job, :customer, :name) ||
            irep_node.arguments.dig(:input, :job, :customer, :fullName)
          end
        end

        if customer_name.blank?
          path = [*base_path(irep_node), 'input', 'job', 'customer']
          path.delete('input') if existing_job?

          memo[:errors].push analysis_error(
            message: CUSTOMER_NAME_ERROR_MESSAGE,
            problems: { 'explanation' => CUSTOMER_NAME_ERROR_MESSAGE },
            irep_node: irep_node,
            path: path,
          )
        end

        customer_phone = begin
          if existing_job?
            job&.customer&.phone
          else
            irep_node.arguments.dig(:input, :job, :customer, :phone)
          end
        end

        if customer_phone.blank?
          path = [*base_path(irep_node), 'input', 'job', 'customer']
          path.delete('input') if existing_job?

          memo[:errors].push analysis_error(
            message: CUSTOMER_PHONE_ERROR_MESSAGE,
            problems: { 'explanation' => CUSTOMER_PHONE_ERROR_MESSAGE },
            irep_node: irep_node,
            path: path,
          )
        end
      end

    end
  end
end
