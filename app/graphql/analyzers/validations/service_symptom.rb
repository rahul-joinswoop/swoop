# frozen_string_literal: true

module Analyzers
  module Validations
    class ServiceSymptom

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      ERROR_MESSAGE = "Argument 'symptom' on InputObject 'ServiceInput' is required"

      def call
        return unless memo.dig(:context).is_client? && memo.dig(:context).has_feature?(::Feature::SHOW_SYMPTOMS)
        symptom = begin
          if existing_job?
            job.symptom
          else
            irep_node.arguments.dig(:input, :job, :service, :symptom)
          end
        end
        if symptom.blank?
          path = [*base_path(irep_node), 'input', 'job', 'service']
          path.delete('input') if existing_job?

          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE,
            problems: {
              'explanation' => ERROR_MESSAGE,
            },
            irep_node: irep_node,
            path: path,
          )
        end
      end

    end
  end
end
