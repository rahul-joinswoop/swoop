# frozen_string_literal: true

module Analyzers
  module Validations
    class Job

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "Couldn't find Job with id=%s"

      def call
        job_id = irep_node.arguments.dig(:input, :job, :id)
        job = SomewhatSecureID.load job_id
        # bail if job is blank (ie, our id is invalid)
        if job.blank?
          context.fail!(error: ERROR_MESSAGE % [job_id.inspect])
        else
          # if we found a job add it to our context so we can use it later
          context.job = job
        end
      end

    end
  end
end
