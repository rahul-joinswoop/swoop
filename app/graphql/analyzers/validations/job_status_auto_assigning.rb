# frozen_string_literal: true

module Analyzers
  module Validations
    class JobStatusAutoAssigning

      include Interactor
      include AnalysisError
      include ContextMethods

      LEGAL_AUTO_ASSIGNING_STATES = ['Accepted', 'Rejected'].freeze

      def call
        # skip if we're not auto-assigning
        return unless context.job.auto_assigning?
        status = ::Job.statuses[irep_node.arguments.dig(:input, :job, :status)]
        if (memo.dig(:context).is_partner? && !LEGAL_AUTO_ASSIGNING_STATES.include?(status)) ||
          (memo.dig(:context).is_client? || memo.dig(:context).is_super?)
          msg = Validations::UpdateJobStatus.error_message(memo.dig(:context, :api_company), ::Job.statuses[context.job.status], status)
          memo[:errors].push analysis_error(
            message: msg,
            problems: { 'explanation' => msg },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'status'],
          )
          context.fail! error: msg
        end
      end

    end
  end
end
