# frozen_string_literal: true

module Analyzers
  module Validations
    class Extra

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "Missing required extra Answer for Question %s"

      def call
        # make sure our answer is valid
        context.extra = context.input[:extra]

        if context.extra.blank? && context.answer.needs_info?
          path = [*base_path(irep_node), 'input', 'job', 'service', 'answers', context.index]
          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE % context.question.question.inspect,
            irep_node: irep_node,
            path: path
          )
          context.fail! error: "missing required extra"
        end
      end

    end
  end
end
