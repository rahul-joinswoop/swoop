# frozen_string_literal: true

module Analyzers
  module Validations
    class JobEditable

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "Job is not editable by current Viewer"

      def call
        unless Resolvers::Editable
            .new(object: context.job, context: memo.dig(:context) || {})
            .resolve(args: irep_node.arguments.dig(:input, :job).to_h.deeper_dup)
          msg = ERROR_MESSAGE
          memo[:errors].push analysis_error(
            message: msg,
            problems: {
              'explanation' => msg,
            },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job'],
          )
          # we want to fail here so that we don't run any more Validations
          context.fail! error: msg
        end
      end

    end
  end
end
