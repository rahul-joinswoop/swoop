# frozen_string_literal: true

module Analyzers
  module Validations
    class ServiceLocation

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      ERROR_MESSAGE = 'Service Location is required'

      def call
        # this is only ever called on an existing job - for new jobs we catch this in the schema.
        if existing_job? && job.service_location.blank?
          msg = ERROR_MESSAGE
          memo[:errors].push analysis_error(
            message: msg,
            problems: { 'explanation' => msg },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'job', 'location'],
          )
        end
      end

    end
  end
end
