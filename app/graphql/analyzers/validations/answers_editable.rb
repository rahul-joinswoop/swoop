# frozen_string_literal: true

module Analyzers
  module Validations
    class AnswersEditable

      include Interactor
      include ContextMethods

      ERROR_MESSAGE = "questions and answers not editable"

      def call
        # We do not support questions and answers editing by Rescue companies.
        # We also do not support it if FleetCompany does not have Questions feature enabled.
        #
        # In both cases we hide 'answers' field from job input, so we're safe to bypass it here.
        if memo[:context].is_partner? || (memo[:context].is_client? && !memo[:context].has_feature?(Feature::QUESTIONS))
          context.fail! error: ERROR_MESSAGE
        end
      end

    end
  end
end
