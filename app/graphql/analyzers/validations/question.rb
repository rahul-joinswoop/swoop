# frozen_string_literal: true

module Analyzers
  module Validations
    class Question

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "%s is not a valid Question"

      # make sure our question is vaid
      def call
        context.question = ::Question.fetch_by_question!(context.input[:question])
      rescue ActiveRecord::RecordNotFound
        path = [*base_path(irep_node), 'input', 'job', 'service', 'answers', context.index]
        memo[:errors].push analysis_error(
          message: ERROR_MESSAGE % context.input[:question].inspect,
          irep_node: irep_node,
          path: path,
          extensions: {
            code: "argumentLiteralsIncompatible",
            typeName: "CoercionError",
          },
        )
        context.fail! error: "invalid question"
      end

    end
  end
end
