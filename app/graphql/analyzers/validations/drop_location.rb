# frozen_string_literal: true

module Analyzers
  module Validations
    class DropLocation

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      ERROR_MESSAGE = "Missing 'dropoffLocation' for Service %s"
      ERROR_EXPLANATION = "Service %s requires a 'dropoffLocation'"

      # make sure we have a dropoffLocation if our ServiceCode requires it
      def call
        # this validation runs when creating a non-draft job and when moving a draft job -> pending.
        # because of this we need to validate either our input _or_ or existing job, depending. sorry.
        service_code = begin
          if existing_job?
            job.service_code
          else
            irep_node.arguments.dig(:input, :job, :service, :name)
          end
        end

        dropoff_location = begin
          if existing_job?
            job.drop_location
          else
            irep_node.arguments.dig(:input, :job, :location, :dropoffLocation)
          end
        end

        if service_code.present? &&
          dropoff_location.blank? &&
          ((memo.dig(:context).is_partner? && service_code&.is_rescue_drop_location_code?) ||
           (!memo.dig(:context).is_partner? && service_code&.is_fleet_drop_location_code?))

          # remove 'input' from our path if we're dealing with an existing job
          path = [*base_path(irep_node), 'input', 'job', 'location']
          path.delete('input') if existing_job?

          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE % [service_code.name.inspect],
            problems: { 'explanation' => ERROR_EXPLANATION % [service_code.name.inspect] },
            irep_node: irep_node,
            path: path,
          )
        end
      end

    end
  end
end
