# frozen_string_literal: true

module Analyzers
  module Validations
    class JobStatusNotAutoAssigning

      include Interactor
      include AnalysisError
      include ContextMethods

      def call
        # if we're auto-assigning then we skip this and call a different chunk of logic
        return if context.job.auto_assigning?

        # TODO - is this lowercased, uppsercased, underscored?
        status = ::Job.statuses[irep_node.arguments.dig(:input, :job, :status)]

        if (memo.dig(:context).is_partner? && !PartnerUpdateJobState::LEGAL_STATES.include?(status)) ||
          (memo.dig(:context).is_client? && !FleetUpdateJobState::LEGAL_STATES.include?(status)) ||
          (memo.dig(:context).is_super? && !SwoopUpdateJobState::LEGAL_STATES.include?(status))
          msg = Validations::UpdateJobStatus.error_message(memo.dig(:context, :api_company), ::Job.statuses[context.job.status], status)
          memo[:errors].push analysis_error(
            message: msg,
            problems: { 'explanation' => msg },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'status'],
          )
          context.fail! error: msg
        end
      end

    end
  end
end
