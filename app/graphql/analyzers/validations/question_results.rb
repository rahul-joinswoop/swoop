# frozen_string_literal: true

module Analyzers
  module Validations
    class QuestionResults

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      ERROR_MESSAGE = "invalid Answers input"

      def call
        # skip in certain circumstances
        return if AnswersEditable.call(context.to_h).failure?

        # setup context.question_results
        context.question_results = if existing_job?
                                     context.job.question_results.includes(:question).to_a
                                   else
                                     []
                                   end
        irep_node
          .arguments
          .dig(:input, :job, :service, :answers)
          .to_a
          .map
          .with_index do |input, index|
            Validations::QuestionAnswerExtra.call(
              memo: memo,
              visit_type: visit_type,
              irep_node: irep_node,
              index: index,
              input: input
            ).tap do |result|
              if result.success?
                context.question_results << {
                  question: result.question,
                  answer: result.answer,
                  extra: result.extra,
                }
              end
            end
          end
      end

    end
  end
end
