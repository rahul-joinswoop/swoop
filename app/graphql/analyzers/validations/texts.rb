# frozen_string_literal: true

module Analyzers
  module Validations
    class Texts

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      ERROR_MESSAGE = "job.texts.sendTextsToPickup requires job.pickupContact.phone"

      def call
        send_texts_to_pickup = begin
          if existing_job?
            job.send_sms_to_pickup
          else
            irep_node.arguments.dig(:input, :job, :texts, :send_texts_to_pickup)
          end
        end

        pickup_contact_phone = begin
          if existing_job?
            job&.pickup_contact&.phone
          else
            irep_node.arguments.dig(:input, :job, :location, :pickupContact, :phone)
          end
        end

        # TODO - better error messages
        if send_texts_to_pickup && pickup_contact_phone.blank?
          path = [*base_path(irep_node), 'input', 'job', 'pickupContact']
          path.delete('input') if existing_job?

          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE,
            problems: { 'explanation' => ERROR_MESSAGE },
            irep_node: irep_node,
            path: path,
          )
        end

        # TODO do something if we don't have a customer phone
        # (['sendLocation', 'sendEtaUpdates', 'sendDriverTracking', 'sendReview'] & texts.keys).each do |key|
        # end
      end

    end
  end
end
