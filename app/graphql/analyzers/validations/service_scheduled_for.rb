# frozen_string_literal: true

module Analyzers
  module Validations
    class ServiceScheduledFor

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "Job must be scheduled for at least 2 hours in the future"

      def call
        scheduled_for = irep_node.arguments.dig(:input, :job, :service, :scheduledFor)
        return if scheduled_for.blank?

        if scheduled_for < (Time.current + 2.hours)
          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE,
            problems: { 'explanation' => ERROR_MESSAGE },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'service', 'scheduledFor'],
          )
        end
      end

    end
  end
end
