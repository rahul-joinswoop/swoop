# frozen_string_literal: true

module Analyzers
  module Validations
    class Vehicle

      include Interactor
      include AnalysisError
      include ContextMethods
      include ExistingJob

      ERROR_MESSAGE = "Argument %s on InputObject 'VehicleInput' is required"

      def call
        # we don't require partner companies to provide this for RescueJobs
        return unless memo[:context].is_client?

        [:make, :model, :color].each do |arg|
          value = begin
            if existing_job?
              job&.driver&.vehicle&.try(arg)
            else
              irep_node.arguments.dig(:input, :job, :vehicle, arg)
            end
          end

          if value.blank?
            path = [*base_path(irep_node), 'input', 'job', 'vehicle']
            path.delete('input') if existing_job?

            msg = ERROR_MESSAGE % [arg.to_s.inspect]

            memo[:errors].push analysis_error(
              message: msg,
              problems: {
                'explanation' => msg,
              },
              irep_node: irep_node,
              path: path,
            )
          end
        end
      end

    end
  end
end
