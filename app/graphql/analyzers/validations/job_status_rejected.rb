# frozen_string_literal: true

module Analyzers
  module Validations
    class JobStatusRejected

      include Interactor
      include AnalysisError
      include ContextMethods

      NOTES_REQUIRED_FOR_REJECTED_OTHER_MESSAGE = "Rejecting a Job with 'Other' requires additional notes"

      def call
        Validations::JobStatusReasonType.call(context)

        if irep_node.arguments.dig(:input, :job, :reason) == 'Other' &&
           irep_node.arguments.dig(:input, :job, :notes, :rejected).blank?
          msg = NOTES_REQUIRED_FOR_REJECTED_OTHER_MESSAGE
          memo[:errors].push analysis_error(
            message: msg,
            problems: {
              'explanation' => msg,
            },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'notes', 'rejected'],
          )
        end
      end

    end
  end
end
