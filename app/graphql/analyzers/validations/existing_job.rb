# frozen_string_literal: true

module Analyzers
  module Validations
    module ExistingJob

      # helper methods for validations that run on input _or_
      # an existing job
      def job
        @context&.job
      end

      def existing_job?
        job.present?
      end

    end
  end
end
