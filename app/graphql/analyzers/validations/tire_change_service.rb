# frozen_string_literal: true

module Analyzers
  module Validations
    class TireChangeService

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "Service must be Tow"
      NO_SPARE_EXPLANATION = "Driver cannot provide a spare tire."
      MULTI_FLAT_EXPLANATION = "Multiple flat tires cannot be changed."

      def call
        service = irep_node.arguments.dig(:input, :job, :service)
        return unless service

        service_code = service.dig(:name)
        return unless service_code&.name == ServiceCode::TIRE_CHANGE

        problems = []

        (problems << { 'explanation' => NO_SPARE_EXPLANATION }) if context.question_results&.any? do |qa|
          qa[:question]&.question == ::Question::HAVE_A_SPARE &&
          qa[:answer]&.answer == ::Answer::NO
        end

        (problems << { 'explanation' => MULTI_FLAT_EXPLANATION }) if context.question_results&.any? do |qa|
          qa[:question]&.question == ::Question::WHICH_TIRE &&
          qa[:answer]&.answer == ::Answer::DAMAGED_TIRE_MULTIPLE
        end

        if problems.present?
          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE,
            problems: problems,
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'service', 'name'],
          )
        end
      end

    end
  end
end
