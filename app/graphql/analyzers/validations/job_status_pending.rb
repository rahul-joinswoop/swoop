# frozen_string_literal: true

module Analyzers
  module Validations
    class JobStatusPending

      include Interactor::Organizer

      organize ServiceName,
               ServiceLocation,
               # we have to run this so that context.question_results is populated
               QuestionResults,
               # these are all the validations we didn't call when we created our new draft job
               # that we actually want to call now
               *(Validations::CreateJob.deep_organized - Validations::CreateDraftJob.deep_organized)

    end
  end
end
