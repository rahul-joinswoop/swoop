# frozen_string_literal: true

module Analyzers
  module Validations
    class AccountName

      include Interactor
      include AnalysisError
      include ContextMethods

      ERROR_MESSAGE = "Invalid Account name %s"

      def call
        account_name = irep_node.arguments.dig(:input, :job, :account, :name)
        return unless account_name
        begin
          ::Account.find_by!(company: memo[:context][:api_company], name: account_name)
        rescue ActiveRecord::RecordNotFound
          memo[:errors].push analysis_error(
            message: ERROR_MESSAGE % [account_name.inspect],
            problems: {
              'explanation' => ERROR_MESSAGE % [account_name.inspect],
            },
            irep_node: irep_node,
            path: [*base_path(irep_node), 'input', 'job', 'account', 'name'],
          )
        end
      end

    end
  end
end
