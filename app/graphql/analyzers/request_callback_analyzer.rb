# frozen_string_literal: true

# @see https://github.com/rmosolgo/graphql-ruby/blob/master/guides/queries/analysis.md

# Used by requestMoreTime mutation
module Analyzers
  class RequestCallbackAnalyzer < MutationAnalyzer

    RSC_ISSC_JOBS_ONLY_ERROR_MESSAGE = "Job does not support requesting a callback"

    # This is like the `reduce` callback.
    # The return value is passed to the next call as `memo`
    def call_on_subject_node(memo, visit_type, irep_node)
      job = SomewhatSecureID.load! irep_node.arguments.dig(:input, :job, :id)
      unless job.can_request_callback?
        problems = {
          'explanation' => RSC_ISSC_JOBS_ONLY_ERROR_MESSAGE,
        }
        memo[:errors].push analysis_error(
          path: [*base_path(irep_node), 'input', 'job', 'id'],
          message: RSC_ISSC_JOBS_ONLY_ERROR_MESSAGE,
          problems: problems,
          irep_node: irep_node,
        )
      end

      memo
    end

  end
end
