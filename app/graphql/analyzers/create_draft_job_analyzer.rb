# frozen_string_literal: true

module Analyzers
  class CreateDraftJobAnalyzer < MutationAnalyzer

    # This is like the `reduce` callback.
    # The return value is passed to the next call as `memo`
    def call_on_subject_node(memo, visit_type, irep_node)
      Validations::CreateDraftJob
        .call(memo: memo, visit_type: visit_type, irep_node: irep_node)
        .memo
    end

  end
end
