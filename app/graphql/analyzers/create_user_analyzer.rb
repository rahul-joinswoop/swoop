# frozen_string_literal: true

# @see https://github.com/rmosolgo/graphql-ruby/blob/master/guides/queries/analysis.md
# Used by CreateUser mutation
module Analyzers
  class CreateUserAnalyzer < MutationAnalyzer

    # This is like the `reduce` callback.
    # The return value is passed to the next call as `memo`

    INVALID_ROLES_MESSAGE = "Invalid Roles"
    INVALID_ROLE_PROBLEM = "%s is not a valid Role"

    def call_on_subject_node(memo, visit_type, irep_node)
      user_args = irep_node.arguments.dig(:input, :user)&.to_h || {}
      roles = Set[*(user_args[:roles] || [])]
      allowable_roles = case memo[:context][:api_company]
                        when RescueCompany
                          Set[*SwoopAuthenticator::ALL_RESCUE_ROLES]
                        when FleetCompany
                          Set[*SwoopAuthenticator::ALL_FLEET_ROLES]
                        else
                          # TODO - implement this
                          Rollbar.error "Analyzers::CreateUserAnalyzer: fell through in allowable_roles with Company(#{memo[:context][:api_company].id})"
                          Set.new
                        end

      unless allowable_roles.superset?(roles)
        extra_roles = roles - allowable_roles
        problems = extra_roles.map { |r| INVALID_ROLE_PROBLEM % [r] }
        memo[:errors].push analysis_error(
          message: INVALID_ROLES_MESSAGE,
          problems: problems,
          irep_node: irep_node,
          path: [*base_path(irep_node), 'input', 'user', 'roles'],
        )
      end
      memo
    end

  end
end
