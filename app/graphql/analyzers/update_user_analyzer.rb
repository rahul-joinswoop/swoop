# frozen_string_literal: true

# @see https://github.com/rmosolgo/graphql-ruby/blob/master/guides/queries/analysis.md
# Used by UpdateUser mutation
module Analyzers
  class UpdateUserAnalyzer < MutationAnalyzer

    USER_NOT_AUTHORIZED_ERROR_MESSAGE = "Viewer is not authorized to update this User"

    VEHICLE_ID_AND_NAME_ERROR_MESSAGE = "Cannot update PartnerVehicle by both name and id"

    # This is like the `reduce` callback.
    # The return value is passed to the next call as `memo`
    def call_on_subject_node(memo, visit_type, irep_node)
      user_args = irep_node.arguments.dig(:input, :user)&.to_h || {}
      user_uuid = user_args.dig(:id)

      api_user = memo.dig(:context, :api_user)

      # make sure that Users can only update themselves
      if user_uuid != api_user&.to_ssid
        problems = {
          'explanation' => USER_NOT_AUTHORIZED_ERROR_MESSAGE,
        }
        memo[:errors].push analysis_error(
          message: USER_NOT_AUTHORIZED_ERROR_MESSAGE,
          problems: problems,
          irep_node: irep_node,
          path: [*base_path(irep_node), 'input', 'user', 'id'],
        )
      end

      # make sure that vehicle.id and vehicle.name aren't both provided
      vehicle_args = user_args.dig(:vehicle) || {}
      if vehicle_args.key?(:id) && vehicle_args.key?(:name)
        problems = {
          'explanation' => VEHICLE_ID_AND_NAME_ERROR_MESSAGE,
        }
        memo[:errors].push analysis_error(
          message: VEHICLE_ID_AND_NAME_ERROR_MESSAGE,
          problems: problems,
          irep_node: irep_node,
          path: [*base_path(irep_node), 'input', 'user', 'vehicle'],
        )
      end

      memo
    end

  end
end
