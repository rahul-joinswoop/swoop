# frozen_string_literal: true

# @see https://github.com/rmosolgo/graphql-ruby/blob/master/guides/queries/analysis.md

# Used by UpdatePartnerVehicle mutation
module Analyzers
  class CreatePartnerVehicleAnalyzer < MutationAnalyzer

    AUTHORIZED_ROLES = %w(admin dispatcher driver).freeze
    NOT_AUTHORIZED_ERROR_MESSAGE = 'This account is not authorized to create partner vehicles.'

    def call_on_subject_node(memo, visit_type, irep_node)
      api_roles = memo.dig(:context, :api_roles)

      if (api_roles & AUTHORIZED_ROLES).empty?
        problems = { 'explanation' => NOT_AUTHORIZED_ERROR_MESSAGE }
        memo[:errors].push analysis_error(
          message: NOT_AUTHORIZED_ERROR_MESSAGE,
          problems: problems,
          irep_node: irep_node,
          path: base_path(irep_node),
        )
      end

      memo
    end

  end
end
