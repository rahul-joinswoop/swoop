# frozen_string_literal: true

class Encoders::LegacySecureEncoder < Encoders::SecureEncoder

  class << self

    # this uses a separator to split incoming values which we don't use anymore
    DEFAULT_SEPARATOR = "-" unless defined?(DEFAULT_SEPARATOR)

    # this returns back an array which was the old behavior
    def decode(opaque_string, nonce: false)
      cipher.decrypt(opaque_string)
    rescue StandardError
      GraphQL::Pro::Encoder::DecodeFailed
    end

    private

    def cipher
      @cipher ||= LegacyBase62Encryptor.new(key: key, iv: iv)
    end

  end

end
