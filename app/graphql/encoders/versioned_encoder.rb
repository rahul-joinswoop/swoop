# frozen_string_literal: true

Encoders::VersionedEncoder = GraphQL::Pro::Encoder.versioned(
  Encoders::SecureEncoder,
  Encoders::LegacySecureEncoder,
)
