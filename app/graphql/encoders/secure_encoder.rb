# frozen_string_literal: true

class Encoders::SecureEncoder

  # these actually use graphql-pro's internal api which takes in and returns a
  # string of json data so the separator isn't needed - this is a way better idea
  # than what we do in base62_encryptor because it removes the need for the
  # separator altogether.
  class << self

    def encode(string_data, nonce: false)
      cipher.encrypt string_data
    end

    def decode(opaque_string, nonce: false)
      # just return the first entry because there won't be any others
      decoded = cipher.decrypt(opaque_string)
      # TODO - would be cool if we could cache this result since we need it
      # elsewhere but here we're just verifying that this is JSON and not a regular
      # string. if the json decode fails we raise GraphQL::Pro::Encoder::DecodeFailed
      # and our legacy parser kicks in
      JSON.parse! decoded
      decoded

    #  decoded.length == 1 ? decoded.first : GraphQL::Pro::Encoder::DecodeFailed
    rescue StandardError
      GraphQL::Pro::Encoder::DecodeFailed
    end

    private

    def cipher
      @cipher ||= Base62Encryptor.new(key: key, iv: iv)
    end

    # for now we use the SomewhatSecureID key/iv ENV values as the default but maybe
    # in the future we use separate keys/ids everywhere? SomewhatSecureID is still
    # providing these opts explicitly so changing it here wouldn't break it.

    def key
      @key ||= ENV.fetch(
        'SSID_KEY',
        ::Rails.application.key_generator.generate_key('somewhat_secure_id.key', 32).unpack("H*").first
      )
    end

    def iv
      @iv ||= ENV.fetch(
        'SSID_IV',
        ::Rails.application.key_generator.generate_key('somewhat_secure_id.iv', 16).unpack("H*").first
      )
    end

  end

end
