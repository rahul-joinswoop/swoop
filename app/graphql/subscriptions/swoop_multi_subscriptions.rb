# frozen_string_literal: true

require 'forwardable'

# SwoopMultiSubscriptions is a proxy class that implements the GrapQL::Subscriptions api. it sends all calls through to
# multiple subscription classes (in this case, SwoopAblySubscriptions and SwoopWebhookSubscriptions).

# we have to inherit from GraphQL::Pro::Subscriptions or else the web interface to subscriptions won't work correctly.
class Subscriptions::SwoopMultiSubscriptions < GraphQL::Pro::Subscriptions

  include UUID
  extend Forwardable
  attr_accessor :swoop_ably_subscriptions, :swoop_webhook_subscriptions
  def_delegators :@swoop_ably_subscriptions, :ably, :topics

  # @see {Subscriptions#initialize} for options, concrete implementations may add options.
  def self.use(defn, options = {})
    schema = defn.target
    options[:schema] = schema
    # we need this here because the graphql pro dashboard expects to be able to access the ably subscriptions
    # via schema.subscriptions

    schema.subscriptions = new(options)
    instrumentation = GraphQL::Subscriptions::Instrumentation.new(schema: schema)
    defn.instrument(:field, instrumentation)
    defn.instrument(:query, instrumentation)
    nil
  end

  # @param schema [Class] the GraphQL schema this manager belongs to
  def initialize(schema:, **rest)
    @schema = schema
    @swoop_ably_subscriptions = Subscriptions::SwoopAblySubscriptions.new(schema: schema, **rest)
    @swoop_webhook_subscriptions = Subscriptions::SwoopWebhookSubscriptions.new(schema: schema, **rest)
  end

  # Fetch subscriptions matching this field + arguments pair
  # And pass them off to the queue.
  # @param event_name [String]
  # @param args [Hash<String, Symbol => Object]
  # @param object [Object]
  # @param scope [Symbol, String]
  # @return [void]
  def trigger(event_name, args, object, scope: nil)
    @swoop_ably_subscriptions.trigger(event_name, args, object, scope: scope)
    @swoop_webhook_subscriptions.trigger(event_name, args, object, scope: scope)
  end

  # `event` was triggered on `object`, and `subscription_id` was subscribed,
  # so it should be updated.
  #
  # Load `subscription_id`'s GraphQL data, re-evaluate the query, and deliver the result.
  #
  # This is where a queue may be inserted to push updates in the background.
  #
  # @param subscription_id [String]
  # @param event [GraphQL::Subscriptions::Event] The event which was triggered
  # @param object [Object] The value for the subscription field
  # @return [void]
  def execute(subscription_id, event, object)
    # ably subscription ids are uuids, webhook subscription ids are numeric ids
    if uuid?(subscription_id)
      @swoop_ably_subscriptions.execute(subscription_id, event, object)
    else
      @swoop_webhook_subscriptions.execute(subscription_id, event, object)
    end
  end

  # Event `event` occurred on `object`,
  # Update all subscribers.
  # @param event [Subscriptions::Event]
  # @param object [Object]
  # @return [void]
  def execute_all(event, object)
    @swoop_ably_subscriptions.execute_all(event, object)
    @swoop_webhook_subscriptions.execute_all(event, object)
  end

  # Get each `subscription_id` subscribed to `event.topic` and yield them
  # @param event [GraphQL::Subscriptions::Event]
  # @yieldparam subscription_id [String]
  # @return [void]
  def each_subscription_id(event)
    @swoop_ably_subscriptions.each_subscription_id(event)
    @swoop_webhook_subscriptions.each_subscription_id(event)
  end

  # The system wants to send an update to this subscription.
  # Read its data and return it.
  # @param subscription_id [String]
  # @return [Hash] Containing required keys
  def read_subscription(subscription_id)
    # ably subscription ids are uuids, webhook subscription ids are numeric ids
    if uuid?(subscription_id)
      @swoop_ably_subscriptions.read_subscription(subscription_id)
    else
      @swoop_webhook_subscriptions.read_subscription(subscription_id)
    end
  end

  # A subscription query was re-evaluated, returning `result`.
  # The result should be send to `subscription_id`.
  # @param subscription_id [String]
  # @param result [Hash]
  # @return [void]
  def deliver(subscription_id, result, more = true)
    # ably subscription ids are uuids, webhook subscription ids are numeric ids
    if uuid?(subscription_id)
      @swoop_ably_subscriptions.deliver(subscription_id, result, more)
    else
      @swoop_webhook_subscriptions.deliver(subscription_id, result, more)
    end
  end

  # `query` was executed and found subscriptions to `events`.
  # Update the database to reflect this new state.
  # @param query [GraphQL::Query]
  # @param events [Array<GraphQL::Subscriptions::Event>]
  # @return [void]
  def write_subscription(query, events)
    if query.context.key?(:webhook_url)
      @swoop_webhook_subscriptions.write_subscription(query, events)
    else
      @swoop_ably_subscriptions.write_subscription(query, events)
    end
  end

  # A subscription was terminated server-side.
  # Clean up the database.
  # @param subscription_id [String]
  # @return void.
  def delete_subscription(subscription_id)
    # ably subscription ids are uuids, webhook subscription ids are numeric ids
    if uuid?(subscription_id)
      @swoop_ably_subscriptions.delete_subscription(subscription_id)
    else
      @swoop_webhook_subscriptions.delete_subscription(subscription_id)
    end
  end

end
