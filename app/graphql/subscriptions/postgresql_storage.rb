# frozen_string_literal: true

class Subscriptions::PostgresqlStorage

  # A subscriptions backend. Tracks:
  # - subscription_key(subscription_id) => Query data
  # - topic_key(topic) => [subscription_id, subscription_id...]
  # - all_topics_key => [topic1, topic2, ... ]
  # @api private

  def initialize(subscriptions:)
    # This reference is used for loading & dumping context
    @subscriptions = subscriptions
  end

  # Record a few things:
  # - Query data for this subscription
  # - Add this subscription to each topic's list of subscriptions
  # - Add these topics to the global list of topics
  def write_subscription(query, topics)
    query_string = query.query_string ? query.query_string : query.document.to_query_string

    subscription = GraphQLSubscription.find_or_create_by!(
      query_string: query_string,
      variables: query.provided_variables.to_h,
      context: query.context.to_h,
      operation_name: query.operation_name,
      viewer: query.context.to_h[:viewer],
    )

    query.context[:subscription_id] = subscription.to_ssid

    topics.each do |topic|
      topic = GraphQLTopic.find_or_create_by!(name: topic.topic)
      topic.graphql_subscriptions << subscription
    end
  end

  # Fetch the query data for this subscription
  def read_subscription(sub_id)
    subscription = GraphQLSubscription.find_by(id: sub_id)
    return if subscription.blank?
    {
      query_string: subscription.query_string,
      variables: subscription.variables,
      context: subscription.context,
      operation_name: subscription.operation_name,
    }
  end

  # Find each subscription for the triggered topic
  def each_subscription_id(event)
    topic = GraphQLTopic.find_by name: event.topic
    return if topic.blank?
    topic.graphql_subscriptions.each do |subscription|
      yield subscription.id
    end
  end

  # Delete this subscription by:
  # - removing it from each of its topics lists of subscriptions
  # - if it was the last subscriber to a topic, removing the topic from the list of topics
  # - removing its query data
  def delete_subscription(sub_id)
    subscription = GraphQLSubscription.find_by id: sub_id
    return if subscription.blank?
    subscription.graphql_topics.each do |topic|
      topic.graphql_subscriptions.delete subscription
      if topic.graphql_subscriptions.blank?
        topic.destroy!
      end
    end
    subscription.destroy!
  end

  # Clear all subscription state
  def clear(cursor: 0)
    # NOT IMPLEMENTING
  end

  # # For the UI:
  def topics(limit:, offset:)
    topics = GraphQLTopic.limit(limit).offset(offset)
      .map do |t|
        OpenStruct.new(
          name: t.name,
          subscriptions_count: t.graphql_subscriptions.count,
        )
      end
    [topics, GraphQLTopic.count]
  end

end
