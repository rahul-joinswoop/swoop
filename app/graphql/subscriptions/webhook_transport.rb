# frozen_string_literal: true

class Subscriptions::WebhookTransport

  include HTTPClient
  include Subscriptions::DeliveryWrapper

  class Error < StandardError; end

  # these are the original http headers that come in
  GRAPHQL_SUBSCRIPTION_WEBHOOK_URL = 'GraphQL-Subscription-Webhook-Url'
  GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET = 'GraphQL-Subscription-Webhook-Secret'
  GRAPHQL_SUBSCRIPTION_ID = 'GraphQL-Subscription-Id'
  GRAPHQL_SUBSCRIPTION_VIEWER = 'GraphQL-Subscription-Viewer'

  # this is how rails gets the headers from rack because LOL I HAVE NO IDEA
  HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_URL = "HTTP_#{GRAPHQL_SUBSCRIPTION_WEBHOOK_URL.underscore.upcase}"
  HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET = "HTTP_#{GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET.underscore.upcase}"
  HTTP_GRAPHQL_SUBSCRIPTION_ID = "HTTP_#{GRAPHQL_SUBSCRIPTION_ID.underscore.upcase}"
  HTTP_GRAPHQL_SUBSCRIPTION_VIEWER = "HTTP_#{GRAPHQL_SUBSCRIPTION_VIEWER.underscore.upcase}"

  # it's safe to make http calls here because we're always being called from inside of a worker
  def deliver(subscription_id, result, more = true)
    delivery_wrapper(subscription_id, result, more) do |payload|
      subscription = GraphQLSubscription.find(subscription_id)
      response = http_client
        .headers(
          GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET => subscription.context[:webhook_secret],
          GRAPHQL_SUBSCRIPTION_ID => subscription.to_ssid,
          GRAPHQL_SUBSCRIPTION_VIEWER => subscription.context[:viewer].to_ssid,
        )
        .post(subscription.context[:webhook_url], json: payload)

      unless ok?(response)
        # convert our response into something that rollbar can hopefully parse
        parsed = JSON.parse(response.to_json)
        Rollbar.error "Subscriptions::WebhookTransport request failed", payload: payload, response: parsed
        # TODO - do we need to raise here? we'll end up with duplicate errors in Rollbar, and i'm not sure what happens
        # if/when we fail here? i guess we could retry a job or something
        raise Error, "Subscriptions::WebhookTransport request failed"
      end
      response
    end
  end

end
