# frozen_string_literal: true

class Subscriptions::SwoopAblySubscriptions < GraphQL::Pro::AblySubscriptions

  include Subscriptions::SwoopSubscriptionExecute
  include Subscriptions::DeliveryWrapper

  # same as original but includes cipher: key
  def execute_all(event, object)
    @storage.each_subscription_id(event.topic) do |subscription_id|
      ably_channel = @ably.channel(subscription_id, cipher: { key: Swoop.ably_encryption_key })
      if ably_channel.presence.get.items.any?
        execute(subscription_id, event, object)
      else
        delete_subscription(subscription_id)
      end
    end
  end

  def deliver(subscription_id, result, more = true)
    delivery_wrapper(subscription_id, result, more) do |payload|
      ably_channel = @ably.channel(subscription_id, cipher: { key: Swoop.ably_encryption_key })
      ably_channel.publish("update", payload)
    end

    if !more
      # subscription has ended, we need to cleanup
      delete_subscription subscription_id
    end
  end

end
