# frozen_string_literal: true

module Subscriptions::SwoopSubscriptionExecute

  include Subscriptions::Debouncer

  # shared #execute method that also rebuilds our only_filter

  def execute(subscription_id, event, object)
    # Lookup the saved data for this subscription
    query_data = read_subscription(subscription_id)

    # Fetch the required keys from the saved data
    query_string = query_data.fetch(:query_string)
    variables = query_data.fetch(:variables)
    operation_name = query_data.fetch(:operation_name)

    context = query_data.fetch(:context)

    # IMPORTANT: the api_roles need to be refreshed from the database before the code below that builds the filter
    if context[:api_access_token]
      authenticator = SwoopAuthenticator.from_context(context)
      if authenticator.authenticated?
        context[:api_roles] = authenticator.roles
      else
        # Access token is revoked or expired so subscription is invalid.
        return deliver(subscription_id, nil, false)
      end
    else
      # TODO this branch can go away after the next release BD 2019-06-12

      # rebuild the context user roles to ensure they reflect the latest values from the database.
      Rails.logger.debug("Subscriptions::SwoopSubscriptionExecute: deprecated call to construct roles")
      if context[:api_user]
        if context[:api_roles] != [SwoopAuthenticator::CUSTOMER_ROLE]
          # special case - we can't get back to our access token here to check the scopes so we assume
          # if we were a customer, we'll always be a customer.
          # TODO - should we store the access_token in graphql context? or should we assume this
          # is the case if we have an api_user without an api_company / api_application?
          context[:api_roles] = context[:api_user].roles.map(&:name)
        end
      elsif context[:api_application]
        context[:api_roles] = SwoopAuthenticator.scope_roles(context[:api_application].scopes, context[:api_company])
      end
    end

    # rebuild our only filter since it can't be serialized
    only_filter = GraphQL::BuildFilter.new(input: { context: context }).call.output[:only_filter]

    # Re-evaluate the saved query
    result = @schema.execute(
      {
        query: query_string,
        context: context,
        subscription_topic: event.topic,
        operation_name: operation_name,
        variables: variables,
        root_value: object,
        only: only_filter,
      }
    )

    # debounce our subscription - will raise a NoUpdateError if we don't need
    # to send it out
    debounce!(subscription_id, result, object, context[:viewer])

    deliver(subscription_id, result)
  rescue GraphQL::Schema::Subscription::NoUpdateError
    # This update was skipped in user code; do nothing.
  rescue GraphQL::Schema::Subscription::UnsubscribedError
    # `unsubscribe` was called, clean up on our side
    # TODO also send `{more: false}` to client?
    delete_subscription(subscription_id)
  end

end
