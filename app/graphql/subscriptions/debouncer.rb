# frozen_string_literal: true

require 'xxhash'

module Subscriptions::Debouncer

  # this module helps prevent duplicate subscription messages from going out on the same
  # subscription.
  #
  # consider the following subscription query:
  #
  # subscription JobUpdated($id: ID!) {
  #   jobUpdated(id: $id) {
  #     id
  #     status
  #   }
  # }
  #
  # this subscription will fire off on every job change (by design). however, the subscriber
  # really only cares about when id or status change - any other change will trigger a new
  # subscription update which looks identical to this subscriber based on the fields they
  # are querying on.
  #
  # to get around this, we maintain a cache of the last subscription response sent for each
  # subscription/root_value/viewer combination. any time we go to send a new subscription update
  # we check our cached value first - if it's identical[*] we skip sending our current update
  # since it's redundant to our subscriber.
  #
  # we only store/compare the last response because it's possible to have two different non-subsequent
  # events that need to be delivered (for example - a job goes from dispatched -> enroute -> dispatched
  # because it's reassigned to a different driver).
  #
  # our cached responses expire in an hour from creation / last comparison time.
  #
  # [*] - we don't actually store/check string equality since response strings can be huge. instead
  #       we use a very fast hashing algorithm and store/compare the hash.

  EXPIRATION = 1.hour.to_i
  LOG_MESSAGE = "GraphQL: %s debouncing Subscription(%s) on root_value %s(%s)"

  module_function def debounce!(subscription_id, result, root_value, viewer)
    return if ENV['GRAPHQL_DEBOUNCING'].blank?

    # build our redis key - we may not have a root object (although i'm not sure
    # how/when this would happen) so gracefully handle that scenario
    key = [
      "graphql",
      "subscription_debouncer",
      subscription_id,
      root_value&.to_ssid,
    ].reject(&:blank?).join(':')

    # hash our result object
    current = XXhash.xxh32(Marshal.dump(result.to_h)).to_s

    previous = Swoop.graphql_redis_client.multi do |redis|
      # this returns back the previous value, which we'll check below
      redis.getset key, current
      redis.expire key, EXPIRATION
    end.first

    # and raise if our previous value is identical to our current
    if previous == current
      Rails.logger.debug(LOG_MESSAGE % [
        Utils::QueryLogger.viewer_string(viewer),
        subscription_id,
        root_value&.class&.name,
        root_value&.id,
      ])
      raise GraphQL::Schema::Subscription::NoUpdateError
    end
  end

end
