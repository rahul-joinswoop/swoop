# frozen_string_literal: true

module Subscriptions::DeliveryWrapper

  class SubscriptionError < StandardError; end

  # shared #deliver_wrapper method
  # TODO - move this up to SwoopMultiSubscriptions#deliver?
  def delivery_wrapper(subscription_id, result, more = true)
    errors = result&.dig('errors')
    if errors.present?
      # don't deliver subscriptions with errors
      Rails.logger.debug("GraphQL: not delivering subscriptions with errors")
      errors
        .reject { |e| e.dig('message')&.starts_with? 'Cannot return null for non-nullable field' }
        .each { |e| Rollbar.error(SubscriptionError.new(e.dig('message')), e) }
      false
    else
      # a nil payload's ok if more == false, otherwise cast to a hash
      payload = {
        result: (result.nil? && !more) ? result : result.to_h,
        more: more,
      }

      yield payload
    end
  end

end
