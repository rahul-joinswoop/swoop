# frozen_string_literal: true

require "forwardable"

class Subscriptions::SwoopWebhookSubscriptions < GraphQL::Subscriptions

  extend Forwardable
  include Subscriptions::SwoopSubscriptionExecute

  # @param schema [Class] the GraphQL schema this manager belongs to
  def initialize(schema:, **rest)
    super(schema: schema)
    @storage = Subscriptions::PostgresqlStorage.new(subscriptions: self)
    @transport = Subscriptions::WebhookTransport.new
  end

  def_delegators :@storage, :delete_subscription, :read_subscription, :write_subscription

  # These are for the UI:
  def_delegators :@storage, :topics, :each_subscription_id, :clear

  def deliver(subscription_id, result, more = true)
    @transport.deliver subscription_id, result, more
    if !more
      # subscription has ended, we need to cleanup
      delete_subscription subscription_id
    end
  end

end
