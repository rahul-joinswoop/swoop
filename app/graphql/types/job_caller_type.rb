# frozen_string_literal: true

class Types::JobCallerType < Types::BaseObject

  field :name, String, null: true
  def name
    # If FleetInHouseJob or FleetMotorClubJob, this is the fleet dispatcher (job.fleet_dispatcher_id)
    # If FleetManagedJob, this is the root dispatcher (job.root_dispatcher_id)
    # Otherwise this is the caller (job.caller_id)
    case object
    when ::FleetInHouseJob, ::FleetMotorClubJob
      object.fleet_dispatcher&.name
    when ::FleetManagedJob
      object.root_dispatcher&.name
    else
      object.caller&.name
    end
  end

  field :phone, Types::E164PhoneNumberType, null: true

  DISPATCH_NUMBER = Phonelib.parse(::Job::DEFAULT_SUPPORT_PHONE).e164.freeze

  def phone
    # - dependent on the type of job:
    # If FleetManagedJob this should be DISPATCH_NUMBER
    # Otherwise this should be the callers number using the logic used to determine Caller Name
    case object
    when ::FleetInHouseJob, ::FleetMotorClubJob
      object.fleet_dispatcher&.phone
    when ::FleetManagedJob
      DISPATCH_NUMBER
    else
      object.caller&.phone
    end
  end

end
