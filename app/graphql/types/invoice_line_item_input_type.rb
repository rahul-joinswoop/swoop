# frozen_string_literal: true

class Types::InvoiceLineItemInputType < Types::BaseInputObject

  argument :id, ID, required: false

  class Types::LineItemJobInputType < Types::BaseInputObject

    argument :id, ID, required: true

  end

  class Types::InvoiceRateInputType < Types::BaseInputObject

    argument :id, ID, required: false

  end

  argument :auto_calculated_quantity,
           Boolean,
           description: 'Used to flag whether Storage item quantity is auto_calculated (true) or changed by the user (false)',
           required: true

  argument :calc_type,
           Types::InvoiceLineItemCalcTypeEnumType,
           description: 'The Calc Type to be used for this LineItem. Can be percentage or flat.',
           required: true

  argument :description, String, description: 'LineItem description.', required: true

  argument :deleted_at, Types::DateTimeType, description: 'Date and time when this LineItem was deleted.', required: false

  argument :estimated,
           Boolean,
           description: 'Flags it as estimated by the system. If `false`, it means the PartnerCompany Admin has changed its original amount or quantity.',
           required: true

  argument :job, Types::LineItemJobInputType, required: false, required_roles: :rescue

  argument :net_amount, String, description: 'Net amount of this LineItem. It will likely be quantity multiplied by unit_price.', required: true

  argument :original_quantity, String, description: 'Original quantity of this LineItem.', required: true

  argument :original_unit_price, String, description: 'Original Unit Price inherited by the Rate from where this LineItem was based on.', required: true

  argument :quantity, String, description: 'Quantity of this LineItem.', required: true

  argument :rate, Types::InvoiceRateInputType, description: 'Rate from where this LineItem auto-calculated value was based on.', required: false

  argument :tax_amount, String, description: 'Tax amount of this LineItem.', required: true

  argument :unit_price, String, description: 'Unit Price inherited by the Rate from where this LineItem was based on.', required: true

  argument :user_created,
           Boolean,
           description: 'Flags as user created. It means it was not auto-calculated by the system, but added manually by the PartnerCompany Admin.',
           required: true

end
