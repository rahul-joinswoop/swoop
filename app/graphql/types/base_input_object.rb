# frozen_string_literal: true

class Types::BaseInputObject < GraphQL::Schema::InputObject

  argument_class Arguments::AuthorizedArgument

end
