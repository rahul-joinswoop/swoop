# frozen_string_literal: true

# Type used for coercing a vehicle class type name into an object and back.
class Types::VehicleClassTypeNameType < Types::BaseScalar

  description 'Name of the vehicle class type'

  class << self

    def coerce_input(value, context)
      vehicle_category = Resolvers::VehicleClassTypes.new(object: context[:api_company], context: context).resolve.find_by(name: value)
      unless vehicle_category
        raise GraphQL::CoercionError, "#{value.inspect} is not a valid VehicleClassTypeType"
      end
      vehicle_category
    end

    def coerce_result(value, _ctx)
      value&.name&.to_s
    end

  end

end
