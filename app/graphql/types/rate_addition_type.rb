# frozen_string_literal: true

class Types::RateAdditionType < Types::BaseObject

  include Utils::Currency

  description 'Specific addition to be added to a Rate.'

  global_id_field :id
  implements Interfaces::TimestampsInterface

  field :name, String, description: 'Name of the Addition.', null: true
  field :calc_type, String, description: 'The Calc Type to be used for this Rate Addition. Can be percentage or flat.', null: true
  field :amount, String, description: 'The amount of this Rate Addition.', null: true

  def amount
    currency_value_or_zero(object.unit_price)
  end
  field :currency,
        Types::CurrencyEnumType,
        description: "Currency used for this Rate addition",
        null: false

end
