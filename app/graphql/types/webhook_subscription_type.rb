# frozen_string_literal: true

class Types::WebhookSubscriptionType < Types::BaseObject

  description <<~DESCRIPTION
    A Webhook Subscription
  DESCRIPTION

  global_id_field :id
  implements Interfaces::TimestampsInterface

  field :operation_name,
        String,
        null: true,
        description: "The GraphQL operation name for the subscription"

  field :query_string,
        String,
        null: false,
        description: "The GraphQL query string for the subscription"

  field :variables,
        Types::JsonType,
        null: false,
        description: "The provided variables for the subscription"

  field :webhook_url,
        String,
        null: false,
        description: "The URL to POST subscription updates to"

  def webhook_url
    object.context[:webhook_url]
  end

  # TODO - do we want this?
  # field :webhook_secret,
  #       String,
  #       null: false

  # def webhook_secret
  #   object.context[:webhook_secret]
  # end

end
