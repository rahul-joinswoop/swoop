# frozen_string_literal: true

class Types::BaseObject < GraphQL::Schema::Object

  field_class Fields::AuthorizedField

  cattr_accessor :required_roles, instance_accessor: false
  cattr_accessor :required_features, instance_accessor: false
  cattr_accessor :if_company, instance_accessor: false
  cattr_accessor :if_context, instance_accessor: false

  # This method is overridden to customize object types:
  def self.to_graphql
    type_defn = super # returns a GraphQL::ObjectType
    # Get a configured value and assign it to metadata
    type_defn.metadata.merge!(
      required_roles: required_roles,
      required_features: required_features,
      if_company: if_company,
      if_context: if_context,
    )
    type_defn
  end

end
