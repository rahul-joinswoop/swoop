# frozen_string_literal: true

class Types::DeprecatedFieldStatsType < Types::BaseObject

  field :field,
        String,
        null: false
  def field
    object[0]
  end

  field :count,
        Integer,
        null: false
  def count
    object[1]
  end

  field :reason,
        String,
        null: false
  def reason
    SwoopSchema.deprecation_reason_for(object[0])
  end

end
