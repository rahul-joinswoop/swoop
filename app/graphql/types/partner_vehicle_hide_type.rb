# frozen_string_literal: true

class Types::PartnerVehicleHideType < Types::BaseInputObject

  argument :offDutyDriver, Boolean, "Filter out PartnerVehicles with off-duty Drivers", required: false, default_value: false
  argument :noDriver, Boolean, "Filter out PartnerVehicles without Drivers", required: false, default_value: false

end
