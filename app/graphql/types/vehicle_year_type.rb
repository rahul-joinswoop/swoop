# frozen_string_literal: true

class Types::VehicleYearType < Types::BaseObject

  description "The Vehicle Year to be used on Jobs."

  field :year, Integer, description: "Name of the VehicleModel", null: true

  def year
    object
  end

end
