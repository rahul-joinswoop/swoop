# frozen_string_literal: true

class Types::JobLocationType < Types::BaseObject

  field :service_location,
        Types::LocationType,
        null: true,
        required_roles: :any

  def service_location
    sl = object.service_location
    if sl && object.is_a?(::Job) && object.auto_assigning? && context.is_partner?
      sl.address = sl.address_without_street_number
    end
    sl
  end

  field :dropoff_location,
        Types::LocationType,
        null: true

  def dropoff_location
    dl = object.drop_location
    if dl && object.is_a?(::Job) && object.auto_assigning? && context.is_partner?
      dl.address = dl.address_without_street_number
    end
    dl
  end

  field :pickup_contact,
        Types::ContactType,
        null: true,
        resolver: Resolvers::HideFromPartnerIfAutoAssigning.build(:pickup_contact),
        description: "Customer may optionally specify a separate contact who will be with the vehicle at the service location."

  field :dropoff_contact,
        Types::ContactType,
        null: true,
        resolver: Resolvers::HideFromPartnerIfAutoAssigning.build(:dropoff_contact),
        description: "Customer may optionally specify a separate contact who will be with the vehicle at the service location."

end
