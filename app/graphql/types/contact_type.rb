# frozen_string_literal: true

class Types::ContactType < Types::BaseObject

  field :full_name, String, description: "First and last name of the contact combined.", null: false, deprecation_reason: "Please use name instead"
  field :name, String, description: "First and last name of the contact combined.", null: false
  field :phone, Types::E164PhoneNumberType, description: "Phone number of the contact.", null: true

end
