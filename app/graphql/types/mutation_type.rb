# frozen_string_literal: true

# TODO Should we split it into specific files?
#      e.g. Types::JobMutationType, Types::PartnerDriverMutationType, etc?
class Types::MutationType < Types::BaseObject

  # NOTE - if you set if_context: on any mutation please make sure you understand the consequences!
  # if in doubt look at the is_valid_mutation_viewer? method and/or check with jon

  # build our custom field_class with default values
  field_class Fields::AuthorizedField.with_defaults(
    if_context: :is_valid_mutation_viewer?,
    null: true,
  )

  field :add_job,
        mutation: Mutations::AddJob,
        deprecation_reason: "Please use createJob instead"

  field :create_demo_job,
        mutation: Mutations::CreateDemoJob,
        required_roles: :rescue # eventually we should support clients here as well

  field :create_document,
        mutation: Mutations::CreateDocument,
        required_roles: [:rescue]

  field :create_draft_job,
        mutation: Mutations::CreateDraftJob

  field :create_job,
        mutation: Mutations::CreateJob

  field :create_partner_vehicle,
        mutation: Mutations::CreatePartnerVehicle,
        required_roles: [:rescue]

  field :create_site,
        mutation: Mutations::CreateSite,
        required_roles: [:rescue, :admin] # eventually we should support clients here as well

  field :create_user,
        mutation: Mutations::CreateUser,
        required_roles: [:rescue, :admin]

  field :delete_document,
        mutation: Mutations::DeleteDocument,
        required_roles: [:rescue]

  # we explicitly allow 3rd party api clients acting as companies to use this
  # mutation but not 3rd party api clients acting as users
  field :delete_subscription,
        mutation: Mutations::DeleteSubscription,
        if_context: -> (context) {
          if context.is_third_party_api_client?
            context[:viewer].is_a?(::Company)
          else
            true
          end
        }

  field :edit_invoice,
        mutation: Mutations::EditInvoice,
        required_roles: :rescue, # we may want to let Swoop to use it in the future
        deprecation_reason: "Please use updateInvoice instead"

  field :edit_job,
        mutation: Mutations::EditJob,
        deprecation_reason: "Please use updateJob instead"

  field :edit_job_status,
        mutation: Mutations::EditJobStatus,
        deprecation_reason: "Please use updateJobStatus instead"

  field :presign_document,
        mutation: Mutations::PresignDocument,
        required_roles: [:rescue]

  # only rescue _users_ can use this because an api_user is required.
  field :request_callback,
        mutation: Mutations::RequestCallback,
        required_roles: -> (roles) {
          roles.include?(:rescue) && ([:admin, :dispatcher] & roles).present?
        }

  field :request_more_time,
        mutation: Mutations::RequestMoreTime,
        required_roles: -> (roles) {
          roles.include?(:rescue) && ([:admin, :dispatcher] & roles).present?
        }

  field :sample_invoice,
        mutation: Mutations::SampleInvoice,
        required_roles: :rescue # we may want to let Swoop to use it in the future

  field :update_invoice,
        mutation: Mutations::UpdateInvoice,
        required_roles: :rescue # we may want to let Swoop to use it in the future

  field :update_job,
        mutation: Mutations::UpdateJob

  field :update_job_status,
        mutation: Mutations::UpdateJobStatus

  # UpdatePartnerVehicleAnalyzer logic guarantees that Drivers can only change PartnerVehicles to themselves
  #
  # NOTE: it will likely be changed in the future to allow Dispatchers and Admins as well, then we'll probably need to filter out
  # attributes that drivers can't change? (like we don't want model or make to be changed by Drivers I suppose)
  field :update_partner_vehicle,
        mutation: Mutations::UpdatePartnerVehicle,
        required_roles: [:rescue, :driver]

  field :update_user,
        mutation: Mutations::UpdateUser,
        required_roles: :rescue

end
