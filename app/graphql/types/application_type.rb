# frozen_string_literal: true

class Types::ApplicationType < Types::BaseObject

  global_id_field :id

  field :company,
        Types::CompanyType,
        null: true,
        resolver: Resolvers::MyApplicationOnly.build(:owner)

  field :name,
        String,
        null: true,
        resolver: Resolvers::MyApplicationOnly.build(:name)

  field :subscriptions,
        Types::WebhookSubscriptionType.connection_type,
        null: true,
        resolver: Resolvers::MyApplicationOnly.build(:graphql_subscriptions)

  field :grantable_roles,
        Types::RoleType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::MyApplicationOnly.build(:grantable_roles),
        description: "Roles that can be granted to other Users"

  field :stats,
        Types::StatsType,
        null: false,
        description: "Stats"

  def stats
    object
  end

end
