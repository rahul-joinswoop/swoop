# frozen_string_literal: true

class Types::Job::LocationInputType < Types::BaseInputObject

  graphql_name "JobLocationInput"
  # TODO - should we change this to pickupLocation instead?
  argument :service_location, Types::LocationInputType, required: false
  argument :dropoff_location, Types::LocationInputType, required: false
  argument :pickup_contact, Types::ContactInputType, description: "Customer may optionally specify a separate contact who will be with the vehicle at the service location.", required: false
  argument :dropoff_contact, Types::ContactInputType, description: "Customer may optionally specify a separate contact who will be with the vehicle at the service location.", required: false

end
