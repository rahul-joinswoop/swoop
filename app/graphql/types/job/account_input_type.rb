# frozen_string_literal: true

class Types::Job::AccountInputType < Types::BaseInputObject

  argument :name, String, required: false, description: "Account name"

end
