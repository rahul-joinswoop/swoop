# frozen_string_literal: true

class Types::Job::ClientInputType < Types::BaseInputObject

  argument :department, Types::DepartmentNameType, required: false, required_features: Feature::DEPARTMENTS do
    description "Name of the client department assigned to the job"
  end

  argument :site, Types::SiteNameType, required: false, required_features: Feature::CLIENT_SITES do
    description "Name of the client site assigned to the job"
  end

end
