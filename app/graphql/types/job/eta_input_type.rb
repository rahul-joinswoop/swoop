# frozen_string_literal: true

class Types::Job::EtaInputType < Types::BaseInputObject

  graphql_name "JobEtaInputType"

  argument :current, Types::DateTimeType, required: false, required_roles: :rescue do
    description "Current ETA"
  end

end
