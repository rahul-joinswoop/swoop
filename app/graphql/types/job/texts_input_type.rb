# frozen_string_literal: true

class Types::Job::TextsInputType < Types::BaseInputObject

  graphql_name "JobTextsInput"
  argument :send_location, Boolean, required: false
  argument :send_eta_updates, Boolean, required: false
  argument :send_driver_tracking, Boolean, required: false
  argument :send_review, Boolean, required: false
  argument :send_texts_to_pickup, Boolean, required: false

end
