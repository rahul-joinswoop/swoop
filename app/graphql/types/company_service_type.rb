# frozen_string_literal: true

class Types::CompanyServiceType < Types::BaseObject

  description 'A company-specific service to be specified during Job creating.'

  # TODO - is this the right id now?
  global_id_field :id
  implements GraphQL::Relay::Node.interface

  field :name, Types::ServiceCodeNameType, method: :service_code, null: true, preload: :service_code do
    description 'Name of the Service.'
  end

  field :is_standard, Boolean, null: true, preload: :service_code do
    description <<~DESCRIPTION
      Flag to mark a Service as Standard. Standard services are added to all
      companies by default, and can be removed. If this service is not standard,
      it means it's a Company custom one.
    DESCRIPTION
  end

  def is_standard
    object.service_code.is_standard
  end

  field :questions, Types::QuestionType.connection_type, null: true, connection: true do
    description 'Questions available for the Company on this Service'
  end

  field :question, Types::QuestionType, null: true do
    description 'Question available for the Company on this Service'
    argument :id, ID, "Question ID", prepare: Resolvers::GlobalId.new, required: true
  end

  def question(**args)
    object.questions.find(args[:id])
  end

end
