# frozen_string_literal: true

class Types::AccountType < Types::BaseObject

  description "An account is a relationship between a PartnerCompany and Swoop or any ClientCompany. Accounts can be created by Partner admins."

  global_id_field :id
  implements Interfaces::TimestampsInterface

  field :name, String, description: "A name to describe the association between a rescue company and a client company. Usually it's useful to add the client company name", null: true
  field :accounting_email, Types::EmailType, description: "The email address used by the PartnerCompany admins to where emails ref accounting will go to.", null: true
  field :primary_email, Types::EmailType, description: "TBD", null: true
  field :notes, String, description: "A simple memo field where the PartnerCompany admin can add more info details to this Account.", null: true
  field :phone, Types::E164PhoneNumberType, description: "A phone number set by the PartnerCompany admin for acccounting related calls.", null: true
  field :fax, String, description: "A fax number set by the PartnerCompany admin for acccounting related faxes.", null: true
  field :contact_name, String, description: "A contact name for the one who's in charge of this Account on the PartnerCompany.", null: true
  field :contact_phone, Types::E164PhoneNumberType, description: "A contact phone for the one who's in charge of this Account on the PartnerCompany.", null: true
  field :contact_email, Types::EmailType, description: "TBD", null: true
  field :accounting_email_cc, Types::EmailType, description: "A cc email address used by the PartnerCompany for emails ref accounting.", null: true
  field :accounting_email_bcc, Types::EmailType, description: "A bcc email address used by the PartnerCompany for emails ref accounting.", null: true
  field :disable_qb_import, Boolean, description: "A flag to enable/disable QuickBooks async import.", null: true
  # RG is it used?
  field :hide_timestamps_on_invoice, Boolean, description: "TBD", null: true
  field :location, Types::LocationType, description: "Represents the billing address of this Account.", null: true
  field :physical_location, Types::LocationType, description: "Represents the physical address of this Account.", null: true
  # TODO - turn these back on when we have authorization
  # field :clientCompany, Types::CompanyType, property: :client_company do
  #   description "The client company associated with this account. Can be Swoop Company or a ClientCompany."
  # end
  # field :rescueCompany, Types::CompanyType, property: :rescue_company do
  #   description "The rescue company associated with this account. It is always a PartnerCompany type, the one in charge of executing the service."
  #
  #   resolve -> (obj, args, ctx) { obj.company }
  # end
  field :priority_color, String, description: "Hold a specific color for this account, for UI purposes.", null: true
  field :po_required_at_job_creation, Boolean, description: "Flag that says whether to require or not a PO number during Job creation.", null: true
  field :po_required_before_sending_invoice, Boolean, description: "TBD", null: true
  field :auto_accept, Boolean, description: "Flag to enable auto accept jobs by MotorClub companies", null: true
  field :auto_eta, Integer, description: "Default ETA value used to set ETA to a MotorClub received Job", null: true
  field :department, Types::DepartmentType, description: "TBD", null: true

  # TODO connection dispatchable_sites (https://github.com/joinswoop/swoop/blob/master/app/views/api/v1/accounts/_account.json.jbuilder#L46)

end
