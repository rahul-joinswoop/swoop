# frozen_string_literal: true

class Types::SiteType < Types::BaseObject

  description "A site can represent a branch of a PartnerCompany that will attend to the Job." \
    "It can also have a different meaning, to represent a physical location for ClientCompanies " \
    "used on service location or drop location fields of a Job."

  global_id_field :id
  implements Interfaces::TimestampsInterface

  field :name, String, description: "Name of the Site.", null: false
  field :type, String, description: "Can be a Site or a Place.", null: false
  field :company, Types::CompanyType, description: "The Company who owns this Site.", null: false, resolver: Resolvers::Company

  field :location, Types::LocationType, description: "The physical address of this Site.", null: false
  field :tz, String, description: "Timezone where this Site is located.", null: true
  field :phone, Types::E164PhoneNumberType, description: "Phone number of the Site.", null: true
  field :within_hours, Boolean, description: "TBD", null: true
  field :dispatchable, Boolean, description: "Applicable to PartnerCompany. If `true` this site will be available to receive dispatched Jobs.", null: true
  field :tire_program, Boolean, description: "If `true` this Site can receive Jobs specific to Tire. Only specific Companies use it.", null: true
  field :storage_lot, Boolean, description: 'TBD', null: true
  field :always_open, Boolean, description: 'Applicable to PartnerCompany. If `true`, it means it is opened 24/7.', null: true
  field :owner_company, Types::CompanyType, description: "Applicable to PartnerCompany. The sub company who owns this Site. Think of sub company as a branch of the Company.", null: true, resolver: Resolvers::Company
  field :force_open, Boolean, description: "Applicable to PartnerCompany. Admins can force open a site anytime for cases when it would be expected to be closed.", null: true
  field :open_time, Types::DateTimeType, description: 'Applicable to PartnerCompany. Specify the time when this site opens for business, so it will available to receive Jobs.', null: true
  field :close_time, Types::DateTimeType, description: 'Applicable to PartnerCompany. Specify the time when this site closes so it will not be available to receive Jobs.', null: true
  field :open_time_sat, Types::DateTimeType, description: 'Applicable to PartnerCompany. Specify the time when this site opens to be available to receive Jobs on Saturdays.', null: true
  field :close_time_sat, Types::DateTimeType, description: 'Applicable to PartnerCompany. Specify the time when this site closes so on Saturdays.', null: true
  field :open_time_sun, Types::DateTimeType, description: 'Applicable to PartnerCompany. Specify the time when this site opens to be available to receive Jobs on Sundays.', null: true
  field :close_time_sun, Types::DateTimeType, description: 'Applicable to PartnerCompany. Specify the time when this site closes so on Sundays.', null: true
  field :site_code, String, description: 'TBD', null: true

  # TODO - reenable when authorization is implemented
  # connection :client_companies, Types::CompanyType.connection_type do
  #   description 'Applicable to PartnerCompany. Client companies to whom this Site will be available to receive dispatched Jobs.'
  #   resolve -> (obj, args, ctx) { obj.providerCompanyNames }
  # end

end
