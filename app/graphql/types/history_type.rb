# frozen_string_literal: true

class Types::HistoryType < Types::BaseObject

  description 'The history records of a Job. It contains a record per Job status change.'

  field :lastSet, Types::DateTimeType, description: 'When this history record was last set/edited.', null: true

  def last_set
    object.last_set_dttm
  end

  field :type, String, description: 'The type of History record', null: true

  def type
    object.ui_type
  end

  field :name, String, description: 'The name of the History record', null: true

  def name
    if object.is_a?(::ParentJobHistoryItem)
      object.name.to_s.tr('*', '') # rip off the * that GenerateJobHistory appends to parent job history names
    elsif object.respond_to?(:name) # all other cases should fall here, but let's check it first to make sure
      object.name
    else
      '' # should never reach it, but let's use it as a fallback
    end
  end

end
