# frozen_string_literal: true

class Types::TrailerType < Types::BaseObject

  implements GraphQL::Relay::Node.interface
  implements Interfaces::VehicleMinimalInterface

  global_id_field :id

  description "The Trailer used by the PartnerDriver during a Job execution"

  def self.authorized?(object, context)
    super &&
    case context[:api_company]
    when FleetCompany
      false
    when RescueCompany
      object.company_id == context[:api_company].id
    when SuperCompany
      case context[:api_company].id
      when ::Company.swoop_id
        # root user, can query any vehicles (including deleted ones)
        context.has_role?(:root)
      else
        false
      end
    else
      false
    end
  end

end
