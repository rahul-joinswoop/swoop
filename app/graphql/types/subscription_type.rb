# frozen_string_literal: true

class Types::SubscriptionType < Types::BaseObject

  # NOTE - if you set if_context: on any mutation please make sure you understand the consequences!
  # if in doubt look at the is_valid_subscription_viewer? method and/or check with jon

  # build our custom field_class with default values
  field_class Fields::AuthorizedField.with_defaults(
    if_context: :is_valid_subscription_viewer?,
    null: false,
    subscription_scope: :viewer,
  )

  graphql_name "Subscription"

  field :activeJobsFeed,
        Types::JobType do
    description "A Job's status has entered Active, an Active Job has been updated, or a Job's status has left Active"
  end

  field :jobDocumentChanged,
        Types::DocumentType,
        required_roles: :rescue do
    description "A Job's document has been added or removed"
    argument :job_id,
             ID,
             required: true,
             prepare: Resolvers::GlobalId.new
    argument :type,
             Types::DocumentTypeEnumType,
             required: false
    argument :container,
             Types::DocumentContainerEnumType,
             required: false
  end

  field :jobStatusEntered,
        Types::JobType do
    description "A Job's status has entered a given JobsStatusGroup"
    argument :states,
             Types::Subscription::JobStatusGroupEnumType,
             default_value: :active_states,
             # TODO - make this true, see ENG-12288
             required: false
    # and maybe later:
    # argument :state, Types::JobStatusEnumType, required: false
  end

  field :jobStatusLeft,
        Types::JobType do
    description "A Job's status has left a given JobsStatusGroup"
    argument :states,
             Types::Subscription::JobStatusGroupEnumType,
             default_value: :active_states,
             # TODO - make this true, see ENG-12288
             required: false
    # and maybe later
    # argument :state, Types::JobStatusEnumType, required: false
  end

  field :jobUpdated,
        Types::JobType,
        required_roles: :any do
    description "A Job has been updated"
    argument :id,
             ID,
             required: true,
             prepare: Resolvers::GlobalId.new,
             required_roles: :any
  end

  # this subscription is company-wide, hence subscription_scope: :api_company and not viewer.
  field :partnerVehicleChanged,
        Types::PartnerVehicleType,
        subscription_scope: :api_company,
        required_roles: :rescue do
    description "A Partner Vehicle has changed"
  end

  field :userUpdated,
        Types::UserType,
        required_roles: :rescue do
    description "A User has been updated"
    argument :id,
             ID,
             required: true,
             prepare: Resolvers::GlobalId.new
  end

end
