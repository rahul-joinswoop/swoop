# frozen_string_literal: true

class Types::DocumentTypeEnumType < Types::BaseEnum

  graphql_name 'DocumentTypeType'
  description 'Document Type'

  value 'Document', 'Document', value: 'AttachedDocument'
  value 'LocationImage', 'LocationImage', value: 'AttachedDocument::LocationImage'
  value 'SignatureImage', 'SignatureImage', value: 'AttachedDocument::SignatureImage'

end
