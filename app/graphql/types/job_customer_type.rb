# frozen_string_literal: true

# Customer being serviced by a job.
class Types::JobCustomerType < Types::BaseObject

  # TODO this should beome a user
  description "Customer being serviced by a job."
  implements Interfaces::TimestampsInterface

  global_id_field :id

  field :first_name,
        String,
        null: true

  field :last_name,
        String,
        null: true

  field :full_name,
        String,
        null: true,
        deprecation_reason: "Please use name instead"

  field :email,
        Types::EmailType,
        null: true

  field :name,
        String,
        null: true

  field :phone,
        Types::E164PhoneNumberType,
        null: true

  field :member_number,
        String,
        null: true,
        required_features: Feature::JOB_MEMBER_NUMBER,
        description: "Customer's Member Number"

end
