# frozen_string_literal: true

class Types::JobCustomerInputType < Types::BaseInputObject

  argument :email, String, description: "Email address of the customer combined.", required: false
  argument :fullName, String, description: "Deprecated: please use name instead.", required: false
  argument :memberNumber, String, description: "Member Number of the customer.", required: false, required_features: Feature::JOB_MEMBER_NUMBER
  argument :name, String, description: "First and last name of the customer combined.", required: false
  argument :phone, Types::E164PhoneNumberType, description: "Phone number of the customer.", required: false

end
