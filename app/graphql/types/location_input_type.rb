# frozen_string_literal: true

# TODO - docs don't say these are optional but they are
# (either address or lat/lng/google_place_id is required?)
# location_type is always required?
class Types::LocationInputType < Types::BaseInputObject

  argument :address, String, description: 'Address that the customer inputs. Should be formatted based on the response from the Google Places API, such as "379 S Van Ness Ave, San Francisco, CA 94103, USA".', required: false
  argument :lat, Float, description: "Latitude returned from Google Places API.", required: false
  argument :lng, Float, description: "Longitude returned from Google Places API.", required: false
  argument :location_type, Types::LocationTypeNameType, description: "See below section for more information on Location Types.", required: false
  argument :google_place_id, String, description: "ID returned from Google Places API. See below for more information on Locations.", required: false
  argument :exact, Boolean, description: "Location is exact and has lat/lng to high accuracy", required: false

end
