# frozen_string_literal: true

class Types::DateTimeRangeInputType < Types::BaseInputObject

  argument :from, Types::DateTimeType, required: false, description: 'From DateTime'
  argument :to, Types::DateTimeType, required: false, description: 'To DateTime'

end
