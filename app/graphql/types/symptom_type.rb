# frozen_string_literal: true

class Types::SymptomType < Types::BaseObject

  description 'A Symptom is a field available for Job creation when Symptoms feature is enabled for the Company. It can be used to hold a specific Symptom for the Job.'

  global_id_field :id
  implements GraphQL::Relay::Node.interface
  implements Interfaces::TimestampsInterface

  field :name, Types::SymptomNameType, description: 'Name of the Symptom.', null: true
  def name(**args)
    object
  end

  field :is_standard, Boolean, description: "Flag to mark a Symptom as Standard. Standard Symptoms are added to all companies by default, and can be removed. If this Symptom is not standard, it means it's a Company custom one.", null: true

end
