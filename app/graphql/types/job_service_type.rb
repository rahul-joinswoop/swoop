# frozen_string_literal: true

class Types::JobServiceType < Types::BaseObject

  description 'A service to be specified during Job creating.'

  field :answer_by,
        Types::DateTimeType,
        null: true

  field :answers,
        Types::QuestionResultType.connection_type,
        method: :question_results,
        null: true,
        connection: true,
        description: <<~DESCRIPTION
          Questions are a series of Service dependent questions to collect from the
          customer that help ensure the service provider arrives with the right
          equipment. Each Question has a Question ID that must be specified, though
          you may customize the exact text displayed to the customer. Swoop will
          expect one of the correlating possible answers when submitting the job.
          All Questions are required fields on a per Service basis.
        DESCRIPTION

  field :classType,
        Types::VehicleClassTypeType,
        null: true,
        method: :invoice_vehicle_category,
        description: "Class of partner vehicle needed to complete the job",
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::FLEET_CLASS_TYPE)
        }

  field :display_name,
        String,
        null: true,
        preload: :service_code,
        description: "Display name of the Service"

  def display_name
    parts = []
    parts << object.service_code&.name&.to_s
    (parts << "Storage") if object.will_store
    parts.join(", ")
  end

  field :name,
        Types::ServiceCodeNameType,
        method: :service_code,
        null: true,
        preload: :service_code,
        description: 'Name of the Service.'

  field :storage_site,
        Types::SiteNameType,
        null: true,
        preload: :storage_site,
        description: 'Name of the Storage Site'

  field :scheduled_for,
        Types::DateTimeType,
        null: true,
        description: 'Customer can optionally schedule the job for the future. Should be in ISO 8601 format.'

  field :storage_type,
        Types::StorageTypeNameType,
        method: :storage_type,
        null: true,
        preload: :storage_type,
        deprecation_reason: "Please use storageTypeName"

  field :storage_type_name,
        Types::StorageTypeNameType,
        method: :storage_type,
        null: true,
        preload: :storage_type,
        description: 'Name of the Storage Type'

  field :symptom,
        Types::SymptomNameType,
        null: true,
        preload: :symptom,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::SHOW_SYMPTOMS)
        },
        description: 'Name of the Symptom'

  field :will_store,
        Boolean,
        null: true,
        description: 'Whether or not to store the customers vehicle'

end
