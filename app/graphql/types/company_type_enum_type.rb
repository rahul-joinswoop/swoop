# frozen_string_literal: true

class Types::CompanyTypeEnumType < Types::BaseEnum

  graphql_name 'CompanyType'
  description 'Company Type'

  ["ClientCompany", "PartnerCompany", "SuperCompany"].each do |t|
    value(t, t, value: t)
  end

end
