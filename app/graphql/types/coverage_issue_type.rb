# frozen_string_literal: true

class Types::CoverageIssueType < Types::BaseObject

  description "Coverage Issue"

  field :issue, String, description: "Coverage Issue", null: true

  def issue
    object
  end

end
