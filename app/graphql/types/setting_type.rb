# frozen_string_literal: true

class Types::SettingType < Types::BaseObject

  description 'A Setting speficified for this Company'

  implements GraphQL::Relay::Node.interface

  global_id_field :id

  field :name, String, method: :key, description: 'Name of the Setting.', null: false

  # this can be null in DB :/ so let's set null: true to avoid errors
  field :value, String, description: 'Value of the Setting.', null: true

end
