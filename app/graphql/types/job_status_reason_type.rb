# frozen_string_literal: true

class Types::JobStatusReasonType < Types::BaseObject

  global_id_field :id
  field :notes_required, Boolean, null: false
  def notes_required
    object&.job_status&.id == JobStatus::REJECTED &&
    object&.text == ::JobStatusReasonType::REJECTED_OTHER[:text]
  end

  field :reason, String, null: false, method: :text
  field :status, Types::JobStatusEnumType, null: false
  def status
    object&.job_status&.status
  end

end
