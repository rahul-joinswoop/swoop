# frozen_string_literal: true

class Types::RateTypeType < Types::BaseObject

  description 'A Rate Type used as a base for the Invoice of a Job.'

  field :name, String, null: false
  field :type, Types::RateTypeEnumType, method: :friendly_type, null: false

end
