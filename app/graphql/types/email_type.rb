# frozen_string_literal: true

class Types::EmailType < Types::BaseScalar

  description "Email Address"

  def self.coerce_input(value, _ctx)
    if EmailAddress::EMAIL_REGEX.match(value)
      value
    else
      raise GraphQL::CoercionError, "#{value.inspect} is not a valid Email"
    end
  end

  # TODO - once we enforce validations on phone numbers in the db we can remove
  # this i think?
  def self.coerce_result(value, _ctx)
    value.to_s
  end

end
