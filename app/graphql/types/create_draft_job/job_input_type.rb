# frozen_string_literal: true

class Types::CreateDraftJob::JobInputType < Types::JobInputType

  graphql_name "CreateDraftJobJobInput"

end
