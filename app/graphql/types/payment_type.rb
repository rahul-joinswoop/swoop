# frozen_string_literal: true

class Types::PaymentType < Types::BaseObject

  graphql_name 'PaymentType'

  description 'The PaymentType used on a Job.'

  global_id_field :id

  field :name, String, description: 'The name of the PaymentType.', null: false

end
