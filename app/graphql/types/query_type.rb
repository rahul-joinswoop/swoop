# frozen_string_literal: true

#
# POST http://localhost:5000/graphql
# {
#   testField
# }
#
# nested array limits, metadata

class Types::QueryType < Types::BaseObject

  # Add root-level fields here.
  # They will be entry points for queries on your schema.
  implements Interfaces::CompanyCommonInterface

  # Get an object(node) by id
  # query getByID($id: ID!) {
  #   node(id: $id) {
  #     id
  #     ... on Job {
  #       created_at
  #       status
  #       location {
  #         drop_location {
  #           address
  #         }
  #       }
  #     }
  #   }
  # }

  add_field GraphQL::Types::Relay::NodeField
  add_field GraphQL::Types::Relay::NodesField

  field :viewer,
        Types::ViewerUnionType,
        resolver: Resolvers::Viewer,
        null: false

end
