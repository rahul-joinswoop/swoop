# frozen_string_literal: true

class Types::VehicleMakeType < Types::BaseObject

  global_id_field :id

  field :name,
        String,
        description: "Name of the VehicleMake",
        null: true

  field :edmunds_id,
        Integer,
        description: "Edmunds ID.",
        null: true

  field :original,
        Boolean,
        description: "Original",
        null: true

  field :models,
        Types::VehicleModelType.connection_type,
        description: "Vehicle Models",
        null: true,
        connection: true,
        resolver: Resolvers::VehicleModels do
    argument :query, String, "Search String", required: false
  end

  field :model,
        Types::VehicleModelType,
        description: "Vehicle Model",
        null: true,
        resolver: Resolvers::VehicleModel do
    argument :id, ID, "Vehicle Model ID", prepare: Resolvers::GlobalId.new, required: false
    argument :name, String, "Vehicle Model Name", required: false
  end

end
