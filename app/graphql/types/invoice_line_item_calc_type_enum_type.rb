# frozen_string_literal: true

class Types::InvoiceLineItemCalcTypeEnumType < Types::BaseEnum

  description 'Set the line item calc type. It can be flat or percentage.'

  InvoicingLineItem::CALC_TYPES.each do |value|
    value(value.capitalize, value, value: value)
  end

end
