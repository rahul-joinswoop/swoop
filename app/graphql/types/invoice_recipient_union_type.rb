# frozen_string_literal: true

class Types::InvoiceRecipientUnionType < Types::BaseUnion

  graphql_name "InvoiceRecipient"
  description "Recipient of the Invoice"
  possible_types(
    Types::UserType,
    Types::AccountType,
  )
  def self.resolve_type(value, ctx)
    case value
    when User
      Types::UserType
    when Account
      Types::AccountType
    else
      raise("Unexpected object: #{value}")
    end
  end

end
