# frozen_string_literal: true

class Types::RefererType < Types::BaseObject

  description 'The referer chosen while a PartnerCompany is created by users on Swoop website.'

  global_id_field :id

  field :name, String, description: 'The referer name.', null: false

end
