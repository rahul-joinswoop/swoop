# frozen_string_literal: true

class Types::CoverageProgramType < Types::BaseObject

  field :code,
        String,
        null: true,
        description: "Client code",
        required_roles: :super_fleet_managed

  def code
    object.data.first["code"]
  end

  field :end_at,
        Types::DateType,
        null: true,
        description: "End date for the applicable Program",
        preload: :pcc_policy,
        required_roles: :super_fleet_managed

  def end_at
    Date.parse get_program_from_policy.dig("end_date")
  rescue StandardError
    nil
  end

  field :name,
        String,
        null: true,
        description: 'Program name',
        required_roles: :fleet

  def name
    object.data.first["name"]
  end

  field :start_at,
        Types::DateType,
        null: true,
        description: "Purchase/Start date for the applicable Program",
        preload: :pcc_policy,
        required_roles: :super_fleet_managed

  def start_at
    Date.parse get_program_from_policy.dig("start_date")
  rescue StandardError
    nil
  end

  field :swcid,
        Integer,
        null: true,
        description: "Program ID",
        method: :client_program_id,
        required_roles: :fleet

  private

  def get_program_from_policy
    (object&.pcc_policy&.data&.dig("programs")&.find { |p| p["program_name"] == object.data.first["name"] }).to_h
  end

end
