# frozen_string_literal: true

class Types::JobCallerInputType < Types::BaseInputObject

  argument :name, String, description: "Name of the customer.", required: false
  argument :phone, Types::E164PhoneNumberType, description: "Phone number of the customer.", required: false

end
