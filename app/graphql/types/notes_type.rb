# frozen_string_literal: true

class Types::NotesType < Types::BaseObject

  field :customer, String, method: :notes, description: "Notes about the customer's situation that will be passed to the service provider. This is an optional field.", null: true
  field :internal, String, null: true, description: "Internal notes about the Job"

  def internal
    object.internal_notes context[:api_company]
  end

  field :dispatcher, String, null: true, required_roles: :rescue, resolver: Resolvers::DispatcherNotes

end
