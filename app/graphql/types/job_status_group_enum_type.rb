# frozen_string_literal: true

class Types::JobStatusGroupEnumType < Types::BaseEnum

  graphql_name 'JobStatusGroup'
  description <<~DESCRIPTION
    Each value represents a group of `JobStatuses` to query on.

    **Draft**: #{::Job.draft_states.map { |j| "`#{j.gsub(/\s+/, '')}`" }.join(", ")}

    **Active**: #{::Job.active_states.map { |j| "`#{j.gsub(/\s+/, '')}`" }.join(", ")}

    **Done**: #{::Job.done_states.map { |j| "`#{j.gsub(/\s+/, '')}`" }.join(", ")}

    **Map**: #{::Job.map_states.map { |j| "`#{j.gsub(/\s+/, '')}`" }.join(", ")}

    **All**: All `JobStatuses`
  DESCRIPTION

  Job::JOB_STATUS_GROUPS.each do |k, v|
    value v[:name], v[:description], value: k
  end

end
