# frozen_string_literal: true

class Types::InvoiceRejectionType < Types::BaseObject

  description 'An InvoiceRejection.'

  global_id_field :id

  # TODO - do we want this connection?
  # field :invoice, !Types::InvoiceType do
  #   description 'The Invoice for this Rejection.'
  # end

  # TODO need to add this type
  # field :reason, Types::InvoiceRejectReasonType, property: :invoice_reject_reason
  #   description 'The Reason for this Rejection.'
  # end

  field :notes, String, description: 'Details about the Invoice Rejection.', null: true

end
