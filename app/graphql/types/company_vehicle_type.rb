# frozen_string_literal: true

class Types::CompanyVehicleType < Types::BaseObject

  field :makes,
        Types::VehicleMakeType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::VehicleMakes do
    argument :query, String, "Search String", required: false
  end

  field :make,
        Types::VehicleMakeType,
        null: true,
        resolver: Resolvers::VehicleMake do
    argument :id, ID, "Vehicle Make ID", prepare: Resolvers::GlobalId.new, required: false
    argument :name, String, "Vehicle Make Name", required: false
  end

  field :styles,
        Types::VehicleStyleType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::VehicleStyles do
    argument :query, String, "Search String", required: false
  end

end
