# frozen_string_literal: true

class Types::PolicyType < Types::BaseObject

  implements GraphQL::Relay::Node.interface
  global_id_field :id

  def self.authorized?(object, context)
    super &&
    case context[:api_company]
    when FleetCompany
      job = object.job
      if job.blank?
        false
      elsif context.is_super_client_managed?
        job.is_a?(::FleetManagedJob)
      else
        job.fleet_company_id == context[:api_company].id
      end
    else
      false
    end
  end

  field :number,
        String,
        null: true,
        description: "Policy Number",
        method: :policy_number,
        required_roles: :fleet

  field :swcid,
        Integer,
        null: true,
        description: "Policy ID",
        method: :id,
        required_roles: :fleet

end
