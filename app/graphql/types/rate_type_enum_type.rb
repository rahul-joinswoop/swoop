# frozen_string_literal: true

class Types::RateTypeEnumType < Types::BaseEnum

  description 'A RateType representation, used to change the RateType of an Invoice'

  # Best if we'd be fetching it from DB. But then it screws the specs,
  # since this enum type is built during Rails application boot and there's
  # no data in test db during the boot.
  #
  # So for now we're using the RateType::ALL_RATE_TYPES const.
  ::RateType::ALL_RATE_TYPES.each do |rate_type|
    value(
      rate_type[:friendly_type], # HoursPortToPort
      rate_type[:name], # "Hourly (Port to Port)"
    )
  end

end
