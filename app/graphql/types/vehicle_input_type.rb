# frozen_string_literal: true

class Types::VehicleInputType < Types::BaseInputObject

  argument :color, String, description: "Vehicle color", required: false
  argument :license, String, description: "Vehicle license", required: false
  argument :make, String, description: "Vehicle make", required: false
  argument :model, String, description: "Vehicle model", required: false
  argument :odometer, GraphQL::Types::BigInt, description: "Vehicle odometer", required: false
  argument :serial_number, String, description: "Serial Number", required: false, required_roles: :rescue
  argument :style, String, description: "Style", required: false, required_roles: :rescue
  argument :tire_size, String, description: "Tire Size", required: false, required_roles: :rescue
  argument :unit_number, String, description: "Unit Number", required: false, required_roles: :rescue
  argument :vin, String, description: "Vehicle vin", required: false
  argument :year, Integer, description: "Vehicle year", required: false

end
