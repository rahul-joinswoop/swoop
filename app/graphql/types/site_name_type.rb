# frozen_string_literal: true

# Type used for coercing a site name into an object and back.
class Types::SiteNameType < Types::BaseScalar

  description 'Name of the site'

  class << self

    def coerce_input(value, context)
      site = Resolvers::Sites.new(object: context[:api_company], context: context).resolve.find_by(name: value)
      unless site
        raise GraphQL::CoercionError, "#{value.inspect} is not a valid Site"
      end
      site
    end

    def coerce_result(value, _ctx)
      value&.name&.to_s
    end

  end

end
