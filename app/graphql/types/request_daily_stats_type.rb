# frozen_string_literal: true

class Types::RequestDailyStatsType < Types::BaseObject

  field :date,
        Types::DateType,
        null: false,
        description: 'Date stats were collected'

  def date
    object[0]
  end

  field :requests,
        Integer,
        null: false,
        description: "Total API requests"

  def requests
    object[1].to_i
  end

end
