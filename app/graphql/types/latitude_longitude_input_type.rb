# frozen_string_literal: true

class Types::LatitudeLongitudeInputType < Types::BaseInputObject

  graphql_name "LatitudeLongitudeInput"
  argument :lat, Float, required: true
  argument :lng, Float, required: true

end
