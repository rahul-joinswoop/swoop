# frozen_string_literal: true

class Types::StorageTypeType < Types::BaseObject

  description "A StorageType is an attribute of a Storage job."

  global_id_field :id

  field :name, String, description: "Name of the StorageType.", null: false

end
