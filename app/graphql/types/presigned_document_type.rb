# frozen_string_literal: true

class Types::PresignedDocumentType < Types::BaseObject

  description "PresignedDocument"

  field :id, ID, null: false, hash_key: :id
  field :storage, String, null: false, hash_key: :storage
  field :url, String, null: false, hash_key: :url

end
