# frozen_string_literal: true

class Types::TireType < Types::BaseObject

  description 'Tire to be used by specific Companies on Job.'

  global_id_field :id

  field :tire_type, Types::TireModelType, description: 'TireModel type.', null: true

end
