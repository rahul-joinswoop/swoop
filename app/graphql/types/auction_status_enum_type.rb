# frozen_string_literal: true

class Types::AuctionStatusEnumType < Types::BaseEnum

  graphql_name 'AuctionStatus'
  description 'Auction Status'

  Auction::Auction::NAME_TO_STATUS_MAP.each do |k, v|
    value(k, k, value: v)
  end

end
