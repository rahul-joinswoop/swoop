# frozen_string_literal: true

class Types::ServiceType < Types::BaseObject

  description 'A service to be specified during Job creating.'

  global_id_field :id

  field :name, Types::ServiceCodeNameType, description: 'Name of the Service.', null: true
  def name(**args)
    object
  end

  field :is_standard, Boolean, description: "Flag to mark a Service as Standard. Standard services are added to all companies by default, and can be removed. If this service is not standard, it means it's a Company custom one.", null: true

end
