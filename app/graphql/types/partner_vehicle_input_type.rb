# frozen_string_literal: true

class Types::PartnerVehicleInputType < Types::BaseInputObject

  argument :name, String, required: true

end
