# frozen_string_literal: true

class Types::DeprecatedFieldsStatsType < Types::BaseObject

  field :daily,
        Types::DeprecatedFieldsDailyStatsType.connection_type,
        null: false,
        method: :graphql_deprecated_fields,
        max_page_size: 30,
        description: "Deprecated field usage from the last 30 days bucketed by date"

end
