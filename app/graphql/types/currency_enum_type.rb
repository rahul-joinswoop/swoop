# frozen_string_literal: true

class Types::CurrencyEnumType < Types::BaseEnum

  graphql_name 'Currency'
  description 'Currency'

  Money::Currency.all.each do |c|
    value c.iso_code, c.name
  end

end
