# frozen_string_literal: true

class Types::AddJob::ServiceInputType < Types::CreateJob::ServiceInputType

  graphql_name "AddJobServiceInput"

end
