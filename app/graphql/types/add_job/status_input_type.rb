# frozen_string_literal: true

class Types::AddJob::StatusInputType < Types::CreateJob::StatusInputType

  graphql_name "AddJobStatusInput"

end
