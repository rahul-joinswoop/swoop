# frozen_string_literal: true

class Types::AddJob::JobLocationInputType < Types::CreateJob::JobLocationInputType

  graphql_name "AddJobJobLocationInput"

end
