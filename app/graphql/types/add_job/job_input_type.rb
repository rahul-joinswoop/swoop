# frozen_string_literal: true

class Types::AddJob::JobInputType < Types::JobInputType

  graphql_name "AddJobJobInput"

  argument :location, Types::AddJob::JobLocationInputType, required: true
  argument :service, Types::AddJob::ServiceInputType, required: true
  argument :status, Types::AddJob::StatusInputType, required: false, description: "Deprecated, please use the createDraftJob mutation instead"

end
