# frozen_string_literal: true

class Types::CompanyQuestionType < Types::BaseObject

  description 'This represents a relationship between a company and its questions based on a chosen Service.' \
    'Used on Job.'

  field :service, Types::ServiceType, method: :service_code, description: 'The Service chosen for the Job.', null: true
  field :question, Types::QuestionType, description: 'An available Question for the Service chosen for the Job.', null: true

end
