# frozen_string_literal: true

class Types::JobEtaType < Types::BaseObject

  field :bidded, Types::DateTimeType, null: true, required_roles: :rescue do
    description "The bidded ETA (if any) from the Partner"
  end

  def bidded
    # i have no idea why this is necessary but if we're modifying toa's (and bta specifically) in
    # our mutation that change won't show up unless we reload our job here. probably has something to do with
    # how we create the toa and/or the fact that we don't have inverse_of: setup correctly for those AR relationships
    object.reload if context.query.mutation?
    object.bta&.time
  end

  field :current,
        Types::DateTimeType,
        null: true,
        required_roles: :any,
        description: "The current ETA for the PartnerVehicle"
  def current
    object.latest_toa&.time
  end

  field :max_bidded_without_explanation, Integer, method: :max_bidded_eta_without_explanation, null: true, required_roles: :rescue do
    description 'Max ETA in minutes for Accepting a Job without requiring an explanation/reason.'
  end

end
