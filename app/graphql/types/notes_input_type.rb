# frozen_string_literal: true

class Types::NotesInputType < Types::BaseInputObject

  argument :customer, String, description: "Notes about the customer's situation that will be passed to the service provider. This is an optional field.", required: false
  argument :internal, String, required: false

end
