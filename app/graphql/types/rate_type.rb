# frozen_string_literal: true

class Types::RateType < Types::BaseObject

  include Utils::Currency

  description 'A Rate used as a base for the Invoice of a Job.'

  global_id_field :id
  implements Interfaces::TimestampsInterface

  field :type, String, description: 'The Rate type.', null: false
  field :company, Types::CompanyType, description: 'The Company who owns this Rate.', null: false, resolver: Resolvers::Company

  field :service, Types::ServiceType, description: 'The Service associated with this Rate.', null: true
  # RG is this attr used?
  field :site, Types::SiteType, description: 'TBD', null: true

  field :account, Types::AccountType, description: 'The Account associated with this Rate.', null: true
  field :class_type,
        Types::VehicleClassTypeType,
        description: 'A VehicleClassTypeType associated with this Rate.',
        null: true,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::FLEET_CLASS_TYPE)
        }

  def class_type
    object.vehicle_category
  end

  field :currency,
        Types::CurrencyEnumType,
        description: "Currency used for this Rate",
        null: false

  field :live, Boolean, description: 'Flags if this Rate will be used as default for its specific Service and ClassType.', null: true
  field :round, String, description: 'TBD', null: true
  field :nearest, String, description: 'TBD', null: true
  # RG is this attr used?
  field :service_group_id, Integer, description: 'TBD', null: true
  # RG is this attr used?
  field :account_group_id, Integer, description: 'TBD', null: true
  # RG is this attr used? Which rate has it exactly? I could not find it...
  field :priority, Integer, description: 'TBD', null: true

  def priority
    if object.has_attribute? :priority
      object.priority
    else
      nil
    end
  end
  field :hourly, String, description: 'Describe the value of this Rate when this is Hourly calculated.', null: true

  def hourly
    currency_value_or_nil(object.hourly)
  end
  field :flat, String, description: 'Describe the value of this Rate when this is Flat calculated.', null: true

  def flat
    currency_value_or_nil(object.flat)
  end
  field :hookup, String, description: 'Describe the value of this Rate when this is calculated based on Hookup.', null: true

  def hookup
    currency_value_or_nil(object.hookup)
  end
  field :miles_towed, String, description: 'Describe the value of this Rate when this is calculated based on Miles Towed.', null: true

  def miles_towed
    currency_value_or_nil(object.miles_towed)
  end
  field :miles_towed_free, String, description: 'Describe the amount of Miles Towed Free for this Rate.', null: true

  def miles_towed_free
    currency_value_or_nil(object.miles_towed_free)
  end
  field :miles_enroute, String, description: 'Describe the value of this Rate when this is calculated based on Miles Enroute.', null: true

  def miles_enroute
    currency_value_or_nil(object.miles_enroute)
  end
  field :miles_enroute_free, String, description: 'Describe the amount of Miles Enroute Free for this Rate.', null: true

  def miles_enroute_free
    currency_value_or_nil(object.miles_enroute_free)
  end
  field :miles_p2p, String, description: 'Describe the value of this Rate when this is calculated based on Miles P2P.', null: true

  def miles_p2p
    currency_value_or_nil(object.miles_p2p)
  end
  field :miles_p2p_free, String, description: 'Describe the amount of Miles P2P Free for this Rate.', null: true

  def miles_p2p_free
    currency_value_or_nil(object.miles_p2p_free)
  end
  field :miles_deadhead, String, description: 'Describe the value of this Rate when this is calculated based on Miles Deadhead.', null: true

  def miles_deadhead
    currency_value_or_nil(object.miles_deadhead)
  end
  field :miles_deadhead_free, String, description: 'Describe the amount of Miles Deadhead Free for this Rate.', null: true

  def miles_deadhead_free
    currency_value_or_nil(object.miles_deadhead_free)
  end
  field :special_dolly, String, description: 'Describe the value of this Rate when this is calculated based on Special Dolly.', null: true

  def special_dolly
    currency_value_or_nil(object.special_dolly)
  end
  field :gone, String, description: 'Describe the value of this Rate when this is calculated when customer is gone.', null: true

  def gone
    currency_value_or_nil(object.gone)
  end
  field :storage_daily, String, description: "Describe the value per day of this Rate when it's a Storage rate.", null: true

  def storage_daily
    currency_value_or_nil(object.storage_daily)
  end
  field :second_day_charge, String, description: "TBD", null: true

  def second_day_charge
    if object.has_attribute? :second_day_charge
      object.second_day_charge
    else
      nil
    end
  end
  field :beyond_charge, String, description: "TBD", null: true

  def beyond_charge
    if object.has_attribute? :beyond_charge
      object.beyond_charge
    else
      nil
    end
  end
  field :seconds_free_window, Integer, description: "Time window free of charge.", null: true

  def seconds_free_window
    if object.has_attribute? :seconds_free_window
      object.seconds_free_window
    else
      nil
    end
  end

  field :additions, Types::RateAdditionType.connection_type, null: true, connection: true

end
