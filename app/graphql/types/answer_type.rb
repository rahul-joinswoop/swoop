# frozen_string_literal: true

class Types::AnswerType < Types::BaseObject

  description 'This describes an answer for a question used on Job.'

  global_id_field :id

  field :answer, String, description: 'The answer for a question on a determined Job.', null: true
  field :needs_extra, Boolean, method: :needs_info?, description: 'Whether or not this answer requires the Extra input field', null: true

end
