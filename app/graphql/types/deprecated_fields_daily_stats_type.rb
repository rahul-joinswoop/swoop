# frozen_string_literal: true

class Types::DeprecatedFieldsDailyStatsType < Types::BaseObject

  field :date,
        Types::DateType,
        null: false,
        description: 'Date stats were collected'

  def date
    object[0]
  end

  field :fields,
        Types::DeprecatedFieldStatsType.connection_type,
        null: false,
        description: "Deprecated fields used"
  def fields
    object[1]
  end

end
