# frozen_string_literal: true

class Types::InvoiceRejectReasonType < Types::BaseObject

  description 'The InvoiceRejection reason, specific by Company.'

  global_id_field :id

  field :text, String, description: 'The reason of the Rejection.', null: false
  field :company, Types::CompanyType, description: 'The Company who owns this InvoiceRejection.', null: false, resolver: Resolvers::Company

end
