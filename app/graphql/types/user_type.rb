# frozen_string_literal: true

class Types::UserType < Types::BaseObject

  implements GraphQL::Relay::Node.interface
  implements Interfaces::JobsInterface

  description <<~DESCRIPTION
    The User representation. Can be a user of Swoop (with Roles), a Customer of the Job,
    a PartnerDriver who's executing the Job.
  DESCRIPTION

  global_id_field :id

  field :swcid,
        Integer,
        null: true,
        method: :id,
        description: "Swoop Client ID"

  field :first_name,
        String,
        null: true,
        resolver: Resolvers::PartnerUser.build(:first_name)

  field :full_name,
        String,
        null: true,
        resolver: Resolvers::PartnerUser.build(:full_name),
        deprecation_reason: "Please use name instead"

  field :name,
        String,
        null: true,
        resolver: Resolvers::PartnerUser.build(:full_name)

  # TODO - when i add a resolver here i get an error:
  #
  # GraphQL::Schema::InvalidTypeError: Field JobStoredVehicle.releasedToUser's return type is invalid: field "onDuty" type must return GraphQL::BaseType, not NilClass (nil)
  #
  # no clue how/why that happens, for now leaving this open (public) but we really need to fix it.
  field :on_duty,
        Boolean,
        null: true,
        connection: false,
        method: :on_duty do
    description <<~DESCRIPTION
      Applicable when User is a Driver. If true, it means the Driver is available
      to receive dispatchable Jobs.
    DESCRIPTION
  end

  field :phone,
        Types::E164PhoneNumberType,
        null: true,
        resolver: Resolvers::PartnerUser.build(:phone)

  field :user_name,
        Types::EmailType,
        null: true,
        resolver: Resolvers::PartnerUser.build(:username)

  # all of these fields are only visible to the user themselves or anyone else in their company
  # TODO - maybe only to admin/dispatchers?
  # TODO - do client companies have users as well?
  field :company,
        Types::CompanyType,
        null: true,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:company),
        description: "Applicable to Users of Swoop. A Company associated with this User."

  field :deleted_at,
        String,
        null: true,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:deleted_at)

  field :email,
        Types::EmailType,
        null: true,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:email)

  field :language, String, description: 'Language selection for this user.', null: false

  field :last_name,
        String,
        null: true,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:last_name)

  field :location,
        Types::LocationType,
        description: "TBD",
        null: true,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:location)

  field :member_number,
        String,
        null: true,
        required_features: Feature::JOB_MEMBER_NUMBER,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:member_number),
        description: "Customer's Member Number"

  field :grantable_roles,
        Types::RoleType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::MyselfOnly.build(:grantable_roles),
        description: "Roles that can be granted to other Users",
        required_roles: [:admin]

  field :roles,
        Types::RoleType.connection_type,
        null: true,
        connection: true,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:roles),
        description: "Applicable to Users of Swoop. Specify the roles of the User."

  field :vehicle,
        Types::PartnerVehicleType,
        null: true,
        preload: :vehicle,
        required_roles: :rescue,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:vehicle)

  field :subscriptions,
        Types::WebhookSubscriptionType.connection_type,
        null: true,
        resolver: Resolvers::MyselfOrMyCompanyOnly.build(:graphql_subscriptions)

end
