# frozen_string_literal: true

class Types::JobStoredVehicleType < Types::BaseObject

  global_id_field :id
  implements Interfaces::TimestampsInterface

  field :site, Types::SiteType, null: true
  field :job, Types::JobType, null: true
  field :type, String, null: true
  field :stored_at, Types::DateTimeType, null: true
  field :released_at, Types::DateTimeType, null: true
  field :released_to_account, Types::AccountType, null: true
  field :released_to_user, Types::UserType, null: true
  field :replace_invoice_info, String, null: true
  field :released_to_email, Types::EmailType, null: true

end
