# frozen_string_literal: true

class Types::BigDecimalType < Types::BaseScalar

  def self.coerce_input(value, _ctx)
    BigDecimal.new(value)
  end

  def self.coerce_result(value, _ctx)
    value&.to_s
  end

end
