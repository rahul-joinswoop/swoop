# frozen_string_literal: true

class Types::JobsFiltersInputType < Types::BaseInputObject

  class JobsClientCompanyFilterInputType < Types::BaseInputObject

    argument :id,
             ID,
             required: true,
             description: 'Client Company ID',
             required_roles: :super_fleet_managed

  end

  class JobsClientFilterInputType < Types::BaseInputObject

    argument :company,
             JobsClientCompanyFilterInputType,
             required: false,
             description: "ClientCompany"

  end

  argument :client,
           JobsClientFilterInputType,
           required: false

  argument :created_at,
           Types::DateTimeRangeInputType,
           required: false,
           description: 'createdAt DateTime range for Jobs'

  argument :states,
           Types::JobStatusGroupEnumType,
           # LOLWUT this has to be in sync with the default value set
           # on the states field in CompanyCommonInterface#jobs because
           # reasons, see
           # https://github.com/rmosolgo/graphql-ruby/issues/1374
           default_value: :active_states,
           required: false

end
