# frozen_string_literal: true

class Types::DateType < Types::BaseScalar

  def self.coerce_input(value, _ctx)
    Date.parse(value)
  end

  def self.coerce_result(value, _ctx)
    value.iso8601
  end

end
