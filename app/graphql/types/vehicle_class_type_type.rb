# frozen_string_literal: true

class Types::VehicleClassTypeType < Types::BaseObject

  description "The Vehicle Class Type."

  global_id_field :id

  field :name, String, description: "Name of the class type", null: false

  field :order_num, Integer, description: "Order of the class types list.", null: false

  field :is_standard, Boolean,
        description: "Flag to mark a class type as standard. Standard class types are added to all companies by default, and can be removed. If this class type is not standard, it means it is a company custom one.",
        null: false

  # we should probably have an enum type here to be used as inputs of queries and mutations;
  # but class_types are dinamically added/removed in the db, and the Enum type needs to be
  # static so it won't work; we may want to revisit it in the future.

end
