# frozen_string_literal: true

class Types::InvoiceLineItemType < Types::BaseObject

  include Utils::Currency

  description 'A Line Item of an Invoice.'

  class InvoiceLineItemJobType < Types::BaseObject

    global_id_field :id

  end

  #
  # We explicitly use a String value for the id instead of global_id_field, to be able to support sample invoices.
  #
  # The line_items in a sample invoice may not have been persisted in db yet,
  #  therefore we need to build a fake one when rendering it back to clients, otherwise it explodes
  #
  field :id, String, null: false
  def id
    if object.id
      object.to_ssid
    else
      SecureRandom.hex
    end
  end

  field :auto_calculated_quantity,
        Boolean,
        description: 'Used to flag whether Storage item quantity is auto_calculated (true) or changed by the user (false)',
        null: false
  def auto_calculated_quantity
    object.auto_calculated_quantity.to_bool
  end

  field :calc_type,
        Types::InvoiceLineItemCalcTypeEnumType,
        description: 'The Calc Type to be used for this LineItem. Can be percentage or flat.',
        null: false

  field :currency,
        Types::CurrencyEnumType,
        description: "Currency used for this LineItem",
        null: false

  field :description, String, description: 'LineItem description.', null: true

  field :deleted_at, Types::DateTimeType, description: 'Date and time when this LineItem was deleted.', null: true

  field :estimated,
        Boolean,
        description: 'Flags it as estimated by the system. If `false`, it means the PartnerCompany Admin has changed its original amount or quantity.',
        null: false
  def estimated
    object.estimated.to_bool
  end

  field :job, InvoiceLineItemJobType, description: 'Job id associated with this LineItem', null: false

  field :net_amount, String, description: 'Net amount of this LineItem. It will likely be quantity multiplied by unit_price.', null: true
  def net_amount
    currency_value_or_zero(object.net_amount)
  end

  field :original_quantity, String, description: 'Original quantity of this LineItem.', null: true
  def original_quantity
    currency_value_or_zero(object.original_quantity)
  end

  field :original_unit_price, String, description: 'Original Unit Price inherited by the Rate from where this LineItem was based on.', null: true
  def original_unit_price
    currency_value_or_zero(object.original_unit_price)
  end

  field :quantity, String, description: 'Quantity of this LineItem.', null: true
  def quantity
    currency_value_or_zero(object.quantity)
  end

  field :rate, Types::RateType, description: 'Rate from where this LineItem auto-calculated value was based on.', null: true

  field :tax_amount, String, description: 'Tax amount of this LineItem.', null: true
  def tax_amount
    currency_value_or_zero(object.tax_amount)
  end

  field :unit_price, String, description: 'Unit Price inherited by the Rate from where this LineItem was based on.', null: true
  def unit_price
    currency_value_or_zero(object.unit_price)
  end

  field :user_created,
        Boolean,
        description: 'Flags as user created. It means it was not auto-calculated by the system, but added manually by the PartnerCompany Admin.',
        null: false
  def user_created
    object.user_created.to_bool
  end

end
