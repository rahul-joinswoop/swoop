# frozen_string_literal: true

class Types::JobPartnerType < Types::BaseObject

  # this is the partner displayed in a job, it's a subset of company. it should
  # only be visible to the fleet_company and partner_company, and some fields are
  # hidden from the fleet company.
  def self.authorized?(object, context)
    super &&
    case context[:api_company]
    when FleetCompany
      return object.fleet_company_id == context[:api_company].id
    when RescueCompany
      return object.rescue_company_id == context[:api_company].id
    when SuperCompany
      case context[:api_company].id
      when ::Company.swoop_id
        # swoop root users can see everything
        if context.has_role?(:root)
          return true
        else
          # swoop dispatchers can see all FleetManagedJobs
          return object.type == 'FleetManagedJob'
        end
      end
    when NilClass
      return object.customer == context[:api_user]
    end
    # fall through here
    false
  end

  field :can_request_more_time,
        Boolean,
        null: false,
        required_roles: :rescue,
        method: :can_request_more_time?

  field :can_request_callback,
        Boolean,
        null: false,
        required_roles: :rescue,
        method: :can_request_callback?

  field :company,
        Types::CompanyType,
        null: true,
        method: :rescue_company,
        preload: :rescue_company

  field :department,
        Types::DepartmentType,
        null: true,
        method: :partner_department,
        preload: :partner_department,
        required_roles: :rescue

  field :driver,
        Types::UserType,
        null: true,
        preload: :rescue_driver

  def driver
    if context.is_client?
      # clients can only see the driver object if a job is enroute or farther and not in a terminal state
      # (completed, goa, cancelled)
      return nil unless ::Job.statuses[object.status].in?(::Job.fleet_show_rescue_vehicle_states)
      return nil if object.rescue_driver.blank?
      # and even if they can see the driver, they don't get the driver's last name
      object.rescue_driver.full_name = object.rescue_driver.first_name
    end
    object.rescue_driver
  end

  field :name,
        String,
        description: 'The name of the Partner.',
        null: true,
        preload: :rescue_company,
        deprecation_reason: "Please use job.partner.company.name instead"
  def name
    object&.rescue_company&.name
  end

  field :phone,
        Types::E164PhoneNumberType,
        description: 'Company phone.',
        null: true,
        preload: :rescue_company,
        deprecation_reason: "Please use job.partner.company.phone instead"
  def phone
    object&.rescue_company&.phone
  end

  field :vehicle,
        Types::PartnerVehicleType,
        null: true,
        preload: { rescue_vehicle: [:driver] },
        resolver: Resolvers::JobPartnerVehicle,
        required_roles: :any

  field :trailer,
        Types::TrailerType,
        null: true,
        method: :trailer_vehicle,
        preload: :trailer_vehicle,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::TRAILERS)
        }

end
