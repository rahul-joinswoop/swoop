# frozen_string_literal: true

class Types::DistanceUnitEnumType < Types::BaseEnum

  graphql_name 'DistanceUnit'
  description 'Distance Unit'
  value 'mi', 'Mile'
  value 'km', 'Kilometer'

end
