# frozen_string_literal: true

# Type used for coercing a department name into an object and back.
class Types::DepartmentNameType < Types::BaseScalar

  description "Name of the department"

  class << self

    def coerce_input(value, context)
      department = Resolvers::Departments.new(object: context[:api_company], context: context).resolve.find_by(name: value)
      unless department
        raise GraphQL::CoercionError, "#{value.inspect} is not a valid Department"
      end
      department
    end

    def coerce_result(value, _ctx)
      value&.name&.to_s
    end

  end

end
