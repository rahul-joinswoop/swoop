# frozen_string_literal: true

class Types::DocumentContainerEnumType < Types::BaseEnum

  graphql_name 'DocumentContainerType'
  description 'Document Container'

  value 'Dropoff', 'Dropoff', value: 'dropoff'
  value 'Pickup', 'Pickup', value: 'pickup'
  value 'Release', 'Release', value: 'release'

end
