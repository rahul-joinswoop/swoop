# frozen_string_literal: true

class Types::EditJob::JobInputType < Types::UpdateJob::JobInputType

  graphql_name "EditJobJobInput"

end
