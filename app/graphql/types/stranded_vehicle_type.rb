# frozen_string_literal: true

class Types::StrandedVehicleType < Types::BaseObject

  implements Interfaces::VehicleInterface
  implements GraphQL::Relay::Node.interface

  description "A customer Vehicle specified on the Job"

  field :tire_size, String, null: true do
    description "Applicable on Jobs. The Tire Size of the Vehicle."
  end

  field :class_type,
        Types::VehicleClassTypeType,
        method: :vehicle_category,
        null: true,
        description: "The ClassType of the Vehicle."

  def license
    if context.is_partner? && object.drive.last&.job&.auto_assigning?
      ''
    else
      object.license
    end
  end

  # only StrandedVehicle's have an odometer reading
  field :odometer, GraphQL::Types::BigInt, null: true
  def odometer
    # TODO - what if a vehicle has more than one drive(r)?
    object.drive.last&.job&.odometer
  end

  field :serial_number,
        String,
        null: true,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::SERIAL_NUMBER)
        },
        description: "Serial Number"

  field :style,
        String,
        null: true,
        method: :vehicle_type,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::HEAVY_DUTY_EQUIPMENT)
        },
        description: "The Style of the Vehicle."

  field :unit_number,
        String,
        null: true,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::JOB_UNIT_NUMBER)
        },
        description: "Unit Number"
  def unit_number
    object.drive.last&.job&.unit_number
  end

end
