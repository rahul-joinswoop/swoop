# frozen_string_literal: true

class Types::AccountFilterEnumType < Types::BaseEnum

  graphql_name 'AccountFilter'
  description 'Filters for Accounts'

  value('All', 'All accounts', value: :all)
  value('Create', 'Accounts valid for Job creation', value: :create)

end
