# frozen_string_literal: true

class Types::JobTypeEnumType < Types::BaseEnum

  graphql_name 'JobType'
  description 'Job Type'

  [::FleetInHouseJob, ::FleetManagedJob, ::FleetMotorClubJob, ::RescueJob].each do |t|
    value(t.name, t.name, value: t.name)
  end

end
