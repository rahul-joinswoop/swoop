# frozen_string_literal: true

class Types::E164PhoneNumberType < Types::BaseScalar

  description "E164-formatted phone number"

  def self.coerce_input(value, _ctx)
    parsed = Phonelib.parse(value).e164
    if PhoneNumber::E164_REGEX.match(parsed)
      parsed
    else
      raise GraphQL::CoercionError, "#{value.inspect} is not a valid phone number"
    end
  end

  # TODO - once we enforce validations on phone numbers in the db we can remove
  # this i think?
  def self.coerce_result(value, _ctx)
    value.to_s
  end

end
