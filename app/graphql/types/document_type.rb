# frozen_string_literal: true

class Types::DocumentType < Types::BaseObject

  implements GraphQL::Relay::Node.interface
  global_id_field :id
  implements Interfaces::TimestampsInterface
  description "Document"

  # you can view a document if:
  # 1. it's for your api_user
  # 2. it's for your api_company
  # 3. your company is the fleet company on the associated job
  # 4. your company is the rescue company on the associated job
  def self.authorized?(object, context)
    super &&
      (context[:api_user] == object.user ||
       context[:api_company].id == object.company_id ||
       context[:api_company].id == object&.job&.rescue_company_id ||
       context[:api_company].id == object&.job&.fleet_company_id)
  end

  field :company, Types::CompanyType, null: true
  field :container, Types::DocumentContainerEnumType, null: true
  field :invoice_attachment, Boolean, null: false
  field :job, Types::JobType, null: true
  field :location, Types::LocationType, description: 'Document Location', null: true
  field :original_filename, String, null: true
  field :type, Types::DocumentTypeEnumType, null: false
  field :url, String, null: true
  field :user, Types::UserType, null: true

end
