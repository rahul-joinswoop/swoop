# frozen_string_literal: true

# LOL i am so much sorrier for this name
class Types::LocationTypeNameType < Types::BaseScalar

  description 'Name of the LocationType'
  def self.coerce_input(value, context)
    Resolvers::LocationTypes.new(object: context[:api_company], context: context).resolve({}).find_by!(name: value)
  rescue StandardError => e
    # if we get any other error here we should log it to rollbar, it means
    # something else went wrong. we can still return the generic validation
    # messaage to our client
    Rollbar.error(e) unless e.is_a?(ActiveRecord::RecordNotFound)

    raise GraphQL::CoercionError, "#{value.inspect} is not a valid LocationType"
  end

  def self.coerce_result(value, _ctx)
    value&.name&.to_s
  end

end
