# frozen_string_literal: true

class Types::QuestionAnswerInputType < Types::BaseInputObject

  argument :question, String, required: true
  argument :answer, String, required: true
  argument :extra, String, required: false

end
