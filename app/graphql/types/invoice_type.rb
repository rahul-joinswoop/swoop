# frozen_string_literal: true

class Types::InvoiceType < Types::BaseObject

  include Utils::Currency

  description "An Invoice of a Job."

  #
  # We explicitly use a String value instead of global_id_field, to be able to support sample invoices.
  #
  # A sample invoice does not have id, therefore we need to build a fake one when rendering it back to
  # clients, otherwise it explodes
  field :id, String, null: false
  def id
    if object.id
      object.to_ssid
    else
      SecureRandom.hex
    end
  end

  # We use the 'available' prefix to emphasize this is about the addons
  # that are still available to be added to this Invoice, and not the ones
  # that might have been added to it already.
  field :available_additional_line_items,
        Types::InvoiceLineItemType.connection_type,
        description: 'Additional LineItems that are available to be added to this Invoice.',
        null: false,
        required_roles: :rescue

  field :balance, String, description: 'Balance of the Invoice. Basically: amount minus payments.', null: true
  def balance
    currency_value_or_zero(object.balance)
  end

  field :class_type, Types::VehicleClassTypeNameType, description: 'The ClassType where this Invoice items were based on.', null: true
  def class_type
    object.job.invoice_vehicle_category
  end

  field :class_types,
        Types::VehicleClassTypeType.connection_type,
        description: 'Class Types available for this Invoice.',
        null: true,
        required_roles: :rescue

  field :created_at, Types::DateTimeType, null: true

  field :currency,
        Types::CurrencyEnumType,
        description: "Currency used for this Invoice",
        null: false

  field :previous_rate_type,
        Types::RateTypeType,
        description: 'Data input by server during edit invoice flow',
        null: true

  def previous_rate_type
    if object.edit_invoice_metadata.is_a? Hash
      object.edit_invoice_metadata.dig(:previous_rate_type)
    end
  end

  field :previous_class_type,
        Types::VehicleClassTypeNameType,
        description: 'Data input by server during edit invoice flow',
        null: true

  def previous_class_type
    if object.edit_invoice_metadata.is_a? Hash
      object.edit_invoice_metadata.dig(:previous_vehicle_category)
    end
  end

  field :job, Types::JobType, description: 'The Job of the Invoice', null: false

  field :line_items, Types::InvoiceLineItemType.connection_type, description: 'Line Items of the Invoice', null: true
  def line_items
    # if the invoice is persisted in the db, we don't want its deleted line_items
    if object.persisted?
      object.line_items.not_deleted
    else
      # but we need deleted items during build sample flow, so we let them pass
      # in case the invoice is not persisted yet (the build sample flow builds
      # the invoice in memory, therefore it's not persisted yet)
      #
      # We need to convert it to array otherwise SampleInvoice mutation does not work
      # (I think because sample_invoice is not persisted in the db?)
      object.line_items.to_a
    end
  end

  field :notes, String, description: 'Notes for the Invoice.', null: true
  def notes
    object.job.driver_notes
  end

  field :paid, Boolean, description: 'Flags an Invoice as Paid.', null: true
  def paid
    object.paid.to_bool
  end

  field :payments,
        Types::InvoicePaymentOrCreditType.connection_type,
        description: 'Payments for this Invoice.',
        null: true
  def payments
    # needed to convert it to array otherwise SampleInvoice mutation does not work
    # (I think because sample_invoice is not persisted in the db?)
    object.payments_or_credits.to_a
  end

  field :rate_type,
        Types::RateTypeType,
        method: :friendly_rate_type,
        description: 'The RateType where this Invoice items were based on.',
        null: true

  field :rate_types,
        Types::RateTypeType.connection_type,
        description: 'Rate Types available for this Invoice.',
        null: true,
        required_roles: :rescue

  # TODO recipient attr on account AR model should have its own type so to enable us to apply OO correctly.
  #   Currently it can be an Account or an User, completely different models with different structure.
  #   Here we clearly separate both into user_recipient and account_recipient so queries run without problem.
  #
  #   ticket https://swoopme.atlassian.net/browse/ENG-5031
  #
  # re: TODO - afaict we can just use a union instead
  field :recipient, Types::InvoiceRecipientUnionType, null: true

  field :rejections,
        Types::InvoiceRejectionType.connection_type,
        method: :invoice_rejections,
        description: 'Rejection explanations about why this Invoice was rejected.',
        null: true,
        connection: true

  field :sender, Types::CompanyType, description: 'The Company that emitted this Invoice', null: false

  field :sent_at, Types::DateTimeType, null: true

  field :status, String, method: :state, description: 'Status of the Invoice.', null: true

  # I think only rescue can see it?
  field :subtotal,
        String,
        description: "Subtotal is the sum of all Invoice lineItems, but without taking 'Tax' and 'Percent' items into the account",
        null: false,
        required_roles: :rescue
  def subtotal
    currency_value_or_zero(object.subtotal_for_ui)
  end

  field :total_amount,
        String,
        description: 'Total amount of the Invoice, excluding taxes.',
        null: true

  def total_amount
    currency_value_or_zero(object.total_amount)
  end

end
