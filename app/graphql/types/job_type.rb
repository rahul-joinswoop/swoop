# frozen_string_literal: true

class Types::JobType < Types::BaseObject

  implements GraphQL::Relay::Node.interface

  global_id_field :id

  def self.authorized?(object, context)
    # see also Resolvers::Jobs
    #
    # just an initial stab at how we can do authorization.
    # TODO - we could do something like this but RescueDriver isn't actually a
    # thing. i think we need to check for who our api_user is and based on that
    # we can filter on Job#user / Job#driver.user / Job#rescue_driver. so what
    # are those all exactly?
    # if ctx[:api_user].is_a? RescueDriver
    #   return jobs.where(rescue_driver: ctx[:api_user])
    # end
    # TODO - any other company types here? what about when an end user is
    # accessing a job via the show_eta / get_location endpoints?
    #
    # NOTE: this runs every time we show a JobType object so it needs to be fast -
    # hence the comparisons with fleet_company_id / context[:api_company].id
    # instead of fleet_company / context[:api_company]

    # VERY BIG TODO TODO TODO
    # can we just use cancancan here instead?
    # at the bare minimum can we break this logic out somewhere else, make it
    # testable, etc?
    super &&
    case context[:api_company]
    when FleetCompany
      if context.is_super_client_managed? && object.is_a?(::FleetManagedJob)
        true
      else
        object.fleet_company_id == context[:api_company].id
      end
    when RescueCompany
      # we allow anyone in a partner(rescue) company to view a single job. we
      # still keep our scoping for jobs_resolver so that the correct jobs show
      # up in a list but this way we can get subscription notices when a job is
      # reassigned / goes out of scope
      object.rescue_company_id == context[:api_company].id ||
      # ****** This logic must be kept in sync with the Jobs resolver ********
      #
      # if we bidded on a job and it's live _or_ it ended less than 15 minutes
      # ago and we didn't get it then make it viewable. the first check allows
      # us to see jobs which we are bidding on, and the second allows us to
      # get a subscription notification for a job which we lost bidding of.
      # (we don't need to handle the case where we won here - that's done above
      # because job.rescue_company is set to us)
      Jobs::RecentlyBid.call(context, Job.where(id: object.id)).present?
    when SuperCompany
      case context[:api_company].id
      when ::Company.swoop_id
        if context.has_role?(:root)
          true
        else
          # swoop dispatchers can see all FleetManagedJobs
          object.type == 'FleetManagedJob'
        end
      else
        # shouldn't ever get here
        false
      end
    when NilClass
      # if there's no company then verify that our api_user is the customer of our job
      object.customer == context[:api_user]
    else
      false
    end
  end

  field :account,
        Types::AccountType,
        null: true,
        resolver: Resolvers::Account

  # TODO - should client users have access to this?
  field :auction,
        Types::AuctionType,
        null: true,
        required_roles: :rescue

  field :caller,
        Types::JobCallerType,
        null: true,
        required_roles: :rescue
  def caller
    if (context.is_partner? && object.auto_assigning?) || object.issc? || object.rsc?
      nil
    else
      object
    end
  end

  field :can_manage_photos,
        Boolean,
        method: :can_partner_manage_photos?,
        null: false,
        description: "Inform if users can manage photos for this Job",
        required_roles: :rescue

  field :client,
        Types::JobClientType,
        null: true,
        description: "Job client information",
        required_roles: :any
  def client
    object
  end

  field :coverage,
        Types::CoverageType,
        null: true,
        description: "Job Coverage information",
        method: :pcc_coverage,
        preload: :pcc_coverage,
        required_roles: :fleet

  field :created_at,
        Types::DateTimeType,
        null: true

  field :customer,
        Types::JobCustomerType,
        null: true,
        description: "Job customer information"
  def customer
    return nil if context.is_partner? && object.auto_assigning?

    c = object.customer
    if context.is_partner? && object.is_a?(::FleetManagedJob)
      c.email = nil
    else
      c.email = object.email if c.email.blank?
    end
    c
  end

  field :documents,
        Types::DocumentType.connection_type,
        resolver: Resolvers::Documents,
        null: true,
        description: "Documents from the Job" do
    argument :type, Types::DocumentTypeEnumType, required: false
    argument :container, Types::DocumentContainerEnumType, required: false
  end

  field :editable,
        Boolean,
        null: false,
        description: "Is this Job editable by the current Viewer?",
        required_roles: :rescue

  # LOLWUT i can't put this in the resolver: field above because graphql
  # explodes?
  def editable
    Resolvers::Editable.new(object: object, context: context).resolve
  end

  field :expires_at,
        Types::DateTimeType,
        null: true,
        description: "Time at which the Job expires"
  def expires_at
    object&.auction&.expires_at
  end

  field :eta,
        Types::JobEtaType,
        null: true,
        required_roles: :any
  def eta
    object
  end

  field :fieldLabels,
        Types::JsonType,
        null: true,
        resolver: Resolvers::FieldLabels

  field :fleet_site,
        Types::SiteType,
        deprecation_reason: "Please use client.site",
        null: true

  field :history,
        Types::HistoryType.connection_type,
        null: true,
        connection: true,
        preload: [:invoice, :audit_sms, :time_of_arrivals_non_live, :audit_job_statuses_order_created]

  def history
    object.job_history_hash(context[:api_company]).values
  end

  # This should be the invoices field for all companies, not partners only.
  #
  # And this is also not a good name, it should be invoices. But since invoices has already been queried
  # by FleetManaged (not sure why?) and that one has a different format, we needed to add this with a new name: job_invoices. Ugh.
  field :job_invoices,
        Types::InvoiceType.connection_type,
        null: true,
        resolver: Resolvers::Invoices

  # Here only for compatibility reasons: some FleetManaged companies query for this one.
  # Not sure though because they shouldn't see any invoices.
  # I ssume that they're just querying everything for no reason.
  #
  # @see https://swoopme.atlassian.net/browse/ENG-9406
  #
  # We only show it for fleets, otherwise we'd have id issues in relay/mobile v3.
  # Hopefully it will be removed soon in favour of job_invoices.
  field :invoices, Types::JobInvoiceType,
        null: true,
        deprecation_reason: 'Please use Job.jobInvoices field instead',
        required_roles: :fleet
  def invoices(**args)
    object
  end

  field :location,
        Types::JobLocationType,
        null: true,
        preload: [:drop_location, :service_location],
        required_roles: :any
  def location
    object
  end

  # many of these types use the main job object to build themselves and therefore
  # set property: :itself - not sure if there's a cleaner way to do this? could
  # add a resolve block but that's way more typing...
  field :notes,
        Types::NotesType,
        null: true
  def notes
    object
  end

  field :partner,
        Types::JobPartnerType,
        null: true,
        required_roles: :any
  def partner
    object
  end

  field :payment_type,
        Types::PaymentType,
        method: :customer_type,
        null: true,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::CUSTOMER_TYPE)
        },
        description: "Type of payment for the job"

  field :po_number,
        String,
        null: true,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::JOB_PO_NUMBER)
        }

  field :priorityResponse,
        Boolean,
        null: true

  field :queueAt,
        Types::DateTimeType,
        null: false,
        method: :adjusted_created_at do
    description <<~DESCRIPTION
      The time at which the Job should be processed in a work queue, usually either
      the scheduledAt or createdAt Timestamp. This is the default sort key jobs are
      returned in.
    DESCRIPTION
  end

  field :refNumber,
        String,
        null: true,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::JOB_REFERENCE_NUMBER)
        },
        description: "Reference number from an external system that can be tied to the job"

  field :rescue_vehicle,
        Types::PartnerVehicleType,
        null: true,
        preload: { rescue_vehicle: [:driver] },
        deprecation_reason: "Please use partner.vehicle instead",
        resolver: Resolvers::JobPartnerVehicle

  field :service,
        Types::JobServiceType,
        null: true
  def service
    object
  end

  # TODO which site is this?
  field :site,
        Types::SiteType,
        null: true

  field :status_updated_at,
        Types::DateTimeType,
        null: true,
        method: :last_status_changed_at

  field :status,
        Types::JobStatusEnumType,
        null: true,
        required_roles: :any

  def status
    object.virtual_status context[:api_company]
  end

  field :status_transitions_available,
        Types::JobStatusEnumType.connection_type,
        null: true,
        description: <<~DESCRIPTION
          A list of potentially valid Job Statuses the current Viewer can transision this Job to. Note that none of these Job Status transitions are guaranteed
          to work due to deeper validations and logic which is applied at runtime.
        DESCRIPTION

  def status_transitions_available
    object.legal_state_changes context[:viewer]
  end

  # TODO we should add the reason into the history field, but it currently has a bug,
  # follow up ticket: https://swoopme.atlassian.net/browse/ENG-8836
  field :status_reasons,
        Types::JobStatusReasonType.connection_type,
        null: false,
        description: 'Status Reason chosen for a given status change.'

  field :status_reasons_available,
        Types::JobStatusReasonType.connection_type,
        null: false,
        resolver: Resolvers::JobStatusReasonTypes,
        required_roles: :rescue,
        description: 'List of Job Status reasons to be used when a Status change requires one for this Job.' do
    argument :status, Types::JobStatusEnumType, required: true, description: 'Filter by JobStatus.'
  end

  field :stored_vehicle,
        Types::JobStoredVehicleType,
        null: true,
        method: :storage

  # Note this may diverge from the partner job id
  field :swcid,
        Integer,
        null: true,
        method: :id,
        description: "Swoop Client ID (visible in the UI)"

  field :texts,
        Types::JobTextsType,
        null: true
  def texts
    object
  end

  # TODO - these need to be graphql types (like company.graphql_type)
  field :type,
        Types::JobTypeEnumType,
        null: false,
        required_roles: :any

  field :vehicle,
        Types::StrandedVehicleType,
        null: true
  def vehicle
    object.stranded_vehicle
  end

end
