# frozen_string_literal: true

class Types::QuestionResultType < Types::BaseObject

  field :question,
        String,
        description: 'The question text presented for the Job',
        null: true

  def question
    object&.question&.question
  end

  field :answer,
        String,
        description: 'The answer text provided for the Job',
        null: true

  def answer
    object&.answer&.answer
  end

  field :extra,
        String,
        method: :answer_info,
        description: 'The extra answer text provided for the Job',
        null: true

end
