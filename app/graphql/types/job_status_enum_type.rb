# frozen_string_literal: true

class Types::JobStatusEnumType < Types::BaseEnum

  graphql_name 'JobStatus'
  description 'A JobStatus. A Job flow will push the Job through different statuses.'

  ::Job.statuses.each do |status, friendly_name|
    # LOLWUT?
    # in the db, our statuses are all "Foo Bar Baz"
    # in rails, our status become "foobarbaz"
    # we can't use spaces because graphql enums don't allow it, and we can't use
    # lowercase-squash-altogether because i think it's ugly.
    # so...
    # we drop the spaces and camelize (pascal case?), ie "FooBarBaz"
    value(
      friendly_name.gsub(/\s+/, '').camelize, # FooBarBaz
      friendly_name, # "Foo Bar Baz"
      value: status # foobarbaz
    )
  end

  # Return the original JobStatus#name given the enum_value
  #
  # 'auto_assigning' => 'Auto Assigning'
  # 'etarejected' => 'ETA Rejected'
  #
  # ...and so on
  #
  def self.friendly_name(enum_value)
    values.values.find { |status| status.value == enum_value }.description
  end

end
