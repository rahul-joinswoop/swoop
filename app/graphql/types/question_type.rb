# frozen_string_literal: true

class Types::QuestionType < Types::BaseObject

  description "Questions are pre-determined by companies and can be used on a Job. It will contain an answer that can potentially help next people who interacts with the Job (dispatchers / drivers / etc) on how to proceed with Job."

  global_id_field :id

  # RG this field is not unique on BD
  field :name, String, description: 'Works as a row key for the question', null: true
  field :question, String, description: 'The question text to be presented for the Job', null: true
  field :order_num, Integer, description: 'Specifies the order in which the question will be displayed', null: true
  field :answers, Types::AnswerType.connection_type, null: true, connection: true

end
