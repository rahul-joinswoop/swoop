# frozen_string_literal: true

class Types::VehicleLocationType < Types::BaseObject

  description "A Vehicle's Location"

  field :updatedAt,
        Types::DateTimeType,
        method: :location_updated_at,
        null: true,
        required_roles: :any,
        description: "Last time the Vehicle's location was updated."

  field :lat, Float, null: true, required_roles: :any
  field :lng, Float, null: true, required_roles: :any

end
