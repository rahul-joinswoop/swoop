# frozen_string_literal: true

class Types::VehicleModelType < Types::BaseObject

  description "The Vehicle Model to be used on Jobs."

  global_id_field :id

  field :name,
        String,
        description: "Name of the VehicleModel",
        null: true

  field :edmunds_id,
        Integer,
        description: "Edmunds ID.",
        null: true

  field :heavy_duty,
        Boolean,
        description: "Heavy duty?",
        null: true

  field :years,
        Types::VehicleYearType.connection_type,
        description: "Years",
        null: true,
        resolver: Resolvers::VehicleYears,
        connection: true do
    argument :query, String, "Search String", required: false
  end

  # TODO - do we need this now? seems like it's only heavy equipment
  field :vehicle_type,
        Types::VehicleTypeType,
        description: "Vehicle Type",
        null: true

end
