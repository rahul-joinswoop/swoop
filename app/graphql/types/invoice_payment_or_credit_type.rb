# frozen_string_literal: true

class Types::InvoicePaymentOrCreditType < Types::BaseObject

  include Utils::Currency

  description 'A Payment or a Credit on an Invoice.'

  global_id_field :id

  field :amount, String, description: 'Amount of the Payment or Credit.', null: true
  def amount
    currency_value_or_zero(object.total_amount)
  end

  field :currency,
        Types::CurrencyEnumType,
        description: "Currency used for this Payment or Credit",
        null: false

  field :deleted_at, Types::DateTimeType, description: 'Date and time when this Payment or Credit was deleted.', null: true
  field :mark_paid_at, Types::DateTimeType, description: 'Date of the Payment.', null: true

  field :memo, String, description: 'Memo field for this Payment, can be used to add details to it.', null: true
  def memo
    object.description
  end

  field :payment_method, String, description: 'The PaymentMethod used for this Payment.', null: true
  field :type, String, description: 'Specify if this is a Payment or a Credit.', null: true

end
