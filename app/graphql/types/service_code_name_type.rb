# frozen_string_literal: true

# warning: Services and Addons are represented on Rails by ServiceCode AR model.
# This type should never be used for Addons, and Types::AddonType
# should never be used for Services. Otherwise we'll have issues with IDs in Relay.
class Types::ServiceCodeNameType < Types::BaseScalar

  description 'Name of the Service.'
  def self.coerce_input(value, context)
    Resolvers::Services.new(object: context[:api_company], context: context).resolve.find_by!(name: value)
  rescue StandardError => e
    # if we get any other error here we should log it to rollbar, it means
    # something else went wrong. we can still return the generic validation
    # messaage to our client
    Rollbar.error(e) unless e.is_a?(ActiveRecord::RecordNotFound)

    raise GraphQL::CoercionError, "#{value.inspect} is not a valid Service"
  end

  def self.coerce_result(value, _ctx)
    value&.name&.to_s
  end

end
