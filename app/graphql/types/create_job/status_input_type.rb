# frozen_string_literal: true

class Types::CreateJob::StatusInputType < Types::BaseEnum

  graphql_name "CreateJobStatusInput"
  description "Override the initial JobStatus"
  value "Draft", "Draft", value: "draft"
  # TODO - we can add this too if/when we want
  # value("PreDraft", "Pre Draft", "predraft")

end
