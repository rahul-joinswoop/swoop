# frozen_string_literal: true

class Types::CreateJob::JobInputType < Types::JobInputType

  graphql_name "CreateJobJobInput"

  argument :location, Types::CreateJob::JobLocationInputType, required: true
  argument :service, Types::CreateJob::ServiceInputType, required: true
  argument :status, Types::CreateJob::StatusInputType, required: false, description: "Deprecated, please use the createDraftJob mutation instead"

end
