# frozen_string_literal: true

class Types::CreateJob::ServiceInputType < Types::ServiceInputType

  graphql_name "CreateJobServiceInput"
  argument :name,
           Types::ServiceCodeNameType,
           description: "See below section for more information on Services.",
           required: true

  argument :symptom,
           Types::SymptomNameType,
           description: "See below section for more information on Symptoms.",
           required: false,
           required_features: Feature::SHOW_SYMPTOMS

  # TODO why do we duplicate it from parent, as both look like to be the same?
  argument :answers,
           [Types::QuestionAnswerInputType],
           description: "See below section for more information on Questions & Answers.",
           required: false,
           required_features: Feature::QUESTIONS,
           required_roles: :fleet

end
