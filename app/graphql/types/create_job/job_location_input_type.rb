# frozen_string_literal: true

class Types::CreateJob::JobLocationInputType < Types::Job::LocationInputType

  graphql_name "CreateJobJobLocationInput"
  # TODO - should we change this to pickupLocation instead?
  argument :service_location, Types::LocationInputType, required: true

end
