# frozen_string_literal: true

# Type used for coercing a payment type name into an object and back.
class Types::PaymentTypeNameType < Types::BaseScalar

  description 'Name of the payment type'

  class << self

    def coerce_input(value, context)
      payment_type = Resolvers::PaymentTypes.new(object: context[:api_company], context: context).resolve.find_by(name: value)
      unless payment_type
        raise GraphQL::CoercionError, "#{value.inspect} is not a valid PaymentType"
      end
      payment_type
    end

    def coerce_result(value, _ctx)
      value&.name&.to_s
    end

  end

end
