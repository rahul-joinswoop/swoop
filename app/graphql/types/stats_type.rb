# frozen_string_literal: true

class Types::StatsType < Types::BaseObject

  field :requests,
        Types::RequestStatsType,
        null: false,
        description: "Stats related to API requests"

  def requests
    object
  end

  field :deprecated_fields,
        Types::DeprecatedFieldsStatsType,
        null: false,
        description: "Stats related to deprecated fields"

  def deprecated_fields
    object
  end

end
