# frozen_string_literal: true

class Types::FeatureType < Types::BaseObject

  description 'Features will allow a Company to have a customized and specific way of using Swoop.' \
    'It can be used to customize what Job fields will be available for a determined Company. ' \
    'Features can only be enabled / disabled by Swoop admins.'

  global_id_field :id
  implements GraphQL::Relay::Node.interface

  field :name, String, description: 'Name of the Feature.', null: false
  field :description, String, description: 'Describes the Feature.', null: false

end
