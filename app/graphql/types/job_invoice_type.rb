# frozen_string_literal: true

# Here for compatibility reasons only, and should not be used.
# It will hopefully be removed soon.
# @see https://swoopme.atlassian.net/browse/ENG-9406
class Types::JobInvoiceType < Types::BaseObject

  field :customer, Types::CustomerInvoiceType, null: true, resolver: Resolvers::EndUserInvoices

end
