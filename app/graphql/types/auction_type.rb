# frozen_string_literal: true

class Types::AuctionType < Types::BaseObject

  global_id_field :id
  # TODO - how do we limit visibility into auctions? are bids actually "invitations to bid"?
  # if so then we can limit based on that i suppose?
  field :expires_at, Types::DateTimeType, null: true
  field :status, Types::AuctionStatusEnumType, null: true
  field :bids, Types::BidType.connection_type, null: true, connection: true
  def bids(**args)
    object.bids.where(company: context[:api_company])
  end

end
