# frozen_string_literal: true

class Types::TireModelType < Types::BaseObject

  description 'TireModel to be used by specific Companies on a Job.'

  global_id_field :id
  implements Interfaces::TimestampsInterface

  field :car, String, description: 'Car model that uses this TireModel', null: true
  field :size, String, description: 'Tire Model type.', null: true
  field :position, String, description: 'Position of Tire Model on the car.', null: true
  field :extras, String, description: 'Extra info about this TireModel', null: true

end
