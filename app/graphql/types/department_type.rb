# frozen_string_literal: true

class Types::DepartmentType < Types::BaseObject

  description 'A Department is a field available for Job creation when Departments feature is enabled for the Company.' \
    'It can be used to hold a department/section of the Company where this Job will be handled.'

  global_id_field :id
  implements Interfaces::TimestampsInterface

  field :name, String, description: 'Name of the Department.', null: true
  field :code, String, description: 'TBD', null: true
  field :company, Types::CompanyType, description: 'Company who owns this Department.', null: true, resolver: Resolvers::Company
  field :sort_order, Integer, description: 'Can be used to specify in which order this Department will be shown in a list of Departments.', null: true

end
