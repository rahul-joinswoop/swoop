# frozen_string_literal: true

class Types::InvoiceTypeEnumType < Types::BaseEnum

  graphql_name 'InvoiceTypeType'
  description 'Invoice Type'

  value 'Client'
  value 'EndUser'

end
