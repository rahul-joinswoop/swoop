# frozen_string_literal: true

# i am so sorry for this name
class Types::VehicleTypeType < Types::BaseObject

  description 'A vehicle type'

  global_id_field :id

  field :name, String, description: 'Vehicle type name', null: true

end
