# frozen_string_literal: true

class Types::Subscription::JobStatusGroupEnumType < Types::BaseEnum

  graphql_name 'SubscriptionJobStatusGroup'
  description <<~DESCRIPTION
    Each value represents a group of `JobStatuses` to query on.

    **Active**: #{::Job.active_states.map { |j| "`#{j.gsub(/\s+/, '')}`" }.join(", ")}

    **Done**: #{::Job.done_states.map { |j| "`#{j.gsub(/\s+/, '')}`" }.join(", ")}

    **Map**: #{::Job.map_states.map { |j| "`#{j.gsub(/\s+/, '')}`" }.join(", ")}
  DESCRIPTION

  Job::JOB_STATUS_GROUPS.reject { |k, v| k == :all_states }.each do |k, v|
    value v[:name], v[:description], value: k
  end

end
