# frozen_string_literal: true

# warning: Addons and Services are represented on Rails by ServiceCode AR model.
# This type should never be used for Services, and Types::ServiceCodeNameType
# should never be used for Addons. Otherwise we'll have issues with IDs in Relay.
class Types::AddonType < Types::BaseObject

  description 'Addons to be used for the Invoice of a Job.'

  field :name, String, null: false

  # we should probably have an enum type here to be used as inputs of queries and mutations;
  # but addons are dinamically added/removed in the db, and the Enum type needs to be
  # static so it won't work; we may want to revisit it in the future.

end
