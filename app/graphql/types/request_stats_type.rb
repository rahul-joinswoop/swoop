# frozen_string_literal: true

class Types::RequestStatsType < Types::BaseObject

  field :daily,
        Types::RequestDailyStatsType.connection_type,
        null: false,
        method: :graphql_requests,
        max_page_size: 30,
        description: "API usage from the last 30 days bucketed by date"

end
