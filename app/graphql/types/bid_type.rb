# frozen_string_literal: true

class Types::BidType < Types::BaseObject

  global_id_field :id
  # companies can only view their own bids
  def self.authorized?(object, context)
    super && object.company_id == context[:api_company].id
  end
  field :eta, Types::DateTimeType, method: :eta_dttm, null: true
  field :status, Types::BidStatusEnumType, null: true
  field :created_at, Types::DateTimeType, null: true
  field :updated_at, Types::DateTimeType, null: true

end
