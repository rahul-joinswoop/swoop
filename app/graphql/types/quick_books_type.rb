# frozen_string_literal: true

class Types::QuickBooksType < Types::BaseObject

  field :imported_at, Types::DateTimeType, method: :qb_imported_at, description: 'Last time when QuickBooks was imported for this Invoice', null: true
  field :pending_import, Boolean, method: :qb_pending_import, description: 'Tells if QuickBooks has pending import. Will likely be used by Swoop flow.', null: true

end
