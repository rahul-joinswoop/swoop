# frozen_string_literal: true

class Types::JobTextsType < Types::BaseObject

  field :send_location, Boolean, method: :get_location_sms?, null: true
  field :send_eta_updates, Boolean, method: :text_eta_updates?, null: true
  field :send_driver_tracking, Boolean, method: :partner_sms_track_driver?, null: true
  field :send_review, Boolean, method: :send_review?, null: true
  field :send_texts_to_pickup, Boolean, method: :send_sms_to_pickup?, null: true

end
