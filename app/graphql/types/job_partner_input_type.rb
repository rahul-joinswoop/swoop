# frozen_string_literal: true

class Types::JobPartnerInputType < Types::BaseInputObject

  class JobPartnerDepartmentInputType < Types::BaseInputObject

    argument :name, String, 'Department name', required: false

  end

  class JobPartnerDriverInputType < Types::BaseInputObject

    argument :name, String, 'Driver name', required: false

  end

  class JobPartnerVehicleInputType < Types::BaseInputObject

    argument :name, String, 'Vehicle name', required: false

  end

  class JobPartnerTrailerInputType < Types::BaseInputObject

    argument :name, String, 'Trailer name', required: false

  end

  argument :driver, JobPartnerDriverInputType, required: false, required_roles: Utils::Auth::RESCUE_ADMIN_OR_DISPATCHER_ONLY
  argument :department, JobPartnerDepartmentInputType, required: false
  argument :trailer, JobPartnerTrailerInputType, required: false
  argument :vehicle, JobPartnerVehicleInputType, required: false

end
