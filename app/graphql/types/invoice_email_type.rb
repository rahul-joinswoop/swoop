# frozen_string_literal: true

class Types::InvoiceEmailType < Types::BaseObject

  field :to, Types::EmailType, method: :email_to, description: 'Email address to where this invoice will be sent to.', null: true
  field :cc, Types::EmailType, method: :email_cc, description: 'Email address to where this invoice will be sent to as a cc.', null: true
  field :bcc, Types::EmailType, method: :email_bcc, description: 'Email address to where this invoice will be sent to as a bcc.', null: true

end
