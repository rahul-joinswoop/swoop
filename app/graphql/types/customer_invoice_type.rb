# frozen_string_literal: true

# Here for compatibility reasons only, and should not be used.
# It will hopefully be removed soon.
# @see https://swoopme.atlassian.net/browse/ENG-9406
class Types::CustomerInvoiceType < Types::BaseObject

  description "An Invoice generated for an end customer."
  global_id_field :id

  field :total_amount, String,
        description: 'Total amount of the Invoice, excluding taxes.', null: true

  def total_amount
    "%.2f" % object.total_amount.truncate(2)
  end

end
