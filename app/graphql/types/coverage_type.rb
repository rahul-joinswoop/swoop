# frozen_string_literal: true

class Types::CoverageType < Types::BaseObject

  implements GraphQL::Relay::Node.interface
  global_id_field :id

  def self.authorized?(object, context)
    super &&
    case context[:api_company]
    when FleetCompany
      job = object.jobs.last
      if job.blank?
        false
      elsif context.is_super_client_managed?
        job.is_a?(::FleetManagedJob)
      else
        job.fleet_company_id == context[:api_company].id
      end
    else
      false
    end
  end

  field :issues,
        Types::CoverageIssueType.connection_type,
        description: "Coverage issues",
        null: true,
        connection: true,
        required_roles: :super_fleet_managed

  def issues
    object&.data&.first&.dig("coverage_issues")
  end

  field :policy,
        Types::PolicyType,
        null: true,
        description: "Policy associated with this Coverage",
        method: :pcc_policy,
        preload: :pcc_policy,
        required_roles: :fleet

  field :program,
        Types::CoverageProgramType,
        null: true,
        description: "Program associated with this Coverage",
        required_roles: :fleet

  def program
    object
  end

  field :result,
        String,
        null: true,
        description: "Result of Coverage lookup",
        required_roles: :fleet

  def result
    object&.data&.first&.dig("result")
  end

end
