# frozen_string_literal: true

class Types::VehicleStyleType < Types::BaseObject

  global_id_field :id

  field :name, String, description: "Name of the VehicleStyle", null: true

end
