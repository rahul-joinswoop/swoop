# frozen_string_literal: true

class Types::LocationType < Types::BaseObject

  description 'A location address. This type is used by many other types on Swoop.'

  global_id_field :id
  implements GraphQL::Relay::Node.interface

  field :address,
        String,
        description: "Full address that the customer inputs",
        null: true

  field :city,
        String,
        description: "City portion of the full address",
        null: true

  field :country,
        String,
        description: "Country portion of the full address",
        null: true

  field :exact,
        Boolean,
        description: "Location is exact and has lat/lng to high accuracy",
        null: true

  field :google_place_id,
        String,
        method: :place_id,
        description: "ID returned from Google Places API. See below for more information on Locations.",
        null: true

  field :lat,
        Float,
        description: "Latitude returned from Google Places API.",
        null: true,
        required_roles: :any

  field :lng,
        Float,
        description: "Longitude returned from Google Places API.",
        null: true,
        required_roles: :any

  field :location_type,
        Types::LocationTypeNameType,
        description: "See below section for more information on Location Types.",
        null: true

  field :postal_code,
        String,
        description: "Postal code portion of the full address",
        null: true,
        method: :zip

  field :state,
        String,
        description: "State portion of the full address",
        null: true

  field :street,
        String,
        description: "Street address portion of the full address",
        null: true

end
