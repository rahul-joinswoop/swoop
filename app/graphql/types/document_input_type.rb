# frozen_string_literal: true

class Types::DocumentInputType < Types::BaseInputObject

  class DocumentJobInputType < Types::BaseInputObject

    argument :id, ID, required: true

  end

  argument :container, Types::DocumentContainerEnumType, required: false
  argument :created_at, Types::DateTimeType, required: false
  argument :document, Types::JsonType, required: true
  argument :invoice_attachment, Boolean, default_value: false, required: false
  argument :job, DocumentJobInputType, required: true
  argument :location, Types::LocationInputType, required: false
  argument :type, Types::DocumentTypeEnumType, required: true

end
