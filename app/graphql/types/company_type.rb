# frozen_string_literal: true

class Types::CompanyType < Types::BaseObject

  description <<~DESCRIPTION
    Companies are the legal entities who use Swoop. They can be of different `Types`:

    **ClientCompany:** Companies that have a set of vehicles that may need help.
    It can be an insurance company, for instance. Or any company that renders rescue service
    for broken cars and that relies on Partner Companies to actually execute the job.

    **In-House ClientCompany:** These companies manage their own Partner Companies networks
    (Partner Companies detailed below). We give them the tools to dispatch directly to towing companies
    and solve customer issues on their own. We can identify Managed companies when it has `in_house` attribute set to `true`.

    **Managed ClientCompany:** These companies use Swoop towing network, and rely on it to dispatch
    and see the Jobs through that they put into our system.
    We can identify Managed companies when it has `in_house` attribute set to `null` or `false`.
  DESCRIPTION
  global_id_field :id
  implements Interfaces::CompanyCommonInterface
  implements GraphQL::Relay::Node.interface

  # NOTE: almost everything in this type is publicly visible. everything inside of Interfaces::CompanyCommonInterface is _not_ publically visible
  # and filtered based on api_user / api_company.

  # TODO - i think maybe this should be protected?
  field :agero_vendor_id,
        Integer,
        description: "Vendor ID from Agero",
        null: true

  field :currency,
        Types::CurrencyEnumType,
        description: "Currency used by this Company",
        null: false

  field :distance_unit,
        Types::DistanceUnitEnumType,
        description: 'Distance units used by this Company',
        null: false

  field :language, String, description: 'Language selection for this company.', null: false

  field :dispatch_email,
        Types::EmailType,
        description: 'Dispatch Email to where dispatching related emails will be sent to.',
        null: true

  field :dropoff_waiver,
        String,
        description: 'Dropoff Waiver',
        null: true,
        resolver: Resolvers::MyCompanyOnly.build(:inherited_dropoff_waiver),
        required_roles: :rescue

  field :fax,
        String,
        description: 'Company fax.',
        null: true

  field :location,
        Types::LocationType,
        description: 'Company address.',
        null: true

  field :logo_url,
        String,
        null: true,
        required_roles: :any do
    description <<~DESCRIPTION
      Admins can customize the Company logo by uploading it in the Client Application. This holds the URL address for the uploaded logo.
    DESCRIPTION
  end

  field :name,
        String,
        description: 'The name of the Company.',
        null: true,
        required_roles: :any

  field :phone,
        Types::E164PhoneNumberType,
        description: 'Company phone.',
        null: true,
        required_roles: :any

  field :pickup_waiver,
        String,
        description: 'Pickup Waiver',
        null: true,
        resolver: Resolvers::MyCompanyOnly.build(:inherited_pickup_waiver),
        required_roles: :rescue

  field :primary_contact,
        Types::UserType,
        description: 'Primary Contact for Company',
        null: true

  # subscription status is only visible to one's own company
  field :subscription_status,
        String,
        null: true,
        resolver: Resolvers::SubscriptionStatus

  field :support_email,
        Types::EmailType,
        description: 'Support Email to where support related emails will be sent to.',
        null: true

  field :type,
        Types::CompanyTypeEnumType,
        null: false,
        method: :graphql_type do
    description <<~DESCRIPTION
      The type of the Company. Can be PartnerCompany, ClientCompany or SuperCompany (unique case, SuperCompany is Swoop).
    DESCRIPTION
  end

  field :global_stats,
        Types::StatsType,
        null: false,
        description: "Global stats",
        if_company: :is_swoop?

  def global_stats
    object
  end

  field :mobile_app,
        Types::ApplicationType,
        null: false,
        if_company: :is_swoop?,
        description: "Fake mobile oauth application"

  def mobile_app
    SuperCompany::IGNORE_ME_I_AM_A_FAKE_MOBILE_APP
  end

  field :subscriptions,
        Types::WebhookSubscriptionType.connection_type,
        null: true,
        resolver: Resolvers::MyCompanyOnly.build(:graphql_subscriptions)

  # removed for now per rob
  # connection :addons, Types::CompanyServiceType.connection_type do
  #   description 'Addons available for the Company, they are used on Job.'
  #     resolve Resolvers::Addons.new(object: object, context: context).resolve(args)
  #   end
  # end

  # field :accounting_email, Types::EmailType do
  #   description 'An accouting email address' # TODO RG should we remove from the model it as it's already contained on Account?
  # end
  # field :tax_id, types.String do
  #   description 'TBD'
  # end
  # field :channel_name, types.String do
  #   description 'TBD'
  # end
  # field :in_house, types.Boolean do
  #   description 'Specifices if this Company is an In-House or Managed variants, only applicable when the Company is a ClientCompany Type.'
  # end
  # field :parent_company, Types::CompanyType do
  #   description 'The parent company in case this is a sub-company.'
  #
  #   resolve -> (obj, args, ctx) { obj.parent_company }
  # end
  # field :live, types.Boolean do
  #   description 'Specifies if the Company is live in the system so it can be used to dispatch Jobs to. Only applicable to PartnerCompany Type.'
  # end
  # field :invoice_minimum_mins, types.Int do
  #   description 'TBD'
  # end
  # field :invoice_increment_mins, types.Int do
  #   description 'TBD'
  # end
  # field :invoice_roundup_mins, types.Int do
  #   description 'TBD'
  # end
  # field :invoice_mileage_rounding, types.String do
  #   description 'TBD'
  # end
  # field :invoice_mileage_minimum, Types::BigDecimalType do
  #   description 'TBD'
  # end
  # field :issc_status, types.String do
  #   description 'Describes the ISSC connection status. Only applicable for MotorClub companies'
  # end
  # field :referer, Types::RefererType do
  #   description "A PartnerCompany can be created by owners or people whos's interested on using Swoop. " +
  #     "This field will hold the referer info chosen by user who created the PartnerCompany during this flow."
  # end
  # field :dispatch_to_all_partners, types.Boolean do
  #   description "Applicable to ClientManaged companies and used internally by the system. " +
  #     "When set to `true`, will allow dispatching Jobs from this ClientManaged to all PartnerCompanies available."
  # end
  # field :issc_client_id, types.String do
  #   description "ISSC Client id, used for ISSC integration."
  # end
  # field :commission_percentage, types.Int do
  #   description "Applicable to PartnerCompany. Specifies the commission percentage for the drivers."
  #
  #   resolve -> (obj, args, ctx) do
  #     if obj.type == RescueCompany.name
  #       obj.commission.default_pct
  #     else
  #       nil
  #     end
  #   end
  # end
  #   description "When a Company is removed by a Swoop admin, it's flagged as deleted_at and never deleted from DB. " +
  #     "This field holds the deleted_at timestamp when it happens."
  # end
  # field :auction_max_bidders, types.Int do
  #   description 'Applicable to ClientCompany. Specifies the max quantity of PartnerCompanies to where a Job with Auction will be sent to.'
  #
  #   resolve -> (obj, args, ctx) do
  #     if obj.type == FleetCompany.name
  #       obj.auction_max_bidders
  #     else
  #       nil
  #     end
  #   end
  # end
  # field :review_email, Types::EmailType do
  #   description 'Review Email to where review related emails by clients will be sent to.'
  # end
  # field :review_email_recipient_key, types.String do
  #   description 'TBD'
  # end
  # field :disable_review_email, types.String do
  #   description 'Flag to enable/disable review emails feature.'
  #
  #   resolve -> (obj, args, ctx) { obj.disable_review_emails }
  # end

  # connection :payment_types, Types::PaymentType.connection_type, property: :customer_types do
  #   description 'Payment Types available for the Company, they are used on Job.'
  # end
  # connection :sites, Types::SiteType.connection_type, property: :live_sites do
  #   description 'Sites available for the Company, they are used on Job.'
  # end
  # connection :commission_exclusions_services, Types::ServiceType.connection_type do
  #   description 'Services array to be excluded from drivers commissions.'
  #   resolve -> (obj, args, ctx) do
  #     if obj.type == RescueCompany.name
  #       obj.commission_exclusions_services
  #     else
  #       nil
  #     end
  #   end
  # end

  # removed for now per rob
  # connection :commission_exclusions_addons, Types::ServiceType.connection_type do
  #   description 'Addons array to be excluded from drivers commissions.'
  #   resolve -> (obj, args, ctx) do
  #     if obj.type == RescueCompany.name
  #       obj.commission_exclusions_addons
  #     else
  #       nil
  #     end
  #   end
  # end

  # connection :fleet_in_house_clients, Types::CompanyType.connection_type do
  #   description 'Applicable to PartnerCompany. An array of all its Client In-House clients.'
  # end
  # connection :fleet_managed_clients, Types::CompanyType.connection_type do
  #   description 'Applicable to PartnerCompany. An array of all its Client Managed clients.'
  # end

end
