# frozen_string_literal: true

class Types::AreaInputType < Types::BaseInputObject

  graphql_name "AreaInputType"
  argument :from, Types::LatitudeLongitudeInputType, required: true
  argument :to, Types::LatitudeLongitudeInputType, required: true

end
