# frozen_string_literal: true

class Types::ServiceFilterEnumType < Types::BaseEnum

  graphql_name 'ServiceFilter'
  description 'Filters for Services'

  value('All', 'All services', value: :all)
  value('Create', 'Services valid for Job creation', value: :create)
  value('Storage', 'Services that allow for storage', value: :storage)

end
