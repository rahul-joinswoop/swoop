# frozen_string_literal: true

# i am so sorry for this name
class Types::LocationTypeType < Types::BaseObject

  description 'A location type'

  global_id_field :id

  field :name, Types::LocationTypeNameType, description: 'location type name', null: true
  def name
    object
  end

end
