# frozen_string_literal: true

# this is a shared base we use for CreateJobInput and UpdateJobInput
class Types::JobInputType < Types::BaseInputObject

  argument :client,
           Types::Job::ClientInputType,
           required: false,
           required_roles: :fleet,
           required_features: -> (features) {
                                (features & [Feature::DEPARTMENTS, Feature::CLIENT_SITES]).present?
                              }
  argument :account, Types::Job::AccountInputType, required: false, required_roles: :rescue
  argument :caller, Types::JobCallerInputType, required: false, required_roles: :rescue
  argument :customer, Types::JobCustomerInputType, required: false
  argument :eta, Types::Job::EtaInputType, required: false, required_roles: :rescue
  argument :location, Types::Job::LocationInputType, required: false
  argument :notes, Types::NotesInputType, required: false
  argument :paymentType, Types::PaymentTypeNameType, required: false, description: 'Deprecated: Please use paymentTypeName instead'
  argument :paymentTypeName, Types::PaymentTypeNameType, required: false, description: 'The name of the PaymentType used on the Job'
  # TODO - only fleet should be able to edit this
  argument :poNumber, String, required: false, required_features: Feature::JOB_PO_NUMBER
  argument :priorityResponse, Boolean, required: false, required_features: Feature::PRIORITY_RESPONSE
  argument :refNumber, String, required: false
  argument :service, Types::ServiceInputType, required: false do
    description "Service details"
  end
  argument :texts, Types::Job::TextsInputType, required: false
  argument :vehicle, Types::VehicleInputType, required: false

  argument :partner, Types::JobPartnerInputType, required: false, required_roles: :rescue

end
