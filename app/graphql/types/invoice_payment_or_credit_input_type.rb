# frozen_string_literal: true

class Types::InvoicePaymentOrCreditInputType < Types::BaseInputObject

  argument :id, ID, required: true

  argument :amount, String, description: 'Amount of the Payment or Credit.', required: true
  def amount
    object.total_amount
  end

  argument :deleted_at, Types::DateTimeType, description: 'Date and time when this Payment or Credit was deleted.', required: false
  argument :mark_paid_at, Types::DateTimeType, description: 'Date of the Payment.', required: false
  argument :memo, String, description: 'Memo field for this Payment, can be used to add details to it.', required: false
  def memo
    object.description
  end

  argument :payment_method, String, description: 'The PaymentMethod used for this Payment.', required: false
  argument :type, String, description: 'Specify if this is a Payment or a Credit.', required: true

end
