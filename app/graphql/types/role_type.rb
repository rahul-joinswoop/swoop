# frozen_string_literal: true

class Types::RoleType < Types::BaseObject

  description 'The user role.'

  global_id_field :id

  field :name, String, description: 'Name of the Role.', null: false
  field :description, String, description: 'Description of the Role', null: false
  def description
    Role::NAME_TO_DESCRIPTION_MAP[Role::STATUS_TO_NAME_MAP[object.name.to_sym]]
  end

end
