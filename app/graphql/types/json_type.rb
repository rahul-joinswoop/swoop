# frozen_string_literal: true

# from https://github.com/rmosolgo/graphql-ruby/pull/1398
class Types::JsonType < GraphQL::Schema::Scalar

  def self.coerce_input(value, _context)
    value
  end

  def self.coerce_result(value, _context)
    value
  end

end
