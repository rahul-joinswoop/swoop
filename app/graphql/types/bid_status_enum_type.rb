# frozen_string_literal: true

class Types::BidStatusEnumType < Types::BaseEnum

  graphql_name 'BidStatus'
  description 'Bid Status'

  Auction::Bid::NAME_TO_STATUS_MAP.each do |k, v|
    value(Auction::Bid::STATUS_TO_ENUM_NAME_MAP[v], Auction::Bid::NAME_TO_DESCRIPTION_MAP[k], value: v)
  end

end
