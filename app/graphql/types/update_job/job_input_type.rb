# frozen_string_literal: true

class Types::UpdateJob::JobInputType < Types::JobInputType

  graphql_name "UpdateJobJobInput"
  argument :id, ID, required: true

end
