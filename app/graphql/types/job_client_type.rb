# frozen_string_literal: true

class Types::JobClientType < Types::BaseObject

  # this is the client displayed in a job, it's a subset of company
  def self.authorized?(object, context)
    super &&
    case context[:api_company]
    when FleetCompany
      if context.is_super_client_managed? && object.is_a?(::FleetManagedJob)
        true
      else
        context[:api_company].id == object.fleet_company_id
      end
    when RescueCompany
      false
    when SuperCompany
      # TODO - is this correct? or should swoop dispatchers be limited somehow?
      context[:api_company].id == ::Company.swoop_id
    when NilClass
      context[:api_user] == object.customer
    else
      false
    end
  end

  field :company,
        Types::CompanyType,
        null: true,
        required_roles: -> (roles) {
          # allow fleet companies or customers to view the company info (but not rescue companies)
          roles.include?(:fleet) || roles.include?(:customer)
        },
        method: :fleet_company,
        preload: :fleet_company

  field :department,
        Types::DepartmentType,
        description: "The client department the job is for",
        null: true,
        if_company: -> (company) {
          !company.is_a?(::FleetCompany) || company.has_feature?(Feature::DEPARTMENTS)
        }

  field :name,
        String,
        description: 'The name of the Client.',
        null: true,
        required_roles: :fleet,
        deprecation_reason: "Please use job.client.company.name instead"

  def name
    fleet_company&.name
  end

  field :phone,
        Types::E164PhoneNumberType,
        description: 'Company phone.',
        null: true,
        required_roles: :fleet,
        deprecation_reason: "Please use job.client.company.phone instead"

  def phone
    fleet_company&.phone
  end

  field :site,
        Types::SiteType,
        null: true,
        method: :fleet_site,
        required_features: Feature::CLIENT_SITES,
        required_roles: :fleet

  private

  def fleet_company
    object&.fleet_company
  end

end
