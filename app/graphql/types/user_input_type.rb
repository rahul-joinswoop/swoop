# frozen_string_literal: true

class Types::UserInputType < Types::BaseInputObject

  argument :name, String, required: true
  argument :phone, Types::E164PhoneNumberType, required: false
  argument :email, Types::EmailType, required: false
  argument :roles, [String], required: false

end
