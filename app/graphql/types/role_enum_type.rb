# frozen_string_literal: true

class Types::RoleEnumType < Types::BaseEnum

  graphql_name 'RoleType'
  description 'Role Type'

  Role::NAME_TO_STATUS_MAP.each do |k, v|
    value(k.gsub(/\s+/, ''), Role::NAME_TO_DESCRIPTION_MAP[k], value: v)
  end

end
