# frozen_string_literal: true

class Types::SiteInputType < Types::BaseInputObject

  argument :name, String, required: true
  argument :location, Types::LocationInputType, required: true

end
