# frozen_string_literal: true

class Types::ServiceInputType < Types::BaseInputObject

  argument :class_type, Types::VehicleClassTypeNameType, description: "Deprecated: please use classTypeName", required: false
  argument :class_type_name, Types::VehicleClassTypeNameType, description: "Class of partner vehicle needed to complete the job", required: false
  argument :name, Types::ServiceCodeNameType, description: "See below section for more information on Services.", required: false
  argument :will_store, Boolean, description: 'Whether or not to store the customers vehicle', required: false
  argument :symptom, Types::SymptomNameType, description: "See below section for more information on Symptoms.", required: false, required_features: Feature::SHOW_SYMPTOMS
  argument :scheduled_for, String, description: "Customer can optionally schedule the job for the future. Should be in ISO 8601 format.", required: false
  argument :answers,
           [Types::QuestionAnswerInputType],
           description: "See below section for more information on Questions & Answers.",
           required: false,
           required_features: Feature::QUESTIONS,
           required_roles: [:fleet]
  argument :storage_type, Types::StorageTypeNameType, description: "Deprecated: please use storageTypeName", required: false
  argument :storage_type_name, Types::StorageTypeNameType, description: "See below section for more information on Storage Types.", required: false
  argument :storage_site, Types::SiteNameType, description: 'Name of the Storage Site', required: false, required_roles: :rescue

end
