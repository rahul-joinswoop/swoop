# frozen_string_literal: true

class Types::StorageTypeNameType < Types::BaseScalar

  description 'Name of the Storage Type.'

  def self.coerce_input(value, context)
    Resolvers::StorageTypes.new(object: context[:api_company], context: context).resolve.find_by!(name: value)
  rescue StandardError => e
    # if we get any other error here we should log it to rollbar, it means
    # something else went wrong. we can still return the generic validation
    # messaage to our client
    Rollbar.error(e) unless e.is_a?(ActiveRecord::RecordNotFound)

    raise GraphQL::CoercionError, "#{value.inspect} is not a valid StorageType"
  end

  def self.coerce_result(value, _ctx)
    value&.name&.to_s
  end

end
