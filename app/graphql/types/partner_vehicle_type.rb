# frozen_string_literal: true

class Types::PartnerVehicleType < Types::BaseObject

  implements Interfaces::VehicleInterface
  implements GraphQL::Relay::Node.interface

  description "The vehicle used by the Partner during a Job execution"

  def self.authorized?(object, context)
    super &&
    case context[:api_company]
    when FleetCompany
      context[:api_company].fleet_rescue_vehicles.where(id: object.id).exists?
    when RescueCompany
      object.company_id == context[:api_company].id
    when SuperCompany
      case context[:api_company].id
      when ::Company.swoop_id
        if context.has_role?(:root)
          # root user, can query any vehicles (including deleted ones)
          true
        else
          # dispatcher - return back the same as FleetCompany
          context[:api_company].fleet_rescue_vehicles.where(id: object.id).exists?
        end
      else
        false
      end
    when NilClass
      object.current_jobs.joins(:customer).where(drives: { user_id: context[:api_user].id }).exists?
    else
      false
    end
  end

  global_id_field :id

  field :active, Boolean, null: false

  field :company, Types::CompanyType, null: true

  def company(**args)
    # TODO - check this logic
    ::Resolvers::Company.new(object: object, context: context).resolve(args)
  end

  field :dedicated_to_swoop, Boolean, description: "TBD", null: false

  field :driver, Types::UserType, null: true, preload: :driver, description: "The current Driver of this vehicle"
  def driver
    # unfortunately we can't filter here based on job status like we do in job_partner_type because we don't have
    # access to a job object (there may not even be one). we can still only allow the first name here
    if context.is_client? && object.driver.present?
      object.driver.full_name = object.driver.first_name
    end
    object.driver
  end

  field :lat, Float, null: true, deprecation_reason: "Please use vehicle.location.lat instead"
  field :lng, Float, null: true, deprecation_reason: "Please use vehicle.location.lng instead"

end
