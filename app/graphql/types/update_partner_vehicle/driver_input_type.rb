# frozen_string_literal: true

# user by UpdatePartnerVehicle mutation
module Types
  module UpdatePartnerVehicle
    class DriverInputType < Types::BaseInputObject

      description "Driver input for PartnerVehicle update."

      # can be null to allow unassigning the PartnerVehicle from the Driver
      argument :id, ID, required: false do
        description <<~DESCRIPTION
          A PartnerDriver id. Used to assign PartnerVehicles to Drivers.
          If the id is passed as null, the Driver will be unassigned.
          Note: drivers can only assign or unassign themselves.
        DESCRIPTION
      end

    end
  end
end
