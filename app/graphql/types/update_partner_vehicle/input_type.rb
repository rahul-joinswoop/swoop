# frozen_string_literal: true

# user by UpdatePartnerVehicle mutation
module Types
  module UpdatePartnerVehicle
    class InputType < Types::BaseInputObject

      argument :id, ID, required: true
      argument :lat, Float, required: false, description: "Deprecated, please use partnerVehicle.location.lat instead"
      argument :lng, Float, required: false, description: "Deprecated, please use partnerVehicle.location.lng instead"
      argument :location, VehicleLocationInputType, required: false

    end
  end
end
