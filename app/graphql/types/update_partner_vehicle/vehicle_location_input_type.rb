# frozen_string_literal: true

# user by UpdatePartnerVehicle mutation
module Types
  module UpdatePartnerVehicle
    class VehicleLocationInputType < Types::BaseInputObject

      argument :lat, Float, required: false
      argument :lng, Float, required: false

    end
  end
end
