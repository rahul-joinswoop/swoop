# frozen_string_literal: true

class Types::InvoiceInputType < Types::BaseInputObject

  class Types::InvoiceJobInputType < Types::BaseInputObject

    argument :id, ID, required: true

  end

  class Types::InvoiceRateTypeInputType < Types::BaseInputObject

    argument :type, Types::RateTypeEnumType, required: :true

  end

  argument :id, ID, required: true
  argument :balance, String, required: true
  argument :class_type, Types::VehicleClassTypeNameType, required: false
  argument :class_type_name, Types::VehicleClassTypeNameType, required: false
  argument :job, Types::InvoiceJobInputType, required: true
  argument :line_items, [Types::InvoiceLineItemInputType], required: true
  argument :notes, String, required: false
  argument :payments, [Types::InvoicePaymentOrCreditInputType], required: true
  argument :previous_class_type, Types::VehicleClassTypeNameType, required: false
  argument :previous_rate_type, Types::InvoiceRateTypeInputType, required: false
  argument :rate_type, Types::InvoiceRateTypeInputType, required: true
  argument :subtotal, String, required: true
  argument :total_amount, String, required: true

end
