# frozen_string_literal: true

class Types::ViewerUnionType < Types::BaseUnion

  graphql_name "Viewer"
  description "Entity associated with current API credentials"
  possible_types Types::UserType,
                 Types::ApplicationType,
                 Types::CompanyType

  def self.resolve_type(value, ctx)
    case value
    when ::User
      Types::UserType
    when Doorkeeper::Application
      Types::ApplicationType
    when ::Company
      Types::CompanyType
    else
      raise("Unexpected object: #{value}")
    end
  end

end
