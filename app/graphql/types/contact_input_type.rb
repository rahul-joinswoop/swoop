# frozen_string_literal: true

class Types::ContactInputType < Types::BaseInputObject

  argument :name, String, description: "First and last name of the customer combined.", required: false
  argument :fullName, String, description: "Deprecated: please use name instead.", required: false
  argument :phone, Types::E164PhoneNumberType, description: "Phone number of the contact.", required: false

end
