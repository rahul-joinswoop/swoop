# frozen_string_literal: true

class CompanyReport < ApplicationRecord

  belongs_to :report
  belongs_to :company

  # This is used to generate a report.
  # By default it will call ReportResult#execute_asynchronously!, meaning that it will be handled by delayed_jobs.
  #
  # If the caller needs it to be synchronously executed, set run_synchronously = true.
  def run(user, submitted_params, after_callback = nil, format = nil, collated = false, run_synchronously = false)
    result = ReportResult.create!(
      report: report,
      company: company,
      user_id: user.id,
      format: format,
      collated: collated
    )

    if run_synchronously
      result.execute_synchronously!(submitted_params, after_callback)
    else
      result.execute_asynchronously!(submitted_params, after_callback)
    end

    result
  end

  def handle_global(api_company)
    if !company
      self.company = api_company
      readonly!
    end
    self
  end

end
