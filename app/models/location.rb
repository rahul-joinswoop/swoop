# frozen_string_literal: true

# Representation of a geographic location.
# NOTE: The `address` field on this model is a full, one line display address.
class Location < BaseModel

  include DistanceToLocation
  include HistoryItemTarget
  include LongitudeLatitude

  belongs_to :user
  has_many :current_jobs,
           -> { where status: [Job::DISPATCHED, Job::ENROUTE, Job::ONSITE, Job::TOWING, Job::TOWDESTINATION] },
           class_name: "Job", foreign_key: "service_location_id", inverse_of: :service_location
  belongs_to :site
  belongs_to :job
  belongs_to :location_type

  after_commit :resolve_place_id, on: :create

  after_commit do
    Subscribable.trigger_job_updated current_jobs.union(job.present? ? Job.where(id: job.id) : Job.none)
    Subscribable.trigger_user_updated user
  end

  # Ensure that the job index is up to date with any changes made on the association
  after_commit { job.reindex if job }

  # validates :zip, length: { minimum: 5 }, allow_blank: true
  validates_length_of :street_number, maximum: 12
  validates_length_of :street_name, maximum: 60

  def resolve_place_id
    Rails.logger.debug "LOCATION resolve_place_id called on #{id} with #{place_id}"
    if address && !having_any_address_component?
      PlacesResolver.perform_async(id)
    end
  end

  def listeners
    ret = Set.new
    # Currently not used anywhere
    # self.current_jobs.each do |job|
    #  ret.merge job.listeners
    # end
    ret
  end

  def render_to_hash
    LocationSerializer.new(self).serializable_hash
  end

  def copy_location(extra_attributes = nil)
    copy_attributes = [
      'lat',
      'lng',
      'lnglat',
      'address',
      'street_number',
      'street_name',
      'exact',
      'state',
      'city',
      'street',
      'zip',
      'country',
      'place_id',
      'location_type_id',
    ]
    copy = self.class.new
    copy_attributes.each do |attr|
      copy[attr] = self[attr]
    end
    # TODO: perhaps create a deep copy of user as well
    copy
  end

  def city_state
    [city, state].compact.join(', ')
  end

  def missing_address_component?
    !(address && street && street_name && street_number && city && state && zip && country)
  end

  def having_any_address_component?
    (street || street_name || street_number || state || city || zip || country)
  end

  def address_without_usa
    address.sub(', USA', '')
  end

  # this will only work if the address _starts_ with the street number, which should usually be the case but who knows?
  def address_without_street_number
    address_without_usa.sub(/^#{(StreetAddress::US.parse(address_without_usa)&.number)}\s+?/, '')
  end

  def latlng_only?
    (lat && lng) && !(address || having_any_address_component?)
  end

  def rescue_companies_whose_teritories_contain_it
    RescueCompany.containing_lat_lng_in_its_territories(lat, lng)
  end

  def territories_that_contain_it
    Territory.containing_lat_lng(lat, lng)
  end

end
