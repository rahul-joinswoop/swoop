# frozen_string_literal: true

class APIAccessToken < ApplicationRecord

  belongs_to :company

  has_many :isscs, -> { where(deleted_at: nil) }

  before_save :sanitize, if: :deleted_at_changed?

  class << self

    def token_for_vendor(company, vendorid)
      APIAccessToken.find_by(company: company, vendorid: vendorid, deleted_at: nil)
    end

  end

  # Load job from a dispatchid. This code ensures that the dispatchid corresponds to
  # the issc records attached to the token so we don't accidentally load a duplicate id.
  def find_job_by_dispatchid(dispatchid)
    if dispatchid.present? && id.present?
      Job.not_deleted.joins(issc_dispatch_request: :issc)
        .where(isscs: { deleted_at: nil, api_access_token_id: id })
        .where(issc_dispatch_requests: { dispatchid: dispatchid })
        .first
    else
      nil
    end
  end

  def location_ids_with_site
    isscs.joins(:site)
      .where(sites: { dispatchable: true })
      .map(&:issc_location)
      .map do |issc_loc|
        [
          ["locationId", issc_loc.location_id],
          ["address", issc_loc.location&.street],
        ].to_h.with_indifferent_access
      end
  end

  def all_associated_location_ids
    isscs.map(&:issc_location).compact.map do |issc_loc|
      [
        ["locationId", issc_loc.location_id],
        ["address", issc_loc.location&.street],
        ["zip", issc_loc.location&.zip],
      ].to_h.with_indifferent_access
    end
  end

  def current_status
    return :disconnected if !connected
    return :needs_configured if isscs.map(&:site).any?(&:nil?)

    isscs.first&.status || :needs_configured
  end

  # Return an instance of Agero::Rsc::API bound to this token.
  def api
    if access_token.present? && source.to_s == "agero"
      @api ||= Agero::Rsc::API.new(access_token)
    else
      nil
    end
  end

  def sign_in
    api.sign_in(id)
  end

  def sign_out
    api.sign_out(id)
  end

  # Subscribe to server notifications and keep the subscription and callback token in sync.
  def subscribe_to_notifications
    return if subscriptionid.present?
    begin
      self.callback_token = SecureRandom.hex
      response = api.subscribe_to_server_notifications(callback_token)
      self.subscriptionid = response["subscriptionId"]
      save!
    rescue => e
      unsubscribe_from_notifications
      raise e
    end
  end

  # Unsubscribe to server notifications and keep the subscription and callback token in sync.
  def unsubscribe_from_notifications
    if subscriptionid.present?
      api.unsubscribe_from_server_notifications(subscriptionid)
      update!(subscriptionid: nil, callback_token: nil)
    end
  end

  # Logout from RSC and delete associated Issc records.
  def logout!
    begin
      sign_out
      unsubscribe_from_notifications
    rescue Agero::Rsc::AccessTokenError => e
      logger.warn(e)
    end

    deleted_time = Time.current
    transaction do
      isscs.each do |issc|
        location_id = issc.issc_location&.location_id
        if location_id
          site_facility_maps = SiteFacilityMap.where(company_id: company_id, fleet_company_id: Company.agero_id, facility_id: location_id)
          site_facility_maps.each do |mapping|
            mapping.update!(deleted_at: deleted_time)
          end
        end

        issc.issc_location.update!(deleted_at: deleted_time)
        issc.update!(deleted_at: deleted_time)
      end

      update!(deleted_at: deleted_time)
    end
  end

  private

  def sanitize
    if deleted_at
      self.access_token = '-'
      self.callback_token = nil
      self.connected = false
    end
  end

end
