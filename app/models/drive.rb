# frozen_string_literal: true

# The class represents the customer and their vehicle that is being serviced by a Job.
class Drive < ApplicationRecord

  belongs_to :user
  belongs_to :vehicle
  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :vehicle

  has_one :job, foreign_key: 'driver_id', inverse_of: :driver

  validates :user, presence: true
  validates :vehicle, presence: true

  # Ensure that the job index is up to date with any changes made on the association
  after_commit { job.reindex if job }

  def description
    if user
      "#{user.full_name} #{vehicle&.description}".strip
    end
  end

  def copy_driver(extra_attributes = nil)
    copy = self.class.new

    if !self["user_id"].nil?
      user_copy = user.copy_user
      user_copy.save!
      copy["user_id"] = user_copy.id
    end
    if !self["vehicle_id"].nil?
      vehicle_copy = vehicle.copy_vehicle
      vehicle_copy.save!
      copy["vehicle_id"] = vehicle_copy.id
    end
    # copy["vehicle_id"] = self.vehicle_id
    # copy["user_id"] = self.user_id
    copy
  end

end
