# frozen_string_literal: true

class VehicleInventoryItem < InventoryItem

  TYPENAME = "VehicleInventoryItem"

end
