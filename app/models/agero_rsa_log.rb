# frozen_string_literal: true

class AgeroRSALog < ApplicationRecord

  belongs_to :job
  belongs_to :partner_company, class_name: "Company"

end
