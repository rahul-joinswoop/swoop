# frozen_string_literal: true

require 'aasm'

class Invoice < InvoicingLedgerItem

  include ValidateCurrency

  include LoggableJob
  include AASM
  include SoftDelete
  include HistoryItemTarget

  #
  # holds in memory data that is required during GraphQL edit invoice flow
  #
  attr_accessor :edit_invoice_metadata

  VERSION = 2

  # Partner sending to customer, swoop or fleet.
  CREATED = 'created' # Business definition: A new invoice, with no line items that is not yet visible to the user
  PARTNER_NEW = 'partner_new' # Business definition: An invoice which has been populated using available rates, which is visible to the user
  ON_SITE_PAYMENT = 'on_site_payment'
  PARTNER_APPROVED = 'partner_approved'
  PARTNER_SENT = 'partner_sent'

  # Fleet receiving and paying partner.
  FLEET_APPROVED = 'fleet_approved'
  FLEET_DOWNLOADED = 'fleet_downloaded'
  FLEET_REJECTED = 'fleet_rejected'

  # Swoop receiving and paying partner.
  SWOOP_APPROVED_PARTNER = 'swoop_approved_partner'
  SWOOP_DOWNLOADING_PARTNER = 'swoop_downloading_partner'
  SWOOP_DOWNLOADED_PARTNER = 'swoop_downloaded_partner'
  SWOOP_REJECTED_PARTNER = 'swoop_rejected_partner'

  # Swoop sending to fleet.
  SWOOP_NEW = 'swoop_new'
  SWOOP_APPROVED_FLEET = 'swoop_approved_fleet'
  SWOOP_SENDING_FLEET = 'swoop_sending_fleet'
  SWOOP_SENT_FLEET = 'swoop_sent_fleet'
  SWOOP_DELETED_FLEET = 'swoop_deleted_fleet'

  SWOOP_SEND_FLEET_ERROR = 'swoop_send_fleet_error'

  # Fleet sending to their customer
  FLEET_CUSTOMER_NEW = 'fleet_customer_new'

  # Deprecated.
  PARTNER_OFFLINE_NEW = 'partner_offline_new'

  STATES_PAST_SENT_FOR_FLEET = [
    PARTNER_APPROVED,
    PARTNER_SENT,
    FLEET_APPROVED,
    FLEET_DOWNLOADED,
    ON_SITE_PAYMENT,
  ].freeze

  EDITABLE_STATES = [
    CREATED,
    PARTNER_NEW,
    PARTNER_APPROVED,
    SWOOP_NEW,
    PARTNER_OFFLINE_NEW,
    SWOOP_REJECTED_PARTNER,
    FLEET_REJECTED,
  ].freeze

  delegate :email, to: :job, prefix: true, allow_nil: true

  belongs_to :job
  belongs_to :incoming_invoice, class_name: "Invoice"
  belongs_to :recipient_company, class_name: "Company"
  belongs_to :rate
  belongs_to :api_async_request, class_name: 'API::AsyncRequest'

  # Overload of recipient to enable preloading as appropriate. These associations are not
  # accessed directly, but rather via the `account_recipient` and `user_recipient` wrappers.
  belongs_to :_account_recipient, class_name: 'Account', foreign_key: :recipient_id, inverse_of: :invoices
  belongs_to :_user_recipient, class_name: 'User', foreign_key: :recipient_id, inverse_of: :invoices

  has_many :invoice_pdfs
  has_many :invoice_rejections
  has_many :payments, autosave: true

  has_many :payments_or_credits,
           -> { only_payments_or_credits.order(:created_at) },
           class_name: "InvoicingLedgerItem",
           inverse_of: :invoice

  has_many :refunds
  has_many :credit_notes
  has_many :invoicing_ledger_items
  has_many :charges, class_name: 'InvoicePaymentCharge'

  has_many :audit_invoice_statuses, autosave: true, inverse_of: :invoice

  accepts_nested_attributes_for :line_items
  accepts_nested_attributes_for :job

  # TODO this attr should have its own model type so to enable us to apply OO correctly.
  #   Currently it can be an Account or an User, completely different models with different structure.
  #   ticket https://swoopme.atlassian.net/browse/ENG-5031
  validates :recipient, presence: true
  validates :sender, presence: true
  validates :paid, inclusion: { in: [true, false] }
  validate_currency_matches :sender, :recipient

  before_validation :calculate_total_amount

  before_save :calculate_alert

  before_create :set_default_billing_type

  # This was causing the flashing in ticket 115975700052708
  # after_commit :publish_job, on: :update

  # app/models/report_result.rb Skips callbacks when updating the invoice
  # state during Tesla invoice download! Jobs are reindexed manually.
  after_commit :job_reindex, on: :create
  after_commit :job_reindex, on: :update, if: :reindex?
  after_commit :send_invoice, if: :sendable?

  scope :filter_between, -> start_at, end_at { where(" ? < invoicing_ledger_items.created_at AND invoicing_ledger_items.created_at <  ?", start_at, end_at) }
  scope :filter_full_name, -> full_name { joins(job: [:customer]).where("lower(users.first_name || ' ' || users.last_name) LIKE ?", "%#{full_name.downcase}%") }
  scope :filter_sender_company_id, -> sender_id { where(sender_id: sender_company_id) }
  scope :filter_recipient_company_id, -> recipient_company_id { where(recipient_company_id: recipient_copmany_id) }

  scope :filter_job_before, -> before { joins(:job).where("jobs.adjusted_created_at < ?", before) }
  scope :filter_job_after, -> after { joins(:job).where("jobs.adjusted_created_at > ?", after) }

  scope :filter_job_search, -> search { joins(job: [:account, driver: [:vehicle, :user]]).where("lower(vehicles.vin) LIKE lower(?) OR lower(vehicles.make) LIKE lower(?) OR lower(vehicles.model) LIKE lower(?) OR lower(vehicles.license) LIKE lower(?) OR lower(users.first_name || ' ' || users.last_name) LIKE lower(?) OR jobs.id=? OR lower(po_number) LIKE lower(?) OR lower(accounts.name) LIKE lower(?)  ", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", search.to_i, "%#{search}%", "%#{search}%") }
  # QQ how can i do these next to in a non-hard coded way?

  # TODO Job.active_and_draft_states is a hack to keep retrocompatibility with legacy code that relied
  # on Job.active_states when it contained 'draft'.
  # This and all spots that use it should be removed by https://swoopme.atlassian.net/browse/ENG-9905
  scope :filter_job_active, -> { joins(:job).where(jobs: { status: Job.active_and_draft_states }) }
  scope :filter_job_done, -> { joins(:job).where(jobs: { status: Job.invoice_states }) }
  scope :all_active_by_job, -> (job) { where(job: job).not_deleted }

  scope :filter_paid, -> { where(paid: true) }
  scope :filter_unpaid, -> { where(paid: false) }

  scope :associated_rendered_models, -> {
    preload(
      job: [
        :site,
        :fleet_site,
        :customer,
        :service_code,
        :eta,
        :pta,
        :sta,
        :estimates,
        :active_fleet_managed_invoice_from_swoop,
        { fleet_company: [:customer_types, :location_types] },
        { rescue_company: [:customer_types, :location_types] },
      ],
      invoice_rejections: [
        :invoice_reject_reason,
      ],
      _account_recipient: [
        :physical_location,
        client_company: [
          :service_codes,
          { partner_sites: :rescue_providers },
        ],
        company: {
          partner_sites: [
            :company,
            :isscs,
            :issc_locations,
            :issc_logins,
          ],
        },
      ],
      _user_recipient: [
        :location,
      ],
      sender: [
        :location,
        :features,
        :customer_types,
        :symptoms,
        :departments,
        :vehicle_categories,
        :role_permissions,
        :fleet_managed_clients,
        :fleet_in_house_clients,
      ],
    )
  }

  after_commit do
    Subscribable.trigger_job_updated job
  end

  after_commit do
    # we also validate this in the service but it's a simple optimization to short-circuit
    # most of the calls here so here it is
    if state_changed? && state.in?(InvoiceAutoApprover::CheckInvoiceState::VALID_STATES)
      InvoiceAutoApproverWorker.perform_async(id)
    end
  end

  # IMPORTANT: If you add, remove, or rename an invoice status here, the corresponding change needs to be made for
  # the invoice_status PostgreSQL enum, which the invoice audit trails depends on.
  enum state: {
    created: CREATED,
    partner_new: PARTNER_NEW,
    partner_approved: PARTNER_APPROVED,
    partner_sent: PARTNER_SENT,
    fleet_approved: FLEET_APPROVED,
    fleet_downloaded: FLEET_DOWNLOADED,
    fleet_rejected: FLEET_REJECTED,
    on_site_payment: ON_SITE_PAYMENT,
    partner_offline_new: PARTNER_OFFLINE_NEW,
    swoop_new: SWOOP_NEW,
    swoop_approved_fleet: SWOOP_APPROVED_FLEET,

    fleet_customer_new: FLEET_CUSTOMER_NEW,

    # This is a transitional state, and is set while we're trying to export
    # the invoice to the fleet external system.
    #
    # The sync step of the flow that sets this is either:
    # InvoiceService::SwoopExportAllToFleet or InvoiceService::SwoopDownloadAll
    #
    # The async step that will process the invoice while in swoop_sending_fleet is either:
    # InvoiceService::ExportToFleet::FleetResponse::ExportAllWorker or
    # GenerateSwoopFleetInvoiceReportWorker
    #
    # These workers ^ will either change the invoice.state to:
    #   swoop_sent_fleet - if well succeeded
    #   swoop_approved_fleet - if fails for any reason
    #
    # Up to now, the fleet managed that uses the export flow (SwoopExportAllToFleet) is
    # Fleet Response only. All other FleetManaged will use the SwoopDownloadAll.
    #
    swoop_sending_fleet: SWOOP_SENDING_FLEET,

    # set when either:
    #
    #  - the worker process mentioned on swoop_sending_fleet ^ is well succeeded
    swoop_sent_fleet: SWOOP_SENT_FLEET,
    swoop_deleted_fleet: SWOOP_DELETED_FLEET,
    swoop_approved_partner: SWOOP_APPROVED_PARTNER,

    # This is a transitional state, and is set while Swoop is trying to download
    # partner invoices.
    #
    # The sync step of the flow that sets this is InvoiceService::SwoopDownloadAll.
    # GenerateSwoopPartnerInvoiceReportWorker
    #
    # This worker ^ will either change the invoice.state to:
    #   swoop_downloaded_partner - if well succeeded
    #   swoop_approved_partner - if fails for any reason
    #
    swoop_downloading_partner: SWOOP_DOWNLOADING_PARTNER,
    swoop_downloaded_partner: SWOOP_DOWNLOADED_PARTNER,
    swoop_rejected_partner: SWOOP_REJECTED_PARTNER,
  }

  aasm column: :state, enum: true do
    state :created, initial: true

    state :partner_new
    state :partner_approved
    state :partner_sent

    state :swoop_approved_partner
    state :swoop_downloading_partner
    state :swoop_downloaded_partner
    state :swoop_rejected_partner

    state :swoop_new
    state :swoop_approved_fleet
    state :swoop_sending_fleet
    state :swoop_sent_fleet
    state :swoop_deleted_fleet

    state :fleet_approved
    state :fleet_downloaded
    state :fleet_rejected

    state :on_site_payment
    state :partner_offline_new

    state :fleet_customer_new

    # new
    event :partner_new do
      transitions from: :created, to: :partner_new
      # transtion below supports payment type changing by Tesla
      transitions(
        from: STATES_PAST_SENT_FOR_FLEET,
        to: :partner_new,
        after: :reset_paid
      )
    end

    event :partner_approved do
      transitions from: [:partner_new, :partner_sent], to: :partner_approved, guard: :not_on_site_payment?, after: :reset_paid
      transitions from: [:partner_new], to: :on_site_payment, guard: :on_site_payment?
      transitions from: [:fleet_rejected, :swoop_rejected_partner], to: :partner_approved, guard: :not_on_site_payment?, after: :reset_paid
    end

    event :partner_sent, after: [:mark_as_sendable, :mark_as_sent, :track_metrics] do
      transitions from: :partner_approved, to: :partner_sent
    end

    event :partner_mark_sent, after: [:mark_as_sent] do
      transitions from: :partner_approved, to: :partner_sent
    end

    event :partner_undo_approval do
      transitions from: :partner_approved, to: :partner_new, guard: [:not_on_site_payment?]
    end

    event :partner_move_back_to_new, after: [:mark_as_not_sendable, :clear_sent_at] do
      transitions from: :partner_sent, to: :partner_approved
      transitions from: :on_site_payment, to: :partner_new, guard: [:on_site_payment?]
    end

    event :fleet_approved do # after lock
      transitions from: :partner_sent, to: :fleet_approved, guard: :not_on_site_payment?
      transitions from: :partner_sent, to: :on_site_payment, after: :mark_paid, guard: :on_site_payment?
    end

    event :fleet_undo_approval do
      transitions from: :fleet_approved, to: :partner_sent, guard: [:not_on_site_payment?, Proc.new { !paid }]
    end

    event :fleet_reject do
      transitions from: :partner_sent, to: :fleet_rejected
    end

    event :swoop_approved_partner do
      transitions from: [:partner_sent, :partner_offline_new], to: :swoop_approved_partner
    end

    event :swoop_downloading_partner do # after lock #only by swoop
      transitions from: :swoop_approved_partner, to: :swoop_downloading_partner
    end

    event :swoop_downloaded_partner do # after lock #only by swoop
      transitions from: [:swoop_approved_partner, :partner_sent], to: :swoop_downloaded_partner
    end

    event :swoop_undo_approval_partner do
      transitions from: :swoop_approved_partner, to: :partner_sent, guard: Proc.new { !paid }
    end

    event :swoop_approved_fleet do # after lock #only by swoop
      transitions from: :swoop_new, to: :swoop_approved_fleet
    end

    event :swoop_undo_approval_fleet do
      transitions from: :swoop_approved_fleet, to: :swoop_new
    end

    event :swoop_delete_fleet, after: Proc.new { self.deleted_at = Time.now } do # after lock #only by swoop
      transitions from: [:swoop_new, :swoop_approved_fleet, :swoop_sending_fleet, :swoop_sent_fleet], to: :swoop_deleted_fleet
    end

    event :swoop_sending_fleet do # after lock #only by swoop
      transitions from: [:swoop_approved_fleet], to: :swoop_sending_fleet
    end

    event :swoop_sent_fleet do
      # we allow it to transition from swoop_approved_fleet too temporarily,
      # until https://swoopme.atlassian.net/browse/ENG-5488 is done
      transitions from: [:swoop_approved_fleet, :swoop_sending_fleet], to: :swoop_sent_fleet
    end

    event :swoop_send_fleet_error do
      transitions from: :swoop_sending_fleet, to: :swoop_approved_fleet
    end

    event :swoop_reject_partner do
      transitions from: :partner_sent, to: :swoop_rejected_partner
    end

    event :fleet_downloaded do
      transitions from: :fleet_approved, to: :fleet_downloaded
    end

    event :fleet_move_back_from_downloaded, after: :reset_paid do
      transitions from: :fleet_downloaded, to: :fleet_approved, guard: :not_on_site_payment?
    end

    # new
    event :swoop_new, after: :reset_paid do
      transitions from: [:created, :swoop_sent_fleet], to: :swoop_new
    end

    # event :swoop_sent do
    #  transitions from: :swoop_new, to: :swoop_sent
    # end
    #
    #

    event :fleet_customer_new do
      transitions from: :created, to: :fleet_customer_new
    end

    # Whenever an AASM transition happens, create an audit trail in the database
    after_all_transitions :audit_status_change
  end

  def reindex?
    state_changed? || paid_changed?
  end

  def paid_changed?
    saved_change_to_paid?
  end

  def generate_pdf
    invoice_pdf = invoice_pdfs.create!
    InvoicePdfWorker.perform_async(id, invoice_pdf.id)
    invoice_pdf
  end

  def sendable?
    Rails.logger.debug "INVOICE(#{id}) sendable? #{!!@sendable}"
    !!@sendable
  end

  def mark_as_sendable
    @sendable = true
  end

  def mark_as_not_sendable
    @sendable = false
  end

  # If the job was attended by a site whose company is a child one,
  # uses the child company name.
  # more details on: https://swoopme.atlassian.net/browse/ENG-2539
  #
  # Otherwise, uses the sender name.
  def sender_name
    if job.site && job.site.owner_company
      return job.site.owner_company.name
    end

    sender.try(:name)
  end

  def account_recipient
    _account_recipient if recipient_type == "Account"
  end

  def user_recipient
    _user_recipient if recipient_type == "User"
  end

  def add_driver_name_to_pdf?
    return false unless sender
    return false if sender_id == Company.swoop_id
    sender.has_feature(Feature::ADD_DRIVER_NAME_ON_INVOICE).present?
  end

  def job_reindex
    job.reindex
  end

  def state_changed?
    saved_change_to_state?
  end

  def emailable?
    customized_recipient_email? || recipient.invoices_emailable?
  end

  def customized_recipient_email?
    !(email_to.blank? && email_cc.blank? && email_bcc.blank?)
  end

  def accounting_to_email
    return email_to if customized_recipient_email?
    recipient.get_accounting_email
  end

  def accounting_cc_email
    return email_cc if customized_recipient_email?
    recipient.accounting_email_cc
  end

  def accounting_bcc_email
    return email_bcc if customized_recipient_email?
    recipient.accounting_email_bcc
  end

  # @deprecated - use recipient_client_company
  def recipient_fleet_company
    recipient.fleet_company
  end

  def recipient_client_company
    recipient.client_company
  end

  # TODO: This could be a utility or class method
  def get_address_parts(owner, include_email = false)
    address = []
    if owner.location.present? && owner.location.address?
      address = owner.location.address.split(',', 2)
    end
    if owner.phone?
      address << "Phone: " + owner.phone.sub('+1', '')
    end
    if include_email
      if owner.try(:accounting_email)
        address << owner.try(:accounting_email)
      elsif owner.try(:email)
        address << owner.try(:email)
      end
    end

    address
  end

  def from_address
    if sender_id == Company.swoop_id
      get_address_parts(Company.swoop)
    elsif job.site
      get_address_parts(job.site)
    else
      get_address_parts(job.rescue_company)
    end
  end

  def send_address
    if replace_invoice_info? && job.storage.released_to_account
      return get_address_parts(job.storage.released_to_account, true)
    elsif replace_invoice_info? && job.storage.released_to_user
      return get_address_parts(job.storage.released_to_user, true)
    elsif recipient_company
      return get_address_parts(recipient_company, true)
    else
      return get_address_parts(recipient, true)
    end

    []
  end

  def bill_to_name
    to_name = if replace_invoice_info? && job.storage.released_to_account
                job.storage.released_to_account.name
              elsif replace_invoice_info? && job.storage.released_to_user
                job.storage.released_to_user.full_name
              elsif recipient.try(:full_name)
                recipient.full_name
              else
                recipient.name
              end

    if (to_name == "Tesla") && job.customer_type.try(:name)
      to_name = to_name + " (" + job.customer_type.try(:name) + ")"
    end

    to_name
  end

  def replace_invoice_info?
    !!job.storage&.replace_invoice_info
  end

  def is_enterprise?
    enterprise = Company.enterprise
    recipient_company_id == enterprise.id if enterprise
  end

  def is_turo?
    recipient_company_id == Company.enterprise_id
  end

  def on_site_payment?
    return false if is_enterprise?

    customer_type = job.customer_type
    logger.debug("Customer Type: #{customer_type}")
    logger.debug("recipient: #{recipient.inspect}")
    if customer_type
      logger.debug "#{customer_type.inspect}"
    end
    if (recipient_type == "User") ||
      (customer_type &&
       ((customer_type.name == "P Card") ||
       (customer_type.name == "Customer Pay")))
      true
    end
  end

  def find_lineitem_by_job_and_description(job, description)
    line_items.each do |li|
      if (li.job == job) && (li.description == description)
        return li
      end
    end
    nil
  end

  def mem_find_storage(job)
    line_items.each do |li|
      # This log is super super heavy and is flooding staging, lets comment out for now
      # XXX- Put back in for production
      # logger.debug "checking li #{li.inspect}"
      if (li.job == job) && (li.description == 'Storage')
        return li
      end
    end
    nil
  end

  def not_on_site_payment?
    !on_site_payment?
  end

  # Invoices can become not editable depending on their subclass or current state.
  def editable?
    job&.invoice_editable? || EDITABLE_STATES.include?(state)
  end

  def new_state?
    [PARTNER_NEW, SWOOP_NEW, PARTNER_OFFLINE_NEW, FLEET_CUSTOMER_NEW].include? state
  end

  def mark_paid
    self.paid = true
    self.mark_paid_at = Time.now
  end

  def reset_paid
    self.paid = false
    self.mark_paid_at = nil
  end

  def resend_invoice(to: nil, cc: nil, bcc: nil)
    if recipient_type == "User"
      send_reciept(to: to, cc: cc, bcc: bcc)
    else
      send_invoice(to: to, cc: cc, bcc: bcc)
    end
  end

  def send_invoice(to: nil, cc: nil, bcc: nil)
    invoice_pdf = generate_pdf
    Delayed::Job.enqueue EmailInvoice.new(id, invoice_pdf.id, to: to, cc: cc, bcc: bcc)
  end

  def send_reciept(to: nil, cc: nil, bcc: nil)
    invoice_pdf = generate_pdf
    Delayed::Job.enqueue EmailInvoiceReceipt.new(id, invoice_pdf.id, to: to, cc: cc, bcc: bcc)
  end

  def send_charge_card_receipt(to: nil, cc: nil, bcc: nil)
    invoice_pdf = generate_pdf

    Delayed::Job.enqueue EmailInvoiceReceipt.new(
      id, invoice_pdf.id, to: to, cc: cc, bcc: bcc, customize_for_credit_card_charge: true
    )
  end

  def listeners
    ret = Set.new
    ret << sender
    if recipient.try(:client_company)
      ret << recipient.client_company
    end
    ret << Company.swoop
    ret
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/invoices/_invoice", locals: { invoice: self }))
  end

  def render_job_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/invoices/_job", locals: { invoice: self }))
  end

  def balance
    t = total_amount
    p = payments_and_credits_total_amount
    return nil unless t && p
    total_amount + payments_and_credits_total_amount
  end

  def payments_and_credits_total_amount
    total = BigDecimal.new("0")

    # There can be payments_or_credits that are not persisted yet, so we
    # use array.reject here instead of a ar_colletion.where to filter out deleted ones
    payments_or_credits.reject(&:deleted_at).each do |payment|
      if payment.total_amount
        total += payment.total_amount
      end
    end

    total
  end

  def new_payment_or_credit(clz, new_args)
    args = {}
    args[:invoice_id] = id
    args[:currency] = currency
    args[:sender] = sender
    args[:recipient] = recipient
    args[:job_id] = job_id

    clz.new(args.merge(new_args))
  end

  def freeze
    self.description = render_job_to_hash.to_json
  end

  def send_to_recipient
    # freeze
    mark_as_sent
    send_reciept
  end

  def mark_as_sent
    self.sent_at = Time.now
  end

  def clear_sent_at
    self.sent_at = nil
  end

  def self.incoming(company, states, page, per_page)
    params = {
      client_company_id: company.id,
      per_page: per_page,
      offset: per_page.to_i * (page.to_i - 1),
    }

    if states
      params[:states] = states
      Invoice.find_by_sql([
        "SELECT  invoicing_ledger_items.* FROM invoicing_ledger_items INNER JOIN accounts ON invoicing_ledger_items.recipient_id = accounts.id WHERE invoicing_ledger_items.type IN ('Invoice') AND accounts.client_company_id = :client_company_id AND invoicing_ledger_items.state IN (:states)  ORDER BY invoicing_ledger_items.created_at DESC LIMIT :per_page OFFSET :offset",
        params,
      ])
    else
      Invoice.find_by_sql([
        "SELECT  invoicing_ledger_items.* FROM invoicing_ledger_items INNER JOIN accounts ON invoicing_ledger_items.recipient_id = accounts.id WHERE invoicing_ledger_items.type IN ('Invoice') AND accounts.client_company_id = :client_company_id ORDER BY invoicing_ledger_items.created_at DESC LIMIT :per_page OFFSET :offset",
        params,
      ])

    end
  end

  def ticket_line_items
    ret = []
    line_items.each do |li|
      if !li.deleted_at && !li.tax?
        ret << {
          description: li.description,
          quantity: li.quantity,
          calc_type: li.calc_type,
          net_amount: number_with_precision(li.net_amount, precision: 2),
        }
      end
    end
    ret
  end

  def line_items_include_original_deleted(deleted)
    # logger.debug "line_items_include_original_deleted call with #{deleted}"
    ret = []
    line_items.each do |li|
      if li.include_original_deleted?(deleted)
        ret << li
      end
    end
  end

  # Get the current sum of the total amount from the line items
  def current_total_amount
    sum = 0.0
    line_items.each do |item|
      next if item.deleted_at.present? || item.marked_for_destruction?
      sum += item.net_amount if item.net_amount
      sum += item.tax_amount if item.tax_amount
    end
    sum
  end

  # PDW: override Invoicing to take account on deleted line items
  def calculate_total_amount
    #    raise StandardError.new "where?" # job.save
    logger.debug "INVOICE(#{job_id}) calculate_total_amount called"
    line_items = self.line_items # ledger_item_class_info.get(self, :line_items)
    return if is_a?(Payment) && line_items.empty?

    net_total = tax_total = BigDecimal('0')

    line_items.each do |line|
      # line.ledger_item=self
      line.valid? # Ensure any before_validation hooks are called

      if line.deleted_at.nil? && !line.marked_for_destruction?
        net_amount = line.net_amount
        tax_amount = line.tax_amount
        net_total += net_amount unless net_amount.nil?
        tax_total += tax_amount unless tax_amount.nil?
      end
    end

    self.total_amount = net_total + tax_total
    self.tax_amount = tax_total

    if ["partner_new", "partner_approved"].include? state
      self.fleet_ui_total_amount = 0
    else
      self.fleet_ui_total_amount = total_amount
    end
    logger.debug "INVOICE(#{job_id}) calculate_total_amount returning #{net_total}"

    net_total
  end

  def calculate_total_amount!
    calculate_total_amount
    save!
  end

  # this is currently being used by invoice.pdf.erb
  #
  def subtotal
    total_amount - tax_amount
  end

  # This is the sum of all line items that are not deleted, are not 'Tax' and are not 'Percent' calc type.
  # This is the subtotal we show to users in the UI.
  #
  # This logic was inspired by web UI SubTotalField:
  # https://github.com/joinswoop/swoop/blob/master/app/assets/javascripts/invoice_fields.coffee#L446-L449
  #
  # TODO https://swoopme.atlassian.net/browse/ENG-9915 - should we really have different 'Subtotal'
  #      values for the PDF and for the UI?
  def subtotal_for_ui
    line_items.reject { |li| li.deleted_at || li.marked_for_destruction? || li.tax? || li.percentage? }
      .map(&:net_amount).compact.sum
  end

  def discounts
    payments_or_credits.where(type: 'Discount', deleted_at: nil)
  end

  def calculate_alert
    logger.debug "INVOICE(#{id}): Calculating Alert"
    alert = false
    line_items.each do |line|
      if line.net_amount && (line.original_quantity != line.quantity)
        alert = true
        break
      end
    end

    logger.debug "INVOICE(#{id}): found alert=#{alert}"

    if ["partner_new", "partner_approved"].include? state
      self.fleet_alert = false
    else
      self.fleet_alert = alert
    end

    self.partner_alert = alert

    logger.debug "INVOICE(#{id}): Finished calc"
  end

  # If the job is FleetManagedJob, and this is a PartnerInvoice
  # approved or downloaded by Swoop, a correspondent FleetManagedInvoice
  # will exist.
  #
  # @return the 'FleetManagedInvoice'
  def find_active_fleet_managed_invoice
    job&.active_fleet_managed_invoice_from_swoop
  end

  # only ever used in b/app/assets/javascripts/components/invoice/list.coffee
  # via app/views/api/v1/invoices/_invoice.json.jbuilder
  FLEET_MANGED_INVOICE_APPROVED_OR_SENT_STATES = [
    'swoop_approved_fleet',
    'swoop_sending_fleet',
    'swoop_sent_fleet',
  ].freeze

  def fleet_managed_invoice_approved_or_sent?
    # NOTE: `find_active_fleet_managed_invoice` is not memoized. Use with care.
    FLEET_MANGED_INVOICE_APPROVED_OR_SENT_STATES.include?(find_active_fleet_managed_invoice&.state)
  end

  # TODO why do we need it? with this it never returns the
  # rate_type value from db, which should be the correct one IMO.
  def rate_type
    derived_rate_type || self[:rate_type]
  end

  def flat_rate?
    rate_type == "FlatRate"
  end

  #
  # Needed to let us expose it as Enum on GraphQL,
  # since original rate_type attr is a String.
  #
  # We try self[:rate_type] before trying calling rate_type
  # because Invoice#rate_type method overrides it by line_items.first.rate.type,
  # and that logic looks wrong, after all we do want the value that is set in the field.
  #
  # If we only use rate_type method, the invoice rate_type will be wrong in the UI.
  # This is noticed in GraphQL Edit Invoice flow. It is not noticed in the
  # web Edit Invoice flow because in web it *always* creates a new invoice when
  # rate_type is changed, and new invoices don't have RateType set, whilst in GraphQL
  # flow we do update the invoice with the new rate_type, and we do want it.
  #
  def friendly_rate_type
    rate_type_attr_val_or_li_derived = self[:rate_type] || rate_type

    if rate_type_attr_val_or_li_derived
      RateType.find_by_type(rate_type_attr_val_or_li_derived)
    end
  end

  def any_charge_receipt_email_sent?
    charges.where(charge_receipt_email_sent: true).exists?
  end

  def zero_invoice(job)
    Rails.logger.debug "Invoice::zero_invoice called #{self}"
    line_items.each do |li|
      if li.job == job
        li.clear_amounts
      end
    end
  end

  def minimum_rate_line_item
    line_items.find { |line_item| line_item.description == Rate::MINIMUM }
  end

  # ENG-8478: Clean up "evil hack" in Invoice#fix_lineitem_percentages
  #
  # PDW evil hack
  #
  # Calculate the net value of the percentage line items as a percentage of
  # the total of non-percentage line items.
  def fix_lineitem_percentages
    total = total_amount_without_percentage_items
    line_items.each do |item|
      if item.percentage? && item.deleted_at.nil? && !item.marked_for_destruction?
        item.set_percentage_net_amount!(total)
      end
    end
  end

  #
  # Build line_items to this invoice from a
  # hash representation. *It does not persist the line items.*
  #
  def build_lineitem(new_line_item_hash, job, rate)
    # Step 1: Look to see if this lineitem already exists
    existing_lineitem = find_lineitem_by_job_and_description(job, new_line_item_hash[:description])

    # Step 2: If it exists and it's estimated, update it
    if existing_lineitem && existing_lineitem.estimated
      new_line_item_hash[:rate] = rate
      existing_lineitem.assign_attributes(new_line_item_hash)
      log :debug, "Invoice#update_model updated existing lineitem ID #{existing_lineitem.id} with description #{existing_lineitem.description}", job: job.id

    # Step 3: Or, if it's new line item, create it
    elsif !existing_lineitem
      new_line_item_hash[:rate] = rate
      new_line_item_hash[:job] = job
      line_items.build(new_line_item_hash)
      log :debug, "Invoice#update_model created new lineitem with description #{new_line_item_hash[:description]}", job: job.id
    end
  end

  # ENG-5264: PartnerChangeRecipientOnInvoice is setting recipient and recipient company
  # on the invoice and not saving it, so before we can reload the invoice we need to
  # hold onto these 2 variables and reapply them after reloading so they're not lost
  def reload_safely
    cached_recipient = recipient
    cached_recipient_company = recipient_company

    reload

    self.recipient = cached_recipient
    self.recipient_company = cached_recipient_company
  end

  # Return the rate types that are available for this invoice.
  #
  # If invoice.job is from Tesla, it returns Tesla Rate + the Rate that is already set in this invoice.
  # Otherwise, it returns RateType.standard_items.
  def rate_types
    if job.fleet_company.is_tesla?
      [RateType.tesla_rate, RateType.find_by(type: job.get_rate.type)].compact.uniq
    else
      RateType.standard_items
    end
  end

  # Return the class types that are available for this invoice.
  #
  # Make sure that invoice.job.invoice_vehicle_category is also inserted in the result in case it exists.
  def class_types
    class_type_ids = sender.vehicle_categories.pluck(:id)

    if job.invoice_vehicle_category_id
      class_type_ids << job.invoice_vehicle_category_id
    end

    VehicleCategory.where(id: class_type_ids)
  end

  #
  # Return the addons that are available to be added to this invoice.
  # We use the 'available' prefix to emphasize this is about the addons
  # that are still available to be added to this Invoice, and not the ones
  # that might have been added to it already.
  #
  # It will include:
  #
  # - the invoice.sender addons.
  # - the invoice.recipient_company addons if the sender is a Partner
  #   and the recipient_company is not a MotorClub.
  # - the 'Custom Item' addon when 'Invoice Custom Items' is turned on
  #
  # It excludes the addons that correspond to current line items as they shouldn't be available.
  #
  # This logic was inspired in web FE code.
  def available_addons
    addon_ids = []

    case recipient_company
    when FleetCompany # when FleetCompany, add sender addons
      addon_ids = sender.addons.pluck(:id)

      if !recipient_company.motor_club?
        addon_ids << recipient_company.addons.pluck(:id) # and if not motor_club, also add FleetCompany addons
      end
    when SuperCompany # when Swoop, use only Swoop addons
      addon_ids = recipient_company.addons.pluck(:id)
    else # use all sender addons
      addon_ids = sender.addons.pluck(:id)
    end

    if sender.rescue? && sender.has_feature(Feature::INVOICE_CUSTOM_ITEMS) && !recipient_company&.is_tesla? && !recipient_company&.is_swoop?
      # it always goes to the beginning of the array
      addon_ids.unshift ServiceCode.live_addons.find_by(name: ServiceCode::CUSTOM_ITEM).id
    end

    addon_ids = addon_ids.flatten.uniq.without(*line_items_corresponding_addons.select(:id).map(&:id))

    ServiceCode.live_addons.where(id: addon_ids).order(:name)
  end

  #
  # Return addons line_items that are available to be added to this Invoice.
  # It's mainly used to build the Addition Items list in the Invoice Form.
  #
  # We use the 'available' prefix to emphasize this is about the line_items addons
  # that are still available to be added to this Invoice, and not the ones that might have
  # been added to it already.
  #
  def available_additional_line_items
    available_addons.map { |addon| Rate.build_additional_line_item_for(invoice: self, addon: addon) }
  end

  class AutoApprovalLimitError < StandardError; end
  def auto_approval_limit
    case job&.status
    when Job::STATUS_GOA
      25.0
    when Job::STATUS_COMPLETED, Job::STATUS_RELEASED
      100.0
    else
      raise AutoApprovalLimitError, "JOB#{id}: auto_approval_limit fell through with status=#{job&.status}"
    end
  end

  private

  # Current total amount of non-percentage line items in the ledger item.
  # This value is used to calculate the net amount for the percentage items.
  def total_amount_without_percentage_items
    total = 0.0
    line_items.each do |item|
      next if item.deleted_at || item.marked_for_destruction?
      next if item.percentage? || item.tax? || item.storage? || item.net_amount.nil?
      total += item.net_amount
    end
    total
  end

  #
  # Log a change in AASM state as a new AuditInvoiceStatus record
  #
  def audit_status_change
    if aasm.from_state != aasm.to_state
      audit_invoice_statuses.build(
        from_invoice_status: aasm.from_state,
        to_invoice_status: aasm.to_state,
        total_amount_before_state_change: total_amount
      )
    end
  end

  # Set the default billing type for fleet managed jobs. This value can be overridden by
  # agents entering billing info during job assignment.
  def set_default_billing_type
    if job.fleet_managed_job? && sender.is_a?(RescueCompany)
      self.billing_type = sender.billing_type
    end
  end

  # service_code names turn into line_item descriptions when
  # they are added as line_item addons
  def line_items_corresponding_addons
    line_items_descriptions = line_items.map(&:description)

    ServiceCode.live_addons.where(name: line_items_descriptions)
  end

  def derived_rate_type
    line_items.detect { |item| !item.storage? && item.deleted_at.nil? && !item.marked_for_destruction? }&.rate_type
  end

  def track_metrics
    Metrics::Service.track(job,
                           'job',
                           'partner_invoice',
                           total_amount)
  end

end
