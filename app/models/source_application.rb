# frozen_string_literal: true

# This model represents a source application that is being used to access
# the Swoop application. This could be our own web site or apps or a third
# party application or integration via an API.
class SourceApplication < ApplicationRecord

  PLATFORM_WEB = "Web"
  PLATFORM_IOS = "iOS"
  PLATFORM_ANDROID = "Android"
  PLATFORM_SMS = "SMS"
  PLATFORM_CONSUMER_MOBILE_WEB = "CMW"
  PLATFORM_CLIENT_API = "Client API"
  PLATFORM_PARTNER_API = "Partner API"
  PLATFORM_ISSC_API = "ISSC API"
  PLATFORM_RSC_API = "RSC API"
  PLATFORM_GPS = "GPS"

  PLATFORMS = [
    PLATFORM_WEB,
    PLATFORM_IOS,
    PLATFORM_ANDROID,
    PLATFORM_SMS,
    PLATFORM_CONSUMER_MOBILE_WEB,
    PLATFORM_CLIENT_API,
    PLATFORM_PARTNER_API,
    PLATFORM_ISSC_API,
    PLATFORM_RSC_API,
    PLATFORM_GPS,
  ].freeze

  SOURCE_SWOOP = "Swoop"

  # Pattern to match the Swoop mobile app user agent
  MOBILE_APP_MATCHER = /(?<platform>ios|android).+Swoop\/(?<version>\d+(\.\d+)+)( \((?<build>\h+)\))?/i.freeze

  belongs_to :oauth_application, class_name: "Doorkeeper::Application", optional: true

  validates :platform, presence: true, inclusion: PLATFORMS
  validates :source, presence: true
  validates :version, absence: { unless: :mobile_app? }
  validates :oauth_application_id, absence: { unless: :api? }

  before_validation :sync_oauth_application_name, if: -> { oauth_application_id.present? }

  class << self

    # Return the application matching the parameters or create it if it doesn't
    # already exist. Platform and one of source or oauth_application_id are required.
    def instance(platform:, source: nil, version: nil, oauth_application_id: nil)
      if source.blank? && oauth_application_id.blank?
        raise ArgumentError, "Source application cannot be instatiated without source or oauth_application_id"
      end

      first_time = true
      application = nil

      begin
        attributes = { platform: platform, version: version }
        if oauth_application_id.present?
          attributes[:oauth_application_id] = oauth_application_id
        else
          attributes[:source] = source
        end

        application = find_by(attributes)

        unless application
          # Use the OAuth application name as the source if one was specified
          if oauth_application_id.present?
            attributes[:source] = Doorkeeper::Application.find(oauth_application_id).name
          end
          application = create!(attributes)
        end
      rescue ActiveRecord::RecordNotUnique => e
        # This error can be raised on a race condition so retry loading the record.
        # It should be there, but adding a check just in case to avoid any possibility
        # of an infinite loop.
        if first_time
          first_time = false
          retry
        else
          raise e
        end
      end

      application
    end

    # Parse an HTTP request for source application attributes. This method
    # will detect client and partner API requests, mobile app requests, and
    # web requests. The default is that any authenticated request will be
    # considered a Swoop web application request.
    def from_request(request)
      authenticator = SwoopAuthenticator.for(request)
      access_token = authenticator.access_token
      return nil unless access_token

      platform = nil
      source = nil
      version = nil
      oauth_application_id = nil

      if access_token.application_id
        # Set the application based on the token application
        oauth_application_id = access_token.application_id
        if authenticator.client_application?
          platform = PLATFORM_CLIENT_API
        elsif authenticator.partner_application?
          platform = PLATFORM_PARTNER_API
        end
      else
        # Otherwise it's either mobile app or a web request.
        mobile_app = request.user_agent.to_s.match(MOBILE_APP_MATCHER)
        if mobile_app
          mobile_platform = mobile_app["platform"].downcase
          if mobile_platform == "ios"
            platform = PLATFORM_IOS
          elsif mobile_platform == "android"
            platform = PLATFORM_ANDROID
          end
          source = SOURCE_SWOOP
          version = mobile_app["version"]
        else
          platform = PLATFORM_WEB
          source = SOURCE_SWOOP
        end
      end

      if platform
        instance(platform: platform, source: source, version: version, oauth_application_id: oauth_application_id)
      else
        nil
      end
    end

    # SourceApplication to be used when a customer interacts via SMS.
    def sms_application
      instance(platform: PLATFORM_SMS, source: SOURCE_SWOOP)
    end

    # SourceApplication to be used when a customer interacts via a consumer mobile web page.
    def consumer_mobile_web
      instance(platform: PLATFORM_CONSUMER_MOBILE_WEB, source: SOURCE_SWOOP)
    end

    # SourceApplication to be used for ISSC intitiated requests.
    def issc_application
      instance(platform: PLATFORM_ISSC_API, source: "ISSC")
    end

    # SourceApplication to be used for RSC initiated requests.
    def rsc_application
      instance(platform: PLATFORM_RSC_API, source: "Agero")
    end

    # SourceApplication to be used when initiating a GPS change
    def gps_application
      instance(platform: PLATFORM_GPS, source: SOURCE_SWOOP)
    end

  end

  def mobile_app?
    platform == PLATFORM_IOS || platform == PLATFORM_ANDROID
  end

  def api?
    platform == PLATFORM_CLIENT_API || platform == PLATFORM_PARTNER_API
  end

  def client_api?
    platform == PLATFORM_CLIENT_API
  end

  def sms?
    platform == PLATFORM_SMS
  end

  private

  def sync_oauth_application_name
    self.source = oauth_application.name if oauth_application
  end

end
