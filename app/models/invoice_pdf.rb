# frozen_string_literal: true

class InvoicePdf < BaseModel

  belongs_to :invoice

  def populate_public_url
    self.public_url = s3_public_url
  end

  def s3_object
    s3_bucket.object(s3_object_key)
  end

  def s3_public_url
    s3_object.public_url.to_s
  end

  def url_for(read_or_write, response_options = {})
    s3_object.presigned_url(:get, response_options.merge(expires_in: S3_SIGNED_URL_EXPIRATION))
  end

  private

  def s3_bucket_name
    Configuration.s3.invoice_bucket
  end

  def s3_bucket
    S3.bucket(s3_bucket_name)
  end

  def s3_object_key
    "#{invoice.job_id}-#{invoice.uuid}-#{id}.pdf"
  end

end
