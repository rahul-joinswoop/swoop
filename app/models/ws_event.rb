# frozen_string_literal: true

class WsEvent

  attr_reader :key, :score, :json

  def self.create(target_type:, target_id:, user_id:, request:, msg:)
    created_at = Time.zone.now
    score = WsEventLog.generate_score(created_at)

    key = WsEventLog.generate_key({
      target_type: target_type,
      target_id: target_id,
    })

    id = "#{key}-#{SecureRandom.hex}"

    linked_msg = msg.dup
    linked_msg[:ws_event_created_at] = created_at
    linked_msg[:ws_event_id] = id

    json = linked_msg.to_json

    new(
      id: id,
      key: key,
      score: score,
      created_at: created_at,
      target_type: target_type,
      target_id: target_id,
      user_id: user_id,
      request: request,
      json: json
    )
  end

  def initialize(
    id:,
    key:,
    score:,
    created_at:,
    target_type:,
    target_id:,
    user_id:,
    request:,
    json:
  )
    @id = id
    @key = key
    @score = score
    @created_at = created_at
    @target_type = target_type
    @target_id = target_id
    @user_id = user_id
    @request = request
    @json = json
  end

  def to_json
    {
      id: @id,
      key: @key,
      score: @score,
      created_at: @created_at,
      target_type: @target_type,
      target_id: @target_id,
      user_id: @user_id,
      request: @request,
      json: @json,
    }.to_json
  end

end
