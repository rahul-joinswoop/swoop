# frozen_string_literal: true

class FlatRate < Rate

  validates :flat, presence: true

  FLAT_RATE = "Flat Rate"

  def mail_attributes
    base = super
    base.merge({ FLAT_RATE => flat })
  end

  def line_item_descriptions
    [TAX, FLAT_RATE]
  end

  def calculate_line_items(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    lis = []
    lis << {
      description: FLAT_RATE,
      net_amount: flat,
      tax_amount: 0,
      estimated: true,
      unit_price: flat,
      original_unit_price: flat,
      quantity: 1,
      original_quantity: 1,
      version: Invoice::VERSION,
    }
    add_common_items(invoicable, lis)
    lis
  end

end
