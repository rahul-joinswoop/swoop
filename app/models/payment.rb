# frozen_string_literal: true

class Payment < InvoicingLedgerItem

  include ValidateCurrency

  PAYMENT = 'Payment'
  DISCOUNT = 'Discount'
  REFUND = 'Refund'
  WRITE_OFF = 'Write Off'
  VCC_PAYMENT = 'VCC Payment'

  belongs_to :invoice
  has_one :charge, class_name: 'InvoicePaymentCharge'

  validates :total_amount, numericality: { less_than_or_equal_to: 0 }
  validate_currency_matches :invoice

  PAYMENT_TYPES_HUMAN_TO_CLASS = {
    PAYMENT => Payment,
    DISCOUNT => Discount,
    WRITE_OFF => WriteOff,
    REFUND => Refund,
  }.freeze

  def display_name
    'Payment'
  end

end
