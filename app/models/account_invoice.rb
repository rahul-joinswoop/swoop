# frozen_string_literal: true

class AccountInvoice < Invoice

  belongs_to :recipient, class_name: 'Account'

end
