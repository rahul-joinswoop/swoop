# frozen_string_literal: true

class Setting < BaseModel

  belongs_to :company

  AUCTION_MAX_BIDDERS = 'Auction Max Bidders'
  AUCTION_MAX_DISTANCE = 'Auction Max Candidate Distance'
  AUCTION_MAX_ETA = 'Auction Max Candidate ETA'
  BILLING_TYPE = 'Billing Type'
  DISABLE_JOB_ALERTS = 'Disable Job Alerts'
  GEOFENCE_PROGRESS_STATUS = 'auto_progress_status'

  # PCC
  PCC_SERVICE_NAME = 'PCC service name'

  # Policy Lookup Service
  POLICY_LOOKUP_SERVICE_CLIENT_CODE = 'Policy Lookup Service client code'
  POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE = 'Policy Lookup Service fields required for Coverage'
  POLICY_LOOKUP_SERVICE_LOOKUP_TYPES = 'Policy Lookup Service lookup types'

  RUN_COVERAGE_ON_NEW_JOB_FORM_LOAD = 'Run Coverage on New Job Form load'

  # this is the mapping from the input params in companies_controller
  # to the constants above. if you add an entry there it has to be
  # added here too
  SETTINGS_MAP = {
    auction_max_bidders: AUCTION_MAX_BIDDERS,
    auction_max_distance: AUCTION_MAX_DISTANCE,
    auction_max_eta: AUCTION_MAX_ETA,
    disable_job_alerts: DISABLE_JOB_ALERTS,
    billing_type: BILLING_TYPE,
  }.freeze

  validates :key, uniqueness: { scope: :company_id }

  def self.get_setting(company, key)
    company.setting(key)
  end

  def self.get_integer(company, key)
    company.setting(key, :integer)
  end

end
