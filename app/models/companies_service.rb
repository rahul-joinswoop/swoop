# frozen_string_literal: true

class CompaniesService < ApplicationRecord

  belongs_to :company
  belongs_to :service_code

  def company_service_questions
    CompanyServiceQuestion.where(service_code: service_code, company: company)
  end

  def questions
    Question.joins(:company_service_questions)
      .where(company_service_questions: { company: company, service_code: service_code })
      .order(:order_num)
  end

end
