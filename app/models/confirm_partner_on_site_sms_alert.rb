# frozen_string_literal: true

class ConfirmPartnerOnSiteSmsAlert < SmsAlert

  VALID_FROM_STATES = [Job::STATUS_DISPATCHED, Job::STATUS_ENROUTE].freeze

end
