# frozen_string_literal: true

class SubscriptionStatus < BaseModel

  has_one :company

end
