# frozen_string_literal: true

#
# This class represents a target's (e.g. user, job) inclusion in an experiment with the associated variant
#
class TargetVariant < ApplicationRecord

  belongs_to :target, polymorphic: true
  belongs_to :variant

end
