# frozen_string_literal: true

#
# Represents a policy program for the given client company, used for
# Coverage processing. A Policy (PCC::Policy) can have multiple programs.
#
# The Coverage flow can be triggered by the UI on the Job Form,
# for client companies that have the respective sections of the flow
# enabled by company features (Job Form Policy Lookup and Job Form Coverage).
#
class ClientProgram < ApplicationRecord

  belongs_to :company

  has_many :service_notes, class_name: 'ClientProgramServiceNotes', dependent: :destroy

  validates :code, uniqueness: { scope: :company_id }

  MANUAL_ENTRY = 'Manual Entry'

  CLIENT_API_COVERED = 'Client API Covered'
  CLIENT_API_COVERED_CODE = 'client_api_covered'
  CLIENT_API_COVERED_NOTES = 'Job created through Client App and set as Covered automatically'

  COVERED_FILENAME = 'covered'

  COVERED = 'Covered'

  def self.manual_program(company_id)
    ClientProgram.where(name: 'Manual Entry', company_id: company_id).or(ClientProgram.where(code: 'manual_entry', company_id: company_id)).first
  end

  #
  # Program to be used by the Client API Covered flow,
  # in which case the expected result is Covered automatically.
  #
  # It will create one in case it does not exist for the given company_id.
  #
  def self.fetch_or_create_client_api_covered!(company_id)
    where(
      client_identifier: CLIENT_API_COVERED_CODE,
      code: CLIENT_API_COVERED_CODE,
      company_id: company_id,
      coverage_filename: COVERED_FILENAME,
      coverage_notes: CLIENT_API_COVERED_NOTES,
      name: CLIENT_API_COVERED,
    ).first_or_create!
  end

  #
  # Filter and select the right coverage notes, falling back to the client_program.coverage_notes
  # if no custom notes is found.
  #
  # It currently supports the following filters:
  # - :service_code - it will look for it in the service_notes association
  #
  # Ideia here is to be able to add more filters as needed. (e.g. vehicle make, or customer address)
  #
  # e.g. if we needed a filter by vehicle.make
  #
  # coverage_notes_by_filter_or_default(
  #   service_code: @job.service_code, vehicle_make: @job.driver.vehicle.make
  # )
  #
  # ...and maybe also by pickup location state:
  #
  # coverage_notes_by_filter_or_default(
  #   service_code: @job.service_code, vehicle_make: @job.driver.vehicle.make, pickup_location: @job.service_location.state
  # )
  #
  # and so on.
  #
  # For 21st Century, there are notes that depend on details of the coverage result, so it could be something like:
  #
  # coverage_notes_by_filter_or_default(
  #   service_code: @job.service_code, coverage_result: :service_limit_exceeded
  # )
  #
  # Gerenally speaking, we will probably need to come up with some sort of hierachical priorization
  # logic to look up for the notes in a generic and unique way to all Client companies.
  #
  # Something like: try coverage_result_notes, if null try vehicle make, if null try service_notes, if null fallback to client_program.coverage_notes.
  # (opened to ideas)
  #
  # So all filters logic is encapsulated here.
  #
  def coverage_notes_by_filter_or_default(**filters)
    service_notes.find_by(service_code: filters[:service_code])&.notes ||
      coverage_notes
  end

end
