# frozen_string_literal: true

class MilesEnRouteRate < Rate

  validates :miles_enroute, presence: true
  validates :miles_enroute_free, presence: true
  validates :miles_towed, presence: true
  validates :miles_towed_free, presence: true

  def mail_attributes
    base = super

    base.merge(
      {
        HOOKUP => hookup,
        MILES_TOWED => miles_towed,
        FREE_MILES_TOWED => miles_towed_free,
        EN_ROUTE_MILES => miles_enroute,
        FREE_EN_ROUTE_MILES => miles_enroute_free,
      }
    )
  end

  def line_item_descriptions
    [
      TAX,
      HOOKUP,
      EN_ROUTE_MILES,
      EN_ROUTE_KILOMETERS,
      FREE_EN_ROUTE_MILES,
      FREE_EN_ROUTE_KILOMETERS,
      MILES_TOWED,
      KILOMETERS_TOWED,
      FREE_MILES_TOWED,
      FREE_KILOMETERS_TOWED,
    ]
  end

  def calculate_line_items(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    lis = []
    lis << base_hookup
    lis.concat(base_en_route(invoicable))
    lis.concat(base_tow(invoicable))
    add_common_items(invoicable, lis)
    adjust_mileage_line_items(invoicable, lis)
    lis
  end

end
