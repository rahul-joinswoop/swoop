# frozen_string_literal: true

class ReviewFollowupAuditSms < AuditSms

  def redirect_url
    "#{ENV['SITE_URL']}"
  end

end
