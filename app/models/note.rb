# frozen_string_literal: true

class Note < ApplicationRecord

  belongs_to :job
  belongs_to :company
  belongs_to :user

  after_commit do
    Subscribable.trigger_job_updated job
  end

end
