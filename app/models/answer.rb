# frozen_string_literal: true

class Answer < ApplicationRecord

  include IdentityCache
  include AnswerDefinitions

  belongs_to :question

  DEFAULT_ANSWERS_PER_QUESTION = {
    QuestionDefinitions::KEYS_PRESENT => [YES, NO],
    QuestionDefinitions::CUSTOMER_WITH_VEHICLE => [YES, NO],
    QuestionDefinitions::CUSTOMER_IN_A_SAFE_LOCATION => [YES, NO],
    QuestionDefinitions::LOW_CLEARANCE => [YES, NO],
    QuestionDefinitions::WILL_CUSTOMER_RIDE => [
      NO,
      TOW_TRUCK_YES_1,
      TOW_TRUCK_YES_2,
      TOW_TRUCK_YES_MULTIPLE,
    ],
    QuestionDefinitions::VEHICLE_STOP_WHEN_RUNNING => [YES, NO],
    QuestionDefinitions::ANYONE_ATTEMPTED_TO_JUMP => [YES, NO],
    QuestionDefinitions::WHICH_TIRE => [
      DAMAGED_TIRE_FRONT_DRIVER,
      DAMAGED_TIRE_FRONT_PASSENGER,
      DAMAGED_TIRE_REAR_DRIVER,
      DAMAGED_TIRE_REAR_PASSENGER,
      DAMAGED_TIRE_MULTIPLE,
    ],
    QuestionDefinitions::HAVE_A_SPARE => [YES, NO],
    QuestionDefinitions::IS_THE_VEHICLE_ON => [YES, NO],
    QuestionDefinitions::WHERE_ARE_THE_KEYS => [
      KEYS_TRUNK_INACCESSIBLE,
      KEYS_TRUNK_ACCESSIBLE,
      KEYS_INSIDE_VEHICLE,
      KEYS_VISOR,
      KEYS_FLOOR_MAT,
      UNKNOWN,
    ],
    QuestionDefinitions::CHILDREN_OR_PETS => [YES, NO],
    QuestionDefinitions::GASOLINE_TYPE => [
      GAS_REGULAR,
      GAS_PLUS,
      GAS_PREMIUM,
      GAS_DIESEL,
    ],
    QuestionDefinitions::CUSTOMER_BEEN_CONTACTED => [YES, NO],
    QuestionDefinitions::VEHICLE_MOVED_TODAY => [YES, NO],
    QuestionDefinitions::GPS_TRACKING => [YES, NO],
    QuestionDefinitions::VEHICLE_CAN_BE_NEUTRAL => [YES, NO, UNKNOWN],
    QuestionDefinitions::VEHICLE_DRIVEABLE_AFTER_WINCH_OUT => [YES, NO],
    QuestionDefinitions::VEHICLE_4WD => [YES, NO, UNKNOWN],
    QuestionDefinitions::WITHIN_10_FEET_OF_PAVED_SURFACE => [YES, NO, UNKNOWN],
  }.freeze

  # list of all questions that require the :answer_info attribute (currently only
  # enforced in graphql api). this exists here so that we can also provide whether
  # or not a particular answer requires the extra field but we also delegate calls
  # to it from QuestionResult as part of a future validation.
  NEEDS_INFO = {
    QuestionDefinitions::LOW_CLEARANCE => [YES],
  }.freeze

  def needs_info?
    NEEDS_INFO.dig(question.question).to_a.include?(answer)
  end

  def no?
    answer == NO
  end

  def yes?
    !no?
  end

end
