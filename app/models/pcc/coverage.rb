# frozen_string_literal: true

class PCC::Coverage < ApplicationRecord

  COVERED_RESULT = "Covered"
  NOT_COVERED_RESULT = "NotCovered"
  PARTIAL_COVERAGE_RESULT = "PartiallyCovered"

  belongs_to :pcc_policy, class_name: 'PCC::Policy', foreign_key: :pcc_policy_id
  belongs_to :pcc_vehicle, class_name: 'PCC::Vehicle', foreign_key: :pcc_vehicle_id
  belongs_to :pcc_driver, class_name: 'PCC::User', foreign_key: :pcc_driver_id
  belongs_to :client_program
  has_many :jobs, foreign_key: :pcc_coverage_id

end
