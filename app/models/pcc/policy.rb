# frozen_string_literal: true

class PCC::Policy < ApplicationRecord

  belongs_to :company
  belongs_to :job

  has_many :drivers, class_name: 'PCC::User', foreign_key: :pcc_policy_id, inverse_of: :pcc_policy
  has_many :vehicles, class_name: 'PCC::Vehicle', foreign_key: :pcc_policy_id, inverse_of: :pcc_policy

  # available lookup fields for Job form
  MANUAL = 'Skip and enter manually'
  NAME_AND_ZIP = 'Last Name & Zip'
  PHONE_NUMBER = 'Phone Number'
  POLICY_NUMBER = 'Policy Number'
  UNIT_NUMBER = 'Unit Number'
  VIN = 'Last 8 Vin'

end
