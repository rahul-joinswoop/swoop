# frozen_string_literal: true

class PCC::Claim < ApplicationRecord

  belongs_to :company

  belongs_to :pcc_policy,   class_name: 'PCC::Policy',   foreign_key: :pcc_policy_id
  belongs_to :pcc_vehicle,  class_name: 'PCC::Vehicle',  foreign_key: :pcc_vehicle_id
  belongs_to :pcc_driver,   class_name: 'PCC::User',     foreign_key: :pcc_driver_id
  belongs_to :pcc_coverage, class_name: 'PCC::Coverage', foreign_key: :pcc_coverage_id

end
