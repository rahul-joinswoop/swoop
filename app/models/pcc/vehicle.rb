# frozen_string_literal: true

class PCC::Vehicle < ApplicationRecord

  belongs_to :pcc_policy, class_name: 'PCC::Policy'

  has_many :coverages, class_name: 'PCC::Coverage', foreign_key: :pcc_vehicle_id

end
