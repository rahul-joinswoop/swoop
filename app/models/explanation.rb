# frozen_string_literal: true

class Explanation < BaseModel

  delegate :issue, to: :reason

  belongs_to :job
  belongs_to :reason

  validates :job, presence: true
  validates :reason, presence: true

  def listeners
    if Company.swoop.has_feature(Feature::ISSUES)
      return job.listeners
    end
    Set.new
  end

  def render_to_hash
    controller = ActionController::Base.new
    opts = { locals: { explanation: self } }
    template_path = "api/v1/explanations/_explanation"
    json_str = controller.render_to_string template_path, opts
    JSON.parse(json_str)
  end

end
