# frozen_string_literal: true

class Referer < ApplicationRecord

  include SoftDelete

  # Currently used referers.
  NAMES = [
    'Advertisement',
    'Agero',
    'App Store',
    'Facebook',
    'Geico',
    'Instagram',
    'Motor Club',
    'Other',
    'Press / News',
    'Referral',
    'Swoop Dispatch',
    'Swoop Email',
    'Web Search',
  ].freeze

  def self.init_all
    NAMES.each { |name| Referer.find_or_create_by(name: name) }
  end

end
