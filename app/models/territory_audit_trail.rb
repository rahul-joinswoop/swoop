# frozen_string_literal: true

# It keeps record of the added and removed territories
class TerritoryAuditTrail < ApplicationRecord

  belongs_to :user
  belongs_to :territory

  validates :user, presence: true
  validates :territory, presence: true
  validates :action, presence: true,
                     inclusion: { in: %w(created destroyed) }

end
