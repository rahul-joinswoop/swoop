# frozen_string_literal: true

class ModelType < ApplicationRecord

  include IdentityCache

  IGNORE_CLASSES = [ActiveRecord::SchemaMigration].freeze
  IGNORE_MODULES = [USGeo].freeze

  cache_index :name, unique: true

  class ModelTypeMissingError < StandardError; end

  class << self

    def find_all
      Rails.application.eager_load! unless Rails.application.config.eager_load
      ActiveRecord::Base.descendants
        .reject(&:abstract_class?)
        .reject { |klass| IGNORE_CLASSES.include?(klass) }
        .reject { |klass| IGNORE_MODULES.include?(klass.parent) }
    end

    def import_all!
      find_all.each { |klass| find_or_create_by!(name: klass.name) }
    end

    def check!
      missing = []
      find_all.each do |klass|
        find_by!(name: klass.name)
      rescue
        missing << klass.name
      end
      if missing.present?
        msg = [
          "Missing ModelType data migrations for:\n",
          *missing,
          "\nPlease run 'rails g seed_migration update_model_types' with the following content:",
          <<~SEED,

            class UpdateModelTypes < SeedMigration::Migration
              def up
                ModelType.import_all!
              end
              def down
              end
            end
          SEED
        ].join("\n")

        raise ModelTypeMissingError, msg
      end
    end

  end

end
