# frozen_string_literal: true

class Vehicle < BaseModel

  include SoftDelete

  self.ignored_columns = %w(_x_name)

  has_many :drive, -> { order('created_at DESC') }, inverse_of: :vehicle
  has_many :users, through: :drive
  belongs_to :company
  belongs_to :vehicle_category
  has_many :jobs, class_name: "Job", foreign_key: "rescue_vehicle_id", inverse_of: :rescue_vehicle
  # TODO - what are these statuses? very similar to Job::fleet_show_rescue_vehicle_states and
  # job.enroute_or_later but neither includes Job::DISPATCHED
  has_many :current_jobs, -> { where status: [Job::DISPATCHED, Job::ENROUTE, Job::ONSITE, Job::TOWING, Job::TOWDESTINATION] }, class_name: "Job", foreign_key: "rescue_vehicle_id", inverse_of: :rescue_vehicle
  has_one :driver, class_name: "User" # rescue vehicle driver

  validates_uniqueness_of :name, scope: [:company_id, :type], conditions: -> { not_deleted }, if: -> { name.present? && company_id.present? && not_deleted? }

  # these are the attributes updated by UpdateVehicleLocationWorker
  LOCATION_UPDATE_WORKER_ATTRS = ["lat", "lng", "location_updated_at", "updated_at"].freeze

  after_commit do
    if Set.new(previous_changes.keys) != Set.new(LOCATION_UPDATE_WORKER_ATTRS)
      # don't send subscription updates for a job when the only changes are from UpdateVehicleLocationWorker
      Subscribable.trigger_job_updated jobs
      Subscribable.trigger_user_updated users
    end
  end

  def copy_vehicle(extra_attributes = nil)
    copy_attributes = [
      'make',
      'model',
      'vehicle_type',
      'serial_number',
      'color',
      'vin',
      'license',
      'company_id',
      'company_type',
      'description',
      'year',
      'type',
    ]
    copy = self.class.new
    copy_attributes.each do |attr|
      copy[attr] = self[attr]
    end
    copy
  end

  def description
    "#{make} #{model} #{year}, #{color}"
  end

  def last_driver
    drive.first
  end

  def driver_description
    driver = last_driver
    if driver
      driver.description
    else
      description
    end
  end

  # Subclasses can override if vehicle location is being tracked.
  def current_location
    nil
  end

  # vehicle moved in last 12 hrs?
  def active?
    loc = current_location
    loc.present? && loc.timestamp >= 12.hours.ago
  end
  alias_method :active, :active?

  # Returns true if the vehicle is actively sending location updates
  def live_updates?
    loc = current_location
    loc.present? && loc.timestamp >= 1.hour.ago
  end

  def listeners
    ret = Set.new

    current_jobs
      .includes(:fleet_company, :rescue_company, bids: [:company])
      .each { |job| ret.merge job.listeners }

    if company
      ret << company
    end
    ret
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/vehicles/_vehicle", locals: { vehicle: self }))
  end

  def vin_indexed
    vin&.strip&.downcase
  end

  def vin_reverse_indexed
    vin_indexed&.reverse
  end

  def latlng
    "#{current_location&.lat},#{current_location&.lng}"
  end

  def has_coords?
    current_location.present?
  end

end
