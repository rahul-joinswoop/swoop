# frozen_string_literal: true

class VehicleType < ApplicationRecord

  has_many :vehicle_models

  scope :with_no_associated_vehicle_models, -> do
    includes(:vehicle_models).where(vehicle_models: { id: nil })
  end

  validates :name, presence: true, uniqueness: true

end
