# frozen_string_literal: true

class Variant < ApplicationRecord

  belongs_to :experiment
  belongs_to :job
  belongs_to :user

end
