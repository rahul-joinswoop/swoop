# frozen_string_literal: true

require 'slack'

# This model is used to subscribe Slack channels to tags. Various actions within
# a Job life cycle will publish tags based on the client company name and action.
# The subscriptions will fuzzy match on the tags and send messages to the Slack
# channels wherever they match.
class SlackChannel < ApplicationRecord

  CLIENT = Slack::Client.new token: ENV['SLACK_API_TOKEN']
  DEFAULT_TAG = 'default'

  scope :for_tags, ->(tags) { tags_query(tags) }
  default_scope    -> { where(active: true) }

  validates :tag, uniqueness: { scope: :channel, if: :active? }

  def message(msg)
    result = {}
    args = { text: msg, channel: channel, username: bot_name, as_user: false }
    enable_wrapper(args) do
      logger.debug "SLACK sending to API #{args}"
      result = CLIENT.chat_postMessage(args)
    end
    result['ok']
  end
  handle_asynchronously :message

  def enable_wrapper(args)
    if self.class.enabled?
      begin
        yield if block_given?
      rescue => e
        log("SLACK Error sending slack: #{e.message}: args: #{args}", :error)
      end
      return
    end
    log("SLACK Skipping - disabled")
  end

  def log(msg, type = :debug)
    enabled_str = 'STUB ' unless enabled?
    logger.send(type, "#{enabled_str}SLACK(#{bot_name}:#{channel}) #{msg}")
  end

  def enabled?
    self.class.enabled?
  end

  class << self

    def message(msg, tags: [], add_default: true)
      enabled_str = 'STUB ' unless enabled?

      logger.debug "#{enabled_str}SLACK : tags:#{tags}, Default:#{add_default}, msg:"\
                   "\"#{msg}\""

      return true unless enabled?

      tags_and_defaults = add_default_tag(tags, add_default)
      logger.debug("SLACK : Sending tags: #{tags_and_defaults}")
      for_tags(tags_and_defaults).each do |chan|
        logger.debug "SLACK : Queuing for send to #{chan.name}"
        chan.message(msg)
      end
    rescue => e
      logger.error "Unable to send Slack message: #{e.message}"
      Rollbar.error(e)
    end

    def add_default_tag(tgs, cond = true)
      tags = tags_to_array(tgs)

      # Add master tag
      # Aka farmers-insurance-review
      # Also farmers-insurance-dispatch
      # This allows us to break it down further
      # if we dont want dispatch notifications just reviews
      tags = tags.collect { |t| t.to_s.gsub(/\s+/, '-').downcase }
      tags << tags.join('-').downcase

      return tags unless cond

      tags << DEFAULT_TAG unless tags.include?(DEFAULT_TAG)

      tags
    end

    def tags_to_array(tgs)
      tgs.is_a?(Array) ? tgs : [tgs]
    end

    def tags_query(tgs)
      tags = tags_to_array(tgs)

      cond = []
      vals = []
      not_cond = []
      not_vals = []

      tags.each do |t|
        cond     << "? LIKE tag"
        not_cond << "? NOT LIKE excludes"

        vals << t
        not_vals << t
      end

      where(vals.unshift(cond.join(' OR ')))
        .where(not_vals.unshift("excludes IS NULL OR (#{not_cond.join(' AND ')})"))
    end

    def enabled?
      return false if Rails.env.test?
      return false if ENV['SLACK_API_TOKEN'].blank?

      ENV['ENABLE_SLACK'].present?
    end

  end

end
