# frozen_string_literal: true

# Represents a notification setting associated to a user.
#
# It allows users to have their job notifications customized by job status transition,
# notification channel (Call or Text).
#
# For now it's used only by Partner users, but it can be extended.
#
# It relies on Configuration.user_notification_settings, so all available configuration
# is written in user_notification_settings.yaml.
#
class UserNotificationSetting < ApplicationRecord

  validates :user, :notification_code, presence: true
  validates :notification_code, inclusion: { in: Configuration.user_notification_settings.keys, message: 'is not valid' }

  belongs_to :user

  class << self

    # List all available notification_settings formatted for the given company.
    # It relies on Configuration.user_notification_settings.
    #
    # Mostly used by UI, to show the available notification settings in the user form.
    def available_user_notification_settings(company:)
      Configuration.user_notification_settings.to_h.map do |key, setting|
        description_base = description_for_current_locale_or_english(setting)
        description = case key.to_sym
                      when :new_job
                        description_base.gsub("%{company_name}", company.name)
                      else
                        description_base
                      end

        {
          notification_code: key,
          description: description,
          supported_roles: setting[:supported_roles],
          available_notification_channels: setting[:available_notification_channels],
          default_notification_channels: setting[:default_notification_channels],
        }
      end
    end

    def description_for_current_locale_or_english(setting)
      setting.dig(:description, I18n.locale) || setting.dig(:description, :en)
    end

    # Each notification_code has a set of specific roles it gives support to.
    # For instance, new_job notification_code only applies to [:admin, :dispatcher].
    #
    # This method returns the set of roles that the notification_code passed as parameters
    # gives support to.
    def supported_roles_by(notification_code:)
      Configuration.user_notification_settings.to_h.dig(notification_code, "supported_roles")
    end

  end

  def supported_job_status_names
    setting_by_notification_code.dig(:supported_job_status_names)
  end

  def supported_roles
    setting_by_notification_code.dig(:supported_roles)
  end

  private

  def setting_by_notification_code
    @setting_by_notification_code ||= Configuration.user_notification_settings[notification_code] || {}
  end

end
