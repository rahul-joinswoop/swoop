# frozen_string_literal: true

class SuperCompany < Company

  include FleetRescueVehicles
  # since we're a SuperCompany our graphql stats access is for all instead of a specific application_id
  # (but see below for more)
  include GraphQLStats

  has_many :outgoing_invoices, foreign_key: :sender_id, class_name: "Invoice", inverse_of: :sender
  has_many :incoming_invoices, foreign_key: :recipient_company_id, class_name: "Invoice", inverse_of: :recipient_company

  # right now we use a hack to record graphql stats from our mobile app as a fake oauth app with id=-1, so
  # defining this here allows us to access the mobile app's stats via delegation
  IGNORE_ME_I_AM_A_FAKE_MOBILE_APP = Doorkeeper::Application.new(
    id: Utils::Stats::Common::MOBILE_APP_ID,
    name: 'Fake Mobile App'
  ).freeze
  delegate :graphql_requests, :graphql_deprecated_fields, to: :IGNORE_ME_I_AM_A_FAKE_MOBILE_APP, prefix: :mobile_app

  # end_user_invoices is what we call this same query for fleet companies
  alias_method :end_user_invoices, :outgoing_invoices

  def job_update_email
    nil
  end

end
