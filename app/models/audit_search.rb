# frozen_string_literal: true

# Audit power search queries.
#
# TABLE audit_searches
#   original                           string
#   options                            string
#   queries                            string
#   filters                            string
#   created_at                         datetime            NOT NULL
#   updated_at                         datetime            NOT NULL
class AuditSearch < ApplicationRecord
end
