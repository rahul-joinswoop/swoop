# frozen_string_literal: true

class InvoicingLineItem < BaseModel

  include SoftDelete
  include ValidateCurrency

  before_save :default_values

  FLAT = 'flat'
  PERCENT = 'percent'
  CALC_TYPES = [FLAT, PERCENT].freeze

  STORAGE = 'Storage'

  def default_values
    self.net_amount ||= 0
    self.tax_amount ||= 0
  end

  belongs_to :ledger_item, class_name: 'InvoicingLedgerItem'
  belongs_to :job
  belongs_to :rate

  validates :net_amount, numericality: { maximum: 10_000_000, minimum: -10_000_000 }, allow_blank: true
  validates :tax_amount, numericality: { maximum: 1_000_000, minimum: -1_000_000 }, allow_blank: true
  validates :unit_price, numericality: { maximum: 1_000_000, minimum: -1_000_000 }, allow_blank: true
  validates :quantity, numericality: { maximum: 1_000_000, minimum: -1_000_000 }, allow_blank: true
  validates :original_unit_price, numericality: { maximum: 1_000_000, minimum: -1_000_000 }, allow_blank: true
  validates :original_quantity, numericality: { maximum: 1_000_000, minimum: -1_000_000 }, allow_blank: true
  validate_currency_matches :ledger_item

  scope :non_storage, -> { where.not(description: STORAGE) }
  scope :storage, -> { where(description: STORAGE) }
  scope :active_storage, -> { not_deleted.storage.joins(:job).where(jobs: { deleted_at: nil, status: JobStatus::NAMES[:STORED] }) }

  def tax_amount=(value)
    super(fix_trailing_decimal(value))
  end

  def unit_price=(value)
    prev = unit_price
    retval = super(fix_trailing_decimal(value))
    update_net_amount! if prev != retval
    retval
  end

  def quantity=(value)
    prev = quantity
    retval = super(fix_trailing_decimal(value))
    update_net_amount! if prev != retval
    retval
  end

  def net_amount=(value)
    # If we have unit price and quantity on a non-percentage item, then we need to make
    # sure net_amount is equal to that calculation
    if unit_price.nil? || quantity.nil? || percentage?
      super(value)
    else
      if net_amount && value && net_amount.to_f.round(4) == value.to_f.round(4)
        # Ignore rounding errors and keep the existing value loaded from the database
        super(value)
      else
        # Otherwise need to recalculate based on the unit_price and quantity
        calculated_value = (unit_price * quantity).round(4)
        super(calculated_value)
      end
    end
  end

  def original_unit_price=(value)
    super(fix_trailing_decimal(value))
  end

  def original_quantity=(value)
    super(fix_trailing_decimal(value))
  end

  def related_job_id
    ledger_item.job.id
  end

  def tax?
    description == Rate::TAX && net_amount.to_f == 0
  end

  # Clears the amount of the line item to zero. This method changes the amount
  # due on an invoice and must not be called on an non-editable invoice.
  def clear_amounts
    self.quantity = 0
    self.net_amount = 0
    self.tax_amount = 0
  end

  def rate_type
    rate&.type
  end

  #
  # This field accepts nil in the DB, and from what
  # our codebase does, when it's nil, it actually means it's flat.
  #
  # This method will always return flat in case the attr value is nil.
  def calc_type
    return self[:calc_type] if self[:calc_type] == PERCENT
    FLAT
  end

  def percentage?
    calc_type == PERCENT
  end

  # For percentage line items the percentage value is stored in quantity.
  def percentage
    if percentage?
      (quantity || 0.0) * 0.01
    else
      nil
    end
  end

  # This method will set the net_amount based on the fixed price total for a
  # percentage line item For better or worse, the net amount on percentage line
  # items needs to be calculated based on the invoice total which is calculated
  # from all the other fixed price line items.
  def set_percentage_net_amount!(total)
    unless percentage?
      raise NotImplementedError, "Can only call this method on percentage line items"
    end
    self.net_amount = (total * percentage).round(4)
  end

  def include_original_deleted?(deleted)
    # logger.debug "include_original_deleted call with #{deleted}"
    if deleted_at
      if deleted
        if deleted_at && original_unit_price
          true
        else
          false
        end
      else
        false
      end
    else
      true
    end
  end

  def should_estimated_be_set_to_false?
    # PDW this used to exclude tax from being set to estimated, but i believe we want that
    # re-enabled during ENG-4653
    (saved_changes.size > 0) && estimated && ((description != STORAGE) || (original_quantity != quantity))
  end

  def storage?
    description == Rate::STORAGE
  end

  def invoice
    ledger_item if ledger_item.is_a?(Invoice)
  end

  private

  # Sometimes user input appears with a trailing decimal which is not a valid number.
  def fix_trailing_decimal(value)
    if value.is_a?(String)
      value.strip.chomp('.')
    else
      value
    end
  end

  # Keep the net amount in sync with unit price and quantity.
  def update_net_amount!
    # Percentages are calculated differently from the invoice total
    return if percentage?

    if unit_price && quantity
      self.net_amount = (unit_price * quantity).round(4)
    else
      self.net_amount = nil
    end
  end

end
