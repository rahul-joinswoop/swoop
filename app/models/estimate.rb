# frozen_string_literal: true

class Estimate < ApplicationRecord

  include Utils

  MINS_ON_SITE = 20.minutes

  belongs_to :job
  belongs_to :site_location, class_name: "Location"
  belongs_to :service_location, class_name: "Location"
  belongs_to :drop_location, class_name: "Location"

  VERSION = 1
  METERS_MILES = 0.00062137

  # This method calls the Google Distance Matrix Service to calculate distance and time for
  # several known legs of a job. The points used are (A) the location of the site servicing the job
  # (which assumes that's where the truck is coming from); (B) the service location, and (C) the
  # drop location.
  #
  # The measurements calculated are:
  #
  # ab: distance/time from the site to the service location; this measurement takes current traffic into account
  # bc: distance/time from the service location to the drop location
  # ca: distance/time from the drop location back to the site location
  # ba: distance/time from the service location back to the site location
  #
  # Not all measurements are calculated on all jobs. For instance a roadside assistance job would not have
  # a bc measurement.
  def run
    self.version = VERSION
    logger.debug "ESTIMATE(#{job.try(:id)}): Running Site:#{site_location.try(:id)} ServiceLocation:#{service_location.try(:id)} DropLocation:#{drop_location.try(:id)} Start:#{start_at}"

    # The matrix hash will only be filled with the legs that reflect valid measurements
    matrix = {}
    if site_location&.has_coords? && service_location&.has_coords?
      matrix[:ab] = { origin: site_location, destination: service_location, start_at: start_at }
    else
      logger.debug "ESTIMATE(#{job.id}) ab not calculated: #{site_location.inspect} -> #{service_location.inspect}"
    end
    if service_location&.has_coords? && drop_location&.has_coords?
      matrix[:bc] = { origin: service_location, destination: drop_location }
    else
      logger.debug "ESTIMATE(#{job.id}) bc not calculated: #{service_location.inspect} -> #{drop_location.inspect}"
    end
    if drop_location&.has_coords? && site_location&.has_coords?
      matrix[:ca] = { origin: drop_location, destination: site_location }
    else
      logger.debug "ESTIMATE(#{job.id}) ca not calculated: #{drop_location.inspect} -> #{site_location.inspect}"
    end
    if service_location&.has_coords? && site_location&.has_coords?
      matrix[:ba] = { origin: service_location, destination: site_location }
    else
      logger.debug "ESTIMATE(#{job.id}) ba not calculated: #{service_location.inspect} -> #{site_location.inspect}"
    end

    JobError.with_job_errors(job, JobError::GOOGLE_MATRIX_NO_RESULTS) do |job_errors|
      results = External::GoogleDistanceMatrixService.multi(matrix.values)
      # Match up the results with the original keys from the matrix; this is OK
      # because ruby Hashes guarantee that the keys and values will match insertion order.
      matrix.keys.zip(results).each do |leg, result|
        meters, seconds = result.first
        if meters && seconds
          self["meters_#{leg}"] = meters
          self["seconds_#{leg}"] = seconds
        else
          logger.warn "ESTIMATE(#{job.id}) Unable to calculate leg #{leg}"
          self["meters_#{leg}"] = nil
          self["seconds_#{leg}"] = nil
          origin = matrix[leg][:origin]
          destination = matrix[leg][:destination]
          raise JobError::JobException.new("Unable to calculate estimates because no results returned from Google origin:(#{origin.lat},#{origin.lng})#{origin.try(:name)} and destination:(#{destination.lat},#{destination.lng}),#{destination.try(:name)}", true)
        end
      end
    end
  end

  def miles_ab
    if meters_ab
      (meters_ab * METERS_MILES).round(1)
    end
  end

  def miles_bc
    if meters_bc
      (meters_bc * METERS_MILES).round(1)
    end
  end

  def miles_ca
    if meters_ca
      (meters_ca * METERS_MILES).round(1)
    end
  end

  def miles_ba
    if meters_ba
      (meters_ba * METERS_MILES).round(1)
    end
  end

  def mins_ab
    if seconds_ab
      (seconds_ab / 60.0).round
    end
  end

  def mins_bc
    if seconds_bc
      (seconds_bc / 60.0).round
    end
  end

  def mins_ca
    if seconds_ca
      (seconds_ca / 60.0).round
    end
  end

  def mins_ba
    if seconds_ba
      (seconds_ba / 60.0).round
    end
  end

  def mins(leg)
    if leg == :ab
      mins_ab
    elsif leg == :bc
      mins_bc
    elsif leg == :ca
      mins_ca
    elsif leg == :ba
      mins_ba
    end
  end

  def miles_p2p
    if drop_location
      if miles_ab && miles_bc && miles_ca
        miles_ab + miles_bc + miles_ca
      end
    else
      if miles_ab && miles_ba
        miles_ab + miles_ba
      end
    end
  end

  def miles_p2p_description
    if drop_location
      "(#{miles_ab}+#{miles_bc}+#{miles_ca})"
    else
      "(#{miles_ab}+#{miles_ba})"
    end
  end

  #  def hours_p2p
  #    if self.drop_location
  #      hours_ab+hours_bc+hours_ca
  #    else
  #      hours_ab+hours_ba
  #    end
  #  end

  def hours_p2p_description
    if drop_location
      "(#{hours_ab}+#{hours_bc}+#{hours_ca})"
    else
      "#{hours_ab}+#{hours_ba}"
    end
  end

end
