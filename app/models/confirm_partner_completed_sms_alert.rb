# frozen_string_literal: true

class ConfirmPartnerCompletedSmsAlert < SmsAlert

  include LoggableJob
  extend LoggableJob

  VALID_FROM_STATES = [Job::STATUS_DISPATCHED, Job::STATUS_ACCEPTED, Job::STATUS_ENROUTE, Job::STATUS_ONSITE, Job::STATUS_TOWING, Job::STATUS_TOWDESTINATION].freeze

  def self.send_if_necessary(job)
    if job.fleet_company.has_feature(Feature::SMS_CONFIRM_COMPLETE)
      log :debug, "ConfirmPartnerCompletedSmsAlert::send_if_necessary feature enabled", job: job.id
      if job.partner_not_live_or_live_and_after_hours?
        log :debug, "ConfirmPartnerCompletedSmsAlert::send_if_necessary sending", job: job.id
        ConfirmPartnerCompletedSmsAlert.create!(job: job,
                                                send_at: Integer(ENV['SMS_COMPLETED_DELAY'] || 30).minutes.from_now)
      else
        log :debug, "ConfirmPartnerCompletedSmsAlert::send_if_necessary partner not live or after hours", job: job.id
      end
    else
      log :debug, "ConfirmPartnerCompletedSmsAlert::send_if_necessary feature disabled", job: job.id
    end
  end

end
