# frozen_string_literal: true

class SystemAlert < ApplicationRecord

  belongs_to :job

end
