# frozen_string_literal: true

class Image < BaseModel

  belongs_to :job

  delegate :listeners, to: :job

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/images/_image", locals: { image: self }))
  end

end
