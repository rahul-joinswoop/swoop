# frozen_string_literal: true

class NpsPureReview < NpsReview

  def format_nps_question(index)
    nps_questions = [
      "How likely are you to recommend %{company_name} to family, friends, or colleagues? (Reply: 1 - 10, 10 is best)",
      "Anything else we can do to improve your experience?",
    ]
    question = nps_questions[index]

    raise ArgumentError, "Invalid question for #{company.name} Survery: #{index}" if !question

    t = "(Question %{current_question}/%{total_questions}) #{question}"
    if company.permission_to_send_reviews_from
      t = "%{company_name}: " + t
    end
    t % { total_questions: nps_questions.length, current_question: index + 1, company_name: company.name }
  end

  def process_response(twilio_response)
    rating = Review.parse_rating twilio_response.body, max: 10
    index = -1
    if nps_question1.nil?
      if rating != -1
        index = 1
        self.nps_question1 = rating
      end
    elsif nps_feedback.nil?
      index = 2
      self.nps_feedback = twilio_response.body
    end

    set_nps_review(question: index) if (index != -1) && (index != 2)
    save!

    send_to_slack((index == 2), 2)

    send_review_sms if index != 2
  end

end
