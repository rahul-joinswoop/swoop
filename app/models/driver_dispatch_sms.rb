# frozen_string_literal: true

class DriverDispatchSms < AuditSms

  def redirect_url
    "#{ENV['SITE_URL']}"
  end

end
