# frozen_string_literal: true

# Available for a towing company: from the towing company perspective in real
# life, Swoop and Fleet InHouse are types of Accounts. Accounts will also
# include other companies that aren’t using Swoop software like "Bob’s Autobody"
# for example, that will call jobs and recite information over the phone instead
# of sending them digitally to the towing company like Swoop or Fleet InHouse.
class Account < BaseModel

  include SoftDelete
  include RedisConcerns
  include Account::Searchable

  self.ignored_columns = %w(fleet_company_id)

  belongs_to :company
  belongs_to :location
  belongs_to :department
  belongs_to :physical_location, class_name: "Location"
  has_many :jobs, inverse_of: :account
  has_many :rates
  belongs_to :client_company, class_name: "Company"
  has_many :invoices, as: :recipient, inverse_of: :_account_recipient
  validates :name, uniqueness: { scope: [:company_id, :deleted_at] }

  after_commit do
    Subscribable.trigger_job_updated jobs
  end

  # PDW this is required to support fleet companies and partners with the same name but not linked e.g. mobile mini
  # validate :duplicate_fleet_name, if: "client_company_id.nil?"

  CASH_CALL = "Cash Call"

  # Fleet End Customer
  CUSTOMER = "Customer"

  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :physical_location

  after_save :mark_deleted_isscs, if: :saved_change_to_deleted_at?

  after_commit :update_dependent_indexes, on: :update

  def self.find_or_create_cash_account(company)
    Account.find_or_create_by!(name: Account::CASH_CALL, company: company, deleted_at: nil)
  end

  def self.find_or_create_fleet_customer_account(company)
    Account.find_or_create_by!(name: Account::CUSTOMER, company: company, deleted_at: nil)
  end

  def agero_account?
    client_company_id == Company.agero_id
  end

  def rsc_enabled?
    agero_account?
  end

  def fleet_account?
    !client_company_id.nil?
  end

  def duplicate_fleet_name
    if FleetCompany.all.map(&:name).include?(name)
      errors.add(:name, "name must be unique from a fleet")
    end
  end

  def listeners
    ret = Set.new
    ret << company
    ret
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/accounts/_account", locals: { account: self }))
  end

  def invoices_emailable?
    !(get_accounting_email.blank? &&
      accounting_email_cc.blank? &&
      accounting_email_bcc.blank?)
  end

  def get_accounting_email
    if accounting_email
      accounting_email
    else
      if client_company && client_company.accounting_email
        client_company.accounting_email
      end
    end
  end

  def cash_call?
    name == CASH_CALL
  end

  def isscs
    if client_company && client_company.issc_client_id.present?
      company.isscs.where(deleted_at: nil, clientid: client_company.issc_client_id).to_a
    else
      []
    end
  end

  def sort_name
    I18n.transliterate(name.to_s.downcase.strip)
  end

  private

  def mark_deleted_isscs
    if deleted_at.present?
      isscs.each do |issc|
        issc.update_attribute(:delete_pending_at, Time.current)
      end
    end
  end

  # Update search indexes that reference this record.
  def update_dependent_indexes
    if saved_change_to_name?
      SearchIndex::AccountDependentIndexUpdateWorker.perform_async(id)
    end
  end

end
