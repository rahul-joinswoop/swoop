# frozen_string_literal: true

class Territory < ApplicationRecord

  include SoftDelete
  include TerritoryConcerns

  # Circleoid is a Polygon with many sides as almost looks like a circle
  SIDES_FOR_CIRCLEOID = 12

  belongs_to :company

  validates :company, presence: true
  validate :is_company_a_rescue_company?

  validates :polygon_area, presence: true
  validate :does_not_have_interior_ring

  default_scope { not_deleted }
  scope :containing_lat_lng, ->(latitude, longitude) do
    where("ST_Contains(polygon_area, ST_GeomFromText('POINT(#{latitude} #{longitude})'))")
  end

  # Territories that's rescue companies are eligible to do the job based on service location
  scope :overlapping_pickup_location, ->(job) do
    if job.service_location && job.service_location.lat && job.service_location.lng
      joins(:company)
        .where(companies: { type: 'RescueCompany' })
        .containing_lat_lng(job.service_location.lat, job.service_location.lng)
    else
      # If service location for the job isn't available, return empty ActiveRecord_Relation
      none
    end
  end

  # Exterior polygons from the provided MultiPolygon is extracted in this method.
  def self.find_exterior_polygons(multi_polygon)
    # Step 1: Each polygon is from the MultiPolygonImpl is extracted and added to the polygons array.
    polygons = multi_polygon.collect { |polygon| polygon }

    # Step 2: Get all the combinations of the polygons array (All the permutations and combinations).
    polygons.product(polygons).
      # Step 3: get rid of the 1-1 mappings
      reject { |polygon1, polygon2| polygon1 == polygon2 }.
      # Step 4: Get rid of the polygon from the list of polygons if it is contained and is part of the compared polygon.
      reject { |polygon1, polygon2| first_territory_contains_second_territory?(polygon1, polygon2) }.
      # Step 5: Get the exterior ring of the provided polygon
      map { |_, polygon2| polygon2.exterior_ring }.uniq
  end

  def self.first_territory_contains_second_territory?(territory_first, territory_second)
    # rubocop:disable Airbnb/RiskyActiverecordInvocation
    ApplicationRecord.execute_query("SELECT ST_Contains('#{territory_first}', ST_GeomFromText('#{territory_second}'))").first['st_contains']
    # rubocop:enable Airbnb/RiskyActiverecordInvocation
  end

  def self.transform_coordinates_to_postgis_polygon_string(coordinates)
    # Ensure the coordinates form a closed polygon
    if coordinates.size > 1 && coordinates.first[:lat] != coordinates.last[:lat] && coordinates.first[:lng] != coordinates.last[:lng]
      coordinates += [{ lat: coordinates.first[:lat], lng: coordinates.first[:lng] }]
    end
    "POLYGON ((#{coordinates.map { |coorditate| "#{coorditate[:lat]} #{coorditate[:lng]}" }.join(',')}))"
  end

  # Calculates the coordinates of the Circleoid by walking around a center in SIDES_FOR_CIRCLEOID steps
  def self.circleoid_coordinates(center_longitude:, center_latitude:, radius:)
    radius_lat = radius / Constant::MILES_PER_LAT
    radius_lng = radius / Constant::MILES_PER_LNG

    sides = SIDES_FOR_CIRCLEOID
    iteration_angle = (2 * Math::PI) / sides

    coordinates = []
    sides.times do |iteration|
      angle = iteration_angle * iteration
      lat = center_latitude + (radius_lat * Math.cos(angle))
      lng = center_longitude + (radius_lng * Math.sin(angle))
      coordinates << { lng: lng, lat: lat }
    end
    # Ensure closed polygon
    coordinates << coordinates.first
    coordinates
  end

  private

  def is_company_a_rescue_company?
    unless company.is_a? RescueCompany
      errors.add :company, "The Company must be a RescueCompany."
    end
  end

end
