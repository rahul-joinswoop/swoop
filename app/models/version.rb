# frozen_string_literal: true

# It represents our system versions. These are manually added using the web UI
# (as root user, go to Settings, there'll be the tabs: Web versions, Android versions and ios Versions)
#
# The name attribute currently holds the component this version refers to.
# It will likely be web, ios or android. But it's not restricted to these values only.
#
class Version < BaseModel

  # The correct approach would be using Gem::Version for this comparision,
  # but then specs fail on CircleCI \_(ツ)_/
  # So let's implement the checker. See https://stackoverflow.com/a/2051427.
  class VersionChecker < Array

    def initialize(version)
      if version
        super(version.split('.').map { |e| e.to_i })
      else
        super([])
      end
    end

    def <(x)
      (self <=> x) < 0
    end

    def <=(x)
      (self <=> x) <= 0
    end

    def >(x)
      (self <=> x) > 0
    end

    def >=(x)
      (self <=> x) >= 0
    end

    def ==(x)
      (self <=> x) == 0
    end

  end

  # there can be only one version by name (name is actually the component this version refers to)
  validates_uniqueness_of :version, scope: :name
  validates :name, :version, presence: true

  before_validation :force_name_to_downcase

  after_commit if: :new_forced_version_for_mobile? do
    ForcedVersionUploadWorker.perform_async(id)
  end

  def listeners
    Rails.logger.error "Iterating over all versions"
    ret = Set.new
    Company.all.each do |company|
      ret << company
    end
    ret
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/versions/_version", locals: { version: self }))
  end

  def s3_bucket
    S3.bucket(Configuration.s3.version_bucket)
  end

  def s3_object
    s3_bucket.object("latest_forced_version_#{name}.json")
  end

  def new_forced_version_for_mobile?
    is_forced_set = forced_previous_change&.second
    is_android_or_ios = ['android', 'ios'].include? name

    # find most recent forced version
    last_forced_version = Version.where(
      name: name,
      forced: true,
    ).order(version: :desc).first

    is_eq_or_newer_than_last_forced_version = VersionChecker.new(version) >= VersionChecker.new(last_forced_version&.version)

    Rails.logger.debug(
      "#{self.class.name}##{__method__} - " \
      "is_forced_set #{is_forced_set}, " \
      "is_android_or_ios #{is_android_or_ios}, " \
      "is_eq_or_newer_than_last_forced_version #{is_eq_or_newer_than_last_forced_version}, "
    )

    is_forced_set && is_android_or_ios && is_eq_or_newer_than_last_forced_version
  end

  def new_forced_version_hash
    { version: version, description: description, component: name }
  end

  private

  def force_name_to_downcase
    name.downcase!
  end

end
