# frozen_string_literal: true

# a model should inherit from this if it needs to support pub/sub or redis caching
class BaseModel < ApplicationRecord

  include RedisConcerns
  include Publishable
  include PermissionsConcerns

  self.abstract_class = true

  class << self

    def find_in_cache(ids)
      fetch(ids)
    end

    def find_by_id_in_cache(ids)
      fetch_by_id(ids)
    end

  end

end
