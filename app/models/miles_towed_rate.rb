# frozen_string_literal: true

class MilesTowedRate < Rate

  validates :miles_towed, presence: true
  validates :miles_towed_free, presence: true

  def mail_attributes
    base = super

    base.merge({
      HOOKUP => hookup,
      MILES_TOWED => miles_towed,
      FREE_MILES_TOWED => miles_towed_free,
    })
  end

  def line_item_descriptions
    [TAX, HOOKUP, MILES_TOWED, KILOMETERS_TOWED, FREE_MILES_TOWED, FREE_KILOMETERS_TOWED]
  end

  def calculate_line_items(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    lis = []
    lis << base_hookup
    lis.concat(base_tow(invoicable))
    add_common_items(invoicable, lis)
    adjust_mileage_line_items(invoicable, lis)
    lis
  end

end
