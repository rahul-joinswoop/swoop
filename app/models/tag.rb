# frozen_string_literal: true

class Tag < BaseModel

  include SoftDelete

  has_and_belongs_to_many :rescue_providers
  belongs_to :company

  PRIMARY = 'Primary'
  SECONDARY = 'Secondary'
  OUT_OF_NETWORK = 'Out of Network'
  TESLA = 'Tesla'
  GEICO = 'Geico'

  def listeners
    Set.new([company])
  end

  def render_to_hash
    TagSerializer.new(self).serializable_hash
  end

end
