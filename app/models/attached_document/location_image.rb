# frozen_string_literal: true

require 'attached_document/image'
class AttachedDocument
  class LocationImage < Image

    self.table_name = "attached_documents"

  end
end
