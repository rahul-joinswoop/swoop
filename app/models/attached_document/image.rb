# frozen_string_literal: true

class AttachedDocument
  class Image < AttachedDocument

    self.table_name = "attached_documents"

  end
end
