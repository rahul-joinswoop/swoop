# frozen_string_literal: true

class AttachedDocument
  class Document < AttachedDocument

    self.table_name = "attached_documents"

  end
end
