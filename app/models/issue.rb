# frozen_string_literal: true

class Issue < BaseModel

  include StandardOrCustomCollection

  has_many :companies_issues, class_name: 'CompaniesIssues'
  has_many :companies, through: :companies_issues
  has_many :reasons, -> { not_deleted }, inverse_of: :issue

  validates :key, presence: true
  validates :name, presence: true

  MISSED_ETA = 'Missed ETA'
  ENTERED_ETA_OVER_75_MIN = 'Entered ETA over 75 min'
  LONG_HANDLE_TIME = 'Long handle time'
  REJECT_JOB = 'Reject Job'
  CLAIM = 'Claim'
  REDISPATCHED = 'Redispatched'
  OVERAGE_OVERRIDE = 'Overage Override'
  CUSTOMER_COMPLAINT = 'Customer Complaint'
  EXTERNAL_COMPLAINT = 'External Complaint'

  ALL_ISSUES = [
    MISSED_ETA,
    ENTERED_ETA_OVER_75_MIN,
    LONG_HANDLE_TIME,
    REJECT_JOB,
    CLAIM,
    REDISPATCHED,
    OVERAGE_OVERRIDE,
    CUSTOMER_COMPLAINT,
    EXTERNAL_COMPLAINT,
  ].freeze

  ISSUE_KEY_MAP = {
    MISSED_ETA => :missed_eta,
    ENTERED_ETA_OVER_75_MIN => :long_eta,
    LONG_HANDLE_TIME => :long_handle,
    REJECT_JOB => :reject,
    CLAIM => :claim,
    REDISPATCHED => :redispatched,
    OVERAGE_OVERRIDE => :overage_override,
    CUSTOMER_COMPLAINT => :customer_complaint,
    EXTERNAL_COMPLAINT => :external_complaint,
  }.freeze

end
