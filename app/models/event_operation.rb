# frozen_string_literal: true

class EventOperation < ApplicationRecord

  include IdentityCache
  cache_index :name, unique: true

  CREATE = 'create'
  READ = 'read'
  READ_ALL = 'read all'
  UPDATE = 'update'
  DELETE = 'delete'
  LOCATION_UPDATE = "location update"
  MUTATION = 'mutation'

  ALL_EVENT_OPS = [CREATE, READ, READ_ALL, UPDATE, DELETE, LOCATION_UPDATE, MUTATION].freeze

  def self.init_all
    ALL_EVENT_OPS.each do |op|
      EventOperation.create(name: op)
    end
  end

end
