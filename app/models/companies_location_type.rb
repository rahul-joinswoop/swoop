# frozen_string_literal: true

class CompaniesLocationType < ApplicationRecord

  belongs_to :company
  belongs_to :location_type

end
