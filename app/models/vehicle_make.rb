# frozen_string_literal: true

class VehicleMake < ApplicationRecord

  has_many :vehicle_models

end
