# frozen_string_literal: true

class ServiceCode < ApplicationRecord

  include StandardOrCustomCollection
  include SoftDelete
  include ServiceCodeDefinitions

  # TODO - this is in db/seed.rb already - once we get feature specs working with
  # txns we can ditch this altogether

  # Order, Addon, Variable, Is Standard
  SERVICE_CODES = {
    ACCIDENT_TOW => [nil, false, nil, true],
    BATTERY_JUMP => [1, false, nil, true],
    DECKING => [nil, false, nil, nil],
    DOLLY => [nil, true, false, true],
    DRIVELINE => [nil, true, true, nil],
    FUEL_DELIVERY => [5, false, nil, true],
    FUEL_SURCHARGE => [nil, true, nil, nil],
    FUEL => [nil, true, true, true],
    GATE_FEE => [nil, true, true, false],
    GOA => [nil, false, nil, false],
    GOA_FEE => [nil, true, nil, false],
    GOJAK => [nil, true, false, true],
    HEAVY_HAUL => [nil, false, nil, nil],
    ILLEGALLY_PARKED => [nil, false, nil, nil],
    IMPOUND => [nil, false, nil, true],
    LABOR => [nil, true, true, true],
    LATE_CANCEL_FEE => [nil, true, nil, false],
    LIEN_FEE => [nil, true, true, false],
    LOANER_WHEEL_PICKUP => [nil, true, true, nil],
    LOANER_WHEEL => [nil, false, nil, nil],
    LOCK_OUT => [3, false, nil, true],
    OTHER => [6, false, nil, true],
    OVERNIGHT_STORAGE => [nil, true, nil, false],
    PRIORITY_RESPONSE => [nil, true, false, nil],
    RECOVERY => [nil, false, nil, true],
    REIMBURSEMENT => [nil, true, nil, true],
    REPO => [nil, false, nil, true],
    ROLLOVER => [nil, false, nil, nil],
    SECONDARY_TOW => [nil, false, nil, true],
    SERVICE_CALL => [nil, false, nil, nil],
    STORAGE => [nil, false, nil, true],
    SWAP_FEE => [nil, true, nil, nil],
    TIRE_CHANGE => [4, false, nil, true],
    TIRE_SKATES => [nil, true, false, true],
    TOLL => [nil, true, true, true],
    TOW_IF_JUMP_FAILS => [2, false, nil, true],
    TOW_IF_TIRE_CHANGE_FAILS => [nil, false, nil, true],
    TOW => [0, false, nil, true],
    TRANSPORT => [nil, false, nil, true],
    UNDECKING => [nil, false, nil, nil],
    UPRIGHTING => [nil, false, nil, nil],
    WAIT_TIME => [nil, true, true, true],
    WINCH_OUT => [nil, false, nil, true],
  }.freeze

  SUPPORTS_STORAGE_SERVICE_CODES = [
    ACCIDENT_TOW,
    DECKING,
    HEAVY_HAUL,
    IMPOUND,
    LAW_ENFORCEMENT,
    RECOVERY,
    SECONDARY_TOW,
    TOW,
    TRANSPORT,
    UNDECKING,
    WINCH_OUT,
  ].freeze

  AUTO_DISPATCHABLE_SERVICE_CODES = [
    BATTERY_JUMP,
    DECKING,
    LOCK_OUT,
    ROLLOVER,
    SERVICE_CALL,
    TIRE_CHANGE,
    TOW,
    TOW_IF_JUMP_FAILS,
    UNDECKING,
  ].freeze

  SERVICE_LOCATION_SERVICE_CODES = [
    BATTERY_JUMP, FUEL_DELIVERY, LOCK_OUT, ROLLOVER, SERVICE_CALL, TIRE_CHANGE,
  ].freeze

  FLEET_DROP_LOCATION_SERVICE_CODES = [
    DECKING, TOW, TOW_IF_JUMP_FAILS, UNDECKING,
  ].freeze

  RESCUE_DROP_LOCATION_SERVICE_CODES = [
    IMPOUND, ACCIDENT_TOW,
  ].freeze

  FLATBED_SERVICE_CODES = [
    TOW, ACCIDENT_TOW, TOW_IF_JUMP_FAILS,
  ].freeze

  SPECIALITY_SERVICE_CODES = [TRANSPORT, SECONDARY_TOW, REPO].freeze

  has_many :company_service_questions
  has_many :companies_services
  has_many :companies, through: :companies_services

  scope :support_storage, -> { not_deleted.where(support_storage: true) }

  scope :services, -> { where.not(addon: true) }
  scope :live_services, -> { services.not_deleted }

  scope :addons, -> { where(addon: true) }
  scope :live_addons, -> { addons.not_deleted }

  class << self

    def storage
      ServiceCode.find_by(name: ServiceCode::STORAGE)
    end

    def goa
      ServiceCode.find_by(name: ServiceCode::GOA)
    end

  end

  def can_auto_dispatch?
    AUTO_DISPATCHABLE_SERVICE_CODES.include? name
  end

  def is_service_location_code?
    SERVICE_LOCATION_SERVICE_CODES.include? name
  end

  def is_rescue_drop_location_code?
    RESCUE_DROP_LOCATION_SERVICE_CODES.include? name
  end

  def is_fleet_drop_location_code?
    FLEET_DROP_LOCATION_SERVICE_CODES.include? name
  end

  def flatbed_required?
    FLATBED_SERVICE_CODES.include? name
  end

  def jump_box_required?
    BATTERY_JUMP == name
  end

  def accident_tow?
    ACCIDENT_TOW == name
  end

  def speciality?
    SPECIALITY_SERVICE_CODES.include?(name)
  end

  def standard?
    !!is_standard
  end

  def custom?
    !standard?
  end

  def goa?
    name == GOA
  end

end
