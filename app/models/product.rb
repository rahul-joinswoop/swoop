# frozen_string_literal: true

class Product < ApplicationRecord

  has_and_belongs_to_many :companies
  DISPATCH = "dispatch"
  TOOLS = "tools"
  FLEET = "fleet"

end
