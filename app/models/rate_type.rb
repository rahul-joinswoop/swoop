# frozen_string_literal: true

# This represents all possible RateType values that we can show in our UI (web and mobile v3).
#
# The standard ones that we always show in the invoice form can be fetched by RateType.standard_items
#
# @see original https://github.com/joinswoop/swoop/blob/master/app/assets/javascripts/base_consts.js#L728-L735
class RateType < ApplicationRecord

  # We have a type column in this table that is currently not being used for STI. So we disable it.
  # We may want to compare this RateType with the invoice.rate_type string internally so I'm leaving
  # it here for now.
  #
  # We also have:
  # - name column: provides a friendly text to be displayed in the UI,
  # - friendly_type column: to be used as the RateTypeEnum type in GraphQL API.
  self.inheritance_column = nil

  include StandardOrCustomCollection

  scope :tesla_rate, -> { find_by!(friendly_type: TESLA) }

  HOURS_P2P = 'HoursPortToPort'
  MILES_TOWED = 'MileageTowed'
  MILES_EN_ROUTE = 'MileageEnRoute'
  MILES_EN_ROUTE_AND_TOWED = 'MilesEnRouteAndTowed'
  MILES_P2P = 'MilesPortToPort'
  FLAT = 'Flat'
  STORAGE = 'Storage'
  TESLA = 'Tesla'

  ALL_RATE_TYPES = [
    { friendly_type: HOURS_P2P, name: 'Hourly (Port to Port)', type: 'HoursP2PRate', is_standard: true },
    { friendly_type: MILES_TOWED, name: 'Mileage (Towed)', type: 'MilesTowedRate', is_standard: true },
    { friendly_type: MILES_EN_ROUTE, name: 'Mileage (En Route)', type: 'MilesEnRouteOnlyRate', is_standard: true },
    { friendly_type: MILES_EN_ROUTE_AND_TOWED, name: 'Mileage (En Route + Towed)', type: 'MilesEnRouteRate', is_standard: true },
    { friendly_type: MILES_P2P, name: 'Mileage (Port to Port)', type: 'MilesP2PRate', is_standard: true },
    { friendly_type: FLAT, name: 'Flat', type: 'FlatRate', is_standard: true },
    { friendly_type: STORAGE, name: 'Storage', type: 'StorageRate' },
    { friendly_type: TESLA, name: 'Tesla', type: 'TeslaRate' },
  ].freeze

  def self.init_all
    ALL_RATE_TYPES.each do |rate_type|
      # we already have (supposedly unused) RateTypes in the DB so we just update them if exist, otherwise create new
      # TODO remove not used ones https://swoopme.atlassian.net/browse/ENG-9334
      rate_type_db = RateType.where(name: rate_type[:name]).first_or_create!

      rate_type_db.update!(
        friendly_type: rate_type[:friendly_type],
        type: rate_type[:type],
        is_standard: rate_type[:is_standard],
      )
    end
  end

end
