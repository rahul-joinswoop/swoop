# frozen_string_literal: true

require 'aasm'
require 'issc'

# PK is contractorid, clientid, issc_location_id

class Issc < BaseModel

  include Utils
  include AASM

  # Supported ISSC as of 08/26/18
  SUPPORTED = ['TSLA', 'AGO', 'USAC', 'QUEST', 'NSD'].freeze

  # These were turned off by ISSC / clients as of 08/26/18
  UNSUPPORTED = ['GCOAPI', 'GCO', 'ALLS', 'ADS', 'RDAM'].freeze

  # only agero and quest support the max bidded eta without explanation
  USES_MAX_BIDDED_ETA_WITHOUT_EXPLANATION = ['AGO', 'QUEST'].freeze

  DEFAULT_MAX_BIDDED_ETA_WITHOUT_EXPLANATION = 90
  # These systems take about a day to authorize providers
  ISSC_MANUAL_AUTHORIZATIONS = ['AGO', 'USAC', 'QUEST', 'NSD'].freeze

  PROVIDERS_WITH_LOCATION_SUPPORT = ['GCOAPI', 'RDAM', 'AGO'].freeze
  AGERO_CLIENTID = "AGO"
  PROVIDERS_WITH_USERNAME_SUPPORT = ['NSD'].freeze
  PROVIDERS_WHICH_REQUIRE_ADDRESS = ['USAC'].freeze

  ISSC_SYSTEM = :issc
  RSC_SYSTEM  = :rsc

  belongs_to :company
  belongs_to :site
  belongs_to :issc_location
  belongs_to :issc_login
  belongs_to :api_access_token
  has_many :dispatch_requests, class_name: "IsscDispatchRequest"

  accepts_nested_attributes_for :issc_location

  scope :dispatchable, -> { joins(:site).where(deleted_at: nil, sites: { dispatchable: true }) }

  before_save :clear_delete_pending_at
  after_commit :publish_account

  UNREGISTERED = 'Unregistered'
  REGISTERING = 'Registering'
  REGISTERED = 'Registered'
  DEREGISTERING = 'Deregistering'
  LOGGING_IN = 'LoggingIn'
  LOGGED_IN = 'LoggedIn'
  LOGGING_OUT = 'LoggingOut'
  REGISTER_FAILED = 'RegisterFailed'
  PASSWORD_INCORRECT = "PasswordIncorrect"
  enum status: {
    unregistered: UNREGISTERED,
    registering: REGISTERING,
    registered: REGISTERED,
    deregistering: DEREGISTERING,
    logged_in: LOGGED_IN,
    logging_in: LOGGING_IN,
    logging_out: LOGGING_OUT,
    register_failed: REGISTER_FAILED,
    password_incorrect: PASSWORD_INCORRECT,
  }

  aasm column: :status, enum: true, no_direct_assignment: false do
    state :unregistered, initial: true
    state :registered, :registering
    state :logged_in, :logging_in
    state :deregistering, :logging_out
    state :register_failed, :password_incorrect

    event :provider_register, after: :clear_error do
      transitions from: :unregistered, to: :registering
    end

    event :provider_register_failed do
      transitions from: [:registered, :registering], to: :register_failed
    end

    event :provider_registered, after: :clear_error do
      transitions from: [:registered, :registering, :unregistered], to: :registered, if: :has_token?
    end

    event :provider_deregister, after: :clear_error do
      transitions from: [:registered, :password_incorrect, :logging_in], to: :deregistering
    end

    event :provider_deregistered, after: :clear_error do
      transitions from: :deregistering, to: :unregistered, unless: :has_token?
    end

    event :provider_login, after: :clear_error do
      transitions from: [:logging_out, :logged_in, :registered], to: :logging_in
    end

    event :provider_password_incorrect, after: :mark_login_incorrect do
      transitions from: :logging_in, to: :password_incorrect
    end

    event :provider_logged_in, after: :clear_error do
      transitions from: :logging_in, to: :logged_in
    end

    event :provider_logout, after: :clear_error do
      transitions from: [:logging_in, :logged_in], to: :logging_out
    end

    event :provider_logged_out, after: :clear_error do
      transitions from: :logging_out, to: :registered
    end

    event :force_logged_out, after: :clear_error do
      transitions from: :logged_in, to: :registered
    end
  end

  scope :client, ->(clientid) { where(clientid: clientid) }
  scope :contractor, ->(contractorid) { where(contractorid: contractorid) }

  def publish_account
    fleet_company = FleetCompany.find_by(issc_client_id: clientid)
    account = company.base_company.accounts.find_by(client_company: fleet_company) if fleet_company
    account.base_publish("update") if account
    true
  end

  def clear_error
    self.error = nil
    self.error_at = nil
  end

  def set_error(error)
    self.error = error
    self.error_at = DateTime.new
  end

  def mark_login_incorrect
    issc_login.incorrect = true
  end

  def register
    Rails.logger.debug "ISSC Registering(#{id}) - Status: #{status} - #{status == "registering"}"
    if status == "registering"
      IsscRegisterProvider.perform_async(id)
    end
  end

  def deregister
    if status == "deregistering"
      IsscDeregisterProvider.perform_async(id)
    end
  end

  def deregister_if_deleted
    if deleted_at
      IsscFixDeleted.perform_async
    end
  end

  def login
    if status == "logging_in"
      IsscLoginProvider.perform_async(id)
    end
  end

  def logout
    if status == "logging_out"
      IsscLogoutProvider.perform_async(id)
    end
  end

  def has_token?
    !token.nil?
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/isscs/_issc", locals: { issc: self }))
  end

  def listeners
    Set.new
  end

  def locationid
    issc_location.locationid if supports_location?
  end

  def supports_location?
    Issc.provider_supports_location? clientid
  end

  def supports_login?
    Issc.provider_supports_login? clientid
  end

  def self.provider_supports_location?(clientid)
    PROVIDERS_WITH_LOCATION_SUPPORT.include? clientid
  end

  def self.provider_supports_login?(clientid)
    PROVIDERS_WITH_USERNAME_SUPPORT.include? clientid
  end

  def self.find_issc(event, fetch_deleted: false)
    # Rails.logger.debug "Finding ISSC #{event}"
    query = Issc.where(clientid: event[:ClientID], contractorid: event[:ContractorID])
    if provider_supports_location? event[:ClientID]
      query = query.where('issc_locations.locationid': event[:LocationID])
    end

    if fetch_deleted
      query = query.where.not(deleted_at: nil)
    else
      query = query.where(deleted_at: nil)
    end

    issc = query.joins('LEFT OUTER JOIN "issc_locations" ON "isscs"."issc_location_id" = "issc_locations"."id"').order(updated_at: :desc).first
    if issc.nil?
      raise ISSC::Unroutable.new(event[:ClientID], event[:LocationID], event[:ContractorID])
    else
      issc
    end
  end

  def self.find_live_issc(contractorid, clientid, locationid = nil, system = nil, rescue_companyid = nil)
    args = {
      contractorid: contractorid,
      clientid: clientid,
      deleted_at: nil,
    }
    if system
      args[:system] = system
    end

    if rescue_companyid
      args[:company_id] = rescue_companyid
    end

    issc = nil
    if locationid
      args[:issc_locations] = { locationid: locationid }
      issc = Issc.joins(:issc_location).where(args).first
    else
      issc = Issc.where(args).first
    end
    issc
  end

  def self.get_login_candidates(system)
    sql = <<~SQL
      SELECT DISTINCT isscs.*
      FROM isscs
      LEFT JOIN companies AS client_companies ON isscs.clientid = client_companies.issc_client_id
      LEFT JOIN sites ON isscs.site_id = sites.id
      LEFT JOIN rescue_providers ON rescue_providers.provider_id = isscs.company_id
        AND rescue_providers.site_id = sites.id
        AND rescue_providers.company_id = client_companies.id
      LEFT JOIN accounts ON accounts.company_id = isscs.company_id
        AND accounts.client_company_id = client_companies.id
      WHERE #{system == Issc::ISSC_SYSTEM ? "(isscs.system IS NULL OR isscs.system = ?)" : "isscs.system = ?"}
        AND isscs.deleted_at IS NULL
        AND isscs.status IN ('Registered')
        AND isscs.delete_pending_at IS NULL
        AND rescue_providers.deleted_at IS NULL
        AND accounts.deleted_at IS NULL
        AND sites.deleted_at IS NULL
        AND sites.dispatchable = TRUE
        AND (sites.within_hours = TRUE OR sites.force_open = TRUE)
        AND (
          isscs.logged_in_at IS NOT NULL
          OR isscs.last_login_attempted_at IS NULL
          OR NOW() - isscs.last_login_attempted_at > INTERVAL ?
        )
    SQL

    interval = (system == Issc::ISSC_SYSTEM ? "4 HOURS" : "5 MINUTES")
    Issc.find_by_sql([sql, system, interval])
  end

  def self.get_logout_candidates(system)
    sql = <<~SQL
      SELECT DISTINCT isscs.*
      FROM isscs
      LEFT JOIN companies AS client_companies ON isscs.clientid = client_companies.issc_client_id
      LEFT JOIN sites ON isscs.site_id = sites.id
      LEFT JOIN rescue_providers ON rescue_providers.provider_id = isscs.company_id
        AND rescue_providers.site_id = sites.id
        AND rescue_providers.company_id = client_companies.id
      LEFT JOIN accounts ON accounts.company_id = isscs.company_id
        AND accounts.client_company_id = client_companies.id
      LEFT OUTER JOIN jobs ON jobs.site_id = sites.id
      WHERE isscs.status IN ('LoggedIn')
        AND isscs.deleted_at IS NULL
        AND #{system == Issc::ISSC_SYSTEM ? "(isscs.system IS NULL OR isscs.system = ?)" : "isscs.system = ?"}
        AND (
          (sites.within_hours is false AND sites.force_open != TRUE)
          OR (rescue_providers.deleted_at IS NOT NULL)
          OR isscs.delete_pending_at IS NOT NULL
          OR sites.deleted_at IS NOT NULL
          OR accounts.deleted_at IS NOT NULL
          OR sites.dispatchable IS NOT TRUE
        )
        AND (jobs.status IS NULL OR (jobs.deleted_at IS NULL AND jobs.status NOT IN ('Assigned', 'Submitted')))
    SQL
    Issc.find_by_sql([sql, system])
  end

  private

  def clear_delete_pending_at
    if deleted_at.present? && delete_pending_at.present?
      self.delete_pending_at = nil
    end
  end

end
