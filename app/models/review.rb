# frozen_string_literal: true

class String

  def numeric?
    !Float(self).nil?
  rescue
    false
  end

end

class Review < BaseModel

  include TwilioConcerns

  DELAY_COMPLETE = "complete"
  DELAY_THREE_HOURS = "three_hours"
  DELAY_SEVEN_PM = "seven_pm"
  DELAY_NINE_AM = "nine_am"
  FEEDBACK_CONFIRMATION = "Thank you for the feedback!"

  belongs_to :company
  belongs_to :user
  belongs_to :job
  belongs_to :survey
  has_many :survey_results, class_name: 'Survey::Result'

  has_many :replies, -> { order(:created_at) }, as: :target, class_name: "TwilioReply", inverse_of: :target

  before_save :normalize_phone

  after_create :set_review_text

  def text_template
    "%{company_name_brand}: Please reply with a number 1-5 (5 is best) to rate your service"
  end

  def company_review_url
    company.review_url || ENV["REVIEW_DEFAULT_URL"] || "http://bit.ly/2d9LhQG"
  end

  def set_review_text
    company_name_brand = nil

    if job.instance_of?(RescueJob) || job.instance_of?(FleetMotorClubJob)
      company_name_brand = job.rescue_company.name
    else
      company_name_brand = job.fleet_company.name
    end

    self.text = text_template % { company_name_brand: company_name_brand }
  end

  def send_review_sms
    Review.send_sms(phone, text, { user_id: user.id, job_id: job.id, type: 'ReviewAuditSms' })
  end

  def self.parse_rating(text, max: 5)
    rating = -1
    text.gsub(/[^a-z0-9\s.]/i, '').split(' ').each do |token|
      Rails.logger.debug token
      if !token.numeric?
        token = token.gsub(/[.]/i, '')
      end
      if token.numeric?
        rating = Float(token)
        Rails.logger.debug "set rating to #{rating}"
        break
      end
    end

    if rating != -1
      rating = rating.clamp(1, max).round
    end
    rating
  end

  def process_response(twilio_response)
    if rating.nil?
      process_first_response(twilio_response)
    elsif !done?
      update!(done: true)

      process_thank_you_for_feedback(twilio_response)
    end
  end

  def process_thank_you_for_feedback(twilio_response)
    # Persist the feedback response from TwilioReply to Review
    update!(nps_feedback: twilio_response.body)

    Review.send_sms(phone, FEEDBACK_CONFIRMATION, { user_id: user.id, job_id: job.id, type: 'ReviewFollowup2AuditSms' })
    Review.delay.send_feedback_to_slack(id, twilio_response)
  end

  def process_first_response(twilio_response)
    rating = Review.parse_rating(twilio_response.body)
    if rating != -1

      body = "Thanks for your rating! How do you think we can improve?"
      if company.permission_to_send_reviews_from
        body = "#{company.try(:name)}: #{body}"
      end
      Review.send_sms(phone, body, { user_id: user.id, job_id: job.id, type: 'ReviewFollowupAuditSms' })

      self.rating = rating
      # this converts the 5 star reviews into the same field as the 10 star review
      # which appears in the reviews screen. Currently only available to Swoop.

      self.nps_question1 = rating
      save!
    end
    Review.delay.send_review_to_slack(id, twilio_response)
  end

  def self.send_review_to_slack(review_id, twilio_response)
    review = Review.find_by!(id: review_id)
    msg = (%w(:star:) * review.rating).join if review.rating
    review.job.slack_notification "#{msg}'#{review.user.full_name}' for "\
                           "'#{review.job.rescue_driver.try(:full_name)}'@'#{review.job.rescue_company.name}'"\
                           " for '#{review.company.name}': \"#{twilio_response.body}\"", tags: %w(review)
  end

  def responses
    user.twilio_replies
  end

  def self.send_feedback_to_slack(review_id, twilio_response)
    review = Review.find_by!(id: review_id)
    review.job.slack_notification "'#{review.user.full_name}' for '#{review.company.name}':"\
                           "\"#{twilio_response.body}\"", tags: %(review)
  end

  def listeners
    job.listeners
  end

  def render_to_hash
    ReviewSerializer.new(self).serializable_hash
  end

  # This is pretty close to how the current code flows, however I want to spec and make sure
  # if a non- vaiable review gets in.
  def send_to_slack(guard = false, question_cnt = 0)
    return unless guard
    # TODO: add Review information such as job and provider
    job.slack_notification nps_slack_message(question_cnt),
                           preface_id: false,
                           tags: %w(review)
  end

  def nps_slack_feedback_message
    job.slack_notification "\tFeedback:\n\t\t#{nps_feedback}\n", tags: %w(review)
  end

  def most_recent_results
    Survey::Result.where("id IN (
      SELECT id FROM (
      SELECT MAX(id) id, survey_question_id FROM survey_results WHERE review_id = ?
      AND id IN (SELECT id FROM survey_results WHERE review_id = ? ORDER BY created_at DESC)
      GROUP BY survey_question_id) results)", id, id).order(:survey_question_id)
  end

  private

  def normalize_phone
    self.phone = Phonelib.parse(phone).e164 if phone_changed?
  end

  def nps_slack_message(question_cnt)
    str = ''

    str += "Review of #{job.swoop_summary_name}\n"
    str += slack_rating_string("Question 1", nps_question1)
    (str += slack_rating_string("Question 2", nps_question2)) if question_cnt >= 2
    (str += slack_rating_string("Question 3", nps_question3)) if question_cnt >= 3
    str
  end

  def slack_rating_string(tag, tag_rating)
    stars = ([':star:'] * tag_rating.to_i).join(' ')
    "\t#{tag}:\t#{tag_rating.to_i}/10 #{stars}\n"
  end

end
