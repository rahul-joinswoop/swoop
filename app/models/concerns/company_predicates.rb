# frozen_string_literal: true

module CompanyPredicates

  # almost all the predicates from the Company class, moved here to make the model somewhat more readable.
  extend ActiveSupport::Concern

  included do
    # TODO - remove this usage
    alias_method :has_feature, :has_feature?
    alias_method :has_product, :has_product?

    # we use client/partner in graphql as much as possible, hence this aliasing
    alias_method :is_client?, :fleet?
    alias_method :is_partner?, :rescue?
    alias_method :is_super?, :super?

    # agero is also a super_fleet_managed company - they can see all FleetManaged jobs
    alias_method :super_fleet_managed?, :is_agero?
    # this exists to keep parity with is_client?, is_rescue?, etc
    alias_method :is_super_client_managed?, :is_agero?

    # TODO - maybe use one or the other?
    alias_method :motor_club?, :issc_motor_club?
  end

  # class-based predicates
  def super?
    is_a? SuperCompany
  end

  def fleet?
    is_a? FleetCompany
  end

  def fleet_managed?
    fleet? && !fleet_in_house?
  end

  def fleet_in_house?
    fleet? && in_house?
  end

  def rescue?
    is_a? RescueCompany
  end

  def issc_motor_club?
    issc_client_id.present?
  end

  # name-based predicates

  # agero is also a super_fleet_managed company - they can see all FleetManaged jobs
  def is_agero?
    name.eql?(Company::AGERO) && fleet?
  end

  def is_turo?
    name.eql?(Company::TURO)
  end

  def is_swoop?
    name.eql?(Company::SWOOP) && super?
  end

  def is_tesla?
    name.eql?(Company::TESLA) && fleet?
  end

  def is_enterprise?
    name.eql?(Company::ENTERPRISE) && fleet?
  end

  def is_enterprise_fleet_management?
    name.eql?(Company::ENTERPRISE_FLEET_MANAGEMENT) && fleet?
  end

  def is_farmers?
    name.eql?(Company::FARMERS) && fleet?
  end

  def is_century?
    name.eql?(Company::CENTURY) && fleet?
  end

  def is_farmers_or_century?
    is_farmers? || is_century?
  end

  def is_mts?
    name.eql?(Company::MOTORCYCLE_TOWING_SERVICES) && fleet?
  end

  LOAD_NAMES = ['FleetLoad', 'PartnerLoad', 'FleetInhouseLoad'].freeze

  def is_load?
    name.in? LOAD_NAMES
  end

  # other

  def has_product?(name)
    name = name.to_s
    products.any? { |product| product.name == name }
  end

  def has_feature?(name)
    name = name.to_s
    features.any? { |feature| feature.name == name }
  end

  # base implementation, overridden in FleetCompany and RescueCompany
  def sites_enabled?
    false
  end

  # base implementation, overridden in FleetCompany and RescueCompany
  def vehicle_class_type_enabled?
    false
  end

  module ClassMethods

    # Optimized method of checking if an object represents Swoop without
    # having to query the database.
    def swoop?(object)
      object.is_a?(Company) && object.id == swoop_id
    end

  end

end
