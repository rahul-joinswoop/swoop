# frozen_string_literal: true

# constants used in Answer model and elsewhere, defined here to avoid
# circular dependencies between Question, Answer, and ServiceCode
module AnswerDefinitions

  extend ActiveSupport::Concern

  # Generic answers
  NO      = 'No'
  YES     = 'Yes'
  UNKNOWN = 'Unknown'

  # "Will customer ride with tow truck?" answers
  TOW_TRUCK_YES_1        = 'Yes - 1 person'
  TOW_TRUCK_YES_2        = 'Yes - 2 people'
  TOW_TRUCK_YES_MULTIPLE = 'Yes - 3+ people'

  # "Where are the keys located?" answers
  KEYS_TRUNK_INACCESSIBLE   = 'Trunk (inaccessible)'
  KEYS_TRUNK_ACCESSIBLE     = 'Trunk (accessible)'
  KEYS_INSIDE_VEHICLE       = 'Inside Vehicle'
  KEYS_VISOR                = 'Visor'
  KEYS_FLOOR_MAT            = 'Under Floor Mat'

  # "What is the gasoline type?" answers
  GAS_REGULAR = 'Regular'
  GAS_PLUS    = 'Plus'
  GAS_PREMIUM = 'Premium'
  GAS_DIESEL  = 'Diesel'

  # "Which tire is damaged?" answers
  DAMAGED_TIRE_FRONT_DRIVER    = 'Driver Front'
  DAMAGED_TIRE_FRONT_PASSENGER = 'Passenger Front'
  DAMAGED_TIRE_REAR_DRIVER     = 'Driver Rear'
  DAMAGED_TIRE_REAR_PASSENGER  = 'Passenger Rear'
  DAMAGED_TIRE_MULTIPLE        = 'Multiple'

end
