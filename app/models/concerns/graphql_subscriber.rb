# frozen_string_literal: true

module GraphQLSubscriber

  extend ActiveSupport::Concern

  included do
    has_many :graphql_subscriptions, as: :viewer
  end

end
