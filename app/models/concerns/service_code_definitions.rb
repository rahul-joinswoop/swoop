# frozen_string_literal: true

# constants used in ServiceCode model and elsewhere, defined here to avoid
# circular dependencies between Question, Answer, and ServiceCode

module ServiceCodeDefinitions

  extend ActiveSupport::Concern

  ACCIDENT_CLEANUP = 'Accident Clean Up'
  ACCIDENT_TOW = 'Accident Tow'
  AFTER_HOURS_FEE = 'After Hours Fee'
  BATTERY_JUMP = 'Battery Jump'
  CC_PROCESSING_FEES = 'CC Processing Fees'
  CUSTOM_ITEM = 'Custom Item'
  DECKING = 'Decking'
  DOLLY = 'Dolly'
  DRIVELINE = 'Driveline'
  FUEL = 'Fuel'
  FUEL_DELIVERY = 'Fuel Delivery'
  FUEL_SURCHARGE = 'Fuel Surcharge'
  GATE_FEE = 'Gate Fee'
  GOJAK = 'GoJak'
  GOA = 'GOA'
  GOA_FEE = 'Gone On Arrival Fee'
  HEAVY_HAUL = 'Heavy Haul'
  ILLEGALLY_PARKED = 'Illegally Parked'
  IMPOUND = 'Impound'
  LABOR = 'Labor'
  LATE_CANCEL_FEE = 'Late Cancel Fee'
  LAW_ENFORCEMENT = 'Law Enforcement'
  LIEN_FEE = 'Lien Fee'
  LOANER_WHEEL = 'Loaner Wheel'
  LOANER_WHEEL_PICKUP = 'Loaner Wheel Pickup'
  LOCK_OUT = 'Lock Out'
  OTHER = 'Other'
  OVERNIGHT_STORAGE = 'Overnight Storage'
  PRIORITY_RESPONSE = 'Priority Response'
  RAMPS = 'Ramps'
  RECOVERY = 'Recovery'
  REIMBURSEMENT = 'Reimbursement'
  RELEASE_FEES = 'Release Fees'
  REPO = 'Repo'
  ROLLOVER = 'Rollover'
  SECONDARY_TOW = 'Secondary Tow'
  SECOND_TRUCK = 'Second Truck'
  SERVICE_CALL = 'Service Call'
  STORAGE = 'Storage'
  SWAP_FEE = 'Swap Fee'
  TIRE_CHANGE = 'Tire Change'
  TIRE_SKATES = 'Tire Skates'
  TOLL = 'Toll'
  TOW = 'Tow'
  TOW_IF_JUMP_FAILS = 'Tow if Jump Fails'
  TOW_IF_TIRE_CHANGE_FAILS = 'Tow if Tire Change Fails'
  TRANSPORT = 'Transport'
  UNDECKING = 'Undecking'
  UPRIGHTING = 'Up-Righting'
  VEHICLE_RELEASE_FEES = 'Vehicle Release Fees'
  WAIT_TIME = 'Wait Time'
  WINCH_OUT = 'Winch Out'

end
