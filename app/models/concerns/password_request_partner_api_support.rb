# frozen_string_literal: true

# shared code used by PartnerAPIPasswordRequest and PartnerAPIEmailVerification (both of which are subclasses of PasswordRequest, hence the class name)
module PasswordRequestPartnerAPISupport

  extend ActiveSupport::Concern

  included do
    # yuck - because client_id is in our custom_data store we can't use a belongs_to here, see
    # https://stackoverflow.com/questions/46635907/rails-use-a-method-for-an-associations-primary-key
    store_accessor :custom_data, :client_id
    validates :client_id, presence: true
    validate :client_id_must_be_valid

    # to make life easier - we take a client_id in but it refers to an application, so i can
    # see why we'd want to use both names.
    alias_method :client, :application
  end

  # do this so that url_for uses our uuid instead of id to create urls
  def to_param
    uuid
  end

  def application
    Doorkeeper::Application.find_by(uid: client_id)
  end

  def send_sms
    raise NoMethodError, "#{self.class.name}#send_sms not implemented"
  end

  private

  def client_id_must_be_valid
    # yuck rails turns this into "Client is invalid" and i can't figure out a way
    # to override this (apparently you can in rails 6 but whatever).
    errors.add(:client_id) if application.blank?
  end

end
