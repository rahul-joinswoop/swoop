# frozen_string_literal: true

module Publishable

  include Utils

  extend ActiveSupport::Concern

  included do
    # Allow Publishable to be implemented by non-ActiveRecord classes
    # (ServiceCodeIdList for instance is not an ActiveRecord)
    if respond_to? :after_commit
      after_commit :after_commit_create, on: :create
      after_commit :after_commit_update, on: :update
      after_commit :after_commit

      after_save :after_save
      after_update :after_update
      after_destroy :after_destroy
    end

    @@msg_id = 0

    def extract_new()
      new = {}
      changes.each do |k, v|
        new[k] = v[1]
      end
      new
    end

    # FIXME location_type is kept here only for retrocompatibility.
    # It should be removed. @see ENG-2065.
    unless defined? VISIBLE_BY_ALL
      VISIBLE_BY_ALL = [
        # "service_code_id"
        "driver_id",
        "fleet_company_id",
        "rescue_company_id",
        "completed_at",
        "created_at",
        "updated_at",
        "status",
        "service_location_id",
        "drop_location_id",
        # "uuid",
        "user_id",
        "rescue_vehicle_id",
        # "drupal_id"
        # "get_location_attempts"
        # "track_technician_sent_at"
        "email",
        "distance",
        "scheduled_for",
        "po_number",
        "caller_id",
        "unit_number",
        "ref_number",
        "last_touched_by_id",
        "rescue_driver_id",
        "odometer",
        "vip",
        "uber",
        "alternate_transportation",
        "customer_type_id",
        "type",
        "parent_id",
        "pickup_contact_id",
        "dropoff_contact_id",
        "no_keys",
        "keys_in_trunk",
        "four_wheel_drive",
        "accident",
        "blocking_traffic",
        "location_type",
        "unattended",
        "reject_reason_id",
        "reject_reason_info",
        "fleet_notes",
        "driver_notes",
        "tire_type_id",
        "get_location_sms",
        "review_sms",
        "original_service_location_id",
      ].freeze
    end

    # fleet both
    VISIBLE_BY_FLEET = ["notes"].freeze unless defined? VISIBLE_BY_FLEET

    # partner
    unless defined? VISIBLE_BY_PARTNER
      VISIBLE_BY_PARTNER = [
        "price",
        "account_id",
        "partner_notes",
        "invoice_id",
        "dispatch_notes",
      ].freeze
    end

    def render_per_listener
      is_a?(Job) || is_a?(::Auction::Auction)
    end

    def base_publish(op, changes = {}, user_id = nil, request = nil, txn_id = nil) # PDW changes unused parameter
      Rails.logger.debug "WS: #{op}, #{changes}, #{user_id}, #{request}, #{txn_id}"
      if defined? render_to_hash

        changes_for_all = extract(VISIBLE_BY_ALL, changes)
        changes_for_partner = extract(VISIBLE_BY_PARTNER, changes)
        changes_for_fleet = extract(VISIBLE_BY_FLEET, changes)
        # logger.debug "PUB: changes_for: #{changes_for_all}, #{changes_for_partner}, #{changes_for_fleet}"
        clz = self.class.name

        msg = { class: clz, operation: op }

        if !render_per_listener
          msg[:target] = render_to_hash
        end

        #        logger.debug("Handling listeners of #{self.inspect}")
        listeners.to_a.compact.each do |listener|
          # Rails.logger.debug "CHECKING_CLZ #{clz} #{self.try(:id)}"
          # Rails.logger.debug "CHECKING_CLZ listener: #{listener.inspect}"

          if listener.is_a? Company
            @api_company = listener
          else
            @api_company = nil
          end

          if render_per_listener
            # Rails.logger.debug "CHECKING_CLZ rendering specific to #{listener.name}"
            msg[:target] = render_to_hash
            # Rails.logger.debug "CHECKING_CLZ rendered #{msg["target"]}"
          end

          # logger.debug "PUB: publishing to listener: #{listener}"
          msg_changes = {}
          msg_changes.merge!(changes_for_all)
          # logger.debug "PUB: merged changes for all : #{msg_changes}"

          if listener.is_a?(RescueCompany) || listener.is_a?(SuperCompany)
            msg_changes.merge!(changes_for_partner)
          end
          if listener.is_a?(FleetCompany) || listener.is_a?(SuperCompany)
            msg_changes.merge!(changes_for_fleet)
          end
          # Rails.logger.debug "WS: Changes: #{msg_changes}"
          msg[:changes] = msg_changes
          channel = "event_#{listener.channel_name}"
          target_type = listener.class.name
          target_id = listener.id
          msg[:channel] = channel
          msg[:pdw_uuid] = @@msg_id
          msg[:txn_id] = txn_id
          @msg_id = @@msg_id + 1

          json = if save_wsevent?(msg)
                   # rubocop:disable Rails/SaveBang
                   ws_event = WsEvent.create({
                     target_type: target_type,
                     target_id: target_id,
                     user_id: user_id,
                     request: request,
                     msg: msg,
                   })
                   # rubocop:enable Rails/SaveBang
                   if listener.is_a? Company
                     WsEventLog::Writer.new(ws_event: ws_event).write
                   end

                   ws_event.json
                 else
                   msg.to_json
                 end

          if is_a?(Job) && ENV['LOG_PUBLISH']
            Rails.logger.debug "WSPUBLISH, Publishing tt:#{clz} tid:#{id} txn:#{txn_id} status:#{msg[:target]['status']} to listener#{target_type}:#{target_id} on channel:#{channel}, changes:#{msg[:changes]}"
          end
          BaseModel.publish(channel, json)
        end
      else
        Rails.logger.debug "No render_to_hash found for (#{id})#{self}"
      end
    end

    def save_wsevent?(msg)
      if (msg[:class] == 'TimeOfArrival') || (msg[:class] == 'RescueVehicle' && location_update)
        return false
      end
      true
    end

    # {
    #  <txn_id> => {<target_id_target_type> => [saves]
    # }
    def store_save(json_changes)
      transactions = RequestStore.store[:txns]
      unless transactions
        transactions = {}
        RequestStore.store[:txns] = transactions
      end

      model_changes = transactions[txn_id]
      unless model_changes
        model_changes = {}
        transactions[txn_id] = model_changes
      end

      changes = model_changes[hash_id]
      unless changes
        changes = []
        model_changes[hash_id] = changes
      end
      changes << json_changes
    end

    def changes_job_id
      if has_attribute?(:job_id)
        job_id
      elsif is_a? Job
        id
      elsif respond_to? :related_job_id
        related_job_id
      end
    end

    def store_publish(update_type)
      if defined?(Rake)
        return if Rake.application.top_level_tasks.include?('db')
      end

      model_changes = RequestStore.store[:txns][stored_txn]

      merged = {}
      changes = model_changes[hash_id] if model_changes
      if changes
        changes.each do |change|
          merged.merge!(change)
        end
      end

      # Can't do this, becuase need to publish all model saves, because of related saves things like user, otherwise user needs to find jobs that are using this user in listeners
      # if merged.size>0

      PublishWorker.perform_async(self.class.name, id, update_type, merged, RequestStore.store[:user_id], RequestStore.store[:request_id], RequestStore.store[:txn_id])
      # end

      # TODO There are cases where model_changes can become nil; these may or may not be legitimate
      # and require nil check or error handling. For now just leave to raise a nil pointer exception.
      model_changes.delete(hash_id)
      if model_changes.empty?
        RequestStore.store[:txns].delete(stored_txn)
      end
    rescue NoMethodError => e
      msg = "Warn: model_changes: #{model_changes.inspect} for update_type: #{update_type.inspect}, stored_txn: #{stored_txn.inspect}"
      Rollbar.warning(e, msg)
    end

    def stored_txn
      RequestStore.store[:txn_id]
    end

    def txn_id
      ApplicationRecord.connection.transaction_id
    end

    def hash_id
      [self.class.name, id].join("_")
    end

    def after_save
      json_changes = saved_changes.to_json

      #      logger.debug "PUBLISHABLE after_save, saving txn transaction: #{RequestStore.store[:txn_id]} new: #{txn_id}"
      changed_txn = false
      if stored_txn && (stored_txn != txn_id)
        changed_txn = true
      end

      RequestStore.store[:txn_id] = txn_id

      if ENV['AR_CHANGES'].present?

        ar = ArChange.create!(callback_name: 'save', target: self, ar_changes: json_changes, ar_previous_changes: previous_changes.to_json,
                              user_id: RequestStore.store[:user_id],
                              request: RequestStore.store[:command].try(:request),
                              txn_id: RequestStore.store[:txn_id],
                              command: RequestStore.store[:command],
                              job_id: changes_job_id)
        if changed_txn
          Rails.logger.debug "PUBLISHABLE txn_changed #{ar.id}"
        end
      end

      store_save saved_changes

      true
    end

    def after_update
      if ENV['AR_CHANGES'].present?
        ArChange.create!(callback_name: 'update', target: self, ar_changes: changes.to_json, ar_previous_changes: previous_changes.to_json,
                         user_id: RequestStore.store[:user_id],
                         request: RequestStore.store[:command].try(:request),
                         txn_id: txn_id,
                         command: RequestStore.store[:command],
                         job_id: changes_job_id)
      end
      true
    end

    def after_destroy
      RequestStore.store[:txn_id] = txn_id

      if ENV['AR_CHANGES'].present?
        ArChange.create!(callback_name: 'destroy', target: self, ar_changes: changes.to_json, ar_previous_changes: previous_changes.to_json,
                         user_id: RequestStore.store[:user_id],
                         request: RequestStore.store[:command].try(:request),
                         txn_id: txn_id,
                         command: RequestStore.store[:command],
                         job_id: changes_job_id)
      end
      true
    end

    def after_commit_create
      #      logger.debug "SM: BaseModel: After create #{self.inspect}"
      #      logger.debug "Changes: #{self.changes}"
      #      logger.debug "Previous Changes: #{self.previous_changes}"
      #      if self.previous_changes.size>0
      #        base_publish("create",self.changes)
      if ENV['AR_CHANGES'].present?
        ArChange.create!(callback_name: 'commit_create', target: self, ar_changes: changes.to_json, ar_previous_changes: previous_changes.to_json,
                         user_id: RequestStore.store[:user_id],
                         request: RequestStore.store[:command].try(:request),
                         txn_id: RequestStore.store[:txn_id],
                         command: RequestStore.store[:command],
                         job_id: changes_job_id)
      end
      #      end
      true
    end

    def after_commit_update
      #      logger.debug "SM: BaseModel: After update #{self.inspect}"
      #      logger.debug "Changes: #{self.changes}"
      #      logger.debug "Previous Changes: #{self.previous_changes}"
      if instance_of?(Job) &&
        (status == "Created")
        raise "WS updating job with Created status #{inspect}"
      end

      #     if self.previous_changes.size >0
      #       base_publish("update",self.changes)

      if ENV['AR_CHANGES'].present?
        ArChange.create!(callback_name: 'commit_update', target: self, ar_changes: changes.to_json, ar_previous_changes: previous_changes.to_json,
                         user_id: RequestStore.store[:user_id],
                         request: RequestStore.store[:command].try(:request),
                         txn_id: RequestStore.store[:txn_id],
                         command: RequestStore.store[:command],
                         job_id: changes_job_id)
      end
      #      end
      true
    end

    def after_commit
      #      logger.debug "store_publish #{hash_id} #{stored_txn}: #{RequestStore.store[:txns]}"

      if transaction_include_any_action?([:create])
        commit_type = :create
      elsif transaction_include_any_action?([:update])
        commit_type = :update
      elsif transaction_include_any_action?([:destroy])
        commit_type = :destroy
      end

      if !(is_a? RescueVehicle)

        request = RequestStore.store[:command].try(:request)
        txn_id = RequestStore.store[:txn_id]
        command = RequestStore.store[:command].try(:id)
        user_id = RequestStore.store[:user_id]

        msg = "ARCHANGE #{Time.now.iso8601(3)} ct:#{commit_type} tt:#{self.class.name} tid:#{id} txn:#{txn_id}"
        if user_id
          msg += " usr:#{user_id}"
        end
        if request
          msg += " rqt:#{request}"
        end
        if command
          msg += " cmd:#{command}"
        end
        if changes_job_id
          msg += " job:#{changes_job_id}"
        end
        msg += " chg:#{saved_changes.to_json}"

        if ENV['LOGSTASH_TOKEN']
          Stash.info(msg)
          # Stash.flush
        end

        if ENV['LOGSTASH_DEBUG_LOG']
          Rails.logger.debug(msg)
        end

      end

      if ENV['AR_CHANGES'].present?

        #        logger.debug "PUBLISHABLE after_commit_rs #{RequestStore.store[:request_id]}"
        #        logger.debug "PUBLISHABLE after_commit_rs #{RequestStore.store[:user_id]}"
        #        logger.debug "PUBLISHABLE after_commit_rs transaction: #{RequestStore.store[:txn_id]}"

        ArChange.create!(callback_name: 'commit', commit_type: commit_type, target: self, ar_changes: changes.to_json, ar_previous_changes: previous_changes.to_json,
                         user_id: RequestStore.store[:user_id],
                         request: RequestStore.store[:command].try(:request),
                         txn_id: RequestStore.store[:txn_id],
                         command: RequestStore.store[:command],
                         job_id: changes_job_id)

        ArChange.where(txn_id: RequestStore.store[:txn_id]).update_all(committed: true)
      end

      if [:create, :update].include?(commit_type)
        store_publish commit_type
      end

      true
    end
  end

end
