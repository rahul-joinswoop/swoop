# frozen_string_literal: true

# this concern contains methods used on RescueVehicle primarily for Auction-related code. these
# are grouped here because of the potential performance impact around has_one_job? / has_no_jobs?,
# see ENG-11899 for more info there.
module RescueVehicleAuction

  extend ActiveSupport::Concern

  METERS_MILES = 0.00062137

  # DO NOT CHANGE #has_one_job? / has_no_jobs? to use exists? / present? / etc - by
  # using #size we can use a cached result set and we guarantee that we only
  # end up making a single query during StartAuctionService.
  def has_one_job?
    current_jobs.size == 1
  end

  def has_no_jobs?
    current_jobs.size == 0
  end

  def current_job
    current_jobs.first if has_one_job?
  end

  # Has current running job, but is sufficiently far through to be eligable for inclusion in a new
  # dispatch algorithm calc
  def has_auto_dispatchable_job?
    has_one_job? && current_job.service_code.can_auto_dispatch? && is_onsite? && (time_onsite / 60) <= 20
  end

  def service_ends_at_service_location?
    has_auto_dispatchable_job? && current_job.service_code.is_service_location_code?
  end

  def service_ends_at_drop_location?
    has_auto_dispatchable_job? && current_job.has_drop_location_service_code?
  end

  def is_onsite?
    if current_job && current_job.has_drop_location_service_code? && current_job.status == Job::STATUS_TOWDESTINATION
      true
    elsif current_job && current_job.service_code.is_service_location_code? && current_job.status == Job::STATUS_ONSITE
      true
    else
      false
    end
  end

  def time_onsite
    current_job.time_at_destination if is_onsite?
  end

  def crowflies_in_miles_to(destination_location)
    Rails.logger.debug("RescueVehicle #{id} - current_job_location lat/lng: #{current_job_location&.lat}/#{current_job_location&.lng}")
    Rails.logger.debug("RescueVehicle #{id} - destination_location lat/lng: #{destination_location&.lat}/#{destination_location&.lng}")

    if current_job_location&.lat && current_job_location&.lng && destination_location&.lat && destination_location&.lng
      distance_in_meters([current_job_location.lat, current_job_location.lng], [destination_location.lat, destination_location.lng]) * METERS_MILES
    end
  end

  # gets the location that the estimate to job site should be from
  # either: current location if the truck isn't busy, or service_location if it is.
  def current_job_location
    @current_job_location ||= begin
      if service_ends_at_service_location?
        location = current_job.service_location
        Rails.logger.debug("RescueVehicle(#{id}) - current_job_location resolving to service location, lat: #{location&.lat}, lng: #{location&.lng}")

        location
      elsif service_ends_at_drop_location?
        location = current_job.drop_location
        Rails.logger.debug("RescueVehicle(#{id}) - current_job_location resolving to drop location, lat: #{location&.lat}, lng: #{location&.lng}")

        location
      else
        location = Location.new({ lat: lat, lng: lng })
        Rails.logger.debug("RescueVehicle(#{id}) - current_job_location resolving to vehicle location, lat: #{location&.lat}, lng: #{location&.lng}")

        location
      end
    end
  end

  #
  # What in_range_of_location? is doing:
  #
  # 1. Loop through all of the sites that this vehicle's rescue company can accept Swoop
  # automatic dispatch jobs from
  # 2. Of those sites, find the shortest distance they'd be willing to go for an auction job
  # 3. Is that distance less than the distance from the starting location to where the tow truck
  # needs to go to, as the crow flies?
  #
  # Essentially: is the distance the tow truck has to drive less than the shortest distance
  # the tow company is willing to go?
  #
  def in_range_of_location?(location:, maximum_distance_allowable:)
    crowmiles = crowflies_in_miles_to(location)
    return false unless crowmiles && crowmiles <= maximum_distance_allowable

    distance_of_closest_rescue_provider = maximum_distance_allowable
    if User.swoop_auto_dispatch.company
      RescueProvider.where(company_id: User.swoop_auto_dispatch.company.id, provider_id: company_id).each do |rp|
        distance_of_closest_rescue_provider = rp.auto_assign_distance_miles if rp.auto_assign_distance_miles < distance_of_closest_rescue_provider
      end
    end
    crowmiles <= distance_of_closest_rescue_provider
  end

  class_methods do
    #
    # Find all available (dispatchable) RescueVehicles within range of
    # a vehicle needing to be towed for an Auction+Job
    #
    def find_all_in_range_of_auction_job(job:, auction:)
      # TODO: get vehicle category array from company specific list

      vehicle_sql = <<~SQL
        SELECT DISTINCT vehicles.*
        FROM companies AS fleet_company
        INNER JOIN rescue_providers ON fleet_company.id = rescue_providers.company_id
        INNER JOIN vehicles ON rescue_providers.provider_id = vehicles.company_id
        LEFT JOIN users ON users.vehicle_id = vehicles.id
        LEFT JOIN companies AS rescue_company ON rescue_providers.provider_id = rescue_company.id
        LEFT JOIN companies_services ON companies_services.company_id = rescue_company.id
        WHERE vehicles.type = 'RescueVehicle'
          AND vehicles.location_updated_at > now() - INTERVAL '5 MINUTES'
          AND rescue_providers.deleted_at IS NULL
          AND rescue_company.live IS true
          AND users.id IS NOT NULL
          AND fleet_company.id = ?
          AND companies_services.service_code_id = ?
        GROUP BY vehicles.id
      SQL

      RescueVehicle
        .find_by_sql([vehicle_sql, job.fleet_company.id, job.service_code_id])
        .to_a
        .select { |rescue_vehicle| rescue_vehicle.in_range_of_location?(location: job.service_location, maximum_distance_allowable: auction.max_candidate_distance) }
    end
  end

end
