# frozen_string_literal: true

module CompanyNames

  extend ActiveSupport::Concern

  # canonical list of company names that are hard coded in our codebase for one reason or another

  AGERO = 'Agero'
  ALLIED_DISPATCH_SOLUTIONS = 'Allied Dispatch Solutions'
  ALLSTATE = 'Allstate'
  CENTURY = '21st Century Insurance'
  DEMO_FLEET_COMPANY = 'Demo Fleet Company' # FleetCompany used as demo
  ENTERPRISE = 'Enterprise'
  ENTERPRISE_FLEET_MANAGEMENT = 'Enterprise Fleet Management'
  FARMERS = 'Farmers Insurance'
  FLEET_RESPONSE = 'Fleet Response'
  GEICO = 'Geico'
  LINCOLN = 'Lincoln'
  LUXE = 'LuxeValet'
  METROMILE = 'Metromile'
  MOBILE_MINI = 'Mobile Mini'
  MOTORCYCLE_TOWING_SERVICES = 'Motorcycle Towing Services, L.C.'
  NATION_SAFE_DRIVERS = 'Nation Safe Drivers'
  QUEST = 'Quest'
  ROAD_AMERICA = 'Road America'
  SILVERCAR = 'Silvercar'
  SWOOP = 'Swoop'
  TANTALUM = 'Tantalum'
  TESLA = 'Tesla'
  TESLA_MOTORS_INC = 'Tesla Motors Inc'
  TURO = 'Turo'
  USAA = 'USAA'
  USAC = 'USAC'

end
