# frozen_string_literal: true

# # #
#
# Our models have the concept of being softly deleted instead of
# being removed from the DB.
#
# This concern is just a scope helper around it, as we have some
# code that should run over not deleted rows only.
#
# e.g. Tag.not_deleted
#
module SoftDelete

  extend ActiveSupport::Concern

  included do
    scope :soft_deleted, -> { where.not(deleted_at: nil) }
    scope :not_deleted, -> { where(deleted_at: nil) }

    def soft_delete!
      update!(deleted_at: Time.zone.now)
    end

    def soft_restore!
      update!(deleted_at: nil)
    end
  end

  def soft_deleted?
    deleted_at.present?
  end

  def not_deleted?
    !soft_deleted?
  end

  # This method is optimized for speed and atomicity
  # - It doesn't interferes with the AR correctness
  # - It doens't call the AR callbacks
  def soft_delete_all(relation)
    relation.update_all(deleted_at: Time.zone.now)
  end
  module_function :soft_delete_all

end
