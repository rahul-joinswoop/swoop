# frozen_string_literal: true

module TwilioConcerns

  extend ActiveSupport::Concern

  # the number our twilio calls come from
  TWILIO_CALL_NUMBER = ENV['TWILIO_CALL_NUMBER']

  # the number(s) our twilio sms messages come from
  TWILIO_SMS_NUMBERS = ENV['TWILIO_SMS_NUMBERS'].to_s.split(/,/).map(&:strip).freeze

  module_function def call_number
    TWILIO_CALL_NUMBER
  end

  # sticky_sms_number guarantees that a given to: number always receives messages
  # from the same from: number (as long as TWILIO_SMS_NUMBERS is the same - if we
  # change the ENV variable then it's possible a to: number will start to get a
  # different number, :shrug:).
  module_function def sticky_sms_number(to)
    TWILIO_SMS_NUMBERS.sample(random: Random.new(to.to_i))
  end

  module ClassMethods

    def send_sms(phone, body, extra = {})
      TwilioMessage.perform_async(phone, body, extra)
    end

    #
    # This call uses a TwiML Bin, which consists of a template stored
    # in Twilio since we need it to process runtime variables.
    #
    # TwiML Bins use mustache.js templates. The current template
    # processes this: "Swoop Job Alert: {{client_name}}, {{scheduled_or_asap}}, {{job_id}}."
    #
    # When we call it, we need to send values for these variables appended in the querystring.
    #
    # An example of a processed message for a phone call that the swoop agent will receive:
    # "Swoop Job Alert: Enterprise Fleet Management, ASAP, 100038."
    #
    # product example for reference:
    # "Swoop Job Alert: [Client name] [scheduled / ASAP] [job ID]"
    #
    # Currently Paul Widden is the only one who can change templates in Twilio.
    #
    # About TwiML Bins:
    # https://support.twilio.com/hc/en-us/articles/230878368-How-to-use-templates-with-TwiML-Bins
    #
    def new_job_call_swoop_agents(phone, job_id)
      job = Job.find(job_id)

      client_name = job.fleet_company.name
      scheduled_or_asap = job.scheduled_for ? 'scheduled' : 'ASAP'

      querystring = {
        job_id: job.id,
        client_name: client_name,
        scheduled_or_asap: scheduled_or_asap,
      }

      uri = Addressable::URI.parse(TwilioCall::NEW_JOB_ALERT_TO_SWOOP_AGENTS_TWILIO_URL)
      uri_with_querystring = URI::HTTPS.build(host: uri.host, query: querystring.to_query, path: uri.path)

      TwilioCall.perform_async(phone, uri_with_querystring)
    end

    def new_job_call(phone)
      TwilioCall.perform_async(phone, TwilioCall::NEW_JOB)
    end

    def dispatcher_not_assigned_to_job_call(phone)
      TwilioCall.perform_async(phone, TwilioCall::DISPATCHER_NOT_ASSIGNED_TO_JOB)
    end

    def not_dispatched_job_call(phone)
      TwilioCall.perform_async(phone, TwilioCall::NOT_DISPATCHED_JOB)
    end

    def scheduled_job_call(phone)
      TwilioCall.perform_async(phone, TwilioCall::SCHEDULED_JOB)
    end

    def new_auction_call(phone)
      TwilioCall.perform_async(phone, TwilioCall::NEW_AUCTION)
    end

    def won_auction_call(phone)
      TwilioCall.perform_async(phone, TwilioCall::WON_AUCTION)
    end

  end

end
