# frozen_string_literal: true

module VirtualStatus

  extend ActiveSupport::Concern

  # when a job has an active auction running it has a status of auto_assigning. during the auction
  # we used to present different statuses to the various rescue_companies bidding on the job based
  # on the bid status. #virtual_status pulls that logic into the back end so that partners using
  # partner api see the status that they should be displaying (rather than "auto_assigning"). currently
  # only used in graphql.
  def virtual_status(company)
    # note that this will always be false if company is a client company
    if company && auto_assigning? && bids.where(company_id: company.id).present?
      bid = bids.where(company_id: company.id).first
      case bid.status
      when "submitted"
        "submitted"
      when "provider_rejected"
        "rejected"
      when "requested"
        "assigned"
      when "auto_rejected"
        "etarejected"
      when "expired"
        "expired"
      when "canceled"
        "canceled"
      else
        status
      end
    else
      status
    end
  end

  def virtual_human_status(company)
    vstatus = virtual_status company
    case vstatus
    when "eta_rejected"
      "ETA Rejected"
    else
      Job.statuses[vstatus] || human_status
    end
  end

end
