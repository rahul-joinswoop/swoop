# frozen_string_literal: true

module GraphQLStats

  extend ActiveSupport::Concern

  NUM_DAYS = 30

  def graphql_requests
    keys = graphql_stats_dates.map do |d|
      "#{Utils::Stats::Queries::KEY}:#{graphql_stats_key}:#{d.strftime('%Y%m%d')}"
    end

    graphql_stats_dates
      .zip(RedisClient.client(:default).mget(*keys))
  end

  def graphql_deprecated_fields
    keys = graphql_stats_dates.map do |d|
      "#{Utils::Stats::DeprecatedFields::KEY}:#{graphql_stats_key}:#{d.strftime('%Y%m%d')}"
    end

    graphql_stats_dates
      .zip(::Swoop.graphql_redis_client.pipelined do |pipeline|
        keys.each do |key|
          pipeline.zrevrange(key, 0, 20, with_scores: true)
        end
      end)
  end

  class GraphQLStatsKeyError < StandardError; end

  private def graphql_stats_key
    case self
    when Doorkeeper::Application
      "application:#{id}"
    when SuperCompany
      "all"
    else
      raise GraphQLStatsKeyError, "GraphQLStats#graphql_stats_key called for unsupported object #{inspect}"
    end
  end

  private def graphql_stats_dates
    # if we start our stats 29 days ago we end up with 30 dates since we include
    # today so subtract one here
    (Date.current - (NUM_DAYS - 1))..Date.current
  end

end
