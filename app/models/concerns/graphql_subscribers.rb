# frozen_string_literal: true

module GraphQLSubscribers

  extend ActiveSupport::Concern

  def add_applications(subscribers)
    subscribers
      .select { |s| s.is_a?(Company) }
      .each { |s| subscribers += s.oauth_applications }
    subscribers
  end

  def add_dispatchers(subscribers)
    subscribers
      .select { |s| s.is_a?(RescueCompany) }
      .each { |s| subscribers += s.dispatchers }
    subscribers
  end

end
