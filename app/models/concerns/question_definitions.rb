# frozen_string_literal: true

# constants used in Question model and elsewhere, defined here to avoid
# circular dependencies between Question, Answer, and ServiceCode

module QuestionDefinitions

  extend ActiveSupport::Concern

  # questions
  ANYONE_ATTEMPTED_TO_JUMP = "Anyone attempted to jump it yet?"
  CHILDREN_OR_PETS = "Children or pets locked inside?"
  CUSTOMER_BEEN_CONTACTED = "Has customer been contacted?"
  CUSTOMER_IN_A_SAFE_LOCATION = "Customer in a safe location?"
  CUSTOMER_WITH_VEHICLE = "Customer with vehicle?"
  GASOLINE_TYPE = "What is the gasoline type?"
  GPS_TRACKING = "GPS tracking on vehicle?"
  HAVE_A_SPARE = "Have a spare in good condition?"
  IS_THE_VEHICLE_ON = "Is the vehicle on or running?"
  KEYS_PRESENT = "Keys present?"
  LOW_CLEARANCE = "Low clearance?"
  VEHICLE_4WD = "Vehicle 4 wheel drive?"
  VEHICLE_CAN_BE_NEUTRAL = "Vehicle can be put in neutral?"
  VEHICLE_DRIVEABLE_AFTER_WINCH_OUT = "Vehicle driveable after winch out?"
  VEHICLE_MOVED_TODAY = "Has vehicle moved today?"
  VEHICLE_STOP_WHEN_RUNNING = "Did the vehicle stop when running?"
  WHERE_ARE_THE_KEYS = "Where are the keys located?"
  WHICH_TIRE = "Which tire is damaged?"
  WILL_CUSTOMER_RIDE = "Will customer ride with tow truck?"
  WITHIN_10_FEET_OF_PAVED_SURFACE = "Within 10 ft of a paved surface?"

  NEUTRAL_CAPABLE_NAME = 'neutral'

end
