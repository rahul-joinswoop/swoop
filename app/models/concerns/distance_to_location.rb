# frozen_string_literal: true

# The including class should implement #lat and #lng.
module DistanceToLocation

  extend ActiveSupport::Concern
  include Utils

  TYPE_ERROR_SUFFIX = '( must respond to :lat and :lng with a Numeric )'

  # location1 = Location.new(lat: 1, lng: 2)
  # location2 = Location.new(lat: 2, lng: 1)
  # location1.distance_to(location2)
  # returns Float
  def distance_to(location)
    if !implements_coordinates?(location)
      raise TypeError, "Wrong argument type #{location.class} #{TYPE_ERROR_SUFFIX}"
    end

    if !implements_coordinates?(self)
      raise TypeError, "DistanceToLocation included on class #{self} #{TYPE_ERROR_SUFFIX}"
    end

    distance_in_meters([lat, lng], [location.lat, location.lng])
  end

  def implements_coordinates?(object)
    return false unless object.respond_to?(:lat)
    return false unless object.respond_to?(:lng)
    return false unless object.lat.is_a?(Numeric)
    return false unless object.lng.is_a?(Numeric)
    true
  end

end
