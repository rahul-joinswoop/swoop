# frozen_string_literal: true

module FleetRescueVehicles

  extend ActiveSupport::Concern
  def fleet_rescue_vehicles
    # returns back a list of all rescue vehicles currently working on jobs for
    # (and therefore visible) to a fleet_company (or super_company in the case
    # of swoop)
    RescueVehicle
      .joins(:jobs)
      .where(deleted_at: nil,
             jobs: {
               status: Job.fleet_show_rescue_vehicle_states,
               fleet_company_id: id,
             })
  end

end
