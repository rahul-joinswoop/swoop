# frozen_string_literal: true

# concern for models that have a lat/lng - uses a postgis point to store the data as
# lnglat (named/ordered that way because that's what the postgis point() method expects
# and mixing these up is a super common occurence).
#
# requires 3 migrations to use on a model, see 20190907185815, 20190907230923, and
# 20190907231201 for the particulars.
#
# eventual goal is to remove the individual lat/lng attributes from the db in favor of the
# single point column.

module LongitudeLatitude

  extend ActiveSupport::Concern

  # based on https://www.cookieshq.co.uk/posts/introduction-to-postgis-and-rails-pt-2
  GEO_FACTORY = RGeo::ActiveRecord::SpatialFactoryStore.instance.factory(geo_type: "point")

  included do
    # keep both of our coordinates stores in sync
    before_save do
      if lng_changed? || lat_changed?
        # if lnglat already exists then we could do self.lnglat.factory.point(...) but when it's
        # nil this doesn't work, hence the reference to the global default for geo_type: 'point'
        # above
        self.lnglat = GEO_FACTORY.point(lng, lat)
      end
    end
  end

  # used for google distance matrix
  def latlng
    "#{lat},#{lng}"
  end

  def has_coords?
    lat.present? && lng.present?
  end

  def to_coordinates
    [lat, lng] if has_coords?
  end

  def timezone
    return nil unless has_coords?

    tz = External::GoogleTimezoneService.new(lat, lng).call

    ActiveSupport::TimeZone[(tz || 'UTC')]
  end

  # Return true if the geocoordinates are close enough. Can't do an exact match due to
  # floating point rounding errors. In case anyone's too concerned, 1 millionth of a
  # degree of latitude is about 4 inches.
  def matches?(latitude:, longitude:)
    if lat.present? && lng.present? && latitude.present? && longitude.present?
      lat.round(6) == latitude.to_f.round(6) && lng.round(6) == longitude.to_f.round(6)
    else
      false
    end
  end

end
