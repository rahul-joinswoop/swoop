# frozen_string_literal: true

module HistoryItemTarget

  extend ActiveSupport::Concern

  included do
    has_many :target_history_items, class_name: "HistoryItem", as: :target, inverse_of: :target, autosave: true
  end

  def has_history_items?
    target_history_items.exists?
  end

end
