# frozen_string_literal: true

module ValidateCurrency

  extend ActiveSupport::Concern

  class_methods do
    def validate_currency_matches(*related_models)
      related_models.each do |related_model|
        method_name = "currency_matches_#{related_model}".to_sym

        validate method_name

        define_method(method_name) do
          return unless new_record? || currency_changed?

          related = send(related_model)
          return unless related.respond_to?(:currency)

          if related && related.currency != currency
            errors.add(:currency, "must match #{related_model.to_s.camelcase}")
          end
        end

        private method_name

        inherit_currency_from(related_model)
      end
    end

    def inherit_currency_from(related_model)
      method_name = "inherit_currency_from_#{related_model}".to_sym

      before_validation method_name

      define_method(method_name) do
        related = send(related_model)
        return unless related.respond_to?(:currency)

        self.currency ||= related.currency
      end

      private method_name
    end
  end

end
