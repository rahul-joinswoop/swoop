# frozen_string_literal: true

module JobHistoryLabelConcern

  extend ActiveSupport::Concern

  #
  # Builds UI Label for Job History items.
  #
  # It currently supports instances of AuditJobStatus and HistoryItem,
  # and requires a viewing_company.
  #
  def ui_label(viewing_company:)
    JobHistory::Label.new(self, viewing_company).ui_label
  end

end
