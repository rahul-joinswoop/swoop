# frozen_string_literal: true

module StandardOrCustomCollection

  extend ActiveSupport::Concern

  included do
    scope :standard_items, -> {
      if column_names.include? :deleted_at.to_s
        where(is_standard: true, deleted_at: nil)
      else
        where(is_standard: true)
      end
    }

    scope :custom_items, -> {
      if column_names.include? :deleted_at.to_s
        where(is_standard: [false, nil], deleted_at: nil)
      else
        where(is_standard: [false, nil])
      end
    }
  end

end
