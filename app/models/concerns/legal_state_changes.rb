# frozen_string_literal: true

module LegalStateChanges

  extend ActiveSupport::Concern

  # these are the possible prefixes to our events
  SERVICE_TYPES = ['partner', 'fleet', 'fleet_managed', 'swoop', 'issc'].freeze

  # these are events that we don't want callers to think they can use. it's not exhaustive, i'm sure there are more
  # to add?
  HIDDEN_STATE_CHANGES = [
    # we don't ever want this presented as an option api users
    'auto_assign',
    # this is called from the DispatchJob service
    'original_store_vehicle',
    # we have a mutation for requesting a callback, we don't need to expose this directly here, especially
    # since it doesn't actually modify the status
    'partner_request_callback',
    # this is called from FleetJob#partner_set_driver
    'partner_fleet_set_driver',
    # this is called from FleetJob#partner_remove_driver
    'partner_fleet_remove_driver',
    'partner_partner_remove_driver',
    'partner_set_driver',
    # this is called directly from several other services
    'store_vehicle',
  ].freeze

  included do
    # need to do this in included since statuses doesn't exist yet
    ALL_JOB_STATUSES = statuses.keys.map(&:to_s)
  end

  # this is a list of events that a given viewer can call to change the state of a job. it's still
  # possible some of these wouldn't be allowed because of other code but this is a subset of possibilities.

  # if viewer is invalid or doesn't have correct permissions nil is returned.
  # otherwise a list of statuses is returned (this could be an empty list)
  def legal_state_changes(viewer)
    # bail if we didn't get a valid viewer
    return nil if viewer.blank?

    service_type = get_service_type_from_viewer(viewer)

    # bail if we didn't get a service_type back
    return nil if service_type.blank?

    unwanted_service_types = SERVICE_TYPES - [service_type]

    aasm.events
      .map(&:name)
      .map(&:to_s)
      .reject { |s| HIDDEN_STATE_CHANGES.include? s }
      .reject { |s| s.starts_with?(*unwanted_service_types) }
      .map { |s| s.sub(/^#{service_type}_/, '') }
      .map { |s| s.include?('pending') ? 'pending' : s }
      .select { |s| s.in? ALL_JOB_STATUSES }
      .uniq
  end

  private def get_service_type_from_viewer(viewer)
    case viewer
    when Doorkeeper::Application
      if viewer.scopes.to_a.include?('fleet')
        'fleet'
      elsif viewer.scopes.to_a.include?('rescue')
        'partner'
      else
        Rollbar.error "LegalStateChanges#get_service_type_from_viewer: Fell through with Doorkeeper::Application(id=#{viewer.id})"
        ''
      end
    when SuperCompany
      'swoop'
    when FleetCompany
      'fleet'
    when RescueCompany
      'partner'
    when User
      if viewer.has_role?(:fleet)
        'fleet'
      elsif viewer.has_role?(:rescue)
        'partner'
      elsif viewer.has_role?(:root)
        'swoop'
      else
        Rollbar.error "LegalStateChanges#get_service_type_from_viewer: Fell through with User(id=#{viewer.id})"
        ''
      end
    else
      Rollbar.error "LegalStateChanges#get_service_type_from_viewer: Fell through with Viewer(#{viewer.inspect})"
      ''
    end
  end

end
