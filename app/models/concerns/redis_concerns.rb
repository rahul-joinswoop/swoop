# frozen_string_literal: true

require 'asyncredis'

module RedisConcerns

  extend ActiveSupport::Concern
  module ClassMethods

    def publish(channel, msg)
      # logger.debug "REDIS Concern publishing #{msg} to #{channel}"
      AsyncRedis.instance.publish(channel, msg)
    end

  end

end
