# frozen_string_literal: true

#
# This is added to all base_models as a convenience to accessing quereies based on the permissions table
#
# Job.pread(user_or_company) => AR query of all jobs readable by user_or_company
#

module PermissionsConcerns

  extend ActiveSupport::Concern

  included do
    def self.pread(accessor)
      Permissions::Service.read(self, accessor)
    end
  end

end
