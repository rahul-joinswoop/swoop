# frozen_string_literal: true

# This is to support backward compatibility of nested classes
# such as AutoAssign::AutoAssignCandidate, AutoAssign::SiteCandidate,
# AutoAssign::TruckCandidate, AutoAssign::Assignment etc.
#
# This will be removed in ENG-9792 after the new manual assign is live on prod
# and all database references to AutoAssign:: have been migrated out of the database.
# The new classes are residing under app/models/job

module AutoAssign; end

AutoAssign::AutoAssignCandidate = Job::Candidate
AutoAssign::SiteCandidate = Job::Candidate::Site
AutoAssign::TruckCandidate = Job::Candidate::Truck
AutoAssign::Assignment = Job::Candidate::Assignment

AutoAssign::StatusCheck = Job::Candidate::StatusCheck
AutoAssign::AutoAssignCandidateJob = Job::Candidate::CandidateJob
