# frozen_string_literal: true

class CompanyJobStatusReasonType < ApplicationRecord

  belongs_to :company
  belongs_to :job_status_reason_type

  validates :company, uniqueness: { scope: :job_status_reason_type }
  # The logic here was originally added to FE, from where it has been copied over:
  # https://github.com/joinswoop/swoop/blob/c28c879533c00eac4ae26812362da5ac9969360a/app/assets/javascripts/components/modals/DriverMapModal/ETAInputContainer.js#L178-L182
  #
  # We should think about using a table for it (CompanyJobStatusReasonType? :/ )
  # But for now only 3 companies need this customization and
  # it's only for Accepted JobStatus, so I'm setting it here.

  # TODO - ACCEPTED should be JobStatus::ACCEPTED
  KEYS_RESTRICTED_BY_COMPANY = {
    AGO: {
      ACCEPTED: [
        :accepted_none,
        :accepted_traffic,
        :accepted_weather,
      ],
      CANCELED: [
        :cancel_another_job_priority,
        :cancel_customer_found_alternate_solution,
        :cancel_no_reason_given,
        :cancel_out_of_area,
        :cancel_prior_job_delayed,
        :cancel_traffic_service_vehicle_problem,
      ],
      GOA: [
        :goa_customer_cancel_after_deadline,
        :goa_customer_not_with_vehicle,
        :goa_incorrect_equipment,
        :goa_incorrect_service,
        :goa_unsuccessful_service_attempt,
        :goa_wrong_location_given,
      ],
      REJECTED: [
        :rejected_dont_accept_payment_type,
        :rejected_no_longer_offer_service,
        :rejected_other,
        :rejected_out_of_coverage_area,
        :rejected_proper_equipment_not_available,
      ],
    },
    NSD: {
      REJECTED: [
        :rejected_equipment_not_available,
        :rejected_out_of_service_area,
        :rejected_payment_issue,
        :rejected_restricted_roadway,
      ],
    },
    QUEST: {
      ACCEPTED: [
        :accepted_construction,
        :accepted_other,
        :accepted_rural_disablement,
        :accepted_service_provider_issue,
        :accepted_traffic,
        :accepted_weather,
      ],
      REJECTED: [
        :rejected_equipment_not_available,
        :rejected_out_of_service_area,
        :rejected_service_not_available,
        :rejected_weather,
      ],
    },
    # lol not the same as TSLA below - the below is for issc jobs, this one
    # is for who fucking knows.
    TESLA: {
      REJECTED: [
        :rejected_more_than_60_mins,
        :rejected_not_interested,
        :rejected_other,
        :rejected_out_of_service_area,
        :rejected_tire_size,
      ],
    },
    TSLA: {
      REJECTED: [
        :rejected_equipment_not_available,
        :rejected_other,
        :rejected_out_of_service_area,
        :rejected_traffic,
        :rejected_weather,
      ],
    },
    USAC: {
      REJECTED: [
        :rejected_equipment_not_available,
        :rejected_out_of_service_area,
        :rejected_payment_issue,
        :rejected_restricted_roadway,
      ],
    },
  }.freeze

  # initialize all the required seed records - only used by seed migrations when we add values above.
  def self.init_all
    KEYS_RESTRICTED_BY_COMPANY.keys.map { |issc_client_id| init_by_client_id(issc_client_id) }
  end

  def self.init_by_client_id(issc_client_id)
    company = if issc_client_id == :TESLA
                Company.tesla
              else
                FleetCompany.find_by!(issc_client_id: issc_client_id, deleted_at: nil)
              end
    statuses = KEYS_RESTRICTED_BY_COMPANY[issc_client_id]
    statuses.each do |status, reasons|
      job_status_id = JobStatus.const_get status
      reasons.each do |reason|
        jsrt = JobStatusReasonType.find_by(key: reason, job_status_id: job_status_id)
        find_or_create_by! job_status_reason_type: jsrt, company: company
      end
    end
  end

end
