# frozen_string_literal: true

class TrailerVehicle < Vehicle

  belongs_to :vehicle_category
  has_many :jobs

  attr_accessor :location_update

  validates_presence_of :name

  def description
    "#{name}"
  end

  def update_name(name)
    self.name = name
  end

  def update_number(num)
    num = num.tr('#', '')
    if num.is_i?
      self.number = num.to_i
    else
      self.number = nil
    end
  end

  def number_default(default)
    if number
      ret = number
    else
      ret = default
    end
    ret
  end

end

class String

  def is_i?
    /\A[-+]?\d+\z/ === self
  end

end
