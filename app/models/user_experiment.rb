# frozen_string_literal: true

#
# This table is depricated and replaced by TargetVariant
#
class UserExperiment < ApplicationRecord

  belongs_to :experiment
  belongs_to :user
  belongs_to :variant
  belongs_to :job

end
