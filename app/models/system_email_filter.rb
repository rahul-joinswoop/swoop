# frozen_string_literal: true

class SystemEmailFilter < BaseModel

  belongs_to :user
  belongs_to :company

  scope :by_company, ->(company) { where(company: company) }

  validates :user, :company, presence: true
  validates :user, uniqueness: { scope: :company }

end
