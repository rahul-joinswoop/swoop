# frozen_string_literal: true

#
# Represents a physical location owned by a company. This is a superclass,
# and should be extended by actual subtypes. (it uses single table inheritance)
#
class Site < BaseModel

  include SoftDelete

  self.ignored_columns = %w(_x_name ci_name)

  scope :all_not_deleted_by_company, lambda { |company|
    where(deleted_at: nil, company: company).order(created_at: :desc)
  }

  scope :dispatchable, -> { where(dispatchable: true) }

  validates :company, presence: true
  #  validates :tz, presence: true
  validates :name, presence: true
  validates_uniqueness_of :name, scope: [:company_id, :type], conditions: -> { not_deleted }, if: -> { not_deleted? }
  validates_uniqueness_of :site_code, scope: [:company_id, :type], conditions: -> { not_deleted }, if: -> { not_deleted? }, allow_nil: true

  belongs_to :location
  belongs_to :company
  belongs_to :manager, class_name: "User"
  belongs_to :owner_company, class_name: "Company"
  has_many :rescue_providers

  accepts_nested_attributes_for :location

  before_validation :normalize_phone

  after_commit :force_open_check!, if: :time_fields_changed
  after_commit :publish_site, on: :update
  after_commit :send_dispatchable_changed_notification, on: :update
  after_commit :update_dependent_indexes, on: :update

  HQ = 'HQ'
  DEFAULT_TIME_ZONE = "America/Los_Angeles"

  def listeners
    ret = Set.new
    ret << company
    ret << Company.swoop
    ret
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/sites/_site", locals: { site: self }))
  end

  def publish_site
    RescueProvider.where(site: self).each do |provider|
      provider.base_publish("update")
    end
  end

  # TODO method name is wrong; return value is companies and they are not rescue providers.
  def providerCompanyNames
    rescue_providers.reject(&:deleted_at).map(&:company).uniq.select { |c| c.super? || c.fleet_in_house? || c.issc_motor_club? }
  end

  def localtime
    Time.find_zone(tz || DEFAULT_TIME_ZONE).now
  end

  def open_for_business?
    open_checker.open_for_business?
  end

  def schedule_google_timezone
    delay.lookup_and_save_tz
  end

  def match_zip?(zip)
    location.try(:zip).present? && (zip == location.zip || zip.slice(0, 5) == location.zip.slice(0, 5))
  end

  def name_with_company
    "#{company.name} #{name}"
  end

  def open_right_now?
    open_checker.open_right_now?
  end

  def compute_next_open_check!
    open_checker.compute_next_open_check!
  end

  private

  def open_checker
    Site::OpenChecker.new(self)
  end

  def force_open_check!
    update_columns(next_open_check: nil)
  end

  def time_fields_changed
    Site::OpenChecker::TIME_FIELDS.any? { |field| saved_change_to_attribute?(field) }
  end

  def send_dispatchable_changed_notification
    if previous_changes[:dispatchable] && !dispatchable
      Delayed::Job.enqueue(EmailDispatchableAdminNotification.new(User.root_users.pluck(:id), site_id: id, rescue_provider_id: nil))
    end
  end

  def normalize_phone
    self.phone = Phonelib.parse(phone).e164 if phone_changed?
  end

  def lookup_and_save_tz
    google_timezone
    save!
  end

  def google_timezone
    loc = location
    if loc && loc.lat && loc.lng
      self.tz = External::GoogleTimezoneService.new(loc.lat, loc.lng).call
      logger.debug "#{inspect}"
    else
      logger.debug "Not doing tz lookup, missing location or location missing lat or lng"
    end
  end

  # Update search indexes that reference this record.
  def update_dependent_indexes
    if saved_change_to_name?
      SearchIndex::SiteDependentIndexUpdateWorker.perform_async(id)
    end
  end

end
