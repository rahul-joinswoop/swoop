# frozen_string_literal: true

class EndUserInvoice < Invoice

  belongs_to :recipient, class_name: 'User'

end
