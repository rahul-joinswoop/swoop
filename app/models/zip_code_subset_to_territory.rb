# frozen_string_literal: true

# This model is responsible for connecting with zip_code_subset_to_territories table.
# This is used in creating Territories for a ZipCode or subset of zipcodes.
class ZipCodeSubsetToTerritory < ApplicationRecord

  include TerritoryConcerns

  validates :polygon_area, presence: true
  validates :zip, presence: true

end
