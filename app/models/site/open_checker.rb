# frozen_string_literal: true

class Site
  class OpenChecker

    attr_reader :site

    # A list of time fields. Any changes to these should force the site to check within_hours on next
    # update_open_businesses! check
    TIME_FIELDS = [
      :open_time,
      :close_time,
      :open_time_sat,
      :close_time_sat,
      :open_time_sun,
      :close_time_sun,
      :tz,
      :always_open,
      :force_open,
    ].freeze

    def initialize(site)
      @site = site
      @localtime = site.localtime
    end

    # This updates the flag for open businesses
    def self.update_open_businesses!
      # find sites with normal hours (not always open) and where a check is due
      out_of_date = PartnerSite.not_deleted.where('next_open_check <= ? OR next_open_check IS NULL', Time.current).where(always_open: false)
      # find sites where always_open is inconsistent with within_hours
      incorrect_always_open = PartnerSite.not_deleted.where(always_open: true, within_hours: false)

      # combine the two queries
      out_of_date.or(incorrect_always_open).in_batches(of: 100) do |sites|
        changed = []
        # Wrap in a transaction to ease the number of calls we're making
        sites.transaction do
          sites.each do |site|
            # Use a specific instance of OpenChecker to prevent the time changing on us
            site_checker = new(site)

            # Compute next update check and apply changes
            site_checker.compute_next_open_check!

            # Check if the site should currently be open and change its state
            if site_checker.open_right_now? != site.within_hours?
              site.update_columns(within_hours: !site.within_hours, force_open: false)
              changed << site
            end
          end
        end

        changed.each { |site| site.base_publish("update") }
      end
    end

    def open_for_business?
      site.force_open? || open_right_now?
    end

    # This intended more as an internal method
    # allow optional param to compute at time (keep update_open_businesses! atomic)
    def open_right_now?
      if site.always_open?
        true
      else
        time_buffer.any? do |hours|
          (hours[:open]...hours[:close]).cover?(localtime)
        end
      end
    end

    # Compute and set the next check time
    # @param at time to check at (keep update_open_businesses! atomic)
    def compute_next_open_check!
      if site.always_open?
        # Since always open sites have particular logic to check them, don't assign
        # a next open check
        site.update_columns(next_open_check: nil)
      else
        # include default value in future times
        future_times = [1.day.from_now]
        time_buffer do |times|
          times.each_value do |time|
            future_times << time if time > localtime
          end
        end

        # Get the next closest update time and make sure it's in UTC
        site.update_columns(next_open_check: future_times.min.utc)
      end
    end

    private

    attr_reader :localtime

    # Get a buffer of 1 day around the current time for computation purposes. Only include present times.
    # Include lower and upper options in case we ever need to compute further into the past/future
    def time_buffer(lower: -1, upper: 1)
      if block_given?
        lower.upto(upper).each do |offset|
          hours = open_hours_on_day(localtime + offset.days)
          yield hours if hours
        end
      else
        enum_for(__method__, lower: lower, upper: upper)
      end
    end

    # Get the open and close times on the day of the check_time. Assumed in the same timezone
    def open_hours_on_day(check_time)
      # Logic for handling where the data is stored
      hours = open_close_times_on_day(check_time)
      opens_at = hours[:open]
      closes_at = hours[:close]

      # only return a values if we have an opens and closes time
      if opens_at.present? && closes_at.present?
        # Change to the current time window by making a copy based on check_time by modifying the
        # relavent hour and minute
        opens_at = check_time.change(hour: opens_at.hour, minute: opens_at.min)
        closes_at = check_time.change(hour: closes_at.hour, minute: closes_at.min)
        closes_at += 1.day if closes_at < opens_at

        { open: opens_at, close: closes_at }
      end
    end

    def open_close_times_on_day(check_time)
      # In the future, it could make sense to store these times in a JSON column in postgres to support
      # multiple days of the week. as it stands, there is no specific request for it, so accept the issues
      # of dealing with postgres times for the time being.
      if check_time.saturday?
        { open: site.open_time_sat, close: site.close_time_sat }
      elsif check_time.sunday?
        { open: site.open_time_sun, close: site.close_time_sun }
      else
        { open: site.open_time, close: site.close_time }
      end
    end

  end
end
