# frozen_string_literal: true

class StorageType < ApplicationRecord

  include SoftDelete

  belongs_to :company
  has_many :jobs

end
