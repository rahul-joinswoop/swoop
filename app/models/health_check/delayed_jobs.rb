# frozen_string_literal: true

module HealthCheck
  class DelayedJobs

    def initialize(verbose = false)
      @verbose = verbose
    end

    def status
      case count
      when 0...100
        :green
      when 100...500
        :yellow
      else
        :red
      end
    end

    def count
      tasks.count
    end

    def to_json
      hash = {
        count: count,
        status: status,
      }

      hash.merge!(tasks: tasks.map(&:id)) if @verbose

      hash
    end

    private

    def tasks
      Delayed::Job.where('run_at < ?', 1.minute.from_now)
    end

  end
end
