# frozen_string_literal: true

module HealthCheck
  class Sidekiq

    def initialize(show_queues = false)
      @show_queues = show_queues
    end

    def count
      queues.sum { |q| q[:size] }
    end

    def status
      case count
      when 0...500
        :green
      when 500...5000
        :yellow
      else
        :red
      end
    end

    def to_json
      hash = {
        count: count,
        status: status,
      }

      hash.merge!(queues: queues) if @show_queues

      hash
    end

    private

    def queues
      ::Sidekiq::Queue.all.map { |q| { name: q.name, size: q.count } }
    end

  end
end
