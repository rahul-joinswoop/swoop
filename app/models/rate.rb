# frozen_string_literal: true

class CannotCalculateRateWithoutInvoicableError < ArgumentError; end

class Rate < BaseModel

  class InvoiceCannotBeBlankError < StandardError; end
  class AddonCannotBeBlankError < StandardError; end

  include Utils
  include SoftDelete
  include ValidateCurrency

  attr_accessor :no_invoice_updates
  attr_accessor :no_mail

  validates :type, presence: true
  validates :company, presence: true
  validate_currency_matches :company

  belongs_to :company
  belongs_to :service_code
  belongs_to :account
  belongs_to :site
  belongs_to :fleet_company, class_name: "Company"
  belongs_to :vehicle_category

  has_many :additions, -> { where deleted_at: nil }, class_name: "RateAddition"

  after_commit :mail_rate_update_to_fleet, on: :update
  after_commit :mail_rate_update_to_fleet, on: :create

  after_commit :update_invoices_for_change, on: :update
  after_commit :update_invoices_for_create, on: :create

  accepts_nested_attributes_for :additions

  PRICE_ATTRIBUTES = [:hookup, :miles_towed, :miles_towed_free, :miles_enroute, :miles_enroute_free, :hourly, :flat, :miles_p2p, :miles_p2p_free].freeze

  TAX = "Tax"
  MILES_TOWED = "Towed Per Mile"
  KILOMETERS_TOWED = 'Towed Per Kilometer'
  FREE_MILES_TOWED = "Towed Free Miles"
  FREE_KILOMETERS_TOWED = 'Towed Free Kilometers'
  EN_ROUTE_MILES = "En Route Per Mile"
  EN_ROUTE_KILOMETERS = 'En Route Per Kilometer'
  FREE_EN_ROUTE_MILES = "En Route Free Miles"
  FREE_EN_ROUTE_KILOMETERS = 'En Route Free Kilometers'
  MILES_DEADHEAD = "Deadhead Per Mile"
  KILOMETERS_DEADHEAD = 'Deadhead Per Kilometer'
  FREE_MILES_DEADHEAD = "Deadhead Free Miles"
  FREE_KILOMETERS_DEADHEAD = 'Deadhead Free Kilometers'
  MILES_PORT_TO_PORT = "Port To Port Miles"
  KILOMETERS_PORT_TO_PORT = 'Port To Port Kilometers'
  PORT_TO_PORT_HOURS = "Port To Port Hours"
  HOOKUP = "Hookup"
  SERVICE_FEE = "Service Fee"
  STORAGE = "Storage"
  MINIMUM = "Minimum"
  GOA = "GOA"

  # Each concrete Rate subclass should implement this method
  # and return an Array of line items so that an Invoice can
  # calculate a rate
  def calculate_line_items(invoicable)
    raise UncallableAbstractParentMethodError
  end

  # PDW TODO use li.rate to identify which invoices need update possibly using sql from RecalculateLiveRatesBatch
  def update_invoices_for_change
    if !no_invoice_updates
      if account
        Job.schedule_rate_change_invoice_updates(account, self)
      end
    end
  end

  def update_invoices_for_create
    if !no_invoice_updates
      logger.debug "update_invoices_for_create - called"
      if account
        Job.schedule_rate_create_invoice_updates(account, self)
      end
    end
  end

  def listeners
    ret = Set.new
    if fleet_company
      ret << fleet_company
    else
      FleetCompany.all.each do |company|
        ret << company
      end
    end
    ret << self.company
    ret << Company.swoop
    ret
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/rates/_rate", locals: { rate: self }))
  end

  def self.sum_line_items(lis)
    total = 0
    if lis
      lis.each do |li|
        if li
          total += li[:net_amount].to_f + li[:tax_amount].to_f
        end
      end
    end
    total
  end

  def self.remove_all_but_tax(lis)
    (0..lis.length - 1).reverse_each do |i|
      if lis[i][:description] != TAX
        lis.delete_at(i)
      end
    end
  end

  def self.best_rate(rates, live = true)
    logger.debug "finding best rates from RATES(#{rates.pluck(:id).join(',')})"

    highest = 0
    ret = []
    rates.each do |rate|
      logger.debug "found RATE(#{rate.id}) with priority=#{rate.priority}"
      if rate.priority > highest
        highest = rate.priority
      end
      if rate.priority == highest
        ret << rate
      end
    end

    if ret.length == 0
      logger.debug "No Valid rate found from RATES(#{rates.pluck(:id).join(',')})"
      return nil
    elsif (ret.length > 1) && live
      raise ArgumentError, "Found multiple live rates for RATES(#{rates.pluck(:id).join(',')})"
    end
    ret[0]
  end

  def self.inherit(company_id, account_id, site_id, vendor_id, service_code_id, vehicle_category_id, create_if_missing = true)
    rates = Rate.inherit_all(company_id, account_id, site_id, vendor_id, service_code_id, vehicle_category_id, nil, true, create_if_missing)
    Rate.best_rate(rates)
  end

  def self.inherit_type(company_id, account_id, site_id, vendor_id, service_code_id, vehicle_category_id, type, create_if_missing = true)
    rates = Rate.inherit_all(company_id, account_id, site_id, vendor_id, service_code_id, vehicle_category_id, type, nil, create_if_missing)
    Rate.best_rate(rates, false)
  end

  def calc_priority
    priority = 0

    if !vehicle_category_id.nil?
      priority += 1
    end
    if !service_code_id.nil?
      priority += 2
    end
    if !site_id.nil?
      priority += 4
    end
    if !vendor_id.nil?
      priority += 8
    end
    if !account_id.nil?
      priority += 16
    end

    priority
  end

  #
  # Build the additional line_item for the given invoice and service addon rate. If there's no rate
  # for the service addon, it will build its respective line_item with
  # qty 0, net_total 0 and net_amount 0.
  #
  # params:
  # - invoice: from where base info will be taken to search for a rate
  # - addon: the addon item from where the line_item will be built
  #
  # tl;dr
  #
  # We have 2 different concepts for addition_item:
  #
  # - 1st case: when user adds an additional item to be automatically added to a Rate in UI Rates table,
  #   (UI field it refers to: AUTOMATICALLY ADD ITEM TO INVOICE),
  #   it's stored in the RateAddition DB table, and can be feched by rate.additions as it's an assocition.
  #
  # - 2nd case: if user wants to setup an 'Additional Item' in the Services column in UI Rates table to be used when user
  #   manually adds an item to the Invoice, then it is *actually stored as a FlatRate type* in the DB
  #   (and not a RateAddition type), and this FlatRate is associated with the specific ServiceCode set as addon = true in
  #   in the ServiceCode DB table.
  #
  # This method cares about the 2nd case: LineItem additions that can be manually added by users in the UI,
  # therefore the ones that are FlatRates in the DB.
  #
  # The query is inspired in rates_controller/set_account_service_index private method, and FE code for InvoiceForm.
  #
  # This is primarily used by GraphQL to expose the available additional line_items
  # through InvoiceType.flat_additional_item field.
  #
  def self.build_additional_line_item_for(invoice:, addon:)
    raise InvoiceCannotBeBlankError if invoice.blank?
    raise AddonCannotBeBlankError if addon.blank?

    #
    # step 1 - search for the flat_additional_item (note: it may not exist)
    #
    # If we reuse the inherit methods, their priority system used in the SQL
    # screws the result, so we use to use a specific query
    #
    flat_additional_item = Rate.where(
      company_id: invoice.sender.id,
      service_code_id: addon.id,
    ).where('account_id = ? OR account_id IS NULL', invoice.job.account_id)
      .where('site_id = ? OR site_id IS NULL', invoice.job.site_id)
      .where('vendor_id = ? OR vendor_id IS NULL', invoice.job.issc_contractor_id)
      .not_deleted.order(:account_id, :site_id, :vendor_id, :vehicle_category_id).first

    #
    # step 2 - set the original_amount to be used. Use flat_additional_item.flat value, or 0
    #          if flat_additional_item does not exist
    original_amount = flat_additional_item&.flat || 0

    #
    # step 3 - build the line_item object
    #
    InvoicingLineItem.new(
      calc_type: "flat",
      description: addon.name,
      job_id: invoice.job.id,
      tax_amount: 0,
      estimated: false,
      unit_price: original_amount,
      original_unit_price: original_amount,
      original_quantity: 1,
      quantity: 1,
      currency: invoice.currency,
      user_created: true,
      version: Invoice::VERSION,
    )
  end

  def self.inherit_all(company_id, account_id, site_id, vendor_id, service_code_id, vehicle_category_id, type, live, create_if_missing)
    Rails.logger.debug("Rate.inherit_all working on company(#{company_id}) account(#{account_id}) site(#{site_id}) " \
      "vendor_id(#{vendor_id}) service(#{service_code_id}) vehicle_category(#{vehicle_category_id}) type(#{type}) live(#{live})")

    if !(company_id && account_id && service_code_id)
      raise ArgumentError, "null passed to inherit: #{{
        company: company_id,
        account: account_id,
        service: service_code_id,
      }}"
    end

    if type
      sql_args = [
        "select (case when vehicle_category_id is not null then 1 else 0 end) +
                                (case when service_code_id is not null then 2 else 0 end) +
                                (case when vendor_id is not null then 4 else 0 end) +
                                (case when site_id is not null then 8 else 0 end) +
                                (case when account_id is not null then 16 else 0 end) +
                                (case when type = ? then 32 else 0 end)
                                as priority,* from rates
                         where company_id = ? and
                               (account_id = ? or account_id is null) and
                               (site_id = ? or site_id is null ) and
                               (vendor_id = ? or vendor_id is null ) and
                               (service_code_id = ? or service_code_id is null) and
                               (vehicle_category_id = ? or vehicle_category_id is null) and
                               (type = ?) and
                               deleted_at is null
                         order by priority desc", type, company_id, account_id, site_id, vendor_id, service_code_id, vehicle_category_id, type,
      ]
    elsif live
      sql_args = [
        "select (case when vehicle_category_id is not null then 1 else 0 end) +
                                (case when service_code_id is not null then 2 else 0 end) +
                                (case when vendor_id is not null then 4 else 0 end) +
                                (case when site_id is not null then 8 else 0 end) +
                                (case when account_id is not null then 16 else 0 end)
                                as priority,* from rates
                         where company_id = ? and
                               live = ? and
                               (account_id = ? or account_id is null) and
                               (site_id = ? or site_id is null ) and
                               (vendor_id = ? or vendor_id is null ) and
                               (service_code_id = ? or service_code_id is null) and
                               (vehicle_category_id = ? or vehicle_category_id is null) and
                               deleted_at is null
                         order by priority desc", company_id, live, account_id, site_id, vendor_id, service_code_id, vehicle_category_id,
      ]
    else
      sql_args = [
        "select (case when vehicle_category_id is not null then 1 else 0 end) +
                                (case when service_code_id is not null then 2 else 0 end) +
                                (case when vendor_id is not null then 4 else 0 end) +
                                (case when site_id is not null then 8 else 0 end) +
                                (case when account_id is not null then 8 else 0 end)
                                as priority,account_id,site_id,service_code_id,vehicle_category_id,type,* from rates
                         where company_id = ? and
                               (account_id = ? or account_id is null) and
                               (site_id = ? or site_id is null ) and
                               (vendor_id = ? or vendor_id is null ) and
                               (service_code_id = ? or service_code_id is null) and
                               (vehicle_category_id = ? or vehicle_category_id is null) and
                               deleted_at is null
                         order by priority desc", company_id, account_id, site_id, vendor_id, service_code_id, vehicle_category_id,
      ]
    end
    rates = Rate.find_by_sql(sql_args)

    if rates.empty? && create_if_missing
      Rate.create!(type: (type || 'FlatRate'),
                   company_id: company_id,
                   live: live,
                   miles_p2p: 0,
                   miles_p2p_free: 0,
                   hourly: 0,
                   hookup: 0,
                   miles_towed: 0,
                   miles_towed_free: 0,
                   miles_enroute: 0,
                   miles_enroute_free: 0,
                   special_dolly: 0,
                   gone: 0,
                   storage_daily: 0,
                   flat: 0)
      rates = Rate.find_by_sql(sql_args)
    end

    rates
  end

  def estimate(job, invoice_id)
    raise ArgumentError, "estimate called on base type - no estimate for this job: #{job.id}"
  end

  def mail_rate_update_to_fleet
    if !no_mail
      logger.debug "called mail_rate_update_to_fleet #{previous_changes.keys}"
      if (previous_changes.keys & PRICE_ATTRIBUTES).any? # => true

        users = Company.swoop.users.where(rate_change_email_alert: true)
        if fleet_company
          users += fleet_company.users.where(rate_change_email_alert: true)
        else
          logger.debug "no fleet company #{inspect}"
        end
        Delayed::Job.enqueue EmailRateChange.new(id, users.map(&:id))
      end
    end
  end

  def mail_attributes
    a = {
      "Rate ID": id,
      "Vendor": company.name,
      "Service": service_code.name,
      "Rate": type,
    }

    if site && (site.name != company.name)
      a["Site"] = site.name
    end
    a
  end

  def base_tax
    {
      description: TAX,
      net_amount: 0,
      tax_amount: 0,
      estimated: true,
      unit_price: 0,
      original_unit_price: 0,
      original_quantity: 0,
      quantity: 0,
      version: Invoice::VERSION,
    }
  end

  def base_hookup
    {
      description: HOOKUP,
      net_amount: hookup,
      tax_amount: 0,
      estimated: true,
      unit_price: hookup,
      original_unit_price: hookup,
      original_quantity: 1,
      quantity: 1,
      version: Invoice::VERSION,
    }
  end

  def add_common_items(invoicable, lis)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)

    lis << base_tax
  end

  # Replace line items with a minimum charge if Company#invoice_mileage_minimum is not met
  def adjust_mileage_line_items(invoicable, lis)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    if invoicable.fleet_company&.is_tesla?
      return
    end
    min_amount = company.invoice_mileage_minimum
    if min_amount
      total = Rate.sum_line_items(lis)
      if total < min_amount
        Rate.remove_all_but_tax(lis)
        lis << {
          description: MINIMUM,
          net_amount: min_amount,
          tax_amount: 0,
          estimated: true,
          unit_price: min_amount,
          original_unit_price: min_amount,
          original_quantity: 1,
          quantity: 1,
          version: Invoice::VERSION,
        }
      end
    end
  end

  # Rate additions are stored on the rate without vehicle_category_type
  #
  # ENG-8479: Change all model/rate.rb methods to return lineitems, instead of adding line items to 'lis' to argument
  def add_rate_additions_for_type(lis, rate, job_id)
    if rate && rate.additions
      rate.additions.each do |addition|
        if addition.is_flat
          lis << {
            description: addition.name,
            net_amount: addition.amount,
            tax_amount: 0,
            estimated: true,
            unit_price: addition.amount,
            original_unit_price: addition.amount,
            original_quantity: 1,
            quantity: 1,
            version: Invoice::VERSION,
            calc_type: "flat",
            job_id: job_id,
          }
        end
      end

      total = Rate.sum_line_items(lis)

      rate.additions.each do |addition|
        if addition.is_percent
          price = 0
          if addition.amount
            price = addition.amount
          end
          lis << {
            description: addition.name,
            net_amount: total * price * 0.01,
            tax_amount: 0,
            estimated: true,
            unit_price: total,
            original_unit_price: total,
            original_quantity: price,
            quantity: price,
            version: Invoice::VERSION,
            calc_type: "percent",
            job_id: job_id,
          }
        end
      end

    end
  end

  def self.fudge(job_id,
                 mins,
                 invoice_increment_mins,
                 invoice_roundup_mins,
                 invoice_minimum_mins)
    logger.debug "RATES(#{job_id}) running with mins:#{mins}, invoice_increment_mins:#{invoice_increment_mins}, invoice_roundup_mins:#{invoice_roundup_mins}, invoice_minimum_mins:#{invoice_minimum_mins}"
    #  invoice_minimum_mins: 60
    #  invoice_increment_mins: 15
    #  invoice_roundup_mins: 5
    if invoice_increment_mins && invoice_roundup_mins
      units = mins / invoice_increment_mins
      if (mins % invoice_increment_mins) > invoice_roundup_mins
        units += 1
      end
      new = units * invoice_increment_mins
      logger.debug "RATES(#{job_id}) rounding #{mins} to #{new}"
      mins = new
    end

    if invoice_minimum_mins && (mins < invoice_minimum_mins)
      mins = invoice_minimum_mins
      logger.debug "RATES(#{job_id}) Applying minumum of #{mins}"
    end

    mins
  end

  def base_hours(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    mins = invoicable.mins_p2p

    if mins
      mins = Rate.fudge(invoicable.job_id,
                        mins,
                        company.invoice_increment_mins || 15,
                        company.invoice_roundup_mins || 5,
                        company.invoice_minimum_mins || 60)
    else
      logger.warn("INVOICE(#{invoicable.job_id}) Unable to build invoice because mins_p2p returned null")
      mins = 0
    end

    _base_hours_with_mins(mins)
  end

  # This is only used by Tesla rate so I'm disentangling it from the other code
  def base_p2p(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    total_distance = 0
    if invoicable.miles_p2p
      total_distance = invoicable.miles_p2p
    end
    # TODO: For whatever reason we aren't calling miles_p2p_free

    if company.metric_distance_units?
      mi_in_km = (1.0 / Geocoder::Calculations::KM_IN_MI)
      total_distance = mi_in_km * total_distance
      unit_price = miles_p2p / mi_in_km
      description = KILOMETERS_PORT_TO_PORT
    else
      unit_price = miles_p2p
      description = MILES_PORT_TO_PORT
    end

    unless invoicable.fleet_company&.is_tesla?
      total_distance = company.round_distance(total_distance)
    end
    _base_p2p_with_mileage(total_distance, unit_price, description)
  end

  def _base_p2p_with_mileage(total_distance, unit_price, description)
    total_distance_bd = total_distance.to_d.round(1)
    unit_price = unit_price.round(2)
    p2p_net = (total_distance_bd * unit_price).round(2)

    [{
      description: description,
      net_amount: p2p_net,
      tax_amount: 0,
      estimated: true,
      unit_price: unit_price,
      original_unit_price: unit_price,
      quantity: total_distance_bd,
      original_quantity: total_distance_bd,
      version: Invoice::VERSION,
    }]
  end

  def base_tow(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    distance = invoicable.estimate.try(:miles_bc)
    if !distance
      logger.debug("INVOICE(#{invoicable.job_id}) Not calculating tow for null estimate or null estimate.distance")
    end

    InvoicingLineItemService::BuildTowAttributes.new(
      distance, miles_towed_free, company, miles_towed, invoicable.fleet_company&.is_tesla?
    ).call
  end

  def base_en_route(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    distance = invoicable.estimate.try(:miles_ab)
    if !distance
      logger.warn("INVOICE(#{invoicable.job_id}) Not calculating enroute for null distance")
    end

    InvoicingLineItemService::BuildEnRouteAttributes.new(
      distance, miles_enroute_free, company, miles_enroute, invoicable.fleet_company&.is_tesla?
    ).call
  end

  def base_deadhead(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    if invoicable.drop_location
      if invoicable.estimate
        distance = invoicable.estimate.try(:miles_ca)
      end
    else
      if invoicable.estimate
        distance = invoicable.estimate.try(:miles_ba)
      end
    end

    if !distance
      logger.warn("INVOICE(#{invoicable.job_id}) Not calculating deadhead for null distance")
    end

    InvoicingLineItemService::BuildDeadheadAttributes.new(
      distance, miles_deadhead_free, company, miles_deadhead, invoicable.fleet_company&.is_tesla?
    ).call
  end

  def duplicate?(company)
    company.rates.exists?(
      account: account,
      site: site,
      vendor_id: vendor_id,
      service_code: service_code,
      type: type,
      vehicle_category_id: vehicle_category_id,
      live: live
    )
  end

  # Return true if this rate represents the GOA rate.
  def goa?
    !!service_code&.goa?
  end

  MINS_PER_HOUR = BigDecimal('60')

  def _base_hours_with_mins(mins)
    quantity = (mins / MINS_PER_HOUR).round(2)
    net_amount = (quantity * hourly).round(2)

    {
      description: PORT_TO_PORT_HOURS,
      net_amount: net_amount,
      tax_amount: 0,
      estimated: true,
      unit_price: hourly,
      original_unit_price: hourly,
      quantity: quantity,
      original_quantity: quantity,
      version: Invoice::VERSION,
    }
  end

end
