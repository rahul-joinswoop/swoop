# frozen_string_literal: true

class JobAuditSms < AuditSms

  def redirect_url
    "#{ENV['SITE_URL']}"
  end

end
