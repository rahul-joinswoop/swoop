# frozen_string_literal: true

class StorageRate < Rate

  include LoggableJob

  validates :storage_daily, presence: true

  before_save :set_defaults

  def set_defaults
    if !beyond_charge
      self.beyond_charge = "Per 24 Hours"
    end
    if !second_day_charge
      self.second_day_charge = "After 24 Hours"
    end
    if !seconds_free_window
      self.seconds_free_window = 0
    end
  end

  # job - only used for debug printing job number
  def get_qty(job, items, utc_now, base_storage_rate)
    # needing to set this would imply a broken rate in the db
    # base_storage_rate.set_defaults()
    seconds_free_window = base_storage_rate.seconds_free_window
    second_day_charge = base_storage_rate.second_day_charge
    beyond_charge = base_storage_rate.beyond_charge

    qty = 0
    items.each do |item|
      if item.deleted_at?
        log :debug, "STORAGE - skipping deleted", rates: job.id
        next
      end

      # Uses history items as they are the true source of information.
      # item.stored_at/released_at are there for performance/sorting reasons.
      ajs = job.audit_job_statuses.for_live_status(Job::STORED).first
      if ajs
        start_time = ajs.adjusted_dttm || ajs.last_set_dttm
      else
        raise log_msg("RATES No storage history item exists when calculating storage", job: job.id)
      end

      ajs = job.audit_job_statuses.for_live_status(Job::RELEASED).first
      if ajs
        end_time = ajs.adjusted_dttm || ajs.last_set_dttm
      end

      if !end_time
        end_time = utc_now
      end

      seconds = (end_time - start_time)

      log :debug, "STORAGE - stored_at: #{start_time}", rates: job.id
      log :debug, "STORAGE - now: #{utc_now}", rates: job.id
      log :debug, "STORAGE - end time: #{end_time}", rates: job.id
      log :debug, "STORAGE - stored for #{seconds} secs", rates: job.id

      # If it's within the free window ignore
      if seconds_free_window && (seconds < seconds_free_window)
        log :debug, "STORAGE - In free seconds window", rates: job.id
        next
      end

      days = (seconds / (3600 * 24))
      log :debug, "STORAGE - #{days} days", rates: job.id

      second_day_start = nil
      day_sum = 1

      if second_day_charge == "After 24 Hours"
        if days > 1
          #          days -= 1
          day_sum += 1
          second_day_start = start_time + 24.hours
        end
      elsif second_day_charge == "After 12 Hours"
        if days > 0.5
          #          days -= 1
          day_sum += 1
          second_day_start = start_time + 12.hours
        end
      elsif second_day_charge == "Next Calendar Day"
        # TODO: Add 1 to day_sum if midnight is within #{days} of start_time
        site_tz = "America/Los_Angeles"
        if item.site.tz
          site_tz = item.site.tz
        end
        stored_at_in_localtime = start_time.in_time_zone(site_tz)
        stored_at_in_localtime_eod = stored_at_in_localtime.end_of_day
        eod_utc = stored_at_in_localtime_eod.utc
        if start_time + seconds > eod_utc
          day_sum += 1
          second_day_start = eod_utc
        end
      else

        raise "Unknown second day charge: #{second_day_charge}"
      end
      if second_day_start
        second_day_start += 1.second
        seconds = (end_time - second_day_start)

        log :debug, "STORAGE 2nd+ - stored_at: #{second_day_start}", rates: job.id
        log :debug, "STORAGE 2nd+ - now: #{utc_now}", rates: job.id
        log :debug, "STORAGE 2nd+ - stored for #{seconds} secs", rates: job.id

        days = (seconds / (3600 * 24))

        log :debug, "STORAGE 2nd+ - #{days} days", rates: job.id

        if beyond_charge == "Per 24 Hours"
          if days > 1
            #          days -= 1
            day_sum += days.floor
          end
        elsif beyond_charge == "Per 12 Hours"
          if days > 0.5
            day_sum += (days * 2).floor / 2.0
          end
        elsif beyond_charge == "Per Calendar Day"
          log :debug, "STORAGE 2nd+ - pcd", rates: job.id
          # TODO: Add 1 to day_sum if midnight is within #{days} of second_day_start
          site_tz = "America/Los_Angeles"
          if item.site.tz
            site_tz = item.site.tz
          end

          stored_at_in_localtime = second_day_start.in_time_zone(site_tz)
          log :debug, "STORAGE 2nd+ - stored_at_in_localtime: #{stored_at_in_localtime}", rates: job.id
          stored_at_in_localtime_eod = stored_at_in_localtime.end_of_day
          log :debug, "STORAGE 2nd+ - stored_at_in_localtime_eod: #{stored_at_in_localtime_eod}", rates: job.id
          eod_utc = stored_at_in_localtime_eod.utc
          if second_day_start + seconds > eod_utc
            log :debug, "STORAGE 2nd+ - ds+", rates: job.id
            day_sum += 1
            secs_left = second_day_start + seconds - eod_utc
            log :debug, "STORAGE 2nd+ - left: #{secs_left}", rates: job.id
            remaining = secs_left / (3600 * 24)
            log :debug, "STORAGE 2nd+ - remaining: #{remaining}", rates: job.id
            day_sum += remaining.floor
          end
        else
          raise "Unknown beyond charge: #{beyond_charge}"
        end
      end

      qty += day_sum
      log :debug, "STORAGE - #{item.id}:#{qty} days", rates: job.id
    end

    qty
  end

  STORAGE = "Storage"

  def line_item_descriptions
    [STORAGE]
  end

  def calculate_line_items(job, add_common_items = true)
    raise CannotCalculateRateWithoutInvoicableError unless job.is_a?(Invoicable)
    items = job.vehicle_inventory_items(false)
    if !items.any?
      logger.debug "STORAGE(#{job.id})No storage items found"
      return StorageRate.blank_line_items
    end

    base_storage_rate = Job.get_storage_or_create_zero_storage_rate(job)
    qty = get_qty(job, items, Time.now.utc, base_storage_rate)

    price = 0
    if storage_daily
      price = storage_daily

      current_price_on_invoice = job.invoice.mem_find_storage(job)&.unit_price

      # If job has an invoice with edited price for the Storage item, we use it to re-calculate the new price,
      # otherwise the item value will be orverriden with the original value of the Rate
      # without considering the new manually edited one.
      #
      # e.g -> StorageRate.storage_daily is 100
      #     -> invoice has 10 days of storage
      #     -> TOTAL = 100 * 10 -> 1000
      #
      #     -> invoice is manually edited on FE and storage item price is changed to 20
      #     -> invoice is re-calculated
      #     -> TOTAL = 20 * 10 -> 200
      #
      #     -> CASE: job history is manually changed: StorageTick will call this, and without this treatment
      #              it would use self.storage_daily value, so TOTAL = 100 * 10 -> 1000,
      #              leading to the original (and wrong) invoice value.
      #
      if current_price_on_invoice.present?
        price = current_price_on_invoice
      end
    end

    lis = [{
      description: "Storage",
      tax_amount: 0,
      estimated: true,
      net_amount: qty * price,
      unit_price: price,
      original_unit_price: price,
      original_quantity: qty,
      quantity: qty,
      version: Invoice::VERSION,
    }]

    if add_common_items
      add_common_items(job, lis)
    end
    lis
  end

  def self.blank_line_items
    [{
      description: "Storage",
      tax_amount: 0,
      estimated: true,
      net_amount: 0,
      unit_price: 0,
      original_unit_price: 0,
      original_quantity: 0,
      quantity: 0,
      version: Invoice::VERSION,
    }]
  end

end
