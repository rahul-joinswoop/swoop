# frozen_string_literal: true

class AuditIssc < ApplicationRecord

  belongs_to :job

  before_save :truncate_fields

  private

  MAX_FIELD_LENGTH = 8096

  # Since this is an audit table for logging system we don't want to blow up if
  # someone posts too much data
  def truncate_fields
    [:path, :data, :clientid, :contractorid, :dispatchid, :locationid, :http_response, :error].each do |field|
      val = self[field]
      if val.present? && val.length > MAX_FIELD_LENGTH
        self[field] = val[0, MAX_FIELD_LENGTH]
      end
    end
  end

end
