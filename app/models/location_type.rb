# frozen_string_literal: true

class LocationType < BaseModel

  include SoftDelete

  AUTO_BODY_SHOP    = 'Auto Body Shop'
  BLOCKING_TRAFFIC  = 'Blocking Traffic'
  BUSINESS          = 'Business'
  DEALERSHIP        = 'Dealership'
  GATED_COMMUNITY   = 'Gated Community'
  HIGHWAY           = 'Highway'
  HOME              = 'Home'
  HQ                = 'HQ'
  IMPOUND_LOT       = 'Impound Lot'
  INTERSECTION      = 'Intersection'
  LOCAL_ROADSIDE    = 'Local Roadside'
  LOW_CLEARANCE     = 'Low Clearance'
  NON_DEALERSHIP_REPAIR_FACILITY = 'Non-dealership Repair Facility'
  OTHER_EXPLAIN_IN_NOTES = 'Other (explain in Notes)'
  PARKING_GARAGE    = 'Parking Garage'
  PARKING_LOT       = 'Parking Lot'
  POINT_OF_INTEREST = 'Point of Interest'
  RESIDENCE         = 'Residence'
  STORAGE_FACILITY  = 'Storage Facility'
  TOW_COMPANY       = 'Tow Company'

  CLIENT_API_LOCATION_TYPES = [
    BLOCKING_TRAFFIC,
    BUSINESS,
    GATED_COMMUNITY,
    HIGHWAY,
    INTERSECTION,
    LOCAL_ROADSIDE,
    PARKING_GARAGE,
    PARKING_LOT,
    RESIDENCE,
  ].freeze

  def listeners
    CompaniesLocationType.distinct.where(location_type: self).map(&:company)
  end

  def render_to_hash
    controller = ActionController::Base.new

    JSON.parse(controller.render_to_string("api/v1/location_types/_location_type", locals: { location_type: self }))
  end

end
