# frozen_string_literal: true

class CannotGetOrCreateRankersForNonManagedFleetCompaniesError < StandardError; end

class FleetCompany < Company

  include FleetRescueVehicles
  has_many :incoming_invoices, ->(obj) { obj.in_house? ? joins(:job).where("jobs.type != 'RescueJob'") : self }, foreign_key: :recipient_company_id, class_name: "Invoice"
  has_many :end_user_invoices, foreign_key: :sender_id, class_name: "Invoice"
  has_many :live_rescue_providers, -> { where deleted_at: nil }, class_name: 'RescueProvider', foreign_key: :company_id, inverse_of: :company
  has_many :rescue_providers, class_name: 'RescueProvider', foreign_key: :company_id, inverse_of: :company
  has_one :ranker, -> { where live: true }, class_name: 'Auction::Ranker', foreign_key: :client_id, inverse_of: :client
  has_many :issc_logins, inverse_of: :fleet_company

  # ISSC supported
  SUPPORTED_ISSC_COMPANIES = [
    AGERO,
    NATION_SAFE_DRIVERS,
    QUEST,
    TESLA_MOTORS_INC,
    USAC,
  ].freeze

  # ISSC unsupported
  UNSUPPORTED_ISSC_COMPANIES = [
    ALLIED_DISPATCH_SOLUTIONS,
    ALLSTATE,
    GEICO,
    ROAD_AMERICA,
  ].freeze

  ALL_ISSC_COMPANIES = [
    *SUPPORTED_ISSC_COMPANIES,
    *UNSUPPORTED_ISSC_COMPANIES,
  ].freeze

  # Note: Geico also has the clientid GCO, but this is handled in s/w
  ISSC_CLIENT_IDS = {
    AGERO => 'AGO',
    ALLIED_DISPATCH_SOLUTIONS => 'ADS',
    ALLSTATE => 'ALLS',
    GEICO => 'GCOAPI',
    NATION_SAFE_DRIVERS => 'NSD',
    QUEST => 'QUEST',
    ROAD_AMERICA => 'RDAM',
    USAC => 'USAC',
    TESLA_MOTORS_INC => 'TSLA',
  }.freeze

  # In House
  ALL_INHOUSE_COMPANIES = [
    TESLA,
    ENTERPRISE,
    MOBILE_MINI,
  ].freeze

  # Managed short names
  FARMERS_SHORT_NAME = 'Farmers'
  CENTURY_SHORT_NAME = '21st Century'

  # Managed camel case
  FLEET_RESPONSE_CAMEL_CASE = 'FleetResponse'

  # all managed
  ALL_MANAGED_COMPANIES = [
    FARMERS,
    METROMILE,
    TURO,
    SILVERCAR,
    LUXE,
    FLEET_RESPONSE,
    USAA,
    TANTALUM,
    LINCOLN,
  ].freeze

  def self.x_digits(x)
    digits = '0123456789'
    ret = []
    x.times do |num|
      ret << digits[rand(digits.length)]
    end
    ret.join
  end

  def self.init_all_isscs
    ALL_ISSC_COMPANIES.each do |isscco|
      # bail if this company already exists
      next if FleetCompany.where(name: isscco, issc_client_id: ISSC_CLIENT_IDS[isscco]).exists?
      features = Feature.where(name: [Feature::SETTINGS])
      CompanyService::Create.new(
        company_attributes: {
          type: 'FleetCompany',
          name: isscco,
          phone: '+1415555' + x_digits(4),
          support_email: "test+#{isscco}_support@joinswoop.com",
          accounting_email: "test+#{isscco}_accounting@joinswoop.com",
          issc_client_id: ISSC_CLIENT_IDS[isscco],
          use_custom_job_status_reasons: true,
        },

        location_attributes: {
          lat: 37.7786953,
          lng: -122.4112873,
          place_id: "ChIJsRjKQoOAhYARwXo4SeJkExQ",
          address: "1121 Mission Street, San Francisco, CA",
          exact: true,
        },
        feature_ids: features.to_a.map(&:id)
      ).call
      if UNSUPPORTED_ISSC_COMPANIES.include?(isscco)
        FleetCompany.find_by(name: isscco, issc_client_id: ISSC_CLIENT_IDS[isscco]).update! deleted_at: DateTime.current
      end
    end
  end

  def self.init_all_inhouse
    ALL_INHOUSE_COMPANIES.each { |company_name| init_inhouse(company_name) }
  end

  def self.init_inhouse(company_name)
    features = Feature.where(name: [Feature::SETTINGS])
    CompanyService::Create.new(
      company_attributes: {
        type: 'FleetCompany',
        name: company_name,
        phone: '+1415555' + x_digits(4),
        support_email: "test+#{company_name}_support@joinswoop.com",
        accounting_email: "test+#{company_name}_accounting@joinswoop.com",
        in_house: true,
      },
      location_attributes: {
        lat: 37.7786953,
        lng: -122.4112873,
        place_id: "ChIJsRjKQoOAhYARwXo4SeJkExQ",
        address: "1121 Mission Street, San Francisco, CA",
        exact: true,
      },
      feature_ids: features.to_a.map(&:id)
    ).call
  end

  def self.init_all_managed
    ALL_MANAGED_COMPANIES.each { |company_name| init_managed(company_name) }
  end

  def self.init_managed(company_name)
    features = Feature.where(name: [Feature::SETTINGS])
    CompanyService::Create.new(
      company_attributes: {
        type: 'FleetCompany',
        name: company_name,
        phone: '+1415555' + x_digits(4),
        support_email: "test+#{company_name}_support@joinswoop.com",
        accounting_email: "test+#{company_name}_accounting@joinswoop.com",
      },
      location_attributes: {
        lat: 37.7786953,
        lng: -122.4112873,
        place_id: "ChIJsRjKQoOAhYARwXo4SeJkExQ",
        address: "1121 Mission Street, San Francisco, CA",
        exact: true,
      },
      feature_ids: features.to_a.map(&:id)
    ).call
  end

  DEFAULT_CUSTOMER_TYPES_FOR_NEW_COMPANY =
    [
      CustomerType::COVERED,
      CustomerType::CUSTOMER_PAY,
    ].freeze

  DEFAULT_FLEET_MANAGED_SMS_FEATURE_NAMES = [
    Feature::SMS_REVIEWS,
    Feature::TEXT_ETA_UPDATES,
    Feature::SMS_TRACK_DRIVER,
    Feature::SMS_REQUEST_LOCATION,
    Feature::SMS_CONFIRM_ON_SITE,
  ].freeze

  DEFAULT_REPORTS_FOR_INHOUSE = [
    Report::ALL_DATA_FLEET_INHOUSE,
  ].freeze

  DEFAULT_REPORTS_FOR_MANAGED = [
    Report::FLEET_ACTIVITY,
    Report::FLEET_CSAT,
    Report::FLEET_ETA_VARIANCE,
  ].freeze

  def defaults
    if fleet_in_house?
      add_reports_by_name(DEFAULT_REPORTS_FOR_INHOUSE)
    elsif fleet_managed?
      add_reports_by_name(DEFAULT_REPORTS_FOR_MANAGED)
      add_symptoms(Symptom.standard_items)
    end

    # We have features dependent on services so we need to add services first
    add_features_by_name(default_features_for_fleet)

    add_customer_types_by_name(DEFAULT_CUSTOMER_TYPES_FOR_NEW_COMPANY)

    super
  end

  def service_list_for_new_company
    ServiceCode.standard_items.where(addon: [nil, false]).where.not(name: ServiceCode::STORAGE)
  end

  def auction_max_bidders
    setting(Setting::AUCTION_MAX_BIDDERS, :integer, Auction::Auction.default_max_bidders)
  end

  # This setting is deprecated with the Territories feature.
  def auction_max_distance
    setting(Setting::AUCTION_MAX_DISTANCE, :integer, Auction::Auction.default_max_candidate_distance)
  end

  # Maximum possible distance in miles a truck could drive to meet the auction ETA at highway speeds.
  # This setting is used as a sanity check to filter out candidates when a company has far flung territories.
  def auction_max_possible_distance
    # Miles per minute traveled at 80 miles per hour
    miles_per_minute = 80.0 / 60.0
    auction_max_eta * miles_per_minute
  end

  def auction_max_eta
    setting(Setting::AUCTION_MAX_ETA, :integer, Auction::Auction.default_max_candidate_eta)
  end

  # If you call this method on a new (unsaved) FleetCompany object, it will
  # save the FleetCompany immediately because `self` needs to be resolved to
  # an `id` for the Auction::WeightedSumRanker's `client` reference; exercise caution
  def get_or_create_ranker
    raise CannotGetOrCreateRankersForNonManagedFleetCompaniesError unless fleet_managed?
    ranker ||
    Auction::WeightedSumRanker.create!(
      name: "Fully speed weighted (ETA)",
      weights: { speed: 1.0, quality: 0.0, cost: 0.0 },
      constraints: {
        min_eta: 5,
        max_eta: max_candidate_eta,
        min_cost: 20,
        max_cost: 150,
        min_rating: 40,
        max_rating: 100,
      },
      live: true,
      client: self
    )
  end

  def max_candidate_eta
    Setting.get_integer(self, Setting::AUCTION_MAX_ETA) || Auction::Auction.default_max_candidate_eta
  end

  def sites_enabled?
    has_feature?(Feature::CLIENT_SITES)
  end

  def vehicle_class_type_enabled?
    has_feature?(Feature::FLEET_CLASS_TYPE)
  end

  def can_auto_swoop_dispatch?
    has_feature(Feature::AUTO_ASSIGN)
  end

  def cant_auto_swoop_dispatch?
    !can_auto_swoop_dispatch?
  end

  private

  def default_features_for_fleet
    feature_names = [Feature::SETTINGS]

    if fleet_managed?
      feature_names << Feature::QUESTIONS
      feature_names << Feature::SHOW_SYMPTOMS
      feature_names += DEFAULT_FLEET_MANAGED_SMS_FEATURE_NAMES
    end

    feature_names
  end

end
