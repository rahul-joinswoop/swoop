# frozen_string_literal: true

# CustomerType is aka as PaymentType in the FE and GraphQL.
#
# A Payment Type must be setup for a Company in the DB in order to be
# available during the job creation / editing.
#
# Company has an alias to this model: payment_types
# @see Company
#
# The join table between a Company and this model is companies_customer_types
class CustomerType < ApplicationRecord

  ACCOUNT = 'Account'
  AUCTION = 'Auction'
  BODY_SHOP = 'Body Shop'
  CASH = 'Cash'
  CHECK = 'Check'
  CLAIM_DAMAGE = 'Claim/Damage'
  COMCHEK = 'Comchek/EFS Check'
  COVERED = 'Covered'
  CREDIT_CARD = 'Credit Card'
  CUSTOMER_PAY = 'Customer Pay'
  FOR_REVIEW = 'For Review'
  GOODWILL = 'Goodwill'
  MECHANICAL = 'Mechanical'
  NON_WARRANTY = 'Non-Warranty'
  PCARD = 'P Card'
  PO = 'PO'
  TBD = 'TBD'
  SERVICE_CENTER = 'Service Center'
  WARRANTY = 'Warranty'

  ALL = [
    WARRANTY,
    NON_WARRANTY,
    PCARD,
    CUSTOMER_PAY,
    COVERED,
    CLAIM_DAMAGE,
    MECHANICAL,
    AUCTION,
    FOR_REVIEW,
    TBD,
    ACCOUNT,
    CASH,
    CHECK,
    COMCHEK,
    CREDIT_CARD,
    PO,
    GOODWILL,
    SERVICE_CENTER,
    BODY_SHOP,
  ].freeze

  has_many :companies_customer_type
  has_many :companies, through: :companies_customer_type

  def self.init_all
    ALL.each do |ct|
      CustomerType.find_or_create_by(name: ct)
    end
  end

  def customer_pay?
    CUSTOMER_PAY == name
  end

  def goodwill?
    GOODWILL == name
  end

end
