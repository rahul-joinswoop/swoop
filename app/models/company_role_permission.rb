# frozen_string_literal: true

class CompanyRolePermission < ApplicationRecord

  # permission types
  CHARGE = 'charge'
  REFUND = 'refund'

  ALL_PERMISSION_TYPES_ALLOWED = [
    CHARGE,
    REFUND,
  ].freeze

  belongs_to :role
  belongs_to :company

  validates :role, :company, :permission_type, presence: true
  validates :permission_type, inclusion: { in: ALL_PERMISSION_TYPES_ALLOWED }

end
