# frozen_string_literal: true

class IsscLogin < ApplicationRecord

  belongs_to :site
  belongs_to :fleet_company, class_name: "FleetCompany", foreign_key: "fleet_company_id", inverse_of: :issc_logins

end
