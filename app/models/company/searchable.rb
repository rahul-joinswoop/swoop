# frozen_string_literal: true

# ElasticSearch specific definitions for companies.
module Company::Searchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    index_name Swoop::ElasticSearch.index_name(:companies)

    scope :elastic_search_backfill, -> {
      finder = elastic_search_preload
      ts = Time.zone.parse(ENV['MODEL_TS']) if ENV['MODEL_TS'].present?
      finder = finder.where('companies.updated_at >= ?', ts) if ts.present?
      finder
    }

    scope :elastic_search_preload, -> {
      preload([:location])
    }

    after_commit on: [:create, :update] do
      Indexer.perform_async(:index, Company.name, id)
    end

    after_commit on: [:destroy] do
      Indexer.perform_async(:delete, Company.name, id)
    end

    settings index: {
      number_of_shards: 1,
      number_of_replicas: 1,
      analysis: {
        filter: {
          preserve_asciifolding: {
            type: "asciifolding",
            preserve_original: true,
          },
          strip_punctuation: {
            type: "pattern_replace",
            pattern: "\\p{P}",
            replacement: "",
          },
        },
        analyzer: {
          name_analyzer: {
            type: "custom",
            tokenizer: "standard",
            filter: ["standard", "lowercase", "preserve_asciifolding", "strip_punctuation"],
          },
        },
        normalizer: {
          lowercase_normalizer: {
            type: "custom",
            filter: ["lowercase"],
          },
        },
      },
    } do
      mappings dynamic: 'false' do
        indexes :accounting_email
        indexes :deleted_at, type: :date
        indexes :dispatch_email
        indexes :id, type: :integer
        indexes :agero_vendor_id, type: :integer
        indexes :location do
          indexes :address
        end
        indexes :name, analyzer: :name_analyzer
        indexes :sort_name, type: :keyword
        indexes :parent_company_id, type: :integer
        indexes :phone, type: :keyword
        indexes :support_email
        indexes :type, type: :keyword, normalizer: "lowercase_normalizer"
        indexes :subtype, type: :keyword, normalizer: "lowercase_normalizer" # ['Managed','InHouse','MotorClub','Partner']
      end
    end
  end

  def as_indexed_json(_options = {})
    as_json(
      only: [
        :accounting_email,
        :deleted_at,
        :dispatch_email,
        :id,
        :name,
        :parent_company_id,
        :phone,
        :support_email,
        :type,
      ], methods: [
        :subtype,
      ],
      include: {
        location: { only: [:address] },
      }
    ).merge("sort_name" => sort_name)
  end

end
