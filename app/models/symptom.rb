# frozen_string_literal: true

class Symptom < ApplicationRecord

  include StandardOrCustomCollection
  include SoftDelete

  ACCIDENT = 'Accident'
  CUSTOMER_UNSAFE = 'Customer unsafe'
  DEAD_BATTERY = 'Dead battery'
  FLAT_TIRE = 'Flat tire'
  LOCKED_OUT = 'Locked out'
  LONG_DISTANCE_TOW = 'Long distance tow'
  MECHANICAL_ISSUE = 'Mechanical issue'
  OTHER = 'Other'
  OUT_OF_FUEL = 'Out of fuel'
  REUNITE = 'Reunite'
  RECOVERY = 'Recovery'
  UNKNOWN = 'Unknown'

  SYMPTOMS = [
    ACCIDENT,
    "Breakdown: Dealer",
    "Breakdown: Shop",
    "Car drives but is experiencing issue",
    "Car will not charge – not enough range to reach service center",
    "Car will not drive",
    "Collision: Auction",
    "Collision: Body Shop",
    "Customer feels unsafe driving",
    "Customer not safe driving",
    CUSTOMER_UNSAFE,
    DEAD_BATTERY,
    "Engine will not turn over-low battery",
    FLAT_TIRE,
    "Flat Tire",
    "Flat tire- Runflat cust refuses to drive",
    "Flat tire- Runflat sidewall damaged",
    "Flat tire - Standard no spare, no kit",
    "Flat tire - Standard with spare or kit",
    "Keys locked inside passenger compartment",
    "Keys locked in the trunk",
    LOCKED_OUT,
    "Lockout Assistance",
    LONG_DISTANCE_TOW,
    "Mechanical Disablement",
    MECHANICAL_ISSUE,
    "Multiple flat tires",
    "No issue reported",
    OTHER,
    "Out of charge",
    OUT_OF_FUEL,
    "Ran out of range",
    RECOVERY,
    REUNITE,
    "Roadside",
    "Service Center",
    "Unauthorized use",
    UNKNOWN,
    "Vehicle abandoned",
  ].freeze

  has_many :company_symptoms, dependent: :destroy
  has_many :companies, -> { where(deleted_at: nil) }, through: :company_symptoms

  def self.gen_symptoms
    File.open('symptom.txt', 'w') do |file|
      file.write "["
      first = true
      Symptom.order(:name).each do |symptom|
        if !first
          file.write ",\n"
        end
        file.write "\"#{symptom.name}\""
        first = false
      end
      file.write "]"
    end
  end

  def self.init_all
    SYMPTOMS.each do |symptom|
      Symptom.find_or_create_by(name: symptom)
    end
  end

  def accident?
    ACCIDENT == name
  end

  def reunite?
    REUNITE == name
  end

end
