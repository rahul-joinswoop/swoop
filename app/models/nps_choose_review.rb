# frozen_string_literal: true

class NpsChooseReview < NpsReview

  def text_template
    "Reply “go” to take a 4 question survey via SMS, or tap: #{company_review_url} - Thank you!"
  end

  def set_review_text
    t = text_template

    if company.permission_to_send_reviews_from
      t = "%{company}: " + t
    end
    self.text = t % { company: company.name, rescue_company_name: job.rescue_company.name }
  end

  def process_response(twilio_response)
    if twilio_response.body.strip.downcase.start_with?("go")
      set_nps_review question: 0
      send_review_sms
    else
      super twilio_response
    end
  end

end
