# frozen_string_literal: true

class RateAddition < ApplicationRecord

  include ValidateCurrency

  belongs_to :rate
  belongs_to :service_code

  validates :name, presence: true
  validate_currency_matches :rate

  after_initialize :init

  def init
    self.amount ||= 0
  end

  def is_flat
    calc_type == "flat"
  end

  def is_percent
    calc_type == "percent"
  end

end
