# frozen_string_literal: true

# RescueProvider is the join between a FleetCompany (or Swoop) and the site of a
# RescueCompany. i.e. it lists all the sites that a fleet can dispatch to e.g.
# [FinishLineTowing (SanFrancisco), FinishLineTowing(San Jose)]
class RescueProvider < BaseModel

  include RescueProvider::Searchable

  MAX_DISTANCE = 99_999
  DEFAULT_AUTO_ASSIGN_DISTANCE_MILES = 10

  validates :site, presence: true
  validates :auto_assign_distance, allow_nil: true, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 250,
  }

  belongs_to :company
  belongs_to :provider, class_name: "Company", foreign_key: "provider_id"
  belongs_to :location
  belongs_to :site
  attr_accessor :job_distance
  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :site
  has_and_belongs_to_many :tags

  # alias to support JobStatusEmail
  alias_attribute :job_update_email, :dispatch_email

  before_validation :set_provider_from_site

  after_commit :send_dispatchable_changed_notification, on: :update

  def self.grouped_by_partner(company, includes_relations)
    sql = "SELECT id FROM rescue_providers WHERE id IN (
             SELECT MIN(id) FROM rescue_providers
             WHERE company_id = ? GROUP BY provider_id)"
    provider_ids = find_by_sql([sql, company.id])
    includes(includes_relations).where(id: provider_ids)
  end

  def phone_or_site_phone
    phone || site&.phone
  end

  def location_address_or_providers
    location&.address || provider.location&.address
  end

  def open
    true
  end

  def open_for_business
    if site
      return site.open_for_business?
    end
    false
  end

  def auto_assign_distance_miles
    auto_assign_distance || DEFAULT_AUTO_ASSIGN_DISTANCE_MILES
  end

  def listeners
    ret = Set.new
    ret << company
    ret << Company.swoop
    ret
  end

  # N.B. It makes no sense for a rescue provider to have a location -- the location belongs on the site. Therefore, it’s unclear what “best location” means.
  def best_location
    return location       if location.present?
    return site.location  if site.present?
    nil
  end

  # Memolize but look for exactly nil as false is a valid value
  def is_primary?
    @is_primary = tags.where(name: "Primary").exists? if @is_primary.nil?
    @is_primary
  end

  def is_secondary?
    @is_secondary = tags.where(name: "Secondary").exists? if @is_secondary.nil?
    @is_secondary
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/providers/_provider", locals: { provider: self }))
  end

  def send_dispatchable_changed_notification
    if previous_changes[:deleted_at] && !deleted_at.nil?
      root_users = User.with_role(:root).where(deleted_at: nil)
      Delayed::Job.enqueue EmailDispatchableAdminNotification.new(root_users.map(&:id), site_id: site_id, rescue_provider_id: id)
    end
  end

  # Customize this RescueProvider instance name with its site.name if necessary.
  # It depends on the quantity of sites this RescueProvider contains.
  #
  # @see RescueCompany#customized_name_having
  def name
    provider.customized_name_having(site)
  end

  def sort_name
    I18n.transliterate(name.to_s.downcase.strip)
  end

  def non_live_or_live_after_hours
    !provider.live || !open_for_business
  end

  # Return true if this is the rescue provider record associated with Swoop
  def swoop?
    company_id == Company.swoop_id
  end

  private

  def set_provider_from_site
    self.provider = site.company if site
  end

end
