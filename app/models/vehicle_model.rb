# frozen_string_literal: true

class VehicleModel < ApplicationRecord

  belongs_to :vehicle_make
  belongs_to :vehicle_type

end
