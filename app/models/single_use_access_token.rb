# frozen_string_literal: true

class SingleUseAccessToken < BaseModel

  EXPIRES_IN = 5.minutes

  belongs_to :user, optional: false

  def self.generate(user)
    now = Time.zone.now.to_i
    token = where("exp > ?", now).find_by(user: user, used_at: nil)
    return token if token.present?

    claims = JwtManager.generate_claims(user.id, EXPIRES_IN)
    attrs = claims.merge(user: user, jwt: JwtManager.encode(claims))
    SingleUseAccessToken.new(attrs)
  end

  def self.use(jwt)
    begin
      JwtManager.verify(jwt)
    rescue JWT::DecodeError
      return nil
    end

    token = find_by(jwt: jwt, used_at: nil)

    if token.present?
      token.use
      token
    else
      nil
    end
  end

  def use
    update_column(:used_at, Time.current) if used_at.nil?
  end

  def to_json(params = nil)
    {
      agent_id: user.username,
      access_token: jwt,
      expires_at: exp,
      created_at: iat,
    }.to_json
  end

end
