# frozen_string_literal: true

class RescueJob < Job

  before_create :init_partner

  def init_partner
    Rails.logger.debug "init_partner #{inspect}"

    self.partner_live = site.present? ? site.open_for_business? : false
  end

  aasm column: :status, enum: true, no_direct_assignment: false, whiny_persistence: true do # need direct assigment to allow undo_storage
    event :partner_partner_remove_driver do
      transitions from: :dispatched, to: :dispatched, guard: :partner_job?
    end

    event :partner_draft do
      transitions from: :predraft, to: :draft, guard: :partner_job?
    end

    event :partner_pending, after: :remove_rescue_driver do
      transitions from: [:dispatched, :enroute, :onsite, :towing, :towdestination], to: :pending, guard: :partner_job?
    end

    event :partner_dispatched do
      transitions from: [:pending, :enroute, :onsite, :towing, :towdestination, :completed, :canceled, :goa], to: :dispatched, guard: :partner_job?
    end

    event :partner_enroute do
      transitions from: [:pending, :dispatched, :onsite, :towing, :towdestination, :completed, :canceled, :goa], to: :enroute, guard: :partner_job?
    end

    event :partner_onsite do
      transitions from: [:pending, :dispatched, :enroute, :towing, :towdestination, :completed, :canceled, :goa], to: :onsite, guard: :partner_job?
    end

    event :partner_towing do
      transitions from: [:pending, :dispatched, :enroute, :onsite, :towdestination, :completed, :canceled, :goa], to: :towing, guard: :partner_job?
    end

    event :partner_towdestination do
      transitions from: [:pending, :dispatched, :enroute, :onsite, :towing, :completed, :canceled, :goa], to: :towdestination, guard: :partner_job?
    end

    event :partner_completed do
      transitions from: [:pending, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :canceled, :goa], to: :completed, guard: :partner_job?
    end

    event :partner_stored, after: [:undo_release, :set_invoice_dirty] do #:schedule_create_invoice_always
      transitions from: [:released], to: :stored
    end

    event :partner_released, after: [:release_storage] do
      transitions from: [:stored], to: :released
    end

    event :partner_canceled, after: :cancel_invoice do
      transitions from: [:pending, :draft, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :canceled, :goa], to: :canceled, guard: :partner_job?
    end

    event :partner_goa do
      transitions from: [:accepted, :pending, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :canceled], to: :goa, guard: :partner_job?
    end

    event :partner_deleted do
      transitions from: [:draft, :completed, :canceled], to: :deleted, guard: [:partner_job?, :can_delete?]
    end
  end

  def partner_set_driver
    if pending?
      partner_dispatch
    end
  end

  def partner_remove_driver
    partner_partner_remove_driver
  end

  def swoop_status_method_name(status)
    "swoop_partner_override_" + status.delete(' ').downcase
  end

  def invoice_method_name
    'partner'
  end

  def email_notes
    partner_notes
  end

  def email_dispatcher
    partner_dispatcher
  end

  def invoice_editable?
    true
  end

  def can_delete?
    !rescue_company.has_feature?(Feature::DISABLE_DELETE_JOB)
  end

  def can_partner_manage_photos?
    true
  end

  def has_drop_location_service_code?
    service_code&.is_rescue_drop_location_code?
  end

end
