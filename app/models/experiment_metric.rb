# frozen_string_literal: true

#
# This class represents the end of the experiment flow when a  metric has been tracked,
#
# Created by Experiments::Service.track_metric, not designed for use elsewhere.
#

class ExperimentMetric < ApplicationRecord

  belongs_to :target, polymorphic: true
  belongs_to :variant

end
