# frozen_string_literal: true

class PartnerSite < Site

  validates :location, presence: true

  has_many :issc_locations, foreign_key: :site_id, autosave: true
  has_many :issc_logins, foreign_key: :site_id, autosave: true
  has_many :isscs, foreign_key: :site_id, autosave: true
  has_many :rescue_providers, foreign_key: :site_id

  after_commit :delete_rescue_providers, on: :update, if: Proc.new { |partner_site| partner_site.deleted_at }

  # PDW: this has hardcoded compnay id doesn't look used...
  #  def dispatchable_sites
  #    sql="""select sites.id as \"site_id\",sites.location_id,rescue_providers.id as \"rescue_provider_id\", array_agg(isscs.contractorid) as \"contractorids\" from sites
  #           left join rescue_providers on (sites.company_id=rescue_providers.provider_id and rescue_providers.site_id=sites.id and rescue_providers.company_id=4403)
  #           left join isscs on (sites.id=isscs.site_id)
  #           where sites.company_id=2
  #           group by sites.id,rescue_providers.id"""
  #  #    ApplicationRecord.connection.execute(sql)
  #    rows=Site.connection.select_all(sql).to_hash
  #  end

  def dispatchable_by?(fleet_company)
    rescue_provider = rescue_provider_for_company(fleet_company)
    rescue_provider && !rescue_provider.deleted_at ? true : false
  end

  def rescue_provider_for_company(fleet_company)
    rescue_providers.detect { |rp| rp.company_id == fleet_company.id }
  end

  def contractorids(fleet_company)
    clientid = fleet_company.issc_client_id

    # TODO This was removed in ENG-7425 because somehow this is being picked up from cache on new Account instance load.
    # list = isscs.select { |issc| issc.clientid == clientid && !issc.deleted_at && !issc.delete_pending_at }
    # list.map(&:contractorid).uniq
    Issc.select('contractorid').where(site: self, clientid: clientid, deleted_at: nil, delete_pending_at: nil)
      .distinct
      .map(&:contractorid)
  end

  def isscs_by_fleet(fleet_company)
    # TODO This was removed in ENG-7425 because somehow this is being picked up from cache on new Account instance load.
    # list = isscs.select { |issc| issc.clientid == fleet_company.issc_client_id && !issc.deleted_at }
    # list.sort_by { |issc| issc.contractorid }
    isscs.where(clientid: fleet_company.issc_client_id, deleted_at: nil).order(:contractorid)
  end

  def issc_location_by_fleet(fleet_company)
    issc_locations.reverse_each do |issc_loc|
      if (issc_loc.fleet_company == fleet_company) && issc_loc.deleted_at.nil?
        return issc_loc
      end
    end
    nil
  end

  def issc_locationid(fleet_company)
    issc_location_by_fleet(fleet_company).try(:locationid)
  end

  def facility(fleet_company)
    SiteFacilityMap.find_by(fleet_company: fleet_company, deleted_at: nil, site: self)
  end

  def set_issc_location_locationid(fleet_company, new_locationid)
    existing = issc_location_by_fleet(fleet_company)

    if !existing || (!existing.locationid && new_locationid) || existing.locationid != new_locationid
      if existing
        existing.deleted_at = Time.now
      end
      if new_locationid
        issc_locations.build(fleet_company: fleet_company, locationid: new_locationid, company: company)
      end
    end
  end

  def issc_login(fleet_company)
    issc_logins.reverse_each do |issc_login|
      if (issc_login.fleet_company == fleet_company) && issc_login.deleted_at.nil?
        return issc_login
      end
    end
    nil
  end

  def set_issc_login(fleet_company, username, password)
    existing = issc_login(fleet_company)

    if !existing || (!(existing[:username]) && username) || (!(existing[:password]) && password) || existing[:username] != username || existing[:password] != password
      if existing
        existing.deleted_at = Time.now
      end
      if username && password
        issc_logins.build(fleet_company: fleet_company, username: username, password: password)
      end
    end
    existing
  end

  def taxid
    if owner_company
      owner_company.tax_id
    else
      company.tax_id
    end
  end

  private

  def delete_rescue_providers
    rescue_providers.each do |rescue_provider|
      rescue_provider.update!(deleted_at: Time.now)
    end
  end

end
