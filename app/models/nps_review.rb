# frozen_string_literal: true

class NpsReview < Review

  def get_questions(user)
    default = [
      [
        "%{fleet_company} (1/3): How well were you kept informed while you waited for your tower to arrive? (Reply: 1 - 10, 10 is best)",
        "%{partner_company} (1/3): How well were you kept informed while you waited for our tower to arrive? (Reply: 1 - 10, 10 is best)",
      ],
      [
        "%{fleet_company} (2/3): How well did the tower adhere to the promised ETA? (Reply: 1 - 10, 10 is best)",
        "%{partner_company} (2/3): How well did we adhere to the promised ETA? (Reply: 1 - 10, 10 is best)",
      ],
      [
        "%{fleet_company} (3/3): How likely are you to recommend %{fleet_company} to family, friends, or colleagues? (Reply: 1 - 10, 10 is best)",
        "%{partner_company} (3/3): How likely are you to recommend %{partner_company} to family, friends, or colleagues? (Reply: 1 - 10, 10 is best)",
      ],
      ["%{fleet_company}: Thanks for your rating! How do you think we can improve?", "%{partner_company}: Thanks for your rating! How do you think we can improve?"],
    ]

    default
  end

  def format_nps_question(index)
    nps_questions = get_questions(user)
    question = nps_questions[index]

    raise ArgumentError, "Invalid question for #{company.name} Survery: #{index}" if !question

    if company.fleet? && company.permission_to_send_reviews_from
      t = question[0]
    else
      t = question[1]
    end

    t % { fleet_company: company.name, partner_company: job.rescue_company.name }
  end

  def set_nps_review(question: 0)
    self.text = format_nps_question(question)
  end

  def set_review_text
    set_nps_review
  end

  def process_response(twilio_response)
    rating = Review.parse_rating twilio_response.body, max: 10
    index = -1
    if nps_question1.nil?
      if rating != -1
        index = 1
        self.nps_question1 = rating
      end
    elsif nps_question2.nil?
      if rating != -1
        index = 2
        self.nps_question2 = rating
      end
    elsif nps_question3.nil?
      if rating != -1
        index = 3
        self.nps_question3 = rating
      end
    elsif nps_feedback.nil?
      index = 4
      self.nps_feedback = twilio_response.body
    end
    Rails.logger.debug "NPSReview #{index}"
    set_nps_review(question: index) if index != 4
    save!

    send_to_slack((index == 3), 3)
    nps_slack_feedback_message if index == 4

    send_review_sms if index != 4 && nps_feedback.blank?
  end

end
