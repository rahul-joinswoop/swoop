# frozen_string_literal: true

#
# This calss represents an experiement we are running, typically an A/B style experiment
# a.k.a split in split.io
#
class Experiment < ApplicationRecord

  validates :name, uniqueness: true
  has_many :variants

end
