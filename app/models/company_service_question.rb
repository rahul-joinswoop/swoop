# frozen_string_literal: true

class CompanyServiceQuestion < ApplicationRecord

  belongs_to :company
  belongs_to :service_code
  belongs_to :question
  has_many :answers, through: :question

end
