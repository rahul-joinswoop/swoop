# frozen_string_literal: true

class Command < ApplicationRecord

  include RedisConcerns

  belongs_to :event_operation
  belongs_to :user
  belongs_to :company
  belongs_to :target, polymorphic: true

  #  validates :event_operation, presence: true

  after_create:set_swoop_command_uuid
  after_create:set_command_id

  def set_swoop_command_uuid
    ENV['SWOOP_COMMAND_UUID'] = request
  end

  def set_command_id
    RequestStore.store[:command] = self
  end

  #  def run
  #    ENV['SWOOP_COMMAND_UUID']=self.request
  #    clz_name="Commands::#{self.service_type.classify}"
  #    clz=clz_name.safe_constantize
  #    if clz
  #      so=clz.new
  #      so.load_from_command(self)
  #      so.call
  #    end
  #  end

end

# 2015-10-19 09:11:43.807 -DEBUG- publishing {"id":2117,"operation":"update","target":{"id":362,"first_name":"Asd","last_name":null,"email":null,"status":"Completed","phone":"+14156039663","service":"Battery Jump","make":"Bugatti","model":"Veyron 16.4","year":2006,"color":"WH","vin":null,"license":null,"service_location":{"address":"1121 Killarney Ln, Burlingame, CA 94010, USA","lat":37.592962,"lng":-122.378808,"street":"1121 Killarney Ln","city":"Burlingame","state":"CA","zip":"94010"},"drop_location":{"address":"Cincinnati, OH 45202, USA","lat":null,"lng":null,"street":null,"city":null,"state":null,"zip":null},"notes":null,"partner_notes":"Description dafs","account":null,"owner_company":{"id":1,"name":"Ted \u0026 Al's"},"rescue_company":{"id":1,"name":"Ted \u0026 Al's"},"created_at":"2015-10-16T17:49:17.382Z","price":34.0,"owner_sequence":121,"rescue_sequence":111,"account_id":null,"get_location_attempts":12}} to company_5e3cf308c6
