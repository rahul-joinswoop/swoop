# frozen_string_literal: true

# mainly used to update FE with current service_ids or addon_ids through WS.
class ServiceCodeIdList

  include RedisConcerns
  include Publishable
  include ActiveModel::Serialization

  attr_reader :company_id, :service_code_ids, :addon

  # @company_id_to_be_added_to_json - the company_id to be rendered on json.
  #
  # @company_id_to_publish_to - company id to whom it will be sent through WS
  #    for instance, it can be fleet service ids being sent to a partner,
  #    so: company_id_to_be_added_to_json <a fleet_company_id>, company_id_to_publish_to: <partner_id>
  #
  # @service_code_ids: list of service_code ids
  #
  # @addon: true/false
  def initialize(company_id_to_be_added_to_json:, company_id_to_publish_to:, service_code_ids:, addon:)
    @company_id = company_id_to_be_added_to_json
    @company_id_to_publish_to = company_id_to_publish_to
    @service_code_ids = service_code_ids
    @addon = addon
  end

  def listeners
    listener = Set.new

    listener << Company.select(:id, :type, :uuid).find(@company_id_to_publish_to)
  end

  def render_to_hash
    ServiceCodeIdListSerializer.new(self).serializable_hash
  end

end
