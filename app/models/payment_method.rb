# frozen_string_literal: true

class PaymentMethod < ApplicationRecord

  ACH = 'ACH'
  AMERICAN_EXPRESS = 'American Express'
  CASH = 'Cash'
  CHECK = 'Check'
  CREDIT_CARD = 'Credit Card'
  DEBIT_CARD = 'Debit Card'
  DISCOVER = 'Discover'
  MASTERCARD = 'MasterCard'
  VISA = 'Visa'

  ALL_PAYMENT_METHODS = [
    ACH,
    AMERICAN_EXPRESS,
    CASH,
    CHECK,
    CREDIT_CARD,
    DEBIT_CARD,
    DISCOVER,
    MASTERCARD,
    VISA,
  ].freeze

  def self.init_all
    ALL_PAYMENT_METHODS.each do |name|
      PaymentMethod.find_or_create_by!(name: name)
    end
  end

end
