# frozen_string_literal: true

class CompaniesVehicleCategory < ApplicationRecord

  belongs_to :company
  belongs_to :vehicle_category

end
