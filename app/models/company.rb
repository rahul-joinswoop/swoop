# frozen_string_literal: true

class Company < BaseModel

  include Company::Searchable
  include CompanyNames
  include CompanyPredicates
  include GraphQLSubscriber
  include IdentityCache
  include SoftDelete
  include Utils

  EMAIL_ADMINS_REVIEW = 'all_admins'
  DISTANCE_ROUND_NEAREST_TENTH = "Nearest Tenth"
  DISTANCE_ROUND_NEAREST = "Round Nearest"
  DISTANCE_ROUND_UP = "Round Up"

  DEFAULT_SUBSCRIPTION_STATUS_ID = 1

  DEFAULT_PAYMENT_TYPE_NAMES = [
    CustomerType::CUSTOMER_PAY,
    CustomerType::COVERED,
  ].freeze

  DEFAULT_LOCATION_TYPES_FOR_NEW_COMPANY =
    [
      LocationType::AUTO_BODY_SHOP,
      LocationType::BLOCKING_TRAFFIC,
      LocationType::BUSINESS,
      LocationType::GATED_COMMUNITY,
      LocationType::HIGHWAY,
      LocationType::IMPOUND_LOT,
      LocationType::INTERSECTION,
      LocationType::LOCAL_ROADSIDE,
      LocationType::LOW_CLEARANCE,
      LocationType::PARKING_GARAGE,
      LocationType::PARKING_LOT,
      LocationType::POINT_OF_INTEREST,
      LocationType::RESIDENCE,
      LocationType::STORAGE_FACILITY,
      LocationType::TOW_COMPANY,
      LocationType::DEALERSHIP,
    ].freeze

  # TODO: These companies are special and currently being looked up by name.
  # Some of these are also referenced as constants in FleetCompany, but since
  # that's a subclass of this class, we can reference them here.
  SPECIAL_COMPANY_NAMES = [
    AGERO,
    ENTERPRISE_FLEET_MANAGEMENT,
    ENTERPRISE,
    FARMERS,
    FLEET_RESPONSE,
    LINCOLN,
    SWOOP,
    TESLA,
    TURO,
  ].freeze

  NOT_SUPPORTED_ERROR_MSG = '%{value} is not supported.'
  MILE = 'mi'
  KILOMETER = 'km'
  SUPPORTED_DISTANCE_UNITS = [MILE, KILOMETER].freeze
  ENGLISH = 'en'
  GERMAN = 'de'
  SUPPORTED_LANGUAGES = [ENGLISH, GERMAN].freeze

  # TODO need to drop these
  self.ignored_columns = %w(auto_assign_distance)

  alias_attribute :job_update_email, :support_email

  belongs_to :primary_contact, class_name: "User", foreign_key: "primary_contact_id", inverse_of: :company
  belongs_to :support_contact, class_name: "User", foreign_key: "support_contact_id", inverse_of: :company
  belongs_to :payment_contact, class_name: "User", foreign_key: "payment_contact_id", inverse_of: :company
  belongs_to :auto_driver, class_name: "User", foreign_key: "auto_driver_id", inverse_of: :company
  belongs_to :auto_truck, class_name: "RescueVehicle", foreign_key: "auto_truck_id", inverse_of: :company
  belongs_to :invited_by_user, class_name: "User"
  belongs_to :invited_by_company, class_name: "Company"
  belongs_to :location
  belongs_to :referer
  belongs_to :network_manager, class_name: "User"

  has_and_belongs_to_many :products
  has_and_belongs_to_many :features
  has_and_belongs_to_many :reports, join_table: :company_reports

  has_and_belongs_to_many :fleet_in_house_clients,
                          -> { where(rescue_providers: { deleted_at: nil }).where(companies: { in_house: true, deleted_at: nil }).distinct },
                          class_name: "FleetCompany",
                          join_table: "rescue_providers",
                          association_foreign_key: "company_id",
                          foreign_key: "provider_id",
                          uniq: true

  has_and_belongs_to_many :fleet_managed_clients,
                          -> { where(rescue_providers: { deleted_at: nil }).where(companies: { in_house: [nil, false], deleted_at: nil }).distinct },
                          class_name: "FleetCompany",
                          join_table: "rescue_providers",
                          association_foreign_key: "company_id",
                          foreign_key: "provider_id",
                          uniq: true

  belongs_to :subscription_status

  has_many :fleet_jobs, class_name: "Job", foreign_key: "fleet_company_id", inverse_of: :fleet_company
  has_many :rescue_jobs, class_name: "Job", foreign_key: "rescue_company_id", inverse_of: :rescue_company
  has_many :vehicles, class_name: "RescueVehicle"
  has_many :trailers, class_name: "TrailerVehicle"
  has_many :users, class_name: "User", foreign_key: "company_id", inverse_of: :company
  has_many :accounts
  has_many :sites
  has_many :rates, -> { where deleted_at: nil }, inverse_of: :company
  has_many :live_sites, -> { where deleted_at: nil }, class_name: "Site", inverse_of: :company
  has_many :partner_sites, -> { where deleted_at: nil, type: 'PartnerSite' }, class_name: "PartnerSite", inverse_of: :company
  has_many :poi_sites, -> { where deleted_at: nil }, class_name: "PoiSite", inverse_of: :company
  has_many :company_service_questions
  has_many :companies_vehicle_categories
  has_many :vehicle_categories, through: :companies_vehicle_categories
  has_many :departments
  has_many :storage_types
  has_many :customers
  has_many :surveys

  # Note: use API::Jobs::StatusReasonTypes services to fetch all JobStatusReasonTypes available for a specific Job. This relationship is primarily used by that service.
  has_many :custom_company_job_status_reason_types, class_name: 'CompanyJobStatusReasonType'
  has_many :custom_job_status_reason_types, through: :custom_company_job_status_reason_types, source: :job_status_reason_type

  # service_code stuff
  has_many :companies_services
  has_many :service_codes, through: :companies_services

  # see also services below
  has_and_belongs_to_many :addons, -> { addons.distinct }, join_table: :companies_services, class_name: "ServiceCode"

  # TODO - why doesn't this work as a has_and_belongs_to_many relationship like
  # addons? i think because we already have a Service model?
  has_many :services, -> { services.distinct }, through: :companies_services, class_name: "ServiceCode", source: :service_code

  has_many :locations, through: :sites
  # PDW cashes with accounts definition above has_many :accounts, foreign_key: :fleet_company_id
  # PDW should only be on fleet company (cant get it to work though)
  has_many :companies_customer_types
  has_many :customer_types, -> { distinct }, through: :companies_customer_types
  has_many :company_symptoms,  dependent: :destroy
  has_many :symptoms, through: :company_symptoms
  has_many :service_aliases

  has_many :companies_issues, class_name: 'CompaniesIssues'
  has_many :issues, through: :companies_issues
  has_many :tags

  # Does this need to me migrated into the new permissions structure?
  has_many :role_permissions, class_name: 'CompanyRolePermission'

  scope :live_or_non_rescue, -> { where("(companies.live = ?) OR (companies.type != 'RescueCompany')", true) }
  scope :not_demo, -> { where("companies.demo is not true") }

  alias payment_types customer_types

  has_many :companies_location_types, -> { where(deleted_at: nil) }, inverse_of: :company
  has_many :location_types, through: :companies_location_types

  # Is this the right place for it or should this be on GraphQL application?
  has_many :company_roles
  has_many :roles, through: :company_roles

  accepts_nested_attributes_for :primary_contact
  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :sites

  has_many :settings

  has_and_belongs_to_many :looker_dashboards

  has_and_belongs_to_many :groups

  # any company type can have any number of oauth applications
  has_many :oauth_applications, class_name: 'Doorkeeper::Application', as: :owner

  validates_uniqueness_of :agero_vendor_id, allow_nil: true, if: :not_deleted?
  validate :review_email_presence
  validate :validate_not_special_company_name
  validate :validate_network_manager
  validates_presence_of :currency, :distance_unit, :language
  validates :currency, inclusion:
    { in: Money::Currency.all.map(&:iso_code), message: NOT_SUPPORTED_ERROR_MSG }, unless: -> { currency.blank? }
  validates :distance_unit, inclusion:
    { in: SUPPORTED_DISTANCE_UNITS, message: NOT_SUPPORTED_ERROR_MSG }, unless: -> { distance_unit.blank? }
  validates :language, inclusion: { in: SUPPORTED_LANGUAGES, message: NOT_SUPPORTED_ERROR_MSG },
                       unless: -> { language.blank? }

  before_validation :normalize_phone
  before_validation :set_default_language, on: :create
  after_commit :audit_subscription_status, on: :update
  before_save :before_save
  after_commit :identify_on_analytics, if: :saved_change_to_name?
  after_commit :update_dependent_indexes, on: :update
  after_commit do
    Subscribable.trigger_job_updated fleet_jobs.union(rescue_jobs)
    Subscribable.trigger_user_updated users
  end

  def self.default_distance_unit
    ENV['DEFAULT_DISTANCE_UNIT']
  end

  def metric_distance_units?
    distance_unit == 'km'
  end

  def round_distance(distance)
    if invoice_mileage_rounding == DISTANCE_ROUND_UP
      distance.to_f.ceil
    elsif invoice_mileage_rounding == DISTANCE_ROUND_NEAREST
      distance.to_f.round
    elsif invoice_mileage_rounding == DISTANCE_ROUND_NEAREST_TENTH
      distance.to_f.round(1)
    else
      distance.to_f.round(1)
    end
  end

  def customer_account
    accounts.find_by(name: Account::CUSTOMER)
  end

  def review_email_presence
    if !email_admins_review? && review_email.blank?
      errors.add(:review_email, "Review email cannot be blank")
    end
  end

  def email_admins_review?
    review_email_recipient_key == EMAIL_ADMINS_REVIEW
  end

  # used by rollbar
  def email
    support_email || dispatch_email
  end

  def review_email_recipient_key
    self.[](:review_email_recipient_key) || EMAIL_ADMINS_REVIEW
  end

  def admins
    users.not_deleted.with_role(:admin)
  end

  def audit_subscription_status
    return if subscription_status_id.nil?
    AuditChangedCompanySubscriptionStatusWorker.perform_async(id)
  end

  # in general we don't want addons to show up in companies services for reasons i don't understand
  def companies_services_excluding_addons
    companies_services
      .joins(:service_code)
      .merge(ServiceCode.services)
      .order("service_codes.name")
  end

  # find all the questions for a given service_code (note we exclude add-ons here)
  def questions_for_service_code(service_code)
    companies_services_excluding_addons
      .find_by!(service_code: service_code)
      .questions
  end

  class << self

    def swoop
      id = swoop_id
      find_by(id: id) if id
    end

    def swoop_id
      cached_company_id(Company::SWOOP)
    end

    def agero_id
      cached_company_id(Company::AGERO)
    end

    def agero
      id = agero_id
      find_by(id: id) if id
    end

    def tesla
      id = tesla_id
      find_by(id: id) if id
    end

    def tesla_id
      cached_company_id(Company::TESLA)
    end

    def enterprise
      id = enterprise_id
      find_by(id: id) if id
    end

    def enterprise_id
      cached_company_id(Company::ENTERPRISE)
    end

    def turo
      id = turo_id
      find_by(id: id) if id
    end

    def turo_id
      cached_company_id(Company::TURO)
    end

    def farmers
      id = farmers_id
      find_by(id: id) if id
    end

    def farmers_id
      cached_company_id(Company::FARMERS)
    end

    def lincoln
      id = lincoln_id
      find_by(id: id) if id
    end

    def lincoln_id
      cached_company_id(Company::LINCOLN)
    end

    def fleet_response
      id = fleet_response_id
      find_by(id: id) if id
    end

    def fleet_response_id
      cached_company_id(Company::FLEET_RESPONSE)
    end

    private

    def find_first_named_company(name)
      # Note: we don't want to use `find_by` because we want the `order by` clause that comes with `first`
      where(name: name).first
    end

    def cached_company_id(name)
      Rails.cache.fetch("Company.id_for(#{name})", expires_in: 1.day) do
        company = find_first_named_company(name)
        company.id if company && company.deleted_at.nil?
      end
    end

  end

  # PDW: the in_house flag is really not clear, we should probably migrate the various types to these values
  # PDW: introduced for ENG-5423
  def subtype
    case self
    when RescueCompany
      'Rescue'
    when FleetCompany
      if issc_client_id
        'FleetMotorClub'
      else
        if in_house
          'FleetInHouse'
        else
          'FleetManaged'
        end
      end
    when SuperCompany
      'Super'
    end
  end

  def defaults
    add_services(service_list_for_new_company)
    add_vehicle_categories(VehicleCategory.standard_items)
    add_location_types_by_name(DEFAULT_LOCATION_TYPES_FOR_NEW_COMPANY)
    add_addons(ServiceCode.standard_items.addons)
  end

  def service_list_for_new_company
    ServiceCode.standard_items.services
  end

  def before_save
    if !uuid
      self.uuid = SecureRandom.hex(5)
    end
  end

  def channel_name
    if !uuid
      msg = "#{name} has no uuid"
      logger.error msg
      Rollbar.error(StandardError.new(msg))
    end
    "company_#{uuid}"
  end

  # Fetch the string value of a company setting.
  # The value will be a string by default, but can be cast to :integer, :float, or :boolean.
  def setting(key, cast = :string, default = nil)
    key = key.to_s
    setting = settings.detect { |s| s.key == key }
    value = (setting ? setting.value : default)
    if value || cast == :boolean
      value = case cast
              when :string
                value.to_s
              when :integer
                Integer(value)
              when :float
                Float(value)
              when :boolean
                value.present?
              end
    end
    value
  end

  def create_or_update_setting!(key:, value:)
    setting = settings.where(key: key).first_or_create!

    setting.update! value: value

    setting
  end

  def render_to_hash
    controller = ActionController::Base.new
    # Calling reload here to make sure the company logo is what's currently persisted in the database
    JSON.parse(controller.render_to_string("api/v1/companies/_company", locals: { company: reload, accounts: false }))
  end

  def listeners
    ret = Set.new
    ret << self
    ret << Company.swoop

    # if this company is an ISSC Motor Club, need to update all partners as may have had an issc status change
    if issc_motor_club?
      Company.where(type: "RescueCompany").each do |company|
        ret << company
      end
    end

    ret
  end

  def inherited_pickup_waiver
    pickup_waiver.blank? ? Company.swoop.pickup_waiver : pickup_waiver
  end

  def inherited_dropoff_waiver
    dropoff_waiver.blank? ? Company.swoop.dropoff_waiver : dropoff_waiver
  end

  def inherited_invoice_waiver
    invoice_waiver.blank? ? Company.swoop.invoice_waiver : invoice_waiver
  end

  def add_customer_types_by_name(types)
    types.each do |type_name|
      customer_type = CustomerType.find_by(name: type_name)
      if customer_type
        if !customer_types.find_by(name: type_name)
          customer_types << customer_type
        end
      else
        raise ArgumentError, "Unable to find Customer Type: '#{type_name}' in DB Customer Types table, please add"
      end
    end
  end

  def add_services(services)
    return if services.blank?

    services.each do |service|
      if !service_codes.find_by(name: service.name)
        service_codes << service
      end
    end
  end

  def add_addons(addons)
    return if addons.blank?

    addons.each do |addon|
      if !service_codes.find_by(name: addon.name)
        service_codes << addon
      end
    end
  end

  def add_symptoms(symptoms)
    return if symptoms.blank?

    symptoms.each do |symptom|
      if !self.symptoms.find_by(name: symptom.name)
        self.symptoms << symptom
      end
    end
  end

  def add_features_by_name(feature_names)
    CompanyService::ChangeFeatures.new(
      company_id: id,
      feature_names: feature_names
    ).execute
  end

  def add_vehicle_categories(categories)
    return if categories.blank?

    categories.each do |category|
      if !vehicle_categories.find_by(name: category.name)
        vehicle_categories << category
      end
    end
  end

  def add_location_types_by_name(location_type_names)
    location_types_in_db = LocationType.where(name: location_type_names)

    comapany_location_type_names = location_types.map(&:name)

    location_types_in_db.each do |location_type|
      location_types << location_type if !comapany_location_type_names.include? location_type.name
    end
  end

  def add_reports_by_name(report_names)
    report_names.each do |report_name|
      report = Report.find_by(name: report_name)

      if report
        if !reports.find_by(name: report_name)
          reports << report
        end
      else
        raise ArgumentError, "Unable to find report: '#{report_name}' in DB reports table, please add"
      end
    end
  end

  def cash_account
    accounts.find_by(name: Account::CASH_CALL)
  end

  def sort_name
    I18n.transliterate(name.to_s.downcase.strip)
  end

  def human_phone
    if phone
      Phonelib.parse(phone).national
    else
      logger.warn "Sending broken email: No phone for %p(#{id})" % name
      nil
    end
  end

  def base_phone
    company = base_company
    company.human_phone
  end

  def base_company
    company = self
    tries = 0
    while !company.parent_company_id.nil?
      company = company.parent_company
      tries += 1
      if tries > 5
        raise ArgumentError, "Traversed 5 companies in the heirarchy #{company.id}"
      end
    end
    company
  end

  def accounting_email_or_dispatch_email
    accounting_email || dispatch_email
  end

  def rates_by_account(conditions)
    raise ArgumentError, "Account is required" unless conditions[:account_id]

    rates.where conditions.merge(deleted_at: nil)
  end

  def live_rates?(conditions = {})
    rates.exists?(conditions.merge(live: true, deleted_at: nil))
  end

  def service_ids
    services.pluck(:id)
  end

  def addon_ids
    addons.pluck(:id)
  end

  # TODO Once we get rid of _company.jbuilder in favor of CompanySerializer,
  # we can move this to CompanySerializer
  def role_permissions_by_type_hash
    role_ids_by_type = {}
    role_permissions.each do |permission|
      role_ids = role_ids_by_type[permission.permission_type]
      unless role_ids
        role_ids = []
        role_ids_by_type[permission.permission_type] = role_ids
      end
      role_ids << permission.role_id
    end

    role_ids_by_type.map { |type, role_ids| { type: type, role_ids: role_ids } }
  end

  def graphql_type
    case self
    when FleetCompany
      "ClientCompany"
    when RescueCompany
      "PartnerCompany"
    when SuperCompany
      "SuperCompany"
    else
      Rollbar.error "Fell through on Company#graphql_type", company: self
      type
    end
  end

  def identify_on_analytics
    AnalyticsData::Identify.new(self).call
  end

  def internal_notes_field
    case self
    when SuperCompany
      :swoop_notes
    when FleetCompany
      :fleet_notes
    when RescueCompany
      :partner_notes
    else
      nil
    end
  end

  # This is overwritten in RescueCompany
  def available_user_notification_settings
    []
  end

  private

  def set_default_language
    self.language ||= ENV['DEFAULT_LANGUAGE']
  end

  def normalize_phone
    self.phone = Phonelib.parse(phone).e164 if phone_changed?
  end

  # TODO this is only needed for validating special companies being looked up by hardcoded names.
  # This can be removed if these companies are moved to seeds and the ids can be hard coded instead.
  def validate_not_special_company_name
    return unless name_changed?
    if persisted? && SPECIAL_COMPANY_NAMES.include?(name_was)
      special = Company.find_by(name: name_was)
      if special && id == special.id
        errors.add(:name, "cannot be changed for this company")
      end
    elsif SPECIAL_COMPANY_NAMES.include?(name)
      special = Company.find_by(name: name)
      if special && id != special.id
        errors.add(:name, "cannot be changed to this value")
      end
    end
  end

  def validate_network_manager
    # Don't validate the network manager unless it's been changed since employees can leave
    # the company and we don't want companies to suddenly become invalid when they loose
    # their root access.
    return unless network_manager_id_changed? && network_manager

    if is_a?(RescueCompany)
      errors.add(:network_manager, "must be a root user") unless network_manager.has_role?(:root)
    else
      errors.add(:network_manager, "only available for partner companies")
    end
  end

  # Update search indexes that reference this record.
  def update_dependent_indexes
    if saved_change_to_name?
      SearchIndex::CompanyDependentIndexUpdateWorker.perform_async(id)
    end
  end

end
