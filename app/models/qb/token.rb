# frozen_string_literal: true

module QB
  class Token < ApplicationRecord

    self.table_name = "qb_company_tokens"

    belongs_to :company
    belongs_to :user, foreign_key: :created_by_user_id, inverse_of: :qb_tokens

    validates :company, uniqueness: true, presence: true
    validates :token, uniqueness: true, presence: true

  end
end
