# frozen_string_literal: true

class SmsAlert < ApplicationRecord

  belongs_to :job
  belongs_to :audit_sms

end
