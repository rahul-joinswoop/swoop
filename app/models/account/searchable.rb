# frozen_string_literal: true

# ElasticSearch specific definitions for accounts.
module Account::Searchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    index_name Swoop::ElasticSearch.index_name(:accounts)

    scope :elastic_search_backfill, -> {
      elastic_search_preload.where("company_id IS NOT NULL")
    }

    scope :elastic_search_preload, -> {
      preload([:location, { client_company: :location }])
    }

    after_commit on: [:create, :update], if: :company_id? do
      Indexer.perform_async(:index, Account.name, id)
    end

    after_commit on: [:destroy] do
      Indexer.perform_async(:delete, Account.name, id)
    end

    settings index: {
      number_of_shards: 2,
      number_of_replicas: 1,
      analysis: {
        filter: {
          preserve_asciifolding: {
            type: "asciifolding",
            preserve_original: true,
          },
          strip_punctuation: {
            type: "pattern_replace",
            pattern: "\\p{P}",
            replacement: "",
          },
        },
        analyzer: {
          name_analyzer: {
            type: "custom",
            tokenizer: "standard",
            filter: ["standard", "lowercase", "preserve_asciifolding", "strip_punctuation"],
          },
        },
      },
    } do
      mappings dynamic: 'false' do
        indexes :id, type: :integer
        indexes :name, analyzer: :name_analyzer
        indexes :sort_name, type: :keyword
        indexes :billing_address
        indexes :primary_phone, type: :keyword
        indexes :primary_email
        indexes :accounting_email
        indexes :notes, analyzer: :english, index_options: :offsets
        indexes :deleted_at, type: :date
        indexes :company do
          indexes :id, type: :integer
        end
      end
    end
  end

  def as_indexed_json(_options = {})
    {
      "id" => id,
      "name" => name,
      "sort_name" => sort_name,
      "billing_address" => partner_set_billing_location_or_fleets,
      "primary_phone" => partner_set_phone_or_fleets,
      "primary_email" => primary_email,
      "accounting_email" => partner_set_accounting_email_or_fleets,
      "notes" => notes,
      "company" => { "id" => company_id },
      "deleted_at" => deleted_at,
    }
  end

  private

  def partner_set_accounting_email_or_fleets
    accounting_email || client_company&.accounting_email
  end

  def partner_set_phone_or_fleets
    phone || client_company&.phone
  end

  def partner_set_billing_location_or_fleets
    location&.address || client_company&.location&.address
  end

end
