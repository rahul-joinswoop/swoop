# frozen_string_literal: true

require 'ostruct'

class FleetInHouseJob < FleetJob

  # notifications
  after_commit do
    if saved_change_to_status? || saved_change_to_rescue_company_id?
      Notifiable.trigger_fleet_in_house_job_assigned self
    end
  end

  aasm column: :status, enum: true, no_direct_assignment: false, whiny_persistence: true do # need direct assigment to allow undo_storage
    # previously fleet_inhouse_override_*
    event :fleet_dispatched do
      transitions from: [:assigned, :accepted, :pending, :unassigned, :enroute, :onsite, :towing, :towdestination, :completed, :canceled, :goa], to: :dispatched, guards: [:fleet_manual?]
    end
    event :fleet_enroute do
      transitions from: [:assigned, :accepted, :pending, :unassigned, :dispatched, :onsite, :towing, :towdestination, :completed, :canceled, :goa], to: :enroute, guards: [:fleet_manual?]
    end

    event :fleet_onsite do
      transitions from: [:assigned, :accepted, :pending, :unassigned, :dispatched, :enroute, :towing, :towdestination, :completed, :canceled, :goa], to: :onsite, guards: [:fleet_manual?]
    end

    event :fleet_towing do
      transitions from: [:assigned, :accepted, :pending, :unassigned, :dispatched, :enroute, :onsite, :towdestination, :completed, :canceled, :goa], to: :towing, guards: [:fleet_manual?]
    end

    event :fleet_towdestination do
      transitions from: [:assigned, :accepted, :pending, :unassigned, :dispatched, :enroute, :onsite, :towing, :completed, :canceled, :goa], to: :towdestination, guards: [:fleet_manual?]
    end

    event :fleet_completed do
      transitions from: [:assigned, :accepted, :pending, :unassigned, :dispatched, :enroute, :onsite, :towing, :towdestination, :canceled, :goa], to: :completed, guards: [:fleet_manual?]
    end

    event :fleet_goa do
      transitions from: [:accepted, :pending, :unassigned, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :canceled], to: :goa, guards: [:fleet_manual?]
      transitions from: [:accepted, :pending, :unassigned, :assigned, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :canceled], to: :goa
    end

    event :fleet_reassigned do
      transitions from: [:assigned, :accepted, :dispatched], to: :reassigned
      transitions from: [:pending, :dispatched], to: :reassigned, guard: :fleet_manual?
    end

    event :fleet_inhouse_assign do
      transitions from: :pending, to: :assigned, if: :fleet_demo?
      transitions from: [:pending, :unassigned, :dispatched], to: :dispatched, if: :partner_dispatch_to_truck_enabled?
      transitions from: [:pending, :unassigned, :dispatched], to: :assigned, if: :partner_live_and_open?
      transitions from: [:pending, :unassigned], to: :pending, if: :scheduled?
      transitions from: [:pending, :unassigned], to: :dispatched
    end
  end

  def swoop_status_method_name(status)
    "swoop_fleet_inhouse_override_" + status.delete(' ').downcase
  end

  def assigning_company
    fleet_company
  end

  def partner_dispatch_to_truck_enabled?
    rescue_company.has_feature?(Feature::PARTNER_DISPATCH_TO_TRUCK)
  end

end
