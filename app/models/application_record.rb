# frozen_string_literal: true

# this is the base class _all_ models should inherit from (directly or indirection
# via BaseModel).
class ApplicationRecord < ActiveRecord::Base

  self.abstract_class = true

  strip_attributes

  class << self

    # Helper method for executing an arbitrary query with santizied params.
    # Useful for things like executing stored procedures.
    # The sql_with_conditions can be an array with `[SQL, var1, var2, ...]` or
    # a string and hash like `(sql, :var_1 => val, ...)`
    def execute_query(*sql_with_conditions)
      sql = sanitize_sql(sql_with_conditions.flatten)
      connection.execute(sql)
    end

    # from http://samueldavies.net/2017/05/06/how-to-automatically-retry-transactions-instead-of-raising-on-activerecord-preparedstatementcacheexpired/
    # Make all transactions for all records automatically retriable in the event of
    # cache failure
    def in_transaction?
      ApplicationRecord.connection.current_transaction.open?
    end

    # Retry automatically on ActiveRecord::PreparedStatementCacheExpired and
    # ActiveRecord::UndefinedColumn
    #
    # Do not use this for transactions with side-effects unless it is acceptable
    # for these side-effects to occasionally happen more than once
    def transaction(**opts, &block)
      # we have to track these retry events separately - it's possible for both
      # to occur in a single transaction, for example (assuming User just dropped
      # a column):
      #
      # User.transaction do
      #   User.first.save!
      #   User.joins(:company).includes(:company).where(id: 1).first.save!
      # end
      #
      prepared_statement_retried ||= false
      undefined_column_retried ||= false
      super
    rescue StandardError => e
      # some parts of rails will swallow the original exception and raise a new
      # one - see here for a list:
      #
      # https://github.com/rails/rails/blob/5033e2c08232a5e6e740fb6c117daeb51d5ef2c1/actionpack/lib/action_dispatch/middleware/exception_wrapper.rb
      #
      # because of that we can't just rescue ActiveRecord::PreparedStatementCacheExpired -
      # instead we have to rescue any StandardError and check to see if the
      # exception is a ActiveRecord::PreparedStatementCacheExpired or if it's a
      # rails exception wrapping a ActiveRecord::PreparedStatementCacheExpired
      #
      # this should be DRYed but i'm having a hard time figuring out how?
      if [e, e&.cause].any? { |_e| _e.is_a? ActiveRecord::PreparedStatementCacheExpired }
        error_klass = if e.is_a?(ActiveRecord::PreparedStatementCacheExpired)
                        "#{e.class}"
                      else
                        "#{e.cause.class} (via #{e.class})"
                      end

        if prepared_statement_retried
          Rails.logger.debug "Caught #{error_klass} on retry, raising"
          raise
        else
          Rails.logger.debug "Caught #{error_klass}, retrying"
          prepared_statement_retried = true
          retry
        end
      elsif [e, e&.cause].any? { |_e| _e.is_a? ActiveRecord::UndefinedColumn }
        error_klass = if e.is_a?(ActiveRecord::UndefinedColumn)
                        "#{e.class}"
                      else
                        "#{e.cause.class} (via #{e.class})"
                      end

        if undefined_column_retried
          Rails.logger.debug "Caught #{error_klass} on retry, raising"
          raise
        else
          Rails.logger.debug "Caught #{error_klass}, retrying"
          undefined_column_retried = true
          retry
        end
      else
        raise
      end
    end

  end

end
