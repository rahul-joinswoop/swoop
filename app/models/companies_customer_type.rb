# frozen_string_literal: true

class CompaniesCustomerType < ApplicationRecord

  belongs_to :company
  belongs_to :customer_type

end
