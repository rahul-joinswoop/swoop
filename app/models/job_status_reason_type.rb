# frozen_string_literal: true

# This associates a JobStatus with a reason (column 'text' in DB).
# It also contains an unique field called 'key' so we can use
# it on mappings.
#
# Mainly used by RSC context (job cancel and GOA).
#
# Agero::Rsc::Data::SWOOP_TO_CANCEL_REASONS and Agero::Rsc::Data::SWOOP_TO_GOA_REASONS
# maps it by JobStatusReasonType#key to Agero reasons.
#
# Update:
#
# - we started migrating Accepted status cases to StatusReason structure as well.
# - follow up ticket: https://swoopme.atlassian.net/browse/ENG-8842
#
# - we started migrating Rejected status cases to StatusReason structure as well.
# - follow up ticket: https://swoopme.atlassian.net/browse/ENG-8845
#

class JobStatusReasonType < ApplicationRecord

  include IdentityCache
  include StandardOrCustomCollection

  cache_index :key, unique: true
  belongs_to :job_status

  validates :job_status, :text, :key, presence: true
  validates :job_status, uniqueness: {
    scope: :text,
    message: "has already been taken for the given text",
  }

  scope :accepted, -> { where(job_status_id: JobStatus::ACCEPTED) }
  scope :canceled, -> { where(job_status_id: JobStatus::CANCELED) }
  scope :goa, -> { where(job_status_id: JobStatus::GOA) }
  scope :rejected, -> { where(job_status_id: JobStatus::REJECTED) }

  ACCEPTED_CONSTRUCTION = { job_status_id: JobStatus::ACCEPTED, text: "Construction", key: :accepted_construction, is_standard: true }.freeze
  ACCEPTED_EVENT_IN_AREA = { job_status_id: JobStatus::ACCEPTED, text: "Event In Area", key: :accepted_event_in_area, is_standard: true }.freeze
  ACCEPTED_NONE = { job_status_id: JobStatus::ACCEPTED, text: "None", key: :accepted_none, is_standard: true }.freeze
  ACCEPTED_OTHER = { job_status_id: JobStatus::ACCEPTED, text: "Other", key: :accepted_other, is_standard: true }.freeze
  ACCEPTED_RURAL_DISABLEMENT = { job_status_id: JobStatus::ACCEPTED, text: "Rural Disablement", key: :accepted_rural_disablement, is_standard: true }.freeze
  ACCEPTED_SERVICE_PROVIDER_ISSUE = { job_status_id: JobStatus::ACCEPTED, text: "Service Provider Issue", key: :accepted_service_provider_issue, is_standard: true }.freeze
  ACCEPTED_TRAFFIC = { job_status_id: JobStatus::ACCEPTED, text: "Traffic", key: :accepted_traffic, is_standard: true }.freeze
  ACCEPTED_WEATHER = { job_status_id: JobStatus::ACCEPTED, text: "Weather", key: :accepted_weather, is_standard: true }.freeze

  CANCEL_ANOTHER_JOB_PRIORITY = { job_status_id: JobStatus::CANCELED, text: "Another job priority", key: :cancel_another_job_priority }.freeze
  CANCEL_CUSTOMER_FOUND_ALTERNATE_SOLUTION = { job_status_id: JobStatus::CANCELED, text: "Customer found alternate solution", key: :cancel_customer_found_alternate_solution }.freeze
  CANCEL_NO_REASON_GIVEN = { job_status_id: JobStatus::CANCELED, text: "No reason given", key: :cancel_no_reason_given }.freeze
  CANCEL_OUT_OF_AREA = { job_status_id: JobStatus::CANCELED, text: "Out of area", key: :cancel_out_of_area }.freeze
  CANCEL_PRIOR_JOB_DELAYED = { job_status_id: JobStatus::CANCELED, text: "Prior job delayed", key: :cancel_prior_job_delayed }.freeze
  CANCEL_TRAFFIC_SERVICE_VEHICLE_PROBLEM = { job_status_id: JobStatus::CANCELED, text: "Traffic/service vehicle problem", key: :cancel_traffic_service_vehicle_problem }.freeze

  GOA_CUSTOMER_CANCEL_AFTER_DEADLINE = { job_status_id: JobStatus::GOA, text: "Customer cancel after deadline", key: :goa_customer_cancel_after_deadline }.freeze
  GOA_CUSTOMER_NOT_WITH_VEHICLE = { job_status_id: JobStatus::GOA, text: "Customer not with vehicle", key: :goa_customer_not_with_vehicle }.freeze
  GOA_INCORRECT_EQUIPMENT = { job_status_id: JobStatus::GOA, text: "Incorrect equipment", key: :goa_incorrect_equipment }.freeze
  GOA_INCORRECT_SERVICE = { job_status_id: JobStatus::GOA, text: "Incorrect service", key: :goa_incorrect_service }.freeze
  GOA_UNSUCCESSFUL_SERVICE_ATTEMPT = { job_status_id: JobStatus::GOA, text: "Unsuccessful service attempt", key: :goa_unsuccessful_service_attempt }.freeze
  GOA_WRONG_LOCATION_GIVEN = { job_status_id: JobStatus::GOA, text: "Wrong location given", key: :goa_wrong_location_given }.freeze

  REJECTED_DONT_ACCEPT_PAYMENT_TYPE = { job_status_id: JobStatus::REJECTED, text: 'Do not accept payment type', key: :rejected_dont_accept_payment_type }.freeze
  REJECTED_EQUIPMENT_NOT_AVAILABLE = { job_status_id: JobStatus::REJECTED, text: 'Equipment Not Available', key: :rejected_equipment_not_available }.freeze
  REJECTED_EVENT = { job_status_id: JobStatus::REJECTED, text: 'Event', key: :rejected_event }.freeze
  REJECTED_MORE_THAN_60_MINS = { job_status_id: JobStatus::REJECTED, text: 'Can not service within 60 mins', key: :rejected_more_than_60_mins }.freeze
  REJECTED_NO_DRIVERS_AVAILABLE = { job_status_id: JobStatus::REJECTED, text: 'No Drivers Available', key: :rejected_no_drivers_available, is_standard: true }.freeze
  REJECTED_NO_LONGER_OFFER_SERVICE = { job_status_id: JobStatus::REJECTED, text: 'No longer offer service', key: :rejected_no_longer_offer_service }.freeze
  REJECTED_NOT_INTERESTED = { job_status_id: JobStatus::REJECTED, text: 'Not Interested', key: :rejected_not_interested, is_standard: true }.freeze
  REJECTED_OTHER = { job_status_id: JobStatus::REJECTED, text: 'Other', key: :rejected_other, is_standard: true }.freeze
  REJECTED_OUT_OF_COVERAGE_AREA = { job_status_id: JobStatus::REJECTED, text: 'Out of my coverage area', key: :rejected_out_of_coverage_area }.freeze
  REJECTED_OUT_OF_SERVICE_AREA = { job_status_id: JobStatus::REJECTED, text: 'Out of Service Area', key: :rejected_out_of_service_area, is_standard: true }.freeze
  REJECTED_PAYMENT_ISSUE = { job_status_id: JobStatus::REJECTED, text: 'Payment Issue', key: :rejected_payment_issue }.freeze
  REJECTED_PROPER_EQUIPMENT_NOT_AVAILABLE = { job_status_id: JobStatus::REJECTED, text: 'Proper equipment not available', key: :rejected_proper_equipment_not_available }.freeze
  REJECTED_QUOTE_NOT_ACCEPTED = { job_status_id: JobStatus::REJECTED, text: 'Quote Not Accepted', key: :rejected_quote_not_accepted }.freeze
  REJECTED_REFUSED_CALL = { job_status_id: JobStatus::REJECTED, text: 'Refused Call', key: :rejected_refused_call }.freeze
  REJECTED_RESTRICTED_ROADWAY = { job_status_id: JobStatus::REJECTED, text: 'Restricted Roadway', key: :rejected_restricted_roadway }.freeze
  REJECTED_SERVICE_NOT_AVAILABLE = { job_status_id: JobStatus::REJECTED, text: 'Service Not Available', key: :rejected_service_not_available, is_standard: true }.freeze
  REJECTED_TIRE_SIZE = { job_status_id: JobStatus::REJECTED, text: 'Do not have requested tire size', key: :rejected_tire_size }.freeze
  REJECTED_TRAFFIC = { job_status_id: JobStatus::REJECTED, text: 'Traffic', key: :rejected_traffic }.freeze
  REJECTED_UNSAFE_LOCATION = { job_status_id: JobStatus::REJECTED, text: 'Unsafe Location', key: :rejected_unsafe_location }.freeze
  REJECTED_WEATHER = { job_status_id: JobStatus::REJECTED, text: 'Weather', key: :rejected_weather }.freeze

  ALL_JOB_STATUS_REASON_TYPES = [
    ACCEPTED_CONSTRUCTION,
    ACCEPTED_EVENT_IN_AREA,
    ACCEPTED_NONE,
    ACCEPTED_OTHER,
    ACCEPTED_RURAL_DISABLEMENT,
    ACCEPTED_SERVICE_PROVIDER_ISSUE,
    ACCEPTED_TRAFFIC,
    ACCEPTED_WEATHER,
    CANCEL_ANOTHER_JOB_PRIORITY,
    CANCEL_CUSTOMER_FOUND_ALTERNATE_SOLUTION,
    CANCEL_NO_REASON_GIVEN,
    CANCEL_OUT_OF_AREA,
    CANCEL_PRIOR_JOB_DELAYED,
    CANCEL_TRAFFIC_SERVICE_VEHICLE_PROBLEM,
    GOA_CUSTOMER_CANCEL_AFTER_DEADLINE,
    GOA_CUSTOMER_NOT_WITH_VEHICLE,
    GOA_INCORRECT_EQUIPMENT,
    GOA_INCORRECT_SERVICE,
    GOA_UNSUCCESSFUL_SERVICE_ATTEMPT,
    GOA_WRONG_LOCATION_GIVEN,
    REJECTED_DONT_ACCEPT_PAYMENT_TYPE,
    REJECTED_EQUIPMENT_NOT_AVAILABLE,
    REJECTED_EVENT,
    REJECTED_MORE_THAN_60_MINS,
    REJECTED_NO_DRIVERS_AVAILABLE,
    REJECTED_NO_LONGER_OFFER_SERVICE,
    REJECTED_NOT_INTERESTED,
    REJECTED_OTHER,
    REJECTED_OUT_OF_COVERAGE_AREA,
    REJECTED_OUT_OF_SERVICE_AREA,
    REJECTED_PAYMENT_ISSUE,
    REJECTED_PROPER_EQUIPMENT_NOT_AVAILABLE,
    REJECTED_QUOTE_NOT_ACCEPTED,
    REJECTED_REFUSED_CALL,
    REJECTED_RESTRICTED_ROADWAY,
    REJECTED_SERVICE_NOT_AVAILABLE,
    REJECTED_TIRE_SIZE,
    REJECTED_TRAFFIC,
    REJECTED_UNSAFE_LOCATION,
    REJECTED_WEATHER,
  ].freeze

  # It will return the correponding legacy row. Currently only needed when it is 'Accepted' JobStatus
  # This will hopefully be removed soon.
  #
  # tl; dr
  # We started migrating Accept flow to use JobStatusReasonType.
  # First step was adding Accepted JobEtaExplanation rows to JobStatusReasonTypes as well.
  # JobStatusReasonTypes.text for Accepted Status are just a copy of JobEtaExplanation.text.
  #
  # It is primarily used by API::MapJobInput (which is used by GraphQL).
  #
  # The second step will be refactoring Job#eta_explanation_id to use JobStatusReasonType
  # instead, @see class description ^.
  #
  # So for now we need to convert it to a respective JobEtaExplanation
  # when we pass a JobStatusReasonType to update a job to Accepted.
  #
  # It's safe to find_by_text! here because it never changes, and we avoid adding one temp FK column to JobEtaExplanation
  #
  # usage:
  #
  # job_status_reason_type = { job_status_id: JobStatus::ACCEPTED).first
  # job_status_reason_type.find_legacy
  #
  # => <JobEtaExplanation id: 8, text: "Other">
  #
  # TODO - what about JobRejectReason? shouldn't those move here too?

  def find_legacy
    if job_status_id == JobStatus::ACCEPTED
      JobEtaExplanation.find_by_text!(text)
    elsif job_status_id == JobStatus::REJECTED
      JobRejectReason.find_by_text!(text)
    end
  end

  def key
    super().to_sym
  end

  # initialize all the required seed records - only used by seed migrations when we add values above.
  def self.init_all
    ALL_JOB_STATUS_REASON_TYPES.each do |status|
      JobStatusReasonType.find_or_create_by(status)
    end
  end

end
