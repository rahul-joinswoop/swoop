# frozen_string_literal: true

class ReviewAuditSms < AuditSms

  def redirect_url
    "#{ENV['SITE_URL']}"
  end

  def find_target(phone)
    Review.where(phone: phone).order(created_at: :desc).first
  end

  def expecting_reply?
    true
  end

end
