# frozen_string_literal: true

class LocationAuditSms < AuditSms

  self.description = 'Request Location'

  def redirect_url
    "#{ENV['SITE_URL']}/get_location/#{job.uuid}"
  end

end
