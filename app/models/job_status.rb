# frozen_string_literal: true

class JobStatus < ApplicationRecord

  scope :for_name, ->(name) { where(name: name).first! }

  INITIAL = 14
  PREDRAFT = 27
  DRAFT = 26
  PENDING = 1
  CREATED = 9
  ASSIGNED = 10
  DISPATCHED = 2
  EN_ROUTE = 3
  ETA_REJECTED = 20
  ETA_EXTENDED = 28
  ON_SITE = 4
  TOWING = 5
  TOW_DESTINATION = 6
  COMPLETED = 7
  CANCELED = 8
  REASSIGNED = 16
  UNASSIGNED = 15
  STORED = 17
  RELEASED = 18
  ACCEPTED = 11
  MARK_PAID = 24
  REJECTED = 12
  EXPIRED = 22
  AUTO_ASSIGNING = 23
  GOA = 21
  DELETED = 25
  SUBMITTED = 19
  REASSIGN = 13

  # virtual ones, only used on rendering
  RETURNED_TO_HQ = 'Returned to HQ'

  # mapping of constant names to friendly names
  NAMES = {
    INITIAL: 'Initial',
    PENDING: 'Pending',
    DISPATCHED: 'Dispatched',
    EN_ROUTE: 'En Route',
    ON_SITE: 'On Site',
    TOWING: 'Towing',
    TOW_DESTINATION: 'Tow Destination',
    COMPLETED: 'Completed',
    CANCELED: 'Canceled',
    CREATED: 'Created',
    PREDRAFT: 'Predraft',
    DRAFT: 'Draft',
    ASSIGNED: 'Assigned',
    ACCEPTED: 'Accepted',
    REJECTED: 'Rejected',
    REASSIGN: 'Reassign',
    UNASSIGNED: 'Unassigned',
    REASSIGNED: 'Reassigned',
    STORED: 'Stored',
    RELEASED: 'Released',
    SUBMITTED: 'Submitted',
    ETA_REJECTED: 'ETA Rejected',
    ETA_EXTENDED: "ETA Extended",
    GOA: 'GOA',
    EXPIRED: 'Expired',
    AUTO_ASSIGNING: 'Auto Assigning',
    MARK_PAID: 'Mark Paid',
    DELETED: 'Deleted',
  }.freeze

  ID_MAP = NAMES.transform_keys { |k| const_get(k) }.freeze
  NAME_MAP = ID_MAP.invert.symbolize_keys.freeze

  HIDE_FROM_HISTORY = [
    INITIAL,
    PREDRAFT,
    PENDING,
    UNASSIGNED,
  ].freeze

  DONE = [
    REJECTED,
    EXPIRED,
    REASSIGNED,
    COMPLETED,
    CANCELED,
    GOA,
  ].freeze

  DEFAULT_EMAIL_STATUSES = [
    CREATED,
    ACCEPTED,
  ].freeze

  EMAILABLE = [
    ACCEPTED,
    ETA_EXTENDED,
    CANCELED,
    COMPLETED,
    CREATED,
    EN_ROUTE,
    GOA,
    ON_SITE,
    REASSIGNED,
    TOW_DESTINATION,
    TOWING,
  ].freeze

  SCHEDULED_FOR_EMAILABLE = [
    CREATED,
    ACCEPTED,
  ].freeze

  COMPANY_EMAILABLE = [
    ACCEPTED,
    CANCELED,
    CREATED,
    GOA,
  ].freeze

  ALL_STATUSES = [
    [PENDING, "Pending"],
    [DISPATCHED, "Dispatched"],
    [EN_ROUTE, "En Route"],
    [ON_SITE, "On Site"],
    [TOWING, "Towing"],
    [TOW_DESTINATION, "Tow Destination"],
    [COMPLETED, "Completed"],
    [CANCELED, "Canceled"],
    [CREATED, "Created"],
    [ASSIGNED, "Assigned"],
    [ACCEPTED, "Accepted"],
    [REJECTED, "Rejected"],
    [REASSIGN, "Reassign"],
    [INITIAL, "Initial"],
    [UNASSIGNED, "Unassigned"],
    [REASSIGNED, "Reassigned"],
    [STORED, "Stored"],
    [RELEASED, "Released"],
    [SUBMITTED, "Submitted"],
    [ETA_REJECTED, "ETA Rejected"],
    [GOA, "GOA"],
    [EXPIRED, "Expired"],
    [AUTO_ASSIGNING, "Auto Assigning"],
    # TODO should we add missing id 24 (Mark Paid) here?,
    [DELETED, "Deleted"],
    [DRAFT, "Draft"],
    [PREDRAFT, "Predraft"],
    [ETA_EXTENDED, "ETA Extended"],
  ].freeze

  class << self

    def status_id(status_name)
      name = Job.statuses[status_name]
      NAME_MAP[name.to_sym] if name
    end

    # Dynamically creates a mark_paid status for job.
    #
    # This seems not to be the best approach though, as commented in ENG-1188
    #
    # TODO we should think about having a job_history table to avoid this
    def job_status_for_job_mark_paid
      JobStatus.new(
        id: JobStatus::MARK_PAID,
        name: JobStatus::ID_MAP[JobStatus::MARK_PAID]
      )
    end

    def init_all
      ALL_STATUSES.each do |status|
        JobStatus.find_or_create_by(id: status[0], name: status[1])
      end
    end

  end

  # this gives us the status value used in the Job table
  def status
    Job::JOB_STATUS_TO_STATUS_MAP[name]
  end

  def db_name
    name.parameterize.underscore
  end

end
