# frozen_string_literal: true

class WriteOff < CreditNote

  validates :total_amount, numericality: { less_than: 0 }
  def display_name
    'Write Off'
  end

end
