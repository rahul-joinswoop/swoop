# frozen_string_literal: true

class CompanySymptom < ApplicationRecord

  belongs_to :company
  belongs_to :symptom

end
