# frozen_string_literal: true

class Feature < ApplicationRecord

  has_and_belongs_to_many :companies

  # API Features
  ALLOW_INVOICE_ADD_ITEM = 'Allow Invoice Add Item'
  ALWAYS_SHOW_CALLER = 'Always Show Caller'
  AUTO_ASSIGN = 'Auto Assign'
  CLIENT_API_COVERAGE_OVERRIDE = 'Client API Coverage Override'
  CUSTOMER_TYPE = 'Payment Type'
  DONT_REQUIRE_CUSTOMER_NAME_AND_PHONE = "Don't require customer name and phone"
  INTERNATIONAL_PHONE_NUMBERS = 'International Phone Numbers'
  INVOICE_CUSTOM_ITEMS = 'Invoice Custom Items'
  INVOICING = 'Invoicing'
  JOB_ALTERNATE_TRANSPORTATION = 'Alternate Transportation'
  JOB_DETAILS_RENTAL_CAR = 'Job Details Rental Car'
  JOB_CUSTOMER_PAYS = 'Customer Pays'
  JOB_MEMBER_NUMBER = 'Job Member Number'
  JOB_ODOMETER = 'Odometer'
  JOB_PO_NUMBER = 'Job PO Number'
  JOB_REFERENCE_NUMBER = 'Job Reference Number'
  JOB_UBER = 'Uber'
  JOB_UNIT_NUMBER = 'Job Unit Number'
  JOB_VIP = 'VIP'
  LEGACY_GRAPHQL_ERRORS = 'Legacy GraphQL Errors'
  LOCATION_CONTACTS = 'Location Contacts'
  NPS_REVIEW = 'NPS Review'
  PARTNER_DISPATCH_TO_TRUCK = 'Partner Dispatch to Truck'
  QUESTIONS = 'Questions'
  REVIEW_4_TEXTS = 'Review 4 Texts'
  REVIEW_CHOOSE = 'Review Choose'
  REVIEW_NPS_SURVEYS = 'Review NPS Survey'
  REVIEW_PURE_NPS = 'Review Pure NPS'
  SETTINGS = 'Settings'
  SHOW_DRIVERS_EDIT_INVOICE = 'Show Drivers Edit Invoice'
  SHOW_DRIVERS_INVOICE = 'Show Drivers Invoice'
  STORAGE = 'Storage'
  # Calculate truck with quickest ETA and assign job to owning company.
  # This needs to be on for both fleet and partner to enable it.
  SWOOP_AUTO_DISPATCHER = 'Swoop Auto Dispatcher'

  # UI Features
  ADD_DRIVER_NAME_ON_INVOICE = 'Add Driver Name on Invoice'
  ADD_PARTNER_COLUMN_TO_FLEET_MANAGED = 'Add Partner Column to Fleet Managed'
  AUTO_ASSIGN_DRIVER = 'Auto Assign Driver'
  AUTO_SELECT_SITE_FROM_LOOKUP = 'Auto Select Site from Lookup'
  AUTOFILL_VIN = 'Autofill VIN'
  CANADIAN_AP = 'Canadian AP'
  CLIENT_SITES = 'Client Sites'
  DEPARTMENTS = 'Departments'
  DISABLE_BANK_SETTINGS = 'Disable Bank Settings'
  DISABLE_DELETE_JOB = 'Disable Delete Job'
  DISABLE_DRIVER_VIEW_INVOICE = 'Disable driver view invoice'
  DISABLE_INVOICE_RESTRICTIONS = "Disable Invoice Restrictions"
  FLEET_CLASS_TYPE = 'Fleet Class Type'
  HEAVY_DUTY_EQUIPMENT = 'Heavy Duty Equipment'
  HIDE_4WD = 'Hide 4WD'
  HIDE_LOCATION_CONTACTS = 'Hide Location Contacts'
  HIDE_PARTNER_ETA = 'Hide Partner ETA'
  HIDE_REPORTING = 'Hide Reporting'
  INVOICE_WARNING = 'Invoice Warning'
  ISSC_NETWORK_STATUS = 'ISSC Network Status'
  ISSUES = 'Issues'
  JOB_FORM_CUSTOMER_LOOKUP = 'Job Form Customer Lookup'
  JOB_FORM_RECOMMENDED_DROPOFF = 'Job Form Recommended Drop Off'
  JOB_FORM_SITE_REQUIRED = 'Job Form Site Required'
  JOB_FORM_UNIT_REQUIRED = 'Job Form Unit Required'
  MC_JOB_AUTO_REJECT_OVERRIDE = "MC Job AutoReject Override"
  PLANE_ANIMATION = 'Plane Animation'
  PO_ON_DASHBOARD = 'PO On Dashboard'
  PRIORITY_RESPONSE = 'Priority Response'
  QUESTION = 'Questions'
  REMOVE_INVOICE_TIMESTAMPS_PER_ACCOUNT = 'Remove invoice timestamps per account'
  SERIAL_NUMBER = 'Serial Number'
  SHOW_CLIENT_SITES_TO_SWOOP = 'Show Client Sites to Swoop'
  SHOW_SYMPTOMS = 'Show Symptoms'
  SMS_CONFIRM_COMPLETE = 'SMS Confirm Complete'
  SMS_CONFIRM_ON_SITE = 'SMS Confirm On Site'
  SMS_DEFAULT_REQUEST_LOCATION_ON = 'SMS_Default Request Location On' # This determines whether the 'Text link to request exact location' (aka job.get_location_sms) is set by default in the UI
  SMS_REQUEST_LOCATION = 'SMS_Request Location'
  SMS_REVIEWS = 'SMS_Reviews'
  SMS_TRACK_DRIVER = 'SMS_Track Driver'
  SWOOP_ROOT_ALL_VEHICLE_UPDATES = 'Swoop Root All Vehicle Updates'
  TESLA_NEW_JOB = 'Tesla New Job'
  TEXT_ETA_UPDATES = 'SMS_ETA Updates'
  TRAILERS = 'Trailers'
  VIN_FIELD_FIRST = 'VIN Field First'

  # mobile
  DISABLE_APP_EDIT_JOBS = 'Disable App Edit Jobs'
  DISABLE_APP_DRIVERS_EDIT_JOBS = 'Disable App Drivers Edit Jobs'

  ALL_FEATURES = [
    ADD_PARTNER_COLUMN_TO_FLEET_MANAGED, ALLOW_INVOICE_ADD_ITEM, ALWAYS_SHOW_CALLER,
    AUTO_ASSIGN_DRIVER, AUTO_ASSIGN, AUTO_SELECT_SITE_FROM_LOOKUP, AUTOFILL_VIN, CANADIAN_AP,
    CUSTOMER_TYPE, DEPARTMENTS, DISABLE_BANK_SETTINGS, DISABLE_DELETE_JOB,
    DISABLE_INVOICE_RESTRICTIONS, FLEET_CLASS_TYPE, CLIENT_SITES, HEAVY_DUTY_EQUIPMENT, HIDE_4WD,
    HIDE_LOCATION_CONTACTS, HIDE_PARTNER_ETA, HIDE_REPORTING, INTERNATIONAL_PHONE_NUMBERS,
    INVOICE_WARNING, INVOICING, ISSC_NETWORK_STATUS, ISSUES, JOB_FORM_CUSTOMER_LOOKUP,
    JOB_FORM_RECOMMENDED_DROPOFF, JOB_FORM_SITE_REQUIRED, JOB_FORM_UNIT_REQUIRED,
    JOB_ALTERNATE_TRANSPORTATION, JOB_CUSTOMER_PAYS, JOB_MEMBER_NUMBER, JOB_ODOMETER,
    JOB_PO_NUMBER, JOB_REFERENCE_NUMBER, JOB_UBER, JOB_UNIT_NUMBER, JOB_VIP, LOCATION_CONTACTS,
    NPS_REVIEW, PARTNER_DISPATCH_TO_TRUCK, PLANE_ANIMATION, PO_ON_DASHBOARD, PRIORITY_RESPONSE,
    QUESTION, REVIEW_4_TEXTS, REVIEW_CHOOSE, REVIEW_NPS_SURVEYS, REVIEW_PURE_NPS,
    SERIAL_NUMBER, SETTINGS, SHOW_DRIVERS_EDIT_INVOICE, SHOW_DRIVERS_INVOICE, SHOW_SYMPTOMS,
    SHOW_CLIENT_SITES_TO_SWOOP, SMS_CONFIRM_COMPLETE, SMS_CONFIRM_ON_SITE,
    SMS_DEFAULT_REQUEST_LOCATION_ON, SMS_REQUEST_LOCATION, SMS_REVIEWS, SMS_TRACK_DRIVER, STORAGE,
    SWOOP_ROOT_ALL_VEHICLE_UPDATES, TESLA_NEW_JOB, TEXT_ETA_UPDATES, TRAILERS, VIN_FIELD_FIRST,
  ].freeze

  DELETED_FEATURES = [ISSC_NETWORK_STATUS, NPS_REVIEW, TESLA_NEW_JOB].freeze

  DESCRIPTIONS = {
    ADD_DRIVER_NAME_ON_INVOICE => 'Applicable to partners: it add the driver assigned to the job to the invoice',
    ADD_PARTNER_COLUMN_TO_FLEET_MANAGED => 'Enables a Fleet Managed company to add Partner (the partner assigned to the job) as a column on the Active tab or Done tab.',
    ALLOW_INVOICE_ADD_ITEM => nil,
    ALWAYS_SHOW_CALLER => nil,
    AUTO_ASSIGN => 'Calculate truck with quickest ETA and assign job to owning company. This needs to be on for both fleet and partner to enable it',
    AUTO_ASSIGN_DRIVER => nil,
    AUTO_SELECT_SITE_FROM_LOOKUP => 'Auto select site with matching Site Code on Policy Lookup Search flow',
    AUTOFILL_VIN => nil,
    CANADIAN_AP => nil,
    CLIENT_API_COVERAGE_OVERRIDE => 'Automatically apply coverage rule to all jobs that arrive via Client API',
    CUSTOMER_TYPE => nil,
    DEPARTMENTS => nil,
    DISABLE_BANK_SETTINGS => 'The whole “link bank account” section is hidden from Partners on settings',
    DONT_REQUIRE_CUSTOMER_NAME_AND_PHONE => "Don't require customer name and phone on Job Form",
    FLEET_CLASS_TYPE => 'Enable setting class type on client jobs',
    CLIENT_SITES => nil,
    HEAVY_DUTY_EQUIPMENT => 'Shows heavy duty vehicle info in drop downs',
    HIDE_4WD => nil,
    HIDE_LOCATION_CONTACTS => nil,
    HIDE_PARTNER_ETA => nil,
    HIDE_REPORTING => nil,
    INTERNATIONAL_PHONE_NUMBERS => 'Enable country specific phone number formatting.',
    INVOICE_WARNING => nil,
    INVOICING => nil,
    ISSC_NETWORK_STATUS => nil,
    ISSUES => nil,
    JOB_DETAILS_RENTAL_CAR => 'Display Rental Car form button on Job Details',
    JOB_FORM_RECOMMENDED_DROPOFF => 'Show recommended fleet sites (e.g. Dealerships) in the dropoff location field in UI',
    JOB_FORM_SITE_REQUIRED => 'Require Site for Client company jobs in the job form',
    JOB_FORM_UNIT_REQUIRED => 'Require Unit for Client company jobs in the job form',
    JOB_ALTERNATE_TRANSPORTATION => nil,
    JOB_CUSTOMER_PAYS => nil,
    JOB_MEMBER_NUMBER => nil,
    JOB_ODOMETER => nil,
    JOB_PO_NUMBER => nil,
    JOB_REFERENCE_NUMBER => nil,
    JOB_UBER => nil,
    JOB_UNIT_NUMBER => nil,
    JOB_VIP => nil,
    LEGACY_GRAPHQL_ERRORS => 'Keeps the old GraphQL error behavior for clients',
    LOCATION_CONTACTS => nil,
    NPS_REVIEW => nil,
    PARTNER_DISPATCH_TO_TRUCK => 'Enables Partner to automatically Dispatch To Truck',
    PLANE_ANIMATION => 'Shows plane animation on sending invoice',
    PO_ON_DASHBOARD => 'PO column in the jobs list',
    PRIORITY_RESPONSE => 'Allows company to edit priority response on a job',
    QUESTION => 'Determines if a company sees required questions on their new job form',
    REVIEW_4_TEXTS => nil,
    REVIEW_CHOOSE => nil,
    REVIEW_NPS_SURVEYS => nil,
    REVIEW_PURE_NPS => nil,
    SERIAL_NUMBER => 'Shows Serial Number on form',
    SETTINGS => nil,
    SHOW_DRIVERS_EDIT_INVOICE => nil,
    SHOW_DRIVERS_INVOICE => nil,
    SHOW_CLIENT_SITES_TO_SWOOP => 'If enabled on a client company, the Site field should be shown to Swoop users in new/edit job form for that client',
    SHOW_SYMPTOMS => nil,
    SMS_CONFIRM_COMPLETE => 'Send an SMS 30 mins after the job goes to onsite to the customer to confirm job completion',
    SMS_CONFIRM_ON_SITE => 'Send an SMS follow up if the job exceeds the last ETA (of any type) to the customer asking if provider arrived',
    SMS_DEFAULT_REQUEST_LOCATION_ON => nil,
    SMS_REQUEST_LOCATION => nil,
    SMS_REVIEWS => 'Company has option to send user a review sms',
    SMS_TRACK_DRIVER => 'Company has option to send user a tracking sms',
    STORAGE => 'Allows Partners to Store Vehicles',
    SWOOP_ROOT_ALL_VEHICLE_UPDATES => 'Should swoop root receive all Rescue Vehicle updates via Websocket?',
    TESLA_NEW_JOB => 'Enables the new job form for Tesla (only applicable to Tesla)',
    TEXT_ETA_UPDATES => nil,
    TRAILERS => 'Shows Trailers on form',
    VIN_FIELD_FIRST => nil,
  }.freeze

  def self.init_all
    ALL_FEATURES.each do |feature_name|
      f = Feature.find_or_create_by!(name: feature_name)
      f.update! description: DESCRIPTIONS[feature_name]
    end

    Feature.where(name: DELETED_FEATURES).each do |feature|
      feature.deleted_at = Time.now
      feature.save!
    end
  end

end
