# frozen_string_literal: true

class ServiceCommissionsExclusions < ApplicationRecord

  belongs_to :company
  belongs_to :service_code

end
