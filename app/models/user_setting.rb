# frozen_string_literal: true

class UserSetting < BaseModel

  belongs_to :user

  validates :key, uniqueness: { scope: :user_id }

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/user_settings/_setting", locals: { setting: self }))
  end

  def listeners
    ret = Set.new
    if user && !user.nil?
      ret << user.company
    end
    ret
  end

end
