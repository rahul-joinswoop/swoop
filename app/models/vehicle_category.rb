# frozen_string_literal: true

# This model is know as "Class Type" on the front end. For the most part,
# it represents a class of vehicle. In reality, though, is kind of an amalgomation
# of rescue/customer vehicle class and rate.
#
# Partners can
class VehicleCategory < ApplicationRecord

  include StandardOrCustomCollection

  FLATBED = 'Flatbed'
  WHEEL_LIFT  = 'Wheel Lift'
  MEDIUM_DUTY = 'Medium Duty'
  LIGHT_DUTY  = 'Light Duty'
  HEAVY_DUTY  = 'Heavy Duty'
  SUPER_HEAVY = 'Super Heavy'
  SERVICE_TRUCK = "Service Truck"
  COVERED_FLATBED = "Covered Flatbed"

  has_many :companies_vehicle_category
  has_many :companies, through: :companies_vehicle_category

  validates :name, presence: true, uniqueness: true

  scope :standard, -> { where(is_standard: true) }

  VEHICLE_CATEGORIES = [
    ["Carrier", nil],
    ["Flatbed", 1],
    ["Heavy Duty", 5],
    ["Heavy Duty After Hours", nil],
    ["Heavy Duty Combo", nil],
    ["Landoll", nil],
    ["Light Duty", 3],
    ["Light Duty After Hours", nil],
    ["Lowboy", nil],
    ["Mariposa", nil],
    ["Medium Duty", 4],
    ["Medium Duty After Hours", nil],
    ["Medium Duty Combo", nil],
    ["Motorcycle", nil],
    ["Multi-Axle Lowboy", nil],
    ["Oakhurst", nil],
    ["Rotator", nil],
    ["Service", nil],
    ["Sliding Axle", nil],
    ["Stepdeck", nil],
    ["Stretch", nil],
    ["Super Heavy", 6],
    ["Super Heavy After Hours", nil],
    ["Super Heavy Combo", nil],
    ["Tractor Only", nil],
    ["Wheel Lift", 2],
    [SERVICE_TRUCK, 7],
    [COVERED_FLATBED, 8],
  ].freeze

  def self.init_all
    VEHICLE_CATEGORIES.each do |vc|
      vc = VehicleCategory.find_or_create_by!(name: vc[0])
      vc.update! order_num: vc[1]
    end
  end

end
