# frozen_string_literal: true

class PartnerAPIPasswordRequest < PasswordRequest

  include PasswordRequestPartnerAPISupport

  # we have to have our user here
  belongs_to :user, optional: false
  before_validation :set_user_input
  validate :user_email_is_present
  validate :user_has_authorized_application

  private

  def set_user_input
    self.user_input = user_email
  end

  def user_email_is_present
    # don't throw an error if we don't have a user because another error will be thrown
    return true if user.blank?
    errors.add(:user, "must have an email address") if user_email.blank?
  end

  def user_has_authorized_application
    # don't throw an error if we don't have an application or user
    return true if [application, user].any?(&:blank?)

    # TODO - is this right? it's ok if this expired (i think) but it's not ok if it's revoked, right?
    unless Doorkeeper::AccessGrant.where(application_id: application.id, resource_owner_id: user.id, revoked_at: nil).exists?
      errors.add(:user, "must have granted access to client app")
    end
  end

  def trigger
    Email::PartnerAPIPasswordRequestWorker.perform_async id
    self
  end

end
