# frozen_string_literal: true

class InventoryItem < BaseModel

  include SoftDelete

  ITEMS = [:Tire, :Vehicle].freeze
  belongs_to :item, polymorphic: true
  belongs_to :tire, -> { where(inventory_items: { item_type: 'Tire' }) }, foreign_key: 'item_id'
  belongs_to :site
  belongs_to :job, autosave: true
  belongs_to :company
  belongs_to :user
  belongs_to :released_to_user, class_name: "User", foreign_key: "released_to_user_id"
  belongs_to :released_to_account, class_name: "Account", foreign_key: "released_to_account_id"
  belongs_to :released_to_job, class_name: "Job", foreign_key: "released_to_job_id"
  accepts_nested_attributes_for :item
  accepts_nested_attributes_for :released_to_user

  validates :company, presence: true
  validates :site, presence: true
  validates :user, presence: true
  validates :item, presence: true

  validate do |inventory_item|
    if inventory_item.replace_invoice_info && !((inventory_item.released_to_user || inventory_item.released_to_account))
      inventory_item.errors[:base] << "replace_invoice_info set, but released_to_user or released_to_account not set"
    end
  end

  # Ensure that the job index is up to date with any changes made on the association
  after_commit { job.reindex if job }

  after_commit do
    Subscribable.trigger_job_updated job
    Subscribable.trigger_user_updated user
  end

  delegate :name, to: :site, prefix: true, allow_nil: true

  def item_attributes=(attributes)
    if ITEMS.include?(attributes[:type].to_sym)
      self.item ||= (Object.const_get attributes[:type]).new
      attributes.delete("type")
      # TODO: filter out appropriate attributes and require needed ones
      self.item.assign_attributes(attributes)
    end
  end

  def listeners
    ret = Set.new
    ret << company
    if item_type == "Tire"
      company = Company.find_by(name: 'Tesla')
      if company
        ret << company
      end
    end
    ret
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/inventory_items/_inventory_item", locals: { inventory_item: self }))
  end

end
