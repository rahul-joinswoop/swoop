# frozen_string_literal: true

module PCC

  def self.table_name_prefix
    'pcc_'
  end

end
