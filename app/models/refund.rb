# frozen_string_literal: true

class Refund < CreditNote

  validates :total_amount, numericality: { greater_than_or_equal: 0 }

  has_one :charge, class_name: 'InvoicePaymentCharge'

  def display_name
    'Refund'
  end

end
