# frozen_string_literal: true

class Question < ApplicationRecord

  include IdentityCache
  include QuestionDefinitions

  has_many :company_service_questions
  has_many :answers

  cache_index :question, unique: true
  cache_has_many :answers

  DEFAULT_QUESTIONS_PER_SERVICE = {
    ServiceCodeDefinitions::ACCIDENT_TOW => [
      KEYS_PRESENT,
      CUSTOMER_WITH_VEHICLE,
      LOW_CLEARANCE,
      WILL_CUSTOMER_RIDE,
    ],
    ServiceCodeDefinitions::BATTERY_JUMP => [
      KEYS_PRESENT,
      CUSTOMER_WITH_VEHICLE,
      VEHICLE_STOP_WHEN_RUNNING,
      ANYONE_ATTEMPTED_TO_JUMP,
      LOW_CLEARANCE,
    ],
    ServiceCodeDefinitions::TIRE_CHANGE => [
      KEYS_PRESENT,
      CUSTOMER_WITH_VEHICLE,
      WHICH_TIRE,
      HAVE_A_SPARE,
      LOW_CLEARANCE,
    ],
    ServiceCodeDefinitions::LOCK_OUT => [
      CUSTOMER_WITH_VEHICLE,
      IS_THE_VEHICLE_ON,
      WHERE_ARE_THE_KEYS,
      CHILDREN_OR_PETS,
      LOW_CLEARANCE,
    ],
    ServiceCodeDefinitions::FUEL_DELIVERY => [
      CUSTOMER_WITH_VEHICLE,
      GASOLINE_TYPE,
      LOW_CLEARANCE,
    ],
    ServiceCodeDefinitions::REPO => [
      KEYS_PRESENT,
      CUSTOMER_BEEN_CONTACTED,
      VEHICLE_MOVED_TODAY,
      GPS_TRACKING,
      LOW_CLEARANCE,
    ],
    ServiceCodeDefinitions::TOW_IF_JUMP_FAILS => [
      KEYS_PRESENT,
      CUSTOMER_WITH_VEHICLE,
      VEHICLE_CAN_BE_NEUTRAL,
      VEHICLE_4WD,
      VEHICLE_STOP_WHEN_RUNNING,
      ANYONE_ATTEMPTED_TO_JUMP,
      LOW_CLEARANCE,
      WILL_CUSTOMER_RIDE,
    ],
    ServiceCodeDefinitions::WINCH_OUT => [
      KEYS_PRESENT,
      CUSTOMER_WITH_VEHICLE,
      VEHICLE_CAN_BE_NEUTRAL,
      VEHICLE_DRIVEABLE_AFTER_WINCH_OUT,
      VEHICLE_4WD,
      WITHIN_10_FEET_OF_PAVED_SURFACE,
      LOW_CLEARANCE,
    ],
    ServiceCodeDefinitions::TOW => [
      KEYS_PRESENT,
      CUSTOMER_WITH_VEHICLE,
      VEHICLE_CAN_BE_NEUTRAL,
      VEHICLE_4WD,
      LOW_CLEARANCE,
      WILL_CUSTOMER_RIDE,
    ],
    ServiceCodeDefinitions::SECONDARY_TOW => [
      KEYS_PRESENT,
      VEHICLE_CAN_BE_NEUTRAL,
      VEHICLE_4WD,
      LOW_CLEARANCE,
    ],
    ServiceCodeDefinitions::TRANSPORT => [
      KEYS_PRESENT,
      LOW_CLEARANCE,
    ],
  }.freeze

  def self.defaults_per_service_code(service_code)
    defaults = DEFAULT_QUESTIONS_PER_SERVICE[service_code.name]
    return unless defaults
    where(question: defaults).all
  end

  def neutral_capable?
    name == NEUTRAL_CAPABLE_NAME
  end

  def fetch_answer(answer)
    fetch_answers.find { |r| r.answer == answer }
  end

  def fetch_answer!(answer)
    fetch_answer(answer) || raise(ActiveRecord::RecordNotFound)
  end

end
