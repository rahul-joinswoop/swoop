# frozen_string_literal: true

class Role < ApplicationRecord

  ADMIN = :admin
  ANSWERING_SERVICE = :answering_service
  DISPATCHER = :dispatcher
  DRIVER = :driver
  FLEET = :fleet
  RESCUE = :rescue
  ROOT = :root
  SWOOP_DISPATCHER = :swoop_dispatcher
  SUPER_FLEET_MANAGED = :super_fleet_managed

  NAME_TO_STATUS_MAP = {
    'Admin' => ADMIN,
    'Answering Service' => ANSWERING_SERVICE,
    'Dispatcher' => DISPATCHER,
    'Driver' => DRIVER,
    'Fleet' => FLEET,
    'Rescue' => RESCUE,
    'Root' => ROOT,
    'Swoop Dispatcher' => SWOOP_DISPATCHER,
    'Super Fleet Managed' => SUPER_FLEET_MANAGED,
  }.freeze

  STATUS_TO_NAME_MAP = NAME_TO_STATUS_MAP.invert

  NAME_TO_DESCRIPTION_MAP = {
    'Admin' => 'Manage the company settings, and can execute Dispatcher tasks as well',
    'Answering Service' => 'Answering Service',
    'Dispatcher' => 'Create and dispatch jobs to Drivers.',
    'Driver' => 'Execute the jobs for a Partner Company.',
    'Fleet' => 'User works for a Fleet Company',
    'Rescue' => 'User works for a Partner (Rescue) Company',
    'Root' => 'User is a Swoop Root user',
    'Swoop Dispatcher' => 'User is a Swoop dispatcher',
    'Super Fleet Managed' => 'User works for a Super FleetManaged company',
  }.freeze

  has_and_belongs_to_many :users, join_table: :users_roles
  belongs_to :resource, polymorphic: true
  has_many :permissions

  validates :resource_type,
            inclusion: { in: Rolify.resource_types },
            allow_nil: true

  def self.init_all
    STATUS_TO_NAME_MAP.keys.each do |role|
      Role.find_or_create_by(name: role)
    end
  end

  scopify

end
