# frozen_string_literal: true

class QuestionResult < BaseModel

  belongs_to :job
  belongs_to :question
  belongs_to :answer, autosave: true

  delegate :neutral_capable?, to: :question, prefix: true, allow_nil: true
  delegate :no?, to: :answer, prefix: true, allow_nil: true
  delegate :needs_info?, to: :answer, prefix: true, allow_nil: true
  # TODO - we should add a validation for this here, something like this (untested):
  # validates :answer_info, presence: true, if: :needs_info?

  def question_name
    question&.question
  end

  def answer_name
    answer&.answer
  end

end
