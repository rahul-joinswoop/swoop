# frozen_string_literal: true

class TwilioReply < BaseModel

  belongs_to :user
  belongs_to :job

  belongs_to :target, polymorphic: true

  def process
    sms = response_to

    if sms
      self.target = sms.find_target(from)
      self.job_id = target.job_id
      self.service = target.type

      target.process_response(self)
    else
      target_job
      logger.warn "SMS from unrecognised number:#{from} linking"\
                "to #{target_type}:#{target_id}"
    end
  end

  def response_to
    # Limit to 20 messages. If we have 20 intermediary messages that we sent to
    # this person, we're problably not tracking they're input from that text anymore.
    # This helps limit loading "all the messages to a person ever", esp. in staging where
    # we may have 100s of texts to a single number.
    AuditSms.where(to: from).order(id: :desc).limit(20).each do |sent_sms|
      if sent_sms.expecting_reply?
        Rails.logger.debug "Found sent_sms:#{sent_sms.inspect}"
        return sent_sms
      end
    end
    nil
  end

  def target_job
    return if target.present? || !user_id

    drive = Drive.where(user_id: user_id).last
    if drive
      job = Job.find_by_driver_id(drive.id)
      self.target = job
    end
  end

  def listeners
    # Change this so if people are replying to our messages and NO review has been sent at all
    # we wont have any listeners, which is fine. But since BaseModel tries and publishes our
    # model we need to let it know that we dont have anything for it to work with here.
    #
    #
    if target && target.present?
      target.listeners
    else
      []
    end
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/twilio_replies/_twilio_reply", locals: { reply: self }))
  end

end
