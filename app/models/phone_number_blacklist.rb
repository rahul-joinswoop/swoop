# frozen_string_literal: true

class PhoneNumberBlacklist < ApplicationRecord

  before_validation :normalize_phone
  validates :phone_number, presence: true, format: { with: PhoneNumber::E164_REGEX, message: "is not valid" }

  class << self

    def blacklisted?(phone_number)
      exists?(phone_number: phone_number)
    end

  end

  private

  def normalize_phone
    self.phone_number = Phonelib.parse(phone_number).e164 if phone_number_changed?
  end

end
