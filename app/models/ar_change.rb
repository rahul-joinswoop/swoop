# frozen_string_literal: true

class ArChange < ApplicationRecord

  belongs_to :target, polymorphic: true
  belongs_to :user
  belongs_to :command

end
