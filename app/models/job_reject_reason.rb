# frozen_string_literal: true

class JobRejectReason < ApplicationRecord

  belongs_to :reason

  DO_NOT_HAVE_TIRE_SIZE = 'Do not have requested tire size'
  OUT_OF_SERVICE_AREA = 'Out of Service Area'
  CAN_NOT_SERVICE_WITHIN = 'Can not service within 60 mins'
  NOT_INTERESTED = 'Not Interested'
  OTHER = 'Other'
  EQUIPMENT_NOT_AVAILABLE = 'Equipment Not Available'
  RESTRICTED_ROADWAY = 'Restricted Roadway'
  WEATHER = 'Weather'
  PAYMENT_ISSUE = 'Payment Issue'
  UNSAFE_LOCATION = 'Unsafe Location'
  SERVICE_NOT_AVAILABLE = 'Service Not Available'
  QUOTE_NOT_ACCEPTED = 'Quote Not Accepted'
  NO_DRIVERS_AVAILABLE = 'No Drivers Available'
  REFUSED_CALL = 'Refused Call'
  TRAFFIC = 'Traffic'
  EVENT = 'Event'

  # Primarily used by RSC:
  DO_NOT_ACCEPT_PAYMENT_TYPE = 'Do not accept payment type'
  NO_LONGER_OFFER_SERVICE = 'No longer offer service'
  OUT_OF_MY_COVERAGE_AREA = 'Out of my coverage area'
  PROPER_EQUIPMENT_NOT_AVAILABLE = 'Proper equipment not available'

end
