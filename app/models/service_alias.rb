# frozen_string_literal: true

class ServiceAlias < ApplicationRecord

  belongs_to :company
  belongs_to :service_code

  validates :company, :service_code, :alias, presence: true
  validates :alias, uniqueness: { scope: [:company_id] }

end
