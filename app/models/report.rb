# frozen_string_literal: true

# Reports are loaded into the database from information in the db/reports.yml
# file by running `rake db:reports:load`.
#
# The structure of the YAML file is:
#
# Storage Report:
#   display_name: Storage Report
#   company_type: RescueCompany
#   sql: db/reports/rescue_company/storage_report.sql
#   params:
#   - name: api_user.id
#     default: api_user.id
#     mandatory: api_user.id
#   - name: api_company.id
#     mandatory: api_company.id
#   - name: account
#     type: account
#   - name: type
#     type: eval
#     method: storage_types
#   - name: site
#     type: eval
#     method: live_sites
#     filter_only: true
#   filters:
#     account: and jobs.account_id=?
#     type: AND jobs.storage_type_id = ?
#     site: AND inventory_items.site_id = ?
#   pdf: true
#
# The values in the params map will be munged together into the
# internal values stored in the all_params, default_params, mandatory_params,
# and available_params fields.
class Report < ApplicationRecord

  include ReportFilters

  has_many :results, class_name: 'ReportResult'
  delegate :fields, :values, :executed_in, to: :last_result, allow_nil: true
  delegate :created_at, :token, prefix: true, to: :last_result, allow_nil: true

  ACCOUNT_SUMMARY          = 'Account Summary'
  ALL_DATA_FLEET_INHOUSE   = 'All Data Report (FleetInHouse)'
  CENTURY_BILLING_FILE     = '21st Century Billing File'
  CENTURY_INVOICE          = '21st Century Invoice'
  COMMISSIONS_REPORT       = 'Commissions'
  DRIVER_REPORT            = 'Driver Report'
  DRIVER_SUMMARY           = 'Driver Summary'
  ENTERPRISE_INVOICE       = 'Enterprise Invoice'
  ETA_ATA_REPORT           = 'ETA ATA Report'
  FLEET_ACTIVITY           = 'Fleet Managed Activity Report'
  FLEET_CSAT               = 'Fleet Managed CSAT Report'
  FLEET_ETA_VARIANCE       = 'Fleet Managed ETA Variance Report'
  JOBS_REPORT              = 'Jobs Report'
  MOTORCYLE_TOWING_INVOICE = 'Motorcycle Towing Services, L.C. Invoice'
  PAYMENTS_REPORT          = 'Payments Report'
  PO_REQUIRED              = 'PO Required'
  STATEMENT                = 'Statement'
  STORAGE_REPORT           = 'Storage Report'
  SWOOP_FLEET_INVOICE      = 'Swoop Fleet Invoice'
  SWOOP_PARTNER_INVOICE    = 'Swoop Partner Invoice'
  TESLA_INVOICE            = 'Tesla Invoice'
  TRUCK_SUMMARY            = 'Truck Summary'

  validates :name, presence: true, uniqueness: true

  class << self

    def century_billing_file
      find_by!(name: CENTURY_BILLING_FILE)
    end

    def century_invoice
      find_by!(name: CENTURY_INVOICE)
    end

    def enterprise_invoice
      find_by!(name: ENTERPRISE_INVOICE)
    end

    def mts_invoice
      find_by!(name: MOTORCYLE_TOWING_INVOICE)
    end

    def storage_list
      find_by!(name: STORAGE_REPORT)
    end

    def tesla_invoice
      find_by!(name: TESLA_INVOICE)
    end

    def swoop_fleet_invoice
      find_by!(name: SWOOP_FLEET_INVOICE)
    end

    def swoop_partner_invoice
      find_by!(name: SWOOP_PARTNER_INVOICE)
    end

    def report_names
      YAML.load_file("db/reports.yml").keys
    end

    # Load reports from files and save to the database.
    def load_reports
      existing_ids = pluck(:id)
      changes = {}
      new_records = []
      YAML.load_file("db/reports.yml").each do |name, data|
        report = find_or_initialize_by(name: name)
        existing_ids.delete(report.id) if report.id
        report.company_type = data["company_type"]
        report.display_name = data["display_name"]
        report.display_order = data["display_order"]
        report.pdf = data["pdf"]
        report.load_params(data["params"])
        report.filter_sql = data["filters"]
        report.sql = File.read(data["sql"]).strip
        report.deleted_at = nil
        if report.changed?
          change_type = (report.new_record? ? "insert" : "update")
          if report.new_record?
            # new records get imported in batches below
            new_records << report
          else
            report.save!
          end
          changes[report.name] = change_type
          report.logger.debug("#{change_type} report: #{report.name}")
        end
      end

      # if we have new records to add do them in a single bulk insert
      if new_records.present?
        import new_records
      end

      unless existing_ids.empty?
        where(id: existing_ids).each do |report|
          unless report.deleted_at
            report.update!(deleted_at: Time.current)
            changes[report.name] = "delete"
            report.logger.debug("delete report: #{report.name}")
          end
        end
      end

      changes
    end

  end

  def last_result
    results.where('serialized_values IS NOT NULL').order(:updated_at).last
  end

  def running?
    results.where(serialized_values: nil).exists?
  end

  def stale?
    last_result.nil? || last_result.updated_at < 10.seconds.ago # 1.minute.ago
  end

  def refresh!
    if !running? && stale?
      results.create!.execute_asynchronously!(sql)
    end
  end

  def processed_available_params(current_company_report:, user:)
    {}.tap do |ret|
      available_params.each do |param, options|
        ret[param] = if options['type'].eql?('eval')
                       handle_eval_param(user, current_company_report, options)
                     else
                       options
                     end
      end
    end
  end

  def handle_eval_param(user, current_company_report, options)
    return {} unless respond_to?(options['method'])
    send(options['method'], user, current_company_report)
  end

  # FILTERS
  # { type: "start_date", default: Time.now }
  def start_date_yesterday_beginning_of_day(user, _ignored)
    tz = user ? user.tz_name : 'America/Los_Angeles'
    time = 1.day.ago.in_time_zone(tz).beginning_of_day
    { type: "start_date", default: time }
  end

  def end_date_yesterday_end_of_day(user, _ignored)
    tz = user ? user.tz_name : 'America/Los_Angeles'
    time = 1.day.ago.in_time_zone(tz).end_of_day
    { type: "end_date", default: time }
  end

  # {type: "options", ?: [{text: "All", value: "0"}, ....], ?:?}
  def storage_types(_user, current_company_report)
    values = [text: "All", value: "0"]

    stypes = StorageType.where(company_id: current_company_report.company_id)
    stypes.each do |stype|
      values << { text: stype.name, value: stype.id.to_s }
    end

    { label: "Types:", options: values, type: "options" }
  end

  def collatable_drivers(_user, current_company_report)
    values = [{ text: "All & Individual", value: "0" }, { text: "All", value: "-1" }]

    current_company_report.company.users.with_role(:driver).each do |driver|
      values << { text: driver.full_name, value: driver.id.to_s }
    end

    { label: "Driver:", options: values, type: "options" }
  end

  def dispatchers(_user, current_company_report)
    values = [{ text: "All", value: "0" }]

    current_company_report.company.users.with_role(:dispatcher).each do |dispatcher|
      values << { text: dispatcher.full_name, value: dispatcher.id.to_s }
    end

    { label: "Dispatcher:", options: values, type: "options" }
  end

  def customize_vehicle_label(user, company_report)
    values = [{ text: "All", value: "0" }]

    vehicles = company_report.company.vehicles.where(deleted_at: nil).order(created_at: :desc)
    vehicles.each do |vehicle|
      values << { text: vehicle.name, value: vehicle.id.to_s }
    end

    { label: "Truck:", options: values, type: "options" }
  end

  def payment_types(user, company_report)
    values = [{ text: "All", value: "0" }]

    types = company_report.company.customer_types
    types.each do |customer_type|
      values << { text: customer_type.name, value: customer_type.id.to_s }
    end

    { label: "Payment Type:", options: values, type: "options" }
  end

  def invoice_payment_methods(user, company_report)
    values = [{ text: "All", value: "0" }]

    PaymentMethod.all.each do |payment_method|
      values << { text: payment_method.name, value: payment_method.id.to_s }
    end

    { label: "Payment Method:", options: values, type: "options" }
  end

  def invoice_status(user, company_report)
    values = [
      { text: "All", value: "0" },
      { text: "Unsent", value: "1" },
      { text: "Outstanding", value: "2" },
      { text: "Paid", value: "3" },
    ]

    { label: "Invoice Status:", options: values, type: "options" }
  end

  def user_sites(user, company_report)
    sites = user.sites
    sites = company_report.company.sites if sites.empty?

    values = [{ text: "All", value: "0" }]
    sites.each do |site|
      values << { text: site.name, value: site.id.to_s }
    end

    { label: "Site:", options: values, type: "options" }
  end

  def partner_sites(user, company_report)
    sites = company_report.company.sites.order("name ASC")

    values = [{ text: "All", value: "0" }]
    sites.each do |site|
      values << { text: site.name, value: site.id.to_s }
    end

    { label: "Site:", options: values, type: "options" }
  end

  def live_sites(user, company_report)
    sites = company_report.company.live_sites.order("name ASC")

    values = [{ text: "All", value: "0" }]
    sites.each do |site|
      values << { text: site.name, value: site.id.to_s }
    end

    { label: "Site:", options: values, type: "options" }
  end

  def vehicle_categories(user, company_report)
    categories = company_report.company.vehicle_categories.order("name ASC")

    values = [{ text: "All", value: "0" }]
    categories.each do |category|
      values << { text: category.name, value: category.id.to_s }
    end

    { label: "Class Type:", options: values, type: "options" }
  end

  # Load the various report parameter fields from a previously serialized hash.
  def load_params(params)
    names = []
    defaults = {}
    mandatory = {}
    available = {}

    params.each do |param|
      attributes = param.dup
      name = attributes.delete("name")
      names << name unless param["filter_only"] == true
      defaults[name] = attributes.delete("default") if attributes.include?("default")
      mandatory[name] = attributes.delete("mandatory") if attributes.include?("mandatory")
      available[name] = attributes unless attributes.empty?
    end

    self.all_params = names
    self.default_params = defaults
    self.mandatory_params = mandatory
    self.available_params = available
  end

end
