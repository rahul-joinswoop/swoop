# frozen_string_literal: true

class Tire < BaseModel

  belongs_to :tire_type
  has_many :inventory_items, as: :item

end
