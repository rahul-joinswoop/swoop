# frozen_string_literal: true

class API::AsyncRequest < ApplicationRecord

  belongs_to :company
  belongs_to :user

  # systems covered by API::AsyncRequest
  DELETE_CUSTOM_ACCOUNT = 'DeleteCustomAccount'
  FARMERS_COVERAGE = 'FarmersCoverage'
  POLICY_LOOKUP_SERVICE = 'PolicyLookupService'
  FLEET_RESPONSE_INVOICE = 'FleetResponseInvoice'
  INVOICE_CHARGE_CARD = 'InvoiceChargeCard'
  INVOICE_REFUND_CARD = 'InvoiceRefundCard'
  RSC_REQUEST_MORE_TIME = 'RscRequestMoreTime'
  SWOOP_PARTNER_INVOICE_DOWNLOAD = 'SwoopPartnerInvoiceDownload'
  SWOOP_FLEET_INVOICE_DOWNLOAD = 'SwoopFleetInvoiceDownload'

end
