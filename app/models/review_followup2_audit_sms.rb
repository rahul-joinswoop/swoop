# frozen_string_literal: true

class ReviewFollowup2AuditSms < AuditSms

  def redirect_url
    "#{ENV['SITE_URL']}"
  end

end
