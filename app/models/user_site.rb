# frozen_string_literal: true

class UserSite < BaseModel

  belongs_to :user
  belongs_to :site

  validates :user, presence: true, uniqueness: { scope: :site }
  validates :site, presence: true

end
