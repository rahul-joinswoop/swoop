# frozen_string_literal: true

# During creation of a company by a Fleet, two RescueProviders are created:
#
# - one for the Fleet that created the company
# - one for Swoop
#
# Both will have RescueProviderTag (aka Network Status in UI). And both will
# have the name of the Fleet that created the Company as the Tag.
#
# The RescueProviderTag created for the **Fleet RescueProvider** is now not
# used, though @quinnbaetz suggested to store it because it can be useful in the
# future.
#
# The RescueProviderTag created for **Swoop RescueProvider** is what is rendered
# in Partners tab for Swoop users. Since the tag will be the name of the
# FleetInHouse, it will be displayed in the respective row.
#
# So if the Fleet that created the Company is called Tesla, the Network status
# for the Swoop Provider will be Tesla.
#
# If the Fleet that created the Company is called SuperFleetX, the Network
# status for the Swoop Provider will be SuperFleetX.
class RescueProvidersTag < BaseModel

  belongs_to :rescue_provider
  belongs_to :tag

end
