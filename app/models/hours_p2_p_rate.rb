# frozen_string_literal: true

class HoursP2PRate < Rate

  validates :hourly, presence: true

  def line_item_descriptions
    [TAX, PORT_TO_PORT_HOURS]
  end

  def mail_attributes
    base = super

    base.merge({ "Hourly P2P": hourly })
  end

  def calculate_line_items(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    lis = []
    lis << base_hours(invoicable)
    add_common_items(invoicable, lis)
    lis
  end

end
