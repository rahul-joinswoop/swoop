# frozen_string_literal: true

class CustomAccount < BaseModel

  include SoftDelete

  EXTERNAL_ACCOUNT_VERIFICATION_ID = 'external_account'
  TEMPLATE_PATH = "api/v1/custom_accounts/show"

  belongs_to :company

  serialize :verification_fields_needed

  validates :upstream_account_id, :company_id, presence: true
  validates :upstream_account_id, uniqueness: true
  validates_uniqueness_of :company_id, if: :any_active_for_company?

  def complete?
    !!upstream_account_id
  end

  def verification_failure?
    fields = Array(verification_fields_needed)
    fields -= [EXTERNAL_ACCOUNT_VERIFICATION_ID]
    fields.any?
  end

  def verification_fields_mismatch?(fields)
    verification_fields_needed != fields
  end

  def sync_verification_fields(fields)
    if verification_fields_mismatch?(fields)
      update!({ verification_fields_needed: fields })
    end
  end

  def listeners
    [company]
  end

  def render_to_hash
    controller = ActionController::Base.new

    locals = {
      :@custom_account => self,
    }

    if deleted_at.blank?
      locals[:@bank_account] = bank_account
    end

    rendered_template = controller.render_to_string(TEMPLATE_PATH, locals: locals)
    JSON.parse(rendered_template)
  end

  private

  def bank_account
    @bank_account ||= begin
      retriever = StripeIntegration::BankAccountRetriever.new({
        stripe_account_id: upstream_account_id,
      })
      retriever.retrieve
    end
  end

  def any_active_for_company?
    !deleted_at && CustomAccount.not_deleted.where(
      company_id: company_id
    ).where.not(id: id).exists?
  end

end
