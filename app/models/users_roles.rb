# frozen_string_literal: true

class UsersRoles < ApplicationRecord

  belongs_to :user
  belongs_to :role

  after_commit { Subscribable.trigger_user_updated user }

end
