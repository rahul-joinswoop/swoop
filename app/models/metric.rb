# frozen_string_literal: true

class Metric < ApplicationRecord

  belongs_to :target, polymorphic: true

  belongs_to :user
  belongs_to :company

  # examples
  # Metric.create(
  # target:<job>,
  # action:Metric.sent,
  # label:"sms_dispatched",
  # value:1
  # )

  # Common actions
  SENT = "sent"
  CLICKED = "clicked"
  VIEWED = "viewed"

  # label examples
  # ['sms_dispatch','get_location_mobile_web'...

end
