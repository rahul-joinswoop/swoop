# frozen_string_literal: true

class VehicleLocation

  attr_reader :vehicle_id, :lat, :lng, :timestamp, :speed

  def initialize(vehicle_id:, lat:, lng:, timestamp:, speed: nil)
    @vehicle_id = Integer(vehicle_id)
    @lat = Float(lat)
    @lng = Float(lng)
    @timestamp = timestamp.to_time
    @speed = Float(speed) if speed.present?
  end

  def to_coordinates
    [lat, lng] if lat && lng
  end

end
