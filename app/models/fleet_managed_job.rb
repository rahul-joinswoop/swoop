# frozen_string_literal: true

class FleetManagedJob < FleetJob

  #
  # Every fleet managed job must either have...
  # a) no drop location, or
  # b) a properly formed Location record for its drop location
  #
  # That is: a fleet managed job never has a drop location
  # that is an empty Location record. Doing so would mean that
  # `if @fleet_managed_job.drop_location` is meaningless.
  #
  # This validation enforces that, by expecting a drop location
  # to have at least a lat + lng
  #
  # This is a workaround for ENG-8549: Add validations to enforce
  # that Location models are never empty
  #
  validate :validate_drop_location_isnt_empty, if: :drop_location

  # only defined for FleetManagedJob, stubbed out for all other classes because reasons. see
  # Invoice#fleet_managed_invoice_approved_or_sent? for the only known usage. original comments
  # from Invoice#find_active_fleet_managed_invoice:
  #
  # If the job is FleetManagedJob, and this is a PartnerInvoice
  # approved or downloaded by Swoop, a correspondent FleetManagedInvoice
  # will exist.
  #
  # @return the 'FleetManagedInvoice'
  has_one :active_fleet_managed_invoice_from_swoop, -> { where(deleted_at: nil).where('invoicing_ledger_items.sender_id = ?', Company.swoop_id) }, class_name: 'Invoice', inverse_of: :job, foreign_key: :job_id

  # Allow for supports to be built
  supports[:auto_assign] = true

  # Refactored swoop_fleet_managed_override_dispatched
  before_create :init_review_sms

  # notifications
  after_commit do
    if saved_change_to_status? || saved_change_to_rescue_company_id?
      Notifiable.trigger_fleet_managed_job_assigned self
    end
  end

  aasm column: :status, enum: true, no_direct_assignment: false, whiny_persistence: true do # need direct assigment to allow undo_storage
    event :fleet_managed_override_canceled, after: [:send_cancelled_alert_swoop, :cancel_invoice] do
      transitions from: [:pending, :unassigned, :assigned, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed], to: :canceled, guard: :fleet_job?
    end

    event :swoop_draft do
      transitions from: :predraft, to: :draft, guards: [:fleet_job?]
    end

    event :swoop_assign do
      transitions from: [:pending, :unassigned, :auto_assigning], to: :assigned, guard: :partner_live_and_open?
      transitions from: [:pending, :unassigned, :auto_assigning], to: :dispatched
    end

    event :swoop_auto_assign do
      transitions from: [:pending, :unassigned, :auto_assigning], to: :assigned
    end

    event :swoop_auto_dispatch do
      transitions from: [:assigned, :auto_assigning], to: :dispatched
    end

    event :swoop_auto_assign_failed do
      transitions from: [:auto_assigning], to: :pending
    end

    event :swoop_auction_won do
      transitions from: [:auto_assigning], to: :accepted
    end

    event :swoop_auction_no_winner do
      transitions from: [:auto_assigning], to: :pending
    end

    event :swoop_dispatched do
      transitions from: [:pending, :assigned, :accepted, :unassigned, :enroute, :onsite, :towing, :towdestination, :completed, :canceled, :goa], to: :dispatched, guards: [:fleet_job?]
    end

    event :swoop_enroute do
      transitions from: [:pending, :assigned, :accepted, :unassigned, :dispatched, :onsite, :towing, :towdestination, :completed, :canceled, :goa], to: :enroute, guards: [:fleet_job?]
    end

    event :swoop_onsite do
      transitions from: [:pending, :assigned, :accepted, :unassigned, :dispatched, :enroute, :towing, :towdestination, :completed, :canceled, :goa], to: :onsite, guards: [:fleet_job?]
    end

    event :swoop_towing do
      transitions from: [:pending, :assigned, :accepted, :unassigned, :dispatched, :enroute, :onsite, :towdestination, :completed, :canceled, :goa], to: :towing, guards: [:fleet_job?]
    end

    event :swoop_towdestination do
      transitions from: [:pending, :assigned, :accepted, :unassigned, :dispatched, :enroute, :onsite, :towing, :completed, :canceled, :goa], to: :towdestination, guards: [:fleet_job?]
    end

    event :swoop_completed do
      transitions from: [:pending, :assigned, :accepted, :unassigned, :dispatched, :enroute, :onsite, :towing, :towdestination, :canceled, :goa], to: :completed, guards: [:fleet_job?]
    end

    event :swoop_canceled do
      transitions from: [:draft, :pending, :unassigned, :assigned, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :goa, :auto_assigning], to: :canceled, guard: :fleet_job?
    end

    event :swoop_goa do
      transitions from: [:accepted, :pending, :unassigned, :assigned, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :canceled], to: :goa, guard: :fleet_job?
    end

    event :swoop_reassigned do
      transitions from: [:assigned, :assigned, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination], to: :reassigned, guard: :fleet_job?
    end

    event :swoop_deleted do
      transitions from: [:draft, :completed, :canceled, :rejected, :unassigned, :reassigned, :stored, :released, :goa, :expired, :auto_assigning], to: :deleted, guard: :fleet_job?
    end
  end

  def fleet_status_method_name(status)
    "fleet_managed_override_" + status.delete(' ').downcase
  end

  def swoop_status_method_name(status)
    "swoop_fleet_managed_override_" + status.delete(' ').downcase
  end

  def assigning_company
    Company.swoop
  end

  def email_dispatcher
    OpenStruct.new(full_name: 'Swoop',
                   email: 'dispatch@joinswoop.com',
                   national_phone: Job::DEFAULT_SUPPORT_PHONE)
  end

  #
  # Verify that the drop location has a lat and a lng
  #
  def validate_drop_location_isnt_empty
    errors.add(:drop_location, "must have a LAT and LNG") if drop_location.lat.blank? || drop_location.lng.blank?
  end

end
