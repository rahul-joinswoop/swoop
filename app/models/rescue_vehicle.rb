# frozen_string_literal: true

class RescueVehicle < Vehicle

  include GraphQLSubscribers
  include RescueVehicleAuction
  include Utils

  belongs_to :vehicle_category
  has_many :jobs

  scope :dedicated_to_swoop, -> { where(dedicated_to_swoop: true) }
  scope :dispatchable, -> { has_partner_dispatch_feature.dedicated_to_swoop.has_coords.distinct }
  scope :stale_auto_dispatch_location, -> { where("location_updated_at < ?", 10.minutes.ago).where(dedicated_to_swoop: true) }
  scope :has_coords, -> { where.not(lat: nil, lng: nil) }
  scope :has_partner_dispatch_feature, -> { joins(:features).where("features.name=?", Feature::PARTNER_DISPATCH_TO_TRUCK) }
  scope :location_current, -> { where("location_updated_at > ?", 5.minutes.ago) }
  scope :eligable_for_swoop_auto_dispatcher, -> { location_current.has_coords.distinct }

  has_many :features, through: :company
  attr_accessor :location_update, :estimated_eta

  validates_presence_of :name

  after_commit do
    # when the only changes are from UpdateVehicleLocationWorker then we only notify the customer
    # on current jobs since the changes happen very frequently and nobody but the customer needs the updates this way
    if Set.new(previous_changes.keys) == Set.new(LOCATION_UPDATE_WORKER_ATTRS)
      # This worker can cause problems if called with too many ids so guard against a vehicle
      # that is assigned to too many jobs.
      unless has_no_jobs? || block_job_update?
        current_job_ids = current_jobs.limit(25).pluck(:id)
        GraphQL::JobUpdatedWorker.perform_async(ids: current_job_ids, customer_only: true)
      end
    else
      # otherwise the vehicle properties changed and we need to publish a regular update.
      Subscribable.trigger_partner_vehicle_changed(self)
    end
  end

  def description
    "#{name}"
  end

  def update_name(name)
    self.name = name
  end

  def update_number(num)
    num = num.tr('#', '')
    if num.is_i?
      self.number = num.to_i
    else
      self.number = nil
    end
  end

  def number_default(default)
    if number
      ret = number
    else
      ret = default
    end
    ret
  end

  def is_available?
    !driver.nil? && driver.on_duty?
  end

  # Update vehicle location
  def update_location!(lat:, lng:, timestamp:)
    lat = Float(lat).round(8)
    lng = Float(lng).round(8)
    timestamp = timestamp.to_time

    # Keep updating database for now since POC
    if location_updated_at.nil? || location_updated_at < timestamp
      self.lat = lat
      self.lng = lng
      self.location_updated_at = timestamp
      self.location_update = true
      save!
    end

    remove_instance_variable(:@current_location) if defined?(@current_location)
  end

  def current_location
    unless defined?(@current_location)
      if lat && lng && location_updated_at
        @current_location = VehicleLocation.new(vehicle_id: id, lat: lat, lng: lng, timestamp: location_updated_at)
      else
        @current_location = nil
      end
    end
    @current_location
  end

  def listeners
    ret = super
    if Company.swoop.has_feature(Feature::SWOOP_ROOT_ALL_VEHICLE_UPDATES)
      ret << Company.swoop
    end
    ret
  end

  def subscribers
    ret = Set.new

    # add a vehicle's company if we've got it
    (ret << company) if company.present?

    # add swoop if that feature is set
    if Company.swoop.has_feature(Feature::SWOOP_ROOT_ALL_VEHICLE_UPDATES)
      ret << Company.swoop
    end

    # add any customers of current jobs for this vehicle
    ret += current_jobs.includes(:customer).joins(:customer).map(&:customer)

    # and go through our subscribers looking for RescueCompanies - if we
    # find one add all the dispatchers from the company to our list of
    # subscribers
    ret = add_dispatchers ret

    # go through our subscribers looking for Companies and add any oauth_applications
    # they may have to our subscribers
    ret = add_applications ret

    ret
  end

  # TODO temporary feature flag to avoid breaking production if the system cannot handle
  # the number of updates. This method should be removed after the initial production deploy
  # BD 2019-06-20
  def block_job_update?
    Flipper[:block_location_updates_to_current_jobs].enabled?
  end

  # The site this truck is associated with (should come from UI in future)
  def associated_site
    company.hq
  end

end

class String

  def is_i?
    /\A[-+]?\d+\z/ === self
  end

end
