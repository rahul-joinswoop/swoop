# frozen_string_literal: true

class User < BaseModel

  include FullNameSplitter
  include IdentityCache
  include User::Searchable
  include SoftDelete
  include GraphQLSubscriber
  include GraphQLSubscribers

  self.ignored_columns = %w(
    reset_password_token reset_password_sent_at remember_created_at
    sign_in_count current_sign_in_at last_sign_in_at current_sign_in_ip
    last_sign_in_ip role drupal_id
  )

  rolify

  devise :database_authenticatable, :validatable

  scope :root_users,       -> { with_role(:root).where(deleted_at: nil) }
  scope :root_alert_users, -> { root_users.where(receive_system_alerts: true).where("phone IS NOT NULL or phone <> ''") }

  scope :swoop_non_root,   -> { where(company_id: Company.swoop_id).without_role(:root) }
  scope :swoop_non_root_alert_users, -> { swoop_non_root.where(receive_system_alerts: true).where("phone IS NOT NULL or phone <> ''") }

  scope :emailable, -> { where("email IS NOT NULL AND email != ''") }
  scope :not_null_company_id, -> { where("company_id IS NOT NULL") }

  has_one :drive

  # TODO: remove rescue_vehicles and consumer_vehicles from User as it looks like we never use it - WMD
  has_many :rescue_vehicles, -> { where type: 'RescueVehicle' }, through: :drive, source: :vehicle
  has_many :consumer_vehicles, -> { where type: 'ConsumerVehicle' }, through: :drive, source: :vehicle

  has_many :twilio_replies
  has_many :jobs, class_name: "Job", foreign_key: "rescue_driver_id", inverse_of: :rescue_driver
  has_many :current_jobs, -> { where status: [Job::DISPATCHED, Job::ENROUTE, Job::ONSITE, Job::TOWING, Job::TOWDESTINATION] }, class_name: "Job", foreign_key: "rescue_driver_id", inverse_of: :rescue_driver
  # user_sites association used to assign users to sites, creating an admin

  has_many :user_sites
  has_many :sites, through: :user_sites
  has_many :job_status_email_preferences
  has_many :job_status_email_statuses, class_name: "JobStatus", through: :job_status_email_preferences, source: :job_status
  has_many :system_email_filters
  has_many :system_email_filtered_companies, through: :system_email_filters, source: :company
  has_many :access_tokens, class_name: "Doorkeeper::AccessToken", foreign_key: :resource_owner_id, dependent: :delete_all # rubocop:disable Rails/InverseOf
  has_many :qb_tokens, class_name: "QB::Token", foreign_key: :created_by_user_id, inverse_of: :user
  has_many :invoices, as: :recipient, inverse_of: :_user_recipient
  has_many :notification_settings,
           -> { joins(:user).where.not(users: { company_id: nil }) },
           class_name: 'UserNotificationSetting',
           inverse_of: :user,
           autosave: true

  has_many :visits, class_name: "User::Visit", inverse_of: :user

  belongs_to :company, inverse_of: :users
  # note that accepts_nested_attributes_for :customer sets autosave: true here,
  # which calls autosave_associated_records_for_customer
  belongs_to :customer
  belongs_to :location
  belongs_to :invited_by_user, class_name: "User"
  belongs_to :invited_by_company, class_name: "Company"
  belongs_to :vehicle # user driving one vehicle #WARN setting this to nil doesn't work....

  has_one :customer_of_company, through: :customer, source: :company
  has_one :job, through: :drive

  before_save :normalize_phone

  before_save :encode_username, if: :will_save_change_to_username?
  before_save :encode_email, if: :will_save_change_to_email?

  after_commit :publish_updates, on: :update

  after_commit do
    Subscribable.trigger_job_updated current_jobs.union(job.present? ? Job.where(id: job.id) : Job.none)
    Subscribable.trigger_user_updated self
  end

  # Ensure that the job index is up to date with any changes made on the association
  after_commit(on: :update) { drive.job.reindex if drive&.job }

  after_commit do
    if saved_change_to_vehicle_id && vehicle_id
      # we do these updates in a worker so we don't block user/vehicle updates
      current_jobs.select(:id, :rescue_driver_id, :status).find_in_batches(batch_size: 50) do |batch|
        AssignRescueDriverVehicleToJobWorker.perform_async batch.map(&:id)
      end
    end
  end

  validates_uniqueness_of :username, if: -> { username.present? && not_deleted? }
  validates_uniqueness_of :email, if: -> { email.present? && !not_deleted? }
  validates_uniqueness_of :first_name, scope: [:last_name, :company_id],
                                       if: :active_company_user?,
                                       conditions: -> { not_deleted },
                                       message: "and Last name have already been taken for this Company"
  # TODO: Extracting these out into module in seperate PR
  validates_presence_of :language
  validates_inclusion_of :language, {
    in: Company::SUPPORTED_LANGUAGES,
    message: Company::NOT_SUPPORTED_ERROR_MSG,
  }
  before_validation :set_default_language

  validate :first_or_last_name_present

  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :customer

  alias_attribute :job_update_email, :email
  alias_attribute :review_email, :email

  delegate :is_farmers_or_century?, to: :company
  delegate :member_number, to: :customer, allow_nil: true

  # TODO temporary code to make sure that these field stay in sync while migrating to case insensitive
  # without locking the database. This is needed since there a too many rows in the table to sync without
  # either locking the table or creating a race condition.
  before_save do
    self._x_first_name = first_name
    self._x_last_name = last_name
    self._x_username = username
    self._x_email = email
  end

  class << self

    def find_by_email_or_usename(identifier)
      return nil if identifier.blank?
      user = nil
      if identifier.include?("@")
        user = find_by(email: identifier, deleted_at: nil)
      end
      user ||= find_by(username: identifier, deleted_at: nil)
      user
    end

    def swoop_auto_dispatch
      find_by(username: 'swoop-auto-dispatch')
    end

    # TODO can we do this in elasticsearch?
    def search_by_full_name(full_name)
      where("LOWER(CONCAT(first_name, ' ', last_name)) = LOWER(?)", "#{full_name}")
    end

    def lookup_by_encoded_email_or_username(key)
      find_by(email_key: key) ||
        find_by(username_key: key)
    end

  end

  # autosave_associated_records_for_ is magically generated at runtime and the
  # only way i figured out to get access to super is via prepend
  module NestedCustomer

    # https://stackoverflow.com/questions/3579924/accepts-nested-attributes-for-with-find-or-create
    def autosave_associated_records_for_customer
      # if we have an existing record use it
      # TODO - member_number can be null in the db but for now we require it here
      # since otherwise we have no distinguishing features for a Customer. in
      # the future it could be one of member_number / phone_number / vin.
      if customer.present? && customer.company_id.present? && customer.member_number.present?
        new_customer = Customer.find_by(company_id: customer.company_id, member_number: customer.member_number)
        self.customer = new_customer if new_customer.present?
      end
      super
    end

  end

  prepend NestedCustomer

  def job_status_email_status_names
    job_status_email_statuses.map(&:name)
  end

  # Override devise validatable check
  def email_required?
    false
  end

  # Override devise validatable check
  def password_required?
    false
  end

  def publish_updates
    if previous_changes[:vehicle_id]
      if previous_changes[:vehicle_id][0]
        Vehicle.find(previous_changes[:vehicle_id][0]).base_publish('update')
      end

      # make sure we update the new vehicle assigned through WS too.
      # (Addeed because it was not updating the FE vehicle when I set a vehicle for a user with vehicle_id == nil)
      # e.g: u = User.find(1); u.vehicle_id <nil>; u.vehicle_id = 20; u.save
      if previous_changes[:vehicle_id][1]
        Vehicle.find(previous_changes[:vehicle_id][1]).base_publish('update')
      end
    end
  end

  def copy_user(extra_attributes = nil)
    copy_attributes = [
      'phone',
      'first_name',
      'last_name',
      'company_id',
    ]
    copy = self.class.new
    copy_attributes.each do |attr|
      copy[attr] = self[attr]
    end
    copy.password = nil
    copy
  end

  def national_phone
    if phone
      Phonelib.parse(phone).national
    end
  end

  # Phonelib.parse("+12315555555").national(false) =>  "2315555555"
  def index_phone
    if phone
      Phonelib.parse(phone).e164
    end
  end

  # On the front end `pager` is labeled as "secondar phone"
  def phone_numbers
    [phone, pager].compact
  end

  def render_to_hash
    controller = ActionController::Base.new

    JSON.parse(controller.render_to_string("api/v1/users/_user", locals: {
      user: self, include_notification_settings: has_role?(:rescue),
    }))
  end

  def listeners
    ret = Set.new

    if company
      # Always add company to the listeners
      ret << company

      # If the company is a fleet managed
      # then go ahead and add swoop to the list to limit the number of websockets
      # we are getting.
      if company.fleet_managed?
        ret << Company.swoop
      end
    end

    if !job || (job && job.fleet_managed_job?)
      ret << Company.swoop
    end

    ret
  end

  def subscribers
    ret = listeners
    ret << self

    # and go through our subscribers looking for RescueCompanies - if we
    # find one add all the dispatchers from the company to our list of
    # subscribers
    ret = add_dispatchers ret

    # go through our subscribers looking for Companies and add any oauth_applications
    # they may have to our subscribers
    ret = add_applications ret

    ret
  end

  def localtime(utc_dttm)
    if timezone
      tz = timezone
    else
      tz = 'America/Los_Angeles'
    end
    utc_dttm.in_time_zone(tz).strftime("%a, %d %b %Y %I:%M:%S %p %Z")
  end

  def tz_name
    timezone.present? ? timezone : 'America/Los_Angeles'
  end

  def name
    full_name
  end

  def sort_name
    I18n.transliterate([last_name, first_name].compact.join(", ").downcase.strip)
  end

  def user_agent_human
    if platform
      platform + '/' + (version ? version : '') + '/' + (build ? build : '')
      platform + '/' + (version ? version : '') + '/' + (build ? build : '')
    end
  end

  def human_roles
    # NOTE: neither roles.pluck(:name) nor roles.select(:name).map(&:name) work correctly here because they're
    # somehow cached after the first call and don't pick up newly added roles from our user spec.
    roles.map(&:name)
  end

  def symbolized_roles
    human_roles.map(&:to_sym)
  end

  # True if this user has all given roles to check.
  # False if any is msising.
  #
  # e.g.
  #
  # u = User.first
  # u.has_roles? :rescue, :admin
  #
  def has_roles?(*roles_to_check)
    Set[*human_roles].superset? Set[*roles_to_check.map(&:to_s)]
  end

  def admin?
    human_roles.include? 'admin'
  end

  # Check if the user is a "rescue driver" only.
  # Drivers are defined by the "rescue" and "driver" roles.
  #
  # If a "driver" also contains "dispatcher" or "admin" roles,
  # it means that he/she is not a "rescue driver" only.
  DRIVER_ONLY_ROLES = Set['driver', 'rescue'].freeze
  def driver_only?
    DRIVER_ONLY_ROLES == Set[*human_roles]
  end

  def grantable_roles
    Role.where(name: Users::FetchAllowedRoles.call(api_user: self))
  end

  # Return true if a user has whitelisted a site. If the user does not have
  # whitelisted sites on a company, then all sites are considered whitelisted.
  def user_site?(site)
    company_site_ids = Set.new
    # Calling sites.each rather than using a method that querie the database to prevent excessive
    # queries when iterating through a list of sites.
    sites.each { |s| company_site_ids << s.id if s.company_id == site.company_id }
    if company_site_ids.empty?
      true
    else
      company_site_ids.include?(site.id)
    end
  end

  def call_notification_enabled_by?(job_status_name:)
    should_receive_notification_by?(job_status_name: job_status_name, notification_channel: :Call)
  end

  def sms_notification_enabled_by?(job_status_name:)
    should_receive_notification_by?(job_status_name: job_status_name, notification_channel: :Text)
  end

  def encode_username
    self.username_key = (username.present? ? Digest::SHA256.hexdigest(username) : nil)
  end

  def encode_email
    self.email_key = (email.present? ? Digest::SHA256.hexdigest(email) : nil)
  end

  def set_default_language
    return if language

    self.language = company&.language || ENV['DEFAULT_LANGUAGE']
  end

  private

  def should_receive_notification_by?(job_status_name:, notification_channel:)
    user_setting = notification_settings.select do |setting|
      setting.supported_job_status_names.include? job_status_name.to_s
    end.first

    return false unless user_setting&.notification_channels&.any?

    user_setting.notification_channels.include?(notification_channel.to_s)
  end

  def normalize_phone
    self.phone = Phonelib.parse(phone).e164 if phone_changed?
    self.pager = Phonelib.parse(pager).e164 if pager_changed?
  end

  def active_company_user?
    company_id.present? && not_deleted?
  end

  def first_or_last_name_present
    return unless active_company_user?
    return if first_name.present? || last_name.present?

    errors.add(:first_name, "can't be blank if last name not provided")
    errors.add(:last_name, "can't be blank if first name not provided")
  end

end
