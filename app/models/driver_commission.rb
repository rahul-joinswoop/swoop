# frozen_string_literal: true

class DriverCommission < ApplicationRecord

  belongs_to :user
  belongs_to :company

end
