# frozen_string_literal: true

class TimeOfArrival < BaseModel

  include JobHistoryLabelConcern

  validates :time, presence: true
  validates :duration, numericality: { greater_than: 0 }, if: :motor_club_bid?

  def motor_club_bid?
    eba_type == BID && job.is_a?(FleetMotorClubJob)
  end

  belongs_to :job
  belongs_to :vehicle
  belongs_to :user
  belongs_to :source_application

  BID = 0                 # Bid by partner for a job
  ESTIMATE = 1            # Estimate by automated system via google directions API
  ACTUAL = 2              # Actual time of arrival
  SWOOP_ESTIMATE = 3      # Swoop user estimate
  FLEET = 4               # Fleet user estimate
  PARTNER = 5             # Partner estimates

  EBA_TO_HUMAN = ["Partner", "Live", "Actual", "Swoop", "Fleet", "Partner"].freeze

  scope :by_type,     ->(type) { where(eba_type: type).order(:created_at) }
  scope :non_live,    -> { where('eba_type <> ? AND eba_type <> ?', ESTIMATE, ACTUAL) }
  scope :actual,      -> { where(eba_type: ACTUAL) }
  scope :live,        -> { where(eba_type: ESTIMATE) }

  after_commit :enqueue_customer_eta_notification, if: ->(record) {
    saved_change_to_time? &&
    [BID, PARTNER, SWOOP_ESTIMATE].include?(record.eba_type)
  }

  after_commit do
    Subscribable.trigger_job_updated job
  end

  after_commit :notify_fleet_regarding_eta_extension, on: :create

  def enqueue_customer_eta_notification
    revision = case eba_type
               when BID
                 # TODO: FIgure out how this works
                 (saved_changes["time"] && saved_changes["time"].first)
               when PARTNER, SWOOP_ESTIMATE
                 has_other_toas?
               end

    CustomerEtaNotification.perform_async(id, revision)
  end

  def send_customer_eta_notification(revision)
    job.send_eta_notification(revision: revision, eba_type: eba_type)
  end

  def has_other_toas?
    self.class.where(job_id: job_id,
                     eba_type: [BID, PARTNER, SWOOP_ESTIMATE])
      .where("id IS DISTINCT FROM ?", id).exists?
  end

  def other_arrivals(type)
    self.class.where(job_id: job.id, eba_type: type)
  end

  def listeners
    job.listeners
  end

  def description(has_others = false)
    revision = has_others ? 'Revised ' : ''
    "#{revision}ETA Entered by #{EBA_TO_HUMAN[eba_type]} of #{duration}min"
  end

  def name
    description
  end

  def ui_type
    'TOA'
  end

  def last_set_dttm
    created_at
  end

  def adjusted_dttm
    created_at
  end

  # Safe operator & here is used call methods on objects without worrying that the object may be nil.
  # Checks if a company exists for a user and the returns user's company.
  # Similarly, with source_application, it checks if oauth_application and subsequently owner is available and returns it.
  # If the details fail to exist, then the history details will not have the company details shown for ETA details.
  def changed_by_company
    if user&.company
      user.company
    elsif source_application&.oauth_application&.owner.is_a?(Company)
      source_application.oauth_application.owner
    end
  end

  # Safe operator & here is used call methods on objects without worrying that the object may be nil.
  # Checks if name exists for a user and returns it. Else the history details will not have the for ETA details.
  def user_name
    user&.name
  end

  # Safe operator & here is used call methods on objects without worrying that the object may be nil.
  # Checks if a company exists for a user and the returns user's company. Else the history details will not have company for ETA.
  def company
    user&.company
  end

  def duration
    t = created_at
    t = Time.current if t.nil?
    minutes_until(t)
  end

  def minutes_until(until_time)
    ((time - until_time) / 60.0).round
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string("api/v1/toa/_toa", locals: { toa: self }))
  end

  def eba_type_for(company)
    self.eba_type = case company
                    when FleetCompany
                      FLEET
                    when SuperCompany
                      SWOOP_ESTIMATE
                    else
                      has_other_toas? ? PARTNER : BID
                    end
  end

  private

  def notify_fleet_regarding_eta_extension
    if job
      # Even though we may have recipients to send the email to, the JobStatusEmail logic
      # requires a user id or a company id. However, if the job doesn't have a user, it is
      # likely a motor club job and the motor club company would not be using this feature
      # anyway.
      # TODO if we ever fix the email logic to work with just recipients, we should remove this check. BD 2019-01-17
      return unless job.user

      # don't send eta_extension emails out when we do live (ESTIMATE) updates of eta
      return if eba_type == TimeOfArrival::ESTIMATE

      delay = 0
      toas = job.time_of_arrivals_non_live.last(2)
      delay = (toas.last.time - toas.first.time) / 1.minute if toas.size == 2

      if delay > 15
        users = []
        companies = [job.fleet_company, job.rescue_company]
        companies << Company.swoop if job.fleet_managed_job?

        companies.each do |company|
          users += company.users.select(:email).where.not(email: nil).includes(:job_status_email_preferences).where(job_status_email_preferences: { job_status_id: JobStatus::ETA_EXTENDED })
        end

        job.send_eta_extension_email({ to: users.collect(&:email) }) if users.any?
      end
    end
  end

end
