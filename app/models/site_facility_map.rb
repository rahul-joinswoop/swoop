# frozen_string_literal: true

# This model is the map between fleet company facilities and Swoop sites.
# This is specifically set up for mapping Agero facilities but is designed
# to be flexible.
#
# The mapping is done via geographic location. Agero, for example, can define
# multiple facilities to a single physical address for billing/rate purposes
# internal to their systems. Swoop sites are mapped to physical addresses so
# a site can have multiple facilities assigned to it.
#
# This table maps back to the isscs table via the issc_locations table.
class SiteFacilityMap < ApplicationRecord

  belongs_to :company, class_name: "RescueCompany", optional: false
  belongs_to :fleet_company, class_name: "FleetCompany", optional: false
  belongs_to :facility, class_name: "Location", optional: false
  belongs_to :site, optional: true

  validate :ensure_site_belongs_to_company

  after_save :sync_issc_location_sites

  def issc_locations
    IsscLocation.where(fleet_company_id: fleet_company_id, company_id: company_id, location_id: facility_id)
  end

  private

  def sync_issc_location_sites
    issc_locations.each do |issc_location|
      if issc_location.site_id != site_id
        issc_location.update!(site_id: site_id)
      end
    end
  end

  def ensure_site_belongs_to_company
    if site
      if site.company
        errors.add(:site, "must belong to the company") unless site.company == company
      else
        self.company = site.company
      end
    end
  end

end
