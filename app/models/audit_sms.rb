# frozen_string_literal: true

class AuditSms < ApplicationRecord

  include JobHistoryLabelConcern

  belongs_to :job
  belongs_to :user
  belongs_to :source_application

  after_commit :publish_job, on: :update

  validates :type, uniqueness: { scope: [:job_id] }, if: :branded?

  SMS = 'SMS'
  SMS_REPLY = 'SMSReply'

  def name
    reply_human_status || "#{description} #{human_status}"
  end

  def ui_type
    if reply_human_status
      SMS_REPLY
    else
      SMS
    end
  end

  def last_set_dttm
    created_at
  end

  def adjusted_dttm
    if reply_human_status
      response_received_at
    else
      sent_date
    end
  end

  def branded?
    is_a? Sms::BrandedReviewLink
  end

  def publish_job
    if saved_change_to_status? && job
      job.base_publish("update")
    end
  end

  def status_callback_url
    "#{ENV['SITE_URL']}/twilio_status/#{id}"
  end

  def sent_date
    created_at
  end

  def human_status
    (status || 'sent').titlecase
  end

  def reply_human_status
    nil
  end

  def description
    self.class.description
  end

  # Support for sms conversations

  def find_target(phone)
    Rails.logger.error "Asked to find target on an SMS with no overridden find_target method"
  end

  def expecting_reply?
    false
  end

  def listeners
    []
  end

  def visible_to_company(company)
    true
  end

  class << self

    attr_accessor :description

  end

end
