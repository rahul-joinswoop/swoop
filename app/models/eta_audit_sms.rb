# frozen_string_literal: true

class EtaAuditSms < AuditSms

  self.description = 'Track Driver Text'

  def redirect_url
    "#{ENV['SITE_URL']}/show_eta/#{job.uuid}"
  end

end
