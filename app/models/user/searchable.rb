# frozen_string_literal: true

# ElasticSearch specific definitions for users.
module User::Searchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    index_name Swoop::ElasticSearch.index_name(:users)

    scope :elastic_search_backfill, -> {
      finder = not_null_company_id.elastic_search_preload
      ts = Time.zone.parse(ENV['MODEL_TS']) if ENV['MODEL_TS'].present?
      finder = finder.where('users.updated_at >= ?', ts) if ts.present?
      finder
    }

    scope :elastic_search_preload, -> {
      preload([:company])
    }

    after_commit on: [:create, :update], if: :company_id? do
      Indexer.perform_async(:index, User.name, id)
    end

    after_commit on: [:destroy] do
      Indexer.perform_async(:delete, User.name, id)
    end

    settings index: {
      number_of_shards: 2,
      number_of_replicas: 1,
      analysis: {
        filter: {
          preserve_asciifolding: {
            type: "asciifolding",
            preserve_original: true,
          },
          strip_punctuation: {
            type: "pattern_replace",
            pattern: "\\p{P}",
            replacement: "",
          },
        },
        analyzer: {
          name_analyzer: {
            type: "custom",
            tokenizer: "standard",
            filter: ["standard", "lowercase", "preserve_asciifolding", "strip_punctuation"],
          },
        },
      },
    } do
      mappings dynamic: 'false' do
        indexes :id, type: :integer
        indexes :deleted_at, type: :date
        indexes :email
        indexes :first_name, analyzer: :name_analyzer
        indexes :last_name, analyzer: :name_analyzer
        indexes :sort_name, type: :keyword
        indexes :username
        indexes :phone, type: :keyword
        indexes :company do
          indexes :id, type: :integer
          indexes :name, analyzer: :name_analyzer
        end
      end
    end
  end

  def as_indexed_json(_options = {})
    as_json(
      only: [
        :deleted_at,
        :email,
        :first_name,
        :id,
        :last_name,
        :username,
      ],
      include: {
        company: { only: [
          :id,
          :name,
        ] },
      }
    ).merge("phone" => phone_numbers, "sort_name" => sort_name)
  end

end
