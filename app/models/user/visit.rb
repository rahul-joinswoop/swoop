# frozen_string_literal: true

# Model for tracking visits by a user broken down per day, per source application.
# The date for visits is defined in UTC, not the user's local time zone.
#
# A "session" is counted as having visited at least once in an hour. Redis is used
# to track when a session started by means of auto expiring keys.
class User::Visit < ApplicationRecord

  belongs_to :user, inverse_of: :visits
  belongs_to :source_application

  validates :session_count, inclusion: { in: (1..24) }

  class SessionTracker

    attr_reader :key

    def initialize(user_id, source_application_id)
      @key = "UserVisit(#{user_id},#{source_application_id})"
    end

    # This method will check redis for a key that expires every hour. If the
    # key is not set, then the block passed to this method is called. Otherwise
    # false is returned.
    def track!(&block)
      RedisClient.with(:default) do |redis|
        # If the redis key exists, then this session has already been tracked.
        return false if redis.exists(key)

        previous_value, _ = redis.multi do |transaction|
          transaction.getset(key, "1")
          transaction.expire(key, 1.hour.to_i)
        end

        # If the getset call returned a value, then we hit a race condition and another
        # process is already tracking the session.
        return false if previous_value.present?
      end

      yield

      true
    end

    # Clear the session tracking flag. Used for testing.
    def clear!
      RedisClient[:default].del(key)
    end

  end

  class << self

    # Track a user visit.
    def track(user_id:, source_application_id:)
      SessionTracker.new(user_id, source_application_id).track! do
        UserVisitWorker.perform_async(Date.today.iso8601, user_id, source_application_id)
      end
    end

  end

end
