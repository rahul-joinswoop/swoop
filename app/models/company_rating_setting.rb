# frozen_string_literal: true

class CompanyRatingSetting < ApplicationRecord

  belongs_to :company

end
