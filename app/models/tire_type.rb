# frozen_string_literal: true

class TireType < ApplicationRecord

  def human
    "#{car} #{position} #{extras} #{size}".gsub("  ", " ").gsub("  ", " ")
  end

end
