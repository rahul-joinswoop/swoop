# frozen_string_literal: true

class CompaniesIssues < ApplicationRecord

  belongs_to :company
  belongs_to :issue

end
