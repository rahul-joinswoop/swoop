# frozen_string_literal: true

class InvoicingLedgerItem < BaseModel

  include HistoryItemTarget

  has_many :line_items,
           -> { order('id') },
           class_name: 'InvoicingLineItem',
           foreign_key: :ledger_item_id,
           autosave: true,
           inverse_of: :ledger_item

  belongs_to :sender, class_name: 'Company'
  belongs_to :recipient, polymorphic: true
  belongs_to :invoice
  # enabling this breaks a bunch of specs, i don't understand why
  # belongs_to :job
  before_save :before_save

  # TODO - these types should be constants somewhere?
  scope :only_payments_or_credits, -> { where(type: ['Payment', 'Discount', 'Refund', 'WriteOff']) }

  def before_save
    if !uuid
      self.uuid = SecureRandom.hex(12)
    end
  end

end
