# frozen_string_literal: true

class NpsLinkReview < Review

  def text_template
    "Your feedback is important to us. Please tap #{company_review_url} to take a 4 question survey on your experience. Thank you!"
  end

end
