# frozen_string_literal: true

# Mapping of a facility/location with a ISSC/RSC locatonid to a Swoop site and location.
class IsscLocation < ApplicationRecord

  has_one :issc

  # The company this location belongs to
  # This is needed for RSC as we receive the facilites (issc_locations) before we match to the sites
  # and without a site (or company) we could never match locations to sites uniquely
  belongs_to :company, class_name: "RescueCompany"

  belongs_to :site
  belongs_to :fleet_company, optional: false
  belongs_to :location

  validates_presence_of :locationid

  after_save :sync_issc_site

  private

  def sync_issc_site
    if issc && site_id && issc.site_id != site_id
      issc.update!(site_id: site_id)
    end
  end

end
