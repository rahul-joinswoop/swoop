# frozen_string_literal: true

class FleetMotorClubJob < FleetJob

  include LoggableJob

  ### ISSC status constants ### TODO move it to something similar to Agero::Rsc::Data

  # A driver has been assigned
  ISSC_DRIVERASSIGNED = 'DriverAssigned'
  # The driver is on the way to the scene
  ISSC_ENROUTE = 'Enroute'
  # The driver has arrived at the scene
  ISSC_ONSCENE = 'OnScene'
  # The vehicle is being transported or towed.
  ISSC_ENROUTETODESTINATION = 'EnrouteToDestination'
  # The transported or towed vehicle is X minutes from the destination.
  ISSC_ONSCENEDESTINATION = 'OnSceneDestination'
  # The transported or towed vehicle has been put in storage
  ISSC_VEHICLEINSTORAGE = 'VehicleInStorage'
  # The service is complete
  ISSC_SERVICECOMPLETE = 'ServiceComplete'
  # The service provider is canceling.
  ISSC_PROVIDERCANCEL = 'ProviderCancel'
  # A "Gone On Arrival" authorization is needed.
  ISSC_GOANEEDED = 'GOANeeded'

  ISSC_STATUS_MAPPING = {
    JobStatus::DISPATCHED => ISSC_DRIVERASSIGNED,
    JobStatus::EN_ROUTE => ISSC_ENROUTE,
    JobStatus::ON_SITE => ISSC_ONSCENE,
    JobStatus::TOWING => ISSC_ENROUTETODESTINATION,
    JobStatus::TOW_DESTINATION => ISSC_ONSCENEDESTINATION,
    JobStatus::STORED => ISSC_VEHICLEINSTORAGE,
    JobStatus::COMPLETED => ISSC_SERVICECOMPLETE,
    JobStatus::CANCELED => ISSC_PROVIDERCANCEL,
    JobStatus::GOA => ISSC_GOANEEDED,
  }.freeze

  ### end ISSC status constants ###

  before_create :init_review_sms

  after_commit :notify_driver_assigned, if: :saved_change_to_rescue_driver_id?

  # notifications
  after_commit do
    if saved_change_to_status? || saved_change_to_rescue_company_id?
      Notifiable.trigger_eta_requested self
    end
  end

  after_commit { Notifiable.trigger_eta_accepted self if saved_change_to_status? }

  after_commit :update_blank_po_number, if: :needs_rsc_po_number?

  aasm column: :status, enum: true, no_direct_assignment: false, whiny_persistence: true do # need direct assigment to allow undo_storage
    state :submitted
    state :etarejected
    state :expired
    state :completed

    # Partner

    event :partner_accepted, after_commit: :queue_clear_submitted do
      transitions from: :assigned, to: :submitted
      transitions from: [:dispatched, :enroute, :onsite, :towing, :towdestination], to: :accepted
    end

    event :partner_goa do
      transitions from: [:accepted, :pending, :unassigned, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :canceled], to: :goa
    end

    event :partner_canceled do
      transitions from: [:accepted, :pending, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :canceled, :goa], to: :canceled
    end

    event :partner_rejected do
      transitions from: :assigned, to: :rejected
    end

    event :partner_request_callback do
      transitions from: :assigned, to: :pending
    end

    event :partner_completed do
      transitions from: [:pending, :dispatched, :enroute, :onsite, :towing, :towdestination, :goa, :canceled], to: :completed, guard: [:fleet_manual?]
      transitions from: [:pending, :assigned, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :goa, :canceled], to: :completed
    end

    # ISSC

    event :issc_assign, after: :clear_last_touched_by do
      transitions from: [:pending, :unassigned], to: :assigned
    end

    event :issc_rejected, after: :clear_last_touched_by do
      transitions from: [:assigned, :submitted], to: :etarejected
    end

    event :issc_canceled, after: :clear_last_touched_by do
      transitions from: [:assigned, :submitted, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :goa, :expired, :canceled, :completed], to: :canceled
    end

    # PDW: we include expired here because of a bug in ISSC/Agero where by a job can get expired and then accepted.
    event :issc_accepted, after: :clear_last_touched_by do
      transitions from: [:submitted, :assigned, :expired], to: :accepted
    end

    event :issc_expired, after: :clear_last_touched_by do
      transitions from: [:created, :submitted, :assigned, :expired], to: :expired
    end
  end

  def queue_clear_submitted
    IsscCancelOverdueJobs.perform_in(6.minutes)
  end

  def clear_last_touched_by
    self.last_touched_by = nil
  end

  # The transported or towed vehicle has been left at the destination.
  # ISSC_VEHICLEATDESTINATION = 'VehicleAtDestination'.freeze # not used?

  def update_digital_dispatcher_status
    log :debug, "ISSC FleetMotorClubJob::update_digital_dispatcher_status called with #{human_status}", job: id
    # don't do anything if we don't have a digital dispatcher
    return unless digital_dispatcher

    case human_status
    when CANCELED # specific case
      DigitalDispatch.strategy_for(
        job_digital_dispatcher: digital_dispatcher, action: :cancel
      ).schedule_for(self, JobStatus::CANCELED)
    when GOA # specific case
      DigitalDispatch.strategy_for(
        job_digital_dispatcher: digital_dispatcher, action: :cancel
      ).schedule_for(self, JobStatus::GOA)
    when REJECTED # specific case
      DigitalDispatch.strategy_for(
        job_digital_dispatcher: digital_dispatcher, action: :reject
      ).perform_async(id)
    when ACCEPTED # specific case
      # note: flow to Accept a Job is kicked by API::Jobs::UpdateByRescueCompany
      log :debug, "status_changed - new state accepted", job: id
    when SUBMITTED # specific case
      log :debug, "status_changed - new state submitted", job: id
      DigitalDispatch.strategy_for(
        job_digital_dispatcher: digital_dispatcher, action: :accept
      ).perform_async(id)
    else # all other cases are considered common status change
      DigitalDispatch.strategy_for(
        job_digital_dispatcher: digital_dispatcher, action: :change_status
      ).schedule_for(self, JobStatus::NAME_MAP[human_status.to_sym])
    end
  end

  def acknowledge_by
    wiggle_room = 30.seconds
    required = answer_by
    if required
      required - wiggle_room
    else
      Time.now + 90.seconds - wiggle_room
    end
  end

  def issc_dispatch_status(status, now)
    IsscDispatchStatus.perform_async(id, status, :Manual, now, now)
  end

  def invoice_editable?
    true
  end

  # Informs the Digital Dispatcher from where this Job was created.
  #
  # Returns nil in case there's no issc_dispatch_request associated (will likely not happen).
  #
  # Defaults to Issc::ISSC_SYSTEM in case issc_dispatch_request.issc.system is nil.
  def digital_dispatcher
    return nil unless issc_dispatch_request

    issc_dispatch_request.issc.system&.to_sym || Issc::ISSC_SYSTEM
  end

  private

  def notify_driver_assigned
    if rescue_driver_id.present?
      strategy = DigitalDispatch.strategy_for(job_digital_dispatcher: digital_dispatcher, action: :assign_driver)
      strategy.perform_async(id) if strategy
    end
  end

  def update_blank_po_number
    Agero::Rsc::DispatchJobPoNumberUpdateWorker.perform_async(id) if needs_rsc_po_number?
  end

  def needs_rsc_po_number?
    rsc? && po_number.blank? && issc_dispatch_request&.dispatchid? && !rsc_po_number_feature_blocked?
  end

  # Kill switch in case we see unintended issues in production
  # To turn this off run this on prod console:
  # Flipper.enable(:block_rsc_job_populate_po_number)
  def rsc_po_number_feature_blocked?
    Flipper[:block_rsc_job_populate_po_number].enabled?
  end

  # The service provider accepted the dispatch.
  # PDW: Don't think this is needed because done in issc_dispatch_response
  ISSC_DISPATCHACCEPTED = 'DispatchAccepted'

  # The motor club's cancel has been received.
  ISSC_MOTORCLUBCANCELRECEIVED = 'MotorClubCancelReceived'

  # Additional service is requested
  ISSC_ADDITIONALSERVICE = 'AdditionalService'

  # The service failed.
  ISSC_SERVICEFAILED = 'ServiceFailed'

  # The transported or towed vehicle is X minutes from the destination.
  ISSC_MINUTESTODESTINATION = 'MinutesToDestination'

  # Driver location
  ISSC_GPS = 'GPS'

  # Ignore, per Kip

  # The service has been delayed.
  ISSC_SERVICEDELAY = 'ServiceDelay'

  # The driver is X minutes away from incident
  ISSC_MINUTESAWAY = 'MinutesAway'

  # The original ETA will not be met
  ISSC_EXTENDEDETA = 'ExtendedETA'

end
