# frozen_string_literal: true

class ParentJobHistoryItem

  attr_reader :history_object

  delegate :id, :last_set_dttm, :adjusted_dttm, :type, :ui_label, :ui_type, to: :history_object

  def initialize(history_object)
    @history_object = history_object
  end

  def name
    "*" + history_object.name.to_s
  end

end
