# frozen_string_literal: true

class ReportResult < BaseModel

  has_secure_token
  belongs_to :report
  belongs_to :company
  belongs_to :user
  before_create :set_initial_state

  CREATED = "Created"
  RUNNING = "Running"
  FINISHED = "Finished"
  FAILED = "Failed"

  def set_initial_state
    self.state = CREATED
  end

  def fields
    YAML.load(serialized_fields) if serialized_fields.present?
  end

  def values
    YAML.load(serialized_values) if serialized_values.present?
  end

  def s3_object
    @s3_object ||= S3.bucket(Configuration.s3.report_bucket).object("#{company.name}/#{report.name}.#{id}.#{format}")
  end

  def run_query(cleaned_params)
    log "REPORT(#{id}) ReportResult#run_query cleaned_params #{cleaned_params}"
    sql = apply_filters(report.sql, cleaned_params)
    log "REPORT(#{id}) ReportResult#run_query applied filters: #{sql}"

    params = merge_params(cleaned_params)
    log "REPORT(#{id}) ReportResult#run_query merged params: #{params}"

    sql_params = order_params(params)
    log "REPORT(#{id}) ReportResult#run_query ordered params: #{sql_params}"

    self.executed_sql = self.class.sanitize_sql([sql] + sql_params)
    save!

    log "REPORT(#{id}) ReportResult#run_query Executing"

    ReportingDatabaseBase.connection.execute(executed_sql)
  end

  def execute_synchronously!(submitted_params = nil, after_callback = nil)
    generate_report!(submitted_params, after_callback)
  end

  # CompanyReport.find(2).run(User.find(1),{"rescue_company"=>2})
  def execute_asynchronously!(submitted_params = nil, after_callback = nil)
    generate_report!(submitted_params, after_callback)
  end
  handle_asynchronously :execute_asynchronously!

  # DONE - 1 replace with aws-sdk v3
  def process_csv(results, after_callback)
    cnt = 0
    values = []
    written_headers = false

    csv = String.new ''
    results.each do |row|
      new_row = after_callback ? send(after_callback, row) : row

      if !written_headers
        # s3_stream << new_row.keys.to_csv
        csv << new_row.keys.to_csv
        written_headers = true
      end

      # s3_stream << new_row.values.to_csv
      csv << new_row.values.to_csv
      if cnt < 100
        values << new_row.values
        cnt += 1
      end
    end
    s3_object.put(body: csv, content_type: 'text/csv', content_disposition: 'attachment')

    self.serialized_values = YAML.dump values
    self.serialized_fields = YAML.dump results.fields
    self.s3_filename = s3_object.presigned_url(:get, expires_in: S3_SIGNED_URL_EXPIRATION)

    self.state = FINISHED

    if @process_method
      # If the process state fails mark this as a failed report
      self.state = FAILED if !send(@process_method)
    end
  end

  # DONE - 2 replace with aws-sdk v3
  def process_html(results, after_callback)
    log "REPORT(#{id}) processing html for #{report.name}"

    # TODO - this is dangerous because it assumes only reports with valid
    # names (a report name that matches one of our HTML templates) will be
    # hitting this method
    s3_object.put(body: render_to_string(results), content_type: 'text/html', content_disposition: 'attachment')

    self.s3_filename = s3_object.presigned_url(:get, expires_in: S3_SIGNED_URL_EXPIRATION)
    self.state = FINISHED
  end

  # DONE 3 - replace with aws-sdk v3
  def process_pdf(results, after_callback)
    # TODO - footer is not used.
    # footer = API::V1::ReportsBaseController.new.render_to_string(partial: 'reports/footer')

    pdf = WickedPdf.new.pdf_from_string(render_to_string(results), {
      orientation: 'Landscape',
    })

    s3_object.put(body: pdf, content_type: 'application/pdf', content_disposition: 'attachment')

    self.s3_filename = s3_object.presigned_url(:get, expires_in: S3_SIGNED_URL_EXPIRATION)
    self.state = FINISHED
  end

  private

  def generate_report!(submitted_params = nil, after_callback = nil)
    Rails.logger.debug("REPORT(#{id}) ReportResult#generate_report submitted_params #{submitted_params}")
    start_time = Time.now
    # each report declares which params are valid for it so we pull those in here
    # and permit them through from submitted_params
    available_params = report.available_params || {}

    # ... also, some filters come mapped as available_params, but are changed on the controller
    # to a specific key that matches some of the report.filter_sql, so we merge it with
    # available_params (specifically reports_base_controller#process_params_for_statement)
    available_params = available_params.merge((report.filter_sql || {}))

    # convert nil/plain old hash into strong parameters
    submitted_params = ActionController::Parameters.new(submitted_params || {}) unless submitted_params.is_a?(ActionController::Parameters)

    cleaned_params = submitted_params.permit(available_params.keys.map(&:to_sym)).to_h

    begin
      self.format ||= 'csv'
      self.state = RUNNING
      self.optional_params = cleaned_params
      save! # Send WS

      meth = case format
             when 'pdf'
               :process_pdf
             when 'html'
               :process_html
             else
               :process_csv
             end
      results = run_query(cleaned_params)
      self.empty = !results.first

      send(meth, results, after_callback)
    rescue => e
      log "#{e}\n", :error
      log "#{e.backtrace.join("\n")}", :error

      self.serialized_fields = YAML.dump ["error_message"]
      self.serialized_values = YAML.dump [[e.message]]
      self.state = FAILED
    end

    self.executed_in = Time.now - start_time

    log "REPORT(#{id}) ReportResult#run_query finished (#{state}) executed in #{executed_in}"

    save!
  end

  def render_to_string(results)
    template_name = report.name.gsub(" ", "_").underscore
    API::V1::ReportsBaseController.new.render_to_string(
      template: "reports/#{template_name}",
      layout: false,
      locals: {
        company: company,
        collate: collated,
        user: user,
        sql_results: results,
        report: report,
        filters: optional_params,
      }
    )
  end

  # filter_sql example: {"rescue_company":"and rescue_company_id=?"}
  def apply_filters(sql, cleaned_params)
    Rails.logger.debug "REPORT(#{id}) cleaned_params:#{cleaned_params}"
    if report.filter_sql
      report.filter_sql.each do |k, v|
        Rails.logger.debug "REPORT(#{id}) Dealing with filter sql #{k}"
        key = "{{#{k}}}"
        if cleaned_params.key?(k)
          Rails.logger.debug "REPORT(#{id}) looking for filter key #{key}"
          sql[key] = v
        else
          sql[key] = ""
        end
      end
    end

    sql
  end

  def get_mandatory_params
    mandatory_map = { 'api_company.id' => company_id, 'api_user.id' => user_id }
    ret = {}
    if report.mandatory_params
      report.mandatory_params.each do |k, v|
        raise "Template value not found in mandatory map #{v}" unless mandatory_map[v]
        ret[k] = mandatory_map[v]
      end
    end
    ret
  end

  def merge_params(cleaned_params)
    params = {}
    params.merge!(report.default_params) if report.default_params
    params.merge!(cleaned_params) if cleaned_params
    params.merge!(get_mandatory_params)
    params
  end

  def order_params(params)
    ret = []
    if report.all_params
      report.all_params.each do |param|
        if params.key?(param)
          ret << params[param]
        end
      end
    end
    ret
  end

  def render_to_hash
    controller = ActionController::Base.new
    JSON.parse(controller.render_to_string('api/v1/report_results/_result', locals: { report_result: self }))
  end

  def listeners
    ret = Set.new
    ret << company
    ret
  end

  def process_invoice_state_partner(row)
    @to_state       ||= :swoop_downloaded_partner
    @process_method ||= :process_invoice_states

    add_record_for_state_change(row["Invoice Id"])
    row
  end

  def process_invoice_state_to_downloaded(row)
    Rails.logger.debug "INVOICING::process_invoice_state_to_downloaded: #{row.inspect}"
    @to_state       ||= :fleet_downloaded
    @process_method ||= :process_invoice_states
    add_record_for_state_change(row["Invoice Id"])
    row.except("Invoice Id")
  end

  # Ok so here is the logic here
  # this will process all the invoices in a single batch at the end
  # however it wraps then in a transaction, so if we fail here we can roll them back
  def process_invoice_states
    # IF we dont have a state to traverse to or records to set
    # then we should just return true, as i doubt there are any records
    # in our report.
    return true if !@to_state || !@records_for_state
    Rails.logger.debug "INVOICING: moving records to fleet_downloaded #{@records_for_state}"
    records = Invoice.where(id: @records_for_state)
    records.update_all(state: @to_state, updated_at: Time.now.to_s(:db))
    records.each(&:job_reindex)
    true
  end

  def generate_csv_header(row)
    keys = row.keys
    vehicle_idx = keys.index("Vehicle")
    if company.rescue? && vehicle_idx
      keys[vehicle_idx] = "Truck"
    end
    keys.to_csv
  end

  # Just a helper to allow us to move the records in and out
  def add_record_for_state_change(rec)
    @records_for_state ||= []
    @records_for_state << rec
  end

  def log(message, meth = :info)
    logger.send(meth, "REPORT(#{id}) #{message}")
  end

end
