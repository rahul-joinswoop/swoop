# frozen_string_literal: true

class TeslaRate < Rate

  validates :hourly, presence: true
  validates :miles_p2p, presence: true

  def mail_attributes
    base = super

    base.merge({
      PORT_TO_PORT_HOURS => hourly,
      MILES_PORT_TO_PORT => miles_p2p,
    })
  end

  def line_item_descriptions
    [TAX, PORT_TO_PORT_HOURS, MILES_PORT_TO_PORT, KILOMETERS_PORT_TO_PORT]
  end

  def calculate_line_items(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    lis = []
    lis << base_hours(invoicable)
    lis.concat(base_p2p(invoicable))
    add_common_items(invoicable, lis)
    lis
  end

end
