# frozen_string_literal: true

module Agero::ApiRequest
  # Base class for Agero API requests. Essentially an interface to define
  # outgoing requests.
  class Base

    BASE_URL =         Configuration.agero_rsa_api.url
    API_KEY =          Configuration.agero_rsa_api.api_key
    SECRET_KEY =       Configuration.agero_rsa_api.api_secret
    CALLBACK_URL =     Configuration.agero_rsa_api.callback_url
    KEY_AND_SECRET =   "#{API_KEY}:#{SECRET_KEY}"

    def initialize(*args)
    end

    def url
      raise NotImplementedError
    end

    def headers
      {}
    end

    def params
      {}
    end

    def self.get(*args)
      request = new(*args)
      params = { params: request.params }.merge(request.headers)
      JSON.parse RestClient.get(request.url, params)
    end

  end
end
