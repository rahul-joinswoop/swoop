# frozen_string_literal: true

module Agero::ApiRequest
  # Generates an authorization code from the Agero API in order to generate
  # an access token in a subsequent request.
  class AuthorizationCode < Agero::ApiRequest::Base

    def url
      "#{BASE_URL}/oauth/authorizationcode"
    end

    def headers
      {
        Authorization: API_KEY,
      }.freeze
    end

    def params
      {
        "serviceProviderId": "NA",
        "callbackNumber": "TODO",
        "testCallbackURL": CALLBACK_URL,
        "requestUri": "ageroapi/authcode",
        "echoUUID": "TODO",
        "response_type": "code",
        "scope": "READ",
        "client_id": API_KEY,
      }.freeze
    end

  end
end
