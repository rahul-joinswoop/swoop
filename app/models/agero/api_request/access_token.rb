# frozen_string_literal: true

module Agero::ApiRequest
  # Generates an access token from the Agero API to be used in RSA and billing
  # requests
  class AccessToken < Agero::ApiRequest::Base

    PATH = "/oauth/accesstoken"

    def initialize(auth_code)
      @auth_code = auth_code
      super
    end

    def url
      "#{BASE_URL}#{PATH}"
    end

    def params
      {
        "grant_type": "authorization_code",
        "code": @auth_code,
      }.freeze
    end

    def headers
      {
        Authorization: Base64.strict_encode64(KEY_AND_SECRET),
      }.freeze
    end

  end
end
