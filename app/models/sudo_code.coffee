# On Load
objs = grabObjs()

for obj in objs
  saveObj(false, obj)


saveObj(local, job) ->
  dbJob = getFromDB(job)
  if existsInDb(job)
    if local
      changes = diff(dbJob, job)
      job.changes = merge(changes, dbJob.changes)
      job.save()
      sendToServer()
    else
      for change in dbJob.changes
        job[change] = dbJob[change]
      job.save
  else
    job.save()
    if local
      sendToServer()


sendToServer() ->
  objs = getAllObjsWithChanges()
  for obj in objs
    changes = obj.changes
    hash = {id: obj.id}
    for change in changes
      hash[change] = obj[change]
      response = uploadToServer(hash)
      saveObj(false, response)




