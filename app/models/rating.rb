# frozen_string_literal: true

#
# Represents the current rating for eash service provider for each client
# Rating can be NPS, 5star or other depending on the client's rating mechanism
#
#
# delete from ratings;
#
# insert into ratings(rescue_company_id,client_company_id,num_reviews,num_jobs,score,updated_at,created_at)
# select
#  id as rescue_company_id,
#  97 as client_company_id,
#  round(random()*20) as num_reviews,
#  round(random()*80) as num_jobs,
#  round((random()*100 - random()*25)::numeric,1) as score,
#  now() as updated_at,
#  now() as created_at
# from companies where type='RescueCompany';
#

class Rating < ApplicationRecord

  belongs_to :rescue_company
  belongs_to :client_company, class_name: 'Company'

end
