# frozen_string_literal: true

module Auction
  class Bid < BaseModel

    include GraphQLSubscribers
    scope :new_or_requested, -> { where(status: [NEW, REQUESTED]) }

    validates :company, presence: true

    after_commit :publish_auction, on: :update
    after_commit do
      Subscribable.trigger_job_updated job
    end
    after_commit { Subscribable.trigger_auction_bid_status_changed(self) if saved_change_to_status? }
    after_commit { Notifiable.trigger_eta_requested(self) if saved_change_to_status? }
    after_commit { Notifiable.trigger_eta_accepted(self) if saved_change_to_status? }

    # TODO - shouldn't we be using statemachine here?
    # Original state
    NEW = 'new'

    # bid created but partner has not interacted with it; auction is LIVE
    REQUESTED = 'requested'

    # partner rejected the job
    PROVIDER_REJECTED = 'provider_rejected'

    # A bid was submitted and awaiting auction end
    SUBMITTED = 'submitted'

    # partner is the successful bidder!
    WON = 'won'

    # the bid was received but they lost the auction
    AUTO_REJECTED = 'auto_rejected'

    # the auction ended before the partner submitted the bid
    EXPIRED = 'expired'

    # The job was manually assigned to a partner by swoop and the auction was canceled
    CANCELED = 'canceled'

    NAME_TO_STATUS_MAP = {
      'New' => NEW,
      'Requested' => REQUESTED,
      'Provider Rejected' => PROVIDER_REJECTED,
      'Submitted' => SUBMITTED,
      'Won' => WON,
      'Auto Rejected' => AUTO_REJECTED,
      'Expired' => EXPIRED,
      'Canceled' => CANCELED,
    }.freeze

    STATUS_TO_NAME_MAP = NAME_TO_STATUS_MAP.invert.freeze

    # these are used with graphql - we can't use spaces there in our enum values
    ENUM_NAME_TO_STATUS_MAP = NAME_TO_STATUS_MAP.transform_keys { |k| k.gsub(/\s+/, '') }.freeze
    STATUS_TO_ENUM_NAME_MAP = ENUM_NAME_TO_STATUS_MAP.invert.freeze

    NAME_TO_DESCRIPTION_MAP = {
      'New' => 'Initial State',
      'Requested' => 'Bid created but partner has not interacted with it; auction is LIVE',
      'Provider Rejected' => 'Partner rejected the job',
      'Submitted' => 'A bid was submitted and awaiting auction end',
      'Won' => 'Partner is the successful bidder',
      'Auto Rejected' => 'The bid was received but the partner lost the auction',
      'Expired' => 'The auction ended before the partner submitted the bid',
      'Canceled' => 'The job was manually assigned to a partner by Swoop and the auction was canceled',
    }.freeze

    belongs_to :candidate_assignment, class_name: 'Job::Candidate::Assignment'
    belongs_to :auction, class_name: 'Auction::Auction'
    belongs_to :job
    belongs_to :company
    belongs_to :job_reject_reason
    belongs_to :candidate, class_name: 'Job::Candidate'

    # Which bids are won, auto-rejected, expired, or cancelled for a job? --
    # a surrogate for the auction having ended, about to be ended, or
    # should be ended
    scope :indicating_auction_ended, ->(job) { where(job: job).where(status: [Bid::WON, Bid::CANCELED, Bid::EXPIRED, Bid::AUTO_REJECTED]) }

    # Which bids (i.e., bidders) are considered "silent" when the auctioneer says:
    # "Going once... going twice..." for a job?
    scope :considered_silent_at_going_once_going_twice, ->(job) { where(job: job).where(status: [Bid::NEW, Bid::SUBMITTED, Bid::PROVIDER_REJECTED]) }

    def notifiable?
      auction.isLive? && [NEW, REQUESTED].include?(status) && !auction.ending_soon?
    end

    def subscribers
      # we only send direct updates about _this_ bid to our bid company - anyone else who wants to
      # get notified is doing so because they care about the job and they're subscribed at that level
      # already
      ret = Set.new([company])

      # and go through our subscribers looking for RescueCompanies - if we
      # find one add all the dispatchers from the company to our list of
      # subscribers
      ret = add_dispatchers ret

      # go through our subscribers looking for Companies and add any oauth_applications
      # they may have to our subscribers
      ret = add_applications ret

      ret
    end

    def listeners
      listener_companies = Set.new
      if auction
        auction.bids.each do |bid|
          listener_companies << bid.company
        end
      end

      listener_companies << Company.swoop

      listener_companies
    end

    def render_to_hash
      BidSerializer.new(self).serializable_hash
    end

    def job_ssid
      SomewhatSecureID.encode(job, self)
    end

    private

    def publish_auction
      auction.base_publish('update')
    end

  end
end
