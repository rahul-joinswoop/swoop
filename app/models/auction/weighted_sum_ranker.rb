# frozen_string_literal: true

require "#{Rails.root}/lib/numeric_extensions"

module Auction
  class WeightedSumRanker < Ranker

    include ActiveSupport::NumberHelper

    BID_ALGORITHM = ::Auction::Rankers::BidWeightedSum
    CANDIDATE_ALGORITHM = ::Auction::Rankers::CandidateWeightedSum

    belongs_to :client, class_name: "Company"

    before_create :set_version

    validate :validate_args

    def calculate_set_candidate_scores(job_id)
      CANDIDATE_ALGORITHM.new(job_id, version, constraints, weights).call
    end

    def calculate_and_set_bid_scores(auction_id)
      BID_ALGORITHM.new(auction_id, version, constraints, weights).call
    end

    private

    # If you need to update version, you'll need to update CandidateWeightedSum and BidWeightedSum as well
    VERSION = 2

    def set_version
      self.version = VERSION
    end

    def zero_to_one(a)
      a >= 0.0 && a <= 1.0
    end

    def validate_args
      if !(weights["cost"] && weights["speed"] && weights["quality"])
        errors.add(:args, "Must include speed, quality, and cost")
      elsif weights["cost"].decimals > 2 || weights["speed"].decimals > 2 || weights["quality"].decimals > 2
        errors.add(:weights, "Cost + Speed + Quality must add up to 1.0") unless Float(number_to_rounded(weights["cost"] + weights["speed"] + weights["quality"], precision: 1)).round == 1
      elsif weights["cost"] == 0.1 || weights["speed"] == 0.1 || weights["quality"] == 0.1
        if weights["cost"] > 0 && weights["speed"] > 0 && weights["quality"] > 0
          errors.add(:weights, "Cost + Speed + Quality must add up to 1.0") unless Float(number_to_rounded(weights["cost"] + weights["speed"] + weights["quality"], precision: 1)).round == 1
        end
      else
        errors.add(:weights, "Speed must be a float") unless weights["speed"].is_a?(Float)
        errors.add(:weights, "Quality must be a float") unless weights["quality"].is_a?(Float)
        errors.add(:weights, "Cost must be a float") unless weights["cost"].is_a?(Float)
        errors.add(:weights, "Cost weight must be between 0.0 and 1.0") unless zero_to_one(weights["cost"])
        errors.add(:weights, "Speed weight must be between 0.0 and 1.0") unless zero_to_one(weights["speed"])
        errors.add(:weights, "Quality weight must be between 0.0 and 1.0") unless zero_to_one(weights["quality"])
        errors.add(:weights, "Cost + Speed + Quality must add up to 1.0") unless weights["cost"] + weights["speed"] + weights["quality"] == 1
      end
      if !(constraints["max_eta"] && constraints["max_cost"] && constraints["min_rating"])
        errors.add(:args, "Must include max_eta, max_cost, and min_rating")
      else
        errors.add(:constraints, "max_eta must be an integer") unless constraints["max_eta"].is_a?(Integer)
        errors.add(:constraints, "max_cost must be an integer") unless constraints["max_cost"].is_a?(Integer)
        errors.add(:constraints, "min_rating must be an integer") unless constraints["min_rating"].is_a?(Integer)
      end
    end

  end
end
