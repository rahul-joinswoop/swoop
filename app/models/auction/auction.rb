# frozen_string_literal: true

module Auction

  class CannotBuildAuctionWithoutJobError < ArgumentError; end
  class CannotBuildAuctionWithoutFleetCompanyForJobError < ArgumentError; end

  class Auction < BaseModel

    belongs_to :job
    belongs_to :service_code
    has_many :bids
    has_many :rescue_companies, through: :bids, source: :company
    belongs_to :ranker, class_name: 'Auction::Ranker'

    after_commit do
      Subscribable.trigger_job_updated job
    end

    # TODO - shouldn't we be using statemachine here?
    NEW = 'new'
    LIVE = 'live' # The auction is in progress
    SUCCESSFUL = 'successful' # The auction has finished and a winner was found
    UNSUCCESSFUL = 'unsuccessful' # The auction has finished and no winner was found
    CANCELED = 'canceled'

    DISPATCHABLE_SERVICE_NAMES = [
      ServiceCode::ACCIDENT_TOW,
      ServiceCode::BATTERY_JUMP,
      ServiceCode::FUEL_DELIVERY,
      ServiceCode::LOCK_OUT,
      ServiceCode::TIRE_CHANGE,
      ServiceCode::TOW,
      ServiceCode::TOW_IF_JUMP_FAILS,
      ServiceCode::TOW_IF_TIRE_CHANGE_FAILS,
      ServiceCode::WINCH_OUT,
    ].freeze

    UNAUCTIONABLE_CLASS_TYPE_NAMES = [
      VehicleCategory::HEAVY_DUTY,
    ].freeze

    NAME_TO_STATUS_MAP = {
      'New' => NEW,
      'Live' => LIVE,
      'Succesful' => SUCCESSFUL,
      'Unsuccessful' => UNSUCCESSFUL,
      'Canceled' => CANCELED,
    }.freeze

    STATUS_TO_NAME_MAP = NAME_TO_STATUS_MAP.invert

    #
    # Build a new Auction for a Fleet-Managed Job
    #
    def self.build_for_job(job:)
      raise CannotBuildAuctionWithoutJobError unless job.is_a?(Job)
      raise CannotBuildAuctionWithoutFleetCompanyForJobError unless job.fleet_company

      auction_length_in_seconds = get_duration_secs(job)

      Auction.new(
        job: job,
        status: :new,
        duration_secs: auction_length_in_seconds,
        expires_at: Time.now + auction_length_in_seconds.seconds,
        max_bidders: Setting.get_integer(job.fleet_company, Setting::AUCTION_MAX_BIDDERS) || default_max_bidders,
        max_candidate_distance: Setting.get_integer(job.fleet_company, Setting::AUCTION_MAX_DISTANCE) || default_max_candidate_distance,
        max_candidate_eta: job.fleet_company.max_candidate_eta,
        min_target_eta: Setting.get_integer(job.fleet_company, 'Auction Min Target ETA') || default_min_target_eta,
        max_target_eta: Setting.get_integer(job.fleet_company, 'Auction Max Target ETA') || default_max_target_eta,
        ranker: get_or_create_ranker(job),
        service_code: job.service_code
      )
    end

    #
    # Get or create the ETA+Cost+NPS ranker we'll use to rank candidates for this auction + job
    #
    # This method is gated by whether the job is priority response. We'll either:
    # 1) Force a priority response ranker (speed: 0.98, quality: 0.01, cost: 0.01), for any priority response job
    # 2) Decide which ranker to apply via Split, for any non-priority response job, we'll either:
    #    2.1) If split test is "on":
    #         Create a 100%-cost-based ranker (speed: 0.0, quality: 0.0, cost: 1.0)
    #         (defaults to "off"; as of Jan 18, 2019, 0% of jobs are receiving this treatment)
    #    2.2) If split treatment is "off":
    #         Use the fleet company's normal ranker
    #         (as of Jan 18, 2019, 100% of jobs are receiving this treatment)
    #
    def self.get_or_create_ranker(job)
      # Step 1: If this is a priority response job, force a ranker w/speed: 0.98, quality: 0.01, cost: 0.01
      if job.priority_response
        # Step 1.1: Try to find a priority response ranker that already exists for this fleet
        priority_response_ranker_name = "Priority response"
        priority_response_ranker = ::Auction::WeightedSumRanker.find_by(client: job.fleet_company, name: priority_response_ranker_name)
        # Step 1.2: If we don't have a priority response ranker for this fleet, make one based
        # off the normal ranker we'd be using if this wasn't a priority response job
        if !priority_response_ranker
          normal_ranker_for_fleet = job.fleet_company.get_or_create_ranker
          priority_response_ranker = ::Auction::WeightedSumRanker.create!(
            name: priority_response_ranker_name,
            weights: { speed: 0.98, quality: 0.01, cost: 0.01 },
            constraints: {
              min_eta: normal_ranker_for_fleet.constraints['min_eta'],
              max_eta: normal_ranker_for_fleet.constraints['max_eta'],
              min_cost: normal_ranker_for_fleet.constraints['min_cost'],
              max_cost: normal_ranker_for_fleet.constraints['max_cost'],
              min_rating: normal_ranker_for_fleet.constraints['min_rating'],
              max_rating: normal_ranker_for_fleet.constraints['max_rating'],
            },
            live: false,
            client: normal_ranker_for_fleet.client
          )
        end
        # Step 1.3: Return the priority response ranker we either found or created
        priority_response_ranker

      # Step 2: Otherwise, this is not a priority response job, and Split will decide which ranker
      # to use
      else
        # Step 2.0: Prepare a little hash about the job that we can send to Split.io in Step 2.1
        attributes_for_job_on_split = {
          client: job.fleet_company&.name,
          city: job.service_location&.city,
          state: job.service_location&.state,
          zip: job.service_location&.zip,
        }
        # Step 2.1: If Split wants to test a 100%-cost-based ranker, let's...
        if External::Split.treatment(job, 'job_claim_cost', "off", attributes_for_job_on_split) == "on"
          # Step 2.1.1: Try to find a 100%-cost-based ranker that already exists for this fleet
          hundred_pc_cost_based_ranker_name = "Fully cost weighted"
          hundred_pc_cost_based_ranker = ::Auction::WeightedSumRanker.find_by(client: job.fleet_company, name: hundred_pc_cost_based_ranker_name)
          # Step 2.1.2: If we don't have a 100%-cost-based ranker for this fleet, make one
          if !hundred_pc_cost_based_ranker
            hundred_pc_cost_based_ranker = ::Auction::WeightedSumRanker.create!(
              name: hundred_pc_cost_based_ranker_name,
              weights: { speed: 0.0, quality: 0.0, cost: 1.0 },
              constraints: {
                min_eta: 5,
                max_eta: job.fleet_company.max_candidate_eta,
                min_cost: 20,
                max_cost: 150,
                min_rating: 40,
                max_rating: 100,
              },
              live: false,
              client: job.fleet_company
            )
          end
          # Step 2.1.3: Return the 100%-cost-based ranker we either found or created
          return hundred_pc_cost_based_ranker
        # Step 2.2: Otherwise, Split doesn't want to test a 100%-cost-based ranker, and so we let
        # the fleet company use its normal ranker
        else
          return job.fleet_company.get_or_create_ranker
        end
      end
    end

    def self.get_duration_secs(job)
      Setting.get_integer(job.fleet_company, 'Auction Duration Seconds') || default_duration_secs
    end

    def self.default_duration_secs
      Integer(ENV['AUCTION_DURATION_SECONDS_DEFAULT'] || 120)
    end

    def self.default_max_bidders
      Integer(ENV['AUCTION_MAX_BIDDERS_DEFAULT'] || 5)
    end

    def self.default_max_candidate_distance
      Integer(ENV['AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT'] || 30)
    end

    def self.default_max_candidate_eta
      Integer(ENV['AUCTION_MAX_CANDIDATE_ETA_DEFAULT'] || 60)
    end

    def self.default_min_target_eta
      Integer(ENV['AUCTION_MIN_TARGET_ETA_DEFAULT'] || 15)
    end

    def self.default_max_target_eta
      Integer(ENV['AUCTION_MAX_TARGET_ETA_DEFAULT'] || 40)
    end

    # Send notifications to slack prefixed with AutoAssign Job(JobID)
    def self.slack_notification(job, *args)
      tags = ['dispatch', 'auto-assign']
      tags.unshift(job.fleet_company&.name)
      msg = args.join("\n\t")
      SlackChannel.message("*Auction JOB(#{job.id}: #{job.service_location.address})*\n\t#{msg}", tags: tags, add_default: false)
    end

    def expires_in
      [(expires_at - Time.zone.now), 0].max
    end

    def ending_soon?
      expires_in <= 30
    end

    def isLive?
      status == LIVE
    end

    def listeners
      ret = Set.new
      bids.each do |bid|
        ret << bid.company
      end

      ret << Company.swoop

      ret
    end

    def render_to_hash
      AuctionSerializer.new(self, scope: @api_company).serializable_hash
    end

  end

end
