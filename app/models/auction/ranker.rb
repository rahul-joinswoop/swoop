# frozen_string_literal: true

module Auction
  class Ranker < ApplicationRecord

    belongs_to :client, class_name: 'Auction::Ranker', foreign_key: :client_id

  end
end
