# frozen_string_literal: true

class AuditEmail < ApplicationRecord

  belongs_to :job
  belongs_to :user
  belongs_to :company
  belongs_to :target, polymorphic: true

end
