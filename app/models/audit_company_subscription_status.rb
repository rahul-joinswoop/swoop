# frozen_string_literal: true

class AuditCompanySubscriptionStatus < BaseModel

  belongs_to :company
  belongs_to :subscription_status

end
