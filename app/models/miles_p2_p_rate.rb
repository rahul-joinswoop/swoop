# frozen_string_literal: true

class MilesP2PRate < Rate

  validates :miles_enroute, presence: true
  validates :miles_enroute_free, presence: true
  validates :miles_towed, presence: true
  validates :miles_towed_free, presence: true
  validates :miles_deadhead, presence: true
  validates :miles_deadhead_free, presence: true

  def mail_attributes
    base = super

    base.merge({
      HOOKUP => hookup,
      EN_ROUTE_MILES => miles_enroute,
      FREE_EN_ROUTE_MILES => miles_enroute_free,
      MILES_TOWED => miles_towed,
      FREE_MILES_TOWED => miles_towed_free,
      MILES_DEADHEAD => miles_deadhead,
      FREE_MILES_DEADHEAD => miles_deadhead_free,
    })
  end

  def line_item_descriptions
    [
      TAX,
      HOOKUP,
      EN_ROUTE_MILES,
      EN_ROUTE_KILOMETERS,
      FREE_EN_ROUTE_MILES,
      FREE_EN_ROUTE_KILOMETERS,
      MILES_TOWED,
      KILOMETERS_TOWED,
      FREE_MILES_TOWED,
      FREE_KILOMETERS_TOWED,
      MILES_DEADHEAD,
      KILOMETERS_DEADHEAD,
      FREE_MILES_DEADHEAD,
      FREE_KILOMETERS_DEADHEAD,
    ]
  end

  def calculate_line_items(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    lis = []
    lis << base_hookup
    lis.concat(base_en_route(invoicable))
    lis.concat(base_tow(invoicable))
    lis.concat(base_deadhead(invoicable))
    add_common_items(invoicable, lis)
    adjust_mileage_line_items(invoicable, lis)
    lis
  end

end
