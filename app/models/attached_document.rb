# frozen_string_literal: true

class AttachedDocument < BaseModel

  include FileUploader[:document]

  alias_method :url, :document_url
  delegate :original_filename, to: :document, allow_nil: true
  delegate :attached?, to: :document_attacher

  belongs_to :job
  belongs_to :company
  belongs_to :user
  belongs_to :location

  validates :user, presence: true

  validate :job_xor_company

  accepts_nested_attributes_for :location

  after_commit { Subscribable.trigger_job_document_changed self }

  def url
    document_url(public: true)
  end

  def listeners
    if job.present?
      job.listeners
    elsif company.present?
      company.listeners
    end
  end

  def render_to_hash
    controller = ActionController::Base.new
    opts = { locals: { attached_document: self } }
    template_path = "api/v1/attached_documents/_attached_document"
    json_str = controller.render_to_string template_path, opts
    JSON.parse(json_str)
  end

  def finalize_document
    document_attacher.finalize if document_attacher.attached?
  end

  private

  def job_xor_company
    unless job.blank? ^ company.blank?
      errors.add(:base_association, "Specify a job or a company, not both")
    end
  end

end
