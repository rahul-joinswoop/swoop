# frozen_string_literal: true

#
# Represents a job status change to be exposed in the UI, and for audit purposes.
#
# Job status changes can happen in the web UI, mobile app, through Digital Dispatch integrations (RSC or ISSC)
# or by Client and Partner APIs (GraphQL)
#
class AuditJobStatus < ApplicationRecord

  include SoftDelete
  include JobHistoryLabelConcern

  belongs_to :job_status
  belongs_to :job, inverse_of: :audit_job_statuses
  belongs_to :company
  belongs_to :user
  belongs_to :source_application

  validates :job, presence: true

  # Will add DB constraint in later release >3.12.x (can't  add here because of order of operations during release)
  validates :job_id, uniqueness: { scope: :job_status_id }

  after_save :job_set_adjusted_created_at

  # PDW not recalcaulting the invoice on update of times
  after_commit :recalc_invoice, on: :create

  # Ensure that the job index is up to date with any changes made to the audit history.
  after_commit { job.reindex if job }
  after_commit :track_metrics

  scope :for_live_status, ->(status_name) { where(job_status: JobStatus.for_name(status_name), deleted_at: nil) }

  UI_TYPE_HISTORY = 'History'

  class << self

    # Dynamically creates a mark_paid audit status for job, based on:
    # =>  invoice.paid attribute; and
    # => invoice.mark_paid_at attribute
    #
    # This seems not to be the best approach though, as pointed in ENG-1188
    #
    # TODO we should think about having a job_history table to avoid this.
    def audit_job_status_for_job_mark_paid(job)
      return nil if job.blank? || !job.invoice || !job.invoice.mark_paid_at

      AuditJobStatus.new(
        id: JobStatus::MARK_PAID,
        job_status: JobStatus.job_status_for_job_mark_paid,
        job: job,
        last_set_dttm: job.invoice.mark_paid_at,
      )
    end

  end

  def job_status_created?
    job_status_id == JobStatus::CREATED
  end

  def job_status_completed?
    job_status_id == JobStatus::COMPLETED
  end

  def job_status_stored?
    job_status_id == JobStatus::STORED
  end

  def job_status_deleted?
    job_status_id == JobStatus::DELETED
  end

  def job_status_accepted?
    job_status_id == JobStatus::ACCEPTED
  end

  def job_status_done?
    JobStatus::DONE.include?(job_status_id)
  end

  def status_name
    job_status.name
  end
  alias :name :status_name

  def adjusted_created_at
    if adjusted_dttm
      adjusted_dttm
    elsif last_set_dttm
      last_set_dttm
    end
  end

  def user_name
    user&.name
  end

  def company_name
    company&.name
  end

  def ui_type
    UI_TYPE_HISTORY
  end

  #
  # It returns the company that changed the job status associated with this AJS.
  # It can be retrieved either by user.company or oauth_application.
  #
  # Also note that AJS has a company attribute, but that attr corresponds to job.fleet_company.
  # This method is about the company that changed the job status in the UI.
  #
  def changed_by_company
    if user
      user&.company
    elsif source_application&.oauth_application&.owner.is_a?(Company)
      source_application.oauth_application.owner
    end
  end

  private

  def recalc_invoice
    # self.job_status_id==JobStatus::ON_SITE or not needed because we're estimating
    # p2p(ab) and we can't use enroute -> onsite
    if (job_status_id == JobStatus::TOWING) ||
       (job_status_id == JobStatus::TOW_DESTINATION) ||
       (job_status_id == JobStatus::COMPLETED)
      # PDW TODO only need to recalc for time based rates
      job.invoice_dirty = true

      # This should probably happen elsewhere
      if adjusted_dttm
        job.last_status_changed_at = adjusted_dttm
      else
        job.last_status_changed_at = last_set_dttm
      end
    end
  end

  def job_set_adjusted_created_at
    if job_status_created? && persisted?
      calc = AdjustedCreatedAtCalculator.new(job, self)
      job.update_column(:adjusted_created_at, calc.calculate)
    end
  end

  def track_metrics
    if job.done?
      ata = job.ata_mins
      return unless ata
      Metrics::Service.track(job,
                             'job',
                             'ata',
                             ata)
    end
  end

end
