# frozen_string_literal: true

class GraphQLTopic < ApplicationRecord

  has_and_belongs_to_many :graphql_subscriptions

end
