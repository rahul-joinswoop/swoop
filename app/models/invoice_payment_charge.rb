# frozen_string_literal: true

class InvoicePaymentCharge < ApplicationRecord

  include ValidateCurrency

  CHARGE_STATUS_RUNNING = 'charge_running'
  CHARGE_STATUS_SUCCESSFUL = 'charge_successful'
  CHARGE_STATUS_ERROR = 'charge_error'

  validates :status, :invoice, presence: true
  validate_currency_matches :invoice

  belongs_to :payment
  belongs_to :refund
  belongs_to :invoice

  belongs_to :charge_user, class_name: 'User'
  belongs_to :refund_user, class_name: 'User'

  scope :not_refunded, -> { where(refund_id: nil) }
  scope :refunded, -> { where.not(refund_id: nil) }
  scope :with_payment, -> { where.not(payment_id: nil) }

  def slack_status_message
    case status
    when CHARGE_STATUS_RUNNING
      :running
    when CHARGE_STATUS_SUCCESSFUL
      :success
    else
      [:failure, error_msg].reject(&:blank?).join(": ")
    end
  end

end
