# frozen_string_literal: true

# PDW: force Ruby to load Survey::Question before this in production mode (otherwise it picks up top level Question)
require 'survey/question'

class Survey
  class LongTextQuestion < Question

    def slack_message(result)
      Utils.slack_rating_text(description, result.string_value)
    end

    def answer_type
      String
    end

  end
end
