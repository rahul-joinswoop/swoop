# frozen_string_literal: true

class Survey
  class Typeform < Survey

    self.table_name = "surveys"

    def process(response)
      raise ArgumentError, "SurveyResponse must contain hidden.job" unless response["hidden"] && response["hidden"]["job"]

      answers = response['answers']
      results = []
      questions.each do |question|
        result = answers.find { |answer| answer['field']['id'] == question.ref }
        if result
          args = { survey_question_id: question.id }
          if result["type"] == "number"
            args["int_value"] = result["number"]
          elsif result["type"] == "text"
            args["string_value"] = result["text"]
          end
          results << args
        else
          # Typeform doesn't always submit answers especially if one isn't filled in
          Rails.logger.debug "Survey::Typeform Unable to find #{question.ref}"
        end
      end
      results
    end

  end
end
