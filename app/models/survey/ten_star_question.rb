# frozen_string_literal: true

# PDW: force Ruby to load Survey::Question before this in production mode (otherwise it picks up top level Question)
require 'survey/question'

class Survey
  class TenStarQuestion < Question

    def slack_message(result)
      Utils.slack_rating_stars(description, result.int_value, 10)
    end

    def answer_type
      Integer
    end

  end
end
