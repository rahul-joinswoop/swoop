# frozen_string_literal: true

class Survey
  class Result < ApplicationRecord

    belongs_to :job
    belongs_to :review, autosave: true
    belongs_to :survey_question, class_name: 'Survey::Question'

    def slack_message
      survey_question.slack_message(self)
    end

  end
end
