# frozen_string_literal: true

class Survey
  class MobileSwoop < Survey

    self.table_name = "surveys"

    def process(response)
      answers = response['survey_results']
      results = []

      answers.each do |answer|
        question = questions.find(answer.survey_question_id)
        if question
          args = { survey_question_id: question.id }
          if question.is_a?(Survey::FiveStarQuestion) || question.is_a?(Survey::TenStarQuestion)
            args["int_value"] = result["value"]
            results << args
          elsif question.is_a?(Survey::LongTextQuestion)
            args["string_value"] = result["value"]
            results << args
          else
            Rails.logger.debug "Survey::MobileSwoop Unable to find question type #{question.type}"
          end
        else
          # Nonexistant question was sent up
          Rails.logger.debug "Survey::MobileSwoop Unable to find #{answer.survey_question_id}"
        end
      end

      results
    end

  end
end
