# frozen_string_literal: true

class Survey
  class Question < ApplicationRecord

    belongs_to :survey
    has_many :survey_results, class_name: 'Survey::Result'

    scope :in_order, -> { order(order: :asc) }

    #
    # Is this the 10 star NPS question similar to:
    # 'Would you recommend us to you friends/family'
    #
    def nps?
      category == 'nps'
    end

  end
end
