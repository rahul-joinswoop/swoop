# frozen_string_literal: true

class Survey
  class Smiley < Survey

    self.table_name = "surveys"

    NPS_QUESTIONS = [
      ['TenStarQuestion', 'How likely are you to recommend us to family, friends, or colleagues?', 'Question 1 - Recommend (NPS)'],
      ['TenStarQuestion', 'How well were you kept informed while waiting for your towing or roadside service provider to arrive?', 'Question 2 - Kept Informed'],
      ['TenStarQuestion', 'Did the tow or roadside service provider arrive within the initial service response time promised?', 'Question 3 - ATA < ETA'],
      ['LongTextQuestion', 'Thanks for your rating! How do you think we can improve?', 'Question 4 - Improvements'],
    ].freeze

    def process(response)
      answers = response[:responses]
      results = []
      questions.each do |question|
        survey_result = answers.find { |answer| answer['survey_question_id'] == question.id }
        if survey_result
          args = { survey_question_id: question.id }
          if question.answer_type == String
            args[:string_value] = survey_result[:value]
          else
            args[:int_value] = survey_result[:value]
          end

          results << args
        else
          # Typeform doesn't always submit answers especially if one isn't filled in
          Rails.logger.debug "Survey::Smiley Unable to find #{question.id}"
        end
      end
      results
    end

  end
end

# def process(response)
#   answers    = response['answers']
#   results=[]
#   questions.each do |question|
#     result=answers.find {|answer| answer['field']['id'] == question.ref }
#     if result
#       args={survey_question_id:question.id}
#       if result["type"]=="number"
#         args["int_value"]=result["number"]
#       elsif result["type"]=="text"
#         args["string_value"]=result["text"]
#       end
#       results << args
#     else
#       #Typeform doesn't always submit answers especially if one isn't filled in
#       Rails.logger.debug "Survey::Typeform Unable to find #{question.ref}"
#     end
#   end
#   results
# end
