# frozen_string_literal: true

class CreditNote < InvoicingLedgerItem

  belongs_to :invoice

  def calculate_total_amount
    Rails.logger.debug "calculate_total_amount called"
    total_amount
  end

end
