# frozen_string_literal: true

require 'set'
require 'aasm'
require 'open-uri'
require 'elasticsearch/model'
require 'ostruct'

class Job < BaseModel

  extend Gem::Deprecate
  include LoggableJob
  extend LoggableJob
  include Notifiable::Links
  include GraphQLSubscribers

  DEFAULT_SUPPORT_PHONE = "847-796-6763"

  include TwilioConcerns
  include Utils
  include AASM
  include Job::Searchable
  include SoftDelete
  include VirtualStatus
  include HistoryItemTarget

  #
  # A job is invoicable, which means it is expected to
  # implement a set of methods that allow rates and invoices
  # to be calculated
  #
  include Invoicable
  #
  # This alias is required in order to implement the invoicable
  # interface
  #
  alias_method :job_id, :id

  class DuplicateProviderAssignmentError < StandardError; end
  class IllegalStateError < StandardError; end

  DISPLAYABLE_BID_STATUSES = [Auction::Bid::REQUESTED, Auction::Bid::SUBMITTED].freeze

  has_many :bids, class_name: 'Auction::Bid'
  scope :bid_jobs, ->(rescue_company) { joins(:bids).where(bids: { company_id: rescue_company.id }) }
  scope :displayable_bids_for_partners, -> (rescue_company) do
    bid_jobs(rescue_company).where(bids: { status: DISPLAYABLE_BID_STATUSES })
  end

  validates :fleet_company, presence: true
  validates :adjusted_created_at, presence: true
  validates :ref_number, length: { maximum: 255 }

  scope :filter_between, ->(start_at, end_at) { where(' ? < jobs.adjusted_created_at AND jobs.adjusted_created_at <  ?', start_at, end_at) }
  scope :filter_full_name, ->(full_name) { joins(driver: [:user]).where("lower(users.first_name || ' ' || users.last_name) LIKE ?", "%#{full_name.downcase}%") }

  scope :filter_search, ->(search) {
    joins(:account, driver: [:vehicle, :user]).joins('LEFT JOIN companies as rescue_company on rescue_company.id=jobs.rescue_company_id')
      .where("lower(vehicles.vin) LIKE lower(?) OR lower(vehicles.make) LIKE lower(?) OR lower(vehicles.model) LIKE lower(?) OR lower(vehicles.license) LIKE lower(?) OR lower(users.first_name || ' ' || users.last_name) LIKE lower(?) OR jobs.id=? OR jobs.original_job_id=? OR lower(jobs.po_number) LIKE lower(?) OR lower(accounts.name) LIKE lower(?) OR lower(rescue_company.name) LIKE lower(?) OR lower(jobs.driver_notes) LIKE lower(?) ", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", search.to_i, search.to_i, "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
  }
  scope :filter_storage_site, ->(storage_site_id) { joins('LEFT JOIN inventory_items as storage on storage.job_id=jobs.id').where('storage.site_id = ?', storage_site_id.to_i) }

  scope :filter_storage_sites, ->(storage_sites) { where('inventory_items.site_id in (?)', storage_sites) }
  scope :filter_storage_types, ->(storage_types) { where('jobs.storage_type_id in (?)', storage_types) }
  # scope :filter_search, -> search { joins(:account,driver:[:vehicle,:user]).where("lower(vehicles.vin) LIKE lower(?) OR lower(vehicles.make) LIKE lower(?) OR lower(vehicles.model) LIKE lower(?) OR lower(vehicles.license) LIKE lower(?) OR lower(users.first_name || ' ' || users.last_name) LIKE lower(?) OR jobs.id=? OR lower(po_number) LIKE lower(?) OR lower(accounts.name) LIKE lower(?)", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", search.to_i, "%#{search}%", "%#{search}%") }
  scope :filter_rescue_driver_id, ->(rescue_driver_id) { where(rescue_driver_id: rescue_driver_id) }

  # TODO Job.active_and_draft_states is a hack to keep retrocompatibility with legacy code that relied
  # on Job.active_states when it contained 'draft'.
  # This and all spots that use it should be removed by https://swoopme.atlassian.net/browse/ENG-9905
  scope :filter_active, -> { where(status: Job.active_and_draft_states) }
  scope :filter_done, -> { where(status: Job.done_states) }
  scope :filter_before, ->(before) { where('jobs.created_at <  ?', before) }
  scope :filter_after, ->(after) { where('jobs.created_at > ?', after) }
  scope :filter_fleet_managed_jobs, ->(_flag) { where(type: 'FleetManagedJob') }
  # PDW this was causing problems on loading storage and doesn't appear to be using in rendering the storage page (diffed the json):   .select('inventory_items.stored_at')
  scope :filter_stored, -> { select('jobs.*').joins(:inventory_items).where("inventory_items.job_id IS NOT NULL and inventory_items.item_type='Vehicle' and inventory_items.deleted_at IS NULL and inventory_items.released_at IS NULL and jobs.status in ('Stored')") }
  scope :order_id_desc, -> { order(id: :desc) }
  scope :by_fleet_company, ->(fleet_company) { where(fleet_company: fleet_company) }
  scope :with_vehicle, -> { joins(driver: :vehicle) }
  scope :in_progress, -> {
    statuses = JobStatus::DONE.map { |s| JobStatus::ID_MAP[s] }
    where.not(status: statuses)
  }
  scope :by_vin, ->(vin) { with_vehicle.where(vehicles: { vin: vin }) }

  scope :not_reassigned, -> { where("jobs.status != ?", JobStatus::ID_MAP[JobStatus::REASSIGNED]) }
  scope :not_rejected, -> { where("jobs.status != ?", JobStatus::ID_MAP[JobStatus::REJECTED]) }
  scope :not_demo, -> { joins("LEFT JOIN companies ON jobs.rescue_company_id = companies.id").merge(Company.not_demo) }
  # In this case, a tow-out is defined as any scheduedled job whose
  # original_job is a storage job.
  scope :not_tow_out, -> {
    joins("LEFT JOIN jobs original_jobs ON original_jobs.id = jobs.original_job_id")
      .joins("LEFT JOIN service_codes original_service_codes ON original_service_codes.id = original_jobs.service_code_id")
      .where("
      jobs.original_job_id IS NULL
      OR (
        original_jobs.will_store IS NOT TRUE
        AND original_service_codes.name IS DISTINCT FROM ?
        AND jobs.scheduled_for IS NULL
      )
    ", ServiceCode::STORAGE)
  }
  scope :filter_reviews, ->(enabled) {
                           joins("INNER JOIN reviews ON jobs.id = reviews.job_id")
                             .joins("LEFT JOIN companies ON jobs.fleet_company_id = companies.id")
                             .where("
                             companies.deleted_at is null
                             AND (
                               reviews.nps_question1 IS NOT NULL
                               OR reviews.nps_question2 IS NOT NULL
                               OR reviews.nps_question3 IS NOT NULL
                               OR reviews.nps_feedback IS NOT NULL
                             )
                           ")
                         }

  scope :filter_q1_scores, ->(scores) { where("reviews.nps_question1 in (?)", scores) }
  scope :without_q1_scores, -> { where("reviews.nps_question1 is null") }
  scope :filter_owner_companies, ->(ids) { where("jobs.fleet_company_id in (?)", ids) }

  # Customer & Vehicle
  belongs_to :driver, class_name: 'Drive'
  has_one :customer, class_name: 'User', through: :driver, source: :user
  has_one :stranded_vehicle, class_name: 'StrandedVehicle', through: :driver, source: :vehicle
  belongs_to :tire_type

  # Rescue
  belongs_to :rescue_driver, class_name: 'User', foreign_key: 'rescue_driver_id', inverse_of: :jobs
  belongs_to :rescue_vehicle, class_name: 'Vehicle', foreign_key: 'rescue_vehicle_id', inverse_of: :jobs
  belongs_to :trailer_vehicle, class_name: 'Vehicle', foreign_key: 'trailer_vehicle_id', inverse_of: :jobs

  has_many :time_of_arrivals
  has_one :bta,  -> { where(eba_type: TimeOfArrival::BID).order(:created_at) }, class_name: 'TimeOfArrival', autosave: true, inverse_of: :job # Time of arrival estimated by partner when bidding on a job
  has_many :eta, -> { where(eba_type: TimeOfArrival::ESTIMATE).order(:created_at) }, class_name: 'TimeOfArrival', inverse_of: :job # Auto estimated time of arrival by truck location. destination locationa and traffic
  has_many :fta, -> { where(eba_type: TimeOfArrival::FLEET).order(:created_at) }, class_name: 'TimeOfArrival', inverse_of: :job # fleet user estimated time
  has_many :sta, -> { where(eba_type: TimeOfArrival::SWOOP_ESTIMATE).order(:created_at) }, class_name: 'TimeOfArrival', inverse_of: :job # human estimated time of arrival by swoop dispatcher
  has_many :pta, -> { by_type(TimeOfArrival::PARTNER) }, class_name: 'TimeOfArrival', inverse_of: :job
  # Latest ToA's

  belongs_to :pcc_claim, class_name: 'PCC::Claim'
  belongs_to :pcc_coverage, class_name: 'PCC::Coverage'

  has_one :original_toa, -> { non_live.order(:id) }, class_name: 'TimeOfArrival', inverse_of: :job
  has_one :latest_ata,   -> { actual.order('created_at DESC') },   class_name: 'TimeOfArrival', inverse_of: :job
  has_one :latest_pta,   -> { non_live.order('created_at DESC') }, class_name: 'TimeOfArrival', inverse_of: :job
  has_one :latest_eta,   -> { live.order('created_at DESC') },     class_name: 'TimeOfArrival', inverse_of: :job
  has_many :time_of_arrivals_non_live, -> { non_live }, class_name: 'TimeOfArrival', inverse_of: :job

  has_many :notifications

  has_many :partner_internal_notes
  has_many :partner_dispatch_notes
  has_many :partner_invoice_notes
  has_many :swoop_internal_notes
  has_many :public_notes
  has_many :fleet_internal_notes

  has_many :sites
  has_many :images, -> { where(deleted_at: nil) }, inverse_of: :job
  has_many :inventory_items

  # TODO - which invoice is this (in the context of the various invoices below)?
  belongs_to :invoice, autosave: true

  # All live invoices for partner, swoop and fleet
  # Checkout invoices_by_viewer if you need the invoices for current viewer
  has_many :live_invoices, -> { where(deleted_at: nil).order(:id) }, class_name: 'Invoice', inverse_of: :job

  # Invoices the partner has created
  has_many :partner_live_invoices, -> (job) { where(deleted_at: nil).where('invoicing_ledger_items.sender_id = ?', job.rescue_company_id).order(:id) }, class_name: 'Invoice', inverse_of: :job

  # Invoices the fleet has received (from partner or swoop) plus the invoices fleet has sent (to customers)
  has_many :fleet_live_invoices, -> (job) { where(deleted_at: nil).where('invoicing_ledger_items.sender_id in (?)', [Company.swoop_id, job.fleet_company_id]).order(:id) }, class_name: 'Invoice', inverse_of: :job

  # only defined for FleetManagedJob, stubbed out for all other classes because reasons. see
  # Invoice#fleet_managed_invoice_approved_or_sent? for the only known usage. original comments
  # from Invoice#find_active_fleet_managed_invoice:
  #
  # If the job is FleetManagedJob, and this is a PartnerInvoice
  # approved or downloaded by Swoop, a correspondent FleetManagedInvoice
  # will exist.
  #
  # @return the 'FleetManagedInvoice'
  has_one :active_fleet_managed_invoice_from_swoop, -> { none }, class_name: 'Invoice', inverse_of: :job, foreign_key: :job_id

  # Only the fleets invoices to their customer
  has_many :fleet_customer_live_invoices, -> (job) { joins('INNER JOIN companies ON (companies.id = invoicing_ledger_items.sender_id)').where(deleted_at: nil).where('invoicing_ledger_items.sender_id = ?', job.fleet_company_id).where("companies.type = 'FleetCompany'").order(:id) }, class_name: 'Invoice', inverse_of: :job

  has_many :question_results, -> { includes(:question).order("questions.order_num ASC") }, autosave: true, inverse_of: :job

  has_many :audit_sms, class_name: 'AuditSms'

  has_many :target_variants, as: :target

  has_one :storage, -> { where(deleted_at: nil, item_type: 'Vehicle') }, class_name: 'InventoryItem', autosave: true, inverse_of: :job
  belongs_to :eta_explanation, class_name: 'JobEtaExplanation'
  belongs_to :reject_reason, class_name: 'JobRejectReason'
  delegate :reason, to: :reject_reason, prefix: true, allow_nil: true
  delegate :member_number, to: :customer, allow_nil: true
  delegate :miles_p2p, to: :estimate, allow_nil: true
  belongs_to :user, optional: true # User who created the job

  belongs_to :service_code
  belongs_to :storage_type
  belongs_to :fleet_company, class_name: 'Company', foreign_key: 'fleet_company_id', inverse_of: :fleet_jobs
  belongs_to :rescue_company, class_name: 'Company', foreign_key: 'rescue_company_id', inverse_of: :rescue_jobs
  belongs_to :originating_company, class_name: 'Company', foreign_key: 'originating_company_id'
  belongs_to :service_location, class_name: 'Location', foreign_key: 'service_location_id'
  belongs_to :original_service_location, class_name: 'Location', foreign_key: 'original_service_location_id' # if service_location was overwritten by a mobile web get_location submission, this is the original dispatcher entered service_location (if existed)
  belongs_to :drop_location, class_name: 'Location', foreign_key: 'drop_location_id'
  has_one :storage_site, through: :drop_location, source: :site
  belongs_to :last_touched_by, class_name: 'User', foreign_key: 'last_touched_by_id'
  belongs_to :caller, class_name: 'User', foreign_key: 'caller_id'
  has_one :billing_info, required: false, inverse_of: :job, autosave: true
  belongs_to :account, inverse_of: :jobs
  belongs_to :partner_department, class_name: 'Department'
  belongs_to :fleet_dispatcher, class_name: 'User', foreign_key: 'fleet_dispatcher_id'
  belongs_to :partner_dispatcher, class_name: 'User', foreign_key: 'partner_dispatcher_id'
  belongs_to :root_dispatcher, class_name: 'User', foreign_key: 'root_dispatcher_id'
  belongs_to :issc_dispatch_request
  has_one :issc, through: :issc_dispatch_request
  delegate :system, to: :issc, prefix: true, allow_nil: true
  delegate :more_time_requested?, to: :issc_dispatch_request, allow_nil: true
  belongs_to :service_alias
  belongs_to :invoice_vehicle_category, class_name: 'VehicleCategory', foreign_key: 'invoice_vehicle_category_id'

  belongs_to :department

  has_one :review
  has_many :twilio_replies

  has_many :history_items, inverse_of: :job

  has_many :all_audit_job_statuses, -> { order(:created_at) }, class_name: 'AuditJobStatus'
  has_many :audit_job_statuses, autosave: true, inverse_of: :job
  has_many :audit_job_statuses_order_created, -> { where(deleted_at: nil).order(:created_at) }, class_name: "AuditJobStatus"
  # has_one :audit_completed_at, -> { where(job_status: JobStatus::COMPLETED).order(last_set_dttm: :DESC).limit(1)}, :class_name => "AuditJobStatus"
  has_many :attached_documents, -> { where(deleted_at: nil).order(created_at: :desc) }, inverse_of: :job

  has_many :job_status_reasons
  has_many :job_status_reason_types, through: :job_status_reasons

  belongs_to :pickup_contact, class_name: 'User', foreign_key: 'pickup_contact_id'
  belongs_to :dropoff_contact, class_name: 'User', foreign_key: 'dropoff_contact_id'
  belongs_to :fleet_site, class_name: 'Site', foreign_key: 'fleet_site_id'
  belongs_to :site
  belongs_to :customer_type
  belongs_to :symptom
  belongs_to :estimate
  belongs_to :parent, class_name: 'Job'
  has_many :estimates
  has_many :job_errors

  has_many :child_jobs, -> { where(deleted_at: nil) }, class_name: 'Job', foreign_key: :original_job_id
  has_many :explanations, -> { where(deleted_at: nil) }

  has_many :pickup_images, -> { where(deleted_at: nil, container: 'pickup') }, class_name: 'AttachedDocument::LocationImage', inverse_of: :job
  has_many :dropoff_images, -> { where(deleted_at: nil, container: 'dropoff') }, class_name: 'AttachedDocument::LocationImage', inverse_of: :job
  has_many :release_images, -> { where(deleted_at: nil, container: 'release') }, class_name: 'AttachedDocument::LocationImage', inverse_of: :job
  has_many :pickup_signatures, -> { where(deleted_at: nil, container: 'pickup') }, class_name: 'AttachedDocument::SignatureImage', inverse_of: :job
  has_many :dropoff_signatures, -> { where(deleted_at: nil, container: 'dropoff') }, class_name: 'AttachedDocument::SignatureImage', inverse_of: :job
  has_many :release_signatures, -> { where(deleted_at: nil, container: 'release') }, class_name: 'AttachedDocument::SignatureImage', inverse_of: :job
  has_many :invoice_documents, -> { where(deleted_at: nil, invoice_attachment: true) }, class_name: 'AttachedDocument', inverse_of: :job

  belongs_to :policy_location, class_name: 'Location'

  has_one :candidate_assignment, class_name: 'Job::Candidate::Assignment'
  has_many :candidates, -> { order(:google_eta) }, class_name: 'Job::Candidate'

  has_one :auction, class_name: 'Auction::Auction'
  has_one :view_alert, autosave: true

  has_many :assignments

  accepts_nested_attributes_for :driver
  accepts_nested_attributes_for :service_location
  accepts_nested_attributes_for :drop_location
  accepts_nested_attributes_for :account
  accepts_nested_attributes_for :caller
  accepts_nested_attributes_for :pickup_contact
  accepts_nested_attributes_for :dropoff_contact
  accepts_nested_attributes_for :images
  accepts_nested_attributes_for :inventory_items
  accepts_nested_attributes_for :issc_dispatch_request
  accepts_nested_attributes_for :question_results

  accepts_nested_attributes_for :policy_location

  accepts_nested_attributes_for :partner_internal_notes
  accepts_nested_attributes_for :partner_dispatch_notes
  accepts_nested_attributes_for :partner_invoice_notes
  accepts_nested_attributes_for :swoop_internal_notes
  accepts_nested_attributes_for :public_notes
  accepts_nested_attributes_for :fleet_internal_notes

  #### ActiveRecord CALLBACKS #####

  # TODO code is ready but commented out as product will work more on it.
  # @see https://swoopme.atlassian.net/browse/ENG-4444
  # after_commit :schedule_swoop_alert_if_fleet_managed_job_not_dispatched, on: :create

  before_validation :set_adjusted_created_at

  before_save :set_uuid
  before_save :remove_service_alias, unless: :valid_service_alias?
  before_save :track_status_change_for_customer, unless: :draft_or_predraft?

  after_save :service_change, unless: :draft_or_predraft?
  after_save :location_change, unless: :draft_or_predraft?
  after_save :priority_response_change, unless: :draft_or_predraft?
  after_save :invoice_vehicle_category_change, unless: :draft_or_predraft?
  after_save :customer_type_change, unless: :draft_or_predraft?, if: :saved_change_to_customer_type_id?
  after_save :site_change, unless: :draft_or_predraft?
  after_save :process_auto_assign

  after_commit :send_status_emails, unless: :draft_or_predraft?

  after_commit :on_estimate_dirty, on: :update, unless: :draft_or_predraft?
  after_commit :on_invoice_dirty, on: :update, unless: :draft_or_predraft?
  after_commit :on_company_assigned, unless: :draft_or_predraft?
  after_commit :slack_after_update, on: :update, unless: :draft_or_predraft?
  after_commit :scheduled_system_alert, on: :update, unless: :draft_or_predraft?, if: :fleet_managed_job?

  after_commit do
    if saved_change_to_status?
      Notifiable.trigger_job_assigned self
    end
  end

  after_commit do
    # changing company, driver, or status can cause a job to appear in a driver or dispatcher's job list
    # so we want to send trigger_job_status_changed in this case
    # TODO - wish we just had a subscription for the list :/
    if saved_change_to_status? || saved_change_to_rescue_company_id? || saved_change_to_rescue_driver_id?
      Subscribable.trigger_job_status_changed self
    end
  end
  # always fire off the job_updated subscription
  after_commit do
    Subscribable.trigger_job_updated self
  end

  after_commit do
    if saved_change_to_status? || saved_change_to_rescue_driver_id?
      Notifiable.trigger_job_dispatched self
    end
  end

  after_commit do
    if saved_change_to_status?
      Notifiable.trigger_job_canceled self
    end
  end

  after_commit do
    if saved_change_to_rescue_driver_id && rescue_driver_id_before_last_save
      Notifiable.trigger_job_reassigned self, rescue_driver_id_before_last_save
    end
  end

  after_commit do
    if saved_change_to_service_code_id && service_code_id_before_last_save
      Notifiable.trigger_job_service_type_changed self
    end
  end

  after_commit do
    if saved_change_to_service_location_id && service_location_id_before_last_save
      Notifiable.trigger_job_location_changed self
    end
  end

  after_commit :run_job_simulator, on: [:create, :update]

  after_commit do
    if saved_change_to_rescue_driver_id && rescue_driver_id
      # we do this in a worker so we don't mess with the saved_changes for any other callback
      AssignRescueDriverVehicleToJobWorker.perform_async id
    end
  end

  #### END ActiveRecord CALLBACKS #####

  attr_accessor :estimate_dirty
  attr_accessor :invoice_dirty
  attr_accessor :company_assigned
  attr_accessor :needs_auto_assign

  # TODO add manual validation on status

  # These are the strings you can use to compare and set job.status field
  STATUS_PREDRAFT = 'predraft'
  STATUS_DRAFT = 'draft'
  STATUS_CREATED = 'created'
  STATUS_AUTO_ASSIGNING = 'auto_assigning'
  STATUS_PENDING = 'pending'
  STATUS_UNASSIGNED = 'unassigned'
  STATUS_ASSIGNED = 'assigned'
  STATUS_ACCEPTED = 'accepted'
  STATUS_ETAEXTENDED = 'etaextended'
  STATUS_REJECTED = 'rejected'
  STATUS_REASSIGNED = 'reassigned'
  STATUS_DISPATCHED = 'dispatched'
  STATUS_ENROUTE = 'enroute'
  STATUS_ONSITE = 'onsite'
  STATUS_TOWING = 'towing'
  STATUS_TOWDESTINATION = 'towdestination'
  STATUS_STORED = 'stored'
  STATUS_RELEASED = 'released'
  STATUS_COMPLETED = 'completed'
  STATUS_CANCELED = 'canceled'
  STATUS_GOA = 'goa'
  STATUS_DELETED = 'deleted'

  # Statuses for FleetMotorClubJobs only
  STATUS_SUBMITTED = 'submitted'
  STATUS_EXPIRED = 'expired'
  STATUS_ETAREJECTED = 'etarejected'

  # we build up our status enum values from JobStatus so we don't define names
  # in both places.
  #
  # TODO - compared to what's in JobStatus we are missing :REASSIGNED and
  # :MARK_PAID in this list - is that intentional?
  STATUSES = {
    initial: :INITIAL,
    created: :CREATED,
    predraft: :PREDRAFT,
    draft: :DRAFT,
    auto_assigning: :AUTO_ASSIGNING,
    pending: :PENDING,
    unassigned: :UNASSIGNED,
    assigned: :ASSIGNED,
    accepted: :ACCEPTED,
    etaextended: :ETA_EXTENDED,
    submitted: :SUBMITTED,
    rejected: :REJECTED,
    etarejected: :ETA_REJECTED,
    expired: :EXPIRED,
    reassigned: :REASSIGNED,
    dispatched: :DISPATCHED,
    enroute: :EN_ROUTE,
    onsite: :ON_SITE,
    towing: :TOWING,
    towdestination: :TOW_DESTINATION,
    stored: :STORED,
    completed: :COMPLETED,
    canceled: :CANCELED,
    released: :RELEASED,
    goa: :GOA,
    deleted: :DELETED,
  }.freeze

  enum status: STATUSES.transform_values { |v| JobStatus::NAMES[v] }

  # this has to happen after the above enum line: or else it won't work
  # TODO - maybe move all the status stuff into a separate concern and then we can setup
  # dependencies correctly?
  include LegalStateChanges

  # used in JobStatus to go from JobStatus -> statuses used in Job (lol at all the naming)
  JOB_STATUS_TO_STATUS_MAP = statuses.invert

  # some places use Job::A_STATUS to get the name of a status so we have to
  # provide these constants here. ideally we change all of those places to
  # use Job.statuses[:a_status] or JobStatus::NAMES[:A_STATUS] instead, then we
  # can remove this hackery. i'll open a ticket for this.
  #
  # there's also a few subtle differences between our status constant names,
  # hence this mapping.
  STATUSES_MAP = {
    ETA_REJECTED: :ETAREJECTED,
    EN_ROUTE: :ENROUTE,
    ON_SITE: :ONSITE,
    TOW_DESTINATION: :TOWDESTINATION,
  }.freeze

  # metaprogram our constants
  # TODO - do we need this? doesn't aasam handle this for us?
  STATUSES.each_value { |v| Job.const_set (STATUSES_MAP[v] || v), JobStatus::NAMES[v] }

  aasm column: :status,
       enum: true,
       # need direct assigment to allow undo_storage
       no_direct_assignment: false,
       whiny_persistence: true do
    # TODO - can we metaprogram these from STATUSES like above?
    state :created, initial: true
    state :predraft
    state :draft
    state :auto_assigning
    state :pending
    state :unassigned
    state :assigned
    state :accepted
    state :rejected
    state :reassigned
    state :dispatched
    state :enroute
    state :onsite
    state :towing
    state :towdestination
    state :stored
    state :released
    state :completed
    state :canceled
    state :goa
    state :deleted

    after_all_transitions :log_transition

    # this event was previously called 'original'
    event :move_to_pending, before: :before_moving_to_pending do
      transitions from: [:created, :draft, :predraft], to: :pending
    end

    event :rejected_copy do
      transitions from: :created, to: :unassigned
    end

    event :released, after: :release_storage do
      transitions from: [:stored], to: :released
    end

    event :original_store_vehicle, after: [:set_invoice_dirty] do
      transitions from: [:pending], to: :stored
    end

    event :store_vehicle, after: [:set_invoice_dirty] do
      transitions from: [:completed], to: :stored
    end

    event :pending_copy, before: :to_pending_copy, after: [:set_invoice_dirty] do
      transitions from: :created, to: :pending
    end

    # I wanted to make this a concern, however the way AASM works
    # I couldn't wrap the methods properly inside this block,
    # with that said, allow anything that extends this to use auto_assign
    # if they want. (Maybe we will do this for more things in the future.)
    event :auto_assign do
      transitions from: [:pending, :unassigned, :created, :rejected, :assigned], to: :auto_assigning, guard: :can_auto_assign?
      transitions from: [:pending, :unassigned, :created, :rejected, :assigned], to: :pending, guard: :cant_auto_assign?
      after do
        @needs_auto_assign = true
      end
    end

    # Partner

    event :partner_dispatch do
      transitions from: :pending, to: :dispatched, guard: :partner_job?
    end
  end

  # this was has_one :job_status but that won't ever work since we don't store a job_status_id here or a job_id there.
  # TODO - is this ever necessary?
  def job_status
    JobStatus.find_by(name: Job.statuses[status])
  end

  # Class methods
  class << self

    # This is just an idea, this will allow us to
    # support various things between each of the jobs,
    # while keeping a uniform interface... My reasoning behind this
    # is you dont need to find your self doing .is_a? or .responds_to?
    # Consequences:
    #   - This may add some more cognative overhead at first
    #   - Allows us to support things like supports[:reminder_notifications] = 5.minutes
    #   - I personally am not sure about this approach.
    def supports
      @supports ||= {}
    end

  end

  def current_assignment
    assignments&.last
  end

  def backwards_compatible_notes(job_field, notes_relation)
    if Flipper[:notes].enabled? && notes_relation.size > 0
      notes_relation[0].text
    else
      self[job_field]
    end
  end

  def backwards_compatible_notes_assign(job_field, notes_relation, str)
    if Flipper[:notes].enabled?
      if notes_relation.size > 0
        Rails.logger.debug "backwards_compatible_notes_assign found relation"
        notes_relation[0].text = str
      else
        notes_relation.build(text: str)
      end
    else
      self[job_field] = str
    end
  end

  # partner_notes  : "partner dispatch and driver notes" => PartnerInternalNote
  def partner_notes
    backwards_compatible_notes(:partner_notes, partner_internal_notes)
  end

  def partner_notes=(str)
    backwards_compatible_notes_assign(:partner_notes, partner_internal_notes, str)
  end

  # dispatch_notes : "partner dispatcher only notes" => PartnerDispatchNote
  def dispatch_notes
    backwards_compatible_notes(:dispatch_notes, partner_dispatch_notes)
  end

  def dispatch_notes=(str)
    Rails.logger.debug "dispatch_notes= called"
    backwards_compatible_notes_assign(:dispatch_notes, partner_dispatch_notes, str)
  end

  # driver_notes   : "partner invoice notes" => PartnerInvoiceNote
  def driver_notes
    backwards_compatible_notes(:driver_notes, partner_invoice_notes)
  end

  def driver_notes=(str)
    backwards_compatible_notes_assign(:driver_notes, partner_invoice_notes, str)
  end

  # swoop_notes    : "swoop only notes" => SwoopInternalNote
  def swoop_notes
    backwards_compatible_notes(:swoop_notes, swoop_internal_notes)
  end

  def swoop_notes=(str)
    backwards_compatible_notes_assign(:swoop_notes, swoop_internal_notes, str)
  end

  # notes          : "job detail notes" => PublicNote
  def notes
    backwards_compatible_notes(:notes, public_notes)
  end

  def notes=(str)
    backwards_compatible_notes_assign(:notes, public_notes, str)
  end

  # fleet_notes    : "fleet internal" => FleetInternalNote
  def fleet_notes
    backwards_compatible_notes(:fleet_notes, fleet_internal_notes)
  end

  def fleet_notes=(str)
    backwards_compatible_notes_assign(:fleet_notes, fleet_internal_notes, str)
  end

  # TODO - this makes brian nauseated.
  def internal_notes(company)
    send company.internal_notes_field
  end

  def set_adjusted_created_at
    calc = AdjustedCreatedAtCalculator.new(self)
    self.adjusted_created_at = calc.calculate
  end

  def audit_job_status_accepted
    audit_job_statuses.detect { |ajs| ajs.job_status_accepted? && ajs.deleted_at.nil? }
  end

  def audit_job_status_created
    audit_job_statuses.detect { |ajs| ajs.job_status_created? && ajs.deleted_at.nil? }
  end

  def audit_job_status_stored_or_completed
    audit_job_statuses.detect { |ajs| ajs.job_status_stored? && ajs.deleted_at.nil? } ||
      audit_job_statuses.detect { |ajs| ajs.job_status_completed? && ajs.deleted_at.nil? }
  end

  DISPATCHED_IF_PENDING_OR_ACCEPTED_TO = [
    JobStatus::SUBMITTED,
    JobStatus::DISPATCHED,
    JobStatus::EN_ROUTE,
    JobStatus::ON_SITE,
    JobStatus::TOWING,
    JobStatus::TOW_DESTINATION,
    JobStatus::COMPLETED,
    JobStatus::STORED,
    JobStatus::RELEASED,
    JobStatus::CANCELED,
    JobStatus::REJECTED,
    JobStatus::GOA,
    JobStatus::EXPIRED,
  ].to_set.freeze

  # Finds the time when the job status transitioned to DISPATCHED or when the job transitions from PENDING/ACCEPTED
  # into one of the above states.
  def dispatched_at
    time = nil
    prev_ajs = nil
    audit_job_statuses.each do |ajs|
      next unless ajs.deleted_at.nil?
      if ajs.job_status_id == JobStatus::DISPATCHED
        time = ajs.adjusted_dttm || ajs.last_set_dttm
      else
        prev_status_id = prev_ajs.job_status_id if prev_ajs
        if (prev_status_id == JobStatus::PENDING || prev_status_id == JobStatus::ACCEPTED) &&
            Job::DISPATCHED_IF_PENDING_OR_ACCEPTED_TO.include?(ajs.job_status_id)
          time = ajs.adjusted_dttm || ajs.last_set_dttm
        end
      end
      prev_ajs = ajs
    end
    time
  end

  def completed_at
    ajs = audit_job_statuses.select { |s| s.id && s.job_status_done? && s.deleted_at.nil? }.sort_by(&:id).last
    ajs.adjusted_dttm || ajs.last_set_dttm if ajs
  end

  def stored_at
    ajs = audit_job_statuses.select { |s| s.id && s.job_status_stored? && s.deleted_at.nil? }.sort_by(&:id).last
    ajs.adjusted_dttm || ajs.last_set_dttm if ajs
  end

  def last_status_date_time
    last_status = audit_job_statuses.joins(:job_status)
      .where(deleted_at: nil)
      .where.not(
        job_statuses: { id: [
          JobStatus::INITIAL,
          JobStatus::PREDRAFT,
          JobStatus::PENDING,
          JobStatus::ASSIGNED,
          JobStatus::AUTO_ASSIGNING,
        ] }
      )
      .select('(CASE WHEN (adjusted_dttm IS NULL) THEN last_set_dttm ELSE adjusted_dttm END) status_date, *')
      .order('status_date DESC').take

    last_status.adjusted_dttm || last_status.last_set_dttm
  end

  def invoice_states
    live_invoices.map(&:state)
  end

  def paid_invoices?
    live_invoices.map(&:paid).include? true
  end

  def unpaid_invoices?
    live_invoices.map(&:paid).include? false
  end

  def claim_number
    pcc_claim&.claim_number
  end

  # mappings that are used in JobStatusGroupEnumType
  JOB_STATUS_GROUPS = {
    draft_states: { name: 'Draft', description: 'Draft Jobs' },
    active_states: { name: 'Active', description: 'Active Jobs' },
    done_states: { name: 'Done', description: 'Done Jobs' },
    map_states: { name: 'Map', description: 'Map Jobs' },
    all_states: { name: 'All', description: 'All Jobs' },
  }.freeze

  def self.job_status_groups
    JOB_STATUS_GROUPS.keys.reject { |s| s == :all_states }
  end

  def track_status_change_for_customer
    log :debug, "track_status_change_for_customer track_status_change_for_customer called", job: id

    if saved_change_to_status?
      publish_customer_websocket('update_status', status_was: status_before_last_save)
    end
  end

  def scheduled?
    scheduled_for
  end

  def partner_live?
    if rescue_company && rescue_company.live
      true
    else
      false
    end
  end

  def partner_live_and_open?
    partner_live? && (site && site.open_for_business?)
  end

  def partner_not_live_or_live_and_after_hours?
    !partner_live? || (site && !site.open_for_business?)
  end

  def remove_rescue_driver
    self.rescue_driver_id = nil
  end

  # PDW this was tracking_lin_sms, then went to partner_sms_track_driver and now is sms_track_driver
  # It's shared by fleet and partner and appears in the UI as "Text link to track driver when enroute"
  # It only appears int he UI as an options when the 'SMS_Track Driver' Feature is turned on (whish is it by deafult for fleet companies)
  def sms_track_driver
    partner_sms_track_driver
  end

  def sms_track_driver=(val)
    self.partner_sms_track_driver = val
  end

  def store_vehicle?
    logger.debug "STORAGE(#{id}) create_storage_if_necessary_and_invoice called"
    if will_store && !storage
      logger.debug "STORAGE(#{id}) Creating storage"
      true
    else
      logger.debug "STORAGE(#{id}) not doing because will_store:#{will_store} or storage:#{storage}"
      false
    end
  end

  # This was used exclusively by BMW, and is kept here for retrocompatibility.
  # It now only returns an empty array.
  #
  # Product team confirmed that we'll need it in the future, but it will likely
  # be a table association rather than a hardcoded method with logic specific to
  # a company (the way it was for BMW)
  #
  def equipment
    []
  end

  # FIXME location_type is kept here only for retrocompatibility.
  # It should be removed. @see ENG-2065.
  def create_additional_job(extra_attributes = nil)
    copy_attributes = %w(
      fleet_company_id
      notes
      email
      unit_number
      ref_number
      vip
      uber
      alternate_transportation
      customer_type_id
      type
      last_touched_by_id
      po_number
      location_type
      fleet_notes
      odometer
      no_keys
      keys_in_trunk
      four_wheel_drive
      accident
      symptom_id
      user_id
      fleet_dispatcher_id
      root_dispatcher_id
      get_location_attempts
      department_id
    )
    copy_attributes += extra_attributes unless extra_attributes.nil?

    copy = self.class.new
    copy_attributes.each do |attr|
      copy[attr] = self[attr]
    end
    # Deep copy users
    user_attributes = ['caller_id']
    user_attributes.each do |attr|
      next if self[attr].nil?
      user = User.find_by!(id: self[attr])
      user_copy = user.copy_user
      user_copy.save!
      copy[attr] = user_copy.id
    end

    # Deep copy locations
    unless self['drop_location_id'].nil?
      loc = Location.find_by!(id: self['drop_location_id'])
      loc_copy = loc.copy_location
      loc_copy.save!
      copy['service_location_id'] = loc_copy.id
    end

    # Deep copy driver information
    unless self['driver_id'].nil?
      driver_copy = driver.copy_driver
      driver_copy.save!
      copy['driver_id'] = driver_copy.id
    end

    copy.parent_id = id
    copy.original_job_id = id
    copy.original_job_id = original_job_id if original_job_id

    copy
  end

  # FIXME location_type and dropoff_location_type are kept here only for retrocompatibility.
  # It should be removed. @see ENG-2065.
  def copy_job(extra_attributes = nil, copy_ajs = true)
    copy_attributes = %w(
      service_code_id
      fleet_company_id
      notes
      email
      scheduled_for
      unit_number
      ref_number
      vip
      uber
      alternate_transportation
      customer_type_id
      type
      last_touched_by_id
      po_number
      location_type
      dropoff_location_type
      blocking_traffic
      unattended
      fleet_notes
      fleet_site_id
      site_id
      odometer
      no_keys
      keys_in_trunk
      four_wheel_drive
      accident
      symptom_id
      user_id
      get_location_attempts
      tire_type_id
      swoop_notes
      review_sms
      priority_response
      policy_number
      department_id
      partner_sms_track_driver
      text_eta_updates
    )
    copy_attributes += extra_attributes unless extra_attributes.nil?

    copy = self.class.new
    copy_attributes.each do |attr|
      copy[attr] = self[attr]
    end
    # Deep copy users
    user_attributes = %w(
      caller_id
      pickup_contact_id
      dropoff_contact_id
    )
    user_attributes.each do |attr|
      next if self[attr].nil?
      user = User.find_by!(id: self[attr])
      user_copy = user.copy_user
      user_copy.save!
      copy[attr] = user_copy.id
    end

    # Deep copy locations
    loc_attributes = %w(
      service_location_id
      drop_location_id
    )
    loc_attributes.each do |attr|
      next if self[attr].nil?
      loc = Location.find_by!(id: self[attr])
      loc_copy = loc.copy_location
      loc_copy.save!
      copy[attr] = loc_copy.id
    end

    # Deep copy driver information
    unless self['driver_id'].nil?
      driver_copy = driver.copy_driver
      driver_copy.save!
      copy['driver_id'] = driver_copy.id
    end

    question_results.each do |qr|
      new_qr = qr.dup
      copy.question_results << new_qr
    end

    if copy_ajs
      original_created = ajs_by_status_id(JobStatus::CREATED)
      copy.audit_job_statuses.build(job_status_id: JobStatus::INITIAL, last_set_dttm: original_created.last_set_dttm, company: fleet_company, user: last_touched_by)
      copy.audit_job_statuses.build(job_status_id: JobStatus::CREATED, last_set_dttm: original_created.last_set_dttm, company: fleet_company, user: last_touched_by)
    end

    copy.parent_id = id
    copy
  end

  def delete_invoice
    return if invoice.nil?

    invoice.deleted_at = Time.now
  end

  def after_partner_reject
    copy = copy_job([
      "pcc_claim_id",
      "pcc_coverage_id",
      "fleet_dispatcher_id",
      "root_dispatcher_id",
    ])
    copy.rejected_copy
    copy.save!

    copy.process_auto_assign
  end

  def after_fleet_reasssigned
    copy = copy_job([
      "pcc_claim_id",
      "pcc_coverage_id",
      "fleet_dispatcher_id",
      "root_dispatcher_id",
    ])
    copy.status = :unassigned
    copy.save!
  end

  # PDW: The invoice is sent upon job completion by the driver on android app. The invoice approval is a formality.
  def send_user_invoice
    logger.debug 'after_override_completed'

    if can_send_user_invoice?
      invoice.send_to_recipient
    end
  end

  # It checks if the user receipt can be sent to this job.
  #
  # It will be allowed if:
  #
  # - job has an invoice
  # - invoice recipient is User (and not account)
  # - job has an email set (it can be set on job form)
  # - vehicle is not going to be stored
  # - a charge receipt hasn't been sent yet
  def can_send_user_invoice?
    invoice &&
    (invoice.recipient_type == 'User') &&
    invoice.job.email &&
    !will_store &&
    !invoice.any_charge_receipt_email_sent?
  end

  def release_storage
    items = vehicle_inventory_items(true)
    if items.count > 0
      items[0].released_at = Time.now
      items[0].save!
    end
  end

  # The job goes to stored status when release is undone on the UI (can only happen to RescueJobs exclusively).
  #
  # It means that storage days need to keep counting on the invoice. To allow it we:
  #    - set vehicle_inventory_items.released_at = nil
  #    - set the 'Released' AuditJobStatus as deleted
  #
  # So when StorageInvoices batch runs, StorageRate.calculate_line_items will be able to keep counting the stored days.
  #
  def undo_release
    items = vehicle_inventory_items(false)

    items.update_all(released_at: nil) if items.present?

    audit_job_statuses.joins(:job_status)
      .where(job_statuses: { id: JobStatus::RELEASED })
      .where(deleted_at: nil)
      .update_all(deleted_at: Time.now)
  end

  # def has_storage_items
  #  items = vehicle_inventory_items(false)
  #  return item.count == 0
  # end

  def progress_storage
    # Get stored state and i

    if will_store || storage_job?
      items = vehicle_inventory_items(false)
      # inventory item was already created, we should make the invoice estimate
      # TODO: probably check if they're all released or deleted
      released if items[0].released_at?
    end
  end

  def log_transition
    Rails.logger.debug "SM: changing #{self}(#{id}) from #{aasm.from_state} to #{aasm.to_state} (event: #{aasm.current_event})"
    status_change(aasm.to_state) if aasm.from_state != aasm.to_state
  end

  ######################################################################
  # Time Estimation Methods
  ######################################################################

  def add_sta(sta)
    self.sta.build(eba_type: TimeOfArrival::SWOOP_ESTIMATE, job: self, time: sta, source_application_id: RequestContext.current.source_application_id)
  end

  def add_eta(eta)
    self.eta.build(eba_type: TimeOfArrival::ESTIMATE, job: self, time: eta, source_application_id: RequestContext.current.source_application_id)
  end

  def add_fta(fta)
    self.fta.build(eba_type: TimeOfArrival::FLEET, job: self, time: fta, source_application_id: RequestContext.current.source_application_id)
  end

  def add_pta(time)
    # If there is no bid, go ahead and update this with the PTA, this allows
    # the front nd to always send a single thing.
    return bid(time) if !bta || !bta.time
    pta.build(eba_type: TimeOfArrival::PARTNER, job: self, time: time, source_application_id: RequestContext.current.source_application_id)
  end

  def update_bid(bta)
    # if self.bta
    #   self.bta.time=bta
    # end
    add_pta(bta)
  end

  def bid(bta)
    build_bta(eba_type: TimeOfArrival::BID, job: self, time: bta, source_application_id: RequestContext.current.source_application_id)
  end

  def submitted_bid
    partner_accepted
  end

  def toa_hash
    hash = {}
    if bta
      hash['bta'] = bta.time
      hash['pta'] = bta.time
    else
      hash['bta'] = nil
    end
    hash['sta'] = (sta.last.time unless sta.empty?)
    hash['eta'] = (eta.last.time unless eta.empty?)
    hash['fta'] = (fta.last.time unless fta.empty?)

    hash['pta'] = pta.present? ? pta.last.time : hash['pta']

    hash
  end

  def eta_hash
    {
      original: original_toa&.time,
      latest: latest_pta.try(:time),
      live: latest_eta.try(:time),
      actual: latest_ata.try(:time),
      count: time_of_arrivals_non_live.length,
    }
  end

  def current_eta_time
    latest_eta&.time || latest_pta&.time
  end

  def current_job_status_id
    JobStatus::NAME_MAP[human_status.to_sym]
  end

  def original_eta_to_pickup
    time_of_arrivals_non_live.order(:created_at).first.time
  end

  def orig_eta_mins
    toa = time_of_arrivals_non_live.order(:created_at).first
    ((toa.time - toa.created_at) / 60.0).round if toa
  end

  def extended_eta_mins
    toas = time_of_arrivals_non_live.order(:created_at)
    ((toas.last.time - toas.first.created_at) / 60.0).round if toas.count > 1
  end

  def pickup_eta
    case current_job_status_id
    when JobStatus::ACCEPTED, JobStatus::DISPATCHED, JobStatus::ETA_EXTENDED
      current_eta_time
    when JobStatus::EN_ROUTE
      current_eta_time || (status_time(JobStatus::EN_ROUTE) + mins_ab.minutes)
    end
  end

  def dropoff_eta
    return unless drop_location.try(:has_coords?)

    case current_job_status_id
    when JobStatus::ACCEPTED, JobStatus::DISPATCHED, JobStatus::ETA_EXTENDED
      if current_eta_time.present? && mins_bc.present?
        current_eta_time + Estimate::MINS_ON_SITE + mins_bc.minutes
      end
    when JobStatus::EN_ROUTE
      if current_eta_time.present? && mins_bc.present?
        current_eta_time + Estimate::MINS_ON_SITE + mins_bc.minutes
      elsif mins_ab.present? && mins_bc.present?
        status_time(JobStatus::EN_ROUTE) + mins_ab.minutes + Estimate::MINS_ON_SITE + mins_bc.minutes
      end
    when JobStatus::ON_SITE
      if mins_bc.present?
        status_time(JobStatus::ON_SITE) + Estimate::MINS_ON_SITE + mins_bc.minutes
      end
    when JobStatus::TOWING
      if mins_bc.present?
        status_time(JobStatus::TOWING) + mins_bc.minutes
      end
    end
  end

  def add_time_of_arrival(time, user, company)
    toa = time_of_arrivals.build(time: time, user: user)
    toa.eba_type_for(company)
    toa
  end

  def has_non_live_eta?
    time_of_arrivals.non_live.present?
  end

  def has_signatures_or_images?
    (release_signatures.length > 0) || (pickup_signatures.length > 0) || (dropoff_signatures.length > 0) || (release_images.length > 0) || (pickup_images.length > 0) || (dropoff_images.length > 0) || (images.length > 0)
  end

  def stub_time_of_arrival
    TimeOfArrival.create!(job: self, lat: drop_location.lat, lng: drop_location.lng, time: Date.new, eba_type: 1, source_application_id: RequestContext.current.source_application_id)
  end

  def original_eta_mins
    bta = self.bta
    ((bta.time - bta.created_at) / 60.0).round if bta
  end

  # This is a product requirement, supposed to match the ETA in "Fleet Managed ETA Variance Report"
  def frontend_eta_mins
    estimate = sta.present? ? sta.last : nil
    return original_eta_mins unless estimate
    if bta.created_at > estimate.created_at
      return original_eta_mins
    end

    return ((estimate.time - estimate.created_at) / 60.0).round if estimate
  end

  # This is a product requirement, supposed to match the ETA in "Fleet Managed ETA Variance Report"
  def frontend_ata_mins
    estimate = sta.present? ? sta.last : nil
    return original_eta_mins unless estimate
    if bta.created_at > estimate.created_at
      return original_eta_mins
    end

    return ((estimate.time - estimate.created_at) / 60.0).round if estimate
  end

  def eta_mins
    estimate = pta.present? ? pta.last : bta
    ((estimate.time - estimate.created_at) / 60.0).round if estimate
  end

  def sta_mins
    estimate = sta.present? ? sta.last : nil
    return eta_mins unless estimate

    ((estimate.time - estimate.created_at) / 60.0).round if estimate
  end

  def ata_mins
    on_site = ajs_by_status_id(JobStatus::ON_SITE)
    bta = self.bta
    ((on_site.last_set_dttm - bta.created_at) / 60.0).round if on_site && bta
  end

  def latest_toa
    TimeOfArrival.where(job: self).order(created_at: :desc).first
  end

  def set_time_of_arrival(toa)
    Rails.logger.debug "handle @toa called with #{toa}"
    if toa
      add_sta(toa[:sta]) if toa[:sta]
      update_bid(toa[:bta]) if toa[:bta]
      add_pta(toa[:pta]) if toa[:pta]
      add_fta(toa[:fta]) if toa[:fta]
    end
  end

  def adjust_times(origin, destination, use_last_set)
    origin_time = nil
    if origin.adjusted_dttm
      # logger.debug 'using origin adjusted'
      origin_time = origin.adjusted_dttm
    elsif use_last_set
      # logger.debug 'using origin last_set'
      origin_time = origin.last_set_dttm
    end
    destination_time = nil
    if destination.adjusted_dttm
      # logger.debug 'using destination adjusted'
      destination_time = destination.adjusted_dttm
    elsif use_last_set
      # logger.debug 'using destination last_set'
      destination_time = destination.last_set_dttm
    end
    [origin_time, destination_time]
  end

  def mins_leg(leg, origin, destination, use_last_set)
    # logger.debug "on leg #{leg}"
    if origin && destination
      origin_time, destination_time = adjust_times(origin, destination, use_last_set)
      if origin_time && destination_time
        delta = ((destination_time - origin_time) / 60.0).round
        logger.debug "#{destination_time} - #{origin_time}: #{delta}"
        return delta
      else
        if estimate
          est = estimate.mins(leg)
          # logger.debug "1 Using est #{est}"
          return est
        end
      end
    else
      if estimate
        # logger.debug "2 Using est instance #{self.estimate.inspect}"
        est = estimate.mins(leg)
        # logger.debug "2 Using est #{est}"
        est
      end
    end
  end

  def mins_ab
    origin = ajs_by_status_id(JobStatus::EN_ROUTE)
    destination = ajs_by_status_id(JobStatus::ON_SITE)
    mins_leg(:ab, origin, destination, false)
  end

  def mins_bc
    towing = ajs_by_status_id(JobStatus::TOWING)
    destination = ajs_by_status_id(JobStatus::TOW_DESTINATION)
    mins_leg(:bc, towing, destination, true)
  end

  def mins_ca
    mins_leg(:ca, nil, nil, false)
  end

  def mins_ba
    mins_leg(:ba, nil, nil, false)
  end

  def mins_on_site
    origin = ajs_by_status_id(JobStatus::ON_SITE)
    destination = ajs_by_status_id(JobStatus::TOWING)
    if origin && destination
      origin_time, destination_time = adjust_times(origin, destination, true)
      if origin_time && destination_time
        ((destination_time - origin_time) / 60.0).round
      else
        20
      end
    else
      20
    end
  end

  def mins_at_dropoff
    origin = ajs_by_status_id(JobStatus::TOW_DESTINATION)
    destination = ajs_by_status_id(JobStatus::COMPLETED)
    if origin && destination
      origin_time, destination_time = adjust_times(origin, destination, true)
      if origin_time && destination_time
        ((destination_time - origin_time) / 60.0).round
      else
        20
      end
    else
      20
    end
  end

  def mins_p2p
    if drop_location
      ab = mins_ab
      on_site = mins_on_site
      at_dropoff = mins_at_dropoff
      bc = mins_bc
      ca = mins_ca
      ab + on_site + bc + at_dropoff + ca if ab && bc && ca
    else
      ab = mins_ab
      on_site = mins_on_site
      ba = mins_ba
      mins_ab + mins_on_site + mins_ba if ab && on_site && ba
    end
  end

  def mins_p2p_description
    if drop_location
      "(#{mins_ab}+#{mins_on_site}+#{mins_bc}+#{mins_ca})"
    else
      "#{mins_ab}+#{mins_on_site}+#{mins_ba}"
    end
  end

  ######################################################################
  # END Time Estimation Methods
  ######################################################################

  def before_moving_to_pending
    driver.vehicle = driver.vehicle.becomes!(StrandedVehicle)

    Rails.logger.debug "SM: arter original in call start #{status}, #{audit_job_statuses.inspect}"
    status_change(:created) # HEADS UP! This saves the job in case it's not saved yet!
    Rails.logger.debug 'SM: arter original in call b4 change'

    unless driver.user
      err = 'Job submitted with no customer information'
      logger.error err
      raise ArgumentError, err
    end

    unless service_code
      err = "Job submitted with no service_code"
      logger.error err
      raise ArgumentError, err
    end

    set_service_location_user

    if partner_job? && !account
      self.account = Account.find_or_create_cash_account(fleet_company)
    end

    Rails.logger.debug 'SM: arter original in call b4 driver'
    Rails.logger.debug 'SM: arter original in call end'
  end

  def set_service_location_user
    service_location&.user = driver.user
    drop_location&.user = driver.user
  end

  def to_pending_copy
    Rails.logger.debug 'SM: to_pending_copy'
    set_service_location_user
  end

  def customized_dispatcher?
    (fleet_job? && user_id != fleet_dispatcher_id) ||
      (partner_job? && user_id != partner_dispatcher_id)
  end

  def fleet_job?
    fleet_company.fleet?
  end

  def partner_job?
    fleet_company.rescue?
  end

  def fleet_in_house_job?
    is_a?(FleetInHouseJob)
  end

  def fleet_managed_job?
    is_a?(FleetManagedJob)
  end

  def fleet_motor_club_job?
    is_a?(FleetMotorClubJob)
  end

  def assigned_to_partner?
    rescue_company_id != nil
  end

  def fleet_manual?
    fleet_manual
  end

  def not_fleet_manual?
    !fleet_manual?
  end

  def self_assigned?
    fleet_company_id == rescue_company_id && !fleet_company_id.nil?
  end

  # Fleet managed and most in house jobs have restrictions requiring an approval
  # step before it can be marked as paid.
  def invoice_restrictions?
    return false if self_assigned?
    return false if fleet_motor_club_job?
    return false if fleet_in_house_job? && fleet_company.has_feature?(Feature::DISABLE_INVOICE_RESTRICTIONS)
    true
  end

  def send_get_location_necessary?
    get_location_sms && (get_location_attempts == 0)
  end

  def not_assigned_to_live_partner?
    rescue_company_id.nil? || (rescue_company && !rescue_company.live)
  end

  def slack_after_create
    log :debug, "slack_after_create called", job: id

    slack_notification(swoop_summary_name.to_s, preface_id: false, tags: [:dispatch, :create])
    true
  end

  def last_touched_by_company
    last_touched_by.company.try(:name) if last_touched_by
  end

  def slack_after_update
    log :debug, "slack_after_update called #{saved_changes}", job: id

    if !fleet_motor_club_job? && saved_changes['rescue_company_id'] && (rescue_company_id != fleet_company_id)
      resc_name = rescue_company ? rescue_company.name : 'Unassigned'
      slack_notification("#{last_touched_by_company} assigned to #{resc_name}", tags: [:dispatch, :assigned])
    end

    if saved_change_to_status?
      old_status = status_before_last_save
      new_status = status
      if fleet_managed_job? || fleet_motor_club_job? ||
          ((fleet_company.name == 'Tesla') &&
           ((new_status == Job::STATUS_ACCEPTED) || (new_status == Job::STATUS_REJECTED)))

        case new_status
        when Job::STATUS_COMPLETED, Job::STATUS_STORED, Job::STATUS_ACCEPTED, Job::STATUS_REJECTED, Job::STATUS_ONSITE, Job::STATUS_CANCELED, Job::STATUS_ENROUTE, Job::STATUS_TOWING, Job::STATUS_TOWDESTINATION, Job::STATUS_EXPIRED, 'Job::REASSIGNED', Job::STATUS_RELEASED, Job::STATUS_GOA
          slack_notification("#{last_touched_by_company} #{new_status}", tags: [:dispatch, "status-#{new_status}"])
        end

        case old_status
        when Job::STATUS_ACCEPTED
          slack_notification("#{last_touched_by_company} dispatched (-> #{human_status})", tags: [:dispatch, :accepted])
        end

      end
    end
  end

  def set_uuid
    self.uuid = SecureRandom.hex(5) unless uuid
  end

  def valid_service_alias?
    service_alias.nil? || service_alias.service_code_id == service_code_id
  end

  def invoices
    Invoice.all_active_by_job(self)
  end

  def draft_or_predraft?
    status == Job::STATUS_DRAFT || status == Job::STATUS_PREDRAFT
  end

  def done?
    self.class.closed_states.include?(human_status)
  end

  def goa?
    human_status == JobStatus::NAMES[:GOA]
  end

  def remove_service_alias
    self.service_alias = nil
  end

  def publish_location
    rescue_vehicle&.base_publish('refresh')
  end

  def publish_customer_websocket(event, data = {})
    # Forces it to goto event_UUID which track tech is listening to
    begin
      BaseModel.publish('event_' + channel_name, {
        target: { id: id, status: human_status },
        event: event,
        uuid: uuid,
        class: 'CustomerJob',
      }.merge(data || {}).to_json)
    rescue => e
      Rails.logger.warn "Unable to publish to event_#{channel_name}"\
        "in :publish_customer_websocket (#{e.message})"
    end
    true
  end

  def send_status_emails
    log :debug, "#send_status_emails called, status #{status}", job: id

    unless @status_email_enqueued
      selector = JobStatusEmailSelector.new(self)

      log :debug, "#send_status_emails, chosen selector is #{selector.class.name}, emailable: #{selector.emailable?}", job: id

      if selector.emailable?
        Delayed::Job.enqueue JobStatusEmailSender.new(id, selector.email_class_name)

        @status_email_enqueued = true
      end
    end
  end

  def send_eta_extension_email(recipients)
    Delayed::Job.enqueue JobStatusEmail::EtaExtended.new(job_id: id, user_id: user_id, validate: false, recipients: recipients)
  end

  def dispatch_email_if_live_and_open
    logger.debug "DISPATCH(#{id}): dispatch_email_if_live_and_open called"
    if !fleet_manual
      if rescue_company.dispatch_email
        Delayed::Job.enqueue EmailDispatch.new(id, rescue_company.id)
      else
        logger.warn "DISPATCH(#{id}): No dispatch email found for #{rescue_company}(#{rescue_company.id})"
      end
    else
      logger.debug "DISPATCH(#{id}): dispatch_email_if_live_and_open not dispatching to fleet_manual job"
    end
  end

  def send_swoop_alert
    log :debug, "SYSTEM_ALERT send_swoop_alert, status: #{status}", job: id

    if (status != Job::STATUS_UNASSIGNED) && (!parent || (parent.status != Job::STATUS_ASSIGNED)) # hasn't changed to rejected yet ....
      if cant_auto_assign? # Auto assign handled in StartAuctionService
        system_alert_swoop_if_necessary
      end
    end
  end

  def system_alert_swoop_if_necessary(check_if_swoop_created_job = true)
    log :debug, "SYSTEM_ALERT send_alert_swoop_if_necesary", job: id

    if can_system_alert_swoop?(check_if_swoop_created_job)
      sms_message = "Job Created  - #{swoop_summary_name}"
      recipients = SystemEmailRecipientCalculator.new(self).calculate

      recipients.each do |root|
        log :debug, "SYSTEM_ALERT Queueing SMS #{root.full_name}:#{root.phone}", job: id
        Job.delay.send_sms(root.phone, sms_message, user_id: root.id, job_id: id, type: 'JobAuditSms')

        if !scheduled?
          log :debug, "SYSTEM_ALERT Queueing phone #{root.full_name}:#{root.phone}", job: id

          Job.delay.new_job_call_swoop_agents(root.phone, id)
        else
          log :debug, "SYSTEM_ALERT Not queueing phone because job is scheduled #{root.full_name}:#{root.phone}", job: id
        end

        log :debug,
            "SYSTEM_ALERT Queueing SendSwoopDispatchersAlertIfNoRootDispatcher to run in " \
            "#{SendSwoopDispatchersAlertIfNoRootDispatcher::FREQUENCY_TO_ALERT_SWOOP_DISPATCHERS_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET} seconds", job: id

        # Commenting out as per https://swoopme.atlassian.net/browse/ENG-5454 (no need to repeat calls) - 19.04.2018
        # TODO left here for now in case we need to reactivate. Can be deleted when we consider it's really not needed.
        #
        # logger.debug(
        #   "JOB(#{id}) SYSTEM_ALERT Queueing SendSwoopDispatchersAlertIfNoRootDispatcher to run in " +
        #   "#{SendSwoopDispatchersAlertIfNoRootDispatcher::FREQUENCY_TO_ALERT_SWOOP_DISPATCHERS_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET} seconds"
        # )

        # Delayed::Job.enqueue(
        #   SendSwoopDispatchersAlertIfNoRootDispatcher.new(id),
        #   run_at: SendSwoopDispatchersAlertIfNoRootDispatcher::FREQUENCY_TO_ALERT_SWOOP_DISPATCHERS_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET.from_now
        # )

        # TODO code is ready but commented out as product will work more on it.
        # @see https://swoopme.atlassian.net/browse/ENG-4443
        #
        # logger.debug(
        #   "JOB(#{id}) SYSTEM_ALERT Queueing SendSwoopRootAlertIfNoRootDispatcher to run in " +
        #   "#{SendSwoopRootAlertIfNoRootDispatcher::FREQUENCY_TO_ALERT_SWOOP_ROOT_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET} seconds"
        # )
        #
        # Delayed::Job.enqueue(
        #   SendSwoopRootAlertIfNoRootDispatcher.new(id),
        #   run_at: SendSwoopRootAlertIfNoRootDispatcher::FREQUENCY_TO_ALERT_SWOOP_ROOT_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET.from_now
        # )
      end
    else
      swoop_originated = originating_company == Company.swoop

      log :debug, " SYSTEM_ALERT Not sending. fleet_managed_job:#{fleet_managed_job?}, swoop_originated:#{swoop_originated}, is_load_test:#{fleet_company.is_load?}", job: id
    end
  end

  def can_system_alert_swoop?(check_if_swoop_created_job = true)
    swoop_originated = check_if_swoop_created_job && (originating_company == Company.swoop)

    Rails.logger.debug(
      "JOB#{id} can_system_alert_swoop? check_if_swoop_created_job:#{check_if_swoop_created_job} " \
      "swoop_originated:#{swoop_originated}"
    )

    !fleet_company.is_load? && fleet_managed_job? && !swoop_originated
  end

  def schedule_swoop_alert_if_fleet_managed_job_not_dispatched
    if can_schedule_swoop_alert_if_fleet_managed_job_not_dispatched?
      log :debug, "SYSTEM_ALERT Queueing SendSwoopAlertIfFleetManagedJobNotDispatched to run in " \
        "#{SendSwoopAlertIfFleetManagedJobNotDispatched::FREQUENCY_TO_ALERT_SWOOP_IF_JOB_NOT_DISPATCHED} seconds",
          job: id

      Delayed::Job.enqueue(
        SendSwoopAlertIfFleetManagedJobNotDispatched.new(id),
        run_at: SendSwoopAlertIfFleetManagedJobNotDispatched::FREQUENCY_TO_ALERT_SWOOP_IF_JOB_NOT_DISPATCHED.from_now
      )
    end
  end

  def can_schedule_swoop_alert_if_fleet_managed_job_not_dispatched?
    can_system_alert_swoop? && !assigned_to_partner?
  end

  def send_cancelled_alert_swoop
    logger.debug("ALERTS(#{id}) send_cancelled_alert_swoop called")
    unless fleet_company.is_load?
      unless fleet_company.fleet_in_house?
        recipient_calc = SystemEmailRecipientCalculator.new(self)
        recipients = recipient_calc.calculate
        recipients.each do |root|
          logger.debug('Sending SMS')
          Job.delay.send_sms(root.phone, "Job Cancelled - #{summary_name}", user_id: root.id, job_id: id, type: 'JobAuditSms')
        end
      end
    end
  end

  def closed
    [CANCELED, COMPLETED, GOA].include?(human_status)
  end

  def can_partner_manage_photos?
    false
  end

  def self.closed_states
    [REJECTED, REASSIGNED, STORED, CANCELED, COMPLETED, RELEASED, GOA, EXPIRED]
  end

  # reflects active tab in ui
  def self.active_states
    [
      PENDING, UNASSIGNED, ASSIGNED, ACCEPTED, SUBMITTED, DISPATCHED,
      ENROUTE, ONSITE, TOWING, TOWDESTINATION, AUTO_ASSIGNING,
    ]
  end

  # TODO Job.active_and_draft_states is a hack to keep retrocompatibility with legacy code that relied
  # on Job.active_states when it contained 'draft'.
  # This and all spots that use it should be removed by https://swoopme.atlassian.net/browse/ENG-9905
  def self.active_and_draft_states
    [DRAFT] + active_states
  end

  def self.draft_states
    [
      PREDRAFT, DRAFT,
    ]
  end

  def self.map_states
    [
      PENDING, ASSIGNED, ACCEPTED, SUBMITTED, DISPATCHED,
      ENROUTE, ONSITE, TOWING, TOWDESTINATION, AUTO_ASSIGNING,
    ]
  end

  def self.done_states
    closed_states
  end

  def self.invoice_states
    [CANCELED, COMPLETED, RELEASED, GOA]
  end

  def self.fleet_no_publish_states
    [CANCELED, PENDING, COMPLETED, DISPATCHED, GOA]
  end

  # for which job states do we show rescue vehicle information?
  def self.fleet_show_rescue_vehicle_states
    [ENROUTE, ONSITE, TOWING, TOWDESTINATION]
  end

  def self.enroute_or_later
    [ENROUTE, ONSITE, TOWING, TOWDESTINATION, COMPLETED]
  end

  def fleet_show_rescue_vehicle?
    self.class.fleet_show_rescue_vehicle_states.include? human_status
  end

  def add_if_not_null(attribs, hash)
    attribs.each do |attrib|
      hash[attrib.humanize] = '✓' if !self[attrib].nil? && self[attrib]
    end
  end

  def question_attributes
    args = {}

    question_results.each do |result|
      if result.answer_name
        args[result.question_name] = { question_key: result.question.name, answer: result.answer_name }
      end
    end

    args
  end

  def human_created(tz)
    human_localtime(created_at, tz)
  end

  def human_scheduled(tz)
    human_localtime(scheduled_for, tz)
  end

  def human_customer_phone
    customer.phone
  end

  def human_dispatcher_phone; end

  def assign_rescue_company(site)
    service = JobAssignRescueCompanyService.new(self, site)
    service.call
  end

  def self.reminder_call(job_id)
    job = Job.find(job_id)
    log :debug, "Reminder #{job.status}", alerts: job_id

    if job.assigned?
      phone = job.site_or_rescue_company_phone
      Job.call_with_new_job_if_allowed(job.rescue_company, job.id, job.site ? job.site_id : nil, phone)
    else
      logger.debug 'ALERTS: All Good'
    end
  end

  def self.call_with_new_job_if_allowed(company, job_id, site_id, phone)
    Job.find(job_id)
    site = Site.find(site_id) unless site_id.nil?
    log :debug, "call_with_new_job_if_allowed: Call requested for job: #{job_id}, site: #{site_id}, company: #{company.name}, phone: #{phone} with Setting::DISABLE_JOB_ALERTS: #{Setting.get_setting(company, Setting::DISABLE_JOB_ALERTS).inspect}, Site Open For Business? #{site&.open_for_business?}", alerts: job_id
    if !Setting.get_setting(company, Setting::DISABLE_JOB_ALERTS) && (!site.nil? ? site.open_for_business? : true)
      log :debug, "call_with_new_job_if_allowed: Sending New Job Call to company:#{company.name}, phone: #{phone}", alerts: job_id
      Job.new_job_call(phone)
    else
      log :debug, "call_with_new_job_if_allowed: Disabled New Job to company:#{company.name}, phone: #{phone}", alerts: job_id
    end
  end

  def self.new_auction_call_if_enabled(company, job_id, phone)
    if !Setting.get_setting(company, Setting::DISABLE_JOB_ALERTS)
      log :debug, "new_auction_call_if_enabled: Sending New Auction Call to company:#{company.name}, phone: #{phone}", alerts: job_id
      Job.new_auction_call(phone)
    else
      log :debug, "new_auction_call_if_enabled: Disabled New Auction to company:#{company.name}, phone: #{phone}", alerts: job_id
    end
  end

  def self.won_auction_call_if_enabled(company, job_id, phone)
    if !Setting.get_setting(company, Setting::DISABLE_JOB_ALERTS)
      log :debug, "won_auction_call_if_enabled: Sending Won Auction Call to company:#{company.name}, phone: #{phone}", alerts: job_id
      Job.won_auction_call(phone)
    else
      log :debug, "won_auction_call_if_enabled: Disabled Won Auction to company:#{company.name}, phone: #{phone}", alerts: job_id
    end
  end

  ##
  # Retrieves the site or the rescue_company phone number for the job.
  # The site phone number takes precedence.
  #
  # Returns nil if job has no site.phone or rescue_company.
  #
  def site_or_rescue_company_phone
    return site.phone if site && site.phone.present?

    return rescue_company.phone if rescue_company

    nil
  end

  def site_or_rescue_company_phone_human
    phone = site_or_rescue_company_phone
    if phone
      Phonelib.parse(phone).national
    else
      nil
    end
  end

  def hack_create_ajs(status)
    js = JobStatus.find_by(name: status)
    audit_job_statuses.build(job_status: js, last_set_dttm: Time.now, company: fleet_company, user: last_touched_by)
  end

  def ajs_by_status(status_name)
    ajs_by_status_id(JobStatus.status_id(status_name))
  end

  def ajs_by_status_id(js_id)
    # TODO should this find the first or last status when there are duplicates? Right now it finds the first.
    audit_job_statuses_order_created.detect { |ajs| ajs.job_status_id == js_id }
  end

  def detect_ajs_by_status(status_name)
    js_id = JobStatus::NAME_MAP[Job.statuses[status_name].to_sym]
    audit_job_statuses.detect do |ajs|
      ajs.job_status_id == js_id
    end
  end

  def status_time(status)
    status = JobStatus::CREATED if status == JobStatus::UNASSIGNED
    ajs = ajs_by_status_id(status)
    ajs&.adjusted_created_at
  end

  def status_change(new_status)
    ajs = detect_ajs_by_status(new_status)

    JobService::StatusChange.call(job: self,
                                  new_status: new_status,
                                  ajs: ajs)
  end

  # XXX moved to service
  # def set_partner_department
  #   if rescue_company.has_feature(Feature::DEPARTMENTS)
  #     self.partner_department_id = account.department_id if account.present?
  #   end
  # end

  def processVehicleChange(old_vehicle); end

  def get_location_url
    uuid = SecureRandom.hex(5)
    ["#{ENV['SITE_URL']}/txt/#{uuid}", uuid]
  end

  def get_track_technician_url
    ["#{ENV['SITE_URL']}/txt/#{uuid}", uuid]
  end

  def sms_phone
    if send_sms_to_pickup
      [pickup_contact.phone, pickup_contact]
    else
      # Driver in a vehicle (customer)
      [driver.user.phone, driver.user]
    end
  end

  ############################################
  # New Customer Notifications
  ############################################

  def send_eta_notification(revision: false, eba_type: nil)
    # This is disabled
    return true unless text_eta_updates
    # Dont revise if we are sending track techniction
    return true if revision && track_technician_sent_at.present?
    # Since we now have send_confirmation_sms that handle ETA up front
    # if we dont have a rescue_company_assigned dont send out a message
    return true if rescue_company_id.blank?

    klass, metric = if revision
                      # Eta Revision from partner
                      [Sms::ReviseEta, 'sms_revise_eta']
                    elsif parent_id && !revision
                      # We have been reassigned and we are not revising an ETA
                      [Sms::Reassign, 'sms_reassign_eta']
                    else
                      # This will now only be sent out
                      # if there is no ETA entered.
                      [Sms::Eta, 'sms_eta']
                    end

    mins = if eba_type.eql?(TimeOfArrival::SWOOP_ESTIMATE)
             sta_mins
           else
             eta_mins
           end

    logger.debug "CONSUMER_SMS(#{id}): Sending SMS #{klass} - #{metric}"

    klass.send_notification(self, eta: mins)
    track(Metric::SENT, metric, 1)
  end

  def send_confirmation_sms
    return unless text_eta_updates # This is disabled

    klass, metric, eta = if !scheduled? && get_location_sms
                           # IF we are not scheduled and we need to send location
                           [Sms::ConfirmLocation, 'sms_cofirm_location', nil]
                         elsif scheduled? && get_location_sms
                           # We are scheduled and we need to send location
                           [Sms::ConfirmLocationSched, 'sms_confirm_location_s', nil]
                         elsif scheduled?
                           # We are scheduled and no location SMS
                           [Sms::ConfirmationSched, 'sms_confirmation_sched', nil]
                         elsif has_non_live_eta?
                           # We have an ETA entered Already
                           eta = time_of_arrivals.non_live.last
                           [Sms::ConfirmationEta, 'sms_confirmation_eta', eta.duration]
                         else
                           # Non of the above, send generic confirmation SMS
                           [Sms::Confirmation, 'sms_confirmation', nil]
                         end

    logger.debug "CONSUMER_SMS(#{id}): Sending SMS #{klass} - #{metric}"

    klass.send_notification(self, eta: eta)
    track(Metric::SENT, metric, 1)
  end

  def support_phone
    if fleet_company.phone.present?
      fleet_company.phone
    elsif site && site.phone.present?
      site.phone
    else
      DEFAULT_SUPPORT_PHONE
    end
  end

  #   PDW: New Requirements (not Implemented) in ENG-1662
  #   If the SMS feature is ON for Fleet:
  #     Text should be SENT branded as the fleet
  #     show it in the Edit Job form settings for Partner
  #     Partner should not be able to edit it (checked checkbox should be locked/grayed out)
  #     Do not allow Partner to edit it
  #     show the event it in the job history
  #   If the SMS feature is ON BUT UNCHECKED FOR THAT JOB by Fleet:
  #     Text should NOT be sent
  #     show it in the Edit Job form settings for Partner
  #     Partner should not be able to edit it (empty checkbox should be locked/grayed out)
  #     Do not allow Partner to edit it
  #     no event in history since not sent
  #   If the SMS feature is OFF for Fleet but ON for Partner:
  #     Text should NOT be sent
  #     show it in the Edit Job form settings for Partner
  #     Partner should not be able to edit it (empty checkbox should be locked/grayed out)
  #     Do not allow Partner to edit it
  #     no event in history since not sent

  def send_get_location
    if fleet_company.has_feature(Feature::SMS_REQUEST_LOCATION)
      if get_location_sms && (get_location_attempts == 1) && [CREATED, PENDING, DISPATCHED, ENROUTE, AUTO_ASSIGNING].include?(human_status)
        send_location_audit_sms
      else
        logger.debug "CONSUMER_SMS(#{id}): Not sending get_location because state not in (CREATED,PENDING,DISPATCHED,ENROUTE,AUTO_ASSIGNING):#{human_status}), get_location_sms:(#{get_location_sms}) or sent count >1 (#{get_location_attempts})"
      end
    else
      logger.debug "CONSUMER_SMS(#{id}): fleet_company_id doesn't have SMS_REQUEST_LOCATION set"
    end
  end

  def send_location_audit_sms
    link, uuid = get_location_url
    phone, user = sms_phone
    company = _owner_company_name
    logger.debug "CONSUMER_SMS(#{id}): send location request via sms with link #{link} to #{phone}"
    if fleet_company && fleet_company.get_location_text && fleet_company.get_location_text.present?
      template = fleet_company.get_location_text
    else
      template = '%{company}: Please click the link to update your location. %{link} Thanks!'
    end
    body = template % { company: company, link: link }
    log :debug, "CONSUMER_SMS send_get_location LocationAuditSms #{template} #{body}", job: id
    Job.delay.send_sms(phone, body, { user_id: user.id, job_id: id, type: 'LocationAuditSms', uuid: uuid })
    slack_notification('Get Location SMS sent', tags: [:sms, :location])

    track(Metric::SENT, 'sms_get_location', 1)
  end

  # Renamed old tracking_link_sms -> partner_sms_track_driver
  def send_track_technician_sms
    updated = false
    if fleet_company.has_feature(Feature::SMS_TRACK_DRIVER)
      if sms_track_driver && !track_technician_sent_at
        if rescue_vehicle_id
          if rescue_vehicle.location_updated_at > 5.minute.ago
            link, uuid = get_track_technician_url
            phone, user = sms_phone
            company = _owner_company_name
            log :debug, "CONSUMER_SMS ETA: send track technican request via sms with link #{link} to #{phone}", job: id
            if fleet_company && fleet_company.track_technician_text && fleet_company.track_technician_text.present?
              template = fleet_company.track_technician_text
            else
              template = '%{company}: Your driver is en route. Click the link to see an ETA: %{link} Thanks.'
            end
            Job.delay.send_sms(phone, template % { company: company, link: link }, user_id: user.id, job_id: id, type: 'EtaAuditSms', uuid: uuid)
            updated = true

            slack_notification "Track ETA SMS sent #{EtaAuditSms.new(job: self).redirect_url}", tags: %w(sms track)

            track(Metric::SENT, 'sms_eta', 1)
          else
            logger.debug "CONSUMER_SMS(#{id}): ETA: Not sending because not updated in last 5 mins #{rescue_vehicle.inspect}"
          end
        else
          logger.debug "CONSUMER_SMS(#{id}): ETA: Not sending because no rescue truck assigned"
        end
      else
        logger.debug "CONSUMER_SMS(#{id}): ETA: Not sending - checked:#{sms_track_driver}  && !sent_at(#{track_technician_sent_at}"
      end
    else
      logger.debug "CONSUMER_SMS(#{id}): ETA: Not sending because fleet does not have this feature (SMS_TRACK_DRIVER)"
    end
    updated
  end

  def self.send_track_technician_sms_and_save(job_id)
    Job.transaction do
      job = Job.find(job_id)
      if job.send_track_technician_sms
        job.track_technician_sent_at = Time.now
        job.save!
      end
    end
  end

  # PDW: not called
  def send_updated_track_technician_sms
    self.track_technician_sent_at = Time.now
    link = get_track_technician_url
    phone, user = sms_phone
    company = _owner_company_name
    log :debug, "CONSUMER_SMS: send updated track technican request via sms with link #{link} to #{phone}", job: id
    template = '%{company}: Your driver has been updated. Click to see your new ETA: %{link}'
    Job.delay.send_sms(phone, template % { company: company, link: link }, user_id: user.id, job_id: id, uuid: uuid)
    track(Metric::SENT, 'sms_updated_eta', 1)
  end

  def send_review_sms
    Rails.logger.debug "CONSUMER_SMS #{id} send_review_sms called"
    if fleet_company.has_feature(Feature::SMS_REVIEWS) || fleet_company.has_feature(Feature::REVIEW_NPS_SURVEYS)
      if send_review?
        Rails.logger.debug "CONSUMER_SMS #{id} Enqueueing ReviewSMS for send at #{send_review_at}"
        ReviewSMS.perform_at(send_review_at, id)
      else
        Rails.logger.debug "CONSUMER_SMS #{id} NOT enqueueing ReviewSMS review_delay: #{review_delay}"
      end
    else
      Rails.logger.debug "CONSUMER_SMS #{id} Not sending ReviewSMS because originating company doesnt have SMS_REVIEWS feature"
    end
  end

  def send_review?
    !!send_review_at
  end

  def send_review_at
    now = Time.now.in_time_zone(timezone)
    case review_delay
    when Review::DELAY_COMPLETE, nil
      now
    when Review::DELAY_THREE_HOURS
      now + 3.hours
    when Review::DELAY_SEVEN_PM
      Utils.upcoming_time(now, 19)
    when Review::DELAY_NINE_AM
      Utils.upcoming_time(now, 9)
    end
  end

  def summary_name
    "#{id}#{summary_parent}: #{fleet_company.try(:name)} | #{service_code.try(:name)} | #{human_service_location_city_state}"
  end

  def summary_parent
    if parent_id
      "(#{parent_id})"
    else
      ''
    end
  end

  def swoop_summary_name
    if rescue_company && (rescue_company_id != fleet_company_id)
      "#{id}#{summary_parent} #{fleet_company.try(:name)} -> #{rescue_company.try(:name)} | #{service_code.try(:name)} | #{human_service_location_city_state}"
    else
      summary_name
    end
  end

  def summary_name_short
    "#{id}: #{fleet_company.try(:name)}"
  end

  def human_service_location_city_state
    sl = service_location
    if sl
      if sl.city
        if sl.state
          "#{sl.city}, #{sl.state}"
        else
          sl.city
        end
      else
        if sl.address
          components = sl.address.split(', ')[-2..-1]
          components&.join(', ')
        end
      end
    end
  end

  def self.schedule_all_rate_invoice_updates(account)
    Rails.logger.debug "Job.schedule_all_rate_invoice_updates (IGNORED via ENG-7797) #{account.name}"
  end

  def self.schedule_rate_change_invoice_updates(account, rate)
    logger.debug "INVOICE (IGNORED via ENG-5049) - schedule_rate_change_invoice_updates #{account.name} rate:#{rate.inspect}"
  end

  def self.schedule_rate_create_invoice_updates(account, rate)
    logger.debug "INVOICE (IGNORED via ENG-7797) schedule_rate_create_invoice_updates #{account.name} rate:#{rate.inspect}"
  end

  # DEPRECATED - doesnt seem to be used
  def fixup_locations_job
    if @job.service_location && !@job.service_location.job
      @job.service_location.job = @job
    end
    if @job.drop_location && !@job.drop_location.job
      @job.drop_location.job = @job
    end
  end

  # XXX looks like this code is not used anywhere
  def caclulate_distance(rp)
    rp.job_distance = RescueProvider::MAX_DISTANCE

    tower_location = if rp.site
                       rp.site.location
                     else
                       rp.provider.location
                     end

    if tower_location && tower_location.lat && tower_location.lng
      rp.job_distance = Geocoder::Calculations.distance_between(
        [tower_location.lat, tower_location.lng],
        [service_location.lat, service_location.lng],
        units: :mi
      )
    end

    rp
  end

  # Are we able to create an invoice for this job?
  def estimatable?
    !storage_job?
  end

  def allowed_to_create_invoice?
    rescue_company_id && site_id
  end

  # For the eta webclient uuid
  def channel_name
    uuid
  end

  def listeners
    ret = Set.new
    if status == "predraft"
      return ret
    end
    ret << fleet_company if fleet_company
    if rescue_company
      ret << rescue_company
      unless Job.fleet_no_publish_states.include? human_status
        # for the eta web client - maybe we send this notification to #customer
        # instead of the job object itself?
        ret << self
      end
    end
    if fleet_managed_job?
      ret << Company.swoop
    end
    if auction_live
      bids.each do |bid|
        ret << bid.company
      end
    end
    ret
  end

  def subscribers
    # start
    ret = listeners

    # remove ourself from this list
    ret.reject! { |s| s == self }

    # add rescue_driver if we have one
    (ret << rescue_driver) if rescue_driver.present?

    # add our customer if we have one
    (ret << customer) if customer.present?

    # and go through our subscribers looking for RescueCompanies - if we
    # find one add all the dispatchers from the company to our list of
    # subscribers
    ret = add_dispatchers ret

    # go through our subscribers looking for Companies and add any oauth_applications
    # they may have to our subscribers
    ret = add_applications ret

    ret
  end

  def auction_live
    auction && auction.status == Auction::Auction::LIVE
  end

  def render_to_hash
    controller = ActionController::Base.new
    controller.instance_variable_set(:@api_company, @api_company)
    JSON.parse(controller.render_to_string('api/v1/jobs/_job', locals: { job: self }))
  end

  def storage_rate
    code = ServiceCode.where(name: 'Storage').first
    Rate.inherit_type(
      rescue_company_id,
      account_id,
      site_id,
      issc_contractor_id,
      code.id,
      invoice_vehicle_category_id,
      'StorageRate'
    )
  end

  def storage_job?
    service_code == ServiceCode.storage
  end

  def audit_job_statuses_as_hash
    audit_job_statuses_order_created
      .reject { |ajs| JobStatus::HIDE_FROM_HISTORY.include?(ajs.job_status_id) }
      .map { |ajs| [ajs.status_name, ajs] }
      .to_h
  end

  def rescue_vehicle_name
    rescue_vehicle.try(:name)
  end

  def rescue_driver_name
    rescue_driver.try(:name)
  end

  def service_name
    service_code.try(:name)
  end

  def timezone
    tz = 'America/Los_Angeles'
    if site && site.tz?
      tz = site.tz
    end
    tz
  end

  def invoice_history_array
    statuses = [
      JobStatus::EN_ROUTE,
      JobStatus::ON_SITE,
      JobStatus::TOWING,
      JobStatus::TOW_DESTINATION,
      JobStatus::COMPLETED,
      JobStatus::STORED,
      JobStatus::RELEASED,
    ]
    invoice_history = []
    statuses.each do |status|
      ajs = ajs_by_status_id(status)
      if ajs
        invoice_history << ajs
      end
    end

    invoice_history.sort_by! { |ajs| ajs.adjusted_created_at }

    if add_return_to_hq_to_history?
      log :debug, "#invoice_history_array Will add Return to HQ to History", job: id

      invoice_history << return_to_hq_history
    end

    invoice_history
  end

  def add_return_to_hq_to_history?
    return false if audit_job_status_stored_or_completed.nil?
    return false if mins_ca.nil?

    invoice&.rate_type == HoursP2PRate.name
  end

  def return_to_hq_history
    returned_to_hq_calc = audit_job_status_stored_or_completed.adjusted_created_at + mins_ca.minutes

    OpenStruct.new(
      id: 'returned_to_hq',
      created_at: returned_to_hq_calc,
      adjusted_created_at: returned_to_hq_calc, # used on invoice.pdf.erb
      name: JobStatus::RETURNED_TO_HQ,
      status_name: JobStatus::RETURNED_TO_HQ, # used on invoice.pdf.erb
      type: 'ReturnedToHQ',
      last_set_dttm: returned_to_hq_calc
    )
  end

  def job_history_hash(viewing_company = nil, recursion_count = 1)
    GenerateJobHistory.call(
      job: self,
      viewing_company: viewing_company,
      recursion_count: recursion_count
    ).results
  end

  def show_template
    'api/v1/jobs/job'
  end

  def partial_template
    'api/v1/jobs/_job'
  end

  def human_status
    Job.statuses[status]
  end

  #  def child_jobs
  #    return Job.where(deleted_at:nil, original_job_id:self.id)
  #  end

  def vehicle_inventory_items(active = true)
    if active
      InventoryItem.where(job_id: id, item_type: 'Vehicle', deleted_at: nil, released_at: nil)
    else
      InventoryItem.where(job_id: id, item_type: 'Vehicle')
    end
  end

  # PDW - replaced by storage
  #  def non_deleted_vehicle_inventory_items()
  #    return InventoryItem.where(job_id:self.id, item_type: "Vehicle", deleted_at: nil)
  #  end

  # This method needs to be called on jobs when they are marked GOA to update the
  # partner invoice since we don't want to pay the full service value if no service
  # was performed.
  def mark_invoice_goa
    raise IllegalStateError unless goa?

    # Update the existing partner invoice (or create one) using the GOA rate for the job.
    update_invoice

    # We're keeping and updating the partner's invoice (above), but we still want to cancel the customer's
    # invoice because the job is GOA and there's no customer any more.
    if fleet_customer_live_invoices.present?
      InvoiceService::Cancel::CancelCustomerInvoice.new(self).call
    end
  end

  def cancel_invoice
    Rails.logger.debug "Job::cancel_invoice called"
    InvoiceService::Cancel::CancelInvoiceForJob.new(self).call
  end

  def cancel_auction
    Auction::CancelAuctionService.new(auction.id).call if auction_live
  end

  #
  # Find the rate we're currently using to calculate this job's cost
  #
  # WARNING: "invoice.rate_type" is confusing because:
  # a) "invoice.rate_type" is both a database column and an instance method
  # b) the invoice table's rate_type column means "a user's override of the invoice's inferred rate type"
  # This confusion is being fixed in ENG-10688. Until then, exercise caution and avoid modifying this method. Ask @jerzy if you have questions.
  #
  def get_rate
    # Step 1: Determine if the user has manually overridden the rate type that the invoice should use to calculate
    user_overriden_invoice_rate_type = invoice.attributes["rate_type"] if invoice

    if goa?
      # Step 2: GOA job should be paid at the rate for the GOA service code.
      Rate.inherit(rescue_company_id,
                   account_id,
                   site_id,
                   issc_contractor_id,
                   ServiceCode.goa.id,
                   invoice_vehicle_category_id)
    elsif user_overriden_invoice_rate_type
      # Step 3.1: If the user has overridden the invoice's rate type, find a rate of that specific rate type for this job
      Rate.inherit_type(rescue_company_id,
                        account_id,
                        site_id,
                        issc_contractor_id,
                        service_code_id,
                        invoice_vehicle_category_id,
                        user_overriden_invoice_rate_type)
    else
      # Step 3.2: Otherwise, if the user has not overridden the invoice's rate type, find the rate for this job
      Rate.inherit(rescue_company_id,
                   account_id,
                   site_id,
                   issc_contractor_id,
                   service_code_id,
                   invoice_vehicle_category_id)
    end
  end

  def get_storage_rate
    Rate.inherit_type(
      rescue_company_id,
      account_id,
      site_id,
      issc_contractor_id,
      ServiceCode.storage.id,
      invoice_vehicle_category_id,
      'StorageRate'
    )
  end

  def new_blank_storage_rate
    StorageRate.new(company_id: rescue_company_id,
                    account_id: nil,
                    site_id: nil,
                    service_code_id: ServiceCode.storage.id,
                    vehicle_category_id: nil,
                    storage_daily: 0)
  end

  def self.get_storage_or_create_zero_storage_rate(job)
    storage_rate = job.get_storage_rate
    unless storage_rate
      Rails.logger.debug "INVOICE(#{job.id}) no Storage, generating 0 rate Storage for #{job.rescue_company.try(:name)}"
      storage_rate = job.new_blank_storage_rate
      storage_rate.save!
    end
    storage_rate
  end

  def fixup_locations
    service_location.job = self if service_location && !service_location.job
    drop_location.job = self if drop_location && !drop_location.job
  end

  def job_status_change(method_name)
    logger.debug 'action status update'
    logger.debug "calling method name #{method_name}"
    if respond_to? method_name
      public_send(method_name)
    else
      err = "Invalid state transition - #{method_name}"
      logger.error err
      # TODO - why don't we raise AASM::InvalidTransition here instead (which is what would
      # happen if we tried to call the invalid state transition directly?)
      raise ArgumentError, err
    end
  end

  def schedule_create_estimate
    if estimatable?
      log :debug, "CEB(#{id}) schedule_create_estimate", job: id
      CreateOrUpdateEstimateForJob.perform_async(id)
    end
  end

  def track(action, label, value = nil)
    Metric.create!(target: self, action: action, label: label, value: value)
    hash = convert_hash(JSON.parse(to_string))
    hash['value'] = value
    segment_track("API #{label} #{action}", hash)
  end

  # Alias so we dont need to call .class. over and over again
  def supports
    self.class.supports
  end

  ######################################################################
  #   Auto Assignment Methods
  ######################################################################

  def process_auto_assign
    log :debug, "process_auto_assign called with status(#{status})", job: id

    return true unless can_auto_assign?
    log :debug, "process_auto_assign can_auto_assign is true", job: id

    if @needs_auto_assign
      log :debug, "process_auto_assign @needs_auto_assign is true", job: id
      StartAuctionServiceWorker.perform_async(id)

      log :debug, "process_auto_assign setting @needs_auto_assign as false", job: id
      @needs_auto_assign = false
    end
  end

  def cant_auto_assign?
    !can_auto_assign?
  end

  # Is this job eligible for auctioning? In other words, can we hold an auction, accept
  # bids from tow operators, and automatically assign a job to the winner?
  def can_auto_assign?
    # Drafts can go from draft status straight to deleted, auto assign can't happen in this case
    return false if status == Job::STATUS_DRAFT || status == Job::STATUS_PREDRAFT || status == Job::STATUS_DELETED
    # It doesn't make sense to auction a job if it's fleet company is a RescueCompany or a SuperCompany
    return false unless fleet_company.instance_of? FleetCompany
    return false unless fleet_managed_job?
    return false if scheduled_for.present?
    return false unless supports[:auto_assign]
    return false if fleet_company.cant_auto_swoop_dispatch?
    return false if Auction::Auction::UNAUCTIONABLE_CLASS_TYPE_NAMES.include?(invoice_vehicle_category&.name)

    Auction::Auction::DISPATCHABLE_SERVICE_NAMES.include?(service_code.name)
  end

  def assigning_company
    raise 'Not supported please implement'
  end

  def fleet_name
    fleet_company.try(:name)
  end

  def fleet_site_name
    fleet_site.try(:name)
  end

  def slack_notification(message, tags: %w(default), preface_id: true)
    tgs = SlackChannel.tags_to_array(tags)
    tgs.unshift(fleet_name)

    localtime = Time.now.in_time_zone('America/Los_Angeles')
    hms = localtime.strftime('%H:%M:%S')
    msg = preface_id ? "#{hms} #{id} #{message}" : "#{hms} #{message}"
    SlackChannel.message(msg, tags: tgs, add_default: false)
  end

  # Create or Update the release status for a job
  # this - we wont update the job status as it just
  # updates the release status

  def create_or_update_release_status(release_date)
    return false unless release_date

    ajs = detect_ajs_by_status(Job::STATUS_RELEASED)
    if ajs
      ajs.deleted_at = nil
    else
      ajs = audit_job_statuses.build(
        job_status_id: JobStatus::NAME_MAP[Job::RELEASED.to_sym]
      )
    end
    ajs.last_set_dttm = release_date if ajs.new_record?
    ajs.adjusted_dttm = release_date unless ajs.new_record?
  end

  # Synchronously update the job invoice.
  def update_invoice
    CreateOrUpdateInvoice.new.perform(id)
  end

  def can_auto_respond?
    status == Job::STATUS_ASSIGNED && answer_by.present?
  end

  def time_at_destination
    ajs = ajs_by_status_id(JobStatus::ON_SITE) if service_code.is_service_location_code?
    ajs = ajs_by_status_id(JobStatus::TOW_DESTINATION) if has_drop_location_service_code?
    if ajs
      time = ajs.adjusted_dttm || ajs.last_set_dttm
    end
    Time.now - time if time
  end

  # Customize its RescueCompany instance name with its site.name if necessary.
  # It depends on the quantity of sites this RescueCompany contains.
  #
  # @see RescueCompany#customized_name_having
  #
  # @return nil if rescue_company.blank?
  # @return self.rescue_company customized name
  def customized_rescue_company_name
    return nil if rescue_company.blank?

    if rescue_company.class == FleetCompany || rescue_company.class == SuperCompany
      return rescue_company.name
    end

    rescue_company.customized_name_having(site)
  end

  def job_status_email_subject
    company = job_status_email_company_name
    service = service_code&.name

    subject = " - #{id}: #{company} | #{service}"

    if service_location
      city = service_location.city
      state = service_location.state
    elsif site && site.location
      city = site.location.city
      state = site.location.state
    end

    subject += " | #{city}, #{state}" unless city.blank? || state.blank?
    subject
  end

  def is_fleet_response?
    fleet_company_id == Company.fleet_response_id
  end

  def invoice_editable?
    false
  end

  SCHEDULED_FOR_REMINDER_TIME = 90.minutes

  def scheduled_for_over_reminder_time_away
    scheduled_for && (scheduled_for > Time.now + SCHEDULED_FOR_REMINDER_TIME)
  end

  def scheduled_for_reminder_time
    scheduled_for - SCHEDULED_FOR_REMINDER_TIME
  end

  def schedule_create_storage_invoice_batch
    log :debug, "schedule_create_storage_invoice_batch called", job: id
    CreateOrUpdateInvoice.perform_async(id) if storage_job?
  end

  def scheduled_system_alert_create
    if fleet_managed_job? && scheduled_for
      log :debug, " SCHEDULED_SYSTEM_ALERT scheduled_system_alert_create calling UpdateScheduledSystemAlert", job: id
      UpdateScheduledSystemAlert.perform_async(id)
    end
    true
  end

  def issc_contractor_id
    issc_dispatch_request&.issc&.contractorid
  end

  def invoices_by_viewer(company)
    return [] if !company
    if company.rescue?
      live_invoices.select { |invoice| rescue_company_id && rescue_company_id == invoice.sender_id }
    elsif company.fleet?
      live_invoices.select { |invoice| fleet_company_id && [Company.swoop_id, fleet_company_id].include?(invoice.sender_id) }
    elsif company.super?
      live_invoices
    end
  end

  # This is primarily used by RSC jobs for CANCEL or GOA cases
  def cancel_or_goa_status_reason
    job_status_reason_types.where(job_status_id: [JobStatus::CANCELED, JobStatus::GOA]).first
  end

  def draft_and_storage?
    human_status == DRAFT && service_code&.name == ServiceCode::STORAGE
  end

  def fleet_demo?
    fleet_company.demo?
  end

  def status_reason_types_by(job_status_id)
    API::Jobs::StatusReasonTypes.new(
      job: self,
      job_status_id: job_status_id,
    ).call
  end

  def status_reason_type_required?(job_status_id)
    status_reason_types_by(job_status_id).present?
  end

  def status_reasons
    status_reasons = job_status_reason_types.to_a

    # TODO this is temporary until we finish migrating ETAExplanation to JobStatusReasonType structure.
    # Follow up ticket: https://swoopme.atlassian.net/browse/ENG-8842
    if eta_explanation
      status_reasons << JobStatusReasonType.find_by(job_status_id: JobStatus::ACCEPTED, text: eta_explanation.text)

    # TODO this is temporary until we finish migrating JobRejectReasons to JobStatusReasonType structure.
    # Follow up ticket: https://swoopme.atlassian.net/browse/ENG-8845
    elsif reject_reason
      status_reasons << JobStatusReasonType.find_by(job_status_id: JobStatus::REJECTED, text: reject_reason.text)
    end

    status_reasons
  end

  def max_bidded_eta_without_explanation
    if issc_dispatch_request.present? &&
       fleet_company.present? &&
       fleet_company.issc_client_id.in?(Issc::USES_MAX_BIDDED_ETA_WITHOUT_EXPLANATION)
      issc_dispatch_request&.max_eta || Issc::DEFAULT_MAX_BIDDED_ETA_WITHOUT_EXPLANATION
    end
  end

  def self.safe_find(api_user, job_id)
    return Job.find(job_id) if api_user.company == Company.swoop
    Job.find_by!(id: job_id,
                 fleet_company: api_user.company)
  end

  def rsc?
    # Issc::RSC_SYSTEM is a symbol, hence the conversion
    issc_system == Issc::RSC_SYSTEM.to_s
  end

  def issc?
    # Issc::ISSC_SYSTEM is a sybol, hence the conversion
    issc_system == Issc::ISSC_SYSTEM.to_s
  end

  def can_request_more_time?
    (is_a?(::FleetMotorClubJob) &&
    assigned? &&
    rsc? &&
    !more_time_requested?).to_bool
  end

  def can_request_callback?
    (is_a?(::FleetMotorClubJob) &&
    assigned? &&
    (rsc? || issc?)).to_bool
  end

  # sometimes we want the account.name for a job which is still in an active
  # auction (and therefore doesn't have an associated account model). this hack
  # lets us predict the name of the account which would be used for the job -
  # the logic is the same as the logic JobAssignRescueCompanyService uses when
  # it calls CreateAccountForJobService but we don't want to actually create the
  # accounts here (rather, we create them if the partner company actually wins
  # the job).
  def account_name
    if account
      account&.name
    elsif fleet_in_house_job? || fleet_motor_club_job?
      fleet_company&.name
    elsif fleet_managed_job?
      Company.swoop.name
    else
      nil
    end
  end

  def update_digital_dispatcher_status
    # currently only defined on FleetMotorClubJobs
  end

  private

  def _owner_company_name
    fleet_company.try(:name)
  end

  def segment_track(event, props = nil)
    user = self.user
    if user
      hash = {
        user_id: user.to_ssid,
        event: event,
        properties: props,
      }
      Analytics.track(
        hash
      )
    end
  end

  # invoices recalculated on:
  # Service change
  # When Arrived at destination (via audit_job_status calling update_invoice)
  # Location change (via jobs_base_controller calling update_distance)
  # Priority Response Change
  # Rate change (Rate calls update invoice)
  # Site change

  def location_change
    log :debug, "location_change checking for service location_change #{saved_changes}", job: id

    location_changes = saved_changes.keys & ['drop_location_id', 'service_location_id']
    return true if location_changes.blank?

    log :debug, "location_change Change detected", job: id

    self.estimate_dirty = true

    # This indicates that an initial value existed for service location. Not
    # using 'saved_changes' here because the previous value is returning nil.
    if saved_changes['service_location_id'] &&
        saved_changes['service_location_id'][0].present?
      CreateHistoryItem.call(
        job: self,
        target: service_location,
        user: last_touched_by,
        event: HistoryItem::PICKUP_LOCATION_EDITED,
      )
    end

    # This indicates that an initial value existed for drop location. Not
    # using 'saved_changes' here because hte previous value is returning nil.
    if saved_changes['drop_location_id'] &&
        saved_changes['drop_location_id'][0].present?
      CreateHistoryItem.call(
        job: self,
        target: drop_location,
        user: last_touched_by,
        event: HistoryItem::DROP_OFF_LOCATION_EDITED,
      )
    end

    true
  end

  def service_change
    log :debug, "service_change checking for service change #{saved_changes}", job: id

    if invoice && saved_change_to_service_code_id?
      self.invoice_dirty = true

      CreateHistoryItem.call(
        job: self,
        target: self,
        event: HistoryItem::SERVICE_EDITED,
      )
    end

    true
  end

  def priority_response_change
    log :debug, "priority_response_change checking for priority_response change #{saved_changes}", job: id

    if invoice && saved_change_to_priority_response?
      self.invoice_dirty = true
    end
    true
  end

  def invoice_vehicle_category_change
    log :debug, "invoice_vehicle_category_change checking #{saved_changes}", job: id

    if saved_change_to_invoice_vehicle_category_id?
      log :debug, "invoice_vehicle_category change detected", job: id
      self.invoice_dirty = true
    end
    true
  end

  def customer_type_change
    log :debug, "customer_type_change checking #{saved_changes}", job: id

    return unless customer_type_id_before_last_save && customer_type_id
    old_customer_type = CustomerType.find(customer_type_id_before_last_save)
    JobService::ChangePaymentType.new(
      job: self, old_payment_type: old_customer_type
    ).call
  end

  def site_change
    log :debug, "site_change checking #{saved_changes}", job: id

    if saved_change_to_site_id?
      self.estimate_dirty = true
    end
  end

  def set_estimate_dirty
    self.estimate_dirty = true
  end

  def on_estimate_dirty
    log :debug, "on_estimate_dirty checking #{self}, dirty:#{estimate_dirty}", job: id

    if estimate_dirty && estimatable? && estimate
      job_errors.where(code: JobError::GOOGLE_MATRIX_NO_RESULTS, failed_permanently: true).update_all(failed_permanently: false, updated_at: Time.now)
      CreateOrUpdateEstimateForJob.perform_async(id)
    end
    self.estimate_dirty = false
  end

  def set_invoice_dirty
    self.invoice_dirty = true
  end

  def on_invoice_dirty
    log :debug, "Job::on_invoice_dirty checking , dirty:#{invoice_dirty}, allowed_to_update_invoice:#{invoice&.new_state?}", job: id

    if invoice_dirty && invoice&.new_state?
      log :debug, "#on_invoice_dirty about to call CreateOrUpdateInvoice", job: id
      CreateOrUpdateInvoice.perform_async(id)
    end
    self.invoice_dirty = false
  end

  def on_company_assigned
    log :debug, "on_company_assigned checking #{self}, dirty:#{company_assigned.inspect}", job: id
    if company_assigned && estimatable?
      log :debug, 'on_company_assigned CEB (on company assigned)', job: id
      log :debug, 'on_company_assigned will call CreateOrUpdateEstimateForJob', job: id
      CreateOrUpdateEstimateForJob.perform_async(id)
    end
    self.company_assigned = false
  end

  def run_job_simulator
    if (saved_change_to_status? || saved_change_to_notes?) && pending? && JobSimulator.enabled?
      simulator = JobSimulator.simulator(notes) if notes.present?
      simulator.start(self) if simulator
    end
  end

  def is_enterprise?
    fleet_company_id == Company.enterprise_id
  end

  def to_string
    controller = ActionController::Base.new
    controller.render_to_string 'api/v1/jobs/_job', locals: { job: self }
  end

  def job_status_email_company_name
    if fleet_motor_club_job?
      subj = fleet_company.name
      subj += " -> #{rescue_company.name}" if rescue_company
      subj
    else
      user&.company&.name || fleet_company.name
    end
  end

  def scheduled_system_alert
    scheduled_changed = saved_changes.key?('scheduled_for')
    if fleet_managed_job? && scheduled_changed
      log :debug, " SCHEDULED_SYSTEM_ALERT scheduled_system_alert calling UpdateScheduledSystemAlert", job: id
      UpdateScheduledSystemAlert.perform_async(id)
    end
    true
  end

end
