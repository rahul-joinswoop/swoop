# frozen_string_literal: true

class StrandedVehicle < Vehicle

  # ENG-5640: unit number should be migrated here as it's a property of the vehicle
  # e.g. Fleet Response has a unit number for each of their vehicles

  # Ensure that the job index is up to date with any changes made on the association
  after_commit(on: :update) { drive.preload(:job).each { |drive| drive.job.reindex if drive.job } }

  validates_absence_of :name, :company_id

end
