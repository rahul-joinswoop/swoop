# frozen_string_literal: true

class Reason < BaseModel

  include SoftDelete

  belongs_to :issue

  validates :issue, presence: true
  validates :name, presence: true

end
