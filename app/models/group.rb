# frozen_string_literal: true

class Group < ApplicationRecord

  has_and_belongs_to_many :companies

  SWOOP_ONLY_CLIENT = 'Swoop Only Client'
  SWOOP_PRIMARY_CLIENT = 'Swoop Primary Client'
  AGERO_ONLY_CLIENT = 'Agero Only Client'
  AGERO_PRIMARY_CLIENT = 'Agero Primary Client'

  SWOOP_PRIMARY_PARTNER = 'Swoop Primary Partner'
  AGERO_PRIMARY_PARTNER = 'Agero Primary Partner'

end
