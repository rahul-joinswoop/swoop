# frozen_string_literal: true

# Subclasses of this class will have a database connection to the reporting database
# which is a replica/follower of the live database. This should be used for running
# reports and other offline queries against so that massive, unoptimized queries
# don't overload the main database.
#
# The reporting database is shared by Looker and can be overloaded itself running
# ad hoc queries. Queries should only be sent against this database from asynchronous
# workers to prevent the web application from getting locked up by reports.
class ReportingDatabaseBase < ApplicationRecord

  if configurations["#{Rails.env}_reporting"]
    establish_connection(configurations["#{Rails.env}_reporting"])
  end

  self.abstract_class = true

end
