# frozen_string_literal: true

# Reasons for rejecting invoices which can be specific for each company.
class InvoiceRejectReason < ApplicationRecord

  belongs_to :company

  validates :text, presence: true

  scope :for_company, ->(company) { where company_id: company.id }
  scope :for_swoop, -> { where company_id: Company.swoop_id }

  def self.fetch_company_reasons(company)
    for_company(company)
  end

end
