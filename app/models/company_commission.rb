# frozen_string_literal: true

class CompanyCommission < ApplicationRecord

  belongs_to :company

end
