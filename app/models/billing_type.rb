# frozen_string_literal: true

class BillingType

  # just a namespace to store these constants in
  VCC = "VCC"
  ACH = "ACH"

  ALL_BILLING_TYPES = [VCC, ACH].freeze

end
