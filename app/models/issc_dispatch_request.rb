# frozen_string_literal: true

require 'aasm'

class IsscDispatchRequest < BaseModel

  include AASM

  belongs_to :issc
  has_one :job

  validates :dispatchid, presence: true, uniqueness: { scope: :issc_id }

  NEW_REQUEST = 'NewRequest'
  ACCEPTED = 'Accepted'
  REJECTED = 'Rejected'
  CANCELED = 'Canceled'
  EXPIRED = 'Expired'
  COMPLETED = 'Completed'
  enum status: {
    new_request: NEW_REQUEST,
    accepted: ACCEPTED,
    rejected: REJECTED,
    canceled: CANCELED,
    expired: EXPIRED,
    completed: COMPLETED,
  }

  aasm column: :status, enum: true do
    state :new_request, initial: true
    state :accepted
    state :rejected
    state :canceled
    state :expired
    state :completed

    event :requester_accepted do
      transitions from: :new_request, to: :accepted
    end

    event :requester_rejected do
      transitions from: :new_request, to: :rejected
    end

    event :requester_canceled do
      transitions from: [:new_request, :accepted], to: :canceled
    end

    event :requester_expired do
      transitions from: [:new_request, :expired], to: :expired
    end
  end

end
