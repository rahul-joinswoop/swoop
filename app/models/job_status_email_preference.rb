# frozen_string_literal: true

class JobStatusEmailPreference < BaseModel

  belongs_to :user
  belongs_to :job_status

  scope :by_job_status_id, ->(job_status_id) { where(job_status_id: job_status_id) }

  validates :job_status, uniqueness: { scope: :user }

end
