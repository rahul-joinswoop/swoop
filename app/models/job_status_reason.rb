# frozen_string_literal: true

# This allows a job to have a reason for a specific JobStatus.
# Primarily used by RSC on Cancel and GOA cases.
#
# So for instance if a RSC job is canceled, it will have a Canceled AJS
# and a reason associated with it, like JobStatusReason(text: 'Prior job delayed').
#
# We validate that we'll always have the same job_status for both
# ajs and job_status_reason by the ajs_and_reason_type_match check.
#
# We also validate that the given job matches audit_job_status.job .
class JobStatusReason < ApplicationRecord

  belongs_to :job
  belongs_to :audit_job_status
  belongs_to :job_status_reason_type

  validates :job, :audit_job_status, :job_status_reason_type, presence: true
  validates :audit_job_status, uniqueness: {
    scope: :job,
    message: "has already been taken for the given job",
  }
  validate :ajs_and_reason_type_match
  validate :job_and_ajs_job_match

  private

  def ajs_and_reason_type_match
    if audit_job_status.job_status.id != job_status_reason_type.job_status.id
      errors.add(
        'job_status',
        "must be the same for audit_job_status.job_status " \
        "and job_status_reason_type.job_status, " \
        "got job_status ids (#{audit_job_status.job_status.id}, " \
        "#{job_status_reason_type.job_status.id}) respectively"
      )
    end
  end

  def job_and_ajs_job_match
    if job.id != audit_job_status.job.id
      errors.add(
        'job',
        "must be the same as audit_job_status.job " \
        "got job ids (#{job.id}, " \
        "#{audit_job_status.job.id}) respectively"
      )
    end
  end

end
