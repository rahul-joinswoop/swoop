# frozen_string_literal: true

class FleetJob < Job

  def initialize(*args)
    raise ArgumentError, "Do not instantiate FleetJob instances directly" if self.class == FleetJob
    super
  end

  aasm column: :status, enum: true, no_direct_assignment: false do # need direct assigment to allow undo_storage
    event :fleet_draft do
      transitions from: :predraft, to: :draft
    end

    event :partner_pending do
      transitions from: [:dispatched, :enroute, :onsite, :towing, :towdestination, :completed], to: :pending, guard: [:fleet_manual?]
    end

    event :partner_dispatched do
      transitions from: [:pending, :enroute, :onsite, :towing, :towdestination, :completed, :goa], to: :dispatched, guard: [:fleet_manual?, :original_job_id]
      transitions from: [:pending, :accepted, :enroute, :onsite, :towing, :towdestination, :completed, :goa], to: :dispatched
    end

    event :partner_enroute do
      transitions from: [:pending, :dispatched, :onsite, :towing, :towdestination, :completed, :goa], to: :enroute, guard: [:fleet_manual?]
      transitions from: [:pending, :accepted, :dispatched, :onsite, :towing, :towdestination, :completed, :goa], to: :enroute
    end

    event :partner_onsite do
      transitions from: [:pending, :dispatched, :enroute, :towing, :towdestination, :completed, :goa], to: :onsite, guard: [:fleet_manual?]
      transitions from: [:pending, :accepted, :dispatched, :enroute, :towing, :towdestination, :completed, :goa], to: :onsite
    end

    event :partner_towing do
      transitions from: [:pending, :dispatched, :enroute, :onsite, :towdestination, :completed, :goa], to: :towing, guard: [:fleet_manual?]
      transitions from: [:pending, :accepted, :dispatched, :enroute, :onsite, :towdestination, :completed, :goa], to: :towing
    end

    event :partner_towdestination do
      transitions from: [:pending, :dispatched, :enroute, :onsite, :towing, :completed, :goa], to: :towdestination, guard: [:fleet_manual?]
      transitions from: [:pending, :accepted, :dispatched, :enroute, :onsite, :towing, :completed, :goa], to: :towdestination
    end

    event :partner_completed do
      transitions from: [:pending, :dispatched, :enroute, :onsite, :towing, :towdestination, :goa], to: :completed, guard: [:fleet_manual?]
      transitions from: [:pending, :assigned, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :goa], to: :completed
    end

    event :fleet_canceled do
      transitions from: [:draft, :pending, :unassigned, :auto_assigning, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :goa], to: :canceled, guards: [:fleet_manual?]
      transitions from: [:draft, :pending, :unassigned, :auto_assigning, :assigned, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :completed, :goa], to: :canceled
    end

    event :fleet_deleted do
      transitions from: [:draft, :completed, :canceled], to: :deleted
    end

    event :partner_goa do
      transitions from: [:pending, :dispatched, :enroute, :onsite, :towing, :towdestination, :canceled, :completed], to: :goa, guard: [:fleet_manual?]
      transitions from: [:pending, :accepted, :dispatched, :enroute, :onsite, :towing, :towdestination, :canceled, :completed], to: :goa
    end

    event :partner_released do
      transitions from: [:stored], to: :released
    end

    event :partner_rejected do
      transitions from: [:assigned], to: :rejected
    end

    event :partner_accepted do
      transitions from: [:assigned, :dispatched, :enroute, :onsite, :towing, :towdestination], to: :accepted
    end

    event :partner_fleet_set_driver do
      transitions from: [:accepted], to: :dispatched, guard: :fleet_job?
      transitions from: [:pending, :assigned], to: :dispatched, guard: -> { fleet_manual? || fleet_motor_club_job? }
    end

    event :partner_fleet_remove_driver do
      transitions from: :dispatched, to: :dispatched, guard: :fleet_job?
    end
  end

  def send_eta_sms
    Sms::Eta.send_notification(self, eta: original_eta_mins)
  end

  def partner_set_driver
    if fleet_manual
      if pending? || accepted?
        partner_fleet_set_driver
      end
    else
      if accepted? || (pending? && fleet_motor_club_job?)
        partner_fleet_set_driver
      end
    end
  end

  def partner_remove_driver
    partner_fleet_remove_driver
  end

  #  def partner_status_method_name(status)
  #    "partner_fleet_override_"+status.delete(' ').downcase
  #  end

  def invoice_method_name
    'fleet'
  end

  def email_notes
    notes
  end

  def email_dispatcher
    fleet_dispatcher
  end

  def can_partner_manage_photos?
    if [STATUS_AUTO_ASSIGNING, STATUS_ASSIGNED, STATUS_SUBMITTED].include?(status)
      false
    else
      true
    end
  end

  def has_drop_location_service_code?
    service_code&.is_fleet_drop_location_code?
  end

  private

  def init_review_sms
    self.review_sms = true if review_sms.nil?
  end

end
