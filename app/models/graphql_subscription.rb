# frozen_string_literal: true

class GraphQLSubscription < ApplicationRecord

  belongs_to :viewer, polymorphic: true

  class GraphQLSerializer

    def self.load(json)
      GraphQL::Subscriptions::Serialize.send :load_value, json
    end

    def self.dump(obj)
      GraphQL::Subscriptions::Serialize.send :dump_value, obj
    end

  end

  serialize :context, GraphQLSerializer
  serialize :variables, GraphQLSerializer

  has_and_belongs_to_many :graphql_topics

end
