# frozen_string_literal: true

# Model used for the Forgot Password flow
class PasswordRequest < ApplicationRecord

  include TwilioConcerns

  belongs_to :user
  delegate :email, :phone, to: :user, prefix: :user, allow_nil: true

  scope :unexpired_requests, -> { where("created_at + interval ? >= now()", "#{DAYS_TO_EXPIRATION_DEFAULT} day") }

  DAYS_TO_EXPIRATION_DEFAULT = ENV.fetch('PASSWORD_REQUEST_DAYS_TO_EXPIRATION', 1).to_i

  # Start the user password reset flow.
  # It invalidates any pending password_request with the same user.
  #    By pending we mean 'link not used yet' by the user.
  #
  # It will always save a new password_request despite the user input,
  # for recording what the user is typing
  #
  # @return the password_request
  def trigger!
    invalidate_unused_request

    self.uuid = SecureRandom.uuid if user
    self.valid_request = user.present?

    save!

    associate_with_invalid_requests

    # call our private trigger method
    trigger
  end

  # checks if the given uuid is valid.
  # @return false if uuid.blank?, if not a valid_request? or if it has_expired?
  #
  def self.valid_request_for?(uuid)
    uuid.present? && unexpired_requests.where(uuid: uuid, valid_request: true).exists?
  end

  def invalidate!
    update! valid_request: false, used_at: Time.current

    user.access_tokens.each(&:revoke)
  end

  private

  # .trigger! is the shared method above which calls .trigger
  def trigger
    if user_email.present?
      Delayed::Job.enqueue EmailPasswordRequest.new(id)
    elsif user_phone.present?
      send_sms
    end

    self
  end

  def invalidate_unused_request
    return nil unless user

    unused_requests.find_each { |ur| ur.update! valid_request: false }
  end

  def unused_requests(valid_request = true)
    self.class.where(
      used_at: nil,
      valid_request: valid_request,
      user: user
    )
  end

  def associate_with_invalid_requests
    unused_requests(false).where(invalidated_by: nil).update_all(invalidated_by: id)
  end

  def send_sms
    message = I18n.t('password_request.sms_text', site_url: ENV['SITE_URL'], uuid: uuid)

    PasswordRequest.send_sms(user.phone, message, type: PasswordRequestAuditSms)
  end

end
