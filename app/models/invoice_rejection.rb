# frozen_string_literal: true

# Fleet InHouse & Swoop are able to reject invoices. +InvoiceRejection+ keeps
# track of rejections and reasons of rejections.
class InvoiceRejection < ApplicationRecord

  belongs_to :invoice_reject_reason
  belongs_to :invoice

  validates :invoice_reject_reason, presence: true
  validates :invoice, presence: true

  # Returns a list of InvoiceRejection objects for the Invoice ids given as
  # +invoice_ids+. The +company+ parameter is required to ensure rejections are
  # only returned for the companies associated.
  #
  # While it is possible to have multiple InvoiceRejections for the same
  # Invoice, this method only returns the last rejection for each Invoice based
  # on the date it was created.
  def self.get_all_by_invoice_ids_and_company(invoice_ids, company)
    query = "invoice_rejections.invoice_id IN (?) "

    conditions = [
      invoice_ids.include?(",") ? invoice_ids.split(",") : [invoice_ids],
    ]

    if company.fleet?
      query += "and jobs.fleet_company_id = ?"
      conditions << [company.id]
    elsif company.rescue?
      query += "and jobs.rescue_company_id = ?"
      conditions << [company.id]
    end

    select("DISTINCT ON (invoice_rejections.invoice_id) invoice_rejections.*")
      .joins(invoice: :job)
      .where(query, *conditions)
      .order(:invoice_id, created_at: :desc)
  end

  def reject_reason
    invoice_reject_reason.text
  end

  def assigning_company_name
    invoice.job.assigning_company.name
  end

end
