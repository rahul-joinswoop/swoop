# frozen_string_literal: true

#
# INVOICE > AUDIT INVOICE STATUS
#
# Represents a change in the status of the invoice. An invoice
# moves from `from_invoice_status` to `to_invoice_status` when the
# status changes. The time that the current transition happened at
# automatically stamped as "now".
#
# - The valid statuses come from Invoice.states, which must match
#   the PostgreSQL enum `invoice_status`
# - Therefore, if you need to add an invoice status, add it to
#   models/Invoice.rb, and add the corresponding status to the
#   PostgreSQL enum `invoice_status`
# - This class is invoked by `Invoice#audit_status_change` every time an
#   invoice changes its AASM status
#
class AuditInvoiceStatus < ApplicationRecord

  belongs_to :invoice, inverse_of: :audit_invoice_statuses

  before_validation :set_transition_time

  before_save :populate_total_amount_after_state_change

  validates :invoice, presence: true
  validates :to_invoice_status, presence: true
  validates :transition_happened_at, presence: true

  # If we need to add more values to this enum, add values in `state` enum of invoice.rb.
  # It will automatically be considered as *_invoice_status enums here
  enum from_invoice_status: Invoice.states, _prefix: true
  enum to_invoice_status: Invoice.states, _prefix: true

  private

  # Set the transition time to "now" if it's not already set
  def set_transition_time
    self.transition_happened_at = Time.now if transition_happened_at.blank?
  end

  def populate_total_amount_after_state_change
    self.total_amount_after_state_change = invoice.total_amount
  end

end
