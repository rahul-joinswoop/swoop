# frozen_string_literal: true

class CommissionExclusion < ApplicationRecord

  belongs_to :company
  belongs_to :service_code

end
