# frozen_string_literal: true

class JobError < ApplicationRecord

  include LoggableJob
  extend LoggableJob

  belongs_to :job

  GOOGLE_MATRIX_NO_RESULTS = 0
  CREATE_OR_UPDATE_INVOICE = 1

  class JobException < StandardError

    attr_reader :failed_permanently
    def initialize(msg, failed_permanently = nil)
      @failed_permanently = failed_permanently
      super(msg)
    end

  end

  # will yield if no permanent erros of this code type for this job
  def self.with_job_errors(job, code)
    errs = job.job_errors.where(code: code, failed_permanently: true)
    if errs.length == 0
      begin
        yield(errs)
      rescue JobException => e
        JobError.create!(job: job,
                         code: code,
                         msg: e.message,
                         failed_permanently: e.failed_permanently)
      end
    else
      log :debug, "has previously failed permanently for code:#{code}", job: job.id
    end
  end

end
