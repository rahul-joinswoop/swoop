# frozen_string_literal: true

class Assignment < ApplicationRecord

  belongs_to :job
  belongs_to :partner, class_name: 'RescueCompany'
  belongs_to :site

end
