# frozen_string_literal: true

#
# This model is meant to capture a job's history as it changes state and has
# actions performed upon it and its related models
#
# It's used interchangeably with AuditJobStatus to expose Job History in the UI,
# therefore some methods exposed in both models (HistoryItem and AuditJobStatus)
# should present the same signature as to avoid unnecessary checks.
#
# TODO Refactor this: https://swoopme.atlassian.net/browse/ENG-11324
# (in this ticket, let's check if we can remove some of these alias_attributes)
class HistoryItem < ApplicationRecord

  include JobHistoryLabelConcern

  belongs_to :company
  belongs_to :user
  belongs_to :job, inverse_of: :history_items
  belongs_to :target, polymorphic: true
  belongs_to :source_application

  # These aliases are needed to keep the same interface that AJS exposes.
  # They are mainly used in JobHistory::Label logic
  alias_attribute :name, :title
  alias_attribute :last_set_dttm, :created_at
  alias_attribute :adjusted_dttm, :adjusted_created_at

  after_commit do
    Subscribable.trigger_job_updated job
  end

  # types
  CALL_BACK_REQUESTED = "Call Back Requested"
  DROP_OFF_LOCATION_EDITED = "Drop Off Location Edited"
  INVOICE_EDITED = "Invoice Edited"
  INVOICE_EDITED_GOA_COST = "Invoice Edited - GOA Cost"
  INVOICE_EDITED_VCC_PAYMENT = "Invoice Edited - VCC Payment"
  JOB_DETAILS_EMAILED = "Job Details Emailed - %s"
  PICKUP_LOCATION_EDITED = "Pickup Location Edited"
  SERVICE_EDITED = "Service Edited"
  UI_TYPE_HISTORY = 'History'

  def company_name
    company&.name
  end

  def company_type
    company&.type
  end

  def user_name
    user&.name
  end

  def changed_by_company
    if user&.company
      user.company
    elsif source_application&.oauth_application&.owner.is_a?(Company)
      source_application.oauth_application.owner
    end
  end

  # History Items are not yet editable. When they are, we'll have to find a
  # different solution for this.
  def unique_id
    @unique_id ||= SecureRandom.uuid
  end

  def ui_type
    UI_TYPE_HISTORY
  end

end
