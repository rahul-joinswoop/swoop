# frozen_string_literal: true

class RescueCompany < Company

  alias_attribute :job_update_email, :dispatch_email

  has_one :tt_account, foreign_key: :company_id, inverse_of: :company
  has_many :outgoing_invoices, -> { order(id: :desc) }, foreign_key: :sender_id, class_name: "Invoice", inverse_of: :sender
  has_many :bids, class_name: 'Auction::Bid', foreign_key: 'company_id', inverse_of: :company
  has_many :auctions, class_name: 'Auction::Auction', through: :bids
  belongs_to :parent_company, class_name: 'RescueCompany'
  has_many :sub_companies, class_name: 'RescueCompany', foreign_key: 'parent_company_id', inverse_of: :parent_company
  has_one :commission, class_name: 'CompanyCommission', foreign_key: 'company_id', inverse_of: :company
  has_many :addon_commissions_exclusions, class_name: 'CommissionExclusion', foreign_key: 'company_id', inverse_of: :company
  has_many :service_commissions_exclusions, class_name: 'ServiceCommissionsExclusions', foreign_key: 'company_id', inverse_of: :company
  has_many :rescue_providers, foreign_key: :provider_id, inverse_of: :provider
  has_many :commission_exclusions_services, -> { distinct }, class_name: "ServiceCode", through: :service_commissions_exclusions, source: :service_code
  has_many :commission_exclusions_addons, -> { distinct }, class_name: "ServiceCode", through: :addon_commissions_exclusions, source: :service_code
  if ENV['RESCUE_PHONE_UNIQUE']
    validates_uniqueness_of :phone, conditions: -> { where(deleted_at: nil) },
                                    if: Proc.new { |rescue_company| rescue_company.phone.present? }
  end

  has_one :custom_account, -> { not_deleted }, foreign_key: :company_id, inverse_of: :company
  has_one :invoice_charge_fee, class_name: "CompanyInvoiceChargeFee", foreign_key: :company_id, inverse_of: :company

  # ISSC
  has_many :isscs, foreign_key: :company_id, inverse_of: :company
  has_many :api_access_tokens, -> { where(deleted_at: nil) }, foreign_key: :company_id, inverse_of: :company

  # people
  has_many :rescue_drivers,
           -> { not_deleted.with_role(:driver) },
           class_name: "User",
           source: :users,
           foreign_key: "company_id",
           inverse_of: :company

  # we treat dispatchers and admins identically in the mobile app so we munge them together here
  has_many :dispatchers,
           -> { not_deleted.with_role(:dispatcher).or(not_deleted.with_role(:admin)).distinct },
           class_name: "User",
           source: :users,
           foreign_key: "company_id",
           inverse_of: :company

  # territories
  has_many :territories, foreign_key: :company_id, inverse_of: :company

  # scopes
  scope :containing_lat_lng_in_its_territories, ->(lat, lng) do
    all.joins(:territories).merge(Territory.containing_lat_lng(lat, lng)).distinct
  end

  scope :live, -> { where(live: true) }

  scope :eligible_to_bid_on_job, -> (job) do
    not_deleted
      .live
      .joins(:rescue_providers).where(rescue_providers: { company_id: job.fleet_company_id, deleted_at: nil })
      .joins(:accounts).where(accounts: { client_company_id: Company.swoop_id, deleted_at: nil })
      .joins(:companies_services).where(companies_services: { service_code_id: job.service_code_id })
      .joins(:territories).merge(Territory.overlapping_pickup_location(job))
  end

  DEFAULT_FEATURES_FOR_NEW_COMPANY =
    [
      Feature::SMS_REQUEST_LOCATION,
      Feature::JOB_PO_NUMBER,
      Feature::INVOICING,
      Feature::SETTINGS,
      Feature::LOCATION_CONTACTS,
      Feature::CUSTOMER_TYPE,
      Feature::ALWAYS_SHOW_CALLER,
      Feature::ALLOW_INVOICE_ADD_ITEM,
      Feature::SHOW_DRIVERS_INVOICE,
      Feature::SHOW_DRIVERS_EDIT_INVOICE,
      Feature::SMS_REVIEWS,
      Feature::SMS_TRACK_DRIVER,
      Feature::STORAGE,
    ].freeze

  DEFAULT_REPORTS_FOR_NEW_COMPANY =
    [
      Report::DRIVER_REPORT,
      Report::COMMISSIONS_REPORT,
      Report::JOBS_REPORT,
      Report::ACCOUNT_SUMMARY,
      Report::DRIVER_SUMMARY,
      Report::TRUCK_SUMMARY,
      Report::STATEMENT,
      Report::PO_REQUIRED,
      Report::ETA_ATA_REPORT,
      Report::PAYMENTS_REPORT,
      Report::STORAGE_REPORT,
    ].freeze

  DEFAULT_CUSTOMER_TYPES_FOR_NEW_COMPANY =
    [
      CustomerType::ACCOUNT,
      CustomerType::CUSTOMER_PAY,
      CustomerType::PO,
    ].freeze

  class << self

    def associated_with_phone(phone)
      raise ArgumentError, 'Phone number cannot be blank' if phone.blank?
      clean_phone = Phonelib.parse(phone).e164
      return nil if clean_phone.blank?
      company = RescueCompany.find_by(phone: clean_phone, deleted_at: nil)
      unless company
        site = PartnerSite.find_by(phone: clean_phone, deleted_at: nil)
        company = site.company if site
      end
      company
    end

  end

  # Don't call directly, Use CompanyService::Create instead
  def defaults
    add_features_by_name(DEFAULT_FEATURES_FOR_NEW_COMPANY)
    add_reports_by_name(DEFAULT_REPORTS_FOR_NEW_COMPANY)
    add_customer_types_by_name(DEFAULT_CUSTOMER_TYPES_FOR_NEW_COMPANY)
    super
  end

  # Return the headquarters site for the company.
  # TODO the HQ site should really be tagged as such in the datbase
  def hq
    hq = sites.where(name: 'HQ', deleted_at: nil).order(:id).first
    hq ||= sites.where(deleted_at: nil).order(:id).first
    hq
  end

  # Customize the its name with site.name if necessary.
  # It depends on the quantity of sites this RescueCompany instance contains.
  #
  # @return self.name if no site is given (not likely to happen)
  # @return self.name if sites.size equals to 1
  # @return self.name with site.name added if:
  #           => this RescueCompany has only one site, and its name IS NOT 'HQ'
  #           => this RescueCompany has more than one site
  def customized_name_having(site)
    return name if site.blank? || site.name.blank?
    "#{name} #{site.name}"
  end

  def commission_exclusions_services_ids
    service_commissions_exclusions.map(&:service_code_id).uniq
  end

  def commission_exclusions_addons_ids
    addon_commissions_exclusions.map(&:service_code_id).uniq
  end

  def sites_enabled?
    true
  end

  def vehicle_class_type_enabled?
    true
  end

  def billing_type
    setting(Setting::BILLING_TYPE, :string, BillingType::VCC)
  end

  # List all available notification_settings for this company.
  #
  # Mostly used by UI, to show the available notification settings in the user form.
  def available_user_notification_settings
    UserNotificationSetting.available_user_notification_settings(company: self)
  end

  # It creates territories for rescue companies based on zipcodes
  # param: zip_codes is Array of zipcodes.
  def build_territories_from_zip_codes(zip_codes)
    raise ArgumentError unless zip_codes.respond_to? :each

    zip_codes.each do |zip_code|
      territories_for_zip_code = ZipCodeSubsetToTerritory.where(zip: zip_code)
      polygon_areas = territories_for_zip_code.map { |territory| { polygon_area: territory['polygon_area'] } }
      territories.create!(polygon_areas)
    end
  end

end
