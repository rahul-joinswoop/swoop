# frozen_string_literal: true

# PDW, only needed to support fixtures, which gives me a not null on row creation
class CompaniesFeature < BaseModel

  belongs_to :company
  belongs_to :feature

end
