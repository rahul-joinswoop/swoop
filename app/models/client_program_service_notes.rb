# frozen_string_literal: true

#
# Represent a custom coverage notes that is specific to this client_program and the service_code.
# It's used in the Policy Lookup and Coverage flows.
#
# The logic on how to fetch the appropriate coverage notes for the ClientProgram can be found in
# ClientProgram#coverage_notes_by_filter_or_default
#
class ClientProgramServiceNotes < ApplicationRecord

  belongs_to :client_program
  belongs_to :service_code

  # "Client program has already been taken for the given service_code"
  validates :client_program, uniqueness: {
    scope: :service_code,
    message: 'has already been taken for the given service_code',
  }

end
