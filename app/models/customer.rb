# frozen_string_literal: true

class Customer < ApplicationRecord

  belongs_to :company
  has_many :users
  has_many :jobs, through: :users, source: :job
  after_commit do
    Subscribable.trigger_job_updated jobs
    Subscribable.trigger_user_updated users
  end

end
