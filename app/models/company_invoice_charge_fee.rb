# frozen_string_literal: true

class CompanyInvoiceChargeFee < ApplicationRecord

  include ValidateCurrency

  PAYOUT_INTERVAL_WEEKLY = 'weekly'
  PAYOUT_INTERVAL_MONTHLY = 'monthly'

  belongs_to :company

  validates :company, :percentage, :fixed_value, :payout_interval, presence: true
  validates :fixed_value, numericality: { greater_than_or_equal_to: 0 }
  validates :percentage, numericality: {
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 100,
  }
  validate_currency_matches :company

end
