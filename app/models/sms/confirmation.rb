# rubocop:disable Style/FrozenStringLiteralComment

module Sms
  class Confirmation < Base

    self.description = 'Confirmation Text'

    self.sms_template = "%{fleet_company} (#%{job_id}): Request received at %{addr} (Reply \"1\" to update). ETA coming soon. Support: %{support_phone}"

    MAX_LENGTH = 160

    # PDW ugh, need to intercept here because of crazy design
    def params
      super

      set_addr_in_params
      strip_country_from_addr

      @params
    end

    def set_addr_in_params
      addr = "(unknown)"
      @params[:addr] = addr

      if @params[:customer_location] && @params[:customer_location].address
        sms_without_addr = template % @params
        Rails.logger.debug "sms_without_addr:#{sms_without_addr}, length: #{sms_without_addr.length}"

        left = MAX_LENGTH - sms_without_addr.length
        loc = @params[:customer_location]

        if loc.address.length < left
          @params[:addr] = loc.address
        else
          @params[:addr] = "#{loc.address[0..(left - 3)]}..."
        end
      end
    end

    def strip_country_from_addr
      if @params[:addr]
        countries = ['USA'] # PDW warning, can't strip canada like this as CA clashes with california
        countries.each do |country|
          @params[:addr].chomp!(", #{country}")
          @params[:addr].chomp!(" #{country}")
        end
      end
    end

    def expecting_reply?
      true
    end

    def find_target(phone)
      self
    end

    def process_response(twilio_response)
      if twilio_response.body.to_s.include?('1')
        job.send_location_audit_sms
      end
    end

  end
end
# rubocop:enable Style/FrozenStringLiteralComment
