# frozen_string_literal: true

module Sms
  class ConfirmationEta < Base

    self.description = 'Confirmation With Eta'

    self.sms_template = "%{fleet_company} (SW%{job_id}) : Thank you, we've received your service request" \
                        " ETA: %{eta}"

    class << self

      # For implicit incase you wanna look at what hash objects are required
      def send_notification(job, eta:)
        super(job, { eta: eta })
      end

    end

  end
end
