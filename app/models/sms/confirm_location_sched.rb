# frozen_string_literal: true

module Sms
  class ConfirmLocationSched < Base

    self.description     = 'Request Location for Sched'

    self.sms_template    = "%{fleet_company} (SW%{job_id}): Please confirm your location here for service " \
                           "scheduled between %{schedule_window}: %{url} Call %{support_phone} for help."

    self.template_params = {
      url: ->(record) { record.url },
      schedule_window: ->(record) { record.schedule_window },
    }

    def schedule_window
      time = utc_to_local_job_time(job.scheduled_for)

      start_hours  = time.strftime('%I').gsub(/^0/, '')
      start_window = time.strftime("%m/%d/%Y #{start_hours}:%M")
      end_time     = time + 1.hour
      end_hours    = end_time.strftime('%I').gsub(/^0/, '')
      end_window   = end_time.strftime("#{end_hours}:%M %p")

      "#{start_window} and #{end_window}"
    end

    def redirect_url
      "#{ENV['SITE_URL']}/get_location/#{job.uuid}"
    end

    def url
      "#{site_url}/txt/#{my_uuid}"
    end

  end
end
