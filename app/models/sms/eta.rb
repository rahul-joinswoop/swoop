# frozen_string_literal: true

module Sms
  class Eta < Base

    self.description     = 'ETA Text'

    self.sms_template    = "%{fleet_company}: %{rescue_company} has been dispatched." \
      " ETA: %{eta} min. You can reach them at %{site_or_company_phone}. Reply \"stop\" to stop receiving updates."

    self.template_params = {
      rescue_company: ->(record) { record.rescue_company_name },
      site_or_company_phone: ->(record) { record.job.site_or_rescue_company_phone_human },
    }

    class << self

      # For implicit incase you wanna look at what hash objects are required
      def send_notification(job, eta:)
        super(job, { eta: eta })
      end

    end

  end
end
