# frozen_string_literal: true

module Sms
  class GreatThanks < Base

    self.sms_template = "Great, Thanks!"

    def expecting_reply?
      true
    end

    def find_target(phone)
      self
    end

    def process_response(twilio_response)
      Rails.logger.debug "Sms::GreatThanks::process_response - received: #{twilio_response.body}"
    end

  end
end
