# frozen_string_literal: true

# Farmers Insurance: Speedy’s Towing has revised your ETA to 60 min.
module Sms
  class ReviseEta < Base

    self.description     = 'Revise ETA Text'

    self.sms_template    = "%{fleet_company}: %{rescue_company} has revised your ETA to %{new_eta} min."

    self.template_params = {
      rescue_company: ->(record) { record.rescue_company_name },
    }

    class << self

      # For implicit incase you wanna look at what hash objects are required
      def send_notification(job, eta:)
        super(job, { new_eta: eta })
      end

    end

  end
end
