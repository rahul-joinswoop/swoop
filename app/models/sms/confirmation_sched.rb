# frozen_string_literal: true

module Sms
  class ConfirmationSched < Base

    self.description  = 'Scheduled Job Confirmation'

    self.sms_template = "%{fleet_company} (SW%{job_id}): Thank you, we've received your service request" \
                        " scheduled between %{schedule_window}."

    self.template_params = {
      schedule_window: ->(record) { record.schedule_window },
    }

    def schedule_window
      time = utc_to_local_job_time(job.scheduled_for)

      start_hours  = time.strftime('%I').gsub(/^0/, '')
      start_window = time.strftime("%m/%d/%Y #{start_hours}:%M")
      end_time     = time + 1.hour
      end_hours    = end_time.strftime('%I').gsub(/^0/, '')
      end_window   = end_time.strftime("#{end_hours}:%M %p")

      "#{start_window} and #{end_window}"
    end

  end
end
