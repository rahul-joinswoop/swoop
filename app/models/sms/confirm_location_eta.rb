# frozen_string_literal: true

module Sms
  class ConfirmLocationEta < Base

    self.description     = 'Request Location with Eta'

    self.sms_template    = "%{fleet_company} (SW%{job_id}): Please confirm your location here for fastest service" \
                           " %{url}. ETA: %{eta}  Call %{support_phone} for help."

    self.template_params = {
      url: ->(record) { record.url },
    }

    def redirect_url
      "#{ENV['SITE_URL']}/get_location/#{job.uuid}"
    end

    def url
      "#{site_url}/txt/#{my_uuid}"
    end

    class << self

      # For implicit incase you wanna look at what hash objects are required
      def send_notification(job, eta:)
        super(job, { eta: eta })
      end

    end

  end
end
