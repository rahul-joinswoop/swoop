# frozen_string_literal: true

module Sms
  class ConfirmLocation < Base

    self.description     = 'Request Location Text'

    self.sms_template    = "%{fleet_company} (#%{job_id}): Confirm your location here for fastest service" \
                           " %{url}. ETA coming soon. Support: %{support_phone}"

    self.template_params = {
      url: ->(record) { record.url },
    }

    def redirect_url
      "#{ENV['SITE_URL']}/get_location/#{job.uuid}"
    end

    def url
      "#{site_url}/txt/#{my_uuid}"
    end

  end
end
