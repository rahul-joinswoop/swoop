# frozen_string_literal: true

module Sms
  class ConfirmPartnerCompleted < Base

    self.description = 'Completion Text'

    self.sms_template = "%{fleet_company}: Has the service been provided to your satisfaction? Reply \"Y\" if yes, \"N\" if further assistance is required."

    def expecting_reply?
      true
    end

    def find_target(phone)
      self
    end

    def process_response(twilio_response)
      ProcessConfirmPartnerCompletedResponse.new(twilio_response, self).call
    end

    def reply_human_status
      if parsed_response == 1
        'Customer Confirmed Complete'
      elsif parsed_response == 2
        'Customer Replied Not Complete'
      else
        nil
      end
    end

    def visible_to_company(company)
      company.is_swoop?
    end

  end
end
