# frozen_string_literal: true

module Sms
  class ReviewLink < Base

    self.description = 'Review Link'

    self.send_slack_url = false

    self.sms_template = "%{fleet_company}: Please tap %{url} to take a quick survey to review your experience. "\
                        "Thank you!"

    self.template_params = {
      url: ->(record) { record.url },
    }

    def url
      preface = job.fleet_company.try(:name).to_s.split.first
      "#{site_url}/r/#{preface.downcase}/#{job.uuid}"
    end

  end
end
