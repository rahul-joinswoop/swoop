# frozen_string_literal: true

module Sms
  class PartnerNotOnSite < Base

    self.description = 'Partner not On Site'

    self.sms_template = "%{fleet_company}: Thanks, we'll contact your provider for an update right away. Reply \"1\" if they arrive on site in the meantime."

    def expecting_reply?
      true
    end

    def find_target(phone)
      self
    end

    def process_response(twilio_response)
      Rails.logger.debug "Sms::PartnerNotOnSite::process_response - received: #{twilio_response.body}"
      ProcessConfirmPartnerOnSiteResponse.new(twilio_response, self, false).call
    end

    def reply_human_status
      if parsed_response == 1
        'Customer Confirmed On Site'
      else
        nil
      end
    end

    def visible_to_company(company)
      company.is_swoop?
    end

  end
end
