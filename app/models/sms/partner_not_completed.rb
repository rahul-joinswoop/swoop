# frozen_string_literal: true

module Sms
  class PartnerNotCompleted < Base

    self.description = 'Not Complete Text'

    self.sms_template = "%{fleet_company}: We will alert a member of our team to reach out to you shortly."

    def expecting_reply?
      true
    end

    def find_target(phone)
      self
    end

    def process_response(twilio_response)
      Rails.logger.debug "Sms::PartnerNotCompleted::process_response - received (ignoring) : #{twilio_response.body}"
    end

    def visible_to_company(company)
      company.is_swoop?
    end

  end
end
