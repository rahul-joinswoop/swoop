# frozen_string_literal: true

module Sms
  class ConfirmPartnerOnSiteFollowUp < Base

    self.description = 'On Site Follow Up Text'

    self.sms_template = "%{fleet_company}: Checking in to make sure everything is going smoothly. Has your service provider arrived? Reply \"Y\" if yes, \"N\" if not yet"

    def expecting_reply?
      true
    end

    def find_target(phone)
      self
    end

    def process_response(twilio_response)
      ProcessConfirmPartnerOnSiteResponse.new(twilio_response, self).call
    end

    def reply_human_status
      if parsed_response == 1
        'Customer Confirmed On Site'
      elsif parsed_response == 2
        'Customer Replied Not On Site'
      else
        nil
      end
    end

    def visible_to_company(company)
      company.is_swoop?
    end

  end
end
