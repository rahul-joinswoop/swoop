# frozen_string_literal: true

# Farmers Insurance: Service provider has been updated to Speedier Towing. ETA: 20 min.
module Sms
  class Reassign < Base

    self.description  = 'Partner Reassign Text'

    self.sms_template = "%{fleet_company}: Service provider has been updated%{rescue_company}" \
      " ETA: %{eta} min.%{rescue_company_phone}"

    self.template_params = {
      rescue_company: ->(record) { record.rescue_company_or_nothing },
      rescue_company_phone: ->(record) { record.rescue_company_phone },
    }

    def rescue_company_or_nothing
      rname = job.rescue_company.try(:name) || 'Other'
      return " to #{rname}" unless rname.eql?('Other')
      ''
    end

    class << self

      def send_notification(job, eta:)
        super(job, { eta: eta })
      end

    end

  end
end
