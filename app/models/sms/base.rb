# frozen_string_literal: true

require 'swoop_twilio'

module Sms
  class Base < AuditSms

    include LoggableJob
    include TwilioConcerns

    DEFAULT_PHONE_FORMAT = /\+1(\d{3})(\d{3})(\d{4})/.freeze

    DEFAULT_PARAMS = {
      fleet_company: ->(record) {
        name = record.job.fleet_company.try(:name).to_s

        [FleetCompany::FARMERS_SHORT_NAME, FleetCompany::CENTURY_SHORT_NAME].find do |short_name|
          name[short_name]
        end || name
      },
      job_id: ->(record) { record.job_id },
      support_phone: ->(record) {
        phone = record.job.support_phone
        phone.gsub! DEFAULT_PHONE_FORMAT, '\1-\2-\3' if phone =~ DEFAULT_PHONE_FORMAT
        phone
      },
      customer_location: ->(record) {
        record.job.service_location
      },
    }.freeze

    attr_accessor :extra_params
    before_create :my_uuid # Ensure UUID is set
    before_save :template_to_sms_body

    def template_to_sms_body
      self.body = template % params if body.blank?
      body
    end

    def params
      unless @params
        tp = DEFAULT_PARAMS.merge(template_params || {})

        @params = {}
        tp.each do |param_name, interpolator|
          @params[param_name] = interpolator.call(self)
        end
        @params.merge!(extra_params || {})
      end
      @params
    end

    def template_params
      self.class.template_params
    end

    def template
      self.class.sms_template
    end

    def slack_notification
      link = " #{params[:url]}" if self.class.send_slack_url
      job.slack_notification "#{self.class} sent#{link}", tags: ['sms', slack_tag]
    end

    def send_sms
      return if to.blank? # Dont send if we dont have a to message
      log :debug, "CONSUMER_SMS Sending to (#{to}) #{body}", job: job_id
      SwoopTwilio.new.message_create(
        to: to,
        from: from,
        body: body,
        status_callback: status_callback_url
      )
    end
    handle_asynchronously :send_sms

    def rescue_company_name
      rname = job.rescue_company.try(:name) || 'Other'
      return 'Your service provider' if rname == 'Other'
      rname
    end

    def rescue_company_phone
      txt = String.new ""
      if job.rescue_company.phone.present?
        phone = job.rescue_company.phone.dup
        if phone =~ DEFAULT_PHONE_FORMAT
          phone.gsub!(DEFAULT_PHONE_FORMAT, '\1-\2-\3')
        end
        txt << " You can reach them at #{phone}."
      end
      txt
    end

    # The other works create our class for us since we already have one
    # we dont want.
    def deliver
      slack_notification
      send_sms
      self
    end

    def slack_tag
      return self.class.slack_tag if self.class.slack_tag
      self.class.name.demodulize.underscore.tr('_', '-')
    end

    def site_url
      @site_url ||= ENV['SITE_URL'].to_s.gsub(/(http|https)\:\/\//, '')
    end

    def utc_to_local_job_time(time)
      job_time_zone.utc_to_local(time)
    end

    def job_time_zone
      @tz ||= job.service_location.timezone
    end

    def my_uuid
      self.uuid ||= SecureRandom.hex(5)
    end

    class << self

      attr_accessor :sms_template, :template_params,
                    :send_slack_url, :slack_tag

      def send_notification(job, extra = {})
        to_number, to_user = job.sms_phone
        sms = new(job: job, user: to_user, to: to_number, from: TwilioConcerns.sticky_sms_number(to_number))

        sms.extra_params = extra
        sms.save!

        sms.deliver
      end

    end

  end
end
