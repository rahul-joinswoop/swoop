# frozen_string_literal: true

module Sms
  class BrandedReviewLink < Base

    self.description = 'Branded Review Link'

    self.send_slack_url = false

    self.sms_template = "%{fleet_company}: Please tap %{url} to take a quick survey to review your experience. "\
                        "Thank you!"

    self.template_params = {
      url: ->(record) { record.url },
    }

    def url
      host = job.fleet_company.survey_host ? "https://#{job.fleet_company.survey_host}" : site_url
      logger.debug "STUB: #{inspect}"
      "#{host}/t/#{my_uuid}"
    end

    # Called from application::txt when a review text is clicked
    def redirect_url
      "/s/#{job.uuid}"
    end

  end
end
