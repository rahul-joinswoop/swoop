# frozen_string_literal: true

# ElasticSearch specific definitions for rescue providers.
module RescueProvider::Searchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    index_name Swoop::ElasticSearch.index_name(:rescue_providers)

    scope :elastic_search_backfill, -> {
      elastic_search_preload.where("company_id IS NOT NULL")
    }

    scope :elastic_search_preload, -> {
      preload([{ provider: :location }, :site, :location, :tags])
    }

    after_commit on: [:create, :update], if: :company_id? do
      Indexer.perform_async(:index, RescueProvider.name, id)
    end

    after_commit on: [:destroy] do
      Indexer.perform_async(:delete, RescueProvider.name, id)
    end

    settings index: {
      number_of_shards: 2,
      number_of_replicas: 1,
      analysis: {
        filter: {
          preserve_asciifolding: {
            type: "asciifolding",
            preserve_original: true,
          },
          strip_punctuation: {
            type: "pattern_replace",
            pattern: "\\p{P}",
            replacement: "",
          },
        },
        analyzer: {
          name_analyzer: {
            type: "custom",
            tokenizer: "standard",
            filter: ["standard", "lowercase", "preserve_asciifolding", "strip_punctuation"],
          },
        },
      },
    } do
      mappings dynamic: 'false' do
        indexes :id, type: :integer
        indexes :status, analyzer: :english, index_options: :offsets
        indexes :name, analyzer: :name_analyzer
        indexes :sort_name, type: :keyword
        indexes :dispatch_phone, type: :keyword
        indexes :dispatch_email
        indexes :fax, type: :keyword
        indexes :address
        indexes :primary_contact
        indexes :primary_phone, type: :keyword
        indexes :primary_email
        indexes :accounting_email
        indexes :notes, analyzer: :english, index_options: :offsets
        indexes :vendor_code, type: :keyword
        indexes :location_code, type: :keyword
        indexes :network
        indexes :deleted_at, type: :date
        indexes :company do
          indexes :id, type: :integer
        end
        indexes :provider do
          indexes :id, type: :integer
        end
      end
    end
  end

  def as_indexed_json(_options = {})
    {
      "id" => id,
      "status" => status,
      "name" => name,
      "sort_name" => sort_name,
      "dispatch_phone" => phone_or_site_phone,
      "dispatch_email" => dispatch_email,
      "fax" => fax,
      "address" => location_address_or_providers,
      "primary_contact" => primary_contact,
      "primary_phone" => primary_phone,
      "primary_email" => primary_email,
      "accounting_email" => accounting_email,
      "notes" => notes,
      "company" => { "id" => company_id },
      "provider" => { "id" => provider_id },
      "vendor_code" => vendor_code,
      "location_code" => location_code,
      "network" => tags.map(&:name),
      "deleted_at" => deleted_at,
    }
  end

end
