# frozen_string_literal: true

class PasswordRequestAuditSms < AuditSms

  def redirect_url
    "#{ENV['SITE_URL']}"
  end

end
