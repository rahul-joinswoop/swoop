# frozen_string_literal: true

class MilesEnRouteOnlyRate < Rate

  validates :miles_enroute, presence: true
  validates :miles_enroute_free, presence: true

  def mail_attributes
    base = super

    base.merge({
      SERVICE_FEE => hookup,
      EN_ROUTE_MILES => miles_enroute,
      FREE_EN_ROUTE_MILES => miles_enroute_free,
    })
  end

  def line_item_descriptions
    [TAX, SERVICE_FEE, EN_ROUTE_MILES, EN_ROUTE_KILOMETERS, FREE_EN_ROUTE_MILES, FREE_EN_ROUTE_KILOMETERS]
  end

  def base_hookup
    hookup = super
    hookup[:description] = SERVICE_FEE
    hookup
  end

  def calculate_line_items(invoicable)
    raise CannotCalculateRateWithoutInvoicableError unless invoicable.is_a?(Invoicable)
    lis = []
    lis << base_hookup
    lis.concat(base_en_route(invoicable))
    add_common_items(invoicable, lis)
    adjust_mileage_line_items(invoicable, lis)
    lis
  end

end
