# frozen_string_literal: true

class Discount < CreditNote

  validates :total_amount, numericality: { less_than: 0 }
  def display_name
    'Discount'
  end

end
