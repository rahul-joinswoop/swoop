# frozen_string_literal: true

#
# JOB > CANDIDATE
#
# A job candidate is a truck or site of a tow company that is eligible to bid on an auction
#
# This is an abstract parent and is never instantiated directly;
# instead, create a Job::Candidate::Site or a Job::Candidate::Truck
#
class Job

  class JobCandidateCostCalculationError < StandardError; end
  class CannotCalculateCostForJobCandidateWithoutEstimateError < JobCandidateCostCalculationError; end
  class CannotCalculateCostForJobCandidateWithoutAccountError < JobCandidateCostCalculationError; end
  class CannotCalculateCostForJobCandidateWithoutJobError < JobCandidateCostCalculationError; end
  class CannotCalculateCostForJobCandidateWithoutCompanyError < JobCandidateCostCalculationError; end
  class CannotCalculateCostForJobCandidateWithoutSwoopCompanyError < JobCandidateCostCalculationError; end
  class CannotCalculateCostForJobCandidateWithoutRateError < JobCandidateCostCalculationError; end
  class CannotCalculateCostForJobCandidateWithStorageRateError < JobCandidateCostCalculationError; end
  class CannotCalculateCostForJobCandidateWithZeroDollarInvoiceError < JobCandidateCostCalculationError; end
  class CannotGenerateBidsBecauseSuppliedListIsInvalidError < JobCandidateCostCalculationError; end

  class Candidate < ApplicationRecord

    self.table_name = "auto_assign_candidates"

    #
    # To estimate the time it'll take a candidate to do a tow job,
    # we need to assume how long every candidate might spend at a
    # theoretical job site and dropoff
    #
    ESTIMATED_MINS_ON_SITE = 20
    ESTIMATED_MINS_AT_DROPOFF = 20

    MAX_BID_MULTIPLIER = 3

    DEFAULT_POOL_TYPE = 'auto_assign'

    # An auto assign candidate says, "This is my invoice, which contains
    # an estimated cost were I to win the job"
    # N.B. This is not persisted, but is useful when building candidates
    # whose cost is calculated using invoices
    attr_accessor :invoice

    belongs_to :company
    belongs_to :job
    belongs_to :site, class_name: "::Site"
    belongs_to :estimate # An auto assign candidate says, "This is the estimate of time and distance for me to do the job"

    #
    # The pools that a candidate can be in
    #
    # ⚠️ If you add, rename, or delete values in this enum, you must do so here AND
    # in the PostgreSQL enum 'job_candidate_pool_type'
    #
    enum pool_type: {
      auto_assign: 'auto_assign',
      manual_assign_with_swoop_account: 'manual_assign_with_swoop_account',
      manual_assign_oon_preferred: 'manual_assign_oon_preferred',
      manual_assign_without_oon_preferred_without_swoop_account: 'manual_assign_without_oon_preferred_without_swoop_account',
    }

    # Set 'auto_assign' as default value to  before saving the record
    before_save { attributes[:pool_type] = pool_type }

    # These associations are only valid on the Truck subclass, but they need to be here in order to eager load them.
    belongs_to :driver, class_name: 'User', optional: true
    belongs_to :vehicle, optional: true

    # An auto assign candidate must belong to the job it's a candidate of
    validates :job, presence: true

    # An auto assign candidate must belong to its rescue company
    validates :company, presence: true
    validate :company_is_a_rescue_company

    #
    # Scopes for each pool_type. If you have a pool type 'auto_assign', this
    # creates a scope 'for_auto_assign(job_id)' which contains all of the
    # candidates in that pool for the job.
    #
    # See enum pool_type above for valid pool types.
    #
    pool_types.each_key do |pool_type|
      scope :"for_#{pool_type}", ->(job_id) { where(pool_type: pool_type, job_id: job_id) }
    end

    class << self

      # For all of the auto assign candidates for a job, copy over
      # the ratings and num_jobs from the ratings table
      def populate_all_ratings_for_job(job:)
        # Step 1: Default all companies to have 100 rating by default;
        # this allows new companies to get first dibs at some jobs, and
        # over time they will get ratings which will come into play
        where(job_id: job.id).update_all(rating: 100)

        # Step 2: Copy over real ratings for companies that have data
        copy_ratings_sql = <<~SQL
          UPDATE auto_assign_candidates
          SET
            rating = ratings.score,
            num_jobs = ratings.num_jobs
          FROM ratings
          WHERE
            auto_assign_candidates.company_id=ratings.rescue_company_id
            AND date = (SELECT MAX(date) FROM ratings)
            AND ratings.client_company_id = ?
            AND job_id = ?
        SQL

        cleaned_copy_ratings_sql = sanitize_sql([copy_ratings_sql, job.fleet_company_id, job.id])
        connection.execute(cleaned_copy_ratings_sql)
      end

      # Find the sites and vehicles that can bid on a job up to the maximum number of bidders
      # for a job. Only the partner companies with the closest sites or vehicles to the job
      # service location are eligible.
      #
      # Additionally, to be eligible, the companies must
      #
      # - have a Swoop account
      # - be eligible to service the client (via a RescueProvider record)
      # - be able to provide the required service
      # - have a territory that includes the job service location
      #
      # The return value is a hash with keys for :sites and :rescue_vehicles. The values will
      # be a list sorted by distance from the job service location.
      def find_sites_and_vehicles_to_be_candidate_for_auction(job)
        empty_response = { sites: [], rescue_vehicles: [] }

        service_point = job.service_location&.to_coordinates
        return empty_response unless service_point

        max_bidders = (job.fleet_company.auction_max_bidders * MAX_BID_MULTIPLIER)
        return empty_response unless max_bidders > 0

        # Step 1: Grab the ids of all partner companies that can bid on a job.
        eligible_bidder_ids = RescueCompany.eligible_to_bid_on_job(job).pluck(:id)
        return empty_response if eligible_bidder_ids.empty?

        # The list of candidates will be split by company and limited to max_bidders companies.
        # Only entities closer than the maximum distance for the furthest bidder will be included.
        candidates = Job::Candidate::Collector.new(max_bidders, job.fleet_company.auction_max_possible_distance)

        # Step 2: Add sites to the candidates only keeping the closest ones for each company.
        # Iterate over the companies and sites in batches to avoid sudden memory bloat in case
        # there are 1000's of records that will be examined by thrown out as too far.
        eligible_bidder_ids.in_groups_of(300, false) do |company_ids|
          ::Site.not_deleted.dispatchable.includes(:location).where(company_id: company_ids).find_each(batch_size: 300) do |site|
            if site.open_for_business?
              distance = Geocoder::Calculations.distance_between(service_point, site.location, units: :mi)
              candidates.add(site, distance)
            end
          end
        end

        # Step 3: Add rescue vehicles to the candidates only keeping the closest ones for each company
        eligible_bidder_ids.in_groups_of(300, false) do |company_ids|
          ::RescueVehicle.not_deleted.where(company_id: company_ids).where("location_updated_at >= ?", 5.minutes.ago).find_each(batch_size: 300) do |vehicle|
            distance = Geocoder::Calculations.distance_between(service_point, vehicle.current_location, units: :mi)
            candidates.add(vehicle, distance)
          end
        end

        # Step 5: Extract the sites and rescue vehicles from the list of candidates and filter out
        # any further than the maximum allowed distance.
        sites = []
        vehicles = []
        candidates.eligible_entities.each do |entity|
          if entity.is_a?(::Site)
            sites << entity
          else
            vehicles << entity
          end
        end
        { sites: sites, rescue_vehicles: vehicles }
      end

    end

    # Assign a default value to `pool_type` in-memory, if current value is nil
    def pool_type
      attributes['pool_type'] || DEFAULT_POOL_TYPE
    end

    # Calculate the costs for this candidate to do the job, and put that value
    # into the cost field
    #
    def calculate_cost
      # Step 0: Ensure the required models to calculate cost for the candidate exist
      raise CannotCalculateCostForJobCandidateWithoutEstimateError unless estimate
      raise CannotCalculateCostForJobCandidateWithoutAccountError unless account
      raise CannotCalculateCostForJobCandidateWithoutJobError unless job && job.is_a?(Job)
      raise CannotCalculateCostForJobCandidateWithoutCompanyError unless company && company.is_a?(RescueCompany)
      raise CannotCalculateCostForJobCandidateWithoutSwoopCompanyError unless Company.swoop

      # Step 1: Find the correct rate for the candidate, were he to do the job
      rate = Rate.inherit(
        company.id,
        account.id,
        site,
        issc_contractor_id,
        job.service_code_id,
        job.invoice_vehicle_category_id,
        false
      )
      raise CannotCalculateCostForJobCandidateWithoutRateError unless rate.is_a? Rate
      raise CannotCalculateCostForJobCandidateWithStorageRateError if rate.is_a? StorageRate

      # Step 2: Create and populate the invoice with line items from the rate
      generated_invoice = Invoice.new(
        sender: company,
        recipient: account,
        currency: company.currency,
        job: job,
        recipient_company: Company.swoop,
        description: 'Generated for auto assign candidate',
        version: Invoice::VERSION
      )
      PopulateInvoiceForJobCandidate.new(
        generated_invoice,
        job,
        rate,
        self
      ).call

      # Step 3: Run validators on the invoice, which will crunch the total_amount
      generated_invoice.valid?
      raise CannotCalculateCostForJobCandidateWithZeroDollarInvoiceError unless generated_invoice.total_amount.nonzero?

      # Step 4: Persist the cost on the candidate
      update!(cost: generated_invoice.total_amount)
    end

    #
    # A job is invoicable, which means it is expected to
    # implement a set of methods that allow rates and invoices
    # to be calculated
    #
    include Invoicable
    #
    # Aliasing company to rescue company is required in order to implement
    # the Invoicable interface
    #
    alias_method :rescue_company, :company
    alias_method :rescue_company=, :company=

    #
    # The fleet company who is requesting this candidate to bid on
    # the job
    #
    def fleet_company
      job.fleet_company
    end

    #
    # The account that this candidate will bill for doing the
    # tow job
    #
    def account
      Account.not_deleted.find_by!(company: rescue_company, client_company_id: Company.swoop_id)
    end

    #
    # The ISSC contractor for the job
    # (Invoicable requires this to be implemented -- don't actually use it)
    #
    def issc_contractor_id
      nil
    end

    #
    # About how long it will take this candidate to run the job, from
    # beginning to end?
    #
    def mins_p2p
      raise CannotCalculateCostForJobCandidateWithoutEstimateError unless estimate

      case
      # If we have a drop location, and we have estimates for A->B->C->A, then
      # estimate the minutes from A->B->C->A, with some padding for how long it'll
      # take the driver to be onsite and be at the dropoff
      when drop_location && estimate.mins_ab && estimate.mins_bc && estimate.mins_ca
        estimate.mins_ab + ESTIMATED_MINS_ON_SITE + estimate.mins_bc + ESTIMATED_MINS_AT_DROPOFF + estimate.mins_ca

      # If we don't have a drop_location and A->B->C->A, we estimate the minutes
      # from A->B->A, if we can
      when estimate.mins_ab && estimate.mins_ba
        estimate.mins_ab + ESTIMATED_MINS_ON_SITE + estimate.mins_ba

      # If we don't have sufficient estimate legs, we can't project the minutes
      else
        nil
      end
    end

    #
    # About how far will the candidate have to travel to run the job,
    # from beginning to end?
    #
    def miles_p2p
      raise CannotCalculateCostForJobCandidateWithoutEstimateError unless estimate

      estimate.miles_p2p
    end

    #
    # What is the drop location where the candidate would
    # leave the vehicle if he/she runs the job?
    #
    def drop_location!
      # Try to use the job's drop location; if not, try to use the estimate's drop location
      # Related to ENG-8315: These two drop locations should be the same, and we shouldn't
      # be choosing the drop location nondeterministically like this
      raise CannotCalculateCostForJobCandidateWithoutEstimateError unless estimate

      job.drop_location || estimate.drop_location
    end

    def drop_location
      drop_location!
    rescue CannotCalculateCostForJobCandidateWithoutEstimateError
      nil
    end

    # Where is this auto assign candidate starting from?
    # (must be implemented concretely)
    def starting_location
      raise UncallableAbstractParentMethodError
    end

    def company_is_a_rescue_company
      unless company.is_a? RescueCompany
        errors.add(:company, "must be a rescue company")
      end
    end

  end

end
