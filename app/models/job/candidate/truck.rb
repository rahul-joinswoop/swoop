# frozen_string_literal: true

class Job::Candidate::Truck < Job::Candidate

  has_many :candidate_jobs, dependent: :destroy, class_name: "Job::Candidate::CandidateJob", foreign_key: 'auto_assign_candidate_id'

  # To enable renaming of AutoAssign::TruckCandidate to Job::Candidate::Truck
  # per ENG-9551, we need to:
  # 1. disable the 'type' column from using Rails' built-in STI
  self.inheritance_column = :_nonexistant_column_to_bypass_sti
  # 2. manually set the type column to Job::Candidate::Truck when we save,
  # so that new and edited records are saved as Job::Candidate::Truck
  before_save { self.type = 'Job::Candidate::Truck' }
  # 3. always report the type as a Job::Candidate::Truck
  def type
    'Job::Candidate::Truck'
  end
  # 4. and set a default scope which only reads type columns that are
  # 'Job::Candidate::Truck' or 'AutoAssign::TruckCandidate' so that STI
  # still appears to be the same as always
  default_scope { where(type: 'Job::Candidate::Truck').or(where(type: 'AutoAssign::TruckCandidate')) }
  # Once this code is deployed and all web nodes and worker nodes have it,
  # we can migrate all type fields to AutoAssign::TruckCandidate, remove the
  # references to AutoAssign::TruckCandidate and re-enable Rails' own STI,
  # which is captured in ENG-9749 and ENG-9792

  def self.create_all_for_job_auction_by_radius(job:, auction:)
    vehicles = RescueVehicle.find_all_in_range_of_auction_job(job: job, auction: auction)
    create_all_for_job_auction_from_list_of_rescue_vehicles(job: job, auction: auction, vehicles: vehicles)
  end

  def self.create_all_for_job_auction_from_list_of_rescue_vehicles(job:, auction:, vehicles:)
    # Raise error if supplied list isn't an array
    raise Job::CannotGenerateBidsBecauseSuppliedListIsInvalidError unless vehicles.is_a?(Array)

    candidates = []

    # Return empty array, if vehicles list is blank
    return candidates if vehicles.empty?

    # TODO: Looping over each Rescue Vehicle to call build_from_job_vehicle is
    # inefficient and costing us extra money to Google, since we're calling it
    # for every rescue vehicle. Instead of O(N) it can be O(1).
    # See [ENG-8127] Fetch all truck candidates in 1 call to Google Distance
    # Matrix to save $$$
    vehicles.each do |vehicle|
      candidate = build_from_job_vehicle(job, auction, vehicle)
      if candidate.google_eta
        candidates.push(candidate)
        candidate.save!
      end
    end

    candidates
  end

  def self.build_from_job_vehicle(job, auction, vehicle)
    # Step 1: Create a new TruckCandidate for the vehicle
    new_truck_candidate = new({
      job: job,
      vehicle: vehicle,
      has_auto_dispatchable_job: vehicle.has_auto_dispatchable_job?,
      service_ends_at_service_location: vehicle.service_ends_at_service_location?,
      service_ends_at_drop_location: vehicle.service_ends_at_drop_location?,
      is_onsite: vehicle.is_onsite?,
      time_onsite: vehicle.time_onsite,
      no_jobs: vehicle.has_no_jobs?,
      lat: vehicle.lat,
      lng: vehicle.lng,
      driver: vehicle.driver,
      location_updated_at: vehicle.location_updated_at,
      company: vehicle.company,
      site: vehicle.associated_site, # ENG-8251 - associate this vehicle with a site
    })

    # Step 2: Add the autoassign candidate-job join to the
    # new truck candidate
    vehicle.current_jobs.map do |current_job|
      a = Job::Candidate::CandidateJob.new({
        job: current_job,
        status: current_job.status,
        job_candidate: new_truck_candidate,
      })
      new_truck_candidate.candidate_jobs << a
    end

    # Step 3: For the truck candidate, create a time & distance estimate using
    # Google Distance Matrix Service
    if vehicle.lat && vehicle.lng
      ::CreateEstimatesForJobCandidatesService.new(
        candidates: [new_truck_candidate],
        auction: auction,
        job: job
      ).call
    end

    # Step 4:
    new_truck_candidate
  end

  # Where is this truck candidate starting from?
  def starting_location
    vehicle.current_job_location
  end

end
