# frozen_string_literal: true

### Stub class for reporting on auto dispatch
class Job::Candidate::CandidateJob < ApplicationRecord

  self.table_name = "auto_assign_candidate_jobs"

  belongs_to :job_candidate, class_name: 'Job::Candidate::Truck', foreign_key: "auto_assign_candidate_id"
  belongs_to :job

end
