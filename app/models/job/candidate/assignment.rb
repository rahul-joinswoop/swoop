# frozen_string_literal: true

class Job::Candidate::Assignment < ApplicationRecord

  self.table_name = "auto_assign_assignments"

  belongs_to :job
  belongs_to :chosen_provider, class_name: "RescueProvider"
  belongs_to :chosen_company, class_name: "Company"
  belongs_to :vehicle

  scope :for_job, ->(job) { where(job: job).order('id DESC').first_or_create }

  MANUAL_ASSIGNMENT = 'manual_assignment' # Job wasn't assigned successfully, it needs to be manually assigned
  ASSIGNED = 'assigned' # The job was successfully assigned

end
