# frozen_string_literal: true

class Job::Candidate::Site < Job::Candidate

  include LoggableJob
  extend LoggableJob

  # To enable renaming of AutoAssign::SiteCandidate to Job::Candidate::Site
  # per ENG-9551, we need to:
  # 1. disable the 'type' column from using Rails' built-in STI
  self.inheritance_column = :_nonexistant_column_to_bypass_sti
  # 2. manually set the type column to Job::Candidate::Site when we save,
  # so that new and edited records are saved as Job::Candidate::Site
  before_save { self.type = 'Job::Candidate::Site' }
  # 3. always report the type as a Job::Candidate::Site
  def type
    'Job::Candidate::Site'
  end
  # 4. and set a default scope which only reads type columns that are
  # 'Job::Candidate::Site' or 'AutoAssign::SiteCandidate' so that STI
  # still appears to be the same as always
  default_scope { where(type: 'Job::Candidate::Site').or(where(type: 'AutoAssign::SiteCandidate')) }
  # Once this code is deployed and all web nodes and worker nodes have it,
  # we can migrate all type fields to AutoAssign::SiteCandidate, remove the
  # references to AutoAssign::SiteCandidate and re-enable Rails' own STI,
  # which is captured in ENG-9749 and ENG-9792

  belongs_to :rescue_provider

  # Where is this site candidate starting from?
  def starting_location
    site.location
  end

  #
  # Create eligible and ineligible site candidates for a job auction, by:
  # 1) finding providers within an acceptable range
  # 2) creating site candidates from those providers
  # 3) calling Google's Distance Matrix Service to get each site's ETA and distance
  # 4) using that to determine each site's eligibility
  #
  def self.create_all_for_job_auction_by_radius(job:, auction:)
    created_site_candidates = []

    # Step 1: Find nearby providers to the job; overestimate so
    # we make sure we include everyone
    log :debug, "Job::Candidate::Site.create_all_for_job_auction_by_radius calling JobGetClosestProvidersService", job: job.id

    nearby_providers = JobGetClosestProvidersService.new(
      job,
      job.fleet_company,
      max_providers: auction.max_bidders * 2,
      max_distance: auction.max_candidate_distance,
      live_only: true,
      for_auto_assignment: true,
      dispatch_company: User.swoop_auto_dispatch.company,
      site_open_for_business: true,
    ).call

    # Step 2: Create site candidates for all of the nearby providers
    nearby_providers.each do |provider|
      created_site_candidates << create!(
        job: job,
        company: provider.provider,
        lat: provider.site.location.lat,
        lng: provider.site.location.lng,
        site: provider.site,
        rescue_provider: provider
      )
    end

    # Step 3: For each site candidate, create a time & distance estimate
    create_estimates_for_candidates!(job, auction, created_site_candidates)

    created_site_candidates
  end

  def self.create_all_for_job_auction_from_list_of_sites(job:, auction:, sites:)
    # Step 0.0: Raise error if supplied list isn't an array
    raise Job::CannotGenerateBidsBecauseSuppliedListIsInvalidError unless sites.is_a?(Array)

    created_site_candidates = []

    # Step 0.1: Return empty array, if sites list is blank
    if sites.empty?
      log :debug, "Job::Candidate::Site.create_all_for_job_auction_from_list_of_sites - supplied `sites` list is empty"
      return created_site_candidates
    end

    log :debug, "Job::Candidate::Site.create_all_for_job_auction_from_list_of_sites calling JobGetClosestProvidersService", job: job.id

    # Step 1: Create site candidates for the sites list
    sites.each do |site|
      created_site_candidates << create!(
        job: job,
        company: site.company,
        lat: site.location.lat,
        lng: site.location.lng,
        site: site,
        rescue_provider: nil
      )
    end

    # Step 2: For each site candidate, create a time & distance estimate
    create_estimates_for_candidates!(job, auction, created_site_candidates)

    created_site_candidates
  end

  def self.create_estimates_for_candidates!(job, auction, created_site_candidates)
    # Using Google Distance Matrix Service to create the estimates
    if created_site_candidates.length > 0
      ::CreateEstimatesForJobCandidatesService.new(
        candidates: created_site_candidates,
        auction: auction,
        job: job
      ).call
    end
  end

  private_class_method :create_estimates_for_candidates!

end
