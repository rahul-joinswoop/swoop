# frozen_string_literal: true

# This class is for building up a list of candidate companies with the closest
# sites/rescue vehicle entities. The size of list of candidate companies will be
# capped by the max_bidders size and max_distance in miles.
class Job::Candidate::Collector

  def initialize(max_bidders, max_distance)
    raise(ArgumentError, "max_bidders must be > 0") unless max_bidders > 0
    @max_bidders = max_bidders
    @max_distance = max_distance
    @company_candidates = {}
  end

  # Add a site/rescue vehicle as a candidate.
  def add(entity, distance)
    return if distance.nil? || distance.nan? || distance > @max_distance

    candidates = @company_candidates[entity.company_id]
    unless candidates
      candidates = []
      @company_candidates[entity.company_id] = candidates
    end
    candidates << { entity: entity, distance: distance }
  end

  # Return all eligible entities. Only entities from the closest max_bidders companies
  # will be returned. Also only the entities closer that the closest entity from the furthest
  # bidder.
  def eligible_entities
    entities = []
    bidders = Set.new
    @company_candidates.values.flatten.sort_by { |candidate| candidate[:distance] }.each do |candidate|
      entity = candidate[:entity]
      bidders << entity.company_id
      entities << entity
      break if bidders.size >= @max_bidders
    end
    entities
  end

end
