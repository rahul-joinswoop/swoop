# frozen_string_literal: true

# This model captures the billing type entered by an agent when a partner
# company is assigned to a job including an optional amount being paid via
# VCC (virtual credit card) at the time o assignment.
#
# This information is ultimately used to generate the invoice which is the
# source of record for the information. It is entered here so that we have
# a record of what the agent originally entered (especially the VCC amount)
# and so it is available to the logic that asynchronously generates the invoice.
class Job::BillingInfo < ApplicationRecord

  include ValidateCurrency

  belongs_to :job, inverse_of: :billing_info, optional: false
  belongs_to :rescue_company, optional: false
  belongs_to :agent, class_name: "User", optional: true
  belongs_to :goa_agent, class_name: "User", optional: true

  validates :billing_type, inclusion: [BillingType::VCC, BillingType::ACH]
  validates :vcc_amount, numericality: { greater_than: 0, less_than: 1_000_000, allow_nil: true }, absence: { unless: :vcc? }
  validates :goa_amount, numericality: { greater_than: 0, less_than: 1_000_000, allow_nil: true }, absence: { unless: :vcc? }
  validates :agent, presence: { if: -> { vcc_amount.present? } }
  validates :goa_agent, presence: { if: -> { goa_amount.present? } }
  validate_currency_matches :rescue_company

  def vcc?
    billing_type == BillingType::VCC
  end

  def ach?
    billing_type == BillingType::ACH
  end

end
