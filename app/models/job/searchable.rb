# frozen_string_literal: true

# ElasticSearch specific definitions for jobs.
module Job::Searchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    index_name Swoop::ElasticSearch.index_name(:jobs_index)
    document_type 'job'

    scope :elastic_search_backfill, -> {
      finder = elastic_search_preload
      ts = Time.zone.parse(ENV['MODEL_TS']) if ENV['MODEL_TS'].present?
      if ts.present?
        finder = joins('LEFT JOIN invoicing_ledger_items ON invoicing_ledger_items.job_id = jobs.id')
        finder = finder.where('jobs.updated_at >= ? OR invoicing_ledger_items.updated_at >= ?', ts, ts)
      end
      finder
    }

    # Associations that should be preloaded when loading ElasticSearch documents.
    scope :elastic_search_preload, -> {
      preload([
        :audit_job_statuses,
        :live_invoices,
        :rescue_company,
        :fleet_company,
        :customer,
        :stranded_vehicle,
        :account,
        :service_location,
        :drop_location,
        :service_code,
        :storage,
        :department,
      ])
    }

    after_commit on: [:create, :update] do
      reindex
    end

    def reindex
      Indexer.perform_async(:index, Job.name, id)
    end

    after_commit on: [:destroy] do
      Indexer.perform_async(:delete, Job.name, id)
    end

    settings index: {
      number_of_shards: 3,
      number_of_replicas: 1,
      analysis: {
        char_filter: {
          letters_numbers_only: {
            type: "pattern_replace",
            pattern: "[^a-zA-Z0-9]",
            replacement: "",
          },
        },
        filter: {
          preserve_asciifolding: {
            type: "asciifolding",
            preserve_original: true,
          },
          strip_punctuation: {
            type: "pattern_replace",
            pattern: "\\p{P}",
            replacement: "",
          },
        },
        analyzer: {
          identifier: {
            type: "custom",
            tokenizer: "keyword",
            filter: ["lowercase", "trim"],
            char_filter: ["letters_numbers_only"],
          },
          name_analyzer: {
            type: "custom",
            tokenizer: "standard",
            filter: ["standard", "lowercase", "preserve_asciifolding", "strip_punctuation"],
          },
          case_insensitive_keyword: {
            type: "custom",
            tokenizer: "keyword",
            filter: ["lowercase", "trim"],
          },
        },
      },
    } do
      mappings dynamic: 'false' do
        indexes :notes,           analyzer: :english, index_options: :offsets
        indexes :partner_notes,   analyzer: :english, index_options: :offsets
        indexes :dispatch_notes,  analyzer: :english, index_options: :offsets
        indexes :fleet_notes,     analyzer: :english, index_options: :offsets
        indexes :driver_notes,    analyzer: :english, index_options: :offsets
        indexes :swoop_notes,     analyzer: :english, index_options: :offsets
        indexes :id,              type: :integer
        indexes :original_job_id, type: :integer
        indexes :po_number, analyzer: :identifier
        indexes :status, analyzer: :case_insensitive_keyword
        indexes :type, analyzer: :case_insensitive_keyword
        indexes :ref_number, analyzer: :identifier
        indexes :policy_number, analyzer: :identifier
        indexes :unit_number, analyzer: :identifier
        indexes :claim_number, analyzer: :identifier
        indexes :created_at,    type: :date
        indexes :dispatched_at, type: :date
        indexes :completed_at,  type: :date
        indexes :stored_at,     type: :date
        indexes :invoice_states, analyzer: :case_insensitive_keyword
        indexes :paid_invoices?, type: :boolean
        indexes :unpaid_invoices?, type: :boolean
        indexes :storage_type_id, type: :integer
        indexes :account_id, type: :integer

        indexes :account do
          indexes :name, analyzer: :name_analyzer
        end
        indexes :rescue_company do
          indexes :id, type: :integer
          indexes :name, analyzer: :name_analyzer
        end
        indexes :fleet_company do
          indexes :id, type: :integer
          indexes :name, analyzer: :name_analyzer
        end
        indexes :service_location do
          indexes :address
          indexes :state
          indexes :city
          indexes :street
          indexes :zip
          indexes :country
        end
        indexes :drop_location do
          indexes :address
          indexes :state
          indexes :city
          indexes :street
          indexes :zip
          indexes :country
        end
        indexes :stranded_vehicle do
          indexes :vin_indexed, analyzer: :identifier
          indexes :vin_reverse_indexed, analyzer: :identifier
          indexes :make
          indexes :model
          indexes :license, analyzer: :identifier
          indexes :serial_number, analyzer: :identifier
          indexes :year, type: :integer
        end
        indexes :customer do
          indexes :full_name, analyzer: :name_analyzer
          indexes :index_phone
        end
        indexes :service_code do
          indexes :name
        end
        indexes :storage do
          indexes :site_id, type: :integer
          indexes :site_name, analyzer: :name_analyzer
        end
        indexes :fleet_site do
          indexes :id, type: :integer
          indexes :name, analyzer: :name_analyzer
        end
        indexes :department do
          indexes :name, analyzer: :name_analyzer
        end
      end
    end
  end

  def as_indexed_json(_options = {})
    as_json(
      only: [
        :notes,
        :partner_notes,
        :dispatch_notes,
        :fleet_notes,
        :driver_notes,
        :swoop_notes,
        :id,
        :type,
        :original_job_id,
        :po_number,
        :created_at,
        :status,
        :invoice_id,
        :ref_number,
        :policy_number,
        :unit_number,
        :storage_type_id,
        :account_id,
      ],
      methods: [
        :dispatched_at,
        :completed_at,
        :stored_at,
        :invoice_states,
        :paid_invoices?,
        :unpaid_invoices?,
        :claim_number,
      ],
      include: {
        rescue_company: { only: [:name, :id] },
        fleet_company: { only: [:name, :id] },
        customer: { only: [], methods: [:full_name, :index_phone] },
        stranded_vehicle: { only: [:make, :model, :license, :year, :serial_number], methods: [:vin_indexed, :vin_reverse_indexed] },
        account: { only: :name },
        service_location: { only: [:address, :state, :city, :street, :zip, :country] },
        drop_location: { only: [:address, :state, :city, :street, :zip, :country] },
        service_code: { only: :name },
        storage: { only: [:site_id, :site_name] },
        fleet_site: { only: [:id, :name] },
        department: { only: :name },
      }
    )
  end

end
