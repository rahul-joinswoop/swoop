# frozen_string_literal: true

class ViewAlert < ApplicationRecord

  belongs_to :job

end
