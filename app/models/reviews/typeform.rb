# frozen_string_literal: true

module Reviews
  class Typeform < WebReview

    include LoggableJob

    def update_from_response(response)
      logger.debug "Typeform Response: #{response.inspect}"

      return if response.blank? # Guard

      form_id    = response['form_id']
      answers    = response['answers']
      definition = response['definition']

      # WTF why isnt this there?
      if definition.present? && definition['fields'].present?
        fields = definition['fields']
        q1 = answer_hunt('informed', fields, answers)
        q2 = answer_hunt('ETA', fields, answers)
        q3 = answer_hunt('recommend', fields, answers)

        feedback   = answer_hunt('experience', fields, answers)
        feedback ||= answer_hunt('improve', fields, answers)

      else
        q1, q2, q3, feedback = answers
      end

      attribs = {
        form_id: form_id,
        nps_question1: q1.present? ? q1['number'] : nil,
        nps_question2: q2.present? ? q2['number'] : nil,
        nps_question3: q3.present? ? q3['number'] : nil,
        nps_feedback: feedback.present? ? feedback['text'] : '',
      }

      log :debug, "Typeform updated review: #{attribs.inspect}", job: job_id

      update_attributes!(attribs)

      send_to_slack(true, 3)
      nps_slack_feedback_message
    end

    def answer_hunt(question, fields, answers)
      fields.each_with_index do |val, findx|
        return answers[findx] if val['title'].to_s[question]
      end
      nil
    end

  end
end
