# frozen_string_literal: true

module Reviews
  class WebReview < Review

    def initialize(*args)
      super(*args)
      init_branding
    end

    def init_branding
      # Previously, we were basing this from the "Whitelabel Survey" feature.
      # After its removal, we're defaulting to true (it was decided that
      # "Whitelabel Survey" would _always_ be active.
      self.branding = true
    end

    # Overload
    def redirect_url
      "#{url}?job=#{job_id}&"\
      "first_name=#{user.first_name}&last_name=#{user.last_name}"
    end

    def iframe_params
      "?job=#{job_id}&"\
      "first_name=#{user.first_name}&last_name=#{user.last_name}"
    end

    def url
      company.try(:review_url) || default_url
    end

    def send_review_sms
      Sms::BrandedReviewLink.send_notification(job)
    end

    def default_url
      if company.fleet?
        ENV['DEFAULT_FLEET_REVIEW_URL']
      else
        ENV['DEFAULT_PARTNER_REVIEW_URL']
      end
    end

    def process_response(*_ignored)
      # Eat it
    end

  end
end
