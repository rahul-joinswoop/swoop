# frozen_string_literal: true

##
# PoiSites (aka Places) are common points of interest like collision centers in
# the new job screen.
#
# Use case: Fleets and partners have places that their jobs are frequently
# associated with, and they'd like a quicker way to add those to jobs without
# searching for them anew every time.
class PoiSite < Site

  accepts_nested_attributes_for :manager

  def render_to_hash
    PoiSiteSerializer.new(self).serializable_hash
  end

end
