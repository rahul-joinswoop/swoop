# frozen_string_literal: true

class Commands::TowOut < Command

  def process
    logger.debug "COMMAND called with #{json}"

    hash = JSON.parse json
    params = ActionController::Parameters.new(hash)
    args = params.require(:job).permit(:id)

    self.target = Job.find(args[:id])
    save!

    job = target
    job.released
    job.save!
    copy = job.copy_job(nil, false)
    copy.service_location = copy.drop_location
    copy.drop_location = nil
    copy.original_job_id = job.id
    copy.rescue_company_id = job.rescue_company_id
    copy.account_id = job.account_id
    copy.service_code = ServiceCode.find_by(name: "Tow")
    copy.site_id = job.site_id
    copy.partner_dispatcher_id = user_id
    copy.scheduled_for = nil
    copy.hack_create_ajs('Initial')
    copy.move_to_pending
    copy.save!
    if !copy.estimate
      CreateOrUpdateEstimateForJob.perform_async(copy.id)
    end

    self.applied = true
    save!

    # TODO: send original job over WS to pick up the child
    copy
  end

end
