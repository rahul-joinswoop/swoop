# frozen_string_literal: true

class JobEtaExplanation < ApplicationRecord

  NONE = "None"
  OTHER = "Other"
  TRAFFIC = "Traffic"
  EVENT_IN_AREA = "Event In Area"
  CONSTRUCTION = "Construction"
  RURAL_DISABLEMENT = 'Rural Disablement'
  SERVICE_PROVIDER_ISSUE = 'Service Provider Issue'
  WEATHER = 'Weather'

end
