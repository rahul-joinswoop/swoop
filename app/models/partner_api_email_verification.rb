# frozen_string_literal: true

# Model used for the Verify Email flow
class PartnerAPIEmailVerification < PasswordRequest

  include PasswordRequestPartnerAPISupport
  store_accessor :custom_data, :redirect_uri
  validate :redirect_uri_must_be_valid

  # overridden from PasswordRequest because we don't need to update any access tokens here
  def invalidate!
    # TODO - why update_attributes! and not update!
    update_attributes! valid_request: false, used_at: Time.current
  end

  private

  def redirect_uri_must_be_valid
    # don't throw this error if application is blank, we're already throwing an error
    # for that above
    return true if application.blank?
    unless Doorkeeper::OAuth::Helpers::URIChecker.valid_for_authorization?(redirect_uri, application.redirect_uri)
      errors.add(:redirect_uri)
    end
  end

  def trigger
    Email::PartnerAPIEmailVerificationWorker.perform_async id
    self
  end

end
