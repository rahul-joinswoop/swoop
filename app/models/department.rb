# frozen_string_literal: true

class Department < BaseModel

  belongs_to :company

  validates :name, presence: true, uniqueness: { scope: [:company_id, :deleted_at] }

  scope :live, -> { where(deleted_at: nil) }

  after_commit :update_dependent_indexes, on: :update

  def listeners
    listeners = Set.new

    listeners << company if company.present?

    listeners
  end

  def render_to_hash
    DepartmentSerializer.new(self).serializable_hash
  end

  private

  # Update search indexes that reference this record.
  def update_dependent_indexes
    if saved_change_to_name?
      SearchIndex::DepartmentDependentIndexUpdateWorker.perform_async(id)
    end
  end

end
