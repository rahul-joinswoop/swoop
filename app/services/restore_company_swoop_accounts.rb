# frozen_string_literal: true

class RestoreCompanySwoopAccounts

  include Interactor

  before do
    @company = context.company
  end

  def call
    return if deleted_swoop_accounts.blank?

    swoop_accounts.each do |swoop_account|
      # Soft delete all accounts to properly handle uniqueness validation on name
      swoop_account.soft_delete!

      next if swoop_account == newest_swoop_account

      swoop_account.jobs.update_all(account_id: newest_swoop_account.id)
    end

    newest_swoop_account.soft_restore!
  end

  private

  def newest_swoop_account
    @newest_swoop_account ||= swoop_accounts.order(:id).last
  end

  def swoop_accounts
    @swoop_accounts ||= @company.accounts.where(name: "Swoop")
  end

  def deleted_swoop_accounts
    @deleted_swoop_accounts ||= swoop_accounts.where.not(deleted_at: nil)
  end

end
