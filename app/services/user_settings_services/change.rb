# frozen_string_literal: true

module UserSettingsServices
  class Change

    include SwoopService

    attr_reader :user_setting

    def initialize(api_user:, key:, value:)
      raise ArgumentError if [api_user, key, value].any? { |param| param.nil? }

      @api_user = api_user
      @key = key
      @value = value

      Rails.logger.debug("UserSettingsServices::Change params: api_user.id = #{@api_user.id}, " \
        "key = #{@key}, value = #{@value}")
    end

    def execute
      @user_setting = UserSetting.where(
        user: @api_user,
        key: @key
      ).first_or_create!

      @user_setting.value = @value

      @user_setting.save!

      self
    end

  end
end
