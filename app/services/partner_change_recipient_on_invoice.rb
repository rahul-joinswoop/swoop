# frozen_string_literal: true

# Changes Recipient on an Invoice and Updates all line items for new job rate

class PartnerChangeRecipientOnInvoice < Service

  include LoggableJob

  attr_reader :job

  def initialize(job)
    log :debug, "INVOICE PartnerChangeAccountOnInvoice called", job: job.id
    raise ArgumentError, "INVOICE(#{job.id}) PartnerChangeAccountOnInvoice - Missing invoice" unless job.invoice
    raise ArgumentError, "INVOICE(#{job.id}) PartnerChangeAccountOnInvoice - Invoice has been sent" unless job.invoice.editable?
    @job = job
  end

  def call
    Rails.logger.debug "Updating Account"
    recipient, recipient_company = JobGetInvoiceRecipientAndCompanyService.new(@job).call
    @job.invoice.recipient = recipient
    @job.invoice.recipient_company = recipient_company
    log :debug, "before UpdateInvoiceForJob has #{@job.invoice.recipient_id} and #{@job.invoice.recipient_company_id}", job: @job.id
    UpdateInvoiceForJob.new(@job.invoice,
                            @job,
                            @job.get_rate).call
    log :debug, "after UpdateInvoiceForJob has #{@job.invoice.recipient_id} and #{@job.invoice.recipient_company_id}", job: @job.id
  end

end
