# frozen_string_literal: true

class ViewAlertService < Service

  def self.customer_says_not_on_site(job, value)
    ViewAlertService.set_attribute(job, :customer_says_not_on_site, value)
  end

  def self.customer_says_not_complete(job, value)
    ViewAlertService.set_attribute(job, :customer_says_not_complete, value)
  end

  def self.cancel_if_necessary(job, status)
    Rails.logger.debug "ViewAlertService::cancel_if_necessary #{job.id}, status:#{status}"

    alert = job.view_alert
    if alert
      if alert.customer_says_not_on_site
        Rails.logger.debug "ViewAlertService::cancel_if_necessary processing 'not on site'"
        case status
        when "On Site"
          ViewAlertService.customer_says_not_on_site(job, nil)
        end

      end
      if alert.customer_says_not_complete
        Rails.logger.debug "ViewAlertService::cancel_if_necessary processing 'not complete'"
        case status
        when "Completed"
          ViewAlertService.customer_says_not_complete(job, nil)
        end

      end
    end
  end

  def self.set_attribute(job, key, value)
    Rails.logger.debug "ViewAlertService::set_attribute setting view alert:#{key}, value:#{value}"
    alert = job.view_alert
    if !alert
      alert = job.build_view_alert
    end
    alert.assign_attributes(key => value)
    #    alert.save!
    alert.job.save!
  end

end
