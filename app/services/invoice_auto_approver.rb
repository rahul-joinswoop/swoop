# frozen_string_literal: true

class InvoiceAutoApprover

  include Interactor::Organizer

  before do
    Rails.logger.debug "#{self.class.name} starting auto-approval for #{context}"
  end

  after do
    Rails.logger.debug "INVOICE(#{context.invoice.id}): auto-approved"
  end

  organize CheckForInvoice,
           CheckInvoiceState,
           CheckInvoiceSender,
           CheckInvoiceRecipient,
           CheckInvoiceBillingType,
           CheckJobStatus,
           CheckForHistoryItems,
           CheckInvoiceAmount,
           CheckInvoiceBalance,
           CheckInvoiceSentDate,
           ApproveInvoice

end
