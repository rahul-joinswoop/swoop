# frozen_string_literal: true

class IsscDispatchAccepted < Service

  def initialize(event)
    @event = event
    @dispatchid = event[:DispatchID]
  end

  def self.accept_job(event, job)
    if event[:AuthorizationNumber]
      job.po_number = event[:AuthorizationNumber]
    end

    if event[:Coverage]
      job.notes += "Additional Coverage:" + JSON.pretty_generate(event[:Coverage]).tr('"{}[]', '') + "\n"
    end

    phone = event[:MemberCallBackNumber]
    job.customer.update_attribute(:phone, phone) if phone.present?

    job.issc_accepted
    job.issc_dispatch_request.requester_accepted

    job
  end

  def call
    raise ISSC::DispatchJobNotFound unless @dispatchid

    Job.transaction do
      job = Job.joins(:issc_dispatch_request).where('issc_dispatch_requests.dispatchid = ?', @dispatchid).take

      raise ISSC::DispatchJobNotFound unless job

      IsscDispatchAccepted.accept_job(@event, job)
    end
  end

end
