# frozen_string_literal: true

module AnalyticsData
  class Base

    COMPANY_TYPE = {
      'SuperCompany' => 'Swoop',
      'RescueCompany' => 'Partner',
      'FleetCompany' => 'FleetCompany',
    }.freeze

    NOT_PROVIDED = 'not provided'

    private

    def user_type
      return 'API' if @api_application

      if @api_user.has_role? :admin
        return Role::ADMIN.to_s.capitalize
      elsif @api_user.has_role?(:driver) && !@api_user.has_role?(:dispatcher)
        return Role::DRIVER.to_s.capitalize
      end

      Role::DISPATCHER.to_s.capitalize
    end

    # We can have GraphQL @api_application instead of @api_user.
    # Is the logic for @api_application correct?
    def user_or_application_values(message_body)
      if @api_user
        message_body.deep_merge!({
          user_id: @api_user.to_ssid,
          properties: {
            company: @api_user.company&.name,
            userId: @api_user.to_ssid,
            company_type: COMPANY_TYPE[@api_user.company&.type],
            subscriptionStatus: @api_user.company&.subscription_status&.name,
          },
        })
      elsif @api_application
        if @api_application.owner_type == "Company" # can we have any other type?
          company = Company.find(@api_application.owner_id)

          message_body.deep_merge!({
            user_id: company.to_ssid,
            properties: {
              company: company.name,
              userId: company.to_ssid,
              company_type: COMPANY_TYPE[company.type],
              subscriptionStatus: company.subscription_status&.name,
            },
          })
        end
      end

      message_body
    end

  end
end
