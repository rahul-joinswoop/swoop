# frozen_string_literal: true

# A service to push Create Job event to the Analytics tool.
#
# Usage: AnalyticsData::CreateJob.new(...).call
#
# or
#
# AnalyticsData::CreateJob.new(...).push_message
#
module AnalyticsData
  class CreateJob < Base

    def initialize(job:, user_agent_hash:, api_company:)
      @job = job
      @api_company = api_company
      @user_agent_hash = user_agent_hash
    end

    def call
      message_body = {
        user_id: @api_company.to_ssid,
        event: "Create Job",
        properties: {
          category: 'Jobs',
          platform: @user_agent_hash[:platform],
          version: @user_agent_hash[:version],
          job_id: @job.id,
          company: @api_company.name,
        },
      }

      Analytics.track(message_body)
    end
    alias_method :push_message, :call

  end
end
