# frozen_string_literal: true

# A service to push Job status change event to the Analytics tool.
#
# It's mainly used by <partner|fleet|root>/job_status_controller to track
# job completed event.
#
# If job status is changed to any other status different from Completed,
# the message will not be sent. Though the component should be ready to send on any
# job status change if the guard is removed at beginning of call method.
#
# Usage: AnalyticsData::JobStatus.new(...).call
# or
# AnalyticsData::JobStatus.new(...).push_message
#
# Its logic is based on deprecated logic from frontend code.
module AnalyticsData
  class JobStatus < Base

    include LoggableJob

    def initialize(job:, user_agent_hash:, referer:, api_user: nil, api_application: nil, status: nil)
      @job = job
      @status = status || @job.human_status
      @user_agent_hash = user_agent_hash
      @referer = referer
      @api_user = api_user
      @api_application = api_application
    end

    def call
      if @api_user.blank? && @api_application.blank?
        raise ArgumentError, "#{self.class.name} user and application not provided"
      end

      return unless [
        ::JobStatus::ID_MAP[::JobStatus::ACCEPTED],
        ::JobStatus::ID_MAP[::JobStatus::REJECTED],
        ::JobStatus::ID_MAP[::JobStatus::DISPATCHED],
        ::JobStatus::ID_MAP[::JobStatus::EN_ROUTE],
        ::JobStatus::ID_MAP[::JobStatus::ON_SITE],
        ::JobStatus::ID_MAP[::JobStatus::COMPLETED],
      ].include?(@status)

      log :debug, "about to push Analytics message for status change", job: @job.id

      message_body = {
        event: "Job #{@status}",
        properties: {
          account: @job.account&.name || '',
          job_id: @job.id,
          service: @job.service_code&.name,
          platform: @user_agent_hash[:platform],
          version: @user_agent_hash[:version],
          user_type: user_type,
          referer: @referer,
        },
      }

      message_body = user_or_application_values(message_body)

      Analytics.track(message_body)
    end
    alias_method :push_message, :call

  end
end
