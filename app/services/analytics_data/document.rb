# frozen_string_literal: true

# A service to push Document status change event to the Analytics tool.
#
# Usage: AnalyticsData::Document.new(...).call
# or
# AnalyticsData::Document.new(.d..).push_message
#
# Its logic is based on deprecated logic from frontend code.
module AnalyticsData
  class Document < Base

    def initialize(document:, user_agent_hash:, referer:, api_user: nil, api_application: nil)
      @document = document
      @user_agent_hash = user_agent_hash
      @referer = referer
      @api_user = api_user
      @api_application = api_application
    end

    def call
      if @api_user.blank? && @api_application.blank?
        raise ArgumentError, "#{self.class.name} user and application not provided"
      end

      # for now only LocationImage(Image) and SignatureImage (Signature) should be sent
      return unless [
        AttachedDocument::LocationImage,
        AttachedDocument::SignatureImage,
      ].include?(@document.class)

      event = @document.is_a?(AttachedDocument::LocationImage) ? 'Image Added' : 'Signature Added'

      Rails.logger.debug(
        "Document(#{@document.id}) #{self.class.name} about to push Analytics message for document"
      )

      message_body = {
        event: event,
        properties: {
          job_id: @document.job_id,
          platform: @user_agent_hash[:platform],
          version: @user_agent_hash[:version],
          user_type: user_type,
          referer: @referer,
        },
      }

      message_body = user_or_application_values(message_body)

      Analytics.track(message_body)
    end
    alias_method :push_message, :call

  end
end
