# frozen_string_literal: true

# A service to identify a user / company to analytics
#
# Usage: AnalyticsData::Idenfity.new(...).call

module AnalyticsData
  class Identify < Base

    def initialize(user_or_company)
      @user_or_company = user_or_company
    end

    def call
      body = case @user_or_company
             when ::User
               {
                 user_id: @user_or_company.to_ssid,
                 traits: {
                   company: @user_or_company.company&.name,
                 },
               }
             when ::Company
               {
                 user_id: @user_or_company.to_ssid,
                 traits: {
                   company: @user_or_company.name,
                 },
               }
             else
               raise ArgumentError, "#{self.class.name} user or company not provided"
             end
      Analytics.identify(body)
      Analytics.flush
    end

  end
end
