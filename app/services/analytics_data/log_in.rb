# frozen_string_literal: true

# Usage: AnalyticsData::LogIn.new(...).call
# or
# AnalyticsData::LogIn.new(...).push_message

module AnalyticsData
  class LogIn < Base

    def initialize(user_agent:, referer:, api_user: nil, api_application: nil)
      @user_agent_hash = UserAgentParser.new(user_agent).call
      @referer = referer
      @api_user = api_user
      @api_application = api_application
    end

    def call
      if @api_user.blank? && @api_application.blank?
        raise ArgumentError, "#{self.class.name} user and application not provided"
      end

      message_body = {
        event: "Log In",
        properties: {
          platform: @user_agent_hash[:platform],
          version: @user_agent_hash[:version],
          referer: @referer || NOT_PROVIDED,
          user_type: user_type,
        },
      }
      message_body = user_or_application_values(message_body)

      Analytics.track(message_body)
    end
    alias_method :push_message, :call

  end
end
