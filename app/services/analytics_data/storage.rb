# frozen_string_literal: true

# A service to push Storage change event to the Analytics tool.
#
# It's mainly used by <partner|fleet|root>/job_status_controller to track
# job completed event.
#
#
# Usage: AnalyticsData::Storage.new(...).call
# or
# AnalyticsData::Storage.new(...).push_message
#
# Its logic is based on deprecated logic from frontend code.
module AnalyticsData
  class Storage < Base

    include LoggableJob

    def initialize(job:, user_agent_hash:, referer:, api_user: nil, api_application: nil)
      @job = job
      @user_agent_hash = user_agent_hash
      @referer = referer
      @api_user = api_user
      @api_application = api_application
    end

    def call
      if @api_user.blank? && @api_application.blank?
        raise ArgumentError, "#{self.class.name} user and application not provided"
      end

      log :debug, "about to push Analytics message for storage change", job: @job.id

      message_body = {
        event: "Storage #{@job.will_store ? 'Added' : 'Removed'}",
        properties: {
          account: @job.account&.name || '',
          job_id: @job.id,
          service: @job.service_code.name,
          platform: @user_agent_hash[:platform],
          version: @user_agent_hash[:version],
          user_type: user_type,
          referer: @referer,
        },
      }

      message_body = user_or_application_values(message_body)

      Analytics.track(message_body)
    end
    alias_method :push_message, :call

  end
end
