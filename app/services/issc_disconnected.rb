# frozen_string_literal: true

require 'issc'

class IsscDisconnected < Service

  def initialize(data, autoreconnect: true, connection_manager: ISSC::ConnectionManager.new)
    @autoreconnect = autoreconnect
    @data = data
    @issccon = connection_manager
  end

  def call
    # Put all the providers in a logged-out state

    if (@data[:Result] == "Success") || (@data[:Description] == "Already disconnected")
      @issccon.set_connection_status ISSC::ConnectionManager::DISCONNECTED
      # IsscFixLogins.perform_async
    end

    @issccon.ensure_connected do
      Rails.logger.debug "Ensuring connection!"
    end
  end

end
