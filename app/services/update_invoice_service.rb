# frozen_string_literal: true

#
# UPDATE INVOICE SERVICE
#
# Given an invoice that already exists:
# 1. Generate new line items from the given rate
# 2. Create or update those line items on the invoice
#
# N.B. This class does not save the invoice; it saves the new
# line items, but not the invoice itself, therefore it should only
# be used to update the contents of the invoice, not the invoice
# itself
#
# This is an abstract parent class to update invoices generically
# for an Invoicable, and therefore you should not call it directly.
#
# Instead:
# - if you wish to update a job's invoice, call the
#   UpdateInvoiceForJob service
# - if you wish to populate a candidate's invoice, call
#   the PopulateInvoiceForJobCandidate service
# - if you wish to generate a hypothetical variation of an invoice
#   based on an existing invoice, call the UpdateInvoiceForJobInMemory service
# - if you want to update an invoice for another Invoicable type,
#   create a similar subclass and override the abstract
#   methods (below) with concrete implementations
#
# N.B. If the invoice can't be updated, this will silently no-op:
# this is expected behavior so that callers do not need to worry
# about whether an invoice can be updated or not -- if you do want
# to know if this service will update the invoice, check #invoice_can_be_updated
#

class CannotInitializeUpdateInvoiceServiceWithoutInvoiceError < ArgumentError; end
class CannotInitializeUpdateInvoiceServiceWithoutJobError < ArgumentError; end
class CannotInitializeUpdateInvoiceServiceWithoutRateError < ArgumentError; end

class UpdateInvoiceService < Service

  include LoggableJob

  def initialize(invoice, job, rate)
    raise CannotInitializeUpdateInvoiceServiceWithoutInvoiceError unless invoice.is_a?(Invoice)
    raise CannotInitializeUpdateInvoiceServiceWithoutJobError unless job.is_a?(Job)
    raise CannotInitializeUpdateInvoiceServiceWithoutRateError unless rate.is_a?(Rate)

    @invoice = invoice
    @job = job
    @rate = rate
  end

  def call
    # Step 1: Ensure the invoice is in a state to be updated -- and if it isn't,
    # silently no-op
    return unless invoice_can_be_updated

    # Step 2: Recalculate the invoice's line items
    # (as long as it's not a storage job -- this service doesn't update
    # storage job invoices)
    unless @job.storage_job?

      # Step 2.1: Verify the provided rate is valid
      @rate.validate!
      log :debug, "call using rate ID #{@rate.id}", job: @job.id

      # Step 2.2: If this invoice already exists, delete any line items
      # that we're about to recalculate
      delete_existing_lineitems

      # Step 2.3: Generate new line items, based on the rate
      new_line_item_hashes = @rate.calculate_line_items(invoicable)

      # Step 2.4: Add rate additions to the new line items
      # Need to override service code for GOA rate to match the GOA pseudo service code
      service_code_id = (@rate.goa? ? ServiceCode.goa.id : @job.service_code_id)
      default_vehicle_rate = Rate.inherit_type(
        @invoice.sender.id,
        account_for_default_vehicle_rate.id,
        invoicable.site&.id,
        invoicable.issc_contractor_id,
        service_code_id,
        nil,
        @rate.type
      )

      @rate.add_rate_additions_for_type(new_line_item_hashes, default_vehicle_rate, @job.id)

      # Step 2.5: Build or overwrite every lineitem on the invoice
      new_line_item_hashes.each do |new_line_item_hash|
        @invoice.build_lineitem(new_line_item_hash, @job, @rate)
      end

      # Step 2.6: Apply billing infomation entered by the agent when assigning the job
      apply_job_billing_info

      # Step 2.7: If the invoice has a minimum rate, destroy all line items except for
      # storage, minimum, and tax
      if @invoice.minimum_rate_line_item
        log :debug, "found minimum rate line item with ID #{@invoice.minimum_rate_line_item.id}", job: @job.id
        @invoice.line_items
          .where(job: @job)
          .where.not(description: [Rate::STORAGE, Rate::MINIMUM, Rate::TAX])
          .where("user_created is not true")
          .destroy_all
      end

      # Step 2.8: Fix lineitem percentages
      # ENG-8478: Clean up "evil hack" in Invoice#fix_lineitem_percentages
      @invoice.fix_lineitem_percentages
    end

    # Step 3: Bump the storage tick forward if this is an active storage job
    bump_storage_tick

    # Step 4: Ensure the invoice version is current
    @invoice.version = Invoice::VERSION
  end

  #
  # Return true or false depending on whether the invoice
  # can be safely updated
  #
  def invoice_can_be_updated
    raise UncallableAbstractParentMethodError
  end

  #
  # If this is a storage job invoice, implement any logic
  # to bump the storage tick here
  #
  def bump_storage_tick
    raise UncallableAbstractParentMethodError
  end

  #
  # If the invoice has existing line items, delete them here
  #
  # Be sure the @invoice is fresh (e.g., by calling #reload) before
  # exiting this method
  #
  def delete_existing_lineitems
    raise UncallableAbstractParentMethodError
  end

  #
  # Return the Invoicable object that we'll use to generate
  # rates and line items
  #
  def invoicable
    raise UncallableAbstractParentMethodError
  end

  #
  # If the agent entered billing or payment information, like VCC, add
  # those payments here and update the invoice as necessary
  #
  def apply_job_billing_info
    raise UncallableAbstractParentMethodError
  end

  #
  # To calculate rate additions for the invoice, the default
  # vehicle rate requires an Account -- return it here
  #
  def account_for_default_vehicle_rate
    raise UncallableAbstractParentMethodError
  end

end
