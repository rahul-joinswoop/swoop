# frozen_string_literal: true

class StorageTick < Service

  include LoggableJob

  attr_reader :job

  def initialize(job)
    @job = job
  end

  def call
    if job.invoice
      if job.storage
        storage_rate = Job.get_storage_or_create_zero_storage_rate(job)
        storage_rate.validate!
        log :debug, "StorageTick using STORAGE_RATE(#{storage_rate.id})", job: job.id
        lis = storage_rate.calculate_line_items(job, false)
        update_storage_model(storage_rate, lis)

        add_additional_items(storage_rate)
      end

      true
    else
      log :warn, "STORAGE No invoice found for job", job: job.id
      if !job.storage && !job.estimate
        CreateOrUpdateEstimateForJob.perform_async(job.id)
      else
        CreateOrUpdateInvoice.perform_async(job.id)
      end
    end
  end

  private

  def update_storage_model(new_rate, lis)
    Rails.logger.debug "STORAGE_RATE(#{new_rate.id}): update_storage_model called with #{lis}"
    lis.each do |args|
      li = job.invoice.mem_find_storage(job)

      args[:rate] = new_rate
      if li
        Rails.logger.debug "STORAGE_RATE(#{new_rate.id}): update_storage_model overwriting INVOICING_LINE_ITEM(#{li.id}) with #{args}"
        # If someone has updated the price, leave price alone (but update storage days)
        if li.unit_price != li.original_unit_price
          args.delete(:unit_price)
          args.delete(:original_unit_price)
        end

        if !li.auto_calculated_quantity
          # TODO: this should remove everything except for original_quantity
          args.delete(:net_amount)
          args.delete(:tax_amount)
          args.delete(:quantity)
          args.delete(:estimated)
        end

        li.assign_attributes(args)
      else
        args[:job] = job
        Rails.logger.debug "STORAGE_RATE(#{new_rate.id}): Creating new li #{args}"
        job.invoice.line_items.build(args)
        # raise ArgumentError.new "INVOICE(#{job.id}) Storage line item not found" PDW this is needed for when tow, store occurs and need to update invoice, but have no storage line item
      end
    end
  end

  def add_additional_items(storage_rate)
    addition_lis = []
    storage_rate.add_rate_additions_for_type(addition_lis, storage_rate, job.id)
    addition_lis.each do |li|
      existing = job.invoice.find_lineitem_by_job_and_description(job, li[:description])
      if existing
        existing.update_attributes(li)
      else
        job.invoice.line_items.build(li)
      end
    end
  end

end
