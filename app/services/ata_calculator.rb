# frozen_string_literal: true

class AtaCalculator

  def initialize(job_id)
    @job_id = job_id
  end

  def call
    @oldest_parent = oldest_parent_or_job
    return nil if @oldest_parent.blank?

    eta = earliest_eta
    onsite = most_recent_onsite

    return nil unless eta.present? && onsite.present?

    # This is meant to be:
    # (dttm onsite - oldest entered ETA dttm created_at) in minutes
    # re: the picture on https://swoopme.atlassian.net/browse/ENG-5328
    (onsite.last_set_dttm - eta.created_at) / 60
  end

  private

  def earliest_eta
    EarliestJobEtaQuery.call(@oldest_parent.id)
  end

  def most_recent_onsite
    MostRecentOnsiteJobStatusQuery.call(@oldest_parent.id)
  end

  def oldest_parent_or_job
    job = Job.find(@job_id)

    result =
      JobAncestorsWithStatusesQuery
        .call(job.id, statuses: [Job::STATUS_ASSIGNED, Job::STATUS_UNASSIGNED])
        .last

    # Use the job itself if it has no parents
    result.blank? ? job : result
  end

end
