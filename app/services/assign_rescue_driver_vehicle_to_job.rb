# frozen_string_literal: true

class AssignRescueDriverVehicleToJob

  include Interactor::Organizer

  organize CheckForJob,
           CheckJobForBlankRescueVehicle,
           CheckJobForRescueDriver,
           CheckJobRescueDriverForVehicle,
           AddRescueVehicleToJob

end
