# frozen_string_literal: true

module Commissions
  class UpdateCommissionExclusionService

    include SwoopService

    attr_reader :company, :type

    def initialize(company:, commission_exclusion_to_add:,
                   commission_exclusion_to_remove:, type: :addon)

      if company.blank?
        raise ArgumentError, 'Commissions::UpdateCommissionExclusionService Company must not be nil'
      end

      @company = company
      @commission_exclusion_to_add = commission_exclusion_to_add || []
      @commission_exclusion_to_remove = commission_exclusion_to_remove || []
      @type = type

      Rails.logger.debug(
        "Commissions::UpdateCommissionExclusionService initialized for company id " \
        "#{@company.id}, type #{@type}"
      )
    end

    def execute
      return if no_commission_exclusion_to_add_or_remove

      add_commission_exclusions

      remove_commission_exclusions

      @company.reload

      self
    end

    def after_execution
      PublishCompanyWorker.perform_after_commit(@company.id, self.class.name)
    end

    private

    def no_commission_exclusion_to_add_or_remove
      @commission_exclusion_to_add.blank? && @commission_exclusion_to_remove.blank?
    end

    def add_commission_exclusions
      service_code_ids = @commission_exclusion_to_add.map do |non_commissionable|
        non_commissionable[:service_code_id]
      end

      service_code_ids.each do |service_code_id|
        service_code = ServiceCode.find_by_id!(service_code_id)

        execute_query_for(service_code).first_or_create!
      end
    end

    def remove_commission_exclusions
      service_code_ids = @commission_exclusion_to_remove.map do |non_commissionable|
        non_commissionable[:service_code_id]
      end

      service_code_ids.each do |service_code_id|
        service_code = ServiceCode.find_by_id!(service_code_id)

        execute_query_for(service_code).delete_all
      end
    end

    def execute_query_for(service_code)
      if @type == :addon
        CommissionExclusion.where(
          company: @company,
          service_code_id: service_code.id,
          item: service_code.name
        )
      else
        ServiceCommissionsExclusions.where(
          company: @company,
          service_code_id: service_code.id
        )
      end
    end

  end
end
