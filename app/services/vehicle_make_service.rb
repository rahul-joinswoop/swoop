# frozen_string_literal: true

class VehicleMakeService

  def initialize(api_company:)
    @api_company = api_company
  end

  def call
    makes = VehicleMake.where(original: true).includes(vehicle_models: :vehicle_type)
    if !@api_company.has_feature(Feature::HEAVY_DUTY_EQUIPMENT)
      makes = makes.where(vehicle_models: { heavy_duty: [false, nil] })
    end
    makes
  end

end
