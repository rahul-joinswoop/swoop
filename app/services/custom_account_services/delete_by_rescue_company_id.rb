# frozen_string_literal: true

module CustomAccountServices
  class DeleteByRescueCompanyId

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "CustomAccountServices::DeleteByCompanyId called #{context}"

      @api_company_id    = context.input[:api_company_id]
      @api_user_id       = context.input[:api_user_id]
      @rescue_company_id = context.input[:rescue_company_id]

      if @api_company_id.blank?
        raise ArgumentError, "CustomAccountServices::DeleteByCompanyId @api_company_id is blank"
      end

      if @api_user_id.blank?
        raise ArgumentError, "CustomAccountServices::DeleteByCompanyId @api_user_id is blank"
      end

      if @rescue_company_id.blank?
        raise(
          ArgumentError, "CustomAccountServices::DeleteByCompanyId @rescue_company_id is null"
        )
      end
    end

    def call
      context.output = create_api_async_request!

      schedule_async_delete_custom_account_worker
    end

    private

    # we create the async_quest for auditing and tracking the flow
    def create_api_async_request!
      @api_async_request = API::AsyncRequest.create!(
        company_id: @api_company_id,
        user_id: @api_user_id,
        request: { rescue_company_id: @rescue_company_id },
        target_id: @rescue_company_id,
        system: API::AsyncRequest::DELETE_CUSTOM_ACCOUNT
      )
    end

    def schedule_async_delete_custom_account_worker
      Stripe::DeleteCustomAccountByRescueCompanyIdWorker.perform_async(
        @api_async_request.id, @rescue_company_id
      )
    end

  end
end
