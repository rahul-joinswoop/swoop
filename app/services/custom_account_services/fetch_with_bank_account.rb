# frozen_string_literal: true

module CustomAccountServices
  class FetchWithBankAccount

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "CustomAccountServices::FetchWithBankAccount called #{context}"

      @api_company_id = context.input[:api_company_id]

      if @api_company_id.blank?
        raise ArgumentError, "CustomAccountServices::DeleteByCompanyId @api_company_id is blank"
      end
    end

    def call
      context.output = {
        custom_account: custom_account,
        stripe_bank_account: stripe_bank_account,
      }
    end

    private

    def custom_account
      return nil unless bank_account_enabled?

      @custom_account ||= CustomAccount.not_deleted.find_by(company_id: @api_company_id)
    end

    def stripe_bank_account
      return nil unless bank_account_enabled? && custom_account

      @stripe_bank_account ||= stripe_bank_account_retrivier.retrieve
    rescue Stripe::PermissionError => e
      Rails.logger.error(e.message)
      Rollbar.error(e.message)

      Rails.logger.error(
        "CustomAccountServices::FetchWithBankAccount - Stripe BankAccount not found on Stripe " \
        "for CustomAccount(#{custom_account.id}), upstream_account_id: " \
        "#{custom_account.upstream_account_id}. We should mark this custom_account as deleted " \
        "in our DB as it's has probably been manually deleted on Stripe end."
      )

      context.fail!(message: "Stripe BankAccount not found on Stripe")
    end

    def stripe_bank_account_retrivier
      StripeIntegration::BankAccountRetriever.new({
        stripe_account_id: custom_account.upstream_account_id,
      })
    end

    def bank_account_enabled?
      !company.has_feature?(Feature::DISABLE_BANK_SETTINGS)
    end

    def company
      @company ||= RescueCompany.find(@api_company_id)
    end

  end
end
