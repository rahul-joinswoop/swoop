# frozen_string_literal: true

# This service handles mapping the request parameters for jobs into the
# standard format that the rest of the Job services will understand.
module API
  class MapJobParams

    include Utils

    def initialize(api_company:, api_user:, job:, params:)
      @api_company = api_company
      @api_user = api_user
      @job = job
      # TODO - really we should be using ActionController::Parameters here and
      # below....
      @params = ActiveSupport::HashWithIndifferentAccess.new(
        params.is_a?(ActionController::Parameters) ? params.to_unsafe_h : params
      )
    end

    def call
      clean = @params

      Rails.logger.debug("cleaned class #{clean.class}")

      Rails.logger.debug("clean: #{clean}")

      obj = extract([
        :fleet_manual, :notes, :dispatch_notes, :partner_notes, :status, :email,
        :scheduled_for, :po_number, :unit_number, :ref_number,
        :customer_type_id, :odometer, :vip, :uber, :alternate_transportation,
        :bta, :rescue_vehicle_id, :rescue_driver_id,
        :no_keys, :keys_in_trunk, :four_wheel_drive, :accident, :review_delay,
        :toa, :blocking_traffic, :unattended, :text_eta_updates,
        :invoice_vehicle_category_id, :symptom_id, :tire_type_id, :policy_number,
        :reject_reason_id, :reject_reason_info, :eta_explanation_id, :fleet_notes, :driver_notes,
        :swoop_notes, :rescue_provider_id, :fleet_dispatcher_id, :partner_dispatcher_id, :root_dispatcher_id,
        :get_location_sms, :review_sms, :partner_sms_track_driver, :send_sms_to_pickup, :will_store, :site_id,
        :storage_type_id, :priority_response, :deleted_at, :fleet_site_id, :department_id, :partner_department_id, :release_date,
        :trailer_vehicle_id, :service_alias_id, :service_code_id, :accounting_code, :pcc_coverage_id, :customer_lookup_type,
      ], clean)
      Rails.logger.debug("cleaned obj:#{obj}")

      # verify objects are owned by this company

      if obj[:rescue_driver_id]
        if obj[:rescue_driver_id] && obj[:rescue_driver_id].present?
          User.find_by!(company: @api_company, id: obj[:rescue_driver_id])
        end
      end
      if obj[:rescue_vehicle_id]
        Vehicle.find_by!(company: @api_company, id: obj[:rescue_vehicle_id])
      end

      if obj[:fleet_dispatcher_id].present?
        User.find_by!(company: @api_company, id: obj[:fleet_dispatcher_id])
      end

      if obj[:partner_dispatcher_id].present?
        User.find_by!(company: @api_company, id: obj[:partner_dispatcher_id])
      end

      # This is basically covering the GraphQL EditJobStatus / EditJob case
      # for when JobStatus passed == 'Accepted', and it has a JobStatusReasonType.
      #
      job_status_reason_type = clean.dig(:job_status_reason_type)
      if job_status_reason_type.present?
        # for Accepted, we need to convert it to its respective JobEtaExplanation#id
        # as it is currently a job.eta_explanation_id
        #
        # TODO finish migrating JobETAExplanation to StatusReason structure
        # follow up ticket: https://swoopme.atlassian.net/browse/ENG-8842
        #
        # Theoretically we only fall here through Mutations::EditJobStatus / Mutations::EditJob.
        if @job.assigned? && clean.dig(:status) == 'accepted'
          # TODO - this assumes we always have the legacy value corresponding to our JSRT
          obj[:eta_explanation_id] = job_status_reason_type.find_legacy.id
        end

        # TODO ENG-8369: job rejected will need to be converted to JobRejectReason
      end

      # We can't let obj[:status] = 'accepted' to pass, otherwise JobUpdate logic breaks.
      # It came here through GraphQL EditJobStatus / EditJob case when status Accepted, so we need to remove it.
      if obj.dig(:status) == 'accepted'
        obj.delete(:status)
      end

      if clean[:owner_company]
        obj[:fleet_company_id] = clean[:owner_company][:id]
      end

      if clean[:rescue_company] && (api_root? || @api_company.fleet_in_house?)
        obj[:rescue_company_id] = clean[:rescue_company][:id]
      end

      if clean[:rescue_vehicle]
        msg = "Depricated rescue_vehicle - Only supply rescue_vehicle or rescue_vehicle_id TODO: android remove rescue_vehicle"
        Rails.logger.warn msg
        # raise ArgumentError.new(msg) if  clean[:rescue_vehicle_id] #PDW:commenting for android
        obj[:rescue_vehicle_id] = clean[:rescue_vehicle][:id]
      end

      if clean[:rescue_driver]
        msg = "Only supply rescue_driver or rescue_driver_id, not both"
        Rails.logger.warn msg
        # raise ArgumentError.new(msg) if  clean[:rescue_driver_id] #PDW:commenting for android
        obj[:rescue_driver_id] = clean[:rescue_driver][:id]
      end

      if @api_company.rescue?
        partner_prepare_account_association(obj) if clean[:account]
      else
        base_prepare_account_association(obj) if clean[:account]
      end

      if clean[:caller]
        obj[:caller_attributes] = extract([:full_name, :name, :phone, :id], clean[:caller])
      end

      if clean[:customer].present?
        load_driver_user(obj)

        # rubocop:disable Rails/SaveBang
        obj[:driver_attributes][:user_attributes].update(extract([:phone, :full_name, :id], clean[:customer]))

        # handle member_number
        member_number = clean.dig(:customer, :member_number)
        customer = clean.dig(:customer, :customer)
        if [member_number, customer].all?(:present?)
          # this shouldn't ever happen - from graphql we're passed an AR Customer
          # instance, from rest api we'd be passed in :member_number so both
          # is a potential error condition. log it in rollbar but continue on
          Rollbar.error("MapJobParams: got customer[:member_number] and customer[:customer]", clean[:customer])
        end

        if customer
          obj[:driver_attributes][:user_attributes].update(customer: customer)
        elsif member_number
          obj[:driver_attributes][:user_attributes][:customer_attributes] ||= {}
          obj[:driver_attributes][:user_attributes][:customer_attributes].update(
            member_number: member_number,
            # TODO - is @api_company set correctly when swoop creates jobs on
            # behalf of other companies?
            company: @api_company,
          )
        end
      elsif (clean.keys & ["phone", "first_name", "last_name"]).any?
        load_driver_user(obj)
        obj[:driver_attributes][:user_attributes].update(extract([:phone, :first_name, :last_name], clean))
      end
      Rails.logger.debug "clean.keys: #{clean.keys}"
      if (clean.keys & ["make", "model", "serial_number", "vehicle_type", "year", "color", "license", "vin", "tire_size"]).any?
        load_driver_vehicle(obj)
        obj[:driver_attributes][:vehicle_attributes].update(extract([:make, :model, :serial_number, :vehicle_type, :year, :color, :license, :vin, :tire_size], clean))
      end
      # rubocop:enable Rails/SaveBang

      if clean[:service_location]
        obj[:service_location_attributes] = extract(
          [
            :id, :address, :lat, :lng, :exact, :place_id, :location_type_id,
            :site_id,
          ],
          clean[:service_location]
        )
      end

      if clean[:drop_location]
        obj[:drop_location_attributes] = extract([:id, :address, :lat, :lng, :exact, :site_id, :place_id, :location_type_id], clean[:drop_location])
      elsif clean.key?('drop_location') && clean['drop_location'].nil?
        # Allow the drop location to be unset
        obj[:drop_location] = nil
      end

      if clean[:pickup_contact]
        obj[:pickup_contact_attributes] = extract([:id, :full_name, :first_name, :last_name, :phone], clean[:pickup_contact])
      end
      if clean[:dropoff_contact]
        obj[:dropoff_contact_attributes] = extract([:id, :full_name, :first_name, :last_name, :phone], clean[:dropoff_contact])
      end

      if clean[:service_code_id]
        service_code = ServiceCode.find_by(id: clean[:service_code_id])
        if service_code
          obj[:service_code_id] = service_code.id
          clean[:service] = service_code.name
        end
      elsif clean[:service]
        if clean[:service].is_a?(Hash)
          name = clean[:service][:name]
        else
          name = clean[:service]
        end

        Rails.logger.debug "Looking up service:#{name}"
        service = ServiceCode.find_by(name: name)
        raise ArgumentError, "Unknown service: #{name}" unless service
        obj[:service_code_id] = service.id
      end

      if (clean[:service] == "Storage") && !(clean[:storage])
        clean[:storage] = {}
      end

      if clean[:storage]
        obj[:inventory_items_attributes] = [extract([:id, :site_id, :account_id, :released_to_email, :replace_invoice_info, :released_to_account_id], clean[:storage])]
        obj[:inventory_items_attributes][0][:company_id] = @api_company.id
        obj[:inventory_items_attributes][0][:user_id] = @api_user.id

        # Seems we are not getting the item information back from the front end anymore
        # its ok, we will just build this for if it doesnt exist.

        if obj[:inventory_items_attributes][0][:item_id].blank?
          # Ew this looks ugly, but simplies the conditions going forward in this block
          veh = (obj[:driver_attributes] || {})[:vehicle_attributes]

          vehicle_id = veh[:id] if veh.present?
          vehicle_id ||= @job.driver.vehicle_id if @job.present?

          obj[:inventory_items_attributes][0][:item_id]   ||= vehicle_id
          obj[:inventory_items_attributes][0][:item_type] ||= 'Vehicle'
        end

        if !(obj[:inventory_items_attributes][0].key? :id)
          # creating first time
          obj[:inventory_items_attributes][0][:stored_at] = Time.now
          site_id = if obj[:drop_location_attributes].present?
                      obj[:drop_location_attributes][:site_id]
                    else
                      @job && @job.drop_location ? @job.drop_location.site_id : nil
                    end
          obj[:inventory_items_attributes][0][:site_id] = site_id
        end

        if clean[:storage][:released_to_account_id].present?
          obj[:inventory_items_attributes][0][:released_to_user_id] = nil
        else
          if clean[:storage][:released_to_user]
            obj[:inventory_items_attributes][0][:released_to_user_attributes] = extract([:id, :full_name, :phone, :license_state, :license_id, :storage_relationship], clean[:storage][:released_to_user])
            if clean[:storage][:released_to_user][:location]
              obj[:inventory_items_attributes][0][:released_to_user_attributes][:location_attributes] = extract([:id, :lat, :lng, :address, :place_id, :exact], clean[:storage][:released_to_user][:location])
            end
          end
        end
      end

      if clean[:images]
        obj[:images_attributes] = clean[:images]
        obj[:images_attributes].each do |image|
          image[:user_id] = @api_user.id
        end
      end

      # When UI switches over to using new notes, it will use this to set user and company
      clean_notes_api(clean, obj, :partner_internal_notes, :partner_internal_notes_attributes)
      clean_notes_api(clean, obj, :partner_dispatch_notes, :partner_dispatch_notes_attributes)
      clean_notes_api(clean, obj, :partner_invoice_notes, :partner_invoice_notes_attributes)
      clean_notes_api(clean, obj, :swoop_internal_notes, :swoop_internal_notes_attributes)
      clean_notes_api(clean, obj, :public_notes, :public_notes_attributes)
      clean_notes_api(clean, obj, :fleet_internal_notes, :fleet_internal_notes_attributes)

      # TODO: overwrite any previous answers given

      if clean[:question_results]

        # if @job

        #   clean[:question_results].collect do |result|
        #    q = @job.question_results.where(question_id: result[:question_id]).first_or_initialize
        #    q.answer_id = result[:answer_id]
        #    @job.question_results << q
        #  end
        obj[:question_results_attributes] = clean[:question_results]
        obj[:question_results_attributes].each do |result|
          if @job
            previous = QuestionResult.where(job_id: @job[:id], question_id: result[:question_id])
            if previous[0]
              result[:id] = previous[0].id
            end
          end
        end
        # else
        #  obj[:question_results_attributes] = clean[:question_results]
        # end
      end

      if clean[:policy_location]
        obj[:policy_location_attributes] = extract([:id, :address, :lat, :lng, :state, :city, :street, :zip, :exact, :place_id],
                                                   clean[:policy_location])
      end

      Rails.logger.debug("Mapped params: #{obj}")
      obj
    end

    private

    def clean_notes_api(clean, obj, notes_field, attributes_field)
      if clean[notes_field]
        obj[attributes_field] = clean[notes_field]
        obj[attributes_field].each do |note|
          note[:user_id] = @api_user.id
          note[:company_id] = @api_company.id
        end
      end
    end

    def partner_prepare_account_association(obj)
      account_params = @params[:account]
      account_id = account_params[:id]
      if account_params.key?(:id) && account_id.blank?
        # clearing the account value
        obj[:account_id] = nil
      elsif account_id
        Account.find_by!({ id: account_id, company: @api_company })
        obj[:account_id] = account_id
      end
    end

    def base_prepare_account_association(obj)
      account_params = @params[:account]
      account_id = account_params[:id]

      if account_id
        Account.find_by!({ id: account_id, company: @api_company })
        obj[:account_id] = account_id
      else
        account_name = account_params[:name]
        account = Account.find_by({ name: account_name, company: @api_company })
        if account
          obj[:account_id] = account.id
        else
          obj[:account_attributes] = {
            name: account_name, company_id: @api_company.id,
          }
        end
      end
    end

    def ensure_driver(obj)
      if !(obj[:driver_attributes])
        obj[:driver_attributes] = {}
        if @job && @job.driver
          obj[:driver_attributes][:id] = @job.driver.id
        end
      end
    end

    def load_driver_user(obj)
      ensure_driver(obj)
      if !(obj[:driver_attributes][:user_attributes])
        obj[:driver_attributes][:user_attributes] = {}
      end
      if @job && @job.driver.user
        obj[:driver_attributes][:user_attributes][:id] = @job.driver.user.id
      end
    end

    def load_driver_vehicle(obj)
      ensure_driver(obj)
      if !(obj[:driver_attributes][:vehicle_attributes])
        obj[:driver_attributes][:vehicle_attributes] = {}
      end
      if @job && @job.driver.vehicle
        obj[:driver_attributes][:vehicle_attributes][:id] = @job.driver.vehicle.id
      end
    end

  end
end
