# frozen_string_literal: true

module API
  module AutoComplete
    class RescueCompaniesByProvider

      COMPANY_FINDER_SQL = <<~SQL
        SELECT companies.*
        FROM companies
        JOIN accounts ON companies.id = accounts.company_id
        WHERE companies.type = 'RescueCompany'
          AND companies.deleted_at IS NULL
          AND accounts.deleted_at IS NULL
          AND accounts.client_company_id = ?
          AND companies.name ILIKE ?
        ORDER BY char_length(companies.name), lower(companies.name)
        LIMIT ?
      SQL
      COMPANY_FINDER_SQL.freeze

      def initialize(term:, api_company:, limit: 20)
        @term = term
        @api_company = api_company
        @limit = limit

        raise ArgumentError, "term must be > 1 chars long, term:#{@term}" unless @term && (@term.length > 1)
      end

      def call
        results = RescueCompany.find_by_sql([COMPANY_FINDER_SQL, @api_company.id, "%#{@term}%", @limit])
        uniqify_company_names!(results)
      end

      private

      # Make the results all have unique names so that the end user can distinguish between duplicates.
      def uniqify_company_names!(results)
        duplicates = {}
        duplicates_exist = false
        results.each do |company|
          dups = duplicates[company.name]
          if dups
            duplicates_exist = true
          else
            dups = []
            duplicates[company.name] = dups
          end
          dups << company
        end

        if duplicates_exist
          duplicates.each_value do |dups|
            make_names_uniq!(dups) if dups.size > 1
          end
        end

        # Resorting the results to be alphabetical. The database results are sorted first
        # by length so that if someone types in an exact match it will appear in the list
        # and not be truncated by the limit.
        results.sort_by { |company| company.name.to_s.downcase }
      end

      # Make the company names unique by adding a city and/or id to the name
      def make_names_uniq!(dups)
        name_counts = Hash.new(0)
        dups.each do |company|
          city = company_city(company)
          if city
            company.name = "#{company.name} [#{city}]"
          else
            company.name = "#{company.name} [##{company.id}]"
          end
          name_counts[company.name] += 1
        end

        name_counts.each do |name, count|
          if count > 1
            dups.each do |company|
              if company.name == name
                company.name = "#{company.name} [##{company.id}]"
              end
            end
          end
        end
      end

      def company_city(company)
        location = (company.hq&.location || company.location)
        location&.city_state
      end

    end
  end
end
