# frozen_string_literal: true

module API
  module AutoComplete
    class Sites

      def initialize(term:, api_company:, limit: 10, **args)
        @term = term
        @api_company = api_company
        @limit = limit

        raise ArgumentError, "term is required" if @term.blank?
      end

      def call
        results = Site.where(company_id: @api_company.id, deleted_at: nil)
          .where('sites.name ILIKE ?', "%#{@term}%")
        if @limit
          results.limit(@limit)
        else
          results
        end
      end

    end
  end
end
