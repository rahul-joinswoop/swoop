# frozen_string_literal: true

module API
  module AutoComplete
    class RootUsers

      def initialize(term, limit: 20)
        @term = term
        @limit = limit
        raise ArgumentError, "term must be > 1 chars long, term:#{@term}" unless @term && (@term.length > 1)
      end

      def call
        results = ::User.root_users.where("CONCAT(first_name, ' ', last_name) ILIKE ?", "%#{@term}%")
        if @limit
          results.limit(@limit)
        else
          results
        end
      end

    end
  end
end
