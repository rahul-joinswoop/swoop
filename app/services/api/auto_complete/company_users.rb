# frozen_string_literal: true

# TODO it should be refactored to use Elastic Search.
# follow up ticket: https://swoopme.atlassian.net/browse/ENG-8516
module API
  module AutoComplete
    class CompanyUsers

      def initialize(term, company_id:, **args)
        @term = term
        @company_id = company_id

        raise ArgumentError, "company_id is required" unless @company_id
        raise ArgumentError, "term is required" if @term.blank?
      end

      def call
        ::Company.find(@company_id).users.where("CONCAT(first_name, ' ', last_name) ILIKE ?", "%#{@term}%").not_deleted
      end

    end
  end
end
