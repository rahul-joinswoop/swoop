# frozen_string_literal: true

module API
  module AutoComplete
    class LocationTypes

      def initialize(term:, api_company:, limit: 10, **args)
        @term = term
        @api_company = api_company
        @limit = limit

        raise ArgumentError, "term is required" if @term.blank?
      end

      def call
        results = @api_company.location_types
          .where('location_types.name ILIKE ?', "%#{@term}%")
          .not_deleted

        if @limit
          results.limit(@limit)
        else
          results
        end
      end

    end
  end
end
