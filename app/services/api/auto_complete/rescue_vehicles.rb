# frozen_string_literal: true

module API
  module AutoComplete
    class RescueVehicles

      def initialize(term:, api_company:, scope: nil, limit: 10, **args)
        @term = term
        @api_company = api_company
        @limit = limit
        @scope = scope || ::RescueVehicle.where(company_id: @api_company.id, deleted_at: nil)
        raise ArgumentError, "term is required" if @term.blank?
      end

      def call
        results = @scope.where('vehicles.name ILIKE ?', "%#{@term}%")
        if @limit
          results.limit(@limit)
        else
          results
        end
      end

    end
  end
end
