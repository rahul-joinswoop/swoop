# frozen_string_literal: true

module API
  module AutoComplete
    class Company

      def initialize(type, term, limit = 10)
        @type =
          type,
          @term = term
        @limit = limit

        raise ArgumentError, "term must be > 1 chars long, term:#{@term}" unless @term && (@term.length > 1)
      end

      def call
        query = ::Company.where("name ILIKE ?", "%#{@term}%")

        # TODO type is not actually being applied to the query
        # if @type != 'Company'
        #   query = query.where(type: @type)
        # end
        if @limit
          query.limit(@limit)
        else
          query
        end
      end

    end
  end
end
