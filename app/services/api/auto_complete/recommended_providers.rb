# frozen_string_literal: true

module API
  module AutoComplete
    class RecommendedProviders

      def initialize(term:, api_company:, job_id:, limit: 10)
        @term = term.to_s.strip
        if @term.length <= 1
          raise ArgumentError, "term must be > 1 chars long, term:#{@term}"
        end

        @api_company = api_company
        @job_id = job_id
        @limit = limit
      end

      def call
        job = Job.find(@job_id)

        providers = RescueProvider
          .preload(:tags, :location, provider: [:location, :sites], site: [:location])
          .where(company_id: @api_company.id, deleted_at: nil)
          .joins(site: [:company])
          .joins(provider: [:companies_services])
          .where(companies: { deleted_at: nil }, sites: { deleted_at: nil })
          .where(companies_services: { service_code_id: job.service_code_id })
          .where("companies.name || ' ' || sites.name ILIKE ? ", "%#{@term}%")

        if @limit
          providers = providers.limit(@limit)
        end

        providers.map { |provider| JobGetProviderDistanceService.new(job, provider).call }
      end

    end
  end
end
