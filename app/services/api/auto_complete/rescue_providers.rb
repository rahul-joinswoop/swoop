# frozen_string_literal: true

module API
  module AutoComplete
    class RescueProviders

      def initialize(term:, api_company:, limit: 10)
        @term = term.to_s.strip
        if @term.length <= 1
          raise ArgumentError, "term must be > 1 chars long, term:#{@term}"
        end

        @api_company = api_company
        @limit = limit
      end

      def call
        # TODO limit parameter is not being used
        results = RescueProvider.where(company_id: @api_company.id, deleted_at: nil).joins(site: [:company])
          .where(companies: { deleted_at: nil }, sites: { deleted_at: nil })
          .where("(companies.name || ' ' || sites.name) ILIKE ?", "%#{@term}%")
          .preload(:provider, :site)

        if @limit
          results.limit(@limit)
        else
          results
        end
      end

    end
  end
end
