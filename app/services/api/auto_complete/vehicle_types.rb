# frozen_string_literal: true

module API
  module AutoComplete
    class VehicleTypes

      def initialize(term:, limit: 10, **args)
        @term = term
        @limit = limit

        raise ArgumentError, "term is required" if @term.blank?
      end

      def call
        results = ::VehicleType.where('vehicle_types.name ILIKE ?', "%#{@term}%")
        if @limit
          results.limit(@limit)
        else
          results
        end
      end

    end
  end
end
