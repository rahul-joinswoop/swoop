# frozen_string_literal: true

module API
  module Jobs
    class UpdateHistory

      include LoggableJob
      include Utils
      include SwoopService

      attr_reader :job

      def initialize(job:, history:, update_invoice: true)
        log :debug, "initializing with history #{history}", job: job.id

        raise ArgumentError, log_msg("needs a job", job: job.id) unless job

        @job = job
        @history = history
        @update_invoice = update_invoice
      end

      def execute
        if @history
          @history.each do |status, obj|
            job_status = JobStatus.find_by({ name: status })

            audit_job_status = AuditJobStatus.find_by({
              job_status: job_status,
              job: @job.id,
            })

            next if audit_job_status.nil?

            previous = audit_job_status.last_set_dttm

            if audit_job_status.adjusted_dttm
              previous = audit_job_status.adjusted_dttm
            end

            audit_job_status.adjusted_dttm = obj[:adjusted_dttm]

            UpdateLastUpdatedStatusOnJob.new(
              @job, audit_job_status, previous
            ).call

            audit_job_status.save!

            if @job.storage
              @job.storage.stored_at = obj[:adjusted_dttm]
              @job.storage.save!

              if @update_invoice
                CreateOrUpdateInvoice.perform_async(@job.id)
              end
            end
          end
        end

        self
      end

    end
  end
end
