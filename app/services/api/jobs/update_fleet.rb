# frozen_string_literal: true

#
# Service to update a Fleet Job.
# This code was extracted from fleets/jobs_controller.
#
module API
  module Jobs
    # ENG-8569: API::Jobs::UpdateFleet should be renamed API::Jobs::UpdateFleetManagedJob
    # because it's not updating a fleet – it's updating a fleet-managed job
    class UpdateFleet

      include LoggableJob
      include Utils
      include SwoopService

      attr_reader :job

      # TODO for some reason, we're not doing anything with browser_platform and request_user_agent
      # follow up ticket: https://swoopme.atlassian.net/browse/ENG-8564
      def initialize(company:, api_user:, job_id:, job_history:, job_storage: false, params:,
                     browser_platform: nil, request_user_agent: nil, save_as_draft: false)

        log :debug, "initializing with params: #{params}", job: @job_id

        raise ArgumentError, log_msg("Non Fleet Company to update a Fleet Job", job: @job_id) unless company.fleet? || company.is_swoop?
        raise ArgumentError, log_msg("needs a job_id", job: @job_id) unless job_id

        @company = company
        @api_user = api_user
        @job_id = job_id
        @params = params
        @job_history = job_history
        @job_storage = job_storage
        @save_as_draft = save_as_draft

        log :debug, "@pcc_coverage_id #{params["pcc_coverage_id"]}", job: @job_id

        set_job
      end

      def execute
        guard_price

        toa = remove_param(:toa, @params)

        log :debug, "current rescue_company: #{@job.rescue_company}", job: @job.id
        log :debug, "rescue_company_was: #{@job.rescue_company_id_was}", job: @job.id

        if @save_as_draft
          @job = API::Jobs::UpdateDraft.new(
            job: @job,
            params: @params,
            api_user: @api_user
          ).call.job
        else
          @job.assign_attributes(@params)

          HandleToa.new(@company, @api_user, @job, toa).call

          API::Jobs::UpdateHistory.new(job: @job, history: @job_history).call

          @job = API::Jobs::Update.new(
            job: @job,
            api_user: @api_user,
            job_storage: @job_storage,
            browser_platform: @browser_platform,
            request_user_agent: @request_user_agent
          ).call.job
        end

        self
      end

      private

      # This logic comes from fleet/jobs#update
      def guard_price
        if @job.rescue_company && @params[:price]
          log :warn, "disallowing setting of cost for", job: @job.id

          remove_param(:price, @params)
        end
      end

      def set_job
        if @company.fleet?
          @job = Job.where(id: @job_id, fleet_company: @company).take!
        elsif @company.is_swoop?
          @job = Job.where(id: @job_id).take!
        end
      end

    end
  end
end
