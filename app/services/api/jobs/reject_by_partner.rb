# frozen_string_literal: true

module API
  module Jobs
    class RejectByPartner

      include SwoopService

      attr_reader :job

      def initialize(job_id:, api_company_id:, api_user_id:, reject_params:)
        @job_id = job_id
        @api_company_id = api_company_id
        @api_user_id = api_user_id
        @reject_params = reject_params

        Rails.logger.debug(
          "API::Jobs::RejectByPartner - initialized with job_id(#{@job_id}, " \
          "@api_company(#{@api_company}), @reject_params(#{@reject_params})"
        )
      end

      def call
        Rails.logger.debug "API::Jobs::RejectByPartner - started execution for @job_id(@job_id)"
        @job ||= Job.find_by!(id: @job_id, rescue_company_id: @api_company_id)

        @job.partner_dispatcher_id = @api_user_id
        @job.last_touched_by_id = @api_user_id

        PartnerUpdateJobState.new(@job, JobStatus::NAMES[:REJECTED]).call

        @job.assign_attributes(@reject_params)

        build_job_explanation_if_needed

        if @job.save
          @job.update_digital_dispatcher_status
        end

        Rails.logger.debug "API::Jobs::RejectByPartner - finished execution for @job_id(@job_id)"

        self
      end

      private

      # TODO: Move this into Job w/ the implementation in FleetManagedJob??
      def build_job_explanation_if_needed
        # This makes it so that only swoop jobs have issues from rejection
        # Should be removed when we add issues to other companies
        if @job.fleet_managed_job?
          reason = @job.reject_reason_reason

          if reason
            @job.explanations.build(
              reason: reason,
              reason_info: @reject_params[:reject_reason_info]
            )
          end
        end
      end

    end
  end
end
