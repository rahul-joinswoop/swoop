# frozen_string_literal: true

module API
  module Jobs
    class InitializeBase < Initialize

      def initialize(api_company:, api_user:, job:)
        super(api_company: api_company, api_user: api_user, job: job)

        raise ArgumentError, "Required parameter 'service' missing" unless @job.service_code
      end

      def call
        super()
        raise "Job in Draft or PreDraft called InitializeBase" if @job.draft_or_predraft?

        @job.hack_create_ajs('Initial')

        # it moves the job to pending or dispatched
        DispatchJob.new(@job).call

        Rails.logger.debug "API::Jobs::InitializeBase - about to call job.save #{@job.inspect}"
        if @job.save!
          API::Jobs::ActivateJob.new(job: @job).call
        end
      end

    end
  end
end
