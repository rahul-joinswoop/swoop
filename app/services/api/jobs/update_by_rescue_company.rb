# frozen_string_literal: true

# Service to update jobs by RescueCompany.
# This code was extracted from partners/jobs_controller.
module API
  module Jobs
    class UpdateByRescueCompany

      include LoggableJob
      include Utils
      include SwoopService

      attr_reader :job

      def initialize(company:, api_user:, job_id:, job_history:, job_storage: false, params:,
                     browser_platform: nil, request_user_agent: nil, save_as_draft: false)

        log :debug, "initializing with params: #{params}", job: job_id

        @company = company
        @api_user = api_user
        @job_id = job_id
        @params = params
        @job_history = job_history
        @job_storage = job_storage
        @save_as_draft = save_as_draft

        raise ArgumentError, log_msg("Non Rescue Company to update a Rescue Job", job: @job_id) unless company.rescue?
        raise ArgumentError, log_msg("needs a job_id", job: @job_id) unless job_id

        set_job
      end

      def execute
        status = remove_param(:status, @params)
        bta    = remove_param(:bta, @params)
        toa    = remove_param(:toa, @params)

        if @save_as_draft
          # TODO - shouldn't this happen in edit job status services?
          @job = API::Jobs::UpdateDraft.new(
            job: @job,
            params: @params,
            api_user: @api_user
          ).call.job
        else
          Rails.logger.debug "assigning attributes: #{@params}"
          @job.assign_attributes(@params)

          if status && (status != @job.human_status)
            log :debug, "status updates on job depricated, use /job/<id>/status API", job: @job_id

            # TODO remove this case and status from allowed params on controller
            PartnerUpdateJobState.new(@job, status).call

            @job.update_digital_dispatcher_status
          elsif !@job.bta && bta
            # TODO - move this into edit job status services, see ENG-8996
            log :debug, "action BTA", job: @job_id

            # this is redundant as API::Jobs::Update call that happens some lines below
            # will do this. But BTA flow needs the last_touched_by to exist at this point as
            # the AJS that will be created for submitted needs it:
            #
            # @job.submitted_bid will trigger an AASM transition, and job#after_all_transitions
            # will create the AJS Submitted
            @job.last_touched_by = @api_user

            @job.bid(bta)
            # submits back to the DigitalDispatcher (ISSC or RSC) if FleetMotorClubJob
            # TODO: split out to sperate controller and call status update
            @job.submitted_bid
          end

          # If true it means that a duplicated Storage job is being pushed from Draft to Created,
          # therefore it's time to set job.storage.stored_at.
          if @job.storage && @job.storage.stored_at.nil?
            @job.storage.stored_at = Time.now
          end

          HandleToa.new(@company, @api_user, @job, toa).call

          API::Jobs::UpdateHistory.new(job: @job, history: @job_history).call

          @job = API::Jobs::Update.new(
            job: @job,
            api_user: @api_user,
            job_storage: @job_storage,
            browser_platform: @browser_platform,
            request_user_agent: @request_user_agent
          ).call.job
        end

        self
      end

      private

      def set_job
        @job = Job.where(id: @job_id, rescue_company: @company).take!
      end

    end
  end
end
