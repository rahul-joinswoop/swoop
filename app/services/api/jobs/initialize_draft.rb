# frozen_string_literal: true

module API
  module Jobs
    class InitializeDraft < Initialize

      include LoggableJob
      include SwoopService

      def initialize(api_company:, api_user:, job:)
        super(api_company: api_company, api_user: api_user, job: job)
      end

      def call
        super()

        set_job_status_as_draft

        build_audit_job_statuses

        @job.driver.vehicle = @job.driver.vehicle.becomes!(StrandedVehicle)

        Rails.logger.debug "API::Jobs::InitializeDraft - job.audit_job_statuses were built #{@job.audit_job_statuses.inspect}"
        Rails.logger.debug "API::Jobs::InitializeDraft - calling job.save #{@job.inspect}"

        @job.save!

        log :debug, "- job created as draft", job: @job.id
      end

      private

      def build_audit_job_statuses
        @job.hack_create_ajs('Initial')
        @job.hack_create_ajs('Draft')
      end

      def set_job_status_as_draft
        @job.status = Job::STATUS_DRAFT # manually setting it so we don't trigger automatic save on aasm
      end

    end
  end
end
