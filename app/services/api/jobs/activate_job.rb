# frozen_string_literal: true

module API
  module Jobs
    class ActivateJob

      include LoggableJob
      include SwoopService

      attr_reader :job

      def initialize(job:)
        @job = job
      end

      def execute
        log :debug, "API::Jobs::ActivateJob starting execution, job status is #{@job.status}", job: @job.id

        #
        # Trigger the ClientAPI Coverage Override flow if should_run_client_api_coverage_override?
        #
        if should_run_client_api_coverage_override?
          pcc_coverage_id = PCC::CoverageDetails::PolicyLookupService::Organizer.call(
            input: { job: @job, request_args: {}, force_client_api_coverage_override: true }
          ).output[:pcc_coverage_id]

          # pcc_coverage_id can be nil, and that's an expected case.
          @job.pcc_coverage_id = pcc_coverage_id if pcc_coverage_id
        end

        log :debug, "API::Jobs::ActivateJob about to call schedule_create_storage_invoice_batch", job: @job.id
        @job.schedule_create_storage_invoice_batch

        log :debug, "API::Jobs::ActivateJob about to call send_swoop_alert", job: @job.id
        @job.send_swoop_alert # Dont send the swoop alerts unless we need to, on auto-assign

        log :debug, "API::Jobs::ActivateJob about to call slack_after_create", job: @job.id
        @job.slack_after_create

        if @job.fleet_managed_job?
          log :debug, "API::Jobs::ActivateJob about to call scheduled_system_alert_create as it is a fleet_managed_job", job: @job.id

          @job.scheduled_system_alert_create
        end

        log :debug, "API::Jobs::ActivateJob about to call schedule_create_estimate", job: @job.id
        @job.schedule_create_estimate

        log :debug, "processing SMS", job: @job.id

        if @job.text_eta_updates
          log :debug, "about to call send_confirmation_sms", job: @job.id

          @job.send_confirmation_sms
        else
          log :debug, "about to call send_get_location as job#text_eta_updates is #{@job.text_eta_updates}", job: @job.id

          @job.send_get_location
        end

        if @job.fleet_managed_job? && @job.fleet_company.has_feature(Feature::SMS_CONFIRM_ON_SITE) && !@job.service_code.speciality?
          log :debug, "about to create a ConfirmPartnerOnSiteSmsAlert", job: @job.id

          ConfirmPartnerOnSiteSmsAlert.create!(job: @job)
        end

        # job has an after_commit :send_status_emails, but at this point job will be pending or dispatched,
        # and there's no email template for pending or dispatched statuses.
        #
        # sincerely not sure how it worked before (my guess is that job was being forced-saved when it turned into 'created')
        #
        # TODO it still needs to be improved.
        if @job.scheduled_for.present?
          Delayed::Job.enqueue JobStatusEmailSender.new(@job.id, JobStatusEmail::Scheduled.name)
        else
          Delayed::Job.enqueue JobStatusEmailSender.new(@job.id, JobStatusEmail::Created.name)
        end

        # Once job activated, should have all info necessary to calc customer invoice (if for example it was saved in
        # draft without a service, there would be no invoice and would need to be created here).
        InvoiceService::CreateOrUpdateFleetCustomerInvoiceIfNecessary.new(
          @job,
          @job.customer
        ).call

        self
      end

      private

      #
      # true if:
      # - job fleet company has Client API Coverage Override feature; and
      # - it's a ClientAPI request; and
      # - @job is in an active state
      # - @job does not have a coverage yet
      #
      def should_run_client_api_coverage_override?
        @job.fleet_company.has_feature?(Feature::CLIENT_API_COVERAGE_OVERRIDE) &&
        RequestContext.current.source_application&.client_api? &&
        Job.active_states.include?(@job.human_status) &&
        !@job.pcc_coverage_id
      end

    end
  end
end
