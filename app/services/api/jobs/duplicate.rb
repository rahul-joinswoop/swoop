# frozen_string_literal: true

module API
  module Jobs
    class Duplicate

      include LoggableJob
      include SwoopService

      attr_reader :duplicated_job

      def initialize(current_job_id:, api_company_id:, api_user_id:)
        @current_job_id = current_job_id
        @api_company_id = api_company_id
        @api_user_id    = api_user_id

        raise ArgumentError, "API::Jobs::Duplicate current_job_id empty" unless @current_job_id

        unless @api_company_id
          raise ArgumentError, log_msg("api_company_id empty", job: @current_job_id)
        end

        unless @api_user_id
          raise ArgumentError, log_msg("api_user_id empty", job: @current_job_id)
        end

        log :info,
            "initialized with " \
            "api_company_id:#{@api_company_id}, api_user_id:#{@api_user_id}",
            job: @current_job_id
      end

      def execute
        log :debug, "starting execution", job: @current_job_id

        duplicate_job!

        schedule_create_estimate_if_needed

        self
      end

      def current_job
        return @current_job if @current_job.present?

        case api_company
        when RescueCompany
          @current_job = Job.find_by!(id: @current_job_id, rescue_company: api_company)
        when FleetCompany
          @current_job = Job.find_by!(id: @current_job_id, fleet_company: api_company)
        when SuperCompany
          @current_job = Job.find_by!(id: @current_job_id)
        else
          raise(
            ArgumentError,
            log_msg("##{__method__} - " \
            "#{api_company.type} not allowed to load job", job: @current_job_id)
          )
        end

        log :debug, "successfully loaded", job: @current_job_id

        @current_job
      end

      private

      def api_company
        @api_company ||= Company.find(@api_company_id)
      end

      def api_user
        @api_user ||= User.find_by!(id: @api_user_id, company: api_company)
      end

      def duplicate_job!
        log :debug,
            "about to duplicate! " \
            "the current_job for #{api_company.type}",
            job: @current_job_id

        if api_company.is_a?(FleetCompany) || api_company.is_a?(SuperCompany)
          @duplicated_job = current_job.copy_job([], false)
        else
          @duplicated_job = current_job.copy_job([
            "rescue_company_id",
            "account_id",
            "partner_notes",
            "dispatch_notes",
            "driver_notes",
            "site_id",
            "partner_dispatcher_id",
          ], false)
        end

        @duplicated_job.user = api_user
        @duplicated_job.last_touched_by = api_user

        # We manually set the state for the new job.
        # If it were done by AASM transition it would force a job.save! and callbacks
        # would be triggered. We don't want callbacks to be triggered here.
        @duplicated_job.status = Job::STATUS_DRAFT
        @duplicated_job.set_service_location_user

        duplicate_inventory_items_if_storage_job

        @duplicated_job.save!

        log :debug,
            "job duplicated, duplicated_job_id:#{@duplicated_job.id}",
            job: @current_job_id
      end

      def duplicate_inventory_items_if_storage_job
        if current_job.service_code&.name == ServiceCode::STORAGE
          @duplicated_job.inventory_items = current_job.inventory_items.map do |inventory_item|
            duplicated_inventory_item = inventory_item.dup
            duplicated_inventory_item.item = inventory_item.item.dup

            duplicated_inventory_item.stored_at = nil
            duplicated_inventory_item.released_to_user_id = nil
            duplicated_inventory_item.released_at = nil

            duplicated_inventory_item
          end
        end
      end

      # does it make sense to create an estimate and even an invoice
      # if the duplicated job.status is always draft? (@see duplicate_job! method ^)
      def schedule_create_estimate_if_needed
        return if @duplicated_job.estimate

        log :debug,
            "#schedule_create_estimate_if_needed - " \
            "about to enqueue delayed_job to CreateOrUpdateEstimateForJob for the " \
            "duplicated_job_id:#{@duplicated_job.id}",
            job: @current_job_id

        CreateOrUpdateEstimateForJob.perform_async(@duplicated_job.id)
      end

    end
  end
end
