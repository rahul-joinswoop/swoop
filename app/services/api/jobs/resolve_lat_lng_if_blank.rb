# frozen_string_literal: true

# This service schedules LatLngResolver worker to fix missing lat/lng
# for both job.service_location and job.drop_location, in case
# any has an address present but does not have respective lat or lng.
module API
  module Jobs
    class ResolveLatLngIfBlank

      include SwoopService

      def initialize(job_id:)
        @job_id = job_id
      end

      def call
        raise ArgumentError if @job_id.blank?

        resolve_if_lat_or_lng_blank_for(job&.service_location)
        resolve_if_lat_or_lng_blank_for(job&.drop_location)
      end

      private

      def resolve_if_lat_or_lng_blank_for(location)
        return if location.blank? || location.address.nil? || (location.lat.present? && location.lng.present?)

        Rails.logger.debug(
          "JOB(@job_id) location(#{location.id}) without lat/lng. Scheduling async LatLngResolver"
        )

        LatLngResolver.perform_async(location.id)
      end

      def job
        @job ||= Job.find(@job_id)
      end

    end
  end
end
