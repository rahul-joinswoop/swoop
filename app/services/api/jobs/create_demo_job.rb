# frozen_string_literal: true

# This service creates a FleetJob from 'Demo Fleet Company' for a PartnerCompany.
#
# It guarantees that there is a rescue_provider associated between Demo Fleet Company and the @rescue_company.
#
# The job_params are hardcoded on demo_job_params method.
#
# The Job is created and assigned to the @rescue_company through the same services that UI uses;
# it mimics the way web does.
#
module API
  module Jobs
    class CreateDemoJob

      include SwoopService

      attr_reader :job

      def initialize(rescue_company_id:, **opts)
        @rescue_company_id = rescue_company_id
        @browser_platform = opts[:browser_platform]
        @user_agent = opts[:user_agent]
      end

      def execute
        @rescue_company = RescueCompany.find(@rescue_company_id)

        # (simulates User opening the Job form, it creates a pre-draft one)
        predraft_job = API::Jobs::CreateFleet.new(
          api_company: demo_fleet_company,
          api_user: demo_user,
          params: {},
          save_as_draft: false,
          save_as_predraft: true,
        ).call.job

        predraft_job.save!

        # (simulates User adding values to the job, and clicking on 'Create' on Job Form)
        @job = API::Jobs::UpdateFleet.new(
          company: demo_fleet_company,
          api_user: demo_user,
          job_id: predraft_job.id,
          params: demo_job_params,
          job_history: {},
          job_storage: nil,
          browser_platform: @browser_platform,
          request_user_agent: @request_user_agent,
          save_as_draft: false
        ).call.job

        @job.save!

        # make sure there's a rescue_provider between demo_fleet_company and the @rescue_company
        create_rescue_provider_if_needed

        # (simulates Fleet assigning the Partner to @rescue_company)
        JobService::FleetInhouseAssignRescueProviderService.new(
          @job, @rescue_company, @rescue_company.hq
        ).call

        @job.save!

        self
      end

      def after_execution
        ::Analytics.track({
          user_id: demo_user.to_ssid,
          event: 'Create Demo Job Click',
          properties: {
            category: 'Onboarding',
            job_id: @job.id,
            company: demo_fleet_company.name,
          },
        })
      end

      # For dev envs, make sure that seed 20181205114801_create_and_flag_demo_fleet_company_as_demo.rb
      # is applied.
      def demo_fleet_company
        @demo_fleet_company ||= FleetCompany.not_deleted.find_by_name(Company::DEMO_FLEET_COMPANY)
      end

      def create_rescue_provider_if_needed
        rescue_provider = RescueProvider.where(
          provider: @rescue_company,
          company: demo_fleet_company,
          site: @rescue_company.hq
        ).first_or_initialize

        if rescue_provider.new_record?
          rescue_provider.location = @rescue_company.hq.location

          rescue_provider.save!
        end
      end

      def demo_user
        demo_user = demo_fleet_company.users.not_deleted.first
        return demo_user if demo_user

        demo_user = User.new(
          full_name: Company::DEMO_FLEET_COMPANY,
          company: demo_fleet_company
        )
        demo_user.save!

        demo_fleet_company.users.reload

        demo_user
      end

      def question_results
        low_clearance = Question.fetch_by_question!(Question::LOW_CLEARANCE)
        attempted_to_jump = Question.fetch_by_question!(Question::ANYONE_ATTEMPTED_TO_JUMP)
        vehicle_stop = Question.fetch_by_question!(Question::VEHICLE_STOP_WHEN_RUNNING)
        customer_with_vehicle = Question.fetch_by_question!(Question::CUSTOMER_WITH_VEHICLE)
        keys_present = Question.fetch_by_question!(Question::KEYS_PRESENT)

        [
          {
            question_id: keys_present.id, answer_id: keys_present.fetch_answer!('Yes').id,
          },
          {
            question_id: customer_with_vehicle.id, answer_id: customer_with_vehicle.fetch_answer!('Yes').id,
          },
          {
            question_id: vehicle_stop.id, answer_id: vehicle_stop.fetch_answer!('Yes').id,
          },
          {
            question_id: attempted_to_jump.id, answer_id: attempted_to_jump.fetch_answer!('Yes').id,
          },
          {
            question_id: low_clearance.id, answer_id: low_clearance.fetch_answer!('Yes').id,
          },
        ]
      end

      def demo_job_params
        pickup_location_based_on_hq = @rescue_company.hq.location
        pickup_location_hash = pickup_location_based_on_hq.attributes.with_indifferent_access.slice(
          :address, :exact, :lat, :lng, :place_id
        )

        tow_company_location_type = LocationType.find_by_name(LocationType::TOW_COMPANY)
        pickup_location_hash[:location_type_id] = tow_company_location_type.id

        job_hash = {
          color: "Red",
          customer: {
            full_name: "Demo Customer",
            phone: "+14158434288",
          },
          fleet_dispatcher_id: demo_user.id,
          make: "Tesla",
          model: "Model S",
          odometer: "1337",
          send_sms_to_pickup: false,
          service_code_id: ServiceCode.find_by(name: ServiceCode::BATTERY_JUMP),
          service_location: pickup_location_hash,
          vin: "1HGCM82633A004352",
          will_store: false,
          year: "2016",
          text_eta_updates: true,
          notes: 'This is a demo job. You can accept it, assign a driver, and progress it through statuses to see how Swoop works!',
          symptom_id: Symptom.find_by_name(Symptom::DEAD_BATTERY).id,
          question_results: question_results,
        }

        API::MapJobParams.new(
          api_company: demo_fleet_company,
          api_user: demo_user,
          job: @job,
          params: job_hash
        ).call
      end

    end
  end
end
