# frozen_string_literal: true

module API
  module Jobs
    class CreateRescue

      include Utils
      include SwoopService

      attr_reader :job

      def initialize(api_company:, api_user:, params:, secondary_tow_params: {}, save_as_draft: false, save_as_predraft: false)
        Rails.logger.debug "API::Jobs::CreateRescue initializing with params:#{params.respond_to?(:to_unsafe_hash) ? params.to_unsafe_hash : params.to_hash}"

        raise ArgumentError, 'API::Jobs::CreateRescue non RescueCompany requested a rescue Job' unless api_company.rescue?

        @api_company = api_company
        @api_user    = api_user
        @originating_company_id = api_company.id
        @secondary_tow_params   = secondary_tow_params

        @params = API::MapJobParams.new(
          api_company: @api_company,
          api_user: @api_user,
          job: nil,
          params: params
        ).call

        @save_as_draft = save_as_draft
        @save_as_predraft = save_as_predraft
      end

      def execute
        Rails.logger.debug "API::Jobs::CreateRescue params #{JSON.pretty_generate(@params)}"

        if @params[:partner_dispatcher_id].nil? && @api_user
          @params[:partner_dispatcher_id] = @api_user.id
        end

        toa = remove_param(:toa, @params)

        @params[:originating_company_id] = @originating_company_id

        Rails.logger.debug "API::Jobs::CreateRescue Creating new RescueJob with #{@params}"
        @job = RescueJob.new(@params)

        if !@job.site
          @job.site = @api_company.hq || (raise ArgumentError, "API::Jobs::CreateRescue sites not configured for #{@api_company.name}")
        end

        @job.rescue_company = @api_company

        if @save_as_draft
          API::Jobs::InitializeDraft.new(
            api_company: @api_company,
            api_user: @api_user,
            job: @job
          ).call
        elsif @save_as_predraft
          API::Jobs::InitializePredraft.new(
            api_company: @api_company,
            api_user: @api_user,
            job: @job
          ).call
        else
          HandleToa.new(@api_company, @api_user, @job, toa).call

          # If logged in user has *only* driver role,
          # assign him as job.rescue_driver as per business rule.
          if !@job.rescue_driver && @api_user&.driver_only?
            @job.rescue_driver = @api_user
            Rails.logger.debug("#{self.class.name} - @api_user assigned as @job.rescue_driver")
          end

          API::Jobs::InitializeBase.new(
            api_company: @api_company,
            api_user: @api_user,
            job: @job
          ).call

          if @secondary_tow_params
            Delayed::Job.enqueue DispatchSecondaryJob.new(@job.id, @secondary_tow_params, @api_user&.id)
          end
        end

        JobService::ReverseGeocodeIfNecessary.new(@job).call

        self
      end

    end
  end
end
