# frozen_string_literal: true

module API
  module Jobs
    class ChoosePartner

      include LoggableJob
      include SwoopService

      attr_reader :job

      # {"job":{"rescue_provider_id":20,
      # "dispatch_email":"paul+4@joinswoop.com",
      # "id":1025,
      # "toa":{"latest":"2017-11-01T23:51:12.217Z"}}}
      def initialize(job:, api_user:, choose_params:)
        log :debug, "- Initializing with #{job.id}, #{choose_params}}", job: job.id

        raise ArgumentError, log_msg("- job_id required parameter", job: job.id) unless job.id
        raise ArgumentError, log_msg("- rescue_provider_id required parameter", job: job.id) unless choose_params[:rescue_provider_id]

        @job = job
        @api_user = api_user
        @rescue_provider = RescueProvider.find(choose_params[:rescue_provider_id])
        @toa = choose_params[:toa]
        @dispatch_email = choose_params[:dispatch_email]
        @billing_type = choose_params[:billing_type]
        @vcc_amount = choose_params[:vcc_amount]
        @goa_amount = choose_params[:goa_amount]
      end

      def execute
        @job.last_touched_by = @api_user

        if @job.fleet_managed_job?
          if @job.auction_live
            Auction::ManualAuctionService.new(@job.auction.id, @rescue_provider.id).call
          else
            JobService::SwoopAssignRescueProviderService.new(@job, @rescue_provider.provider, @rescue_provider.site).call
          end
          store_billing_info(@job)
        elsif @job.fleet_in_house_job?
          JobService::FleetInhouseAssignRescueProviderService.new(@job, @rescue_provider.provider, @rescue_provider.site).call
        else
          raise ArgumentError, log_msg("- Job type not allowed #{@jobs.class.name}", job: job.id)
        end

        HandleToa.new(@api_company, @api_user, @job, @toa).call

        @job.save!
      end

      def after_execution
        update_provider_dispatch_email_if_needed

        dispatch_email_to_rescue_provider_if_non_live_or_live_afterhours
      end

      private

      def update_provider_dispatch_email_if_needed
        log :debug, "update_provider_dispatch_email_if_needed, @dispatch_email is #{@dispatch_email}", job: @job_id

        return if @dispatch_email.blank?

        if @rescue_provider.dispatch_email != @dispatch_email
          @rescue_provider.dispatch_email = @dispatch_email

          log :debug,
              "update_provider_dispatch_email_if_needed, " \
              "rescue_provider.dispatch_email changed to #{@dispatch_email}",
              job: @job_id

          @rescue_provider.save!
        end
      end

      def dispatch_email_to_rescue_provider_if_non_live_or_live_afterhours
        if @rescue_provider.non_live_or_live_after_hours
          log :debug, "dispatch_email_to_rescue_provider_if_non_live_or_afterhours will dispatch email to rescue_company #{@rescue_provider.provider.id}", job: @job.id

          Delayed::Job.enqueue JobStatusEmail::Dispatched.new(job_id: @job.id, rescue_provider_id: @rescue_provider.id)
        else
          log :debug, "dispatch_email_to_rescue_provider_if_non_live_or_afterhours not sending dispatch email because !rescue_provider.non_live_or_live_after_hours", job: @job.id
        end
      end

      def store_billing_info(job)
        return unless @billing_type.present? || @vcc_amount.present? || @goa_amount.present?

        billing_info = job.billing_info || job.build_billing_info
        billing_info.rescue_company_id = @rescue_provider.provider_id

        if @billing_type.present? || @vcc_amount.present?
          billing_info.billing_type = @billing_type
          if @vcc_amount.to_i > 0
            billing_info.billing_type ||= BillingType::VCC
            billing_info.vcc_amount = @vcc_amount
          end
          billing_info.agent = @api_user
        end

        if @goa_amount.present?
          billing_info.goa_amount = @goa_amount if @goa_amount.to_i > 0
          billing_info.goa_agent = @api_user
        end
      end

    end
  end
end
