# frozen_string_literal: true

# Service to update a job coming from Web request.
# This code was extracted from JobsBaseontroller.
module API
  module Jobs
    class Update

      include LoggableJob
      include Utils
      include SwoopService

      ANDROID = 'android'
      OK_HTTP = 'okhttp'

      attr_reader :job

      def initialize(job:, api_user:, job_storage: false, browser_platform: nil, request_user_agent: nil)
        @job      = job
        @api_user = api_user
        @job_storage = job_storage
        @browser_platform = browser_platform
        @request_user_agent = request_user_agent
        @job_was_draft = @job.status == Job::STATUS_DRAFT
        @job_was_predraft = @job.status == Job::STATUS_PREDRAFT
        @get_location = false

        log :debug, "@pcc_coverage_id #{@job.pcc_coverage_id}", job: @job.id
      end

      def execute
        @job.last_touched_by = @api_user

        @get_location = @job.send_get_location_necessary?

        if @get_location
          @job.get_location_attempts += 1
        end

        @job.fixup_locations

        # TODO - why is this here instead of inside of UpdateJobStatus?
        if @job_was_draft || @job_was_predraft
          log :debug, "API::Jobs::Update about to call DispatchJob, job status #{@job.status}", job: @job.id

          # it moves the job to pending or dispatched
          DispatchJob.new(@job).call

          log :debug, "API::Jobs::Update finished calling DispatchJob, job status #{@job.status}", job: @job.id
        end

        # If job is completed but storage was checked then add storage to the job
        if (@job.status == Job::STATUS_COMPLETED) && @job.store_vehicle?

          JobService::CreateStorage.call(
            job: @job,
            user: @api_user,
          )

          @job.slack_notification('Vehicle Stored', tags: [:dispatch, :stored])
        end

        set_platform

        should_create_claim =
          (@job.pcc_coverage_id.present? && @job.pcc_claim_id.blank?) || # It means there's no claim yet for this job.
          (@job.pcc_coverage_id.present? && @job.pcc_coverage_id_changed?) # And it means the coverage has been updated on the job, we need a new claim.

        added_rescue_driver = @job.rescue_driver_id_change && @job.rescue_driver.present?
        removed_rescue_driver = @job.rescue_driver_id_was && !@job.rescue_driver
        driver_was_present = @job.rescue_driver_id_was.present?

        log :debug, "action Driver 2 #{added_rescue_driver} #{removed_rescue_driver}", job: @job_id

        if added_rescue_driver
          log :debug, "action Driver 2 #{driver_was_present ? 'reassigned' : 'assigned'}, will dispatch if haven't yet", job: @job_id

          # submits to issc
          # TODO move to own controller and call status update
          @job.partner_set_driver

        elsif removed_rescue_driver
          log :debug, "action Driver 2 removed", job: @job_id

          @job.partner_remove_driver

        end

        job_status_changed = job.status_changed?

        if @job.save
          log :debug, "-- Job saved, status #{@job.status}", job: @job.id, issclog: @job.id

          # PDW: JobForm
          # TODO: PartnerChangeRecipient only needs to be called if one of the storage properties is updated
          if @job.invoice && @job.invoice.editable? && (@job.saved_change_to_account_id? || @job_storage) # QB: The recipient sometimes changes based on storage properties
            # Call this once the job is saved so storage will reflect the proper state
            log :debug, "needs to update the invoice's recipient; the old recipient is #{@job.invoice.recipient_id} and the old recipient company is #{@job.invoice.recipient_company_id}", job: @job.id
            Delayed::Job.enqueue UpdateRecipientOnInvoice.new(@job.id)
          end

          if @job_was_draft || @job_was_predraft
            # this will trigger all flow needed for an active job
            # which will include sending location requests
            API::Jobs::ActivateJob.new(job: @job).call
          elsif @get_location
            @job.send_get_location
          end

          # go create a claim
          if should_create_claim
            log :debug, "pcc_coverage_id: #{@job.pcc_coverage_id} " \
              "about to trigger PCC::PCCService.claim_creation", job: @job_id

            PCC::PCCService.claim_creation(job: job)
          end

          if job_status_changed || added_rescue_driver
            @job.update_digital_dispatcher_status
          end

        end

        self
      end

      private

      def set_platform
        return if !@job.status_changed?
        if @job.status.eql?(Job::STATUS_COMPLETED)
          @job.platform = if @request_user_agent&.include?(OK_HTTP) || @request_user_agent&.include?(ANDROID)
                            ANDROID
                          else
                            @browser_platform
                          end

        end
      end

    end
  end
end
