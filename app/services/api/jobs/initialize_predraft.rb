# frozen_string_literal: true

module API
  module Jobs
    class InitializePredraft < Initialize

      include LoggableJob
      include SwoopService

      def initialize(api_company:, api_user:, job:)
        super(api_company: api_company, api_user: api_user, job: job)
      end

      def call
        super()

        build_audit_job_statuses

        set_job_status_as_predraft

        @job.driver.vehicle = @job.driver.vehicle.becomes!(StrandedVehicle)

        Rails.logger.debug "API::Jobs::InitializePredraft - calling job.save #{@job.inspect}"

        @job.save!

        log :debug, "- job created as predraft", job: @job.id
      end

      private

      def build_audit_job_statuses
        @job.hack_create_ajs('Initial')
        @job.hack_create_ajs('Predraft')
      end

      def set_job_status_as_predraft
        @job.status = Job::STATUS_PREDRAFT # manually setting it so we don't trigger automatic save on aasm
      end

    end
  end
end
