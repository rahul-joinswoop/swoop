# frozen_string_literal: true

#
#   API::Jobs::SendDetailsEmail.call(
#     input: {
#       to: 'to@email.com',
#       cc: 'cc@email.com',
#       bcc: 'bcc@email.com',
#       job_id: Job.where(fleet_company_id: 19).last.id,
#       user_id: 34,
#       company_id: 19
#     }
#   )
#

module API
  module Jobs
    class SendDetailsEmail

      include Interactor
      include Interactor::Swoop

      before do
        Rails.logger.debug "API::Jobs::SendDetailsEmail called #{context}"

        @to = context.input[:to]
        @cc = context.input[:cc]
        @bcc = context.input[:bcc]
        @job_id = context.input[:job_id]
        @user_id = context.input[:user_id]
        @company_id = context.input[:company_id]

        raise ArgumentError, 'API::Jobs::SendDetailsEmail to, cc and bcc are null' unless @to || @cc || @bcc

        raise ArgumentError, 'API::Jobs::SendDetailsEmail job_id is null' unless @job_id

        raise ArgumentError, 'API::Jobs::SendDetailsEmail user_id is null' unless @user_id

        raise ArgumentError, 'API::Jobs::SendDetailsEmail company_id is null' unless @company_id
      end

      def call
        @job = Job.find(@job_id)

        Delayed::Job.enqueue job_email_details

        CreateHistoryItem.call(
          job: @job,
          target: @job,
          event: HistoryItem::JOB_DETAILS_EMAILED % [@to],
        )
      end

      private

      def job_email_details
        recipients = { to: @to }
        recipients.merge!({ cc: @cc }) if @cc.present?
        recipients.merge!({ bcc: @bcc }) if @bcc.present?

        JobStatusEmail::Details.new(
          job_id: @job_id,
          user_id: @user_id,
          company_id: @company_id,
          recipients: recipients,
          job_status_email: job_status_email
        )
      end

      def job_status_email
        job_status_email = JobStatusEmailSelector.new(@job)

        if job_status_email.present? && job_status_email.email_class_name
          job_status_email.email_class_name.constantize.new(job_id: @job_id)
        end
      end

    end
  end
end
