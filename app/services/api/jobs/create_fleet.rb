# frozen_string_literal: true

module API
  module Jobs
    class CreateFleet

      include Utils
      include SwoopService

      attr_reader :job

      def initialize(api_company:, api_user:, params:, save_as_draft: false, save_as_predraft: false)
        Rails.logger.debug "Initializing CreateFleet with params:#{params}"

        @originating_company_id = api_company.id

        if api_company.is_swoop?
          acts_as_company_id = remove_param("acts_as_company_id", params)

          raise ArgumentError, 'Swoop tried to create a job but did not supply acts_as_company_id' unless acts_as_company_id

          @company = FleetCompany.find(acts_as_company_id)
          @fleet_dispatcher_id = nil
        else
          @company = api_company
          remove_param("root_dispatcher_id", params)
          remove_param("swoop_notes", params)
          @fleet_dispatcher_id = api_user&.id
        end

        @api_user = api_user

        @params = API::MapJobParams.new(
          api_company: @company,
          api_user: @api_user,
          job: nil,
          params: params
        ).call

        raise ArgumentError, 'Non Fleet Company requested a Fleet Job' unless @company.fleet?

        @save_as_predraft = save_as_predraft
        @save_as_draft = save_as_draft
      end

      def execute
        Rails.logger.debug "API::Job::CreateFleet params:#{@params}"
        p = @params
        toa = remove_param(:toa, p)

        remove_param(:rescue_provider_id, p) # ignore on create

        if p[:fleet_dispatcher_id].nil?
          p[:fleet_dispatcher_id] = @fleet_dispatcher_id
        end

        if p[:rescue_company] && p[:price]
          logger.warn "Disallowing setting of cost for Job:#{p}"
          remove_param(:price, p)
        end

        p[:originating_company_id] = @originating_company_id

        # NB - PG - The only "In House" supported through this API endpoint is tesla
        if @company.fleet_in_house?
          @job = FleetInHouseJob.new(p)
        elsif  @company.motor_club?
          # PDW: currently only used for testing in Fake::Generator
          @job = FleetMotorClubJob.new(p)
        else
          @job = FleetManagedJob.new(p)
        end

        if @save_as_draft
          API::Jobs::InitializeDraft.new(
            api_company: @company,
            api_user: @api_user,
            job: job
          ).call
        elsif @save_as_predraft
          API::Jobs::InitializePredraft.new(
            api_company: @company,
            api_user: @api_user,
            job: job
          ).call
        else
          HandleToa.new(@company, @api_user, job, toa).call

          API::Jobs::InitializeBase.new(
            api_company: @company,
            api_user: @api_user,
            job: job
          ).call
        end

        InvoiceService::CreateOrUpdateFleetCustomerInvoiceIfNecessary.new(
          job,
          job.customer
        ).call

        JobService::ReverseGeocodeIfNecessary.new(@job).call

        self
      end

    end
  end
end
