# frozen_string_literal: true

module API
  module Jobs
    class UpdateDraft

      include LoggableJob
      include Utils
      include SwoopService

      attr_reader :job

      def initialize(job:, params:, api_user:)
        @job      = job
        @params   = params
        @api_user = api_user
      end

      def execute
        remove_param(:status, @params) # will likely never come with status param; this line guards it anyways

        @job.last_touched_by = @api_user
        @job.assign_attributes(@params)

        @job.fixup_locations

        @job.driver.vehicle = @job.driver.vehicle.becomes!(StrandedVehicle)

        if @job.status == Job::STATUS_PREDRAFT
          if @api_user.company.super?
            @job.swoop_draft
          elsif @api_user.company.rescue?
            @job.partner_draft
          elsif @api_user.company.fleet?
            @job.fleet_draft
          end
        end

        if @job.save
          API::Jobs::UpdateHistory.new(job: @job, history: @job_history).call
        end

        log :debug, "-- Draft updated, status #{@job.status}", job: @job.id

        self
      end

    end
  end
end
