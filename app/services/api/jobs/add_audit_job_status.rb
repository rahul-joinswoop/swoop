# frozen_string_literal: true

#
#   API::Jobs::AddAuditJobStatus.call(
#     input: {
#       job_id: Job.last.id,
#       user_id: 34,
#       company_id: 19,
#       audit_job_status_attributes: {
#         last_set_dttm: Time.current,
#         job_status_name: "ON_SITE"
#       }
#     }
#   )
#
module API
  module Jobs
    class AddAuditJobStatus

      include LoggableJob
      include Interactor
      include Interactor::Swoop

      before do
        Rails.logger.debug "API::Jobs::AddAuditJobStatus called #{context}"

        @job_id = context.input[:job_id]
        @user_id = context.input[:user_id]
        @company_id = context.input[:company_id]
        @last_set_dttm = context.input.dig(:audit_job_status_attributes, :last_set_dttm)

        job_status_name = context.input.dig(:audit_job_status_attributes, :job_status_name)
        if JobStatus::NAME_MAP[job_status_name.to_sym].present?
          @job_status_id = JobStatus::NAME_MAP[job_status_name.to_sym]
        else
          @job_status_id = "JobStatus::#{job_status_name.upcase}".constantize
        end

        unless @job_id
          raise ArgumentError, "API::Jobs::AddAuditJobStatus job_id is null"
        end

        unless @user_id
          raise ArgumentError, "API::Jobs::AddAuditJobStatus @user_id is null"
        end

        unless @company_id
          raise ArgumentError, "API::Jobs::AddAuditJobStatus @company_id is null"
        end

        unless @job_status_id
          raise ArgumentError, "API::Jobs::AddAuditJobStatus @job_status_id is null"
        end

        unless @last_set_dttm
          raise ArgumentError, "API::Jobs::AddAuditJobStatus @last_set_dttm is null"
        end
      end

      def call
        audit_job_status = AuditJobStatus.find_by(
          job_id: @job_id,
          job_status_id: @job_status_id
        )

        if audit_job_status.present?
          log :info, "audit_job_status already exists for job_status id #{@job_status_id}", job: @job_id
        else
          audit_job_status = AuditJobStatus.create!(
            job_id: @job_id,
            user_id: @user_id,
            company_id: @company_id,
            job_status_id: @job_status_id,
            last_set_dttm: @last_set_dttm
          )

          job.base_publish('update')
        end

        context.output = audit_job_status
      end

      private

      def job
        Job.find(@job_id)
      end

    end
  end
end
