# frozen_string_literal: true

module API
  module Jobs
    class Initialize

      include SwoopService

      def initialize(api_company:, api_user:, job:)
        @api_company = api_company
        @api_user    = api_user
        @job         = job
      end

      def call
        set_user_to_job
        set_fleet_company_to_job
        set_fake_driver_to_job_if_needed
      end

      private

      def set_user_to_job
        if @api_user
          @job.user = @api_user
          @job.last_touched_by = @api_user
        end
      end

      def set_fleet_company_to_job
        @job.fleet_company = @api_company unless @api_user&.has_role?(:root) && @job.fleet_company
      end

      def set_fake_driver_to_job_if_needed
        if !@job.driver
          Rails.logger.debug "API::Jobs::Initialize @job.driver is nil, setting @job.driver = Drive.new"

          @job.driver = Drive.new
        end

        if !@job.driver.user
          Rails.logger.debug "API::Jobs::Initialize @job.driver.user is nil, setting @job.driver.user = User.new"

          user = User.new

          @job.driver.user = user
        end

        if !@job.driver.vehicle
          Rails.logger.debug "API::Jobs::Initialize @job.driver.vehicle is nil, setting @job.driver.vehicle = Vehicle.new"

          @job.driver.vehicle = Vehicle.new
        end
      end

    end
  end
end
