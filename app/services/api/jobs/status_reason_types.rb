# frozen_string_literal: true

# Fetches status reason types that can be used by the given Job and JobStatus.id.
#
# Logic for Accept and Reject were copied from FE.
#
# For Accept, @see https://github.com/joinswoop/swoop/blob/master/app/assets/javascripts/components/modals/DriverMapModal/ETAInputContainer.js#L93-L96
# For Rejecte, @see https://github.com/joinswoop/swoop/blob/master/app/assets/javascripts/stores/reject_reason_store.coffee#L21-L46
#
# There are companies that require custom status reasons. These reasons are stored in CompanyJobStatusReasonType table.
# Such companies are flagged as company.use_custom_job_status_reasons in the db. If no custom reason is found, it falls back to JobStatusReasonType.standard_items.
#
# If no custom or starndard items found, it returns an empty collection.
#
# REJECT case has a specific logic: if job.fleet_company does not use_custom_job_status_reasons, its reasons will only be available if job.fleet_company is not a motor club.
#
# The default case is to return JobStatusReasonType.standard_items filtered by the given job_status_id.
module API
  module Jobs
    class StatusReasonTypes

      def initialize(job:, job_status_id:)
        @job = job
        @job_status_id = job_status_id
      end

      def call
        fleet_company = @job.fleet_company

        if fleet_company.use_custom_job_status_reasons?
          custom_reasons = fleet_company.custom_job_status_reason_types

          custom_reasons.any? ? custom_reasons : standard_reasons
        else
          case @job_status_id
          when JobStatus::REJECTED
            if !fleet_company.motor_club?
              standard_reasons
            else
              JobStatusReasonType.none
            end
          else
            standard_reasons
          end
        end
          .where(job_status_id: @job_status_id)
          .order(Arel.sql("job_status_reason_types.text = 'Other' ASC"))
      end

      private

      def standard_reasons
        JobStatusReasonType.standard_items
      end

    end
  end
end
