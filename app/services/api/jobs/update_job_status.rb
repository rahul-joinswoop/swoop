# frozen_string_literal: true

# Super class to PartnerUpdateJobState, FleetUpdateJobState and SwoopUpdateJobState.
#
# It has common beahiour between ^ related to Job status changes.
# It accepts a JobStatus#name and a JobStatusReasonType#id
module API
  module Jobs
    class UpdateJobStatus

      class CustomActionError < StandardError; end

      include LoggableJob
      include SwoopService

      def initialize(job, status, **opts)
        @job       = job
        @status    = status
        @job_status_reason_type_id = opts[:job_status_reason_type_id]
        @api_user = opts[:api_user]
        @api_application = opts[:api_application]
        @user_agent_hash = opts[:user_agent_hash] || {}
        @referer = opts[:referer]

        log :debug,
            "called with STATUS(#{@status}) and " \
            "JobStatusReasonType(#{@job_status_reason_type_id})",
            job: @job.id
      end

      def execute
        @job.last_touched_by = @api_user if @api_user

        method = "#{self.class::SERVICE_TYPE}_" + @status.delete(' ').downcase
        @job.job_status_change(method)

        @job.save!

        ViewAlertService.cancel_if_necessary(@job, @status)

        execute_custom_actions_for_status

        build_job_status_reason

        push_analytics_message
      end

      protected

      def execute_custom_actions_for_status
        raise NotImplementedError, "Subclasses must implement this method"
      end

      private

      def push_analytics_message
        return unless @api_user || @api_application

        AnalyticsData::JobStatus.new(
          job: @job,
          user_agent_hash: {
            platform: @user_agent_hash[:platform],
            version: @user_agent_hash[:version],
          },
          referer: @referer,
          api_user: @api_user,
          api_application: @api_application,
        ).push_message
      end

      def build_job_status_reason
        return unless @job_status_reason_type_id

        @job.job_status_reasons.build(
          job_status_reason_type_id: job_status_reason_type.id,
          audit_job_status: @job.audit_job_statuses.find_by!(
            job_status: job_status_reason_type.job_status
          )
        )
      end

      def job_status_reason_type
        @job_status_reason_type ||= JobStatusReasonType.find(@job_status_reason_type_id)
      end

    end
  end
end
