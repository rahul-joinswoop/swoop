# frozen_string_literal: true

# This service is the glue between GraphQL and the existing services built for
# the REST API. It will convert the Hash of arguments for a GraphQL Job mutation
# into the same format as the request parameters used by the REST API.
module API
  class MapJobInput

    def initialize(api_company:, api_user: nil, job: nil, params:)
      @api_company = api_company
      @api_user = api_user
      @job = job
      @params = params
    end

    def call
      job_params = @params.dig(:job).to_h

      params = {}

      # TODO: - is this right? how do we tell if it's a fleet_dispatcher vs
      # a root_/partner_?
      params[:fleet_dispatcher_id] = @api_user.id if @api_user.present?

      copy_params!(job_params, params, [:po_number, :ref_number, :priority_response])
      map_object_ids(job_params, params, payment_type: :customer_type_id)
      map_object_ids(job_params, params, payment_type_name: :customer_type_id)

      map_account_params!(job_params, params)
      map_caller_params!(job_params, params)
      map_client_params!(job_params, params)
      map_customer_params!(job_params, params)
      map_eta_params!(job_params, params)
      map_location_params!(job_params, params)
      map_notes_params!(job_params, params)
      map_partner_params!(job_params, params)
      map_service_params!(job_params, params)
      map_status_params!(job_params, params)
      map_texts_params!(job_params, params)
      map_vehicle_params!(job_params, params)

      # default value
      save_as_draft = false

      status = job_params.dig(:status)

      if @job.present?
        # editing an existing job
        if @job.draft?
          if status.present? && status == Job::STATUS_PENDING && params.except(:fleet_dispatcher_id).blank?
            # transitioning from draft -> pending on an existing job. this means
            # we were called from EditJobStatus . we check to make sure we're
            # not being called with any other params since that would signify
            # being called from EditJob (the only exception is :fleet_dispatcher_id
            # since we add that in whenever it's available). we don't need to pass
            # on the status in params since there's only one possiblity.
            #
            # we intentionally set save_as_draft: false here since we want to
            # transition out of it.
            save_as_draft = false
          else
            # otherwise we're editing an existing job which is currently in draft
            # so we need to set this to true - this is how we preserve draft status
            save_as_draft = true
          end
        end
      else
        # creating a new job, set save_as_draft based on any provided status
        save_as_draft = (status == Job::STATUS_DRAFT)
      end

      # TODO - we could implement this just like above, for now just hardcode it
      save_as_predraft = false

      {
        api_company: @api_company,
        api_user: @api_user,
        params: params,
        save_as_draft: save_as_draft,
        save_as_predraft: save_as_predraft,
      }.tap { |p| p[:job] = @job if @job.present? }
    end

    private

    # Copy parameters from one hash to another. The field_map can be either
    # a hash specifying the mapping of keys from one hash to the other or an
    # array specifying the keys to copy.
    def copy_params!(from_hash, to_hash, field_map)
      return if from_hash.blank?
      field_map.each do |from_key, to_key|
        to_key ||= from_key
        if from_hash.include?(from_key)
          to_hash[to_key] = from_hash[from_key]
        end
      end
    end

    # Map object's id from one hash to a key in another. The field_map can be either
    # a hash specifying the mapping of keys from one hash to the other or an
    # array specifying the keys to copy.
    def map_object_ids(from_hash, to_hash, field_map)
      return if from_hash.blank?
      field_map.each do |from_key, to_key|
        to_key ||= from_key
        if from_hash.include?(from_key)
          object = from_hash[from_key]
          # Need to use the ternary since the parameter coercion we use will map nil objects
          # to an empty hash.
          to_hash[to_key] = (object.present? ? object.id : nil)
        end
      end
    end

    # MapJobInput is mainly used by AddJob and EditJob mutations.
    #
    # But it can also be used by EditJobStatus. There are a couple of JobStatus updates that are
    # actually a Job update in the API, they're treated differently.
    def map_status_params!(job_params, output_params)
      return unless @job # nothing to do if job is currently being created.
      return if @job.draft? # users can't trigger job status updates for a draft job.

      status = job_params[:status]
      return unless status

      output_params[:status] = status
      if job_params[:reason]
        job_status_id = JobStatus::NAME_MAP[Job.statuses[status].to_sym]
        output_params[:job_status_reason_type] = JobStatusReasonType.find_by!(text: job_params[:reason], job_status_id: job_status_id)
      end
    end

    # the reason for this is that we need to convert caller[:name] into caller[:full_name]
    def map_caller_params!(job_params, output_params)
      caller_params = job_params[:caller]
      return unless caller_params
      caller = {}.tap do |c|
        c[:full_name] = caller_params[:name] if caller_params.key?(:name)
        c[:phone] = caller_params[:phone] if caller_params.key?(:phone)
      end

      if @job&.caller_id && caller.size == 1
        # if we already have a caller record and we're only changing one record treat it as an update
        # to the existing record and add our id to caller. see the update_only docs here:
        # https://api.rubyonrails.org/classes/ActiveRecord/NestedAttributes/ClassMethods.html#method-i-accepts_nested_attributes_for
        # for why we're doing this
        caller[:id] = @job.caller_id
      end

      output_params[:caller] = caller if caller.present?
    end

    def map_eta_params!(job_params, output_params)
      eta_params = job_params[:eta]
      return unless eta_params

      bta = eta_params.dig(:bidded)
      output_params[:bta] = bta if bta.present?

      toa = eta_params.dig(:current)
      output_params[:toa] = { latest: toa } if toa.present?
    end

    def map_service_params!(job_params, output_params)
      service_params = job_params[:service]
      return unless service_params

      if service_params[:answers].present?
        question_results = []
        service_params[:answers].each do |qa|
          # these shouldn't ever fail because they've already been checked by graphql
          # before we get here
          question = Question.fetch_by_question!(qa[:question])
          answer = question.fetch_answer!(qa[:answer])
          result = { question_id: question.id, answer_id: answer.id }
          result[:answer_info] = qa[:extra] if qa[:extra].present?
          question_results << result
        end
        output_params[:question_results] = question_results
      end

      output_params[:symptom_id] = service_params[:symptom]&.id if service_params.include?(:symptom)
      output_params[:service_code_id] = service_params[:name].id if service_params[:name].present?
      output_params[:will_store] = !!service_params[:will_store] if service_params.include?(:will_store)
      output_params[:storage_type_id] = service_params[:storage_type]&.id if service_params.key?(:storage_type)
      output_params[:storage_type_id] = service_params[:storage_type_name]&.id if service_params.key?(:storage_type_name)
      output_params[:scheduled_for] = service_params[:scheduled_for] if service_params.include?(:scheduled_for)

      # if so, then it needs to be converted into the job.drop_location
      if service_params.include?(:storage_site)
        storage_site = service_params[:storage_site]
        storage_site_location = storage_site.location.attributes.symbolize_keys.slice(
          :address, :lat, :lng, :exact, :place_id, :location_type_id
        )
        storage_site_location[:site_id] = storage_site.id

        output_params[:drop_location] = storage_site_location
      end

      map_object_ids(service_params, output_params, class_type: :invoice_vehicle_category_id)
      map_object_ids(service_params, output_params, class_type_name: :invoice_vehicle_category_id)
    end

    # Map pick up and drop off locations and/or contacts
    def map_location_params!(job_params, output_params)
      location_params = job_params[:location]
      return unless location_params

      copy_params!(location_params, output_params, [:pickup_contact, :dropoff_contact])

      # support contact.name as a preferred input to contact.fullName
      [:pickup_contact, :dropoff_contact].each do |contact|
        if output_params.dig(contact, :name)
          output_params[contact][:full_name] = output_params[contact][:name]
        end
      end

      if location_params.include?(:service_location)
        # we need to keep all existent @job.service_location data
        output_params[:service_location] = if @job&.service_location
                                             @job.service_location.attributes.with_indifferent_access
                                           else
                                             HashWithIndifferentAccess.new
                                           end
        # and only override the ones that we receive from clients
        output_params[:service_location].merge!(munge_location_params(location_params[:service_location]))
      end

      # We also check if output_params[:drop_location].nil?. If not nil, it means that is has
      # previously been set by job.service.storage_site location attributes.
      if location_params.include?(:dropoff_location) && output_params[:drop_location].nil?
        # we need to keep all existent @job.drop_location data
        output_params[:drop_location] = if @job&.drop_location
                                          @job.drop_location.attributes.with_indifferent_access
                                        else
                                          HashWithIndifferentAccess.new
                                        end
        # and only override the ones that we receive from clients
        output_params[:drop_location].merge!(munge_location_params(location_params[:dropoff_location]))
      end
    end

    # Convert pickup/dropoff location
    def munge_location_params(location_hash)
      output_hash = {}
      if location_hash.present?
        copy_params!(location_hash, output_hash, [:address, :lat, :lng, :exact])
        copy_params!(location_hash, output_hash, google_place_id: :place_id)
        map_object_ids(location_hash, output_hash, location_type: :location_type_id)
      end
      output_hash
    end

    def map_vehicle_params!(job_params, output_params)
      copy_params!(job_params[:vehicle],
                   output_params,
                   [:make, :model, :year, :color, :license, :odometer, :serial_number, :unit_number, :tire_size, :vin])

      copy_params!(job_params[:vehicle], output_params, style: :vehicle_type)
    end

    def map_notes_params!(job_params, output_params)
      copy_params!(job_params[:notes], output_params, {
        customer: :notes,
        internal: @api_company.internal_notes_field,
      })
    end

    def map_texts_params!(job_params, output_params)
      copy_params!(job_params[:texts], output_params, {
        send_texts_to_pickup: :send_sms_to_pickup,
        send_review: :review_sms,
        send_driver_tracking: :partner_sms_track_driver,
        send_eta_updates: :text_eta_updates,
      })
    end

    # Move around the job parameters listed under the job.client namespace.
    def map_client_params!(job_params, output_params)
      client_params = job_params[:client]
      return unless client_params

      map_object_ids(client_params, output_params, department: :department_id, site: :fleet_site_id)
    end

    # Move around the job parameters listed under the job.customer namespace.
    def map_customer_params!(job_params, output_params)
      output_customer_params = {}
      customer_params = job_params[:customer]
      return unless customer_params

      copy_params!(customer_params, output_customer_params, [:full_name, :phone, :member_number])
      output_customer_params[:full_name] = customer_params[:name] if customer_params[:name].present?
      output_params[:email] = customer_params[:email] if customer_params.include?(:email)

      if customer_params[:member_number].present?
        output_customer_params[:customer] = Customer.new(member_number: customer_params[:member_number], company: @api_company)
      end

      output_params[:customer] = output_customer_params unless output_customer_params.empty?
    end

    def map_partner_params!(job_params, output_params)
      partner_params = job_params[:partner]
      return unless partner_params

      driver_params = partner_params[:driver]
      if driver_params && driver_params.key?(:name)
        if driver_params.dig(:name).present?
          driver = User.search_by_full_name(driver_params.dig(:name))
            .where(company: @api_company).with_role(:driver).not_deleted.first
          output_params[:rescue_driver_id] = driver.id
        else
          # clearing out current department value
          output_params[:rescue_driver_id] = nil
        end
      end

      department_params = partner_params[:department]
      if department_params && department_params.key?(:name)
        if department_params.dig(:name).present?
          department = ::Department.find_by!({ name: department_params.dig(:name), company: @api_company })
          output_params[:partner_department_id] = department.id
        else
          # clearing out current department value
          output_params[:partner_department_id] = nil
        end
      end

      vehicle_params = partner_params[:vehicle]
      if vehicle_params && vehicle_params.key?(:name)
        if vehicle_params.dig(:name).present?
          vehicle = ::RescueVehicle.find_by!({ name: vehicle_params.dig(:name), company: @api_company })
          output_params[:rescue_vehicle_id] = vehicle.id
        else
          # clearing out current department value
          output_params[:rescue_vehicle_id] = nil
        end
      end

      trailer_params = partner_params[:trailer]
      if trailer_params && trailer_params.key?(:name)
        if trailer_params.dig(:name).present?
          trailer = ::TrailerVehicle.find_by!({ name: trailer_params.dig(:name), company: @api_company })
          output_params[:trailer_vehicle_id] = trailer.id
        else
          # clearing out current department value
          output_params[:trailer_vehicle_id] = nil
        end
      end
    end

    def map_account_params!(job_params, output_params)
      account_params = job_params[:account]
      return unless account_params

      if account_params.key?(:name)
        if account_params.dig(:name).present?
          account = Account.find_by!({ name: account_params.dig(:name), company: @api_company })
          output_params[:account] = { id: account.id }
        else
          # clearing out current account value
          output_params[:account] = { id: nil }
        end
      end
    end

  end
end
