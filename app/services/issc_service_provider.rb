# frozen_string_literal: true

class IsscServiceProvider < Service

  def initialize(connection_manager: ISSC::ConnectionManager.new)
    Rails.logger.debug "IsscServiceProvider initialized"
    @issccon = connection_manager
  end

  #   {
  #       "ClientID": "AGO",
  #       "ContractorID": "3447567",
  #       "LocationID": "6",
  #       "TaxID": "34-4556573",
  #       "Token": "0A17031C-2E17423E-A8B204C5-F6467461",
  #       "ContactName": "Site 1",
  #       "ContactPhone": "4155551234",
  #       "ProviderName": "Finish Line Towing",
  #       "ProviderPhone": "4155551234",
  #       "LoggedIn": true
  #   },
  #
  #   {
  #       "ClientID": "GCO",
  #        "ContractorID": "OH45454545",
  #       "TaxID": "86-3765292",
  #       "Token": "F2F6FEAF-5E5743F7-B1CAEAD5-3BFCA9D8",
  #       "ContactName": "Site 1",
  #       "ContactPhone": "4155551234",
  #       "ProviderName": "Demo Tow Company 1",
  #       "ProviderPhone": "4155552222",
  #       "LoggedIn": true
  #   },

  def call
    ret = @issccon.issc_client.serviceproviders
    Rails.logger.debug "IsscServiceProvider response: #{JSON.pretty_generate(ret)}"
    ret['ServiceProviders'].each do |provider|
      issc = Issc.find_live_issc(provider['ContractorID'], provider['ClientID'], provider['LocationID'])

      if issc
        Rails.logger.debug "IsscServiceProvider found: #{issc.contractorid}:#{issc.clientid}:#{issc.issc_location&.locationid} #{issc.status} #{provider['LoggedIn']}"
        if provider['LoggedIn']
          if issc.status != 'logged_in'
            Rails.logger.debug "IsscServiceProvider updating to logged_in"
            issc.update!(status: 'logged_in')
          end
        else
          if issc.status == 'logged_in'
            Rails.logger.debug "IsscServiceProvider updating to registered"
            issc.update!(status: 'registered')
          end
        end
      else
        # Rails.logger.debug "IsscServiceProvider not found: #{provider['ContractorID']} #{provider['ClientID']} #{provider['LocationID']}"

      end
    end
  end

end
