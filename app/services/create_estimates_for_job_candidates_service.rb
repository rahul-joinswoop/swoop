# frozen_string_literal: true

class CannotCreateEstimatesForJobCandidatesWithoutArrayOfCandidatesError < ArgumentError; end
class CannotCreateEstimatesForJobCandidatesServiceWithHeterogeneousCandidatesError < ArgumentError; end
class CannotCreateEstimatesForJobCandidatesWithoutAJobError < ArgumentError; end
class CannotCreateEstimatesForJobCandidatesWithoutAnAuctionError < ArgumentError; end
class CannotCreateEstimatesForJobCandidatesWithoutJobServiceLocationError < ArgumentError; end

#
# CREATE ESTIMATES FOR JOB CANDIDATES
#
# Take a bunch of job candidates, and:
# 1. Call Google Distance Matrix Service and get the distance and time:
#    a. from the starting location
#    b. to the service location
#    c. to the drop location (optionally)
#    d. back to the starting location
# 2. Populate estimates for each one
# 3. Copy the results into the google_eta and google_miles fields
# 4. If eligible, mark the candidate as eligible
#
class CreateEstimatesForJobCandidatesService < Service

  attr_reader :candidates, :auction, :job, :candidate_type_klass

  def initialize(candidates:, auction:, job:)
    raise CannotCreateEstimatesForJobCandidatesWithoutArrayOfCandidatesError unless candidates.is_a?(Array) &&
      !candidates.empty? &&
      candidates.all? { |el| el.is_a?(Job::Candidate) }
    raise CannotCreateEstimatesForJobCandidatesServiceWithHeterogeneousCandidatesError unless
      candidates.all? { |el| el.instance_of?(Job::Candidate::Site) } ||
      candidates.all? { |el| el.instance_of?(Job::Candidate::Truck) }
    raise CannotCreateEstimatesForJobCandidatesWithoutAJobError unless job.is_a?(Job)
    raise CannotCreateEstimatesForJobCandidatesWithoutAnAuctionError unless auction.is_a?(Auction::Auction)
    raise CannotCreateEstimatesForJobCandidatesWithoutJobServiceLocationError if job.service_location.nil?

    @candidates = candidates
    @auction = auction
    @job = job
    @candidate_type_klass = candidates.first.class # They're guaranteed to all be the same type because of CannotCreateEstimatesForJobCandidatesServiceWithDifferentTypesOfCandidatesError
  end

  def call
    # Step 1: Find the starting location of each candidate
    starting_locations_of_all_candidates = @candidates.map(&:starting_location)

    # Step 2: Call Google Distance Matrix Service, and calculate from the starting
    # locations...
    waypoint_results_from_google = if @job.drop_location
                                     # ...to the service location, then to the drop location, and finally back to the starting locations
                                     External::WaypointDistanceMatrixService.as_to_b_to_c_to_as(
                                       starting_locations_of_all_candidates,
                                       @job.service_location,
                                       @job.drop_location,
                                       External::AToBToCToARoundTripPossibility
                                     )
                                   else
                                     # ...to the service location, and back to the starting locations
                                     External::WaypointDistanceMatrixService.as_to_b_to_as(
                                       starting_locations_of_all_candidates,
                                       @job.service_location,
                                       External::AToBToARoundTripPossibility
                                     )
                                   end

    # Step 3: Mash up the Google distance results with the candidates, and for each candidate:
    # a) build an estimate with the ETA and miles
    # b) deep copy the ETA and miles to the candidate itself
    # c) determine out if the candidate could do the job if he won
    @candidates.zip(waypoint_results_from_google).each do |candidate, waypoints_results_for_candidate|
      if waypoints_results_for_candidate.nil? || waypoints_results_for_candidate.doesnt_have_all_results?
        candidate.vehicle_eligible = :eta_missing
      else
        # Step 3.a: Build an estimate with the ETA and miles
        candidate.create_estimate!(
          job: @job,
          service_location: @job.service_location,
          site_location: candidate.starting_location,
          meters_ab: waypoints_results_for_candidate.a_to_b_in_meters,
          seconds_ab: waypoints_results_for_candidate.a_to_b_in_seconds,
          meters_ba: (waypoints_results_for_candidate.b_to_a_in_meters if waypoints_results_for_candidate.is_a?(External::AToBToARoundTripPossibility)),
          seconds_ba: (waypoints_results_for_candidate.b_to_a_in_seconds if waypoints_results_for_candidate.is_a?(External::AToBToARoundTripPossibility)),
          meters_bc: (waypoints_results_for_candidate.b_to_c_in_meters if waypoints_results_for_candidate.is_a?(External::AToBToCToARoundTripPossibility)),
          seconds_bc: (waypoints_results_for_candidate.b_to_c_in_seconds if waypoints_results_for_candidate.is_a?(External::AToBToCToARoundTripPossibility)),
          meters_ca: (waypoints_results_for_candidate.c_to_a_in_meters if waypoints_results_for_candidate.is_a?(External::AToBToCToARoundTripPossibility)),
          seconds_ca: (waypoints_results_for_candidate.c_to_a_in_seconds if waypoints_results_for_candidate.is_a?(External::AToBToCToARoundTripPossibility)),
        )

        # Step 3.b: Deep copy the ETA and miles of the A->B to the candidate itself
        candidate.google_eta = case
                               when Job::Candidate::Site == @candidate_type_klass
                                 waypoints_results_for_candidate.a_to_b_in_seconds / 60
                               when Job::Candidate::Truck == @candidate_type_klass
                                 if waypoints_results_for_candidate.a_to_b_in_seconds && candidate.vehicle.has_no_jobs?
                                   waypoints_results_for_candidate.a_to_b_in_seconds / 60
                                 elsif waypoints_results_for_candidate.a_to_b_in_seconds && candidate.vehicle.is_onsite? && candidate.vehicle.time_onsite
                                   (waypoints_results_for_candidate.a_to_b_in_seconds + (20.minutes - candidate.vehicle.time_onsite)) / 60
                                 end
                               end
        candidate.google_miles = waypoints_results_for_candidate.a_to_b_in_meters * RescueVehicle::METERS_MILES

        # Step 3.c: Determine out if the candidate could do the job if he won
        candidate.vehicle_eligible = case
                                     when Job::Candidate::Site == @candidate_type_klass
                                       candidate.google_eta < @auction.max_candidate_eta ? :eligible : :eta_too_high
                                     when Job::Candidate::Truck == @candidate_type_klass
                                       if !(candidate.vehicle.has_no_jobs? || candidate.vehicle.has_auto_dispatchable_job?)
                                         :busy
                                       elsif !candidate.vehicle.driver
                                         :no_driver
                                       elsif !candidate.vehicle.is_available?
                                         :driver_not_on_duty
                                       else
                                         :eligible
                                       end
                                     end
      end

      candidate.save!
    end
  end

end
