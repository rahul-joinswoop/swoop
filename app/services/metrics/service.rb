# frozen_string_literal: true

#
# Track metrics in the system
#
# Sends to metrics table and Split
# Updates any experiments that are running with the metrics via Experiments::Service
#
# Potentially in the future this could be one place to route all metrics thru e.g. segment, GA
#

module Metrics
  class Service

    #
    # target: Object you are tracking against e.g. user, job
    # action: Action the target took e.g. clicked
    # label:  Name of the metric e.g. 'cost'
    # value:  Integer value for the metric
    #
    def self.track(target, action, label, value)
      Metric.create!(target: target, action: action, label: label, value: value)

      Experiments::Service.track_metric(target, label, value)

      External::Split.track(target,
                            action,
                            label,
                            value)
    end

  end
end
