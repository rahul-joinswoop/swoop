# frozen_string_literal: true

# Fleet has selected a state within the drop down
#
class FleetUpdateJobState < API::Jobs::UpdateJobStatus

  SERVICE_TYPE = :fleet
  LEGAL_STATES = [
    # not used in the state machine below but still a legal state - any
    # attempts to change to pending get intercepted by EditJob instead
    "Pending",
    "Dispatched",
    "En Route",
    "On Site",
    "Towing",
    "Tow Destination",
    "Completed",
    "GOA",
    "Canceled",
    "Reassigned",
    "Deleted",
  ].freeze

  private

  def execute_custom_actions_for_status
    case @status
    when "Dispatched"
    when "En Route"
    when "On Site"
    when "Towing"
    when "Tow Destination"
    when "Completed"
      @job.send_user_invoice
      @job.send_review_sms
    when "GOA"
      @job.mark_invoice_goa
    when "Canceled"
      @job.cancel_invoice
      @job.cancel_auction
    when "Reassigned"
      @job.after_fleet_reasssigned
      @job.delete_invoice
    when "Deleted"
      @job.deleted_at = Time.zone.now
      @job.delete_invoice
    else
      raise CustomActionError, "Unknown job status: #{@status}"
    end
  end

end
