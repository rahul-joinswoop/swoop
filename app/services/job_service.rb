# frozen_string_literal: true

module JobService
  class PaymentTypeChangeNotAllowedError < StandardError; end
end
