# frozen_string_literal: true

class IsscProviderLoggedOut < IsscService

  def call
    issc = find_issc
    if issc.logging_out?
      issc.provider_logged_out
    else
      Rails.logger.debug "IsscProviderLoggedOut event #{@event}"
      if @event[:Description].include? "Switched"
        Rails.logger.debug "IsscProviderLoggedOut ISSC(#{issc.id}) Received a switched logout for company:#{issc.company.name} site:#{issc.site.name}"

        # Think this means it's been logged in on another software platform
        issc.force_logged_out
        issc.save!
      else
        Rails.logger.warn "IsscProviderLoggedOut ISSC(#{issc.id}) Received an unexpected logout for company:#{issc.company.name} site:#{issc.site.name}"
      end
    end

    issc
  end

end
