# frozen_string_literal: true

module Interactor
  module Swoop

    def self.included(descendant)
      descendant.send(:include, ::Interactor) unless descendant.include?(::Interactor)
      descendant.extend(ClassMethods)
    end

    module ClassMethods

      def _apply(context)
        if context[:output]
          context[:input] = context[:output]
        end
        raise ArgumentError, "No context['input'] specified to Interactor::Swoop" unless context[:input]
      end

      def call(context = {})
        _apply(context)
        new(context).tap(&:run).context
      end

      def call!(context = {})
        _apply(context)
        new(context).tap(&:run!).context
      end

    end

  end
end
