# frozen_string_literal: true

class AdjustedCreatedAtCalculator

  def initialize(job, audit_job_status = nil)
    @job = job
    @audit_job_status = audit_job_status
  end

  def calculate
    if @job.scheduled_for
      return @job.scheduled_for
    end

    if ajs&.adjusted_created_at
      return ajs.adjusted_created_at
    end

    @job.created_at || Time.zone.now
  end

  private

  def ajs
    if @audit_job_status
      @audit_job_status
    else
      @job.audit_job_status_created
    end
  end

end
