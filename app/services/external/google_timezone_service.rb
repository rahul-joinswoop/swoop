# frozen_string_literal: true

module External
  class GoogleTimezoneService < Service

    def initialize(lat, lng)
      raise ArgumentError, "lat is nil" if lat.nil?
      raise ArgumentError, "lng is nil" if lng.nil?
      @lat = lat
      @lng = lng
    end

    def location_string
      "#{@lat},#{@lng}"
    end

    def params
      {
        location: location_string,
        timestamp: 1331161200,
      }
    end

    def call
      url = Swoop::GoogleMapsHelper.query_url(Swoop::GoogleMapsHelper::TIMEZONE, params)
      Rails.logger.debug "GoogleTimezoneService Hitting google timezone API #{url}"
      res = JSON.load(open(url, { read_timeout: 20, ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE }))
      # Rails.logger.debug "Res: #{res}"
      res.present? ? res['timeZoneId'] : nil
    end

  end
end
