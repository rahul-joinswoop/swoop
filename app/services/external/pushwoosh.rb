# frozen_string_literal: true

class External::Pushwoosh < Service

  extend HTTPClient
  BASE_URI = 'https://cp.pushwoosh.com/json/1.3'
  CREATE_MESSAGE_URI = "#{BASE_URI}/createMessage"
  SOUND_BELLS_MONO = 'bells_mono.wav'

  class Error < StandardError; end

  class << self

    # hack to disable pushwoosh everywhere except production and only when it's configured
    def enabled?
      ((::Rails.env.production? || ENV['ENABLE_PUSHWOOSH']) && ENV['PUSHWOOSH_API_KEY']).to_bool
    end

    def create_message(title:, msg:, link:, user_ids:, sound_name: SOUND_BELLS_MONO)
      ::Rails.logger.debug("Inside of External::Pushwoosh.create_message: #{{ title: title, msg: msg, link: link, user_ids: user_ids }.inspect}")

      return false unless enabled?

      sound_name_base = File.basename(sound_name, '.*')
      response = http_client.post(CREATE_MESSAGE_URI, json: {
        request: {
          application: ENV['PUSHWOOSH_PROJECT_ID'],
          auth: ENV['PUSHWOOSH_API_KEY'],
          notifications: [
            {
              # Content settings
              send_date: "now", # YYYY-MM-DD HH:mm OR 'now'
              # ignore_user_timezone: true, # or false
              # timezone: "America/New_York", # optional. If ignored UTC-0 is default for "send_date". See http://php.net/manual/timezones.php for supported timezones.
              # campaign: "CAMPAIGN_CODE", # optional. Campaign code to which you want to assign this push message.
              content: msg, # object( language1: 'content1', language2: 'content2' ) OR string. Ignored for Windows 8, use "wns_content" instead. (Use \n for multiline text. Ex: "hello\nfriend")
              ios_title: title,
              android_header: title,
              # TODO - try these to make push notifications stand out?
              # android_priority: 2,
              # android_delivery_priority: 'high',
              # android_silent: 0,
              # page_id: 39, # optional, integer. HTML Page ID.
              # rich_media: "XXXXX-XXXXX", # optional, string. Copy the Rich Media code from the URL bar of the Rich Media editor page in Pushwoosh Control Panel.
              # remote_page: "http://myremoteurl.com", # optional, string. Remote Rich HTML Page URL. <scheme>://<authority>
              link: link, # "http://google.com", # optional, string. For deeplinks add "minimize_link":0
              minimize_link: 0, # optional. False or 0 — do not minimize, 1 — Google, 2 — bitly. Default = 1. Google URL shortener is deprecated since March 30, 2018, and will be disabled on March 30, 2019. Please note that shorteners have restrictions on a number of calls.
              # data: {
              #  key: "value", # JSON string or JSON object, will be passed as "u" parameter in the payload (converted to JSON string).
              # },
              # transactionId: "6e22a9af-84e4-46e6-af16-e457a4a6e7e5", # optional, string. Unique message identifier to prevent re-sending in case of network problems. Stored on the side of Pushwoosh for 1 day.
              # platforms: [1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 13], # 1 — iOS; 2 — BB; 3 — Android; 5 — Windows Phone; 7 — OS X; 8 — Windows 8; 9 — Amazon; 10 — Safari; 11 — Chrome; 12 — Firefox; 13 - IE11; ignored if "devices" < 10
              # preset: "Q1A2Z-6X8SW", # optional. Push Preset Code from your Control Panel.
              # send_rate: 100, # throttling. Valid values are from 100 to 1000 pushes/second.

              # To save the message to the Inbox via API, use "inbox_date" or "inbox_image". The message is saved when at least one of these parameters is used.
              # inbox_date: "2017-02-02", # optional. Specify when to remove a message from the Inbox. If not specified, the default removal date is the next day after the send date. Ignored if the "users" parameter is specified.
              # inbox_image: "Inbox image URL", # optional. The image to be shown near the message.
              # devices: [ # optional. Specify tokens or hwids to send targeted push notifications. Not more than 1000 tokens/hwids in an array. If set, the message will only be sent to the devices on the list. Application Group for devices list is not allowed. iOS push tokens can only be lower case.
              #  "dec301908b9ba8df85e57a58e40f96f523f4c2068674f5fe2ba25cdc250a2a41",
              # ],

              # user-centric push notifications
              # optional. If set, message will only be delivered to the specified user ID's (set via /registerUser call). If specified together with devices parameter, the latter will be ignored. Not more than 1000 user ID's in an array.
              users: user_ids,

              # filters and conditions
              # filter: "FILTER_NAME", # optional
              # dynamic_content_placeholders: { # optional. Placeholders for dynamic content instead of device tags.
              #  firstname: "John",
              #  lastname: "Doe",
              # },
              # "conditions": [TAG_CONDITION1, TAG_CONDITION2, ..., TAG_CONDITIONN], # optional. See the remark below.
              # metadata: { mapper: { "9999001": { custom_id: "40503658-b2be-42ac-be05-83e9209a1907" } } }, # optional, JSON-object.
              #
              #
              #
              # **********************************************************************
              # Adding Custom Sounds To Mobile App
              # IOS - Drag file to Project resources
              # Android - Copy file to android/app/src/main/res/raw. File must be initialized
              # in the project or push will be silent. See preparePushMedia() in MainActivity
              # Remove extension for android sounds (from API call, not the actual file)
              ios_sound: sound_name,
              android_sound: sound_name_base,
              # https://help.pushwoosh.com/hc/en-us/articles/360020246811-Custom-notification-sound-for-Android-8-devices
              # Once pw channel is used. The sound is tied to it on that device...weirdly.
              android_root_params: { 'pw_channel' => "#{sound_name_base}_chan" },
              # **********************************************************************
            },
          ],
        },
      })
      unless ok?(response)
        Rollbar.error "Pushwoosh request failed", response
        raise Error, "Pushwoosh request failed"
      end
      response
    end

  end

end
