# frozen_string_literal: true

module External::Looker

  def self.created_signed_embed_url(options)
    # looker options
    secret = ENV['LOOKER_EMBED_SECRET'] # options[:secret]
    host = options[:host]

    # user options
    json_external_user_id   = options[:external_user_id].to_json
    json_first_name         = options[:first_name].to_json
    json_last_name          = options[:last_name].to_json
    json_permissions        = options[:permissions].to_json
    json_models             = options[:models].to_json
    json_group_ids          = options[:group_ids].to_json
    json_external_group_id  = options[:external_group_id].to_json
    json_user_attributes    = options[:user_attributes].to_json
    json_access_filters     = options[:access_filters].to_json

    # url/session specific options
    embed_path              = '/login/embed/' + CGI.escape(options[:embed_url])
    json_session_length     = options[:session_length].to_json
    json_force_logout_login = options[:force_logout_login].to_json

    # computed options
    json_time               = Time.now.to_i.to_json
    json_nonce              = SecureRandom.hex(16).to_json

    # compute signature
    string_to_sign  = ""
    string_to_sign += host                  + "\n"
    string_to_sign += embed_path            + "\n"
    string_to_sign += json_nonce            + "\n"
    string_to_sign += json_time             + "\n"
    string_to_sign += json_session_length   + "\n"
    string_to_sign += json_external_user_id + "\n"
    string_to_sign += json_permissions      + "\n"
    string_to_sign += json_models           + "\n"

    # optionally add settings not supported in older Looker versions
    string_to_sign += json_group_ids        + "\n" if options[:group_ids]
    string_to_sign += json_external_group_id + "\n" if options[:external_group_id]
    string_to_sign += json_user_attributes + "\n" if options[:user_attributes]

    string_to_sign += json_access_filters

    signature = Base64.encode64(
      OpenSSL::HMAC.digest(
        OpenSSL::Digest.new('sha1'),
        secret,
        string_to_sign.force_encoding("utf-8")
      )
    ).strip

    # construct query string
    query_params = {
      nonce: json_nonce,
      time: json_time,
      session_length: json_session_length,
      external_user_id: json_external_user_id,
      permissions: json_permissions,
      models: json_models,
      access_filters: json_access_filters,
      first_name: json_first_name,
      last_name: json_last_name,
      force_logout_login: json_force_logout_login,
      signature: signature,
    }
    # add optional parts as appropriate
    query_params[:group_ids] = json_group_ids if options[:group_ids]
    query_params[:external_group_id] = json_external_group_id if options[:external_group_id]
    query_params[:user_attributes] = json_user_attributes if options[:user_attributes]
    query_params[:user_timezone] = options[:user_timezone].to_json if options.key?(:user_timezone)

    query_string = URI.encode_www_form(query_params)

    "#{host}#{embed_path}?#{query_string}"
  end

end

# fifteen_minutes = 15 * 60
#
# url_data = {
#     host:               'swoopme.looker.com',
#     external_user_id:   '57',
#     first_name:         'Embed Paul',
#     last_name:          'Widden',
#     permissions:        [],
#     models:             [],
#     group_ids:          [5, 2],
#     external_group_id:  'awesome_engineers',
#     user_attributes:    {},
#     access_filters:     {},
#     session_length:     fifteen_minutes,
#     embed_url:          "/embed/dashboards/3",
#     force_logout_login: true
# }
#
# url = External::Looker::created_signed_embed_url(url_data)
# puts "https://#{url}"
#
#
#
# sdk = LookerSDK::Client.new(
#   :client_id => ENV['LOOKER_API_CLIENTID'],
#   :client_secret => ENV['LOOKER_API_SECRET'],
#   :api_endpoint => "https://swoopme.looker.com:19999/api/3.0"
# )
#
