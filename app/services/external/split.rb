# frozen_string_literal: true

#
# This class is a simple wrapper around: https://github.com/splitio/ruby-client
#
module External
  class Split

    #
    # Find the treatment we're using for this entity
    #
    # entity:     Object you are tracking e.g. user, job
    # split_name: Name of the split defined in split.io
    #
    # External::Split.treatment(job, 'job_claim_cost') -> 'on'/'off'
    #
    def self.treatment(entity, split_name, default = "off", attributes = nil)
      return default unless Rails.configuration.split_client
      Rails.logger.debug "Sending get_treatment to Split, entity:#{entity.id}, name:#{split_name}, attributes:#{attributes}"
      if attributes
        variant_name = Rails.application.config.split_client.get_treatment(entity.id.to_s, split_name, attributes)
      else
        variant_name = Rails.application.config.split_client.get_treatment(entity.id.to_s, split_name)
      end

      Experiments::Service.add_target_to_experiment(entity,
                                                    "split_#{split_name}",
                                                    variant_name)
      variant_name
    end

    #
    # Track an event on split.io
    #
    # entity_id:    Object you are tracking e.g. user, job
    # traffic_type: Traffic type of this object as defined in split.io e.g. user,job
    # event_type:   Type of event we are tracking, links to the split.io metric -> event_type e.g. 'cost_estimate'
    # value:        Value of this metric e.g 100 (dollars)
    #
    # External::Split.track(job, 'job', 'cost_estimate', 100)
    #
    def self.track(entity, traffic_type, event_type, value)
      return unless Rails.configuration.split_client
      Rails.logger.debug "Sending track to Split, entity:#{entity.id}, traffic_type:#{traffic_type}, event_type:#{event_type}, value:#{value}"
      Rails.application.config.split_client.track(entity.id.to_s,
                                                  traffic_type,
                                                  event_type,
                                                  value)
    end

  end
end
