# frozen_string_literal: true

# Cleanup old web socket subscriptions from redis.
module External::Redis
  class Cleanup

    KEY_PREFIXES = ['RescueCompany'].freeze

    def initialize(name)
      Rails.logger.info "REDIS::Cleanup initialising"
      @name = name
    end

    def call
      count = 0
      total_count = 0
      deleted_count = 0

      redis = RedisClient.client(@name)
      redis.scan_each do |key|
        check_key = nil

        KEY_PREFIXES.each do |job_name|
          if key.start_with?(job_name) && redis.type(key) == "zset"
            check_key = key
            break
          end
        end

        if check_key
          _val, score = redis.zrevrange(check_key, 0, 1, with_scores: true).first
          newest = Time.at(score / 1000.0) if score
          if newest.nil? || newest < 1.day.ago
            Rails.logger.info("REDIS::Cleanup Deleting #{check_key} (last timestamp #{newest ? newest.iso8601 : 'none'})")
            redis.del(check_key)
            deleted_count += 1
          end
        end
        count += 1
        total_count += 1
      end

      Rails.logger.info("REDIS::Cleanup Deleted #{deleted_count}/#{total_count}")
    end

  end
end
