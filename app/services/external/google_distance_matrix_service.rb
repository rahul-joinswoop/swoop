# frozen_string_literal: true

# GOOGLE DISTANCE MATRIX SERVICE
#
# Connect to the Google Distance Matrix API, and calculate the distances
# (as meters and seconds) for all combinations of origins and
# destinations

# Poor man's autoloading; This class is called inside threads and if it hasn't been loaded yet,
# ActiveSupport's autoloading will freak out inside the threads.
Swoop::GoogleMapsHelper

class External::GoogleDistanceMatrixService < Service

  extend HTTPClient
  extend LocationsHelper

  EMPTY_RESPONSE = [[nil, nil]].freeze

  # Calculate travel time & distance as the matrix of origins x destinations
  # at a given time
  def self.calculate(origins, destinations, requested_time = nil)
    # Step 1: Validate that the origins and destinations provided are valid, and
    # raise exceptions or return an empty response if they aren't; if the origins
    # or destinations is not an array, turn them into single-element arrays for ease
    array_of_origins = Array(origins)
    array_of_destinations = Array(destinations)
    raise ArgumentError, "Origins are empty" if array_of_origins.empty?
    raise ArgumentError, "Destinations are empty" if array_of_destinations.empty?
    begin
      do_all_locations_have_coords?(array_of_origins + array_of_destinations)
    rescue LocationsHelper::OriginOrDestinationDoesNotHaveLatAndLong
      return EMPTY_RESPONSE
    end

    # Step 2: Build the request for the Google Distance Matrix API
    params = {
      origins: array_of_origins.map(&:latlng).join('|'),
      destinations: array_of_destinations.map(&:latlng).join('|'),
      departure_time: ensure_requested_time_is_now_or_in_the_future(requested_time),
    }

    # Step 3: Contact the Google Distance Matrix API and parse the results
    url = Swoop::GoogleMapsHelper.query_url(Swoop::GoogleMapsHelper::DISTANCE_MATRIX, params)
    start_time = Time.now
    response = http_client.get(url)

    if ok?(response)
      parsed_response = MultiJson.load(response.to_s)
      meters_and_seconds_for_all_distance_combinations_parsed_from_google = extract_meters_and_seconds_from_response(parsed_response)
      return meters_and_seconds_for_all_distance_combinations_parsed_from_google
    else
      Rails.logger.warn "External::GoogleDistanceMatrixService encountered an error after #{(Time.now - start_time).round(3)}s: #{response.inspect}"
      return EMPTY_RESPONSE
    end
  end

  # Calculate multiple distances at once. This method takes a variable number of arguments as a hash hashes like
  # { origin:, destination:, start_at: } that are passed to the calculate method in separate threads.
  # The results are returned in an array in the same order. In order to prevent *bad things* from happening
  # if many args are passed, no more than 16 thread will be run at once.
  #
  # The Google Maps Distance Matrix Service does have support for passing in multiple origins and destinations
  # which should be used when possible. However, the return results are a complete matrix with
  # (origins x destinations) elements. If you don't need all the results in the matrix or if only some
  # of them need the start at time parameter (which triggers real time traffic estimates), this method is
  # preferable because the API is billed by number of elements returned, not by number of HTTP requests and
  # traffic estimates are billed at a higher rate.
  def self.multi(*args)
    # Use a thread pool to coordinate making the requests
    thread_pool = SimpleThreadPool.new(16)
    error = nil
    results = []
    args.flatten.each_with_index do |options, index|
      # If any of the threads have set the error then re-raise it in the outer most thread.
      raise error if error

      thread_pool.execute do
        options = options.with_indifferent_access
        matrix = calculate(options[:origin], options[:destination], options[:start_at])
        # Add the results to a datastructure that will be returned by the parent thread.
        # The index is used to insure the order of the responses matches the order of input.
        thread_pool.synchronize do
          results << [index, matrix]
        end
      rescue => e
        # When an exception occurs inside a thread we assign it to a variable that
        # is being monitored by the parent thread. If multi threads raise an exception
        # we'll only get the error for the last one bubbled up.
        error = e
      end
    end

    # Wait for all threads to finish
    thread_pool.finish
    raise error if error

    # Return results in same order as the arguments
    results.sort_by(&:first).map(&:last)
  end

  def self.ensure_requested_time_is_now_or_in_the_future(requested_time)
    requested_time && requested_time > Time.now ? requested_time.to_i : "now"
  end
  private_class_method :ensure_requested_time_is_now_or_in_the_future

  def self.extract_meters_and_seconds_from_response(response_from_google)
    # Step 1: If the Distance Matrix API returned an error message, return the EMPTY_RESPONSE
    if response_from_google.key? "error_message"
      Rails.logger.warn "External::GoogleDistanceMatrixService received an error message from Google: #{response_from_google["error_message"]}"
      return EMPTY_RESPONSE
    end

    # Step 2: If any of the calculations from the Distance Matrix API yielded zero results
    # or were not found, return the EMPTY_RESPONSE
    response_from_google["rows"].each do |elementses|
      elementses["elements"].each do |element|
        if (element["status"] == "ZERO_RESULTS") || (element["status"] == "NOT_FOUND")
          Rails.logger.warn "External::GoogleDistanceMatrixService returned ZERO_RESULTS or NOT_FOUND for one or more Google Distance Matrix API results"
          return EMPTY_RESPONSE
        end
      end
    end

    # Step 3: The results from Distance Matrix API are valid, so let's parse them and
    # return them
    response_from_google["rows"].flat_map do |elementses|
      elementses["elements"].map do |element|
        meters = element["distance"]["value"]
        seconds = element.key?("duration_in_traffic") ? element["duration_in_traffic"]["value"] : element["duration"]["value"]
        [meters, seconds]
      end
    end
  end
  private_class_method :extract_meters_and_seconds_from_response

end
