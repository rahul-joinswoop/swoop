# frozen_string_literal: true

# GOOGLE Directions SERVICE
#
# Connect to the Google Directions API and get directions.

# Poor man's autoloading; This class is called inside threads and if it hasn't been loaded yet,
# ActiveSupport's autoloading will freak out inside the threads.
Swoop::GoogleMapsHelper

class External::GoogleDirectionsService < Service

  extend HTTPClient

  EMPTY_RESPONSE = {}.freeze

  class InvalidLocationError < StandardError; end
  class << self

    def get(origin, destination, waypoints = [])
      # Step 1: Validate that the origin and destination provided are valid, and
      # raise exceptions or return an empty response if they aren't
      raise ArgumentError, "Origin is required " if origin.blank?
      raise ArgumentError, "Destination is required" if destination.blank?

      # Step 2: Build the request for the Google Directions API
      begin
        params = {
          origin: parse_location(origin),
          destination: parse_location(destination),
          waypoints: parse_location(waypoints),
        }
      rescue InvalidLocationError => e
        Rails.logger.error e
        return EMPTY_RESPONSE
      end

      # Step 3: Contact the Google Directions Matrix API and parse the results
      url = Swoop::GoogleMapsHelper.query_url(Swoop::GoogleMapsHelper::DIRECTIONS, params)
      Rails.logger.debug "External::GoogleDirectionsService contacting Google using params #{params.inspect} at url #{url}"
      response = http_client.get(url)

      if ok?(response)
        parsed_response = MultiJson.load(response.to_s)
        Rails.logger.debug "External::GoogleDirectionsService successfully replied with #{parsed_response.inspect}"
        parsed_response
      else
        Rails.logger.warn "External::GoogleDirectionsService encountered an error: #{response.inspect}"
        return EMPTY_RESPONSE
      end
    end

    def parse_location(location)
      if location.is_a?(Array)
        location.map { |l| parse_location l }.join('|')
      elsif location.is_a?(String)
        location
      elsif location.respond_to?(:lat) && location.respond_to?(:lng)
        [location.lat, location.lng].join(',')
      elsif location.respond_to?(:dig) && location.dig(:lat) && location.dig(:lng)
        [location.dig(:lat), location.dig(:lng)].join(',')
      else
        raise InvalidLocationError, "Got invalid location: #{location}"
      end
    end

  end

end
