# frozen_string_literal: true

# WAYPOINT DISTANCE MATRIX SERVICE
#
# Calculate the distance and time for a journey which includes
# multiple waypoints by calling the GoogleDistanceMatrixService
# in a cost-efficient way, and return the results as a Hash
# or an array of RoundTripPossibility objects

# Representation of a round trip that may include multiple waypoints
class External::RoundTripPossibility

  attr_accessor :a,
                :b

  def has_all_results?
    has_all_meters_results? && has_all_seconds_results?
  end

  def doesnt_have_all_results?
    !has_all_results?
  end

  def has_all_meters_results?
    raise UncallableAbstractParentMethodError
  end

  def has_all_seconds_results?
    raise UncallableAbstractParentMethodError
  end

  def total_distance_in_meters?
    raise UncallableAbstractParentMethodError
  end

  def total_time_in_seconds?
    raise UncallableAbstractParentMethodError
  end

end

# Representation of a round trip from A -> B
class External::AToBRoundTripPossibility < External::RoundTripPossibility

  attr_accessor :a_to_b_in_meters,
                :a_to_b_in_seconds

  def has_all_meters_results?
    !!a_to_b_in_meters
  end

  def has_all_seconds_results?
    !!a_to_b_in_seconds
  end

  def total_distance_in_meters?
    a_to_b_in_meters
  end

  def total_time_in_seconds?
    a_to_b_in_seconds
  end

end

# Representation of a round trip from A -> B -> A
class External::AToBToARoundTripPossibility < External::RoundTripPossibility

  attr_accessor :a_to_b_in_meters,
                :b_to_a_in_meters,
                :a_to_b_in_seconds,
                :b_to_a_in_seconds

  def has_all_meters_results?
    a_to_b_in_meters && b_to_a_in_meters
  end

  def has_all_seconds_results?
    a_to_b_in_seconds && b_to_a_in_seconds
  end

  def total_distance_in_meters?
    a_to_b_in_meters + b_to_a_in_meters
  end

  def total_time_in_seconds?
    a_to_b_in_seconds + b_to_a_in_seconds
  end

end

# Representation of a round trip from A -> B -> C -> A
class External::AToBToCToARoundTripPossibility < External::RoundTripPossibility

  attr_accessor :c,
                :a_to_b_in_meters,
                :b_to_c_in_meters,
                :c_to_a_in_meters,
                :a_to_b_in_seconds,
                :b_to_c_in_seconds,
                :c_to_a_in_seconds

  def has_all_meters_results?
    a_to_b_in_meters && b_to_c_in_meters && c_to_a_in_meters
  end

  def has_all_seconds_results?
    a_to_b_in_seconds && b_to_c_in_seconds && c_to_a_in_seconds
  end

  def total_distance_in_meters?
    a_to_b_in_meters + b_to_c_in_meters + c_to_a_in_meters
  end

  def total_time_in_seconds?
    a_to_b_in_seconds + b_to_c_in_seconds + c_to_a_in_seconds
  end

end

# Poor man's autoloading; This class is called inside threads and if it hasn't been loaded yet,
# ActiveSupport's autoloading will freak out inside the threads.
External::GoogleDistanceMatrixService

class External::WaypointDistanceMatrixService < Service

  # Calculate the distance and time from multiple A (starting)
  # destinations to a single B (ending) destination
  def self.as_to_b(as, b, result_format = Hash)
    # Step 1: Ensure the response format is a Hash or a AToBRoundTripPossibility
    raise ArgumentError, "must be either Hash or External::AToBRoundTripPossibility" unless [Hash, External::AToBRoundTripPossibility].include? result_format

    # Step 2: Call the Google Distance Matrix API, and organize the results into a hash
    google_distance_matrix_results = { as_to_b: nil }
    google_distance_matrix_results[:as_to_b] = External::GoogleDistanceMatrixService.calculate(as, b, nil)

    # Step 3: If the caller requests a Hash, give back the results directly from
    # External::GoogleDistanceMatrixService#calculate
    return google_distance_matrix_results if result_format == Hash

    # Step 4: If the caller requests AToBRoundTripPossibility's, build and give back
    # an array of AToBRoundTripPossibility objects built from google_distance_matrix_results
    if result_format == External::AToBRoundTripPossibility
      return as.each_with_index.map do |a, index|
        rtp = External::AToBRoundTripPossibility.new
        rtp.a = a
        rtp.b = b
        rtp.a_to_b_in_meters = google_distance_matrix_results[:as_to_b][index][0]
        rtp.a_to_b_in_seconds = google_distance_matrix_results[:as_to_b][index][1]
        rtp
      end
    end
  end

  # Calculate the distance and time from multiple A (starting)
  # destinations to a single B (waypoint) destination to multiple A
  # (ending) destinations
  def self.as_to_b_to_as(as, b, result_format = Hash)
    # Step 1: Ensure the response format is a Hash or a AToBToARoundTripPossibility
    raise ArgumentError, "must be either Hash or External::AToBToARoundTripPossibility" unless [Hash, External::AToBToARoundTripPossibility].include? result_format

    # Step 2: Call the Google Distance Matrix API in threads for each leg of the journey, and
    # organize the results into a hash
    google_distance_matrix_results = { as_to_b: nil, b_to_as: nil }
    google_request_threads = []
    google_request_threads << Thread.new { [:as_to_b, External::GoogleDistanceMatrixService.calculate(as, b, nil)] }
    google_request_threads << Thread.new { [:b_to_as, External::GoogleDistanceMatrixService.calculate(b, as, nil)] }
    google_request_threads.each do |thread|
      key, value = thread.value
      google_distance_matrix_results[key] = value
    end

    # Step 3: If the caller requests a Hash, give back the results directly from
    # External::GoogleDistanceMatrixService#calculate
    return google_distance_matrix_results if result_format == Hash

    # Step 4: If the caller requests AToBToARoundTripPossibility's, build and give back
    # an array of AToBToARoundTripPossibility objects built from google_distance_matrix_results
    if result_format == External::AToBToARoundTripPossibility
      return as.each_with_index.map do |a, index|
        rtp = External::AToBToARoundTripPossibility.new
        rtp.a = a
        rtp.b = b
        rtp.a_to_b_in_meters = google_distance_matrix_results[:as_to_b][index][0]
        rtp.a_to_b_in_seconds = google_distance_matrix_results[:as_to_b][index][1]
        rtp.b_to_a_in_meters = google_distance_matrix_results[:b_to_as][index][0]
        rtp.b_to_a_in_seconds = google_distance_matrix_results[:b_to_as][index][1]
        rtp
      end
    end
  end

  # Calculate the distance and time from multiple A (starting)
  # destinations to a single B (waypoint) destination to a single C
  # (waypoint) destination to multiple A (ending) destinations
  def self.as_to_b_to_c_to_as(as, b, c, result_format = Hash)
    # Step 1: Ensure the response format is a Hash or a AToBToCToARoundTripPossibility
    raise ArgumentError, "must be either Hash or External::AToBToCToARoundTripPossibility" unless [Hash, External::AToBToCToARoundTripPossibility].include? result_format

    # Step 2: Call the Google Distance Matrix API in threads for each leg of the journey, and
    # organize the results into a hash
    google_distance_matrix_results = { as_to_b: nil, b_to_c: nil, c_to_as: nil }
    google_request_threads = []
    google_request_threads << Thread.new { [:as_to_b, External::GoogleDistanceMatrixService.calculate(as, b, nil)] }
    google_request_threads << Thread.new { [:b_to_c, External::GoogleDistanceMatrixService.calculate(b, c, nil)] }
    google_request_threads << Thread.new { [:c_to_as, External::GoogleDistanceMatrixService.calculate(c, as, nil)] }
    google_request_threads.each do |thread|
      key, value = thread.value
      google_distance_matrix_results[key] = value
    end

    # Step 3: If the caller requests a Hash, give back the results directly from
    # External::GoogleDistanceMatrixService#calculate
    return google_distance_matrix_results if result_format == Hash

    # Step 4: If the caller requests AToBToCToARoundTripPossibility's, build and give back
    # an array of AToBToCToARoundTripPossibility objects built from google_distance_matrix_results
    if result_format == External::AToBToCToARoundTripPossibility
      return as.each_with_index.map do |a, index|
        rtp = External::AToBToCToARoundTripPossibility.new
        rtp.a = a
        rtp.b = b
        rtp.c = c
        rtp.a_to_b_in_meters = google_distance_matrix_results[:as_to_b][index][0]
        rtp.a_to_b_in_seconds = google_distance_matrix_results[:as_to_b][index][1]
        rtp.b_to_c_in_meters = google_distance_matrix_results[:b_to_c].first[0]
        rtp.b_to_c_in_seconds = google_distance_matrix_results[:b_to_c].first[1]
        rtp.c_to_a_in_meters = google_distance_matrix_results[:c_to_as][index][0]
        rtp.c_to_a_in_seconds = google_distance_matrix_results[:c_to_as][index][1]
        rtp
      end
    end
  end

end
