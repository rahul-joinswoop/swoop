# frozen_string_literal: true

class AddClientCompanyToPartner < Service

  def initialize(rescue_company, client_company, site)
    @rescue_company = rescue_company
    @client_company = client_company
    @site = site

    raise ArgumentError, "rescue company, client company or site nil" unless @rescue_company && @client_company && @site
    if RescueProvider.where(provider: @rescue_company, company: @client_company, deleted_at: nil, site: @site).length > 0
      raise ArgumentError, "'#{@rescue_company.name}' already provides service to '#{@client_company.name}' for site '#{@site.name}'"
    end
    Rails.logger.debug "#{@site.inspect}"
  end

  # Add accounts if necessary
  #
  def call
    RescueProvider.new(company: @client_company, provider: @rescue_company, site: @site)
  end

end
