# frozen_string_literal: true

class InvoiceAutoApprover::CheckJobStatus

  include Interactor

  VALID_STATES = [Job::STATUS_COMPLETED, Job::STATUS_RELEASED, Job::STATUS_GOA].freeze
  FAILURE_MESSAGE = "Not auto-approving because job.status isn't in #{VALID_STATES.map { |s| Job.statuses[s] }}"

  def call
    unless context.invoice.job.status.in? VALID_STATES
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    end
  end

end
