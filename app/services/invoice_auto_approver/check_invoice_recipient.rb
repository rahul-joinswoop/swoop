# frozen_string_literal: true

class InvoiceAutoApprover::CheckInvoiceRecipient

  include Interactor

  FAILURE_MESSAGE = "Not auto-approving because recipient_company isn't Swoop"

  def call
    unless context.invoice.recipient_company&.is_swoop?
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    end
  end

end
