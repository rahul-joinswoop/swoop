# frozen_string_literal: true

class InvoiceAutoApprover::CheckInvoiceBillingType

  include Interactor

  VALID_BILLING_TYPES = [BillingType::ACH].freeze
  FAILURE_MESSAGE = "Not auto-approving because billing_type isn't in #{VALID_BILLING_TYPES}"

  def call
    unless context.invoice.billing_type.in? VALID_BILLING_TYPES
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    end
  end

end
