# frozen_string_literal: true

class InvoiceAutoApprover::CheckForHistoryItems

  include Interactor

  FAILURE_MESSAGE = "Not auto-approving because invoice has history items"

  def call
    if context.invoice.has_history_items? || context.invoice.invoicing_ledger_items.any?(&:has_history_items?)
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    end
  end

end
