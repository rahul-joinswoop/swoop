# frozen_string_literal: true

class InvoiceAutoApprover::CheckInvoiceSentDate

  include Interactor

  FAILURE_MESSAGE = "Not auto-approving because invoice sent date is > 90 days after job completed date"

  def call
    if context.invoice.sent_at >= context.invoice.job.last_status_date_time + 90.days
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    end
  end

end
