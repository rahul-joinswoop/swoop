# frozen_string_literal: true

class InvoiceAutoApprover::ApproveInvoice

  include Interactor

  def call
    InvoiceService.transition_for(:swoop_approved_partner).new(
      invoice_id: context.invoice.id,
      api_company_id: Company.swoop_id
    ).call.save_mutated_objects
  rescue => e
    Rails.logger.debug "INVOICE(#{context.invoice.id}): #{e.message}"
    context.fail! message: e.message
  end

end
