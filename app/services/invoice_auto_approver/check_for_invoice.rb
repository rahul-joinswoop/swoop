# frozen_string_literal: true

class InvoiceAutoApprover::CheckForInvoice

  include Interactor
  FAILURE_MESSAGE = 'invoice is required'

  def call
    if context.invoice.blank?
      Rails.logger.error "#{self.class.name}: #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    else
      Rails.logger.debug "INVOICE(#{context.invoice.id}): starting invoice auto-approver"
    end
  end

end
