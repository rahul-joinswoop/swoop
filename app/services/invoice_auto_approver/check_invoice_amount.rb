# frozen_string_literal: true

class InvoiceAutoApprover::CheckInvoiceAmount

  include Interactor

  ZERO_FAILURE_MESSAGE = "Not auto-approving because invoice total amount is $0.00"
  TOO_HIGH_FAILURE_MESSAGE =  "Not auto-approving because because invoice amount is > $%s"

  def call
    if !context.invoice.total_amount || context.invoice.total_amount == 0.0
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{ZERO_FAILURE_MESSAGE}"
      context.fail! message: ZERO_FAILURE_MESSAGE
    end

    max_amount = context.invoice.auto_approval_limit
    if context.invoice.total_amount > max_amount
      message = TOO_HIGH_FAILURE_MESSAGE % [max_amount]
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{message}"
      context.fail! message: message
    end
  end

end
