# frozen_string_literal: true

class InvoiceAutoApprover::CheckInvoiceSender

  include Interactor

  FAILURE_MESSAGE = "Not auto-approving because sender isn't a RescueCompamny"

  def call
    unless context.invoice.sender&.rescue?
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    end
  end

end
