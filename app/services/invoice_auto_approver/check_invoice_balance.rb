# frozen_string_literal: true

class InvoiceAutoApprover::CheckInvoiceBalance

  include Interactor

  FAILURE_MESSAGE = "Not auto-approving because invoice balance is $0.00"

  def call
    balance = context.invoice.balance
    if !balance || balance == 0.0
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    end
  end

end
