# frozen_string_literal: true

class InvoiceAutoApprover::CheckInvoiceState

  include Interactor

  VALID_STATES = [Invoice::PARTNER_SENT].freeze
  FAILURE_MESSAGE = "Not auto-approving because invoice.state isn't in #{VALID_STATES}"

  def call
    if !context.invoice.state.in?(VALID_STATES)
      Rails.logger.debug "INVOICE(#{context.invoice.id}): #{FAILURE_MESSAGE}"
      context.fail! message: FAILURE_MESSAGE
    end
  end

end
