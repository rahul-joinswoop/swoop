# frozen_string_literal: true

class IsscDispatchTranscript < IsscDispatchNew

  def getClubJobType()
    "Phone Transcript"
  end

  def call
    Job.transaction do
      job = FleetMotorClubJob.joins(:issc_dispatch_request).where({
        issc_dispatch_requests: { dispatchid: @payload[:issc_dispatch_request_attributes][:dispatchid] },
      }).first

      if job.nil?
        job = super
      else
        job.update(@payload)
      end

      case @event.dig(:Transcript, 'Disposition')

      when 'Rejected'
        job.partner_rejected
      else
        # 'Accepted'
        job.partner_accepted
        IsscDispatchAccepted.accept_job(@event, job)
      end

      job
    end
  end

end
