# frozen_string_literal: true

class IsscDeregister < Service

  def initialize(issc)
    @issc = issc
    Rails.logger.debug("ISSCLOG(#{@issc.contractorid}) IsscDeregister init called, issc status:#{@issc.status}")
  end

  def call
    if @issc.status == "logged_in"
      Rails.logger.debug("ISSCLOG(#{@issc.contractorid}) IsscDeregister Log Out")
      @issc.provider_logout
      @issc.save!
      # TODO: These could all be grouped into one call
      @issc.logout
    elsif (@issc.status == "registered") || (@issc.status == "logging_in")
      Rails.logger.debug("ISSCLOG(#{@issc.contractorid}) IsscDeregister Un Register")
      @issc.provider_deregister
      @issc.save!
      # TODO: These could all be grouped into one call
      @issc.deregister
    elsif @issc.status == "registering"
      Rails.logger.debug("ISSCLOG(#{@issc.contractorid}) IsscDeregister Failed Registration")
      @issc.provider_register_failed
      @issc.deleted_at = Time.now
      @issc.save!
    else
      Rails.logger.warn("ISSCLOG(#{@issc.contractorid}) IsscDeregister unknown #{@issc.status}")
    end
  end

end
