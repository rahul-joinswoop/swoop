# frozen_string_literal: true

module QuestionServices
  # Given a Job and the text of a question, this service finds the specific
  # answer a customer provided.
  class FindAnswer

    include Interactor

    before do
      @job = context.job
      @question = context.question
    end

    def call
      context.result = fetch_answer
    end

    private

    def fetch_answer
      QuestionResult.joins(:question)
        .where(job: @job)
        .merge(Question.where(question: @question))
        .first
        &.answer
    end

  end
end
