# frozen_string_literal: true

module TireTypeServices
  class Batch

    def initialize(tire_type_ids:)
      @tire_type_ids = tire_type_ids.split(',')

      Rails.logger.debug "TireType batch load param: tire_type_ids = #{@tire_type_ids}"
    end

    def call
      TireType.where(id: @tire_type_ids)
    end

  end
end
