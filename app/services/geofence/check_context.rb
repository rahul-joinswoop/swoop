# frozen_string_literal: true

module Geofence
  module CheckContext

    extend ActiveSupport::Concern

    included do
      before do
        context.fail!(error: "#{self.class} called without :jobs") unless context.jobs
        if context.rescue_vehicle.nil?
          context.fail!(error: "#{self.class} called without :rescue_vehicle")
        end

        if !context.redis_client.is_a? Redis
          raise ArgumentError, "#{context.redis_client.class} instead of RedisClient at :redis_client"
        end
      end
    end

  end
end
