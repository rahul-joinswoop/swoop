# frozen_string_literal: true

module Geofence
  module RemoveFromContext

    extend ActiveSupport::Concern

    included do
      before do
        @jobs_to_remove = []
      end

      after do
        context.jobs = context.jobs.reject { |j| @jobs_to_remove.include?(j.id) }
      end
    end

    def remove_from_context(job)
      @jobs_to_remove << job.id
    end

  end
end
