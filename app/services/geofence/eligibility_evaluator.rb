# frozen_string_literal: true

module Geofence
  class EligibilityEvaluator

    include Interactor::Organizer

    MAX_ACTIVE_JOBS = 50 # Just in case

    before do
      context.fail!(error: 'Vehicle is required') if context.rescue_vehicle.nil?
      context.fail!(error: 'Jobs is required') if context.jobs.nil?
      context.redis_client ||= RedisClient[:default]

      if context.jobs.size > MAX_ACTIVE_JOBS
        context.fail!(error: 'Aborted due to active jobs count')
      end

      context.jobs = context.jobs.uniq.sort_by(&:updated_at).reverse
    end

    organize [
      OnsiteEligibilityEvaluator,
      TowDestinationEligibilityEvaluator,
      TowingEligibilityEvaluator,
      CompletedServiceEligibilityEvaluator,
      CompletedDropoffEligibilityEvaluator,
    ]

  end
end
