# frozen_string_literal: true

module Geofence
  class TowDestinationEligibilityEvaluator

    include Interactor
    include Scheduling
    include CheckContext
    include RemoveFromContext

    TIME_THRESHOLD_SECONDS = Integer(ENV.fetch('GEOFENCE_TOW_DESTINATION_TIME_THRESHOLD_SECONDS', 180))
    # 0.2 miles
    RADIUS_IN_METERS = Integer(ENV.fetch('GEOFENCE_TOW_DESTINATION_RADIUS_IN_METERS', 322))

    ELIGIBLE_JOB_STATUSES = [
      Job::STATUS_ONSITE,
      Job::STATUS_TOWING,
    ].freeze

    DEFAULT_WORKER_KLASS = Geofence::TowDestinationWorker

    def call
      context.jobs.each do |job|
        next unless ELIGIBLE_JOB_STATUSES.include?(job.status)
        next unless job.drop_location&.lat && job.drop_location&.lng

        distance_to_dropoff = job.drop_location.distance_to(context.rescue_vehicle)

        if distance_to_dropoff < RADIUS_IN_METERS
          schedule(job)
          remove_from_context(job)
        else
          deschedule(job)
        end
      end
    rescue => e
      debug e.message
      context.fail!(error: e.message)
    end

  end
end
