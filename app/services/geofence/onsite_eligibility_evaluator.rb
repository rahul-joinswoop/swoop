# frozen_string_literal: true

module Geofence
  class OnsiteEligibilityEvaluator

    include Interactor
    include Scheduling
    include CheckContext
    include RemoveFromContext

    TIME_THRESHOLD_SECONDS = Integer(ENV.fetch('GEOFENCE_ONSITE_TIME_THRESHOLD_SECONDS', 180))
    RADIUS_IN_METERS = Integer(ENV.fetch('GEOFENCE_ONSITE_RADIUS_IN_METERS', 322)) # 0.2 miles

    ELIGIBLE_JOB_STATUSES = [
      Job::STATUS_DISPATCHED,
      Job::STATUS_ENROUTE,
    ].freeze

    # I'm expecting to move most of the logic aside from constants into a base class
    # when I write the next transition
    DEFAULT_WORKER_KLASS = Geofence::OnsiteWorker

    def call
      context.jobs.each do |job|
        next unless ELIGIBLE_JOB_STATUSES.include?(job.status)
        next unless job.service_location&.lat && job.service_location&.lng

        distance_to_job = job.service_location.distance_to(context.rescue_vehicle)

        if distance_to_job <= RADIUS_IN_METERS
          schedule(job)
          remove_from_context(job)
        else
          deschedule(job)
        end
      end
    rescue => e
      debug e.message
      context.fail!(error: e.message)
    end

  end
end
