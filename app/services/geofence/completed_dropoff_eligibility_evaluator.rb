# frozen_string_literal: true

module Geofence
  class CompletedDropoffEligibilityEvaluator

    include Interactor
    include Scheduling
    include CheckContext
    include RemoveFromContext

    TIME_THRESHOLD_SECONDS = Integer(ENV.fetch('GEOFENCE_COMPLETED_DROPOFF_TIME_THRESHOLD_SECONDS', 900))
    # 0.2 miles
    RADIUS_IN_METERS = Integer(ENV.fetch('GEOFENCE_COMPLETED_DROPOFF_RADIUS_IN_METERS', 322))

    ELIGIBLE_JOB_STATUSES = [
      Job::STATUS_TOWDESTINATION,
    ].freeze

    DEFAULT_WORKER_KLASS = Geofence::CompletedWorker

    def call
      context.jobs.each do |job|
        next unless ELIGIBLE_JOB_STATUSES.include?(job.status)
        next unless job.drop_location&.lat && job.drop_location&.lng

        distance_from_drop = job.drop_location.distance_to(context.rescue_vehicle)

        if distance_from_drop > RADIUS_IN_METERS
          schedule(job)
          remove_from_context(job)
        else
          deschedule(job)
        end
      end
    rescue => e
      debug e.message
      context.fail!(error: e.message)
    end

  end
end
