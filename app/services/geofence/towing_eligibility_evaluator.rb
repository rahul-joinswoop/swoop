# frozen_string_literal: true

module Geofence
  class TowingEligibilityEvaluator

    include Interactor
    include Scheduling
    include CheckContext
    include RemoveFromContext

    TIME_THRESHOLD_SECONDS = Integer(ENV.fetch('GEOFENCE_TOWING_TIME_THRESHOLD_SECONDS', 180))
    RADIUS_IN_METERS = Integer(ENV.fetch('GEOFENCE_TOWING_RADIUS_IN_METERS', 322)) # 0.2 miles

    ELIGIBLE_JOB_STATUSES = [
      Job::STATUS_ONSITE,
    ].freeze

    DEFAULT_WORKER_KLASS = Geofence::TowingWorker

    def call
      context.jobs.each do |job|
        next unless ELIGIBLE_JOB_STATUSES.include?(job.status)
        next unless job.service_location&.lat && job.service_location&.lng
        next unless job.drop_location

        distance_from_pickup = job.service_location.distance_to(context.rescue_vehicle)

        if distance_from_pickup > RADIUS_IN_METERS
          schedule(job)
          remove_from_context(job)
        else
          deschedule(job)
        end
      end
    rescue => e
      debug e.message
      context.fail!(error: e.message)
    end

  end
end
