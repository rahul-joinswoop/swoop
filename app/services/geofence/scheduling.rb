# frozen_string_literal: true

module Geofence
  module Scheduling

    extend ActiveSupport::Concern

    def time_threshold_seconds
      self.class.const_get(:TIME_THRESHOLD_SECONDS)
    end

    def debug(msg)
      Rails.logger.debug "#{self.class} #{msg}"
    end

    # This does NOT indicate the job is currently scheduled in Redis. This is
    # merely an optimization intended to make #schedule amortized O(1)
    # in the context of redis operations.
    def previously_scheduled?(job)
      recently_scheduled_jobs.include?(job.id.to_s)
    end

    def schedule(job)
      worker_klass.schedule(time_threshold_seconds, job.id)
      context.redis_client.lpush key, job.id
      context.redis_client.expire key, time_threshold_seconds
    end

    def deschedule(job)
      return unless previously_scheduled?(job)
      worker_klass.deschedule(time_threshold_seconds, job.id)
    end

    def recently_scheduled_jobs
      @recently_scheduled_jobs ||= context.redis_client.lrange key, 0, -1
    end

    def key
      "#{self.class}_#{context.worker_klass}_#{context.rescue_vehicle&.id}"
    end

    def worker_klass
      self.class.const_get(:DEFAULT_WORKER_KLASS)
    end

  end
end
