# frozen_string_literal: true

# This service supports RescueCompany only.
module VehicleCategoryServices
  class FetchAllStandard

    def initialize(api_company:)
      @api_company = api_company
    end

    def call
      if @api_company.instance_of? SuperCompany
        raise ArgumentError, "Company type not allowed: #{@api_company.class}"
      end

      VehicleCategory.standard_items
    end

  end
end
