# frozen_string_literal: true

module VehicleCategoryServices
  class Batch

    def initialize(api_company:, vehicle_category_ids:)
      @api_company          = api_company
      @vehicle_category_ids = vehicle_category_ids.split(',')

      Rails.logger.debug "VehicleCategory batch load params: company.id = #{@api_company.id}, vehicle_category_ids = #{@vehicle_category_ids}"
    end

    def call
      if @api_company.fleet?
        # fleets can only have standard ones as per business rule
        VehicleCategory.standard_items.where(id: @vehicle_category_ids)
      else
        VehicleCategory.where(id: @vehicle_category_ids)
      end
    end

  end
end
