# frozen_string_literal: true

module VehicleCategoryServices
  class Change

    include SwoopService

    attr_reader :vehicle_categories_added

    def initialize(api_company:, standard_vehicle_category_ids_to_add:,
                   standard_vehicle_category_ids_to_remove:, custom_vehicle_categories_names_to_add:)

      @api_company = api_company
      @standard_vehicle_category_ids_to_add = standard_vehicle_category_ids_to_add || []
      @standard_vehicle_category_ids_to_remove = standard_vehicle_category_ids_to_remove || []
      @custom_vehicle_categories_names_to_add = custom_vehicle_categories_names_to_add || []

      @vehicle_categories_added = []

      Rails.logger.debug("VehicleCategoryServices::Change params: api_company.id = #{@api_company.id}, " \
        "standard_vehicle_category_ids_to_add = #{@standard_vehicle_category_ids_to_add}, " \
        "standard_vehicle_category_ids_to_remove = #{@standard_vehicle_category_ids_to_remove}, " \
        "custom_vehicle_categories_names_to_add = #{@custom_vehicle_categories_names_to_add}.")
    end

    def execute
      if @api_company.instance_of? SuperCompany
        raise ArgumentError, "Company type not allowed: #{@api_company.class}"
      end

      add_standard_vehicle_categories

      remove_standard_vehicle_categories

      add_custom_vehicle_categories

      @vehicle_categories_added.uniq
    end

    def after_execution
      PublishCompanyWorker.perform_after_commit(@api_company.id, self.class.name)
    end

    private

    def add_standard_vehicle_categories
      standard_vehicle_categories_ids_already_added =
        @api_company.vehicle_categories.where(id: @standard_vehicle_category_ids_to_add).pluck(:id)

      (@standard_vehicle_category_ids_to_add - standard_vehicle_categories_ids_already_added).each do |vehicle_category_id|
        vehicle_category = VehicleCategory.find(vehicle_category_id)

        if vehicle_category
          @vehicle_categories_added << vehicle_category
          @api_company.vehicle_categories << vehicle_category
        end
      end
    end

    def remove_standard_vehicle_categories
      @standard_vehicle_category_ids_to_remove.each do |vehicle_category_id|
        vehicle_category = VehicleCategory.find(vehicle_category_id)

        if vehicle_category
          @api_company.vehicle_categories.destroy(vehicle_category)
        end
      end
    end

    def add_custom_vehicle_categories
      return if !@api_company.rescue? # only rescue companies can have custom ones

      @custom_vehicle_categories_names_to_add.each do |vehicle_category_name|
        name = vehicle_category_name[:name]

        custom_vehicle_category = VehicleCategory.where('lower(name) = ?', name.downcase).first

        if !custom_vehicle_category
          custom_vehicle_category = VehicleCategory.create!(name: name)
        end

        if !@api_company.vehicle_categories.where('lower(name) = ?', name.downcase).first
          @vehicle_categories_added << custom_vehicle_category
          @api_company.vehicle_categories << custom_vehicle_category
        end
      end
    end

  end
end
