# frozen_string_literal: true

module InventoryItemService
  module Tire
    class QuantityBySiteAndType < Service

      def initialize(api_company:)
        @api_company = api_company
      end

      def call
        InventoryItem.where(site_id: sites_ids, deleted_at: nil)
          .joins(tire: [:tire_type]).where(tire_types: { deleted_at: nil })
          .select(
            'inventory_items.site_id as site_id',
            'tire_types.id as tire_type_id',
            'count(tire_types.id) as tire_quantity'
          )
          .group('site_id, tire_types.id')
      end

      def sites_ids
        if @api_company.instance_of? RescueCompany
          PartnerSite.where(
            deleted_at: nil,
            company_id: @api_company.id,
            tire_program: true
          ).pluck(:id)
        else # is Tesla
          RescueProvider.where(company_id: @api_company.id, deleted_at: nil).pluck(:site_id)
        end
      end

    end
  end
end
