# frozen_string_literal: true

module InventoryItemService
  module Tire
    class DecreaseQuantity < Service

      def initialize(site_id:, tire_type_id:, api_company:)
        @site_id = site_id
        @tire_type_id = tire_type_id
        @api_company = api_company
      end

      def call
        tires = InventoryItem.where(
          site_id: @site_id,
          deleted_at: nil
        )
          .joins(:tire).where(tires: {
            tire_type_id: @tire_type_id,
          })

        if @api_company.instance_of? RescueCompany
          tires.where(company_id: @api_company)
        end

        if tires.present?
          tire = tires.last

          tire.deleted_at = Time.now
          tire.save!

          tire
        end
      end

    end
  end
end
