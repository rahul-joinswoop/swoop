# frozen_string_literal: true

module InventoryItemService
  class Update

    include Interactor

    before do
      @inventory_item = context.inventory_item
      @params = context.params
      @event = context.event
    end

    def call
      @inventory_item.assign_attributes(@params)

      update_job!

      if @inventory_item.save
        Rails.logger.debug "SM: Calling inventory_item.update returned"

        context.result = @inventory_item

        return if @event.blank?

        @event.target = @inventory_item
        @event.save!
      else
        raise ArgumentError, @inventory_item.errors.to_json
      end
    end

    private

    def update_job!
      return if job.nil? || !job.stored?
      return unless (@inventory_item.item_type == "Vehicle") && @inventory_item.deleted_at?

      JobService::RemoveStorage.call(job: job)

      job.save!
    end

    def job
      @inventory_item.job
    end

  end
end
