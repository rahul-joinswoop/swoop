# frozen_string_literal: true

module Partner
  class VehicleService

    include SwoopService

    def initialize(api_user:, vehicle:, vehicle_params:)
      @api_user = api_user
      @vehicle = vehicle
      @vehicle_params = vehicle_params
    end

    def execute
      @vehicle_params[:location_updated_at] = DateTime.now

      if @vehicle_params.key?(:driver_id)
        driver_id = @vehicle_params[:driver_id]

        if driver_id.present?
          @vehicle.driver = User.find(driver_id)
        elsif (driver_id || driver_id.nil?) && @vehicle.driver&.id == @api_user.id
          @vehicle.driver = nil
        end

        @vehicle_params.delete(:driver_id)
      end

      self
    end

  end
end
