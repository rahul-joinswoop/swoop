# frozen_string_literal: true

# Swoop services have the following goals/features:
#
#   1. They provide the ability for multiple services to be combined _without_
#   loosing the ability for the composer to control overall persistence via a
#   transaction.
#
#   2. They provide the ability in both cases (single service, composed
#   services) for executers to ensure that async tasks are enqueued after the
#   _encompassing_ transaction (i.e. the one mentioned in #1) commits.
#
# To achieve these goals, a service must implement two methods, `execute` for
# business logic and `after_execution` for tasks that need to happen only when
# data is surely persisted.
#
# By implementing `execute` and `after_execution`, a service runner or composer
# can do bulk work like this:
#
#   class ServiceRunner
#     def callAll
#       ApplicationRecord.transaction do
#         services.each(&:execute)
#       end
#       service.each(&:after_execution)
#     end
#   end
#
#   runner = ServiceRunner.new
#   runner << MyNewService.new(job_one)
#   runner << MyNewService.new(job_two)
#   runner.callAll
#
module SwoopService

  # Define tasks that need or can be run inside an
  # ApplicationRecord.transaction That is, with great simplification, generally
  # tasks that update a model's attributes. Changes state.
  def execute; end

  # Define tasks that can be called async or need to be run after the `execute`
  # is definitely completed without errors. Think of emails and active jobs.
  def after_execution; end

  def call
    execute
    after_execution
    self
  end

  # Simplify calling a single service inside a transaction. This method can be
  # used when `execute` requires multiple objects to be persisted in order to
  # be successful, for example +RateService::UpdateLive+
  #
  # Services that use methods like `save` or `destroy` can use `call` as these
  # are already wrapped in a transaction: http://api.rubyonrails.org/v4.2/classes/ActiveRecord/Transactions/ClassMethods.html
  def call_with_transaction
    ApplicationRecord.transaction do
      execute
    end
    after_execution
    self
  end

end
