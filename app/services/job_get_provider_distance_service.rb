# frozen_string_literal: true

# This service will calculate the distance between a Job and a RescueProvider and
# cache that value in a transient attribute of RescueProvider#job_distance.
#
# One clear way to make this reusable:
# -don't use transient attribute because it violates SRP + polutes the layers above.
# -remove the usage of transient attributes from the views (a lot of coffee uses it)
# -turn this into a functional stlye, simply return the distance and reuse that.
#
# TODO this is not ideal since it introduces state on RescueProvider from an inferred context.
class JobGetProviderDistanceService < Service

  include LoggableJob

  MAX_DISTANCE = (ENV['CLOSEST_PROVIDERS_MAX_DISTANCE'] || 60).to_i

  class CannotGetProviderDistanceWithoutProviderError < ArgumentError; end
  class CannotGetProviderDistanceWithoutJobError < ArgumentError; end

  def initialize(job, provider)
    # Halt the execution If the inputs are not present
    # in order to make the distance calculation simpler.
    raise CannotGetProviderDistanceWithoutProviderError if provider.blank?
    raise CannotGetProviderDistanceWithoutJobError if job.blank?

    @job = job
    @provider = provider
    log :debug, "initialised", job: @job.id
  end

  def call
    # Step 0: initialise the distance with the maximum
    @provider.job_distance = RescueProvider::MAX_DISTANCE

    # Step 1: log the service and the tower location
    service_location = @job.service_location
    tower_location = @provider.best_location
    log :debug, "Best Location: #{tower_location.inspect}", job: @job.id
    log :debug, "Service Location: #{service_location.inspect}", job: @job.id

    # Step 2: calculate and set the job distance if possible
    if canCalculateDistance? service_location, tower_location
      @provider.job_distance = Geocoder::Calculations.distance_between([tower_location.lat, tower_location.lng], [service_location.lat, service_location.lng], units: :mi)
    end

    # Step 3: return the provider
    @provider
  end

  private

  def canCalculateDistance?(service_location, tower_location)
    service_location.present? &&
    service_location.lat? &&
    service_location.lng? &&
    tower_location.present? &&
    tower_location.lat &&
    tower_location.lng
  end

end
