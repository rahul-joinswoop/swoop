# frozen_string_literal: true

class JobSearchService < SearchService

  class << self

    def model_class
      Job
    end

    def power_searchable_fields
      [
        [:job,               :integer,  :id],
        [:po,                :string,   :po_number],
        [:service,           :string,   :"service_code.name"],
        [:pickup,            :string,   :"service_location.address"],
        [:dropoff,           :string,   :"drop_location.address"],
        [:customer,          :string,   :"customer.full_name"],
        [:phone,             :phone,    :"customer.index_phone"],
        [:vin,               :wildcard, :"stranded_vehicle.vin_indexed"],
        [:license,           :string,   :"stranded_vehicle.license"],
        [:make,              :string,   :"stranded_vehicle.make"],
        [:model,             :string,   :"stranded_vehicle.model"],
        [:serial_number,     :string,   :"stranded_vehicle.serial_number"],
        [:unit_number,       :string,   :unit_number],
        [:dispatched,        :date,     :dispatched_at],
        [:completed,         :date,     :completed_at],
        [:stored,            :date,     :stored_at],
        [:notes,             :string,   :notes],
        [:storage_site,      :integer,  :"storage.site_id"],
        [:storage_site_name, :integer,  :"storage.site_name"],
        [:storage_type,      :integer,  :storage_type_id],
        [:partner_notes,     :string,   :partner_notes],
        [:driver_notes,      :string,   :driver_notes],
        [:dispatch_notes,    :string,   :dispatch_notes],
        [:account,           :string,   :"account.name"],
        [:account_id,        :integer,  :account_id],
        [:fleet,             :string,   :"fleet_company.name"],
        [:fleet_site_name,   :string,   :"fleet_site.name"],
        [:ref_number,        :string,   :ref_number],
        [:policy_number,     :string,   :policy_number],
        [:claim_number,      :string,   :claim_number],
        [:partner,           :string,   :"rescue_company.name"],
        [:swoop_notes,       :string,   :swoop_notes],
        [:fleet_notes,       :string,   :fleet_notes],
        [:status,            :string,   :status],
        [:invoice_state,     :string,   :invoice_states],
        [:original_job,      :integer,  :original_job_id],
        [:department,        :string,   :"department.name"],
      ].freeze
    end

    def filterable_fields
      [
        [:status,        :status],
        [:invoice_state, :invoice_states],
        [:fleet_site_id, :"fleet_site.id"],
        [:storage_site,  :"storage.site_id"],
        [:storage_type,  :storage_type_id],
        [:account_id,    :account_id],
        [:paid_invoices?, :"paid_invoices?"],
        [:unpaid_invoices?, :"unpaid_invoices?"],
        [:filtered_storage_sites, :"storage.site_id"],
        [:filtered_storage_types, :storage_type_id],
      ].freeze
    end

    def default_sort_order
      { id: :desc }.freeze
    end

  end

  # Create Job search service instance.
  #
  # @param [Company|nil] company filter (for job search)
  # @param [String|nil] page number
  # @param [String|nil] records per page
  # @param [Boolean|false] true if invoice search
  #
  def initialize(company:, page: nil, per_page: nil, invoice_search: false)
    super(page: page, per_page: per_page)
    @company = company
    @invoice_search = invoice_search
  end

  protected

  def generate_filter_queries(opts)
    filter_queries = super

    if @invoice_search
      after = Time.zone.parse(opts[:job_after].to_s)&.beginning_of_day if opts
      before = Time.zone.parse(opts[:job_before].to_s)&.end_of_day if opts
      if after || before
        range = {}
        range[:gte] = after if after
        range[:lte] = before if before
        filter_queries << { range: { "dispatched_at": range } }
      end
      filter_queries << generate_term_query(:status, Job.invoice_states.map(&:downcase))
    else
      filter_queries << company_filter
    end

    if opts && opts[:active].present?
      # TODO Job.active_and_draft_states is a hack to keep retrocompatibility with legacy code that relied
      # on Job.active_states when it contained 'draft'.
      # This and all spots that use it should be removed by https://swoopme.atlassian.net/browse/ENG-9905
      status_filter = (ActiveModel::Type::Boolean.new.cast(opts[:active]) ? Job.active_and_draft_states : Job.done_states)
      filter_queries << generate_term_query(:status, status_filter)
    end

    filter_queries
  end

  def power_searchable_fields
    case @company
    when SuperCompany
      self.class.power_searchable_fields
    when FleetCompany
      self.class.power_searchable_fields.reject do |name, _kind, _index_name|
        [:partner_notes, :dispatch_notes, :driver_notes, :account, :fleet, :swoop_notes].include?(name)
      end
    when RescueCompany
      self.class.power_searchable_fields.reject do |name, _kind, _index_name|
        [:partner, :swoop_notes, :fleet_notes].include?(name)
      end
    else
      raise "Unknown company class: #{@company.class.name}"
    end
  end

  private

  def company_filter
    case @company
    when SuperCompany;  generate_term_query(:type, "FleetManagedJob")
    when FleetCompany;  generate_term_query(:"fleet_company.id", @company.id)
    when RescueCompany; generate_term_query(:"rescue_company.id", @company.id)
    else                raise "Unknown company class: #{@company.class.name}"
    end
  end

end
