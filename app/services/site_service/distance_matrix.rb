# frozen_string_literal: true

module SiteService
  #
  # Try to find the nearest FleetSites to the given origin using Google's Distance Matrix
  #
  # Returns: Sorted array (closest first) of hashes containing:
  #  {
  #    site: <site>,
  #    miles: <miles from origin by road if available, crow flies otherwise>
  #    meters: <meters from origin by road if available, crow flies otherwise>
  #    seconds: <time in traffic if available>
  #  }
  #
  # Always returns Miles and Meters, Only returns seconds in traffic if available.
  #
  class DistanceMatrix

    def initialize(fleet_sites, origin)
      @origin = origin
      @fleet_sites = fleet_sites
    end

    def call()
      return [] if @fleet_sites.empty?

      destinations = @fleet_sites.map(&:location)
      results = External::GoogleDistanceMatrixService.calculate(@origin, destinations, nil)

      ret = []

      if results == External::GoogleDistanceMatrixService::EMPTY_RESPONSE
        # No results from Google. Probably origin is in the desert / mountains
        @fleet_sites.each do |site|
          ret << {
            site: site,
            meters: (site.distance_in_miles * Constant::MILES_METERS).round(2),
            miles: site.distance_in_miles.round(2),
            seconds: nil,
          }
        end
      else
        @fleet_sites.zip(results).each do |site, meters_seconds|
          ret << {
            site: site,
            meters: meters_seconds[0],
            miles: (meters_seconds[0] * Constant::METERS_MILES).round(2),
            seconds: meters_seconds[1],
          }
        end

      end

      ret.sort! do |a, b|
        a[:meters] <=> b[:meters]
      end

      ret
    end

  end
end
