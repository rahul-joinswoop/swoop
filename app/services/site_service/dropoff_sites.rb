# frozen_string_literal: true

module SiteService
  class DropoffSites

    def self.list_by(drop_sites_params:, api_user:)
      job = Job.safe_find(api_user, drop_sites_params.dig(:job, :id))

      async_request = API::AsyncRequest.create!(
        company: job.fleet_company,
        user: api_user,
        request: {
          method: 'DropoffSites',
          params: {
            drop_sites_params: drop_sites_params,
          },
        }
      )

      # Kick this off on queue
      Sites::Dropoff::Worker.perform_async(
        async_request.id, job.id, drop_sites_params
      )

      # render response with async_request
      {
        async_request: {
          id: async_request.id,
        },
      }
    end

  end
end
