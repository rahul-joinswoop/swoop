# frozen_string_literal: true

module SiteService
  # This service synchronizes dispatchable data for a site to work around the
  # existing data structure is not reflected in the business processes. For
  # auto dispatching to work, each site need to have RescueProviders for
  # Swoop and for each of the FleetManagedCompanies associated with the
  # partner.
  class SyncRescueProviders

    include SwoopService

    def initialize(site)
      @site = site
    end

    def execute
      return unless @site.dispatchable? && @site.is_a?(PartnerSite)

      rescue_providers = {}
      fleet_ids = provider_fleet_ids
      @site.rescue_providers.where(company_id: fleet_ids).each do |rescue_provider|
        rescue_providers[rescue_provider.company_id] = rescue_provider
      end

      @site.transaction do
        fleet_ids.each do |fleet_id|
          rescue_provider = rescue_providers[fleet_id]
          if rescue_provider
            if rescue_provider.deleted_at
              rescue_provider.update_attributes!(deleted_at: nil)
            end
          else
            @site.rescue_providers.create!(provider: provider, company_id: fleet_id)
          end
        end
      end
    end

    private

    def provider
      @site.company
    end

    def provider_fleet_ids
      fleets = [Company.swoop_id] + provider.fleet_managed_clients.pluck(:id)
      fleets.uniq.compact
    end

  end
end
