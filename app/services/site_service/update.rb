# frozen_string_literal: true

###
# Service for updating Sites
module SiteService
  class Update

    include SwoopService

    attr_reader :site_id, :site_params, :company, :site, :duplicated_site

    def initialize(site_id:, site_params:, company:)
      @site_id = site_id
      @site_params = site_params
      @company     = company
    end

    def execute
      check_duplicated_name

      return if duplicated_site?

      @site = Site.find(@site_id)
      @site.transaction do
        if @site.update(@site_params)
          SiteService::SyncRescueProviders.new(@site).call
        end
      end
    end

    def duplicated_site?
      duplicated_site.present?
    end

    private

    def check_duplicated_name
      @duplicated_site =
        Site.where.not(id: @site_id).find_by(
          name: @site_params[:name],
          company: company,
          deleted_at: nil
        )
    end

  end
end
