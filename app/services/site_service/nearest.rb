# frozen_string_literal: true

module SiteService
  #
  # Try to find the nearest FleetSites to the given origin using Google's Distance Matrix
  # If the call to Distance Matrix Service fails, then use the crow flies distance
  #
  # Returns: Sorted array (closest first) of hashes containing:
  #  {
  #    site: <site>,
  #    miles: <miles from origin by road if available, crow flies otherwise>
  #    meters: <meters from origin by road if available, crow flies otherwise>
  #    seconds: <time in traffic if available>
  #  }
  #
  # Always returns Miles and Meters, Only returns seconds in traffic if available.
  #
  class Nearest

    include LoggableJob

    # clz - the type of Site you want to look at e.g. FleetSite, PoiSite
    def initialize(clz,
                   origin,
                   bounding_box_max_distance,
                   location_type_names,
                   fleet_company,
                   job_id)

      @origin = origin
      @bounding_box_max_distance = bounding_box_max_distance
      @location_type_names = location_type_names
      @fleet_company = fleet_company
      @job_id = job_id
      @clz = clz
    end

    def call
      # Find the nearest 15 sites to the origin using a two phase process:
      # 1) Bounding box on lat/lng the associated Locations
      # 2) calculate haversine distance (crow flies) and select the top 15
      sql = <<~HERESQL
                SELECT
                  sites.*,
                  69.0 * DEGREES(ACOS(COS(RADIANS(?))
                       * COS(RADIANS(locations.lat))
                       * COS(RADIANS(?) - RADIANS(locations.lng))
                       + SIN(RADIANS(?))
                       * SIN(RADIANS(locations.lat)))) AS distance_in_miles
                FROM sites left join locations on sites.location_id=locations.id
                WHERE
                  sites.type=?
                  and sites.deleted_at is null
                  and sites.company_id=?
                  and locations.lat BETWEEN ? AND ?
                  and locations.lng BETWEEN ? AND ?
                  #{@location_type_names.present? ? 'and locations.location_type_id in (?)' : ''}
                ORDER BY distance_in_miles
                LIMIT 15
      HERESQL

      # Convert miles to lat/lng using an approximation for the bounding box
      bounding_box_lat = @bounding_box_max_distance / Constant::MILES_PER_LAT
      bounding_box_lng = @bounding_box_max_distance / Constant::MILES_PER_LNG

      sql_parameters = [
        sql,
        @origin.lat,
        @origin.lng,
        @origin.lat,
        @clz.name,
        @fleet_company.id,
        @origin.lat - bounding_box_lat,
        @origin.lat + bounding_box_lat,
        @origin.lng - bounding_box_lng,
        @origin.lng + bounding_box_lng,
      ]

      if @location_type_names.present?
        # Add the LocationTypes filter
        sql_parameters << LocationType.where(name: @location_type_names).map(&:id)
      end

      dropoff_sites = Site.find_by_sql(sql_parameters).to_a

      if dropoff_sites.length == 0
        log :debug, "No sites within distance", job: @job_id
        return []
      end

      log :debug, "Running distance matrix with service:#{@origin.inspect} drop:#{dropoff_sites}", job: @job_id

      Rails.logger.debug "MILES #{dropoff_sites.map(&:distance_in_miles)}"

      dropoff_sites
    end

  end
end
