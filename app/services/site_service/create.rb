# frozen_string_literal: true

###
# Service for creating Sites
module SiteService
  class Create

    include SwoopService

    attr_reader :site_params, :site_type, :company, :site, :duplicated_site

    def initialize(site_params:, site_type:, company:)
      @site_params = site_params
      @company     = company
      @site_type   = site_type
    end

    def execute
      check_duplicated_name

      return if duplicated_site?

      @site = @site_type.new(@site_params)
      @site.company = @company

      if @company.is_a? RescueCompany
        copy_hq_times_if_not_dispatchable
      end

      @site.transaction do
        if @site.save
          place_location = @site.location

          if place_location && !place_location.site_id
            place_location.update(site_id: @site.id)
          end

          SiteService::SyncRescueProviders.new(@site).call
        else
          raise site.errors.to_hash.inspect
        end
      end
    end

    def duplicated_site?
      duplicated_site.present?
    end

    private

    def check_duplicated_name
      @duplicated_site = Site.find_by(
        name: @site_params[:name],
        company: company,
        deleted_at: nil
      )
    end

    def copy_hq_times_if_not_dispatchable
      # Web UI allows users to set the new site as dispatchable,
      # therefore allowing them to set specific times during site creation.
      #
      # In this case, we should not override it with HQ times.
      return if @site.dispatchable

      hq = @company.hq

      if hq
        @site.attributes = {
          open_time: hq.open_time,
          close_time: hq.close_time,
          open_time_sat: hq.open_time_sat,
          close_time_sat: hq.close_time_sat,
          open_time_sun: hq.open_time_sun,
          close_time_sun: hq.close_time_sun,
          always_open: hq.always_open,
        }
      else
        @site.attributes = { always_open: true }
      end
    end

  end
end
