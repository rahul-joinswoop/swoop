# frozen_string_literal: true

class SurveyResponse < Service

  def initialize(review, response)
    @review = review
    @response = response
    Rails.logger.debug "SurveyResponse #{review.inspect}"
    Rails.logger.debug "SurveyResponse #{response}"
    @send_slack = false
    super()
  end

  def call
    @review.survey.process(@response).each do |result|
      Rails.logger.debug "SurveyResponse creating #{result}"
      result[:review] = @review
      result[:job] = @review.job
      sr = Survey::Result.new(result)
      denormalize(sr)
      @mutated_objects.push(sr)
      @send_slack = true if sr.survey_question.order == 1
      track(sr)
    end

    self
  end

  def denormalize(result)
    order = result.survey_question.order
    if result.string_value.present?
      result.review.nps_feedback = result.string_value
    elsif result.int_value.present?
      key = "nps_question#{order}"
      if result.review.respond_to?(key)
        result.review[key] = result.int_value
      end
    end
  end

  def save_mutated_objects_throw_errors
    Reviews::SurveyReview.transaction do
      @mutated_objects.map(&:save!)
    end
    if @send_slack
      DelayedSlackSurveyWorker.perform_in(2.minutes, @review.id)
    end
  end

  private

  def track(survey_result)
    if survey_result.survey_question.nps?
      Metrics::Service.track(survey_result.job,
                             'job',
                             'customer_nps',
                             survey_result.int_value)
    end
  end

end
