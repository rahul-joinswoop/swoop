# frozen_string_literal: true

# This service will return a list of the RescueProviders for a Job
# that are within maximum allowed distances.
#
# It returns a list of providers
# -sorted by job_distance in an ascending order
# -which are closer than a max distance from the job
# -can provide the requested service  (they have the service code)
#
# Optionally the above list of providers can be narrowed down more:
# -open for business providers
# -live only providers
#
# If used for auto assignment, you should pass for_auto_assignment: true and the company
# doing the dispatching.
#
class JobGetClosestProvidersService < Service

  include LoggableJob

  MAX_DISTANCE = (ENV['CLOSEST_PROVIDERS_MAX_DISTANCE'] || 60).to_i
  MILES_PER_LAT = 69.0
  MILES_PER_LNG = 55.0 # ROUGHLY AT 40DEG

  def initialize(job, viewing_company, max_providers: nil, max_distance: nil, live_only: false, for_auto_assignment: false, dispatch_company: nil, site_open_for_business: false)
    @job = job
    @viewing_company = viewing_company
    @max_providers = max_providers
    @max_distance = max_distance || MAX_DISTANCE
    @live_only = live_only
    @dispatch_company = dispatch_company
    @for_auto_assignment = for_auto_assignment
    @site_open_for_business = site_open_for_business

    log :debug,
        "Initialised with providers:#{@max_providers} " \
        "distance:#{@max_distance}",
        job: @job.id
  end

  def call
    # We return an empty array if the job doens't have any coordinates.
    if missing_job_coordinates?
      log_missing_coordinates_error
      return []
    end

    log :debug, "lookup up for: #{@viewing_company.name}", job: @job.id

    # selecting the closest providers
    select_providers
  end

  private

  def select_providers
    # STEP 0
    providers = fetch_all_providers_with_service_code_within_bounding_box_of_the_job

    # STEP 1: remove the providers who can't do the job right now, if the caller requested it
    providers = providers.reject do |rescue_provider|
      not_live_but_should_be?(rescue_provider) ||
      not_open_but_should_be?(rescue_provider)
    end

    # STEP 2: narrow down the list more by selecting only those which are closer than a threshold
    providers = providers.select do |rescue_provider|
      # calculate the distance
      JobGetProviderDistanceService.new(@job, rescue_provider).call
      # use the distance to determine if the provider is close
      closer_than_threshold?(rescue_provider)
    end

    # STEP 3: sort the providers by job_distance in an ascending order
    providers.sort_by!(&:job_distance)

    # STEP 4: limit the result set to the maximum number specified
    providers = providers.take(@max_providers) if @max_providers

    # STEP 5: return the final narrowed down list
    providers
  end

  def fetch_all_providers_with_service_code_within_bounding_box_of_the_job
    bounding_box_lat = @max_distance / MILES_PER_LAT
    bounding_box_lng = @max_distance / MILES_PER_LNG

    log :debug,
        "fetch_all_providers_with_service_code_within_bounding_box_of_the_job - " \
        "searching for RP's with max_distance: #{@max_distance} bounding " \
        "deg:[#{bounding_box_lat},#{bounding_box_lng}]" \
        "around #{@job.service_location.lat},#{@job.service_location.lng}",
        job: @job.id

    RescueProvider.where(company: @viewing_company, deleted_at: nil)
      .includes(:tags, :location, provider: [:location, :sites], site: [:location])
      .joins(provider: [:service_codes]).where(
        companies: { deleted_at: nil }, service_codes: { id: @job.service_code_id }
      ).joins(site: [:location]).where(
        'locations.lat > ? and locations.lat < ? and locations.lng > ? and locations.lng < ?',
        @job.service_location.lat - bounding_box_lat, @job.service_location.lat + bounding_box_lat,
        @job.service_location.lng - bounding_box_lng, @job.service_location.lng + bounding_box_lng
      )
  end

  def max_allowed_distance(rescue_provider)
    # default distance
    max_provider_distance = @max_distance

    # If this is an auto assign job, use, the max distance on the rescue provider
    # configured for the dispatching company.
    if @for_auto_assignment.present?
      max_provider_distance = [provider_distance(rescue_provider), max_provider_distance].min
    end

    max_provider_distance
  end

  def provider_distance(rescue_provider)
    # Step 0: return the distance for the dispatch company's rescue provider if any
    if @dispatch_company.present?
      dispatch_rescue_provider = RescueProvider.find_by(
        company_id: @dispatch_company.id,
        provider_id: rescue_provider.provider_id,
        site_id: rescue_provider.site_id,
      )

      if dispatch_rescue_provider.present?
        return dispatch_rescue_provider.auto_assign_distance_miles
      end
    end

    # Step 1: return a default distance
    RescueProvider::DEFAULT_AUTO_ASSIGN_DISTANCE_MILES
  end

  def missing_job_coordinates?
    @job.service_location&.lat.blank? ||
    @job.service_location&.lng.blank?
  end

  def closer_than_threshold?(rescue_provider)
    max_distance = max_allowed_distance(rescue_provider)

    log :debug,
        "closer_than_threshold?:#{rescue_provider.job_distance <= max_distance} for:#{rescue_provider.inspect}",
        job: @job.id

    rescue_provider.job_distance <= max_distance
  end

  def not_open_but_should_be?(rescue_provider)
    @site_open_for_business && !rescue_provider.site.open_for_business?
  end

  def not_live_but_should_be?(rescue_provider)
    @live_only && !rescue_provider.provider.live?
  end

  def log_missing_coordinates_error
    log :error,
        "Can't fetch recommended_providers as job has no service_location lat / lng set.",
        job: @job.id
  end

end
