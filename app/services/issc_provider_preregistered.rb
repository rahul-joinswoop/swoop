# frozen_string_literal: true

require 'street_address'
class IsscProviderPreregistered < IsscService

  def match_zip(zip, sites)
    if zip.blank?
      []
    else
      sites.select { |s| s.match_zip?(zip) }
    end
  end

  def create_issc(pre, clientid, rescue_company, fleet_company, site)
    contractorid = pre[:ContractorID]
    locationid = pre[:LocationID]

    issc = Issc.find_live_issc(contractorid, clientid, locationid)

    if !issc

      location = Location.new(city: pre[:City],
                              state: pre[:State],
                              zip: pre[:ZipCode],
                              address: pre[:AddressLine1])

      issc_location = IsscLocation.where(
        site: site,
        locationid: locationid,
        fleet_company: fleet_company,
      ).first_or_create! do |loc|
        loc.location = location
        loc.save!
      end

      Issc.create!({
        company_id: rescue_company.id,
        site: site,
        contractorid: contractorid,
        clientid: clientid,
        issc_location: issc_location,
        status: 'Unregistered',
      })
    else
      Rails.logger.debug "IsscProviderPreregistered - Found existing ISSC, leaving alone #{issc.inspect}"
      nil
    end
  end

  def call
    tax_id = @event[:TaxID]
    clientid = @event[:ClientID]
    contractorid = @event[:ContractorID]
    rescue_company = RescueCompany.includes(sites: [:location]).find_by(tax_id: tax_id)
    guard = 0
    while rescue_company.parent_company_id && (guard < 5)
      rescue_company = RescueCompany.includes(sites: [:location]).find(rescue_company.parent_company_id)
      guard += 1
    end
    fleet_company = FleetCompany.find_by(issc_client_id: clientid)

    preregs = @event[:preregistrations] # PDW this was overly restrictive ,taken care of by the existing check:  .reject {|p| existing_locations.include?(p[:LocationID])}
    Rails.logger.debug "Preregistrations: #{preregs}"

    matches = []
    isscs = preregs.collect do |pre|
      # Match using zipcode

      zip = pre[:ZipCode]
      unless zip
        sa = StreetAddress::US.parse(pre[:AddressLine1])
        zip = sa&.postal_code
      end

      site_matches = match_zip(zip, rescue_company.sites.where(deleted_at: nil))

      if site_matches.length > 1
        # Matched another site, this will be logged but not put into the database
        Rails.logger.warn "IsscProviderPreregistered: MATCH DUPLICATES clientid: #{clientid}, contractorid: #{contractorid}, locationid: #{pre[:LocationID]}, zip: #{pre[:ZipCode]}"
      end
      if site_matches.length == 0
        Rails.logger.warn "IsscProviderPreregistered: NO MATCH clientid: #{clientid}, contractorid: #{contractorid}, locationid: #{pre[:LocationID]}, zip: #{pre[:ZipCode]}"

        if (ENV["SWOOP_ENV"] == "staging") || (ENV["SWOOP_ENV"] == "local")
          to = 'staging_isscalert@joinswoop.com'
        else
          to = 'isscalert@joinswoop.com'
        end

        Delayed::Job.enqueue EmailAlert.new(to,
                                            "ISSC Alert - preregistration for #{pre[:ContractorID]} #{pre[:LocationID]} failed to match a site",
                                            "Unable to match against: #{rescue_company.sites.where(deleted_at: nil).to_a}",
                                            pre)
      end

      matches.append [pre[:LocationID], site_matches.map(&:location).as_json]

      create_issc(pre, clientid, rescue_company, fleet_company, site_matches.first)
    end

    isscs = isscs.compact

    Rails.logger.debug "IsscProviderPreregistered: #{JSON.pretty_generate(preregs)}, Matches:, #{JSON.pretty_generate(matches)}"

    isscs
  end

end
