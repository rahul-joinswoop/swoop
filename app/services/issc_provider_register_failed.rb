# frozen_string_literal: true

class IsscProviderRegisterFailed < IsscService

  def call
    issc = find_issc
    issc.provider_register_failed
    issc.set_error "Failed to add Contractor ID #{issc.contractorid} for #{issc.company.name} - Please check for mistakes."
    issc.base_publish('error')
    issc
  end

end
