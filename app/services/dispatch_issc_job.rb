# frozen_string_literal: true

class ISSCFleetCompanyMismatch < StandardError; end
class DispatchIsscJob < DispatchJob

  attr_reader :issc

  def initialize(issc, job)
    @issc = issc
    super(job)
  end

  def call
    clientid = @issc.clientid

    fleet_company = FleetCompany.where(issc_client_id: clientid).first
    raise ISSCFleetCompanyMismatch, "ISSC Client ID #{clientid} does not match a fleet company" if fleet_company.nil?

    @job.fleet_company_id = fleet_company.id
    @job.issc_dispatch_request.issc = @issc

    @job.hack_create_ajs('Initial')

    super

    @job.issc_assign
    @job.save!
    @job.assign_rescue_company(@issc.site)
  end

end
