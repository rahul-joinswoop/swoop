# frozen_string_literal: true

class HandleToa

  include LoggableJob

  def initialize(api_company, api_user, job, toa)
    @api_company = api_company
    @api_user = api_user
    @job = job
    @toa = toa
  end

  def call()
    return if @toa.blank? || @toa['latest'].blank?

    Rails.logger.debug "handle @toa called with #{@toa}"
    time = @toa['latest']
    new_toa = @job.add_time_of_arrival(time, @api_user, @api_company)

    if @job.fleet_managed_job?
      existing_alert = ConfirmPartnerOnSiteSmsAlert.find_by(job: @job, sent_at: nil, canceled_at: nil)
      if !existing_alert
        if new_toa.time > Time.now
          log :debug, "HandleToa, updated ETA with no ConfirmPartnerOnSiteSmsAlert set, creating new one", job: @job.id
          ConfirmPartnerOnSiteSmsAlert.create!(job: @job)
        else
          log :debug, "HandleToa TOA updated to be in the past", job: @job.id
        end
      else
        log :debug, "HandleToa, found existing TOA, leaving alone", job: @job.id
      end
    end
  end

end
