# frozen_string_literal: true

# Service to update only blank fields of a model.
# ex.
#
# UpdateModelBlankFields.new(
#   model_instance: RescueCompany.first,
#   model_attributes: [:fax, :phone],
#   model_new_values: {fax: '3045551234', phone: '3046669900' }).call
#
# The instance above ^ will update RescueCompany.first fax and phone fields
# ONLY if they are blank in the RescueCompany.first instance.
class UpdateModelBlankFields

  include SwoopService

  attr_reader :model_instance, :model_attributes, :model_new_values

  # @attr model_instance - a model instance
  # @attr model_attributes - array containing the attributes to be checked if blank in the instance
  # @attr model_new_values - array containing new values for the blank fields found based on model_attributes
  def initialize(model_instance:, model_attributes:, model_new_values:)
    @model_instance = model_instance
    @model_attributes = model_attributes
    @model_new_values = model_new_values
  end

  def execute
    return self if [@model_instance, @model_attributes, @model_new_values].include? nil

    blank_fields = {}

    model_attributes.each do |attribute|
      blank_fields[attribute] = model_new_values[attribute] if @model_instance.send(attribute).blank?
    end

    @model_instance.update_attributes!(blank_fields)

    self
  end

end
