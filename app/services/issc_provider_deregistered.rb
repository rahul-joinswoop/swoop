# frozen_string_literal: true

class IsscProviderDeregistered < IsscService

  def call
    issc = find_issc
    issc.token = nil
    issc.deleted_at = Time.now if issc.delete_pending_at.present?
    issc.provider_deregistered
    issc
  end

end
