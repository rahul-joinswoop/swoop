# frozen_string_literal: true

module Territories
  class BackfillTerritoriesForCompany

    def call(company_id, simulation: true)
      super_user = User.with_role(:root).first

      puts "Starting to Backfill Territories for company_id: #{company_id}"
      unless RescueCompany.exists?(company_id)

        # Error Step E0 - in case the company doesn't exist log the error and return
        puts "Backfill failed: The RescueCompany with id: #{company_id} does not exist"
        return false
      end

      # Step 0 - Find the Rescue Company
      rescue_company = RescueCompany.find company_id
      rescue_company_sites = rescue_company.sites.not_deleted

      puts "Backfill: The RescueCompany is found: #{rescue_company}"

      unless rescue_company.territories.count == 0
        # Alternate Path 0 - in case there are territories then halt
        puts "Backfill halted: There are '#{rescue_company.territories.count}' nr of territories for RescueCompany with id: #{company_id}"
        return false
      end

      ActiveRecord::Base.transaction do
        # Step 1 - create the new terrotiories for each site
        rescue_company_sites.each do |site|
          if can_calculate_the_circleoid?(site)
            puts "Backfill: can calculate the Circloid for site: #{site}"
            latitude = site.location.lat
            longitude = site.location.lng
            radius = fetch_radius_from_site(site) || RescueProvider::DEFAULT_AUTO_ASSIGN_DISTANCE_MILES

            unless simulation
              polygon = Territory.circleoid_coordinates center_longitude: longitude,
                                                        center_latitude: latitude,
                                                        radius: radius

              Territories::Create.call company_id: rescue_company.id,
                                       polygon: polygon,
                                       user: super_user
            end
            puts "Backfill: territory was successfull created for the site: #{site}"
          end
        end
      end

      puts "Done with Backfill Territories for company_id: #{company_id}"
      true
    rescue
      # Error Step E1 - in case of an exception log the expcetion
      puts "Backfill failed: #{$ERROR_INFO}"
      false
    end

    private

    def fetch_radius_from_site(site)
      # On production & staging, a site never has more than one rescue provider with a distance set
      provider = site.rescue_providers.where.not(auto_assign_distance: nil).first
      provider.try(:auto_assign_distance)
    end

    def can_calculate_the_circleoid?(site)
      site.location.lat.present? &&
        site.location.lng.present?
    end

  end
end
