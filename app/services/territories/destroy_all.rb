# frozen_string_literal: true

module Territories
  class DestroyAll

    include Interactor

    before do
      # STEP 0.0: Finding the company
      context.company = Company.find context.company_id

      # STEP 0.1: Populating the territories
      context.territories = context.company.territories

      # STEP 0.2: Asking for permission form the ACL layer
      raise SecurityError unless TerritoriesPolicy.new(context).can_destroy_all?
    end

    def call
      # These operations should be ACIDified
      ApplicationRecord.transaction do
        # STEP 1: Oterating through the territories
        context.territories.each do |territory|
          # STEP 1.1: Creating the audit record
          context.audit_record = TerritoryAuditTrail.create! territory: territory,
                                                             user: context.user,
                                                             action: :destroyed
        end

        # STEP 2: Soft deleting all the territories
        SoftDelete.soft_delete_all context.territories
      end
    end

  end
end
