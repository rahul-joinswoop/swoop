# frozen_string_literal: true

module Territories
  class Destroy

    include Interactor

    before do
      # STEP 0.0: Finding the Territory
      context.territory = Territory.find context.territory_id

      # STEP 0.1: Asking for permission form the ACL layer
      raise SecurityError unless TerritoriesPolicy.new(context).can_destroy?
    end

    def call
      # These operations should be ACIDified
      ApplicationRecord.transaction do
        # STEP 1: Creating the audit record
        TerritoryAuditTrail.create! territory: context.territory,
                                    user: context.user,
                                    action: :destroyed

        # STEP 2: Soft deleting the entity
        context.territory.soft_delete!
      end
    end

  end
end
