# frozen_string_literal: true

module Territories
  class Show

    include Interactor

    before do
      # STEP 0.0: Finding the Territory
      context.territory = Territory.find context.territory_id

      # STEP 0.1: Asking for permission form the ACL layer
      raise SecurityError unless TerritoriesPolicy.new(context).can_show?
    end

    def call; end

  end
end
