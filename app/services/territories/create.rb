# frozen_string_literal: true

module Territories
  class Create

    include Interactor

    before do
      # STEP 0.0: Finding the company
      context.company = Company.find context.company_id

      # STEP 0.1: Asking for permission form the ACL layer
      raise SecurityError unless TerritoriesPolicy.new(context).can_create?
    end

    def call
      # STEP 1: Preparing the incoming params to be suitable for the postgis
      polygon = Territory.transform_coordinates_to_postgis_polygon_string context.polygon

      # These operations should be ACIDified
      ApplicationRecord.transaction do
        # STEP 2: Creating the territory
        context.territory = context.company.territories.create! polygon_area: polygon,
                                                                company_id: context.company.id

        # STEP 3: Creating the audit record
        context.audit_record = TerritoryAuditTrail.create! territory: context.territory,
                                                           user: context.user,
                                                           action: :created
      end
    end

  end
end
