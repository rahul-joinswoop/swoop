# frozen_string_literal: true

module Territories
  class Index

    include Interactor

    before do
      # STEP 0.0: Finding the Company
      context.company = Company.find context.company_id

      # STEP 0.1: Asking for permission form the ACL layer
      raise SecurityError unless TerritoriesPolicy.new(context).can_index?
    end

    def call
      # STEP 1: Populating the territories
      context.territories = context.company.territories
    end

  end
end
