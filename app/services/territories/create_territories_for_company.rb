# frozen_string_literal: true

module Territories
  class CreateTerritoriesForCompany

    DEFAULT_AUTO_ASSIGN_DISTANCE_MILES_FOR_NEW_RESCUE_COMPANY = 5

    def call(rescue_company)
      create_territories rescue_company
    end

    private

    def create_territories(rescue_company)
      # TODO - This solution might fail when we try to get the first root user and the user is not available
      # as deleted for whatever reason.
      super_user = User.root_users.first

      polygon = Territory.circleoid_coordinates center_longitude: rescue_company.location.lng,
                                                center_latitude: rescue_company.location.lat,
                                                radius: DEFAULT_AUTO_ASSIGN_DISTANCE_MILES_FOR_NEW_RESCUE_COMPANY

      Territories::Create.call company_id: rescue_company.id,
                               polygon: polygon,
                               user: super_user
      Rails.logger.debug "CreateTerritoriesForCompany: territory was successfull created for the RescueCompany: #{rescue_company.id}"
    end

  end
end
