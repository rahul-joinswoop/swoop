# frozen_string_literal: true

require 'issc'

class IsscConnected < Service

  def initialize(data, connection_manager: ISSC::ConnectionManager.new)
    @data = data
    @issccon = connection_manager
  end

  def init_connect
    Rails.logger.debug "ISSC - Connected"
    @issccon.set_connection_status ISSC::ConnectionManager::CONNECTED
    @issccon.update_heartbeat(-1, Time.now.utc)
  end

  def call
    if @data[:Result] == "Success"
      # Put all the providers in a logged-out state
      # PDW
      # on disconnect all providers are logged out. it's not clear to me if the same is true on a network timeout
      # might be better doing a serviceprovider status

      Issc.where(system: nil, status: ["LoggedIn", "LoggingIn", "LoggingOut"]).update_all(status: "Registered")

      init_connect
    elsif @data[:Description] == "Already connected."
      init_connect
    end
  end

end
