# frozen_string_literal: true

class IsscService < Service

  def initialize(event)
    @event = event
    Rails.logger.debug "IsscService #{@event}"
  end

  def find_issc(fetch_deleted: false)
    Issc.find_issc(@event, fetch_deleted: fetch_deleted)
  end

end
