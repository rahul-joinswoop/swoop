# frozen_string_literal: true

class CreateAccountForJobService < Service

  def initialize(job, fleet_company)
    @job = job
    @fleet_company = fleet_company

    raise ArgumentError, "job or fleet_company" unless @job && @fleet_company
  end

  def call
    rescue_company = @job.rescue_company
    args = { client_company: @fleet_company, company: rescue_company }
    account = Account.find_by(args)

    if account
      Rails.logger.debug "CreateAccountForJobService: Found account: #{account.inspect}"
    else
      args[:accounting_email] = @fleet_company[:accounting_email]
      args[:name] = @fleet_company[:name]
      args[:location_id] = @fleet_company[:location_id]
      args[:phone] = @fleet_company[:phone]
      account = Account.create!(args)
      Rails.logger.debug "CreateAccountForJobService: Created account #{account.inspect}"
    end
    # not sure if it would be a good idea to move `account` assigment
    # here instead of `Job#assign_rescue_company`
    account
  end

end
