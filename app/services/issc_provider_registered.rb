# frozen_string_literal: true

class IsscProviderRegistered < IsscService

  def call
    issc = find_issc

    if @event[:Token]
      issc.token = @event[:Token]
    end

    issc.provider_registered
    issc
  end

end
