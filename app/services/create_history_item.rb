# frozen_string_literal: true

class CreateHistoryItem

  include Interactor

  attr_accessor :job, :target, :event

  # The values here indicate the type of history item each description
  # represents. Defaults to "History"

  HISTORY_TYPES = {
    HistoryItem::DROP_OFF_LOCATION_EDITED => "Edited",
    HistoryItem::PICKUP_LOCATION_EDITED => "Edited",
    HistoryItem::INVOICE_EDITED => "Edited",
    HistoryItem::INVOICE_EDITED_VCC_PAYMENT => "Edited",
    HistoryItem::SERVICE_EDITED => "Edited",
  }.freeze

  before do
    @job = context.job
    @target = context.target
    @event = context.event
    @user = context.user
  end

  def call
    history_item = job.history_items.find_by(
      title: event
    )

    if history_item && create_only_once?
      history_item.touch(:adjusted_created_at)
    else
      history_item = job.history_items.build(
        title: event,
        target: target,
        company: company,
        user: user,
        history_type: fetch_history_type,
        source_application_id: RequestContext.current.source_application_id,
      )

      # If the target has not been persisted yet, then we don't want to call save here because
      # doing so will persist the target and it should not be up to this class to make that
      # decision. Because of this, all classes that may appear as the target should include
      # the HistoryItemTarget concern
      if target.nil? || target.persisted? || !target.respond_to?(:target_history_items)
        history_item.save!
      else
        target.target_history_items << history_item
      end
    end

    context.result = history_item
  end

  private

  def create_only_once?
    if HISTORY_TYPES[event] == "Edited" || event.include?("Job Details Emailed")
      return false
    end

    true
  end

  def user
    @user || job.last_touched_by
  end

  def company
    if job.last_touched_by
      job.last_touched_by.company
    else
      job.fleet_company
    end
  end

  def fetch_history_type
    return @history_type if @history_type

    @history_type = HISTORY_TYPES[@event]

    if @event.include?("Job Details Emailed")
      @history_type ||= "Emailed"
    end

    @history_type ||= "History"

    @history_type
  end

end
