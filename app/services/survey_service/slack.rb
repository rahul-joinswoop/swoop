# frozen_string_literal: true

module SurveyService
  class Slack

    def initialize(review_id)
      @review = Review.find(review_id)
    end

    def call
      localtime = Time.now.in_time_zone('America/Los_Angeles')
      hms = localtime.strftime('%H:%M:%S')
      str = "#{hms} #{@review.job.swoop_summary_name}\n"

      @review.survey_results.each do |result|
        str += result.slack_message
      end

      tags = ['review']
      tags.unshift(@review.job.fleet_company&.name)
      SlackChannel.message(str, tags: tags, add_default: false)
    end

  end
end
