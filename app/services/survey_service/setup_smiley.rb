# frozen_string_literal: true

module SurveyService
  class SetupSmiley

    CLASS_MAP = {
      'LongTextQuestion' => Survey::LongTextQuestion,
      'TenStarQuestion' => Survey::TenStarQuestion,
    }.freeze

    # SetupTypeformSurvey.new('Super Fleet Name','Super Fleet Typeform Survey','lamtha.survey.com',
    #                [['LongTextQuestion','How was the phone reps service today?','Question 1 - Phone Rep Service'],...]).call
    def initialize(company_name, survey_name, survey_host, questions)
      @company = Company.find_by!(name: company_name)
      @survey_host = survey_host
      @survey_name = survey_name
      @questions = questions
    end

    def call
      @company.update_attributes!( # review_url:"https://#{@survey_host}/to/",
        survey_template: 'api/v1/review/whitelabel/swoop_gamified.html.erb',
        survey_host: @survey_host
      )
      @company.save!

      survey = Survey::Smiley.create!(
        name: @survey_name,
        company: @company
      )

      idx = 1
      @questions.each do |clz_name, question, description|
        clz = CLASS_MAP[clz_name]
        clz.create!(
          survey: survey,
          order: idx,
          question: question,
          description: description
        )

        idx += 1
      end
    end

  end
end
