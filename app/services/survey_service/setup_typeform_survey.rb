# frozen_string_literal: true

module SurveyService
  class SetupTypeformSurvey

    CLASS_MAP = {
      'LongTextQuestion' => Survey::LongTextQuestion,
      'FiveStarQuestion' => Survey::FiveStarQuestion,
    }.freeze

    # SetupTypeformSurvey.new('Super Fleet Name','Super Fleet Typeform Survey','lamtha.nasurvey.com','eoPPHs',
    #                [['LongTextQuestion','Question 1 - Phone Rep Service','tTey'],...]).call
    def initialize(company_name, survey_name, survey_host, typeform_surveyid, questions)
      @company = Company.find_by!(name: company_name)
      @survey_host = survey_host
      @typeform_surveyid = typeform_surveyid
      @survey_name = survey_name
      @questions = questions
    end

    def call
      @company.update_attributes!(review_url: "https://swoopme.typeform.com/to/#{@typeform_surveyid}",
                                  survey_template: 'api/v1/review/whitelabel/fleet_managed.html',
                                  survey_host: @survey_host)
      @company.save!

      survey = Survey::Typeform.create!(
        name: @survey_name,
        company: @company,
        ref: @typeform_surveyid
      )

      idx = 1
      @questions.each do |clz_name, description, ref|
        clz = CLASS_MAP[clz_name]
        clz.create!(
          survey: survey,
          order: idx,
          ref: ref,
          description: description
        )

        idx += 1
      end
    end

  end
end
