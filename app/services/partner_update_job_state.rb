# frozen_string_literal: true

# Partner has selected a state withing the drop down
#
class PartnerUpdateJobState < API::Jobs::UpdateJobStatus

  SERVICE_TYPE = :partner
  LEGAL_STATES = [
    # we shouldn't ever be called for Pending below but we
    # need this in the list of legal statuses
    "Pending",
    "Accepted",
    "Dispatched",
    "En Route",
    "On Site",
    "Towing",
    "Tow Destination",
    "Completed",
    "Released",
    "GOA",
    "Stored",
    "Canceled",
    "Rejected",
    "Deleted",
  ].freeze

  private

  def execute_custom_actions_for_status
    case @status
    when "Pending"
    when "Accepted"
      @job.remove_rescue_driver
    when "Dispatched"
    when "En Route"
    when "On Site"
    when "Towing"
    when "Tow Destination"
    when "Completed"
      @job.send_user_invoice
      @job.send_review_sms
      if @job.store_vehicle?
        JobService::CreateStorage.call(
          job: @job,
          user: @api_user
        )
      end
    when "Released"
      @job.release_storage
    when "GOA"
      @job.mark_invoice_goa
    when "Stored"
    when "Canceled"
      @job.cancel_invoice
    when "Rejected"
      @job.after_partner_reject
      @job.delete_invoice
    when "Deleted"
      @job.deleted_at = Time.now
      @job.delete_invoice
    else
      raise CustomActionError, "Unknown job status: #{@status}"
    end
  end

end
