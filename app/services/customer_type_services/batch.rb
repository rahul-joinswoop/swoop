# frozen_string_literal: true

module CustomerTypeServices
  class Batch

    def initialize(api_company:, customer_type_ids:)
      @api_company = api_company
      @customer_type_ids = customer_type_ids.split(',')

      Rails.logger.debug "CustomerType batch load params: company.id = #{@api_company.id}, customer_type_ids = #{@customer_type_ids}"
    end

    def call
      CustomerType.where(id: @customer_type_ids)
    end

  end
end
