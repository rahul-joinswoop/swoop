# frozen_string_literal: true

###
# Service for creating CustomerTypes
module CustomerTypeServices
  class Create

    include SwoopService

    attr_reader :customer_type, :duplicated_customer_type, :empty_name

    def initialize(customer_type_name: [])
      @customer_type_name = customer_type_name
    end

    def execute
      check_duplicated_name
      check_empty

      return if @duplicated_customer_type
      return if @empty_name

      @customer_type = CustomerType.create!(name: @customer_type_name)

      self
    end

    def empty_name?
      empty_name
    end

    def duplicated_customer_type?
      duplicated_customer_type
    end

    private

    def check_duplicated_name
      @duplicated_customer_type =
        CustomerType.where('lower(name) = ?', @customer_type_name.downcase).exists?
    end

    def check_empty
      @empty_name = @customer_type_name.to_s.strip.empty?
    end

  end
end
