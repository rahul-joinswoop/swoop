# frozen_string_literal: true

class ProcessConfirmPartnerOnSiteResponse

  include LoggableJob

  def initialize(twilio_response, sms_alert, allow_negative_response = true)
    @twilio_response = twilio_response
    @sms_alert = sms_alert
    @allow_negative_response = allow_negative_response
  end

  def set_parsed_response(sms, response)
    sms.parsed_response = response
    sms.response_received_at = Time.now
  end

  def call()
    response = @twilio_response.body.to_s
    sms = @twilio_response.target
    sms.response = response

    job_id = @sms_alert.job_id

    if !sms.parsed_response
      if response.include?('Y') || response.include?('1')
        # @sms_alert.job.send_location_audit_sms
        log :debug, "process_response - parsed 1", job: job_id
        set_parsed_response(sms, 1)

        # if dispatched,enroute transition to onsite
        if [Job::STATUS_DISPATCHED, Job::STATUS_ENROUTE].include? @sms_alert.job.status
          srv = SwoopUpdateJobState.new(@sms_alert.job, "On Site")
          srv.call
          @sms_alert.job.save!
        end

        # send thanks
        Sms::GreatThanks.send_notification(@sms_alert.job)

      elsif @allow_negative_response && (response.include?('N') || response.include?('2'))
        log :debug, "process_response - parsed 2", job: job_id
        set_parsed_response(sms, 2)
        Sms::PartnerNotOnSite.send_notification(@sms_alert.job)
        ViewAlertService.customer_says_not_on_site(@sms_alert.job, true)
      else
        log :warn, "::process_response - expected 1 or 2, got:#{response}", job: job_id
      end
    else
      log :debug, "process_response discarding response as already have one: #{response}", job: job_id
    end

    sms.save!
  end

end
