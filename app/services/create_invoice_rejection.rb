# frozen_string_literal: true

# Service to deal with what happens when a user rejects an invoice.
#
# The service will create a new InvoiceRejection with the given invoice, invoice
# reject reason and notes. The given invoice will transition to a new state via
# the specified invoice event. It also adds an EmailInvoiceRejection to the
# async worker queue.
#
# Raises InvalidInvoiceRejectionError when the InvoiceRejection is not valid.
#
# Raises AASM::InvalidTransition when the invoice can't transition with the
# given event.
class CreateInvoiceRejection

  include SwoopService

  # Error raised when attempting to create an invalid InvoiceRejection
  class InvalidInvoiceRejectionError < StandardError; end

  attr_reader :invoice_rejection

  def initialize(invoice:, invoice_state_event:, invoice_reject_reason:,
                 notes: nil)
    @invoice = invoice
    @invoice_state_event = invoice_state_event
    @invoice_reject_reason = invoice_reject_reason
    @notes = notes
  end

  def execute
    new_invoice_rejection
    raise InvalidInvoiceRejectionError unless @invoice_rejection.valid?

    return unless @invoice_rejection.save

    @invoice.send("#{@invoice_state_event}!")
    @invoice_rejection
  end

  def after_execution
    Delayed::Job.enqueue EmailInvoiceRejection.new(@invoice_rejection.id)
  end

  private

  def new_invoice_rejection
    @invoice_rejection ||= InvoiceRejection.new(
      invoice: @invoice,
      invoice_reject_reason: @invoice_reject_reason,
      notes: @notes
    )
  end

end
