# frozen_string_literal: true

module QBServices
  class FetchToken

    include SwoopService

    attr_reader :qb_token

    def initialize(company_id:)
      raise ArgumentError, "QB::FetchToken#initialize - company_id is null" unless company_id

      @company_id = company_id

      Rails.logger.debug("QB::FetchToken#initialize - company_id #{@company_id}")
    end

    def execute
      @qb_token = QB::Token.find_by_company_id(@company_id)
    end

  end
end
