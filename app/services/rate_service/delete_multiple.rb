# frozen_string_literal: true

module RateService
  # This service will mark as deleted a list of Rate objects.
  #
  # No rate will be marked as deleted unless all given can be marked as deleted.
  #
  # After the Rate objects are marked as deleted,
  # Job.schedule_all_rate_invoice_updates will be called.
  #
  # @attr [Array<Rate>] rates a list of Rate objects
  # @attr [Account] account an account associated with the Rate objects
  class DeleteMultiple

    include SwoopService

    attr_reader :rates, :account

    # @param [Array<Rate>] rates the list of Rate objects to mark as deleted
    # @param [Account] an account associated to the Rates
    def initialize(rates:, account:)
      @rates = rates
      @account = account
    end

    def execute
      rates.each do |rate|
        # TODO split the delete logic between class_types and rate_types
        # otherwise, the line below will throw error when class_types are deleted in the FE
        # raise ArgumentError, "Cannot delete live rates" if rate.live
        rate.no_invoice_updates = true
        rate.deleted_at = Time.zone.now
        rate.save!
      end
    end

    def after_execution
      return unless @account

      Job.schedule_all_rate_invoice_updates(account)
    end

  end
end
