# frozen_string_literal: true

module RateService
  # This service expects a `rates` hash with rate (:id, :attributes) pairs. For
  # each :id, the corresponding rate is fetched for the given `company` and
  # updated with the :attributes.
  #
  # Example of the `rates` hash:
  #
  #   {
  #     123: {
  #       "flat": "95.00",
  #       "vehicle_category_id": "6",
  #       "service_code_id": null,
  #       "account_id":4,
  #       "type": "FlatRate"
  #     }
  #   }
  #
  # No rate will be saved unless all can be saved.
  #
  # Raises ActiveRecord::RecordNotFound when a rate can't be found for the
  # given `company` by an `id`
  class UpdateMultiple

    include SwoopService

    attr_reader :rates, :saved_rates, :company

    def initialize(rates:, company:)
      @rates = rates
      @company = company
      @saved_rates = []
    end

    def execute
      rates.each do |id, attributes|
        Rails.logger.debug "UpdateMultiple assigning: #{attributes}"
        update_rate(id, attributes)
      end
    end

    private

    def update_rate(id, attributes)
      rate = company.rates.find_by!(id: id)
      rate.assign_attributes(attributes)
      rate.save!
      @saved_rates << rate
    end

  end
end
