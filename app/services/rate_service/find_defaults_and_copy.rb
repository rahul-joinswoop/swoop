# frozen_string_literal: true

# # #
#
# Service to copy existent default rates using the specific params passed.
#
# It's totally based on levels of a Rate, and it follows this order:
#   VALID_ORDERED_FILTERS = [:account_id, :site_id, :vendor_id, :service_code_id]
#
# For instance, say we have a universal default Rate:
#   { company_id: 1, account_id: nil, site_id: nil, vendor_id: nil, service_code_id: nil, type: 'HoursP2PRate' }
#
#   and it receives { account_id: 10 } on @rate_attributes_filter_hash
#
#  (it means the user has added a new account on Rates table)
#
#   it will copy the existent default rate ^ for this account_id:
#   new rate will be: { company_id: 1, account_id: 10, site_id: nil, vendor_id: nil, service_code_id: nil, type: 'HoursP2PRate' }
#
#
#
# Another example, when there's a default rate for a vendor_id level:
#   { company_id: 1, account_id: 10, site_id: 20, vendor_id: 304050, service_code_id: nil, type: 'HoursP2PRate' }
#
#   and we add a new service for that rate level (vendor_id level):
#   { account_id: 10, site_id: 20, vendor_id: 304050, service_code_id: 60} on @rate_attributes_filter_hash
#
#  (it means the user has added a new service for an account with site and contractor id on Rates table)
#
#   it will copy the default rate ^ for this account_id, site_id and vendor_id, and append the service_code_id passed:
#   new rate will be: { company_id: 1, account_id: 10, site_id: 20, vendor_id: 304050, service_code_id: 60, type: 'HoursP2PRate' }
#
#
# One variation for this vendor_id case is when we already have previous rates added for the vendor_id level,
#   but the vendor_id level doesn't have default rates. In this case, the service will go up a level and try to
#   look for default rates of the site level.
#
#   To illustrate:
#
#   default rate only exists on site_id level (it means vendor_id will be nil):
#   { company_id: 1, account_id: 10, site_id: 20, vendor_id: nil, service_code_id: nil, type: 'HoursP2PRate' }
#
#   and we add a new service for that rate level (vendor_id level):
#   { account_id: 10, site_id: 20, vendor_id: 304050, service_code_id: 60} on @rate_attributes_filter_hash
#
#   it will copy the default rate ^ for the given account_id and site_id, and will append the vendor_id and service_code_id passed:
#   new rate will be: { company_id: 1, account_id: 10, site_id: 20, vendor_id: 304050, service_code_id: 60, type: 'HoursP2PRate' }
#
#  Still in this case, if a default rate for the site_id doesn't exist exist, it will go up one level more and try to
#  look for default rates on the account level. If no account level exists, it will try the universal rate level.
#
#
module RateService
  class FindDefaultsAndCopy

    include Interactor
    include Interactor::Swoop

    VALID_ORDERED_FILTERS = [:account_id, :site_id, :vendor_id, :service_code_id].freeze

    before do
      Rails.logger.debug "RateService::FindDefaultsAndCopy called #{context}"

      @api_company_id              = context.input[:api_company_id]
      @rate_attributes_filter_hash = context.input[:rate_attributes_filter_hash].to_h.symbolize_keys

      unless @api_company_id
        raise ArgumentError, 'RateService::FindDefaultsAndCopy @api_company_id is null'
      end

      unless @rate_attributes_filter_hash
        raise ArgumentError, 'RateService::FindDefaultsAndCopy @rate_attributes_filter_hash is null'
      end
    end

    def call
      rates_to_be_copied = nil

      (1..valid_filter_keys.size).each do |filter_level|
        query_hash = build_query_hash(filter_level)

        rates_to_be_copied = api_company.rates.where(query_hash.merge(deleted_at: nil))

        break if rates_to_be_copied.exists?
      end

      copy_rates_service = RateService::CopyRates.new(
        rates: rates_to_be_copied,
        company: api_company,
        new_attributes: @rate_attributes_filter_hash
      )

      context.output = copy_rates_service.call_with_transaction.new_rates
    end

    private

    def build_query_hash(filter_level)
      filter_keys = build_filter_keys(filter_level)

      (filter_valid_values(filter_keys) + filter_nil_values(filter_keys)).reduce({}, :merge)
    end

    def build_filter_keys(filter_level)
      VALID_ORDERED_FILTERS.slice(0, valid_filter_keys.size - filter_level)
    end

    def filter_valid_values(filter_keys)
      filter_keys.map { |filter_key| { filter_key => @rate_attributes_filter_hash[filter_key] } }
    end

    def filter_nil_values(filter_keys)
      (VALID_ORDERED_FILTERS - filter_keys).map { |filter_key| { filter_key => nil } }
    end

    # Will filter out not valid filter keys that might come in @rate_attributes_filter_hash
    def valid_filter_keys
      VALID_ORDERED_FILTERS & @rate_attributes_filter_hash.keys
    end

    def api_company
      @api_company ||= Company.find(@api_company_id)
    end

  end
end
