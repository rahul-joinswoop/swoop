# frozen_string_literal: true

module RateService
  class MailRateChange

    include SwoopService

    def initialize(company:, user:, company_to_be_notified:)
      @company  = company
      @user     = user
      @company_to_be_notified = company_to_be_notified
    end

    def execute; end

    def after_execution
      if @company_to_be_notified.accounting_email.blank?
        Rails.logger.error I18n.t('rate.change_email_not_sent',
                                  company_name: @company_to_be_notified.name, company_id: @company_to_be_notified.id)

        return nil
      end

      if @company.accounting_email.blank?
        Rails.logger.error I18n.t('rate.company_with_no_accounting_email',
                                  company_name: @company.name, company_id: @company.id)
      end

      Delayed::Job.enqueue(change_rate_email)
    end

    private

    def change_rate_email
      EmailRateChangeToPartnerAndFleet.new(@company.id, @company_to_be_notified.id, @user.id)
    end

  end
end
