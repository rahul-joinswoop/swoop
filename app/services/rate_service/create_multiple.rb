# frozen_string_literal: true

module RateService
  # This service expects an array of Rate attributes that will be saved to the
  # database.
  #
  # Example:
  #
  #   [
  #     {
  #       "flat": "95.00",
  #       "vehicle_category_id": "6",
  #       "service_code_id": null,
  #       "account_id":4,
  #       "type": "FlatRate"
  #     }
  #   ]
  #
  # No rate will be saved unless all can be saved.
  #
  # Raises ArgumentError when a rate with the given attributes already exists in
  # the database.
  class CreateMultiple

    include SwoopService

    attr_reader :rates, :created_rates, :company

    def initialize(rates:, company:, account:)
      @rates = rates
      @company = company
      @account = account
      @created_rates = []
    end

    def execute
      rates.each do |attributes|
        Rails.logger.debug "CreateMultiple creating: #{attributes}"

        rate = new_rate(attributes)
        rate.save!

        @created_rates << rate
      end
    end

    private

    def new_rate(attributes)
      rate = Rate.new(attributes)
      rate.account = @account if @account

      check_unique(rate)

      rate.company = company
      rate_account = rate.account
      rate.fleet_company = rate_account.client_company if rate_account

      unless company.live_rates?(
        account: rate_account, service_code: rate.service_code
      )
        rate.live = true
      end

      rate
    end

    def check_unique(rate)
      return unless rate.duplicate?(company)

      raise ArgumentError,
            "check_unique - Found existing rate for site(#{rate.site_id}), " \
            "vendor_id(#{rate.vendor_id}), " \
            "account(#{rate.account_id}), service(#{rate.service_code_id}, " \
            "rate(#{rate.type}), vehicle_category_id(#{rate.vehicle_category_id}))"
    end

  end
end
