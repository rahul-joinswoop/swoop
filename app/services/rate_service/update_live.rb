# frozen_string_literal: true

module RateService
  # This service will go through the given `rates` and make live the ones that
  # have the `type` passed on initalization. Existing live rates will be marked
  # as non-live.
  #
  # No rate will be saved unless all can be saved.
  class UpdateLive

    include SwoopService

    attr_reader :rates, :type

    def initialize(rates:, type:)
      @rates = rates
      @type = type
      @from_rate = nil
      @to_rate = nil
    end

    def execute
      rates.each do |rate|
        Rails.logger.debug("dealing with #{rate}")

        make_nonlive(rate) if rate.live
        make_live(rate) if rate.type == type

        rate.no_invoice_updates = true
        rate.save!
      end
    end

    def after_execution
      return unless @from_rate

      Delayed::Job.enqueue RecalculateLiveRatesBatch.new(@from_rate.id, @to_rate.id)
    end

    private

    def make_live(rate)
      rate.live = true
      @to_rate = rate
    end

    def make_nonlive(rate)
      @from_rate = rate
      rate.live = false
    end

  end
end
