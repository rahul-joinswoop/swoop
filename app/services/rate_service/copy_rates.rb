# frozen_string_literal: true

module RateService
  # This service creates new rates based on the properties of the ones passed on
  # initialization and new attributes that will override the ones of the passed
  # rates.
  #
  # New rates will not get saved if live or non-live ones exist based on the new
  # merged attributes.
  class CopyRates

    include SwoopService

    attr_reader :rates, :new_rates, :company, :new_attributes

    def initialize(rates:, company:, new_attributes:)
      @rates = rates
      @company = company
      @new_rates = []
      @new_attributes = new_attributes
    end

    def execute
      rates.each do |rate|
        rate = Rate.new(rate.attributes.merge(new_attributes))
        rate.created_at = nil
        rate.updated_at = nil
        rate.id = nil
        rate.no_invoice_updates = true
        rate.no_mail = true

        next if non_live_rate_exists?(rate)
        next if live_rate_exists?(rate)

        rate.save!
        @new_rates << rate
      end
    end

    private

    def non_live_rate_exists?(rate)
      non_live = {
        account: rate.account,
        site: rate.site,
        service_code: rate.service_code,
        vendor_id: rate.vendor_id,
        type: rate.type,
        live: false,
      }.merge(new_attributes)

      exists = company.rates.exists?(non_live)
      Rails.logger.debug "found existing non-live rate: #{non_live}" if exists
      exists
    end

    def live_rate_exists?(rate)
      live = {
        account: rate.account,
        site: rate.site,
        vendor_id: rate.vendor_id,
        service_code: rate.service_code,
        type: rate.type,
        live: true,
        vehicle_category_id: rate.vehicle_category_id,
      }.merge(new_attributes)

      exists = company.rates.exists?(live)
      Rails.logger.debug "found existing live rate for #{live}" if exists
      exists
    end

  end
end
