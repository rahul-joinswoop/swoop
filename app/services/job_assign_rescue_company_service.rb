# frozen_string_literal: true

class JobAssignRescueCompanyService < Service

  include LoggableJob

  attr_reader :job, :call_result
  attr_reader :site
  attr_reader :rescue_company

  def initialize(job, site)
    @job = job
    @site = site
    @rescue_company = site.company
    @is_assigning_driver = false
    @call_result = nil # true if call was executed, maybe refactor this into method
    raise ArgumentError, log_msg('received nil job') unless @job
    raise ArgumentError, log_msg('received nil site on', job: job.id) unless @site
    raise ArgumentError, log_msg('received site with no rescue_company associated on job', site: @site.id, job: job.id) unless @rescue_company
  end

  def account
    @job.account
  end

  def service_code
    @job.service_code
  end

  def logger
    Rails.logger
  end

  def call
    logger.debug "JobAssignRescueCompanyService #{job.id} START #{job.fleet_company.try(:id)} #{job.rescue_company.try(:id)}"

    assignment = setup_assignment

    # TODO remove when switched over to assignments
    deprecated_job_attributes(assignment)

    assign_account
    set_partner_department
    assign_driver_for_fleet_in_house_job
    auto_assign_driver # TODO check comment in the method: looks like it's not being used and should be removed
    call_live_company
    finish_assign
  end

  private

  def setup_assignment
    partner_live = @job.partner_live?
    partner_open = @site.present? ? @site.open_for_business? : false

    @job.assignments.build(partner: @rescue_company,
                           site: @site,
                           partner_live: partner_live,
                           partner_open: partner_open,)
    # TODO fleet_manual: !job.partner_live_and_open?
  end

  def deprecated_job_attributes(assignment)
    @job.site = assignment.site
    @job.rescue_company = assignment.partner

    job.fleet_manual = !job.partner_live_and_open?
    job.partner_live = job.partner_live?
    job.partner_open = site.present? ? site.open_for_business? : false
  end

  def assign_account
    # assign (and create) account
    unless account
      if job.fleet_in_house_job? || job.fleet_motor_club_job?
        Rails.logger.debug "JobAssignRescueCompanyService #{job.id}: inhouse|motorclub"
        job.account = CreateAccountForJobService.new(job, job.fleet_company).call

      elsif @job.fleet_managed_job?
        Rails.logger.debug "JobAssignRescueCompanyService #{job.id}: managed"
        job.account = CreateAccountForJobService.new(job, Company.swoop).call

      else
        raise ArgumentError, "Unknown job type #{job.inspect}"
      end
    end
  end

  def assign_driver_for_fleet_in_house_job
    return unless @job.fleet_in_house_job? && @rescue_company.has_feature?(Feature::PARTNER_DISPATCH_TO_TRUCK)
    return if @job.scheduled? || @rescue_company.rescue_drivers.empty?

    @job.rescue_driver = @rescue_company.rescue_drivers.first
  end

  def auto_assign_driver
    if rescue_company.has_feature(Feature::AUTO_ASSIGN_DRIVER)
      # TODO Are these attributes ever set? The only place I see it is on specs. Should we remove this logic?
      # follow up ticket: https://swoopme.atlassian.net/browse/ENG-8603
      if rescue_company.auto_driver_id && rescue_company.auto_truck_id
        @is_assigning_driver = true
        job.rescue_driver  = rescue_company.auto_driver
        job.rescue_vehicle = rescue_company.auto_truck
      end
    end
  end

  def set_partner_department
    Rails.logger.debug "set_partner_department called"
    if rescue_company.has_feature(Feature::DEPARTMENTS)
      if account.present?
        Rails.logger.debug "set_partner_department setting partner_department"
        job.partner_department = account.department
      else
        Rails.logger.debug "set_partner_department Account not set"
      end
    end
  end

  def call_live_company
    if rescue_company.live
      if job.site_or_rescue_company_phone
        log :debug, "Queuing phone alert for #{rescue_company.name}(#{rescue_company.id});  PO:#{job.po_number}, name:#{job.customer.full_name}, phone:#{job.site_or_rescue_company_phone}", alerts: @site.id, job: job.id
        log :debug, "Auction #{job.auction.try(:id)} #{job.auction.try(:status)}", alerts: @site.id

        if job.auction && (job.auction.status == Auction::Auction::SUCCESSFUL)
          logger.debug "ALERTS Auction running leaving for Auction system to alert company"
        else
          logger.debug "ALERTS normal alert"
          Job.call_with_new_job_if_allowed(job.rescue_company, job.id, job.site_id, job.site_or_rescue_company_phone)
        end

        log :debug, "Queuing reminder phone alert for #{rescue_company.name}(#{rescue_company.id}); PO:#{job.po_number}, name:#{job.customer.full_name}", alerts: job.id, job: job.id

        Job.delay(run_at: 4.minutes.from_now).reminder_call(job.id)

        @call_result = true
      else
        msg = log_msg "No Rescue Company phone for #{rescue_company.name}(#{rescue_company.id}), couldn't alert", alerts: job.id
        logger.error msg
        Rollbar.error(StandardError.new(msg))

        @call_result = false
      end
    end
  end

  def finish_assign
    # kick off Estimate
    job.company_assigned = true
    job.status = Job::DISPATCHED if @is_assigning_driver
  end

end
