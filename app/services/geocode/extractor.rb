# frozen_string_literal: true

module Geocode
  class Extractor

    def initialize(gecoder_results)
      @results = gecoder_results
      @first = @results[0]
    end

    def call
      {
        state: state_code,
        street: street,
        street_number: street_number,
        street_name: street_name,
        city: city,
        zip: zip,
        country: country,
        lat: lat,
        lng: lng,
        address: address,
      }
    end

    def self.short_route(result)
      result.address_components_of_type(:route).first&.dig('short_name')
    end

    private

    def address
      @first.address
    end

    def state_code
      @first.state_code
    end

    def street
      components = [@first.street_number, Extractor.short_route(@first)].compact
      return nil if components.length == 0
      components.join(' ')
    end

    def street_number
      @first.street_number
    end

    def street_name
      Extractor.short_route(@first)
    end

    def city
      @first.city
    end

    def zip
      return @first.postal_code if @first.postal_code
      result = find_type('postal_code')
      result.postal_code if result
    end

    def country
      @first.country_code
    end

    def lat
      @first.coordinates[0]
    end

    def lng
      @first.coordinates[1]
    end

    # e.g. 'street_address'
    def find_type(name)
      @results.select { |el| el.types.include?(name) }.first
    end

  end
end
