# frozen_string_literal: true

module Geocode
  class LatLngSearch

    def initialize(lat, lng)
      raise ArgumentError, 'Lat out of range' unless lat.between?(-90, 90)
      raise ArgumentError, 'Lng out of range' unless lng.between?(-180, 180)
      @lat = lat
      @lng = lng
    end

    def call
      results = Geocoder.search("#{@lat},#{@lng}")
      return {} if results.size == 0
      Geocode::Extractor.new(results).call
    end

  end
end
