# frozen_string_literal: true

class UpdateLastUpdatedStatusOnJob < Service

  # This is meant to set last_status_changed_at on a job to be the latest time of any of the statuses on a job
  # BUG: If the latest status is moved to before a different status it will continue using that instead of the later status
  def initialize(job, ajs, previous)
    @job = job
    @ajs = ajs
    @previous = previous
  end

  def call
    return if @ajs.job_status_deleted?

    if @ajs.adjusted_dttm
      if @job.last_status_changed_at.nil? || (@ajs.adjusted_dttm > @job.last_status_changed_at) || (@previous && (@job.last_status_changed_at == @previous))
        @job.last_status_changed_at = @ajs.adjusted_dttm
      end
    else
      if @job.last_status_changed_at.nil? || (@ajs.last_set_dttm > @job.last_status_changed_at) || (@previous && (@job.last_status_changed_at == @previous))
        @job.last_status_changed_at = @ajs.last_set_dttm
      end
    end
  end

end
