# frozen_string_literal: true

module StripeIntegration
  class InvalidRequestError

    FIELD_MAP = {
      "legal_entity[address][city]" => :address_city,
      "legal_entity[address][line1]" => :address_line1,
      "legal_entity[address][postal_code]" => :address_postal_code,
      "legal_entity[address][state]" => :address_state,
      "legal_entity[business_name]" => :business_name,
      "legal_entity[business_tax_id]" => :business_tax_id,
      "legal_entity[dob][day]" => :dob_day,
      "legal_entity[dob][month]" => :dob_month,
      "legal_entity[dob][year]" => :dob_year,
      "legal_entity[first_name]" => :dob_year,
      "legal_entity[last_name]" => :dob_year,
      "legal_entity[personal_address][city]" => :personal_address_city,
      "legal_entity[personal_address][line1]" => :personal_address_line1,
      "legal_entity[personal_address][postal_code]" => :personal_address_postal_code,
      "legal_entity[personal_address][state]" => :personal_address_state,
      "legal_entity[ssn_last_4]" => :ssn_last_4,
    }.freeze

    def initialize(invalid_request_error:)
      body = invalid_request_error.json_body[:error]
      param = body[:param]
      @field = FIELD_MAP[param] || param
      @message = body[:message]
    end

    def to_json(params)
      { @field => [@message] }.to_json
    end

  end
end
