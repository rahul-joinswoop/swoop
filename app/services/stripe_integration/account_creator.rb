# frozen_string_literal: true

module StripeIntegration
  class AccountCreator

    DEFAULT_WEEK_DAY = 'friday'
    DEFAULT_MONTH_DAY = 1

    def initialize(
      address_city:,
      address_line1:,
      address_postal_code:,
      address_state:,
      business_name:,
      business_tax_id:,
      dob_day:,
      dob_month:,
      dob_year:,
      first_name:,
      last_name:,
      personal_address_city:,
      personal_address_line1:,
      personal_address_postal_code:,
      personal_address_state:,
      request_ip:,
      ssn_last_4:,
      api_company:
    )
      @address_city = address_city
      @address_line1 = address_line1
      @address_postal_code = address_postal_code
      @address_state = address_state
      @business_name = business_name
      @business_tax_id = business_tax_id
      @dob_day = dob_day
      @dob_month = dob_month
      @dob_year = dob_year
      @first_name = first_name
      @last_name = last_name
      @personal_address_city = personal_address_city
      @personal_address_line1 = personal_address_line1
      @personal_address_postal_code = personal_address_postal_code
      @personal_address_state = personal_address_state
      @request_ip = request_ip
      @ssn_last_4 = ssn_last_4

      @payout_interval = api_company.invoice_charge_fee.payout_interval
      @payout_schedule = {
        interval: @payout_interval,
      }

      add_interval_anchor_to_payout_schedule
    end

    def create
      Stripe::Account.create(attributes)
    end

    private

    def attributes
      {
        type: 'custom',
        country: 'US',
        legal_entity: {
          address: {
            city: @address_city,
            country: 'US',
            line1: @address_line1,
            postal_code: @address_postal_code,
            state: @address_state,
          },
          business_name: @business_name,
          business_tax_id: @business_tax_id,
          dob: {
            day: @dob_day,
            month: @dob_month,
            year: @dob_year,
          },
          first_name: @first_name,
          last_name: @last_name,
          personal_address: {
            city: @personal_address_city,
            country: 'US',
            line1: @personal_address_line1,
            postal_code: @personal_address_postal_code,
            state: @personal_address_state,
          },
          ssn_last_4: @ssn_last_4,
          type: "company",
        },
        payout_schedule: @payout_schedule,
        tos_acceptance: {
          date: Time.now.utc.to_i,
          ip: @request_ip,
        },
      }
    end

    def add_interval_anchor_to_payout_schedule
      case @payout_interval
      when CompanyInvoiceChargeFee::PAYOUT_INTERVAL_WEEKLY
        @payout_schedule[:weekly_anchor] = DEFAULT_WEEK_DAY
      when CompanyInvoiceChargeFee::PAYOUT_INTERVAL_MONTHLY
        @payout_schedule[:monthly_anchor] = DEFAULT_MONTH_DAY
      end
    end

  end
end
