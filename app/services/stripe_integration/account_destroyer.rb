# frozen_string_literal: true

module StripeIntegration
  class AccountDestroyer

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "StripeIntegration::AccountDestoyer called #{context}"

      @custom_account = context.input[:custom_account]

      if @custom_account.blank?
        raise ArgumentError, "CustomAccountServices::DeleteByCompanyId @custom_account is blank"
      end
    end

    def call
      account = Stripe::Account.retrieve(@custom_account.upstream_account_id)
      stripe_response = account.delete

      Rails.logger.debug(
        "StripeIntegration::AccountDestoyer account deleted, stripe_response: #{stripe_response}"
      )

      context.output = stripe_response
    end

  end
end
