# frozen_string_literal: true

module StripeIntegration
  class BankAccountRetriever

    def initialize(stripe_account_id:)
      @stripe_account_id = stripe_account_id
    end

    def retrieve
      retriever = StripeIntegration::AccountRetriever.new({
        stripe_account_id: @stripe_account_id,
      })
      stripe_account = retriever.retrieve
      accts = stripe_account.external_accounts.all({
        object: "bank_account",
        limit: 1,
      })
      acct = accts.first
      acct
    end

  end
end
