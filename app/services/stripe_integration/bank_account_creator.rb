# frozen_string_literal: true

module StripeIntegration
  class BankAccountCreator

    def initialize(stripe_account_id:, token:)
      @stripe_account_id = stripe_account_id
      @token = token
    end

    def create
      retriever = StripeIntegration::AccountRetriever.new({
        stripe_account_id: @stripe_account_id,
      })
      stripe_account = retriever.retrieve
      # rubocop:disable Rails/SaveBang
      stripe_account.external_accounts.create({
        external_account: @token,
        default_for_currency: true,
      })
      # rubocop:enable Rails/SaveBang
    end

  end
end
