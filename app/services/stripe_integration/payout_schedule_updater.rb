# frozen_string_literal: true

module StripeIntegration
  class PayoutScheduleUpdater

    include Interactor
    include Interactor::Swoop

    DEFAULT_DELAY_DAYS = 2
    DEFAULT_WEEK_DAY = 'friday'
    DEFAULT_MONTH_DAY = 1

    before do
      @custom_account_id = context.input[:custom_account_id]
      @delay_days = context.input[:delay_days] || DEFAULT_DELAY_DAYS
      @payout_interval = context.input[:payout_interval]

      raise ArgumentError, 'StripeIntegration::PayoutScheduleUpdater custom_account_id blank' if
        @custom_account_id.blank?

      raise ArgumentError, 'StripeIntegration::PayoutScheduleUpdater delay_days blank' if
        @delay_days.blank?

      raise ArgumentError, 'StripeIntegration::PayoutScheduleUpdater payout_interval blank' if
        @payout_interval.blank?
    end

    def call
      stripe_account_id = CustomAccount.find(@custom_account_id).upstream_account_id

      Rails.logger.debug(
        "StripeIntegration::PayoutScheduleUpdater - about to retrieve account for " \
        "stripe_account_id #{stripe_account_id}"
      )
      account_retriever = StripeIntegration::AccountRetriever.new({
        stripe_account_id: stripe_account_id,
      })

      @stripe_account = account_retriever.retrieve

      @stripe_account.payout_schedule.delay_days = @delay_days
      @stripe_account.payout_schedule.interval = @payout_interval

      add_interval_anchor_to_stripe_account

      # rubocop:disable Rails/SaveBang
      @stripe_account.save
      # rubocop:enable Rails/SaveBang

      Rails.logger.debug(
        "StripeIntegration::PayoutScheduleUpdater - payout_schedule updated successfully for " \
        "stripe_account_id #{stripe_account_id}"
      )

      context.output = @stripe_account
    end

    private

    def add_interval_anchor_to_stripe_account
      case @payout_interval
      when CompanyInvoiceChargeFee::PAYOUT_INTERVAL_WEEKLY
        @stripe_account.payout_schedule.weekly_anchor = DEFAULT_WEEK_DAY
      when CompanyInvoiceChargeFee::PAYOUT_INTERVAL_MONTHLY
        @stripe_account.payout_schedule.monthly_anchor = DEFAULT_MONTH_DAY
      end
    end

  end
end
