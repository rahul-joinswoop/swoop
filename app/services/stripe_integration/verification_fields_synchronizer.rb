# frozen_string_literal: true

module StripeIntegration
  class VerificationFieldsSynchronizer

    SYNC_DELAY = 5.minutes

    def initialize(stripe_account_id)
      @stripe_account_id = stripe_account_id

      Rails.logger.debug(
        "StripeIntegration::VerificationFieldsSynchronizer initialized with " \
        "stripe_account_id(#{@stripe_account_id})"
      )
    end

    def self.enqueue(stripe_account_id)
      run_at = Time.zone.now + SYNC_DELAY
      Delayed::Job.enqueue(new(stripe_account_id), run_at: run_at)
    end

    def perform
      custom_account = CustomAccount.not_deleted.find_by(
        upstream_account_id: @stripe_account_id,
      )

      if custom_account.blank?
        Rails.logger.debug(
          "StripeIntegration::VerificationFieldsSynchronizer there is no active " \
          "custom_account for upstream_account_id: #{@stripe_account_id}"
        )

        return
      end

      retriever = StripeIntegration::AccountRetriever.new({
        stripe_account_id: @stripe_account_id,
      })
      stripe_account = retriever.retrieve
      verification_fields = stripe_account.verification.fields_needed

      custom_account.sync_verification_fields(verification_fields)

      if custom_account.verification_failure?
        email = EmailStripeVerificationFields.new(custom_account.id)
        Delayed::Job.enqueue(email)
      end
    end

  end
end
