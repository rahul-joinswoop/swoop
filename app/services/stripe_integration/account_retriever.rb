# frozen_string_literal: true

module StripeIntegration
  class AccountRetriever

    def initialize(stripe_account_id:)
      @stripe_account_id = stripe_account_id
    end

    def retrieve
      Stripe::Account.retrieve(@stripe_account_id)
    end

  end
end
