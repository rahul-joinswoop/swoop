# frozen_string_literal: true

module StripeIntegration
  module EventHandlers
    class ChargeDisputeCreated < Base

      STRIPE_EVENT = CHARGE_DISPUTE_CREATED

      protected

      def run!
        Rails.logger.info("StripeIntegration::EventHandlers::DisputeCreated about to email it")

        if invoice_payment_charge
          # alert through email
          email = Stripe::EmailChargeDisputeCreated.new(invoice_payment_charge.id)

          Delayed::Job.enqueue(email)
        else
          Rails.logger.info(
            "StripeIntegration::EventHandlers::DisputeCreated no invoice_payment_charge found" \
            "for CHARGE_ID(#{charge_id})"
          )
        end
      end

      private

      def invoice_payment_charge
        @invoice_payment_charge ||= InvoicePaymentCharge.find_by(
          upstream_charge_id: charge_id
        )
      end

      def charge_id
        @charge_id ||= @event.data.object.charge
      end

    end
  end
end
