# frozen_string_literal: true

module StripeIntegration
  module EventHandlers
    class RefundUpdated < Base

      include LoggableJob

      STRIPE_EVENT = REFUND_UPDATED

      protected

      def run!
        Rails.logger.debug(
          "StripeIntegration::EventHandlers::RefundUpdated - run! " \
          "with @event.data.object: #{@event.data.object}"
        )

        # for now we only treat Refund failed transactions,
        # and the only possible way to do it is by checking if:
        if @event.data.object.respond_to? :failure_balance_transaction
          job = Job.find(@event.data.object.metadata[:job_id].to_i)

          log :info,
              "refund failed, " \
              "about to email it",
              job: job.id

          job = Job.find(@event.data.object.metadata[:job_id].to_i)

          # alert through email
          email = Stripe::EmailRefundFailed.new(
            job.rescue_company_id,
            job.id,
            @event.data.object.created
          )
          Delayed::Job.enqueue(email)
        end
      end

    end
  end
end
