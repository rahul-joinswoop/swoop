# frozen_string_literal: true

# This handler fetches all Charges on Stripe end associated
# with the Stripe Payout that has been paid to the CustomAccount Bank Account.
#
# With the charge_ids we can associate the Payout info with the respective
# invoice_payment_charge rows in Swoop DB.
module StripeIntegration
  module EventHandlers
    class PayoutPaid < Base

      STRIPE_EVENT = PAYOUT_PAID

      protected

      # It rescues from Stripe::InvalidRequestError that will eventually happen when
      # someone does a manual payout on Stripe side, as manual payouts do not generate
      # transaction, since there will be no charges associated with it (Sripe is designed this way).
      #
      # In this case, we log just it as info.
      def run!
        Rails.logger.debug(
          "StripeIntegration::EventHandlers::PayoutPaid - run! " \
          "with @event.data.object: #{@event.data.object}"
        )

        update_charges_with_payout_date!
      rescue Stripe::InvalidRequestError => e
        Rails.logger.info(
          "StripeIntegration::EventHandlers::PayoutPaid - Stripe returned the following error: " \
          "#{e}"
        )
      end

      private

      def update_charges_with_payout_date!
        invoice_payment_charges = InvoicePaymentCharge.where(upstream_charge_id: charge_ids)

        Rails.logger.debug(
          "StripeIntegration::EventHandlers::PayoutPaid - charge_ids(#{charge_ids}) " \
          "fetched on Stripe end that are associated with Payout('#{payout.id}')"
        )

        Rails.logger.debug(
          "StripeIntegration::EventHandlers::PayoutPaid - " \
          "invoice_payment_charges(#{invoice_payment_charges.map(&:id)})" \
          "on Swoop DB for the charge_ids(#{charge_ids})"
        )

        invoice_payment_charges.update_all(
          upstream_payout_id: payout.id,
          upstream_payout_created_at: stripe_datetime(payout.created)
        )
      end

      # 'payment_transaction.source.source_transfer.source_transaction'
      # will give us the charge_id.
      def charge_ids
        @charge_ids ||= only_payment_transactions.map do |payment_transaction|
          payment_transaction.source.source_transfer.source_transaction
        end
      end

      def only_payment_transactions
        @only_payment_transactions ||= balance_transaction_list.select do |transaction|
          transaction.source.id.match /^py_/ # will match only payments
        end
      end

      # It fetches all balance transactions involved in the Payout.
      #
      # We pass it the expand: ['data.source.source_transfer'] param.
      #
      # This param will force the response to come with all the nested objects
      # needed until reaching the charge_ids involved in the Payout.
      #
      # (then the charge_id can be resolved by 'source.source_transfer.source_transaction')
      def balance_transaction_list
        @balance_transaction_list ||= Stripe::BalanceTransaction.list(
          {
            payout: payout.id,
            expand: ['data.source.source_transfer'],
          },
          stripe_account: stripe_custom_account
        ).data
      end

      def payout
        @payout ||= @event.data.object
      end

      def stripe_custom_account
        @stripe_custom_account ||= @event.account
      end

    end
  end
end
