# frozen_string_literal: true

module StripeIntegration
  module EventHandlers
    class PaymentCreated < Base

      STRIPE_EVENT = PAYMENT_CREATED

      protected

      def run!
        Rails.logger.debug(
          "StripeIntegration::EventHandlers::PaymentCreated - run! " \
          "with @event.data.object: #{@event.data.object}"
        )

        add_payment_id_to_charge!
      end

      private

      def add_payment_id_to_charge!
        charge = InvoicePaymentCharge.find_by!(upstream_charge_id: upstream_charge_id)

        charge.update!(upstream_payment_id: upstream_payment_id)
      end

      def upstream_charge_id
        @upstream_charge_id ||= transfer.source_transaction
      end

      def transfer
        @transfer ||= Stripe::Transfer.retrieve(source_transfer)
      end

      def source_transfer
        @source_transfer ||= @event.data.object.source_transfer
      end

      def upstream_payment_id
        @upstream_payment_id ||= @event.data.object.id
      end

    end
  end
end
