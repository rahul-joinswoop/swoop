# frozen_string_literal: true

module StripeIntegration
  module EventHandlers
    class PayoutFailed < Base

      STRIPE_EVENT = PAYOUT_FAILED

      protected

      def run!
        Rails.logger.info("StripeIntegration::EventHandlers::PayoutFailed about to email it")

        custom_account = CustomAccount.find_by!(upstream_account_id: @event.account)

        # alert through email
        email = EmailStripePayoutFailed.new(custom_account.company_id)
        Delayed::Job.enqueue(email)
      end

    end
  end
end
