# frozen_string_literal: true

module StripeIntegration
  module EventHandlers
    class AccountUpdated < Base

      STRIPE_EVENT = ACCOUNT_UPDATED

      protected

      def run!
        stripe_account = @event.data.object

        StripeIntegration::VerificationFieldsSynchronizer.enqueue(
          stripe_account.id
        )
      end

    end
  end
end
