# frozen_string_literal: true

# handler for Not Handled events.
# For now it just logs it.
module StripeIntegration
  module EventHandlers
    class NotHandled < Base

      def handle
        Rails.logger.debug(
          "StripeIntegration::EventHandler event not treated by Swoop: #{@event.type}"
        )
      end

    end
  end
end
