# frozen_string_literal: true

module StripeIntegration
  module EventHandlers
    class Base

      # Connect webhook events
      ACCOUNT_UPDATED = 'account.updated' # Connect
      PAYMENT_CREATED = 'payment.created' # Connect
      PAYOUT_FAILED = 'payout.failed' # Connect
      PAYOUT_PAID = 'payout.paid' # Connect

      # Account webhook events
      CHARGE_DISPUTE_CREATED = 'charge.dispute.created' # Account
      REFUND_UPDATED = 'charge.refund.updated' # Account

      CONNECT_EVENTS = {
        ACCOUNT_UPDATED => 'AccountUpdated',
        PAYMENT_CREATED => 'PaymentCreated',
        PAYOUT_FAILED => 'PayoutFailed',
        PAYOUT_PAID => 'PayoutPaid',
      }.freeze

      ACCOUNT_EVENTS = {
        CHARGE_DISPUTE_CREATED => 'ChargeDisputeCreated',
        REFUND_UPDATED => 'RefundUpdated',
      }.freeze

      class << self

        def find_connect_handler_by(payload:, sig_header:, webhook_secret:)
          find_handler_by(
            payload: payload,
            sig_header: sig_header,
            webhook_secret: webhook_secret,
            webhook_type_event_hash: CONNECT_EVENTS
          )
        end

        def find_account_handler_by(payload:, sig_header:, webhook_secret:)
          find_handler_by(
            payload: payload,
            sig_header: sig_header,
            webhook_secret: webhook_secret,
            webhook_type_event_hash: ACCOUNT_EVENTS
          )
        end

        private

        def find_handler_by(payload:, sig_header:, webhook_secret:, webhook_type_event_hash:)
          event = Stripe::Webhook.construct_event(payload, sig_header, webhook_secret)

          handler_as_string = webhook_type_event_hash[event.type]

          if handler_as_string
            return "StripeIntegration::EventHandlers::#{handler_as_string}".constantize.new(
              event: event
            )
          end

          StripeIntegration::EventHandlers::NotHandled.new(event: event)
        end

      end

      def initialize(event:)
        @event = event

        Rails.logger.debug "#{self.class.name} - initialized with @event: #{@event}"
      end

      def handle
        return unless livemode?
        return unless allowed_to_handle_event?

        run!
      end

      protected

      def run!
        raise NotImplementedError, "Subclasses must implement this method"
      end

      private

      def livemode?
        ENV['STRIPE_DEBUG_WEBHOOK'] || @event.livemode
      end

      # STRIPE_EVENT constant must be declared by children EventHandlers
      def allowed_to_handle_event?
        @event.type == self.class::STRIPE_EVENT
      end

      def stripe_datetime(seconds_from_epoch)
        Time.at(seconds_from_epoch).utc.to_datetime
      end

    end
  end
end
