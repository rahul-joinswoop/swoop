# frozen_string_literal: true

module Looker
  class FetchDashboards

    def initialize
    end

    def lookersdk
      if ENV['LOOKER_API_CLIENTID']
        LookerSDK::Client.new(
          client_id: ENV['LOOKER_API_CLIENTID'],
          client_secret: ENV['LOOKER_API_SECRET'],
          api_endpoint: "https://swoopme.looker.com:19999/api/3.0"
        )
      end
    end

    def call
      sdk = lookersdk
      if sdk
        LookerSpace.all.each do |space|
          ret = sdk.search_dashboards(space_id: space.spaceid)
          ret.each do |dash|
            existing = LookerDashboard.find_by(dashboardid: dash.id)
            if existing
              existing.update!(name: dash.title)
            else
              LookerDashboard.create!(name: dash.title, dashboardid: dash.id)
            end
          end
        end
      end
    end

  end
end
