# frozen_string_literal: true

# This module provides a namespace for integrations with digital dispatching providers
# like ISSC and Agero.
module DigitalDispatch

  # Available strategies (implemented by workers) that a Digital Dispatch
  # flow can use for status changes.
  STRATEGIES = {
    issc: {
      auto_accept: IsscAutoAcceptJob,
      accept: IsscDispatchResponseAccept,
      reject: IsscDispatchResponseReject,
      change_status: IsscDispatchStatus,
      cancel: IsscDispatchStatus,
      assign_driver: NoOpWorker,
      dispatchable_sites: ISSC::UpdateDispatchableSitesStrategy,
      request_call_back: ISSC::CallbackJob,
    },
    rsc: {
      accept: Agero::Rsc::AcceptDispatchWorker,
      reject: Agero::Rsc::RejectDispatchWorker,
      change_status: Agero::Rsc::ChangeDispatchStatusWorker,
      cancel: Agero::Rsc::CancelDispatchWorker,
      assign_driver: Agero::Rsc::AssignDriverWorker,
      dispatchable_sites: Agero::Rsc::DispatchableSites::Update,
      request_call_back: Agero::Rsc::RequestCallBack,
    },
  }.with_indifferent_access.freeze

  # Return an available strategy that a Digital Dispatch
  # flow can use for digital dispatch changes.
  #
  # e.g DigitalDispatch.strategy_for(job_digital_dispatcher: :rsc, action: :accept)
  def self.strategy_for(job_digital_dispatcher:, action:)
    STRATEGIES.dig(job_digital_dispatcher, action) ||
      raise(ArgumentError, "Strategy not found for [#{job_digital_dispatcher}][#{action}]")
  end

end
