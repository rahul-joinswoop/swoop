# frozen_string_literal: true

class JwtManager

  def self.generate_claims(sub, expires_in)
    @sub = sub.to_s
    @expires_in = expires_in
    @iat = Time.zone.now.to_i
    @exp = @iat + @expires_in
    nonce = (Time.zone.now.to_f * 1000).to_i
    jti_raw = [ENV['JWT_SECRET'], nonce].join(':').to_s
    @jti = Digest::MD5.hexdigest(jti_raw)
    {
      iat: @iat,
      exp: @exp,
      jti: @jti,
      sub: @sub,
    }
  end

  def self.encode(claims)
    JWT.encode(claims, ENV['JWT_SECRET'])
  end

  def self.decode(token)
    JWT.decode(token, ENV['JWT_SECRET'], true, {
      exp_leeway: 10,
      iat_leeway: 10,
      verify_iat: true,
      verify_jti: true,
    })
  end

  def self.verify(token)
    decode(token)
  end

end
