# frozen_string_literal: true

module WsEventLog

  DEFAULT_MAX_EVENTS = 100

  def self.max_events
    (ENV['MAX_WS_EVENTS'] || DEFAULT_MAX_EVENTS).to_i
  end

  def self.handle_error(klass, command, e)
    Rails.logger.error "REDIS: WsEventLog::#{klass}: #{command}: #{e}"
    # Occasionally, Eventmachine is handing us back an error with a nil backtrace, so in these cases,
    # return an informational message
    Rails.logger.error(e.backtrace.try(:join, "\n") || "#{e.class.name} - No backtrace found. Check Rollbar?")
    Rollbar.error(e)
  end

  def self.generate_key(target_type:, target_id:)
    "#{target_type}-#{target_id}"
  end

  def self.generate_score(time)
    time = Time.parse(time) unless time.is_a?(Time)
    (time.to_f * 1000).to_i
  end

end
