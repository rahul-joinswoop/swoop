# frozen_string_literal: true

# This will return a hash with expect User Agent values like browser, platform, version and build.
# The hash will also contain two custom fields:
#   - swoop_custom_platform: it can be:
#       - mobile platform: React iOS or React Android
#       - web: Web
#       - GraphQL API: API
#
#   - client_or_swoop_version: it will be either the mobile version sent by the mobile device
#     or current Swoop version (to cover the case when FE sends the request)
#
# These custom fields are primarily used by AnalyticsData::JobStatus.
#
# The logic for Web currently covers it for MacOS or Windows OS. And it's ok for now.
class UserAgentParser

  ANDROID = 'android'
  BROKEN = 'broken'
  API = 'API'
  IOS = 'ios'
  MAC = 'mac'
  REACT_IOS = 'React iOS'
  REACT_ANDROID = 'React Android'
  WEB = 'Web'
  WINDOWS = 'windows'

  def initialize(user_agent, platform: nil)
    @user_agent = user_agent
    @platform = platform
  end

  def call
    browser = Browser.new(@user_agent)
    platform = browser.platform
    version = browser.platform.subject.try(:swoop?) ? platform.version : browser.version
    build = browser.platform.subject.try(:build)

    if browser.platform.other? && @user_agent.include?("Swoop")
      if @user_agent.include? "okhttp"
        platform = ANDROID
      else
        platform, version, bstring = @user_agent.split(' ')
        version&.slice!('Swoop/')
        if bstring
          build = bstring[1..-2]
        end
      end
    else
      platform = platform.id.to_s
    end

    ret = {
      platform: platform,
      swoop_custom_platform: swoop_custom_platform(platform, version),
      browser: browser.id,
      version: version || browser.version,
      build: build,
      client_or_swoop_version: client_or_swoop_version(platform, version),
    }
    ret
  end

  def swoop_custom_platform(platform, version)
    return version if version == BROKEN

    case platform
    when IOS
      REACT_IOS
    when ANDROID
      REACT_ANDROID
    when MAC
      api_or_web
    when WINDOWS
      api_or_web
    when BROKEN
      BROKEN
    else
      API
    end
  end

  def client_or_swoop_version(platform, version)
    if platform == IOS || platform == ANDROID || version == BROKEN
      return version
    end

    Swoop::VERSION
  end

  def api_or_web
    return API if @platform&.to_sym == :api

    WEB
  end

end

#
# ios react
#
# "ios  Swoop/1.03.520"
# "ios  Swoop/1.15.33.265 (086e96f7d)"
# "ios  Swoop/1.21.27.309 (3b211ea69)"
# "ios  Swoop/1.25.13.349 (a50616b31)"
#
# react android
# "android  Swoop/41.292 (ce09a0d8b)"
# "android  Swoop/52.308 (a71f158a9)"
# "android  Swoop/1.23.331 (bcfa1332e)"
# "android  Swoop/1.25.349 (a50616b31)" (latest)
#
#
#
#
# Old (native android)
# "BROWSER: other android Swoop/2.0.63"
#
#
#
# "BROWSER: other SwoopMobile/9 CFNetwork/808.3 Darwin/16.3.0"
# "BROWSER: other SwoopMobile/9 CFNetwork/811.5.4 Darwin/16.6.0"
# "BROWSER: windows Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"
# "BROWSER: ios Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_2 like Mac OS X) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.0 Mobile/14F89 Safari/602.1"
