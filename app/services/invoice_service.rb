# frozen_string_literal: true

module InvoiceService

  class MissingInvoiceError < StandardError; end
  class ServiceTransitionNotFoundError < StandardError; end
  class InvoiceStateTransitionError < StandardError; end
  class MissingFleetManagedInvoiceError < StandardError; end

  TRANSITION_SERVICES = {
    swoop_undo_approval_partner: InvoiceService::SwoopUndoApprovalPartner,
    swoop_approved_partner: InvoiceService::SwoopApprovedPartner,
    swoop_sent_fleet: InvoiceService::SwoopSentFleet,
    partner_move_back_to_new: InvoiceService::PartnerMoveBackToNew,
    partner_approved: InvoiceService::PartnerApproved,
  }.freeze

  def self.transition_for(event_name)
    raise ArgumentError if event_name.blank?

    TRANSITION_SERVICES[event_name.to_sym] || (raise ServiceTransitionNotFoundError)
  end

end
