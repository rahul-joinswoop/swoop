# frozen_string_literal: true

class TrackEvent

  include Interactor

  before do
    @event = context.event
    @user = context.user
    @target = context.target
    @additional_attrs = context.additional_attrs || {}
  end

  def call
    attrs = {}

    if @user
      attrs[:user_id] = @user.to_ssid
    else
      attrs[:anonymous_id] = SecureRandom.uuid
    end

    attrs.merge!(
      event: @event,
      properties: properties,
    )

    context.result = attrs

    Analytics.track(attrs)
  rescue => e
    Rollbar.error(e)

    Rails.logger.error "TrackEvent #{e}"
    Rails.logger.error e.backtrace.join("\n")

    context.result = { error: e }
  end

  private

  def properties
    @additional_attrs.merge(target_attrs)
  end

  def target_attrs
    if @target.nil?
      {}
    elsif @target.class <= Job
      @target.render_to_hash
    end
  end

end
