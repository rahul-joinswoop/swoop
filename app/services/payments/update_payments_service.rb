# frozen_string_literal: true

module Payments
  class UpdatePaymentsService < Service

    attr_reader :created_payments_or_credits, :updated_payments_or_credits

    def initialize(params, invoice)
      @params = params
      @invoice = invoice
      @created_payments_or_credits = []
      @updated_payments_or_credits = []

      super()
    end

    def call
      Rails.logger.debug "UpdatePaymentsService params:#{@params}, invoice:#{@invoice}"

      @params.map(&:to_h).map(&:deep_symbolize_keys).each do |payment|
        args = {}

        payment_type = payment[:type]

        clz = nil

        if payment_type
          clz = Payment::PAYMENT_TYPES_HUMAN_TO_CLASS[payment_type]

          raise ArgumentError, "Unsupported type: #{payment_type}" unless clz

          args[:type] = clz.name
        end

        args[:total_amount] = payment[:amount] if payment.key?(:amount)
        args[:description] = payment[:memo].blank? ? "" : payment[:memo]
        args[:payment_method] = payment[:payment_method] if payment.key?(:payment_method)

        args[:qb_pending_import] = payment[:qb_pending_import] if payment.key?(:qb_pending_import)

        args[:mark_paid_at] = payment[:mark_paid_at] if payment.key?(:mark_paid_at)
        args[:invoice_id] = @invoice.id
        args[:currency] = @invoice.currency
        args[:sender] = @invoice.sender
        args[:recipient] = @invoice.recipient
        args[:job_id] = @invoice.job_id
        args[:deleted_at] = payment[:deleted_at] if payment.key?(:deleted_at)

        Rails.logger.debug "UpdatePaymentsService args:#{args}"

        if payment[:id]
          payment = InvoicingLedgerItem.find(payment[:id])

          if clz
            # PDW this is needed otherwise when updating from a payment to a refund,
            # the payment validations run and error
            payment = payment.becomes(clz)
          end

          payment.update!(args)

          @updated_payments_or_credits << payment
        else
          args[:qb_imported_at] = payment[:qb_imported_at] if payment.key?(:qb_imported_at)
          args[:status] = 'Received'

          @created_payments_or_credits << @invoice.payments_or_credits.create!(args)
        end
      end

      self
    end

  end
end
