# frozen_string_literal: true

###
# Service for creating a new Logo for the given api_company
#
module CompanyService
  class CreateLogo

    include SwoopService

    attr_reader :api_company

    def initialize(api_company:, api_user:, attached_document_attributes:)
      @api_company = api_company
      @api_user = api_user
      @attached_document_attributes = attached_document_attributes
    end

    def execute
      create_attached_document

      add_logo_url_to_company

      @api_company
    end

    private

    def create_attached_document
      @attached_document =
        AttachedDocument.new(@attached_document_attributes).tap do |document|
          document.user_id = @api_user.id
          document.company_id = @api_company.id

          document.save!
        end

      @attached_document.finalize_document
    end

    def add_logo_url_to_company
      @api_company.logo_url = @attached_document.url

      @api_company.save!
    end

  end
end
