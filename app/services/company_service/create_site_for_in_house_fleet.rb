# frozen_string_literal: true

###
# Service for creating a Site by a InHouseFleet
module CompanyService
  class CreateSiteForInHouseFleet

    include SwoopService

    attr_reader :site_attributes, :location_attributes, :partner_company, :site

    ##
    # @attr rescue_provider -> the rescue_provider to be added in the tag
    # @attr tag_name -> name for the tag (displayed as Network Status)
    def initialize(site_attributes:, location_attributes:, partner_company:)
      @site_attributes      = site_attributes
      @location_attributes  = location_attributes
      @partner_company      = partner_company
      @site = nil
    end

    def execute
      @location = Location.create!(@location_attributes)

      @site = PartnerSite.new(@site_attributes).tap do |site|
        site.company = @partner_company
        site.location = @location
        site.dispatchable = true
      end

      set_office_hours_from_hq_or_9_5_mon_fri

      @site.save!

      self
    end

    private

    def set_office_hours_from_hq_or_9_5_mon_fri
      partner_hq = @partner_company.hq

      if partner_hq&.dispatchable?
        Rails.logger.debug("Company(#{@partner_company.id}) CompanyService::CreateSiteForInHouseFleet#set_office_hours_from_hq_or_9_5_mon_fri " \
          "will set hq office hours to the new site")
        @site.always_open = partner_hq.always_open

        @site.open_time  = partner_hq.open_time
        @site.close_time = partner_hq.close_time

        @site.open_time_sat  = partner_hq.open_time_sat
        @site.close_time_sat = partner_hq.close_time_sat

        @site.open_time_sun  = partner_hq.open_time_sun
        @site.close_time_sun = partner_hq.close_time_sun
      else
        Rails.logger.debug("Company(#{@partner_company.id}) CompanyService::CreateSiteForInHouseFleet#set_office_hours_from_hq_or_9_5_mon_fri " \
          "will set 9-5 mon-fri to the new site")

        @site.open_time  = Time.parse('2000-01-01T09:00:00.000Z')
        @site.close_time = Time.parse('2000-01-01T17:00:00.000Z')
      end
    end

  end
end
