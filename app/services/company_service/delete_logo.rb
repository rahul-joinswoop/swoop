# frozen_string_literal: true

###
# Service for creating a new Logo for the given api_company
#
module CompanyService
  class DeleteLogo

    include SwoopService

    def initialize(api_company:)
      @api_company = api_company
    end

    def execute
      @api_company.logo_url = nil
      @api_company.save!

      @api_company
    end

  end
end
