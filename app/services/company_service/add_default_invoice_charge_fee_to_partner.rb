# frozen_string_literal: true

module CompanyService
  class AddDefaultInvoiceChargeFeeToPartner

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "CompanyService::AddDefaultInvoiceChargeFee called #{context}"

      @company_id = context.input[:company_id]

      if @company_id.blank?
        raise ArgumentError, "CompanyService::AddDefaultInvoiceChargeFee @company_id is null"
      end
    end

    def call
      company = RescueCompany.find(@company_id)

      context.output = CompanyInvoiceChargeFee.create!(
        company: company,
        percentage: Configuration.stripe.invoice_charge_fee_percentage,
        fixed_value: Configuration.stripe.invoice_charge_fee_fixed_value,
        payout_interval: Configuration.stripe.invoice_charge_fee_payout_interval,
        currency: company.currency,
      )
    end

  end
end
