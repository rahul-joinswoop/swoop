# frozen_string_literal: true

module CompanyService
  # Associate Feature objects to a company. Features can be passed either by id
  # with :feature_ids or by name with feature_names:.
  #
  # The #features association can also be reset for a company by passing
  # overwrite_features as an empty Array.
  #
  # If a newly added feature is QUESTIONS, the service to add default questions
  # to a company will also be called.
  class ChangeFeatures

    include SwoopService
    include SwoopServiceComposite

    def initialize(company_id:, feature_ids: [], feature_names: [], overwrite_features: nil)
      @company = Company.find(company_id)

      initial_features = @company.features.to_a

      @company.features = overwrite_features if overwrite_features
      @existing_features = @company.features

      @features_to_add =
        if !feature_ids.empty?
          Feature.where(id: feature_ids).to_a
        elsif !feature_names.empty?
          Feature.where(name: feature_names).to_a
        else
          []
        end

      @removed_features = (initial_features - @features_to_add).to_a
    end

    def execute
      @features_to_add.compact.each do |feature|
        next if @existing_features.include?(feature)

        @company.features << feature

        handle_feature_add!(feature.name)
      end

      @removed_features.each do |feature|
        handle_feature_removal!(feature.name)
      end

      remove_storage_service_from_company_if_needed

      { company: @company }
    end

    private

    def handle_feature_add!(feature_name)
      case feature_name
      when Feature::QUESTIONS
        create_default_questions
      when Feature::STORAGE
        add_storage_service_if_rescue_company
      when Feature::REVIEW_NPS_SURVEYS
        setup_surveys
      when Feature::SHOW_SYMPTOMS
        add_standard_symptoms_to_company_if_needed
      when Feature::ISSUES
        if @company.in_house? && @company.issues.empty?
          add_issues_to_fleet_in_house
        end
      end
    end

    def handle_feature_removal!(feature_name)
    end

    def setup_surveys
      SurveyService::SetupSmiley.new(
        @company.name,
        "#{@company.name} Smiley Survey",
        @company.survey_host,
        Survey::Smiley::NPS_QUESTIONS,
      ).call
    end

    def create_default_questions
      add_and_execute_child_service(CompanyService::CreateDefaultQuestions.new(
        company: @company, service_codes: @company.service_codes
      ))
    end

    def add_storage_service_if_rescue_company
      return if !@company.instance_of? RescueCompany

      storage_service_id =
        ServiceCode.select(:id).find_by(name: ServiceCode::STORAGE).id

      ServiceCodeServices::Change.new(
        api_company: @company,
        standard_services_ids_to_add: [storage_service_id],
        should_publish_company: false
      ).call
    end

    # This method checks if the company doesn't have the Storage FEATURE anymore,
    # and triggers the Storage SERVICE removal in case it still exists.
    def remove_storage_service_from_company_if_needed
      return if @company.features.where(name: Feature::STORAGE).exists?
      return if @company.service_codes.where(name: ServiceCode::STORAGE).blank?

      storage_service_id =
        ServiceCode.select(:id).find_by(name: ServiceCode::STORAGE).id

      ServiceCodeServices::Change.new(
        api_company: @company,
        services_ids_to_remove: [storage_service_id],
        should_publish_company: false
      ).call
    end

    def add_standard_symptoms_to_company_if_needed
      return if !@company.instance_of? RescueCompany

      standard_symptoms = Symptom.standard_items

      standard_symptoms.each do |symptom|
        @company.symptoms << symptom if !@company.symptoms.include? symptom
      end
    end

    def add_issues_to_fleet_in_house
      Company.transaction do
        Issue.standard_items.each do |issue|
          @company.issues << issue unless @company.issues.include?(issue)
        end
      end
    end

  end
end
