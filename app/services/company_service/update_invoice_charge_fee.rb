# frozen_string_literal: true

# It updates the charge_fee info for the given RescueCompany.
#
# If the payout_interval is changed, it needs to be updated on Stripe
# side as well. This task is accomplished async by Stripe::UpdatePayoutScheduleWorker.
module CompanyService
  class UpdateInvoiceChargeFee

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "CompanyService::UpdateInvoiceChargeFee called #{context}"

      @company_id = context.input[:company_id]
      @invoice_charge_fee_attributes = context.input[:invoice_charge_fee_attributes]

      raise ArgumentError, "CompanyService::UpdateInvoiceChargeFee @company_id is null" if
        @company_id.blank?

      if @invoice_charge_fee_attributes.blank?
        raise(
          ArgumentError,
          "CompanyService::UpdateInvoiceChargeFee @invoice_charge_fee_attributes is null"
        )
      end

      all_attrs_blank = [:percentage, :fixed_value, :payout_interval].all? do |k|
        @invoice_charge_fee_attributes[k].blank?
      end

      if all_attrs_blank
        raise(
          ArgumentError,
          "CompanyService::UpdateInvoiceChargeFee percentage, " \
          "fixed_value and payout_interval are null"
        )
      end

      @company = RescueCompany.find(@company_id)
    end

    def call
      invoice_charge_fee = CompanyInvoiceChargeFee.find_by!(company: @company)
      invoice_charge_fee.assign_attributes(@invoice_charge_fee_attributes)

      payout_interval_changed = invoice_charge_fee.payout_interval_changed?

      invoice_charge_fee.save!

      if payout_interval_changed && @company.custom_account
        async_update_payout_interval_on_stripe
      end

      context.output = invoice_charge_fee
    end

    private

    # If fee is successfully updated on the company, we schedule
    # the update on the Stripe side as well.
    #
    # The async worker is setup to try it for 5 times.
    def async_update_payout_interval_on_stripe
      Stripe::UpdatePayoutScheduleWorker.perform_after_commit(
        @company.custom_account.id, @invoice_charge_fee_attributes[:payout_interval]
      )
    end

  end
end
