# frozen_string_literal: true

###
# Reset customer types for the given company.
#
module CompanyService
  class ChangeCustomerTypes

    include SwoopService

    attr_reader :company, :customer_type_ids

    def initialize(company:, customer_type_ids:)
      Rails.logger.debug("CompanyService::ChangeCustomerTypes initialized with company #{company.id}, customer_type_ids #{customer_type_ids}")

      @company = company
      @customer_type_ids = customer_type_ids

      company_current_customer_type_ids = company.customer_types.pluck(:id)

      @customer_types_to_add = customer_type_ids - company_current_customer_type_ids

      @customer_types_to_remove = company_current_customer_type_ids - customer_type_ids

      Rails.logger.debug("CompanyService::ChangeCustomerTypes customer_types_to_add #{@customer_types_to_add}")
      Rails.logger.debug("CompanyService::ChangeCustomerTypes customer_types_to_remove #{@customer_types_to_remove}")
    end

    def execute
      add_customer_types
      remove_customer_types

      self
    end

    private

    def add_customer_types
      @customer_types_to_add.each do |customer_type_id|
        customer_type = CustomerType.find(customer_type_id)

        if customer_type && !@company.customer_types.include?(customer_type)
          @company.customer_types << customer_type
        end
      end
    end

    def remove_customer_types
      @customer_types_to_remove.each do |customer_type_id|
        customer_type = CustomerType.find(customer_type_id)

        if customer_type && @company.customer_types.include?(customer_type)
          @company.customer_types.destroy(customer_type)
        end
      end
    end

  end
end
