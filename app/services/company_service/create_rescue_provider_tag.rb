# frozen_string_literal: true

# # #
#
# Service for creating a RescueProvider between a client company (Swoop or FleetInhouse)
# and a Partner.
module CompanyService

  class RescueProviderNotFoundError < StandardError; end

  class CreateRescueProviderTag

    include SwoopService

    attr_reader :rescue_provider, :tag_name, :rescue_provider_tag

    ##
    # @attr rescue_provider -> the rescue_provider to be added in the tag
    # @attr tag_name -> name for the tag (displayed as Network Status)
    def initialize(rescue_provider:, tag_name:)
      @rescue_provider = rescue_provider
      @tag_name = tag_name
      @rescue_provider_tag = nil
    end

    def execute
      tag = Tag.where(
        name: @tag_name,
        company: @rescue_provider.company
      ).first_or_create!

      tag.update!(deleted_at: nil) # make sure it will be active if it has been previously deleted.

      @rescue_provider_tag = RescueProvidersTag.where(
        rescue_provider: @rescue_provider,
        tag: tag
      ).first_or_create!

      self
    end

  end

end
