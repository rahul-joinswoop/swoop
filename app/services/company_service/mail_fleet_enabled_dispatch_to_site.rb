# frozen_string_literal: true

##
# Schedule EmailFleetEnabledDispatchToSite to be executed. Ideally it will
# be used when InHouseFleet added a Site (+ a RescueProvider) as a Partner
module CompanyService
  class MailFleetEnabledDispatchToSite

    include SwoopService

    def initialize(fleet_company:, fleet_user:, rescue_provider:)
      @fleet_company  = fleet_company
      @fleet_user     = fleet_user
      @rescue_provider = rescue_provider
    end

    def execute; end

    def after_execution
      if @rescue_provider.provider.dispatch_email.blank?
        Rails.logger.error I18n.t('company.fleet_enabled_dispatch_to_site_email_not_sent',
                                  rescue_company_name: @rescue_provider.provider.name, rescue_company_id: @rescue_provider.provider.id)

        return nil
      end

      if @fleet_company.support_email.blank?
        Rails.logger.error I18n.t('company.company_with_no_support_email',
                                  fleet_company_name: @fleet_company.name, fleet_company_id: @fleet_company.id)
      end

      Delayed::Job.enqueue(fleet_enabled_dispatch_to_site_mail)
    end

    private

    def fleet_enabled_dispatch_to_site_mail
      EmailFleetEnabledDispatchToSite.new(@fleet_company.id, @rescue_provider.id, @fleet_user.id)
    end

  end
end
