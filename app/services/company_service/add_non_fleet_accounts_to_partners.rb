# frozen_string_literal: true

module CompanyService
  class AddNonFleetAccountsToPartners

    include SwoopService

    attr_reader :accounts

    def initialize(company:, account_names:)
      raise ArgumentError if company.blank? || account_names.blank?

      @company = company
      @account_names = account_names
    end

    def execute
      accounts_to_be_created = @account_names.map do |account_name|
        { name: account_name, company_id: @company.id }
      end

      @accounts = Account.create!(accounts_to_be_created)

      self
    end

  end
end
