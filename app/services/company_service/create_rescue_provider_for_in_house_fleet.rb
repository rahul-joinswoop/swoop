# frozen_string_literal: true

###
# Service for creating a RescueProvider between an InHouseFleet and a Partner
module CompanyService

  class PartnerCompanyNotFoundError < StandardError; end

  class CreateRescueProviderForInHouseFleet

    include SwoopService

    attr_reader :attributes, :inhouse_company, :partner_company, :rescue_provider

    ##
    # @attr attributes -> attributes for the new rescue_provider
    # @attr inhouse_company -> fleet inhouse to what the provider will be added
    # @attr partner_company -> partner company - aka the provider
    # @attr site -> partner_company's site that will serve as the site provider
    def initialize(attributes:, inhouse_company:, partner_company:, site:)
      @attributes = attributes
      @inhouse_company = inhouse_company
      @partner_company = partner_company
      @site = site
      @rescue_provider = nil
    end

    def execute
      @rescue_provider = existing_rescue_provider || new_rescue_provider
      @rescue_provider.assign_attributes(@attributes)
      @rescue_provider.deleted_at = nil # force cancel soft delete
      @rescue_provider.save!

      self
    rescue ActiveRecord::RecordNotUnique
      raise ArgumentError,
            "Duplicate RescueProvider for site(#{@site.id}), " \
            "provider(#{@partner_company.id}), company(#{@inhouse_company.id})"
    end

    private

    def existing_rescue_provider
      RescueProvider.find_by(
        company: @inhouse_company, provider: @partner_company, site: @site
      )
    end

    def new_rescue_provider
      RescueProvider.new.tap do |rescue_provider|
        rescue_provider.company   = @inhouse_company
        rescue_provider.provider  = @partner_company
        rescue_provider.site      = @site
        rescue_provider.location  = @site.location
      end
    end

  end

end
