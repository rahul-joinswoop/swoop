# frozen_string_literal: true

###
# Changes the rescue company's commission.
#
module CompanyService
  class UpdateCommission

    include SwoopService

    attr_reader :company

    def initialize(company:, commission_attributes:)
      Rails.logger.debug("CompanyService::UpdateCommission initialized with company_id #{company.id}, commission_id #{commission_attributes[:id]}")

      @company = company
      @commission_attributes = commission_attributes
    end

    def execute
      if @commission_attributes[:id]
        commission = CompanyCommission.find_by!(
          id: @commission_attributes[:id],
          company: @company
        )

        commission.update_attributes!(@commission_attributes)
      else
        commission = CompanyCommission.new(@commission_attributes).tap do |c|
          c.company = @company
        end

        commission.save!
      end

      company.commission.reload

      self
    end

    def after_execution
      PublishCompanyWorker.perform_after_commit(@company.id, self.class.name)
    end

  end
end
