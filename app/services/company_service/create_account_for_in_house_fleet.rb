# frozen_string_literal: true

###
# Service for creating an Account between an InHouseFleet and a Partner
module CompanyService

  class PartnerCompanyNotFoundError < StandardError; end

  class CreateAccountForInHouseFleet

    include SwoopService

    attr_reader :inhouse_company, :partner_company, :account

    def initialize(inhouse_company:, partner_company:)
      @inhouse_company = inhouse_company
      @partner_company = partner_company
      @account = nil
    end

    def execute
      @account = Account.create!(
        name: @inhouse_company.name,
        client_company: @inhouse_company,
        company: @partner_company
      )

      self
    end

  end

end
