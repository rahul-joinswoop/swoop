# frozen_string_literal: true

module CompanyService
  class Batch

    def initialize(api_company:, company_ids:)
      @api_company = api_company
      @company_ids = company_ids.map(&:to_i)

      Rails.logger.debug "Company batch load param: company_ids = #{@company_ids}"
    end

    def call
      if @api_company.instance_of? SuperCompany
        # Swoop can load anyone
        batch_load_by(@company_ids)
      elsif @api_company.instance_of? RescueCompany
        companies_filtered_for_rescue_company
      else
        companies_filtered_for_fleet_company
      end
    end

    private

    # Partners can only load themselves, their subcompanies, and account companies
    def companies_filtered_for_rescue_company
      client_companies_from_accounts + partner_sub_companies + own_company
    end

    # Fleets can only load themselves and providers companies
    def client_companies_from_accounts
      client_company_ids_from_accounts =
        @api_company.accounts.where(client_company_id: [@company_ids])
          .pluck(:client_company_id)

      batch_load_by(client_company_ids_from_accounts)
    end

    def partner_sub_companies
      @api_company.sub_companies.where(id: @company_ids).includes(*query_includes)
    end

    def companies_filtered_for_fleet_company
      rescue_company_from_providers + own_company
    end

    def rescue_company_from_providers
      rescue_company_ids_from_providers =
        @api_company.rescue_providers.where(provider_id: @company_ids)
          .pluck(:provider_id)

      batch_load_by(rescue_company_ids_from_providers)
    end

    def own_company
      return batch_load_by(@api_company.id) if @company_ids.include? @api_company.id

      []
    end

    def query_includes
      [
        :location,
        :service_codes,
        :accounts,
        :features,
        :vehicle_categories,
        :symptoms,
        :company_service_questions,
        :departments,
        :fleet_in_house_clients,
        :fleet_managed_clients,
        live_sites: [:location],
        companies_customer_types: [:customer_type],
      ]
    end

    def batch_load_by(filtered_company_ids)
      Company.where(id: filtered_company_ids)
        .order(created_at: :desc)
        .includes(*query_includes).distinct
    end

  end
end
