# frozen_string_literal: true

###
# Service for updating a Company
# Company types allowed: RescueCompany, FleetCompany (InHouse or Managed)
module CompanyService
  class Update

    include SwoopService

    attr_reader :company

    def initialize(
      company_id:,
      company_attributes:,
      feature_ids: nil,
      customer_type_ids: nil,
      settings_attributes: nil,
      fleet_in_house_client_ids_to_add: nil,
      fleet_managed_client_ids_to_add: nil,
      invoice_charge_fee_attributes: nil,
      autoassignment_ranker: nil
    )

      Rails.logger.debug(%{
        Company(#{company_id})
        CompanyService::Update initialized with
        company_id: #{company_id},
        company_attributes: #{company_attributes},
        feature_ids: #{feature_ids},
        customer_type_ids: #{customer_type_ids},
        settings_attributes: #{settings_attributes},
        fleet_in_house_client_ids_to_add: #{fleet_in_house_client_ids_to_add},
        fleet_managed_client_ids_to_add: #{fleet_managed_client_ids_to_add},
        autoassignment_ranker: #{autoassignment_ranker}
      }.squish)

      @company = Company.find_by!(id: company_id)
      @dispatch_to_all_partners_was = @company.dispatch_to_all_partners
      @company_attributes = company_attributes
      @feature_ids = feature_ids
      @customer_type_ids = customer_type_ids
      @settings_attributes = settings_attributes
      @fleet_in_house_client_ids_to_add = fleet_in_house_client_ids_to_add
      @fleet_managed_client_ids_to_add = fleet_managed_client_ids_to_add
      @invoice_charge_fee_attributes = invoice_charge_fee_attributes
      @autoassignment_ranker = autoassignment_ranker
    end

    def execute
      if @feature_ids
        CompanyService::ChangeFeatures.new(
          company_id: @company.id,
          feature_ids: @feature_ids,
          overwrite_features: []
        ).execute
      end

      if @customer_type_ids
        CompanyService::ChangeCustomerTypes.new(
          company: @company,
          customer_type_ids: @customer_type_ids
        ).call
      end

      CompanyService::SaveWithJobs.call(
        company: @company,
        params: @company_attributes
      )

      if @settings_attributes
        CompanyService::CreateOrUpdateSettings.new(
          settings_attributes: @settings_attributes,
          company: @company
        ).call
      end

      if @company.instance_of?(FleetCompany) &&
        @company.fleet_managed? &&
        @autoassignment_ranker &&
        @autoassignment_ranker.key?(:autoassignment_ranker) &&
        @autoassignment_ranker[:autoassignment_ranker].any?
        CompanyService::ChangeAutoassignmentRankings.call(
          company: @company,
          autoassignment_ranker: @autoassignment_ranker
        )
      end

      handle_fleet_associations(@fleet_in_house_client_ids_to_add)
      handle_fleet_associations(@fleet_managed_client_ids_to_add, false)

      handle_dispatch_to_all_partners

      update_invoice_charge_fee

      self
    end

    private

    def handle_fleet_associations(fleet_ids, in_house = true)
      Rails.logger.debug("Company(#{@company.id}) CompanyService::Update " \
        "handle_fleet_associations called with fleet_ids: #{fleet_ids}, " \
        "in_house: #{in_house}, company.type: #{@company.type}")

      return unless @company.rescue? && fleet_ids

      current_fleet_ids = []

      if in_house
        current_fleet_ids = @company.fleet_in_house_client_ids
      else
        current_fleet_ids = @company.fleet_managed_client_ids
      end

      new_ids = fleet_ids - current_fleet_ids
      new_ids.each { |id| add_fleet(id) }

      removed_ids = current_fleet_ids - fleet_ids
      removed_ids.each { |id| remove_fleet(id) }
    end

    def add_fleet(fleet_id)
      fleet_company = Company.find_by({
        id: fleet_id,
        type: ["FleetCompany", "SuperCompany"],
      })

      CompanyService::LinkProviderAndClient.new(
        rescue_company: @company,
        client_company: fleet_company
      ).call_with_transaction
    end

    def remove_fleet(fleet_id)
      fleet_company = Company.find_by({
        id: fleet_id,
        type: ["FleetCompany", "SuperCompany"],
      })

      CompanyService::UnlinkProviderAndClient.new(
        rescue_company: @company,
        client_company: fleet_company
      ).call_with_transaction
    end

    def handle_dispatch_to_all_partners
      Rails.logger.debug(
        "Company(#{@company.id}) CompanyService::Update handle_dispatch_to_all_partners called " \
        " with company: #{@company.id}, company.type: #{@company.type}, company.in_house?: " \
        "#{@company.in_house?}, @dispatch_to_all_partners_was: #{@dispatch_to_all_partners_was}, " \
        "company.dispatch_to_all_partners: #{@company.dispatch_to_all_partners}, "
      )

      return unless @company.fleet_managed? && (
        @dispatch_to_all_partners_was != @company.dispatch_to_all_partners
      )

      CompanyService::HandleDispatchToAllPartners.new(
        company_id: @company.id,
        enable_dispatch_to_all_partners: @company.dispatch_to_all_partners
      ).call
    end

    def update_invoice_charge_fee
      return unless @company.rescue? && @invoice_charge_fee_attributes.present?

      CompanyService::UpdateInvoiceChargeFee.call(
        input: {
          company_id: @company.id,
          invoice_charge_fee_attributes: @invoice_charge_fee_attributes,
        }
      )
    end

  end
end
