# frozen_string_literal: true

module CompanyService
  # Add default questions to a company based on the ServiceCodes given.
  #
  # This service can be used when adding new ServiceCodes to a company to ensure
  # default questions are created if necessary for that service. For example:
  #
  #   comp = Company.last
  #   repo = ServiceCode.find_by name: ServiceCode::REPO
  #   comp.service_codes << repo
  #
  #   if comp.has_feature?(Feature::Questions)
  #     CreateDefaultQuestions.new(company: comp, service_codes: [repo]).execute
  #   end
  class CreateDefaultQuestions

    def initialize(company:, service_codes:)
      @company = company
      @service_codes = service_codes
      @company_service_questions = []
    end

    def execute
      @service_codes.each do |service_code|
        # skip the defaults when the company already has the service_code with any question,
        # so we don't overrride the current ones.
        next if @company.company_service_questions.where(service_code_id: service_code.id).any?

        questions = Question.defaults_per_service_code(service_code)

        create_company_service_questions(service_code, questions) if questions
      end

      { company_service_questions: @company_service_questions }
    end

    private

    def create_company_service_questions(service_code, questions)
      questions.each do |question|
        attributes = {
          company: @company, service_code: service_code, question: question,
        }

        next if CompanyServiceQuestion.exists?(attributes)

        @company_service_questions << CompanyServiceQuestion.create(attributes)
      end
    end

  end
end
