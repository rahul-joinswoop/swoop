# frozen_string_literal: true

###
# Service for handling auto dispatch settings of a FleetManaged company to all Partners.
# Company type allowed: FleetCompany
module CompanyService
  class HandleDispatchToAllPartners

    include SwoopService

    attr_reader :company

    def initialize(company_id:, enable_dispatch_to_all_partners:)
      Rails.logger.debug("Company(#{company_id}) CompanyService::HandleDispatchToAllPartners initialized with " \
        "company_id: #{company_id}, enable_dispatch_to_all_partners: #{enable_dispatch_to_all_partners}")

      @company = Company.find_by!(id: company_id, type: 'FleetCompany', in_house: [false, nil])
      @enable_dispatch_to_all_partners = enable_dispatch_to_all_partners
    end

    def execute
      if @enable_dispatch_to_all_partners
        Rails.logger.debug("Company(#{@company.id}) CompanyService::HandleDispatchToAllPartners will call dispatch_to_all_partners db function")
        ApplicationRecord.execute_query("SELECT dispatch_to_all_partners(?)", @company.id)
      else
        Rails.logger.debug("Company(#{@company.id}) CompanyService::HandleDispatchToAllPartners will soft delete all its rescue_providers")
        RescueProvider.where(company_id: @company.id).update_all(deleted_at: Time.now)
      end

      handle_custom_services_to_partners

      self
    end

    private

    def handle_custom_services_to_partners
      fleet_managed_custom_services = @company.service_codes.custom_items.where(addon: [nil, false])

      Rails.logger.debug("Company(#{@company.id}) CompanyService::HandleDispatchToAllPartners handle_custom_services_to_partners called " \
        "fleet_managed_custom_services.present? #{fleet_managed_custom_services.present?}")

      if fleet_managed_custom_services.present?
        if @enable_dispatch_to_all_partners
          Rails.logger.debug("Company(#{@company.id}) CompanyService::HandleDispatchToAllPartners handle_custom_services_to_partners " \
            "will call UpdateCustomServiceCodesToPartnersWorker.perform_async to add custom services")

          UpdateCustomServiceCodesToPartnersWorker.perform_async(
            @company.id,
            fleet_managed_custom_services.map(&:id),
            nil,
            false
          )
        else
          Rails.logger.debug("Company(#{@company.id}) CompanyService::HandleDispatchToAllPartners handle_custom_services_to_partners " \
            "will call UpdateCustomServiceCodesToPartnersWorker.perform_async to remove custom services")

          UpdateCustomServiceCodesToPartnersWorker.perform_async(
            @company.id,
            nil,
            fleet_managed_custom_services.map(&:id),
            false
          )
        end
      end

      fleet_managed_custom_addons = @company.service_codes.custom_items.where(addon: true)

      Rails.logger.debug("Company(#{@company.id}) CompanyService::HandleDispatchToAllPartners handle_custom_services_to_partners called " \
        "fleet_managed_custom_addons.present? #{fleet_managed_custom_addons.present?}")

      if fleet_managed_custom_addons.present?
        if @enable_dispatch_to_all_partners
          Rails.logger.debug("Company(#{@company.id}) CompanyService::HandleDispatchToAllPartners handle_custom_services_to_partners " \
            "will call UpdateCustomServiceCodesToPartnersWorker.perform_async to add custom addons")

          UpdateCustomServiceCodesToPartnersWorker.perform_async(
            @company.id,
            fleet_managed_custom_addons.map(&:id),
            nil,
            true
          )
        else
          Rails.logger.debug("Company(#{@company.id}) CompanyService::HandleDispatchToAllPartners handle_custom_services_to_partners " \
            "will call UpdateCustomServiceCodesToPartnersWorker.perform_async to remove custom addons")

          UpdateCustomServiceCodesToPartnersWorker.perform_async(
            @company.id,
            nil,
            fleet_managed_custom_addons.map(&:id),
            true
          )
        end
      end
    end

  end
end
