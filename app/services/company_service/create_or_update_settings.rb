# frozen_string_literal: true

###
# Service for updating the Settings of a Company
module CompanyService
  class CreateOrUpdateSettings

    include SwoopService
    include SwoopServiceComposite

    attr_reader :company, :settings

    ##
    # @attr settings_attributes -> attributes for the settings
    def initialize(settings_attributes: {}, company:)
      @settings_attributes = settings_attributes
      @company             = company
    end

    def execute
      @settings_attributes.each do |setting_attribute|
        setting_key   = setting_attribute[:key]
        setting_value = setting_attribute[:value]

        Setting.where(
          key: setting_key,
          company: @company
        ).first_or_initialize.tap do |setting|
          setting.value = setting_value

          setting.save!
        end
      end
    end

  end
end
