# frozen_string_literal: true

# Service to update the autoassignment rankings of a company
module CompanyService

  class CannotChangeAutoassignmentRankingsUnlessAManagedFleetCompanyError < StandardError; end
  class CannotChangeAutoassignmentRankingsUnlessARankerExistsForCompanyError < StandardError; end
  class CannotChangeAutoassignmentRankingsWithoutWeightsOrConstraintsError < StandardError; end

  class ChangeAutoassignmentRankings

    include Interactor

    before do
      raise CannotChangeAutoassignmentRankingsUnlessAManagedFleetCompanyError unless context.company.instance_of?(FleetCompany) && context.company.fleet_managed?
      raise CannotChangeAutoassignmentRankingsUnlessARankerExistsForCompanyError unless context.company.ranker
      raise CannotChangeAutoassignmentRankingsWithoutWeightsOrConstraintsError unless
        context.autoassignment_ranker.is_a?(Hash) &&
        context.autoassignment_ranker.key?(:autoassignment_ranker) &&
        %w(weights constraints).any? { |required_property| context.autoassignment_ranker[:autoassignment_ranker].keys.include? required_property }

      @company = context.company
      @autoassignment_ranker = context.autoassignment_ranker[:autoassignment_ranker]
    end

    def call
      @company.transaction do
        # Step 1: Save a reference to the old ranker
        old_ranker = @company.ranker
        # Step 2: Create a new live ranker from the old one
        new_ranker = @company.ranker.dup
        # Step 3: Update the new ranker's autoassignment weights and constraints, ensuring types
        # are cast so that updates are not unnecessarily triggered, and persist
        new_ranker.update!(weights: @company.ranker.weights.deep_merge(@autoassignment_ranker['weights'].transform_values { |v| v.to_f })) if @autoassignment_ranker.key?('weights')
        new_ranker.update!(constraints: @company.ranker.constraints.deep_merge(@autoassignment_ranker['constraints'].transform_values { |v| v.to_i })) if @autoassignment_ranker.key?('constraints')

        # Step 4: Assign the new ranker to the company
        @company.ranker = new_ranker
        # Step 5: Turn off the old ranker
        old_ranker.update!(live: false)
      end

      self
    end

  end

end
