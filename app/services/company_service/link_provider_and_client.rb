# frozen_string_literal: true

module CompanyService
  class LinkProviderAndClient

    include SwoopService

    def initialize(rescue_company:, client_company:)
      @rescue_company = rescue_company
      @client_company = client_company

      raise ArgumentError, 'must supply a rescue and fleet company' unless @rescue_company.rescue? && @client_company.fleet?
    end

    def execute
      link_account

      add_custom_service_codes_to_partner

      link_providers
    end

    private

    def link_account
      return if @client_company.fleet_managed? # Fleet Managed doesn't have accounts

      account = @rescue_company.accounts.find_by(client_company: @client_company)

      if account.nil?
        account = Account.new({
          company: @rescue_company,
          client_company: @client_company,
          name: @client_company.name,
        })
      else
        account.deleted_at = nil
      end

      account.save! if !account.persisted? || account.changed?
    end

    def add_custom_service_codes_to_partner
      ServiceCodeServices::AddCustomServiceCodesToPartner.new(
        rescue_company: @rescue_company,
        client_company: @client_company
      ).call
    end

    def link_providers
      providers = []

      @rescue_company.live_sites.where(dispatchable: true).each do |site|
        provider = RescueProvider.find_by({
          company: @client_company,
          provider: @rescue_company,
          site: site,
        })

        if provider.nil?
          provider_builder = AddClientCompanyToPartner.new(@rescue_company, @client_company, site)
          provider = provider_builder.call
        end

        provider.deleted_at = nil
        providers << provider
      end

      providers.each(&:save!)
    end

  end
end
