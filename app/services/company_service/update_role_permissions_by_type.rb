# frozen_string_literal: true

module CompanyService
  class UpdateRolePermissionsByType

    include Interactor
    include Interactor::Swoop

    before do
      @company          = context.input[:company]
      @role_ids         = context.input[:role_ids].map(&:to_i)
      @permission_type  = context.input[:permission_type]

      if @company.blank?
        raise ArgumentError, "CompanyService::ResetRolePermissionsByType @company is blank"
      end

      if @role_ids.nil?
        raise ArgumentError, "CompanyService::ResetRolePermissionsByType @role_ids is nil"
      end

      if @permission_type.blank?
        raise ArgumentError, "CompanyService::ResetRolePermissionsByType @permission_type is blank"
      end
    end

    def call
      add_company_roles_by_type!

      remove_permissions_not_included_in_role_ids!

      context.output = {
        company: @company,
      }

      PublishCompanyWorker.perform_after_commit(@company.id, self.class.name)
    end

    private

    def add_company_roles_by_type!
      @role_ids.each do |role_id|
        @company.role_permissions.where(
          permission_type: @permission_type,
          role_id: role_id
        ).first_or_create!
      end
    end

    def remove_permissions_not_included_in_role_ids!
      @company.role_permissions
        .where(permission_type: @permission_type)
        .where.not(role_id: @role_ids)
        .delete_all
    end

  end
end
