# frozen_string_literal: true

###
# Service for creating a new Company
# Company types allowed: RescueCompany, FleetCompany (InHouse or Managed)
module CompanyService
  class Create

    include SwoopService
    include SwoopServiceComposite

    # company can be a RescueCompany or FleetCompany at the end of the execution
    attr_reader :company_attributes, :feature_ids, :company, :location, :rescue_provider_for_swoop

    ##
    # @attr company_attributes -> attributes for the new company
    #
    # @attr rescue_provider_attributes -> attributes for the new rescue_provider when
    #                                     RescueCompany is created
    #
    # @attr feature_ids -> additional feature ids to add add to the company
    def initialize(company_attributes:, rescue_provider_attributes: {}, location_attributes: {},
                   feature_ids:, api_company: nil, api_user: nil)

      Rails.logger.debug("CompanyService::Create initialized with " \
        "company_attributes: #{company_attributes}, " \
        "rescue_provider_attributes: #{rescue_provider_attributes}, " \
        "location_attributes: #{location_attributes}, " \
        "feature_ids: #{feature_ids}, api_company: #{api_company}, api_user: #{api_user}")

      @company_attributes         = company_attributes
      @rescue_provider_attributes = rescue_provider_attributes
      @location_attributes = location_attributes
      @feature_ids = feature_ids
      @api_company = api_company
      @api_user = api_user

      @company = nil
      @rescue_provider_for_swoop = nil
    end

    ##
    # It creates a new Company.
    #
    # The 'company.defaults' call adds some specific attributes to the Company,
    # depending on the Company type.
    # @see Company<type>#defaults method.
    #
    # If Company is_kind_of? RescueCompany, it also adds a RescueProvider
    # and an Account for Swoop Company.
    def execute
      @company = Company.create!(@company_attributes)
      @location = Location.create!(@location_attributes)
      @company.location = @location

      Rails.logger.debug("CompanyService::Create#execute - Company created #{@company.id}")

      @company.defaults
      add_features_to_company if feature_ids

      if @company.is_a? RescueCompany
        customize_new_rescue_company
      end

      add_invited_by_variables_to_company if @api_company.present? && @api_user.present?

      handle_adding_as_dispatchable_to_fleet_managed # when RescueCompany
      handle_dispatch_to_all_partners # when Fleet

      @company.save!

      # Every managed fleet company that's created should have a new default ranker
      if @company.instance_of?(FleetCompany) && @company.fleet_managed?
        @company.get_or_create_ranker
      end

      @company.reload # make sure we have a fresh version before returning
      @company
    end

    private

    def customize_new_rescue_company
      if @company.sites.empty?
        site_hq = create_hq_site_for_company

        @company.location.site_id = site_hq.id
        @company.location.save!

        site_hq.schedule_google_timezone

        add_non_fleet_accounts_to_partner
        # Creating default territory for a newly created RescueCompany
        if @company.location.lat.present? && @company.location.lng.present?
          Territories::CreateTerritoriesForCompany.new.call(@company)
        end
      end

      create_rescue_provider_for_swoop
      create_account_for_swoop
      add_swoop_custom_service_codes
      add_qb_token_to_rescue_company
      add_goa_rate_for_swoop

      add_default_invoice_charge_fee_to_partner
      add_default_role_permissions_to_partner
    end

    def add_swoop_custom_service_codes
      ServiceCodeServices::AddCustomServiceCodesToPartner.new(
        rescue_company: @company,
        client_company: Company.swoop
      ).call
    end

    def create_rescue_provider_for_swoop
      swoop_company = Company.swoop

      rescue_provider_attributes_for_swoop =
        {
          company: swoop_company,
          provider: @company,
          site: @company.hq,
          location: @location,
        }

      rescue_provider_attributes_for_swoop.merge!(@rescue_provider_attributes.to_h)

      @rescue_provider_for_swoop = RescueProvider.create!(rescue_provider_attributes_for_swoop)
    end

    def create_hq_site_for_company
      PartnerSite.create(
        name: Site::HQ,
        company: @company,
        location: @company.location,
        dispatchable: true,
        always_open: true,
        phone: @company.phone
      )
    end

    def create_account_for_swoop
      Company.swoop.tap do |swoop_company|
        Account.create(
          company: @company,
          client_company: swoop_company,
          name: swoop_company.name
        )
      end
    end

    # Add an default GOA rate for Swoop since we don't want to pay the normal fee on GOA's.
    def add_goa_rate_for_swoop
      account = Account.find_by(company_id: @company.id, client_company_id: Company.swoop_id)
      if account
        @company.rates.create!(type: FlatRate.name, account: account, service_code: ServiceCode.goa, site_id: nil, vendor_id: nil, vehicle_category_id: nil, flat: 0, live: true, currency: @company.currency)
      end
    end

    def add_features_to_company
      add_features_service = CompanyService::ChangeFeatures.new(
        company_id: company.id, feature_ids: feature_ids
      )
      add_child_service(add_features_service)
      add_features_service.execute
    end

    def add_non_fleet_accounts_to_partner
      CompanyService::AddNonFleetAccountsToPartners.new(
        company: @company,
        account_names: [Account::CASH_CALL]
      ).call
    end

    def add_invited_by_variables_to_company
      @company.invited_by_company_id = @api_company.id
      @company.invited_by_user_id = @api_user.id

      @company.save!
    end

    def add_qb_token_to_rescue_company
      # Sanity check that the token hasn't been created yet
      QB::Token.find_or_create_by!(company: @company) do |token|
        token.token = SecureRandom.uuid
      end
    end

    def handle_dispatch_to_all_partners
      Rails.logger.debug("Company(#{@company.id}) CompanyService::Create " \
        "handle_dispatch_to_all_partners called with " \
        "company: #{@company.id}, company.type: #{@company.type}, " \
        "company.in_house?: #{@company.in_house?}, " \
        "company.dispatch_to_all_partners: #{@company.dispatch_to_all_partners}")

      return unless @company.fleet_managed? && @company.dispatch_to_all_partners

      CompanyService::HandleDispatchToAllPartners.new(
        company_id: @company.id,
        enable_dispatch_to_all_partners: true
      ).call
    end

    def handle_adding_as_dispatchable_to_fleet_managed
      return unless @company.instance_of? RescueCompany

      Rails.logger.debug(
        "Company(#{@company.id}) CompanyService::Create handle_dispatch_to_all_partners called"
      )

      fleet_managed_companies = FleetCompany.where(
        dispatch_to_all_partners: true,
        type: FleetCompany.name,
        in_house: [nil, false],
        deleted_at: nil
      )

      fleet_managed_companies.each do |fleet_managed|
        Rails.logger.debug("Company(#{@company.id}) CompanyService::Create " \
          "handle_dispatch_to_all_partners will call " \
          "CompanyService::LinkProviderAndClient with rescue_company(#{@company.id}), " \
          "client_company(#{fleet_managed.id}")

        CompanyService::LinkProviderAndClient.new(
          rescue_company: @company,
          client_company: fleet_managed
        ).call
      end
    end

    def add_default_invoice_charge_fee_to_partner
      CompanyService::AddDefaultInvoiceChargeFeeToPartner.call(
        input: { company_id: @company.id }
      )
    end

    def add_default_role_permissions_to_partner
      role_admin_id = Role.find_by(name: 'admin').id
      role_dispatcher_id = Role.find_by(name: 'dispatcher').id

      CompanyService::UpdateRolePermissionsByType.call(
        input: {
          company: @company,
          role_ids: [role_admin_id, role_dispatcher_id],
          permission_type: CompanyRolePermission::CHARGE,
        }
      )

      CompanyService::UpdateRolePermissionsByType.call(
        input: {
          company: @company,
          role_ids: [role_admin_id],
          permission_type: CompanyRolePermission::REFUND,
        }
      )
    end

  end
end
