# frozen_string_literal: true

module CompanyService
  class SaveWithJobs

    include Interactor

    before do
      @company = context.company
      @params = context.params

      @old_name = @company.name
    end

    def call
      context.company = update_company!

      if name_updated?
        CompanyNameChange.perform_after_commit(@company.id, @old_name)
      end
    end

    private

    def name_updated?
      previous_changes.keys.include?('name')
    end

    def previous_changes
      @company.previous_changes
    end

    def update_company!
      @company.update(@params)
    end

  end
end
