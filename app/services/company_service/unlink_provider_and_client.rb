# frozen_string_literal: true

module CompanyService
  class UnlinkProviderAndClient

    include SwoopService

    def initialize(rescue_company:, client_company:)
      @rescue_company = rescue_company
      @client_company = client_company

      raise ArgumentError, 'must supply a rescue and fleet company' unless @rescue_company.rescue? && (@client_company.fleet? || @client_company.super?)
    end

    def execute
      remove_account

      remove_custom_service_codes_from_partner

      remove_rescue_providers
    end

    def remove_account
      account = @rescue_company.accounts.find_by(client_company: @client_company)

      account.update(deleted_at: Time.zone.now) unless account.nil?
    end

    def remove_custom_service_codes_from_partner
      custom_service_codes_ids_from_fleet =
        @client_company.service_codes.where(is_standard: [nil, false]).pluck(:id)

      service_codes_ids_used_by_other_accounts =
        filter_service_code_ids_by_partner_accounts(custom_service_codes_ids_from_fleet)

      service_code_ids_to_be_removed =
        (custom_service_codes_ids_from_fleet - service_codes_ids_used_by_other_accounts)

      ServiceCodeServices::Change.new(
        api_company: @rescue_company,
        services_ids_to_remove: service_code_ids_to_be_removed
      ).call
    end

    def remove_rescue_providers
      RescueProvider.where({
        company: @client_company,
        provider: @rescue_company,
      }).update_all(deleted_at: Time.zone.now)
    end

    def filter_service_code_ids_by_partner_accounts(custom_service_codes_ids_from_fleet)
      custom_service_codes_ids_from_fleet.map do |service_code_id|
        account = @rescue_company.accounts
          .joins(client_company: [:service_codes])
          .where.not(client_company: @client_company)
          .where(deleted_at: nil, service_codes: { id: service_code_id }).distinct

        service_code_id if account.present?
      end.compact
    end

  end
end
