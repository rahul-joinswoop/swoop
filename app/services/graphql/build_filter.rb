# frozen_string_literal: true

module GraphQL
  class BuildFilter

    include Interactor
    include Interactor::Swoop

    def call
      company = context.dig(:input, :context, :api_company)
      roles = context.dig(:input, :context, :api_roles)

      filters = [
        require_role_filter(roles),
        require_feature_filter(company),
        company_filter(company),
        context_filter,
      ]

      context.output = {
        context: context.input[:context],
        only_filter: -> (schema_member, ctx) {
          filters.all? { |filter| filter.call(schema_member, ctx) }
        },
      }

      context
    end

    private

    # Build a filter that will return true only if the api user or company has the
    # specified list of roles.
    # Example: `requires_roles: :rescue`
    # If a proc is given as the value for :required_roles, it will be called with
    # the array of roles as symbols.
    def require_role_filter(roles)
      # build our required roles filter
      roles = Array(roles).map(&:to_sym)
      lambda do |schema_member, ctx|
        meta_data_roles = schema_member.metadata[:required_roles]
        # return true if there are no required_roles
        return true if meta_data_roles.blank?

        # or if the current viewer has a root role
        return true if roles.include?(:root)

        # if meta_data_roles is a proc, call it with our roles/scopes and return the result
        return roles.present? && meta_data_roles.call(roles) if meta_data_roles.respond_to?(:call)

        return false if roles.blank?

        meta_data_roles = Array(meta_data_roles).map(&:to_sym)

        # or if required_roles: :any and we have some roles
        return true if meta_data_roles.include?(:any)

        # or otherwise if we have all the roles requested
        meta_data_roles.all? { |role| roles.include?(role) }
      end
    end

    # Build a filter that will return true only if the api companies feature flags match
    # the specified list of flags.
    # Example: `require_feature: Feature::DEPARTMENTS`
    def require_feature_filter(company)
      features = Set.new(company&.features&.map(&:name))
      lambda do |schema_member, ctx|
        meta_data_features = schema_member.metadata[:required_features]
        return true if meta_data_features.blank?

        # if meta_data_features is a proc, call it with our features and return the result
        return meta_data_features.call(features) if meta_data_features.respond_to?(:call)

        # otherwise dedupe meta_data_features and check to see if they're contained in our
        # features
        required_features = Array(meta_data_features).to_set
        required_features.subset?(features)
      end
    end

    # Build a filter that will return true if the company responds to the listed
    # methods and they return a truthy value.
    # Example: `if_company: :sites_enabled?`
    def company_filter(company)
      lambda do |schema_member, ctx|
        meta_data_methods = schema_member.metadata[:if_company]
        return true if meta_data_methods.blank?

        # if we have an if_company defined and we don't have a company go ahead and short-circuit false
        return false if company.blank?

        # if meta_data_methods is a proc, call it with our company and return the result
        return meta_data_methods.call(company) if meta_data_methods.respond_to?(:call)

        Array(meta_data_methods).all? do |method_name|
          company.respond_to?(method_name) && company.send(method_name)
        end
      end
    end

    # Build a filter that will return true if the context responds to the listed
    # methods and they return a truthy value.
    # Example: `if_context: :is_valid_subscription_viewer?`
    # context methods are defined in Utils::Context
    def context_filter
      lambda do |schema_member, ctx|
        meta_data_methods = schema_member.metadata[:if_context]
        return true if meta_data_methods.blank?

        # if meta_data_methods is a proc, call it with our features and return the result
        return meta_data_methods.call(ctx) if meta_data_methods.respond_to?(:call)

        Array(meta_data_methods).all? do |method_name|
          ctx.respond_to?(method_name) && ctx.send(method_name)
        end
      end
    end

  end
end
