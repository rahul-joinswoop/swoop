# frozen_string_literal: true

module GraphQL
  class Authenticate

    include Interactor::Organizer
    include Interactor::Swoop

    organize AuthenticateClient, BuildFilter

  end
end
