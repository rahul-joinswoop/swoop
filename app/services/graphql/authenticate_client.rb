# frozen_string_literal: true

module GraphQL
  # Take a doorkeeper token and request and extracts:
  #
  #  Company
  #  User (nil in the case of a Company Application)
  #  Application (nil in the case of a user login)
  #  Request_id
  #
  # currently used in graphql_controller / graphl_channel to authenticate
  # authenticate and construct graphql context
  class AuthenticateClient

    include Interactor
    include Interactor::Swoop

    AUTHENTICATION_FAILURE = "authentication failure"

    WHITELISTED_HEADERS = ['HTTP_USER_AGENT', 'HTTP_REFERER'].freeze

    class NoViewerError < StandardError
    end

    def call
      request = context.input[:request]
      authenticator = SwoopAuthenticator.for(request)
      doorkeeper_token = authenticator.access_token
      if doorkeeper_token.blank?
        context.fail!(message: AUTHENTICATION_FAILURE)
      end

      company = authenticator.company
      user = authenticator.user
      viewer = authenticator.viewer
      application = authenticator.application

      # fail if we don't have a viewer
      context.fail!(message: "invalid viewer") unless viewer

      # fail if we have an oauth application but no associated company
      context.fail!(message: "invalid application") if application && !company

      # TODO - let's set :company in context and use that since sometimes we'll
      # need api_company but also acts_as_company

      # TODO - customers (aka StrandedDrivers) don't have companies :/
      graphql_context = {
        api_application: application,
        api_access_token: authenticator.access_token,
        api_company: company,
        api_user: user,
        api_roles: authenticator.roles,
        headers: request.headers.to_h.slice(*WHITELISTED_HEADERS),
        operation_id: request.params[:operationId],
        request_id: request.uuid,
        viewer: viewer,
      }

      if request.headers.include?(::Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_URL)
        graphql_context[:webhook_url] = request.headers[::Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_URL]
        graphql_context[:webhook_secret] = request.headers[::Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET]
      end

      context.output = { context: graphql_context }
    end

  end
end
