# frozen_string_literal: true

#
# This is an internal service used by the metrics and the split services.
# Not designed to be called from elsewhere.
#
module Experiments
  class Service

    #
    # Enter the target into the experiment
    #
    # target:          Object you are tracking e.g. user, job
    # experiment_name: Experiement Name
    # variant_name:    Variant Name
    #
    def self.add_target_to_experiment(target, experiment_name, variant_name)
      experiment = Experiment.find_or_create_by!(name: experiment_name)
      variant = Variant.find_or_create_by!(experiment: experiment, name: variant_name)
      TargetVariant.find_or_create_by!(variant: variant, target: target)
    end

    #
    # Track a metric against any experiment variants the target may be involved in
    #
    # target: Object you are tracking against e.g. user, job
    # label:  Name of the metric e.g. 'cost'
    # value:  Integer value for the metric
    #
    def self.track_metric(target, label, value)
      Rails.logger.debug "Experiments::Service.track_metric target:#{target.class.name}(#{target.id}) label:#{label} value:#{value}"
      target_variants = TargetVariant.where(target: target)
      target_variants.each do |ue|
        ExperimentMetric.create!(target: target, variant: ue.variant, label: label, value: value)
      end
    end

  end
end
