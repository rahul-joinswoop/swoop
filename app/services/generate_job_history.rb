# frozen_string_literal: true

#
# Build a list of Job History items for the given job and viewing_company.
#
# A Job history can contain:
#
# - instances of AuditJobStatus - these represent a job status change (e.g. Dispatched -> En Route).
#
# - instances of HistoryItem - these are changes that happen in the job that
#                              do not trigger a job status change, but that need to be exposed
#                              as a History Item (e.g. Job Invoice Editing)
#
# - instances of OpenStruct built by this service: changes that are required as a Job History item,
#                                                  but are not AuditJobStatus or HistoryItem
#                                                  (e.g. job related SMS sent by customers)
#
# TODO Refactor this: https://swoopme.atlassian.net/browse/ENG-11324
class GenerateJobHistory

  class InvalidJobError < StandardError; end

  include LoggableJob
  include Interactor

  attr_accessor :job, :viewing_company, :recursion_count

  before do
    @job = context.job
    @viewing_company = context.viewing_company
    @recursion_count = context.recursion_count || 1

    raise InvalidJobError, "A Job must be provided" unless @job.is_a?(Job)
  end

  delegate :audit_job_statuses_as_hash,
           :invoice,
           :add_return_to_hq_to_history?,
           :return_to_hq_history,
           :audit_sms,
           :time_of_arrivals_non_live,
           :scheduled_for,
           :parent,
           :status,
           :created_at,
           :timezone,
           :user,
           to: :@job

  def call
    # STEP 1 - load all audit job statuses as hash
    results = audit_job_statuses_as_hash

    # STEP 2 - load virtual job statuses, the ones that are not persisted as audit_job_statuses
    history_item_results = @job.history_items
      .map { |i| [i.id, i] }
      .to_h

    # STEP 3 - merge both AJS and virtual statuses
    results.merge!(history_item_results)

    # STEP 4 - if invoice paid, add the corresponding AuditJobStatus to it
    # (why it didn't add it in audit_job_statuses_as_hash call?)
    if invoice && invoice.paid? && invoice.mark_paid_at
      results[JobStatus::ID_MAP[JobStatus::MARK_PAID]] =
        AuditJobStatus.audit_job_status_for_job_mark_paid(job)
    end

    # STEP 5 - add Return to HQ virtual status if necessary
    if add_return_to_hq_to_history?
      log :debug, "#job_history_hash Will add Return to HQ to History", job: job.id

      results[JobStatus::RETURNED_TO_HQ] = return_to_hq_history
    end

    # STEP 6 - add audit SMS entries, in case no viewing_company set, or based on viewing_company
    # in case it's present
    audit_sms.each do |audit|
      if (!viewing_company || audit.visible_to_company(viewing_company)) && audit.description.present?
        results[audit.name] = audit
      end
    end

    # STEP 7 - add ETAs that were input by users (non live)
    # as a virtual status
    time_of_arrivals_non_live.each do |toa|
      name = toa.description
      results[name] = toa
    end

    # STEP 8 - add invoice sent_at as a virtual status
    if invoice&.sent_at
      name = 'Invoice Sent'

      results[name] = OpenStruct.new(
        id: invoice.id,
        created_at: invoice.sent_at,
        name: name,
        last_set_dttm: invoice.sent_at,
        ui_type: 'InvoiceSentAt',
        adjusted_dttm: invoice.sent_at
      )
    end

    # STEP 9 - add job.scheduled_for as a virtual status
    if scheduled_for.present?
      name = 'Scheduled For'

      results[name] = OpenStruct.new(
        id: 'scheduled_for',
        created_at: created_at,
        name: "#{name} #{scheduled_for.in_time_zone(timezone).strftime('%m/%d/%y %k:%M')}",
        user: user,
        ui_type: 'ScheduledFor',
        last_set_dttm: scheduled_for
      )
    end

    #
    # STEP 10 - Add job.parent statuses
    #
    # TODO (Brenton) - Use JobAncestorsWithStatusesQuery (is this suggestion still valid?)
    if parent && (parent.status == Job::STATUS_REASSIGNED || status == Job::STATUS_UNASSIGNED) && (recursion_count < 10)
      parent_results = parent.job_history_hash(viewing_company, recursion_count + 1)

      parent_results.each do |pair|
        key = "#{pair[0]}#{parent.id}"

        if pair[1].instance_of? AuditJobStatus
          context = ActionController::Base.new.view_context
          # WHY DO WE DO THIS?
          ajs = OpenStruct.new(JbuilderTemplate.new(context) do |json|
            json.partial! "/api/v1/status_history/status_history.json.jbuilder", audit_job_status: pair[1]
          end.attributes!)

          if ajs.name.to_s != "Created"
            if ajs.name
              ajs.name = "*" + ajs.name
            end

            results[key] = ajs
          end
        else
          if pair[1].name.to_s != "Created"
            if pair[1].is_a?(OpenStruct) # TODO WE SHOULD REALLY GET RID OF THESE OpenStruct rows, they always break something!
              results[key] = pair[1]
              results[key].name = "*" + results[key].name.to_s
            else
              results[key] = ParentJobHistoryItem.new(pair[1])
            end
          end
        end
      end
    end

    context.results = results
  end

end
