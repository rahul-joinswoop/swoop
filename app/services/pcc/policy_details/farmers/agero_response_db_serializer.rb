# frozen_string_literal: true

module PCC
  module PolicyDetails
    module Farmers
      # input: take context.response and write them to pcc:Policy, pcc:Person and pcc:Vehicle models
      #
      # sideeffect: db rows written
      #
      # output: context.policies [<pcc:Policy>...]
      class AgeroResponseDBSerializer

        include Interactor
        include Interactor::Swoop

        before do
          Rails.logger.debug "PCC::PolicyDetails::AgeroResponseDBSerializer called #{context}"

          @agero_response = context.input[:response]
          @policy_id = context.input[:policy_id]

          raise ArgumentError, 'PCC::PolicyDetails::Farmers::AgeroResponseDBSerializer agero_response is null' unless
            @agero_response

          raise ArgumentError, 'PCC::PolicyDetails::Farmers::AgeroResponseDBSerializer agero_response not open struct' unless
            @agero_response.class.name == "OpenStruct"

          raise ArgumentError, 'PCC::PolicyDetails::Farmers::AgeroResponseDBSerializer policy_id is null' unless
            @policy_id
        end

        def call
          parser = PolicyDetailsParser.new(@agero_response&.policy)
          policy = PCC::Policy.find(@policy_id)

          transaction_id = @agero_response&.transactionId

          ApplicationRecord.transaction do
            policy_vehicles = []

            parser.vehicles.each do |vehicle|
              policy_vehicles << PCC::Vehicle.create(
                pcc_policy_id: policy.id,
                vin: vehicle.vin,
                make: vehicle.make,
                model: vehicle.model,
                year: vehicle.year,
                data: vehicle.data
              )
            end

            policy_drivers = []

            parser.drivers.each do |driver|
              policy_drivers << PCC::User.create(
                pcc_policy_id: policy.id,
                full_name: driver.full_name,
                data: driver.data
              )
            end

            policy_vehicles.map(&:save!)
            policy_drivers.map(&:save!)

            policy.data.merge! @agero_response&.policy&.to_h
            policy.data[:transactionId] = transaction_id

            policy.save!
          end

          context.output = policy
        end

      end
    end
  end
end
