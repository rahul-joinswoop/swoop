# frozen_string_literal: true

#
# ** create a Policy first:
# PCC::PCCService.policy_lookup(policy_number: "0103293256", company: Company.farmers, user: User.where(company: Company.farmers).last)
#
# ** then call the PolicyDetails organizer:
# drivers_and_cars = PCC::PolicyDetails::Farmers::Organizer.call(input: { policy_id: <policy_id created on the policy_lookup call> })
#
# ** It will add vehicles and drivers to the PCC::Policy (so the PCC::Policy must exist on our DB, otherwise it will throw error),
# ** and the result will be the drivers and vehicles serialized by PCC::PolicyDriversAndVehiclesSerializer
#
module PCC
  module PolicyDetails
    module Farmers
      class Organizer

        include Interactor::Organizer
        include Interactor::Swoop

        organize AgeroApiRequest, AgeroResponseDBSerializer, FormatSwoopApiResponse

      end
    end
  end
end
