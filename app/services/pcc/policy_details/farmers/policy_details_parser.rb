# frozen_string_literal: true

module PCC
  module PolicyDetails
    module Farmers
      class PolicyDetailsParser

        attr_reader :data

        def initialize(open_struct)
          raise ArgumentError, 'PCC::PolicyDetails::Farmers::Parser agero_response not open struct' unless open_struct.class.name == "OpenStruct"

          @data = open_struct
        end

        def policy_number
          @data.policyNumber
        end

        def drivers
          @data.drivers.map do |driver|
            OpenStruct.new(
              full_name: "#{driver.firstName} #{driver.lastName}",
              data: driver
            )
          end
        end

        def vehicles
          @data.vehicles.map do |vehicle|
            OpenStruct.new(
              make: vehicle.make,
              model: vehicle.model,
              year: vehicle.year,
              vin: vehicle.vin,
              data: vehicle
            )
          end
        end

      end
    end
  end
end

#
# {
#     "transactionId": "G0x7faaa1886008",
#     "policy": {
#         "policyNumber": "0103293256",
#         "policyStateCode": "97",
#         "policyType": "Auto",
#         "companyCode": "1",
#         "policyHolder": {
#             "firstName": "KENNETH",
#             "middleName": "",
#             "lastName": "DIXON",
#             "contactInfo": {
#                 "address": {
#                     "street": "20052 PINEVILLE CT # 60",
#                     "city": "YRBA LNDA",
#                     "state": "CA",
#                     "zip": "92886"
#                 },
#                 "phoneNumber": null,
#                 "faxNumber": null
#             }
#         },
#         "drivers": [
#             {
#                 "firstName": "KENNETH",
#                 "lastName": "DIXON",
#                 "relation": "H"
#             }
#         ],
#         "vehicles": [
#             {
#                 "policyUnit": "1",
#                 "year": "2013",
#                 "make": "HONDA",
#                 "model": "ACCORD 2D LX-S",
#                 "vin": "1HGCT1B32DA021662",
#                 "propertyType": "Auto"
#             }
#         ],
#         "agent": {
#             "agentCode": "38E",
#             "firstName": "DEBRA",
#             "lastName": "MCCAMISH",
#             "stateCode": "97",
#             "districtCode": "18",
#             "contactInfo": {
#                 "address": {
#                     "street": "",
#                     "city": "",
#                     "state": "",
#                     "zip": ""
#                 },
#                 "phoneNumber": "",
#                 "faxNumber": ""
#             }
#         },
#         "coverageSystemSource": "APPSCE"
#     }
# }
#
