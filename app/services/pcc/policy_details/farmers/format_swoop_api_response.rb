# frozen_string_literal: true

module PCC
  module PolicyDetails
    module Farmers
      class FormatSwoopApiResponse

        include Interactor
        include Interactor::Swoop

        before do
          @policy = context.input
        end

        # input: take context.policies from AgeroResponseDbSerializer
        # output: JSON hash suitable for returning to the UI
        # swoop_response:{
        #   policy_id:
        #   drivers_and_vehicles:[
        #   {
        #     driver_id:
        #     vehicle_id:
        #     driver_name:
        #     vehicle_description:
        #     vin:
        #   }]
        # }

        def call
          # serialise drivers and cars

          context.output = PCC::PolicyDriversAndVehiclesSerializer.new(
            @policy
          ).serializable_hash
        end

      end
    end
  end
end
