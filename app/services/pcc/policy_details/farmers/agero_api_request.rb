# frozen_string_literal: true

#
# request = PCC::PolicyDetails::Farmers::AgeroApiRequest.call(input: { policy_id: 123 })
#
module PCC
  module PolicyDetails
    module Farmers
      class AgeroApiRequest

        include Interactor
        include Interactor::Swoop

        API_RESOURCE_TEMPLATE = '/profiles/{policy_number}'

        before do
          Rails.logger.debug "PCC::PolicyDetails::Farmers::AgeroApiRequest called #{context}"

          @policy_id = context.input[:policy_id]
        end

        # param needed: policy_id
        #
        def call
          raise ArgumentError, "policy_id supplied in a hash, e.g.: { policy_id: \"123\"" if @policy_id.blank?

          policy   = PCC::Policy.find(@policy_id)
          resource = API_RESOURCE_TEMPLATE.gsub(/\{policy_number\}/, policy.policy_number.to_s)

          query_string_hash = {
            loss_date: policy.data["loss_date"],
            policy_state_code: policy.data["policyStateCode"],
            coverage_system_source: policy.data["coverageSystemSource"],
          }

          Rails.logger.debug "PCC::PolicyDetails::Farmers::AgeroApiRequest about to call AgeroAPI with resource #{resource}"

          response = AgeroApi.get(resource: resource, query_string_hash: query_string_hash)

          if response.code == 200
            context.output = {
              policy_id: @policy_id,
              response: JSON.parse(response.body, object_class: OpenStruct),
            }
          else
            context.fail!(
              error: {
                message: 'Problem with Farmers Integration, please see the logs',
                code: '500',
              }
            )
          end
        end

      end
    end
  end
end
