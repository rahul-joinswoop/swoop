# frozen_string_literal: true

module PCC
  module Publishers
    class Websocket

      include Interactor
      include Interactor::Swoop

      # input: context.async_request_id
      # sideeffect: Events published
      # output: None
      def call
        request = API::AsyncRequest.find(context.async_request_id)

        # publish context.input to request.company
        channels = ["event_#{request.company.channel_name}", "event_#{Company.swoop.channel_name}"]

        msg = {
          class: "AsyncRequest",
          operation: "update",
          target: {
            id: request.id,
            data: context.input,
          },
        }

        msg = msg.to_json

        Rails.logger.debug "PCC:Publishers::Websocket Sending: #{msg}"

        channels.each do |channel|
          AsyncRedis.instance.publish(channel, msg)
        end
      end

    end
  end
end
