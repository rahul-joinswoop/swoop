# frozen_string_literal: true

module PCC
  module NewClientSetup
    class AddPolicyLookupTypesSetting

      include Interactor
      include SettingUpdater

      def call
        create_or_update_setting!(Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES)
      end

    end
  end
end
