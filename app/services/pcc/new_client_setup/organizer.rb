# frozen_string_literal: true

#
# This is a helper to make it easier to setup the Policy Lookup Server and Coverage engine to client companies.
# This will likely be used by seed files, and maybe it can be reused by Swoop UI in the future (moving towards clients being setup by root admins).
#
# We can add more capabilities to this service once we feel confident about it (like add ClientPrograms, or any other new settings)
#
# Example on how to use it:
#
# PCC::NewClientSetup::Organizer.call(
#   company: company,
#   policy_lookup_client_settings: {
#     Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE => 'efm',
#     Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => [PCC::Policy::MANUAL, PCC::Policy::VIN]
#     Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE => ['customer_lookup_type', 'year', 'service'],
#   }
# )
#
# All these keys ^ are required. The Interactors will fail the context in case any of these is missing. It's up
# to clients to treat it. The failure message will be included into context.message .
#
module PCC
  module NewClientSetup
    class Organizer

      include Interactor::Organizer

      organize AddClientCodeSetting, AddPolicyLookupTypesSetting, AddRequiredFieldsForCoverageSetting

    end
  end
end
