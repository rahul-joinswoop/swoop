# frozen_string_literal: true

module PCC
  module NewClientSetup
    module SettingUpdater

      def create_or_update_setting!(setting_key)
        client_code = context.policy_lookup_client_settings[setting_key]

        if client_code
          context.company.create_or_update_setting!(
            key: setting_key, value: client_code,
          )
        else
          context.fail!(message: "Can't set '#{setting_key}' because its value is blank")
        end
      end

    end
  end
end
