# frozen_string_literal: true

module PCC
  module NewClientSetup
    class AddClientCodeSetting

      include Interactor
      include SettingUpdater

      def call
        create_or_update_setting!(Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE)
      end

    end
  end
end
