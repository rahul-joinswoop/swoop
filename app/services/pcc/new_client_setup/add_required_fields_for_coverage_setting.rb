# frozen_string_literal: true

module PCC
  module NewClientSetup
    class AddRequiredFieldsForCoverageSetting

      include Interactor
      include SettingUpdater

      def call
        create_or_update_setting!(Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE)
      end

    end
  end
end
