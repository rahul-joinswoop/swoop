# frozen_string_literal: true

module PCC
  class PCCService

    class MissingPCCWorkerClass < StandardError; end

    FARMERS = 'Farmers'
    POLICY_LOOKUP_SERVICE = 'PolicyLookupService'

    class << self

      include LoggableJob

      # PCC::PCCService.policy_lookup(job_id:, user: User.find(1), {policy_number:<num>})
      def policy_lookup(job_id:, api_user:, search_terms:)
        job = Job.find(job_id)
        company = job.fleet_company
        pls_name = pcc_service_name(company)

        clz_name = "PCC::PolicyLookup::#{pls_name}::Worker"
        clazz_constant = clazz_constant_for(clz_name, company.id)

        async_request_system = case pls_name
                               when FARMERS # Farmers and 21st Century specific
                                 API::AsyncRequest::FARMERS_COVERAGE
                               when POLICY_LOOKUP_SERVICE # clients of POLICY_LOOKUP_SERVICE (likely to be all the other client companies)
                                 API::AsyncRequest::POLICY_LOOKUP_SERVICE
                               end

        async_request = API::AsyncRequest.create!(
          company: company,
          user: api_user,
          request: {
            method: 'PolicyLookup',
            params: {
              job_id: job_id,
              search_terms: search_terms,
            },
          },
          system: async_request_system
        )

        # Kick this off on queue
        clazz_constant.perform_async(async_request.id, job_id, search_terms)

        # render response with async_request
        {
          async_request: {
            id: async_request.id,
          },
        }
      end

      # PCC::PCCService.policy_details(
      #    policy_id:, company: Company.farmers, user: User.where(company: Company.farmers).last
      # )
      def policy_details(policy_id:, api_user:)
        policy = Policy.find(policy_id)
        company = policy.job.fleet_company

        clz_name = "PCC::PolicyDetails::#{pcc_service_name(company)}::Worker"
        clazz_constant = clazz_constant_for(clz_name, company.id)

        async_request = API::AsyncRequest.create!(
          company: company,
          user: api_user,
          request: {
            method: 'PolicyDetails',
            params: {
              policy_id: policy_id,
            },
          }
        )

        # Kick this off on queue
        clazz_constant.perform_async(async_request.id, policy_id)

        # render response with async_request
        {
          async_request: {
            id: async_request.id,
          },
        }
      end

      # PCC::PCCService.coverage_details(policy_id: 51, pcc_vehicle_id: 66, pcc_driver_id: 69, user: User.where(company: Company.farmers).last)
      def coverage_details(policy_id:, pcc_vehicle_id:, pcc_driver_id:, api_user:)
        policy = Policy.find(policy_id)
        company = policy.job.fleet_company

        clz_name = "PCC::CoverageDetails::#{pcc_service_name(company)}::Worker"
        clazz_constant = clazz_constant_for(clz_name, company.id)

        async_request = API::AsyncRequest.create!(
          company: company,
          user: api_user,
          request: {
            method: 'CoverageDetails',
            params: {
              policy_id: policy_id,
              pcc_vehicle_id: pcc_vehicle_id,
              pcc_driver_id: pcc_driver_id,
            },
          }
        )

        # Kick this off on queue
        clazz_constant.perform_async(async_request.id, policy_id, pcc_vehicle_id, pcc_driver_id)

        # render response with async_request
        {
          async_request: {
            id: async_request.id,
          },
        }
      end

      # PCC::PCCService.coverage_details(policy_id: 51, pcc_vehicle_id: 66, pcc_driver_id: 69, user: User.where(company: Company.farmers).last)
      def coverage(args:, api_user:)
        job = Job.safe_find(api_user, args[:job][:id])

        if args[:pcc_policy_id]
          policy = PCC::Policy.find_by!(id: args[:pcc_policy_id],
                                        job_id: job.id)
        else
          policy = nil
        end

        company = job.fleet_company

        clz_name = "PCC::CoverageDetails::#{pcc_service_name(company)}::Worker"
        clazz_constant = clazz_constant_for(clz_name, company.id)

        async_request = API::AsyncRequest.create!(
          company: company,
          user: api_user,
          request: {
            method: 'CoverageDetails',
            params: {
              args: args,
            },
          }
        )

        # Kick this off on queue
        clazz_constant.perform_async(async_request.id, job.id, policy&.id, args)

        # render response with async_request
        {
          async_request: {
            id: async_request.id,
          },
        }
      end

      # PCC::PCCService.claim_creation(job: Job.find(1180))
      #
      # It will likely only be called after a job creation in the case when a claim should be POSTed on the Fleet system through systems integration.
      def claim_creation(job:)
        # If job.service is WinchOut, and question "Within 10 ft of a paved surface?" is "No", then it's not covered
        if job.service_code.name == ServiceCode::WINCH_OUT
          question_result = job.question_results
            .joins(:question)
            .where(questions: { question: Question::WITHIN_10_FEET_OF_PAVED_SURFACE })
            .take

          if question_result.present? && (question_result.answer&.answer == "No")
            log :info,
                "PCC_COVERAGE(#{job.pcc_coverage_id}) - " \
                "PCC::ClaimCreation - PCC::PCCService.claim_creation - Claim not created: job not covered since its service_location is Winch Out " \
                "and #{Question::WITHIN_10_FEET_OF_PAVED_SURFACE.inspect} answer is no",
                job: job.id

            return
          end
        end

        pcc_coverage = PCC::Coverage.find(job.pcc_coverage_id)

        if pcc_coverage.exceeded_service_count_limit
          log :info,
              "PCC_COVERAGE(#{job.pcc_coverage_id}) - " \
              "PCC::ClaimCreation - PCC::PCCService.claim_creation - Claim not created: job not covered since its coverage has exceeded its service count limit.",
              job: job.id

          return
        end

        if !pcc_coverage.covered
          log :info,
              "PCC_COVERAGE(#{job.pcc_coverage_id}) - " \
              "PCC::ClaimCreation - PCC::PCCService.claim_creation - Claim not created: coverage is not covered.",
              job: job.id

          return
        end

        company = job.fleet_company

        clz_name = "PCC::ClaimCreation::#{pcc_service_name(company)}::Worker"
        clazz_constant = clazz_constant_for(clz_name, company.id)

        # Kick this off on queue
        clazz_constant.perform_async(job.id)
      end

      private

      # It's basically here yet only to support it for 21st Century, since it uses a
      # legacy Agero/Farmers endpoint. All other clients use POLICY_LOOKUP_SERVICE
      # by default.
      #
      # note: it can be removed once we migrate 21st Century to use POLICY_LOOKUP_SERVICE
      # (in which case all spots that rely on this method can be refactored to use POLICY_LOOKUP_SERVICE)
      def pcc_service_name(company)
        pcc_service_name = Setting.get_setting(company, Setting::PCC_SERVICE_NAME)

        pcc_service_name || POLICY_LOOKUP_SERVICE
      end

      def clazz_constant_for(clazz_name, company_id)
        clazz_name.constantize
      rescue NameError
        raise MissingPCCWorkerClass, "#{clazz_name} not found for company(#{company_id})"
      end

    end

  end
end
