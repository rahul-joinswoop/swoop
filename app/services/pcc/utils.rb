# frozen_string_literal: true

module PCC
  class Utils

    def self.id_with_13_chars(id)
      ("%13.13s" % id).gsub(' ', 'x')
    end

    def self.id_with_9_chars(id)
      ("%9.9s" % id).gsub(' ', 'x')
    end

  end
end
