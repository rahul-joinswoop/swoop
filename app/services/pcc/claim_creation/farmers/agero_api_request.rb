# frozen_string_literal: true

#
#   json_string_example = File.read(File.expand_path File.dirname(__FILE__) + "/examples/policy_and_claims/farmers_claim_creation_complete.json")
#
#   json = JSON.parse(json_string_example)
#
#   request =
#    PCC::ClaimCreation::Farmers::AgeroApiRequest.call(input: {
#     claim_json: json,
#     job_id: job_id
#   }).output
#

# TODO agree on type it will receive in context.input[:claim_json], for now it's a JSON object
module PCC
  module ClaimCreation
    module Farmers
      class AgeroApiRequest

        include LoggableJob
        include Interactor
        include Interactor::Swoop

        API_RESOURCE = '/claims'

        before do
          Rails.logger.debug "PCC::ClaimCreation::Farmers::AgeroApiRequest called #{context}"

          @claim_json      = context.input[:claim_json]
          @job_id          = context.input[:job_id]
        end

        # Take context.input[:claim_json] from FormatAgeroApiClaimJson and POST it to AgeroAPI
        def call
          raise ArgumentError, 'claim_json and job_id must be supplied in a hash, e.g.: { claim_json: json_object, job_id: 123 }' unless @claim_json.present? && @job_id.present?

          response = AgeroApi.post(
            resource: API_RESOURCE,
            json: @claim_json.to_json
          )

          if response.code == 201
            context.output = {
              response: JSON.parse(response.body, object_class: OpenStruct),
              job_id: @job_id,
            }
          else
            log :error, "- PCC::ClaimCreation::Farmers::AgeroApiRequest - Could not create a claim on Agero.", job: @job_id
            raise PCC::ClaimCreation::Error, "#{response.code}: #{response.body}"
          end
        end

      end
    end
  end
end
