# frozen_string_literal: true

module PCC
  module ClaimCreation
    module Farmers
      # input: take context.response and store the claim as PCC::Claim
      #
      # sideeffect: db row created
      #
      # output: context.coverage <PCC::Claim>
      class AgeroResponseDBSerializer

        include Interactor
        include Interactor::Swoop

        before do
          Rails.logger.debug "PCC::ClaimCreation::AgeroResponseDBSerializer called #{context}"

          @agero_response  = context.input[:response]
          @job_id          = context.input[:job_id]

          raise ArgumentError, 'PCC::ClaimCreation::Farmers::AgeroResponseDBSerializer agero_response is null' unless @agero_response

          raise ArgumentError, 'PCC::ClaimCreation::Farmers::AgeroResponseDBSerializer agero_response not open struct' unless @agero_response.class.name == OpenStruct.name

          raise ArgumentError, 'PCC::ClaimCreation::Farmers::AgeroResponseDBSerializer job_id is null' unless @job_id
        end

        def call
          parser = ClaimCreationParser.new(@agero_response)

          ApplicationRecord.transaction do
            job = Job.find(@job_id)

            pcc_coverage = PCC::Coverage.find(job.pcc_coverage_id)
            pcc_policy   = pcc_coverage.pcc_policy
            pcc_driver   = pcc_coverage.pcc_driver
            pcc_vehicle  = pcc_coverage.pcc_vehicle

            # Claim allows pcc_policy_id, pcc_coverage_id, pcc_driver_id and pcc_vehicle_id values so to let it be
            # as flexible as possible for eventual new integration with other Fleets
            claim = PCC::Claim.create!(
              company_id: job.fleet_company_id,
              pcc_policy_id: pcc_policy.id,
              pcc_coverage_id: pcc_coverage.id,
              pcc_driver_id: pcc_driver.id,
              pcc_vehicle_id: pcc_vehicle.id,
              claim_number: parser.claim_number,
              data: @agero_response
            )

            job.pcc_claim_id = claim.id

            job.save!

            context.output = claim
          end
        end

      end
    end
  end
end
