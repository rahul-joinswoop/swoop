# frozen_string_literal: true

# json = PCC::ClaimCreation::Farmers::CreateClaimJson.call(input: { job_id: 1175 }).output
module PCC
  module ClaimCreation
    module Farmers
      class CreateClaimJson

        include Interactor
        include Interactor::Swoop

        before do
          @coverage = context.input
        end

        # input: take context.input from Organizer
        # output: JSON hash with a new claim formatted to be POSTed to AgeroAPI
        before do
          Rails.logger.debug "PCC::ClaimCreation::Farmers::CreateClaimJson called #{context}"

          @job_id = context.input[:job_id]
        end

        def call
          if @job_id.blank?
            raise ArgumentError, "PCC::ClaimCreation::Farmers::CreateClaimJson " \
              "job_id must be supplied in a hash, e.g.: input: { job_id: <job_id> }"
          end

          @job = Job.find(@job_id)

          @coverage = PCC::Coverage.find(@job.pcc_coverage_id)
          @policy   = @coverage.pcc_policy
          @driver   = @coverage.pcc_driver
          @vehicle  = @coverage.pcc_vehicle

          claim_json = claim_basic_attributes
          claim_json[:disablementInfo] = claim_disablement_info_attributes
          claim_json[:policy] = claim_policy_attributes
          claim_json[:driver] = claim_driver_attributes

          context.output = {
            claim_json: claim_json,
            job_id: @job_id,
          }
        end

        private

        def claim_basic_attributes
          basic_attributes = {}

          basic_attributes[:caseNumber] = PCC::Utils.id_with_13_chars(@job.id)
          basic_attributes[:transactionId] = @policy.data["transactionId"]
          basic_attributes[:coverageReferenceNumber] = @coverage.coverage_number
          basic_attributes[:accident] = ((@job.service_code.name == ServiceCode::ACCIDENT_TOW) ? true : false)
          basic_attributes[:purchaseOrderNumber] = PCC::Utils.id_with_9_chars(@job.id)

          basic_attributes
        end

        def claim_disablement_info_attributes
          attributes = {}

          # we need a map to transform our service into one that Farmers accepts
          attributes[:disablementReason] = disablement_reason_for_symtom(@job.symptom&.name)
          attributes[:disablementLocation] = disablement_info_location_attributes
          attributes[:lossDate] = @policy.data['loss_date']

          attributes
        end

        def disablement_reason_for_symtom(job_symtom_name)
          case job_symtom_name
          when "Accident"
            "Accident"
          when "Flat tire"
            "Flat Tire"
          when "Inoperable Problem"
            "Inoperable Problem"
          when "Locked out"
            "Lockout"
          when "Out of fuel"
            "Out of Fuel"
          when "Specialty Vehicle/RV Disablement"
            "Specialty Vehicle/RV Disablement"
          when "Stuck"
            "Stuck"
          when "Would not Start"
            "Would not Start"
          else # will like happen if a new Symptom is added to Farmers
            "Stuck"
          end
        end

        def disablement_info_location_attributes
          job_service_location = @job.service_location

          attributes = {}

          attributes[:addressType] = job_service_location.location_type&.name || 'Not defined'
          attributes[:street] = job_service_location.street || 'Not defined'
          attributes[:city] = job_service_location.city || 'Not defined'
          attributes[:state] = job_service_location.state || 'Not defined'
          attributes[:zip] = job_service_location.zip
          attributes[:crossStreet] = 'Not defined' # TODO Required by AgeroAPI, do we have it?

          attributes
        end

        def claim_policy_attributes
          attributes = {}

          attributes[:policyStateCode] = @policy.data['policyStateCode']
          attributes[:policyNumber] = @policy.policy_number
          attributes[:coverageSystemSource] = @policy.data['coverageSystemSource']
          attributes[:agent] = policy_agent_attributes

          attributes
        end

        def policy_agent_attributes
          @policy_agent_data = @policy.data['agent']['table']

          attributes = {}

          attributes[:contactInfo] = agent_contact_info_attributes
          attributes[:stateCode] = @policy_agent_data['stateCode']
          attributes[:districtCode] = @policy_agent_data['districtCode']
          attributes[:code] = @policy_agent_data['agentCode']
          attributes[:firstName] = @policy_agent_data['firstName']
          attributes[:lastName] = @policy_agent_data['lastName']

          attributes
        end

        def agent_contact_info_attributes
          contact_info_data = @policy.data.dig('agent', 'table', 'contactInfo', 'table')

          attributes = {}

          attributes[:phoneNumber] = contact_info_data['phoneNumber']
          attributes[:faxNumber] = contact_info_data['faxNumber']

          attributes[:address] = agent_address_attributes

          attributes
        end

        def agent_address_attributes
          address_data = @policy.data.dig('agent', 'table', 'contactInfo', 'table', 'address', 'table')

          attributes = {}

          attributes[:street] = address_data['street']
          attributes[:city] = address_data['city']
          attributes[:state] = address_data['state']
          attributes[:zip] = address_data['zip']

          attributes
        end

        def claim_driver_attributes
          attributes = {}

          attributes[:firstName] = @driver.data.dig('table', 'firstName')
          attributes[:middleName] = nil
          attributes[:lastName] = @driver.data.dig('table', 'lastName')
          attributes[:contactInfo] = driver_contact_info_attributes
          attributes[:vehicle] = driver_vehicle_attributes

          # commented out as not required (as of 2018/mar/27)
          # attributes[:permissive] = false # TODO do need it? Where do we get it?

          attributes
        end

        def driver_contact_info_attributes
          attributes = {}

          attributes[:phoneNumber] = @job.customer&.phone&.gsub(/\D/, '')
          attributes[:address] = driver_address_attributes

          attributes
        end

        def driver_address_attributes
          policy_holder_address = @policy.data.dig('policyHolder', 'table', 'contactInfo', 'table', 'address', 'table')

          attributes = {}

          attributes[:street] = policy_holder_address['street'] || 'Not defined'
          attributes[:city] = policy_holder_address['city'] || 'Not defined'
          attributes[:state] = policy_holder_address['state'] || 'Not defined'
          attributes[:zip] = policy_holder_address['zip'] || 'Not defined'

          attributes
        end

        def driver_vehicle_attributes
          attributes = {}

          attributes[:policyUnit] = @vehicle.data.dig('table', 'policyUnit')
          attributes[:vin] =  @vehicle.vin
          attributes[:make] = @vehicle.make
          attributes[:model] = @vehicle.model
          attributes[:year] = @vehicle.year

          attributes
        end

      end
    end
  end
end
