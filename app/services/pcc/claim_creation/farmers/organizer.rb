# frozen_string_literal: true

#
# ** create a Policy first:
# PCC::PCCService.policy_lookup(policy_number: "0103293256", company: Company.farmers, user: User.where(company: Company.farmers).last)
#
# ** then call the PolicyDetails organizer:
# drivers_and_cars = PCC::PolicyDetails::Farmers::Organizer.call(input: { policy_id: <policy_id created on the policy_lookup call> })
#
# ** then call the CoverageDetails organizer:
# coverage_details_request =
#  PCC::CoverageDetails::Farmers::AgeroApiRequest.call(input: {
#   policy_id: <policy_id>,
#   vehicle_id: <pcc_vehicle_id>
# })
#
# ** then we'll need a job id (for testing purposes, it can be any of the DB)
#
# ** then call the ClaimCreation organizer:
#
# claim_creation = PCC::ClaimCreation::Farmers::Organizer.call(input: {
#   job_id: job_id,    # the job_id
# })
#
# ** The result is a PCC::Claim created into our DB associated it with its respective PCC::Policy and the Job.
#    The job will be saved during the process so FE will receive a WS with the job associated with the claim.
module PCC
  module ClaimCreation
    module Farmers
      class Organizer

        include Interactor::Organizer
        include Interactor::Swoop

        organize CreateClaimJson, AgeroApiRequest, AgeroResponseDBSerializer

      end
    end
  end
end
