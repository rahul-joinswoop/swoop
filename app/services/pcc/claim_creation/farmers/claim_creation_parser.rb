# frozen_string_literal: true

module PCC
  module ClaimCreation
    module Farmers
      class ClaimCreationParser

        attr_reader :data

        def initialize(open_struct)
          raise ArgumentError, 'PCC::ClaimCreation::Farmers::Parser agero_response not open struct' unless
            open_struct.class.name == OpenStruct.name

          @data = open_struct
        end

        def claim_number
          @data.claimNumber
        end

      end
    end
  end
end

#
# {
#   claimId="ZFS-1-862PH94",
#   claimNumber="2001001651-1",
#   claimUnitId="ZFS-1-862XKG1",
#   claimUnitNumber="2001001651-1-1",
#   purchaseOrderNumber="179483855",
#   caseReferenceNumber="0000000001175"
# }
