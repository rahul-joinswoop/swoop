# frozen_string_literal: true

# Error raised if there is any problem creating a claim.
module PCC
  module ClaimCreation
    class Error < StandardError
    end
  end
end
