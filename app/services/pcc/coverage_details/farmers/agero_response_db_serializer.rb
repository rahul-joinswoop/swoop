# frozen_string_literal: true

module PCC
  module CoverageDetails
    module Farmers
      # input: take context.response and store the coverage as PCC::Coverage
      #
      # sideeffect: db row created
      #
      # output: context.coverage <PCC::Coverage>
      class AgeroResponseDBSerializer

        include Interactor
        include Interactor::Swoop

        before do
          Rails.logger.debug "PCC::CoverageDetails::AgeroResponseDBSerializer called #{context}"

          @agero_response = context.input[:response]
          @policy_id      = context.input[:policy_id]
          @pcc_vehicle_id = context.input[:pcc_vehicle_id]
          @pcc_driver_id  = context.input[:pcc_driver_id]

          raise ArgumentError, 'PCC::PolicyDetails::Farmers::AgeroResponseDBSerializer agero_response is null' unless @agero_response

          raise ArgumentError, 'PCC::PolicyDetails::Farmers::AgeroResponseDBSerializer agero_response not open struct' unless @agero_response.class.name == OpenStruct.name

          raise ArgumentError, 'PCC::PolicyDetails::Farmers::AgeroResponseDBSerializer policy_id is null' unless @policy_id

          raise ArgumentError, 'PCC::PolicyDetails::Farmers::AgeroResponseDBSerializer pcc_vehicle_id is null' if @pcc_vehicle_id.blank?

          raise ArgumentError, 'PCC::PolicyDetails::Farmers::AgeroResponseDBSerializer pcc_driver_id is null' if @pcc_driver_id.blank?
        end

        def call
          parser = CoverageDetailsParser.new(@agero_response)

          ApplicationRecord.transaction do
            coverage = PCC::Coverage.where(
              coverage_number: parser.coverage_number,
              pcc_policy_id: @policy_id,
              pcc_vehicle_id: @pcc_vehicle_id,
              pcc_driver_id: @pcc_driver_id
            ).first_or_initialize

            coverage.covered = parser.covered
            coverage.exceeded_service_count_limit = parser.exceeded_service_count_limit
            coverage.data = @agero_response

            coverage.save!

            context.output = coverage
          end
        end

      end
    end
  end
end
