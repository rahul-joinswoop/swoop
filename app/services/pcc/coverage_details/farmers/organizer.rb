# frozen_string_literal: true

#
# ** create a Policy first:
# PCC::PCCService.policy_lookup(policy_number: "0103293256", company: Company.farmers, user: User.where(company: Company.farmers).last)
#
# ** then call the PolicyDetails organizer:
# drivers_and_cars = PCC::PolicyDetails::Farmers::Organizer.call(input: { policy_id: <policy_id created on the policy_lookup call> })
#
# ** then call the CoverageDetails organizer:
# coverage = PCC::PolicyDetails::Farmers::Organizer.call(input: {
#   policy_id: <policy_id created on the policy_lookup call>,
#   pcc_vehicle_id: <one vehicle_id created on the drivers_and_cars call>,
#   pcc_driver_id:  <one vehicle_id created on the drivers_and_cars call>,
# })
#
# ** It will create a PCC::Coverage into our DB and associate it with its respective PCC::Vehicle.
# ** The result will be The PCC::Coverage serialized by PCC::CoverageSerializer.
#
module PCC
  module CoverageDetails
    module Farmers
      class Organizer

        include Interactor::Organizer
        include Interactor::Swoop

        organize AgeroApiRequest, AgeroResponseDBSerializer, FormatSwoopApiResponse

      end
    end
  end
end
