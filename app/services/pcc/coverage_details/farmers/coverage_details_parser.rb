# frozen_string_literal: true

module PCC
  module CoverageDetails
    module Farmers
      class CoverageDetailsParser

        attr_reader :data

        def initialize(open_struct)
          raise ArgumentError, 'PCC::PolicyCoverage::Farmers::Parser agero_response not open struct' unless
            open_struct.class.name == OpenStruct.name

          @data = open_struct
        end

        def covered
          @data.coverageStatus
        end

        def exceeded_service_count_limit
          @data.exceededFuelServiceCountLimit || @data.exceededRoadsideServiceCountLimit
        end

        def coverage_number
          @data.coverageReferenceNumber
        end

      end
    end
  end
end

#
# {
#   coverageReferenceNumber="HX874356",
#   coverageStatus=true,
#   coverageLimit=150.0,
#   coverageDescription="TOW COVERED BY TOW COVERAGE",
#   exceededFuelServiceCountLimit=false,
#   exceededRoadsideServiceCountLimit=false
# }
#
