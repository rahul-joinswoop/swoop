# frozen_string_literal: true

module PCC
  module CoverageDetails
    module Farmers
      class FormatSwoopApiResponse

        include Interactor
        include Interactor::Swoop

        before do
          @coverage = context.input
        end

        # input: take context.coverage from AgeroResponseDbSerializer
        # output: JSON hash suitable for returning to the UI
        # swoop_response: {
        #   coverage: {
        #     :id, :pcc_vehicle_id, :pcc_driver_id, :covered, :exceeded_service_count_limit
        #   }
        # }
        def call
          context.output = ActiveModelSerializers::SerializableResource.new(
            @coverage
          ).serializable_hash
        end

      end
    end
  end
end
