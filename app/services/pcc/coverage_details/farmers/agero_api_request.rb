# frozen_string_literal: true

#
#  coverage_details_request =
#    PCC::CoverageDetails::Farmers::AgeroApiRequest.call(input: {
#     policy_id: <policy_id>,
#     pcc_vehicle_id: <vehicle_id>,
#     pcc_driver_id: <driver_id>
#   })
#
module PCC
  module CoverageDetails
    module Farmers
      class AgeroApiRequest

        include Interactor
        include Interactor::Swoop

        API_RESOURCE_TEMPLATE = '/profiles/{policy_number}/vehicles/{policy_unit}/coverages'

        before do
          Rails.logger.debug "PCC::CoverageDetails::Farmers::AgeroApiRequest called #{context}"

          @policy_id = context.input[:policy_id]
          @pcc_vehicle_id = context.input[:pcc_vehicle_id]
          @pcc_driver_id  = context.input[:pcc_driver_id]
        end

        def call
          raise ArgumentError, "policy_id, pcc_vehicle_id and pcc_driver_id must be supplied in a hash, e.g.: { policy_id: \"123\", pcc_vehicle_id: \"97\", pcc_driver_id: \"100\" }" if
            [@policy_id, @pcc_vehicle_id, @pcc_driver_id].any? { |param| param.nil? }

          policy = PCC::Policy.find(@policy_id)
          vehicle = PCC::Vehicle.find(@pcc_vehicle_id)

          @policy_number = policy.policy_number # Policy number for which coverage is being requested Example: 0163789146
          @policy_unit = vehicle.data["table"]["policyUnit"] # Policy unit for which coverage is being requested, that is returned in Profile Search Example: 1
          @policy_state_code = policy.data["policyStateCode"] # Policy State Code if available from policy search Example: 96
          @loss_date = policy.data["loss_date"]
          @coverage_system_source = policy.data["coverageSystemSource"] # CoverageSystemSource code returned in policy Search Example: APPSCE

          resource = API_RESOURCE_TEMPLATE.gsub(/\{policy_number\}/, @policy_number.to_s).gsub(/\{policy_unit\}/, @policy_unit.to_s)

          query_string_hash = {
            policy_state_code: @policy_state_code,
            loss_date: @loss_date,
            coverage_system_source: @coverage_system_source,
            case_number: PCC::Utils.id_with_13_chars(policy.job_id),
          }

          Rails.logger.debug "PCC::CoverageDetails::Farmers::AgeroApiRequest about to call AgeroAPI with resource #{resource}"

          response = AgeroApi.get(resource: resource, query_string_hash: query_string_hash)

          if response.code == 200
            context.output = {
              policy_id: @policy_id,
              pcc_vehicle_id: @pcc_vehicle_id,
              pcc_driver_id: @pcc_driver_id,
              response: JSON.parse(response.body, object_class: OpenStruct),
            }
          else
            context.fail!(
              error: {
                message: 'Problem with Farmers Integration, please see the logs',
                code: '500',
              }
            )
          end
        end

      end
    end
  end
end
