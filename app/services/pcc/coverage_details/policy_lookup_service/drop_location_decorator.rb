# frozen_string_literal: true

module PCC
  module CoverageDetails
    module PolicyLookupService
      #
      # This class populates the coverage_args with the dropoff hash, e.g.:
      # dropoff: {
      #   type: 'Dealership',
      #   distance: 12.456, # miles
      # }
      # Type can only be populated if dropoff exists and is a Site(not just a lat/lng)
      # Distance can only be populated if pickup and dropoff exist with lat/lng's
      #
      class DropLocationDecorator

        include Interactor
        include Interactor::Swoop

        before do
          @request_args = context.input[:request_args]
          @job = context.input[:job]
        end

        def call
          Rails.logger.debug "PCC(#{@job.id}) DropLocationDecorator request_args #{@request_args}"

          coverage_args_drop_location = drop_location_coverage

          context.output = context.input
          context.output[:coverage_args][:dropoff].merge!(coverage_args_drop_location) if coverage_args_drop_location
          Rails.logger.debug("DROPOFF:  #{coverage_args_drop_location}, merged: #{context.output[:coverage_args][:dropoff]}")
        end

        private

        def drop_location_coverage()
          coverage_args_drop_location = nil

          dropoff_location_args = @request_args.dig('job', 'location', 'dropoff_location')
          if dropoff_location_args
            destination = nil

            site_id = dropoff_location_args['site_id']
            if site_id
              destination = Site.find_by!(id: site_id, company_id: @job.fleet_company_id).location
              coverage_args_drop_location = {
                type: destination.location_type&.name,
              }
            else
              destination = Location.new(lat: dropoff_location_args['lat'],
                                         lng: dropoff_location_args['lng'])
            end

            service_location_args = @request_args.dig('job', 'location', 'service_location')
            return coverage_args_drop_location if !service_location_args

            origin = Location.new(lat: service_location_args['lat'],
                                  lng: service_location_args['lng'])
            results = External::GoogleDistanceMatrixService.calculate(origin, destination, nil)
            if results && (results.length == 1)
              meters, _seconds = results[0]
              miles = (meters * Constant::METERS_MILES).round(1)
              if coverage_args_drop_location
                coverage_args_drop_location[:distance] = miles
              else
                coverage_args_drop_location = {
                  type: nil,
                  distance: miles,
                }
              end
            end

          end

          coverage_args_drop_location
        end

      end
    end
  end
end
