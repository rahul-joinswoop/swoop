# frozen_string_literal: true

#
# Apply a Coverage Rule over the Policy Client Program, in case a Policy exists.
# Otherwise, execute the Manual Coverage flow automatically.
#
# ClientPrograms are specific to Client companies, so the Coverage rule is always applied to the @job.fleet_company.
# (terminology: fleet_company == client_company)
#
# If @force_client_api_coverage_override is true, then it will run the client_api_covered_flow.
# It's a special flow primarily only triggered by the ClientAPI create/update job mutation.
# @see API::Jobs::ActivateJob, it contains the logic that will allow triggering it.
#
# If a Policy exists, it means that user has looked for it in the UI and it has been found.
#
# If a Policy does not exist, it means that either:
#   - the user has looked for a Policy in the UI and it has NOT been found
#   - the user has opted for a 'Manual Entry' in the UI instead of looking for a Policy
#
module PCC
  module CoverageDetails
    module PolicyLookupService
      class ProcessCoverageResult

        include Interactor
        include Interactor::Swoop

        before do
          @policy = context.input[:policy]
          @request_args = (context.input[:request_args] || {}).with_indifferent_access
          @job = context.input[:job]
          @force_client_api_coverage_override = context.input[:force_client_api_coverage_override]

          request_job_service_name = @request_args.dig(:job, :service, :name)
          if request_job_service_name
            @job_service_code = ServiceCode.where.not(addon: true).find_by(name: request_job_service_name)
          end
        end

        def call
          Rails.logger.debug "PCC(#{@job.id}) #{self.class.name} called"

          coverage_args = context.input[:coverage_args]

          construct_args(coverage_args, :vehicle, :odometer, ["job", "vehicle", "odometer"])
          construct_args(coverage_args, :vehicle, :year, ["job", "vehicle", "year"])
          construct_args(coverage_args, :job, :service, ["job", "service", "name"])

          program_results = []

          if @force_client_api_coverage_override
            #
            # Run the "Client API Covered" program, which will
            # result as Covered automatically.
            #
            program_results = client_api_covered_flow
          elsif @policy
            #
            # If we have policy data, then use the associated programs to populate the response.
            #
            # It means that user has looked for a policy info in the UI and it has been found.
            #
            program_results = policy_based_coverage(coverage_args, @policy.data["programs"])
          else
            #
            # Otherwise, do a manual coverage check.
            #
            # This 'else' case can be achieved when:
            #
            # - the user has looked for a policy info in the UI and it has NOT been found
            # - the user has opted for a 'Manual Entry' in the UI
            #
            program_results = [manual_coverage(coverage_args)]
          end

          Rails.logger.debug "PCC(#{@job.id}) results:#{program_results}"
          context.output = context.input
          context.output[:program_results] = program_results
        end

        private

        def client_api_covered_flow
          client_program = ClientProgram.fetch_or_create_client_api_covered!(@job.fleet_company_id)

          # TODO: this block and the one used in manual_coverage are the same,
          # these could be refactored into a method to be used by both.
          client_api_coverage = run_coverage(client_program, {}).tap do |result|
            result[:name] = client_program.name
            result[:code] = client_program.code
            result[:rank] = client_program.rank
            result[:coverage_notes] =
              client_program.coverage_notes_by_filter_or_default(service_code: @job_service_code)
          end

          [client_api_coverage]
        end

        def manual_coverage(coverage_args)
          coverage_args[:program] = {}
          coverage_args[:program][:expired] = false

          client_program = ClientProgram.manual_program(@job.fleet_company_id)
          coverage_args[:custom_issues] = client_program.coverage_custom_issues

          manual_coverage_result = run_coverage(client_program, coverage_args)

          manual_coverage_result[:name] = client_program.name
          manual_coverage_result[:code] = client_program.code
          manual_coverage_result[:rank] = client_program.rank
          manual_coverage_result[:coverage_notes] =
            client_program.coverage_notes_by_filter_or_default(service_code: @job_service_code)

          manual_coverage_result[:priority] = client_program.priority

          manual_coverage_result
        end

        def policy_based_coverage(coverage_args, programs)
          program_results = []

          programs.each do |program|
            Rails.logger.debug "Running program: #{program}"
            coverage_args[:program] = {}
            coverage_args[:program][:expired] = program['expired'] if program.key?('expired')

            client_program = ClientProgram.find_by(code: program['program_code'], company_id: @job.fleet_company_id)

            populate_program_args(coverage_args, program)

            output = run_coverage(client_program, coverage_args)

            output[:name] = program["program_name"]
            output[:code] = program["program_code"]
            output[:site_code] = program["site_code"]
            output[:rank] = client_program.rank
            output[:coverage_notes] =
              client_program.coverage_notes_by_filter_or_default(service_code: @job_service_code)

            output[:priority] = client_program.priority

            program_results << output
          end

          program_results
        end

        def populate_program_args(coverage_args, program)
          if program.dig("details")
            # extract details
            ["odometer_start", "odometer_limit"].each do |attribute|
              Rails.logger.debug "program:#{program}"
              coverage_args[:program][attribute.to_sym] = program.dig("details", attribute) if program.dig("details", attribute)
            end
          end
        end

        def run_coverage(client_program, coverage_args)
          begin
            Rails.logger.debug "PCC(#{@job.id}) #{self.class.name} Calling engine with coverage_args:#{coverage_args}"

            coverage_engine = PolicyCoverage.get_engine(client_program.coverage_filename)
            output = coverage_engine.evaluate(coverage_args)
          rescue PolicyCoverage::Utils::MissingFieldError => e
            output = {
              result: 'Failed',
              failed_reasons: [e.to_s],
            }
          rescue Errno::ENOENT => e
            output = {
              result: 'Failed',
              failed_reasons: [
                "Failed to find coverage file",
                e.to_s,
              ],
            }
          end
          output
        end

        def construct_args(coverage_args, component, attribute, path)
          coverage_args[component][attribute] = @request_args.dig(*path) if @request_args.dig(*path)
        end

      end
    end
  end
end
