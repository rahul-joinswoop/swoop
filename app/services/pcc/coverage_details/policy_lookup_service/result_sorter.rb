# frozen_string_literal: true

module PCC
  module CoverageDetails
    module PolicyLookupService
      # Likely Lincoln specific
      class ResultSorter

        include Interactor
        include Interactor::Swoop

        before do
          @program_results = context.input[:program_results]
          @job = context.input[:job]
        end

        def call
          Rails.logger.debug "PCC(#{@job.id}) ResultSorter called"

          not_covered = []
          rest = []
          @program_results.each do |res|
            if res[:result] == 'NotCovered'
              not_covered << res
            else
              rest << res
            end
          end

          results = rest.sort_by { |hsh| hsh[:rank] } + not_covered.sort_by { |hsh| hsh[:rank] }

          context.output = context.input
          context.output[:program_results] = results
        end

      end
    end
  end
end
