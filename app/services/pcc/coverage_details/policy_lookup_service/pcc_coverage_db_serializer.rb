# frozen_string_literal: true

module PCC
  module CoverageDetails
    module PolicyLookupService
      class PCCCoverageDBSerializer

        include Interactor
        include Interactor::Swoop

        before do
          @policy = context.input[:policy]
          @program_results = context.input[:program_results]
          @job = context.input[:job]
          @request_args = context.input[:request_args]
        end

        def call
          Rails.logger.debug "PCC(#{@job.id}) PCCCoverageDBSerializer called"

          if @program_results.any?
            pcc_coverage = begin
              if @policy
                created_policy_based_coverage!
              else
                create_coverage_without_policy!
              end
            end

            context.output[:pcc_coverage_id] = pcc_coverage.id
            context.output[:pcc_policy_id] = pcc_coverage.pcc_policy_id
            context.output[:pcc_vehicle_id] = pcc_coverage.pcc_vehicle_id
            context.output[:pcc_driver_id] = pcc_coverage.pcc_driver_id
          end

          context.output = context.input
        end

        private

        def find_program_in_policy(program_code)
          @policy.data["programs"].detect { |program| program["program_code"] == program_code }
        end

        def create_coverage_without_policy!
          program = ClientProgram.find_by(
            company_id: @job.fleet_company_id,
            code: @program_results[0][:code],
          )

          ::PCC::Coverage.create!(
            result: @program_results[0][:result],
            data: @program_results,
            client_program: program
          )
        end

        def created_policy_based_coverage!
          result = @program_results[0]

          program = find_program_in_policy(result[:code])

          pcc_program = ::ClientProgram.find_by(code: program['program_code'],
                                                company_id: @job.fleet_company_id)
          if pcc_program.nil?
            begin
              pcc_program = ::ClientProgram.create!(name: program['program_name'],
                                                    code: program['program_code'],
                                                    client_identifier: program['client_identifier'],
                                                    company_id: @job.fleet_company_id)
            rescue ActiveRecord::RecordNotUnique
              pcc_program = ::ClientProgram.find_by(code: program['program_code'],
                                                    company_id: @job.fleet_company_id)
            end
          end

          ::PCC::Coverage.create!(
            pcc_policy_id: @policy.id,
            pcc_vehicle_id: @request_args["pcc_vehicle_id"],
            pcc_driver_id: @request_args["pcc_driver_id"],
            result: @program_results[0][:result],
            client_program: pcc_program,
            data: @program_results
          )
        end

      end
    end
  end
end
