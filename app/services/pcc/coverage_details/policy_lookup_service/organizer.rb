# frozen_string_literal: true

module PCC
  module CoverageDetails
    module PolicyLookupService
      class Organizer

        include Interactor::Organizer
        include Interactor::Swoop

        organize InitializeDecorator,
                 DropLocationDecorator,
                 SiteDecorator,
                 ProcessCoverageResult,
                 ResultSorter,
                 PCCCoverageDBSerializer

      end
    end
  end
end
