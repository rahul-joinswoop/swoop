# frozen_string_literal: true

module PCC
  module CoverageDetails
    module PolicyLookupService
      class InitializeDecorator

        include Interactor
        include Interactor::Swoop

        def call
          response = context.input

          response[:coverage_args] = {
            vehicle: {},
            job: {},
            dealer: {},
            dropoff: {},
          }
        end

      end
    end
  end
end
