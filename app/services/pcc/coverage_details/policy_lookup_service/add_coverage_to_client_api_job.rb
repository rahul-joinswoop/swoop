# frozen_string_literal: true

#
# If pcc_coverage_id is present, adds it to the incoming job.
#
# This Interactor is used by ClientAPIApprovedOrganizer for jobs updated
# by the Client API flow.
#
module PCC
  module CoverageDetails
    module PolicyLookupService
      class AddCoverageToClientAPIJob

        include Interactor
        include Interactor::Swoop

        before do
          @job = context.input[:job]
          @pcc_coverage_id = context.input[:pcc_coverage_id]
        end

        def call
          if @pcc_coverage_id
            @job.update! pcc_coverage_id: @pcc_coverage_id
          end

          context.output = context.input
        end

      end
    end
  end
end
