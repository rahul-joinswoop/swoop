# frozen_string_literal: true

module PCC
  module CoverageDetails
    module PolicyLookupService
      # This populates the coverage_args with site coverage args,
      # e.g
      #   dealer.nearest,
      #   dropoff.nearest_collision_center
      # which is sent to the coverage engine
      # It finds all Sites whose locations are of type 'Dealership' and orders them on road distance
      # If the dropoff site passed from the API call is the closest site
      class SiteDecorator

        include Interactor
        include Interactor::Swoop

        # The number of miles to add and subtract around the pickup location to quickly eliminate
        # dealers that won't be used in the calculation (before sending to google for accurate miles)
        DEALERSHIP_BOUNDING_BOX_MAX_DISTANCE = (ENV['DEALERSHIP_BOUNDING_BOX_MAX_DISTANCE'] || 75).to_i

        before do
          @request_args = context.input[:request_args]
          @job = context.input[:job]
        end

        def call
          Rails.logger.debug "PCC(#{@job.id}) SiteDecorator request_args #{@request_args}"

          context.output = context.input

          decorate_nearest(context.output,
                           @request_args.dig('job', 'location', 'dropoff_location', 'site_id'))
        end

        private

        def distance_matrix_hash(site_type, location_type)
          service_location_args = @request_args.dig('job', 'location', 'service_location')
          return nil if !service_location_args
          origin = Location.new(lat: service_location_args['lat'],
                                lng: service_location_args['lng'])

          nearest_sites = ::SiteService::Nearest.new(
            site_type,
            origin,
            DEALERSHIP_BOUNDING_BOX_MAX_DISTANCE,
            [location_type],
            @job.fleet_company,
            @job.id
          ).call

          ret = SiteService::DistanceMatrix.new(
            nearest_sites,
            origin
          ).call

          return nil if ret.length == 0

          ret
        end

        #
        # If the given site is the nearest of it's class type and location type, set dropoff.nearest
        #
        def decorate_nearest(output, dropoff_site_id)
          nearest = nil

          dropoff_site_id
          if dropoff_site_id
            site = Site.find(dropoff_site_id)
            closest = distance_matrix_hash(
              site.class,
              [site.location&.location_type&.name]
            )

            if closest && closest[0]
              nearest = (closest[0][:site].id == dropoff_site_id)
            end

          end

          output[:coverage_args][:dropoff][:nearest] = nearest if !nearest.nil?
        end

      end
    end
  end
end
