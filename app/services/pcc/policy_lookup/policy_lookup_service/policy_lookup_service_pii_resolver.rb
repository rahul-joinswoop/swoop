# frozen_string_literal: true

# Resolve all PII URL's in a response from the Policy Lookup Service. The URL's are fetched
# and the returned data is returned in the pii_response key.
module PCC
  module PolicyLookup
    module PolicyLookupService
      class PolicyLookupServicePiiResolver

        include Interactor
        include Interactor::Swoop

        before do
          @policy_search_response = context.input[:response]

          raise ArgumentError, "pii missing from @policy_search_response:#{@policy_search_response}" unless @policy_search_response['pii']

          @pii_urls = @policy_search_response['pii']
          @job_id = context.input[:job_id]
          @client_code = context.input[:client_code]

          raise ArgumentError, "#{self.class.name} client_code is null" unless @client_code
        end

        def call
          Rails.logger.debug "PCC(#{@job_id}): #{self.class.name} called #{context}"

          begin
            pii = {}
            client = ::PolicyLookupService::Client.new(@client_code)

            @pii_urls.each do |pii_url|
              data = client.pii(pii_url)
              pii.merge!(data)
            end

            context.output = {
              policy_search_response: @policy_search_response,
              pii_response: pii,
              job_id: @job_id,
            }
          rescue => e
            Rollbar.error(e)
            context.fail!(
              error: {
                message: 'Problem with Policy Lookup Service PII Integration, please see the logs',
                code: '500',
              }
            )
            return
          end
        end

      end
    end
  end
end
