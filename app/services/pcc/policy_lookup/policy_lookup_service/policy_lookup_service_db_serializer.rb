# frozen_string_literal: true

module PCC
  module PolicyLookup
    module PolicyLookupService
      # input: take context.response
      # output: context.policies [<pcc:Policy>...]
      class PolicyLookupServiceDBSerializer

        include Interactor
        include Interactor::Swoop

        before do
          @policy_search_response = context.input[:policy_search_response]
          @job_id                 = context.input[:job_id]
          @pii_response = context.input[:pii_response]

          raise ArgumentError, "#{self.class.name} policy_search_response is null" unless @policy_search_response
          raise ArgumentError, "#{self.class.name} job_id is null" unless @job_id
          raise ArgumentError, "#{self.class.name} pii_response is null" unless @pii_response
        end

        def call
          Rails.logger.debug "PCC(#{@job_id}): PolicyLookupResponseSerializer called"

          policies = []

          job_fleet_company = Job.find(@job_id).fleet_company

          PCC::Policy.transaction do
            @policy_search_response['policies'].each do |policy_json|
              policy = PCC::Policy.create!(
                uuid: policy_json['uuid'],
                policy_number: policy_json['policy_number'],
                job_id: @job_id,
                company: job_fleet_company,
                data: policy_json.to_h
              )

              policy_json['vehicles'].each do |vehicle_proxy|
                policy.vehicles.build(
                  uuid: vehicle_proxy['uuid'],
                )
              end

              policy_json['customers'].each do |customer_proxy|
                policy.drivers.build(
                  uuid: customer_proxy['uuid'],
                )
              end

              policy.save!

              policies << policy
            end
          end

          context.output = {
            policy_search_response: @policy_search_response,
            pii_response: @pii_response,
            job_id: @job_id,
            policies: policies,
          }

          Rails.logger.debug(JSON.pretty_generate(context.output))
        end

      end
    end
  end
end
