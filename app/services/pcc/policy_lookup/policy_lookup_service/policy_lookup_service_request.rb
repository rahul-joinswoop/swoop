# frozen_string_literal: true

# Search for policies by vin, license_number, (phone_number, country), or (last_name, postal_code)
# Results come back without PII, but include a URL that can be used to request the PII.
module PCC
  module PolicyLookup
    module PolicyLookupService
      class PolicyLookupServiceRequest

        include Interactor
        include Interactor::Swoop

        before do
          @search_params = context.input[:search_params]
          @job_id = context.input[:job_id]
        end

        def call
          Rails.logger.debug "PCC(#{@job_id}): #{self.class.name} called #{context}"

          job = Job.find(@job_id)
          fleet_company = job.fleet_company
          client_code = Setting.get_setting(fleet_company, Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE)

          if !client_code
            Rails.logger.warn("#{self.class.name} client_code is null for fleet_company(#{fleet_company.id})")

            context.fail!(
              error: {
                message: "#{self.class.name} client_code is null",
                code: '500',
              }
            )
          end

          begin
            client = ::PolicyLookupService::Client.new(client_code)
            response = client.search(@search_params)

            context.output = { response: response, job_id: @job_id, client_code: client_code }
          rescue => e
            Rollbar.error(e)

            context.fail!(
              error: {
                message: 'Problem with Policy Lookup Service Request Integration, please see the logs',
                code: '500',
              }
            )
          end
        end

      end
    end
  end
end
