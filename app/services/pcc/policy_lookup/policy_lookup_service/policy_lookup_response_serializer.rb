# frozen_string_literal: true

#
# This service takes the policies and a respective pii_response
# that are fetched in PolicyLookupService, and formats it accordingly
# to be used by clients ("clients" for now is Swoop web UI only).
#
module PCC
  module PolicyLookup
    module PolicyLookupService
      # input: take context.response
      # output: context.policies [<pcc:Policy>...]
      class PolicyLookupResponseSerializer

        include Interactor
        include Interactor::Swoop

        before do
          @job_id = context.input[:job_id]
          @pii_response = context.input[:pii_response]
          @policies = context.input[:policies]
        end

        def call
          Rails.logger.debug "PCC(#{@job_id}): PolicyLookupResponseSerializer called"

          policies = @policies.map do |policy_obj|
            pcc_vehicles = build_pcc_vehicles_collection(policy_obj)
            pcc_drivers = build_pcc_drivers_collection(policy_obj)
            pcc_drivers_and_vehicles_matrix = build_vehicles_and_drivers_matrix(pcc_vehicles, pcc_drivers)

            # id and uuid -> are policies identifiers
            # pcc_vehicles -> contains all policy vehicles
            # pcc_drivers -> contains all policy drivers
            #
            # pcc_vehicles_and_drivers_matrix -> is a matrix of vehicles and drivers
            # e.g. if we have:
            #
            # pcc_vehicles = [vehicle_1, vehicle_2]
            # pcc_drivers = [driver_1, driver_2]
            #
            # then pcc_vehicles_and_drivers_matrix will be (pseudo code):
            #
            # [
            #   { vehicle_1 -> driver_1 },
            #   { vehicle_1 -> driver_2 },
            #   { vehicle_2 -> driver_1 },
            #   { vehicle_2 -> driver_2 },
            # ]
            #
            # So all vehicles will be associated with all drivers in the list,
            # making it a bit easier for the UI to present it.
            #
            # pcc_vehicles and pcc_drivers will objects likely be deprecated and removed
            # in favour of pcc_vehicles_and_drivers_matrix. I'm leaving it here for now
            # just to make sure we'll not break anything.
            #
            {
              id: policy_obj.id,
              uuid: policy_obj.uuid,
              policy_number: policy_obj.policy_number,
              pcc_vehicles: pcc_vehicles,
              pcc_drivers: pcc_drivers,
              pcc_vehicles_and_drivers_matrix: pcc_drivers_and_vehicles_matrix,
            }
          end

          context.output = policies

          Rails.logger.debug("PCC(#{@job_id}): #{JSON.pretty_generate(context.output)}")
        end

        private

        def build_pcc_vehicles_collection(policy_obj)
          policy_obj.vehicles.map do |vehicle_proxy|
            vehicle = { id: vehicle_proxy.id }

            vehicle.merge!(@pii_response[vehicle_proxy.uuid]["vehicle"].symbolize_keys)
          end
        end

        def build_pcc_drivers_collection(policy_obj)
          policy_obj.drivers.map do |customer_proxy|
            driver = { id: customer_proxy.id }

            driver.merge!(@pii_response[customer_proxy.uuid]["customer"].symbolize_keys)
          end
        end

        def build_vehicles_and_drivers_matrix(pcc_vehicles, pcc_drivers)
          pcc_vehicles.map do |pcc_vehicle|
            pcc_drivers.map do |pcc_driver|
              {
                pcc_vehicle: pcc_vehicle,
                pcc_driver: pcc_driver,
              }
            end
          end.flatten
        end

      end
    end
  end
end

# agero_response=JSON.parse(
#   '{
#     "totalResultCount": 1,
#     "results": [
#         {
#             "policyType": "1",
#             "policyNumber": "1234567890",
#             "policyOwner": {
#                 "lastName": "John",
#                 "firstName": "Doe",
#                 "middleName": "",
#                 "contactInfo": {
#                     "phoneNumber": "",
#                     "address": {
#                         "street": "123 West Lake",
#                         "state": "MD",
#                         "zip": "20810",
#                         "city": "Bethesda"
#                     }
#                 }
#             },
#             "policyStateCode": "79",
#             "coverageSystemSource": "APPSCE"
#         }
#     ]
# }',object_class:OpenStruct
# )
