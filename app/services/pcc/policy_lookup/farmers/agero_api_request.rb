# frozen_string_literal: true

#
#   policy_lookup = PCC::PolicyLookup::Farmers::AgeroApiRequest.call(
#     input: { policy_number: "0103293256" }
#   )
#
module PCC
  module PolicyLookup
    module Farmers
      class AgeroApiRequest

        include Interactor
        include Interactor::Swoop

        API_RESOURCE = '/policies/search'

        before do
          Rails.logger.debug "PCC::PolicyLookup::Farmers::AgeroApiRequest called #{context}"
          # TODO add contract

          @policy_number = context.input[:policy_number]
          @job_id        = context.input[:job_id]
        end

        def call
          raise ArgumentError, 'policy_number and job_id must be supplied in a hash, e.g.: { policy_number: 12123123 }' unless
            @policy_number.present? && @job_id.present?

          loss_date = Time.now.utc

          response = AgeroApi.post(
            resource: API_RESOURCE,
            json: JSON.generate({ "policyNumber" => @policy_number, "lossDate" => loss_date })
          )

          if response.code == 200
            context.output = {
              response: JSON.parse(response.body, object_class: OpenStruct),
              loss_date: loss_date,
              job_id: @job_id,
            }
          else
            context.fail!(
              error: {
                message: 'Problem with Farmers Integration, please see the logs',
                code: '500',
              }
            )
          end
        end

      end
    end
  end
end
