# frozen_string_literal: true

module PCC
  module PolicyLookup
    module Farmers
      class PolicyParser

        def initialize(open_struct)
          raise ArgumentError, 'PCC::PolicyLookup::Farmers::Parser agero_response not open struct' unless open_struct.class.name == "OpenStruct"
          @data = open_struct
        end

        def owner
          @_owner ||= @data.policyOwner
        end

        def full_name
          "#{owner&.firstName} #{owner&.lastName}".strip
        end

        def policy_number
          @data.policyNumber
        end

        def location
          @_location ||= owner&.contactInfo&.address
        end

        def address
          "#{location.street}, #{location.city}, #{location.state}".strip
        end

      end
    end
  end
end

# {
#     "totalResultCount": 1,
#     "results": [
#         {
#             "policyType": "1",
#             "policyNumber": "1234567890",
#             "policyOwner": {
#                 "lastName": "John",
#                 "firstName": "Doe",
#                 "middleName": "",
#                 "contactInfo": {
#                     "phoneNumber": "",
#                     "address": {
#                         "street": "123 West Lake",
#                         "state": "MD",
#                         "zip": "20810",
#                         "city": "Bethesda"
#                     }
#                 }
#             },
#             "policyStateCode": "79",
#             "coverageSystemSource": "APPSCE"
#         }
#     ]
# }
