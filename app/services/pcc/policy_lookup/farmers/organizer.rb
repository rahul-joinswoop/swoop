# frozen_string_literal: true

# lookup = PCC::PolicyLookup::Farmers::Organizer.call(input:{policy_number: "0103293256", job_id: 1190})

module PCC
  module PolicyLookup
    module Farmers
      class Organizer

        include Interactor::Organizer
        include Interactor::Swoop

        organize AgeroApiRequest, AgeroResponseDBSerializer, FormatSwoopApiResponse

      end
    end
  end
end
