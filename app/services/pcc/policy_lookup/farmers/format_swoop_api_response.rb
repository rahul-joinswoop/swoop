# frozen_string_literal: true

module PCC
  module PolicyLookup
    module Farmers
      class FormatSwoopApiResponse

        include Interactor
        include Interactor::Swoop

        before do
          @policies = context.input
        end

        # input: take context.policies from AgeroResponseDbSerializer
        # output: JSON hash suitable for returning to the UI
        # swoop_response:{
        #   policies:[
        #     {
        #       id: <pcc_policies.id>
        #       policy_number:<string>
        #       full_name:
        #       address:
        #     }
        #   ]
        # }

        # serialise policies
        def call
          context.output = ActiveModelSerializers::SerializableResource.new(
            @policies
          ).serializable_hash
        end

      end
    end
  end
end
