# frozen_string_literal: true

module PCC
  module PolicyLookup
    module Farmers
      # input: take context.response and write them to pcc:Policy, pcc:Person and pcc:Vehicle models
      # sideeffect: db rows written
      # output: context.policies [<pcc:Policy>...]
      class AgeroResponseDBSerializer

        include Interactor
        include Interactor::Swoop

        before do
          Rails.logger.debug "AgeroResponseDBSerializer called #{context}"
          @agero_response = context.input[:response]
          @loss_date      = context.input[:loss_date]
          @job_id         = context.input[:job_id]

          raise ArgumentError, 'PCC::PolicyLookup::Farmers::AgeroResponseDBSerializer agero_response is null' unless @agero_response
          raise ArgumentError, 'PCC::PolicyLookup::Farmers::AgeroResponseDBSerializer agero_response not open struct' unless @agero_response.class.name == "OpenStruct"

          unless @loss_date.present? && @job_id.present?
            raise ArgumentError, "PCC::PolicyLookup::Farmers::AgeroResponseDBSerializer loss_date and job_id must be supplied in a hash, " \
              "e.g.: { loss_date: Time.now.utc, job_id: 123 }'"
          end
        end

        def call
          policies = []

          @agero_response&.results.each do |policy_json|
            parser = PolicyParser.new(policy_json)

            policy = PCC::Policy.new(
              policy_number: parser.policy_number,
              job_id: @job_id,
              full_name: parser.full_name,
              address: parser.address,
              company: Company.farmers,
              data: policy_json.to_h
            )

            policy.data[:loss_date] = @loss_date

            policy.save!

            policies << policy
          end

          context.output = policies
        end

      end
    end
  end
end

# agero_response=JSON.parse(
#   '{
#     "totalResultCount": 1,
#     "results": [
#         {
#             "policyType": "1",
#             "policyNumber": "1234567890",
#             "policyOwner": {
#                 "lastName": "John",
#                 "firstName": "Doe",
#                 "middleName": "",
#                 "contactInfo": {
#                     "phoneNumber": "",
#                     "address": {
#                         "street": "123 West Lake",
#                         "state": "MD",
#                         "zip": "20810",
#                         "city": "Bethesda"
#                     }
#                 }
#             },
#             "policyStateCode": "79",
#             "coverageSystemSource": "APPSCE"
#         }
#     ]
# }',object_class:OpenStruct
# )
