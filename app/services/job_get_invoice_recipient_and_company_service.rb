# frozen_string_literal: true

class JobGetInvoiceRecipientAndCompanyService < Service

  def initialize(job)
    @job = job
  end

  def call
    cash_account = Account.find_or_create_cash_account(@job.rescue_company)
    recipient_company = nil

    items = @job.vehicle_inventory_items

    if items.count > 0 && items[0].replace_invoice_info && (items[0].released_to_user || items[0].released_to_account)
      # XXX hard to test it
      if items[0].released_to_user
        recipient = items[0].released_to_user
      else
        recipient = items[0].released_to_account
        recipient_company = items[0].released_to_account.client_company
      end
    # Check for Cash Call account NAME. If the CASH ACCOUNT account got deleted for any reason,
    # a new one will be returned from Account.find_or_create_cash_account call above ^. In this case,
    # the account.id will differ but name will be the same, which means it's a customer recipient.
    elsif @job.account.name == cash_account&.name
      recipient = @job.customer
    else
      recipient = @job.account
      recipient_company = recipient.client_company # may be nil
    end

    [recipient, recipient_company]
  end

end
