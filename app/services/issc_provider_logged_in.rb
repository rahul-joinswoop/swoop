# frozen_string_literal: true

class IsscLoginNotAllowed < StandardError; end
class IsscProviderLoggedIn < IsscService

  def call
    issc = find_issc
    if @event[:Result] == 'Success'
      issc.provider_logged_in
      issc.logged_in_at = Time.zone.now
    elsif @event[:Result] == 'Failure'
      desc = @event[:Description]
      if desc.nil?
        msg = "Unknown issc provider logged in error: #{issc.id} - No Description"
        Rails.logger.error msg
        Rollbar.error(StandardError.new(msg))
        # Unknwon issc provider logged in error: Service requester error.
      elsif desc == "Not registered."
        # TODO: We should never be able to get into this state and the below code will raise an exception
        issc.status = "Unregistered"
      elsif desc == "Already logged in."
        issc.status = 'logged_in'
        issc.logged_in_at = Time.zone.now
      elsif desc == "Service requester error. Provider not active or Incorrect login credentials. (4999)"
        Rails.logger.debug "Login credentials incorrect for NSD provider: #{issc.id}"
        issc.provider_password_incorrect
      elsif desc.include? "Approval pending."
        Rails.logger.debug "Approval pending - Waiting for registration, issc_id:#{issc.id}"
      elsif desc.include? "Service requester error"
        Rails.logger.debug "Service requester error, issc_id:#{issc.id}"
      elsif desc.include? "Operation not allowed."
        Rails.logger.warn "ISSCLOG Potential Race Condition: issc_id:#{issc.id} desc:#{desc}"
        # raise IsscLoginNotAllowed, "#{issc.id} - #{desc}"
      elsif desc.include? "Timeout."
        Rails.logger.warn "Timeout: issc_id:#{issc.id}, desc:#{desc}"
      elsif desc.include? "Invalid ClientID"
        issc.status = 'register_failed'
        issc.error = 'Invalid ClientID'
        issc.error_at = Time.now
      else
        msg = "Unknown issc provider logged in error: #{desc}"
        Rails.logger.warning msg
        Rollbar.error(StandardError.new(msg))
      end
    end

    issc
  end

end
