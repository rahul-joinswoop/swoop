# frozen_string_literal: true

module DepartmentServices
  class Batch

    def initialize(api_company:, department_ids:)
      @api_company = api_company
      @department_ids = department_ids.split(',')

      Rails.logger.debug "Department batch load params: company.id = #{@api_company.id}, department_ids = #{@department_ids}"
    end

    def call
      Department.where(
        id: @department_ids,
        company: @api_company
      ).distinct
    end

  end
end
