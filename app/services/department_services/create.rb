# frozen_string_literal: true

module DepartmentServices
  class Create

    include SwoopService

    attr_reader :department

    def initialize(api_company:, department_attributes:)
      @api_company = api_company
      @department_attributes = department_attributes
    end

    def execute
      @department = Department.new(@department_attributes).tap do |department|
        department.company = @api_company

        department.save!
      end

      self
    end

    def after_execution
      PublishCompanyWorker.perform_after_commit(@api_company.id, self.class.name)
    end

  end
end
