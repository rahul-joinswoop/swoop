# frozen_string_literal: true

module DepartmentServices
  class Delete

    include SwoopService

    attr_reader :department

    def initialize(api_company:, department_id:)
      @api_company = api_company
      @department_id = department_id
    end

    def execute
      @department = @api_company.departments.where(
        deleted_at: nil
      ).find(@department_id)

      if @department.present?
        @department.deleted_at = Time.now

        @department.save!
      end

      self
    end

    def after_execution
      PublishCompanyWorker.perform_after_commit(@api_company.id, self.class.name)
    end

  end
end
