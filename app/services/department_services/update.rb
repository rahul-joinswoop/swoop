# frozen_string_literal: true

module DepartmentServices
  class Update

    include SwoopService

    attr_reader :department

    def initialize(api_company:, department_attributes:)
      @api_company = api_company
      @department_attributes = department_attributes
    end

    def execute
      @department = Department.find_by!(
        id: @department_attributes[:id],
        company: @api_company,
        deleted_at: nil
      )

      if @department.present?
        @department.update(@department_attributes)
      end

      self
    end

  end
end
