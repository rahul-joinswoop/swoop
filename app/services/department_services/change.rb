# frozen_string_literal: true

# Used when partner or fleet (inHouse or managed) is editing their departments
module DepartmentServices
  class Change

    include SwoopService

    attr_reader :departments_added

    def initialize(api_company:, departments_to_add: [], department_ids_to_remove: [],
                   department_to_rename: [])

      @api_company = api_company
      @departments_to_add = departments_to_add
      @department_ids_to_remove = department_ids_to_remove
      @department_to_rename     = department_to_rename

      @departments_added = []

      Rails.logger.debug("DepartmentServices::Change params: api_company.id = #{@api_company.id}, " \
        "departments_to_add = #{@departments_to_add}, " \
        "department_ids_to_remove = #{@department_ids_to_remove}, " \
        "department_to_rename = #{@department_to_rename}.")
    end

    def execute
      return if @api_company.super?

      add_departments

      remove_departments

      rename_departments
    end

    private

    def add_departments
      @departments_to_add.each do |department_to_add|
        name = department_to_add[:name]

        department = @api_company.departments
          .where('lower(name) = ?', name.downcase)
          .find_by(company: @api_company)

        if !department
          department = Department.create!(name: name, company: @api_company)

          @departments_added = department
        end
      end
    end

    def remove_departments
      @department_ids_to_remove.each do |department_id|
        department = Department.where(company: @api_company).find(department_id)

        if department
          department.deleted_at = Time.now

          department.save!
        end
      end
    end

    def rename_departments
      @department_to_rename.each do |department_to_rename|
        department = Department.where(company: @api_company).find(department_to_rename[:id])

        if department
          department.name = department_to_rename[:name]

          department.save!
        end
      end
    end

  end
end
