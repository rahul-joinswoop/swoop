# frozen_string_literal: true

#
# POPULATE INVOICE FOR AUTO ASSIGN CANDIDATE
#
# Provided a new & empty invoice, populate it for an auto
# assign candidate, using the abstract invoice updating
# logic of UpdateInvoiceService, so that cost can be calculated
# for a candidate
#

class CannotInitializePopulateInvoiceForJobCandidateWithoutCandidateError < ArgumentError; end

class PopulateInvoiceForJobCandidate < UpdateInvoiceService

  def initialize(invoice, job, rate, candidate)
    raise CannotInitializePopulateInvoiceForJobCandidateWithoutCandidateError unless candidate.is_a?(Job::Candidate)

    @candidate = candidate

    super(invoice, job, rate)
  end

  #
  # A candidate's invoice has no restrictions on when it can
  # be safely updated
  #
  def invoice_can_be_updated
    true
  end

  #
  # Candidates are never assigned for storage jobs, so we never
  # need to worry about bumping the storage tick when generating
  # an invoice for a candidate
  #
  def bump_storage_tick
  end

  #
  # Since a candidate's invoice isn't persisted, we don't need
  # to delete any existing line items
  #
  def delete_existing_lineitems
  end

  #
  # When updating an invoice for a candidate, the candidate is
  # used to generate rates and line items
  #
  def invoicable
    @candidate
  end

  #
  # The candidate's Account will be used for the default vehicle rate
  # additions
  #
  def account_for_default_vehicle_rate
    @candidate.account
  end

  #
  # When an invoice is populated for a candidate, it is a new invoice, so
  # there is no billing or payment info to apply
  #
  def apply_job_billing_info
  end

end
