# frozen_string_literal: true

class SystemEmailRecipientCalculator

  def initialize(job)
    @job = job
  end

  def calculate(include_non_root: false)
    users = User.root_alert_users
    users = (users + User.swoop_non_root_alert_users).uniq if include_non_root

    # Rails.logger.debug "ALERT: users #{users.map(&:full_name)}"
    company_filters = SystemEmailFilter.by_company(@job.fleet_company)
    user_filters = User.joins(:system_email_filters).merge(company_filters)
    # Rails.logger.debug "ALERT: filters #{user_filters.map(&:full_name)}"
    users - user_filters
  end

end
