# frozen_string_literal: true

class IsscServiceRequestorStatus < Service

  def initialize(event)
    @event = event
    @status = @event[:Status]
    @company = @event[:Name]
    @comment = @event[:UpComment] || @event[:DownComment]
    @since = @event[:UpSince] || @event[:DownSince]
  end

  def call
    root_users = User.with_role(:root).where(deleted_at: nil)
    emails = root_users.map(&:email).push('pg@joinswoop.com').uniq
    tz = root_users.first.try(:timezone)

    subject = "ISSC Alert: #{@company} #{@status}"

    Rails.logger.debug subject
    Rails.logger.debug emails
    SystemMailer.send_mail({
      template_name: 'alert_service_requestor_status',
      to: emails,
      from: 'operations@joinswoop.com',
      subject: subject,
      locals: { :@tz => tz, :@company => @company, :@status => @status, :@comment => @comment, :@since => @since },
    },
                           nil, nil, Company.swoop, "Servicecompany-#{@company}-#{@status}", nil).deliver_now
  end

end
