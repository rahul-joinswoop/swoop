# frozen_string_literal: true

class SearchService < Service

  DEFAULT_PAGE = 1
  DEFAULT_PER_PAGE = 200
  MIN_PER_PAGE = 1
  MAX_PER_PAGE = 200

  attr_reader :page, :per_page, :after_id

  class << self

    def model_class
      raise NotImplementedError, "Define model class in sub-class method."
    end

    # List of searchable fields. Format is an array with entries like:
    #  [:field_name, :field_type, :index_field_name]
    # where :field_type is one of :string, :integer, :date, :phone, :wildcard
    def power_searchable_fields
      []
    end

    # List of power search field names to skip in general searches.
    def general_search_skip_fields
      []
    end

    # List of fields that can be used to filter the search. Format is an array with entries like:
    #   [:field_name, :index_field_name]
    def filterable_fields
      []
    end

    def power_search_params
      @power_search_params ||= Set.new((power_searchable_fields + filterable_fields).map(&:first))
    end

    # Default sort order for the results. Subclasses can override.
    # Return format is a hash that corresponds to the ElasticSearch sort syntax:
    # https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-sort.html
    def default_sort_order
      {}
    end

  end

  # The after_id argument is used for pagination and will override the page argument if both
  # are provided. The value for after_id should be the id of the last record from a previous
  # search results.
  def initialize(page: nil, per_page: nil, after_id: nil)
    @page = (page || DEFAULT_PAGE).to_i
    @per_page = (per_page || DEFAULT_PER_PAGE).to_i.clamp(MIN_PER_PAGE, MAX_PER_PAGE)
    @after_id = Integer(after_id) if after_id

    # It's invalid to use pagination with an offset greating than 10k with ElasticSearch > 5.0
    # This is either some sort of out of control client code or something nefarious. The ElasticSearch
    # error is very ugly and cryptic so at least return a better error to the client.
    if @after_id.nil? && @page * @per_page > 10_000
      raise ArgumentError, "Search pagination limit reached"
    end
  end

  def search(params)
    Rails.logger.debug "#{self.class.name}.search called with #{params.inspect}"

    search_query = query(params)
    bool_query = search_query[:bool]
    main_queries = (bool_query[:must] || bool_query[:should])
    record_power_search(params, main_queries, bool_query[:filter])

    search_options = { query: search_query, min_score: 0.0001, sort: sort_order_query, size: per_page }
    if after_id
      last_record = self.class.model_class.find_by(id: after_id)
      if last_record
        search_after = []
        search_options[:sort].each do |sort|
          search_after << last_record.send(sort.keys.first)
        end
      end
      search_options[:search_after] = search_after
    else
      search_options[:from] = (page - 1) * per_page
    end

    perform_query(search_options)
  end

  # Return the structured query that will be sent to ElasticSearch.
  def query(params)
    params = exclude_deleted_at(params)
    if has_power_search_params?(params)
      power_search_query(params)
    else
      terms = params[:search_terms][:term] if params[:search_terms]
      general_search_query(terms, params)
    end
  end

  protected

  def exclude_deleted_at(params)
    params = params.clone
    params[:must_not] ||= {}
    params[:must_not][:exists] ||= []
    must_not_list = params[:must_not][:exists]
    unless must_not_list.is_a?(Array)
      must_not_list = Array(must_not_list)
      params[:must_not][:exists] = must_not_list
    end
    unless must_not_list.include?(:deleted_at)
      must_not_list << :deleted_at
    end
    params
  end

  # Override to filter available power search fields.
  def power_searchable_fields
    self.class.power_searchable_fields
  end

  # Override to change the sort order.
  def sort_order
    self.class.default_sort_order
  end

  private

  def power_search_query(options)
    must_queries = generate_search_term_queries(options[:search_terms])
    filter_queries = generate_filter_queries(options[:filters])
    must_not_queries = generate_must_not_queries(options[:must_not])
    {
      bool: {
        must: must_queries,
        filter: filter_queries,
        must_not: must_not_queries,
      },
    }
  end

  def general_search_query(string, options = {})
    string = string.to_s.strip
    should_queries = nil
    if string.blank?
      should_queries = { match_all: {} }
    else
      should_queries = generate_search_term_queries(string)
    end
    filter_queries = generate_filter_queries(options[:filters])
    must_not_queries = generate_must_not_queries(options[:must_not])
    {
      bool: {
        should: should_queries,
        filter: filter_queries,
        must_not: must_not_queries,
      },
    }
  end

  # Generates a typed_query_set if passed a String.
  # Generates an exists + typed query set if passed a Hash.
  # Used as 'should' in general_search and as 'must' in power_search.
  def generate_search_term_queries(opts)
    typed_queries = generate_typed_query_set(opts)
    return typed_queries if opts.is_a?(String)

    exists_queries = generate_exists_queries(opts)
    exists_queries + typed_queries
  end

  def generate_filter_queries(opts)
    return [] if opts.blank?
    filter_exists = generate_exists_queries(opts)
    filter_terms = generate_term_query_set(opts)
    filter_exists + filter_terms
  end

  def generate_must_not_queries(opts)
    return [] if opts.blank?
    must_not_exists = generate_exists_queries(opts)
    must_not_terms = generate_term_query_set(opts)
    must_not_exists + must_not_terms
  end

  def generate_exists_queries(opts)
    return [] if opts.blank?
    exists = Array(opts[:exists])
    exists.map do |field|
      name, index_name = self.class.filterable_fields.find { |name, _| field == name }
      index_name = field if name.nil?
      { exists: { field: index_name } }
    end
  end

  def generate_typed_query_set(opts)
    skip_fields = self.class.general_search_skip_fields
    power_searchable_fields.map do |name, kind, index_name|
      if opts.is_a?(String)
        next if skip_fields.include?(name)
        generate_typed_query(kind, index_name, opts, general: true)
      else
        value = (opts[name] || opts[name.to_s])
        generate_typed_query(kind, index_name, value) if value
      end
    end.compact
  end

  def generate_typed_query(kind, index_name, value, general: false)
    return nil if value.blank?

    # TODO we should not be downcasing the query because that could mess up case sensitive analyzers.
    # This should be removed after reindexing
    value = value.downcase

    case kind
    when :string
      if index_name.is_a?(Array)
        { multi_match: { query: value, type: :cross_fields, fields: index_name, operator: "and" } }
      else
        { match: { index_name => { "query" => value, "operator" => "and" } } }
      end
    when :integer
      value = value.gsub(/\s+/, "") # Strip whitespace
      if value.scan(/\D/).empty? # Making sure all digits ensures don't have to deal with to_i returning 0
        number = value.to_i
        { match: { index_name => number } } if number > -2147483648 && number < 2147483647 # ES max integer is 2^31-1
      else
        nil
      end
    when :phone
      phone = Phonelib.parse(value).e164
      { match: { index_name => phone } } if phone
    when :wildcard
      { wildcard: { index_name => "*#{value}*" } }
    when :date
      if value =~ /\A(\d\d)(\d\d)(\d\d\d\d)(-(\d\d)(\d\d)(\d\d\d\d))?\z/
        start_stamp = "#{Regexp.last_match(2)}/#{Regexp.last_match(1)}/#{Regexp.last_match(3)}"
        start_range = begin
                        Time.zone.parse(start_stamp).beginning_of_day
                      rescue StandardError => e
                        raise e unless general
                      end

        end_stamp = Regexp.last_match(4) ? "#{Regexp.last_match(6)}/#{Regexp.last_match(5)}/#{Regexp.last_match(7)}" : start_stamp
        end_range = begin
                      Time.zone.parse(end_stamp).end_of_day
                    rescue StandardError => e
                      raise e unless general
                    end

        if start_range && end_range
          { range: { index_name => { gte: start_range, lte: end_range } } }
        end
      end
    else
      raise ArgumentError, "SearchService Unknown type:#{kind}"
    end
  end

  def generate_term_query_set(opts)
    return [] if opts.blank?

    # value could be a string or an array.
    self.class.filterable_fields.map do |name, index_name|
      value = opts[name] || opts[name.to_s]
      next nil if value.blank?
      values = Array(value)
      generate_term_query(index_name, values)
    end.compact
  end

  # Generate a term query on the index field to match one or more values.
  # Note that term query values are not analyzed, and this code downcases
  # any string values with the assumption that all keyword (text) fields are
  # configured to use a case insensitive analyzer during indexing.
  def generate_term_query(index_field_name, value)
    terms = Array(value).map { |term| term.is_a?(String) ? term.downcase : term }
    if terms.size > 1
      { terms: { index_field_name => terms } }
    elsif terms.size == 1
      { term: { index_field_name => terms.first } }
    end
  end

  def record_power_search(options, queries, filters)
    AuditSearch.create({
      original: options[:original],
      options: options.keys.join(","),
      queries: queries.to_s,
      filters: filters.to_s,
    })
  end

  def sort_order_query
    order = sort_order
    order = [order] unless order.is_a?(Array)
    # We need a predictable order so always include the primary key as the last tie breaker.
    sort_keys = order.map { |sort| sort.keys.first.to_s }
    unless sort_keys.include?("id")
      order << { id: :asc }
    end
    order
  end

  def perform_query(search_options)
    response = self.class.model_class.search(search_options)
    total = response.results.total
    records = response.records

    Rails.logger.debug "#{self.class.name}.perform_query did receive #{records.size} records, #{total} total."
    [total, records]
  end

  # Truthy if params contain a power search parameter.
  def has_power_search_params?(params)
    if params[:search_terms].respond_to? :keys
      params[:search_terms].keys.find do |param|
        self.class.power_search_params.include?(param.to_sym)
      end
    end
  end

end
