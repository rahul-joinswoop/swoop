# frozen_string_literal: true

module JobHistory
  class LabelForCompany

    CUSTOMER = 'Customer'
    BULLET = ' • '
    SWOOP = 'Swoop'
    SMS = 'SMS'

    def should_present_customer_string?
      platforms_to_present_customer = [SourceApplication::PLATFORM_SMS, SourceApplication::PLATFORM_CONSUMER_MOBILE_WEB]
      platforms_to_present_customer.include?(@ajs_or_history_item.source_application&.platform)
    end

  end
end
