# frozen_string_literal: true

#
# This formats labels for Job History.
# It works with AuditJobStatus, HistoryItem, AuditSms and ParentJobHistoryItem, therefore it depends on their attributes.
#
# If it's an AuditSms, just call JobHistory::LabelForSms service.
# Otherwise, selects a specific implementation based on the viewing company.
#
module JobHistory
  class Label

    def initialize(job_history_row, viewing_company)
      return '' unless job_history_row.is_a?(AuditJobStatus) || job_history_row.is_a?(HistoryItem) || job_history_row.is_a?(TimeOfArrival) || job_history_row.is_a?(AuditSms) || job_history.is_a?(ParentJobHistoryItem)

      @job_history_row = job_history_row
      @job = job_history_row.job
      @viewing_company = viewing_company
    end

    def ui_label
      #
      # AuditSms has a special but really simple treatment, since it's always treated as SMS coming from customer
      #
      if @job_history_row.is_a?(AuditSms)
        return LabelForSms.new(@job_history_row).ui_label
      end

      changed_by_company_or_company = @job_history_row.changed_by_company || @job_history_row.company

      #
      # AuditJobStatus and HistoryItem have a more complex treatment, and it depends on the viewing_company
      #
      if @viewing_company&.rescue?
        LabelForPartner.new(@job_history_row, @viewing_company, changed_by_company_or_company).ui_label
      elsif @viewing_company&.is_swoop?
        LabelForSwoop.new(@job_history_row, @viewing_company, changed_by_company_or_company).ui_label
      elsif @viewing_company&.fleet?
        LabelForClient.new(@job_history_row, @viewing_company, changed_by_company_or_company).ui_label
      end
    end

  end
end
