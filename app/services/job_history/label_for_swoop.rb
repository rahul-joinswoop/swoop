# frozen_string_literal: true

module JobHistory

  class InvalidSwoopCompanyError < StandardError; end

  #
  # This formats labels for Job History when viewing_company is Swoop.
  # It works with AuditJobStatus and HistoryItem, therefore it depends on their attributes.
  #
  class LabelForSwoop < LabelForCompany

    def initialize(ajs_or_history_item, viewing_company, changed_by_company_or_company)
      raise InvalidPartnerCompanyError, "id #{viewing_company.id}" unless viewing_company.is_swoop?

      @ajs_or_history_item = ajs_or_history_item
      @viewing_company = viewing_company
      @company_or_application_name =
        changed_by_company_or_company&.name || @ajs_or_history_item.source_application&.oauth_application&.name
    end

    def ui_label
      ui_label = ''

      return ui_label if !@company_or_application_name && !should_present_customer_string?

      # if @ajs_or_history_item.source_application.platform is either SMS or CWM, append 'Customer' string;
      # if not, append changed_by_company_or_company.name and user_name
      if should_present_customer_string?
        ui_label = CUSTOMER # e.g. "Customer"
      else
        if @ajs_or_history_item.source_application&.api?
          ui_label = @company_or_application_name
        else
          # e.g. "Super Tow • John Smith" or "Super Tow"
          ui_label = [@company_or_application_name, @ajs_or_history_item.user_name].reject(&:blank?).join(BULLET)
        end
      end

      # if source_application and @ajs_or_history_item.source_application.api?, append source_application.source
      # if source_application but no @ajs_or_history_item.source_application.api?, append source_application.platform
      if @ajs_or_history_item.source_application
        if @ajs_or_history_item.source_application.api?
          ui_label += "#{BULLET}#{@ajs_or_history_item.source_application.source}" # e.g. "Super Tow • John Smith • Client 1" or "Customer • Client 1"
        else
          ui_label += "#{BULLET}#{@ajs_or_history_item.source_application.platform}" # e.g. "Super Tow • John Smith • Web" or "Customer • SMS"
        end
      end

      ui_label
    end

  end

end
