# frozen_string_literal: true

module JobHistory
  #
  # This formats labels for AuditSms instances.
  #
  class LabelForSms

    class InvalidAuditSmsInstance < TypeError; end

    CUSTOMER = 'Customer'
    BULLET = ' • '
    SMS = 'SMS'

    def initialize(audit_sms_object)
      raise InvalidAuditSmsInstance unless audit_sms_object.is_a?(AuditSms)

      @audit_sms_object = audit_sms_object
    end

    def ui_label
      if @audit_sms_object&.source_application&.sms?
        "#{CUSTOMER}#{BULLET}#{SMS}"
      else
        ''
      end
    end

  end
end
