# frozen_string_literal: true

module JobHistory

  class InvalidPartnerCompanyError < StandardError; end

  #
  # This formats labels for Job History when viewing_company is Partner (aka RescueCompany).
  # It works with AuditJobStatus and HistoryItem, therefore it depends on their attributes.
  #
  class LabelForPartner < LabelForCompany

    def initialize(ajs_or_history_item, viewing_company, changed_by_company_or_company)
      raise InvalidPartnerCompanyError, "id #{viewing_company.id}" unless viewing_company.rescue?

      @ajs_or_history_item = ajs_or_history_item
      @job = ajs_or_history_item.job
      @changed_by_company_or_company = changed_by_company_or_company
      @viewing_company = viewing_company
    end

    def ui_label
      return CUSTOMER if should_present_customer_string?

      return '' unless @changed_by_company_or_company

      if @job.fleet_motor_club_job?
        job_statuses_to_present_motor_club_name = [
          JobStatus::NAMES[:CREATED], JobStatus::NAMES[:ASSIGNED], JobStatus::NAMES[:ETA_REJECTED], JobStatus::NAMES[:ACCEPTED],
        ]

        digital_dispatcher_canceled = !@ajs_or_history_item.company&.name && !@ajs_or_history_item.user_name && @ajs_or_history_item.name == JobStatus::NAMES[:CANCELED]

        if job_statuses_to_present_motor_club_name.include?(@ajs_or_history_item.name) || digital_dispatcher_canceled
          return @job.fleet_company&.name || ''
        end
      end

      case @ajs_or_history_item.source_application&.platform
      when SourceApplication::PLATFORM_CLIENT_API
        if @job.fleet_managed_job?
          return SWOOP
        else
          return @ajs_or_history_item.source_application.oauth_application.owner.name
        end
      when SourceApplication::PLATFORM_PARTNER_API
        return @ajs_or_history_item.source_application&.oauth_application&.name || ''
      end

      if @viewing_company&.id == @changed_by_company_or_company&.id
        user_name_or_blank = @ajs_or_history_item.user_name ? (BULLET + @ajs_or_history_item.user_name) : ''

        @changed_by_company_or_company.name + user_name_or_blank # e.g. "Super Tow • John Smith" or "Super Tow"
      elsif @job.fleet_managed_job?
        SWOOP
      elsif @changed_by_company_or_company
        @changed_by_company_or_company.name
      else
        ""
      end
    end

  end

end
