# frozen_string_literal: true

module JobHistory

  class InvalidClientCompanyError < StandardError; end

  #
  # This formats labels for Job History when viewing_company is Client (aka FleetCompany).
  # It works with AuditJobStatus and HistoryItem, therefore it depends on their attributes.
  #
  class LabelForClient < LabelForCompany

    def initialize(ajs_or_history_item, viewing_company, changed_by_company_or_company)
      raise InvalidClientCompanyError, "id #{viewing_company.id}" unless viewing_company.fleet?

      @ajs_or_history_item = ajs_or_history_item
      @job = ajs_or_history_item.job
      @changed_by_company_or_company = changed_by_company_or_company
      @viewing_company = viewing_company
    end

    def ui_label
      return CUSTOMER if should_present_customer_string?

      return '' unless @changed_by_company_or_company

      if @ajs_or_history_item.source_application&.api?
        oauth_application = @ajs_or_history_item.source_application&.oauth_application

        # Client name || Partner App name
        oauth_application&.owner&.name || oauth_application&.name
      elsif @viewing_company&.id == @changed_by_company_or_company.id
        [@changed_by_company_or_company.name, @ajs_or_history_item.user_name].reject(&:blank?).join(BULLET)
      else
        @changed_by_company_or_company.name
      end
    end

  end

end
