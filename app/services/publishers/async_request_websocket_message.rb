# frozen_string_literal: true

# This is a generalization of PCC::Publishers::Websocket
#
# TODO we should probably refactor PCC to use this.
module Publishers
  class AsyncRequestWebsocketMessage

    include Interactor
    include Interactor::Swoop

    def call
      @request = API::AsyncRequest.find(context.id)

      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: @request.id,
            data: data,
          },
          company: Company.find(@request.company_id),
        }
      )
    end

    private

    def data
      context.input.merge(async_request: { id: @request.id })
    end

  end
end
