# frozen_string_literal: true

module Publishers
  class GenericWebsocketMessage

    include Interactor
    include Interactor::Swoop

    def call
      raise ArgumentError, "Publishers::GenericWebsocketMessage needs a class_name to be passed into context.input" if context.input[:class_name].blank?

      raise ArgumentError, "Publishers::GenericWebsocketMessage needs a target_data to be passed into context.input" if context.input[:target_data].blank?

      raise ArgumentError, "Publishers::GenericWebsocketMessage needs a company to be passed into context.input" if context.input[:company].blank?

      msg = {
        class: class_name,
        operation: operation,
        target: data,
      }

      msg = msg.to_json

      Rails.logger.debug "Publishers::GenericWebSocketMessage Sending: #{msg}"

      channels.each do |channel|
        AsyncRedis.instance.publish(channel, msg)
      end
    end

    private

    def channels
      context.channels || ["event_#{context.input[:company].channel_name}"]
    end

    def class_name
      context.input[:class_name]
    end

    def operation
      context.input[:operation] || 'update'
    end

    def data
      context.input[:target_data]
    end

  end
end
