# frozen_string_literal: true

class CompanySearchService < SearchService

  class << self

    def model_class
      Company
    end

    def power_searchable_fields
      @power_searchable_fields ||= [
        [:accounting_email, :string, :accounting_email],
        [:address, :string, :"location.address"],
        [:dispatch_email, :string, :dispatch_email],
        [:id, :integer, :id],
        [:agero_vendor_id, :integer, :agero_vendor_id],
        [:name, :string, :name],
        [:phone, :phone, :phone],
        [:support_email, :string, :support_email],
        [:type, :string, :type],
      ].freeze
    end

    def filterable_fields
      @filterable_fields ||= [
        [:parent_company_id, :parent_company_id],
        [:subtype, :subtype],
      ].freeze
    end

    def default_sort_order
      { sort_name: :asc }
    end

  end

end
