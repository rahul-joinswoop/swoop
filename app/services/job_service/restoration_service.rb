# frozen_string_literal: true

module JobService
  # This service will change a job's state from 'deleted' to its previous
  # valid state
  class RestorationService

    include Interactor

    class NoValidPreviousStatus < StandardError; end

    before do
      @job = context.job
    end

    def call
      raise NoValidPreviousStatus if previous_valid_status.blank?

      SwoopUpdateJobState.new(@job, previous_valid_status)

      restore_job!
      restore_invoices!
    end

    private

    def restore_invoices!
      deleted_job_invoices.find_each do |invoice|
        invoice&.update!(deleted_at: nil)
      end
    end

    def restore_job!
      @job.status = previous_valid_status
      @job.deleted_at = nil
      @job.save!
    end

    def deleted_job_invoices
      Invoice.where.not(deleted_at: nil).where(job: @job)
    end

    def previous_valid_status
      @previous_status ||= AuditJobStatus.where(job: @job)
        .where.not(job_status_id: JobStatus::DELETED)
        .last&.status_name
    end

  end
end
