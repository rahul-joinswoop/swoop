# frozen_string_literal: true

module JobService
  class RemoveStorage

    include Interactor

    attr_accessor :job

    before do
      self.job = context.job
    end

    def call
      Rails.logger.debug "STORAGE(#{job.id}) undo_storage called"

      stored_audit_job_status =
        job.ajs_by_status_id(JobStatus::STORED)

      job.will_store = false
      job.status_change(:completed)
      job.status = Job::STATUS_COMPLETED

      unless stored_audit_job_status
        raise ArgumentError, "STORAGE(#{job.id}) Undo Storage unable to find AuditJobStatus"
      end

      stored_audit_job_status.deleted_at = Time.now
      stored_audit_job_status.save!
    end

  end
end
