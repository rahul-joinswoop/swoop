# frozen_string_literal: true

module JobService
  class SwoopAssignRescueProviderService < AssignRescueProviderService

    def initialize(job, rescue_company, site)
      super(job, rescue_company, site, :swoop_assign)
    end

  end
end
