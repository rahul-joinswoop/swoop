# frozen_string_literal: true

module JobService
  class ReverseGeocodeIfNecessary

    def initialize(job)
      Rails.logger.debug "JobService::ReverseGeocode initialized with #{job&.id}"
      @job = job
    end

    def call
      Rails.logger.debug "JobService::ReverseGeocode called"
      reverse_geocode_if_necessary(@job.service_location)
      reverse_geocode_if_necessary(@job.drop_location)
    end

    def reverse_geocode_if_necessary(location)
      if location
        if location && location.latlng_only?
          PlacesResolver.perform_async(location.id)
        end
      end
    end

  end
end
