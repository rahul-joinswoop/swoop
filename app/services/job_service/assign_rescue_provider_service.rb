# frozen_string_literal: true

module JobService
  class AssignRescueProviderService

    include SwoopService

    def initialize(job, rescue_company, site, assign_company_event)
      raise ArgumentError if [job, rescue_company, site, assign_company_event].include?(nil)

      Rails.logger.debug("#{self.class.name} - executing with #{rescue_company.id}")

      @rescue_company = rescue_company
      @job = job
      @site = site
      @assign_company_event = assign_company_event
    end

    def execute
      Rails.logger.debug("#{self.class.name} - executing with #{@rescue_company.id}")

      @job.assign_rescue_company(@site)
      @job.send(@assign_company_event)
      @job.dispatch_email_if_live_and_open
    end

  end
end
