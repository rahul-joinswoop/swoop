# frozen_string_literal: true

module JobService
  class CreateStorage

    include LoggableJob
    include Interactor

    class MissingStorageUser < StandardError; end

    before do
      @job = context.job
      @user = context.user

      # Storage must be assigned a user
      raise MissingStorageUser if @user.nil?

      raise log_msg("Create Storage called when job has existing storage", job: @job.id) if @job.storage
    end

    def call
      log :debug, "in create storage", job: @job.id

      @job.build_storage(
        item: @job.driver.vehicle,
        site: @job.drop_location.site,
        stored_at: Time.zone.now,
        company: @job.rescue_company,
        user: @user
      )

      # Set's job status as stored,
      # marks invoice as dirty
      # adds AJS entry for storage
      @job.store_vehicle

      @job.save!

      # trigger this manually here
      GraphQL::JobStatusChangedWorker.perform_async(@job.id, status: ['towdestination', 'stored'])
    end

    private

    def deleted_stored_ajs
      @job.all_audit_job_statuses
        .where.not(deleted_at: nil)
        .find_by(job_status_id: JobStatus::STORED)
    end

  end
end
