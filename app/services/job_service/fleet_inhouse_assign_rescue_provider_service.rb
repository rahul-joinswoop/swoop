# frozen_string_literal: true

module JobService
  class FleetInhouseAssignRescueProviderService < AssignRescueProviderService

    include SwoopService

    def initialize(job, rescue_company, site)
      super(job, rescue_company, site, :fleet_inhouse_assign)
    end

  end
end
