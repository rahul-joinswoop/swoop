# frozen_string_literal: true

module JobService
  # This service will move an invoice associated to the given job to state
  # :partner_new if the invoice has a specific state and the job's associated
  # CustomerType (or PaymentType as it's aliased) also has a specific name.
  class ChangePaymentType

    include SwoopService

    REQUIRE_INVOICE_TRANSITION = {
      CustomerType::WARRANTY => [
        CustomerType::PCARD, CustomerType::CUSTOMER_PAY,
      ],
      CustomerType::GOODWILL => [
        CustomerType::PCARD, CustomerType::CUSTOMER_PAY,
      ],
      CustomerType::PCARD => [
        CustomerType::WARRANTY, CustomerType::GOODWILL,
      ],
      CustomerType::CUSTOMER_PAY => [
        CustomerType::WARRANTY, CustomerType::GOODWILL,
      ],
    }.freeze

    def initialize(job:, old_payment_type:)
      @job = job
      @invoice = @job.invoice
      @old_payment_type = old_payment_type
      @new_payment_type = @job.customer_type
    end

    def execute
      return unless @invoice

      required_list = REQUIRE_INVOICE_TRANSITION[@old_payment_type.name]

      return unless required_list &&
                    required_list.include?(@new_payment_type.name)

      return unless Invoice::STATES_PAST_SENT_FOR_FLEET.include?(@invoice.state)

      @invoice.partner_new!
    end

  end
end
