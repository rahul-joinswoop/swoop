# frozen_string_literal: true

module JobService
  # Heads up: this saves the job when AuditJobStatus.create is executed!
  #
  # One (not so clear) consequence when this happens is:
  #   - this status transition will call log_transition to create an AJS to 'Created', which in turn will call this method again
  #       with status_change('Created')
  #
  # TODO Should we refactor this to AuditJobStatus.new and rely on service transaction to commit it to DB exclusively?
  class StatusChange

    include Interactor

    attr_accessor :job, :new_status, :ajs

    before do
      @job = context.job
      @new_status = context.new_status.downcase
      @ajs = context.ajs
    end

    def call
      Rails.logger.debug "JobService::StatusChange called with #{new_status}, #{@ajs}"

      previous = nil

      if ajs
        Rails.logger.debug "JobService::StatusChange updating existing ajs"
        previous = ajs.last_set_dttm
        previous = ajs.adjusted_dttm if ajs.adjusted_dttm

        ajs.last_set_dttm = Time.now
        ajs.deleted_at = nil
      else
        Rails.logger.debug "JOB(#{job.id}): JobService::StatusChange new ajs"

        # PDW: without the self here, this does not work
        self.ajs = @job.audit_job_statuses.build(
          job_status_id: JobStatus::NAME_MAP[Job.statuses[new_status].to_sym],
          last_set_dttm: Time.now,
          company: job.fleet_company,
          user: job.last_touched_by,
          source_application_id: RequestContext.current.source_application_id,
        )

      end

      Rails.logger.debug "JobService::StatusChange calling UpdateLastUpdatedStatusOnJob with ajs:#{ajs}"
      UpdateLastUpdatedStatusOnJob.new(job, ajs, previous).call

      context.job = job
    end

  end
end
