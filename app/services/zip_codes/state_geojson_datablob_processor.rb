# frozen_string_literal: true

require 'json'

# ZipCodes module holds the necessary functionality related to converting ZipCode subset to Territories.
module ZipCodes
  # Ingests the GEOJSON data data blob and creates separate polygon records.
  # This functionality takes in one zipcode ata time and create territories.
  class StateGeojsonDatablobProcessor

    TWENTY_TEN_CENSUS_5_DIGIT_ZIP = "ZCTA5CE10"

    class JSONCouldNotBeParsedError < ArgumentError; end

    class JSONBlobCannotBeEmptyError < ArgumentError; end

    def initialize(json_blob_data)
      # Halt the execution If the inputs file to be processed is not present
      raise JSONBlobCannotBeEmptyError if json_blob_data.blank?

      @json_blob_data = json_blob_data
    end

    # PostGIS Polygon creation for each Geojson data blob provided.
    # Each provided object is a zipcode with lat/lang boundary details.
    # Each of these boundaries for a zipcode are created as a record/records based on the Geometry Type.
    def call
      json_blob_data = JSON.parse(@json_blob_data.to_json)
      unless valid_json?(@json_blob_data)
        Rails.logger.debug("No Features found in file - #{json_blob_data}")
        return
      end
      # Each record provided in hash form has to be parsed and converted o JSON
      # ZCTA5CE10 Attribute Definition: 2010 Census 5-digit ZIP Code Tabulation Area code
      properties_object = json_blob_data["properties"]
      zip_code_from_record = properties_object[TWENTY_TEN_CENSUS_5_DIGIT_ZIP] if properties_object.key?(TWENTY_TEN_CENSUS_5_DIGIT_ZIP)
      geometry_type = json_blob_data.try(:[], 'geometry').try(:[], 'type')
      if geometry_type == "MultiPolygon"
        Rails.logger.debug("Processing MultiPolygon record for  zip - #{zip_code_from_record}")
        ZipCodes::IngestGeojsonData.create_multipolygon_record(zip_code_from_record, json_blob_data)
      elsif geometry_type == "Polygon"
        Rails.logger.debug("Processing Polygon record for  zip - #{zip_code_from_record}")
        ZipCodes::IngestGeojsonData.create_polygon_record(zip_code_from_record, json_blob_data)
      else
        Rails.logger.debug("Unhandled geometry type - #{geometry_type}")
      end
    end

    private

    def valid_json?(json)
      JSON.parse(json.to_json)
    rescue JSON::ParserError => e
      Rails.logger.error("JSON blob could not be parsed - #{e}")
      raise e
    rescue StandardError => e
      Rails.logger.error("Something wrong in parsing JSON data - #{e}")
      raise e
    end

  end
end
