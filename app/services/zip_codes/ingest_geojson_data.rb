# frozen_string_literal: true

require 'json'

# ZipCodes module holds the necessary functionality related to converting ZipCode subset to Territories.
module ZipCodes
  # Ingests the GEOJSON data file and creates seperate polygon records.
  # This functionality takes care of both Polygon and MultiPolygon Geometries and converts them to territories mapped to zipcode
  class IngestGeojsonData

    TWENTY_TEN_CENSUS_5_DIGIT_ZIP = "ZCTA5CE10"

    class FileObjectNotFoundError < ArgumentError; end

    class ZipCodeCannotBeNilError < ArgumentError; end

    def initialize(file_object)
      # Halt the execution If the inputs file to be processed is not present
      raise FileObjectNotFoundError if file_object.blank?

      @file_object = file_object
    end

    # PostGIS Polygon creation for each zip code is initialized.
    # Each state GeoJSON data file is processed (Check for zipcode and if the area type is polygon or multipolygon) for
    # individual zipcode and create a record/records based on the Geometry Type.
    def call
      state_zip_code_data = open_zip_code_coordinate_data_file
      unless state_zip_code_data.try(:[], 'features')
        Rails.logger.debug("No Features found in file - #{@file_object}")
        return
      end
      state_zip_code_data["features"].each do |record|
        # ZCTA5CE10 Attribute Definition: 2010 Census 5-digit ZIP Code Tabulation Area code
        zip_code_for_record_being_processed_from_coordinate_data_file = record["properties"][TWENTY_TEN_CENSUS_5_DIGIT_ZIP] if record["properties"].key?(TWENTY_TEN_CENSUS_5_DIGIT_ZIP)
        geometry_type = record.try(:[], 'geometry').try(:[], 'type')
        if geometry_type == "MultiPolygon"
          self.class.create_multipolygon_record(zip_code_for_record_being_processed_from_coordinate_data_file, record)
        elsif geometry_type == "Polygon"
          self.class.create_polygon_record(zip_code_for_record_being_processed_from_coordinate_data_file, record)
        else
          Rails.logger.debug("Unhandled geometry type - #{geometry_type}")
        end
      end
    end

    # Opens each file and returns parsed JSON data.
    def open_zip_code_coordinate_data_file
      file = open(@file_object)
      JSON.load file
    rescue Oj::ParseError => e
      Rails.logger.error("File could not be loaded as a JSON file #{e}")
      raise e
    rescue => e
      Rails.logger.error("Something wrong in open_zip_code_coordinate_data_file - #{e}")
      raise e
    end

    # frozen_string_literal being true prevents the transformation of a string. To convert the return LINEARSTRING object
    # to POLYGON from RGeo decoding, we need to remove the LINESTRING part of the string.
    def self.remove_linearstring_keyword(string_to_be_edited)
      string_to_be_edited.dup.sub! 'LINESTRING ', ''
    end

    # Intakes a polygon record for a zip code and creates the polygon area for the exterior ring.
    def self.create_polygon_record(zip_code_for_record_being_processed_from_coordinate_data_file, record)
      decoded_record_to_linear_string = RGeo::GeoJSON.decode(record)
      polygon_area = remove_linearstring_keyword("POLYGON(#{decoded_record_to_linear_string.geometry.exterior_ring})")
      create(zip_code_for_record_being_processed_from_coordinate_data_file, polygon_area)
    end

    # Intakes a multipolygon record for a zip code and extracts polygons (Inner rings are exterior rings are excluded)
    def self.create_multipolygon_record(zip_code_for_record_being_processed_from_coordinate_data_file, record)
      decoded_record_to_linear_string = RGeo::GeoJSON.decode(record)
      all_exterior_polygons = get_all_exterior_polygons_for_multipolygon(decoded_record_to_linear_string.geometry)
      all_exterior_polygons.each do |polygon|
        polygon_area = remove_linearstring_keyword("POLYGON(#{polygon})")
        create(zip_code_for_record_being_processed_from_coordinate_data_file, polygon_area)
      end
    end

    # Creates a record for a specific zip code and polygon
    def self.create(zip_code_for_record_being_processed_from_coordinate_data_file, polygon)
      raise ZipCodeCannotBeNilError if zip_code_for_record_being_processed_from_coordinate_data_file.nil?

      ZipCodeSubsetToTerritory.create! zip: zip_code_for_record_being_processed_from_coordinate_data_file, polygon_area: polygon
    end

    def self.get_all_exterior_polygons_for_multipolygon(multi_polygons)
      Territory.find_exterior_polygons(multi_polygons)
    end

  end
end
