# frozen_string_literal: true

module ZipCodes
  # Initializes download of GEOJSON datafile and feeds to StateGeojsonDatablobProcessor.
  class StateJsonFileToPolygonCreator

    def call(geojson_data_file_obj)
      downloader = S3Downloader.new
      local_geojson_data_file_path = downloader.download_specific_file(geojson_data_file_obj)
      state_zip_code_data = ZipCodes::IngestGeojsonData.new(local_geojson_data_file_path).open_zip_code_coordinate_data_file
      features = state_zip_code_data.try(:[], 'features')
      if features.present? && features.respond_to?(:each)
        features.each do |feature|
          ZipCodes::StateGeojsonDatablobProcessor.new(feature).call
        end
      end
      nil
    end

  end
end
