# frozen_string_literal: true

class JobProviderChanger < Service

  class MissingFleetCompanyError < StandardError; end
  class MissingRescueCompanyError < StandardError; end
  class IncorrectOldCompanyError < StandardError; end

  def initialize(job, old_rescue_company_id, new_rescue_company_id)
    super()
    if [job, old_rescue_company_id, new_rescue_company_id].include? nil
      raise ArgumentError
    end
    @job = job
    @old_rescue_company_id = old_rescue_company_id
    @new_rescue_company_id = new_rescue_company_id
  end

  def call
    raise MissingFleetCompanyError if @job.fleet_company.blank?
    raise MissingRescueCompanyError if @job.rescue_company.blank?
    old = Company.find(@old_rescue_company_id)
    raise IncorrectOldCompanyError if @job.rescue_company != old
    new_rescue_company = Company.find(@new_rescue_company_id)
    @job.rescue_company = new_rescue_company
    @job.site = new_rescue_company.sites.first

    if @job.fleet_company.fleet_in_house?
      account = new_rescue_company.accounts.create_with(company: new_rescue_company).find_or_initialize_by(client_company: @job.fleet_company)
    else
      account = new_rescue_company.accounts.create_with(company: new_rescue_company).find_or_initialize_by(client_company_id: Company.swoop_id)
    end
    @mutated_objects.push account if account.new_record?

    @job.account = account

    if @job.invoice
      @job.delete_invoice
      @mutated_objects.push @job.invoice
      @job.invoice_id = nil
    end
    @mutated_objects.push @job
  end

end
