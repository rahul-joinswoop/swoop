# frozen_string_literal: true

#
# Queries the db for ScheduledSystemAlerts based on the send_at value,
# and triggers a Job Call alert to Swoop agents that have 'System Alerts' enabled.
#
# To enable System Alerts to a user: log in as a Swoop root user,
# then go to settings -> users -> select a user from Swoop company -> enable it in the form
#
module JobAlerts
  class NinetyMinutesFromSchedule

    include LoggableJob

    def call
      alerts = ScheduledSystemAlert
        .where('send_at < ?', 1.minutes.from_now)
        .where(sent_at: nil, deleted_at: nil)
        .includes(:job)

      alerts.each do |alert|
        job = alert.job

        if ["pending", "unassigned"].include?(job.status)

          log :debug, "SCHEDULED_SYSTEM_ALERT calling", job: job.id

          calc = SystemEmailRecipientCalculator.new(job)
          users = calc.calculate

          users.each do |user|
            log :debug, "SCHEDULED_SYSTEM_ALERT phoning #{user.full_name}(#{user.phone})", job: job.id
            Job.new_job_call_swoop_agents(user.phone, job.id)
          end

          alert.update!(sent_at: Time.now)
        else
          log :debug, "SCHEDULED_SYSTEM_ALERT cancel call as status: #{job.status}", job: job.id
          alert.update!(deleted_at: Time.now)
        end
      end
    end

  end
end
