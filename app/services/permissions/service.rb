# frozen_string_literal: true

#
# A YAML Permissions backed query generator
#
# Permissions::Service.read(Job, user_or_company) => AR query of all jobs readable by user_or_company
#

module Permissions
  class Service

    #
    # Read
    #
    # Given appropriate permissions in config/roles.yml and config/permissions.yml
    #
    # flt_driver = User.find(31)
    # flt_driver.add_role(:run_jobs)
    #
    # Permissions::Service.read(Job, flt_driver)
    #   => Job Load (19.0ms)  SELECT "jobs".* FROM "jobs" WHERE "jobs"."rescue_driver_id" = 31
    #
    def self.read(subject, accessor)
      Rails.logger.debug "Permissions::Service - read() accessor: #{accessor.class.name}(#{accessor.id}), subject:#{subject}"
      queries = []
      Permissions::YamlHelper.each_permission(accessor, 'read', subject.name) do |perm|
        Rails.logger.debug "Permissions::Service - read() processing permission: #{perm.inspect}"
        query = subject.all
        if perm.scope
          perm.scope.each do |k, v|
            Rails.logger.debug "Dealing with scope #{k} #{v}"

            if v == "user"
              arg = accessor
            elsif v == "company"
              if accessor.is_a?(User)
                arg = accessor.company
              else
                arg = accessor
              end
            else
              arg = v
            end
            # I'm a little wary here, maybe this would be safer/clearer as two columns:
            # A scope column for this and a where column for the where clause
            if subject.methods.include?(k.to_sym)
              query = query.send(k, arg)
            else
              query = query.where({ k => arg })
            end
          end
        end
        queries << query
      end

      queries.reduce { |q, x| q.union(x) } || subject.none
    end

    #
    # View Permissions for attributes
    #
    # Initial thinking here is to have a 'view' action and add an attributes field to the permissions yaml.
    #
    # rescue_company_role,
    #   action: 'view',
    #   subject_type: 'Job',
    #   attributes: [:invoice,'invoice.total']
    # )
    def self.view(subject, accessor, attribute)
      # TBD
    end

  end
end
