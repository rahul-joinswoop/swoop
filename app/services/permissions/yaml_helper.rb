# frozen_string_literal: true

module Permissions
  class YamlHelper

    def self.each_permission(accessor, action, subject)
      accessor.roles.each do |role|
        Rails.logger.debug "YamlHelper - looking up role: #{role.inspect}"
        permission_name_array = Rails.configuration.roles[role.name] || []
        Rails.logger.debug "YamlHelper - permissions: #{permission_name_array}"
        permission_name_array.each do |permission_name|
          perm = OpenStruct.new(Rails.configuration.permissions[permission_name])
          Rails.logger.debug "YamlHelper - found permission: #{perm}"
          if perm.action == action && perm.subject_type == subject
            yield perm
          end
        end
      end

      nil
    end

  end
end
