# frozen_string_literal: true

require 'twilio-ruby'
require 'ostruct'

class SwoopTwilio

  class StubTwilioResponse < OpenStruct; end

  # See https://www.twilio.com/docs/api/errors/21610
  TWILIO_BLACKLIST_ERROR_CODE = "21610" unless defined? TWILIO_BLACKLIST_ERROR_CODE
  TWILIO_INVALID_ERROR_CODE = "20404" unless defined? TWILIO_INVALID_ERROR_CODE
  # See https://www.twilio.com/docs/api/errors/21614
  TWILIO_INVALID_TO_MOBILE_CODE = "21614" unless defined? TWILIO_INVALID_TO_MOBILE_CODE
  # https://www.twilio.com/docs/api/errors/21211
  TWILIO_INVALID_TO_PHONE_CODE = "21211" unless defined? TWILIO_INVALID_TO_PHONE_CODE

  attr_reader :account_sid, :auth_token

  def initialize
    @account_sid = ::ENV['TWILIO_ACCOUNT_ID']
    @auth_token  = ::ENV['TWILIO_AUTH_TOKEN']
  end

  def client
    @twilio ||= Twilio::REST::Client.new(account_sid, auth_token)
  end

  def message_create(options)
    use_log(options)
    return if blacklisted?(options, type: :message)
    return StubTwilioResponse.new(options) unless twilio_enabled?

    begin
      # rubocop:disable Rails/SaveBang
      client.account.messages.create(options)
      # rubocop:enable Rails/SaveBang
    rescue Twilio::REST::RequestError => e
      Rails.logger.debug "twilio err #{e}"

      raise e unless [
        TWILIO_BLACKLIST_ERROR_CODE,
        TWILIO_INVALID_ERROR_CODE,
        TWILIO_INVALID_TO_PHONE_CODE,
        TWILIO_INVALID_TO_MOBILE_CODE,
      ].include? e.code.to_s
    end
  end

  def call_create(options)
    use_log(options)
    return if blacklisted?(options, type: :call)
    return StubTwilioResponse.new(options) unless twilio_enabled?
    # rubocop:disable Rails/SaveBang
    client.account.calls.create(options)
    # rubocop:enable Rails/SaveBang
  end

  private

  def blacklisted?(options, type:)
    if PhoneNumberBlacklist.blacklisted? options[:to]
      Rails.logger.debug "Not creating Twilio #{type} to blacklisted number #{options[:to]}"
    end
  end

  def twilio_enabled?
    Rails.env.production? || (ENV['ENABLE_TWILIO'].present? && !Rails.env.test?)
  end

  def use_log(log_message)
    calling_method = caller_locations(1, 1)[0].label
    stubbed        = 'STUB' unless twilio_enabled?

    Rails.logger.debug "#{stubbed} TwilioClient::#{calling_method} #{log_message}"
  end

end
