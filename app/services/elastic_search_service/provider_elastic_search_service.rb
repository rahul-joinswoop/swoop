# frozen_string_literal: true

# service = ElasticSearchService::Provider.new({page: 1,per_page: 10})
#
# total,records = service.search({search_terms:{term:"Finish Line"}, filters:{"company_id" => 9}})
#
# records.map(&:id)
#
module ElasticSearchService
  class ProviderElasticSearchService < SearchService

    class << self

      def model_class
        RescueProvider
      end

      def power_searchable_fields
        @power_searchable_fields ||= [
          [:id, :integer, :id],
          [:status, :string, :status],
          [:name, :string, :name],
          [:dispatch_phone, :phone, :dispatch_phone],
          [:dispatch_email, :string, :dispatch_email],
          [:fax, :phone, :fax],
          [:address, :string, :address],
          [:primary_contact, :string, :primary_contact],
          [:primary_phone, :phone, :primary_phone],
          [:primary_email, :string, :primary_email],
          [:accounting_email, :string, :accounting_email],
          [:special_instructions, :string, :notes],
          [:vendor_id, :string, :vendor_code],
          [:location_id, :string, :location_code],
          [:network, :string, :network],
          [:partner_id, :integer, :"provider.id"],
        ].freeze
      end

      def filterable_fields
        @filterable_fields ||= [[:company_id, :"company.id"]].freeze
      end

      def default_sort_order
        { sort_name: :asc }
      end

    end

  end
end
