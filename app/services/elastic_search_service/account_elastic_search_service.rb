# frozen_string_literal: true

# service = AccountSearchService.new({page: 1,per_page: 10})
# total,records = service.search({search_terms:{term:"Tesla"}, filters:{"company_id" => 17}})
# records.map(&:id)
module ElasticSearchService
  class AccountElasticSearchService < SearchService

    class << self

      def model_class
        Account
      end

      def power_searchable_fields
        @power_searchable_fields ||= [
          [:id, :integer, :id],
          [:name, :string, :name],
          [:address, :string, :billing_address],
          [:primary_phone, :phone, :primary_phone],
          [:primary_email, :string, :primary_email],
          [:accounting_email, :string, :accounting_email],
          [:notes, :string, :notes],
        ].freeze
      end

      def filterable_fields
        @filterable_fields ||= [[:company_id, :"company.id"]].freeze
      end

      def default_sort_order
        { sort_name: :asc }
      end

    end

  end
end
