# frozen_string_literal: true

module Authentication
  class VerifyEmailForPartnerAPI

    include Interactor

    def call
      # Step 1 verify input params
      # TODO - i guess we could skip this since these would be caught by AR below
      # (but i copied this code from ResetForgottenPassword which used this pattern).
      context.fail!(error: "email is required!") if context.email.blank?
      context.fail!(error: "client_id is required!") if context.client_id.blank?
      context.fail!(error: "redirect_uri is required!") if context.redirect_uri.blank?

      # Step 2 instantiate the password_request object
      email_verification = PartnerAPIEmailVerification.new

      # Step 3 feed the input for the email_verification instance
      email_verification.user_input = context.email.downcase
      email_verification.user = User.find_by_email email_verification.user_input
      email_verification.client_id = context.client_id
      email_verification.redirect_uri = context.redirect_uri

      # Step 4 delegate the email verification to the email_verification object!
      context.result = begin
                         email_verification.trigger!
                       rescue StandardError => e
                         context.fail!(error: e.message)
                       end

      # Step 5 set the outcome
      context.fail!(error: "Invalid request") unless email_verification.valid_request?
    end

  end
end
