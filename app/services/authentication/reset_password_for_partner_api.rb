# frozen_string_literal: true

module Authentication
  class ResetPasswordForPartnerAPI

    include Interactor

    def call
      # Step 1 verify input params
      # TODO - i guess we could skip this since these would be caught by AR below
      # (but i copied this code from ResetForgottenPassword which used this pattern).
      context.fail!(error: "user is required!") if context.user.blank?
      context.fail!(error: "client is required!") if context.client.blank?

      # Step 2 instantiate the password_request object
      password_reset_request = PartnerAPIPasswordRequest.new

      # Step 3 feed the input for the password_request instance
      password_reset_request.user = context.user
      password_reset_request.client_id = context.client.uid

      # Step 4 delegate the pwd reset to the password_reset_request object!
      context.result = begin
                         # theoretically this could fail, although we don't actually
                         # enforce any validations for PasswordRequest so it shouldn't
                         # be possible in reality. still we handle this just in case
                         password_reset_request.trigger!
                       rescue StandardError => e
                         context.fail!(error: e.message)
                       end

      # Step 5 set the outcome
      context.fail!(error: "Invalid request") unless password_reset_request.valid_request?
    end

  end
end
