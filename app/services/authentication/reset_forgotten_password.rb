# frozen_string_literal: true

module Authentication
  class ResetForgottenPassword

    include Interactor

    def call
      user_identifier = context.user_input
      # Step 1 check for the necessary inputs
      context.fail!(error: "user_input is required!") if user_identifier.blank?

      # Step 2 instantiate the password_request object
      password_reset_request = PasswordRequest.new

      # Step 3 feed the input for the password_request instance
      user_identifier = user_identifier.downcase

      # Step 4 feed the input for the password_request instance
      password_reset_request.user_input = user_identifier
      password_reset_request.user = User.find_by_email_or_usename user_identifier

      # Step 5 delegate the pwd reset to the password_reset_request object!
      context.result = begin
                        # theoretically this could fail, although we don't actually
                        # enforce any validations for PasswordRequest so it shouldn't
                        # be possible in reality. still we handle this just in case
                        password_reset_request.trigger!
                       rescue StandardError => e
                         context.fail!(error: e.message)
                      end

      # Step 6 set the outcome
      context.fail! unless password_reset_request.valid_request?
    end

  end
end
