# frozen_string_literal: true

class UpdateDispatchableSites < Service

  def initialize(rescue_company, fleet_company, site_args)
    Rails.logger.debug "UpdateDispatchableSites called #{rescue_company.try(:name)}, #{fleet_company.try(:name)}, #{site_args}"

    raise ArgumentError, "RescueCompany missing" unless rescue_company
    raise ArgumentError, "FleetCompany missing" unless fleet_company
    @rescue_company = rescue_company
    @fleet_company = fleet_company
    @sites_args = site_args
  end

  def call
    objs = []
    @sites_args.each do |site_args|
      site = PartnerSite.find(site_args[:site_id])
      deleted_contractorids = []
      if ['AGO'].include? @fleet_company.issc_client_id
        deleted_contractorids = process_locationid(objs, site, site_args)
      elsif @fleet_company.issc_client_id == 'NSD'
        deleted_contractorids = process_login(objs, site, site_args)
      end
      process_rescue_provider(objs, site, site_args)
      process_contractorids(objs, site, site_args, deleted_contractorids)
      objs << site
    end
    objs
  end

  def validate_contractorids(contractorids)
    if contractorids
      contractorids.each do |contractorid|
        raise "Empty contractorid" if contractorid.blank?
      end
    end
  end

  def process_contractorids(objs, site, site_args, readd_contractorids)
    contractorids = site_args[:contractorids]
    Rails.logger.debug "UpdateDispatchableSites::process_contractorids contractorids:#{contractorids}"
    validate_contractorids(contractorids)
    if site_args.key?(:contractorids)
      if contractorids
        existing = site.contractorids(@fleet_company) # site.isscs.where(clientid: @fleet_company.issc_client_id).map(&:contractorid)
        Rails.logger.debug("UpdateDispatchableSites::process_contractorids Existing: #{existing}")
        existing -= readd_contractorids
        Rails.logger.debug("UpdateDispatchableSites::process_contractorids Existing: #{existing}")
        Rails.logger.debug("UpdateDispatchableSites::process_contractorids Existing- contractorids: #{(existing - contractorids)}")

        (existing - contractorids).each do |contractorid|
          Rails.logger.debug("UpdateDispatchableSites process_contractorids Deleting: #{contractorid}")

          all_isscs_for_contractorid = site.isscs.where(clientid: @fleet_company.issc_client_id, contractorid: contractorid, deleted_at: nil).to_a

          all_isscs_for_contractorid.each do |issc|
            Rails.logger.debug("UpdateDispatchableSites::process_contractorids to_delete: #{issc}")
            process_delete(objs, issc)
          end
        end

        (contractorids - existing).each do |contractorid|
          clientid = @fleet_company.issc_client_id

          issc = Issc.new(company: @rescue_company,
                          site: site,
                          contractorid: contractorid,
                          clientid: clientid,
                          issc_location: if Issc.provider_supports_location?(clientid) then site.issc_location_by_fleet(@fleet_company) end,
                          issc_login: if Issc.provider_supports_login?(clientid) then site.issc_login(@fleet_company) end)

          objs << issc
        end
      else
        site.isscs.where(clientid: @fleet_company.issc_client_id).update_all(delete_pending_at: Time.now)
      end
    end
  end

  def process_delete(changed_objs, to_delete)
    if to_delete
      if (to_delete.status.to_sym == :register_failed) || (to_delete.status.to_sym == :unregistered)
        to_delete.deleted_at = Time.now
      else
        to_delete.delete_pending_at = Time.now
        # to_delete.status=:deregistering PDW: need to know the state so we can log them out if necessary
      end
      changed_objs << to_delete
    end
  end

  def process_locationid(objs, site, site_args)
    deleted_contractorids = []
    # if site_args[:locationid]

    issc_location = site.issc_location_by_fleet(@fleet_company)
    site.set_issc_location_locationid(@fleet_company, site_args[:locationid])
    # If we have changed the location id
    if (issc_location.nil? ^ site_args[:locationid].blank?) || (issc_location && (issc_location[:locationid] != site_args[:locationid]))
      # Delete all coorisponding Isscs (the clientids will be recreated above)
      isscs = Issc.where(system: nil, site: site, clientid: @fleet_company.issc_client_id, company: @rescue_company, issc_location: issc_location, deleted_at: nil)
      isscs.each do |issc|
        if ['AGO'].include? issc.clientid
          # For AGO's delete them and recreate them
          issc.delete_pending_at = Time.now
          objs << issc
          deleted_contractorids << issc.contractorid
        end
      end
    end
    objs << site
    # end

    deleted_contractorids
  end

  def process_login(objs, site, site_args)
    Rails.logger.debug "process_login: #{site.name}"
    deleted_contractorids = []
    # if site_args[:locationid]

    issc_login = site.issc_login(@fleet_company)
    Rails.logger.debug "process_login: #{site.name} #{issc_login.try(:username)}"
    site.set_issc_login(@fleet_company, site_args[:username], site_args[:password])
    # If we have changed the location id
    if issc_login.nil? || issc_login[:username] != site_args[:username] || issc_login[:password] != site_args[:password]
      # Delete all coorisponding Isscs (the clientids will be recreated above)
      isscs = Issc.where(system: nil, site: site, clientid: @fleet_company.issc_client_id, company: @rescue_company, issc_login: issc_login, deleted_at: nil)
      Rails.logger.debug("process_login - ISSCS Count: #{isscs.length}")
      isscs.each do |issc|
        Rails.logger.debug "process_login: #{issc.status}"
        case issc.status.to_sym
        when :password_incorrect, :registered
          issc.issc_login = site.issc_login(@fleet_company)
          issc.provider_deregister
        when :logged_in
          issc.issc_login = site.issc_login(@fleet_company)
          issc.provider_logout
        else
          issc.delete_pending_at = Time.now
          deleted_contractorids << issc.contractorid
        end
        objs << issc
      end
    end
    objs << site
    # end

    Rails.logger.debug "process_login: #{objs}"
    deleted_contractorids
  end

  def process_rescue_provider(objs, site, site_args)
    if site_args[:enabled]
      rp = RescueProvider.find_by(company: @fleet_company, provider: @rescue_company, site: site)
      if rp
        if rp.deleted_at
          rp.deleted_at = nil
          objs << rp
        end
      else
        objs << AddClientCompanyToPartner.new(@rescue_company, @fleet_company, site).call
      end
    end
    if site_args[:enabled] == false
      rp = RescueProvider.find_by(company: @fleet_company, provider: @rescue_company, site: site)
      if rp
        rp.deleted_at = Time.now
        objs << rp
      end
    end
  end

end
