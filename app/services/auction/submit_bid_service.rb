# frozen_string_literal: true

module Auction
  # When a partner company submits a bid for the auction, we find the existing bid
  # row and update it with the incoming bid and the rating and cost from the chosen
  # site candidate
  class SubmitBidService < Service

    include AuctionHelper

    def initialize(job, rescue_company, submitted_bid_time, candidate_id = nil)
      @job = job
      @rescue_company = rescue_company
      raise ArgumentError, "SubmitBid: Missing job" unless @job
      raise ArgumentError, "SubmitBid: Missing rescue_company" unless @rescue_company
      raise ArgumentError, "SubmitBid: Missing bid" unless submitted_bid_time

      @submitted_bid_time = submitted_bid_time.is_a?(Time) ? submitted_bid_time : Integer(submitted_bid_time)

      @candidate_id = candidate_id
      if @candidate_id.present?
        @candidate_id = Integer(candidate_id)
        @candidate = Job::Candidate.find @candidate_id
      end
    end

    def call
      # Step 1: Find the bid for this rescue company, for this job
      bid = Bid.find_by!(company: @rescue_company, job: @job)

      # Step 2: find the candidate
      candidate = populate_candidate(@candidate_id, @job, @rescue_company)

      # Step 3: calculate the bid times
      submitted_bid_time, bid_dttm, now = calculate_bid_time(@submitted_bid_time)

      # Step 4: Update the stubbed Bid with the ETA, cost, and rating --
      # and mark it SUBMITTED
      bid.update!(bid_submission_dttm: now,
                  eta_mins: submitted_bid_time,
                  eta_dttm: bid_dttm,
                  status: Bid::SUBMITTED,
                  candidate: candidate,
                  cost: candidate.cost,
                  rating: candidate.rating)

      # Step 5: Terminate the auction early if no eligible bidders are left
      expire_auction_immediately_if_no_bidders_remain(bid, @job)

      # Step 6: Notify slack
      Auction.slack_notification(@job, "  Received Bid: #{bid.company.name}, ETA: #{bid.eta_mins}")

      # Step 7: Profit!
      bid
    end

    private

    def calculate_bid_time(bid_minutes)
      now = Time.now

      if bid_minutes.is_a?(Time)
        # graphql passes in a Time object for submitted_bid_time so we use that for @bid_dttm and compute @submitted_bid_time
        # from it
        submitted_bid_time = ((bid_minutes - now) / 60).round
        bid_dttm = bid_minutes
      else
        # our controllers pass in an Int for submitted_bid_time which we use for submitted_bid_time. we have to compute bid_dttm
        # from it
        submitted_bid_time = bid_minutes
        bid_dttm = now + bid_minutes.minutes
      end

      [submitted_bid_time, bid_dttm, now]
    end

    def populate_candidate(candidate_id, job, rescue_company)
      if @candidate.blank?
        @candidate = Job::Candidate.where(job: job, company: rescue_company).order('score DESC NULLS LAST').first
      end

      @candidate
    end

  end
end
