# frozen_string_literal: true

require_dependency 'auction/auction'

module Auction
  class ExpireAuctionService < EndAuctionService

    include LoggableJob

    private

    def find_winner
      @auction.ranker.calculate_and_set_bid_scores(@auction.id)

      best_bid = Bid.where(auction: @auction)
        .where.not(score: nil)
        .order(score: :desc)
        .first

      if best_bid
        log :debug,
            "winning bid: #{best_bid.company.name}, " \
            "eta:#{best_bid.eta_mins}",
            job: @job.id
      else
        log :debug, "no winning bid", job: @job.id
      end

      best_bid
    end

    def assign(winner)
      if winner
        won(winner)
      else
        if false # PDW: disabled by request ENG-4211
          # if  best_candidate
          #   candidate = best_candidate
          company = candidate.company
          @job.last_touched_by = dispatcher_user
          JobService::SwoopAssignRescueProviderService.new(@job, company, company.hq).call
          Job::Candidate::Assignment.create!(
            job: @job,
            auto_assigned: true,
            chosen_provider: @job.fleet_company.live_rescue_providers.find_by(provider: company),
            chosen_company: company,
            eta: candidate.google_eta,
            status: Job::Candidate::Assignment::ASSIGNED,
            result: :auction_no_winner_assigned_best_truck_eta,
            vehicle: candidate.vehicle
          )
        else
          # in case of Auction Fail, the dispatcher should be removed from
          # the job as per ENG-5815
          @job.root_dispatcher_id = nil

          NoWinnerService.new(@job).call
        end
      end
    end

  end
end
