# frozen_string_literal: true

module Auction

  # When a partner company is invited to bid on an auction, and they decline to
  # bid, this service handles that rejection by finding the partner's "stub" Bid
  # record and changing its status to "provider rejected"
  class RejectBidService < Service

    include AuctionHelper

    def initialize(job, rescue_company, reject_reason_id, reject_reason_info)
      @job = job
      @rescue_company = rescue_company
      @reject_reason_id = reject_reason_id
      @reject_reason_info = reject_reason_info
      raise ArgumentError, "RejectBidService: Missing job" unless @job
      raise ArgumentError, "RejectBidService: Missing rescue_company" unless @rescue_company
      raise ArgumentError, "RejectBidService: Missing reject reason" unless @reject_reason_id
    end

    def call
      # Step 1: Find the bid for this rescue company, for this job
      bid = Bid.find_by!(company: @rescue_company, job: @job)

      # Step 2: Ensure we're trying to reject a bid that is eligible
      # for rejection:

      # Step 2.1: If the bid is already rejected, expired, or cancelled,
      # it's too late to be rejected, so don't let it be re-rejected; this
      # prevents race conditions where API users reject a job twice
      # (there's sometimes a lag marking it as rejected on the front end)
      if [Bid::PROVIDER_REJECTED, Bid::AUTO_REJECTED, Bid::EXPIRED, Bid::CANCELED].include?(bid.status)
        Auction.slack_notification(@job, "  Bid Re-Rejected (no-op): #{bid.company.name}, reason: #{bid.job_reject_reason&.text}")
        raise CannotRejectABidThatIsPastRejectionStageError, bid.status.to_s
      end

      # Step 2.2: If the bid isn't in the requested state, it can't be rejected
      if bid.status != Bid::REQUESTED
        raise CannotRejectABidThatIsntInRequestedStateError, bid.status.to_s
      end

      # Step 3: Update the stubbed Bid with the reason for rejection, and the
      # PROVIDER REJECTED status
      bid.update!(bid_submission_dttm: Time.now,
                  job_reject_reason_id: @reject_reason_id,
                  job_reject_reason_info: @reject_reason_info,
                  status: Bid::PROVIDER_REJECTED)

      # Step 4: Terminate the auction early if no eligible bidders are left
      expire_auction_immediately_if_no_bidders_remain(bid, @job)

      # Step 5: send slack notifications
      Auction.slack_notification(@job, "  Bid Rejected (successfully): #{bid.company.name}, reason: #{bid.job_reject_reason&.text}")

      # Step 6:
      bid
    end

  end

  class CannotRejectABidThatIsntInRequestedStateError < StandardError; end
  class CannotRejectABidThatIsPastRejectionStageError < StandardError; end

end
