# frozen_string_literal: true

require_dependency 'auction/auction'

module Auction
  class CancelAuctionService < Service

    include LoggableJob

    def initialize(auction_id)
      Rails.logger.debug("CancelAuctionService init called #{auction_id}")
      @auction = Auction.find(auction_id)

      if @auction.blank?
        raise ArgumentError, "CancelAuctionService Auction is missing"
      end

      @job = @auction.job

      log :debug, "CancelAuctionService initialising with auction status: #{@auction.status}, job status: #{@job.status}", job: @job.id
    end

    def call
      @auction.with_lock do
        execute
      end
    end

    def execute
      log :debug, "CancelAuctionService starting'", job: @job.id
      if @auction.status == Auction::LIVE
        close_auction
      end
    end

    private

    def close_auction
      @auction.bids.each do |bid|
        bid.status = Bid::CANCELED
        bid.save!
      end
      @auction.status = Auction::CANCELED
      @auction.save!
    end

  end
end
