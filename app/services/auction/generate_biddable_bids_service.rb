# frozen_string_literal: true

require_dependency 'auction/auction'

#
# GENERATE BIDDABLE BIDS SERVICE
#
# For an auction job, generate bids for it by:
# - finding sites in range
# - finding tow trucks in range
# - gathering up the ratings of each site and truck
# - calculating the cost of each site and truck
# - using those ratings and costs to calculate a "score" for each site and truck
# - creating bids for each site and truck, that are ready to be bid on
#   (hence, "biddable" bids)
#
module Auction

  class GenerateBiddableBidsError < StandardError
  end

  class CannotGenerateBidsForFleetCompanyWithoutAutomaticDispatchError < GenerateBiddableBidsError

    def initialize(job_id:, auction:, fleet_company_id:, unautodispatchable:)
      super
    end

  end

  class CannotGenerateBidsBecauseNoCandidatesInRangeError < GenerateBiddableBidsError

    def initialize(job_id:, auction:, site_candidates:)
      super
    end

  end
  class CannotGenerateBidsBecauseNoCandidatesHaveScoresError < GenerateBiddableBidsError

    def initialize(job_id:, auction:, companies:)
      super
    end

  end

  class GenerateBiddableBidsService < Service

    include LoggableJob

    def initialize(job:, auction:)
      raise ArgumentError, "GenerateBiddableBidsService job must be a Job" unless job.is_a?(Job)
      raise ArgumentError, "GenerateBiddableBidsService auction must be an Auction" unless auction.instance_of?(Auction)

      @job = job
      @auction = auction
    end

    def call
      log :debug, "starting to create bids'", job: @job.id

      # Step 1: Ensure the fleet has Feature::AUTO_ASSIGN, or else bids can't
      # be generated
      raise CannotGenerateBidsForFleetCompanyWithoutAutomaticDispatchError, job_id: @job.id, auction: @auction, fleet_company_id: @job.fleet_company&.id, unautodispatchable: @job.fleet_company&.cant_auto_swoop_dispatch? if @job.fleet_company.cant_auto_swoop_dispatch?

      # Check for Split (split.io)
      should_use_territories_to_select_candidates = (External::Split.treatment(@auction, 'territorial_candidates', 'off') == 'on')
      eligible_candidates = Job::Candidate.find_sites_and_vehicles_to_be_candidate_for_auction(@job) if should_use_territories_to_select_candidates

      # Step 2: Generate candidates for the auction that are brick-and-mortar
      # tow company locations in-range of the vehicle
      site_candidates = if should_use_territories_to_select_candidates
                          Job::Candidate::Site.create_all_for_job_auction_from_list_of_sites(job: @job, auction: @auction, sites: eligible_candidates.try(:[], :sites))
                        else
                          Job::Candidate::Site.create_all_for_job_auction_by_radius(job: @job, auction: @auction)
                        end

      # Step 3: Generate candidates for the auction that are tow trucks in-range of the vehicle
      truck_candidates = if should_use_territories_to_select_candidates
                           Job::Candidate::Truck.create_all_for_job_auction_from_list_of_rescue_vehicles(job: @job, auction: @auction, vehicles: eligible_candidates.try(:[], :rescue_vehicles))
                         else
                           Job::Candidate::Truck.create_all_for_job_auction_by_radius(job: @job, auction: @auction)
                         end

      # Step 4: Ensure there's at least one truck or site available (dispatchable)
      if truck_candidates.empty? && site_candidates.empty?
        raise CannotGenerateBidsBecauseNoCandidatesInRangeError, job_id: @job.id, auction: @auction, site_candidates: site_candidates
      end

      # Step 5: Add ratings and estimated costs to every site candidate and
      # truck candidate -- and if the cost can't be calculated, set the candidate
      # to an ineligible status
      #
      # N.B. this feature is currently gated with a "feature bit" called
      # ENABLE_CANDIDATE_COST_CALCULATION until product is ready to enable it
      Job::Candidate.populate_all_ratings_for_job(job: @job)
      (site_candidates + truck_candidates).each do |candidate|
        if ENV['ENABLE_CANDIDATE_COST_CALCULATION'] == 'TRUE'
          begin
            candidate.calculate_cost
          rescue Job::CannotCalculateCostForJobCandidateWithoutRateError
            candidate.update!(vehicle_eligible: :no_rate_to_calculate_cost_error)
          rescue Job::CannotCalculateCostForJobCandidateWithStorageRateError
            candidate.update!(vehicle_eligible: :cannot_calculate_cost_from_storage_rate_error)
          rescue Job::CannotCalculateCostForJobCandidateWithZeroDollarInvoiceError
            candidate.update!(vehicle_eligible: :zero_invoice_cost_error)
          rescue Job::JobCandidateCostCalculationError
            candidate.update!(vehicle_eligible: :cost_calculation_error)
          end
        else
          candidate.update!(cost: 100)
        end
      end

      # Step 6: Now, with ratings and projected costs calculated for all candidates,
      # calculate a "score" that says how good each candidate is for the job
      @auction.ranker.calculate_set_candidate_scores(@job.id)

      # Step 7: Finally, make a new bid for every rescue company that we have a "score" for
      # N.B. We gather the distinct rescue companies who have candidates with scores -- not
      # the trucks or sites specifically -- which product verbally interprets as "trucks don't
      # get auction jobs" because they see it as "tow companies get auction jobs"
      companies_who_will_have_bids_generated = <<~SQL
      SELECT
        companies.*,
        score
      FROM (
        SELECT
          company_id,
          max(score) AS score
          FROM auto_assign_candidates
          WHERE
            job_id=?
            AND score IS NOT NULL
          GROUP BY company_id
          ORDER BY max(score) DESC
        ) candidates_grouped_by_company
      LEFT JOIN companies ON
        candidates_grouped_by_company.company_id=companies.id
      LIMIT ?
      SQL
      companies = RescueCompany.find_by_sql([companies_who_will_have_bids_generated, @job.id, @auction.max_bidders])
      raise CannotGenerateBidsBecauseNoCandidatesHaveScoresError, job_id: @job.id, auction: @auction, companies: companies if companies.length == 0
      companies.each do |company|
        Bid.create!(
          auction: @auction,
          job: @job,
          company: company,
          status: 'new'
        )
      end
      Auction.slack_notification(@job, @auction.bids.map { |bid| "  Sending Bid Request : #{bid.company.name}" })
    end

  end

end
