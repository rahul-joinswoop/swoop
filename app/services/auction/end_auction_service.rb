# frozen_string_literal: true

require_dependency 'auction/auction'

module Auction
  class EndAuctionService < Service

    include LoggableJob

    def clz_name
      self.class.name
    end

    def find_winner
      raise UncallableAbstractParentMethodError
    end

    def initialize(auction_id)
      Rails.logger.debug("#{clz_name} init called auction:#{auction_id}")
      @auction = Auction.find(auction_id)

      if @auction.blank?
        raise ArgumentError, "#{clz_name} Auction is missing"
      end

      @job = @auction.job

      log :debug, "initialising with auction status: #{@auction.status}, job status: #{@job.status}", job: @job.id
    end

    def call
      # PDW: There will always be an expire running in sidekiq.
      # When @rp is set this service will be called again with the forced RP
      # Don't want the forced service and the timed service to clash.
      success = false
      winner = nil
      @auction.with_lock do
        success, winner = execute
      end
      announce(winner) if success
      success
    end

    private

    def execute
      log :debug, "execute starting'", job: @job.id
      if @auction.status == Auction::LIVE && @job.status == Job::STATUS_AUTO_ASSIGNING
        winner = find_winner
        close_auction(winner)
        assign(winner)
        @job.last_touched_by = dispatcher_user
        @job.save!
        [true, winner]
      else
        log :debug, "not running , auction state:#{@auction.status}, job state #{@job.status}", job: @job.id
        [false, nil]
      end
    end

    # called from assign() in sub classes
    def won(winner)
      log :debug, "won, assigning company and moving to accepted state", job: @job.id
      Auction.slack_notification(@job, "  Winner #{winner.company.name}, ETA:#{winner.eta_mins}")

      company = winner.company
      @job.last_touched_by = dispatcher_user
      @job.assign_rescue_company(winner.candidate&.site || winner.company.hq)
      @job.swoop_auction_won
      Job::Candidate::Assignment.create!(
        job: @job,
        auto_assigned: true,
        chosen_provider: @job.fleet_company.live_rescue_providers.find_by(provider: company),
        chosen_company: company,
        eta: winner.eta_mins,
        status: Job::Candidate::Assignment::ASSIGNED,
        result: :ok
      )
      if winner.eta_mins
        @job.add_time_of_arrival(Time.now + winner.eta_mins.minutes, nil, company)
      end

      if winner.candidate
        Metrics::Service.track(@job,
                               'job',
                               'cost_estimate',
                               winner.candidate.cost)

        Metrics::Service.track(@job,
                               'job',
                               'nps_estimate',
                               winner.candidate.rating)
        Metrics::Service.track(@job,
                               'job',
                               'eta_estimate',
                               winner.candidate.google_eta)
      end
    end

    def dispatcher_user
      @dispatcher_user ||= User.swoop_auto_dispatch
    end

    def close_auction(winner)
      @job.bids.each do |bid|
        if bid == winner
          bid.status = Bid::WON
        else
          if bid.status != Bid::PROVIDER_REJECTED
            if bid.eta_mins
              bid.status = Bid::AUTO_REJECTED
            else
              bid.status = Bid::EXPIRED
            end
          end
        end
        bid.save!
      end

      if winner
        @auction.status = Auction::SUCCESSFUL
      else
        @auction.status = Auction::UNSUCCESSFUL
      end

      if @rp
        @auction.manual_dttm = Time.now
      end

      @auction.save!
    end

    def announce(winner)
      if winner
        company = winner.company
        Job.won_auction_call_if_enabled(company, @job.id, @job.site_or_rescue_company_phone)
      end
    end

  end
end
