# frozen_string_literal: true

require_dependency 'auction/auction'

module Auction
  class ManualAuctionService < EndAuctionService

    def initialize(auction_id, rescue_provider_id)
      @rp = RescueProvider.find(rescue_provider_id)
      super(auction_id)
    end

    private

    def find_winner
      Bid.where(auction: @auction, company: @rp.provider).where.not(eta_mins: nil).first
    end

    def assign(winner)
      if winner
        won(winner)
      else
        company = @rp.provider
        JobService::SwoopAssignRescueProviderService.new(@job, company, @rp.site).call
        Job::Candidate::Assignment.create!(
          job: @job,
          auto_assigned: false,
          chosen_provider: @job.fleet_company.live_rescue_providers.find_by(provider: company),
          chosen_company: company,
          status: Job::Candidate::Assignment::MANUAL_ASSIGNMENT,
          result: :ok
        )
      end
    end

    def close_auction(winner)
      @auction.manual_dttm = Time.now
      super
    end

  end
end
