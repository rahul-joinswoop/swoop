# frozen_string_literal: true

module Auction
  class NoWinnerService

    def initialize(job)
      @job = job
    end

    def call
      @job.swoop_auction_no_winner

      @job.system_alert_swoop_if_necessary(false)

      Job::Candidate::Assignment.create!(
        job: @job,
        auto_assigned: false,
        status: Job::Candidate::Assignment::MANUAL_ASSIGNMENT,
        result: :auction_no_winner
      )
    end

  end
end
