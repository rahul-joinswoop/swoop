# frozen_string_literal: true

module Auction
  module Rankers
    class BidWeightedSum

      def initialize(auction_id,
                     version,
                     constraints = { min_eta: 5, max_eta: 90, min_cost: 20, max_cost: 100, min_rating: 40, max_rating: 100 },
                     weights = { quality: 0.2, cost: 0.6, speed: 0.2 })
        @version = version
        @constraints = constraints.symbolize_keys
        @constraints[:auction_id] = auction_id
        @weights = weights.symbolize_keys
      end

      class V1

        include LoggableJob

        def initialize(constraints, weights)
          @constraints = constraints
          @weights = weights
        end

        def call
          log :debug, "Running WeightedSum with constraints:#{@constraints}, weights:#{@weights}", job: @job_id
          if ::Auction::Bid.where(BID_WHERE_CLAUSE, @constraints).count == 0
            Rails.logger.debug "No Bids found within contraints"
            return
          end
          calc_bid_scores
        end

        private

        BID_WHERE_CLAUSE = "
              auction_id= :auction_id
              and eta_mins <= :max_eta
              and cost <= :max_cost
              and rating >= :min_rating"

        # Calculate scores on the bid table, analagous to the candidate scores, but using the BTA instead of the google_eta on the bid table instead of the condidate table.
        def calc_bid_scores
          sql = "update bids
             set score=
                :quality * (case when (:max_rating_bound - :min_rating_bound)=0 then 1 else round((rating - :min_rating_bound)::numeric / (:max_rating_bound - :min_rating_bound),2) end) +
                :cost * (case when (:max_cost_bound - :min_cost_bound)=0 then 1 else round((:max_cost_bound - cost)::numeric / (:max_cost_bound - :min_cost_bound),2) end) +
                :speed * (case when (:max_eta_bound - :min_eta_bound)=0 then 1 else round((:max_eta_bound - eta_mins)::numeric / (:max_eta_bound - :min_eta_bound),2) end)
           where" + BID_WHERE_CLAUSE
          calc_scores(sql)
        end

        def calc_scores(sql)
          args = get_bounds.merge(@constraints).merge(@weights)
          Rails.logger.debug "Setting Bid scores with #{args}"
          cleaned_sql = BaseModel.sanitize_sql([sql, args])
          BaseModel.connection.execute(cleaned_sql)
        end

        def get_bounds
          sql = "select
             max(eta_mins) as max_eta_bound,
             min(eta_mins) as min_eta_bound,
             max(rating) as max_rating_bound,
             min(rating) as min_rating_bound,
             max(cost) as max_cost_bound,
             min(cost) as min_cost_bound
           from
             bids
           where" + BID_WHERE_CLAUSE
          cleaned_sql = BaseModel.sanitize_sql([
            sql,
            @constraints,
          ])
          results = BaseModel.connection.execute(cleaned_sql)
          results[0].symbolize_keys
        end

      end

      class V2

        include LoggableJob

        def initialize(constraints, weights)
          @constraints = constraints
          @weights = weights
          raise ArgumentError, 'required constraint missing' unless (@constraints.keys & [:min_eta, :max_eta, :min_cost, :max_cost, :min_rating, :max_rating]).length == 6
        end

        def call
          log :debug, "Running WeightedSum with constraints:#{@constraints}, weights:#{@weights}", job: @job_id
          if ::Auction::Bid.where(BID_WHERE_CLAUSE, @constraints).count == 0
            Rails.logger.debug "No Bids found within contraints"
            return
          end
          calc_bid_scores
        end

        private

        BID_WHERE_CLAUSE = "
              auction_id= :auction_id
              and eta_mins <= :max_eta
              and cost <= :max_cost
              and rating >= :min_rating"

        # Calculate scores on the bid table, analagous to the candidate scores, but using the BTA instead of the google_eta on the bid table instead of the condidate table.
        def calc_bid_scores
          sql = "update bids
             set score=
                :quality * (case when (:max_rating_bound - :min_rating_bound)=0 then 1 else round((rating - :min_rating_bound)::numeric / (:max_rating_bound - :min_rating_bound),2) end) +
                :cost * (case when (:max_cost_bound - :min_cost_bound)=0 then 1 else round((:max_cost_bound - GREATEST(cost,:min_cost_bound))::numeric / (:max_cost_bound - :min_cost_bound),2) end) +
                :speed * (case when (:max_eta_bound - :min_eta_bound)=0 then 1 else round((:max_eta_bound - GREATEST(eta_mins,:min_eta_bound))::numeric / (:max_eta_bound - :min_eta_bound),2) end)
           where" + BID_WHERE_CLAUSE
          calc_scores(sql)
        end

        def calc_scores(sql)
          args = get_bounds.merge(@constraints).merge(@weights)
          Rails.logger.debug "Setting Bid scores with #{args}"
          cleaned_sql = BaseModel.sanitize_sql([sql, args])
          BaseModel.connection.execute(cleaned_sql)
        end

        def get_bounds
          {
            max_eta_bound: @constraints[:max_eta],
            min_eta_bound: @constraints[:min_eta],
            max_rating_bound: @constraints[:max_rating],
            min_rating_bound: @constraints[:min_rating],
            max_cost_bound: @constraints[:max_cost],
            min_cost_bound: @constraints[:min_cost],
          }
        end

      end

      VERSIONS = {
        1 => V1,
        2 => V2,
      }.freeze

      def call
        VERSIONS[@version].new(@constraints, @weights).call
      end

    end
  end
end
