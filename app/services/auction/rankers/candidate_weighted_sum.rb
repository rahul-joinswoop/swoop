# frozen_string_literal: true

module Auction
  module Rankers
    class CandidateWeightedSum

      def initialize(job_id,
                     version,
                     constraints = { min_eta: 5, max_eta: 90, min_cost: 20, max_cost: 100, min_rating: 40, max_rating: 100 },
                     weights = { quality: 0.2, cost: 0.6, speed: 0.2 })
        @version = version
        @constraints = constraints.symbolize_keys
        @constraints[:job_id] = job_id
        @weights = weights.symbolize_keys
      end

      def call
        VERSIONS[@version].new(@constraints, @weights).call
      end

      class Base

        include LoggableJob

        CANDIDATE_WHERE_CLAUSE = "
              job_id= :job_id
              and google_eta <= :max_eta
              and cost <= :max_cost
              and rating >= :min_rating
              and vehicle_eligible='eligible'"

        def call
          log :debug, "Running WeightedSum with constraints:#{@constraints}, weights:#{@weights}", job: @job_id

          if Job::Candidate.where(CANDIDATE_WHERE_CLAUSE, @constraints).count == 0
            Rails.logger.debug "No AutoassignCandidates found within contraints"
            return
          end
          calc_candidate_scores
        end

        def calc_scores(sql)
          args = get_bounds.merge(@constraints).merge(@weights)
          Rails.logger.debug "Setting scores with #{args}"
          cleaned_sql = BaseModel.sanitize_sql([sql, args])
          BaseModel.connection.execute(cleaned_sql)
        end

      end

      class V1 < Base

        def initialize(constraints, weights)
          @constraints = constraints
          @weights = weights
        end

        private

        # Candidate Scores

        def calc_candidate_scores
          sql = "update auto_assign_candidates
             set score=
                :quality * (case when (:max_rating_bound - :min_rating_bound)=0 then 1 else round((rating - :min_rating_bound)::numeric / (:max_rating_bound - :min_rating_bound),2) end) +
                :cost * (case when (:max_cost_bound - :min_cost_bound)=0 then 1 else round((:max_cost_bound - cost)::numeric / (:max_cost_bound - :min_cost_bound),2) end) +
                :speed * (case when (:max_eta_bound - :min_eta_bound)=0 then 1 else round((:max_eta_bound - google_eta)::numeric / (:max_eta_bound - :min_eta_bound),2) end)
           where" + Base::CANDIDATE_WHERE_CLAUSE
          calc_scores(sql)
        end

        def get_bounds
          sql = "select
             max(google_eta) as max_eta_bound,
             min(google_eta) as min_eta_bound,
             max(rating) as max_rating_bound,
             min(rating) as min_rating_bound,
             max(cost) as max_cost_bound,
             min(cost) as min_cost_bound
           from
             auto_assign_candidates
           where" + Base::CANDIDATE_WHERE_CLAUSE
          cleaned_sql = BaseModel.sanitize_sql([
            sql,
            @constraints,
          ])
          results = BaseModel.connection.execute(cleaned_sql)
          results[0].symbolize_keys
        end

      end

      class V2 < Base

        def initialize(constraints, weights)
          @constraints = constraints
          @weights = weights
          raise ArgumentError, 'required constraint missing' unless (@constraints.keys & [:min_eta, :max_eta, :min_cost, :max_cost, :min_rating, :max_rating]).length == 6
        end

        private

        # Candidate Scores

        def calc_candidate_scores
          sql = "update auto_assign_candidates
             set score=
                :quality * (case when (:max_rating_bound - :min_rating_bound)=0 then 1 else round((rating - :min_rating_bound)::numeric / (:max_rating_bound - :min_rating_bound),2) end) +
                :cost * (case when (:max_cost_bound - :min_cost_bound)=0 then 1 else round((:max_cost_bound - GREATEST(cost,:min_cost_bound))::numeric / (:max_cost_bound - :min_cost_bound),2) end) +
                :speed * (case when (:max_eta_bound - :min_eta_bound)=0 then 1 else round((:max_eta_bound - GREATEST(google_eta,:min_eta_bound))::numeric / (:max_eta_bound - :min_eta_bound),2) end)
           where" + Base::CANDIDATE_WHERE_CLAUSE
          calc_scores(sql)
        end

        def get_bounds
          {
            max_eta_bound: @constraints[:max_eta],
            min_eta_bound: @constraints[:min_eta],
            max_rating_bound: @constraints[:max_rating],
            min_rating_bound: @constraints[:min_rating],
            max_cost_bound: @constraints[:max_cost],
            min_cost_bound: @constraints[:min_cost],
          }
        end

      end

      VERSIONS = {
        1 => V1,
        2 => V2,
      }.freeze

    end
  end
end
