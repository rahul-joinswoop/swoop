# frozen_string_literal: true

require_dependency 'auction/auction'

#
# START AUCTION SERVICE
#
# The StartAuctionService kicks off a new auction for a job. It:
# - Makes a new Auction record
# - Finds tow trucks and tow sites in radius, and makes
#   empty bids they can populate
# - Lets the eligible tow trucks and tow sites know they
#   can fill in these empty bids
# - Starts the countdown to end the auction
#
# And, if, at any stage, the auction can't be built, we halt it
#
module Auction

  class StartAuctionServiceLeftAuctionInUnknownStateError < StandardError

    def initialize(job_id:, auction:)
      super
    end

  end

  class StartAuctionService < Service

    include LoggableJob

    def initialize(job)
      raise ArgumentError, "StartAuctionService job must be a Job" unless job.is_a?(Job)
      @job = job

      log :debug, "job status: #{@job.status}", job: @job.id
      raise ArgumentError, "StartAuctionService Can't Auction a scheduled Job" if @job.scheduled_for
      raise ArgumentError, "StartAuctionService job has no service location" unless @job.service_location
      raise ArgumentError, "StartAuctionService job.fleet_company is missing" if @job.fleet_company.blank?
      raise ArgumentError, "StartAuctionService User.swoop_auto_dispatch is missing" if User.swoop_auto_dispatch.blank?
    end

    def call
      @auction = nil

      optional_transaction_around_auction do
        log :debug, "call starting'", job: @job.id

        # Step 1: Make a new auction for the job
        @auction = Auction.build_for_job(job: @job)
        @auction.save!

        # Step 2.1: Figure out all the towers we want
        # to bid, and generate placeholder bids they can bid on...
        begin
          # Generate bids for the auction
          GenerateBiddableBidsService.new(job: @job, auction: @auction).call
          # Set the auction to live
          @auction.status = Auction::LIVE
        # Step 2.2: ...or, if bids can't be created, halt the auction
        rescue GenerateBiddableBidsError => e
          # Update the auction result and auction status if bids couldn't
          # be created
          case e
          when CannotGenerateBidsForFleetCompanyWithoutAutomaticDispatchError
            @auction.result = :bidding_not_enabled
          when CannotGenerateBidsBecauseNoCandidatesInRangeError
            @auction.result = :no_live_trucks_or_sites_in_range
          when CannotGenerateBidsBecauseNoCandidatesHaveScoresError
            @auction.result = :no_elgible_sites_or_vehicles_in_eta
          end
          @auction.status = Auction::UNSUCCESSFUL
        end

        # Step 3: Save the auction and job
        @auction.save!
        @job.save!
        log :debug, "call finished", job: @job.id
      end

      # Step 4: With the auction record created + persisted, dispatch work to
      # Sidekiq and services depending on whether it was successfully started or not
      case @auction&.status
      when Auction::LIVE
        # Notify the companies that they have new biddable bids to bid on
        if @auction.duration_secs > 0
          @auction.bids.each do |bid|
            log :debug, "Calling BidAlertWorker with #{bid.inspect}", job: @job.id
            BidAlertWorker.perform_async(bid.id)
            BidAlertWorker.perform_in(1.minute, bid.id)
            BidAlertWorker.perform_in(2.minutes, bid.id)
          end
        else
          log :debug, "not notifying because of zero auction duration: #{@auction.duration_secs}", job: @job.id
        end
        # Start an asynchronous countdown to end the auction
        log :debug, "Calling ExpireAuctionTimer triggering at: #{@auction.expires_at}", job: @job.id
        ExpireAuctionTimer.perform_at(@auction.expires_at, @auction.id)
      when Auction::UNSUCCESSFUL
        log :debug, "calling NoWinnerService", job: @job.id
        NoWinnerService.new(@job).call
        @job.save!
      else
        raise StartAuctionServiceLeftAuctionInUnknownStateError, @job.id, @auction
      end
    end # def call

    private

    # TODO: Remove this method and dependencies once testing for atomicity is done.
    def optional_transaction_around_auction
      atomically_generate_biddable_bids = External::Split.treatment(@job, 'atomically_generate_biddable_bids', 'on')

      if atomically_generate_biddable_bids == 'on'
        ApplicationRecord.transaction do
          yield
        end # ApplicationRecord.transaction
      else
        yield
      end
    end

  end

end
