# frozen_string_literal: true

module Auction
  # This service is called once a day to populate the ratings table from the reviews table
  # pr=Auction::PopulateRatings.new(crs.company.id, crs.settings)
  class PopulateRatings

    def initialize(client_id,
                   settings)
      @client_id = client_id
      @settings = settings.with_indifferent_access
    end

    def nps
      sql = "insert into ratings(rescue_company_id,client_company_id,num_reviews,num_jobs,score,updated_at,created_at,date)
select
jobs.rescue_company_id,
:client_id as client_company_id,
sum(case when nps_question1 is not null then 1 else 0 end) as num_reviews,
count(*) as num_jobs,
case when (COALESCE(SUM((case when nps_question1 is not null then 1 else 0 end) ), 0))=0
         then null
         else 100.0*((COALESCE(SUM((case when nps_question1 > 8 then 1 else 0 end) ), 0)) - (COALESCE(SUM((case when nps_question1 < 7 then 1 else 0 end) ), 0)))/(COALESCE(SUM((case when nps_question1 is not null then 1 else 0 end) ), 0))
         end AS score,
now() as updated_at,
now() as created_at,
current_date as date
 from reviews left join jobs on reviews.job_id=jobs.id where company_id=:client_id
 group by jobs.rescue_company_id
 having sum(case when nps_question1 is not null then 1 else 0 end)>:min_num_jobs
order by score desc,sum(case when nps_question1 is not null then 1 else 0 end) desc
ON CONFLICT DO NOTHING"

      args = {
        client_id: @client_id,
        min_num_jobs: @settings[:min_num_jobs],
      }

      Rails.logger.debug "Running #{sql} with #{args}"

      cleaned_sql = BaseModel.sanitize_sql([
        sql,
        args,
      ])
      BaseModel.connection.execute(cleaned_sql)
    end

    def call
      type = @settings[:type].is_a?(String) ? @settings[:type].intern : @settings[:type]
      case type
      when :nps
        nps
      else
        raise ArgumentError, "Unknown type passed to PopulateRatings: #{@type}"
      end
    end

  end
end
