# frozen_string_literal: true

#
# UPDATE INVOICE FOR IN MEMORY
#
# Generate a hypothetical invoice variation based on a real, persisted
# invoice, using the abstract invoice updating of UpdateInvoiceService
#
# InvoiceService::BuildSample uses this class.
#
class UpdateInvoiceForJobInMemory < UpdateInvoiceService

  #
  # If this invoice isn't in a new state, we can't update it
  #
  def invoice_can_be_updated
    @invoice.new_state?
  end

  #
  # We don't want to bump the storage tick for an invoice in memory,
  # so we don't implement it
  #
  def bump_storage_tick
  end

  #
  # We don't want to delete any line items for an invoice in memory,
  # so we don't implement it
  #
  def delete_existing_lineitems
  end

  #
  # We don't want to apply billing infomation for an invoice in memory,
  # so we don't implement it
  #
  def apply_job_billing_info
  end

  #
  # When updating an invoice for a job, the job is used to
  # generate rates and line items
  #
  def invoicable
    @job
  end

  #
  # The Account that will be used for the default vehicle rate
  # additions
  #
  def account_for_default_vehicle_rate
    if @invoice.sender.is_a?(FleetCompany)
      if @invoice.recipient.is_a?(User)
        @job.fleet_company.customer_account
      else
        @invoice.recipient
      end
    else
      @job.account
    end
  end

end
