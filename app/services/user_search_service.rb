# frozen_string_literal: true

class UserSearchService < SearchService

  class << self

    def model_class
      User
    end

    def power_searchable_fields
      @power_searchable_fields ||= [
        [:company_id, :integer, :"company.id"],
        [:company_name, :string, :"company.name"],
        [:email, :string, :email],
        [:first_name, :string, :first_name],
        [:id, :integer, :id],
        [:last_name, :string, :last_name],
        [:phone, :phone, :phone],
        [:user, :integer, :id], # TODO this is a duplicate of :id
        [:username, :string, :username],
        # TODO it's apparently applying an AND clause instead of OR on first_name and last_name
        # follow up ticket: https://swoopme.atlassian.net/browse/ENG-8516
        [:fullname, :string, [:first_name, :last_name]],
      ].freeze
    end

    def filterable_fields
      @filterable_fields ||= [[:company_id, :"company.id"]].freeze
    end

    def default_sort_order
      { sort_name: :asc }
    end

  end

  def general_search_skip_fields
    [:first_name, :last_name]
  end

end
