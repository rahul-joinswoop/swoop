# frozen_string_literal: true

module InvoicingLineItemService
  # Abstractclass subclasses should implement:
  #
  # #description
  # #free_description
  class BuildAttributes

    attr_reader :company, :disable_rounding

    CALCULATED_MSG = "Calculated %s with distance:%s -> quantity:%s, price:%s, net:%s"

    def initialize(distance_in_miles, free_distance_in_miles, company, per_mile, disable_rounding)
      @distance_in_miles = distance_in_miles || 0
      @free_distance_in_miles = free_distance_in_miles
      @per_mile = per_mile
      @company = company
      @disable_rounding = disable_rounding
    end

    def call
      li = build_line_item_hash(description, rounded_distance, per_unit_price)
      free_li = build_line_item_hash(free_description, free, -per_unit_price)
      [li, free_li]
    end

    private

    def free
      free_distance > rounded_distance ? rounded_distance : free_distance
    end

    def rounded_distance
      return 0 unless distance

      if disable_rounding
        distance
      else
        company.round_distance(distance)
      end
    end

    def metric_distance?
      company.metric_distance_units?
    end

    def build_line_item_hash(description, distance, price)
      if price.nil?
        price = 0
      else
        # This is a hack because we are converting km to mi on the FE.
        # We don't want to show unit_price 0.0999786 if they originally entered 0.10
        # 0.0001 is amount that can be justified by rounding due to our column configuration
        # precision: 20 scale: 4
        price = price.round(2)
      end

      # we round to tenths unless it's a free line item per spec
      significant_digits = price.positive? ? 1 : 2
      tow_quantity = distance.to_d.round(significant_digits)
      distance_net = (tow_quantity * price).round(2)

      Rails.logger.debug CALCULATED_MSG % [description, distance, tow_quantity, price, distance_net]

      {
        description: description,
        net_amount: distance_net,
        tax_amount: 0,
        estimated: true,
        unit_price: price,
        original_unit_price: price,
        quantity: tow_quantity,
        original_quantity: tow_quantity,
        version: Invoice::VERSION,
      }
    end

    def mi_in_km
      1.0 / Geocoder::Calculations::KM_IN_MI
    end

    def per_unit_price
      (metric_distance? ? @per_mile / mi_in_km : @per_mile)
    end

    def distance
      metric_distance? ? (@distance_in_miles * mi_in_km) : @distance_in_miles
    end

    def free_distance
      metric_distance? ? (@free_distance_in_miles * mi_in_km) : @free_distance_in_miles
    end

  end
end
