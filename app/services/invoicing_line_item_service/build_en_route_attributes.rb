# frozen_string_literal: true

module InvoicingLineItemService
  class BuildEnRouteAttributes < BuildAttributes

    def description
      metric_distance? ? Rate::EN_ROUTE_KILOMETERS : Rate::EN_ROUTE_MILES
    end

    def free_description
      metric_distance? ? Rate::FREE_EN_ROUTE_KILOMETERS : Rate::FREE_EN_ROUTE_MILES
    end

  end
end
