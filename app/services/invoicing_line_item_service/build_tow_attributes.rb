# frozen_string_literal: true

module InvoicingLineItemService
  class BuildTowAttributes < BuildAttributes

    def description
      metric_distance? ? Rate::KILOMETERS_TOWED : Rate::MILES_TOWED
    end

    def free_description
      metric_distance? ? Rate::FREE_KILOMETERS_TOWED : Rate::FREE_MILES_TOWED
    end

  end
end
