# frozen_string_literal: true

module InvoicingLineItemService
  class BuildDeadheadAttributes < BuildAttributes

    def description
      metric_distance? ? Rate::KILOMETERS_DEADHEAD : Rate::MILES_DEADHEAD
    end

    def free_description
      metric_distance? ? Rate::FREE_KILOMETERS_DEADHEAD : Rate::FREE_MILES_DEADHEAD
    end

  end
end
