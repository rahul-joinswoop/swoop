# frozen_string_literal: true

##
# This module allows composing different services together as part of a macro
# service, i.e. a service that uses other services to operate changes or
# actions.
#
# It requires a SwoopService.
#
# As services implement #execute for state changes and #after_execution for
# async code that needs to be run when state is persisted, when using other
# services inside #execute we need to keep track of those services so we know
# when/if to run #after_execution.
#
# Example usage:
#
# class UpperCaseNameService
#   def initialize(company:)
#     @company = company
#   end
#
#   def execute
#     @company.name.upcase!
#     @company
#   end
#
#   def after_execution; end
# end
#
# class CompleteAddressFieldsService
#   def initialize(company:, post_code:)
#     @company = company
#     @post_code = post_code
#   end
#
#   def execute
#     @company.address = AddressGateway.query(post_code)
#   end
#
#   def after_execution
#     SendErrorEmailToAdminJob.new(@company) unless @company.address
#   end
# end
#
# class NewCompanyService
#   include SwoopServiceComposite
#
#   def initialize(company_attributes:)
#     @company_attributes = company_attributes
#   end
#
#   def execute
#     @company = Company.new(@company_attributes)
#     find_company_address
#     upcase_company_name
#   end
#
#   def after_execution
#     SendEmailToOwnerJob.new(@company)
#     after_execution_child_services
#   end
#
#   private
#
#   def find_company_address
#     add_and_execute_child_service(CompleteAddressFieldsService.new(
#       company: @company, post_code: @company_attributes[:zip]
#     ))
#   end
#
#   def upcase_company_name
#     name_service = UpperCaseNameService.new(company: @company)
#     add_child_service(name_service)
#     name_service.execute
#   end
# end
module SwoopServiceComposite

  def child_services
    @child_services ||= []
  end

  def add_child_service(service)
    child_services << service
  end

  def execute_child_services
    child_services.each(&:execute)
  end

  def after_execution_child_services
    child_services.each(&:after_execution)
  end

  def execute
    execute_child_services
  end

  def after_execution
    after_execution_child_services
  end

  def add_and_execute_child_service(service)
    add_child_service(service)
    service.execute
  end

end
