# frozen_string_literal: true

class IsscAutoAccept

  include LoggableJob

  def initialize(job)
    @job = job
  end

  def call
    log :debug, "IsscAutoAccept ignoring", job: @job.id
  end

end
