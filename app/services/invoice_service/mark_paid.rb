# frozen_string_literal: true

# # #
# Service to Mark an Invoice as Paid.
#
# It also adds a new Payment to clear the balance in case it's > than 0.
#
# It needs an invoice_id, and it can be either Partner Invoice id or Swoop Invoice id.
module InvoiceService
  class MarkPaid < Service

    attr_reader :invoice

    def initialize(invoice_id:, api_company_id:, paid: true)
      super()

      raise MissingInvoiceError if invoice_id.blank?

      raise ArgumentError, "InvoiceService::MarkPaid api_company_id is blank" if api_company_id.blank?

      @invoice_id = invoice_id
      @api_company_id = api_company_id
      @paid = paid

      Rails.logger.debug("INVOICE(#{@invoice_id}) InvoiceService::MarkPaid initialized with @api_company_id=#{@api_company_id}, @paid=#{@paid}")
    end

    def call
      @invoice = Invoice.find_by!(id: @invoice_id, sender_id: @api_company_id)

      if @paid
        @invoice.paid = true

        Rails.logger.debug("INVOICE(#{@invoice_id}) InvoiceService::MarkPaid @invoice.paid set to #{@paid}")

        clear_invoice_balance_if_not_zero
      else
        @invoice.reset_paid
      end

      @mutated_objects.push @invoice

      self
    end

    def save_mutated_objects
      ApplicationRecord.transaction do
        super
      end
    end

    private

    def clear_invoice_balance_if_not_zero
      balance = @invoice.balance

      if balance > 0
        Rails.logger.debug("INVOICE(#{@invoice_id}) InvoiceService::MarkPaid @invoice.paid about to " \
          "create a new Payment to clear the invoice balance")

        payment = @invoice.new_payment_or_credit(Payment, {
          total_amount: -balance,
          description: "Marked Paid",
          payment_method: nil,
          mark_paid_at: Time.now,
        })

        @mutated_objects.push payment
      end
    end

  end
end
