# frozen_string_literal: true

module InvoiceService
  class ChargeCard

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "InvoiceService::Charge::Invoice called with #{context.input}"

      @company_id = context.input[:company_id]
      @user_id = context.input[:user_id]
      @invoice_id = context.input[:invoice_id]
      @invoice_charge_attributes = context.input[:invoice_charge_attributes]

      if @company_id.blank?
        raise ArgumentError, "InvoiceService::Charge::Invoice @company_id is blank"
      end

      if @user_id.blank?
        raise ArgumentError, "InvoiceService::Charge::Invoice @user_id is blank"
      end

      if @invoice_id.blank?
        raise ArgumentError, "InvoiceService::Charge::Invoice @invoice_id is blank"
      end

      if @invoice_charge_attributes.blank?
        raise ArgumentError, "InvoiceService::Charge::Invoice @invoice_id is blank"
      end
    end

    def call
      context.output = create_api_async_request!

      schedule_async_charge_worker
    end

    private

    def invoice
      @invoice ||= Invoice.find_by!(
        id: @invoice_id,
        sender_id: @company_id,
        deleted_at: nil
      )
    end

    # we create the async_quest for auditing and tracking the flow
    def create_api_async_request!
      @api_async_request = API::AsyncRequest.create!(
        company_id: @company_id,
        user_id: @user_id,
        request: @invoice_charge_attributes,
        target_id: @invoice_id,
        system: API::AsyncRequest::INVOICE_CHARGE_CARD
      )
    end

    # This is the only method in the ChargeCardService
    # where Stripe is mentioned, because here
    # we need to trigger the worker using a
    # specific implementation.
    def schedule_async_charge_worker
      Stripe::ChargeWorker.perform_async(
        @api_async_request.id,
        @invoice_charge_attributes,
        @invoice_id
      )
    end

  end
end
