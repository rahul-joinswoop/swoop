# frozen_string_literal: true

# This creates a new invoice between a fleet client and their customer
# It's generated from the fleet customer rate
module InvoiceService
  class CreateOrUpdateFleetCustomerInvoiceIfNecessary

    include LoggableJob

    def initialize(job, recipient)
      @job = job
      @recipient = recipient
      log :debug, "CreateOrUpdateFleetCustomerInvoiceIfNecessary initialized", job: @job.id
    end

    def call
      return unless @job.fleet_company&.customer_account

      return unless @job.service_code

      log :debug, "CreateOrUpdateFleetCustomerInvoiceIfNecessary calculating customer invoice", job: @job.id

      account = @job.fleet_company.customer_account

      fleet_customer_rate = Rate.inherit(
        @job.fleet_company_id,
        account,
        @job.site_id,
        nil,
        @job.service_code_id,
        @job.invoice_vehicle_category_id
      )

      invoice = @job.fleet_customer_live_invoices.find_by(recipient: @recipient)

      if !invoice
        invoice = Invoice.new(
          sender: @job.fleet_company,
          recipient: @recipient,
          currency: @job.fleet_company&.currency,
          job: @job
        )
        invoice.fleet_customer_new
      end

      UpdateInvoiceForJob.new(
        invoice,
        @job,
        fleet_customer_rate
      ).call
      invoice.save!
    end

  end
end
