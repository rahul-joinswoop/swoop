# frozen_string_literal: true

# Service to Move an Invoice back to new by Partner.
module InvoiceService
  class PartnerApproved < Service

    def initialize(invoice_id:, api_company_id: nil, api_user_id: nil)
      super()

      if invoice_id.blank?
        raise MissingInvoiceError
      end

      @partner_invoice_id = invoice_id
      @mark_paid = nil
    end

    def call
      @partner_invoice = Invoice.find_by_id(@partner_invoice_id)

      @partner_invoice.partner_approved

      if @partner_invoice.on_site_payment?
        @mark_paid = MarkPaid.new(invoice_id: @partner_invoice_id, api_company_id: @partner_invoice.sender_id)
        @mark_paid.call

        create_new_swoop_invoice_if_needed
      end

      @mutated_objects.push @partner_invoice

      self
    end

    def save_mutated_objects
      ApplicationRecord.transaction do
        super
      end
      @mark_paid.save_mutated_objects if @mark_paid
    end

    private

    def create_new_swoop_invoice_if_needed
      swoop_invoice = InvoiceService::NewSwoopInvoice.new(
        partner_invoice_id: @partner_invoice_id
      ).call.swoop_invoice

      @mutated_objects.push swoop_invoice if swoop_invoice.present?
    end

  end
end
