# frozen_string_literal: true

# Service to undo approval of Partner Invoice by Swoop.
#
# Business rule: it also softly deletes the active FleetManaged Invoice
module InvoiceService
  class SwoopUndoApprovalPartner < Service

    def initialize(invoice_id:, api_company_id: nil, api_user_id: nil)
      super()

      if invoice_id.blank?
        raise MissingInvoiceError
      end

      @partner_invoice_id = invoice_id
    end

    def call
      @partner_invoice = Invoice.find_by_id(@partner_invoice_id)
      raise InvoiceStateTransitionError if !@partner_invoice.may_swoop_undo_approval_partner?

      @partner_invoice.swoop_undo_approval_partner
      @mutated_objects.push @partner_invoice

      flag_fleet_managed_invoice_to_be_deleted

      self
    end

    def save_mutated_objects
      ApplicationRecord.transaction do
        super
      end
    end

    private

    def flag_fleet_managed_invoice_to_be_deleted
      fleet_managed_invoice = @partner_invoice.find_active_fleet_managed_invoice

      raise MissingFleetManagedInvoiceError if fleet_managed_invoice.blank?
      raise InvoiceStateTransitionError if !fleet_managed_invoice.may_swoop_delete_fleet?

      fleet_managed_invoice.swoop_delete_fleet

      @mutated_objects.push fleet_managed_invoice
    end

  end
end
