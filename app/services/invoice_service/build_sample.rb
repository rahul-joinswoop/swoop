# frozen_string_literal: true

# This service takes an original_invoice and builds a dupe based on it that we call sample_invoice.
#
# It does not persist the sample invoice. The main purpose for this service is to be able to recalculate
# the invoice for UI users while they're editing it, without persisting the changes.
#
# It accepts values for RateType, VehicleCategory and InvoiceLineItem hashes to replace them
# in the sample_invoice.
#
# It also receives a special hash called edit_invoice_metadata, and this service expects two keys in this hash:
#
# {
#   previous_rate_type: nil
#   previous_vehicle_category: nil
# }
#
# It can also be an empty hash: {}.
#
# These values are created by this service, and indicates the previous rate_type and previous vehicle category set by the last build_sample iteration
# to the sample invoice. It's used to check if user in the UI has changed the original rate to a different one,
# but then decided to set it back to the original again, in which case we just set back the original line items
# and keep the user_created ones that exist in memory in the new sample invoice.
#
# The sample_invoice will have its total_amount and balance recalculated based on the values passed.
# All line_items will have their net_amount updated accordingly as well.
#
# It uses UpdateInvoiceForJobInMemory for it, and all the logic added here is required to correctly
# calculate the sample_invoice.
#
# It's maily inspired in web Edit Invoice flow: FE logic and BE logic (@see invoices_controller/samplerate)
#
# params:
#
#   - original_invoice (required): an existent invoice
#   - rate_type (required): an instance of RateType model, used for Rate calculation
#   - vehicle_category (optional): an instance of VehicleCategory model, used for Rate calculation
#   - line_items_hashes (required): an array of line_items hashes to be merged to the sample_invoice.line_items
#   - edit_invoice_metadata (required): a hash with previous_rate_type and previous_vehicle_category values
#
# Usage:
#
#    InvoiceService::BuildSample.new(
#      original_invoice: invoice,
#      rate_type: RateType.find_by_type('Flat'),
#      vehicle_category: nil,
#      line_items_hashes: [{
#        description: "Flat Rate",
#        estimated: true,
#        net_amount: BigDecimal("10.0"),
#        original_quantity: BigDecimal("1.0"),
#        original_unit_price: BigDecimal("10.0"),
#        quantity: BigDecimal("1.0"),
#        tax_amount: BigDecimal("0.0"),
#        unit_price: BigDecimal("10.0"),
#        user_created: false,
#        rate_id: rate.id,
#      }]
#    ).call.sample_invoice
#
module InvoiceService
  class BuildSample

    class OriginalInvoiceRequiredError < StandardError; end
    class OriginalInvoiceIsNotAnInvoiceInstanceError < StandardError; end
    class NewRateTypeRequiredError < StandardError; end
    class NewRateTypeIsNotARateTypeInstanceError < StandardError; end
    class LineItemsHashesNilOrEmptyError < StandardError; end

    attr_reader :sample_invoice

    def initialize(original_invoice:, rate_type:, vehicle_category: nil, line_items_hashes: nil, edit_invoice_metadata:)
      @original_invoice = original_invoice # required

      raise OriginalInvoiceRequiredError if @original_invoice.blank?
      raise OriginalInvoiceIsNotAnInvoiceInstanceError if !@original_invoice.is_a? Invoice

      @rate_type = rate_type # required

      raise NewRateTypeRequiredError if @rate_type.blank?
      raise NewRateTypeIsNotARateTypeInstanceError if !@rate_type.is_a? RateType

      @vehicle_category = vehicle_category

      # check if it's blank, or if all its hashes are blank and raises if true
      raise LineItemsHashesNilOrEmptyError if line_items_hashes.blank? || line_items_hashes.map(&:blank?).first
      @line_items_hashes = line_items_hashes.map(&:symbolize_keys) # required

      @previous_rate_type = edit_invoice_metadata.dig(:previous_rate_type)
      @previous_vehicle_category = edit_invoice_metadata.dig(:previous_vehicle_category)
    end

    def call
      # step 1 - dup the invoice, with some custom attributes
      @sample_invoice = @original_invoice.dup.tap do |invoice|
        invoice.created_at = nil
        invoice.deleted_at = nil
        invoice.updated_at = nil
        invoice.uuid = nil
        invoice.rate_type = @rate_type.type
        invoice.job.invoice_vehicle_category = @vehicle_category
        invoice.edit_invoice_metadata = {
          previous_rate_type: @rate_type,
          previous_vehicle_category: @vehicle_category,
        }
      end

      if Rails.env.development?
        Rails.logger.debug("BuildSample @original_invoice.rate_type #{@original_invoice.rate_type} and @rate_type.type #{@rate_type&.type}")
        Rails.logger.debug("BuildSample @previous_vehicle_category #{@previous_vehicle_category} and @previous_rate_type #{@previous_rate_type&.type}")
      end

      # step 2 - check if the rate_type is being set back to the same one set in the original invoice
      rate_type_changed_back_to_the_original_one =
        @previous_rate_type &&
        (@previous_rate_type != @rate_type || @previous_vehicle_category != @vehicle_category) &&
        @original_invoice.rate_type == @rate_type.type

      # step 2 check - if the rate_type has been set back to the same one set in the original invoice,
      #                we build the original line items in the sample, and build the ones from @line_items_hashes
      #                that are user_created, not persisted and not deleted. Then jump straight to step 10.
      if rate_type_changed_back_to_the_original_one
        Rails.logger.debug("BuildSample - Invoice(#{@original_invoice.id}) Rate changed back to the original one")

        @sample_invoice.line_items = @original_invoice.line_items
        @sample_invoice.line_items.build(@line_items_hashes.select { |li| li[:id].nil? && li[:user_created] && li[:deleted_at].nil? })
      else # step 2 check - otherwise, run a more complex logic to build the new line items into the sample

        # step 3 - inherit the rate based on sample_invoice attributes
        rate = Rate.inherit_type(
          @sample_invoice.sender_id,
          @sample_invoice.job.account_id,
          @sample_invoice.job.site_id,
          @sample_invoice.job.issc_contractor_id,
          @sample_invoice.job.service_code_id,
          @sample_invoice.job.invoice_vehicle_category,
          @sample_invoice.rate_type
        )

        # step 4 - build items_to_be_softly_deleted from @line_items_hashes, and mark them as deleted_at if:
        #
        #  - it's has a different rate_id than the rate.id found on step 3 ^
        #  - it's not user_created; and
        #  - it's not Storage;
        #
        # This covers the case when the rate is changed by the user, which means all
        # line_items that were previously built by the previous rate should be softly deleted
        # as per business rule.
        #
        # We add them back in step 9 only, as UpdateInvoiceForJobInMemory internals don't know how to
        # treat deleted_at items.
        #
        # tl;dr:
        #
        #  - when an item from @line_items_hashes is Storage:
        #      Storage LineItems are always built by a specific StorateRate during Invoice creation,
        #      despite the Invoice RateType. It's a special case that must be treated separately
        #      from RateType changes this Invoice may have, since no other RateType can
        #      built Storate LineItems. So we must always keep it here.
        #
        #  - when an item from @line_items_hashes is user_created:
        #     It means the item was manually added by the user in the UI,
        #     and it does not have an associated rate. Therefore this item should always be kept as per business rule.
        #
        # note: in current web version, the current line_items from 'Rate A' are not passed
        # to samplerate REST route when user changes it to 'Rate B', and this is why this treatment is not needed there.
        #
        items_to_be_softly_deleted = @line_items_hashes.select do |li|
          li[:rate_id] != rate.id && !li[:user_created] && li[:description] != 'Storage' && li[:rate_id]
        end

        items_to_be_softly_deleted.each { |li| li[:deleted_at] = Time.now }

        # step 5 - build items_allowed_to_be_edited by substracting items_to_be_softly_deleted from @line_items_hashes
        items_allowed_to_be_edited = @line_items_hashes - items_to_be_softly_deleted

        # step 6 - make sure we update li.net_amount and that li.job_id is correctly set with invoice.job_id for all @sample_invoice.line_items
        @sample_invoice.line_items.build(items_allowed_to_be_edited).each do |li|
          li.job_id = @sample_invoice.job_id
        end

        # step 7 - build new LineItem instances from the line items that are allowed to be edited
        UpdateInvoiceForJobInMemory.new(@sample_invoice, @sample_invoice.job, rate).call

        # step 8 - build a Storage line_item if job.storage exists and
        #          @sample_invoice does not have it yet
        #
        # There's one case that will cause the following 'if block' to be true:
        #
        # - the invoice has a Storage Item
        # - on a second step, user deletes that Storage item
        # - on a third step, the user changes the invoice rate
        #
        # In this case, it will cause new line items for the new rate to be added,
        # and as per business rule, if job.storage exists,
        # we always guarantee that there's a Storage line_item built as well.
        #
        storage_item = @sample_invoice.line_items.find { |li| li.description == 'Storage' }

        if !storage_item && @sample_invoice.job.storage
          storage_rate = Rate.inherit_type(
            @sample_invoice.sender_id,
            @sample_invoice.job.account_id,
            @sample_invoice.job.site_id,
            @sample_invoice.job.issc_contractor_id,
            @sample_invoice.job.service_code_id,
            @sample_invoice.job.invoice_vehicle_category,
            "StorageRate"
          )

          UpdateInvoiceForJobInMemory.new(@sample_invoice, @sample_invoice.job, storage_rate).call
        end

        # step 9 - build items_to_be_softly_deleted into @sample_invoice; we keep those so if user
        #          decides to persist the invoice changes, we can update them as deleted_at in the db
        @sample_invoice.line_items.build(items_to_be_softly_deleted)
      end

      # step 10 - reject all line items that are not persisted but are already deleted in the UI, as we don't care about them
      @sample_invoice.line_items = @sample_invoice.line_items.reject { |li| li[:id].nil? && li[:deleted_at].present? }

      if Rails.env.development?
        @sample_invoice.line_items.each do |li|
          Rails.logger.debug("BuildSample built line_items estimated #{li.estimated}, li.user_created #{li.user_created}, deleted_at #{li.deleted_at}, id #{li.id}, description #{li.description}")
        end
      end

      # step 11 - copy over all sample_invoice.payments_or_credits.
      # (but it will likely get refactored as soon as we enable payments to be changed in mobile)
      @sample_invoice.payments_or_credits.build(
        @original_invoice.payments_or_credits.map do |payment_or_credit|
          payment_or_credit.dup.tap do |dup_payment_or_credit|
            dup_payment_or_credit.id = payment_or_credit.id
          end.attributes
        end
      )

      # step 12 - update totals
      @sample_invoice.calculate_total_amount

      self
    end

  end
end
