# frozen_string_literal: true

# Service to trigger Swoop Sending Fleet invoice transition.
#
# Business rule: if Fleet Response, it will send the invoice
# to Fleet Reponse endpoint; otherwise, it will just set the
# invoice state to 'swoop_sending_fleet'.
#
module InvoiceService
  class SwoopSentFleet

    include SwoopService

    FLEETS_ALLOWED_TO_RECEIVE_INVOICE = {
      Company::FLEET_RESPONSE => FleetCompany::FLEET_RESPONSE_CAMEL_CASE,
    }.freeze

    def initialize(invoice_id:, api_company_id: nil, api_user_id: nil)
      raise MissingInvoiceError if invoice_id.blank? || api_company_id.blank? || api_user_id.blank?

      @swoop_invoice_id  = invoice_id
      @api_company_id    = api_company_id
      @api_user_id       = api_user_id
      @api_async_request = nil

      Rails.logger.debug(
        "INVOICE(#{@swoop_invoice_id}) InvoiceService::ExportToFleet InvoiceService::SwoopSentFleet initialized with " \
        "swoop_invoice_id: #{@swoop_invoice_id}, api_company_id: #{@api_company_id}, api_user_id: #{@api_user_id}"
      )
    end

    def execute
      Invoice.transaction do
        @swoop_invoice = Invoice.find_by!(id: @swoop_invoice_id, sender_id: @api_company_id)

        Rails.logger.debug("INVOICE(#{@swoop_invoice_id}) InvoiceService::ExportToFleet InvoiceService::SwoopSentFleet call - " \
          "swoop invoice found, state #{@swoop_invoice.state}")

        raise InvoiceStateTransitionError if !@swoop_invoice.may_swoop_sending_fleet?

        @swoop_invoice.swoop_sending_fleet

        if can_send_invoice_to_fleet_endpoint?
          create_and_associate_api_async_request
          clean_current_export_error_on_invoice
        end

        @swoop_invoice.save!

        Rails.logger.debug("INVOICE(#{@swoop_invoice_id}) InvoiceService::ExportToFleet InvoiceService::SwoopSentFleet " \
          "call - partner invoice state changed to #{@swoop_invoice.state}")
      end

      self
    end

    def save_mutated_objects
      # not impletemented as it's not needed in this case.
      # Here just to make it compatible with the other legacy invoice transition services,
      # so the caller can safely call save_mutated_objects without error.
    end

    def after_execution
      # this is called in 'after_execution' because we need the invoice and
      # api_async_request created on execute ^ to be commited to the DB.
      send_invoice_to_fleet_endpoint_async_if_allowed
    end

    private

    def can_send_invoice_to_fleet_endpoint?
      FLEETS_ALLOWED_TO_RECEIVE_INVOICE.key?(@swoop_invoice.recipient_company.name)
    end

    def create_and_associate_api_async_request
      Rails.logger.debug("INVOICE(#{@swoop_invoice_id}) InvoiceService::ExportToFleet InvoiceService::SwoopSentFleet " \
        "create_and_associate_api_async_request - about to create a new api_async_request")

      @api_async_request = API::AsyncRequest.create!(
        target_id: @swoop_invoice.id,
        company_id: @api_company_id,
        user_id: @api_user_id,
        system: API::AsyncRequest::FLEET_RESPONSE_INVOICE
      )

      @swoop_invoice.api_async_request_id = @api_async_request.id

      Rails.logger.debug("INVOICE(#{@swoop_invoice_id}) InvoiceService::ExportToFleet InvoiceService::SwoopSentFleet " \
        "create_and_associate_api_async_request - api_async_request(#{@api_async_request.id}) created and associated with the swoop invoice")
    end

    def clean_current_export_error_on_invoice
      @swoop_invoice.export_error_msg = nil
    end

    def send_invoice_to_fleet_endpoint_async_if_allowed
      Rails.logger.debug("INVOICE(#{@swoop_invoice_id}) InvoiceService::ExportToFleet InvoiceService::SwoopSentFleet " \
        "send_invoice_to_fleet_endpoint_async_if_allowed - can_send_invoice_to_fleet_endpoint? #{can_send_invoice_to_fleet_endpoint?}")

      return unless can_send_invoice_to_fleet_endpoint?

      worker =
        "InvoiceService::ExportToFleet::#{FLEETS_ALLOWED_TO_RECEIVE_INVOICE[@swoop_invoice.recipient_company.name]}::Worker"
          .constantize

      Rails.logger.debug("INVOICE(#{@swoop_invoice_id}) InvoiceService::ExportToFleet InvoiceService::SwoopSentFleet " \
        "send_invoice_to_fleet_endpoint_async_if_allowed - worker found #{worker.name}")

      worker.perform_async(@swoop_invoice_id, @api_async_request.id)
    end

  end
end
