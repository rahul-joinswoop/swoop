# frozen_string_literal: true

# Service to trigger Swoop Sending Fleet invoice transition in batch.
#
# It will:
#   - fetch all invoices in swoop_approved_fleet state for the given client_company_id.
#   - update all invoices state to swoop_sending_fleet
#   - trigger ExportAllWorker asynchornously to export the invoices to the client company endpoint.
#     - it will split the invoices by INVOICES_SENT_AT_A_TIME and trigger ExportAllWorker for each splitted portion.
#
#   - total amount of invoices allowed is defined by LIMIT_INVOICE_SENDS
#
module InvoiceService
  class SwoopExportAllToFleet

    include SwoopService

    attr_reader :async_request, :invoice_ids

    FLEETS_ALLOWED_TO_RECEIVE_INVOICE = {
      Company::FLEET_RESPONSE => InvoiceService::ExportToFleet::FleetResponse::ExportAllWorker,
    }.freeze

    LIMIT_INVOICE_SENDS = ENV.fetch('LIMIT_INVOICE_SENDS', 4000).to_i
    INVOICES_SENT_AT_A_TIME = ENV.fetch('INVOICES_SENT_AT_A_TIME', 20).to_i

    def initialize(api_company_id: nil, api_user_id: nil, client_company_id: nil)
      @api_company_id    = api_company_id
      @api_user_id       = api_user_id
      @client_company_id = client_company_id
      @async_request     = nil
      @invoice_ids       = nil

      raise ArgumentError, "InvoiceService::SwoopExportAllToFleet#initialize api_company_id is empty" unless @api_company_id

      raise ArgumentError, "InvoiceService::SwoopExportAllToFleet#initialize api_user_id is empty" unless @api_user_id

      raise ArgumentError, "InvoiceService::SwoopExportAllToFleet#initialize client_company_id is empty" unless @client_company_id

      Rails.logger.debug(
        "InvoiceService::SwoopExportAllToFleet " \
        "initialized with api_company_id: #{@api_company_id}, api_user_id: #{@api_user_id}, client_company_id: #{@client_company_id}"
      )
    end

    def execute
      create_async_request!

      fetch_swoop_approved_fleet_invoice_ids

      if @invoice_ids.present? && can_send_invoice_to_fleet_endpoint?
        move_invoices_state_to_swoop_sending_fleet_and_clean_errors!

        publish_partial_invoices_through_ws

        split_invoices_and_async_export_them_to_fleet_endpoint
      else
        Rails.logger.debug("InvoiceService::SwoopExportAllToFleet#execute no invoices found")
      end

      Rails.logger.debug("InvoiceService::SwoopExportAllToFleet#execute finished")

      self
    end

    private

    def create_async_request!
      @async_request ||= API::AsyncRequest.create!(
        company_id: @api_company_id,
        user_id: @api_user_id,
        request: {
          method: 'SwoopExportAllToFleet',
          params: {
            api_company_id: @api_company_id,
            api_user_id: @api_user_id,
            client_company_id: @client_company_id,
          },
        },
        system: API::AsyncRequest::FLEET_RESPONSE_INVOICE
      )
    end

    def fetch_swoop_approved_fleet_invoice_ids
      @invoice_ids ||= Invoice.not_deleted.where(
        state: Invoice::SWOOP_APPROVED_FLEET,
        recipient_company_id: @client_company_id
      ).limit(LIMIT_INVOICE_SENDS).pluck(:id)

      Rails.logger.debug("InvoiceService::SwoopExportAllToFleet fetch_swoop_approved_fleet_invoice_ids - " \
        "invoices found #{@invoice_ids}")

      @invoice_ids
    end

    # # #
    #
    # http://guides.rubyonrails.org/active_record_callbacks.html#skipping-callbacks
    #
    # invoices#update_all will skip callbacks, and that's right here since we don't
    # need any callbacks to be kicked, and mostly we don't want each invoice to be
    # pushed by WS on this (WS update msg is kicked by a callback on Publishable concern).
    #
    def move_invoices_state_to_swoop_sending_fleet_and_clean_errors!
      Rails.logger.debug(
        "InvoiceService::SwoopExportAllToFleet#move_invoices_state_to_swoop_sending_fleet! " \
        "about to move invoices to #{Invoice::SWOOP_SENDING_FLEET}"
      )

      Invoice.where(id: @invoice_ids).update_all(
        state: Invoice::SWOOP_SENDING_FLEET,
        export_error_msg: nil,
        updated_at: Time.now
      )
    end

    # we refetch the invoices with current state
    def invoices_with_state
      Invoice.where(id: @invoice_ids).select(:id, :state)
    end

    def invoices_with_state_array
      invoices_with_state.map do |invoice|
        { id: invoice.id, state: invoice.state }
      end
    end

    def publish_partial_invoices_through_ws
      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'PartialObjectArray',
          target_data: {
            partial_invoices: invoices_with_state_array,
          },
          company: Company.find(@api_company_id),
        }
      )
    end

    def can_send_invoice_to_fleet_endpoint?
      FLEETS_ALLOWED_TO_RECEIVE_INVOICE.keys.include?(
        client_company.name
      )
    end

    def client_company
      @client_company ||= Company.find(@client_company_id)
    end

    def split_invoices_and_async_export_them_to_fleet_endpoint
      Rails.logger.debug("InvoiceService::SwoopExportAllToFleet " \
        "split_invoices_and_async_export_them_to_fleet_endpoint - can_send_invoice_to_fleet_endpoint? #{can_send_invoice_to_fleet_endpoint?}")

      return unless can_send_invoice_to_fleet_endpoint?

      worker = FLEETS_ALLOWED_TO_RECEIVE_INVOICE[client_company.name]

      @invoice_ids.in_groups_of(INVOICES_SENT_AT_A_TIME, false) do |split_invoice_ids|
        Rails.logger.debug("InvoiceService::SwoopExportAllToFleet " \
          "export_invoices_to_fleet_endpoint_async - worker found: #{worker.name}, about to trigger it for invoice_ids #{split_invoice_ids}")

        worker.perform_async(split_invoice_ids, @async_request.id)
      end
    end

  end
end
