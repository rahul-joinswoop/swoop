# frozen_string_literal: true

module InvoiceService
  class RefundCard

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "InvoiceService::Refund::Invoice called with #{context.input}"

      @company_id = context.input[:company_id]
      @user_id = context.input[:user_id]
      @invoice_id = context.input[:invoice_id]
      @payment_id = context.input[:payment_id]

      if @company_id.blank?
        raise ArgumentError, "InvoiceService::Refund::Invoice @company_id is blank"
      end

      if @user_id.blank?
        raise ArgumentError, "InvoiceService::Refund::Invoice @user_id is blank"
      end

      if @invoice_id.blank?
        raise ArgumentError, "InvoiceService::Refund::Invoice @invoice_id is blank"
      end

      if @payment_id.blank?
        raise ArgumentError, "InvoiceService::Refund::Invoice @payment_id is blank"
      end
    end

    def call
      context.output = create_api_async_request!

      schedule_async_charge_worker
    end

    private

    def invoice
      @invoice ||= Invoice.find_by!(
        id: @invoice_id,
        sender_id: @company_id,
        deleted_at: nil
      )
    end

    # we create the async_quest for auditing and tracking the flow
    def create_api_async_request!
      @api_async_request = API::AsyncRequest.create!(
        company_id: @company_id,
        user_id: @user_id,
        request: invoice_payment_hash,
        target_id: @invoice_id,
        system: API::AsyncRequest::INVOICE_REFUND_CARD
      )
    end

    def invoice_payment_hash
      {
        payment: {
          id: @payment_id.to_i,
        },
      }
    end

    # This is the only method in the RefundCard service
    # where Stripe is mentioned, because here we need to trigger
    # the worker using a specific implementation.
    def schedule_async_charge_worker
      Stripe::RefundWorker.perform_async(
        @api_async_request.id,
        @invoice_id,
        @payment_id
      )
    end

  end
end
