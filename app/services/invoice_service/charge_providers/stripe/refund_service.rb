# frozen_string_literal: true

module InvoiceService
  module ChargeProviders
    module Stripe

      class InvalidBankAccountError < StandardError; end
      class UserNotAuthorizedError < StandardError; end
      class PaymentAlreadyRefundedError < StandardError; end

      class RefundService

        include Interactor
        include Interactor::Swoop

        BANK_ACCOUNT_NOT_LINKED_ERROR = 'Bank account not linked'
        USER_ROLE_NOT_AUTHORIZED_ERROR = 'User role not authorized'
        PAYMENT_ALREADY_REFUNDED_ERROR = 'Payment has already been refunded'

        before do
          Rails.logger.debug(
            "InvoiceService::ChargeProviders::Stripe::RefundService called with #{context}"
          )

          @sender_id  = context.input[:sender_id]
          @invoice_id = context.input[:invoice_id]
          @user_id = context.input[:user_id]
          @payment_id = context.input[:payment_id]

          if @sender_id.blank?
            raise(
              ArgumentError,
              'InvoiceService::ChargeProviders::Stripe::RefundService @sender_id is null'
            )
          end

          if @invoice_id.blank?
            raise(
              ArgumentError,
              'InvoiceService::ChargeProviders::Stripe::RefundService @invoice_id is null'
            )
          end

          if @user_id.blank?
            raise(
              ArgumentError,
              'InvoiceService::ChargeProviders::Stripe::RefundService @user_id is null'
            )
          end

          if @payment_id.blank?
            raise(
              ArgumentError,
              'InvoiceService::ChargeProviders::Stripe::RefundService @payment_id is null'
            )
          end

          Rails.logger.debug(
            "INVOICE(#{@invoice_id}) InvoiceService::ChargeProviders::Stripe::RefundService " \
            "perform triggered for sender_id #{@sender_id}, payment_id " \
            "#{@payment_id}"
          )
        end

        def call
          refund!
        end

        private

        def refund!
          Rails.logger.debug(
            "INVOICE(#{@invoice_id}) InvoiceService::ChargeProviders::Stripe::RefundService " \
            "refund! - starting"
          )

          validate_user_permissions
          validate_linked_bank_account
          validate_payment_already_refunded

          # create refund on Stripe end
          @stripe_refund = fetch_existent_or_create_new_refund_on_stripe!

          Invoice.transaction do
            add_refund_to_payment!

            context.output = {
              refund: @refund,
              stripe_refund: @stripe_refund,
            }

            Rails.logger.debug(
              "INVOICE(#{@invoice_id}) InvoiceService::ChargeProviders::Stripe::RefundService " \
              "refund well succeeded."
            )

            invoice.base_publish('update')
          end
        end

        def fetch_existent_or_create_new_refund_on_stripe!
          @stripe_refund = fetch_existent_stripe_refund || create_stripe_refund!
        end

        def fetch_existent_stripe_refund
          @stripe_refund =
            ::Stripe::Charge.retrieve(payment_charge.upstream_charge_id).refunds.first
        end

        def create_stripe_refund!
          ::Stripe::Refund.create(stripe_refund_hash)
        end

        def add_refund_to_payment!
          # create refund based on payment charge
          @refund = create_invoice_refund!

          # update charge in the db
          update_payment_charge!
        end

        def create_invoice_refund!
          @refund = create_swoop_invoice_refund!
        end

        def create_swoop_invoice_refund!
          @refund = Payments::UpdatePaymentsService.new(
            new_refund_hash,
            invoice
          ).call.created_payments_or_credits.first
        end

        def new_refund_hash
          [
            {
              type: Payment::REFUND,
              payment_method: PaymentMethod::CREDIT_CARD,
              amount: - payment.total_amount,
              currency: payment.currency,
              mark_paid_at: Time.current,
            },
          ]
        end

        def update_payment_charge!
          payment_charge.update!(
            refund_id: @refund.id,
            refund_user_id: user.id,
          )
        end

        def stripe_refund_hash
          @stripe_refund_hash ||= {
            charge: payment_charge.upstream_charge_id,
            metadata: {
              job_id: invoice.job_id,
              invoice_payment_charge_id: payment_charge.id,
            },
            reverse_transfer: true,
          }
        end

        def invoice
          @invoice ||= Invoice.find_by!(id: @invoice_id, sender_id: @sender_id)
        end

        def payment_charge
          payment.charge
        end

        def payment
          @payment ||= invoice.payments.find(@payment_id)
        end

        def user
          @user ||= User.find_by!(id: @user_id, company_id: @sender_id)
        end

        def permitted_roles
          @permitted_roles ||= invoice.sender.role_permissions.where(
            permission_type: CompanyRolePermission::REFUND
          ).map(&:role)
        end

        def validate_user_permissions
          raise UserNotAuthorizedError, USER_ROLE_NOT_AUTHORIZED_ERROR unless
            user.roles.any? { |role| permitted_roles.include? role }
        end

        def validate_linked_bank_account
          retriever = StripeIntegration::BankAccountRetriever.new({
            stripe_account_id: invoice.sender.custom_account&.upstream_account_id,
          })

          raise InvalidBankAccountError, BANK_ACCOUNT_NOT_LINKED_ERROR if
            retriever.retrieve.blank?
        end

        def validate_payment_already_refunded
          raise PaymentAlreadyRefundedError, PAYMENT_ALREADY_REFUNDED_ERROR if
            payment_charge.refund_id.present?
        end

      end

    end
  end
end
