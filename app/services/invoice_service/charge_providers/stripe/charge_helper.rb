# frozen_string_literal: true

module InvoiceService
  module ChargeProviders
    module Stripe
      module ChargeHelper

        def stripe_charge_hash
          @stripe_charge_hash ||= {
            amount: charge_amount_to_stripe,
            currency: invoice.currency,
            source: stripe_token,
            statement_descriptor: cleaned_partner_company_name,
            description: invoice.job_id,
            metadata: {
              job_id: invoice.job_id,
              invoice_payment_charge_id: @invoice_payment_charge&.id,
            },
            destination: {
              amount: destination_amount,
              account: partner_stripe_account_id,
            },
          }
        end

        # Stripe doe not allow these chars: < > " ' \
        #
        # Stripe also limit it in 22 chars.
        def cleaned_partner_company_name
          invoice.sender.name.gsub(/[<>"'\\]/, '').strip[0..21]
        end

        def partner_stripe_account_id
          invoice.sender.custom_account.upstream_account_id
        end

        def destination_amount
          return @destination_amount if @destination_amount

          # Stripe only accepts cents as integer, so we multiply it by 100
          @destination_amount = (
            (charge_amount - charge_fee).round(2, BigDecimal::ROUND_DOWN) * 100
          ).to_i
        end

        def charge_fee
          return @charge_fee if @charge_fee

          fixed_value = invoice.sender.invoice_charge_fee.fixed_value

          percentage_over_total_amount =
            charge_amount * (invoice.sender.invoice_charge_fee.percentage / 100)

          @charge_fee ||= BigDecimal(
            fixed_value + percentage_over_total_amount
          ).round(2, BigDecimal::ROUND_DOWN)
        end

        def charge_amount
          @charge_amount ||= BigDecimal(@invoice_charge_attributes[:amount].to_s)
        end

        # stripe accepts cents as integer, so we need to multiply it by 100
        # Ex $100,00 will be 10000 to Stripe API
        def charge_amount_to_stripe
          @charge_amount_to_stripe ||= (charge_amount * 100).to_i
        end

        def stripe_token
          @invoice_charge_attributes[:token]
        end

        def stripe_card_error_code_from_exception(exception)
          return '' unless exception.instance_of? ::Stripe::CardError

          exception.json_body[:error][:decline_code] || exception.json_body[:error][:code]
        end

      end
    end
  end
end
