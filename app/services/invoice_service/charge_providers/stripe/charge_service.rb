# frozen_string_literal: true

module InvoiceService
  module ChargeProviders
    module Stripe

      class InvalidAmountError < StandardError; end
      class InvalidBankAccountError < StandardError; end
      class UserNotAuthorizedError < StandardError; end

      class ChargeService

        include Interactor
        include Interactor::Swoop
        include ChargeHelper

        GENERIC_ERROR = 'Charge failed. Please try again.'
        AMOUNT_CANNOT_EXCEED_BALANCE_ERROR = 'Amount cannot exceed balance'
        BANK_ACCOUNT_NOT_LINKED_ERROR = 'Bank account not linked'
        USER_ROLE_NOT_AUTHORIZED_ERROR = 'User role not authorized'

        before do
          Rails.logger.debug(
            "InvoiceService::ChargeProviders::Stripe::ChargeService called with #{context}"
          )

          @sender_id  = context.input[:sender_id]
          @user_id    = context.input[:user_id]
          @invoice_id = context.input[:invoice_id]
          @invoice_charge_attributes = context.input[:invoice_charge_attributes]

          if @sender_id.blank?
            raise(
              ArgumentError,
              'InvoiceService::ChargeProviders::Stripe::ChargeService @sender_id is null'
            )
          end

          if @user_id.blank?
            raise(
              ArgumentError,
              'InvoiceService::ChargeProviders::Stripe::ChargeService @user_id is null'
            )
          end

          if @invoice_id.blank?
            raise(
              ArgumentError,
              'InvoiceService::ChargeProviders::Stripe::ChargeService @invoice_id is null'
            )
          end

          if @invoice_charge_attributes.blank?
            raise(
              ArgumentError,
              "InvoiceService::ChargeProviders::Stripe::ChargeService " \
              "@invoice_charge_attributes is null"
            )
          end
        end

        def call
          Rails.logger.debug(
            "INVOICE(#{@invoice_id}) InvoiceService::ChargeProviders::Stripe::ChargeService " \
            "perform triggered for sender_id #{@sender_id}"
          )

          charge!

          context.output = {
            payment: @payment,
            stripe_charge: @stripe_charge,
          }
        end

        private

        def charge!
          Rails.logger.debug(
            "INVOICE(#{@invoice_id}) InvoiceService::ChargeProviders::Stripe::ChargeService " \
            "charge! - about to lock the invoice and start the charge flow"
          )

          create_running_invoice_payment_charge!

          run_validation_and_raise_error_if_needed

          # create charge on Stripe end, this is out of the transaction
          # to avoid a locking overhead

          # rubocop:disable Rails/SaveBang
          @stripe_charge = ::Stripe::Charge.create(stripe_charge_hash)
          # rubocop:enable Rails/SaveBang

          update_invoice_with_payment!

          push_analytics_metric
        rescue ::Stripe::CardError => e
          error_code = stripe_card_error_code_from_exception(e)

          add_error_to_invoice_payment_charge!(error_code)

          push_analytics_metric('Failure', error_code)

          context.fail!(error: e)
        rescue InvalidAmountError, InvalidBankAccountError, UserNotAuthorizedError,
               StandardError => e
          Rails.logger.info(
            "INVOICE(#{@invoice_id}) InvoiceService::ChargeProviders::Stripe::ChargeService " \
            "could not charge the invoice, #{e.message}"
          )

          add_error_to_invoice_payment_charge!(e.message)

          context.fail!(error: e)
        ensure
          send_slack_notification
        end

        def run_validation_and_raise_error_if_needed
          validate_user_permissions
          validate_linked_bank_account
          validate_payment_less_or_equals_balance
        end

        def create_running_invoice_payment_charge!
          @invoice_payment_charge = InvoicePaymentCharge.create!(
            invoice_id: @invoice_id,
            status: InvoicePaymentCharge::CHARGE_STATUS_RUNNING,
            charge_user_id: user.id,
          )
        end

        def update_invoice_with_payment!
          invoice.with_lock do
            # add payment based on charge
            add_payment!

            # add charge to db
            update_invoice_payment_charge!

            Rails.logger.debug(
              "INVOICE(#{@invoice_id}) InvoiceService::ChargeProviders::Stripe::ChargeService " \
              "charge well succeeded."
            )

            invoice.base_publish('update')
          end

          if send_recipient_to_email_address.present?
            invoice.send_charge_card_receipt(to: send_recipient_to_email_address)

            @invoice_payment_charge.update!(charge_receipt_email_sent: true)
          end
        end

        def send_recipient_to_email_address
          @send_recipient_to_email_address ||= @invoice_charge_attributes[:send_recipient_to]
        end

        def add_payment!
          @payment = Payments::UpdatePaymentsService.new(
            new_payment_hash,
            invoice
          ).call.created_payments_or_credits.first
        end

        def new_payment_hash
          [
            {
              type: Payment::PAYMENT,
              payment_method: PaymentMethod::CREDIT_CARD,
              memo: payment_memo,
              amount: - charge_amount,
              mark_paid_at: Time.current,
            },
          ]
        end

        def update_invoice_payment_charge!
          @invoice_payment_charge.update!(
            payment_id: @payment.id,
            last_4: @stripe_charge.source.last4,
            card_brand: @stripe_charge.source.brand,
            upstream_charge_id: @stripe_charge.id,
            data: JSON.parse(@stripe_charge.to_s),
            status: InvoicePaymentCharge::CHARGE_STATUS_SUCCESSFUL,
            fee: charge_fee
          )
        end

        def push_analytics_metric(event = 'Success', error_code = nil)
          Rails.logger.debug(
            "INVOICE(#{@invoice_id}) #{self.class.name} " \
            "about to push Analytics message for Stripe Card error code #{error_code}"
          )

          message_body = {
            user_id: user.to_ssid,
            event: "Charge Card - #{event}",
            properties: {
              category: 'Payments',
              value: charge_amount.to_s,
              account: invoice.job.account.name,
              service: invoice.job.service_code.name,
              jobId: invoice.job.id,
            },
          }

          message_body[:properties][:label] = error_code if error_code

          Analytics.track(message_body)
        end

        def add_error_to_invoice_payment_charge!(error_message)
          @invoice_payment_charge.update!(
            status: InvoicePaymentCharge::CHARGE_STATUS_ERROR,
            error_msg: error_message,
          )
        end

        def invoice
          @invoice ||= Invoice.find_by!(id: @invoice_id, sender_id: @sender_id)
        end

        def payment_memo
          @payment_memo ||= @invoice_charge_attributes[:memo]
        end

        def user
          @user ||= User.find_by!(id: @user_id, company_id: @sender_id)
        end

        def permitted_roles
          @permitted_roles ||= invoice.sender.role_permissions.where(
            permission_type: CompanyRolePermission::CHARGE
          ).map(&:role)
        end

        def validate_user_permissions
          raise UserNotAuthorizedError, USER_ROLE_NOT_AUTHORIZED_ERROR unless
            user.roles.any? { |role| permitted_roles.include? role }
        end

        def validate_payment_less_or_equals_balance
          Rails.logger.debug(
            "INVOICE(#{@invoice_id}) InvoiceService::ChargeProviders::Stripe::ChargeService " \
            "charge_amount: #{charge_amount} and invoice.balance: #{invoice.balance}"
          )

          raise InvalidAmountError, AMOUNT_CANNOT_EXCEED_BALANCE_ERROR if
            charge_amount > invoice.balance
        end

        def validate_linked_bank_account
          retriever = StripeIntegration::BankAccountRetriever.new({
            stripe_account_id: invoice.sender.custom_account&.upstream_account_id,
          })

          raise InvalidBankAccountError, BANK_ACCOUNT_NOT_LINKED_ERROR if
            retriever.retrieve.blank?
        end

        def send_slack_notification
          if @invoice_payment_charge.present?
            SlackChannel.message([
              invoice.sender.name,
              invoice.recipient_type == 'Account' ? invoice.recipient.name : 'Cash Call',
              @invoice_payment_charge.updated_at,
              @invoice_payment_charge.slack_status_message,
            ].join(" | "), tags: [:payment])
          end
        end

      end

    end
  end
end
