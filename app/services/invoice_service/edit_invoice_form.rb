# frozen_string_literal: true

# Service to Move an Invoice back to new by Partner.
module InvoiceService
  class EditInvoiceForm < Service

    include LoggableJob
    include Utils

    attr_reader :invoice

    def initialize(invoice, params)
      super()
      @invoice = invoice
      @params = params
      log :debug, "called with invoice:#{@invoice.id}, params:#{@params}", job: @invoice.job_id
    end

    def call
      ApplicationRecord.transaction do
        process_payments
        if editable?(@invoice, @params)
          @invoice.update!(@params)
          set_estimated_false_if_necessary
        else
          raise ArgumentError, "not allowed to edit invoice in state: #{@invoice.state}, params:#{@params}"
        end
      end

      @invoice.reload

      self
    end

    private

    def process_payments
      payments = remove_param(:payments, @params)
      Rails.logger.debug "calling update with p:#{p}, payments:#{payments}"
      if payments
        payments_service = Payments::UpdatePaymentsService.new(payments, @invoice)
        payments_service.call
      end
    end

    def set_estimated_false_if_necessary
      # PDW: Don't freeze the invoice when one of the LI's change ENG-3933

      Rails.logger.debug "EditInvoiceForm saved_changes:#{@invoice.saved_changes}"

      @invoice.line_items.each do |li|
        Rails.logger.debug "EditInvoiceForm li saved_changes:#{li.saved_changes} should be set to false: #{li.should_estimated_be_set_to_false?}"
        if li.should_estimated_be_set_to_false?
          li.estimated = false
          li.save!
        end
      end
    end

    def editable?(invoice, p)
      p.blank? || invoice.editable? || allowed_attribs(p)
    end

    def allowed_attribs(p)
      keys = p.keys - ["email_to", "email_cc", "email_bcc", "paid", "qb_pending_import"]

      if keys.include? "job_attributes"
        job_attrs = p["job_attributes"]
        if !((job_attrs && job_attrs.size == 2 && !job_attrs['driver_notes'].nil?))
          return false
        end
        keys.delete("job_attributes")
      end

      keys.empty?
    end

  end
end
