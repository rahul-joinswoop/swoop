# frozen_string_literal: true

module InvoiceService
  module ExportToFleet
    module FleetResponse
      class UpdateApiAsyncRequestHashWithRequestValues

        include Interactor
        include Interactor::Swoop

        before do
          Rails.logger.debug "INVOICE(#{context.input[:swoop_invoice].id}) InvoiceService::ExportToFleet::UpdateApiAsyncRequestWithRequestValues called"

          @swoop_invoice        = context.input[:swoop_invoice]
          @invoice_changes_hash = context.input[:invoice_changes_hash]
          @api_async_request_changes_hash = context.input[:api_async_request_changes_hash]
          @soap_request_values = context.input[:soap_request_values]

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateApiAsyncRequestWithRequestValues swoop_invoice is null' unless @swoop_invoice

          raise ArgumentError, 'InvoiceService::ExportToFleet::MapSoapRequestValues invoice_changes_hash is null' unless @invoice_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint api_async_request_changes_hash is null' unless @api_async_request_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateApiAsyncRequestWithRequestValues soap_request_values is null' unless @soap_request_values
        end

        def call
          begin
            soap_request = SoapClient.instance.build_request(:swoop_billing, message: @soap_request_values)
          rescue StandardError => e
            Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::FleetResponse::UpdateApiAsyncRequestWithRequestValues " \
              "swoop_invoice.api_async_request there was an error on Fleet Response SOAP endpoint call")

            raise e
          end

          @api_async_request_changes_hash = {
            external_request_url: soap_request.url.to_s,
            external_request_headers: soap_request.headers.to_json,
            external_request_body: soap_request.body,
          }

          context.output = {
            swoop_invoice: @swoop_invoice,
            invoice_changes_hash: @invoice_changes_hash,
            api_async_request_changes_hash: @api_async_request_changes_hash,
            soap_request_values: @soap_request_values,
          }
        end

      end
    end
  end
end
