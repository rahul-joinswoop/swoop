# frozen_string_literal: true

module InvoiceService
  module ExportToFleet
    module FleetResponse
      class UpdateInvoiceHashWithExportValues

        include Interactor
        include Interactor::Swoop

        # FleetResponse errors
        FLEET_RESPONSE_ERROR = 'Error'
        TAX_EXEMPT = 'Tax Exempt'
        NOT_FOUND = 'Not Found'

        # error messages
        NOT_ABLE_TO_PROCESS_ERROR_MESSAGE = 'Fleet Response was unable to process the transaction and Fleet Response has been notified'
        TAX_EXEMPT_ERROR_MESSAGE = 'Client is tax exempt but is being charged tax, invoice is rejected and not saved'
        NOT_FOUND_ERROR_MESSAGE = 'Fleet Response Claim not found or did not have record of Swoop service on that claim'
        GENERIC_ERROR_MESSAGE = 'Error with the export invoice process. Please try again or report to an admin.'

        before do
          Rails.logger.debug "InvoiceService::ExportToFleet::UpdateInvoiceWithResponseValues called"

          @swoop_invoice = context.input[:swoop_invoice]
          @invoice_changes_hash = context.input[:invoice_changes_hash]
          @api_async_request_changes_hash = context.input[:api_async_request_changes_hash]
          @soap_response = context.input[:soap_response]

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateInvoiceWithResponseValues swoop_invoice is null' unless @swoop_invoice

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateInvoiceWithResponseValues invoice_changes_hash is null' unless @invoice_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateInvoiceWithResponseValues api_async_request_changes_hash is null' unless @api_async_request_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateInvoiceWithResponseValues soap_response is null' unless @soap_response
        end

        def call
          response_status = @soap_response.body[:swoop_billing_response][:swoop_billing_result]

          Rails.logger.debug("INVOICE(#{@swoop_invoice_id}) InvoiceService::ExportToFleet::FleetResponse::UpdateInvoiceWithResponseValues " \
            "response_status == #{response_status}")

          if response_status == "Accepted"
            @invoice_changes_hash[:state_transition] = state_transition_to_sent
          else
            @invoice_changes_hash[:export_error_msg] = export_error_message_to_invoice(response_status)

            @invoice_changes_hash[:state_transition] = state_transition_go_back_to_approved
          end

          context.output = {
            invoice_changes_hash: @invoice_changes_hash,
            api_async_request_changes_hash: @api_async_request_changes_hash,
          }
        end

        private

        def state_transition_to_sent
          Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::FleetResponse::UpdateInvoiceWithResponseValues " \
            "adding invoice state transition to swoop_sent_fleet")

          Invoice::SWOOP_SENT_FLEET
        end

        def state_transition_go_back_to_approved
          Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::FleetResponse::UpdateInvoiceWithResponseValues " \
            "adding invoice state transition to swoop_approved_fleet")

          Invoice::SWOOP_SEND_FLEET_ERROR
        end

        def export_error_message_to_invoice(response_status)
          case response_status
          when FLEET_RESPONSE_ERROR
            NOT_ABLE_TO_PROCESS_ERROR_MESSAGE
          when TAX_EXEMPT
            TAX_EXEMPT_ERROR_MESSAGE
          when NOT_FOUND
            NOT_FOUND_ERROR_MESSAGE
          else
            GENERIC_ERROR_MESSAGE
          end
        end

      end
    end
  end
end
