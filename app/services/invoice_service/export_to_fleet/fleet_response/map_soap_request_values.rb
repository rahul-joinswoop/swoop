# frozen_string_literal: true

module InvoiceService
  module ExportToFleet
    module FleetResponse
      class MapSoapRequestValues

        include Interactor
        include Interactor::Swoop

        before do
          Rails.logger.debug "INVOICE(#{context.input[:swoop_invoice].id}) InvoiceService::ExportToFleet::MapSoapRequestValues called}"

          @swoop_invoice = context.input[:swoop_invoice]
          @invoice_changes_hash = context.input[:invoice_changes_hash]
          @api_async_request_changes_hash = context.input[:api_async_request_changes_hash]

          raise ArgumentError, 'InvoiceService::ExportToFleet::MapSoapRequestValues swoop_invoice is null' unless @swoop_invoice

          raise ArgumentError, 'InvoiceService::ExportToFleet::MapSoapRequestValues invoice_changes_hash is null' unless @invoice_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint api_async_request_changes_hash is null' unless @api_async_request_changes_hash
        end

        def call
          job = @swoop_invoice.job

          soap_request_values = {
            username: Configuration.fleet_response_invoice_endpoint.username,
            password: Configuration.fleet_response_invoice_endpoint.password,
            FleetClaim: job.ref_number, # 758588,# currently commented out for testing purposes, otherwise Fleet Response returns as Not Found
            SwoopClaimNumber: job.id, # 400980, currently commented out for testing purposes, otherwise Fleet Response returns as Not Found
            pdfInvoice: generate_invoice_pdf_encoded_64_bytes,
            invoiceAmount: @swoop_invoice.balance,
            discount: sprintf('%.2f', @swoop_invoice.discounts.map(&:total_amount).sum.abs),
            tax: sprintf('%.2f', @swoop_invoice.tax_amount),
            completionDate: job.completed_at.strftime('%Y-%m-%d'),
            workState: @swoop_invoice.job.service_location.state,
            serviceDate: job.completed_at.strftime('%Y-%m-%d'),
            ers: job.service_code.name,
          }

          Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::MapSoapRequestValues call " \
            "built (user/password/pdfInvoice not shown): #{soap_request_values.without(:username, :password, :pdfInvoice)}")

          context.output = {
            swoop_invoice: @swoop_invoice,
            invoice_changes_hash: @invoice_changes_hash,
            api_async_request_changes_hash: @api_async_request_changes_hash,
            soap_request_values: soap_request_values,
          }
        end

        private

        # heads up: this can potentially cause much overhead!
        def generate_invoice_pdf_encoded_64_bytes
          Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::MapSoapRequestValues " \
            "generate_invoice_pdf_encoded_64_bytes about to generate the invoice pdf. Heads up: this can potentially cause much overhead!")

          pdf = InvoicePdfGenerator.new(@swoop_invoice).generate

          Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::MapSoapRequestValues " \
            "generate_invoice_pdf_encoded_64_bytes pdf generated")

          Base64.encode64(pdf)
        end

      end
    end
  end
end
