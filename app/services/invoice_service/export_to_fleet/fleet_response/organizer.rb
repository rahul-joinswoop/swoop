# frozen_string_literal: true

module InvoiceService
  module ExportToFleet
    module FleetResponse
      class Organizer

        include Interactor::Organizer
        include Interactor::Swoop

        # step 2 - Create soap_request_values
        # step 3 - Generate a soap_request and update api_async_request with its values
        # step 4 - POST request Fleet Response endpoint
        # step 5 - Parse soap response and update api_async_request its details
        organize MapSoapRequestValues, UpdateApiAsyncRequestHashWithRequestValues, PostRequestToSoapEndpoint, UpdateApiAsyncRequestHashWithResponseValues, UpdateInvoiceHashWithExportValues

      end
    end
  end
end
