# frozen_string_literal: true

module InvoiceService
  module ExportToFleet
    module FleetResponse
      class UpdateApiAsyncRequestHashWithResponseValues

        include Interactor
        include Interactor::Swoop

        before do
          Rails.logger.debug "INVOICE(#{context.input[:swoop_invoice].id}) InvoiceService::ExportToFleet::UpdateApiAsyncRequestWithResponseValues called"

          @swoop_invoice = context.input[:swoop_invoice]
          @invoice_changes_hash = context.input[:invoice_changes_hash]
          @api_async_request_changes_hash = context.input[:api_async_request_changes_hash]
          @soap_response = context.input[:soap_response]

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateApiAsyncRequestWithResponseValues swoop_invoice is null' unless @swoop_invoice

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateApiAsyncRequestWithResponseValues invoice_changes_hash is null' unless @invoice_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateApiAsyncRequestWithResponseValues api_async_request_changes_hash is null' unless @api_async_request_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::UpdateApiAsyncRequestWithResponseValues soap_response is null' unless @soap_response
        end

        def call
          response_details = @soap_response.http

          @api_async_request_changes_hash[:external_response_body]    = response_details.raw_body
          @api_async_request_changes_hash[:external_response_headers] = response_details.headers.to_json
          @api_async_request_changes_hash[:external_response_code]    = response_details.code

          context.output = {
            swoop_invoice: @swoop_invoice,
            invoice_changes_hash: @invoice_changes_hash,
            api_async_request_changes_hash: @api_async_request_changes_hash,
            soap_response: @soap_response,
          }
        end

      end
    end
  end
end
