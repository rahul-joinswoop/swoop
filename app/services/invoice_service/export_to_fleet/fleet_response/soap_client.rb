# frozen_string_literal: true

module InvoiceService
  module ExportToFleet
    module FleetResponse
      class SoapClient

        def self.instance
          Savon.client do
            wsdl Configuration.fleet_response_invoice_endpoint.url
            env_namespace :soapenv
            namespace_identifier :tem
            convert_request_keys_to :none
            open_timeout Configuration.fleet_response_invoice_endpoint.timeout
            read_timeout Configuration.fleet_response_invoice_endpoint.timeout
            logger       Rails.logger
            log_level    :debug
            log          true
          end
        end

      end
    end
  end
end
