# frozen_string_literal: true

module InvoiceService
  module ExportToFleet
    module FleetResponse
      class PostRequestToSoapEndpoint

        include Interactor
        include Interactor::Swoop

        before do
          Rails.logger.debug "INVOICE(#{context.input[:swoop_invoice].id}) InvoiceService::ExportToFleet::PostRequestToSoapEndpoint called"

          @swoop_invoice          = context.input[:swoop_invoice]
          @invoice_changes_hash   = context.input[:invoice_changes_hash]
          @api_async_request_changes_hash = context.input[:api_async_request_changes_hash]
          @soap_request_values = context.input[:soap_request_values]

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint swoop_invoice is null' unless @swoop_invoice

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint invoice_changes_hash is null' unless @invoice_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint api_async_request_changes_hash is null' unless @api_async_request_changes_hash

          raise ArgumentError, 'InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint soap_request_values is null' unless @soap_request_values
        end

        def call
          soap_response = nil

          begin
            Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint " \
              "about to post soap message to Fleet Response endpoint")

            soap_response = SoapClient.instance.call(:swoop_billing, message: @soap_request_values)
          rescue StandardError => e
            Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint " \
              "swoop_invoice.api_async_request there was an error on Fleet Response SOAP endpoint call")

            Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint " \
              "exception: #{e.message}")

            raise e
          end

          Rails.logger.debug("INVOICE(#{@swoop_invoice.id}) InvoiceService::ExportToFleet::FleetResponse::PostRequestToSoapEndpoint " \
            "response received from Fleet Response endpoint #{soap_response.body}")

          context.output = {
            swoop_invoice: @swoop_invoice,
            invoice_changes_hash: @invoice_changes_hash,
            api_async_request_changes_hash: @api_async_request_changes_hash,
            soap_response: soap_response,
          }
        end

      end
    end
  end
end
