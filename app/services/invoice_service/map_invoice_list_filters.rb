# frozen_string_literal: true

module InvoiceService
  class MapInvoiceListFilters

    include SwoopService
    include Utils

    PAGE_DEFAULT = 1
    PER_PAGE_DEFAULT = 1000

    attr_reader :conditions, :page, :per_page

    # company_id: id from the company who's searching the invoices
    #
    # params: filter params received in the controller
    def initialize(company_id:, params:)
      @company = Company.find(company_id)
      @params  = params
    end

    def execute
      @conditions = parse_conditions
      @page       = parse_page
      @per_page   = parse_per_page

      self
    end

    private

    # this code got moved from API::V1::InvoicesController#parse_query
    def parse_conditions
      conditions = { deleted_at: nil }

      if @params[:states]
        conditions[:state] = @params[:states].split(',')
      end

      if @params[:inline_jobs]
        @inline_jobs = true
      end

      if @params[:provider_id]
        provider = RescueProvider.find_by!(id: @params[:provider_id], company: @company)
        conditions[:sender_id] = provider.provider.id
      elsif @params[:sender_id]
        conditions[:sender_id] = @params[:sender_id]
      end

      if @params[:account_id]
        Rails.logger.debug("InvoicesService::MapInvoiceListSearchParams parse_conditions: " \
          "searching account by account_id #{@params[:account_id]} and company_id: #{@company.id}")

        account = Account.find_by!(id: @params[:account_id], company: @company)

        # if cash call, return recipient type user, else use account
        if account.cash_call?
          conditions[:recipient_type] = "User"
        else
          conditions[:recipient_id] = account.id
          conditions[:recipient_type] = "Account"
        end
      end

      if @params[:client_company_id]
        conditions[:recipient_company_id] = @params[:client_company_id]
      end

      if @params[:alert]
        alert = parse_bool(@params, :alert)
        conditions[:alert] = alert
      end

      Rails.logger.debug "InvoiceService::MapInvoiceListSearchParams parse_conditions: conditions resolved to #{conditions}"

      conditions
    end

    def parse_page
      @params[:page] || PAGE_DEFAULT
    end

    def parse_per_page
      @params[:per_page] || PER_PAGE_DEFAULT
    end

  end
end
