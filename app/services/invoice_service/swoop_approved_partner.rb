# frozen_string_literal: true

# Service to approve Partner Invoice by Swoop.
#
# Business rule: it creates a new Invoice for FleetManaged
module InvoiceService
  class SwoopApprovedPartner < Service

    def initialize(invoice_id:, api_company_id: nil, api_user_id: nil)
      super()

      if invoice_id.blank?
        raise MissingInvoiceError
      end

      @partner_invoice_id = invoice_id

      Rails.logger.debug("INVOICE(#{@partner_invoice_id}) InvoiceService::SwoopApprovedPartner initialized")
    end

    def call
      @partner_invoice = Invoice.find_by!(id: @partner_invoice_id)

      Rails.logger.debug("INVOICE(#{@partner_invoice_id}) InvoiceService::SwoopApprovedPartner call - " \
        "partner invoice found, state #{@partner_invoice.state}")

      raise InvoiceStateTransitionError if !@partner_invoice.may_swoop_approved_partner?

      @partner_invoice.swoop_approved_partner
      @mutated_objects.push @partner_invoice

      Rails.logger.debug("INVOICE(#{@partner_invoice_id}) InvoiceService::SwoopApprovedPartner call - " \
        "partner invoice state changed to #{@partner_invoice.state}")

      create_swoop_outgoing_invoice

      self
    end

    def save_mutated_objects
      ApplicationRecord.transaction do
        super
      end
    end

    private

    def create_swoop_outgoing_invoice
      Rails.logger.debug("INVOICE(#{@partner_invoice_id}) InvoiceService::SwoopApprovedPartner " \
        "create_swoop_outgoing_invoice called")

      swoop_invoice = InvoiceService::NewSwoopInvoice.new(
        partner_invoice_id: @partner_invoice_id
      ).call.swoop_invoice

      @mutated_objects.push swoop_invoice if swoop_invoice.present?
    end

  end
end
