# frozen_string_literal: true

# This service will apply the job.billing_info to the job.invoice.
#
# Billing info is specific to Swoop and is entered by an agent when assigning a
# partner to a job. The agent will enter the partner's billing type (ACH or VCC)
# and an amount if the type is VCC if the partner does not have rates in the system.
#
# In the real world, the agent is generating a payment via a virtual credit card
# when they enter VCC, so we will use this information to establish the job cost
# via a flat rate line item if the partner does not have rates in the system. If there
# is a discrepancy with the rates, then assume that the amount we paid is correct
# and generate a flat rate invoice.
#
# Regardless of whether or not they have rates, since the payment has also been
# made, we'll record it against the invoice.
module InvoiceService
  class ApplyJobBillingInfo

    attr_reader :job, :invoice

    def initialize(job, invoice)
      @job = job
      @invoice = invoice
    end

    def call
      return unless billing_info && billing_info.rescue_company_id == job.rescue_company_id

      invoice.billing_type ||= billing_info.billing_type

      if billing_info.vcc? && billing_info.vcc_amount && billing_info.vcc_amount > 0
        unless job.goa?
          add_vcc_amount_to_invoice
        end
        add_vcc_payment
      end

      if job.goa? && billing_info.vcc? && !goa_amount_applied?
        set_goa_amount
      end
    end

    private

    def billing_info
      job.billing_info
    end

    def add_vcc_amount_to_invoice
      flat_rate_item = nil

      # If the generated invoice matches the VCC amount, then no need to mess with the invoice.
      invoice_total = invoice.current_total_amount
      if invoice_total == billing_info.vcc_amount
        flat_rate_item = invoice.line_items.detect do |item|
          item.net_amount == billing_info.vcc_amount &&
            (item.rate.is_a?(FlatRate) || item.rate.nil?) &&
            item.deleted_at.nil? &&
            !item.marked_for_destruction?
        end
        return unless flat_rate_item
      end

      # Otherwise, we assume that the VCC amount is the canonical amount since it was
      # supposedly just negotiated between the agent and the partner.

      unless flat_rate_item
        # Check if the invoice already has a flat rate item placeholder. If so, we'll update it.
        first_item ||= invoice.line_items.first
        if invoice_total == 0 && invoice.flat_rate? && first_item&.net_amount == 0 && first_item.deleted_at.nil? && !first_item.marked_for_destruction?
          flat_rate_item = first_item
        else
          flat_rate_item = invoice.line_items.build(job: job)
        end
      end

      flat_rate_item.quantity = 1
      flat_rate_item.unit_price = billing_info.vcc_amount
      flat_rate_item.original_quantity = nil
      flat_rate_item.original_unit_price = nil
      flat_rate_item.estimated = false
      flat_rate_item.user_created = false
      flat_rate_item.description = FlatRate::FLAT_RATE

      invoice.rate_type = FlatRate.name

      # Remove everything else from the invoice.
      invoice.line_items.each do |item|
        next if item == flat_rate_item || item.user_created?
        if item.tax?
          item.clear_amounts
        else
          item.deleted_at ||= Time.current
          item.mark_for_destruction if !item.persisted?
        end
      end
    end

    # Apply the vcc amount as a payment to the invoice if it's not already there.
    def add_vcc_payment
      vcc_payment = invoice.payments.detect { |payment| payment.description == Payment::VCC_PAYMENT }
      return if vcc_payment

      payment = invoice.new_payment_or_credit(Payment, {
        total_amount: -billing_info.vcc_amount,
        description: Payment::VCC_PAYMENT,
        payment_method: PaymentMethod::CREDIT_CARD,
        mark_paid_at: Time.current,
      })
      # Ugh. The new_payment_or_credit method instantiates a new object, but doesn't add it
      # to the invoice.payments because it's a bit too abstract.
      invoice.payments.proxy_association.add_to_target(payment)

      add_payment_to_job_history(payment)
    end

    # Set the GOA amount if the invoice has a single, new GOA flat rate line item.
    def set_goa_amount
      return if billing_info.goa_amount.blank?

      goa_line_items = invoice.line_items.select { |item| !item.tax? && item.rate&.goa? }
      line_item = goa_line_items.first if goa_line_items.size == 1
      return unless line_item && line_item.rate_type == "FlatRate"

      line_item.unit_price = billing_info.goa_amount
      line_item.quantity = 1

      add_goa_invoice_to_job_history
    end

    def goa_amount_applied?
      job.history_items.any? { |item| item.title == HistoryItem::INVOICE_EDITED_GOA_COST }
    end

    def add_payment_to_job_history(payment)
      CreateHistoryItem.call(
        job: job,
        target: payment,
        user: billing_info.agent,
        event: HistoryItem::INVOICE_EDITED_VCC_PAYMENT,
      )
    end

    def add_goa_invoice_to_job_history
      CreateHistoryItem.call(
        job: job,
        target: invoice,
        user: billing_info.goa_agent,
        event: HistoryItem::INVOICE_EDITED_GOA_COST,
      )
    end

  end
end
