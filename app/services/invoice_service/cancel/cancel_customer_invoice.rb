# frozen_string_literal: true

module InvoiceService
  module Cancel
    class CancelCustomerInvoice

      def initialize(job)
        Rails.logger.debug "CCI #{job.status}"
        unless [Job::STATUS_GOA, Job::STATUS_CANCELED, Job::STATUS_DELETED].include?(job.status)
          raise ArgumentError, "Asked to cancel job #{job.id} when not in GOA/Canceled/Deleted"
        end
        @job = job
        @previous_status = @job.audit_job_statuses.order(created_at: :desc).limit(2)&.last&.job_status&.db_name
        Rails.logger.debug "CancelCustomerInvoice called with JOB:#{job.id}, previous_state:#{@previous_status}"
      end

      def call
        @job.fleet_customer_live_invoices.each do |invoice|
          Rails.logger.debug "CancelCustomerInvoice zeroing invoice"

          invoice.zero_invoice(@job)
          calculate_fee(invoice)
          invoice.save!
        end
      end

      private

      def calculate_fee(invoice)
        if (@job.status == Job::STATUS_GOA) || enroute_or_later
          Rails.logger.debug "CancelCustomerInvoice Curerntly GOA or Job was canceled in Enroute or later"
          rate = find_rate
          add_fee(invoice, rate.flat) if rate
        end
      end

      def find_rate
        Rate.inherit(@job.fleet_company_id,
                     @job.fleet_company.customer_account,
                     @job.site_id,
                     nil,
                     ServiceCode.find_by(name: get_code_name(@job.status)),
                     @job.invoice_vehicle_category_id,
                     false)
      end

      def get_code_name(status)
        Rails.logger.debug "looking up #{status}"
        {
          Job::STATUS_CANCELED => ServiceCode::LATE_CANCEL_FEE,
          Job::STATUS_GOA => ServiceCode::GOA_FEE,
        }[status]
      end

      def enroute_or_later
        Job.enroute_or_later.map { |a| a.parameterize.underscore }.include?(@previous_status)
      end

      def add_fee(invoice, fee)
        Rails.logger.debug "CancelCustomerInvoice Adding fee"
        invoice.line_items.create!(description: get_code_name(@job.status), net_amount: fee, job: @job)
      end

    end
  end
end
