# frozen_string_literal: true

module InvoiceService
  module Cancel
    class CancelInvoiceForJob < Service

      include LoggableJob

      attr_reader :job

      def initialize(job)
        @job = job
        log :debug, "CancelInvoiceForJob called status:#{@job.status}", job: @job.id
      end

      def call
        if @job.invoice
          InvoiceService::Cancel::CancelPartnerInvoice.new(@job).call
          @job.invoice.save!
        end

        return if @job.fleet_customer_live_invoices.empty?

        InvoiceService::Cancel::CancelCustomerInvoice.new(@job).call
      end

    end
  end
end
