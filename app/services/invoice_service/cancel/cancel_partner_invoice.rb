# frozen_string_literal: true

module InvoiceService
  module Cancel
    class CancelPartnerInvoice

      def initialize(job)
        @job = job
      end

      def call
        @job.invoice.zero_invoice(@job)
      end

    end
  end
end
