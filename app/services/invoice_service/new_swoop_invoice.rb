# frozen_string_literal: true

# This service instantiates a new Swoop Invoice.
# Swoop Invoices are the ones that Swoop uses to charge FleetManaged.
#
# It's just a new instance and it's supposed to be saved by the caller.
module InvoiceService
  class NewSwoopInvoice

    include SwoopService

    attr_reader :swoop_invoice

    def initialize(partner_invoice_id:)
      @partner_invoice_id = partner_invoice_id
      @swoop_invoice = nil
    end

    def execute
      @partner_invoice = Invoice.find(@partner_invoice_id)

      return unless can_create_swoop_invoice?

      Rails.logger.debug("INVOICE(#{@partner_invoice_id}) InvoiceService::CreateSwoopInvoice " \
        "execute - searching for account by #{account_attributes}")

      account = Account.find_by(account_attributes) || Account.new(account_attributes)

      Rails.logger.debug("INVOICE(#{@partner_invoice_id}) InvoiceService::CreateSwoopInvoice " \
        "execute - account found #{account.id}")

      @swoop_invoice = Invoice.new(
        sender: Company.swoop,
        recipient: account,
        currency: Company.swoop&.currency,
        job: @partner_invoice.job,
        incoming_invoice: @partner_invoice,
        recipient_company: @partner_invoice.job.fleet_company
      )

      # Creating and adding Tax line_item without a Rate set. (Swoop invoices do not have calc based on Rates on BE)
      @swoop_invoice.line_items << InvoicingLineItem.new(
        description: 'Tax',
        currency: @swoop_invoice.currency
      )

      @swoop_invoice.swoop_new

      self
    end

    private

    def can_create_swoop_invoice?
      can_create = job_owned_by_fleet_managed? && job_doesnt_have_active_swoop_invoice?

      Rails.logger.debug("INVOICE(#{@partner_invoice_id}) InvoiceService::CreateSwoopInvoice " \
        "execute - can_create_swoop_invoice? #{can_create}")

      can_create
    end

    def account_attributes
      @account_attributes ||= {
        company: Company.swoop,
        client_company: @partner_invoice.job.fleet_company,
        name: @partner_invoice.job.fleet_company.name,
      }
    end

    def job_owned_by_fleet_managed?
      @job_owned_by_fleet_managed ||=
        @partner_invoice.job.fleet_company.fleet_managed? && !@partner_invoice.job.fleet_company.issc_motor_club?

      Rails.logger.debug("INVOICE(#{@partner_invoice.id}) InvoiceService::CreateSwoopInvoice checking if job owned by a fleet managed company " \
        "job_owned_by_fleet_managed? #{@job_owned_by_fleet_managed}")

      @job_owned_by_fleet_managed
    end

    def job_doesnt_have_active_swoop_invoice?
      @blank_swoop_invoice_for_this_job ||= @partner_invoice.find_active_fleet_managed_invoice.blank?

      Rails.logger.debug("INVOICE(#{@partner_invoice.id}) InvoiceService::CreateSwoopInvoice checking if the swoop invoice for this job does not exist - " \
        "blank_swoop_invoice_for_this_job #{@blank_swoop_invoice_for_this_job}")

      @blank_swoop_invoice_for_this_job
    end

  end
end
