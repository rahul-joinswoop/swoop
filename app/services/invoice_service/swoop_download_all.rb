# frozen_string_literal: true

# # #
#
# Service to trigger downloading all Invoices by Swoop.
#
# Required attributes:
#   report_params: the request params received by the UI. It requires
#   report_params[:client_company_id] key, otherwise will fail.
#
#   api_company_id: api_company_id, should always be Swoop
#   (only root controller calls this up to now)
#
#   api_user_id: user who's executing this service, the async_request and the report will need it
#
#   sender_alias: either :swoop or :partner
#   if :swoop, it will process fleet invoices whose sender is Swoop
#      The invoices for the client_company_id with state swoop_approved_fleet will be fetched and
#      moved to swoop_sending_fleet.
#
#   if :partner, it will process partner invoices whose sender is a Partner
#      The invoices for the client_company_id with state swoop_approved_partner will be fetched and
#      moved to swoop_sending_partner.
#
# A WS message will be kicked containing a bulk set of invoices whose state got changed to either
# swoop_sending_fleet or swoop_sending_partner, depending on sender_alias param.
#
# It will create an async_request to log the entire flow.
# This is the only readable attribute after the call execution.
#
# Then it will schedule an async worker to generate the report and push it to FE.
#
# For more details @see GenerateSwoopFleetInvoiceReportWorker or
# @GenerateSwoopPartnerInvoiceReportWorker
#
module InvoiceService
  class SwoopDownloadAll

    include SwoopService

    attr_reader :async_request, :invoice_ids

    CONSTS = {
      swoop: {
        system: API::AsyncRequest::SWOOP_FLEET_INVOICE_DOWNLOAD,
        current_invoice_state: Invoice::SWOOP_APPROVED_FLEET,
        new_invoice_state: Invoice::SWOOP_SENDING_FLEET,
        worker: GenerateSwoopFleetInvoiceReportWorker,
      },
      partner: {
        system: API::AsyncRequest::SWOOP_PARTNER_INVOICE_DOWNLOAD,
        current_invoice_state: Invoice::SWOOP_APPROVED_PARTNER,
        new_invoice_state: Invoice::SWOOP_DOWNLOADING_PARTNER,
        worker: GenerateSwoopPartnerInvoiceReportWorker,
      },
    }.freeze

    def initialize(report_params:, api_company_id:, api_user_id:, sender_alias:)
      @report_params  = report_params
      @api_company_id = api_company_id
      @api_user_id    = api_user_id
      @sender_alias   = sender_alias.to_sym

      raise ArgumentError, "invalid report_params" if @report_params.blank?

      if @report_params.dig(:client_company_id).blank?
        raise ArgumentError, "invalid client_company_id"
      end

      raise ArgumentError, "invalid sender_id" if @report_params.dig(:sender_id).blank?

      raise ArgumentError, "invalid api_company_id" if @api_company_id.blank?
      raise ArgumentError, "invalid api_user_id" if @api_user_id.blank?

      if @sender_alias.blank? || (@sender_alias != :swoop && @sender_alias != :partner)
        raise ArgumentError, "invalid sender_alias"
      end

      Rails.logger.debug(
        "#{self.class.name} called for client_company_id: #{@report_params[:client_company_id]}"
      )
    end

    def execute
      @async_request = create_async_request!

      @invoice_ids = fetch_invoice_ids

      if @invoice_ids.present?
        Rails.logger.debug(
          "InvoiceService::SwoopDownloadAll#execute invoices found: #{@invoice_ids}"
        )

        move_invoices_state_to_new_state!

        publish_partial_invoices_through_ws

        generate_swoop_invoices_report_asynchronously
      else
        Rails.logger.debug("InvoiceService::SwoopDownloadAll#execute no invoices found")
      end

      self
    end

    private

    def create_async_request!
      @async_request ||= API::AsyncRequest.create!(
        company_id: @api_company_id,
        user_id: @api_user_id,
        request: {
          method: 'SwoopDownloadAll',
          params: @report_params,
        },
        system: CONSTS[@sender_alias][:system]
      )
    end

    def fetch_invoice_ids
      Invoice.not_deleted.where(
        state: CONSTS[@sender_alias][:current_invoice_state],
        sender_id: @report_params[:sender_id],
        recipient_company_id: @report_params[:client_company_id],
      ).pluck(:id)
    end

    # # #
    #
    # http://guides.rubyonrails.org/active_record_callbacks.html#skipping-callbacks
    #
    # invoices#update_all will skip callbacks, and that's right here since we don't
    # need any callbacks to be kicked, and mostly we don't want each invoice to be
    # pushed by WS on this (WS update msg is kicked by a callback on Publishable concern).
    #
    def move_invoices_state_to_new_state!
      new_state = CONSTS[@sender_alias][:new_invoice_state]

      Rails.logger.debug(
        "#{self.class.name}#move_invoices_state_to_new_state! will push invoices to #{new_state}"
      )

      Invoice.where(id: @invoice_ids).update_all(state: new_state, updated_at: Time.now)
    end

    # we refetch the invoices with current state
    def invoices_with_state
      Invoice.where(id: @invoice_ids).select(:id, :state)
    end

    def invoices_with_state_array
      invoices_with_state.map do |invoice|
        { id: invoice.id, state: invoice.state }
      end
    end

    def publish_partial_invoices_through_ws
      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'PartialObjectArray',
          target_data: {
            partial_invoices: invoices_with_state_array,
          },
          company: Company.find(@api_company_id),
        }
      )
    end

    def generate_swoop_invoices_report_asynchronously
      worker = CONSTS[@sender_alias][:worker]

      worker.perform_async(
        @report_params,
        @api_user_id,
        @invoice_ids,
        @async_request.id
      )
    end

  end
end
