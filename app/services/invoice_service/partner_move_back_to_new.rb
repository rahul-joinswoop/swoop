# frozen_string_literal: true

# Service to Move an Invoice back to new by Partner.
module InvoiceService
  class PartnerMoveBackToNew < Service

    def initialize(invoice_id:, api_company_id: nil, api_user_id: nil)
      super()

      if invoice_id.blank?
        raise MissingInvoiceError
      end

      @partner_invoice_id = invoice_id
    end

    def call
      @partner_invoice = Invoice.find_by_id(@partner_invoice_id)

      raise InvoiceStateTransitionError if !@partner_invoice.may_partner_move_back_to_new?

      @partner_invoice.partner_move_back_to_new
      @partner_invoice.reset_paid

      @mutated_objects.push @partner_invoice

      self
    end

    def save_mutated_objects
      ApplicationRecord.transaction do
        super
      end
    end

  end
end
