# frozen_string_literal: true

class EtaCalculator

  def initialize(job_id)
    @job_id = job_id
  end

  # Find the oldest ETA of the oldest (max depth 10) parent
  def call
    @initial_job = Job.find(@job_id)
    return nil if @initial_job.blank?

    # Use the job itself if it has no parents
    @job = oldest_parent.blank? ? @initial_job : oldest_parent

    return nil if earliest_job_eta.blank?

    # This is meant to be the time that was estimated for arrival
    # minus the time that the estimation was made.
    # re: https://swoopme.atlassian.net/browse/ENG-5196
    # re: the picture on https://swoopme.atlassian.net/browse/ENG-5328
    ((earliest_job_eta.time - earliest_job_eta.created_at) / 60)
  end

  private

  def earliest_job_eta
    EarliestJobEtaQuery.call(@job.id)
  end

  def oldest_parent
    JobAncestorsWithStatusesQuery
      .call(@initial_job.id, statuses: [Job::STATUS_ASSIGNED, Job::STATUS_UNASSIGNED])
      .last
  end

end
