# frozen_string_literal: true

module Agero::Auth
  # Generates OAuth token from the Agero Roadside Assistance API
  class GenerateOauthToken

    include Interactor

    def call
      authorization_code = Agero::ApiRequest::AuthorizationCode.get['authorizationCode']

      context.response = Agero::ApiRequest::AccessToken.get(authorization_code)

      Rails.logger.debug "OneRoad::GenerateOauthToken: OAuth token generated"

      context
    end

  end
end
