# frozen_string_literal: true

# Agero::Rsc::ResetCompany.new(26).call
module Agero
  module Rsc
    class ResetCompany

      def initialize(company)
        @company = company
      end

      def call
        @company.api_access_tokens.each do |token|
          token.deleted_at = Time.now
          token.save!
        end

        @company.sites.each do |site|
          site.issc_locations.where(fleet_company: Company.agero).each do |location|
            location.deleted_at = Time.now
            location.save!
          end
        end

        @company.isscs.where(deleted_at: nil).each do |issc|
          issc.issc_location.deleted_at = Time.now
          issc.issc_location.save!

          issc.deleted_at = Time.now
          issc.save!
        end

        SiteFacilityMap.where(company: @company,
                              fleet_company: Company.agero,
                              deleted_at: nil).each do |mapping|
          mapping.deleted_at = Time.now
          mapping.save!
        end
      end

    end
  end
end
