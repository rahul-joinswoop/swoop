# frozen_string_literal: true

module Agero::Rsc
  module AgeroCanceled
    class Service

      include Agero::Rsc::EventConcern

      before do
        if job.nil?
          raise(ArgumentError, "job from dispatchid #{@event[:dispatchRequestNumber]} is nil")
        end
      end

      def call
        current_job_status = job.human_status

        return if current_job_status == JobStatus::NAMES[:CANCELED]
        return if current_job_status == JobStatus::NAMES[:GOA]

        job.issc_canceled
        job.issc_dispatch_request.requester_canceled

        job.save!

        context.output = { job: job }
      end

    end
  end
end
