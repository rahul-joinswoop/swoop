# frozen_string_literal: true

# This receives a job_id and pushes the job acceptance to RSC.
module Agero::Rsc
  class AcceptDispatch

    include LoggableJob
    include Interactor

    before do
      @job = context.job
    end

    def call
      dispatch_request_number = @job.issc_dispatch_request.dispatchid

      if @job.bta&.duration.nil?
        raise ArgumentError, log_msg("does not have a bta.duration", job: @job.id)
      end

      if @job.bta.duration >= 90
        reason ||= Agero::Rsc::Data.job_eta_explanation_code(@job.eta_explanation_id)
      end

      rsc_api.accept_dispatch(dispatch_request_number, eta: @job.bta.duration, reason: reason)
    end

    private

    def rsc_api
      @rsc_api ||= Agero::Rsc::API.for_job(@job.id)
    end

  end
end
