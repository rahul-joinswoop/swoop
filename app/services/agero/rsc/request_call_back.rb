# frozen_string_literal: true

# This receives a job, a dispatch_number and a secondary_number,
# updates the job accordingly and schedules a RequestCallBackWorker
# to post it to RSC.
module Agero
  module Rsc
    class RequestCallBack

      include SwoopService

      def initialize(job:, api_user:, call_back_hash:)
        @job = job
        @api_user = api_user
        @call_back_hash = call_back_hash

        raise ArgumentError, '@job is blank' if @job.blank?
        raise ArgumentError, '@api_user is blank' if @api_user.blank?
      end

      def call
        @job.partner_dispatcher = @api_user
        @job.answer_by = nil
        @job.partner_request_callback

        @job.save!

        Agero::Rsc::RequestCallBackWorker.perform_async(@job.id, @call_back_hash)
      end

    end
  end
end
