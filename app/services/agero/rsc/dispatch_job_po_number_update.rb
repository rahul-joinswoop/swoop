# frozen_string_literal: true

module Agero::Rsc
  class DispatchJobPoNumberUpdate

    include Interactor

    before do
      @job = context.job
    end

    def call
      dispatch_request_number = @job.issc_dispatch_request.dispatchid

      dispatch = rsc_api.get_dispatch(dispatch_request_number)
      if dispatch['poNumber'].present? && @job.po_number.blank?
        @job.update!(po_number: dispatch['poNumber'])
      else
        context.errors = "Missing RSC response PO Number: #{dispatch['poNumber']} or Swoop PO already populated: #{@job.po_number}"
        context.fail!
      end
    end

    private

    def rsc_api
      @rsc_api ||= Agero::Rsc::API.for_job(@job.id)
    end

  end
end
