# frozen_string_literal: true

module Agero
  module Rsc
    module DispatchableSites
      class Update

        def initialize(rescue_company, fleet_company, site_args)
          Rails.logger.debug "Agero::Rsc::DispatchableSites::Update called #{rescue_company.try(:name)}, #{fleet_company.try(:name)}, #{site_args}"
          @rescue_company = rescue_company
          @fleet_company = fleet_company
          @sites_args = site_args
        end

        def call
          @sites_args.each do |site|
            Rails.logger.debug "Rsc::DispatchableSites::Update dealing with site:#{site}"
            BaseModel.transaction do
              mapping = SiteFacilityMap.find_by(fleet_company: @fleet_company,
                                                company: @rescue_company,
                                                deleted_at: nil,
                                                site_id: site[:site_id])

              Rails.logger.debug "Mapping:#{mapping.inspect}"

              if mapping && mapping.facility_id != site[:locationid]
                disassociate_site_and_location(mapping)
              end

              if site[:locationid]
                mapping = SiteFacilityMap.find_by(fleet_company: @fleet_company,
                                                  company: @rescue_company,
                                                  deleted_at: nil,
                                                  facility_id: site[:locationid])

                if mapping && mapping.site_id != site[:site_id]
                  disassociate_site_and_location(mapping)
                  associate_site_and_location(mapping, site[:site_id])
                end
              end

              rescue_provider_enablement(site[:site_id], site[:enabled])
            end
          end
        end

        def disassociate_site_and_location(mapping)
          Rails.logger.debug "Rsc::DispatchableSites::Update disassociate_site_and_location #{mapping}"
          mapping.company.isscs.joins(:issc_location).where(issc_locations: { location: mapping.facility }).each do |issc|
            issc.site = nil
            issc.save!
          end
          mapping.site_id = nil
          mapping.save!
        end

        def associate_site_and_location(mapping, site_id)
          Rails.logger.debug "Rsc::DispatchableSites::Update associate_site_and_location #{[mapping, site_id]}"
          mapping.site_id = site_id
          mapping.save!
          mapping.company.isscs.joins(:issc_location).where(issc_locations: { location: mapping.facility }).each do |issc|
            issc.site_id = site_id
            issc.save!
          end
        end

        def rescue_provider_enablement(site_id, enabled)
          Rails.logger.debug "rescue_provider_enablement site:#{site_id}, #{enabled}"
          rp = RescueProvider.find_by(company: @fleet_company, provider: @rescue_company, site_id: site_id)

          if !rp
            rp = AddClientCompanyToPartner.new(@rescue_company, @fleet_company, Site.find(site_id)).call
            rp.save!
          end

          if enabled
            if rp.deleted_at
              rp.deleted_at = nil
            end
          else
            rp.deleted_at = Time.now
          end

          rp.save! if rp.deleted_at_changed?
        end

      end
    end
  end
end
