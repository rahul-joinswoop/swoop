# frozen_string_literal: true

module Agero
  module Rsc
    class SignOutVendorIds

      include Sidekiq::Worker
      sidekiq_options queue: 'issc', retry: 5

      def perform(vendors)
        Rails.logger.debug("Agero::Rsc::SignOutVendorIds - called #{vendors}")
        vendors.each do |vendorid, isscs|
          # Find the latest access token for this vendorid
          token = APIAccessToken.token_for_vendor(isscs.first.company, vendorid)

          if token

            if token.isscs.sort != isscs.sort
              Rails.logger.debug "Agero::Rsc::SignOutVendorIds - asked to logout #{isscs.sort.map(&:id)} but some are staying in: #{token.isscs.sort.map(&:id)} "
              next
            end

            token.isscs.each do |issc|
              Rails.logger.debug("Agero::Rsc::SignOutVendorIds - logging out #{issc.id}")
              issc.provider_logout
              issc.save!
            end

            # Throws Agero::Rsc::Error if error and retries in sidekiq
            token.sign_out
            token.isscs.each do |issc|
              Rails.logger.debug "moving #{issc} to logged_out"
              issc.provider_logged_out
              issc.save!
            end
          else
            Rails.logger.debug "Agero::Rsc::SignOutVendorIds No token found"
          end
        end
      end

    end
  end
end
