# frozen_string_literal: true

# Handle job approved notifications from RSC. This message is used for several purposes:
#
# If the job is submitted, then this is Agero's approval of the ETA and we will now have access to driver details
#
# If the job is a phone transcript, then this is Agero sending us the approved job details.
#
# Otherwise, any updates to pickup or drop off location and comments are sent with this message.
module Agero::Rsc
  module JobApproved
    class Service

      include Agero::Rsc::EventConcern

      before do
        if job.nil? && !phone_transcript?
          raise(ArgumentError, "job from dispatchid #{@event[:dispatchRequestNumber]} is nil")
        end
      end

      def call
        approved_job = job

        # Load dispatch details before opening the transaction
        dispatch = dispatch_details
        Job.transaction do
          if approved_job.nil? && phone_transcript?
            approved_job = create_phone_transcript_job!(dispatch)
          elsif approved_job.submitted? || approved_job.assigned? || approved_job.expired? # ENG-7747
            approve_job!(approved_job, dispatch)
          else
            update_job!(approved_job, dispatch)
          end
        end

        context.output = { job: approved_job }
      end

      private

      def approve_job!(job, dispatch)
        update_job_details(job, dispatch)
        job.issc_accepted
        job.issc_dispatch_request.requester_accepted
        job.save!
        job
      end

      def update_job!(job, dispatch)
        update_job_details(job, dispatch)
        job.save!
        job
      end

      def create_phone_transcript_job!(dispatch)
        job = FleetMotorClubJob.new
        job.account = agero_account
        job.fleet_company = agero_account.client_company
        job.issc_dispatch_request = phone_transcript_issc_request(dispatch)

        Agero::Rsc::JobBuilder.new(job).build(dispatch)
        job.hack_create_ajs(JobStatus::NAMES[:INITIAL])
        job.save!

        job.move_to_pending
        job.issc_assign
        job.assign_rescue_company((issc.site || agero_account.company.hq))
        job.save!

        job.partner_accepted
        job.issc_accepted
        job.issc_dispatch_request.requester_accepted
        job.save!

        job
      end

      def update_job_details(job, dispatch)
        Agero::Rsc::JobBuilder.new(job).update!(dispatch)
      end

      def phone_transcript?
        Agero::Rsc::Data.club_job_type(event["dispatchSource"]) == "Phone Transcript"
      end

      def phone_transcript_issc_request(dispatch)
        IsscDispatchRequest.new(
          dispatchid: dispatch["dispatchRequestNumber"],
          club_job_type: Agero::Rsc::Data.club_job_type(dispatch["dispatchSource"]),
          status: IsscDispatchRequest::NEW_REQUEST,
          issc: issc,
        )
      end

      def agero_account
        @agero_account ||= Account.not_deleted.find_by!(
          company: api_access_token.company,
          client_company: Company.agero,
        )
      end

    end
  end
end
