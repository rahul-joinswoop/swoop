# frozen_string_literal: true

module Agero
  module Rsc
    class SignInVendorIds

      def perform(vendors)
        vendors.each do |vendorid, isscs|
          # Find the latest access token for this vendorid
          token = APIAccessToken.token_for_vendor(isscs.first.company, vendorid)
          if token
            # Iterate over them setting logging in state and sending to FE
            token.isscs.each do |issc|
              Rails.logger.debug("Agero::Rsc::SignInVendorIds::perform logging in issc.id:#{issc.id}")
              issc.provider_login
              issc.last_login_attempted_at = Time.now
              issc.save!
            end
            # This returns vendorid, username and role, which we already have
            # Throws Agero::Rsc::Error if error and retries in sidekiq
            token.sign_in
            # Iterate over isscs setting loggedin state and sending to FE
            token.isscs.each do |issc|
              Rails.logger.debug "Agero::Rsc::SignInVendorIds moving #{issc} to logged_in"
              issc.logged_in_at = Time.now
              issc.provider_logged_in
              issc.save!
            end
          else
            Rails.logger.debug "Agero::Rsc::SignInVendorIds - no token found"
          end
        end
      end

    end
  end
end
