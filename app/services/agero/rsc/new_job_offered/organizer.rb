# frozen_string_literal: true

# This Organizes calls the services needed to:
#
# GetDispatchDetails - get the NewJobOffered details on RSC
# BuildFleetMotorClubJobFromDispatchDetails - builds a Job (not save)
# MoveJobToAssignedAndSave - moves the Job.status to assigned and consequentially save it
#
module Agero::Rsc
  module NewJobOffered
    class Organizer

      include Interactor::Organizer
      include Interactor::Swoop

      organize(
        BuildFleetMotorClubJobFromDispatchDetails,
        MoveJobToAssignedAndSave
      )

    end
  end
end
