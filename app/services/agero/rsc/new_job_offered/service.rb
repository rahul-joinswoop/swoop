# frozen_string_literal: true

# This service will take the NewJobOffered message from RSC
# and create the respective FleetMotorClubJob into Swoop db.
#
# The NewJobOffered message is expected to come from RSC with:
#   - a vendorId -> which maps to a Partner (RescueCompany) in Swoop
#   - a locationId -> which maps to a Partner's site in Swoop
#
# If successfully processed, the result will be an assigned FleetMotorClubJob
# in the Partner Dashboard ready to be accepted or rejected.
#
# On StandardError, we make sure the Job will be canceled. (TODO should we do this?)
module Agero::Rsc
  module NewJobOffered
    class Service

      include LoggableJob
      include Agero::Rsc::EventConcern

      def call
        if already_have_request?
          Rails.logger.warn("Duplicate RSC New Offer: #{@event.inspect}")
          return
        end

        @new_fleet_motor_club_job = Agero::Rsc::NewJobOffered::Organizer.call(
          input: {
            callback_token: @callback_token,
            event: @event,
          }
        ).output[:new_fleet_motor_club_job]

        @new_fleet_motor_club_job.assign_rescue_company(@new_fleet_motor_club_job.site)
        @new_fleet_motor_club_job.save!

        TrackEvent.call(
          event: "RSC Job Offered",
          target: @new_fleet_motor_club_job,
        )

        log :debug,
            "Agero::Rsc::Service finished creating Swoop job" \
            "for RSC event[notificationEventId](#{notification_event_id})",
            job: @new_fleet_motor_club_job.id

        update_audit_rows_with_job_id!

        kick_async_workers_after_job_created

        context.output = { new_fleet_motor_club_job: @new_fleet_motor_club_job }
      rescue StandardError => e
        move_job_to_canceled_if_created

        raise e
      end

      def already_have_request?
        dispatch_request_number.present? && issc.dispatch_requests.where(dispatchid: dispatch_request_number).exists?
      end

      private

      def update_audit_rows_with_job_id!
        AuditIssc.where(
          dispatchid: @new_fleet_motor_club_job.issc_dispatch_request.dispatchid
        ).update_all(job_id: @new_fleet_motor_club_job.id)
      end

      # Async workers to kick after job has been committed.
      #
      # ResolveLatLngIfBlank - will add lat/lng to service_location or drop_location
      # in case any of these doesn't have it.
      def kick_async_workers_after_job_created
        ::API::Jobs::ResolveLatLngIfBlank.new(job_id: @new_fleet_motor_club_job.id).call

        email_sender = JobStatusEmailSender.new(
          @new_fleet_motor_club_job.id, 'JobStatusEmail::Created'
        )
        Delayed::Job.enqueue email_sender
        Agero::Rsc::NewJobOffered::ExpiredTimerWorker.perform_at(@new_fleet_motor_club_job.answer_by,
                                                                 @new_fleet_motor_club_job.id)
        JobService::ReverseGeocodeIfNecessary.new(@new_fleet_motor_club_job).call
      end

      def move_job_to_canceled_if_created
        Rails.logger.error(
          "Agero::Rsc::Service could not create the Job" \
          "for RSC event[notificationEventId](#{notification_event_id})"
        )

        if @new_fleet_motor_club_job&.persisted?
          @new_fleet_motor_club_job.hack_create_ajs(JobStatus::NAMES[:CANCELED])

          log :error,
              "pushing job to canceled" \
              "for RSC event[notificationEventId](#{notification_event_id})",
              job: @new_fleet_motor_club_job.id

          @new_fleet_motor_club_job.canceled! # will move job to Canceled and save it.
        end
      end

    end
  end
end
