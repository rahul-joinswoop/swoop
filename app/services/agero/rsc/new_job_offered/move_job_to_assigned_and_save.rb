# frozen_string_literal: true

# Takes an instance of FleetMotorClub job, moves its to assign status
# and consequentially saves it, as job.move_to_pending call pushes the status
# (and respective record AJS) to 'created' and it will force-commit the Job.
module Agero::Rsc
  module NewJobOffered
    class MoveJobToAssignedAndSave

      include Agero::Rsc::EventConcern

      before do
        @new_fleet_motor_club_job = context.input[:new_fleet_motor_club_job]

        if @new_fleet_motor_club_job.blank?
          raise(ArgumentError, "new_fleet_motor_club_job is blank")
        end
      end

      def call
        # ISSC uses DispatchIsscJob and DispatchJob services for a new dispatch.
        # @see IsscDispatchNew
        #
        # I'm avoiding using those for RSC, as to give RSC
        # a fresh and hopefully a more easily maintainable code.
        #
        # After all only a few lines are required from these services for RSC
        # (and even for ISSC I think).
        #
        # At this point, next lines are the only ones that we'll need from the
        # DispatchIsscJob and DispatchJob services.
        @new_fleet_motor_club_job.hack_create_ajs(JobStatus::NAMES[:INITIAL]) # only builds the AJS

        @new_fleet_motor_club_job.move_to_pending
        @new_fleet_motor_club_job.issc_assign # only moves job to assign, it doesn't save the job

        Rails.logger.debug "MoveJobToAssignedAndSave  AJS: #{@new_fleet_motor_club_job.audit_job_statuses.inspect}"
        @new_fleet_motor_club_job.save!

        # see ENG-9743 for why this is necessary - once it's fixed the after_commit callbacks should
        # start working correctly.
        GraphQL::JobStatusChangedWorker.perform_async(@new_fleet_motor_club_job.id, {
          status: [nil, @new_fleet_motor_club_job.status],
          rescue_company_id: [nil, @new_fleet_motor_club_job.rescue_company_id],
        })

        # same problem here, have to trigger this manually
        Notification::EtaRequestedWorker.perform_async @new_fleet_motor_club_job.id

        context.output = { new_fleet_motor_club_job: @new_fleet_motor_club_job }
      end

    end
  end
end
