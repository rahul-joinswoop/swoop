# frozen_string_literal: true

# This service builds the Swoop Job. It should be called just after fetching
# the DispatchDetails by Agero::Rsc::NewJobOffered::GetDispatchDetails.
#
# Note that this service does not save the Job, it only instantiates it.
module Agero::Rsc
  module NewJobOffered
    class BuildFleetMotorClubJobFromDispatchDetails

      include Agero::Rsc::EventConcern

      # Length of time until an RSC dispatch expires.
      EXPIRE_SECONDS = ENV.fetch("RSC_EXPIRE_SECONDS", "240").to_i.seconds

      # Number of seconds to expire a job early if it hasn't been accepted.
      # This is to prevent race conditions with RSC expiring a job at the same time Swoop is accepting it.
      WIGGLE_ROOM_SECONDS = ENV.fetch("RSC_WIGGLE_ROOM_SECONDS", "10").to_i.seconds

      ANSWER_BY = EXPIRE_SECONDS - WIGGLE_ROOM_SECONDS

      # Agero requires an explanation if the ETA is longer than this.
      MAX_ETA = ENV.fetch("RSC_MAX_ETA_MINUTES", "90").to_i

      before do
        raise(ArgumentError, "dispatch_details is blank") if dispatch_details.blank?
      end

      def call
        context.output = { new_fleet_motor_club_job: new_fleet_motor_club_job }
      end

      private

      def new_fleet_motor_club_job
        job = FleetMotorClubJob.new
        Agero::Rsc::JobBuilder.new(job).build(dispatch_details)
        job.account = agero_account
        job.fleet_company = agero_account.client_company
        job.answer_by = calculate_answer_by
        job.issc_dispatch_request = issc_dispatch_request_from_dispatch_details
        job.rescue_company = agero_account.company
        job.site = (issc.site || job.rescue_company&.hq)
        job
      end

      def agero_account
        @agero_account ||= Account.not_deleted.find_by!(
          company: api_access_token.company,
          client_company: Company.agero,
        )
      end

      def issc_dispatch_request_from_dispatch_details
        dispatch_request = issc.dispatch_requests.find_by(dispatchid: dispatch_details["dispatchRequestNumber"])
        dispatch_request ||= IsscDispatchRequest.new(
          dispatchid: dispatch_details["dispatchRequestNumber"],
          club_job_type: Agero::Rsc::Data.club_job_type(dispatch_details["dispatchSource"]),
          status: IsscDispatchRequest::NEW_REQUEST,
          max_eta: MAX_ETA, # minutes (it's an int value on DB)
          issc: issc,
        )
        dispatch_request
      end

      def received_time
        dispatch_details["receivedTime"]
      end

      def calculate_answer_by
        new_status = dispatch_details["statusHistory"]&.detect { |s| s["code"] == "2" }

        base_time = if new_status.present?
                      DateTime.parse(new_status["statusTime"].to_s)
                    elsif received_time.present?
                      DateTime.parse(received_time.to_s)
                    else
                      Time.zone.now
                    end

        base_time + ANSWER_BY
      end

    end
  end
end
