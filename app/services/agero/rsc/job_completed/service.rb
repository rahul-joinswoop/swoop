# frozen_string_literal: true

# Handle job completed notifications from RSC. This message is used for several purposes:
#
module Agero::Rsc
  module JobCompleted
    class Service

      include Agero::Rsc::EventConcern

      before do
        if job.nil?
          raise(ArgumentError, "job from dispatchid #{@event[:dispatchRequestNumber]} is nil")
        end
      end

      def call
        # Load dispatch details before opening the transaction
        dispatch = dispatch_details
        Job.transaction do
          update_job!(job, dispatch)
        end

        context.output = { job: job }
      end

      private

      def update_job!(job, dispatch)
        update_job_details(job, dispatch)
        job.status = Job::STATUS_COMPLETED
        job.issc_dispatch_request.status = Job::STATUS_COMPLETED

        job.save!
        job
      end

      def update_job_details(job, dispatch)
        Agero::Rsc::JobBuilder.new(job).update!(dispatch)
      end

    end
  end
end
