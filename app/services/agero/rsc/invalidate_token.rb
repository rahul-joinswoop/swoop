# frozen_string_literal: true

# Service class for invalidating tokens when the Agero API responds with a 401
# or 403 HTTP status. This will change the status of the token from connected,
# as well as send an email to the company notifying them that their account has
# been disconnected.
class Agero::Rsc::InvalidateToken

  include Interactor
  include Utils

  before do
    @access_token_string = context.access_token_string
    Rails.logger.debug "Agero::Rsc::InvalidateToken @access_token_string: #{@access_token_string}"
  end

  def call
    access_token = APIAccessToken.find_by(access_token: @access_token_string)
    access_token.update!(connected: false)

    return if access_token.company.accounting_email.nil?

    email_args = {
      template_name: "agero_account_disconnected",
      to: access_token.company.accounting_email,
      from: SWOOP_OPERATIONS_EMAIL,
      subject: "Agero Account Disconnected",
      locals: { :@vendor_id => access_token.vendorid },
    }

    SystemMailer.send_mail(
      email_args,
      nil,
      nil,
      access_token.company,
      "Agero Account Disconnected",
      access_token.company
    ).deliver_now
  end

end
