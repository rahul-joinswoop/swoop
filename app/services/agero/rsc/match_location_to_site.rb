# frozen_string_literal: true

# Match an Agero facility to a Swoop Site on ZIP code.
class Agero::Rsc::MatchLocationToSite

  include Interactor

  before do
    @company = context.company
    @facility = context.facility
  end

  def call
    return if @facility["zipcode"].blank?

    site = Site.joins(:location)
      .where(company_id: @company.id, deleted_at: nil, locations: { zip: @facility["zipcode"] })
      .order(:id)
      .first

    context.result = site
  end

end
