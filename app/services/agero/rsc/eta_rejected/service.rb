# frozen_string_literal: true

module Agero::Rsc
  module EtaRejected
    class Service

      include Agero::Rsc::EventConcern

      before do
        if job.nil?
          raise(ArgumentError, "job from dispatchid #{@event[:dispatchRequestNumber]} is nil")
        end
      end

      def call
        if job.done?
          Rails.logger.warn("Job #{job.id} ETA rejected by RSC after already marked #{job.status}")
        else
          job.issc_rejected
          job.issc_dispatch_request.requester_rejected
          job.save!
        end

        context.output = { job: job }
      end

    end
  end
end
