# frozen_string_literal: true

module Agero::RSA
  # Maps a Swoop location type to an Agero-consumable format
  class MapLocationType

    include Interactor

    WORK_DRIVEWAYS = [
      'Auto Body Shop',
      'Business',
      'Gated Community',
      'Impound Lot',
      'Point of Interest',
      'Residence',
      'Storage Facility',
      'Tow Company',
    ].freeze

    WORK_STREETS = ['Intersection', 'Local Roadside'].freeze

    before do
      @location_type = context.location_type
    end

    def call
      context.result = parse_location_type
    end

    protected

    def parse_location_type
      case @location_type
      when *WORK_DRIVEWAYS
        'Residence/Work-Driveway'
      when 'Blocking Traffic'
        'Roadside-Street'
      when 'Highway'
        'Roadside-Highway'
      when *WORK_STREETS
        'Residence/Work-Street'
      when 'Low Clearance'
        'Residence/Work-ParkingGarage'
      when 'Parking Garage'
        'GeneralParkingGarage'
      when 'Parking Lot'
        'GeneralParkingLot'
      end
    end

  end
end
