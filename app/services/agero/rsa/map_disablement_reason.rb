# frozen_string_literal: true

module Agero::RSA
  class MapDisablementReason

    include Interactor

    TOW_NOT_LEAKING = "Tow (Not leaking fuel)"
    UNKNOWN         = "Unknown Problem"

    SYMPTOM_MAPPINGS = {
      Symptom::CUSTOMER_UNSAFE => TOW_NOT_LEAKING,
      Symptom::DEAD_BATTERY => "Jump Start (Did not stall while driving)",
      Symptom::MECHANICAL_ISSUE => TOW_NOT_LEAKING,
      Symptom::OUT_OF_FUEL => "Out of Fuel (Cost of fuel not covered)",
      Symptom::RECOVERY => "Vehicle Stuck",
      Symptom::UNKNOWN => UNKNOWN,
    }.freeze

    # Tire related symptom mappings
    MULTIPLE_FLATS   = "Multiple Flat Tires"
    HAS_GOOD_SPARE   = "One Flat Tire - Good spare"
    LACKS_GOOD_SPARE = TOW_NOT_LEAKING

    # Keys locked symptom mappings
    LOCKOUT_WITH_KEYS    = "Lockout - Keys in car"
    LOCKOUT_WITHOUT_KEYS = "Lockout - Keys broken/missing"

    before do
      @job = context.job
    end

    def call
      context.result = fetch_disablement_reason
    end

    private

    def fetch_disablement_reason
      reason = SYMPTOM_MAPPINGS[@job.symptom.name]
      return reason if reason

      case @job.symptom.name
      when Symptom::FLAT_TIRE
        fetch_flat_tire_reason
      when Symptom::LOCKED_OUT
        fetch_locked_out_reason
      else
        UNKNOWN
      end
    rescue => e
      Rollbar.error(e)

      UNKNOWN
    end

    def fetch_flat_tire_reason
      flat_tire_answer = QuestionServices::FindAnswer.call(
        job: @job, question: Question::WHICH_TIRE
      ).result

      if flat_tire_answer.answer == Answer::DAMAGED_TIRE_MULTIPLE
        return MULTIPLE_FLATS
      end

      spare_answer = QuestionServices::FindAnswer.call(
        job: @job, question: Question::HAVE_A_SPARE
      ).result

      if spare_answer.answer == Answer::YES
        HAS_GOOD_SPARE
      else
        LACKS_GOOD_SPARE
      end
    end

    def fetch_locked_out_reason
      locked_out_answer = QuestionServices::FindAnswer.call(
        job: @job, question: Question::WHERE_ARE_THE_KEYS
      ).result

      if [Answer::KEYS_TRUNK_INACCESSIBLE, Answer::UNKNOWN].include?(locked_out_answer.answer)
        LOCKOUT_WITHOUT_KEYS
      else
        LOCKOUT_WITH_KEYS
      end
    end

  end
end
