# frozen_string_literal: true

# Helper class for setting up SlackChannel feeds.
module Slack
  class Config

    def initialize(company_name, bot_name, channel_name)
      @company_name = company_name
      @company_tag = company_tag(company_name)
      if Rails.env.development?
        warn "WARNING: Print Mode (won't create the channels, just prints the code to do it)"
      end
      @bot_name = bot_name
      @channel_name = channel_name
    end

    def create_client
      create_channels([
        ['towdestination', 'dispatch-status-towde%'],
        ['summary', 'summary'],
        ['stored', 'dispatch-status-store%'],
        ['sms track-click', 'sms-track-click'],
        ['sms track', 'sms-track'],
        ['sms location', 'sms-location'],
        ['sms confirmation', 'sms-confirmation'],
        ['sms branded-review-link', 'sms-branded-review-link'],
        ['review', 'review'],
        ['released', 'dispatch-status-relea%'],
        ['onsite', 'dispatch-status-onsit%'],
        ['goa', 'dispatch-status-goa%'],
        ['enroute', 'dispatch-status-enrou%'],
        ['Create', 'dispatch-create'],
        ['completed', 'dispatch-status-compl%'],
        ['canceled', 'dispatch-status-cance%'],
        ['accepted', 'dispatch-status-accep%'],
      ])
    end

    def create_ops
      create_channels([["Ops", '%', 'review-link']])
    end

    def create_reviews
      create_channels([["review", 'review']])
    end

    private

    def company_tag(company_name)
      company_name.gsub(/\s+/, '-').downcase
    end

    def create_channels(pairs)
      pairs.each do |name, tag, excludes|
        attributes = {
          name: "#{@company_name} #{name}",
          bot_name: @bot_name,
          tag: "#{@company_tag}-#{tag}",
          channel: @channel_name,
          active: true,
          excludes: excludes,
        }
        slack_channel = SlackChannel.find_by(tag: attributes[:tag], channel: attributes[:channel])
        if Rails.env.development?
          STDOUT.puts "SlackChannel.create!(#{attributes.inspect})"
        else
          SlackChannel.create!(attributes) unless slack_channel
        end
      end
    end

  end
end
