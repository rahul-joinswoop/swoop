# frozen_string_literal: true

class JobStatusEmailSelector

  def initialize(job)
    @job = job
  end

  def emailable?
    !!email_class
  end

  def email_class_name
    email_class&.name
  end

  private

  def email_class
    if scheduled_email?
      JobStatusEmail::Scheduled
    elsif status_email?
      case @job.current_job_status_id
      when JobStatus::ACCEPTED
        JobStatusEmail::Accepted
      when JobStatus::ETA_EXTENDED
        JobStatusEmail::EtaExtended
      when JobStatus::CANCELED
        JobStatusEmail::Canceled
      when JobStatus::COMPLETED
        JobStatusEmail::Completed
      when JobStatus::CREATED
        JobStatusEmail::Created
      when JobStatus::GOA
        JobStatusEmail::Goa
      when JobStatus::EN_ROUTE
        JobStatusEmail::EnRoute
      when JobStatus::ON_SITE
        JobStatusEmail::OnSite
      when JobStatus::REASSIGNED
        JobStatusEmail::Reassigned
      when JobStatus::TOW_DESTINATION
        JobStatusEmail::TowDestination
      when JobStatus::TOWING
        JobStatusEmail::Towing
      end
    end
  end

  def status_change
    @status_change ||= @job.saved_changes["status"]&.[](1)
  end

  def scheduled_for_change
    @scheduled_for_change ||= @job.saved_changes["scheduled_for"]
  end

  def scheduled_email?
    (scheduled_for_change && scheduled_for_change&.[](0).present?) ||
    (
      @job.scheduled_for &&
      status_change &&
      JobStatus::SCHEDULED_FOR_EMAILABLE.include?(@job.current_job_status_id)
    )
  end

  def status_email?
    (status_change && JobStatus::EMAILABLE.include?(@job.current_job_status_id))
  end

end
