# frozen_string_literal: true

# This is primarily only used by RSC jobs.
#
# This will schedule Agero::Rsc::RequestMoreTimeWorker to send the
# more time request to Agero.
#
# It will return an AsyncRequest to keep track of the async flow.
# Agero::Rsc::RequestMoreTimeWorker will push the AsyncRequest by WS to
# FE so it can treat the UI behaviour either in success or error.
module DigitalDispatch
  class RequestMoreTime

    include SwoopService

    attr_reader :async_request

    def initialize(job_id:, api_company_id:, api_user_id:)
      @job_id = job_id
      @api_company_id = api_company_id
      @api_user_id = api_user_id
    end

    def execute
      create_api_async_request!

      schedule_request_more_time_worker

      self
    end

    # we create the async_quest for auditing and tracking the flow
    def create_api_async_request!
      @async_request = API::AsyncRequest.create!(
        company_id: @api_company_id,
        user_id: @api_user_id,
        request: {},
        target_id: @job_id,
        system: API::AsyncRequest::RSC_REQUEST_MORE_TIME
      )
    end

    def schedule_request_more_time_worker
      Agero::Rsc::RequestMoreTimeWorker.perform_async(@job_id, @async_request.id)
    end

  end
end
