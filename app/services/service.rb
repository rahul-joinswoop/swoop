# frozen_string_literal: true

class Service

  ## Abstract class to hold base behaviour for services

  def initialize
    @mutated_objects = []
  end

  def save_mutated_objects
    Rails.logger.debug "Saving mutated_objects: #{@mutated_objects}"
    @mutated_objects.map { |o| o.save! }
  end

end
