# frozen_string_literal: true

class IsscDispatchCanceled < Service

  def initialize(event)
    @event = event
    @dispatchid = event[:DispatchID]
  end

  def call
    raise ISSC::DispatchJobNotFound unless @dispatchid

    Job.transaction do
      job = Job.joins(:issc_dispatch_request).where('issc_dispatch_requests.dispatchid = ?', @dispatchid).take

      raise ISSC::DispatchJobNotFound unless job

      Rails.logger.debug "#{job.inspect}"

      job.issc_canceled
      job.issc_dispatch_request.requester_canceled

      job
    end
  end

end
