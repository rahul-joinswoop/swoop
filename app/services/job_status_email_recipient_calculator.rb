# frozen_string_literal: true

class JobStatusEmailRecipientCalculator

  def initialize(job, job_status_id_email_preference)
    @job = job
    @job_status_id_email_preference = job_status_id_email_preference
  end

  def execute
    sql = []

    if reassigned?
      [@job.rescue_company]
    else
      generate_frequency_all_sql(sql)
      generate_frequency_dispatcher_sql(sql)

      recipients = find_recipient_users(sql)
      recipients + find_recipient_companies
    end
  end

  private

  def reassigned?
    @job_status_id_email_preference == JobStatus::REASSIGNED && @job.rescue_company&.live? && !@job.fleet_manual
  end

  def generate_frequency_all_sql(sql)
    all_sql = []

    if @job.rescue_company_id
      all_sql << "company_id = #{@job.rescue_company_id}"
    end

    if @job.fleet_company_id
      all_sql << "company_id = #{@job.fleet_company_id}"
    end

    if @job.fleet_managed_job?
      all_sql << "company_id = #{Company.swoop_id}"
    end

    if all_sql.any?
      sql << "(job_status_email_frequency = 'all' AND (#{all_sql.join(" OR ")}))"
    end
  end

  def generate_frequency_dispatcher_sql(sql)
    dispatcher_sql = []

    if @job.partner_dispatcher_id
      dispatcher_sql << "users.id = #{@job.partner_dispatcher_id}"
    end

    if @job.fleet_dispatcher_id
      dispatcher_sql << "users.id = #{@job.fleet_dispatcher_id}"
    end

    if @job.root_dispatcher_id
      dispatcher_sql << "users.id = #{@job.root_dispatcher_id}"
    end

    if @job.customized_dispatcher? && @job_status_id_email_preference == JobStatus::CREATED && @job.user_id
      dispatcher_sql << "users.id = #{@job.user_id}"
    end

    if dispatcher_sql.any?
      sql << "(job_status_email_frequency = 'dispatcher' AND (#{dispatcher_sql.join(" OR ")}))"
    end
  end

  def find_recipient_users(sql)
    if sql.empty?
      []
    else
      arel = User
        .emailable
        .joins(:company)
        .joins(:job_status_email_preferences)
        .merge(Company.live_or_non_rescue)
        .merge(JobStatusEmailPreference.by_job_status_id(@job_status_id_email_preference))
        .where(sql.join(" OR "))

      if @job.fleet_site_id
        arel = arel.where("
          ((SELECT COUNT(*) FROM user_sites WHERE user_id = users.id) = 0)
          OR ((SELECT COUNT(*) FROM user_sites WHERE user_id = users.id AND site_id = ?) > 0)
        ", @job.fleet_site_id)
      else
        arel = arel.where("(SELECT COUNT(*) FROM user_sites WHERE user_id = users.id) = 0")
      end

      arel
    end
  end

  def find_recipient_companies
    companies = []

    if JobStatus::COMPANY_EMAILABLE.include?(@job_status_id_email_preference)
      companies << @job.rescue_company if @job.rescue_company&.live?
    end

    companies
  end

end
