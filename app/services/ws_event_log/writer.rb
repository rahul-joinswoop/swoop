# frozen_string_literal: true

module WsEventLog
  class Writer

    def initialize(ws_event:, max_events: nil)
      @redis = AsyncRedis.instance
      @ws_event = ws_event
      @max_events = max_events || WsEventLog.max_events
    end

    def write
      # Rails.logger.debug "WsEventLog::Writer write called for #{@ws_event.key}"
      EM.schedule do
        zadd = @redis.zadd(@ws_event.key, @ws_event.score, @ws_event.to_json)
        zadd.errback { |e| WsEventLog.handle_error(self.class, :zadd, e) }
        zadd.callback do
          # Rails.logger.debug "WsEventLog::Writer zadd callback for #{@ws_event.key}"
          EM.schedule do
            zcard = @redis.zcard(@ws_event.key)
            zcard.errback { |e| WsEventLog.handle_error(self.class, :zcard, e) }
            zcard.callback do |count|
              # Rails.logger.debug "WsEventLog::Writer zcard callback for #{@ws_event.key} , count:#{count}"
              idx = count - @max_events
              if idx > 0
                EM.schedule do
                  zremrangebyrank = @redis.zremrangebyrank(@ws_event.key, 0, idx - 1)
                  zremrangebyrank.errback { |e| WsEventLog.handle_error(self.class, :zremrangebyrank, e) }
                  # zremrangebyrank.callback do |rem_ret|
                  #  Rails.logger.debug "WsEventLog::Writer zremrangebyrank callback for #{@ws_event.key} return:#{rem_ret}"
                  # end
                end
              end
            end
          end
        end
      end
    end

    #     def _old_write
    #       zadd = @redis.zadd(@ws_event.key, @ws_event.score, @ws_event.to_json)
    #       zadd.errback {|e| WsEventLog.handle_error(self.class, :zadd, e) }
    #       zadd.callback do
    #         Rails.logger.debug "WsEventLog::Writer zadd callback"
    #         zcard = @redis.zcard(@ws_event.key)
    #         zcard.errback {|e| WsEventLog.handle_error(self.class, :zcard, e) }
    #         zcard.callback do |count|
    #           Rails.logger.debug "WsEventLog::Writer zcard callback #{count}"
    #           idx = count - @max_events
    #           Rails.logger.debug "WsEventLog::Writer idx: #{idx}"
    #           if idx > 0
    #             zremrangebyrank = @redis.zremrangebyrank(@ws_event.key, 0, idx - 1)
    #             zremrangebyrank.errback { |e| WsEventLog.handle_error(self.class, :zremrangebyrank, e) }
    #             zremrangebyrank.callback do |rem_ret|
    #               Rails.logger.debug "WsEventLog::Writer zremrangebyrank:#{rem_ret}"
    #             end
    #           end
    #         end
    #       end
    #     end

  end
end
