# frozen_string_literal: true

module WsEventLog
  class Reader

    INF = "+inf"

    def initialize(target_type:, target_id:, last_updated_at:)
      @key = WsEventLog.generate_key({
        target_type: target_type,
        target_id: target_id,
      })
      @score = WsEventLog.generate_score(last_updated_at)
    end

    def count
      RedisClient.client(:default).zcount(@key, "(#{@score}", INF)
    end

    # ws_events = WsEvent.where("created_at > ?",last_updated_at).where(target_type:target_type,target_id:target_id).order(:created_at)
    def events
      RedisClient.client(:default).zrangebyscore(@key, "(#{@score}", INF).map do |json|
        parsed_json = JSON.parse(json).symbolize_keys
        WsEvent.new(parsed_json)
      end
    end

  end
end
