# frozen_string_literal: true

class CreateInvoiceForJob < Service

  include LoggableJob

  attr_reader :job

  def initialize(job)
    @job = job
  end

  def call
    log :debug, "CreateInvoiceForJob", invoice: job.id
    log :debug, "CreateInvoiceForJob", job: job.id
    recipient, recipient_company = JobGetInvoiceRecipientAndCompanyService.new(@job).call

    if !@job.rescue_company
      log :debug, "Rescue Company Not Set when trying to create invoice", job: @job.id
      raise ArgumentError, log_msg("Rescue Company Not Set when trying to create invoice", invoice: @job.id)
    end
    if !recipient
      log :debug, "recipient Not Set when trying to create invoice", job: @job.id
      raise ArgumentError, log_msg("recipient Not Set when trying to create invoice", invoice: @job.id)
    end

    if @job.invoice && @job.invoice.deleted_at.nil?
      log :debug, "Asked to create invoice when one exists", job: @job.id
      raise ArgumentError, log_msg("Asked to create invoice when one exists", invoice: @job.id)
    end

    invoice = Invoice.new(
      sender: @job.rescue_company,
      recipient: recipient,
      currency: @job.rescue_company&.currency,
      job: @job,
      recipient_company: recipient_company,
      description: @job.driver_notes,
      version: Invoice::VERSION
    )

    invoice.state = 'created'

    invoice.rate_type = nil
    invoice.version = Invoice::VERSION
    invoice
  end

end
