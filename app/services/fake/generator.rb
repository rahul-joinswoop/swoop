# frozen_string_literal: true

module Fake
  class Generator

    SUPPORTED_OBJECTS = ['Account', 'FleetJob', 'Provider', 'RescueJob', 'RescueVehicle', 'Site', 'User', 'RescueCompany'].freeze

    def initialize(company, object_name, count)
      @company = company
      @object_name = object_name
      @count = count.to_i

      raise ArgumentError, "Fake::Generator - Count too large #{@count}" unless @count <= 100000
      raise ArgumentError, "Fake::Generator - Unsupported object" unless SUPPORTED_OBJECTS.include?(@object_name)
      raise ArgumentError, "Fake::Generator - Unsupported company type #{@company.class}" unless valid_company_type? || company_creation?
    end

    def call
      Rails.logger.debug "Fake::Generator - Creating #{@count} #{@object_name} for #{@company.name}"

      load_objects

      fn = method("fake_#{@object_name.underscore}".to_sym)

      Rails.logger.debug "fn:#{fn}"
      @count.times do
        fn.call
      rescue => e
        Rails.logger.debug e
        Rails.logger.debug e.backtrace.join("\n")
      end

      true
    end

    def fake_account
      Account.create!(name: Faker::Company.name,
                      company: @company)
    end

    def fake_rescue_vehicle
      RescueVehicle.create!(name: Faker::Lorem.word.capitalize,
                            company: @company,
                            vehicle_category: @vehicle_categories.sample)
    end

    def fake_rescue_job
      raise ArgumentError, "Fake::Generator - tried to create a rescue job for a non rescue-company #{@company.name}" unless @company.rescue?

      make, model = random_vehicle_make_model
      service = @services.sample
      # service=ServiceCode.find_by(name:'Storage')

      args =
        {
          service: service.name,
          first_name: Faker::Name.first_name,
          last_name: Faker::Name.last_name,
          phone: fake_phone,
          make: make,
          model: model,
        }

      if service.name == 'Storage'
        site = random_site
        args[:site_id] = site.id
        args[:drop_location] = {}
        args[:drop_location][:site_id] = site.id
      end

      API::Jobs::CreateRescue.new(
        api_company: @company,
        api_user: @company.users.last,
        params: args,
        secondary_tow_params: nil
      ).call
    end

    def fake_user
      email = fake_email
      user = User.create!(full_name: Faker::Name.name,
                          company: @company,
                          email: email,
                          username: email,
                          phone: fake_phone,
                          password: fake_password)
      roles = []
      case @company
      when RescueCompany
        roles << [:admin, :driver, :dispatcher, :answering_service].sample
        roles << :rescue
      when FleetCompany
        roles << [:admin, :dispatcher].sample
        roles << :fleet
      else
        Rails.logger.debug "Unknown class:#{@company.class.inspect}"
      end

      roles.map { |role| user.add_role(role) }
      user
    end

    def fake_site
      args = {

        "name": Faker::HitchhikersGuideToTheGalaxy.location,
        "location": fake_location_instance,
        "phone": fake_phone,
        "dispatchable": random_boolean,
        "always_open": random_boolean,
        "open_time": "09:00",
        "close_time": "17:00",
        "open_time_sat": "9:00",
        "close_time_sat": "17:00",
        "open_time_sun": "9:00",
        "close_time_sun": "17:00",

      }
      site_service_create = SiteService::Create.new(
        site_params: args,
        site_type: @company.rescue? ? PartnerSite : FleetSite,
        company: @company
      )
      ApplicationRecord.transaction do
        site_service_create.call.site
      end
    end

    def fake_fleet_job
      raise ArgumentError, "Fake::Generator - tried to create a fleet job for a non fleet company #{@company.name}" unless @company.fleet?

      make, model = random_vehicle_make_model

      fleet_dispatcher = @company.users.last

      # This is modeled on required fields for Tesla
      args = {
        "make": make,
        "send_sms_to_pickup": false,
        "fleet_dispatcher_id": fleet_dispatcher,
        "department_id": "1",
        "customer": {
          "full_name": Faker::Name.name,
          "phone": fake_phone,
        },
        "customer_type_id": random_customer_type&.id,
        "model": model,
        "color": Faker::Color.color_name,
        "odometer": Random.rand(100000),
        "vin": fake_vin,
        "service_location": fake_location,
        "symptom_id": random_symptom.id,
        "service_code_id": random_service.id,
        "drop_location": fake_location,
        "pickup_contact": {
          "full_name": Faker::Name.name,
          "phone": fake_phone,
        },
        "will_store": false,
        "fleet_site_id": nil,
      }
      API::Jobs::CreateFleet.new(
        api_company: @company,
        api_user: fleet_dispatcher,
        params: args
      ).call
    end

    def fake_rescue_company
      args = {
        "type": "RescueCompany",
        "name": Faker::Company.name,
        "phone": fake_phone,
        "dispatch_email": fake_email,
        "accounting_email": fake_email,
        "live": random_boolean,
      }
      location_attributes = fake_location

      Rails.logger.debug "XXX Creating company with args:#{args}"

      ApplicationRecord.transaction do
        @company = CompanyService::Create.new(
          company_attributes: args,
          location_attributes: location_attributes,
          feature_ids: []
        ).call.company
      end
    end

    def fake_provider
      rcs = RescueCompany.order("RANDOM()").limit(10)
      rcs.each do |rc|
        if RescueProvider.where(company: @company, provider: rc).count == 0
          RescueProvider.create!(company: @company, provider: rc, site: rc.hq)
          return
        end
      end
    end

    private

    def valid_company_type?
      [RescueCompany, FleetCompany].include? @company.class
    end

    def company_creation?
      @company.nil? && ['RescueCompany'].include?(@object_name)
    end

    def load_objects
      vehicle_category_names = ['Flatbed', 'Light Duty', 'Medium Duty', 'Rotator', 'Medium Duty Combo', 'Lowboy', 'Carrier']
      @vehicle_categories = VehicleCategory.where(name: vehicle_category_names).to_a
      @vehicle_categories << nil

      @services = @company.service_codes.to_a if @company

      @vehicle_count = VehicleMake.count
    end

    def random_service
      @services.sample
    end

    def random_boolean
      [true, false].sample
    end

    def fake_phone
      # "#{Random.rand(100000)}555#{Faker::PhoneNumber.subscriber_number}"
      10.times do
        phone = Faker::PhoneNumber.cell_phone
        if Phonelib.valid?(Phonelib.parse(phone).e164)
          return phone
        end
      end
    end

    def fake_email
      Faker::Internet.safe_email
    end

    def fake_password
      Faker::Internet.password(min_length: 10)
    end

    def random_vehicle_make_model
      model = VehicleModel.where.not(vehicle_make: nil).order("RANDOM()").limit(1).first
      [model.vehicle_make.name, model.name]
    end

    def random_site
      @company.sites.order("RANDOM()").limit(1).first
    end

    def random_customer_type
      cct = CompaniesCustomerType.where(company: @company).order("RANDOM()").limit(1).first
      cct&.customer_type
    end

    def random_symptom
      cs = CompanySymptom.where(company: @company).order("RANDOM()").limit(1).first
      cs.symptom
    end

    def fake_location
      {
        "lat": Faker::Address.latitude,
        "lng": Faker::Address.longitude,
        #                  "place_id": "ChIJWd40ezB2j4ARHM0LPhM0VMg",
        "address": "#{Faker::Address.street_address}, #{Faker::Address.city}, #{Faker::Address.state_abbr} #{Faker::Address.zip_code}",
        "exact": true,
      }
    end

    def fake_location_instance
      Location.create
    end

    def fake_vin
      Faker::Vehicle.vin
    end

  end
end
