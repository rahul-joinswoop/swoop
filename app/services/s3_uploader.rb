# frozen_string_literal: true

class S3Uploader

  def initialize(s3_obj)
    @s3_obj = s3_obj
  end

  def upload(str)
    @s3_obj.put(body: str)
  end

end
