# frozen_string_literal: true

class InvoicePdfGenerator

  def initialize(invoice, pdf_generator: WickedPdf)
    @invoice = invoice
    @pdf_generator = pdf_generator.new
  end

  def generate
    @pdf_generator.pdf_from_string(render_to_string, {
      orientation: 'Portrait',
      page_size: 'Letter',
      footer: {
        font_name: 'Open Sans',
        font_size: 8,
        left: 'Powered by Swoop',
        right: '([page] of [topage])',
      },
    })
  end

  private

  def render_to_string
    ApplicationController.new.render_to_string({
      layout: false,
      locals: {
        invoice: @invoice,
        job: @invoice.job,
        scale_factor: 4.5,
      },
      margin: {
        top: 10,
        right: 10,
        bottom: 35,
        left: 10,
      },
      template: 'pdfs/invoice.pdf.erb',
    })
  end

end
