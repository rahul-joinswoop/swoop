# frozen_string_literal: true

module AsyncRequest
  module TreatError

    private

    def treat_error(exception, attribute, errors, company_to_receive_msg)
      Rails.logger.info(
        "#{self.class.name} error occured, and will be treated - #{exception.message}"
      )

      publish_async_request_with_error(attribute, errors, company_to_receive_msg)
    end

    def publish_async_request_with_error(attribute, errors, company_to_receive_msg)
      Rails.logger.debug(
        "#{self.class.name} " \
        "there was an error on the charge process for the " \
        "async_request(#{@async_request_id}), " \
        "about to publish the error throught WS"
      )

      Publishers::GenericWebsocketMessage.call(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: @async_request_id,
            error: {
              attribute => errors,
            },
          },
          company: company_to_receive_msg,
        }
      )
    end

  end
end
