# frozen_string_literal: true

##
# Helper that allows multiple services to be executed.
#
# The service must implement one of the following methods:
# :execute and :after_execution (this last is optional)
class ServiceRunner

  def initialize
    @services = []
  end

  def <<(service)
    @services << service
  end

  def call_all
    ApplicationRecord.transaction do
      @services.compact.each(&:execute) if @services.present?
    end

    if @services.present?
      @services.compact.each do |service|
        service.after_execution if service.respond_to? :after_execution
      end
    end
  end

end
