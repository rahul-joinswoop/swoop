# frozen_string_literal: true

# To be used when an account gets added to a partner, so partner will
# receive any custom services from client company
module ServiceCodeServices
  class AddCustomServiceCodesToPartner

    include SwoopService

    attr_reader :services_added

    def initialize(rescue_company:, client_company:)
      @rescue_company = rescue_company
      @client_company = client_company
    end

    def execute
      custom_services_ids_from_client =
        @client_company.service_codes.where(is_standard: [nil, false]).pluck(:id)

      @services_added = ServiceCodeServices::Change.new(
        api_company: @rescue_company,
        standard_services_ids_to_add: custom_services_ids_from_client
      ).call

      self
    end

  end
end
