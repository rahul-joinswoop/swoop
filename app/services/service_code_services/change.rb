# frozen_string_literal: true

# Used when partner or fleet (inHouse or managed) is editing their services or addons
# Used by Swoop (SuperCompany) to edit their addons only
module ServiceCodeServices
  class Change

    include SwoopService

    attr_reader :service_codes_added, :custom_services_added, :custom_services_removed

    def initialize(api_company:, standard_services_ids_to_add: nil, services_ids_to_remove: nil,
                   custom_service_names_to_add: nil, addon: [false, nil], should_publish_company: true)

      @api_company = api_company
      @standard_services_ids_to_add = standard_services_ids_to_add || []
      @services_ids_to_remove       = services_ids_to_remove || []
      @custom_service_names_to_add  = custom_service_names_to_add || []
      @addon = addon
      @should_publish_company = should_publish_company

      @standard_services_added = []
      @standard_services_removed = []
      @custom_services_added = []
      @custom_services_removed = []

      @service_codes_added = []

      Rails.logger.debug("ServiceCodeServices::Change params: api_company.id = #{@api_company.id}, " \
        "standard_services_ids_to_add = #{@standard_services_ids_to_add}, " \
        "services_ids_to_remove = #{@services_ids_to_remove}, " \
        "custom_service_names_to_add = #{@custom_service_names_to_add}, " \
        "addon = #{@addon}.")
    end

    def execute
      add_standard_service_codes

      remove_service_codes

      add_custom_service_codes

      @service_codes_added = (@custom_services_added + @standard_services_added)

      self
    end

    def after_execution
      # should_publish_company will be false if called by UpdateCustomServiceCodesToPartnersWorker
      if @should_publish_company
        # this publishes current api_company through WS
        PublishCompanyWorker.perform_after_commit(@api_company.id, self.class.name)
      end

      if @api_company.fleet? || @api_company.super?
        if @custom_services_added.present? || @custom_services_removed.present?
          # this updates partners services with fleets custom ones and
          # publishes WS updates the partners
          UpdateCustomServiceCodesToPartnersWorker.perform_async(
            @api_company.id,
            @custom_services_added.map(&:id),
            @custom_services_removed.map(&:id),
            addon_as_boolean_value
          )
        elsif @standard_services_added.present? || @standard_services_removed.present?
          # this publishes fleet or swoop standard service_codes changes WS to their partners
          # In this case partners don't need their own service_codes list to be updated.
          PublishServiceCodeIdsToPartnersWorker.perform_async(@api_company.id, addon_as_boolean_value, false)
        end
      end
    end

    private

    def add_standard_service_codes
      standard_services_ids_already_added =
        @api_company.service_codes.where(id: @standard_services_ids_to_add).pluck(:id)

      (@standard_services_ids_to_add - standard_services_ids_already_added).each do |service_code_id|
        service_code = ServiceCode.find(service_code_id)

        if service_code
          @standard_services_added << service_code
          @api_company.service_codes << service_code
        end
      end
    end

    def remove_service_codes
      @services_ids_to_remove.each do |service_code_id|
        service_code = ServiceCode.find(service_code_id)

        if service_code
          if service_code.custom?
            @custom_services_removed << service_code
          else
            @standard_services_removed << service_code
          end

          @api_company.service_codes.destroy(service_code)
        end
      end
    end

    def add_custom_service_codes
      @custom_service_names_to_add.each do |custom_service_name|
        name = custom_service_name[:name]

        custom_service_code = ServiceCode.where(addon: @addon).where('lower(name) = ?', name.downcase).first

        if !custom_service_code
          custom_service_code = ServiceCode.create!(name: name, addon: addon_as_boolean_value)
        end

        if !@api_company.service_codes.include? custom_service_code
          @custom_services_added << custom_service_code
          @api_company.service_codes << custom_service_code
        end
      end
    end

    def addon_as_boolean_value
      return @addon if !@addon.instance_of?(Array)

      @addon.any? ? true : false
    end

  end
end
