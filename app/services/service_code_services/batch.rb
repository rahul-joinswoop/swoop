# frozen_string_literal: true

module ServiceCodeServices
  class Batch

    def initialize(api_company:, service_code_ids:, addon: true)
      @api_company      = api_company
      @service_code_ids = service_code_ids.split(',')

      Rails.logger.debug "ServiceCode batch load params: company.id = #{@api_company.id}, service_code_ids = #{@service_code_ids}"
    end

    def call
      ServiceCode.where(
        id: @service_code_ids,
      ).distinct
    end

  end
end
