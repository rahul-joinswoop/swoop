# frozen_string_literal: true

module ServiceCodeServices
  class BatchByName

    def initialize(api_company:, service_code_names:, addon:)
      @api_company = api_company
      @service_code_names = service_code_names.split(',')
      @addon = addon

      Rails.logger.debug "ServiceCode batch by name load params: company.id = #{@api_company.id}, " \
        "service_code_names = #{@service_code_names}, addon = #{@addon}"
    end

    def call
      ServiceCode.where(
        name: @service_code_names,
        addon: addon_as_boolean_value
      ).distinct
    end

    private

    def addon_as_boolean_value
      return @addon if !@addon.instance_of?(Array)

      @addon.any? ? true : false
    end

  end
end
