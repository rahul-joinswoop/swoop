# frozen_string_literal: true

module ServiceCodeServices
  class FetchAllStandard

    def initialize(api_company:, addon: [false, nil])
      @api_company = api_company
      @addon       = addon # [false, nil] for services, true for addons
    end

    def call
      standard_service_codes = fetch_service_codes

      if !@api_company.features.exists?(name: Feature::STORAGE)
        storage_service = ServiceCode.find_by_name(ServiceCode::STORAGE)

        return standard_service_codes - [storage_service]
      end

      standard_service_codes
    end

    private

    def fetch_service_codes
      if @api_company.instance_of? RescueCompany
        standard_service_codes_for_partners
      elsif @api_company.instance_of? FleetCompany
        standard_service_codes
      elsif @api_company.instance_of? SuperCompany
        standard_service_codes
      else
        raise ArgumentError, "Company type not allowed: #{@api_company.class}"
      end
    end

    def standard_service_codes_for_partners
      custom_services_codes_from_accounts + standard_service_codes
    end

    def custom_services_codes_from_accounts
      client_company_ids =
        Account
          .where(company: @api_company, deleted_at: nil)
          .where.not(client_company_id: nil).pluck(:client_company_id)

      ServiceCode.custom_items.joins(:companies)
        .where(companies: { id: client_company_ids }, addon: @addon).distinct
    end

    def standard_service_codes
      ServiceCode.standard_items.where(addon: @addon)
    end

  end
end
