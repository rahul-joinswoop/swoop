# frozen_string_literal: true

module Users
  # Determine the roles a user is allowed to assign
  class FetchAllowedRoles

    ALLOWED_ROLES = ['admin', 'dispatcher', 'driver', 'answering_service', 'swoop_dispatcher'].freeze
    NOT_ALLOWED_ROLES_ERROR = "#{self.class.name}: Insecurely allowing user to assign roles that they don't have"

    def self.call(api_user: nil, api_roles: nil, new_roles: nil)
      api_roles = if !api_roles.nil?
                    Set[*api_roles.map(&:to_s)]
                  elsif api_user.present?
                    Set[*api_user.human_roles]
                  else
                    raise ArgumentError, "one of api_user: or api_roles: is required"
                  end
      new_roles = new_roles.nil? ? Set.new : Set[*new_roles.map(&:to_s)]

      if api_roles.include?('root')
        allowed_roles = ALLOWED_ROLES.dup
        allowed_roles << 'looker'
        # Only allow adding root in production if the user is paul
        allowed_roles << 'root' if (api_user.present? && api_user.email == "paul@joinswoop.com") || ENV["SWOOP_ENV"] != "production"

        allowed_roles
      elsif api_roles.include?('admin')
        if api_roles.include?('rescue')
          SwoopAuthenticator::ALL_RESCUE_ROLES
        elsif api_roles.include?('fleet')
          SwoopAuthenticator::ALL_FLEET_ROLES
        else
          ALLOWED_ROLES
        end
      else
        # we shouldn't ever get here - figure out when/why we do and fix it.

        # not_allowed_roles is a list of roles that we're trying to assign to another user that we do not have. we want to
        # ping rollbar when this happens so we can investigate and either fix the user or fix the code that's calling this.

        # NOTE: we currently don't prevent them from assiging these roles, we just log and complain

        # NOTE: there's similar code in Users::AddAllowedRolesToUser, except it checks the return value of this service
        # against new_roles and refuses to add roles if they're not allowed. the problem is that we're returing
        # ALLOWED_ROLES here for users that aren't admin / root but we're potentially allowing them to add admin to other
        # users.
        not_allowed_roles = (new_roles - api_roles) & ALLOWED_ROLES
        if not_allowed_roles.present?
          Rollbar.error(NOT_ALLOWED_ROLES_ERROR,
                        api_user: api_user,
                        api_roles: (api_user.present? ? [] : api_roles.to_a),
                        not_allowed_roles: not_allowed_roles.to_a)
        end
        ALLOWED_ROLES
      end
    end

  end
end
