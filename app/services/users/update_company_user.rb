# frozen_string_literal: true

# Service to update users for companies. It persists the changes in the db if @user.valid?
#
# attrs:
#
#  user: the user to be updated
#  user_attributes: changes to the user
#  roles: array with roles to be added to the new user
#  notification_settings: array with notification_settings to be added to the new user
#  system_email_filtered_company_ids: used to update swoop users mailing settings
#  api_user: current logged in user who's creating the new user; currently it's used to
#            check which company is creating the new user (api_user.company), and
#            to fetch the allowed roles through Users::FetchAllowedRoles(user: api_user) service,
#            as it requires it.
#
module Users
  class UpdateCompanyUser

    include AddAllowedRolesToUser

    attr_reader :user

    def initialize(user:, user_attributes:, roles:, notification_settings:, system_email_filtered_company_ids:, api_user:)
      @user = user
      @user_attributes = user_attributes
      @new_roles = roles
      @notification_settings = notification_settings
      @system_email_filtered_company_ids = system_email_filtered_company_ids

      @api_user = api_user
    end

    def call
      User.transaction do
        @user.assign_attributes(@user_attributes)

        remove_user_roles_not_contained_in_new_roles
        add_allowed_roles_to_user(user: @user, new_roles: @new_roles, api_user: @api_user)

        # only rescue company users can have notification settings for now
        if @user.company&.rescue? && @notification_settings
          update_notification_settings
        end

        update_system_email_filtered_companies

        @user.save if @user.valid?
      end

      self
    end

    private

    def remove_user_roles_not_contained_in_new_roles
      if @new_roles
        @user.roles.each do |user_role|
          new_roles_and_fleet_and_rescue = @new_roles | ['fleet', 'rescue']

          if !new_roles_and_fleet_and_rescue.include?(user_role.name)
            Rails.logger.debug(
              "#{self.class.name} User(#{@user.id}) - #{user_role.name} not included in new_roles_and_fleet_and_rescue, let's remove it from user"
            )

            @user.remove_role user_role.name
          end
        end
      end
    end

    def update_notification_settings
      @notification_settings.each do |new_setting_hash|
        user_notification_setting = @user.notification_settings.where(
          notification_code: new_setting_hash[:notification_code],
        ).first_or_initialize

        user_notification_setting.notification_channels = new_setting_hash[:notification_channels]
        @user.notification_settings << user_notification_setting
      end
    end

    def update_system_email_filtered_companies
      if @system_email_filtered_company_ids && (@api_user.has_role?(:root) || @user.id == @api_user.id)
        @user.system_email_filtered_companies = []

        @system_email_filtered_company_ids.each do |company_id|
          company = Company.find(company_id)
          @user.system_email_filtered_companies << company
        end
      end
    end

  end
end
