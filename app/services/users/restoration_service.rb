# frozen_string_literal: true

module Users
  # This service will restore a user from a deleted state
  class RestorationService

    include Interactor

    before do
      @username, @email = context.username, context.email
    end

    def call
      user.update_attributes!(deleted_at: nil)
      context.user = user
    end

    private

    def user
      @user ||= User.find_by!(email: @email) if @email
      @user ||= User.find_by!(username: @username) if @username

      raise ActiveRecord::RecordNotFound unless @user

      @user
    end

  end
end
