# frozen_string_literal: true

# Service to build users for companies. It does not persist it in the db: it's up to callers to call user.save!
#
# attrs:
#  user_attributes: attributes for the new User instance
#  new_roles: array with roles to be added to the new user
#  notification_settings: array with notification_settings to be added to the new user
#  api_user: current logged in user who's creating the new user; currently it's used to
#            check which company is creating the new user (api_user.company), and
#            to fetch the allowed roles through Users::FetchAllowedRoles(user: api_user) service,
#            as it requires it.
#
module Users
  class BuildCompanyUser

    include AddAllowedRolesToUser

    attr_reader :user
    class CompanyIdNotAllowedError < StandardError; end

    def initialize(user_attributes:, new_roles:, notification_settings: nil, api_user: nil,
                   api_company: nil, api_roles: nil)
      @user = User.new(user_attributes)

      @user_attributes = user_attributes
      @new_roles = new_roles
      @notification_settings = notification_settings
      @api_user = api_user
      @api_company = api_company || api_user&.company
      raise ArgumentError, "Couldn't find api_company" if @api_company.blank?
      @api_roles = api_roles
    end

    def call
      User.transaction do
        if @user.company_id.blank? || (@user.company_id != @api_company.id && !@api_company.is_swoop?)
          raise CompanyIdNotAllowedError("Invalid api_company.id(#{@api_company.id})")
        end

        add_allowed_roles_to_user(user: @user, new_roles: @new_roles, api_user: @api_user, api_roles: @api_roles)

        # only rescue company users can have notification settings for now
        if @user.company&.rescue? && @notification_settings&.any?
          @user.notification_settings.build(@notification_settings)
        end
      end

      self
    end

  end
end
