# frozen_string_literal: true

module Users
  module AddAllowedRolesToUser

    def add_allowed_roles_to_user(user:, new_roles:, api_user: nil, api_roles: nil)
      allowed_roles = Users::FetchAllowedRoles.call(api_user: api_user, api_roles: api_roles, new_roles: new_roles)

      # TODO currently if dispatcher is a role for a Swoop user, frontend sets it as 'swoop_dispatcher',
      # we should move that logic to BE on a separate ticket.
      if new_roles
        new_roles.each do |new_role|
          if allowed_roles.include?(new_role)
            user.add_role(new_role) unless user.has_role?(new_role)
          else
            Rails.logger.warn "Attempt to add disallowed role #{new_role} by #{api_user}"
          end
        end
      end

      if user.company.fleet?
        user.add_role :fleet
      elsif user.company.rescue?
        user.add_role :rescue
      end
    end

  end
end
