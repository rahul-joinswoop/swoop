# frozen_string_literal: true

module SymptomServices
  class FetchAllStandard

    def call
      Symptom.standard_items
    end

  end
end
