# frozen_string_literal: true

module SymptomServices
  class Change

    include SwoopService

    attr_reader :symptoms_added

    def initialize(api_company:, standard_symptom_ids_to_add:,
                   symptom_ids_to_remove:, custom_symptom_names_to_add:)

      @api_company = api_company
      @standard_symptom_ids_to_add = standard_symptom_ids_to_add || []
      @symptom_ids_to_remove = symptom_ids_to_remove || []
      @custom_symptom_names_to_add = custom_symptom_names_to_add || []

      @symptoms_added = []

      Rails.logger.debug("SymptomServices::Change params: api_company.id = #{@api_company.id}, " \
        "standard_symptom_ids_to_add = #{@standard_symptom_ids_to_add}, " \
        "symptom_ids_to_remove = #{@symptom_ids_to_remove}, " \
        "custom_symptom_names_to_add = #{@custom_symptom_names_to_add}.")
    end

    def execute
      if @api_company.instance_of? SuperCompany
        raise ArgumentError, "SuperCompany not allowed to change Symptoms"
      end

      add_standard_symptoms

      remove_symptoms

      add_custom_symptoms

      @symptoms_added.uniq
    end

    def after_execution
      PublishCompanyWorker.perform_after_commit(@api_company.id, self.class.name)
    end

    private

    def add_standard_symptoms
      standard_symptom_ids_already_added =
        @api_company.symptoms.where(id: @standard_symptom_ids_to_add).pluck(:id)

      (@standard_symptom_ids_to_add - standard_symptom_ids_already_added).each do |symptom_id|
        symptom = Symptom.find(symptom_id)

        if symptom
          @symptoms_added << symptom
          @api_company.symptoms << symptom
        end
      end
    end

    def remove_symptoms
      @symptom_ids_to_remove.each do |symptom_id|
        symptom = Symptom.find(symptom_id)

        if symptom
          @api_company.symptoms.destroy(symptom)
        end
      end
    end

    def add_custom_symptoms
      @custom_symptom_names_to_add.each do |symptom|
        name = symptom[:name]

        custom_symptom = Symptom.where('lower(name) = ?', name.downcase).first

        if !custom_symptom
          custom_symptom = Symptom.create!(name: name)
        end

        if !@api_company.symptoms.where('lower(name) = ?', name.downcase).first
          @symptoms_added << custom_symptom
          @api_company.symptoms << custom_symptom
        end
      end
    end

  end
end
