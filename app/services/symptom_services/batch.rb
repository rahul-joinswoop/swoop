# frozen_string_literal: true

module SymptomServices
  class Batch

    def initialize(api_company:, symptom_ids:)
      @api_company = api_company
      @symptom_ids = symptom_ids.split(',')

      Rails.logger.debug "Symptom batch load param: symptom_ids = #{@symptom_ids}"
    end

    def call
      Symptom.where(
        id: @symptom_ids,
      ).distinct
    end

  end
end
