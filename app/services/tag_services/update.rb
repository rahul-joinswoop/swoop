# frozen_string_literal: true

module TagServices
  class Update

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "TagServices::Update called with #{context}"

      @api_company_id = context.input[:api_company_id]
      @tag_id = context.input[:tag_id]
      @tag_attributes = context.input[:tag_attributes]

      raise(ArgumentError, 'TagServices::Update api_company_id is null') unless @api_company_id

      raise(ArgumentError, 'TagServices::Update tag_id is null') unless @tag_id

      raise(ArgumentError, 'TagServices::Update tag_attributes is null') unless @tag_attributes
    end

    def call
      tag = Tag.find_by!(
        company_id: @api_company_id, id: @tag_id
      )

      tag.update!(@tag_attributes)

      context.output = tag
    end

  end
end
