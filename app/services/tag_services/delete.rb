# frozen_string_literal: true

module TagServices
  class Delete

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "TagServices::Delete called with #{context}"

      @api_company_id = context.input[:api_company_id]
      @tag_id = context.input[:tag_id]

      raise(ArgumentError, 'TagServices::Delete api_company_id is null') unless @api_company_id

      raise(ArgumentError, 'TagServices::Delete tag_id is null') unless @tag_id
    end

    def call
      tag = Tag.find_by!(
        company_id: @api_company_id, id: @tag_id
      )

      tag.update!(deleted_at: Time.now)

      context.output = tag
    end

  end
end
