# frozen_string_literal: true

module TagServices
  class Create

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "TagServices::Create called with #{context}"

      @api_company_id = context.input[:api_company_id]
      @tag_attributes = context.input[:tag_attributes]

      raise(ArgumentError, 'TagServices::Create api_company_id is null') unless @api_company_id

      raise(ArgumentError, 'TagServices::Create tag_attributes is null') unless @tag_attributes
    end

    def call
      tag = Tag.where(tag_attributes).first_or_create!

      tag.update!(deleted_at: nil) # make sure it will be active if it has been previously deleted.

      context.output = tag
    end

    def tag_attributes
      @tag_attributes.merge(company_id: @api_company_id)
    end

  end
end
