# frozen_string_literal: true

module TagServices
  class Batch

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "TagServices::Batch called with #{context}"

      @api_company_id = context.input[:api_company_id]
      @tag_ids = context.input[:tag_ids]

      raise(ArgumentError, 'TagServices::Batch api_company_id is null') unless @api_company_id

      raise(ArgumentError, 'TagServices::Batch tag_ids is null') if @tag_ids.blank?
    end

    def call
      Rails.logger.debug "Tag ids: #{@tag_ids}"
      context.output = Tag.where(
        company_id: @api_company_id,
        id: @tag_ids
      )
    end

  end
end
