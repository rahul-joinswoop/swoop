# frozen_string_literal: true

# S3 downloader could be used to download a list of objects form Amazon S3 storage. The connection properties are
# setup in s3.yml and aws-sdk-s3.rb files.
class S3Downloader

  # Lists and download list of objects withe provided prefix.
  def download
    local_downloaded_objects = []
    list_of_objects_to_download_objects = S3_CLIENT.list_objects({ bucket: s3_bucket_name, prefix: geojson_data_path })
    list_of_objects_to_download_objects[:contents].collect(&:key).each do |object|
      next unless object != "#{geojson_data_path}/"
      local_downloaded_file_path = Tempfile.new("#{object.split('/').last}")
      File.open(local_downloaded_file_path, 'w') do |file|
        S3_CLIENT.get_object(response_target: file, bucket: s3_bucket_name, key: object)
        local_downloaded_objects << local_downloaded_file_path
      end
    end
    local_downloaded_objects
  end

  def download_specific_file(file_name_to_download)
    local_downloaded_file_path = Tempfile.new("#{file_name_to_download}")
    File.open(local_downloaded_file_path, 'w') do |file|
      S3_CLIENT.get_object(response_target: file, bucket: s3_bucket_name, key: "#{geojson_data_path}/#{file_name_to_download}")
    end
    local_downloaded_file_path
  end

  private

  def s3_bucket_name
    Configuration.s3.geojson_data_bucket
  end

  def geojson_data_path
    Configuration.s3.geojson_data_path
  end

end
