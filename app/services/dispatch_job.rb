# frozen_string_literal: true

# refactored from jobs_base_controller#create
class DispatchJob < Service

  include LoggableJob

  attr_reader :job

  def initialize(job)
    @job = job
  end

  def call
    log :debug, "DispatchJob#call called with #{@job.status}, #{@job.inspect}", job: job.id

    @job.fixup_locations

    unless @job.fleet_company.is_a?(RescueCompany)
      if !(@job.driver && @job.driver.vehicle)
        raise ArgumentError, log_msg("DispatchJob#call - Vehicle details are missing (make, model)", job: job.id)
      end
    end

    log :debug, "DispatchJob#call calling move_to_pending #{@job.status},#{@job.inspect}", job: job.id

    # TODO it probably shouldn't be called here, it should be refactored to ActivateJob
    # (but it would probably demand changes on DispatchISSCJob as it calls DispatchJob too, so I left it here for now)
    @job.move_to_pending

    log :debug, "DispatchJob#call done move_to_pending", job: job.id

    if @job.service_code.try(:name) == "Storage"
      @job.inventory_items[0].item = @job.driver.vehicle
      @job.original_store_vehicle
    end

    if @job.send_get_location_necessary?
      @job.get_location_attempts += 1
    end

    # PDW: has to be here
    if @job.rescue_driver
      @job.partner_dispatch
    end

    # So it seems that  job needs to call all this stuff first
    # inorder to get proper user stuff. So, lets do that then
    # if we support auto assign lets take it off the list.
    @job.auto_assign! if @job.supports[:auto_assign]
  end

end
