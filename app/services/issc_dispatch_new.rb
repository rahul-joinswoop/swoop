# frozen_string_literal: true

class IsscDispatchNew < Service

  attr_reader :event

  MAPPING = {
    "tow" => "Tow",
    "jump" => "Battery Jump",
    "flat" => ServiceCode::TIRE_CHANGE,
    "lock" => "Lock Out",
    "fuel" => ServiceCode::FUEL_DELIVERY,
    "repo" => "Repo",
    "winch" => "Winch Out",
    "recovery" => "Recovery",
    "impound" => "Impound",
    'decking' => 'Decking',
    'undecking' => 'Undecking',
    'rollover' => 'Rollover',
    'service_call' => 'Service Call',
    'acc' => 'Accident Tow',
    'accscene' => 'Accident Tow',
    'loaner wheel' => ServiceCode::LOANER_WHEEL,
    'tire change' => ServiceCode::TIRE_CHANGE,
  }.freeze

  def initialize(event)
    @event = event.with_indifferent_access
    # This modifies @payload directly
    map_params(@event)
  end

  def map_params(event)
    vehicle = event[:Vehicle]
    account_info = event[:AccountInfo].with_indifferent_access
    incident = event[:Incident].with_indifferent_access
    destination = event[:Destination].try(:with_indifferent_access)
    tasks = event.dig(:Job, 'PrimaryTasks')

    # For now lets just use the first service type
    service_amalgam = String.new ""
    if tasks.present?
      service_amalgam = String.new "Services:"
      tasks.each do |task|
        if task['Task'].present?
          service_amalgam << " #{task['Task']}"
        end
        if task['SecondaryTasks'].present?
          service_amalgam << "[#{task['SecondaryTasks'].join(" ")}]"
        end
      end
    end

    found_key = false
    primary_service = nil
    search_string = service_amalgam.downcase

    MAPPING.each do |search, key|
      if search_string.include?(search)
        primary_service = ServiceCode.find_by(name: key)
        found_key = true
        break
      end
    end
    accident = false
    if search_string.include?("accsec")
      primary_service = ServiceCode.find_by(name: "Tow")
      found_key = true
      accident = true
    end
    if !found_key || primary_service.blank?
      primary_service = ServiceCode.find_by(name: "Other")
    end

    notes = ""
    job = event['Job']
    job['PrimaryTasks'].each do |task|
      task.each do |key, value|
        if key == "ETDRequired"
          # continue
        else
          notes += key + ": " + value.to_s + "\n"
        end
      end
    end

    scheduled_for = job['RequestedFutureDateTime']

    if event['Coverage']
      notes += "Coverage: " + event['Coverage'].to_s.tr('"{}[]', '').gsub("=>", ": ") + "\n"
    end

    description = job['JobDescription']
    if description
      mileage_string = description.scan(/Loaded Mileage: [0-9]+/).last
      if mileage_string
        description = description.gsub(mileage_string, '')
        value = mileage_string.scan(/([0-9]+)/).last.first.to_i
        notes += "Loaded Mileage: " + value.to_s + "\n"
      end

      mileage_string = description.scan(/Covered Mileage: [0-9]+/).last
      if mileage_string
        description = description.gsub(mileage_string, '')
        value = mileage_string.scan(/([0-9]+)/).last.first.to_i
        notes += "Covered Mileage: " + value.to_s + "\n"
      end

      mileage_string = description.scan(/Over Mileage: [0-9]+/).last
      if mileage_string
        description = description.gsub(mileage_string, '')
        value = mileage_string.scan(/([0-9]+)/).last.first.to_i
        if value != 0
          notes += "Over Mileage: " + value.to_s + "\n"
        end
      end

      mileage_string = description.scan(/Enroute Mileage :[0-9]+/).last
      if mileage_string
        description = description.gsub(mileage_string, '')
        value = mileage_string.scan(/([0-9]+)/).last.first.to_i
        notes += "Enroute Mileage: " + value.to_s + "\n"
      end
    end

    four_wheel_drive = false
    if job['ServiceQuestions']
      job['ServiceQuestions'].each do |question|
        responses = question['Responses']
        if question['Question'] == "Flatbed Required?"
          if responses.include? "AWD/4WD"
            four_wheel_drive = true
            responses.delete("AWD/4WD")
          end
        elsif question['Question'] == "How many passengers will ride in tow truck?"
          if responses.include? "0"
            responses.delete("0")
          end
        elsif question['Question'] == "Is the customer in need of special accommodation?"
          if responses.include? "No"
            responses.delete("No")
          end
        elsif question['Question'] == "What is the drive type?"
          if responses.include? "All Wheel/Four Wheel Drive"
            four_wheel_drive = true
            responses.delete("All Wheel/Four Wheel Drive")
          end
        end

        if responses.length > 0
          notes += question['Question'] + ": " + responses.join(",") + "\n"
        end
      end
    end

    if incident[:CustomerWithVehicle]
      notes += 'Customer With Vehicle: ' + incident[:CustomerWithVehicle] + '\n'
    end

    if job['ServiceComments']
      notes += 'Service Comments: ' + job['ServiceComments'] + '\n'
    end

    incident_comment_hash = {}
    if incident && incident[:Comments]
      incident_comment_hash = { 'Pickup Comments': incident[:Comments] }
    end

    @payload = {
      service_code_id: primary_service.id,
      odometer: vehicle[:Odometer],
      po_number: event.dig(:Job, 'AuthorizationNumber') || event[:AuthorizationNumber],
      accident: accident,
      four_wheel_drive: four_wheel_drive,
      scheduled_for: scheduled_for,
      driver_attributes: {
        user_attributes: {
          full_name: "#{account_info[:Member][:FirstName]}  #{account_info[:Member][:LastName]}",
          # NB - PG - IS this the correct phone number?
          phone: account_info[:CallBackPhone],
        },
        vehicle_attributes: {
          make: vehicle[:Make],
          model: vehicle[:Model],
          color: vehicle[:Color],
          vin: vehicle[:VIN],
          year: vehicle[:Year],
          license: vehicle[:License],
          type: "StrandedVehicle",
        },
      },
      service_location_attributes: {
        address: (incident[:Address1].presence || incident[:CrossStreet1].presence || '') + ', ' + (incident[:City].presence || '') + ', ' + (incident[:State].presence || '') + ' ' + (incident[:Zip].presence || ''),
        city: incident[:City],
        state: incident[:State],
        zip: incident[:Zip],
        street: incident[:CrossStreet1],
        lat: incident[:Latitude],
        lng: incident[:Longitude],
      },
      issc_dispatch_request_attributes: {
        dispatchid: event[:DispatchID],
        responseid: event[:ResponseID],
        jobid: (event.dig(:Job, 'JobID') || event[:JobID]),
        max_eta: event.dig(:Job, 'MaxETA'),
        preferred_eta: event.dig(:Job, 'PreferredETA'),
        club_job_type: getClubJobType,
      },
    }
    required_ack_time = event.dig(:Job, 'RequiredAcknowledgeTime')
    if required_ack_time.present?
      @payload[:answer_by] = DateTime.now + required_ack_time.seconds
    end

    if destination
      @payload[:drop_location_attributes] = {
        address: (destination[:Address1].presence || destination[:CrossStreet1].presence || '') + ', ' + (destination[:City].presence || '') + ', ' + (destination[:State].presence || '') + ' ' + (destination[:Zip].presence || ''),
        city: destination[:City],
        state: destination[:State],
        zip: destination[:Zip],
        street: destination[:CrossStreet1],
        lat: destination[:Latitude],
        lng: destination[:Longitude],
      }
    end

    pc = event.dig(:AccountInfo, :Customer)
    if pc
      @payload["pickup_contact_attributes"] = {
        full_name: "#{pc[:FirstName]} #{pc[:LastName]}",
      }
    end

    Rails.logger.debug "Setting pickup phone, hash: #{incident_comment_hash}"
    pickup_phone = incident_comment_hash['Contact Phone']
    if pickup_phone
      if @payload["pickup_contact_attributes"]
        @payload["pickup_contact_attributes"][:phone] = pickup_phone
      else
        @payload["pickup_contact_attributes"] = { phone: pickup_phone }
      end
      incident_comment_hash.delete('Contact Phone')
    end

    dc_phone = event.dig(:Destination, "Phone")
    dc_location = event.dig(:Destination, "LocationInfo")
    if dc_phone || dc_location
      @payload["dropoff_contact_attributes"] = {
        full_name: dc_location,
        phone: dc_phone,
      }
    end

    engine_type = vehicle[:EngineType]

    if engine_type.present?
      notes += "Engine Type: #{engine_type}\n"
    end

    incident_comment_hash.each do |k, v|
      notes += "#{k}: #{v}\n"
    end

    if description
      notes += description.gsub(/[\n\t\ ]+/, ' ')
    end

    @payload["notes"] = notes

    Rails.logger.debug "PAYLOAD: #{JSON.pretty_generate(@payload)}"
    @payload
  end

  # OVerridden by transcript
  def getClubJobType()
    "Digital"
  end

  def call
    issc = Issc.find_issc(@event)

    @job = FleetMotorClubJob.new(@payload)

    # Look up rescue provider

    DispatchIsscJob.new(issc, @job).call

    acktime = event.dig(:Job, 'RequiredAcknowledgeTime')
    if acktime.present?
      IsscCancelOverdueJobs.perform_in((acktime + 1).seconds + 4.minutes)
    end

    @job.save!

    treat_missing_location_lat_lng_if_needed

    @job
  end

  private

  def treat_missing_location_lat_lng_if_needed
    if @job&.service_location&.address && (@job&.service_location&.lat.blank? || @job&.service_location&.lng.blank?)
      Rails.logger.debug "ISSC (LOCATION #{@job.service_location}) without lat/lng. Will try to resolve with LatLngResolver"

      LatLngResolver.perform_async(@job.service_location.id)
    end

    if @job&.drop_location&.address && (@job&.drop_location&.lat.blank? || @job&.drop_location&.lng.blank?)
      Rails.logger.debug "ISSC (LOCATION #{@job.drop_location}) without lat/lng. Will try to resolve it with LatLngResolver"

      LatLngResolver.perform_async(@job.drop_location.id)
    end
  end

end
