# frozen_string_literal: true

class IsscDispatchRejected < Service

  def initialize(event)
    @event = event
    @dispatchid = event[:DispatchID]
  end

  def self.reject_job(event, job)
    job.issc_rejected
    job.issc_dispatch_request.requester_rejected
    job
  end

  def call
    raise ISSC::DispatchJobNotFound unless @dispatchid

    # MAke sure that we pick from the correct set or some Motorclubs will reject the reject

    Job.transaction do
      job = Job.joins(:issc_dispatch_request).where('issc_dispatch_requests.dispatchid = ?', @dispatchid).take

      raise ISSC::DispatchJobNotFound unless job

      Rails.logger.debug "#{job.inspect}"

      IsscDispatchRejected.reject_job(@event, job)
    end
  end

end
