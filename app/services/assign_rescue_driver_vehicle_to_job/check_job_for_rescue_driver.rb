# frozen_string_literal: true

class AssignRescueDriverVehicleToJob::CheckJobForRescueDriver

  include Interactor
  FAILURE_MESSAGE = 'job.rescue_driver is blank'

  def call
    if context.job.rescue_driver.blank?
      Rails.logger.debug "JOB(#{context.job.id}): #{self.class.name}: #{FAILURE_MESSAGE}"
      context.fail! error: FAILURE_MESSAGE
    end
  end

end
