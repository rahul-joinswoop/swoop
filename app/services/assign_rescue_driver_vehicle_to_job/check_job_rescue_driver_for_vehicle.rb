# frozen_string_literal: true

class AssignRescueDriverVehicleToJob::CheckJobRescueDriverForVehicle

  include Interactor
  FAILURE_MESSAGE = 'job.rescue_driver.vehicle is blank'

  def call
    if context.job.rescue_driver.vehicle.blank?
      Rails.logger.debug "JOB(#{context.job.id}): #{self.class.name}: #{FAILURE_MESSAGE}"
      context.fail! error: FAILURE_MESSAGE
    end
  end

end
