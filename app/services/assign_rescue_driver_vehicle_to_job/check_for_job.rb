# frozen_string_literal: true

class AssignRescueDriverVehicleToJob::CheckForJob

  include Interactor
  FAILURE_MESSAGE = 'job is required'

  def call
    if context.job.blank?
      Rails.logger.debug "#{self.class.name}: #{FAILURE_MESSAGE}"
      context.fail! error: FAILURE_MESSAGE
    else
      Rails.logger.debug "JOB(#{context.job.id}): AssignRescueDriverVehicleToJob starting"
    end
  end

end
