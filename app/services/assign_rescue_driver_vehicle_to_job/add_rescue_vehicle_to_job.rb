# frozen_string_literal: true

class AssignRescueDriverVehicleToJob::AddRescueVehicleToJob

  include Interactor
  FAILURE_MESSAGE = "Couldn't update job.rescue_vehicle: %s"

  def call
    context.job.update! rescue_vehicle: context.job.rescue_driver.vehicle
    Rails.logger.debug "JOB(#{context.job.id}): #{self.class.name} added rescue_vehicle"
  rescue => e
    error = FAILURE_MESSAGE % [e.message]
    Rails.logger.error "JOB(#{context.job.id}): #{self.class.name}: #{error}"
    context.fail! error: error
  end

end
