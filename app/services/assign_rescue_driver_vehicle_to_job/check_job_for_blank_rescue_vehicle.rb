# frozen_string_literal: true

class AssignRescueDriverVehicleToJob::CheckJobForBlankRescueVehicle

  include Interactor
  FAILURE_MESSAGE = 'job already has a rescue_vehicle'

  def call
    if context.job.rescue_vehicle.present?
      Rails.logger.debug "JOB(#{context.job.id}): #{self.class.name}: #{FAILURE_MESSAGE}"
      context.fail! error: FAILURE_MESSAGE
    end
  end

end
