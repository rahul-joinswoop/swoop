# frozen_string_literal: true

module SignupService
  class SignupCompany < CompanyService::Create

    include SwoopService

    attr_reader :user

    def initialize(company_attributes:, location_attributes:, user_attributes:)
      super(company_attributes: company_attributes, location_attributes: location_attributes, feature_ids: [])

      @user_attributes = user_attributes
      @user = nil

      Rails.logger.debug("SignupService::SignupCompany initialized with: Company: #{company_attributes} " \
        "| Location: #{location_attributes}" \
        "| User: #{user_attributes.except(:password)}")
    end

    def execute
      super() # super call creates company

      instantiate_new_user

      # save it!
      @user.save!

      Rails.logger.debug("SignupService::SignupCompany new Company created id: #{@company.id}, name: #{@company.name}")
      Rails.logger.debug("SignupService::SignupCompany new User created id: #{@user.id}, name: #{@user.name}")

      self
    end

    def after_execution
      if @user.valid?
        Rails.logger.debug("SignupService::SignupCompany after_execution will queue async job PartnerSignupEmail for company id: #{@company.id}")

        Delayed::Job.enqueue(partner_signup_email)
      end
    end

    private

    def instantiate_new_user
      @user_attributes[:job_status_email_frequency] = 'dispatcher'
      @user_attributes[:job_status_email_preferences] = ['Created', 'Accepted', 'GOA'].map do |job_status|
        JobStatusEmailPreference.new(job_status: JobStatus.find_by_name(job_status))
      end

      @user = User.new(@user_attributes)

      ['admin', 'driver', 'dispatcher', 'rescue'].each do |role|
        @user.add_role role
      end

      # associate the company with the user (@company is instantiated by super)
      @user.company = @company
    end

    def partner_signup_email
      EmailPartnerSignup.new(@company.id, @user.id)
    end

  end
end
