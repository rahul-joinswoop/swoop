# frozen_string_literal: true

module ISSC
  class LoginService

    MAX_LOGIN_BATCH_SIZE = 20

    def initialize(isscs)
      @isscs = isscs
    end

    def call
      grouped = @isscs.group_by { |issc| issc.clientid == 'AGO' ? :ago : :other }

      login(grouped[:other]) if grouped[:other].present?

      login_agero(grouped[:ago]) if grouped[:ago].present?
    end

    def login(isscs)
      Rails.logger.debug("ISSC::LoginService logging in #{isscs.length} isscs")

      extra = false
      if isscs.length > MAX_LOGIN_BATCH_SIZE
        isscs = isscs[0..MAX_LOGIN_BATCH_SIZE - 1]
        extra = true
      end
      Issc.transaction do
        isscs.each do |issc|
          Rails.logger.debug("ISSC::LoginService logging in issc.id:#{issc.id}")
          issc.provider_login
          issc.last_login_attempted_at = Time.now
          issc.save!
        end
        args = isscs.map(&:id).sort
        IsscLoginProvider.perform_async(args)
      end
      if extra
        Rails.logger.debug('ISSC::LoginService scheduling extra')
        IsscFixLogouts.perform_at(Time.now + 1.minute)
      end
    end

    def login_agero(isscs)
      Rails.logger.debug "ISSC::LoginService::login_agero called with #{isscs.map(&:id)}"
      groups = Issc.dispatchable.where(
        clientid: 'AGO',
        contractorid: isscs.map(&:contractorid).uniq,
        status: 'Registered',
        system: nil
      ).group_by(&:contractorid)
      Rails.logger.debug "ISSC::LoginService::login_agero groups: #{groups}"
      locations = groups.values.select do |grouped_isscs|
        sites = grouped_isscs.map(&:site)
        sites.all?(&:within_hours) || sites.any?(&:force_open)
      end.flatten
      Rails.logger.debug "ISSC::LoginService::login_agero locations: #{locations}"
      login(locations) if locations.present?
    end

  end
end
