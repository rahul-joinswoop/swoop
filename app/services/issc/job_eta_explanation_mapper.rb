# frozen_string_literal: true

# This is primarily used by ISSC to map job acceptance
# reasons before sending the status to ISSC.
module ISSC
  class JobEtaExplanationMapper

    include SwoopService

    attr_reader :explanation

    QUEST = 'QUEST'
    AGO = 'AGO'

    EXPLANATION_MAP = {
      AGO => {
        JobEtaExplanation::NONE => "None",
        JobEtaExplanation::TRAFFIC => "Traffic",
        JobEtaExplanation::WEATHER => 'Weather',
      },
      QUEST => {
        JobEtaExplanation::CONSTRUCTION => "Construction",
        JobEtaExplanation::OTHER => "Other",
        JobEtaExplanation::RURAL_DISABLEMENT => "RuralDisablement",
        JobEtaExplanation::SERVICE_PROVIDER_ISSUE => "ServiceProviderIssue",
        JobEtaExplanation::TRAFFIC => "Traffic",
        JobEtaExplanation::WEATHER => 'Weather',
      },
    }.freeze

    def initialize(job_id:, eta:)
      @job_id = job_id
      @eta    = eta

      raise ArgumentError, '@job_id is blank' if @job_id.blank?
      raise ArgumentError, '@eta is blank' if @eta.blank?
    end

    def execute
      return unless job&.issc_dispatch_request&.max_eta

      if @eta > job.issc_dispatch_request.max_eta
        @explanation = EXPLANATION_MAP.dig(
          job.issc_dispatch_request.issc.clientid,
          job.eta_explanation&.text
        ) || ""
      end
    end

    private

    def job
      @job ||= Job.find(@job_id)
    end

  end
end
