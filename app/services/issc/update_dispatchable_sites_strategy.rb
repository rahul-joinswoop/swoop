# frozen_string_literal: true

module ISSC
  class UpdateDispatchableSitesStrategy

    def initialize(rescue_company, fleet_company, site_args)
      @rescue_company = rescue_company
      @fleet_company = fleet_company
      @sites_args = site_args
    end

    def call
      objs = UpdateDispatchableSites.new(
        @rescue_company, @fleet_company, @sites_args
      ).call

      Rails.logger.debug("ISSC::UpdateDispatchableSitesStrategy service results #{objs}")

      return if objs.blank?

      g = objs.group_by do |issc|
        if !issc.is_a?(Issc)
          :other
        elsif issc.status == 'logging_out'
          :logging_out
        elsif issc.delete_pending_at && (issc.status != 'unregistered')
          :deregistering
        else
          :other
        end
      end

      objs.map(&:save!)

      Rails.logger.debug "ISSC::UpdateDispatchableSitesStrategy grouped #{g}"

      logging_out = g[:logging_out]
      IsscLogoutProvider.perform_async(logging_out.map(&:id)) if logging_out.present?

      deregistering = g[:deregistering]
      if deregistering.present?
        IsscFixDeleted.perform_after_commit
      end
    end

  end
end
