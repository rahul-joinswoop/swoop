# frozen_string_literal: true

module ISSC
  class AcceptJob

    include SwoopService

    def initialize(job)
      @job = job
    end

    def execute
      accept_time = @job.account.auto_eta
      @job.bid(Time.now + accept_time.minutes)
      @job.partner_accepted
      @job.save!
    end

    def after_execution
      IsscDispatchResponseAccept.perform_async(@job.id, :Automated)
    end

  end
end
