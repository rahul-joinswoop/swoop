# frozen_string_literal: true

require 'issc'

module ISSC
  class RejectJob

    include SwoopService

    def initialize(job)
      @job = job
    end

    def execute
      @job.partner_rejected
      @job.save!
    end

    def after_execution
      IsscDispatchResponseReject.perform_async(@job.id, :Automated)
    end

  end
end
