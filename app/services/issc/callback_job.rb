# frozen_string_literal: true

module ISSC
  class CallbackJob

    include LoggableJob
    include SwoopService

    # call_back_hash: is here only for signature matching
    # with RSC, it's not used at all by this service.
    def initialize(job:, api_user:, call_back_hash:)
      @job = job
      @api_user = api_user

      log :debug, "ISSC::CallbackJob initialized for", job: @job.id
    end

    def execute
      @job.partner_dispatcher = @api_user
      @job.answer_by = nil
      @job.partner_request_callback
      @job.save!
    end

    def after_execution
      IsscDispatchResponseCallback.perform_async(@job.id, :Automated)
    end

  end
end
