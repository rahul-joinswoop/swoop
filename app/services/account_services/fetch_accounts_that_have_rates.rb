# frozen_string_literal: true

module AccountServices
  class FetchAccountsThatHaveRates

    include Interactor
    include Interactor::Swoop

    before do
      Rails.logger.debug "Accounts::FetchAccountsThatHaveRatesService called #{context}"

      @api_company_id = context.input[:api_company_id]

      raise(ArgumentError, 'RateService::FindDefaultsAndCopy @api_company_id is null') unless
        @api_company_id
    end

    def call
      context.output = uniq_account_ids.map do |account_id|
        {
          id: account_id,
          sites: sites_with_vendor_ids_by(account_id),
        }
      end
    end

    private

    def uniq_account_ids
      accounts_with_sites_and_vendor_ids.map(&:id).uniq
    end

    def accounts_with_sites_and_vendor_ids
      @accounts_with_sites_and_vendor_ids ||= api_company.accounts
        .joins(:rates)
        .select(:id, :name, 'rates.site_id AS site_id', 'rates.vendor_id AS vendor_id')
        .where(accounts: { deleted_at: nil }, rates: { deleted_at: nil })
        .group(:id, :name, 'rates.site_id', 'rates.vendor_id')
        .order(:name).to_a
    end

    def sites_with_vendor_ids_by(account_id)
      site_ids_by(account_id).map do |site_id|
        {
          id: site_id,
          vendor_ids: vendor_ids_by(account_id, site_id),
        }
      end
    end

    def site_ids_by(account_id)
      accounts_with_sites_and_vendor_ids.select do |account_with_sites_and_vendor_ids|
        account_with_sites_and_vendor_ids.id == account_id
      end.map(&:site_id).uniq
    end

    def vendor_ids_by(account_id, site_id)
      accounts_with_sites_and_vendor_ids.select do |account_with_sites_and_vendor_ids|
        account_with_sites_and_vendor_ids.id == account_id &&
        account_with_sites_and_vendor_ids.site_id == site_id
      end.map(&:vendor_id)
    end

    def api_company
      @api_company ||= Company.find(@api_company_id)
    end

  end
end
