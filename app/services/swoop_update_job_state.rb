# frozen_string_literal: true

# Swoop has selected a state withing the drop down
class SwoopUpdateJobState < API::Jobs::UpdateJobStatus

  SERVICE_TYPE = :swoop
  LEGAL_STATES = [
    # Pending has to be in this list even though we don't handle it
    # below
    # TODO - should this include every status? maybe only when a root
    # user is calling?
    "Pending",
    "Assigned",
    "Dispatched",
    "En Route",
    "On Site",
    "Towing",
    "Tow Destination",
    "Completed",
    "Released",
    "Canceled",
    "Reassigned",
    "Rejected",
    "GOA",
    'Deleted',
  ].freeze

  def initialize(job, status, **opts)
    super
    @goa_amount = opts[:goa_amount]
  end

  private

  def execute_custom_actions_for_status
    case @status
    when "Assigned"
      # This has to be wrong... @job.assign_rescue_company
    when "Dispatched"
    when "En Route"
    when "On Site"
      ConfirmPartnerCompletedSmsAlert.send_if_necessary(@job)
    when "Towing"
    when "Tow Destination"
    when "Completed"
      @job.send_user_invoice
      @job.send_review_sms
    when "Released"
    when "Canceled"
      @job.cancel_invoice
      @job.cancel_auction
    when "Reassigned"
      @job.after_fleet_reasssigned
      @job.delete_invoice
    when "Rejected"
    when "GOA"
      update_goa_billing_info
      @job.mark_invoice_goa
    when 'Deleted'
      @job.deleted_at = Time.now
      @job.delete_invoice
    else
      raise CustomActionError, "Unknown job status: #{@status}"
    end
  end

  def update_goa_billing_info
    if @goa_amount.present?
      billing_info = (@job.billing_info || @job.build_billing_info)
      billing_info.rescue_company ||= @job.rescue_company
      billing_info.billing_type ||= BillingType::VCC
      billing_info.goa_amount = @goa_amount
      billing_info.goa_agent = @api_user
      billing_info.save!
    end
  end

end
