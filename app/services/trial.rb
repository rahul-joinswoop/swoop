# frozen_string_literal: true

# Creates external connections

class Trial

  include LoggableJob

  def initialize(experiment)
    Rails.logger.debug "TRIAL: creating experiment: #{experiment}"
    @experiment = Experiment.find_or_create_by!(name: experiment)
  end

  def self.create_variant(experiment_name, variant_name, data)
    experiment = Experiment.find_or_create_by!(name: experiment_name)
    Variant.create!(experiment: experiment, name: variant_name, data: data) unless Variant.find_by(experiment: experiment, name: variant_name)
  end

  def variant(user, job = nil)
    log :debug, "TRIAL::variant enrolling user:#{user.id} in experiment #{@experiment.name}", job: job.try(:id)
    variant_name = choose_variant(user)
    log :debug, "TRIAL chose: #{variant_name}", job: job.try(:id)
    if variant_name
      variant = Variant.find_or_create_by!(experiment: @experiment, name: variant_name)
      ue = UserExperiment.create_with(job: job).find_or_create_by!(experiment: @experiment, user: user, variant: variant)
      log :debug, "TRIAL created ue #{ue.inspect}", job: job.try(:id)
      variant
    else
      log :debug, "TRIAL: nil returned from choose_variant", job: job.try(:id)
      nil
    end
  end

  def convert(user)
    Rails.logger.debug "TRIAL::convert #{user.id}"
    user_experiment = UserExperiment.find_by(experiment: @experiment, user: user)
    if user_experiment
      user_experiment.update_columns({ conversion: true })
    else
      msg = "Unable to convert #{user.id} in trial #{@experiment.try(:name)}, no UserExperiment found"
      Rails.logger.error msg
      Rollbard.error(StandardError.new(msg))
    end
  end

  private

  def choose_variant(user)
    options = {}
    @experiment.variants.each do |var|
      options[var] = var.weight
    end
    picker(options).try { :name }
  end

  #    picker({"A" => 1, "B" => 3})

  def picker(options)
    current, max = 0, options.values.inject(:+)
    random_value = rand(max) + 1
    options.each do |key, val|
      current += val
      return key if random_value <= current
    end
  end

end
