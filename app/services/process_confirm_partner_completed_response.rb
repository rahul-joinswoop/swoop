# frozen_string_literal: true

class ProcessConfirmPartnerCompletedResponse

  include LoggableJob

  def initialize(twilio_response, sms_alert)
    @twilio_response = twilio_response
    @sms_alert = sms_alert
  end

  def set_parsed_response(sms, response)
    sms.parsed_response = response
    sms.response_received_at = Time.now
  end

  def call()
    response = @twilio_response.body
    sms = @twilio_response.target
    sms.response = response

    job_id = @sms_alert.job_id

    if !sms.parsed_response
      if response.include?('Y') || response.include?('1')
        log :debug, "parsed 1", job: job_id
        set_parsed_response(sms, 1)
        if ConfirmPartnerCompletedSmsAlert::VALID_FROM_STATES.include? @sms_alert.job.status
          srv = SwoopUpdateJobState.new(@sms_alert.job, "Completed")
          srv.call
          @sms_alert.job.save!
        end
      elsif response.include?('N') || response.include?('2')
        log :debug, "parsed 2", job: job_id
        set_parsed_response(sms, 2)
        Sms::PartnerNotCompleted.send_notification(@sms_alert.job)
        ViewAlertService.customer_says_not_complete(@sms_alert.job, true)
      else
        log :warn, "expected 1 or 2, got:#{response}", job: job_id
      end
    else
      log :debug, " discarding response as already have one: #{response}", job: job_id
    end

    sms.save!
  end

end
