# frozen_string_literal: true

class AblyController < ActionController::Base

  before_action :handle_authentication

  def auth
    render status: 201, json: Swoop.ably_rest_client.auth.create_token_request(
      capability: { '*' => ['presence', 'subscribe'] },
      client_id: 'graphql-subscriber',
    )
  end

  private def handle_authentication
    @auth_result = GraphQL::Authenticate.call(input: { request: request })
    unless @auth_result.success?
      Rollbar.error(@auth_result.message || GraphQLController::AUTHENTICATION_FAILURE, @auth_result)
      Rails.logger.debug @auth_result
      return render status: 401, json: {
        status: 401,
        message: GraphQLController::AUTHENTICATION_FAILURE,
      }
    end
  end

end
