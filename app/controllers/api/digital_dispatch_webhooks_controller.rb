# frozen_string_literal: true

# This controller holds routes to receive digital dispatch events
#
# Routes available:
#   - Agero::Rsc -> action handle_rsc_event
class API::DigitalDispatchWebhooksController < API::APIController

  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  around_action :audit_event
  before_action :check_for_disconnected_token!

  # This route receives Agero::Rsc job events. The Handler Selector
  # will choose the appropriate handler given the notificationCode passed
  # in the event_body_hash; the handler will be responsible to give it
  # the appropriate treatment.
  #
  # All handlers validate the Authorization header passed: we must have a
  # corresponding:
  #
  #   APIAccessToken.where(
  #     callback_code: request.headers['Authorization'], vendorid: event_body_hash[:vendorId]
  #   )
  #
  # in our DB, otherwise the handler will throw Agero::Rsc::CallbackTokenError,
  # in which case we rescue from it by responding :bad_request to the request.
  def handle_rsc_event
    event_handler = Agero::Rsc::EventHandlers::Selector.find_handler_to(
      event: event_body_hash,
      callback_token: callback_token,
    )

    event_handler.handle!

    head :ok
  end

  private

  def check_for_disconnected_token!
    if api_access_token.nil?
      notify_callback_token_error("RSC callback token not found")
      render plain: "Forbidden", status: 401
    elsif !api_access_token.connected?
      notify_callback_token_error("RSC callback token #{api_access_token.id} not connected")
      render plain: "Not connected", status: 401
    end
    nil
  end

  def notify_callback_token_error(message)
    logger.error(message)
    Rollbar.error(Agero::Rsc::CallbackTokenError.new(message))
  end

  def callback_token
    auth_header = request.headers['Authorization']
    if auth_header.present?
      Agero::Rsc.clean_authorization_header(auth_header)
    else
      nil
    end
  end

  def api_access_token
    unless defined?(@api_access_token)
      auth_token = callback_token
      if auth_token.present?
        @api_access_token = APIAccessToken.find_by(callback_token: auth_token, deleted_at: nil)
      else
        @api_access_token = nil
      end
    end
    @api_access_token
  end

  def event_body_hash
    @event_body_hash ||= MultiJson.load(request.body.read).with_indifferent_access
  end

  # Record the request in the audit table.
  def audit_event
    audit_issc = nil
    error = nil

    begin
      json = JSON.pretty_generate(event_body_hash)
      dispatchid = event_body_hash[:dispatchRequestNumber]
      job = api_access_token&.find_job_by_dispatchid(dispatchid)
      logger.debug("#{self.class.name} - received event #{json}")
      audit_issc = AuditIssc.create!(
        path: request.path,
        data: json,
        clientid: Issc::AGERO_CLIENTID,
        job_id: job&.id,
        contractorid: event_body_hash[:vendorId],
        locationid: event_body_hash[:facilityId],
        dispatchid: dispatchid,
        incoming: true,
        company_id: api_access_token&.company_id,
        system: Issc::RSC_SYSTEM,
      )
    rescue => e
      # Don't raise any errors if the audit row can't be created
      logger.error(e)
      Rollbar.error(e)
    end

    begin
      yield
    rescue => e
      error = e.inspect
      response.status = 500 if response.status && response.status < 300
      raise e
    ensure
      if audit_issc
        begin
          audit_issc.update_columns(http_status_code: response.status, http_response: response.body, error: error)
        rescue => e
          logger.error(e)
          Rollbar.error(e)
        end
      end
    end
  end

  protected

  def set_request_context
    context = RequestContext.current
    context.source_application_id = SourceApplication.rsc_application.id
  end

end
