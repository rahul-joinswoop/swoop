# frozen_string_literal: true

# PDW who calls this???

class API::V1::AccountsController < API::APIController

  REST_CLASS = Account

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_accounts, only: [:index]
  before_action :set_batch_accounts, only: [:batch]

  def index
    pagination_hack
  end

  def batch
    pagination_hack
    render template: "api/v1/accounts/index"
  end

  private

  def set_accounts
    @accounts = api_company.accounts.not_deleted.includes(*includes)
  end

  def set_batch_accounts
    if params[:account_ids]
      execute_batch_query
    else
      @accounts = []
    end
  end

  def includes
    [:location, :physical_location, :client_company, company: [:sites]]
  end

  def execute_batch_query
    account_ids = params[:account_ids].split(',')
    set_accounts
    @accounts = @accounts.where(id: account_ids).order(created_at: :desc)
  end

  def pagination_hack
    # HACK - this should always be enabled by default.
    if params[:page] || params[:per_page]
      @accounts = @accounts.paginate(page: params[:page], per_page: params[:per_page])
    end
  end

end
