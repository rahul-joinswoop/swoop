# frozen_string_literal: true

class API::V1::VersionsController < API::APIController

  REST_CLASS = Version

  def index
    page         = params[:page]
    per_page     = params[:per_page]
    name         = params[:name]

    pagination_opts = { page: page, per_page: per_page }

    @versions = Version
      .where(name: name)
      .order(:created_at)
      .reverse_order
      .paginate(pagination_opts)

    render template: "api/v1/versions/index"
  end

  def latest
    @version = Version.where({ name: params[:name] }).order(:created_at).reverse_order.first
    render template: "api/v1/versions/show"
  end

end
