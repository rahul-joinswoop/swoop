# frozen_string_literal: true

###### WARNING - this class has no authentication or authorization (uuid based)

class API::V1::InvoicesController < API::APIController

  REST_CLASS = Invoice
  before_action :set_invoice_uuid, only: [:show_uuid]
  around_action :set_time_zone, only: :search

  has_scope :filter_between, using: [:start_at, :end_at], type: :hash, as: :between
  has_scope :filter_full_name, as: :full_name
  has_scope :filter_sender_company_id, as: :sender_company_id
  has_scope :filter_recipient_company_id, as: :recipient_company_id
  has_scope :filter_job_before, as: :job_before
  has_scope :filter_job_after, as: :job_after
  has_scope :filter_job_search, as: :job_search

  has_scope :filter_job_active, as: :job_active, type: :boolean, allow_blank: true do |controller, scope, active|
    active ? scope.filter_job_active : scope.filter_job_done
  end

  has_scope :filter_paid, as: :paid, type: :boolean, allow_blank: true do |controller, scope, paid|
    paid ? scope.filter_paid : scope.filter_unpaid
  end

  def create
    args = invoice_params
    logger.debug "args: #{args}"
    payments = remove_param(:payments, args)
    @invoice = Invoice.new(args)
    job = @invoice.job

    if !(@invoice.recipient && @invoice.job)
      raise ArgumentError, 'Must supply valid job_id, recipient_id and recipient_type(Account|User)'
    end
    logger.debug "Recipeint: #{@invoice.recipient}"
    # logger.debug "invoice for job: #{@invoice.job.invoice.id}"
    if @invoice.recipient.is_a? Account
      @invoice.recipient_company = @invoice.recipient.client_company
    end

    @invoice.currency = invoice_sender.currency
    @invoice.sender = invoice_sender

    old_invoice = @invoice.job.invoice
    job.invoice = @invoice

    @invoice.version = Invoice::VERSION
    @invoice.line_items.each do |li|
      li.version = Invoice::VERSION
    end

    Invoice.transaction do
      @invoice.save!

      if payments
        payments_service = Payments::UpdatePaymentsService.new(payments, @invoice)
        payments_service.call
      end

      job.save!

      if old_invoice
        old_invoice.payments_or_credits.each do |poc|
          poc.deleted_at = Time.now
          poc.save!
        end
        old_invoice.deleted_at = Time.now
        old_invoice.save!
      end
      respond_to do |format|
        format.json { render 'api/v1/invoices/show', status: :created }
      end
    end
  end

  # GET /api/v1/invoices/:id
  def show
    render template: "api/v1/invoices/show"
  end

  # GET /api/v1/invoices/:uuid
  def show_uuid
    if params[:inline_jobs]
      @inline_jobs = true
    end
    render "api/v1/invoices/show"
  end

  # GET /api/v1/invoices
  def index
    #    logger.debug @invoices.inspect
  end

  # GET /api/v1/invoices_search
  def search
    Rails.logger.debug("invoice search: #{params}")

    # perform a jobs search based on the search terms
    service = JobSearchService.new(company: api_company, invoice_search: true)

    filter_paid = params[:filters].delete(:paid)

    if !filter_paid.nil?
      key = filter_paid == "true" ? :paid_invoices? : :unpaid_invoices?
      params[:filters][key] = true
    end

    invoice_states = params[:filters][:invoice_state]
    if invoice_states.is_a?(String)
      invoice_states = invoice_states.split(' ')
    elsif invoice_states.nil?
      invoice_states = []
    end

    search_params = params.merge(filters: params[:filters].merge(invoice_states: invoice_states))
    _jobs_total, jobs = service.search(search_params)

    # filter by incoming or outgoing invoices
    Rails.logger.debug("Filter invoice states: #{invoice_states}")
    invoice_filter = case api_company
                     when SuperCompany
                       if %w(swoop_new swoop_sent swoop_approved_fleet swoop_sending_fleet swoop_sent_fleet).include?(invoice_states.first)
                         Rails.logger.debug("Swoop - outgoing")
                         api_company.outgoing_invoices
                       else
                         Rails.logger.debug("Swoop - incoming")
                         api_company.incoming_invoices
                       end
                     when FleetCompany
                       Rails.logger.debug("Fleet incoming")
                       api_company.incoming_invoices
                     else # RescueCompany
                       Rails.logger.debug("Rescue outgoing")
                       api_company.outgoing_invoices
                     end

    # pull invoices for each job and filter by state / invoice set
    invoices = []
    jobs.each do |job|
      job.live_invoices.each do |invoice|
        invoices << invoice if invoice_states.include?(invoice.state) && invoice_filter.include?(invoice)
      end
    end

    if !filter_paid.nil?
      match = []
      match << true if filter_paid == "true"
      match += [false, nil] if filter_paid == "false"
      invoices.select! { |i| match.include? i.paid }
    end

    # simple pagination
    @invoices_total = invoices.size
    page = (params[:page] || 1).to_i
    per_page = (params[:per_page] || 20).to_i
    @invoices = invoices[(page - 1) * per_page, per_page]
    render "api/v1/invoices/index"
  end

  # PATCH/PUT /api/v1/invoices/1
  def update
    @inline_jobs = true if params[:inline_jobs]
    p = invoice_params
    state = remove_param(:state, p)
    state_change(p, "#{state}") if state

    edit_invoice_service = InvoiceService::EditInvoiceForm.new(@invoice, p)

    respond_to do |format|
      edit_invoice_service.call

      CreateHistoryItem.call(
        job: @invoice.job,
        target: @invoice,
        user: api_user,
        event: HistoryItem::INVOICE_EDITED,
      )

      logger.debug "invoice: #{@invoice.inspect}"
      format.json { render "api/v1/invoices/show", status: :ok }
    end
  end

  # This route only triggers the invoice PDF creation asynchronously.
  #
  # It will return a json containing the invoice_pdf whose file is going to be created
  # by a worker. The client can use it on invoices#show_invoice_pdf
  # route to get the PDF details (and then check if the URL is ready for instance).
  #
  def create_pdf_asynchronously
    invoice = Invoice.find_by!(uuid: params[:uuid])

    @invoice_pdf = invoice.generate_pdf

    render json: @invoice_pdf, status: :accepted
  end

  # Returns the Invoice PDF by the given invoice_pdf_id.
  def show_pdf
    @invoice_pdf = InvoicePdf.find(params[:pdf_id])

    Rails.logger.debug "INVOICE(#{@invoice_pdf.invoice_id}) InvoicesController#create_pdf_asynchronously " \
      "response_content_disposition: #{params[:response_content_disposition]}"

    render json: @invoice_pdf, scope: { response_content_disposition: params[:response_content_disposition] }, status: :ok
  end

  def samplerate
    invoice = Invoice.find_by!({ id: params[:id] })
    args = params.require(:invoice).permit(:rate_type, :invoice_vehicle_category_id)

    logger.debug "working on #{args}"
    @invoice = Invoice.new(
      rate_type: args[:rate_type],
      sender: invoice_sender,
      recipient: invoice.recipient,
      job: invoice.job,
      currency: invoice.currency
    )

    code = ServiceCode.where(name: "Storage").first

    if invoice.job.service_code_id != code.id
      logger.debug "INVOICE(#{invoice.job.id}) samplerate #{args[:rate_type]}"
      rate = Rate.inherit_type(
        invoice_sender.id,
        invoice.job.account_id,
        invoice.job.site_id,
        invoice.job.issc_contractor_id,
        invoice.job.service_code_id,
        args[:invoice_vehicle_category_id],
        args[:rate_type]
      )

      # logger.debug "INVOICE(#{invoice.job.id}) samplerate inherit returned #{rate.inspect}"
      if !(rate && rate.valid?)

        rate = Rate.new(
          company: invoice_sender,
          type: args[:rate_type],
          service_code: invoice.job.service_code,
          account: invoice.job.account,
          site: invoice.job.site,
          vendor_id: invoice.job.issc_contractor_id,
          vehicle_category_id: invoice.job.invoice_vehicle_category_id,
          miles_p2p: 0,
          miles_p2p_free: 0,
          hourly: 0,
          hookup: 0,
          miles_towed: 0,
          miles_towed_free: 0,
          miles_enroute: 0,
          miles_enroute_free: 0,
          special_dolly: 0,
          gone: 0,
          storage_daily: 0,
          flat: 0,
          miles_deadhead: 0,
          miles_deadhead_free: 0
        )
      end

      logger.debug "Using Rate: #{rate.inspect}"
      #      invoice.job.estimate_invoice(rate,@invoice)
      estimate_invoice(rate, invoice.job)
    end

    if invoice.job.storage
      logger.debug "INVOICE(#{invoice.job.id}) samplerate storage"
      storage_rate = Rate.inherit_type(
        invoice_sender.id,
        invoice.job.account_id,
        invoice.job.site_id,
        invoice.job.issc_contractor_id,
        code.id,
        args[:invoice_vehicle_category_id],
        "StorageRate"
      )

      #      invoice.job.estimate_invoice(storage_rate,@invoice)
      if storage_rate
        estimate_invoice(storage_rate, invoice.job)
      end

    end

    render "api/v1/invoices/show"
  end

  def paid
    service = InvoiceService::MarkPaid.new(
      invoice_id: params[:id],
      api_company_id: api_company.id,
      paid: paid_params[:paid]
    )

    service.call.save_mutated_objects

    @invoice = service.invoice

    render 'api/v1/invoices/show', status: :ok
  end

  protected

  def invoice_params
    logger.debug "#{controller_name} invoice_params:#{ALLOWED_ATTRIBS}"

    # we're safe to extract the hash from it, since only the permitted params will pass.
    args = params.require(:invoice).permit(ALLOWED_ATTRIBS).to_h

    attributize(args, [:line_items, :job])

    args
  end

  ALLOWED_ATTRIBS = [
    :state,
    :description,
    :job_id,
    :recipient_id,
    :recipient_type,
    :rate_type,
    :paid,
    :email_to,
    :email_cc,
    :email_bcc,
    :qb_pending_import,
    :qb_imported_at,
    :billing_type,
    job: [
      :id,
      :driver_notes,
      :invoice_vehicle_category_id,
    ],
    line_items: [
      :id,
      :job_id,
      :quantity,
      :unit_price,
      :original_quantity,
      :original_unit_price,
      :net_amount,
      :estimated,
      :auto_calculated_quantity,
      :tax_amount,
      :total_amount,
      :user_created,
      :description,
      :deleted_at,
      :calc_type,
      :rate_id,
    ],
    payments: [
      :id,
      :type,
      :payment_method,
      :memo,
      :amount,
      :mark_paid_at,
      :deleted_at,
      :qb_pending_import,
      :qb_imported_at,
    ],
  ].freeze

  private

  def state_change(p, method_name)
    logger.debug "invoice state update"
    if p.size > 0
      err = "Override has other parameters: #{p}, #{method_name}"
      logger.warn err
    end
    logger.debug "calling method name #{method_name}"
    if @invoice.respond_to? method_name
      @invoice.public_send(method_name)
    else
      err = "Invalid state transition - #{method_name}"
      logger.error err
      raise ArgumentError, err
    end
  end

  def set_invoice_uuid
    @invoice = Invoice.find_by!({ uuid: params[:uuid] })
  end

  def parse_query
    invoice_params_service =
      InvoiceService::MapInvoiceListFilters.new(
        company_id: api_company.id,
        params: params
      ).call

    conditions = invoice_params_service.conditions
    page       = invoice_params_service.page
    per_page   = invoice_params_service.per_page
    order      = build_order

    Rails.logger.debug "InvoicesController parse_query results: conditions: #{conditions} " \
      "page: #{page}, per_page: #{per_page}, order: #{order}"

    [conditions, page, per_page, order]
  end

  def build_order
    order = []
    parse_order do |col, direction|
      if col == "amount"
        order << { total_amount: direction }
      elsif col == "fleet_alert"
        order << { fleet_alert: direction }
      elsif col == "partner_alert"
        order << { partner_alert: direction }
      elsif col == "id"
        order << { job_id: direction }
      elsif col == "account"
        order << "jobs.accounts.name #{direction}"
      elsif col == "partner"
        order << "jobs.rescue_companies_jobs.name #{direction}"
      else
        raise ArgumentError, "Unknown column for ordering: #{col}"
      end
    end

    order << { created_at: :desc } if order.length == 0
    order
  end

  def set_invoices
    conditions, page, per_page, order = parse_query
    set_conditional_invoices(conditions, page, per_page, order)

    scopes = current_scopes
    if !scopes[:job_search].nil?
      db_order = nil
      db_order_direction = nil
      parse_order do |col, direction|
        if db_order.nil? || db_order_direction.nil?
          db_order = col
          db_order_direction = direction
        else
          db_order += "," + col
          db_order_direction += "," + direction
        end
      end
      # order, order_direction = parse_order.first
      search = Search.new(term: scopes[:job_search],
                          target: "#{REST_CLASS}",
                          company: api_company,
                          user: api_user,
                          url: request.fullpath,
                          scope: scopes.to_json,
                          order: db_order,
                          order_direction: db_order_direction,
                          page: params[:page] || 1,
                          per_page: params[:per_page] || 1000)
      search.save!
    end
  end

  def estimate_invoice(rate, job)
    @invoice.state = 'partner_new'
    UpdateInvoiceForJob.new(@invoice,
                            job,
                            rate).call
    @invoice.state = 'created'
    @invoice.calculate_total_amount
  end

  def paid_params
    params.require(:invoice).permit([:paid])
  end

end
