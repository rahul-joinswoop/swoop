# frozen_string_literal: true

class API::V1::MetricsController < API::APIController

  protect_from_forgery with: :null_session

  before_action :doorkeeper_authorize! # Authentication

  before_action :api_authorized_user # Authorization

  # post /api/v1/metrics
  def create
    args = metric_params
    @metric = Metric.new(args)
    @metric.company = api_company
    @metric.user = api_user
    respond_to do |format|
      if @metric.save
        format.json { render 'api/v1/metrics/show', status: :created }
      else
        format.json { render json: @metric.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def metric_params
    params.require(:metric).permit(:target_id,
                                   :target_type,
                                   :action,
                                   :label,
                                   :value)
  end

end
