# frozen_string_literal: true

class API::V1::AutocompleteController < API::APIController

  # authorization and authentication on children controllers

  # primarily used on invoice filters
  def rescue_companies_by_provider
    companies = API::AutoComplete::RescueCompaniesByProvider.new(
      term: ac_params[:term],
      api_company: api_company,
      limit: ac_params[:limit]
    ).call

    render json: companies, each_serializer: AutocompleteCompanySerializer
  end

  # primarily used on reporting filters
  def rescue_providers
    rescue_providers = API::AutoComplete::RescueProviders.new(
      term: ac_params[:term],
      api_company: api_company,
      limit: ac_params[:limit]
    ).call

    render json: rescue_providers, each_serializer: AutocompleteRescueProviderSerializer
  end

  # used on choose_partner modal
  def recommended_providers
    recommended_providers = API::AutoComplete::RecommendedProviders.new(
      term: ac_params[:term],
      api_company: api_company,
      job_id: ac_params[:job_id],
      limit: ac_params[:limit]
    ).call

    render json: recommended_providers, each_serializer: RecommendedProviderSerializer
  end

  # primarily used on reporting filters by partners and on invoice filters by Root and Partner
  def accounts
    accounts = API::AutoComplete::Accounts.new(
      term: ac_params[:term],
      api_company: api_company,
      limit: ac_params[:limit]
    ).call

    render json: accounts, each_serializer: AutocompleteAccountSerializer
  end

  def ac_params
    params.require(:autocomplete).permit(
      :type,
      :term,
      :limit,
      :job_id
    )
  end

end
