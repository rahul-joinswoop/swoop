# frozen_string_literal: true

class API::V1::ProvidersController < API::APIController

  REST_CLASS = RescueProvider

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet_or_swoop_dispatcher,
                except: [:tire_program, :unique] # Authorization Also allows root

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_provider, only: [:show, :update]
  before_action :set_providers, only: [:index]

  def index
    logger.debug "providers: #{@providers.count}"
    render template: "api/v1/providers/index"
  end

  def show
    render template: "api/v1/providers/show"
  end

  def update
    args = provider_params
    tags = remove_param(:tags, args)
    # TODO: this should probably go ina  transaction
    tag(tags)

    @provider.assign_attributes(args)

    respond_to do |format|
      if @provider.save
        format.json { render "api/v1/providers/show", status: :ok }

        # Fix any iscss that might have been affected by this change
        # (specifically deleted_at changing)
        #
        # IsscFixLogins.perform_async
        # IsscFixLogouts.perform_async

      else
        format.json { render json: @provider.errors, status: :unprocessable_entity }
      end
    end
  end

  def tag(tags)
    if tags
      @provider.tags = []
      tags.each do |tag_id|
        tag = Tag.find(tag_id.to_i)
        @provider.tags << tag
      end
    end
  end

  def batch
    set_batch_providers

    render template: "api/v1/providers/index"
  end

  # used by FE when loading a provider using an attribute as filter.
  # ex. ProviderStore.getBySiteId()
  def search_batch
    return [] if !params[:filter]

    # {{host}}/api/v1/providers/searchBatch?filter=site_id=1&provider_id=19
    # (filters are encoded, will be something like filter=site_id%3D1%26provider_id%3D19)
    conditions = { company_id: api_company.id }.merge(search_filters)

    @providers = RescueProvider.where(conditions).includes(includes_defaults)
    pagination_hack

    render template: "api/v1/providers/index"
  end

  def tire_program
    api_authorized_tesla

    @providers = RescueProvider.includes(includes_defaults)
      .where(company_id: api_company.id, sites: { tire_program: true })
    pagination_hack

    render template: "api/v1/providers/index"
  end

  # used by FE on Rates table
  def grouped_by_partner
    api_authorized_fleet_in_house

    @providers = RescueProvider.grouped_by_partner(api_company, includes_defaults)
    pagination_hack

    render template: "api/v1/providers/index"
  end

  def provider_params
    obj = params
    # map over tire program
    if !params[:provider].nil?
      if !params[:provider][:tire_program].nil?
        obj[:provider][:site_attributes] = {
          id: @provider.site_id,
          tire_program: params[:provider][:tire_program],
        }
      end
      if !obj[:provider][:location].nil?
        obj[:provider][:location_attributes] = obj[:provider][:location]
      end
    end
    obj.require(:provider).permit(:name,
                                  :phone,
                                  :primary_email,
                                  :accounting_email,
                                  :dispatch_email,
                                  :primary_contact,
                                  :primary_email,
                                  :primary_phone,
                                  :auto_assign_distance,
                                  :status,
                                  :notes,
                                  :fax,
                                  :deleted_at,
                                  :location_code,
                                  :vendor_code,
                                  location_attributes: [:address, :lat, :lng, :place_id, :exact],
                                  site_attributes: [:id, :tire_program],
                                  tags: [])
  end

  private

  def includes_defaults
    [
      :location,
      :site,
      :tags,
      provider: [
        :location,
        :features,
        :sites,
      ],
      company: [
        :location,
        :primary_contact,
        :addons,
        :services,
        :settings,
      ],
    ]
  end

  # phone, primary_email, notes

  def search_filters
    return nil if !params[:filter]

    params[:filter].split('&').reduce({}) do |memo, _filter|
      key_value = _filter.split('=')

      return memo if key_value.length < 2 # means there are no values for the key

      key = key_value[0]
      value = key_value[1].split(',')

      memo[key] = value

      memo
    end
  end

  def set_providers
    # TODO: fleet should get deleted providers as they have important data on providers for
    # approving invoices (these should be hidden in the UI though)
    where = { company: api_company }
    if params['live']
      where[:live] = true
    end

    @providers = RescueProvider.where(where).includes(includes_defaults)
    pagination_hack
  end

  def pagination_hack
    # HACK - this should always be enabled by default.
    if params[:page] || params[:per_page]
      @providers = @providers.paginate(page: params[:page], per_page: params[:per_page])
    end
  end

  def set_provider
    @provider = RescueProvider.find_by!({ id: params[:id], company: api_company })
  end

  def set_batch_providers
    if params[:provider_ids]
      execute_batch_query
    else
      @providers = []
    end
  end

  def execute_batch_query
    provider_ids = params[:provider_ids].split(',')

    @providers = RescueProvider.where(
      id: provider_ids, company_id: api_company.id
    )
      .includes(includes_defaults)
      .order(created_at: :desc)

    pagination_hack
  end

end
