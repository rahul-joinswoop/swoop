# frozen_string_literal: true

# Concern to allow reuse of common behaviour when rejecting invoices as Swoop or
# Fleet InHouse.
module API::V1::Shared::InvoiceRejections

  extend ActiveSupport::Concern

  # Controllers using the +create+ action must implement a method
  # +invoice_state_event+ to specify the event to run on the specified invoice.
  #
  # It implements a POST create method that expect params with the form:
  #
  #   { invoice_rejection: { invoice_id:, invoice_reject_reason_id:, notes: } }
  #
  # Raises AASM::InvalidTransition when the invoice loaded through
  # params[:invoice_rejection][:invoice_id] can't transition to the state
  # returned by +invoice_state_event+
  def create
    @invoice_rejection = create_invoice_rejection
    render "api/v1/invoice_rejections/show"
  rescue CreateInvoiceRejection::InvalidInvoiceRejectionError
    message = {
      message: I18n.t("invoice_rejections_controller.error.invalid"),
    }
    render json: message, status: :bad_request
  end

  def reasons
    @reasons = InvoiceRejectReason.fetch_company_reasons(api_company)
    render "api/v1/invoice_rejections/reasons"
  end

  def index
    @invoice_rejections =
      InvoiceRejection.get_all_by_invoice_ids_and_company(
        params[:invoice_ids], api_company
      )

    if @invoice_rejections
      render "api/v1/invoice_rejections/index"
    else
      message = { status: "404", message: "Not Found" }
      render json: message, status: :not_found
    end
  end

  private

  def create_invoice_rejection
    invoice = Invoice.find invoice_rejection_params[:invoice_id]
    invoice_reject_reason = get_invoice_reject_reason(
      invoice_rejection_params[:invoice_reject_reason_id]
    )

    CreateInvoiceRejection
      .new(
        invoice: invoice,
        invoice_state_event: invoice_state_event,
        invoice_reject_reason: invoice_reject_reason,
        notes: invoice_rejection_params[:notes]
      )
      .call_with_transaction
      .invoice_rejection
  end

  def invoice_rejection_params
    params[:invoice_rejection]
  end

  def get_invoice_reject_reason(invoice_reject_reason_id)
    reason = InvoiceRejectReason.find(invoice_reject_reason_id)
    raise SecurityError if reason.company != api_company
    reason
  end

end
