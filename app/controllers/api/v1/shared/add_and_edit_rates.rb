# frozen_string_literal: true

# Concern to allow reuse of common behaviour when creating and editing rates
# as Partner or Fleet InHouse.
#
# It requires the following methods to be implemented by controllers:
#
#   - `rates_provider` to return a +Company+, provider that rates are associated
#     with
#   - `rates_account` to return an +Account+ that rates are associated with, can
#     be `nil`
#   - `rate_args` to return an array of attributes allowed to be saved for a
#     rate
module API::V1::Shared::AddAndEditRates

  extend ActiveSupport::Concern

  def create
    rates = params[:rates].map { |rate_params| prepare_attributes(rate_params) }

    create_service = RateService::CreateMultiple.new(
      rates: rates, company: rates_provider, account: rates_account
    )

    ServiceRunner.new.tap do |runner|
      runner << create_service
      runner << mail_rate_change_service
    end.call_all

    @rates = create_service.created_rates

    render "api/v1/rates/index", status: :created
  end

  def update_multi
    rates = params[:rates].each_with_object({}) do |rate_params, rates_memo|
      rates_memo[rate_params[:id]] = prepare_attributes(rate_params)
    end

    update_service = RateService::UpdateMultiple.new(
      rates: rates, company: rates_provider
    )

    ServiceRunner.new.tap do |runner|
      runner << update_service
      runner << mail_rate_change_service
    end.call_all

    @rates = update_service.saved_rates

    render "api/v1/rates/index", status: :ok
  end

  private

  def prepare_attributes(rate_params)
    args = rate_params.permit(rate_args)

    additions = args[:additions]

    return args unless additions

    args[:additions_attributes] = additions
    args.delete(:additions)
    args
  end

end
