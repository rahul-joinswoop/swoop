# frozen_string_literal: true

module API::V1::Shared::Stripe::HandleEvent

  extend ActiveSupport::Concern

  protected

  def handle_stripe_account_event
    find_handler_and_handle_stripe_event!(
      event_type_method: :find_account_handler_by,
      webhook_secret: ::Configuration.stripe.account_webhook_secret,
    )
  end

  def handle_stripe_connect_event
    find_handler_and_handle_stripe_event!(
      event_type_method: :find_connect_handler_by,
      webhook_secret: ::Configuration.stripe.connect_webhook_secret,
    )
  end

  private

  def find_handler_and_handle_stripe_event!(event_type_method:, webhook_secret:)
    payload = request.raw_post
    sig_header = request.env['HTTP_STRIPE_SIGNATURE']

    handler_instance = StripeIntegration::EventHandlers::Base.send(
      event_type_method,
      payload: payload,
      sig_header: sig_header,
      webhook_secret: webhook_secret,
    )

    handler_instance.handle
  end

end
