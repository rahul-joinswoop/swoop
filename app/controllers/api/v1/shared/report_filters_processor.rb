# frozen_string_literal: true

module API::V1::Shared::ReportFiltersProcessor

  extend ActiveSupport::Concern

  TRANSACTION_TYPES = {
    1 => 'only_payments',
    2 => 'only_discounts',
    3 => 'only_refunds',
    4 => 'only_writeoffs',
    5 => 'only_swoop_charges',
  }.freeze

  PAYMENT_STATUSES = {
    1 => 'charged',
    2 => 'refunded',
  }.freeze

  def process_params_for_payments_report(params)
    process_transaction_type(params)

    process_payment_status(params)

    invoice_payment_method = params['invoice_payment_method'] || 0
    params.delete('invoice_payment_method') if invoice_payment_method.to_i.zero?

    Rails.logger.debug(
      "REPORT(#{@company_report.report.id}) process_params_for_payments_report params #{params}"
    )

    params
  end

  private

  # primarily used by Payments Report
  def process_transaction_type(params)
    transaction_type = params['transaction_type'].to_i

    key = TRANSACTION_TYPES[transaction_type]

    params[key] = true unless key.nil?
    params.delete 'transaction_type'

    params
  end

  # primarily used by Payments Report
  def process_payment_status(params)
    payment_status = params['payment_status'].to_i

    key = PAYMENT_STATUSES[payment_status]

    params[key] = true unless key.nil?
    params.delete 'payment_status'

    params
  end

end
