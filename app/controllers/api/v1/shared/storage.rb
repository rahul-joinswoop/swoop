# frozen_string_literal: true

# concern to create an storage for a future new static object.
# mainly used by:
# Api::V1::AttachedDocumentsController -> to create job.attached_documents from job details
# Api::V1::CompaniesController -> to create company.attached_documents for company logo
module API::V1::Shared::Storage

  extend ActiveSupport::Concern

  def presign_url(key)
    bucket = FileUploader.new(:cache).storage.bucket
    object = bucket.object("cache/#{key}")
    object.presigned_url(:put)
  end

  def presign
    key = SecureRandom.hex
    url = presign_url key
    json = <<-JSON
      {
        "id":"#{key}",
        "storage":"cache",
        "url":"#{url}"
      }
    JSON

    respond_to do |format|
      format.json { render json: json }
    end
  end

end
