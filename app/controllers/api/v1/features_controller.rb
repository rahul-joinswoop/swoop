# frozen_string_literal: true

class API::V1::FeaturesController < ApplicationController

  # GET /features
  # GET /features.json
  def index
    @features = Feature.where(deleted_at: nil)
    render 'api/v1/features/index'
  end

end
