# frozen_string_literal: true

class API::V1::TagsController < API::APIController

  before_action :api_authorized_user

  def index
    @tags = api_company.tags.not_deleted

    render json: @tags
  end

  def create
    @tag = TagServices::Create.call(
      input: {
        api_company_id: api_company.id,
        tag_attributes: tag_attributes,
      }
    ).output

    render json: @tag
  end

  def update
    @tag = TagServices::Update.call(
      input: {
        api_company_id: api_company.id,
        tag_id: params[:id],
        tag_attributes: tag_attributes,
      }
    ).output

    render json: @tag
  end

  def destroy
    @tag = TagServices::Delete.call(
      input: {
        api_company_id: api_company.id,
        tag_id: params[:id],
      }
    ).output

    render json: @tag
  end

  def batch
    tag_ids = params[:tag_ids]

    return render json: [] if tag_ids.blank?

    @tags = TagServices::Batch.call(
      input: {
        api_company_id: api_company.id,
        tag_ids: tag_ids.split(",").map(&:to_i),
      }
    ).output

    render json: @tags
  end

  private

  def tag_attributes
    params.require(:tag).permit(
      :name,
      :color,
      :deleted_at
    ).to_h
  end

end
