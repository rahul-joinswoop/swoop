# frozen_string_literal: true

# Quickbooks integration. All the magic here is happening in stored procedures. The consumer
# for this API is a Windows application.
class API::V1::Qbd::V0Controller < ActionController::Base

  # GET
  def accounts
    # user = ApplicationRecord.execute_query("SELECT current_user").getvalue(0, 0)
    # Rails.logger.debug "request:#{request}, secret:#{secret}, pass:#{pass}, user:#{user}"
    # PDW: note to self - added superuser to towuser locally to overcome  'permission denied for relation'
    records = ApplicationRecord.execute_query("SELECT * FROM get_accounts(?)", secret)
    render json: records.getvalue(0, 0)
  end

  def invoices
    records = ApplicationRecord.execute_query("SELECT * FROM get_invoice_headers(?)", secret)
    render json: records.getvalue(0, 0)
  end

  def payments
    records = ApplicationRecord.execute_query("SELECT * FROM get_payments(?)", secret)
    render json: records.getvalue(0, 0)
  end

  def lineitems
    records = ApplicationRecord.execute_query("SELECT * FROM get_invoice_lines(?)", secret)
    render json: records.getvalue(0, 0)
  end

  def items
    records = ApplicationRecord.execute_query("SELECT * FROM get_items(?)", secret)
    render json: records.getvalue(0, 0)
  end

  def user
    records = ApplicationRecord.execute_query("SELECT * FROM get_user(?)", secret)
    render json: records.getvalue(0, 0)
  end

  # PUT

  def imported
    params.require(:ledger_id)
    ApplicationRecord.execute_query("SELECT * FROM mark_imported(?, ?)", secret, params[:ledger_id])
    render json: {}
  end

  def onboard
    params.require(:qb_imported_at)
    ApplicationRecord.execute_query("SELECT * FROM set_qb_imported_at(?, ?)", secret, params[:qb_imported_at])
    render json: {}
  end

  private

  # The secret is being passed as the username via HTTP Basic Auth.
  def secret
    secret, _password = ActionController::HttpAuthentication::Basic.user_name_and_password(request)
    secret
  end

end
