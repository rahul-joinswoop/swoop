# frozen_string_literal: true

class API::V1::SettingsController < API::APIController

  REST_CLASS = Setting

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_settings, only: [:index]
  before_action :set_setting, only: [:update, :destroy, :show]

  def index
    render template: "api/v1/settings/index"
  end

  def show
    logger.debug "Rendering #{@setting}"
    render template: "api/v1/settings/show"
  end

  def create
    @setting = Setting.find_or_initialize_by({
      company: api_company,
      key: setting_params[:key],
    })
    @setting.value = setting_params[:value]

    respond_to do |format|
      if @setting.save
        format.json { render 'api/v1/settings/show', status: :created }
      else
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @setting.update(setting_params)
        format.json { render "api/v1/settings/show", status: :ok }
      else
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @setting.destroy!

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def set_settings
    @settings = Setting.where({ company: api_company })
  end

  def set_setting
    @setting = Setting.find_by!({ id: params[:id], company: api_company })
  end

  def setting_params
    @setting_params ||= params.require(:setting).permit(:key, :value)
  end

end
