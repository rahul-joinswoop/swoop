# frozen_string_literal: true

# NOTE: will replace Api::V1::Root::JobsSearchController
class API::V1::Root::JobSearchController < API::V1::JobSearchController

  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

end
