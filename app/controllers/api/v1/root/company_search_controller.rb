# frozen_string_literal: true

class API::V1::Root::CompanySearchController < API::V1::CompanySearchController

  before_action :api_authorized_root_or_swoop_dispatcher

  # live sites and company_service_questions are not required for Root
  # live sites and company_service_questions are required for Fleet and Partner Users
  # overide the base incls method to remove live_sites: [:location]
  def incls
    [
      :location,
      :service_codes,
      :accounts,
      :features,
      :vehicle_categories,
      :fleet_in_house_clients,
      :fleet_managed_clients,
      companies_customer_types: [:customer_type],
    ]
  end

  private

  def template
    "api/v1/root/company_search/index"
  end

  def apply_filters
    # TODO there should be a better approach to this
    params[:filters][:must_not] = { exists: [:parent_company_id, :deleted_at] }
  end

end
