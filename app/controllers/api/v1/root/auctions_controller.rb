# frozen_string_literal: true

class API::V1::Root::AuctionsController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

  def index
    @auctions = Auction::Auction.where(status: Auction::Auction::LIVE)

    render json: @auctions, scope: api_company
  end

end
