# frozen_string_literal: true

class API::V1::Root::PartialJobsController < API::V1::PartialJobsController

  before_action :api_authorized_root_or_swoop_dispatcher

  # Used on Done tab
  has_scope :filter_fleet_managed_jobs, as: :fleet_managed_jobs

  def get_jobs(conditions, page, per_page, order, includes)
    Rails.logger.debug "Root::PartialJobsController::get_jobs - #{conditions},#{page},#{per_page},#{order}"

    jobs = apply_scopes(Job.all)
      .where(conditions)
      .paginate(page: page, per_page: per_page)
      .order(order)
      .includes(includes)

    jobs_total = apply_scopes(Job.all)
      .where(conditions).count

    [jobs, jobs_total]
  end

  def get_all_fields
    COMMON_FIELDS
  end

end
