# frozen_string_literal: true

class API::V1::Root::InvoicesController < API::V1::InvoicesController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root_or_swoop_dispatcher # Authorization
  before_action :set_invoice, only: [:show, :update, :paid]

  def incoming_or_outgoing
    incomming_states = ['partner_offline_new', 'partner_sent', 'swoop_approved_partner', 'swoop_downloaded_partner']
    outgoing_states = ['swoop_new', 'swoop_sent', 'swoop_approved_fleet', 'swoop_sending_fleet', 'swoop_sent_fleet']

    conditions, _page, _per_page = parse_query
    conditions[:recipient_type] = "Account"

    switch_state = conditions[:state][0]
    logger.debug "switching on '#{switch_state}'"

    if incomming_states.include? switch_state
      logger.debug "incoming request #{switch_state}"
      incoming
    elsif outgoing_states.include? switch_state
      logger.debug "outgoing request #{switch_state}"
      outgoing
    else
      raise ArgumentError, "could not determine incoming or outgoing from #{switch_state}"
    end
  end

  def controller_name
    'root'
  end

  def incoming
    conditions, page, per_page, order = parse_query

    # TODO: Only show fleet managed invoices
    # conditions[:recipient_type]="Account"
    logger.debug "incoming, using #{conditions[:state]}"
    @invoices = apply_scopes(api_company.incoming_invoices)
      .associated_rendered_models
      .joins(:job)
      .where("jobs.type = 'FleetManagedJob'")
      .where(conditions).paginate(page: page, per_page: per_page).order(order)

    render "api/v1/invoices/index"
  end

  def outgoing
    conditions, page, per_page, order = parse_query

    # TODO: Only show fleet managed invoices
    # conditions[:recipient_type]="Account"
    @invoices = apply_scopes(api_company.outgoing_invoices)
      .associated_rendered_models
      .joins(:job)
      .where("jobs.type = 'FleetManagedJob'")
      .where(conditions)
      .paginate(page: page, per_page: per_page)
      .order(order)

    render "api/v1/invoices/index"
  end

  # # #
  #
  # This action will trigger the bulk invoices downloading
  # by client_company_id. This is used by Swoop admin users
  # on the UI: invoices tab / client approved invoices / filter by company / "Download All"
  #
  # It will return an async_request created by InvoiceService::SwoopDownloadAll.
  #
  def download_fleet
    Rails.logger.debug "Root::InvoicesController#download_fleet called"

    service = InvoiceService::SwoopDownloadAll.new(
      report_params: fleet_report_params.to_h.merge(sender_id: api_company.id),
      api_company_id: api_company.id,
      api_user_id: api_user.id,
      sender_alias: :swoop
    )

    service.call

    if service.invoice_ids.present?
      render json: { async_request: service.async_request }
    else
      render json: { async_request: service.async_request }, status: :not_found
    end
  end

  # # #
  #
  # This action will trigger the invoices export bulk operation, for the given
  # client_company_id. This is used by Swoop admin users
  # on the UI: invoices tab / client approved invoices / filter by company / "Send All"
  #
  # It will return an async_request created by InvoiceService::SwoopExportAllToFleet.
  #
  def export_fleet
    Rails.logger.debug "Root::InvoicesController#export_fleet called"

    service = InvoiceService::SwoopExportAllToFleet.new(
      api_company_id: api_company.id,
      api_user_id: api_user.id,
      client_company_id: params[:client_company_id]
    )

    service.call

    if service.invoice_ids.present?
      render json: { async_request: service.async_request }
    else
      render json: { async_request: service.async_request }, status: :not_found
    end
  end

  # DEPRECATED - should use download_all_partner instead
  def download_partner
    Rails.logger.debug "Root::InvoicesController#download_partner called"

    @report = Report.swoop_partner_invoice

    Rails.logger.debug "Root::InvoicesController#download_partner report found #{@report.id}"

    @report_result = company_report.run(
      api_user,
      { 'state' => 'swoop_approved_partner' }.merge(partner_report_params.to_h),
      :process_invoice_state_partner
    )

    render 'api/v1/report_results/show'
  end

  # # #
  #
  # This action will trigger the bulk invoices downloading
  # by sender_id. This is used by Swoop admin users
  # on the UI: invoices tab / partner approved invoices / filter by company / "Download All"
  #
  # It will return an async_request created by InvoiceService::SwoopDownloadAll.
  #
  def download_all_partner
    Rails.logger.debug "#{self.class.name}#download_all_partner called"

    service = InvoiceService::SwoopDownloadAll.new(
      report_params: partner_report_params.to_h.merge(client_company_id: api_company.id),
      api_company_id: api_company.id,
      api_user_id: api_user.id,
      sender_alias: :partner
    )

    service.call

    if service.invoice_ids.present?
      render json: { async_request: service.async_request }
    else
      render json: { async_request: service.async_request }, status: :not_found
    end
  end

  def build_order
    order = []
    parse_order do |col, direction|
      if col == "amount"
        order << { total_amount: direction }
      elsif col == "fleet_alert"
        order << { fleet_alert: direction }
      elsif col == "partner_alert"
        order << { partner_alert: direction }
      elsif col == "id"
        order << { job_id: direction }
      elsif col == "account"
        order << "jobs.accounts.name #{direction}"
      elsif col == "partner"
        order << "jobs.rescue_companies_jobs.name #{direction}"
      else
        raise ArgumentError, "Unknown column for ordering: #{col}"
      end
    end

    order << { created_at: :desc } if order.length == 0
    order
  end

  # Checks if the 'state' is actually an 'event' that triggers a state transition
  # already supported by InvoiceService.
  #
  # If not supported, call super (/api/v1/invoices_controller#update) to
  # keep compatibility with legacy code and return.
  #
  # Now, if it's supported by InvoiceService, retrieves the specif service class with
  # InvoiceService.transition_for and executes it.
  #
  # TODO both /api/v1/root/invoices/:id and /api/v1/partner/invoices/:id implements this.
  #       It should be refactored to one endpoint only, maybe in parent controller
  #       (/api/v1/invoices/:id)
  #
  # PATCH/PUT /api/v1/root/invoices/:id
  def update
    if invoice_state_has_transition_service?
      InvoiceService.transition_for(params[:invoice][:state].to_sym).new(
        invoice_id: params[:id],
        api_company_id: api_company.id,
        api_user_id: api_user.id
      ).call.save_mutated_objects

      render 'api/v1/invoices/show', status: :ok
    else
      super && return
    end
  rescue InvoiceService::MissingInvoiceError
    render json: {
      message: I18n.t('invoices_controller.error.not_found'),
      action: I18n.t('invoices_controller.error.default_action'),
    }, status: :bad_request
  rescue InvoiceService::InvoiceStateTransitionError
    render json: {
      message: I18n.t('invoices_controller.error.invalid_transition'),
      action: I18n.t('invoices_controller.error.default_action'),
    }, status: :unprocessable_entity
  rescue InvoiceService::MissingFleetManagedInvoiceError
    render json: {
      message: I18n.t('invoices_controller.error.fleet_managed.not_found'),
      action: I18n.t('invoices_controller.error.default_action'),
    }, status: :unprocessable_entity
  end

  private

  def company_report
    @company_report ||= CompanyReport.new(company: api_company, report: @report)
  end

  def invoice_state_has_transition_service?
    params[:invoice][:state] && InvoiceService::TRANSITION_SERVICES.key?(
      params[:invoice][:state].to_sym
    )
  end

  # For root this will take the company_id from params, and it should always be a Partner's one
  def invoice_sender
    if params[:company_id]
      RescueCompany.find(params[:company_id]) # used when POST root/invoices/samplerate
    else
      RescueCompany.find(params[:invoice][:sender][:id]) # used when POST root/invoices
    end
  end

  def set_invoice
    @invoice = Invoice.includes([:line_items, :payments_or_credits]).find_by!({ id: params[:id] })
  end

  def partner_report_params
    params.require(:report).permit(
      :sender_id,
      :job_active,
      :client_company_id,
      :job_search,
      :job_after, # not sure if used, but FE sends
      :job_before, # not sure if used, but FE sends
    )
  end

  def fleet_report_params
    params.require(:report).permit(
      :client_company_id,
      :job_active,
      :job_after, # not sure if used, but FE sends
      :job_before, # not sure if used, but FE sends
    )
  end

end
