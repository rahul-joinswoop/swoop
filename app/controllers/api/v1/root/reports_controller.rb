# frozen_string_literal: true

class API::V1::Root::ReportsController < API::V1::ReportsBaseController

  before_action :api_authorized_root # Authorization

end
