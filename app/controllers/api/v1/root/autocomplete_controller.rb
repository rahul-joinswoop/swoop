# frozen_string_literal: true

class API::V1::Root::AutocompleteController < API::V1::AutocompleteController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root, except: [:recommended_providers, :root_users] # Authorization
  before_action :api_authorized_root_or_swoop_dispatcher, only: [:recommended_providers, :root_users]

  ALLOWED_COMPANY_TYPES = ['RescueCompany', 'FleetInHouseCompany', 'FleetManagedCompany', 'Company'].freeze

  def company
    args = ac_params
    company_type = args[:type]
    raise ArgumentError, "Unsupported Type: #{company_type}" unless ALLOWED_COMPANY_TYPES.include?(company_type)

    companies = API::AutoComplete::Company.new(company_type, args[:term], args[:limit]).call

    render json: companies, each_serializer: AutocompleteCompanySerializer
  end

  # Autocomplete lookup for root (Swoop admin) users.
  def root_users
    args = ac_params
    users = API::AutoComplete::RootUsers.new(args[:term], limit: args[:limit]).call
    render json: users, each_serializer: SimpleUserSerializer
  end

end
