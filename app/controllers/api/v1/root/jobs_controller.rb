# frozen_string_literal: true

class API::V1::Root::JobsController < API::V1::JobsBaseController

  REST_CLASS = Job

  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_job, only: [:show, :update, :destroy, :recommended_providers, :recommended_provider, :billing_info]
  before_action :set_provider, only: [:recommended_provider]
  before_action :set_jobs, only: [:index]

  # {"job":{"id":3279,"rescue_provider_id":5064}}
  def update
    params_to_update = map_params
    raise ArgumentError, "Old status method, use jobs_status/update controller" if remove_param(:status, params_to_update)

    rescue_provider_id = remove_param(:rescue_provider_id, params_to_update)
    remove_param(:dispatch_email, params_to_update)

    if rescue_provider_id
      logger.warn "provider updates on job depricated, use /job/<id>/choose_partner API"

      return redirect_to url_for(controller: :jobs_choose_partner, action: :update)
    end

    if params_to_update[:root_dispatcher_id].present?
      params_to_update[:has_or_had_a_root_dispatcher_assigned] = true
    end

    # ENG-8569: API::Jobs::UpdateFleet should be renamed API::Jobs::UpdateFleetManagedJob
    # because it's not updating a fleet – it's updating a fleet-managed job
    @job = API::Jobs::UpdateFleet.new(
      company: api_company,
      api_user: api_user,
      job_id: params[:id],
      params: params_to_update,
      job_history: params.dig(:job, :history),
      job_storage: params.dig(:job, :storage),
      browser_platform: browser.platform,
      request_user_agent: request.user_agent,
      save_as_draft: params[:save_as_draft]
    ).call.job

    if @job.valid?
      render template: 'api/v1/jobs/show', status: :ok
    else
      raise ArgumentError, @job.errors.to_json
    end
  end

  # override for root
  def done
    where = { status: Job.done_states, type: "FleetManagedJob", deleted_at: nil }
    set_conditional_jobs where
    render "api/v1/jobs/index"
  end

  # override for root
  def map
    set_conditional_jobs status: Job.map_states, type: "FleetManagedJob", deleted_at: nil
    render json: @jobs
  end

  def active
    set_conditional_jobs status: Job.active_and_draft_states, type: "FleetManagedJob", deleted_at: nil
    render "api/v1/jobs/index"
  end

  def change_provider_to
    @job = Job.find(params[:job_id])
    super
  end

  # Return billing info for the job. This information is only available to a root user.
  def billing_info
    billing_info = @job.billing_info
    if billing_info && billing_info.rescue_company_id == @job.rescue_company_id
      render json: {
        agent_id: billing_info.agent_id,
        billing_type: billing_info.billing_type,
        vcc_amount: billing_info.vcc_amount.to_f,
        goa_amount: billing_info.goa_amount.to_f,
        goa_agent_id: billing_info.goa_agent_id,
      }
    else
      render json: {}
    end
  end

  def getCompanyType
    "SuperCompany"
  end

  def getJoin
    :super_company
  end

  def recommended_providers
    @providers = JobGetClosestProvidersService.new(@job, api_company).call
    render json: @providers, each_serializer: RecommendedProviderSerializer
  end

  def recommended_provider
    @provider = JobGetProviderDistanceService.new(@job, @provider).call # [0..10]
    render json: @provider, serializer: RecommendedProviderSerializer
  end

  protected

  def job_filter(condition)
    apply_scopes(Job.all).where(condition).distinct(:id)
  end

  def extra_job_includes
    [
      { target_variants: [:variant] },
      :pcc_claim,
      :pcc_coverage,
    ]
  end

  private

  def set_job
    logger.debug "Looking for #{params[:id]}"
    @job = Job.find_by!({ id: params[:id] })
  end

  def set_provider
    @provider = RescueProvider.find_by!({ id: params[:provider_id] })
  end

  def job_params
    params.require(:job).permit(
      :first_name,
      :last_name,
      :phone,
      :email,
      :status,
      :service,
      :service_alias_id,
      :service_code_id,
      :make,
      :model,
      :year,
      :serial_number,
      :vehicle_type,
      :color,
      :license,
      :vin,
      :notes,
      :price,
      :scheduled_for,
      :po_number,
      :ref_number,
      :unit_number,
      :odometer,
      :vip,
      :uber,
      :alternate_transportation,
      :rescue_vehicle_id,
      :rescue_driver_id,
      :customer_type_id,
      :symptom_id,
      :bta,
      :no_keys,
      :keys_in_trunk,
      :four_wheel_drive,
      :blocking_traffic,
      :accident,
      :unattended,
      :reject_reason_id,
      :reject_reason_info,
      :fleet_notes,
      :driver_notes,
      :partner_notes,
      :dispatch_notes,
      :get_location_sms,
      :review_sms,
      :partner_sms_track_driver,
      :swoop_notes,
      :rescue_provider_id,
      :root_dispatcher_id,
      :deleted_at,
      :text_eta_updates,
      :review_delay,
      :fleet_manual,
      :department_id,
      :tire_type_id,
      :accounting_code,
      :send_sms_to_pickup,
      :priority_response,
      :policy_number,
      :fleet_site_id,
      :pcc_coverage_id,
      :invoice_vehicle_category_id,
      :drop_location,
      :customer_lookup_type,
      pickup_contact: [:id, :full_name, :first_name, :last_name, :phone],
      dropoff_contact: [:id, :full_name, :first_name, :last_name, :phone],
      service_location: [
        :id, :lat, :lng, :address, :exact, :place_id, :location_type_id, :site_id,
      ],
      drop_location: [
        :id, :lat, :lng, :address, :exact, :place_id, :location_type_id,
        :site_id,
      ],
      caller: [:id, :phone, :full_name],
      customer: [:id, :full_name, :phone, :member_number],
      owner_company: [:id],
      toa: [:latest],
      images: [:id, :url, :deleted_at],
      question_results: [:question_id, :answer_id, :answer_info],
      policy_location: [
        :id, :lat, :lng, :address, :state, :city, :street, :zip, :exact,
        :place_id,
      ]
    ).
      # There are two options for the drop_location:
      # 1. a Hash of a Location for the drop location, with certain permitted fields; we use this to set or change the drop location
      # 2. a nil; we use this to unset the drop location
      # Due to a limitation in StrongParameters, we can't say that. This is a workaround.
      delete_if do |param_key, param_value|
      param_key == "drop_location" && !param_value.nil? && param_value.empty?
    end
  end

  # PDW was going to use in the view, really needs policy based serialization
  #  def job_notes
  #    [:notes,:fleet_notes,:driver_notes,:partner_notes,:dispatch_notes,:swoop_notes]
  #  end
  helper_method :job_notes

end
