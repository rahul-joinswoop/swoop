# frozen_string_literal: true

class API::V1::Root::JobRestorationController < API::V1::Root::BaseController

  def create
    @job = Job.find(params[:job_id])
    JobService::RestorationService.call(job: @job)
    render json: { job: @job }
  end

end
