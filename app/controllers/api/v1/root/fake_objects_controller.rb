# frozen_string_literal: true

class API::V1::Root::FakeObjectsController < API::V1::Root::BaseController

  def create
    if ENV['SWOOP_ENV'] == 'production'
      Rails.logger.debug "Not creating fake data on production"
      return render json: {}
    end
    args = generate_params
    FakeWorker.perform_async(args['company_id'],
                             args['type'],
                             Integer(args[:count]))
    render json: {}
  end

  def generate_params
    params.require(:fake).permit(:company_id, :type, :count)
  end

end
