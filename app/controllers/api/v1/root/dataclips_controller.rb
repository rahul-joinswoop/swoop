# frozen_string_literal: true

class API::V1::Root::DataclipsController < API::APIController

  before_action :api_authorized_root
  before_action :set_dataclip, only: :show

  def index
    @dataclips = Report.all
  end

  def show
    @dataclip.refresh!
    @rows = paginate @dataclip.values
  end

  private

  def set_dataclip
    @dataclip = Report.find params[:id]
  end

end
