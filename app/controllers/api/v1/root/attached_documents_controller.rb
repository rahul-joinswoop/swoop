# frozen_string_literal: true

class API::V1::Root::AttachedDocumentsController < API::V1::AttachedDocumentsController

  before_action :api_authorized_root_or_swoop_dispatcher

  private

  def set_job
    @job = Job.find_by!(id: params[:job_id])
  end

end
