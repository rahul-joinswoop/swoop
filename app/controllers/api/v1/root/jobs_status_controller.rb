# frozen_string_literal: true

class API::V1::Root::JobsStatusController < API::V1::JobsStatusController

  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_job, only: [:update]

  def update_service
    SwoopUpdateJobState
  end

  private

  def set_job
    @job = Job.find_by!({ id: params[:id] })
  end

end
