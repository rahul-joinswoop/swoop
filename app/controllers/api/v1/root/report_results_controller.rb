# frozen_string_literal: true

class API::V1::Root::ReportResultsController < API::V1::ReportResultsBaseController

  before_action :api_authorized_root # Authorization

  before_action :set_report_result, only: [:show]

  def set_report_result
    @report_result = ReportResult.find_by!({ id: params[:id], company: api_company })
  end

end
