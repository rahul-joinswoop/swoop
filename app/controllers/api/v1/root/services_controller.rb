# frozen_string_literal: true

class API::V1::Root::ServicesController < API::V1::ServicesController

  REST_CLASS = ServiceCode

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

  # N.B. Don't move these up to base as they will get called before the
  # Authorization above

end
