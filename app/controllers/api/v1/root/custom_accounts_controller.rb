# frozen_string_literal: true

class API::V1::Root::CustomAccountsController < API::APIController

  REST_CLASS = CustomAccount

  before_action :doorkeeper_authorize!
  before_action :api_authorized_root

  def destroy
    async_request = CustomAccountServices::DeleteByRescueCompanyId.call(
      input: {
        api_company_id: api_company.id,
        api_user_id: api_user.id,
        rescue_company_id: params[:rescue_company_id],
      }
    ).output

    render json: { async_request: async_request }
  end

end
