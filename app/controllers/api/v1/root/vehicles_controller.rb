# frozen_string_literal: true

class API::V1::Root::VehiclesController < API::APIController

  REST_CLASS = Vehicle

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root_or_swoop_dispatcher # Authorization
  before_action :api_authorized_root, only: [:update]

  before_action :set_vehicles, only: [:index]
  before_action :set_vehicle, only: [:update]

  def index
    render template: "api/v1/vehicles/map_index"
  end

  # allow Swoop to load any vehicle, no restrictions (only not deleted)
  def batch
    if params[:vehicle_ids]
      @vehicles = RescueVehicle.where(
        id: params[:vehicle_ids],
        deleted_at: nil
      )
    else
      @vehicles = []
    end

    render template: "api/v1/vehicles/index"
  end

  def update
    p = vehicle_params
    @vehicle.update_location!(lat: p[:lat], lng: p[:lng], timestamp: Time.current)
    respond_to do |format|
      format.json { render "api/v1/vehicles/show", status: :ok, vehicle: @vehicle }
    end
  end

  def set_vehicle
    # we really don't need the api_root? check here - #set_vehicle is only
    # called for #update and #update is protected by #api_authorized_root
    if api_root?
      @vehicle = RescueVehicle.find_by!({ id: params[:id] })
    end
  end

  def set_vehicles
    # root and swoop dispatchers can see all vehicles here but this doesn't make
    # sense when compared to the partner VehiclesController#set_vehicles which
    # only returns all vehicles when we are logged in as root.
    @vehicles = RescueVehicle.where({ deleted_at: nil }).includes(:driver)
    # HACK - this should always be enabled by default.
    if params[:page] || params[:per_page]
      @vehicles = @vehicles.paginate(page: params[:page], per_page: params[:per_page])
    end
  end

  def vehicle_params
    params.require(:vehicle).permit(:lat,
                                    :lng,)
  end

end
