# frozen_string_literal: true

class API::V1::Root::UsersController < API::V1::Root::BaseController

  before_action :set_users, only: [:index]
  before_action :set_batch_users, only: [:batch]

  def index
    render 'api/v1/admin/users/index'
  end

  def restore
    @user = Users::RestorationService.call(undelete_params).user
    render json: { user: @user }
  end

  def authenticate_as
    user_id = as_params[:user_id]
    @oauth_access_token = Doorkeeper::AccessToken.create!(
      resource_owner_id: user_id, expires_in: 60 * 60 * 12
    )

    render "api/v1/oauth/show"
  end

  def set_users
    @users = User.all.includes(
      :company,
      :roles,
    ).with_any_role(:root, :fleet, :rescue).sort! do |a, b|
      a[:company_id] <=> b[:company_id]
    end
  end

  private

  def undelete_params
    params.permit(:username, :email)
  end

  def as_params
    params.permit(:user_id)
  end

end
