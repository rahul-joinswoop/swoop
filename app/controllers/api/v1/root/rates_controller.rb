# frozen_string_literal: true

class API::V1::Root::RatesController < API::V1::RatesController

  before_action :api_authorized_root_or_swoop_dispatcher # Authorization Also allows root

  def account_service
    set_account_service_index

    render template: "api/v1/rates/index"
  end

end
