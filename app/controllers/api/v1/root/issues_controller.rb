# frozen_string_literal: true

class API::V1::Root::IssuesController < API::V1::IssuesController

  before_action :api_authorized_root_or_swoop_dispatcher

end
