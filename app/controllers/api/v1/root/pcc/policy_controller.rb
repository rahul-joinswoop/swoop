# frozen_string_literal: true

class API::V1::Root::PCC::PolicyController < API::V1::PCC::PolicyController

  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

end
