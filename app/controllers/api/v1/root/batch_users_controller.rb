# frozen_string_literal: true

class API::V1::Root::BatchUsersController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

  def batch
    set_batch_users
    render json: @users, each_serializer: MinimalUserSerializer
  end

  def set_batch_users
    conditions = {}

    # {{host}}/api/v1/root/jobs/?job_ids=1076
    if params[:user_ids]
      conditions[:id] = params[:user_ids].split(',')
    end

    incls = [
      :company,
      :roles,
    ]
    @users = User.where(conditions).includes(*incls)
  end

end
