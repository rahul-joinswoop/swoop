# frozen_string_literal: true

# DEPRICATED (can be removed after VendorID Rates FE is shipped)
# use api/v1/tags instead as it treats all actions through services
class API::V1::Root::TagsController < API::APIController

  # before_action :set_feature, only: [:show, :edit, :update, :destroy]

  # GET /tag
  # GET /tag.json
  def index
    @tags = Tag.all
    render 'api/v1/tags/index'
  end

  # POST /tag
  # POST /tag.json
  # def create
  #  @feature = Feature.new(feature_params)
  #
  #  respond_to do |format|
  #    if @feature.save
  #      format.json { render 'api/v1/tag/show', status: :created}
  #    else
  #      raise ArgumentError.new(@feature.errors.to_json)
  #    end
  #  end
  # end

  # PATCH/PUT /tag/1
  # PATCH/PUT /tag/1.json
  # def update
  #  respond_to do |format|
  #    if @feature.update(feature_params)
  #      format.json { render 'api/v1/tag/show', status: :ok}
  #    else
  #      raise ArgumentError.new(@feature.errors.to_json)
  #    end
  #  end
  # end

  # private
  # Use callbacks to share common setup or constraints between actions.
  # def set_feature
  #  @feature = Feature.find(params[:id])
  # end

  # Never trust parameters from the scary internet, only allow the white list through.
  # def feature_params
  #  params.require(:feature).permit(:name)
  # end

end
