# frozen_string_literal: true

# Handle InvoiceRejection objects for Swoop
class API::V1::Root::InvoiceRejectionsController < API::APIController

  include API::V1::Shared::InvoiceRejections

  before_action :api_authorized_root

  private

  # Required by Api::V1::Shared::InvoiceRejections
  def invoice_state_event
    :swoop_reject_partner
  end

end
