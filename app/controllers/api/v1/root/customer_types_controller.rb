# frozen_string_literal: true

class API::V1::Root::CustomerTypesController < API::V1::CustomerTypesController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

  def index
    @customer_types = CustomerType.all

    render json: @customer_types, each_serializer: CustomerTypeSerializer
  end

  def create
    service = CustomerTypeServices::Create.new(
      customer_type_name: params[:customer_type][:name]
    ).call

    if service.empty_name?
      return render json: {
        name: ["required"],
      }, status: :unprocessable_entity
    elsif service.duplicated_customer_type?
      return render json: {
        name: [params[:customer_type][:name] + " already exists"],
      }, status: :unprocessable_entity
    else
      render json: service.customer_type
    end
  end

end
