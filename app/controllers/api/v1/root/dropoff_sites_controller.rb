# frozen_string_literal: true

module API
  module V1
    module Root
      class DropoffSitesController < API::V1::DropoffSitesController

        before_action :api_authorized_root_or_swoop_dispatcher # Authorization

      end
    end
  end
end
