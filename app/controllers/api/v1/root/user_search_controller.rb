# frozen_string_literal: true

class API::V1::Root::UserSearchController < API::V1::UserSearchController

  before_action :api_authorized_root_or_swoop_dispatcher

  private

  def apply_filters
    params[:filters][:exists] = [:company_id, :username]
  end

end
