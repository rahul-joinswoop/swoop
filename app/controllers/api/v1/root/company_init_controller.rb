# frozen_string_literal: true

class API::V1::Root::CompanyInitController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

  def index
    @company = get_acts_as_company

    @services = @company.service_codes.order(created_at: :desc)

    @customer_types = @company.customer_types

    @service_aliases = @company.service_aliases

    if @company.has_feature(Feature::JOB_FORM_RECOMMENDED_DROPOFF)
      @sites = []
    else
      @sites = Site.where(company: @company, deleted_at: nil)
    end

    roles = [:dispatcher, :admin, :root, :answering_service, :swoop_dispatcher]
    conditions = { company_id: @company.id, deleted_at: nil, roles: { name: roles } }

    @users = User.distinct.joins(:roles).where(conditions)

    # PDW extract company info from here using the fleet_company_id parameter
    #    render template: 'api/v1/users/me', locals:{user:@user}

    @settings = Setting.where({ company: @company })

    @symptoms = @company.symptoms

    # TODO: only need departments if departments feature is turned on for the company
    @departments = @company.departments

    render template: 'api/v1/companies/init'
  end

  def get_acts_as_company
    acts_as_company_id = remove_param("acts_as_company_id", params)

    if acts_as_company_id.blank?
      raise ArgumentError, 'acts_as_company_id is invalid'
    end

    FleetCompany.includes(:vehicle_categories).find(acts_as_company_id)
  end

end
