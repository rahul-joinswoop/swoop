# frozen_string_literal: true

class API::V1::Root::JobsChoosePartnerController < API::APIController

  before_action :api_authorized_root_or_swoop_dispatcher # Authorization

  def update
    @job = API::Jobs::ChoosePartner.new(
      job: Job.find_by_id!(params[:id]),
      api_user: api_user,
      choose_params: choose_params
    ).call.job

    render 'api/v1/jobs/show'
  end

  private

  def choose_params
    params.require(:job).permit(
      :rescue_provider_id,
      :dispatch_email,
      :billing_type,
      :vcc_amount,
      :goa_amount,
      toa: [:latest],
    )
  end

end
