# frozen_string_literal: true

class AutoassignmentRankersProvidedForNonManagedFleetCompanyError < StandardError; end

class API::V1::Root::CompaniesController < API::V1::CompaniesController

  REST_CLASS = Company

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root, except: [:batch, :create] # Authorization
  before_action :api_authorized_root_or_swoop_dispatcher, only: [:batch]
  before_action :api_authorized_set_localization, only: [:create]

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_company, only: [:show]

  def create
    api_authorized_root_or_swoop_dispatcher
    raise SecurityError if (company_params['type'] != 'RescueCompany') && api_swoop_dispatcher?

    return if duplicated_rescue_company_number?

    new_company_attributes = company_params
    new_company_attributes.delete(:fleet_in_house_client_ids)
    new_company_attributes.delete(:fleet_managed_client_ids)

    feature_ids = remove_param(:features, new_company_attributes)
    remove_param(:customer_type_ids, new_company_attributes) # TODO is it needed?
    location_attributes = remove_param(:location_attributes, new_company_attributes)

    ApplicationRecord.transaction do
      @company = CompanyService::Create.new(
        company_attributes: new_company_attributes,
        location_attributes: location_attributes,
        feature_ids: feature_ids,
        api_company: api_company,
        api_user: api_user
      ).call.company

      if settings_attributes
        CompanyService::CreateOrUpdateSettings.new(
          settings_attributes: settings_attributes,
          company: @company
        ).call
      end
    end

    if @company.valid?
      render 'api/v1/companies/show', locals: { accounts: true }, status: :created
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  def index
    render template: "api/v1/companies/index", locals: { accounts: true, hide_waivers: true }
  end

  def show
    logger.debug "Rendering #{@company}"
    render template: "api/v1/companies/show", locals: { accounts: true }
  end

  def update_swoop
    @company = Company.swoop
    params[:id] = @company.id
    # rubocop:disable Rails/SaveBang
    update
    # rubocop:enable Rails/SaveBang
  end

  def update
    return if duplicated_rescue_company_number?

    updatable_company_params = company_params
    feature_ids = remove_param(:features, updatable_company_params)
    customer_type_ids = customer_type_ids_param
    remove_param(:customer_type_ids, updatable_company_params)
    fleet_in_house_client_ids = updatable_company_params.delete(:fleet_in_house_client_ids)
    fleet_managed_client_ids = updatable_company_params.delete(:fleet_managed_client_ids)

    @company = CompanyService::Update.new(
      company_id: params[:id],
      company_attributes: updatable_company_params,
      feature_ids: feature_ids,
      customer_type_ids: customer_type_ids,
      settings_attributes: settings_attributes,
      fleet_in_house_client_ids_to_add: fleet_in_house_client_ids,
      fleet_managed_client_ids_to_add: fleet_managed_client_ids,
      invoice_charge_fee_attributes: invoice_charge_fee_params.to_h,
      autoassignment_ranker: autoassignment_ranking_params.to_h,
    ).call_with_transaction.company

    if @company.valid?
      render "api/v1/companies/show", status: :ok, locals: { accounts: true }
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  private

  NOT_AUTHORIZED_MSG = 'Not authorized to set %s'

  def api_authorized_set_localization
    return if api_root?

    if company_params.key?(:currency) && (company_params[:currency] != Money.default_currency.iso_code)
      raise SecurityError, NOT_AUTHORIZED_MSG % :currency
    end

    if company_params.key?(:distance_unit) && company_params[:distance_unit] != Company.default_distance_unit
      raise SecurityError, NOT_AUTHORIZED_MSG % :distance_unit
    end
  end

  # Checks if company_params is of RescueCompany type
  # and if we have a rescue_company with the same phone
  # (and with a different id, if it's for the update action)
  #
  # If so, we log an error and respond HTTP 409 (:conflict)
  def duplicated_rescue_company_number?
    # if the number is the same previsosly stored, it's not considered as duplicated
    type = company_params[:type]
    if @company
      type = @company.type
      return if @company.phone == company_params[:phone]
    end
    if type == 'RescueCompany' && company_params[:phone].present?
      if RescueCompany.where(phone: company_params[:phone]).where.not(id: params[:id]).any?
        message = I18n.t(
          'company.error.duplicated_phone_error',
          phone: company_params[:phone]
        )

        logger.warn message

        render(json: { phone: [message] }, status: :conflict)
      end
    end
  end

  def set_company
    @company = Company.find_by!({ id: params[:id] })
  end

  def query_includes
    [
      :location,
      :service_codes,
      :accounts,
      :features,
      :vehicle_categories,
      :symptoms,
      :company_service_questions,
      :departments,
      live_sites: [:location],
      companies_customer_types: [:customer_type],
    ]
  end

  def render_batch
    api_authorized_root_or_swoop_dispatcher
    render template: "api/v1/companies/index", locals: { accounts: true, hide_waivers: true }
  end

  def set_companies
    # @companies=Company.all.order(created_at: :desc).includes(:location,:service_codes,:accounts,:features,:vehicle_categories,live_sites:[:location])
    page = params[:page] || 1
    per_page = params[:per_page] || 1000
    @companies = Company.where({ parent_company_id: nil })
      .where({ deleted_at: nil })
      .order(created_at: :desc)
      .includes(*query_includes)
      .paginate(page: page, per_page: per_page)
  end

  ##
  # We're using companies_controller for managing some settings from the company_form.coffee.
  # And treat them inside CompanyService::CreateOrUpdateSettings.
  #
  # If more settings are needed, add the attribute as a constant in Setting.
  #
  # Ex. auction_max_bidders param would get evaluated to Setting::AUCTION_MAX_BIDDERS
  #     auction_max_eta param would get evalued to Setting::AUCTION_MAX_ETA
  #     ...and so on.
  #
  # The value of the param will be the value of the setting.
  #
  # Ex. with value: {auction_max_bidders: 5} -> Setting(key: Setting::AUCTION_MAX_BIDDERS, value: 5)
  def settings_attributes
    params.require(:company)
      .permit(*Setting::SETTINGS_MAP.keys)
      .to_unsafe_hash
      .map { |k, v| { key: Setting::SETTINGS_MAP[k.to_sym], value: v } }
  end

  def customer_type_ids_param
    return nil if !params.require(:company).key?(:customer_type_ids)

    customer_type_ids_param = params.require(:company).permit(customer_type_ids: [])

    customer_type_ids_param[:customer_type_ids] || []
  end

  def invoice_charge_fee_params
    return nil if params[:company][:invoice_charge_fee].blank?

    params.require(:company).require(:invoice_charge_fee)
      .permit(:percentage, :fixed_value, :payout_interval)
  end

  def autoassignment_ranking_params
    requested_company = Company.find_by!({ id: params[:id] })
    is_managed_fleet = requested_company.instance_of?(FleetCompany) && requested_company.fleet_managed?
    if !is_managed_fleet && !params.require(:company).permit(autoassignment_ranker: [:weights, :constraints]).to_h.empty?
      raise AutoassignmentRankersProvidedForNonManagedFleetCompanyError, "Autoassignment rankings cannot be provided for non-managed fleet companies"
    elsif is_managed_fleet
      params.require(:company).permit(autoassignment_ranker: [weights: [:speed, :quality, :cost], constraints: [:min_eta, :max_eta, :min_cost, :max_cost, :min_rating, :max_rating]])
    else
      {}
    end
  end

  def company_params
    company_args = params.require(:company)
    args = company_args.permit(:name, :phone, :accounting_email, :type,
                               :dispatch_email, :live, # partner specific
                               :in_house, :support_email, # Fleet specific
                               :deleted_at, # safe delete
                               :currency,
                               :distance_unit,
                               :language,
                               :pickup_waiver,
                               :dropoff_waiver,
                               :subscription_status_id,
                               :network_manager_id,
                               :invoice_waiver,
                               :dispatch_to_all_partners,
                               location: [:address, :lat, :lng, :place_id, :exact],
                               sites: [:id, :type, :open_time, :close_time, :open_time_sat, :close_time_sat, :open_time_sun, :close_time_sun, :name, :deleted_at, :tz, :location_id, location: [:id, :lat, :lng, :address, :place_id, :exact]],
                               features: [],
                               customer_type_ids: [],
                               fleet_in_house_client_ids: [],
                               fleet_managed_client_ids: [])

    attributize(args, [:location])

    sites = args.delete :sites
    if sites
      args[:sites_attributes] = sites
      args[:sites_attributes].each do |site|
        loc = site.delete :location
        if loc
          site[:location_attributes] = loc
        end
      end
    end

    if company_args.key?(:fleet_in_house_client_ids) && company_args[:fleet_in_house_client_ids].nil?
      args[:fleet_in_house_client_ids] = []
    end

    if company_args.key?(:fleet_managed_client_ids) && company_args[:fleet_managed_client_ids].nil?
      args[:fleet_managed_client_ids] = []
    end

    args
  end

end
