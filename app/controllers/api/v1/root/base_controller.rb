# frozen_string_literal: true

class API::V1::Root::BaseController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root # Authorization

end
