# frozen_string_literal: true

require 'csv'

class API::V1::Root::DataclipResultsController < API::APIController

  # Authentication is handled via secret token
  def show
    @result = ReportResult.find_by_token! params[:token]

    # Stream CSV to client to work-around response timeout.
    headers["Content-Type"] = "text/csv"
    headers["Content-disposition"] = "attachment; filename=\"#{params[:filename]}.csv\""
    headers['X-Accel-Buffering'] = 'no' # Stop NGINX from buffering
    headers["Cache-Control"] = "no-cache" # Stop downstream caching
    headers.delete("Content-Length") # See one line above

    self.response_body = Enumerator.new do |socket|
      socket << @result.fields.to_csv
      @result.values.each do |row|
        socket << row.to_csv
      end
    end
  end

end
