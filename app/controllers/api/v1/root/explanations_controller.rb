# frozen_string_literal: true

class API::V1::Root::ExplanationsController < API::V1::ExplanationsController

  before_action :api_authorized_root_or_swoop_dispatcher

  private

  def fetch_job_for_api_company
    @job = Job.find_by!(id: params[:job_id])
  end

end
