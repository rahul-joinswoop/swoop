# frozen_string_literal: true

class API::V1::Root::VersionsController < API::V1::VersionsController

  REST_CLASS = Version

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_root # Authorization

  before_action :set_version, only: [:update]

  def create
    @version = Version.new(version_params)
    respond_to do |format|
      if @version.save
        format.json { render 'api/v1/versions/show', status: :created }
      else
        format.json { render json: @version.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @version.update(version_params)
        format.json { render 'api/v1/versions/show', status: :ok }
      else
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  def set_version
    # @version=Version.find_by(name:params[:name])
    @version = Version.find(params[:id])
  end

  def version_params
    params.require(:version).permit(:name, :version, :description, :forced)
  end

end
