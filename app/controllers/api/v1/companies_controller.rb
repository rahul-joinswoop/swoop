# frozen_string_literal: true

class API::V1::CompaniesController < API::APIController

  include API::V1::Shared::Storage

  REST_CLASS = Company

  before_action :doorkeeper_authorize! # Authentication

  def batch
    if params[:company_ids]
      company_ids = params[:company_ids].split(',')

      @companies = CompanyService::Batch.new(
        api_company: @api_company,
        company_ids: company_ids
      ).call
    else
      @companies = []
    end

    render_batch # implemented on children
  end

  def create_logo
    CompanyService::CreateLogo.new(
      attached_document_attributes: attached_document_attributes,
      api_user: api_user,
      api_company: Company.find(api_company.id),
    ).call

    render json: api_company, serializer: CompanySerializer
  end

  def delete_logo
    CompanyService::DeleteLogo.new(
      api_company: Company.find(api_company.id)
    ).call

    render json: api_company, serializer: CompanySerializer
  end

  private

  def attached_document_attributes
    params.require(:attached_document).permit(:document)
  end

end
