# frozen_string_literal: true

class API::V1::UsersController < API::APIController

  REST_CLASS = User

  protect_from_forgery with: :null_session
  before_action :doorkeeper_authorize!, except: [:password_request, :password_request_status, :change_password] # Authentication
  before_action :user_api_authorized_admin, except: [:me, :index, :dispatchers, :drivers, :batch, :password_request, :password_request_status, :change_password] # Authorization

  before_action :set_user, only: [:show, :update]
  before_action :set_users, only: [:index]
  before_action :set_batch_users, only: [:batch]

  # action that starts the Forgot Password flow
  # @param params[:user_input]
  #
  # @return status: 200 when the user can't be found by the given user_input
  def password_request
    service_context = Authentication::ResetForgottenPassword.call params
    password_reset_request = service_context.result

    template = {
      locals: { password_request: password_reset_request },
    }

    if service_context.success?
      render template.merge(status: 201)
    else
      render template.merge(status: 200)
    end
  end

  def password_request_status
    password_request_uuid = params[:uuid]

    if PasswordRequest.valid_request_for?(password_request_uuid)
      password_request = PasswordRequest.find_by_uuid(password_request_uuid)

      render template: 'api/v1/users/password_request_status',
             locals: { password_request: password_request }, status: 200
    else
      render json: { password_request: { valid_request: false } }, status: :not_found
    end
  end

  # Changes the password if params[:password_request][:uuid] is valid.
  # @param params[:password_request][:uuid] - password_request.uuid
  # @param params[:user][:password] - new password
  #
  # @return :ok if password is successfully changed
  # @return :not_found if password_request is not valid for the given uuid
  # @return :unprocessable_entity if the new password is not valid
  def change_password
    password_request_uuid = params[:password_request][:uuid]
    new_password = params[:user][:password]

    if !PasswordRequest.valid_request_for?(password_request_uuid)
      return render json: { password_request: { valid_request: false } }, status: :not_found
    end

    password_request = PasswordRequest.find_by_uuid(password_request_uuid)
    user = password_request.user

    if user.update_attributes password: new_password
      password_request.invalidate!
    else
      return render json: { errors: user.errors }, status: :unprocessable_entity
    end

    render json: {}, status: :ok
  end

  def me
    api_authorized_fleet_or_rescue
    @user = api_user

    # partner users have notification_settings, so we load them
    # in case the logged in user is partner and has dispatcher or admin role,
    # so they can manage their own notification_settings in
    # settings > configuration > My Notifications > Notification Settings
    if @user.has_role?(:rescue) && (@user.has_role?(:dispatcher) || @user.has_role?(:admin))
      @include_notification_settings = true
    end

    render template: 'api/v1/users/me', locals: {
      user: @user,
      include_notification_settings: @include_notification_settings,
    }
  end

  def index
    api_authorized_fleet_or_rescue
    render template: "api/v1/users/index"
  end

  def show
    render template: "api/v1/users/show"
  end

  def add_fleet_or_rescue
    if @user.company.fleet?
      @user.add_role :fleet
    elsif @user.company.rescue?
      @user.add_role :rescue
    end
  end

  def create
    user_attributes = user_params
    user_attributes[:company_id] = api_company.id unless user_attributes[:company_id]

    new_roles = remove_param('roles', user_attributes)
    notification_settings = remove_param('notification_settings', user_attributes)

    @user = Users::BuildCompanyUser.new(
      user_attributes: user_attributes,
      new_roles: new_roles,
      notification_settings: notification_settings,
      api_user: api_user
    ).call.user

    respond_to do |format|
      if @user.save
        @include_notification_settings = true

        format.json { render 'api/v1/users/show', status: :created }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def batch
    api_authorized_fleet_or_rescue
    render json: @users, each_serializer: MinimalUserSerializer
  end

  def dispatchers
    roles = [:dispatcher, :admin, :root, :answering_service, :swoop_dispatcher]
    conditions = { company_id: api_company.id, deleted_at: nil, roles: { name: roles } }

    @users = User
      .distinct
      .joins(:roles)
      .where(conditions)

    render json: @users, each_serializer: MinimalUserSerializer
  end

  def drivers
    roles = [:driver]
    conditions = { company_id: api_company.id, deleted_at: nil, roles: { name: roles } }

    @users = User
      .distinct
      .joins(:roles)
      .where(conditions)

    render json: @users, each_serializer: MinimalUserSerializer
  end

  def search
    search_params
    user = nil
    if search_params.size > 0
      user = User.find_by(search_params)
    end
    respond_to do |format|
      if user
        ret = { found: true }
        if user.company == api_company
          ret[:user_id] = user.id
        end
        format.json { render json: ret }
      else
        format.json { render json: { found: false } }
      end
    end
  end

  def update
    user_attributes = user_params
    roles = remove_param('roles', user_attributes)
    notification_settings = remove_param('notification_settings', user_attributes)
    system_email_filtered_company_ids = remove_param('system_email_filtered_companies', user_attributes)

    if user_attributes.key?("site_ids") && user_attributes["site_ids"].nil?
      user_attributes["site_ids"] = []
    end

    @user = Users::UpdateCompanyUser.new(
      user: @user,
      user_attributes: user_attributes,
      roles: roles,
      notification_settings: notification_settings,
      system_email_filtered_company_ids: system_email_filtered_company_ids,
      api_user: api_user,
    ).call.user

    respond_to do |format|
      if @user.valid?
        @include_notification_settings = true
        format.json { render "api/v1/users/show", status: :ok }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_user
    if api_root?
      @user = User.find_by!({ id: params[:id] })
    else
      @user = User.find_by!({ id: params[:id], company: api_company })
    end
  end

  def set_batch_users
    conditions = { company_id: api_company.id }

    # {{host}}/api/v1/root/jobs/?job_ids=1076
    if params[:user_ids]
      conditions[:id] = params[:user_ids].split(',')
    end

    incls = [
      :job_status_email_preferences,
      :job_status_email_statuses,
      :sites,
      :company,
      :roles,
    ]
    @users = User.where(conditions).includes(*incls)
  end

  def set_users
    args = {}
    if !params[:include_deleted]
      args = { deleted_at: nil }
    end

    incls = [
      :job_status_email_preferences,
      :job_status_email_statuses,
      :sites,
      :company,
      :roles,
    ]

    if api_root? || api_swoop_dispatcher?
      Rails.logger.debug "UserController::set_users Doing root or swoop_dispatcher"
      page = params[:page] || 1
      per_page = params[:per_page] || 500
      pagination_opts = { page: page, per_page: per_page }
      @users = User
        .includes(*incls)
        .where.not({ company_id: nil, username: nil })
        .where(args)
        .order(created_at: :desc)
        .paginate(pagination_opts)
    else
      Rails.logger.debug "UserController::set_users  Doing other"
      @users = api_company.users
        .includes(*incls)
        .where(args)
        .order(created_at: :desc)
    end
  end

  def search_params
    params.require(:user).permit(:username, :phone, :email)
  end

  def user_params
    defaults = [
      :full_name,
      :first_name,
      :last_name,
      :username,
      :password,
      :email,
      :phone,
      :pager,
      :language,
      :deleted_at,
      :timezone,
      :on_duty,
      :job_status_email_frequency,
      { roles: [] },
      { site_ids: [] },
      { system_email_filtered_companies: [] },
      { notification_settings: [:notification_code, notification_channels: []] },
    ]

    if api_root?
      defaults += [:company_id, :receive_system_alerts, :custom_account_visible]
    end

    user_args = params.require(:user)
    args = user_args.permit(*defaults)

    if user_args.key?("job_status_email_preferences")
      prefs = user_args.delete("job_status_email_preferences") || []
      ids = prefs.map { |name| JobStatus::NAME_MAP[name.to_sym] }
      args["job_status_email_status_ids"] = ids
    end

    args.to_h
  end

end
