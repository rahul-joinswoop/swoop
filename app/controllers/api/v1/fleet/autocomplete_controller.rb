# frozen_string_literal: true

class API::V1::Fleet::AutocompleteController < API::V1::AutocompleteController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet_in_house # Authorization

end
