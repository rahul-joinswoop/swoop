# frozen_string_literal: true

class API::V1::Fleet::ReportsController < API::V1::ReportsBaseController

  before_action :api_authorized_fleet # Authorization

end
