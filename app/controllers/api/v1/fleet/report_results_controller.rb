# frozen_string_literal: true

class API::V1::Fleet::ReportResultsController < API::V1::ReportResultsBaseController

  before_action :api_authorized_fleet # Authorization

  before_action :set_report_result, only: [:show]
  before_action :api_authorized_admin, only: [:show]

  def set_report_result
    @report_result = ReportResult.find_by!({ id: params[:id], company: api_company })
  end

end
