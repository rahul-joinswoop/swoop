# frozen_string_literal: true

class API::V1::Fleet::IssuesController < API::V1::IssuesController

  before_action :api_authorized_fleet_in_house

end
