# frozen_string_literal: true

class API::V1::Fleet::UserSearchController < API::V1::UserSearchController

  before_action :api_authorized_fleet

end
