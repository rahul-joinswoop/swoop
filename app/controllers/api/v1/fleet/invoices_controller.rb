# frozen_string_literal: true

class API::V1::Fleet::InvoicesController < API::V1::InvoicesController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet # Authorization
  before_action :set_invoice, only: [:show, :update]
  before_action :set_invoices, only: [:index]
  before_action :set_download_invoices, only: [:download]

  def controller_name
    'fleet'
  end

  def index
    @include_original_deleted_line_items = true
    super
  end

  # incoming invoices
  def set_conditional_invoices(conditions, page, per_page, order)
    @invoices = apply_scopes(api_company.incoming_invoices)
      .associated_rendered_models
      .where(conditions)
      .paginate(page: page, per_page: per_page)
      .order(order)

    @invoices_total = apply_scopes(api_company.incoming_invoices).where(conditions).count
  end

  def set_download_invoices()
    conditions, _page, _per_page, _order = parse_query
    @invoices = apply_scopes(api_company.incoming_invoices).associated_rendered_models.where(conditions)
  end

  def download
    @report = nil

    if api_company.is_tesla?
      @report =  Report.tesla_invoice
    elsif api_company.is_enterprise?
      @report =  Report.enterprise_invoice
    elsif api_company.is_mts?
      @report =  Report.mts_invoice
    end

    report_params = params[:report] || {}
    @report_result = company_report.run(api_user, report_params,
                                        :process_invoice_state_to_downloaded)

    render 'api/v1/report_results/show'
  end

  private

  def company_report
    @company_report ||= CompanyReport.new(company: api_company, report: @report)
  end

  def set_invoice
    @invoice = Invoice.includes([:line_items, :payments_or_credits])
      .find_by!(id: params[:id], recipient_company_id: api_company.id)
  end

end
