# frozen_string_literal: true

class API::V1::Fleet::AttachedDocumentsController < API::V1::AttachedDocumentsController

  before_action :api_authorized_fleet

  private

  def set_job
    @job = Job.find_by!(id: params[:job_id], fleet_company: api_company)
  end

end
