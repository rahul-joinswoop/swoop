# frozen_string_literal: true

class API::V1::Fleet::InventoryItemsController < API::V1::InventoryItemsController

  before_action :api_authorized_fleet # Authorization

  before_action :set_inventory_item, only: [:show, :update, :destroy, :api_authorized_tesla]
  before_action :api_authorized_admin, only: [:create, :update, :destroy, :api_authorized_tesla]
  def create
    args = map_params

    @inventory_item = InventoryItem.new(args)
    @inventory_item.user = api_user

    respond_to do |format|
      logger.debug "SM: Calling inventory_item.save #{@inventory_item.inspect}"
      if @inventory_item.save
        logger.debug "SM: Calling inventory_item.save returned"

        @event.target = @inventory_item
        @event.save!

        format.json { render "api/v1/inventory_items/show" }
      else
        raise ArgumentError, @inventory_item.errors.to_json
      end
    end
  end

  def index
    # PDW: This is dangerous, but at the moment we only have one fleet company using tire program
    if api_company.is_tesla?
      @inventory_items = InventoryItem.where(deleted_at: nil, item_type: "Tire")
    else
      @inventory_items = []
    end

    render "api/v1/inventory_items/index"
  end

  def tires
    if api_company.is_tesla?
      @inventory_items =
        InventoryItem.where(item_type: 'Tire', deleted_at: nil).includes(:user)
    else
      @inventory_items = []
    end

    render "api/v1/inventory_items/index"
  end

  def inventory_item_params
    args = params.require(:inventory_item).permit(:site_id,
                                                  :released_at,
                                                  :deleted_at,
                                                  :company_id,
                                                  item: [
                                                    :type,
                                                    :tire_type_id,
                                                  ])
    attributize(args, [:item])
    args
  end

  def set_inventory_item
    @inventory_item = InventoryItem.find_by!({ id: params[:id], item_type: "Tire" })
  end

end
