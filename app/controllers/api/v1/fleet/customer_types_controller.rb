# frozen_string_literal: true

class API::V1::Fleet::CustomerTypesController < API::V1::CustomerTypesController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_customer_types, only: [:index]

  def index
    render template: "api/v1/customer_types/index"
  end

  private

  def set_customer_types
    @customer_types = api_company.customer_types
  end

end
