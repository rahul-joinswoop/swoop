# frozen_string_literal: true

class API::V1::Fleet::ServicesController < API::V1::ServicesController

  REST_CLASS = ServiceCode

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet # Authorization

  # N.B. Don't move these up to base as they will get called before the
  # Authorization above
  before_action :set_services_with_rates, only: [:with_rates_by_provider]

  def index_by_rates_filter
    set_services_with_rates

    render template: "api/v1/services/index"
  end

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use services/index_by_rates_filter route instead
  def with_rates_by_provider
    render template: "api/v1/services/index"
  end

  def partner
    @services = rescue_company.service_codes.order(created_at: :desc)

    render template: "api/v1/services/index"
  end

  private

  def set_services_with_rates
    @services =
      rescue_company
        .rates
        .select(:service_code_id)
        .where(account: account)
        .group(:service_code_id)
        .map(&:service_code)
  end

  def account
    @account ||= Account.find_by!(company: rescue_company, client_company: api_company)
  end

  def rescue_company
    @rescue_company ||= RescueProvider.find(params[:rescue_provider_id]).provider
  end

end
