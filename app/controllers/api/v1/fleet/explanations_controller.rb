# frozen_string_literal: true

class API::V1::Fleet::ExplanationsController < API::V1::ExplanationsController

  before_action :api_authorized_fleet_in_house

  private

  def fetch_job_for_api_company
    @job = Job.find_by!(id: params[:job_id], fleet_company_id: api_company.id)
  end

end
