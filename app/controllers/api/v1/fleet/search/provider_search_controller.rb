# frozen_string_literal: true

# GET {{host}}/api/v1/fleet/providers/search?page=1&per_page=5&search_terms%5Bterm%5D=finish%20line

class API::V1::Fleet::Search::ProviderSearchController < API::V1::Search::ProviderSearchController

  before_action :api_authorized_fleet

end
