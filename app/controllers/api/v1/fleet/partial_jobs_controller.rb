# frozen_string_literal: true

class API::V1::Fleet::PartialJobsController < API::V1::PartialJobsController

  before_action :api_authorized_fleet

  FLEET_FIELDS = [
    :customer_type_id,
    :customized_rescue_company_name,
    :department_id,
    :fleet_site_id,
  ].freeze

  protected

  def search_filter
    filter = {}
    if api_company.fleet_managed?
      filter[:must_not] = { status: Job::STATUS_REASSIGNED }
    end
    site_ids = api_user_site_ids
    if site_ids.present?
      filter[:filters] = { fleet_site_id: site_ids }
    end
    filter
  end

  def get_jobs(conditions, page, per_page, order, includes)
    Rails.logger.debug "Fleet::PartialJobsController::set_jobs - #{conditions},#{page},#{per_page},#{order}"

    finder = apply_scopes(api_company.fleet_jobs).where(conditions)
    if api_company.fleet_managed?
      finder = finder.where.not(status: Job::STATUS_REASSIGNED)
    end
    site_ids = api_user_site_ids
    if site_ids.present?
      finder = finder.where(fleet_site_id: site_ids)
    end

    jobs = finder.paginate(page: page, per_page: per_page).order(order).includes(includes)
    jobs_total = finder.count
    [jobs, jobs_total]
  end

  def get_all_fields
    (COMMON_FIELDS + FLEET_FIELDS).uniq
  end

  private

  def api_user_site_ids
    api_user.user_sites.map(&:site_id)
  end

end
