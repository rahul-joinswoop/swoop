# frozen_string_literal: true

#
# POST /api/v1/fleet/policy {
# "number":"1234567890"
# }
#

class API::V1::Fleet::PCC::PolicyController < API::V1::PCC::PolicyController

  before_action :api_authorized_fleet # Authorization

end
