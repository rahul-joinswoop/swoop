# frozen_string_literal: true

# Handle InvoiceRejection objects for Swoop
class API::V1::Fleet::InvoiceRejectionsController < API::APIController

  include API::V1::Shared::InvoiceRejections

  before_action :api_authorized_fleet_in_house

  private

  # Required by Api::V1::Shared::InvoiceRejections
  def invoice_state_event
    :fleet_reject
  end

end
