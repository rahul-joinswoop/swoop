# frozen_string_literal: true

module API
  module V1
    module Fleet
      class DropoffSitesController < API::V1::DropoffSitesController

        before_action :api_authorized_fleet # Authorization

      end
    end
  end
end
