# frozen_string_literal: true

class API::V1::Fleet::SymptomsController < API::V1::SymptomsController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet # Authorization

  def index
    @symptoms = api_company.symptoms

    render json: @symptoms, each_serializer: SymptomSerializer
  end

end
