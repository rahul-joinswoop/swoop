# frozen_string_literal: true

class API::V1::Fleet::JobSearchController < API::V1::JobSearchController

  before_action :api_authorized_fleet # Authorization

end
