# frozen_string_literal: true

class API::V1::Fleet::LookerController < API::V1::LookerBaseController

  before_action :api_authorized_fleet

end
