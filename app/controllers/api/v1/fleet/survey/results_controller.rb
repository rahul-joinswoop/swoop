# frozen_string_literal: true

class API::V1::Fleet::Survey::ResultsController < API::APIController

  def create
    review = get_review
    raise ActiveRecord::RecordNotFound unless review
    Rails.logger.debug "Found Review: #{review.id} #{review_params}"

    SurveyResponse.new(review, review_params).call.save_mutated_objects_throw_errors
    render json: { "status": "200" }
  end

  def get_review
    token = bearer_token
    return if !token
    review = Review.find_by(uuid: token)
    return if !review
    review
  end

  def bearer_token
    pattern = /^Bearer /
    header  = request.headers['Authorization']
    header.gsub(pattern, '') if header && header.match(pattern)
  end

  def review_params
    params.require('survey').permit(responses: [:survey_question_id, :value])
  end

end
