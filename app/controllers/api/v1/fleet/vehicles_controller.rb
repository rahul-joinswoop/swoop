# frozen_string_literal: true

class API::V1::Fleet::VehiclesController < API::APIController

  REST_CLASS = Vehicle

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet # Authorization

  before_action :set_vehicles, only: [:index]

  def index
    logger.debug "rendering vehicles: #{@vehicles}"
    render template: "api/v1/vehicles/index"
  end

  def set_vehicles
    # TODO - #set_vehicles used to filter on jobs with status NOT IN Job.fleet_no_publish_states,
    # but #batch (and other places in the code base) instead filter on jobs
    # with status IN Job.fleet_show_rescue_vehicle_states and right now this is
    # what we are doing here. is this right?
    @vehicles = api_company&.fleet_rescue_vehicles || RescueVehicle.none
  end

  # for fleet managed users, we make sure the requested vehicle
  # is associated with one of their jobs with the allowed status
  def batch
    if params[:vehicle_ids].blank?
      @vehicles = []
    else
      @vehicles = (api_company&.fleet_rescue_vehicles || RescueVehicle.none)
        .where(id: params[:vehicle_ids])
    end

    render template: "api/v1/vehicles/index"
  end

end
