# frozen_string_literal: true

class API::V1::Fleet::JobsChoosePartnerController < API::APIController

  before_action :api_authorized_fleet_in_house # Authorization

  def update
    @job = API::Jobs::ChoosePartner.new(
      job: Job.where(fleet_company: api_company).find_by_id!(params[:id]),
      api_user: api_user,
      choose_params: choose_params
    ).call.job

    render 'api/v1/jobs/show'
  end

  private

  def choose_params
    params.require(:job).permit(
      :rescue_provider_id,
      :dispatch_email,
      toa: [:latest]
    )
  end

end
