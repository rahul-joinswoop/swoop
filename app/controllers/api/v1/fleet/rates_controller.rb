# frozen_string_literal: true

class API::V1::Fleet::RatesController < API::V1::RatesController

  include API::V1::Shared::AddAndEditRates

  before_action :api_authorized_fleet # Authorization Also allows root
  before_action :api_authorized_fleet_in_house,
                only: [:live, :create, :update_multi, :delete]

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_provider_rates, only: [:provider]

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use fetch_by_filters route instead
  def provider
    logger.debug "rendering #{@rates}"
    render template: "api/v1/rates/index"
  end

  # This route fetches rates give the filters passed on params
  #
  # GET rates/fetch_by_filters
  def fetch_by_filters
    set_provider_rates

    logger.debug "Fleet::RatesController#fetch_by_filters - total rates found: #{@rates.size}"

    render template: "api/v1/rates/index"
  end

  def addons
    logger.debug "rendering #{@rates}"
    render template: "api/v1/rates/index"
  end

  def live
    args = params.require(:rate).permit(:type, :provider_id, :service_code_id)
    Rails.logger.debug "fleet /rates/live called with #{args}"

    rates = rates_provider.rates_by_account(
      account_id: rates_account.id,
      service_code_id: args[:service_code_id]
    )

    ServiceRunner.new.tap do |runner|
      runner << RateService::UpdateLive.new(rates: rates, type: args[:type])
      runner << mail_rate_change_service
    end.call_all

    render template: "api/v1/rates/index"
  end

  def delete
    args = params.require(:rates).permit(
      :account_id, :service_code_id, :type, :vehicle_category_id
    )
    args[:account_id] = rates_account.id

    rates = rates_provider.rates_by_account(args)

    ServiceRunner.new.tap do |runner|
      runner << RateService::DeleteMultiple.new(
        rates: rates, account: rates_account
      )
      runner << mail_rate_change_service
    end.call_all

    head :no_content
  end

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use create_based_on_defaults route instead
  def create_service
    account_id = rates_account.id

    service_code_id = params[:service_code_id]
    service_code_id = nil if service_code_id == "null"

    rates = rates_provider.rates_by_account(
      account_id: account_id,
      service_code_id: nil
    )

    copy_rates_service = RateService::CopyRates.new(
      rates: rates,
      company: api_company,
      new_attributes: { account_id: account_id, service_code_id: service_code_id }
    )

    @rates = copy_rates_service.call_with_transaction.new_rates

    render template: "api/v1/rates/index"
  end

  # This route receives specific filter params from UI (Rates table)
  # to create new default rates for the current api_company
  # based on the rescue_provider passed.
  #
  # POST rates/defaults
  def create_based_on_defaults
    account_id = rates_account.id
    service_code_id = params[:rate][:service_code_id]

    Rails.logger.debug(
      "Fleet::RatesController#create_based_on_defaults - about to create rate defaults " \
      "based on company_id(#{api_company.id}), account_id(#{account_id}), service_code_id(#{service_code_id})"
    )

    rates = rates_provider.rates_by_account(
      account_id: account_id,
      service_code_id: nil
    )

    copy_rates_service = RateService::CopyRates.new(
      rates: rates,
      company: api_company,
      new_attributes: { account_id: account_id, service_code_id: service_code_id }
    )

    @rates = copy_rates_service.call_with_transaction.new_rates

    Rails.logger.debug("Fleet::RatesController#create_based_on_defaults - total rates created: #{@rates.size}")

    render template: "api/v1/rates/index"
  end

  private

  def _get_rp_and_account
    rp = RescueProvider.find_by!(company: api_company, id: params[:rescue_provider_id])
    logger.debug "rp: #{rp.inspect}"
    account = Account.find_by(company: rp.provider, client_company: api_company)
    if !account
      raise ArgumentError, "Unable  To find account for rescue company: #{rp.provider.id} and fleet company: #{api_company.id}"
    end
    logger.debug "account: #{account.inspect}"
    [rp, account]
  end

  def set_provider_rates
    rp, account = _get_rp_and_account
    service_code_id = params[:service_code_id]
    if service_code_id == 'null'
      service_code_id = nil
    end

    Rails.logger.debug(
      "Fleet::RatesController#set_provider_rates - about to fetch rates by " \
      "rescue_provider_id(#{rp.id}), account_id(#{account.id}), service_code_id(#{service_code_id})"
    )

    @rates = rp.provider.rates.where(account: account, site: [rp.site, nil], service_code: service_code_id)
  end

  def mail_rate_change_service
    RateService::MailRateChange.new(
      company: api_company,
      user: api_user,
      company_to_be_notified: rates_provider
    )
  end

  # Required by Api::V1::Shared::AddAndEditRates
  def rates_provider
    return @rates_provider if @rates_provider

    rescue_provider_id =
      params[:rescue_provider_id] || params[:rate][:provider_id] || params[:rates][:provider_id]

    rescue_provider = RescueProvider.find_by!(
      company: api_company, id: rescue_provider_id
    )
    @rates_provider = rescue_provider.provider
    @rates_provider
  end

  # Required by Api::V1::Shared::AddAndEditRates
  def rates_account
    @rates_account ||=
      Account.find_by(company: rates_provider, client_company: api_company)
  end

  def rate_args
    [
      :type,
      :service_code_id,
      :site_id,
      :vendor_id,
      :vehicle_category_id,
      :hourly,
      :flat,
      :hookup,
      :miles_towed,
      :miles_towed_free,
      :miles_enroute,
      :miles_enroute_free,
      :miles_deadhead,
      :miles_deadhead_free,
      :miles_p2p,
      :special_dolly,
      :gone,
      :storage_daily,
      :deleted_at,
      :service_group_id,
      :account_group_id,
      :second_day_charge,
      :beyond_charge,
      :seconds_free_window,
      :round,
      :nearest,
      additions: [:id, :calc_type, :amount, :name, :deleted_at],
    ]
  end

end
