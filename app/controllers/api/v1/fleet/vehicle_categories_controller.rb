# frozen_string_literal: true

class API::V1::Fleet::VehicleCategoriesController < API::APIController

  before_action :api_authorized_fleet # Authorization

  def partner
    rescue_provider = RescueProvider.find_by!(
      company: api_company,
      id: params[:rescue_provider_id]
    )

    @vehicle_categories = rescue_provider.provider.vehicle_categories

    render json: @vehicle_categories, each_serializer: VehicleCategorySerializer
  end

end
