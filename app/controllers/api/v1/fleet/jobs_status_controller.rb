# frozen_string_literal: true

class API::V1::Fleet::JobsStatusController < API::V1::JobsStatusController

  before_action :api_authorized_fleet # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_job, only: [:update]

  def update_service
    FleetUpdateJobState
  end

  private

  def set_job
    @job = Job.find_by!({ id: params[:id], fleet_company: api_company })
  end

end
