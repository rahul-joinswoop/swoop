# frozen_string_literal: true

class API::V1::Fleet::SitesController < API::V1::SitesController

  REST_CLASS = FleetSite

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet # Authorization
  before_action :api_authorized_fleet_in_house, only: :search
  before_action :lookup_site, only: [:show, :update]

  def index
    @sites = Site.where(company: api_company.id, deleted_at: nil)
    render template: "api/v1/sites/index"
  end

  def show
    render template: "api/v1/sites/show"
  end

  def update
    site_service_update = SiteService::Update.new(
      site_id: params[:id],
      site_params: site_params,
      company: api_company
    )

    ApplicationRecord.transaction do
      @site = site_service_update.call.site
    end

    if site_service_update.duplicated_site?
      site_type = site_service_update.duplicated_site.type == 'PoiSite' ? 'Place' : 'Site'

      return render json: { name: ["A #{site_type} with this name already exists"] }, status: :unprocessable_entity
    end

    respond_to do |format|
      if @site.valid?
        CheckSiteTimesWorker.perform_async

        format.json { render "api/v1/sites/show", status: :ok, site: @site }
      else
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    site_service_create = SiteService::Create.new(
      site_params: site_params,
      site_type: FleetSite,
      company: api_company
    )

    ApplicationRecord.transaction do
      @site = site_service_create.call.site
    end

    if site_service_create.duplicated_site?
      site_type = site_service_create.duplicated_site.type == 'PoiSite' ? 'Place' : 'Site'

      return render json: { name: ["A #{site_type} with this name already exists"] }, status: :unprocessable_entity
    end

    respond_to do |format|
      if @site.valid?
        @site.schedule_google_timezone

        format.json { render 'api/v1/sites/show', status: :created }
      else
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  def search
    phone = params[:phone]
    head(:bad_request) && return if phone.length != 12
    @sites = PartnerSite.where(phone: phone, deleted_at: nil)
    render(json: []) && return if @sites.empty?
    @fleet_company = api_company
    render template: "api/v1/sites/search"
  end

  private

  def lookup_site
    @site = Site.find(params[:id])
  end

  def site_params
    args = params.require(:site).permit(
      :name,
      :phone,
      :deleted_at,
      :owner_company_id,
      :site_code,
      location: [:id, :lat, :lng, :address, :place_id, :exact, :location_type_id]
    )

    if args[:location]
      args[:location_attributes] = args[:location]
      if @site && @site[:id]
        args[:location_attributes][:site_id] = @site[:id]
      end
      args.delete(:location)
    elsif params["site"].key?("location") && params["site"]["location"].nil?
      args["location"] = nil
    end

    args
  end

end
