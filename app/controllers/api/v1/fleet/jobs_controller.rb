# frozen_string_literal: true

class API::V1::Fleet::JobsController < API::V1::JobsBaseController

  include LoggableJob

  REST_CLASS = Job

  before_action :api_authorized_fleet_or_swoop_dispatcher # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_job, only: [:show, :update, :destroy, :recommended_providers, :recommended_provider, :change_payment_type_to]
  before_action :set_provider, only: [:recommended_provider]
  before_action :set_jobs, only: [:index, :partial]

  def create
    @job = API::Jobs::CreateFleet.new(
      api_company: api_company,
      api_user: api_user,
      params: job_params,
      save_as_draft: params[:save_as_draft],
      save_as_predraft: params[:save_as_predraft]
    ).call.job

    super
  end

  def update
    params_to_update = map_params
    status = remove_param(:status, params_to_update)
    rescue_provider_id = remove_param(:rescue_provider_id, params_to_update)

    if status
      log :warn, "status updates on job depricated, use /job/<id>/status API", job: params[:id]

      # TODO remove this case and status from allowed params
      return redirect_to url_for(controller: 'jobs_status', action: 'update')
    elsif rescue_provider_id
      log :warn, "provider updates on job depricated, use /job/<id>/choose_partner API", job: params[:id]

      return redirect_to url_for(controller: :jobs_choose_partner, action: :update)
    end

    @job = API::Jobs::UpdateFleet.new(
      company: api_company,
      api_user: api_user,
      job_id: params[:id],
      params: params_to_update,
      job_history: params.dig(:job, :history),
      job_storage: params.dig(:job, :storage),
      browser_platform: browser.platform,
      request_user_agent: request.user_agent,
      save_as_draft: params[:save_as_draft]
    ).call.job

    if @job.valid?
      render template: 'api/v1/jobs/show', status: :ok
    else
      raise ArgumentError, @job.errors.to_json
    end
  end

  def getCompanyType
    "FleetCompany"
  end

  def getJoin
    :fleet_company
  end

  def recommended_providers
    @providers = JobGetClosestProvidersService.new(@job, api_company).call # [0..10]
    render json: @providers, each_serializer: RecommendedProviderSerializer
  end

  def recommended_provider
    @provider = JobGetProviderDistanceService.new(@job, @provider).call # [0..10]
    render json: @provider, serializer: RecommendedProviderSerializer
  end

  def change_provider_to
    @job = Job.find_by(id: params[:job_id], fleet_company: api_company) # PDW this is insecure as it allows fleets to change any job
    super
  end

  def change_payment_type_to
    @job.update!(customer_type: CustomerType.find(params[:new_payment_type_id]))
    render 'api/v1/jobs/show'
  end

  protected

  def job_filter(condition)
    apply_scopes(Job.all).where(fleet_company: api_company).where(condition).distinct(:id)
  end

  def extra_job_includes
    [
      :pcc_claim,
      :pcc_coverage,
    ]
  end

  private

  def set_job
    arel = Job.order_id_desc.by_fleet_company(api_company)
    @job = if params[:vin]
             arel.by_vin(params[:vin]).take!
           else
             arel.find(params[:id]) # PDW: TODO: this looks insecure
           end
  end

  def set_provider
    @provider = RescueProvider.find_by!({ id: params[:provider_id], company_id: api_company.id })
  end

  def job_params
    params.require(:job).permit(
      :first_name,
      :last_name,
      :phone,
      :email,
      :status,
      :service,
      :service_alias_id,
      :service_code_id,
      :serial_number,
      :vehicle_type,
      :make,
      :model,
      :year,
      :color,
      :license,
      :vin,
      :symptom_id,
      :notes,
      :scheduled_for,
      :po_number,
      :ref_number,
      :unit_number,
      :odometer,
      :vip,
      :uber,
      :alternate_transportation,
      :price, # only if not assigned to partner
      :customer_type_id,
      :no_keys,
      :keys_in_trunk,
      :four_wheel_drive,
      :blocking_traffic,
      :accident,
      :unattended,
      :reject_reason_id,
      :reject_reason_info,
      :fleet_notes,
      :driver_notes,
      :swoop_notes, # Needed for when swoop creates a job on behalf of fleet
      :rescue_provider_id,
      :tire_type_id,
      :fleet_dispatcher_id,
      :get_location_sms,
      :review_sms,
      :partner_sms_track_driver,
      :send_sms_to_pickup,
      :priority_response,
      :policy_number,
      :fleet_site_id,
      :department_id,
      :text_eta_updates,
      :fleet_manual,
      :accounting_code,
      :acts_as_company_id,
      :root_dispatcher_id,
      :invoice_vehicle_category_id,
      :pcc_coverage_id,
      :customer_lookup_type,
      pickup_contact: [:id, :full_name, :first_name, :last_name, :phone],
      dropoff_contact: [:id, :full_name, :first_name, :last_name, :phone],
      caller: [:id, :phone, :full_name],
      customer: [:id, :full_name, :phone, :member_number],
      service_location: [
        :id, :lat, :lng, :address, :state, :city, :street, :zip, :exact,
        :place_id, :location_type_id, :site_id,
      ],
      drop_location: [
        :id, :lat, :lng, :address, :state, :city, :street, :zip, :exact,
        :place_id, :location_type_id, :site_id,
      ],
      toa: [:latest],
      question_results: [:question_id, :answer_id, :answer_info],
      policy_location: [
        :id, :lat, :lng, :address, :state, :city, :street, :zip, :exact,
        :place_id,
      ]
    )
  end

end
