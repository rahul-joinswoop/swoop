# frozen_string_literal: true

class API::V1::Fleet::RescueCompaniesController < API::V1::CompaniesController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet # Authorization
  before_action :api_authorized_fleet_in_house,
                only: [:create, :add_site_to_existent_partner, :add_existing_site, :show]

  def search_rescue_companies
    @companies = RescueCompany.where("lower(name) like ?", "#{params[:term].downcase}%")
    render 'api/v1/companies/index', locals: { accounts: false }
  end

  def create
    new_company_attributes = company_attributes
    new_company_attributes[:type] = RescueCompany.to_s

    ApplicationRecord.transaction do
      new_company_service = company_service_for_new_company(new_company_attributes)

      # creates the company (and all default relationships to it, @see CompanyService::Create)
      @company = new_company_service.call.company

      # update the HQ site that new_company_service has created with tire_program (if any)
      @company.hq.update_attributes!(tire_program: site_attributes[:tire_program])

      # creates a rescue_provider for the api_company
      rescue_provider = company_service_for_new_rescue_provider(
        api_company, @company.hq
      ).call.rescue_provider

      # creates an account for the api_company and the new @company created
      company_service_for_new_account.call

      create_new_rescue_provider_tag_for_fleet(rescue_provider)

      # creates a rescue_provider_tag for Swoop
      rescue_provider_for_swoop = new_company_service.rescue_provider_for_swoop
      company_service_for_new_rescue_provider_tag(rescue_provider_for_swoop, api_company.name).call

      # schedule an email to notify the new addded Partner that its HQ Site
      # has been enabled for dispatching for the fleet_company
      mail_fleet_enabled_dispatch_to_site(rescue_provider).call
    end

    if @company.valid?
      render 'api/v1/companies/show',
             locals: { accounts: false, company: @company }, status: :created
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  def add_existing_site
    ApplicationRecord.transaction do
      # @company is the partner company
      @company = RescueCompany.find(company_attributes[:id])

      # Update blank fields in the RescueCompany loaded above (aka @company).
      # For this action (aka add_existing_site), only fax can be updated in the UI form
      service_for_update_rescue_company_blank_fields_only(model_attributes: [:fax]).call

      @site = @company.sites.find(partner_params[:site_id])
      @site.update(dispatchable: true) unless @site.dispatchable?

      # creates a new rescue_provider for Fleet
      rescue_provider_for_fleet =
        company_service_for_new_rescue_provider(api_company, @site).call.rescue_provider

      find_or_create_account

      create_new_rescue_provider_tag_for_fleet(rescue_provider_for_fleet)

      # schedule an email to notify the new addded Partner that its HQ Site
      # has been enabled for dispatching for the fleet_company
      mail_fleet_enabled_dispatch_to_site(rescue_provider_for_fleet).call
    end

    render "api/v1/companies/show", locals: { accounts: false }
  end

  def add_site_to_existent_partner
    ApplicationRecord.transaction do
      @company = RescueCompany.find(company_attributes[:id])

      # Update blank fields in the RescueCompany loaded above (aka @company)
      #
      # For this action (aka add_site_to_existent_partner), the company fields
      # in the UI form are: [:dispatch_email, :phone, :accounting_email, :fax]
      service_for_update_rescue_company_blank_fields_only(
        model_attributes: [:dispatch_email, :phone, :accounting_email, :fax]
      ).call

      # creates a new site
      site = company_service_for_new_site(@company).call.site

      # creates a new rescue_provider for Fleet
      rescue_provider_for_fleet = company_service_for_new_rescue_provider(
        api_company, site
      ).call.rescue_provider

      # creates a new rescue_provider for Swoop
      rescue_provider_for_swoop = company_service_for_new_rescue_provider(
        Company.swoop, site
      ).call.rescue_provider

      find_or_create_account

      create_new_rescue_provider_tag_for_fleet(rescue_provider_for_fleet)

      # creates a rescue_provider_tag for Swoop
      company_service_for_new_rescue_provider_tag(rescue_provider_for_swoop, api_company.name).call

      # schedule an email to notify the Partner that one of its providers
      # has been enabled for dispatching for the fleet_company
      mail_fleet_enabled_dispatch_to_site(rescue_provider_for_fleet).call
    end

    if @company.valid?
      render 'api/v1/companies/show',
             locals: { accounts: false, company: @company }, status: :created
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  def show
    company = RescueCompany.find params[:id]

    render(
      partial: "api/v1/companies/partner_company",
      locals: { company: company, fleet_company: api_company }
    )
  end

  private

  def company_service_for_new_company(new_company_attributes)
    CompanyService::Create.new(
      company_attributes: new_company_attributes,
      rescue_provider_attributes: rescue_provider_attributes,
      location_attributes: location_attributes,
      feature_ids: [],
      api_company: api_company,
      api_user: api_user
    )
  end

  def company_service_for_new_rescue_provider(inhouse_company, site)
    CompanyService::CreateRescueProviderForInHouseFleet.new(
      attributes: rescue_provider_attributes,
      inhouse_company: inhouse_company,
      partner_company: @company,
      site: site
    )
  end

  def company_service_for_new_account
    CompanyService::CreateAccountForInHouseFleet.new(
      inhouse_company: api_company,
      partner_company: @company
    )
  end

  def company_service_for_new_rescue_provider_tag(rescue_provider, tag_name)
    CompanyService::CreateRescueProviderTag.new(
      rescue_provider: rescue_provider,
      tag_name: tag_name
    )
  end

  def company_service_for_new_site(partner_company)
    CompanyService::CreateSiteForInHouseFleet.new(
      site_attributes: site_attributes,
      location_attributes: location_attributes,
      partner_company: partner_company
    )
  end

  def mail_fleet_enabled_dispatch_to_site(rescue_provider)
    CompanyService::MailFleetEnabledDispatchToSite.new(
      fleet_company: api_company,
      fleet_user: api_user,
      rescue_provider: rescue_provider
    )
  end

  def find_or_create_account
    account = Account.find_by(company: @company, client_company: api_company)

    if account
      account.update deleted_at: nil if account.deleted_at
    else
      company_service_for_new_account.call
    end

    ServiceCodeServices::AddCustomServiceCodesToPartner.new(
      rescue_company: @company,
      client_company: api_company
    ).call
  end

  # When adding a new site to the fleet, the form in the UI
  # will allow some fields of the RescueCompany to be updated too,
  # but only if they are blank.
  #
  # The fields are: dispatch_email, phone, accounting_email, fax
  def service_for_update_rescue_company_blank_fields_only(model_attributes:)
    UpdateModelBlankFields.new(
      model_instance: @company,
      model_attributes: model_attributes,
      model_new_values: company_attributes
    )
  end

  def partner_params
    @partner_params ||= params[:partner]
  end

  def fetch_from_partner_and_sanitize(fields)
    selected = partner_params.select { |key, _| fields.include? key.to_sym }
    selected.permit(fields)
  end

  def site_attributes
    return @site_attributes if @site_attributes

    site_fields = [
      :name, :phone, :tire_program,
    ]

    @site_attributes = fetch_from_partner_and_sanitize(site_fields)
  end

  def company_attributes
    return @company_attributes if @company_attributes

    company_fields = [
      :id, :name,
      :dispatch_email, :phone, :accounting_email, :fax, # these fields apply to the company too
    ]

    @company_attributes = fetch_from_partner_and_sanitize(company_fields)
  end

  # Location param is composed by some attributes in the UI.
  # It's built in javascripts/fields/geosuggest_field.coffee and reused
  # oftenly.
  #
  # Its attributes don't come flattened, instead they come in the
  # location node. So we require location, and permit its fields.
  def location_attributes
    return @location_attributes if @location_attributes

    location_fields = [
      :address, :lat, :lng, :place_id, :exact,
    ]

    @location_attributes = partner_params.require(:location).permit(location_fields)
  end

  def rescue_provider_attributes
    return @rescue_provider_attributes if @rescue_provider_attributes

    rescue_provider_fields = [
      :dispatch_email, :phone, :accounting_email, :fax, # these fields apply to the company too
      :primary_contact, :primary_phone, :primary_email, :status, :vendor_code,
      :location_code, :notes,
    ]

    @rescue_provider_attributes =
      fetch_from_partner_and_sanitize(rescue_provider_fields)
  end

  def tag_id_from_params
    @tag_id_from_params ||= params.require(:partner).permit(tags: [])[:tags]&.first
  end

  def render_batch
    render template: "api/v1/companies/index", locals: { accounts: true }
  end

  # TODO this should be moved to a service, together with most of the logic
  # contained in this controller
  def create_new_rescue_provider_tag_for_fleet(rescue_provider)
    return if tag_id_from_params.blank?

    network_status = Tag.find(tag_id_from_params)

    # creates a rescue_provider_tag for the api_company
    company_service_for_new_rescue_provider_tag(rescue_provider, network_status.name).call
  end

end
