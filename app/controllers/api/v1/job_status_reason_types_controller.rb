# frozen_string_literal: true

class API::V1::JobStatusReasonTypesController < API::APIController

  before_action :doorkeeper_authorize!

  def index
    # this list only has 12 rows, so all good to fetch all rows for now
    render json: JobStatusReasonType.includes(:job_status).all
  end

end
