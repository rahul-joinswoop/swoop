# frozen_string_literal: true

class API::V1::RatesController < API::APIController

  REST_CLASS = Rate

  before_action :doorkeeper_authorize! # Authentication

  def index
    logger.debug "rates: #{@rates.count}"
    render template: "api/v1/rates/index"
  end

  def show
    render template: "api/v1/rates/show"
  end

  private

  def rates_filters
    rates_filters = rate_params.to_h.slice(:account_id, :site_id, :vendor_id, :service_code_id).map do |key, value|
      if value.blank? || value == 'null'
        { key => nil }
      else
        { key => value }
      end
    end

    rates_filters.reduce({}, :merge)
  end

  def set_rates
    @rates = api_company.rates.includes(:account, :site)
  end

  def set_rate
    @rate = api_company.rates.find_by!({ id: params[:id] })
  end

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use create_based_on_defaults route instead
  def set_account_service_index
    account_id = params[:account_id]
    if account_id == 'null'
      account_id = nil
    end
    service_id = params[:service_id]
    if service_id == 'null'
      service_id = nil
    end

    where = { account_id: account_id, service_code: service_id, deleted_at: nil }
    @rates = api_company.rates.where(where).includes(:account, :site)
  end

end
