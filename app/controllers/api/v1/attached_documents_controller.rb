# frozen_string_literal: true

class API::V1::AttachedDocumentsController < API::APIController

  include API::V1::Shared::Storage

  before_action :doorkeeper_authorize!

  before_action :set_job # Required to be implemented by each subclass

  def set_job
    raise UncallableAbstractParentMethodError
  end

  def create
    p = attached_document_params
    p[:user_id] = api_user.id
    p[:job_id] = @job.id

    @attached_document = AttachedDocument.create!(p)
    @attached_document.finalize_document

    respond_to do |format|
      format.json { render :show, status: :created }
    end
  end

  def update
    @attached_document = AttachedDocument.find(params[:id])
    @attached_document.update!(attached_document_params)

    respond_to do |format|
      format.json { render :show }
    end
  end

  private

  def attached_document_params
    args = params.require(:attached_document).permit(
      :type,
      :deleted_at,
      :document,
      :container,
      :invoice_attachment,
      location: [
        :lat,
        :lng,
        :place_id,
        :address,
        :exact,
        :location_type_id,
      ]
    )

    if args[:location]
      args[:location_attributes] = args[:location]
      args.delete(:location)
    elsif params["attached_document"].key?("location") && params["attached_document"]["location"].nil?
      args["location"] = nil
    end
    args
  end

end
