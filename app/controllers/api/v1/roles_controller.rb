# frozen_string_literal: true

class API::V1::RolesController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  def index
    @roles = Role.all

    render json: @roles, each_serializer: RoleSerializer
  end

end
