# frozen_string_literal: true

class API::V1::ReferersController < API::APIController

  def index
    @referers =
      Referer.not_deleted.sort do |a, b|
        if a.name == "Other"
          1
        elsif b.name == "Other"
          -1
        else
          a.name.casecmp(b.name)
        end
      end

    render json: @referers, each_serializer: RefererSerializer
  end

end
