# frozen_string_literal: true

class API::V1::Partner::RequestCallbackController < API::APIController

  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_job, only: [:create]

  def create
    DigitalDispatch.strategy_for(
      job_digital_dispatcher: @job.digital_dispatcher, action: :request_call_back
    ).new(
      job: @job, api_user: api_user, call_back_hash: call_back_params.to_h
    ).call

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def set_job
    @job = Job.find_by!({ id: params[:id], rescue_company: api_company })
  end

  def call_back_params
    params.permit(:comments, :dispatch_number, :secondary_number)
  end

end
