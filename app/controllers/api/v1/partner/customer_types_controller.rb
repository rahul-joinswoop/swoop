# frozen_string_literal: true

# PDW: depricated , the cuatomer_types are returned on the /partner/accounts company
class API::V1::Partner::CustomerTypesController < API::V1::CustomerTypesController

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_customer_types, only: [:index]

  def index
    render template: "api/v1/customer_types/index"
  end

  private

  def set_customer_types
    @customer_types = api_company.customer_types
  end

end
