# frozen_string_literal: true

class API::V1::Partner::JobsBidController < API::APIController

  before_action :api_authorized_partner # Authorization
  before_action :set_job, only: [:bid, :reject]
  # N.B. Don't move these up to base as they will get called before the Authorization above

  def bid
    @bid = Auction::SubmitBidService.new(@job, api_company, bid_params[:minutes], bid_params[:candidate_id]).call

    render json: @bid
  end

  def reject
    # If a bid is in the requested state, reject it
    begin
      @bid = Auction::RejectBidService.new(@job, api_company, reject_params[:reject_reason_id], reject_params[:reject_reason_info]).call
    # If a bid is past the requested state, we can't re-reject it; just pretend this request never happened
    rescue Auction::CannotRejectABidThatIsPastRejectionStageError
      @bid = Auction::Bid.find_by!(company: api_company, job: @job)
    end

    render json: @bid
  end

  private

  def set_job
    @job = Job.bid_jobs(api_company).find(params[:id])
  end

  def bid_params
    params.permit(:minutes,
                  :candidate_id)
  end

  def reject_params
    params.permit(:reject_reason_id,
                  :reject_reason_info)
  end

end
