# frozen_string_literal: true

class API::V1::Partner::ReportsController < API::V1::ReportsBaseController

  before_action :api_authorized_partner # Authorization

  def index
    @company_reports = CompanyReport.where("company_id = ? OR (company_id IS NULL AND reports.company_type = 'RescueCompany')", api_company.id)
      .where("reports.deleted_at IS NULL")
      .joins(:report)
      .to_a.uniq { |c| c.report_id }
    render 'api/v1/reports/index'
  end

  def storage_list
    @company_report = CompanyReport.new(report: Report.storage_list, company: api_company)
    @report_result = @company_report.run(api_user, run_params)
    render 'api/v1/report_results/show'
  end

end
