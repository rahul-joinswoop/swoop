# frozen_string_literal: true

class API::V1::Partner::LookerController < API::V1::LookerBaseController

  before_action :api_authorized_partner

end
