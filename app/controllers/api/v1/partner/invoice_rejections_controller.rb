# frozen_string_literal: true

# Controller to manage InvoiceRejections for Partners
class API::V1::Partner::InvoiceRejectionsController < API::APIController

  # Requires a list of invoice ids separated by comma and passed as parameter
  # :invoice_ids. For example: "invoice_ids=1,2,3"
  def index
    @invoice_rejections =
      InvoiceRejection.get_all_by_invoice_ids_and_company(
        params[:invoice_ids], api_company
      )

    if @invoice_rejections
      render "api/v1/invoice_rejections/index"
    else
      message = { status: "404", message: "Not Found" }
      render json: message, status: :not_found
    end
  end

end
