# frozen_string_literal: true

class API::V1::Partner::AccountsController < API::V1::AccountsController

  REST_CLASS = Account

  MOTORCLUBS = [
    Company::AGERO,
    Company::ALLIED_DISPATCH_SOLUTIONS,
    Company::ALLSTATE,
    Company::GEICO,
    Company::NATION_SAFE_DRIVERS,
    Company::QUEST,
    Company::ROAD_AMERICA,
    Company::TESLA_MOTORS_INC,
    Company::USAC,
  ].freeze

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_account, only: [:show, :update]
  before_action :set_accounts_with_rates, only: [:with_rates]

  respond_to :json

  def create
    args = account_params
    @account = Account.new(args)
    @account.company = api_company

    # NB - PG - Quinn says that magic names are fine
    if MOTORCLUBS.include? @account.name
      @account.client_company = FleetCompany.find_by!(name: @account.name)
      # Check if this already exists and had been deleted
      prev_account = Account.find_by(
        company_id: api_company.id, client_company: @account.client_company
      )

      if prev_account
        @account = prev_account
        return update(remove_delete: true)
      end
    end

    Account.transaction do
      @account.save!(args)
      update_dispatchable_sites if @dispatchable_sites_args
    end

    schedule_issc_fixups

    render 'api/v1/accounts/show', status: :created
  end

  def update(remove_delete: false)
    args = account_params(remove_delete: remove_delete)

    Account.transaction do
      @account.update!(args)
      update_dispatchable_sites if @dispatchable_sites_args
    end

    schedule_issc_fixups

    render "api/v1/accounts/show", status: :ok
  end

  def show
    render "api/v1/accounts/show", status: :ok
  end

  # This will fetch all Accounts that have not deleted Rates,
  # and will structure the response back with site_ids and vendor_ids
  # appended when existent. This is mainly used by Rates table.
  #
  # e.g
  # [
  #   {
  #     account_id: 25,
  #     sites: [
  #       {
  #         id: null,
  #         vendor_ids: [null]
  #       },
  #       {
  #         id: 1,
  #         vendor_ids: [null]
  #       },
  #       {
  #         id: 1,
  #         vendor_ids: ['CA12345', 'NY998877']
  #       },
  #       {
  #         id: 2,
  #         vendor_ids: ['BR123321']
  #       }
  #     ]
  #   }
  # ]
  def index_structured_for_rates
    accounts_with_rates = AccountServices::FetchAccountsThatHaveRates.call(
      input: { api_company_id: api_company.id }
    ).output

    render json: accounts_with_rates
  end

  # DEPRICATED in favor of new 'all_structured_for_rates' ^ (can be removed after VendorID Rates FE is shipped)
  # use all_structured_for_rates route instead
  def with_rates
    render template: "api/v1/accounts/index"
  end

  def without_rates
    ids_with_rates = api_company.rates.select(:account_id)
      .where.not(account_id: nil)
      .group(:account_id)
      .map(&:account_id)

    @accounts = api_company.accounts
      .where(deleted_at: nil)
      .where.not(id: ids_with_rates)

    render template: "api/v1/accounts/index"
  end

  def motorclub
    @accounts = Account.where(company: api_company)
      .joins(:client_company)
      .where('companies.issc_client_id IS NOT NULL')
      .where(deleted_at: nil)
      .includes(:location, :physical_location, :client_company, company: [:sites])

    render template: "api/v1/accounts/index"
  end

  private

  def set_account
    @account = Account.find_by!({ id: params[:id], company: api_company })
  end

  # DEPRICATED in favor of new 'all_structured_for_rates' ^ (can be removed after VendorID Rates FE is shipped)
  # use all_structured_for_rates route instead
  def set_accounts_with_rates
    @accounts = []
    api_company.rates.joins(:account).select(:account_id)
      .where(accounts: { deleted_at: nil })
      .where.not(account_id: nil).group(:account_id).each do |rate|
        @accounts << rate.account
      end
  end

  def updating_email?(args)
    (args.keys & ["accounting_email", "accounting_email_cc", "accounting_email_bcc"]).any?
  end

  def update_dispatchable_sites
    raise ArgumentError, "RescueCompany missing" unless @account.company
    raise ArgumentError, "FleetCompany missing" unless @account.client_company

    is_agero = (@account.client_company == Company.agero)

    system = if is_agero
               :rsc
             else
               :issc
             end

    impl_class = DigitalDispatch.strategy_for(job_digital_dispatcher: system,
                                              action: :dispatchable_sites)

    Rails.logger.debug "AccountsController::update_dispatchable_sites - using strategy: #{impl_class.name}"
    impl_class.new(@account.company, @account.client_company, @dispatchable_sites_args).call
  end

  def schedule_issc_fixups
    # IsscFixLogins.perform_async
    # IsscFixLogouts.perform_async
    IsscFixDeleted.perform_async
    IsscFixUnregistered.perform_async
    FixAccountlessRescueProviders.perform_async if @account.deleted_at
  end

  # phone, primary_email, notes
  def account_params(remove_delete: false)
    args = params.require(:account).permit(
      :name, :phone, :primary_email, :accounting_email, :phone, :notes, :deleted_at, :color,
      :auto_accept,
      :auto_eta,
      :department_id,
      :po_required_at_job_creation,
      :po_required_before_sending_invoice,
      :contact_email,
      :fax, :contact_name, :contact_phone, :accounting_email_cc, :accounting_email_bcc,
      :disable_qb_import,
      :hide_timestamps_on_invoice,
      physical_location: [:address, :lat, :lng, :place_id, :exact],
      location: [:address, :lat, :lng, :place_id, :exact],
      dispatchable_sites: [:site_id, :enabled, :locationid, :username, :password, contractorids: []]
    ).tap do |whitelisted|
      if params[:account][:dispatchable_sites]
        whitelisted[:dispatchable_sites] = params[:account][:dispatchable_sites]
      end
    end

    if args[:location]
      args[:location_attributes] = args[:location]
      args.delete(:location)
    end

    if args[:physical_location]
      args[:physical_location_attributes] = args[:physical_location]
      args.delete(:physical_location)
    end

    if args[:dispatchable_sites]
      @dispatchable_sites_args = args[:dispatchable_sites]
      args[:dispatchable_sites].each do |site|
        site["contractorids"] = [] if site["contractorids"].nil?
      end
      args.delete(:dispatchable_sites)
    end

    if remove_delete
      args[:deleted_at] = true
    end
    Rails.logger.debug "Passing #{args} #{@dispatchable_sites_args}"

    args
  end

end
