# frozen_string_literal: true

class API::V1::Partner::StripeWebhooksController < API::APIController

  include API::V1::Shared::Stripe::HandleEvent

  protect_from_forgery with: :null_session, except: [:account_event, :connect_event]

  respond_to :json

  # it will receive Stripe Account events
  def account_event
    Rails.logger.debug(
      "Partner::StripeWebhooksController(account_event) - " \
      "Stripe event received from the Account webhook, payload: #{request.raw_post}"
    )

    handle_stripe_account_event

    head :ok
  end

  # it will receive Stripe Connect events
  def connect_event
    Rails.logger.debug(
      "Partner::StripeWebhooksController(connect_event) - " \
      "Stripe event received from the Connect webhook, payload: #{request.raw_post}"
    )

    handle_stripe_connect_event

    head :ok
  end

end
