# frozen_string_literal: true

class API::V1::Partner::CommandsController < API::APIController

  before_action :api_authorized_partner # Authorization

  def process_cmd
    clz_name = "Commands::#{@command.classify}"
    clz = clz_name.safe_constantize
    if clz
      cmd = clz.new(user: api_user?,
                    company: api_company?,
                    json: request.raw_post,
                    request: request.uuid)
      cmd.type = clz.name
      cmd.save!
      logger.debug "Working on command: #{cmd.inspect}"
      @job = cmd.process
      respond_to do |format|
        if @job
          format.json { render "api/v1/jobs/show", status: :ok }
        else
          raise ArgumentError, @job.errors.to_json
        end
      end

    else
      raise ArgumentError, "Unknown command class: #{clz_name}"
    end
  end

  def command
    map_params

    if @command
      process_cmd
    end
  end

  def map_params
    p = params.permit(:command)
    @command = remove_param(:command, p)
    p
  end

end
