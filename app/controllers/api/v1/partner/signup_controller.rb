# frozen_string_literal: true

# {
#   "company": {
#       "name": "NewCo1",
#       "phone": "+14155559302",
#       "location": {
#         "lat": 37.7548833,
#         "lng": -122.4212138,
#         "place_id": "ChIJcx8DAD9-j4ARClnAM476u1M",
#         "address": "1120 Valencia Street, San Francisco, CA",
#         "exact": true
#     }
#   },
#
#   "user":{
#     "first_name":"Paul",
#     "last_name":"Wids",
#     "email":"paul+newco1@joinswoop.com",
#     "username":"paulnewco1",
#     "password":"43kuhergoijsdo8v",
#     "referer":"Advertisement"
#   }
# }

class API::V1::Partner::SignupController < API::APIController

  def create
    if RescueCompany.associated_with_phone(company_attributes[:phone])
      return duplicated_rescue_company_error
    end

    signup_service = SignupService::SignupCompany.new(
      company_attributes: company_attributes,
      location_attributes: company_location_attributes,
      user_attributes: user_params
    )

    begin
      signup_service.call_with_transaction

      @company = signup_service.company
      @user = signup_service.user

      render 'create', status: :created
    rescue ActiveRecord::RecordInvalid
      errs = {}

      if signup_service.user
        signup_service.user.errors.keys.map do |error|
          errs["user.#{error}"] = signup_service.user.errors[error]
        end
      end

      if signup_service.company
        signup_service.company.errors.keys.map do |error|
          errs["company.#{error}"] = signup_service.company.errors[error]
        end
      end

      render(
        json: errs,
        status: :unprocessable_entity
      )
    end
  end

  private

  def duplicated_rescue_company_error
    message = I18n.t(
      'company.error.duplicated_phone_error',
      phone: company_attributes[:phone]
    )

    logger.warn message

    render(json: { "company.phone": [message] }, status: :unprocessable_entity)
  end

  def company_attributes
    params.require(:company).permit(:name, :phone, :referer_id).tap do |company_attributes|
      company_attributes[:type] = "RescueCompany"
      company_attributes[:dispatch_email]   = user_params[:email]
      company_attributes[:accounting_email] = user_params[:email]
    end
  end

  def company_location_attributes
    params.require(:company).require(:location).permit(
      :address,
      :lat,
      :lng,
      :place_id,
      :exact
    )
  end

  def user_params
    params.require(:user).permit(
      :first_name,
      :last_name,
      :email,
      :username,
      :password
    )
  end

end
