# frozen_string_literal: true

class API::V1::Partner::StorageTypesController < API::APIController

  REST_CLASS = StorageType

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_storage_types, only: [:index]
  before_action :set_storage_type, only: [:update, :destroy, :show]

  def index
    render template: "api/v1/storage_types/index"
  end

  def show
    logger.debug "Rendering #{@storage_type}"
    render template: "api/v1/storage_types/show"
  end

  def create
    args = storage_type_params
    @storage_type = StorageType.new(args)
    @storage_type.company = api_company
    respond_to do |format|
      if @storage_type.save
        format.json { render 'api/v1/storage_types/show', status: :created }
      else
        format.json { render json: @storage_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    args = storage_type_params
    respond_to do |format|
      if @storage_type.update(args)
        format.json { render "api/v1/storage_types/show", status: :ok }
      else
        format.json { render json: @storage_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @storage_type.destroy!
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def set_storage_types
    @storage_types = StorageType.where({ company: api_company })
  end

  def set_storage_type
    @storage_type = StorageType.find_by!({ id: params[:id], company: api_company, deleted_at: nil })
  end

  def storage_type_params
    params.require(:storage_type).permit(:name, :deleted_at)
  end

end
