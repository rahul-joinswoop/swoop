# frozen_string_literal: true

class API::V1::Partner::SitesController < API::V1::SitesController

  REST_CLASS = Site

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_site, only: [:show, :update]
  before_action :set_sites, only: [:index]

  def index
    render template: "api/v1/sites/index"
  end

  def show
    render template: "api/v1/sites/show"
  end

  def update
    site_service_update = SiteService::Update.new(
      site_id: params[:id],
      site_params: site_params,
      company: api_company
    )

    ApplicationRecord.transaction do
      @site = site_service_update.call.site
    end

    if site_service_update.duplicated_site?
      site_type = site_service_update.duplicated_site.type == 'PoiSite' ? 'Place' : 'Site'

      return render json: { name: ["A #{site_type} with this name already exists"] }, status: :unprocessable_entity
    end

    respond_to do |format|
      if @site.valid?
        # Fix any iscss that might have been affected by this change
        # Currently these cause a race condition as it puts them in a trasnitionary period
        # before the site times calls them, will need to fix this if we want to add site
        # deleting into logging out of Isscs
        # IsscFixLogins.perform_async
        # IsscFixLogouts.perform_async
        CheckSiteTimesWorker.perform_async

        format.json { render "api/v1/sites/show", status: :ok, site: @site }
      else
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    site_service_create = SiteService::Create.new(
      site_params: site_params,
      site_type: PartnerSite,
      company: api_company
    )

    ApplicationRecord.transaction do
      @site = site_service_create.call.site
    end

    if site_service_create.duplicated_site?
      site_type = site_service_create.duplicated_site.type == 'PoiSite' ? 'Place' : 'Site'

      return render json: { name: ["A #{site_type} with this name already exists"] }, status: :unprocessable_entity
    end

    respond_to do |format|
      if @site.valid?
        @site.schedule_google_timezone

        format.json { render 'api/v1/sites/show', status: :created }
      else
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  def dispatchable
    @sites = PartnerSite.where(
      deleted_at: nil,
      company_id: api_company.id,
      dispatchable: true
    ).order(created_at: :desc)

    render template: "api/v1/sites/index"
  end

  def tire_program
    @sites = PartnerSite.where(
      deleted_at: nil,
      company_id: api_company.id,
      tire_program: true
    ).order(created_at: :desc)

    render template: "api/v1/sites/index"
  end

  private

  def set_site
    @site = Site.find_by!({ id: params[:id] })
  end

  def set_sites
    @sites = PartnerSite.where(deleted_at: nil, company_id: api_company.id).order(created_at: :desc)
  end

  def site_params
    args = params.require(:site).permit(
      :name,
      :always_open,
      :force_open,
      :dispatchable,
      :open_time,
      :close_time,
      :open_time_sat,
      :close_time_sat,
      :open_time_sun,
      :close_time_sun,
      :tire_program,
      :storage_lot,
      :phone,
      :deleted_at,
      :owner_company_id,
      location: [:id, :lat, :lng, :address, :place_id, :exact, :location_type_id]
    )

    if args[:location]
      args[:location_attributes] = args[:location]

      if @site && @site[:id]
        args[:location_attributes][:site_id] = @site[:id]
      end

      args.delete(:location)
    end

    args
  end

end
