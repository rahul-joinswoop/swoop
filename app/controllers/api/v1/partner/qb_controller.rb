# frozen_string_literal: true

class API::V1::Partner::QBController < API::APIController

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  def token
    api_authorized_admin

    @qb_token = QBServices::FetchToken.new(
      company_id: api_company.id
    ).call.qb_token

    if @qb_token.blank?
      return head :not_found
    end

    render json: @qb_token
  end

end
