# frozen_string_literal: true

class API::V1::Partner::TrailersController < API::APIController

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_trailer, only: [:show, :update, :destroy]
  before_action :set_trailers, only: [:index]

  def create
    args = trailer_params
    args[:company] = api_company
    @trailer = TrailerVehicle.new(args)
    respond_to do |format|
      if @trailer.save
        format.json { render 'api/v1/trailers/show', status: :created }
      else
        format.json { render json: @trailer.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    render template: "api/v1/trailers/index"
  end

  def show
    render template: "api/v1/trailers/show"
  end

  def destroy
    @trailer.deleted_at = Time.now
    respond_to do |format|
      if @trailer.save
        format.json { render json: {}, status: :ok }
      else
        format.json { render json: @trailer.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    p = trailer_params

    respond_to do |format|
      if @trailer.update(p)
        format.json { render "api/v1/trailers/show", status: :ok, trailer: @trailer }
      else
        format.json { render json: @trailer.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_trailer
    if api_root?
      @trailer = TrailerVehicle.find_by!({ id: params[:id] })
    else
      @trailer = TrailerVehicle.find_by!({ id: params[:id], company: api_company })
    end
  end

  def set_trailers
    if api_root?
      @trailers = TrailerVehicle.where(deleted_at: nil).order(created_at: :desc)
    else
      @trailers = api_company.trailers.where(deleted_at: nil).order(created_at: :desc)
    end
  end

  def trailer_params
    params.require(:trailer).permit(:name,
                                    :make,
                                    :model,
                                    :year,
                                    :vehicle_category_id,
                                    :deleted_at,)
  end

end
