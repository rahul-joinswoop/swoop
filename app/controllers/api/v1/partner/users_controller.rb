# frozen_string_literal: true

class API::V1::Partner::UsersController < API::APIController

  REST_CLASS = User

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  def jobs
    @jobs = api_user.current_jobs
    render template: "api/v1/jobs/index"
  end

end
