# frozen_string_literal: true

# GET {{host}}/api/v1/partner/accounts/search?page=1&per_page=5&search_terms%5Bterm%5D=guys%20rock
class API::V1::Partner::Search::AccountSearchController < API::V1::Search::AccountSearchController

  before_action :api_authorized_partner

end
