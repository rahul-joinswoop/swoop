# frozen_string_literal: true

class API::V1::Partner::TerritoriesController < API::APIController

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  def index
    result = Territories::Index.call user: api_user,
                                     company_id: params[:company_id]

    @territories = result.territories
  end

  def show
    result = Territories::Show.call user: api_user,
                                    territory_id: params[:id]

    @territory = result.territory
  end

  def destroy_all
    Territories::DestroyAll.call user: api_user,
                                 company_id: params[:company_id]

    head :ok
  end

  def create
    result = Territories::Create.call user: api_user,
                                      company_id: params[:company_id],
                                      polygon: params[:territory][:paths]
    @territory = result.territory

    render status: :created
  end

  def destroy
    Territories::Destroy.call user: api_user,
                              territory_id: params[:id]

    head :ok
  end

end
