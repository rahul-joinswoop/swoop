# frozen_string_literal: true

class API::V1::Partner::CompaniesController < API::V1::CompaniesController

  REST_CLASS = Company

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_company, only: [:drivers, :update]
  before_action :set_companies, only: [:index]
  before_action :set_sub_company, only: [:show, :update_subcompany, :destroy]

  def update
    args = company_params

    respond_to do |format|
      if @company.update(args)
        format.json { render "api/v1/companies/show", status: :ok, locals: { accounts: true } }
      else
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_commission
    @company = CompanyService::UpdateCommission.new(
      company: api_company,
      commission_attributes: comission_params
    ).call.company

    render template: 'api/v1/companies/show', status: :ok, locals: { accounts: true }
  end

  # Companies

  def update_subcompany
    respond_to do |format|
      if CompanyService::SaveWithJobs.call(company: @company,
                                           params: sub_company_params)
        format.json { render "api/v1/companies/show", status: :ok, locals: { accounts: true } }
      else
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_role_permissions
    @company = CompanyService::UpdateRolePermissionsByType.call(
      input: {
        company: api_company,
        permission_type: role_permissions_params[:permission_type],
        role_ids: role_permissions_params[:role_ids],
      }
    ).output[:company]

    render template: 'api/v1/companies/show', status: :ok, locals: { accounts: true }
  end

  def destroy
    if !@company.parent_company_id
      raise ArgumentError, "Can't remove an original company"
    end
    if @company.sites.length > 0
      raise ArgumentError, "Can't remove a company that has sites"
    end
    @company.destroy!
    respond_to do |format|
      format.html { redirect_to twilio_replies_url, notice: 'Company was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def index
    render template: "api/v1/companies/index", locals: { accounts: true }
  end

  def show
    # logger.debug "Rendering #{@company}"
    render template: "api/v1/companies/show", locals: { accounts: true }
  end

  def create
    args = sub_company_params
    @company = RescueCompany.new(args)
    @company.parent_company_id = api_company.id

    respond_to do |format|
      if @company.save
        format.json { render 'api/v1/companies/show', locals: { accounts: true }, status: :created }
      else
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def drivers
    @users = @company.users.with_role(:driver)
    render template: "api/v1/users/index"
  end

  private

  def set_sub_company
    @company = Company.find(params[:id])

    raise SecurityError unless api_user.company == @company
  end

  def set_company
    # PDW: don't load this from the cache as it's used for updating
    # (results in 'RescueCompany is marked as readonly' error)
    @company = Company.find(api_company.id)
  end

  def query_includes
    [
      :location,
      :service_codes,
      :accounts,
      :features,
      :vehicle_categories,
      :departments,
      live_sites: [:location],
    ]
  end

  def render_batch
    render template: "api/v1/companies/index", locals: { accounts: true }
  end

  def set_companies
    @companies = Company.where(parent_company_id: api_company.id)
      .order(created_at: :desc).includes(*query_includes)

    @companies.to_a.unshift(api_company)
  end

  def company_params
    params.require(:company).permit(
      :phone,
      :accounting_email,
      :dispatch_email,
      :review_email,
      :review_email_recipient_key,
      :disable_review_emails,
      :invoice_minimum_mins,
      :invoice_increment_mins,
      :invoice_roundup_mins,
      :invoice_mileage_rounding,
      :invoice_mileage_minimum,
      :pickup_waiver,
      :dropoff_waiver,
      :invoice_waiver
    )
  end

  def role_permissions_params
    params.require(:company).require(:role_permissions).permit(:permission_type, role_ids: [])
  end

  def comission_params
    params.require(:company).require(:commission).permit(:id, :default_pct)
  end

  def sub_company_params
    params.require(:company).permit(:name, :tax_id)
  end

end
