# frozen_string_literal: true

class API::V1::Partner::InventoryItemsController < API::V1::InventoryItemsController

  before_action :api_authorized_partner # Authorization

  before_action :set_inventory_item, only: [:show, :update, :destroy]

  def create
    args = map_params

    @inventory_item = InventoryItem.new(**args, company: api_company, user: api_user)

    respond_to do |format|
      logger.debug "SM: Calling inventory_item.save #{@inventory_item.inspect}"
      if @inventory_item.save
        logger.debug "SM: Calling inventory_item.save returned"

        @event.target = @inventory_item
        @event.save!

        format.json { render "api/v1/inventory_items/show" }
      else
        raise ArgumentError, @inventory_item.errors.to_json
      end
    end
  end

  def tires
    @inventory_items =
      InventoryItem.where(item_type: 'Tire', company: api_company, deleted_at: nil).includes(:user)
    render "api/v1/inventory_items/index"
  end

  def inventory_item_params
    args = params.require(:inventory_item)
      .permit(
        :site_id,
        :released_at,
        :deleted_at,
        item: [
          :type,
          :tire_type_id,
        ]
      )

    attributize(args, [:item])

    args
  end

  def set_inventory_item
    @inventory_item = InventoryItem.find_by!({ id: params[:id], company: api_company })
  end

end
