# frozen_string_literal: true

class API::V1::Partner::JobsStatusController < API::V1::JobsStatusController

  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_job, only: [:update]

  def update_service
    PartnerUpdateJobState
  end

  private

  def set_job
    @job = Job.find_by!({ id: params[:id], rescue_company: api_company })
  end

end
