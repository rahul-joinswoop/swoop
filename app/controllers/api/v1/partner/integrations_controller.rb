# frozen_string_literal: true

class API::V1::Partner::IntegrationsController < API::APIController

  before_action :doorkeeper_authorize!
  before_action :api_authorized_partner
  before_action :set_company

  respond_to :json

  def destroy
    api_access_token = APIAccessToken.find_by(company_id: @company.id, vendorid: params[:id])

    return head(:forbidden) unless api_access_token

    Agero::Rsc::DisconnectOauthWorker.perform_async(api_access_token.company_id, api_access_token.vendorid)

    render json: { success: true }
  end

  private

  def set_company
    @company = api_user.company
  end

end
