# frozen_string_literal: true

class API::V1::Partner::PartialJobsController < API::V1::PartialJobsController

  before_action :api_authorized_partner

  # storage tab
  has_scope :filter_stored, as: :stored, type: :boolean, allow_blank: true do |controller, scope, active|
    logger.debug "Active stored: #{active}"
    if active
      logger.debug "Active stored made it through"
      scope.filter_stored
    end
  end

  # Only a driver on the done tab
  has_scope :filter_rescue_driver_id, as: :rescue_driver_id

  PARTNER_FIELDS = [
    :child_job,
    :partner_department_id,
    :partner_live_invoices,
  ].freeze

  def get_jobs(conditions, page, per_page, order, includes)
    Rails.logger.debug "Partner::PartialJobsController::set_jobs - #{conditions},#{page},#{per_page},#{order}"

    jobs = apply_scopes(api_company.rescue_jobs).where(conditions).paginate(page: page, per_page: per_page).order(order)
      .includes(includes)
    jobs_total = apply_scopes(api_company.rescue_jobs).where(conditions).count

    [jobs, jobs_total]
  end

  def get_all_fields
    (COMMON_FIELDS + PARTNER_FIELDS).uniq
  end

end
