# frozen_string_literal: true

class API::V1::Partner::UserSearchController < API::V1::UserSearchController

  before_action :api_authorized_partner

end
