# frozen_string_literal: true

class API::V1::Partner::CommissionsController < API::APIController

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  def update_addons_exclusions
    @company = Commissions::UpdateCommissionExclusionService.new(
      company: api_company,
      commission_exclusion_to_add: commission_exclusion_to_add,
      commission_exclusion_to_remove: commission_exclusion_to_remove,
      type: :addon
    ).call.company

    render json: @company, each_serializer: CommissionExclusionSerializer
  end

  def update_services_exclusions
    @company = Commissions::UpdateCommissionExclusionService.new(
      company: api_company,
      commission_exclusion_to_add: commission_exclusion_to_add,
      commission_exclusion_to_remove: commission_exclusion_to_remove,
      type: :service
    ).call.company

    render json: @company, each_serializer: CommissionExclusionSerializer
  end

  private

  def commission_exclusion_to_add
    params[:commission_exclusions][:change].permit(add: [:service_code_id])[:add]
  end

  def commission_exclusion_to_remove
    params[:commission_exclusions][:change].permit(remove: [:service_code_id])[:remove]
  end

end
