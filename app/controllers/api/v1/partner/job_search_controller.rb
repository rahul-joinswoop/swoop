# frozen_string_literal: true

# NOTE: will replace Api::V1::Partner::JobsSearchController
class API::V1::Partner::JobSearchController < API::V1::JobSearchController

  before_action :api_authorized_partner # Authorization

end
