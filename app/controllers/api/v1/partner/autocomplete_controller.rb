# frozen_string_literal: true

class API::V1::Partner::AutocompleteController < API::V1::AutocompleteController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_partner, only: [:accounts] # Authorization

end
