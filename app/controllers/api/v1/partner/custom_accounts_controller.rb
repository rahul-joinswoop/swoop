# frozen_string_literal: true

class API::V1::Partner::CustomAccountsController < API::APIController

  REST_CLASS = CustomAccount

  before_action :doorkeeper_authorize!
  before_action :api_authorized_partner
  before_action :authorize_custom_account_access, except: [:show]

  respond_to :json

  def create
    symbolized_params = custom_account_params.to_h.symbolize_keys
    symbolized_params[:request_ip] = request.remote_ip

    account_creator = StripeIntegration::AccountCreator.new(
      symbolized_params.merge(api_company: api_company)
    )

    begin
      # rubocop:disable Rails/SaveBang
      stripe_account = account_creator.create
      # rubocop:enable Rails/SaveBang
      @custom_account = CustomAccount.create!({
        company: api_company,
        upstream_account_id: stripe_account.id,
        verification_fields_needed: stripe_account.verification.fields_needed,
      })

      StripeIntegration::VerificationFieldsSynchronizer.enqueue(stripe_account.id)

      email = EmailCustomAccountCreated.new(@custom_account.id)
      Delayed::Job.enqueue(email)

      render 'api/v1/custom_accounts/show', status: :created
    rescue Stripe::InvalidRequestError => e
      error = StripeIntegration::InvalidRequestError.new(invalid_request_error: e)

      render json: error, status: e.http_status
    end
  end

  def show
    service_call = CustomAccountServices::FetchWithBankAccount.call(
      input: { api_company_id: api_company.id }
    )

    if service_call.failure?
      return render json: {}, status: :ok
    end

    custom_account      = service_call.output[:custom_account]
    stripe_bank_account = service_call.output[:stripe_bank_account]

    if custom_account
      render json: custom_account, scope: { stripe_bank_account: stripe_bank_account }, status: :ok
    else
      render json: {}, status: :ok
    end
  end

  def update
    @custom_account = CustomAccount.not_deleted.find_by(company_id: api_company.id)

    retriever = StripeIntegration::BankAccountRetriever.new({
      stripe_account_id: @custom_account.upstream_account_id,
    })
    old_bank_account = retriever.retrieve

    creator = StripeIntegration::BankAccountCreator.new({
      stripe_account_id: @custom_account.upstream_account_id,
      token: custom_account_params[:token],
    })
    # rubocop:disable Rails/SaveBang
    @bank_account = creator.create
    # rubocop:enable Rails/SaveBang

    old_bank_account&.delete

    render 'api/v1/custom_accounts/show', status: :ok
  end

  private

  def custom_account_params
    params.require(:custom_account).permit(
      :address_city,
      :address_line1,
      :address_postal_code,
      :address_state,
      :business_name,
      :business_tax_id,
      :dob_day,
      :dob_month,
      :dob_year,
      :first_name,
      :last_name,
      :personal_address_city,
      :personal_address_line1,
      :personal_address_postal_code,
      :personal_address_state,
      :ssn_last_4,
      :token
    )
  end

  def authorize_custom_account_access
    raise SecurityError unless api_user.custom_account_visible?
  end

end
