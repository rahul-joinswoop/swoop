# frozen_string_literal: true

class API::V1::Partner::RejectJobController < API::APIController

  before_action :api_authorized_partner # Authorization

  def create
    @job = API::Jobs::RejectByPartner.new(
      job_id: params[:id],
      api_company_id: api_company.id,
      api_user_id: api_user.id,
      reject_params: reject_params
    ).call.job

    if @job.valid?
      render "api/v1/jobs/show", status: :ok
    else
      raise ArgumentError, @job.errors.to_json
    end
  end

  private

  def reject_params
    params.require(:job).permit(:reject_reason_id, :reject_reason_info)
  end

end
