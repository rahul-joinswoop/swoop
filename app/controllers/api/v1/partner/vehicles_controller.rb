# frozen_string_literal: true

require 'edmunds'
class API::V1::Partner::VehiclesController < API::APIController

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_vehicle, only: [:show, :update, :jobs, :logout, :destroy, :track]
  before_action :set_vehicles, only: [:index]

  def create
    args = vehicle_params
    args[:company] = api_company
    @vehicle = RescueVehicle.new(args)
    respond_to do |format|
      if @vehicle.save
        format.json { render 'api/v1/vehicles/show', status: :created }
      else
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    render template: "api/v1/vehicles/index"
  end

  def show
    logger.debug "trailers vehicles: #{@vehicles}"
    render template: "api/v1/vehicles/show"
  end

  def destroy
    @vehicle.deleted_at = Time.now
    respond_to do |format|
      if @vehicle.save
        format.json { render json: {}, status: :ok }
      else
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  def jobs
    @jobs = @vehicle.jobs
    render template: "api/v1/jobs/index"
  end

  def with_driver
    @vehicles = api_company.vehicles.where(deleted_at: nil).joins(:driver)

    render template: 'api/v1/vehicles/index'
  end

  def batch
    if params[:vehicle_ids]
      @vehicles = RescueVehicle.where(
        id: params[:vehicle_ids],
        deleted_at: nil,
        company: api_company
      )
    else
      @vehicles = []
    end

    render template: "api/v1/vehicles/index"
  end

  # Edmunds update
  def makes
    @makes = VehicleMake.all

    logger.debug "EDMUNDS: GOT BACK MAKES: ", @makes.inspect
    if @makes.size < 60
      logger.debug "EDMUNDS: FINDING MAKES"
      makes = Edmunds::Vehicle::Specification::Make::Makes.find
      logger.debug "EDMUNDS: FINDING MAKES ", @makes.inspect
      makes.makes.each do |make|
        vmake = VehicleMake.create!(name: make.name, edmunds_id: make.id)
        make.models.each do |model|
          years = model.years.collect(&:year)
          VehicleModel.create!(name: model.name, edmunds_id: model.id, vehicle_make: vmake, years: years)
        end
      end
      @makes = VehicleMake.all
    end
    logger.debug @makes
  end

  # When websockets is not available,
  # Mobile clients fallback to this method to send location updates.
  def location_update
    vehicle_params = location_vehicle_params
    vehicle_id = Integer(params[:id])
    lat = Float(vehicle_params[:lat])
    lng = Float(vehicle_params[:lng])
    UpdateVehicleLocationWorker.perform_async(api_company.id, vehicle_id, lat, lng, Time.now.utc.iso8601)
    render json: {}
  end

  def update
    if throttle?
      respond_to do |format|
        format.json { render "api/v1/vehicles/show", status: :ok, vehicle: @vehicle }
      end
      return
    end

    p = vehicle_params
    vehicle_service = ::Partner::VehicleService.new(api_user: api_user, vehicle: @vehicle, vehicle_params: p)
    p = vehicle_service.call.as_json['vehicle_params']
    lat = p.delete("lat")
    lng = p.delete("lng")
    if lat.present? && lng.present?
      UpdateVehicleLocationWorker.perform_async(api_company.id, @vehicle.id, lat.to_f, lng.to_f, Time.now.utc.iso8601)
    end
    unless p.empty?
      @vehicle.update(p)
    end

    respond_to do |format|
      if @vehicle.errors.empty?
        format.json { render "api/v1/vehicles/show", status: :ok, vehicle: @vehicle }
      else
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  def logout
    respond_to do |format|
      logger.debug "api_user vehicle: #{api_user.vehicle}"
      logger.debug "@vehicle: #{@vehicle}"
      if api_user.vehicle == @vehicle
        @vehicle.driver = nil

        if @vehicle.save
          logger.debug "api_user vehicle: #{api_user.vehicle}"
          format.json { render json: { status: 200, message: "logged out user from vehicle" }, status: :ok }
        else
          format.json { render json: @vehicle.errors + api_user.errors, status: :unprocessable_entity }
        end
      else
        format.json { render json: { message: "You are not allocated to that vehicle and therefore cannot logout of it" }, status: :unprocessable_entity }
      end
    end
  end

  private

  # Throttling mechanism because we were getting hundreds of updates per second from android at
  # one point and it took down the backend.
  #
  # Added guard so that it doesn't break tests
  def throttle?
    (rand(Integer(ENV['VEHICLE_LOCATION_UPDATE_DROP'] || 4)) != 0) && Rails.env.production?
  end

  def set_vehicle
    # TODO is this check correct? this is a partner controller, should root reach this?
    if api_root?
      @vehicle = RescueVehicle.find_by!({ id: params[:id] })
    else
      @vehicle = RescueVehicle.find_by!({ id: params[:id], company: api_company })
    end
  end

  def set_vehicles
    # TODO is this check correct? this is a partner controller, should root reach
    # this? also this is not identical to the query we use in
    # root/VehiclesController#set_vehicles
    if api_root?
      @vehicles = RescueVehicle.where(deleted_at: nil).order(created_at: :desc)
    else
      @vehicles = api_company.vehicles.where(deleted_at: nil).order(created_at: :desc)
    end
  end

  def vehicle_params
    params.require(:vehicle).permit(:name,
                                    :number,
                                    :make,
                                    :model,
                                    :year,
                                    :color,
                                    :vehicle_category_id,
                                    :lat,
                                    :lng,
                                    :deleted_at,
                                    :driver_id,
                                    :dedicated_to_swoop)
  end

  def location_vehicle_params
    params.require(:vehicle).permit(:lat, :lng)
  end

end
