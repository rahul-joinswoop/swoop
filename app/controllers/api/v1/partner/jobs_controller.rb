# frozen_string_literal: true

class API::V1::Partner::JobsController < API::V1::JobsBaseController

  REST_CLASS = RescueJob

  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_job, only: [:show, :update, :destroy, :get_location, :release_vehicle]
  before_action :set_jobs, only: [:index]

  def create
    @job = API::Jobs::CreateRescue.new(
      api_company: api_company,
      api_user: api_user,
      params: job_params,
      secondary_tow_params: secondary_tow_params,
      save_as_draft: params[:save_as_draft],
      save_as_predraft: params[:save_as_predraft]
    ).call.job

    super
  end

  def update
    params_to_update = map_params

    if @command
      process_cmd && return
    end

    if @job[:partner_dispatcher_id].nil? && params_to_update[:partner_dispatcher_id].nil?
      logger.debug "Partner::JobsController::update user id #{api_user.id}"

      params_to_update[:partner_dispatcher_id] = api_user.id
    end

    @job = API::Jobs::UpdateByRescueCompany.new(
      company: api_company,
      api_user: api_user,
      job_id: params[:id],
      params: params_to_update,
      job_history: params.dig(:job, :history),
      job_storage: params.dig(:job, :storage),
      browser_platform: browser.platform,
      request_user_agent: request.user_agent,
      save_as_draft: params[:save_as_draft],
    ).call.job

    if @job.valid?
      render template: 'api/v1/jobs/show', status: :ok
    else
      raise ArgumentError, @job.errors.to_json
    end
  end

  # POST 'partner/jobs/demo'
  def create_demo
    @job = API::Jobs::CreateDemoJob.new(rescue_company_id: api_company.id).call.job

    render template: 'api/v1/jobs/show', status: :ok
  end

  # POST 'partner/jobs/:id/get_location'
  def get_location
    respond_to do |format|
      @job.get_location_attempts += 1
      if @job.save
        @job.send_get_location
        format.json { render action: :show, status: :ok }
      else
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  def release_vehicle
    mapped_params = map_params
    release_date  = remove_param(:release_date, mapped_params)
    _ignored      = remove_param(:status, mapped_params)

    @job.assign_attributes(mapped_params)

    @job.inventory_items.each(&:save) # Force save of inventory items
    # since we manually look them up here
    # race condition added on 11/17/2016
    # but absolutely do not save the job

    update_recipient_on_job

    @job.released

    # Since we are going directly to the table in vehicle_items method call
    # in the jobs, the model EXPECTs for the inventory items to be saved
    # however seting the status to released will update the released_at for the inventory
    # items, so lets do this before we save the job.
    # With that said, we under no circumstances want to save the job at this
    # point. Mitch

    @job.create_or_update_release_status(release_date || Time.now)

    update_history(update_invoice: false)

    fail ArgumentError, @job.errors.to_json unless @job.save

    @job.update_invoice

    @show_full_invoice = true

    render template: 'api/v1/jobs/show'
  end

  def getCompanyType
    "RescueCompany"
  end

  def getJoin
    :rescue_company
  end

  def storage
    where = { deleted_at: nil }
    _db_order, _db_order_direction, order = get_order
    set_storage_jobs(where, order)
    render "api/v1/jobs/index"
  end

  def request_more_time
    async_request = DigitalDispatch::RequestMoreTime.new(
      api_company_id: api_company.id,
      api_user_id: api_user.id,
      job_id: params[:id],
    ).call.async_request

    render json: { async_request: async_request }
  end

  protected

  def job_filter(condition)
    apply_scopes(api_company.rescue_jobs).union(Job.displayable_bids_for_partners(api_company)).where(condition).distinct(:id)
  end

  private

  def secondary_tow_params
    secondary_tow = extract_second_tow

    Rails.logger.debug "Api::V1::Partner::JobsController secondary_tow: #{secondary_tow}"

    secondary_tow.present? ? secondary_tow : nil
  end

  def update_recipient_on_job
    if @job.invoice && @job.invoice.editable? &&
            (@job.saved_change_to_account_id? ||
             params[:job][:storage])
      # Since we are doing everything else synchronous lets do this as well
      # so we can give back upto date invoices.
      UpdateRecipientOnInvoice.new(@job.id).perform
      @job.reload
    end
  end

  def set_storage_jobs(condition, order)
    page = params[:page] || 1
    per_page = params[:per_page] || 1000

    @jobs = apply_scopes(api_company.rescue_jobs).where(condition).paginate(page: page, per_page: per_page).order(order)
      .includes(:service_code,
                :fleet_company,
                :rescue_company,
                :driver,
                :service_location,
                :drop_location,
                :review,
                :all_audit_job_statuses,
                :rescue_vehicle,
                :trailer_vehicle,
                :rescue_driver,
                :customer,
                :stranded_vehicle,
                :user,
                :bta,
                :eta,
                :sta,
                :fta,
                :account,
                :images,
                :question_results,
                :storage,
                :live_invoices,
                :child_jobs,
                issc_dispatch_request: [:issc])

    @jobs_total = apply_scopes(api_company.rescue_jobs).where(condition).count
  end

  def set_job
    @job = Job.find_by!({
      id: params[:id],
      rescue_company: api_company,
    })
  end

  def job_params
    params.require(:job).permit(:first_name,
                                :last_name,
                                :status,
                                :phone,
                                :email,
                                :service,
                                :service_alias_id,
                                :service_code_id,
                                :make,
                                :model,
                                :serial_number,
                                :vehicle_type,
                                :year,
                                :color,
                                :license,
                                :vin,
                                :price,
                                :step,
                                :partner_notes,
                                :dispatch_notes,
                                :scheduled_for,
                                :po_number,
                                :ref_number,
                                :unit_number,
                                :odometer,
                                #:vip, Only settable by fleet
                                #:uber,  Only settable by fleet
                                #:alternate_transportation,  Only settable by fleet
                                :bta,
                                :rescue_vehicle_id,
                                :rescue_driver_id,
                                :customer_type_id,
                                :no_keys,
                                :keys_in_trunk,
                                :four_wheel_drive,
                                :blocking_traffic,
                                :accident,
                                :unattended,
                                :reject_reason_id,
                                :reject_reason_info,
                                :eta_explanation_id,
                                :driver_notes,
                                :invoice_vehicle_category_id,
                                :get_location_sms,
                                :review_sms,
                                :partner_dispatcher_id,
                                :partner_sms_track_driver,
                                :send_sms_to_pickup,
                                :will_store,
                                :site_id,
                                :storage_type_id,
                                :partner_department_id,
                                :text_eta_updates,
                                :release_date,
                                :trailer_vehicle_id,
                                :pcc_coverage_id,
                                :symptom_id,
                                pickup_contact: [:id, :full_name, :first_name, :last_name, :phone],
                                dropoff_contact: [:id, :full_name, :first_name, :last_name, :phone],
                                service_location: [:id, :lat, :lng, :address, :exact, :place_id, :site_id, :location_type_id],
                                drop_location: [:id, :lat, :lng, :address, :exact, :place_id, :site_id, :location_type_id],
                                storage: [:id, :site_id, :released_to_account_id, :released_to_email, :replace_invoice_info, released_to_user: [:id, :full_name, :phone, :license_state, :license_id, :storage_relationship, location: [:id, :lat, :lng, :address, :place_id, :exact]]],
                                caller: [:id, :full_name, :phone],
                                customer: [:id, :full_name, :phone, :member_number],
                                rescue_vehicle: [:id],
                                rescue_driver: [:id],
                                account: [:id, :name],
                                toa: [:latest],
                                images: [:id, :url, :lat, :lng, :deleted_at])
  end

  def extract_second_tow
    params.require(:job).permit(
      :second_tow_scheduled_for,
      second_tow_dropoff: [
        :id,
        :lat,
        :lng,
        :address,
        :place_id,
        :exact,
        :site_id,
      ]
    )
  end

end
