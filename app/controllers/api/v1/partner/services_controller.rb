# frozen_string_literal: true

class API::V1::Partner::ServicesController < API::V1::ServicesController

  REST_CLASS = ServiceCode

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_services_with_rates, only: [:with_rates_by_account]

  def index_by_rates_filter
    @services = []

    Rails.logger.debug(
      "Partner::ServicesController#index_structured_for_rates" \
      "about to fetch services by rate_filter_params #{rate_filter_params}"
    )

    api_company.rates.select(:service_code_id).where(rate_filter_params)
      .where.not(service_code_id: nil).group(:service_code_id).each do |rate|
        @services << rate.service_code
      end

    Rails.logger.debug(
      "Partner::ServicesController#index_structured_for_rates" \
      "services found #{@services.map(&:id)}"
    )

    render template: "api/v1/services/index"
  end

  def with_rates_by_account
    render template: "api/v1/services/index"
  end

  private

  def set_services_with_rates
    account_id = params[:account_id]
    if account_id == 'null'
      account_id = nil
    end
    logger.debug "account_id #{account_id}"

    @services = []
    api_company.rates.select(:service_code_id)
      .where(account_id: account_id)
      .where.not(service_code_id: nil)
      .group(:service_code_id).each do |rate|
        @services << rate.service_code
      end
  end

end
