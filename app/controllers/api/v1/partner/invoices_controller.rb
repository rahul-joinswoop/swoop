# frozen_string_literal: true

class API::V1::Partner::InvoicesController < API::V1::InvoicesController

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_partner # Authorization
  before_action :set_invoice, only: [:show, :update, :resend]
  before_action :set_invoices, only: [:index]

  def controller_name
    'partner'
  end

  def resend
    email_params = params.permit(:email_to, :email_cc, :email_bcc)
    @invoice.resend_invoice(
      to: email_params[:email_to],
      cc: email_params[:email_cc],
      bcc: email_params[:email_bcc]
    )
    render "api/v1/invoices/show"
  end

  def set_conditional_invoices(conditions, page, per_page, order)
    @invoices = apply_scopes(api_company.outgoing_invoices)
      .associated_rendered_models
      .where(conditions)
      .paginate(page: page, per_page: per_page)
      .order(order)
      .includes([:line_items, :payments_or_credits])

    @invoices_total = apply_scopes(api_company.outgoing_invoices).where(conditions).count
  end

  # Checks if the 'state' is actually an 'event' that triggers a state transition
  # already supported by InvoiceService.
  #
  # If not supported, call super (/api/v1/invoices_controller#update) to
  # keep compatibility with legacy code and return.
  #
  # Now, if it's supported by InvoiceService, retrieves the specif service class with
  # InvoiceService.transition_for and executes it.
  #
  # TODO both /api/v1/partner/invoices/:id and /api/v1/root/invoices/:id implements this.
  #       It should be refactored to one endpoint only, maybe in parent controller
  #       (/api/v1/invoices/:id)
  #
  # PATCH/PUT /api/v1/partner/invoices/:id
  def update
    if params[:invoice][:state].blank? ||
      !InvoiceService::TRANSITION_SERVICES.key?(params[:invoice][:state].to_sym)
      super && return
    end

    InvoiceService.transition_for(params[:invoice][:state].to_sym).new(
      invoice_id: params[:id],
      api_company_id: api_company.id,
      api_user_id: api_user.id
    ).call.save_mutated_objects
    @invoice.reload
    render 'api/v1/invoices/show', status: :ok
  rescue InvoiceService::MissingInvoiceError
    render json: {
      message: I18n.t('invoices_controller.error.not_found'),
      action: I18n.t('invoices_controller.error.default_action'),
    }, status: :bad_request
  rescue InvoiceService::InvoiceStateTransitionError
    render json: {
      message: I18n.t('invoices_controller.error.invalid_transition'),
      action: I18n.t('invoices_controller.error.default_action'),
    }, status: :unprocessable_entity
  end

  def charge
    async_request = InvoiceService::ChargeCard.call(
      input: {
        invoice_id: params[:id],
        invoice_charge_attributes: invoice_charge_attributes,
        company_id: api_company.id,
        user_id: api_user.id,
      }
    ).output

    render json: { async_request: async_request }
  end

  def refund
    async_request = InvoiceService::RefundCard.call(
      input: {
        invoice_id: params[:id],
        payment_id: params[:payment_id],
        company_id: api_company.id,
        user_id: api_user.id,
      }
    ).output

    render json: { async_request: async_request }
  end

  private

  def invoice_charge_attributes
    params.require(:invoice).require(:charge).permit(
      :token,
      :amount,
      :send_recipient_to,
      :memo
    ).to_h
  end

  def invoice_sender
    api_company
  end

  def set_invoice
    @invoice = Invoice.includes([:line_items, :payments_or_credits])
      .find_by!(id: params[:id], sender_id: api_company.id)
  end

end
