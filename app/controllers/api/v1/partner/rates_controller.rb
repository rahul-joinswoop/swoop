# frozen_string_literal: true

class API::V1::Partner::RatesController < API::V1::RatesController

  include API::V1::Shared::AddAndEditRates

  before_action :api_authorized_partner # Authorization Also allows root

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_rate, only: [:show, :update]
  before_action :set_rates, only: [:index]

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use fetch_by_filters route instead
  before_action :set_account_rates, only: [:account]

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use fetch_by_filters route instead
  before_action :set_account_service_index, only: [:account_service]

  def update
    @rate.assign_attributes(rate_params)
    update_additions(@rate)
    respond_to do |format|
      if @rate.save
        format.json { render "api/v1/rates/show", status: :ok }
      else
        raise ArgumentError, @rate.errors.to_json
      end
    end
  end

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use fetch_by_filters route instead
  def account
    render template: "api/v1/rates/index"
  end

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use fetch_by_filters route instead
  def account_service
    render template: "api/v1/rates/index"
  end

  # This route fetches rates give the filters passed on params
  #
  # GET rates/fetch_by_filters
  def fetch_by_filters
    @rates = api_company.rates.where(rates_filters.merge(deleted_at: nil)).includes(:account, :site)

    render json: @rates
  end

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use create_based_on_defaults route instead
  def create_account
    account_id = params[:account_id]

    rates = api_company.rates.where(account_id: nil)

    copy_rates_service = RateService::CopyRates.new(
      rates: rates,
      company: api_company,
      new_attributes: { account_id: account_id }
    )

    @rates = copy_rates_service.call_with_transaction.new_rates

    render template: "api/v1/rates/index"
  end

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use create_based_on_defaults route instead
  def create_service
    account_id = params[:account_id]
    account_id = nil if account_id == "null"

    service_id = params[:service_id]
    service_id = nil if service_id == "null"

    rates = api_company.rates.where(
      account_id: account_id,
      service_code_id: nil
    )

    copy_rates_service = RateService::CopyRates.new(
      rates: rates,
      company: api_company,
      new_attributes: { account_id: account_id, service_code_id: service_id }
    )

    @rates = copy_rates_service.call_with_transaction.new_rates

    render template: "api/v1/rates/index"
  end

  # This route receives specific filter params from UI (Rates table)
  # to create new default rates for those params.
  #
  # POST rates/defaults
  def create_based_on_defaults
    service_call = RateService::FindDefaultsAndCopy.call(
      input: {
        api_company_id: api_company.id,
        rate_attributes_filter_hash: rates_filters,
      }
    )

    @rates = service_call.output

    render template: "api/v1/rates/index"
  end

  # post /rates/live
  def live
    Rails.logger.debug "partner /rates/live called with #{rate_params}"
    rate_type = rate_params[:type]
    rates = api_company.rates.where(rates_filters.merge(deleted_at: nil))

    ServiceRunner.new.tap do |runner|
      runner << RateService::UpdateLive.new(rates: rates, type: rate_type)
      runner << mail_rate_change_service
    end.call_all

    render template: "api/v1/rates/index"
  end

  def delete
    args = params.require(:rates).permit(
      :account_id, :site_id, :vendor_id, :service_code_id, :type, :vehicle_category_id
    )

    Rails.logger.debug("delargs: #{args}")

    rates = api_company.rates.where(args)
    account = Account.find(args[:account_id]) if args[:account_id]

    unless rates.empty?
      ServiceRunner.new.tap do |runner|
        runner << RateService::DeleteMultiple.new(
          rates: rates, account: account
        )
        runner << mail_rate_change_service
      end.call_all
    end

    head :no_content
  end

  private

  def check_unique_update(rate)
    dupes = api_company.rates.where(
      account: rate.account,
      site: rate.site,
      vendor_id: rate.vendor_id,
      service_code: rate.service_code,
      vehicle_category_id: rate.vehicle_category_id,
      type: rate.type,
      live: rate.live
    )
    if dupes.length > 0
      if dupes.length > 1
        raise ArgumentError, "Partner::RatesController#check_unique_update - " \
          "Found multiple existing rates for site(#{rate.site_id}), account(#{rate.account_id}), " \
          "service(#{rate.service_code_id}, rate(#{rate.type}), " \
          "vehicle_category_id(#{rate.vehicle_category_id}), vendor_id(#{rate.vendor_id})"
      end
      if dupes.first.id != rate.id
        raise ArgumentError, "Partner::RatesController#check_unique_update - " \
          "Found existing rate for site(#{rate.site_id}), account(#{rate.account_id}), " \
          "service(#{rate.service_code_id}, rate(#{rate.type}), " \
          "vehicle_category_id(#{rate.vehicle_category_id}, vendor_id(#{rate.vendor_id})" \
          "new rate: #{rate.inspect}, existing: #{dupes.first.inspect})"
      end
    end
  end

  def update_additions(rate)
    check_unique_update(rate)
    if rate.account
      rate.fleet_company = rate.account.client_company
    end
  end

  def rate_args
    [
      :type,
      :service_code_id,
      :account_id,
      :site_id,
      :vendor_id,
      :vehicle_category_id,
      :hourly,
      :flat,
      :hookup,
      :miles_towed,
      :miles_towed_free,
      :miles_enroute,
      :miles_enroute_free,
      :miles_deadhead,
      :miles_deadhead_free,
      :miles_p2p,
      :special_dolly,
      :gone,
      :storage_daily,
      :deleted_at,
      :service_group_id,
      :account_group_id,
      :second_day_charge,
      :beyond_charge,
      :seconds_free_window,
      :round,
      :nearest,
      :live,
      additions: [:id, :calc_type, :amount, :name, :deleted_at],
    ]
  end

  # Instantiate a RateService::MailRateChange
  # when rate is for a FleetCompany.
  #
  # If no account is found, it means that the change
  # is for the Defaults, and no email will to be sent.
  # It returns nil in this case.
  def mail_rate_change_service
    account = Account.find_by_id(account_id_from_params)
    client_company = account.client_company if account

    return if !client_company || client_company.motor_club?

    RateService::MailRateChange.new(
      company: rates_provider,
      user: api_user,
      company_to_be_notified: client_company
    )
  end

  # This is needed because FE sends the accound_id in different params format
  # for each type of action.
  def account_id_from_params
    if params[:rates].is_a? Array
      params[:rates].first[:account_id]
    elsif params[:rate].present?
      params[:rate][:account_id]
    else
      params[:rates][:account_id]
    end
  end

  # Required by Api::V1::Shared::AddAndEditRates
  alias rates_provider api_company

  # Required by Api::V1::Shared::AddAndEditRates
  # Returns nil as account_id will be set for each rate passed with a request
  def rates_account; end

  def rate_params
    args = params.require(:rate).permit(rate_args)

    logger.debug "rate args: #{args}"
    args
  end

  # DEPRICATED (can be removed after VendorID Rates FE is shipped)
  # use fetch_by_filters route instead
  def set_account_rates
    where = { account_id: params[:account_id], deleted_at: nil }
    @rates = api_company.rates.where(where).includes(:account, :site)
  end

end
