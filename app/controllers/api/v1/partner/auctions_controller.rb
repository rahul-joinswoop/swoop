# frozen_string_literal: true

class API::V1::Partner::AuctionsController < API::APIController

  before_action :api_authorized_partner # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above

  def index
    auctions = @api_company.auctions.where(status: [Auction::Auction::LIVE])

    render json: auctions, scope: api_company
  end

end
