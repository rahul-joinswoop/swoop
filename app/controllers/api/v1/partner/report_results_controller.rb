# frozen_string_literal: true

class API::V1::Partner::ReportResultsController < API::V1::ReportResultsBaseController

  before_action :api_authorized_partner # Authorization

  before_action :set_report_result, only: [:show]
  before_action :api_authorized_admin, only: [:show]

  def set_report_result
    @report_result = ReportResult.find_by!({ id: params[:id], company: api_company })
  end

end
