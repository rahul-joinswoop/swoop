# frozen_string_literal: true

class API::V1::Partner::AttachedDocumentsController < API::V1::AttachedDocumentsController

  before_action :api_authorized_partner

  private

  def set_job
    @job = Job.find_by!(id: params[:job_id], rescue_company: api_company)
  end

end
