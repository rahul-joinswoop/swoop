# frozen_string_literal: true

class API::V1::ReportResultsBaseController < API::APIController

  REST_CLASS = ReportResult

  before_action :doorkeeper_authorize! # Authentication

  def show
    render 'api/v1/report_results/show'
  end

end
