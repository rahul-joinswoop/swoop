# frozen_string_literal: true

# Endpoint for managing Oauth application credentials for accessing
# the GraphQL API.
class API::V1::OauthApplicationsController < API::APIController

  before_action :doorkeeper_authorize!
  before_action :api_authorized_admin
  before_action :api_authorized_fleet

  def index
    credentials = api_company.oauth_applications.map { |application| serialize_application(application) }
    render json: credentials
  end

  def create
    scopes = nil
    if api_company.fleet?
      scopes = :fleet
    elsif api_company.rescue?
      scopes = :rescue
    end
    application = api_company.oauth_applications.create!(
      name: "#{api_company.name} Client App",
      scopes: scopes,
      redirect_uri: "https://#{request.host_with_port}/",
      confidential: true,
    )
    render json: serialize_application(application), status: :created
  end

  def destroy
    application = api_company.oauth_applications.find(params[:id])
    application.destroy!
    render json: { id: application.id }
  end

  private

  def serialize_application(application)
    {
      id: application.id,
      name: application.name,
      client_id: application.uid,
      client_secret: application.secret,
    }
  end

end
