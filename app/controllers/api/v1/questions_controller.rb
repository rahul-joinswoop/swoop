# frozen_string_literal: true

class API::V1::QuestionsController < API::APIController

  # GET /features
  # GET /features.json
  def index
    @questions = api_company.company_service_questions.order(created_at: :desc)
    render 'api/v1/company_service_questions/index'
  end

end
