# frozen_string_literal: true

class API::V1::TireTypesController < API::APIController

  before_action :doorkeeper_authorize! # Authentication

  def index
    @tire_types = TireType.where(deleted_at: nil)

    render json: @tire_types, each_serializer: TireTypeSerializer
  end

  def batch
    @tire_types = TireTypeServices::Batch.new(
      tire_type_ids: params[:tire_type_ids]
    ).call

    render json: @tire_types, each_serializer: TireTypeSerializer
  end

end
