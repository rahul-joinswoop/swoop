# frozen_string_literal: true

class API::V1::ServicesController < API::APIController

  REST_CLASS = ServiceCode

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_services, only: [:index]

  def index
    render template: "api/v1/services/index"
  end

  def set_services
    if api_root?
      if params[:company_id]
        @services = Company.find(company_id).service_codes.order(created_at: :desc)
      else
        @services = ServiceCode.order(created_at: :desc)
      end
    else
      @services = api_company.service_codes.order(created_at: :desc)
    end
  end

  def standard
    @services = ServiceCodeServices::FetchAllStandard.new(
      api_company: api_company, addon: addon_param
    ).call

    render json: @services, each_serializer: ServiceCodeSerializer
  end

  def batch
    @services = ServiceCodeServices::Batch.new(
      api_company: api_company,
      service_code_ids: params[:service_ids]
    ).call

    render json: @services, each_serializer: ServiceCodeSerializer
  end

  def batch_by_name
    @services = ServiceCodeServices::BatchByName.new(
      api_company: api_company,
      service_code_names: params[:service_names],
      addon: addon_param
    ).call

    render json: @services, each_serializer: ServiceCodeSerializer
  end

  def change
    @services = ServiceCodeServices::Change.new(
      api_company: api_company,
      standard_services_ids_to_add: standard_services_ids_to_add_param,
      services_ids_to_remove: services_ids_to_remove_param,
      custom_service_names_to_add: custom_service_names_to_add_param,
      addon: addon_param
    ).call_with_transaction.service_codes_added

    render json: @services, each_serializer: ServiceCodeSerializer
  end

  private

  def addon_param
    if params[:addons]
      params[:addons]
    else
      [false, nil]
    end
  end

  def standard_services_ids_to_add_param
    params[:service][:change].permit(add: [])[:add]
  end

  def services_ids_to_remove_param
    params[:service][:change].permit(remove: [])[:remove]
  end

  def custom_service_names_to_add_param
    custom_service_names_param = params[:service][:change].permit(add_custom: [:name])[:add_custom]

    if custom_service_names_param.present?
      custom_service_names_param.map { |custom_service| custom_service.to_h.symbolize_keys }
    end
  end

  def rate_filter_params
    rate_filter_hash_from_params.map do |key, value|
      if value.blank? || value == 'null'
        { key => nil }
      else
        { key => value }
      end
    end.reduce({}, :merge)
  end

  def rate_filter_hash_from_params
    {
      account_id: params[:account_id],
      site_id: params[:site_id],
      vendor_id: params[:vendor_id],
    }
  end

end
