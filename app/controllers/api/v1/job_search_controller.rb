# frozen_string_literal: true

class API::V1::JobSearchController < API::APIController

  REST_CLASS = "Job"
  before_action :doorkeeper_authorize!
  around_action :set_time_zone

  def index
    Rails.logger.debug("search params (per-parsed) = #{params}")
    Rails.logger.debug("original search (per-parsed) = #{params[:original]}")

    service = JobSearchService.new({
      company: api_company,
      page: params[:page],
      per_page: params[:per_page],
    })

    @jobs_total, @jobs = service.search(params)

    render "api/v1/jobs/index"
  end

end
