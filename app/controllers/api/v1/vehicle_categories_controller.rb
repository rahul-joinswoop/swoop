# frozen_string_literal: true

class API::V1::VehicleCategoriesController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  def index
    @vehicle_categories = VehicleCategory.all

    render json: @vehicle_categories, each_serializer: VehicleCategorySerializer
  end

  def standard
    @vehicle_categories =
      VehicleCategoryServices::FetchAllStandard.new(api_company: api_company).call

    render json: @vehicle_categories, each_serializer: VehicleCategorySerializer
  end

  def batch
    @vehicle_categories = VehicleCategoryServices::Batch.new(
      api_company: api_company,
      vehicle_category_ids: params[:vehicle_category_ids]
    ).call

    render json: @vehicle_categories, each_serializer: VehicleCategorySerializer
  end

  def change
    @vehicle_categories = VehicleCategoryServices::Change.new(
      api_company: api_company,
      standard_vehicle_category_ids_to_add: standard_vehicle_category_ids_to_add,
      standard_vehicle_category_ids_to_remove: standard_vehicle_category_ids_to_remove,
      custom_vehicle_categories_names_to_add: custom_vehicle_categories_names_to_add,
    ).call.vehicle_categories_added

    render json: @vehicle_categories, each_serializer: VehicleCategorySerializer
  end

  private

  def standard_vehicle_category_ids_to_add
    params[:vehicle_category][:change].permit(add: [])[:add]
  end

  def standard_vehicle_category_ids_to_remove
    params[:vehicle_category][:change].permit(remove: [])[:remove]
  end

  def custom_vehicle_categories_names_to_add
    custom_vehicle_category_names_param =
      params[:vehicle_category][:change].permit(add_custom: [:name])[:add_custom]

    if custom_vehicle_category_names_param.present?
      custom_vehicle_category_names_param.map { |custom_vehicle| custom_vehicle.to_h.symbolize_keys }
    end
  end

end
