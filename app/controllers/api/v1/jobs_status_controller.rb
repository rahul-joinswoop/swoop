# frozen_string_literal: true

require "browser"

class API::V1::JobsStatusController < API::APIController

  REST_CLASS = RescueJob

  # TODO move this logic to a service
  def update
    args = job_params

    logger.debug "Api::V1::JobsStatusController called with #{args}"
    raise ArgumentError, "missing state" unless args[:status]

    original_status = @job.human_status

    # Constraining to scheduled only per ENG-2852.
    if @job.partner_dispatcher_id.nil? && @job.scheduled?
      @job.partner_dispatcher_id = api_user.id
    end

    set_platform(original_status, args[:status])

    update_service_instance.call_with_transaction

    if @job.save
      respond_to do |format|
        @job.update_digital_dispatcher_status
        format.json { render "api/v1/jobs/show", status: :ok }
      end
    else
      render json: @job.errors.to_json, status: 400
    end
  end

  def set_platform(old_status, new_status)
    if (old_status != "Completed") && (new_status == "Completed")
      @job.platform = parsed_user_agent[:platform]

      user = User.find(api_user.id)
      user.update!(
        user_agent: request.user_agent,
        platform: parsed_user_agent[:platform],
        browser: parsed_user_agent[:browser],
        version: parsed_user_agent[:version],
        build: parsed_user_agent[:build],
      )
    end
  end

  private

  def update_service_instance
    @update_service_instance ||= update_service.new(
      @job,
      job_params[:status],
      job_status_reason_type_id: job_status_reason_type_id_from_params,
      goa_amount: job_params.dig(:goa_amount),
      api_user: api_user,
      user_agent_hash: {
        platform: parsed_user_agent[:swoop_custom_platform],
        version: parsed_user_agent[:client_or_swoop_version],
      },
      referer: request.referer
    )
  end

  def parsed_user_agent
    @parsed_user_agent ||= UserAgentParser.new(request.user_agent).call
  end

  def job_params
    params.require(:job).permit(:status, :goa_amount, job_status_reason_types: [:id])
  end

  def job_status_reason_type_id_from_params
    return if job_params.dig(:job_status_reason_types).blank?

    job_params.dig(:job_status_reason_types).first["id"]
  end

end
