# frozen_string_literal: true

class API::V1::IssuesController < API::APIController

  before_action :api_authorized_user

  def index
    @issues = api_company.issues

    render 'api/v1/issues/index'
  end

end
