# frozen_string_literal: true

class API::V1::RejectReasonsController < API::APIController

  REST_CLASS = JobRejectReason

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_reject_reasons, only: [:index]

  def index
    render template: "api/v1/reject_reasons/index"
  end

  def set_reject_reasons
    @reject_reasons = JobRejectReason.all
  end

end
