# frozen_string_literal: true

###### WARNING - this class has no authentication or authorization (uuid based)

class API::V1::InventoryItemsController < API::APIController

  before_action :doorkeeper_authorize! # Authentication

  REST_CLASS = InventoryItem
  # GET /api/v1/invoices/:id
  def show
    render template: "api/v1/invoices/show"
  end

  # GET /api/v1/invoices
  def index
    #    logger.debug @invoices.inspect
  end

  # PATCH/PUT /api/v1/invoices/1
  def update
    InventoryItemService::Update.call(
      inventory_item: @inventory_item,
      params: map_params,
      event: @event
    )

    respond_to do |format|
      format.json { render "api/v1/inventory_items/show" }
    end
  end

  def destroy
    InventoryItemService::Update.call(
      inventory_item: @inventory_item,
      params: { deleted_at: Time.zone.now },
      event: @event
    )

    respond_to do |format|
      format.json { render "api/v1/inventory_items/show" }
    end
  end

  def released_to_user
    users = User.where(id: released_to_user_id).limit(1)
    if users.count > 0
      return users[0]
    end
    nil
  end

  def tire_quantity_by_site_and_type
    tires_by_site_and_type = InventoryItemService::Tire::QuantityBySiteAndType.new(
      api_company: api_company
    ).call

    render json: tires_by_site_and_type, each_serializer: TireQuantityBySiteAndTypeSerializer
  end

  def decrease_tire_quantity
    @inventory_item = InventoryItemService::Tire::DecreaseQuantity.new(
      site_id: inventory_item_params[:site_id],
      tire_type_id: inventory_item_params[:item_attributes][:tire_type_id],
      api_company: api_company
    ).call

    render 'api/v1/inventory_items/show'
  end

  def map_params
    inventory_item_params
  end

end
