# frozen_string_literal: true

class API::V1::LookerBaseController < API::APIController

  REST_CLASS = "Looker"

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_objects_for_view

  def api_objects_for_view
    @api_company = api_company
    @api_user    = api_user
  end

  def dashboards
    dashboards = api_company.looker_dashboards
    render json: dashboards
  end

  def look
    dashboard = api_company.looker_dashboards.find_by(id: look_params[:dashboard_id])

    if dashboard

      fifteen_minutes = 15 * 60

      url_data = {
        host: 'swoopme.looker.com',
        external_user_id: @api_user.id,
        first_name: @api_user.first_name,
        last_name: @api_user.last_name,
        permissions: ['access_data', 'see_looks', 'see_user_dashboards', 'download_with_limit'],
        models: ['swoop_trial'],
        group_ids: [8], # 8:Embedded
        #        external_group_id:  nil,
        user_attributes: { company: @api_user.company.name, company_id: @api_user.company.id },
        access_filters: {},
        session_length: fifteen_minutes,
        force_logout_login: true,
      }

      if ENV['LOOKER_API_CLIENTID']
        url_data[:embed_url] = "/embed/dashboards/#{dashboard.dashboardid}"
        Rails.logger.debug "Calling Looker with url_data:#{url_data}"
        url = External::Looker.created_signed_embed_url(url_data)
      else
        url = 's3-us-west-1.amazonaws.com/joinswoop.static/index.html'
      end
      @look = { url: "https://#{url}" }

      render 'api/v1/look/look'
    else
      render json: {}, status: 401
    end
  end

  private

  def look_params
    params.permit(:dashboard_id)
  end

end
