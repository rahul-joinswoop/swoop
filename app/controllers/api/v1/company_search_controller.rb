# frozen_string_literal: true

# Examples
#
# Find FleetMotorClubs
# {{host}}/api/v1/root/companies/search/?page=1&per_page=50&order=name,asc&original=type%3AFleetCompany&search_terms%5Btype%5D=FleetCompany&filters%5Bsubtype%5D=FleetMotorClub
#

class API::V1::CompanySearchController < API::APIController

  REST_CLASS = "Company"

  before_action :doorkeeper_authorize!

  def index
    params[:filters] ||= {}

    apply_filters

    service = CompanySearchService.new({
      page: params[:page],
      per_page: params[:per_page],
    })

    @total, records = service.search(params)

    if records.empty?
      @companies = records
    else
      # We include :fleet_in_house_clients and :fleet_managed_clients on the query so
      # the fleet client ids for a rescue company can be loaded in one single query.
      #
      # So :distinct clause is needed, otherwise the same rescue_company will be multiplied by <n> rescue_providers it has
      @companies = records.order(created_at: :desc).includes(*incls).distinct
    end

    render template
  end

  def template
    "api/v1/company_search/index"
  end

  def incls
    [
      :location,
      :service_codes,
      :accounts,
      :features,
      :vehicle_categories,
      :company_service_questions,
      :fleet_in_house_clients,
      :fleet_managed_clients,
      live_sites: [:location],
      companies_customer_types: [:customer_type],
    ]
  end

  private

  def apply_filters
    params[:filters][:parent_company_id] = api_company.id
  end

end
