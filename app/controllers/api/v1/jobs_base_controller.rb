# frozen_string_literal: true

class API::V1::JobsBaseController < API::APIController

  include Utils

  before_action :doorkeeper_authorize! # Authentication

  before_action :set_controller, only: [:index, :show, :create, :update]

  has_scope :filter_between, using: [:start_at, :end_at], type: :hash, as: :between
  has_scope :filter_full_name, as: :full_name
  has_scope :filter_vin, as: :vin
  has_scope :filter_search, as: :search
  has_scope :filter_rescue_driver_id, as: :rescue_driver_id
  has_scope :filter_before, as: :before
  has_scope :filter_after, as: :after
  has_scope :filter_fleet_managed_jobs, as: :fleet_managed_jobs
  has_scope :filter_storage_site, as: :storage_site_id

  has_scope :filter_storage_types, as: :shown_storage_types, type: :array, allow_blank: true do |controller, scope, value|
    value.present? ? scope.filter_storage_types(value) : scope.filter_storage_types([])
  end

  has_scope :filter_storage_sites, as: :shown_storage_sites, type: :array, allow_blank: true do |controller, scope, value|
    value.present? ? scope.filter_storage_sites(value) : scope.filter_storage_sites([])
  end

  has_scope :filter_active, as: :active, type: :boolean, allow_blank: true do |controller, scope, active|
    active ? scope.filter_active : scope.filter_done
    # scope.filter_active()
  end

  has_scope :filter_stored, as: :stored, type: :boolean, allow_blank: true do |controller, scope, active|
    if active
      scope.filter_stored
    end
  end

  def set_controller
    @controller = self
  end

  # GET /api/v1/jobs
  def index
    render "api/v1/jobs/index"
  end

  # GET /api/v1/jobs/map
  def map
    set_conditional_jobs({ status: Job.map_states, deleted_at: nil }, [{ adjusted_created_at: :desc }])
    render json: @jobs
  end

  # GET /api/v1/jobs/active
  def active
    set_conditional_jobs({ status: Job.active_and_draft_states, deleted_at: nil }, [{ adjusted_created_at: :desc }])
    render "api/v1/jobs/index"
  end

  def done
    where = { status: Job.done_states, deleted_at: nil }
    if params[:driver] && (params[:driver] == 'me')
      where[:rescue_driver_id] = api_user.id
    end
    _db_order, _db_order_direction, order = get_order

    set_conditional_jobs(where, order)
    render "api/v1/jobs/index"
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
    render "api/v1/jobs/show"
  end

  # POST /jobs/1/copy
  # POST /jobs/1/copy.json
  def copy
    duplicate_job_service = API::Jobs::Duplicate.new(
      current_job_id: params[:id],
      api_company_id: api_company.id,
      api_user_id: api_user.id
    )

    duplicate_job_service.call

    duplicated_job = duplicate_job_service.duplicated_job

    render partial: 'api/v1/jobs/job', locals: { job: duplicated_job, controller: @controller }
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @event.target = @job
    @event.save!

    respond_to do |format|
      format.json { render "api/v1/jobs/show", status: :created }
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    raise "PATCH status, don't DELETE job"
  end

  def update_history(update_invoice: true)
    history = params[:job][:history]
    if history
      save_storage = false
      history.each do |status, obj|
        js = JobStatus.find_by({ name: status })
        ajh = AuditJobStatus.find_by({ job_status: js, job: @job.id })
        previous = ajh.last_set_dttm
        if ajh.adjusted_dttm
          previous = ajh.adjusted_dttm
        end
        ajh.adjusted_dttm = obj[:adjusted_dttm]
        UpdateLastUpdatedStatusOnJob.new(@job, ajh, previous).call
        ajh.save!
        if @job.storage
          if ajh.job_status_id == JobStatus::STORED
            @job.storage.stored_at = obj[:adjusted_dttm]
            save_storage = true
          end
          if ajh.job_status_id == JobStatus::RELEASED
            @job.storage.released_at = obj[:adjusted_dttm]
            save_storage = true
          end
        end
      end

      if save_storage
        @job.storage.save!
        if update_invoice
          CreateOrUpdateInvoice.perform_async(@job.id)
        end
      end
    end
  end

  def send_details_email
    API::Jobs::SendDetailsEmail.call(
      input: {
        to: email_recipient_params[:to],
        cc: email_recipient_params[:cc],
        bcc: email_recipient_params[:bcc],
        job_id: params[:id],
        user_id: api_user.id,
        company_id: api_company.id,
      }
    )

    head :ok
  end

  protected

  # Subclasses must implement this method to find the list of the jobs to load
  # after applying both api company logic and the passed in query condition.
  def job_filter(condition)
    raise NotImplementedError
  end

  # Subclasses may override to add eager load extra associations that are only required
  # by the api company.
  def extra_job_includes
    []
  end

  def change_provider_to
    # PDW this is insecure as it allows fleets to change any job - moved to child class
    # @job = Job.find(params[:job_id])

    changer = JobProviderChanger.new(@job, params[:old_rescue_provider_id], params[:new_rescue_provider_id])
    changer.call

    ApplicationRecord.transaction do
      changer.save_mutated_objects
    end
    CreateOrUpdateInvoice.perform_async(@job.id)

    render 'api/v1/jobs/show'
  rescue JobProviderChanger::IncorrectOldCompanyError
    render(json: {
      message: 'Old Company Doesn\'t Match',
      action: 'Please copy this message into the support chat (in the bottom right)',
    },
           status: 422)
  end

  def email_recipient_params
    params.require(:email_recipients).permit(:to, :cc, :bcc)
  end

  def map_params
    API::MapJobParams.new(
      api_company: api_company,
      api_user: api_user,
      job: @job,
      params: job_params
    ).call
  end

  private

  # Job associations to preload when rendering partials/_job.jbuilder.json
  # JSON response. This prevents JBuilder and AMS from hammering the
  # database for Job associations. Subclasses that render additional Job
  # relationships in their JSON responses should add additional preloads
  # by implementing #extra_job_includes
  def job_preload_associations
    [
      { invoice: { line_items: [:rate] } },
      { live_invoices: { line_items: [:rate] } },
      { storage: [{ released_to_user: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] }, :item] },
      { customer: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] },
      { caller: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] },
      { root_dispatcher: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] },
      { fleet_dispatcher: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] },
      { user: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] },
      { pickup_contact: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] },
      { dropoff_contact: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] },
      { rescue_driver: [:company, :location, :job_status_email_statuses, :roles, :vehicle, :system_email_filters, :sites] },
      { question_results: [:question, :answer] },
      { review: [:replies, :survey] },
      { explanations: [:reason] },
      { fleet_company: [{ companies_customer_types: [:customer_type] }] },
      { estimate: [:service_location, :drop_location] },
      { audit_job_statuses_order_created: [:job_status, { user: :company }, :company] },
      :audit_job_statuses,
      { history_items: [{ user: :company }, :company] },
      :audit_sms,
      :child_jobs,
      { candidate_assignment: [:chosen_company, :vehicle] },
      { candidates: [:company, :site, :vehicle, :driver] },
      { auction: { bids: :company } },
      :view_alert,
      :rescue_company,
      :account,
      :last_touched_by,
      :issc_dispatch_request,
      :customer_type,
      :service_code,
      :symptom,
      :images,
      :attached_documents,
      :service_location,
      :drop_location,
      :rescue_vehicle,
      :stranded_vehicle,
      :site,
      :pta,
      :bta,
      :eta,
      :sta,
      :fta,
      :original_toa,
      :latest_pta,
      :latest_ata,
      :latest_eta,
      :time_of_arrivals_non_live,
      :auction,
      :bids,
    ]
  end

  # Sets the @jobs and @jobs_total variable with the list of jobs to load for the action.
  # The query to run is defined by the implementation of job_filter.
  def set_conditional_jobs(condition, order = { adjusted_created_at: :desc })
    per_page = params[:per_page].to_i

    if per_page <= 0
      # Set `per_page` with number of job_ids sent by the client, if `per_page` is not present in the params
      per_page = (condition[:id].present? ? Array(condition[:id]).size : 24)
    end

    # Strict limit for `per_page` to 100
    per_page = 100 if per_page > 100

    page = params[:page] || 1
    finder = job_filter(condition)
    @jobs = finder.paginate(page: page, per_page: per_page).order(order).preload(job_preload_associations + extra_job_includes)
    @jobs_total = finder.count
  end

  def set_job
    if api_company.rescue?
      @job = Job.find_by!({ id: params[:id], rescue_company: api_company })
    else
      @job = Job.find_by!({ id: params[:id], fleet_company: api_company })
    end
  end

  def set_jobs
    conditions = { deleted_at: nil }

    # {{host}}/api/v1/root/jobs/?job_ids=1076
    if params[:job_ids]
      conditions[:id] = params[:job_ids].split(',')
    end

    db_order, db_order_direction, order = get_order

    logger.debug "job conditions: #{conditions}"
    logger.debug "job order: #{order}"

    set_conditional_jobs(conditions, order)

    scopes = current_scopes
    if !scopes[:search].nil?
      search = Search.new(term: scopes[:search],
                          target: "Job",
                          company: api_company,
                          user: api_user,
                          url: request.fullpath,
                          scope: scopes.to_json,
                          order: db_order,
                          order_direction: db_order_direction,
                          page: params[:page] || 1,
                          per_page: params[:per_page] || 1000)
      search.save!
    end

    @event = Command.last
  end

  def get_order
    order = []
    db_order = nil
    db_order_direction = nil
    parse_order do |col, direction|
      if db_order.nil? || db_order_direction.nil?
        db_order = col
        db_order_direction = direction
      else
        db_order += "," + col
        db_order_direction += "," + direction
      end
      if col == "account"
        order << "accounts.name #{direction}"
      elsif col == "date"
        order << { adjusted_created_at: direction } # pdw this was fixed to desc ?? #"audit_completed_ats_jobs.last_set_dttm #{direction}"
      elsif col == "id"
        order << { id: direction }
      elsif col == "partner"
        order << "rescue_companies_jobs.name #{direction}"
      elsif col == "storage_date"
        order << "inventory_items.stored_at #{direction}"
      elsif col == "storage_days"
        order << "item.stored_at #{direction}" # TODO: this is a bit of a hack and won't always be right (but pretty close)
      elsif col == "last_status_changed_at"
        order << { last_status_changed_at: direction }
      else
        raise ArgumentError, "Unknown column for ordering: #{col}"
      end
    end
    if order.length == 0
      order << { adjusted_created_at: :desc }
    end
    [db_order, db_order_direction, order]
  end

  def set_platform
    # Make sure the job status is changing to completed.
    return if !(@job.status_changed? && @job.status.eql?(Job::STATUS_COMPLETED))

    @job.platform = if request.user_agent["okhttp"] || request.user_agent["android"]
                      'android'
                    else
                      browser.platform
                    end
  end

end
