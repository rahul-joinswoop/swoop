# frozen_string_literal: true

class API::V1::Search::AccountSearchController < API::APIController

  REST_CLASS = "Account"

  before_action :doorkeeper_authorize!

  def index
    incls = [:location, :physical_location, :client_company, { company: [:partner_sites] }]
    page = params[:page]
    per_page = params[:per_page]
    if !params[:search_terms]
      Rails.logger.debug "AccountSearchController - Searching using the DB"
      @accounts = Account.where({ company: api_company, deleted_at: nil }).order('name': :asc).paginate(page: page, per_page: per_page).includes(*incls)
      @total = Account.where({ company: api_company, deleted_at: nil }).count
    else
      Rails.logger.debug "AccountSearchController - Searching using ElasticSearch"
      params[:filters] ||= {}
      apply_filters

      service = ElasticSearchService::AccountElasticSearchService.new({
        page: page,
        per_page: per_page,
      })

      @total, records = service.search(params)

      @accounts = records.includes(*incls)
    end

    Rails.logger.debug "Got accounts: #{@accounts.map(&:name)}"

    render "api/v1/search/accounts/index"
  end

  private

  def apply_filters
    params[:filters][:company_id] = api_company.id
  end

end
