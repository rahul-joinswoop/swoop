# frozen_string_literal: true

class API::V1::Search::ProviderSearchController < API::APIController

  REST_CLASS = "Provider"

  before_action :doorkeeper_authorize!

  def index
    if params[:search_terms]
      search_using_elastic_search
    else
      search_using_db # fallback to DB for correct ordering on partners list
    end
  end

  private

  def apply_filters
    params[:filters][:company_id] = api_company.id
  end

  def search_using_db
    Rails.logger.debug "ProviderSearchController - Searching using the DB"

    @providers = RescueProvider.where(company: api_company, deleted_at: nil)
      .joins(:site, :provider)
      .includes(:provider)
      .paginate(page: params[:page], per_page: params[:per_page])
      .order('companies.name ASC, sites.name ASC')

    @total = RescueProvider.where(company: api_company, deleted_at: nil).count
  end

  def search_using_elastic_search
    Rails.logger.debug "ProviderSearchController - Searching using ElasticSearch"

    params[:filters] ||= {}

    apply_filters

    service = ElasticSearchService::ProviderElasticSearchService.new({
      page: params[:page],
      per_page: params[:per_page],
    })

    Rails.logger.debug "ProviderSearchController - params #{params}"

    search_params = params.permit(
      "search_terms" => ["term"], "filters" => ["company_id"]
    ).to_h

    @total, records = service.search(search_params)

    @providers = records.includes(:provider)
  end

end
