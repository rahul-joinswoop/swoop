# frozen_string_literal: true

#
# One route only exposed to Consumer Mobile Web experience: confirm/update job location.
#
class API::V1::GetLocationController < API::APIController

  protect_from_forgery with: :null_session

  #
  # Exposes a route to customers to confirm or update their location.
  # POST '/get_location'
  #
  def location
    @job = Job.find_by!(uuid: params[:uuid])

    loc = params.require(:location).permit(:lat, :lng, :address, :place_id)
    user = @job.driver.user

    data = {
      user: user,
      lat: loc[:lat],
      lng: loc[:lng],
      address: loc[:address],
      place_id: loc[:place_id],
      job_id: @job.id,
    }

    location = Location.create!(data)
    if !@job.original_service_location
      @job.original_service_location = @job.service_location
    end
    @job.service_location = location
    @job.location_updated = true
    @job.last_touched_by = @job.customer

    op = EventOperation.fetch_by_name(EventOperation::UPDATE)
    @event = Command.new(event_operation: op,
                         user: @job.customer,
                         company: nil,
                         json: request.raw_post,
                         target_id: @job.id,
                         target_type: Job)
    @event.save!

    respond_to do |format|
      if @job.save
        @event.update!(applied: true)
        @job.slack_notification("Customer updated Location", tags: [:sms, :location, :updated])
        format.json { head :no_content }
      else
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  protected

  def set_request_context
    context = RequestContext.current
    context.source_application_id = SourceApplication.consumer_mobile_web.id
  end

end
