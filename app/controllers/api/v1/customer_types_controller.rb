# frozen_string_literal: true

class API::V1::CustomerTypesController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  def batch
    @customer_types = CustomerTypeServices::Batch.new(
      api_company: api_company,
      customer_type_ids: params[:customer_type_ids]
    ).call

    render json: @customer_types, each_serializer: CustomerTypeSerializer
  end

end
