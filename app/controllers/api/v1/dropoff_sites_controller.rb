# frozen_string_literal: true

module API
  module V1
    class DropoffSitesController < API::APIController

      def dropoff_sites
        async_request = ::SiteService::DropoffSites.list_by(
          drop_sites_params: drop_sites_params,
          api_user: api_user
        )

        render json: async_request
      end

      def drop_sites_params
        params.permit(
          job: [
            :id,
            service: [:name],
            location: [
              service_location: [:lat, :lng, :google_place_id],
            ],
          ]
        )
      end

    end
  end
end

# POST /drop_off_sites {
#   job:{
#     service:{
#       name: <Service Name:String>
#     },
#     location: {
#       service_location:{
#         lat: <Latitude:Double>,
#         lng: <Longitude:Double>,
#         google_place_id:<PlaceID:String>
#       }
#     }
# }
