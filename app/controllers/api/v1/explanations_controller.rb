# frozen_string_literal: true

class API::V1::ExplanationsController < API::APIController

  before_action :api_authorized_user

  def create
    @job = fetch_job_for_api_company

    explanation = Explanation.create!(explanation_attributes)

    render json: explanation, include: [reason: [:issue]], status: :created
  end

  def update
    @job = fetch_job_for_api_company

    explanation = Explanation.find(params[:id])
    explanation.update!(explanation_params)

    render json: explanation
  end

  private

  def explanation_attributes
    attributes = explanation_params
    attributes[:job_id] = @job.id

    attributes
  end

  def explanation_params
    params.require(:explanation).permit(:issue_id, :reason_id, :deleted_at)
  end

end
