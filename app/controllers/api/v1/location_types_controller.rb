# frozen_string_literal: true

class API::V1::LocationTypesController < API::APIController

  before_action :doorkeeper_authorize!  # Authentication
  before_action :api_authorized_user    # Authorization

  def show
    @location_type = LocationType.find_by!(id: params[:id])

    render template: 'api/v1/location_types/show'
  end

  def search
    @location_type = LocationType.find_by!(search_params)

    render template: 'api/v1/location_types/show'
  end

  private

  def search_params
    { name: params[:name] }
  end

end
