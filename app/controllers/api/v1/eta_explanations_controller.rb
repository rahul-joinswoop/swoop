# frozen_string_literal: true

class API::V1::EtaExplanationsController < API::APIController

  REST_CLASS = JobEtaExplanation

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_eta_explanations, only: [:index]

  def index
    render template: "api/v1/eta_explanations/index"
  end

  def set_eta_explanations
    @eta_explanations = JobEtaExplanation.all
  end

end
