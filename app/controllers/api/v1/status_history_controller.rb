# frozen_string_literal: true

class API::V1::StatusHistoryController < API::APIController

  REST_CLASS = AuditJobStatus
  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet_or_rescue # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_audit_job_status, only: [:update]

  def update
    respond_to do |format|
      previous = @audit_job_status.last_set_dttm
      if @audit_job_status.adjusted_dttm
        previous = @audit_job_status.adjusted_dttm
      end
      if @audit_job_status.update(history_params)
        UpdateLastUpdatedStatusOnJob.new(@audit_job_status.job, @audit_job_status, previous).call
        @audit_job_status.job.save!
        format.json { render "api/v1/status_history/show", status: :ok }
      else
        format.json { render json: @audit_job_status.errors, status: :unprocessable_entity }
      end
    end
  end

  def set_audit_job_status
    if api_root?
      @audit_job_status = AuditJobStatus.find_by!({ id: params[:id] })
    else
      @audit_job_status = AuditJobStatus.find_by!({ id: params[:id], company: api_company })
    end
  end

  private

  def history_params
    params.require(:status_history).permit(:adjusted_dttm)
  end

end
