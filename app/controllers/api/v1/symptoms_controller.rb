# frozen_string_literal: true

class API::V1::SymptomsController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  def standard
    @symptoms = SymptomServices::FetchAllStandard.new.call

    render json: @symptoms, each_serializer: SymptomSerializer
  end

  def change
    @symptoms = SymptomServices::Change.new(
      api_company: api_company,
      standard_symptom_ids_to_add: standard_symptom_ids_to_add,
      symptom_ids_to_remove: symptom_ids_to_remove,
      custom_symptom_names_to_add: custom_symptom_names_to_add,
    ).call.symptoms_added

    render json: @symptoms, each_serializer: SymptomSerializer
  end

  def batch
    @symptoms = SymptomServices::Batch.new(
      api_company: api_company,
      symptom_ids: params[:symptom_ids]
    ).call

    render json: @symptoms, each_serializer: SymptomSerializer
  end

  private

  def standard_symptom_ids_to_add
    params[:symptom][:change].permit(add: [])[:add]
  end

  def symptom_ids_to_remove
    params[:symptom][:change].permit(remove: [])[:remove]
  end

  def custom_symptom_names_to_add
    custom_symptoms_names_param = params[:symptom][:change].permit(add_custom: [:name])[:add_custom]

    if custom_symptoms_names_param.present?
      custom_symptoms_names_param.map { |custom_symptom| custom_symptom.to_h.symbolize_keys }
    end
  end

end
