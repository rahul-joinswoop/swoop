# frozen_string_literal: true

class API::V1::ServiceAliasesController < API::APIController

  before_action :doorkeeper_authorize!

  def index
    @service_aliases = api_company.service_aliases

    respond_to do |format|
      format.json { render 'api/v1/service_aliases/index' }
    end
  end

end
