# frozen_string_literal: true

class API::V1::AuditJobStatusesController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  def create
    audit_job_status = API::Jobs::AddAuditJobStatus.call(
      input: {
        job_id: params[:job_id],
        audit_job_status_attributes: audit_job_status_attributes,
        user_id: api_user.id,
        company_id: api_company.id,
      }
    ).output

    if audit_job_status.valid?
      render json: audit_job_status
    else
      render json: audit_job_status.errors, status: :unprocessable_entity
    end
  end

  private

  def audit_job_status_attributes
    params.require(:audit_job_status)
      .permit(:job_status_name, :last_set_dttm)
  end

end
