# frozen_string_literal: true

class API::V1::UserLookupController < API::APIController

  REST_CLASS = User

  protect_from_forgery with: :null_session
  before_action :doorkeeper_authorize!
  before_action :user_api_authorized_admin

  def by_encoded_email_or_username
    @user = User.not_deleted.lookup_by_encoded_email_or_username(params[:key]) if params[:key]

    if @user
      render json: {  message: "found" }, status: :ok
    else
      render json: {  message: "not found" }, status: :not_found
    end
  end

end
