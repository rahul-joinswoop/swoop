# frozen_string_literal: true

class API::V1::UserSearchController < API::APIController

  REST_CLASS = "User"

  before_action :doorkeeper_authorize!

  def index
    params[:filters] ||= {}
    apply_filters

    service = UserSearchService.new({
      page: params[:page],
      per_page: params[:per_page],
    })

    @total, records = service.search(params)

    incls = [
      :job_status_email_preferences,
      :job_status_email_statuses,
      :sites,
      :company,
      :roles,
    ]

    # partner users have notification_settings, so we load them
    # in case it's a rescue user or a swoop user logged in,
    #  so they can be managed in the user form.
    if api_user.has_role?(:rescue) || api_user.company&.is_swoop?
      incls << :notification_settings
      @include_notification_settings = true
    end

    @users = records.order(created_at: :desc).includes(*incls)

    render "api/v1/user_search/index"
  end

  private

  def apply_filters
    params[:filters][:company_id] = api_company.id
  end

end
