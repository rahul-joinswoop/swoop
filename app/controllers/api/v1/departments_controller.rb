# frozen_string_literal: true

class API::V1::DepartmentsController < API::APIController

  REST_CLASS = Department

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  # Just make this simple for now since we are not updating
  def index
    render json: departments.all.order('sort_order ASC')
  end

  def show
    render json: departments.find(params[:id])
  end

  def batch
    @departments = DepartmentServices::Batch.new(
      api_company: api_company,
      department_ids: params[:department_ids]
    ).call

    render json: @departments, each_serializer: DepartmentSerializer
  end

  def create
    @department = DepartmentServices::Create.new(
      api_company: api_company,
      department_attributes: department_params
    ).call.department

    if @department.valid?
      render json: @department, serializer: DepartmentSerializer
    else
      render json: @department.errors, status: :unprocessable_entity
    end
  end

  def update
    @department = DepartmentServices::Update.new(
      api_company: api_company,
      department_attributes: department_params
    ).call.department

    if @department.valid?
      render json: @department, serializer: DepartmentSerializer
    else
      render json: @department.errors, status: :unprocessable_entity
    end
  end

  def destroy
    DepartmentServices::Delete.new(
      api_company: api_company,
      department_id: params[:id]
    ).call

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def change
    @departments = DepartmentServices::Change.new(
      api_company: api_company,
      departments_to_add: departments_to_add,
      department_ids_to_remove: department_ids_to_remove,
      department_to_rename: department_to_rename,
    ).call.departments_added

    render json: @departments, each_serializer: DepartmentSerializer
  end

  private

  def departments
    @departments ||= api_company.departments
  end

  def departments_to_add
    if params.dig(:department, :change, :add)
      params[:department][:change].require(:add)
    else
      []
    end
  end

  def department_ids_to_remove
    if params.dig(:department, :change, :remove)
      params[:department][:change].require(:remove)
    else
      []
    end
  end

  def department_to_rename
    if params.dig(:department, :change, :rename)
      params[:department][:change]
        .permit(rename: [:id, :name]).require(:rename).map { |department_rename| department_rename.to_h.symbolize_keys }
    else
      []
    end
  end

  def department_params
    params.require(:department).permit(:id, :name, :deleted_at)
  end

end
