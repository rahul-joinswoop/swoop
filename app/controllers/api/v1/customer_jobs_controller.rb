# frozen_string_literal: true

###### WARNING - this class has no authentication or authorization (uuid based)

class API::V1::CustomerJobsController < API::APIController

  before_action :set_job, only: [:show]

  REST_CLASS = Job
  # GET /api/v1/jobs/uuid/:uuid
  def show
    render template: "api/v1/jobs/customer/show"
  end

  def set_job
    @job = Job.find_by!({ uuid: params[:uuid] })
  end

end
