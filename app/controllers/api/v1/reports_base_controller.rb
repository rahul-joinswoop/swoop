# frozen_string_literal: true

class API::V1::ReportsBaseController < API::APIController

  include API::V1::Shared::ReportFiltersProcessor

  REST_CLASS = "Report"

  helper ReportHelper

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_objects_for_view

  def api_objects_for_view
    @api_company = api_company
    @api_user    = api_user
  end

  # available frontend filters:
  # partner, fleet, account, site, service, driver, vehicle, start_date, end_date, date
  # Only one start and end date is allowed.  Multiple “date”s are allowed and
  # will return results with keys “start_”+passed_key and “end_”+passed_key

  def index
    @company_reports = CompanyReport.where(company_id: api_company.id)
      .where("reports.deleted_at IS NULL")
      .joins(:report)

    render 'api/v1/reports/index'
  end

  def run_report
    @company_report = get_company_report

    r_params, collated = report_params

    Rails.logger.debug("REPORT(#{@company_report.report.id}) run_report r_params #{r_params}")
    Rails.logger.debug("REPORT(#{@company_report.report.id}) run_report collated #{collated}")

    @report_result = @company_report.run(api_user, r_params, nil, report_format, collated)
    render 'api/v1/report_results/show'
  end

  def get_company_report
    unless @company_report
      @company_report = CompanyReport.find_by(company_id: api_company.id, report_id: params[:id])
      if @company_report.company_id.blank?
        @company_report.company = api_company
        @company_report.readonly!
      end
    end

    @company_report
  end

  def report_params
    prms = run_params

    report        = get_company_report.report
    report_method = "process_params_for_#{report.name.gsub(/\s/, '').underscore}"

    logger.debug "REPORT: looking for method: #{report_method}"

    respond_to?(report_method) ? send(report_method, prms) : prms
  end

  def run_params
    whitelisted_params = {}
    allowed_report_params = {}

    if params[:report].present?
      allowed_report_params = params.require(:report).permit(
        @company_report.report.available_params.keys.map(&:to_sym)
      )
    end

    @company_report.report.available_params.each_key do |available_filter|
      next if allowed_report_params[available_filter.to_sym].blank?

      whitelisted_params[available_filter] = allowed_report_params[available_filter.to_sym]
    end

    whitelisted_params
  end

  def process_params_for_statement(prms)
    status = prms['status'].to_i
    key = case status
          when 1
            'unsent'
          when 2
            'outstanding'
          when 3
            'paid'
          end
    prms[key] = true unless key.nil?
    prms.delete 'status'

    prms.delete('invoice_payment_method') if prms['invoice_payment_method']&.to_i&.zero?

    Rails.logger.debug("REPORT(#{@company_report.report.id}) process_params_for_statement prms #{prms}")

    prms
  end

  def process_params_for_driver_report(prms)
    collated = false

    driver = prms['driver'].to_i

    prms.delete('driver') if driver < 1
    collated = true       if driver.zero?

    prms.delete('vehicle') if prms['vehicle']&.to_i&.zero?

    [prms, collated]
  end

  def process_params_for_jobs_report(prms)
    prms.delete('vehicle') if prms['vehicle']&.to_i&.zero?
    prms
  end

  def process_params_for_etaata_report(prms)
    prms.delete('vehicle') if prms['vehicle']&.to_i&.zero?
    prms
  end

  def process_params_for_truck_summary(prms)
    prms.delete('vehicle') if prms['vehicle']&.to_i&.zero?
    prms
  end

  def process_params_for_storage_report(prms)
    prms.delete 'type' if prms['type'].to_i.zero?
    [prms, false]
  end

  private

  def report_format
    unless @report_format
      rep_format = params[:report_format]
      @report_format = case rep_format
                       when 'csv', 'pdf', 'html'
                         rep_format
                       else
                         'csv'
                       end
    end
    @report_format
  end

end
