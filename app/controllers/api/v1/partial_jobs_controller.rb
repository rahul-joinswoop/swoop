# frozen_string_literal: true

class API::V1::PartialJobsController < API::APIController

  before_action :doorkeeper_authorize! # Authentication

  has_scope :filter_active, as: :active, type: :boolean, allow_blank: true do |controller, scope, active|
    active ? scope.filter_active : scope.filter_done
  end

  has_scope :filter_reviews, as: :reviews

  has_scope :filter_q1_scores, type: :array, allow_blank: true do |controller, scope, value|
    if value.present?
      with_no_review_score = value.include?('null')
      value.delete('null')

      if with_no_review_score
        scope.filter_q1_scores(value).or(scope.without_q1_scores)
      else
        scope.filter_q1_scores(value)
      end
    else
      scope.filter_q1_scores([])
    end
  end

  has_scope :filter_owner_companies, type: :array, allow_blank: true do |controller, scope, value|
    value.present? ? scope.filter_owner_companies(value) : scope.filter_owner_companies([])
  end

  has_scope :filter_storage_types, as: :filtered_storage_types, type: :array, allow_blank: true do |controller, scope, value|
    if value.present?
      with_no_storage_types = value.include?('null')
      value.delete('null')

      if with_no_storage_types
        scope.filter_storage_types(value).or(scope.where(storage_type_id: nil))
      else
        scope.filter_storage_types(value)
      end
    else
      scope.filter_storage_sites([])
    end
  end

  has_scope :filter_storage_sites, as: :filtered_storage_sites, type: :array, allow_blank: true do |controller, scope, value|
    value.present? ? scope.filter_storage_sites(value) : scope.filter_storage_sites([])
  end

  has_scope :filter_fleet_managed_jobs, as: :fleet_managed_jobs

  COMMON_FIELDS = [
    :id,
    :created_at,
    :department_id,
    :owner_company_id,
    :owner_company,
    :rescue_company_id,
    :rescue_company,
    :scheduled_for,
    :customer_id,
    :customer,
    :service_code_id,
    :service_location_id,
    :service_alias_id,
    :drop_location_id,
    :service_location,
    :drop_location,
    :toa,
    :status,
    :root_dispatcher_id,
    :fleet_dispatcher_id,
    :partner_dispatcher_id,
    :rescue_driver_id,
    :rescue_vehicle_id,
    :account_id,
    :odometer,
    :phone,
    :license,
    :color,
    :vehicle_type,
    :model,
    :make,
    :year,
    :po_number,
    :vin,
    :type,
    :last_status_changed_at,
    :customized_rescue_company_name,
    :ref_number,
    :unit_number,
    :fleet_manual,
    :review,
    :serial_number,
    :storage,
    :storage_type_id,
    :partner_live_invoices,
    :child_job,
    :will_store,
    :invoice_vehicle_category_id,
  ].freeze

  ORDER_MAP = {
    id: :id,
    last_status_changed_at: :last_status_changed_at,
    storage_date: "inventory_items.stored_at ",
    review_updated_at: "reviews.updated_at ",
  }.freeze

  DYNAMIC_INCLUDES = {
    storage: :storage,
    service_location: :service_location,
    drop_location: :drop_location,
    customer: :customer,
    owner_company: :fleet_company,
    rescue_company: :rescue_company,
    toa: [:time_of_arrivals_non_live, :latest_pta, :latest_eta, :original_toa, :latest_ata],
    # service: :service_code, not used in API
    vin: :stranded_vehicle,
    year: :stranded_vehicle,
    make: :stranded_vehicle,
    model: :stranded_vehicle,
    color: :stranded_vehicle,
    license: :stranded_vehicle,
    vehicle_type: :stranded_vehicle,
    child_job: :child_jobs,
    phone: :customer,
    review: :review,
  }.freeze

  # GET {{host}}/api/v1/fleet/partial_jobs?page=2&per_page=25&fields=id,status,owner_company_id
  # GET {{host}}/api/v1/root/jobs/search_new/?page=5&per_page=12&original=service%3A%22Battery+Jump%22&order=last_status_changed_at,desc&filters%5Bactive%5D=0&filters%5Bfleet_managed_jobs%5D=true&search_terms%5Bservice%5D=Battery+Jump
  def index
    Rails.logger.debug "PartialJobController::index receive params #{params}"

    conditions, page, per_page, order = get_conditional_args

    fields = parse_fields

    if params[:search_terms]
      raise ArgumentError, "Ordering is not supported on search: order=#{params[:order]}" if params[:order]

      service = JobSearchService.new({
        company: api_company,
        page: params[:page],
        per_page: params[:per_page],
      })

      jobs_total, jobs = service.search(search_params(params))

      search_job_ids = jobs.map(&:id)
      conditions[:id] = search_job_ids

      jobs, _ignore =
        get_jobs(conditions,
                 1,
                 per_page,
                 Arel.sql("array_position(array#{search_job_ids}::int[], jobs.id)"),
                 get_includes(fields))

    else
      jobs, jobs_total =
        get_jobs(conditions,
                 page,
                 per_page,
                 order,
                 get_includes(fields))

    end

    render json: jobs, each_serializer: JobSerializer,
           fields: fields,
           meta: { total: jobs_total },
           root: "jobs",
           adapter: :json
  end

  protected

  def get_jobs(conditions, page, per_page, order, includes)
    raise NotImplementedError
  end

  def get_all_fields
    raise NotImplementedError
  end

  # Subclasses can override to add filters onto a SearchService query as a returned
  # hash to be merged into the :filter params.
  def search_filter
    nil
  end

  private

  def search_params(params)
    params = params.to_unsafe_hash if params.respond_to?(:to_unsafe_hash)
    params = params.with_indifferent_access
    params.delete(:controller)
    params.delete(:action)
    filter = search_filter
    if filter
      filter.each do |name, value|
        if value.present?
          params[name] ||= HashWithIndifferentAccess.new
          params[name].merge!(value)
        end
      end
    end
    params
  end

  def parse_fields
    user_fields = parse_list(params[:fields])

    if !user_fields || (user_fields.length == 0)
      fields = get_all_fields
    else
      fields = user_fields & get_all_fields
      Rails.logger.debug "safe fields param: #{fields}"
      raise ArgumentError, "Unsupported field(s): #{user_fields - fields}, supported fields:#{get_all_fields}" unless fields.length == user_fields.length
    end
    fields
  end

  def get_conditional_args
    orders = []
    parse_order do |col, direction|
      raise ArgumentError, "unknown order: #{col}, supported orders are: #{ORDER_MAP.keys}" unless col && ORDER_MAP.key?(col.to_sym)
      order = ORDER_MAP[col.to_sym]
      if order.is_a? String
        orders << "#{order} #{direction}"
      else
        orders << { order => direction }
      end
    end
    if orders.length == 0
      orders << { id: :desc }
    end

    page = params[:page] || 1
    per_page = params[:per_page] || 25
    conditions = { deleted_at: nil }
    [conditions, page, per_page, orders]
  end

  def parse_list(arg)
    arg&.split(',')&.map(&:to_sym)
  end

  def get_includes(fields)
    includes = []
    fields.each do |field|
      if DYNAMIC_INCLUDES.key?(field)
        includes << DYNAMIC_INCLUDES[field]
      end
    end
    ret = includes.flatten.uniq
    Rails.logger.debug "Using includes: #{ret}"
    ret
  end

end
