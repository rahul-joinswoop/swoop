# frozen_string_literal: true

# Manage PoiSite models for all admin users independent of company type
class API::V1::PoiSitesController < API::APIController

  before_action :doorkeeper_authorize!
  before_action :api_authorized_admin, only: [:create, :update]

  def batch
    api_authorized_fleet_or_rescue

    set_batch_places

    render json: @places, each_serializer: PoiSiteSerializer
  end

  def create
    site_service_create = SiteService::Create.new(
      site_params: place_params,
      site_type: PoiSite,
      company: api_company
    )

    ApplicationRecord.transaction do
      @place = site_service_create.call.site
    end

    if site_service_create.duplicated_site?
      site_type = site_service_create.duplicated_site.type == 'PoiSite' ? 'Place' : 'Site'

      return render json: { name: ["A #{site_type} with this name already exists"] }, status: :unprocessable_entity
    end

    if @place.valid?
      render json: @place
    else
      render json: @place.errors, status: :unprocessable_entity
    end
  end

  def index
    @places = PoiSite.all_not_deleted_by_company(api_company)
    render json: @places, each_serializer: PoiSiteSerializer
  end

  def update
    site_service_update = SiteService::Update.new(
      site_id: params[:id],
      site_params: place_params,
      company: api_company
    )

    ApplicationRecord.transaction do
      @place = site_service_update.call.site
    end

    if site_service_update.duplicated_site?
      site_type = site_service_update.duplicated_site.type == 'PoiSite' ? 'Place' : 'Site'

      return render json: { name: ["A #{site_type} with this name already exists"] }, status: :unprocessable_entity
    end

    if @place.valid?
      render json: @place
    else
      render json: @place.errors, status: :unprocessable_entity
    end
  end

  private

  def set_batch_places
    if params[:place_ids]
      execute_batch_query
    else
      @places = []
    end
  end

  def execute_batch_query
    place_ids = params[:place_ids].split(',')

    @places = PoiSite.where(id: place_ids, company_id: api_company.id)
      .includes(:location)
      .order(created_at: :desc)
  end

  def place_params
    args = params.require(:poiSite).permit(
      :name,
      :location_type_id,
      :phone,
      :deleted_at,
      location: [
        :id, :lat, :lng, :address, :place_id, :exact, :location_type_id,
      ],
      manager: [:id, :full_name, :first_name, :last_name]
    )

    add_location_attributes(args) if args[:location]
    add_manager_attributes(args) if args[:manager]

    args
  end

  def add_location_attributes(args)
    args[:location_attributes] = args[:location]
    args[:location_attributes][:site_id] = @place.id if @place
    args.delete(:location)
  end

  def add_manager_attributes(args)
    args[:manager_attributes] = args[:manager]
    args.delete(:manager)
  end

end
