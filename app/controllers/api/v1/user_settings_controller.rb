# frozen_string_literal: true

class API::V1::UserSettingsController < API::APIController

  REST_CLASS = UserSetting

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_user # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  before_action :set_settings, only: [:index]
  before_action :set_setting, only: [:destroy, :show]

  def index
    render template: "api/v1/settings/index"
  end

  def show
    logger.debug "Rendering #{@setting}"
    render template: "api/v1/settings/show"
  end

  def create
    @setting = UserSettingsServices::Change.new(
      api_user: api_user,
      key: setting_params[:key],
      value: setting_params[:value]
    ).call.user_setting

    if @setting.valid?
      render 'api/v1/settings/show', status: :created
    else
      render json: @setting.errors, status: :unprocessable_entity
    end
  end

  def update
    @setting = UserSettingsServices::Change.new(
      api_user: api_user,
      key: setting_params[:key],
      value: setting_params[:value]
    ).call.user_setting

    if @setting.valid?
      render "api/v1/settings/show", status: :ok
    else
      render json: @setting.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @setting.destroy!
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def set_settings
    @settings = UserSetting.where({ user: api_user })
  end

  def set_setting
    @setting = UserSetting.find_by!({ id: params[:id], user: api_user })
  end

  def setting_params
    params.require(:userSetting).permit(:key, :value)
  end

end
