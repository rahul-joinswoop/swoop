# frozen_string_literal: true

class API::V1::PCC::PolicyController < API::APIController

  # Depricated, use search instead
  #
  # This action will only trigger the Policy Searching.
  # The response will not contain a Policy, it will only contain an async_request.
  #
  # The Policy Search results will actually be sent through WS afterwards
  def create
    ActiveSupport::Deprecation.warn('Use search instead')
    async_request = ::PCC::PCCService.policy_lookup(
      search_terms: { policy_number: create_params[:number] },
      job_id: create_params[:job_id],
      api_user: api_user
    )

    render json: async_request
  end

  # This action will only trigger the Policy Searching.
  # The response will not contain a Policy, it will only contain an async_request.
  #
  # The Policy Search results will actually be sent through WS afterwards
  def search
    async_request = ::PCC::PCCService.policy_lookup(
      search_terms: search_params[:search_terms],
      job_id: search_params[:job_id],
      api_user: api_user
    )

    render json: async_request
  end

  # This action will only trigger the Policy Details Fetching.
  # The response will not contain Policy Details, it will only contain an async_request.
  #
  # The Policy Details results will actually be sent through WS afterwards,
  # in case the fetch has been well succeeded.
  #
  # The WS socket message will be serialized by PCC::PolicyDriversAndVehiclesSerializer,
  # and will be something like:
  #
  #  {
  #    policy_id: <policy_id>,
  #    drivers_and_vehicles: [
  #      {
  #        driver_id:, vehicle_id:, driver_name:, vehicle_description:, vin:
  #      }
  #    ]
  #  }
  #
  def show
    async_request = ::PCC::PCCService.policy_details(
      policy_id: params[:id],
      api_user: api_user
    )

    render json: async_request
  end

  def show_coverage
    async_request = ::PCC::PCCService.coverage_details(
      policy_id: params[:policy_id],
      pcc_vehicle_id: params[:pcc_vehicle_id],
      pcc_driver_id: params[:pcc_driver_id],
      api_user: api_user
    )

    render json: async_request
  end

  def coverage
    async_request = ::PCC::PCCService.coverage(
      args: coverage_params,
      api_user: api_user
    )

    render json: async_request
  end

  private

  def create_params
    params.permit(:number, :job_id)
  end

  def search_params
    params.permit(:job_id,
                  search_terms: [
                    :last_name,
                    :policy_number,
                    :phone_number,
                    :postal_code,
                    :unit_number,
                    :vin,
                  ])
  end

  def coverage_params
    params.permit(
      :pcc_policy_id,
      :pcc_vehicle_id,
      :pcc_driver_id,
      job: [
        :id,
        service: [:name],
        vehicle: [:year, :odometer],
        location: [
          service_location: [:lat, :lng, :site_id],
          dropoff_location: [:lat, :lng, :site_id],
        ],
      ]
    )
  end

end
