# frozen_string_literal: true

# Most of this is depricated, except for the vehicles makes call as it's not in partner/vehicles or fleet/vehicles
# Remove pending change of Android

require 'edmunds'
class API::V1::VehiclesController < API::APIController

  before_action :doorkeeper_authorize! # Authentication
  before_action :api_authorized_fleet_or_rescue # Authorization

  # N.B. Don't move these up to base as they will get called before the Authorization above
  #
  before_action :set_vehicle, only: [:show, :update, :jobs, :logout]
  before_action :set_vehicles, only: [:index]

  def index
    logger.warn "Call to depricated API vehicles::index"
    render template: "api/v1/vehicles/index"
  end

  def show
    logger.warn "Call to depricated API vehicles::show"
    render template: "api/v1/vehicles/show"
  end

  def jobs
    logger.warn "Call to depricated API vehicles::jobs"
    @jobs = @vehicle.jobs
    render template: "api/v1/jobs/index"
  end

  # Edmunds update
  def extra_types
    @types = VehicleType.with_no_associated_vehicle_models
    render json: @types
  end

  def makes
    @makes = VehicleMakeService.new(api_company: api_company).call
  end

  def update
    logger.warn "Call to depricated API vehicles::update"
    p = vehicle_params

    # TODO need to eliminate this deprecated endpoint.
    p[:location_updated_at] = DateTime.now
    p[:long] = p[:lng]
    p.delete(:lng)

    @vehicle.driver = api_user

    respond_to do |format|
      if @vehicle.update(p)
        format.json { render "api/v1/vehicles/show", status: :ok, vehicle: @vehicle }
      else
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  def logout
    respond_to do |format|
      logger.debug "api_user vehicle: #{api_user.vehicle}"
      logger.debug "@vehicle: #{@vehicle}"
      if api_user.vehicle == @vehicle
        @vehicle.driver = nil

        if @vehicle.save
          logger.debug "api_user vehicle: #{api_user.vehicle}"
          format.json { render json: { status: 200, message: "logged out user from vehicle" }, status: :ok }
        else
          format.json { render json: @vehicle.errors + api_user.errors, status: :unprocessable_entity }
        end
      else
        format.json { render json: { message: "You are not allocated to that vehicle and therefore cannot logout of it" }, status: :unprocessable_entity }
      end
    end
  end

  private

  # only our fleet controller inherits this - partner and root override it so
  # maybe we should just move this to fleet controller?
  def set_vehicle
    if api_root?
      @vehicle = RescueVehicle.find_by!({ id: params[:id] })
    else
      @vehicle = RescueVehicle.find_by!({ id: params[:id], company: api_company })
    end
  end

  # this is overridden in the fleet, partner, and root vehicles controller and is
  # not used anywhere afaict - can we remove it?
  def set_vehicles
    page = params[:page] || 1
    per_page = params[:per_page] || 10000

    if api_root?
      @vehicles = RescueVehicle.all.order(created_at: :desc).paginate(page: page, per_page: per_page)
    else
      @vehicles = api_company.vehicles.order(created_at: :desc).paginate(page: page, per_page: per_page)
    end
  end

  def vehicle_params
    params.require(:vehicle).permit(:lat,
                                    :lng)
  end

end
