# frozen_string_literal: true

class API::V1::SitesController < API::APIController

  REST_CLASS = Company

  before_action :doorkeeper_authorize!, except: [:search] # Authentication

  def batch
    api_authorized_fleet_or_rescue

    set_batch_sites

    render template: "api/v1/sites/index"
  end

  def search
    head(:bad_request) && return if params[:phone].length != 12
    company = fetch_rescue_company

    if company.nil?
      render json: []
      return
    end

    send_existing_company_notice(company.id)
    render json: company.sites, each_serializer: PublicSiteSerializer
  end

  private

  def fetch_rescue_company
    RescueCompany.associated_with_phone(params[:phone])
  end

  def send_existing_company_notice(company_id)
    ExistingPartnerSignupAttemptWorker.perform_async(
      company_id
    )
  end

  def set_batch_sites
    if params[:site_ids]
      execute_batch_query
    else
      @sites = []
    end
  end

  def execute_batch_query
    site_ids = params[:site_ids].split(',')

    where_clause = { id: site_ids }

    # if partner or fleet we make sure they can only fetch their own sites
    if !api_root? && !api_swoop_dispatcher?
      where_clause[:company_id] = api_company.id
    end

    @sites = Site.where(where_clause)
      .where.not(type: 'PoiSite')
      .includes(:location)
      .order(created_at: :desc)
  end

end
