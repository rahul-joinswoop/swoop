# frozen_string_literal: true

class API::APIController < ActionController::Base

  include Utils
  include ControllerRequestContext

  serialization_scope :view_context

  #  skip_before_action  :verify_authenticity_token
  protect_from_forgery with: :null_session

  before_action :set_request_context
  before_action :init_logging
  before_action :no_cache
  before_action :set_api_company, if: :api_company?
  around_action :identity_cache_memoization

  def identity_cache_memoization
    IdentityCache.cache.with_memoization { yield }
  end

  before_action except: :location_update do |controller|
    if ['POST', 'PATCH'].include? request.method
      save_event
    end
  end

  after_action except: :location_update do |controller|
    if ['POST', 'PATCH'].include? request.method
      done_event
    end
  end

  def no_cache
    expires_now
  end

  def init_logging
    RequestStore.store[:request_id] = request.request_id

    if doorkeeper_token
      u = api_user?
      if u
        RequestStore.store[:user_id] = u.id
      else
        logger.warn "no user for this request request.original_url"
      end
    end
  end

  def user_api_authorized_admin
    logger.debug("_----------> #{api_user.id} == #{params[:id]} = #{api_user.id.to_s != params[:id].to_s} #{api_user.id.class} #{params[:id].class}")
    if api_user.id.to_s != params[:id].to_s
      api_authorized_admin
    end
  end

  rescue_from SecurityError do |exception|
    failed_event
    logger.warn(exception.inspect)
    logger.warn(exception.backtrace.join("\n"))
    render json: {
      "status": "403",
      "message": "You are authenticated, but unauthorized to access this resource",
    }.to_json, status: :forbidden
  end

  SUPPORT_MSG = "Please copy this message into the support chat (in the bottom right)"
  HOST_MATCHER = /( |^)([\w-]+\.){2,}[\w-]+:\d+\b/.freeze

  def err_msg(name, msg)
    "#{name}: \"#{sanitize_hosts(msg)}\" has occurred."
  end

  def sanitize_hosts(msg)
    if msg.blank?
      "an error"
    else
      # replace hostname and port in server error messages to users
      # EX: Error with redis.amazonservice.com:6699 => Error with remote_service
      msg.gsub(HOST_MATCHER, '\1remote service')
    end
  end

  rescue_from StandardError do |exception|
    logger.error "Caught StandardError #{exception.message}"
    logger.error(exception.inspect)
    logger.error(exception.backtrace.join("\n"))
    Rollbar.error(exception)
    failed_event
    msg = {
      status: "500",
      message: err_msg("ServerError", exception.message),
      "action": SUPPORT_MSG,
    }
    render json: msg.to_json, status: :internal_server_error
  end

  rescue_from APIException do |exception|
    logger.error "Caught API Exception  #{exception.reason} "
    Rollbar.error(exception)
    logger.debug(exception.inspect)
    logger.debug(exception.backtrace.join("\n"))
    msg = {
      status: "422",
      "message": err_msg("APIException", exception.reason),
      "action": SUPPORT_MSG,
    }
    render json: msg.to_json, status: :unprocessable_entity
  end

  rescue_from ArgumentError do |exception|
    logger.error "Caught ArgumentError  #{exception.message}"
    Rollbar.error(exception)
    logger.debug(exception.inspect)
    logger.debug(exception.backtrace.join("\n"))
    msg = {
      status: "422",
      "message": err_msg("ArgumentError", exception.message),
      "action": SUPPORT_MSG,
    }
    render json: msg.to_json, status: :unprocessable_entity
  end

  rescue_from AASM::InvalidTransition do |exception|
    msg = {
      status: "422",
      "message": err_msg("InvalidTransition", exception.message),
      "action": SUPPORT_MSG,
    }
    render json: msg.to_json, status: :unprocessable_entity
  end

  rescue_from ActiveRecord::RecordInvalid do |exception|
    logger.debug(exception.inspect)
    logger.debug(exception.backtrace.join("\n"))
    failed_event
    if Rails.env.production? && ['staging', 'beta'].exclude?(ENV['SWOOP_ENV'])
      msg = {
        status: "422",
        message: "Record Invalid",
      }
    else
      msg = {
        status: "422",
        message: err_msg("ActiveRecord::RecordInvalid", exception.message),
      }
    end
    render json: msg.to_json, status: 422
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    failed_event
    if Rails.env.production?
      msg = {
        status: "404",
        message: "Not Found",
      }
    else
      msg = {
        status: "404",
        message: exception.backtrace.join("\n"),
      }
    end
    render json: msg.to_json, status: 404
  end

  @@event_action_map = {
    "index" => "read all",
    "show" => "read",
    "create" => "create",
    "update" => "update",
    "delete" => "delete",
    "GET" => 'read',
    "POST" => 'create',
    "PATCH" => 'update',
    "DELETE" => 'delete',
  }

  def save_event
    # logger.debug("body #{request.body}")
    # logger.debug("raw #{request.raw_post}")
    # logger.debug("action: #{params[:action].inspect}")

    name = @@event_action_map[params[:action]]
    if !name
      name = @@event_action_map[request.method]
    end
    # logger.debug "action or method: #{name}"
    op = EventOperation.fetch_by_name(name)

    # logger.debug("mapped #{name} to #{op.inspect}")

    if defined? self.class::REST_CLASS
      target_clz = self.class::REST_CLASS
    else
      target_clz = request.original_url
    end

    @event = Command.new(event_operation: op,
                         user: api_user?,
                         company: api_company?,
                         json: request.raw_post,
                         target_id: params[:id],
                         target_type: target_clz,
                         request: request.uuid)

    @event.save!
  end

  def done_event
    @event.update({ applied: true })
    #    log_event = !@disable_event_logging
    #    if log_event
    #      #logger.debug "raw post : #{request.raw_post.class} #{request.raw_post}"
    #      segment_track("API #{@event.target_type} #{@event.event_operation.name.capitalize} ", self.get_segment_data)
    #    end
  end

  attr_reader :event

  def failed_event
    @event.update({ applied: false }) if @event
  end

  # TODO: Rename this. Should probably be called `api_company!` to indicate it might raise an error.
  def api_company
    company = SwoopAuthenticator.for(request).company
    raise SecurityError unless company
    company
  end

  # TODO: Rename this. This is really badly named. I expect this to return a boolean, not a Company
  def api_company?
    SwoopAuthenticator.for(request).company
  end

  # TODO: Rename this. Should probably be called `api_user!` to indicate it might raise an error.
  def api_user
    user = SwoopAuthenticator.for(request).user
    raise SecurityError unless user
    user
  end

  # TODO: Rename this. This is really badly named. I expect this to return a boolean, not a User
  # If we want to be consistent with Rails conventions, this method should be called `current_user`
  def api_user?
    SwoopAuthenticator.for(request).user
  end

  def api_root?
    SwoopAuthenticator.for(request).has_role?(:root)
  end

  def api_swoop_dispatcher?
    SwoopAuthenticator.for(request).has_role?(:swoop_dispatcher)
  end

  def api_authorized?(role)
    raise SecurityError unless SwoopAuthenticator.for(request).has_any_role?(role, :root)
  end

  def api_authorized_tesla
    raise SecurityError unless api_company.is_tesla?
  end

  def api_authorized_fleet
    api_authorized?(:fleet)
  end

  def api_authorized_fleet_in_house
    api_authorized?(:fleet)
    raise SecurityError unless api_company.fleet_in_house?
  end

  def api_authorized_fleet_managed
    api_authorized?(:fleet)
    raise SecurityError unless api_company.fleet_managed?
  end

  def api_authorized_partner
    api_authorized?(:rescue)
  end

  def api_authorized_admin
    api_authorized?(:admin)
  end

  def api_authorized_root
    api_authorized?(:root)
  end

  def api_authorized_root_or_swoop_dispatcher
    raise SecurityError unless SwoopAuthenticator.for(request).has_any_role?(:root, :swoop_dispatcher)
  end

  def api_authorized_fleet_or_swoop_dispatcher
    raise SecurityError unless SwoopAuthenticator.for(request).has_any_role?(:fleet, :root, :swoop_dispatcher)
  end

  def api_authorized_fleet_or_rescue
    raise SecurityError unless SwoopAuthenticator.for(request).has_any_role?(:fleet, :rescue, :root, :swoop_dispatcher)
  end

  def api_authorized_user
    raise SecurityError unless api_user
  end

  def get_segment_data
    dotted_hash = {}
    return dotted_hash if Rails.env.test?

    data = request.raw_post
    if data && (data.length > 2)
      hash = begin
               JSON.parse(request.raw_post)
             rescue JSON::ParserError
               { "raw_post" => request.raw_post }
             end
      dotted_hash = convert_hash(hash)
    end
    dotted_hash
  end

  def segment_track(event, props = nil)
    user = api_user?
    if user
      Analytics.track(
        user_id: api_user.to_ssid,
        event: event,
        properties: props
      )
    end
  end

  def parse_order
    if params[:order]
      order_list = params[:order].split(',')
      if (order_list.length == 0) || ((order_list.length % 2) != 0)
        raise ArgumentError, "Ordering parameters must be a list of pairs of columns and directions, e.g. order=<col1>,<direction1>,..."
      end
      order_list.each_slice(2) do |col, direction|
        if !["asc", "desc"].include? direction
          raise ArgumentError, "invalid direction: #{direction}"
        end

        logger.debug "Ordering params: #{col}:#{direction}"
        yield(col, direction)
      end
    end
  end

  def paginate(array)
    page = params[:page] || 1
    WillPaginate::Collection.create(page, WillPaginate.per_page, array.length) do |pager|
      pager.replace array[pager.offset, pager.per_page].to_a
    end
  end

  def set_time_zone
    user = doorkeeper_token && api_user
    time_zone = user&.timezone || 'America/Los_Angeles' # TEMP: should set config.time_zone= in application.rb
    Time.use_zone(time_zone) { yield }
  end

  def set_api_company
    @api_company = api_company
  end

  protected

  def set_request_context
    context = RequestContext.current
    context.source_application_id = SourceApplication.from_request(request)&.id
    context.user_id = SwoopAuthenticator.for(request).user&.id
  end

end
