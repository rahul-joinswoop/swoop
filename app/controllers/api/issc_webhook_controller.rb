# frozen_string_literal: true

require 'issc'
require 'active_support/core_ext/hash/indifferent_access'

class ConnectEvent < ISSC::Event

  define_event 'connect'
  parameters :Result, :Description

  def dispatch
    super

    Issc.transaction do
      IsscConnected.new(@data).call
    end
    IsscFixUnregistered.perform_async
    IsscFixLogouts.perform_async
  end

end

class DisconnectEvent < ISSC::Event

  define_event 'disconnect'
  parameters :Result, :Description

  def dispatch
    super
    Issc.transaction do
      IsscDisconnected.new(@data, autoreconnect: true).call
    end
  end

end

class HeartbeatEvent < ISSC::Event

  define_event 'heartbeat'
  parameters :SequenceNumber, :UTC

  def dispatch
    super

    # PDW use our own time instead of the time in the message (it's drifting and currently >2 mins out)
    # @issccon.update_heartbeat @data[:SequenceNumber], @data[:UTC]
    @issccon.update_heartbeat @data[:SequenceNumber], Time.now.utc
  end

end

class RegisterEvent < ISSC::Event

  define_event 'register'
  parameters :Result, :LocationID, :ClientID, :ContractorID, :Token, :Description

  def dispatch
    super
    if @data[:Result] == 'Success' || ((@data[:Result] == 'Failure') && (@data[:Description] == 'Already registered.'))
      # NB - PG - This is calling the service directly not doing it in a batch
      issc = nil
      Issc.transaction do
        issc = IsscProviderRegistered.new(@data).call
        issc.save!
      end
      IsscFixLogouts.perform_async
    elsif @data[:Result] == 'Failure'
      Issc.transaction do
        issc = IsscProviderRegisterFailed.new(@data).call
        issc.save!
      end
    end
  end

end

class DeregisterEvent < ISSC::Event

  define_event 'deregister'
  parameters :Result, :ClientID, :ContractorID, :LocationID, :Description

  def dispatch
    super
    if @data[:Result] == 'Success'
      # NB - PG - This is calling the service directly not doing it in a batch
      issc = IsscProviderDeregistered.new(@data).call
      issc.save!
    end
    IsscFixUnregistered.perform_async
  end

end

class PreregistrationsEvent < ISSC::Event

  define_event 'preregistrations'
  parameters :Result, :ClientID, :ContractorID, :TaxID, :preregistrations, :Description

  def dispatch
    super

    if @data[:Result] == 'Success' && @data[:preregistrations].present?
      Issc.transaction do
        # Should this be moved into a worker?
        IsscProviderPreregistered.new(@data).call
      end
    else
      Rails.logger.warn "Preregistration Error: #{@data}"
    end
    IsscFixUnregistered.perform_async
  end

end

class LoginEvent < ISSC::Event

  define_event 'login'
  parameters :Result, :ClientID, :ContractorID, :LocationID, :Description

  def dispatch
    super
    Issc.transaction do
      issc = IsscProviderLoggedIn.new(@data).call
      issc.save!
    end
    # IsscFixLogins.perform_async
    # IsscFixLogouts.perform_async
  end

end

class LogoutEvent < ISSC::Event

  define_event 'logout'
  parameters :Result, :ClientID, :ContractorID, :LocationID, :Description

  def dispatch
    super
    if (@data[:Result] == 'Success') || (@data[:Description] == "Not logged in.")
      # NB - PG - This is calling the service directly not doing it in a batch
      issc = IsscProviderLoggedOut.new(@data).call
      issc.save!

      # IsscFixLogins.perform_async
      # IsscFixLogouts.perform_async
      IsscFixDeleted.perform_async
    end
  end

end

class DispatchNewEvent < ISSC::Event

  define_event 'dispatch.new'
  parameters :Result, :ClientID, :ContractorID, :DispatchID, :ResponseID, :AccountInfo, :Job, :Vehicle, :Incident, :Coverage, :Destination, :LocationID

  def dispatch
    super
    job = IsscDispatchNew.new(@data).call

    ISSC::AutoResponseTimer.perform_at(job.acknowledge_by, job.id) if ENV['ISSC_AUTORESPONSE'] == "true"
  end

end

class DispatchTranscriptEvent < ISSC::Event

  define_event 'dispatch.transcript'
  parameters :Result, :Transcript, :Coverage, :CustomerContactMethod, :MemberCallBackNumber, :AuthorizationNumber, :ClientID, :ContractorID, :DispatchID, :ResponseID, :AccountInfo, :Job, :Vehicle, :Incident, :Coverage, :Destination, :Transcript, :LocationID

  def dispatch
    super
    job = IsscDispatchTranscript.new(@data).call
    job.save!
  end

end

class DispatchAcceptedEvent < ISSC::Event

  define_event 'dispatch.accepted'
  parameters :Result, :ClientID, :ContractorID, :DispatchID, :ResponseID, :JobID, :AuthorizationNumber, :Coverage, :MemberCallBackNumber, :CustomerContactMethod

  def dispatch
    super
    job = IsscDispatchAccepted.new(@data).call
    job.save!
  end

end

class DispatchRejectedEvent < ISSC::Event

  define_event 'dispatch.rejected'
  parameters :Result, :ClientID, :ContractorID, :DispatchID, :ResponseID, :JobID, :Remarks

  def dispatch
    super
    job = IsscDispatchRejected.new(@data).call
    job.save!
  end

end

class DispatchCanceledEvent < ISSC::Event

  define_event 'dispatch.canceled'
  parameters :Result, :ClientID, :ContractorID, :DispatchID, :ResponseID, :JobID, :Reason

  def dispatch
    super
    job = IsscDispatchCanceled.new(@data).call
    if job.save
      job.issc_dispatch_status(FleetMotorClubJob::ISSC_MOTORCLUBCANCELRECEIVED, Time.now)
    end
  end

end

class DispatchExpiredEvent < ISSC::Event

  define_event 'dispatch.expired'
  parameters :Result, :ClientID, :ContractorID, :DispatchID, :ResponseID, :JobID

  def dispatch
    super
    job = IsscDispatchExpired.new(@data).call
    job.save!
  end

end

class DispatchErrorEvent < ISSC::Event

  define_event 'dispatch.error'
  parameters :Result, :ClientID, :ContractorID, :DispatchID, :ResponseID, :JobID

  def dispatch
    super
    Rails.logger.warn "DispatchErrorEvent: #{@data}"
  end

end

class ServiceRequestorUp < ISSC::Event

  define_event 'servicerequester.up'
  parameters :Name, :ClientID, :Status, :UpSince, :UpComment

  def dispatch
    @data[:Status] = "up"
    IsscServiceRequestorStatus.new(@data).call
  end

end

class ServiceRequestorDown < ISSC::Event

  define_event 'servicerequester.down'
  parameters :Name, :ClientID, :Status, :DownSince, :DownComment

  def dispatch
    @data[:Status] = "down"
    IsscServiceRequestorStatus.new(@data).call
  end

end

class API::IsscWebhookController < API::APIController

  if ENV['ISSC_SWOOP_USERNAME']
    http_basic_authenticate_with name: ENV['ISSC_SWOOP_USERNAME'], password: ENV['ISSC_SWOOP_PASSWORD'], only: :create
  end
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  def track_error(job, e, audit_issc, http_response_code, http_response)
    logger.error "ISSCLOG(#{job.try(:id)}) ERROR #{e.message}, HTTP_RESPONSE: #{http_response_code}"
    logger.debug e.backtrace.join("\n")
    Rollbar.error(e)
    if audit_issc
      audit_issc.update_columns(http_status_code: http_response_code, http_response: http_response)
    end
  end

  def create
    @disable_event_logging = true
    audit_issc = nil
    job = nil
    begin
      # KIP posts us an array so params can't suss it
      events = JSON.parse request.body.read
      Rails.logger.debug "ISSCLOG Event1 #{events}"
      if events.is_a?(Hash)
        events = events['body.multi']
      end

      Rails.logger.debug "ISSCLOG Event2 #{events}"
      # Do this dead simple for now
      events.each do |raw_event|
        job = nil
        clientid = raw_event["ClientID"]
        contractorid = raw_event["ContractorID"]
        locationid = raw_event["LocationID"]
        dispatchid = raw_event["DispatchID"]

        audit_issc = AuditIssc.create!(path: '/issc/event', data: JSON.pretty_generate(raw_event),
                                       clientid: clientid, contractorid: contractorid, locationid: locationid,
                                       incoming: true, dispatchid: dispatchid)

        # rubocop:disable Rails/SaveBang
        event = ISSC::Event.create(raw_event.with_indifferent_access)
        # rubocop:enable Rails/SaveBang

        audit_issc.update!(data: JSON.pretty_generate(event.data))
        event.dispatch

        if dispatchid
          job = IsscDispatchRequest.find_by(dispatchid: dispatchid).try(:job)
        end

        if contractorid && clientid
          company = Issc.where(system: nil, clientid: clientid, contractorid: contractorid).first.try(:company)
        end

        audit_issc.update!(job_id: job.try(:id), company_id: company.try(:id))
      end
    rescue ISSC::UnsupportedServiceRequestor => e
      response = { success: 0, message: "Unsupported ServiceRequestor" }
      track_error(job, e, audit_issc, 422, JSON.pretty_generate(response))
      # render json: response, :status => :unprocessable_entity
    rescue ISSC::Unroutable => e
      response = { success: 0, message: "Unable to route request to service provider: unknown combination ClientID(#{e.client_id}), LocationID(#{e.location_id}), ContractorID(#{e.contractor_id})" }
      track_error(job, e, audit_issc, 422, JSON.pretty_generate(response))
      # render json: response, :status => :unprocessable_entity
    rescue StandardError => e
      response = { success: 0, message: e.message }
      track_error(job, e, audit_issc, 422, JSON.pretty_generate(response))
      # render json:response, :status => :unprocessable_entity
    end
    render json: { success: 1 }
  end

  protected

  def set_request_context
    context = RequestContext.current
    context.source_application_id = SourceApplication.issc_application.id
  end

end
