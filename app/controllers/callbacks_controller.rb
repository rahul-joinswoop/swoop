# frozen_string_literal: true

class CallbacksController < ApplicationController

  skip_before_action :verify_authenticity_token, only: [:twilio_status, :review_callback]
  before_action :set_consumer_mobile_web_request_context, only: [:review_callback]

  # SMS click tracking via the AuditSMS table
  #
  # This just updates the current AuditSms object when user clicks on a link contained in their SMS message.
  #
  def txt
    logger.debug "AuditSMSBROWSER: #{browser.platform} #{request.user_agent} #{request.original_url} "
    audit_sms = AuditSms.find_by!(uuid: params[:uuid])
    if !audit_sms.clicked_dttm
      audit_sms.clicked_dttm = Time.current
      audit_sms.save!

      case audit_sms
      when EtaAuditSms
        audit_sms.job.slack_notification("Track ETA SMS Clicked", tags: [:sms, :track, :clicked])
      when Sms::ConfirmLocation, LocationAuditSms
        audit_sms.job.slack_notification("Confirm Location SMS Clicked", tags: [:sms, :location, :clicked])
        audit_sms.job.track(Metric::VIEWED, "location_page_view")
      end
    end

    redirect_to audit_sms.redirect_url
  end

  def review_callback
    resp = params[:form_response]

    return head :not_acceptable if resp.blank?
    logger.debug "Typeform Response: #{JSON.fast_generate(params.permit(:form_response).to_unsafe_hash)}"

    if resp['hidden'].blank?
      logger.debug "Typeform missing hidden params!!!!!!!"
      return head :precondition_failed
    end

    return head :bad_request if resp['hidden']['job'].blank?

    job_id = resp['hidden']['job'].to_i # Santatize
    review = Review.where(job_id: job_id).includes(:job).last

    return head :not_found unless review
    return head :unprocessable_entity unless review.is_a?(Reviews::SurveyReview)

    sr = SurveyResponse.new(review, resp)
    sr.call.save_mutated_objects_throw_errors

    review.job.track(Metric::VIEWED, "review_page_submited")

    head :ok
  end

  def review_redirect
    job = Job.preload(:review).find_by(uuid: params[:uuid])
    return head :not_found unless job && job.review.present? && job.review.respond_to?(:redirect_url)

    job.track(Metric::VIEWED, "review_link_clicked")

    redirect_to job.review.redirect_url
  end

  #
  # This is automatically called by Twilio and update the status of the SMS
  # message that was sent to the customer.
  #
  def twilio_status
    audit = AuditSms.find(params[:audit_id]) # This will force 404
    status = params[:MessageStatus]

    if status.present?
      audit.update_attributes!(
        status: status,
        status_update_at: Time.now,
        error_code: params[:ErrorCode],
        error_message: params[:ErrorMessage],
      )

      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

  def set_consumer_mobile_web_request_context
    context = RequestContext.current
    context.source_application_id = SourceApplication.consumer_mobile_web.id
  end

end
