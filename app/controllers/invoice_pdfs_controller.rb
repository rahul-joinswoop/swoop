# frozen_string_literal: true

class InvoicePdfsController < ApplicationController

  def show
    @invoice = Invoice.find_by!(uuid: params[:invoice_uuid])
    @attempts = params[:attempts].to_i
    @attempts += 1

    if params[:id]
      @invoice_pdf = InvoicePdf.find(params[:id])

      if params[:poll]

        # uuid/timestamp?poll=true not ready
        if @invoice_pdf.public_url.blank?
          render :show, layout: false

        # uuid/timestamp?poll=true ready
        else
          redirect_to @invoice_pdf.url_for(:read, response_content_disposition: 'inline').to_s
        end

      # uuid/id not polling, direct link
      else
        redirect_to @invoice_pdf.url_for(:read, response_content_disposition: 'inline').to_s
      end
    else
      @invoice_pdf = @invoice.generate_pdf
      render :show, layout: false
      # render text: "still working, reply with timestamp: #{invoice_pdf.id}"
    end
  end

  ##
  # The following action at '/pdf' is used for generating a test pdf in development
  # you can request a specific PDF using '/pdf?id=num'
  def pdf
    pdf_id = params[:id].to_i
    respond_to do |format|
      invoice = pdf_id > 0 ? Invoice.find_by(id: pdf_id) : Invoice.first
      sender_name = nil
      if invoice.sender && invoice.sender.name?
        sender_name = invoice.sender.name
      end
      locals = { invoice: invoice, job: invoice.job, sender_name: sender_name, scale_factor: 4.5 }
      format.pdf do
        render pdf: "file_name",
               locals: locals,
               template: 'pdfs/invoice.pdf.erb',
               footer: {
                 font_name: 'Open Sans',
                 font_size: 8,
                 left: 'Powered by Swoop',
                 right: '([page] of [topage])',
               }
      end

      format.html do
        render "pdfs/invoice.pdf", layout: nil, locals: locals
      end
    end
  end

end
