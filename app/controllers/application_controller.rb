# frozen_string_literal: true

require "application_responder"

class ApplicationController < ActionController::Base

  serialization_scope :view_context
  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # In Docker, the node process and rails process are separate and in seprarte hosts.
  # Becasue of this, we get errors when making requests from the node process posting to the Rails API.
  # Need to eventually fix this but this is a work around for docker dev.
  if ENV['DOCKER_RUN'] == true && Rails.env.development?
    protect_from_forgery with: :null_session
  else
    protect_from_forgery with: :exception
  end

  rescue_from SecurityError do |exception|
    redirect_to root_url
  end

  def current_user
    SwoopAuthenticator.for(request).user
  end

  def current_company
    SwoopAuthenticator.for(request).company
  end

end
