# frozen_string_literal: true

# Expose REST APIs to Twilio endpoint.
class TwilioRepliesController < ApplicationController

  include ControllerRequestContext

  before_action :set_twilio_reply, only: [:show, :edit, :update, :destroy]
  before_action :set_request_context

  skip_before_action :verify_authenticity_token

  # protect_from_forgery with: :null_session, :if => Proc.new { |c| c.request.format == 'application/json' }

  # GET /twilio_replies
  # GET /twilio_replies.json
  def index
    Rails.logger.error "twilio_replies index called"
    # @twilio_replies = TwilioReply.all
  end

  # GET /twilio_replies/1
  # GET /twilio_replies/1.json
  def show
  end

  # GET /twilio_replies/new
  def new
    @twilio_reply = TwilioReply.new
  end

  # GET /twilio_replies/1/edit
  def edit
  end

  #
  # This is the route used by Twilio end to route SMS replies from Customers.
  # e.g. when customer confirm Partner On Site.
  #
  # POST /twilio_replies.json
  #
  # Parameters: {"ToCountry"=>"US", "ToState"=>"AL", "SmsMessageSid"=>"SM88391eac4cacaee1d517f6b7cd1c5620", "NumMedia"=>"0", "ToCity"=>"GADSDEN", "FromZip"=>"", "SmsSid"=>"SM88391eac4cacaee1d517f6b7cd1c5620", "FromState"=>"CA", "SmsStatus"=>"received", "FromCity"=>"", "Body"=>"Sup", "FromCountry"=>"US", "To"=>"+12563990952", "ToZip"=>"35904", "MessageSid"=>"SM88391eac4cacaee1d517f6b7cd1c5620", "AccountSid"=>"AC6e404418519f46ded0fd769a1171cdcf", "From"=>"+14156039663", "ApiVersion"=>"2010-04-01"}
  def create
    t = twilio_reply_params

    user = User.where(phone: t[:From]).order(created_at: :desc).first

    pmap = {
      sms_message_sid: t[:SmsMessageSid],
      sms_sid: t[:SmsSid],
      from_state: t[:FromState],
      from_country: t[:FromCountry],
      sms_status: t[:SmsStatus],
      body: t[:Body],
      to: t[:To],
      message_sid: t[:MessageSid],
      account_sid: t[:AccountSid],
      from: t[:From],
      service: params[:service],
      user: user,
    }

    @twilio_reply = TwilioReply.new(pmap)

    respond_to do |format|
      if @twilio_reply.save
        @twilio_reply.process
        @twilio_reply.save!

        format.html { redirect_to @twilio_reply, notice: 'Twilio reply was successfully created.' }
        format.json { render body: nil, status: 200, content_type: 'text/html' }
      else
        format.html { render :new }
        format.json { render json: @twilio_reply.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /twilio_replies/1
  # PATCH/PUT /twilio_replies/1.json
  #
  # TODO Is this ever used by Twilio? Or us?
  #
  def update
    respond_to do |format|
      if @twilio_reply.update(twilio_reply_params)
        format.html { redirect_to @twilio_reply, notice: 'Twilio reply was successfully updated.' }
        format.json { render :show, status: :ok, location: @twilio_reply }
      else
        format.html { render :edit }
        format.json { render json: @twilio_reply.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /twilio_replies/1
  # DELETE /twilio_replies/1.json
  #
  # TODO Is this ever used by Twilio? Or us?
  #
  def destroy
    @twilio_reply.destroy!
    respond_to do |format|
      format.html { redirect_to twilio_replies_url, notice: 'Twilio reply was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  protected

  def set_request_context
    context = RequestContext.current
    context.source_application_id = SourceApplication.sms_application.id
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_twilio_reply
    @twilio_reply = TwilioReply.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def twilio_reply_params
    # params.permit(:sms_message_sid, :sms_sid, :from_state, :from_country, :sms_status, :body, :to, :message_sid, :account_sid, :from)
    params.permit(:SmsMessageSid, :SmsSid, :FromState, :SmsStatus, :Body, :FromCountry, :To, :MessageSid, :AccountSid, :From)
  end

end
