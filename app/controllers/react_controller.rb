# frozen_string_literal: true

class ReactController < ActionController::Base

  include ControllerRequestContext

  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from SecurityError do |exception|
    redirect_to root_url
  end

  def application
    react 'application'
  end

  # the rest of these endpoints are only enabled in development

  def dev_survey
    @company = Company.first
    gon.token = "token"

    gon.survey_questions = []
    gon.dispatcher_name = "Quinn Baetz"
    gon.job_id = 1

    Survey::Smiley::NPS_QUESTIONS.each_with_index do |question, index|
      gon.survey_questions << {
        id: index + 1,
        type: "Survey::#{question[0]}",
        question: question[1],
      }
    end

    set_assets_for 'survey'

    render "api/v1/review/whitelabel/swoop_gamified.html.erb", layout: false
  end

  def dev
    react 'dev'
  end

  protected

  def react(bundle)
    set_assets_for bundle
    render html: '', layout: "react_application"
  end

  def set_assets_for(bundle)
    @version = Swoop::VERSION
    @js = WebpackAssets.instance.js(bundle)
    @css = WebpackAssets.instance.css(bundle)
  end

end
