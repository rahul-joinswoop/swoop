# frozen_string_literal: true

class PartnerAPIEmailVerificationsController < ApplicationController

  protect_from_forgery with: :null_session

  def show
    @email_verification = PartnerAPIEmailVerification.find_by params.permit(:uuid)

    if @email_verification&.valid_request?
      # first invalidate our @email_verification since we're using it now
      @email_verification.invalidate!
      # then set an access token in our session - this is only usable for our
      # oauth pages, it doesn't actually log the user in.
      session[:access_token] = Doorkeeper::AccessToken.create!(
        resource_owner_id: @email_verification.user_id,
        expires_in: 1.hours.to_i,
      ).token

      # setup this stuff so we can send off emails after successful authorization
      session[:partner_api_email_verification] = true
      session[:application_uid] = @email_verification.client_id
      session[:user_id] = @email_verification.user_id

      # then redirect to the oauth page with the correct client_id / redirect_uri
      redirect_to oauth_authorization_url(
        client_id: @email_verification.client_id,
        redirect_uri: @email_verification.redirect_uri,
        scope: "company",
        response_type: "code"
      )
    else
      # TODO - need a client side route to render an error page for this, ie
      # /auth/email_verifications/:uuid. we only get here if the above failed.
      @version = Swoop::VERSION
      @js = WebpackAssets.instance.js('application')
      @css = WebpackAssets.instance.css('application')

      gon.app = {
        name: @email_verification&.application&.name,
      }

      render html: '', layout: "react_application"
    end
  end

  def create
    @service_context = Authentication::VerifyEmailForPartnerAPI.call params.permit(:email, :client_id, :redirect_uri)

    if @service_context.success?
      render json: {}, status: :created
    else
      render json: { errors: [@service_context.error] }, status: :unprocessable_entity
    end
  end

end
