# frozen_string_literal: true

# These endpoints represent the consumer mobile web experience.
class ConsumerMobileWebController < ReactController

  before_action :set_job
  before_action :set_request_context

  def get_location
    @job.update!(get_location_clicked: true)
    react 'get_location'
  end

  def show_eta
    react 'show_eta'
  end

  # test at http://farmerssurvey1.com:5000/s/6a96685884
  def survey
    @job.track(Metric::VIEWED, "review_link_clicked")
    @company = @job.fleet_company
    @review_url = "#{@company.review_url || ENV['SURVEY_REVIEW_URL']}#{@job.review.iframe_params}"
    template = @company.survey_template || ENV['SURVEY_TEMPLATE']
    if @job.review.survey
      setup_survey_gon(@job)
    end

    set_assets_for 'survey'

    render template, layout: false
  end

  protected

  def set_request_context
    context = RequestContext.current
    context.source_application_id = SourceApplication.consumer_mobile_web.id
    context.user_id = @job&.driver&.user_id
  end

  private

  def set_job
    @job = Job.find_by!(uuid: params[:uuid])
  end

  def setup_survey_gon(job)
    if !job.review.uuid
      job.review.uuid = SecureRandom.uuid
      job.review.save!
    end
    gon.token = job.review.uuid
    gon.job_id = job.id

    gon.survey_questions = []
    gon.customer_id = job.customer.id

    # Eventually we will want to make work for jobs besides fleet managed
    # At that point update this to be the correct dispatcher ENG-8617
    if job.root_dispatcher
      gon.dispatcher_name = job.root_dispatcher.name
    end

    job.review.survey.questions.in_order.each do |question|
      gon.survey_questions << {
        id: question.id,
        type: question.type,
        question: question.question,
      }
    end
  end

end
