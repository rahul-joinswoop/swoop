# frozen_string_literal: true

class WelcomeController < ApplicationController

  skip_before_action :verify_authenticity_token, only: [:render_404]

  def time
    time = DateTime.now.utc
    logger.debug "sending time of #{time}"
    render json: time, content_type: 'application/json'
  end

  def render_404
    respond_to do |format|
      format.html { render file: "#{Rails.root}/public/404", layout: false, status: :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  APPLE_APP_SITE_ASSOCIATION = <<~JSON
    {
      "webcredentials": {
        "apps": [ "2593825Y9B.com.swoop.mobile" ]
      }
    }
  JSON

  def apple_app_site_association
    # we do this in a controller because otherwise there's no easy way to force the content-type to json :/
    render json: APPLE_APP_SITE_ASSOCIATION, content_type: "application/json"
  end

  ANDROID_APP_SITE_ASSOCIATION = <<~JSON
    [{
      "relation": ["delegate_permission/common.get_login_creds"],
      "target": {
        "namespace": "web",
        "site": "https://app.joinswoop.com"
      }
     },
     {
      "relation": ["delegate_permission/common.get_login_creds"],
      "target": {
        "namespace": "android_app",
        "package_name": "com.swoopmobile",
        "sha256_cert_fingerprints": [ "#{ENV['ANDROID_SHA256_CERT_FINGERPRINTS']}" ]
      }
     }]
  JSON

  def assetlinks
    render json: ANDROID_APP_SITE_ASSOCIATION, content_type: "application/json"
  end

end
