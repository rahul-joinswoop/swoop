# frozen_string_literal: true

class PartnerAPIPasswordRequestsController < ApplicationController

  protect_from_forgery with: :null_session

  def show
    uuid = params.permit(:uuid).dig(:uuid)
    @password_request = PartnerAPIPasswordRequest.find_by uuid: uuid

    if PartnerAPIPasswordRequest.valid_request_for?(uuid)
      # we have a valid password request, handle it
      status = 200
      gon.uuid = uuid
      gon.user = { email: @password_request.user_email }
    elsif @password_request.present?
      # we have an expired or already used password request, allow the user to
      # resend an email about it
      status = 404
      gon.uuid = uuid
      gon.user = { email: @password_request.user_email }
      gon.application = { uid: @password_request.application.uid }
      gon.error = 'expired request'
    else
      # the uuid is bogus, there's nothing we can do for them since we don't know which email address + client id they
      # were trying to use, redirect to the regular forgotPassword flow
      return redirect_to '/forgotPassword'
    end

    @version = Swoop::VERSION
    @js = WebpackAssets.instance.js('application')
    @css = WebpackAssets.instance.css('application')

    render html: '', layout: "react_application", status: status
  end

  def create
    begin
      user = User.find_by!(params.require(:user).permit(:email))
      client = Doorkeeper::Application.find_by!(params.require(:application).permit(:uid))
      service_context = Authentication::ResetPasswordForPartnerAPI.call user: user, client: client
      if !service_context.success?
        Rails.logger.error "#{self.class.name}: Authentication::ResetPasswordForPartnerAPI failed for params=#{params.to_unsafe_hash}: #{service_context.error}"
      end
    rescue StandardError => e
      Rails.logger.error "#{self.class.name}: failed to create password request for params=#{params.to_unsafe_hash}: #{e.message}"
    end

    # TODO - in users controller we return 201 if we created a new request but 200 otherwise - this means we can
    # use this endpoint to figure out which email addresses exist and which ones don't for now to be safe i'm always r
    # eturning created
    render json: {}, status: :created
  end

  def update
    if PartnerAPIPasswordRequest.valid_request_for?(params.permit(:uuid).dig(:uuid))
      begin
        @password_request = PartnerAPIPasswordRequest.find_by! params.permit(:uuid)
        # ugly but no other way to do it correctly, see the end of
        # https://edgeapi.rubyonrails.org/classes/ActionController/Parameters.html#method-i-require
        user_params = params.require(:user).permit(:password).tap do |up|
          up.require(:password) # SAFER
        end
        @password_request.user.update! user_params
        @password_request.invalidate!
        render json: {}, status: :ok
      rescue StandardError => e
        render json: { errors: [e.message] }, status: :unprocessable_entity
      end
    else
      render json: {}, status: :not_found
    end
  end

end
