# frozen_string_literal: true

class AgeroWebhookController < ActionController::Base

  protect_from_forgery

  def auth_code
    access_token = Agero::ApiRequest::AccessToken.get(
      params['authorizationCode']
    )

    # TODO: Remove this
    AgeroRSALog.create! do |log|
      log.request_type = 'GetAccessToken'
      log.path = Agero::ApiRequest::AccessToken::PATH
      log.incoming_request = false
      log.request_data = params.to_json
      log.response_data = access_token
      log.http_response_code = 200
    end

    render json: { success: true }
  end

end
