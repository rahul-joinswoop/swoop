# frozen_string_literal: true

#
#   Swoop GraphQL endpoint uses a client_credentials OAuth flow for authentication.
#
#   It requires an application to be created like this:
#
#   Doorkeeper::Application.create(
#    name:"Farmers Client App 1",
#    redirect_uri:ENV['SITE_URL'],
#    scopes:'fleet',
#    owner:<Farmers Company>
#   )
#
#   Which creates an entry in the oauth_applications table containing the uid and secret.
#
#   Obtain an access token like this:
#
#   POST /oauth/token
#   {
#     "grant_type":"client_credentials",
#     "client_id":"<uid>",
#     "client_secret":"<secret>",
#     "scope":"fleet"
#   }
#
#   returns:
#   {
#     "access_token": "426ddc9bc742e980d5c615fc3f895524dd382c4435e19347c81d44a5cbfb0702",
#     "token_type": "bearer",
#     "expires_in": 31557600,
#     "scope": "fleet",
#     "created_at": 1520393200
#   }
#
#   The token should then be passed up as a bearer token in the HTTP headers:
#
#   Authorization: "Bearer 426ddc9bc742e980d5c615fc3f895524dd382c4435e19347c81d44a5cbfb0702"
#
class GraphQLController < ActionController::Base

  include GraphQLConcerns
  include ControllerRequestContext

  before_action :set_request_context
  before_action :handle_authentication, except: :fake_webhook
  around_action :wrap_in_error_bubbling, except: :fake_webhook
  UNKNOWN_ERROR = "unknown error"
  AUTHENTICATION_FAILURE = "authentication failure"

  rescue_from StandardError do |exception|
    # for any uncaught error we send to rollbar and render a generic json response
    Rollbar.error(exception)
    Rails.logger.error exception
    render_result result: { errors: [{ message: UNKNOWN_ERROR }] }
    raise exception if Rails.env.development?
  end

  def execute
    query = build_query params
    result = SwoopSchema.execute(query)
    handle_subscription result
    render_result result: result.to_h
  end

  def batch
    queries = params[:_json].to_a.map { |param| build_query param }
    result = SwoopSchema.multiplex(queries).map do |query|
      handle_subscription query
      query.to_h
    end
    render_result result: result
  end

  # used for testing webhook subscriptions locally
  def fake_webhook
    _headers = request.headers.to_h.slice(
      Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET,
      Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_ID,
      Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_VIEWER
    )
    Rails.logger.debug "GraphQLController#fake_webhook: got result=#{params['result']}, headers=#{_headers}"
    render status: 200, json: {}
  end

  private

  def build_query(params)
    query = {
      query: params[:query],
      variables: ensure_hash(params[:variables]),
      context: @auth_result.dig(:output, :context),
      operation_name: params[:operationName],
      only: @auth_result.dig(:output, :only_filter),
      root_value: @auth_result.dig(:output, :context, :api_company),
    }
    Utils::QueryVariableInliner.inline! query
    query
  end

  def viewer_company
    @auth_result.dig(:output, :context, :api_company)
  end

  # TODO ENG-10811 - handle batched subscriptions
  def handle_subscription(result)
    if result&.subscription?
      response.headers["X-Subscription-ID"] = result.context[:subscription_id]
      # Required for CORS requests:
      response.headers["Access-Control-Expose-Headers"] = "X-Subscription-ID"
      result['extensions'] ||= {}
      result['extensions']['subscription'] = { 'id' => result.context[:subscription_id] }
    end
  end

  def render_result(result: {}, status: :ok)
    # using Oj shaves off 50-150ms vs using active_model_serializer or using .to_json
    render status: status, json: Oj.dump(result)
  end

  def handle_authentication
    @auth_result = GraphQL::Authenticate.call(input: { request: request })
    unless @auth_result.success?
      Rollbar.error(@auth_result.message || AUTHENTICATION_FAILURE)
      return render_result status: :unauthorized, result: {
        errors: [{ message: @auth_result.message || AUTHENTICATION_FAILURE }],
      }
    end
  end

  def wrap_in_error_bubbling
    if @auth_result.dig(:output, :context, :api_company)&.has_feature?(Feature::LEGACY_GRAPHQL_ERRORS)
      # LOL is this threadsafe?
      begin
        SwoopSchema.error_bubbling = true
        yield
      ensure
        SwoopSchema.error_bubbling = false
      end
    else
      yield
    end
  end

  protected

  def set_request_context
    context = RequestContext.current
    context.source_application_id = SourceApplication.from_request(request)&.id
    context.user_id = SwoopAuthenticator.for(request).user&.id
  end

end
