# frozen_string_literal: true

# Available Params
# _skip_checks - Comma seperated line to skip checks
# _verbose     - Enable additional output
# _show_queues - Show the queues in various workers
# _force_services - Services to enable force status on
# _force_status   - Status to force above services into

class HealthCheckController < ApplicationController

  DEFAULT_CHECKS = [
    :database,
    :sidekiq,
    :redis,
    :delayed_jobs,
    :sidekiq_scheduled,
  ].freeze

  def index
    checks = available_checks
    checks << :issc unless ENV['ISSC_AUTOCONNECT'].eql?('false')

    services, our_status = perform_check_services(*checks)

    status_code = our_status.eql?(:red) ? 500 : 200

    if status_code == 500
      if ENV["SWOOP_ENV"] == "staging"
        Rails.logger.debug "HEALTH_FAILED #{services}"
      else
        Rails.logger.error "HEALTH_FAILED #{services}"
      end
    end

    return head(status_code) unless verbose_mode?
    render json: { current_status: our_status, services: services }, status: status_code
  end

  private

  def available_checks
    DEFAULT_CHECKS.select { |x| params[:_skip_checks].to_s[x.to_s].blank? }
  end

  #############################################################
  # Check Methods
  #############################################################

  def perform_check_services(*services)
    results    = {}
    our_status = :green

    services.each do |service|
      results[service] = send("check_#{service}")

      results[service][:status] = forced_status(service, results[service][:status])

      # Dont overwrite red state
      next if our_status.eql?(:red)

      our_status =  case results[service][:status]
                    when :green, :yellow
                      :green
                    else
                      :red
                    end
    end
    [results, our_status]
  end

  def check_issc
    check_wrapper do
      online = AuditIssc.where('data LIKE ?', '%"Event": "heartbeat"%')
        .where('created_at > ?', 3.minutes.ago).exists?
      { status: online ? :green : :red }
    end
  end

  # percentage of used memory for our states
  REDIS_YELLOW = 0.85
  REDIS_RED = 0.95

  def check_redis
    check_wrapper do
      rclient = RedisClient.client(:default)
      info = {}
      status = begin
                 info = rclient.info.slice("used_memory", "maxmemory")
                 usage = info["used_memory"].to_i / info["maxmemory"].to_i
                 if usage >= REDIS_RED
                   :red
                 elsif usage >= REDIS_YELLOW
                   :yellow
                 else
                   :green
                 end
               rescue ZeroDivisionError
                 # get here via division by zero which is a green state (no memory limit)
                 :green
               rescue StandardError
                 # get here from some other bad state (ie, no redis client)
                 :red
               end
      { status: status }.merge(info)
    end
  end

  def check_database
    check_wrapper do
      ApplicationRecord.connection.execute('SELECT 1')
      { status: :green }
    end
  end

  def check_sidekiq
    check_wrapper do
      HealthCheck::Sidekiq.new(show_queues?).to_json
    end
  end

  def check_sidekiq_scheduled
    check_wrapper do
      count = Sidekiq::ScheduledSet.new.size
      { status: :green, count: count }
    end
  end

  def check_delayed_jobs
    check_wrapper do
      HealthCheck::DelayedJobs.new(verbose_mode?).to_json
    end
  end

  #############################################################
  # Helper Methods
  #############################################################
  def forced_status(service, status)
    return status if params[:_force_services].blank?

    services = params[:_force_services].split(',')
    return status unless services.include?(service.to_s)

    params[:_force_status]&.to_sym || :green
  end

  def verbose_mode?
    params[:_verbose].present?
  end

  def show_queues?
    params[:_show_queues].present?
  end

  def check_wrapper
    yield
  rescue => e
    { status: :red, error: e.message }
  end

end
