# frozen_string_literal: true

# Controller for supplying Oauth2 authentication flow.
class AuthController < ApplicationController

  class InvalidOauthStateError < StandardError; end
  class InvalidAccessTokenError < StandardError; end
  class InvalidRedirectUriError < StandardError; end

  # TODO render as friendly user error page; this is here just to expose errors for debugging.
  rescue_from Agero::Rsc::Error do |exception|
    if exception.response&.payload
      render json: exception.response.payload, status: 500
    else
      raise exception
    end
  end

  rescue_from InvalidOauthStateError do |e|
    # TODO render a friendly user error page if this fails
    render plain: "Invalid Oauth2 state", status: 403
  end

  rescue_from InvalidAccessTokenError do |e|
    render plain: "Invalid Swoop access token", status: 403
  end

  # This action copies the user access token to the session so that the user
  # can be authenticated when we get the OAuth2 callback after authenticating with RSC.
  def agero_login
    raise InvalidAccessTokenError if params[:access_token].blank?

    state = SecureRandom.hex
    session[:auth_state] = state
    session[:access_token] = params[:access_token]
    location = Agero::Rsc::API.new.authorize_url(state)
    redirect_to(location)
  end

  # This action is the callback for RSC OAuth2 authentication. It will be
  # called with an authentication code which will be exchanged for an access token.
  def agero_callback
    validate_callback_state!

    info = Agero::Rsc::API.new.get_access_token(params[:code])

    @token = APIAccessToken.where(
      company: fetch_company!,
      vendorid: info["userClaim"]["vendorId"],
      deleted_at: nil,
      source: :agero
    ).first_or_initialize

    @token.access_token = info["token"]["access_token"]
    @token.save!
    logout_duplicate_tokens!(@token)
    @token.subscribe_to_notifications

    Agero::Rsc::PullLocationsWorker.perform_async(@token.id)

    session.delete(:auth_state)

    logger.debug "Agero access token: #{@token.access_token}"

    redirect_to root_url(anchor: "user/settings?integrationstab=true")
  end

  # This action is called by external services to initiate the OAuth2 flow with Swoop
  # as a provider. The view logic will determine if the user is logged in by seeing
  # if the access token is present. If not, the user will get a login screen. When it
  # has an access token, the view will redirect the browser back to this action via a POST.
  # This should be called with params for `client_id` and `redirect_uri` which will
  # be passed along to the Doorkeeper authorization endpoint.
  def partner_authentication
    status = 200
    error = nil

    begin
      # Ensure client_id and redirct_uri params are valid
      params.require([:client_id, :redirect_uri])
      filtered_params = params.permit(:client_id, :redirect_uri, :access_token)

      @application = Doorkeeper::Application.find_by!(uid: filtered_params[:client_id])

      raise InvalidRedirectUriError unless Doorkeeper::OAuth::Helpers::URIChecker.valid_for_authorization?(
        filtered_params[:redirect_uri],
        @application.redirect_uri
      )

      if filtered_params[:access_token].present? && request.post?
        # Store the access token in the session so it doesn't leak out in a referer URL.
        session[:access_token] = filtered_params[:access_token]

        company = fetch_company!

        unless company.live
          CompanyService::Update.new(
            company_id: company.id,
            company_attributes: { live: true }
          ).call
        end

        return redirect_to oauth_authorization_url(
          client_id: filtered_params[:client_id],
          redirect_uri: filtered_params[:redirect_uri],
          scope: "company",
          response_type: "code"
        )
      end
    rescue ActiveRecord::RecordNotFound
      error = 'invalid client_id'
      status = 404
      Rollbar.error("AuthController: #{error}", params.to_unsafe_hash)
    rescue InvalidRedirectUriError
      error = 'invalid redirect_uri'
      status = 422
      Rollbar.error("AuthController: #{error}", params.to_unsafe_hash)
    rescue ActionController::ParameterMissing => e
      error = "#{e.param} is missing"
      status = 422
      Rollbar.error("AuthController: #{error}", params.to_unsafe_hash)
    end

    @version = Swoop::VERSION
    @js = WebpackAssets.instance.js('application')
    @css = WebpackAssets.instance.css('application')

    gon.app = {
      client_id: @application&.uid,
      name: @application&.name,
      redirect_uri: @application&.redirect_uri,
    }

    gon.error = error if error.present?

    render html: '', layout: "react_application", status: status
  end

  private

  def fetch_company!
    # This essentially re-implements the api_user/api_company methods from
    # the ApiController.
    user_id = Doorkeeper::AccessToken.find_by!(token: session[:access_token])
      .resource_owner_id

    User.find(user_id).company
  end

  def validate_callback_state!
    if params[:state] != session[:auth_state]
      raise InvalidOauthStateError
    end
  end

  # Ensure that only one company is logged in to vendorid at a time.
  # This should not be an issue in production, but it can happen in
  # development and staging where it causes testing problems.
  def logout_duplicate_tokens!(token)
    duplicates = APIAccessToken.where(vendorid: token.vendorid, source: token.source, deleted_at: nil).where.not(id: token.id)
    duplicates.each do |duplicate|
      Agero::Rsc::DisconnectOauthWorker.perform_async(duplicate.company_id, duplicate.vendorid)
    end
  end

end
