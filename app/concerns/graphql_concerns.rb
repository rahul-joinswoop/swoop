# frozen_string_literal: true

module GraphQLConcerns

  extend ActiveSupport::Concern

  def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      if ambiguous_param.present?
        ensure_hash(JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash
      ambiguous_param
    when ActionController::Parameters
      # we don't need to permit these at all afaict - our schema means we know
      # exactly what we do and don't accept in mutations so mass assignment issues
      # shouldn't be present
      ambiguous_param.to_unsafe_h
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end

end
