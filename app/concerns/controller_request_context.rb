# frozen_string_literal: true

# Base controllers that need to maintain the request context and record
# user visits should include this module.
module ControllerRequestContext

  extend ActiveSupport::Concern

  included do
    prepend_around_action :request_context_filter
    after_action :record_visit
  end

  private

  # Record a user visit
  def record_visit
    if response.successful?
      context = RequestContext.current
      if context && context.user_id && context.source_application_id
        User::Visit.track(user_id: context.user_id, source_application_id: context.source_application_id)
      end
    end
  end

  # Setup the request context
  def request_context_filter(&block)
    context = RequestContext.new(uuid: request.uuid)
    RequestContext.use(context, &block)
  end

end
