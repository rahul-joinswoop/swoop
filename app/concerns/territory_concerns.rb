# frozen_string_literal: true

module TerritoryConcerns

  extend ActiveSupport::Concern

  # PostGIS Polygon: A polygon is a representation of an area.
  # The outer boundary of the polygon is represented by a ring.
  # This ring is a linestring that is both closed and simple as defined above.
  # Holes within the polygon are also represented by rings.
  #
  # As 'holes' (interior rings) are not applicable in territories case, we just need external coordinates.
  #
  # More Info:
  # http://postgis.net/workshops/postgis-intro/geometries.html#polygons
  # https://www.rubydoc.info/github/rgeo/rgeo/RGeo/Feature/Polygon
  #
  def coordinates
    polygon_area.exterior_ring.coordinates.map do |lat, lng|
      { lat: lat, lng: lng }
    end
  end

  # As 'holes' (interior rings) are not applicable in territories case,
  # we are validating that supplied polygon should not have any interior ring
  def does_not_have_interior_ring
    if polygon_area.present? && !polygon_area.interior_rings.empty?
      errors.add :polygon_area, "should not have interior ring."
    end
  end

end
