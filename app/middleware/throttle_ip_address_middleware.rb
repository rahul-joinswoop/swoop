# frozen_string_literal: true

class ThrottleIpAddressMiddleware

  class ThrottleError < StandardError
  end

  attr_reader :rate_limit, :rate_period, :whitelisted_ips, :blacklisted_ips

  def initialize(app, rate_limit:, rate_period:, blacklisted_ips: nil, whitelisted_ips: nil, enabled: true)
    @app = app
    @rate_limit = rate_limit
    @rate_period = rate_period
    @whitelisted_ips = Array(whitelisted_ips).map { |value| IPAddr.new(value) }.freeze
    @blacklisted_ips = Array(blacklisted_ips).map { |value| IPAddr.new(value) }.freeze
    @enabled = enabled
  end

  def call(env)
    remote_ip = remote_ip_addr(env)
    if blacklisted?(remote_ip)
      blacklist_response(remote_ip)
    elsif !localhost?(remote_ip) && !whitelisted?(remote_ip) && throttled?(remote_ip)
      request_throttle = throttle(remote_ip)
      wait_time = request_throttle.wait_time
      request_count = request_throttle.peek

      error = ThrottleError.new("IP address: #{remote_ip}, size: #{request_count}, wait time: #{wait_time}")
      warn(error)
      Rollbar.error(ThrottleError.new(error))

      if enabled?
        throttle_response(remote_ip, wait_time)
      else
        @app.call(env)
      end
    else
      @app.call(env)
    end
  end

  def enabled?
    @enabled
  end

  private

  def remote_ip_addr(env)
    remote_ip = ActionDispatch::Request.new(env).remote_ip
    if remote_ip.present?
      begin
        IPAddr.new(remote_ip)
      rescue
        nil
      end
    else
      nil
    end
  end

  def warn(message)
    Rails.logger.warn(message) if Rails.logger
  end

  def localhost?(remote_ip)
    remote_ip.loopback?
  end

  def blacklisted?(remote_ip)
    remote_ip.present? && blacklisted_ips.any? { |ip_addr| ip_addr.include?(remote_ip) }
  end

  def whitelisted?(remote_ip)
    remote_ip.present? && whitelisted_ips.any? { |ip_addr| ip_addr.include?(remote_ip) }
  end

  def throttled?(remote_ip)
    if remote_ip.blank? || rate_limit <= 0 || rate_period <= 0
      false
    else
      !throttle(remote_ip).allowed!
    end
  end

  def throttle(remote_ip)
    SimpleThrottle.new("ip:#{remote_ip}", limit: rate_limit, ttl: rate_period)
  end

  def blacklist_response(remote_ip)
    warn("#{self.class.name}: blocked request from blacklisted IP address: #{remote_ip}")
    [503, { "Content-Type": "text/plain" }, ["Server not available"]]
  end

  def throttle_response(remote_ip, wait_time)
    headers = { 'Content-Type' => 'application/json', 'Retry-After' => wait_time.ceil.to_s }
    payload = { error: "Throttle limit reached. Retry later." }.to_json
    [429, headers, [payload]]
  end

end
