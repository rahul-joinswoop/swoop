# frozen_string_literal: true

# Copied directly from ActiveRecord::Migration::CheckPending and modified to handle seed migrations
# The overall logic is exactly the same.
class CheckPendingSeedMigrations

  def initialize(app)
    @app = app
    @last_check = 0
  end

  def call(env)
    ActiveRecord::Base.logger.silence do
      mtime = SeedMigration::Migrator.get_last_migration_date.to_i
      if @last_check < mtime
        SeedMigration::Migrator.check_pending!
        @last_check = mtime
      end
    end
    @app.call(env)
  end

end
