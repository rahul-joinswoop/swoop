# frozen_string_literal: true

# Automatically use the ActiveSupport::TaggedLogging to tag requests with the request id
class LogRequestId

  REQUEST_ID = "action_dispatch.request_id"

  def initialize(app)
    @app = app
  end

  def call(env)
    request_id = env[REQUEST_ID] || SecureRandom.uuid

    Rails.logger.tagged(request_id) do
      @app.call(env)
    end
  end

end
