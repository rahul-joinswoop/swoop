# frozen_string_literal: true

# Rack middleware that will report warnings to Rollbar if a request takes above a certain
# amount of time. The reporting threshold is specified in the LONG_RUNNING_REQUEST_THRESHOLD
# environment variable.
class ReportLongRunningRequestMiddleware

  def initialize(app, threshold = nil)
    @app = app
    @threshold = (threshold || Float(ENV.fetch("LONG_RUNNING_REQUEST_THRESHOLD", "5")))
  end

  def call(env)
    request = ActionDispatch::Request.new(env)
    start_time = Time.now
    begin
      @app.call(env)
    ensure
      elapsed_time = Time.now - start_time
      if elapsed_time >= @threshold
        begin
          report_long_running_request(request, elapsed_time)
        rescue => e
          Rails.logger.warn(e)
        end
      end
    end
  end

  private

  def report_long_running_request(request, elapsed_time)
    elapsed_time = elapsed_time.round(3)
    data = {}
    data[:uuid] = request.uuid if request.uuid
    data[:url] = request.url
    data[:elapsed_time] = elapsed_time

    # Adding dependency on NewRelic here because it has useful information from both
    # traditional controllers and from GraphQL queries and it's just adding a bunch
    # of complex logic to re-implement this to get the same value.
    transaction_name = NewRelic::Agent.get_transaction_name
    message = "Long running request #{transaction_name}: #{elapsed_time}s"
    Rollbar.warn(message, data)
  end

end
