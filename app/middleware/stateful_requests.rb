# frozen_string_literal: true

# Since the app is mostly an API based app, we don't want statefulness.
# This middleware will allow adding cookies and session support to a
# whitelisted list of paths.
class StatefulRequests

  def initialize(app, *paths)
    @app = app
    @stateful_paths = paths.flatten.to_set
    session_store = ActionDispatch::Session::CookieStore.new(app)
    cookies = ActionDispatch::Cookies.new(session_store)
    @stateful_middleware = ActionDispatch::Session::CookieStore.new(cookies)
  end

  def call(env)
    request = Rack::Request.new(env)
    if @stateful_paths.any? { |path| path.match?(request.path) }
      @stateful_middleware.call(env)
    else
      @app.call(env)
    end
  end

end
