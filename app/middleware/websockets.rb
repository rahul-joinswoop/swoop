# frozen_string_literal: true

require 'faye/websocket'
require 'json'
require 'erb'
require 'asyncredis'

class Websockets

  class BaseWebsocket

    def set_channel(env)
      index = env['REQUEST_URI'].index('/', 2)
      @channel = env['REQUEST_URI'][index + 1..-1]
    end

    def initialize(ws, env)
      @ws = ws
      set_channel env
      @authenticated_token = false
    end

    def get_channel
      # the uuid
      @channel
    end

    def send(opts)
      # Rails.logger.debug("WS: Send called on #{@ws} with #{opts}")
      if @ws
        @ws.send opts
        true
      else
        false
      end
    end

    def close_websocket
      @ws = nil
    end

    def close_403
      Rails.logger.debug "WSAUTH: close 403"
      @ws.close
    end

    def get_company
      uuid = get_channel.gsub("event_company_", "")
      begin
        SomewhatSecureID.load! uuid
      rescue SomewhatSecureID::Error
        Company.find_by!({ uuid: uuid })
      end
    end

    def get_user_string
      if @user
        "#{@user.full_name}(#{@user.id})"
      else
        ""
      end
    end

  end

  class TrackTechnicianWebsocket < BaseWebsocket

    def message(data)
      uuid = data['uuid']
      job = Job.find_by({ uuid: uuid })
      if job
        job.publish_location
        job.service_location.base_publish("update")
      end
    end

    def authenticated(data)
      true
    end

  end

  class PartnerFleetWebsocket < BaseWebsocket

    def authenticated(data)
      if ENV['WSUSER']
        if @authenticated_token
          #          Rails.logger.debug "WSAUTH Already authenticated"
          true
        elsif data['sync']
          Rails.logger.debug "WSAUTH Sync received"

          token = data['sync']['token']

          oauth_token = OauthAccessToken.find_by(token: token)
          Rails.logger.debug "WSAUTH got oauth_token:#{oauth_token}"
          if !oauth_token || oauth_token.revoked_at
            if ENV['WSAUTH']
              Rails.logger.debug "WSAUTH (ENABLED) Unauthenticated: no token found or revoked, closing socket"
              return false
            else
              Rails.logger.debug "WSAUTH (DISABLED) Unauthenticated: no token found or revoked"
              return true
            end
          end
          @user = User.find(oauth_token.resource_owner_id)
          @authenticated_token = true
          Rails.logger.debug "WSAUTH Authenticated OK: #{get_user_string}"
          true
        else
          if ENV['WSAUTH']
            Rails.logger.debug "WSAUTH (ENABLED) Unauthenticated: Not already authenticated and not a sync message"
            false
          else
            #            Rails.logger.debug "WSAUTH (DISABLED) Unauthenticated: Not already authenticated and not a sync message"
            true
          end
        end
      else
        #        Rails.logger.debug "WSAUTH: WSUSER disabled"
        true
      end
    end

    def get_vehicle_id(id)
      (_, vehicle_id) = SomewhatSecureID.decode id
      # if we get here we're using a graphql id and a user is required.
      return nil if @user.blank?

      vehicle = get_company.vehicles.joins(:driver).where(users: { id: @user.id }).find(vehicle_id)
      vehicle.id
    rescue ActiveRecord::RecordNotFound
      nil
    rescue SomewhatSecureID::Error
      # we get here with legacy usage (ie, from web/old app)
      id
    end

    def message(data)
      # Rails.logger.debug("WS: received msg - #{data} on #{@ws} channel: #{get_channel}")
      if data['test']
        AsyncRedis.instance.publish(@channel, data)
      elsif data['vehicle_location_update']
        location_update = data['vehicle_location_update']
        vehicle_id = get_vehicle_id(location_update['id'])
        return unless vehicle_id
        lat = location_update['lat']
        lng = location_update['lng']
        UpdateVehicleLocationWorker.perform_async(get_company.id, vehicle_id, lat, lng, Time.now.utc.iso8601)
      elsif data['vehicle'] # depricate, remove after old android gone
        # {"vehicle":{"driver_id":885,"id":171,"lat":0,"lng":1}}
        company = get_company
        vehicle = data['vehicle']
        driver_id = vehicle['driver_id']
        if driver_id
          driver = User.find(driver_id)
        end
        vehicle.delete('driver_id')
        v = Vehicle.find_by(id: vehicle['id'], company: company)
        if v

          if driver
            v.driver = driver
            Rails.logger.debug("WS: RVLoc #{v.id} #{company.name} #{v.name} #{driver.full_name} with #{data}")
          else
            Rails.logger.debug("WS: RVLoc #{v.id} #{company.name} #{v.name} with #{data}")
          end
          vehicle.delete(:id)
          vehicle[:location_updated_at] = Time.now
          vehicle[:location_update] = true
          v.update!(vehicle)
        else
          Rails.logger.debug "Detected bad client from #{company.name}"
        end
      elsif data['sync']
        # e.g. {"sync":{"last_updated_at":"2016-04-21 16:14:10.248Z"}}

        if !ENV['WS_SYNC_DISABLE']

          last_updated_at = data['sync']['last_updated_at']

          Rails.logger.debug "WS: last_updated_at: #{last_updated_at}"
          too_old = false
          if last_updated_at
            last_time = DateTime.parse(last_updated_at)
            if last_time < 1.day.ago
              too_old = true
            end
          end

          company = get_company
          target_type = company.class.name
          target_id = company.id

          reader = WsEventLog::Reader.new({
            target_type: target_type,
            target_id: target_id,
            last_updated_at: last_updated_at,
          })
          num_events = reader.count

          if too_old || (num_events >= WsEventLog.max_events)
            body = {
              status: 403,
              message: 'Too many WS updates (too_old:#{too_old}, num_events:#{num_events})',
              refresh: true,
            }
            send(body.to_json)
          else
            reader.events.each { |ws_event| send(ws_event.json) }
          end
        end

      end
    end

    def set_channel(env)
      index = env['REQUEST_URI'].index('/', 2)
      @channel = env['REQUEST_URI'][index + 1..-1]
    end

  end

  KEEPALIVE_TIME = 15 unless defined? KEEPALIVE_TIME # in seconds
  unless defined? HANDLERS
    HANDLERS = {
      "track_technician" => TrackTechnicianWebsocket,
      "partner_fleet" => PartnerFleetWebsocket,
    }.freeze
  end

  def initialize(app)
    @app = app
    @fooclients = {}
  end

  def unsubscribe(handler)
    handler.close_websocket
    channel = handler.get_channel
    Rails.logger.debug "unsubscribe looking up channel in fooclients:#{channel} #{@fooclients.keys}" # channel broken here for some
    if @fooclients[channel]
      @fooclients[channel].delete(handler)
      if @fooclients[channel].length == 0
        AsyncRedis.instance.unsubscribe(channel)
        @fooclients.delete(channel)
      end
    else
      Rails.logger.warn "Can't find channel #{channel} for handler whilst unsubscribing"
    end
  end

  def publish(channel, msg)
    if @fooclients[channel]
      # Rails.logger.debug "WSPUBLISH: Sending msg to #{@fooclients[channel].length} listeners on #{channel}"
      @fooclients[channel].each do |handler|
        debug_line = nil
        if ENV['LOG_PUBLISH'] && msg.include?("Job\",")
          hash = JSON.parse(msg)
          job = hash['target']
          debug_line = "tt:#{job['type']} tid:#{job['id']} txn:#{hash['txn_id']} status:#{job['status']} channel: #{channel} user:#{handler.get_user_string}"
          Rails.logger.debug "WSPUBLISH: Redis      #{debug_line}"
        end

        if !handler.send(msg)
          if debug_line
            Rails.logger.debug "WSPUBLISH: Redis FAILED to send #{debug_line}"
          end
          unsubscribe(handler)
        end
      end
    else
      Rails.logger.debug "WS: No one listenening on channel #{channel}"
    end
  end

  def call(env)
    if Faye::WebSocket.websocket?(env)
      ws = Faye::WebSocket.new(env, nil, { ping: KEEPALIVE_TIME })
      handler = nil
      ws.on :open do |event|
        uri = env['REQUEST_URI'] # #ws://locahost/track_technician/3507ffccc1
        Rails.logger.debug "WS: open1 called #{uri}"
        index = uri.index('/', 2)
        Rails.logger.debug("WS: index: #{index}")
        path = uri[1..index - 1]
        Rails.logger.debug("WS: path: #{path}")
        handler = HANDLERS[path].new(ws, env)
        channel = handler.get_channel
        Rails.logger.debug("WS: channel: #{channel}")
        if !(@fooclients[channel])
          @fooclients[channel] = []
          AsyncRedis.instance.subscribe(channel, self)
        end
        @fooclients[channel] << handler
        Rails.logger.debug "WS: instance #{AsyncRedis.instance}"
      rescue Exception => e
        Rails.logger.error "WS: on :open Caught Exception #{e}"
        Rails.logger.error e.backtrace.join("\n")
        Rollbar.error(e)
      end

      ws.on :message do |event|
        json = JSON.parse(event.data)
        if ENV['WS_DEBUG']
          Rails.logger.debug("WS: websocket received #{json}")
        end
        if handler
          if handler.authenticated(json)
            handler.message(json)
          else
            handler.close_403
            unsubscribe(handler)
          end
        else
          msg = "WS: handler not initialised before message received"
          Rails.logger.error msg
          Rollbar.error(StandardError.new(msg))
        end
      rescue Exception => e
        Rails.logger.error "WS: on :message Caught Exception #{e}"
        Rails.logger.error e.backtrace.join("\n")
        Rollbar.error(e)
      end

      ws.on :close do |event|
        Rails.logger.debug "WS: websocket closed - #{ws.object_id}, #{event.code}, #{event.reason}"
        if handler
          unsubscribe(handler)
        end
      rescue Exception => e
        Rails.logger.error "WS: on :close Caught Exception #{e}"
        Rails.logger.error e.backtrace.join("\n")
        Rollbar.error(e)
      end

      # Return async Rack response
      ws.rack_response

    else
      begin
        @app.call(env)
      rescue Exception => e
        Rails.logger.error "WS: app threw exception :message Caught Exception #{e}"
        Rails.logger.error e.backtrace.join("\n")
        raise e
      end
    end
  end

end
