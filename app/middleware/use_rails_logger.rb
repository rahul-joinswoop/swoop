# frozen_string_literal: true

class UseRailsLogger

  def initialize(app)
    @app = app
  end

  def call(env)
    p "userailslogger:call called"
    env['rack.logger'] = Rails.logger
    @app.call(env)
  end

end
