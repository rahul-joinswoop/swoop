import UserStore from 'stores/user_store'
import ServiceStore from 'stores/service_store'
import JobStore from 'stores/job_store'
import Consts from 'consts'
import BaseTracking from 'BaseTracking'
import Utils from 'utils'
import Logger from 'lib/swoop/logger'
import { clone, isEmpty } from 'lodash'

class Tracking extends BaseTracking {
  constructor(...props) {
    super(...props)
    this.referrer = document?.referrer
    this.utmInformation = {}

    if (this.referrer.indexOf('joinswoop.com') !== -1) {
      this.referrer = Utils.getCookie('swoopReferrer')
      Consts.UTM_KEYS.forEach((utmKey) => {
        const cookieValue = Utils.getCookie(`swoop_utm_${utmKey}`)
        if (cookieValue) {
          this.utmInformation[utmKey] = cookieValue
        }
      })
    }
  }

  _sendEvent = (name, options, extra_options) => {
    let newOptions = options
    if (this.referrer && !options.referrer) {
      newOptions = clone(options)
      newOptions.referrer = this.referrer
    }

    if (window?.analytics?.track) {
      window.analytics.track(name, newOptions, extra_options)
    }
  }

  identify = (user, extra_options) => {
    if (window?.analytics?.identify) {
      const identifyOptions = {
        userId: user.id?.toString(),
        name: user.full_name,
        company: user.company?.name,
        companyType: user.company?.type,
      }
      window.analytics.identify(user.id, identifyOptions, extra_options)
    } else {
      Logger.debug("Segment analytics not init'd")
    }
  }

  identifyForSalesforce = (data, newrecord, referrer) => {
    if (window?.analytics?.identify) {
      const identifyOptions = {
        userId: data.user_id?.toString(),
        name: `${newrecord.user.first_name} ${newrecord.user.last_name}`,
        // see if can use full name
        company: data.name,
        companyType: data.type,
        email: newrecord.user.email,
        phone: data.phone,
        // state: Consts.STATE_MAP[data.location?.state],
        // city: data.location?.city,
        // postalCode: data.location?.zip,
        // country: if data.location?.zip? then 'USA' else '',
        // street: data.location?.street,
        // leadSource: 'Product Onboarding',
        // marketingSource: referrer,
        utmSource: this.utmInformation.source || '',
        utmCampaign: this.utmInformation.campaign || '',
        utmMedium: this.utmInformation.medium || '',
        utmTerm: this.utmInformation.term || '',
        utmContent: this.utmInformation.content || '',
      }
      const integrations = {
        integrations: {
          Salesforce: true,
        },
      }
      window.analytics.identify(data.user_id, identifyOptions, integrations)
    } else {
      Logger.debug("Segment analytics not init'd")
    }
  }

  page = (name, options = {}) => {
    const user = UserStore.getUser()
    const newOptions = clone(options)

    if (this.referrer && !options.referrer) {
      newOptions.referrer = this.referrer
    }

    if (user?.id && !options.userId) {
      newOptions.userId = user.id?.toString()
    }

    if (window?.analytics?.page) {
      window.analytics.page(name, newOptions)
    }
  }

  track = (name, options = {}, extra_options = {}) => {
    const newOptions = clone(options)
    let newExtraOptions = extra_options

    newOptions.version = window.version
    newOptions.user_type = 'Dispatcher'
    newOptions.platform = Utils.isMobile() ? 'Mobile Web' : 'Web'
    // Information re setting UTM values in Segment at: https://segment.com/docs/destinations/google-analytics/#page-screen

    if (UserStore.isAdmin()) {
      newOptions.user_type = 'Admin'
    } else if (UserStore.isOnlyDriver()) {
      newOptions.user_type = 'Driver'
    }

    if (!isEmpty(this.utmInformation)) {
      newExtraOptions = clone(extra_options)
      newExtraOptions.context = {
        campaign: {},
      }
      Consts.UTM_KEYS.forEach((utmKey) => {
        const utmValue = this.utmInformation[utmKey]

        if (utmValue) {
          if (utmKey === 'campaign') {
            newExtraOptions.context.campaign.name = utmValue
          } else {
            newExtraOptions.context.campaign[utmKey] = utmValue
          }
        }
      })
    }
    super.track(name, newOptions, newExtraOptions)
  }

  trackDispatchEvent = (name, options = {}, extra_options = {}) => {
    const newOptions = clone(options)
    newOptions.category = 'Dispatch'
    this.track(name, newOptions, extra_options)
  }

  trackJobEvent = (name, job_id, options = {}) => {
    const newOptions = clone(options)
    newOptions.job_id = job_id

    if (UserStore.isPartner()) {
      const account = JobStore.getAccountNameByJobId(job_id)

      if (!isEmpty(account)) {
        newOptions.account = account
      }

      newOptions.source = account === 'Agero' ? 'RSC' : 'ISSC'
    }

    const serviceName = ServiceStore.get(JobStore.get(job_id, 'service_code_id'), 'name')

    if (serviceName) {
      newOptions.service = serviceName
    }

    if (job_id) {
      this.track(name, newOptions)
    }
  }

  trackRSCEvent = (name, options = {}, extra_options = {}) => {
    const newOptions = clone(options)
    newOptions.category = 'RSC'
    this.track(name, newOptions, extra_options)
  }
}

window.tracking = new Tracking()
export default window.tracking
