import Logger from 'lib/swoop/logger'
import Consts from 'consts'
import $ from 'jquery'
import { extend, isEmpty, map, mapValues, partial } from 'lodash'
import jParam from 'jquery-param'
import Dispatcher from 'lib/swoop/dispatcher' // TODO: add bearer in header

$.ajaxSetup({
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json; charset=utf-8',
  },
})

let setup = false
let _token = null
const Api = {
  PARTNER: 'partner',
  FLEET: 'fleet',
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
  UPDATE: 'PATCH',

  getBearerToken() {
    return _token
  },

  setBearerToken(token) {
    _token = token

    setup = true
    return $.ajaxSetup({
      headers: {
        Authorization: `Bearer ${token}`,
      },
      dataType: 'json',
    })
  },

  clearBearerToken() {
    return $.ajaxSetup({
      headers: {
        'Content-Type': 'application/json',
        Authorization: '',
      },
      dataType: 'json',
    })
  },

  revokeToken(andThen) {
    const revokeData = {
      token: _token,
    }
    return $.ajax({
      type: Api.POST,
      url: '/oauth/revoke',
      data: JSON.stringify(revokeData),
      success: typeof response !== 'undefined' && response !== null ? response.success : undefined,
      error: typeof response !== 'undefined' && response !== null ? response.error : undefined,
      jsonp: false,
    }).always(andThen)
  },

  _request401(setup, isSuccessfullCallback) {
    return setup ? Dispatcher.send('refreshToken', isSuccessfullCallback) : undefined
  },

  _request403() {
    Logger.debug('403 -PROGRAMMING ERROR- Sending logout', Consts.USER_STORE.LOGOUT, Consts.USER_STORE.RECEIVED_LOGIN)
    return setTimeout(Dispatcher.send(Consts.USER_STORE.LOGOUT, null), 0)
  },

  _showAuthError() {
    return Dispatcher.send('new_notification', {
      type: Consts.ERROR,
      message: Consts.MESSAGE_AUTH_ERR,
      dismissable: true,
      timeout: Consts.N_TIME,
      id: 'auth_error',
    })
  },

  request(type, url, data, response, options = {}) {
    const localOptions = options
    let apiUrl
    apiUrl = Consts.ROOT_PATH + Consts.API_PATH + url

    if (localOptions?.overrideUrl) {
      apiUrl = url
    }

    return $.ajax({
      type,
      url: apiUrl,
      data: data ? JSON.stringify(data) : null,
      success: response?.success,
      jsonp: false,
      ...options.ajaxOptions,
    }).fail((jqXHR, textStatus, errorThrown) => {
      Logger.debug('request fail', jqXHR)

      if (jqXHR.status === 401 && !localOptions.ignore401) {
        // Ensure we don't get into a refresh loop, only allow 5 attempts
        const newOptions = extend(
          {
            request_attempts: 0,
          },
          localOptions,
        )

        newOptions.request_attempts++

        if (newOptions.request_attempts > 5) {
          Api._showAuthError()

            response?.error?.(jqXHR, textStatus, errorThrown)
            return
        }

        return Api._request401(setup, (tokenRefreshed) => {
          if (tokenRefreshed) {
            return Api.request(type, url, data, response, newOptions)
          } else {
            // Perhaps it failed because a different request refreshed already,
            // wait a bit and try again
            return setTimeout(partial(Api.request, type, url, data, response, newOptions), 100)
          }
        })
      } else {
        Logger.debug(jqXHR.status, 'received')
        return response?.error?.(jqXHR, textStatus, errorThrown)
      }
    })
  },

  loginWithToken(token, response) {
    return this.process_login(
      {
        token,
        grant_type: 'password',
      },
      response,
    )
  },

  login(username, pass, response) {
    if (username == null) {
      return
    }

    const data = {
      grant_type: 'password',
      password: pass,
    }

    if (username.indexOf('@') > 0) {
      data.email = username.toLowerCase().trim()
    } else {
      data.username = username.toLowerCase().trim()
    }

    return this.process_login(data, response)
  },

  process_login(data, response) {
    return $.ajax({
      type: Api.POST,
      url: `${Consts.ROOT_PATH}/oauth/token`,
      data: JSON.stringify(data),
      success: response.success,
      error: response.error,
    })
  },

  newPasswordRequest(user_input, response) {
    return $.ajax({
      type: Api.POST,
      url: `${Consts.API_PATH}/users/password_request`,
      data: JSON.stringify({
        user_input,
      }),
      success: response.success,
      error: response.error,
    })
  },

  vinLookup(vin, response) {
    return fetch(`https://vpic.nhtsa.dot.gov/api/vehicles/DecodeVINValues/${vin}?format=json`).
    then((res) => {
      if (res.ok) {
        return res.json().then(response.success)
      } else {
        return response.error()
      }
    }).
    catch(e => response.error(e))
  },

  jobAssignNewJobProvider(data, endpoint, response) {
    return $.ajax({
      type: Api.UPDATE,
      url: `${Consts.API_PATH}${endpoint}/jobs/${data.id}/change_provider_to/${
        data.new_rescue_provider_id
      }`,
      data: JSON.stringify(data),
      success: response.success,
      error: response.error,
    })
  },

  jobChoosePartner(data, endpoint, response) {
    return $.ajax({
      type: Api.UPDATE,
      url: `${Consts.API_PATH}${endpoint}/jobs/${data.job.id}/choose_partner`,
      data: JSON.stringify(data),
      success: response.success,
      error: response.error,
    })
  },

  jobChangePaymentMethod(data, endpoint, response) {
    return $.ajax({
      type: Api.UPDATE,
      url: `${Consts.API_PATH}${endpoint}/jobs/${data.id}/change_payment_type_to/${
        data.new_payment_type_id
      }`,
      data: JSON.stringify(data),
      success: response.success,
      error: response.error,
    })
  },

  validatePasswordRequest(uuid, response) {
    return $.ajax({
      type: Api.GET,
      url: `${Consts.API_PATH}users/password_request/status?uuid=${uuid}`,
      success: response.success,
      error: response.error,
    })
  },

  changePassword(data, response) {
    return $.ajax({
      type: Api.UPDATE,
      url: `${Consts.API_PATH}/users/change_password`,
      data: JSON.stringify(data),
      success: response.success,
      error: response.error,
    })
  },

  getTime(response) {
    return $.ajax({
      type: Api.GET,
      url: `${Consts.ROOT_PATH}/time?${new Date().getTime()}`,
      success: response.success,
      error: response.error,
    })
  },

  authenticateAs(user_id, response) {
    return $.ajax({
      type: Api.POST,
      url: `${Consts.ROOT_PATH}/api/v1/root/authenticate_as`,
      data: JSON.stringify({
        user_id,
      }),
      success: response.success,
      error: response.error,
    })
  },

  refreshTokenRequest(token, response) {
    const data = {
      grant_type: 'refresh_token',
      refresh_token: token,
    }
    return Api.request(Api.POST, `${Consts.ROOT_PATH}/oauth/token`, data, response, {
      overrideUrl: true,
      ignore401: true,
    })
  },

  getRecommendedProvider(type, job_id, provider_id, response) {
    return Api.objectsRequest(
      `${type}/jobs/${job_id}/recommended_providers/${provider_id}`,
      Api.GET,
      null,
      response,
    )
  },

  objectsRequest(url, requestType, id, response, data) {
    return Api.request(requestType, url + (id != null ? `/${id}` : ''), data, response)
  },

  track(action, label, value, target_id, target_type) {
    return Api.objectsRequest('metrics', Api.POST, null, null, {
      metric: {
        action,
        label,
        value,
        target_id,
        target_type,
      },
    })
  },

  userMeRequest(response) {
    return Api.objectsRequest('users/me', Api.GET, null, response, null)
  },

  signup(data, response) {
    return Api.objectsRequest('partner/signup', Api.POST, null, response, data)
  },

  undeleteUser(data, response) {
    return Api.request(Api.UPDATE, 'root/users/restore', data, response)
  },

  undeleteJob(data, response) {
    return Api.request(Api.UPDATE, `root/jobs/${data.id}/restore`, data, response)
  },

  deleteStripeAccount(data, response) {
    return Api.request(
      Api.DELETE,
      `root/custom_accounts?rescue_company_id=${data.name}`,
      null,
      response,
    )
  },

  deleteIntegration(integration_id, response) {
    return Api.request(
      Api.DELETE,
      `partner/integrations/${integration_id}`,
      null,
      response
    )
  },

  deleteStoredVehicle(job_storage_id, response) {
    return Api.request(
      Api.DELETE,
      `partner/inventory_items/${job_storage_id}`,
      null,
      response
    )
  },

  searchSitesByPhone(data, response) {
    return Api.objectsRequest('fleet/sites/search', Api.POST, null, response, data)
  },

  publicSearchSitesByPhone(data, response) {
    return Api.objectsRequest('sites/search', Api.POST, null, response, data)
  },

  addExistingPartner(data, response) {
    return Api.objectsRequest('fleet/rescue_companies/existing_site', Api.POST, null, response, data)
  },

  getCompanyView(id, response) {
    return Api.objectsRequest(
      `root/companies/init?acts_as_company_id=${id}`,
      Api.GET,
      null,
      response,
    )
  },

  addNewSite(data, response) {
    return Api.objectsRequest('fleet/rescue_companies/site', Api.POST, null, response, data)
  },

  getDispatchableSites(response) {
    return Api.objectsRequest('partner/sites/dispatchable', Api.GET, null, response)
  },

  getTireProgramSites(response) {
    return Api.objectsRequest('partner/sites/tire_program', Api.GET, null, response)
  },

  getTireProgramProviders(response) {
    return Api.objectsRequest('providers/tire_program', Api.GET, null, response)
  },

  getProvidersGroupedByPartner(response) {
    return Api.objectsRequest('providers/grouped_by_partner', Api.GET, null, response)
  },

  getMotorClubAccounts(response) {
    return Api.objectsRequest('partner/accounts/motorclub', Api.GET, null, response)
  },

  getAccountsWithoutRate(response) {
    return Api.objectsRequest('partner/accounts/without_rates', Api.GET, null, response)
  },

  createJobStatus(job_id, status, last_set_dttm, response) {
    return Api.objectsRequest(`jobs/${job_id}/audit_job_statuses`, Api.POST, null, response, {
      audit_job_status: {
        job_status_name: status,
        last_set_dttm,
      },
    })
  },

  loadPartnerCompany(id, response) {
    return Api.objectsRequest('fleet/rescue_companies', Api.GET, id, response)
  },

  addNewRescueProvider(data, response) {
    return Api.objectsRequest('fleet/rescue_companies', Api.POST, null, response, data)
  },

  versionsRequest(requestType, id, response, version) {
    return Api.objectsRequest('root/versions/', requestType, id, response, version)
  },

  versionsRequestPaginated(response, page, per_page, name) {
    return Api.objectsRequest(
      `versions?page=${page}&per_page=${per_page}&name=${name}`,
      Api.GET,
      null,
      response,
    )
  },

  addFleet(id, response, data) {
    return Api.objectsRequest(`root/companies/${id}/add_fleet`, Api.POST, null, response, data)
  },

  removeFleet(id, response, data) {
    return Api.objectsRequest(`root/companies/${id}/remove_fleet`, Api.POST, null, response, data)
  },

  providersRequest(requestType, id, response, provider) {
    return Api.objectsRequest('fleet/providers/', requestType, id, response, provider)
  },

  documentUploadRequest(userType, id, props, response) {
    return Api.request(
      Api.GET,
      `${userType}/jobs/${id}/attached_documents/presign`,
      props,
      response,
    )
  },

  documentCreate(userType, id, props, response) {
    return Api.request(Api.POST, `${userType}/jobs/${id}/attached_documents`, props, response)
  },

  documentUpdate(userType, id, obj, response) {
    return Api.request(
      Api.UPDATE,
      `${userType}/jobs/${id}/attached_documents/${obj.id}`,
      obj,
      response,
    )
  },

  getLogoCompany(userType) {
    return userType === 'fleet' ? 'rescue_company' : 'company'
  },

  logoUploadRequest(userType, id, props, response) {
    return Api.request(Api.GET, `${userType}/${this.getLogoCompany(userType)}/logo/presign`, props, response)
  },

  logoCreate(userType, id, props, response) {
    return Api.request(Api.POST, `${userType}/${this.getLogoCompany(userType)}/logo`, props, response)
  },

  updateCommission(commission, response) {
    return Api.request(Api.UPDATE, 'partner/company/commission', commission, response)
  },

  updateRolePermissions(permission, response) {
    return Api.request(Api.UPDATE, 'partner/company/role_permissions', permission, response)
  },

  updateAddonCommissionsExclusions(commissionExclusions, response) {
    return Api.request(
      Api.UPDATE,
      'partner/commissions/addons/exclusions',
      commissionExclusions,
      response,
    )
  },

  updateServiceCommissionsExclusions(commissionExclusions, response) {
    return Api.request(
      Api.UPDATE,
      'partner/commissions/services/exclusions',
      commissionExclusions,
      response,
    )
  },

  logoRemove(userType, response) {
    return Api.request(Api.DELETE, `${userType}/${this.getLogoCompany(userType)}/logo`, null, response)
  },

  updateJobDocument(type, job_id, props, response) {
    return Api.request(
      Api.UPDATE,
      `${type}/jobs/${job_id}/attached_documents/${props.id}`,
      props,
      response,
    )
  },

  vehiclesRequest(requestType, id, response, vehicle) {
    return Api.objectsRequest('partner/vehicles/', requestType, id, response, vehicle)
  },

  vehicleRequest(requestType, id, response, vehicle) {
    return Api.objectsRequest('partner/vehicles/', requestType, id, response, vehicle)
  },

  vehiclesLocationRequest(requestType, id, response, vehicle) {
    return Api.objectsRequest(
      `partner/vehicles/${id}/location`,
      requestType,
      null,
      response,
      vehicle,
    )
  },

  getVehiclesWithDriver(response) {
    return Api.objectsRequest('partner/vehicles/with_driver', Api.GET, null, response)
  },

  requestCallbackRequest(type, id, data, response) {
    return Api.request(Api.POST, `${type}/jobs/${id}/request_callback`, data, response)
  },

  jobCopy(type, id, response) {
    return Api.request(Api.POST, `${type}/jobs/${id}/copy`, null, response)
  },

  partnerCommand(data, response) {
    return Api.request(Api.POST, 'partner/commands', data, response)
  },

  sendJobDetailsEmail(endpoint, data, response) {
    return Api.request(
      Api.POST,
      `${endpoint}/jobs/${data.id}/send_details_email`,
      data,
      response,
    )
  },

  addOnJob(type, id, data, response) {
    return Api.request(Api.POST, `${type}/jobs/${id}/add_job`, data, response)
  },

  rejectJob(rtype, type, id, response, data) {
    return Api.request(Api.POST, `${type}/jobs/${id}/reject`, data, response)
  },

  releaseJob(rtype, type, id, response, data) {
    return Api.request(Api.UPDATE, `${type}/jobs/${id}/release`, data, response)
  },

  jobsStatusRequest(requestType, type, id, response, job) {
    Logger.debug('about to call jobsrequest with ', job)
    return Api.objectsRequest(`${type}/jobs/${id}/status`, requestType, null, response, job)
  },

  jobsRequest(requestType, type, id, response, job) {
    Logger.debug('about to call jobsrequest with ', job)
    return Api.objectsRequest(`${type}/jobs/`, requestType, id, response, job)
  },

  jobsVinRequest(requestType, type, id, response, job) {
    return Api.objectsRequest(`${type}/jobs/vin`, requestType, id, response, job)
  },

  invoiceCharge(id, charge_obj, response) {
    return Api.objectsRequest(`partner/invoices/${id}/charge`, Api.POST, null, response, charge_obj)
  },

  invoicePaymentRefund(invoice_id, payment_id, obj, response) {
    return Api.objectsRequest(
      `partner/invoices/${invoice_id}/payments/${payment_id}/refund`,
      Api.POST,
      null,
      response,
      obj,
    )
  },

  rscRequestMoreTime(job_id, response) {
    return Api.objectsRequest(
      `partner/jobs/${job_id}/request_more_time`, Api.POST, null, response, null
    )
  },

  invoicePaidRequest(requestType, type, id, response, job) {
    return Api.objectsRequest(`${type}/invoices/${id}/paid`, requestType, null, response, job)
  },

  invoiceRequest(requestType, type, id, response, job) {
    return Api.objectsRequest(`${type}/invoices/`, requestType, id, response, job)
  },

  invoiceRejectionReasons(requestType, type, id, response) {
    return Api.objectsRequest(
      `${type}/invoice_rejections/reasons`,
      requestType,
      null,
      response,
      null,
    )
  },

  invoiceRejectionRequest(requestType, type, id, response, data) {
    return Api.objectsRequest(`${type}/invoice_rejections`, requestType, id, response, data)
  },

  invoiceRejectionsListRequest(requestType, type, list, response, data) {
    return Api.objectsRequest(
      `${type}/invoice_rejections/?invoice_ids=${list.join(',')}`,
      requestType,
      null,
      response,
      data,
    )
  },

  locationTypeRequest(requestType, id, response, data) {
    return Api.objectsRequest('location_types', requestType, id, response, data)
  },

  locationTypeSearch(data, response) {
    return Api.objectsRequest('location_types/search', Api.POST, null, response, data)
  },

  jobsListRequest(requestType, type, list, response, job) {
    if (list != null) {
      return Api.objectsRequest(
        `${type}/jobs/?job_ids=${list.join(',')}`,
        requestType,
        null,
        response,
        job,
      )
    }
  },

  usersListRequest(requestType, list, response) {
    if (list != null) {
      return Api.objectsRequest(
        `/users/batch?user_ids=${list.join(',')}`,
        requestType,
        null,
        response,
      )
    }
  },

  metricsRequest(requestType, type, id, response, job) {
    return Api.objectsRequest(`${type}/metrics/`, requestType, id, response, job)
  },

  reportsRequest(requestType, type, id, response, job) {
    return Api.objectsRequest(`${type}/reports/`, requestType, id, response, job)
  },

  reportsPaginated(type, page, report, response, extras) {
    return Api.request(
      Api.GET,
      `${type
      }/reports/${
        report
      }?page=${
        page
      }&per_page=${
        Consts.REPORTING_JOBS_LOAD_COUNT
      }${extras != null ? `&${extras}` : ''}`,
      null,
      response,
    )
  },

  reportsDownload(type, report, response, extras) {
    return Api.request(
      Api.GET,
      `${type}/reports/${report}?${extras != null ? `&${extras}` : ''}`,
      null,
      response,
    )
  },

  parseSearch(search, entityType = 'job') {
    const keysByType = {
      job: [
        'job',
        'po',
        'service',
        'pickup',
        'dropoff',
        'customer',
        'phone',
        'vin',
        'license',
        'make',
        'model',
        'dispatched',
        'completed',
        'stored',
        'notes',
        'partner_notes',
        'driver_notes',
        'dispatch_notes',
        'account',
        'fleet',
        'ref_number',
        'partner',
        'swoop_notes',
        'fleet_notes',
        'status',
        'original_job',
        'department',
      ],
      user: [
        'id',
        'user',
        'username',
        'company_id',
        'email',
        'first_name',
        'last_name',
        'phone',
        'company_name',
      ],
      company: [
        'id',
        'name',
        'phone',
        'support_email',
        'accounting_email',
        'dispatch_email',
        'parent_company_id',
        'type',
      ],
      provider: [
        'id',
        'name',
        'dispatch_phone',
        'dispatch_email',
        'fax',
        'address',
        'primary_contact',
        'primary_phone',
        'primary_email',
        'accounting_email',
        'special_instructions',
        'vendor_id',
        'location_id',
        'wheels',
      ],
      account: [
        'id',
        'name',
        'address',
        'primary_phone',
        'primary_email',
        'accounting_email',
        'notes',
      ],
    }
    const keys = keysByType[entityType] // Doesn't allow escaped "'s
    // Matches a single param in the form of key:nospaces or key:"includes
    // spaces but no escaped quotes"
    const re = new RegExp(`(${keys.join('|')}):((?:"[^"]*")|[^\ ]*)`, 'i')
    let str = search
    const hash = {
      original: search,
    }
    let results = re.exec(str)
    hash.term = ''

    while (results) {
      let needle
      str = str.replace(results[0], '')
      const val = results[2].replace(/^"/, '').replace(/"$/, '') // Make sure that if expecting a date it matches, otherwise put it in term

      if (
        ((needle = results[1].toLowerCase()),
        !['dispatched', 'completed', 'stored'].includes(needle)) ||
        val.match(/(\d{8})(-\d{8})?$/) ||
        entityType !== 'job'
      ) {
        hash[results[1].toLowerCase()] = val
      } else {
        hash.term += results[0]
      }

      results = re.exec(str)
    }

    if (str != null) {
      hash.term += str.replace(/\s\s+/g, ' ').trim()
    }

    if (hash.term.length === 0) {
      delete hash.term
    }

    return hash
  },

  jobsRequestPaginated(requestType, type, page, response, extras, tab = 'jobs_index') {
    let localExtras = extras
    if (localExtras?.['search'] != null) {
      let order, original
      let stored = false

      if (localExtras.stored) {
        stored = true
        delete localExtras.stored
      }

      const searchMap = this.parseSearch(localExtras.search)
      delete localExtras.search

      if (stored) {
        localExtras.status = 'stored'
      }

      if (localExtras.order != null) {
        order = localExtras.order
        delete localExtras.order
      }

      if (searchMap.original != null) {
        original = searchMap.original
        delete searchMap.original
      }

      if (isEmpty(localExtras)) {
        localExtras = null
      }

      Api.request(
        requestType,
        `${type
        }/jobs/search_new/?${
          [
            `page=${page}`,
            `per_page=${Consts.JOBS_PER_PAGE_COUNT}`,
            original != null ?
              jParam({
                original,
              }) :
              undefined,
            order != null ? `order=${order}` : undefined,
            localExtras != null ?
              jParam({
                filters: localExtras,
              }) :
              undefined,
            jParam({
              search_terms: searchMap,
            }),
          ].join('&')}`,
        null,
        response,
      )
      return
    }

    if (isEmpty(localExtras)) {
      localExtras = null
    } // if partner is loading storage tab, we append 'storage' to the path

    const uri = `jobs${tab === 'storage_index' ? '/storage' : ''}`
    return Api.request(
      requestType,
      `${type
      }/${uri}/?page=${
        page
      }&per_page=${
        Consts.JOBS_PER_PAGE_COUNT
      }${localExtras != null ? `&${jParam(localExtras)}` : ''}`,
      null,
      response,
    )
  },

  searchPaginated(
    searchUrl,
    searchValue,
    filters,
    searchPage,
    paginationSize,
    order,
    type,
    response,
  ) {
    let hackedFilters
    let querySearchTerm = ''
    let originalTerm = ''

    if (searchValue?.length > 0) {
      const querySearchParsed = this.parseSearch(searchValue, type)

      if (querySearchParsed.original) {
        originalTerm = `&${jParam({
          original: querySearchParsed.original,
        })}`
        delete querySearchParsed.original
      } else {
        originalTerm = ''
      }

      querySearchTerm = `&${jParam({
        search_terms: querySearchParsed,
      })}`
    }

    if (filters != null) {
      // This is a hack as jParam (and rails) excludes empty arrays
      // https://stackoverflow.com/a/31996181
      hackedFilters = mapValues(filters, (v) => {
        if (Array.isArray(v)) {
          if (v.length === 0) {
            return null
          } else {
            return map(v, (el) => {
              if (el === null) {
                return 'null'
              } else {
                return el
              }
            })
          }
        }

        return v
      })
    }

    const queryFilters = hackedFilters != null ? `&${jParam(hackedFilters)}` : ''
    const queryOrder = order != null ? `&order=${order}` : ''
    const finalUrl =
      `${searchUrl
      }?page=${
        searchPage
      }&per_page=${
        paginationSize
      }${queryFilters
      }${queryOrder
      }${originalTerm
      }${querySearchTerm}`
    return Api.request(Api.GET, finalUrl, null, response)
  },

  doneDriverJobsRequest(requestType, type, page, response, extras) {
    Logger.debug('about to make job request', requestType, type, response)
    return Api.request(
      requestType,
      `${type
      }/users/me/jobs/done/?page=${
        page
      }&per_page=${
        Consts.JOBS_PER_PAGE_COUNT
      }${extras != null ? `&${extras}` : ''}`,
      null,
      response,
    )
  },

  runReport(type, id, filters, format, response) {
    return Api.request(
      Api.POST,
      `${type}/reports/${id}/run.${format || 'csv'}`,
      {
        report: filters != null ? filters : {},
      },
      response,
    )
  },

  downloadStorageList(type, id, filters, response) {
    return Api.request(
      Api.POST,
      `${type}/reports/storage_list`,
      {
        report: filters != null ? filters : {},
      },
      response,
    )
  },

  downloadFleetApproved(type, id, filters, format, response) {
    return Api.request(
      Api.POST,
      `${type}/invoices/download`,
      {
        report: filters != null ? filters : {},
      },
      response,
    )
  },

  downloadFleetUnpaid(type, id, filters, format, response) {
    return Api.request(
      Api.POST,
      `${type}/invoices/download/fleet`,
      {
        report: filters != null ? filters : {},
      },
      response,
    )
  },

  sendFleetApproved(type, id, filters, format, response) {
    return Api.request(
      Api.POST,
      `${type}/invoices/export/fleet`,
      filters != null ? filters : {},
      response,
    )
  },

  downloadPartnerUnpaid(type, id, filters, format, response) {
    // TODO: replace endpoint when unblocked by backend ticket: ENG-7873, FE ticket is ENG-7874
    // `${type}/invoices/download_all/partner`
    return Api.request(
      Api.POST,
      `${type}/invoices/download/partner`,
      {
        report: filters != null ? filters : {},
      },
      response,
    )
  },

  generateInvoicePdf(invoice_uuid, response) {
    return Api.request(Api.POST, `invoices/${invoice_uuid}/pdfs`, null, response)
  },

  showInvoicePdf(invoice_uuid, invoice_pdf_id, responseContentDisposition, response) {
    return Api.request(
      Api.GET,
      `invoices/${invoice_uuid}/pdfs/${invoice_pdf_id}?response_content_disposition=${responseContentDisposition}`,
      null,
      response,
    )
  },

  activeJobsRequest(requestType, type, response, page, load_count = Consts.JOBS_PER_PAGE_COUNT) {
    return Api.request(
      requestType,
      `${type}/jobs/active/?page=${page}&per_page=${load_count}`,
      null,
      response,
    )
  },

  activeDriverJobsRequest(requestType, type, response, page) {
    return Api.request(
      requestType,
      `${type}/users/me/jobs/?page=${page}&per_page=${Consts.JOBS_PER_PAGE_COUNT}`,
      null,
      response,
    )
  },

  doneJobsRequest(requestType, type, page, response, extras) {
    Logger.debug('about to make job request', requestType, type, response)
    return Api.request(
      requestType,
      `${type
      }/jobs/done/?page=${
        page
      }&per_page=${
        Consts.JOBS_PER_PAGE_COUNT
      }${extras != null ? `&${extras}` : ''}`,
      null,
      response,
    )
  },

  adminUsersRequest(requestType, type, id, response, job) {
    Logger.debug('about to make adminusers request', requestType, type, id, response, job)
    return Api.request(requestType, `${type}/users/`, job, response)
  },

  getUser(response) {
    return Api.request(Api.GET, 'users/me', null, response)
  },

  getVersion(response, name = 'web') {
    return Api.request(Api.GET, `versions/${name}/latest`, null, response)
  },

  resendInvoice(id, emails, response) {
    return Api.request(Api.POST, `partner/invoices/${id}/resend/`, emails, response)
  },

  getSampleRates(id, userEndpoint, company_id, type, vtype, response) {
    let url = `${userEndpoint}/invoices/${id}/samplerate` // will be passed when UserStore.isSwoop(). @see invoice_fields

    if (company_id) {
      url += `?company_id=${company_id}`
    }

    return Api.request(
      Api.POST,
      url,
      {
        invoice: {
          rate_type: type,
          invoice_vehicle_category_id: vtype,
        },
      },
      response,
    )
  },

  getPartnerServices(partner_id, response) {
    return Api.request(Api.GET, `fleet/services/partner/${partner_id}`, null, response)
  },

  getPartnerVehicleCategories(partner_id, response) {
    return Api.request(Api.GET, `fleet/vehicle_categories/partner/${partner_id}`, null, response)
  },

  requestPartnerJobs(type, id, response, job) {
    return Api.jobsRequest(type, Api.PARTNER, id, job, response)
  },

  getPartnerJobs(id, response) {
    return Api.requestPartnerJobs(Api.GET, id, response)
  },

  createPartnerJob(id, data, response) {
    return Api.requestPartnerJobs(Api.POST, id, response, data)
  },

  createDemoJob(response) {
    return Api.request(Api.POST, 'partner/jobs/demo', null, response)
  },

  updatePartnerJob(id, job, response) {
    return Api.requestPartnerJobs(Api.UPDATE, id, response, job)
  },

  deletePartnerJob(id, job, response) {
    return Api.requestPartnerJobs(Api.DELETE, id, response, job)
  },

  requestFleetJobs(type, id, response, job) {
    return Api.request(type, Api.FLEET, id, job, response)
  },

  getFleetJobs(id, response) {
    return Api.requestFleetJobs(Api.GET, id, response)
  },

  createFleetJob(id, data, response) {
    return Api.requestFleetJobs(Api.POST, id, response, data)
  },

  updateFleetJob(id, job, response) {
    return Api.requestFleetJobs(Api.UPDATE, id, response, job)
  },

  deleteFleetJob(id, job, response) {
    return Api.requestFleetJobs(Api.DELETE, id, response, job)
  },

  addJobIssue(endpoint, job_id, props, response) {
    return Api.request(
      Api.POST,
      `${endpoint}/jobs/${job_id}/explanations/`,
      props,
      response,
    )
  },

  updateJobIssue(type, job_id, props, response) {
    return Api.request(
      Api.UPDATE,
      `${type}/jobs/${job_id}/explanations/${props.id}`,
      props,
      response,
    )
  },

  sendJobNotification(id, action, data, response) {
    Logger.debug('this is the response', response)
    return Api.request(Api.POST, `partner/jobs/${id}/${action}`, data, response)
  },

  sendLocation(id, lat, lng, address, place_id, auto, response) {
    // $.ajax
    //  type: Api.POST
    //  url: "/location"
    //  data: JSON.stringify({lat: lat, lng: lng, address: address})
    //  success: response?.success
    //  error: response?.error
    return Api.request(
      Api.POST,
      'get_location',
      {
        uuid: id,
        location: {
          lat,
          lng,
          address,
          place_id,
        },
        auto,
      },
      response,
    )
  },

  changeProvider(props, response) {
    return Api.request(
      Api.UPDATE,
      `root/jobs/${props.job_id}/change_provider_to/${props.new_rescue_provider_id}`,
      props,
      response,
    )
  },

  getApiCredentials(response) {
    return Api.request(Api.GET, 'oauth_applications', {}, response)
  },

  generateApiCredentials(response) {
    return Api.request(Api.POST, 'oauth_applications', {}, response)
  },

  deleteApiCredentials(id, response) {
    return Api.request(Api.DELETE, `oauth_applications/${id}`, {}, response)
  },

  createObjects(props, response) {
    return Api.request(Api.POST, 'root/fake_objects', props, response)
  },

  getCustomerJob(uuid, response) {
    return Api.request(Api.GET, `/jobs/uuid/${uuid}`, {}, response)
  },

  companies(response) {
    return Api.request(Api.GET, '/root/companies', {}, response)
  },

  fetchRecommendedProviders(id, type, response) {
    return Api.request(Api.GET, `${type}/jobs/${id}/recommended_providers`, {}, response)
  },

  providers(response) {
    return Api.request(Api.GET, '/fleet/providers', {}, response)
  },

  searchPolicies(request_type, number, jobId, response) {
    return Api.request(
      Api.POST,
      `${request_type}/policy`,
      {
        number,
        job_id: jobId,
      },
      response,
    )
  },

  requestDropLocationOptions(request_type, payload, response) {
    return Api.request(
      Api.POST,
      `${request_type}/dropoff_sites`,
      payload,
      response,
    )
  },

  lookupCoverage(request_type, payload, response) {
    return Api.request(
      Api.POST,
      `${request_type}/policy/coverage`,
      payload,
      response,
    )
  },

  searchPoliciesNew(request_type, jobId, search_terms, response) {
    return Api.request(
      Api.POST,
      `${request_type}/policy/search`,
      {
        search_terms,
        job_id: jobId,
      },
      response,
    )
  },

  policyDetails(request_type, id, response) {
    return Api.request(Api.GET, `${request_type}/policy/${id}`, null, response)
  },

  policyCoverage(request_type, id, pcc_vehicle_id, pcc_driver_id, response) {
    return Api.request(
      Api.GET,
      `${request_type}/policy/${id}/pcc_vehicle/${pcc_vehicle_id}/pcc_driver/${pcc_driver_id}/coverage`,
      null,
      response,
    )
  },

  invoicesPaginated(type, page, states, response, extras) {
    let localExtras = extras
    if (localExtras?.['job_search'] != null) {
      // STRIP OUT SEARCH HERE AND PULL OUT KEYS
      let order, original
      const searchMap = this.parseSearch(localExtras.job_search)

      delete localExtras.job_search

      if (localExtras.order != null) {
        order = localExtras.order
        delete localExtras.order
      }

      localExtras.invoice_state = states

      if (searchMap.original != null) {
        original = searchMap.original
        delete searchMap.original
      }

      if (isEmpty(localExtras)) {
        localExtras = null
      }

      Api.request(
        Api.GET,
        `${type
        }/invoices_search?page=${
          page
        }&per_page=25${
          original != null ?
            `&${jParam({
              original,
            })}` :
            ''
        }${order != null ? `&order=${order}` : ''
        }${localExtras != null ?
          `&${jParam({
            filters: localExtras,
          })}` :
          ''
        }&${
          jParam({
            search_terms: searchMap,
          })}`,
        null,
        response,
      )
      return
    }

    if (isEmpty(localExtras)) {
      localExtras = null
    }

    return Api.request(
      Api.GET,
      `${type
      }/invoices?page=${
        page
      }&per_page=25&states=${
        states.join(',')
      }${localExtras != null ? `&${jParam(localExtras)}` : ''}`,
      null,
      response,
    )
  },

  invoices(type, states, response) {
    return Api.request(Api.GET, `${type}/invoices?states=${states.join(',')}`, null, response)
  },

  accounts(response) {
    return Api.request(Api.GET, '/accounts', {}, response)
  },

  vehicleCategories(response) {
    return Api.request(Api.GET, '/partner/vehicle_categories', {}, response)
  },

  checkUsername(username, response) {
    return Api.request(
      Api.POST,
      '/users/search',
      {
        username,
      },
      response,
    )
  },

  customerTypes(response, type) {
    return Api.request(Api.GET, `${type}/customer_types`, {}, response)
  },

  standardServices(endpoint, response) {
    return Api.request(Api.GET, `${endpoint}/services/standard`, null, response)
  },

  standardAddons(endpoint, response) {
    return Api.request(Api.GET, `${endpoint}/services/standard?addons=true`, null, response)
  },

  standardVehicleCategories(endpoint, response) {
    return Api.request(Api.GET, 'vehicle_categories/standard', null, response)
  },

  standardSymptoms(endpoint, response) {
    return Api.request(Api.GET, 'symptoms/standard', null, response)
  },

  changeServices(services_object, response) {
    return Api.request(
      Api.POST,
      'services/change',
      {
        service: {
          change: services_object,
        },
      },
      response,
    )
  },

  changeAddons(addons_object, response) {
    return Api.request(
      Api.POST,
      'services/change?addons=true',
      {
        service: {
          change: addons_object,
        },
      },
      response,
    )
  },

  changeVehicleCategories(vehicle_categories_object, response) {
    return Api.request(
      Api.POST,
      'vehicle_categories/change',
      {
        vehicle_category: {
          change: vehicle_categories_object,
        },
      },
      response,
    )
  },

  changeSymptoms(symptoms_object, response) {
    return Api.request(
      Api.POST,
      'symptoms/change',
      {
        symptom: {
          change: symptoms_object,
        },
      },
      response,
    )
  },

  batchServiceByName(names, addon, response) {
    return Api.request(
      Api.GET,
      `services/batch_by_name?addons=${addon.toString()}&service_names=${names.toString()}`,
      null,
      response,
    )
  },

  loadServiceByName(name, addon, response) {
    return Api.batchServiceByName([name], addon, response)
  },

  invoice(uuid, response) {
    return Api.request(Api.GET, `/invoices/uuid/${uuid}?inline_jobs=true`, {}, response)
  },

  drivers(response) {
    return Api.request(Api.GET, '/partner/companies/drivers', {}, response)
  },

  vehicleMakes(response) {
    return Api.request(Api.GET, '/vehicles/makes', {}, response)
  },

  vehicleExtraTypes(response) {
    return Api.request(Api.GET, '/vehicles/extra_types', {}, response)
  },

  getQBToken(response) {
    return Api.request(Api.GET, 'partner/qb/token', {}, response)
  },

  getTireQuantityBySiteAndType(endpoint, response) {
    return Api.request(
      Api.GET,
      `${endpoint}/inventory_items/tire_quantity_by_site_and_type`,
      {},
      response,
    )
  },

  decreaseTireQuantity(endpoint, siteId, typeId, response) {
    const obj = {
      site_id: siteId,
      item: {
        tire_type_id: typeId,
      },
    }
    return Api.request(
      Api.UPDATE,
      `${endpoint}/inventory_items/decrease_tire_quantity`,
      {
        inventory_item: obj,
      },
      response,
    )
  },

  versions(response) {
    return Api.request(Api.GET, '/versions', {}, response)
  },

  lookerReport(request_type, dashboard_id, response) {
    // $.ajax
    //  type: Api.POST
    //  url: "#{Consts.API_PATH}/#{request_type}/look"
    //  data: JSON.stringify looker_path:looker_path
    //  success: response.success,
    //  error: response.error
    return Api.request(
      Api.POST,
      `${request_type}/look`,
      {
        dashboard_id,
      },
      response,
    )
  },

  autocomplete_company(type = null, term, count, response) {
    const args = {
      autocomplete: {
        type,
        term,
        count,
      },
    }
    return Api.request(Api.POST, '/root/autocomplete/company', args, response)
  },

  autocomplete_rescue_companies_by_provider(term, limit = 20, userEndpoint, response) {
    return this.autocomplete(term, 'rescue_companies_by_provider', limit, userEndpoint, response)
  },

  autocomplete_rescue_providers(term, limit = 10, userEndpoint, response) {
    return this.autocomplete(term, 'rescue_providers', limit, userEndpoint, response)
  },

  autocomplete_recommended_providers(term, limit = 10, userEndpoint, response, filters) {
    return this.autocomplete(term, 'recommended_providers', limit, userEndpoint, response, filters)
  },

  autocomplete_accounts(term, limit, userEndpoint, response, filters) {
    return this.autocomplete(term, 'accounts', limit, userEndpoint, response, filters)
  },

  autocomplete_root_users(term, limit, response, filters) {
    return this.autocomplete(term, 'root_users', limit, 'root', response, filters)
  },

  autocomplete(term, type, limit = 10, userEndpoint, response, filters = {}) {
    if (!term || !type || !userEndpoint) {
      return
    }

    const default_params = {
      term,
      limit,
    }
    const args = {
      autocomplete: extend({}, default_params, filters),
    }
    return Api.request(Api.POST, `/${userEndpoint}/autocomplete/${type}`, args, response)
  },

  signupAgeroPartner(email, client_id, redirect_uri) {
    return new Promise((resolve, reject) =>
      $.ajax({
        type: Api.POST,
        url: '/auth/email_verifications',
        data: JSON.stringify({ email, client_id, redirect_uri }),
        success: resolve,
        error: reject,
      })
    )
  },

  createPasswordRequestAgeroPartner(email, uid) {
    return new Promise((resolve, reject) =>
      $.ajax({
        type: Api.POST,
        url: '/auth/password_requests',
        data: JSON.stringify({ user: { email }, application: { uid } }),
        success: resolve,
        error: reject,
      })
    )
  },

  changePasswordAgeroPartner(uuid, password) {
    return new Promise((resolve, reject) =>
      $.ajax({
        type: Api.UPDATE,
        url: `/auth/password_requests/${uuid}`,
        data: JSON.stringify({ user: { password } }),
        success: resolve,
        error: reject,
      })
    )
  },

}
export default Api
