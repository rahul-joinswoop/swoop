import $ from 'jquery'
import ReactDOMFactories from 'react-dom-factories'

global.$ = $
global.jQuery = $

const tags = [
  'del', 'div', 'table', 'span', 'strong', 'textarea',
  'thead', 'tbody', 'tr', 'th', 'td', 'input', 'h1', 'h2', 'h3',
  'h4', 'hr', 'form', 'label', 'select', 'option', 'button',
  'aside', 'ul', 'li', 'footer', 'header', 'img', 'nav', 'br',
]

tags.forEach(tag => global[tag] = ReactDOMFactories[tag])

if (window.Audio) {
  global.ding = new Audio('https://s3-us-west-1.amazonaws.com/joinswoop.static/ding_new.mp3')
}

if (!google) {
  global.google = {
    maps: {
      LatLng: (lat, lng) => ({
        latitude: parseFloat(lat),
        longitude: parseFloat(lng),
        lat: () => lat,
        lng: () => lng,
      }),
      LatLngBounds: (ne, sw) => ({
        getSouthWest: () => sw,
        getNorthEast: () => ne,
      }),

      OverlayView: () => ({}),
      InfoWindow: () => ({}),
      Marker: () => ({}),
      MarkerImage: () => ({}),
      Map: () => ({}),
      Point: () => ({}),
      Size: () => ({}),
      event: {
        addListener: () => ({}),
        addListenerOnce: () => ({}),
      },
    },
  }
}

export default global
