import Consts from 'consts'
import Api from 'api'
Fields = require 'fields/fields'
CompanyStore = require('stores/company_store').default
UsersStore = require('stores/users_store').default
SiteStore = require('stores/site_store').default
UserStore = require 'stores/user_store'
FeatureStore = require 'stores/feature_store'
import Utils from 'utils'
import { map, findIndex } from 'lodash'

class MultiCheckboxField extends Fields.CheckboxField
    getValue: ->
        if not @record?[@_recordProp]
            return false
        @record[@_recordProp].indexOf(@getKey()) != -1
    getKey: -> @_name
    alwaysBeforeSend: (newrecord) ->
      if (!@record?.id? || @original?[@_recordProp]?.length > 0) && @record?[@_recordProp]?.length == 0
        newrecord[@_recordProp] = []
    onChange: (e) ->
        if not @record[@_recordProp]
            @record[@_recordProp] = []

        key = @getKey()
        index = @record[@_recordProp].indexOf key

        if e.target.checked
            if index == -1
                @record[@_recordProp].push key
        else
            if index != -1
                @record[@_recordProp].splice index, 1

        @changeCallback?()

class RoleCheckboxField extends MultiCheckboxField
  _recordProp: "roles"


class EmailStatusCheckboxField extends MultiCheckboxField
  _recordProp: "job_status_email_preferences"
  isShowing: -> @record.job_status_email_frequency != 'none'
  getLabel: -> @_name

module.exports =
  FirstNameField: class FirstNameField extends Fields.Field
      _name: 'first_name'
      _label: 'First Name*'
      _required: true

  LastNameField: class LastNameField extends Fields.Field
      _name: 'last_name'
      _label: 'Last Name*'
      _required: true
      findErrors: () =>
        firstNameError = @props.form.fields.first_name.getError()
        # durty hack because of poor BE error reporting
        if firstNameError == 'and Last name have already been taken for this Company'
          'and First name have already been taken for this Company'
        else
          super(arguments...)

  PhoneField: class PhoneField extends Fields.PhoneField
    _name: "phone"
    _label: "Phone"
    allowDisableMasking: true

  PagerField: class PagerField extends Fields.PhoneField
    _name: "pager"
    _label: "Second Number"

  EmailField: class EmailField extends Fields.EmailField
    _name: "email"
    _label: "Email"
    isRequired: ->
      UserStore.isAdmin(@record)

  UserNameField: class UserNameField extends Fields.Field
      _name: 'username'
      _label: 'Username*'
      _required: true

  PasswordField: class PasswordField extends Fields.Field
      _name: 'password'
      _placeholder: "At least 8 characters"
      _inputType: 'password'
      getLabel: -> if @record.id then "New Password*" else "Password*"
      isRequired: -> !@record.id?
      findErrors: () ->
        if @record.password
          length = @record.password.length
          if length > 0 and length < 8
            return "Must be at least 8 characters"
        super(arguments...)
      alwaysBeforeSend: (newrecord) ->
        if newrecord?.password? and newrecord.password == ""
          delete newrecord.password

  CompanyField: class CompanyField extends Fields.BackendSuggestField
      _name: 'company_id'
      _label: 'Company'
      _placeholder: 'Type to search'
      _required: true
      isShowing: -> UserStore.isSwoop()
      getSuggestions: (options, callback) ->
        Api.autocomplete_company("Company",options.input,10,{
          success: (data) =>
            locationMap = map(data, (company) ->
                id: company.id
                label: company.name + ' (' + company.id + ')',
                name: company.name
              )
            callback(locationMap)
          error: (data, textStatus, errorThrown) =>
            console.warn("Autocomplete failed")
        })

      customFindErrors: ->
        if !@record.company_id && (@isRequired() || @component?.state?.userInput?.length > 0)
          return "Please select from dropdown"

      onSuggestSelect: (val) ->
        @hasSelectedSuggest = true
        if val?
          Utils.setValue(@record, @getName(), val.id)
          Utils.setValue(@record, "company_name", val.name)
        else
          Utils.setValue(@record, @getName(), null)
          Utils.setValue(@record, "company_name", null)
        Utils.setValue(@record, "roles", null)
        @changeCallback?()

      getValue: ->
        companyId = @record.company_id
        if companyId
          companyName = CompanyStore.get(companyId, "name", @props.componentId)
          if companyName != undefined
            return companyName + ' (' + companyId + ')'
          return ""+companyId
  SitesField: class SitesField extends Fields.MultiSelectField
      _name: 'site_ids'
      _label: 'Sites'
      _emptyIsAll: true
      isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_FLEET_SITES)
      prefill: ->
        if not @orignal?.site_ids?
          @record.site_ids = []
      isRequired: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_FLEET_SITES)
      getSuggestions: ->
        sites = SiteStore.loadAndGetAll(@props.componentId, 'name')

        map(sites, (site) ->
          value: site.id
          text: site.name
        )

  # This is a very tricky factory. It instantiates one field
  # corresponding to one element of UserStore.getCompany().availableNotificationSettings.
  #
  # On UserForm, it also renders or hides the corresponding field depending on the selected
  # role, and it can happen dinamically.
  #
  # This is an example of BE corresponding data for each element:
  # {
  #   notification_code: "new_job",
  #   notification_channels: ["Call", "Text"]
  # }
  #
  # The values that user can select in the form refer to notification_channels.
  # We need to set the notification_code manually in case the user does
  # not have it yet (see prefill() method).
  #
  NotificationSettingFactory: (availableNotificationSetting) ->
    notificationCode = availableNotificationSetting.notification_code
    name = "notification_settings.#{notificationCode}.notification_channels"
    label = availableNotificationSetting.description
    supportedRoles = availableNotificationSetting.supported_roles
    defaultValues = Array.from(availableNotificationSetting.default_notification_channels)

    return class NotificationSettingField extends Fields.MultiSelectField
      _notificationCode: notificationCode
      _name: name
      _supportedRoles: supportedRoles
      _defaultValues: defaultValues
      _placeholder: 'None'
      _showDropdownTitle: false

      getLabel: =>
        # UI logic specific to dispatched_to_driver notificationCode.
        #
        # BE sends the label generically as 'Job dispatched to driver'.
        # The logic in the UI for this case is:
        #
        # a - if user is being edited and first_name is present, replace 'driver' by user.first_name
        # b - if a new user being created or first_name is not present, replace 'driver' by 'this user'
        #
        if notificationCode == Consts.USER_NOTIFICATION_SETTINGS.DISPATCHED_TO_DRIVER
          if @record?.id
            if @record.id == UserStore.getUser().id
              label.replace('driver', 'me')
            else if @record.first_name?.length > 0
              label.replace('driver', @record?.first_name)
            else
              label.replace('driver', 'this user')
          else
            label.replace('driver', 'this user')
        else
          label

      showAll: -> false

      findOrSetCurrentSettingIndex: =>
        if @currentSettingIndex
          return @currentSettingIndex

        if !@record.notification_settings?
          @record.notification_settings = []

        index = findIndex(@record.notification_settings, (setting) => setting.notification_code == @_notificationCode)

        if index > -1
          @currentSettingIndex = index
        else
          @currentSettingIndex = @record.notification_settings.length

      getName: =>
        "notification_settings.#{@findOrSetCurrentSettingIndex()}.notification_channels"

      prefill: =>
        @record.notification_settings[@findOrSetCurrentSettingIndex()] = { notification_code: @_notificationCode }

        if !@record.id?
          @setValue(@_defaultValues, true)
        else
          @setValue([], true)

      getSuggestions: =>
        map(availableNotificationSetting.available_notification_channels, (channel) ->
          value: channel
          text: channel
        )

      isShowing: =>
        return false if !@record.roles?
        @_supportedRoles.some((role) => @record.roles.indexOf(role) >= 0)

      onChange: (value) =>
        if value?
          super(value.sort())
        else
          super([])

  SystemEmailFilteredCompanies: class SystemEmailFilteredCompanies extends Fields.FilterOutMultiSelectField
      _name: 'system_email_filtered_companies'
      _dropdownTitle: 'Fleets'
      _allText: 'All Fleet Managed'
      _filterText: "FILTER BY FLEET MANAGED"

      isLoading: -> CompanyStore._loadingFleetIds

      isShowing: -> UserStore.isSwoop()

      prefill: ->
        if not @original[@getName()]
          @record[@getName()] = []

      getSuggestions: ->
        CompanyStore.getSortedFleetManagedIds(@props.componentId)

      getObjectName: (id) ->
        longName = @getObjectLongName(id)
        if longName
          splittedLongName = longName.split(" ")
          if splittedLongName[0].length < 5 and splittedLongName[1]
            return splittedLongName[0] + " " + splittedLongName[1]
          else
            return splittedLongName[0]
        else
          return "loading..."

      getObjectLongName: (id) -> CompanyStore.get(id, 'name', @props.componentId)

      renderInput: ->
        div null,
          span null,
            "Send alerts for jobs from these fleets: "
          super(arguments...)

  AdminField: class AdminField extends MultiCheckboxField
      _name: 'admin'
      _label: 'Admin'
      _recordProp: "roles"

      isShowing: -> @record?.company_name != 'Swoop'

      onChange: ->
        super(arguments...)
        if @getValue() == false
          @props?.form?.fields.custom_account_visible?.setValue(false)

          lookerIndex = @record[@_recordProp].indexOf 'looker'
          if lookerIndex != -1
            @record[@_recordProp].splice lookerIndex, 1

        @props.onUserRoleChanged()

  RootField: class RootField extends MultiCheckboxField
      _name: 'root'
      _label: 'Root'
      _recordProp: "roles"

      isShowing: -> @record?.company_name == 'Swoop'

      onChange: ->
        super(arguments...)
        @props.onUserRoleChanged()

      _enabled: -> (UserStore.isRoot() and (Consts.JS_ENV != 'production' or UserStore.getUser().email == 'paul@joinswoop.com'))

  DispatcherField: class DispatcherField extends MultiCheckboxField
      _name: 'dispatcher'
      _label: 'Dispatcher'
      _recordProp: "roles"
      isShowing: -> @record?.company_name != 'Swoop' or ('root' not in (@record.roles or []))

      onChange: ->
        super(arguments...)
        @props.onUserRoleChanged()

      getKey: -> if @record?.company_name == 'Swoop' then 'swoop_dispatcher' else 'dispatcher'

  DriverField: class DriverField extends MultiCheckboxField
      _name: 'driver'
      _label: 'Driver'
      _recordProp: "roles"

      onChange: ->
        super(arguments...)
        @props.onUserRoleChanged()

      isShowing: -> (UserStore.isPartner() or UserStore.isRoot()) && @record?.company_name != 'Swoop'

  AnsweringServiceField: class AnsweringServiceField extends MultiCheckboxField
      _name: 'answering_service'
      _label: 'Answering Service'
      _recordProp: "roles"
      isShowing: -> (UserStore.isPartner() or UserStore.isRoot()) && @record?.company_name != 'Swoop'


  LookerField: class LookerField extends MultiCheckboxField
    _name: 'looker'
    _label: 'Insights'
    _recordProp: "roles"
    isShowing: ->
      UserStore.isRoot() && @record?.roles && 'admin' in @record.roles

  ReceiveSystemAlertsField: class ReceiveSystemAlertsField extends Fields.CheckboxField
      _name: 'receive_system_alerts'
      _label: 'System Alerts'
      isShowing: -> UserStore.isSwoop() && @record?.company_name == 'Swoop'

  CustomAccountField: class CustomAccountField extends Fields.CheckboxField
      _name: 'custom_account_visible'
      _label: 'Payment Settings'

      isShowing: ->
        if UserStore.isSwoop() && @record?.roles && 'admin' in @record.roles && @record.company_id
          rescueCompanyType = CompanyStore.get(@record.company_id, "type", @props.componentId) == 'RescueCompany'

          if rescueCompanyType && !@getValue()?
            @onChange(target:checked:true)

          rescueCompanyType

      # if company type changes from a RescueCompany to Fleet, we make sure it is unset (this is an edge case)
      alwaysBeforeSend: (newrecord) ->
        if newrecord?.company_id? && CompanyStore.get(@record.company_id, "type", @props.componentId) != 'RescueCompany'
          newrecord.custom_account_visible = null

  EmailFilterField: class EmailFilterField extends Fields.SelectField
    _name: "job_status_email_frequency"
    getOptions: ->
      name = @record.first_name
      after_name = " is"
      if !name? || name.length == 0
        name = "this user"
      if @render_props?.context == "user_form"
        name = "I"
        after_name = " am"
      [
        {text: "Don't send any emails", value: "none"},
        {text: "Send emails for jobs where #{name} #{after_name} the dispatcher", value: "dispatcher"},
        {text: "Send emails for all jobs", value: "all"}
      ]
    renderInput: ->
      div null,
        super(arguments...)
        if @getValue() != 'none'
          span null,
            "for the following statuses: "
    prefill: ->
      if !@record?.id?
        @setValue("dispatcher")

  EmailCreatedField: class EmailCreatedField extends EmailStatusCheckboxField
    _name: Consts.CREATED
    getLabel: -> 'Job Created or Scheduled'
    forcePrefill: ->
      if !@record?.id?
        @onChange(target:checked:true)

  EmailTowingField: class EmailTowingField extends EmailStatusCheckboxField
    _name: Consts.TOWING

  EmailAcceptedField: class EmailAcceptedField extends EmailStatusCheckboxField
    _name: Consts.ACCEPTED
    getLabel: -> 'ETA Provided'
    forcePrefill: ->
      if !@record?.id?
        @onChange(target:checked:true)

  EmailEtaExtendedField: class EmailEtaExtendedField extends EmailStatusCheckboxField
    _name: Consts.ETAEXTENDED
    getLabel: -> 'ETA Extended'

  EmailTowDestinationField: class EmailTowDestinationField extends EmailStatusCheckboxField
    _name: Consts.TOWDESTINATION

  EmailEnRouteField: class EmailEnRouteField extends EmailStatusCheckboxField
    _name: Consts.ENROUTE

  EmailCompletedField: class EmailCompletedField extends EmailStatusCheckboxField
    _name: Consts.COMPLETED
    getLabel: -> "Job Completed"

  EmailOnSiteField: class EmailOnSiteField extends EmailStatusCheckboxField
    _name: Consts.ONSITE

  EmailCanceledField: class EmailCanceledField extends EmailStatusCheckboxField
    _name: Consts.GOA
    getLabel: -> "Job Canceled or GOA"
    forcePrefill: ->
      if !@record?.id?
        @onChange(target:checked:true)
