#!/usr/bin/env node
const fs = require('fs')
const path = require('path')

const buildDir = path.resolve(__dirname, '../../../../public/assets')
const KeenTracking = require('keen-tracking')

// eslint-disable-next-line security/detect-non-literal-require, import/no-dynamic-require
const stats = require(path.resolve(buildDir, 'stats'))
const unsorted = [...new Set(
  [].concat(
    ...Object.values(stats.namedChunkGroups).
    map(c => c.assets.
    filter(a => a.match(/\.(js|css)$/)).
    map((f) => {
      const pieces = f.split('.')
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      return { [[pieces[0], pieces[pieces.length - 1]].join('.')]: fs.statSync(path.resolve(buildDir, f)).size }
    }))
  )
)].reduce((memo, v) => {
  Object.assign(memo, { ...v })
  return memo
}, {})

const sorted = Object.keys(unsorted).sort().reduce((memo, k) => {
  Object.assign(memo, { [k]: unsorted[k] })
  return memo
}, {})

const client = new KeenTracking({
  projectId: process.env.KEEN_BUILD_PROJECT_ID,
  writeKey: process.env.KEEN_BUILD_WRITE_KEY,
})

Object.keys(sorted).forEach(k => client.recordEvent('build', { name: k, size: sorted[k] }))
