
window = {}

let start, // log start timestamp
  end

const WebSocket = require('ws')

const ws = new WebSocket('ws://localhost:5000/partner_fleet/event_company_2a4e67a8c2')
// var ws = new WebSocket('wss://swoopmestaging.herokuapp.com/partner_fleet/event_company_fae3ad2dd4');
// var ws = new WebSocket('wss://swoopmestaging.herokuapp.com/partner_fleet/event_company_srkju34oJd3');


let i = 0


let min = 99999999
let max = 0
let total_time = 0
let num_times = 0

function send() {
  start = +new Date()
  ws.send(JSON.stringify({ test: i++ }))
  setTimeout(send, 1)
}

console.log('starting')
ws.on('open', () => {
  send()
  console.log('sent')
})

ws.on('message', (data, flags) => {
  const end = +new Date() // log end timestamp
  const diff = end - start
  if (diff < min) {
    min = diff
  }
  if (diff > max) {
    max = diff
  }
  total_time += diff
  num_times++
  console.log('received', data, 'in ', diff, 'ms', 'min/avg/max: ', min, total_time / num_times, max)

  // flags.binary will be set if a binary data is received.
  // flags.masked will be set if the data was masked.
})
