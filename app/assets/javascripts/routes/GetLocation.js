import React from 'react'
import ReactDOM from 'react-dom'
import GetLocation from 'mobile/GetLocation'
import 'mobile/Mobile'

ReactDOM.render(<GetLocation />, document.getElementById('root'))
