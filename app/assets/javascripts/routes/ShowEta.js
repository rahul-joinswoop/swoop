import React from 'react'
import ReactDOM from 'react-dom'
import 'mobile/Mobile'
import ShowEta from 'mobile/ShowEta'

ReactDOM.render(<ShowEta />, document.getElementById('root'))
