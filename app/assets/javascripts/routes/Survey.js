import React from 'react'
import ReactDOM from 'react-dom'
import 'mobile/Mobile'
import Survey from 'mobile/Survey'

ReactDOM.render(<Survey />, document.getElementById('root'))
