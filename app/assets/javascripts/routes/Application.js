/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS206: Consider reworking classes to avoid initClass
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'
import moment from 'moment-timezone'
import Api from 'api'
import 'application.scss'
import 'partner.scss'
import Logger from 'lib/swoop/logger'
import ReactDOM from 'react-dom'
import ApplicationEvents from 'application_events'
import Utils from 'utils'
import ReactDOMFactories from 'react-dom-factories'
import Application from 'components/Application'
import Router from 'components/Router'
import config from 'window'

window.moment = moment

Api.getTime({
  success(time) {
    const server = +moment(time, moment.ISO_8601) // add a second to account for round trip time

    const millis = +new Date()
    Logger.debug('Server Local Time Delta', server - millis)

    if (Math.abs(server - millis) > 10 * 60 * 1000) {
      window?.analytics?.track('Computer Time Off', {
        delta: server - millis,
        server_time: server,
        computer_time: millis,
        server_text: time,
      })
    }

    // Getting the wrong present time from some versions of IE, affecting ETAs, scheduling, etc.;
    // Explicitly get time from server to prevent this issue.
    window.moment.now = () => {
      const localTime = +new Date()
      const serverTime = server + localTime - millis
      return serverTime
    }

    return ApplicationEvents.serverTimeSet()
  },
})

if (typeof Audio !== 'undefined' && Audio !== null) {
  window.ding = new Audio('https://s3-us-west-1.amazonaws.com/joinswoop.static/ding_new.mp3')
}

Window.L = str => str

window.logging = {
  renders: false,
  websockets: false,
  updateChecks: false,
  showEventTriggers: false,
  frontendSearch: false,
}
if (Utils.isMobile()) {
  document.getElementsByTagName('body')[0].classList.add('mobile')
}

// Hack to get permissions to play audio in chrome/safari ENG-6586
document.documentElement.onclick = () => {
  if (typeof window.ding !== 'undefined' && window.ding !== null) {
    window.ding.volume = 0
    Utils.playDing(() => {
      window.ding.pause()
      return (window.ding.volume = 1)
    })
  }

  return (document.documentElement.onclick = null)
}

// F5 is intentionally disabled by product requirement https://swoopme.atlassian.net/browse/ENG-9676
document.addEventListener('keydown', (event) => {
  if ((event.which || event.keyCode) === 116) {
    event.preventDefault()
  }
})

if (window.HubSpotConversations) {
  window.HubSpotConversations.widget.load()
} else {
  window.hsConversationsOnReady = [
    () => {
      window.HubSpotConversations.widget.load()
    },
  ];
}

ReactDOM.render(<Application><Router /></Application>, document.getElementById('root'))
