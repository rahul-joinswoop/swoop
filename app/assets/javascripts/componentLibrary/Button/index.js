import React from 'react'
import PropTypes from 'prop-types'
import './style.scss'

const Button = (props) => {
  const Component = props.component || 'button'
  return (
    <Component
      className={['component-button', props.size, props.color, props.className].filter(el => el != null).join(' ')}
      disabled={props.disabled}
      href={props.href}
      name={props.name}
      onClick={props.onClick}
      style={{
        lineHeight: Number(props.lineHeight) ? `${props.lineHeight}px` : props.lineHeight,
        ...props.style,
      }}
      target={props.target}
      type={props.type}
    >
      {props.label || props.children}
    </Component>
  )
}

Button.defaultProps = {
  component: 'button',
  disabled: false,
  label: null,
  name: null,
  type: 'button',
}

Button.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  component: PropTypes.string,
  disabled: PropTypes.bool,
  href: PropTypes.string,
  label: PropTypes.string,
  lineHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  name: PropTypes.string,
  onClick: PropTypes.func,
  size: PropTypes.string,
  target: PropTypes.string,
  type: PropTypes.string,
}

export default Button
