import React from 'react'
import { mount } from 'enzyme'
import Button from '.'

describe('<Button /> Component', () => {
  let component

  const handleClick = jest.fn()

  it('Renders the component', () => {
    component = mount(<Button />)
    expect(component).toBeDefined()
  })

  it('Triggers click handler', () => {
    component = mount(<Button onClick={handleClick()} />)
    component.simulate('click')
    expect(handleClick).toHaveBeenCalled()
  })

  it('Accepts label as child', () => {
    component = mount(<Button>Button Name</Button>)
    expect(component.find('button').text() === 'Button Name').toEqual(true)
  })

  it('Accepts label as prop', () => {
    component = mount(<Button label="Button Name" />)
    expect(component.find('button').text() === 'Button Name').toEqual(true)
  })
})
