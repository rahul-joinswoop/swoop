import { useEffect, useCallback, useRef, useState } from 'react'

// Usage
// @link: https://usehooks.com/useHover/
export function useHover() {
  const [value, setValue] = useState(false)
  const ref = useRef(null)
  const handleMouseOver = () => setValue(true)
  const handleMouseOut = () => setValue(false)

  useEffect(
    () => {
      const node = ref.current
      if (node) {
        node.addEventListener('mouseover', handleMouseOver)
        node.addEventListener('mouseout', handleMouseOut)

        return () => {
          node.removeEventListener('mouseover', handleMouseOver)
          node.removeEventListener('mouseout', handleMouseOut)
        }
      }
    },
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
    [ref.current] // Recall only if ref changes - TODO the linter says this isn't right
  )
  return [ref, value]
}

// Usage
// @link: https://usehooks.com/useOnClickOutside/
export function useOnClickOutside(ref, handler) {
  useEffect(
    () => {
      const listener = (event) => {
        // Do nothing if clicking ref's element or descendent elements
        if (!ref.current || ref.current.contains(event.target)) {
          return
        }
        handler(event)
      }

      document.addEventListener('mousedown', listener)
      document.addEventListener('touchstart', listener)

      return () => {
        document.removeEventListener('mousedown', listener)
        document.removeEventListener('touchstart', listener)
      }
    },
    [ref, handler]
  )
}

// Usage
// @link: https://usehooks.com/usePrevious/
export function usePrevious(value) {
  // The ref object is a generic container whose current property is mutable ...
  // ... and can hold any value, similar to an instance property on a class
  const ref = useRef()

  // Store current value in ref
  useEffect(() => {
    ref.current = value
  }, [value]) // Only re-run if value changes

  // Return previous value (happens before update in useEffect above)
  return ref.current
}


// Used to create an unchanging variable in your function with a callback (e.g. form creation)
// Param: create is a callback function meant to return the instance variable on first render
export function useInstance(create) {
  const instance = useRef(null)
  if (instance.current === null) {
    instance.current = create()
  }
  return instance.current
}

// Usage
// @link: https://github.com/joinswoop/swoop/wiki/componentLibrary-Hooks#usetoggle
export function useToggle(initial = false) {
  const [toggle, setToggle] = useState(initial)
  const handleToggle = (shouldShow = null) => {
    setToggle((showing) => {
      if (shouldShow === true || shouldShow === false) {
        return shouldShow
      }
      return !showing
    })
  }
  return [toggle, handleToggle]
}

// Usage
// @link: https://github.com/joinswoop/swoop/wiki/componentLibrary-Hooks#usererender
export function useRerender() {
  const [, update] = useState(0)
  return useCallback(() => {
    update(currentCount => currentCount + 1)
  }, [])
}
