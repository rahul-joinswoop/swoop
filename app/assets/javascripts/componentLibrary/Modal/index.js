import { createContext } from 'react'
import contextWrapper from '../Context'

const ModalContext = createContext()
export const ModalContextProvider = ModalContext.Provider
export const ModalContextConsumer = ModalContext.Consumer
export const withModalContext = contextWrapper(ModalContextConsumer)
export default ModalContext
