import React from 'react'
import PropTypes from 'prop-types'
import './style.scss'

const FontAwesomeButton = (props) => {
  const label = props.label || props.children
  const hasLabel = label ? 'has-label' : null
  return (
    <button
      className={['component-fabutton fa', props.icon, props.color, props.size, hasLabel, props.className].filter(el => el != null).join(' ')}
      disabled={props.disabled}
      name={props.name}
      onClick={props.onClick}
      style={{
        lineHeight: Number(props.size) ? `${props.size}px` : null,
        minWidth: Number(props.size) ? `${props.size}px` : null,
        ...props.style,
      }}
      title={props.title}
      type={props.type}
    >
      { label ?
        <span>{label}</span> : null}
    </button>
  )
}

FontAwesomeButton.defaultProps = {
  disabled: false,
  label: null,
  type: 'button',
}

FontAwesomeButton.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  icon: PropTypes.string,
  label: PropTypes.string,
  lineHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  name: PropTypes.string,
  onClick: PropTypes.func,
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  title: PropTypes.string,
  type: PropTypes.string,
}

export default FontAwesomeButton
