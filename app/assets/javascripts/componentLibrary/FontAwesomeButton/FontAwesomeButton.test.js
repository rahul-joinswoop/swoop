import React from 'react'
import { mount } from 'enzyme'
import FontAwesomeButton from '.'

describe('<FontAwesomeButton /> Component', () => {
  let component

  it('Renders the component', () => {
    component = mount(<FontAwesomeButton />)
    expect(component).toBeDefined()
  })
})
