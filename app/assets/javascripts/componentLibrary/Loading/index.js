/* eslint-disable */
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import './style.scss'

class Loading extends PureComponent {
  static defaultProps = {
    color: 'rgba(0, 0, 0, 0.25)',
    padding: '12px 0',
    size: 48,
  }

  static propTypes = {
    className: PropTypes.string,
    color: PropTypes.string,
    display: PropTypes.string,
    id: PropTypes.string,
    message: PropTypes.string,
    padding: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    size: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    title: PropTypes.string,
  }

  icon = () => (
    <svg width="200" height="200" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid">
      <g transform="translate(80,50)">
        <g transform="rotate(0)">
          <circle cx="0" cy="0" r="10" fill={this.props.color} fillOpacity="1" transform="scale(1.06088 1.06088)">
            <animateTransform attributeName="transform" type="scale" begin="-0.875s" values="1.1 1.1;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
            <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.875s"></animate>
          </circle>
        </g>
      </g><g transform="translate(71.21320343559643,71.21320343559643)">
        <g transform="rotate(45)">
          <circle cx="0" cy="0" r="10" fill={this.props.color} fillOpacity="0.875" transform="scale(1.07338 1.07338)">
            <animateTransform attributeName="transform" type="scale" begin="-0.75s" values="1.1 1.1;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
            <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.75s"></animate>
          </circle>
        </g>
      </g><g transform="translate(50,80)">
        <g transform="rotate(90)">
          <circle cx="0" cy="0" r="10" fill={this.props.color} fillOpacity="0.75" transform="scale(1.08588 1.08588)">
            <animateTransform attributeName="transform" type="scale" begin="-0.625s" values="1.1 1.1;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
            <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.625s"></animate>
          </circle>
        </g>
      </g><g transform="translate(28.786796564403577,71.21320343559643)">
        <g transform="rotate(135)">
          <circle cx="0" cy="0" r="10" fill={this.props.color} fillOpacity="0.625" transform="scale(1.09838 1.09838)">
            <animateTransform attributeName="transform" type="scale" begin="-0.5s" values="1.1 1.1;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
            <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.5s"></animate>
          </circle>
        </g>
      </g><g transform="translate(20,50.00000000000001)">
        <g transform="rotate(180)">
          <circle cx="0" cy="0" r="10" fill={this.props.color} fillOpacity="0.5" transform="scale(1.01088 1.01088)">
            <animateTransform attributeName="transform" type="scale" begin="-0.375s" values="1.1 1.1;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
            <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.375s"></animate>
          </circle>
        </g>
      </g><g transform="translate(28.78679656440357,28.786796564403577)">
        <g transform="rotate(225)">
          <circle cx="0" cy="0" r="10" fill={this.props.color} fillOpacity="0.375" transform="scale(1.02338 1.02338)">
            <animateTransform attributeName="transform" type="scale" begin="-0.25s" values="1.1 1.1;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
            <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.25s"></animate>
          </circle>
        </g>
      </g><g transform="translate(49.99999999999999,20)">
        <g transform="rotate(270)">
          <circle cx="0" cy="0" r="10" fill={this.props.color} fillOpacity="0.25" transform="scale(1.03588 1.03588)">
            <animateTransform attributeName="transform" type="scale" begin="-0.125s" values="1.1 1.1;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
            <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.125s"></animate>
          </circle>
        </g>
      </g><g transform="translate(71.21320343559643,28.78679656440357)">
        <g transform="rotate(315)">
          <circle cx="0" cy="0" r="10" fill={this.props.color} fillOpacity="0.125" transform="scale(1.04838 1.04838)">
            <animateTransform attributeName="transform" type="scale" begin="0s" values="1.1 1.1;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
            <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="0s"></animate>
          </circle>
        </g>
      </g>
    </svg>
  )
  /* eslint-enable */

  render = () => (
    <div
      className={['loadingContainer', this.props.className].filter(el => el != null).join(' ')}
      id={this.props.id}
      style={{
        display: this.props.display,
        padding: this.props.padding,
      }}
    >
      {this.props.title && (
        <span className="loadingTitle">{this.props.title}</span>
      )}
      <div
        className="loading"
        style={{
          display: this.props.display,
          width: this.props.size,
        }}
      ><div><div>{this.icon()}</div></div>
      </div>
      {this.props.message && (
        <span>{this.props.message}</span>
      )}
    </div>
  )
}

export default Loading
