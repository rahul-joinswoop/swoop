import React from 'react'
import { mount, shallow } from 'enzyme'
import Loading from '.'

describe('<Loading /> Component', () => {
  let component

  it('Renders the component', () => {
    component = mount(<Loading />)
    expect(component).toBeDefined()
  })

  it('Contains SVG', () => {
    component = mount(<Loading />)
    expect(component.find('svg').exists()).toEqual(true)
  })

  it('Sets the CSS width property via the SIZE prop (number)', () => {
    component = shallow(<Loading size={24} />)
    expect(component.find('.loading').get(0).props.style).toHaveProperty('width', 24)
  })

  it('Sets the CSS width property via the SIZE prop (string)', () => {
    component = shallow(<Loading size="24px" />)
    expect(component.find('.loading').get(0).props.style).toHaveProperty('width', '24px')
  })
})
