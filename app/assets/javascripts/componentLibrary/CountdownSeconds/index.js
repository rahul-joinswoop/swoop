import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment-timezone'
import Moment from 'moment'
import './style.scss'

class CountdownSeconds extends Component {
  static defaultProps = {
    now: moment, // allow to inject now time for testing
    suffix: true,
  }

  static propTypes = {
    now: PropTypes.func,
    suffix: PropTypes.bool,
    time: PropTypes.instanceOf(Moment).isRequired,
  }

  displayName = 'CountdownSeconds'

  state = { rev: 0 }

  componentDidMount() {
    this.interval = setInterval(this.generalChanged, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  generalChanged() {
    this.setState(({ rev }) => ({ rev: rev + 1 }))
  }

  render() {
    let seconds
    if (this.props.time) {
      seconds = Math.ceil(
        this.props.time.diff(this.props.now(), 'seconds', true)
      )
    }

    return (
      <span className="CountdownSeconds">
        {seconds && <span>{`${seconds}${this.props.suffix ? ' s' : ''}`}</span>}
      </span>
    )
  }
}

export default CountdownSeconds
