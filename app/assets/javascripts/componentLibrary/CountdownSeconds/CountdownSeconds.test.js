import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment-timezone'
import CountdownSeconds from '.'

describe('CountdownSeconds Component', () => {
  it('renders', () => {
    let seconds = 0
    function now() {
      seconds += 1
      return moment(`2019-04-25 16:31:0${seconds}.981000`)
    }

    jest.useFakeTimers()

    const wrapper = shallow(<CountdownSeconds now={now} time={moment('2019-04-25 16:31:40.981000')} />)

    expect(wrapper.contains(<span>39 s</span>)).toBe(true)

    jest.runOnlyPendingTimers()

    expect(wrapper.contains(<span>38 s</span>)).toBe(true)
  })
})
