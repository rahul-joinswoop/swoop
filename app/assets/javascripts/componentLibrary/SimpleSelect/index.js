import React, { Component } from 'react'
import PropTypes from 'prop-types'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import { isEqual } from 'lodash'
import cn from 'classnames'
import './style.scss'

const OPTION_HEIGHT = 26 // 26px height of one item in the list

class Select extends Component {
  static propTypes = {
    autoFocus: PropTypes.bool,
    defaultValue: PropTypes.object,
    disabled: PropTypes.bool,
    height: PropTypes.number, // height in number of visible items
    labelField: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
    required: PropTypes.bool,
    value: PropTypes.object,
  }

  constructor(props) {
    super(props)

    this.state = {
      isInFocus: false,
      optionsOpen: false,
    }

    this.input = React.createRef()
    this.select = React.createRef()
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside)

    if (this.props.autoFocus) {
      this.handleFocus()
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside)
  }

  handleBlur = () => {
    this.input.current.blur()
    this.setState({ isInFocus: false })
  }

  handleClickOutside = (e) => {
    if (!this.select.current?.contains(e.target)) {
      e.preventDefault()
      this.setState({ optionsOpen: false }, this.handleBlur)
    }
  }

  handleFocus = () => {
    this.input.current.focus()
    this.setState({ isInFocus: true })
  }

  handleOptionSelect = (option) => {
    this.props.onChange(option)
    this.setState({ optionsOpen: false }, this.handleFocus)
  }

  handleOptionsToggle = (e) => {
    e.preventDefault()
    e.stopPropagation()
    if (e.target.tagName !== 'LI') {
      this.setState(
        ({ optionsOpen }) => ({ optionsOpen: !optionsOpen }),
        this.handleFocus,
      )
    }
  }

  render() {
    const {
      disabled, defaultValue, value, required, labelField, options, height, renderSelected,
    } = this.props

    const selected = value || defaultValue

    const {
      isInFocus, optionsOpen,
    } = this.state

    return (
      /* eslint-disable-next-line */
      <div
        className={cn('component-select-simple', {
          disabled,
          required,
          'input-has-focus': isInFocus,
        })}
        onClick={disabled ? null : this.handleOptionsToggle}
        ref={this.select}
      >
        <p className="selection-label">
          {renderSelected ? renderSelected(selected) : selected?.[labelField]}
        </p>
        <input
          disabled={disabled}
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          ref={this.input}
          required={required || null}
          style={{ opacity: 0 }}
          type="text"
        />
        <FontAwesomeButton
          className="toggle-options"
          disabled={disabled}
          icon={optionsOpen ? 'fa-angle-up' : 'fa-angle-down'}
          onClick={this.handleOptionsToggle}
        />
        {options &&
          <ul
            className="component-select-simple-options"
            style={{
              display: optionsOpen ? 'block' : 'none',
              height: height ? height * OPTION_HEIGHT : null,
            }}
          >
            {options.map(option =>
              /* eslint-disable-next-line */
              <li
                className={cn({ highlight: isEqual(selected, option) })}
                key={option[labelField]}
                /* eslint-disable-next-line */
                onClick={() => this.handleOptionSelect(option)}
              >
                {option[labelField]}
              </li>
            )}
            {options.length === 0 && <li className="no-matches">No matches ...</li>}
          </ul>}
      </div>
    )
  }
}

Select.defaultProps = {
  autoFocus: false,
  defaultValue: null,
  disabled: false,
  height: null,
  labelField: 'label',
  required: false,
  value: null,
}

export default Select
