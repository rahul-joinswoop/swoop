import React from 'react'
import { mount } from 'enzyme'
import Select from '.'

function noop() {}

describe('<SimpleSelect /> component', () => {
  const options = [
    { id: 1, name: 'object1', height: 300 },
    { id: 2, name: 'object2', height: 400 },
    { id: 3, name: 'object3', height: 500 },
  ]

  it('renders', () => {
    const wrapper = mount(<Select labelField="name" onChange={noop} options={options} />)
    expect(wrapper).toBeDefined()
  })

  it('shows/hides options', () => {
    const wrapper = mount(<Select labelField="name" onChange={noop} options={options} />)

    wrapper.find('.component-select-simple').simulate('click')

    expect(wrapper.find('.component-select-simple-options').prop('style')).toHaveProperty('display', 'block')

    wrapper.find('FontAwesomeButton.toggle-options').simulate('click')

    expect(wrapper.find('.component-select-simple-options').prop('style')).toHaveProperty('display', 'none')
  })

  it('selects option', () => {
    const handleOnChange = jest.fn()

    const wrapper = mount(<Select labelField="name" onChange={handleOnChange} options={options} />)

    wrapper.find('FontAwesomeButton.toggle-options').simulate('click')
    wrapper.find('.component-select-simple-options li').at(1).simulate('click')

    expect(handleOnChange).toHaveBeenCalledWith(options[1])

    expect(wrapper.find('.component-select-simple-options').prop('style')).toHaveProperty('display', 'none')
  })

  it('highlights default', () => {
    const wrapper = mount(<Select defaultValue={options[1]} labelField="name" onChange={noop} options={options} />)

    expect(wrapper.find('li.highlight').exists()).toBe(true)
    expect(wrapper.find('li.highlight').text()).toBe(options[1].name)
  })
})
