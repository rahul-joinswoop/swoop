import React, { Component } from 'react'
import PropTypes from 'prop-types'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import './style.scss'
import { find } from 'lodash'

class Select extends Component {
  static defaultProps = {
    allowCustom: false,
    autoFocus: false,
    clearButton: false,
    disabled: false,
    exactMatch: false,
    onChange: null,
    options: [],
    required: false,
    searchable: false,
    sortOptions: true,
  }

  static propTypes = {
    allowCustom: PropTypes.bool,
    autoFocus: PropTypes.bool,
    clearButton: PropTypes.bool,
    default: PropTypes.oneOfType([
      PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.string,
      }),
      PropTypes.string,
    ]),
    disabled: PropTypes.bool,
    exactMatch: PropTypes.bool,
    onChange: PropTypes.func,
    options: PropTypes.array,
    required: PropTypes.bool,
    searchable: PropTypes.bool,
    sortOptions: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.func,
    ]),
  }

  constructor(props) {
    super()
    this.state = {
      addedOptions: [],
      isDisabled: props.disabled,
      isInFocus: false,
      isValid: true,
      label: '',
      options: props.options,
      optionHighlight: null,
      optionsOpen: false,
      selected: null,
      value: typeof props.default === 'object' ? props.default.value : props.default || '',
    }
    this.input = React.createRef()
    this.select = React.createRef()
    this.selected = React.createRef()
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside)
    document.addEventListener('keyup', this.handleKeypress)
    this.isRequired()

    if (this.props.default) {
      this.mapInputToOption()
    }

    if (this.props.autoFocus) {
      this.handleFocus()
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside)
    document.removeEventListener('keyup', this.handleKeypress)
  }

  filterOptions = () => {
    if (this.props.searchable) {
      const options = this.sortOptions().filter((option) => {
        if (this.props.exactMatch) {
          return option.label.toLowerCase().indexOf(this.state.value.toLowerCase()) === 0
        } else {
          return option.label.toLowerCase().indexOf(this.state.value.toLowerCase()) !== -1
        }
      })

      if (this.props.allowCustom && !find(this.sortOptions(), option =>
        option.value.toLowerCase() === this.state.value.toLowerCase())
      ) {
        return this.handleNewOption(options, this.state.value)
      }
      return options
    } else {
      return this.props.options
    }
  }

  getFilteredOptions = () => {
    if (this.props.searchable) {
      this.setState(state => ({
        optionHighlight: state.value === '' ? null : 0,
        options: this.filterOptions(),
      }), () => {
        this.setState({
          isValid: this.isValidInput(),
        })
      })
    }
  }

  handleBlur = () => {
    this.input.current.blur()
    this.setState({
      isInFocus: false,
      optionHighlight: null,
    })
  }

  handleChange = (e) => {
    const { value } = e.target
    this.setState({
      optionsOpen: true,
      value,
    }, () => {
      this.isRequired()
      this.getFilteredOptions()
    })
    if (this.props.onChange) {
      this.props.onChange(e)
    }
  }

  handleClearInput = (e) => {
    e.preventDefault()
    if (this.state.value.length > 0) {
      e.stopPropagation()
    }
    this.setState({
      label: '',
      options: this.props.options,
      optionHighlight: 0,
      optionsOpen: false,
      selected: null,
      value: '',
    }, () => {
      this.select.current.classList.remove('input-is-invalid')
      this.handleFocus()
    })
  }

  handleClickOutside = (e) => {
    if (!this.select.current?.contains(e.target)) {
      e.preventDefault()
      this.setState({
        optionsOpen: false,
      }, () => {
        this.handleBlur()
      })
    }
  }

  handleFocus = () => {
    this.input.current.focus()
    this.setState({
      isInFocus: true,
    })
  }

  handleKeypress = (e) => {
    const { options } = this.state
    if (this.state.isInFocus) {
      if (e.key === 'ArrowDown') {
        this.handleKeypressForArrows(e, 'down', options)
      }

      if (e.key === 'ArrowUp') {
        this.handleKeypressForArrows(e, 'up', options)
      }

      if (e.key === 'Enter') {
        this.handleKeypressForEnter(e, options)
      }

      if (e.key === 'Escape' || e.key === 'Tab') {
        e.preventDefault()
        this.setState({
          optionsOpen: false,
        })
        this.handleBlur()
      }
    }
  }

  handleKeypressForArrows = (e, direction, options) => {
    e.preventDefault()
    const initialIndex = () => (direction === 'down' ?
      (this.selected.current && Number(this.selected.current.getAttribute('data-index'))) || 0 :
      (this.selected.current && Number(this.selected.current.getAttribute('data-index'))) || options.length - 1
    )
    const nextIndex = direction === 'down' ?
      (this.state.optionHighlight + 1) % options.length :
      (this.state.optionHighlight - 1 + options.length) % options.length
    if (this.state.optionsOpen) {
      this.setState(state => ({
        optionHighlight: state.optionHighlight === null ? initialIndex() : nextIndex,
        optionsOpen: true,
      }), () => {
        this.setState(state => ({
          value: options[state.optionHighlight].value,
        }))
      })
    } else {
      this.getFilteredOptions()
      this.setState({
        optionHighlight: initialIndex(),
        optionsOpen: true,
      }, () => {
        this.setState(state => ({
          value: options[state.optionHighlight].value,
        }))
      })
    }
    this.mapInputToOption()
  }

  handleKeypressForEnter = (e, options) => {
    e.preventDefault()
    if (this.state.optionsOpen) {
      if (this.state.value === '') {
        this.setState({
          optionHighlight: null,
          optionsOpen: false,
          value: '',
        })
      } else if (options.length > 0) {
        this.setState(state => ({
          optionHighlight: null,
          optionsOpen: false,
          value: options[state.optionHighlight].value,
        }))
      }
      this.storeNewOption()
      this.getFilteredOptions()
      this.mapInputToOption()
    } else {
      this.handleBlur()
    }
  }

  handleNewOption = (options, inputValue) => {
    if (this.state.value !== '') {
      const newOption = inputValue.charAt(0).toUpperCase() + inputValue.slice(1)
      options.push({
        label: newOption,
        value: newOption,
      })
    }
    return options
  }

  handleOptionSelect = (e) => {
    e.preventDefault()
    const value = e.target.getAttribute('data-value')
    this.setState({
      optionHighlight: null,
      optionsOpen: false,
      value,
    }, () => {
      this.storeNewOption()
      this.getFilteredOptions()
      this.handleFocus()
      this.mapInputToOption()
    })
  }

  handleOptionsToggle = (e) => {
    e.preventDefault()
    e.stopPropagation()
    if (e.target.tagName !== 'LI') {
      this.setState(state => ({
        options: this.sortOptions(),
        optionsOpen: !state.optionsOpen,
      }), () => {
        this.handleFocus()
      })
    }
  }

  isRequired = () => {
    if (this.props.required) {
      if (this.state.value.length === 0) {
        return true
      }
      if (
        !this.props.allowCustom &&
        !this.props.options.filter(option => option.label === this.state.value).length > 0
      ) {
        return true
      }
    }
    return false
  }

  isValidInput = () => {
    if (this.props.allowCustom) {
      return true
    }
    if (this.state.value.length > 0 && this.state.options.length === 0) {
      return false
    }
    return true
  }

  mapInputToOption = () => {
    const selected = find(this.state.options, option => option.value === this.state.value)
    if (!selected) {
      this.setState({
        label: '',
        selected: null,
      })
    } else {
      this.setState({
        label: selected.label || selected.value || '',
        selected,
      })
    }
  }

  sortOptions = () => {
    if (typeof this.props.sortOptions === 'function') {
      return [...this.props.options, ...this.state.addedOptions].sort(this.props.sortOptions)
    } else if (this.props.sortOptions) {
      return [...this.props.options, ...this.state.addedOptions].sort((a, b) => a.label.localeCompare(b.label))
    }
    return [...this.props.options, ...this.state.addedOptions]
  }


  storeNewOption = () => {
    if (this.props.allowCustom) {
      if (!find(this.sortOptions(), option => option.value === this.state.value) && this.state.value !== '') {
        const newOption = this.state.value.charAt(0).toUpperCase() + this.state.value.slice(1)
        const addedOptions = this.state.addedOptions.slice(0)
        addedOptions.push({
          label: newOption,
          value: newOption,
        })
        this.setState({
          addedOptions,
        })
      }
    }
    return null
  }

  render() {
    const labelSwitch = this.state.label === '' || (this.props.searchable && this.state.isInFocus)

    return (
      <div
        className={[
          'component-select',
          (this.props.searchable ? 'searchable' : null),
          (this.state.isDisabled ? 'disabled' : null),
          (this.isRequired() ? 'required' : null),
          (this.state.isInFocus ? 'input-has-focus' : null),
          (this.state.isValid ? null : 'input-is-invalid'),
        ].filter(el => el != null).join(' ')}
        onClick={(!this.state.isDisabled && !this.props.searchable) ? this.handleOptionsToggle : null}
        ref={this.select}
      >
        <p
          className="selection-label"
          onClick={this.handleFocus}
          style={{
            opacity: labelSwitch ? 0 : 1,
          }}
        >
          {this.state.label}
        </p>
        <input
          disabled={this.state.isDisabled || !this.props.searchable}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          placeholder={this.props.placeholder}
          ref={this.input}
          required={this.props.required || null}
          style={{
            opacity: labelSwitch ? 1 : 0,
          }}
          type="text"
          value={this.state.value}
        />
        { this.props.clearButton &&
          <FontAwesomeButton
            className="clear-value"
            disabled={this.state.isDisabled}
            icon="fa-times-circle"
            onClick={this.handleClearInput}
            size="small"
            style={{
              opacity: this.state.value.length > 0 ? 1 : 0,
            }}
          />}
        <FontAwesomeButton
          className="toggle-options"
          disabled={this.state.isDisabled}
          icon={this.state.optionsOpen ? 'fa-chevron-up' : 'fa-chevron-down'}
          onClick={this.handleOptionsToggle}
        />
        { this.state.options &&
          <ul
            className="component-select-options"
            style={{
              display: this.state.optionsOpen ? 'block' : 'none',
            }}
          >
            { this.state.options.map((option, i) => (
              <li
                className={[
                  (this.state.optionHighlight === i ? 'highlight' : null),
                  (this.state.selected && this.state.selected.value === option.value ? 'selected' : null),
                ].filter(el => el != null).join(' ')}
                data-index={i}
                data-value={option.value}
                key={option.value}
                onClick={this.handleOptionSelect}
                ref={this.state.selected && this.state.selected.value === option.value ? this.selected : null}
              >
                {option.label}
              </li>
            ))}
            { this.state.options.length === 0 &&
              <li className="no-matches">No matches ...</li>}
          </ul>}
      </div>
    )
  }
}


export default Select
