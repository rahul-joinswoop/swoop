import React from 'react'
import { mount, shallow } from 'enzyme'
import Select from '.'

describe('<Select /> Component', () => {
  it('Renders and invokes `componentDidMount` when mounted', () => {
    jest.spyOn(Select.prototype, 'componentDidMount')
    const component = shallow(<Select />)
    expect(component).toBeDefined()
    expect(Select.prototype.componentDidMount).toHaveBeenCalled()
    Select.prototype.componentDidMount.mockRestore()
  })

  it('Fires isRequired on componentDidMount', () => {
    const component = shallow(<Select />)
    const instance = component.instance()
    jest.spyOn(instance, 'isRequired')
    instance.componentDidMount()
    expect(instance.isRequired).toHaveBeenCalled()
  })

  it('autoFocus prop works', () => {
    const component = mount(<Select autoFocus />)
    const instance = component.instance()
    jest.spyOn(instance, 'handleFocus')
    instance.componentDidMount()
    expect(instance.handleFocus).toHaveBeenCalled()
  })

  it('Populates options from props', () => {
    const component = mount(<Select
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
        { value: 'Pangolin', label: 'Pangolin' },
      ]}
    />)
    expect(component.find('li[data-value="Aardvark"]').exists()).toEqual(true)
    expect(component.find('li[data-value="Pangolin"]').exists()).toEqual(true)
    expect(component.find('li[data-value="Panther"]').exists()).toEqual(false)
    expect(component.find('li[data-value]')).to.have.lengthOf(2)
  })

  it('Takes a default value via props', () => {
    const component = mount(<Select
      default="Pangolin"
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
        { value: 'Pangolin', label: 'Pangolin' },
      ]}
    />)
    expect(component.state().value).toEqual('Pangolin')
  })

  it('Takes an onChange callback', () => {
    const testFunction = jest.fn()
    const component = mount(<Select
      onChange={testFunction}
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
        { value: 'Pangolin', label: 'Pangolin' },
      ]}
    />)
    const input = component.find('input')
    input.simulate('focus')
    input.simulate('change', { target: { value: 'Aardvark' } })
    expect(component.state().value).toEqual('Aardvark')
    expect(testFunction).toHaveBeenCalled()
  })

  it('Clicking the Toggle-options button flips state.optionsOpen', () => {
    const component = mount(<Select
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
      ]}
    />)
    expect(component.state().optionsOpen).toEqual(false)
    component.find('button.toggle-options').simulate('click')
    expect(component.state().optionsOpen).toEqual(true)
  })

  it('Clicking component does not open options when `searchable`', () => {
    const component = mount(<Select
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
      ]}
      searchable
    />)
    expect(component.state().optionsOpen).toEqual(false)
    component.find('div.component-select').simulate('click')
    expect(component.state().optionsOpen).toEqual(false)
  })

  it('Clicking component opens options when not `searchable`', () => {
    const component = mount(<Select
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
      ]}
    />)
    expect(component.state().optionsOpen).toEqual(false)
    component.find('div.component-select').simulate('click')
    expect(component.state().optionsOpen).toEqual(true)
  })

  it('<Select /> search and select works', () => {
    const component = mount(<Select
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
        { value: 'Pangolin', label: 'Pangolin' },
      ]}
      searchable
    />)
    const instance = component.instance()
    jest.spyOn(instance, 'getFilteredOptions')
    jest.spyOn(instance, 'handleOptionSelect')
    jest.spyOn(instance, 'isValidInput')
    jest.spyOn(instance, 'sortOptions')
    instance.componentDidMount()

    const input = component.find('input')
    input.simulate('focus')
    input.simulate('change', { target: { value: 'Aardvark' } })
    expect(component.state().value).toEqual('Aardvark')
    expect(instance.getFilteredOptions).toHaveBeenCalled()
    expect(instance.isValidInput).toHaveBeenCalled()
    expect(instance.sortOptions).toHaveBeenCalled()
    expect(component.find('li[data-value]')).to.have.lengthOf(1)
    component.find('li[data-value="Aardvark"]').simulate('click')
    expect(instance.handleOptionSelect).toHaveBeenCalled()
  })

  it('<Select /> filters options and allows custom input', () => {
    const component = mount(<Select
      allowCustom
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
        { value: 'Pangolin', label: 'Pangolin' },
      ]}
      searchable
    />)
    const instance = component.instance()
    jest.spyOn(instance, 'handleNewOption')
    jest.spyOn(instance, 'handleOptionSelect')
    jest.spyOn(instance, 'storeNewOption')
    instance.componentDidMount()

    const input = component.find('input')
    input.simulate('focus')
    input.simulate('change', { target: { value: 'Pan' } })
    expect(component.state().value).toEqual('Pan')
    expect(component.find('li[data-value]')).to.have.lengthOf(2)
    input.simulate('change', { target: { value: 'Panda' } })
    expect(component.state().value).toEqual('Panda')
    expect(instance.handleNewOption).toHaveBeenCalled()
    expect(component.find('li[data-value]')).to.have.lengthOf(1)
    component.find('li[data-value="Panda"]').simulate('click')
    expect(instance.handleOptionSelect).toHaveBeenCalled()
    expect(instance.storeNewOption).toHaveBeenCalled()
  })

  it('<Select /> handles keyboard input', () => {
    const map = {}
    document.addEventListener = jest.fn((event, cb) => {
      map[event] = cb
    })

    const component = mount(<Select
      allowCustom
      options={[
        { value: 'Aardvark', label: 'Aardvark' },
        { value: 'Pangolin', label: 'Pangolin' },
        { value: 'Sika Deer', label: 'Sika Deer' },
      ]}
    />)
    const input = component.find('input')

    expect(component.state().optionsOpen).toEqual(false)
    expect(component.state().isInFocus).toEqual(false)
    input.simulate('focus')
    expect(component.state().isInFocus).toEqual(true)

    map.keyup({
      key: 'ArrowDown',
      preventDefault: () => {},
    })
    expect(component.state().optionsOpen).toEqual(true)
    expect(component.state().optionHighlight).toEqual(0)
    map.keyup({
      key: 'ArrowDown',
      preventDefault: () => {},
    })
    map.keyup({
      key: 'ArrowDown',
      preventDefault: () => {},
    })
    expect(component.state().optionHighlight).toEqual(2)
    map.keyup({
      key: 'ArrowUp',
      preventDefault: () => {},
    })
    expect(component.state().optionHighlight).toEqual(1)
    map.keyup({
      key: 'Enter',
      preventDefault: () => {},
    })
    expect(component.state().value).toEqual('Pangolin')
    map.keyup({
      key: 'ArrowUp',
      preventDefault: () => {},
    })

    map.keyup({
      key: 'Escape',
      preventDefault: () => {},
    })
    expect(component.state().isInFocus).toEqual(false)
  })
})
