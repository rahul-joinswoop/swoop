/*
 * decaffeinate suggestions:
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'
import PropTypes from 'prop-types'
import { clone, partial, union, uniq } from 'lodash'
import BaseComponent from 'components/base_component'
import classNames from 'classnames'
import onClickOutside from 'react-onclickoutside'
import './style.scss'

const colors = [null, '#FF7171', '#FCC37F', '#FDEB8F', '#9BE192', '#ADB8FE', '#E0B7FB']

class ColorSelector extends BaseComponent {
  static initClass() {
    this.prototype.displayName = 'ColorSelector'
  }

  constructor(props) {
    super(props)
    this.handleClickOutside = this.handleClickOutside.bind(this)
    this.state = {
      show: false,
    }
  }

  handleClickOutside() {
    this.setState({ show: false })
  }

  handleToggle = () => this.setState(({ show }) => ({ show: !show }))

  render() {
    super.render()
    let localColors = clone(colors) // clone so we don't mess with original array

    if (this.props.additionalColors) {
      localColors = union(localColors, this.props.additionalColors)
    }

    if (this.props.color && !colors.includes(this.props.color) && this.props.color !== 'transparent') {
      // insert into the second index the existing color so we don't mess up 'no Color' at top
      localColors.splice(1, 0, this.props.color)
    }

    const classes = classNames('geosuggest', 'ColorSelector', this.props.className)
    return (
      /* eslint-disable-next-line */
      <div
        className={classes}
        onClick={this.handleToggle}
        style={{
          ...this.props.style,
          backgroundColor: this.props.color,
        }}
      >
        {(!this.props.color || this.props.color === 'transparent') && 'No Color'}
        <ul
          className={`ColorSelector-list geosuggest__suggests autosuggest_list${
            !this.state.show ? ' geosuggest__suggests--hidden' : ''
          }`}
          style={{
            height: this.props.maxHeight || 212,
            maxHeight: this.state.show ? this.props.maxHeight || 212 : 0,
          }}
        >
          {uniq(localColors).map(color => (
            /* eslint-disable-next-line */
            <li
              className="geosuggest-item"
              key={color}
              onClick={partial(this.props.setColor, color)}
              style={{ backgroundColor: color }}
            >
              {!color && 'No Color'}
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

ColorSelector.initClass()

ColorSelector.propTypes = {
  additionalColors: PropTypes.array,
  color: PropTypes.string,
  maxHeight: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  setColor: PropTypes.func.isRequired,
  style: PropTypes.object,
}

export default onClickOutside(ColorSelector)
