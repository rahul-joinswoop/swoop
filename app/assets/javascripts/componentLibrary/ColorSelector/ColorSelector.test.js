import React from 'react'
import { mount } from 'enzyme'
import ColorSelector from '.'

describe('ColorSelector component', () => {
  it('shows/hides list of colors', () => {
    const mountNode = document.createElement('div')
    document.body.appendChild(mountNode)

    const outerNode = document.createElement('div')
    document.body.appendChild(outerNode)

    const emptyFunc = () => {}
    const wrapper = mount(<ColorSelector setColor={emptyFunc} />, { attachTo: mountNode })

    expect(wrapper.exists('.geosuggest__suggests--hidden')).toBe(true)
    expect(wrapper.find('ColorSelector').state('show')).toBe(false)

    wrapper.simulate('click')

    expect(wrapper.exists('.geosuggest__suggests--hidden')).toBe(false)
    expect(wrapper.find('ColorSelector').state('show')).toBe(true)

    // trigger click outside of component
    outerNode.dispatchEvent(new Event('mousedown', { bubbles: true }))
    // this is required because of https://github.com/airbnb/enzyme/blob/master/docs/guides/migration-from-2-to-3.md#for-mount-updates-are-sometimes-required-when-they-werent-before
    // https://github.com/airbnb/enzyme/issues/1153
    wrapper.update()

    expect(wrapper.exists('.geosuggest__suggests--hidden')).toBe(true)
    expect(wrapper.find('ColorSelector').state('show')).toBe(false)
  })

  it('selects color', () => {
    const props = {
      additionalColors: ['red', 'green', 'blue'],
      color: 'red',
      setColor: (color) => {
        expect(color).toBe('blue')
      },
    }

    const wrapper = mount(<ColorSelector {...props} />)

    expect(wrapper.find('.ColorSelector').prop('style')).toHaveProperty('backgroundColor', 'red')

    wrapper.find({ style: { backgroundColor: 'blue' } }).simulate('click')

    props.color = 'blue'
    wrapper.setProps(props)

    expect(wrapper.find('.ColorSelector').prop('style')).toHaveProperty('backgroundColor', 'blue')
  })
})
