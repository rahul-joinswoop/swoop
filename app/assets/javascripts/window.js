import ReactDOMFactories from 'react-dom-factories'

// yuck yuck yuck - jquery has to be loaded this way and set globally so that
// the other jquery libs don't explode
import $ from 'jquery'

// polyfill for Element.prototype.(scroll, scrollTo, scrollBy, and scrollIntoView)
if (!('scrollBehavior' in document.documentElement.style)) {
  import('scroll-behavior-polyfill')
}

window.$ = $
window.jQuery = $

require('jquery-validation')
require('bootstrap-sass')
require('backbone')

// end jquery hackery

window.Tracking = require('tracking').default // We should strive to remove these

const tags = [
  'del',
  'div',
  'table',
  'span',
  'strong',
  'textarea',
  'thead',
  'tbody',
  'tr',
  'th',
  'td',
  'input',
  'h1',
  'h2',
  'h3',
  'h4',
  'hr',
  'form',
  'label',
  'select',
  'option',
  'button',
  'aside',
  'ul',
  'li',
  'footer',
  'header',
  'img',
  'nav',
  'br',
]

tags.forEach(tag => window[tag] = ReactDOMFactories[tag])
