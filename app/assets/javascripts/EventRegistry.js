import MicroEvent from 'microevent-github'

import { isEmpty, keys, uniqBy } from 'lodash'

class EventRegistry {
  constructor() {
    this._watchedEvents = {}
    this._watcherToEvents = {}
    this._pendingTriggerIds = []
    this._triggerTimeout = null
  }

  register(watcherId, modelId, listId = null, objId = null, property = null) {
    if (window.logging?.showEventRegisters) {
      console.log(watcherId, modelId, listId, objId, property)
    }

    if (watcherId != null) {
      const eventKey = [modelId, listId, objId, property, watcherId]

      this._createNestedMap(...[...(eventKey || [])])

      if (this._watcherToEvents[watcherId] == null) {
        this._watcherToEvents[watcherId] = []
      }

      return this._watcherToEvents[watcherId].push(eventKey)
    }
  }

  triggerRerender(watcherId) {
    return this.trigger(this.getTrigger(watcherId))
  }

  sendPendingTriggers = () => {
    const uniqTriggers = uniqBy(this._pendingTriggerIds, s => `${s}`)
    uniqTriggers.map(id => this.triggerRerender(id))
    return (this._pendingTriggerIds = [])
  }

  getTrigger = watcherId => `RERENDER_${watcherId}`

  triggerListeners = (modelId, listId = null, objId = null, property = null) => {
    // Queue up rerender calls but wait for them to come in to uniquify
    const watcherIds = this._getKeys(modelId, listId, objId, property)

    if (window.logging?.showEventTriggers) {
      console.log(modelId, listId, objId, property, watcherIds)
    }

    watcherIds.map(id => this._pendingTriggerIds.push(id))
    return this.checkPendingTriggers()
  }

  addPendingTriggerId(id) {
    this._pendingTriggerIds.push(id)
    return this.checkPendingTriggers()
  }

  checkPendingTriggers() {
    if (this._triggerTimeout) {
      clearTimeout(this._triggerTimeout)
    }
    if (this._pendingTriggerIds.length >= 1000) {
      return this.sendPendingTriggers()
    } else {
      return (this._triggerTimeout = setTimeout(this.sendPendingTriggers, 20))
    }
  }


  triggerPropListeners(modelId, listId, objId, property) {
    return this.triggerListeners(modelId, listId, objId, property)
  }

  triggerObjListeners(modelId, listId, objId) {
    return this.triggerListeners(modelId, listId, objId, null)
  }

  triggerListListeners(modelId, listId) {
    return this.triggerListeners(modelId, listId, null, null)
  }

  getWatchedProperties(modelId, listId, objId) {
    const idPropertyWatchers = this._getKeys(modelId, listId, objId)

    const generalPropertyWatchers = this._getKeys(modelId, listId, null)

    const watchedProps = idPropertyWatchers.concat(generalPropertyWatchers)
    return watchedProps
  }

  clearListeners(watcherId) {
    if (window.logging?.showEventClears) {
      console.log(watcherId)
    }

    if (watcherId != null) {
      if (this._watcherToEvents[watcherId]) {
        this._watcherToEvents[watcherId].forEach((keys) => {
          let listId, modelId, objId, prop;
          // TODO: this if statement shouldn't be needed, figure outwhy [prop]
          // was sometimes returning null
          [modelId, listId, objId, prop, watcherId] = [...keys]

          if (this._watchedEvents[modelId]?.[listId]?.[objId]?.[prop]?.[watcherId]) {
            delete this._watchedEvents[modelId][listId][objId][prop][watcherId]

            if (isEmpty(this._watchedEvents[modelId][listId][objId][prop])) {
              delete this._watchedEvents[modelId][listId][objId][prop]
            }

            if (isEmpty(this._watchedEvents[modelId][listId][objId])) {
              delete this._watchedEvents[modelId][listId][objId]
            }
          }
        })
      }

      return delete this._watcherToEvents[watcherId]
    }
  }

  _getKeys(...args) {
    let base = this._watchedEvents

    for (const arg of args) {
      if (!base[arg]) {
        return []
      }

      base = base[arg]
    }

    return keys(base)
  }

  _createNestedMap(...args) {
    let base = this._watchedEvents

    for (const arg of args) {
      if (!base[arg]) {
        base[arg] = {}
      }

      base = base[arg]
    }

    return base
  }
}

MicroEvent.mixin(EventRegistry)
export default new EventRegistry()
