import Utils from 'utils'

import React from 'react'
JobStore = require('stores/job_store')
UserStore = require('stores/user_store')
import Consts from 'consts'
Countdown = React.createFactory(require 'components/countdown')
BaseComponent = require('components/base_component')
AuctionStore = require('stores/auction_store').default
DashboardETATime = React.createFactory(require('components/dashboard_eta_time'))

class FleetEta extends BaseComponent
  render: ->
    super(arguments...)

    #TODO: split this out into the individual properties instead of all job changes for this object
    job = JobStore.getById(@props.job_id, false, @getId())
    if UserStore.isSwoop() && AuctionStore.jobHasLiveAuction(job)
      return 'Collecting'
    else if UserStore.isSwoop() && AuctionStore.jobHasUnsuccessfulAuction(job) && not Utils.isScheduled(UserStore, job) &&
      (job?.status == Consts.AUTO_ASSIGNING || job?.status == Consts.PENDING) && !AuctionStore.getAuctionByJob(job)?.manual_dttm
        return 'Failed'

    if @props.enableDashboardFormatting and job?.scheduled_for
      DashboardETATime
        jobId: @props.job_id
    else
      ret = Utils.getLastEtaObj(job, false)

      if job?.status in [Consts.CREATED, Consts.PENDING, Consts.REJECTED, Consts.UNASSIGNED, Consts.REASSIGN, Consts.ASSIGNED, Consts.ACCEPTED, Consts.SUBMITTED, Consts.DISPATCHED, Consts.ENROUTE]
        if ret
          return span null,
            if @props.showWrapper
              span null,
                " ("
            Countdown
              time: ret
              actualEta: Utils.isETA(job)
            if ret and job?.toa.original and !moment(job.toa.original).isSame(ret) and !Utils.isDoubleOverdue(job)
              span null,
                span null,
                  " ("
                Countdown
                  time: moment(job?.toa.original)
                  actualEta: false
                span null,
                  ")"+(if job?.toa.count > 2 then "^" + (job.toa.count - 1) else "")
            if @props.showWrapper
              span null,
                " ETA)"

      else
        if @props.showWrapper
          return null
        return "--"

      return null
module.exports = FleetEta
