import React from 'react'
import createReactClass from 'create-react-class'
import $ from 'jquery'
import Logger from 'lib/swoop/logger'
GeosuggestItem = React.createFactory(require('components/geosuggest/GeosuggestItem'))
UserStore = require('stores/user_store')
MapStore = require('stores/map_store')
import Utils from 'utils'
PoiStore = require('stores/poi_store').default
SiteStore = require('stores/site_store').default
BaseComponent = require('components/base_component')
import { debounce, extend, filter } from 'lodash'
onClickOutside = require('react-onclickoutside').default
LocationTypeStore = require('stores/location_type_store')

SwoopMapClass=createReactClass(
  boundSet: false
  bounds: null
  center: null
  zoom: null
  map: null
  marker: null
  directions: null
  infoWindow: null
  truckMarker: null
  trackChanges: false
  lookupTable: {}
  componentDidMount: ->
    #TODO: figure out why i have to wait to set this up
    setTimeout () =>
      @map = @createMap(@props)
      @marker = @createMarker()
      @updateMapComponents(@props)
      @createGeocoder()
      #@marker = @createMarker()
      #@infoWindow = @createInfoWindow()

      google.maps.event.addListener(@map, "click", (event) =>
          @geocodeLatLng(event.latLng.lat(), event.latLng.lng())
      )
    , 500


    # have to define google maps event listeners here too
    # because we can't add listeners on the map until its created
    #google.maps.event.addListener @map, 'zoom_changed', => @handleZoomChange()
    #google.maps.event.addListener @map, 'dragend', => @handleDragEnd()

  createGeocoder: () ->
    if not @geocoder
      @geocoder = new google.maps.Geocoder()

  geocodeLatLng: (lat, lng) ->
    latlng = {lat: lat, lng: lng}
    if @props.setLocation?
      if @props.loadingCallback?
        @props.loadingCallback()
      @geocoder.geocode({'location': latlng}, (results, status) =>
        if @props.doneLoadingCallback?
          @props.doneLoadingCallback()

        if (status == google.maps.GeocoderStatus.OK)
          if (results[0])
            @props.setLocation(lat, lng, results[0].formatted_address, results[0].place_id)
          else
            @props.setLocation(lat, lng)
        else
          @props.setLocation(lat, lng)

      )


  updateMapComponents: (props) ->
    @updateMarkers(props)
    #make sure to do this right if no props
    if props.lat? and props.lng? and (!@props? or (@props.lat != props.lat or @props.lng != @props.lng))
      @updateMapBounds(props)
    else if props.address? and (!@props? or @props.address != props.address)
      @centerAtLocation(props.address)

  setZoom: (zoom) ->
    @map?.setZoom(zoom)

  recenter: () ->
    if @props.lat? and @props.lng?
      @updateMapBounds(@props)
    else if @props.address?
      @centerAtLocation(@props.address)
  removeMapComponents: (props) ->
    if @marker?
      @marker.setMap(null)

    @marker = null

  UNSAFE_componentWillReceiveProps: (props) ->
    @updateMapComponents(props)


  componentWillUnmount: ->
    #This was throwing an error
    #google.maps.event.clearListeners(@map, "click")
    @removeMapComponents()

  updateMapBounds: (props) ->
    if @map? and props.lat? and props.lng?
      @map.panTo(@getLocation(props))

  updateMarkers: (props) ->
    if @marker?
      if props.lat? and props.lng?
        @marker.setPosition(@getLocation(props))
        @marker.setVisible(true)
      else
        @marker.setVisible(false)

  getLocation: (props) ->
      if props?.lat? and props?.lng?
        origin=new google.maps.LatLng(props.lat,props.lng)
      return origin


  centerAtLocation: (address) ->
    @createGeocoder()

    if not address? || address == ""
      return

    if @lookupTable[address]?
      @updateMapBounds
        lat: @lookupTable[address][0]
        lng: @lookupTable[address][1]
      return

    if @props.loadingCallback?
      @props.loadingCallback()
    @geocoder.geocode { address: address, region: "US"}, ((results, status) =>
      if @props.doneLoadingCallback?
        @props.doneLoadingCallback()

      if status != google.maps.GeocoderStatus.OK
        Logger.debug("got bad return status ")
        return
      location = results[0].geometry.location
      @lookupTable[address] = [location.lat(), location.lng()]
      @updateMapBounds
        lat: location.lat()
        lng: location.lng()
    )

  createMap: (props) ->
    coords = @props.coords


    mapOptions =
      zoom: 17
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false
      styles: [{ featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }]},
               { featureType: "transit", elementType: "labels", stylers: [{ visibility: "off" }]}]

    if props.centerLat? and props.centerLng?
      mapOptions.center =
        lat: props.centerLat
        lng: props.centerLng
    else
      mapOptions.center =
        lat: 37.7577
        lng: -122.4376

    if not (props.lat? and props.lng?)
      mapOptions.zoom = 15

    if !props.lat? and !props.lng?
      @centerAtLocation(props.address)

    if @mapCanvas
      theMap = MapStore.stealMap(@mapCanvas, mapOptions)

    return theMap

  createMarker: () ->
    marker = MapStore.createMarker
      key: "marker",
      defaultAnimation: 2
      map: @map

    return marker

  render: ->
    div
      className: "GMap"
      div
        ref: (ref) => this.mapCanvas = ref
)

SwoopMap = React.createFactory(SwoopMapClass)


export class Geosuggest extends BaseComponent
  displayName: 'Geosuggest'
  lastSelect: null
  loading: false
  constructor: (props) ->
    super(props)
    @state = state =
      isSuggestsHidden: true
      show_map: false
      userInput: null
      placeLat: null
      placeLng: null
      mapAddress: null
      activeSuggest: null
      suggests: []
      state: ""


    if @props.initialValue? and @props.initialValue.length > 0
      state.userInput = @props.initialValue
      state.mapAddress =  @props.initialValue
    if @props.initialLat? and @props.initialLat != 0
      state.placeLat = @props.initialLat
    if @props.initialLng? and @props.initialLng != 0
      state.placeLng = @props.initialLng

  @defaultProps = {
      fixtures: []
      initialValue: ''
      placeholder: 'Search places'
      disabled: false
      className: ''
      location: null
      radius: null
      bounds: null
      country: null
      types: null
      googleMaps: null
      placeLat: null
      placeLng: null
      onSuggestSelect: ->
      onFocus: ->
      onBlur: ->
      onChange: ->
      skipSuggest: ->
      getSuggestLabel: (suggest) ->
        suggest.description
      autoActivateFirstSuggest: false
    }


  UNSAFE_componentWillReceiveProps: (props) ->
    super(props)
    if @props.initialValue != props.initialValue and !@state.userChange
      @setState
        userInput: props.initialValue
        userChange: false
    else
      @setState
        userChange: false

    return
  componentDidMount: ->
    super(arguments...)
    @setInputValue @props.initialValue

    if @state.placeLat? and @state.placeLng? and @state.placeLat != 0 and @state.placeLng != 0
      Logger.debug("setting state to complete", @state.userInput, @state.placeLat)
      @setState
        status: "complete"

    googleMaps = @props.googleMaps or google and google.maps or @googleMaps
    if !googleMaps
      Logger.error 'Google map api was not found in the page.'
    else
      @googleMaps = googleMaps
    @autocompleteService = new (googleMaps.places.AutocompleteService)(null, types: [ 'geocode' ])
    @placeServices = new (google.maps.places.PlacesService)($("<div></div>")[0])
    @geocoder = new (googleMaps.Geocoder)()
    return
  setInputValue: (value) =>
    @setState userInput: value
    return
  onInputChange: =>
    userInput = @refs.geosuggestInput.value

    if userInput != @state.userInput

      @setState { userInput: userInput, userChange: true, status: "", placeLat: null, placeLng: null }, (->
        @showSuggests()
        @props.onChange userInput
        if @props.send_all_addresses
            suggest =
              label: userInput
              exact: true
              location:
                lat: null
                lng: null

            @props.onSuggestSelect suggest
        return
      ).bind(this)
    return
  onFocus: =>
    @props.onFocus()
    @showSuggests()
    return

  clear: =>
    @setState { userInput: '' }, (=>
      @hideSuggests()
      return
    )
    return

  searchSuggests: debounce(->
    if !@state.userInput
      @updateSuggests()
      return

    if @state.userInput?.length < 3
      return

    options = input: @state.userInput
    if @props.location
      options.location = @props.location
    if @props.radius
      options.radius = @props.radius
    else
      options.radius = 0
    if @props.bounds
      options.bounds = @props.bounds
    if @props.types
      options.types = @props.types
    if @props.country
      options.componentRestrictions = country: @props.country
    #if !options.types?
    #  options.types = ["geocode", "address", "establishment"]
    @autocompleteService.getPlacePredictions options, ((suggestsGoogle) ->
      @updateSuggests suggestsGoogle
      if @props.autoActivateFirstSuggest
        @activateSuggest 'next'
      return
    ).bind(this)
    return
  , 500)


  updateSuggests: (suggestsGoogle) =>
    _this = this
    if !suggestsGoogle
      suggestsGoogle = []
    suggests = []
    regex = new RegExp(Utils.escapeRegExp(@state.userInput), 'gim')
    skipSuggest = @props.skipSuggest
    @props.fixtures.forEach (suggest) ->
      if !skipSuggest(suggest) and suggest.label.match(regex)
        suggest.placeId = suggest.label
        suggests.push suggest
      return

    suggestsGoogle.forEach (suggest) ->
      if !skipSuggest(suggest)
        suggests.push
          label: Utils.shortenAddress(_this.props.getSuggestLabel(suggest))
          placeId: suggest.place_id
          reference: suggest.reference
          suggest: suggest

      return
    @setState suggests: suggests
    return
  showSuggests: =>
    @searchSuggests()
    @showMap()
    @setState
      isSuggestsHidden: false
    return

  hideSuggests: =>
    @props.onBlur()
    setTimeout (=>
      @setState isSuggestsHidden: true
      return
    ), 100
    return
  onInputKeyDown: (event) =>
    switch event.which
      when 40
        # DOWN
        event.preventDefault()
        @activateSuggest 'next'
      when 38
        # UP
        event.preventDefault()
        @activateSuggest 'prev'
      when 13
        # ENTER
        event.preventDefault()

        if @inputValueIsLatLng()
          @reverseGeocode()
        else
          @selectSuggest @state.activeSuggest, true
          if @refs.map
            @refs.map?.setZoom(15)
          #  @refs.map.recenter()
      when 9
        # TAB
        @selectSuggest @state.activeSuggest
        @closeMap()

      when 27
        # ESC
        @hideSuggests()

        break
    return
  activateSuggest: (direction) =>
    if @state.isSuggestsHidden
      @showSuggests()
      return
    suggestsCount = @state.suggests.length - 1
    next = direction == 'next'
    newActiveSuggest = null
    newIndex = 0
    i = 0
    # eslint-disable-line id-length
    i
    while i <= suggestsCount
      if @state.suggests[i] == @state.activeSuggest
        newIndex = if next then i + 1 else i - 1
      i++
    if !@state.activeSuggest
      newIndex = if next then 0 else suggestsCount
    if newIndex >= 0 and newIndex <= suggestsCount
      newActiveSuggest = @state.suggests[newIndex]
    @setState activeSuggest: newActiveSuggest
    return


  getDetails: (reference, callback) =>
    @placeServices.getDetails({
      reference: reference
    }, callback)

  selectSuggest: (suggest, showMap, isFixedOption = false) =>
    #if !suggest and @state.suggests?.length > 0
    #  suggest = @state.suggests[0]

    if !suggest
      @handleClickOutside()
      if showMap
        @setState
          status: "loading"
          placeLat: null
          placeLng: null

        @geocoder.geocode { address: @state.userInput, region: "US", bounds: @props.bounds}, ((results, status) =>
          if (status == google.maps.GeocoderStatus.OK)
            @setExactLocation(results[0].geometry.location.lat(), results[0].geometry.location.lng(), results[0].formatted_address, results[0].place_id, showMap)
            if showMap
              @showMap()
            if @props?.callback_on_enter?
              @props?.callback_on_enter?(@state.suggests, @state.userInput, results[0].geometry)

          else
            if @state.suggests?.length > 0
              @selectSuggest(@state.suggests[0], showMap, isFixedOption)
            else
              @setState
               status: "failed"
           #@props?.callback_on_enter?(@state.suggests, @state.userInput, results[0].geometry)
        )

        @showMap()

      return

    if isFixedOption
      address = suggest.text
      if suggest.address
        address = suggest.address

      @setExactLocation suggest.location?.lat, suggest.location?.lng, address, suggest.location?.place_id, showMap, suggest
      if showMap
        @showMap()
    else
      @geocoder.geocode {'placeId': suggest.placeId}, (results, status) =>
        if (status == google.maps.GeocoderStatus.OK)
          @setExactLocation(results[0].geometry.location.lat(), results[0].geometry.location.lng(), suggest.label, suggest.placeId, showMap)
          if showMap
            @showMap()

    @setState
      isSuggestsHidden: true
      activeSuggest: null

    if isFixedOption
      address = suggest.text
      if suggest.address
        address = suggest.address

      @setState
        status: 'complete'
        placeLat: suggest.location.lat
        placeLng: suggest.location.lng
        userInput: address
        mapAddress: suggest.location.address
    else
      @setState
        status: "loading"
        placeLat: null
        placeLng: null
        userInput: suggest.label
        mapAddress: suggest.label

    return

  isLoading: =>
    return @state.status == "loading"

  clickedOff: (e) =>
    @reverseGeocode() if @inputValueIsLatLng()
    setTimeout (=>
      @hideSuggests()
    ), 100

  inputValueIsLatLng: () =>
    regex = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?), ?[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/
    regex.test(@state.userInput)

  reverseGeocode: =>
    parseCoord = (coord) -> parseFloat(coord.trim())

    [lat, lng] = @state.userInput.split(',')

    @setLatLng(parseCoord(lat), parseCoord(lng))

  showMap: =>
    if @state.show_map == false and @props.onMapOpen
      @props.onMapOpen()

    @setState
      show_map: true

  toggleMap: =>
    if !@state.show_map
      @showMap()
    else
      @closeMap()

  setExactLocation: (lat, lng, label, placeId, showMap, fixedOption) =>
    if not label?
      label = @state.userInput

    label = Utils.shortenAddress(label)

    suggest =
      label: label
      exact: true
      placeId: placeId
      location:
        lat: lat
        lng: lng

    @setState
        activeSuggest: null
        isSuggestsHidden: true
        userInput: label
        status: "complete"
        placeLat: lat
        placeLng: lng

    @props.onSuggestSelect suggest, showMap, fixedOption

  closeMap: =>
    @setState
      show_map: false

  handleClickOutside: (e) =>
    if @state.userInput? and @state.userInput.length > 0 and (@state.placeLat == null || @state.placeLng == null)
      @setState
        status: "failed"

    @closeMap()
    @hideSuggests()

  loadingCallback: =>
    if "loading" != @state.status
      setTimeout ( =>
        @setState
          status: "loading"
        return
      ), 0

  doneLoadingCallback: =>
    setTimeout (=>
      status = ""
      if @state.userInput? and @state.userInput.length > 0 and (@state.placeLat == null || @state.placeLng == null)
        status = "failed"
      else if @state.placeLat? and @state.placeLng? and @state.placeLat != 0 and @state.placeLng != 0
        status = "complete"
      if status != @state.status
          @setState
            status: status
          return
    ), 0

  filterMapResults: (results) =>
    filteredResults = []
    if @props.value?.location_type_id

      locType = LocationTypeStore.getLocationTypeById(@props.value.location_type_id)?.name

      if locType && locType in ['Highway']
        filteredResults = filter(results, (result) ->
          'route' in result.types
        )

      if locType && locType in ['Intersection']
        filteredResults = filter(results, (result) ->
          'bus_station' in result.types || 'transit_station' in result.types
        )
        if filteredResults.length > 0
          filteredResults = filter(results, (result) ->
            result.geometry.location_type == 'GEOMETRIC_CENTER'
          )

      if locType && locType in ['Point of Interest']
        filteredResults = filter(results, (result) ->
          'point_of_interest' in result.types
        )

    if filteredResults.length > 0
      results = filteredResults

    return results

  setLatLng: (lat, lng, fixedOption) =>
    @setState
      status: "loading"

    if not @geocoder
      @geocoder = new google.maps.Geocoder
    latlng = {lat: lat, lng: lng}

    @props.onSuggestSelect({ location: latlng, exact: true }, null, fixedOption)

    @geocoder.geocode({'location': latlng}, (results, status) =>
      if results? and results.length > 0

        results = @filterMapResults(results)

        @setState
          status: "complete"
          placeLat: lat
          placeLng: lng
          userInput: results[0].formatted_address

        if fixedOption?
          if Utils.isSiteAndNotPlace(SiteStore, fixedOption?.id)
            siteOrPlaceName = SiteStore.get(fixedOption.id, 'name', @getId())
            siteOrPlaceLocation = SiteStore.get(fixedOption.id, 'location', @getId())
          else
            siteOrPlaceName = PoiStore.get(fixedOption.id, 'name', @getId())
            siteOrPlaceLocation = PoiStore.get(fixedOption.id, 'location', @getId())

        @props.onSuggestSelect({
          gmaps: results[0]
          location: latlng
          placeId: results[0].place_id
          label: if fixedOption?.id? && siteOrPlaceName && siteOrPlaceLocation then Utils.formatAddressWithSite(siteOrPlaceName, siteOrPlaceLocation) else results[0].formatted_address
          exact: true
        })

      else
        @setState
          status: "failed"
    )

  render: ->
    super(arguments...)
    addr = @state.userInput
    if addr? and typeof addr == "string"
      addr = addr.replace(", United States", "")
      addr = addr.replace(", USA", "")

    if @props.formatInputValue and typeof @props.formatInputValue == 'function'
      addr = @props.formatInputValue(addr)

    mapProps =
      setLocation: @setExactLocation
      centerLat: @props.location?.lat()
      centerLng: @props.location?.lng()
      loadingCallback: @loadingCallback
      ref: "map"
      doneLoadingCallback: @doneLoadingCallback

    if @state.placeLat? and @state.placeLng?
      mapProps.lat = @state.placeLat
      mapProps.lng = @state.placeLng
      mapProps.centerLat = @state.placeLat
      mapProps.centerLng = @state.placeLng

    mapProps.address = @state.mapAddress

    showMap = (@state.show_map || @props.always_show_map)
    if @props.hide_map
      showMap = false

    if @props.fixedOptions and @props.fixedOptions.length
      if @state.userInput?.length && !@props.featureFlags?.recommendedDropOff
        fixedOptions = @props.fixedOptions.filter (option) =>
          text = option.text
          if (option.sortKey)
            text = option.sortKey
          if typeof text == 'string'
            text?.toLowerCase()?.indexOf(@state.userInput?.toLowerCase()) != -1
          else
            text
      else
        fixedOptions = @props.fixedOptions

    isFixedOptionsAvailable = fixedOptions and fixedOptions.length

    div
      className: 'geosuggest ' + @state.status + " " + @props.className + (if showMap == false then " nomap" else "")
      onClick: @props.onClick
      style: extend({}, { height: 34 }, @props.style)
      input
        className: 'geosuggest__input'
        ref: 'geosuggestInput'
        type: 'text'
        value: if addr? then addr else ''
        autoComplete: 'off'
        style:
          height: "calc(100%)"
          padding: "0px 6px"
        placeholder: @props.placeholder
        disabled: @props.disabled
        onKeyDown: @onInputKeyDown
        onChange: @onInputChange
        onFocus: @onFocus
        onBlur: @clickedOff
      if not @props.hideIndicator? || @props.hideIndicator == false
        div
          className: "geosuggest_status"
          # onClick: @toggleMap
      if showMap
        SwoopMap mapProps

      if (@state.suggests and @state.suggests.length > 0) or isFixedOptionsAvailable
        ul
          className: @getSuggestsClasses()
          if isFixedOptionsAvailable && @props.fixedOptionsLabel
              @renderOptionsLabel @props.fixedOptionsLabel
          if isFixedOptionsAvailable
            @getFixedItems fixedOptions
          if isFixedOptionsAvailable && @state.suggests and @state.suggests.length > 0
            @renderOptionsLabel 'GOOGLE AUTOFILL'
          @getSuggestItems()

  renderOptionsLabel: (label) ->
    li
      className: 'geosuggest-item title'
      label

  getFixedItems: (options) =>
    options.map (option) =>
      GeosuggestItem
        key: option.value
        suggest: option
        isActive: false
        onSuggestSelect: (option) =>
          @selectSuggest option, false, true
        renderItem: (option) ->
          option.text

  getSuggestItems: =>
    @state.suggests.map ((suggest) =>
      isActive = @state.activeSuggest and suggest.placeId == @state.activeSuggest.placeId
      GeosuggestItem
        key: suggest.placeId
        suggest: suggest
        isActive: isActive
        onSuggestSelect: @selectSuggest
        renderItem: @props.renderItem
    )
  getSuggestsClasses: =>
    classes = 'geosuggest__suggests'
    classes += if @state.isSuggestsHidden then ' geosuggest__suggests--hidden' else ''
    classes



export default onClickOutside(Geosuggest)
