import React from 'react'
import createReactClass from 'create-react-class'
GeosuggestItem = createReactClass(
  getDefaultProps: ->
    {
      isActive: false
      suggest: label: ''
      onSuggestSelect: ->

    }
  onClick: (event) ->
    event.preventDefault()
    event.stopPropagation()
    @props.onSuggestSelect @props.suggest, true
    return
  render: ->
    li
      key: @props.suggest.label
      className: @getSuggestClasses()
      onClick: @onClick
      if @props.renderItem?
        @props.renderItem(@props.suggest)
      else
        @props.suggest.label.replace(", United States", "")
  getSuggestClasses: ->
    className = @props.suggest.className
    classes = 'geosuggest-item'
    classes += if @props.isActive then ' geosuggest-item--active' else ''
    classes += if className then ' ' + className else ''
    classes
)
module.exports = GeosuggestItem
