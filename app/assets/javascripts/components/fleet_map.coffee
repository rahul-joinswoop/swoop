import React from 'react'
import ReactDOMServer from 'react-dom/server'
import Utils from 'utils'
import Consts from 'consts'
require('stylesheets/components/fleet_map.scss')
UserStore = require('stores/user_store')
JobStore = require('stores/job_store')
UsersStore = require('stores/users_store').default
CompanyStore = require('stores/company_store').default
SwoopMap=React.createFactory(require('components/rmap/map').default)
Marker=React.createFactory(require('components/rmap/marker'))
import TabsImport from 'components/tabs'
Tabs = React.createFactory(TabsImport)
JobListHelper = require('helpers/job_list')
BaseComponent = require('components/base_component')
import { partial, sortBy, without, pick, assign, includes, pull, concat, values } from 'lodash'
import { map, flow, flatMap } from 'lodash/fp'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
import SiteStore from 'stores/site_store'
import SiteMarkerImport from 'components/rmap/SiteMarker'
SiteMarker = React.createFactory(SiteMarkerImport)
Markers = React.createFactory(require('components/FleetMap/Markers').default)

JobCardAddress = React.createFactory (props) =>
  div
    className: "job_card_address"
    span
      className: "letter"
      "#{props.letter}: "
    span
      className: "address"
      props.address

JobCard = React.createFactory class JobCard extends BaseComponent
  render: =>
    super(arguments...)
    #TODO: move this to a property watcher
    job = JobStore.getById(@props.job_id, false, @getId())
    li
      className: "job_card " + if @props.selected then "selected"
      onClick: @props.onClick
      div
        className: "top_section"
        div
          className: "job_info"
          "#" + JobStore.getId(job) + " | " + job.service
        JobCardAddress
          letter: "A"
          address: Utils.shortenLocationAddress(job.service_location)
        if job.drop_location?.address?
          JobCardAddress
            letter: "B"
            address: Utils.shortenLocationAddress(job.drop_location)
      div
        className: "bottom_section"
        div
          className: "status"
          Utils.getTransformedJobStatus(UserStore, job)
        div
          className: "tower"
          job.rescue_company?.name

class PartnerFleet extends BaseComponent
  constructor: ->
    super(arguments...)
    @state.leftBarCollapsed = false
    @state.activeJobs = []
    @jobListHelper = new JobListHelper(@getId())

  getBoundsZoomLevel: (bounds, mapDim) =>
    latRad = (lat) ->
      sin = Math.sin(lat * Math.PI / 180)
      radX2 = Math.log((1 + sin) / (1 - sin)) / 2
      Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2

    zoom = (mapPx, worldPx, fraction) ->
      Math.floor Math.log(mapPx / worldPx / fraction) / Math.LN2

    WORLD_DIM =
      height: 256
      width: 256
    ZOOM_MAX = 21
    ne = bounds.getNorthEast()
    sw = bounds.getSouthWest()
    latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI
    lngDiff = ne.lng() - sw.lng()
    lngFraction = (if lngDiff < 0 then lngDiff + 360 else lngDiff) / 360
    latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction)
    lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction)

    Math.min latZoom, lngZoom, ZOOM_MAX

  resetBounds: =>
    if @state?.activeJobs?.length > 0
      setAmount = 0
      bounds = new (google.maps.LatLngBounds)
      for id in @state.activeJobs
        #Because this is a call and forget function, we don't have to set up watchers
        job = JobStore.getById(id)
        if job?
          latlng = (new (google.maps.LatLng)(job.service_location.lat, job.service_location.lng))
          bounds.extend latlng
          setAmount++
          if job.drop_location?.lat?
            latlng = (new (google.maps.LatLng)(job.drop_location.lat, job.drop_location.lng))
            setAmount++
            bounds.extend latlng

      if setAmount > 1

        boundsProps = {}
        if @state.leftBarCollapsed
          boundsProps.left = 30
        else
          boundsProps.left = 330

        zoom = @getBoundsZoomLevel(bounds, @refs.map.getMapDims())
        if !isNaN(zoom)
          boundsProps?.zoom = zoom

        @refs.map?.fitBounds(bounds, boundsProps)

        #Making sure we don't zoom too far in after setting bounds
        setTimeout( =>
          if @refs.map?
            zoomOverride = @refs.map.getZoom()
            if zoomOverride > 16
              zoomOverride = 16

              @refs.map?.setZoom(zoomOverride)
        , 100)

      else
        @refs.map?.setZoom(13)
        @refs.map?.setCenter(latlng.lat(), latlng.lng())

          #Truck seems like overkill
          #if job.rescue_vehicle_id
          #  #Because this is a call and forget function, we don't have to set up watchers
          #  vehicle = VehicleStore.getById(job.rescue_vehicle_id)
          #  if vehicle?.lat? and vehicle?.lng?
          #    latlng = (new (google.maps.LatLng)(vehicle.lat, vehicle.lng))
          #    setAmount++
          #    bounds.extend latlng

      @refs.map

  onJobClick: (jobId) =>
    { activeJobs } = @state
    if @isJobActive(jobId)
      activeJobs = without(activeJobs, jobId)
    else
      activeJobs = [jobId]
      @jobs[jobId].aPin.active = true
      @jobs[jobId].bPin?.active = false
    @setState({ activeJobs })
    setTimeout(@resetBounds, 100)

  isJobActive: (job_id) =>
    job_id in @state.activeJobs

  updateJobs: (tabList) ->
    jobIds = flow(
      flatMap((tab) -> tab.jobs),
      map('id')
    )(tabList)

    @jobs = @jobs || {}
    for jobId in jobIds
      @jobs[jobId] = assign(@jobs[jobId], JobStore.getById(jobId, false, @getId()))

    @jobs = pick(@jobs, jobIds)

  locationKey: (location) ->
    "#{location?.lat}-#{location?.lng}"

  buildMarkers: () ->
    # TODO make it derived state
    @markers = @markers || {}

    for id, job of @jobs
      # create A pin
      if not job.aPin
        job.aPin = { key: @locationKey(job.service_location), type: 'A', job, active: false }

      # update B pin
      @updateDropLocationMarker(job, @markers)

    # TODO implement site updates
    for site in SiteStore.getFilteredSites(null, @getId())
      site = SiteStore.getById(site.id, false, @getId())
      sitePinKey = @locationKey(site.location)
      if @markers[sitePinKey]
        @markers[sitePinKey].site = site
      else
        @markers[sitePinKey] = { key: sitePinKey, type: 'Site', site }

  updateDropLocationMarker: (job, markers) ->
    attachToMarker = (job, markers) =>
      bPinKey = @locationKey(job.drop_location)
      if markers[bPinKey]
        marker = markers[bPinKey]
        if marker.type == 'B'
          if not includes(marker.jobs, job)
            marker.jobs.push(job)
          marker.active = (job.bPin?.active or marker.active)
          job.bPin = marker
        else if marker.type == 'Site'
          marker.type = 'B'
          marker.active = job.bPin?.active ? false
          marker.jobs = [job]
      else
        markers[bPinKey] = { key: bPinKey, type: 'B', jobs: [job], active: job.bPin?.active ? false }
        job.bPin = markers[bPinKey]

    detachFromMarker = (job, markers) =>
      pull(job.bPin.jobs, job)
      if !job.bPin.jobs.length then delete markers[job.bPin.key]

    if job.bPin
      if job.drop_location
        bPinKey = @locationKey(job.drop_location)
        # if drop location has changed
        if bPinKey != job.bPin.key
          detachFromMarker(job, markers)
          attachToMarker(job, markers)
      else
        detachFromMarker(job, markers)
        delete job.bPin
    else if job.drop_location
      attachToMarker(job, markers)

  render: ->
    super(arguments...)
    tabList = @jobListHelper.getTabs()

    @updateJobs(tabList)

    tabList = map((tab) =>
      id: tab.id
      name: tab.name
      component: () =>
        if !JobStore.jobsLoaded()
          return Loading null
        if tab.jobs?.length == 0
          div
            style:
              textAlign: "center"
              marginTop: 20
              color: "#CCC"
            "No "+tab.name.toLowerCase()+" jobs."
        ul
          className: "job_list"
          for job in sortBy(tab.jobs, ['id']).reverse()
            JobCard
              job_id: job.id
              key: job.id
              selected: @isJobActive(job.id)
              onClick: partial(@onJobClick, job.id)
    , tabList)

    @buildMarkers()

    div
      className: 'fleet-map' + if @state.leftBarCollapsed then " leftCollapsed" else ""
      div
        className: "left-side-bar"
        div
          className: "content"
          Tabs
            tabList: tabList
        div
          className: "exanderContainer"
          onClick: =>
              @setState
                leftBarCollapsed: !@state.leftBarCollapsed
          div
            className: "expander"

      SwoopMap
        ref: "map"
        initialCenterLat: 41
        initialCenterLng: -110
        initialZoom: 4
        map((marker) =>
          Markers
            activateJobs: (jobs) =>
              @setState({ activeJobs: @state.activeJobs.concat(map('id', jobs)) })
            deactivateJobs: (jobs) =>
              @setState({ activeJobs: without(@state.activeJobs, ...map('id', jobs)) })
            dropZoomLevel: 8
            isJobActive: (job) => @isJobActive(job.id)
            key: marker.key
            marker: marker
            siteZoomLevel: 8
        , concat(values(@markers), map('aPin', @jobs)))



module.exports = PartnerFleet
