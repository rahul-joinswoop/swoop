
import React from 'react'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import EditForm from 'components/edit_form' // TODO: if a change comes in, show an error that something has changed and to refresh
import FeatureStore from 'stores/feature_store'
import Logger from 'lib/swoop/logger'
import VehicleCategoryStore from 'stores/vehicle_category_store'

class VehicleForm extends BaseComponent {
  render() {
    const categories = VehicleCategoryStore.getAllSorted(this.getId())
    const categoryOptions = [
      {
        value: null,
        text: '- Select -',
      },
    ]

    categories.forEach((category) => {
      categoryOptions.push({
        value: category.id,
        text: category.name,
      })
    })

    const data = [
      {
        name: 'name',
        label: 'Name*',
        type: 'text',
        required: true,
      },
      {
        name: 'make',
        label: 'Make',
        type: 'text',
        classes: 'right',
      },
      {
        name: 'model',
        label: 'Model',
        type: 'text',
      },
      {
        name: 'year',
        label: 'Year',
        type: 'year',
        classes: 'right',
      },
      {
        name: 'vehicle_category_id',
        label: 'Type*',
        type: 'selector',
        required: true,
        options: categoryOptions,
      },
      {
        name: 'dedicated_to_swoop',
        label: 'Enable direct dispatching to truck',
        type: 'checkbox',
        classes: 'right',
        containerStyle: {
          position: 'relative',
          top: 15,
        },

        show() {
          return FeatureStore.isFeatureEnabled(
            Consts.FEATURE_ENABLE_AUTO_DISPATCH_TO_TRUCK,
          )
        },
      },
    ]
    let title = 'Create New Vehicle'

    if (this.props.record?.id) {
      title = 'Edit Vehicle'
    }

    Logger.debug('title: ', title)
    return (
      <EditForm
        {...this.props}
        cols={[
          {
            fields: data,
          },
        ]}
        title={title}
      />
    )
  }
}

export default VehicleForm
