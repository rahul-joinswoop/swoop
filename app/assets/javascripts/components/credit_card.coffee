require('stylesheets/components/credit_card.scss')
getSource = (props) ->
  switch(props.brand)
    when "American Express" then require('public/images/credit_cards/amex.png')
    when "Diners Club" then require('public/images/credit_cards/diners_club.png')
    when "Discover" then require('public/images/credit_cards/discover.png')
    when "MasterCard" then require('public/images/credit_cards/mastercard.png')
    when "Visa" then require('public/images/credit_cards/visa.png')
    when "JCB" then require('public/images/credit_cards/jcb.png')
    when "UnionPay" then require('public/images/credit_cards/union_pay.png')
    else require('public/images/credit_cards/default.png')

module.exports = (props) ->
  div
    className: "credit_card"
    img
      src: getSource(props)
    span null,
      "••••"
    span null,
      if props.last4
        props.last4
      else
        "••••"
