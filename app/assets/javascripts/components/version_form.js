
import React from 'react' // TODO: if a change comes in, show an error that something has changed and to refresh
import EditForm from 'components/edit_form'
import VersionStore from 'stores/version_store'
import { withModalContext } from 'componentLibrary/Modal'

class VersionForm extends React.Component {
  newSubmit = (newRecord) => {
    if (newRecord.id) {
      VersionStore.updateItem(newRecord)
    } else {
      VersionStore.addItem(newRecord)
    }

    this.props.hideModals()
  }

  render() {
    const data = [
      {
        name: 'name',
        label: 'name',
        type: 'span',
        value: this.props.name,
        required: true,
      },
      {
        name: 'version',
        label: 'Version',
        type: 'text',
        required: true,
      },
      {
        name: 'description',
        label: 'Description',
        type: 'text',
        classes: 'right',
      },
      {
        name: 'forced',
        label: 'Force Upgrade',
        type: 'checkbox',
      },
    ]

    let title = `Create New ${this.props.name} Version`

    if (this.props.record?.id) {
      title = 'Edit Record'
    }

    return (
      <EditForm
        {...this.props}
        cols={[
          {
            fields: data,
          },
        ]}
        disabled={false}
        extraClasses="simple_modal"
        onCancel={this.props.hideModals}
        onConfirm={this.newSubmit}
        show
        submitButtonName="Submit"
        title={title}
      />
    )
  }
}

export default withModalContext(VersionForm)
