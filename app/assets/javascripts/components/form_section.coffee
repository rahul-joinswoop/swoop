import Consts from 'consts'

BaseComponent = require('components/base_component')
import { merge } from 'lodash'

class FormSection extends BaseComponent
  displayName: 'FormSection'

  getTextStyle: =>
    if @props.textStyle
      @props.textStyle
    else
      color: "#90C4E4"
      padding: "3px 10px 3px"
      border: "0"
      display: 'inline-block'
      textTransform: "uppercase"
      fontWeight: "bold"

  render: ->
    defaultStyle = {
      backgroundColor: "#fff"
      textAlign: "left"
      borderRadius: 5
      border: "1px solid #E2E2E2"
      marginBottom: 10
    }
    style = merge(defaultStyle, @props.style)
    div
      className: @props.className
      style: style

      div
        style:
          borderBottom: "1px solid #E2E2E2"
        h3
          onClick: =>
            if @props.form?
              if !@count
                @count = 0
              @count++
              if (@count > 5 || window.hackFill) and Consts.JS_ENV!='production' and !@props?.storage?
                 window.hackFill = true
                 @props.form.fillExamples()
          style: @getTextStyle()
          @props.title
        if @props.headerView
          @props.headerView
      div
        className: @props.title?.toLowerCase() + "-section"
        style:
          padding: 10
        @props.children

module.exports = FormSection
