import React from 'react'
import moment from 'moment-timezone'
import ReactDOMFactories from 'react-dom-factories'
import createReactClass from 'create-react-class'
AccountStore = require('stores/account_store')
AuctionStore = require('stores/auction_store').default
import AutoAssignResults from 'components/job/AutoAssignResults'
BaseComponent = require('components/base_component')
import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import CarouselImport from 'components/Carousel'
Carousel = React.createFactory(CarouselImport)
PartnerFormModal = React.createFactory(require('components/modals/partner_form'))
JobModals = require 'components/job_modals'
UnsetRescueCompanyModal = React.createFactory(JobModals[4])
JobFormModal = React.createFactory(JobModals[0])
AssignDriverModal = React.createFactory(JobModals[2])
FleetSiteModal = React.createFactory(require('components/fleet_site_form'))
DeleteJobModal =  React.createFactory(require('components/modals/delete_job'))
InvoiceFormModal = React.createFactory(require('components/modal_invoice'))
ChargeCardModal = React.createFactory(require("components/modals/CreditCard/ChargeCardModal").default)
CompanyStore = require('stores/company_store').default
import CarouselModalImport from 'components/CarouselModal'
CarouselModal = React.createFactory(CarouselModalImport)
import Consts from 'consts'
Countdown = React.createFactory(require 'components/countdown')
CustomerTypeStore = require('stores/customer_type_store')
DepartmentStore = require('stores/department_store')
DetailHistory = React.createFactory require('components/job/detail_history')
TransitionModal = React.createFactory(require 'components/modals')
import Dispatcher from 'lib/swoop/dispatcher'
DocumentList = React.createFactory(require('components/job/document_list').default)
DropDownButton = React.createFactory(require 'components/simple/dropdown_button')
import ETATimeImport from 'components/eta_time'
ETATime = React.createFactory(ETATimeImport)
FeatureStore = require('stores/feature_store')
InputDriver =  React.createFactory(require('components/input_driver'))
InvoiceStore = require ('stores/invoice_store')
IssueList = React.createFactory(require('components/job/issue_list').default)
IssueStore = require('stores/issue_store')
import JobDetailCarouselItemImport from 'components/JobDetailCarouselItem'
JobDetailCarouselItem = React.createFactory(JobDetailCarouselItemImport)
import JobHistory from 'components/JobHistory'
JobStore = require('stores/job_store')
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
LocationTypeStore = require('stores/location_type_store')
ModalStore = require('stores/modal_store')
ProviderStore = require('stores/provider_store')
import SetupCardModalImport from "components/modals/CreditCard/SetupCardModal"
SetupCardModal = React.createFactory(SetupCardModalImport)
SiteStore = require('stores/site_store').default
Stars = React.createFactory(require("components/stars"))
SwoopMap=React.createFactory(require('components/maps/map'))
import TabsImport from 'components/tabs'
Tabs = React.createFactory(TabsImport)
TireTypeStore = require('stores/tire_type_store')
TrailerStore = require('stores/trailer_store')
UserStore = require('stores/user_store')
UsersStore = require('stores/users_store').default
VehicleCategoryStore = require('stores/vehicle_category_store')
VehicleStore = require('stores/vehicle_store')
import Utils from 'utils'
import {
  shouldShowChargeCard,
  isBankingSetUp,
  renderDropLocationFieldForJobDetails,
  renderPickupLocationFieldForJobDetails,
} from 'DataUtils'
import { filter, keys, map, partial, pick, sortBy, uniq } from 'lodash'
require('stylesheets/components/job_details.scss')
import { withSplitIO } from 'components/split.io'
import { withModalContext } from 'componentLibrary/Modal'
import SwoopMapImport from 'components/maps/SwoopMap'
import { Route as RouteImport, TruckMarker as TruckMarkerImport} from 'components/maps/map_new'
import { fitBounds } from 'components/maps/Utils'
import Logger from 'lib/swoop/logger'
import { formatDistance } from 'lib/localize'

SwoopMapNew = React.createFactory(SwoopMapImport)
Route = React.createFactory(RouteImport)
TruckMarker = React.createFactory(TruckMarkerImport)


class JobDetails extends BaseComponent
  displayName: 'JobDetails'
  expandedJobs: {}

  constructor: ->
    super(arguments...)

    @register(CustomerTypeStore, CustomerTypeStore.CHANGE)
    @register(JobStore, JobStore.CHANGE)
    @register(InvoiceStore, InvoiceStore.CHANGE)
    @register(ModalStore, ModalStore.CHANGE)

  rerender: -> @rerenderWhenModalCloses()

  jobChangeSubmit: (job, close, saveAsDraft = false) =>
    if !job.id?
      JobStore.jobAdded(job, {save_as_draft: saveAsDraft})
    else
      JobStore.jobChanged(job, false, saveAsDraft)
    if not close? || close
      @props.hideModals()

  showJobFormModal: (job, e) =>
    e.preventDefault()
    e.stopPropagation()
    @props.showModal(
      JobFormModal
        show: true
        onCancel: @props.hideModals
        onConfirm: @jobChangeSubmit
        submitButtonName: 'Save'
        disabled: false
    )

  invoiceChangeSubmit: (invoice, close) ->
    Logger.debug("about to call dispatch", invoice)

    if keys(invoice).length == 2 and invoice.id?
      console.warn("No changes were made to invoice so skipping send")
    else
      Dispatcher.send(InvoiceStore.INVOICE_CHANGED, invoice)

    if not close? || close
      @props.hideModals()

  showPartnerFormModal: (provider, e) =>
    e.preventDefault()
    e.stopPropagation()
    @props.showModal(
      PartnerFormModal
        loadedFromJobDetails: true
        record: provider
        show: true
        onCancel: @props.hideModals)

  showFleetSiteModal: (site, e) =>
    e.preventDefault()
    e.stopPropagation()
    @props.showModal(
      FleetSiteModal
        record: site
        show: true
        onCancel: @props.hideModals
        disable: true
        hideLocationType: true
        loadedFromJobDetails: true)

  showDeleteJobModal: (job, e) =>
    e.preventDefault()
    e.stopPropagation()
    @props.showModal(
      DeleteJobModal
        record: job
        show: true
        onCancel: @props.hideModals
        onConfirm: @jobChangeSubmit)

  showUnsetRescueCompanyModal: (job, e) =>
    e.preventDefault()
    e.stopPropagation()
    if UserStore.isFleet() || UserStore.isSwoop()
      @props.showModal(
        UnsetRescueCompanyModal
          record: job
          show: true
          onCancel: @props.hideModals
          onConfirm: @jobChangeSubmit
          disabled: false)

  showAssignDriverModal: (job, e) =>
    e.preventDefault()
    e.stopPropagation()
    if UserStore.isPartner()
      @props.showModal(
        AssignDriverModal
          show: true
          record: job
          onCancel: @props.hideModals
          onConfirm: @jobChangeSubmit
          submitButtonName: 'Save'
          disabled: false
      )

  handleChange: (e) =>
    obj = {
      id: @props.id
    }
    obj[e.target.name] = if e.target.value!= "" then e.target.value else null
    JobStore.jobChanged(obj)
    return

  assignDriver: (driver_id) =>
    obj = {
      id: @props.id
      rescue_driver_id: if driver_id != "" then driver_id else null
    }

    if !JobStore.getJobById(@props.id)?.rescue_vehicle_id?
      # this is a callback function, no need to pass @getId()
      vehicleId = VehicleStore.getVehicleIdByDriverId(driver_id)
      if vehicleId
        obj["rescue_vehicle_id"] = vehicleId

    JobStore.jobChanged(obj)

  editInvoice: (invoice, e) =>
    e.preventDefault()
    e.stopPropagation()
    @props.showModal(
      InvoiceFormModal
        record: invoice.id
        show: true
        onCancel: @props.hideModals
        cancel: "Cancel"
        onConfirm: @invoiceChangeSubmit
        submitButtonName: 'Save'
        disabled: false)

  shouldHideEditButton: (job) =>
    state = ""
    displayInvoice = Utils.getDisplayInvoice(job, UserStore.isRoot())

    if displayInvoice?
      id = displayInvoice.id
      state = displayInvoice?.state
      invoice = InvoiceStore.getInvoiceById(id, false)
      if invoice?
        state = invoice.state

    hiddenFleetStatuses = [Consts.COMPLETED, Consts.CANCELED, Consts.GOA]
    hiddenFleetStates = ["", Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT,
                         Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED,
                         Consts.ISWOOPAPPROVED, Consts.ISWOOPNEW, Consts.ISWOOPSENT]

    if UserStore.isFleet() and job.status in hiddenFleetStatuses and state in hiddenFleetStates
      return true

    hiddenPartnerStatuses = [Consts.ASSIGNED]
    hiddenPartnerStates = [Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED,
                           Consts.ISWOOPAPPROVED, Consts.ISWOOPNEW, Consts.ISWOOPSENT]

    if Utils.isAssignedFromFleet(job) and UserStore.isPartner() and job.status in hiddenPartnerStatuses and state in hiddenPartnerStates
      return true

    return false

  renderEditButton: (job) =>
    #Always render edit unless you are a fleet user with a canceled job
    if @shouldHideEditButton(job)
      return null

    return Button
      color: "primary-o"
      name: 'edit'
      onClick: partial(@showJobFormModal, job)
      size: "full-width"
      "Edit"


  renderCancelButton: (job) =>
    if (job.status in [Consts.COMPLETED, Consts.CANCELED, Consts.GOA]) or ((job.type == "FleetInHouseJob" or job.type == "FleetMotorClubJob") and UserStore.isSwoop())
      return null
    else if Utils.isAssignedFromFleet(job) and UserStore.isPartner() and job.status is Consts.ASSIGNED
      return null

    return Button
      color: "red-o"
      name: 'cancel'
      onClick: partial(@showDeleteJobModal, job)
      size: "full-width"
      "Cancel"

  deleteImage: (job, image, e) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "simple",
      props: {
        title: "Delete Image"
        confirm: 'OK'
        body: "Are you sure you want to delete this image?"
        onConfirm: =>
          obj = {
            id: job.id
            images: [
              {
                id: image.id
                deleted_at: moment().toISOString()
              }
            ]
          }
          JobStore.jobChanged(obj)
          Dispatcher.send(ModalStore.CLOSE_MODALS)
      }
    )

    e.preventDefault()
    e.stopPropagation()

  shouldShowTotalTimeInvoice: (job, invoice, rateType) =>
    if job && invoice
      lineItem  = InvoiceStore.findLineItemByType(invoice.id, Consts.PORT_TO_PORT_HOURS)
      totalTime = JobStore.totalTime(job.id, @getId())

      # invoice.rate_type is eqs to rateType param AND lineItem.quantity is different from job.totalTime
      return invoice.rate_type == rateType and Utils.toFixed((parseFloat(lineItem?.quantity) * 60), 2) != Utils.toFixed(totalTime, 2)

  render: ->
    super(arguments...)

    job = JobStore.getFullJobById(@props?.id, true)

    #Hack to set up watcher for the job
    #TODO: getFullJobById should take a watcherId
    JobStore.getById(@props?.id, false, @getId())
    if not job? || job.partial
      return Loading null

    div
      ref: job.id+'details'
      'data-job': job.id
      className: 'job_details'
      @renderMap(job)
      Tabs
        className: 'detail_tabs'
        tabs_key: job.id
        saveOnRefresh: false
        defaultTab: 0
        disableSavedTabIndex: true
        tabList: [
          {
            name: "Details"
            component: =>
              @renderDetails(job)
          },
          {
            name: 'History'
            component: =>
              Tracking.trackJobEvent("History Visit", job.id, {
                category: 'Dispatch'
              })
              DetailHistory
                job_id: job.id

          },
          {
            name: 'Auto Assign',
            component: =>
              <AutoAssignResults
                job={job}
              />
            hideTab: !UserStore.isSwoop() || !job.auction
          },
        ]

  showChargeCardModal: (invoice_id) =>
    this.props.showModal(
      ChargeCardModal
        invoice_id: invoice_id
    )

  showSetupCardModal: () =>
    this.props.showModal(
      SetupCardModal
        isAdminWithPayPermission: UserStore.isAdmin() && UserStore.hasPermissionsToCharge()
    )

  renderMap: (job) ->
    truckId = null
    if job.status not in [Consts.COMPLETED, Consts.REASSIGNED, Consts.ETAREJECTED, Consts.EXPIRED, Consts.CANCELED, Consts.GOA, Consts.REJECTED, Consts.STORED, Consts.RELEASED, Consts.DISPATCHED, Consts.PENDING]
      truckId = job.rescue_vehicle?.id

    if not Utils.isMobile()
      if ModalStore.getModal() in ["assign", "job_form_edit", "storage_vehicle_new", "job_form_new"]
        div
          className: "emptyMap"
      else
        if not @props.treatments?[Consts.SPLITS.DISABLE_NEW_JOB_DETAILS_MAP]
          return SwoopMapNew
            onLoad: (map) =>
              locations = [
                job.service_location,
                job.drop_location,
              ]

              if truckId
                truckLocation =
                  lat: VehicleStore.get(truckId, 'lat', @getId())
                  lng: VehicleStore.get(truckId, 'lng', @getId())
                locations.push(truckLocation)

              fitBounds(map, locations)
            TruckMarker
              id: truckId
              hideOldTrucks: true
              showTruckNames: true
            Route
              obfuscate: UserStore.isPartner() && job.status == Consts.AUTO_ASSIGNING
              route: {
                id: job.id
                origin: pick(job.service_location, 'lat', 'lng')
                dest: pick(job.drop_location, 'lat', 'lng')
                originMarker: "/assets/images/marker_green.png"
                destMarker: "/assets/images/marker_red.png"
              }
        else
          return SwoopMap
            autoAssigning: job.status == Consts.AUTO_ASSIGNING
            routes: [
              {
                id: job.id
                origin:job.service_location
                dest:job.drop_location
                originMarker: "/assets/images/marker_green.png"
                destMarker: "/assets/images/marker_red.png"
              }
            ]
            trucks:[truckId]
            showTruckNames: true
            hideOldTrucks: true

  shouldShowEta: (job) ->
    return UserStore.isPartner() and not Utils.isETA(job) and
      job.status in [Consts.CREATED, Consts.PENDING, Consts.REJECTED,
                     Consts.UNASSIGNED, Consts.REASSIGN, Consts.ASSIGNED,
                     Consts.ACCEPTED, Consts.DISPATCHED, Consts.ENROUTE]

  shouldShowTruckAssigned: (job) ->
    return UserStore.isPartner() and job.status not in [Consts.DRAFT, Consts.SUBMITTED] and
      (not Utils.isAssignedFromFleet(job) or job.status != Consts.ASSIGNED or job.rescue_vehicle_id!= null) and
      !AuctionStore.jobHasLiveAuction(job)

  shouldShowDriverAssigned: (job) ->
    return @shouldShowTruckAssigned(job)

  shouldShowRateEstimate: (job, invoice, cost) ->
    return (UserStore.isPartner() || UserStore.isSwoop()) and
      cost? and not isNaN(cost) and invoice? and
      job.status not in [Consts.CREATED, Consts.UNASSIGNED, Consts.AUTO_ASSIGNING,
                         Consts.REJECTED, Consts.REASSIGN, Consts.SUBMITTED,
                         Consts.REASSIGNED, Consts.ETAREJECTED, Consts.EXPIRED]

  handlePhotoClick: (photoItem, allPhotoItems, e) ->
    @props.showModal(
      CarouselModal
        show: true
        onCancel: @props.hideModals
        indexClicked: allPhotoItems.indexOf(photoItem),
        photoItem: photoItem,
        allPhotoItems: allPhotoItems,
        handlePhotoDelete: @handlePhotoDelete
    )
    e.preventDefault()
    e.stopPropagation()

  handlePhotoDelete: (item, e) ->
    @props.showModal(
      TransitionModal
        show: true
        extraClasses: "simple_modal"
        onCancel: @props.hideModals
        disabled: false
        title: "Delete Image"
        confirm: 'OK'
        onConfirm: =>
          JobStore.removeDocument(item.job_id, item.id)
          @props.hideModals()
        "Are you sure you want to delete this image?"
    )

  sortPhotosChronologically: (photoItems) ->
    return sortBy(photoItems, (item) => return moment(item.created_at))

  getPhotoCarousel: (jobId, type) ->
    photoItems = @sortPhotosChronologically(JobStore.getAttachedDocuments(jobId, Consts.TYPE_LOCATION_IMAGE, type))
    if photoItems?.length > 0
      div
        className: 'carousel-container'
        Carousel
          hideScrollBar: true
          buttonSize: 26
          buttonTop: '48px'
          handlePhotoDelete: @handlePhotoDelete
          scrollManyOnClick: true
          items: map(photoItems, (photoItem) =>
            JobDetailCarouselItem
              key: photoItem.id
              photoItem: photoItem
              onClick: (e) => @handlePhotoClick(photoItem, photoItems, e)
          )

  renderDetails: (job) ->
    distanceUnit = CompanyStore.get(UserStore.getCompany().id, 'distance_unit', this.getId())
    notes_title = ""

    if UserStore.isSwoop()
      notes_title = 'loading...'
      if job.owner_company?.id and CompanyStore.get(job.owner_company.id, 'name', @getId())
        notes_title = "Client Notes:"

    else
      notes_title = "Internal Notes:"

    invoice = Utils.getDisplayInvoice(job, UserStore.isRoot())

    rate_type = invoice?.rate_type
    managedFleet = (UserStore.isFleet() and not UserStore.isFleetInHouse())

    if invoice?
      saved_invoice = InvoiceStore.getInvoiceById(invoice.id)
      if saved_invoice?
        invoice = saved_invoice

    cost = (parseFloat(invoice?.total_amount))
    if invoice?.cost?
      cost = (parseFloat(invoice?.cost))

    if job?
      datetime = Utils.formatDateTime(Utils.getJobCreatedAt(job))
      if job.scheduled_for?
        datetime = Utils.formatDateTime(job.scheduled_for)

    private_customer = false#(UserStore.isPartner() and Utils.isAssignedFromFleet(job) and job.owner_company.name == "Tesla")

    data = [
      {header: "Edit:", value: @renderEditButton(job), show: Utils.isMobile()}
      {header: "Cancel:", value: @renderCancelButton(job), show: Utils.isMobile()}
      {header: "ID:", value: job.id, show: Utils.isMobile()}
      {
        header: "Exceptions:",
        show: job?.owner_company?.name == Consts.ENTERPRISE_FLEET_MANAGEMENT and UserStore.isPartner(),
        value: =>
          span
            style:
              color: 'red'
              fontWeight: 'bold'
            'Do not change drop off address without Swoop Dispatch authorization: 847-796-6763.'
      }
      {header: "Time:", value: datetime, show: Utils.isMobile() and not (UserStore.isFleet() and UserStore.isFleetInHouse())}

      {header: "Rate Type:", show: @props.invoice? and UserStore.isFleet() and UserStore.isFleetInHouse()?, value: =>

        return Consts.RATE_TYPE_MAP[rate_type]
      }

      {header: 'Partner:', classes: "company", show: UserStore.isSwoop() or UserStore.isFleetInHouse(), value: =>
        if job.rescue_company?.name?
          providerId = ProviderStore.getPropertyBySiteId(job.site_id, 'id', @getId(), true)
          preventUnsetStates = [Consts.ENROUTE, Consts.ONSITE, Consts.TOWING,
                                Consts.TOWDESTINATION, Consts.REJECTED, Consts.CANCELED,
                                Consts.GOA, Consts.COMPLETED, Consts.REASSIGNED, Consts.ETAREJECTED]
          [
            ReactDOMFactories.a
              key: 'partner_form'
              onClick: partial(@showPartnerFormModal, ProviderStore.getById(providerId)) # Pass the partner object to the form, not only a property. Should we do this when loading forms?
              JobStore.partnerNameFor(job)
            if not @props.invoice? and not @props.storage? and job.status not in preventUnsetStates
              ReactDOMFactories.i
                className: "fa fa-times"
                key: JobStore.partnerNameFor(job)
                onClick: partial(@showUnsetRescueCompanyModal, job)
          ]
      }
      {header: "Questions:", multiline: true, show: (job?.question_results? and job.question_results.length > 0), value: ->
        answers = []
        if job?.question_results?
          for answer in job.question_results
            if answer.question and answer.answer
              #Slight hack to ensure customer safe is at the top
              if answer.question == Consts.QUESTION_CUSTOMER_SAFE
                answers.unshift(answer)
              else
                answers.push(answer)

        if answers.length == 0
          return

        ul null,
          for answer in answers
            li
              key: answer.answer_id
              Utils.getQuestionResultSpan(job?.owner_company?.name, answer)

      }
      {
        header: "Tax Exempt:",
        multiline: true,
        show: CustomerTypeStore.get(job.customer_type_id, "name", @getId()) == Consts.CUSTOMER_TYPE_TAX_EXEMPT,
        value: "Do not charge tax on invoice, customer is exempt. Documentation available upon request."
      }
      {header: "Job Details:", value: job.notes, multiline: true}
      {header: (if UserStore.isSwoop() then "Partner Notes:" else "Internal Notes:"), value: job.partner_notes, show: UserStore.isPartner(), multiline: true}
      {header: (if UserStore.isSwoop() then "Client Notes:" else "Internal Notes:"), value: job.fleet_notes, show: (UserStore.isSwoop() or UserStore.isFleet()), multiline: true}
      {header: "Swoop Notes:", value: job.swoop_notes, show:UserStore.isSwoop(), multiline: true}
      {header: "Dispatcher Notes:", value: job.dispatch_notes, show: (UserStore.isPartner() and not UserStore.isOnlyDriver()) or UserStore.isSwoop(), multiline: true}
      {header: "Invoice Notes:", value: job.driver_notes, multiline: true, show: !UserStore.isFleetManaged()}
      {header: "Live ETA:", show: UserStore.isPartner() and Utils.isETA(job) and job.status in [Consts.ENROUTE], value: =>
        ret = Utils.getLastEtaObj(job)
        if ret?
          Countdown
            rerender: @rerender
            time: ret
      }
      {header: "ETA:", show: @shouldShowEta(job), value: =>
        ETATime
          job: job
          rerender: @rerender
      }
      {header: "Dispatch Site:", show: not UserStore.isFleet(), value: =>
        if job.site_id
          SiteStore.get(job.site_id, 'name', @getId()) || Consts.LOADING
        else
          ''
      }
      {header: "Department:", show: FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS) and UserStore.isFleet(), value: => DepartmentStore.get(job?.department_id, "name", @getId()) }
      {header: "Truck Assigned:", show: @shouldShowTruckAssigned(job), value: =>
        if UserStore.isSwoop() or @props.invoice? or @props.storage?
          VehicleStore.get(job.rescue_vehicle_id, 'name', @getId())
        else
          select
            className: 'form-control'
            name: 'rescue_vehicle_id'
            value: if job.rescue_vehicle_id? then job.rescue_vehicle_id else ''
            onChange: @handleChange
            option
                value: ''
                className: 'option_header'
                '- Select -'
            for vehicle in VehicleStore.getSortedVehicles(@getId())
                option
                  key: vehicle.id
                  value: vehicle.id
                  className: 'option_row'
                  vehicle.name
      }
      {header: "Driver Assigned:", row_styles: {zIndex: 9999}, value_styles: {overflow: "visible"}, show: @shouldShowDriverAssigned(job), value: =>
        span null,
        if job.rescue_driver_id? and job?.status? and job.status in [Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION]
          name = UsersStore.get(job.rescue_driver_id, "full_name", @getId())
          if name? then name else ""
        else
          # force load the vehicles with driver to be able to use it in the callback function (@assignDriver)
          # TODO do we have a better way to do this?
          VehicleStore._loadVehiclesWithDriverListIds()

          DropDownButton
            enabled: if @props.invoice? or @props.storage? then false else true
            record: job
            value: job.rescue_driver_id
            onChange: @assignDriver
            width: 200
            vehicle: job.rescue_vehicle_id
            buttonClick: partial(@showAssignDriverModal, job)
      }
      {
        header: "Trailer:", value: => if UserStore.isPartner() and job.trailer_vehicle_id then TrailerStore.get(job.trailer_vehicle_id, 'name', @getId())
      }
      {
        header: "Location:",
        value: () => renderPickupLocationFieldForJobDetails(job, @getId())
      }
      {
        header: "Drop Off:",
        value: () => renderDropLocationFieldForJobDetails(job, @getId())
      }


      {header: (if Utils.isEnterpriseJob(UserStore, job) then "Branch Contact:" else "Customer Name:"), value: ( => job.customer.full_name), show:
        job.customer? and not private_customer && (if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job)) }
      {header: (if Utils.isEnterpriseJob(UserStore, job) then "Branch Phone:" else "Customer Phone:"), value: ( => Utils.renderPhoneNumber(job.customer.phone)), show:
        job.customer? and not private_customer && (if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job))}
      {header: "Member Number:", value: job.customer?.member_number, classes: "fs-exclude", show: (FeatureStore.isFeatureEnabled(Consts.FEATURE_MEMBER_NUMBER) && !UserStore.isPartner())}
      {header: "Policy Number:", value: job.policy_number, classes: "fs-exclude", show: !UserStore.isPartner()}
      {header: "Claim Number:", value: job.pcc_claim_number, classes: "fs-exclude", show: !UserStore.isPartner()}
      {header: "Pickup Contact:", value: job.pickup_contact?.full_name, classes: "fs-exclude", show: if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job) else true }
      {header: "Pickup Phone:", value: ( => Utils.renderPhoneNumber(job.pickup_contact.phone)), classes: "fs-exclude", show: job.pickup_contact?.phone? && (if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job)) }
      {header: "Dropoff Contact:", value: job.dropoff_contact?.full_name, classes: "fs-exclude", show: if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job) else true }
      {header: "Dropoff Phone:", value: ( => Utils.renderPhoneNumber(job.dropoff_contact.phone)), classes: "fs-exclude", show: job.dropoff_contact?.phone?  && (if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job)) }
      {
        header: "Rental Car:",
        value:
          ReactDOMFactories.a
            key: "launch_form_anchor"
            class: "component-button narrow secondary"
            href: process.env.RENTAL_CAR_BASE_URL + (job.ref_number || '')
            target: '_blank'
            "Launch Form"
        classes: "fs-exclude",
        show: FeatureStore.isFeatureEnabled(Consts.FEATURES_JOB_DETAILS_RENTAL_CAR),
      }
      {header: (if UserStore.isSwoop() then "Client Site:" else "Site:"),  classes: "company", value: (=>
        ReactDOMFactories.a
          key: 'fleet_site_form'
          onClick: partial(@showFleetSiteModal, SiteStore.getById(job.fleet_site_id))
          SiteStore.get(job.fleet_site_id, 'name', @getId())
      ), show: (UserStore.isSwoop() || UserStore.isFleet()) && job.fleet_site_id? }

      {header: (if UserStore.isEnterprise() then "Claim Type:" else "Payment Type:"), value: ( =>
        [
          Utils.getPaymentTypeSpan(CustomerTypeStore, InvoiceStore, job, @getId())
        ]
        ), show: job.customer_type_id?
      }
      {header: "Vehicle:", value: (
        Utils.getVehicleName(job)
        )
      }
      {header: "Class Type:", value: VehicleCategoryStore.get(job.invoice_vehicle_category_id, "name",  @getId())}
      {header: "Style:", value: job.vehicle_type}
      {header: Utils.getSymptomLabel(UserStore, {postfix:":"}), value: if job.symptom? then job.symptom }
      {header: "License:", value: (if job.license? then job.license.toUpperCase()), classes: "fs-exclude", show: if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job) else true }
      {header: "VIN:", value: (if job.vin? then job.vin.toUpperCase()), classes: "fs-exclude", show: if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job) else true }
      {header: "Serial:", value: if job.serial_number? then job.serial_number  }
      {header: "Odometer:", value: job.odometer }
      {header: "Tire Size:", show: job.tire_type_id, value: =>
        return TireTypeStore.getTireTypeName(job.tire_type_id, @getId())
      }
      {header: Utils.getPOLabel(UserStore, job, ":"), value: job.po_number}
      {header: "Vendor ID:", value: job?.issc_dispatch_request?.contractor_id}
      {header: "Club Job Type:", value: job?.issc_dispatch_request?.club_job_type}
      {header: Utils.getRefLabel(UserStore, job, ":"), value: job.ref_number}
      {header: "Unit:", value: job.unit_number}
      {header: "Accounting Code:", value: job.accounting_code, show: UserStore.isFleet()}
      {header: "Customer Email:", value: job.email, classes: "fs-exclude", show: (UserStore.isPartner() && !Utils.isSwoopJob(UserStore, job)) || UserStore.isTuro() }
      {header: "Caller Name:", classes: "fs-exclude", show: not UserStore.isFleet(), value: =>
        if job.type in ["FleetInHouseJob", "FleetMotorClubJob"] or UserStore.isSwoop()
          if job.fleet_dispatcher?
            job.fleet_dispatcher.full_name
        else if job.type in ["FleetManagedJob"]
          if job.root_dispatcher?
            job.root_dispatcher.full_name
        else if caller?
          job.caller.full_name
      }
      {header: "Caller Phone:", classes: "fs-exclude", value: =>
        if job.type in ["FleetInHouseJob", "FleetMotorClubJob"] or UserStore.isSwoop()
          if job.fleet_dispatcher?
            Utils.renderPhoneNumber(job.fleet_dispatcher.phone)
        else if job.type in ["FleetManagedJob"]
          if job.root_dispatcher?
            Utils.renderPhoneNumber(job.root_dispatcher.phone)
        else if caller?
            Utils.renderPhoneNumber(job.caller.phone)
      }
      {header: "Agent Inputted:", value: (=> Utils.shortenAddress(job?.original_service_location?.address)), show: not @props.invoice?}
      {header: "Customer Inputted:", value: (=> Utils.shortenAddress(job?.service_location?.address)), show: not @props.invoice? and job?.original_service_location?}
      {header: "Blocking Traffic:", value: (if job.blocking_traffic then "✓" else null)}
      {header: "Keys in Trunk:", value: "✓", show: job.keys_in_trunk == true}
      {header: "4WD:", value: "✓", show: job.four_wheel_drive == true}
      {header: "Accident:", value: "✓", show: job.accident == true}

      {header: "Dispatcher:", value: =>
        if UserStore.isPartner()
          id = job.partner_dispatcher_id
        else if UserStore.isFleet()
          id = job.fleet_dispatcher_id
        else if UserStore.isSwoop()
          id = job.root_dispatcher_id

        return UsersStore.get(id, "full_name", @getId())

      }
      {header: "Dispatcher Number:", show: UserStore.isSwoop(), value: =>
        if UserStore.isPartner()
          id = job.partner_dispatcher_id
        else if UserStore.isFleet()
          id = job.fleet_dispatcher_id
        else if UserStore.isSwoop()
          id = job.root_dispatcher_id

        return Utils.renderPhoneNumber(UsersStore.get(id, "phone", @getId()))
      }


      {header: "Rating:", value: (=>
          div null,
            if job.review?.rating?
              Stars(stars: job.review.rating)
            else [
              if job.review?.nps_question1?
                div null,
                    "Question 1 - How well informed: " + job.review.nps_question1
              if job.review?.nps_question2?
                div null,
                  "Question 2 - How on time: " + job.review.nps_question2
              if job.review?.nps_question3?
                div null,
                  "Question 3 - NPS: " + job.review.nps_question3
              if !job.review?.rating? and !job.review?.nps_question1? and !job.review?.nps_question2? and !job.review?.nps_question3?
                "Request Sent"
            ]
        ), show: (job.review? and not job.review.survey?)}
      {header: "Survey:", value: (=>
        div null,
          if job?.review?.survey_results?
            for result in sortBy(job.review.survey_results, "order")
              div null,
                if result.type == 'Survey::FiveStarQuestion' or result.type == 'Survey::TenStarQuestion'
                  result.description + " : " + result.int_value
                else if result.type == 'Survey::LongTextQuestion'
                  result.description + " : " + result.string_value
          if job?.review?.survey? and job.review.survey_results.length == 0
            "Request Sent"
      ), show: (job.review?.survey?)}

      {header: "Feedback/Comments", value: (=>
          if job.review? and job.review?.nps_feedback?
            return job.review.nps_feedback
          else if job.review?.rating? and job.review?.replies? and job.review.replies.length > 1
            map(sortBy(job.review.replies, "id").slice(1), (reply) ->
              if reply.body?
                return reply.body
              else
                return ""
            ).join(", ")
        ), show: (job.review? and job.review?.replies?.length > 1)}
      {
        header: "Invoice Total:"
        show: @shouldShowRateEstimate(job, invoice, cost),
        classes: "fs-exclude",
        value: =>
          div
            className: "invoice-amounts",
            span null,
              Utils.formatMoney(cost, invoice.currency)
            if invoice.state in [Consts.IONSITEPAYMENT, Consts.ICREATED, Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT] && (UserStore.isPartner() || UserStore.isSwoop())
              Button
                color: "secondary-o"
                onClick: partial(@editInvoice, invoice)
                size: "narrow small"
                style: {
                  fontSize: 11,
                  margin: '0 2px',
                }
                "Edit Invoice"
            if invoice.state in [Consts.IONSITEPAYMENT, Consts.ICREATED, Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT]
              Button
                color: "secondary-o"
                onClick: partial(Utils.showInvoice, invoice)
                size: "narrow small"
                style: {
                  fontSize: 11,
                  margin: '0 2px',
                }
                "View Invoice"
      }
      {
        header: "Invoice Balance:"
        show: @shouldShowRateEstimate(job, invoice, cost),
        classes: "fs-exclude",
        value: =>
          balance = Utils.getInvoiceBalance(invoice)
          showChargeCard = shouldShowChargeCard(invoice.id, @getId())
          handleButtonClick = if showChargeCard then @showChargeCardModal else @showSetupCardModal
          div
            className: "invoice-amounts"
            span null,
              Utils.formatMoney(balance, invoice.currency)
            if (showChargeCard || !isBankingSetUp(@getId())) && !Utils.isSwoopJob(UserStore, job) && !(UserStore.isPartner() && job?.type == Consts.FLEET_IN_HOUSE_JOB)
              Button
                color: "green"
                onClick: partial(handleButtonClick, invoice.id)
                size: "narrow small"
                style: {
                  fontSize: 11,
                  margin: '0 2px',
                }
                "Charge Card"
      }

      {
        header: 'Rate:',
        value: =>
          invoice = Utils.getDisplayInvoice(job, UserStore.isRoot())

          if invoice
            line_item = InvoiceStore.findLineItemByType(invoice.id, Consts.PORT_TO_PORT_HOURS)

            if line_item
              if Utils.toFixed(line_item.unit_price, 2) == "0.00"
                return undefined
              else
                '$' + line_item.unit_price + ' / hr'
        ,
        show: rate_type == Consts.HOURLY_P2P
      }

      # MILES
      # MILEAGE_TOWED = "MilesTowedRate"
      # MILEAGE_EN_ROUTE = "MilesEnRouteRate"
      # MILEAGE_P2P = "MilesP2PRate"

      #TIMES
      {header: "HQ to Pick Up:", key: "hqtopickupmin",  value: (if job.mins_ab? then job.mins_ab + " min")}
      {header: "Time On Site:", value: (if job.mins_on_site? then job.mins_on_site + " min")}
      {header: "Pick Up to Drop Off:", key: "pickuptodropoffmin", value: (if job.mins_bc? then job.mins_bc + " min")}
      {header: "Time at Drop Off:", value: (if job.mins_at_dropoff? then job.mins_at_dropoff + " min")}
      {header: "Drop Off to HQ:", key: "dropofftoqhmin", value: (if job.mins_ca? then job.mins_ca + " min")}
      {header: "Pick Up to HQ:", key: "pickuptohqmin", value: (if job.mins_ba? then job.mins_ba + " min")}
      {
        header: "Total Time:",
        key: "totaltimemins",
        value: => JobStore.totalTimeFormatted(job.id, @getId()),
        show: rate_type == Consts.HOURLY_P2P
      }
      {
        header: "Total Time (Invoice):",
        key: "totaltimeinvoicemins",
        value: =>
          if invoice?
            line_item = InvoiceStore.findLineItemByType(invoice.id, Consts.PORT_TO_PORT_HOURS)

            if line_item
              Utils.formatMinsInTotalHours(parseFloat(line_item.quantity)*60, true)
        ,
        show: @shouldShowTotalTimeInvoice(job, invoice, Consts.HOURLY_P2P)
      }

      #Distances
      {
        header: "HQ to Pick Up:",
        key: "hqtopickupmiles",
        value: ->
          if job.miles_ab?
            formatDistance(job.miles_ab, distanceUnit)
      }
      {
        header: "Pick Up to Drop Off:",
        key: "pickuptodropoffmiles",
        value: ->
          if job.miles_bc?
            formatDistance(job.miles_bc, distanceUnit)
      }
      {
        header: "Drop Off to HQ:",
        key: "dropofftoqhmiles",
        value: ->
          if job.miles_ca?
            formatDistance(job.miles_ca, distanceUnit)
      }
      {
        header: "Pick Up to HQ:",
        key: "pickuptohqmiles",
        value: ->
          if job.miles_ba?
            formatDistance(job.miles_ba, distanceUnit)
      }

      {
        header: "Pick Up to Drop Off:",
        key: "pickuptodropoffdistance",
        value: ->
          if job.miles_bc?
            formatDistance(job.miles_bc, distanceUnit)
      }


      #release to info
      {header: "Release To:", classes: "fs-exclude", value: if job.storage?.released_to_account_id then AccountStore.get(job.storage.released_to_account_id, 'name', @getId()) }
      {header: "Release To User Type:", value: job.storage?.released_to_user?.storage_relationship}
      {header: "Release To Name:", classes: "fs-exclude", value: JobStore.storageReleasedToUserName(job.id) }
      {header: "Release To Address:", classes: "fs-exclude", value: job.storage?.released_to_user?.location?.address}
      {header: "Release To Phone:", classes: "fs-exclude", value: job.storage?.released_to_user?.phone}
      {header: "Release To Email:", classes: "fs-exclude", value: job.storage?.released_to_email}
      {header: "Driver License State:", value: job.storage?.released_to_user?.license_state}
      {header: "Driver License ID:", classes: "fs-exclude", value: job.storage?.released_to_user?.license_id}
      {header: "Replace Customer Info:", value: if job.storage?.released_to_user?.replace_invoice_info then "✓"}
      {header: "Experiments:", classes: "experiments",  value: (=>
        div null,
          for experiment in job.experiments
            div null,
              "#{experiment.experiment_name}: #{experiment.variant_name}"
      ), show: UserStore.isSwoop() and job.experiments?.length > 0}
      {
        header: "Auto Assign:",
        value: (
          if job.auction?.status == 'successful'
            "Success: #{job.assignment?.company_name}"
          else if job.auction?.result
            "Failure - #{job.auction?.result}"
          else
            "Failure"
        ),
        show: UserStore.isSwoop() && job.auction?.status in ['successful', 'unsuccessful'],
      }
      {header: "Candidates:", classes: "candidates",  value: (=>

        # cleanup w/ ENG-9792
        candidates = job.candidates || job.auto_assign_candidates

        div null,
          for candidate in candidates
            siteOrTruck = if Consts.TRUCK_CANDIDATE.has(candidate.type) then "Truck: #{candidate.vehicle?.name}" else "Site: #{candidate.site?.name}"
            div
              key: siteOrTruck
              "#{candidate.company_name} | #{siteOrTruck} (#{candidate.vehicle_eligible}) | Cost: "
              Utils.formatMoney(candidate.cost)
              " | Calculated ETA: #{candidate.eta} | Quality: #{candidate.rating} | Score: #{Math.ceil(candidate.swoop_score * 100)}"
      ), show: UserStore.isSwoop() and (job.candidates? || job.auto_assign_candidates?) and (@props.treatments?[Consts.SPLITS.DISABLE_AUTO_ASSIGN_DETAILS] != false)
      }

      {header: "Bids:", classes: "bids", value: (=>
        div null,
          for bid in job.auction.bids
            companyName = CompanyStore.get(bid.company_id, 'name', @getId())
            div
              key: bid.company_id
              if bid.company_id && companyName
                if bid.status == Consts.BID_EXPIRED.STATUS
                  "#{companyName} | Status: #{bid.status}"
                else
                  [
                    "#{companyName} | Status: #{bid.status} | Cost: ",
                    if bid.cost then Utils.formatMoney(bid.cost) else "N/A",
                    " | Bid ETA: #{bid.submitted_eta || "N/A"} | Quality: #{bid.rating || "N/A"} | Score: #{Math.ceil(bid.swoop_score*100)}"
                  ]
              else
                'loading...'
      ), show: UserStore.isSwoop() and job.auction? and (@props.treatments?[Consts.SPLITS.DISABLE_AUTO_ASSIGN_DETAILS] != false)}

      {header: "Original ID:", show: UserStore.isFleet() || UserStore.isSwoop(), value: if job.parent_id? then "#" + job.parent_id}
      {header: "Statuses:", value: (=> return (<JobHistory record={job} showExtended={false} hideUser/>)), show: job.history?}
      {
        header: "Track:"
        show: UserStore.isSwoop() and (job.status in [Consts.DISPATCHED, Consts.COMPLETED, Consts.CANCELED, Consts.GOA])
        value:
          ReactDOMFactories.a
            href: "/get_location/#{job.uuid}"
            'Technician'
      }
      {
        header: "Issues:",
        value: (=> return IssueList(job_id: job.id)),
        show: (FeatureStore.isFeatureEnabled(Consts.FEATURES_ISSUES) and
              (IssueStore.hasPermissions())) || ((!UserStore.isSwoop() and !UserStore.isFleetInHouse()) and
              job.explanations?.length > 0)
      }
      {header: "Images:", show: job.images? and job.images.length > 0, value: =>
        ul
          className: "images"
          map(uniq(job.images, (image) =>
            return image.url
          ), (image, index) =>
            li
              key: image.id
              ReactDOMFactories.a
                className: "image_link"
                href: image.url
                target: "_blank"
                Utils.formatDateTime(image.creted_at)
              if image.lat? and image.lng
                ReactDOMFactories.a
                  className: "map_link"
                  href: "http://maps.google.com/maps?q=loc:"+image.lat+"+"+image.lng
                  target: "_blank"
                  "(Map)"
              if UserStore.isPartner()
                ReactDOMFactories.i
                  className: "fa fa-times-circle"
                  key: image.url
                  onClick: partial(@deleteImage, job, image)
          )
      }
      {header: "Pickup Photos:", value_styles: {overflow: "visible"}, value: => @getPhotoCarousel(job.id, 'pickup')}
      {header: "Drop Off Photos:", value_styles: {overflow: "visible"}, value: => @getPhotoCarousel(job.id, 'dropoff')}
      {header: "Documents:", value: DocumentList(job_id: job.id), show: if UserStore.isPartner() then !AuctionStore.jobHasLiveAuction(job) else true }
    ]
    firstRendered = true
    data = filter(data, ((col) -> not col.show? or col.show))

    div
      className: 'table table-details'
      if Utils.isPartnerOnSwoopJob(UserStore, job) && AuctionStore.jobHasLiveAuction(job)
        div
          className: 'details_row'
          div
            className: 'details-dont-accept-job'
            'Exact location available if ETA accepted'
      for row in data
        show = (not row.show? or row.show)
        value = row.value
        if typeof value == "function" and show
          value = value()
        if show and value and ((typeof value != "string") or value.length > 0)
          div
            className: "details_row",
            key: "details_"+(if row.key then row.key else row.header)
            style:
              if row.row_styles? then row.row_styles else {}
            div
              className: if row.header == 'Documents:' then 'details-header documents-header' else 'details-header'
              row.header
            div
              className: 'details-status ' + if row.classes? then row.classes else "" + if row.multiline then " multiline" else ""
              style:
                if row.value_styles? then row.value_styles else {}
              value
            if firstRendered
              #Putting the job id here so highlighting text works intuitively
              firstRendered = false
              span
                style:
                  position: "absolute"
                  right: 22
                  top: 22
                  color: "#ccc"
                  marginTop: 2
                " #"+JobStore.getId(job)

module.exports = {
  JobDetails: JobDetails,
  default: withModalContext(withSplitIO(JobDetails)),
}
