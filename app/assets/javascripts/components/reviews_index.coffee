import React from 'react'
require('stylesheets/components/reviews_index.scss')
import EventRegistry from 'EventRegistry'
UserSettingStore = require('stores/user_setting_store')
CompanyStore = require('stores/company_store').default
UserStore = require('stores/user_store')
JobList = React.createFactory(require('components/job/job_list'))
FTable = React.createFactory((require 'components/tables/SortTable').Table)
{Column} = (require 'components/tables/SortTable')
import SearchBox from 'components/search_box'
BaseComponent = require('components/base_component')
JobStore = require('stores/job_store')
ReviewFilter = require('components/filters/review_filter')
DashboardCols = require('dashboard_cols')
FeatureStore = require('stores/feature_store')
FleetCompanyFilter = React.createFactory(require 'components/filters/fleet_company_filter')
UserSettingStore = require('stores/user_setting_store')
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
import Utils from 'utils'
import { get, uniq, without } from 'lodash'
import Consts from 'consts'

class ReviewsIndex extends BaseComponent
  displayName: 'ReviewsIndex'
  constructor: ->
    super(arguments...)
    @register(JobStore, JobStore.SEARCH_CHANGED)
    @register(JobStore, JobStore.SEARCH_LOADED)
    @register(FeatureStore, FeatureStore.LOADED)

    #Call update search when settingChangeWatcherId watcher is triggered
    @settingChangeWatcherId = @createNewWatcher(@updateSearch)

    if UserSettingStore.isAllLoaded()
      @setup()
    else
      @register(UserSettingStore, UserSettingStore.LOADED, @setup)

  setup: () =>
    @setupColumns()
    @updateSearch()

  setupColumns: ->
    @cols = {}

    job_cols = DashboardCols.getCols({
      isSortable: -> false
      isFrontendSort: -> false
      isExpanded: -> false
      componentId: @getId()
    })

    for col_index in @getColNames()
      if job_cols[col_index]?
        @cols[col_index] = new job_cols[col_index]()

  componentDidMount: ->
    super(arguments...)


  componentWillUnmount: ->
    super(arguments...)
    JobStore.resetSearchResult()

  updateSearch: =>
    EventRegistry.clearListeners(@settingChangeWatcherId)

    filtered_ratings = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_REVIEWS, @settingChangeWatcherId)
    filtered_companies = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_FLEET_COMPANIES_FOR_REVIEWS, @settingChangeWatcherId)

    @search(filtered_ratings, filtered_companies)


  getColNames: (jobs) ->
    #TODO: memoize this
    cols_to_show = ["id", "completed_date"]
    if UserStore.isSwoop()
      cols_to_show.push("company")

    #Only shows nps values that are in result set
    if jobs
      showMap = {
        "q1" : "review.nps_question1"
        "q2" : "review.nps_question2"
        "q3" : "review.nps_question3"
        "feedback" : "review.feedback"
      }
      for col_key, job_key of showMap
        for job in jobs
          if get(job, job_key)?
            cols_to_show.push(col_key)
            break

    cols_to_show.push("customer_name", "phone", "estimated_toa", "actual_toa")
    return cols_to_show

  search: (filtered_ratings, filtered_companies) ->
    #Wait for user to finish loading before search
    if !UserStore.getUser()?
      return

    order = 'id,desc'
    names = @getColNames()
    props = ["id"]
    filters = reviews: true
    if UserStore.isSwoop()
      filters.fleet_managed_jobs = true

    if filtered_ratings
      filters.filter_q1_scores = filtered_ratings
    else
      filters.filter_q1_scores = [0..10].concat([null])
    if filtered_companies?.length > 0
      filters.filter_owner_companies = without(filtered_companies, "None")

    for name in names
      usedProps = @cols[name]?.getUsedProperties()
      if usedProps?
        props.push.apply(props, usedProps)

    filters.fields = uniq(props).join(",")

    JobStore.search('', filters, order, 1)

  render: ->
    super(arguments...)
    jobs = if JobStore.isLoadingSearch() then [] else JobStore.getRecordsOfCurrentSearchPage()

    div
      className: 'reviews_index'
      <SearchBox
        onChange={@updateSearch}
        store={JobStore}
        enablePagination
        showSearch={false}
        show
      />
      if FeatureStore.isFeatureEnabled(Consts.FEATURE_REVIEW_FEED_Q1_FILTER)
        ReviewFilter()
      if UserStore.isSwoop()
        FleetCompanyFilter
          setting_name: Consts.USER_SETTING.FILTERED_FLEET_COMPANIES_FOR_REVIEWS
      div
        className: 'table_container'
        JobList
          jobs: jobs
          tableKey: "review_jobs"
          searching:  JobStore.isLoadingSearch()
          colNames: @getColNames(jobs)
          sortable: false
          rowStyle: {}
          parentType: "review"
      if JobStore.getSearch().allLoaded and JobStore.getSearch().total == 0
        span
          className: "empty_message"
          'No search results found'
      if JobStore.isLoadingSearch()
        Loading null


module.exports = ReviewsIndex
