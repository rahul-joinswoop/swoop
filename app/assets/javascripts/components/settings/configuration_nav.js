import React from 'react'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import FeatureStore from 'stores/feature_store'
import UserStore from 'stores/user_store'
import { withSplitIO } from 'components/split.io'
import 'stylesheets/components/settings/settings-configuration-nav.scss'

const ConfigurationNavButton = (props) => {
  if (props.display) {
    return (
      <li>
        <button
          className={props.current === props.target ? 'current' : null}
          onClick={() => props.onClick(props.target)}
          style={props.style}
          type="button"
        >
          {props.name}
        </button>
      </li>
    )
  } else {
    return null
  }
}

class ConfigurationNav extends BaseComponent {
  render() {
    return (
      <div className="settings--configure--navigation">
        <ul>
          <li><strong>My Settings</strong></li>
          <ConfigurationNavButton
            current={this.props.current}
            display
            name="My Notifications"
            onClick={this.props.onClick}
            target="myNotifications"
          />
          <ConfigurationNavButton
            current={this.props.current}
            display
            name="Time Zone"
            onClick={this.props.onClick}
            target="timezone_settings"
          />
          <ConfigurationNavButton
            current={this.props.current}
            display
            name="Language"
            onClick={this.props.onClick}
            target="languageSetting"
          />
          <ConfigurationNavButton
            current={this.props.current}
            display
            name="Dashboard Columns"
            onClick={this.props.onClick}
            target="dashboardcolumns_settings"
          />
        </ul>

        { (UserStore.isRoot() || UserStore.isAdmin()) &&
          <ul>
            <li><strong>Company Settings</strong></li>
            <ConfigurationNavButton
              current={this.props.current}
              display
              name="Company Notifications"
              onClick={this.props.onClick}
              target="companyNotificationsRef"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display
              name="Dashboard Setup"
              onClick={this.props.onClick}
              target="dashboardSetup"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display={UserStore.isPartner()}
              name="Sub-Companies"
              onClick={this.props.onClick}
              target="subCompanies"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display={UserStore.isPartner()}
              name="Territory"
              onClick={this.props.onClick}
              style={{ display: this.props.treatments && this.props.treatments[Consts.SPLITS.TERRITORIES_PARTNER_MAP] ? null : 'none' }}
              target="territories"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display={UserStore.isPartner()}
              name="Invoices &amp; Payments"
              onClick={this.props.onClick}
              target="invoicesPayments"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display={UserStore.isPartner()}
              name="Commissions"
              onClick={this.props.onClick}
              target="commissions"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display
              name="Custom Fields"
              onClick={this.props.onClick}
              target="customFields"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display={UserStore.isPartner() || UserStore.isSwoop()}
              name="Custom Waivers"
              onClick={this.props.onClick}
              target="customWaivers"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display={UserStore.isPartner() && FeatureStore.isFeatureEnabled(Consts.FEATURE_QUICKBOOK)}
              name="QuickBooks"
              onClick={this.props.onClick}
              target="quickbooks"
            />
            <ConfigurationNavButton
              current={this.props.current}
              display={UserStore.isPartner()}
              name="General Settings"
              onClick={this.props.onClick}
              target="generalSettings"
            />
          </ul>}
      </div>
    )
  }
}

export default withSplitIO(ConfigurationNav)
