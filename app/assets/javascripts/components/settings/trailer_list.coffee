import React from 'react'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Logger from 'lib/swoop/logger'
import { extend, values } from 'lodash'
BaseComponent = require('components/base_component')
FTable = React.createFactory(require 'components/resizableTable/FTable')
TrailerStore = require('stores/trailer_store')
VehicleCategoryStore = require('stores/vehicle_category_store')

ICON_COL_WIDTH = 40
SMALL_WIDTH = 30

class TrailerList extends BaseComponent
  displayName: 'TrailerList'

  cols: [
          {header: "Name", key: "name"}
          {header: "", key: "edit", width: ICON_COL_WIDTH, sortable: false, func: (truck, props) ->
            FontAwesomeButton
              color: 'secondary-o'
              icon: 'fa-pencil'
              name: 'edit'
              onClick: if @? and @props? then $.proxy(@props.editEntry, null, truck)
              size: 'medium'
          }
          {header: "Make", key: "make"}
          {header: "Model", key: "model"}
          {header: "Type", key: "type", func: (truck, props) ->
            VehicleCategoryStore.get(truck["vehicle_category_id"], 'name', @props.componentId)
          }
          {header: "", key: "delete", width: ICON_COL_WIDTH, sortable: false, func: (truck, props) ->
            FontAwesomeButton
              color: 'secondary-o'
              icon: 'fa-times'
              name: 'delete'
              onClick: if @? and @props? then $.proxy(@props.removeEntry, null, truck, { noun: 'trailer', title: 'Delete Trailer' })
              size: 'medium'
          }
        ]

  render: ->
    super(arguments...)

    trailers = values(TrailerStore.getAll(true, @getId(), ['name', 'make', 'model']))
    Logger.debug("FOUND TRAILERS: ", trailers)
    maxWidth = 800
    FTable extend({}, @props,
      rows: trailers
      cols: @cols
      maxWidth: maxWidth
      sortKey: "name"
      sortDir: 1
      componentId: @getId()
    )

module.exports = TrailerList
