import React from 'react'
import Consts from 'consts'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Utils from 'utils'
import { extend, filter, map, values } from 'lodash'
BaseComponent = require('components/base_component')
FTable = React.createFactory(require 'components/resizableTable/FTable')
SiteStore = require('stores/site_store').default
TableColumns = require('components/resizableTable/columns')
UserStore = require('stores/user_store')

import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

ICON_COL_WIDTH = 40

class SiteList extends BaseComponent
  displayName: 'SiteList'
  getCols: =>
     [
        {
          header: "Name"
          key: "name"
          func: (site) =>
            TableColumns.NameWithPin(name: site.name, { iconClasses: 'blue' })
        }
        {
          header: "Location Type"
          width: 150
          key: "location.location_type_id"
          show: UserStore.isFleet()
          func: (site) =>
            TableColumns.LocationName(location: site.location)
        }
        {header: "Address", key: "location.address" }
        {header: "Phone", key: "phone", width: 110, func: (site) => Utils.renderPhoneNumber(site.phone) }
        {
          header: UserStore.siteCodeLabel()
          key: "site_code"
          width: TableColumns.NARROW_COL_WIDTH
          show: UserStore.isFleet()
        }
        {header: "Monday-Friday", width: 130, key: "times", show: UserStore.isPartner(), func: (site) =>
          if site.always_open
            return "24/7"

          if not site.open_time or not site.close_time
            "Answering Service"
          else
            site.open_time + " - "+ site.close_time
        }
        {header: "Saturday", width: 130, key: "sattimes", show: UserStore.isPartner(), func: (site) =>
          if site.always_open
            return "24/7"

          if not site.open_time_sat or not site.close_time_sat
            "Answering Service"
          else
            site.open_time_sat + " - "+ site.close_time_sat
        }
        {header: "Sunday", width: 130, key: "suntimes", show: UserStore.isPartner(), func: (site) =>
          if site.always_open
            return "24/7"

          if not site.open_time_sun or not site.close_time_sun
            "Answering Service"
          else
            site.open_time_sun + " - "+ site.close_time_sun
        }

        {header: "Dispatch", key: "dispatchable", width: 50, show: UserStore.isPartner(), func: (site) => if site.dispatchable then "✓" else "▢"}
        {header: "Tesla Tires", key: "tire_program", width: 50, show: UserStore.isPartner() and SiteStore.hasTeslaAccount(), func: (site) => if site.tire_program then "✓" else "▢"}
        {header: "", key: "edit", width: ICON_COL_WIDTH, sortable: false, func: (site, props) =>
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-pencil'
            name: 'edit'
            onClick: if @? and @props? then $.proxy(@props.editEntry, null, site)
            size: 'medium'
        }
        {header: "", key: "delete", width: ICON_COL_WIDTH, sortable: false, func: (site, props) =>
          siteProviderCompanies = site.providerCompanies

          # if not siteProviderCompanies? or siteProviderCompanies.length == 0
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-times'
            name: 'delete'
            onClick: if @? and @props? then $.proxy(@props.removeEntry, null, {id: site.id}, {
              message: if site.providerCompanies.length > 0 then 'The following accounts will no longer be able to send jobs to this site.',
              noun: 'site',
              providerCompanies: site.providerCompanies,
              title: 'Delete Site',
            })
            size: 'medium'
        }
      ]

  render: ->
    super(arguments...)

    cols = filter(@getCols(), ((col) -> not col.show? or col.show))

    # since we pass all the props to be watched here, values on the rows don't need to pass @getId() to SiteStore calls.
    sites = values SiteStore.loadAndGetAll(@getId(), [
      'name'
      'location.address',
      'location.location_type_id',
      'phone',
      'always_open',
      'open_time',
      'close_time',
      'open_time_sat',
      'close_time_sat',
      'open_time_sun',
      'close_time_sun',
      'dispatchable',
      'tire_program',
      'site_code'
    ])

    if sites.length > 0
      FTable extend(
        {}
        @props
        rows: sites
        cols: cols
        sortKey: "name"
        sortDir: 1
        key: 'sites-table'
        description: Consts.SITE.HELPER_TEXT
      )
    else
      Loading null

module.exports = SiteList
