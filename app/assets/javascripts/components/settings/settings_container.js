import React from 'react'
import BaseComponent from 'components/base_component'
import 'stylesheets/components/settings/settings_container.scss'

class SettingsContainer extends BaseComponent {
  render() {
    return (
      <div className="settings_container_class">
        <h3>{this.props.title}</h3>
        {this.props.children}
      </div>
    )
  }
}

export default SettingsContainer
