import React from 'react'
import BaseConsts from 'base_consts'
import Consts from 'consts'
import Dispatcher from 'lib/swoop/dispatcher'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import ReactDOMFactories from 'react-dom-factories'
import { extend, map, partial } from 'lodash'
AccountStore = require('stores/account_store')
BaseComponent = require('components/base_component')
{ Column } = (require 'components/tables/SortTable')
FTable = React.createFactory((require 'components/tables/SortTable').Table)
ModalStore = require('stores/modal_store')
import SearchBox from 'components/search_box'
UserSettingStore = require('stores/user_setting_store')
UserStore = require('stores/user_store')
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

ICON_COL_WIDTH = 40
SMALL_WIDTH = 30

class AccountList extends BaseComponent
  displayName: 'AccountList'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()

    @register(AccountStore, AccountStore.CHANGE)
    @register(AccountStore, AccountStore.SEARCH_CHANGED)

  getInitialState: ->
    that = this

    cols = [
      class Name extends Column
        _header: "Name", _key: "name"
        getValue: ->
          name = AccountStore.getAccountKey(@record?.id, "name", that.getId())
          if @record?.color?
           span
             className: "account_wrapper"
             style:
               backgroundColor: @record.color
             name
          else
            name

      class Edit extends Column
        _header: "", _key: "edit", _originalWidth: ICON_COL_WIDTH
        getValue: ->
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-pencil'
            name: 'edit'
            onClick: partial(that.props.editEntry, @record)
        isShowing: -> !UserStore.isOnlyDispatcher()

      class Address extends Column
        _header: "Address", _key: "address"
        getValue: ->
          if !@record?
            return ''

          AccountStore.getAccountKey(@record?.id, "location.address", that.getId())

      class PrimaryPhone extends Column
        _header: "Primary Phone", _key: "phone"
        getValue: -> AccountStore.getAccountKey(@record?.id, "phone", that.getId())

      class PrimaryEmail extends Column
        _header: "Primary Email", _key: "primary_email"
        getValue: -> AccountStore.getAccountPrimaryEmail(@record?.id, that.getId())

      class AccountingEmail extends Column
        _header: "Accounting Email", _key: "accounting_email"
        getValue: -> @record?.accounting_email || AccountStore.getAccountKey(@record?.id, "accounting_email", that.getId())

      class Notes extends Column
        _header: "Notes", _key: "notes"

      class Delete extends Column
        _header: "", _key: "delete", _originalWidth: ICON_COL_WIDTH
        getValue: ->
          record = @record
          deletionVerbage = { noun: 'account', title: 'Delete Account' }
          if record?.name != BaseConsts.CASHCALL and record?.name != BaseConsts.SWOOP and record?.name != BaseConsts.AGERO
            FontAwesomeButton
              color: 'secondary-o'
              icon: 'fa-times'
              name: 'delete'
              onClick: ->
                isMotorClub = record.name in Consts.MOTORCLUBS
                if isMotorClub
                  sites = record.dispatchable_sites
                  vendorConnected = sites.some((site) => site.isscs.some((issc) => issc.status in [
                    'logged_in',
                    'logging_in',
                    'logging_out',
                    'registering',
                    'deregistering',
                  ]))
                  if not vendorConnected
                    that.props.removeEntry(record, deletionVerbage)
                  else
                    Dispatcher.send(ModalStore.SHOW_MODAL,
                      key: 'simple',
                      props:
                        title: 'Cannot Delete Account'
                        body: 'Cannot delete Account until active Vendor IDs have been deregistered. Must deregister all Vendor IDs before deleting Account.'
                        confirm: null
                        cancel: 'OK'
                    )
                else
                  that.props.removeEntry(record, deletionVerbage)
              size: 'medium'
        isShowing: -> !UserStore.isOnlyDispatcher()
    ]

    @cols = map(cols, (col) -> new col())

    lastSortBy = UserSettingStore.getUserSettingByKey(BaseConsts.USER_SETTING.ACCOUNT_SORTBY)

    initialSortBy = if lastSortBy then JSON.parse(lastSortBy) else undefined
    initialSortKey = initialSortBy?.key || "id"
    initialSortOrder = if initialSortBy?.order == 1 then "asc" else "desc"

    return {
      searchKey: initialSortKey
      searchOrder: initialSortOrder
      search: ' '
      pageSize: BaseConsts.ACCOUNT_PAGE_SIZE
    }

  componentDidMount: ->
    super(arguments...)

    # it will trigger 'load the first page'
    @searchChange(@state.search)

  componentWillUnmount: ->
    super(arguments...)

    AccountStore.resetSearchResult()

  sortChange: (key, order) ->
    searchOrder = if order == 1 then "asc" else "desc"

    @setState
      searchKey: key
      searchOrder: searchOrder

    @search(@state.search, AccountStore.getSearch().searchPage)

  searchChange: (value) =>
    if(value == null)
      value = ' '

    @search(value, 1)

    @setState
      search: value

  search: (value, page) ->
    order = @state.searchKey + ',' + @state.searchOrder

    AccountStore.search(value, null, order, page)

  render: ->
    super(arguments...)

    ReactDOMFactories.div
      className: 'accounts_container'
      <SearchBox
        onChange={@searchChange}
        store={AccountStore}
        enablePagination
        showSearch
        show
      />
      ReactDOMFactories.div
        style:
          clear: "both"
        FTable extend({}, @props,
          tableKey: "accounts"
          rows: if AccountStore.isLoadingSearch() then [] else AccountStore.getRecordsOfCurrentSearchPage()
          cols: @cols
          originalSortDir: @state.searchOrder
          originalSortKey: @state.searchKey
          onSortChange: partial(@sortChange)
          componentId: @getId()
          rowClick: -> # just to avoid BaseTable throwing error
        )
      if AccountStore.getSearch().allLoaded and AccountStore.getSearch().total == 0 and @state?.search?
        span
          className: "empty_message"
          'No search results found'

      if AccountStore.isLoadingSearch()
        Loading null

module.exports = AccountList
