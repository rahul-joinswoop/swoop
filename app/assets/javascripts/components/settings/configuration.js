import React from 'react'
import BaseComponent from 'components/base_component'
import BaseConsts from 'base_consts'
import Commissions from 'components/settings/configure/company_settings/commissions'
import Companies from 'components/settings/configure/company_settings/companies'
import ConfigurationNav from 'components/settings/configuration_nav'
import Consts from 'consts'
import DashboardColors from 'components/settings/configure/company_settings/dashboard_colors'
import DashboardColumns from 'components/settings/configure/my_settings/dashboard_columns'
import DashboardFilters from 'components/settings/configure/company_settings/dashboard_filters'
import DashboardTabs from 'components/settings/configure/company_settings/dashboard_tabs'
import Departments from 'components/settings/configure/company_settings/departments'
import EmailSettings from 'components/settings/configure/my_settings/email'
import FeatureStore from 'stores/feature_store'
import GeneralSettings from 'components/settings/configure/company_settings/general'
import Logo from 'components/settings/configure/company_settings/logo'
import NetworkStatus from 'components/settings/configure/company_settings/network_status'
import Notifications from 'components/settings/configure/company_settings/notifications'
import NotificationSettings from 'components/settings/configure/my_settings/notification_settings'
import ObjectSection from 'components/settings/configure/object_section'
import PaymentPermissions from 'components/settings/configure/company_settings/paymentPermissions'
import Quickbooks from 'components/settings/configure/company_settings/quickbooks'
import ServiceStore from 'stores/service_store'
import StorageTypes from 'components/settings/configure/company_settings/storage_types'
import StripeSettings from 'components/stripeBankAccount/stripeSettings'
import SymptomStore from 'stores/symptom_store'
import TerritoriesPartnerMap from 'components/maps/Territories/TerritoriesPartnerMap'
import TimeZone from 'components/settings/configure/my_settings/time_zone'
import Language from 'components/settings/configure/my_settings/language'
import UserStore from 'stores/user_store'
import Utils from 'utils'
import VehicleCategoryStore from 'stores/vehicle_category_store'
import Waivers from 'components/settings/configure/company_settings/waivers'
import { withSplitIO } from 'components/split.io'
import { debounce, map, sortBy, without } from 'lodash'
import 'stylesheets/components/settings/settings--configure.scss'

class Configuration extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      current: null,
      scrolling: false,
    }
    this.containerRef = React.createRef()
    this.childSectionsRefs = {}
    this.myNotifications = React.createRef()
    this.companyNotificationsRef = React.createRef()
    this.dashboardSetup = React.createRef()
    this.subCompanies = React.createRef()
    this.invoicesPayments = React.createRef()
    this.commissions = React.createRef()
    this.customFields = React.createRef()
    this.customWaivers = React.createRef()
    this.quickbooks = React.createRef()
    this.generalSettings = React.createRef()
    this.territories = React.createRef()
  }

  componentDidMount() {
    const wrapper = document.scrollingElement || document.documentElement
    wrapper.addEventListener('scroll', debounce(this.queryScrolledTo, 100))
  }

  componentWillUnmount() {
    const wrapper = document.scrollingElement || document.documentElement
    wrapper.removeEventListener('scroll', debounce(this.queryScrolledTo, 100))
  }

  queryScrolledTo = () => {
    if (this.state.scrolling) {
      this.setState({
        scrolling: false,
      })
    } else {
      this.setState({
        current: null,
      })
    }
  }

  handleScroll = (target) => {
    const wrapper = document.scrollingElement || document.documentElement
    const container = this.containerRef.current
    const element = this.childSectionsRefs[target] || this[target].current

    this.setState({
      current: target,
      scrolling: true,
    }, () => {
      wrapper.scrollTo({
        top: (element.offsetTop + container.getBoundingClientRect().top + wrapper.scrollTop),
        behavior: 'smooth',
      })
    })
  }

  setRef = (name, ref) => {
    this.childSectionsRefs[name] = ref
  }

  servicesGetItems(componentId) {
    return without(map(sortBy(ServiceStore.getServices(componentId), service => service.name.toLowerCase()), service => service.name), 'GOA')
  }

  servicesIsLoading() {
    return UserStore.getCompany().service_ids.length > 0
  }

  classTypesGetItems(componentId) {
    return map(sortBy(VehicleCategoryStore.getCompanyItems(componentId), cat => cat.name.toLowerCase()), service => service.name)
  }

  classTypesIsLoading() {
    return UserStore.getCompany().vehicle_categories.length > 0
  }

  additionalItemsGetItems(componentId) {
    return map(sortBy(ServiceStore.getAddons(componentId), cat => cat.name.toLowerCase()), service => service.name)
  }

  additionalItemsisLoading() {
    return UserStore.getCompany().addon_ids.length > 0
  }

  symptomsGetItems(componentId) {
    return map(sortBy(SymptomStore.getCompanyItems(componentId), symptom => symptom.name.toLowerCase()), symptom => symptom.name)
  }

  symptomsIsLoading() {
    return UserStore.getCompany().symptoms.length > 0
  }

  render() {
    return (
      <div id="settings-configure-container" ref={this.containerRef}>
        <ConfigurationNav
          current={this.state.current}
          onClick={this.handleScroll}
        />

        <div id="settings-configure">

          <div id="settings--configure--my-settings">
            <div className="configure-component">
              <h3 className="configure-section-heading" ref={this.myNotifications}>My Notifications</h3>

              { (UserStore.isPartner()) &&
                <div id="settings--configure--my-notifications">
                  <NotificationSettings setRef={this.setRef} />
                </div>}
              <div id="settings--configure--my-email-settings">
                <EmailSettings setRef={this.setRef} />
              </div>
              <TimeZone setRef={this.setRef} />
              <Language setRef={this.setRef} />
              <DashboardColumns setRef={this.setRef} />

            </div>
          </div>

          { (UserStore.isAdmin() || UserStore.isRoot()) &&
            <div id="settings--configure--company-settings">
              <div id="settings--configure--company-notifications" ref={this.companyNotificationsRef}>
                <Notifications />
              </div>
              <div id="settings--configure--dashboard-setup" ref={this.dashboardSetup}>
                <h3 className="configure-section-heading">Dashboard Setup</h3>
                { (UserStore.isPartner() || UserStore.isFleet()) &&
                  <Logo />}
                <DashboardTabs />
                <DashboardFilters />
                <DashboardColors />
              </div>
              { UserStore.isPartner() &&
                <>
                  <div id="settings--configure--sub-companies" ref={this.subCompanies}>
                    <h3 className="configure-section-heading">Sub-Companies</h3>
                    <Companies />
                  </div>
                  { this.props.treatments && this.props.treatments[Consts.SPLITS.TERRITORIES_PARTNER_MAP] && (
                    <div id="territories" ref={this.territories}>
                      <TerritoriesPartnerMap />
                    </div>
                  )}
                  <div id="settings--configure--invoices-and-payments" ref={this.invoicesPayments}>
                    <h3 className="configure-section-heading">Invoices &amp; Payments</h3>
                    { UserStore.userHasPaymentSettingsEnabled() &&
                      <StripeSettings />}
                    <PaymentPermissions />
                  </div>
                  <div id="settings--configure--commissions" ref={this.commissions}>
                    <h3 className="configure-section-heading">Commissions</h3>
                    <Commissions />
                  </div>
                </>}
              <div id="settings--configure--custom-fields" ref={this.customFields}>
                <h3 className="configure-section-heading">Custom Fields</h3>
                { (UserStore.isPartner() || UserStore.isFleet()) &&
                  <ObjectSection
                    description="Services are selectable when creating a job and when setting rates."
                    getItems={this.servicesGetItems}
                    id="services_in_companySettings"
                    isLoading={this.servicesIsLoading}
                    label="Services"
                    modalKey="services"
                    splitIntoThree
                  />}
                { (UserStore.isPartner() || FeatureStore.isFeatureEnabled(BaseConsts.FEATURE_FLEET_CLASS_TYPE)) &&
                  <ObjectSection
                    description="Class Types modify the rate that is applied to the invoice for a particular service."
                    getItems={this.classTypesGetItems}
                    id="class_types_in_companySettings"
                    isLoading={this.classTypesIsLoading}
                    label="Class Types"
                    modalKey="class_type"
                    splitIntoThree
                  />}
                <ObjectSection
                  description="Additional items are selectable when editing invoices and setting rates."
                  getItems={this.additionalItemsGetItems}
                  id="additionalItems_in_companySettings"
                  isLoading={this.additionalItemsisLoading}
                  label="Additional Items"
                  modalKey="additional_items"
                  splitIntoThree
                />
                { (FeatureStore.isFeatureEnabled(BaseConsts.FEATURE_SYMPTOMS) && (UserStore.isPartner() || UserStore.isFleet())) &&
                  <ObjectSection
                    description={`${Utils.getSymptomLabel(UserStore, { plural: true })} are selectable when creating a job.`}
                    getItems={this.symptomsGetItems}
                    id="symptoms_in_companySettings"
                    isLoading={this.symptomsIsLoading}
                    label={Utils.getSymptomLabel(UserStore, { plural: true })}
                    modalKey="symptom"
                    splitIntoThree
                  />}
                {
                  (FeatureStore.isFeatureEnabled(BaseConsts.FEATURES_DEPARTMENTS) &&
                    (UserStore.isPartner() || UserStore.isFleet())
                  ) &&
                  <Departments />
                }
                { (UserStore.isPartner()) &&
                  <StorageTypes />}
                { (UserStore.isSwoop() || UserStore.isFleetInHouse()) &&
                  <NetworkStatus />}
              </div>
              { (UserStore.isPartner() || UserStore.isSwoop()) &&
                <div id="settings--configure--custom-waivers" ref={this.customWaivers}>
                  <h3 className="configure-section-heading">Custom Waivers</h3>
                  <Waivers />
                </div>}
              { UserStore.isPartner() &&
                <>
                  { FeatureStore.isFeatureEnabled(Consts.FEATURE_QUICKBOOK) &&
                    <div id="settings--configure--quickbooks" ref={this.quickbooks}>
                      <h3 className="configure-section-heading">QuickBooks</h3>
                      <Quickbooks />
                    </div>}
                  <div id="settings--configure--general-settings" ref={this.generalSettings}>
                    <h3 className="configure-section-heading">General Settings</h3>
                    <GeneralSettings />
                  </div>
                </>}
            </div>}
        </div>
      </div>
    )
  }
}

export default withSplitIO(Configuration)
