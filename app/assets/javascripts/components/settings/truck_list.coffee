import React from 'react'
import Consts from 'consts'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import moment from 'moment-timezone'
import { extend, filter, values } from 'lodash'
BaseComponent = require('components/base_component')
FeatureStore = require('stores/feature_store')
FTable = React.createFactory(require 'components/resizableTable/FTable')
UserStore = require('stores/user_store')
VehicleCategoryStore = require('stores/vehicle_category_store')
VehicleStore = require('stores/vehicle_store')

ICON_COL_WIDTH = 40
SMALL_WIDTH = 30

class TruckList extends BaseComponent
  displayName: ->
    'TruckList'

  cols: ->
    that = this
    [
      {header: "Name", key: "name"}
      {header: "", key: "edit", width: ICON_COL_WIDTH, sortable: false, func: (truck, props) ->
        FontAwesomeButton
          color: 'secondary-o'
          icon: 'fa-pencil'
          name: 'edit'
          onClick: if @? and @props? then $.proxy(@props.editEntry, null, truck)
          size: 'medium'
      }
      {header: "Make", key: "make"}
      {header: "Model", key: "model"}
      {header: "Year", key: "year"}
      {header: "Type", key: "type", func: (truck, props) ->
        VehicleCategoryStore.get(truck["vehicle_category_id"], 'name', that.getId())
      }
      {
        header: "Auto Dispatch",
        key: "dedicated_to_swoop",
        width: 60,
        show: -> FeatureStore.isFeatureEnabled(Consts.FEATURE_ENABLE_AUTO_DISPATCH_TO_TRUCK),
        func: (truck, props) ->  if truck.dedicated_to_swoop then "✓" else "▢"
      }
      {
        header: "Location Updated"
        key: "location_updated_at"
        func: (truck, props) ->
          if truck.location_updated_at?
            moment(truck.location_updated_at).fromNow()
          else
            "Never"
      }
      {
        header: "", key: "delete", width: ICON_COL_WIDTH, sortable: false, func: (truck, props) ->
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-times'
            name: 'delete'
            onClick: if @? and @props? then $.proxy(@props.removeEntry, null, truck, { noun: 'truck', title: 'Delete Truck'})
            size: 'medium'
      }
    ]

  render: ->
    super(arguments...)

    cols = filter(@cols(), ((col) ->
      if not col.show? or col.show == true
        return true
      if typeof(col.show) == "function"
        return col.show()
    ))

    maxWidth = 800

    # TODO migrate to load the collection dynamically (and maybe ES?) once we stop loading vehicles on userChanged
    watchedAttributes = ['name', 'make', 'model', 'year', 'type', 'location_updated_at', 'dedicated_to_swoop']
    storeTrucks = VehicleStore.getAll(false, @getId(), watchedAttributes)
    trucks = values(storeTrucks)

    FTable extend({}, @props,
      rows: trucks
      cols: cols
      maxWidth: maxWidth
      sortKey: "name"
      sortDir: 1
      componentId: @getId()
      )

module.exports = TruckList
