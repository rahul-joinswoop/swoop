import React from 'react'
import FTable from 'components/resizableTable/FTable'
import {
  DeleteButton, EditButton, NameWithPin, LocationName,
  ICON_COL_WIDTH, NARROW_COL_WIDTH,
} from 'components/resizableTable/columns'
import Utils from 'utils'
import Consts from 'consts'
import PoiStore from 'stores/poi_store'
import BaseComponent from 'components/base_component'
import { partial, values } from 'lodash'
import Loading from 'componentLibrary/Loading'

class PoiSiteList extends BaseComponent {
  render() {
    super.render()

    const poiSites = values(PoiStore.loadAndGetAll(this.getId(), [
      'name',
      'location.address',
      'location.location_type_id',
      'phone',
      'manager.full_name',
    ]))

    const columns = [
      {
        header: 'Name',
        key: 'name',
        func: partial(NameWithPin, partial.placeholder, { iconClasses: 'yellow' }),
      },
      {
        header: 'Location Type',
        width: 150,
        key: 'location.location_type_id',
        func: LocationName,
      },
      {
        header: 'Address',
        key: 'location.address',
      },
      {
        header: 'Phone',
        key: 'phone',
        width: NARROW_COL_WIDTH,
        func: poiSite => Utils.renderPhoneNumber(poiSite.phone),
      },
      {
        header: 'Contact Name',
        key: 'manager.full_name',
      },
      {
        header: '',
        key: 'edit',
        width: ICON_COL_WIDTH,
        sortable: false,
        func: partial(EditButton, partial.placeholder, this.props),
      },
      {
        header: '',
        key: 'delete',
        width: ICON_COL_WIDTH,
        sortable: false,
        func: partial(DeleteButton, partial.placeholder, this.props, { noun: 'place', title: 'Delete Place' }),
      },
    ]

    let componentView = null

    if (PoiStore.isAllLoaded()) {
      componentView = (
        <FTable
          {...this.props}
          cols={columns}
          description={Consts.POI_SITE.HELPER_TEXT}
          rows={poiSites}
          sortDir={1}
          sortKey="name"
        />
      )
    } else {
      componentView = (
        <Loading />
      )
    }

    return componentView
  }
}

export default PoiSiteList
