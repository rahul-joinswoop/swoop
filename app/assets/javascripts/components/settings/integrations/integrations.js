import React from 'react'
import UserStore from 'stores/user_store'
import AgeroIntegration from 'components/settings/integrations/rsc/agero_integration'
import APICredentials from 'components/settings/integrations/API_Credentials/api_credentials'
import 'stylesheets/components/settings/integrations/integrations.scss'

const Integrations = () => (
  <div className="integrations">
    { (UserStore.isAdmin() && (UserStore.isFleetManaged() && !UserStore.isMotorClub())) &&
      <div className="integration" key="APICredentials">
        <APICredentials />
      </div>}
    { (UserStore.isAdmin() && UserStore.isPartner()) &&
      <div className="integration" key="AgeroIntegration">
        <AgeroIntegration />
      </div>}
  </div>
)

export default Integrations
