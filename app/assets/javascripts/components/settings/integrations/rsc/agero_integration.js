import React from 'react'
import BaseComponent from 'components/base_component'
import AgeroLogo from 'public/images/Agero_Logo_Vector.svg'
import SimpleModal from 'components/modals/simple_modal'
import AccountFormModal from 'components/account_form'
import AccountStore from 'stores/account_store'
import AgeroConnections from 'components/settings/integrations/rsc/agero_connections'
import Loading from 'componentLibrary/Loading'
import Api from 'api'
import { every, find } from 'lodash'
import StoreWrapper from 'StoreWrapper'
import Utils from 'utils'
import Button from 'componentLibrary/Button'
import { withModalContext } from 'componentLibrary/Modal'

import 'stylesheets/components/settings/integrations/rsc/agero_integration.scss'

class AgeroIntegration extends BaseComponent {
  renderAddButton() {
    if (this.props.vendorConnections.length > 0) {
      return <i className="fa fa-plus add-button" onClick={() => { this.handleConnectClick(true) }} />
    }
    return null
  }

  sendTrackingAutoConfigureInformation = (vendorConnections) => {
    if (window.location.href.includes('?return-from-rsc-connect')) {
      const configureMessage = every(vendorConnections, ['status', 'connected']) ? 'Success' : 'Failure'
      Tracking.trackRSCEvent(`RSC Auto Configure ${configureMessage}`)
    }
  }

  renderConnectionStatus = () => {
    if (this.props.vendorConnections.length > 0) {
      this.sendTrackingAutoConfigureInformation(this.props.vendorConnections)
      return (<AgeroConnections
        handleConnectClick={this.handleConnectClick}
        handleDeleteConnectionClick={this.handleDeleteConnectionClick}
        vendorConnections={this.props.vendorConnections}
      />)
    }
    return <span className="agero-connections">Receive Agero jobs directly in Swoop</span>
  }

  deleteConnection(vendorId) {
    Api.deleteIntegration(vendorId, {
      success: () => {
        Utils.sendSuccessNotification('Vendor ID successfully disconnected')
        Tracking.trackRSCEvent('RSC Remove Vendor ID')
      },
      error: () => {
        (data, textStatus, errorThrown) => {
          errorMessage = 'An error occurred processing your request. Please contact our support chat (in the bottom right).'
          Utils.sendErrorNotification(errorMessage)
        }
      },
    })
    this.props.hideModals()
  }

  handleDeleteConnectionClick(vendorId) {
    this.props.showModal(
      <SimpleModal
        body="Are you sure you want to disconnect this Vendor ID? You will no longer receive jobs for this Vendor ID in Swoop."
        cancel="Cancel"
        className="disconnect-vendor-id"
        confirm="Disconnect"
        onConfirm={() => { this.deleteConnection(vendorId) }}
        title="Disconnect Vendor ID"
      />
    )
  }

  handleConnectClick(additionalVendorAdd = false) {
    const connectEvent = additionalVendorAdd ? 'RSC Add Vendor ID Click' : 'RSC Connect Click'
    Tracking.trackRSCEvent(connectEvent)
    window.location = `/auth/agero?access_token=${Api.getBearerToken()}`
  }

  handleConfigureClick() {
    this.props.showModal(
      <AccountFormModal
        ageroVendorConnections={this.props.vendorConnections}
        onCancel={() => { this.props.hideModals() }}
        onConfirm={(newObj) => {
          AccountStore.updateItem(newObj)
          this.props.hideModals()
        }}
        record={find(AccountStore.getMotorClubAccounts(this.getId(), 'accountIntegrations'), club => club.name === 'Agero')}
        show
        submitButtonName="Save"
      />
    )
  }

  renderMainButton() {
    if (this.props.vendorConnections.length > 0) {
      return (
        <Button
          color="primary"
          onClick={this.handleConfigureClick}
        >
          Configure
        </Button>
      )
    } else {
      return (
        <Button
          color="primary"
          onClick={this.handleConnectClick}
        >
          Connect
        </Button>
      )
    }
  }

  render() {
    if (this.props.vendorConnections) {
      return (
        <div className="agero-integration">
          <div className="logo-and-content-container">
            <img className="logo" src={AgeroLogo} />
            <div className="content">
              <div className="label-and-add-button-container">
                <span className="agero-label">Agero</span>
                {this.renderAddButton()}
              </div>
              {this.renderConnectionStatus()}
            </div>
          </div>
          {this.renderMainButton()}
        </div>
      )
    } else {
      return <Loading />
    }
  }
}

AgeroIntegration = StoreWrapper(AgeroIntegration, function () {
  const account = find(AccountStore.getMotorClubAccounts(this.getId(), 'accountIntegrations'), club => club.name === 'Agero')
  const vendorConnections = account ? account.accountIntegrations : []
  return { vendorConnections }
})

export default withModalContext(AgeroIntegration)

export const Component = AgeroIntegration
