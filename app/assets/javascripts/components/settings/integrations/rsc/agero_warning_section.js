import React from 'react'
import { difference, forEach, isEmpty, map } from 'lodash'

import 'stylesheets/components/settings/integrations/rsc/agero_warning_section.scss'

const AgeroWarningSection = ({ selectedLocationIds, allAgeroLocations }) => {
  let message = null
  let unusedLocations = null
  const unusedLocationsDisplay = []

  if (!allAgeroLocations || isEmpty(allAgeroLocations)) {
    message = 'Visit Settings > Integrations to connect Agero'
  } else {
    const allLocationIds = map(allAgeroLocations, location => location.locationId)
    unusedLocations = difference(allLocationIds, selectedLocationIds.map(location => parseInt(location, 10)))

    if (!isEmpty(unusedLocations)) {
      message = 'Select Sites for the following Agero Locations'
      forEach(allAgeroLocations, (location) => {
        if (unusedLocations.includes(location.locationId)) {
          unusedLocationsDisplay.push(
            <span className="location" key={location.locationId}>
              {`${location.address}, ${location.zip}`}
            </span>
          )
        }
      })
    }
  }

  if (message) {
    return (
      <div className="agero-warning-section">
        <span className="connect-message">
          <i className="fa fa-exclamation-triangle" />
          <span>{message}</span>
        </span>
        <span className="unused-locations">
          {unusedLocationsDisplay}
        </span>
      </div>
    )
  }

  return null
}

export default AgeroWarningSection
