import React from 'react'
import { forEach, isEmpty } from 'lodash'
import 'stylesheets/components/settings/integrations/rsc/agero_locations_vendor_list.scss'
import Utils from 'utils'

const AgeroLocationsVendorList = ({ vendorStatuses }) => {
  const latestVendorConnection = {}
  const vendorDisplays = []

  forEach(vendorStatuses, (vendorConnection) => {
    latestVendorConnection[vendorConnection.contractorid] = vendorConnection
  })

  forEach(latestVendorConnection, (vendorConnection, vendorId) => {
    const displayStatus = Utils.isscHumanStatus(vendorConnection)
    const colorClass = Utils.isscStatusColor(vendorConnection.status)
    vendorDisplays.push(
      <span className="vendor-id-message" key={vendorId}>
        <span className="vendor-id">{vendorId}</span>
        {displayStatus && (<span className={`status ${colorClass}`}>{displayStatus || ''}</span>)}
      </span>
    )
  })

  if (isEmpty(vendorDisplays)) {
    vendorDisplays.push(
      <span className="vendor-id-message">
        <span className="vendor-id">Select Agero Location</span>
      </span>
    )
  }

  return (
    <div className="agero-locations-vendor-list">
      <div className="vender-label">Vendor IDs</div>
      {vendorDisplays}
    </div>
  )
}

export default AgeroLocationsVendorList
