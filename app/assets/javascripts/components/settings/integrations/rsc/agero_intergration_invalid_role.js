import React from 'react'
import BaseComponent from 'components/base_component'
import Api from 'api'
import Context from 'Context'
import Button from 'componentLibrary/Button'

import 'stylesheets/components/settings/integrations/rsc/agero_integration_invalid_role.scss'

class AgeroIntegrationSuccess extends BaseComponent {
  handleConnectClick() {
    window.location = `/auth/agero?access_token=${Api.getBearerToken()}`
  }

  handleReturnClick() {
    Context.navigate('user/settings', { integrationstab: 'true', 'no-rsc-connect': null })
  }

  render() {
    return (
      <div className="agero-integration-invalid-role">
        <i className="fa fa-exclamation-triangle" />
        <span className="title">Invalid User Role</span>
        <span className="message">Please log in to an <strong>Owner</strong> or <strong>Dispatching</strong> account on Agero to connect with Swoop.</span>
        <span className="button-container">
          <Button onClick={this.handleReturnClick}>Return to Swoop</Button>
          <Button color="primary" onClick={this.handleConnectClick}>Connect Again</Button>
        </span>
      </div>
    )
  }
}

export default AgeroIntegrationSuccess
