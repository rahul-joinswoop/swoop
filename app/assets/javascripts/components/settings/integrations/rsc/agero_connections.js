import React from 'react'
import BaseComponent from 'components/base_component'
import { forEach } from 'lodash'
import Utils from 'utils'
import Button from 'componentLibrary/Button'

import 'stylesheets/components/settings/integrations/rsc/agero_connections.scss'

class AgeroConnections extends BaseComponent {
  render() {
    const connections = []

    forEach(this.props.vendorConnections, (connection) => {
      let messageClassName = 'message'
      let message = 'Connected'
      let reconnectButton = null
      if (connection.status === 'disconnected') {
        messageClassName += ' red'
        message = 'Disconnected'
        reconnectButton = <Button onClick={this.props.handleConnectClick}>Reconnect</Button>
      } else if (connection.status === 'needs_configured') {
        messageClassName += ' orange'
        message = 'Click Configure to complete setup'
      } else {
        message = Utils.isscHumanStatus(connection)
        messageClassName += ` ${Utils.isscStatusColor(connection.status)}`
      }
      connections.push(
        <span className="connection">
          {this.props.readOnly ? null : <i className="fa fa-times-circle" onClick={() => { this.props.handleDeleteConnectionClick(connection.vendorId) }} />}
          <span className="vendor-id-label">Vendor ID:</span>
          <span className="vendor-id">{connection.vendorId}</span>
          <span className={messageClassName}>{message}</span>
          {this.props.readOnly ? null : reconnectButton}
        </span>)
    })

    return <span className="agero-connections">{connections}</span>
  }
}

export default AgeroConnections
