import React, { Component } from 'react'
import Api from 'api'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import SimpleModal from 'components/modals/simple_modal'
import SwoopIcon from 'public/images/Swoop-logo-square.svg'
import Utils from 'utils'
import 'stylesheets/components/settings/integrations/api-credentials/api-credentials.scss'
import { withModalContext } from 'componentLibrary/Modal'

class APICredentials extends Component {
  constructor(props) {
    super(props)
    this.state = {
      credentials: null,
    }
  }

  componentDidMount() {
    Api.getApiCredentials({
      success: (data) => {
        this.setState({
          credentials: data,
        })
      },
      error: () => {
        this.setState({
          credentials: 'error',
        })
      },
    })
  }

  handleGenerateKeys = (e) => {
    e.preventDefault()
    Api.generateApiCredentials({
      success: (data) => {
        this.setState({
          credentials: [data],
        })
      },
    })
  }

  handleRevokeKeys = (e) => {
    e.preventDefault()
    this.props.showModal(
      <SimpleModal
        body={(
          <>
            <p>Are you sure you want to revoke API credentials?</p>
            <p>This will remove the existing Client ID and Client Secret. Your application will no longer be able to make Swoop API calls until new credentials are generated and implemented.</p>
          </>
        )}
        cancel="Cancel"
        className="confirm-revoke-credentials"
        confirm="Confirm"
        onConfirm={() => {
          this.revokeKeys()
          this.props.hideModals()
        }}
        title="Revoke Credentials"
      />
    )
  }

  revokeKeys = () => {
    const { id } = this.state.credentials[0]
    Api.deleteApiCredentials(id, {
      success: () => {
        this.setState({
          credentials: [],
        })
      },
      error: () => {
        Utils.sendNotification('Failed to delete credentials', Consts.ERROR)
      },
    })
  }

  renderButtons = () => {
    if (Array.isArray(this.state.credentials) && this.state.credentials.length > 0) {
      return (
        <>
          <a className="apiDocs" href="https://docs.joinswoop.com/docs" rel="noopener noreferrer" target="_blank">API Documentation</a>
          <Button
            className="revoke"
            color="secondary"
            onClick={this.handleRevokeKeys}
          >
            Revoke Credentials
          </Button>
        </>
      )
    } else {
      return (
        <>
          <Button
            className="generateKeys"
            color="primary"
            onClick={this.handleGenerateKeys}
          >
            Generate Credentials
          </Button>
        </>
      )
    }
  }

  renderCredentials = () => {
    if (this.state.credentials === 'error') {
      return <p className="api-error" style={{ color: 'red' }}>Error getting credentials. Contact Swoop for assistance.</p>
    } else if (!this.state.credentials || (Array.isArray(this.state.credentials) && this.state.credentials.length === 0)) {
      return <p>Generate credentials to begin developing with the Swoop API.</p>
    } else {
      const { client_id, client_secret } = this.state.credentials[0]
      return (
        <ul>
          <li><span>Client ID:</span> {client_id}</li>
          <li><span>Client Secret:</span> {client_secret}</li>
        </ul>
      )
    }
  }

  render() {
    if (this.state.credentials) {
      return (
        <div className="api-integration">
          <div className="api-display">
            <img alt="Swoop logo" src={SwoopIcon} />
            <div className="api-credentials">
              <p className="api-credentials-heading">API Credentials</p>
              {this.renderCredentials()}
            </div>
          </div>
          <div className="api-actions">
            {this.renderButtons()}
          </div>
        </div>
      )
    }
    return null
  }
}

export default withModalContext(APICredentials)
