import React from 'react'
import Consts from 'consts'
import createReactClass from 'create-react-class'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
ICON_COL_WIDTH = 40
SMALL_WIDTH = 30
UserSettingStore = require('stores/user_setting_store')
CompanyStore = require('stores/company_store').default
FTable = React.createFactory((require 'components/tables/SortTable').Table)
{Column} = (require 'components/tables/SortTable')
import SearchBox from 'components/search_box'
import { extend, map, partial } from 'lodash'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

CompanyList = createReactClass(
  displayName: 'CompanyList'

  getInitialState: ->
    that = this

    cols = [
      class Id extends Column
        _header: "Company ID", _key: "id", _originalWidth: 70

      class VendorId extends Column
        _header: "Vendor ID", _key: "agero_vendor_id", _originalWidth: 70

      class Name extends Column
        _header: "Name", _key: "name"

      class Edit extends Column
        _header: "", _key: "edit", _originalWidth: SMALL_WIDTH
        getValue: ->
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-pencil'
            name: 'edit'
            onClick: partial(that.props.editEntry, @record)

      class Status extends Column
        _header: "Status", _key: "status"
        getValue: ->
          if @record.type == "RescueCompany"
            return Consts.SUBSCRIPTION_STATUS[@record.subscription_status_id] || ''
          return ''

      class Type extends Column
        _header: "Type", _key: "type"
        getValue: ->
          if !@record?
            return ''

          if @record.type == "RescueCompany"
            return "Partner"
          else
            if @record.in_house == true
              return "In-House Fleet"
            else
              return "Managed Fleet"

      class Live extends Column
        _header: "Live", _key: "live", _originalWidth: SMALL_WIDTH
        getValue: ->
          if !@record?
            return ''

          if @record.type == "RescueCompany"
            if @record.live then "✓" else "▢"

      class Address extends Column
        _header: "Address", _key: "address"
        getValue: ->
          if !@record?
            return ''

          if @record.location? then @record.location.address

      class Phone extends Column
        _header: "Phone", _key: "phone"

      class DispatchOrSupportEmail extends Column
        _header: "Dispatch/Support Email", _key: "email"
        getValue: ->
          if !@record?
            return ''

          if @record.type == "RescueCompany"
            @record.dispatch_email
          else
            @record.support_email

      class AccountingEmail extends Column
        _header: "Accounting Email", _key: "accounting_email"

      class Delete extends Column
        _header: "", _key: "delete", _originalWidth: ICON_COL_WIDTH
        getValue: ->
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-times'
            name: 'add'
            size: 'medium'
            onClick: partial(that.props.removeEntry, @record, { noun: 'company', title: 'Delete Company' })
    ]

    @cols = map(cols, (col) -> new col())

    lastSortBy = UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.COMPANIES_SORTBY)

    initialSortBy = if lastSortBy then JSON.parse(lastSortBy) else undefined
    initialSortKey = initialSortBy?.key || "id"
    initialSortOrder = if initialSortBy?.order == 1 then "asc" else "desc"

    return {
      searchKey: initialSortKey
      searchOrder: initialSortOrder
      search: ' '
      pageSize: Consts.COMPANY_PAGE_SIZE
    }

  componentDidMount: ->
    CompanyStore.bind(CompanyStore.CHANGE, this.companyChanged)

    # it will trigger 'load the first page'
    @searchChange(@state.search)

  componentWillUnmount: ->
    CompanyStore.unbind(CompanyStore.CHANGE, this.companyChanged)
    CompanyStore.resetSearchResult()

  companyChanged: ->
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously

  sortChange: (key, order) ->
    console.log("companies search order key: #{key}, order: #{order}")
    searchOrder = if order == 1 then "asc" else "desc"

    @setState
      searchKey: key
      searchOrder: searchOrder

    @search(@state.search, CompanyStore.getSearch().searchPage)

  searchChange: (value) ->
    if(value == null)
      value = ' '

    @search(value, 1)

    @setState
      search: value

  search: (value, page) ->
    order = this.state.searchKey + ',' + this.state.searchOrder

    CompanyStore.search(value, null, order, page)

  render: ->
    div
      className: 'companies_container'
      <SearchBox
        onChange={@searchChange}
        store={CompanyStore}
        enablePagination
        showSearch
        show
      />
      div
        style:
          clear: "both"
        FTable extend({}, @props,
          tableKey: "companies"
          rows: if CompanyStore.isLoadingSearch() then [] else CompanyStore.getRecordsOfCurrentSearchPage()
          cols: @cols
          originalSortDir: @state.searchOrder
          originalSortKey: @state.searchKey
          onSortChange: partial(@sortChange)
          rowClick: -> # just to avoid BaseTable throwing error
        )
      if CompanyStore.getSearch().allLoaded and CompanyStore.getSearch().total == 0 and @state?.search?
        span
          className: "empty_message"
          'No search results found'

      if CompanyStore.isLoadingSearch()
        Loading null
)

module.exports = CompanyList
