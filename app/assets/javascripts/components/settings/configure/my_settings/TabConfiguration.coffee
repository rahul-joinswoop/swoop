import React from 'react'
import Dispatcher from 'lib/swoop/dispatcher'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import ReactDOMFactories from 'react-dom-factories'
import Utils from 'utils'
import { bind, filter, indexOf, keys, map, partial, without } from 'lodash'
BaseComponent = require('components/base_component')
DashboardCols = require('dashboard_cols')
FTable = React.createFactory(require('components/resizableTable/FTable'))
ModalStore = require('stores/modal_store')
SettingStore = require('stores/setting_store')
UserSettingStore = require('stores/user_setting_store')

class TabConfiguration extends BaseComponent
  displayName: 'TabConfiguration'

  constructor: (props) ->
    super(props)

    @cols = DashboardCols.getCols(
      isFrontendSort: => @props.active
      isSortable: -> false
    )
    for index, col of @cols
      @cols[index] = new col()

    {}

  getTabName: =>
    if @props.active then "active_tab" else "done_tab"

  getTitle: =>
    if @props.active
      name = ""
      count = 0
      if SettingStore.getSettingByKey("Scheduled Tab")?
        name = "Scheduled, "
        count++
      if SettingStore.getSettingByKey("Pending Tab")?
        name += "Pending, In-Progress"
      else
        name += "Active"
    else
      name = "Done"
    return name + " Tab"

  getHeader: =>
    span null,
      @getTitle()

  getSettings: =>
    if !@state? || !@state.dragging
      if @props.active
        @settings = UserSettingStore.getActiveTabSettings(@getId())
      else
        @settings = UserSettingStore.getDoneTabSettings(@getId())

    @settings

  commitSettings: =>
    UserSettingStore.setUserSettings(@getTabName(), @settings)

  addSelection: (selection) =>
    @settings.unshift(selection)
    Dispatcher.send(ModalStore.CLOSE_MODALS)
    @commitSettings()
    @forceUpdate()

  removeSelection: (selection, e) =>
    @settings = without(@settings, selection)
    @commitSettings()
    @forceUpdate()
    Utils.ignoreEvents(e)

  moveSelectionTo: (selection, index) =>
    settings = @getCurrentCols()
    oldIndex = indexOf(settings, selection)

    @settings = without(settings, selection)
    @settings.splice(index, 0, selection)
    @setState
      dragChanged: true
    @forceUpdate()

  showAdditionalColumns: (selectable, e) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "options",
      props:
        target: e.target
        offsetTop: 25
        offsetLeft: 0
        options: map(selectable, (selection) =>
          header = @cols[selection].getHeader()
          if header? and header.length > 0
            li
              style:
                minWidth: 100
              onClick: partial(@addSelection, selection)
              header
        )
    )

  onMouseUp: () =>
    $(window).off("mouseup", @onMouseUp)
    if @state.dragChanged
      @commitSettings()

    @setState
      dragging: false
      draggingKey: null
      dragChanged: false

  onMouseOver: (key) =>
    if @state.draggingKey? and @state.draggingKey != key
       @moveSelectionTo(@state.draggingKey, indexOf(@settings, key))

  getSelectableCols: =>
    filter(keys(@cols), (key) =>
      if key not in @getSettings()
        if @cols[key]?.isShowing()
          return true
      return false
    )

  getCurrentCols: =>
    filter(@settings, (key) =>
      if @cols[key]?.isShowing()
          return true
    )

  render: ->
    super(arguments...)

    selectable = @getSelectableCols()

    div
      className: "noselect"
      style:
        cursor: if @state.dragging then "ns-resize"
      if selectable and filter(selectable, (selection) -> selection not in ["action", "actions"]).length > 0
        FontAwesomeButton
          color: 'secondary'
          icon: 'fa-plus'
          name: 'add'
          onClick: partial(@showAdditionalColumns, selectable)
          style: {
            margin: 10,
            position: 'absolute',
            right: 0,
            top: 0,
            zIndex: 1,
          }

      FTable
        rows: @getCurrentCols()
        cols: [
          {header: @getHeader(), className: "dashboard_tab_option", sortable: false, func: (key) =>
              if @cols[key]?
                div
                  className: "row-text"
                  onMouseOver: bind(@onMouseOver, @, key)
                  ReactDOMFactories.i
                    className: "fa fa-bars"
                    onMouseDown: =>
                      @setState
                        dragging: true
                        draggingKey: key
                      $(window).on("mouseup", @onMouseUp)
                  span
                    style:
                      fontWeight: if key == @state.draggingKey then "bold"
                    @cols[key].getHeader()
                  span
                    className: "remove"
                    onClick: partial(@removeSelection, key)
                    "×"
          }
        ]

module.exports = TabConfiguration
