import React from 'react'
import Consts from 'consts'
BaseComponent = require('components/base_component')
Renderer = require 'form_renderer'
UserStore = require('stores/user_store')
UserFields = require('user_fields')
UsersStore = require('stores/users_store').default
import { keys, pick } from 'lodash'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class EmailSettings extends BaseComponent
  constructor: (props) ->
    super(props)
    user = UserStore.getUser()
    if user?
      @form = new Renderer(UserStore.getUser(), @recordChanged, {
        componentId: @getId()
      })
      @form.registerFields(UserFields, true)

    @register(UserStore, UserStore.USERME_CHANGED)

  componentDidMount: ->
    super(arguments...)
    @props.setRef('email_settings', this.refs.email_settings)

  showSpinner: (showSpinner) =>
    @setState
      showSpinner: showSpinner

  rerender: =>
    if !@form
      @form = new Renderer(UserStore.getUser(), @recordChanged, {
        componentId: @getId()
      })
      @form.registerFields(UserFields, true)
    else
      @form.replaceRecord(UserStore.getUser())

    super(arguments...)

  recordChanged: =>
    emailSettingsChanges = pick(@form.getRecordChanges(),
      ['id', 'job_status_email_frequency', 'job_status_email_preferences'])

    #TODO: figure out why this is being called on replaceRecord
    if keys(emailSettingsChanges).length > 1
      UsersStore.updateItem(emailSettingsChanges)

  render: ->
    super(arguments...)

    div
      className: 'configure-component'
      id: 'email_settings'
      ref: 'email_settings'
      h4 null,
        "Email Settings"
      if @state?.showSpinner
        Loading null
      else
        div null,
          @form?.renderInputs([
            ["job_status_email_frequency", context: "user_form"],
            Consts.CREATED,
            Consts.TOWING,
            Consts.ACCEPTED,
            Consts.TOWDESTINATION,
            Consts.ETAEXTENDED,
            Consts.ENROUTE,
            Consts.COMPLETED,
            Consts.ONSITE,
            Consts.GOA
          ])

module.exports = EmailSettings
