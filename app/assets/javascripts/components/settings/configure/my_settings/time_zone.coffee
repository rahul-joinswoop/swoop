import React from 'react'
BaseComponent = require('components/base_component')
import BaseConsts from 'base_consts'
UserStore = require('stores/user_store')
UsersStore = require('stores/users_store').default
import moment from 'moment-timezone'

class TimeZone extends BaseComponent
  constructor: ->
    super(arguments...)
    @state =
      timezone: UserStore.getTimezone()

  setTimezone: (e) =>
    moment.tz.setDefault(e.target.value)
    UsersStore.updateItem({
      id: UserStore.getUser().id,
      timezone: e.target.value
    })
    @setState
      timezone: e.target.value

  componentDidMount: ->
    super(arguments...)
    #TODO: bind to global interval
    @props.setRef('timezone_settings', this.refs.timezone_settings)
    @interval = setInterval(=>
      @rerender()
    , 1000)

  componentWillUnmount: ->
    super(arguments...)
    clearInterval(@interval)

  render: ->
    super(arguments...)
    guessTimeZone = moment.tz.guess()
    currentTimeZone = @state.timezone

    div
      className: 'configure-component'
      id: 'timezone_settings'
      ref: 'timezone_settings'
      h3
        className: 'configure-section-heading'
        "Time Zone"
      h4 null,
        'Time Zone'
      span null,
        "Current Time: "+moment().format("HH:mm A")
      select
        onChange: @setTimezone
        value: if currentTimeZone? then currentTimeZone else guessTimeZone
        if currentTimeZone?
          option
            key: currentTimeZone
            value: currentTimeZone
            currentTimeZone
        if currentTimeZone != guessTimeZone
          option
            key: guessTimeZone
            value: guessTimeZone
            guessTimeZone
        for timeZone in BaseConsts.STANDARD_TIMEZONES
          option
            key: timeZone
            value: timeZone
            timeZone

module.exports = TimeZone
