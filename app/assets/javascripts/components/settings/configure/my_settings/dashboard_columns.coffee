import React from 'react'
BaseComponent = require('components/base_component')
TabConfiguration = React.createFactory require('components/settings/configure/my_settings/TabConfiguration')

class SettingDashboardColumns extends BaseComponent
  componentDidMount: ->
    super(arguments...)
    @props.setRef('dashboardcolumns_settings', this.refs.dashboardcolumns_settings)

  render: ->
    div
      className: 'configure-component'
      id: 'dashboardcolumns_settings'
      ref: 'dashboardcolumns_settings'
      h3
        className: 'configure-section-heading'
        "Dashboard Columns"
      h4 null,
        "Dashboard Columns"
      div
        className: "description"
        "Drag to change the order of columns, click plus button to add more columns."
      TabConfiguration
        active: true
      TabConfiguration
        active: false

module.exports = SettingDashboardColumns
