import React from 'react'
import BaseComponent from 'components/base_component'
import Renderer from 'form_renderer'
import UserStore from 'stores/user_store'
import UserFields from 'user_fields'
import UsersStore from 'stores/users_store'
import { keys, pick, each } from 'lodash'
import 'stylesheets/components/settings/configure/my_settings/Notifications.scss'

class NotificationSettings extends BaseComponent {
  constructor(props) {
    super(props)

    this.form = new Renderer(UserStore.getUser(), this.patchRecordAndRerender, {
      componentId: this.getId(),
    })
  }

  notificationSettingsRows() {
    if (UserStore.isPartner()) {
      const availableNotificationSettings = UserStore.getCompany().available_user_notification_settings
      const availableNotificationSettingsRows = []

      each(availableNotificationSettings, (setting) => {
        const fieldClass = UserFields.NotificationSettingFactory(setting)
        const field = this.form.register(fieldClass, fieldClass._name)

        availableNotificationSettingsRows.push(this.form.renderInput(field._name))
      })

      return availableNotificationSettingsRows
    } else {
      return null
    }
  }

  componentDidMount() {
    // we need to call super here, otherwise the selectbox does not get updated while opened
    BaseComponent.prototype.componentDidMount.call(this)
    this.props.setRef('notification_settings', this.refs.notification_settings)
  }

  patchRecordAndRerender() {
    const notificationSettingsChanges = pick(this.form.getRecordChanges(), ['id', 'notification_settings'])

    if (keys(notificationSettingsChanges).length > 1) {
      UsersStore.updateItem(notificationSettingsChanges)
    }

    this.rerender()
  }

  render() {
    return (
      <div className="configure-component NotificationSettings-container">
        <h4>Notification Settings</h4>
        <div>
          {this.notificationSettingsRows()}
        </div>
      </div>
    )
  }
}

module.exports = NotificationSettings
