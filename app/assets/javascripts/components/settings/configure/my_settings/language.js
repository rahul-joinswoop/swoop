import React, { useEffect, useRef, useState } from 'react'
import { map } from 'lodash'
import StoreWrapper from 'StoreWrapper'
import UserStore from 'stores/user_store'
import UsersStore from 'stores/users_store'
import { DEFAULT_LANGUAGE, LANGUAGES } from 'lib/localize'

const LanguageSetting = ({ language, setRef }) => {
  const languageSetting = useRef()
  const [selectedLanguage, setSelectedLanguage] = useState(language)

  useEffect(() => {
    // jives with Configuration page side navigation
    setRef('languageSetting', languageSetting.current)
  }, [setRef])

  const handleChange = e => {
    UsersStore.updateItem({
      id: UserStore.getUser().id,
      language: e.target.value,
    })
    setSelectedLanguage(e.target.value)
    window.Localize.setLanguage(e.target.value)
  }

  return (
    <div
      className="configure-component"
      id="language_setting"
      ref={languageSetting}
    >
      <h3 className="configure-section-heading">
        Language
      </h3>
      <h4>Language</h4>
      <select
        onChange={handleChange}
        value={selectedLanguage}
      >
        {map(LANGUAGES, lang => (
          <option key={lang.id} value={lang.id}>{lang.name}</option>
        ))}
      </select>

    </div>
  )
}

/* eslint-disable-next-line */
const Language = StoreWrapper(LanguageSetting, function getProps(props) {
  return {
    language: UserStore.getUser().language || UserStore.getCompany().language || DEFAULT_LANGUAGE,
  }
})

export default Language
