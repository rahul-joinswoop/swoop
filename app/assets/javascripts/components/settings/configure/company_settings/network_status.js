import React from 'react'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'

import ColorSelector from 'componentLibrary/ColorSelector'
import { partial, values } from 'lodash'

import 'stylesheets/components/settings/company_settings/network_status.scss'
import BaseComponent from 'components/base_component'

import FTable from 'components/resizableTable/FTable'

import { withModalContext } from 'componentLibrary/Modal'
import TagStore from 'stores/tag_store'

import SimpleModal from 'components/modals/simple_modal'
import SimpleFieldModal from 'components/modals/simple_field_modal'

class SettingNetworkStatus extends BaseComponent {
  deleteCompanyTag = (tag) => {
    this.props.showModal(
      <SimpleModal
        body={`Are you sure you want to delete the Network Status: ${tag.name}?`}
        confirm="Delete Network Status"
        onCancel={this.props.hideModals}
        onConfirm={partial(this.onConfirmDeleteCompanyTag, tag)}
        title="Warning"
      />,
    )
  }

  onConfirmDeleteCompanyTag = (tag) => {
    TagStore.deleteItem(tag)
    this.props.hideModals()
  }

  addNetworkStatus = () => {
    this.props.showModal(
      <SimpleFieldModal
        confirm="Create"
        label="Network Status Name"
        onCancel={this.props.hideModals}
        onConfirm={this.onConfirmAddNetworkStatus}
        title="Create Network Status"
      />,
    )
  }

  onConfirmAddNetworkStatus = (name) => {
    TagStore.addItem({
      name,
    })
    this.props.hideModals()
  }

  editCompanyTag = (tag) => {
    this.props.showModal(
      <SimpleFieldModal
        confirm="Update"
        initialValue={tag.name}
        label="Network Status Name"
        onCancel={this.props.hideModals}
        onConfirm={partial(this.onConfirmEditCompanyTag, tag.id)}
        title="Edit Network Status"
      />,
    )
  }

  onConfirmEditCompanyTag = (tagId, name) => {
    TagStore.updateItem({
      id: tagId,
      name,
    })
    this.props.hideModals()
  }

  setColor = (network_status_id, color) => {
    TagStore.updateItem({
      color,
      id: network_status_id,
    })
  }

  render() {
    const tags = values(TagStore.loadAndGetAll(this.getId(), ['name', 'color']))
    return (
      <div className="configure-component" id="networkStatuses_in_companySettings">
        <h4>
          Network Statuses
          {FontAwesomeButton({
            color: 'secondary-o',
            icon: 'fa-plus',
            name: 'add',
            onClick: this.addNetworkStatus,
            size: 'medium',
          })}
        </h4>
        <div className="description">
          Network Statuses can be assigned to partners and appear in the Choose Partner Map
        </div>
        <FTable
          cols={[
            {
              header: 'Network Statuses',
              key: 'name',
            },
            {
              func: network_status => (
                <ColorSelector
                  additionalColors={['#6C6C6C']}
                  color={network_status.color}
                  maxHeight="initial"
                  setColor={partial(this.setColor, network_status.id)}
                />
              ),
              header: '',
              key: 'Color',
              sortable: false,
            },
            {
              header: '',
              key: 'edit',
              width: 230,
              sortable: false,
              func: (tag, props, three) => <div>
                {FontAwesomeButton({
                  color: 'secondary-o',
                  icon: 'fa-pencil',
                  name: 'edit',
                  onClick: partial(this.editCompanyTag, tag),
                  size: 'medium',
                })}
                {FontAwesomeButton({
                  color: 'secondary-o',
                  icon: 'fa-times',
                  name: 'delete',
                  onClick: partial(this.deleteCompanyTag, tag),
                  size: 'medium',
                })}
              </div>,
            },
          ]}
          rows={tags}
          sortDir={1}
          sortKey="name"
        />
      </div>
    )
  }
}

export default withModalContext(SettingNetworkStatus)
