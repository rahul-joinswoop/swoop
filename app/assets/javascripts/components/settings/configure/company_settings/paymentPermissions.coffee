import React from 'react'
import 'stylesheets/components/settings/company_settings/paymentPermissions.scss'
import { forEach } from 'lodash'

BaseComponent = require('components/base_component')
import RolesStore from 'stores/roles_store'
UserStore = require('stores/user_store')
RolesFilter = React.createFactory(require 'components/filters/roles_filter')

class PaymentPermissions extends BaseComponent

  componentDidMount: ->
    super(arguments...)

  getFilterRoleIds: ->
    roleIds = {}
    forEach(RolesStore.loadAndGetAll(@getId()), (value, key) ->
      roleIds[value?.name] = parseInt(key) if ['admin', 'dispatcher', 'driver'].includes(value?.name))
    return roleIds

  render: ->
    roleIds = @getFilterRoleIds()
    div
      className: 'configure-component'
      id: 'paymentPermissions_in_companySettings'
      h4 null,
        'Payment Permissions'
      div
        className: 'description'
        'Specify which types of users can process payments'
      div
        className: 'multiselect-box'
        span
          className: 'multiselect-label'
          'Charge cards'
        RolesFilter
          roleIds: roleIds,
          permissionType: 'charge'
      div
        className: 'multiselect-box'
        span
          className: 'multiselect-label'
          'Refund cards'
        RolesFilter
          roleIds: roleIds,
          permissionType: 'refund'

module.exports = PaymentPermissions
