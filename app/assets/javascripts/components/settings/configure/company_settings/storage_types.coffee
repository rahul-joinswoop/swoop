import React from 'react'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
BaseComponent = require('components/base_component')
import Utils from 'utils'
FTable = React.createFactory(require 'components/resizableTable/FTable')
StorageTypeStore = require('stores/storage_type_store')
import { extend, partial, values } from 'lodash'
TransitionModal = React.createFactory(require 'components/modals')
import { withModalContext } from 'componentLibrary/Modal'

class StorageTypes extends BaseComponent
  deleteStorageType: (storageType) =>
    if storageType?
      @props.showModal(
        TransitionModal
          show: true
          extraClasses: "simple_modal"
          onCancel: @props.hideModals
          disabled: false
          title: "Warning"
          confirm: 'OK'
          onConfirm: =>
            StorageTypeStore.deleteItem(storageType)
            @props.hideModals()
          "Are you sure you want to delete "+storageType.name+"?"
      )

  render: ->
    div
      className: 'configure-component'
      id: "storageTypes_in_companySettings"
      h4 null,
        "Storage Types"

        FontAwesomeButton
          color: 'secondary-o'
          icon: 'fa-plus'
          name: 'add'
          onClick: partial(Utils.showModal, 'storage_type_modal')
          size: 'medium'

      FTable extend({}, @props,
        rows: values(StorageTypeStore.loadAndGetAll(@getId(), 'name'))
        cols: [
          {header: "Storage Type", key: "name"}
          {header: "", key: "edit", width: 230, sortable: false, func: (storageType, props, three) =>
            div null,
              FontAwesomeButton
                color: 'secondary-o'
                icon: 'fa-pencil'
                name: 'edit'
                onClick: partial(Utils.showModal, "storage_type_modal", partial.placeholder, record:storageType)
                size: 'medium'
              FontAwesomeButton
                color: 'secondary-o'
                icon: 'fa-times'
                name: 'delete'
                onClick: partial(@deleteStorageType, storageType)
                size: 'medium'
          }
        ]
        sortKey: "name"
        sortDir: 1
      )

module.exports = withModalContext(StorageTypes)
