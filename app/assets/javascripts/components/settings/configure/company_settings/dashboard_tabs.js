import React, { useEffect } from 'react'
import CheckboxSetting from 'components/settings/configure/CheckboxSetting'
import { useRerender } from 'componentLibrary/Hooks'
import SettingStore from 'stores/setting_store'

const SettingCompanies = () => {
  const rerender = useRerender()

  useEffect(() => {
    SettingStore.bind(SettingStore.CHANGE, rerender)
    return () => SettingStore.unbind(SettingStore.CHANGE, rerender)
  }, [])

  return (
    <div
      className="configure-component"
      id="dashboardTabs_in_companySettings"
    >
      <h4>Dashboard Tabs</h4>
      <div className="description">
        Add tabs to appear on the Dashboard.

        <CheckboxSetting
          id="scheduled_tab"
          name="Scheduled Tab"
          subtext="(jobs scheduled for the future)"
          text="Scheduled"
        />
        <CheckboxSetting
          id="pending_tab"
          name="Pending Tab"
          subtext="(jobs you've received or created but haven't dispatched)"
          text="Pending"
        />
        <CheckboxSetting
          id="drafts_tab"
          name="Drafts Tab"
          subtext="(jobs that are not yet ready to be dispatched)"
          text="Drafts"
        />
        {SettingStore.getSettingByKey('Pending Tab') && <CheckboxSetting
          id="monitoring_tab"
          name="Monitoring Tab"
          subtext="(jobs in the On Site, Towing, and Tow Destination statuses)"
          text="Monitoring"
        />}
      </div>
    </div>
  )
}

export default SettingCompanies
