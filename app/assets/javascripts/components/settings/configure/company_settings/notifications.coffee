import React from 'react'
import Consts from 'consts'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
BaseComponent = require('components/base_component')
import CheckboxSetting from 'components/settings/configure/CheckboxSetting'
UserStore = require('stores/user_store')
CompanyStore = require('stores/company_store').default
import { partial } from 'lodash'
EditEmailReviewNotifications = React.createFactory(require('components/modals/edit_email_review_notifications'))
import { withModalContext } from 'componentLibrary/Modal'

class Notifications extends BaseComponent
  editEmailReviewNotifications: =>
    @props.showModal(
      EditEmailReviewNotifications
        show: true
        onCancel: @props.hideModals
        onConfirm: @handleReviewEmailChange
    )

  handleReviewEmailChange: (record) =>
    CompanyStore.updateEmailReview(
      id: record.id,
      review_email_recipient_key: record.review_email_recipient_key,
      review_email: record.review_email
    )
    @props.hideModals()

  getReviewEmailRecipientKey: =>
    if UserStore.getUser()?.company?.id
      CompanyStore.get(UserStore.getUser().company.id, 'review_email_recipient_key', @getId())
    else
      'all_admins'

  getEmailReviewContent: =>
    if @getReviewEmailRecipientKey() == 'all_admins'
      div
        style:
          display: 'inline-block'
        "Email review notifications to all admins"
    else
      review_email = ''

      if UserStore.getUser()?.company?.id
        review_email = CompanyStore.get(UserStore.getUser().company.id, 'review_email', @getId())

      div
        style:
          display: 'inline-block'
        div
          style:
            display: 'inline-block'
          "Email review notifications to specific email address:"
        span
          style:
            fontWeight: 'bold'
            paddingLeft: 5
          review_email
  render: ->
    div
      className: 'configure-component'
      id: 'notifications_in_companySettings'
      h3
        className: 'configure-section-heading'
        "Company Notifications"
      h4 null,
        "Notifications"

      if UserStore.isPartner()
        [
          <CheckboxSetting
            text={@getEmailReviewContent()}
            onChange={(e) ->
              enabled = e.target.checked

              company = UserStore.getUser().company

              CompanyStore.updateEmailReview(id: company.id, disable_review_emails: !enabled)
            }
            getChecked={=>
              if UserStore.getUser()?.company?.id
                !CompanyStore.get(UserStore.getUser().company.id, 'disable_review_email', @getId())
              else
                false
            }
            name="Email Review Notifications Enabled"
            id="company_notifications_email_checkbox"
            key="company_notifications_email_checkbox"
            editIcon={FontAwesomeButton
              color: 'secondary-o'
              icon: 'fa-pencil'
              name: 'edit'
              onClick: partial(@editEmailReviewNotifications)
              size: 'medium'
            }
          />

          <CheckboxSetting
            text="Phone calls for new job alerts"
            name={Consts.SETTTING_DISABLE_JOB_ALERTS}
            id="disable_job_alerts"
            key="disable_job_alerts"
            inverted />
        ]

      <CheckboxSetting
        text='Dashboard notification bars'
        name={Consts.SETTING_DISABLE_NOTIFICATIONS}
        id='disable_notifications'
        key="disable_notifications"
        inverted />

module.exports = withModalContext(Notifications)
