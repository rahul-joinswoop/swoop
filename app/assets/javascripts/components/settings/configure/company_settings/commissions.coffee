import React from 'react'
require('stylesheets/components/settings/company_settings/commissions.scss')
import Dispatcher from 'lib/swoop/dispatcher'
import BaseConsts from 'base_consts'
import NonCommissionableServicesForm from 'components/modals/NonCommissionableServicesForm'
import NonCommissionableAddonsForm from 'components/modals/NonCommissionableAddonsForm'

BaseComponent = require('components/base_component')
ObjectTable = React.createFactory(require('components/settings/configure/object_table'))
ObjectSetting = React.createFactory(require('components/settings/configure/object_setting'))
CompanyStore = require('stores/company_store').default
ServiceStore = require('stores/service_store')
ModalStore = require('stores/modal_store')
import { map, sortBy } from 'lodash'
import { withModalContext } from 'componentLibrary/Modal'

class Commissions extends BaseComponent
  getCommissionPercentage: =>
    commission = CompanyStore.get(UserStore.getCompany().id, 'commission', @getId())

    if commission?.default_pct
      commission.default_pct + '%'
    else
      ''

  getNonCommissionableCollection: (serviceOrAddonType) =>
    exclusionIds = CompanyStore.get(UserStore.getCompany().id, serviceOrAddonType, @getId())

    if exclusionIds
      return sortBy(map(exclusionIds, (serviceOrAddonId) => ServiceStore.get(serviceOrAddonId, 'name', @getId())))
    else
      return []

  getNonCommissionableServices: =>
    @getNonCommissionableCollection('commission_exclusions_services_ids')

  getNonCommissionableItems: =>
    @getNonCommissionableCollection('commission_exclusions_addons_ids')

  showEditModal: (modalKey) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: modalKey,
      props: {}
    )

  editNonCommissionableAddons: =>

  render: ->
    super(arguments...)

    if @props.showing == false
      return null

    div
      className: "object_section configure-component"
      id: "commissions_in_companySettings"
      h4 null,
        "Commissions"
      div
        className: "description"
        BaseConsts.COMMISSION_PERCENTAGE.SETTING_TEXT
      ObjectSetting
        label: 'Commission Percentage'
        value: @getCommissionPercentage()
        modalKey: 'commission_percentage_modal'
      ObjectTable
        label: 'Non Commissionable Services'
        isCommissions: true
        isLoading: -> false
        getItems: @getNonCommissionableServices
        emptyCollecionMessage: 'All Services set as commissionable'
        onEditCollection: () => this.props.showModal(<NonCommissionableServicesForm />)
      ObjectTable
        label: 'Non Commissionable Items'
        isCommissions: true
        isLoading: -> false
        getItems: @getNonCommissionableItems
        emptyCollecionMessage: 'All Items set as commissionable'
        onEditCollection: () => this.props.showModal(<NonCommissionableAddonsForm />)

export Component = Commissions
export default withModalContext(Commissions)