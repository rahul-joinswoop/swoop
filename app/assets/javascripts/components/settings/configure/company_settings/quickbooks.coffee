import Consts from 'consts'
import React from 'react'
require('stylesheets/components/settings/company_settings/quickbooks.scss')

BaseComponent = require('components/base_component')
import CheckboxSetting from 'components/settings/configure/CheckboxSetting'
UserStore = require('stores/user_store')
FeatureStore = require('stores/feature_store')
import Api from 'api'

#TODO: HACK: this won't clear after the user logs out / logs in
token = null
class SettingQuickbooks extends BaseComponent

  componentDidMount: ->
    super()
    if @isShowing() && !token?
      Api.getQBToken({
        success: (data) =>
          token = data.token
          @rerender()
      })

  isShowing: ->
    UserStore.isPartner() && FeatureStore.isFeatureEnabled(Consts.FEATURE_QUICKBOOK)
  render: ->
    super()
    if @isShowing()
      div
        className: 'configure-component'
        id: "quickbooks_in_companySettings"
        h4 null,
          "QuickBooks"
        if token?
          div
            className: 'token'
            span
              style:
                fontWeight: "bold"
              "Secret Key: "
            span null,
              token
        <CheckboxSetting
          text="Include invoices for canceled jobs in QuickBooks import"
          name={Consts.SETTING_QB_IGNORE_CANCELED}
          id="include_canceled_in_quickbooks"
          inverted />
        <CheckboxSetting
          text="Include payments in QuickBooks import"
          name={Consts.SETTING_QB_SYNC_PAYMENTS}
          id="include_payments_in_quickbooks" />
    else
      return null
module.exports = SettingQuickbooks
