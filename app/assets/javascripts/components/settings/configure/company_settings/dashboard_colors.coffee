import React from 'react'
import ReactDOMFactories from 'react-dom-factories'
import Consts from 'consts'
BaseComponent = require('components/base_component')
import SettingsColor from 'components/SettingsColor'

class DashboardColors extends BaseComponent
  render: ->
    div
      className: 'configure-component'
      id: 'dashboardColors_in_companySettings'
      h4 null,
        "Dashboard Colors"
      div
        className: "description"
        "Adjust color outline on job statuses on the Dashboard. "
        ReactDOMFactories.a
          href: "https://intercom.help/swoopme"
          target: "_blank"
          "Learn more about statuses."
      div null,
        for color in [
          {
            key: "Received"
            text: "Received"
            subtext: "(will flash)"
            default: "#FCC37F"
          },
          Consts.DRAFT,
          {
            key: "Pending"
            text: "Pending"
          },
          ["Submitted", "Scheduled", "Accepted", "Assigned", "Dispatched"]...,
          {
            key: "Long Dispatch"
            text: "10 min on Dispatched"
            subtext: "(will flash)"
            default: "#FF7171"
          }
          "En Route",
          {
            key: "Eta Approaching"
            text: "Within 10 min of ETA"
            default: "#FDEB8F"
          },
          {
            key: "Past Eta"
            text: "Past ETA"
            default: "#FF7171"
          },
          ["On Site", "Towing", "Tow Destination"]...
        ]
          <SettingsColor
            key={color.key || color}
            setting={color}/>

module.exports = DashboardColors
