import React from 'react'
import ReactDOMFactories from 'react-dom-factories'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Utils from 'utils'
import { extend, partial } from 'lodash'
BaseComponent = require('components/base_component')
CompanyStore = require('stores/company_store').default
FTable = React.createFactory(require 'components/resizableTable/FTable')

class SettingCompanies extends BaseComponent
  render: ->
    div
      className: 'configure-component'
      id: "companies_in_companySettings"
      h4 null,
        "Sub-Companies"

        FontAwesomeButton
          color: 'secondary-o'
          icon: 'fa-plus'
          name: 'add'
          onClick: partial(Utils.showModal, 'tax_company_modal')
          size: 'medium'

      FTable extend({}, @props,
        rows: CompanyStore.getSubCompaniesIds(@getId())
        cols: [
          {header: "Company Name", key: "name", func: (companyId) =>
            CompanyStore.get(companyId, 'name', @getId())
          }
          {header: "Tax ID", key: "tax_id", func: (companyId) =>
            CompanyStore.get(companyId, 'tax_id', @getId())
          }
          {header: "", key: "edit", width: 230, sortable: false, func: (companyId, props, three) =>
            div null,
              FontAwesomeButton
                color: 'secondary-o'
                icon: 'fa-pencil'
                name: 'edit'
                size: 'medium'
                onClick: partial(Utils.showModal, "tax_company_modal", partial.placeholder, record: CompanyStore.getById(companyId)) # TaxCompany modal uses EditForm, se we still pass the whole object to it
          }
        ]
        sortKey: "created_at"
        sortDir: -1
      )

module.exports = SettingCompanies
