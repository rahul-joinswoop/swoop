import React from 'react'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import CheckboxSetting from 'components/settings/configure/CheckboxSetting'
import FeatureStore from 'stores/feature_store'
import SiteStore from 'stores/site_store'
import UserStore from 'stores/user_store'

class DashboardFilters extends BaseComponent {
  render() {
    return (
      <div
        className="configure-component"
        id="dashboardFilters_in_companySettings"
      >
        <h4>Dashboard Filters</h4>
        <div className="description">
          Add filters to appear on the Dashboard.

          {(SiteStore.getDispatchSites(this.getId()).length >= 2 && UserStore.isPartner()) && (
            <CheckboxSetting
              id="dispatch_filter"
              name="Dispatch Filter"
              text="Filter by Dispatch Sites"
            />
          )}
          <CheckboxSetting
            id="dispatcher_filter"
            name="Dispatcher Filter"
            text="Filter by Dispatcher"
          />
          {(UserStore.isSwoop()) && (
            <CheckboxSetting
              id="fleet_company_filter"
              name="Filter by Fleet Company"
              text="Filter by Fleet Company"
            />
          )}
          <CheckboxSetting
            id="service_filter"
            name="Filter by Service"
            text="Filter by Service"
          />
          <CheckboxSetting
            id="state_filter"
            name="Filter by State"
            text="Filter by State"
          />
          {(FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS)) && (
            <CheckboxSetting
              id="department_filter"
              name="Filter by Department"
              text="Filter by Department"
            />
          )}
          <CheckboxSetting
            id="active_job_search"
            name="Active Job Search"
            text="Active Job Search"
          />
        </div>
      </div>
    )
  }
}

export default DashboardFilters
