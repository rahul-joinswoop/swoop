import React from 'react'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Utils from 'utils'
import { extend, partial } from 'lodash'
import { withModalContext } from 'componentLibrary/Modal'
BaseComponent = require('components/base_component')
DepartmentStore = require('stores/department_store')
FTable = React.createFactory(require 'components/resizableTable/FTable')
TransitionModal = React.createFactory(require 'components/modals')

class SettingDepartments extends BaseComponent

  deleteDepartment: (department) =>
    if department?
      @props.showModal(
        TransitionModal
          show: true
          extraClasses: "simple_modal"
          onCancel: @props.hideModals
          disabled: false
          title: "Delete Department"
          confirm: 'Delete'
          onConfirm: =>
            DepartmentStore.deleteItem(department)
            @props.hideModals()
          "Are you sure you want to delete this department: "+department.name+"?"
      )

  render: ->
    rows = DepartmentStore.getDepartments(@getId())

    div
      className: 'configure-component'
      id: "departments_in_companySettings"
      h4 null,
        "Departments"

        FontAwesomeButton
          color: 'secondary-o'
          icon: 'fa-plus'
          name: 'add'
          onClick: partial(Utils.showModal, 'department_modal')
          size: 'medium'

      FTable extend({}, @props,
        rows: rows
        cols: [
          {header: "Departments", key: "name"}
          {header: "", key: "edit", width: 230, sortable: false, func: (storageType, props, three) =>
            div null,
              FontAwesomeButton
                color: 'secondary-o'
                icon: 'fa-pencil'
                name: 'edit'
                onClick: partial(Utils.showModal, "department_modal", partial.placeholder, record: storageType)
                size: 'medium'
              FontAwesomeButton
                color: 'secondary-o'
                icon: 'fa-times'
                name: 'delete'
                onClick: partial(@deleteDepartment, storageType)
                size: 'medium'
          }
        ]
        sortKey: "name"
        sortDir: 1
      )

module.exports = withModalContext(SettingDepartments)
