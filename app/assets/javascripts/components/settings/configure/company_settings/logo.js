import React from 'react'
import BaseComponent from 'components/base_component'
import LogoUploader from 'components/settings/configure/company_settings/logo_uploader'
import UserStore from 'stores/user_store'

class SettingLogo extends BaseComponent {
  render() {
    if (UserStore.isSwoop()) {
      return null
    }

    return (
      <div
        className="configure-component"
        id="logo_in_companySettings"
      >
        <h4>Logo</h4>
        <LogoUploader />
      </div>
    )
  }
}

export default SettingLogo
