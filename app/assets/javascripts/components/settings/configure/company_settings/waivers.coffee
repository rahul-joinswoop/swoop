import React from 'react'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
BaseComponent = require('components/base_component')
UserStore = require('stores/user_store')
ModalStore = require('stores/modal_store')
CompanyStore = require('stores/company_store').default
import { partial } from 'lodash'
import Dispatcher from 'lib/swoop/dispatcher'

class StorageTypes extends BaseComponent
  editWaiver: (type) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "edit_waiver",
      props: {
        type: type
      }
    )

  render: ->
    super(arguments...)
    div
      className: 'configure-component'
      id: "waivers_in_companySettings"
      for type in [{name: "pickup", text: "Pickup"}, {name: "dropoff", text: "Drop Off"}, {name: "invoice", text: "Invoice"}]
        div
          id: type.name + 'Waiver_in_companySettings'
          key: type.name
          h4 null,
            type.text+" Waiver"

            FontAwesomeButton
              color: 'secondary-o'
              icon: 'fa-pencil'
              name: 'edit'
              onClick: partial(@editWaiver, type.name)
              size: 'medium'

          div null,
            CompanyStore.get(UserStore.getUser()?.company_id, type.name+"_waiver", @getId())

module.exports = StorageTypes
