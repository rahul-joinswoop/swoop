import React, { useCallback, useState } from 'react'
import Api from 'api'
import Button from 'componentLibrary/Button'
import CompanyStore from 'stores/company_store'
import Consts from 'consts'
import Dispatcher from 'lib/swoop/dispatcher'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Loading from 'componentLibrary/Loading'
import ModalStore from 'stores/modal_store'
import S3Upload from 'S3Upload'
import StoreWrapper from 'StoreWrapper'
import { useDropzone } from 'react-dropzone'
import UserStore from 'stores/user_store'
import Utils from 'utils'

const LogoIsTrue = ({ src }) => {
  const removeLogo = () => Dispatcher.send(ModalStore.SHOW_MODAL, {
    key: 'simple',
    props: {
      title: 'Remove Logo',
      confirm: 'Remove',
      body: 'Are you sure you want to remove this logo?',
      onConfirm: () => {
        Api.logoRemove(UserStore.getEndpoint(), {
          success: (data) => {
            Utils.sendNotification('Logo has been removed.', Consts.SUCCESS)
          },
          error: (data) => {
            Utils.sendNotification('Logo failed to remove.', Consts.ERROR)
          },
        })
        Dispatcher.send(ModalStore.CLOSE_MODALS)
      },
    },
  })

  return (
    <>
      <img alt="Logo" src={src} />
      <FontAwesomeButton
        color="secondary"
        icon="fa-times"
        onClick={removeLogo}
        style={{
          fontSize: 14,
          marginLeft: 5,
        }}
      />
    </>
  )
}

const LogoIsFalse = () => {
  const [error, setError] = useState(null)
  const [uploading, setUploading] = useState(false)

  const getSignedUrl = (file, callback) => {
    setUploading(true)
    Api.logoUploadRequest(UserStore.getEndpoint(), null, null, {
      success: (data) => {
        callback({
          signedUrl: data.url,
          document: {
            id: data.id,
            storage: data.storage,
            metadata: {
              size: file.size,
              filename: file.name,
              mime_type: file.type,
            },
          },
        })
      },
      error: (data) => {
        setUploading(false)
        Utils.sendNotification('Logo failed to add.', Consts.ERROR)
      },
    })
  }

  const onProgress = () => {}

  const onError = (data) => {
    setUploading(false)
    Utils.sendNotification('Logo failed to add.', Consts.ERROR)
  }

  const onFinish = (data, datatwo) => {
    const attached_document = { document: JSON.stringify(data.document) }
    Api.logoCreate(UserStore.getEndpoint(), null, { attached_document }, {
      success: (data) => {
        setUploading(false)
        Utils.sendNotification('Logo has been added.', Consts.SUCCESS)
      },
      error: (data) => {
        setUploading(false)
        Utils.sendNotification('Logo failed to add.', Consts.ERROR)
      },
    })
  }

  const onDrop = useCallback(acceptedFiles => {
    const [file] = acceptedFiles

    if (file && file.type !== 'image/jpeg' && file.type !== 'image/png' && file.type !== 'image/pjpeg') {
      setError('Invalid Format. File must be png or jpeg.')
      return
    }

    setError(null)
    const s3upload = new S3Upload({
      contentDisposition: 'auto',
      files: acceptedFiles,
      getSignedUrl,
      onProgress,
      onError,
      onFinishS3Put: onFinish,
      uploadRequestHeaders: {},
    })
  }, [])
  const { getRootProps, getInputProps } = useDropzone({ onDrop })

  return (
    <>
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        <Button
          color="primary"
          style={{
            marginRight: 10,
          }}
        >
          { uploading ? <Loading color="#fff" padding={0} size={24} /> : 'Upload'}
        </Button>
      </div>
      { error && (
        <span className="error">{error}</span>
      )}
    </>
  )
}

const LogoUploader = ({ logoURL }) => (
  <div className="logo-uploader">
    { logoURL ? <LogoIsTrue src={logoURL} /> : <LogoIsFalse /> }
  </div>
)

const WrappedLogoUploader = StoreWrapper(LogoUploader, function getProps() {
  return {
    logoURL: CompanyStore.get(UserStore.getCompany().id, 'logo_url', this.getId()),
  }
})

export default WrappedLogoUploader
