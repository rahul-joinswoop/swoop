import React from 'react'
import BaseComponent from 'components/base_component'
import CheckboxSetting from 'components/settings/configure/CheckboxSetting'
import Consts from 'consts'
import UserStore from 'stores/user_store'
import { withSplitIO } from 'components/split.io'

class GeneralSettings extends BaseComponent {
  render() {
    return (
      <div
        className="configure-component"
        id="generalSettings_in_companySettings"
      >
        <h4>General Settings</h4>
        <CheckboxSetting
          id="require_on_site"
          name="require_on_site"
          text="Require On Site status to ensure accurate ETA and ATA reporting"
        />
        <CheckboxSetting
          id="always_show_customer_email"
          name="always_show_customer_email"
          text="Always include Customer Email on job form"
        />
        <CheckboxSetting
          id="auto_progress_status"
          name="auto_progress_status"
          showing={this.props.treatments && this.props.treatments[Consts.SPLITS.GENERAL_SETTING_GEOFENCING] && UserStore.isAdmin()}
          text="Automatically progress statuses on mobile based on driver GPS location"
        />
      </div>
    )
  }
}

export default withSplitIO(GeneralSettings)
