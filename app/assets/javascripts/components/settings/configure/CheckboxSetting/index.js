import PropTypes from 'prop-types'
import React, { Component } from 'react'
import SettingStore from 'stores/setting_store'
import './style.scss'

class CheckboxSetting extends Component {
  static propTypes = {
    editIcon: PropTypes.node,
    getChecked: PropTypes.func, // getChecked returns current value
    id: PropTypes.string, // html id attribute for input
    inverted: PropTypes.bool,
    name: PropTypes.string, // name is key of setting
    onChange: PropTypes.func,
    showing: PropTypes.bool,
    style: PropTypes.object,
    subtext: PropTypes.string,
    text: PropTypes.node,
    textLabelStyle: PropTypes.object,
  }

  static defaultProps = {
    showing: true,
  }

  displayName = 'CheckboxSetting'

  state = { rev: 0 }

  componentDidMount() {
    SettingStore.bind(SettingStore.CHANGE, this.generalChanged)
  }

  componentWillUnmount() {
    SettingStore.unbind(SettingStore.CHANGE, this.generalChanged)
  }

  getChecked() {
    if (this.props.getChecked) {
      return this.props.getChecked()
    } else {
      const val = SettingStore.getSettingByKey(this.props.name)

      if (this.props.inverted) {
        return !val
      }

      return !!val
    }
  }

  generalChanged = () => this.setState(({ rev }) => ({ rev: rev + 1 }))

  handleOnChange = (e) => {
    if (this.props.onChange) {
      this.props.onChange(e)
    } else {
      let { checked } = e.target

      if (this.props.inverted) {
        checked = !checked
      }

      if (checked) {
        SettingStore.addItem({
          value: true,
          key: this.props.name,
        })
      } else {
        const setting = SettingStore.getSettingByKey(this.props.name)
        SettingStore.deleteItem({
          id: setting.id,
        })
      }
    }
  }

  render() {
    if (!this.props.showing) {
      return null
    }

    return (
      <div className="CheckboxSetting-component" style={this.props.style}>
        <input
          checked={this.getChecked()}
          id={this.props.id}
          onChange={this.handleOnChange}
          type="checkbox"
        />
        {/* eslint-disable-next-line */}
        <label
          htmlFor={this.props.id}
          style={this.props.textLabelStyle}
        >
          {this.props.text}
          {this.props.subtext &&
            <span className="CheckboxSetting-subtext">
              {this.props.subtext}
            </span>}
        </label>
        {this.props.editIcon}
      </div>
    )
  }
}

export default CheckboxSetting
