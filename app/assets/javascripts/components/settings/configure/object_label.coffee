import React from 'react'
import Dispatcher from 'lib/swoop/dispatcher'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
BaseComponent = require('components/base_component')
ModalStore = require('stores/modal_store')

class ObjectLabel extends BaseComponent
  _modalKey: "services"

  editObjects: (e) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: @props.modalKey,
      props: {}
    )

  getLabel: =>
    @props.label + if @props.value then (': ' + @props.value) else ''

  render: ->
    super(arguments...)

    h4
      style: @props.labelStyle
      @getLabel()
      if !@props.hideEditablePencil
        FontAwesomeButton
          color: 'secondary-o'
          icon: 'fa-pencil'
          name: 'edit'
          onClick: @editObjects
          size: 'medium'

module.exports = ObjectLabel
