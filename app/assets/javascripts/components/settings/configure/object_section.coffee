import React from 'react'
BaseComponent = require('components/base_component')
ObjectLabel = React.createFactory(require('components/settings/configure/object_label'))
ObjectTable = React.createFactory(require('components/settings/configure/object_table'))

class ObjectSection extends BaseComponent
  _label: "Services"
  _description: "Services are selectable when creating a job and when setting rates."

  render: ->
    super(arguments...)

    if @props.showing == false
      return null

    div
      className: "object_section configure-component"
      id: @props.id
      ObjectLabel
        label: @props.label
        hideEditablePencil: @props.hideEditablePencil
        modalKey: @props.modalKey
      div null,
        div
          className: "description"
          @props.description
        ObjectTable
          label: @props.label
          isLoading: @props.isLoading
          getItems: @props.getItems
          splitIntoThree: @props.splitIntoThree

module.exports = ObjectSection
