import React from 'react'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Utils from 'utils'
import { extend } from 'lodash'
BaseComponent = require('components/base_component')
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)


class ObjectTable extends BaseComponent
  colSpan: =>
    if @props.splitIntoThree
      3
    else
      1

  renderTableFormatted:(objects) =>
    if @props.splitIntoThree?
      splits = Utils.splitIntoThree(objects)

      for obj, i in splits[0]
        tr
          key: (@props.label + splits[0][i]).replace(' ','')
          td null,
            splits[0][i]
          td null,
            splits[1][i]
          td null,
            splits[2][i]
    else
      for obj, i in objects
        tr
          key: (@props.label + i).replace(' ','')
          td null,
            if obj
              obj
            else
              Loading null

  renderHeader: =>
    th
      colSpan: @colSpan()
      className: "object_table_header"
      span null,
      @props.label

      if @props.onEditCollection
        FontAwesomeButton
          color: 'secondary-o'
          icon: 'fa-pencil'
          name: 'edit'
          onClick: @props.onEditCollection
          size: 'medium'

  renderEmptyCollectionMessage: =>
    @props.emptyCollecionMessage || ("No "+@props.label+" Enabled")

  render: ->
    super(arguments...)

    if @props.isCommissions
      objects = @props.getItems.bind(@)()
    else
      objects = @props.getItems(@getId())

    div
      className: "object_table_wrapper"
      table
        className: "object_table"
        style: @props.style
        thead null,
          tr null,
            @renderHeader()
        tbody
          className: "object_table_body"
          if objects.length > 0
            @renderTableFormatted(objects)

          else if @props.isLoading.bind(@)()
            tr null,
              td
                colSpan: @colSpan()
                Loading null
          else
            tr null,
              td
                colSpan: @colSpan()
                span
                  className: "empty_state"
                  @renderEmptyCollectionMessage()

module.exports = ObjectTable
