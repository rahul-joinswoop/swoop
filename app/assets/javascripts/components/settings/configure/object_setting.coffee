import React from 'react'
BaseComponent = require('components/base_component')
ObjectLabel = React.createFactory(require('components/settings/configure/object_label'))

class ObjectSetting extends BaseComponent
  _appendColonOnLabel: true

  render: ->
    ObjectLabel
      label: @props.label
      value: @props.value
      modalKey: @props.modalKey
      onConfirm: @props.onConfirm

module.exports = ObjectSetting
