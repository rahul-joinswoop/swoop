import React from 'react'
BaseComponent = require('components/base_component')
import ChangeRescueCompanyImport from 'components/settings/tools/change_rescue_company'
ChangeRescueCompany = React.createFactory(ChangeRescueCompanyImport)
import CreateCompanyObjectsImport from 'components/settings/tools/create_company_objects'
CreateCompanyObjects = React.createFactory(CreateCompanyObjectsImport)
CreatePaymentType = React.createFactory(require('components/settings/tools/create_payment_type'))
DeleteStripeConnectAccount = React.createFactory(require('components/settings/tools/delete_stripe_connect_account'))
MoveVehicle = React.createFactory(require('components/settings/tools/move_vehicle'))
import UndeleteJobImport from 'components/settings/tools/undelete_job'
UndeleteJob = React.createFactory(UndeleteJobImport)
import UndeleteUserImport from 'components/settings/tools/undelete_user'
UndeleteUser = React.createFactory(UndeleteUserImport)
require('stylesheets/components/settings/tools_list.scss')

class ToolsList extends BaseComponent
  displayName: 'ToolsList'

  render: ->
    div
      className: "tools_list"
      CreatePaymentType()
      ChangeRescueCompany()
      CreateCompanyObjects()
      UndeleteUser()
      UndeleteJob()
      if UserStore.isRoot()
        MoveVehicle()
      DeleteStripeConnectAccount()

module.exports = ToolsList
