import React from 'react'
UserStore = require('stores/user_store')
AccountStore = require('stores/account_store')
FTable = React.createFactory(require 'components/resizableTable/FTable')
BaseComponent = require('components/base_component')
AccountsSection = require('components/rates/accounts_section')
RatesSection = require('components/rates/rates_section')
ServicesSection = require('components/rates/services_section')
PartnersSection = require('components/rates/partners_section')
import { extend, filter } from 'lodash'
require('stylesheets/components/rates/rates.scss')


class RatesList extends BaseComponent
  displayName: 'RatesList'

  constructor: (props) ->
    super(props)
    @state = @getInitialState()

  getInitialState: ->
    account_id: null
    service_id: null
    partner: null
    tesla_account: null
    rev: 0

  setAccount: (account_id, tesla_account = false) =>
    @setState
      account_id: account_id
      tesla_account: tesla_account
    @setService(null)

  setService: (service_id) =>
    @setState
      service_id: service_id

  setPartner: (partner) =>
    @setService(null)
    @setState
      partner: partner

  render: ->
    super(arguments...)

    service_id = @state.service_id

    hideDeletes = false
    if UserStore.isPartner() and @state.account_id
      hideDeletes = AccountStore.isTeslaAccount(@state.account_id, @getId())

    cols= [{header: "Account", show: UserStore.isPartner(), width: 300, key: "account", sortable: false, func: () =>
        AccountsSection
          setAccount: @setAccount
          account_id: @state.account_id
      }
      {header: "Partner", show: not UserStore.isPartner(), width: 300, key: "partner", sortable: false, func: () =>
        PartnersSection
          setPartner: @setPartner
          partner_id: @state.partner?.id
      }
      {header: "Service", key: "service", width: 300, sortable: false, func: () =>
        ServicesSection
          setService: @setService
          service_id: service_id
          account_id: @state.account_id
          tesla_account: @state.tesla_account
          partner_id: @state.partner?.id
      }
      {header: "Rates", key: "rates", sortable: false, func: () =>
        RatesSection
          service_id: service_id
          account_id: @state.account_id
          partner: @state.partner
          hideDeletes: hideDeletes
      }
    ]

    cols = filter(cols, ((col) -> not col.show? or col.show))

    div
      className: "rates"
      style:
        marginTop: 15
      div
        className: "ratesWidget" + if UserStore.isFleet() then " fleet" else ""
        FTable extend({}, @props,
          rows: [{}]
          rowHeight: $(window).height()-200
          headerHeight: 40
          cols: cols
        )

module.exports = RatesList
