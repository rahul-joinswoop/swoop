import React from 'react'
import Consts from 'consts'
import createReactClass from 'create-react-class'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import { extend, map, partial } from 'lodash'

{ Column } = (require 'components/tables/SortTable')
CompanyStore = require('stores/company_store').default
FTable = React.createFactory((require 'components/tables/SortTable').Table)
import SearchBox from 'components/search_box'
UserStore = require('stores/user_store')
UsersStore = require('stores/users_store').default
UserSettingStore = require('stores/user_setting_store')

import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

ICON_COL_WIDTH = 40
SMALL_WIDTH = 30

UserList = createReactClass(
  displayName: 'UserList'

  getInitialState: ->
    that = this

    cols = [
      class ID extends Column
        _header: "User ID", _key: "id", _originalWidth: 50
        isShowing: -> UserStore.isRoot()

      class Login extends Column
        _header: '', _key: "login", _originalWidth: SMALL_WIDTH
        isShowing: -> UserStore.isRoot()
        getValue: ->
          if !@record?
            return ''

          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-sign-in'
            name: 'add'
            onClick: partial(that.loginAs, @record.id)
            size: 'medium'
            style: {
              color: '#333',
            }

      class Company extends Column
        _header: "Company", _key: "company"
        isShowing: -> UserStore.isRoot()
        getValue: ->
          if !@record?
            return ''

          return @record.company_name

      class CompanyID extends Column
        _header: "Company ID", _key: "company_id", _originalWidth: 78
        isShowing: -> UserStore.isRoot()

      class FirstName extends Column
        _header: "First Name", _key: "first_name"
        getValue: ->
          span
            className: 'fs-exclude'
            @record.first_name

      class Edit extends Column
        _header: '', _key: "edit", _originalWidth: ICON_COL_WIDTH
        isShowing: -> UserStore.isAdmin() or UserStore.isRoot()
        getValue: ->
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-pencil'
            name: 'add'
            onClick: partial(that.props.editEntry, @record)
            size: 'medium'

      class LastName extends Column
        _header: "Last Name", _key: "last_name"
        getValue: ->
          span
            className: 'fs-exclude'
            @record.last_name

      class Phone extends Column
        _header: "Phone", _key: "phone"
        getValue: ->
          span
            className: 'fs-exclude'
            @record.phone

      class Email extends Column
        _header: "Email", _key: "email"
        getValue: ->
          span
            className: 'fs-exclude'
            @record.email

      class Username extends Column
        _header: "Username", _key: "username"

      class Admin extends Column
        _header: "Admin", _key: "isAdmin", _originalWidth: 60
        getValue: ->
          if !@record?
            return ''

          if @record?.roles? and "admin" in @record?.roles then "✓" else "▢"

      class Dispatcher extends Column
        _header: "Dispatcher", _key: "isDispatcher", _originalWidth: 90
        getValue: ->
          if !@record?
            return ''

          if @record?.roles? and ("dispatcher" in @record?.roles or "swoop_dispatcher" in @record?.roles) then "✓" else "▢"

      class Driver extends Column
        _header: "Driver", _key: "isDriver", _originalWidth: 65
        isShowing: -> UserStore.isPartner() or UserStore.isRoot()
        getValue: ->
          if !@record?
            return ''

          if @record?.roles? and "driver" in @record?.roles then "✓" else "▢"

      class AnsweringService extends Column
        _header: "Answering Service", _key: "isAnsweringService", _originalWidth: SMALL_WIDTH
        isShowing: -> UserStore.isPartner() or UserStore.isRoot()
        getValue: ->
          if !@record?
            return ''

          if @record?.roles? and "answering_service" in @record?.roles then "✓" else "▢"

      class Version extends Column
        _header: "Client", _key: "mobile_version"
        isShowing: -> UserStore.isRoot() or UserStore.isPartner()
        getValue: ->
          if !@record? or !@record.mobile_user_agent_human
            return ''

          @record?.mobile_user_agent_human

      class Delete extends Column
        _header: '', _key: "delete", _originalWidth: ICON_COL_WIDTH
        isShowing: -> UserStore.isRoot() or UserStore.isAdmin()
        getValue: ->
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-times'
            name: 'delete'
            onClick: partial(that.props.removeEntry, @record, { noun: 'user', title: 'Delete User' })
            size: 'medium'
    ]

    @cols = map(cols, (col) -> new col())

    lastSortBy = UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.USERS_SORTBY)

    initialSortBy = if lastSortBy then JSON.parse(lastSortBy) else undefined
    initialSortKey = initialSortBy?.key || "id"
    initialSortOrder = if initialSortBy?.order == 1 then "asc" else "desc"

    return {
      searchKey: initialSortKey
      searchOrder: initialSortOrder
      search: ' '
      pageSize: Consts.USER_PAGE_SIZE
    }

  componentDidMount: ->
    UsersStore.bind(UsersStore.CHANGE, this.usersChanged)
    UsersStore.bind(UsersStore.SEARCH_CHANGED, this.usersChanged)

    # it will trigger 'load the first page'
    @searchChange(@state.search)

  componentWillUnmount: ->
    UsersStore.unbind(UsersStore.CHANGE, this.usersChanged)
    UsersStore.unbind(UsersStore.SEARCH_CHANGED, this.usersChanged)
    UsersStore.resetSearchResult()

  usersChanged: ->
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously

  loginAs: (user_id) ->
    UserStore.authenticateAs(user_id)

  sortChange: (key, order) ->
    searchOrder = if order == 1 then "asc" else "desc"

    @setState
      searchKey: key
      searchOrder: searchOrder

    @search(@state.search, UserStore.getSearch().searchPage)

  searchChange: (value) ->
    if(value == null)
      value = ' '

    @search(value, 1)

    @setState
      search: value

  search: (value, page) ->
    order = this.state.searchKey + ',' + this.state.searchOrder

    UsersStore.search(value, null, order, page)

  render: ->
    div
      className: 'users_container'
      <SearchBox
        onChange={@searchChange}
        store={UsersStore}
        enablePagination
        showSearch
        show
      />
      div
        style:
          clear: "both"
        FTable extend({}, @props,
          tableKey: "users"
          rows: if UsersStore.isLoadingSearch() then [] else UsersStore.getRecordsOfCurrentSearchPage()
          cols: @cols
          originalSortDir: @state.searchOrder
          originalSortKey: @state.searchKey
          onSortChange: partial(@sortChange)
          rowClick: -> # just to avoid BaseTable throwing error
        )
      if UsersStore.getSearch().allLoaded and UsersStore.getSearch().total == 0 and @state?.search?
        span
          className: "empty_message"
          'No search results found'

      if UsersStore.getSearch().loading
        Loading null
)

module.exports = UserList
