import React from 'react'
import Consts from 'consts'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
UserSettingStore = require('stores/user_setting_store')
ProviderStore = require('stores/provider_store')
import TagStore from 'stores/tag_store'
FTable = React.createFactory((require 'components/tables/SortTable').Table)
{ Column } = (require 'components/tables/SortTable')
import SearchBox from 'components/search_box'
BaseComponent = require('components/base_component')
import { extend, map, partial } from 'lodash'
import Utils from 'utils'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

ICON_COL_WIDTH = 40
SMALL_WIDTH = 30

class PartnerList extends BaseComponent
  displayName: 'PartnerList'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()
    @register(ProviderStore, ProviderStore.CHANGE)
    @register(ProviderStore, ProviderStore.SEARCH_CHANGED)

  getInitialState: =>
    that = this

    cols = []

    cols.push(
      class Name extends Column
        _header: 'Name', _key: 'name'

      class CompanyID extends Column
        _header: "Company ID", _key: "company_id", _originalWidth: 78
        getValue: ->
          if @record?.company? then @record.company.id

      class Edit extends Column
        _header: '', _key: 'edit', _originalWidth: SMALL_WIDTH
        getValue: ->
          FontAwesomeButton
            color: 'secondary-o'
            icon: 'fa-pencil'
            name: 'edit'
            onClick: partial(that.props.editEntry, @record)
            size: 'medium'

      class Phone extends Column
        _header: 'Dispatch Phone', _key: 'phone', _minWidth: 110
        getValue: ->
          Utils.renderPhoneNumber(@record?.phone)

      class DispatchEmail extends Column
        _header: 'Dispatch Email', _key: 'dispatch_email'
    )

    if UserStore.isTesla()
      cols.push(class Fax extends Column
        _header: 'Fax', _key: 'fax', _minWidth: 100)

    cols.push(
      class Address extends Column
        _header: 'Address', _key: 'address'
        getValue: ->
          if @record?.location? then @record.location.address

      class PrimaryContact extends Column
        _header: 'Primary Contact', _key: 'primary_contact'

      class PrimaryPhone extends Column
        _header: 'Primary Phone', _key: 'primary_phone', _minWidth: 110
        getValue: ->
          Utils.renderPhoneNumber(@record?.primary_phone)

      class PrimaryEmail extends Column
        _header: 'Primary Email', _key: 'primary_email'

      class AccountingEmail extends Column
        _header: 'Accounting Email', _key: 'accounting_email'
    )

    if UserStore.isTesla()
      cols.push(
        class SpecialInstructions extends Column
          _header: 'Special Instructions', _key: 'notes'

        class VendorId extends Column
          _header: 'Vendor Id', _key: 'vendor_code'

        class LocationId extends Column
          _header: 'Location Id', _key: 'location_code'

        class Wheels extends Column
          _header: 'Wheels', key: 'tire_program', _originalWidth: 65
          getValue: ->
            if @record?.tire_program then "✓" else "▢"
      )

    if UserStore.isRoot() || UserStore.isFleetInHouse()
      cols.push(class NetworkStatus extends Column
        _header: 'Network Status', _key: 'network'
        getValue: ->
          if @record?.tags.length > 0
            tag_id = @record.tags[0]
            if tag_id?
              return span
                style:
                  display: "inline-block"
                  borderRadius: 0
                  paddingTop: 0
                  paddingBottom: 0
                  paddingLeft: 7
                  paddingRight: 7
                  backgroundColor: TagStore.get(tag_id, "color",that.getId()) || "#FFF"
                TagStore.get(tag_id, "name",that.getId()) || "Loading..."
          return "--"
      )

    @cols = map(cols, (col) -> new col())

    lastSortBy = UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.PARTNER_SORTBY)

    initialSortBy = if lastSortBy then JSON.parse(lastSortBy) else undefined
    initialSortKey = initialSortBy?.key || "id"
    initialSortOrder = if initialSortBy?.order == 1 then "asc" else "desc"

    return {
      searchKey: initialSortKey
      searchOrder: initialSortOrder
      search: ' '
      pageSize: Consts.PARTNER_PAGE_SIZE
    }

  componentDidMount: ->
    super(arguments...)

    # it will trigger 'load the first page'
    @searchChange(@state.search)

  componentWillUnmount: ->
    super(arguments...)

    ProviderStore.resetSearchResult()


  sortChange: (key, order) =>
    console.log("partners search order key: #{key}, order: #{order}")
    searchOrder = if order == 1 then "asc" else "desc"

    @setState
      searchKey: key
      searchOrder: searchOrder

    @search(@state.search, ProviderStore.getSearch().searchPage)

  searchChange: (value) =>
    if(value == null)
      value = ' '

    @search(value, 1)

    @setState
      search: value

  search: (value, page) =>
    order = @state.searchKey + ',' + @state.searchOrder

    ProviderStore.search(value, null, order, page)

  render: ->
    super(arguments...)

    div
      className: 'partners_container'
      <SearchBox
        onChange={@searchChange}
        store={ProviderStore}
        enablePagination
        showSearch
        show
      />
      div
        style:
          clear: "both"
        FTable extend({}, @props,
          tableKey: "partners"
          rows: if ProviderStore.isLoadingSearch() then [] else ProviderStore.getRecordsOfCurrentSearchPage()
          cols: @cols
          originalSortDir: @state.searchOrder
          originalSortKey: @state.searchKey
          onSortChange: partial(@sortChange)
          rowClick: -> # just to avoid BaseTable throwing error
        )
      if ProviderStore.getSearch().allLoaded and ProviderStore.getSearch().total == 0 and @state?.search?
        span
          className: "empty_message"
          'No search results found'

      if ProviderStore.getSearch().loading
        Loading null

module.exports = PartnerList
