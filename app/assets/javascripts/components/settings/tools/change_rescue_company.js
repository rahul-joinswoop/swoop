import Consts from 'consts'
import Api from 'api'
import React from 'react'
import Button from 'componentLibrary/Button'
import Renderer from 'form_renderer'
import BaseComponent from 'components/base_component'
import { changeRescueCompanyFields } from 'components/settings/tools/input_fields'
import CompanyStore from 'stores/company_store'
import TransitionModal from 'components/modals'
import { withModalContext } from 'componentLibrary/Modal'

import Utils from 'utils'
import SettingsContainer from 'components/settings/settings_container'

class ChangeRescueCompany extends BaseComponent {
  constructor(props) {
    super(props)
    this.form = new Renderer({}, this.rerender, {
      componentId: this.getId(),
    })
    this.form.registerFields(changeRescueCompanyFields)
  }

  handleConfirm = (newrecord) => {
    Api.changeProvider(newrecord, {
      success() {
        Utils.sendNotification('Job was successfully changed', Consts.SUCCESS)
      },
      error(data, textStatus, errorThrown) {
        Utils.sendNotification('Job failed to change, check inputs and try again', Consts.ERROR)
      },
    })
    this.props.hideModals()
  }

  submit = () => {
    if (this.form.isValid()) {
      const newrecord = this.form.getRecordChanges()
      this.props.showModal(
        <TransitionModal
          confirm="Submit"
          disabled={false}
          extraClasses="simple_modal"
          onCancel={this.props.hideModals}
          onConfirm={() => this.handleConfirm(newrecord)}
          show
          title="Confirmation"
        >{`Are you sure you want to move Job ${newrecord.job_id} \
from ${CompanyStore.get(newrecord.old_rescue_provider_id, 'name')} \
to ${CompanyStore.get(newrecord.new_rescue_provider_id, 'name')}?`}
        </TransitionModal>
      )
    } else {
      this.form.attemptedSubmit()
      this.rerender()
    }
  }

  render() {
    return (
      <SettingsContainer title="Change Rescue Company For Job">
        <div key="Change Rescue Company For Job">
          <div className="tool_wrapper">
            {this.form.renderInputs(['old_rescue_provider_id', 'new_rescue_provider_id', 'job_id'])}
          </div>
          <Button
            color="primary"
            label="Submit"
            onClick={this.submit}
            style={{ margin: '0 0 20px 20px' }}
          />
        </div>
      </SettingsContainer>
    )
  }
}

export default withModalContext(ChangeRescueCompany)
