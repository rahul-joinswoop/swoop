import Api from 'api'
import { clone, map } from 'lodash'
CompanyStore = require('stores/company_store').default
Fields = require('fields/fields')

CompanyIdField: class CompanyIdField extends Fields.BackendSuggestField
  getSuggestions: (options, callback) ->
    Api.autocomplete_company(
      'Company',
      options.input,
      10,
      {
        success: (data) ->
          locationMap = map(data, (company) ->
            id: company.id
            label: company.name
          )
          callback(locationMap)
        error: (data, textStatus, errorThrown) ->
          console.warn('Autocomplete failed')
      }
    )
  getValue: ->
    if @record[@getName()]
      CompanyStore.get(@record[@getName()], 'name')
  _required: true

JobIdField: class JobIdField extends Fields.NumberField
  _name: 'job_id'
  _label: 'Job ID'
  _required: true

ObjectCountField: class ObjectCountField extends Fields.NumberField
  _name: 'count'
  _label: 'Count'
  _required: true

PaymentTypeField: class PaymentTypeField extends Fields.Field
  _name: 'name'
  _label: 'Payment Type Name'
  _required: true

PartnerCompanyId: class PartnerCompanyId extends Fields.NumberField
  _name: 'name'
  _label: 'Partner Company ID'
  _required: true

TypeField: class TypeField extends Fields.SelectField
  _name: 'type'
  _label: 'Type'
  _required: true
  getOptions: ->
    @optionMapper(
      [
        'Account',
        'FleetJob',
        'Provider',
        'RescueCompany',
        'RescueVehicle',
        'RescueJob',
        'Site',
        'User'
      ],
      clone(super(arguments...)))

UndeleteJobIdField: class UndeleteJobIdField extends Fields.NumberField
  _name: 'id'
  _label: 'Job ID'
  _required: true

UserEmailField: class UserEmailField extends Fields.EmailField
  _name: 'email'
  _label: 'Email'
  _required: true

UsernameField: class UsernameField extends Fields.Field
  _name: 'username'
  _label: 'Username'
  _required: true

# Extended fields below
CreateCompanyObjectsCompanyIdField: class CreateCompanyObjectsCompanyIdField extends CompanyIdField
  _name: 'company_id'
  _label: 'Company'
  _required: true

NewCompanyIdField: class NewCompanyIdField extends CompanyIdField
  _name: 'new_rescue_provider_id'
  _label: 'New Company'

OldCompanyIdField: class OldCompanyIdField extends CompanyIdField
  _name: 'old_rescue_provider_id'
  _label: 'Old Company'

# Form input field combos
changeRescueCompanyFields = {
  OldCompanyIdField
  NewCompanyIdField
  JobIdField
}

createCompanyObjectsFields = {
  CreateCompanyObjectsCompanyIdField
  TypeField
  ObjectCountField
}

paymentTypeField = {
  PaymentTypeField
}

partnerCompanyId = {
  PartnerCompanyId
}

undeleteJobField = {
  UndeleteJobIdField
}

undeleteUserEmailField = {
  UserEmailField
}

undeleteUsernameField = {
  UsernameField
}

module.exports = {
  changeRescueCompanyFields
  createCompanyObjectsFields
  paymentTypeField
  partnerCompanyId
  undeleteJobField
  undeleteUserEmailField
  undeleteUsernameField
}
