import React from 'react'
import Button from 'componentLibrary/Button'
import { clone } from 'lodash'
Renderer = require('form_renderer')
{ partnerCompanyId } = require('components/settings/tools/input_fields')
BaseComponent = require('components/base_component')
import SettingsContainerImport from 'components/settings/settings_container'
SettingsContainer = React.createFactory(SettingsContainerImport)
NotificationStore = require('stores/notification_store')
CustomAccountStore = require('stores/custom_account_store')
import SimpleModalImport from 'components/modals/simple_modal'
SimpleModal = React.createFactory(SimpleModalImport)
AsyncRequestStore = require('stores/async_request_store').default
import { withModalContext } from 'componentLibrary/Modal'

import Utils from 'utils'

class DeleteStripeConnectAccount extends BaseComponent
  constructor: (props) ->
    super(props)
    @registerForm()

  registerForm: ->
    @form = new Renderer({}, @rerender, {
      componentId: @getId()
    })
    @form.registerFields(partnerCompanyId)

  submit: =>
    if @form.isValid()
      data = clone(@form.record)
      hideModals = @props.hideModals
      @props.showModal(
        SimpleModal
          title: 'Confirmation'
          confirm: 'Submit'
          body: "Are you sure you want to delete the Stripe Connect account for Partner Company ID #{data.name}?"
          onConfirm: ->
            AsyncRequestStore.deleteStripeAccount(data, {
                success: ->
                  Utils.sendSuccessNotification('Stripe Connect account successfully deleted')
                error: (data, textStatus, errorThrown) ->
                  errorMessage = data?.error?.custom_account?[0]
                  if !errorMessage && data?.responseJSON?.action? && data?.responseJSON?.message?
                    errorMessage = data?.responseJSON?.action + ': ' + data?.responseJSON?.message
                  if !errorMessage
                    errorMessage = 'An error occurred processing your request. Please contact our support chat (in the bottom right).'
                  Utils.sendErrorNotification(errorMessage)
              })
            hideModals()
      )
    else
      @form.attemptedSubmit()
      @rerender()

  render: ->
    SettingsContainer
      title: 'Delete Stripe Connect Account'
      div
        className: 'tool_wrapper'
        @form.renderInput('name')
      Button
        color: 'primary'
        label: 'Delete'
        onClick: @submit
        style: {
          margin: '0 0 20px 20px',
        }

CreatePaymentType = React.createFactory(DeleteStripeConnectAccount)

module.exports = withModalContext(DeleteStripeConnectAccount)
