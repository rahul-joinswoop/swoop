import Consts from 'consts'
import Api from 'api'
import React from 'react'
import Utils from 'utils'
import Button from 'componentLibrary/Button'
import Renderer from 'form_renderer'
import BaseComponent from 'components/base_component'
import { createCompanyObjectsFields } from 'components/settings/tools/input_fields'
import SettingsContainer from 'components/settings/settings_container'
import { withModalContext } from 'componentLibrary/Modal'
import TransitionModal from 'components/modals'
import CompanyStore from 'stores/company_store'

class CreateCompanyObjects extends BaseComponent {
  constructor(props) {
    super(props)
    this.form = new Renderer({}, this.rerender, {
      componentId: this.getId(),
    })
    this.form.registerFields(createCompanyObjectsFields)
  }

  handleConfirm = (newrecord) => {
    Api.createObjects({
      fake: newrecord,
    }, {
      success() {
        Utils.sendNotification('Objects were successfully created', Consts.SUCCESS)
      },

      error(data, textStatus, errorThrown) {
        Utils.sendNotification('Objects failed to create, try again', Consts.ERROR)
      },
    })

    this.props.hideModals()
  }

  submit = () => {
    if (this.form.isValid()) {
      const newrecord = this.form.getRecordChanges()
      this.props.showModal(
        <TransitionModal
          confirm="Submit"
          disabled={false}
          extraClasses="simple_modal"
          onCancel={this.props.hideModals}
          onConfirm={() => this.handleConfirm(newrecord)}
          show
          title="Confirmation"
        >{`Are you sure you want to create ${newrecord.count} \
${newrecord.type}s for \
${CompanyStore.get(newrecord.company_id, 'name')}?`}
        </TransitionModal>
      )
    } else {
      this.form.attemptedSubmit()
      this.rerender()
    }
  }

  render() {
    return (
      <SettingsContainer title="Create objects for company">
        <div key="Create objects for company">
          <div className="tool_wrapper">
            {this.form.renderInputs(['company_id', 'type', 'count'])}
          </div>
          <Button
            color="primary"
            label="Submit"
            onClick={this.submit}
            style={{ margin: '0 0 20px 20px' }}
          />
        </div>
      </SettingsContainer>
    )
  }
}

export default withModalContext(CreateCompanyObjects)
