import React from 'react'
import Api from 'api'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import Utils from 'utils'
import { clone } from 'lodash'
import BaseComponent from 'components/base_component'
import { undeleteUserEmailField, undeleteUsernameField } from 'components/settings/tools/input_fields'
import Renderer from 'form_renderer'
import SettingsContainer from 'components/settings/settings_container'
import TransitionModal from 'components/modals'
import { withModalContext } from 'componentLibrary/Modal'

class UndeleteUser extends BaseComponent {
  constructor(props) {
    super(props)
    this.registerForm()
  }

  registerForm = () => {
    this.formUsername = new Renderer({}, this.rerender, {
      componentId: this.getId(),
    })
    this.formEmail = new Renderer({}, this.rerender, {
      componentId: this.getId(),
    })
    this.formUsername.registerFields(undeleteUsernameField)
    this.formEmail.registerFields(undeleteUserEmailField)
  }

  handleConfirm = (data) => {
    Api.undeleteUser(data, {
      success() {
        Utils.sendNotification('User was successfully restored', Consts.SUCCESS)
      },

      error(data, textStatus, errorThrown) {
        Utils.sendNotification('User restore failed, check inputs and try again', Consts.ERROR)
      },
    })

    this.props.hideModals()
  }

  restoreUser = (data) => {
    this.props.showModal(
      <TransitionModal
        confirm="Submit"
        disabled={false}
        extraClasses="simple_modal"
        onCancel={this.props.hideModals}
        onConfirm={() => this.handleConfirm(data)}
        show
        title="Confirmation"
      >
        {`Are you sure you want to restore user ${data.email || data.username}?`}
      </TransitionModal>
    )
  }

  submitEmail = () => {
    if (this.formEmail.isValid()) {
      const data = clone(this.formEmail.record)
      this.restoreUser(data)
    } else {
      this.formEmail.attemptedSubmit()
      this.rerender()
    }
  }

  submitUsername = () => {
    if (this.formUsername.isValid()) {
      const data = clone(this.formUsername.record)
      this.restoreUser(data)
    } else {
      this.formUsername.attemptedSubmit()
      this.rerender()
    }
  }

  render() {
    return (
      <SettingsContainer title="Undelete User">
        <div key="Undelete User">
          <div className="tool_wrapper">
            {this.formEmail.renderInput('email')}
          </div>
          <Button
            color="primary"
            label="Undelete"
            onClick={this.submitEmail}
            style={{ margin: '0 0 20px 20px' }}
          />
          <div className="tool_wrapper">
            {this.formUsername.renderInput('username')}
          </div>
          <Button
            color="primary"
            label="Undelete"
            onClick={this.submitUsername}
            style={{ margin: '0 0 20px 20px' }}
          />
        </div>
      </SettingsContainer>
    )
  }
}

export default withModalContext(UndeleteUser)
