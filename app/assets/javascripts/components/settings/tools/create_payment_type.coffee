import React from 'react'
import Button from 'componentLibrary/Button'
import { clone } from 'lodash'
{ paymentTypeField } = require('components/settings/tools/input_fields')
Renderer = require('form_renderer')
CustomerTypeStore = require('stores/customer_type_store')
BaseComponent = require('components/base_component')
import SettingsContainerImport from 'components/settings/settings_container'
SettingsContainer = React.createFactory(SettingsContainerImport)

class CreatePaymentType extends BaseComponent
  constructor: (props) ->
    super(props)
    @registerForm()

  registerForm: ->
    @form = new Renderer({}, @rerender, {
      componentId: @getId()
    })
    @form.registerFields(paymentTypeField)

  submit: =>
    if @form.isValid()
      data = clone(@form.record)
      data.callbacks = {
        success: (a, b) =>
          @registerForm()
          @rerender()
        error: (errorResponse) =>
          @form.setResponseError errorResponse.responseJSON
          return true
      }
      CustomerTypeStore.addItem(data)
    else
      @form.attemptedSubmit()
      @rerender()

  render: ->
    SettingsContainer
      title: 'Create Payment Types'
      div
        key: "Create Payment Types"
        div
          className: 'tool_wrapper'
          @form.renderInput('name')
        Button
          color: 'primary'
          label: 'Create'
          onClick: @submit
          style: {
            margin: '0 0 20px 20px',
          }

CreatePaymentType = React.createFactory(CreatePaymentType)

module.exports = CreatePaymentType
