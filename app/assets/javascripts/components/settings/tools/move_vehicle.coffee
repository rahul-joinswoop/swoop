import React from 'react'
import Button from 'componentLibrary/Button'
import { partial } from 'lodash'
Renderer = require('form_renderer')
VehicleStore = require('stores/vehicle_store')
BaseComponent = require('components/base_component')
import SettingsContainerImport from 'components/settings/settings_container'
SettingsContainer = React.createFactory(SettingsContainerImport)
Fields = require('fields/fields')

VehicleIdField: class VehicleIdField extends Fields.NumberField
  _name: 'vehicle_id'
  _label: 'Vehicle ID'
  _required: true

FromAddress: class FromAddress extends Fields.GeoSuggestField
  _name: 'from'
  _label: 'From Address'
  _required: true

ToAddress: class ToAddress extends Fields.GeoSuggestField
  _name: 'to'
  _label: 'To Address'
  _required: true

Seconds: class Seconds extends Fields.NumberField
  _name: 'seconds'
  _label: 'Seconds'
  _required: true

moveTruck = (id, fromLat, fromLng, toLat, toLng, time, ittr=0) ->
  if ittr > time
    VehicleStore.updateItem({id: id, lat: toLat, lng: toLng})
    return

  totalIterations = time

  currentLat = fromLat + (((toLat - fromLat)/totalIterations)*ittr)
  currentLng = fromLng + (((toLng - fromLng)/totalIterations)*ittr)

  VehicleStore.updateItem({id: id, lat: currentLat, lng: currentLng})
  setTimeout(partial(moveTruck, id, fromLat, fromLng, toLat, toLng, time, ittr+1), 1000)

class MoveTruck extends BaseComponent
  constructor: (props) ->
    super(props)
    @registerForm()

  registerForm: ->
    @form = new Renderer({}, @rerender, {
      componentId: @getId()
    })
    @form.registerFields(VehicleIdField:VehicleIdField)
    @form.registerFields(FromAddress:FromAddress)
    @form.registerFields(ToAddress:ToAddress)
    @form.registerFields(Seconds:Seconds)


  submit: =>
    if @form.isValid()
      alert("Moving Truck for #{@form.record.seconds} Seconds")
      record = @form.record
      moveTruck(record.vehicle_id, record.from.lat, record.from.lng, record.to.lat, record.to.lng, record.seconds)
    else
      @form.attemptedSubmit()
      @rerender()

  render: ->
    SettingsContainer
      title: 'Simulate Truck Movement'
      div
        key: 'Simulate Truck Movement'
        div
          className: 'tool_wrapper'
          @form.renderInput('vehicle_id')
          @form.renderInput('from')
          @form.renderInput('to')
          @form.renderInput('seconds')
        Button
          color: 'primary'
          label: 'Run Truck Simulation'
          onClick: @submit
          style: {
            margin: '0 0 20px 20px',
          }

MoveTruck = React.createFactory(MoveTruck)

module.exports = MoveTruck
