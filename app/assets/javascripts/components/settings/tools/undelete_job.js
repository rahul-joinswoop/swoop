import React from 'react'
import Api from 'api'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import Utils from 'utils'
import { clone } from 'lodash'
import BaseComponent from 'components/base_component'
import { undeleteJobField } from 'components/settings/tools/input_fields'
import Renderer from 'form_renderer'
import SettingsContainer from 'components/settings/settings_container'
import TransitionModal from 'components/modals'
import { withModalContext } from 'componentLibrary/Modal'

class UndeleteJob extends BaseComponent {
  constructor(props) {
    super(props)
    this.registerForm()
  }

  registerForm = () => {
    this.form = new Renderer({}, this.rerender, {
      componentId: this.getId(),
    })
    this.form.registerFields(undeleteJobField)
  }

  handleConfirm = (data) => {
    Api.undeleteJob(data, {
      success() {
        Utils.sendNotification('Job was successfully restored', Consts.SUCCESS)
      },

      error(data, textStatus, errorThrown) {
        Utils.sendNotification('Job restore failed, check inputs and try again', Consts.ERROR)
      },
    })
    this.props.hideModals()
  }

  submit = () => {
    if (this.form.isValid()) {
      const data = clone(this.form.record)
      this.props.showModal(
        <TransitionModal
          confirm="Submit"
          disabled={false}
          extraClasses="simple_modal"
          onCancel={this.props.hideModals}
          onConfirm={() => this.handleConfirm(data)}
          show
          title="Confirmation"
        >
          {`Are you sure you want to restore Job ${data.id}?`}
        </TransitionModal>
      )
    } else {
      this.form.attemptedSubmit()
      this.rerender()
    }
  }

  render() {
    return (
      <SettingsContainer title="Undelete job">
        <div key="Undelete job">
          <div className="tool_wrapper">
            {this.form.renderInput('id')}
          </div>
          <Button
            color="primary"
            label="Undelete"
            onClick={this.submit}
            style={{ margin: '0 0 20px 20px' }}
          />
        </div>
      </SettingsContainer>
    )
  }
}

export default withModalContext(UndeleteJob)
