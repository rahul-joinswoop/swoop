import React from 'react'
import classNames from 'classnames'

const TimerIcon = ({ iconExtraClasses, color } = {}) =>
  <i
    aria-hidden="true"
    className={classNames('fa fa-clock-o timer-icon', iconExtraClasses)}
    style={{ color }}
  />

export default TimerIcon
