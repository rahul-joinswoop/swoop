import Api from 'api'
import createReactClass from 'create-react-class'
import React from 'react'
import Utils from 'utils'
import Consts from 'consts'
UserStore = require('stores/user_store')
import LookerDashboardStore from 'stores/looker_dashboard_store'
TimerMixin = require('react-timer-mixin')
import { values } from 'lodash'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)


class LookerFrame extends React.Component
  mixins: [ TimerMixin ]
  displayName: 'LookerFrame'

  constructor: (props) ->
    super(props)
    @state = {
      spinners: []
      loading: true
    }


  componentDidMount: ->
    dashboards = values(LookerDashboardStore.getAll())
    @getLook(dashboards?[0]?.id)
    Tracking.track('Insights Visit', {
      category: 'Reporting'
    })
    #TODO: this will update the list on a lookerdashboard changing, but not the URL
    LookerDashboardStore.bind(LookerDashboardStore.CHANGE, this.generalChanged)

  componentWillUnmount: ->
    LookerDashboardStore.unbind(LookerDashboardStore.CHANGE, this.generalChanged)
    clearTimeout(@timeoutId)

  generalChanged: =>
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously

  renderFrame: () =>
    if !@state.loading
      React.createElement "iframe",
        ref: "iframe",
        frameBorder: "0",
        src: @state.url
        height: "100%",
        width:  "100%",
        style:
          height: "calc(100VH - 100px)",
          width:  "100%"
    else
      Loading null

  dashSwitch: (e) =>
    @getLook(e.target.value)
    @setState
      loading:true

  getLook: (dashboard_id) =>
    if dashboard_id and Utils.getEndpoint(UserStore)?
      clearTimeout(@timeoutId)
      Api.lookerReport(Utils.getEndpoint(UserStore), dashboard_id, {
        success: (data) =>
          console.log "LF Received looker obj", data
          @setState
            url: data.url
            loading: false
          @timeoutId = setTimeout(@getLook, 1000*60*15)
        error: (data, textStatus, errorThrown) =>
          Utils.sendNotification("Failed to get Look", Consts.ERROR)
          Utils.handleError(data, textStatus, errorThrown, true)
      })


  render: ->
    dashboards = values(LookerDashboardStore.getAll())

    div
      style:
        paddingTop:"10px"
      if dashboards
        select
          style:
            width:"200px"
          className: 'form-control'
          onChange: @dashSwitch
          for dash in dashboards
            option
              key: dash.id
              value: dash.id
              className: 'option_row'
              dash.name

      @renderFrame()


module.exports = LookerFrame
