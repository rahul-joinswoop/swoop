import React from 'react'
import { hot } from 'react-hot-loader'
import { partial, map } from 'lodash'
import { RouterProvider } from 'react-router5'
import $ from 'jquery'
import AgeroIntegrationInvalidRole from 'components/settings/integrations/rsc/agero_intergration_invalid_role'
import BaseConsts from 'base_consts'
import browserPlugin from 'router5-plugin-browser'
import ChangePassword from 'components/login/change_password'
import CompanyExists from 'components/login/company_exists'
import Context from 'Context'
import CreateAccount from 'components/login/create_account'
import createRouter from 'router5'
import Dispatcher from 'lib/swoop/dispatcher'
import EnterpriseBorder from 'components/EnterpriseBorder'
import FleetMapNew from 'components/FleetMap'
import FleetMap from 'components/fleet_map'
import ForgotPassword from 'components/login/forgot_password'
import ForgotPasswordSuccess from 'components/login/forgot_password_success'
import Invoices from 'components/invoices'
import JobsIndex from 'components/job/jobs_index'
import JobStore from 'stores/job_store'
import Loading from 'componentLibrary/Loading'
import Logger from 'lib/swoop/logger'
import Login from 'components/login/login'
import Modals from 'components/modal_components'
import Notifications from 'components/Notifications'
import PartnerApiAllowAccess, { LinkExpired as PartnerApiLinkExpired } from 'components/PartnerOnBoarding/AllowAccess'
import PartnerApiLandingPage from 'components/PartnerOnBoarding/LandingPage'
import PartnerApiPasswordRequests from 'components/PartnerOnBoarding/SetupAccount'
import PartnerApiSetupBilling from 'components/PartnerOnBoarding/SetupAccount/SetupBilling'
import PartnerFleet from 'components/PartnerFleetMap'
import Reporting from 'components/reporting'
import Reviews from 'components/reviews_index'
import Settings from 'components/settings'
import Signup from 'components/login/signup'
import StorageIndex from 'components/storage_index'
import TerritoriesNetworkMap from 'components/maps/Territories/TerritoriesNetworkMap'
import Tracking from 'tracking'
import TransitionModal from 'components/modals'
import UserStore from 'stores/user_store'
import Utils from 'utils'
import VersionStore from 'stores/version_store'
import { withSplitIO } from 'components/split.io'

VersionStore.fetchVersion()

class RouterClass extends React.Component {
  static defaultProps = {
    current_user: {
      role: 'admin',
      home_url: '/',
    },
    env: {
      GOOGLE_BROWSER_API_KEY: '',
      path: 'dashboard',
    },
  }

  constructor(props) {
    super(props)
    this.state = {
      rev: 0,
      page: 'login',
    }
  }

  componentDidMount() {
    this.setRoutes()
    return VersionStore.bind(VersionStore.REFRESH_REQUIRED, this.updateRequired)
  }

  componentWillUnmount() {
    return VersionStore.unbind(VersionStore.REFRESH_REQUIRED, this.updateRequired)
  }

  setRoutes = () => {
    this.routes = {
      'partner/dispatch': { path: '/partner/dispatch', page: this.routePartnerDispatch },
      dispatch: { path: '/dispatch', page: this.routePartnerDispatch },
      'partner/fleet': { path: '/partner/fleet', page: this.routePartnerFleet },
      'partner/storage': { path: '/partner/storage', page: this.routePartnerStorage },
      'fleet/storage': { path: '/fleet/storage', page: this.routePartnerStorage },
      territories: { path: '/territories', page: this.routeSwoopTerritories },
      'fleet/map': { path: '/fleet/map', page: this.routeFleetMap },
      'partner/invoices': { path: '/partner/invoices', page: this.routeInvoices },
      'fleet/invoices': { path: '/fleet/invoices', page: this.routeInvoices },
      invoices: { path: '/invoices', page: this.routeInvoices },
      'partner/reporting': { path: '/partner/reporting', page: this.routeReporting },
      'fleet/reporting': { path: '/fleet/reporting', page: this.routeReporting },
      reporting: { path: '/reporting', page: this.routeReporting },
      'fleet/dashboard': { path: '/fleet/dashboard', page: this.routeFleet },
      fleet: { path: '/fleet', page: this.routeFleet },
      login: { path: '/login', page: this.routeLogin },
      signup: { path: '/signup', page: this.routeSignup },
      createAccount: { path: '/createAccount', page: this.routeCreateAccount },
      forgotPassword: { path: '/forgotPassword', page: this.routeForgotPassword },
      forgotPasswordSuccess: { path: '/forgotPasswordSuccess', page: this.routeForgotPasswordSuccess },
      'user/reset': { path: '/user/reset', page: this.routeChangePassword },
      companyExists: { path: '/companyExists', page: this.routeCompanyExists },
      'user/settings': { path: '/user/settings', page: this.routeSettings },
      test: { path: '/test', page: this.routeTest },
      'fleet/reviews': { path: '/fleet/reviews', page: this.routeReviews },
      'partner/reviews': { path: '/partner/reviews', page: this.routeReviews },
      reviews: { path: '/reviews', page: this.routeReviews },
      ageroIntegrationInvalidRole: { path: '/ageroIntegrationInvalidRole', page: this.routeAgeroIntegrationInvalidRole },
      'jobs/show/vin': { path: '/jobs/show/vin/:vin', page: this.routeShowExistingJob }, // showJob, login
      'auth/partner': { path: '/auth/partner', page: this.routePartnerApiLandingPage },
      'oauth/authorize': { path: '/oauth/authorize', page: this.routePartnerApiAllowAccess },
      'auth/password_requests': { path: '/auth/password_requests/:uuid', page: this.routePartnerApiPasswordRequests },
      'auth/email_verifications': { path: '/auth/email_verifications/:uuid', page: this.routePartnerApiLinkExpired },
      'partner/setupBilling': { path: '/partner/setupBilling', page: this.routePartnerApiSetupBilling },
      // { name: 'resetPasswordTargetHack', path: /^user\/reset.*/,       page: 'changePassword'        },
    }

    const routes = map(this.routes, ({ path }, name) => ({ name, path }))

    this.router5 = createRouter(routes, { queryParamsMode: 'loose' })
    this.router5.usePlugin(browserPlugin())

    Context.setRouter5(this.router5)

    this.router5.start(this.startRouting)

    this.router5.subscribe(({ route }) => {
      this.routes[route.name].page()
    })
  }

  getPage() {
    switch (this.state.page) {
      case 'partnerDispatch':
        return (
          <EnterpriseBorder active="dashboard" path={this.state.page}>
            <JobsIndex path={this.state.page} />
          </EnterpriseBorder>
        )

      case 'fleetMap': {
        let component
        if (this.props.treatments?.[BaseConsts.SPLITS.DISABLE_NEW_FLEET_MAP]) {
          component = <FleetMap />
        } else {
          component = <FleetMapNew />
        }

        return (
          <EnterpriseBorder
            active="map"
            path={this.state.page}
            showFooter={false}
          >
            {component}
          </EnterpriseBorder>
        )
      }
      case 'partnerFleet':
        return (
          <EnterpriseBorder
            active="fleet"
            path={this.state.page}
            showFooter={false}
          >
            <PartnerFleet />
          </EnterpriseBorder>
        )
      case 'swoopTerritories':
        return (
          <EnterpriseBorder
            active="territories"
            path={this.state.page}
            showFooter={false}
          >
            <TerritoriesNetworkMap />
          </EnterpriseBorder>
        )

      case 'partnerStorage':
        return (
          <EnterpriseBorder
            active="storage"
            path={this.state.page}
          >
            <StorageIndex />
          </EnterpriseBorder>
        )

      case 'ageroIntegrationInvalidRole':
        return (
          <EnterpriseBorder
            active={null}
            onlyShowLogo
            path={this.state.page}
            showFooter={false}
          >
            <AgeroIntegrationInvalidRole />
          </EnterpriseBorder>
        )

      case 'invoices':
        return (
          <EnterpriseBorder
            active="invoices"
            path={this.state.page}
          >
            <Invoices />
          </EnterpriseBorder>
        )

      case 'fleetDashboard':
        return (
          <EnterpriseBorder
            active="dashboard"
            path={this.state.page}
          >
            <JobsIndex path={this.state.page} />
          </EnterpriseBorder>
        )

      case 'login':
        return <Login />

      case 'forgotPassword':
        return <ForgotPassword />

      case 'companyExists':
        return <CompanyExists />

      case 'signup':
        return <Signup />

      case 'createAccount':
        return <CreateAccount />

      case 'forgotPasswordSuccess':
        return <ForgotPasswordSuccess />

      case 'changePassword':
        return <ChangePassword />

      case 'reporting':
        return (
          <EnterpriseBorder
            active="reporting"
            path={this.state.page}
          >
            <Reporting />
          </EnterpriseBorder>
        )

      case 'loading':
        return (
          <EnterpriseBorder
            path={this.state.page}
          >
            <Loading />
          </EnterpriseBorder>
        )

      case 'showJob':
        return (
          <EnterpriseBorder
            active="dashboard"
            path={this.state.page}
          >
            <JobsIndex
              path={this.state.page}
              showJobId={this.state.showJobId}
            />
          </EnterpriseBorder>
        )

      case 'settings':
        return (
          <EnterpriseBorder
            active="settings"
            path={this.state.page}
          >
            <Settings />
          </EnterpriseBorder>
        )

      case 'reviews':
        return (
          <EnterpriseBorder
            active="reviews"
            path={this.state.page}
          >
            <Reviews />
          </EnterpriseBorder>
        )

      case 'partnerApiLandingPage':
        return <PartnerApiLandingPage />

      case 'partnerApiPasswordRequests':
        return <PartnerApiPasswordRequests />

      case 'partnerApiAllowAccess':
        return <PartnerApiAllowAccess />

      case 'partnerApiLinkExpired':
        return <PartnerApiLinkExpired />

      case 'partnerApiSetupBilling':
        return <PartnerApiSetupBilling />

      default:
        return <div>test</div>
    }
  }


  setLoginHref() {
    return this.router5.navigate('login')
  }

  startRouting = (error, route) => {
    // check if there is old style hash route
    if (error && window.location.hash) {
      const hash = `/${window.location.hash.substring(1)}${window.location.search}`
      const redirectRoute = this.router5.matchPath(hash)
      // redirect if there is route
      if (redirectRoute) {
        setTimeout(() => {
          window.location.hash = ''
          this.router5.navigate(redirectRoute.name, redirectRoute.params, { replace: true })
          this.setState({ rev: 1 })
        }, 0)
        return
      }
    }

    const routeName = route?.name || UserStore.getUserUrl()
    this.routes[routeName].page()
    this.setState({ rev: 1 })
  }

  generalChanged = () => this.setState(prevState => ({ rev: prevState.rev + 1 }))

  notFound(path) {
    return Logger.debug(path)
  }


  refresh() {
    return location.reload()
  }

  refreshSnooze() {
    this.setState({
      update: false,
    })

    if (!this.state.keepModal) {
      $('.modal-backdrop.fade.in').remove()
      $('body').removeClass('modal-open')
    }

    return setTimeout(() => this.setState({
      keepModal: $('body').hasClass('modal-open'),
      update: true,
    }), 60000)
  }

  routeAfterFetchingVin(vin) {
    return JobStore.fetchJobByVin(vin, job => this.setState({
      page: 'showJob',
      showJobId: job.id,
    }))
  }

  routeAgeroIntegrationInvalidRole() {
    return this.setState({
      page: 'ageroIntegrationInvalidRole',
    })
  }

  routeChangePassword() {
    Logger.debug('routed to change password')
    return this.setState({
      page: 'changePassword',
    })
  }

  routeCompanyExists() {
    return this.setState({
      page: 'companyExists',
    })
  }

  routeCreateAccount() {
    return this.setState({
      page: 'createAccount',
    })
  }

  routeFleet() {
    setTimeout(() => Dispatcher.send(Context.CONTEXT_CHANGED), 0)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'fleetDashboard',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routeFleetMap() {
    Dispatcher.send(Context.CONTEXT_CHANGED)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'fleetMap',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routeForgotPassword() {
    Logger.debug('routed to forgot_password')
    return this.setState({
      page: 'forgotPassword',
    })
  }

  routeForgotPasswordSuccess() {
    Logger.debug('routed to forgot_password_success')
    return this.setState({
      page: 'forgotPasswordSuccess',
    })
  }

  routeInvoices() {
    Dispatcher.send(Context.CONTEXT_CHANGED)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'invoices',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routeLogin() {
    Logger.debug('routed to login')
    return this.setState({
      page: 'login',
    })
  }

  routePartnerApiLandingPage() {
    this.setState({ page: 'partnerApiLandingPage' })
  }

  routePartnerApiPasswordRequests() {
    this.setState({ page: 'partnerApiPasswordRequests' })
  }

  routePartnerApiLinkExpired() {
    this.setState({ page: 'partnerApiLinkExpired' })
  }

  routePartnerApiSetupBilling() {
    this.setState({ page: 'partnerApiSetupBilling' })
  }

  routePartnerApiAllowAccess() {
    this.setState({ page: 'partnerApiAllowAccess' })
  }

  routePartnerDispatch() {
    setTimeout(() => Dispatcher.send(Context.CONTEXT_CHANGED), 0)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'partnerDispatch',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routePartnerFleet() {
    Dispatcher.send(Context.CONTEXT_CHANGED)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'partnerFleet',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routeSwoopTerritories() {
    Dispatcher.send(Context.CONTEXT_CHANGED)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'swoopTerritories',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routePartnerStorage() {
    Dispatcher.send(Context.CONTEXT_CHANGED)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'partnerStorage',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routeReporting() {
    Dispatcher.send(Context.CONTEXT_CHANGED)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'reporting',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routeReviews() {
    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'reviews',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routeSettings() {
    Logger.debug('routed to login')
    Dispatcher.send(Context.CONTEXT_CHANGED)

    if (UserStore.hasAuthToken()) {
      return this.setState({
        page: 'settings',
      })
    } else {
      return this.setLoginHref()
    }
  }

  routeShowExistingJob(data) {
    let vin = data
    this.setState({ page: 'loading' })

    if (data.indexOf('&') >= 0) {
      vin = data.substring(0, data.indexOf('&'))
    }

    if (UserStore.getBearerToken()) {
      return this.waitForDoneLoading(partial(this.routeAfterFetchingVin, vin))
    } else {
      return UserStore.tokenLogin(Utils.getParamsByName('token'), {
        success: (_data) => {
          _data.preventRouting = true
          Dispatcher.send(BaseConsts.RECEIVED_API_KEY, _data)
          return this.waitForDoneLoading(partial(this.routeAfterFetchingVin, vin))
        },
        error: () => this.setState({ page: 'login' }),
      })
    }
  }

  routeSignup() {
    return this.setState({
      page: 'signup',
    })
  }

  routeTest() {
    return this.setState({
      page: 'test',
    })
  }

  sendTrackingInformation = () => Tracking.page(Context.swoopContext())

  updateRequired() {
    const version = VersionStore.getMostRecentVersion()
    let interval
    if (version?.forced) {
      this.setState({
        update: true,
        keepModal: $('body').hasClass('modal-open'),
        timeLeft: version?.delay ? parseInt(version.delay, 10) : 60,
      })
      interval = setInterval(() => {
        if (this.state.update) {
          this.setState(prevState => ({ timeLeft: prevState.timeLeft - 1 }))

          if (this.state.timeLeft <= 0) {
            clearInterval(interval)
          }
        } else {
          clearInterval(interval)
        }
      }, 1000)
    }
  }

  waitForDoneLoading() {}

  render() {
    if (this.state.rev === 0) {
      return <Loading />
    }

    this.sendTrackingInformation()
    return (
      <>
        <Notifications />
        <Modals />
        {this.state.update &&
        <div style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0,0,0,.5)',
          zIndex: 10001,
          position: 'fixed',
        }}
        />}
        <TransitionModal
          {...this.props}
          callbackKey="refresh"
          cancel={this.state.timeLeft <= 0 ? 'Snooze' : false}
          confirm={this.state.timeLeft <= 0 ? 'Reload' : false}
          hideClose
          onCancel={this.refreshSnooze}
          onConfirm={this.refresh}
          show={this.state.update}
          style={{
            zIndex: 10001,
            textAlign: 'left',
            marginTop: 150,
          }}
          title="Reload Swoop to get the latest updates"
          transitionName="refreshModalTransition"
        >
          <span className="inline-block" style={{ textAlign: 'left' }}>
            {this.state.timeLeft && this.state.timeLeft >= 0 ?
              `Hang tight! We're updating Swoop and will be done in ${
                this.state.timeLeft
              } seconds.` :
              'Please reload Swoop so you can take advantage of all the updates within the latest release.'}
          </span>
        </TransitionModal>
        <RouterProvider router={this.router5}>
          {this.getPage()}
        </RouterProvider>
      </>
    )
  }
}
// TODO: Move this up so that it interacts with the border as well
export const UnwrappedRouter = RouterClass
export default hot(module)(withSplitIO(RouterClass))
