import React from 'react'
import Consts from 'consts'
import createReactClass from 'create-react-class'
import Dispatcher from 'lib/swoop/dispatcher'
import Logger from 'lib/swoop/logger'
import { div } from 'react-dom-factories'
import { withSplitIO } from 'components/split.io'

AccountModal =  React.createFactory(require("components/modal_account_email"))
AccountPickerModal = React.createFactory(require('components/modal_account_picker'))
AddAdditionalSite = React.createFactory(require('components/modals/add_additional_site'))
AddInvoiceAttachments = React.createFactory(require('components/modals/add_invoice_attachments'))
AddJobIssue = React.createFactory(require('components/modals/add_job_issue'))
AddonForm = React.createFactory(require('components/modals/addon_form'))
AuctionStore = require('stores/auction_store').default
ChoosePartnerMapModal = React.createFactory(require "components/modals/choose_partner_map/choose_partner_map")
import ChoosePartnerMapModalNew from "components/modals/choose_partner_map"
CommissionPercentageFormModal = React.createFactory(require('components/modals/commission_percentage_form'))
CompanyFormModal = React.createFactory(require('components/modals/company_form'))
CompanyStore = require('stores/company_store').default
DeleteModal =  React.createFactory(require "components/modals/delete_job")
DepartmentFormModal = React.createFactory(require('components/modals/department_form'))
EditEmailReviewNotifications = React.createFactory(require('components/modals/edit_email_review_notifications'))
EditInvoiceRecipients = React.createFactory(require('components/modals/edit_invoice_recipients'))
EditWaiver = React.createFactory(require('components/modals/edit_waiver'))
EquipmentPickerModal = React.createFactory(require('components/modal_equipment_picker'))
import ErrorModal from 'components/modal_error'
ErrorStore = require('stores/error_store')
FeatureStore = require('stores/feature_store')
InvoiceChangesModal =  React.createFactory(require("components/modal_invoice_changes"))
InvoiceModal = React.createFactory(require('components/modal_invoice'))
InvoiceStore = require('stores/invoice_store')
JobDetailsEmailRecipients = React.createFactory(require('components/modals/job_details_email_recipients'))
JobStore = require('stores/job_store')
ModalStore = require('stores/modal_store')
PartnerFormModal = React.createFactory(require('components/modals/partner_form'))
RateTypePickerModal = React.createFactory(require('components/modal_rate_type_picker'))
RequestPhoneCallBack = React.createFactory(require("components/modals/request_phone_call_back").default)
SearchPartnerModal = React.createFactory(require('components/modals/partner_search'))
ServiceForm = React.createFactory(require('components/modals/service_form'))
ServicePickerModal = React.createFactory(require('components/modal_service_picker'))
SetCustomerType = React.createFactory(require('components/modals/set_customer_type'))
StorageTypeFormModal = React.createFactory(require('components/modals/storage_type_form'))
SymptomForm = React.createFactory(require('components/modals/symptom_form'))
TaxCompanyFormModal = React.createFactory(require('components/tax_company_form'))
TireTypePickerModal = React.createFactory(require('components/modal_tire_type_picker'))
TransitionModal = React.createFactory(require 'components/modals')
UserStore = require('stores/user_store')
VehicleCategoryForm = React.createFactory(require('components/modals/vehicle_category_form'))
VehiclePickerModal = React.createFactory(require('components/modal_vehicle_type_picker'))

OldModal = React.createFactory(require 'components/modals')
import { extend, keys, partial } from 'lodash'

# Note: array sequence matters here; do not sort:
[JobFormModal, AcceptModalNew, AssignModal, RejectModal, UnsetRescueCompanyModal] = (require 'components/job_modals').map (lib) -> React.createFactory(lib)

#BIG TODO: convert this to only rendering one modal at a time, need to figure out transitions

Modals = createReactClass(
  displayName: 'Modals'
  componentDidMount: ->
    ModalStore.bind( ModalStore.CHANGE, @generalChanged )
    JobStore.bind( JobStore.CHANGE, @generalChanged )
    ErrorStore.bind( ErrorStore.CHANGE, @generalChanged )

    # workaround to restore modal-open class when one of multiple open modals is closed
    $(document).on 'hidden.bs.modal', '.modal', () ->
      $('.modal:visible').length and $(document.body).addClass('modal-open')

  componentWillUnmount: ->
    ModalStore.unbind( ModalStore.CHANGE, @generalChanged )
    JobStore.unbind( JobStore.CHANGE, @generalChanged )
    ErrorStore.unbind( ErrorStore.CHANGE, @generalChanged )


  closeModal: ->
    Dispatcher.send(ModalStore.CLOSE_MODALS)

  closeModalAndCallback: ->
    props = ModalStore.getProps()
    @closeModal()
    props?.callback?()

  invoiceChangeSubmit: (invoice, close, updateState=true) ->
    Logger.debug("about to call dispatch", invoice)

    if keys(invoice).length == 2 and invoice.id?
      console.warn("No changes were made to invoice so skipping send")
    else
      Dispatcher.send(InvoiceStore.INVOICE_CHANGED, invoice)

    if not close? || close
      @closeModal()

  requestPhoneCallBack: (job, requestCallBackObj) ->
    JobStore.requestCallbackRequest(job, requestCallBackObj)
    Dispatcher.send(ModalStore.CLOSE_MODALS)

  #TODO: Merge these two callback mechanisms
  jobChangeSubmit: (job, close, saveAsDraft = false) ->
    Logger.debug("about to call dispatch", job)
    props = ModalStore.getProps()

    if props?.callback?
      prev_callback = job?.callbacks?.success
      if !job.callbacks?
        job.callbacks = {}

      job.callbacks.success = (data) =>
        prev_callback?(data)
        props.callback(data)
        #if job.id?
        #  Tracking.trackJobEvent("Edit Job", job.id)
        #else
        #  Tracking.trackJobEvent("Create Job", data.id)

    if !job.id?
      JobStore.jobAdded(job, {save_as_draft: saveAsDraft})
    else
      JobStore.jobChanged(job, false, saveAsDraft)
    if not close? || close
      @closeModal()

  createCompany: (company) ->
    props = ModalStore.getProps()

    if props?.callback?
      prev_callback = company?.callbacks?.success
      if !company.callbacks?
        company.callbacks = {}
      company.callbacks
      company.callbacks.success = (company) =>
        prev_callback?(company)
        props.callback(company)

    CompanyStore.addItem(company)

  handleJobAssignNewJobProvider: (data) ->
    Logger.debug('about to call api job#change_provider_to', data)
    props = ModalStore.getProps()

    if props?.callback?
      job.callback = props.callback

    JobStore.assignNewProvider(data)
    @closeModal()

  choosePartner: (data) ->
    Logger.debug('about to call api job/<id>/choose_partner', data)
    props = ModalStore.getProps()

    if props?.callback?
      job.callback = props.callback

    JobStore.choosePartner(data)
    @closeModal()

  handleReviewEmailChange: (record) ->
    CompanyStore.updateEmailReview(
      id: record.id,
      review_email_recipient_key: record.review_email_recipient_key,
      review_email: record.review_email
    )

    Dispatcher.send(ModalStore.CLOSE_MODALS)

  jobChangeSubmitAndOpen: (newModal, job, close) ->
    props = ModalStore.getProps()
    props.secondModal = true
    @jobChangeSubmit(job, false)
    @closeModal()

  bidToETASubmittedByPartner: (bidToETAData, close) ->
    Dispatcher.send(AuctionStore.BID_TO_ETA_SUBMITTED_BY_PARTNER, bidToETAData)
    @closeModal()

  bidToETARejectedByPartner: (bidToETAData, close) ->
    Dispatcher.send(AuctionStore.BID_TO_ETA_REJECTED_BY_PARTNER, bidToETAData)
    @closeModal()

  generalChanged: ->
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously


  refreshOnError: ->
    location.reload()

  clearError: ->
      Dispatcher.send(ErrorStore.CLEAR_ERRORS, null)
      @closeModal()

  render: ->
    jobModal = JobFormModal
    div null,
      if ErrorStore.getError()?
        ErrorModal(extend({}, @props,
          show: true
          error: ErrorStore.getError()
          cancel: ''
          submitButtonName: 'Refresh'
          onConfirm: @refreshOnError
          onCancel: @clearError))
      switch ModalStore.getModal()
        when "company"
          CompanyFormModal(extend({},
            show: true
            onCancel: @closeModalAndCallback
            onConfirm: @createCompany
            ModalStore.getProps()))
        when "services"
          ServiceForm(extend({},
            show: true
            onCancel: @closeModalAndCallback
            onConfirm: @createCompany
            ModalStore.getProps()))
        when "additional_items"
          AddonForm(extend({},
            show: true
            onCancel: @closeModalAndCallback
            onConfirm: @createCompany
            ModalStore.getProps()))
        when "class_type"
          VehicleCategoryForm(extend({},
            show: true
            onCancel: @closeModalAndCallback
            onConfirm: @createCompany
            hideCustomItems: !UserStore.isPartner()
            ModalStore.getProps()))
        when "symptom"
          SymptomForm(extend({},
            show: true
            onCancel: @closeModalAndCallback
            onConfirm: @createCompany
            ModalStore.getProps()))
        when "accept"
          acceptProps =
            show: true
            onCancel: @closeModal
            onConfirm: if AuctionStore.jobHasLiveAuction(ModalStore.getProps()?.record)
              partial(@bidToETASubmittedByPartner)
            else
              partial(@jobChangeSubmitAndOpen, "assign")
            submitButtonName: 'Next'
            disabled: false
          AcceptModalNew(extend({}, acceptProps, ModalStore.getProps()))
        when "reject"
          RejectModal(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: if AuctionStore.jobHasLiveAuction(ModalStore.getProps()?.record)
              partial(@bidToETARejectedByPartner)
            else
              @jobChangeSubmit
            submitButtonName: 'Reject Job'
            disabled: false, ModalStore.getProps()))
        when "assign"
          if UserStore.isPartner()
            AssignModal(extend({},
              show: true
              onCancel: @closeModal
              onConfirm: @jobChangeSubmit
              submitButtonName: 'Save'
              disabled: false, ModalStore.getProps()))
        when "storage_type_modal"
          if UserStore.isPartner()
            StorageTypeFormModal(extend({},
              show: true
              onCancel: @closeModal, ModalStore.getProps()))


        when "department_modal"
          if FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS) && (UserStore.isPartner() || UserStore.isFleet())
            DepartmentFormModal(extend({},
                show: true
                onCancel: @closeModal, ModalStore.getProps()))

        when "commission_percentage_modal"
          if UserStore.isPartner()
            CommissionPercentageFormModal(extend({},
              show: true
              onCancel: @closeModal, ModalStore.getProps()))

        when "tax_company_modal"
          if UserStore.isPartner()
            TaxCompanyFormModal(extend({},
              show: true
              onCancel: @closeModal, ModalStore.getProps()))
        when "partner"
          if UserStore.isRoot() || UserStore.isSwoopDispatcher() || UserStore.isFleet()
            if not @props.treatments?[Consts.SPLITS.DISABLE_NEW_CHOOSE_PARTNER_MAP]
              <ChoosePartnerMapModalNew
                show
                onCancel={@closeModal}
                onConfirm={@choosePartner}
                disabled={false}
                {...ModalStore.getProps()}
              />
            else
              ChoosePartnerMapModal(extend({},
                show: true
                onCancel: @closeModal
                onConfirm: @choosePartner
                disabled: false, ModalStore.getProps()))
  #
        when "unsetRescueCompany"
          if UserStore.isFleet() || UserStore.isSwoop()
            UnsetRescueCompanyModal(extend({},
              show: true
              onCancel: @closeModal
              onConfirm: @jobChangeSubmit
              disabled: false, ModalStore.getProps()))
  #
        ##TODO: Clean error modal up, make all Stores triggers errors if necessary
        when "delete"
          DeleteModal(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: @jobChangeSubmit, ModalStore.getProps()))
        when "request_phone_call_back"
          RequestPhoneCallBack(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: @requestPhoneCallBack, ModalStore.getProps()))
        when "job_form_edit"
          jobModal(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: @jobChangeSubmit
            submitButtonName: 'Save'
            disabled: false, ModalStore.getProps()))
        when "storage_vehicle_release"
          jobModal(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: @jobChangeSubmit
            submitButtonName: 'Save'
            storage: true
            release: true
            form_type: "Storage"
            service: "Storage"
            disabled: false, ModalStore.getProps()))
        when "storage_vehicle_edit"
          jobModal(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: @jobChangeSubmit
            submitButtonName: 'Save'
            storage: true
            form_type: "Storage"
            service: "Storage"
            disabled: false, ModalStore.getProps()))
        when "storage_vehicle_new"
          jobModal(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: @jobChangeSubmit
            storage: true
            form_type: "Storage"
            service: "Storage"
            submitButtonName: "Create Request"
            disabled: false, ModalStore.getProps()))
        when "job_form_new"
          jobModal(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: @jobChangeSubmit
            submitButtonName: "Create Request"
            disabled: false, ModalStore.getProps()))
        when "modal_account_email"
          AccountModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "invoice_changes"
          InvoiceChangesModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "account_picker_modal"
          AccountPickerModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "service_picker_modal"
          ServicePickerModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "set_customer_type"
          SetCustomerType(extend({},
            show: true
            onConfirm: @jobChangeSubmit
            onCancel: @closeModal
            ModalStore.getProps()))
        when "equipment_picker_modal"
          EquipmentPickerModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "vehicle_type_picker_modal"
          VehiclePickerModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "tire_type_picker_modal"
          TireTypePickerModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "rate_type_picker_modal"
          RateTypePickerModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "simple"
          TransitionModal extend({}, @props,
            show: true
            extraClasses: "simple_modal " + (if  ModalStore.getProps()? and  ModalStore.getProps().extraClasses? then ModalStore.getProps().extraClasses else "")
            onCancel: @closeModal
            disabled: false, ModalStore.getProps()),
            (
              if  ModalStore.getProps()?
                ModalStore.getProps().body
            )
        when "invoice_form_edit"
          InvoiceModal(extend({},
            show: true
            onCancel: @closeModal
            cancel: "Cancel"
            onConfirm: @invoiceChangeSubmit
            submitButtonName: 'Save'
            disabled: false, ModalStore.getProps()))
        when "partner_form"
          PartnerFormModal(extend({},
            show: true
            onCancel: @closeModal,
            ModalStore.getProps()))
        when "edit_invoice_recipients"
          EditInvoiceRecipients(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "job_details_email_recipients"
          JobDetailsEmailRecipients(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "add_job_issue"
          AddJobIssue(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "edit_waiver"
          EditWaiver(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "edit_email_review_notifications"
          EditEmailReviewNotifications(extend({},
            show: true
            onCancel: @closeModal
            onConfirm: @handleReviewEmailChange
            ModalStore.getProps()))
        when "search_partner"
          SearchPartnerModal(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "add_additional_site"
          AddAdditionalSite(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))
        when "add_invoice_attachments"
          AddInvoiceAttachments(extend({},
            show: true
            onCancel: @closeModal
            ModalStore.getProps()))

)

module.exports = withSplitIO(Modals)
