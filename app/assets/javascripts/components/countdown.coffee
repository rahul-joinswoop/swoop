
import Utils from 'utils'
import moment from 'moment-timezone'

import TimeStore from 'stores/time_store'
SettingStore = require('stores/setting_store')
BaseComponent = require('components/base_component')
class Countdown extends BaseComponent
  displayName: 'countdown'

  getColorHash: (key, def_color) =>
    setting = SettingStore.getSettingByKey(key)
    if setting?.value? and setting.value != "transparent"
      if setting.value == "#FDEB8F"
        return color: "#E6B724"
      else
        return color: setting.value
    else
      return color: def_color

  getStyle: (minutes, live) =>
    if minutes <= 0
      return @getColorHash("Past Eta", "#FF7171")
    else if minutes <= 10
      return @getColorHash("Eta Approaching", "#E6B724")
    else if live
      return @getColorHash("En Route", "#000")

    return {}

  render: ->
    super(arguments...)
    TimeStore.watchMinute(@getId())
    span
      className: 'countdown'
      if @props.time
        minutes = Math.ceil(@props.time.diff(moment(), "minutes", true))
        if not isNaN(minutes)

          if minutes <= -60
            hours = Math.floor(Math.abs(Math.floor(minutes))/60)
            span
              className: "error "
              style: @getStyle(minutes, @props.actualEta)
              "-"+hours+" hr "+Math.abs(minutes)%60 + " min"
          else if minutes <= 0
            #if @past and @props.rerender?
            #  @past = false
            #  setTimeout(@props.rerender, 0)
            span
              className: "error "
              style: @getStyle(minutes, @props.actualEta)
              minutes + " min"
          else if minutes <= 10
            #Hack to make parent update row color when the time ticks over
            #@past = true
            #if @close and @props.rerender?
            #  @close = false
            #  setTimeout(@props.rerender, 0)
            span
              className: "warning "
              style: @getStyle(minutes, @props.actualEta)
              minutes + " min"
          else if minutes <= 60
            @close = true
            span
              className: ""
              style: @getStyle(minutes, @props.actualEta)
              minutes + " min"
          else if minutes < 120
            @close = true
            span
              className: ""
              style: @getStyle(minutes, @props.actualEta)
              "1 hr "+ (minutes-60) + " min"
          else if minutes == 120
            @close = true
            span
              className: ""
              style: @getStyle(minutes, @props.actualEta)
              "2 hrs"
          else
            span
              className: ""
              style: @getStyle(minutes, @props.actualEta)
              Utils.formatDateTime(@props.time)



module.exports = Countdown
