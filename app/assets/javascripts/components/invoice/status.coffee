import createReactClass from 'create-react-class'
import React from 'react'
import Consts from 'consts'
ModalStore = require('stores/modal_store')
UserStore = require('stores/user_store')
AccountStore = require('stores/account_store')
JobStore = require('stores/job_store')
InvoiceStore = require('stores/invoice_store')
FeatureStore = require('stores/feature_store')
import Utils from 'utils'
import { partial } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import FontAwesomeButtonImport from 'componentLibrary/FontAwesomeButton'
FontAwesomeButton = React.createFactory(FontAwesomeButtonImport)

import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

DropDownButton = React.createFactory createReactClass(
  close: ->
    setTimeout(=>
      @setState
        open: false
    , 100)
  render: ->
    div
      className: 'dropdown_button'
      ref: "container"
      style:
        display: "inline-block"
        verticalAlign: "middle"
        position: "relative"
        backgroundColor: "#4BBF61"
        borderRadius: 5
        width: "100%"
        color: "white"
        whiteSpace: "nowrap"
      div
        className: "button"
        style:
          width: "calc(100% - 35px)"
          display: "inline-block"
          textAlign: "center"
          cursor: "pointer"
          lineHeight: "21px"
          overflow: "hidden"
          textOverflow: "ellipsis"
          verticalAlign: "middle"
          paddingBottom: "2px"
        onClick: @props.onClick
        @props.text
      div
        style:
          display: "inline-block"
          borderLeft: "1px solid white"
          backgroundColor: if @state?.open then "#30863F"
          borderRadius: "0 3px 3px 0"
          cursor: "pointer"
        onClick: (e) =>
          if !@state?.open
            @props.onDropDownClick(e, @close)

          @setState
            open: !@state?.open

          Utils.ignoreEvents(e)
        ReactDOMFactories.i
          className: "fa fa-chevron-down"
          style:
            color: "white"
            margin: "0 3px"
            border: 0
            padding: "0px 5px"
            fontSize: 18

)

InvoiceRecipients = React.createFactory createReactClass(
  render: ->
    div
      style:
        marginTop: 0
        whiteSpace: "nowrap"
      label
        style:
          display: "inline-block"
          width: 35
          color: "#303030"
        @props.label
      div
        style:
          display: "inline-block"
          width: "calc(100% - 25px)"
          whiteSpace: "normal"
          verticalAlign: "top"
          color: "#404040"
        @props.recipients

)


InvoiceStatus = createReactClass(
  displayName: "InvoiceStatus"

  componentDidMount: ->
    # Force loading the account because it's needed on this component logic
    # on callbacks only (@handleInvoiceSend and @showRecipients) for partners.
    # Also, componentId is not passed because there's not need for rerendeding.
    # We just need to fetch it.
    #
    # TODO maybe there's a more elegant way for doing this?
    if UserStore.isPartner() && @props.invoice?.recipient_type == 'Account'
      AccountStore.get(@props.invoice.recipient.id, 'id')

  handleInvoiceSend: (id, recipient, state, e) ->
    if recipient? and recipient.recipient_type == "Account"
      # no need to pass componentId because this is a callback function
      accounting_email = AccountStore.get(recipient.id, 'accounting_email')

      if not accounting_email or accounting_email.length == 0
        Dispatcher.send(ModalStore.SHOW_MODAL,
          key: "modal_account_email",
          props: {
            account: recipient
            onConfirm: (account) =>
              AccountStore._update(account)
              Dispatcher.send(ModalStore.CLOSE_MODALS)
          }
        )
        e?.preventDefault()
        e?.stopPropagation()
        return
      else if AccountStore.get(recipient.id, 'po_required_before_sending_invoice')
          job = @props.invoice?.job
          if !job?.po_number? || job?.po_number == ""
            Dispatcher.send(ModalStore.SHOW_MODAL,
              key: "simple",
              props: {
                title: Utils.getJobId(@props.invoice?.job)+": PO Required"
                confirm: 'OK'
                cancel: null
                body: "Please add a PO number to the job before sending this invoice."
                onConfirm: (account) =>
                  Dispatcher.send(ModalStore.CLOSE_MODALS)
              }
            )
            e?.preventDefault()
            e?.stopPropagation()
            return

    if UserStore.isPartner() and @props.invoice?.job?.id and JobStore.getAttachedDocuments(@props.invoice?.job?.id, Consts.TYPE_ATTACHED_DOCUMENT).length > 0
      Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "add_invoice_attachments",
        props: {
          job_id: @props.invoice?.job?.id
          onConfirm: () =>
            if @props.handleInvoiceChange
              @props.handleInvoiceChange(id, state)
        }
      )
      e?.preventDefault()
      e?.stopPropagation()
      return

    if @props.handleInvoiceChange
      @props.handleInvoiceChange(id, state, e)

  editInvoiceRecipients: (invoice, e) ->
    @setState
      open: false
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "edit_invoice_recipients"
      props:
        invoice: invoice
    )

  showRecipients: (invoice, e, close) ->
    # todo - !sure e is durable b/c react synthetic event can persist it or assign it in calling func
    buttonWidth = e.target.getBoundingClientRect().width
    popupWidth  = 270
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "options",
      props:
        target: e.target
        offsetTop: 25
        offsetLeft: -(popupWidth - buttonWidth) + 2 # todo: 2 is a magic number I think from border css
        id: invoice.id
        onUnfocus: close
        getView: =>
          email_to = ""
          email_bcc = ""
          email_cc = ""
          if invoice.recipient_type == "Account"
            # no need to pass componentId because this is a callback function
            email_to = AccountStore.get(invoice.recipient.id, 'accounting_email')
            email_cc = AccountStore.get(invoice.recipient.id, 'accounting_email_cc')
            email_bcc = AccountStore.get(invoice.recipient.id, 'accounting_email_bcc')

          if invoice.email_to || invoice.email_bcc || invoice.email_cc
            email_to = invoice.email_to
            email_cc = invoice.email_cc
            email_bcc = invoice.email_bcc

          div
            style:
              padding: 10
              width: popupWidth
              overflow: "hidden"
            span
              style:
                textTransform: "uppercase"
                color: "#818181"
                fontSize: 12
                marginBottom: "10"
                display: "block"
              "Invoice will be sent to"
            if email_to? and email_to.length > 0
              InvoiceRecipients
                label: "To:"
                recipients: email_to
                invoice: invoice
            if email_cc? and email_cc.length > 0
              InvoiceRecipients
                label: "Cc:"
                recipients: email_cc
                invoice: invoice
            if email_bcc? and email_bcc.length > 0
              InvoiceRecipients
                label: "Bcc:"
                recipients: email_bcc
                invoice: invoice
            if invoice.recipient_type == "Account" and !email_to? and !email_bcc? and !email_cc?
                span null,
                  "Add in Account Settings"
            else if !Utils.isAssignedFromFleet(JobStore.getJobById(invoice.job_id))
              FontAwesomeButton
                color: "secondary"
                icon: "fa-pencil"
                onClick: partial(@editInvoiceRecipients, invoice)
                size: "medium"
                style: {
                  marginLeft: 35,
                }
                "Edit Recipients"
    )
  render: () ->
    invoice = @props.invoice
    job = invoice.job
    if InvoiceStore.isUpdatingInvoice(invoice.id)?.state? || InvoiceStore.isDownloadingInvoice(invoice.id) || invoice.state == Consts.ISWOOPSENDINGFLEET
      return Loading
        padding: 0
        size: 22
        onClick: Utils.preventPropogation
    else if InvoiceStore.isUpdatingInvoice(invoice.id)?.paid?
      return Loading
        padding: 0
        size: 22
        onClick: Utils.preventPropogation

    stateMap = {}
    stateMap[Consts.ICREATED] = {name: "Approve", nextState: Consts.IPARTNERAPPROVED}
    stateMap[Consts.IPARTNERNEW] = {name: "Approve", nextState: Consts.IPARTNERAPPROVED}
    stateMap[Consts.IFLEETREJECTED] = {name: "Reapprove", nextState: Consts.IPARTNERAPPROVED}
    stateMap[Consts.ISWOOPREJECTEDPARTNER] = {name: "Reapprove", nextState: Consts.IPARTNERAPPROVED}
    stateMap[Consts.IPARTNERAPPROVED] = {name: "Send", getView: (obj, invoice, onClick) =>
      if !Utils.isAssignedFromFleet(JobStore.getJobById(invoice.job_id))
        DropDownButton
          onClick: onClick
          text: "Send"
          onDropDownClick: (e, close) =>
            @showRecipients(@props.invoice, e, close)


    ,onClick: partial(@handleInvoiceSend, invoice.id, invoice.recipient, Consts.IPARTNERSENT), nextState: Consts.IPARTNERSENT}
    stateMap[Consts.IONSITEPAYMENT] = {name: "Paid On Site"}
    stateMap[Consts.IPARTNEROFFLINENEW] = {name: "Sent"}

    if UserStore.isPartner()
      remainingStates = [
        Consts.IFLEETDOWNLOADED, Consts.ISWOOPAPPROVED,
        Consts.ISWOOPDOWNLOADEDPARTNER
      ]

      # TODO this should be able to be just `if job&.invoice_restrictions`, but I'm not sure of side effects. (BD 2018-06-15)
      if Utils.isAssignedFromFleet(job) && !JobStore.isMotorClub(job) && job?.invoice_restrictions != false
        stateMap[Consts.IPARTNERSENT] = { name: "Account Received" }
        stateMap[Consts.IFLEETAPPROVED] = { name: "Account Approved" }
        stateMap[Consts.ISWOOPAPPROVEDPARTNER] = { name: "Account Approved" }
      else
        remainingStates.push(
          Consts.IPARTNERSENT, Consts.IFLEETAPPROVED,
          Consts.ISWOOPAPPROVEDPARTNER
        )

      for state in remainingStates
        stateMap[state] = { name: "Sent" }

    if UserStore.isFleet()
      stateMap[Consts.ICREATED] = {name: "Pending"}
      stateMap[Consts.IPARTNERNEW] = {name: "Pending"}
      stateMap[Consts.IPARTNERAPPROVED] = {name: "Pending"}
      stateMap[Consts.IPARTNERSENT] = {name: "Approve", nextState: Consts.IFLEETAPPROVED}
      stateMap[Consts.IFLEETAPPROVED] = {name: "Approved"}
      stateMap[Consts.IFLEETDOWNLOADED] = {name: "Downloaded"}

    if UserStore.isRoot()
      if job? and invoice.job.rescue_company != null
        stateMap[Consts.ICREATED] = {name: "Pending"}
        stateMap[Consts.IPARTNERNEW] = {name: "Pending"}
        stateMap[Consts.IPARTNERAPPROVED] = {name: "Pending"}
        stateMap[Consts.IPARTNERSENT] = {name: "Approve", nextState: Consts.ISWOOPAPPROVEDPARTNER}
        stateMap[Consts.IPARTNEROFFLINENEW] = {name: "Set Price"}

        if invoice.cost != 0
          stateMap[Consts.IPARTNEROFFLINENEW].name = "Approve"
          stateMap[Consts.IPARTNEROFFLINENEW].nextState = Consts.ISWOOPAPPROVEDPARTNER

      else
        stateMap[Consts.ICREATED] = {name: "Set Provider"}
        stateMap[Consts.IPARTNERNEW] = {name: "Set Provider"}
        stateMap[Consts.IPARTNERAPPROVED] = {name: "Set Provider"}
        stateMap[Consts.IPARTNERSENT] = {name: "Set Provider"}
        stateMap[Consts.IPARTNEROFFLINENEW] = {name: "Set Provider"}

      stateMap[Consts.ISWOOPNEW] = {name: "Approve", nextState: Consts.ISWOOPAPPROVEDFLEET}


      if invoice?.recipient?.company?.name == Consts.FLEET_RESPONSE
        stateMap[Consts.ISWOOPAPPROVEDFLEET] = {name: "Send", nextState: Consts.ISWOOPSENTFLEET}
      else
        stateMap[Consts.ISWOOPAPPROVEDFLEET] = {name: "Approved"}


      stateMap[Consts.ISWOOPSENDINGFLEET] = {name: "Loading"}
      stateMap[Consts.ISWOOPSENTFLEET] = {name: "Sent"}

      stateMap[Consts.ISWOOPAPPROVEDPARTNER] = {name: "Approved"}
      stateMap[Consts.ISWOOPDOWNLOADEDPARTNER] = {name: "Paid"}


    if stateMap[invoice.state]?
      if UserStore.isPartner() || UserStore.isSwoop()
        if invoice.paid
          if invoice.state != Consts.IONSITEPAYMENT
            return span
              className: "invoice_state paid tab_"+@props.tab
              "Paid"
        else if stateMap[invoice.state].name == "Sent" and @props.tab != "new"
          return Button
            color: "green"
            onClick: partial(@props.handleInvoiceChange, invoice.id, {paid: true})
            size: "full-width narrow"
            "Mark Paid"
      if stateMap[invoice.state].nextState? and job?
        onClick = partial(@props.handleInvoiceChange, invoice.id, stateMap[invoice.state].nextState)
        if stateMap[invoice.state].onClick?
          onClick = stateMap[invoice.state].onClick

        if stateMap[invoice.state].getView?
          ret = stateMap[invoice.state].getView(stateMap[invoice.state], invoice, onClick)
          if ret
            return ret
        return Button
          color: if stateMap[invoice.state].name == "Send" then "green" else "primary"
          onClick: (event) =>
            event.stopPropagation()
            if stateMap[invoice.state].name  == 'Approve'
              Tracking.track('Approve Invoice', {
                category: 'Invoices'
              })
            onClick()
          size: "full-width narrow"
          stateMap[invoice.state].name
      else
        return span
          className: "invoice_state " + stateMap[invoice.state].name.toLowerCase() +  (if invoice.paid then "paid" else "" ) + " tab_"+@props.tab + " feature_plane_" + FeatureStore.isFeatureEnabled("Plane Animation")
          stateMap[invoice.state].name

    return null
)

module.exports = InvoiceStatus
