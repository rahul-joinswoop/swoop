import React from 'react'
import PropTypes from 'prop-types'
import BaseConsts from 'base_consts'
import InvoiceRejectionStore from 'stores/invoice_rejection_store'
import Dispatcher from 'lib/swoop/dispatcher'
import { assign } from 'lodash'

const propTypes = {
  invoice_id: PropTypes.number.isRequired,
  onSuccess: PropTypes.func.isRequired,
}

// A React component that shows a form with the necessary fields to complete for
// rejecting an invoice. It is rendered using Bootstrap markup.
//
// It exposes public methods to validate `isValid()` and submit `submit()`the
// form.
class InvoiceRejectionForm extends React.Component {
  constructor(props) {
    super(props)

    InvoiceRejectionStore.getReasons()

    this.state = {
      rejectReasons: [],
      invoiceRejection: {
        invoice_id: props.invoice_id,
      },
    }
  }

  componentDidMount() {
    InvoiceRejectionStore.bind(InvoiceRejectionStore.REASONS_LOADED, this.reasonsLoaded)
    InvoiceRejectionStore.bind(InvoiceRejectionStore.REJECTED_INVOICE, this.invoiceRejected)
  }

  componentWillUnmount() {
    InvoiceRejectionStore.unbind(InvoiceRejectionStore.REASONS_LOADED, this.reasonsLoaded)
    InvoiceRejectionStore.unbind(InvoiceRejectionStore.REJECTED_INVOICE, this.invoiceRejected)
  }

  handleChange = (event) => {
    const { target } = event
    const invoiceRejection = assign(this.state.invoiceRejection, { [target.name]: target.value })

    this.setState({ invoiceRejection })
  }

  invoiceRejected() {
    this.props.onSuccess()
  }

  isValid = () => !!this.state.invoiceRejection.invoice_reject_reason_id

  reasonsLoaded = (reasons) => {
    this.setState({ rejectReasons: reasons })
  }

  showRequiredFields = () => {
    const selectRequired = !this.isValid()
    this.setState({
      showSelectRequired: selectRequired,
    })
  }

  submit = () => {
    Dispatcher.send(InvoiceRejectionStore.REJECT_INVOICE, this.state.invoiceRejection)
  }

  render() {
    const showSelectError = this.state.showSelectRequired

    function reasonSelectOption(reason) {
      return (
        <option key={`option-${reason.id}`} value={reason.id}>
          {reason.text}
        </option>
      )
    }

    return (
      <form className="invoice-rejection-form" id="invoice-rejection-form">
        <p>
          {BaseConsts.INVOICE_REJECTION.CONFIRMATION}
        </p>

        <div className={`form-group ${showSelectError ? 'has-error' : ''}`}>
          <label htmlFor="invoice-rejection-form-reject-reason">
            Reject Reason
          </label>
          <select
            className="form-control invoice-rejection-form-select"
            id="invoice-rejection-form-reject-reason"
            name="invoice_reject_reason_id"
            onChange={this.handleChange}
            placeholder="Select"
          >
            <option key="blankOption" value="">
Select
            </option>
            {this.state.rejectReasons.map(reasonSelectOption)}
          </select>
          {showSelectError &&
            (
              <span className="help-block">
Reject reason is required.
              </span>
            )}
        </div>

        <div className="form-group">
          <label htmlFor="invoice-rejection-form-notes">
Notes
          </label>
          <textarea
            className="form-control"
            id="invoice-rejection-form-notes"
            name="notes"
            onChange={this.handleChange}
            placeholder="Optional"
            rows="3"
          />
        </div>
      </form>
    )
  }
}

InvoiceRejectionForm.propTypes = propTypes

export default InvoiceRejectionForm
