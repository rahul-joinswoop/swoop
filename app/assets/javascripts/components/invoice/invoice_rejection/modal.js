import React from 'react'
import Utils from 'utils'
import Dispatcher from 'lib/swoop/dispatcher'
import InvoiceRejectionForm from 'components/invoice/invoice_rejection/form'
import ModalStore from 'stores/modal_store'

function InvoiceRejectionModal(invoice, callback) {
  let invoiceRejectionForm

  function onSuccessfulRejection() {
    Dispatcher.send(ModalStore.CLOSE_MODALS)
    Utils.sendSuccessNotification(`Invoice #${invoice.job_id} has been rejected!`)
    callback()
  }

  const modalProps = {
    title: 'Reject Invoice',
    confirm: 'Reject',
    cancel: 'Cancel',
    body:
  <InvoiceRejectionForm
    invoice_id={invoice.id}
    onSuccess={onSuccessfulRejection}
    ref={(form) => { invoiceRejectionForm = form }}
  />,
    onConfirm: () => {
      invoiceRejectionForm.showRequiredFields()

      if (invoiceRejectionForm.isValid()) {
        invoiceRejectionForm.submit()
        return
      }

      Utils.sendErrorNotification('Please complete required fields!', { timeout: 2000 })
    },
  }

  Dispatcher.send(ModalStore.SHOW_MODAL, { key: 'simple', props: modalProps })
}

export default InvoiceRejectionModal
