import React from 'react'
import InfoBubble from 'components/info_bubble/info_bubble'

// Component that shows an InfoBubble with the reason for why an invoice was
// rejected.
//
// It requires an invoice_id property as it fetches an InvoiceRejection object
// from the server.
class InvoiceRejectionInfoIcon extends React.Component {
  render() {
    const { invoiceRejection } = this.props
    let companyThatRejectedTheInvoice = 'The company'

    if (this.props.invoice.recipient && this.props.invoice.recipient.company && this.props.invoice.recipient.company.name != null) {
      companyThatRejectedTheInvoice = this.props.invoice.recipient.company.name
    }

    if (!invoiceRejection || !invoiceRejection.reject_reason) {
      return null
    }

    const title = `${companyThatRejectedTheInvoice} Rejected Invoice`

    let content = `<b>Reason:</b> ${invoiceRejection.reject_reason}`

    if (invoiceRejection.notes) {
      content += `<br><b>Notes:</b> ${invoiceRejection.notes}`
    }

    return (
      <InfoBubble
        content={content}
        icon="fa-times"
        status="error"
        title={title}
      />
    )
  }
}

export default InvoiceRejectionInfoIcon
