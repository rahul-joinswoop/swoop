import Api from 'api'
import createReactClass from 'create-react-class'
import React from 'react'
import Consts from 'consts'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'
import { withModalContext } from 'componentLibrary/Modal'
import { getCurrencyClass } from 'lib/localize'

ModalStore = require('stores/modal_store')
JobStore = require('stores/job_store.coffee')
UserStore = require('stores/user_store')
InvoiceRejectionStore = require('stores/invoice_rejection_store')
DownloadStore = require('stores/download_store')
InvoiceStore = require('stores/invoice_store')
ServiceAliasStore = require('stores/service_alias_store')
import Utils from 'utils'
import { shouldShowChargeCard } from 'DataUtils'
InvoiceStatus = React.createFactory(require('components/invoice/status'))
FTable = React.createFactory((require 'components/tables/SortTable').Table)
{Column, Row} = (require 'components/tables/SortTable')

import JobDetailsImport from 'components/job_details'
JobDetails = React.createFactory(JobDetailsImport.default)

AccountStore = require('stores/account_store')
DepartmentStore = require('stores/department_store')
UserSettingStore = require('stores/user_setting_store')
FeatureStore = require('stores/feature_store')
InvoiceRejectionInfoIcon = React.createFactory(
  require('components/invoice/invoice_rejection/info_icon').default
)
import InvoiceRejectionModal from 'components/invoice/invoice_rejection/modal'
InfoBubble = React.createFactory(
  require('components/info_bubble/info_bubble').default
)
BaseComponent = require('components/base_component')
CompanyStore = require('stores/company_store').default
DownloadButtonAsync = React.createFactory(require('components/download_button_async'))
ChargeCardModal = React.createFactory(require("components/modals/CreditCard/ChargeCardModal").default)

import { cloneDeep, defer, extend, filter, intersection, isEqual, isFunction, map, partial } from 'lodash'

import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

EditInvoiceButton = React.createFactory(createReactClass(
  componentDidMount: ->

  displayName: "InvoiceEditButton"
  partnerEditWarn: (invoice, e) ->
    #if UserStore.isPartner() and invoice.state in [Consts.IPARTNERSENT]
    #  Dispatcher.send(ModalStore.SHOW_MODAL,
    #    key: "simple",
    #    props: {
    #      title: "Warning"
    #      confirm: 'OK'
    #      body: "Editing a sent invoice will move it to the New tab. The account will not receive the updated invoice until you send it again."
    #      onConfirm: =>
    #        Utils.showModal('invoice_form_edit', e, record: invoice.id)
    #    }
    #  )
    #else
    Utils.showModal('invoice_form_edit', e, record: invoice.id)

    if e?
      e.preventDefault()
      e.stopPropagation()
  render: ->
    invoice = @props.invoice

    if @props.disabled
      ReactDOMFactories.i
        className: 'fa fa-pencil edit inline invisible'
        onClick: partial(@partnerEditWarn, invoice)
        name: 'edit'
    else
      ReactDOMFactories.i
        className: 'fa fa-pencil edit inline'
        onClick: partial(@partnerEditWarn, invoice)
        name: 'edit'

))

class InvoiceList extends BaseComponent
  displayName: 'InvoiceList'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()

    @register(InvoiceStore, InvoiceStore.SEARCH_FINISHED)
    @register(UserSettingStore, UserSettingStore.CHANGE)

  expandedJobs: {}
  rowCallbacks: {}

  undoApprovalEventFor: (invoice_state) =>
    return @undo_approval_state[invoice_state] if @undo_approval_state

    undo_approval_state = {}
    undo_approval_state[Consts.IPARTNERAPPROVED]      = Consts.IPARTNERUNDOAPPROVAL
    undo_approval_state[Consts.IFLEETAPPROVED]        = Consts.IFLEETUNDOAPPROVAL
    undo_approval_state[Consts.ISWOOPAPPROVEDPARTNER] = Consts.ISWOOPUNDOAPPROVALPARTNER
    undo_approval_state[Consts.ISWOOPAPPROVEDFLEET]   = Consts.ISWOOPUNDOAPPROVALFLEET

    @undo_approval_state = undo_approval_state
    @undo_approval_state[invoice_state]

  componentDidMount: ->
    super(arguments...)

    DownloadStore.bind( DownloadStore.DOWNLOADING_REPORT,  @clearAndDownload )
    InvoiceStore.bind( InvoiceStore.SHOULD_RELOAD, @redownload )

    @searchInvoices(@props.types, @props.filters, @props.search)
    @rerender()
    $(document).on("scroll", @scrollHandler)

  componentWillUnmount: ->
    super(arguments...)

    DownloadStore.unbind( DownloadStore.DOWNLOADING_REPORT,  @clearAndDownload )
    InvoiceStore.unbind( InvoiceStore.SHOULD_RELOAD, @redownload )

    $(document).off("scroll", @scrollHandler)

  scrollHandler: (tab) =>
    if $(document).scrollTop() + $(document).height() > $('.wrapper').prop('scrollHeight') - 100
      @searchInvoices(@props.types, @props.filters, @props.search)

  getStateFromProps: (props) ->
    account_id: props.account_id
    partner_id: props.partner_id
    searchKey: props.searchKey
    searchOrder: props.searchOrder

  emailInvoice: (invoice) ->
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "edit_invoice_recipients"
      props:
        invoice: invoice
        onlySend: true
    )

  sendInvoiceAgain: (invoice) ->
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "edit_invoice_recipients"
      props:
        invoice: invoice
    )

  moveBackToNewModalTextFor: (invoice) =>
    if @moveBackToNewModalSettings && isFunction(@moveBackToNewModalSettings[invoice.state])
      return @moveBackToNewModalSettings[invoice.state](invoice)

    settings = {}
    settings[Consts.IPARTNERSENT] = (invoice) => @moveBackToNewWhenReceived(invoice)
    settings[Consts.ISWOOPAPPROVEDPARTNER] = (invoice) => @moveBackToNewWhenApproved(invoice)
    settings[Consts.IFLEETAPPROVED] = (invoice) => @moveBackToNewWhenApproved(invoice)
    settings[Consts.IFLEETDOWNLOADED] = (invoice) => @moveBackToNewWhenDownloaded(invoice)
    settings[Consts.ISWOOPDOWNLOADEDPARTNER] = (invoice) => @moveBackToNewWhenDownloaded(invoice)
    settings[Consts.IONSITEPAYMENT] = (invoice) => @moveBackToNewWhenOnSitePayment(invoice)
    settings[Consts.ISWOOPSENTFLEET] = (invoice) => @moveBackToNewWhenDownloaded(invoice)
    settings[Consts.ISWOOPSENTFLEET] = (invoice) => @genericMoveBackToNewMessage(invoice)

    @moveBackToNewModalSettings = settings
    if isFunction(@moveBackToNewModalSettings[invoice.state])
      @moveBackToNewModalSettings[invoice.state](invoice)

  genericMoveBackToNewMessage: (invoice) ->
    #HACK: Bad CSS beacuse br tags are used everywhere
    #TODO: remove br tags and do proper styling throughout
    span
      style:
        marginBottom: -30
        position: 'relative'
        top: -15
      "Are you sure you want to move it back to the New tab?"

  moveBackToNewWhenReceived: (invoice) ->
    span null,
      Consts.INVOICE.FLEET_OR_SWOOP_HAS_ALREADY_RECEIVED(invoice.recipient.name)
      br null
      br null
      Consts.INVOICE.REMOVE_FROM_APPROVAL_QUEUE

  moveBackToNewWhenApproved: (invoice) ->
    span null,
      Consts.INVOICE.FLEET_OR_SWOOP_HAS_ALREADY_APPROVED(invoice.recipient.name)
      br null
      br null
      Consts.INVOICE.CONTACT_FLEET_OR_SWOOP(invoice.recipient.name)

  moveBackToNewWhenDownloaded: (invoice) ->
    span null,
      Consts.INVOICE.FLEET_OR_SWOOP_HAS_ALREADY_PAID(invoice.recipient.name)
      br null
      br null
      Consts.INVOICE.CONTACT_FLEET_OR_SWOOP(invoice.recipient.name)

  moveBackToNewWhenOnSitePayment: (invoice) ->
    span null,
      Consts.INVOICE.ARE_YOU_SURE_TO_MARK_UNPAID
      br null
      br null
      Consts.INVOICE.IT_CAN_BE_EDITED_AGAIN

  showMoveBackToNew: (invoice) =>
    ((UserStore.isPartner() && @props.tab == Consts.TAB.SENT) ||
    (UserStore.isSwoop() && invoice.state == Consts.ISWOOPSENTFLEET) ||
    (UserStore.isPartner() && @props.tab == Consts.TAB.PAID && invoice.state != Consts.IPARTNERAPPROVED))

  showRejectInvoice: (invoice) ->
    (UserStore.isFleetInHouse() || UserStore.isSwoop()) &&
      invoice.state == Consts.IPARTNERSENT

  showInvoiceRejectionInfo: (invoice) ->
    invoice.invoice_rejection? &&
    invoice.state in [
      Consts.IFLEETREJECTED, Consts.ISWOOPREJECTEDPARTNER, Consts.IPARTNERSENT,
      Consts.IPARTNERAPPROVED
    ]

  showInvoiceWarning: (invoice) =>
    if (UserStore.isFleet() or UserStore.isRoot()) and invoice.state in [
      Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED]
      return false

    if UserStore.isPartner() and invoice.job?.account?.id and not AccountStore.isTeslaAccount(invoice.job.account.id, @getId())
      return false

    if (not UserStore.isFleet() and invoice.partner_alert) or (
      UserStore.isFleet() and invoice.fleet_alert)
      return true


  showSimpleModal: (title, confirmText, cancelText, body, onConfirm) ->
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: 'simple',
      props:
        title: title
        confirm: confirmText
        cancel: cancelText
        body: body
        onConfirm: onConfirm
    )


  markUnpaid: (invoice) =>
    @showSimpleModal("mark Unpaid", "Mark Unpaid", "Cancel", "Are you sure you'd like to mark unpaid?", =>
      @handleInvoiceChange(invoice.id, {paid: false})
      Dispatcher.send(ModalStore.CLOSE_MODALS)
    )

  moveBackToNew: (invoice) =>
    title = 'Move Back to New'
    if invoice.state in [Consts.ISWOOPAPPROVEDPARTNER, Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED, Consts.ISWOOPDOWNLOADEDPARTNER]
      title ='Cannot Move Invoice Back'
    confirm = 'Move Back'
    if invoice.state in [Consts.ISWOOPAPPROVEDPARTNER, Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED, Consts.ISWOOPDOWNLOADEDPARTNER]
      confirm = 'OK'
    cancel = 'Cancel'
    if invoice.state in [Consts.ISWOOPAPPROVEDPARTNER, Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED, Consts.ISWOOPDOWNLOADEDPARTNER]
      cancel = ''

    nextState = null
    if invoice.state in [Consts.ISWOOPSENTFLEET]
      nextState = Consts.ISWOOPNEW
    else if invoice.state not in [Consts.ISWOOPAPPROVEDPARTNER, Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED, Consts.ISWOOPDOWNLOADEDPARTNER]
      nextState = Consts.IPARTNERMOVEBACKTONEW

    #TODO: This is super ugly, we should be using styles here
    body = div null,
      br null
        if invoice.job.type == Consts.JOB.TYPE.RESCUE && invoice.state != Consts.IONSITEPAYMENT
          span null,
            Consts.INVOICE.ALREADY_BEEN_SENT
            br null
            br null
            Consts.INVOICE.CAN_BE_EDITED_AGAIN
        else
          @moveBackToNewModalTextFor(invoice)

    @showSimpleModal(title, confirm, cancel, body, =>
      if nextState
        Dispatcher.send(InvoiceStore.INVOICE_CHANGED,
          id: invoice.id
          state: nextState
        )
      Dispatcher.send(ModalStore.CLOSE_MODALS)
    )

  markJobPaid: (invoice) ->
    Dispatcher.send(InvoiceStore.INVOICE_CHANGED,
      {
        id: invoice.id
        paid: true
      }
    )
    Dispatcher.send(ModalStore.CLOSE_MODALS)

  markSent: (invoice) ->
    Dispatcher.send(InvoiceStore.INVOICE_CHANGED,
      {
        id: invoice.id
        state: Consts.IPARTNERMARKSENT
      }
    )
    Dispatcher.send(ModalStore.CLOSE_MODALS)

  showMoveBackFromDownloaded: (invoice) ->
    UserStore.isFleet() && (invoice.state == Consts.IFLEETDOWNLOADED || invoice.state == Consts.IONSITEPAYMENT)

  showMarkSent: (invoice) ->
    UserStore.isPartner() && invoice.state == Consts.IPARTNERAPPROVED

  moveBackFromDownloaded: (invoice) ->
    props = undefined

    if invoice.state == Consts.IFLEETDOWNLOADED
      props = {
        title: 'Move Invoice Back'
        confirm: 'Move Back'
        cancel: 'Cancel'
        body:
          div null,
            br null
            Consts.INVOICE.ALREADY_BEEN_DOWNLOADED
        onConfirm: ->
          Dispatcher.send(InvoiceStore.INVOICE_CHANGED,
            {
              id: invoice.id
              state: Consts.IFLEETMOVEBACKFROMDOWNLOADED
            }
          )
          Dispatcher.send(ModalStore.CLOSE_MODALS)
      }
    else if invoice.state == Consts.IONSITEPAYMENT
      props = {
        title: 'Cannot Move Invoice Back'
        confirm: 'OK'
        cancel: ''
        body:
          div null,
            br null
            Consts.INVOICE.WAS_PAID_ON_SITE
        onConfirm: ->
          Dispatcher.send(ModalStore.CLOSE_MODALS)
      }

    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: 'simple',
      props: props
    )

  showMarkJobPaid: (invoice) ->
    UserStore.isPartner() &&
    invoice.state == Consts.IPARTNERAPPROVED &&
    (JobStore.isPartner(invoice.job) || JobStore.isMotorClub(invoice.job)) &&
    (not invoice.paid? || invoice.paid == false)

  showMarkUnpaid: (invoice) ->
    invoice.paid &&
      (
        (UserStore.isPartner() && !(invoice.state in [Consts.ISWOOPDOWNLOADEDPARTNER, Consts.IONSITEPAYMENT])) ||
        (UserStore.isSwoop() && invoice.state in [Consts.ISWOOPNEW, Consts.ISWOOPSENTFLEET, Consts.ISWOOPAPPROVEDFLEET])
      )
  rejectInvoice: (invoice) ->
    InvoiceRejectionModal(invoice, @clearAndDownload)

  generatePdf: (invoice, responseContentDisposition) ->
    invoiceOptions =
      invoice: invoice
      responseContentDisposition: responseContentDisposition

    Dispatcher.send(InvoiceStore.GENERATE_PDF, invoiceOptions)
    Dispatcher.send(ModalStore.CLOSE_MODALS)

  showChargeCard: (invoice_id) =>
    this.props.showModal(
      ChargeCardModal
        invoice_id: invoice_id
    )

  toggleOptions: (job, invoice, e) =>
    if job?
      Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "options",
        props:
          target: e.target
          offsetTop: 25
          offsetLeft: -60
          id: job.id
          onUnfocus: @rerender
          options: [
            if invoice?
              li
                key: 'view_invoice'
                onClick: partial(Utils.showInvoice, invoice)
                'View Invoice'
            if invoice?
              li
                key: 'download_invoice'
                onClick: partial(@generatePdf, invoice, 'attachment')
                'Download Invoice'
            if @showEmailInvoice(invoice)
              li
                key: 'send_invoice'
                onClick: partial(@emailInvoice, invoice)
                'Email Invoice'
            if shouldShowChargeCard(invoice.id, @getId())
              li
                key: 'charge_card'
                onClick: partial(@showChargeCard, invoice.id)
                'Charge Card'
            if (Utils.isCashCall(job) and invoice?.state == Consts.IONSITEPAYMENT ) || (invoice? and invoice?.state in Consts.PARTNER_INVOICE_SENT_STATES and !Utils.isAssignedFromFleet(JobStore.getJobById(invoice.job_id)))
              li
                key: 'send_invoice_again'
                onClick: partial(@sendInvoiceAgain, invoice)
                'Send Invoice Again'
            if @showUndoApproval(invoice)
              li
                key: 'undo_approval'
                onClick: =>
                  @undoApproval(invoice)
                'Undo Approval'
            if @showMarkJobPaid(invoice)
              li
                key: 'mark_partner_job_paid'
                onClick: partial(@markJobPaid, invoice)
                'Mark Paid'
            if @showMoveBackFromDownloaded(invoice)
              li
                key: 'move_back_from_downloaded'
                onClick: partial(@moveBackFromDownloaded, invoice)
                'Move Back to Approved'
            if @showMarkSent(invoice)
              li
                key: 'mark_sent'
                onClick: partial(@markSent, invoice)
                'Mark Sent'
            if @showMarkUnpaid(invoice)
              li
                key: 'mark_unpaid'
                onClick: partial(@markUnpaid, invoice)
                'Mark Unpaid'
            if @showMoveBackToNew(invoice)
              li
                key: 'move_back_to_new'
                onClick: partial(@moveBackToNew, invoice)
                'Move Back to New'
            if @showRejectInvoice(invoice)
              li
                key: 'reject_invoice'
                onClick: partial(@rejectInvoice, invoice)
                'Reject Invoice'
          ]
        )
    e.preventDefault()
    e.stopPropagation()

  clearAndDownload: =>
    InvoiceStore.clearSearchInvoices()
    @redownload()
    @rerender()

  isDisabled: (invoice) =>
    if UserStore.isFleetInHouse()
      return true
    disabled = false
    job = invoice.job
    if UserStore.isSwoop() && invoice.state in [Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT, Consts.IONSITEPAYMENT, Consts.ISWOOPAPPROVEDPARTNER, Consts.ISWOOPDOWNLOADEDPARTNER]
      if not job?
        return true
      else if UserStore.isPartner() # it looks like this case is never touched; TODO check if this is correct
        if invoice.state not in [
          Consts.IONSITEPAYMENT, Consts.ICREATED, Consts.IPARTNERNEW,
          Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT, Consts.IFLEETREJECTED,
          Consts.ISWOOPREJECTEDPARTNER
        ]
          return true
        if invoice.state in [Consts.IONSITEPAYMENT] and invoice.job.status in [Consts.COMPLETED, Consts.CANCELED, Consts.GOA]
          return true
      else if UserStore.isSwoop()
        # it looks like this case is never touched if invoice.state == (Consts.ISWOOPSENDINGFLEET or Consts.ISWOOPSENTFLEET); TODO check if this is correct
        if invoice.state in [Consts.ICREATED, Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT, Consts.IONSITEPAYMENT, Consts.ISWOOPSENDINGFLEET, Consts.ISWOOPSENTFLEET, Consts.ISWOOPAPPROVEDPARTNER, Consts.ISWOOPDOWNLOADEDPARTNER]
          return true

  getInitialState: =>
    that = this

    @cols = [
      class ID extends Column
        _header: "ID", _key: "id", _sortable: true
        getValue: ->
          str = ""
          if @record.job?.original_job_id?
            return @record.job.original_job_id+":B"
          else
            if @record.job?
              str += @record.job?.id
              if @record.job?.child_job?
                str += ":A"
          return str

      class Date extends Column
        _header: "Created Date", _key: "date"
        getValue: ->
          if @record?.job?
            datetime = Utils.formatDateTime(Utils.getJobCreatedAt(@record.job))
            if @record.job.scheduled_for?
              datetime = Utils.formatDateTime(@record.job.scheduled_for)
            datetime

      class SentDate extends Column
        _header: "Sent Date", _key: "sent_at"
        getValue: -> Utils.formatDateTime(@record?.sent_at)
        isShowing: ->  UserStore.isSwoopRoot() and that.props.tabType == 'partner'

      class Account extends Column
        _header: "Account", _key: "account"
        getValue: -> JobStore.getAccountNameByJobId(@record?.job?.id, that.getId())
        isShowing: ->  UserStore.isPartner()

      class PO extends Column
        _header: "PO", _key: "po_number", _originalWidth: 90,
        getValue: () ->
          if @record.job?.po_number?
            job = @record.job
            job.po_number
          else
            ""
        isShowing: ->
          FeatureStore.isFeatureEnabled("Job PO Number") and UserStore.isPartner()

      class Company extends Column
        _header: "Company", _key: "job.owner_company.name"
        isShowing: -> UserStore.isRoot()

      class Recipient extends Column
        _header: "Recipient", _key: "recipient.company.name"
        isShowing: -> UserStore.isRoot()

      class Partner extends Column
        _header: "Partner", _key: JobStore.partnerNameKey()
        isShowing: -> UserStore.isRoot() || UserStore.isFleet()

      class Service extends Column
        _header: "Service", _key:"job.service"
        getValue: () ->
          if @record.job?.service?
            job = @record.job
            span null,
              if job.priority_response
                span
                  className: "fa fa-flag"
                  style:
                    color: "red"
                    marginRight: 5
              span null,
                ServiceAliasStore.getServiceNameOrAlias(job?.service_code_id, job?.service_alias_id, that.getId()) + (if job.will_store then ", Storage" else "")
          else
            ""

      class Status extends Column
        _header: "Job Status", _key: "job.status"

      class Department extends Column
        _header: "Department", _key: "job.department_id"

        isShowing: ->
          UserStore.isTesla()

        getValue: ->
          DepartmentStore.get(@record.job?.department_id, "name", that.getId())

      class CustomerType extends Column
        _key: "job.customer_type_name"
        getHeader: -> if UserStore.isEnterprise() then "Claim Type" else "Payment Type"
        isShowing: -> that.props.tabType != 'partner'

      class AccountingCode extends Column
        _header: "Accounting Code", _key: "job.accounting_code"
        isShowing: -> UserStore.isEnterprise()
        newSelection: (job_id, e) ->
          value = e.target.value
          if !value || value.length == 0
            value = null
          JobStore.jobChanged({
            id: job_id
            accounting_code: e.target.value
          })

          Utils.ignoreEvents(e)
        getValue: ->
          options = ["1686","1773","2237","2260","239-RM","338","3452","538-T9","557","558","559","881","FA01","For review"]

          if @record?.job?.accounting_code
            # add the job accounting_code into the options in case it has been removed from the original 'options list ^'
            if ! (@record.job.accounting_code in options)
              options.unshift @record.job.accounting_code

          select
            className: "form-control"
            value: @record?.job?.accounting_code
            onClick: Utils.ignoreEvents
            onChange: partial(@newSelection, @record.job_id)
            style:
              height: 24
            option
              value: ""
              "- Select -"
            for o in options
              option
                value: o
                o



      class WarningSyncIssue extends Column
        _header: '', _key: "warning", _backend_key: "swoop_alert"
        isShowing: ->
          UserStore.isSwoop() and intersection([Consts.ISWOOPAPPROVEDFLEET, Consts.ISWOOPNEW], that.props.types).length > 0
        getValue: ->
          return div
            className: "text-center"
            if @record?.export_error_msg
              InfoBubble
                icon: "fa-exclamation-triangle"
                content: @record.export_error_msg


      class Warning extends Column
        _header: '', _key: "warning", _backend_key: "fleet_alert"
        isSortable: -> UserStore.isFleet()
        isShowing: ->
          if intersection([Consts.ISWOOPAPPROVEDFLEET, Consts.ISWOOPNEW, Consts.ISWOOPSENTFLEET], that.props.types).length > 0
            return false

          UserStore.isPartner() or
          ((UserStore.isFleet() or UserStore.isRoot()) and FeatureStore.isFeatureEnabled("Invoice Warning"))
        getValue: ->
          return div
            className: "text-center"
            if UserStore.isPartner() && JobStore.getAccountNameByJobId(@record?.job?.id, that.getId()) == "Agero"
              InfoBubble
                onClick: (e) ->
                  e.stopPropagation()
                  window.open('https://intercom.help/swoopme/admin-training/account-configuration/how-to-bill-agero-when-using-swoop-admin-training-210')
                icon: "fa-exclamation-triangle"
                content: "Agero invoices must be sent from the Agero Support Website. Click for instructions on how to invoice Agero."
            if that.showInvoiceRejectionInfo(@record)
              InvoiceRejectionInfoIcon
                invoiceRejection: @record.invoice_rejection
                invoice: @record
            if that.showInvoiceWarning(@record)
              InfoBubble
                onClick:
                  partial(that.showModal, 'invoice_changes', @record.id)
                icon: "fa-exclamation-triangle"
                content: "Partner edited the auto calculated price."

      class Cost extends Column
        _header: "Invoice Amount", _key: "cost", _backend_key: "amount", _sortable: true
        getValue: ->
          loading = InvoiceStore.isUpdatingInvoice(@record.id)?.line_items?
          div
            className: "invoice_amount_container fs-exclude"
            onClick: partial(that.showModal, 'invoice_changes', @record.id)
            div
              className: (if loading then "no-amount" else "money") + getCurrencyClass(@record.currency)
              if loading
                div
                  className: "money-spinner-container"
                  onClick: Utils.preventPropogation
                  Loading null
              else
                Utils.formatNumber(Utils.toFixed(parseFloat(@record.cost), 2))

      class Balance extends Column
        _header: "Balance", _key: "balance"
        getValue: ->
          isDisabled = that.isDisabled(@record)
          val = @record.balance
          if !val? || isNaN(parseFloat(val))
            val = 0
          div
            className: "invoice_amount_container fs-exclude" + if isDisabled then " disabled" else ""
            onClick: partial((record, e) =>
              if record.job? and !isDisabled
                that.showModal('invoice_form_edit', record.id, e)
            , @record)
            div
              className: "money" + getCurrencyClass(@record.currency)
              Utils.formatNumber(Utils.toFixed(parseFloat(val), 2))

      class State extends Column
        _header: "", _key: "state"
        getValue: -> InvoiceStatus
          invoice: @record
          handleInvoiceChange: that.handleInvoiceChange
          tab: that.props.tab

      class EditInvoice extends Column
        _header: "", _key: "editInvoice"
        getValue: ->
          job = @record.job
          if job?
            toggleButtonClasses = 'fa fa-chevron-down'
            if that.expandedJobs[job.id]
              toggleButtonClasses = 'fa fa-chevron-up'
          return div
            className: ''
            if (UserStore.isPartner() or UserStore.isRoot()) and job?
              EditInvoiceButton
                invoice: @record
                disabled: that.isDisabled(@record)
            ReactDOMFactories.i
             onClick: partial(that.toggleOptions, job, @record)
             className: 'fa fa-ellipsis-h options inline'
             name: 'options'
            if job?
              ReactDOMFactories.i
                className: toggleButtonClasses
    ]

    @cols = map(@cols, (col) ->
      new col()
    )
    return @getStateFromProps(@props)

  UNSAFE_componentWillReceiveProps: (props) ->
    if !isEqual(props, @props)
      defer(=>
        @searchInvoices(props.types, props.filters, props.search,undefined,undefined,undefined,props.paid )
      )
  sortChange: (key, order) =>
    searchOrder = if order == 1 then "asc" else "desc"
    @searchInvoices(@props.types, @props.filters, @props.search, key, searchOrder)
    @setState
      searchKey: key
      searchOrder: searchOrder

    return true

  redownload: () =>
    @searchInvoices(@props.types, @props.filters, @props.search, @state.searchKey, @state.searchOrder, Math.max(1, InvoiceStore.getSearchPage()-1))

  searchInvoices: (types, filters, search, searchKey, searchOrder, page, paid) =>
    if typeof searchKey == "undefined"
      searchKey = @state.searchKey
    if typeof searchOrder == "undefined"
      searchOrder = @state.searchOrder
    if typeof paid == "undefined"
      paid = @props.paid
    if not filters?
      filters = {}

    if paid?
      filters["paid"] = paid

    filters["job_active"] = false
    InvoiceStore.loadSearchInvoices(types, filters, search, "order":searchKey+","+searchOrder, page)

  handleToggleRow: (row, invoice) =>
    job = invoice.job
    if not job?
      return
    if !@expandedJobs[job.id]?
      @expandedJobs[job.id] = false

    @expandedJobs[job.id] = !@expandedJobs[job.id]
    @rowCallbacks?[job.id]?()

  #ID: id of invoice
  #obj:
  # Object: Invoice changes that will be sent
  # String: New invoice state
  #e: triggering event
  handleInvoiceChange: (id, obj, e) =>
    if typeof obj == "string"
      obj = {
        id: id
        state: obj
      }
    else
      obj.id = id

    Dispatcher.send(InvoiceStore.INVOICE_CHANGED, obj)
    #TODO: HACK SHOULD HAPPEN AFTER REQUEST GOES THROUGH, edge case here
    setTimeout( =>
      if @?.redownload?
        @redownload()
    , 1000)

    Dispatcher.send(ModalStore.CLOSE_MODALS)

    if e?
      e.preventDefault()
      e.stopPropagation()

    return false

  handleChange: (id, e) ->
    obj = {
      id: id
    }
    obj[e.target.name] = e.target.value
    JobStore.jobChanged(obj)
    return

  showModal: (key, job, e) ->
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: key,
      props:
        record: job
    )
    if e?
      e.preventDefault()
      e.stopPropagation()

  showEmailInvoice: (invoice) ->
    UserStore.isPartner() && invoice.state in Consts.PARTNER_INVOICE_APPROVED_AND_SENT_STATES

  # Rendering:
  #
  # ** PARTNER - IN APPROVED TAB **
  # - IF Partner user (but not Swoop user), and invoice state is partner_approved
  #
  # ** FLEET INHOUSE - IN APPROVED TAB **
  # - IF FleetInhouse user and invoice state is fleet_approved
  #
  # ** SWOOP - IN PARTNER UNPAID TAB (Partner bills Swoop) **
  # - IF Swoop user and invoice state is swoop_approved_partner and invoice is not paid yet
  #
  # ** SWOOP - IN FLEET UNPAID TAB (Swoop bills FleetManaged) **
  # - IF Swoop user and invoice state is swoop_approved_fleet
  #
  showUndoApproval: (invoice) ->
    (UserStore.isPartner() && (invoice.state == Consts.IPARTNERAPPROVED && not invoice.paid)) ||
    (UserStore.isFleetInHouse() && invoice.state == Consts.IFLEETAPPROVED) ||
    (UserStore.isSwoop() && invoice.state == Consts.ISWOOPAPPROVEDPARTNER && not invoice.paid) ||
    (UserStore.isSwoop() && invoice.state == Consts.ISWOOPAPPROVEDFLEET)

  warnBeforeUndoApproval: (invoice) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: 'simple',
      props: {
        title: 'Undo Approval'
        confirm: 'Unnaprove'
        body: Consts.INVOICE.UNDO_APPROVAL_WARNING
        onConfirm: =>
          Dispatcher.send(InvoiceStore.INVOICE_CHANGED,
            {
              id: invoice.id
              state: @undoApprovalEventFor(invoice.state)
            }
          )
          Dispatcher.send(ModalStore.CLOSE_MODALS)
      }
    )

  undoApproval: (invoice) =>
    if invoice.state in [Consts.ISWOOPAPPROVEDPARTNER, Consts.ISWOOPDOWNLOADEDPARTNER] and
        invoice.fleet_managed_invoice_approved_or_sent
      @warnBeforeUndoApproval(invoice)
    else
      Dispatcher.send(InvoiceStore.INVOICE_CHANGED,
        {
          id: invoice.id
          state: @undoApprovalEventFor(invoice.state)
        }
      )
      Dispatcher.send(ModalStore.CLOSE_MODALS)

    #let state switch
  render: ->
    super(arguments...)

    invoices = InvoiceStore.getSearchInvoices()
    invoices = filter(invoices, ((invoice) -> if invoice? then (not invoice.deleted_at? or invoice.deleted_at == null) else false ))

    toLoad = []
    for invoice in invoices
      #grab jobs for invoices and fetch if needed
      if invoice?
        job = JobStore.getFullJobById(invoice.job_id, true)
        #Hack to set up watcher for the job
        #TODO: getFullJobById should take a watcherId
        JobStore.getById(invoice.job_id, false, @getId())
        invoice.job = job

    div null,
      if @props.shouldShowDownloadButton? && @props.shouldShowDownloadButton(invoices)
        text = "Download All"
        apiCall = @props.downloadApiEndPoint || Api.downloadFleetUnpaid
        apiFilters = cloneDeep(@props.filters) || {}
        if UserStore.isSwoop() and CompanyStore.get(@props.filters?.client_company_id, "name") == Consts.FLEET_RESPONSE
          text = "Send All"
        if @props.includeSearchInDownloadCall && @props.search?
          apiFilters["job_search"] = @props.search
        div
          className: "extra_options"
          DownloadButtonAsync
            text: text
            apiCall: partial(apiCall, UserStore.getEndpoint(), null, apiFilters, null)
            onComplete: @clearAndDownload


      div
        style:
          clear: "both"
        FTable extend({}, @props,
          tableKey: "invoices"
          rows: invoices
          cols: @cols
          originalSortDir: -1
          originalSortKey: "id"
          onSortChange: @sortChange
          rowClick: @handleToggleRow
          fetchUpdatedRecord: (orig_invoice) ->
            if orig_invoice?
              invoice = InvoiceStore.getInvoiceById(orig_invoice.id)
            if invoice?
              invoice.job = JobStore.getJobById(invoice.job_id)
              return invoice
            else
              return orig_invoice
          rowDidMount: (rowComp, i, invoice) =>
            if invoice?
              InvoiceStore.bind( InvoiceStore.CHANGE+"_"+invoice.id, rowComp.generalChanged )
              JobStore.bind( JobStore.CHANGE+"_"+invoice.job_id, rowComp.generalChanged )
              @rowCallbacks[invoice.job_id] = rowComp.generalChanged
          rowWillUnmount: (rowComp, i, invoice) =>
            if invoice?
              InvoiceStore.unbind( InvoiceStore.CHANGE+"_"+invoice.id, rowComp.generalChanged )
              JobStore.unbind( JobStore.CHANGE+"_"+invoice.job_id, rowComp.generalChanged )

          rowWillReceiveProps: (rowComp, newProps, oldProps) =>
            if newProps?.row? and oldProps?.row? and newProps.row.id != oldProps.row.id
              invoice = oldProps.row
              InvoiceStore.unbind( InvoiceStore.CHANGE+"_"+invoice.id, rowComp.generalChanged )
              JobStore.unbind( JobStore.CHANGE+"_"+invoice.job_id, rowComp.generalChanged )
              invoice = newProps.row
              InvoiceStore.bind( InvoiceStore.CHANGE+"_"+invoice.id, rowComp.generalChanged )
              JobStore.bind( JobStore.CHANGE+"_"+invoice.job_id, rowComp.generalChanged )
              @rowCallbacks[invoice.job_id] = rowComp.generalChanged
          rowFooter: (i, invoice) =>
            job = invoice.job
            if job? and @expandedJobs[job.id]
              JobDetails
                id: invoice.job.id
                invoice: true
          )
        if InvoiceStore.areLoaded(@props.types) and invoices.length == 0 and toLoad.length == 0
          span
            className: "empty_message"
            @props.empty_message

        if not InvoiceStore.finishedLoadingSearchJobs()
          Loading null

module.exports = {
  Component: InvoiceList,
  default: withModalContext(InvoiceList),
}
