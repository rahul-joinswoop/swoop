import ReactDOM from 'react-dom'
import React from 'react'
import { div } from 'react-dom-factories'
import $ from 'jquery'
import Logger from 'lib/swoop/logger'
BaseComponent = require('components/base_component')
ModalContent = React.createFactory(require('components/modals_content'))

class BootstrapModal extends BaseComponent
  displayName: 'BootstrapModal'
  @defaultProps =
    extraClasses: " "
    updateConfirmButton: null

  constructor: (props) ->
    super(props)
    @modal = React.createRef()

  componentDidMount: ->
    super(arguments...)
    # When the component is added, turn it into a modal
    $(@modal.current).modal
      backdrop: 'static'
      keyboard: false
      show: true
    return

  componentWillUnmount: ->
    super(arguments...)
    $(@modal.current)
      .off 'hidden', @handleHidden
      .removeClass('fade')
      .modal('hide')

  render: ->
    super(arguments...)
    div
      className: 'modal fade'
      style: @props.style
      ref: @modal
      div
        className: 'modal-dialog'
        if @props.modalContent
          React.cloneElement(@props.modalContent, @props)
        else
          ModalContent @props,
            @props.children

BootstrapModal = React.createFactory(BootstrapModal)

class TransitionModal extends BaseComponent
  displayName: 'TransitionModal'
  @defaultProps =
    show: false

  render: ->
    super(arguments...)
    div null,
      @props.show && BootstrapModal @props,
        @props.children


module.exports = TransitionModal
