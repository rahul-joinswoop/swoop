import React from 'react'
import Logger from 'lib/swoop/logger'
import Api from 'api'
import Consts from 'consts'
JobStore = require('stores/job_store')
InvoiceStore = require('stores/invoice_store')
UserStore = require('stores/user_store')
EdmundsStore = require('stores/edmunds_store')
SiteStore = require('stores/site_store').default
AccountStore = require('stores/account_store')
StorageTypeStore = require('stores/storage_type_store')
ModalStore = require('stores/modal_store')
JobList = React.createFactory(require('components/job/job_list'))
FTable = React.createFactory(require 'components/resizableTable/FTable')
import JobDetailsImport from 'components/job_details'
JobDetails = React.createFactory(JobDetailsImport.default)
import Utils from 'utils'
BaseComponent = require('components/base_component')
import { extend, filter, map, partial } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class StorageList extends BaseComponent
  displayName: 'StorageList'

  constructor: (props) ->
    super(props)

    @state =
      options: false

    @register(UserStore, UserStore.LOGIN)

  expandedJobs: {}
  rowCallbacks: {}

  componentDidMount: ->
    super(arguments...)
    @researchOnUpdate = false

  handleToggleRow: (row, job) =>
    if not job?
      return

    id = job.id
    if !@expandedJobs[id]?
      @expandedJobs[id] = false

    @expandedJobs[id] = !@expandedJobs[id]
    @rerender()

  showModal: (key, job, e) ->
    Logger.debug("Shwoing modal ", key, " with job ",job.id, job)
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: key,
      props:
        record: job
        storage: true
    )
    if e?
      e.preventDefault()
      e.stopPropagation()

  showSpinnerModal: ->
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "simple",
      props: {
        title: "Invoice Loading"
        confirm: false
        cancel: false
        body:
          Loading
            size: 24
      }
    )

  showInvoiceModal: (job_id, e) =>
    Dispatcher.send(ModalStore.CLOSE_MODALS)
    job = JobStore.getJobById(job_id)

    invoice_id = job.partner_live_invoices?[0]?.id
    if !invoice_id
      displayInvoice = Utils.getDisplayInvoice(job, UserStore.isRoot())
      invoice_id = displayInvoice?.id

    Utils.showModal('invoice_form_edit', e,
      record: invoice_id
      title: "Release Vehicle",
      releasing: true
      submitButtonName: 'Release'
      submitCallback: =>
        @researchOnUpdate = true
        Dispatcher.send(ModalStore.CLOSE_MODALS)
    )

  releaseVehicle: (job, e) =>
    if job?
      Utils.showModal('storage_vehicle_release', e,
        record: job
        title: "Release Vehicle"
        submitButtonName: 'Next'
        callback: (data) =>
          if data?
            JobStore.addJob(data)
            if data.invoice?
              InvoiceStore.addInvoice(data.invoice)

          setTimeout(partial(@showInvoiceModal, job.id, e), 2000) #show spinner for 1 second so it doesn't hide to fast and gives time for WS to come through
        submitCallback: (newrecord) =>
          setTimeout(@showSpinnerModal, 100) #makes sure close event doesn't come before we show
      )

    if e?
      e.preventDefault()
      e.stopPropagation()

  towVehicle: (job, e) =>
    if job?
      if job.account?.id and AccountStore.isTeslaAccount(job.account.id, @getId())
        Dispatcher.send(ModalStore.SHOW_MODAL,
          key: "simple",
          props: {
            title: "Cannot Tow Out Tesla Vehicle"
            confirm: 'OK'
            cancel: null
            body:
              span null,
                "Please release this vehicle and call Tesla to request a new Tow request to be sent for the same vehicle."
            onConfirm: =>
              Dispatcher.send(ModalStore.CLOSE_MODALS)
          }
        )
      else if job.child_job?
        Dispatcher.send(ModalStore.SHOW_MODAL,
          key: "simple",
          props: {
            title: "Tow Out Already Exists"
            confirm: 'OK'
            cancel: null
            body:
              span null,
                "A tow out already exists on the Dashboard for this job as #"+job.id+":B."
            onConfirm: =>
              Dispatcher.send(ModalStore.CLOSE_MODALS)
          }
        )
      else
        Dispatcher.send(ModalStore.SHOW_MODAL,
          key: "simple",
          props: {
            title: "Tow Vehicle"
            confirm: 'Tow Vehicle'
            body:
              div
                className: "release_modal"
                div null,
                  "Please confirm you'd like to tow this vehicle out. Next, you'll be prompted to fill out job details for the tow."
                div null,
                  "Once created, the job will move to the Active tab of the Dashboard to manage the tow. A separate invoice will be created."
            onConfirm: =>
              JobStore.addOnJob(job.id, (data) ->
                Dispatcher.send(ModalStore.SHOW_MODAL,
                  key: "job_form_edit",
                  props:
                    record: data
                )
              )

              Dispatcher.send(ModalStore.CLOSE_MODALS)
          }
      )

    if e?
      e.preventDefault()
      e.stopPropagation()

  removeStorage: (job, e) ->
    if job? && UserStore.isPartner()
      Api.deleteStoredVehicle(job.storage.id, {
        success: =>
          Utils.sendNotification("Job ##{job.id} was removed from Storage", Consts.SUCCESS)
        error: =>
          Utils.sendNotification("Job ##{job.id} could not be removed from Storage", Consts.WARNING)
      })
      #TODO: make this a callback instead
      setTimeout(JobStore.reloadSearch, 200)
      Dispatcher.send(ModalStore.CLOSE_MODALS)

    if e?
      e.preventDefault()
      e.stopPropagation()

  toggleOptions: (job, e) =>
    invoice_id = job.partner_live_invoices?[0]?.id
    displayInvoice = Utils.getDisplayInvoice(job, UserStore.isRoot())
    if !invoice_id
      invoice_id = displayInvoice?.id
    if invoice?
      saved_invoice = InvoiceStore.getInvoiceById(invoice.id)

    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "options",
      props:
        target: e.target
        offsetTop: 25
        offsetLeft: -60
        id: job.id
        onUnfocus: @rerender
        options: [
          li
            onClick: (e) =>
              JobStore.getFullJob(job, (job) =>
                showInvoice = Utils.getDisplayInvoice(job, UserStore.isRoot())
                if showInvoice
                  Utils.showInvoice(showInvoice, e)
                else
                  Rollbar.error("Attempt to show an undefined invoice, from storage_list.coffee, job id:", {
                    jobId: job.id,
                  })
                  return
              )
              Utils.ignoreEvents(e)
            "View Invoice"
          if invoice_id?
            li
              onClick:  (e) =>
                JobStore.getFullJob(job, =>
                  @showModal('invoice_form_edit', invoice_id, e)
                )
                Utils.ignoreEvents(e)
              "Edit Invoice"
          if invoice_id?
            li
              onClick: (e) =>
                JobStore.getFullJob(job, @releaseVehicle)
                Utils.ignoreEvents(e)
              "Release Vehicle"
          li
            onClick: (e) =>
              JobStore.getFullJob(job, @towVehicle)
              Utils.ignoreEvents(e)
            "Tow Vehicle"
          if UserStore.isAdmin()
            li
              onClick: partial(@removeStorage, job)
              "Undo Storage"
        ]
      )
    e.preventDefault()
    e.stopPropagation()

  getDays: (job) ->
    invoice = job.partner_live_invoices?[0]
    if !invoice?
      invoice = Utils.getDisplayInvoice(job, UserStore.isRoot())

    if invoice?
      saved_invoice = InvoiceStore.getInvoiceById(invoice.id)
      if saved_invoice?
        invoice = saved_invoice
        for item in invoice.line_items
          if item.description == "Storage"
            return parseFloat(item.quantity)

  getInvoice: (job) ->
    invoice = job.partner_live_invoices?[0]
    if !invoice?
      invoice = Utils.getDisplayInvoice(job, UserStore.isRoot())
    if invoice?
      if InvoiceStore.isUpdatingInvoice(invoice.id)?.line_items?
        return Loading
          size: 24
          onClick: Utils.preventPropogation
      saved_invoice = InvoiceStore.getInvoiceById(invoice.id)
      if saved_invoice?
        invoice = saved_invoice
      return invoice
    return null

  render: ->
    super(arguments...)
    cols = [
      {header: "ID", width: 70, fixedWidth: true, sortable: true, key: "id", backendKey: "id", func: (job,props) => (if job.original_job_id? then job.original_job_id+":B" else job.id) + (if job.child_job? then ":A" else "") }
      {header: "Date", sortable: true, backendKey: "storage_date", key: "storage_date", func: (job, props) ->
        Utils.formatDateTime(job.storage.stored_at)
      }
      {header: "Days", maxWidth: 60, key: "storage_days",  func: (job, props) =>
          days = @getDays(job)
          if days?
            return days
          Loading
            padding: 0
            size: 24
            onClick: Utils.preventPropogation
      }
      {header: "Site", key: "location", func: (job, props) ->
        siteName = SiteStore.get(job.storage.site_id, 'name', @props.componentId)

        if siteName?
          siteName
        else
          "Unknown Site"
      }
      {header: "Type", key: "storage_type_id", func: (job, props) ->
        typeName = if job.storage_type_id then StorageTypeStore.get(job.storage_type_id, 'name', @props.componentId)
        if typeName
          typeName
        else
          if job.storage_type_id?
            "Unknown Type"
          else
            ""
      }
      {header: "Account", backendKey: "account", func: (job, props) -> JobStore.getAccountNameByJobId(job?.id, @props.componentId) }
      {header: "Year", maxWidth: 60, key: "year" }
      {header: "Make", key: "make" }
      {header: "Model", key: "model" }
      {header: "License", key: "license" }
      {header: "VIN", key: "vin" }
      {header: "Invoice Total", maxWidth: 100, key: "cost", func: (job, props) =>
        invoice = @getInvoice(job)
        cost = parseFloat(invoice?.total_amount)
        if cost? and !isNaN(cost)
          return Utils.toFixed(cost, 2)
        return null
      }
      {header: "Balance", maxWidth: 100, key: "balance", func: (job, props) =>
        invoice = @getInvoice(job)
        balance = parseFloat(invoice?.balance)
        if balance? and !isNaN(balance)
          return Utils.toFixed(balance, 2)
        return null
      }
      {header: "",  sortable: false, fixedWidth: true, width: 90, key: "expand", className: "right expand_action", func: (job, props) =>
        classes = 'item'
        toggleButtonClasses = 'fa fa-chevron-down'
        if @expandedJobs[job.id]
          classes = 'item selected'
          toggleButtonClasses = 'fa fa-chevron-up'

        div
          className: "storageActions right",
          ReactDOMFactories.i
           onClick: (e) =>
             JobStore.getFullJob(job, (job) =>
               @showModal('storage_vehicle_edit', job, e)
             )
             Utils.ignoreEvents(e)
           className: 'fa fa-pencil edit inline'
           name: 'edit'
          ReactDOMFactories.i
           onClick: partial(@toggleOptions, job)
           className: 'fa fa-ellipsis-h options inline'
           name: 'options'
          ReactDOMFactories.i
            className: toggleButtonClasses
      }

    ]

    cols = filter(cols, ((col) -> not col.show? or col.show))
    cols = map(cols, (val) =>
      if not val.key?
        val.key = val.header

      if not val.sortable?
        val.sortable = false
      return val
    )

    div
      style:
        position: "relative"
      FTable extend({}, @props,
        rows: @props.jobs
        cols: cols
        search: false
        sortable: true
        backendSort: true
        page: 1
        shouldSort: false
        componentId: @getId()
        sortKey: "job.storage.id"
        rowClick: @handleToggleRow
        rowDidMount: @rowDidMount
        rowWillUnmount: @rowWillUnmount
        rowWillReceiveProps: @rowWillReceiveProps
        rowClassNameGetter: (i, job) =>
          className = ""
          if not job?
            return className
          if @expandedJobs[job.id]
            className += " expanded"
          #if @shouldAlwaysFlash(@jobs[i])
          #  className += " blink_me_always"
          #else if @shouldFlash(@jobs[i])
          # className += " blink_me"
          return className
        rowFooter: (i, job) =>
          if not job?
            return

          if @expandedJobs[job.id]
            JobDetails
              id: job.id
              storage: true
        sortDir: 1
      )

module.exports = React.createFactory(StorageList)
