import React, { Component } from 'react'
import { withRouter } from 'react-router5'
import Api from 'api'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import Utils from 'utils'

class SetPassword extends Component {
  confirmation = React.createRef()

  password = React.createRef()

  state = {
    error: '',
    loading: false,
  }

  setError(error) {
    this.setState({ error })
  }

  async setPassword() {
    const {
      router, setExpired, uuid,
    } = this.props
    const confirmation = this.confirmation.current.value
    const password = this.password.current.value

    if (password.trim().length < 8) {
      this.setError(Consts.CHANGE_PASSWORD.INVALID_PASSWORD)
      return
    }

    if (password !== confirmation) {
      this.setError(Consts.CHANGE_PASSWORD.PASSWORDS_DONT_MATCH)
      return
    }

    this.setState({ loading: true })
    try {
      await Api.changePasswordAgeroPartner(uuid, password)
      router.navigate('partner/setupBilling')
    } catch ({ status }) {
      if (status === 422) {
        this.setError(Consts.CHANGE_PASSWORD.INVALID_PASSWORD)
      } else if (status === 404) {
        setExpired()
      } else {
        Utils.handleError()
      }
    }
    this.setState({ loading: false })
  }

  cleanError = () => this.setError(null)

  render() {
    const { email } = this.props
    const { error, loading } = this.state

    return (
      <div className="SetPassword">
        <div className="SetPassword-description">
          You must create a new password on the Swoop Platform to invoice all Agero (Swoop) jobs.
        </div>

        <div className="field">
          <div>Email</div>
          <div className="SetPassword-username">{email}</div>
        </div>

        {error && <div className="SetPassword-error">{error}</div>}

        <div className="field">
          {/* eslint-disable-next-line */}
          <label className="label-reg" htmlFor="password">New Password</label>

          <input
            className="form-control"
            id="password"
            name="user[password]"
            onChange={this.cleanError}
            placeholder={Consts.CHANGE_PASSWORD.PASSWORD_PLACEHOLDER}
            ref={this.password}
            required
            type="password"
          />
        </div>

        <div className="field">
          {/* eslint-disable-next-line */}
          <label className="label-reg" htmlFor="confirmation">Confirm Password</label>

          <input
            className="form-control"
            id="confirmation"
            name="user[password_confirmation]"
            onChange={this.cleanError}
            placeholder={Consts.CHANGE_PASSWORD.CONFIRMATION_PLACEHOLDER}
            ref={this.confirmation}
            required
            type="password"
          />
        </div>

        <div className="actions">
          <Button
            color="primary"
            disabled={loading}
            lineHeight={40}
            name="commit"
            onClick={this.setPassword}
            size="full-width"
            type="submit"
          >
            {loading ?
              <i className="fa fa-spinner fa-spin fa-2x" /> :
              'Create Password'
            }
          </Button>
        </div>
      </div>
    )
  }
}

export default withRouter(SetPassword)
