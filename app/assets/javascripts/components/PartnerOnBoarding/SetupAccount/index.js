import React, { Component } from 'react'
import { withRoute } from 'react-router5'
import LinkExpired from './LinkExpired'
import SetPassword from './SetPassword'
import Wrapper from '../Wrapper'
import './style.scss'

class SetupAccount extends Component {
  state = {
    expired: false,
  }

  setExpired = () => this.setState({ expired: true })

  render() {
    const { route } = this.props
    const { expired } = this.state

    const { error } = window.gon
    const email = window.gon?.user?.email
    const application_uid = window.gon?.application?.uid

    let title, component
    if (expired || error) {
      title = 'Session Expired'
      component = <LinkExpired {...{ email, application_uid }} />
    } else if (email) {
      title = 'Create a Swoop Password for Billing'
      component = <SetPassword
        email={email}
        setExpired={this.setExpired}
        uuid={route.params.uuid}
      />
    }

    return <Wrapper title={title}>{component}</Wrapper>
  }
}

export default withRoute(SetupAccount)
