import React, { Component } from 'react'
import Api from 'api'
import Button from 'componentLibrary/Button'
import Consts from 'consts'

class LinkExpired extends Component {
  state = {
    error: '',
    loading: false,
    success: false,
  }

  setError = error => this.setState({ error })

  sendEmail = async () => {
    const { email, application_uid } = this.props

    this.setState({ loading: true })
    try {
      // this will always return 201, the only error case here is if the network connection was down or
      // if the remote server had other issues
      await Api.createPasswordRequestAgeroPartner(email, application_uid)
      this.setState({ success: true })
    } catch {
      this.setError(Consts.PASSWORD_REQUEST.FAIL)
    }
    this.setState({ loading: false })
  }

  render() {
    const { error, loading, success } = this.state

    if (success) {
      return (
        <>
          <div className="LinkExpired-success">
            We sent you an email. Please click on the link in the email to create your new password.
          </div>

          <div>
            Having trouble finding our email? Check your spam folder or <a href="mailto:support@joinswoop.com">Contact Support</a>.
          </div>
        </>
      )
    }

    const { email } = this.props

    return (
      <>
        <div className="LinkExpired-description">
          It looks like the link you’re trying to use has expired. Click below to send another link to <b>{email}</b>:
        </div>

        <div className="field">
          {error &&
          <>
            <div className="errorMark">!</div>
            <div className="errorText">{error}</div>
          </>}
        </div>

        <div className="LinkExpired-actions">
          <Button
            color="primary"
            disabled={loading}
            lineHeight={40}
            name="commit"
            onClick={this.sendEmail}
            size="full-width"
            type="submit"
          >
            {loading ?
              <i className="fa fa-spinner fa-spin fa-2x" /> :
              'Re-Send Email'
            }
          </Button>
        </div>
      </>
    )
  }
}

export default LinkExpired
