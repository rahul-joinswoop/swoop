import React from 'react'
import Button from 'componentLibrary/Button'
import Wrapper from '../Wrapper'
import './style.scss'

const SetupBilling = () => (
  <Wrapper title="Set Up Billing">
    <div className="SetupBilling-description">
      You’re all set to submit invoices on the Swoop platform!<br />
      <br />
      After you complete your jobs labeled <b>Agero (Swoop)</b>, log onto the Swoop platform to review and approve invoices.
      Payments will be made to your Paymode account using your Agero settings.
    </div>

    <div className="SetupBilling-actions">
      <Button
        color="primary"
        component="a"
        href="https://intercom.help/swoopme/en/articles/126531-how-do-i-approve-and-send-invoices"
        label="Learn How to Invoice on Swoop"
        lineHeight={40}
        name="commit"
        size="full-width"
      />
    </div>
  </Wrapper>
)

export default SetupBilling
