import React from 'react'
import Button from 'componentLibrary/Button'

import Wrapper from '../Wrapper'
import './style.scss'

const AllowAccess = ({ pre_auth, oauth_authorization_path, client_name }) =>
  <>
    <div>
          Thanks for confirming your email. <br />
      <br />
          Click the button below to give {client_name} access to your Agero (Swoop) jobs.
    </div>

    <div className="AllowAccess-actions">
      <form action={oauth_authorization_path} method="post">
        {Object.entries(pre_auth).map(
          ([name, value]) => <input
            key={name}
            name={name}
            type="hidden"
            value={value || ''}
          />
        )}
        <Button
          color="primary"
          label="Allow Access"
          lineHeight={40}
          size="full-width"
          type="submit"
        />
      </form>
    </div>
  </>

export const LinkExpired = () => {
  const { app } = window.gon

  return (
    <Wrapper title="Validation Link Expired">
      <div>
        It looks like the link you’re trying to use has expired.
        Please log back into your {app?.name} application to connect Swoop.
        Check your email within 24 hours to complete the validation process.
      </div>
    </Wrapper>
  )
}

const Index = () => {
  // 3rd party app
  const { pre_auth, oauth_authorization_path, client_name } = window.gon

  // let title, component
  // for now we're not doing link validations here
  // if (expired) {
  //   title = 'Validation Link Expired'
  //   component = <LinkExpired app={app} />
  // } else {
  const title = `Allow Access to ${client_name}`
  const component = <AllowAccess {...{ pre_auth, oauth_authorization_path, client_name }} />
  // }

  return <Wrapper title={title}>{component}</Wrapper>
}

export default Index
