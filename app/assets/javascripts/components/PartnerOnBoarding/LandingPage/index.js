import React, { useState } from 'react'
import Api from 'api'
import Button from 'componentLibrary/Button'
import Utils from 'utils'
import Wrapper from '../Wrapper'
import './style.scss'

// cn means className
const cn = strings => `LandingPage-${strings[0]}`

const ConfirmEmail = ({ app, email }) => (
  <>
    <div className={cn`confirmationMessage`}>
      We sent a confirmation email to {email}. Click on the link in that email to finish connecting your {app?.name} account.
    </div>

    <div>
      Having trouble finding our email? Check your spam folder or <a href="https://agerosupport.com/Content/SingleSectionContentPageNoBreadCrumbs.aspx?ContentCollectionName=ContactUs">Contact Support</a>.
    </div>
  </>
)

const ConnectForm = ({ callback, app: { client_id, redirect_uri } }) => {
  const [email, setEmail] = useState('')
  const [error, setError] = useState(null)
  const [loading, setLoading] = useState(false)

  const handleEmail = (e) => {
    setError(null)
    setEmail(e.target.value)
  }

  async function connect() {
    if (!email) {
      setError('Please enter Agero Account Email Address')
      return
    }

    if (!Utils.isValidEmail(email)) {
      setError('Invalid Email')
      return
    }

    setLoading(true)
    try {
      await Api.signupAgeroPartner(email, client_id, redirect_uri)
      callback(email)
    } catch (e) {
      setError('Email not found. Try again or enter another email you use to log into Agero.')
    }
    setLoading(false)
  }

  return (
    <>
      <div className={cn`description`}>
        Enter the email address associated with your Agero account to continue.
      </div>

      <div className="field">
        <label className="label-reg" htmlFor="email">Agero Account Email Address</label>

        <input
          className="form-control"
          id="email"
          onChange={handleEmail}
          required
          value={email}
        />

        {error &&
        <>
          <div className="errorMark">!</div>
          <div className="errorText">{error}</div>
        </>}
      </div>

      <div className={cn`actions`}>
        <Button
          color="primary"
          disabled={loading}
          lineHeight={40}
          name="commit"
          onClick={connect}
          size="full-width"
          type="submit"
        >
          {loading ?
            <i className="fa fa-spinner fa-spin fa-2x" /> :
            'Submit'}
        </Button>
      </div>

      <div className={cn`termsOfService`}>
      By clicking Submit, you agree to Agero and Swoop’s <a href="/SwoopTerms.pdf">Terms of Service</a>, <strong>including performing services at Agero contracted rates.</strong>
      </div>

      <div className={cn`contactSupport`}>
        Having issues connecting your account? <a href="https://agerosupport.com/Content/SingleSectionContentPageNoBreadCrumbs.aspx?ContentCollectionName=ContactUs">Contact Support</a>
      </div>
    </>
  )
}

const LandingPage = () => {
  // 3rd party app
  const { app, error } = window.gon

  const [email, setEmail] = useState('')

  let title, component
  if (error) {
    component = (
      <>
        We&apos;ve encountered an error.
        <div className={cn`error`}>{error}</div>
      </>
    )
  } else if (email) {
    title = 'Confirm Your Email'
    component = <ConfirmEmail email={email} />
  } else {
    title = 'Connect the Swoop Platform'
    component = <ConnectForm app={app} callback={setEmail} />
  }

  return <Wrapper error={!!error} title={title}>{component}</Wrapper>
}

export default LandingPage
