import React, { Component } from 'react'
import Consts from 'consts'
import Header from 'components/Header'
import AgeroSwoopLogo from 'images/Agero_Plus_Swoop_Logo_FullColor.png'
import './style.scss'

// cn means className
const cn = strings => `PartnerOnBoarding-${strings[0]}`

class Wrapper extends Component {
  render() {
    const { children, error, title } = this.props

    let content
    if (error) {
      content = <div className={cn`error`}>{children}</div>
    } else {
      content = (
        <div className="box-form reg">
          <div className="box-body reg">
            <div className={cn`logos`}>
              <img alt="Agero Plus Swoop Logo" className="AgeroSwoopLogo" src={AgeroSwoopLogo} />
            </div>

            <h1 className="form-title">{title}</h1>

            <div className="form-group user-mock-group">
              {children}
            </div>
          </div>
        </div>
      )
    }

    return (
      <div className="PartnerOnBoarding">
        <Header />

        <div className="content-wrapper reg">
          <div className="row">
            {content}

            <div className={cn`contactSupport`}>
              {Consts.CONTACT_INFO}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Wrapper
