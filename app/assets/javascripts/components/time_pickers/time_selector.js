import React from 'react'
import PropTypes from 'prop-types'
import 'stylesheets/components/time_pickers/time_selector.scss'
import BaseComponent from 'components/base_component'
import SwoopDateTime from 'components/time_pickers/swoop_datetime'

class TimeSelector extends BaseComponent {
  constructor(props) {
    super(props)

    this.datetime = React.createRef()
  }

  getValue(...props) {
    return this.datetime.current.getValue(...props)
  }

  getTempInputValue(...props) {
    return this.datetime.current.getTempInputValue(...props)
  }

  render() {
    return (
      <SwoopDateTime
        className={
          ['time_selector', this.props.className || ''].join(' ')
        }
        dateFormat={false}
        onChange={this.props.onChange}
        placeholder="HH:MM AM"
        ref={this.datetime}
        value={this.props.value}
      />
    )
  }
}

TimeSelector.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.object,
}

export default TimeSelector
