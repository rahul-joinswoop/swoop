import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment-timezone'
import 'stylesheets/react-datetime.scss'
import 'stylesheets/components/time_pickers/date_time_selector.scss'
import { isEqual } from 'lodash'
import DateSelector from 'components/time_pickers/date_selector'
import TimeSelector from 'components/time_pickers/time_selector'
import BaseComponent from 'components/base_component'

class DateTimeSelector extends BaseComponent {
  constructor(props) {
    super(props)

    this.date = React.createRef()
    this.time = React.createRef()

    this.state = {
      date: props?.value ? moment(props.value) : moment(new Date()),
      time: props?.value ? moment(props.value) : null,
      previousValue: null,
    }
  }

  // Public interface
  getValue = () => this._getDate(this.state.date, this.state.time)

  static getDerivedStateFromProps(props, state) {
    if (props.value && !isEqual(props.value, state.previousValue)) {
      return {
        date: props.value ? moment(props.value) : moment(new Date()),
        time: props.value ? moment(props.value) : null,
        previousValue: props.value,
      }
    }
    return null
  }

  handleDateChange = (newDate) => {
    const { date } = this.state
    let { time } = this.state

    if (newDate && !date && !time && this.time?.current) {
      time = this.time.current.getTempInputValue()
    }

    this.setState({
      date: newDate,
      time,
    })
    this.sendOnChange(newDate, time)
  }

  handleTimeChange = (newTime) => {
    const { time } = this.state
    let { date } = this.state

    if (newTime && !date && !time && this.date?.current) {
      date = this.date.current.getTempInputValue()
    }

    this.setState({
      time: newTime,
      date,
    })
    this.sendOnChange(date, newTime)
  }

  sendOnChange = (date, time) => {
    // Combines date and time into one moment object
    if (this.props.onChange) {
      this.props.onChange(this._getDate(date, time))
    }
  }

  _getDate = (date, time) => {
    // will return null unless BOTH date and time have selected values
    if (
      date &&
      time &&
      moment.isMoment(date) &&
      moment.isMoment(time) &&
      date.isValid() &&
      time.isValid()
    ) {
      const ret = moment(date)
      ret.set('hour', time.get('hour'))
      ret.set('minute', time.get('minute'))
      return ret
    } else {
      return null
    }
  }

  render() {
    return (
      <div className={['date_time_selector', this.props.className || ''].join(' ')}>
        <DateSelector
          onChange={this.handleDateChange}
          ref={this.date}
          value={this.state.date}
        />
        <TimeSelector
          onChange={this.handleTimeChange}
          ref={this.time}
          value={this.state.time}
        />
      </div>
    )
  }
}

DateTimeSelector.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.object,
}

export default DateTimeSelector
