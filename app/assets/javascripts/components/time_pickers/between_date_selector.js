import React from 'react'
import PropTypes from 'prop-types'
import DateSelector from 'components/time_pickers/date_selector'
import moment from 'moment-timezone'
import 'stylesheets/components/time_pickers/between_date_selector.scss'
import BaseComponent from 'components/base_component'

class BetweenDateSelector extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      toValue: props.initialFrom,
      fromValue: props.initialFrom,
    }
  }

  validateFrom = (fromDate) => {
    let fromDateTime = null
    if (fromDate !== null && fromDate !== undefined) {
      fromDateTime = moment(
        `${fromDate.format('MM/DD/YYYY')} 12:00 am ${moment().format('z')}`,
        'MM/DD/YYYY hh:mm a z',
      )
    }

    this.setState({
      fromValue: fromDateTime,
    })
    return this.props.onChangeFrom?.(fromDateTime)
  }

  validateTo = (toDate) => {
    let toDateTime = null
    if (toDate !== null && toDate !== undefined) {
      toDateTime = moment(
        `${toDate.format('MM/DD/YYYY')} 11:59 pm ${moment().format('z')}`,
        'MM/DD/YYYY hh:mm a z',
      )
    }

    this.setState({
      toValue: toDateTime,
    })
    return this.props.onChangeTo?.(toDateTime)
  }

  isValidFrom = (current) => {
    let toPlusOne

    if (this.state.toValue) {
      toPlusOne = moment(this.state.toValue).add('days', 1)
    } // currently can't choose a date beyond today

    return current?.isBefore(moment()) && (!toPlusOne || current?.isBefore(toPlusOne))
  }

  isValidTo = (current) => {
    let fromMinusOne

    if (this.state.fromValue) {
      fromMinusOne = moment(this.state.fromValue).subtract('days', 1)
    } // currently can't choose a date beyond today

    return current?.isBefore(moment()) && (!fromMinusOne || current?.isAfter(fromMinusOne))
  }

  render() {
    return (
      <div className="between_date_selector">
        <DateSelector
          isValidDate={this.isValidFrom}
          onChange={this.validateFrom}
          placeholder="From"
          value={this.state.fromValue}
        />
        <DateSelector
          isValidDate={this.isValidTo}
          onChange={this.validateTo}
          placeholder="To"
          value={this.state.toValue}
        />
      </div>
    )
  }
}

BetweenDateSelector.propTypes = {
  onChangeFrom: PropTypes.func,
  onChangeTo: PropTypes.func,
}

export default BetweenDateSelector
