import React from 'react'
import PropTypes from 'prop-types'
import 'stylesheets/components/time_pickers/date_selector.scss'
import BaseComponent from 'components/base_component'
import { compact, flattenDeep, keys, map, values, zipObject } from 'lodash'

import SwoopDateTime from 'components/time_pickers/swoop_datetime'

class DateSelector extends BaseComponent {
  constructor(props) {
    super(props)

    this.datetime = React.createRef()
  }

  getValue(...props) {
    return this.datetime.current.getValue(...props)
  }

  getTempInputValue(...props) {
    return this.datetime.current.getTempInputValue(...props)
  }

  render() {
    let styles
    if (Array.isArray(this.props.style)) {
      const style = compact(this.props.style) || {}
      styles = zipObject(map(style, keys), flattenDeep(map(style, values)))
    } else {
      styles = this.props.style
    }
    return (
      <SwoopDateTime
        className={['date_selector', this.props.className || ''].join(' ')}
        isValidDate={this.props.isValidDate}
        onChange={this.props.onChange}
        placeholder={this.props.placeholder || 'MM/DD/YYYY'}
        ref={this.datetime}
        style={styles}
        timeFormat={false}
        value={this.props.value}
      />
    )
  }
}

DateSelector.propTypes = {
  className: PropTypes.string,
  isValidDate: PropTypes.func,
  onChange: PropTypes.func,
  value: PropTypes.object,
}

export default DateSelector
