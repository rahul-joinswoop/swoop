import React from 'react'
import PropTypes from 'prop-types'
import DateTimeSelector from 'components/time_pickers/date_time_selector'
import moment from 'moment-timezone'
import BaseConsts from 'base_consts'
import BaseComponent from 'components/base_component'
import UserStore from 'stores/user_store'
import 'stylesheets/components/time_pickers/date_time_selector_with_timezone.scss'
import { clone, forEach, sortBy } from 'lodash'

class DateTimeSelectorWithTimezone extends BaseComponent {
  constructor(props) {
    super(props)

    const userTimeZone = UserStore.getUser()?.timezone

    this.baseTimeZone = userTimeZone || moment.tz.guess()

    this.state = {
      timeZone: this.baseTimeZone,
      currentValue: this.props.value,
    }
  }

  isValid = (newMoment) => {
    if (this.props.disableValidation) {
      return moment.isMoment(newMoment)
    }

    if (!moment.isMoment(newMoment) || !newMoment.isValid()) {
      return false
    }

    if (!this.props.validate) {
      return true
    }

    return this.props.validate(newMoment?.format('MM/DD/YY H:mm z'))
  }

  utcOffset(timezone) {
    const now = moment()
    const localOffset = now.utcOffset()
    now.tz(timezone)
    const centralOffset = now.utcOffset()
    return localOffset - centralOffset
  }

  handleTimeZoneChange = (e) => {
    const minuteDifference = this.utcOffset(e.target.value)
    const newMoment = this.state.currentValue.clone().add(minuteDifference, 'minutes')

    if (this.isValid(newMoment)) {
      this.setState({
        timeZone: e.target.value,
      })
      this.props.onChange(newMoment)
    }
  }

  handleDateTimeChange = (newDateTime) => {
    const minuteDifference = this.utcOffset(this.state.timeZone)
    const newMoment = newDateTime?.clone().add(minuteDifference, 'minutes')

    if (this.isValid(newMoment)) {
      this.setState({
        currentValue: newDateTime,
      })
      this.props.onChange(newMoment)
    }
  }

  normalizedDisplayValue = () => {
    const minuteDifference = -this.utcOffset(this.state.timeZone)
    return this.props.value.clone().add(minuteDifference, 'minutes')
  }

  getNewProps = () => {
    const newProps = clone(this.props)

    newProps.value = this.normalizedDisplayValue()
    newProps.onChange = this.handleDateTimeChange
    return newProps
  }

  getSelectOptions = () => {
    const guess = moment.tz.guess()
    const options = []
    const timeZones = {}

    if (this.state.timeZone) {
      const currentTimeZone = moment.tz(this.state.timeZone).format('z')

      if (!timeZones[currentTimeZone]) {
        options.push({
          value: this.state.timeZone,
          text: currentTimeZone,
        })
        timeZones[currentTimeZone] = true
      }
    }

    if (this.state.timeZone !== guess) {
      const guessTimeZone = moment.tz(guess).format('z')

      if (!timeZones[guessTimeZone]) {
        options.push({
          value: guess,
          text: guessTimeZone,
        })
        timeZones[guessTimeZone] = true
      }
    }

    forEach(BaseConsts.STANDARD_TIMEZONES, (timeZoneName) => {
      const timeZone = moment.tz(timeZoneName).format('z')
      if (!timeZones[timeZone]) {
        options.push({
          value: timeZoneName,
          text: timeZone,
        })
        timeZones[timeZone] = true
      }
    })

    return sortBy(options, 'text')
  }

  render() {
    return (
      <div className="date_time_selector_with_timezone">
        <DateTimeSelector {...this.getNewProps()} />
        <div className="timezone_selector">
          <select className="form-control" onChange={this.handleTimeZoneChange} value={this.state.timeZone}>
            {this.getSelectOptions().map(opt => (
              <option key={opt.value} value={opt.value}>{opt.text}</option>
            ))}
          </select>
        </div>
      </div>
    )
  }
}

DateTimeSelectorWithTimezone.propTypes = {
  className: PropTypes.string,
  disableValidation: PropTypes.bool,
  future: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  onError: PropTypes.func,
  placeholder: PropTypes.string,
  showTimeZone: PropTypes.bool,
  style: PropTypes.object,
  type: PropTypes.string,
  validate: PropTypes.func,
  value: PropTypes.object,
}

export default DateTimeSelectorWithTimezone
