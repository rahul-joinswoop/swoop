import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment-timezone'
import 'stylesheets/components/time_pickers/swoop_datetime.scss'
import DateTime from 'react-datetime'
import BaseComponent from 'components/base_component'
import { isEqual } from 'lodash'

/*
swoop_datetime component
  timeFormat: 'HH:MM A' (default), false (for date only), custom format
  dateFormat: 'MM/DD/YYYY' (default), false (for time only), custom format
  value: datetime to display
  onChange: callback when a valid datetime is selected or null if non-valid
*/

class SwoopDateTime extends BaseComponent {
  constructor(props) {
    super(props)
    this.datetimeSelector = React.createRef()
    this.state = {
      value: this.props?.value || null,
      tempInputValue: null,
    }
  }

  componentDidMount() {
    // need to add listener here instead of component because
    // can't get calendar to close on enter when focus on it instead of input field
    // and can't add listener/pass needed prop into 3rd party component.
    return document.addEventListener('keydown', this.handleOnKeyDown)
  }

  componentWillUnmount() {
    return document.removeEventListener('keydown', this.handleOnKeyDown)
  }

  // Get's the current input datetime value, will differ from getValue if getValue is null
  getValue = () => this.state.value

  // TODO: ^ When passed an invalid date tempInputValue should be cleared,
  // but necessary to keep it around for the swoop_date_time_picker

  getTempInputValue = (format = this.lastFormat) => {
    // Only unsets on local input state if a real new different moment will exists in state
    if (this.state.tempInputValue) {
      return this.datetimeSelector.current.localMoment(this.state.tempInputValue, format)
    } else {
      return this.getValue()
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps?.value && typeof this.props.value === undefined) {
      console.warn(
        'A component is changing an uncontrolled input of type \n' +
        'SwoopDateTime to be controlled. Input elements should not switch from \n' +
        'uncontrolled to controlled (or vice versa).'
      )
    }

    if (nextProps?.value && !isEqual(nextProps.value, this.props.value)) {
      const propMoment = nextProps.value ? moment(nextProps.value) : null
      return this.setNewState(propMoment)
    }
  }

  setNewState = (datetime) => {
    if (!moment.isMoment(datetime) || !datetime.isValid()) {
      // TODO: We should maybe clear the tempInputValue here
      this.setState({
        value: null,
      })
      return
    }

    const newState = {
      value: datetime,
    }
    const format = this.lastFormat || datetime?._f
    const tempDate = this.getTempInputValue(format)
    // If a new date is passed in, reset the input and use the value

    if (this.state.tempInputValue && datetime.format(format) !== tempDate?.format(format)) {
      newState.tempInputValue = null
    }

    this.setState(newState)
  }

  setInputFieldToCalendarSelection = () => {
    // if no date is selected in the calendar on enter we want to
    // reset our state and the inputField of the component
    this.setState({
      tempInputValue: null,
    })

    if (!this.datetimeSelector.current.state.selectedDate) {
      // this takes care of this situation: user types 0,
      // presses Enter --> the refs setState clears the field
      this.setState({
        value: null,
      })
      this.handleOnChange(null)
    }
  }

  handleOnKeyDown = (event) => {
    if (
      this.datetimeSelector.current.state.open &&
      (this.state.value === null ||
        (moment.isMoment(this.state.value) && this.state.value.isValid()))
    ) {
      const isDateValid = this.props.isValidDate ? this.props.isValidDate(this.state.value) : true

      if (isDateValid) {
        switch (event.which) {
          case 9:
            // Keyboard Tab
            return this.datetimeSelector.current.closeCalendar()

          case 13:
            // Keyboard Enter
            event.preventDefault()
            this.datetimeSelector.current.closeCalendar()
            return document.activeElement.blur()

          default:
            return null
        }
      }
    }
  } // Passes back any valid datetimes or null

  handleOnChange = (datetime) => {
    if (this.props?.onChange) {
      // Only pass up valid dates or null
      if (moment.isMoment(datetime) && datetime.isValid()) {
        // && tempDate && tempDate.format(format) == datetime.format(format)
        this.lastFormat = datetime._f
        return this.props.onChange(datetime)
      } else {
        return this.props.onChange(null)
      }
    } else {
      return this.setNewState(datetime)
    }
  } // This hack was created to allow a user to type in their own custom input while the element is being controlled

  // The controller will receive all changes with the corresponding date but the inputValues state will only be udpated
  // If the received state is a different date than the interpreted date of the input value

  renderInputHack = (props) => {
    props.value = this.state.tempInputValue?.valueOf() || props.value // Creates middleware for the onchange so we can keep track of the true input value

    const oldOnChange = props.onChange

    props.onChange = (e) => {
      this.setState({
        tempInputValue: e.target.value,
      })
      return oldOnChange(e)
    }

    return <input {...props} />
  }

  render() {
    return (
      <DateTime
        className={['swoop_datetime', this.props.className || ''].join(' ')}
        dateFormat={this.props.dateFormat}
        inputProps={{
          placeholder: this.props.placeholder,
          style: this.props.style,
        }}
        isValidDate={this.props.isValidDate}
        onBlur={this.setInputFieldToCalendarSelection}
        onChange={this.handleOnChange}
        ref={this.datetimeSelector}
        renderInput={this.renderInputHack}
        strictParsing={false}
        timeFormat={this.props.timeFormat}
        value={this.state.value}
      />
    )
  }
}

SwoopDateTime.propTypes = {
  className: PropTypes.string,
  dateFormat: PropTypes.bool,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.object,
}

export default SwoopDateTime
