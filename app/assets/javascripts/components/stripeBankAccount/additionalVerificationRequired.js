import React from 'react'
import Consts from 'consts'
import StripeSettingsContent from './stripeSettingsContent'

const StripeAdditionalVerificationRequired = ({ bankAccount } = {}) => {
  let bankName = null
  let last4 = null

  if (bankAccount && bankAccount.bank_name) {
    bankName = (
      <div>
        <strong>{bankAccount.bank_name.capitalizeAllFirstLetters().capitalizeWords(['N.a.', 'Na', 'Us'])}</strong>
      </div>
    )
  }

  if (bankAccount && bankAccount.last4) {
    last4 = (
      <div>
        {`Account: ${bankAccount.last4}`}
      </div>
    )
  }

  const content = (
    <>
      {bankName}
      {last4}
      <div>
        {'Status: '}
        <span style={{ color: '#F1C232', fontWeight: 'bold' }}>
          {' Additional Verification Required'}
        </span>
      </div>
      <div>
        {`Contact Swoop: ${Consts.SWOOP_CONTACT_NUMBER}`}
      </div>
    </>
  )

  return (
    <StripeSettingsContent content={content.props.children} />
  )
}

export default StripeAdditionalVerificationRequired
