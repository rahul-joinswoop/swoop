import React from 'react'
import StripeSettingsContent from './stripeSettingsContent'

const StripeLinkedBankAccount = ({ bankAccount } = {}) => {
  const content = (
    <div>
      <div className="bank-name">
        {bankAccount.bank_name.capitalizeAllFirstLetters().capitalizeWords(['N.a.', 'Na', 'Us'])}
      </div>
      <div className="bank-number">
        Account:
        {` ${bankAccount.last4}`}
      </div>
      <div>
        Status:&nbsp;
        <span className="bank-status">
          Linked
        </span>
      </div>
    </div>
  )

  return (
    <div className="stripe-linked-bank-account">
      <StripeSettingsContent content={content} />
    </div>
  )
}

export default StripeLinkedBankAccount
