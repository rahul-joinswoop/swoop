import React, { Component } from 'react'
import Consts from 'consts'
import CustomAccountStore from 'stores/custom_account_store'
import Dispatcher from 'lib/swoop/dispatcher'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Loading from 'componentLibrary/Loading'
import ModalStore from 'stores/modal_store'
import moment from 'moment-timezone'
import NotificationStore from 'stores/notification_store'
import UserStore from 'stores/user_store'
import { clone, extend } from 'lodash'
import StripeAdditionalVerificationRequired from './additionalVerificationRequired'
import StripeFinishLinkingBankAccount from './finishLinkingBankAccount'
import StripeLinkBankAccount from './linkBankAccount'
import StripeLinkedBankAccount from './linkedBankAccount'


const StripeBusinessAndPersonalInfoFormModal = React.createFactory(require('../modals/custom_account/stripe_business_and_personal_info_form'))
const StripeBankInfoFormModal = React.createFactory(require('../modals/custom_account/stripe_bank_info_form'))

class StripeSettings extends Component {
  constructor(props) {
    super(props)

    this.state = {
      custom_account: {
        business_name: UserStore.getUser().company.name,
        business_tax_id: UserStore.getUser().company.tax_id,
        first_name: UserStore.getUser().first_name,
        last_name: UserStore.getUser().last_name,
      },
      formToShow: null,
      business_address_loaded: false,
      showPersonalAndBusinessSavedNotification: false,
    }
  }

  componentDidMount() {
    CustomAccountStore.bind(CustomAccountStore.CHANGE, this.rerender)
    CustomAccountStore.loadGoogleAddressIfCompanyHasPlaceId()
  }

  componentWillUnmount() {
    CustomAccountStore.unbind(CustomAccountStore.CHANGE, this.rerender)
  }

  getComponent(customAccount) {
    if (!CustomAccountStore.hasLoaded()) {
      return (
        <Loading display="inline-block" />
      )
    } if (!customAccount || !customAccount.complete) {
      return <StripeLinkBankAccount handleOnClick={() => { this.setFormToShow('stripe_business_info_form', true) }} />
    } if (customAccount.verification_failure) {
      return <StripeAdditionalVerificationRequired bankAccount={customAccount.bank_account} />
    } if (customAccount.complete && !customAccount.linked) {
      return <StripeFinishLinkingBankAccount handleOnClick={() => { this.setFormToShow('stripe_bank_info_form') }} />
    } if (customAccount.complete && customAccount.linked) {
      return <StripeLinkedBankAccount bankAccount={customAccount.bank_account} />
    }
  }

  setFormToShow(formToShow) {
    this.setState({ formToShow })
  }

  closeModal() {
    Dispatcher.send(ModalStore.CLOSE_MODALS)
    this.setState({ formToShow: undefined })
  }

  continueToStripePersonalInfoForm(stripeCustomAccount) {
    Dispatcher.send(ModalStore.CLOSE_MODALS)

    this.setState({
      custom_account: extend(this.state.custom_account, stripeCustomAccount),
      formToShow: 'stripe_personal_info_form',
    })
  }

  postCustomAccountDataToBackend(stripeCustomAccount) {
    const postData = extend({}, this.state.custom_account, clone(stripeCustomAccount))

    const dateOfBirth = new Date(Number(moment(postData.date_of_birth)))
    delete postData.date_of_birth

    postData.dob_day = dateOfBirth.getDate()
    postData.dob_month = dateOfBirth.getMonth() + 1
    postData.dob_year = dateOfBirth.getFullYear()

    // here we add a custom success callback
    postData.callbacks.success = () => {
      Dispatcher.send(ModalStore.CLOSE_MODALS)

      this.setState({
        custom_account: extend(this.state.custom_account, stripeCustomAccount),
        formToShow: 'stripe_bank_info_form',
        showPersonalAndBusinessSavedNotification: true,
      })
    }

    postData.silent = true

    CustomAccountStore.addItem(postData)
  }

  registerBankAccountOnStripeApi(stripeBankAccount) {
    const stripe = Stripe(Consts.STRIPE_PUBLIC_KEY)

    function treatStripeReturn(stripeBankAccount) {
      return function (result) {
        if (result.error) {
          let errorResponse = {
            responseJSON: {
              bank_routing_number: [
                'Please check if this is correct',
              ],
              bank_account_number: [
                'Please check if this is correct',
              ],
            },
          }

          if (result.error.message.search(/Routing number/i) > -1) {
            errorResponse = {
              responseJSON: {
                bank_routing_number: [
                  result.error.message,
                ],
              },
            }
          } else if (result.error.message.search(/Account number/i) > -1) {
            errorResponse = {
              responseJSON: {
                bank_account_number: [
                  result.error.message,
                ],
              },
            }
          }

          return stripeBankAccount.callbacks.error(errorResponse)
        }

        CustomAccountStore.updateItem({
          token: result.token.id,
          silent: true, // to not display the default 'changed' message
          callbacks: {
            success: (remoteData) => {
              Dispatcher.send(NotificationStore.NEW_NOTIFICATION, {
                type: Consts.SUCCESS,
                message: 'Bank Account successfully linked',
                dismissable: true,
                timeout: 2000,
              })

              this.closeModal()
            },
          },
        })
      }
    }

    const treatResult = treatStripeReturn(stripeBankAccount)

    stripe.createToken('bank_account', {
      country: 'us',
      currency: 'usd',
      routing_number: stripeBankAccount.bank_routing_number,
      account_number: stripeBankAccount.bank_account_number,
      account_holder_name: `${this.state.custom_account.first_name} ${this.state.custom_account.last_name}`,
      account_holder_type: 'company',
    }).then(treatResult.bind(this))
  }

  // set state and force rerender asynchronously
  rerender() {
    let rev = 0

    if (this.state && this.state.rev) {
      rev = this.state.rev + 1
    }

    this.setState({
      rev,
    })

    if (!CustomAccountStore.getCustomAccount() && CustomAccountStore.getBusinessAddress() && !this.state.business_address_loaded) {
      this.setState({
        business_address_loaded: true,
        custom_account: extend({}, CustomAccountStore.getBusinessAddress(), this.state.custom_account),
      })
    }
  }

  showPersonalAndBusinessSavedNotification() {
    if (this.state.showPersonalAndBusinessSavedNotification) {
      Dispatcher.send(NotificationStore.NEW_NOTIFICATION, {
        type: Consts.SUCCESS,
        message: 'Personal and Business saved',
        dismissable: true,
        timeout: 2000,
      })

      this.setState({
        showPersonalAndBusinessSavedNotification: false,
      })
    }

    this.closeModal()
  }

  stripeBankInfoFormModal() {
    return StripeBankInfoFormModal({
      key: 'stripe_bank_info_form',
      show: this.state.formToShow === 'stripe_bank_info_form',
      onConfirm: this.registerBankAccountOnStripeApi,
      onCancel: this.showPersonalAndBusinessSavedNotification,
      record: this.state.custom_account,
    })
  }

  stripeBusinessAndPersonalInfoFormModal() {
    return StripeBusinessAndPersonalInfoFormModal({
      key: 'stripe_business_info_form',
      show: this.state.formToShow === 'stripe_business_info_form',
      onConfirm: this.postCustomAccountDataToBackend,
      onCancel: this.closeModal,
      record: this.state.custom_account,
    })
  }

  renderEditIcon = (customAccount) => {
    if (customAccount && customAccount.complete && customAccount.linked) {
      return (
        <FontAwesomeButton
          color="secondary-o"
          icon="fa-pencil"
          name="edit"
          onClick={() => { this.setFormToShow('stripe_bank_info_form', true) }}
          size="medium"
        />
      )
    }
    return null
  }

  render() {
    const customAccount = CustomAccountStore.getCustomAccount()
    const mainComponent = this.getComponent(customAccount)

    return (
      <div className="configure-component" id="bankinformation_in_companySettings">
        {this.stripeBusinessAndPersonalInfoFormModal()}
        {this.stripeBankInfoFormModal()}
        <h4>Bank Information {this.renderEditIcon(customAccount)}</h4>
        {mainComponent}
      </div>
    )
  }
}

export default StripeSettings
