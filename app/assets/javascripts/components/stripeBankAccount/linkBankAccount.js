import React from 'react'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import StripeSettingsContent from './stripeSettingsContent'

const StripeLinkBankAccount = ({ handleOnClick } = {}) =>
  <div>
    <StripeSettingsContent content={Consts.STRIPE_BANK_ACCOUNT.LINK} />
    <Button
      color="primary"
      onClick={handleOnClick}
      style={{
        display: 'block',
        margin: '10px 0',
      }}
    >
      Link a Bank Account
    </Button>
  </div>

export default StripeLinkBankAccount
