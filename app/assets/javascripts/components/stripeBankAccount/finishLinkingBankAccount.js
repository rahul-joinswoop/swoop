import React from 'react'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import StripeSettingsContent from './stripeSettingsContent'

const StripeFinishLinkingBankAccount = ({ handleOnClick } = {}) => {
  const content = (
    <div>
      <div>
        {Consts.STRIPE_BANK_ACCOUNT.FINISH_LINKING}
      </div>
      <div className="incomplete-label-container">
        {'Status:'}
        <span className="incomplete-label">
          {' Incomplete'}
        </span>
      </div>
    </div>
  )

  return (
    <div>
      <StripeSettingsContent content={content.props.children} />
      <Button
        color="primary"
        onClick={handleOnClick}
        style={{
          display: 'block',
          margin: '10px 0',
        }}
      >
        Finish Linking Bank Account
      </Button>
    </div>
  )
}

export default StripeFinishLinkingBankAccount
