import React from 'react'
import BaseComponent from 'components/base_component'
import PolicySearchSection from 'components/JobForm/PolicySearchSection'
import RetryPolicySearch from 'components/JobForm/RetryPolicySearch'
import VehiclePolicyLookup from 'components/JobForm/VehiclePolicyLookup'
import AsyncRequestStore from 'stores/async_request_store'
import Consts from 'consts'
import { includes } from 'lodash'

const SEARCH_TYPE = {
  loading: 'loading',
  retry: 'retry',
  select: 'select',
}

const VEHICLE_FIELDS = ['make', 'model', 'year', 'color', 'license', 'vin', 'unit_number']

const CUSTOMER_FULL_NAME = 'customer.full_name'
const CUSTOMER_PHONE = 'customer.phone'

const CUSTOMER_FORM_FIELDS = [CUSTOMER_FULL_NAME, CUSTOMER_PHONE]

const POLICY_CUSTOMER_FIELD = {}
POLICY_CUSTOMER_FIELD[CUSTOMER_FULL_NAME] = 'name'
POLICY_CUSTOMER_FIELD[CUSTOMER_PHONE] = 'phone_number'

const FORM_LOOKUP_TYPE_FIELDS = [
  'customer_lookup_type',
  'lookup_type_last_name',
  'lookup_type_phone_number',
  'lookup_type_policy_number',
  'lookup_type_unit_number',
  'lookup_type_vin',
  'lookup_type_zip',
]

class NewPolicySearch extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      searchContent: null,
      searchResults: null,
      searchString: null,
      selectedPolicyItem: null,
      coverageResults: null,
    }
  }

  componentDidMount() {
    super.componentDidMount()
    const { form } = this.props
    const record = form?.record
    if (record.pcc_coverage && record.customer_lookup_type !== Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL) {
      this.initializeSavedForm(form)
    }
    const shouldRunManualCoverageOnNewJobLoad = this.props.stores.SettingStore.getSettingByKey(Consts.COMPANY_SETTINGS.RUN_COVERAGE_MANUAL.RUN_COVERAGE_ON_NEW_JOB_FORM_LOAD, true)
    if (shouldRunManualCoverageOnNewJobLoad) {
      this.handleEnterManuallyClick()
    }

  }

  shouldComponentUpdate() {
    return true
  }

  componentDidUpdate() {
    this.clearCoverageWhenSelectManualLookUp()
    this.clearClientAPICoverageOnPolicyLookupChange()
  }

  lockSavedFields = (form) => {
    if (form?.fields) {
      VEHICLE_FIELDS.forEach((fieldName) => {
        if (form.fields[fieldName] && form.fields[fieldName].getValue() && form.fields[fieldName].getValue() !== '') {
          form.fields[fieldName].setToDisabled()
        }
      })

      const record = form?.record

      if (record.pcc_coverage.data[0].site_code) {
        form.fields.fleet_site_id?.setToDisabled()
      }
    }
  }

  initializeSavedForm = (form) => {
    this.lockSavedFields(form)
    this.disableSearchInputUnlessClientAPICovered(form)
    this.setState({
      selectedPolicyItem: 'loaded',
    })
  }

  clearCoverageWhenSelectManualLookUp = () => {
    const { form } = this.props
    const record = form?.record
    const { selectedPolicyItem } = this.state
    const { lastFieldChanged } = form
    if (lastFieldChanged === 'customer_lookup_type' && record.customer_lookup_type === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL && selectedPolicyItem) {
      this.clearSelectedPolicyItem()
    }
  }

  clearClientAPICoverageOnPolicyLookupChange = () => {
    const { form } = this.props
    const record = form?.record
    const { selectedPolicyItem } = this.state
    const { lastFieldChanged } = form
    const isClientAPICovered = record?.pcc_coverage?.data[0]?.name === Consts.COVERAGE.CLIENT_API_COVERED

    if (isClientAPICovered && lastFieldChanged === 'customer_lookup_type' && selectedPolicyItem) {
      this.clearSelectedPolicyItem()
    }
  }

  searchPolicies = (searchPayload) => {
    AsyncRequestStore.searchPoliciesNew(searchPayload.job_id, searchPayload.search_terms, {
      success: (data) => {
        if (data.error) {
          this.setState({ searchContent: SEARCH_TYPE.retry })
        } else {
          this.setState({
            searchContent: SEARCH_TYPE.select,
            searchResults: data.policies,
          })
        }
      },
      error: (error) => {
        this.setState({ searchContent: SEARCH_TYPE.retry })
        Rollbar.error('Error looking up coverage with api endpoint /policy/search: ', error)
      },
    })
  }

  handlePolicySearchClick = () => {
    const { form } = this.props
    const [searchPayload, searchString] = this.buildSearchPayload()

    this.setState({
      searchContent: SEARCH_TYPE.loading,
      searchString,
    })

    this.disableSearchInputUnlessClientAPICovered(form)
    this.searchPolicies(searchPayload)
  }

  buildSearchPayload = () => {
    const { form } = this.props
    const { record } = form

    const searchPayload = {
      job_id: record?.id,
      search_terms: {},
    }

    const lookupType = record?.customer_lookup_type
    let searchString = "Search for '"

    if (lookupType === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN) {
      searchPayload.search_terms.vin = record?.lookup_type_vin
      searchString += `${record?.lookup_type_vin}'`
    } else if (lookupType === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP) {
      searchPayload.search_terms.last_name = record?.lookup_type_last_name
      searchPayload.search_terms.postal_code = record?.lookup_type_zip
      searchString += `${record?.lookup_type_last_name} ${record?.lookup_type_zip}'`
    } else if (lookupType === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER) {
      searchPayload.search_terms.unit_number = record?.lookup_type_unit_number
      searchString += `${record?.lookup_type_unit_number}'`
    } else if (lookupType === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER) {
      searchPayload.search_terms.policy_number = record?.lookup_type_policy_number
      searchString += `${record?.lookup_type_policy_number}'`
    } else if (lookupType === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER) {
      searchPayload.search_terms.phone_number = record.lookup_type_phone_number
      searchString += `${record.lookup_type_phone_number}'`
    }

    return [searchPayload, searchString]
  }

  isButtonEnabled = () => {
    const { form } = this.props
    const { record } = form

    if (record) {
      switch (record.customer_lookup_type) {
        case Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP:
          return (form.record.lookup_type_last_name?.length > 0) && (form.record.lookup_type_zip?.length > 0)

        case Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER:
          return form.record.lookup_type_phone_number?.length > 0

        case Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER:
          return form.record.lookup_type_policy_number?.length > 0

        case Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER:
          return form.record.lookup_type_unit_number?.length > 0

        case Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN:
          return form.record.lookup_type_vin?.length > 0

        default:
          return false
      }
    }
  }

  isButtonVisible = () => {
    if (this.state.searchContent) {
      return false
    }
    const { form } = this.props
    const record = form?.record
    let isButtonEnabled = false
    if (record) {
      isButtonEnabled = record.customer_lookup_type && record.customer_lookup_type !== Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL
    }
    return isButtonEnabled
  }

  closeSearchContent = (enableSearchInputs = true) => {
    if (enableSearchInputs) {
      this.enableSearchInput()
    }
    this.setState({ searchContent: null })
  }

  handleEnterManuallyClick = () => {
    const { form } = this.props
    if (form?.fields) {
      form.fields.customer_lookup_type.setValue(Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL)
      this.props.rerender()
    }
    this.closeSearchContent()
  }

  fillFormWithSelectedPolicyItem = (policyItem) => {
    const { form, rerender } = this.props

    if (form?.fields) {
      const { policyVehicle, policyCustomer } = policyItem

      VEHICLE_FIELDS.forEach((fieldName) => {
        if (form.fields[fieldName] && policyVehicle[fieldName]) {
          let newValue = policyVehicle[fieldName]

          if (fieldName === 'color') {
            // e.g. "New awesomE blUE" => "New Awesome Blue"
            newValue = newValue.capitalizeAllFirstLetters()
          }

          if (fieldName === 'make') {
            // e.g. mcLaren => McLaren
            newValue = newValue.upcaseFirstLetters()
          }

          form.fields[fieldName].setValue(newValue)
          form.fields[fieldName].animate()
          form.fields[fieldName].setToDisabled()
        }
      })

      CUSTOMER_FORM_FIELDS.forEach((formFieldName) => {
        const customerField = form.fields[formFieldName]
        const policyFieldName = POLICY_CUSTOMER_FIELD[formFieldName]
        const policyCustomerFieldValue = policyCustomer[policyFieldName]

        if (customerField && policyCustomerFieldValue) {
          let newValue = policyCustomerFieldValue

          if (formFieldName === CUSTOMER_FULL_NAME) {
            newValue = newValue.capitalizeAllFirstLetters()
          }

          if (formFieldName === CUSTOMER_PHONE) {
            customerField.useMasking?.(false)
          }

          customerField.setValue(newValue)
          customerField.animate()
        }
      })

      rerender()
    }
  }

  setSelectedPolicyItem = (selectedPolicyItem, fillForm = false) => {
    this.setState({
      selectedPolicyItem,
    }, () => {
      this.props.setFormSelectedPolicyItem(selectedPolicyItem)
      if (fillForm) {
        this.fillFormWithSelectedPolicyItem(selectedPolicyItem)
      }
    })
  }

  handleVehicleListItemSelect = (selectedPolicyItem) => {
    if (selectedPolicyItem) {
      // PCC system in the BE stores the license as as license_number, whereas in job form it's only license.
      // We add the license attribute in case license_number exists so the field can be auto filled.
      selectedPolicyItem.policyVehicle.license = selectedPolicyItem.policyVehicle.license_number

      this.setSelectedPolicyItem(selectedPolicyItem, true)
    }

    this.closeSearchContent(false)
  }

  switchLookup = () => {
    const { form } = this.props
    const record = form?.record
    let retryOption = null
    if (record.customer_lookup_type === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN) {
      const customerLookupTypes = this.props.stores.SettingStore.getSettingByKey(Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.CUSTOMER_LOOKUP_TYPES, true)

      if (includes(customerLookupTypes, Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER)) {
        retryOption = Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER
      } else {
        retryOption = Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP
      }
    } else {
      retryOption = Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN
    }

    if (form?.fields) {
      form.fields.customer_lookup_type.setValue(retryOption)
      this.props.rerender()
    }
    this.closeSearchContent()
  }

  getSeachContent = () => {
    const { form } = this.props
    const record = form?.record
    const { searchContent, searchResults } = this.state
    if (searchContent === SEARCH_TYPE.loading) {
      return <VehiclePolicyLookup handleCloseClick={this.closeSearchContent} isLoading />
    } else if (searchContent === SEARCH_TYPE.select && searchResults && searchResults.length > 0) {
      return <VehiclePolicyLookup coverageOptions={searchResults} handleCloseClick={this.closeSearchContent} onHandleEnterManuallyClick={this.handleEnterManuallyClick} onHandleVehicleListItemSelect={this.handleVehicleListItemSelect} />
    } else if (searchContent === SEARCH_TYPE.retry || (searchContent === SEARCH_TYPE.select && searchResults?.length === 0)) {
      return <RetryPolicySearch currentLookup={record.customer_lookup_type} handleSwitchLookupClick={this.switchLookup} handleTryAgainClick={this.closeSearchContent} stores={this.props.stores} />
    }
    return null
  }

  disableSearchInputUnlessClientAPICovered = (form) => {
    const { record } = form

    // Client API Covered should allow users to change it at will, so we don't disable
    // the Lookup Type fields
    if (record?.pcc_coverage && record.pcc_coverage.data[0].name === Consts.COVERAGE.CLIENT_API_COVERED) {
      return
    }

    if (form?.fields) {
      FORM_LOOKUP_TYPE_FIELDS.forEach((lookupTypeField) => {
        form.fields[lookupTypeField].setToDisabled()
      })

      this.props.rerender()
    }
  }

  enableSearchInput = () => {
    const { form, rerender } = this.props
    if (form?.fields) {
      FORM_LOOKUP_TYPE_FIELDS.forEach((lookupTypeField) => {
        form.fields[lookupTypeField].setToEnabled()
      })

      rerender()
    }
  }

  clearSecretFields = (form, record) => {
    const secretFields = ['lookup_type_zip', 'lookup_type_last_name']
    secretFields.forEach((secretFieldKey) => {
      if (record[secretFieldKey] === Consts.SECRET_FIELD_DOTS) {
        form.fields[secretFieldKey]?.setValue('')
      }
    })
  }

  clearSelectedPolicyItem = () => {
    const { form } = this.props
    const { record } = form
    if (form?.fields) {
      this.setSelectedPolicyItem(null)
      record.pcc_coverage = null
      record.pcc_coverage_id = null
      VEHICLE_FIELDS.concat(CUSTOMER_FORM_FIELDS).forEach(fieldName => form.fields[fieldName]?.setToEnabled())
      form.fields.fleet_site_id?.setToEnabled()
      this.clearSecretFields(form, record)
      this.enableSearchInput()
    }
  }

  render() {
    super.render()
    const { form } = this.props
    const record = form?.record

    const isButtonVisible = this.isButtonVisible()
    return <PolicySearchSection
      handleClearSelectedCoverageOption={this.clearSelectedPolicyItem}
      isButtonEnabled={this.isButtonEnabled()}
      isButtonVisible={isButtonVisible}
      isClearButton={this.state.selectedPolicyItem}
      onHandlePolicySearchClick={this.handlePolicySearchClick}
      searchContent={this.getSeachContent()}
      searchString={this.state.searchString}
      showSearchString={!isButtonVisible && this.state.searchString &&
        (record.customer_lookup_type && record.customer_lookup_type !== Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL)}
    />
  }
}

NewPolicySearch.displayName = 'NewPolicySearch'
export default NewPolicySearch
