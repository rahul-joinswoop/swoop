import React from 'react'
import BaseComponent from 'components/base_component'
import Button from 'componentLibrary/Button'
import 'stylesheets/components/JobForm/PolicySearchSection.scss'

class PolicySearchSection extends BaseComponent {
  render() {
    super.render()
    const {
      handleClearSelectedCoverageOption, isButtonEnabled, isButtonVisible,
      isClearButton, onHandlePolicySearchClick, searchContent, searchString, showSearchString,
    } = this.props
    return (
      <div className="policy-search-section">
        <span className="button-container">
          { isButtonVisible &&
            <Button
              color={isButtonEnabled && !isClearButton ? 'green' : 'cancel'}
              disabled={!isButtonEnabled && !isClearButton}
              onClick={isClearButton ? handleClearSelectedCoverageOption : onHandlePolicySearchClick}
            >
              {isClearButton ? 'Clear' : 'Search'}
            </Button>}
        </span>
        { showSearchString &&
          <div className="search-information">
            {searchString}
          </div>}
        <span className="search-content">
          {searchContent}
        </span>
      </div>
    )
  }
}

PolicySearchSection.displayName = 'PolicySearchSection'
export default PolicySearchSection
