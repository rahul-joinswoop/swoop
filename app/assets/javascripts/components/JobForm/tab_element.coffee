TabElement = ({ children, filled, form, tabName }) ->
  style = {
    display: "inline-block"
    marginBottom: 3
    marginTop: 3
    padding: "2px 7px"
    cursor: "pointer"
    color: "#ccc"
  }
  if filled
    style.color = "#333"
  if form.isShowingTab("notes", tabName)
    style.borderBottom = "3px solid #7FBCE2"
    style.marginBottom = 0
    style.color = "#7FBCE2"
  span
    onClick: => form.setTab("notes", tabName)
    style: style
    children

export default TabElement
