import React from 'react'
import Loading from 'componentLibrary/Loading'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import { map, flatten } from 'lodash'
import PolicyItem from './PolicyItem'
import 'stylesheets/components/JobForm/VehiclePolicyLookup.scss'

const PolicyLookupBody = ({ coverageOptions, onHandleEnterManuallyClick, onHandleVehicleListItemSelect }) => (
  <div className="lookup-body">
    <span className="title">Select Vehicle</span>
    <ul>
      {flatten(map(coverageOptions, policy =>
        map(policy.pcc_vehicles_and_drivers_matrix, policyVehicleAndDriver => (
          <PolicyItem
            onHandleVehicleListItemSelect={onHandleVehicleListItemSelect}
            policyCustomer={policyVehicleAndDriver.pcc_driver}
            policyId={policy.id}
            policyNumber={policy.policy_number}
            policyVehicle={policyVehicleAndDriver.pcc_vehicle}
          />
        ))
      ))}

      <li
        className="vehicle-list-item"
        id="vehicle-not-found"
        key="vehicle-not-found"
        onClick={onHandleEnterManuallyClick}
      >
        <span className="message">Vehicle not found - Enter manually</span>
      </li>
    </ul>
  </div>
)

const VehiclePolicyLookup = ({
  coverageOptions, handleCloseClick, onHandleEnterManuallyClick, onHandleVehicleListItemSelect, isLoading,
}) => (
  <div className="vehicle-policy-lookup">
    <FontAwesomeButton
      color="light-grey"
      icon="fa-times"
      onClick={handleCloseClick}
      size="medium"
    />
    { isLoading ?
      <Loading display="inline-block" message="Looking up vehicles..." size={40} /> :
      <PolicyLookupBody
        coverageOptions={coverageOptions}
        onHandleEnterManuallyClick={onHandleEnterManuallyClick}
        onHandleVehicleListItemSelect={onHandleVehicleListItemSelect}
      />}
  </div>
)

VehiclePolicyLookup.displayName = 'VehiclePolicyLookup'
export default VehiclePolicyLookup
