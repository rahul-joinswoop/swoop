import React, { useEffect, useState } from 'react'
import { first, pick } from 'lodash'
import { DirectionsRenderer, useGoogleMap } from '@react-google-maps/api'
import { RouteMarker } from 'components/maps/map_new'
import { getDirections } from 'lib/google-maps'
import InfoMarker from 'components/maps/InfoMarker'
import { fitBounds } from 'components/maps/Utils'

const DropMarker = ({ dest, destMarker, distance }) => (
  <InfoMarker
    icon={{
      url: destMarker,
      labelOrigin: new google.maps.Point(13, 15),
    }}
    markerProps={{
      animation: 2,
      label: {
        color: '#FFF',
        text: 'B',
      },
    }}
    onClick={() => {}}
    onMouseOut={({ toggleInfoWindow }) => toggleInfoWindow(false)}
    onMouseOver={({ toggleInfoWindow }) => toggleInfoWindow(true)}
    position={pick(dest, 'lat', 'lng')}
  >
    {distance && <div>{distance.text}</div>}
  </InfoMarker>
)

const Route = ({ origin, dest, setDistance }) => {
  const [directions, setDirections] = useState(null)

  const directionsCallback = (result, status) => {
    if (status === google.maps.DirectionsStatus.OK) {
      setDirections(result)

      // in our case we always have only one route and one leg
      const route = first(result.routes)
      const leg = first(route.legs)
      setDistance(leg.distance)
    } else {
      Rollbar.warning('Got bad response back looking up directions', { status, origin, dest })
    }
  }

  useEffect(
    () => {
      if (origin && dest) {
        getDirections(origin, dest, directionsCallback)
      }
    },
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
    [dest.lat, dest.lng, origin?.lat, origin?.lng]
  )

  return directions && <DirectionsRenderer
    options={{
      directions,
      preserveViewport: true,
      suppressInfoWindows: false,
      suppressMarkers: true,
      polylineOptions: {
        strokeColor: '#90BAD4',
      },
    }}
  />
}

const JobRoute = ({
  origin, originMarker, dest, destMarker, setLastDistance,
}) => {
  const map = useGoogleMap()
  const [distance, setDistance] = useState(null)

  useEffect(
    () => {
      fitBounds(map, [origin, dest], { top: 70 })
    },
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
    [map, dest?.lat, dest?.lng, origin?.lat, origin?.lng]
  )

  return (
    <>
      {origin?.lat && origin?.lng && <RouteMarker
        icon={{
          url: originMarker,
          labelOrigin: new google.maps.Point(13, 15),
        }}
        label="A"
        routeNode={origin}
      />}
      {dest?.lat && dest?.lng && <DropMarker
        dest={dest}
        destMarker={destMarker}
        distance={distance}
      />}
      {origin?.lat && origin?.lng && dest?.lat && dest?.lng && <Route
        dest={dest}
        origin={origin}
        setDistance={(routeDistance) => {
          setDistance(routeDistance)
          setLastDistance(routeDistance.text.slice(0, -3))
        }}
      />}
    </>
  )
}

export default JobRoute
