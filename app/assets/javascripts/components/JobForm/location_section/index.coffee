import React from 'react'
import ReactDOMFactories from 'react-dom-factories'
import { extend, keys } from 'lodash'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import { withSplitIO } from 'components/split.io'
import ContactWidget from './contact_widget'
import SwoopMapNew from 'components/maps/SwoopMap'
import { SiteMarkers } from 'components/maps/map_new'
import JobRoute from './JobRoute'
import { fitBounds } from 'components/maps/Utils'
LocationWidget = React.createFactory(require('./location_widget').default)
FormSection = React.createFactory(require('components/form_section'))
SwoopMap = React.createFactory(require('components/maps/map'))

RequestPopup = () ->
  div
    style:
      position: "absolute"
      top: "50%"
      width: "100%"
      textAlign: "center"
      zIndex: 1
    span
      style:
        backgroundColor: "rgba(255,255,255,.9)"
        borderRadius: 3
        padding: 5
      ReactDOMFactories.i
        className: "fa fa-check"
        style:
          fontSize: 18
          position: "relative"
          top: 2
      span null,
        "Request will be sent after creating job."

SMSRequestLocation = ({ form, onChange }) ->
  if !form.record.get_location_sms
    form.renderInput("get_location_sms_button", {
      style: extend({}, Styles.buttonStyle, (if form.record.second_tow then Styles.onButtonStyle))
      onChange: onChange
    })
  else
    span
      style:
        padding: "5px 10px 5px 5px"
        display: "inline-block"
      ReactDOMFactories.i
        className: "fa fa-check"
        style:
          marginRight: 4
      span null,
        if form.record.get_location_attempts? and form.record.get_location_attempts > 0
          if form.record.original_service_location?
            "Location updated"
          else
            "Location Request Sent"
        else
          "Request will be sent"

ClickInstructions = () ->
  div
    className: "click_instructions"
    style:
      pointerEvents: "none"
      position: "absolute"
      bottom: "10%"
      width: "100%"
      textAlign: "center"
      zIndex: 1
    span
      style:
        borderRadius: 4
        padding: "5px 8px"
        backgroundColor: "rgba(86,86,86,0.80)"
        color: "#FFF"
      span null,
        "Click in a location field above to drop a pin"

ShowSitesAndPlaces = ({ hide }) ->
  div
    className: 'add_all_sites_and_places_pin'
    div
      className: 'sites_and_placess_tooltip'
      'Show Sites & Places'
    div
      style:
        cursor: 'pointer'
      onClick: hide
      ReactDOMFactories.i
        className: 'fa fa-map-marker'
        'aria-hidden': 'true'
        style:
          fontSize: '1.7em'
          color: 'white'
          marginTop: '5px'

class LocationSection extends BaseComponent
  shouldShowAddAllSitesAndPlacesPin: ->
    { PoiStore, SiteStore, UserStore } = @props

    if UserStore.isSwoop()
      return false
    else
      sitesPlacesCount = SiteStore.getFilteredSites(null, @getId()).length + keys(PoiStore.loadAndGetAll(@getId())).length
      # return this bool value
      return sitesPlacesCount > Consts.SITES_AND_PLACES_LOAD_THRESHOLD and !@state.hideAllSitesAndPlacesPin

  getLocationCacheBreak: () ->
    JSON.stringify(@props.form?.drop_location_information?.dropOffSites)

  getContactWidgetCacheBreaker: (type, include_sms) ->
    { form } = @props
    cacheBreak = form.record?[type+"_contact"]?.full_name + form.record?[type+"_contact"]?.phone
    cacheBreak += form.submit_attempts
    if include_sms
      cacheBreak += form.record?.customer?.full_name + form.record?.customer?.phone
      cacheBreak += (form.record?.review_sms || form.record?.partner_sms_track_driver || form.record?.text_eta_updates || form.record?.get_location_sms) + form.record?.send_sms_to_pickup

    cacheBreak

  onSMSRequestLocation: =>
    if @state?.showRequestPopupTimeout?
      clearTimeout(@state.showRequestPopupTimeout)

    @setState
      showRequestPopup: true
      showRequestPopupTimeout: setTimeout(=>
        @setState
          showRequestPopup: false
      , 4000)

  onClick: (lat, lng, fixedOption = null) =>
    @setState
      showRequestPopup: false
    @pickup?.mapClicked(lat, lng, fixedOption)
    @dropoff?.mapClicked(lat, lng, fixedOption)

  renderMap: =>
    { form, PoiStore, setRef, SiteStore, treatments, UserStore } = @props

    routes = null
    if form.record.service_location? || form.record.drop_location?
      routes = [
        {
          id: if form.record.id? then form.record.id else "new_job"
          origin: form.record.service_location
          dest: form.record.drop_location
          originId: "origin"
          destId: "dest"
          originMarker: "/assets/images/marker_green.png"
          destMarker: "/assets/images/marker_red.png"
        }
      ]

    # not passing any property on purpose to not trigger any update on the map on the fly
    sites = []
    poiSites = []
    if not UserStore.isSwoop()
      sites = SiteStore.getFilteredSites(null, @getId())
      poiSites = PoiStore.loadAndGetAll(@getId())

    mapOptions =
      disableDefaultUI: true
      zoomControl: true
      zoom: 13
      styles: [{ featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }]}]

    if not treatments?[Consts.SPLITS.DISABLE_NEW_JOB_FORM_MAP]
      <SwoopMapNew
        onBoundsChanged={() =>
          @setState
            showRequestPopup: false
            bounds: @map.getBounds()
        }
        onClick={(e) =>
          @onClick(e.latLng.lat(), e.latLng.lng(), e)
        }
        onLoad={(map) =>
          map.setOptions(mapOptions)

          if not fitBounds(map, [form.record.service_location, form.record.drop_location], { top: 70 })
            user = UserStore.getUser()
            if user?.company?.location?
              map.setCenter(user.company.location)

          @map = map
          setRef({
            setOptions: (options) =>
              map.setOptions(options)
            getLastDistanceInMiles: =>
              @distance
          })
        }
        style={
          height: '300px'
          width: '100%'
        }
      >
        {routes && <JobRoute
          setLastDistance={(distance) => @distance = distance}
          {...routes[0]}
        />}
        <SiteMarkers
          onClick={@onClick}
          poiSites={poiSites}
          sites={sites}
          useSitesAndPlacesTreshold={@shouldShowAddAllSitesAndPlacesPin()}
        />
      </SwoopMapNew>
    else
      SwoopMap
        sites: sites
        poiSites: poiSites
        useSitesAndPlacesTreshold: @shouldShowAddAllSitesAndPlacesPin()
        width: "100%"
        height: "300px"
        paddingTop: 70
        ref: (map) =>
          @map = map
          setRef(map)
        mapOptions: mapOptions
        routes: routes
        showDistanceOnDropLocation: true
        onDistanceCalculated: @rerender
        onClick: @onClick
        onBoundsChanged: (bounds) =>
          @setState
            showRequestPopup: false
            bounds: bounds

  render: =>
    { FeatureStore, form, formSectionTextStyle, UserStore } = @props

    FormSection
      title: "Location"
      textStyle: formSectionTextStyle
      headerView: div
        style:
          float: "right"
        if FeatureStore.isFeatureEnabled("SMS_Request Location")
          <SMSRequestLocation form={form} onChange={@onSMSRequestLocation} />
      div
        className: "job_map_container"
        style: Styles.jobMapContainer
        if @state.showRequestPopup
          <RequestPopup />
        if !@pickup?.state?.focused and !@dropoff?.state?.focused
          <ClickInstructions />
        if @shouldShowAddAllSitesAndPlacesPin()
          <ShowSitesAndPlaces
            hide={=>
              @setState
                hideAllSitesAndPlacesPin: true}
          />
        div
          style:
            cursor: "pointer"
            position: "absolute"
            top: 0
            left: 0
            width: "100%"
            margin: 5
            zIndex: 1

          LocationWidget
            form: form
            ref: (input) =>
              @pickup = input?.instanceRef
            submit_attempts: form.submit_attempts
            location: form.record?.service_location
            location_type_id: form.record?.service_location?.location_type_id
            service: form.record?.service
            letter: "A"
            type_attr: "service_location.location_type_id"
            address_attr: "service_location"
            bounds: @state.bounds
            map: @map
            forceChange: @rerender
            type_required: UserStore.isFleet()
            selectedCursor: "url('/assets/images/marker_green_faded.png') 13 37, auto"
            badgeColor: "rgb(52,174,64)"

          LocationWidget
            form: form
            letter: "B"
            location: form.record?.drop_location
            location_type_id: form.record?.drop_location?.location_type_id
            submit_attempts: form.submit_attempts
            service: form.record?.service
            cacheBreak: @getLocationCacheBreak()
            ref: (input) =>
              @dropoff = input?.instanceRef
            forceChange: @rerender
            type_attr: "drop_location.location_type_id"
            address_attr: "drop_location"
            bounds: @state.bounds
            map: @map
            selectedCursor: "url('/assets/images/marker_red_faded.png') 13 37, auto"
            badgeColor: "rgb(234,0,0)"

        @renderMap()

      <ContactWidget
        form={form}
        letter="A"
        attr="pickup"
        label="Pickup Contact"
        copy
        cacheBreaker={@getContactWidgetCacheBreaker("pickup", true)}
      />
      <ContactWidget
        form={form}
        attr="dropoff"
        label="Drop Off Contact"
        letter="B"
        cacheBreaker={@getContactWidgetCacheBreaker("dropoff")}
      />

Styles = {
  buttonStyle : {
    backgroundColor: "#AAAAAA"
    height: 20
    color: "white"
    borderRadius: 3
    margin: 4
    padding: "0 5px"
    border: "1px solid #AAA"
  }
  jobMapContainer : {
    width: "calc(100% + 20px)"
    marginLeft: -10
    marginTop: -10
    marginBottom: 10
    position: "relative"
    zIndex: 3
  }
  onButtonStyle : {
    backgroundColor: "#FFF"
    color: "#AAA"
  }
}

export default withSplitIO(LocationSection)
