import ReactDOMFactories from 'react-dom-factories'
import { extend } from 'lodash'
import BaseComponent from 'components/base_component'

class ContactWidget extends BaseComponent
  displayName: 'ContactWidget'
  render: ->
    form = @props.form
    div
      style:
        margin: "0 10px"
      div null,
        label
          style:
            display: "inline-block"
            width: "auto"
          @props.label
        if @props.copy?
          form.renderInput("copy_customer", style: extend({}, Styles.buttonStyle, (if !form.getField("copy_customer")?.canCopy() then Styles.onButtonStyle)))
      div
        style:
          position: "relative"
        div
          style:
            borderRadius: 10
            backgroundColor: "#81BDE0"
            width: 20
            height: 20
            textAlign: "center"
            color: "white"
            position: "absolute"
            zIndex: 1
            top: 8
            left: 8
          @props.letter
        ReactDOMFactories.i
          className: "fa fa-user"
          style:
            borderRadius: 10
            color: "#E2E2E2"
            position: "absolute"
            top: 8
            zIndex: 1
            left: 32
            fontSize: 20
        ReactDOMFactories.i
          className: "fa fa-phone"
          style:
            borderRadius: 10
            color: "#E2E2E2"
            zIndex: 1
            position: "absolute"
            top: 8
            fontSize: 20
            left: "calc(50% + 8px)"
        form.renderInput(@props.attr+"_contact.full_name",
          containerStyle:
            width: "50%"
          style:
            borderRadius: "5px 0 0 5px"
            borderRight: 0
            paddingLeft: 50
          labelStyle:
            display: "none"
        )
        form.renderInput(@props.attr+"_contact.phone",
          containerStyle:
            width: "50%"
          style:
            borderRadius: "0 5px 5px 0"
            paddingLeft: 25
          labelStyle:
            display: "none"
        )

Styles = {
  buttonStyle : {
    backgroundColor: "#AAAAAA"
    height: 20
    color: "white"
    borderRadius: 3
    margin: 4
    padding: "0 5px"
    border: "1px solid #AAA"
  }
  onButtonStyle : {
    backgroundColor: "#FFF"
    color: "#AAA"
  }
}

export default ContactWidget
