import $ from 'jquery'
import { includes } from 'lodash'
import onClickOutside from 'react-onclickoutside'
import Api from 'api'
import BaseComponent from 'components/base_component'

class LocationWidget extends BaseComponent
  displayName: 'LocationWidget'
  constructor: (props) ->
    super(props)
    @state = {}
    @clicked_times = 0

  mapClicked: (lat, lng, fixedOption) =>
    if @state?.focused
      if @props.form.fields[@props.address_attr].isEnabled()
        @clicked_times++
        Api.track("clicked", @props.address_attr, @clicked_times)
        @props.form.fields[@props.address_attr].setLatLng(lat, lng, fixedOption)

  handleClickOutside: (e) =>
    #Only allow map clicking
    if $(e.target).closest(".GMap").length > 0
      return
    if @state?.focused
      @setState
        focused: false
      setTimeout(@props?.forceChange, 100)
      @props?.map?.setOptions({draggableCursor: undefined})

  render: ->
    super(arguments...)

    badgeColor = @props.badgeColor || "#81BDE0"

    form = @props.form
    letterStyle = {
      borderRadius: 10
      width: 20
      marginTop: 5
      marginLeft: 5
      height: 20
      color: "white"
      backgroundColor: badgeColor
    }

    div
      style:
        verticalAlign: "top"
        position: "relative"
      onClick: (e) =>
        previouslyFocused = @state?.focused
        if not previouslyFocused
          @setState
            focused: true
          setTimeout(@props?.forceChange, 0)
          @props?.map?.setOptions({draggableCursor: @props.selectedCursor})
        else
          if includes(e.target.classList, 'location-widget-letter')
            @setState
              focused: false
            setTimeout(@props?.forceChange, 100)
            @props?.map?.setOptions({draggableCursor: undefined})
      div
        className: "location-widget-letter"
        style:
          backgroundColor: "white"
          border: "1px solid rgb(226, 226, 226)"
          borderColor: if @state.focused then "rgb(127, 188, 226)" else "rgb(226, 226, 226)"
          width: 32
          height: 32
          display: "inline-block"
          borderRadius: 3
          marginTop: 2
          textAlign: "center"
          verticalAlign: "top"
          zIndex: 1
        div
          className: "location-widget-letter"
          style: letterStyle
          @props.letter
      @props.form.renderInput(@props.type_attr,
        containerStyle: Styles.location_type.containerStyle
        style: Styles.location_type.style
        labelStyle: "display": "none"
      )
      @props.form.renderInput(@props.address_attr,
        callback_on_enter: (suggests, userInput, geometry) =>
          if geometry.viewport?
            @props.map.fitBounds(geometry.viewport, true)
          else
            @props.map.setCenter(geometry.location.lat(), geometry.location.lng())
            @props.map.setZoom(15)
        bounds: @props.bounds
        containerStyle:
          width: "calc(65% - 10px)"
          marginLeft: 5
          marginTop: 1
          marginBottom: 2
          verticalAlign: "top"
        labelStyle: "display": "none"
        style:[
          {margin: 0},
          if @state.focused and @props.form.fields[@props.address_attr].isEnabled() then Styles.glowStyle
        ]
      )

Styles = {
  glowStyle : {
    boxShadow: "0px 0px 5px 3px #81BDE0"
  }
  location_type: {
    containerStyle:
      width: "calc(35% - 42px)"
      marginBottom: 2
      marginLeft: 5
      marginTop: 1
      verticalAlign: "top"
    style:
      borderRadius: "5px 0 0 5px"
      verticalAlign: "top"
  }
}

export default onClickOutside(LocationWidget)
