import { Component } from 'react'
import $ from 'jquery'
import ReactDOMFactories from 'react-dom-factories'
import { get } from 'lodash'
import onClickOutside from 'react-onclickoutside'
import Consts from 'consts'

class SmsOptions extends Component
  displayName: 'SmsOptions'

  handleClickOutside: (e) ->
    if $(e.target).closest(".downarrow")[0] != @refs.container
      @setState
        open: false

  render: ->
    { FeatureStore, form } = @props

    div
      className: "downarrow"
      ref: "container"
      style:
        width: 50
        marginRight: 5
        backgroundColor: "#FFF"
        float: "left"
        borderRadius: 5
      div
        style:
          padding: "0 8px"
          cursor: "pointer"
        onClick: =>
          @setState
            open: !@state?.open
        ReactDOMFactories.i
          className: "fa fa-mobile"
          'aria-hidden': true
          style:
            color: "#AAAAAA"
            fontSize: 34
      if @state?.open and form?.record?
        div
          style:
            position: "absolute"
            bottom: "37px"
            zIndex: 999
            backgroundColor: "white"
            borderRadius: "4px"
            border: "1px solid #E2E2E2"
            boxShadow: "0px 2px 4px 0px rgba(0,0,0,0.23)"
            whiteSpace: "nowrap"
            padding: 20
          form.renderInput("send_sms_to_pickup",
            labelStyle:
              color: "#ccc"
              fontSize: 10
              textTransform: "uppercase"
              fontWeight: "bold"
            containerStyle:
              paddingBottom: 10
              marginBottom: 10
              borderBottom: "1px solid #ccc"

          )
          div
            className: "send_options"
            if FeatureStore.isFeatureEnabled("SMS_Request Location")
              div
                className: "option"
                if form.record.get_location_attempts? and form.record.get_location_attempts > 0
                  span null,
                    ReactDOMFactories.i
                      className: "fa fa-check"
                    span null,
                      if form.record.original_service_location?
                        "Location updated"
                      else
                        "Location Request Sent"
                else
                  form.renderInput("get_location_sms")
            if FeatureStore.isFeatureEnabled("SMS_ETA Updates")
              div
                className: "option"
                form.renderInput("text_eta_updates")
            if FeatureStore.isFeatureEnabled("SMS_Track Driver")
              div
                className: "option"
                if get(form.record, "track_technician_sent_at")?
                  span null,
                    ReactDOMFactories.i
                      className: "fa fa-check"
                    "Driver tracking link sent"
                else
                  form.renderInput("partner_sms_track_driver")
            if FeatureStore.isFeatureEnabled("SMS_Reviews") || FeatureStore.isFeatureEnabled(Consts.FEATURES_REVIEW_NPS_SURVEY)
              div
                className: "option"
                if form.record.review?
                  span null,
                    ReactDOMFactories.i
                      className: "fa fa-check"
                    if form.record.review.rating?
                      "Review updated"
                    else
                      "Review request sent"
                else
                  form.renderInputs([
                    ["review_sms", {}],
                    ["review_delay", containerStyle: { marginBottom: 0, paddingTop: 10, marginTop: 10, borderTop: "1px solid #ccc"}, labelStyle: {display: "inline"}, style: {marginLeft: 10, display: "inline", width: 150}]
                  ])

export default onClickOutside(SmsOptions)
