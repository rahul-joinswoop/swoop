import React from 'react'
import BaseComponent from 'components/base_component'

class PolicyItem extends BaseComponent {
  onHandleItemSelect = () => {
    const { policyId, policyVehicle, policyCustomer } = this.props

    this.props.onHandleVehicleListItemSelect({
      policyId, policyVehicle, policyCustomer,
    })
  }

  render() {
    super.render()

    const {
      policyId, policyVehicle, policyCustomer, policyNumber,
    } = this.props

    const formattedCustomerName = policyCustomer?.name ? policyCustomer.name.capitalizeAllFirstLetters() : ''

    return (
      <li
        className="vehicle-list-item"
        id={policyId}
        key={policyId}
        onClick={this.onHandleItemSelect}
      >
        { policyNumber && <span className="policy-number" id={policyId}>{policyNumber}</span> }
        <span className="vin" id={policyId}>{policyVehicle.vin}</span>
        <span className="car" id={policyId}>
          {`${policyVehicle.year || ''}
            ${policyVehicle.make ? policyVehicle.make.capitalizeAllFirstLetters() : ''}
            ${policyVehicle.model ? policyVehicle.model.capitalizeAllFirstLetters() : ''}
            ${policyVehicle.color ? policyVehicle.color.capitalizeAllFirstLetters() : ''}`}
        </span>
        <span className="name" id={policyId}>{formattedCustomerName}</span>
        <span className="zip" id={policyId}>{policyCustomer.postal_code || ''}</span>
      </li>
    )
  }
}

PolicyItem.displayName = 'PolicyItem'
export default PolicyItem
