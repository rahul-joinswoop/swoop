import React from 'react'
import Consts from 'consts'
import Button from 'componentLibrary/Button'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import { includes } from 'lodash'
import 'stylesheets/components/JobForm/RetryPolicySearch.scss'

const RetryPolicySearch = ({
  currentLookup, handleTryAgainClick, handleSwitchLookupClick, stores,
}) => {
  let retryOption = null

  if (currentLookup === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN) {
    // we use the stores loaded by JobForm, so when a Swoop user is logged in it can correctly fetch the current loadaded company in the form
    // @see https://github.com/joinswoop/swoop/blob/master/app/assets/javascripts/components/job_form.coffee#L621-L622
    const customerLookupTypes = stores.SettingStore.getSettingByKey(Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.CUSTOMER_LOOKUP_TYPES, true)

    if (includes(customerLookupTypes, Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER)) {
      retryOption = Consts.CUSTOMER_LOOKUP_RETRY.UNIT_NUMBER
    } else {
      retryOption = Consts.CUSTOMER_LOOKUP_RETRY.LAST_NAME_AND_ZIP
    }
  } else {
    retryOption = Consts.CUSTOMER_LOOKUP_RETRY.VIN
  }

  return (
    <div className="retry-policy-search">
      <FontAwesomeButton
        color="light-grey"
        icon="fa-times"
        onClick={handleTryAgainClick}
        size="medium"
      />
      <span className="no-results-container">
        <i className="fa fa-exclamation-triangle" />
        <span className="message">No Results</span>
      </span>
      <span className="button-section">
        <Button
          color="cancel"
          label="Try Again"
          onClick={handleTryAgainClick}
        />
        <Button
          color="primary"
          label={retryOption}
          onClick={handleSwitchLookupClick}
        />
      </span>
    </div>
  )
}

RetryPolicySearch.displayName = 'RetryPolicySearch'
export default RetryPolicySearch
