import { keys, sortBy } from 'lodash'
import moment from 'moment-timezone'
import { HistoryFactory } from 'job_fields'

HistoryWidget = ({ form, release }) ->
  history = form.record.history
  historyKeys = sortBy(keys(history), (key) -> history[key].sort_date)

  div
    style:
      marginTop: 10
    for status in historyKeys
      if form.record.history[status].type not in ['SMS', 'TOA', 'Edited', 'Emailed']
        field = form.register(HistoryFactory(form.record.history[status], status), "history."+status+".adjusted_dttm")
        form.renderInput(field._name, Styles.history)
    if release and !form.orig_record.history.Released?
      if !form.record.release_date?
        time = moment().toISOString()
        form.record.release_date = time
        form.orig_record.history.release_date = time
      form.renderInput("release_date", Styles.history)


Styles = {
  history: {
    containerStyle: {
      marginTop: 0
      marginLeft: 10
      marginRight: 10
      verticalAlign: "top"
      marginBottom: 3
      width: "calc(100% - 20px)"
    }
    labelStyle: {
      display: "inline-block"
      width: "30%"
      verticalAlign: "middle"
    }
    style: {
      display: "inline-block"
      width: "calc(70%)"
      verticalAlign: "middle"
    }
  }
}

export default HistoryWidget
