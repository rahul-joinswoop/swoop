import React from 'react'
import Consts from 'consts'
import FormSection from 'components/form_section'
import Loading from 'componentLibrary/Loading'
import AsyncRequestStore from 'stores/async_request_store'
import Button from 'componentLibrary/Button'
import Utils from 'utils'
import { map, clone, filter, find, isEmpty, isEqual, pull } from 'lodash'
import 'stylesheets/components/JobForm/CoverageSectionContainer.scss'
import CompanyStore from 'stores/company_store'

const DEBOUNCED_FIELDS = ['odometer']
const DEBOUNCE_DELAY = 750
const NO_MISSING_FIELDS_COVERAGE_ERROR_DELAY = 5000
const REQUIRED_FIELD_KEY_TO_WORDS_MAP = {
  odometer: 'Odometer',
  year: 'Vehicle Year',
  service: 'Service',
  drop_location: 'Drop Off Location',
  service_location: 'Pickup Location',
}

const coverageResultValueToEnglish = (result) => {
  if (result === 'Covered') {
    return 'Fully Covered'
  } else if (result === 'NotCovered') {
    return 'Not Covered'
  } else if (result === 'PartiallyCovered') {
    return 'Partially Covered'
  }
  return result
}

const CoverageResult = ({ coverage }) => (
  <div className="coverage-result">
    <div className="result-and-program row-section">
      <span className="column coverage">
        <div className="title">Coverage</div>
        <div className="icon-and-message">
          <i className={`fa ${coverage.result === 'Covered' ? 'fa-check-circle' : 'fa-exclamation-triangle'}`} />
          <span className="message">{coverageResultValueToEnglish(coverage.result)}</span>
        </div>
      </span>
      <span className="column program">
        <div className="title">Program</div>
        <div>{coverage.name || 'Unknown'}</div>
      </span>
    </div>
    {
      coverage.result !== 'Covered' &&
      <div className="coverage-issue row-section">
        <span className="column issue">
          <div className="title">Coverage Issue</div>
          {
            <ul>
              {map(coverage.coverage_issues, issue => <li key={issue}>{issue}</li>)}
            </ul>
          }
        </span>
        <span className="column customer-payment">
          <div className="title">Customer Payment</div>
          {
            <div className="message">
              {
                coverage.result === 'NotCovered' ? 'Customer pays full cost of service' :
                  (
                    <ul>
                      {map(coverage.limitations, payment => <li key={payment}>{payment}</li>)}
                    </ul>
                  )
              }
            </div>
          }
        </span>
      </div>
    }
    {
      coverage.coverage_notes && !isEmpty(coverage.coverage_notes) &&
      <span className="notes-section">
        <div className="title">Coverage Notes</div>
        <div className="notes">{coverage.coverage_notes}</div>
      </span>
    }
  </div>
)

const MissingFieldList = ({ missingFields, programName }) => (
  <div className="missing-field-section row-section">
    <span className="column missing-fields">
      <div className="title">Missing Coverage Fields</div>
      <ul>
        {map(missingFields, fieldKey => <li key={fieldKey}>{REQUIRED_FIELD_KEY_TO_WORDS_MAP[fieldKey]}</li>)}
      </ul>
    </span>
    <span className="column program">
      <div className="title">Program</div>
      <div>{programName}</div>
    </span>
  </div>
)

const CoverageError = ({ retryCoverageCall }) => (
  <div className="error-message-container">
    <i className="fa fa-exclamation-triangle" />
    <div className="message">
      <span>Coverage lookup temporarily unavailable.</span>
      <span>Proceed as Fully Covered.</span>
    </div>
    <Button color="cancel" onClick={retryCoverageCall}>Retry</Button>
  </div>
)

const LookupCoverageMessage = () => <div className="default-message">Lookup customer for coverage details</div>

const CoverageLoading = () => <Loading display="inline-block" message="Looking up coverage details..." size={40} />

class CoverageSectionContainer extends React.Component {
  constructor(props) {
    super(props)
    const { form } = props
    const { record } = form
    let coverage = null
    if (record?.pcc_coverage) {
      coverage = record.pcc_coverage.data[0] ? record.pcc_coverage.data[0] : record.pcc_coverage.data
      record.coverage = coverage
      form.renderInput('drop_location')
    }
    this.state = {
      coverage,
      isLoading: false,
      coverageError: false,
    }
    this.initialPCCCoverageId = !!record.pcc_coverage_id
    this.debouncedCoverageRequest = null
    this.savedLastFieldChangedValue = null
    this.savedLastFieldChanged = null
    this.noAPIResponseTimeout = null
    this.initialCoverageRequestMade = !!record.pcc_coverage
    this.previousPayload = null
  }

  componentDidUpdate(prevProps) {
    if (this.shouldRunCoverageOnNewJobFormLoad()) {
      this.updateCoverage(prevProps)
    } else {
      this.setCompleteFormCoverageError()
      this.updateCoverage(prevProps)
      this.setCoverageIncompleteError()
    }
  }

  shouldRunCoverageOnNewJobFormLoad = () => {
    const runCoverageOnNewJobFormLoad = !!this.props.stores?.SettingStore?.getSettingByKey(
      Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.RUN_COVERAGE_ON_NEW_JOB_FORM_LOAD
    )

    const { record } = this.props.form
    const jobWithoutCoverage = !record.pcc_coverage

    return runCoverageOnNewJobFormLoad && jobWithoutCoverage
  }

  // This error is set when there is a a coverage result 'Failed', a successful vin/zip or manual policy lookup, and there are no missing coverage fields.
  // We wait 5 seconds to show the Retry screen to make sure a valid coverage result has a chance to come from BE and there is no flicker
  setCompleteFormCoverageError = () => {
    if (this.isCompleteFormCoverageError) {
      clearTimeout(this.isCompleteFormCoverageErrorTimeOut)
      this.isCompleteFormCoverageErrorTimeOut = setTimeout(() => {
        // check conditions for showing this error again after 5 seconds
        if (!this.state.coverageError && this.getIsCompleteFormCoverageError()) {
          this.setState({
            isLoading: false,
            coverageError: true,
          })
        }
      }, NO_MISSING_FIELDS_COVERAGE_ERROR_DELAY)
    }
  }

  getIsCompleteFormCoverageError = () => {
    const { form, requiredFieldsForCoverage, selectedPolicyItem } = this.props
    const { record } = form
    const { coverage } = this.state
    const lookupType = record?.customer_lookup_type
    const missingFields = this.getMissingFields(requiredFieldsForCoverage, record)
    return (coverage?.result === 'Failed' && missingFields.length === 0 &&
       (selectedPolicyItem || lookupType === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL))
  }

  setCoverageIncompleteError = () => {
    const { form, isCoverageDeterminationIncomplete } = this.props
    const { record } = form
    if (record) {
      record.isCoverageLoading = this.state.isLoading
    }
    this.props.setCoverageIncompleteError(isCoverageDeterminationIncomplete())
  }

  serviceChangesRequiredLocationUpdate = (requiredFieldsForCoverage) => {
    const { form } = this.props
    const { record } = form
    let newRequiredFieldsForCoverage = clone(requiredFieldsForCoverage)
    if (Consts.SERVICES_WITHOUT_DROPOFF.includes(record.service)) {
      newRequiredFieldsForCoverage = pull(newRequiredFieldsForCoverage, 'drop_location', 'service_location')
    }
    return newRequiredFieldsForCoverage
  }

  // This method exists to prevent extra calls based on programtic changes in the location field that would result in unnecessary extra calls while the field is processing
  extraLocationChangeCall = (changedFieldIsSame, newValue, oldValue) => {
    // The first time a user enters information in the location field, the new value does not include a place id
    // we do not need to make a coverage call because the location field with programmatically update with a place id (we should just make one call, the second one with the place id)
    // the reason we check that a lat is present is because no place id will exist if the field is blank and we DO want to call coverage for an update in that case
    if (!newValue.place_id && newValue.lat) {
      return true
    }
    // if we have a site id on the same lat/lng use it -- don't call coverage for same field again without site id
    if (changedFieldIsSame) {
      if (Utils.isSameToDecimal(newValue.lat, oldValue.lat, 7) && Utils.isSameToDecimal(newValue.lng, oldValue.lng, 7) && !newValue.site_id && oldValue.site_id) {
        return true
      }
    }
    return false
  }

  allowCovergeRequest = (lastFieldChanged, changedFieldIsSame, newValue, oldValue) => {
    if ((lastFieldChanged === 'drop_location' || lastFieldChanged === 'service_location') && this.extraLocationChangeCall(changedFieldIsSame, newValue, oldValue)) {
      return false
    }
    return true
  }

  hasRequiredCoverageFieldChanged = () => {
    const { requiredFieldsForCoverage, form } = this.props
    const { fields, lastFieldChanged } = form
    if (this.serviceChangesRequiredLocationUpdate(requiredFieldsForCoverage).includes(lastFieldChanged) || lastFieldChanged === 'service_code_id') {
      const lastFieldChangedValue = fields[lastFieldChanged]?.getValue()
      const changedFieldIsSame = lastFieldChanged === this.savedLastFieldChanged
      if (!changedFieldIsSame || lastFieldChangedValue !== this.savedLastFieldChangedValue) {
        if (this.allowCovergeRequest(lastFieldChanged, changedFieldIsSame, lastFieldChangedValue, this.savedLastFieldChangedValue)) {
          if (DEBOUNCED_FIELDS.includes(lastFieldChanged)) {
            clearTimeout(this.debouncedCoverageRequest)
            this.debouncedCoverageRequest = setTimeout(() => {
              this.requestCoverage()
            }, DEBOUNCE_DELAY)
          } else {
            this.requestCoverage()
          }
        }
        this.savedLastFieldChanged = lastFieldChanged
        this.savedLastFieldChangedValue = lastFieldChangedValue
      }
    }
  }

  updateCoverage = (prevProps) => {
    const { form, selectedPolicyItem, setSelectedPolicyItem } = this.props
    const { record } = form

    // if switch customer lookup type, or if loaded saved form with coverage and then coverage is cleared (i.e. user clicked on clear),
    // clear all coverage results and clear selectedPolicyItem selection
    if (form.lastFieldChanged === 'customer_lookup_type' || (this.initialPCCCoverageId && !record.pcc_coverage_id)) {
      if (this.state.coverage) {
        this.setState({
          coverage: null,
          isLoading: false,
          coverageError: false,
        })
        record.pcc_coverage = null
        record.pcc_coverage_id = null
      }
      if (selectedPolicyItem) {
        setSelectedPolicyItem(null)
      }
      // if switch customer lookup type to manual, run a coverage request as no selectedPolicyItem required
      if (record.customer_lookup_type === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL) {
        form.lastFieldChanged = ''
        this.requestCoverage()
      }
      this.initialPCCCoverageId = null
    // if user clicks clear button after choosing a selectedPolicyItem, then we need to clear all coverage results
    } else if (prevProps.selectedPolicyItem && !selectedPolicyItem) {
      this.setState({
        coverage: null,
        isLoading: false,
        coverageError: false,
      })
    // if user chooses a selectedPolicyItem when there was none before, or makes a new choice, then we need to make a coverage call
    } else if (selectedPolicyItem && (!prevProps.selectedPolicyItem || selectedPolicyItem.id !== prevProps.selectedPolicyItem.id)) {
      this.requestCoverage()
    // new drop off sites exist (from drop off site api response triggered by pickup location change), need to recall coverage to make sure dropoff site still valid
    } else if ((prevProps.dropOffSites && prevProps.dropOffSites.length > 0) && !isEqual(prevProps.dropOffSites, this.props.dropOffSites)) {
      this.requestCoverage()
    // Run a coverage request if a field that determines coverage has changed if:
    // 1. we have a manual lookup; or
    // at least one coverage request has been made before (e.g. a policy/selectedPolicyItem is selected), and
    // 2. and currently a selectedPolicyItem exists, or
    // 3. this is a saved form.
    } else if (record.customer_lookup_type === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL ||
              (this.initialCoverageRequestMade && (selectedPolicyItem || record.pcc_coverage_id))) {
      this.hasRequiredCoverageFieldChanged()
      // new condition: if they have the setting and this is the first time we try to run it,
      // then make the request for coverage.
    } else if (this.props.stores?.SettingStore?.getSettingByKey(Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.RUN_COVERAGE_ON_NEW_JOB_FORM_LOAD) && !this.initialCoverageRequestMade) {
      this.requestCoverage()
    }
  }

  createCoveragePayload = () => {
    const { form, selectedPolicyItem } = this.props
    const { record } = form

    const {
      service, year, odometer, service_location, drop_location, id,
    } = record

    if (!id) {
      return null
    }

    const payload = {}
    payload.job = {
      id,
    }

    if (selectedPolicyItem) {
      const { policyCustomer, policyVehicle, policyId } = selectedPolicyItem
      payload.pcc_policy_id = policyId
      payload.pcc_vehicle_id = policyVehicle.id
      payload.pcc_driver_id = policyCustomer.id
    } else if (record.pcc_coverage) {
      payload.pcc_policy_id = record.pcc_coverage.pcc_policy_id
      payload.pcc_vehicle_id = record.pcc_coverage.pcc_vehicle_id
      payload.pcc_driver_id = record.pcc_coverage.pcc_driver_id
    }

    if (service) {
      payload.job.service = {
        name: service,
      }
    }

    if (year || odometer) {
      payload.job.vehicle = {}
      if (year) {
        payload.job.vehicle.year = parseInt(year, 10)
      }
      if (odometer) {
        payload.job.vehicle.odometer = parseInt(odometer, 10)
      }
    }

    const serviceLocationExists = service_location && service_location.lat && service_location.lng
    const dropLocationExists = drop_location && drop_location.lat && drop_location.lng

    if (serviceLocationExists || dropLocationExists) {
      payload.job.location = {}
      if (serviceLocationExists) {
        payload.job.location.service_location = {
          lat: service_location.lat,
          lng: service_location.lng,
        }
        if (service_location.place_id) {
          payload.job.location.service_location.google_place_id = service_location.place_id
        }
      }
      if (dropLocationExists) {
        payload.job.location.dropoff_location = {
          lat: drop_location.lat,
          lng: drop_location.lng,
        }
        if (drop_location.place_id) {
          payload.job.location.dropoff_location.google_place_id = drop_location.place_id
        }
        if (drop_location.site_id) {
          payload.job.location.dropoff_location.site_id = drop_location.site_id
        } else if (form.drop_location_options) {
          const fixedOption = find(form.drop_location_options, option => Utils.isSameToDecimal(option.location?.lat, drop_location.lat, 7) && Utils.isSameToDecimal(option.location?.lng, drop_location.lng, 7))
          if (fixedOption?.location?.site_id) {
            payload.job.location.dropoff_location.site_id = fixedOption.location.site_id
          }
        }
      }
    }

    return payload
  }

  handleNoAPIResponse = () => {
    this.noAPIResponseTimeout = setTimeout(() => {
      if (this.state.isLoading && !this.state.coverage) {
        this.setState({
          isLoading: false,
          coverageError: true,
        })
      }
    }, 1000 * 30)
  }

  retryCoverageCall = () => {
    this.requestCoverage(true)
  }

  handleBackEndCoverageEndpointError = () => {
    this.setState({
      isLoading: false,
      coverageError: true,
    })
  }

  requestCoverage = (retryCoverageCall = false) => {
    const { form, onCoverageResponse } = this.props
    const { record } = form

    const payload = (retryCoverageCall && this.previousPayload) ? this.previousPayload : this.createCoveragePayload()
    if (!isEmpty(payload)) {
      if (!retryCoverageCall && isEqual(payload, this.previousPayload)) {
        return
      }
      this.previousPayload = payload
      this.setState({
        isLoading: true,
        coverage: null,
      }, () => {
        clearTimeout(this.noAPIResponseTimeout)
        this.handleNoAPIResponse()
      })
      AsyncRequestStore.lookupCoverage(payload, {
        success: (data) => {
          if (data.error) {
            this.handleBackEndCoverageEndpointError()
            Rollbar.error('Error looking up coverage with api endpoint /policy/coverage, DATA.ERROR:', JSON.stringify(data.error))
          } else {
            /* eslint-disable */
            const coverage = data.programs ? data.programs[0] : data
            this.setState({
              coverage,
              isLoading: false,
              coverageError: false,
            }, () => {
              record.coverage = coverage
              record.pcc_coverage_id = data.pcc_coverage_id
              form.renderInput('drop_location')
              onCoverageResponse(coverage)
            })
          }
          clearTimeout(this.noAPIResponseTimeout)
        },
        error: (error) => {
          this.handleBackEndCoverageEndpointError()
          clearTimeout(this.noAPIResponseTimeout)
          Rollbar.error('Error looking up coverage with api endpoint /policy/coverage, ERROR:', JSON.stringify(error))
        },
      })
      this.initialCoverageRequestMade = true
    }
  }

  getMissingFields = (requiredFieldsForCoverage, record) => {
    const newRequiredFieldsForCoverage = this.serviceChangesRequiredLocationUpdate(requiredFieldsForCoverage)
    let missingFields = pull(newRequiredFieldsForCoverage, 'customer_lookup_type')
    missingFields = filter(newRequiredFieldsForCoverage, fieldKey => !record[fieldKey] ||
      (fieldKey === 'drop_location' && !record[fieldKey].lat) || (fieldKey === 'service_location' && !record[fieldKey].lat))
    missingFields = filter(missingFields, fieldKey => REQUIRED_FIELD_KEY_TO_WORDS_MAP[fieldKey])

    if (record?.coverage) {
      record.coverage.missingFields = missingFields
    }

    return missingFields
  }

  renderBody = () => {
    const { form, requiredFieldsForCoverage, selectedPolicyItem } = this.props
    const { record } = form
    const { coverage } = this.state
    const lookupType = record?.customer_lookup_type
    const isManualLookup = lookupType === Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL
    const missingFields = this.getMissingFields(requiredFieldsForCoverage, record)
    const runCoverageOnNewJobFormLoad = this.shouldRunCoverageOnNewJobFormLoad()

    this.isCompleteFormCoverageError = false

    if (coverage?.result) {
      if (coverage.result !== 'Failed') {
        return <CoverageResult coverage={coverage} />
      } else if (missingFields.length === 0) {
        if (selectedPolicyItem || isManualLookup || runCoverageOnNewJobFormLoad) {
          this.isCompleteFormCoverageError = true
          return <CoverageLoading />
        } else {
          return <LookupCoverageMessage />
        }
      }
    }

    if (missingFields.length > 0) {
      const coverageLookedUp = (selectedPolicyItem || isManualLookup || record.pcc_coverage || runCoverageOnNewJobFormLoad)
      return coverageLookedUp ?
        <MissingFieldList
          missingFields={missingFields}
          programName={isManualLookup ? 'Manual Entry' : (coverage?.name || 'Warranty')}
        /> :
        <LookupCoverageMessage />
    } else if (!selectedPolicyItem) {
      return <LookupCoverageMessage />
    }
    return <CoverageLoading />
  }

  getContainerClass = (coverage, isLoading) => {
    let containerClass = 'coverage-section-container '
    if (this.props.coverageIncompleteError && !['Covered', 'NotCovered', 'PartiallyCovered'].includes(coverage?.result)) {
      return containerClass += 'incomplete-error'
    } else if (isLoading) {
      return containerClass
    } else if (coverage?.result) {
      return containerClass += coverage.result.toLowerCase().replace(/\s/g, '')
    }
    return containerClass += 'no-result'
  }

  render() {
    const { coverage, isLoading, coverageError } = this.state
    return (
      <div className={this.getContainerClass(coverage, isLoading)}>
        <FormSection
          className="coverage-section-container-inner"
          title="Coverage"
        >
          <div className="coverage-section-body">
            { coverageError && <CoverageError retryCoverageCall={this.retryCoverageCall} /> }
            { !coverageError && (isLoading ? <CoverageLoading /> : this.renderBody()) }
          </div>
        </FormSection>
      </div>
    )
  }
}

export default CoverageSectionContainer
