import React from 'react'
import moment from 'moment'

/* A React Timer component.
  Can receive these options:
    className:      class to change the component style
    color:          customize the color; defaults to black
    expiresAt:      a date that represents when the timer is supposed to end.
    showTimer:      by default the component shows a timer icon. Set it to false to hide it.
*/

import TimerIcon from './timerIcon'


class Timer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      elapsed: 0,
      timerLengthIsSet: false,
    }
  }

  componentDidMount() {
    const expiresAt = moment(this.props.expiresAt)
    const currentDate = moment()

    const diffBetweenDates = expiresAt.diff(currentDate)
    const differenceInSeconds = moment.duration(diffBetweenDates).asSeconds().toFixed(0)

    if (differenceInSeconds > 0) {
      this.timerStartAt = differenceInSeconds
      this.timer = setInterval(this.tick, 1000)
    }
  }

  UNSAFE_componentWillReceiveProps(props) {
    if (props.showExpired) {
      this.setState({ elapsed: 'Expired' })
      clearInterval(this.timer)
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  tick() {
    if (this.timerStartAt === -1) {
      clearInterval(this.timer)

      if (this.props.showExpired === true) {
        this.setState({ elapsed: 'Expired' })
      }

      return
    }

    this.timerStartAt -= 1

    this.setState({ elapsed: `${this.timerStartAt}s` })

    if (!this.state.timerLengthIsSet) {
      this.setState({ timerLengthIsSet: true })
    }
  }

  render() {
    if (!this.state.timerLengthIsSet) {
      return null
    }

    let timerIcon = <TimerIcon color={this.props.color} iconExtraClasses={this.props.iconExtraClasses || ''} />

    if (this.props.showTimerIcon === false) {
      timerIcon = null
    }

    return (
      <div className={this.props.className} style={{ color: this.props.color || '' }}>
        { this.props.appendToLeft }
        { timerIcon }
        <span className="timer-counter">
          {this.state.elapsed}
        </span>
      </div>
    )
  }
}

export default Timer
