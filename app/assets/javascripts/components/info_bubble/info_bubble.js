import React from 'react'
import PropTypes from 'prop-types'
import ReactDOMServer from 'react-dom/server'
import $ from 'jquery'

const defaultProps = {
  onClick: (event) => {
    event.preventDefault()
    event.stopPropagation()
  },
  placement: 'top',
  status: '',
  title: '',
}

// This component is a wrapper for a Bootstrap popover. It builds the necessary
// markup to show the popover and enables it programatically.
//
// Conceptually, this should have been a functional component, but as we need to
// actually enable a popover for each id after rendering we use a
// React.Component for the `componentDidMount()` life cycle method.
class InfoBubble extends React.Component {
  static propTypes = {
    content: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string,
    ]).isRequired,
    icon: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    placement: PropTypes.string,
    status: PropTypes.oneOf(['', 'error', 'disabled']),
    title: PropTypes.string,
  }

  constructor(props) {
    super(props)

    function getRandomId(min, max) {
      return Math.floor((Math.random() * (max - min)) + min)
    }

    this.popoverId = `popover-${getRandomId(100, 999)}`
  }

  componentDidMount() {
    $(`#${this.popoverId}`).popover({
      trigger: 'hover',
      html: true,
    })
  }

  contentAsString() {
    if (this.props.content !== null && typeof this.props.content === 'object') {
      // so if this.props.content is a ReactDOMFactories object, it will get converted to HTML
      // TODO do we have a better way to do this?
      return ReactDOMServer.renderToStaticMarkup(this.props.content)
    }

    return this.props.content
  }

  render() {
    const pointerCursor = this.props.onClick === defaultProps.onClick ? '' : 'pointer'

    return (
      <i
        className={
          `fa ${this.props.icon} ${this.props.status} ${pointerCursor}
          info-bubble`
        }
        data-container="body"
        data-content={this.contentAsString()}
        data-original-title={this.props.title}
        data-placement={this.props.placement}
        data-toggle="popover"
        id={this.popoverId}
        onClick={this.props.onClick}
        role="dialog"
        style={this.props.style}
      />
    )
  }
}

InfoBubble.defaultProps = defaultProps

export default InfoBubble
