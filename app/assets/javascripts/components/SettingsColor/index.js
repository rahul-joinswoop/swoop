
import ColorSelector from 'componentLibrary/ColorSelector'
import SettingStore from 'stores/setting_store'
import { partial } from 'lodash'
import React from 'react'
import './style.scss'

class SettingsColor extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rev: 0,
    }
  }

  componentDidMount() {
    SettingStore.bind(SettingStore.CHANGE, this.generalChanged)
  }

  componentWillUnmount() {
    SettingStore.unbind(SettingStore.CHANGE, this.generalChanged)
  }

  setSetting = (setting, setting_info, color) => {
    let localColor = color
    const { key } = setting_info

    if (!localColor && setting_info.default != null) {
      localColor = 'transparent'
    }

    if (setting != null) {
      if (localColor == null) {
        return SettingStore.deleteItem({
          id: setting.id,
        })
      } else {
        return SettingStore.updateItem({
          value: localColor,
          key,
          id: setting.id,
        })
      }
    } else {
      return SettingStore.addItem({
        value: localColor,
        key,
      })
    }
  }

  generalChanged = () => {
    this.setState(prevState => ({ rev: prevState.rev + 1 }))
  }

  render() {
    let { setting } = this.props

    if (!setting || !setting.text) {
      setting = {
        text: setting,
        key: setting,
      }
    }

    const currentSetting = SettingStore.getSettingByKey(setting.key)
    const settingDefault = setting.default != null ? setting.default : null
    const currentColor = currentSetting != null ? currentSetting.value : settingDefault

    const color = partial(this.setSetting, currentSetting, setting)

    const subtext = setting.subtext != null && <span>{setting.subtext}</span>

    return (
      <div key={setting.key}>
        <div className="color-label SettingsColor-colorLabel">
          <span className="SettingsColor-colorLabel--main">
            {setting.text}
          </span>
          {subtext}
        </div>
        <ColorSelector
          className="SettingsColor-colorSelector"
          color={currentColor}
          setColor={color}
        />
      </div>
    )
  }
}

export default SettingsColor
