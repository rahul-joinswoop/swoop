import React from 'react'
import { extend, partial, without } from 'lodash'
import Consts from 'consts'
import Utils from 'utils'
import Loading from 'componentLibrary/Loading'
import BaseComponent from 'components/base_component'
import TransitionModal from 'components/modals'
import 'stylesheets/components/modals/object_form.scss'

class AllObjectsForm extends BaseComponent {
  displayName = 'AllObjectsForm'

  _changeFunction = null

  _selectedItemsFunction = null

  _store = null

  _objTitle = 'Objects'

  _placeholder = 'Custom Object Name'

  getAll() {
    return []
  }

  constructor(props) {
    super(props)
    this.getAll = this.getAll.bind(this)
    this.editObject = this.editObject.bind(this)
    this.renderObjColumn = this.renderObjColumn.bind(this)
    this.renderAllObjs = this.renderAllObjs.bind(this)
    this.onConfirm = this.onConfirm.bind(this)
    this.renderBody = this.renderBody.bind(this)
    this.state = {
      remove: [],
      add: [],
      loading: false,
    }
  }

  editObject(id, e) {
    if (e.target.checked) {
      const remove = without(this.state.remove, id)
      let { add } = this.state

      if (!this._selectedItemsFunction(id, this.getId())) {
        add = add.concat([id])
      }

      this.setState({ remove, add })
    } else {
      const add = without(this.state.add, id)
      let { remove } = this.state

      if (this._selectedItemsFunction(id, this.getId())) {
        remove = remove.concat([id])
      }

      this.setState({ remove, add })
    }
  }

  renderObjColumn(objects, key) {
    return (
      <div key={key}>
        {objects.map(object => (
          <div key={object.name}>
            <input
              checked={this.state.add.indexOf(object.id) > -1 ||
              (this._selectedItemsFunction(object.id, this.getId()) &&
                this.state.remove.indexOf(object.id) === -1)}
              id={object.name}
              onChange={partial(this.editObject, object.id)}
              type="checkbox"
            />
            <label htmlFor={object.name}>{object.name}</label>
          </div>
        ))}
      </div>
    )
  }

  renderAllObjs() {
    let content
    if (this.state.loading) {
      content = <Loading />
    } else {
      const objs = this.getAll()
      const splits = Utils.splitIntoThree(objs)
      content = [
        this.renderObjColumn(splits[0], 'one'),
        this.renderObjColumn(splits[1], 'two'),
        this.renderObjColumn(splits[2], 'three'),
      ]
    }

    return (
      <div key="AllObjs">
        <div className="standard_obj_list">
          {content}
        </div>
      </div>
    )
  }

  onConfirm() {
    this.setState({ loading: true })
    const objectParams = {}

    if (this.state.add.length > 0) {
      objectParams.add = this.state.add
    }

    if (this.state.remove.length > 0) {
      objectParams.remove = this.state.remove
    }

    this._changeFunction(objectParams, {
      success: this.props.hideModals,
      error: (data, textStatus, errorThrown) => {
        Utils.handleError(data, textStatus, errorThrown)
        return this.setState({ loading: false })
      },
    })
  }

  renderBody() {
    return [
      <p className="AllObjectsForm-helper-text" key="helper-text">
        {Consts.COMMISSION_PERCENTAGE.NON_COMMISSIONABLE_MODAL_TEXT(this._placeholder)}
      </p>,
      this.renderAllObjs(),
    ]
  }

  render() {
    super.render()

    return (
      <TransitionModal {...extend({}, this.props, {
        title: this._objTitle,
        extraClasses: 'obj_form',
        onCancel: this.props.hideModals,
        onConfirm: this.onConfirm,
        confirm: 'Save',
        show: true,
      })}
      >
        {this.renderBody()}
      </TransitionModal>
    )
  }
}

export default AllObjectsForm
