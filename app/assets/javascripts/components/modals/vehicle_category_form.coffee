VehicleCategoryStore = require('stores/vehicle_category_store')
ObjectForm = require('components/modals/object_form')
import Api from 'api'
import { filter, map, sortBy, values } from 'lodash'

class VehicleCategoryForm extends ObjectForm
  displayName: 'ServiceForm'
  _getStandardFunction: Api.standardVehicleCategories
  _changeFunction: Api.changeVehicleCategories
  _store: VehicleCategoryStore
  _objTitle: "Class Types"
  _placeholder: "Custom Class Type"
  getStandardObjs: () =>
    return sortBy(@state.standard_objects, (obj) -> obj.name.toLowerCase())
  getCustomObjs: () =>
    type_ids = map(@state.standard_objects, (type) -> type.id)
    return sortBy(filter(@_store.getCompanyItems(@getId()), (type) => type_ids.indexOf(type.id) == -1 and @state.remove.indexOf(type.id) == -1), (type) -> type.name.toLowerCase())
  getAll: () =>
    values(@_store.getCompanyItems(@getId())).concat(@state.custom)

module.exports = VehicleCategoryForm
