
ServiceStore = require('stores/service_store')
ObjectForm = require('components/modals/object_form')
import Api from 'api'
import { filter, map, sortBy, values } from 'lodash'

class ServiceForm extends ObjectForm
  displayName: 'AddonForm'
  _getStandardFunction: Api.standardAddons
  _changeFunction: Api.changeAddons
  _store: ServiceStore
  _objTitle: "Additional Items"
  _placeholder: "Custom Item Name"
  getStandardObjs: () =>
    return sortBy(@state.standard_objects, (obj) -> obj.name.toLowerCase())
  getCustomObjs: () =>
    service_ids = map(@state.standard_objects, (service) -> service.id)
    return sortBy(filter(@_store.getAddons(@getId()), (service) => service_ids.indexOf(service.id) == -1 and @state.remove.indexOf(service.id) == -1), (service) -> service.name.toLowerCase())

  getAll: () =>
    values(@_store.getAddons(@getId())).concat(@state.custom)

module.exports = ServiceForm
