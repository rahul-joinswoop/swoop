import React from 'react'
EditForm = React.createFactory(require('components/edit_form'))
DepartmentStore = require('stores/department_store')
BaseComponent = require('components/base_component')
ModalStore = require('stores/modal_store')
import Utils from 'utils'
import { extend, find, get } from 'lodash'
import Dispatcher from 'lib/swoop/dispatcher'

class DepartmentForm extends BaseComponent
  displayName: 'DepartmentForm'
  submitDepartment: (department) =>
    if department.id?
      DepartmentStore.updateItem(department)
    else
      DepartmentStore.addItem(department)

    Dispatcher.send(ModalStore.CLOSE_MODALS)

  render: ->
    super(arguments...)

    enabled = not @props.enabled? or enabled == true
    that = @
    data = [
      {
        name: "name"
        label: "Department Name"
        required: true
        isValid: (col) ->
          value = get(@record, col.name)
          # the entire collection will be loaded at this moment
          db_type = find(DepartmentStore.getDepartments(that.getId()), (type) =>
            return type.name == value && @record.id != type.id
          )
          if db_type?
            return "Name Exists"
      }
    ]
    title = "Create Department"
    if @props.record? and @props.record.id?
      title = "Edit Department"

    EditForm extend({}, @props,
      cols: [{
        style:
          marginTop: 15
        fields:data}]
      title: title
      showSubmit: enabled
      submitButtonName: if @props?.record then 'Save' else 'Add'
      onConfirm: @submitDepartment
    )

module.exports = DepartmentForm
