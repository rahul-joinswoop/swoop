import React from 'react'
import TransitionModal from 'components/modals_new'
import DriverMapModalContents from 'components/modals/DriverMapModal/DriverMapModalContents'
import 'stylesheets/components/modals/DriverMapModal/DriverMapModal.scss'

const DriverMapModal = (props) => {
  const {
    acceptJobMethods, acceptJobState, assignDriverMethods, assignDriverState, callbackKey, cancel,
    confirm, confirmError, enableSend, extraClasses, getId, onConfirm, onCancel, record,
    show, title, transitionName,
  } = props
  return (
    <TransitionModal
      {...props}
      callbackKey={callbackKey}
      cancel={cancel}
      confirm={confirm}
      confirmError={confirmError}
      enableSend={enableSend}
      extraClasses={extraClasses}
      onCancel={onCancel}
      onConfirm={onConfirm}
      style={{ padding: 0 }}
      title={title}
      transitionName={transitionName}
    >
      {show &&
        <DriverMapModalContents
          acceptJobMethods={acceptJobMethods}
          acceptJobState={acceptJobState}
          assignDriverMethods={assignDriverMethods}
          assignDriverState={assignDriverState}
          getId={getId}
          record={record}
        />}
    </TransitionModal>
  )
}

export default DriverMapModal
