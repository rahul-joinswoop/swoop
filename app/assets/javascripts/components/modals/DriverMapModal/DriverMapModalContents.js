import React from 'react'
import Consts from 'consts'
import DriverList from 'components/modals/DriverMapModal/DriverList'
import ETAInputContainer from 'components/modals/DriverMapModal/ETAInputContainer'
import JobDetailsOverlay from 'components/JobDetailsOverlay/forAcceptJobModal'
import LastHistoryItemContainer from 'components/modals/DriverMapModal/LastHistoryItemContainer'
import SwoopMap from 'components/maps/map'
import SwoopMapNew from 'components/maps/SwoopMap'
import { MapRoutes, TruckMarker } from 'components/maps/map_new'
import { fitBounds } from 'components/maps/Utils'
import UserStore from 'stores/user_store'
import VehicleStore from 'stores/vehicle_store'
import { createTruckInfoWindowContent } from 'components/maps/MapUtils'
import { filter, keyBy, map } from 'lodash'
import { withSplitIO } from 'components/split.io'
import VehicleCategoryStore from 'stores/vehicle_category_store'

import 'stylesheets/components/modals/DriverMapModal/DriverMapModalContents.scss'

const TruckPopupContent = ({ truck: { name, driver } }) =>
  <div className="main-section">
    <div className="equal-cols driver-and-truck-section">
      <div className="col-xs-6 driver-name-and-toggle">
        <i
          className={`on-off-circle fa fa-circle ${driver.on_duty === false ? 'false' : 'true'}`}
          title={driver.on_duty ? 'On Duty' : 'Off Duty'}
        />
        <span className="name" title={driver.full_name}>
          {driver.full_name}
        </span>
      </div>
      <div className="col-xs-6 select-truck-container">
        <span>{name}</span>
      </div>
    </div>
    {driver.jobs?.length ?
      <LastHistoryItemContainer jobs={driver.jobs} /> :
      null}
  </div>

class DriverMapModalContents extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      map: null,
      selectedTruckId: null,
    }
  }

  componentDidUpdate() {
    this.state.map?.rerenderInfoWindows()
  }

  getVehicleDriverId = vehicle_id => VehicleStore.get(vehicle_id, 'driver_id', this.props.getId())

  setMapRef = (mapRef) => {
    if (!this.state.map) {
      this.setState({ map: mapRef })
    }
  }

  handleTruckClick = (id, setSelectedDriverId) => {
    const driverId = this.getVehicleDriverId(id)
    setSelectedDriverId(driverId)
  }

  showTruckInfoWindow = (truckId) => {
    this.state.map?.showTruckInfoWindow(truckId)
    this.setState({ selectedTruckId: truckId })
  }

  renderClassType = () => {
    const {
      record,
    } = this.props

    let class_type = null
    if (record.invoice_vehicle_category_id) {
      class_type = VehicleCategoryStore.get(record.invoice_vehicle_category_id, 'name', this.props.getId())
    }

    if (!class_type) {
      class_type = null
    } else {
      class_type = <span className="ChoosePartner-classTypeDisplay">{class_type}</span>
    }

    let classField = null
    if (class_type) {
      classField = (
        <div className="ChoosePartner-classTypeField">
          <span className="ChoosePartner-classTypeLabel">Class Type</span>
          {class_type}
        </div>
      )
    }

    return classField
  }

  renderMap = () => {
    const {
      treatments, record, assignDriverState, assignDriverMethods, getId,
    } = this.props

    const { selectedTruckId } = this.state

    const routes = [
      {
        id: record?.id,
        origin: record?.service_location,
        dest: record?.drop_location,
        originMarker: '/assets/images/marker_green.png',
        destMarker: '/assets/images/marker_red.png',
      },
    ]

    const drivers = keyBy(assignDriverMethods.getSortedDrivers(), 'id')

    const visibleTrucks = filter(
      VehicleStore.getSortedDriverVehicles(getId()), truck => {
        if (truck.driver_id && drivers[truck.driver_id]) {
          /* eslint-disable-next-line no-param-reassign */
          truck.driver = drivers[truck.driver_id]
          return true
        }
      }
    )

    const truckIds = map(visibleTrucks, 'id')

    if (!treatments?.[Consts.SPLITS.DISABLE_NEW_DRIVER_MAP_MODALS]) {
      // showInfoWindow={assignDriverState.highlighted_truck}
      // highlighted_truck is not set anywhere, so we don't need it


      // This is used for showing info window need to come up with alternative
      // setMapRef={this.setMapRef} // createRef pattern isn't working here

      // truckInfoWindow={(id, nameOrNode) => createTruckInfoWindowContent(id, nameOrNode, this.getVehicleDriverId(id), getId)}


      return (
        <SwoopMapNew
          onLoad={(gMap) => {
            fitBounds(gMap, [
              record.service_location,
              record.drop_location,
              ...visibleTrucks,
            ])
          }}
          style={{ width: '100%' }}
        >
          <MapRoutes
            obfuscate={record?.status === Consts.AUTO_ASSIGNING && UserStore.isPartner()}
            routes={routes}
          />
          {visibleTrucks.map(truck =>
            <TruckMarker
              id={truck.id}
              key={truck.id}
              onClick={(id) => {
                this.handleTruckClick(id, assignDriverMethods.setSelectedDriverId)
              }}
              setSelectedId={id => this.setState({ selectedTruckId: id })}
              showInfoWindow={selectedTruckId === truck.id}
              showTruckNames
            >
              <TruckPopupContent truck={truck} />
            </TruckMarker>
          )}

        </SwoopMapNew>
      )
    } else {
      return (
        <SwoopMap
          autoAssigning={record?.status === Consts.AUTO_ASSIGNING}
          routes={routes}
          setMapRef={this.setMapRef} // createRef pattern isn't working here
          showAddressNames
          showInfoWindow={assignDriverState.highlighted_truck}
          showTruckNames
          /* eslint-disable-next-line */
          truckClick={(id) => {
            this.handleTruckClick(id, assignDriverMethods.setSelectedDriverId)
          }}
          /* eslint-disable-next-line */
          truckInfoWindow={(id, nameOrNode) => createTruckInfoWindowContent(id, nameOrNode, this.getVehicleDriverId(id), getId)}
          trucks={truckIds}
          width="100%"
        />
      )
    }
  }

  render() {
    const {
      acceptJobMethods, acceptJobState, assignDriverMethods, assignDriverState, getId, record,
    } = this.props


    return (
      <div className="container-fluid driver-map-modal-contents">
        <div className="row inner-container">
          <div className="col-md-8 job-detail-and-map-container">
            <JobDetailsOverlay job_id={record?.id} record={record} />
            {assignDriverState.showMap && this.renderMap()}
          </div>
          <div className="col-md-4 driver-list-container">
            <DriverList
              assignDriverMethods={assignDriverMethods}
              getId={getId}
              isAcceptModal={!!acceptJobState}
              onTruckClick={this.showTruckInfoWindow}
              savedVehicles={assignDriverState.savedVehicles}
              search={assignDriverState.search}
              selectedDriverId={assignDriverState.selectedDriverId}
              showOffDuty={assignDriverState.showOffDuty}
              vehicles={assignDriverState.vehicles}
            />
            <div className="DriverListModal-listBottom">
              {this.renderClassType()}
              {acceptJobState &&
                <ETAInputContainer
                  acceptJobMethods={acceptJobMethods}
                  acceptJobState={acceptJobState}
                  getId={getId}
                  record={record}
                />}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withSplitIO(DriverMapModalContents)
