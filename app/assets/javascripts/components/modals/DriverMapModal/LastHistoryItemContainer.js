import React, { Component } from 'react'
import Utils from 'utils'
import { map } from 'lodash'
import 'stylesheets/components/modals/DriverMapModal/LastHistoryItemContainer.scss'

const LastHistoryItemContainer = ({ jobs, selectedJob }) => (
  <div className="last-history-item-container">
    {map(jobs, job => <LastHistoryItem job={job} key={job.id} selectedJob={selectedJob} />)}
  </div>
)

class LastHistoryItem extends Component {
  constructor(props) {
    super(props)
    this.ref = React.createRef()
  }

  componentDidUpdate() {
    if (this.isSelected()) {
      this.ref.current.scrollIntoView({ block: 'nearest' })
    }
  }

  isSelected() {
    const { job, selectedJob } = this.props
    return job.id === selectedJob?.id
  }

  handleJobClick = (event) => {
    const { job, selectedJob } = this.props
    if (selectedJob?.update) {
      event.stopPropagation()
      selectedJob.update(job.id)
    }
  }

  render() {
    const { job } = this.props
    const { name, time } = Utils.getJobsLastHistoryItemTimeAndName(job)
    const selected = this.isSelected() ? 'selected' : ''

    return (
      <div className="last-history-item" key={`status_${job.id}`} ref={this.ref}>
        {/* eslint-disable-next-line */}
        <span className={`last-history-item-details ${selected}`} onClick={this.handleJobClick}>
          {`#${job.id} `}
          {name && time && `• ${name} ${time} `}
          {`• ${job.service}`}
        </span>
      </div>
    )
  }
}

export default LastHistoryItemContainer
