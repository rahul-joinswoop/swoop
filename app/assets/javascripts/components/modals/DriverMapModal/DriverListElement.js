import React from 'react'
import LastHistoryItemContainer from 'components/modals/DriverMapModal/LastHistoryItemContainer'
import FontAwesomeHoverBubble from 'components/FontAwesomeHoverBubble'
import Utils from 'utils'
import Consts from 'consts'
import UsersStore from 'stores/users_store'
import VehicleStore from 'stores/vehicle_store'
import ToggleButton from 'react-toggle-button'
import { clone, find, map } from 'lodash'
import moment from 'moment-timezone'
import 'stylesheets/components/modals/DriverMapModal/DriverListElement.scss'

const selectTruckOnChange = (optionValue, vehicles, setVehicles, driverId) => {
  const driverVehicles = clone(vehicles) || {}
  driverVehicles[driverId] = optionValue
  setVehicles(driverVehicles)
}

const toggleTruckOnDuty = (driver) => {
  UsersStore.updateItem({
    id: driver.id,
    on_duty: !driver.on_duty,
  })
}

const TruckOptions = ({ getId }) => (
  map(VehicleStore.getSortedVehicles(getId()), vehicle => (
    <option className="option_row" key={vehicle.id} value={vehicle.id}>
      {vehicle.name}
    </option>
  ))
)

export const isLocationUpdateOutdated = truck =>
  truck?.location_updated_at && moment(truck.location_updated_at).isBefore(moment().subtract(Consts.DRIVER_ALERT_AFTER, 'minutes'))

export const GPSAlertHover = ({
  isNotAssignDriver, listItemIndex, vehicle,
}) => {
  let alertLastGPSUpdate = null
  if (isLocationUpdateOutdated(vehicle)) {
    alertLastGPSUpdate = vehicle.location_updated_at
  }

  let message
  if (alertLastGPSUpdate) {
    alertLastGPSUpdate = Utils.formatDateTime(alertLastGPSUpdate)
    message = (
      <div className="alert-last-gps-update-bubble">
        <div className="title">GPS Not Updating</div>
        <div className="message">{`Last updated ${alertLastGPSUpdate}`}</div>
      </div>
    )
  } else if (vehicle && !(vehicle.lat && vehicle.lng)) {
    message = (
      <div className="alert-last-gps-update-bubble">
        <div><b>GPS Unavailable</b></div>
      </div>
    )
  }

  if (isNotAssignDriver && message) {
    return <FontAwesomeHoverBubble
      bubbleContents={message}
      className="fa-exclamation-triangle"
      color="#ffc300"
      flipToBottom={listItemIndex === 0}
    />
  }
  return null
}

const TruckInfoOnly = ({
  isNotAssignDriver, driver, listItemIndex, vehicle,
}) => (
  <div className="equal-cols driver-and-truck-section truck-info-only">
    <div className="col-xs-6 driver-name-and-toggle no-driver">
      No Driver
    </div>
    <div className="col-xs-6 select-truck-container">
      <GPSAlertHover
        isNotAssignDriver={isNotAssignDriver}
        listItemIndex={listItemIndex}
        vehicle={vehicle}
      />
      <div className="truck-name">{driver.truck.name}</div>
    </div>
  </div>
)

const TruckSelect = ({
  driver, isNotAssignDriver, listItemIndex, vehicle,
  vehicles, setVehicles, getId, selectedVehicleId,
}) => (
  <div className="equal-cols driver-and-truck-section">
    <div className="col-xs-6 driver-name-and-toggle">
      <i className={`on-off-circle fa fa-circle ${driver.on_duty === false ? 'false' : 'true'}`} />
      <div className={`truck-toggle${listItemIndex === 0 ? ' flip-bubble' : ''}`}>
        <ToggleButton
          activeLabel={<div className="toggle-background" />}
          colors={{
            activeThumb: {
              base: '#65c343',
            },
            inactiveThumb: {
              base: '#ff0000',
            },
            active: {
              base: '#d6d6d6',
            },
            inactive: {
              base: '#d6d6d6',
            },
          }}
          inactiveLabel={<div className="toggle-background" />}
          /* eslint-disable */
          onToggle={() => toggleTruckOnDuty(driver)}
          thumbAnimateRange={[0, 16]}
          thumbStyle={{ boxShadow: 'none' }}
          trackStyle={{ width: 34, height: 16 }}
          value={driver.on_duty || ''}
        />
        <div className="bubble" ><span>{driver.on_duty ? 'On Duty' : 'Off Duty' }</span></div>
      </div>
      <span className="name" title={driver.full_name}>
        {driver.full_name}
      </span>
    </div>
    <div className="col-xs-6 select-truck-container">
      <GPSAlertHover
        isNotAssignDriver={isNotAssignDriver}
        listItemIndex={listItemIndex}
        vehicle={vehicle}
      />
      <select
        className={`form-control select-truck ${isNotAssignDriver ? 'disable-for-accept' : ''}`}
        name="rescue_vehicle_id"
        /* eslint-disable */
        onChange={(event) => {
          selectTruckOnChange(event.target.value, vehicles, setVehicles, driver.id)
        }}
        value={selectedVehicleId}
      >
        <option className="option_header" value="">{isNotAssignDriver ? 'No Truck Selected' : '- Select -'}</option>
        <TruckOptions getId={getId} />
      </select>
    </div>
  </div>
)

const DriverListElement = ({
  isAcceptModal, isPartnerFleetMap, driver, getId, listItemIndex, onTruckClick, selectedDriverId, setSelectedDriverId,
  setVehicles, vehicles, savedVehicles, selectedJob,
}) => {
  const selectedVehicleId = driver.truck?.id || savedVehicles?.[driver.id] || VehicleStore.getVehicleIdByDriverId(driver.id, getId())

  const selectedVehicle = find(vehicles, { id: selectedVehicleId })

  const isNotAssignDriver = isAcceptModal || isPartnerFleetMap

  return (
    /* eslint-disable-next-line */ // about keyboard listener
    <li
      className="driver-list-element"
      key={`Driver_${driver.id || driver.truck?.id}`}
      onClick={((driver_id, vehicleId) => () => {
        if (vehicleId && onTruckClick) {
          onTruckClick(driver.truck?.id || vehicleId)
        }
        driver_id && setSelectedDriverId(driver_id)
      })(driver.id, selectedVehicleId)}
      style={{
        boxShadow: (!isNotAssignDriver && selectedDriverId === driver.id) ? '0px 0px 3px 3px #7FBCE2' : '',
      }}
    >
      { driver.id &&
        <div
        className="main-section"
        id={`driver_jobs_${driver.id}`}
        >
          <TruckSelect
            driver={driver}
            getId={getId}
            isNotAssignDriver={isNotAssignDriver}
            listItemIndex={listItemIndex}
            setVehicles={setVehicles}
            selectedVehicleId={selectedVehicleId}
            vehicles={vehicles}
            vehicle={selectedVehicle}
          />
          <LastHistoryItemContainer jobs={driver.jobs} selectedJob={selectedJob} />
        </div>
      }
      { !driver.id && driver.truck?.id &&
        <div className="main-section" id={`driver_jobs_${driver.truck.id}`}>
          <TruckInfoOnly
            driver={driver}
            isNotAssignDriver={isNotAssignDriver}
            listItemIndex={listItemIndex}
            vehicle={selectedVehicle}
          />
        </div>
      }
    </li>
  )
}

export default DriverListElement
