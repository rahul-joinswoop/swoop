import React from 'react'
import Consts from 'consts'
import Utils from 'utils'
import Timer from 'components/timer'
import AuctionStore from 'stores/auction_store'
import JobStore from 'stores/job_store'
import EtaExplanationStore from 'stores/eta_explanation_store'
import { map } from 'lodash'
import 'stylesheets/components/modals/DriverMapModal/ETAInputContainer.scss'

const NoJobUntilEtaLabel = ({ job, record }) => {
  if ((AuctionStore.jobHasAuction(job) && job.status === Consts.AUTO_ASSIGNING) ||
  (record?.issc_dispatch_request && job.status === Consts.ASSIGNED)) {
    return (
      <div className="no-job-eta-message">
        { !Utils.isTimerExpired(job, AuctionStore) ?
          'Do not run job until ETA accepted' :
          'This job has expired and is no longer available'}
      </div>
    )
  }
  return null
}

const TimerContainer = ({ job }) => {
  let timer = null
  let expiresAt = null

  if (AuctionStore.jobHasLiveAuction(job)) {
    expiresAt = AuctionStore.getExpiresAtByJob(job)
  } else if (JobStore.isMotorClub(job)) {
    expiresAt = job.answer_by
  }

  if (expiresAt) {
    timer = <Timer
      className={`timer-eta accept-job-timer ${!AuctionStore.jobHasLiveAuction(job) ? 'accept-job-timer-expired' : ''}`}
      expiresAt={expiresAt}
      iconExtraClasses={!AuctionStore.jobHasLiveAuction(job) ? 'padding-right-5px' : ''}
      showExpired={Utils.isTimerExpired(job, AuctionStore)}
    />
  }

  return timer
}

const ETAInput = (props) => {
  const {
    formState, getId, onHandleEtaInputChange, record, selectEtaReason, showEtaReasons,
  } = props

  if (record?.scheduled_for) {
    return (
      <div className="scheduled-time">
        <div className="title">Scheduled Time:</div>
        <span>{`${Utils.formatDateTime(record.scheduled_for, true)}`}</span>
      </div>
    )
  }

  const { eta } = formState

  const clientsWithConditions = ['GCOAPI', 'AGO', 'QUEST']
  const clientName = record.issc_dispatch_request?.client_id

  return (
    <div className="eta-input-section">
      <div className="input-and-label-container">
        <div className="eta-label">ETA</div>
        <div className="input-container">
          <input
            className={`form-control ${formState.error ? 'error' : ''}`}
            onChange={onHandleEtaInputChange}
            placeholder="Minutes"
            value={eta || ''}
          />
          {formState.error &&
            <div className="error_bubble">!
              <div className="bubble"><span>Required</span></div>
            </div>}
        </div>
      </div>
      {showEtaReasons() &&
        <div className="explanation-container">
          <div className="explanation-label">Explanation</div>
          <select
            className="form-control"
            onChange={selectEtaReason}
            value={formState.reason}
          >
            <option className="option_header" value={0}>- Select -</option>
            {
              map(EtaExplanationStore.getAll(false, getId()), (obj, index) => {
                if (!clientsWithConditions.includes(clientName) ||
                  (clientName === 'GCOAPI' && [3, 2, 7].includes(parseInt(index, 10))) ||
                  (clientName === 'AGO' && [1, 2, 7].includes(parseInt(index, 10))) ||
                  (clientName === 'QUEST' && [4, 5, 6, 7, 8, 2].includes(parseInt(index, 10)))
                ) {
                  return <option key={index} value={index}>{obj.text}</option>
                } else {
                  return null
                }
              })
            }
          </select>
          <div className="error">{formState.error_reason}</div>
        </div>}
    </div>
  )
}

const ETAInputContainer = ({
  acceptJobMethods, acceptJobState, getId, record,
}) => (
  <div className="eta-input-container">
    <div className="information-timer-container">
      <NoJobUntilEtaLabel job={acceptJobState.job} record={record} />
      <TimerContainer job={acceptJobState.job} />
    </div>
    <ETAInput
      formState={acceptJobState}
      getId={getId}
      onHandleEtaInputChange={acceptJobMethods.handleEtaInputChange}
      record={record}
      selectEtaReason={acceptJobMethods.selectEtaReason}
      showEtaReasons={acceptJobMethods.showEtaReasons}
    />
  </div>
)

export default ETAInputContainer
