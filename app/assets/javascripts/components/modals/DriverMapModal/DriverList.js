import React from 'react'
import DriverSearch from 'components/modals/DriverMapModal/DriverSearch'
import DriverListElement from 'components/modals/DriverMapModal/DriverListElement'
import { map } from 'lodash'
import 'stylesheets/components/modals/DriverMapModal/DriverList.scss'

const DriverList = ({
  assignDriverMethods, getId, isAcceptModal, isPartnerFleetMap, onTruckClick, search,
  selectedDriverId, showOffDuty, vehicles, savedVehicles, showButton, selectedJob,
}) => {
  const {
    getSortedDrivers, getSortedTrucksAndDrivers, setSelectedDriverId, setSearch, setShowOffDuty, setVehicles,
  } = assignDriverMethods

  const offDutyButtonText = showOffDuty ? 'Hide Off Duty Drivers' : 'Show Off Duty Drivers'

  const truckAndDrivers = (getSortedDrivers && getSortedDrivers()) || getSortedTrucksAndDrivers()

  return (
    <div className={`drivers-list ${isAcceptModal ? 'accept-job' : ''} ${isPartnerFleetMap ? 'partner-fleet' : ''}`}>
      <DriverSearch
        isAcceptModal={isAcceptModal}
        isPartnerFleetMap={isPartnerFleetMap}
        search={search}
        setSearch={setSearch}
      />
      <ul>
        {truckAndDrivers.length ?
          map(truckAndDrivers, (driver, index) => (
            <DriverListElement
              driver={driver}
              getId={getId}
              isAcceptModal={isAcceptModal}
              isPartnerFleetMap={isPartnerFleetMap}
              key={driver.id ? `driver-${driver.id}` : `truck-${driver.truck?.id}`}
              listItemIndex={index}
              onTruckClick={onTruckClick}
              savedVehicles={savedVehicles}
              selectedDriverId={selectedDriverId}
              selectedJob={selectedJob}
              setSelectedDriverId={setSelectedDriverId}
              setVehicles={setVehicles}
              vehicles={vehicles}
            />
          )) :
          <li className="driver-list-element empty-driver-list">
            No Trucks or Drivers
          </li>}
        {showButton &&
          /* eslint-disable-next-line */
          <li
            className="off-duty-button"
            key="off_duty"
            /* eslint-disable-next-line */ // need to bind
            onClick={() => setShowOffDuty(!showOffDuty)}
            style={{
              border: showOffDuty ? '1px solid #777777' : '1px solid transparent',
              color: showOffDuty ? '#777777' : '#FFFFFF',
              backgroundColor: showOffDuty ? '' : '#AAAAAA',
            }}
          >
            <span>{offDutyButtonText}</span>
          </li>}
      </ul>
    </div>
  )
}

DriverList.defaultProps = {
  showButton: true,
}

export default DriverList
