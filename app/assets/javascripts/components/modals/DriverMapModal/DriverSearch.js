import React from 'react'
import 'stylesheets/components/modals/DriverMapModal/DriverSearch.scss'

class DriverSearch extends React.Component {
  handleClearInputClick = () => {
    /* eslint-disable-next-line */ // tried with createRef; didn't work
    this.refs.searchInput.focus()
    this.props.setSearch('')
  }

  render() {
    const {
      isAcceptModal, isPartnerFleetMap, search, setSearch,
    } = this.props
    let searchLabel = 'SELECT DRIVER'
    if (isAcceptModal) {
      searchLabel = 'DRIVERS'
    } else if (isPartnerFleetMap) {
      searchLabel = 'TRUCKS AND DRIVERS'
    }

    return (
      <div className="driver-search-container">
        <h2 className="search-label">
          {searchLabel}
        </h2>
        <div className="input-container">
          <i className="fa fa-search" />
          {/* eslint-disable-next-line */}
          {search.length > 0 && <i
            className="fa fa-times-circle"
            onClick={this.handleClearInputClick}
          />}
          <input
            className="form-control"
            /* eslint-disable-next-line */ // how to pass parameter into onclick if no bind
            onChange={(event) => {
              setSearch(event.target.value)
            }}
            placeholder={isPartnerFleetMap ? 'Search' : 'Search Driver'}
            /* eslint-disable-next-line */ // tried with createRef; didn't work
            ref='searchInput'
            value={search}
          />
        </div>
      </div>
    )
  }
}

export default DriverSearch
