import React from 'react'
import Api from 'api'
import { withModalContext } from 'componentLibrary/Modal'
require('stylesheets/components/modals/add_on_site_status.scss')
TransitionModal = React.createFactory(require 'components/modals')
import Utils from 'utils'
JobStore = require('stores/job_store')
Fields = require('fields/fields')
Renderer = require('form_renderer')
BaseComponent = require('components/base_component')
import Consts from 'consts'
import DateTimeSelectorImport from 'components/time_pickers/date_time_selector.js'
DateTimeSelector = React.createFactory(DateTimeSelectorImport)
UserStore = require('stores/user_store')
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
import SelectImport from 'componentLibrary/SimpleSelect'
Select = React.createFactory(SelectImport)
import moment from 'moment-timezone'

class AddOnSiteStatus extends BaseComponent
  displayName: 'AddOnSiteStatus'

  constructor: (props) ->
    super(props)
    @state.delay = 0 # in minutes

  checkAndSetError: (onSiteTime = @getOnSiteTime()) =>
    #Grab the closest prior status that on site time must be after
    afterStatusName = null
    for statusName in Consts.FORCE_ONSITE_AFTER
      afterStatus = JobStore.get(@props.job_id, "history.#{statusName}")
      if afterStatus? and !afterStatus.deleted_at?
        afterStatusTime = afterStatus.adjusted_dttm || afterStatus.last_set
        if afterStatusTime?
          afterStatusName = statusName
          break

    if afterStatusTime? and onSiteTime.isBefore(afterStatusTime)
      @setState
        error: "Must be after #{afterStatusName} time: #{Utils.formatDateTime(afterStatusTime, true)}"
      return true

    @setState
      error: null

  componentDidMount: =>
    super(arguments...)

    if moment().isBefore(@getEstimatedOnSite())
      @onConfirm(moment())

  onConfirm: (onSiteTime = @getOnSiteTime()) =>
    @setState
      attemptedSubmit: true

    if @checkAndSetError(onSiteTime)
      return

    if @state.posting
      return

    @setState
      posting: true

    #TODO: this should go through a store and add the response onto the job
    Api.createJobStatus(@props.job_id, Consts.ONSITE, onSiteTime.toISOString(), {
      success: =>
        @props.callback()
        this.props.hideModals()
      error: =>
        Utils.sendNotification('On Site Time failed to save', Consts.ERROR)
        @setState
          posting: false
    })

  onCancel: ->
    this.props.hideModals()

  getEstimatedOnSite: () ->
    toa = JobStore.get(@props.job_id, 'toa', @getId())
    moment(toa.live || toa.latest)

  getOnSiteTime: () ->
    @getEstimatedOnSite().add(@state.delay, 'minutes')

  render: ->
    super(arguments...)

    dispatched = JobStore.get(@props.job_id, "history.#{Consts.DISPATCHED}", @getId())
    estimatedOnSite = @getEstimatedOnSite()

    if moment().isBefore(estimatedOnSite)
      return null

    # check if someone else set On Site status in parallel
    if JobStore.get(@props.job_id, "history.#{Consts.ONSITE}", @getId())
      error = 'Someone else just set On Site status for this Job'
      hideConfirm = true
    else
      error = @state.error
      hideConfirm = false

    if @props.status == Consts.ONSITE
      title = "Confirm On Site Time"
    else
      title = "On Site Time Required"

    TransitionModal
      show: true
      title: title
      extraClasses: "add_on_site_status"
      onConfirm: @onConfirm
      onCancel: @onCancel
      confirm: "Save"
      hideConfirm: hideConfirm
      if @state.posting
        Loading null
      else
        div null,
          if dispatched
            dispatched = moment(dispatched.adjusted_dttm || dispatched.last_set)
            # floor dates to minutes to avoid inaccurate round confusions
            eta = moment.duration(estimatedOnSite.startOf('minute').diff(dispatched.startOf('minute')))
            [
              div
                className: 'on_site-row'
                span
                  className: 'on_site-field'
                  'Dispatched: '
                Utils.formatDateTime(dispatched, true)
              div
                className: 'on_site-row'
                span
                  className: 'on_site-field'
                  'ETA: '
                Utils.formatMomentDuration(eta)
            ]
          div
            className: 'on_site-row'
            span
              className: 'on_site-field'
              'Estimated On Site: '
            Utils.formatDateTime(estimatedOnSite, true)
          div
            className: 'form-inline' + (if error then ' has-error' else '')
            span
              className: 'prompt'
              'Please confirm what time the driver arrived On Site.'
            Select
              defaultValue:
                label: 'On time'
                value: "#{@state.delay}"
              value: @state.selected
              height: 10
              onChange: (option) =>
                @setState(
                  { selected: option, delay: parseInt(option.value) },
                  () => # when delay updated
                    if @state.attemptedSubmit
                      @checkAndSetError()
                )
              options: [
                (for mins in [-30..-5] by 5
                  value: "#{mins}", label: "#{-mins} minutes early")...
                value: "0", label: 'On time'
                (for mins in [5..55] by 5
                  value: "#{mins}", label: "#{mins} minutes late")...
                (for totalMins in [60..240] by 15
                  hours = totalMins/60|0
                  mins = totalMins % 60
                  s = if hours > 1 then 's' else ''
                  mins = if mins > 0 then "#{mins} minutes " else ''
                  value: "#{totalMins}", label: "#{hours} hour#{s} #{mins}late")...
              ]
          if error
            label
              className: 'error'
              error

module.exports = withModalContext(AddOnSiteStatus)
