import React from 'react'
import Logger from 'lib/swoop/logger'
EditForm = React.createFactory(require('components/edit_form'))
#TODO: if a change comes in, show an error that something has changed and to refresh
import { extend, sortBy } from 'lodash'

VehicleCategoryStore = require('stores/vehicle_category_store')
BaseComponent = require('components/base_component')

class TrailerForm extends BaseComponent
  displayName: 'TrailerForm'

  render: ->
    super(arguments...)

    categories = VehicleCategoryStore.getAllSorted(@getId())
    categoryOptions = [{value: null, text: "- Select -"}]

    categories = sortBy(categories, "order_num")
    for category in categories
      categoryOptions.push({value: category.id, text: category.name})
    data = [
      {
        name: "name"
        label: "Name*"
        type: "text"
        required: true
      }
      {
        name: "make"
        label: "Make"
        type: "text"
        classes: "right"
      }
      {
        name: "model"
        label: "Model"
        type: "text"
      }
      {
        name: "vehicle_category_id"
        label: "Type"
        classes: "right"
        type: "selector"
        options: categoryOptions
      }
    ]
    title = "Create New Trailer"
    if @props.record? and @props.record.id?
      title = "Edit Trailer"

    Logger.debug("title: ", title)
    EditForm extend({}, @props,
      cols: [{fields:data}]
      title: title
    )

module.exports = TrailerForm
