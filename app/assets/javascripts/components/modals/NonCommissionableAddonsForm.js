import { sortBy, values } from 'lodash'
import AllObjectsForm from 'components/modals/AllObjectsForm'
import ServiceStore from 'stores/service_store'
import CommissionStore from 'stores/commission_store'
import { withModalContext } from 'componentLibrary/Modal'

class NonCommissionableAddonsForm extends AllObjectsForm {
  displayName = 'NonCommissionableAddonsForm'

  _changeFunction = CommissionStore.updateAddonCommissionsExclusions

  _store = ServiceStore

  _selectedItemsFunction = CommissionStore.companyContainsCommissionExclusionAddon

  _objTitle = 'Non Commissionable Items'

  _placeholder = 'items'

  getAll() {
    return sortBy(values(this._store.getAddons(this.getId())), addon =>
      addon.name.toLowerCase(),
    )
  }
}

export const Component = NonCommissionableAddonsForm
export default withModalContext(NonCommissionableAddonsForm)
