import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
UserStore = require ('stores/user_store')
CustomerTypeStore = require('stores/customer_type_store')
import Consts from 'consts'
JobFields = require('job_fields')
Renderer = require('form_renderer')
import { extend } from 'lodash'

BaseComponent = require('components/base_component')

CTypeIdNoTBDField =class CTypeIdNoTBDField extends JobFields.CustomerTypeIdField
  findErrors: ->
    if CustomerTypeStore.get(@record.customer_type_id, "name", @props.componentId) == "TBD"
      return "Invalid"
    super(arguments...)

class SetCustomerType extends BaseComponent

  displayName: 'SetCustomerType'
  constructor: (props) ->
    super(props)
    @form = new Renderer(@props.record, @rerender)
    @form.registerFields(CustomerTypeIdField: CTypeIdNoTBDField)


  onConfirm: =>
    if @form.isValid()
      newrecord = @form.getRecordChanges()
      @props.onConfirm newrecord

    else
      @form.attemptedSubmit()
      @rerender()

  render: ->
    super(arguments...)
    TransitionModal extend({}, @props,
      callbackKey: 'delete'
      transitionName: 'SetCustomerTypeTransition'
      title: "Please Update Payment Type"
      extraClasses: "customer_type_picker"
      onConfirm: @onConfirm
      style:
        textAlign: "left"
      confirm: 'OK'),
      div null,
        span
          className: 'inline-block'
          style:
            marginBottom: 10
          Consts.MESSAGE_PAYMENT_TYPE_TBD
        if @props.show
          @form.renderInput("customer_type_id", {containerStyle:
            {
              display: "inline-block"
              width: 150
            }
          })

module.exports = SetCustomerType
