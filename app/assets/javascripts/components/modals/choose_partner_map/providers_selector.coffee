import Api from 'api'

import React from 'react'
RecommendedProvidersStore = require('stores/recommended_providers_store')
StoreSelector = React.createFactory(require('components/storeSelector'))
BaseComponent = require('components/base_component')
BackendSuggest = React.createFactory require('components/autosuggest/backend_suggest').default
import { extend, map } from 'lodash'

class ProvidersSelector extends BaseComponent
  render: ->
    super(arguments...)

    BackendSuggest
      placeholder: "Type or select other partner"

      style:
        height: 32

      onSuggestSelect: (recommendedProvider) =>
        if recommendedProvider
          RecommendedProvidersStore.addIfNotLoaded(recommendedProvider, @props.record)
          @props.onChange(recommendedProvider.id)

      getSuggestions: (options, callback) =>
        if !UserStore.isSwoop() && !UserStore.isFleetInHouse()
          return
        Api.autocomplete_recommended_providers(options.input, 10, UserStore.getEndpoint(), {
          success: (data) =>
            locationMap = map(data, (recommendedProvider) ->
              label = recommendedProvider.name + " (" + recommendedProvider.job_distance + " miles)"
              extend({}, recommendedProvider, {label: label})
            )
            callback(locationMap)
          error: (data, textStatus, errorThrown) =>
           console.warn("Autocomplete failed")
        }, {job_id: @props.record?.id})

module.exports = ProvidersSelector
