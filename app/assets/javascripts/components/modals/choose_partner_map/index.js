import React from 'react'
import Dispatcher from 'lib/swoop/dispatcher'
import JobDetailsOverlay from 'components/JobDetailsOverlay/forChoosePartnerModal'
import Utils from 'utils'
import { find, maxBy, partial } from 'lodash'
import { withModalContext } from 'componentLibrary/Modal'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import AuctionStore from 'stores/auction_store'
import BaseComponent from 'components/base_component'
import JobStore from 'stores/job_store'
import ModalStore from 'stores/modal_store'
import RecommendedProvidersStore from 'stores/recommended_providers_store'
import Renderer from 'form_renderer'
import TagStore from 'stores/tag_store'
import TransitionModal from 'components/modals_new'
import UserStore from 'stores/user_store'
import SwoopMap from 'components/maps/SwoopMap'
import Timer from 'components/timer'
import JobMarkers from './JobMarkers'
import ChoosePartnerProvidersSelector from './providers_selector'
import ETAField from './ETAField'
import EmailDetailsField from './EmailDetailsField'
import BillingTypeField from './BillingTypeField'
import AchDescriptionField from './AchDescriptionField'
import VccCostField from './VccCostField'
import ProviderMarker from './ProviderMarkerNew'
import RecommendedProviderList from './RecommendedProviderList'
import CallPartnerModal from './CallPartner'
import 'stylesheets/components/modals/choose_partner_map.scss'
import VehicleCategoryStore from 'stores/vehicle_category_store'

class ChoosePartner extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      extra_ids: [],
      chosen_id: null,
      partners: [],
      error: null,
    }
    this.setupForm(props)
    this.form = new Renderer(null, () => {})
  }

  onConfirm = () => {
    if (this.form.isValid(true)) {
      let newrecord, partner
      if (this.state.chosen_id != null) {
        newrecord = { job: this.form.getRecordChanges() }
        partner = find(RecommendedProvidersStore.getRecommendedProviders(), { id: this.state.chosen_id })
      }

      const job = this.props.record
      const { onConfirm } = this.props

      if (partner) {
        if (partner.open && partner.company_live) {
          return onConfirm(newrecord)
        } else {
          Dispatcher.send(ModalStore.CLOSE_MODALS)
          ModalStore.closeModals()
          this.props.showModal(
            <CallPartnerModal
              dispatch_email={this.form.record.dispatch_email}
              job_id={job.id}
              onCancel={() => {
                this.props.hideModals()
                setTimeout(() => Dispatcher.send(ModalStore.SHOW_MODAL, {
                  key: 'partner',
                  props: { record: job },
                }
                ), 0)
              }}
              onConfirm={() => {
                onConfirm(newrecord)
                this.props.hideModals()
              }}
              partner={partner}
            />
          )
        }
      }
    } else {
      this.form.attemptedSubmit()
      this.rerender()
    }
  }

  static cleanupBeforeSend(newrecord) {
    /* eslint-disable-next-line no-param-reassign */
    delete newrecord.question_results
  }

  setupForm(props) {
    this.form = new Renderer(props.record || {}, this.rerender, {
      form_type: props.form_type,
      aboutToSend: ChoosePartner.cleanupBeforeSend,
      componentId: this.getId(),
    })
    this.form.register(ETAField, 'temp_eta_mins')
    this.form.register(EmailDetailsField, 'dispatch_email')
    this.form.register(BillingTypeField, 'billing_type')
    this.form.register(VccCostField, 'vcc_amount')
    this.form.register(AchDescriptionField, 'ach_description')
    this.form.isValid()

    // HACK: Gets the largest id of provider (shoudld be the one just created)
    this.shouldSelectLargest = !!props.chose_largest
  }

  UNSAFE_componentWillReceiveProps() {
    // if showing for the first time and received new props then do the call again

    // TODO: SHOULD UPDATE RECORD changes

    // IF LOCATIONS CHANGE UPDATE RECORDS
    // TODO: this will eventually include drop location and service
    const job = JobStore.getJobById(this.form.record.id)

    if (
      this.form.record?.service_location?.lat !== job?.service_location?.lat ||
      this.form.record?.service_location?.lng !== job?.service_location?.lng
    ) {
      // refetch providers
      RecommendedProvidersStore.clearRecommendedProviders()

      // Hack until we update record with changes set it manually to the new address
      this.form.record.service_location = job.service_location
      this.form.orig_record.service_location = job.service_location
    }
  }


  componentDidMount() {
    super.componentDidMount()

    if (AuctionStore.jobHasLiveAuction(this.props.record)) {
      AuctionStore.bind(`${AuctionStore.AUCTION_RAN_OUT}_${this.props.record.id}`, this.rerender)
    }

    this.setupForm(this.props)
  }

  componentWillUnmount() {
    super.componentWillUnmount()

    RecommendedProvidersStore.clearRecommendedProviders()

    if (this.props.record.auction) {
      AuctionStore.unbind(`${AuctionStore.AUCTION_RAN_OUT}_${this.props.record.auction.id}`, this.rerender)
    }
  }


  enableSend() {
    if (this.state.loadingProvider || !this.state.chosen_id) {
      return false
    }

    return true
  }

  getTransitionModalProps(record) {
    let appendToLeft = null
    if (AuctionStore.jobHasLiveAuction(record)) {
      appendToLeft =
        <div className="choose-partner-timer-message">
          <span>Still Collecting ETAs: </span>
        </div>
    }

    let footerView
    if (UserStore.isSwoop() && AuctionStore.jobHasAuction(record) && !record?.rescue_company && !AuctionStore.jobHasUnsuccessfulAuction(record)) {
      footerView = <Timer
        appendToLeft={appendToLeft}
        className="timer-eta choose-partner-timer "
        color="#fff"
        expiresAt={AuctionStore.getExpiresAtByJob(record)}
        iconExtraClasses="choose-partner-timer-icon "
        showExpired={!AuctionStore.jobHasLiveAuction(record)}
      />
    }

    return {
      callbackKey: 'choose_partner_map',
      transitionName: 'ChoosePartnerTransition',
      confirm: 'Assign',
      cancel: 'Cancel',
      enableSend: this.enableSend(record),
      title: 'Assign Partner',
      extraClasses: 'choose_partner_map',
      onConfirm: this.onConfirm,
      footerView,
      rightComponent: <span className="job-id">{record?.id ? `#${record?.id}` : ''}</span>,
    }
  }

  getProviders(job, watcher = this.getId(), id = this.state?.chosen_id) {
    const list = RecommendedProvidersStore.getSortedList(RecommendedProvidersStore.SORT_BY_CUSTOM_AMY, job, watcher, id)

    // TODO: HACK: This assumes that the created company is returned by the server as recommended, will choose the wrong one otherwise
    if (this.shouldSelectLargest && list && list.length > 0) {
      this.shouldSelectLargest = false
      const provider = maxBy(list, prov => prov.id)
      // Hack to get it to scroll to the right position
      setTimeout(partial(this.selectAndScrollTo, provider), 0)
    }
    return list
  }

  selectAndScrollTo = (partner) => {
    // Scroll to position
    this.selectAndScrollToId(partner.id)
  }

  selectAndScrollToId = (id) => {
    this.handlePartnerChange(id)
    this.providerList?.scrollToId(id)
  }

  markerClose = () => {
    this.handlePartnerChange(null)
  }

  handlePartnerChange = (id) => {
    let billing_type, providerDispatchEmail
    this.form.record.rescue_provider_id = id

    if (id) {
      const provider = find(RecommendedProvidersStore.getRecommendedProviders(), { id })

      if (!provider) {
        Rollbar.error('Selected partner does not exist in the store')
      }

      providerDispatchEmail = provider?.dispatch_email || provider?.rescue_provider_dispatch_email
      billing_type = provider?.billing_type
    }

    if (providerDispatchEmail) {
      this.form.record.dispatch_email = providerDispatchEmail
    } else {
      this.form.record.dispatch_email = null
    }

    if (UserStore.isSwoop()) {
      this.form.record.billing_type = billing_type || null
      delete this.form.record.vcc_amount
    }

    this.form.onChange()
    this.setState({
      chosen_id: id,
      error: null,
    })
  }


  generateMarkers(providers, record) {
    const ret = []
    ret.push(<JobMarkers jobId={record.id} key={`markersFor-${record.id}`} />)
    if (providers) {
      providers.forEach((partner) => {
        const tagColor = TagStore.get(partner?.tags ? partner.tags[0] : undefined, 'color', this.getId()) || '#6C6C6C'
        ret.push(
          <ProviderMarker
            chosen={this.state.chosen_id === partner.id}
            color={tagColor}
            job={record}
            key={partner.id}
            onClick={this.selectAndScrollTo}
            onInfoWindowClickClose={this.markerClose}
            partner={partner}
          />
        )
      })
    }
    return ret
  }

  render() {
    let providers
    super.render()

    if (!this.props.show) {
      return null
    }

    const { record } = this.props

    const rescue_company = JobStore.get(record.id, 'rescue_company', this.getId())

    if (rescue_company) {
      Dispatcher.send(ModalStore.SHOW_MODAL, {
        key: 'simple',
        props: {
          title: 'Job Already Assigned',
          confirm: 'OK',
          body: 'This job has already been assigned to a partner',
          onConfirm: () => Dispatcher.send(ModalStore.CLOSE_MODALS),
          onCancel: () => Dispatcher.send(ModalStore.CLOSE_MODALS),
        },
      })
      return null
    }

    let tempProvider = null
    if (record) {
      providers = this.getProviders(record)
      if (providers) {
        providers.forEach((partner) => {
          if (this.state.chosen_id === partner.id) {
            tempProvider = partner
          }
        })
      }
    }

    let class_type = null
    if (record.invoice_vehicle_category_id) {
      class_type = VehicleCategoryStore.get(record.invoice_vehicle_category_id, 'name', this.getId())
    }

    if (!class_type) {
      class_type = null
    } else {
      class_type = <span className="ChoosePartner-classTypeDisplay">{class_type}</span>
    }

    let classField = null
    if (class_type) {
      classField = (
        <div className="ChoosePartner-classTypeField">
          <span className="ChoosePartner-classTypeLabel">Class Type</span>
          {class_type}
        </div>
      )
    }

    return (
      <TransitionModal {...this.props} {...this.getTransitionModalProps(record)}>
        {this.props.show && record &&
          <div className="panelContainer">
            <div className="leftPanel">
              <JobDetailsOverlay job_id={record.id} />
              <SwoopMap
                onLoad={map => map.setOptions({
                  center: {
                    lat: JobStore.get(record.id, 'service_location.lat', this.getId()),
                    lng: JobStore.get(record.id, 'service_location.lng', this.getId()),
                  },
                  zoom: 11,
                })}
              >
                {this.generateMarkers(providers, record)}
              </SwoopMap>
            </div>
            <div className="rightPanel">
              <div className="select_partner_title">
                <span>Select Partner</span>
                {UserStore.isSwoop() &&
                  <FontAwesomeButton
                    color="secondary"
                    icon="fa-plus"
                    onClick={e => Utils.showModal('company', e, {
                      force_partner: true,
                      callback: () => Utils.showModal('partner', e, { record, chose_largest: true }),
                    })}
                    style={{
                      float: 'right',
                    }}
                  />}
              </div>
              <ChoosePartnerProvidersSelector
                onChange={this.selectAndScrollToId}
                record={record}
              />
              <RecommendedProviderList
                getProviders={this.getProviders}
                id={this.state.chosen_id}
                job_id={record.id}
                loaded={RecommendedProvidersStore._isLoaded}
                onChange={this.handlePartnerChange}
                ref={providerList => this.providerList = providerList}
              />
              <div className="input_container">
                {classField}
                <div>
                  {this.form.renderInput('billing_type', { partner: tempProvider })}
                  {this.form.renderInput('vcc_amount', { partner: tempProvider })}
                  {this.form.renderInput('ach_description', { partner: tempProvider })}
                  {this.form.renderInput('temp_eta_mins', { partner: tempProvider })}
                  {this.form.renderInput('dispatch_email', { partner: tempProvider })}
                </div>
              </div>
            </div>
          </div>}
      </TransitionModal>
    )
  }
}

export default withModalContext(ChoosePartner)
