import React from 'react'
import { findIndex, partial, partition, sortBy, union } from 'lodash'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

AuctionStore = require('stores/auction_store').default
BaseComponent = require('components/base_component')
JobStore = require('stores/job_store')
import ProviderContent from './ProviderContent'
RecommendedProvidersStore = require('stores/recommended_providers_store')
import TagStore from 'stores/tag_store'
require('stylesheets/components/recommended_provider_list.scss')

class RecommendedProviderList extends BaseComponent
  constructor: (props) ->
    super(props)

    @state = {
      providersLimit: 5
    }

  componentDidMount: ->
    super(arguments...)

    job = JobStore.getById(@props.job_id)

    if AuctionStore.jobHasLiveAuction(job)
      # We are saving the auction ID for unbinding in case the job gets removed from the store
      @boundAuctionId = job?.auction?.id
      AuctionStore.bind(AuctionStore.AUCTION_RAN_OUT + '_' + @boundAuctionId, @rerender)
      RecommendedProvidersStore.watchBidChangesFor(job)

    @updateProvidersList()

  componentDidUpdate: ->
    @updateProvidersList()

  componentWillUnmount: ->
    super(arguments...)

    if @boundAuctionId
      AuctionStore.unbind(AuctionStore.AUCTION_RAN_OUT + '_' + @boundAuctionId, @rerender)
      @boundAuctionId = null

  updateProvidersList: =>
    currentProviderIndex = findIndex(@recommendedProviders, (provider) => provider.id == @props.id)
    if currentProviderIndex != -1 && @state.providersLimit < currentProviderIndex + 1
      @setState
        providersLimit: currentProviderIndex + 1

  getRecommendedProviders: =>
    job = JobStore.getById(@props.job_id, false, @getId())
    recommendedProviders = @props.getProviders(job, @getId(), @props.id) || []
    return @sortProvidersByNetworkStatus(recommendedProviders)

  partitionProvidersByTagName: (providers, tag) =>
    partition(providers, (provider) =>
      TagStore.get(provider?.tags[0], "name", @getId())?.toLowerCase().indexOf(tag) >= 0
    )

  sortProvidersByNetworkStatus: (recommendedProviders) =>
    # Sort Providers by Distance, then Network Status
    allProviders = partition(recommendedProviders, (provider) => provider.job_distance <= 15)

    # within 15 miles
    nearbyProviders = sortBy(allProviders[0], (provider) => provider.job_distance)

    # tagged as "Primary"
    primaryProviders = @partitionProvidersByTagName(nearbyProviders, 'primary')

    # tagged as "Secondary"
    secondaryProviders = @partitionProvidersByTagName(primaryProviders[1], 'secondary')

    # tagged as "Tertiary"
    tertiaryProviders = @partitionProvidersByTagName(secondaryProviders[1], 'tertiary')

    # tagged as "OoN Preferred" + Remaining results
    outOfNetworkProviders = @partitionProvidersByTagName(tertiaryProviders[1], 'oon preferred')

    # more than 15 miles
    distantProviders = sortBy(allProviders[1], (provider) => provider.job_distance)

    # put the groups together in order
    recommendedProviders = union(primaryProviders[0], secondaryProviders[0], tertiaryProviders[0], outOfNetworkProviders[0], outOfNetworkProviders[1], distantProviders)

  scrollToId: (id, count=0) =>
    if @refs?["provider_"+id]?
      @refs?["provider_"+id].scrollIntoView()
    else if count < 30
      #HACK TO WAIT UNTIL REF IS CREATED
      setTimeout(partial(@scrollToId, id, count++), 100)

  handleSelectRow: (partner) =>
    @props.onChange(partner.id, partner.name)

  handleAddProvider: (e) =>
    @setState
      providersLimit: @state.providersLimit + 1

  render: ->
    super(arguments...)

    @recommendedProviders = @getRecommendedProviders()
    displayedProviders = @recommendedProviders.slice(0, @state.providersLimit)

    job = JobStore.getById(@props.job_id, false, @getId())

    if @recommendedProviders.length > 0
      ul
        className: "recommendedProviderList"
        for provider in displayedProviders
          li
            className: if provider.id == @props.id then " selected" else ""
            key: "provider_"+provider.id
            ref: "provider_"+provider.id
            <ProviderContent
              provider={provider}
              hasAuction={AuctionStore.jobHasAuction(job)}
              job_id={job.id}
              onClick={@handleSelectRow}
            />
        div
          className: "show-next-partner-button"
          Button
            color: "secondary"
            disabled: displayedProviders.length == @recommendedProviders.length
            label: if displayedProviders.length == @recommendedProviders.length then "No More Partners" else "Show Next Partner"
            onClick: @handleAddProvider
            size: "full-width"

    else if RecommendedProvidersStore._isLoaded
      div
        style:
          marginTop: 30
          color: "#ccc"
        "No partners within 60 miles"
    else
      Loading null

export default RecommendedProviderList
