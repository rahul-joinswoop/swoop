import React from 'react'
import AuctionStore from 'stores/auction_store'
import BaseComponent from 'components/base_component'
import InfoWindow from 'components/rmap/info_window'
import Marker from 'components/rmap/marker'
import ProviderContent from './ProviderContent'

class ProviderMarker extends BaseComponent {
  getProviderInfoWindowContent = () => {
    const { job, partner } = this.props

    return <ProviderContent
      hasAuction={AuctionStore.jobHasAuction(job)}
      job_id={job.id}
      provider={partner}
    />
  }

  onClick = () => {
    this.props.onClick(this.props.partner)
  }

  render() {
    super.render()

    const {
      color, chosen, job, map, onInfoWindowClickClose, partner,
    } = this.props

    if (partner?.location) {
      return (
        <Marker
          color={color}
          forceShowInfoWindow={chosen}
          lat={partner.location?.lat}
          lng={partner.location?.lng}
          map={map}
          onClick={this.onClick}
          onInfoWindowClickClose={onInfoWindowClickClose}
        >
          <InfoWindow
            getContent={this.getProviderInfoWindowContent}
            job={job}
            provider={partner}
          />
        </Marker>
      )
    } else {
      return null
    }
  }
}

export default ProviderMarker
