import React, { useEffect, useState } from 'react'
import $ from 'jquery'
import Consts from 'consts'
import BaseComponent from 'components/base_component'
import JobStore from 'stores/job_store'
import { useGoogleMap, Polyline } from '@react-google-maps/api'
import 'stylesheets/components/rmap/job_markers.scss'

const ACTIVE_DIRECTIONS = '#68B5DF'
const INACTIVE_DIRECTIONS = '#A0A0A0'

const Marker = ({
  bounceTimes = 2, children, image, position,
}) => {
  const map = useGoogleMap()

  const [marker] = useState(() => {
    const options = {
      icon: {
        url: image,
        labelOrigin: new google.maps.Point(13, 15),
      },
      map,
      position,
    }

    if (bounceTimes) {
      options.animation = google.maps.Animation.BOUNCE
    }

    return new google.maps.Marker(options)
  })

  useEffect(() => {
    if (bounceTimes) {
      setTimeout(() => marker.setAnimation(null), 750 * bounceTimes)
    }
    return () => marker.setMap(null)
  }, [bounceTimes, marker])

  useEffect(() => marker.setPosition(position), [marker, position])

  /* eslint-disable no-param-reassign */
  if (children) {
    children = React.Children.only(children)
    children = React.cloneElement(children, { map, marker })
  }

  return children || null
}

class DistanceToDropOff extends BaseComponent {
  constructor(props) {
    super(props)
    this.info = new google.maps.InfoWindow()
  }

  getElId() {
    return `${this.props.jobId}-distance`
  }

  getDistance() {
    const distance = JobStore.get(this.props.jobId, 'distance', this.getId())

    if (distance) {
      return `<div id="${this.getElId()}">${distance} miles</div>`
    } else {
      return ''
    }
  }

  componentDidMount() {
    super.componentDidMount()

    const distance = this.getDistance()
    this.info.setContent(distance)

    if (distance) {
      const { marker, map } = this.props
      /* eslint-disable-next-line */
      this.mouseover = marker.addListener('mouseover', () => this.info.open(map, marker))
      this.mouseout = marker.addListener('mouseout', () => this.info.close())
      this.info.addListener('domready', () =>
        // this is durty hack to hide close button, refactor it to use custom popup without close button
        $(`.gm-style-iw:has(#${this.getElId()}) + .gm-ui-hover-effect`,).css('display', 'none')
      )
    }
  }

  componentWillUnmount() {
    super.componentWillUnmount()

    this.mouseover?.remove()
    this.mouseout?.remove()

    this.info.close()
    this.info.setMap(null)
  }

  render() {
    super.render()
    this.info.setContent(this.getDistance())
    return null
  }
}

class JobMarker extends BaseComponent {
  render() {
    super.render()

    const { jobId } = this.props

    const pickupLocation = JobStore.get(jobId, 'service_location', this.getId())
    const dropLocation = JobStore.get(jobId, 'drop_location', this.getId())

    const status = JobStore.get(jobId, 'status', this.getId())

    return (
      <>
        {/* PICKUP PIN */}
        <Marker
          image="/assets/images/marker_a.png"
          key="pickupPin"
          position={pickupLocation}
        />
        {dropLocation && (
          <>
            {/* DROP PIN */}
            <Marker
              image="/assets/images/marker_b.png"
              key="dropoffPin"
              position={dropLocation}
            >
              <DistanceToDropOff jobId={jobId} />
            </Marker>

            {/* A TO B LINE */}
            <Polyline
              key="AtoBLine"
              options={{
                geodesic: true,
                path: [pickupLocation, dropLocation],
                strokeColor: status === Consts.TOWING ? ACTIVE_DIRECTIONS : INACTIVE_DIRECTIONS,
              }}
            />
          </>
        )}
      </>
    )
  }
}

export default JobMarker
