import React from 'react'
import Consts from 'consts'
import Fields from 'fields/fields'
import UserStore from 'stores/user_store'

class AchDescriptionField extends Fields.SpanField {
  _label = null

  _name = 'ach_description'

  /* eslint-disable-next-line */
  getValue() {
    /* eslint-disable-next-line */
    return ['Contracted', <br />, 'Rate']
  }

  isShowing() {
    return UserStore.isSwoop() && this.record.billing_type === Consts.JOB.BILLING_TYPE.ACH
  }
}

export default AchDescriptionField
