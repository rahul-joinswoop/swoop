import React from 'react'
import ReactDOMFactories from 'react-dom-factories'
import Utils from 'utils'
AuctionStore = require('stores/auction_store').default
BaseComponent = require('components/base_component')
JobStore = require('stores/job_store')
import TagStore from 'stores/tag_store'
TireStore = require('stores/tire_store')
TireTypeStore = require('stores/tire_type_store')
UserStore = require('stores/user_store')
require('stylesheets/components/provider_content.scss')

class ProviderContent extends BaseComponent
  onClick: =>
    if @props.onClick
      @props.onClick(@props.provider)

  renderNetworkStatus: =>
    partner = @props.provider
    tagName = null
    tagColor = null

    if partner? and partner.tags.length > 0
      tag_id = partner.tags[0]
      tagName = TagStore.get(tag_id, "name", @getId())
      tagColor = TagStore.get(tag_id, "color", @getId())
    else if UserStore.isSwoop()
      tagName = "Out of Network"
      tagColor = "#6C6C6C"

    if tagName
      span
        className: 'label_highlight'
        style:
          backgroundColor: tagColor
          color: Utils.readableColorOnBG(tagColor)
        tagName

  render: ->
    if @props.provider?
      partner = @props.provider

      tire_type_id = JobStore.get(@props.job_id, "tire_type_id", @getId())

      div
        onClick: @onClick
        className: "provider_content"
        div
          className: 'name'
          partner.name
        if partner?.phone?
          div
            className: 'phone',
            Utils.renderPhoneNumber(partner.phone)
        div
          className: 'attributes',
          if partner? and partner.company_live
            ReactDOMFactories.i
              className: "fa fa-check"
          if partner? and partner.company_live and not partner.open
            ReactDOMFactories.i
              className: "fa fa-phone"
          if partner?
            span
              className: 'label_highlight label_miles'
              if UserStore.isSwoop()
                partner.billing_type
              else
                partner.job_distance + " Miles"
          if UserStore.isSwoop() || UserStore.isFleetInHouse()
            @renderNetworkStatus()
        if tire_type_id
          div
            className: 'tire_info'
            span null,
              TireTypeStore.getBaseTireTypeName(tire_type_id, @getId()) + " Tires: "
            span
              className: 'tire_count'
              TireStore.getTireQuantityBySiteAndType(partner.site_id, tire_type_id, @getId()) || "--"
        if UserStore.isSwoop() and partner?
          div
            className: 'provider-distance'
            "Distance: #{partner.job_distance} miles"
        if @props.hasAuction
          job = JobStore.getById(@props.job_id)
          submitted = AuctionStore.getFormattedSubmittedETA(partner, job)
          siteEta = AuctionStore.getSiteETA(partner, job)
          truckEta = AuctionStore.getTruckETA(partner, job)

          div
            className: 'etas'
            if submitted? && submitted != "--" && submitted != ""
              span null,
                "Submitted ETA: " + submitted
            if siteEta?
              span null,
                "Site ETA: "+ siteEta + " mins"
            if truckEta?
              span null,
                "Truck ETA: "+ truckEta + " mins"

export default ProviderContent
