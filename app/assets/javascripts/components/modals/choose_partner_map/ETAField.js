import JobFields from 'job_fields'
import ProviderStore from 'stores/provider_store'
import UserStore from 'stores/user_store'

class ETAField extends JobFields.TempEtaField {
  _label = 'Enter ETA:'

  _placeholder = 'minutes'

  /* eslint-disable-next-line */
  isRequired() {
    return UserStore.isSwoop()
  }

  beforeSend() {
    if (!this.isShowing()) {
      delete this.record.temp_eta_mins
    } else {
      super.beforeSend()
    }
  }

  isShowing() {
    const showing = super.isShowing()

    if (!showing || !this.record.rescue_provider_id || !this.render_props.partner) {
      return false
    }

    if (!this.render_props.partner.open || !this.render_props.partner.company_live) {
      return true
    }

    const provider = ProviderStore.getById(this.record.rescue_provider_id)

    if (!provider?.company) {
      return false
    }

    // TODO: HACK: partner should not be passed in via render props, instead partner open prop should be merged with provider on receiving
    return !provider?.company.live || !this.render_props.partner.open
  }
}

export default ETAField
