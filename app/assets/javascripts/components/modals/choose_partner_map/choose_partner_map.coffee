import React from 'react'
import Dispatcher from 'lib/swoop/dispatcher'
import JobDetailsOverlay from 'components/JobDetailsOverlay/forChoosePartnerModal'
import Utils from 'utils'
import { extend, find, maxBy, partial } from 'lodash'
import 'stylesheets/components/modals/choose_partner_map.scss'

import { withModalContext } from 'componentLibrary/Modal'

import CallPartnerModalImport from './CallPartner'
CallPartnerModal = React.createFactory(CallPartnerModalImport)
import FontAwesomeButtonImport from 'componentLibrary/FontAwesomeButton'
FontAwesomeButton = React.createFactory(FontAwesomeButtonImport)

AuctionStore = require('stores/auction_store').default
BaseComponent = require('components/base_component')
import ChoosePartnerProvidersSelector from './providers_selector'
JobMarkers = React.createFactory(require('components/rmap/job_markers'))
JobStore = require('stores/job_store')
ModalStore = require('stores/modal_store')
RecommendedProvidersStore = require('stores/recommended_providers_store')
Renderer = require('form_renderer')
SwoopMap = React.createFactory(require('components/rmap/map').default)
import TagStore from 'stores/tag_store'
Timer = React.createFactory(require('components/timer').default)
TransitionModal = React.createFactory(require 'components/modals_new')
UserStore = require('stores/user_store')

ETAField = require('./ETAField').default
EmailDetailsField = require('./EmailDetailsField').default
BillingTypeField = require('./BillingTypeField').default
AchDescriptionField = require('./AchDescriptionField').default
VccCostField = require('./VccCostField').default
import ProviderMarker from './ProviderMarker'
import RecommendedProviderList from './RecommendedProviderList'

class ChoosePartner extends BaseComponent
  displayName: 'ChoosePartner'

  constructor: (props) ->
    super(props)
    @state = @getInitialState()
    @setupForm(props)
    @form = new Renderer(null, =>)

  onConfirm: =>
    if @form.isValid(true)
      if @state.chosen_id?
        newrecord = { job: @form.getRecordChanges() }
        partner = find(RecommendedProvidersStore.getRecommendedProviders(), {id: @state.chosen_id})

      job = @props.record
      chosen_id = @state.chosen_id
      onConfirm = @props.onConfirm

      if partner
        if partner.open and partner.company_live
          return onConfirm(newrecord)
        else
          Dispatcher.send(ModalStore.CLOSE_MODALS)
          ModalStore.closeModals()
          this.props.showModal(
            CallPartnerModal
              dispatch_email: @form.record.dispatch_email
              partner: partner
              job_id: job.id
              onConfirm: =>
                onConfirm(newrecord)
                @props.hideModals()
              onCancel: =>
                @props.hideModals()
                setTimeout =>
                  Dispatcher.send(ModalStore.SHOW_MODAL,
                    key: "partner"
                    props: record: job
                  )
                , 0
          )
      return
    else
      @form.attemptedSubmit()
      @rerender()

  getInitialState: ->
    extra_ids: []
    chosen_id: null
    partners: []
    error: null

  cleanupBeforeSend: (newrecord) ->
    delete newrecord.question_results

  setupForm: (props) ->
    @form = new Renderer(props.record || {}, @rerender,
      form_type: props.form_type
      aboutToSend: @cleanupBeforeSend
      componentId: @getId()
    )
    @form.register(ETAField, "temp_eta_mins")
    @form.register(EmailDetailsField, "dispatch_email")
    @form.register(BillingTypeField, "billing_type")
    @form.register(VccCostField, "vcc_amount")
    @form.register(AchDescriptionField, "ach_description")
    @form.isValid()

    if props.chose_largest
      #HACK: Gets the largest id of provider (shoudld be the one just created)
      @shouldSelectLargest = true
    else
      @shouldSelectLargest = false

  UNSAFE_componentWillReceiveProps: (props) ->
    #if showing for the first time and received new props then do the call again

      #TODO: SHOULD UPDATE RECORD changes

      #IF LOCATIONS CHANGE UPDATE RECORDS
      #TODO: this will eventually include drop location and service
      job = JobStore.getJobById(@form.record.id)

      if @form.record?.service_location?.lat != job?.service_location?.lat || @form.record?.service_location?.lng != job?.service_location?.lng
        #refetch providers
        RecommendedProvidersStore.clearRecommendedProviders()

        #Hack until we update record with changes set it manually to the new address
        @form.record.service_location = job.service_location
        @form.orig_record.service_location = job.service_location

  componentDidMount: ->
    super(arguments...)

    if AuctionStore.jobHasLiveAuction(@props.record)
      AuctionStore.bind(AuctionStore.AUCTION_RAN_OUT + '_' + @props.record.id, @rerender)

    @setupForm(@props)
  providerChanged: =>
    @rerender()

  componentWillUnmount: ->
    super(arguments...)

    RecommendedProvidersStore.clearRecommendedProviders()

    if @props.record.auction
      AuctionStore.unbind(AuctionStore.AUCTION_RAN_OUT + '_' + @props.record.auction.id, @rerender)


  enableSend: (job) =>
    if @state.loadingProvider || !@state.chosen_id
      return false

    return true

  getTransitionModalProps: (record) ->
    appendToLeft = null

    if AuctionStore.jobHasLiveAuction(record)
      appendToLeft =
        div
          className: 'choose-partner-timer-message'
          span
            'Still Collecting ETAs: '

    callbackKey: 'choose_partner_map'
    transitionName: 'ChoosePartnerTransition'
    confirm: 'Assign'
    cancel: 'Cancel'
    enableSend: @enableSend(record)
    title: "Assign Partner"
    extraClasses: "choose_partner_map"
    onConfirm: @onConfirm
    footerView: (if UserStore.isSwoop() && AuctionStore.jobHasAuction(record) && !record?.rescue_company && !AuctionStore.jobHasUnsuccessfulAuction(record) then Timer
        className: 'timer-eta choose-partner-timer '
        expiresAt: AuctionStore.getExpiresAtByJob(record)
        color: '#fff'
        iconExtraClasses: 'choose-partner-timer-icon '
        showExpired: !AuctionStore.jobHasLiveAuction(record)
        appendToLeft: appendToLeft
    )
    rightComponent: span
      className: 'job-id'
      if record?.id then "##{record?.id}" else ""

  getProviders: (job, watcher=@getId(), id=@state?.chosen_id) ->
    list = RecommendedProvidersStore.getSortedList(RecommendedProvidersStore.SORT_BY_CUSTOM_AMY, job, watcher, id)

    #TODO: HACK: This assumes that the created company is returned by the server as recommended, will choose the wrong one otherwise
    if @shouldSelectLargest && list? && list.length > 0
      @shouldSelectLargest = false
      provider = maxBy(list, (provider) ->  provider.id; )
      #Hack to get it to scroll to the right position
      setTimeout(partial(@selectAndScrollTo, provider), 0)
    return list

  selectAndScrollTo: (partner) =>
    #Scroll to position
    @selectAndScrollToId(partner.id)

  selectAndScrollToId: (id) =>
    @handlePartnerChange(id)
    @refs?.providerList?.scrollToId(id)

  markerClose: () =>
    @handlePartnerChange(null)

  handlePartnerChange: (id) =>
    @form.record.rescue_provider_id = id

    if id?
      provider = find(RecommendedProvidersStore.getRecommendedProviders(), { id })

      if !provider
        Rollbar.error("Selected partner does not exist in the store")

      providerDispatchEmail = provider?.dispatch_email || provider?.rescue_provider_dispatch_email
      billing_type = provider?.billing_type

    if providerDispatchEmail
      @form.record.dispatch_email = providerDispatchEmail
    else
      @form.record.dispatch_email = null

    if UserStore.isSwoop()
      @form.record.billing_type = billing_type || null
      delete @form.record.vcc_amount

    @form.onChange()
    @setState
      chosen_id: id
      error: null


  generateMarkers: (providers, record) =>
    ret = []
    ret.push(
      JobMarkers
        job_id: record.id
        key: "markersFor-" + record.id
        showDistance: true
    )
    if providers?
      for partner in providers
        tagColor = TagStore.get(partner?.tags?[0], "color", @getId()) || "#6C6C6C"
        ret.push(
          <ProviderMarker
            chosen={@state.chosen_id == partner.id}
            color={tagColor}
            job={record}
            key={partner.id}
            onClick={@selectAndScrollTo}
            onInfoWindowClickClose={@markerClose}
            partner={partner}
          />
        )
    return ret

  render: ->
    super(arguments...)

    if not @props?.show
      return null

    record = @props.record

    rescue_company = JobStore.get(record.id, "rescue_company", @getId())

    if rescue_company?
      Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "simple",
        props: {
          title: "Job Already Assigned"
          confirm: 'OK'
          body: "This job has already been assigned to a partner"
          onConfirm: =>
            Dispatcher.send(ModalStore.CLOSE_MODALS)
          onCancel: =>
            Dispatcher.send(ModalStore.CLOSE_MODALS)
        }
      )
      return null

    tempProvider = null
    if record?
      providers = @getProviders(record)
      if providers?
        for partner in providers
          if @state.chosen_id == partner.id
            tempProvider = partner

    TransitionModal extend({}, @props,
      @getTransitionModalProps(record)
      ),
      if @props.show && record?
        div
          className: 'panelContainer'
          div
            className: "leftPanel",

            <JobDetailsOverlay job_id={record.id} />
            SwoopMap
              ref: "map"
              initialCenterLat: JobStore.get(record.id, "service_location.lat", @getId())
              initialCenterLng: JobStore.get(record.id, "service_location.lng", @getId())
              offsetY: 100
              initialZoom: 11
              @generateMarkers(providers, record)

          div
            className: "rightPanel",
            div
              className: "select_partner_title"
              span null,
                "Select Partner"
              if UserStore.isSwoop()
                FontAwesomeButton
                  color: "secondary"
                  icon: "fa-plus"
                  onClick: (e) =>
                    Utils.showModal("company", e, {
                      force_partner: true,
                      callback: =>
                        Utils.showModal('partner', e, record: record, chose_largest: true )
                    })
                  style: {
                    float: 'right',
                  }
            <ChoosePartnerProvidersSelector
              onChange={@selectAndScrollToId}
              record={record}
            />
            <RecommendedProviderList
              getProviders={@getProviders}
              id={@state.chosen_id}
              job_id={record.id}
              loaded={RecommendedProvidersStore._isLoaded}
              onChange={@handlePartnerChange}
              ref='providerList'
            />
            div
              className: "input_container"
              @form.renderInput("billing_type", {partner: tempProvider})
              @form.renderInput("vcc_amount", {partner: tempProvider})
              @form.renderInput("ach_description", {partner: tempProvider})
              @form.renderInput("temp_eta_mins", {partner: tempProvider})
              @form.renderInput("dispatch_email", {partner: tempProvider})

module.exports = withModalContext(ChoosePartner)
