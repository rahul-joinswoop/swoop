import React, { useEffect, useState } from 'react'
import AuctionStore from 'stores/auction_store'
import Consts from 'consts'
import { useGoogleMap, InfoWindow } from '@react-google-maps/api'
import ProviderContent from './ProviderContent'

const ProviderMarker = ({
  color, chosen, job, onClick, onInfoWindowClickClose, partner,
}) => {
  const map = useGoogleMap()

  const [marker] = useState(() => new google.maps.Marker({
    icon: {
      path: Consts.GENERIC_MARKER_PATH,
      fillColor: color,
      strokeColor: '#565656',
      fillOpacity: 1,
      anchor: new google.maps.Point(26 / 2, 37),
      strokeWeight: 1,
      scale: 1,
    },
    map,
    position: partner?.location,
  }))

  useEffect(() => marker.setPosition(partner?.location), [marker, partner])

  useEffect(() => {
    const listener = marker.addListener('click', () => onClick(partner))
    return () => listener.remove()
  }, [marker, onClick, partner])

  return chosen && (
    <InfoWindow anchor={marker} onCloseClick={onInfoWindowClickClose}>
      <ProviderContent
        hasAuction={AuctionStore.jobHasAuction(job)}
        job_id={job.id}
        provider={partner}
      />
    </InfoWindow>
  )
}

export default ProviderMarker
