import React from 'react'
import BaseComponent from 'components/base_component'
import SimpleModal from 'components/modals/simple_modal'
import Utils from 'utils'
import JobStore from 'stores/job_store'
import { withModalContext } from 'componentLibrary/Modal'

class CallPartnerModal extends BaseComponent {
  renderActionMessage = () => {
    const { partner, dispatch_email } = this.props
    if (dispatch_email) {
      return (
        <div>
          <span style={{ width: 400 }}>
            Job details will be emailed to {dispatch_email}.
          </span>
          <span>
            Call {partner.name} to confirm:
          </span>
        </div>
      )
    } else if (!partner.open) {
      return `Please call ${partner.name} answering service and provide the job details.`
    } else {
      return `Please call ${partner.name} and provide job details, as they are not yet live on Swoop.`
    }
  }

  handleRescueCompanyAlreadySet = () => {
    this.props.showModal(
      <SimpleModal
        body="This job has already been assigned to a partner"
        confirm="OK"
        extraClasses="CallPartner-jobAlreadyAssigned"
        title="Job Already Assigned"
      />
    )
    return null
  }

  renderBody(props) {
    const { partner, dispatch_email } = props
    return (
      <div>
        <i
          className={!partner.open ? 'fa fa-phone' : 'fa fa-exclamation-triangle'}
        />
        <div>
          <div>
            {this.renderActionMessage()}
            {!dispatch_email && (
              <div className="call">
                Call Dispatch Line
              </div>
            )}
            <div className="phone">
              {Utils.renderPhoneNumber(partner.phone)}
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    super.render()

    const {
      onConfirm, job_id, onCancel,
    } = this.props

    // Partner (rescue_company) has a job if they created the job.
    const rescue_company = JobStore.get(job_id, 'rescue_company', this.getId())

    if (rescue_company) {
      return this.handleRescueCompanyAlreadySet()
    }

    return (
      <SimpleModal
        body={this.renderBody(this.props)}
        confirm="OK"
        extraClasses="CallPartner call_partner_modal"
        onCancel={onCancel}
        onConfirm={onConfirm}
        title="Call Partner"
      />
    )
  }
}

export default withModalContext(CallPartnerModal)

export const Component = CallPartnerModal
