import { map } from 'lodash'
import Consts from 'consts'
import Fields from 'fields/fields'
import UserStore from 'stores/user_store'

class BillingTypeField extends Fields.SelectField {
  _label = 'Invoice:'

  _name = 'billing_type'

  _required = true

  getOptions() {
    return this.optionMapper(map(Consts.JOB.BILLING_TYPE, (value, key) => ({ id: key, name: value })), [])
  }

  isShowing() {
    return UserStore.isSwoop() && this.record.rescue_provider_id
  }
}

export default BillingTypeField
