import React from 'react'
import Consts from 'consts'
import Fields from 'fields/fields'
UserStore = require('stores/user_store')

class VccCostField extends Fields.MoneyField
  _label: null
  _name: 'vcc_amount'
  _required: true

  isShowing: -> UserStore.isSwoop() and @record.billing_type == Consts.JOB.BILLING_TYPE.VCC

export default VccCostField
