import React from 'react'

import SimpleModal from 'components/modals/simple_modal'

import 'stylesheets/components/modals/simple_field.scss'
import BaseComponent from 'components/base_component'

import Field from 'fields/field'
import Renderer from 'form_renderer'

class ValueField extends Field {
  _name = 'value'

  _required = true

  getLabel() {
    return this.render_props.label
  }
}

class SimpleFieldModal extends BaseComponent {
  constructor(props) {
    super(props)
    this.form = new Renderer(
      {
        value: props.initialValue,
      },
      this.rerender,
      {
        componentId: this.getId,
      },
    )
    this.form.registerFields({
      ValueField,
    })
  }

  onConfirm = () => {
    if (this.form.isValid()) {
      const newrecord = this.form.getRecordChanges()
      return this.props.onConfirm(newrecord.value)
    } else {
      this.form.attemptedSubmit()
      return this.rerender()
    }
  }

  render() {
    return (
      <SimpleModal
        body={this.form.renderInput('value', {
          label: this.props.label,
        })}
        className="simple_field"
        confirm={this.props.confirm}
        onCancel={this.props.onCancel}
        onConfirm={this.onConfirm}
        title={this.props.title}
      />
    )
  }
}

export default SimpleFieldModal
