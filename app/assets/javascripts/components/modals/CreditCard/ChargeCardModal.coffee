import React from 'react'
require('stylesheets/components/modals/CreditCard/ChargeCardModal.scss')
BaseComponent = require('components/base_component')
AsyncRequestStore = require('stores/async_request_store').default
import SimpleModalImport from 'components/modals/simple_modal'
SimpleModal = React.createFactory(SimpleModalImport)
InvoiceStore = require('stores/invoice_store')
JobStore = require('stores/job_store')
FeatureStore = require('stores/feature_store')
Renderer = require('form_renderer')
Fields = require('fields/fields')
import { withModalContext } from 'componentLibrary/Modal'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

import Consts from 'consts'
{ Elements, injectStripe } = require('react-stripe-elements')
CreditCard = React.createFactory(require('components/credit_card'))
Elements = React.createFactory(Elements)
import Utils from 'utils'
import StoreWrapper from 'StoreWrapper'
import StripeLoaded from 'components/StripeLoaderHoC'
import { compose } from 'recompose'
import { getCurrencyClass } from 'lib/localize'

export ChargeCardFields = {
  ChargeAmountField : class ChargeAmountField extends Fields.Field
    _name: "amount"
    _label: "Charge Amount"
    _required: true

    getEditableClass: ->
      super() + getCurrencyClass(@record?.currency)

    forcePrefill: ->
      @setValue(Utils.toFixed(Utils.getInvoiceBalance(@record), 2))

    findErrors: ->
      balance = Utils.getInvoiceBalance(@record)
      value = @getValue()
      amount = parseFloat(value)
      if value? && value.length > 0 && isNaN(amount)
        return "Please enter a valid amount"

      # On succesfull charge, for some misterious reason getValue is returning
      # the element itself, and not the value of the field.
      #
      # So we parse both getValue() and balance as float so this check works as expected
      if parseFloat(@getValue()) > parseFloat(balance)
        return "Amount cannot exceed balance"

      return super(arguments...)

    getValue: ->
      if !@render_props?.editable
        return Utils.formatNumber(Utils.toFixed(super(arguments...), 2), keepSymbol: true)

      return super(arguments...)

    renderError: ->
      error = @getError()
      if error?
        span
          className: "error"
          error
    #TODO: show $ next to charge amount

  InvoiceBalanceField: class InvoiceBalanceField extends Fields.SpanField
    _name: "balance"
    _label: "Invoice Balance"
    getValue: ->
      balance = Utils.getInvoiceBalance(@record)
      Utils.formatMoney(balance, @record.currency)

  CreditCardField: class CreditCardField extends Fields.CreditCardField
    _name: "card"
    _label: "Credit or Debit Card"
    _required: true
    renderError: ->
      if !@render_props.hideErrors
        error = @getError()
        if error?
          span
            className: "error card_error"
            error

    renderInput: ->
      if !@render_props?.editable
        CreditCard
          last4: @render_props?.last4
          brand: @render_props?.brand
      else
        return super(arguments...)

  ZipField: class ZipField extends Fields.ZipField
    _name: "zip"
    _label: "Billing Zip Code"
    _placeholder: "Optional"
    renderError: ->
      error = @getError()
      if error?
        span
          className: "error"
          error

  MemoField: class MemoField extends Fields.Field
    _name: "memo"
    _label: "Memo"
    _placeholder: "Optional"
    isShowing: -> @isEditable() || (@getValue()? and @getValue().length > 0)

  SendReceiptToField: class SendReceiptToField extends Fields.EmailField
    _name: "send_recipient_to"
    _placeholder: "Email (optional)"
    isShowing: -> @isEditable() || (@getValue()? and @getValue().length > 0)
    getLabel: ->
      if @isEditable()
        return "Send Receipt To"
      else
        return "Receipt Sent To"
    renderError: ->
      error = @getError()
      if error?
        span
          className: "error"
          error

    forcePrefill: ->
      @setValue(@props.initialEmailValue)
}


BasicForm = (props) =>
  [form, hideErrors, last4, brand, hidePostalCode] = props.args
  return div null,
    form.renderInputs([
      ["amount",  containerClass: "half_width left money"]
      ["balance", containerClass: "half_width right balance"]
      ["card", {hideErrors, last4, brand, hidePostalCode}]
      "zip" if hidePostalCode
      "memo"
      "send_recipient_to"
    ])


BasicForm = React.createFactory(BasicForm)


class ChargeCardModal extends BaseComponent
  constructor: (props) ->
    super(arguments...)

    #TODO: may need to have a different object then the invoice to represent balance
    @form = new Renderer(props.invoice, @rerender, {
      submit_attempts: 1 #Causes errors to be shown immediately
      initialEmailValue: props.initialEmailValue
    })
    @form.registerFields(ChargeCardFields)

    @state = {
      hideStripeErrors: true
    }

  isSendEnabled: =>
    return @form.isValid()

  sendCharge: =>
    @setState
      showSpinner: true

    #Check if form is valid
    if @form.isValid()
      charge_obj = @form.getRecordChanges()
      charge_obj.amount = parseFloat(charge_obj.amount) # reformat the amount; handles dangling decimal points, i.e. "50."
      extraCreateTokenOptions = {}
      if charge_obj.zip
        extraCreateTokenOptions.address_zip = charge_obj.zip
      @props.stripe.createToken(extraCreateTokenOptions).then((result) =>
        @setState
          brand: result.token?.card?.brand
          last4: result.token?.card?.last4
        if result.error?
          @form.setResponseError(result.error.message)
          @setState
            showSpinner: false
            hideStripeErrors: false
        else
          charge_obj.token = result.token.id
          AsyncRequestStore.chargeInvoice(@props.invoice_id, invoice:charge:charge_obj, {
            success: (data) =>
              @form.setEditable(false)
              @setState
                showSpinner: false
                sent: true
                error: null
            error: (data) =>
              newState = showSpinner: false
              if data?.error?
                if data.error.charge?
                  newState.error = data.error.charge
                else
                  @form.setResponseError(data.error)
                  newState.hideStripeErrors = false

              else
                newState.error = "Charge failed. Please try again."

              @setState(newState)

          })


      )

  renderBody: =>
    div
      className: [
        "disabled" if !@form.isEditable()
      ].join(' ')
      if @state.error?
        span
          className: 'general_error error'
          "Charged failed. Please try again."
      else if @state.sent
        span
          className: 'success'
          "Charge Successful"

      BasicForm
        args: [
          @form,
          @state.hideStripeErrors,
          @state.last4,
          @state.brand,
          @props.isCreditCardZipOptional
        ]

      if @state.sent
        Button
          color: 'primary'
          onClick: @props.hideModals
          size: 'full-width'
          'Close'
      else
        Button
          color: 'green'
          disabled: !@isSendEnabled()
          onClick: if @isSendEnabled() then @sendCharge
          size: 'full-width'
          ['Charge ', Utils.formatNumber(@form.record.amount, keepSymbol: true)]

  render: ->
    super(arguments...)

    SimpleModal
      title: "##{@props.job_id_str}: Charge Card"
      body: @renderBody()
      className: "charge-card-modal"
      confirm: null
      cancel: null
      overlay: Loading({
        title: 'Processing Payment',
        message: 'Don\'t refresh your browser or leave the page.',
        size: 96
      }) if @state.showSpinner

#HACK: allows us to wrap with elements so we can injectStripe
wrappedStripeRadium  = compose(injectStripe, StoreWrapper)
injectedChargeCardModal = wrappedStripeRadium(ChargeCardModal, (props = {}) ->
  invoice = InvoiceStore.getInvoiceById(props.invoice_id)
  if !invoice
    Rollbar.warning('Invoice not found in charge card modal.', {invoice_id: props.invoice_id})

  return {
    invoice: invoice
    job_id_str: JobStore.getWatchedId(invoice?.job_id, @getId())
    isCreditCardZipOptional: FeatureStore.isFeatureEnabled(Consts.FEATURE_OPTIONAL_CREDIT_CARD_ZIP)
    initialEmailValue: JobStore.get(invoice?.job_id, "email")
    company: JobStore.get(invoice?.job_id, "rescue_company")
    service: JobStore.get(invoice?.job_id, "service")
    account: JobStore.get(invoice?.job_id, "account")
  }
)

WrappedInjectedChargeCardModal = (props) ->
  Elements null,
    React.createElement(injectedChargeCardModal, props)

export default withModalContext(StripeLoaded(WrappedInjectedChargeCardModal))
