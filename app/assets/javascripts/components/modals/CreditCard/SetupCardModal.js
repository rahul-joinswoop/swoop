import React from 'react'
import SimpleModal from 'components/modals/simple_modal'
import Context from 'Context'
import GenericCreditCardIcon from 'public/images/credit_cards/genericcreditcard.png'
import UserStore from 'stores/user_store'
import Tracking from 'tracking'
import 'stylesheets/components/modals/CreditCard/SetupCardModal.scss'
import { withModalContext } from 'componentLibrary/Modal'

const SetupCardModalBody = () => (
  <div className="setup-card-modal-body">
    <span className="title">Collect Payments in Swoop!</span>
    <img
      alt="Credit Card Icon"
      className="credit-card-icon"
      src={GenericCreditCardIcon}
    />
    <span className="main-text">
      Swoop makes collecting payment easier than ever, with low processing fees.
      Visit Settings or contact your company admin to setup payments and start saving time and money!
    </span>
    <span className="questions">Questions? Contact sales@joinswoop.com</span>
  </div>
)

class SetupCardModal extends React.Component {
  componentDidMount() {
    this.sendTracking('Visit')
  }

  sendTracking = (type) => {
    Tracking.track(`Payment Setup Prompt ${type}`, {
      category: 'Payments',
    })
  }

  navigateToConfigure = () => {
    Context.navigate('user/settings', { configuretab: null })
    this.props.hideModals()
    this.sendTracking('Click')
  }

  launchEmail = () => {
    const accountingEmail = UserStore.getCompany()?.accounting_email
    const receipients = accountingEmail ? `${accountingEmail},sales@joinswoop.com` : 'sales@joinswoop.com'
    const subject = 'Collect payments directly in Swoop'
    const body = 'Your team has expressed interest in collecting payments directly in Swoop. Enable payments today by connecting your bank account in Settings > Configure > Bank Information. To learn more call (866) 219-8136.'
    const link = document.createElement('a')
    link.href = `mailto:${receipients}?subject=${subject}&body=${body}`
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
    this.sendTracking('Click')
  }

  render() {
    return (
      <SimpleModal
        body={<SetupCardModalBody />}
        cancel={null}
        className="setup-card-modal"
        confirm="Setup Payments"
        onConfirm={this.props.isAdminWithPayPermission ? this.navigateToConfigure : this.launchEmail}
        title=""
      />
    )
  }
}

export const Component = SetupCardModal
export default withModalContext(SetupCardModal)
