import Consts from 'consts'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'

import React from 'react'
require('stylesheets/components/modals/commission_percentage_form.scss')
BaseComponent = require('components/base_component')
TransitionModal = React.createFactory require 'components/modals'
Fields = require 'fields/fields'
Renderer = require 'form_renderer'
CompanyStore = require('stores/company_store').default
ModalStore = require('stores/modal_store')
import { bind, extend } from 'lodash'

class CommissionPercentageField extends Fields.PercentageField
    _name: "default_pct"
    _label: "Commission Percentage"
    _maxLength: 2

class CommissionPercentageForm extends BaseComponent
  displayName: 'CommissionPercentageForm'

  constructor: (props) ->
    super(props)

    commission = CompanyStore.get(UserStore.getCompany().id, 'commission', @getId())

    @form = new Renderer(commission || {}, @rerender, {
      componentId: @getId()
    })

    @form.registerFields([CommissionPercentageField], true)

  onConfirm: (record) =>
    obj =
      company: {
        commission: {
          id: UserStore.getCompany().commission?.id
          default_pct: record.default_pct
        }
      }

    CompanyStore.updateCommission(obj)

    Dispatcher.send(ModalStore.CLOSE_MODALS)

  render: ->
    super(arguments...)

    TransitionModal extend({}, @props,
      extraClasses: 'commission_percentage_form'
      title: "Commission Percentage"
      showSpinner: false
      onConfirm: if @form? then bind(@form.attemptSubmit, @form, @onConfirm)),
      [
        ReactDOMFactories.p({ key: 'helper-text' }, Consts.COMMISSION_PERCENTAGE.HELPER_TEXT)
        @form.renderInput("default_pct", {containerClass: "percentage_field"})
      ]

module.exports = CommissionPercentageForm
