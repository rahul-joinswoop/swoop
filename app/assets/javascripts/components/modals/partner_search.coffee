import Api from 'api'
import Dispatcher from 'lib/swoop/dispatcher'
import createReactClass from 'create-react-class'
import React from 'react'
require('stylesheets/components/modals/partner_search.scss')
TransitionModal = React.createFactory(require 'components/modals')
UserStore = require ('stores/user_store')
import Consts from 'consts'
Fields = require('fields/fields')
Renderer = require('form_renderer')
ModalStore = require('stores/modal_store')
PartnerCheckList = React.createFactory(require('components/partner_check_list'))
import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
BaseComponent = require('components/base_component')
import { extend, find } from 'lodash'

SearchPhoneField = class SearchPhoneField extends Fields.PhoneField
  _name: "phone"
  _label: Consts.MESSAGE_PARTNER_DISPATCH_NUMBER
  findErrors: ->
    if !@getValue()? || @getValue().length == 0
      return Consts.MESSAGE_INVALID_PHONE
    super(arguments...)

  #TODO: add a way to submit from fields
  #searchKeyDown: (e) ->
  #  if e.keyCode == 13
  #    @handleSubmit
  #getInputProps: ->
  #  props = super(arguments...)
  #  props.onkeydown=@searchKeyDown
  #  props


Message = React.createFactory(createReactClass(
  render: ->
    div
      className: "message"
      @props.children
))

class PartnerSearch extends BaseComponent
  displayName: 'PartnerSearch'
  constructor: (props) ->
    super(props)
    @state = @getInitialState()
    @state.rev = 0
    @form = new Renderer({}, @rerender)
    @form.registerFields(SearchPhoneField: SearchPhoneField)


  setChosen: (chosen) =>
    @setState
      chosen: chosen

  getInitialState: ->
    sites: null
    loading: null
    error: null
    phone: null
    chosen: null


  numberSearch: =>
    state = @getInitialState()
    if @form.isValid()
      newrecord = @form.getRecordChanges()
      state.phone = newrecord.phone
      state.loading = true
      Api.searchSitesByPhone(newrecord,
        success: (data) =>
          #Replace with data returned correctly
          count = 0
          for site in data
            if !site.dispatchable_by_fleet
              count++
              selection = site.id
          @setState
            sites: data
            loading: false
            chosen: if count == 1 then selection else null
        error: (data, textStatus, errorThrown) =>
          @setState
            error: Consts.PARTNER_SEARCH.SERVER_ERROR
            loading: false
      )



    else
      @form.attemptedSubmit()
      state.error = Consts.PARTNER_SEARCH.ERROR.BAD_NUMBER

    @setState state

  canCreateNew: =>
    @state?.sites?.length == 0

  createNewPartner: =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "partner_form",
      props: {
        phone: @state.phone
        type: 'create'
        title: "Add New Partner"
      }
    )

  addSelected: =>
    chosenSite = find(@state.sites, (site) => site.id == @state?.chosen )
    if chosenSite
      Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "partner_form",
        props: {
          record: chosenSite.company
          site: chosenSite
          type: 'existing_site'
          title: "Add New Partner"
        }
      )

  render: ->
    super(arguments...)
    TransitionModal extend({}, @props,
      title: Consts.PARTNER_SEARCH.TITLE
      extraClasses: "partner_search"
      onConfirm: if @canCreateNew() then @createNewPartner else @addSelected
      enableSend: @canCreateNew() || @state?.chosen?
      confirm: if @canCreateNew()
          Consts.PARTNER_SEARCH.BUTTON.ADD_PARTNER
        else
          Consts.PARTNER_SEARCH.BUTTON.SELECT_SITE
      ),
      div null,
        span
          className: 'instructions'
          Consts.PARTNER_SEARCH.ENTER_NUMBER_LONG
        if @props.show
          div null,
            @form.renderInput("phone")
            Button
              color: 'primary'
              disabled: !!@state?.loading
              onClick: @numberSearch
              style: {
                top: -1
              }
              Consts.PARTNER_SEARCH.SEARCH
            PartnerCheckList
              sites: @state?.sites
              chosen: @state?.chosen
              setChosen: @setChosen
            if @state.loading
              Loading null
            else if @state.error
              div
                className: "error"
                @state.error
            else if @canCreateNew()
              Message null,
                div null, Consts.PARTNER_SEARCH.NO_RESULTS
                div null, Consts.PARTNER_SEARCH.CREATE_NEW
            else if !@state?.sites?
              Message null,
                Consts.PARTNER_SEARCH.ENTER_NUMBER


module.exports = PartnerSearch
