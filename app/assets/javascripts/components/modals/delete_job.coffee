import React from 'react'
import Consts from 'consts'
import createReactClass from 'create-react-class'
import Utils from 'utils'
import { extend } from 'lodash'
import 'stylesheets/components/modals/delete_job.scss'

BaseComponent = require('components/base_component')
Fields = require 'fields/fields'
JobStatusReasonTypeStore = require('stores/job_status_reason_type_store').default
Renderer = require('form_renderer')
TransitionModal = React.createFactory(require 'components/modals')
UserStore = require ('stores/user_store')

JobStatusReasonTypeField: class JobStatusReasonTypeField extends Fields.SelectField
  _name: "job_status_reason_type_id"
  _placeholder: "Select a reason"
  _label: ""
  _required: true
  getOptions: =>
    reasonTypes = JobStatusReasonTypeStore.getByJobStatusName(
      @props.newStatus, @props.componentId
    )

    @optionMapper(
      reasonTypes, ["Select a reason"], "text"
    )

class DeleteModal extends BaseComponent
  @defaultProps =
    component: TransitionModal

  displayName: 'DeleteModal'

  constructor: (props) ->
    super(props)

    JobStatusReasonTypeStore

    @form = new Renderer({ id: props.record.id }, @rerender, {
      componentId: @getId(), newStatus: @props.newStatus
    })
    @form.registerFields(JobStatusReasonTypeField: JobStatusReasonTypeField)

  isPartnerViewOfFleet: =>
    Utils.isAssignedFromFleet(@props.record) and
    UserStore.isPartner() and
    @props.record?.type in ["FleetInHouseJob", "FleetManagedJob"]

  onConfirm: =>
    status = @props.newStatus || Consts.CANCELED

    if @isPartnerViewOfFleet()
      status = Consts.REJECTED

    jobObject =
      id: @props.record.id
      status: status

    if @props.requireJobStatusReason
      if @form.isValid()
        jobObject["job_status_reason_types"] = [
          { id: @form.getRecordChanges()["job_status_reason_type_id"] }
        ]

        @props.onConfirm(jobObject)
      else
        @form.attemptedSubmit()
        @rerender()
    else
      @props.onConfirm(jobObject)

  getStatus: =>
    if @isPartnerViewOfFleet() and @props.record?.status == Consts.ASSIGNED
      return "reject"

    return "cancel"

  getBody: =>
    if @props.newStatus == Consts.GOA
      return "Are you sure you would like to mark this job as GOA?"

    return "Are you sure you would like to " + @getStatus() + " this job?"

  getTitle: =>
    if @isBlocked() && @props.newStatus == Consts.GOA
      Utils.getJobId(@props.record) + ": Unable to Mark GOA"
    else if @isBlocked()
      Utils.getJobId(@props.record) + ": Unable to "+@getStatus()+" job"
    else if @props.newStatus == Consts.GOA
      "Mark as GOA"
    else
      Utils.getJobId(@props.record) + ": " + @getStatus()+" job"

  isBlocked: =>
    return @isPartnerViewOfFleet() and @props.record?.status != Consts.ASSIGNED

  render: ->
    super(arguments...)

    block = @isBlocked()
    @props.component extend({}, @props,
      callbackKey: 'delete'
      transitionName: 'deleteModalTransition'
      title: @getTitle()
      extraClasses: "delete_modal"
      onConfirm: if block then null else @onConfirm
      confirm: if block then null else 'Yes'
      cancel: if block then "OK" else 'No'),
      span
        className: 'inline-block'
        if block && Utils.isPartnerOnSwoopJob(UserStore, @props.record)
          if @props.newStatus == Consts.GOA
            "Please contact Swoop to mark this job GOA: " + Consts.SWOOP_TELEPHONE
          else
            "Please contact Swoop to cancel this job: " + Consts.SWOOP_TELEPHONE
        else if block
          "Please contact the account dispatcher to "+@getStatus()+" this job."
        else
          div null,
            span null,
              @getBody()
            if @props.requireJobStatusReason
              div null,
              @form.renderInput('job_status_reason_type_id')

module.exports = DeleteModal
