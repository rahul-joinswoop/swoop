import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
UserStore = require ('stores/user_store')
JobStore = require('stores/job_store')
IssueStore = require ('stores/issue_store')
ModalStore = require ('stores/modal_store')
import Consts from 'consts'
Fields = require('fields/fields')
Renderer = require('form_renderer')
BaseComponent = require('components/base_component')
import { extend } from 'lodash'
import Dispatcher from 'lib/swoop/dispatcher'

IssueField: class IssueField extends Fields.SelectField
  _name: "issue_id"
  _label: ""
  _required: true
  _placeholder: "Select an issue"
  getOptions: -> @optionMapper(IssueStore.loadAndGetAll(@props.componentId), ["Select an issue"])

ReasonField: class ReasonField extends Fields.SelectField
  _name: "reason_id"
  _placeholder: "Select a reason"
  _label: ""
  _required: true
  getOptions: -> @optionMapper(IssueStore.get(@record.issue_id, 'reasons', @props.componentId), ["Select a reason"])

EmailField: class EmailField extends Fields.EmailField
  _name: "email"
  _placeholder: "Email to Receive Claim Form"
  _label: "Customer Email"
  _required: true
  isShowing: -> String(@record.issue_id) == Consts.ISSUES_CLAIM_VALUE and String(@record.reason_id) == Consts.ISSUES_DAMAGE_VALUE

class AddJobIssue extends BaseComponent
  displayName: 'AddJobIssue'

  constructor: (props) ->
    super(props)

    @form = new Renderer({}, @rerender, {componentId: @getId()})
    @form.registerFields(IssueField: IssueField, ReasonField: ReasonField, EmailField: EmailField)

  onConfirm: =>
    if @form.isValid()
      changes = @form.getRecordChanges()
      JobStore.addIssue(@props.job_id, changes)
      Dispatcher.send(ModalStore.CLOSE_MODALS)
    else
      @form.attemptedSubmit()
      @rerender()

  render: ->
    super(arguments...)
    TransitionModal extend({}, @props,
      transitionName: 'AddJobIssueTransition'
      title: "Add an issue"
      extraClasses: "add_job_issue"
      onConfirm: @onConfirm
      style:
        textAlign: "left"
      confirm: 'Add'),
      div null,
        span
          className: 'inline-block'
          style:
            marginBottom: 10
          Consts.MESSAGE_SELECT_ISSUE
        if @props.show
          @form.renderInputs([
            ["issue_id", FieldStyleOne]
            ["reason_id", FieldStyleTwo]
            ["email", FieldStyleThree]
          ])

FieldStyleOne = {
  containerStyle:
    display: "inline-block"
    width: "49%"
    float: "left"
}

FieldStyleTwo = {
  containerStyle:
    display: "inline-block"
    width: "49%"
    float: "right"
}

FieldStyleThree = {
  containerStyle:
    display: "block"
}

module.exports = AddJobIssue
