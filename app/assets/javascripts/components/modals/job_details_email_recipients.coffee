import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
JobStore = require ('stores/job_store')
Renderer = require('form_renderer')
BaseComponent = require('components/base_component')
Fields = require('fields/fields')
import { extend, partial } from 'lodash'
import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import { withModalContext } from 'componentLibrary/Modal'

EmailToField : class EmailToField extends Fields.EmailField
  _name: "email_to"
  _label: "To:"
  _allowMultiple: true

EmailCcField : class EmailCcField extends Fields.EmailField
  _name: "email_cc"
  _label: "Cc:"
  _allowMultiple: true

EmailBccField : class EmailBccField extends Fields.EmailField
  _name: "email_bcc"
  _label: "Bcc:"
  _allowMultiple: true

class JobDetailsEmailRecipients extends BaseComponent
  displayName: 'JobDetailsEmailRecipients'

  constructor: (props) ->
    super(props)

    @form = new Renderer({ id: @props.job.id }, @rerender)
    @form.registerFields([EmailToField, EmailCcField, EmailBccField])

  handleCancel: (button) =>
    if @props.onCancel
      @props.onCancel @props.callbackKey, button
    return

  sendEmail: =>
    if @form.isValid()
      id = @form.record.id
      email_to = if @form.record.email_to then @form.record.email_to else ""
      email_cc = if @form.record.email_cc then @form.record.email_cc else ""
      email_bcc = if @form.record.email_bcc then @form.record.email_bcc else ""

      JobStore.sendJobDetailsEmail(id, email_to, email_cc, email_bcc)

      @props.hideModals()
    else
      @form.attemptedSubmit()
      @rerender()

  getTitle: =>
    "#{@form.record.id}: Email Job Details"

  getSendButtonTitle: =>
    "Send"

  render: ->
    super()
    #TODO: Merge this with edit_invoice_recipients

    TransitionModal extend({}, @props,
      callbackKey: 'delete'
      transitionName: 'AddJobDetailsEmailRecipientsTransition'
      title: @getTitle()
      extraClasses: "customer_type_picker"
      extraStyles:
        maxWidth: 550
      confirm: null
      cancel: null
      style:
        textAlign: "left"
      ),
      div
        style:
          marginBottom: 10
        span
          className: 'inline-block'
          style:
            marginBottom: 10
          "Add multiple emails seperated with a comma or semicolon."
        if @props.show
          @form.renderInputs([
            ["email_to", Styles.emailStyle],
            ["email_cc", Styles.emailStyle],
            ["email_bcc", Styles.emailStyle]
          ])
      div
        style:
          marginTop: 20
          textAlign: 'right'

        Button
          color: 'cancel'
          onClick: partial(@handleCancel, true)
          'Cancel'

        Button
          color: 'primary'
          onClick: @sendEmail
          style: {
            marginLeft: 10
          }
          'Send'

Styles =
  emailStyle:
    containerStyle:
      marginBottom: 5
    labelStyle:
      display: 'inline-block'
      width: 30
    style:
      display: 'inline-block'
      width: "calc(100% - 30px)"

module.exports = withModalContext(JobDetailsEmailRecipients)
