import React from 'react'
require('stylesheets/components/modals/object_form.scss')
BaseComponent = require('components/base_component')
UserStore = require('stores/user_store')
TransitionModal = React.createFactory(require 'components/modals')
import Utils from 'utils'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
ModalStore = require('stores/modal_store')
import { extend, partial, without } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'

class ObjectForm extends BaseComponent
  #PROPERTIES TO OVERWRITE
  displayName: 'ObjForm'
  _getStandardFunction: null
  _changeFunction: null
  _store: null
  _objTitle: "Objects"
  _placeholder: "Custom Object Name"
  getStandardObjs: () =>
    return []
  getCustomObjs: () =>
    return []
  getAll: () =>
    return []
  getObjTitle: ->
    return @_objTitle
  getPlaceholder: ->
    return @_placeholder
  ##########################

  constructor: ->
    super(arguments...)
    @state.add = []
    @state.remove = []
    @state.custom = []
    @state.loading = true
    @_getStandardFunction(UserStore.getEndpoint(), {
      success: (data) =>
        @setState
          standard_objects: data
          loading: false
      error: (data) =>
        console.warn(data)
    })

  editObject: (id, e) =>
    if e.target.checked
      remove = without(@state.remove, id)
      add = @state.add
      if !@_store.companyContains(id, @getId())
        add = @state.add.concat([id])
      @setState
        add: add
        remove: remove
    else
      add = without(@state.add, id)
      remove = @state.remove
      if @_store.companyContains(id, @getId())
        remove = @state.remove.concat([id])
      @setState
        add: add
        remove: remove

  renderStandardObjColumn: (objects) =>
    div null,
      for object in objects
        div null,
          input
            type: "checkbox"
            id: object.name
            onChange: partial(@editObject, object.id)
            checked: @state.add.indexOf(object.id) > -1 || (@_store.companyContains(object.id, @getId()) && @state.remove.indexOf(object.id) == -1)
          label
            htmlFor: object.name
            object.name

  renderStandardObjs: =>
    #TODO: Do I need to remove GOA here?  / Can I depend on them coming back presorted
    div null,
      if UserStore.isPartner()
        h4 null,
          "Standard "+@getObjTitle()
      div
        className: "standard_obj_list"
        if !@state.loading
          objs = @getStandardObjs()
          splits = Utils.splitIntoThree(objs)
          [
            @renderStandardObjColumn(splits[0])
            @renderStandardObjColumn(splits[1])
            @renderStandardObjColumn(splits[2])
          ]
        else
          Loading null

  renderCustomObjColumn: (objects) =>
    div null,
      for object in objects
        div
          className: 'custom_object'
          ReactDOMFactories.i
            className: 'fa fa-times-circle'
            onClick: partial(@editObject, object.id, {target:{checked:false}})
          span null,
            object.name

  multipleObjectsExist: (objectName) =>
    count = 0
    arr = @getAll()
    #TODO: this can be optimized
    for object in arr
      if object.name.toLowerCase() == objectName.toLowerCase()
        count++
        if count > 1
          return true

    return false

  renderCustomObjs: () =>
    div
      className: "custom_objects"
      h4 null,
        "Custom "+@getObjTitle()
        div
          className: "add_custom_object_btn"
          onClick: =>
            custom = @state.custom.concat([{name: "", key: Math.random()}])
            @setState
              custom: custom
          span null,
            "+"
      if !@state.loading
        objs = @getCustomObjs()
        splits = Utils.splitIntoThree(objs)
        div null,
          div
            className: 'custom_object_columns'
            @renderCustomObjColumn(splits[0])
            @renderCustomObjColumn(splits[1])
            @renderCustomObjColumn(splits[2])
          div null,
            for object, i in @state.custom
              div
                key: object.key
                className: "custom_object new_add"
                ReactDOMFactories.i
                  className: 'fa fa-times-circle'
                  onClick: partial((i, e) =>
                    @state.custom.splice(i, 1)
                    @forceUpdate()
                  , i)
                div
                  className: 'inputWrapper'
                  input
                    type: 'input'
                    placeholder: @getPlaceholder()
                    className: "form-control"
                    onChange: partial((i, e) =>
                      @state.custom[i].name = e.target.value
                      @forceUpdate()
                    , i)
                    value: object.name
                  if  @state.attemptedSubmit and @multipleObjectsExist(object.name)
                    div
                      className: "error_bubble"
                      style:
                        position: "absolute"
                        right: 5
                        bottom: 7
                        textAlign: "center"
                        border: "1px solid #F00"
                        color: "#f00"
                        width: 20
                        height: 20
                        borderRadius: 10
                        fontWeight: "bold"
                      "!"
                      div
                        className: "bubble"
                        span null,
                          object.name + " already exists"

      else
        Loading null
  onConfirm: =>
    for object in @state.custom
      if @multipleObjectsExist(object.name)
        @setState
          attemptedSubmit: true
        return

    @setState
      loading: true
    #TODO: remove keys from custom

    object_params = {}

    if @state.add.length > 0
      object_params['add'] = @state.add

    if @state.remove.length > 0
      object_params['remove'] = @state.remove

    if @state.custom.length > 0
      object_params['add_custom'] = @state.custom

    console.debug('object_params', object_params)

    @_changeFunction(object_params, {
      success: (data) =>
        for obj in data
          @_store._add(obj)
        Dispatcher.send(ModalStore.CLOSE_MODALS)
      error: (data, textStatus, errorThrown) =>
        Utils.handleError(data, textStatus, errorThrown)
        @setState
          loading: false
    })


  renderBody: =>
    [@renderStandardObjs(),
    if @props.hideCustomItems then null else @renderCustomObjs()]

  render: ->
    super(arguments...)
    TransitionModal extend({}, @props,
      title: "Edit "+@getObjTitle()
      extraClasses: "obj_form"
      onConfirm: @onConfirm
      confirm: 'Save'
    ), @renderBody()


module.exports = ObjectForm
