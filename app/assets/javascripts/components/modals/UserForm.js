import React, { useState } from 'react'

import Consts from 'consts'
import 'stylesheets/components/modals/UserForm.scss'
import UserStore from 'stores/user_store'
import CompanyStore from 'stores/company_store'
import TransitionModal from 'components/modals'
import Renderer from 'form_renderer'
import UsersStore from 'stores/users_store'
import UserFields from 'user_fields'
import BaseComponent from 'components/base_component'
import { bind, each, map, uniq } from 'lodash'

const Notifications = ({ form } = {}) => {
  const [rev, rerender] = useState(0)
  form.options.onUserRoleChanged = () => rerender(rev + 1)

  let availableNotificationSettings = null

  if (UserStore.isSwoop()) {
    availableNotificationSettings = CompanyStore.get(form.record.company_id, 'available_user_notification_settings', form.options.componentId)
  } else if (UserStore.isPartner()) {
    availableNotificationSettings = UserStore.getCompany().available_user_notification_settings
  }

  let supportedRoles = map(availableNotificationSettings, setting => setting.supported_roles)
  supportedRoles = uniq([].concat(...supportedRoles))

  const userHasAnySupportedRole = supportedRoles.some(role => form.record?.roles?.indexOf(role) >= 0)
  let swoopEditingPartnerUser = false

  if (UserStore.isSwoop()) {
    const userCompanyType = CompanyStore.get(form.record.company_id, 'type', form.options.componentId)

    swoopEditingPartnerUser = CompanyStore.isRescueCompany({ type: userCompanyType })
  }

  if ((UserStore.isPartner() || swoopEditingPartnerUser) && userHasAnySupportedRole) {
    const availableNotificationSettingsRows = []

    each(availableNotificationSettings, (setting) => {
      const fieldClass = UserFields.NotificationSettingFactory(setting)
      const field = form.register(fieldClass, fieldClass._name)

      availableNotificationSettingsRows.push(form.renderInput(field._name))
    })

    return (
      <div className="UserForm-Notifications">
        <div className="divider">
         Notifications
        </div>
        { availableNotificationSettingsRows }
      </div>
    )
  } else {
    return null
  }
}

const EmailSettings = ({ form } = {}) =>
  <div>
    <div className="divider">
        Email Settings
    </div>
    {form.renderInputs([
      'job_status_email_frequency',
      Consts.CREATED,
      Consts.TOWING,
      Consts.ACCEPTED,
      Consts.TOWDESTINATION,
      Consts.ETAEXTENDED,
      Consts.ENROUTE,
      Consts.COMPLETED,
      Consts.ONSITE,
      Consts.GOA,
    ])}
  </div>

const Permissions = ({ form } = {}) =>
  <div>
    <div className="divider">
        Permissions
    </div>
    {form.renderInputs([
      'admin',
      'root',
      'dispatcher',
      'driver',
      'answering_service',
      'receive_system_alerts',
      'looker',
      'custom_account_visible',
    ])}
  </div>

const SystemEmailFilteredCompanies = ({ form } = {}) => {
  if (
      form?.record?.company_id &&
      UserStore.isSwoop() &&
      form.record.company_id === CompanyStore.getSwoopId() &&
      form.fields.receive_system_alerts.getValue() === true
  ) {
    return (
      <div>
        <div className="divider">
            System Alerts Filter
        </div>
        {form.renderInputs(['system_email_filtered_companies'])}
      </div>
    )
  } else {
    return null
  }
}

const BasicUser = ({ form } = {}) =>
  form.renderInputs([
    'first_name',
    'last_name',
    'phone',
    'pager',
    'email',
    'username',
    'password',
    'company_id',
    'site_ids',
  ])

class UserForm extends BaseComponent {
  static defaultProps = {
    record: {},
  }

  constructor(props) {
    super(props)
    const record = props.record || {}
    this.form = new Renderer(record, this.rerender, { componentId: this.getId() })
    this.form.registerFields(UserFields, true)

    if (record?.id != null) {
      const company_id = UsersStore.get(record?.id, 'company_id', this.getId())

      if (CompanyStore.get(company_id, 'id', this.getId())) {
        this.showSpinner(false)
      } else {
        this.showSpinner(true)
      }
    }
  }

  rerender = () => {
    this.showSpinner(false)
    return super.rerender()
  }

  showSpinner = showSpinner => this.setState({ showSpinner })

  render() {
    super.render()

    const { onConfirm, onCancel } = this.props
    const { form } = this

    const handleConfirm = form ?
      bind(
        form.attemptSubmit,
        form,
        onConfirm,
        onCancel,
        this.showSpinner,
      ) :
      undefined

    return (
      <TransitionModal
        {...this.props}
        extraClasses="user_form"
        onConfirm={handleConfirm}
        recordLabel="User"
        showSpinner={this.state?.showSpinner}
      >
        {this.props.show &&
        <div>
          <BasicUser {...{ form }} />
          <Permissions {...{ form }} />
          <SystemEmailFilteredCompanies {...{ form }} />
          <Notifications {...{ form }} />
          <EmailSettings {...{ form }} />
        </div>}
      </TransitionModal>
    )
  }
}

export default UserForm
