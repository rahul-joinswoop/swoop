import React from 'react'
import { withModalContext } from 'componentLibrary/Modal'
require('stylesheets/components/modals/select_service_provided.scss')
TransitionModal = React.createFactory(require 'components/modals')
JobStore = require('stores/job_store')
import Consts from 'consts'
Fields = require('fields/fields')
Renderer = require('form_renderer')
BaseComponent = require('components/base_component')
import { clone } from 'lodash'

ServiceField = class ServiceField extends Fields.SelectField
  _name: "service"
  _label: ""
  _required: true
  _placeholder: "Select a service"
  getOptions: ->
    if JobStore.get(@record.id, "service") == Consts.TOWIFJUMPFAILS
      @optionMapper([Consts.BATTERYJUMP, Consts.TOWIFJUMPFAILS], clone(super(arguments...)))
    else if JobStore.get(@record.id, "service") == Consts.TOWIFTIRECHANGEFAILS
      @optionMapper([Consts.TIRE_CHANGE, Consts.TOWIFTIRECHANGEFAILS], clone(super(arguments...)))

class SelectServiceProvider extends BaseComponent
  displayName: 'SelectServiceProvider'

  constructor: (props) ->
    super(props)
    @form = new Renderer({id: props.job_id}, @rerender, {componentId: @getId()})
    @form.registerFields(ServiceField: ServiceField)

  onConfirm: =>
    if @form.isValid()
      changes = @form.getRecordChanges()
      changes.callbacks = {
        success: @props.callback
      }
      JobStore.jobChanged(changes)
      this.props.hideModals()
    else
      @form.attemptedSubmit()
      @rerender()

  onCancel: ->
    this.props.hideModals()

  render: ->
    super(arguments...)
    TransitionModal
      show: true
      title: JobStore.getWatchedId(@props.job_id, @getId()) + ": Select Service Provided"
      extraClasses: "select_service_provided"
      onConfirm: @onConfirm
      onCancel: @onCancel
      confirm: "Save"
      div null,
        span
          className: 'inline-block'
          'This service is currently "'+JobStore.get(@props.job_id, "service", @getId())+'". Which service was provided?'
        @form.renderInput("service")

module.exports = withModalContext(SelectServiceProvider)
