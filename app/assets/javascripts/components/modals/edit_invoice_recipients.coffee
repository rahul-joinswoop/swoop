import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
import Utils from 'utils'
InvoiceStore = require ('stores/invoice_store')
JobStore = require ('stores/job_store')
AccountStore = require('stores/account_store')
UserStore = require ('stores/user_store')
import Consts from 'consts'
InvoiceFields = require('invoice_fields')
Renderer = require('form_renderer')
ModalStore = require('stores/modal_store')
import Api from 'api'
BaseComponent = require('components/base_component')
import { clone, extend, partial } from 'lodash'
import Dispatcher from 'lib/swoop/dispatcher'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)

class EditInvoiceRecipients extends BaseComponent
  displayName: 'EditInvoiceRecipients'

  constructor: (props) ->
    super(props)
    invoice = clone(props.invoice)
    if !invoice.email_to and !invoice.email_cc and !invoice.email_bcc
      if invoice.recipient_type == "Account"
        account = AccountStore.getById(invoice.recipient_id)
        invoice.email_to = account.accounting_email
        invoice.email_cc = account.accounting_email_cc
        invoice.email_bcc = account.accounting_email_bcc
      else if invoice.recipient_type == "User"
        job = JobStore.getJobById(invoice.job_id)
        invoice.email_to = job.email

    @form = new Renderer(invoice, @rerender)
    @form.registerFields(InvoiceFields)


  handleCancel: (button) =>
    if @props.onCancel
      @props.onCancel @props.callbackKey, button
    return

  save: =>
    if @form.isValid()
      changes =
        id: @form.record.id
        email_to: if @form.record.email_to then @form.record.email_to else ""
        email_cc: if @form.record.email_cc then @form.record.email_cc else ""
        email_bcc: if @form.record.email_bcc then @form.record.email_bcc else ""

      Dispatcher.send(InvoiceStore.INVOICE_CHANGED, changes)
      Dispatcher.send(ModalStore.CLOSE_MODALS)

    else
      @form.attemptedSubmit()
      @rerender()

  saveAndSend: (id, job_id, to, cc, bcc, state) =>
    changes =
      id: id
      email_to: if to then to else ""
      email_cc: if cc then cc else ""
      email_bcc: if bcc then bcc else ""

    if state not in Consts.PARTNER_INVOICE_SENT_STATES and state != Consts.IONSITEPAYMENT and not @props.onlySend
      changes.state = Consts.IPARTNERSENT
      Dispatcher.send(InvoiceStore.INVOICE_CHANGED, changes)
    else
      Api.resendInvoice(id, changes, {
        success: ->
          Utils.sendNotification("Invoice is sending", Consts.SUCCESS)
        error: (data, textStatus, errorThrown) =>
          Utils.sendNotification("Invoice failed to send", Consts.ERROR)
      })

    Dispatcher.send(ModalStore.CLOSE_MODALS)

  saveAndSendPrepare: (e) =>
    if @form.isValid()
      id = @form.record.id
      job_id = @form.record.job_id
      to = @form.record.email_to
      cc = @form.record.email_cc
      bcc = @form.record.email_bcc
      state = @props?.invoice?.state
      if UserStore.isPartner() and job_id and JobStore.getAttachedDocuments(job_id, Consts.TYPE_ATTACHED_DOCUMENT).length > 0
        Dispatcher.send(ModalStore.SHOW_MODAL,
          key: "add_invoice_attachments",
          props: {
            job_id: job_id
            onConfirm: () =>
              @saveAndSend(id, job_id, to, cc, bcc, state)
          }
        )
        e.preventDefault()
        e.stopPropagation()
        return
      else
        @saveAndSend(id, job_id, to, cc, bcc, state)
    else
      @form.attemptedSubmit()
      @rerender()

  getTitle: (invoiceAlreadySent) =>
    title = "Edit Invoice Recipients"

    if @props.onlySend then return title

    if invoiceAlreadySent then title = "Send Invoice Again"

    return title

  getSendButtonTitle: (invoiceAlreadySent) =>
    if invoiceAlreadySent || @props.onlySend then "Send" else "Save & Send"

  render: ->
    super(arguments...)
    sent = @props?.invoice?.state in Consts.PARTNER_INVOICE_SENT_STATES || @props?.invoice?.state == Consts.IONSITEPAYMENT
    TransitionModal extend({}, @props,
      callbackKey: 'delete'
      transitionName: 'EditInvoiceRecipientsTransition'
      title: Utils.getJobId(@props?.invoice?.job)+": "+ @getTitle(sent)
      extraClasses: "customer_type_picker"
      extraStyles:
        maxWidth: 550
      confirm: null
      cancel: null
      style:
        textAlign: "left"
      ),
      div
        style:
          marginBottom: 10
        span
          className: 'inline-block'
          style:
            marginBottom: 10
          "Add multiple emails seperated with a comma or semicolon."
        if @props.show
          @form.renderInputs([
            ["email_to", Styles.emailStyle],
            ["email_cc", Styles.emailStyle],
            ["email_bcc", Styles.emailStyle]
          ])
      div
        style:
          marginTop: 20
          textAlign: 'right'

        Button
          color: 'cancel'
          onClick: partial(@handleCancel, true)
          style: {
            float: 'left',
            marginLeft: 30,
          }
          "Cancel"

        if !sent && !@props.onlySend
          Button
            color: 'primary'
            disabled: if @props.enableSend == false then true else false
            onClick: @save
            style: {
              marginLeft: 10,
            }
            "Save"

        Button
          color: 'green'
          disabled: if @props.enableSend == false then true else false
          onClick: @saveAndSendPrepare
          style: {
            marginLeft: 10,
          }
          @getSendButtonTitle(sent)

Styles =
  emailStyle:
    containerStyle:
      marginBottom: 5
    labelStyle:
      display: 'inline-block'
      width: 30
    style:
      display: 'inline-block'
      width: "calc(100% - 30px)"

module.exports = EditInvoiceRecipients
