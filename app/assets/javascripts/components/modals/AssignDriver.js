import React from 'react'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import DriverMapModal from 'components/modals/DriverMapModal/DriverMapModal'
import JobStore from 'stores/job_store'
import Logger from 'lib/swoop/logger'
import moment from 'moment-timezone'
import UserSettingStore from 'stores/user_setting_store'
import UsersStore from 'stores/users_store'
import VehicleStore from 'stores/vehicle_store'
import { filter, map, sortBy } from 'lodash'

class AssignDriver extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      confirmError: null,
      selectedDriverId: props?.record?.rescue_driver_id,
      showMap: false,
      search: '',
      showOffDuty: UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.SHOW_OFF_DUTY_ASSIGN_DRIVER_MAP, this.getId()) === 'true',
      savedVehicles: {},
    }

    VehicleStore._loadVehiclesWithDriverListIds()

    setTimeout(() => this.setState({
      showMap: true,
    }), 500)
  }

  handleConfirm = () => {
    const { selectedDriverId, savedVehicles } = this.state
    const { record } = this.props
    const obj = {
      id: record.id,
    }

    if (selectedDriverId) {
      obj.rescue_driver_id = selectedDriverId
    } else {
      this.setState({ confirmError: 'Select driver to continue' })
      return
    }

    if (savedVehicles && savedVehicles[selectedDriverId]) {
      obj.rescue_vehicle_id = savedVehicles[selectedDriverId]
    } else if (record?.rescue_vehicle_id) {
      Logger.debug('Rescue Vehicle already set, so not defaulting')
    } else if (VehicleStore.getVehicleIdByDriverId(selectedDriverId)) {
      obj.rescue_vehicle_id = VehicleStore.getVehicleIdByDriverId(selectedDriverId)
    }
    /* eslint-disable-next-line */ // defined globally
    Tracking.trackJobEvent('Dispatch to Driver', record?.id, {
      category: 'Dispatch',
    })

    this.props.onConfirm(obj)
  }

  getDriverJobs = id => filter(JobStore.getJobs(), job => (
    job.rescue_driver_id === id &&
      Consts.DISPATCHED_STATUSES_AND_ASSIGNED.includes(job.status)
  ))

  getDrivers = () => {
    const { search, showOffDuty } = this.state
    if (search || !showOffDuty) {
      return filter(UsersStore.getDriverIds(this.getId()), (driver_id) => {
        if (!showOffDuty && UsersStore.get(driver_id, 'on_duty', this.getId()) === false) {
          return false
        }
        return (
          UsersStore.get(driver_id, 'full_name', this.getId()).
          toLowerCase().
          indexOf(search.trim().toLowerCase()) >= 0
        )
      })
    } else {
      return UsersStore.getDriverIds(this.getId())
    }
  }

  getSortedDrivers = () => {
    const drivers = map(this.getDrivers(), driver_id => (
      {
        id: driver_id,
        on_duty: UsersStore.get(driver_id, 'on_duty', this.getId()),
        full_name: UsersStore.get(driver_id, 'full_name', this.getId()),
        jobs: sortBy(this.getDriverJobs(driver_id), job => +moment(job.last_status_changed_at)),
      }
    ))
    const driversSortedByJobAmount = sortBy(drivers, driver => driver.jobs.length)
    return sortBy(driversSortedByJobAmount, driver => (driver.on_duty === false ? 1 : 0))
  }

  setSelectedDriverId = driverId => this.setState({ confirmError: null, selectedDriverId: driverId })

  setSearch = searchInput => this.setState({ search: searchInput })

  setShowOffDuty = hasShowOffDuty => this.setState({ showOffDuty: hasShowOffDuty }, () => {
    UserSettingStore.setUserSettings(Consts.USER_SETTING.SHOW_OFF_DUTY_ASSIGN_DRIVER_MAP, hasShowOffDuty)
  })

  setVehicles = newVehicles => this.setState({ savedVehicles: newVehicles })

  enableSend = (enableSend, record, vehicles) => (enableSend || (UsersStore.getDriverIds(this.getId()).length > 0 || vehicles.length > 0)) && (
    !Consts.AUTO_ASSIGNING || (
      Consts.AUTO_ASSIGNING && (
        new Date(record?.auction?.expires_at) >= new Date() || [Consts.ASSIGNED].includes(record?.status)
      )
    ) ||
    [
      Consts.PENDING,
      Consts.ACCEPTED,
      Consts.ASSIGNED,
      Consts.DISPATCHED,
      Consts.ENROUTE,
      Consts.ONSITE,
      Consts.TOWING,
      Consts.TOWDESTINATION,
    ].includes(record?.status)
  )

  render() {
    super.render()
    const {
      acceptJobMethods, acceptJobState, callbackKey, cancel, confirm,
      enableSend, extraClasses, getId, onCancel, onConfirmAccept, record,
      secondModal, show, title, transitionName,
    } = this.props
    const vehicles = VehicleStore.getSortedVehicles(this.getId())
    const {
      confirmError, highlighted_truck, search, selectedDriverId, showOffDuty, showMap, savedVehicles,
    } = this.state
    const {
      getSortedDrivers, setSelectedDriverId, setSearch, setShowOffDuty, setVehicles,
    } = this

    return (
      <DriverMapModal
        {...this.props}
        acceptJobMethods={acceptJobMethods}
        acceptJobState={acceptJobState}
        assignDriverMethods={{
          getSortedDrivers, setSelectedDriverId, setSearch, setShowOffDuty, setVehicles,
        }}
        assignDriverState={{
          highlighted_truck,
          search,
          selectedDriverId,
          showOffDuty,
          showMap,
          vehicles,
          savedVehicles,
        }}
        callbackKey={callbackKey || 'assign'}
        cancel={cancel || ((!acceptJobState && secondModal) ? 'Skip' : null)}
        confirm={confirm ||
          ([Consts.PENDING, Consts.ACCEPTED, Consts.ASSIGNED].includes(record?.status) ?
            'Dispatch' : 'Assign')}
        confirmError={acceptJobState?.error ? true : confirmError}
        enableSend={this.enableSend(enableSend, record, vehicles)}
        extraClasses={extraClasses || 'driver-map-modal assign_driver'}
        /* eslint-disable-next-line */
        getId={getId ?
          () => {
            getId()
            this.getId()
          } : this.getId}
        key="assign_modal_container_modal"
        onCancel={onCancel}
        onConfirm={onConfirmAccept || this.handleConfirm}
        record={record}
        rightComponent={<span className="job-id">{record?.id ? `#${record?.id}` : ''}</span>}
        show={show}
        title={title || 'Assign Driver'}
        transitionName={transitionName || 'assignModalTransition'}
      />
    )
  }
}

AssignDriver.displayName = 'AssignModal'
export default AssignDriver
