ServiceStore = require('stores/service_store')
ObjectForm = require('components/modals/object_form')
import Api from 'api'
import { filter, map, sortBy, values } from 'lodash'

class ServiceForm extends ObjectForm
  displayName: 'ServiceForm'
  _getStandardFunction: Api.standardServices
  _changeFunction: Api.changeServices
  _store: ServiceStore
  _objTitle: "Services"
  _placeholder: "Custom Service Name"
  getStandardObjs: () =>
    return sortBy(filter(@state.standard_objects, (obj) -> obj.name != "GOA"), (obj) -> obj.name.toLowerCase())
  getCustomObjs: () =>
    service_ids = map(@state.standard_objects, (service) -> service.id)
    return sortBy(filter(@_store.getServices(@getId()), (service) => service_ids.indexOf(service.id) == -1 and @state.remove.indexOf(service.id) == -1), (service) -> service.name.toLowerCase())

  getAll: () =>
    values(@_store.getServices(@getId())).concat(@state.custom)

module.exports = ServiceForm
