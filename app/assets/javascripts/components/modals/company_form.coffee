import React from 'react'
import Api from 'api'
import Consts from 'consts'
import UserStore from 'stores/user_store'
import UsersStore from 'stores/users_store'
import Utils from 'utils'
import { calcCrow } from 'components/maps/Utils'
import { withSplitIO } from 'components/split.io'
import { bind, clone, extend, map, sortBy, values } from 'lodash'
import { getDefaultCountry, CURRENCIES, DISTANCE_UNITS, LANGUAGES } from 'lib/localize'
require('stylesheets/components/modals/company_form.scss')

BaseComponent = require('components/base_component')
CompanyStore = require('stores/company_store').default
FeatureStore = require('stores/feature_store')
Fields = require 'fields/fields'
NotificationStore = require('stores/notification_store')
PaymentTypeStore = require('stores/customer_type_store')
Renderer = require 'form_renderer'
TransitionModal = React.createFactory require 'components/modals'

#TODO: if a change comes in, show an error that something has changed and to refresh

clickTimes = 0

showAutoAssignField = (record, props) ->
  record.type == "FleetCompany" && !record.in_house && !record.issc_client_id && FeatureStore.isFeatureEnabled(Consts.FEATURES_AUTO_ASSIGN, props.componentId, record)

getAutoAssignWeightFieldValue = (value) ->
  return value?.toString().replace(/[^0-9.]/g, '').slice(0, 4)

findAutoAssignWeightFieldErrors = (value) ->
  if (parseFloat(value) > 1)
    return "Value must be between 0 and 1"

companyFields = [
  class TypeField extends Fields.SelectField
    _name: 'type'
    _label: 'Type'
    _required: true
    _options: [
      {text: "Fleet", value: 'FleetCompany'}
      {text: "Partner", value: 'RescueCompany'}
    ]
    getContainerClasses: ->
      if @getValue() in ["Fleet", "Partner", "RescueCompany"] and UserStore.isRoot()
        [
          super(arguments...)
          'half_width'
        ].join(' ')
      else
        super(arguments...)
    isEditable: ->  !@record?.id? && !@props.force_partner
    prefill: ->
      if @props.force_partner
        @setValue("RescueCompany")
      else if @props.defaultType
        @setValue(@props.defaultType)
      else
        @setValue("FleetCompany")
    getValue: ->
      val = super(arguments...)
      if !@isEditable()
        if val == "RescueCompany"
          return "Partner"
        else if val == "FleetCompany"
          return "Fleet"
      return val

  class LiveField extends Fields.CheckboxField
    _name: 'live'
    _label: "Live"
    getContainerClasses: ->
      [
        super(arguments...)
        'half_width'
      ].join(' ')
    isShowing: -> @record?.type == 'RescueCompany' and UserStore.isRoot()

  class InHouseField extends Fields.CheckboxField
    _name: "in_house"
    _label: "In-House"
    getContainerClasses: ->
      [
        super(arguments...)
        'half_width'
      ].join(' ')
    isShowing: -> @record?.type == "FleetCompany"

  class AllPartnersDispatchableField extends Fields.CheckboxField
    _name: 'dispatch_to_all_partners'
    _label: ''
    isShowing: -> @record?.type == "FleetCompany" and @record?.in_house != true and !@record?.issc_client_id?
    getContainerStyle: ->
      superContainerStyle = super(arguments...)
      if !@record?.id
        Utils.combineStyles(superContainerStyle,
        {
          marginTop: 16,
          height: 38
        })
      else
        superContainerStyle
    renderInput: ->
      superInput = super(arguments...)
      div null,
        superInput
        label null,
          'All Partners Dispatchable'
        span
          style:
            display: 'inline-block'
            fontSize: 'smaller'
          if @getValue()
            "If unchecked fleet will be removed from all partners"
          else
            "If checked fleet will be added to all partners"

  class NameField extends Fields.Field
    _name: "name"
    _label: "Name*"
    _required: true
    isEditable: -> @record?.type == 'RescueCompany' || !@record?.id?

  class PhoneField extends Fields.PhoneField
    _name: "phone"
    _label: "Phone*"
    _required: true
    allowDisableMasking: true
    #"A company already exists with this phone"

  class DispatchEmailField extends Fields.EmailField
    _name: 'dispatch_email'
    _label: 'Dispatch Email*'
    _required: true
    isShowing: -> @record?.type == 'RescueCompany'

  class SupportEmailField extends Fields.EmailField
    _name: 'support_email'
    _label: 'Support Email*'
    _required: true
    isShowing: -> @record?.type != 'RescueCompany'

  class AccountingEmailField extends Fields.EmailField
    _name: 'accounting_email'
    _label: 'Accounting Email*'
    _required: true

  class LocationField extends Fields.GeoSuggestField
    _name: 'location'
    _label: 'Address*'
    _required: true
    hide_map: true #TODO: Check to make sure this still works
    onChange: (field, record) ->
      if !@original.location
        if record?.location?.lat && record?.location?.lng
          country = getDefaultCountry(record?.location)

          @setValue(country.distanceUnit, false, 'distance_unit')
          @setValue(country.currency, false, 'currency')
          @setValue(country.language, false, 'language')
      super(arguments...)

  class LanguageField extends Fields.SelectField
    _name: "language"
    _label: "Language"
    _required: true
    getOptions: -> @optionMapper(LANGUAGES, clone(super(arguments...)))

  class DistanceField extends Fields.SelectField
    _name: "distance_unit"
    _label: "Distance"
    _required: true
    getOptions: -> @optionMapper(DISTANCE_UNITS, clone(super(arguments...)))

  class CurrencyField extends Fields.SelectField
    _name: "currency"
    _label: "Currency"
    _required: true
    getOptions: -> @optionMapper(CURRENCIES, clone(super(arguments...)))

  class AutoAssignMaxDistance extends Fields.NumberField
    _name: "auction_max_distance"
    _label: "Auto Assign Radius"
    _placeholder: "30"
    isShowing: ->
      showAutoAssignField(@record, @props) &&
      @render_props?.treatments?[Consts.SPLITS.DISABLE_AUTO_ASSIGN_RADIUS] != true

  class AutoAssignMaxETARequests extends Fields.NumberField
    _name: "auction_max_bidders"
    _label: "Max ETA Requests"
    _placeholder: "5"
    isShowing: -> showAutoAssignField(@record, @props)

  class AutoAssignETAWeight extends Fields.Field
    _name: "autoassignment_ranker.weights.speed"
    _label: "ETA Weight"
    _placeholder: "0.1"
    isShowing: -> showAutoAssignField(@record, @props)
    getContainerClasses: -> super(arguments...) + ' sixth_width'
    getValue: -> getAutoAssignWeightFieldValue(super(arguments...))
    findErrors: -> findAutoAssignWeightFieldErrors(@getValue())
    getErrorStyle: -> Utils.combineStyles(super(arguments...), top: 26)
    getBottomError: -> if @render_props?.totalWeightError then return 'Weight values have to add up to 1'

  class AutoAssignCostWeight extends Fields.Field
    _name: "autoassignment_ranker.weights.cost"
    _label: "Cost Weight"
    _placeholder: "0.8"
    isShowing: -> showAutoAssignField(@record, @props)
    getContainerClasses: -> super(arguments...) + ' sixth_width'
    getValue: -> getAutoAssignWeightFieldValue(super(arguments...))
    findErrors: -> findAutoAssignWeightFieldErrors(@getValue())
    getErrorStyle: -> Utils.combineStyles(super(arguments...), top: 26)

  class AutoAssignQualityWeight extends Fields.Field
    _name: "autoassignment_ranker.weights.quality"
    _label: "Quality Weight"
    _placeholder: "0.1"
    isShowing: -> showAutoAssignField(@record, @props)
    getContainerClasses: -> super(arguments...) + ' sixth_width'
    getValue: -> getAutoAssignWeightFieldValue(super(arguments...))
    findErrors: -> findAutoAssignWeightFieldErrors(@getValue())
    getErrorStyle: -> Utils.combineStyles(super(arguments...), top: 26)

  class InvoiceChargeInterval extends Fields.SelectField
    _name: "invoice_charge_fee.payout_interval"
    _label: "Payout Interval"
    getOptions: -> @optionMapper(Consts.PAYOUT_INTERVAL, clone(super(arguments...)))
    getContainerClasses: ->
      [
        super(arguments...)
        'full_width'
      ].join(' ')
    isShowing: -> @record.id? && @record?.type == 'RescueCompany'

  class AutoAssignMinETA extends Fields.NumberField
    _name: "autoassignment_ranker.constraints.min_eta"
    _label: "Auto Assign Min ETA"
    _placeholder: "5"
    isShowing: -> showAutoAssignField(@record, @props)

  class AutoAssignMaxETA extends Fields.NumberField
    _name: "autoassignment_ranker.constraints.max_eta"
    _label: "Auto Assign Max ETA"
    _placeholder: "60"
    isShowing: -> showAutoAssignField(@record, @props)

  class AutoAssignMinCost extends Fields.NumberField
    _name: "autoassignment_ranker.constraints.min_cost"
    _label: "Auto Assign Min Cost"
    _placeholder: "20"
    isShowing: -> showAutoAssignField(@record, @props)

  class AutoAssignMaxCost extends Fields.NumberField
    _name: "autoassignment_ranker.constraints.max_cost"
    _label: "Auto Assign Max Cost"
    _placeholder: "30"
    isShowing: -> showAutoAssignField(@record, @props)

  class AutoAssignMinQuality extends Fields.NumberField
    _name: "autoassignment_ranker.constraints.min_rating"
    _label: "Auto Assign Min Quality"
    _placeholder: "50"
    isShowing: -> showAutoAssignField(@record, @props)

  class AutoAssignMaxQuality extends Fields.NumberField
    _name: "autoassignment_ranker.constraints.max_rating"
    _label: "Auto Assign Max Quality"
    _placeholder: "100"
    isShowing: -> showAutoAssignField(@record, @props)

  class InvoiceChargeFeePercentage extends Fields.Field
    _name: "invoice_charge_fee.percentage"
    _label: "Transaction Fee (%)"
    getContainerClasses: ->
      [
        super(arguments...)
        'half_width'
        'percent'
      ].join(' ')
    findErrors: () ->
      value = @getValue()
      value_fl = parseFloat(value)
      if (value? && (""+value).length > 0) && (isNaN(value_fl) || value_fl < 0 || value_fl > 100)
        return "Please enter a valid percentage"
    isShowing: -> @record.id? && @record?.type == 'RescueCompany'

  class InvoiceChargeFee extends Fields.Field
    _name: "invoice_charge_fee.fixed_value"
    _label: "Transaction Fee (Base)"
    getContainerClasses: ->
      [
        super(arguments...)
        'half_width'
        'money'
      ].join(' ')
    findErrors: ->
      value = @getValue()
      value_fl = parseFloat(value)
      if (value? && (""+value).length > 0) && (isNaN(value_fl) || value_fl < 0)
        return "Please enter a valid number"
    isShowing: -> @record.id? && @record?.type == 'RescueCompany'

  class SubsciptionStatusField extends Fields.SelectField
    _name: 'subscription_status_id'
    _label: 'Subscription Status'
    getOptions: -> @optionMapper(map(Consts.SUBSCRIPTION_STATUS, (value, key) -> return {id: key, name: value}), clone(super(arguments...)))
    getContainerClasses: ->
      [
        super(arguments...)
        'full_width'
      ].join(' ')
    isShowing: -> UserStore.isRoot() and @record?.type == 'RescueCompany'

  class BillingTypeField extends Fields.SelectField
    _name: 'billing_type'
    _label: 'Billing Type'
    _required: true
    getOptions: ->
      @optionMapper(map(Consts.JOB.BILLING_TYPE, (value, key) -> return {id: key, name: value}), [])
    getContainerClasses: ->
      [
        super(arguments...)
        'full_width'
      ].join(' ')
    isShowing: ->
      isShowing = UserStore.isRoot() and @record?.type == 'RescueCompany'
      if isShowing and not @record.billing_type
        @record.billing_type = Consts.JOB.BILLING_TYPE.VCC
      else if not isShowing and @record.billing_type
        delete @record.billing_type

      isShowing

  class NetworkManagerField extends Fields.BackendSuggestField
    _name: 'network_manager_id'
    _label: 'Network Manager'
    getContainerClasses: ->
      [
        super(arguments...)
        'full_width'
      ].join(' ')
    isShowing: -> UserStore.isRoot() and @record?.type == 'RescueCompany'
    getSuggestions: (options, callback) =>
      Api.autocomplete_root_users(options.input, 10, {
        success: (data) =>
          usersMap = map(data, (user) ->
              id: user.id
              label: user.full_name
            )
          callback(usersMap)
        error: (data, textStatus, errorThrown) =>
          Rollbar.error("Autocomplete failed", data, textStatus, errorThrown)
      })

    getValue: ->
      if @record?.network_manager_id
        userName = UsersStore.get(@record.network_manager_id, 'full_name', @props.componentId)

      return userName || ""

  class FeaturesField extends Fields.MultiSelectField
    _name: "features"
    _label: "Features"
    _showAll: false
    _alwaysShowSelections: true
    isShowing: -> @record?.id?
    getContainerClasses: ->
      super(arguments...) + " clear"
    getSuggestions: ->
      options = []
      features = sortBy(values(FeatureStore.getAll()), "name")
      for feature in features
        options.push({value: feature.id, text: feature.name})
      return options

  class PaymentTypeField extends Fields.MultiSelectField
    _name: "customer_type_ids"
    _label: "Payment Types"
    _showAll: false
    _alwaysShowSelections: true
    isShowing: ->
      @record?.id? and FeatureStore.isFeatureEnabled(Consts.FEATURES_PAYMENT_TYPE, @props.componentId, CompanyStore.getById(@record.id))
    getSuggestions: ->
      options = []
      paymentTypes = sortBy(values(PaymentTypeStore.loadAndGetAll(@props.componentId, "name")), "name")
      for paymentType in paymentTypes
        options.push({value: paymentType.id, text: paymentType.name})
      return options

  class InHouseFleetsField extends Fields.MultiSelectField
    _name: "fleet_in_house_client_ids"
    _label: "InHouse Fleets"
    _showAll: false
    _alwaysShowSelections: true
    isShowing: -> UserStore.isSwoop() and @record?.type == 'RescueCompany' and @record?.id?
    getSuggestions: ->
      options = []

      fleetCompaniesIds = CompanyStore.getFleetInHouseIds(@props.componentId)

      for companyId in fleetCompaniesIds
        options.push({
          value: companyId,
          text: CompanyStore.get(companyId, 'name', @props.componentId),
          disabled: CompanyStore.get(companyId, 'name', @props.componentId) in Consts.MOTORCLUBS
        })

      return sortBy(options, (option) -> option.text)

  class ManagedFleetsField extends Fields.MultiSelectField
    _name: "fleet_managed_client_ids"
    _label: "Managed Fleets"
    _showAll: false
    _alwaysShowSelections: true
    isShowing: -> UserStore.isSwoop() and @record?.type == 'RescueCompany' and @record?.id?
    getSuggestions: ->
      options = []

      fleetCompaniesIds = CompanyStore.getFleetManagedIds(@props.componentId)

      for companyId in fleetCompaniesIds
        options.push({
          value: companyId,
          text: CompanyStore.get(companyId, 'name', @props.componentId),
          disabled: CompanyStore.get(companyId, 'name', @props.componentId) in Consts.MOTORCLUBS
        })

      return sortBy(options, (option) -> option.text)
]

class CompanyForm extends BaseComponent
  displayName: 'CompanyForm'

  @defaultProps = {
    record: {}
  }

  cleanupBeforeSend: (newrecord) ->
    # We can't edit questions on the company form, exists in newrecord because of problem in diffing
    # Let's remove it here until we fix the underlying issue
    delete newrecord.questions

  constructor: (props) ->
    super(props)

    @form = new Renderer(props.record || {}, @rerender, {
      componentId: @getId()
      defaultType: @props.defaultType
      force_partner: @props.force_partner,
      aboutToSend: @cleanupBeforeSend
    })

    @form.registerFields(companyFields, true)

    @state = {
      showSpinner: false,
      autoAssignTotalWeightError: null
    }

  setAutoAssignTotalWeightError: () =>
    autoAssignWeightFields = @form?.record.autoassignment_ranker?.weights
    if autoAssignWeightFields
      # Overcomplicated maths to circumvent floating point math issues, i.e.
      # 0.1 + 0.7 + 0.2 should === 1, but instead === 0.999999999999999... =/
      runningTotal = (parseFloat(autoAssignWeightFields.quality) * 1000 + parseFloat(autoAssignWeightFields.cost) * 1000 + parseFloat(autoAssignWeightFields.speed) * 1000) / 1000

      if runningTotal != 1
        if !@state.autoAssignTotalWeightError
          @setState({autoAssignTotalWeightError: 'Weight values have to add up to 1'})
          return true
      else if @state.autoAssignTotalWeightError
        @setState({autoAssignTotalWeightError: null})

  onConfirm: () =>
    if (@form? && !@setAutoAssignTotalWeightError()) then bind(@form.attemptSubmit, @form, @props.onConfirm, @props.onCancel, @showSpinner)

  showSpinner: (showSpinner) =>
    @setState
      showSpinner: showSpinner

  render: ->
    super(arguments...)

    renderAllInputsOptions = { treatments: @props.treatments }

    if @state.autoAssignTotalWeightError
      renderAllInputsOptions = { renderAllInputsOptions..., totalWeightError: true }

    TransitionModal extend({}, @props,
      extraClasses: 'company_form'
      recordLabel: "Company"
      showSpinner: @state?.showSpinner
      onConfirm: @onConfirm()),
      @form.renderAllInputs(renderAllInputsOptions)
      if @props.record?.distance_unit || @props.record?.currency
        if @props.record?.distance_unit != @form.record?.distance_unit || @props.record?.currency != @form.record?.currency
          div
            className: 'distance-or-currency-changed'
            'Changing Distance or Currency units will impact company invoices. Please contact Swoop Product before changing this selection.'


module.exports = withSplitIO(CompanyForm)
