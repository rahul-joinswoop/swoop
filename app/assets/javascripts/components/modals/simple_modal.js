import React, { useContext } from 'react'
import TransitionModal from 'components/modals'
import ModalStore from 'stores/modal_store'
import ModalContext from 'componentLibrary/Modal'

function SimpleModal(props) {
  const { hideModals } = useContext(ModalContext)

  function handleCancel() {
    if (props.onCancel) {
      props.onCancel()
    } else {
      ModalStore.closeModals()
      hideModals()
    }
  }

  return (
    <TransitionModal
      disabled={false}
      extraClasses={
        [
          'simple_modal',
          props.className ? props.className : '',
        ].join(' ')
      }
      onCancel={handleCancel}
      overlay={props.overlay}
      show
      {...props}
    >
      { props.body || props.children || null}
    </TransitionModal>
  )
}

export default SimpleModal
