import createReactClass from 'create-react-class'
import React from 'react'
require('stylesheets/components/modals/stripe_form.scss')
import Consts from 'consts'
StripeFields = require 'stripe_fields'
TransitionModal = React.createFactory require 'components/modals'
UserStore = require 'stores/user_store'
Renderer = require 'form_renderer'
import { bind, extend } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'

StripeBusinessAndPersonalInfoForm = createReactClass(
  displayName: 'BusinessInfoForm'
  generalChanged: -> @setState rev: if @state?['rev']? then ++@state?['rev'] else 0

  UNSAFE_componentWillReceiveProps: (props) ->
    stripeBusinessInfoFields = [
      StripeFields.BusinessNameField,
      StripeFields.EmployerIdNumber,
      StripeFields.BusinessStreetAddress,
      StripeFields.BusinessCity,
      StripeFields.BusinessState,
      StripeFields.BusinessZipCode,
      StripeFields.FirstNameField,
      StripeFields.LastNameField,
      StripeFields.SSNLastFourDigitsField,
      StripeFields.DateOfBirthField,
      StripeFields.PersonalStreetAddress,
      StripeFields.PersonalCity,
      StripeFields.PersonalState,
      StripeFields.PersonalZipCode
    ]

    if props.show and !@props.show
      record = props.record || {}
      @form = new Renderer(record, @generalChanged)
      @form.registerFields(stripeBusinessInfoFields, true)
      @setState
        showSpinner: false

  showSpinner: (showSpinner) ->
    @setState
      showSpinner: showSpinner

  render: ->
    children = [
      ReactDOMFactories.p({ key: 'helper-text' }, Consts.STRIPE_BUSINESS_AND_PERSONAL_INFO.HELPER_TEXT),
      @form.renderAllInputs() if @props.show
    ]

    TransitionModal(
      extend(
        {},
        @props,
        title: 'Business & Personal Info'
        extraClasses: 'stripe-business-form'
        recordLabel: 'Stripe Account Setup'
        showSpinner: @state?.showSpinner
        record: @props.record
        confirm: 'Continue'
        onConfirm: if @form? then bind(@form.attemptSubmit, @form, @props.onConfirm, @props.onCancel, @showSpinner)
      ),
      children
    )
)

module.exports = StripeBusinessAndPersonalInfoForm
