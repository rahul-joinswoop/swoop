import createReactClass from 'create-react-class'
import React from 'react'
require('stylesheets/components/modals/stripe_form.scss')
import Consts from 'consts'
StripeFields = require 'stripe_fields'
TransitionModal = React.createFactory require 'components/modals'
UserStore = require 'stores/user_store'
Renderer = require 'form_renderer'
import { bind, extend } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import stripe_bank_link_howto from 'public/stripe_bank_link_howto.png'

StripeBankInfoForm = createReactClass(
  displayName: 'BankInfoForm'
  generalChanged: -> @setState rev: if @state?['rev']? then ++@state?['rev'] else 0

  UNSAFE_componentWillReceiveProps: (props) ->
    stripeBankInfoFields = [
      StripeFields.BankRoutingNumber,
      StripeFields.BankAccountNumber
    ]

    if props.show and !@props.show
      record = props.record || {}
      @form = new Renderer(record, @generalChanged)
      @form.registerFields(stripeBankInfoFields, true)
      @setState
        showSpinner: false

  showSpinner: (showSpinner) ->
    @setState
      showSpinner: showSpinner

  render: ->
    children = [
      ReactDOMFactories.p({ key: 'helper-text' }, Consts.STRIPE_BANK_INFO.HELPER_TEXT),
      ReactDOMFactories.div
        className: 'how-to'
        key: 'how-to'
        ReactDOMFactories.img
          src: stripe_bank_link_howto
      @form.renderAllInputs() if @props.show,
      ReactDOMFactories.div
        className: 'stripe-bank-account-confidential'
        key: 'stripe-bank-account-confidential'
        'Swoop protects your bank account by keeping your financial information confidential.'
    ]

    titleBulletSteps = undefined

    TransitionModal(
      extend(
        {},
        @props,
        title: 'Bank Info'
        titleBulletSteps: titleBulletSteps
        extraClasses: 'stripe-bank-form'
        recordLabel: 'Stripe Account Setup'
        showSpinner: @state?.showSpinner
        record: @props.record
        confirm: 'Done'
        showCancel: false
        onCancel: @props.onCancel # we don't show cancel button, but the 'x' on the header of the modal will trigger onCancel anyway if clicked
        onConfirm: if @form? then bind(@form.attemptSubmit, @form, @props.onConfirm, @props.onCancel, @showSpinner)
      ),
      children
    )
)

module.exports = StripeBankInfoForm
