SymptomStore = require('stores/symptom_store')
ObjectForm = require('components/modals/object_form')
import Api from 'api'
import { filter, map, sortBy, values } from 'lodash'
import Utils from 'utils'
UserStore = require('stores/user_store')

class SymptomForm extends ObjectForm
  displayName: 'SymptomForm'
  _getStandardFunction: Api.standardSymptoms
  _changeFunction: Api.changeSymptoms
  _store: SymptomStore
  getObjTitle: -> Utils.getSymptomLabel(UserStore, {plural: true})
  getPlaceholder: -> "Custom #{Utils.getSymptomLabel(UserStore)}"
  getStandardObjs: () =>
    return sortBy(@state.standard_objects, (obj) -> obj.name.toLowerCase())
  getCustomObjs: () =>
    symptom_ids = map(@state.standard_objects, (symptom) -> symptom.id)
    return sortBy(filter(@_store.getCompanyItems(@getId()), (symptom) => symptom_ids.indexOf(symptom.id) == -1 and @state.remove.indexOf(symptom.id) == -1), (symptom) -> symptom.name.toLowerCase())

  getAll: () =>
    values(@_store.getCompanyItems(@getId())).concat(@state.custom)

module.exports = SymptomForm
