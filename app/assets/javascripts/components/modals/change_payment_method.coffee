import createReactClass from 'create-react-class'
import React from 'react'
import Logger from 'lib/swoop/logger'
TransitionModal = React.createFactory(require 'components/modals')
import Consts from 'consts'
StoreSelector = React.createFactory(require('components/storeSelector'))
JobStore = require ('stores/job_store')
CustomerTypeStore = require('stores/customer_type_store')
import { extend, filter, map, zipObject } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'

ChangePaymentMethod = createReactClass(
  displayName: 'ChangePaymentMethod'

  getInitialState: ->
    chosen_id: null
    error: null

  handleConfirm: ->
    if @state.chosen_id?
      return @props.onConfirm({
          id: @props.record.id
          new_payment_type_id: @state.chosen_id
        })
    else
      @setState
        error: 'Please Choose a Payment Type'

  componentDidMount: ->
    JobStore.bind( JobStore.CHANGE, @generalChanged )
    Logger.debug('ChangePaymentMethod modal did mount', @props)

  componentWillUnmount: ->
    JobStore.bind( JobStore.CHANGE, @generalChanged )

  generalChanged: ->
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously

  getCustomerTypes: () ->
    if @props.record?
      customerTypes = filter(@props.record.owner_company.customer_types, (customer_type) =>
        if @props.record.customer_type_name in [Consts.CUSTOMER_TYPE_WARRANTY, Consts.CUSTOMER_TYPE_GOODWILL, Consts.CUSTOMER_TYPE_BODY_SHOP]
          customer_type.name in [Consts.CUSTOMER_TYPE_CUSTOMER_PAY, Consts.CUSTOMER_TYPE_PCARD]
        else if @props.record.customer_type_name in [Consts.CUSTOMER_TYPE_CUSTOMER_PAY, Consts.CUSTOMER_TYPE_PCARD]
          customer_type.name in [Consts.CUSTOMER_TYPE_WARRANTY, Consts.CUSTOMER_TYPE_GOODWILL, Consts.CUSTOMER_TYPE_BODY_SHOP])

      zipObject map(customerTypes, (customerType) -> customerType.id),
        map(customerTypes, (customerType) ->
          name: customerType.name
        )

  handleCustomerTypeChange: (id) ->
    @setState
      chosen_id: id
      error: null

  render: ->
    TransitionModal extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'changePaymentMethodModalTransition'
      confirm: 'Save'
      title: 'Edit Payment Type'
      extraClasses: 'partner_modal'
      onConfirm: @handleConfirm
      cancel: 'Cancel'),
      Consts.JOB.EDIT_PAYMENT_TYPE
      ReactDOMFactories.br null
      ReactDOMFactories.br null
      ReactDOMFactories.label
        className: 'label-reg'
        htmlFor: 'store_selector'
        'Payment Type'
      StoreSelector
          onChange: @handleCustomerTypeChange
          value:  ''
          placeholder: 'Select a new payment type'
          populateFunc: @getCustomerTypes
          store: CustomerTypeStore
          populateEvent: CustomerTypeStore.CHANGE
      if @state.error?
        ReactDOMFactories.label
          className: 'error'
          @state.error
)

module.exports = ChangePaymentMethod
