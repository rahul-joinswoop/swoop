import React, { PureComponent, useState } from 'react'
import Button from 'componentLibrary/Button'
import { withModalContext } from 'componentLibrary/Modal'
import SimpleModal from 'components/modals/simple_modal'
import Tracking from 'tracking'
import Utils from 'utils'
import MobileAppScreenshot from './MW_App_Prompt.png'
import './style.scss'

const FEEDBACK_EMAIL = 'feedback@joinswoop.com'
const FEEDBACK_SUBJECT = 'Mobile Feedback'

const FeedBackForm = withModalContext((props = {}) => {
  const [feedback, setFeedback] = useState('')

  const handleFeedbackChange = e => setFeedback(e.target.value)

  const confirm = (
    <Button
      color="primary"
      component="a"
      href={`mailto:${FEEDBACK_EMAIL}?subject=${FEEDBACK_SUBJECT}&body=${feedback}`}
      onClick={props.hideModals}
      style={{ marginLeft: 10 }}
      target="_blank"
    >
      Submit
    </Button>
  )

  return (
    <SimpleModal
      cancel="Close"
      className="FeedBackForm"
      hideHeader
      onCancel={props.hideModals}
      updateConfirmButton={confirm}
    >
      <h4>We Appreciate Your Feedback</h4>
      <p>Why are you not interested in the Swoop Towing app?</p>
      <textarea className="form-control" onChange={handleFeedbackChange} placeholder="Type feedback here" rows="5" />
    </SimpleModal>
  )
})

class MobileAppPrompt extends PureComponent {
  state = {
    modal: 'prompt',
  }

  componentDidMount() {
    Tracking.track('Mobile Web App Prompt Displayed')
  }

  handleGetApp = () => {
    const appUrl = Utils.isIOS() ?
      'https://appsto.re/us/meEoeb.i' :
      'https://play.google.com/store/apps/details?id=com.swoopmobile'

    /* eslint-disable-next-line */
    window.open(appUrl)

    this.props.hideModals()

    Tracking.track('Mobile Web App Prompt - Get App')
  }

  handleLater = () => {
    this.setState({ modal: 'feedback' })
    Tracking.track('Mobile Web App Prompt - Skip')
  }

  render() {
    const { modal } = this.state

    if (modal === 'feedback') {
      return <FeedBackForm />
    }

    return (
      <SimpleModal
        cancel="Maybe Later"
        className="MobileAppPrompt"
        confirm="Get the Free App"
        hideHeader
        onCancel={this.handleLater}
        onConfirm={this.handleGetApp}
      >
        <img alt="Mobile app screenshot" className="MobileAppPrompt-image" src={MobileAppScreenshot} />
        <h4>Get the Swoop Towing App</h4>
        <p>
          Swoop does not support mobile web browsers.<br />
          Download the Swoop Towing app to receive and manage your jobs on mobile.
        </p>
      </SimpleModal>
    )
  }
}

export default withModalContext(MobileAppPrompt)
