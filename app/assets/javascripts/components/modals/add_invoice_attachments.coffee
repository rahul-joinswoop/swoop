import React from 'react'
require('stylesheets/components/modals/add_invoice_attachments.scss')
TransitionModal = React.createFactory(require 'components/modals')
UserStore = require ('stores/user_store')
JobStore = require('stores/job_store')
import Consts from 'consts'
BaseComponent = require('components/base_component')
import { extend, partial } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'

class AddInvoiceAttachment extends BaseComponent
  displayName: 'AddInvoiceAttachment'

  constructor: (props) ->
    super(props)
    @register(JobStore, JobStore.CHANGE)

  toggleDoc: (job_id, doc_id, e) ->
    JobStore.updateDocument(job_id, {
      id: doc_id,
      invoice_attachment: e.target.checked
    })

  getDocuments: ->
    JobStore.getAttachedDocuments(@props.job_id, Consts.TYPE_ATTACHED_DOCUMENT)

  render: ->
    super(arguments...)
    TransitionModal extend({}, @props,
      transitionName: 'AddInvoiceAttachments'
      title: @props.job_id + ": Attach Documents"
      extraClasses: "add_invoice_attachments"
      onConfirm: @props.onConfirm
      style:
        textAlign: "left"
      confirm: 'Send'),
      div null,
        span
          className: 'inline-block instructions'
          "Select documents that you'd like to send with the invoice."
        if @props.show
          ul null,
            for doc in @getDocuments()
              li null,
                input
                  type: "checkbox"
                  checked: doc.invoice_attachment
                  onChange: partial(@toggleDoc, @props.job_id, doc.id)
                ReactDOMFactories.a
                  target: "_blank"
                  href: doc.url
                  doc.original_filename



module.exports = AddInvoiceAttachment
