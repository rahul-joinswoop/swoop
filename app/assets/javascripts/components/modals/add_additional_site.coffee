import React from 'react'
require('stylesheets/components/modals/add_additional_site.scss')
import Dispatcher from 'lib/swoop/dispatcher'

TransitionModal = React.createFactory(require 'components/modals')
UserStore = require ('stores/user_store')
import Consts from 'consts'
Fields = require('fields/fields')
Renderer = require('form_renderer')
ModalStore = require('stores/modal_store')
PartnerCheckList = React.createFactory(require('components/partner_check_list'))
BaseComponent = require('components/base_component')
import { extend, filter, find, omit } from 'lodash'

class AddAdditionalSite extends BaseComponent
  displayName: 'AddAdditionalSite'

  constructor: (props) ->
    sites = []
    super(props)
    @state =
      sites: filter(props.company?.sites, (site) -> !site.dispatchable_by_fleet)
      error: null
      phone: null
      chosen: null

  setChosen: (chosen) =>
    @setState
      chosen: chosen

  createNewPartner: =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "partner_form",
      props: {
        phone: @state.phone
        type: 'create'
      }
    )

  addSelected: =>
    chosenSite = find(@state.sites, (site) => site.id == @state?.chosen )
    if chosenSite
      Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "partner_form",
        props: {
          record: @props.company
          site: chosenSite
          type: 'existing_site'
          title: Consts.MESSAGE_ADD_ADDITIONAL_SITE
        }
      )

  createNewSite: =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "partner_form",
      props: {
        record: omit(@props.company, ["location"])
        type: 'new_site'
        title: Consts.MESSAGE_ADD_ADDITIONAL_SITE
      }
    )

  render: ->
    super(arguments...)
    TransitionModal extend({}, @props,
      callbackKey: 'delete'
      transitionName: 'AddAdditionalSiteTransition'
      title: Consts.ADDITIONAL_SITE.TITLE
      extraClasses: "add_additional_site"
      onConfirm: @addSelected
      enableSend: @state?.chosen?
      confirm: Consts.ADDITIONAL_SITE.BUTTONS.ADD_SITE,
      fallback: Consts.ADDITIONAL_SITE.BUTTONS.CREATE_SITE,
      onFallback: @createNewSite),
      div null,
        div
          className: "instructions"
          Consts.ADDITIONAL_SITE.COMPANY_HAS_SITES(@props?.company?.name)
        if @props.show
          div null,
            PartnerCheckList
              sites: @state?.sites
              chosen: @state?.chosen
              setChosen: @setChosen

module.exports = AddAdditionalSite
