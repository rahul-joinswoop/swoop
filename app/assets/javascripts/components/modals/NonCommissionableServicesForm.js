import { sortBy, values } from 'lodash'
import AllObjectsForm from 'components/modals/AllObjectsForm'
import ServiceStore from 'stores/service_store'
import CommissionStore from 'stores/commission_store'
import { withModalContext } from 'componentLibrary/Modal'

class NonCommissionableServicesForm extends AllObjectsForm {
  displayName = 'NonCommissionableServicesForm'

  _changeFunction = CommissionStore.updateServiceCommissionsExclusions

  _store = ServiceStore

  _selectedItemsFunction = CommissionStore.companyContainsCommissionExclusionService

  _objTitle = 'Non Commissionable Services'

  _placeholder = 'services'

  getAll() {
    return sortBy(values(this._store.getServices(this.getId())), service =>
      service.name.toLowerCase(),
    )
  }
}

export const Component = NonCommissionableServicesForm
export default withModalContext(NonCommissionableServicesForm)
