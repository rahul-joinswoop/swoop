import 'stylesheets/components/modals/request_phone_call_back.scss'

import React from 'react'

import Consts from 'consts'
import TransitionModal from 'components/modals'
import Renderer from 'form_renderer'
import JobStore from 'stores/job_store'
import SiteStore from 'stores/site_store'
import Fields from 'fields/fields'
import BaseComponent from 'components/base_component'
import Utils from 'utils'
import { bind } from 'lodash'

class IsscCommentsField extends Fields.Field {
  _name = 'issc_comment'

  _label = 'Add any comments'
}

class RscDispatchNumberField extends Fields.PhoneField {
  _name = 'dispatch_number'

  _required = true

  _label = 'Dispatch Number'

  forcePrefill() {
    let siteDispatchNumber = SiteStore.get(this.props.record.site_id, 'phone', this.props.componentId)
    siteDispatchNumber = Utils.renderPhoneNumber(siteDispatchNumber)

    this.setValue(siteDispatchNumber, false, 'dispatch_number')
  }
}

class RscSecondaryNumberField extends Fields.PhoneField {
  _name = 'secondary_number'

  _label = 'Secondary Number'

  _placeholder = 'Optional'
}

class RequestPhoneCallBack extends BaseComponent {
  static defaultProps = {
    record: {},
  }

  constructor(props) {
    super(props)

    this.form = new Renderer({}, this.rerender, {
      componentId: this.getId(), record: props.record,
    })

    if (props.record.issc_dispatch_request?.system == Consts.DIGITAL_DISPATCH.RSC) {
      this.form.registerFields([RscDispatchNumberField, RscSecondaryNumberField])
    } else {
      this.form.registerFields([IsscCommentsField])
    }
  }

  onConfirm() {
    const { form, props } = this
    let requestCallBackObj = null
    let source = 'ISSC'

    if (props.record.issc_dispatch_request?.system == Consts.DIGITAL_DISPATCH.RSC) {
      requestCallBackObj = {
        dispatch_number: form.getRecordChanges().dispatch_number,
        secondary_number: form.getRecordChanges().secondary_number,
      }
      source = 'RSC'
    } else {
      requestCallBackObj = {
        comments: `comments: ${form.getRecordChanges().issc_comment}`,
      }
    }

    Tracking.trackDispatchEvent('Request Call Back', {
      source,
    })

    props.onConfirm(props.record, requestCallBackObj)
  }

  render() {
    super.render()

    const { form } = this
    const spanText = `Please confirm you'd like to request a call back from ${JobStore.getAccountNameByJobId(this.props.record?.id, this.getId())}.`

    const handleConfirm = bind(
      form.attemptSubmit,
      form,
      this.onConfirm
    )

    return (
      <TransitionModal
        {...this.props}
        extraClasses="request_phone_call_back"
        onConfirm={handleConfirm}
        title="Request Call Back"
      >
        <div>
          <span>
            { spanText }
          </span>
        </div>
        <div>
          {form.renderAllInputs()}
        </div>
      </TransitionModal>
    )
  }
}

export default RequestPhoneCallBack
