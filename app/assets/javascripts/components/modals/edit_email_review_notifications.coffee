import React from 'react'
require('stylesheets/components/modals/review_email_form.scss')
import ReactDOMFactories from 'react-dom-factories'
TransitionModal = React.createFactory(require 'components/modals')
Renderer = require('form_renderer')
Fields = require('fields/fields')
CompanyStore = require('stores/company_store').default
BaseComponent = require('components/base_component')
import { bind, extend } from 'lodash'

class EditEmailReviewNotifications extends BaseComponent
  displayName: 'EditEmailReviewNotifications'

  constructor: ->
    super(arguments...)

    fields = [
      ReviewEmailFields.ReviewEmailRecipientKeyField,
      ReviewEmailFields.ReviewEmailField
    ]

    if UserStore.getUser()?.company?.id
      record =
        id: UserStore.getUser().company.id
    else
      record =  {}

    @form = new Renderer(record, @rerender, {
      componentId: @getId()
    })
    @form.registerFields(fields, true)

    @state.showSpinner = false

  render: ->
    super(arguments...)

    children = [
      ReactDOMFactories.p({ key: 'helper-text' }, 'Email review notifications to:'),
      @form.renderAllInputs() if @props.show
    ]

    TransitionModal extend({}, @props,
      transitionName: 'EditEmailReviewNotificationsTransition'
      title: 'Review Emails'
      onConfirm: if @form? then bind(@form.attemptSubmit, @form, @props.onConfirm, @props.onCancel, @showSpinner)
      extraClasses: 'review_email_form'
      confirm: "Save"),
      children

ReviewEmailFields =
  ReviewEmailRecipientKeyField: class ReviewEmailRecipientKeyField extends Fields.SelectField
    _name: 'review_email_recipient_key'

    prefill: ->
      @setValue(CompanyStore.get(@record.id, 'review_email_recipient_key', @props.componentId))

    getOptions: ->
      [
        {text: 'All Admins', key: 'all_admins', value: 'all_admins'},
        {text: 'Specific Email Address', key: 'custom', value: 'custom'}
      ]

    renderInput: ->
      div null,
        div
          style:
            width: '200px'
            display: 'inline-block'
          super(arguments...)
        div
          style:
            paddingLeft: '10px'
            paddingRight: '10px'
            display: 'inline-block'
          if @getValue() == 'custom'
            " : "

  ReviewEmailField: class ReviewEmailField extends Fields.EmailField
    _name: 'review_email'
    _required: true

    prefill: ->
      @setValue(CompanyStore.get(@record.id, 'review_email', @props.componentId))

    renderInput: ->
      div null,
        div
          style:
            width: '250px'
            display: 'inline-block'
          super(arguments...)

    isShowing: ->
      @record?.review_email_recipient_key == 'custom'

module.exports = EditEmailReviewNotifications
