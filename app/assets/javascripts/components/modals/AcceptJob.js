import React from 'react'
import BaseComponent from 'components/base_component'
import JobStore from 'stores/job_store'
import Consts from 'consts'
import Dispatcher from 'lib/swoop/dispatcher'
import ModalStore from 'stores/modal_store'
import Tracking from 'tracking'
import NotificationStore from 'stores/notification_store'
import AuctionStore from 'stores/auction_store'
import AssignDriver from 'components/modals/AssignDriver'
import { extend } from 'lodash'
import moment from 'moment-timezone'

class AcceptJob extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      eta: null,
      reason: 0,
      error: null,
    }

    if (props.record) {
      NotificationStore.removeNotification(`Assigned Job #${props.record.id}`)
      NotificationStore.removeNotification(`#${props.record.id} is available for ETA submissions`)
    }

    this.register(AuctionStore, AuctionStore.AUCTION_RAN_OUT)
    this.register(AuctionStore, AuctionStore.TARGET_ETA_UPDATED)
  }

  goodEta = () => {
    const { eta } = this.state
    if (!eta || eta.length === 0) {
      this.setState({ error: 'You must set an ETA' })
      return false
    }

    if (eta === '0') {
      this.setState({ error: 'ETA must be greater than 0' })
      return false
    }

    return true
  }

  sendTracking = (label) => {
    Tracking.trackJobEvent('Accept Job', this.props.record?.id, {
      label: label ||
      (Consts.MOTORCLUBS.includes(this.props.record?.account?.name) ? 'Motor Club' : 'Standard'),
    })
  }

  handleEtaInputChange = (event) => {
    this.setState({
      eta: event.target.value.replace(/\D/g, ''),
      error: null,
    })
  }

  handleConfirm = () => {
    this.setState({
      error: null,
      error_reason: null,
    })

    if (AuctionStore.jobHasLiveAuction(this.props.record) && this.goodEta()) {
      const bidToETAData = {
        job_id: this.props.record.id,
        minutes: this.state.eta,
      }
      this.sendTracking('Auction')
      this.props.onConfirm(bidToETAData)
    } else if (this.props.record?.scheduled_for) {
      this.sendTracking(null)
      const confirmOptions = {
        id: this.props.record.id,
        status: Consts.ACCEPTED,
      }
      this.props.onConfirm(confirmOptions, true)
    } else if (this.goodEta()) {
      const data = {
        id: this.props.record.id,
        bta: moment().add(this.state.eta, 'minutes').toISOString(),
      }
      if (this.showEtaReasons()) {
        if (this.state.reason === 0) {
          this.setState({ error_reason: 'You must specify a reason' })
        } else {
          data.eta_explanation_id = parseInt(this.state.reason, 10)
        }
      }
      this.sendTracking(null)
      this.props.onConfirm(data, true)
    }
  }

  selectEtaReason = event => this.setState({ reason: event.target.value })

  requestCallback = (key, button) => {
    if (button) {
      Dispatcher.send(ModalStore.SHOW_MODAL, {
        key: 'request_phone_call_back',
        props: {
          record: this.props.record,
        },
      })
    } else {
      this.props.onCancel()
    }
  }

  showEtaReasons = () => (
    parseInt(this.state.eta, 10) >= 90 ||
    (this.props.record?.issc_dispatch_request?.max_eta &&
    parseInt(this.state.eta, 10) > this.props.record.issc_dispatch_request?.max_eta &&
    ['GCOAPI', 'AGO', 'QUEST'].includes(this.props.record.issc_dispatch_request?.client_id))
  )

  enableSend = (job) => {
    if (job?.status === Consts.AUTO_ASSIGNING) {
      return AuctionStore.jobHasLiveAuction(job)
    }
    return true
  }

  getCancelButton = (job) => {
    let cancelButton = null
    if (!AuctionStore.jobHasLiveAuction(job)) {
      cancelButton = Consts.MOTORCLUBS.includes(this.props.record?.owner_company?.name) ?
        'Request Call Back' :
        'Cancel'
    }
    return cancelButton
  }

  render() {
    super.render()
    const { record } = this.props
    const job = extend({}, record, JobStore.getJobById(record?.id))
    const { eta, error, reason } = this.state
    const { handleEtaInputChange, selectEtaReason, showEtaReasons } = this
    const isAuctionAndAutoAssigning = AuctionStore.jobHasAuction(job) &&
     job.status === Consts.AUTO_ASSIGNING
    const cancelButtonText = this.getCancelButton(job)
    const isCallBackJob = cancelButtonText === 'Request Call Back'

    return (
      <AssignDriver
        {...this.props}
        acceptJobMethods={{
          handleEtaInputChange, selectEtaReason, showEtaReasons,
        }}
        acceptJobState={{
          eta, error, job, reason,
        }}
        callbackKey="accept"
        cancel={cancelButtonText}
        confirm={record?.scheduled_for ? 'Accept' : 'Submit ETA'}
        enableSend={this.enableSend(job)}
        extraClasses={['driver-map-modal',
          'accept_modal',
          (record?.scheduled_for ? 'schedule-job' : ''),
          ((!isAuctionAndAutoAssigning && !record?.issc_dispatch_request) ? 'no-auction' : ''),
          (isCallBackJob ? 'callback-job' : '')].join(' ')}
        getId={this.getId}
        key="accept_modal_container_modal"
        onCancel={Consts.MOTORCLUBS.includes(record?.owner_company?.name) ?
          this.requestCallback :
          this.props.onCancel}
        onConfirmAccept={this.handleConfirm}
        record={record}
        show
        title={record?.scheduled_for ? 'Accept Job' : 'Submit ETA'}
        transitionName="acceptModalTransition"
      />
    )
  }
}

AcceptJob.displayName = 'AcceptJob'
export default AcceptJob
