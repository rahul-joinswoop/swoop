import Api from 'api'
import Dispatcher from 'lib/swoop/dispatcher'
import React from 'react'
require('stylesheets/components/modals/partner_form.scss')
UserStore = require 'stores/user_store'
TransitionModal = React.createFactory require 'components/modals'
Fields = require 'fields/fields'
Renderer = require 'form_renderer'
ProviderStore = require('stores/provider_store')
ErrorStore = require('stores/error_store')
import TagStore from 'stores/tag_store'
ModalStore = require 'stores/modal_store'
NotificationStore = require('stores/notification_store')
import Consts from 'consts'
BaseComponent = require('components/base_component')
import { withSplitIO } from 'components/split.io'
import { assign, clone, extend, map, pick, sortBy, values } from 'lodash'
import Utils from 'utils'

class PartnerForm extends BaseComponent
  displayName: 'PartnerForm'

  constructor: (props) ->
    super(props)
    record = props.record
    if !record?
      record = {}
    if props.phone?
      record.phone = props.phone

    if props.type == 'existing_site'
      #TODO: this needs to be a mix of the company and site properties to fill out the form
      @setExistingSiteRecordDefaults(record, props.site)

    @form = new Renderer(record, @rerender, {
      componentId: @getId()
    })
    @form.registerFields(@createFieldClasses(props))
    @form.isValid()

  getDefaultProps: =>
    record: {}

  createFieldClasses: (props) =>
    [
      class SiteNameField extends Fields.Field
        _name: 'site_name'
        _label: 'Site Name*'
        placeholder: ''
        _required: true
        isShowing: -> props?.type == 'new_site'
        getStyle: ->
          extend({}, super(arguments...), { flex: "1 1 auto", verticalAlign: "middle" })
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true
        renderInput: ->
          span
            style:
              display: "flex"
            span
              style:
                flex: "0 1 auto"
                whiteSpace: "nowrap"
                paddingTop: 8
                paddingRight: 5
              @record?.name
            super(arguments...)

      class NameField extends Fields.SpanField
        _name: 'name'
        _label: 'Name*'
        placeholder: ''
        _required: true
        isShowing: -> props?.type != 'new_site'
        isEnabled: ->
          if props.loadedFromJobDetails
            return false
          else
            !(props?.type == 'existing_site' and props.record?.name?) || props?.type == "create"
        isEditable: -> !@isEnabled() || props?.type == "create"

      class CompanyIDField extends Fields.Field
        _name: 'company.id'
        _label: 'Company ID*'
        _required: true
        isShowing: ->
          if props.loadedFromJobDetails
            return true
          else
            return false
        isEnabled: -> false

      class DispatchPhoneField extends Fields.PhoneField
        _name: "phone"
        _label: "Dispatch Phone"
        _required: true
        allowDisableMasking: true
        isEnabled: ->
          if props.loadedFromJobDetails
            return false
          if props?.type == 'create' and props.phone?.length > 0
            return false
          if props?.type == 'existing_site' and props.record?.phone?.length > 0
            return false
          return true

      class DispatchEmailField extends Fields.EmailField
        _name: "dispatch_email"
        _label: "Dispatch Email*"
        _required: true
        allowDisableMasking: true
        isEnabled: ->
          if props.loadedFromJobDetails
            return false
          else
            !(props?.type == 'existing_site' and props.record?.dispatch_email?)

      class PrimaryContactField extends Fields.CapitalizedField
        _name: "primary_contact"
        _label: "Primary Contact Name"
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true
      class PrimaryPhoneField extends Fields.PhoneField
        _name: "primary_phone"
        _label: "Primary Contact Phone"
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true
      class PrimaryEmailField extends Fields.EmailField
        _name: "primary_email"
        _label: "Primary Email"
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true
      class AccountingEmailField extends Fields.EmailField
        _name: "accounting_email"
        _label: "Accounting Email*"
        _required: true
        isEnabled: ->
          if props.loadedFromJobDetails
            return false
          else
            return !(props?.type == 'existing_site' and props.record?.accounting_email?)

      class FaxField extends Fields.PhoneField
        _name: "fax"
        _label: "Fax"
        isShowing: -> UserStore.isTesla()
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true

      class AddressField extends Fields.GeoSuggestField
        _name: 'location'
        _label: 'Address*'
        _required: true
        isEnabled: ->
          if props.loadedFromJobDetails
            return false
          else
            !(props?.type == 'existing_site' and props.record?.location?)

      class AutoAssignRadius extends Fields.NumberField
        _name: "auto_assign_distance"
        _label: "Auto Assign Radius (Miles)"
        _placeholder: "10"
        isShowing: ->
          UserStore.isSwoop() &&
          props?.treatments?[Consts.SPLITS.DISABLE_AUTO_ASSIGN_RADIUS_ON_PARTNERS] != true
        isEnabled: ->
          if props.loadedFromJobDetails
            return false
          return true

      class VendorCodeField extends Fields.Field
        _name: "vendor_code"
        _label: "Vendor Id"
        isShowing: -> UserStore.isTesla()
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true

      class LocationCodeField extends Fields.Field
        _name: "location_code"
        _label: "Location Id"
        isShowing: -> UserStore.isTesla()
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true

      class TireProgramField extends Fields.CheckboxField
        _name: "tire_program"
        _label: "Loaner Wheel Program"
        isShowing: -> UserStore.isTesla()
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true

      class NotesField extends Fields.TextareaField
        _name: "notes"
        _label: 'Special Instructions'
        isShowing: -> UserStore.isFleet()
        getContainerClasses: ->
          super(arguments...) + ' full'
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true

      class NetworkStatusField extends Fields.SelectField
        _name: "tags.0"
        _label: "Network Status"
        alwaysBeforeSend: (newrecord) ->
          #If the user unsets the tag, remove all tags
          if newrecord?.tags?[0] == ""
            newrecord.tags = []
        getOptions: ->
          options = TagStore.loadAndGetAll(@props.componentId, "name")
          currentTagId = parseInt(@getValue())
          if !isNaN(currentTagId) && !options[currentTagId]
            options[currentTagId] = TagStore.getById(currentTagId, true, @props.componentId)

          options = sortBy(options, (ct) ->  ct?.name?.toLowerCase())

          @optionMapper(options, clone(super(arguments...)))
        isEnabled: ->
          if props.loadedFromJobDetails
            return false

          return true

      class BillingTypeField extends Fields.SelectField
        _name: 'billing_type'
        _label: 'Billing Type'

        getOptions: -> @optionMapper(map(Consts.JOB.BILLING_TYPE, (value, key) -> {id: key, name: value}), [])
        isEnabled: -> false
        isShowing: -> UserStore.isSwoop() and props.loadedFromJobDetails

    ]

  setExistingSiteRecordDefaults: (record, site) ->
    existingRescueProviderAttributes = pick(
      site.fleet_rescue_provider,
      [
        'primary_contact', 'primary_phone', 'primary_email', 'status',
        'vendor_code', 'location_code', 'vendor_code', 'notes'
      ]
    )

    if existingRescueProviderAttributes
      assign(record, existingRescueProviderAttributes)

    record.name = site.name_with_company
    record.location = site.location
    record.phone = site.phone
    record.tire_program = site.tire_program

    record


  save: =>
    if @form.isValid()
      newRecord = @form.getRecordChanges()
      #TODO: figure out why we are using this instead of the forceFullObject property ENG-6133
      record = @form.record
      #Once above TODO is figured out should be able to remove this line ENG-6133
      record.tags = map(values(record.tags), (tagId) -> parseInt(tagId))
      if @props.type == 'create'
        Api.addNewRescueProvider(partner: record,
          success: ->
            Utils.sendNotification("Rescue Provider, Site, and Company have been created.", Consts.SUCCESS)
          error: (data, textStatus, errorThrown) ->
            Dispatcher.send(ErrorStore.SHOW_ERROR,
              data: data
              textStatus: textStatus
              errorThrown: errorThrown
            )
        )
        Dispatcher.send(ModalStore.CLOSE_MODALS)

      else if @props.type == 'existing_site'
        record.site_id = @props.site.id

        Api.addExistingPartner(partner: record,
          success: ->
            Utils.sendNotification("Rescue Provider has been created.", Consts.SUCCESS)
          error: (data, textStatus, errorThrown) ->
            Dispatcher.send(ErrorStore.SHOW_ERROR,
              data: data
              textStatus: textStatus
              errorThrown: errorThrown
            )
        )
        Dispatcher.send(ModalStore.CLOSE_MODALS)
      else if @props.type == 'new_site'
        record.name = record.site_name
        delete record.site_name
        Api.addNewSite(partner: record,
          success: ->
            Utils.sendNotification("Rescue Provider and Site have been created.", Consts.SUCCESS)
          error: (data, textStatus, errorThrown) ->
            Dispatcher.send(ErrorStore.SHOW_ERROR,
              data: data
              textStatus: textStatus
              errorThrown: errorThrown
            )
        )
        Dispatcher.send(ModalStore.CLOSE_MODALS)
      else
        @props.onConfirm newRecord
    else
      @form.attemptedSubmit()
      @rerender()

  addAdditionalSite: (x, button) =>
    #TODO: maybe show a spinner
    @props?.onCancel?()
    if button
      Dispatcher.send(ModalStore.CLOSE_MODALS)

      Api.loadPartnerCompany(@props.record?.company?.id,
        success: (data) =>
          showList = false
          for site in data?.sites
            if site.dispatchable_by_fleet != true
              showList = true
              break

          if showList
            Dispatcher.send(ModalStore.SHOW_MODAL,
              key: "add_additional_site",
              props:
                company: data
            )
          else
            Dispatcher.send(ModalStore.SHOW_MODAL,
              key: "partner_form",
              props: {
                record: data
                type: 'new_site'
                title: "Add Additional Site"
              }
            )
        error: (data) =>
          #TODO: throw error indicating that an additional site can not be added right now, try again later
      )

  getTitle: =>
    if @props?.title?
      return @props.title
    if @props?.record?.id
      return 'Edit Record'
    return 'Create Record'

  showAddAdditionalSite: =>
    if UserStore.isFleetInHouse() and !@props?.type?
      srecord = JSON.stringify(@form?.record)
      soriginal = JSON.stringify(@form?.orig_record)
      return srecord == soriginal || (srecord? and soriginal? and srecord.replace(/null/g, '""') == soriginal.replace(/null/g, '""'))


  render: ->
    super(arguments...)
    record = @props.record
    TransitionModal extend({}, @props,
      key: 'partner_modal_container_modal'
      callbackKey: 'partner_form_edit'
      transitionName: 'partnerModalTransition'
      title: @getTitle()
      confirm: 'Save'
      extraClasses: 'partner_form'
      enableSend: !@props.loadedFromJobDetails
      onConfirm: @save
      cancel: if @showAddAdditionalSite() then "Add Additional Site" else @props.cancel
      onCancel: if @showAddAdditionalSite() then @addAdditionalSite else @props.onCancel
      footerView: null
      fullHeightModal: false),
      @form.renderAllInputs()

module.exports = withSplitIO(PartnerForm)
