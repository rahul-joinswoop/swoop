import React from 'react'
import Api from 'api'
require('stylesheets/components/modals/add_additional_site.scss')
TransitionModal = React.createFactory(require 'components/modals')
import Utils from 'utils'
UserStore = require ('stores/user_store')
CompanyStore = require('stores/company_store').default
Fields = require('fields/fields')
Renderer = require('form_renderer')
PartnerCheckList = React.createFactory(require('components/partner_check_list'))
BaseComponent = require('components/base_component')
import { extend } from 'lodash'

class EditWaiver extends BaseComponent
  displayName: 'EditWaiver'
  setChosen: (chosen) =>
    @setState
      chosen: chosen

  constructor: (props) ->
    super(props)
    @state.waiver = UserStore.getCompany()[props.type+"_waiver"]

  addWaiver: =>
    obj = {}
    obj[@props.type+"_waiver"] = @state.waiver

    Api.objectsRequest(UserStore.getEndpoint()+"/company", Api.POST, null,
      success: =>
        Utils.sendSuccessNotification("Waiver Updated")
      error: =>
        Utils.sendErrorNotification("waiver Failed to update")

    , obj)
    @props.onCancel()

  render: ->
    super(arguments...)
    company = UserStore.getCompany()
    text = ""
    if @props.type
      text = @props.type.capitalize()
    if @props.type == "dropoff"
      text = "Drop Off"

    TransitionModal extend({}, @props,
      callbackKey: 'delete'
      transitionName: 'EditWaiverTransition'
      title: "Edit #{text} Waiver"
      onConfirm: @addWaiver
      extraStyles: width: 600, maxWidth: 600
      confirm: "Save"),
      div null,
        div
          className: "instructions"
          if @props.type == "invoice"
            "This waiver language appears on PDF invoices"
          else
            "#{text} Waiver language for signatures inside the Swoop Towing App"

        div null,
          textarea
            className: "form-control"
            style:
              display: "block"
              minHeight: 120
              marginTop: 10
            maxLength: if @props.type == "invoice" then 750
            value: @state.waiver
            onChange: (e) =>
              @setState waiver: e.target.value
          if @props.type == "invoice" && @state.waiver? && @state.waiver.length >= 750
            span
              style:
                color: "red"
                marginTop: 10
                display: "block"
              "750 character max"


module.exports = EditWaiver
