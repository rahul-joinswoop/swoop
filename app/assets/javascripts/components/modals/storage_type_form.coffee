import React from 'react'
EditForm = React.createFactory(require('components/edit_form'))
StorageTypeStore = require('stores/storage_type_store')
BaseComponent = require('components/base_component')
ModalStore = require('stores/modal_store')
import Utils from 'utils'
import Dispatcher from 'lib/swoop/dispatcher'
import { extend, find, get } from 'lodash'

class StorageTypeForm extends BaseComponent
  displayName: 'StorageTypeForm'

  submitStorageType: (storageType) =>
    if storageType.id?
      StorageTypeStore.updateItem(storageType)
    else
      StorageTypeStore.addItem(storageType)
    Dispatcher.send(ModalStore.CLOSE_MODALS)


  render: ->
    super(arguments...)

    enabled = not @props.enabled? or enabled == true
    that = @
    data = [
      {
        name: "name"
        label: "Name*"
        required: true
        isValid: (col) ->
          value = get(@record, col.name)
          # the entire collection will be loaded at this moment
          db_type = find(StorageTypeStore.getAll(false, that.getId()), (type) =>
            return type.name == value && @record.id != type.id
          )
          if db_type?
            return "Name Exists"
      }
    ]
    title = "Create Storage Type"
    if @props.record? and @props.record.id?
      title = "Edit Storage Type"

    EditForm extend({}, @props,
      cols: [{
        style:
          marginTop: 15
        fields:data}]
      title: title
      showSubmit: enabled
      submitButtonName: if @props?.record then 'Save' else 'Add'
      onConfirm: @submitStorageType

    )

module.exports = StorageTypeForm
