import Api from 'api'
import ReactDOMFactories from 'react-dom-factories'

import createReactClass from 'create-react-class'
import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
import Consts from 'consts'
StoreSelector = React.createFactory(require('components/storeSelector'))
BackendSuggest = React.createFactory require('components/autosuggest/backend_suggest').default
import { extend, filter, map } from 'lodash'

AssignNewJobProvider = createReactClass(
  displayName: 'AssignNewJobProvider'

  getInitialState: ->
    chosen_id: null
    error: null

  handleConfirm: ->
    if @state.chosen_id?
      obj =
        id: @props.record.id
        old_rescue_provider_id: @props.record.rescue_company.id
        new_rescue_provider_id: @state.chosen_id

      @props.onConfirm(obj)
    else
      @setState
        error: 'Please Choose a Partner'


  handlePartnerChange: (id) ->
    @setState
      chosen_id: id
      error: null

  render: ->
    fleet_company_name = 'fleet company'

    if @props.record?.root_dispatcher?.company_name?
      fleet_company_name = @props.record.root_dispatcher.company_name

    TransitionModal extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'editAssignedPartnerModalTransition'
      confirm: 'Assign'
      title: "Edit Assigned Partner"
      extraClasses: 'partner_modal'
      onConfirm: @handleConfirm
      cancel: 'Cancel'),
      Consts.JOB.SELECT_NEW_PARTNER(fleet_company_name)
      ReactDOMFactories.br null
      ReactDOMFactories.br null
      ReactDOMFactories.div
        className: ''
        ReactDOMFactories.label
          className: 'label-reg'
          htmlFor: 'store_selector'
          'Partner'
        BackendSuggest
          style:
            height: 32
          onSuggestSelect: (suggest) =>
            if suggest?
              @handlePartnerChange(suggest.id)
            else
              @handlePartnerChange(null)
          getSuggestions: (options, callback) ->
            if !UserStore.isSwoop() && !UserStore.isFleetInHouse()
              return
            Api.autocomplete_rescue_providers(options.input, 10, UserStore.getEndpoint(), {
              success: (data) =>
                providers_without_other = filter(data, (provider) -> provider.name != 'Other' and provider.name != 'Other HQ')

                locationMap = map(providers_without_other, (rescue_provider) ->
                  id: rescue_provider.rescue_company_id
                  label: rescue_provider.name
                )
                callback(locationMap)
              error: (data, textStatus, errorThrown) =>
               console.warn("Autocomplete failed")
            })
        if @state.error?
          ReactDOMFactories.label
            className: 'error'
            @state.error
)
module.exports = AssignNewJobProvider
