import React from 'react'
require('stylesheets/components/modals/poi_site_form.scss')
import Consts from 'consts'
Fields = require 'fields/fields'
SiteFields = require 'site_fields'
TransitionModal = React.createFactory require 'components/modals'
Renderer = require 'form_renderer'
BaseComponent = require('components/base_component')
import { bind, extend, get } from 'lodash'
import Utils from 'utils'
import ReactDOMFactories from 'react-dom-factories'
import { mixin } from 'fields/common/field'

InheritMixin = (prop) ->
  beforeSend: () ->
    if get(@record, @getName())? and get(@original, prop)?
      @record[prop].id = get(@original, prop).id

class NameField extends SiteFields.NameField
  _label: 'Place Name'

class ContactName extends Fields.Field
  _name: 'manager.full_name'
  _label: 'Contact Name'
  mixin(@, InheritMixin("manager"))

class PlaceForm extends BaseComponent
  displayName: 'PlaceForm'
  @defaultProps: record: {}
  constructor: (props) ->
    super(props)
    placeFields = [
      NameField,
      SiteFields.LocationTypeField,
      SiteFields.LocationField,
      SiteFields.PhoneField,
      ContactName
    ]

    record = props.record || {}
    @form = new Renderer(record, @rerender)
    @form.registerFields(placeFields, true)
    @state.showSpinner = false

  showSpinner: (showSpinner) =>
    @setState
      showSpinner: showSpinner

  render: ->
    super(arguments...)

    children = [
      ReactDOMFactories.p({ key: 'helper-text' }, Consts.POI_SITE.HELPER_TEXT),
      @form.renderAllInputs() if @props.show
    ]

    TransitionModal(
      extend(
        {},
        @props,
        title: 'Add Place'
        extraClasses: 'poi_site_form'
        recordLabel: 'Place'
        showSpinner: @state?.showSpinner
        onConfirm: if @form? then bind(@form.attemptSubmit, @form, @props.onConfirm, @props.onCancel, @showSpinner)
      ),
      children
    )


module.exports = PlaceForm
