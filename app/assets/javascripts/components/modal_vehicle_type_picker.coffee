import React from 'react'
import Logger from 'lib/swoop/logger'
TransitionModal = React.createFactory(require 'components/modals')
VehicleCategoryStore = require ('stores/vehicle_category_store')
UserStore = require ('stores/user_store')
StoreSelector = React.createFactory(require('components/storeSelector'))
BaseComponent = require('components/base_component')
import { filter, partial, sortBy } from 'lodash'

class TypePickerModal extends BaseComponent
  displayName: 'TypePickerModal'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()

  getInitialState: ->
    type_id: null
    type_name: null
    error: null

  onConfirm: =>
    ids = []
    for id, val of @state
      if val? and val and id != "error"
        ids.push(id)
    if ids.length > 0
      @props.onConfirm ids
    else
      @setState
        error: "Choose Type"

  handleTypeChange: (id, e) =>
    Logger.debug("picker ",id, e)
    obj = {error: null}
    obj[id] = e.currentTarget.checked
    @setState obj

    #@setState
    #  type_id: id
    #  type_name: value

  getTypes: =>
    companyTruckTypes = @props.companyTruckTypes
    cats = VehicleCategoryStore.getAllSorted(@getId(), companyTruckTypes)

    if @props.exclude?
      cats =  filter(cats, (cat) =>
        return cat.id not in @props.exclude
      )

    cats = sortBy(cats, "order_num")
    return cats

  handleChange: (e) =>
    @setState
      email: e.target.value
      error: null

  render: ->
    super(arguments...)

    TransitionModal $.extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'TypeModalTransition'
      confirm: 'Add'
      title: "Add Class Type"
      extraClasses: "type_picker_modal"
      onConfirm: @onConfirm
      cancel: 'Cancel'),
      div null,
        div
          className: "header_info"
          "Select what Class Types you want to specify custom rates for:"
        for id, type of @getTypes()
          div
            key: "type_"+type.id
            className: "type_items",
            input
              id: "type_"+type.id
              name: "type_"+type.id
              type: "checkbox"
              checked: @state[type.id]
              onChange: partial(@handleTypeChange, type.id)
            label
              htmlFor: "type_"+type.id
              type.name
        label
          className: 'error'
          @state.error

module.exports = TypePickerModal
