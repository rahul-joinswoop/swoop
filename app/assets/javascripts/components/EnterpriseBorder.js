import React from 'react'
import $ from 'jquery'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import { Header } from 'components/Header'
import ModalStore from 'stores/modal_store'
import MobileAppPrompt from 'components/modals/MobileAppPrompt'
import OptionsModal from 'components/modal_options'
import { withModalContext } from 'componentLibrary/Modal'
import PopupModal from 'components/modal_popup'
import Storage from 'stores/storage'
import UserStore from 'stores/user_store'
import Utils from 'utils'

const MOBILE_APP_PROMPT_WIDTH = 900
const MOBILE_APP_PROMPT_KEY = 'EnterpriseBorder.mobileAppPromptShown'

const EnterpriseFooter = () => (
  <footer className="main-footer">
    <ul>
      <li className="left">Contact Us: <a href="tel:+18662198136">(866) 219-8136</a> | <a href={Consts.CONTACT_URL} rel="noopener noreferrer" target="_blank">Email</a></li>
      <li>©{new Date().getFullYear()} SwoopMe, Inc.</li>
      <li><a href="/SwoopPrivacy.pdf" rel="noopener noreferrer" target="_blank">Privacy</a></li>
      <li><a href="/SwoopTerms.pdf" rel="noopener noreferrer" target="_blank">Terms</a></li>
    </ul>
  </footer>
)

class EnterpriseBorder extends BaseComponent {
  constructor(props) {
    super(props)
    this.register(ModalStore, ModalStore.CHANGE)
    this.register(UserStore, UserStore.USERME_CHANGED)
  }

  static isSmallMobileScreen() {
    return Utils.isMobile() && $(window).width() < MOBILE_APP_PROMPT_WIDTH
  }

  showMobileAppPrompt = () => {
    if (!Storage.get(MOBILE_APP_PROMPT_KEY) && UserStore.isPartner() && this.constructor.isSmallMobileScreen()) {
      Storage.set(MOBILE_APP_PROMPT_KEY, 1)
      this.props.showModal(<MobileAppPrompt />)
    }
  }

  componentDidMount() {
    super.componentDidMount()
    this.showMobileAppPrompt()
  }

  componentDidUpdate() {
    super.componentDidUpdate()
    this.showMobileAppPrompt()
  }

  render() {
    return (
      <div
        className={[
          'wrapper',
          ((ModalStore.getModal() || this.props.modalShowing) ? 'modal_is_open' : undefined),
        ].filter(el => el != null).join(' ')}
      >
        <Header {...this.props} />
        { ModalStore.getModal() === 'options' &&
          <OptionsModal {...ModalStore.getProps()} />}
        { ModalStore.getPopupProps() &&
          <PopupModal {...ModalStore.getPopupProps()} />}
        <>
          {this.props.children}
        </>
        { (this.props.showFooter || this.props.showFooter === undefined) &&
          <EnterpriseFooter />}
      </div>
    )
  }
}

module.exports = withModalContext(EnterpriseBorder)
