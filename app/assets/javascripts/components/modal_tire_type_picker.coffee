import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
TireStore = require ('stores/tire_store')
StoreSelector = React.createFactory(require('components/storeSelector'))
BaseComponent = require('components/base_component')

class TireTypePickerModal extends BaseComponent
  displayName: 'TireTypePickerModal'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()

  getInitialState: ->
    tire_type_id: null
    tire_name: null
    error: null

  onConfirm: =>
    if @state.tire_type_id != null
      @props.onConfirm
        id: @state.tire_type_id
    else
      @setState
        error: "Choose Tire Type"

  handleTireSizeChange: (id, value) =>
    @setState
      tire_type_id: id
      tire_name: value
      error: null

  handleCopyChange: (id, e) =>
    obj = {error: null}
    obj[id] = e.currentTarget.checked
    @setState obj

  getTireSizes: =>
    TireStore.getNotAddedTiresListForStore(@getId(), @props.excludeIds)

  UNSAFE_componentWillReceiveProps: (props) ->
    if not props.show
      @setState @getInitialState()

  handleChange: (e) ->
    @setState
      email: e.target.value
      error: null

  render: ->
    super(arguments...)

    TransitionModal $.extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'AccountModalTransition'
      confirm: 'Add'
      title: "Add Tire Size"
      extraClasses: "account_picker_modal"
      onConfirm: @onConfirm
      cancel: 'Cancel'),
      div null,
        div null,
          "Choose a new Tire Size you’d like to add tires for."
        StoreSelector
          value: @state.tire_name
          onChange: @handleTireSizeChange
          populateFunc: @getTireSizes
          placeholder: "-Select-"
        label
          className: 'error'
          @state.error

module.exports = TireTypePickerModal
