import Consts from 'consts'
import moment from 'moment-timezone'

import React from 'react'
import Utils from 'utils'
Countdown = React.createFactory(require 'components/countdown')
import TimeStore from 'stores/time_store'
JobStore = require('stores/job_store')
BaseComponent = require('components/base_component')
ScheduledDateTimeInfoBubble= require('components/job/scheduled_datetime_info_bubble')

class DashboardETATime extends BaseComponent
  displayName: 'eta_time'

  renderLiveScheduledTime: (jobObject, countdownProps) =>
    span null,
      ScheduledDateTimeInfoBubble
        jobId: jobObject.id
      Countdown
        time: moment(jobObject.toa.live)
        actualETA: Utils.isETA(jobObject)
      span null,
      " ("
      span null,
      @renderScheduledCountdown(countdownProps)
      span null,
      ")"

  renderScheduledTime: (jobObject, countdownProps) =>
    span null,
      ScheduledDateTimeInfoBubble
        jobId: jobObject.id
      @renderScheduledCountdown(countdownProps)

  renderScheduledCountdown: (countdownProps) ->
    Countdown
      time: countdownProps.time
      actualEta: countdownProps.actualETA

  render: ->
    super(arguments...)

    TimeStore.watchMinute(@getId())

    jobObject = {
      id: @props.jobId
      scheduled_for: JobStore.get(@props.jobId, 'scheduled_for', @getId())
      toa: JobStore.get(@props.jobId, 'toa', @getId())
    }

    lastETA = Utils.getLastEtaObj(jobObject)

    if jobObject.toa && JobStore.get(@props.jobId, 'status', @getId()) in [Consts.CREATED, Consts.PENDING, Consts.REJECTED, Consts.UNASSIGNED, Consts.REASSIGN, Consts.ASSIGNED, Consts.ACCEPTED, Consts.SUBMITTED, Consts.DISPATCHED, Consts.ENROUTE]
      countdownProps =
        time: lastETA
        actualETA: Utils.isETA(jobObject)

      span null,
      if jobObject.scheduled_for
        if jobObject.toa.live
          @renderLiveScheduledTime(jobObject, countdownProps)
        else
          @renderScheduledTime(jobObject, countdownProps)
      else
        ret = Utils.getLastEtaObj(jobObject)
        span null,
          Countdown
            time: ret
            actualEta: Utils.isETA(jobObject)
          if ret and jobObject.toa.original and !moment(jobObject.toa.original).isSame(ret) and !Utils.isDoubleOverdue(jobObject)
            span null,
              span null,
                " ("
              Countdown
                time: moment(jobObject.toa.original)
                actualEta: false
              span null,
                ")"+(if jobObject.toa.count>2 then "^"+(jobObject.toa.count-1) else "")
    else
      "--"

module.exports = DashboardETATime
