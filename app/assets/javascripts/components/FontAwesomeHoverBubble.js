import React from 'react'
import PropTypes from 'prop-types'
import 'stylesheets/components/FontAwesomeHoverBubble.scss'

const FontAwesomeHoverBubble = ({
  bubbleContents, className, color, flipToBottom, fontSize,
}) => (
  <i
    className={`font-awesome-hover-bubble fa ${className} ${flipToBottom ? 'flip' : ''}`}
    style={{
      color,
      fontSize,
      width: fontSize,
    }}
  >
    <div className="error-bubble-container">
      <div className="bubble"><span>{bubbleContents}</span></div>
    </div>
  </i>
)

FontAwesomeHoverBubble.defaultProps = {
  color: 'red',
  flipToBottom: false,
  fontSize: 16,
}

FontAwesomeHoverBubble.propTypes = {
  bubbleContents: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.element,
  ]).isRequired,
  className: PropTypes.string.isRequired,
  color: PropTypes.string,
  flipToBottom: PropTypes.bool,
  fontSize: PropTypes.number,
}

export default FontAwesomeHoverBubble
