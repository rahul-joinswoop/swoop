import React from 'react'
import Dispatcher from 'lib/swoop/dispatcher'
import Button from 'componentLibrary/Button'

require('stylesheets/components/storage_index.scss')
import Logger from 'lib/swoop/logger'
import EventRegistry from 'EventRegistry'
JobStore = require('stores/job_store')
InvoiceStore = require('stores/invoice_store')
ServiceStore = require('stores/service_store')
UserStore = require('stores/user_store')
SiteStore =  require('stores/site_store').default
FeatureStore = require('stores/feature_store')
UserSettingStore = require('stores/user_setting_store')
import TabsImport from 'components/tabs'
Tabs = React.createFactory(TabsImport)
ModalStore = require('stores/modal_store')
JobList = React.createFactory(require('components/job/job_list'))
import SearchBox from 'components/search_box'
FTable = React.createFactory(require 'components/resizableTable/FTable')
import Consts from 'consts'
import JobDetailsImport from 'components/job_details'
JobDetails = React.createFactory(JobDetailsImport.default)
import Utils from 'utils'
TireList =  React.createFactory(require('components/tire_list'))
StorageList =  React.createFactory(require('components/storage_list'))
StorageTypeFilter = React.createFactory(require 'components/filters/storage_type_filter')
StorageSiteFilter = React.createFactory(require 'components/filters/storage_site_filter')
BaseComponent = require('components/base_component')
import { filter, partial, values } from 'lodash'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class StorageIndex extends BaseComponent
  displayName: 'StorageIndex'
  constructor: (props) ->
    super(props)
    @state = {
      tire_ids: []
      search: null
      searchKey: "id"
      searchOrder: "desc"
      filters: []
    }
    @selectedStorageSites = null
    @selectedStorageTypes = null

    @settingChangeWatcherId = @createNewWatcher(@updateJobSearch)
    if UserSettingStore.isAllLoaded()
      @updateJobSearch()
    else
      @register(UserSettingStore, UserSettingStore.LOADED, @updateJobSearch)

    #HACK: Add Vehicle requires that storage be loaded before the form is open for setting the service
    ServiceStore.getServiceByName("Storage")

    @register(FeatureStore, FeatureStore.LOADED)
    @register(InvoiceStore, InvoiceStore.CHANGE)
    @register(JobStore, JobStore.CHANGE)
    @register(JobStore, JobStore.SEARCH_CHANGED)
    @register(JobStore, JobStore.SEARCH_LOADED)
    @register(UserStore, UserStore.LOGIN)

  updateJobSearch: =>
    EventRegistry.clearListeners(@settingChangeWatcherId)
    @selectedStorageTypes = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_STORAGE_TYPES, @settingChangeWatcherId)
    @selectedStorageSites = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_STORAGE_SITES, @settingChangeWatcherId)
    if !JobStore.isLoadingSearch()
      @searchJobs()
      @rerender()

  showTireSizes: =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "tire_type_picker_modal",
      props:
        excludeIds: @state.tire_ids
        onConfirm: (obj) =>
          ids = @state.tire_ids
          if not ids?
            ids = []
          @setState
            tire_ids: ids.concat([obj.id])

          @rerender()
          Dispatcher.send(ModalStore.CLOSE_MODALS)
    )

  changeTab: (tab) =>
    setTimeout () =>
      @setState
        activeTab: tab?.name
    , 0

  jobList: () =>
    jobs = if JobStore.isLoadingSearch() then [] else JobStore.getRecordsOfCurrentSearchPage()
    jobs = filter(jobs, (data) =>
      if UserStore.isRoot()
          if data.type != "FleetManagedJob"
              return false
      statuses = [Consts.STORED]
      if data?
        return data.status in statuses and data.storage? and not data.storage.released_at? and not data.storage.deleted_at?
    )
    return jobs

  searchJobs: (search, searchKey, searchOrder, searchFilters) =>
    if typeof search == "undefined"
      search = @state.search
    if typeof searchKey == "undefined"
      searchKey = @state.searchKey
    if typeof searchOrder == "undefined"
      searchOrder = @state.searchOrder
    if typeof searchFilters == "undefined"
      searchFilters = @state.filters

    filters = stored: true
    if searchFilters.site_id?
      filters.storage_site_id = searchFilters.site_id

    if UserStore.isOnlyDriver()
      filters["rescue_driver_id"] = UserStore.getUser().id
    if UserStore.isRoot()
      filters["fleet_managed_jobs"] = true

    if @selectedStorageTypes
      filters[Consts.USER_SETTING.FILTERED_STORAGE_TYPES] = @selectedStorageTypes
    if @selectedStorageSites
      filters[Consts.USER_SETTING.FILTERED_STORAGE_SITES] = @selectedStorageSites

    if search?
      delete filters.stored
      filters.status = "stored"
      filters = {filters: filters}

    fields = ["id", "status", "type", "storage", "storage_type_id", "account_id", "year", "make", "model", "color", "license", "vin", "partner_live_invoices", "child_job"]
    filters.fields = fields.join(",")

    tab = if UserStore.isPartner() then 'storage_index' else ''

    order = searchKey+","+searchOrder
    if search? or searchKey not in ["id", "storage_date"]
      order = null

    JobStore.search(search, filters, order)

  searchChange: (value) =>
    if value == ""
      value = null

    Logger.debug("search changed to ", value)
    @searchJobs(value, @state.searchKey, @state.searchOrder)
    @setState
      search: value

  research: () =>
    @searchJobs()

  sortChange: (key, order) =>
    searchOrder =  Utils.getOrderString(order)
    @searchJobs(@state.search, key, searchOrder)
    @setState
      searchKey: key
      searchOrder: searchOrder

  filterList: (filters) =>
    @searchJobs(@state.search, @state.searchKey, @state.searchOrder, filters)
    @setState
      filters: filters

  render: ->
    super(arguments...)
    tabList = []

    if UserStore.isPartner()
      sites = values(SiteStore.loadAndGetAll(@getId(), 'tire_program'))

      if FeatureStore.isFeatureEnabled("Storage")
        tabList.push({
          name: "Vehicles"
          onEnter: =>
            JobStore.resetSearchResult()
            @searchJobs(null, "id", "desc")
            @setState
              search: null
              searchKey: "id"
              searchOrder: "desc"
          component: =>
            if UserStore.getUser()?
              div null,
                StorageList
                  search: @state.search
                  jobs: @jobList()
                  onSortChange: @sortChange
                  research: @research
                if JobStore.isLoadingSearch()
                  Loading null
        })

      if sites?
        for site in sites
          if site.tire_program
            tabList.push({
              name: "Tires"
              component: =>
                if UserStore.getUser()?
                  TireList
                    tire_ids: @state.tire_ids
            })
            break

    else if UserStore.isTesla()
      tabList.push({
        name: "Tires"
        component: =>
          if UserStore.getUser()?
            TireList()
      })

    div
      id: 'content-wrapper'
      className: 'content-wrapper storage storage_container'
      #if @state.activeTab == "Vehicles"
      #  Filters
      #    filterList: @filterList
      #    showServiceFilter: false
      #    showDriverFilter: false
      #    showTruckFilter: false
      #    showDate: false
      #    showBetweenDates: false
      #    showAccountFilter: false
      #    showSiteFilter: true

      Tabs
        changeTab: @changeTab
        rev: @state.rev
        tabList: tabList
        sideComponents:
          div
            className: "sideComponents"
            if not UserStore.isRoot() and not UserStore.isOnlyDriver() and @state.activeTab == "Vehicles"
              Button
                color: 'primary'
                label: 'Add Vehicle'
                onClick: partial(Utils.showModal, "storage_vehicle_new")
                style: {
                  marginLeft: 10,
                }

            #if @state.searchable
            #  SearchBox
            #    onChange: @searchChange
            if UserStore.isPartner() and @state.activeTab == "Tires"
              Button
                color: 'primary'
                label: 'Add Tire Size'
                onClick: @showTireSizes
                style: {
                  marginLeft: 10,
                }
        bottomComponents:
          if @state.activeTab == "Vehicles"
            div
              className: 'bottomComponents'
              <SearchBox
                onChange={@searchChange}
                enablePagination
                store={JobStore}
              />
              div
                className: "filters"
                StorageTypeFilter()
                StorageSiteFilter()


module.exports = StorageIndex
