UsersStore = require('stores/users_store').default
import Consts from 'consts'
BaseComponent = require('components/base_component')

class DriverInput extends BaseComponent
  constructor: ->
    super(arguments...)
    @state = @getInitialState()
  getInitialState: ->
    {}

  UNSAFE_componentWillReceiveProps: (props) ->
    @setState
      rescue_driver_id: props.value
      record: props.record

  componentDidMount: ->
    super(arguments...)

    @setState
      rescue_driver_id: @props.value
      record: @props.record

  componentWillUnmount: ->
    super(arguments...)

  render: ->
    super(arguments...)

    if @props.enabled==false || (@state.record? and @state.record.status? and @state.record.status in [Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION])
      span null,
        UsersStore.get(@state.rescue_driver_id, "full_name", @getId()) || "..loading"
    else

      drivers = UsersStore.getSortedDriverIds(@getId())
      if  @state.rescue_driver_id not in drivers
        drivers.push( @state.rescue_driver_id)

      select
        className: 'form-control'
        name: 'rescue_driver_id'
        value: @state.rescue_driver_id
        onChange: @props.onChange
        option
            value: ''
            className: 'option_header'
            '- Select -'
        for id in drivers
            option
              key: id
              value: id
              className: 'option_row'
              UsersStore.get(id, "full_name", @getId()) || "..loading"



module.exports = DriverInput
