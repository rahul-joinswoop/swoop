import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
StoreSelector = React.createFactory(require('components/storeSelector'))
ServiceStore = require('stores/service_store')
import { partial, sortBy } from 'lodash'

BaseComponent = require('components/base_component')

class EquipmentPickerModal extends BaseComponent

  displayName: 'EquipmentPickerModal'
  constructor: ->
    super(arguments...)
    @state.error = null

  onConfirm: =>
    ids = []
    for id, val of @state
      if !isNaN(parseInt(id)) and val? and val == true
        ids.push(id)
    if ids.length > 0
      @props.onConfirm ids
    else
      @setState
        error: "Choose Equipment"


  handleEquipmentChange: (id, e) =>
    obj = {error: null}
    obj[id] = e.currentTarget.checked
    @setState obj

  getEquipments: () ->
    return sortBy(@props.addons, (addon) -> addon.name.toLowerCase())


  render: ->
    super(arguments...)
    TransitionModal $.extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'EquipmentModalTransition'
      confirm: 'Add'
      title: "Additional Items"
      extraClasses: "equipment_picker_modal"
      onConfirm: @onConfirm
      cancel: 'Cancel'),
      div null,
        div
          className: "header_info"
          "Select what additional items you want to add rates for:"
        for id, type of @getEquipments()

          div
            key: "type_"+type.id
            className: "type_items",
            input
              id: "type_"+type.id
              name: "type_"+type.id
              type: "checkbox"
              checked: @state[type.id]
              onChange: partial(@handleEquipmentChange, type.id)
            label
              htmlFor: "type_"+type.id
              type.name
        label
          className: 'error'
          @state.error


module.exports = EquipmentPickerModal
