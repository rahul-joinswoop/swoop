import React from 'react'
import Consts from 'consts'
Fields = require 'fields/fields'
SiteFields = require 'site_fields'
TransitionModal = React.createFactory require 'components/modals'
UserStore = require('stores/user_store')
Renderer = require 'form_renderer'
BaseComponent = require('components/base_component')
import { bind, extend } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'

branchOrSite = ->
  if UserStore.isEnterprise() then 'Branch' else 'Site'

class NameField extends SiteFields.NameField
  getLabel: -> branchOrSite() + " Name"

class FleetSiteForm extends BaseComponent
  displayName: 'FleetSiteForm'
  getDefaultProps: -> record: {}
  constructor: (props) ->
    super(props)
    siteFields = [
      NameField,
      SiteFields.LocationTypeField,
      SiteFields.LocationField,
      SiteFields.PhoneField,
      SiteFields.SiteCodeField
    ]

    record = props.record || {}
    @form = new Renderer(record, @rerender)
    @form.registerFields(siteFields, true)
    @setState
      showSpinner: false

  showSpinner: (showSpinner) =>
    @setState
      showSpinner: showSpinner

  title: () =>
    if @props.loadedFromJobDetails
      if UserStore.isSwoop()
        "Client Site"
      else
        "Site"
    else
      "Add " + branchOrSite()

  render: ->
    record = @props.record || {}

    children = [
      ReactDOMFactories.p({ key: 'helper-text' }, Consts.SITE.HELPER_TEXT),
      @form.renderAllInputs({disabled: @props.disable, hideLocationType: @props.hideLocationType}) if @props.show
    ]

    TransitionModal(
      extend(
        {},
        @props,
        title: @title()
        enableSend: !@props.loadedFromJobDetails
        extraClasses: 'poi_site_form'
        recordLabel: 'Place'
        showSpinner: @state?.showSpinner
        onConfirm: if @form? then bind(@form.attemptSubmit, @form, @props.onConfirm, @props.onCancel, @showSpinner)
      ),
      children
    )


module.exports = FleetSiteForm
