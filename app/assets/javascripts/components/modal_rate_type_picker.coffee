import createReactClass from 'create-react-class'
import React from 'react'
import Logger from 'lib/swoop/logger'
TransitionModal = React.createFactory(require 'components/modals')
import Consts from 'consts'
StoreSelector = React.createFactory(require('components/storeSelector'))
import { clone, zipObject } from 'lodash'

TypePickerModal = createReactClass(
  displayName: 'TypePickerModal'

  onConfirm: ->
    if @state.type_id != null
      @props.onConfirm
        id: @state.type_id
    else
      @setState
        error: "Choose Type"

  getInitialState: ->
    type_id: null
    type_name: null
    error: null

  handleTypeChange: (id, value) ->
    Logger.debug("picker ",id, value)
    @setState
      type_id: id
      type_name: value
      error: null

  getTypes: () ->
    types = clone(Consts.RATE_TYPE_MAP)

    if @props.exclude?
      for id in @props.exclude
        delete types[id]

    delete types[Consts.STORAGE]

    if UserStore.isPartner()
      if typeof(@props.showTesla) == "undefined" || @props.showTesla == false
        delete types[Consts.TESLA_RATE]
      else
        types

    ret = {}
    for key, type of types
      ret[key] = name: type

    return ret

  UNSAFE_componentWillReceiveProps: (props) ->
    if not props.show
      @setState @getInitialState()

  handleChange: (e) ->
    @setState
      email: e.target.value
      error: null
  render: ->
    TransitionModal $.extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'TypeModalTransition'
      confirm: 'Add'
      title: "Add Rate Type"
      extraClasses: "type_picker_modal"
      onConfirm: @onConfirm
      cancel: 'Cancel'),
      div null,
        div null,
          "Select a new Rate Type to add, then add rates to it."
        div
          className: "select_container"
          StoreSelector
            value: @state.type_name
            onChange: @handleTypeChange
            placeholder: "- Select -"
            populateFunc: @getTypes
        label
          className: 'error'
          @state.error
)


module.exports = TypePickerModal
