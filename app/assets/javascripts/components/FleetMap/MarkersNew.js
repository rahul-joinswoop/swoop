import React, { useEffect, useState } from 'react'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import FleetEta from 'components/fleet_eta'
import JobStore from 'stores/job_store'
import Utils from 'utils'
import UserStore from 'stores/user_store'
import { useGoogleMap, InfoWindow, Polyline } from '@react-google-maps/api'
import Marker from 'components/maps/Marker'
import SiteMarker from './SiteMarker'
import SitePopup from './SitePopup'
import 'stylesheets/components/rmap/job_markers.scss'

class JobInfo extends BaseComponent {
  render() {
    // const job = JobStore.getById(job.id, false)
    const { job } = this.props

    return (
      <div className="job_info_window">
        <div className="status">
          {`#${JobStore.getWatchedId(job.id, this.getId())} | ${job.service}`}
        </div>
        <div>
          <span>{Utils.getTransformedJobStatus(UserStore, job)}</span>
          <FleetEta job_id={job.id} showWrapper />
        </div>
        <div>{job.rescue_company?.name}</div>
      </div>
    )
  }
}

const Markers = ({
  activateJobs, deactivateJobs, dropZoomLevel, isJobActive, marker, siteZoomLevel,
}) => {
  function activateMarker() {
    /* eslint-disable-next-line */
    marker.active = true
    activateJobs(marker.jobs || [marker.job])
  }

  function deactivateMarker() {
    /* eslint-disable-next-line */
    marker.active = false
    deactivateJobs(marker.jobs || [marker.job])
  }

  const map = useGoogleMap()
  const [, setRev] = useState(0)

  useEffect(() => {
    let zoomListener
    if (dropZoomLevel) {
      zoomListener = map.addListener('zoom_changed', () => setRev(rev => rev + 1))
    }

    return () => {
      if (zoomListener) {
        /* eslint-disable-next-line */
        google.maps.event.removeListener(zoomListener)
      }
    }
  }, [dropZoomLevel, map])

  if (marker.type === 'A') {
    const active = marker.active && isJobActive(marker.job)

    return (
      <Marker
        bounceTimes={2}
        icon={{
          url: '/assets/images/marker_a.png',
          labelOrigin: new google.maps.Point(13, 15),
        }}
        key={`marker-${marker.key}`}
        onClick={() => {
          if (active) {
            // deactivate A pin and its job
            deactivateMarker()
          } else if (!active) {
            // deactivate all jobs associated with B pin with their A pins, activate A pin and its job
            if (marker.job.bPin) {
              /* eslint-disable-next-line */
              marker.job.bPin.active = false
              /* eslint-disable-next-line */
              marker.job.bPin.jobs.forEach(job => job.aPin.active = false)
              deactivateJobs(marker.job.bPin.jobs)
            }
            activateMarker()
          }
        }}
        position={marker.job.service_location}
      >
        {active &&
        <InfoWindow onCloseClick={deactivateMarker}>
          <JobInfo job={marker.job} />
        </InfoWindow>}
      </Marker>
    )
  } else if (marker.type === 'B') {
    if (map.getZoom() >= dropZoomLevel) {
      const active = marker.active && marker.jobs.some(isJobActive)

      let infoWindow
      if (active) {
        infoWindow = (
          <InfoWindow onCloseClick={deactivateMarker}>
            <>
              {marker.jobs.map(job => <JobInfo job={job} key={job.id} />)}
            </>
          </InfoWindow>
        )
      } else if (marker.site) {
        infoWindow = <SitePopup map={map} site={marker.site} />
      }

      const showInfoWindow = active || marker.site
      const result = [
        <Marker
          bounceTimes={2}
          icon={{
            url: '/assets/images/marker_b.png',
            labelOrigin: new google.maps.Point(13, 15),
          }}
          key={`marker-${marker.key}`}
          onClick={() => {
            if (active) {
              deactivateMarker()
            } else if (!active) {
              /* eslint-disable-next-line */
              marker.jobs.forEach(job => job.aPin.active = false)
              activateMarker()
            }
          }}
          position={marker.jobs[0].drop_location}
        >
          {showInfoWindow && infoWindow}
        </Marker>,
      ]

      marker.jobs.forEach((job) => {
        if (job.status !== Consts.TOWING) {
          result.push(<Polyline
            key={`directions-${job.id}`}
            options={{
              geodesic: true,
              path: [job.service_location, job.drop_location],
              strokeColor: job.status === Consts.TOWING ? '#68B5DF' : '#A0A0A0',
            }}
          />)
        }
      })

      return result
    } else if (marker.site && map.getZoom() >= siteZoomLevel) {
      return <SiteMarker
        key={`site-${marker.site.id}`}
        showOnZoom={siteZoomLevel}
        site={marker.site}
      />
    }

    return null
  } else if (marker.type === 'Site') {
    return <SiteMarker
      key={`site-${marker.site.id}`}
      showOnZoom={siteZoomLevel}
      site={marker.site}
    />
  }
}

export default Markers
