import React from 'react'
import UserStore from 'stores/user_store'
import JobStore from 'stores/job_store'
import Tabs from 'components/tabs'
import JobListHelper from 'helpers/job_list'
import BaseComponent from 'components/base_component'
import { partial, sortBy, without, pick, assign, includes, pull, concat, values } from 'lodash'
import { map, flow, flatMap } from 'lodash/fp'
import Loading from 'componentLibrary/Loading'
import SiteStore from 'stores/site_store'
import SwoopMap from 'components/maps/SwoopMap'
import Utils from 'utils'
import Markers from './MarkersNew'
import 'stylesheets/components/fleet_map.scss'

/* eslint-disable no-restricted-syntax */
/* eslint-disable no-param-reassign */

const JobCardAddress = props =>
  <div className="job_card_address">
    <span className="letter">
      {props.letter}:
    </span>
    <span className="address">
      {props.address}
    </span>
  </div>

class JobCard extends BaseComponent {
  render() {
    super.render()
    // TODO: move this to a property watcher
    const job = JobStore.getById(this.props.job_id, false, this.getId())
    return (
      <li
        className={`job_card ${this.props.selected ? 'selected' : ''}`}
        onClick={this.props.onClick}
      >
        <div className="top_section">
          <div className="job_info">
            {`#${JobStore.getId(job)} | ${job.service}`}
          </div>
          <JobCardAddress
            address={Utils.shortenLocationAddress(job.service_location)}
            letter="A"
          />
          {job.drop_location?.address &&
            <JobCardAddress
              address={Utils.shortenLocationAddress(job.drop_location)}
              letter="B"
            />}
        </div>
        <div className="bottom_section">
          <div className="status">
            {Utils.getTransformedJobStatus(UserStore, job)}
          </div>
          <div className="tower">
            {job.rescue_company?.name}
          </div>
        </div>
      </li>
    )
  }
}

class FleetMap extends BaseComponent {
  map = null

  constructor(props) {
    super(props)
    this.state.leftBarCollapsed = false
    this.state.activeJobs = []
    this.jobListHelper = new JobListHelper(this.getId())
  }

  getBoundsZoomLevel = (bounds, mapDim) => {
    const latRad = (lat) => {
      const sin = Math.sin((lat * Math.PI) / 180)
      const radX2 = Math.log((1 + sin) / (1 - sin)) / 2
      return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2
    }

    const zoom = (mapPx, worldPx, fraction) =>
      Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2)

    const WORLD_DIM = {
      height: 256,
      width: 256,
    }
    const ZOOM_MAX = 21
    const ne = bounds.getNorthEast()
    const sw = bounds.getSouthWest()
    const latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI
    const lngDiff = ne.lng() - sw.lng()
    const lngFraction = (lngDiff < 0 ? lngDiff + 360 : lngDiff) / 360
    const latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction)
    const lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction)

    return Math.min(latZoom, lngZoom, ZOOM_MAX)
  }

  getMapDimensions() {
    const div = this.map?.getDiv()
    return {
      width: div?.clientWidth,
      height: div?.clientHeight,
    }
  }

  resetBounds = () => {
    if (this.state?.activeJobs?.length > 0) {
      let latlng
      let setAmount = 0
      const bounds = new google.maps.LatLngBounds()
      for (const id of this.state.activeJobs) {
        // Because this is a call and forget function, we don't have to set up watchers
        const job = JobStore.getById(id)
        if (job) {
          latlng = new google.maps.LatLng(job.service_location.lat, job.service_location.lng)
          bounds.extend(latlng)
          setAmount += 1
          if (job.drop_location?.lat) {
            latlng = new google.maps.LatLng(job.drop_location.lat, job.drop_location.lng)
            setAmount += 1
            bounds.extend(latlng)
          }
        }
      }

      if (setAmount > 1) {
        const boundsProps = {}
        if (this.state.leftBarCollapsed) {
          boundsProps.left = 30
        } else {
          boundsProps.left = 330
        }

        const zoom = this.getBoundsZoomLevel(bounds, this.getMapDimensions())
        if (!isNaN(zoom)) {
          boundsProps.zoom = zoom
        }

        this.map?.fitBounds(bounds, boundsProps)

        // Making sure we don't zoom too far in after setting bounds
        setTimeout(() => {
          if (this.map) {
            let zoomOverride = this.map.getZoom()
            if (zoomOverride > 16) {
              zoomOverride = 16

              this.map.setZoom(zoomOverride)
            }
          }
        }, 100)
      } else {
        this.map?.setZoom(13)
        this.map?.panTo(latlng)

        // Truck seems like overkill
        // if job.rescue_vehicle_id
        //  #Because this is a call and forget function, we don't have to set up watchers
        //  vehicle = VehicleStore.getById(job.rescue_vehicle_id)
        //  if vehicle?.lat? and vehicle?.lng?
        //    latlng = (new (google.maps.LatLng)(vehicle.lat, vehicle.lng))
        //    setAmount++
        //    bounds.extend latlng
      }
    }
  }

  onJobClick = (jobId) => {
    let { activeJobs } = this.state
    if (this.isJobActive(jobId)) {
      activeJobs = without(activeJobs, jobId)
    } else {
      activeJobs = [jobId]
      this.jobs[jobId].aPin.active = true
      if (this.jobs[jobId].bPin) {
        this.jobs[jobId].bPin.active = false
      }
    }
    this.setState({ activeJobs })
    setTimeout(this.resetBounds, 100)
  }

  isJobActive = job_id => this.state.activeJobs.includes(job_id)

  updateJobs(tabList) {
    const jobIds = flow(
      flatMap(tab => tab.jobs),
      map('id')
    )(tabList)

    this.jobs = this.jobs || {}
    for (const jobId of jobIds) {
      this.jobs[jobId] = assign(this.jobs[jobId], JobStore.getById(jobId, false, this.getId()))
    }

    this.jobs = pick(this.jobs, jobIds)
  }

  static locationKey(location) {
    return `${location?.lat}-${location?.lng}`
  }

  buildMarkers() {
    // TODO make it derived state
    this.markers = this.markers || {}

    /* eslint-disable-next-line */
    for (const id in this.jobs) {
      // create A pin
      const job = this.jobs[id]
      if (!job.aPin) {
        job.aPin = {
          key: FleetMap.locationKey(job.service_location), type: 'A', job, active: false,
        }
      }

      // update B pin
      FleetMap.updateDropLocationMarker(job, this.markers)
    }

    // TODO implement site updates
    for (let site of SiteStore.getFilteredSites(null, this.getId())) {
      site = SiteStore.getById(site.id, false, this.getId())
      const sitePinKey = FleetMap.locationKey(site.location)
      if (this.markers[sitePinKey]) {
        this.markers[sitePinKey].site = site
      } else {
        this.markers[sitePinKey] = { key: sitePinKey, type: 'Site', site }
      }
    }
  }

  static attachToMarker(job, markers) {
    const bPinKey = FleetMap.locationKey(job.drop_location)
    if (markers[bPinKey]) {
      const marker = markers[bPinKey]
      if (marker.type === 'B') {
        if (!includes(marker.jobs, job)) {
          marker.jobs.push(job)
        }
        marker.active = (job.bPin?.active || marker.active)
        job.bPin = marker
      } else if (marker.type === 'Site') {
        marker.type = 'B'
        marker.active = job.bPin?.active ? job.bPin?.active : false
        marker.jobs = [job]
      }
    } else {
      markers[bPinKey] = {
        key: bPinKey, type: 'B', jobs: [job], active: job.bPin?.active ? job.bPin?.active : false,
      }
      job.bPin = markers[bPinKey]
    }
  }

  static detachFromMarker(job, markers) {
    pull(job.bPin.jobs, job)
    if (!job.bPin.jobs.length) {
      delete markers[job.bPin.key]
    }
  }

  static updateDropLocationMarker(job, markers) {
    if (job.bPin) {
      if (job.drop_location) {
        const bPinKey = FleetMap.locationKey(job.drop_location)
        // if drop location has changed
        if (bPinKey !== job.bPin.key) {
          FleetMap.detachFromMarker(job, markers)
          FleetMap.attachToMarker(job, markers)
        }
      } else {
        FleetMap.detachFromMarker(job, markers)
        delete job.bPin
      }
    } else if (job.drop_location) {
      FleetMap.attachToMarker(job, markers)
    }
  }

  render() {
    super.render()
    let tabList = this.jobListHelper.getTabs()

    this.updateJobs(tabList)

    tabList = map(tab => ({
      id: tab.id,
      name: tab.name,
      component: () => {
        if (!JobStore.jobsLoaded()) {
          return <Loading />
        }
        if (tab.jobs?.length === 0) {
          return (
            <div
              style={{
                textAlign: 'center',
                marginTop: 20,
                color: '#CCC',
              }}
            >
              {`No ${tab.name.toLowerCase()} jobs.`}
            </div>
          )
        }
        return (
          <ul className="job_list">
            {sortBy(tab.jobs, ['id']).reverse().map(job =>
              <JobCard
                job_id={job.id}
                key={job.id}
                onClick={partial(this.onJobClick, job.id)}
                selected={this.isJobActive(job.id)}
              />)}
          </ul>
        )
      },
    }), tabList)

    this.buildMarkers()

    return (
      <div
        className={`fleet-map${this.state.leftBarCollapsed ? ' leftCollapsed' : ''}`}
      >
        <div className="left-side-bar">
          <div className="content">
            <Tabs tabList={tabList} />
          </div>
          <div
            className="exanderContainer"
            onClick={() => this.setState(state => ({ leftBarCollapsed: !state.leftBarCollapsed }))}
          >
            <div className="expander" />
          </div>
        </div>
        <SwoopMap
          onLoad={(swoopMap) => {
            this.map = swoopMap
            swoopMap.setOptions({
              center: { lat: 41, lng: -110 },
              zoom: 4,
            })
          }}
        >
          {map(marker => <Markers
            activateJobs={jobs =>
              this.setState(state => ({ activeJobs: state.activeJobs.concat(map('id', jobs)) }))}
            deactivateJobs={jobs =>
              this.setState(state => ({ activeJobs: without(state.activeJobs, ...map('id', jobs)) }))}
            dropZoomLevel={8}
            isJobActive={job => this.isJobActive(job.id)}
            key={`${marker.key}-${marker.job?.id}`}
            marker={marker}
            siteZoomLevel={8}
          />, concat(values(this.markers), map('aPin', this.jobs)))}
        </SwoopMap>
      </div>
    )
  }
}


export default FleetMap
