import { useEffect, useState } from 'react'
import { useGoogleMap } from '@react-google-maps/api'

const SitePopup = ({ anchor, site }) => {
  const map = useGoogleMap()
  const [infoWindow] = useState(() => new google.maps.InfoWindow())

  useEffect(() => {
    /* eslint-disable-next-line */
    const mouseover = anchor.addListener('mouseover', () => infoWindow.open(map, anchor))
    const mouseout = anchor.addListener('mouseout', () => infoWindow.close())

    return () => {
      mouseover.remove()
      mouseout.remove()

      infoWindow.close()
      infoWindow.setMap(null)
    }
  }, [anchor, infoWindow, map])

  useEffect(() => infoWindow.setContent(site.name), [infoWindow, site.name])

  return null
}

export default SitePopup
