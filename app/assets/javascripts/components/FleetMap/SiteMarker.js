import React, { useCallback, useEffect, useState } from 'react'
import { useGoogleMap, InfoWindow } from '@react-google-maps/api'

const SiteMarker = ({ site, showOnZoom = 0 }) => {
  const map = useGoogleMap()
  const [showInfoWindow, setShowInfoWindow] = useState(false)
  const [marker] = useState(() => new google.maps.Marker({
    icon: { url: '/assets/images/marker_blue_site.png' },
    map,
  }))

  useEffect(() => {
    marker.addListener('mouseover', () => setShowInfoWindow(true))
    marker.addListener('mouseout', () => setShowInfoWindow(false))

    return () => marker.setMap(null)
  }, [marker])

  const updateVisibility = useCallback(() => marker.setVisible(map.getZoom() >= showOnZoom), [map, marker, showOnZoom])

  // updateZoomListener
  useEffect(() => {
    const zoomListener = showOnZoom ?
      map.addListener('zoom_changed', updateVisibility) :
      null

    return () => zoomListener?.remove()
  }, [map, showOnZoom, updateVisibility])

  updateVisibility()

  useEffect(() => marker.setPosition(site.location), [marker, site.location])

  return showInfoWindow ?
    <InfoWindow anchor={marker}><>{site.name}</></InfoWindow> :
    null
}

export default SiteMarker
