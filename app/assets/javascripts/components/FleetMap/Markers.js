import React, { Component, useEffect, useState } from 'react'
import Marker from 'components/rmap/marker'
import InfoWindow from 'components/rmap/info_window'
import MapStore from 'stores/map_store'
import JobStore from 'stores/job_store'
import FleetEta from 'components/fleet_eta'
import Utils from 'utils'
import UserStore from 'stores/user_store'
import SiteMarker from 'components/rmap/SiteMarker'
import Directions from 'components/rmap/directions'
import Consts from 'consts'
import BaseComponent from 'components/base_component'
import 'stylesheets/components/rmap/job_markers.scss'

class JobInfo extends BaseComponent {
  render() {
    // const job = JobStore.getById(job.id, false)
    const { job } = this.props

    return (
      <div className="job_info_window">
        <div className="status">
          {`#${JobStore.getWatchedId(job.id, this.getId())} | ${job.service}`}
        </div>
        <div>
          <span>{Utils.getTransformedJobStatus(UserStore, job)}</span>
          <FleetEta job_id={job.id} showWrapper />
        </div>
        <div>{job.rescue_company?.name}</div>
      </div>
    )
  }
}

class SitePopup extends Component {
  constructor(props) {
    super(props)
    this.info = MapStore.createInfoWindow()
  }

  componentDidMount() {
    /* eslint-disable-next-line */
    this.mouseover = this.props.marker.addListener('mouseover', () => this.info.open(this.props.map, this.props.marker))
    this.mouseout = this.props.marker.addListener('mouseout', () => this.info.close())
  }

  componentWillUnmount() {
    /* eslint-disable-next-line */
    google.maps.event.removeListener(this.mouseover)
    /* eslint-disable-next-line */
    google.maps.event.removeListener(this.mouseout)

    this.info.close()
    this.info.setMap(null)
  }

  render() {
    this.info.setContent(this.props.site.name)
    return null
  }
}

function Markers({
  activateJobs, deactivateJobs, dropZoomLevel, isJobActive, map, marker, siteZoomLevel,
}) {
  function activateMarker() {
    /* eslint-disable-next-line */
    marker.active = true
    activateJobs(marker.jobs || [marker.job])
  }

  function deactivateMarker() {
    /* eslint-disable-next-line */
    marker.active = false
    deactivateJobs(marker.jobs || [marker.job])
  }

  const [, setRev] = useState(0)

  useEffect(() => {
    let zoomListener
    if (dropZoomLevel) {
      zoomListener = map.addListener('zoom_changed', () => setRev(rev => rev + 1))
    }

    return () => {
      if (zoomListener) {
        /* eslint-disable-next-line */
        google.maps.event.removeListener(zoomListener)
      }
    }
  }, [dropZoomLevel, map])

  if (marker.type === 'A') {
    const active = marker.active && isJobActive(marker.job)

    return (
      <Marker
        bounceTimes={2}
        forceShowInfoWindow={active}
        image="/assets/images/marker_a.png"
        key={`marker-${marker.key}`}
        lat={marker.job.service_location.lat}
        lng={marker.job.service_location.lng}
        map={map}
        onClick={() => {
          if (active) {
            // deactivate A pin and its job
            deactivateMarker()
          } else if (!active) {
            // deactivate all jobs associated with B pin with their A pins, activate A pin and its job
            if (marker.job.bPin) {
              /* eslint-disable-next-line */
              marker.job.bPin.active = false
              /* eslint-disable-next-line */
              marker.job.bPin.jobs.forEach(job => job.aPin.active = false)
              deactivateJobs(marker.job.bPin.jobs)
            }
            activateMarker()
          }
        }}

        onInfoWindowClickClose={deactivateMarker}
      >
        <InfoWindow getContent={() => <JobInfo job={marker.job} />} />
      </Marker>
    )
  } else if (marker.type === 'B') {
    if (map.getZoom() >= dropZoomLevel) {
      const active = marker.active && marker.jobs.some(isJobActive)

      let infoWindow
      if (active) {
        infoWindow = <InfoWindow
          getContent={() =>
            marker.jobs.map(job => <JobInfo job={job} key={job.id} />)}
        />
      } else if (marker.site) {
        infoWindow = <SitePopup site={marker.site} />
      }

      const result = [
        <Marker
          bounceTimes={2}
          forceShowInfoWindow={active || marker.site}
          image="/assets/images/marker_b.png"
          key={`marker-${marker.key}`}
          lat={marker.jobs[0].drop_location.lat}
          lng={marker.jobs[0].drop_location.lng}
          map={map}
          onClick={() => {
            if (active) {
              deactivateMarker()
            } else if (!active) {
              /* eslint-disable-next-line */
              marker.jobs.forEach(job => job.aPin.active = false)
              activateMarker()
            }
          }}

          onInfoWindowClickClose={deactivateMarker}
        >
          {infoWindow}
        </Marker>,
      ]

      marker.jobs.forEach((job) => {
        if (job.status !== Consts.TOWING) {
          result.push(<Directions
            color={job.status === Consts.TOWING ? '#68B5DF' : '#A0A0A0'}
            end_lat={job.drop_location.lat}
            end_lng={job.drop_location.lng}
            key={`directions-${job.id}`}
            map={map}
            start_lat={job.service_location.lat}
            start_lng={job.service_location.lng}
          />)
        }
      })

      return result
    } else if (marker.site && map.getZoom() >= siteZoomLevel) {
      return <SiteMarker
        key={`site-${marker.site.id}`}
        map={map}
        showOnZoom={siteZoomLevel}
        site={marker.site}
      />
    }

    return null
  } else if (marker.type === 'Site') {
    return <SiteMarker
      key={`site-${marker.site.id}`}
      map={map}
      showOnZoom={siteZoomLevel}
      site={marker.site}
    />
  }
}

export default Markers
