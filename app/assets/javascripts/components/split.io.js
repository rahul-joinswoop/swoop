import React, { useEffect, useState } from 'react'
import Consts from 'consts'
import { SplitFactory } from '@splitsoftware/splitio'

// A master list of all splits should be available via our context
const splits = Object.values(Consts.SPLITS)

const SplitIOContext = React.createContext()

// To apply specific treatments based on custom attributes. Include values in the attributes below.
// Additionally, on split.io, you must setup targeting rules based on those attributes and
// allocate traffic to the split. If no traffic is allocated, the Default Treatment will be applied.
// If traffic is allocated and your rules do not match, the Default Rule will be applied.
const parseTreatments = (client, attributes) => {
  const gottenTreatments = client.getTreatments(splits, attributes)
  const parsedTreatments = {}
  Object.keys(gottenTreatments).forEach((key) => {
    if (gottenTreatments[key] === 'on') {
      parsedTreatments[key] = true
    } else
    if (gottenTreatments[key] === 'off') {
      parsedTreatments[key] = false
    }
  })
  return parsedTreatments
}

const SplitIOProvider = ({ id, children, attributes }) => {
  const [treatments, setTreatments] = useState(null)

  useEffect(() => {
    const factory = SplitFactory({
      core: {
        authorizationKey: Consts.SPLIT_KEY,
        key: id,
      },
      startup: {
        readyTimeout: 10, // 1.5 sec
      },
    })
    const client = factory.client()
    client.on(client.Event.SDK_READY, () => {
      setTreatments(parseTreatments(client, attributes))
    })
    client.on(client.Event.SDK_UPDATE, () => {
      setTreatments(parseTreatments(client, attributes))
    })
    return client.destroy
  }, [id, attributes])

  return (
    <SplitIOContext.Provider value={{ treatments }}>
      {children}
    </SplitIOContext.Provider>
  )
}

const withSplitIO = Component => class Consumer extends React.PureComponent {
  render() {
    return (
      <SplitIOContext.Consumer>
        {({ treatments }) => <Component treatments={treatments} {...this.props} />}
      </SplitIOContext.Consumer>
    )
  }
}

export { SplitIOContext, SplitIOProvider, withSplitIO }
