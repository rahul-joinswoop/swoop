import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
FeatureStore = require('stores/feature_store')
ServiceStore = require ('stores/service_store')
StoreSelector = React.createFactory(require('components/storeSelector'))
BaseComponent = require('components/base_component')
import { filter, partial, sortBy } from 'lodash'

class ServicePickerModal extends BaseComponent
  displayName: 'ServicePickerModal'
  constructor: ->
    super(arguments...)
    @state =
      service_id: null
      service_name: null
      error: null
      copy: true
  onConfirm: =>
    ids = []
    for id, val of @state
      if !isNaN(parseInt(id)) and val? and val == true
        ids.push(id)
    if ids.length > 0
      @props.onConfirm {
        ids: ids
        copy: @state.copy
      }
    else
      @setState
        error: "Choose Service"

  handleServiceChange: (id, e) =>
    obj = {error: null}
    obj[id] = e.currentTarget.checked
    @setState obj

  getServices: () =>
    services = @props.services
    myServices = ServiceStore.getServices(@getId())
    if !services?
      services = {}
    for id, obj of myServices
      services[id] = obj

    if @props.exclude?
      services =  filter(services, (service) =>
        if not FeatureStore.isFeatureEnabled("Storage") and service.name == "Storage"
          return false

        return service.id not in @props.exclude
      )
    services = sortBy(services, "name")
    return services

  render: ->
    super(arguments...)
    TransitionModal $.extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'ServiceModalTransition'
      confirm: 'Add'
      title: "Add Services"
      extraClasses: "service_picker_modal"
      onConfirm: @onConfirm
      cancel: 'Cancel'),
      div null,
        div
          className: "header_info"
          "Select what Services you want to add rates for:"
        for id, type of @getServices()
          div
            key: "type_"+type.id
            className: "type_items",
            input
              id: "type_"+type.id
              name: "type_"+type.id
              type: "checkbox"
              checked: @state[type.id] == true
              onChange: partial(@handleServiceChange, type.id)
            label
              htmlFor: "type_"+type.id
              type.name
        div
          className: "type_items copy"
          input
            id: "copy"
            name: "copy"
            type: "checkbox"
            checked: @state["copy"]
            onChange: partial(@handleServiceChange, "copy")
          label
            htmlFor: "copy"
            "Copy over existing rates from Default Service when adding new Services."
        label
          className: 'error'
          @state.error



module.exports = ServicePickerModal
