import React from 'react'
import Logger from 'lib/swoop/logger'
import Consts from 'consts'
DownloadStore = require('stores/download_store')
ReportStore = require('stores/report_store')
FeatureStore = require('stores/feature_store')
UserStore = require('stores/user_store')
UserFormModal = React.createFactory(require('components/modals/UserForm').default)
TransitionModal = React.createFactory(require 'components/modals')
Filters = React.createFactory(require 'components/filters')
import TabsImport from 'components/tabs'
Tabs = React.createFactory(TabsImport)
LookerFrame = require('components/reports/looker_frame')
BaseComponent = require('components/base_component')
import { clone, groupBy, kebabCase, partial, reduce, sortBy, values, without } from 'lodash'
import moment from 'moment-timezone'
import ReactDOMFactories from 'react-dom-factories'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

ACCOUNTS = 1
PARTNERS = 2
USERS = 3
TRUCKS = 4
COMPANIES = 5
WEBVERSIONS = 6
ANDROIDVERSIONS = 7
DISPATCHERS = 8
FEATURES = 9
RATES = 10
ICON_COL_WIDTH = 40
SMALL_WIDTH = 30

FTable = React.createFactory(require 'components/resizableTable/FTable')
StoreSelector = React.createFactory(require('components/storeSelector'))


class Reporting extends BaseComponent
  displayName: 'Reporting'
  constructor: ->
    super(arguments...)
    @state = {
      inited: false
      rev: 0
      open: []
      showServiceFilter: UserStore.isPartner()
      showDriverFilter: UserStore.isPartner()
      showTruckFilter: UserStore.isPartner()
      filters:
        after: moment(moment().format("MM/DD/YYYY")+ " 12:00 AM " + moment().format("z")).toISOString()
    }

    @register(DownloadStore, DownloadStore.CHANGED_REPORT_RESULT)
    @register(UserStore, UserStore.LOGIN)

  componentDidMount: ->
    super.componentDidMount()
    if UserStore.getUser()?
      @loadReports()

  loadReports: () ->
    vals =  values(ReportStore.loadAndGetAll())
    if vals?.length > 0
      @setState
        open: [vals[0].id]
        inited: true

  rerender: =>
    super(arguments...)

    if @state.inited == false
      @loadReports()

  downloadHTML: (id, e) =>
    @download(id, 'html', e)

  downloadPdf: (id, e) =>
    @download(id, 'pdf', e)

  downloadCsv: (id, e) =>
    @download(id, 'csv', e)

  download: (id, format, e) =>
    Tracking.track('Report Download', {
      category: 'Reporting'
    })
    DownloadStore.downloadReport(id, @state.filters[id], format)


  filterList: (id, new_filters) =>
    filters = @state.filters
    filters[id] = new_filters
    @setState
      filters: filters

  hideFilters: =>
    @setState
      showServiceFilter: false
      showDriverFilter: false
      showTruckFilter: false
      showBetweenDates: false
      showDate: false

  toggleRow: (row_id) =>
    open = clone(@state.open)
    if !open?
      open = []
    if row_id in open
      open = without(open, row_id)
    else
      open.push(row_id)

    @setState
      open: open

  setRevenue: (revenue) =>
    @setState
      revenue: revenue
  sortedReports: =>

    #TODO: should cache this
    reports = values(ReportStore.loadAndGetAll(@getId()))
    reports = groupBy(reports, (r) -> return r.display_order != null )
    ordered = sortBy(reports.true, "display_order")
    sorted = sortBy(reports.false, "title")
    ordered.concat(sorted)

  concertina: =>
    reports = @sortedReports()
    div
      id: "reporting_container"
      style:
        margin: 15
      ul
        style:
          padding: 0
          maxWidth: 1110
          paddingTop: 15
          margin: "0 auto"
        for report in reports

          open = report.id in @state?.open
          li
            className: kebabCase(report.title)
            key: kebabCase(report.title)
            style:
              border: "1px solid #CCCCCC"
              marginBottom: 7
              borderRadius: 3

            div
              className: "report_header"
              onClick: partial(@toggleRow, report.id)
              style:
                height: 36
              h3
                className: if open then "open" else ""
                style:
                  fontFamily: "Open Sans"
                  fontWeight: 700
                  fontSize: 13
                  padding: 10
                report.title
              ReactDOMFactories.i
                className: "fa " + if open then "fa-chevron-up" else "fa-chevron-down"
            if open

              filters = reduce(report.filters, (obj, val, key) ->
                if val.type == "start_date"
                  for index, filter of report.filters
                    if filter.type == "end_date"
                      obj.push({
                        keys: ["start_date", "end_date"]
                        type: "dates"
                        initialFrom: if val.default? then moment(val.default)
                        initialTo: if filter.default? then moment(filter.default)
                      })
                      break
                else if val.type == "end_date"

                else if val.type == "date"
                  obj.push({
                    keys: ["start_"+key, "end_"+key]
                    type: val.type
                  })
                else
                  val.key = key
                  obj.push(val)
                return obj
              , [])

              state = DownloadStore.getLastReportState(report.id)

              Logger.debug("REPORTS last state: ", report.id, state)
              div
                style:
                  borderTop: "1px solid #CCCCCC"
                  padding: 15
                Filters
                  filters: filters
                  filterList: partial(@filterList, report.id)
                if state in ["Loading", "Created", "Running"]
                  Loading
                    display: 'inline-block'
                    message: 'Preparing report ...'
                    padding: 0
                    size: 24
                else
                  div null,
                    div
                      style:
                        clear: "both"
                        display: "block"
                        borderRadius: 3
                    Button
                      color: "green-o"
                      onClick: partial(@downloadCsv, report.id)
                      size: "narrow"
                      style: {
                        margin: '0 5px 0 0'
                      }
                      "Download CSV"
                    if report.pdf
                      span null,
                        Button
                          color: "green-o"
                          onClick: partial(@downloadPdf, report.id)
                          size: "narrow"
                          style: {
                            margin: '0 5px 0 0'
                          }
                          "Download PDF"
                if state in ["error", "Failed", "Empty"]
                  span
                    style:
                      color: "red"
                      margin: 5
                    if state == "error"
                      "Problem Running Report"
                    if state == "Failed"
                      "Report Failed"
                    if state == "Empty"
                      "No Results"



  render: ->
    super(arguments...)
    tabList = []

    if (UserStore.isAdmin() or UserStore.isRoot()) and FeatureStore.isFeatureEnabled(Consts.FEATURE_LOOKER, @getId()) and UserStore.hasRole("looker")
      tabList.push({
        name: "Insights"
        component: =>
          React.createElement(LookerFrame)
      })

    tabList.push({
      name: "Reports"
      component: =>
        if ReportStore.isAllLoaded(@getId())
          @concertina()
    })

    div
      className: 'content-wrapper'
      style:
        padding: '30px'
      Tabs
        changeTab: @changeTab
        tabList: tabList

module.exports = Reporting
