import React from 'react'
import Consts from 'consts'
EditForm = React.createFactory(require('components/edit_form'))
UserStore = require('stores/user_store')
CompanyStore = require('stores/company_store').default
SiteStore = require('stores/site_store').default
PoiStore = require('stores/poi_store').default
import FeatureStore from 'stores/feature_store'
import TimeSelectorImport from 'components/time_pickers/time_selector.js'
TimeSelector = React.createFactory(TimeSelectorImport)
BaseComponent = require('components/base_component')
#TODO: if a change comes in, show an error that something has changed and to refresh
import { extend, get, values } from 'lodash'
import Utils from 'utils'
import moment from 'moment-timezone'

class SiteForm extends BaseComponent
  displayName: 'SiteForm'

  getPhoneField: ->
    if FeatureStore.isFeatureEnabled(Consts.FEATURE_INTERNATIONAL_PHONE_NUMBERS)
      classes: "right"
      isValid: (col) ->
        phone = @getValue(@record, col.name)
        if phone and not /^\+\d{11}$/.test(phone)
          'Invalid phone number'
      label: "Phone"
      name: "phone"
      pattern: "^(?:\\+\\d{0,11})?$"
      placeholder: '+XXXXXXXXXXXX'
      realTimePattern: true
      required: true
      type: 'intlPhone'
    else
      name: "phone"
      label: "Phone"
      type: "phone"
      classes: "right"
      required: true

  render: ->
    super(arguments...)

    data = [
      {
        name: "name"
        label: "Site Name"
        type: "custom"
        containerStyle:
          marginTop: "10px"
        classes: "col"
        isValid: (col) ->
          value = @getValue(@record, col.name)?.toLowerCase()
          valid = true
          count = 0
          if not value? or value == ""
            return span
              style:
                marginLeft: 130
              "Site Name Required"
          # this happens only in the validation step, so there's no need to pass the component id.
          # TODO perhaps do this validation on BE?
          for site_id in SiteStore.getAllIds()
            siteName = SiteStore.get(site_id, 'name')

            if siteName?.toLowerCase() == value and @record.id != site_id
              return span
                style:
                  marginLeft: 130
                "A Site with this name already exists"

          # this happens only in the validation step, so there's no need to pass the component id.
          # TODO perhaps do this validation on BE?
          for place_id in PoiStore.getAllIds()
            if PoiStore.get(place_id, 'name').toLowerCase() == value
              return span
                style:
                  marginLeft: 130
                "A Place with this name already exists"

          return
        getView: (site) ->
          div
            className: "site_name"
            span
              className: "nickname"
              "Add a nickname to your company name to identify this site."
            #Leave this as it is if there is only one company
            if values(CompanyStore.getSubCompaniesIds(@props.componentId))?.length > 1
              select
                style:
                  width: "150px"
                  display: "inline-block"
                  verticalAlign: "top"
                className: "form-control"
                onChange: (e) =>
                  @setrecordData("owner_company_id", e.target.value)
                value: if site.owner_company_id? then site.owner_company_id else site.company_id
                for companyId in values(CompanyStore.getSubCompaniesIds(@props.componentId))
                  option
                    key: companyId
                    value: companyId
                    CompanyStore.get(companyId, 'name')
            else
              span
                className: "company_name"
                if CompanyStore._loadingSubCompanyIds
                  'loading companies...'
                else
                  UserStore.getUser()?.company?.name
            input
              type: "text"
              className: "form-control"
              style:
                width: "150px"
                display: "inline-block"
                verticalAlign: "top"
                marginLeft: "10px"
              value: site.name
              name: "name"
              onChange: (e) =>
                @handleChange(e)
        required: true

      }
      {
        type: "clear"
        classes: "col"
      }
      {
        name: "location"
        label: "Address"
        type: "address"
        classes: "site_address"
        required: true
        containerStyle:
          marginBottom: "20px !important"
      }
      @getPhoneField()
      {
        name: "dispatchable"
        isValid: ->
          dispatchable_count = 0

          all_account_companies = []
          if !@record.dispatchable
            # create a list of all the other non-motor club companies that are currently being served on other sites
            #
            # this happens only in the validation step, so there's no need to pass the component id.
            # TODO perhaps do this validation on BE?
            for site_id in SiteStore.getAllIds()
              if site_id == @record.id || !SiteStore.get(site_id, 'dispatchable')?
                continue

              providerCompanies = SiteStore.get(site_id, 'providerCompanies')

              if providerCompanies?
                for company in providerCompanies
                  if company.name == Consts.SWOOP || company.in_house
                    all_account_companies.push(company.id)

            #Go through the current sites companies as check if this site is the only one servicing a non-motor club
            if @record?.providerCompanies?
              for company in @record.providerCompanies
                if (company.name == Consts.SWOOP || company.in_house) and all_account_companies.indexOf(company.id) == -1
                  return Consts.MESSAGE_NEED_ONE_DISPATCH_SITE({
                    site_name: @record.name
                    company_name: company.name
                  })

        label: "Dispatch"
        type: "checkbox"
        classes: "col"
        containerStyle:
          borderTop: "1px solid #E2E2E2"
          paddingTop: "20px"
          marginTop: "20px"
          direction: "ltr"
      }

      {
        name: "dispatchable_info"
        type: "span"
        classes: "col subtitle"
        label: "Enable this site to receive jobs digitally from allowed accounts and be used for generating invoices."
      }

       {
         name: ""
         type: "custom"
         classes: "col"
         show: (site) -> site.dispatchable and site?.providerCompanies? and site?.providerCompanies?.length > 0
         getView: (site) ->
           div
             className: "dispatchable_info"
             label
               className: "control-label"
               "Allowed Accounts"
             span
               className: "nickname"
               "The following accounts are allowed to send jobs digitally to this site. Edit within Accounts in Settings."
             if site?.providerCompanies?
               div
                 className: "provider_companies"
                 for account in site.providerCompanies
                   span
                     className: "account_name"
                     account.name
       }
            # }
      {
         name: "tire_program"
         show: (site) =>
          if !site.dispatchable
            return false
          if site?.providerCompanies?
            for c in site.providerCompanies
              if c.name == "Tesla"
                return true
          return false
         label: if UserStore.isFleet() then "Loaner Wheel Program" else "Tesla Tires"
         type: "checkbox"
         classes: "col"
      }
      {
        name: "tesla_tire_info"
        show: (site) =>
          if !site.dispatchable
            return false
          if site?.providerCompanies?
            for c in site.providerCompanies
              if c.name == "Tesla"
                return true
        type: "span"
        classes: "subtitle"
        label: "Tesla tires are stored at this site."
      }

      {
          name: "always_open"
          label: "Hours to Receive Jobs Digitally"
          type: "selector"
          classes: "col"
          containerStyle:
            borderTop: "1px solid #E2E2E2"
            paddingTop: "20px"
            marginTop: "10px"
            marginBottom: "20px"
          style:
            width: "150px"
            display: "inline-block"
          show: (site) ->
            site.dispatchable
          getValue: (site) ->
            if site.always_open
              return "24/7"
            else
              return "Office Hours"
          onChange: (e) ->
            @setrecordData("always_open", e.target.value == "24/7")
          options: ((record) =>
            return ["24/7", "Office Hours"]
          )
      }

      {
        name: "work_hours"
        type: "custom"
        classes: "col work_hours"
        show: (site) ->
          site.dispatchable and !site.always_open
        beforeSend: (site, col) ->
          for prop in ["open_time", "close_time"]
            m = moment(moment().format("MM/DD/YYYY ") + get(site, prop) + moment().format(" z"))
            if m.isValid()
              Utils.setValue(site, prop, m.format('HH:mm'))

            delete site["times"]
            delete site["times_sat"]
            delete site["times_sun"]

        getView: (site) ->
          div
            className: "work_hours"
            for time in [{name: "Monday - Friday:", postfix: ""}, {name: "Saturday:", postfix: "_sat"}, {name: "Sunday", postfix: "_sun"}]
              openTime = moment(moment().format('YYYY-MM-DD 09:00'))
              if site["open_time"+time.postfix]?
                openTime = moment(moment().format("YYYY-MM-DD #{site["open_time"+time.postfix]}"))

              closeTime = moment(moment().format('YYYY-MM-DD 17:00'))
              if site["close_time"+time.postfix]?
                closeTime = moment(moment().format("YYYY-MM-DD #{site["close_time"+time.postfix]}"))

              show_times = (site["open_time"+time.postfix]? and site["close_time"+time.postfix]?) || site["times"+time.postfix]
              div
                className: "dt_selector"
                style:
                  marginBottom: "5px"
                  height: "35px"
                input
                  type: "checkbox"
                  checked: show_times
                  onChange: ((time) => ((e) =>
                    @setrecordData("times"+time.postfix, e.target.checked)
                    if !e.target.checked
                      @setrecordData("open_time"+time.postfix, null)
                      @setrecordData("close_time"+time.postfix, null)
                    else
                      @setrecordData("open_time"+time.postfix, "9:00")
                      @setrecordData("close_time"+time.postfix, "17:00")


                  ))(time)
                span
                  className: "weekdays"
                  style:
                    verticalAlign: "middle"
                    lineHeight: "35px"
                    marginLeft: "5px"
                    width: "130px"
                    display: "inline-block"

                  time.name
                if show_times
                  div
                    style:
                      display: "inline"
                    TimeSelector
                      value: openTime
                      onChange: ((time) => ((str) =>
                        if not str?
                          return
                        @setrecordData("open_time"+time.postfix, moment(str).format("H:mm"))
                      ))(time)
                      placeholder: "Time"
                      className: "inside-edit-site"

                    span null,
                      "-"
                    TimeSelector
                      value: closeTime
                      onChange: ((time) => ((str) =>
                        if not str?
                          return
                        @setrecordData("close_time"+time.postfix, moment(str).format("H:mm"))
                      ))(time)
                      placeholder: "Time"
                      className: "inside-edit-site"
                    span null,
                      if site.tz? then site.tz else ""
      }
    ]

    title = "Add Site"
    submitButtonName = "Add"

    if @props.record? and @props.record.id?
      title = "Edit Site"
      submitButtonName = "Save"

    # set default phone value
    record = { phone: '', ...@props.record }

    EditForm extend({}, @props,
      cols: [{fields:data}]
      title: title
      showCancel: true
      extraClasses: "site_form"
      submitButtonName: submitButtonName
      componentId: @getId()
      record: record
    )

module.exports = SiteForm
