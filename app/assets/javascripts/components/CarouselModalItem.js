import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Loading from 'componentLibrary/Loading'
import Utils from 'utils'
import 'stylesheets/components/CarouselModalItem.scss'

class CarouselModalItem extends PureComponent {
  static propTypes = {
    photoItem: PropTypes.object,
    width: PropTypes.number,
  }

  constructor(props) {
    super(props)
    this.state = {
      isLoaded: false,
    }
  }

  handleOnLoad = () => {
    this.setState({ isLoaded: true })
  }

  render() {
    return (
      <div className="image-wrapper">
        <div
          className="image-container"
          style={{
            width: this.props.width,
          }}
        >
          <img
            alt={this.props.photoItem.created_at}
            className={`prevent-modal-close ${this.state.isLoaded ? null : 'hidden'}`}
            onLoad={this.handleOnLoad}
            src={Utils.carouselImgURL(this.props.photoItem.url, null, 562, true)}
            srcSet={Utils.carouselImgURL(this.props.photoItem.url, null, 1124, true, true)}
          />
          {!this.state.isLoaded && <Loading color="white" padding={40} size={100} />}
        </div>
        <span className="date-time prevent-modal-close">
          { Utils.formatDateTime(this.props.photoItem?.created_at, true) }
          { this.props.photoItem?.location && (
            <span className="maps-link prevent-modal-close">&nbsp; &middot; &nbsp;
              <a
                className="prevent-modal-close"
                href={`https://www.google.com/maps/search/?api=1&query=${this.props.photoItem?.location.lat},${this.props.photoItem?.location.lng}`}
                rel="noopener noreferrer"
                target="_blank"
              >
                View Photo Location
              </a>
            </span>
          )}
          <span className="full-image-link prevent-modal-close">&nbsp; &middot; &nbsp;
            <a
              className="prevent-modal-close"
              href={this.props.photoItem.url}
              rel="noopener noreferrer"
              target="_blank"
            >
              View Full-size
            </a>
          </span>
        </span>
      </div>
    )
  }
}

export default CarouselModalItem
