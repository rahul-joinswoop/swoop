import React from 'react'
import $ from 'jquery'
import Consts from 'consts'
import Dispatcher from 'lib/swoop/dispatcher'
import UserStore from 'stores/user_store'
import UserMock from 'components/login/user_mock'

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      error: false,
      userInput: '',
      userNotFound: false,
    }
  }

  componentDidMount() {
    UserStore.bind(UserStore.USER_NOT_FOUND, this.userNotFound)
    UserStore.bind(UserStore.PASSWORD_REQUEST_FAIL, this.passwordRequestFail)
    UserStore.bind(UserStore.LOGIN, this.handleLogin)
  }

  componentWillUnmount() {
    UserStore.unbind(UserStore.USER_NOT_FOUND, this.userNotFound)
    UserStore.unbind(UserStore.PASSWORD_REQUEST_FAIL, this.passwordRequestFail)
    UserStore.unbind(UserStore.LOGIN, this.handleLogin)
  }

  passwordRequestFail() {
    this.setState({
      error: true,
      userNotFound: false,
    })
  }

  newPasswordRequest() {
    Dispatcher.send(UserStore.NEW_PASSWORD_REQUEST, {
      userInput: $('#user_input').val(),
    })
  }

  handleBottomAction() {
    Dispatcher.send(UserStore.LOGIN, {})
  }

  fields() {
    const error = this.state.error && (
      <p className="password_request-alert-message">
        {Consts.PASSWORD_REQUEST.FAIL}
      </p>
    )

    let passwordMessage

    if (this.state.userNotFound) {
      const type = this.state.userInput?.indexOf('@') >= 0 ?
        Consts.PASSWORD_REQUEST.EMAIL_ADDRESS :
        Consts.PASSWORD_REQUEST.USERNAME

      passwordMessage = (
        <div>
          <p className="password_request-alert-message">
            {Consts.PASSWORD_REQUEST.EMAIL_OR_USERNAME_NOT_RECOGNIZED(type)}
          </p>
          <p className="password_request-alert-message">
            {Consts.PASSWORD_REQUEST.HELP}
          </p>
        </div>
      )
    }

    const userNotFound = !this.state.userNotFound && (
      <div>
        <p>
          {Consts.PASSWORD_REQUEST.ENTER_USERNAME_OR_PASSWORD}
        </p>
      </div>
    )

    return (
      <div>
        {error}
        {passwordMessage}
        {userNotFound}
        <div className="field password_request-field">
          <label className="label-reg" htmlFor="user_input">
            Username or Email:
          </label>
          <input
            className="form-control"
            id="user_input"
            name="user[input]"
            onChange={this.handleChange}
            type="text"
            value={this.state.userInput}
          />
        </div>
      </div>
    )
  }

  handleChange = (e) => {
    this.setState({
      error: false,
      userInput: e.target.value,
      userNotFound: false,
    })
  }

  handleLogin = () => {
    if (!this.state.userInput) {
      const user = UserStore.getUser()
      if (user) {
        this.setState({
          userInput: user.email,
        })
      }
    }
  }

  userNotFound() {
    this.setState({
      error: false,
      userNotFound: true,
    })
  }

  render() {
    return (
      <UserMock
        bottomActionValue="Back to Login"
        errorMessage={this.errorMessage}
        fields={this.fields()}
        handleBottomAction={this.handleBottomAction}
        handleSubmit={this.newPasswordRequest}
        redirect={false}
        showBottomLink
        submitValue="Send Link"
        title="FORGOT PASSWORD"
      />
    )
  }
}

export default ForgotPassword
