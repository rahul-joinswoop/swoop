import React, { useEffect, useState } from 'react'
import { Link } from 'react-router5'
import jquery from 'jquery'
import Dispatcher from 'lib/swoop/dispatcher'
import UserStore from 'stores/user_store'
import UserMock from './user_mock'

function handleInput(setter) {
  return e => setter(e.target.value)
}

function Fields({ app, username: [username, setUsername], password: [password, setPassword] }) {
  return [
    <div className="OauthAuthorize-description" key="description">
      Log into your Swoop account to receive your Swoop jobs inside {app?.name}
    </div>,
    <div className="field" key="username">
      {/* eslint-disable-next-line */}
      <label className="label-reg" htmlFor="user_email_or_email">Username or Email:</label>
      <input
        className="form-control"
        id="user_username_or_email"
        onChange={handleInput(setUsername)}
        required
        value={username}
      />
    </div>,
    <div className="field" key="password">
      {/* eslint-disable-next-line */}
      <label className="label-reg password" htmlFor="user_password">Password:</label>
      <input
        autoComplete="off"
        className="form-control fs-exclude"
        id="user_password"
        onChange={handleInput(setPassword)}
        required
        type="password"
        value={password}
      />
    </div>,
  ]
}

function authorize([username], [password]) {
  return () => Dispatcher.send(UserStore.RECEIVED_LOGIN, {
    username,
    password,
    preventRouting: true,
  })
}

function errorMessage() {
  const error = UserStore.getError()
  if (error) {
    let errorStr = 'There was a problem connecting to the server'
    if (error.status === 400 || error.status === 401) {
      errorStr = 'Please enter a valid email and password'
    } else if (error.status === 500) {
      errorStr = 'There\'s was a problem with the server, please try again later'
    }

    return <p className="alert-message">{errorStr}</p>
  }

  return null
}

function RedirectForm({ token }) {
  const form = React.createRef()

  useEffect(() => {
    form.current.submit()
  })

  const csrfToken = jquery('meta[name=csrf-token]').attr('content')

  return (
    <form action={window.location.href} className="hidden" method="POST" ref={form}>
      <input name="access_token" type="hidden" value={token} />
      <input name="authenticity_token" type="hidden" value={csrfToken} />
    </form>
  )
}

export default function OauthAuthorize() {
  const [rev, rerender] = useState(0)

  useEffect(() => {
    UserStore.bind(UserStore.CHANGE, () => rerender(rev + 1))
    UserStore.bind(UserStore.LOGIN, () => rerender(rev + 1))
  })

  const username = useState('')
  const password = useState('')

  const token = UserStore.getBearerToken()
  if (token) {
    return <RedirectForm token={token} />
  }

  const { app } = window.gon

  /* eslint-disable-next-line */
  return (
    <UserMock
      errorMessage={errorMessage}
      fields={<Fields app={app} password={password} username={username} />}
      handleSubmit={authorize(username, password)}
      redirect={false}
      submitValue="Connect"
      title={`Connect to ${app?.name}`}
    >
      <Link className="user-login-links signup" routeName="signup">
        New to Swoop? Create a Swoop Account
      </Link>
    </UserMock>
  )
}
