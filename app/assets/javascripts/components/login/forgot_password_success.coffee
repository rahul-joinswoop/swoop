import createReactClass from 'create-react-class'
import React from 'react'
UserStore = require('stores/user_store')
import Consts from 'consts'
import Utils from 'utils'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'

UserBorder       = React.createFactory(require 'components/login/user_border')
UserMock         = React.createFactory(require 'components/login/user_mock')

ForgotPasswordSuccess = createReactClass(
  getInitialState: -> { }

  handleBottomAction: ->
    Dispatcher.send(UserStore.LOGIN, {})

  userEmail: ->
    UserStore.getPasswordRequest()?.email

  userPhone: () ->
    UserStore.getPasswordRequest()?.phone

  fields: ->
    ReactDOMFactories.div
      className: ''
      if not @userEmail() and not @userPhone()
        ReactDOMFactories.div
          className: 'password_request-no-match'
          ReactDOMFactories.p
            className: 'password_request-alert-message'
            Consts.PASSWORD_REQUEST.NO_CONTACT_CHANNEL
          ReactDOMFactories.p
            className: 'password_request-alert-message'
            Consts.PASSWORD_REQUEST.CONTACT_YOUR_TEAM
      else
        method = Consts.PASSWORD_REQUEST.EMAILED_TO
        location = @userEmail()
        if !location?
          method = Consts.PASSWORD_REQUEST.TEXTED_TO
          location = Utils.renderPhoneNumber(@userPhone())
        ReactDOMFactories.div
          className: ''
          ReactDOMFactories.p
            className: 'password_request-notice-message'
            Consts.PASSWORD_REQUEST.SUCCESS(method, location)

  render: ->
    UserMock
      title: 'FORGOT PASSWORD'
      fields: @fields()
      showBottomLink: true
      handleBottomAction: @handleBottomAction
      bottomActionValue: 'Back to Login'
      onlyShowNoticeMessage: true
      redirect: false
)

module.exports = ForgotPasswordSuccess
