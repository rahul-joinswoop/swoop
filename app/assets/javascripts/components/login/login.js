
import React from 'react'
import { Link } from 'react-router5'
import Dispatcher from 'lib/swoop/dispatcher'
import Logger from 'lib/swoop/logger'
import UserStore from 'stores/user_store'
import UserMock from 'components/login/user_mock'

class Login extends React.Component {
  constructor(props) {
    super(props)

    this.state = { rev: 0 }
  }

  componentDidMount() {
    Logger.debug('LOGIN: user login bound')
    UserStore.bind(UserStore.CHANGE, this.userChanged)
  }

  componentWillUnmount() {
    UserStore.unbind(UserStore.CHANGE, this.userChanged)
  }

  userChanged = () => {
    this.setState(prevState => ({ rev: prevState.rev + 1 }))
    Logger.debug('LOGIN: checking user')
  }

  login = () => {
    // TODO: CHECK IF USER IS ADMIN AND LET IT GO THROUGH
    // This would allow us to go straight to admin tools
    // if UserStore.isRoot()
    //  true
    Dispatcher.send(UserStore.RECEIVED_LOGIN, {
      username: $('#user_username_or_email').val(),
      password: $('#user_password').val(),
    })
  }

  handleBottomAction = () => {
    Dispatcher.send(UserStore.FORGOT_PASSWORD, {})
  }

  signup = () => {}

  errorMessage = () => {
    // TODO: make sure this  is actually and invalid email and password and not another server error
    if (UserStore.getError() != null) {
      const r = UserStore.getError()
      Logger.debug('rendering error', UserStore.getError())
      let error = 'There was a problem connecting to the server'

      if (r.status === 400 || r.status === 401) {
        error = 'Please enter a valid email and password'
      }

      if (r.status === 500) {
        error = "There's was a problem with the server, please try again later"
      }

      return (
        <p className="alert-message">
          {error}
        </p>
      )
    }
  }

  fields = () => (
    <div className="">
      <div className="field">
        <label className="label-reg" htmlFor="user_email_or_email">
            Username or email
        </label>
        <input
          className="form-control"
          defaultValue=""
          id="user_username_or_email"
          name="user[username_or_email]"
          required
        />
      </div>
      <div className="field">
        <label className="label-reg password" htmlFor="user_password">
            Password
        </label>
        <input
          autoComplete="off"
          className="form-control fs-exclude"
          id="user_password"
          name="user[password]"
          required
          type="password"
        />
      </div>
    </div>
  )

  render() {
    return (
      <UserMock
        bottomActionValue="Forgot Password?"
        errorMessage={this.errorMessage}
        fields={this.fields()}
        handleBottomAction={this.handleBottomAction}
        handleSubmit={this.login}
        showBottomLink
        submitValue="Log In"
        title="LOG IN"
      >
        <Link
          className="user-login-links signup"
          onClick={this.signup}
          routeName="signup"
        >
          New to Swoop? Try for Free!
        </Link>
      </UserMock>
    )
  }
}

export default Login
