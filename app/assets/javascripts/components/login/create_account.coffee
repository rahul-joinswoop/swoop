import React from 'react'
require('stylesheets/components/login/create_account.scss')
UserStore = require('stores/user_store')
import LoginStore from 'stores/login_store'
RefererStore = require('stores/referer_store')
Field = require('fields/field')
import PhoneField from 'fields/phone_field'
EmailField = require('fields/email_field')
CheckboxField = require('fields/checkbox_field')
GeosuggestField = require('fields/geosuggest_field')
SelectField = require('fields/select_field')
import Api from 'api'
UserBorder       = React.createFactory(require 'components/login/user_border')
UserMock         = React.createFactory(require 'components/login/user_mock')
BaseComponent = require 'components/base_component'
Renderer=require('form_renderer')
import { clone } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'
import Context from 'Context'

class CompanyNameField extends Field
  _name: "company.name"
  _placeholder: "Enter Company Name"
  _label: "Company Name:"
  _required: true
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style

class DispatchNumberField extends PhoneField
  _name: "company.phone"
  _label: "Dispatch Phone:"
  getClassName: -> super(arguments...) + " fs-exclude"
  _required: true
  _enabled: false
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style


class CompanyAddressField extends GeosuggestField
  _name: "company.location"
  _label: "Company Address:"
  _required: true
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style


class FirstNameField extends Field
  _name: "user.first_name"
  _label: "First Name:"
  getClassName: -> super(arguments...) + " fs-exclude"
  _required: true
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style


class LastNameField extends Field
  _name: "user.last_name"
  _label: "Last Name:"
  getClassName: -> super(arguments...) + " fs-exclude"
  _required: true
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style


class EmailField extends EmailField
  _name: "user.email"
  _label: "Email:"
  _required: true
  _placeholder: "your contact email"
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style

class UserNameField extends Field
  _name: "user.username"
  _label: "Username:"
  _required: true
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style

class PasswordField extends Field
  _name: "user.password"
  _label: "Password:"
  _inputType: "password"
  _placeholder: "at least 8 characters"
  _required: true
  getClassName: -> super(arguments...) + " fs-exclude"
  findErrors: ->
    if !@record.user.password? || @record.user.password?.length < 8
      return "Password must be at least 8 characters"
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style

class ConfirmPasswordField extends Field
  _name: "user.confirm_password"
  _label: "Confirm Password:"
  _inputType: "password"
  _placeholder: "must match"
  _required: true
  getClassName: -> super(arguments...) + " fs-exclude"
  findErrors: ->
    if !@record.user.confirm_password?
      return "Required"
    if @record.user.password != @record.user.confirm_password
      return "Passwords don't match"
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style


class RefererField extends SelectField
  _name: "company.referer_id"
  _label: "How did you hear about Swoop?"
  _required: true
  getOptions: -> @optionMapper(RefererStore.getAllForStore(@props.componentId), clone(super(arguments...)))
  addErrorBorder: (style) ->
    if @props.form.hasAttemptedSubmit()
      return super(arguments...)
    return style

class TermsField extends CheckboxField
  _name: "user.terms"
  findErrors: ->
    if !@record.user.terms
      return "Required"
  getLabel: ->
    showError = @findErrors()? and @props.form.hasAttemptedSubmit()
    span null,
      span null,
        "I agree to Swoops "
      ReactDOMFactories.a
        href: "https://app.joinswoop.com/SwoopTerms.pdf"
        target: "_blank"
        "Terms"
      span null,
        " and "
      ReactDOMFactories.a
        href: "https://app.joinswoop.com/SwoopPrivacy.pdf"
        target: "_blank"
        "Privacy Policy"
      if showError
        div
          className: "error"
          "Please accept terms to proceed"

class CreateAccount extends BaseComponent
  componentDidMount: ->
    super(arguments...)

    Tracking.track("Sign Up Form - Visit", {
      category: "Onboarding"
    })

  createUser: =>
    if @form.isValid(true)
      newrecord = @form.getRecordChanges()

      @form.attemptSubmit((newrecord) =>
        callbacks = newrecord.callbacks
        delete newrecord.callbacks

        Api.signup(newrecord, {
          success: (data) =>
            callbacks.success(data)
            Dispatcher.send(UserStore.RECEIVED_LOGIN, {
              username: newrecord.user.email,
              password: newrecord.user.password
            })

            referrer = RefererStore.get(data.referer_id, 'name')

            Tracking.track("Sign Up - Success", {
              category: "Onboarding"
              referral_source: referrer
            })

            Tracking.identifyForSalesforce(data, newrecord, referrer)

          error: (data) =>
            callbacks.error(data)
            Tracking.track("Sign Up Form - Error", {
              category: "Onboarding"
            })

        })

      , null, (loading) =>
        @setState
          loading: loading
      )

    else
      @form.attemptedSubmit()
      @rerender()

    Tracking.track("Sign Up Form - Create Account", {
      category: "Onboarding"
      content_name:'Free Trial Account'
    })

  constructor: ->
    super(arguments...)
    @setupForm()

  setupForm: =>
    phone = LoginStore.getPhone()

    @form = new Renderer({}, @rerender, {componentId: @getId()})
    @form.record = {
      company: {}
      user: {}
    }
    @form.record.company.name = LoginStore.getName()
    @form.record.company.phone = phone
    if !phone
      Context.navigate('login')
      return

    @form.register(CompanyNameField, "company.name")
    @form.register(DispatchNumberField, "company.phone")
    @form.register(CompanyAddressField, "company.location")
    @form.register(FirstNameField, "user.first_name")
    @form.register(LastNameField, "user.last_name")
    @form.register(EmailField, "user.email")
    @form.register(UserNameField, "user.username")
    @form.register(PasswordField, "user.password")
    @form.register(ConfirmPasswordField, "user.confirm_password")
    @form.register(RefererField, "company.referer_id")
    @form.register(TermsField, "user.terms")


  fields: =>
    div null,
      div
        className: "info"
        "Final step to start your free trial! Please fill out the rest of your company details."
      @form.renderInputs([
        ["company.name", {  }]
        ["company.phone", {  }]
        ["company.location", {  }]
      ])
      h1
        className: 'form-title'
        "PERSONAL INFO"
      div
        className: "info info_personal"
        "Add in your personal info to finish creating your account."
      @form.renderInputs([
        ["user.first_name", {  }]
        ["user.last_name", {  }]
        ["user.email", {  }]
        ["user.username", {  }]
        ["user.password", {  }]
        ["user.confirm_password", {  }]
        ["company.referer_id", {  }]
        ["user.terms", {  }]
      ])


  render: ->
    super(arguments...)

    UserMock
      title: 'COMPANY INFO'
      className: 'create_account'
      showSpinning: @state.loading
      handleSubmit: @createUser
      fields: @fields()
      submitValue: 'Start Free Trial'
      errorMessage: @errorMessage
      showBottomLink: true
      withLogos: true


module.exports = CreateAccount
