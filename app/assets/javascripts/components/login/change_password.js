import React from 'react'
import Consts from 'consts'
import Dispatcher from 'lib/swoop/dispatcher'
import UserMock from 'components/login/user_mock'
import UserStore from 'stores/user_store'
import Utils from 'utils'

class ChangePassword extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      password_request_uuid: this.queryStringUUID(),
      password_request_valid: false,
      passwordsMatch: true,
      username: null,
      validated_uuid: false,
    }
  }

  componentDidMount() {
    UserStore.bind(UserStore.PASSWORD_REQUEST_IS_VALID, this.showFields)
    UserStore.bind(
      UserStore.PASSWORD_REQUEST_IS_NOT_VALID,
      this.handlePasswordRequestNotValid,
    )
    UserStore.bind(UserStore.INVALID_PASSWORD, this.handleInvalidPassword)
    UserStore.bind(UserStore.CHANGE_PASSWORD_FAIL, this.handleError)
    Dispatcher.send(UserStore.VALIDATE_PASSWORD_REQUEST, {
      uuid: this.state.password_request_uuid,
    })
  }

  componentWillUnmount() {
    UserStore.unbind(UserStore.PASSWORD_REQUEST_IS_VALID, this.showFields)
    UserStore.unbind(
      UserStore.PASSWORD_REQUEST_IS_NOT_VALID,
      this.handlePasswordRequestNotValid,
    )
    UserStore.unbind(UserStore.INVALID_PASSWORD, this.handleInvalidPassword)
    UserStore.unbind(UserStore.CHANGE_PASSWORD_FAIL, this.handleError)
  }

  showFields = (data) => {
    this.clearErrors()
    this.setState({
      password_request_valid: true,
      username: data.username,
      validated_uuid: true,
    })
  }

  handleError() {
    // setTimeout is used to run Utils.handleError on next event loop tick to avoid error
    // Dispatch.dispatch(...): Cannot dispatch in the middle of a dispatch.
    // because Utils.handleError calls Dispatcher.dispatch
    return setTimeout(Utils.handleError, 0)
  }

  handlePasswordRequestNotValid = (data) => {
    this.clearErrors()
    this.setState({
      password_request_valid: false,
      username: null,
      validated_uuid: true,
    })
  }

  handleInvalidPassword = (data) => {
    this.clearErrors()
    this.setState({
      invalidPassword: true,
    })
  }

  handlePasswordsDontMatch = () => {
    this.clearErrors()
    this.setState({
      passwordsMatch: false,
    })
  }

  clearErrors = () => {
    this.setState({
      invalidPassword: false,
      passwordsMatch: true,
    })
  }

  handleBottomAction() {
    Dispatcher.send(UserStore.LOGIN, {})
  }

  resetPassword = () => {
    if (this.state.user_password !== this.state.user_password_confirmation) {
      this.handlePasswordsDontMatch()
      return
    }

    Dispatcher.send(UserStore.CHANGE_PASSWORD, {
      user: {
        password: this.state.user_password,
        username: this.state.username,
      },
      password_request: {
        uuid: this.state.password_request_uuid,
      },
    })
  }

  setUserPassword = (e) => {
    this.setState({
      user_password: e.target.value,
    })
  }

  setUserPasswordConfirmation = (e) => {
    this.setState({
      user_password_confirmation: e.target.value,
    })
  }

  fields = () => {
    if (this.state.password_request_valid) {
      const invalidPassword = this.state.invalidPassword && (
        <div>
          <p className="password_request-alert-message change-password-invalid">
            {Consts.CHANGE_PASSWORD.INVALID_PASSWORD}
          </p>
        </div>
      )

      const passwordMismatch = !this.state.passwordsMatch && (
        <div>
          <p className="password_request-alert-message change-password-invalid">
            {Consts.CHANGE_PASSWORD.PASSWORDS_DONT_MATCH}
          </p>
        </div>
      )

      return (
        <div>
          {invalidPassword}
          {passwordMismatch}
          <p className="change-password-username">
            {`Username: ${this.state.username}`}
          </p>
          <div className="field">
            <label className="label-reg password" htmlFor="user_input">
              New Password:
            </label>
            <input
              autoComplete="off"
              className="form-control"
              id="user_password"
              name="user[password]"
              onChange={this.setUserPassword}
              placeholder={Consts.CHANGE_PASSWORD.PASSWORD_PLACEHOLDER}
              type="password"
            />
          </div>
          <div className="field">
            <label className="label-reg password" htmlFor="user_input">
              Re-enter Password:
            </label>
            <input
              autoComplete="off"
              className="form-control"
              id="user_password_confirmation"
              name="user[password_confirmation]"
              onChange={this.setUserPasswordConfirmation}
              placeholder={Consts.CHANGE_PASSWORD.CONFIRMATION_PLACEHOLDER}
              type="password"
            />
          </div>
        </div>
      )
    } else {
      return (
        <div className="password-change-error-message">
          <p className="password_request-alert-message change-password-invalid-uuid">
            {Consts.CHANGE_PASSWORD.UUID_NOT_FOUND}
          </p>
        </div>
      )
    }
  }

  queryStringUUID() {
    const regexS = '[\\?&]uuid=([^&#]*)'
    const regex = new RegExp(regexS)
    const results = regex.exec(location.href)

    if (results != null) {
      return results[1]
    }

    return null
  }

  render() {
    return (
      <UserMock
        bottomActionValue="Back to Login"
        errorMessage={this.errorMessage}
        fields={this.fields()}
        handleBottomAction={this.handleBottomAction}
        handleSubmit={this.resetPassword}
        onlyShowNoticeMessage={!this.state.password_request_valid}
        redirect={false}
        showBottomLink={!this.state.password_request_valid}
        showSpinning={!this.state.validated_uuid}
        submitValue="Reset Password"
        title="RESET PASSWORD"
      />
    )
  }
}

export default ChangePassword
