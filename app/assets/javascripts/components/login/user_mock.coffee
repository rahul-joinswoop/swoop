import React from 'react'
require('stylesheets/devise/base.scss')
import { form, input, a, div } from 'react-dom-factories'
UserStore = require('stores/user_store')
import Consts from 'consts'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)

import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

UserBorder = React.createFactory(require 'components/login/user_border')
Header = React.createFactory(require('components/Header').default)
BaseComponent = require('components/base_component')
class UserMock extends BaseComponent
  @defaultProps =
    redirect: true

  componentDidMount: ->
    super(arguments...)
    UserStore.bind( UserStore.LOGIN, @checkUser )
    @checkUser()

  componentWillUnmount: ->
    super(arguments...)
    UserStore.unbind( UserStore.LOGIN, @checkUser )

  checkUser: ->
    if UserStore.getUser()? and @props.redirect
      UserStore.routeUser  UserStore.getUser()

  handleBottomAction: (e) =>
    e.preventDefault()
    e.stopPropagation()

    @props.handleBottomAction()

    return false

  handleSubmit: (e) =>
    e.preventDefault()
    e.stopPropagation()

    @props.handleSubmit()

    return false

  render: ->
    super(arguments...)
    div
      className: @props.className
      Header null
      UserBorder
        title: @props.title
        withLogos: @props.withLogos
        div
          className: 'form-group user-mock-group'
          if @props.showSpinning? && @props.showSpinning
            div
              className: 'change-password-loading'
              Loading null
          else
            if @props.onlyShowNoticeMessage
              @props.fields
            else
              form
                className: 'new_user'
                id: 'new_user'
                action: '#'
                acceptCharset: 'UTF-8'
                method: 'post'
                onSubmit: @handleSubmit
                @props.errorMessage() if @props.errorMessage?
                @props.fields
                div
                  className: 'actions'
                  Button
                    color: 'primary'
                    lineHeight: 40
                    label: @props.submitValue
                    name: 'commit'
                    size: 'full-width'
                    style: {
                      fontSize: 16,
                      margin: '20px 0 0',
                    }
                    type: 'submit'
          if @props.showBottomLink
            a
             className: 'user-login-links'
             href: '#'
             onClick: @handleBottomAction
             @props.bottomActionValue
          @props.children


module.exports = UserMock
