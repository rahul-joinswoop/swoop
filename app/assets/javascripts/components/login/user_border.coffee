import createReactClass from 'create-react-class'
import React from 'react'
import { div, img, h1  } from 'react-dom-factories'
import Consts from 'consts'
UserStore = require('stores/user_store')
bigValleyTowingLogo = require('public/images/towerLogos/bigValleyTowingLogo.png')
downtownAutoCenterLogo = require('public/images/towerLogos/downtownAutoCenterLogo.png')
finishLineTowingLogo = require('public/images/towerLogos/finishLineTowingLogo.png')
jimsTowingLogo = require('public/images/towerLogos/jimsTowingLogo.png')
roadRunnerLogo = require('public/images/towerLogos/roadRunnerLogo.png')

UserBorder = createReactClass(
  displayName: 'EnterpriseBorder'
  render: ->
    div
      className: 'content-wrapper reg'
      style: minHeight: 440
      div
        className: 'row'
        div
          className: 'box-form reg'
          div
            className: 'box-body reg'
            style: if UserStore.getError() then { height: 440 }
            h1
              className: 'form-title'
              @props.title
            @props.children
      if @props.withLogos
        div
          className: 'bottom-logos-container'
          div
            className: 'text'
            "Proudly powering the nation's best operators"
          div
            className: 'logos'
            img
              className: 'road-runner-logo'
              src: roadRunnerLogo
            [finishLineTowingLogo, bigValleyTowingLogo, jimsTowingLogo, downtownAutoCenterLogo].map (logo) ->
              img
                key: logo
                src: logo

      div
        className: 'row'
        div
          className: 'user-border-contact-info'
          Consts.CONTACT_INFO
)

module.exports = UserBorder
