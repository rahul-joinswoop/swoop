import React from 'react'
require('stylesheets/components/login/company_exists.scss')
UserStore = require('stores/user_store')
import LoginStore from 'stores/login_store'
Field = require('fields/field')
import PhoneField from 'fields/phone_field'
UserMock = React.createFactory(require 'components/login/user_mock')
BaseComponent = require 'components/base_component'
import Utils from 'utils'
import Dispatcher from 'lib/swoop/dispatcher'
import Context from 'Context'
import { Link } from 'react-router5'
RouterLink = React.createFactory(Link)

class Signup extends BaseComponent


  constructor: ->
    super(arguments...)

  handleBottomAction: ->
    Dispatcher.send(UserStore.LOGIN, {})

  fields: =>
    div
      className: 'company_exists'
      span null,
        "We found an existing company on Swoop based on the phone number you entered: " + Utils.renderPhoneNumber(LoginStore.getPhone())
      div
        className: 'details'
        span null,
          LoginStore.getCompanyName()
        span null,
          LoginStore.getCompanyAddress()
      div null,
        "Don't have an account?  Please contact your manager or admin to set up a user for you."
      div
        className: 'forgot_password'
        span null,
          "Forgot your password? "
        RouterLink
          routeName: 'forgotPassword'
          "Click here"

  goToSignup: ->
    #TODO: Clear signup
    Context.navigate('signup')

  render: ->
    super(arguments...)

    UserMock
      title: 'START FREE TRIAL'
      className: 'company_exists'
      showSpinning: @state.loading
      handleSubmit: @goToSignup
      fields: @fields()
      submitValue: 'Back'
      showBottomLink: true
      handleBottomAction: @handleBottomAction
      bottomActionValue: 'Existing User? Log In'
      withLogos: true


module.exports = Signup
