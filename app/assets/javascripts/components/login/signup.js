import React from 'react'
import Api from 'api'
import BaseComponent from 'components/base_component'
import Context from 'Context'
import Dispatcher from 'lib/swoop/dispatcher'
import Field from 'fields/field'
import LoginStore from 'stores/login_store'
import PhoneField from 'fields/phone_field'
import Renderer from 'form_renderer'
import UserMock from 'components/login/user_mock'
import UserStore from 'stores/user_store'
import 'stylesheets/components/login/user_signup.scss'

class CompanyNameField extends Field {
  _name = 'name'

  _label = 'Company Name:'
}

class DispatchNumberField extends PhoneField {
  _name = 'phone'

  _label = 'Dispatch Number:'

  _required = true

  getClassName() {
    return `${super.getClassName(...arguments)} fs-exclude`
  }
}

class Signup extends BaseComponent {
  constructor(props) {
    super(props)
    this.setupForm()
  }

  checkCompany() {
    if (this.form.isValid(true)) {
      const newrecord = this.form.getRecordChanges()
      this.setState({
        loading: true,
      })
      LoginStore.clearInfo(newrecord.name, newrecord.phone)
      Api.publicSearchSitesByPhone(newrecord, {
        success: (sites) => {
          this.setState({
            loading: false,
          })
          LoginStore.setInfo(newrecord.name, newrecord.phone)

          if (sites.length === 0) {
            Context.navigate('createAccount')
            Tracking.track('Sign Up - Check Company - New', {
              category: 'Onboarding',
            })
          } else {
            LoginStore.setSite(sites[0])
            Context.navigate('companyExists')
            Tracking.track('Sign Up - Check Company - Exists', {
              category: 'Onboarding',
              label: newrecord.phone,
            })
          }
        },
        error: data => this.setState({
          errorMessage: 'Problem looking up phone',
          loading: false,
        }),
      })
    } else {
      this.form.attemptedSubmit()
      this.rerender()
    }

    Tracking.track('Sign Up - Check Company - Next', {
      category: 'Onboarding',
      content_name: 'Free Trial Account',
    })
  }

  componentDidMount() {
    super.componentDidMount(...arguments)

    Tracking.track('Sign Up - Check Company - Visit', {
      category: 'Onboarding',
    })
  }

  setupForm = () => {
    this.form = new Renderer({}, this.rerender)
    this.form.register(CompanyNameField, 'name')
    this.form.register(DispatchNumberField, 'phone')
    this.form.isValid()
  }

  handleBottomAction = () => {
    Dispatcher.send(UserStore.LOGIN, {})
  }

  clearState = () => {
    this.setupForm()

    this.setState({
      company: null,
      loading: false,
    })
  }

  fields = () => {
    return (
      <div>
        <span className="info_text" key="info_text">
          You're less than a minute away from your free trial with Swoop. Let's see if your company is already in our system.
        </span>
        {this.form.renderInputs([['name', {}], ['phone', {}]])}
      </div>
    )
  }

  errorMessage = () => {
    return this.state.errorMessage
  }

  render() {
    super.render(...arguments)

    return (
      <UserMock
        bottomActionValue="Existing User? Log In"
        className="user_signup"
        errorMessage={this.errorMessage}
        fields={this.fields()}
        handleBottomAction={this.handleBottomAction}
        handleSubmit={this.checkCompany}
        showBottomLink
        showSpinning={this.state.loading}
        submitValue="Next"
        title="START FREE TRIAL"
        withLogos
      />
    )
  }
}

export default Signup
