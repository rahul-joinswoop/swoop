import createReactClass from 'create-react-class'
import React from 'react'
require('stylesheets/components/partner_check_list.scss')
import Utils from 'utils'
FTable = React.createFactory(require 'components/resizableTable/FTable')
import { extend } from 'lodash'

PartnerCheckList = createReactClass(
  handleSelectRow: (row, partner) ->
    if !partner?.dispatchable_by_fleet
      @props?.setChosen(partner?.id)

  getCols: ->
     [
      {header: "Name", key: "name_with_company"}
      {header: "Phone", key: "phone", func: (partner) -> Utils.renderPhoneNumber(partner.phone)}
      {header: "Address", key: "location.address"}
      {header: "", key: "options", func: (partner, props) =>
        if partner.dispatchable_by_fleet
          return "Existing Partner"
        if @props.chosen == partner.id
          return div
            className: 'text-center'
            span
              className: 'fa fa-check'

      }
    ]

  render: ->
    FTable extend({}, @props,
      className: "partner_check_list"
      rows: if @props?.sites? then @props?.sites else []
      cols: @getCols()
      maxWidth: 538
      rowClick: @handleSelectRow
      switchWidth: 50
      rowClassNameGetter: (index, partner) =>
        if partner?.dispatchable_by_fleet
          return "existing"
        if partner.id == @props.chosen
          return "selected"
      )

)

module.exports = PartnerCheckList
