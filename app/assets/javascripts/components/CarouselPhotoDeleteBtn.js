import React from 'react'
import PropTypes from 'prop-types'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import 'stylesheets/components/CarouselPhotoDeleteBtn.scss'

const CarouselPhotoDeleteBtn = props => (
  <FontAwesomeButton
    className="delete-photo"
    icon="fa-times-circle"
    onClick={e => props.handlePhotoDelete(props.item, e)}
  />
)

CarouselPhotoDeleteBtn.propTypes = {
  handlePhotoDelete: PropTypes.func,
  item: PropTypes.object,
}

export default CarouselPhotoDeleteBtn
