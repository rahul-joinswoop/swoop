import React from 'react'
import BaseComponent from 'components/base_component'
import Countdown from 'components/countdown'
import moment from 'moment-timezone'
import TimeStore from 'stores/time_store'
import Utils from 'utils'

class ETATime extends BaseComponent {
  renderMinutes = (minutes) => {
    const hours = Math.floor(Math.abs(Math.floor(minutes)) / 60)
    const time = (minutes <= -60) ? `-${hours}:${Math.abs(minutes) % 60} hrs` : `${minutes} min`
    return (
      <span>
        <span> (</span>
        { minutes <= 0 &&
          <span className="error">
            {time}
          </span>}
        <span>)</span>
      </span>
    )
  }

  renderLiveScheduledTime = (job, minutes, show_minutes, countdownProps) => {
    countdownProps.time = moment(job?.toa?.live)
    return (
      <span>
        <Countdown {...countdownProps} />
        { show_minutes ?
          this.renderMinutes(minutes) :
          <span>
            {` (${Utils.formatDateTime(job.scheduled_for)})`}
          </span>}
      </span>
    )
  }

  renderScheduledTime = (job, countdownProps) => {
    const scheduled_time = moment(job.scheduled_for)
    const minutes = Math.ceil(scheduled_time.diff(moment(), 'minutes', true))
    let show_minutes = false

    if (minutes < 0 && !isNaN(minutes)) {
      show_minutes = true
    }

    if (job.toa.live) {
      return this.renderLiveScheduledTime(
        job,
        minutes,
        show_minutes,
        countdownProps,
      )
    } else {
      return (
        <span>
          {Utils.formatDateTime(job.scheduled_for)}
          {show_minutes && this.renderMinutes(minutes)}
        </span>
      )
    }
  }

  render() {
    super.render(...arguments)

    TimeStore.watchMinute(this.getId())
    const { job } = this.props
    const ret = Utils.getLastEtaObj(job)

    const countdownProps = {
      time: ret,
    }

    if (this.props.actualEta) {
      countdownProps.actualEta = this.props.actualEta
    }

    if (this.props.rerender) {
      countdownProps.rerender = this.props.rerender
    }

    if (job.scheduled_for) {
      return this.renderScheduledTime(job, countdownProps)
    }

    return (
      <span>
        <Countdown {...countdownProps} />
        {(ret && job.toa.original && !moment(job.toa.original).isSame(ret) && !Utils.isDoubleOverdue(job)) &&
          <span>
            <span>
              {' ('}
            </span>
            <Countdown actualEta={false} time={moment(job.toa.original)} />
            <span>
              {`)${job.toa.count > 2 ? `^${job.toa.count - 1}` : ''}`}
            </span>
          </span>}
      </span>
    )
  }
}

export default ETATime
