import React from 'react'
import AccountStore from 'stores/account_store'
import BaseComponent from 'components/base_component'
import CompanyStore from 'stores/company_store'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import onClickOutside from 'react-onclickoutside'
import SiteStore from 'stores/site_store'
import UserStore from 'stores/user_store'
import Utils from 'utils'
import { forEach } from 'lodash'
import './IsscStatus.scss'

class IsscStatus extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
    }
    this.register(AccountStore, AccountStore.CHANGE)
    this.register(CompanyStore, CompanyStore.CHANGE)
  }

  accountStatus = (companyId) => {
    let status = ''
    switch (CompanyStore.get(companyId, 'issc_status', this.getId())) {
      case 'Up':
        status = ''
        break
      case 'Down':
        status = '(Disconnected)'
        break
      default:
        status = '(Unknown)'
    }
    return status
  }

  handleClick = () => {
    this.setState(previousState => ({
      open: !previousState.open,
    }))
  }

  handleClickOutside = () => {
    this.setState({
      open: false,
    })
  }

  networkColor = () => {
    const { motorClubAccounts } = this.props
    let networkColor = 'white'
    forEach(motorClubAccounts, (account) => {
      if (account?.company?.id) {
        if (CompanyStore.get(account.company.id, 'issc_status', this.getId()) === 'Down') {
          return 'red'
        }
        const sites = account.dispatchable_sites
        forEach(sites, (site) => {
          if (site.enabled && site.isscs) {
            forEach(site.isscs, (issc) => {
              const color = Utils.isscStatusColor(issc.status)
              if (color === 'red') {
                return color
              } else
              if (color === 'orange') {
                networkColor = color
              }
            })
          }
        })
      }
    })
    return networkColor
  }

  hasVendorIDs = account => account?.dispatchable_sites?.some(site => site.isscs.length) || account?.unconfigured_vendor_ids?.length

  renderMotorclubs = (motorClubAccounts) => {
    const motorclubs = []
    forEach(motorClubAccounts, (account) => {
      if (!AccountStore.isDisabledMotorClub(account)) {
        const accountStatus = this.accountStatus(account?.company?.id)
        const color = accountStatus === 'Connected' ? 'green' : 'red'
        motorclubs.push(
          <li
            className="account-item"
            key={account?.id}
          >
            <span className="account-name">{account?.name} <span style={{ color }}>{accountStatus}</span></span>
            { !this.hasVendorIDs(account) &&
              <div className="no-vendors">No Vendor IDs</div>}
            <ul className="site-list">
              { account?.dispatchable_sites &&
                account.dispatchable_sites.map(site => (
                  <li className="site-list-row" key={site.site_id}>
                    <span className="site-name">
                      {SiteStore.get(site.site_id, 'name', this.getId())}
                      {!(site.enabled && site.site_dispatchable) &&
                        ' (non-dispatchable)'}
                    </span>
                    <ul className="vendors-list">
                      { site.isscs.map(issc => this.renderVendorRow(issc, site)) }
                    </ul>
                  </li>
                ))}
              { account?.unconfigured_vendor_ids?.length > 0 &&
                <li className="site-list-row">
                  <span className="site-name">No Sites</span>
                  <ul className="vendors-list">
                    { account.unconfigured_vendor_ids.map(vendorID =>
                      <li className="vendors-list-row" key={vendorID}>
                        {vendorID}
                        <span className="vendors-list-status" style={{ color: 'orange' }}>Configure Site</span>
                      </li>
                    )}
                  </ul>
                </li>}
            </ul>
          </li>
        )
      }
    })
    return motorclubs
  }

  renderVendorRow = (issc, site) => {
    if (issc.clientid === 'GCO') {
      return null
    }
    const color = site.enabled ? Utils.isscStatusColor(issc.status) : 'black'
    return (
      <li className="vendors-list-row">
        {issc.contractorid}
        {issc.clientid === 'GCOAPI' && <span className="gcoapi"> (GCOAPI)</span>}
        <span className="vendors-list-status" style={{ color }}>{Utils.isscHumanStatus(issc)}</span>
      </li>
    )
  }

  render() {
    const { motorClubAccounts } = this.props
    if (UserStore.isPartner() && motorClubAccounts.length) {
      return (
        <li className="issc-status">
          <FontAwesomeButton
            className={this.state.open ? 'active' : undefined}
            icon="fa-podcast"
            onClick={this.handleClick}
            size="medium"
            style={{
              color: this.networkColor(),
            }}
          />
          { this.state.open &&
            <div className="issc-container">
              <FontAwesomeButton
                icon="fa-times"
                onClick={this.handleClick}
                size="small"
              />
              <ul className="motorclubs">
                {this.renderMotorclubs(motorClubAccounts)}
              </ul>
            </div>}
        </li>
      )
    }

    return null
  }
}

export default onClickOutside(IsscStatus)
