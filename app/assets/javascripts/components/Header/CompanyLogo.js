import React from 'react'
import CompanyStore from 'stores/company_store'
import UserStore from 'stores/user_store'
import './CompanyLogo.scss'

const CompanyLogo = ({ id } = {}) => {
  const current_user = UserStore.getUser()
  const companyId = current_user?.company?.id
  const companyLogoUrl = companyId && CompanyStore.get(companyId, 'logo_url', id)

  return (
    <a className="logo" href={UserStore.getUserUrl()}>
      { companyId && companyLogoUrl ?
        <span className="custom-logo">
          <img
            alt="Company Logo"
            className="company_logo"
            src={companyLogoUrl}
          />
        </span> :
        <>
          <span className="logo-mini">
            <img
              alt="Swoop"
              src={require('images/Swoop_Logo-white-transparent-S.png')}
              width="40"
            />
          </span>
          <span className="logo-lg">
            <img
              alt="Swoop"
              src={require('images/Swoop_Logo-white-transparent.png')}
              width="144"
            />
          </span>
        </>}
    </a>
  )
}
export default CompanyLogo
