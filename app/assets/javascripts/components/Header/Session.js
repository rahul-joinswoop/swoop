import React from 'react'

export const DEFAULT_SESSIONS = [
  '',
  'Tesla',
  'Turo',
  'Swoop',
  'SwoopDispatcher',
  'Partner1',
  'Partner2',
  'Enterprise',
  'ManagedFleet',
  'Fleet1',
]

const handleSessionChange = e =>
  /* eslint-disable-next-line */
  window.open(`${window.location.origin}#session_id=${e.target.value}`, '_blank')

const Session = ({ session } = {}) =>
  <li
    style={{
      background: '#565656',
      borderRadius: 9,
      bottom: -7,
      height: 20,
      left: 0,
      lineHeight: '12px',
      padding: '0 15px',
      position: 'absolute',
      textAlign: 'center',
      zIndex: 1000,
    }}
  >
    <span
      style={{
        display: 'inline-block',
        fontSize: 10,
        lineHeight: '12px',
        margin: 0,
        marginRight: 15,
        verticalAlign: 'baseline',
      }}
    >
      Session #
    </span>
    <select
      onChange={handleSessionChange}
      style={{
        color: '#000',
        display: 'inline-block',
        fontSize: 9,
        lineHeight: 1.15,
        margin: 0,
        textTransform: 'none',
        verticalAlign: 'baseline',
      }}
      value={session}
    >
      {session >= 10 &&
      <option value={session}>
        {session}
      </option>}
      {DEFAULT_SESSIONS.map((d, i) =>
        <option key={`default-value-${d}`} value={i - 1}>
          {`${i - 1} - ${d}`}
        </option>
      )}
    </select>
  </li>

export default Session
