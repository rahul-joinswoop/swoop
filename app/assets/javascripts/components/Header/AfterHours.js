import React from 'react'
import BaseComponent from 'components/base_component'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import onClickOutside from 'react-onclickoutside'
import SiteStore from 'stores/site_store'
import ToggleButton from 'react-toggle-button'
import UserStore from 'stores/user_store'
import { partial, some } from 'lodash'
import './AfterHours.scss'

const AfterHoursView = ({ handleClick, sites }) => (
  <div className="after-hours-options">
    <p>Override to receive jobs digitally
      <FontAwesomeButton
        icon="fa-times"
        onClick={handleClick}
        size="small"
      />
    </p>
    <ul className="override-sites">
      { sites.map(site => (
        <li key={site.id}>
          <ToggleButton
            colors={{
              inactive: {
                base: 'rgb(170,170,170)',
                hover: 'rgb(170,170,170)',
              },
            }}
            onToggle={partial((siteId, checked) => SiteStore.updateItem({
              id: siteId,
              force_open: !checked,
            }), site.id)}
            value={site.force_open || false}
          />
          <span>{site.name}</span>
        </li>
      ))}
    </ul>
  </div>
)

class AfterHours extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
    }
  }

  handleClick = () => {
    this.setState(previousState => ({
      open: !previousState.open,
    }))
  }

  handleClickOutside = () => {
    this.setState({
      open: false,
    })
  }

  render() {
    const { sites } = this.props
    const siteOn = some(sites, site => !!site.force_open)

    if (sites.length === 0 || !UserStore.isPartner()) {
      return null
    }

    return (
      <div className="after-hours-widget">
        <FontAwesomeButton
          className="after-hours-icon"
          icon="fa-clock-o"
          onClick={this.handleClick}
          size="medium"
          style={{ color: siteOn ? '#FFF' : '#F00' }}
        />

        <div className="after-hours-status" onClick={this.handleClick} role="button">
          <div>
            <span>After Hours</span>
            <span>Digital Dispatch: </span>
            <span
              style={{ backgroundColor: siteOn ? '#42b756' : '#F00' }}
            >
              {siteOn ? 'On' : 'Off'}
            </span>
          </div>
          <FontAwesomeButton
            icon={this.state.open ? 'fa-chevron-up' : 'fa-chevron-down'}
          />
        </div>

        { this.state.open && (
          <AfterHoursView handleClick={this.handleClick} sites={sites} />
        )}
      </div>
    )
  }
}

export default onClickOutside(AfterHours)
