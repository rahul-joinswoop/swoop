import React from 'react'
import classNames from 'classnames'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import onClickOutside from 'react-onclickoutside'
import UserStore from 'stores/user_store'
import VersionStore from 'stores/version_store'
import { isEmpty } from 'lodash'
import HeaderLink from './HeaderLink'

class DropNav extends React.Component {
  handleClickOutside = (e) => {
    this.props.handleToggle(e, false)
  }

  render() {
    const {
      active,
      handleClick,
      logout,
      menu,
      navigate,
      settings,
      toggle,
      type,
    } = this.props

    let name = 'Administrator'
    const current_user = UserStore.getUser()

    if (isEmpty(current_user)) {
      return null
    }

    if (!UserStore.isRoot() && current_user.company != null) {
      name = current_user.company.name
    }

    const username = current_user.full_name || current_user.username

    return (
      <ul className={classNames('app-drop-nav', toggle ? 'menu-open' : 'menu-closed')}>
        <li className="swoop-user" onClick={this.props.handleToggle}>
          <div className="swoop-user-container">
            <div className="swoop-user-names">
              <div>
                <span className="userName">
                  {username}
                </span>
                <span className="companyName">
                  {name}
                </span>
              </div>
            </div>
            <FontAwesomeButton
              icon={toggle ? 'fa-chevron-up' : 'fa-chevron-down'}
            />
          </div>
        </li>

        <li className="toggled-menu">
          <ul>
            { menu.map((menuItem) => {
              if (menuItem.show === false) return null
              return (
                <li
                  className={classNames('responsive-nav-item', { active: menuItem.title === active })}
                  key={`menu-item-${menuItem.title}`}
                >
                  <HeaderLink {...{ handleClick, menuItem, type }} />
                </li>
              )
            })}

            <li className="swoop-version">
              <span>{`Swoop ${VersionStore.swoop_env} ${window.VERSION}`}</span>
            </li>

            { settings &&
              <li className={classNames('settings-cog', 'responsive-nav-item', { active: active === 'settings' })}>
                <FontAwesomeButton
                  icon="fa-cog"
                  onClick={() => navigate('user/settings')}
                >
                  Settings
                </FontAwesomeButton>
              </li>}
            { UserStore.isPartner() &&
              <li className="swoop-help responsive-nav-item">
                <FontAwesomeButton
                  icon="fa-question-circle"
                  onClick={() => {
                    Tracking.track('Help Center Visit', {
                      category: 'Onboarding',
                    })
                    window.open('https://intercom.help/swoopme', '_blank')
                  }}
                >
                  Help
                </FontAwesomeButton>
              </li>}
            <li>
              <FontAwesomeButton
                icon="fa-power-off"
                onClick={logout}
              >
                Log Out
              </FontAwesomeButton>
            </li>
          </ul>
        </li>
      </ul>
    )
  }
}

export default onClickOutside(DropNav)
