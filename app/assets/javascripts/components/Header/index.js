import React from 'react'
import AccountStore from 'stores/account_store'
import BaseComponent from 'components/base_component'
import classNames from 'classnames'
import Consts from 'consts'
import Context from 'Context'
import Dispatcher from 'lib/swoop/dispatcher'
import FeatureStore from 'stores/feature_store'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import moment from 'moment'
import SiteStore from 'stores/site_store'
import Storage from 'stores/storage'
import UserStore from 'stores/user_store'
import { isEmpty, isEqual } from 'lodash'
import AfterHours from './AfterHours'
import CompanyLogo from './CompanyLogo'
import DropNav from './DropNav'
import HeaderLink from './HeaderLink'
import IsscStatus from './IsscStatus'
import Session from './Session'
import './style.scss'

export const DISPATCH_MENU = [
  { title: 'dashboard', href: 'dispatch' },
  { title: 'fleet', href: 'fleet' },
]

export const TOOLS_MENU = [
  { title: 'dashboard', href: 'tools' },
  { title: 'fleet', href: 'fleet' },
]

export const FLEET_MENU = [
  { title: 'dashboard', href: 'dashboard' },
]

export const ROOT_MENU = [
  { title: 'dashboard', href: 'dispatch' },
  { title: 'territories', href: 'territories', show: Consts.TERRITORIES },

  // Product has described the Map page as a "self-destruct" button;
  // hiding it from the nav
  // { title: 'map', href: 'map' },
]

export const SWOOP_DISPATCH_MENU = [
  { title: 'dashboard', href: 'dispatch' },
]

export const FLEET_SERVICE_MAP_MENU_ITEM = { title: 'map', href: 'map' }
export const INVOICING_MENU_ITEM = { title: 'invoices', href: 'invoices' }
export const REVIEW_FEED_MENU_ITEM = { title: 'reviews', href: 'reviews' }
export const REPORTING_MENU_ITEM = { title: 'reporting', href: 'reporting' }
export const STORAGE_MENU_ITEM = { title: 'storage', href: 'storage' }

const MenuItem = ({ menuItem, active, ...other } = {}) =>
  <li className={classNames({ active: menuItem.title === active })}>
    <HeaderLink {...{ menuItem }} {...other} />
  </li>

export class Header extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      path: null,
      toggle: false,
    }
    this.register(FeatureStore, FeatureStore.CHANGE)
    this.register(SiteStore, SiteStore.CHANGE)
    this.register(UserStore, UserStore.LOGIN)
    this.register(UserStore, UserStore.LOGOUT)
  }

  handleToggle = (e, open) => this.setState(prevState => ({
    toggle: open === false ? false : !prevState.toggle,
  }))

  static getDerivedStateFromProps(props, state) {
    if (props.path && state.path === null) {
      return {
        path: props.path,
      }
    }
    return null
  }

  buildMenu = () => {
    const menu = []

    if (UserStore.isPartner()) {
      menu.push(...DISPATCH_MENU)
    }

    if (UserStore.isTools()) {
      menu.push(...TOOLS_MENU)
    }

    if (UserStore.isFleet()) {
      menu.push(...FLEET_MENU)
    }

    if (UserStore.isRoot()) {
      menu.push(...ROOT_MENU)
    }

    if (UserStore.isSwoopDispatcher()) {
      menu.push(...SWOOP_DISPATCH_MENU)
    }

    if (UserStore.isFleet() && FeatureStore.isFeatureEnabled(Consts.FEATURE_FLEET_SERVICE_MAP)) {
      menu.push(FLEET_SERVICE_MAP_MENU_ITEM)
    }

    if (
      FeatureStore.isFeatureEnabled('Invoicing') &&
            ((!UserStore.isSwoop() && UserStore.hasRole('admin')) || UserStore.hasRole('root'))
    ) {
      menu.push(INVOICING_MENU_ITEM)
    }

    if (FeatureStore.isFeatureEnabled(Consts.FEATURE_REVIEW_FEED)) {
      menu.push(REVIEW_FEED_MENU_ITEM)
    }

    if (
      UserStore.isRoot() ||
            (UserStore.hasRole('admin') && !FeatureStore.isFeatureEnabled('Hide Reporting'))
    ) {
      menu.push(REPORTING_MENU_ITEM)
    }

    if (UserStore.isTesla()) {
      menu.push(STORAGE_MENU_ITEM)
    } else if (
      UserStore.isPartner() &&
              (FeatureStore.isFeatureEnabled(Consts.FEATURE_STORAGE) || SiteStore.inTireProgram())
    ) {
      menu.push(STORAGE_MENU_ITEM)
    }

    if (UserStore.isOnlyDriver()) {
      const onlyDriverMenu = menu.filter(item =>
        !isEqual(item, { title: 'reviews', href: 'reviews' }) &&
        !isEqual(item, { title: 'fleet', href: 'fleet' })
      )
      return onlyDriverMenu
    }

    return menu
  }

  handleClick = (button) => {
    if (button === 'fleet') {
      Tracking.trackDispatchEvent('Fleet Map Visit')
    } else if (button === 'reviews') {
      Tracking.track('Reviews Feed Visit', {
        category: 'Reporting',
      })
    }
    Storage.remove('#partner/dispatch-tabs')
    Storage.remove('#fleet/dispatch-tabs')
    this.setState({
      path: button,
      toggle: false,
    })
  }

  handleClickHack = () => {
    if (this.firstClickTime == null) {
      this.firstClickTime = moment()
    }

    if (this.clicks == null) {
      this.clicks = 0
    }

    if (moment().diff(this.firstClickTime) > 2000) {
      this.firstClickTime = null
      this.clicks = 0
      return this.clicks
    }
    this.clicks++

    if (this.clicks > 5) {
      this.setState({
        showSession: true,
      })
    }
  }

  logout = (e) => {
    Dispatcher.send(UserStore.LOGOUT)
    return e.preventDefault()
  }

  navigate = (path, e) => {
    if (e?.metaKey) {
      window.open(path, '_blank')
    } else {
      Context.navigate(path)
    }

    this.setState({
      toggle: false,
    })
  }

  render() {
    const { active, type, onlyShowLogo } = this.props
    const { showSession } = this.state
    const menu = this.buildMenu()
    const settings = FeatureStore.isFeatureEnabled('Settings')
    const closedDispatchSites = SiteStore.getClosedDispatchSites(this.getId(), ['name', 'force_open'])
    const session = Storage.getSession()
    const current_user = UserStore.getUser()

    return (
      <header
        className="app-header"
        onClick={this.handleClickHack}
      >
        <div className="logo-container">
          <CompanyLogo id={this.getId()} />
        </div>

        { !onlyShowLogo &&
          <div className="app-nav-container">
            <ul className={classNames('app-nav', isEmpty(current_user) ? 'no-user-logged-in' : undefined)}>
              { menu.map((menuItem) => {
                if (menuItem.show !== false) {
                  return (
                    <MenuItem
                      key={`menu-item-${menuItem.title}`}
                      {...{ menuItem, active, type }}
                      handleClick={this.handleClick}
                    />
                  )
                }
                return null
              })}
              {(showSession) && <Session {...{ session }} />}
              { settings &&
                <li className={classNames('settings-cog', { active: active === 'settings' })}>
                  <FontAwesomeButton
                    icon="fa-cog"
                    onClick={(e) => this.navigate('user/settings', e)}
                    size="medium"
                  />
                </li>}
              { UserStore.isPartner() &&
                <li className="swoop-help">
                  <FontAwesomeButton
                    icon="fa-question-circle"
                    onClick={() => {
                      Tracking.track('Help Center Visit', {
                        category: 'Onboarding',
                      })
                      window.open('https://intercom.help/swoopme', '_blank')
                    }}
                    size="medium"
                  />
                </li>}
              <IsscStatus motorClubAccounts={AccountStore.getMotorClubAccounts(this.getId())} />
              <li className="after-hours"><AfterHours sites={closedDispatchSites} /></li>
            </ul>
            <DropNav
              active={active}
              handleClick={this.handleClick}
              handleToggle={this.handleToggle}
              logout={this.logout}
              menu={menu}
              navigate={this.navigate}
              settings={settings}
              sites={closedDispatchSites}
              toggle={this.state.toggle}
              type={type}
            />
          </div>}
      </header>
    )
  }
}

export default Header
