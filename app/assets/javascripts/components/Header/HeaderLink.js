import React from 'react'
import { isEmpty } from 'lodash'
import UserStore from 'stores/user_store'
import { Link } from 'react-router5'

const HeaderLink = ({
  menuItem, type, handleClick, ...other
} = {}) => {
  if (isEmpty(menuItem)) {
    return null
  }
  const { title, href } = menuItem
  let path = type
  if (!path) {
    if (UserStore.isPartner()) {
      path = 'partner/'
    } else if (UserStore.isFleet()) {
      path = 'fleet/'
    } else {
      path = ''
    }
  }

  return (
    <Link
      onClick={() => handleClick && handleClick(href)}
      routeName={`${path}${href}`}
      {...other}
      {...{ title }}
    >
      {title}
    </Link>
  )
}

export default HeaderLink
