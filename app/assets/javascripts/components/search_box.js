
import React from 'react'
import { partial } from 'lodash'
import Button from 'componentLibrary/Button'

class SearchBox extends React.Component {
  constructor(props = {}) {
    super(props)
    this.state = {
      value: props.value || '',
      lastValue: props.value || '',
      searching: false,
      focused: false,
    }
  }

  static getDerivedStateFromProps = (newProps, prevState) => {
    if (newProps.showSearch === false || newProps.show === false) {
      return {
        value: newProps.value || '',
        lastValue: newProps.value || '',
        searching: false,
        focused: false,
      }
    }

    if (newProps.value) {
      return {
        value: (prevState && prevState.value) || newProps.value,
        lastValue: newProps.value,
        searching: prevState.searching,
        focused: prevState.focused,
      }
    } else {
      return prevState
    }
  }

  getResultsTotal() {
    let { resultTotal } = this.props

    if (!resultTotal && this.props.store != null) {
      resultTotal = this.props.store.getTotalResultSize()
    }

    return resultTotal
  }

  getValue() {
    return this.state.value
  }

  handleChange = (e) => {
    this.setState({
      value: e.target.value,
    })
  }

  setFocus = (focused) => {
    this.setState({
      focused,
    })
  }

  checkForEnter = (e) => {
    if (e.keyCode === 13) {
      this.submitValue(e)
    }
  }

  hasChanged = () => {
    let { lastValue } = this.state
    let { value } = this.state

    if (lastValue == null) {
      lastValue = ''
    }

    if (value == null) {
      value = ''
    }

    return lastValue !== value
  }

  clearSearch = (e) => {
    this.setState({
      lastValue: null,
      value: '',
    })

    this.props.onChange(null)

    e.preventDefault()
    e.stopPropagation()
  }

  submitValue = (e) => {
    if (this.hasChanged()) {
      if (!this.state.value) {
        return this.clearSearch(e)
      }

      this.setState(prevState => ({
        lastValue: prevState.value,
      }))
      this.props.onChange(this.state.value)
    }

    e.preventDefault()
    e.stopPropagation()
  }

  isFirstPage() {
    return this.props.store.pageStartAtToBeDisplayed() === 1
  }

  isLastPage() {
    return this.props.store.pageEndAtToBeDisplayed() === this.getResultsTotal()
  }

  render() {
    let { resultSize } = this.props

    if (!resultSize && this.props.store != null) {
      resultSize = this.props.store.getSizeofCurrentSearchPage()
    }

    const showPagination = this.props.show !== false && this.props.enablePagination

    const paginatable = showPagination && (
      <div className="page-controls">
        <div
          className={this.isFirstPage() ? 'disabled' : ''}
          onClick={this.props.store.handlePrevPage}
        >
          &lt;
        </div>
        <div
          className={this.isLastPage() ? 'disabled' : ''}
          onClick={this.props.store.handleNextPage}
        >
          &gt;
        </div>
      </div>
    )

    const showSearch = this.props.show !== false &&
      (this.props.showSearch == null || this.props.showSearch)

    const searchable = showSearch && (
      <div
        className={`search ${
          this.hasChanged() && this.state.value.length > 0 ?
            'search-active' :
            ''
        }`}
      >
        <input
          className={null}
          onBlur={partial(this.setFocus, false)}
          onChange={this.handleChange}
          onFocus={partial(this.setFocus, true)}
          onKeyDown={this.checkForEnter}
          placeholder="Search"
          type="search"
          value={this.state.value}
        />
        <Button
          className="search fa fa-search"
          color="green"
          disabled={!(this.hasChanged() && this.state.value.length > 0)}
          onClick={this.submitValue}
        />
      </div>
    )

    const resultTotal = this.getResultsTotal()
    return (
      <div
        className={`search-container ${
          this.props.className ? this.props.className : ''
        }`}
      >
        <div>
          {resultSize && resultSize > 0 && resultTotal != null && resultTotal > 0 ?
            <span className="result-size">
              {this.props.enablePagination ?
                `${this.props.store.pageStartAtToBeDisplayed()
                } - ${
                  this.props.store.pageEndAtToBeDisplayed()
                } of ${resultTotal}` :
                `${resultSize} of ${resultTotal}`}
            </span> :
            undefined}
          {this.state.lastValue != null && this.state.lastValue.length > 0 ?
            <div className="search-results">
              <span>
                {`search results for "${this.state.lastValue}"`}
              </span>
              <i className="fa fa-times inline" onClick={this.clearSearch} />
            </div> :
            undefined}
          {paginatable}
          {searchable}
        </div>
      </div>
    )
  }
}

export default SearchBox
