import React from 'react'
BaseComponent = require('components/base_component')
MapStore = require ('stores/map_store')
import { partial } from 'lodash'
import ReactDOM from 'react-dom'

zindex = 1
class InfoWindow extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @info = MapStore.createInfoWindow()

  componentDidMount: ->
    super(arguments...)


    #@info.setMap(@props.map)
    zindex++
    @info.setZIndex(zindex)
    @setContent()

    #This allows the component to render and size properly before showing
    setTimeout(=>
      if @_mounted
        @info.open(@props.map, this.props.marker)
    , 0)



    if @props.onInfoClose?
      if !@listener
        @listener = @info.addListener('closeclick', partial(@props.onInfoClose))

  rerender: ->
    @setContent()

  setContent: ->
    if @info? && @props.getContent?
      tree = @props.getContent(@props)
      containerDiv = document.createElement("div")
      ReactDOM.render(tree, containerDiv)
      @info.setContent(containerDiv)

  componentWillUnmount: ->
    super(arguments...)

    google.maps.event.removeListener(@listener)
    @info.close()
    @info.setMap(null)

  render: ->
    super(arguments...)

    @setContent()

    null

module.exports = InfoWindow
