import React from 'react'
BaseComponent = require('components/base_component')
MapStore = require ('stores/map_store')
import { map } from 'lodash'
import Consts from 'consts'

genericMarkerPath = Consts.GENERIC_MARKER_PATH
class Marker extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @state = {
      showInfoWindow: props.forceShowInfoWindow
      rev: 0
      marker: null
    }

  createMarker: (options) =>
    if @props.createMarker?
      @marker = @props.createMarker(options)
    else
      @marker = MapStore.createMarker(options)

    return @marker

  componentDidMount: ->
    super(arguments...)

    options = @getOptions()

    if @props.bounceTimes
      options.label = null
      options.animation = google.maps.Animation.BOUNCE
      setTimeout(=>
        @marker.setAnimation(null)
        @marker.setOptions(@getOptions())
      , 750 * @props.bounceTimes)

    @setState(marker: @createMarker(options))

    @UNSAFE_componentWillReceiveProps(@props, true)

    @marker.setMap(this.props.map)


  getIcon: (props = @props) ->
    if props.createIcon?
      return props.createIcon(props)
    else
      if props.image
        return {
          url: props.image #|| "/assets/images/marker_blue.png"
          labelOrigin: new google.maps.Point(13, 15)
        }
      else
        {
          path: genericMarkerPath,
          fillColor: props.color || "#6C6C6C",
          strokeColor: props.strokeColor || "#565656"
          fillOpacity: 1,
          anchor: new google.maps.Point(26/2,37),
          strokeWeight: 1,
          scale: 1
        }

  getOptions: (props = @props) ->

    options = {
      icon: @getIcon(props)
      label: {
        color: "#FFF"
        text: props.label || ' '
      }
    }
    if props.lat && props.lng
      options.position = {lat: props.lat, lng: props.lng}
    return options

  UNSAFE_componentWillReceiveProps: (props, force=false) ->
    if @props.forceShowInfoWindow != props.forceShowInfoWindow
      if props.forceShowInfoWindow
        @showInfoWindow()
      else
        @hideInfoWindow()

    if !props.lat? || !props.lng?
      @marker.setMap(null)
      return null

    @marker.setOptions(@getOptions(props))

    if props.children || @props.onClick
      if !@listener
        @listener = @marker.addListener('click', @props.onClick || @toggleInfoWindow)
      else if props.onClick != @props.onClick
        google.maps.event.removeListener(@listener)
        if props.onClick
          @listener = @marker.addListener('click', props.onClick)

    else if @listener
      google.maps.event.removeListener(@listener)

  handleInfoClose: =>
    if @props.onInfoWindowClickClose?
      return @props.onInfoWindowClickClose()
    @hideInfoWindow()

  hideInfoWindow: =>
    @setState
      showInfoWindow: false
    @props.onInfoChange?(false)

  showInfoWindow: =>
    @setState
      showInfoWindow: true
    @props.onInfoChange?(true)

  toggleInfoWindow: =>
    show = !@state.showInfoWindow
    @setState
      showInfoWindow: show

    @props.onInfoChange?(show)


  componentWillUnmount: ->
    super(arguments...)
    google.maps.event.clearListeners(@marker, 'click')
    @marker.setMap(null)

  render: ->
    super(arguments...)

    #if @props.getInfoContent?
    #  @marker.addListener('click', @toggleInfoWindow)
    #else
    #  google.maps.event.clearListeners(@marker, 'click')
    if @state.showInfoWindow && @state.marker
      children = @props.children
      if !Array.isArray(children)
        children = [children]

      map(children, (child) =>
        React.cloneElement(child,
          key: @marker
          map: @props.map
          marker: @marker
          onInfoClose: @handleInfoClose
        )
      )
    else
      null


module.exports = Marker
