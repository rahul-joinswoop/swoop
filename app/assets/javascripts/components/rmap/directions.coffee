BaseComponent = require('components/base_component')
MapStore = require ('stores/map_store')

class Directions extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @polyline = MapStore.createPolyLine({
      strokeColor: props.color,
      geodesic: true,
      path: [
        {lat: props.start_lat, lng: props.start_lng},
        {lat: props.end_lat, lng: props.end_lng},
      ]
    })
    #@directions = MapStore.createDirectionsRenderer
    #  preserveViewport: true
    #  suppressInfoWindows: false
    #  suppressMarkers: true
    #  polylineOptions: {
    #    strokeColor: @props.color
    #  }

  componentDidMount: ->
    super(arguments...)

    setTimeout(=>
      @polyline.setMap(@props.map)
    ,0)

  componentWillUnmount: ->
    super(arguments...)
    @polyline.setMap(null)

  getDirections:(origin, dest) =>
    #MapStore.getDirections(origin, dest, (result, status) =>
    #  if status == google.maps.DirectionsStatus.OK
    #    if @directions?
    #      @directions.setDirections(result)
    #)

  render: ->
    super(arguments...)

    #console.log("RENDERING DIRECTIONS: ", @props.start_lat, @props.start_lng, @props.end_lat, @props.end_lng)
    if !@props.start_lat || !@props.start_lng || !@props.end_lat || !@props.end_lng
      @polyline.setMap(null)
      return null
#
#
    #@getDirections({
    #  lat: @props.start_lat
    #  lng: @props.start_lng
    #}, {
    #  lat: @props.end_lat
    #  lng: @props.end_lng
    #})
#
    #@directions.setOptions(
    #  preserveViewport: true
    #  suppressInfoWindows: false
    #  suppressMarkers: true
    #  polylineOptions: {
    #    strokeColor: @props.color
    #})
#
    #@directions.setMap(@props.map)
    @polyline.setOptions({
      strokeColor: @props.color,
    })
    @polyline.setPath([
        {lat: @props.start_lat, lng: @props.start_lng},
        {lat: @props.end_lat, lng: @props.end_lng},
    ])
    @polyline.setMap(@props.map)
    null

module.exports = Directions
