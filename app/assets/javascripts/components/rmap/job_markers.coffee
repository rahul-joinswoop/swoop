import React from 'react'
require('stylesheets/components/rmap/job_markers.scss')
BaseComponent = require('components/base_component')
MapStore = require ('stores/map_store')
VehicleStore = require('stores/vehicle_store')
Marker = React.createFactory(require('components/rmap/marker'))
InfoWindow = React.createFactory(require('components/rmap/info_window'))
Directions = React.createFactory(require('components/rmap/directions'))
VehicleMarker = React.createFactory(require('components/rmap/vehicle_marker'))
JobStore = require('stores/job_store')
import Consts from 'consts'
FleetEta = React.createFactory(require 'components/fleet_eta')
UserStore = require('stores/user_store')
ACTIVE_DIRECTIONS = "#68B5DF"
INACTIVE_DIRECTIONS = "#A0A0A0"
import Utils from 'utils'
import moment from 'moment-timezone'

class JobInfoWindowContent extends BaseComponent

  render: ->
    super(arguments...)
    service = JobStore.get(@props.job_id, "service", @getId())
    status = JobStore.get(@props.job_id, "status", @getId())
    scheduled_for = JobStore.get(@props.job_id, "scheduled_for", @getId())
    rescue_company_name = JobStore.get(@props.job_id, "rescue_company.name", @getId())

    #This is just to get the correct status
    #It depends on status and scheduled_for which we have watchers for so don't pass in extra
    job = JobStore.getById(@props.job_id, false)

    div
      className: "job_info_window",
      div
        className: "status"
        "#" + JobStore.getWatchedId(@props.job_id, @getId()) + " | " + service
      div null,
        span null,
          Utils.getTransformedJobStatus(UserStore, job)
        FleetEta
          job_id: @props.job_id
          showWrapper: true
      div null,
        rescue_company_name

class TruckInfoWindowContent extends BaseComponent

  render: ->
    super(arguments...)

    if @props.vehicle_id?
      company = VehicleStore.get(@props.vehicle_id, "company", @getId())
      driver = VehicleStore.get(@props.vehicle_id, "driver_name", @getId())
      name = VehicleStore.get(@props.vehicle_id, "name", @getId())

      nameRow = name
      if driver?
        nameRow = driver + " - " + nameRow


    if !company
      company = JobStore.get(@props.job_id, "rescue_company.name", @getId())

    div
      className: "truck_info_window",
      if company?
        div
          className: "company"
          company
      if nameRow?
        div null,
          nameRow
      if @props.job_id?
        status = JobStore.get(@props.job_id, "status", @getId())
        scheduled_for = JobStore.get(@props.job_id, "scheduled_for", @getId())
        job = JobStore.getById(@props.job_id, false)

        div null,
          Utils.getTransformedJobStatus(UserStore, job) + " - #" + JobStore.getWatchedId(@props.job_id, @getId())

class DistanceToDropOff extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @info = MapStore.createInfoWindow()

  getElId: ->
    "#{@props.job_id}-distance"

  getDistance: ->
    distance = JobStore.get(@props.job_id, 'distance', @getId())
    if distance then "<div id=\"#{@getElId()}\">#{distance} miles</div>" else ''

  componentDidMount: ->
    super(arguments...)

    distance = @getDistance()
    @info.setContent(distance)

    if distance
      @mouseover = @props.marker.addListener('mouseover', => @info.open(@props.map, @props.marker))
      @mouseout = @props.marker.addListener('mouseout', => @info.close())
      @info.addListener('domready', =>
        # this is durty hack to hide close button, refactor it to use custom popup without close button
        $(".gm-style-iw:has(##{@getElId()}) + .gm-ui-hover-effect").css('display', 'none')
      )

  componentWillUnmount: ->
    super(arguments...)

    if @mouseover
      google.maps.event.removeListener(@mouseover)
      google.maps.event.removeListener(@mouseout)

    @info.close()
    @info.setMap(null)

  render: ->
    super(arguments...)
    @info.setContent(@getDistance())
    null

JobInfoWindowContent = React.createFactory(JobInfoWindowContent)
TruckInfoWindowContent = React.createFactory(TruckInfoWindowContent)
DistanceToDropOff = React.createFactory(DistanceToDropOff)

class JobMarker extends BaseComponent
  @defaultProps = {
    showDropOnZoom: 0,
  }

  constructor: (props) ->
    super(arguments...)
    @state.info_window_on_a = true

  rerender: ->
    super(arguments...)

  getJobInfowWindowContent: (props) =>
    JobInfoWindowContent
      job_id: @props.job_id

  getTruckInfoWindowContent: (props) =>
    #This is a bit confusing, we can't pass props into this method because then it would become dynamic and cause the
    #popup to rerender every time it's container renders
    #We need to setupListeners to cause the info window to rerender when it's props might have changed
    TruckInfoWindowContent
      vehicle_id: props.vehicle_id
      job_id: @props.job_id

  onPickupClick: =>
    if !@props.forceShowInfoWindow || @state.info_window_on_a
      @props.onClick?()

    @setState
      info_window_on_a: true

  onDropoffClick: =>
    if !@props.forceShowInfoWindow || !@state.info_window_on_a
      @props.onClick?()

    @setState
      info_window_on_a: false

  UNSAFE_componentWillReceiveProps: (props) ->
    super(arguments...)
    if props.forceShowInfoWindow && !@props.forceShowInfoWindow
      @setState
        info_window_on_a: true

  componentDidMount: ->
    super(arguments...)
    @listenZoomChanged()

  componentDidUpdate: ->
    super(arguments...)
    @listenZoomChanged()

  listenZoomChanged: () ->
    if @props.showDropOnZoom
      @zoomListener = @zoomListener or @props.map.addListener('zoom_changed', @rerender)
    else if @zoomListener
      google.maps.event.removeListener(@zoomListener)
      delete @zoomListener

  componentWillUnmount: () ->
    super(arguments...)

    if @zoomListener
      google.maps.event.removeListener(@zoomListener)
      delete @zoomListener

  render: ->
    super(arguments...)

    pickup_lat = JobStore.get(@props.job_id, "service_location.lat", @getId())
    pickup_lng = JobStore.get(@props.job_id, "service_location.lng", @getId())

    drop_lat = JobStore.get(@props.job_id, "drop_location.lat", @getId())
    if drop_lat?
      drop_lng = JobStore.get(@props.job_id, "drop_location.lng", @getId())

    status = JobStore.get(@props.job_id, "status", @getId())

    vehicle_id = JobStore.get(@props.job_id, "rescue_vehicle_id", @getId())

    if @vehicleTimeout?
      clearTimeout(@vehicleTimeout)

    showLiveVehicle = false
    if vehicle_id
      loc_updated_at = VehicleStore.get(vehicle_id, "location_updated_at", @getId())

      sec_diff = 1000000 #large number Math.max doesn't work


      if loc_updated_at?
        sec_diff = moment().diff(loc_updated_at, 'second')

      #The vehicle is old, so don't show it
      if sec_diff <= 5*60
        #rerender after the time is up
        @vehicleTimeout = setTimeout(@rerender, (1 + 5*60 - sec_diff)*1000)
        showLiveVehicle = true
    [
      #PICKUP PIN
      Marker
        key: "pickupPin"
        map: @props.map
        lat: pickup_lat
        lng: pickup_lng
        image: "/assets/images/marker_a.png"
        forceShowInfoWindow: @props.forceShowInfoWindow && @state.info_window_on_a
        onClick: @onPickupClick
        onInfoWindowClickClose: @props.onInfoWindowClickClose
        bounceTimes: 2
        if @state.info_window_on_a
          InfoWindow
            getContent: @getJobInfowWindowContent

      #DROP PIN
      if drop_lat? and @props.map.getZoom() >= @props.showDropOnZoom then Marker(
        key: "dropoffPin"
        map: @props.map
        lat: drop_lat
        lng: drop_lng
        forceShowInfoWindow: (@props.forceShowInfoWindow && !@state.info_window_on_a) or @props.showDistance
        onClick: @onDropoffClick
        image: "/assets/images/marker_b.png"
        bounceTimes: 2
        if @props.forceShowInfoWindow and !@state.info_window_on_a
          InfoWindow
            getContent: @getJobInfowWindowContent
        else if @props.showDistance
          DistanceToDropOff
            job_id: @props.job_id
      ),

      #A TO B LINE shown when en route
      if drop_lat? and @props.map.getZoom() >= @props.showDropOnZoom and (status != Consts.TOWING || (status == Consts.TOWING && !showLiveVehicle)) then Directions(
        key: "AtoBLine"
        map: @props.map
        start_lat: pickup_lat
        start_lng: pickup_lng
        end_lat: drop_lat
        end_lng: drop_lng
        color: if status == Consts.TOWING then ACTIVE_DIRECTIONS else INACTIVE_DIRECTIONS
      ),

      #TRUCK TO A LINE
      if vehicle_id? and status == Consts.ENROUTE and showLiveVehicle then Directions(
        key: "truckToALine"
        map: @props.map
        start_lat: VehicleStore.get(vehicle_id, "lat", @getId())
        start_lng: VehicleStore.get(vehicle_id, "lng", @getId())
        end_lat: pickup_lat
        end_lng: pickup_lng
        color: ACTIVE_DIRECTIONS
      ),

      #A TO TRUCK LINE
      if vehicle_id? and status == Consts.TOWING and showLiveVehicle then Directions(
        key: "AtoTruckLine"
        map: @props.map
        start_lat: pickup_lat
        start_lng: pickup_lng
        end_lat: VehicleStore.get(vehicle_id, "lat", @getId())
        end_lng: VehicleStore.get(vehicle_id, "lng", @getId())
        color: INACTIVE_DIRECTIONS
      ),

      #TRUCK TO B LINE
      if vehicle_id? and drop_lat? and drop_lng? and status == Consts.TOWING and showLiveVehicle then Directions(
        key: "truckToBLine"
        map: @props.map
        start_lat: VehicleStore.get(vehicle_id, "lat", @getId())
        start_lng: VehicleStore.get(vehicle_id, "lng", @getId())
        end_lat: drop_lat
        end_lng: drop_lng
        color: ACTIVE_DIRECTIONS
      ),
    ]

module.exports = JobMarker
