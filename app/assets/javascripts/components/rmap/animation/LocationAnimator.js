import { createLatLng } from 'lib/google-maps'

function scheduleLocationUpdate(callback) {
  const requestAnimationFrame = window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame

  // use requestAnimationFrame if it exists on this browser. If not, use setTimeout with ~60 fps
  if (requestAnimationFrame) {
    return requestAnimationFrame(callback)
  } else {
    return setTimeout(callback, 17)
  }
}

function cancelLocationUpdate(locationUpdate) {
  // stop possibly running animation
  if (window.cancelAnimationFrame) {
    window.cancelAnimationFrame(locationUpdate)
  } else {
    clearTimeout(locationUpdate)
  }
}

class LocationAnimator {
  animate(startLocation, endLocation, duration) {
    this.duration = duration
    this.startLocation = startLocation

    let endLocationLng = endLocation.lng()

    // crossing the 180° meridian and going the long way around the earth?
    if (Math.abs(endLocationLng - startLocation.lng()) > 180) {
      if (endLocationLng > startLocation.lng()) {
        endLocationLng -= 360
      } else {
        endLocationLng += 360
      }
    }

    this.endLocation = createLatLng(endLocation.lat(), endLocationLng)

    // stop possibly running animation
    this.stopAnimation()

    this.startTime = (new Date()).getTime()
    this.animateStep()
  }

  animateStep = () => {
    const elapsedTime = (new Date()).getTime() - this.startTime
    const durationRatio = elapsedTime / this.duration // 0 - 1

    if (durationRatio < 1) {
      const newLocation = createLatLng(
        this.startLocation.lat() + (this.endLocation.lat() - this.startLocation.lat()) * durationRatio,
        this.startLocation.lng() + (this.endLocation.lng() - this.startLocation.lng()) * durationRatio,
      )

      this.update?.(newLocation)

      this.locationUpdate = scheduleLocationUpdate(this.animateStep)
    } else {
      this.update?.(this.endLocation)
      this.complete?.(this.endLocation)
      this.locationUpdate = null
    }
  }

  onComplete(func) {
    this.complete = func
    return this
  }

  onUpdate(func) {
    this.update = func
    return this
  }

  stopAnimation() {
    cancelLocationUpdate(this.locationUpdate)
  }
}

export default LocationAnimator
