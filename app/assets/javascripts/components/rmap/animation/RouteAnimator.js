import { first } from 'lodash'
import { convertToLatLng, getDistanceBetween, computeHeading, checkIsOnPath, getDistanceToStep } from 'lib/google-maps'
import LocationAnimator from './LocationAnimator'

const SUPER_SPEED = 999 // 999 meters per second

// duration in seconds
function calcDuration(leg, step) {
  return leg.speed ?
    step.distance.value / leg.speed :
    step.duration.value
}

function calcSpeed(leg, duration) {
  // choose min value
  const time = Math.min(duration, leg.duration.value)
  return leg.distance.value / time
}

function createCustomLeg(leg) {
  const customLeg = {
    distance: { value: leg.distance.value },
    duration: { value: leg.duration.value },
    end_location: leg.end_location,
    start_location: leg.start_location,
    path: [leg.start_location],
  }

  // split leg into smaller steps for more accuracy
  customLeg.steps = leg.steps.reduce((steps, step) => {
    const speed = step.distance.value / step.duration.value

    for (let index = 0; index < step.path.length - 1; index++) {
      const startLocation = step.path[index]
      const endLocation = step.path[index + 1]

      const distance = getDistanceBetween(startLocation, endLocation)
      const duration = distance / speed

      steps.push({
        index: steps.length,
        distance: { value: distance },
        duration: { value: duration },
        end_location: endLocation,
        start_location: startLocation,
      })

      customLeg.path.push(endLocation)
    }

    return steps
  }, [])

  return customLeg
}

function sliceLeg(leg, startStep, location) {
  const slicedLeg = {
    duration: { value: 0 },
    distance: { value: 0 },
    steps: [],
  }

  location = convertToLatLng(location)

  let index, closestStep
  let minDist = Number.MAX_VALUE

  for (index = startStep; index < leg.steps.length; index++) {
    const step = leg.steps[index]

    // TODO instead of just closest step we can use projection of location on step to show more accurate location
    // these libs https://www.movable-type.co.uk/scripts/latlong.html, https://github.com/chrisveness/geodesy
    // and https://github.com/manuelbieh/geolib can be helpful
    const distanceToStep = getDistanceToStep(location, step)

    if (distanceToStep < minDist) {
      minDist = distanceToStep
      closestStep = step
    }
  }

  if (!closestStep) {
    return [null, null]
  }

  for (index = startStep; index <= closestStep.index; index++) {
    const step = leg.steps[index]

    slicedLeg.duration.value += step.duration.value
    slicedLeg.distance.value += step.distance.value
    slicedLeg.steps.push(step)
  }

  return [slicedLeg, index]
}

/**
 * RouteAnimator receives route and location update and emits animation location, heading and remaining path along specified route
 */
class RouteAnimator {
  currentLeg = null // we need currentLeg to re-start animation with super speed

  currentStep = null

  customLeg = null

  destinationStepIndex = 0

  enableSuperSpeed = false

  legs = []

  route = null

  constructor(enableSuperSpeed = false) {
    this.enableSuperSpeed = enableSuperSpeed
    this.locationAnimator = (new LocationAnimator()).onUpdate(this.emitLocation)
  }

  newLocation(route, start, end, duration) {
    // if route has changed
    if (route !== this.route) {
      this.route = route
      this.customLeg = createCustomLeg(first(route.legs))
      this.destinationStepIndex = 0
      this.stopAnimation()
    }

    const remainingPath = this.customLeg.path.slice(this.destinationStepIndex)
    if (checkIsOnPath(end, remainingPath)) {
      const [slice, index] = sliceLeg(this.customLeg, this.destinationStepIndex, end)

      if (slice) {
        this.destinationStepIndex = index + 1

        this.pushSlice(slice, duration)
      }
    }
  }

  pushSlice(slicedLeg, duration) {
    slicedLeg.speed = calcSpeed(slicedLeg, duration)

    if (this.enableSuperSpeed) {
      // increase speed of all legs in queue
      if (this.legs.length) {
        this.legs.forEach(leg => leg.speed = SUPER_SPEED)
      }

      if (this.currentLeg) {
        this.currentLeg.speed = SUPER_SPEED
        // recalculate duration of current animation to increase speed immediately on next location calculation
        this.locationAnimator.duration = calcDuration(this.currentLeg, this.currentStep) * 1000
      }
    }

    this.legs.push(slicedLeg)

    this.startNextLeg()
  }

  startNextLeg() {
    // TODO check if !this.currentLeg is really required here
    if (!this.currentLeg && this.legs.length) {
      this.currentLeg = this.legs.shift()

      // start animation recursion
      this.animateStep(this.currentLeg, 0)
    }
  }

  animateStep = (leg, index) => {
    // finish leg animation
    if (index >= leg.steps.length) {
      this.currentStep = null
      this.currentLeg = null
      return this.startNextLeg()
    }

    this.currentStep = leg.steps[index]

    const duration = calcDuration(leg, this.currentStep) * 1000

    this.locationAnimator.
    onComplete(() => this.animateStep(leg, index + 1)).
    animate(this.currentStep.start_location, this.currentStep.end_location, duration)
  }

  stopAnimation() {
    this.locationAnimator.stopAnimation()
    if (this.currentStep) {
      this.emitLocation(this.currentStep.end_location)
    }

    this.legs = []
    this.currentLeg = null
    this.currentStep = null
  }

  emitLocation = (location) => {
    let heading = null
    if (this.currentStep && !location.equals(this.currentStep.end_location)) {
      heading = computeHeading(location, this.currentStep.end_location) + 90 // add 90 to correct heading
    }

    let remaining = []
    if (this.currentStep) {
      remaining = [location, ...this.customLeg.path.slice(this.currentStep.index + 1)]
    } else if (this.customLeg) {
      remaining = this.customLeg.path.slice(this.destinationStepIndex)
    }

    this.update?.({
      heading,
      location,
      remaining,
    })
  }

  onUpdate(func) {
    this.update = func
    return this
  }
}

export default RouteAnimator
