import { Component } from 'react'
import PropTypes from 'prop-types'
import { convertToLatLng, computeHeading, getDistanceBetween } from 'lib/google-maps'
import LocationAnimator from './LocationAnimator'
import RouteAnimator from './RouteAnimator'

const FIVE_MINS = 300000 // 5 mins in milliseconds

/**
 * This component accepts truck (new truck location to be precise) and route if route presents
 * and emits new animation location, heading, and remaining path if route presents or undefined if there is no route
 *
 * If route presents truck is animated along this route if truck is going along this route, i.e. if new truck location updates
 * are on this route.
 *
 * If route does not present truck is animated directly from old location to new location.
 */
class TruckAnimator extends Component {
  static DEFAULT_DURATION = 20000

  static propTypes = {
    enableDurationCalculation: PropTypes.bool, // this is enables duration, speed calculations and super speed
    route: PropTypes.object, // route is optional
    truck: PropTypes.object.isRequired,
  }

  static calcDuration(truck, prevTruck) {
    const updatedAt = new Date(truck.location_updated_at)
    const prevUpdatedAt = new Date(prevTruck.location_updated_at)

    let duration = updatedAt.getTime() - prevUpdatedAt.getTime()
    duration = duration > FIVE_MINS ? FIVE_MINS : duration
    return duration
  }

  /**
   * @type {RouteAnimator}
   */
  animator = null

  locationAnimator = new LocationAnimator()

  constructor(props) {
    super(props)

    this.state = {
      heading: null,
      location: null,
      remaining: null,
    }

    this.animator = new RouteAnimator(props.enableDurationCalculation)
    this.animator.onUpdate(animation => this.setState(animation))
  }

  componentDidUpdate(prevProps) {
    const { enableDurationCalculation, route, truck } = this.props

    // animate if truck location has changed
    if (truck.lat !== prevProps.truck.lat || truck.lng !== prevProps.truck.lng) {
      const duration = enableDurationCalculation ?
        this.constructor.calcDuration(truck, prevProps.truck) :
        this.constructor.DEFAULT_DURATION // use old 20 seconds duration

      const start = convertToLatLng(prevProps.truck)
      const end = convertToLatLng(truck)

      if (route) {
        this.animator.newLocation(route, start, end, duration / 1000)
      } else {
        this.animateDirectly(start, end, duration)
      }
    }

    // reset truck location, heading, and remaining path to null
    if (route !== prevProps.route) {
      /* eslint-disable-next-line */
      this.setState({
        heading: null,
        location: null,
        remaining: null,
      })
    }
  }

  animateDirectly(start, end, duration) {
    const animator = this.locationAnimator

    const animate = () => {
      animator.onUpdate((location) => {
        if (!location.equals(end)) {
          const heading = computeHeading(location, end) + 90 // add 90 to correct heading
          this.setState({ heading, location })
        }
      })
      animator.onComplete(null) // reset on complete callback
      animator.animate(start, end, duration)
    }

    // if animation is in progress super speed it
    if (animator.locationUpdate) {
      const distance = getDistanceBetween(animator.startLocation, animator.endLocation)
      animator.duration = distance / 999 // 999 meters per second super speed

      animator.onComplete(animate)
    } else {
      animate()
    }
  }

  render() {
    return this.props.children(this.state)
  }
}

export default TruckAnimator
