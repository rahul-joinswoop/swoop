import React from 'react'
import { extend, map, partial } from 'lodash'
import BaseComponent from 'components/base_component'
import MapStore from 'stores/map_store'
import setupMapPadding from 'setupMapPadding'

setupMapPadding()

export MapContext = React.createContext()

class SwoopMapClass extends BaseComponent
  constructor: (props) ->
    super(props)
    @state = @getStartState()

  getStartState: ->
    return {
      staticMap: false
      rev: 0
    }

  reinitialize: =>
    if @state.staticMap
      @setState @getStartState()
      #wait for render to complete so map container is available
      setTimeout(@componentDidMount.bind(@),100)

  getMapDims: =>
    width = $(@refs.map_canvas).width()
    height = null
    if width? and !isNaN(width)
      height = $(@refs.map_canvas).height()
    else
      width = $(@refs.map_alternate).width()
      height = $(@refs.map_alternate).height()

    return  {
      width: width,
      height: height
    }

  getZoom: =>
    @state.map.getZoom()

  setZoom: (zoom) =>
    @state.map.setZoom(zoom)

  setCenter: (lat, lng) =>
    if @state.map? and lat? and lng?
      @state.map.panTo(new google?.maps.LatLng(lat, lng))

  componentDidMount: ->
    super(arguments...)

    if @boundFunc
      MapStore.unbind(MapStore.MAP_STOLEN, @boundFunc)

    theMap = @createMap(@props)

    @setState
      map: theMap

    #TODO: Get children to render

    #HACK TO GET MAP TO ALWAYS SHOW UP INSTEAD OF GRAY
    google?.maps.event.addListenerOnce(theMap, 'idle', =>
      if theMap?
        google.maps.event.trigger(theMap, 'resize')
        #TODO: may need to set proper bounds her e
        theMap.setCenter(new google?.maps.LatLng(@props.initialCenterLat || 37.7753281516829, @props.initialCenterLng || -122.414474487305))
        if @props.offsetY
          theMap.panBy(0, -@props.offsetY)
    )

    @boundFunc = partial(@unloadMap, @getId())
    MapStore.bind(MapStore.MAP_STOLEN, @boundFunc)

  fitBounds: (bounds, props) ->
    @state.map.fitBounds(bounds, props)

  unloadMap: =>
    @setState
      staticMap: true

  componentWillUnmount: ->
    super(arguments...)
    MapStore.unbind(MapStore.MAP_STOLEN, @boundFunc)

  getMapOptions: () ->
    mapOptions =
      zoom: @props.initialZoom || 13
      center: {lat: @props.initialCenterLat || 37.7753281516829, lng: @props.initialCenterLng || -122.414474487305}

  createMap: () =>
    MapStore.stealMap(@refs.map_canvas, @props.options || @getMapOptions())

  render: ->
    super(arguments...)
    style = {}

    div
      className: "GMap"
      style: extend({}, style, {
        position: 'relative'
      })
      if @state.staticMap
        div
          style: extend({}, style, {
            cursor: "pointer"
          })
          onClick: @reinitialize
          ref: "map_alternate"
          div
            style:
              backgroundImage: 'url("https://s3-us-west-1.amazonaws.com/joinswoop.static/blurry_map.png")'
              backgroundSize: 'cover'
              position: 'absolute'
              left: 0
              top: 0
              rigth: 0
              bottom: 0
              borderRadius: 3
          div
            style:
              width: 150
              height: 40
              left: "calc(50% - 75px)"
              top:  "calc(50% - 40px)"
              border: "1px solid #63B4E2"
              color: "#63B4E2"
              padding: "10px"
              background: "white"
              display: "block"
              textAlign: "center"
              position: 'absolute'
              borderRadius: 3
            "View Map"
      else
        div
          className: "map_canvas"
          ref: "map_canvas"
          style: style
      if !@state.staticMap && @state.map
        children = @props.children
        if !Array.isArray(children)
          children = [children]

        <MapContext.Provider value={@state.map}>
          {map(children, (child) =>
            if React.isValidElement(child)
              React.cloneElement(child, {
                map: @state.map,
              })
            else
              child
          )}
        </MapContext.Provider>

export default SwoopMapClass
