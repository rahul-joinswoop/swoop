import React from 'react'
BaseComponent = require('components/base_component')
MapStore = require ('stores/map_store')
SlidingMarker = require('marker-animate-unobtrusive')
Marker = React.createFactory require('components/rmap/marker')
VehicleStore = require('stores/vehicle_store')
import $ from 'jquery'
import { extend, memoize } from 'lodash'
cacheImage = new Image()
imageLoaded = false
$(cacheImage).on('load', () =>
  imageLoaded = true
)
.on('error', () => cacheImage = null  ) #TODO: retry here
.attr("src", '/assets/images/truck.png')


makeIcon = memoize((angle, url, width, height) =>
  options = options || {}
  options.width = Math.max(width, height)
  options.height = Math.max(width, height)
  canvas = document.createElement("canvas")
  canvas.width = options.width
  canvas.height = options.height
  context = canvas.getContext("2d")
  centerX = options.width/2
  centerY = options.height/2
  ctx = canvas.getContext("2d")
  ctx.clearRect(0, 0, options.width, options.height)
  ctx.save()
  ctx.translate(centerX, centerY)
  ctx.rotate(angle)
  ctx.translate(-centerX, -centerY)
  ctx.drawImage(cacheImage, 0, (width-height)/2)
  ctx.restore()
  return canvas.toDataURL('image/png')
)

createTruckImage = (rotation) ->

  scale = 1

  if !imageLoaded
    image =
      url: '/assets/images/truck.png'
      scaledSize: new google.maps.Size(50*scale, 22*scale)
      origin: new google.maps.Point(0, 0)
      anchor: new google.maps.Point(25*scale, 11*scale)
  else
    image =
      url: makeIcon((rotation+90) * Math.PI / 180 , '/assets/images/truck.png', 124, 51)
      #url: '/assets/images/truck.png'
      scaledSize: new google.maps.Size(50*scale, 50*scale)
      origin: new google.maps.Point(0, 0)
      anchor: new google.maps.Point(25*scale, 25*scale)

    return image

class VehicleMarker extends BaseComponent

  createMarker: (options) =>
    @marker = new SlidingMarker(options)
    @marker.setDuration(2000)
    @marker.setEasing("linear")
    MapStore.addMarker(@marker)
    return @marker

  createIcon: (props) =>
    if !@rot?
      @rot = Math.floor(Math.random()*360)

    if @marker?
      pos = @marker.getAnimationPosition()
      if props.lat? && props.lng? && pos?.lat? && pos?.lng?
        point2 = new google.maps.LatLng(props.lat, props.lng)
        if !@lastRotPos?
          @lastRotPos = pos

        dist = google.maps?.geometry?.spherical.computeDistanceBetween(@lastRotPos, point2)
        if dist > 5
          @lastRotPos = pos
          @rot = google.maps.geometry.spherical.computeHeading(pos, point2)

      truckImg = createTruckImage(@rot)
    return truckImg

  componentDidMount: ->
    super(arguments...)

  render: ->
    super(arguments...)

    if @props.forced_lat
      lat = @props.forced_lat
      lng = @props.forced_lng
    else
      lat = VehicleStore.get(@props.vehicle_id, "lat", @getId())
      lng = VehicleStore.get(@props.vehicle_id, "lng", @getId())


    if lat != @lat || @lng != lng
      @lastLat = @lat
      @lastLng  = @lng
    @lat = lat
    @lng = lng

    return Marker(extend({}, @props, {
      lat: lat
      lng: lng
      createIcon: @createIcon
      createMarker: @createMarker
    }), @props.children)

module.exports = VehicleMarker
