import React, { Component } from 'react'
import MapStore from 'stores/map_store'
import InfoWindow from './info_window'

class SiteMarker extends Component {
  static defaultProps = {
    showOnZoom: 0,
  }

  constructor(props) {
    super(props)

    this.state = {
      showInfoWindow: false,
    }

    this.marker = MapStore.createMarker(
      { icon: { url: '/assets/images/marker_blue_site.png' } }
    )
  }

  componentDidMount() {
    this.marker.setMap(this.props.map)
    this.marker.addListener('mouseover', () => this.setState({ showInfoWindow: true }))
    this.marker.addListener('mouseout', () => this.setState({ showInfoWindow: false }))

    this.updateZoomListener()
  }

  componentDidUpdate() {
    this.updateZoomListener()
  }

  componentWillUnmount() {
    this.marker.setVisible(false)
    this.marker.setPosition(null)
    if (this.zoomListener) {
      /* eslint-disable-next-line */
      google.maps.event.removeListener(this.zoomListener)
    }
  }

  updateVisibility = () => {
    const { map, showOnZoom } = this.props
    this.marker.setVisible(map.getZoom() >= showOnZoom)
  }

  updateZoomListener() {
    const { map, showOnZoom } = this.props
    if (showOnZoom) {
      this.zoomListener = this.zoomListener || map.addListener('zoom_changed', this.updateVisibility)
    } else if (this.zoomListener) {
      /* eslint-disable-next-line */
      google.maps.event.removeListener(this.zoomListener)
      delete this.zoomListener
    }
  }

  render() {
    const { site, map } = this.props

    this.marker.setPosition(site.location)
    this.updateVisibility()

    return (this.state.showInfoWindow ?
      /* eslint-disable-next-line */
      <InfoWindow getContent={() => site.name} map={map} marker={this.marker} /> :
      null
    )
  }
}

export default SiteMarker
