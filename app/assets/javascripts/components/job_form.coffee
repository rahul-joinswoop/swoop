import createReactClass from 'create-react-class'
import React from 'react'
import $ from 'jquery'
require('stylesheets/components/job_form.scss')
InputDriver = React.createFactory(require('components/input_driver'))
EdmundsStore = require('stores/edmunds_store')
import Consts from 'consts'
import Utils from 'utils'
import Api from 'api'
SimpleTransitionModal = React.createFactory(require 'components/modals')
TransitionModal = React.createFactory(require 'components/modals_new')
Renderer=require('form_renderer')
JobFields = require('job_fields')
Field = require('fields/field')
FormSection = React.createFactory(require('components/form_section'))
PoiStore = require('stores/poi_store').default
JobStore = require('stores/job_store')
BaseComponent = require('components/base_component')
#TODO: if a change comes in, show an error that something has changed and to refresh
CompanyStore = require('stores/company_store').default
clickTimes = 0
import moment from 'moment-timezone'
import ReactDOMFactories from 'react-dom-factories'
import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
import CoverageSectionContainerImport from 'components/JobForm/CoverageSectionContainer'
CoverageSectionContainer = React.createFactory(CoverageSectionContainerImport)
import NewPolicySearchImport from 'components/JobForm/NewPolicySearch'
NewPolicySearch = React.createFactory(NewPolicySearchImport)
PolicySearch = React.createFactory(require('components/policy_search'))
import LocationSection from 'components/JobForm/location_section'
import SmsOptions from 'components/JobForm/sms_options'
import TabElement from 'components/JobForm/tab_element'
import HistoryWidget from 'components/JobForm/history_widget'
ModalStore = require('stores/modal_store')
UsersStore = realUsersStore = require('stores/users_store').default
UserStore = realUserStore = require('stores/user_store')
PoiStore = realPoiStore = require('stores/poi_store').default
SiteStore = realSiteStore = require('stores/site_store').default
ServiceStore = realServiceStore = require('stores/service_store')
SettingStore = realSettingStore = require('stores/setting_store')
CustomerTypeStore = realCustomerTypeStore = require('stores/customer_type_store')
ServiceAliasStore = realServiceAliasStore = require('stores/service_alias_store')
FeatureStore = realFeatureStore = require('stores/feature_store')
SymptomStore = realSymptomStore = require('stores/symptom_store')
LocationTypeStore = realLoactionTypeStore = require('stores/location_type_store')
DepartmentStore = realDepartmentStore = require('stores/department_store')
QuestionStore = realQuestionStore = require('stores/question_store')
VehicleCategoryStore = realVehicleCategoryStore = require('stores/vehicle_category_store')

import { defer, extend, filter, get, isEmpty, keys, partial, sortBy, values } from 'lodash'
onClickOutside = require('react-onclickoutside').default

COVERAGE_SECTION_CONTAINER_REF = "coverage_section_container"

class ClearField extends Field
  _name: "clear"
  renderContainer: ->
    div
      className:  @getContainerClasses()
      id: "Container"+Utils.turnIntoClassName(@getName())
      style: @getContainerStyle()
  getContainerClasses: -> ''


class InfoField extends Field
  _name: "info"
  _label: ""
  getInfo: ->
    "Thank you, your service provider will be dispatched immediately and you’ll receive an ETA soon after that. Any last questions? Thank you for choosing #{UserStore.getCompany()?.name}!"
  render: (@render_props) ->
    @renderLabel()

class CustomerInfoField extends Field
  _name: "customer_info"
  _label: ""
  render: (@render_props) ->
    @renderLabel()

  getInfo: ->
    "Thank you for calling #{UserStore.getCompany()?.name?.replace("Insurance", "")} Roadside Assistance. My name is #{realUserStore.getUser().first_name}. Are you and your vehicle in a safe location?"

class CoverageInfoField extends Field
  _name: "coverage_info"
  _label: ""
  render: (@render_props) ->
    @renderLabel()
  getInfoIconText: -> "?"
  getInfoStyle: ->
    extend({}, super(arguments...), {
      color: "white",
      backgroundColor: '#9B9B9B'
    })
  getInfo: ->
    if @render_props?.info_text?
      return @render_props.info_text
    return "Insured can submit for consideration for reimbursement."


class JobForm extends BaseComponent
  displayName: 'JobForm'
  clickTimes: 0
  @defaultProps = {
    record: {}
  }
  constructor: (props) ->
    super(props)
    @lockedFields = []
    @state = {
      selectedPolicyItem: null,
      coverageIncompleteError: false,
    }

  UNSAFE_componentWillMount: () ->
    @setupForm(@props)

  resetStores: ->
    UsersStore = realUsersStore
    UserStore = realUserStore
    ServiceStore = realServiceStore
    SettingStore = realSettingStore
    CustomerTypeStore = realCustomerTypeStore
    ServiceAliasStore = realServiceAliasStore
    FeatureStore = realFeatureStore
    PoiStore = realPoiStore
    SiteStore = realSiteStore
    SymptomStore = realSymptomStore
    LocationTypeStore = realLoactionTypeStore
    DepartmentStore = realDepartmentStore
    QuestionStore = realQuestionStore
    VehicleCategoryStore = realVehicleCategoryStore

    @form?.fields.acts_as_company_id?.updateStores(@getStoreMap())

  handleScroll: ->
    clearTimeout(@scrollZoomTimeout)
    @map?.setOptions({ scrollwheel: false })
    @scrollZoomTimeout = setTimeout(=>
      @map?.setOptions({ scrollwheel: true })
    , 750)

  setCoverageIncompleteError: (coverageIncompleteError) ->
    coverageEndPointError = @.refs?[COVERAGE_SECTION_CONTAINER_REF]?.state?.coverageError
    # don't show user incomplete coverage error if there is a coverage endpoint error
    if coverageEndPointError
      coverageIncompleteError = false
    if @state.coverageIncompleteError != coverageIncompleteError
      @setState
        coverageIncompleteError: coverageIncompleteError

  cleanupBeforeSend: (newrecord) ->
    #Only people that have questions enabled should be able to edit questions
    #We need to do this because the current diff algorithm doesn't work with arrays
    #The algorithm to clean up questions in the field only works if they are showing
    if !FeatureStore.isFeatureEnabled("Questions")
      delete newrecord.question_results

    #Make sure we aren't editing job state here as it's not allowed
    delete newrecord.status
  setupForm: (props, acts_as_company_id=null) ->
    @form = new Renderer(props.record || {}, @rerender,
      form_type: props.form_type
      aboutToSend: @cleanupBeforeSend
      componentId: @getId()
    )

    if acts_as_company_id?
      @form.record.acts_as_company_id = acts_as_company_id


    @form.registerFields(JobFields) #job storage )
    @form.register(JobFields.CustomerPhoneFactory(FeatureStore.isFeatureEnabled(Consts.FEATURE_INTERNATIONAL_PHONE_NUMBERS)))
    if !acts_as_company_id?
      @resetStores()
    @form.register(ClearField, "clear")
    @form.register(InfoField, "info")
    @form.register(CustomerInfoField, "customer_info")
    @form.register(CoverageInfoField, "coverage_info")

    @form.isValid()

    setTimeout(=>
      @refs.body?.addEventListener('scroll', @handleScroll)
    , 0)

    if props.new_job
      if props.record?.service_location?.lat? and props.record?.service_location?.lng?
        setTimeout(=>
          @form.fields["service_location"].setLatLng(props.record.service_location.lat, props.record.service_location.lng)
        , 0)
      if props.record?.drop_location?.lat? and props.record?.drop_location?.lng?
        setTimeout(=>
          @form.fields["drop_location"].setLatLng(props.record.drop_location.lat, props.record.drop_location.lng)
        , 0)

    if UserStore.isSwoop() && @form.record?.id? && @form.record?.owner_company?.id?
      @reloadStores(@form.record.owner_company.id)

    if !props.record.id?
      if realUserStore.isSwoop()
        if acts_as_company_id?
          JobStore.jobAdded({acts_as_company_id:acts_as_company_id, callbacks: {success: @setPredraftJob}}, {save_as_predraft: true})
      else
        #FIX: If hack is removed backend will not process job param correctly
        JobStore.jobAdded({hack:true, callbacks: {success: @setPredraftJob}}, {save_as_predraft: true})

    if props.record.covered?
      vehicle_fields = ["policy_number", "make", "model", "year", "color", "license", "vin", "customer.full_name"]
      for field_name in vehicle_fields
        if get(props.record, field_name)?
          @lockedFields.push(field_name)

  setPredraftJob: (data) =>
    #Grab the important bits from the new job
    @form.record.id = data.id
    @form.record.status = data.status
    @form.record.type = data.type
    @rerender()


  UNSAFE_componentWillReceiveProps: (props) ->
    if props.show and !@props.show
      @setupForm(props)

    if !props.show
      @refs?.body?.removeEventListener('scroll', @handleScroll)

    if !props.show && @props.show
      @resetStores()

  callbackAfterEverything: (newrecord) =>
    if @props?.submitCallback?
      @props.submitCallback(newrecord)

  renderFooterView: =>
    [
      div
        style:
          float: "left"
          textAlign: "left"
          width: "60%"
          paddingTop: 5
          paddingLeft: 10
        if @props.show
          div
            style:
              float: "left"
              textAlign: "left"
              width: "auto"
              whiteSpace: "nowrap"
            if FeatureStore.isFeatureEnabled("SMS_Request Location") || FeatureStore.isFeatureEnabled("SMS_ETA Updates") ||  FeatureStore.isFeatureEnabled("SMS_Track Driver") || FeatureStore.isFeatureEnabled("SMS_Reviews") || FeatureStore.isFeatureEnabled(Consts.FEATURES_REVIEW_NPS_SURVEY)
              <SmsOptions FeatureStore={FeatureStore} form={@form} />
            @form.renderInputs([
              ["dispatcher_id", { containerStyle: Styles.footerStyle }]
              ["site_id", { containerStyle: Styles.footerStyle }]
            ])
      if @props.show and @form.hasAttemptedSubmit() and (!@form.isValid() || @state.coverageIncompleteError)
        div
          className: 'bubble'
          span
            style:
              color: "red"
              textAlign: "center"
              maxWidth: "140px"
            if @state.coverageIncompleteError then "Complete coverage lookup to proceed" else "Check For Errors"
      if UserStore.isFarmersOwned() and @props.show

        div
          style:
            float: "right"
            marginRight: 10
            marginTop: 5
            borderRadius: 4
            backgroundColor: "white"
            width: 35
            height: 35
            textAlign: "center"
          @form.renderInput("info",
            info_style:
              width: 24
              height: 24
              borderRadius: 12
              marginTop: 5
              marginLeft: 0
              paddingLeft: 1
              position: "relative"
              fontWeight: "bold"
              fontSize: 14
              color: "#AAA"
              display: "inline-block"
              border: "2px solid #AAA" )
    ]

  renderBody: =>
    div
      className: 'container-fluid'
      style:
        backgroundColor: "#F4F4F4"
        padding: 0
        height: "100%"
      if @props.show
        div
          className: 'row'
          style:
            margin: 0
            height: "100%"
            overflowY: (if Utils.isWebkit() then "overlay" else "auto")
          ref: "body"
          div
            className: 'col-md-6'
            style:
              height: "100%"
              padding: "10px 5px 10px 10px"
              position: "relative"
            if !@props?.release?
              @renderCustomer()
            if @shouldShowFields() && !@props?.release?
              @renderNotes()
            if @shouldShowFields() && !@props?.release?
              @renderService()
            if @shouldShowFields() && @props?.storage? || (@props?.record?.status == Consts.RELEASED && UserStore.isPartner())
              @renderReleaesInfo()
          if @shouldShowFields()
            div
              className: 'col-md-6'
              style:
                height: "100%"
                padding: "10px 10px 10px 5px"
                position: "relative"
              if UserStore.isFarmersOwned()
                @renderCoverage()
              if FeatureStore.isFeatureEnabled(Consts.FEATURE_JOB_FORM_COVERAGE) && !UserStore.isPartner()
                requiredFieldsForCoverage = []
                requiredFieldsForCoverageString = SettingStore.getSettingByKey(Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.FIELDS_REQUIRED_FOR_COVERAGE)?.value

                # we need this check, otherwise JSON.parse explodes
                if requiredFieldsForCoverageString
                  requiredFieldsForCoverage = JSON.parse(requiredFieldsForCoverageString)

                CoverageSectionContainer
                  ref: COVERAGE_SECTION_CONTAINER_REF
                  isCoverageDeterminationIncomplete: @isCoverageDeterminationIncomplete
                  setSelectedPolicyItem: @setSelectedPolicyItem
                  selectedPolicyItem: @state.selectedPolicyItem
                  setCoverageIncompleteError: @setCoverageIncompleteError
                  coverageIncompleteError: (@state.coverageIncompleteError && @form.submit_attempts > 0)
                  requiredFieldsForCoverage: requiredFieldsForCoverage
                  dropOffSites: @form?.drop_location_information?.dropOffSites
                  form: @form
                  onCoverageResponse: @onCoverageResponse
                  stores: @getStoreMap()
              if !@props?.release?
                @renderVehicle()
              if !@props?.storage?
                <LocationSection
                  FeatureStore={FeatureStore}
                  form={@form}
                  formSectionTextStyle={@formSectionTextStyle()}
                  PoiStore={PoiStore}
                  setRef={(map) =>
                      @map = map}
                  SiteStore={SiteStore}
                  UserStore={UserStore}
                />
              @renderHistory()

  onCoverageResponse: (coverage) =>
    shouldRerender = false
    shouldRunCoverageOnNewJobForm = !!SettingStore.getSettingByKey(
      Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.RUN_COVERAGE_ON_NEW_JOB_FORM_LOAD
    )

    if FeatureStore.isFeatureEnabled(Consts.ADDITION_PRIORITY_RESPONSE) and JobStore.isPredraft(@form.record) and coverage.priority?
      @form.record.priority_response = coverage.priority
      shouldRerender = true

    if (FeatureStore.isFeatureEnabled(Consts.FEATURES_FLEET_SITES) || FeatureStore.isFeatureEnabled(Consts.FEATURES_SHOW_CLIENT_SITES_TO_SWOOP)) &&
        FeatureStore.isFeatureEnabled(Consts.FEATURES_AUTO_SELECT_SITE_FROM_LOOKUP)

      site = SiteStore.getBySiteCode(coverage.site_code, @getId(), 'id')
      @form.record.fleet_site_id = site?.id || null

      if site # only lock it if it has a site, otherwise let user choose at will
        @form.fields.fleet_site_id.animate()
        @form.fields.fleet_site_id.setToDisabled()
        shouldRerender = true

    if shouldRunCoverageOnNewJobForm && !@form.record.pcc_coverage
      shouldRerender = true

    if shouldRerender then @rerender()

  renderConfirmationScreen: =>
    answers = []
    #HACK: TODO: question_results should never contain undefined values, and should fully map to questions and answers
    if @form.record?.question_results?
      for recordAnswer in @form.record.question_results
        if recordAnswer?.question_id and recordAnswer?.answer_id

          question = QuestionStore.getQuestionById(recordAnswer.question_id)
          if question
            answer = QuestionStore.getAnswerFromQuestion(question, recordAnswer.answer_id)
            if answer?
              answers.push({
                answer: answer.answer + (if recordAnswer.answer_info then " - " + recordAnswer.answer_info else "")
                question: question.question
              })

    data = {}
    data["Customer Name:"] = @form.record.customer?.full_name
    data["Customer Phone:"] = Utils.renderPhoneNumber(@form.record.customer?.phone)
    data["Pickup Location:"] = @form.record.service_location?.address
    data["Drop Off:"] = @form.record.drop_location?.address
    data["Vehicle:"] =  Utils.getVehicleName(@form.record)
    data[Utils.getSymptomLabel(UserStore, {postfix: ":"})] = SymptomStore.get(@form.record.symptom_id, "name", @getId())
    data["Service:"] = ServiceAliasStore.getServiceNameOrAlias(@form.record?.service_code_id, @form.record?.service_alias_id)
    data["Pickup Contact Name:"] = @form.record.pickup_contact?.full_name
    data["Pickup Contact Phone:"] = Utils.renderPhoneNumber(@form.record.pickup_contact?.phone)
    data["Drop Contact Name:"] = @form.record.dropoff_contact?.full_name
    data["Drop Contact Phone:"] = Utils.renderPhoneNumber(@form.record.dropoff_contact?.phone)

    for answer in answers
      data[answer.question.replace("?", ":")] = answer.answer

    return SimpleTransitionModal
      title: "Confirm Job Details"
      confirm: "Create Job"
      cancel: "Save Draft"
      onConfirm: partial(@save, skip_confirmation: true)
      onCancel: partial(@saveAsDraft, skip_confirmation: true)
      hideClose: true
      extraClasses: "confirm_job_details"
      show: true
      div
        className: 'confirmation_details'
        h2 null,
          "Please confirm these details with the customer before creating the job."
        for key, val of data
          if val? and val.length > 0
            div null,
              label null,
                key
              span null,
                val
        ReactDOMFactories.a
          onClick: =>
            #HACK: to remove back drop since we aren't closing the previous modal
            $('.modal-backdrop.fade.in').remove()
            $('body').removeClass('modal-open')

            @setState
              showConfirmationDetails: false
          "Edit Details"

  render: =>

    if @state.showConfirmationDetails
      return @renderConfirmationScreen()

    title = "Create Job"
    saveButton = "Create Job"

    if FeatureStore.isFeatureEnabled(Consts.FEATURE_CONFIRM_JOB_DETAILS)
      saveButton = "Continue"

    if @props.record? and @props.record.id? and !@props.new_job and @props.record?.status != Consts.PREDRAFT
      if @props.record?.status == Consts.DRAFT
        title = "Edit Draft"
      else
        title = "Edit Job"
        saveButton = "Save"
    if @props?.release?
      title = "Release Vehicle"
      saveButton = "Next"

    if @props.title
      title = @props.title

    saveAsDraftButton = null

    if !@props.storage and (!@props.record.id || @props.record.status == Consts.DRAFT)
      saveAsDraftButtonProps =
        text: 'Save Draft'
        className: 'cancel'
        onConfirm: @saveAsDraft

    TransitionModal extend({}, @props,
      key: "job_modal_container_modal"
      callbackKey: 'job_form_edit'
      transitionName: 'jobModalTransition'
      title: title
      rightComponent: span
        className: 'job-id'
        if @form?.record?.id then "##{@form?.record?.id}" else ""
      enableSend: true
      style:
        padding: 0
      onConfirm: @save
      cancel: if @props.secondModal then 'Skip' else null
      confirm: saveButton
      extraButton: saveAsDraftButtonProps
      extraClasses: "job_form editJob"
      footerView: if @shouldShowFields() && !@props.release then @renderFooterView()),
      @renderBody()

  isCoverageDeterminationIncomplete: () ->
    coverageEndPointError = @.refs?[COVERAGE_SECTION_CONTAINER_REF]?.state?.coverageError
    # allow user to save if there is a coverageEndPointError
    if coverageEndPointError
      return false

    return @form.record?.isCoverageLoading ||
    (!@form.record?.pcc_coverage_id && (@form.record?.customer_lookup_type != Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL)) ||
    (@form.record?.coverage?.result == 'Failed' && !isEmpty(@form.record?.coverage?.missingFields))

  save: (options = {}) =>
    saveAsDraft = options.saveAsDraft || false

    @form.setIgnoreRequired(saveAsDraft)

    coverageIncompleteError = false
    if !saveAsDraft && FeatureStore.isFeatureEnabled(Consts.FEATURE_JOB_FORM_COVERAGE) && @form?.record
      coverageIncompleteError = @isCoverageDeterminationIncomplete()
      @setCoverageIncompleteError(coverageIncompleteError)

    if @form.isValid(true) && !coverageIncompleteError
      if @props.new_job
        newrecord = @form.record
        #TODO: Perhaps go through records and call alwaysBeforeSend with newrecord
        delete newrecord.release_date
      else
        newrecord = @form.getRecordChanges()
        Tracking.trackJobEvent("Edit Job", newrecord?.id)

      if newrecord.service_location && !newrecord.service_location.lat
        if @props.record?.service_location?
          newrecord.service_location = null
        else
          delete newrecord.service_location

      if newrecord.drop_location && !newrecord.drop_location.lat
        if @props.record?.drop_location?
          newrecord.drop_location = null
        else
          delete newrecord.drop_location


      if !saveAsDraft and @form.record.status in [Consts.DRAFT, Consts.PREDRAFT] and FeatureStore.isFeatureEnabled(Consts.FEATURE_CONFIRM_JOB_DETAILS) && !options.skip_confirmation
        #HACK: to remove back drop since we aren't closing the previous modal
        $('.modal-backdrop.fade.in').remove()
        $('body').removeClass('modal-open')

        @setState
          showConfirmationDetails: true
      else
        @callbackAfterEverything(newrecord)
        if @props?.release?
          newrecord.status = Consts.RELEASED
          newrecord.sync_invoice = true

        if @props.storage
          newrecord.callbacks = {
            success: =>
              setTimeout(JobStore.reloadSearch, 0)
          }
        @props.onConfirm(newrecord, true, options.saveAsDraft)

        if @props.service == 'Storage'
          Tracking.track('Add Storage', {
            category: 'Storage'
          })
    else
      @form.attemptedSubmit()
      @rerender()

  saveAsDraft: (options={}) =>
    options.saveAsDraft = true
    if UserStore.isSwoop()
      if !@form.record.acts_as_company_id?
        options.saveAsDraft = false
    Tracking.trackJobEvent("Create Draft", @form.record?.id, {
      category: 'Dispatch'
    })
    return @save(options)

  shouldShowFields: =>
    !@state.showCustomerSpinner && !UserStore.isSwoop()

  getStoreMap: () =>
    {
      UserStore: UserStore
      UsersStore: UsersStore
      PoiStore: PoiStore
      SiteStore: SiteStore
      ServiceStore: ServiceStore
      SettingStore: SettingStore
      CustomerTypeStore: CustomerTypeStore
      ServiceAliasStore: ServiceAliasStore
      FeatureStore: FeatureStore
      SymptomStore: SymptomStore
      LocationTypeStore:LocationTypeStore
      DepartmentStore: DepartmentStore
      QuestionStore: QuestionStore
      VehicleCategoryStore: VehicleCategoryStore
    }

  reloadStores: (id) =>
    if !id?
      @resetStores()
      return

    @setState
      showCustomerSpinner: true
    Api.getCompanyView(id, {
      success: (data) =>
        #Create all the stores
        UsersStore = UsersStore.createInstance()
        UserStore = UserStore.createInstance({
          roles: ['fleet'],
          company: data.company
        })
        ServiceStore = ServiceStore.createInstance()
        SettingStore = SettingStore.createInstance()
        CustomerTypeStore = CustomerTypeStore.createInstance()
        ServiceAliasStore = ServiceAliasStore.createInstance()
        FeatureStore = FeatureStore.createInstance()
        PoiStore = PoiStore.createInstance()
        SiteStore = SiteStore.createInstance()
        SymptomStore = SymptomStore.createInstance()
        LocationTypeStore = LocationTypeStore.createInstance()
        DepartmentStore = DepartmentStore.createInstance()
        QuestionStore = QuestionStore.createInstance()
        VehicleCategoryStore = VehicleCategoryStore.createInstance()

        stores = @getStoreMap()

        #Replace stores in other stores and fields
        for storeKey, store of stores
          store.updateStores?(stores)

        #Load Data
        UsersStore.loadData(data.users)
        ServiceStore.loadData(data.services)
        SettingStore.loadData(data.settings)
        CustomerTypeStore.loadData(data.customer_types)
        ServiceAliasStore.loadData(data.service_aliases)
        FeatureStore.loadData(values(realFeatureStore.getAll()))
        PoiStore.loadData(filter(data.sites, (site) -> site.type == 'PoiSite'))
        SiteStore.loadData(filter(data.sites, (site) -> site.type == 'FleetSite'))
        SymptomStore.loadData(data.symptoms)
        DepartmentStore.loadData(data.departments)
        VehicleCategoryStore.loadData(data.vehicle_categories)

        #Update fields to act as a specific company
        @form.fields.acts_as_company_id.updateStores(stores)

        #Reset everything
        @setupForm(@props, id)

        #show everything
        @setState
          showCustomerSpinner: false
    })

  formSectionTextStyle: (styles = {}) ->
    color: "#434343"
    padding: "3px 10px 3px"
    border: "0"
    display: 'inline-block'
    fontWeight: "bold"

  delayedRerender: =>
    defer(@rerender)

  getPolicySearchValue: =>
    if @form.record.policy_search_type == "policy"
      if @form.fields.policy_number.findErrors()
        @form.fields.policy_number.setForceError(true)
        @rerender()
        return
      else
        {
          type: "policy"
          value: @form.record.policy_number
        }

  enterPolicyManually: =>
    @form.fields.policy_search_type.onChange(target: value: "manual")

  setSelectedPolicyItem: (policyItem) =>
    @setState
      selectedPolicyItem: policyItem

  onSubmitPolicy: (result) =>
    @lockedFields = []

    vehicle_fields = ["make", "model", "year", "color", "license", "vin"]
    for field_name in vehicle_fields
      if result.vehicle_obj[field_name]? && result.vehicle_obj[field_name].length > 0
        @form.fields[field_name].setValue(result.vehicle_obj[field_name], false)
        @form.fields[field_name].animate()
        @lockedFields.push(field_name)

    if result.driver_obj["name"]? && result.driver_obj["name"].length > 0
      @form.fields["customer.full_name"].setValue(result.driver_obj["name"], false)
      @form.fields["customer.full_name"].animate()
      @lockedFields.push("customer.full_name")


    #TODO: make sure this comes from the backend

    @form.record.covered = result.covered
    @form.record.exceeded_service_count_limit = result.exceeded_service_count_limit
    @form.record.pcc_coverage_id = result.id

    @rerender()

  clearCoverage: =>
     delete @form.record.covered
     delete @form.record.exceeded_service_count_limit
     delete @form.record.pcc_coverage_id

     @lockedFields = []
     @rerender()

  renderNewPolicySearch: =>
    NewPolicySearch
      ref: "new_policy_search"
      setFormSelectedPolicyItem: @setSelectedPolicyItem
      form: @form
      rerender: @rerender
      stores: @getStoreMap()

  renderPolicySearch: =>
    PolicySearch
      ref: "policy_search"
      enabled: @form.record.policy_number?.length > 0
      onShow: @delayedRerender
      onHide: @delayedRerender
      getValue: @getPolicySearchValue
      enterManually: @enterPolicyManually
      onSubmit: @onSubmitPolicy
      jobId: @form.record.id

  policySearchTypeChanged: (e) =>
    @lockedFields = []
    value = e.target.value
    if value == "manual"
      @form.record.covered = false
    else
      @form.record.covered = null

  renderCustomer: =>
    showAccountEditable = UserStore.isPartner() && (not @props.record?.account || not @props.record.owner_company? || not @props.record.rescue_company?.id? || @props.record.owner_company.id == @props.record.rescue_company.id)
    showPOEditable = (UserStore.isPartner()) and not Utils.isAssignedFromFleet(@props.record)
    fleetFirstAttrs = !UserStore.isPartner() or (UserStore.isPartner() and not Utils.isAssignedFromFleet(@props.record))

    FormSection
      title: "Customer"
      storage: @props.storage
      textStyle: @formSectionTextStyle()
      headerView: if UserStore.isFarmersOwned() then div
        className: "below"
        style:
          display: "inline-block"
          marginLeft: -5
          backgroundColor: "white"
          textAlign: "center"
        @form.renderInput("customer_info",
          info_style:
            marginTop: 5
            marginLeft: 0
            position: "relative"
            fontWeight: "bold"
            color: "#AAA"
            display: "inline-block"
            border: "2px solid #AAA" )
      form: @form
      if (UserStore.isSwoop() || @form.record.acts_as_company_id?) && (CompanyStore.loadFleetManagedIfNeeded(@getId()) || @state.showCustomerSpinner)
        Loading null
      else
        [
          @form.renderInput("acts_as_company_id", {
            containerStyle: Styles.halfStyle
            onChange: @reloadStores
          })
          if @shouldShowFields() and QuestionStore.getQuestion(Consts.QUESTION_CUSTOMER_SAFE)
            question =  QuestionStore.getQuestion(Consts.QUESTION_CUSTOMER_SAFE)
            field = @form.register(JobFields.QuestionFactory(question), "question_"+question.question_id)
            @form.renderInput(field._name, {containerStyle: Styles.halfStyle, forceShow: true, labelClassName: 'below'})

          if @shouldShowFields()
            @form.renderInputs([
              ["account", {
                containerStyle: Styles.halfStyle
                classes: if not Utils.isAssignedFromFleet(@props.record) and not FeatureStore.isFeatureEnabled("Always Show Caller") then "phoneIcon"
              }]
              ["account_notes", { containerStyle: Styles.fullStyle }]
              ["department_id", {containerStyle: Styles.halfStyle}]
              ["partner_department_id", {containerStyle: Styles.halfStyle}]
              ["fleet_site_id", { containerStyle: Styles.halfStyle }]
              ["customer_lookup_type", { containerStyle: Styles.fullStyle }]
              ["lookup_type_vin", { containerStyle: Styles.halfStyle }]
              ["lookup_type_phone_number", { containerStyle: Styles.halfStyle }]
              ["lookup_type_policy_number", { containerStyle: Styles.halfStyle }]
              ["lookup_type_unit_number", { containerStyle: Styles.halfStyle }]
              ["lookup_type_last_name", { containerStyle: Styles.smallStyle }]
              ["lookup_type_zip", { containerStyle: Styles.smallStyle }]
          ])

          if @shouldShowFields() && FeatureStore.isFeatureEnabled(Consts.FEATURE_JOB_FORM_CUSTOMER_LOOKUP)
            @renderNewPolicySearch()
          if @shouldShowFields()
            @form.renderInputs([
              if UserStore.isPartner() then "clear" else null
              ["customer_type_id", {containerStyle: Styles.halfStyle}]
              ["po_number", { containerStyle: Styles.smallStyle }]
              ["customer.member_number", { containerStyle: Styles.halfStyle}]
              ["ref_number", { containerStyle: if Utils.isEnterpriseFleetManagementJob(UserStore, @props.record) then Styles.halfStyle else Styles.smallStyle }]
              if !UserStore.isFleet() then "clear" else null
              ["caller.full_name", { containerStyle: Styles.halfStyle }]
              ["caller.phone", { containerStyle: Styles.halfStyle }]
              if !UserStore.isMTS() then "clear" else null
              ["policy_search_type", { containerStyle: Styles.halfStyle, onChange: @policySearchTypeChanged  }]
              if UserStore.isFarmersOwned() then "clear" else null
              ["policy_holder", { containerStyle: Styles.halfStyle }]
              ["policy_number", { containerStyle: Styles.halfStyle, disabled: @refs.policy_search?.state?.popupOpen || (@form.record.policy_search_type != "manual" && @form.record.pcc_coverage_id?) }]
            ])
          if @shouldShowFields() and UserStore.isFarmersOwned()
            if @form.record.policy_search_type != "manual"
              if !@form.record.pcc_coverage_id?
                @renderPolicySearch()
              else
                Button
                  color: "secondary"
                  onClick: @clearCoverage
                  style: {
                    margin: "19px 0 0 12px",
                  }
                  "Clear"
          if @shouldShowFields()
            @form.renderInputs([
              ["policy_location", { containerStyle: Styles.halfStyle }]
              "clear"
              ["customer.full_name", { containerStyle: Styles.halfStyle, disabled: "customer.full_name" in @lockedFields }]
              ["customer.phone", { containerStyle: Styles.halfStyle }]
              ["email", { containerStyle: Styles.halfStyle }]
            ])
          else
            div
              key: 'Select Client to complete Job Form'
              style:
                margin: "40px auto 70px"
                textAlign: "center"
                color: "#ccc"
              span null,
                "Select Client to complete Job Form"
        ]

  renderNotes: =>
    containerStyle = {
      margin: 0
    }
    style = {
      border: 0
      width: "100%"
      margin: 0
      maxWidth: "100%"
      padding: 6
    }
    note_cols = @form.getTabGroupCols("notes")
    if realUserStore.isSwoop() and @props.record?.id? and !@props.new_job
      @form.setFirstTab('notes', 'swoop_notes')
    FormSection
      title: "Notes"
      textStyle: @formSectionTextStyle()
      headerView: div
        style:
          float: "right"
        for col in note_cols
          if col.isShowing()
            <TabElement
              form={@form}
              key={col.getName()}
              tabName={col.getName()}
              filled={col.getValue()?.length > 0}
            >
              {col.getLabel()}
            </TabElement>
      @form.renderInputs([
        ["notes", { style: style, containerStyle: containerStyle }]
        ["fleet_notes", { style: style, containerStyle: containerStyle }]
        ["partner_notes", { style: style, containerStyle: containerStyle }]
        ["dispatch_notes", { style: style, containerStyle: containerStyle }]
        ["driver_notes", { style: style, containerStyle: containerStyle }]
        ["swoop_notes", { style: style, containerStyle: containerStyle }]
      ])

  renderService: =>
    questions = QuestionStore.getQuestionsForServiceId(@form.record.service_code_id)
    FormSection
      title: "Service"
      textStyle: @formSectionTextStyle()
      headerView: div
        style:
          float: "right"
        span null,
          @form.renderInput("second_tow", {
            style: extend({}, Styles.buttonStyle, { width: 115 }, (if @form.record.second_tow then Styles.onButtonStyle))
          }),
        @form.renderInput("will_store", {
          style: extend({}, Styles.buttonStyle, { width: 115 }, (if @form.record.will_store then Styles.onButtonStyle))
        })

      @form.renderInputs([
        ["service_time", { containerStyle: Styles.halfStyle }]
        ["scheduled_for", { containerStyle: extend({}, Styles.halfStyle, { marginBottom: 0 }) }]
        ["temp_eta_mins", {containerStyle: Styles.smallStyle}]
        ["toa.live", {containerStyle: Styles.smallStyle}]
        "clear"
        ["symptom_id", { containerStyle: Styles.halfStyle }]
        ["service", {
          containerStyle: extend({}, Styles.halfStyle, (if FeatureStore.isFeatureEnabled("Priority Response") then { width: "calc(43% - 22px)" }))
        }]
        ["priority_response", {style: backgroundColor: "transparent", border: 0, marginTop: 20, fontSize: 20, padding: '0 6px'}]
        ["invoice_vehicle_category_id", { containerStyle: Styles.halfStyle }]
        "clear"
        ["rescue_driver_id", { containerStyle: Styles.halfStyle }]
        ["rescue_vehicle_id", { containerStyle: Styles.halfStyle }]
      ])
      div
        style:
          width: "50%"
          display: "inline-block"
      @form.renderInputs([
        ["trailer_vehicle_id", { containerStyle: Styles.halfStyle }]
        ["clear", {containerStyle: (if @form.record.will_store then Styles.clearLine)}]
        ["storage_type_id", { containerStyle: Styles.halfStyle }]
        ["drop_location.site_id", {inheritFrom: "clear",  containerStyle: Styles.halfStyle }]
        ["clear", { containerStyle: (if @form.record.second_tow then Styles.clearLine)}]
        ["second_tow_dropoff", { containerStyle: Styles.halfStyle, style: marginTop: 0, marginBottom: 0 }]
        ["second_tow_scheduled_for", { containerStyle: Styles.halfStyle, bounds: @state.bounds }]
        ["clear", { containerStyle: marginBottom: 20}]

      ])
      for question in sortBy(questions, "order_num")
        field = @form.register(JobFields.QuestionFactory(question), "question_"+question.question_id)
        if question.question == "Low clearance?"
          clearance = @form.register(JobFields.ClearanceQuestionFactory(question), "question_extras_"+question.question_id)
          @form.renderInputs([
            [field._name, Styles.questionWithExtras]
            [clearance._name, Styles.questionExtras]
          ])
        else if question.question == Consts.QUESTION_CUSTOMER_SAFE
          continue
        else
          @form.renderInput(field._name, Styles.question)

  renderHistory: =>
    if not @props.record.id? || not @props.record.history? || $.isEmptyObject(@props.record.history)
      return null

    FormSection
      title: "History"
      textStyle: @formSectionTextStyle()
      style: {
        marginBottom: 230
      }
      <HistoryWidget
        form={@form}
        release={@props.release}
      />

  renderReleaesInfo: =>
    FormSection
      title: "Release Information"
      textStyle: @formSectionTextStyle()
      @form.renderInputs([
        ["storage.released_to_account_id", { containerStyle: Styles.halfStyle }]
        ["storage.released_to_user.full_name", { containerStyle: Styles.halfStyle }]
        ["storage.released_to_user.location", { containerStyle: Styles.halfStyle, style: marginTop: 0, marginBottom: 0 }]
        ["storage.released_to_user.phone", { containerStyle: Styles.halfStyle }]
        ["storage.released_to_email", { containerStyle: Styles.halfStyle }]
        ["storage.released_to_user.license_state", { containerStyle: Styles.smallStyle }]
        ["storage.released_to_user.license_id", { containerStyle: Styles.smallStyle }]
        ["storage.replace_invoice_info", {
          containerStyle: extend({}, Styles.fullStyle, { marginTop: 5 })
        }]
      ])

  renderCoverage: =>
    textColor = color = "#AAAAAA"
    text = "Lookup policy first"
    icon = null
    className: ''
    helpText = null
    miles = @map?.getLastDistanceInMiles()
    if @form.record.exceeded_service_count_limit
      textColor = color = "#FF8B00"
      text = "Not Covered"
      icon = 'fa-exclamation-triangle'
      helpText = "Event limit reached. Insured can submit to #{UserStore.getCompany()?.name?.replace("Insurance", "")} for potential consideration for reimbursement."
      className = 'not_covered'
    else if @form.record.covered and !@form.record.service
      text = "Select Service"
    else if UserStore.isFarmers() && @form.record.service == Consts.WINCHOUT and QuestionStore.getQuestionAnswer(@form.record?.service_code_id, @form.record?.question_results, "Within 10 ft of a paved surface?")?.answer == "No"
      textColor = color = "#FF8B00"
      text = "Not Covered"
      icon = 'fa-exclamation-triangle'
      helpText = "Winch outs over 10 ft not covered. Insured can submit for consideration for reimbursement."
      className = 'not_covered'
    else if @form.record.covered
      if Consts.TOW_SERVICES.indexOf(@form.record.service) > -1
        if !@form.record.service_location? || !@form.record.service_location.address?
          text = "Enter service Location"
        else if !@form.record.drop_location? || !@form.record.drop_location.address?
          text = "Enter drop off Location"
        else if miles? and ((miles < 20 and UserStore.isFarmers()) || (miles < 10 and UserStore.isCentury()))
          text = "Fully Covered"
          textColor = color = "#00B40F"
          icon = 'fa-check-circle'
          className = 'covered'
        else
          text = "Partially Covered"
          textColor = "#000"
          className = 'not_covered'
          helpText = "Policy holder is covered, but distance of tow requires an out of pocket payment from the customer"
      else
        text = "Fully Covered"
        textColor = color = "#00B40F"
        icon = 'fa-check-circle'
        className = 'covered'
    else if @form.record.covered == false || @form.record.policy_search_type == 'manual'
      textColor = color = "#FF8B00"
      text = "Not Covered"
      icon = 'fa-exclamation-triangle'
      helpText = "Insured can submit to #{UserStore.getCompany()?.name?.replace("Insurance", "")} for potential consideration for reimbursement."
      className = 'not_covered'

    FormSection
      title: "Coverage"
      className: 'coverage_section '+className
      textStyle: @formSectionTextStyle()
      div null,
        div
          style: Styles.halfStyle
          className: 'coverage below'
          label null,
            "Coverage:"
          if icon?
            ReactDOMFactories.i
              className: 'fa '+icon
              style:
                color: color
          span
            style:
              color: textColor
            text
          if helpText
            @form.renderInput("coverage_info", info_text: helpText)

            #ReactDOMFactories.i
            #  className: 'fa fa-question-circle'
            #  onClick: (e) ->
            #    ModalStore.showPopup(
            #      target: e.target
            #      offsetTop: -40
            #      offsetLeft: 45
            #      getView: -> helpText
            #    )
        if Consts.TOW_SERVICES.indexOf(@form.record.service) > -1
          div
            className: 'covered_miles'
            style: Styles.halfStyle
            label null,
              "Tow Distance:"
            if !@form.record.service_location?.address?.length > 0
              span
                style:
                  color: "#AAA"
                "Enter location"
            else if !@form.record.drop_location?.address?.length > 0
              span
                style:
                  color: "#AAA"
                "Enter drop location"
            else if miles?
              span null,
                miles+ " miles"
        #ReactDOMFactories.i
        #  className: 'fa-question fa'
        #  style:
        #    color: "#9B9B9B"
  renderVehicle: =>
    FormSection
      title: "Vehicle"
      textStyle: @formSectionTextStyle()
      if FeatureStore.isFeatureEnabled(Consts.FEATURES_VIN_FIELD_FIRST)
        @form.renderInputs([
          ["vin", { containerStyle: Styles.halfStyle, disabled: "vin" in @lockedFields }]
          ["unit_number", { containerStyle: Styles.smallStyle }]
          ["make", { containerStyle: Styles.halfStyle, disabled: "make" in @lockedFields }]
          ["vehicle_type", { containerStyle: Styles.halfStyle }]
          ["model", { containerStyle: Styles.halfStyle, disabled: "model" in @lockedFields }]
          ["year", { containerStyle: Styles.smallStyle, disabled: "year" in @lockedFields }]
          ["color", { containerStyle: Styles.smallStyle, disabled: "color" in @lockedFields }]
          ["license", { containerStyle: Styles.smallStyle, disabled: "license" in @lockedFields }]
          ["odometer", { containerStyle: Styles.smallStyle }]
          ["serial_number", { containerStyle: Styles.halfStyle }]
          ["tire_type_id", {containerStyle: Styles.halfStyle }]
        ])
      else
        @form.renderInputs([
          ["make", { containerStyle: Styles.halfStyle, disabled: "make" in @lockedFields }]
          ["vehicle_type", { containerStyle: Styles.halfStyle }]
          ["model", { containerStyle: Styles.halfStyle, disabled: "model" in @lockedFields }]
          ["year", { containerStyle: Styles.smallStyle, disabled: "year" in @lockedFields }]
          ["color", { containerStyle: Styles.smallStyle, disabled: "color" in @lockedFields }]
          ["license", { containerStyle: Styles.smallStyle, disabled: "license" in @lockedFields }]
          ["odometer", { containerStyle: Styles.smallStyle }]
          ["vin", { containerStyle: Styles.halfStyle, disabled: "vin" in @lockedFields }]
          ["serial_number", { containerStyle: Styles.halfStyle }]
          ["unit_number", { containerStyle: Styles.smallStyle }]
          ["tire_type_id", {containerStyle: Styles.halfStyle }]
        ])


container = {
  marginTop: 0
  marginLeft: 10
  marginRight: 10
  marginBottom: 10
  verticalAlign: "top"
}
halfStyle = extend({}, container, { width: "calc(50% - 20px)" })
smallStyle = extend({}, container, { width: "calc(25% - 20px)" })
fullStyle = extend({}, container, { width: "calc(100% - 20px)" })

questionLabelStyle = {
  display: "inline-block"
  width: "calc(50% - 5px)"
  verticalAlign: "middle"
  marginRight: 15
  marginBottom: 0
}

Styles = {
    container: container
    halfStyle:  halfStyle
    smallStyle : smallStyle
    fullStyle : fullStyle
    buttonStyle : {
      backgroundColor: "#AAAAAA"
      height: 20
      color: "white"
      borderRadius: 3
      margin: 4
      padding: "0 5px"
      border: "1px solid #AAA"
    }
    onButtonStyle : {
      backgroundColor: "#FFF"
      color: "#AAA"
    }
    clearLine : {
      width: "calc(100% + 20px)"
      height: 1
      backgroundColor: "#E2E2E2"
      marginBottom: 5
      marginLeft: -10
    }
    footerStyle:
      margin: "0px 5px"
      maxWidth: 175
      verticalAlign: "top"
    question:
      {
        containerStyle: extend({}, fullStyle, { marginBottom: 2 })
        labelStyle: questionLabelStyle
        style: {
          display: "inline-block"
          width: "calc(50% - 10px)"
          verticalAlign: "middle"
        }
      }
    questionWithExtras:
      {
        containerStyle: extend({}, fullStyle, { width:"calc(75% - 10px)", display: "inline-block", marginRight: 0, marginBottom: 2 })
        labelStyle: extend({}, questionLabelStyle, {width: "calc(66.6666% - 7.5px)"})
        style: {
          display: "inline-block"
          width: "calc(33% - 10px)"
          verticalAlign: "middle"
        }
      }
    questionExtras:
      {
        containerStyle: extend({}, fullStyle, { width:"calc(25% - 10px)", display: "inline-block", margin: 0 })
        labelStyle: {}
        style: {
          display: "inline-block"
          width: "100%"
          verticalAlign: "middle"
        }
      }
}

module.exports = JobForm
