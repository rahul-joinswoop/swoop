
import React from 'react'
import Utils from 'utils'

import UserStore from 'stores/user_store'
import BaseComponent from 'components/base_component'
import JobStore from 'stores/job_store'
import './style.scss'

class JobHistory extends BaseComponent {
  grabTimeFromStatus(status) {
    let time = status.last_set

    if (status.adjusted_dttm) {
      time = status.adjusted_dttm
    }

    return time
  }

  getFormattedStatus = status => Utils.formatDateTime(this.grabTimeFromStatus(status), true)

  getUsernameText = status => (UserStore.isSwoop() && this.props.hideUser !== true && (
    <td>{status.user_name}</td>
  ))

  render() {
    const history = JobStore.getVisibleHistory(
      this.props.record.id,
      this.props.showExtended,
      this.getId(),
    )

    const userText = UserStore.isSwoop() && this.props.hideUser !== true && (
      <th>User</th>
    )

    function getStatusElement(status) {
      let statusElement

      if (status.deleted_at) {
        statusElement = <del>{status.name}</del>
      } else if (status.name && status.name.indexOf('Scheduled For') > -1) {
        statusElement = 'Scheduled For'
      } else {
        statusElement = status.name
      }

      return statusElement
    }

    return (
      <table className="table-history">
        <thead>
          <tr>
            <th>
              Status
            </th>
            <th>
              Time
            </th>
            {userText}
          </tr>
        </thead>
        <tbody>
          {history.map(status =>
            <tr
              className="JobHistory-row"
              key={`history_${status.name}_${status.id}_row`}
            >
              <td>
                {getStatusElement(status)}
              </td>
              <td>
                {status.deleted_at ? <del>{this.getFormattedStatus(status)}</del> : this.getFormattedStatus(status)}
              </td>
              {this.getUsernameText(status)}
            </tr>,
          )}
        </tbody>
      </table>
    )
  }
}

export default JobHistory
