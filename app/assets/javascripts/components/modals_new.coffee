import createReactClass from 'create-react-class'
import React from 'react'
import ReactDOM from 'react-dom'
import Logger from 'lib/swoop/logger'
import { extend, partial, isString } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import FontAwesomeButtonImport from 'componentLibrary/FontAwesomeButton'
FontAwesomeButton = React.createFactory(FontAwesomeButtonImport)

BootstrapModal = React.createFactory(createReactClass(
  displayName: 'BootstrapModal'
  getDefaultProps: ->
    extraClasses: " "
    updateConfirmButton: null

  getInitialState: () ->
    @modal = React.createRef()
    return null

  componentDidMount: ->
    # When the component is added, turn it into a modal
    $(@modal.current).modal
      backdrop: 'static'
      keyboard: false
      show: true
    return

  componentWillUnmount: ->
    $(@modal.current)
      .off('hidden', @handleHidden)
      .removeClass('fade')
      .modal('hide')

  handleCancel: (button, event) ->
    if @props.closeOnBodyClick && event.target.className.indexOf?('prevent-modal-close') != -1
      return
    Logger.debug "modal cancel clicked", @props.onCancel,  @props.callbackKey
    if @props.onCancel
      @props.onCancel @props.callbackKey, button
    return

  handleConfirm: ->
    if @props.onConfirm and (not @props.enableSend? || @props.enableSend)
      @props.onConfirm @props.callbackKey
    return

  render: ->
    #Logger.debug "inside modals", @props

    extraButton = null
    cancelButton = null
    confirmButton = null

    if @props.extraButton
      extraButton = Button
        className: (@props.extraButton.className || 'primary')
        disabled: if @props.enableSend == false then true else false
        onClick: @props.extraButton.onConfirm
        style: {
          margin: '5px 10px 5px 0'
        }
        @props.extraButton.text

    if @props.cancel
      cancelButton = Button
        color: 'cancel'
        onClick: partial(@handleCancel, true)
        style: {
          margin: '5px 10px 5px 0'
        }
        @props.cancel

    if @props.updateConfirmButton
      confirmButton = @props.updateConfirmButton
    else if @props.confirm
      confirmButton = Button
        className: 'confirm-button'
        color: 'primary'
        disabled: if @props.enableSend == false then true else false
        onClick: @handleConfirm
        style: {
          margin: '5px 10px 5px 0'
        }
        @props.confirm

        if @props.confirmError
          div
            className: 'error-bubble-container'
            div
              className: 'bubble'
              span null,
                if isString(@props.confirmError) then @props.confirmError else "Check For Errors"

    div
      className: 'modal fade modal-new ' + (if @props.fullHeightModal then 'full-height' else '') + ' ' + @props.extraModalClasses
      style: extend({padding: 0}, @props.style)
      onClick: (event) => @props.closeOnBodyClick && @handleCancel(false, event)
      ref: @modal
      div
        className: 'modal-dialog'
        div
          className: 'modal-content' + ' ' + @props.extraClasses
          div
            className: 'modal-header'
            if @props.rightComponent
              @props.rightComponent
            if not @props.hideClose
              FontAwesomeButton
                className: "modal-close-button"
                color: "secondary-o"
                icon: "fa-times"
                onClick: !@props.closeOnBodyClick && partial(@handleCancel, false)
            h3 null,
              @props.title
          div
            className: 'modal-body'
            @props.children
          div
            className: 'modal-footer '
            style:
              zIndex: 3
            if @props.footerView
              @props.footerView
            extraButton
            cancelButton
            confirmButton
))

TransitionModal = createReactClass(
  displayName: 'TransitionModal'

  getDefaultProps: ->
    component: 'div'
    transitionName: 'test'
    confirm: 'Confirm'
    cancel: 'Cancel'
    confirmError: null
    onConfirm: ''
    onCancel: ''
    hideClose: false
    show: false
    fullHeightModal: true
    extraButton: null,
    extraModalClasses: ''

  render: ->
    div
      key: "transition_modal_"+@props.transitionName
      if @props.show
        BootstrapModal
          callbackKey: @props.callbackKey
          confirm: @props.confirm
          confirmError: @props.confirmError
          cancel: @props.cancel
          onCancel: @props.onCancel
          enableSend: @props.enableSend
          onConfirm: @props.onConfirm
          extraButton: @props.extraButton
          extraClasses: @props.extraClasses
          extraModalClasses: @props.extraModalClasses
          style: @props.style
          footerView: @props.footerView
          title: @props.title
          hideClose: @props.hideClose
          closeOnBodyClick: @props.closeOnBodyClick
          fullHeightModal: @props.fullHeightModal
          rightComponent: @props.rightComponent
          @props.children
)

module.exports = TransitionModal
