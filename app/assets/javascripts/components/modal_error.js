import React from 'react'
import TransitionModal from 'components/modals'

const ErrorModal = props => (
  <TransitionModal
    {...props}
    callbackKey="error"
    confirm="Refresh"
    extraClasses="error_modal"
    transitionName="errorModalTransition"
  >
    {props.error}
  </TransitionModal>
)

export default ErrorModal
