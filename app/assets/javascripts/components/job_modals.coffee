import createReactClass from 'create-react-class'
import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
JobForm = React.createFactory(require('components/job_form'))
InputDriver = React.createFactory(require 'components/input_driver')
import Utils from 'utils'
NotificationStore = require ('stores/notification_store')
import AssignModalImport from 'components/modals/AssignDriver'
AssignModal = React.createFactory(AssignModalImport)
JobStore = require('stores/job_store')
FeatureStore = require('stores/feature_store')
InvoiceStore = require ('stores/invoice_store')
RejectReasonStore = require('stores/reject_reason_store')
BaseComponent = require('components/base_component')
import Consts from 'consts'
import { extend } from 'lodash'

JobFormModal = createReactClass(
  displayName: 'JobFormModal'

  render: ->
    JobForm @props
)

import AcceptModalImportNew from 'components/modals/AcceptJob.js'
AcceptModalNew = React.createFactory(AcceptModalImportNew)


class RejectModal extends BaseComponent
  displayName: 'RejectModal'

  constructor: (props) ->
    super(props)

    @state = {
      reject_reason: 0
      error: null
      show_eta: false
      show_textarea: false
    }

    super(arguments...)
    NotificationStore.removeNotification("##{props.record?.id} is available for ETA submissions")

  onConfirm: =>
    val = @state.reject_reason
    if val == 0 and @showRejectReasons()
      @setState
        error: "Must select reason"
      return

    obj =
      id: @props.record.id
      status: Consts.REJECTED
      reject_reason_id: @state.reject_reason

    if !@showRejectReasons()
      delete obj.reject_reason_id

    if val == 3
      obj.reject_reason_info = @state.ETA

    if val == 5
      obj.reject_reason_info = @state.reason_text

    @props.onConfirm obj

    Tracking.trackDispatchEvent('Reject Job', {
      source: if @props.record?.account?.name == 'Agero' then 'RSC' else 'ISSC'
    })

  showRejectReasons: () ->
    return UserStore.isPartner()

  onChangeReason: (e) =>
    show_eta = false
    show_textarea = false
    val = parseInt(e.target.value)
    error = null
    if val == 0
      error = "Must select reason"
    else if val == 3
      show_eta = true
    else if val == 5
      show_textarea = true

    @setState
      reject_reason: val
      show_eta: show_eta
      show_textarea: show_textarea
      error: error

  handleEtaChange: (e) =>
    @setState
      ETA: e.target.value.replace(/\D/g,'')

  handleTextChange: (e) =>
    @setState
        reason_text: e.target.value

  renderReasonSelector: =>
    account_name = JobStore.getAccountNameByJobId(@props.record?.id, @getId())
    select
      className: 'form-control'
      name: 'reject_reason'
      value: @state.reject_reason
      onChange: @onChangeReason
      option
        value: 0
        className: 'option_header'
        '- Select -'

      for o in RejectReasonStore.getRejectReasonsForJob(@props.record, @getId(), JobStore)
        option
          value: o.id
          key: o.id
          className: 'option_row'
          o.name

  render: ->
    super(arguments...)

    company_name = @props.record?.owner_company?.name
    if @props.record?.type == "FleetManagedJob"
      company_name = "Swoop"
    TransitionModal extend({}, @props,
      callbackKey: 'reject'
      transitionName: 'rejectModalTransition'
      confirm: 'Reject Job'
      title: Utils.getJobId(@props.record) + ": Reject Job"
      extraClasses: "reject_modal"
      onConfirm: @onConfirm
      cancel: 'Cancel'),
      span
        className: 'inline-block'
        'Are you sure you want to reject this job from '+company_name
        if @showRejectReasons()
          div
            className: "reason_container"
            span null,
              "Reason:"
            @renderReasonSelector()
            if @state.error?
              label
                className: 'error'
                @state.error
            if @state.show_eta
              [
                span null,
                  "ETA: "
                input
                  onChange: @handleEtaChange
                  value: @state.ETA
                  placeholder: "MIN"
              ]
            else if @state.show_textarea
              textarea
                className: 'form-control'
                onChange: @handleTextChange
                value: @state.reason_text

UnsetRescueCompanyModal = createReactClass(
  displayName: 'UnsetRescueCompanyModal'
  onConfirm: ->
    @props.onConfirm
      id: @props.record.id
      status: Consts.REASSIGNED

  render: ->
    TransitionModal extend({}, @props,
      callbackKey: 'unsetRescueCompany'
      transitionName: 'unsetRescueCompanyModalTransition'
      confirm: 'Reassign'
      title: Utils.getJobId(@props.record) + ": Reassign"
      extraClasses: "reject_modal"
      onConfirm: @onConfirm
      cancel: 'Cancel'),
      (
        name = @props.record?.rescue_company?.name
        if name and name.slice(-2) != ("’s") and name.slice(-2) != ("'s")
          name += "'s"
        span
          className: 'inline-block'
          "Please confirm you’d like to remove this job from "+name+" queue and reassign to another towing company."
      )

)

module.exports = [JobFormModal, AcceptModalNew, AssignModal, RejectModal, UnsetRescueCompanyModal]
