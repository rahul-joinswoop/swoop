import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
AccountStore = require ('stores/account_store')
StoreSelector = React.createFactory(require('components/storeSelector'))
BackendSuggest = React.createFactory require('components/autosuggest/backend_suggest').default
BaseComponent = require('components/base_component')
import { keys, partial } from 'lodash'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class AccountPickerModal extends BaseComponent
  displayName: 'AccountPickerModal'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()
    AccountStore.clearAllWithoutRatesList()

  onConfirm: =>
    if @state.account_id != null
      @props.onConfirm
        id: @state.account_id
        copy: @state.copy
    else
      @setState
        error: "Choose Account"

  getInitialState: ->
    account_id: null
    account_name: null
    error: null
    copy: true

  getAccounts: =>
    if !@props.show
      return {}

    AccountStore.getAllWithoutRatesForStore(@getId())

  getAccountsLength: =>
    keys(@getAccounts()).length

  handleAccountChange: (id, value) =>
    @setState
      account_id: id
      account_name: value
      error: null

  handleCopyChange: (id, e) =>
    obj = {error: null}
    obj[id] = e.currentTarget.checked
    @setState obj

  handleChange: (e) =>
    @setState
      email: e.target.value
      error: null

  render: ->
    super(arguments...)

    accounts_length = @getAccountsLength()

    TransitionModal $.extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'AccountModalTransition'
      confirm: if accounts_length > 0 then 'Add' else "OK"
      title: "Add Account"
      extraClasses: "account_picker_modal"
      onConfirm: if accounts_length > 0 then @onConfirm
      cancel: if accounts_length > 0 then 'Cancel' else null),
      if AccountStore.LOADING_ALL_IDS_WITHOUT_RATES
        div
          style:
            width: '100%'
            display: 'block'
            textAlign: 'center'
            marginTop: 10
            marginBottom: 10
          Loading null
      else if accounts_length == 0
        "Rates already added for all accounts"
      else
        div null,
          div null,
            "Choose an Account you’d like to add rates for."
          StoreSelector
            value: @state.account_name
            onChange: @handleAccountChange
            populateFunc: @getAccounts
          div
            className: "type_items copy"
            input
              id: "copy"
              name: "copy"
              type: "checkbox"
              checked: @state["copy"]
              onChange: partial(@handleCopyChange, "copy")
            label
              htmlFor: "copy"
              "Copy over existing rates from Default Service when adding new Services."
          label
            className: 'error'
            @state.error

module.exports = AccountPickerModal
