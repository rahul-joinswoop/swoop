import PhoneNumber from 'lib/phonenumber'
import Api from 'api'
import createReactClass from 'create-react-class'
import React from 'react'
import Logger from 'lib/swoop/logger'
TransitionModal = React.createFactory(require 'components/modals')
Geosuggest = React.createFactory(require('components/geosuggest/Geosuggest').default)
import Utils from 'utils'
StoreSelector = React.createFactory(require('components/storeSelector'))
Autosuggest = React.createFactory(require('components/autosuggest/autosuggest'))
import { bind, difference, extend, get, isEmpty, isFunction, omit, partial, values, without } from 'lodash'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)

#TODO: if a change comes in, show an error that something has changed and to refresh
ADDRESS_ERROR = "Select an exact address or drop a pin with the pin icon."

recordForm = createReactClass(
  displayName: 'recordForm'
  loading: {}
  getInitialState: ->
    Logger.debug("----------> PASSED STATE TO FORM ", @props.record)
    @record = $.extend(true, {}, @props.record)
    @orig_record = $.extend(true, {}, @props.record)
    @refresh = not @props.show



    @fillSelectors(@props)
    @fillDefaults(@props)
    @fixData(@props)
    return {
      rev: 1
      errors: {}
      attrs: {}
      classes: {}
      checked: @getInitialChecked(@props)
      tabs: @getInitialTabs(@props)
      defaultLat: 37.775
      defaultLon: -122.419
    }

  getCols: (props) ->
    cols = without(props.cols, null)
    if props.getExtraCols?
      cols = cols.concat(props.getExtraCols(@record))
    return cols


  getInitialChecked: (props) ->
      checked = {}
      if props?
        for column in @getCols(props)
         for col in column.fields
           if col.type == "toggle"
             if @getValue(props.record, col.clear)? or col.checked == true
               checked[col.name] = true
      return checked

  getInitialTabs: (props) ->
    tabs = {}
    if props?
        for column in @getCols(props)
         for col in column.fields
           if col.type == "tab"
              shown = true
              if col.show?
                if typeof col.show == "function"
                  shown = col.show(@record)
                else
                  shown = col.show
              if shown and (not tabs[col.tabGroup] or (@getValue(props.record, col.prop)? and @getValue(props.record, col.prop).length > 0))
                tabs[col.tabGroup] = col.name
      return tabs

  fillDefaults: (props) ->
    if isEmpty(props.record)
      for column in @getCols(props)
         for col in column.fields
            if col.default?
              onChange = if col.onChange then bind(col.onChange, @) else @handleChange
              if col.type == "autosuggest"
                onChange = partial(@handleAutosuggestChange, @getOnChange(col.valueType), col.name)
                onChange(col.default)
              else
                onChange(target: name: col.name, value: col.default)
  fillSelectors: (props) ->
    #fill in any selectors with their initial value
    if isEmpty(props.record)
      for column in @getCols(props)
         for col in column.fields
            shown = true
            if col.show?
              if typeof col.show == "function"
                shown = col.show(@record)
              else
                shown = col.show
           if col.type == "selector" and shown
            onChange = if col.onChange then bind(col.onChange, @) else @handleChange
            options = col.options
            if typeof col.options == "function"
              options = col.options(props.record, col)
            if options and options.length > 0
              o = options[0]
            if typeof o == "object"
              value = o.value
              text = o.text
            else
              value = o
              text = o

            onChange(target: name: col.name, value: value, true)

  fixData: (props) ->
    for column in @getCols(props)
      for col in column.fields
        if typeof col.fixData == "function"
          col.fixData(@record, col)

  refreshOnProps: () ->
    @refresh = true

  prefill: (props) ->
    for column in @getCols(props)
      for col in column.fields
        if typeof col.prefill == "function"
          bind(col.prefill, @)()#(@record, col)

  UNSAFE_componentWillReceiveProps: (props) ->

    Logger.debug("components will recieve props")
    if not props.show
      @refresh = true
    else if @refresh or (isEmpty(@record) and not isEmpty(props.record))
      @record = $.extend(true, {}, props.record)
      @orig_record = $.extend(true, {}, props.record)
      @refresh = false
      @loading = {}
      @fillSelectors(props)
      @fillDefaults(props)
      @fixData(props)
      @prefill(props)
      @setState
        errors: {}
        attrs: {}
        classes: {}
        tabs: @getInitialTabs(props)
        checked: @getInitialChecked(props)

    #Don't want the record to overwrite changes the dispatcher doesn't know about
    #else
      #@orig_record = $.extend(true, {}, props.record)

  getDefaultProps: ->
    submitButtonName: 'Submit'

  componentDidMount: ->
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition(@updateDefaultLocation)


  componentWillUnmount: ->

  updateDefaultLocation: (position) ->
    if position.coords.latitude and position.coords.longitude
      @setState
        defaultLat: position.coords.latitude
        defaultLon: position.coords.longitude

  recordsChanged: ->
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously



  onAddressChange: (name, suggest) ->
    if typeof @getValue(@record, name) == "undefined"
      @setValue(@record, name, {})
    if suggest.location?
      @setValue(@record, name+".lat", suggest.location.lat)
      @setValue(@record, name+".lng", suggest.location.lng)


    @setValue(@record, name+".place_id", suggest.placeId)
    @setValue(@record, name+".address", suggest.label)

    if suggest.exact
      @setValue(@record, name+".exact", suggest.exact)
    else
      @setValue(@record, name+".exact", false)

    if suggest.forceError
      @setError(name, ADDRESS_ERROR)
    else if suggest.location?
      @setError(name)
    else if suggest.label == ""
      @setError(name)

    #street = ""
    #if suggest.gmaps? and suggest.gmaps.address_components?
    #  for comp in suggest.gmaps.address_components
    #    switch comp.types[0]
    #      when "street_number"
    #        street += comp.short_name + " "
    #      when "route"
    #        street += comp.short_name
    #      when "locality"
    #        @record[type].city = comp.short_name
    #      when "administrative_area_level_1"
    #        @record[type].state = comp.short_name
    #      when "postal_code"
    #        @record[type].zip = comp.short_name
    #@setValue(@record, name+".street", street)


  clearLocation: (location, value) ->
    if @record.location?.location_type_id
      @setValue(@record, location, {location_type_id: @record.location.location_type_id})
    else
      @setValue(@record, location, null)
    @setError(name)

  getValue: (obj, path) ->
    get(obj, path)

  setValue: (obj, path, val) ->
    if val? and val == ""
      val = null
    Utils.setValue(obj, path, val)
    Logger.debug("set ", path," to ",val, obj)


  setError: (name, error) ->
    errors = @state.errors
    if error?
      errors[name] = error
    else
      delete errors[name]
    @setState
      errors: errors

  setLoading: (name, bool) ->
    if bool
      @loading[name] = bool
      @addClass(name, "loading")
    else
      delete @loading[name]
      @removeClass(name, "loading")

  addClass: (name, c) ->
    classes = @state.classes
    if not classes[name]
      classes[name] = ""
    classes[name] += " "+c+" "
    @setState
      classes: classes

  removeClass: (name, c) ->
    classes = @state.classes
    if not classes[name]
      return
    classes[name] = classes[name].replace(" "+c+" ", "")
    @setState
      classes: classes

  removeAllClasses: (name) ->
    classes = @state.classes
    if not classes[name]
      return
    classes[name] = ""
    @setState
      classes: classes


  checkUsername: (e) ->
    username = e.target.value
    name = e.target.name
    if username != @orig_record[name]
      if username.length == 0
        @addClass(name, "error")
        @removeClass(name, "complete")
        @setError(name, "You must set a username")
      else
        @setLoading(name, true)
        Api.checkUsername(username, {
          success: (data) =>
            @setLoading(name)
            if data.found and (not data.user_id or data.user_id != @orig_record.id)
              @addClass(name, "error")
              @removeClass(name, "complete")
              @setError(name, "Sorry this username is already taken")
            else
              @removeClass(name, "error")
              @addClass(name, "complete")
              @setError(name)
          error: (data) =>
            @setLoading(name)
            @setError(name)
          }
        )
    else
      @setLoading(name)
      @removeClass(name, "error")
      @addClass(name, "complete")
      @setError(name)


  setrecordData: (strName, value, silent) ->
    @setValue(@record, strName, value)
    if not silent? or not silent
      @recordsChanged()


  onDateChange: (name, value) ->
    @setrecordData(name, value)
    @setError(name)

  onDateError: (name) ->
    @setError(name, "Time is out of acceptable range")

  handleChange: (e, silent) ->
    @setrecordData(e.target.name, e.target.value, silent)

  handleCheck: (e) ->
    @setrecordData(e.target.name, e.target.checked)

  handleUppercaseChange: (e) ->
    @setrecordData(e.target.name, e.target.value.toUpperCase())

  handlePriceChange: (e) ->
    @setrecordData(e.target.name, e.target.value.replace(/[^0-9\-\.]/g,''))

  handleFloatChange: (e) ->
    @setrecordData(e.target.name, e.target.value.replace(/[^0-9]/g,''))

  handlePhoneChange: (e) ->
    number = e.target.value.replace(/[^0-9]/g, '')
    #input.setSelectionRange(selectionStart, selectionEnd)
    sel = e.target.selectionStart
    if number.length < 4

    else if number.length < 7
      number = number.substr(0, 3) + '-' + number.substr(3, 3)
      if sel==4
        sel++
    else
      number = number.substr(0, 3) + '-' + number.substr(3, 3) + '-' + number.substr(6, 4)
      if sel==8
        sel++
      else if sel == 4
        sel++

    @setrecordData(e.target.name, number)
    if e.target?
      target = e.target
      target.setSelectionRange(sel, sel)
      setTimeout ->
        target.setSelectionRange(sel, sel)
      , 0
  handleCapitalizeChange: (e) ->
    if e.target.value?
      @setrecordData(e.target.name, e.target.value.substring(0, 1).toUpperCase() + e.target.value.substring(1))
    else
      @setrecordData(e.target.name, null)
  handleLowerCaseChange: (e) ->
    @setrecordData(e.target.name,  $.trim(e.target.value.toLowerCase()))

  handleNumberChange: (e) ->
    if e.target.value?
      @setrecordData(e.target.name,  e.target.value.replace(/\D/g,''))
    else
      @setrecordData(e.target.name, '')

  handleErrorClear: (e) ->
    @removeAllClasses(e.target.name)
    @setError(e.target.name)
    @setrecordData(e.target.name, e.target.value)

  onPasswordFocus: (e) ->
    if e.target.value == "********"
      obj = @state.attrs
      obj[e.target.name] = "hide_stars"
      @setState
        attrs: obj

  onPasswordBlur: (e) ->
    if e.target.value == ""
      obj = @state.attrs
      delete obj[e.target.name]
      @setState
        attrs: obj


  isValidPhone: (obj, name) ->
    if obj? and @getValue(obj, name)?
      if not PhoneNumber.validate(@getValue(obj, name))
        return false
      #else
        #@setValue(obj, name, pn.getNumber())
        #@setState
        #  rev: @state.rev++

    return true

  isValidEmail: (email) ->
    Utils.isValidEmail(email)

  isValid: ->
    isValid = true
    errors = {}
    for column in @getCols(@props)
      for col in column.fields
        #make sure all required props are filled in
        shown = true
        if col.show?
          if typeof col.show == "function"
            shown = col.show(@record)
          else
            shown = col.show

        if col.validateVin and shown
          if col.getValue
            value = col.getValue(@record, col.name)
          else
            value = @getValue(@record, col.name)
          if value? and value.length > 0
            if not Utils.validateVin(value)
              isValid = false
              errors[col.name] = 'Not a valid vin'

        if col.required and shown
          required = true
          if typeof col.required == "function"
            required = col.required(@record)
          if required
            if col.getValue
              value = col.getValue(@record, col.name)
            else
              if col.type == "address"
                value = @getValue(@record, col.name+".address")
              else if col.type == "password"
                #require original record to exist or require a password
                value = @getValue(@record, col.name)
                if not value and @orig_record.id
                  value = true
              else
                value = @getValue(@record, col.name)
            if not value || value == ""
              if col.errorText?
                errors[col.name] = col.errorText
              else
                errors[col.name] = 'This is a required field.'
              isValid = false
            else
              if col.requiredLength? and value.length != col.requiredLength
                errors[col.name] = 'Not a valid '+col.label+'.'
                isValid = false

        if shown
          if col.isValid?
            error = bind(col.isValid, @, col)()

            if error?
              errors[col.name] = error
              isValid = false

          else if col.type == "phone"
            #check phone number
            if !@isValidPhone(@record, col.name)
              errors[col.name] = 'Invalid phone number'
              isValid = false

          else if col.type == "float"
            value = @getValue(@record, col.name)
            if value? and typeof value == "string" and (value.indexOf(",") > -1 || value.indexOf(".") > -1)
             errors[col.name] = ", and .'s not supported"
             isValid = false
          else if col.type == "email"
            if !@isValidEmail(@getValue(@record, col.name))
              errors[col.name] = 'Invalid email'
              isValid = false
          else if col.type == "password"
            #check password requirements
            value = @getValue(@record, col.name)
            if value and value.length > 0 and value.length < 8
              errors[col.name] = 'Password is too short'
              isValid = false
          else if col.type == "address"
            name = col.name
            if @refs[name]?
              isLoadingfunction = @refs[name].isLoading || this.refs[name].instanceRef.isLoading

              if isLoadingfunction()
                errors[name] = "Address is still loading"
                isValid = false

            #check to make sure we have locations for service_location
            if @getValue(@record, col.name)? and (not col.send_all_addresses) and (not @getValue(@record, col.name+".lat") or not @getValue(@record, col.name+".lng"))
              location = @getValue(@record, col.name)

              if('location_type_id' in location)
                errors[col.name] = ADDRESS_ERROR
                isValid = false

          else if col.type == "datetime"
            if @state.errors? and @state.errors[col.name]
              errors[col.name] = @state.errors[col.name]
              isValid = false
          else if col.type == "username"
            name = col.name
            if @loading[name]
              errors[name] = "Username is being checked, please submit again"
              isValid = false
            if @state.errors[name]
              isValid = false
              errors[name] = @state.errors[name]
          else if col.type == "storeSelector"
            name = col.name
            if @state.errors[name]
              isValid = false
              errors[name] = @state.errors[name]

    @setState
      errors: errors

    if !isValid
      Logger.debug("Failed because of ", errors)
    isValid

  #compareArrs: (newObj, arr1, arr2) ->

  addMissingIds: (obj1, obj2) ->
    if typeof obj1 is "object" && typeof obj2 is "object" and obj1 != null and obj2 != null and obj1.constructor != Array
      if not obj1.id?
        for prop of obj1
          @addMissingIds(obj1[prop], obj2[prop])
        obj1.id = obj2.id

  compareObjs: (newObj, obj1, obj2) ->
    if obj1 and obj1.constructor == Array
      #if obj is an array (TODO: currently this assumes all primitive values in an array will fail for arrays of objects)

      for elem1 in obj1
        if typeof elem1 is "object"
          if not elem1.id?
            if not newObj?
              newObj = []
            newObj.push(elem1)
          else
            if obj2? and obj2.constructor == Array
              for elem2 in obj2
                if not elem2? or elem1.id == elem2.id
                  ret = @compareObjs({}, elem1, elem2)
                  if not isEmpty(ret)
                    ret.id = elem2.id
                    if not newObj?
                      newObj = []
                    newObj.push(ret)
                  break
        else
          if not obj2 || difference(obj1, obj2).length > 0 || difference(obj2, obj1).length > 0
            newObj = obj1
    else
      #if obj1 is an object
      for prop of obj1
        if typeof obj1[prop] is "object" && obj1[prop] != null
          if typeof newObj[prop] is "undefined"
            if obj1[prop].constructor == Array
              newObj[prop] = @compareObjs(null, obj1[prop], obj2?[prop])
              if newObj[prop] == null
                delete newObj[prop]
            else
              newObj[prop] = @compareObjs({}, obj1[prop], obj2?[prop])
              if $.isEmptyObject(newObj[prop])
                delete newObj[prop]

        else
          if (typeof obj2 == "undefined" or obj2 == null) or (typeof obj2[prop] == "undefined" or obj2[prop] == null and obj1[prop] != null) || obj1[prop] != obj2[prop]
            newObj[prop] = obj1[prop]
    return newObj

  handleCancel: (e) ->
    if @props.onCancel?
      @props.onCancel()

    @setState @getInitialState
    e.preventDefault()

  handleRecordFormSubmit: (e) ->
    e.preventDefault()

    #give unfocus listeners a chance to do their thing
    setTimeout ( =>
      newrecord = {}

      if @isValid()
        #Copies over record diffs
        #TODO: this is only one level deep right now, need to make it recursive/loop over props
        #also it doesn't work with arrays
        for column in @getCols(@props)
          for col in column.fields
            if typeof col.beforeSend == "function"
              col.beforeSend(@record, col)

        if @props.forceFullObject? and @props.forceFullObject
          newrecord = @record
        else
          newrecord = @compareObjs({}, @record, @orig_record)

          @addMissingIds(newrecord, @orig_record)
          #Ensures if a piece of the service location changes the new location row has all the info
          for column in @getCols(@props)
            for col in column.fields
              if col.type == "address"
                obj = @getValue(newrecord, col.name)

                if obj?
                  extend(obj, @getValue(@record, col.name))
                  if obj['id']
                    delete @getValue(newrecord, col.name)["id"]
                  delete obj.state
                  delete obj.city
                  delete obj.street
                  delete obj.zip
                  delete obj.updated_at

                if (@record.location?.location_type_id? && not newrecord.location?.location_type_id)
                  if not newrecord['location']?
                    newrecord['location'] = { 'location_type_id': null }

                if col.send_all_addresses != true && (not newrecord.location?.lat? || not newrecord.location?.lng?)
                  newrecord['location'] = null

              else if col.type == "phone"
                obj = @getValue(newrecord, col.name)
                if obj
                  @setValue(newrecord, col.name, "+1"+obj.split("-").join(""))

              if col.inheritOnChange?
                if @getValue(newrecord, col.inheritOnChange)?
                  @getValue(newrecord, col.inheritOnChange).id = @getValue(@record, col.inheritOnChange).id

        if @props.ignoreFields?
          for field in @props.ignoreFields
            names = field.split(".")
            if names.length == 1
              if newrecord[names[0]]?
                delete newrecord[names[0]]
            else if names.length == 2
              if newrecord[names[0]]? && newrecord[names[0]][names[1]]?
                delete newrecord[names[0]][names[1]]
            else if names.length == 3
              if newrecord[names[0]]? && newrecord[names[0]][names[1]]? && newrecord[names[0]][names[1]][names[2]]?
                delete newrecord[names[0]][names[1]][names[2]]

        for column in @getCols(@props)
          for col in column.fields
            if typeof col.aboutToSend == "function"
              if Utils.hasKey(newrecord, col.name)
                col.aboutToSend(newrecord, col)
            if typeof col.alwaysBeforeSend == "function"
              col.alwaysBeforeSend(@record, newrecord, col)

        if @props?.submitCallback?
          callback = @props.submitCallback

        if @props.aboutToSend?
          @props.aboutToSend(newrecord)

        @props.onConfirm(newrecord)

        if callback?
          callback(newrecord)

    ), 100

  handleChangeStoreSelector: (name, id, value) ->
    if value? and value.length > 0
      if not id?
        @setError(name, "Please select account")
      else
        @setError(name)
      @setrecordData(name, {
        id: id
        name: value
      })
    else
      @setrecordData(name, null)
      @setError(name)

  handleAutosuggestChange: (handler, name, val) ->
    handler
      target:
        name: name
        value: val

  handleTabChange: (e, group, name) ->
    obj = @state.tabs
    obj[group] = name
    @setState
      tabs: obj


  handleToggleChange: (e, id, names) ->

    if not Array.isArray(names)
      names = [names]

    obj = @state.checked
    obj[id] = if not obj[id]? then true else !obj[id]
    if !obj[id]
      for name in names
        @setrecordData(name, null)
        @setError(name)
        if obj[name]? and obj[name]
          handleToggleChange(e, name, [])

    @setState
      checked: obj

  refreshMaps: () ->

  turnIntoClassName: (name) ->
    Utils.turnIntoClassName(name)
  renderInput: (col) ->
    getValue = @getValue

    if col.getValue
      getValue = col.getValue

    enabled = col.enabled
    if col.enabled? and typeof col.enabled == "function"
      enabled = enabled(@record)

    if !enabled?
      enabled = true

    if col.type == "address" and (not enabled? or enabled == true)
      addressProps =
        placeholder: 'Select address or click pin'
        onSuggestSelect: partial(@onAddressChange, col.name)
        ref: col.name
        className: 'InputVehicleAddress geosuggest-input address'
        name: col.name
        onChange: partial(@clearLocation, col.name)
        initialValue: ""
        initialLat: null
        initialLng: null
        location: new google.maps.LatLng(@state.defaultLat, @state.defaultLon)
        always_show_map: if typeof col.always_show_map == "function" then col.always_show_map(@record) else col.always_show_map
        send_all_addresses: col.send_all_addresses
        hide_map: if typeof col.hide_map == "function" then col.hide_map(@record) else (if col.hide_map? then col.hide_map else false)
        onMapOpen: ->
          if col.onMapOpen?
            col.onMapOpen(col)

      if @record and @getValue(@record, col.name)?
        addressProps.initialValue = @getValue(@record, col.name+".address")
        addressProps.initialLat = @getValue(@record, col.name+".lat")
        addressProps.initialLng = @getValue(@record, col.name+".lng")


      Geosuggest addressProps
    else if col.type == "selector"
      inputProps =
        type: 'text'
        className: 'form-control'
        name: col.name
        style: col.style
        id:  'Input'+@turnIntoClassName(col.name)
        value: getValue(@record, col.name)
        onChange: if col.onChange then bind(col.onChange, @) else @handleChange
        disabled: !enabled
        readOnly: !enabled
      options = col.options
      if typeof col.options == "function"
        options = col.options(@record, col)

      index = 1
      select inputProps,
        for o in options
            if typeof o == "object"
              value = o.value
              text = o.text
            else
              value = o
              text = o

            option
                key: "option_"+text+index++
                value: if not value? then '' else value
                className: 'option_row'
                text
    else if col.type == "storeSelector"
      StoreSelector extend({}, col,
        onChange:  if col.onChange then bind(col.onChange, @, col.name) else partial(@handleChangeStoreSelector, col.name) #TODO: MAKE THIS A GENERAL CASE
        value: getValue(@record, col.name)
        sortFunc: col.sortFunc
      )
    else if col.type == "autosuggest"
      props = extend({}, col,
        onSuggestSelect:  partial(@handleAutosuggestChange, @getOnChange(col.valueType), col.name) #TODO: MAKE THIS A GENERAL CASE
        initialValue: getValue(@record, col.name)
      )

      if typeof props.getSuggestions == "function"
        props.suggestions = props.getSuggestions(@record)

      Autosuggest props
    else if col.type == "button"
      text = col.text
      if col.getText?
        text = bind(col.getText, @, col.name)()
      Button
        className: 'form-control'
        onClick: if col.onClick? then bind(col.onClick, @, @props.record, col)
        text
    else if col.type == "custom"
      if col.getView?
        bind(col.getView, @)(@record, @state.errors, col)
      else
        div null
    else if col.type == "span"
      if col.value?
        @setValue(@record, col.name, col.value)
      span null,
        getValue(@record, col.name)
    else
      domObject = input
      inputProps =
        type: 'text'
        className: 'form-control'
        name: col.name #record[driver_attributes[user_attributes[first_name]]] value="<%=@record.driver.user.first_name%>" required
        id:  'Input'+@turnIntoClassName(col.name)
        value: getValue(@record, col.name)
        onChange: if col.onChange then bind(col.onChange, @) else @getOnChange(col.type)
        placeholder: col.placeholder
        maxLength: col.maxLength
        autoComplete: 'off'
        style: col.style

      if inputProps.value == null
        inputProps.value = ""

      if col.onBlur?
        inputProps.onBlur = bind(col.onBlur, @)

      if col.type == "phone"
        if not inputProps.placeholder?
          inputProps.placeholder = "xxx-xxx-xxxx"
          val = Utils.renderPhoneNumber(inputProps.value)
          if val?
            inputProps.value = val
          else
            inputProps.value = ""

      if col.type == "address" and !enabled
        inputProps.value = @getValue(@record, col.name+".address")
      if col.type == "password"
        #if not getValue(@record, col.name) and not @state.attrs[col.name]
        #  inputProps.value = "********"
        inputProps.ref = col.name
        inputProps.type = "password"
        #inputProps.onFocus = @onPasswordFocus
        #inputProps.onBlur = @onPasswordBlur


      if col.type=="username"
        inputProps.onBlur = @checkUsername

      if col.type == "number"
        inputProps.type = "number"
        if col.step?
          inputProps.step = col.step

      if col.type == "textarea"
        domObject = textarea

      if enabled == false
        inputProps.readOnly = true

      if col.type == "checkbox"
        inputProps.type = "checkbox"
        inputProps.className = "form-checkbox"
        inputProps.checked = getValue(@record, col.name)? and getValue(@record, col.name)

        if enabled == false
          inputProps.disabled = true

      if col.forceType?
        inputProps.type = col.forceType

      if col.pattern?
        inputProps.pattern=col.pattern

        # real time pattern validation implementation
        if col.realTimePattern
          # save original value
          inputProps['data-original-value'] = inputProps.value

          # add patter validation on each change event
          originOnChange = inputProps.onChange
          inputProps.onChange = (e) =>
            @realTimePatternValidation(e)
            originOnChange(e)

      if col.inputWrapperClass
        if isFunction(col.inputWrapperClass)
          inputWrapperClass = col.inputWrapperClass(@record, col)
        else
          inputWrapperClass = col.inputWrapperClass

      return div
        className: "inputWrapper #{inputWrapperClass || ''}" + (if @state.classes[col.name] then @state.classes[col.name] else "")
        domObject inputProps

  # prev value var is used because we already have new changed value
  # in case new value is invalid we need to restore prev valid value
  realTimePatternValidation: (e) ->
    # initialize prev val with initial value
    if !e.target.dataset.prevVal?
      e.target.dataset.prevVal = e.target.dataset.originalValue

    # test if new value is bad
    if !(new RegExp(e.target.pattern)).test(e.target.value)
      # don't save new changes and restore prev valid value
      e.target.value = e.target.dataset.prevVal
    else
      # save valid value
      e.target.dataset.prevVal = e.target.value

  getOnChange: (type) ->
    if type == "lowercase" || type=="email" || type=="username"
      return @handleLowerCaseChange

    if type == "capitalized"
      return @handleCapitalizeChange

    if type=="username"
      return @handleErrorClear

    if type == "year"
      return @handleNumberChange

    if type == "checkbox"
      return @handleCheck

    if type == "uppercase"
      return @handleUppercaseChange

    if type == "float"
      return @handleFloatChange

    if type == "phone"
      return @handlePhoneChange

    return @handleChange

  updateErrorsImmediately: (col, i, errors) ->
      error = col.isValid(@record)
      if error && !@state.errors[col.name] && col.show(@record)
        errors[col.name] = error
        @setState {errors: errors}
      else if !error && @state.errors[col.name]
        errors = omit(errors, [col.name])
        @setState {errors: errors}

  renderFormGroup: (col, i, errors) ->
    render = true
    labelName = col.label
    if typeof col.label == "function"
      labelName = col.label(@record, col)

    if col.type == "toggle"

      div
        className: "form-group toggle_control "+(if col.classes? then col.classes else "")
        key: i
        input
          type: 'checkbox'
          id: "toggle" + @turnIntoClassName(col.name)
          className: 'form-checkbox'
          checked: @state.checked[col.name]
          onChange: partial(@handleToggleChange, partial.placeholder, col.name, col.clear)
        label
          htmlFor: "toggle" + @turnIntoClassName(col.name)
          labelName
    else if col.type == "tab"
      classes = "tab_control" + (if @state.tabs[col.tabGroup] == col.name then " active" else "")
      #TODO: CAN ONLY GRAB PROPERTIES WITHOUT A getValue function
      value = @getValue(@record, col.prop)
      if value? and value.length > 0
        classes += " filled"

      div
        className: classes
        onClick:  partial(@handleTabChange, partial.placeholder, col.tabGroup, col.name)
        key: i
        id: "tab" +  @turnIntoClassName(col.name)
        labelName

    else if (not col.toggleName and not col.tabName) || @state.checked[col.toggleName] ||  col.tabName in values(@state.tabs)
      classes = 'form-group'
      if col.classes
        classes += " "+col.classes
      if col.type == "checkbox"
        classes += " checkbox_control"
      if col.toggleName?
        classes += " toggleable"
      if col.tabName?
        classes += " tabable"
      div
        className:  classes
        key: "Container"+@turnIntoClassName(col.name)
        id: "Container"+@turnIntoClassName(col.name)
        style: col.containerStyle

        if col.type != "clear" and labelName?
          label
              htmlFor: 'Input'+@turnIntoClassName(col.name)
              onClick: if col.labelClick then bind(col.labelClick, @, @props.record)
              className: 'control-label'
              if col.customLabel
                col.customLabel
              else
                labelName

        if col.type not in ["clear", "label"]
          @renderInput(col)


        if errors? and errors[col.name]? and col.type not in ["clear", "label"]
            label
                className: 'error'
                errors[col.name]

  renderContainer: ->
    for col, i in @getCols(@props)
      div
        key: i
        className: "column_group" + (if col.classes then " "+col.classes else "")
        style: col.style
        if col.label?
          h1
            className: "group_title"
            onClick: if col.labelClick then bind(col.labelClick, @, @props.record)
            if col.customLabel?
              col.customLabel
            else
              col.label
        for group, i in col.fields
          errors = @state.errors
          if group.updateErrorsImmediately
            @updateErrorsImmediately(group, i, errors)
          if not group.show? or (typeof group.show == "function" and group.show(@record)) or (typeof group.show == "boolean" and group.show)
            @renderFormGroup(group, i, errors)

  renderForm: ->
    div
      className: "form_container"
      input
        style: display:"none"
        type:"text"
        name:"fakeusernameremembered"
      input
        style: display:"none"
        type:"password"
        name:"fakepasswordremembered"
      if @props.renderContainer?
        bind(@props.renderContainer, @)()
      else
        @renderContainer()

      br null
      div
        className: "buttons"

        if @props.showCancel? and @props.showCancel
          Button
            color: 'cancel'
            label: 'Cancel'
            onClick: @handleCancel
            style: {
              marginLeft: 10,
              marginRight: 10
            }
            type: 'submit'

        if @props.saveComponent?
          bind(@props.saveComponent, @)()
        else if not @props.showSubmit? or @props.showSubmit
          Button
            color: 'primary'
            disabled: if @props?.loading then true else false
            label: @props.submitButtonName
            onClick: if !@props?.loading then @handleRecordFormSubmit
            type: 'submit'
        if @state.errors and not isEmpty(@state.errors)
          label
            className: 'main error'
            "Check form for errors"
  renderModal: ->
    if @props.record?.id
      cbkey = 'record_form_edit'
    else
      cbkey = 'record_form_new'

    if @props.callbackKey
      cbkey = @props.callbackKey

    TransitionModal extend({}, @props,
      callbackKey: cbkey
      transitionName: 'recordFormModalTransition'
      cancel: ''
      confirm: ''
      enableSend: !@props?.loading? || @props.loading==false
      onSubmit: @handleRecordFormSubmit
      extraClasses: "form editmodal" + (if @props.extraClasses then " " + @props.extraClasses else "" )),
      @renderForm()

  render: ->
    if @props.renderModal
      bind(@props.renderModal, @)()
    else
      @renderModal()
)

module.exports = recordForm
