import React, { useEffect, useState } from 'react'
import { IntlProvider } from 'react-intl'
import Storage from 'stores/storage'
import { ModalContextProvider } from 'componentLibrary/Modal'
import { SplitIOProvider } from 'components/split.io'
import { useStoreBind } from 'stores/storeHooks'
import UserStore from 'stores/user_store'

const Application = (props) => {
  const { children } = props
  const locale = Storage.get('language') || 'en'
  const [modal, setState] = useState(null)
  useStoreBind(UserStore, UserStore.LOGIN)
  useStoreBind(UserStore, UserStore.LOGOUT)

  function showModal(m) {
    setState(m)
  }

  function hideModals() {
    setState(null)
  }

  const user = UserStore.getUser()
  let splitId = user?.id
  // If we don't have a user id yet, generate a unique id that's stored in the browser
  if (!splitId) {
    splitId = localStorage.getItem('temporarySplitId')
    if (!splitId) {
      splitId = Date.now()
      localStorage.setItem('temporarySplitId', splitId)
    }
  }
  const attributes = {}
  if (user && user.company) {
    attributes.company_id = user.company.id
  }

  useEffect(() => {
    if (window.Localize && user) {
      window.Localize.setLanguage(user.language)
    }
  }, [user])

  return (
    <SplitIOProvider attributes={attributes} id={splitId}>
      <IntlProvider {...{ locale }}>
        <ModalContextProvider value={{ hideModals, showModal, modalShowing: !!modal }}>
          {children}
          {modal}
        </ModalContextProvider>
      </IntlProvider>
    </SplitIOProvider>
  )
}

export default Application
