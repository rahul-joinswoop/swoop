import React from 'react'
import SettingStore from 'stores/setting_store'
import { mount, shallow } from 'enzyme'
import { NotificationsWidget } from '.'

describe('<Notifications /> Component', () => {
  let component

  const notifications = [
    {
      type: 'info',
      message: 'The car is crashed.',
      dismissable: true,
      timeout: 5000,
      id: 0,
    },
    {
      type: 'info',
      message: 'The man is red.',
      dismissable: true,
      timeout: 5000,
      id: 1,
    },
  ]

  SettingStore.isAllLoaded = jest.fn()
  SettingStore.isAllLoaded.mockReturnValue(true)

  it('Renders the component', () => {
    component = shallow(<NotificationsWidget />)
    expect(component).toBeDefined()
  })

  it('Does not render if empty', () => {
    component = shallow(<NotificationsWidget />)
    expect(component.find('.component-notifications').exists()).toBe(false)
  })

  it('Renders notifications', () => {
    component = mount(<NotificationsWidget notifications={notifications} />)
    expect(component.find('.component-notifications').exists()).toBe(true)
    expect(component.find('li')).toHaveLength(2)
  })
})
