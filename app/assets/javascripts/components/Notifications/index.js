import React from 'react'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import Dispatcher from 'lib/swoop/dispatcher'
import NotificationStore from 'stores/notification_store'
import SettingStore from 'stores/setting_store'
import StoreWrapper from 'StoreWrapper'
import './style.scss'

const Notification = ({ notification }) => {
  const canBeDisabled = n => !n.type || [Consts.INFO, Consts.WARNING].indexOf(n.type) >= 0

  const closeNotification = (e) => {
    const id = e.target.getAttribute('data-id')
    Dispatcher.send(NotificationStore.CLOSE_NOTIFICATION, { id })
  }

  const isEnabled = n => !(SettingStore.getSettingByKey(Consts.SETTING_DISABLE_NOTIFICATIONS) && canBeDisabled(n))

  const formatMessage = (msg) => {
    if (typeof msg === 'string') {
      return <span className="message">{msg}</span>
    }
    return msg
  }

  if (!isEnabled(notification)) {
    return null
  }

  return (
    <li
      className={notification.type || Consts.INFO}
      key={notification.id}
    >
      {formatMessage(notification.message)}
      { notification.dismissable !== false &&
        <span
          className="close_notification"
          data-id={notification.id}
          onClick={closeNotification}
          role="button"
        >
          Dismiss
        </span>}
    </li>
  )
}

class NotificationsWidget extends BaseComponent {
  state = {
    rev: 0,
  }

  generalChanged = () => this.setState(prevState => ({
    rev: prevState.rev + 1,
  }))

  render() {
    if (!SettingStore.isAllLoaded(this.getId())) {
      return null
    }

    if (this.props.notifications && this.props.notifications.length > 0) {
      return (
        <ul className="component-notifications notification_list">
          { this.props.notifications.map(notification => (
            <Notification key={notification.id} notification={notification} />
          ))}
        </ul>
      )
    }
    return null
  }
}

NotificationsWidget.defaultProps = {
  notifications: {},
}

const Notifications = StoreWrapper(NotificationsWidget, function getProps() {
  return {
    notifications: Object.values({ ...NotificationStore.getNotifications(this.getId()) }),
  }
}, ({
  constructor() {
    this.register(NotificationStore, NotificationStore.CHANGE)
  },
}))

export { NotificationsWidget }
export default Notifications
