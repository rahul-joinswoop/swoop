import React from 'react'
import RcInputNumber from 'rc-input-number'
import Logger from 'lib/swoop/logger'
FTable = React.createFactory(require 'components/resizableTable/FTable')
import JobDetailsImport from 'components/job_details'
JobDetails = React.createFactory(JobDetailsImport.default)
Countdown = React.createFactory(require 'components/countdown')
UserStore = require('stores/user_store')
InvoiceStore = require('stores/invoice_store')
ProviderStore = require('stores/provider_store')
TireStore = require('stores/tire_store')
TireTypeStore = require('stores/tire_type_store')
import Utils from 'utils'
InputNumber = React.createFactory(RcInputNumber)
SiteStore = require('stores/site_store').default
BaseComponent = require('components/base_component')
import { extend, keys, map, partial } from 'lodash'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class TireList extends BaseComponent
  displayName: 'TireList'

  constructor: (props) ->
    super(props)
    @state = {
      spinners: []
    }

  rerender: =>
    @setState
      spinners: []

    super(arguments...)

  onChange: (siteId, typeId, tireQuantity, value) =>
    if UserStore.isTesla()
      # component id is not passed to method call because this is an onChange callback
      companyId = ProviderStore.getPropertyBySiteId(siteId, 'company').id
    else
      companyId = SiteStore.get(siteId, 'company')?.id

    if value < tireQuantity
      TireStore.decreaseTireQuantity(siteId, typeId, companyId)
    else if value > tireQuantity || !tireQuantity # tireQuantity == null means there are no tires for the site
      TireStore.addItem(
        site_id: siteId
        company_id: companyId
        item: {
          tire_type_id: typeId
          type: "Tire"
        }
      )
    else
      return

    spinners = @state.spinners.concat([@getSiteTireKey(typeId, siteId)])
    @setState
      spinners: spinners

  getSiteTireKey: (typeId, siteId) =>
    siteId+"_"+typeId

  renderNumberInput: (type, site_id) =>
    if UserStore.isPartner() or UserStore.isAdmin()
      if @getSiteTireKey(type.id, site_id) in @state.spinners
        Loading
          size: 24
          onClick: Utils.preventPropogation
      else
        tireQuantity = TireStore.getTireQuantityBySiteAndType(site_id, type.id, @getId()) || 0

        InputNumber
          min: 0
          value: tireQuantity
          onKeyDown: (e) =>
            # only allow up/down arrow keys
            e.preventDefault() unless e.which == 38 or e.which == 40
          onChange: partial(@onChange, site_id, type.id, tireQuantity)
    else
      if site_id
        tireQuantity = TireStore.getTireQuantityBySiteAndType(site_id, type.id, @getId())
        span null,
          tireQuantity

  render: ->
    super(arguments...)

    totalWidth = 200
    cols = [
      {header: "Location", fixedWidth: true, width: totalWidth, key: "location", func: (siteId) =>
        if siteId?
          if UserStore.isTesla()
            providerName = ProviderStore.getPropertyBySiteId(siteId, 'name', @getId())

            if providerName
              return providerName

            providerCompanyName = ProviderStore.getPropertyBySiteId(siteId, 'company', @getId())?.name

            if providerCompanyName
              return providerCompanyName
          else
            return SiteStore.get(siteId, 'name', @getId())
      }
    ]

    types = TireTypeStore.getAll(false, @getId())
    names = {}

    for id,type of types
      # check if any site has these tires
      Logger.debug("tire check, ", type, type.id, @props.tire_ids)
      if not TireStore.partnerHasSiteWithType(type.id, @getId()) and (not @props.tire_ids? or type.id not in @props.tire_ids) and UserStore.isPartner()
        continue

      name = TireTypeStore.getBaseTireTypeName(type.id, @getId())

      top_width = 140
      if $(window).width() < 1100
        top_width = 92

      width = (if type.extras?.length > 0 then top_width else 92)
      if not names[type.car]?
        names[type.car] = width
      else
        names[type.car] += width
      totalWidth += width
      cols.push(
        {
          header: name
          key: name
          width: width
          sortable: false
          fixedWidth: true
          className: "numberPicker"
          func: partial(@renderNumberInput, type)
        }
      )

    if UserStore.isTesla()
      itemsIds = map(ProviderStore.getTireProgramProviders(@getId(), ['name', 'company']), 'site_id')
    else
      itemsIds = map(SiteStore.getTireProgramSites(@getId(), ['name', 'company']), 'id')

    Logger.debug("itemsIds: ", itemsIds)

    setTimeout =>
      if @refs?.table?
        #Hack to make it adjust currectly
        @refs?.table?.refs?.table?.refs?.table?.windowResize?()
    , 0

    if TireStore.LOADING_TIRE_BY_SITE_AND_TYPE_LIST
      Loading null
    else
      div
        className: "tire_list"
        div
          className: "car_names"
          for name in keys(names)#.reverse()
            div
              key: name
              style: width: names[name]
              name
        div
          style:
            width: totalWidth
          FTable extend({}, @props,
            ref: "table"
            maxWidth: totalWidth
            switchWidth: 0
            rows: itemsIds
            cols: cols
            resizable: false
            sortKey: "location"
            sortDir: 1
            )

module.exports = TireList
