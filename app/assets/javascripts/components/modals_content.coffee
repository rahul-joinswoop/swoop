import React from 'react'
import ReactDOMFactories from 'react-dom-factories'
import Consts from 'consts'
import { extend, partial } from 'lodash'
BaseComponent = require('components/base_component')
import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import FontAwesomeButtonImport from 'componentLibrary/FontAwesomeButton'
FontAwesomeButton = React.createFactory(FontAwesomeButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class ModalContent extends BaseComponent
  @defaultProps =
    confirm: 'Confirm'
    cancel: 'Cancel'
    showCancel: true
    extraComponentToFooter: null
    footer: null
    fallback: ''
    onConfirm: ''
    onCancel: ''
    onFallback: ''
    hideClose: false

  handleCancel: (button) =>
    if @props.onCancel
      @props.onCancel @props.callbackKey, button
    return
  handleConfirm: =>
    if @props.onConfirm and (not @props.enableSend? || @props.enableSend)
      @props.onConfirm @props.callbackKey
    return
  handleFallback: =>
    @props.onFallback(@props.callbackKey) if @props.onFallback
    return

  getTitle: =>
    name = "Record"
    if @props.recordLabel?
      name = @props.recordLabel

    if @props.titleBulletSteps
      stepNumber = @props.titleBulletSteps.number
      stepsQuantity = @props.titleBulletSteps.quantity
      bulletsWithTitle = []

      for value in [1..stepsQuantity]
        bulletSpecificClassName = if value == stepNumber then 'bullet-step-blue' else 'bullet-step-gray'
        lastBulletClass = if value == stepsQuantity then 'last' else ''
        bulletsWithTitle.push(
          ReactDOMFactories.i
            key: "bullet_step_#{value}"
            className: "fa fa-circle #{bulletSpecificClassName} #{lastBulletClass}"
        )
      bulletsWithTitle.push @props.title

      return bulletsWithTitle

    else if @props.title?
      return @props.title
    else if @props.record?.id?
      Consts.MESSAGE_EDIT(name)
    else
      Consts.MESSAGE_CREATE(name)

  render: ->
    confirmButton = null
    cancelButton = null
    fallbackButton = null
    if @props.updateConfirmButton
      confirmButton = @props.updateConfirmButton
       # @props.confirm
    else if @props.confirm and not @props.hideConfirm
      confirmButton = Button
        color: 'primary'
        disabled: if @props.enableSend == false then true else false
        onClick: @handleConfirm
        style: {
          marginLeft: 10
        }
        @props.confirm
    if @props.cancel && @props.showCancel
      cancelButton = Button
        color: 'cancel'
        onClick: partial(@handleCancel, true)
        style: {
          marginLeft: 10
        }
        @props.cancel
    if @props.fallback
      fallbackButton = Button
        onClick: @handleFallback
        @props.fallback

    componentToBottom = if @props.footer
      @props.footer
    else
      div
        className: 'modal-footer '
        @props.extraComponentToFooter
        fallbackButton
        cancelButton
        confirmButton

    div
      className: 'modal-content' + ' ' + @props.extraClasses
      style: @props.extraStyles
      @props.overlay
      if not @props.hideHeader
        div
          className: 'modal-header'
          if @props.rightComponent
            @props.rightComponent
          if not @props.hideClose
            FontAwesomeButton
              className: "modal-close-button"
              color: "secondary-o"
              icon: "fa-times"
              onClick: partial(@handleCancel, false)
          h3 null,
            @getTitle()
      if @props?.showSpinner
        Loading null
      else
        div null,
          div
            className: 'modal-body'
            @props.children
          componentToBottom

module.exports = ModalContent
