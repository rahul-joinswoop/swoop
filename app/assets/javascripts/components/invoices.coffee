import createReactClass from 'create-react-class'
import React from 'react'
require('stylesheets/components/invoices.scss')
import Logger from 'lib/swoop/logger'
import EventRegistry from 'EventRegistry'
import Api from 'api'
DownloadStore = require('stores/download_store')
CompanyStore = require('stores/company_store').default
InvoiceStore = require('stores/invoice_store')
UserStore = require('stores/user_store')
UserSettingStore = require('stores/user_setting_store')
StoreSelector = React.createFactory(require('components/storeSelector'))
import TabsImport from 'components/tabs'
Tabs = React.createFactory(TabsImport)
{Column, Row} = (require 'components/tables/SortTable')
import Utils from 'utils'
InvoiceFilter = React.createFactory(require 'components/invoice_filter')
import Consts from 'consts'
import SearchBox from  'components/search_box'
InvoiceList = React.createFactory(require('components/invoice/list').default)
InvoiceStatusFilter = React.createFactory(require('components/filters/invoice_status_filter'))
import { extend, filter, isEmpty } from 'lodash'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

BaseComponent = require('components/base_component')
AsyncRequestStore = require('stores/async_request_store').default

SearchWrapper = React.createFactory createReactClass(
  componentDidMount: ->
    InvoiceStore.bind(InvoiceStore.CHANGE, @generalChanged)

  componentWillUnmount: ->
    InvoiceStore.unbind(InvoiceStore.CHANGE, @generalChanged)

  generalChanged: ->
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously

  render: ->
    Logger.debug(InvoiceStore.getSearchTotal?(), InvoiceStore.getSearchInvoices?()?.length)
    <SearchBox
      onChange={@props.onChange}
      resultTotal={InvoiceStore.getSearchTotal?()}
      resultSize={InvoiceStore.getSearchInvoices?()?.length}
    />
)

# TODO: delete DownloadButton when unblocked by backend ticket: ENG-7873, FE ticket is ENG-7874
DownloadButton = React.createFactory createReactClass(
  componentDidMount: ->
    InvoiceStore.bind(InvoiceStore.CHANGE, @generalChanged)
    DownloadStore.bind( DownloadStore.CHANGED_REPORT_RESULT,  @generalChanged )
    DownloadStore.bind( DownloadStore.DOWNLOADING_REPORT,  @generalChanged )

  componentWillUnmount: ->
    InvoiceStore.unbind(InvoiceStore.CHANGE, @generalChanged)
    DownloadStore.unbind( DownloadStore.CHANGED_REPORT_RESULT,  @generalChanged )
    DownloadStore.unbind( DownloadStore.DOWNLOADING_REPORT,  @generalChanged )

  getInitialState: -> {}
  generalChanged: ->
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously

  downloadCount: (state) ->
    filter(InvoiceStore.getSearchInvoices?(), (invoice) ->
          invoice?.state == state
      )?.length

  getDownloadApiCall: ->
    if UserStore.isFleetInHouse()
      return Api.downloadFleetApproved
    if @props.state == Consts.ISWOOPAPPROVEDPARTNER
      return Api.downloadPartnerUnpaid

    return Api.downloadFleetUnpaid

  getDownloadReportName: ->
    if UserStore.isTesla()
      return "tesla"
    else if UserStore.isEnterprise()
      return "enterprise"
    else if  UserStore.isMTS()
      return 'mts'
    else if @props.state == Consts.ISWOOPAPPROVEDPARTNER
      return "partner_unpaid"

    return "fleet_unpaid"

  download: ->
    obj = @props.filters
    if @state.search?
      obj["job_search"] = @props.search
    #This is a hack until the backend can create an asyncjob version of this call ENG-6009
    if @props.filters?.sender_id?
      obj["client_company_id"] = @props.filters.sender_id
    DownloadStore.downloadReport(null, obj, null, @getDownloadApiCall(), @getDownloadReportName())

  render: ->
    div
      className: "download_button"
      if !@props.searching
        state = DownloadStore.getLastReportStateByName(@getDownloadReportName())
        if state in ["Loading", "Created", "Running"]
          Loading
            display: 'inline-block'
            message: 'Preparing report ...'
            padding: 0
            size: 24
        else if @downloadCount(@props.state) > 0 and (!UserStore.isSwoop() || @props.filters?.sender_id)
          text = "Download All"
          if UserStore.isSwoop() and CompanyStore.get(@props.filters?.client_company_id, "name") == Consts.FLEET_RESPONSE
            text = "Send All"
          Button
            color: "secondary"
            onClick: @download
            style: {
              float: 'left'
            }
            text
      if state in ["error", "Failed"]
        span
          className: "error"
          style:
            color: "red"
          "Download Failed"
)

class Invoices extends BaseComponent
  displayName: 'Invoices'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()
    @settingChangeWatcherId = @createNewWatcher(@rerender)

    if UserSettingStore.isAllLoaded()
      @rerender()
    else
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)


  rerender: ->
    EventRegistry.clearListeners(@settingChangeWatcherId)
    UserSettingStore.getUserSettingByKeyAsObject(Consts.USER_SETTING.FILTERED_INVOICE_STATUS, @settingChangeWatcherId)
    @rerenderWhenModalCloses()

  getInitialState: ->
    search: null

  componentDidMount: ->
    super(arguments...)

    InvoiceStore.clearSearchInvoices()

  componentWillUnmount: ->
    super(arguments...)

    InvoiceStore.clearSearchInvoices()

  getSortKey: () =>
    setting = UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.INVOICES_SORTBY, @getId())

    if setting?
      return JSON.parse(setting).key

    return "id"

  getSortOrder: () ->
    setting = UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.INVOICES_SORTBY, @getId())

    if setting?
      return Utils.getOrderString(JSON.parse(setting).order)

    return "desc"

  searchChange: (value) =>
    @setState
      search: value

    #THROW OUT OLD INVOICES
    #REDO SEARCH
  renderNewInvoices: ->

  filterList: (filters) =>
    @setState
      filters: filters

    #THROW OUT OLD INVOICES
    #REDO SEARCH

  render: ->
    super(arguments...)

    getProps = (props) =>
      extend({
        filters: @state.filters
        search: @state.search
        ref: "invoice_list"
        searchKey: @getSortKey()
        searchOrder: @getSortOrder()
      }, props)

    tabList = [
      # PARTNER VIEW
      {
        name: "New"
        show: UserStore.isPartner()
        component: =>
          InvoiceList getProps(
            types: [
              Consts.ICREATED, Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED,
              Consts.IFLEETREJECTED, Consts.ISWOOPREJECTEDPARTNER
            ]
            paid: false
            empty_message: "No new jobs"
            tab: "new"
          )

      }
      {
        name: "Sent"
        show: UserStore.isPartner()
        component: =>
          InvoiceList getProps(
            types: [Consts.IPARTNERSENT, Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED, Consts.ISWOOPAPPROVED, Consts.IPARTNEROFFLINENEW, Consts.IONSITEPAYMENT, Consts.ISWOOPDOWNLOADEDPARTNER, Consts.ISWOOPAPPROVEDPARTNER]
            paid: false
            empty_message: "No sent jobs"
            tab: "sent"
          )
      }
      {
        name: "Paid"
        show: UserStore.isPartner()
        component: =>
          InvoiceList getProps(
            types: [Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT, Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED, Consts.ISWOOPAPPROVED, Consts.IPARTNEROFFLINENEW, Consts.IONSITEPAYMENT, Consts.ISWOOPDOWNLOADEDPARTNER, Consts.ISWOOPAPPROVEDPARTNER]
            paid: true
            empty_message: "No paid jobs"
            tab: "paid"
          )
      }

      {
        name: "New"
        show: UserStore.isFleet() && UserStore.isFleetInHouse()
        component: =>
          unsent = [Consts.ICREATED, Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED,Consts.IFLEETREJECTED, Consts.ISWOOPREJECTEDPARTNER]
          sent = [Consts.IPARTNERSENT]
          toShow = []
          invoiceStatus = UserSettingStore.getUserSettingByKeyAsObject(Consts.USER_SETTING.FILTERED_INVOICE_STATUS, @settingChangeWatcherId)
          if invoiceStatus?.isAllSelected
            toShow = unsent.concat(sent)
          else if isEmpty(invoiceStatus) || 0 in invoiceStatus?.selected
              toShow = sent
          else if 1 in invoiceStatus?.selected
              toShow = unsent

          div null,
            div
              className: "extra_options"
              InvoiceStatusFilter
                options: ["Partner Sent", "Partner Unsent"]
            InvoiceList getProps(
              types: toShow
              empty_message: "No new jobs to approve"
          )
      }
      {
        name: "Approved"
        show: UserStore.isFleetInHouse()
        component: =>
          div null,
            # TODO: delete the below when unblocked by backend ticket: ENG-7873, FE ticket is ENG-7874
            if UserStore.isTesla() || UserStore.isEnterprise() || UserStore.isMTS()
              div
                className: "extra_options"
                DownloadButton
                  searching: @state.search
                  state: Consts.IFLEETAPPROVED
                  filters: @state.filters
                  search: @state.search
                  generalChanged: @rerender
            InvoiceList getProps(
              types: [Consts.IFLEETAPPROVED]
              empty_message: "No approved jobs to download"
              # TODO: uncomment the below when unblocked by backend ticket: ENG-7873, FE ticket is ENG-7874
              # includeSearchInDownloadCall: true
              # downloadApiEndPoint: if UserStore.isFleetInHouse() then Api.downloadFleetApproved else Api.downloadPartnerUnpaid
              # shouldShowDownloadButton: (invoices) =>
              #   return !@state?.search &&
              #   (UserStore.isTesla() || UserStore.isEnterprise() || UserStore.isMTS()) &&
              #   filter(invoices, (invoice) ->
              #     invoice.state == Consts.IFLEETAPPROVED
              #   )?.length > 0
            )
      }
      {
        name: "Downloaded"
        show: UserStore.isFleet() && UserStore.isFleetInHouse()
        component: =>
          InvoiceList getProps(
            types: [Consts.IFLEETDOWNLOADED, Consts.ISWOOPAPPROVED, Consts.IPARTNEROFFLINENEW, Consts.IONSITEPAYMENT]
            empty_message: "No downloaded jobs"
          )
      }

      {
        name: "Partner New"
        show: UserStore.isRoot()
        component: =>
          InvoiceList getProps(
            types: [Consts.IPARTNEROFFLINENEW, Consts.ICREATED, Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT]
            empty_message: "No new jobs to approve"
            tabType: 'partner'
          )
      }
      {
        name: "Partner Unpaid"
        show: UserStore.isRoot()
        component: =>
          div null,
            # TODO: delete the below when unblocked by backend ticket: ENG-7873, FE ticket is ENG-7874
            div
              className: "extra_options"
              DownloadButton
                searching: @state.search
                state: Consts.ISWOOPAPPROVEDPARTNER
                filters: @state.filters
                search: @state.search
                generalChanged: @rerender
            InvoiceList getProps(
              types: [Consts.ISWOOPAPPROVEDPARTNER]
              empty_message: "No approved jobs to send"
              tabType: 'partner'
              # TODO: uncomment the below when unblocked by backend ticket: ENG-7873, FE ticket is ENG-7874
              # includeSearchInDownloadCall: true
              # downloadApiEndPoint: if UserStore.isFleetInHouse() then Api.downloadFleetApproved else Api.downloadPartnerUnpaid
              # shouldShowDownloadButton: (invoices) =>
              #   return !@state?.search &&
              #   (!UserStore.isSwoop() || @state?.filters?.sender_id) &&
              #   filter(invoices, (invoice) ->
              #     invoice.state == Consts.ISWOOPAPPROVEDPARTNER
              #   )?.length > 0
            )
      }
      {
        name: "Partner Paid"
        show: UserStore.isRoot()
        component: =>
          InvoiceList getProps(
            types: [Consts.ISWOOPDOWNLOADEDPARTNER, Consts.IONSITEPAYMENT]
            empty_message: "No sent jobs"
            tabType: 'partner'
          )
      }
      {
        name: "Client New"
        show: UserStore.isRoot()
        component: =>
          InvoiceList getProps(
            types: [Consts.ISWOOPNEW]
            empty_message: "No new jobs to approve"
          )
      }
      {
        name: "Client Approved"
        show: UserStore.isRoot()
        component: =>
          div null,
            InvoiceList getProps(
              types: [Consts.ISWOOPAPPROVEDFLEET, Consts.ISWOOPSENDINGFLEET]
              empty_message: "No approved jobs to send"
              shouldShowDownloadButton: (invoices) =>
                return invoices.length > 0 &&
                (!@state?.search || @state?.search?.length == 0) &&
                @state?.filters?.client_company_id
            )
      }
       {
         name: "Client Sent"
         show: UserStore.isRoot()
         component: =>
          InvoiceList getProps(
            types: [Consts.ISWOOPSENTFLEET]
            paid: false
            empty_message: "No sent jobs"
          )
       }
      {
        name: "Client Paid"
        show: UserStore.isRoot()
        component: =>
          InvoiceList getProps(
            types: [Consts.ISWOOPSENTFLEET]
            paid: true
            empty_message: "No sent jobs"
          )
      }
    ]

    tabList = filter(tabList, ((col) -> not col.show? or col.show))
    currentTab = @state.currentTab
    div
      className: "content-wrapper invoice_container"
      if UserSettingStore.hasLoaded(@getId())
        #Don't update based on report results changes, this will be handled by invoices
        Tabs
          ref: "tabs"
          tabList: tabList
          bottomComponents: (tab) =>
            div
              className: 'bottomComponents'
              InvoiceFilter
                filterList: @filterList
                showPartner: tab < 3
                showFleet: tab >= 3
              SearchWrapper
                onChange: @searchChange

module.exports = Invoices
