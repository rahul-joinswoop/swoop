import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
InputDriver = React.createFactory(require 'components/input_driver')
import Utils from 'utils'
JobStore = require('stores/job_store')
BaseComponent = require('components/base_component')

class AccountModal extends BaseComponent
  displayName: 'AccountModal'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()

  onConfirm: =>
    if @state.email != null and @state.email.length > 0 and Utils.isValidEmail(@state.email)
      @props.onConfirm
        id: @props.account.id
        accounting_email: @state.email
    else
      @setState
        error: "Invalid Email"

  getInitialState: ->
    email: null
    error: null

  handleChange: (e) =>
    @setState
      email: e.target.value
      error: null

  render: ->
    super(arguments...)

    name = JobStore.getAccountNameByJobId(@props?.id, @getId())
    TransitionModal $.extend({}, @props,
      callbackKey: 'accept'
      transitionName: 'AccountModalTransition'
      confirm: 'OK'
      title: "No Email Listed"
      extraClasses: "account_email_modal"
      onConfirm: @onConfirm
      cancel: 'Cancel'),
      div null,
        span null,
          "You don't have an email listed for "+(if name? then name else "this account")+". Please type it in below to be able to send invoices to them."
        div null,
          "Accounting Email:"
        div null,
          input
            className: 'form-control'
            onChange: @handleChange
            value: @state.email
          label
            className: 'error'
            @state.error

module.exports = AccountModal
