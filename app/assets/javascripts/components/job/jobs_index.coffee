import React from 'react'
require('stylesheets/components/job/jobs_index.scss')
import Dispatcher from 'lib/swoop/dispatcher'

import Logger from 'lib/swoop/logger'
import Tracking from 'tracking'

# styles
require('stylesheets/jobs.scss')
# stores
JobStore = require('stores/job_store')
UserStore = require('stores/user_store')
ModalStore = require('stores/modal_store')
SettingStore = require('stores/setting_store')
UserSettingStore = require('stores/user_setting_store')
FeatureStore = require('stores/feature_store')
Storage = require('stores/storage')
# components
import JobModals from 'components/job_modals'
[JobFormModalImport] = JobModals
JobFormModal = React.createFactory(JobFormModalImport)
import TabsImport from 'components/tabs'
Tabs = React.createFactory(TabsImport)
JobList = React.createFactory(require('components/job/job_list'))
import SearchBox from 'components/search_box'
import { withModalContext } from 'componentLibrary/Modal'
FTable = React.createFactory(require 'components/resizableTable/FTable')
DispatchSitesFilter = React.createFactory(require 'components/filters/dispatch_sites_filter')
FleetSitesFilter = React.createFactory(require 'components/filters/fleet_sites_filter')
DepartmentFilter = React.createFactory(require 'components/filters/department_filter')
ServiceFilter = React.createFactory(require 'components/filters/service_filter')
DispatcherFilter = React.createFactory(require 'components/filters/dispatcher_filter')
FleetCompanyFilter = React.createFactory(require 'components/filters/fleet_company_filter')
StateFilter = React.createFactory(require 'components/filters/state_filter')
AuctionStore = require('stores/auction_store').default
BaseComponent = require('components/base_component')
DashboardCols = require('dashboard_cols')
JobListHelper = require('helpers/job_list')

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

import Consts from 'consts'
import Utils from 'utils'
import { filter, maxBy, partial, uniq } from 'lodash'

SCHEDULED_FOR_TAB_TIME_LIMIT = 90

class TabContent extends BaseComponent
  showModal: (key, e) =>
    e.preventDefault()
    e.stopPropagation()
    Tracking.track('Blank State Create Job Click', {
      category: 'Onboarding',
    })
    @props.showModal(JobFormModal(
      show: true
      onConfirm: @jobChangeSubmit
      onCancel: @props.hideModals
      submitButtonName: "Create Request"
      disabled: false))

  jobChangeSubmit: (job, close, saveAsDraft = false) ->
    Logger.debug("about to call dispatch", job)
    JobStore.jobChangeSubmit(job, saveAsDraft)
    if not close? || close
      @props.hideModals()

  tabContents: (type, orderByDriver, jobs, showHeader) =>
    showSpinner = true
    if (jobs? and jobs.length > 0) or JobStore.jobsLoaded(@getId())
      showSpinner = false

    isDone = false
    suffix = ''
    colConfiguration = "active"
    if type == "done"
      suffix = '-done'
      isDone = true
      showSpinner = JobStore.isLoadingSearch()
      colConfiguration = "done"
    else if type == "show_job"
      isDone = true
      showSpinner = false
      expanded_id = @props.showJobId

    div
      className: "tab-content job_list"
      div
        id: type+"tabcontents"
        className: "tab-pane active"
        div
          className: "content"
          div
            className: "box-body"
            #style:
            #  border: "1px solid #D5D5D5"
            if showHeader != false
              div
                className: "section-header-container whitebg "+type
                style:
                  position: "relative"
                h2
                  className: "section-header"
                  if type == "dispatched"
                    "In-Progress"
                  else
                    type.replace("_", " ")

            JobList
              jobs: jobs
              orderByDriver: orderByDriver
              sortable: true
              backendSort: isDone
              colConfiguration: colConfiguration
              tableKey: type + "Jobs"
              onSortChange: if isDone then @props.onSortChange else null
              shouldSort: !isDone
              searching: @props.search? and isDone
              expanded_id: expanded_id
            if showSpinner
              div
                className: "whitebg"
                style:
                  textAlign: "center"
                  position: "relative"
                Loading null
            else if @props.search? and isDone and jobs.length == 0
              div
                className: "whitebg"
                style:
                  textAlign: "center"
                  position: "relative"
                  paddingTop: 10
                span null,
                  "No search results found"
            else if jobs.length == 0
              callToAction = switch type
                when 'draft' then 'No draft jobs. '
                when 'scheduled' then 'No scheduled jobs. '
                when 'pending'
                  if UserStore.isOnlyDriver()
                    'No active jobs. '
                when 'dispatched'
                  if UserStore.isPartner() and !UserStore.isOnlyDriver()
                    'Assign a job to a driver! '
                  else
                    'No jobs in-progress. '
                when 'done' then 'No completed jobs. '
              div
                className: "callToAction"
                span null,
                  callToAction
                if type in ['draft', 'scheduled', 'pending', 'done'] and !UserStore.isOnlyDriver()
                  if UserStore.isPartner()
                    div null,
                      button
                        onClick: partial(@showModal, "job_form_new")
                        'Create a job'
                      span null,
                        ' now or '
                      button
                        onClick: JobStore.createDemoJob
                        'receive a demo job!'
                  else
                    button
                      onClick: partial(@showModal, "job_form_new")
                      'Create a New Job!'

  render: ->
    super(arguments...)
    @tabContents(@props.type, @props.orderByDriver, @props.jobs, @props.showHeader)


TabContent = React.createFactory(withModalContext(TabContent))

class JobsIndex extends BaseComponent
  displayName: 'JobsIndex'

  @defaultProps:
    records: []

  isActiveJobSearchEnabled: ->
    SettingStore.getSettingByKey("Active Job Search")?

  constructor: ->
    super(arguments...)
    @register(ModalStore, ModalStore.CHANGE)
    @register(JobStore, JobStore.SEARCH_CHANGED)
    @register(JobStore, JobStore.SEARCH_LOADED)
    @register(JobStore, JobStore.KNOWN_STATE_CHANGE)
    @register(UserStore, UserStore.LOGIN)
    @register(UserStore, UserStore.CHANGE)

    @state = {
      toggleRows: []
      search: null
      jobsLoaded: JobStore.getJobs().length > 0
      searchKey: "last_status_changed_at"
      searchOrder: "desc"
      toggleable: true
      ref: 0 #needed as we are overwritting the base state setting
    }

    @jobListHelper = new JobListHelper(@getId())

    if UserSettingStore.isAllLoaded()
      @rerender()
    else
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  rerender: ->
    @rerenderWhenModalCloses()

  componentDidMount: ->
    super(arguments...)

    JobStore.bind( JobStore.JOB_ADDED, @navigateBasedOnNewJob )

    if @props.showJobId?
      @setState
        showJobId: @props.showJobId

  componentWillUnmount: ->
    super(arguments...)
    JobStore.unbind( JobStore.JOB_ADDED, @navigateBasedOnNewJob )
    clearInterval(@interval)

  componentDidUpdate: ->
    # TODO: remove this logic when dashboard tab logic moved to BE
    @rerenderOnScheduledTime()

  rerenderOnScheduledTime: =>
    @minutesElapsed = 0
    clearInterval(@interval)
    @interval = setInterval(=>
      @minutesElapsed++
      @shouldRerenderOnScheduledTime()
    , 1000 * 60)

  shouldRerenderOnScheduledTime: =>
    rerenderTime = SCHEDULED_FOR_TAB_TIME_LIMIT + @minutesElapsed
    if @scheduledJobRerenderMinutes?.has(rerenderTime)
      @scheduledJobRerenderMinutes.delete(rerenderTime)
      @rerender()

  navigateBasedOnNewJob: =>
    jobs = JobStore.getJobs()
    job = maxBy(jobs, (job) -> job.id)
    if @refs["tabs"]?
      if job?.scheduled_for and SettingStore.getSettingByKey("Scheduled Tab")?
        @refs["tabs"].goToTab("Scheduled")
      else if not job.rescue_driver_id? and SettingStore.getSettingByKey("Pending Tab")?
        @refs["tabs"].goToTab("Pending")
      else
        @refs["tabs"].goToTab("Active")

  jobsChanged: =>
    if !@state.jobsLoaded
      @setState
        jobsLoaded: true
        rev: if @state?['rev']? then ++@state?['rev'] else 0

  addJob: (record) =>
    JobStore.jobAdded(record)

  addDefaultJobs: (record) =>
    JobStore.jobAdded(record)

  toggleRows: (id) =>
    obj = {}
    obj[record.id] = true
    toggleRows.push(obj)
    @setState toggleRows
    return

  filterJobs: (jobs) =>
    jobs = @jobListHelper.filterOnDispatcher(jobs)
    jobs = @jobListHelper.filterOnServices(jobs)
    jobs = @jobListHelper.filterOnStates(jobs)
    jobs = @jobListHelper.filterOnSite(jobs, false, "site_id")
    jobs = @jobListHelper.filterOnSearch(jobs, @state.activeSearch)
    jobs = @jobListHelper.filterOnDepartments(jobs)
    jobs = @jobListHelper.filterOnFleetCompanies(jobs)
    return jobs

  pendingJobList: (excludeScheduled, excludeDraft) =>
    # TODO: remove this logic in if-statement when dashboard tab logic moved to BE
    if UserStore.isSwoop()
      jobs = @jobListHelper.sortJobs(JobStore.getPendingJobs(false, excludeDraft, @getId()))
      if excludeScheduled
        jobs = filter(jobs, (job) =>
          if job?.scheduled_for && job?.status != Consts.ASSIGNED
            scheduled_time = moment(job.scheduled_for)
            minutes = Math.ceil(scheduled_time.diff(moment(), "minutes", true))
            if minutes > SCHEDULED_FOR_TAB_TIME_LIMIT
              return false
          return true
        )
    else
      jobs = @jobListHelper.sortJobs(JobStore.getPendingJobs(excludeScheduled, excludeDraft, @getId()))
    jobs = @filterJobs(jobs)

  dispatchedJobList: (onlyInProgress) =>
    jobs = JobStore.getDispatchedJobs(@getId(), onlyInProgress)
    jobs = @filterJobs(jobs)

  monitoringJobList: =>
    jobs = JobStore.getMonitoringJobs(@getId())
    jobs = @filterJobs(jobs)

  scheduledJobList: =>
    jobs = JobStore.getScheduledJobs(@getId())
    # TODO: remove this logic in if-statement when dashboard tab logic moved to BE
    if UserStore.isSwoop()
      if !@scheduledJobRerenderMinutes
        @scheduledJobRerenderMinutes = new Set()
      jobs = filter(jobs, (job) =>
        scheduled_time = moment(job?.scheduled_for)
        minutes = Math.ceil(scheduled_time.diff(moment(), "minutes", true))
        if minutes <= SCHEDULED_FOR_TAB_TIME_LIMIT
          return false
        @scheduledJobRerenderMinutes.add(minutes)
        return true
      )
    jobs = @filterJobs(jobs)

  draftsJobList: =>
    jobs = JobStore.getDraftsJobs(@getId())
    jobs = @filterJobs(jobs)

  doneJobList: =>
    jobs = if JobStore.isLoadingSearch() then [] else JobStore.getRecordsOfCurrentSearchPage()
    jobs = filter(jobs, (data) ->
      if !data?
        return false
      if UserStore.isRoot()
          if data.type != "FleetManagedJob"
              return false
      if UserStore.isFleet()
          statuses = [Consts.CANCELED, Consts.GOA, Consts.COMPLETED, Consts.STORED, Consts.RELEASED, Consts.REJECTED, Consts.REASSIGNED]
      else
          statuses = [Consts.REJECTED, Consts.ETAREJECTED, Consts.CANCELED, Consts.GOA, Consts.COMPLETED, Consts.REASSIGNED, Consts.STORED, Consts.RELEASED, Consts.EXPIRED, Consts.REASSIGNED]
          if data.type == "FleetManagedJob" and data.status == Consts.EXPIRED
            return false
          if data.status == Consts.REASSIGNED && UserStore.isSwoop() && (UserStore.isRoot() || UserStore.isSwoopDispatcher())
            return true
      return data.status in statuses
    )

    jobs = @jobListHelper.filterOnSite(jobs, true, "site_id")


    return jobs

  searchJobs: (search, searchKey, searchOrder) =>
    if typeof search == "undefined"
      search = @state.search
    if typeof searchKey == "undefined"
      searchKey = @state.searchKey
    if typeof searchOrder == "undefined"
      searchOrder = @state.searchOrder
    filters = active: 0
    if UserStore.isOnlyDriver()
      filters["rescue_driver_id"] = UserStore.getUser().id
    if UserStore.isSwoop()
      filters["fleet_managed_jobs"] = true

    #TODO: we may want to switch to "service_location_id","drop_location_id",

    settings = UserSettingStore.getDoneTabSettings(@getId())
    cols = DashboardCols.getCols(
      isSortable: -> false
      isFrontendSort: -> false
    )
    props = ["id", "type", "status"]
    #Used for filters
    if UserStore.isFleet()
      props.push("fleet_site_id")

    #TODO: Shouldn't need to create the object to get this info
    for key in settings
      row = new cols[key]
      usedProps = row.getUsedProperties()
      if usedProps?
        props.push.apply(props, usedProps)

    for key in ["action", "actions"]
      #TODO: only use the above properties if they are actually shown
      row = new cols[key]
      if row.isShowing()
        usedProps = row.getUsedProperties()
        if usedProps?
          props.push.apply(props, usedProps)

    if search?
      filters = {filters: filters}
    fields = uniq(props)
    filters.fields = fields.join(",")
    sort = searchKey+","+searchOrder
    if search? or searchKey not in ["id", "last_status_changed_at"]
      sort = null

    JobStore.search(search, filters, sort)

  showModal: (key, e) =>
    # this gets called when you click the "New Job" button
    e.preventDefault()
    e.stopPropagation()
    @props.showModal(JobFormModal(
      show: true
      onConfirm: @jobChangeSubmit
      onCancel: @props.hideModals
      submitButtonName: "Create Request"
      disabled: false))

  jobChangeSubmit: (job, close, saveAsDraft = false) ->
    Logger.debug("about to call dispatch", job)
    JobStore.jobChangeSubmit(job, saveAsDraft)
    if not close? || close
      @props.hideModals()

  sortChange: (key, order) =>
    searchOrder = Utils.getOrderString(order)
    @searchJobs(@state.search, key, searchOrder)
    @setState
      searchKey: key
      searchOrder: searchOrder

    return true
    #let state switch

  searchChange: (value) =>
    if value == ""
      value = null

    Logger.debug("search changed to ", value)
    @searchJobs(value, @state.searchKey, @state.searchOrder)
    @setState
      search: value

  activeSearchChange: (value) =>
    if value == ""
      value = null

    if value?
      value = value.toLowerCase()

    @setState
      activeSearch: value

  getSortKey: () =>
    setting = UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.DONE_JOBS_SORTBY, @getId())

    if setting?
      return JSON.parse(setting).key

    return "last_status_changed_at"

  getSortOrder: () =>
    setting = UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.DONE_JOBS_SORTBY, @getId())

    if setting?
      return Utils.getOrderString(JSON.parse(setting).order)

    return "desc"

  renderFilters: () =>
    div
      className: "filter_container"
      if FeatureStore.isFeatureEnabled(Consts.FEATURES_FLEET_SITES)
        FleetSitesFilter()
      if FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS) and SettingStore.getSettingByKey("Filter by Department")?
        DepartmentFilter()
      if SettingStore.getSettingByKey("Filter by State")?
        StateFilter()
      if SettingStore.getSettingByKey("Filter by Service")?
        ServiceFilter()
      if @state.toggleable
        DispatchSitesFilter()
      if @state.toggleable
        DispatcherFilter()
      if UserStore.isSwoop() and SettingStore.getSettingByKey("Filter by Fleet Company")?
        FleetCompanyFilter()

  render: ->
      super(arguments...)

      if !UserSettingStore.hasLoaded(@getId())
        return Loading null

      onActiveEnter = =>
        setTimeout(=>
          @setState
            searchable: false
            toggleable: true
        , 0)

      if Utils.isMobile()
        tabList = []
        if not UserStore.isOnlyDriver()
          tabList.push({
            name: "Pending"
            component: ((pendingJobs) => () =>
              div
                className: "blinker"
                TabContent
                  onSortChange: @sortChange
                  type: "pending"
                  orderByDriver: false
                  jobs: pendingJobs
                  search: @state.search
                  showJobId: @state.showJobId
            )(@pendingJobList())
          })
        tabList.push({
          name: "In-Progress"
          onEnter: onActiveEnter
          component: ((dispatchedJobs) => () =>
            div
              className: "blinker"
              TabContent
                onSortChange: @sortChange
                type: "dispatched"
                orderByDriver: UserStore.isPartner()
                jobs: dispatchedJobs
                search: @state.search
                showJobId: @state.showJobId
          )(@dispatchedJobList())
        })
      else
        tabList = []

        if SettingStore.getSettingByKey("Drafts Tab")?
          draftsJobs = @draftsJobList()

          tabList.push(
            {
              name: "Drafts ("+draftsJobs.length+")"
              id: "Drafts"
              onEnter: onActiveEnter
              component: ((draftsJobs) => () =>
                div
                  className: "blinker"
                  TabContent
                    onSortChange: @sortChange
                    type: "draft"
                    orderByDriver: UserStore.isPartner()
                    jobs: draftsJobs
                    showHeader: false
                    search: @state.search
                    showJobId: @state.showJobId
              )(@draftsJobList())
            }
          )

        scheduledJobs = @scheduledJobList()

        if SettingStore.getSettingByKey("Scheduled Tab")?
          tabList.push(
            {
              name: "Scheduled ("+scheduledJobs.length+")"
              id: "Scheduled"
              onEnter: onActiveEnter
              component: ((scheduledJobs) => () =>
                div
                  className: "blinker"
                  TabContent
                    onSortChange: @sortChange
                    type: "scheduled"
                    orderByDriver: UserStore.isPartner()
                    jobs: scheduledJobs
                    showHeader: false
                    search: @state.search
                    showJobId: @state.showJobId
              )(@scheduledJobList())
            }
          )

        activeName = "Active"
        activeShowing = true

        excludeDraftsFromPending = SettingStore.getSettingByKey("Drafts Tab")?
        pendingJobs =  @pendingJobList(SettingStore.getSettingByKey("Scheduled Tab")?, excludeDraftsFromPending)

        # we check both here because we show Monitoring setting only when Pending setting is enabled
        splitMonitoring = SettingStore.getSettingByKey("Monitoring Tab")? and SettingStore.getSettingByKey("Pending Tab")?

        dispatchedJobs = @dispatchedJobList(splitMonitoring)
        if SettingStore.getSettingByKey("Pending Tab")?
          activeName = "In-Progress"
          activeShowing = false
          tabList.push(
            {
              id: "Pending"
              onEnter: onActiveEnter
              name: ((pendingJobs) => (active) =>
                if !active and pendingJobs.length > 0
                  div
                    style:
                      position: "relative"
                    span null,
                      "Pending "
                    span
                      style:
                        backgroundColor: "#FE0A1A"
                        width: 16
                        height: 16
                        borderRadius: 8
                        lineHeight: "16px"
                        color: "#FFF"
                        display: "block"
                        position: "absolute"
                        fontSize: 9
                        fontWeight: "bold"
                        paddingTop: 0
                        top: -2
                        right: 20
                      pendingJobs.length
                else
                  "Pending (" + pendingJobs.length + ")"
              )(pendingJobs)
              component: ((pendingJobs) => () =>
                div
                  className: "blinker"
                  TabContent
                    onSortChange: @sortChange
                    type: "pending"
                    orderByDriver: false
                    jobs: pendingJobs
                    showHeader: false
                    search: @state.search
                    showJobId: @state.showJobId
              )(pendingJobs)
            }
          )
          pendingJobs = []


        tabList.push(
          {
            name: activeName + " ("+ (pendingJobs?.length + dispatchedJobs?.length) + ")"
            id: "Active"
            onEnter: onActiveEnter
            component: ((pendingJobs, dispatchedJobs) => () =>
              header = false
              div
                className: "blinker"
                div
                  className: "background_flasher"
                if not UserStore.isOnlyDriver()
                  if not SettingStore.getSettingByKey("Pending Tab")?
                    header = true
                    TabContent
                      onSortChange: @sortChange
                      type: "pending"
                      orderByDriver: false
                      jobs: pendingJobs
                      search: @state.search
                      showJobId: @state.showJobId


                TabContent
                  onSortChange: @sortChange
                  type: "dispatched"
                  orderByDriver: UserStore.isPartner()
                  jobs: dispatchedJobs
                  showHeader: header
                  search: @state.search
                  showJobId: @state.showJobId
            )(pendingJobs, dispatchedJobs)
          }
        )

        if splitMonitoring
          monitoringJobs = @monitoringJobList()

          tabList.push(
            {
              name: "Monitoring (#{monitoringJobs.length})"
              id: "Monitoring"
              onEnter: onActiveEnter
              component: ((monitoringJobs) => () =>
                div
                  className: "blinker"
                  TabContent
                    onSortChange: @sortChange
                    type: "dispatched"
                    orderByDriver: UserStore.isPartner()
                    jobs: monitoringJobs
                    showHeader: false
                    search: @state.search
                    showJobId: @state.showJobId
              )(monitoringJobs)
            }
          )

      onEnter = =>
        #Wait for the user to exist before loading done jobs
        if UserSettingStore.hasLoaded(@getId()) != true
          setTimeout(onEnter, 500)
        else
          JobStore.resetSearchResult()


          @searchJobs(null, @getSortKey(), @getSortOrder())
          setTimeout =>
            @setState
              searchable: true
              search: null
              searchKey: @getSortKey()
              searchOrder: @getSortOrder()
              toggleable: false
          , 0

      tabList.push
        name: "Done"
        component: =>
          div
            className: "blinker"
            TabContent
              onSortChange: @sortChange
              type: "done"
              orderByDriver: false
              jobs: @doneJobList()
              showHeader: false
              search: @state.search
              showJobId: @state.showJobId
        onEnter: onEnter
        onExit: =>
          @setState
            searchable: false
            toggleable: true
            searchKey: @getSortKey()
            searchOrder: @getSortOrder()


      forcedTab = null
      if @state.showJobId?
        tabList.push
          name: "#" + @state.showJobId
          hideTab: true
          component: =>
            div
              className: "blinker"
              TabContent
                onSortChange: @sortChange
                type: "show_job"
                orderByDriver: false
                jobs: [JobStore.getJobById(@state.showJobId)]
                showHeader: false
                search: @state.search
                showJobId: @state.showJobId
          onEnter: onEnter
          onExit: =>
            @setState
              showJobId: null
              searchable: false
              toggleable: true
              searchKey: @getSortKey()
              searchOrder: @getSortOrder()
        forcedTab = tabList.length - 1
      defaultTab = 0
      if SettingStore.getSettingByKey("Scheduled Tab")?
        defaultTab = 1
      div
        id: 'content-wrapper'
        className: 'content-wrapper dashboard dashboard_container '

        Tabs
          ref: "tabs"
          defaultTab: defaultTab
          tabList: tabList
          forcedTab: forcedTab
          sideComponents:
            div
              className: "sideComponents"
              if not UserStore.isOnlyDriver()
                Button
                  color: 'primary'
                  label: 'New Job'
                  onClick: partial(@showModal, "job_form_new")
                  size: 'qnav-button'

          bottomComponents:
            div
              className: "bottomComponents"
              if @state.searchable
                <SearchBox
                  onChange={@searchChange}
                  showSearch={@state.searchable || false}
                  show={@state.searchable || false}
                  enablePagination={@state.searchable || false}
                  store={JobStore}
                />
              else
                @renderFilters()
              if @isActiveJobSearchEnabled() and !@state.searchable
                <SearchBox
                  className="active_search"
                  value={@state.activeSearch}
                  onChange={@activeSearchChange}
                  showSearch={!@state.searchable}
                  show={!@state.searchable || false}
                />
              div
                className: 'clear'

module.exports = withModalContext(JobsIndex)
