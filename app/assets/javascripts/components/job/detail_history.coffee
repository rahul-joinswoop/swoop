import React from 'react'
require('stylesheets/components/job/detail_history.scss')
JobStore = require('stores/job_store')
UserStore = require('stores/user_store')
BaseComponent = require('components/base_component')
import Consts from 'consts'
import Utils from 'utils'
import ReactDOMFactories from 'react-dom-factories'

class DetailHistoryItem extends BaseComponent
  consructor: ->
    super(arguments...)
    @state.wrap = false
  toggleWrap: =>
    @setState
      wrap: !@state.wrap

  render: ->
    status = @props.status

    if status.type in ["History", "ScheduledFor", "InvoiceSentAt", "ReturnedToHQ"]
      item = 'check'
      color = 'green'
    else if status.type in ['SMS', 'SMSReply', 'Emailed']
      item = 'comment'
      color = 'gray'
    else if status.type in ["TOA", "Edited"]
      item = 'pencil'
      color = 'gray'

    name = status.name
    li
      className: if @state.wrap then "wrap" else ""
      key: name
      onClick: @toggleWrap
      div
        className: 'icon',
        span
          className: 'circle '+color
          ReactDOMFactories.i
            className: item
      div
        className: 'info'
        span
          className: 'name'
          if status.deleted_at?
            del null,
              status.name
          else
            status.name
        span
          className: 'editor'
          status.ui_label
        span
          className: 'right'
          Utils.formatDateTime(status.adjusted_dttm || status.last_set, true)

detailHistoryItem = React.createFactory(DetailHistoryItem)


class DetailHistory extends BaseComponent
  renderExpander: (expand, text, key) =>
    li
      className: 'expander'
      key: key
      onClick: => @setState {expand: expand}
      div
        className: 'info'
        span null,
          text

  render: ->
    super()

    history = JobStore.getVisibleHistory(@props.job_id, true, @getId())
    MAX_LENGTH = 15
    MIN_HIDDEN = 2
    to_hide = history.length - MAX_LENGTH
    if to_hide >= MIN_HIDDEN
      if !@state?.expand
        history.splice(1, to_hide, "expand")
      else
        history.splice(1, 0, "contract")
    div
      className: 'detail_history'
      ul
        className: 'history_list'
        key: 'history-list-for-' + @props.job_id
        for status, i in history
          if status == "expand"
            @renderExpander(true, "Show "+to_hide+" More Events", i)
          else if status == "contract"
            @renderExpander(false, "Hide "+to_hide+" Events", i)
          else
            detailHistoryItem
              job_id: @props.job_id
              key: history[i].id
              status: status

module.exports = DetailHistory
