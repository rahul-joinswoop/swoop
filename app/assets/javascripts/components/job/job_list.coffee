import React from 'react'

FTable = React.createFactory((require 'components/tables/SortTable').Table)
{Column, Row} = (require 'components/tables/SortTable')

import JobDetailsImport from 'components/job_details'
JobDetails = React.createFactory(JobDetailsImport.default)

Countdown = React.createFactory(require 'components/countdown')
UserStore = require('stores/user_store')
UsersStore = require('stores/users_store').default
InvoiceStore = require('stores/invoice_store')
JobStore = require('stores/job_store')
import Utils from 'utils'
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'
DashboardCols = require('dashboard_cols')
BaseComponent = require('components/base_component')
SettingStore = require('stores/setting_store')
FeatureStore = require('stores/feature_store')
import { extend, filter, values } from 'lodash'
import moment from 'moment-timezone'

class JobList extends BaseComponent
  displayName: 'JobList'
  expandedJobs: {}
  rowCallbacks: {}
  jobs: []

  constructor: ->
    super(arguments...)
    @state = @getInitialState()
    @register(UserSettingStore, UserSettingStore.CHANGE)

  rerender: -> @rerenderWhenModalCloses()

  UNSAFE_componentWillReceiveProps: (props) ->
    if props.expanded_id
      @addExpandedJob(props.expanded_id, true)

  isSortable: =>
    @props.sortable && !@props.searching
  getInitialState: =>
    @cols = DashboardCols.getCols
      getStatusStyle: @getStatusStyle
      expandedJobs: @expandedJobs
      isSortable: @isSortable
      isFrontendSort: @isFrontendSort
      sortStatus: @sortStatus
      isExpanded: @isExpanded
      componentId: @getId()
      parentType: @props.parentType
    for index, col of @cols
      @cols[index] = new col()
    {}

  isFrontendSort: =>
    !@props.backendSort and @isSortable()

  isExpanded: (id) =>
    @expandedJobs[id] and @expandedJobs[id]['expanded'] and @expandedJobs[id]['tab'] == @props.tableKey

  addExpandedJob: (id, expanded = false) =>
    @expandedJobs[id] =
      tab: @props.tableKey
      expanded: expanded

  handleToggleRow: (row, job) =>
    if not job?
      return

    id = job.id

    @addExpandedJob(id, !@isExpanded(id))

    @rowCallbacks?[id]?()

  showModal: (key, job, e) ->
    Utils.showModal(key, e, record: job)

  advanceStatus: (job, status, e) ->
    JobStore.jobChanged({
      id: job.id
      status: status
    })

    e.preventDefault()
    e.stopPropagation()

  shouldFlash: (job) =>
    #if the job was created less then a minute ago when it's inserted then flash
    if Utils.getJobCreatedAt(job)? and Utils.wasRecent(Utils.getJobCreatedAt(job))
      return @getColor("Received", "#FCC37F")

    #check if history was recent
    for key, status of job.history
      if Utils.wasRecent(status.last_set)
        return {} # {backgroundColor: "#F2F2F2"}

    if job.shouldFlash
      if moment().diff(job.shouldFlash) < 5000
        return @getColor("Received", "#FCC37F")
    return false

  shouldAlwaysFlash: (job) =>
    if UserStore.isSwoop() and !job.scheduled_for
      if !job.root_dispatcher_id?
        return {backgroundColor: "#F2F2F2"}

    else if UserStore.isPartner()
      if job.status is Consts.ASSIGNED
        return @getColor("Received", "#FCC37F")
      if job.status in [Consts.DISPATCHED] and not job.scheduled_for?
        # TODO: This flash is based on other jobs, so won't necessarily be triggered without a rerender being called
        if job.history?.Dispatched?
          time = job.history.Dispatched.last_set
          if moment(time) < moment().subtract(10, "minutes")
            if not job.rescue_driver_id?
              return @getColor("Long Dispatch", "#FF7171")
            else
              jobs = JobStore.getJobs()
              jobs = filter(jobs, (fjob) ->
                if fjob.rescue_driver_id != job.rescue_driver_id
                  return false
                if fjob.id == job.id
                  return false
                return fjob.status in [Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION]
              )
              if jobs.length == 0
                return @getColor("Long Dispatch", "#FF7171")

    else if UserStore.isFleet() and UserStore.isFleetInHouse() and job.rescue_company == null and job.status in [Consts.PENDING, Consts.UNASSIGNED, Consts.REASSIGN]
      return @getColor("Received", "#FCC37F")
    return false

  getColor: (setting, def, transparent) =>
    setting = SettingStore.getSettingByKey(setting)
    color = def
    if !transparent?
      transparent = .5
    if setting?.value?
      color = setting.value
    if color == "transparent"
      return {}
    result = Utils.hexToRgb(color)
    return {backgroundColor: "rgba("+result.r+","+result.g+","+result.b+", "+transparent+")"}


  getColorKey: (value) =>
    eta = Utils.getLastEtaObj(value)
    mins = 100
    if eta?
      mins = Math.ceil(eta.diff(moment(), "minutes", true))

    key = null
    if value.status in [Consts.PENDING, Consts.SUBMITTED, Consts.ACCEPTED, Consts.ASSIGNED, Consts.DISPATCHED, Consts.ENROUTE]
      if mins <= 0
        return "Past Eta"
      else if mins <= 10
        return "Eta Approaching"
      else if Utils.isScheduled(UserStore, value)
        return "Scheduled"

    if value.status in [Consts.DRAFT, Consts.PENDING, Consts.SUBMITTED, Consts.ACCEPTED, Consts.ASSIGNED, Consts.DISPATCHED, Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION]
      return value.status

  getStatusStyle: (value, status) =>
    if !value?
      return
    if @props?.backendSort
      return

    key = @getColorKey(value)

    # allow status to fall through when 'key' is undefined; cross-reference ...jobs/status.coffee, getStatus
    if !key? && typeof(status) == "string"
      key = status

    if !key?
      return

    setting = SettingStore.getSettingByKey(key)
    if setting?
      return backgroundColor: setting.value
    else if key == "Eta Approaching"
      return @getColor(key, "#FDEB8F", 1)
    else if key == "Past Eta"
      return @getColor(key, "#FF7171", 1)


  sortStatus: (job, props) =>
    index = Consts.ORDERED_STATUSES.indexOf(Utils.getJobStatusText(UserStore, job))
    if index != -1
      return index

  getRowStyle: (i, job) =>
    if @props.rowStyle?
      return @props.rowStyle
    else
      if i? and job?
        alwaysFlashStyle = @shouldAlwaysFlash(job)
        if alwaysFlashStyle
          return alwaysFlashStyle
        flashStyle = @shouldFlash(job)
        if flashStyle
          return flashStyle
        return {}

  getRowClassName: (i, job) =>
    if i? and job?
      className = ""
      if not job?
        return className
      if @isExpanded(job.id)
        className += " expanded"
      if @shouldAlwaysFlash(job)
        className += " blink_me_always"
      else if @shouldFlash(job)
       className += " blink_me"
       #A Row is blinking so trigger a rerender after 3 seconds
       if @blinkTimeout?
        clearTimeout(@blinkTimeout)
       @blinkTimeout = setTimeout(=>
        @rerender()
       , 3000)
      return className

  getRowHeader: (i, job) =>
    if not job?
      return

    if @props.orderByDriver
      if i == 0 or @jobs[i].rescue_driver_id != @jobs[i-1].rescue_driver_id
        return div
          className: "driverName"
          if @jobs[i].rescue_driver_id?
            UsersStore.get(@jobs[i].rescue_driver_id, "full_name", @getId()) || "..loading"
          else
            "No Driver Assigned"
  getRowFooter: (i, job) =>
    if not job?
      return

    if @isExpanded(job.id)
      JobDetails
        id: job.id

  orderByDriver: (jobs) =>
    jobs.sort((a, b) =>
      name1 = a.rescue_driver_id && UsersStore.get(a.rescue_driver_id, "full_name", @getId()) || ""
      name2 = b.rescue_driver_id && UsersStore.get(b.rescue_driver_id, "full_name", @getId()) || ""

      if name1 == name2
        a_date = Utils.getJobCreatedAt(a)
        b_date = Utils.getJobCreatedAt(b)
        if a.scheduled_for?
            a_date = a.scheduled_for
        if b.scheduled_for?
            b_date = b.scheduled_for
        return (new Date(a_date)).getTime() - (new Date(b_date)).getTime()

      if name1 == ""
        return -1
      if name2 == ""
        return 1

      return name1.localeCompare(name2)
    )



  createCols: =>
    #limit based on settings
    if @props.colNames?
      settings = @props.colNames
    else if @props.colConfiguration == "done"
      settings = UserSettingStore.getDoneTabSettings(@getId())
    else
      settings = UserSettingStore.getActiveTabSettings(@getId())

    ret = []
    for key in settings
      ret.push(@cols[key])

    if UserStore.isDriver() and Utils.isMobile()
      ret.push(@cols["action"])

    ret.push(@cols["actions"])

    return ret

  getJobs: =>
    jobs = @props.jobs
    if @props.orderByDriver
      jobs = @orderByDriver(jobs)

    @jobs = jobs
    return @jobs

  fetchUpdatedRecord: (job) =>
    if job?
      job = JobStore.getJobById(job.id)

    if job?
      if @isExpanded(job.id)
        job.expanded = true
      else
        job.expanded = false
    job


  rowDidMount: (rowComp, i, job) =>
    if job?
      invoice = Utils.getDisplayInvoice(job, UserStore.isRoot())
      if invoice?
        InvoiceStore.bind( InvoiceStore.CHANGE+"_"+invoice.id, rowComp.generalChanged )
      JobStore.bind( JobStore.CHANGE+"_"+job.id, rowComp.generalChanged )
      @rowCallbacks[job.id] = rowComp.generalChanged

  rowWillUnmount: (rowComp, i, job) =>
    if job?
      invoice = Utils.getDisplayInvoice(job, UserStore.isRoot())
      JobStore.unbind( JobStore.CHANGE+"_"+job.id, rowComp.generalChanged )
      if invoice?
        InvoiceStore.unbind( InvoiceStore.CHANGE+"_"+invoice.id, rowComp.generalChanged )

  rowWillReceiveProps: (rowComp, newProps, oldProps) =>
    if newProps?.row? and oldProps?.row? and newProps.row.id != oldProps.row.id
      JobStore.unbind( JobStore.CHANGE+"_"+oldProps.row.id, rowComp.generalChanged )
      if oldProps?.row?.invoices?[0]?
        InvoiceStore.unbind( InvoiceStore.CHANGE+"_"+oldProps.row.invoices[0].id, rowComp.generalChanged )
      if newProps?.row?.invoices?[0]?
        InvoiceStore.bind( InvoiceStore.CHANGE+"_"+newProps.row.invoices[0].id, rowComp.generalChanged )
      JobStore.bind( JobStore.CHANGE+"_"+newProps.row.id, rowComp.generalChanged )
      @rowCallbacks[newProps.row.id] = rowComp.generalChanged
      #TOFIX: Because the job can dissappear and reappear the delete was happening after the add causing it not to work
      #delete @rowCallbacks[job.id]

  render: ->
    super(arguments...)

    if not UserStore.getUser() or values(FeatureStore.getAll()).length == 0
      return null

    FTable extend({}, @props,
        className: "jobsTable"
        tableKey: @props.tableKey
        rows: @getJobs()
        cols: @createCols()
        originalSortDir: 1
        originalSortKey: "id"
        frontEndSort: @isFrontendSort()
        fetchUpdatedRecord: @fetchUpdatedRecord
        rowDidMount: @rowDidMount
        rowWillUnmount: @rowWillUnmount
        rowWillReceiveProps: @rowWillReceiveProps
        sortable: @props.sortable
        rowClick: @handleToggleRow
        rowClassNameGetter: @getRowClassName
        rowStyleGetter: @getRowStyle
        rowFooter: @getRowFooter
        rev: if @state then @state.rev
      )
module.exports = JobList
