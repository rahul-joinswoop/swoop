import React from 'react'
import Consts from 'consts'
import Utils from 'utils'
import InvoiceStore from 'stores/invoice_store'
import UserStore from 'stores/user_store'
import JobStore from 'stores/job_store'
import ModalStore from 'stores/modal_store'
import { find, includes, partial } from 'lodash'
import PropTypes from 'prop-types'
import Dispatcher from 'lib/swoop/dispatcher'

const propTypes = {
  job: PropTypes.object.isRequired,
}

// Helper class to allow passing user type dependent conditions and running,
// verifying those conditions.
//
// The constructor will verify the UserStore for what type of user is signed in
// and methods like `forbidden` will run on the given conditions object the
// corresponding method (i.e. `forbidden`) for the signed in user. If there is
// no signed in user dependent condition, the method (i.e. `forbidden`) will be
// run on the conditions object itself.
//
// Conditions objects must implement the validation methods used, like
// `forbidden` for example, and must return a boolean. Each condition object
// can implement the validation method as necessary with specific arguments as
// well.
//
// Example:
//
//   const conditions = { forbidden: (myOwnArgument]) => true }
//   const toggler = new Toggler();
//   const forbidden = toggler.forbidden(conditions)(myOwnArgument);
class Toggler {
  constructor() {
    // Order is important as it emphasises hirerachy
    const userTypes = ['isSwoop', 'isFleetInHouse', 'isFleet', 'isPartner']

    this.userType = find(userTypes, (userType) => {
      if (UserStore[userType] && UserStore[userType].call()) {
        return userType
      }
      return undefined
    })
  }

  forbidden(conditions) {
    if (this.userType && conditions[this.userType]) {
      return (...args) => conditions[this.userType].forbidden(...args)
    } if (conditions.forbidden) {
      return (...args) => conditions.forbidden(...args)
    }

    return () => false
  }
}

const jobConditions = {
  forbidden: (jobStatus) => {
    const jobStatusesNotAllowed = [
      Consts.SUBMITTED, Consts.EXPIRED, Consts.REASSIGNED,
    ]

    return includes(jobStatusesNotAllowed, jobStatus)
  },
}

const fleetInHouseOrSwoopConditions = {
  forbidden: (job, invoice) => {
    const forbiddenJobStatuses = [
      Consts.COMPLETED, Consts.CANCELED, Consts.GOA,
    ]
    if (!invoice) {
      return includes(forbiddenJobStatuses, job.status)
    }
    const forbiddenInvoiceStates = [
      Consts.IPARTNERSENT, Consts.IFLEETAPPROVED,
      Consts.IFLEETDOWNLOADED, Consts.ISWOOPAPPROVEDPARTNER, Consts.ISWOOPNEW,
      Consts.ISWOOPSENDINGFLEET, Consts.ISWOOPSENTFLEET,
    ]

    return includes(forbiddenJobStatuses, job.status) &&
      includes(forbiddenInvoiceStates, invoice.state)
  },
}

const fleetConditions = {
  forbidden: (job, invoice) => {
    const forbiddenJobStatuses = [
      Consts.COMPLETED, Consts.CANCELED, Consts.GOA,
    ]
    if (!invoice) {
      return includes(forbiddenJobStatuses, job.status)
    }
    const forbiddenInvoiceStates = [
      Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT, Consts.IFLEETAPPROVED,
      Consts.IFLEETDOWNLOADED, Consts.ISWOOPAPPROVEDPARTNER, Consts.ISWOOPNEW,
      Consts.ISWOOPSENDINGFLEET, Consts.ISWOOPSENTFLEET,
    ]

    return includes(forbiddenJobStatuses, job.status) && invoice &&
      includes(forbiddenInvoiceStates, invoice.state)
  },
}

const partnerConditions = {
  forbidden: (job, invoice) => {
    const forbiddenJobStatuses = [Consts.ASSIGNED]
    const forbiddenInvoiceStates = [
      Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED,
      Consts.ISWOOPAPPROVEDPARTNER, Consts.ISWOOPNEW,
      Consts.ISWOOPSENDINGFLEET, Consts.ISWOOPSENTFLEET,
    ]

    return Utils.isAssignedFromFleet(job) &&
      includes(forbiddenJobStatuses, job.status) && invoice &&
      includes(forbiddenInvoiceStates, invoice.state)
  },
}

const userConditions = {
  isSwoop: fleetInHouseOrSwoopConditions,
  isFleetInHouse: fleetInHouseOrSwoopConditions,
  isFleet: fleetConditions,
  isPartner: partnerConditions,
}

const loadAndEditJob = (job, e) => {
  if (job.partial) {
    JobStore.getFullJobById(job.id, true, (job) => {
      if (isEditable(job)) {
        Utils.showModal('job_form_edit', e, { record: job })
      } else {
        Utils.showModal('simple', e, {
          title: 'Cannot Edit Job',
          confirm: 'OK',
          cancel: null,
          body: 'This job is in a status that cannot be edited. Contact Swoop if you need additional support.',
          onConfirm: (() => {
            Dispatcher.send(ModalStore.CLOSE_MODALS)
          }),
        })
      }
    })
  } else {
    Utils.showModal('job_form_edit', e, { record: job })
  }
  Utils.ignoreEvents(e)
}

function isEditable(job) {
  let invoice

  if (job?.invoices) {
    invoice = Utils.getDisplayInvoice(job, UserStore.isRoot())
  }

  if (invoice) {
    const invoiceFromStore = InvoiceStore.getInvoiceById(invoice.id, false)

    if (invoiceFromStore) {
      invoice = invoiceFromStore
    }
  }

  const toggler = new Toggler()
  const forbiddenJob = toggler.forbidden(jobConditions)(job.status)
  if (forbiddenJob) {
    return false
  }

  if (!(job.partial && !invoice)) {
    const forbiddenUser = toggler.forbidden(userConditions)(job, invoice)
    if (forbiddenUser) {
      return false
    }
  }
  return true
}

function EditButton(props) {
  const { job } = props
  if (!isEditable(job)) {
    return null
  }

  return (
    <i
      className="fa fa-pencil edit inline"
      name="edit"
      onClick={partial(loadAndEditJob, job)}
    />
  )
}

EditButton.propTypes = propTypes

export default EditButton
