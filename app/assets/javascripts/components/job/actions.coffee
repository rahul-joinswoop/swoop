require('stylesheets/components/job/actions.scss')

import createReactClass from 'create-react-class'
import React from 'react'
import Utils from 'utils'
import Consts from 'consts'
UserStore = require('stores/user_store')
import CountdownSeconds from 'componentLibrary/CountdownSeconds'
EditButton = React.createFactory(require('components/job/edit_button').default)
JobOptions = React.createFactory(require('components/job/options'))
AuctionStore = require('stores/auction_store').default
Timer = React.createFactory(require('components/timer').default)
import moment from 'moment-timezone'
import ReactDOMFactories from 'react-dom-factories'

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)

import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

Actions = (createReactClass(
  getInitialState: ->
    {
      showSpinnerInsteadOfTimer: false
    }

  handleRequestMoreTime: (showSpinnerInsteadOfTimer) ->
    if showSpinnerInsteadOfTimer
      @setState
        showSpinnerInsteadOfTimer: true
    else
      @setState
        showSpinnerInsteadOfTimer: false

  render: ->
    job = @props.job
    classes = 'item'
    toggleButtonClasses = 'fa fa-chevron-down'
    if @props.expanded
      classes = 'item selected'
      toggleButtonClasses = 'fa fa-chevron-up'

    div
      className: "jobActions right",
      if job?.status == Consts.ASSIGNED and job?.type == "FleetMotorClubJob" and job?.answer_by?
        if @state.showSpinnerInsteadOfTimer
          Loading
            padding: 0
            size: 24
        else
          <CountdownSeconds time={moment(job.answer_by)} />
      else if UserStore.isPartner() && AuctionStore.jobHasLiveAuction(job)
        Timer
          className: 'timer-eta'
          expiresAt: AuctionStore.getExpiresAtByJob(job)
          showTimerIcon: false
      else if not Utils.isMobile() and !(UserStore.isPartner() and job?.status in [Consts.ASSIGNED, Consts.AUTO_ASSIGNING]) && !@props.hideEdit
        EditButton
          job: job
      if not Utils.isMobile() and !(UserStore.isPartner() and job?.status == Consts.AUTO_ASSIGNING) and @props?.parentType != "review"
        JobOptions
          job: job
          update: @props.update
          onRequestMoreTime: @handleRequestMoreTime
      if not Utils.isMobile()
        ReactDOMFactories.i
          className: toggleButtonClasses
      else
        Button
          color: "green"
          size: "full-width"
          if @props.expanded
            "Less"
          else
            "More"

))
module.exports = Actions
