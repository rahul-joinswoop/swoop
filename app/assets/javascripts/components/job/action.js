import React from 'react'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import JobStore from 'stores/job_store'
import UserStore from 'stores/user_store'
import { includes, partial } from 'lodash'

const Action = ({ job }) => {
  const MAP = {}

  MAP[Consts.DISPATCHED] = Consts.ENROUTE
  MAP[Consts.ENROUTE] = Consts.ONSITE

  if (includes([Consts.DECKING, Consts.IMPOUND, Consts.SECONDARYTOW, Consts.TOW, Consts.UNDECKING], job.service)) {
    MAP[Consts.ONSITE] = Consts.TOWING
  } else {
    MAP[Consts.ONSITE] = Consts.COMPLETED
  }

  MAP[Consts.TOWING] = Consts.TOWDESTINATION
  MAP[Consts.TOWDESTINATION] = Consts.COMPLETED

  const advanceStatus = (job, status, e) => {
    JobStore.jobChanged({
      id: job.id,
      status,
    })

    e.preventDefault()
    e.stopPropagation()
  }

  if (!MAP[job.status]) {
    return null
  }

  const user = UserStore.getUser()
  if (user && user.id !== job.rescue_driver_id) {
    return null
  }

  return (
    <Button
      color="green-o"
      onClick={partial(advanceStatus, job, MAP[job.status])}
      size="full-width narrow"
    >
      {`Mark ${MAP[job.status]}`}
    </Button>
  )
}

export default Action
