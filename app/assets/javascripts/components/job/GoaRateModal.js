import React, { useState, useMemo, useEffect } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import FormRenderer from 'form_renderer'
import InvoiceStore from 'stores/invoice_store'
import JobStore from 'stores/job_store'
import ModalContent from 'components/modals_content'
import MoneyField from 'fields/MoneyField'
import SiteStore from 'stores/site_store'
import UserStore from 'stores/user_store'
import Utils from 'utils'
import 'stylesheets/components/job/GoaRateModal.scss'
import { withModalContext } from 'componentLibrary/Modal'
import Loading from 'componentLibrary/Loading'
import { useRerender, useInstance } from 'componentLibrary/Hooks'
import { useStoreBind, useRenderWatcherId } from 'stores/storeHooks'

function cn(name) {
  return `GoaRateModal-${name}`
}

class GoaRateField extends MoneyField {
  _label = 'GOA Cost'

  _name = 'goaRate'

  _required = true
}

function GoaRateBalance({ balance, saving, submitted }) {
  // If we've submitted but haven't received an invoice back show a spinner
  if (submitted && saving) {
    return <Loading />
  }
  const showBalance = submitted && !saving && !!balance
  return (
    <div className={cn('balance')}>
      <div><b>Balance</b></div>
      <div className={classNames({ [cn('red')]: showBalance })}>
        {showBalance ?
          `$${balance} (request VCC reimbursement from partner)` :
          'Submit GOA Cost to determine balance'}
      </div>
    </div>
  )
}

function GoaRateModalContent({
  balance, invoiceId, jobId, loading, submitted, setSubmitted, saving, setSaving,
}) {
  const watcherId = useRenderWatcherId()
  const formRerender = useRerender()
  const form = useInstance(() => (
    new FormRenderer({ goaRate: '' }, formRerender)
  ))
  const goaRateField = useInstance(() => (
    form.register(GoaRateField, 'goaRate')
  ))

  // If we do have the invoice, make sure when it changes we update our component
  // This gets rid of the spinner after a save
  useStoreBind(InvoiceStore, invoiceId ? `${InvoiceStore.CHANGE}_${invoiceId}` : null, () => {
    if (saving) {
      setSaving(false)
      formRerender()
    }
  })

  if (loading || !form) { return <Loading /> }

  const handleGoaRateSubmit = () => {
    if (saving) {
      return
    }
    if (form.isValid()) {
      // disable GOA Rate field
      goaRateField._editable = false
      setSaving(true)
      setSubmitted(true)
      const { goaRate } = form.record
      JobStore.jobChanged({
        goa_amount: goaRate,
        id: jobId,
        status: Consts.GOA,
      })
    } else {
      form.attemptedSubmit()
      formRerender()
    }
  }

  const rescueCompanyName = JobStore.get(jobId, 'rescue_company.name', watcherId) || ''
  const siteId = JobStore.get(jobId, 'site_id', watcherId)
  const siteName = SiteStore.get(siteId, 'name', watcherId) || ''
  const sitePhone = SiteStore.get(siteId, 'phone', watcherId) || ''

  return (
    <div>
      {submitted ? null : <div>Please enter the GOA cost to determine balance.</div>}
      <div className={cn('partner')}>
        <div><b>Partner</b></div>
        <div>{`${rescueCompanyName} ${siteName}`}</div>
        <div>{sitePhone}</div>
      </div>

      <div className={cn('rate')}>
        {!submitted && !saving && (
          <>
            {form.renderInput('goaRate')}
            <Button color="primary" onClick={handleGoaRateSubmit}>
              {saving ? (<i className="fa fa-spinner fa-pulse" />) : 'Submit'}
            </Button>
          </>
        )}
      </div>
      <GoaRateBalance balance={balance} saving={saving} submitted={submitted} />
    </div>
  )
}


function GoaRateModal({ job, ...props }) {
  const [loading, setLoading] = useState(true)
  const [submitted, setSubmitted] = useState(false)
  const [saving, setSaving] = useState(false)
  const [error, setError] = useState()
  const [invoiceId, setInvoiceId] = useState()

  // Ensure we have a full job with an invoice, if not load it in
  useEffect(() => {
    JobStore.getFullJob(job, (fullJob) => {
      try {
        const invoice = Utils.getDisplayInvoice(fullJob, UserStore.isRoot())
        setInvoiceId(invoice.id)
      } catch {
        Rollbar.warning('Marking a job as GOA without an invoice', { jobId: fullJob.id })
        setError('Error has occured, please contact technical support')
      }
      setLoading(false)
    })
  }, [job])

  const invoice = InvoiceStore.getInvoiceById(invoiceId)
  const balance = Utils.getInvoiceBalanceStr(invoice)

  return (
    <ModalContent
      confirm="Done"
      enableSend={submitted && !saving && balance}
      extraClasses="simple_modal"
      onCancel={props.hideModals}
      onConfirm={props.hideModals}
      showCancel={false}
      title="GOA Cost Required"
      {...props}
    >
      {error || (
        <GoaRateModalContent
          balance={balance}
          invoiceId={invoiceId}
          jobId={job.id}
          loading={loading}
          saving={saving}
          setSaving={setSaving}
          setSubmitted={setSubmitted}
          submitted={submitted}
        />
      )}
    </ModalContent>
  )
}

GoaRateModal.propTypes = {
  job: PropTypes.object.isRequired,
  // and all props from ModalContent to override them, please see components/modals_content for details
}

export default withModalContext(GoaRateModal)

export const Component = GoaRateModal
