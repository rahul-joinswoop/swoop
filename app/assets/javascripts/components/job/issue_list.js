import React from 'react'
import Button from 'componentLibrary/Button'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import IssueStore from 'stores/issue_store'
import JobStore from 'stores/job_store'
import Utils from 'utils'
import { partial } from 'lodash'
import 'stylesheets/components/job/issue_list.scss'

class IssueList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rev: 0,
    }
  }

  componentDidMount() {
    JobStore.bind(`${JobStore.CHANGE}_${this.props.job_id}`, this.generalChanged)
  }

  componentWillUnmount() {
    JobStore.unbind(`${JobStore.CHANGE}_${this.props.job_id}`, this.generalChanged)
  }

  addItem = e => Utils.showModal('add_job_issue', e, {
    job_id: this.props.job_id,
  })

  generalChanged = () => {
    this.setState(prevState => ({
      rev: prevState.rev + 1,
    }))
  }

  getItems = () => {
    const job = this.getJob()
    return job?.explanations || []
  }

  getJob = () => JobStore.getJobById(this.props.job_id)

  handleOnClick = (e) => {
    this.addItem(e)
  }

  onDelete = (item) => {
    JobStore.removeIssue(this.props.job_id, item.id)
    this.generalChanged()
  }

  showSpinner = item => (item && item.url === null)

  render() {
    return (
      <>
        <div className="issue-list add-list">
          { this.getItems().length > 0 &&
            <>
              <div className="issues-headings">
                <span>Issues</span>
                <span>Reasons</span>
              </div>
              <ul>
                { this.getItems().map(item => (
                  <li key={item?.reason?.issue?.key}>
                    <span>
                      { IssueStore.hasPermissions() &&
                        <FontAwesomeButton
                          color="secondary-o"
                          icon="fa-times-circle"
                          onClick={partial(this.onDelete, item)}
                          size="small"
                        />}
                      {item?.reason?.issue?.name}
                    </span>
                    <span>{item?.reason?.name + (item?.reason_info ? ` (${item?.reason_info})` : '')}</span>
                  </li>
                ))}
              </ul>
            </>}
        </div>
        { IssueStore.hasPermissions() &&
          <Button
            color="secondary-o"
            onClick={this.handleOnClick}
            size="narrow"
          >
            Add Issue
          </Button>}
      </>
    )
  }
}

export default IssueList
