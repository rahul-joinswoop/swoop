import AddOnSiteStatus from 'components/modals/add_on_site_status'
import AuctionStore from 'stores/auction_store'
import BaseComponent from 'components/base_component'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import FeatureStore from 'stores/feature_store'
import JobStore from 'stores/job_store'
import moment from 'moment'
import React from 'react'
import SelectServiceProvided from 'components/modals/select_service_provided'
import SettingStore from 'stores/setting_store'
import TimeStore from 'stores/time_store'
import UserStore from 'stores/user_store'
import Utils from 'utils'
import { partial } from 'lodash'
import { withModalContext } from 'componentLibrary/Modal'
import './status.scss'

class Status extends BaseComponent {
  constructor(props) {
    super(props)

    this.state = { rev: 0 }

    if (UserStore.isPartner()) {
      this.register(AuctionStore, AuctionStore.BID_UPDATED_ON_CLIENT)
    }
  }

  changeStatus = (e, opts = {}) => {
    const status = e.target.value
    const jobId = this.props.job.id
    const { showModal } = this.props

    const local_e = {
      target: {
        name: 'status',
        value: status,
      },
    }

    if (status === Consts.ENROUTE) {
      Tracking.trackJobEvent('En Route', jobId)
    }

    if (this.shouldShowForceServiceModal(jobId, status, opts.skipForcedDecision)) {
      opts.skipForcedDecision = true
      showModal(
        <SelectServiceProvided
          callback={() => this.changeStatus(local_e, opts)}
          job_id={jobId}
        />,
      )
    } else if (this.shouldShowOnSiteModal(jobId, status, opts.skipOnSiteService)) {
      opts.skipOnSiteService = true
      showModal(
        <AddOnSiteStatus
          callback={() => this.changeStatus(local_e, opts)}
          job_id={jobId}
          status={status}
        />,
      )
    } else {
      Utils.handleJobChange(JobStore, jobId, e)
    }
  }

  getEditableJobOptions = (job) => {
    const options = this.getJobStatusOptions(job)
    if (!options.length) {
      return Consts.RELEASED
    }

    return (
      <select
        className="JobStatus-options status-select form-control"
        id="status"
        name="status"
        onChange={this.changeStatus}
        onClick={Utils.preventPropogation}
        value={job.status}
      >
        {options.map(option =>
          <option className="JobStatus-option option_row" key={option} value={option}>
            {option}
          </option>,
        )}
      </select>
    )
  }

  getStatus = () => {
    const job = JobStore.getJobById(this.props.job?.id)
    if (!job) {
      return ''
    }

    if (job.status === Consts.DRAFT) {
      return Utils.getJobStatusText(UserStore, job)
    }

    if (UserStore.isFleetManaged()) {
      if (job.status === Consts.AUTO_ASSIGNING) {
        return Consts.PENDING
      } else {
        return Utils.getJobStatusText(UserStore, job) // job.status or Scheduled
      }
    } else if (UserStore.isSwoop() && job.status === Consts.AUTO_ASSIGNING) {
      if (AuctionStore.jobHasLiveAuction(job)) {
        return 'Collecting'
      } else {
        return 'Pending'
      }
    } else if (UserStore.isFleetInHouse() || UserStore.isSwoop()) {
      if (!job.rescue_company || !job.fleet_manual || [Consts.CREATED, Consts.STORED, Consts.EXPIRED, Consts.REJECTED, Consts.REASSIGNED, Consts.ETAREJECTED, Consts.AUTO_ASSIGNING].includes(job.status)) {
        return Utils.getJobStatusText(UserStore, job)
      } else {
        return this.getEditableJobOptions(job)
      }
    } else if (UserStore.isPartner()) {
      if ([Consts.CREATED, Consts.STORED, Consts.EXPIRED, Consts.REJECTED, Consts.REASSIGNED, Consts.ETAREJECTED, Consts.SUBMITTED].includes(job.status)) {
        return Utils.getJobStatusText(UserStore, job)
      } else if (this.isJobSubmitted(job)) {
        return Consts.SUBMITTED
      } else if (this.isJobAssignable(job)) {
        return this.getAssignableActions(job)
      } else {
        return this.getEditableJobOptions(job)
      }
    }

    return ''
  }

  getAssignableActions = job => (
    <div className="JobStatus-assignAction">
      <Button
        className="green-o"
        label="Accept"
        onClick={partial(Utils.showModal, 'accept', partial.placeholder, {
          record: job,
        })}
        size="narrow small"
        style={{ marginRight: 5 }}
      />
      <Button
        className="red-o"
        label="Reject"
        onClick={partial(Utils.showModal, 'reject', partial.placeholder, {
          record: job,
        })}
        size="narrow small"
      />
    </div>
  )

  isJobSubmitted(job) {
    return job.status === Consts.AUTO_ASSIGNING && AuctionStore.hasPartnerSentBidToJob(job)
  }

  isJobAssignable(job) {
    const jobIsAssignedFromAFleet = Utils.isAssignedFromFleet(job)
    const fleetAssignedJob = jobIsAssignedFromAFleet && job.status === Consts.ASSIGNED
    const jobExperiencingAuction = AuctionStore.jobHasAuction(job) && job.status === Consts.AUTO_ASSIGNING
    return fleetAssignedJob || jobExperiencingAuction
  }

  shouldShowForceServiceModal(jobId, status, skipForcedDecision) {
    const jobHasAmbiguousService = [
      Consts.TOWIFJUMPFAILS,
      Consts.TOWIFTIRECHANGEFAILS,
    ].includes(JobStore.get(jobId, 'service'))

    const isFeatureDisabled = FeatureStore.isFeatureEnabled(
      Consts.FEATURE_DISABLE_FORCE_SERVICE_RESOLUTION,
    )

    const state = status === Consts.COMPLETED &&
      jobHasAmbiguousService &&
      !isFeatureDisabled &&
      !skipForcedDecision

    return state
  }

  shouldShowOnSiteModal(jobId, status, skipShowingOnSiteModal) {
    const toa = JobStore.get(jobId, 'toa')
    const isStatusAfterOnSite = [
      Consts.TOWING,
      Consts.TOWDESTINATION,
      Consts.COMPLETED,
    ].includes(status)
    const isJobMissingOnSite = !JobStore.get(jobId, `history.${Consts.ONSITE}`)
    const hasToa = toa && (toa.live || toa.latest)
    const eta = toa && moment(toa.live || toa.latest || toa.original)
    const isPastTime = eta && moment().subtract(Consts.ON_SITE_POPUP_THRESHOLD_IN_MINUTES, 'minutes').isAfter(eta.toDate()) && status === Consts.ONSITE
    const requireOnSite = !!SettingStore.getSettingByKey('require_on_site')
    const isFleetManagedJob = JobStore.get(jobId, 'type') === 'FleetManagedJob'

    const state = (
      !skipShowingOnSiteModal &&
      isStatusAfterOnSite &&
      isJobMissingOnSite &&
      hasToa &&
      (requireOnSite || isFleetManagedJob)
    ) || (!skipShowingOnSiteModal && hasToa && isPastTime)

    return state
  }

  getSchedulableJobStatus(job) {
    let { status } = job
    const jobIsAssignable = [Consts.PENDING, Consts.ACCEPTED].includes(status) || (UserStore.isPartner() && status === Consts.REASSIGN)
    const jobIsScheduled = job.scheduled_for !== null && jobIsAssignable
    if (jobIsScheduled) status = Consts.SCHEDULED
    return status
  }

  /**
   * The default statuses that will show up for a Job when you click
   * to manage its status manually in the table.
   */

  getJobStatusOptions(job) {
    const status = this.getSchedulableJobStatus(job)

    let options = []

    if (![...Consts.DISPATCHED_STATUSES, Consts.COMPLETED].includes(status)) {
      options.push(status)
    }

    if (UserStore.isPartner() && Consts.DISPATCHED_STATUSES.includes(status)) {
      if (Utils.isAssignedFromFleet(job)) {
        if (!job.fleet_manual) {
          options.push(Consts.ACCEPTED)
        }
      } else {
        options.push(Consts.PENDING)
      }
    }

    if (status === Consts.RELEASED) {
      if (UserStore.isPartner() && job.type === Consts.JOB.TYPE.RESCUE) {
        options = [Consts.RELEASED, Consts.STORED]
      } else {
        options = []
      }
    } else {
      options.push(
        Consts.DISPATCHED,
        Consts.ENROUTE,
        Consts.ONSITE,
        Consts.TOWING,
        Consts.TOWDESTINATION,
        Consts.COMPLETED,
      )
    }

    return options
  }

  render() {
    super.render(...arguments)

    TimeStore.watchMinute(this.getId())

    const status = this.getStatus()
    const job = JobStore.getJobById(this.props.job?.id)

    const style = this.props.getStatusStyle(job, status) || {}

    return (
      <div className="JobStatus" key={JSON.stringify(style)} style={style}>
        <span className="JobStatus-text">{status}</span>
        <span className="JobStatus-hack">
          {this.state.rev}
        </span>
      </div>
    )
  }
}

export default withModalContext(Status)
