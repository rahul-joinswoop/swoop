import React from 'react'
import PropTypes from 'prop-types'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import DeleteModal from 'components/modals/delete_job'
import GoaRateModal from 'components/job/GoaRateModal'
import JobBillingInfoStore from 'stores/JobBillingInfoStore'
import JobStore from 'stores/job_store'
import ModalContent from 'components/modals_content'
import SimpleModal from 'components/modals/simple_modal'
import ModalStore from 'stores/modal_store'
import UserStore from 'stores/user_store'
import { withModalContext } from 'componentLibrary/Modal'

class MarkAsGoaModal extends BaseComponent {
  state = {
    billingInfo: null,
  }

  closeModal = () => {
    // backwards compatability
    ModalStore.closeModals()
    this.props.hideModals()
  }

  async componentDidMount() {
    super.componentDidMount()

    if (UserStore.isSwoop()) {
      const billingInfo = await JobBillingInfoStore.getBillingInfo(this.props.job.id)
      this.setState({ billingInfo })
    }
  }

  getDeleteModalContent(job) {
    const requireJobStatusReason = job?.issc_dispatch_request?.system === Consts.DIGITAL_DISPATCH.RSC

    return <DeleteModal
      component={React.createFactory(ModalContent)}
      newStatus={Consts.GOA}
      onCancel={this.closeModal}
      onConfirm={this.jobChangeSubmit}
      record={job}
      requireJobStatusReason={requireJobStatusReason}
    />
  }

  getModalContent() {
    const { job } = this.props

    if (UserStore.isSwoop()) {
      const { billingInfo } = this.state

      if (billingInfo) {
        if (JobBillingInfoStore.isVcc(billingInfo)) {
          return <GoaRateModal job={job} />
        } else {
          return this.getDeleteModalContent(job)
        }
      } else {
        // show spinner
        return <ModalContent showSpinner title="Mark as GOA" />
      }
    } else {
      return this.getDeleteModalContent(job)
    }
  }

  jobChangeSubmit = (job) => {
    JobStore.jobChanged(job)
    this.closeModal()
  }

  render() {
    super.render()

    return <SimpleModal modalContent={this.getModalContent()} show />
  }
}

MarkAsGoaModal.propTypes = {
  job: PropTypes.object.isRequired,
  // and all props from ModalContent to override them, please see components/modals_content for details
}

export default withModalContext(MarkAsGoaModal)
