import React from 'react'
import Utils from 'utils'
import Consts from 'consts'
import ReactDOMFactories from 'react-dom-factories'

BaseComponent = require('components/base_component')
InfoBubble = React.createFactory(
  require('components/info_bubble/info_bubble').default
)

JobStore = require('stores/job_store')

class ScheduledDateTimeInfoBubble extends BaseComponent
  constructor: (props) ->
    super(props)

  getStyle: ->
    if @props.style
      return @props.style

    return {
      color: '#AAA'
      fontSize: '1.3em'
    }

  getContent: ->
    jobScheduledFor = JobStore.get(@props.jobId, 'scheduled_for', @getId())

    ReactDOMFactories.div
      className: 'scheduled-time'
      ReactDOMFactories.span
        style:
          fontWeight: 'bold'
          display: 'block'
        Consts.SCHEDULED_JOB
      ReactDOMFactories.span null,
        Utils.formatDateTime(jobScheduledFor, true)

  render: ->
    super(arguments...)

    InfoBubble
      style: @getStyle()
      icon: "fa-calendar"
      content: @getContent()

module.exports = React.createFactory(ScheduledDateTimeInfoBubble)
