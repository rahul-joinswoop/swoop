import Utils from 'utils'
import EditButton from 'components/job/edit_button'
import UserStore from 'stores/user_store'
import { sample } from 'lodash'

function getResultFromRender(job) {
  jest.resetModules()
  return EditButton({ job })
}

function expectNothingForJobWithInvoiceState(job, invoiceState) {
  it(`renders nothing when invoice state is ${invoiceState}`, () => {
    // eslint-disable-next-line no-param-reassign
    job.invoices = [{ state: invoiceState }]
    const result = getResultFromRender(job)

    expect(result).toBeNull()
  })
}

beforeEach(() => {
  UserStore.isFleet = () => false
  UserStore.isFleetInHouse = () => false
  UserStore.isSwoop = () => false
  UserStore.isPartner = () => false
})

it('returns an <i> tag', () => {
  const result = getResultFromRender({})
  expect(result.type).toBe('i')
})

it('renders nothing when job status is Submitted', () => {
  const job = { status: 'Submitted' }
  const result = getResultFromRender(job)
  expect(result).toBeNull()
})

it('renders nothing when job status is Expired', () => {
  const job = { status: 'Expired' }
  const result = getResultFromRender(job)
  expect(result).toBeNull()
})

it('renders nothing when job status is Reassigned', () => {
  const job = { status: 'Reassigned' }
  const result = getResultFromRender(job)
  expect(result).toBeNull()
})

describe('when user is Fleet', () => {
  beforeEach(() => {
    UserStore.isFleet = () => true
  })

  const renderNothingForSelectedInvoiceStatesAndJobStatusAs = (jobStatus) => {
    describe(`when job status is ${jobStatus}`, () => {
      const job = { status: jobStatus }

      expectNothingForJobWithInvoiceState(job, 'partner_approved')

      expectNothingForJobWithInvoiceState(job, 'partner_sent')

      expectNothingForJobWithInvoiceState(job, 'fleet_approved')

      expectNothingForJobWithInvoiceState(job, 'fleet_downloaded')

      expectNothingForJobWithInvoiceState(job, 'swoop_approved_partner')

      expectNothingForJobWithInvoiceState(job, 'swoop_new')

      expectNothingForJobWithInvoiceState(job, 'swoop_sending_fleet')

      expectNothingForJobWithInvoiceState(job, 'swoop_sent_fleet')

      it('renders nothing when invoice is undefined', () => {
        const result = getResultFromRender(job)

        expect(result).toBeNull()
      })
    })
  }

  describe('when job status is forbidden', () => {
    renderNothingForSelectedInvoiceStatesAndJobStatusAs('Completed')

    renderNothingForSelectedInvoiceStatesAndJobStatusAs('Canceled')

    renderNothingForSelectedInvoiceStatesAndJobStatusAs('GOA')
  })

  describe('when job status allowed', () => {
    let job

    beforeEach(() => {
      job = { status: 'any other job status' }
    })

    it('renders an icon tag when the invoice is undefined', () => {
      job.invoices = []
      const result = getResultFromRender(job)

      expect(result.type).toBe('i')
    })
  })
})

describe('when user is partner', () => {
  beforeEach(() => {
    UserStore.isPartner = () => true
  })

  describe('when job assigned from fleet', () => {
    const job = { status: 'Assigned' }

    beforeEach(() => {
      Utils.isAssignedFromFleet = () => true
    })

    describe('when job status is Assigned', () => {
      expectNothingForJobWithInvoiceState(job, 'fleet_approved')

      expectNothingForJobWithInvoiceState(job, 'fleet_downloaded')

      expectNothingForJobWithInvoiceState(job, 'swoop_approved_partner')

      expectNothingForJobWithInvoiceState(job, 'swoop_new')

      expectNothingForJobWithInvoiceState(job, 'swoop_sending_fleet')

      expectNothingForJobWithInvoiceState(job, 'swoop_sent_fleet')

      it('renders an <i> tag when invoice is undefined', () => {
        job.invoices = []
        const result = getResultFromRender(job)

        expect(result.type).toBe('i')
      })
    })
  })

  describe('when job not assigned from fleet', () => {
    const job = { status: 'Assigned' }

    beforeEach(() => {
      Utils.isAssignedFromFleet = () => false
    })

    it('renders an icon tag', () => {
      job.invoices = [{ state: 'fleet_approved' }]
      const result = getResultFromRender(job)

      expect(result.type).toBe('i')
    })
  })
})

describe('when user is Fleet InHouse', () => {
  let job

  beforeEach(() => {
    UserStore.isFleetInHouse = () => true
  })

  describe('when job status is forbidden', () => {
    beforeEach(() => {
      job = { status: sample(['Completed', 'Canceled', 'GOA']) }
    })

    describe('when invoice state is partner_approved', () => {
      it('renders an icon tag', () => {
        job.invoices = [{ state: 'partner_approved' }]
        const result = getResultFromRender(job)

        expect(result.type).toBe('i')
      })
    })

    describe('when invoice state is partner_sent', () => {
      it('renders nothing', () => {
        job.invoices = [{ state: 'partner_sent' }]
        const result = getResultFromRender(job)

        expect(result).toBeNull()
      })
    })

    it('renders nothing when invoice is undefined', () => {
      job.invoices = []
      const result = getResultFromRender(job)

      expect(result).toBeNull()
    })
  })

  describe('when job status allowed', () => {
    beforeEach(() => {
      job = { status: 'any other job status' }
    })

    it('renders an icon tag when the invoice is undefined', () => {
      job.invoices = []
      const result = getResultFromRender(job)

      expect(result.type).toBe('i')
    })
  })
})

describe('when user is Swoop', () => {
  let job

  beforeEach(() => {
    UserStore.isSwoop = () => true
  })

  describe('when job status is forbidden', () => {
    beforeEach(() => {
      job = { status: sample(['Completed', 'Canceled', 'GOA']) }
    })

    describe('when invoice state is partner_approved', () => {
      it('renders an icon tag', () => {
        job.invoices = [{ state: 'partner_approved' }]
        const result = getResultFromRender(job)

        expect(result.type).toBe('i')
      })
    })

    describe('when invoice state is partner_sent', () => {
      it('renders nothing', () => {
        job.invoices = [{ state: 'partner_sent' }]
        const result = getResultFromRender(job)

        expect(result).toBeNull()
      })
    })

    it('renders nothing when invoice is undefined', () => {
      const result = getResultFromRender(job)

      expect(result).toBeNull()
    })
  })

  describe('when job status allowed', () => {
    beforeEach(() => {
      job = { status: 'any other job status' }
    })

    it('renders an icon tag when the invoice is undefined', () => {
      job.invoices = []
      const result = getResultFromRender(job)

      expect(result.type).toBe('i')
    })
  })
})
