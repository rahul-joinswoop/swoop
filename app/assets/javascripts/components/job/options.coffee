import React from 'react'
import Utils from 'utils'
import Consts from 'consts'
UserStore = require('stores/user_store')
ModalStore = require('stores/modal_store')
JobStore = require('stores/job_store')
ProviderStore = require('stores/provider_store')
FeatureStore = require('stores/feature_store')
AsyncRequestStore = require('stores/async_request_store').default
BaseComponent = require('components/base_component')
import { partial } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'
import MarkAsGoaModal from 'components/job/MarkAsGoaModal'
import { withModalContext } from 'componentLibrary/Modal'
import OptionsModalImport from 'components/modal_options'
OptionsModal = React.createFactory(OptionsModalImport)
TransitionModal = React.createFactory(require 'components/modals')
JobDetailsEmailRecipients = React.createFactory(require('components/modals/job_details_email_recipients'))
RequestPhoneCallBack = React.createFactory(require("components/modals/request_phone_call_back").default)

class JobOptions extends BaseComponent
  copyJob: (job, e) =>
    copyable = (not Utils.isAssignedFromFleet(job) and UserStore.isPartner()) or not UserStore.isPartner()

    @props.showModal(
      TransitionModal
        title: "Duplicate Job"
        confirm: 'OK'
        onConfirm: () =>
          if copyable
            JobStore.copyJob(job.id)
          @props.hideModals()
        show: true
        extraClasses: "simple_modal"
        onCancel: @props.hideModals
        disabled: false
        if copyable
          "Are you sure you want to duplicate this job?"
        else
          "You cannot duplicate a job from "+ JobStore.getAccountNameByJobId(job?.id, @getId())+". Please ask "+ JobStore.getAccountNameByJobId(job?.id, @getId())+" to duplicate the job and assign it you."
    )

    e.preventDefault()
    e.stopPropagation()

  checkDeleteDraft: (job, e) =>
    @props.showModal(
      TransitionModal
        title: job.id+": Delete Draft"
        confirm: 'Yes'
        cancel: 'No'
        onConfirm: () =>
          JobStore.jobChanged({
            id: job.id
            status: Consts.DELETED
          })
          @props.hideModals()
        show: true
        extraClasses: "simple_modal"
        onCancel: @props.hideModals
        disabled: false
        span null,
          div null,
            "Are you sure you'd like to delete this draft?"
    )

  checkDelete: (job, e) =>
    @props.showModal(
      TransitionModal
        show: true
        extraClasses: "simple_modal"
        onCancel: @props.hideModals
        disabled: false
        title: job.id + ": Delete Job"
        confirm: 'Delete'
        cancel: 'Cancel'
        onConfirm: () =>
          JobStore.jobChanged({
            id: job.id
            status: Consts.DELETED
          })
          @props.hideModals()
        span null,
          div null,
            "Are you sure you want to permanently delete job ##{job.id}? All data for this job will be lost, including an invoice."
    )

  addJobDetailsEmailRecipients: (job, e) ->
    @props.showModal(
      JobDetailsEmailRecipients
        job: job
        show: true
        onCancel: @props.hideModals
    )

  requestCallback: (job, e) =>
    @props.showModal(
      RequestPhoneCallBack
        show: true
        record: job
        onCancel: @props.hideModals
        onConfirm: @requestPhoneCallBack
    )

  requestPhoneCallBack: (job, requestCallBackObj) =>
    JobStore.requestCallbackRequest(job, requestCallBackObj)
    @props.hideModals()

  requestMoreTimeToAccept: (job, e) ->
    # will show the spinner instead of timer on the motorclub job row
    @props.onRequestMoreTime(true)

    AsyncRequestStore.rscRequestMoreTime(job.id, {
      success: (data) =>
        # we hide the spinner; BE will push the job with the new timer
        # so it will be displayed
        @props.onRequestMoreTime(false)
        Tracking.trackDispatchEvent('Request More Time')
      error: (data) =>
        # we hide the spinner and show a notification error
        @props.onRequestMoreTime(false)
        Utils.sendNotification("Job #{job.id} #{data.error.request_more_time[0]}", Consts.ERROR)
    })

    Dispatcher.send(ModalStore.CLOSE_MODALS)

  checkCancel: (job, e) ->
    Dispatcher.send(ModalStore.CLOSE_MODALS)
    @props.hideModals()
    if job?.issc_dispatch_request?.system == Consts.DIGITAL_DISPATCH.RSC
      Utils.showModal('delete', e, {
        record: job,
        requireJobStatusReason: true,
        newStatus: Consts.CANCELED
      })
    else
      Utils.showModal('delete', e, {
        record: job,
        newStatus: Consts.CANCELED
        })

  checkGOA: (job, e) ->
    this.props.showModal(<MarkAsGoaModal job={job} />)

    e?.preventDefault?()
    e?.stopPropagation?()

  setFleetManual: (job, e) =>
    @props.showModal(
      TransitionModal
        show: true
        extraClasses: "simple_modal"
        onCancel: @props.hideModals
        disabled: false
        title: 'Manage Status'
        confirm: 'OK'
        cancel: 'Cancel'
        onConfirm: () =>
          JobStore.jobChanged({
            id: job.id
            fleet_manual: true
          })

          @props.hideModals()
        Consts.JOB.MANAGE_STATUS_WARN
    )

  showCancel: (job) ->
    if job.status == Consts.DRAFT
      return false
    else if UserStore.isPartner() and job.type == Consts.JOB.TYPE.RESCUE and job.status in [Consts.COMPLETED]
      return true
    else if UserStore.isFleetInHouse() and job.status in [Consts.COMPLETED]
      return true
    else if job.status in [Consts.COMPLETED, Consts.CANCELED, Consts.EXPIRED, Consts.REASSIGNED] or ((job.type == "FleetInHouseJob" or job.type == "FleetMotorClubJob") and UserStore.isRoot())
      return false
    else if Utils.isAssignedFromFleet(job) and UserStore.isPartner() and job.status in [Consts.SUBMITTED, Consts.ASSIGNED]
      return false
    return true

  showDeleteDraft: (job) ->
    job.status == Consts.DRAFT

  showRequestCallback: (job) ->
    return JobStore.shouldShowRequestCallback(job)

  showDuplicateJob: (job) ->
    return job.type != "FleetMotorClubJob"

  # root users are able to delete jobs.
  # also, a partner can delete jobs if:
  # - the job was created by the partner
  # - the job has status 'completed'
  showDelete: (job) ->
    if job.status in [Consts.DRAFT, Consts.PENDING, Consts.ACCEPTED, Consts.ASSIGNED, Consts.DISPATCHED, Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION]
      return false
    return UserStore.isRoot() ||
        (UserStore.isFleetInHouse() &&
        UserStore.isAdmin() &&
        (job.status == Consts.COMPLETED || job.status == Consts.CANCELED)) ||
        (UserStore.isPartner() &&
          UserStore.isAdmin() &&
          job.type == Consts.JOB.TYPE.RESCUE &&
          (job.status == Consts.COMPLETED || job.status == Consts.CANCELED)) &&
          !FeatureStore.isFeatureEnabled(Consts.FEATURES_DISABLE_DELETE_JOB)

  showEmailJobDetails: (job) ->
    UserStore.isRoot() || UserStore.isAdmin() || UserStore.isDispatcher() || UserStore.isSwoopDispatcher()

  showManageStatus: (job) ->
    (UserStore.isSwoop() || UserStore.isFleetInHouse()) &&
      (job.fleet_manual != true) && job.rescue_company?

  showMarkAsGOA: (job) =>
    if UserStore.isFleetManaged()
      return false
    if UserStore.isFleet()
      if !job.rescue_company?
        return false
      providerLive = ProviderStore.getPropertyBySiteId(job.site_id, 'live', @getId())

      if providerLive and not job.fleet_manual
        return false
    return job.status in [Consts.ACCEPTED, Consts.PENDING, Consts.UNASSIGNED, Consts.DISPATCHED, Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION, Consts.COMPLETED, Consts.CANCELED]

  showJobOptions: (job, e) =>
    @props.showModal(
      OptionsModal
        target: e.target
        offsetTop: 25
        offsetLeft: -60
        id: job.id
        onUnfocus: @rerender
        options: [
          if @showRequestCallback(job)
            li
              key: "callback"
              onClick: partial(@requestCallback, job)
              "Request Call Back"
          if JobStore.shouldShowRequestMoreTimeToAccept(job)
            li
              key: "callback"
              onClick: partial(@requestMoreTimeToAccept, job)
              "Request More Time"
          if @showDuplicateJob(job)
            li
              key: "duplicate"
              onClick: partial(@copyJob, job)
              "Duplicate Job"
          if @showManageStatus(job)
            ReactDOMFactories.li
              key: 'manageStatus'
              onClick: partial(@setFleetManual, job)
              'Manage Status'
          if @showMarkAsGOA(job)
            li
              key: "goa"
              onClick: partial(@checkGOA, job)
              "Mark as GOA"
          if @showCancel(job)
            li
              key: "cancel"
              onClick: partial(@checkCancel, job)
              "Cancel Job"
          if @showDeleteDraft(job)
            li
              key: "delete"
              onClick: partial(@checkDeleteDraft, job)
              "Delete Draft"
          if @showDelete(job)
            li
              key: "delete"
              onClick: partial(@checkDelete, job)
              "Permanently Delete"
          if @showEmailJobDetails(job)
            li
              key: "email_job_details"
              onClick: partial(@addJobDetailsEmailRecipients, job)
              "Email Job Details"
        ]
    )

    e.stopPropagation()
    e.preventDefault()
    if @props?.update?
      @props.update()

  render: ->
    super(arguments...)

    job = @props.job
    show = @showMarkAsGOA(job) || @showCancel(job) || @showDuplicateJob(job) || @showRequestCallback(job)
    ReactDOMFactories.i
      style:
        visibility: if show then "" else "hidden"
      onClick: partial(@showJobOptions, job)
      className: 'fa fa-ellipsis-h options inline ' + (if window.active_options_id == job.id then "active" else "")
      name: 'options'

module.exports = withModalContext(JobOptions)
module.exports.Component = JobOptions