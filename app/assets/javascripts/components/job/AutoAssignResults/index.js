/* eslint react/no-multi-comp: 0 */

import React from 'react'
import AuctionStore from 'stores/auction_store'
import CompanyStore from 'stores/company_store'
import Consts from 'consts'
import CountdownSeconds from 'componentLibrary/CountdownSeconds'
import InfoBubble from 'components/info_bubble/info_bubble'
import JobStore from 'stores/job_store'
import moment from 'moment-timezone'
import StoreWrapper from 'StoreWrapper'
import { find, orderBy } from 'lodash'
import { getCurrencySymbol } from 'lib/localize'
import './style.scss'

const sortBids = data => orderBy(data, [o => o.status === 'won', 'score'], ['desc', 'desc'])

const transformBidsToBidItems = (bids, candidates) => {
  const data = bids.map((bid) => {
    const candidate = find(candidates, candidate => candidate.rescue_company_id === bid.company_id)
    return {
      id: bid.id,
      company: bid.company_name,
      provider: Consts.TRUCK_CANDIDATE.has(candidate.type) && candidate.vehicle ? candidate.vehicle.name : candidate.site.name,
      type: Consts.TRUCK_CANDIDATE.has(candidate.type) ? 'truck' : 'building-o',
      eta: bid.submitted_eta || 0,
      quality: bid.rating || 0,
      cost: bid.cost ? `$${bid.cost}` : 0,
      score: Math.ceil(bid.swoop_score * 100) || 0,
      status: (bid.status).replace(/_/g, ' '),
    }
  })
  return data
}

const sortCandidates = data => orderBy(data, [o => o.status === 'eligible', 'score'], ['desc', 'desc'])

const transformCandidatesToCandidateItems = (candidates) => {
  const data = candidates.map(candidate => ({
    id: candidate.id,
    company: candidate.company_name,
    provider: Consts.TRUCK_CANDIDATE.has(candidate.type) && candidate.vehicle ? candidate.vehicle.name : candidate.site.name,
    type: Consts.TRUCK_CANDIDATE.has(candidate.type) ? 'truck' : 'building-o',
    eta: candidate.eta,
    quality: candidate.rating || 0,
    cost: candidate.cost ? `$${candidate.cost}` : 0,
    score: Math.ceil(candidate.swoop_score * 100) || 0,
    status: (candidate.vehicle_eligible).replace(/_/g, ' '),
  }))
  return data
}

const CandidatesTableEntry = ({ item }) => (
  <tr>
    <td title={item.company}>{item.company}</td>
    <td title={item.provider}><i className={`fa fa-${item.type}`} /> {item.provider}</td>
    <td>{item.eta}</td>
    <td>{item.quality}</td>
    <td>{item.cost}</td>
    <td>{item.score}</td>
    <td title={item.status}>{item.status}</td>
  </tr>
)

const AutoAssignTab = ({
  currencySymbol, job, maxBids, rules,
}) => {
  const { auction, candidates } = job
  const { bids } = auction
  const tableHeaders = ['Company', 'Candidate', 'ETA', 'Quality', 'Cost', 'Score', 'Status']
  const winner = find(bids, bid => bid.status === 'won') || null

  const sortedBids = sortBids(transformBidsToBidItems(bids, candidates))
  const sortedCandidates = sortCandidates(transformCandidatesToCandidateItems(candidates))

  let expiresAt
  if (AuctionStore.jobHasLiveAuction(job)) {
    expiresAt = AuctionStore.getExpiresAtByJob(job)
  } else if (JobStore.isMotorClub(job)) {
    expiresAt = job.answer_by
  }

  if (!auction) {
    return null
  }

  return (
    <div className="component-auto-assign-results">
      <div>
        <p className="section-title">
          Auto Assign Rules
          <InfoBubble
            content="Auto Assign rules are set per Client."
            icon="fa-info-circle"
            style={{
              color: '#7fbce2',
              margin: '0 1ch',
            }}
          />
        </p>
        <ul className="auto-assign-rules">
          <li>
            <ul>
              <li><strong>{rules.constraints.min_eta} - {rules.constraints.max_eta} min</strong> ETA Range</li>
              <li><strong>{currencySymbol}{rules.constraints.min_cost} - {currencySymbol}{rules.constraints.max_cost}</strong> Cost Range</li>
              <li><strong>{rules.constraints.min_rating} - {rules.constraints.max_rating}</strong> Quality Range</li>
            </ul>
          </li>
          <li>
            <ul>
              <li>
                <span><strong>{rules.weights.speed * 100}% ETA</strong>, </span>
                <span><strong>{rules.weights.cost * 100}% Cost</strong>, </span>
                <span><strong>{rules.weights.quality * 100}% Quality</strong> </span>
                <span>Weight Adjustments</span>
              </li>
              <li><strong>{maxBids}</strong> Bids Allowed</li>
            </ul>
          </li>
        </ul>
      </div>

      {/*
      <div>
        <p><strong>Company Selection ... ?</strong></p>
      </div>
      */}

      <div>
        <p className="section-title">
          Candidates
          <InfoBubble
            content="Candidates are Sites and Trucks of eligible companies. An estimated Swoop score is calculated for each Candidate to determine which will become Bids."
            icon="fa-info-circle"
            style={{
              color: '#7fbce2',
              margin: '0 1ch',
            }}
          />
        </p>
        <table>
          <thead>
            <tr>{tableHeaders.map(title => <th key={title}>{title}</th>)}</tr>
          </thead>
          <tbody>
            { sortedCandidates.map(candidate => (
              <CandidatesTableEntry item={candidate} key={candidate.id} />
            ))}
          </tbody>
        </table>
      </div>

      <div>
        <p className="section-title">
          Bids
          <InfoBubble
            content="Partners offered a Bid must accept the and submit their ETA within the time limit. The Swoop score for each Bid is calculated to determine the winner."
            icon="fa-info-circle"
            style={{
              color: '#7fbce2',
              display: 'inline-block',
              margin: '0 1ch',
            }}
          />
          { expiresAt &&
            <span className="auction-countdown">
              <CountdownSeconds
                suffix={false}
                time={moment(expiresAt)}
              /> seconds remaining
            </span>}
        </p>
        <table>
          <thead>
            <tr>{tableHeaders.map(title => <th key={title}>{title}</th>)}</tr>
          </thead>
          <tbody>
            { sortedBids.map(bid => (
              <CandidatesTableEntry item={bid} key={bid.id} />
            ))}
          </tbody>
        </table>
      </div>

      { winner &&
        <div className="auction-winner">
          <p><strong>{winner.company_name}</strong> won, with score of <strong>{Math.ceil(winner.swoop_score * 100) || 0}</strong>.</p>
        </div>}
    </div>
  )
}

const AutoAssignResults = StoreWrapper(AutoAssignTab, function getProps() {
  const company = CompanyStore.getById(this.props.job.owner_company.id, this.getId())
  const rules = CompanyStore.get(this.props.job.owner_company.id, 'autoassignment_ranker', this.getId())
  return {
    currencySymbol: getCurrencySymbol(company?.currency),
    maxBids: company?.auction_max_bidders,
    rules,
  }
})

export { AutoAssignTab } // export for testing
export default AutoAssignResults
