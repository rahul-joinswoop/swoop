import React from 'react'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import Dropzone from 'react-dropzone'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import JobStore from 'stores/job_store'
import Loading from 'componentLibrary/Loading'
import Logger from 'lib/swoop/logger'
import S3Upload from 'S3Upload'
import Utils from 'utils'
import { partial } from 'lodash'
import 'stylesheets/components/job/document_list.scss'

class DocumentList extends React.Component {
  constructor(props) {
    super(props)
    this.dropzone = React.createRef()
    this.state = {
      rev: 0,
    }
  }

  componentDidMount() {
    JobStore.bind(`${JobStore.CHANGE}_${this.props.job_id}`, this.generalChanged)
  }

  componentWillUnmount() {
    JobStore.unbind(`${JobStore.CHANGE}_${this.props.job_id}`, this.generalChanged)
  }

  generalChanged = () => {
    this.setState(prevState => ({
      rev: prevState.rev + 1,
    }))
  }

  getItems = () => JobStore.getAttachedDocuments(this.getJob()?.id, Consts.TYPE_ATTACHED_DOCUMENT) || []

  getJob = () => JobStore.getJobById(this.props.job_id)

  getSignedUrl = (file, callback) => JobStore.documentUploadRequest(this.props.job_id, file, callback)

  handleOnDrop = (files) => {
    const s3upload = new S3Upload({
      contentDisposition: 'auto',
      files,
      getSignedUrl: this.getSignedUrl,
      onError: this.onError,
      onFinishS3Put: this.onFinish,
      onProgress: this.onProgress,
      uploadRequestHeaders: {},
    })
  }

  onDelete = (item) => {
    JobStore.removeDocument(this.props.job_id, item.id)
    this.generalChanged()
  }

  onError = () => {
    Utils.sendNotification('Document failed to add to Job.', Consts.SUCCESS)
  }

  onFinish = (data) => {
    JobStore.createDocument(this.props.job_id, data.document)
  }

  onProgress = (data) => {
    Logger.debug('Progress', data)
  }

  renderElement = item =>
    <a
      className="link"
      href={item.url}
      rel="noopener noreferrer"
      target="_blank"
    >
      {item.original_filename}
    </a>

  renderRow = (item) => {
    if (this.showSpinner(item)) {
      return (
        <li
          key={`loading-${item.id}`}
        >
          <Loading
            display="inline-block"
            padding={0}
            size={16}
          />
        </li>
      )
    } else {
      return (
        <li
          key={item.id}
        >
          <FontAwesomeButton
            color="secondary-o"
            icon="fa-times-circle"
            onClick={partial(this.onDelete, item)}
            size="small"
          />
          <span className="element">
            {this.renderElement(item)}
          </span>
        </li>
      )
    }
  }

  showSpinner = item => (item && item.url === null)

  render() {
    return (
      <div className="documents-dropzone">
        <div className="add-list">
          { this.getItems().length > 0 && (
            <ul>
              { this.getItems().map(item => this.renderRow(item)) }
            </ul>
          )}
        </div>
        <Dropzone
          onDrop={this.handleOnDrop}
        >
          {({ getRootProps, getInputProps }) => (
            <section>
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                <Button
                  color="secondary-o"
                  onClick={null}
                  size="narrow"
                  style={{
                    margin: 2,
                  }}
                >
                  Add Document
                </Button>
              </div>
            </section>
          )}
        </Dropzone>
      </div>
    )
  }
}

export default DocumentList
