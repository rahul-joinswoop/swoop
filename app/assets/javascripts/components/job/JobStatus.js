import React from 'react'
import { JobStatus as JobStatusEnumType } from 'graphql/enums.json'

// map a JobStatusEnumType to the human readable version
const JobStatus = ({ status } = {}) =>
  <span>
    {JobStatusEnumType[status]}
  </span>

export default JobStatus
