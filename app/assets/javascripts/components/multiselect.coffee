import React from 'react'
require('components/multiselect.scss')
import Utils from 'utils'
BaseComponent = require('components/base_component')
UserStore = require ('stores/user_store')
BaseComponent = require('components/base_component')
UsersStore = require('stores/users_store').default
onClickOutside = require('react-onclickoutside').default
UsersStore = (require ('stores/users_store')).default
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
import { filter, isEmpty, partial } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'

filterId = 0
FilterOption = (props)  ->
  id = props.item.name + filterId
  li
    ref: "li#{props.index}"
    key: props.item.name + " #{props.index}"
    className: if props.isFocused then "focused" else ""
    if props.item.loading
      Loading
        size: 24
    else
      checked = props.item.selected
      if props.isAllSelected && props.item.name != 'All'
        checked = false
      input
        id: id
        type: "checkbox"
        checked: checked
        onChange: if !props.item.disabled then props.onChange
        disabled: props.item.disabled
    label
      htmlFor: id
      props.item.name

class MultiSelect extends BaseComponent
  displayName: 'MultiSelect'
  constructor: (props) ->
    super(props)
    @state = {
      open: false,
      searchString: '',
      focusedItem: 0
    }
    @allSearchFilteredItems = null

  componentDidMount: () =>
    super(arguments...)
    document.body.addEventListener('keydown', @handleKeyDown)

  componentWillUnmount: () =>
    super(arguments...)
    document.body.removeEventListener('keydown', @handleKeyDown)

  handleClickOutside: (event) =>
    if $(event.target).closest(".multiselect")[0]!=@refs["container"]
      @setState
        open: false

  centerDropDown: (isDirectionUp, currentItem) =>
    marginOffSetTop = @refs["li0"]?.offsetTop
    itemOffSetTop = @refs["li#{currentItem}"]?.offsetTop
    itemHeight = @refs["li#{currentItem}"]?.clientHeight
    dropdownContainerHeight = @refs?.dropdownContainer?.clientHeight
    dropdownContainerScrollTop = @refs?.dropdownContainer?.scrollTop
    if isDirectionUp
      if currentItem is 1
        Utils.smoothScrollVertical(@refs?.dropdownContainer, 0, false, 4)
      else if itemOffSetTop < dropdownContainerScrollTop
        Utils.smoothScrollVertical(@refs?.dropdownContainer, (dropdownContainerScrollTop - itemHeight), false, 4)
    else if itemOffSetTop + marginOffSetTop > dropdownContainerHeight + dropdownContainerScrollTop
      Utils.smoothScrollVertical(@refs?.dropdownContainer, (itemOffSetTop + marginOffSetTop - itemHeight), true, 4)

  handleKeyDown: (event) =>
    return unless @state.open
    keyCode = event.keyCode || event.which
    event.preventDefault() if keyCode is 40 or keyCode is 38 # kill browser ul scrolling when focused on ul
    currentItem = @state.focusedItem
    if keyCode is 38 and currentItem != 0 # keyboard press up and not on first item
      @setState {focusedItem: currentItem - 1}
      @centerDropDown(true, currentItem)
    else if keyCode is 40 and currentItem < @allSearchFilteredItems?.length - 1 # keyboard press down and not on last item
      @setState {focusedItem: currentItem + 1}
      @centerDropDown(false, currentItem)
    else if keyCode is 13 && !isEmpty(@allSearchFilteredItems) # keyboard press enter and there is an item to select
      @props.toggleEntry(@allSearchFilteredItems?[currentItem])

  getselectedItems: =>
    filter(@getAllItems(), (item) -> item.selected)

  getAllItems: =>
    @props.items

  getAllSelectedText: =>
    "All "+Utils.getPlural(@props.objectName)

  getNoneSelectedText: =>
    "No "+Utils.getPlural(@props.objectName)+" Selected"

  getLabel: =>
    "Filter by "+@props.objectName

  renderSummaryItem: (name, index) =>
    if name != 'Assigned To Me'
      li
        key: name + index
        name

  renderSummaryItems: (allItems, selectedItems) =>
    ul null,
      if @props.loading
        @renderSummaryItem("Loading...")
      else if selectedItems?
        if selectedItems.length == 0
          @renderSummaryItem(@getNoneSelectedText())
        else if selectedItems.length == allItems.length
          @renderSummaryItem(@getAllSelectedText())
        else
          count = 0
          for obj in selectedItems
            if count <= 2
              count++
              if count == 3 and selectedItems.length > 3
                @renderSummaryItem("& "+(selectedItems.length-2)+" more", count)
              else
                @renderSummaryItem(obj.name, count)

  renderSearch: () =>
    div
      className: 'search-input-container'
      input
        className: 'search-input'
        value: @state.searchString
        onFocus: (event) => @setState {focusedItem: 0}
        onChange: (event) => @setState {searchString: event.target.value, focusedItem: 0}
        placeholder: "Search"
      ReactDOMFactories.i
        className: if @state.searchString is "" then "fa fa-search" else "fa fa-times-circle"
        onClick: () => @setState {searchString: "", focusedItem: 0}
        tabIndex: -1

  normalizeWordForComparison: (word) =>
    return word.toLowerCase().replace(/[^a-zA-Z0-9]/gmi, "")

  isItemNameInSearch: (itemName) =>
    itemName = itemName.toString() if typeof itemName == 'number'
    return true if @normalizeWordForComparison(itemName).indexOf(@normalizeWordForComparison(@state.searchString)) is 0
    itemNameArray = itemName.split(' ')
    for name in itemNameArray
      return true if @normalizeWordForComparison(name).indexOf(@normalizeWordForComparison(@state.searchString)) is 0
    return false

  filterItemsBySearch: (allItems) =>
    if @state.searchString is ""
      return allItems
    filter(allItems, (item) => item.id? and @isItemNameInSearch(item.name))

  handleItemOnChange: (item, index) =>
    @setState {focusedItem: index}
    @props.toggleEntry(item)

  isAllSelected: (allItems) ->
    for item in allItems
      if item.name == 'All'
        return item.selected

  render: ->
    super(arguments...)

    selectedItems = @getselectedItems()
    allItems = @getAllItems()
    @allSearchFilteredItems = @filterItemsBySearch(allItems)

    div
      className: [
        'multiselect'
        @props.className if @props.className?
      ].join(' ')
      ref: "container"
      div
        className: 'selected_container'
        onClick: =>
          if !@props.loading
            @setState
              open: !@state.open
              searchString: ''
        @renderSummaryItems(allItems, selectedItems)
        ReactDOMFactories.i
          className: "fa " + (if !@state.open then "fa-chevron-down" else "fa-chevron-up")
      if @state.open
        div
          ref: "dropdownContainer"
          className: 'dropdown_container'
          if !@props.noSearch
            [
              @renderSearch()
              span null,
                @getLabel()
            ]
          ul
            ref: "ul"
            className: 'dropdownUl'
            for item, index in @allSearchFilteredItems
              FilterOption
                item: item
                index: index
                onChange: partial(@handleItemOnChange, item, index)
                isFocused: index is @state.focusedItem
                isAllSelected: @isAllSelected(allItems)

module.exports = onClickOutside(MultiSelect)
