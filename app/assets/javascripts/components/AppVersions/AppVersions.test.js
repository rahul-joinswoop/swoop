import React from 'react'
import { mount, shallow } from 'enzyme'
import AppVersions from 'components/AppVersions'

jest.mock('components/resizableTable/FTable', () => () => <div className="ftable" />)

describe('AppVersions View', () => {
  let component

  it('<AppVersions /> renders', () => {
    component = shallow(<AppVersions />)
    expect(component).toBeDefined()
  })

  it('Has Select Options', () => {
    component = mount(<AppVersions />)
    expect(component.find('.app-versions-select').exists()).toEqual(true)
    expect(component.find('.app-versions-select option')).to.have.lengthOf(4)
  })
})
