
import React from 'react'
import BaseComponent from 'components/base_component'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import FTable from 'components/resizableTable/FTable'
import Loading from 'componentLibrary/Loading'
import StoreWrapper from 'StoreWrapper'
import Utils from 'utils'
import VersionFormModal from 'components/version_form'
import { withModalContext } from 'componentLibrary/Modal'
import VersionStore from 'stores/version_store'
import './style.scss'
import { partial } from 'lodash'

const VersionTable = withModalContext(({
  name,
  versions,
  showModal,
}) => {
  function onClickEdit(entry) {
    showModal(<VersionFormModal name={entry.name} record={entry} />)
  }

  const cols = [
    { header: 'ID', key: 'id', width: 128 },
    { header: 'Version', key: 'version', width: 256 },
    { header: (name === 'web' ? 'Description' : 'User Message'), key: 'description' },
    {
      header: 'Created At', key: 'created_at', width: 128, func: version => Utils.formatDateTime(version.created_at),
    },
    {
      header: 'Forced', key: 'forced', width: 64, func: version => (version.forced ? '✓' : '▢'),
    },
    {
      header: '',
      key: 'edit',
      sortable: false,
      width: 64,
      func: version => (
        <FontAwesomeButton
          icon="fa-pencil"
          onClick={partial(onClickEdit, version)}
        />
      ),
    },
  ]
  return (
    <div className="version-list">
      <FTable
        cols={cols}
        key={name}
        rows={versions}
        sortDir={-1}
        sortKey="id"
      />
      { VersionStore.loadingVersion(name) &&
        <Loading />}
    </div>
  )
})

class AppVersions extends BaseComponent {
  state = {
    currentVersion: 'Web',
  }

  handleClickOnTab = (e) => {
    this.setState({
      currentVersion: e.target.innerText,
    })
  }

  handleSelect = (e) => {
    this.setState({
      currentVersion: e.target.value,
    })
  }

  onClickAdd = (name) => {
    this.props.showModal(<VersionFormModal name={name} />)
  }

  render() {
    const {
      androidVersions,
      iOSVersions,
      mobileVersions,
      webVersions,
    } = this.props

    const { currentVersion } = this.state
    const names = ['Web', 'Android', 'iOS', 'Mobile']

    return (
      <>
        <div className="app-versions-select">
          <select onChange={this.handleSelect} value={currentVersion}>
            { names.map(name => (
              <option key={name} value={name}>{name} Versions</option>
            ))}
          </select>

          <FontAwesomeButton
            className="add-version-button"
            color="secondary"
            icon="fa-plus"
            onClick={partial(this.onClickAdd, this.state.currentVersion.toLowerCase())}
            size={22}
          />
        </div>

        <div className="app-versions-list">
          { currentVersion === 'Android' &&
            <VersionTable name="android" versions={androidVersions} />}

          { currentVersion === 'iOS' &&
            <VersionTable name="ios" versions={iOSVersions} />}

          { currentVersion === 'Mobile' &&
            <VersionTable name="mobile" versions={mobileVersions} />}

          { currentVersion === 'Web' &&
            <>
              <VersionTable name="web" versions={webVersions} />
            </>}
        </div>
      </>
    )
  }
}

AppVersions = StoreWrapper(AppVersions, function getProps() {
  return {
    androidVersions: VersionStore.getAndroidVersions(this.getId()),
    iOSVersions: VersionStore.getIOSVersions(this.getId()),
    mobileVersions: VersionStore.getMobileVersions(this.getId()),
    webVersions: VersionStore.getWebVersions(this.getId()),
  }
})

export default withModalContext(AppVersions)
