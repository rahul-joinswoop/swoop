import React from 'react'
import Consts from 'consts'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Utils from 'utils'
import { extend, filter, find, map, partial, reject, sortBy, uniq, values } from 'lodash'
import 'stylesheets/components/modal_invoice.scss'

BaseComponent = require('components/base_component')
DocumentList = React.createFactory(require('components/job/document_list').default)
FeatureStore = require('stores/feature_store')
FormSection = React.createFactory(require('components/form_section'))
InvoiceFields = require 'invoice_fields'
InvoiceHelper = require('helpers/invoice')
InvoiceStore = require ('stores/invoice_store')
JobStore = require ('stores/job_store')
ModalStore = require ('stores/modal_store')
import PaymentMethodStore from 'stores/payment_method_store'
PopupBubble = React.createFactory(require('components/popups/popup_bubble'))
RateStore = require ('stores/rate_store')
Renderer = require('form_renderer')
ServiceStore = require('stores/service_store')
TransitionModal = React.createFactory(require 'components/modals_new')
UserStore = require('stores/user_store')

import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

SWOOP_ADDONS = []

class InvoiceModal extends BaseComponent
  displayName: 'InvoiceModal'
  constructor: (props) ->
    super(props)

    if UserStore.isSwoop()
      SWOOP_ADDONS = [
        {id: 0, name: "Admin Fee"}
        {id: 1, name: "Passthrough"}
        {id: 2, name: "Reimbursement"}
        {id: 3, name: "Roadside Fee"}
        {id: 4, name: "Service Fee"}
      ]

      if FeatureStore.isFeatureEnabled(Consts.FEATURE_INVOICE_CUSTOM_ITEMS)
        SWOOP_ADDONS.push({id: 5, name: "Custom Item"})

    @state = @getInitialState()

    record_id = props.record

    if record_id?
      invoice = InvoiceStore.getInvoiceById(record_id)
      if invoice?
        invoice.job = JobStore.getJobById(invoice.job_id)

      if invoice?.payments
        invoice.payments = sortBy(invoice.payments, 'id')

      @form = new Renderer(invoice || {id: record_id}, @rerender, {
        componentId: @getId()
      })
      @helper = new InvoiceHelper(@form, @)

      fields = {
        DriverNotesField: InvoiceFields.DriverNotesField,
        RateTypeField: InvoiceFields.RateTypeField,
        InvoiceVehicleCategoryIdField: InvoiceFields.InvoiceVehicleCategoryIdField,
        LineItemNameFactory: InvoiceFields.LineItemNameFactory,
        LineItemQtyFactory: InvoiceFields.LineItemQtyFactory,
        LineItemPriceFactory: InvoiceFields.LineItemPriceFactory,
        LineItemTotalFactory: InvoiceFields.LineItemTotalFactory,
        SubTotalField: InvoiceFields.SubTotalField,
        LineItemPercentFactory: InvoiceFields.LineItemPercentFactory,
        LineItemPercentTotalFactory: InvoiceFields.LineItemPercentTotalFactory,
        LineItemTaxInputFactory: InvoiceFields.LineItemTaxInputFactory,
        LineItemTaxFactory: InvoiceFields.LineItemTaxFactory,
        TotalField: InvoiceFields.TotalField,
        StoredDateField: InvoiceFields.StoredDateField,
        ReleasedDateField: InvoiceFields.ReleasedDateField,
        PaymentTypeFactory: InvoiceFields.PaymentTypeFactory,
        PaymentNameFactory: InvoiceFields.PaymentNameFactory,
        PaymentMemoFactory: InvoiceFields.PaymentMemoFactory,
        PaymentDateFactory: InvoiceFields.PaymentDateFactory,
        PaymentAmountFactory: InvoiceFields.PaymentAmountFactory,
        BalanceField: InvoiceFields.BalanceField,
        BillingTypeField: InvoiceFields.BillingTypeField,
      }
      @form.registerFields(fields)
      @form.isValid()
      @register(InvoiceStore, InvoiceStore.CHANGE)

  componentDidMount: ->
    record_id = @props.record

    InvoiceStore.bind(InvoiceStore.CHANGE+"_"+record_id, @updateRenderer)
    super(arguments...)

  componentWillUnmount: ->
    InvoiceStore.unbind(InvoiceStore.CHANGE+"_"+@form?.record?.id, @updateRenderer)
    super(arguments...)

  getInitialState: ->
    loadingRate: false

  setRateTypes: (account_id) =>
    rates = RateStore.getRatesByObject(
      account_id:  account_id
    )
    @setState
      allowedTypes: uniq(map(rates, (rate) => rate.type))

  updateRenderer: =>
    #TODO: this only replaces the invoice if we don't know about it yet,
    #Should really be smarter about it (maybe popup message if someone else changes invoice)

    if !@form?.record?.line_items?
      invoice = InvoiceStore.getInvoiceById(@form.record.id)
      @form.replaceRecord(invoice)
      @rerender()

  isSwoopFleetInvoice: =>
    return UserStore.isSwoop() and @form.record?.sender?.id == UserStore.getCompany().id

  renderViewTypes: =>
    propsWithLoadCallback = {
      containerStyle:
        width: 200
        marginRight: 20
    }
    propsWithLoadCallback.onLoad = =>
      console.log("RATE TYPE: loading")
      @setState
        spinner: true
    propsWithLoadCallback.onComplete = =>
      console.log("RATE TYPE: complete")
      @setState
        spinner: false
    propsWithLoadCallback.onError = =>
      console.log("RATE TYPE: ERROR WITH RATE LOADING")

    fields = []

    if !@isSwoopFleetInvoice()
      fields.push(['rate_type', propsWithLoadCallback])
      fields.push(["job.invoice_vehicle_category_id", propsWithLoadCallback])
      fields.push(['billing_type', propsWithLoadCallback])

    fields.push(['job.history.Stored', {}])
    fields.push(["job.history.Released", {}])

    if !Utils.isInvoiceEditable(@form.record)
      if @form.record.state in [Consts.IPARTNERSENT]
        div
          style:
            color: "#999"
            marginTop: 5
            marginBottom: 10
          @form.record.recipient.name+" has already received this invoice. To make edits move the invoice back to the New state."
      else if @form.record.state in [Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED, Consts.ISWOOPDOWNLOADEDPARTNER, Consts.ISWOOPAPPROVEDPARTNER]
        div
          style:
            color: "#999"
            marginTop: 5
            marginBottom: 10
          @form.record.recipient.name+" has already approved this invoice. Contact "+@form.record.recipient.name+" to make edits."

    div
      className: "invoice-view-types"
      @form.renderInputs(fields)


  addNewItemChange: (e) =>
    id = e.target.value
    if not id? or id == "-1"
      return

    if id == 0 || id == '0'
      addon = name: ""
    else if UserStore.isSwoop()
      addons = @helper.getAdditionalItems(@getId())
      addon = find(addons, (addon) -> ""+addon.id == ""+id)
    else
      addon = ServiceStore.getById(id)

    @helper.addItem(parseInt(id), addon?.name)

    e.preventDefault()
    e.stopPropagation()

  addNewItemSwoopChange: (e) =>
    id = e.target.value
    if not id? or id == "-1"
      return

    obj = {
      description: SWOOP_ADDONS[id].name
      unit_price: 0
      net_amount: 0
      estimated: false
      user_created: true
      job_id: @form.record.job_id
    }

    if SWOOP_ADDONS[id].name == "Custom Item"
      obj.custom = true
      obj.description = ""

    @form.record.line_items.push(obj)
    @rerender()

  addNewTransaction: (e) =>
    name = e.target.value
    if not name? or name == "0"
      return

    @helper.addTransaction(name)

    e.preventDefault()
    e.stopPropagation()

  renderBalance: =>
    div
      className: 'totalsList balanceList'
      div
        className: "balance"
        div null,
          "Balance"
        div
          id: "invoice_modal_balance_container"
          @form.renderInput("balance", {containerClass: 'price-group fs-exclude'})

  renderTotals: =>

    div
      className: "totalsList " + (if @isSwoopFleetInvoice() then "swoop" else "")
      div
        className: 'subtotal'
        div null,
          "Subtotal:"
        @form.renderInput("subtotal", {containerClass: "price-group fs-exclude"})

      for index, item of @helper.getPercentLineItems()
        fields = []
        fieldClass = InvoiceFields["LineItemNameFactory"](item, index)
        fields.push(@form.register(fieldClass, fieldClass._name))

        fieldClass = InvoiceFields["LineItemPercentFactory"](item, index)
        fields.push(@form.register(fieldClass, fieldClass._name))

        fieldClass = InvoiceFields["LineItemPercentTotalFactory"](item, index)
        fields.push(@form.register(fieldClass, fieldClass._name))

        div
          className: "percentItem"
          key: fields[0]._name
          @form.renderInput(fields[0]._name, {})
          @form.renderInput(fields[1]._name, {containerClass: 'percent-group fs-exclude'})
          @form.renderInput(fields[2]._name, {containerClass: 'price-group fs-exclude'})

      for index, item of @helper.getTaxLineItems()
        classNames = ["LineItemTaxInputFactory"]
        fieldClass = InvoiceFields["LineItemTaxInputFactory"](item, index)
        field = @form.register(fieldClass, fieldClass._name)

        div
          className: 'tax'
          key: item.id
          div null,
            "Tax"
          div null,
            @form.renderInput(field._name, {containerClass: "price-group fs-exclude"})
      div
        className: 'total'
        div null,
          "Invoice Total:"
        div null,
          @form.renderInput("total_field", {containerClass: "price-group fs-exclude"})
          if FeatureStore.isFeatureEnabled(Consts.FEATURE_QUICKBOOK) && UserStore.isPartner()
            @renderQuickbookButton(@form.record)


  onQuickbookPopupSubmit: (item, isPendingImport, e) =>
    item.qb_pending_import = isPendingImport
    ModalStore.hidePopup()
    Utils.ignoreEvents(e)
    @rerender()

  renderQuickbookButton: (item) =>
    if item?.qb_pending_import and item?.qb_imported_at?
      div
        className: "quickbook_button gray"
        onClick: (e) =>
          ModalStore.showPopup(
            target: e.target
            offsetTop: -60
            offsetLeft: 45
            getView: =>
              PopupBubble
                className: "quickbook_popup pending"
                showHeader: false
                body: span null,
                  'Pending QuickBooks import'
                submitText: 'Cancel Import'
                onSubmit: partial(@onQuickbookPopupSubmit, item, false)
          )
    else if item?.qb_imported_at?
      div
        className: "quickbook_button green"
        onClick: (e) =>
          ModalStore.showPopup(
            target: e.target
            offsetTop: -60
            offsetLeft: 45
            getView: =>
              PopupBubble
                className: "quickbook_popup"
                showHeader: false
                body: span null,
                  'Imported to Quickbooks'
                submitText: 'Import Again'
                onSubmit: partial(@onQuickbookPopupSubmit, item, true)
          )
  showDeletePopup: (e, title, text, callback) =>
    ModalStore.showPopup(
      target: e.target
      offsetTop: -63
      offsetLeft: 26
      getView: =>
        PopupBubble
          title: title
          body: span null,
            text
          submitText: 'Remove'
          onSubmit: (e) =>
            callback()
            ModalStore.hidePopup()
    )

  showDeletePaymentPopup: (index, e) =>
    @showDeletePopup(e, "Remove Transaction", "Are you sure you'd like to remove this transaction?", =>
      @helper.removePayment(index)
      ModalStore.hidePopup()
    )


  showDeleteItemPopup: (index, e) =>
    @showDeletePopup(e, "Remove Line Item", "Are you sure you'd like to remove this line Item?", =>
      @helper.removeLineItem(index)
      ModalStore.hidePopup()
    )


  renderItems: =>
    invoiceEditable = Utils.isInvoiceEditable(@form.record)
    if @state.spinner
      Loading null
    else
      div
        className: "invoice-item-list"
        div
          className: "table_headers",
          div
            className: "LineItemNameFactory"
            "Item"
          div
            className: "LineItemQtyFactory"
            "Qty"
          div
            className: "LineItemPriceFactory"
            "Price"
          div
            className: "LineItemTotalFactory"
            "Total"

        for index, item of @helper.getRegularLineItems()
          classNames = ["LineItemNameFactory", "LineItemQtyFactory", "LineItemPriceFactory", "LineItemTotalFactory"]
          className = "line-item"
          classAddition = 0
          if item.description == "Storage" and !item.user_created #TODO: move this to use fields isStorage method
            className = "storageLineItem"
            classNames.splice(1, 0, "LineItemEstimatedFactory")
            classAddition++
          div
            className: className
            key: item.id
            div
              className: "delete-line-item"
              FontAwesomeButton
                className: if invoiceEditable then null else 'disabled'
                color: "secondary-o"
                icon: "fa-times-circle"
                onClick: if invoiceEditable then partial(@showDeleteItemPopup, index)
                size: "small"

            for className, classIndex in classNames
              #TODO: add class names to align by type rather than index
              fieldClass = InvoiceFields[className](item, index)
              field = @form.register(fieldClass, fieldClass._name)
              @form.renderInput(field._name, {
                containerClass: className + ' ' + if classIndex >= 2+classAddition then "price-group fs-exclude" else ""
              })
        if invoiceEditable
          if @isSwoopFleetInvoice()
            addons = SWOOP_ADDONS
          else
            addons = @helper.getAdditionalItems(@getId())

          div
            className: 'addon-selector-container'
            if addons?.length > 0
              select
                className: 'form-control addon_selector'
                name: 'addon'
                value: -1
                onChange: if @isSwoopFleetInvoice() then @addNewItemSwoopChange else @addNewItemChange
                style:
                    color: "#CCC"
                option
                  value: -1
                  className: 'option_header'
                  'Add Item'
                for item in addons
                    option
                      key: item.id
                      value: item.id
                      style:
                        color: "#000"
                      className: 'option_row'
                      item.name
        div
          className: 'invoice_bottom'
          if filter(reject(@helper.getRegularLineItems(), 'deleted_at'), 'user_created').length > 0 && Utils.isPartnerOnSwoopJob(UserStore, @form.record.job)
            div
              className: 'additional-items-warning'
              'Additional items subject to 15 day review period, and require supporting notes and documentation.'

          if !@isSwoopFleetInvoice()
            div
              className: "invoice-notes-wrapper"
              @form.renderInput("job.driver_notes", {containerClass: 'invoice_notes'})
              if @form.record.job?.account?.name == "Swoop"
                div null,
                  label
                    className: "documents-label"
                    "Documents"
                  DocumentList(job_id: @form.record.job_id)

          @renderTotals()

  renderPayments: =>
    invoiceEditable = Utils.isInvoiceEditable(@form.record)
    div
      className: "payments-section"

      div
        className: "paymentList"
        div
          className: 'table_headers',
          div null,
            "Transaction Type"
          div null,
            "Payment Method"
          div null,
            "Memo"
          div
            className: "payment-date"
            "Date"
          div
            className: "payment-amount"
            "Amount"

        if @form.record?.payments?
          for item, index in @form.record.payments
            if !item.deleted_at?
              editable = @helper.isPaymentEditable(index)
              classNames = ["PaymentTypeFactory", "PaymentNameFactory", "PaymentMemoFactory", "PaymentDateFactory", "PaymentAmountFactory"]
              div
                className: if !editable then "external_payment" else "payments-line-items"
                key: item.id
                div
                  className: "payments-line-item"
                  div
                    className: "delete-payment"
                    if editable
                      FontAwesomeButton
                        color: "secondary-o"
                        icon: "fa-times-circle"
                        onClick: partial(@showDeletePaymentPopup, index)
                        size: "small"

                  for className, classIndex in classNames
                    fieldClass = InvoiceFields[className](item, index)
                    field = @form.register(fieldClass, fieldClass._name)
                    show_qb_button = className == "PaymentAmountFactory" && FeatureStore.isFeatureEnabled(Consts.FEATURE_QUICKBOOK) && UserStore.isPartner()
                    div
                      className: if show_qb_button then "qb_container" else className
                      key: className
                      @form.renderInput(field._name, {
                        containerClass: (if classIndex == 4 then "price-group fs-exclude" else ""),
                        helper: @helper,
                      })
                      if show_qb_button
                        @renderQuickbookButton(item)

      div
        className: 'addon-selector-container'
        select
          className: 'form-control addon_selector'
          name: 'addon'
          value: 0
          onChange: @addNewTransaction
          placeholder: "Add Transaction"
          style:
            color: "#CCC"
          option
            value: 0
            className: 'option_header'
            'Add Transaction'
          for item in values(Consts.TRANSACTIONS)
              option
                key: item
                value: item
                className: 'option_row'
                style:
                  color: "#000"
                item

      div
        className: 'invoice_bottom'
        @renderBalance()
        div
          className: 'clear'

  renderBody: =>
    if @props.show
      div
        className: 'modal_invoice'
        FormSection
          title: "Invoice"
          @renderViewTypes()
          @renderItems()
        FormSection
          title: "Payments"
          @renderPayments()

  save: =>
    if @form.isValid()
      newrecord = @helper.prepareForSend()
      @props.onConfirm(newrecord, false)
    else
      @form.attemptedSubmit()
      @rerender()

  saveAndClose: =>
    if @form.isValid(true)
      newrecord = @helper.prepareForSend()
      @props.onConfirm(newrecord)
    else
      @form.attemptedSubmit()
      @rerender()

  renderFooterView: =>
    if @props.show and @form and @form.hasAttemptedSubmit() and !@form.isValid()
      div
        style:
          top: 31
          right: 10
          position: "relative"
        div
          className: 'bubble'
          span
            style:
              color: "red"
            "Check For Errors"
  render: ->
    return TransitionModal extend({}, @props,
      key: "invoice_modal_container_modal"
      callbackKey: 'invoice_form_edit'
      transitionName: 'invoiceModalTransition'
      title: if @props.releasing then "Release Vehicle" else "Edit Invoice"
      extraClasses: "editInvoice"
      enableSend: true
      cancel: null
      rightComponent: span
        style:
          color: "#CCC"
          right: 40
          position: "absolute"
          top: 8
          fontSize: 16
        "#" + Utils.getJobId(JobStore.getJobById(@form?.record?.job_id))
      style:
        padding: 0
      onConfirm: @saveAndClose
      confirm: if @props.releasing then "Release" else "Save"
      footerView: @renderFooterView()
    ), @renderBody()

module.exports = InvoiceModal
