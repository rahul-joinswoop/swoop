import React from 'react'
AccountStore = require('stores/account_store')
UserStore    = require('stores/user_store')
ModalStore   = require('stores/modal_store')
ServiceStore = require('stores/service_store')
RateStore    = require('stores/rate_store')
RateItem      = require('components/rates/rate_item')
import Consts from 'consts'
BaseComponent = require('components/base_component')
import { map, union, uniq, without } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'
import LoadingImport from 'componentLibrary/Loading'
import CompanyStore from 'stores/company_store'
Loading = React.createFactory(LoadingImport)

class RatesSection extends BaseComponent
  displayName: 'Services'

  rate_types: []

  constructor: (props) ->
    super(props)
    @register(RateStore, RateStore.CHANGE)

  UNSAFE_componentWillReceiveProps: (props) ->
    if props.account_id != @props.account_id or props.service_id != @props.service_id or props.partner?.id != @props.partner?.id
      @rate_types = []
      if @refs.ratesForm?
        @refs.ratesForm?.refreshOnProps()

  getRateTypes: () =>
    if ServiceStore.getById(@props.service_id)?.name == "Storage"
      return [Consts.STORAGE]

    api_rates = RateStore.getApiRates(@props.account_id, @props.service_id, @rerender)

    if not api_rates?
      rates = RateStore.getRatesByObject(
        account_id: @props.account_id
        service_code_id: @props.service_id
      )
    else
      rates = api_rates

    ret = uniq(map(api_rates, (rate) -> rate.type))
    ret = union(ret, @rate_types)

    return ret

  deleteRateType: (rate_type, e) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "simple",
      props:
        title: "Warning"
        confirm: 'Delete'
        body: "Are you sure you want to remove this rate type from this service?"
        onConfirm: =>
          RateStore.deleteRates(
            {
              rates: {
                account_id: @props.account_id,
                service_code_id: @props.service_id,
                type: rate_type
              },
              rescue_provider_id: @props.partner?.id
            }
          )

          @rate_types = without(@rate_types, rate_type)
          Dispatcher.send(ModalStore.CLOSE_MODALS)
    )

    e.preventDefault()
    e.stopPropagation()

  addRateType: (e) =>
    if UserStore.isPartner()
      ids = @getRateTypes()
    else
      rates = RateStore.getRatesByPartner(@props.partner?.id, @props.service_id, @rerender)
      ids = uniq(map(rates, (rate) => if rate? then rate.type else null))
      ids = union(ids, @rate_types)

    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "rate_type_picker_modal",
      props:
        exclude: ids
        showTesla: (if @props.account_id then AccountStore.isTeslaAccountOrTeslaMotors(@props.account_id, @getId())) || UserStore.isTesla()
        onConfirm: (obj) =>
          @rate_types.push(obj.id)
          @rerender()
          Dispatcher.send(ModalStore.CLOSE_MODALS)
    )
    e.preventDefault()
    e.stopPropagation()

  canAddRates: (service) =>
    if UserStore.isPartner() and @props.account_id and AccountStore.isTeslaAccount(@props.account_id, @getId())
      return false

    if (UserStore.isPartner() || UserStore.isFleetInHouse()) and (not service? or not service.addon) and ServiceStore.getById(@props.service_id)?.name != "Storage"
      if UserStore.isPartner() and @props.account_id and AccountStore.isTeslaAccount(@props.account_id, @getId()) && Consts.TESLA_RATE in @getRateTypes()
        return false
      return true
    return false

  render: ->
    super(arguments...)
    if !UserStore.isPartner() and (not @props.partner?.id)
      return span
        style:
          margin: "10px 15px"
          display: "inline-block"
        "Select a Partner"

    if @props.service_id?
      service = ServiceStore.getById(@props.service_id)

    if service? and service.addon
      data = [
        {
          fields: [
            {
              name: "flat"
              label: service.name
              classes: "money topmargin"
            }
          ]
        }
      ]

    service = null
    if UserStore.isPartner()
      partnerCompany = UserStore.getCompany()
      api_rates = RateStore.getApiRates(@props.account_id, @props.service_id, @rerender)

      if not api_rates?
        spinner = true
      ids = @getRateTypes()

    else
      partnerCompany = CompanyStore.getById(@props.partner?.company?.id, true, @getId())
      rates = RateStore.getRatesByPartner(@props.partner.id, @props.service_id, @rerender)
      if rates == null
        spinner = true

      ids = uniq(map(rates, (rate) => if rate? then rate.type else null))
      ids = union(ids, @rate_types)

    service = ServiceStore.getById(@props.service_id)
    if service? and service.addon
      ids = [Consts.FLAT]

    div
      className: "rate_col rate_items"
      div
          className: "action_header"
          h3
            className: "section_header"
            "Rate Type"
          if @canAddRates(service)
            ReactDOMFactories.a
              onClick: @addRateType
              className: "action"
              "+"
          else if UserStore.isPartner() and @props.account_id and AccountStore.isTeslaAccount(@props.account_id, @getId())
            span
              style:
                float: "right"
                color: "#AAA"
                marginTop: -4
              "Contact Tesla to Edit"

      div
        className: "rates_type_container"
        ul null,
          for id, index in ids
            RateItem
              key: id
              rate_type: id
              partnerCompany: partnerCompany
              deleteRateType: @deleteRateType
              service_id: @props.service_id
              account_id: @props.account_id
              partner_id: @props.partner?.id
              renderBreak: Math.random() #HACK: to ensure view updates on load
              live: ids.length == 1 and index == 0
        if spinner
          Loading null

module.exports = React.createFactory(RatesSection)
