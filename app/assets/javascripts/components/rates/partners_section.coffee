import React from 'react'
ProviderStore = require('stores/provider_store')
BaseComponent = require('components/base_component')
import { partial } from 'lodash'

class PartnerSection extends BaseComponent
  displayName: 'PartnerSection'

  render: ->
    super(arguments...)

    providers = ProviderStore.getListGroupedByPartner(@getId(), 'company.name')

    div
      className: "comapny_selection rate_col"
      div null,
        ul
          className: "key_list"
          if providers?
            for provider in providers
              if provider?
                li
                  key: provider.id
                  className: if @props.partner_id == provider.id then "selected" else "",
                  onClick: partial(@props.setPartner, provider)
                  provider.company.name

module.exports = React.createFactory(PartnerSection)
