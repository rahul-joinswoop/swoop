import React from 'react'
AccountStore = require('stores/account_store')
UserStore    = require('stores/user_store')
ModalStore   = require('stores/modal_store')
RateStore    = require('stores/rate_store')
BaseComponent = require('components/base_component')
import { partial, sortBy } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class AccountsSection extends BaseComponent
  displayName: 'AccountsSection'

  constructor: (props) ->
    super(props)
    @register(RateStore, RateStore.CHANGE)

  deleteAccount: (account_id, e) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "simple",
        props: {
          title: "Warning"
          confirm: 'Delete'
          body: "Are you sure you want to remove all custom rates for this account?"
          onConfirm: =>
            RateStore.deleteAccountRates(account_id, @rerender)
            Dispatcher.send(ModalStore.CLOSE_MODALS)
            if @props.account_id == account_id
              @props.setAccount(null)
          }
      )

    e.preventDefault()
    e.stopPropagation()

  addAccount: (e) =>
    #TODO: need to also grab these myself from rates in case they've changed
    ids = RateStore.getAccountIdsFromRates()
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "account_picker_modal",
      props:
        exclude: ids

        onConfirm: (obj) =>
          if obj.copy
            RateStore.createAccountRates(obj.id, =>
              @rerender()
              @props.setAccount(parseInt(obj.id))
            )
          else
            RateStore.addTempAccount(parseInt(obj.id))

          Dispatcher.send(ModalStore.CLOSE_MODALS)
          @rerender()
    )
    e.preventDefault()
    e.stopPropagation()

  render: ->
    super(arguments...)

    accountIds = RateStore.getAccountIdsFromRates(@rerender)
    accountIds = sortBy(accountIds, (accountId) =>
      if accountId
        AccountStore.getName(accountId, @getId())
      else
        undefined
    )

    div
      className: "comapny_selection rate_col"
      div null,
        div
          className: "action_header"
          h3
            className: "section_header"
            "Accounts"
          if UserStore.isPartner()
            ReactDOMFactories.a
              onClick: @addAccount
              className: "action"
              "+"
        ul
          className: "key_list"
          li
            className: if not @props.account_id? then "selected" else "",
            onClick: partial(@props.setAccount, null, false)
            "Default"
          for accountId in accountIds
            if accountId
              isTeslaAccount = AccountStore.get(accountId, 'company', @getId())?.name == 'Tesla'

              accountName = AccountStore.getName(accountId, @getId())

              li
                key: accountId
                className: if @props.account_id == accountId then "selected" else "",
                onClick: partial(@props.setAccount, accountId, isTeslaAccount)
                if accountName then accountName else 'loading..'
                if !isTeslaAccount and UserStore.isPartner()
                  ReactDOMFactories.i
                    onClick: partial(@deleteAccount, accountId)
                    className: "fa fa-times-circle"
        if not accountIds
          Loading null

module.exports = React.createFactory(AccountsSection)
