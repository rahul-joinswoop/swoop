import React from 'react'
AccountStore = require('stores/account_store')
UserStore    = require('stores/user_store')
ModalStore   = require('stores/modal_store')
ServiceStore = require('stores/service_store')
RateStore    = require('stores/rate_store')
VehicleCategoryStore = require('stores/vehicle_category_store')
EditForm = React.createFactory(require('components/edit_form'))
import Consts from 'consts'
import Logger from 'lib/swoop/logger'
BaseComponent = require('components/base_component')
import { extend, get, includes, map, partial, set, sortBy, union, uniq, without } from 'lodash'
import Utils from 'utils'
import moment from 'moment-timezone'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'
import {
  distanceToFE,
  distanceToBE,
  getCurrencyClass,
  getCurrencySymbol,
  getDistanceUnitName,
  moneyToFE,
  moneyToBE,
} from 'lib/localize'
import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)

class RateItem extends BaseComponent
  displayName: 'RateItem'

  constructor: (props) ->
    super(props)

    if UserStore.isPartner()
      rates = RateStore.getRatesByObject({
        account_id: @props.account_id
        service_code_id: @props.service_id
        type: @props.rate_type
      })

    @state =
      editing: (not rates? or rates.length == 0) and not @props.rate_type == Consts.STORAGE

  vehicle_types: []

  componentDidMount: ->
    super(arguments...)

    RateStore.bind(RateStore.CHANGE, @ratesChanged)
    RateStore.bind(RateStore.CHANGED_RATES, @ratesChanged)
    @UNSAFE_componentWillReceiveProps(@props)

  componentWillUnmount: ->
    super(arguments...)

    RateStore.unbind(RateStore.CHANGE, @ratesChanged)
    RateStore.unbind(RateStore.CHANGED_RATES, @ratesChanged)

  ratesChanged: =>
    if !@state.editing && @refs?.ratesForm?
      @refs.ratesForm?.refreshOnProps()

    @vehicle_types = []

    @rerender()

  starRateType: (e) =>
    if !@isLive()
      Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "simple",
        props: {
          title: "Set Default Rate Type"
          confirm: 'Confirm'
          cancel: 'Cancel'
          showCancel: true
          body: "Please confirm you’d like to set "+Consts.RATE_TYPE_MAP[@props.rate_type]+" as your default Rate type."
          onConfirm: =>
            RateStore.markLive(
              account_id: @props.account_id,
              service_code_id: @props.service_id,
              type: @props.rate_type,
              provider_id: @props.partner_id
            )

            Dispatcher.send(ModalStore.CLOSE_MODALS)
          }
      )

    e.preventDefault()
    e.stopPropagation()


  isLive: =>
    rates = @getRates()
    @props.live or (rates? and rates[0]? and rates[0].live)

  setEditing: (type) =>
    if not type?
      type = !@state.editing
    @setState
      editing: type

  sendRecord: (rate) =>
    if not rate.id?
      #THIS IS AN UNNECESSARY SAFE GUARD FOR BAD DATA IN STAGING
      if not rate.type?
        temp_rate = RateStore.getRateByValue(@props.service_id, @props.account_id)
        if temp_rate? and temp_rate.type?
          rate.type = temp_rate.type
        else
          service = ServiceStore.getById(@props.service_id)
          if service? and service.addon
            rate.type = "FlatRate"
          else
            rate.type = "HoursP2PRate"
      RateStore.addItem(rate)
    else
      RateStore.updateItem(rate)

  get_base_rate: =>
    base_rate = RateStore.getRatesByObject({
      service_code_id: @props.service_id,
      account_id: @props.account_id
      type: @props.rate_type
      vehicle_category_id: null
      })

    if base_rate.length > 0
      return base_rate = base_rate[0]

    return {}

  get_base_storage_rate: =>
    base_rate = RateStore.getRatesByObject({
      service_code_id: @props.service_id,
      account_id: @props.account_id
      type: Consts.STORAGE
      vehicle_category_id: null
      })

    if base_rate.length > 0
      return base_rate = base_rate[0]

    return {}

  change_prop: (prop, e) =>
    obj = {}
    obj[prop] = e.target.value
    @setState obj

  saveRecord: (rates) =>
    toSaveNew = []
    newTypeIds = []
    toSaveOld = []

    props = ["hourly", "hookup", "miles_towed", "miles_towed_free", "miles_p2p_free", "miles_enroute", "miles_p2p", "miles_enroute_free", "flat", "storage_daily", "miles_deadhead", "miles_deadhead_free"]

    company = @props.partnerCompany

    if @state.invoice_mileage_rounding || @state.invoice_mileage_minimum
      obj = {}
      if @state.invoice_mileage_rounding
        obj.invoice_mileage_rounding = @state.invoice_mileage_rounding
      if @state.invoice_mileage_minimum
        obj.invoice_mileage_minimum = @state.invoice_mileage_minimum
      @setState
        invoice_mileage_rounding: null
        invoice_mileage_minimum: null
      RateStore.setMileageRules(obj)

    if (@state.invoice_minimum_mins? or @state.invoice_increment_mins? or @state.invoice_roundup_mins?)
      obj = {}
      if @state.invoice_minimum_mins?
        obj["invoice_minimum_mins"] = @state.invoice_minimum_mins

      if @state.invoice_increment_mins?
        obj["invoice_increment_mins"] = @state.invoice_increment_mins
        if company?.invoice_roundup_mins in [0, 7, 15]
          obj["invoice_roundup_mins"] = Math.floor(parseInt(@state.invoice_increment_mins))/2
        if @state.invoice_increment_mins == "1"
          obj["invoice_roundup_mins"] = 5


      if @state.invoice_roundup_mins?
        obj["invoice_roundup_mins"] = @state.invoice_roundup_mins
        if  @state.invoice_roundup_mins == "mid"
          if @state.invoice_increment_mins?
            obj["invoice_roundup_mins"] = Math.floor(parseInt(@state.invoice_increment_mins))/2
          else
            obj["invoice_roundup_mins"] = Math.floor(parseInt(company?.invoice_increment_mins))/2

      @setState
        invoice_minimum_mins: null
        invoice_increment_mins: null
        invoice_roundup_mins: null

      RateStore.addHourlyRates(obj)

    if @state.second_day_charge? or @state.beyond_charge? or @state.free_window?
      set_base = true
      for vehicle_type of rates
        if vehicle_type == "0"
          set_base = false

    if set_base
      rates["0"] = @get_base_storage_rate()

    for vehicle_type, rate of rates

      if rate?
        if vehicle_type != "0"
          rate.vehicle_category_id = vehicle_type
        else
          set_base = true
          if @state.second_day_charge?
            rate.second_day_charge = @state.second_day_charge

          if @state.beyond_charge?
            rate.beyond_charge = @state.beyond_charge

          if @state.free_window?
            rate.seconds_free_window = @state.free_window*60*60

        rate.service_code_id = @props.service_id
        rate.account_id = @props.account_id
        rate.type = @props.rate_type
        if @isLive()
          rate.live = true

        if rate.additions?
          for addition, i in rate.additions
            if !addition.name? && !addition.id?
              #TODO: this should throw an error instead of setting it to string
              if !rate.additions[i].deleted_at
                delete rate.additions[i]
              else
                addition.name = 'DELETED UNSELECTED ITEM'
        if rate.id?
          for prop in props
            if rate[prop] == null or rate[prop] == ""
                rate[prop] = 0
          toSaveOld.push(rate)
        else
          for prop in props
            if not rate[prop]? or rate[prop] == ""
              rate[prop] = 0
          toSaveNew.push(rate)
        newTypeIds.push(parseInt(vehicle_type))

    if @state.second_day_charge? or @state.beyond_charge? or @state.free_window?
      @setState
        second_day_charge: null
        beyond_charge: null
        free_window: null

    for type in @vehicle_types
      if type not in newTypeIds
        rate = {
          vehicle_category_id: type
          service_code_id: @props.service_id
          account_id: @props.account_id
          type: @props.rate_type
        }
        for prop in props
          rate[prop] = 0
        if @isLive()
          rate.live = true
        toSaveNew.push(rate)

    @vehicle_types = []

    if toSaveOld.length > 0
      RateStore.addRates({
        rates: toSaveOld
        rescue_provider_id: @props.partner_id
      })
    if toSaveNew.length > 0
      RateStore.addRates({
        rates: toSaveNew
        rescue_provider_id: @props.partner_id
      })

    @refs.ratesForm?.refreshOnProps()
    @setEditing(false)

  UNSAFE_componentWillReceiveProps: (props) ->
    if props.account_id != @props.account_id or props.service_id != @props.service_id or props.partner_id != @props.partner_id or props.rate_type != @props.rate_type or UserStore.isFleet()
      @vehicle_types = []
      if @refs.ratesForm?
        @refs.ratesForm?.refreshOnProps()
      @setEditing(false)

  deleteTruckType: (name, vehicle_type, e) =>
    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "simple",
      props: {
        title: "Warning"
        confirm: 'Delete'
        body: "Are you sure you want to remove " + name + " from rates?"
        onConfirm: =>
          RateStore.deleteRates(
            {
              rates: {
                account_id: @props.account_id,
                service_code_id: @props.service_id,
                type: @props.rate_type,
                vehicle_category_id: vehicle_type
              },
              rescue_provider_id: @props.partner_id
            })

          @vehicle_types = without(@vehicle_types, vehicle_type)
          Dispatcher.send(ModalStore.CLOSE_MODALS)
        }
    )

    e.preventDefault()
    e.stopPropagation()

  addTruckType: (e) =>
    that = this
    ids = @getVehicleTypes()

    showTruckTypesModal = (companyTruckTypes) ->
      Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "vehicle_type_picker_modal",
        props:
          exclude: ids
          companyTruckTypes: companyTruckTypes
          onConfirm: (ids) =>
            Logger.debug("CHECKC", ids)
            for id in ids
              that.vehicle_types.push(parseInt(id))
              Dispatcher.send(ModalStore.CLOSE_MODALS)
            that.setEditing(true)
      )

    if UserStore.isFleetInHouse()
      VehicleCategoryStore.getPartnerVehicleCategories(@props.partner_id, showTruckTypesModal)
    else
      showTruckTypesModal()

    e.preventDefault()
    e.stopPropagation()

  getMoneyFieldView: (rates, errors, col) =>
    Utils.formatMoney(get(rates, col.name, 0), rates[col.vehicleType]?.currency || @props.partnerCompany?.currency)

  getInputWrapperClass: (rates, col) =>
    getCurrencyClass(rates[col.vehicleType]?.currency || @props.partnerCompany?.currency)

  beforeSend: (func, distanceUnit) =>
    (rates, col) =>
      value = func(get(rates, col.name, 0), distanceUnit)
      set(rates, col.name, value)

  getFields: (vehicle_type) =>
    distanceUnit = @props.partnerCompany?.distance_unit
    fields = [
      {
        name: vehicle_type + ".hourly"
        label: "Hourly Rate"
        classes: "money"
        show: @props.rate_type in [Consts.HOURLY_P2P, Consts.TESLA_RATE]
      }
      {
        name: vehicle_type + ".hookup"
        label: if @props.rate_type == Consts.MILEAGE_EN_ROUTE_ONLY then "Service Fee" else "Hookup"
        classes: "money"
        show: @props.rate_type in [Consts.MILEAGE_TOWED, Consts.MILEAGE_EN_ROUTE, Consts.MILEAGE_EN_ROUTE_ONLY, Consts.MILEAGE_P2P]
      }
      {
        name: vehicle_type + ".miles_enroute"
        label: span null,
          "En Route"
          br()
          "Per"
          br()
          getDistanceUnitName(distanceUnit, { plural: false })
        prepareValue: moneyToFE
        aboutToSend: @beforeSend(moneyToBE, distanceUnit)
        classes: "money"
        show: @props.rate_type in [Consts.MILEAGE_P2P, Consts.MILEAGE_EN_ROUTE, Consts.MILEAGE_EN_ROUTE_ONLY]
      }
      {
        name: vehicle_type + ".miles_enroute_free"
        label: span null,
          "En Route"
          br()
          "Free"
          br()
          getDistanceUnitName(distanceUnit)
        prepareValue: distanceToFE
        aboutToSend: @beforeSend(distanceToBE, distanceUnit)
        classes: "rightSide"
        show: @props.rate_type in [Consts.MILEAGE_P2P, Consts.MILEAGE_EN_ROUTE, Consts.MILEAGE_EN_ROUTE_ONLY]
      }
      {
        name: vehicle_type + ".miles_towed"
        label:  span null,
          "Towed"
          br()
          "Per"
          br()
          getDistanceUnitName(distanceUnit, { plural: false })
        prepareValue: moneyToFE
        aboutToSend: @beforeSend(moneyToBE, distanceUnit)
        classes: "money"
        show: @props.rate_type in [Consts.MILEAGE_P2P, Consts.MILEAGE_TOWED, Consts.MILEAGE_EN_ROUTE]
      }
      {
        name: vehicle_type + ".miles_towed_free"
        label: span null,
          "Towed"
          br()
          "Free"
          br()
          getDistanceUnitName(distanceUnit)
        prepareValue: distanceToFE
        aboutToSend: @beforeSend(distanceToBE, distanceUnit)
        classes: "rightSide"
        show: @props.rate_type in [Consts.MILEAGE_P2P, Consts.MILEAGE_TOWED, Consts.MILEAGE_EN_ROUTE]
      }
      {
        name: vehicle_type + ".miles_deadhead"
        label: span null,
          "Deadhead"
          br()
          "Per"
          br()
          getDistanceUnitName(distanceUnit, { plural: false })
        prepareValue: moneyToFE
        aboutToSend: @beforeSend(moneyToBE, distanceUnit)
        classes: "money"
        show: @props.rate_type in [Consts.MILEAGE_P2P]
      }
      {
        name: vehicle_type + ".miles_deadhead_free"
        label: span null,
          "Deadhead"
          br()
          "Free"
          br()
          getDistanceUnitName(distanceUnit)
        prepareValue: distanceToFE
        aboutToSend: @beforeSend(distanceToBE, distanceUnit)
        classes: "rightSide"
        show: @props.rate_type in [Consts.MILEAGE_P2P]
      }
      {
        name: vehicle_type + ".miles_p2p"
        label: span null,
          "P2P"
          br()
          "Per"
          br()
          getDistanceUnitName(distanceUnit, { plural: false })
        prepareValue: moneyToFE
        aboutToSend: @beforeSend(moneyToBE, distanceUnit)
        classes: "rightSide"
        show: @props.rate_type in [Consts.TESLA_RATE]
      }
      {
        name: vehicle_type + ".flat"
        label: "Flat Rate"
        classes: "money"
        show: @props.rate_type in [Consts.FLAT]
      }
      {
        name: vehicle_type + ".storage_daily"
        label: "Storage #{getCurrencySymbol(@props.partnerCompany?.currency)}/Day"
        classes: "money rightSide"
        required: ((rate) =>
          if not @props.service_id?
            return false
          service = ServiceStore.getById(@props.service_id)
          return (service? and service.name == "Impound")
        )()
        show: @props.rate_type in [Consts.STORAGE]
      }
    ]

    editing = @state.editing
    return map(fields, (col) =>
      # allow 6 chars before dot and 2 after
      col.pattern = "^\\d{0,6}(\\.|(\\.\\d{1,2})?)$"
      col.realTimePattern = true

      col.vehicleType = vehicle_type
      if editing and (UserStore.isPartner() || UserStore.isFleetInHouse())
        col.type = 'text'
        if includes(col.classes, 'money')
          col.inputWrapperClass = @getInputWrapperClass
      else if includes(col.classes, 'money')
        col.type = 'custom'
        col.getView = @getMoneyFieldView
      else
        col.type = 'span'

      col.onBlur = (e) ->
        if e.target.value
          # it is possible user leave value like '44.' without decimal part, so we need to fix this value
          e.target.value = parseFloat(e.target.value).toFixed(2)
          # update prev valid value
          e.target.dataset.prevVal = e.target.value

        @getOnChange(col.type)(e)

      col
    )

  getVehicleTypes: () =>
    if UserStore.isPartner()
      rates = RateStore.getRatesByObject(
        account_id: @props.account_id
        service_code_id: @props.service_id
        type: @props.rate_type
      )
    else
      rates = RateStore.getRatesByObject(
        provider_id: @props.partner_id
        service_code_id: @props.service_id
        type: @props.rate_type
      )

    ret = uniq(map(rates, (rate) -> rate.vehicle_category_id))
    ret =  uniq(union(ret, @vehicle_types))

    return ret

  getTemplate: =>
    vehicle_types = @getVehicleTypes(@props.rate_type)
    vehicle_types.unshift(0)
    vehicle_types = uniq(vehicle_types)
    xs = (UserStore.isPartner() || UserStore.isFleetInHouse())
    ret = []
    vehicle_types = sortBy(vehicle_types, (id) =>
      if id?
        orderNum = VehicleCategoryStore.get(id, 'order_num', @getId())
        if orderNum?
          return orderNum
        return 0
    )

    for vehicle_type in vehicle_types
      if vehicle_type?
        vehicleName = VehicleCategoryStore.get(vehicle_type, 'name', @getId())
        if vehicle_type == 0
          row = {
            label:
              div
                className: (if xs then "default")
                key: vehicleName
                'Default Price'
            fields: @getFields(vehicle_type)
          }

        else
          row = {
            label:
              div
                className: "vehcile_name"
                key: vehicleName
                if xs
                  ReactDOMFactories.i
                    onClick: partial(@deleteTruckType, VehicleCategoryStore.get(vehicle_type, 'name', @getId()), vehicle_type)
                    className: "fa fa-times-circle"
                vehicleName
            fields: @getFields(vehicle_type)
          }

        ret.push(row)

    return ret

  getRates: =>
    if UserStore.isPartner()
      RateStore.getRatesByObject({
        account_id: @props.account_id
        service_code_id: @props.service_id
        type: @props.rate_type
      })
    else
      RateStore.getRatesByObject({
        provider_id: @props.partner_id
        service_code_id: @props.service_id
        type: @props.rate_type
      })

  getRecord: (cols) =>
    ret = {}
    rates = @getRates()
    distanceUnit = @props.partnerCompany?.distance_unit

    if rates?
      for rate in rates
        vtype = rate.vehicle_category_id
        if not vtype?
          vtype = 0
        ret[vtype] = rate

    # convert miles or `per mile` values to kilometers to `per kilometer`
    for col in cols
      for group in col.fields
        if group.prepareValue
          value = group.prepareValue(get(rates, group.name, 0), distanceUnit)
          set(rates, group.name, value)

    return ret

  renderRateTable: =>
    cols = @getTemplate()
    EditForm extend({}, @props,
      key: "editForm"
      extraClasses: "editRates rate_table"
      ref: "ratesForm"
      cols: cols
      record: @getRecord(cols)
      show: true
      #forceFullObject: forceFullObject
      showCancel: if UserStore.isPartner() || UserStore.isFleetInHouse() then @state.editing else false
      showSubmit: if UserStore.isPartner() || UserStore.isFleetInHouse() then @state.editing else false
      submitButtonName: "Save"
      onCancel: ((that) -> (->
          that.setState
            free_window: null
            second_day_charge: null
            beyond_charge: null
            invoice_minimum_mins: null
            invoice_increment_mins: null
            invoice_roundup_mins: null
            invoice_mileage_rounding: null
            invoice_mileage_minimum: null
          that.setEditing(false)
        ))(@)

      renderContainer: ((that) -> (->
        fleet_company_id = AccountStore.get(@props.account_id, "company_id", that.getId())
        base_rate = that.get_base_rate()
        accountName = if @props.account_id then AccountStore.get(@props.account_id, 'name', that.getId()) else 'loading..'
        canAddClassType = UserStore.isPartner() || UserStore.isFleetInHouse()
        canAddItem = UserStore.isPartner() || UserStore.isFleetInHouse()
        if UserStore.isPartner() and @props.account_id and AccountStore.isTeslaAccount(@props.account_id, that.getId())
          canAddClassType = false
          #if @props.rate_type not in [Consts.TESLA_RATE]
          canAddItem = false

        div
          className: "rate_table"
          for col, col_index in @getCols(@props)
            headers = null
            if col_index == 0
              headers = div
                className: "rate_row"
                key: col_index
                div null, ""
                for group, group_index in col.fields
                  if group.show
                    div
                      key: 'group' + group_index
                      group.label

            [
              headers
              div
                className: "rate_row"
                key: 'rate_row-' + col.label
                div
                  key: col.label
                  col.label
                for group, group_index in col.fields
                  if group.show
                    div
                      className: "value_container " + group.classes
                      key: group.name
                      @renderInput(group)
            ]
          if canAddClassType
            Button
              className: (if that.state.editing or that.getVehicleTypes().length-1 >= VehicleCategoryStore.getAllSorted(that.getId()).length then "invisible" else "")
              color: "secondary-o"
              lineHeight: 24
              onClick: that.addTruckType
              size: "narrow small"
              style: {
                marginLeft: 20
              }
              "Add Class Type"

          if @props.rate_type in [Consts.MILEAGE_TOWED, Consts.MILEAGE_EN_ROUTE_ONLY, Consts.MILEAGE_EN_ROUTE, Consts.MILEAGE_P2P]
            company = that.props.partnerCompany
            div null,
              div
                className: "hourly_rate_options",
                h4 null,
                  "Set Mileage Rules"
                div
                  className: "single_line money",
                  label
                    className: "set_width"
                    "Set minimum amount:"
                  if that.state.editing and accountName != Consts.TESLA and UserStore.isPartner()
                    div
                      className: 'inputWrapper' + getCurrencyClass(company?.currency)
                      input
                        className: "form-control"
                        value: if that.state.invoice_mileage_minimum? then that.state.invoice_mileage_minimum else (if company?.invoice_mileage_minimum? then company?.invoice_mileage_minimum else "")
                        onChange: (e) =>
                          that.setState
                            invoice_mileage_minimum: e.target.value.replace(/[^0-9\-\.]/g,'')
                  else
                    span null,
                      if !isNaN(parseFloat(company?.invoice_mileage_minimum)) then Utils.formatMoney(company?.invoice_mileage_minimum, company?.currency) else Utils.formatMoney(0, company?.currency)
              div
                className: "hourly_rate_options"
                style:
                  borderTop: 0
                div
                  className: "single_line",
                  label
                    className: "set_width"
                    "Round miles:"
                  if that.state.editing and accountName != Consts.TESLA  and UserStore.isPartner()
                    select
                      className: "form-control"
                      onChange: partial(that.change_prop, "invoice_mileage_rounding")
                      value: ""+(if that.state.invoice_mileage_rounding? then that.state.invoice_mileage_rounding else (if company?.invoice_mileage_rounding? then company?.invoice_mileage_rounding else Consts.DISTANCE_ROUND_NEAREST_TENTH))
                      option
                        value: Consts.DISTANCE_ROUND_NEAREST_TENTH
                        "to nearest tenth"
                      option
                        value: Consts.DISTANCE_ROUND_NEAREST
                        "to nearest whole number"
                      option
                        value: Consts.DISTANCE_ROUND_UP
                        "always up to next whole number"
                  else
                    span null,
                      if accountName == Consts.TESLA
                        "to nearest tenth"
                      else if company?.invoice_mileage_rounding == Consts.DISTANCE_ROUND_NEAREST
                        "to nearest whole number"
                      else if company?.invoice_mileage_rounding == Consts.DISTANCE_ROUND_UP
                        "always up to next whole number"
                      else
                        "to nearest tenth"
          if @props.rate_type == Consts.HOURLY_P2P
            company = that.props.partnerCompany
            div
              className: "hourly_rate_options",
              div
                className: "single_line",
                label
                  className: "set_width"
                  "Set minimum quantity:"
                if that.state.editing and UserStore.isPartner()
                  select
                    className: "form-control"
                    onChange: partial(that.change_prop, "invoice_minimum_mins")
                    value: ""+(if that.state.invoice_minimum_mins? then that.state.invoice_minimum_mins else (if company?.invoice_minimum_mins? then company?.invoice_minimum_mins else "60"))
                    option
                      value: "0"
                      "No minimum"
                    option
                      value: "30"
                      "30 min"
                    option
                      value: "60"
                      "1 hour"
                else
                  span null,
                    if company?.invoice_minimum_mins? and company?.invoice_minimum_mins > 0
                      if company?.invoice_minimum_mins == 60
                        "1 hour"
                      else
                        company?.invoice_minimum_mins + " min"
                    else "1 hour"
               div
                className: "single_line",
                label
                  className: "set_width"
                  "Increment quantity by:"
                if that.state.editing and UserStore.isPartner()
                  select
                    className: "form-control"
                    onChange: partial(that.change_prop, "invoice_increment_mins")
                    value: if that.state.invoice_increment_mins? then that.state.invoice_increment_mins else (if company?.invoice_increment_mins? then company?.invoice_increment_mins else "15")
                    option
                      value: "1"
                      "1 min"
                    option
                      value: "15"
                      "15 min"
                    option
                      value: "30"
                      "30 min"
                else
                  span null,
                    if company?.invoice_increment_mins?
                      company?.invoice_increment_mins + " min"
                    else "15 min"
              div
                className: "single_line",
                label
                  className: "set_width"
                  "Round up to next increment after:"
                if that.state.editing and UserStore.isPartner()
                  select
                    className: "form-control"
                    onChange: partial(that.change_prop, "invoice_roundup_mins")
                    value: if that.state.invoice_roundup_mins? then that.state.invoice_roundup_mins else (if company?.invoice_roundup_mins in [5, 10] then company?.invoice_roundup_mins else (if company?.invoice_roundup_mins? then "mid" else "5"))
                    option
                      value: "5"
                      "5 min"
                    option
                      value: "10"
                      "10 min"
                    if that.state.invoice_increment_mins != "1" and (company?.invoice_increment_mins != 1 or (that.state.invoice_increment_mins in ["15", "30"]))
                      option
                        value: "mid"
                        "increment midpoint"
                else
                  span null,
                    if company?.invoice_roundup_mins?
                      if company?.invoice_roundup_mins in [5, 10]
                        company?.invoice_roundup_mins + " min"
                      else
                        "Increment midpoint"
                    else "5 min"

          if ServiceStore.getById(that.props.service_id)?.name == "Storage"
            base_rate = that.get_base_storage_rate()
            Logger.debug("Using base_rate to : ", base_rate)
            div
              className: "storage_rate_options",
              div
                className: "single_line",
                label
                  className: "set_width "+(if that.state.editing and UserStore.isPartner() then " hours" else "")
                  "Free Pickup Window:"
                if that.state.editing and UserStore.isPartner()
                  input
                    className: "form-control"
                    value: if that.state.free_window? then that.state.free_window else (if isNaN(base_rate.seconds_free_window) then 0 else base_rate.seconds_free_window/3600)
                    onChange: (e) =>
                      that.setState
                        free_window: e.target.value
                else
                  span null,
                    (if isNaN(base_rate.seconds_free_window) then 0 else base_rate.seconds_free_window/3600) + " hours"
              div
                className: "single_line",
                label
                  className: "set_width"
                  "Charge for 2nd Day:"
                if that.state.editing and UserStore.isPartner()
                  select
                    className: "form-control"
                    onChange: partial(that.change_prop, "second_day_charge")
                    value: if that.state.second_day_charge? then that.state.second_day_charge else base_rate?.second_day_charge
                    option
                      value: "After 24 Hours"
                      "After 24 Hours"
                    option
                      value: "After 12 Hours"
                      "After 12 Hours"
                    option
                      value: "Next Calendar Day"
                      "Next Calendar Day"
                else
                  span null,
                    if base_rate?.second_day_charge? then base_rate?.second_day_charge else "After 24 Hours"
              div
                className: "single_line",
                label
                  className: "set_width"
                  "Charge After That:"
                if that.state.editing and UserStore.isPartner()
                  select
                    className: "form-control"
                    onChange: partial(that.change_prop, "beyond_charge")
                    value: if that.state.beyond_charge? then that.state.beyond_charge else base_rate?.beyond_charge
                    option
                      value: "Per 24 Hours"
                      "Per 24 Hours"
                    option
                      value: "Per 12 Hours"
                      "Per 12 Hours"
                    option
                      value: "Per Calendar Day"
                      "Per Calendar Day"
                else
                  span null,
                    if base_rate?.beyond_charge? then base_rate?.beyond_charge else "Per 24 Hours"

          if @props.service_id not in map(ServiceStore.getAddons(that.getId(), fleet_company_id), (addon) -> addon.id) and !AccountStore.isTeslaAccount(@props.account_id, that.getId())
            div
              className: "additional_items_container"
              key: "additions: " + if @record?[0]?.additions? then @record?[0]?.additions.length
              if (@record?[0]?.additions? and @record?[0]?.additions.length > 0) || UserStore.isPartner()
                h4 null,
                  "Automatically add item to invoice"
              if @record?[0]?.additions?
                for addition, index in @record?[0]?.additions
                  if addition.deleted_at == null || !addition.deleted_at
                    ((addition_index) =>
                      rate_type = @props.rate_type

                      div
                        key: addition_index
                        className: "additional_items"

                        ReactDOMFactories.i
                          onClick: =>
                              #if addition.id
                              @record[0].additions[addition_index].deleted_at = moment().toISOString()
                              #else
                              #  delete @record[0].additions.splice(addition_index, 1)
                              @recordsChanged()
                          className: "fa fa-times-circle " + if !that.state.editing || @record?[0]?.additions?[addition_index]?.name == "Tesla Margin" then "invisible"

                        @renderFormGroup
                          name: "0.additions."+addition_index+".name"
                          type: if !that.state.editing || @record?[0]?.additions?[addition_index]?.name == "Tesla Margin" then "span" else "selector"
                          classes: if that.state.editing && @record?[0]?.additions?[addition_index]?.name != "Tesla Margin" then "editing" else "bold"
                          containerStyle:
                            width: 150
                            margin: "0 5px"
                          placeholder: "Add Custom Type"
                          onChange: (e) ->
                            if e.target.value == "- Select -"
                              @recordsChanged()
                              return
                            @handleChange(e)
                          options: =>
                            equipments = []
                            if AccountStore.isMotorClubId(that.props.account_id, that.getId())
                              addons = ServiceStore.getAddons(that.getId())
                            else
                              addons = ServiceStore.getAddons(that.getId(), fleet_company_id)

                            options = [
                              "- Select -"
                              map(sortBy(addons, 'name'), (val) ->
                                val.name
                              )...
                            ]
                            val = @record?[0]?.additions?[addition_index]?.name
                            if val? and options.indexOf(val) == -1
                              options.push(val)
                            return options
                          addition_index+"name"
                        span null,
                          "at a"
                        @renderFormGroup
                          name: "0.additions."+addition_index+".calc_type"
                          type: if !that.state.editing || @record?[0]?.additions?[addition_index]?.name == "Tesla Margin" then "span" else "selector"
                          classes: if that.state.editing && @record?[0]?.additions?[addition_index]?.name != "Tesla Margin" then "editing" else "bold"
                          containerStyle:
                            width: 100
                            margin: "0 5px"
                          getValue: =>
                            if @record?[0]?.additions?[addition_index]?.calc_type == "flat" and !that.state.editing
                              return "flat rate"
                            else
                              @record?[0]?.additions?[addition_index]?.calc_type

                          options: ->
                            return [
                              {text: "flat rate", value: "flat"}
                              {text: "percent", value: "percent"}
                            ]
                          addition_index+"calc_type"
                        span null,
                          if that.state.editing then "of" else "of invoice total of"
                        @renderFormGroup
                          name: "0.additions."+addition_index+".amount"
                          type: if !that.state.editing
                            if addition.calc_type == "flat" then "custom" else "span"
                          getView: that.getMoneyFieldView
                          inputWrapperClass: (rates, col) ->
                            if addition.calc_type == "flat"
                              getCurrencyClass(get(rates, "0.additions.#{addition_index}.currency", that.props.partnerCompany?.currency))
                          containerStyle:
                            width: 75
                            margin: "0 5px"
                          classes: (if that.state.editing then "editing " else "bold ") + (if addition.calc_type == "flat" then "money" else "percent")
                          addition_index+"amount"+addition.calc_type
                      )(index)
              if canAddItem
                Button
                  className: (if that.state.editing or that.getVehicleTypes().length-1 >= VehicleCategoryStore.getAllSorted(that.getId()).length then "invisible" else "")
                  color: "secondary-o"
                  lineHeight: 24
                  onClick: =>
                    adds = []
                    if @record?[0]?.additions?
                      adds = @record[0].additions
                    adds.push({calc_type: "flat"})
                    item = @record?[0]
                    if !item?
                      item = {}
                    item.additions = adds
                    @setrecordData("0", item)
                    that.setState
                      editing: true
                  size: "narrow small"
                  style: {
                    marginLeft: 20
                  }
                  "Add Item"
        ))(@)
      onConfirm: @saveRecord
      renderModal: ->
        @renderForm()
    )

  isPartnerOrFleetInHouse: -> UserStore.isPartner() || UserStore.isFleetInHouse()

  isPartnerInTeslaAccount: =>
    UserStore.isPartner() and @props.account_id and AccountStore.isTeslaAccount(@props.account_id, @getId())

  canEditRates: =>
    if @isPartnerInTeslaAccount()
      return false
    return @isPartnerOrFleetInHouse()

  canDeleteRates: =>
    if @isPartnerInTeslaAccount()
      return false
    return @isPartnerOrFleetInHouse() and not @isLive() and ServiceStore.getById(@props.service_id)?.name != "Storage"

  canStarRates: =>
    if @isPartnerInTeslaAccount()
      return false
    rates = @getRates()
    return rates.length > 0 and @props.live == false and ServiceStore.getById(@props.service_id)?.name != "Storage"

  render: ->
    super(arguments...)

    if !UserStore.isPartner() and (not @props.partner_id?)
      return span
        style:
          margin: "10px 15px"
          display: "inline-block"
        "Select a Partner"

    types = @getVehicleTypes()
    partnerOrFleetInHouse = UserStore.isPartner() ||
      UserStore.isFleetInHouse()

    li
      className: 'rate_item'
      div
        className: "action_header"
        h3
          className: "section_header"
          Consts.RATE_TYPE_MAP[@props.rate_type]
        if @canStarRates()
          ReactDOMFactories.a
            onClick: if partnerOrFleetInHouse then @starRateType else null
            className: "action fa " + if @isLive() then "fa-star" else "fa-star-o"
        if @canDeleteRates()
          ReactDOMFactories.a
            onClick: partial(@props.deleteRateType, @props.rate_type)
            className: "action fa fa-trash"
        if @canEditRates()
          ReactDOMFactories.i
            onClick: partial(@setEditing, true)
            className: "action fa fa-pencil"
      @renderRateTable()

module.exports = React.createFactory(RateItem)
