import React from 'react'
ServiceStore = require('stores/service_store')
UserStore    = require('stores/user_store')
RateStore    = require('stores/rate_store')
ModalStore   = require('stores/modal_store')
AccountStore = require('stores/account_store')
BaseComponent = require('components/base_component')
FeatureStore = require('stores/feature_store')
import { filter, partial, sortBy, values } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class ServicesSection extends BaseComponent
  displayName: 'ServicesSection'

  constructor: (props) ->
    super(props)
    @register(ServiceStore, ServiceStore.CHANGE)

  addService: (e) =>
    that = this

    showServicesModal = (services={}) ->
      Dispatcher.send(ModalStore.SHOW_MODAL,
        key: "service_picker_modal",
        props:
          exclude: ids
          onConfirm: (obj) =>
            if obj.ids? and obj.ids.length > 0
              for id in obj.ids
                if UserStore.isFleetInHouse()
                  if obj.copy
                    RateStore.createServiceRatesForPartner(
                      that.props.partner_id,
                      parseInt(id),
                      =>
                        that.props.setService(parseInt(id))
                        that.rerender()
                    )
                  else
                    RateStore.addTempServiceToAccount(
                      that.props.partner_id, parseInt(id)
                    )
                else
                  if  ServiceStore.getById(id)?.name == "Storage"
                      RateStore.addTempServiceToAccount(that.props.account_id, parseInt(id))
                  else
                    if obj.copy
                      RateStore.createServiceRates(that.props.account_id, parseInt(id), =>
                        that.props.setService(parseInt(id))
                        that.rerender()
                      )
                    else
                      RateStore.addTempServiceToAccount(that.props.account_id, parseInt(id))

            Dispatcher.send(ModalStore.CLOSE_MODALS)
            that.rerender()
      )

    if UserStore.isTesla()
      ids = RateStore.getFleetServicesByPartner(@props.partner_id)
      showServicesModal()
    else if UserStore.isFleetInHouse()
      ids = RateStore.getFleetServicesByPartner(@props.partner_id)
      RateStore.getPartnerServices(@props.partner_id, showServicesModal)
    else
      ids = RateStore.getServiceIdsFromRates(@props.account_id)
      showServicesModal()

    e.preventDefault()
    e.stopPropagation()

  deleteService: (service_id, e) =>
    entity = "partners"
    filter_id = @props.partner_id

    if UserStore.isPartner()
      entity = "accounts"
      filter_id = @props.account_id

    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "simple",
      props: {
        title: "Warning"
        confirm: 'Delete'
        body: "Are you sure you want to remove this "+entity+" custom rates for this service?"
        onConfirm: =>
          RateStore.deleteServiceRates(filter_id, service_id, @rerender)
          Dispatcher.send(ModalStore.CLOSE_MODALS)

          if @props.service_id == service_id
            @props.setService(null)
        }
    )

    e.preventDefault()
    e.stopPropagation()

  fetchServices: () =>
    if UserStore.isPartner()
      #TODO: need to also grab these myself from rates in case they've changed
      ids = RateStore.getServiceIdsFromRates(@props.account_id, @rerender)
    else
      ids = RateStore.getFleetServicesByPartner(@props.partner_id, @rerender)
    return ids

  getAddableAddons: () =>
    equipments = []
    if AccountStore.isMotorClubId(@props.account_id, @getId())
      equipments = ServiceStore.getAddons(@getId())
    else
      company_id = AccountStore.get(@props.account_id, "company_id", @getId())
      equipments = ServiceStore.getAddons(@getId(), company_id)

    exclude = @fetchServices()
    if exclude?
      equipments =  filter(equipments, (equipment) =>
        return equipment.id not in exclude
      )
    return equipments


  addEquipment: (addableAddons, e) =>


    Dispatcher.send(ModalStore.SHOW_MODAL,
      key: "equipment_picker_modal",
      props:
        addons: addableAddons
        onConfirm: (ids) =>
          for id in ids
            @props.setService(parseInt(id))
            if UserStore.isPartner()
              RateStore.addTempServiceToAccount(@props.account_id, parseInt(id))
            else
              RateStore.addTempServiceToPartner(@props.partner_id, parseInt(id))

          Dispatcher.send(ModalStore.CLOSE_MODALS)
          @rerender()
    )
    e.preventDefault()
    e.stopPropagation()

  canAddServices: (services) =>
    if UserStore.isPartner() and @props.account_id and AccountStore.isTeslaAccount(@props.account_id, @getId())
      return false

    extra_service_count = 0
    if FeatureStore.isFeatureEnabled("Storage")
      extra_service_count = 1

    return UserStore.isPartner() || UserStore.isFleetInHouse() and services? and ServiceStore.getServices()? and services.length < (values(ServiceStore.getServices()).length+extra_service_count)

  render: ->
    super(arguments...)

    if !UserStore.isPartner() and (not @props.partner_id?)
      return span
        style:
          margin: "10px 15px"
          display: "inline-block"
        "Select a Partner"

    services = []
    addons = []

    all = @fetchServices()

    if all?
      for id in all
        if id?
          service = ServiceStore.getById(id, true, @getId())
          if service?
            if service.addon == true
              addons.push(service)
            else
              services.push(service)
        else
          services.push(null)

    services = sortBy(services, (i) -> if i? then return i.name.toLowerCase())
    if addons?
      addons = sortBy(addons, (i) -> if i? then return i.name.toLowerCase())

    addableAddons = @getAddableAddons()

    div
      className: "rate_col"
      div null,
        div
          className: "action_header"
          h3
            className: "section_header"
            "Services"

          if @canAddServices(services)
            ReactDOMFactories.a
              onClick: @addService
              className: "action"
              "+"
        ul
          className: "key_list"
          if UserStore.isPartner() || null in services || UserStore.isFleetInHouse()
            li
              className: if not @props.service_id? then "selected" else "",
              onClick: partial(@props.setService, null)
              "Default"

          for service in services
            if service?
              if service.name == "Storage" and not FeatureStore.isFeatureEnabled("Storage")
                continue
              li
                key: service.id
                className: if @props.service_id == service.id then "selected" else "",
                onClick: partial(@props.setService, service.id)
                service.name
                if UserStore.isPartner() || UserStore.isFleetInHouse()
                  ReactDOMFactories.i
                    onClick: partial(@deleteService, service.id)
                    className: "fa fa-times-circle"
        if not all?
          Loading null

        div
          className: "action_header"
          h3
            className: "section_header"
            "Additional Items"
          if ((UserStore.isPartner() and !AccountStore.isTeslaAccount(@props.account_id, @getId())) || UserStore.isFleetInHouse()) and addableAddons.length > 0
            ReactDOMFactories.a
              onClick: partial(@addEquipment, addableAddons)
              className: "action"
              "+"

        ul
          className: "key_list"
          if addons?
            for service in addons
              li
                key: service.id
                className: if @props.service_id == service.id then "selected" else "",
                onClick: partial(@props.setService, service.id)
                service.name
                if (UserStore.isPartner() || UserStore.isFleetInHouse())
                  ReactDOMFactories.i
                    onClick: partial(@deleteService, service.id)
                    className: "fa fa-times-circle"
          else if UserStore.isFleet()
            Loading null
        if not all?
          Loading null

module.exports = React.createFactory(ServicesSection)
