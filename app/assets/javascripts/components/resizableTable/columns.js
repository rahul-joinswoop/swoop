// Index to keep common configuration options and functions used in FTables

export const ICON_COL_WIDTH = 40
export const NARROW_COL_WIDTH = 110

export { default as DeleteButton } from './columns/delete_button'
export { default as EditButton } from './columns/edit_button'
export { default as NameWithPin } from './columns/name_with_pin'
export { default as LocationName } from './columns/location_name'
