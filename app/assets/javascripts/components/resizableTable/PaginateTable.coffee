import createReactClass from 'create-react-class'
import React from 'react'
import Logger from 'lib/swoop/logger'
{Table, Cell, Column} = require('components/resizableTable/ResizeTable')

Table = React.createFactory(Table)
Column = React.createFactory(Column)
BaseCell = React.createFactory(Cell)

import { extend } from 'lodash'

PaginateTabe = createReactClass(
  displayName: 'PaginateTabe'

  getInitialState: ->
    index: 0

  getValue: (rowIndex, colIndex) ->
    return @props.getValue(rowIndex+@state.index, colIndex)

  prevPage: ->
    @setState
      index: Math.max(0, @state.index-100)
  nextPage: ->
    if @state.index < @props.rowsCount - 100
      Logger.debug("about to set index to ")
      @setState
        index: Math.min(@props.rowsCount, @state.index+100)

  render: ->
    rowsCount = Math.min(100, @props.rowsCount - @state.index)
    div
      className: "paginationTable"
      div
        className: "page-controls"
        span null,
          (@state.index+1) + " - " +  (@state.index+rowsCount) + " of " + @props.rowsCount
        div
          onClick: @prevPage
          "<"
        div
          onClick: @nextPage
          ">"
      Table extend({}, @props,
        getValue: @getValue
        rowsCount: Math.min(100, @props.rowsCount - @state.index)
        rowClick: (index) =>
          if typeof @props.rowClick  == "function"
            @props.rowClick(index+@state.index)
        rowHeader: (index) =>
          if typeof @props.rowHeader  == "function"
            @props.rowHeader(index+@state.index)
        rowFooter: (index) =>
          if typeof @props.rowFooter  == "function"
            @props.rowFooter(index+@state.index)
        rowClassNameGetter: (index) =>
          className = ""
          if typeof @props.rowClassNameGetter == "function"
            className = @props.rowClassNameGetter(index+@state.index)
        rowStyleGetter: (index) =>
          Style = {}
          if typeof @props.rowStyleGetter == "function"
            Style = @props.rowStyleGetter(index+@state.index)
          Style


      )
)


module.exports =
  Table: PaginateTabe
  Column: Column
  Cell: Cell
