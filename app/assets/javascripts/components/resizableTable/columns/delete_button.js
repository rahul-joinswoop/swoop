import React from 'react'
import PropTypes from 'prop-types'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import { bind } from 'lodash'

function DeleteButton(object, props, modalProps) {
  let onClick

  if (props) {
    onClick = bind(props.removeEntry, null, object, modalProps)
  }

  return <FontAwesomeButton
    color="secondary-o"
    icon="fa-times"
    onClick={onClick}
    size="medium"
  />
}

DeleteButton.propTypes = {
  removeEntry: PropTypes.func,
}

DeleteButton.defaultProps = {
  removeEntry: (event) => {
    event.preventDefault()
    event.stopPropagation()
  },
}

export default DeleteButton
