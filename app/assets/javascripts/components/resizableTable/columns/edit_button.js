import React from 'react'
import PropTypes from 'prop-types'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import { bind } from 'lodash'

function EditButton(object, props) {
  const onClick = props ? bind(props.editEntry, null, object) : ''

  return <FontAwesomeButton
    color="secondary-o"
    icon="fa-pencil"
    onClick={onClick}
    size="medium"
  />
}

EditButton.propTypes = {
  editEntry: PropTypes.func,
}

EditButton.defaultProps = {
  editEntry: (event) => {
    event.preventDefault()
    event.stopPropagation()
  },
}

export default EditButton
