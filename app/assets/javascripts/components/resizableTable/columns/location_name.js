import LocationTypeStore from 'stores/location_type_store'

function LocationName(object) {
  return (object?.location?.location_type_id &&
    LocationTypeStore.getLocationTypeById(object.location.location_type_id)?.name) || ''
}

export default LocationName
