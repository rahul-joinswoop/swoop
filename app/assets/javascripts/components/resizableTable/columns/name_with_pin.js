import React from 'react'
import PropTypes from 'prop-types'

function NameWithPin(object, props) {
  return (
    <span>
      <i className={`fa fa-map-marker ${props.iconClasses}`} />
      {object.name}
    </span>
  )
}

NameWithPin.propTypes = {
  iconClasses: PropTypes.string,
}

NameWithPin.defaultProps = {
  iconClasses: '',
}

export default NameWithPin
