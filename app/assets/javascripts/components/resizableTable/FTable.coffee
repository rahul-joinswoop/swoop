import createReactClass from 'create-react-class'
import React from 'react'
{Table, Cell, Column} = require('components/resizableTable/ResizeTable')

RTable = React.createFactory(Table)
RColumn = React.createFactory(Column)

{Table, Cell, Column} = require('components/resizableTable/SearchTable')

STable = React.createFactory(Table)
SColumn = React.createFactory(Column)


{Table, Column, Cell}  = (require 'components/resizableTable/SortableTable')

#Table = React.createFactory(Table)
Table = React.createFactory(Table)
Column = React.createFactory(Column)
Cell = React.createFactory(Cell)

import Utils from 'utils'
import { bind, extend, get } from 'lodash'

class FTable extends React.Component
  displayName: 'FTable'

  constructor: (props) ->
    super(props)
    @UNSAFE_componentWillReceiveProps(@props)
    #  tableWidth = @getWidth()
    #  for col in @props.cols
    #  @columnPerc[col.header] = 1/@props.cols.length

  UNSAFE_componentWillReceiveProps: (props) ->

  createCell: (row, col, props) =>
    if col.value
      col.value
    else if col.func
      bind(col.func, @)(row, props)
    else if col.key
      get(row, col.key)


  getCell: (value, column, i, props) =>

    col = @props.cols[i]

    style = {}

    if col.style?
      if typeof col.style is "function"
        style = col.style(@props.rows[props.row])
      else
        style = col.style

    if !style?
      style = {}
    cellProps = extend({}, props, {
      className: (if col?.className then col.className else "") + " " + (if props.className then props.className else "")
      style: style
    })
    if @props.getCell
      return @props.getCell(value, column, i, cellProps)
    else
      return Cell cellProps, value


  getValue: (rowIndex, colIndex) =>
    col = @props.cols[colIndex]
    if rowIndex < @props.rows.length
      @createCell(@props.rows[rowIndex], col , null)

  getColumn: (i, props) =>
    col = @props.cols[i]

    columnProps = extend({}, props,
      columnKey: col.key
      flexGrow: 2
      isResizable: not col.isResizable? or col.isResizable
      header: col.header
    )

    if @props.getColumn
      @props.getColumn(i, columnProps)
    else
      if @props.sortable == false
        RColumn columnProps
      else
        Column columnProps

  render: ->
    rowHeight = 40
    if @props.rowHeight?
      rowHeight = @props.rowHeight

    headerHeight = rowHeight
    if @props.headerHeight?
      headerHeight = @props.headerHeight

    cols = @props.cols or []
    if not cols?
      cols = []

    TableElem = Table
    if @props.sortable == false
      TableElem = RTable
    if @props.search? and @props.search
      TableElem = STable

    div
      className: "generalTable "+(if @props.className? then @props.className else "")
      TableElem extend({}, @props,
        ref: "table"
        rowClick: (index, val) =>
          if not val?
            val = @props.rows[index]
          if typeof @props.rowClick  == "function"
            return @props.rowClick(index, val)
        rowHeader: (index) =>
          if not val?
            val = @props.rows[index]
          if typeof @props.rowHeader  == "function"
            return @props.rowHeader(index, val)
        rowFooter: (index) =>
          if not val?
            val = @props.rows[index]
          if typeof @props.rowFooter  == "function"
            return @props.rowFooter(index, val)
        rowClassNameGetter: (index) =>
          if not val?
            val = @props.rows[index]
          className = (index % 2 ? 'odd' : 'even') + (if val?.className? then ' ' + val.className else '')
          if typeof @props.rowClassNameGetter == 'function'
            className = @props.rowClassNameGetter(index, val)
          return className

        rowHeight: rowHeight
        getCell: @getCell
        getValue: @getValue
        headerHeight: headerHeight
        rowsCount: if @props?.rows? then @props.rows.length else 0
        columnsCount: cols.length
        cols: cols
        getColumn: @getColumn
      )

module.exports = FTable
