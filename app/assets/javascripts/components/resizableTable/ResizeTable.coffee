import createReactClass from 'create-react-class'
import React from 'react'
{Table, Cell, Column} = require('components/resizableTable/BaseTable')
import ReactDOM from 'react-dom'

Table = React.createFactory(Table)
Column = React.createFactory(Column)
BaseCell = React.createFactory(Cell)
import { extend, sortBy, values } from 'lodash'
import Utils from 'utils'

ResizeCell = createReactClass(
  displayName: 'ResizeCell'
  resizing: false
  origX: null
  origWidth: null
  handleMove: (e) ->
    @origX = e.clientX
  getInitialState: ->
    resizing: false
  onResizeDown: (e) ->
    if not @props.resizable
      return
    @setState
      resizing: true
    if typeof @props.onResizingCallback == "function"
      @props.onResizingCallback(true)
    $(window).on("mousemove", @onResizeMove)
    $(window).on("mouseup", @onResizeUp)
    @handleMove(e)
    @origWidth = $(ReactDOM.findDOMNode(@refs.cell)).outerWidth()
    @preventDefault(e)

  preventDefault: (e) ->
    e.stopPropagation()
    e.preventDefault()

  onResizeUp: (e) ->
    @setState
      resizing: false
    if typeof @props.onResizingCallback == "function"
      @props.onResizingCallback(false)
    $(window).off("mousemove", @onResizeMove)
    $(window).off("mouseup", @onResizeUp)
    @preventDefault(e)
  onResizeMove: (e) ->
    if typeof @props.onColumnResizeEndCallback == "function"
      @props.onColumnResizeEndCallback(@origWidth + (e.clientX - @origX), @props.columnKey)

  render: ->

    BaseCell extend({}, @props,
        ref: "cell"
        className: "resizableCell " + (if @props.className? then @props.className else "") + (if @state.resizing then " resizing" else "")
      ),
      @props.children
      if (not @props.resizable? or @props.resizable) and @props.onResizingCallback? and @props.onColumnResizeEndCallback?
        div
          className: "resize_handle" + (if not @props.resizable then " disabled" else "")
          onMouseDown: @onResizeDown
          onClick: @preventDefault
          ""

)

Cell = React.createFactory(ResizeCell)

class ResizeTable extends React.Component

  displayName: 'ResizeTable'
  columnPerc: null
  smallSplit: .25

  constructor: (props) ->
    super(props)
    @state = {
      resizing: false
      clickable: true
    }
    @UNSAFE_componentWillReceiveProps(@props)

  componentDidMount: ->
    window.addEventListener('resize', @windowResize)
    @_isMounted = true

  componentWillUnmount: ->
    window.removeEventListener('resize', @windowResize)
    @_isMounted = false

  windowResize: =>
    @resizeColumns(@getWidth(), 0, @props.cols.length)
    @resized()

  resized: =>
    if @_isMounted
      @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously

  UNSAFE_componentWillReceiveProps: (props) ->
    tableWidth = @getWidth()

    if not props.cols?
      @columnPerc = {}
      return
    #TODO: make this so that it inspects the cols to make sure they heven't changed
    if @columnPerc == null || not @props.cols || @props.cols.length != props.cols.length
      @columnPerc = {}


      order = 0
      #add up perc taken by set widths
      for col in props.cols
        @columnPerc[col.key] = {
          value: 1
          order: order++
          key: col.key
        }
        if col.width
          @columnPerc[col.key].fixed = true
          @columnPerc[col.key].fixedWidth = col.width
        if col.minWidth
          @columnPerc[col.key].minWidth = col.minWidth
        if col.maxWidth
          @columnPerc[col.key].maxWidth = col.maxWidth


      @resizeColumns(@getWidth(), 0, props.cols.length)
      for col in props.cols
         if not col.fixedWidth? || col.fixedWidth == false
          @columnPerc[col.key].fixed = false

      @resized()

  getWidth: () =>
    tableWidth = $(window).width() - 100
    if @props.maxWidth
      tableWidth = Math.min(@props.maxWidth, tableWidth)
    return tableWidth

  # This code below fixes the width of the columns when scrollbar appears, but unfortunately breaks job loading
  # because getSnapshotBeforeUpdate kills UNSAFE_componentWillReceiveProps -- thank you React gods
  # TODO: fix this

  # getSnapshotBeforeUpdate: (prevProps, prevState) ->
  #   return @getWidth()
  #
  # componentDidUpdate: (prevProps, prevState, snapshot) ->
  #   if snapshot && @getWidth() != snapshot
  #     @resized()

  onResizingCallback: (isResizing) =>
    setTimeout(=>
      @setState
        resizing: isResizing
        clickable: !isResizing
    , 0)
  onSmallResizeEndCallback: (newColumnWidth, columnKey) =>
    width = @getWidth()
    if columnKey == "headers"
      @smallSplit = newColumnWidth/width
    else
      @smallSplit = 1 - newColumnWidth/width

    @smallSplit = Math.min(Math.max(@smallSplit, .05), .95)
    @resized()


  applyColumnChange: (cols, perc) =>

  resizeColumns: (newWidth, startIndex, endIndex) =>
    ordered = sortBy(values(@columnPerc), "order").splice(startIndex, endIndex-startIndex)
    width = @getWidth()
    sum = 0


    #get rid of fixedWidths
    takenCareOf = 0
    for i in [ordered.length-1..0]
        col = ordered[i]
        if col? and col.fixed
          col.value = col.fixedWidth/width
          newWidth -= col.fixedWidth
          ordered.splice(i, 1)

    #all fixed width to the right
    if ordered.length == 0
      return -newWidth

    for col in ordered
      sum += col.value

    #percentage left to take up
    newRight = newWidth/width
    for col in ordered
      #Go through and assign all of the widths
      col.value = newRight*col.value/sum

    minWidthPerc = .05
    extraPercToSteal = 0

    adjusted = null
    times = 0
    while (adjusted > 0 or times == 0) and times < 10
      times++
      extraPercToSteal = 0
      adjusted = 0
      for i in [ordered.length-1..0]
        col = ordered[i]
        #make sure we adjust for minimum width
        if col.minWidth? and col.value * width < col.minWidth
          extraPercToSteal += col.minWidth/width - col.value
          col.value = col.minWidth/width
          ordered.splice(i, 1)
          adjusted++
          continue

        #make sure the columns don't get too small
        if col.value < minWidthPerc
          extraPercToSteal += minWidthPerc - col.value
          col.value = minWidthPerc
          ordered.splice(i, 1)
          adjusted++
          continue

        #make sure we adjust for max width
        if col.maxWidth? and col.value * width > col.maxWidth
          extraPercToSteal += col.maxWidth/width - col.value
          col.value = col.maxWidth/width
          ordered.splice(i, 1)
          adjusted++
          continue

      #if we just can't do it
      if ordered.length == 0
        return width * extraPercToSteal

      sum = 0
      for col in ordered
        sum += col.value
      newRight = sum - extraPercToSteal
      for col in ordered
        col.value = newRight*col.value/sum
    return 0



  onColumnResizeEndCallback: (newColumnWidth, columnKey) =>
    newColumnWidth = Math.max(newColumnWidth, 5)
    minWidthPerc = .05
    width = @getWidth()
    orig = @columnPerc[columnKey]


    if orig.minWidth >= newColumnWidth
      return

    if orig.maxWidth <= newColumnWidth
      return

    percentConsumedOnLeft = 0
    count = 0
    ordered = sortBy(values(@columnPerc), "order")
    for col in ordered
      if col.key == columnKey
        break
      count++
      percentConsumedOnLeft += col.value
    #set the width fixed of the current column

    origF = @columnPerc[columnKey].fixed
    origFW = @columnPerc[columnKey].fixedWidth
    @columnPerc[columnKey].fixed = true
    @columnPerc[columnKey].fixedWidth  = newColumnWidth



    #set new column sizes with width
    leftOverWidth = @resizeColumns(width-(width*percentConsumedOnLeft), count, @props.cols.length)

    #set it back
    @columnPerc[columnKey].fixed = origF
    @columnPerc[columnKey].fixedWidth  = origFW

    #if we failed to resize on the right side, try the left side
    #if leftOverWidth != 0
      #TODO: I think this is an overoptimization

    #just F it and put the remaining on the column that's trying to steal it
    @columnPerc[columnKey].value -= leftOverWidth/width

    @resized()

  getCell: (value, column, i, props) =>
    #TODO: turn off resize cell for fixedWidth

    tableBreak = 500
    switchWidth = 688
    tableWidth = @getWidth()
    if @props.switchWidth?
      switchWidth = @props.switchWidth
    if tableWidth < switchWidth
      small = true

    cellProps = props
    #TODO: go through and remove resize handle if nothing to resize to the right (all fixed width for example)

    if Utils.isMobile() and column.props.columnKey == "headers"
      return null

    if not @columnPerc[column.props.columnKey]? or not @columnPerc[column.props.columnKey].fixed
      cellProps = extend({}, props,
        onResizingCallback: @onResizingCallback
        resizing: @state.resizing
        className: (if small and column.props.columnKey == "headers" then "headerColCell " else "valueColCell ") + (if props.className then props.className else "")
        onColumnResizeEndCallback: if small then @onSmallResizeEndCallback else @onColumnResizeEndCallback
        columnKey: column.props.columnKey
        resizable: i < @props.columnsCount - 1 and (not @props.resizable? or @props.resizable) and not Utils.isMobile()
      )

    if small
      i = parseInt(props.row%@props.cols.length)


    if @props.getCell
      @props.getCell(value, column, i, cellProps)
    else
      Cell cellProps, value

  getValue: (rowIndex, colIndex) =>
    tableBreak = 500
    switchWidth = 688
    tableWidth = @getWidth()
    cols = @props.cols
    if @props.switchWidth?
      switchWidth = @props.switchWidth

    col = @props.cols[colIndex]
    if tableWidth < switchWidth
      columnIndex = rowIndex % @props.cols.length
      if colIndex == 0
        column = @props.getColumn(columnIndex, {})
        if column?.props? and not Utils.isMobile()
          return column.props.header
        else
          return null
      else
        return @props.getValue( parseInt(rowIndex/@props.cols.length), columnIndex)

    return @props.getValue(rowIndex, colIndex)

  getColumn: (i, props) =>
    tableBreak = 500
    switchWidth = 688
    tableWidth = @getWidth()
    cols = @props.cols
    if @props.switchWidth?
      switchWidth = @props.switchWidth

    col = @props.cols[i]
    if tableWidth < switchWidth

      columnProps = extend({}, props,
        columnKey: if i == 0 then "headers" else "values"
        flexGrow: 1
        isResizable: true
        width: if i == 0 then @smallSplit else (1-@smallSplit)
        header: if i == 0 then "headers" else "values"
      )

      return Column columnProps
    else
      columnProps = extend({}, props,
        width: if  @columnPerc[col.key]? then @columnPerc[col.key].value
      )


    if @props.getColumn
      @props.getColumn(i, columnProps)
    else
      Column columnProps

  render: ->
    tableBreak = 500
    switchWidth = 688
    tableWidth = @getWidth()
    cols = @props.cols
    if @props.switchWidth?
      switchWidth = @props.switchWidth

    if tableWidth < switchWidth
      Table extend({}, @props,
          rowsCount: @props.rowsCount * cols.length
          getValue: @getValue
          rowClick: (index) =>
            if typeof @props.rowClick  == "function"
              @props.rowClick(parseInt(index/cols.length))
          rowFooter: (index) =>
            if @props.rowFooter and parseInt(index/cols.length) != parseInt((index+1)/cols.length)
              @props.rowFooter(parseInt(index/cols.length))
          rowHeader: (index) =>
            if @props.rowHeader and parseInt(index/cols.length) != parseInt((index-1)/cols.length)
              @props.rowHeader(parseInt(index/cols.length))

          rowClassNameGetter: (index) =>
            className = ""

            if parseInt(index/cols.length) != parseInt((index+1)/cols.length)
              className += "lastInRow"


            if (parseInt(index/cols.length) != parseInt((index-1)/cols.length) and index-1 > 0) || (index == 0 && Utils.isMobile())
              className += "firstInRow"

            if typeof @props.rowClassNameGetter == "function"
              className +=  " " + @props.rowClassNameGetter(parseInt(index/cols.length))
            return className
          rowStyleGetter: (index) =>
            Style = {}
            if typeof @props.rowStyleGetter == "function"
              Style = @props.rowStyleGetter(parseInt(index/cols.length))
            return Style

          columnsCount: 2
          getColumn: @getColumn
          width: tableWidth
          className: (if @props.className? then @props.className else "") + " stackedTable " + ' resizingTable' + (if @state.resizing then " resizing" else "")
          getCell: @getCell
          rowClickable: @state.clickable and (not @props.rowClickable? or @props.rowClickable)
        )
    else
      Table extend({}, @props,
          rowsCount: @props.rowsCount
          width: tableWidth
          getValue: @props.getValue
          getColumn: @getColumn
          className: (if @props.className? then @props.className else "") + ' resizingTable' + (if @state.resizing then " resizing" else "")
          getCell: @getCell
          rowClickable: @state.clickable and (not @props.rowClickable? or @props.rowClickable)
        )

module.exports =
  Table: ResizeTable
  Column: Column
  Cell: ResizeCell
