import createReactClass from 'create-react-class'
import React from 'react'
{Table, Cell, Column} = require('components/resizableTable/ResizeTable')

RTable = React.createFactory(Table)
RColumn = React.createFactory(Column)

{Table, Column, Cell}  = (require 'components/resizableTable/SortableTable')

#Table = React.createFactory(Table)
Table = React.createFactory(Table)
Column = React.createFactory(Column)
Cell = React.createFactory(Cell)

import Utils from 'utils'
import { clone, extend, filter, get } from 'lodash'

class PaginateTabe extends React.Component
  displayName: 'PaginateTabe'
  origRows: []
  lastSearch: ""
  lastSearchRows: []

  constructor: (props) ->
    super(props)
    @state = {
      search: ""
    }
    @UNSAFE_componentWillReceiveProps(@props)
  #  tableWidth = @getWidth()
  #  for col in @props.cols
  #    @columnPerc[col.header] = 1/@props.cols.length

  getValue: (rowIndex, colIndex) ->
    return @props.getValue(@lastSearchRows[@state.index].order, colIndex)

  handleSearchChange: (e) ->
    @setState
      search: e.target.value

  UNSAFE_componentWillReceiveProps: (props) ->
    @origRows = clone(props.rows)
    i = 0
    for row in @origRows
      row.order = i++

    @lastSearchRows = @origRows
    @lastSearch = ""

  getRows: ->
    if @state.search == @lastSearch
      return @lastSearchRows


    toSearch = @origRows

    if @state.search.toLowerCase().indexOf( @lastSearch.toLowerCase()) >= 0
      toSearch = @lastSearchRows

    @lastSearch = @state.search

    @lastSearchRows = filter(@origRows, (value, index) =>
      for name in @props.search
        if typeof name == "string"
          val = get(value, name)
        else if typeof name == "function"
          val = name(value)
        if typeof val == "string" and val.toLowerCase().indexOf(@state.search.toLowerCase()) >= 0
          return true
      return false
    )

    return @lastSearchRows

  render: ->
    TableElem = Table
    if @props.sortable == false
      TableElem = RTable

    rows = @getRows()
    div
      className: "SearchTable"
      input
        className: "form-control searchbar"
        onChange: @handleSearchChange
        value: @state.search
      TableElem extend({}, @props,
        getValue: @getValue
        rowsCount: rows.length
        rows: rows
        rowClick: (index) =>
          if typeof @props.rowClick  == "function"
            @props.rowClick(@lastSearchRows[index].order)
        rowHeader: (index) =>
          if typeof @props.rowHeader  == "function"
            @props.rowHeader(@lastSearchRows[index].order)
        rowFooter: (index) =>
          if typeof @props.rowFooter  == "function"
            @props.rowFooter(@lastSearchRows[index].order)
        rowClassNameGetter: (index) =>
          className = ""
          if typeof @props.rowClassNameGetter == "function"
            className = @props.rowClassNameGetter(@lastSearchRows[index].order)
            className = className.replace("odd", "")
            className = className.replace("even", "")
            if index % 2
              className += " odd"
            else
              className += " even"
          return className
        rowStyleGetter: (index) =>
          Style = {}
          if typeof @props.rowStyleGetter == "function"
            Style = @props.rowStyleGetter(@lastSearchRows[index].order)
          return Style
      )

module.exports =
  Table: PaginateTabe
  Column: Column
  Cell: Cell
