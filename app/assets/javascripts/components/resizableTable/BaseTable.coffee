import createReactClass from 'create-react-class'
import React from 'react'
{Table, Column} = require('fixed-data-table')
import { extend, map, partial } from 'lodash'

BaseCell = createReactClass(
  displayName: 'BaseCell'
  render: ->
    style = @props.style
    if not style?
      style = {}

    style.width = @props.width
    style.height = @props.height

    props = {}

    removeProps = ["header", "row", "onSortChange", "sortDir", "sortKey", "reverseSort", "backendKey", "onResizingCallback", "resizing", "onColumnResizeEndCallback", "columnKey", "resizable" ]
    for insideProp, value of @props
      if insideProp not in removeProps
        props[insideProp] = value

    div  extend({}, props,
      className: "cell "+ (if @props.className? then @props.className else "")
      style: style

    ), @props.children

)

Cell = React.createFactory(BaseCell)

class Table extends React.Component
  displayName: 'BaseTable'

  constructor: (props) ->
    super(props)
    @state = {}
    @UNSAFE_componentWillReceiveProps(@props)
    #  tableWidth = @getWidth()
    #  for col in @props.cols
    #    @columnPerc[col.header] = 1/@props.cols.length

  UNSAFE_componentWillReceiveProps: (props) ->

  renderHeader: (row) =>
    if typeof @props.rowHeader == "function"
      header = @props.rowHeader(row)
      if header?
        return div
          className: "row_header"
          header

  renderFooter: (row) =>
    if typeof @props.rowFooter == "function"
      footer = @props.rowFooter(row)
      if footer?
        return div
          className: "row_footer"
          footer


  rowClick: (func, row) =>
    if not @props.rowClickable? or @props.rowClickable
      func(row)

  getCell: (value, column, i, props) =>
    cellProps = extend({}, props,
      width: column.props.width * 100+ "%"
      height: @props.rowHeight
      key: "cell" + i
    )
    cellProps.className = ""
    if props.header
      cellProps.className += "header"


    if props.height?
      cellProps.height = props.height

    if @props.cols[i]?.className
      cellProps.className += " "+@props.cols[i].className

    if @props.getCell
      @props.getCell(value, column, i, cellProps)
    else
      Cell cellProps, value

  getValue: (rowIndex, colIndex) =>
    if @props.getValue
      @props.getValue(rowIndex, colIndex)
    else
      ""
  getColumn: (i, props) =>
    columnProps = extend({}, props)

    if @props.getColumn
      @props.getColumn(i, columnProps)
    else
      Column columnProps

  render: ->
    columns = []
    for i in [0...@props.columnsCount]
      columns.push(@getColumn(i))
    div
      className: "swoopTable "+(if @props.className? then @props.className else "")
      style:
        width: "100%"#@props.width
        height: @props.height

      if @props.description
        div
          className: "helper-text"
          @props.description

      div
        className: "row_container table_header"
        div
          className: "row"
          for column, i in columns
            @getCell(column.props.header, column, i, {header: true, height: @props.headerHeight, row: 0})
      div
        className: "table_body"
        for row in [0...@props.rowsCount]
          values = false
          cells = map(columns, (column, i) =>
            val = @getValue(row, i)
            if val?
              values = true
            @getCell(val,column, i, {row: row})

          )
          if values
            style = (if typeof @props.rowStyleGetter == "function" then @props.rowStyleGetter(row) else {})
            div
              key: row + JSON.stringify(style)
              className: "row_container " + (if typeof @props.rowClassNameGetter == "function" then @props.rowClassNameGetter(row))
              style: style
              @renderHeader(row)
              div
                className: "row"
                onClick: partial(@rowClick, @props.rowClick, row)
                cells
              @renderFooter(row)

module.exports =
  Table: Table
  Column: Column
  Cell: BaseCell
