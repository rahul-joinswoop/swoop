import createReactClass from 'create-react-class'
import React from 'react'
import ReactDOMFactories from 'react-dom-factories'

{Table, Cell, Column} = require('components/resizableTable/PaginateTable')

PTable = React.createFactory(Table)
PColumn = React.createFactory(Column)
PCell = React.createFactory(Cell)

{Table, Cell, Column} = require('components/resizableTable/ResizeTable')

Table = React.createFactory(Table)
Column = React.createFactory(Column)

BCell = React.createFactory(Cell)
import Utils from 'utils'
import { bind, extend, get, omit } from 'lodash'

CustomSortHeaderCell = React.createFactory(createReactClass(
  handleCustomSortToggle: (e) ->
    e.preventDefault()

    if(@props.isCustomSortTurnedOn && @props.customSortKeyTurnedOn == @props.columnKey) then return
    @props.customSortChange(@props.columnKey)

  render: ->
    classes = "sortHeaderCell #{@props.className || ''}"

    if @props.isCustomSortTurnedOn && @props.customSortKeyTurnedOn == @props.columnKey then classes += " customSorting down"

    propsWithoutCustom = omit(@props, 'customSortKeyTurnedOn', 'customSortChange', 'isCustomSortTurnedOn')

    BCell extend({}, propsWithoutCustom,
        className: classes
        style: backgroundColor: "blue"
        onClick: @handleCustomSortToggle),
      ReactDOMFactories.a
        @props.children
))

SortHeaderCell = React.createFactory(createReactClass(
  SortTypes :
    ASC: -1
    DESC: 1

  reverseSortDirection: (sortDir) ->
    if sortDir == @SortTypes.DESC
      @SortTypes.ASC
    else
     @SortTypes.DESC


  _onSortChange: (e) ->
    e.preventDefault()
    if @props.onSortChange?
      @props.onSortChange(
        @props.sortKey,
        (if @props.sortDir then @reverseSortDirection(@props.sortDir) else (if @props.reverseSort? then @SortTypes.ASC else @SortTypes.DESC)),
        @props.backendKey
      )

#fa-arrow-circle-o-up
  render: ->
    classes = "sortHeaderCell "+if @props.className then @props.className else ""

    if !@props.customSortKeyTurnedOn
      if @props.sortDir?
        classes += " sorting"
        if @props.sortDir == @SortTypes.DESC
          classes += " down"
        else
          classes += " up"

      if @props.reverseSort?
        classes += " reversed"
    BCell extend({}, @props,
        className: classes
        style: backgroundColor: "blue"
        onClick: if not @props.resizing then @_onSortChange),
      @props.children
))



SortableTable = createReactClass(
  displayName: 'SortableTable'
  lastSort: []
  lastKey: null
  lastDir: null


  UNSAFE_componentWillReceiveProps: (props) ->
    @lastKey = null
    @lastDir = null
    if props.page? and props.page != @props.page
      @setState
        colSortDirs: {}
        isCustomSortTurnedOn: @state.isCustomSortTurnedOn

  sortChange: (key, dir, backendKey) ->
    obj = {}
    obj[key] = dir
    @setState
      colSortDirs: obj
      isCustomSortTurnedOn: false

    @setState
      sortKey: key
      sortDir: dir

    if @props.onSortChange?
      @props.onSortChange(backendKey, dir)

  getInitialState: ->
    resizing: false
    isCustomSortTurnedOn: @props.customSortTurnedOn

  handleCustomSortChange: (key) ->
    @setState
      isCustomSortTurnedOn: true
      sortKey: null
      sortDir: null
      colSortDirs: {}

    @lastSort = []
    @lastKey = null
    @lastDir = null

    @props.onCustomSortChange(key)

  getData: ->
    data = @props.rows

    if not @props.shouldSort? or @props.shouldSort == true
      dir = @props.sortDir
      key = @props.sortKey
      if @state? and @state.sortKey?
        key = @state.sortKey
      if @state? and @state.sortDir?
        dir = @state.sortDir

      if @lastKey != null and @lastDir != null and @lastkey == key and @lastDir == dir
        return @lastSort



      @lastKey = key
      @lastDir = dir

      if @state.customSortTurnedOn
        @lastSort = data
        return

      if key? and dir?
        data.sort((a, b) =>
          if typeof key == "function"
            str1 = key(a)
            if typeof str1 == "object" && str1 != null
              str1 = ""+str1.props.children
            str2 = key(b)
            if typeof str2 == "object" && str2 != null
              str2 = ""+str2.props.children
          else
            str1 = get(a, key)
            str2 = get(b, key)


          if str1 == str2
            sortKey = @props.sortKey
            if not sortKey?
              sortKey = "id"
            return  Utils.sortBy(sortKey, a, b, dir)
          return Utils.compare(str1, str2, dir)
      )
    @lastSort = data
    return data


  createCell: (row, col, props) ->
    if col.value
      col.value
    else if col.func
      bind(col.func, @)(row, props)
    else if col.key
      get(row, col.key)


  getCell: (value, column, i, cellProps) ->
    col = @props.cols[i]

    if cellProps.header and (col.key? || col.value? || col.func) and (col.sortable == true or not col.sortable?)
      key = col.sortFunc
      if not key?
        key = col.backendKey
      if not key?
        key = col.func
      if not key?
        key = col.value
      if not key?
        key = col.key

      if col.useCustomSort
        return CustomSortHeaderCell extend({}, cellProps,
          customSortChange: @handleCustomSortChange
          isCustomSortTurnedOn: @state.isCustomSortTurnedOn
          customSortKeyTurnedOn: @props.customSortKeyTurnedOn
          sortKey: key
          backendKey: if col.backendKey? then col.backendKey else key
        ), value
      else
        return SortHeaderCell extend({}, cellProps,
          onSortChange: @sortChange
          sortDir: if @state? and @state.colSortDirs? then @state.colSortDirs[key]
          sortKey: key
          reverseSort: col.reverseSort
          backendKey: if col.backendKey? then col.backendKey else key
        ), value

    if @props.getCell
      return @props.getCell(value, column, i, cellProps)
    else
      return BCell cellProps, value

  getValue: (rowIndex, colIndex) ->
    col = @props.cols[colIndex]
    if rowIndex < @props.rows.length
      @createCell(@lastSort[rowIndex], col , null)


  render: ->
    @getData()

    elem = Table
    if @props?.rows?.length > 100 and (not @props.paginate? or @props.paginate == true)
      elem = PTable

    elem extend({}, @props,
      getCell: @getCell
      getValue: @getValue
      ref: "table"
      rowClick: (index) =>
        if typeof @props.rowClick  == "function"
          @props.rowClick(index, @lastSort[index])
      rowHeader: (index) =>
        if typeof @props.rowHeader  == "function"
          @props.rowHeader(index, @lastSort[index])
      rowFooter: (index) =>
        if typeof @props.rowFooter  == "function"
          @props.rowFooter(index, @lastSort[index])
      rowClassNameGetter: (index) =>
        className = ""
        if typeof @props.rowClassNameGetter == "function"
          className = @props.rowClassNameGetter(index, @lastSort[index])

        if index % 2
          className += " odd"
        else
          className += " even"
        return className
      rowStyleGetter: (index) =>
        Style = {}
        if typeof @props.rowStyleGetter == "function"
          Style = @props.rowStyleGetter(index, @lastSort[index])

        return Style
    )
)


module.exports =
  Table: SortableTable
  Column: Column
  Cell: Cell
