import Api from 'api'
import React from 'react'
require('stylesheets/components/invoice_filter.scss')
import Logger from 'lib/swoop/logger'
UserStore = require('stores/user_store')
StoreSelector = React.createFactory(require('components/storeSelector'))
import BetweenDateSelectorImport from 'components/time_pickers/between_date_selector.js'
BetweenDateSelector = React.createFactory(BetweenDateSelectorImport)
CompanyStore = require('stores/company_store').default
ProviderStore = require('stores/provider_store')
BackendSuggest = React.createFactory require('components/autosuggest/backend_suggest').default
BaseComponent = require('components/base_component')
import { filter, keys, map, partial } from 'lodash'

#TODO: The state management in this is horrible and needs to be rewritten
class InvoiceFilter extends BaseComponent
  displayName: 'DateTimeSelector'

  constructor: (props) ->
    super(props)

    @state = @getInitialState()

  touched: false

  getInitialState: ->
    account_id: ''
    last_account_id: ''
    partner_id: ''
    last_partner_id: ''
    account_name: "All"
    partner_name: "All"
    fleet_id: ''
    last_fleet_id: ''
    last_company_id: ''
    fleet_name: "All"
    to: null
    last_to: null
    from: null
    last_from: null

  UNSAFE_componentWillReceiveProps: (props) ->
    if @props.showPartner != props.showPartner || @props.showFleet != props.showFleet
      @setState @getInitialState()
      setTimeout(partial(@sendFilter, true), 0)

  handleAccountChange: (id, value) =>
    @setState
      account_id: id
      account_name: value
    #make sure the state has time to set
    setTimeout(@sendFilter, 0)


  handleCompanyChange: (id, value) =>
    @setState
      company_id: id
      company_name: value
    #make sure the state has time to set
    setTimeout(@sendFilter, 0)

  handlePartnerChange: (id, value) =>
    @setState
      partner_id: id
      partner_name: value
    #make sure the state has time to set
    setTimeout(@sendFilter, 0)

  handleFleetChange: (id, value) =>
    Logger.debug(id, value)
    @setState
      fleet_id: id
      fleet_name: value
    #make sure the state has time to set
    setTimeout(@sendFilter, 0)

  setFrom: (m) =>
    @setState
      from: m
    #make sure the state has time to set
    setTimeout(@sendFilter, 0)

  setTo: (m) =>
    @setState
      to: m
    #make sure the state has time to set
    setTimeout(@sendFilter, 0)

  hasChanged: () =>
    @state.last_fleet_id != @state.fleet_id or
    @state.last_company_id != @state.company_id or
    @state.last_partner_id != @state.partner_id or @state.last_account_id != @state.account_id or
    (@state.to? and !@state.to.isSame(@state.last_to)) or (@state.last_to? and not @state.to?) or
    (@state.from? and !@state.from.isSame(@state.last_from)) or (@state.last_from? and not @state.from?)

  sendFilter: (force) =>

    if not @hasChanged() and !force
      return
    filterItems = {}
    if !isNaN(parseInt(@state.account_id))
      filterItems.account_id = parseInt(@state.account_id)

    if !isNaN(parseInt(@state.partner_id))
      filterItems.sender_id = parseInt(@state.partner_id)

    if !isNaN(parseInt(@state.fleet_id))
      filterItems.fleet_id = parseInt(@state.fleet_id)

    if !isNaN(parseInt(@state.company_id))
      filterItems.client_company_id = parseInt(@state.company_id)

    from = null
    to = null
    if @state.from?
      Logger.debug("filter", @state.from)
      if @state.from.isValid()
        from = @state.from
        filterItems["job_after"] = from.toISOString()
      #else
      #  Logger.debug("invalid from date", @state.from)
        #@setState
        #  error: "Invalid From Date"


    if @state.to?
      if @state.to.isValid()
        to = @state.to
        filterItems["job_before"] = to.toISOString()
      #else
        #@setState
        #  error: "Invalid To Date"

    @setState
      error: null
      last_from: @state.from
      last_to: @state.to
      last_account_id: @state.account_id
      last_partner_id: @state.partner_id
      last_fleet_id: @state.fleet_id
      last_company_id: @state.company_id
    @props.filterList(filterItems)

  renderPartnerAutoSuggest: =>
    div
      className: "selector extended"
      span null,
        "Partner:"
      BackendSuggest
        placeholder: 'All'
        style:
          height: 32
        onSuggestSelect: (suggest) =>
          if suggest?
            @handlePartnerChange(suggest.id, suggest.label)
          else
            @handlePartnerChange(null, null)
        getSuggestions: (options, callback) ->
          if !UserStore.isSwoop() && !UserStore.isFleetInHouse()
            return

          Api.autocomplete_rescue_companies_by_provider(options.input, 20, UserStore.getEndpoint(), {
            success: (data) =>
              locationMap = map(data, (company) ->
                id: company.id
                label: company.name
              )
              callback(locationMap)
            error: (data, textStatus, errorThrown) =>
             console.warn("Autocomplete failed")
          })

  renderAccountAutoSuggest: =>
    div
      className: "selector extended"
      style:
        width: 270
      span null,
        "Account:"
      BackendSuggest
        placeholder: 'All'
        style:
          height: 32
        onSuggestSelect: (suggest) =>
          if suggest?
            @handleAccountChange(suggest.id, suggest.label)
          else
            @handleAccountChange(null, null)
        getSuggestions: (options, callback) ->
          if !UserStore.isPartner() && !UserStore.isSwoop()
            return
          Api.autocomplete_accounts(options.input, 10, UserStore.getEndpoint(), {
            success: (data) =>
              locationMap = map(data, (company) ->
                id: company.id
                label: company.name
              )
              callback(locationMap)
            error: (data, textStatus, errorThrown) =>
             console.warn("Autocomplete failed")
          })

  addAll: (fetch) =>
    objs = fetch(@getId())
    objs[""] = name: "All", id: ""
    return objs


  getFleetManagedSuggestions: (options, callback) =>
    companies = CompanyStore.getFleetManagedForStore()
    companies = map(keys(companies), (id) ->
      {id: id, label: companies[id].name}
    )

    #This is a bit of a hack until we have a really backend powered autosuggest and/or
    # rewrite the company store to take callbacks
    if CompanyStore.isLoadingFleetManaged()
      setTimeout(partial(@getFleetManagedSuggestions, options, callback), 500)
    else
      if options.input?.length > 0
        companies = filter(companies, (company) ->
          company.label.toLowerCase().indexOf(options.input.toLowerCase()) > -1
        )
      callback(companies)
  renderFleetManagedCompanyAutoSuggest: =>
    div
      className: "selector extended"
      style:
        width: "auto"
      span null,
        "Company:"
      BackendSuggest
        placeholder: 'All'
        minCharacters: 0
        showClear: true
        style:
          height: 32
        onSuggestSelect: (suggest) =>
          if suggest?
            @handleCompanyChange(suggest.id, suggest.label)
          else
            @handleCompanyChange(null, null)
        getSuggestions: @getFleetManagedSuggestions

  render: ->
    super(arguments...)

    div
      className: "invoice_filter"
      div
        className: "filters"
        if UserStore.isFleetInHouse()
          @renderPartnerAutoSuggest()

        if UserStore.isPartner()
          @renderAccountAutoSuggest()

        if UserStore.isSwoop() and @props.showPartner
          @renderPartnerAutoSuggest()

        if UserStore.isSwoop() and @props.showFleet
          @renderFleetManagedCompanyAutoSuggest()

        #if UserStore.isSwoop() and @props.showFleet
        #  @renderAccountAutoSuggest()

        div
          className: "selector dates"
          BetweenDateSelector
            onChangeFrom: @setFrom
            onChangeTo: @setTo

      if @state.error?
        label
          className: "error"
          @state.error

module.exports = InvoiceFilter
