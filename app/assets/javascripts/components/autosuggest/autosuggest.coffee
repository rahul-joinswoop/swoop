import React from 'react'
import createReactClass from 'create-react-class'
import { extend } from 'lodash'
SuggestItem = React.createFactory(require('components/autosuggest/suggestItem'))

Autosuggest = createReactClass(
  displayName: 'Autosuggest'
  lastSelect: null
  loading: false
  getDefaultProps: ->
      suggestions: []
      onSuggestSelect: ->
      onFocus: ->
      onBlur: ->
      onChange: ->
      skipSuggest: ->

  getInitialState: ->
      isSuggestsHidden: true
      userInput: @props.initialValue
      suggests: []

  UNSAFE_componentWillReceiveProps: (props) ->
    if @props.initialValue != props.initialValue
      @setState userInput: props.initialValue

  componentDidMount: ->
    @setInputValue @props.initialValue

  setInputValue: (value) ->
    @setState userInput: value

  onInputChange: ->
    userInput = @refs.suggestInput.value
    @setState { userInput: userInput }, (=>
      @showSuggests()
      @props.onChange userInput
      return
    )

  onFocus: ->
    #Timeout necessary because of other timeouts
    setTimeout (=>
      @props.onFocus()
      @showSuggests()
    ), 150
  update: (value) ->
    @setState userInput: value
    @props.onChange value

  clear: ->
    @setState { userInput: '' }, (=>
      @hideSuggests()
    )

  updateSuggests: (suggestsGoogle) ->
    suggests = []
    if @state.userInput?.length > 0
      @props.suggestions.forEach (suggest) =>
        if suggest? and @state.userInput? and suggest.toLowerCase().indexOf(@state.userInput.toLowerCase()) >= 0
          suggests.push suggest
    if suggests.length == 0
      suggests = @props.suggestions

    @setState suggests: suggests

  showSuggests: ->
    @updateSuggests()
    @setState isSuggestsHidden: false

  clickedOff: (e) ->
    #timeout necessary so that it gives the item select cick time to work first
    setTimeout (=>
      currentTime = (new Date()).getTime()
      if @lastSelect == null || currentTime - @lastSelect > 100
        @selectSuggest @state.activeSuggest
        @hideSuggests()
      ), 100
  hideSuggests: ->
    @props.onBlur()
    setTimeout (=>
      @setState isSuggestsHidden: true
    ), 100

  componentDidUpdate: (event) ->
    sub = $(".geosuggest-item--active")
    list = $(@refs.suggestList)
    if sub.length > 0
      if sub.position().top < list.scrollTop()
        list.scrollTop(sub.position().top)
      else if sub.position().top + sub.outerHeight(true) > list.scrollTop() + list.height()
        list.scrollTop(sub.position().top + sub.outerHeight(true) - list.height())
  onInputKeyDown: (event) ->
    switch event.which
      when 40
        # DOWN
        event.preventDefault()
        @activateSuggest 'next'
      when 38
        # UP
        event.preventDefault()
        @activateSuggest 'prev'
      when 13
        # ENTER
        event.preventDefault()
        #@selectSuggest @state.activeSuggest

        focusable = $("body").find('input,a,select,button,textarea').filter(':visible')
        next = focusable.eq(focusable.index(event.target)+1)
        if next.length
          next.focus()

      when 9
        # TAB
        @selectSuggest @state.activeSuggest
      when 27
        # ESC
        @hideSuggests()
      else
        break
    return
  activateSuggest: (direction) ->
    if @state.isSuggestsHidden
      @showSuggests()

    suggestsCount = @state.suggests.length - 1
    next = direction == 'next'
    newActiveSuggest = null
    newIndex = 0
    i = 0
    # eslint-disable-line id-length
    i
    while i <= suggestsCount
      if @state.suggests[i] == @state.activeSuggest
        newIndex = if next then i + 1 else i - 1
      i++
    if !@state.activeSuggest
      newIndex = if next then 0 else suggestsCount
    if newIndex >= 0 and newIndex <= suggestsCount
      newActiveSuggest = @state.suggests[newIndex]
    @setState activeSuggest: newActiveSuggest
    return
  selectSuggest: (suggest) ->
    @lastSelect = (new Date()).getTime()
    if !suggest
      suggest = @state.userInput

    if suggest? and @props.suggestions?
      if suggest not in @props.suggestions
        for s in @props.suggestions
          if suggest? and s? and typeof(suggest) == "string" and typeof(suggest) == "string" and suggest.toLowerCase().replace(" ", "") == s.toLowerCase().replace(" ", "")
            suggest = s
            break
    @setState
      isSuggestsHidden: true
      userInput: suggest
      activeSuggest: null

    @props.onSuggestSelect suggest

  onClick: ->
    @setState userInput: ""

  render: ->
    suggests = @getSuggestItems()
    div
      className: 'geosuggest ' + @props.className
      style: extend({}, { height: 32, margin: 0 }, @props.style)
      input
          className: 'geosuggest__input'
          ref: 'suggestInput'
          type: 'text'
          value: @state.userInput
          style:
            height: "calc(100%)"
            padding: "0px 6px"
          placeholder: @props.placeholder
          disabled: @props.disabled
          onKeyDown: @onInputKeyDown
          onChange: @onInputChange
          onFocus: @onFocus
          onBlur: @clickedOff
          onClick: @onClick
      if suggests? and suggests.length > 0
        ul
          className: @getSuggestsClasses()
          ref: "suggestList"
          suggests
  getSuggestItems: ->
    suggests = []
    if @props.sortFunc?
      suggests = @props.sortFunc(@state.suggests)
    else
      suggests = @state.suggests.sort((a, b) ->
        if a? and b?
          a.toLowerCase().localeCompare(b.toLowerCase())
      )

    suggests.map ((suggest) =>
      isActive = @state.activeSuggest and suggest == @state.activeSuggest
      SuggestItem
        key: if suggest?.placeId? then suggest.placeId else suggest
        myKey: if suggest?.placeId? then suggest.placeId else suggest
        suggest: suggest
        isActive: isActive
        onSuggestSelect: @selectSuggest
    )
  getSuggestsClasses: ->
    classes = 'geosuggest__suggests autosuggest_list'
    classes += if @state.isSuggestsHidden then ' geosuggest__suggests--hidden' else ''
    classes
)
module.exports = Autosuggest
