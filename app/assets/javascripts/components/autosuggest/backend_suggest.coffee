import React from 'react'
require('stylesheets/components/autosuggest/backend_suggest.scss')
SuggestItem = React.createFactory(require('components/autosuggest/suggestItem'))
BaseComponent = require('components/base_component')
import { debounce, extend, find } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import onClickOutside from 'react-onclickoutside'

export class BackendSuggest extends BaseComponent
  displayName: 'BackendSuggest'
  lastSelect: null
  constructor: (props) ->
    super(props)
    @state = state =
      isSuggestsHidden: true
      userInput: null
      activeSuggest: null
      suggests: null
      state: ""

    if @props.initialValue?
      state.userInput = @props.initialValue

  @defaultProps = {
      initialValue: ''
      disabled: false
      className: ''
      setHeight: true
      minCharacters: 2
      minCharactersWord: 'two'
      onSuggestSelect: ->
      onFocus: ->
      onBlur: ->
      onChange: ->
      skipSuggest: ->
      getSuggestions: (options, callback) ->
        callback([{label: options.input, id: 1},{label: "cow", id: 12},{label: "abc", id: 13},{label: "def", id: 14},{label: "asd", id: 15},{label: "dfg", id: 16}])

      getSuggestLabel: (suggest) ->
        suggest.description
      autoActivateFirstSuggest: false
    }


  UNSAFE_componentWillReceiveProps: (props) ->
    super(props)
    if @props.initialValue != props.initialValue and !@state.userChange
      @setState
        userInput: props.initialValue
        userChange: false
    else
      @setState
        userChange: false

    return
  componentDidMount: ->
    super(arguments...)
    @setInputValue @props.initialValue

  componentWillUnmount: ->
    clearTimeout(this.clickedOffTimeout)
    clearTimeout(this.hideSuggestTimeout)

  setInputValue: (value) =>
    @setState userInput: value
    return
  onInputChange: (e) =>
    userInput = e.target.value

    if userInput != @state.userInput
      @setState { userInput: userInput, userChange: true }, (->
        @props.onSuggestSelect(null)
        @showSuggests()
        @props.onChange userInput
      ).bind(this)
    return
  onFocus: =>
    @props.onFocus()
    @showSuggests()
    return

  clear: =>
    @setState { userInput: '' }, (=>
      @hideSuggests()
      return
    )
    return

  searchSuggests: debounce(->
    if !@state.userInput && @props.minCharacters != 0
      @updateSuggests()
      return

    if @state.userInput?.trim().length < @props.minCharacters
      return

    options = input: @state.userInput

    @props.getSuggestions options, ((suggests) ->
      @updateSuggests suggests
      if @props.autoActivateFirstSuggest
        @activateSuggest 'next'
      return
    ).bind(this)
    return
  , 250)

  updateSuggests: (suggests) =>
    @setState suggests: suggests

  showSuggests: =>
    @searchSuggests()
    @setState
      isSuggestsHidden: false

  hideSuggests: =>
    @props.onBlur()
    this.hideSuggestTimeout = setTimeout (=>
      @setState
        isSuggestsHidden: true
        suggests: null
    ), 100

  onInputKeyDown: (event) =>
    switch event.which
      when 40
        # DOWN
        event.preventDefault()
        @activateSuggest 'next'
      when 38
        # UP
        event.preventDefault()
        @activateSuggest 'prev'
      when 13
        #Enter
        event.preventDefault()
        @selectSuggest @state.activeSuggest
        @props.onEnter?()
      when 9
        # TAB
        @selectSuggest @state.activeSuggest
      when 27
        # ESC
        @hideSuggests()

        break
    return
  activateSuggest: (direction) =>
    if @state.isSuggestsHidden
      @showSuggests()
      return
    if @state.suggests?
      suggestsCount = @state.suggests.length - 1
      next = direction == 'next'
      newActiveSuggest = null
      newIndex = 0
      i = 0
      # eslint-disable-line id-length
      i
      while i <= suggestsCount
        if @state.suggests[i] == @state.activeSuggest
          newIndex = if next then i + 1 else i - 1
        i++
      if !@state.activeSuggest
        newIndex = if next then 0 else suggestsCount
      if newIndex >= 0 and newIndex <= suggestsCount
        newActiveSuggest = @state.suggests[newIndex]
      @setState activeSuggest: newActiveSuggest
      return

  selectSuggest: (suggest) =>
    if !suggest
      @handleClickOutside()
      return
    @props.onSuggestSelect(suggest)

    @setState
      isSuggestsHidden: true
      activeSuggest: null
      userInput: suggest.label

    return


  clickedOff: (e) =>
    input = @state.userInput
    if input and @state.suggests?
      suggest = find(@state.suggests, (suggest) =>
        suggest.label.toLowerCase() == input.toLowerCase()
      )
      if suggest
        @props.onSuggestSelect(suggest)

    @props.onBlur?()
    this.clickedOffTimeout = setTimeout (=>
      @hideSuggests()
    ), 100



  handleClickOutside: (e) =>
    @hideSuggests()

  onClick: (e) =>
    @props.onClick?(e)

  selectAll: ->
    if @state.userInput?
      @refs.input?.setSelectionRange(0, @state.userInput.length)

  removeSuggest: =>
    @clear()
    @props.onSuggestSelect(null)

  render: ->
    super(arguments...)

    style = @props.style
    if @props.setHeight
      style = extend({}, { height: 34 }, @props.style)

    div
      className: 'geosuggest backendSuggest ' + @props.className
      style: style
      input
        className: 'geosuggest__input'
        type: 'text'
        value: @state.userInput
        ref: "input"
        style:
          height: "calc(100%)"
          padding:  "0px 15px 0px 6px"
        placeholder: @props.placeholder || 'Type to search'
        disabled: @props.disabled
        onKeyDown: @onInputKeyDown
        onChange: @onInputChange
        onFocus: @onFocus
        onClick: @onClick
        onBlur: @clickedOff
      if @state.userInput?.length > 0 and @props.showClear
        ReactDOMFactories.i
          className: 'fa fa-times-circle'
          onClick: @removeSuggest
      if @state.userInput?.trim().length < @props.minCharacters
        ul
          className: @getSuggestsClasses()
          SuggestItem
            myKey: "type"
            suggest: "Type "+@props.minCharactersWord+" characters to search"
            className: "info"
            onSuggestSelect: -> null
      else if @state.userInput?.length >= @props.minCharacters
        ul
          className: @getSuggestsClasses()
          if @state.suggests and @state.suggests.length > 0
            @getSuggestItems()
          else if !@state.suggests?
            SuggestItem
              myKey: "type"
              suggest: "Loading..."
              className: "info"
              onSuggestSelect: -> null

  renderOptionsLabel: (label) ->
    li
      className: 'geosuggest-item title'
      label

  getSuggestItems: =>
    @state.suggests.map ((suggest) =>
      isActive = @state.activeSuggest and suggest == @state.activeSuggest
      SuggestItem
        key: suggest.id
        myKey: suggest.id
        suggest: suggest
        isActive: isActive
        className: if suggest.id == @props.currentId then 'bold' else ''
        onSuggestSelect: @selectSuggest
    )
  getSuggestsClasses: =>
    classes = 'geosuggest__suggests'
    classes += if @state.isSuggestsHidden then ' geosuggest__suggests--hidden' else ''
    classes



export default onClickOutside(BackendSuggest)
