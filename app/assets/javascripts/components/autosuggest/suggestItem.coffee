import React from 'react'
class SuggestItem extends React.Component
  onClick: (event) =>
    event.preventDefault()
    event.stopPropagation()
    @props.onSuggestSelect @props.suggest
    return

  getSuggestClasses: ->
    className = @props.suggest.className

    classes = 'geosuggest-item'
    classes += if @props.isActive then ' geosuggest-item--active' else ''
    classes += if className then ' ' + className else ''
    classes += if @props.className then ' ' + @props.className else ''
    classes

  render: ->
    li
      className: @getSuggestClasses()
      onClick: @onClick
      key: @props.myKey
      if @props.suggest?.label? then @props.suggest.label else @props.suggest

module.exports = SuggestItem
