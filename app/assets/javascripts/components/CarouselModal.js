import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { map } from 'lodash'
import 'stylesheets/components/CarouselModal.scss'

import TransitionModal from 'components/modals_new'
import Carousel from 'components/Carousel'
import CarouselModalItem from 'components/CarouselModalItem'

const RESIZE_TIMEOUT = 16

class CarouselModal extends Component {
  static propTypes = {
    allPhotoItems: PropTypes.array,
    photoItem: PropTypes.object,
  }

  constructor(props) {
    super(props)
    this.modalRef = React.createRef()
    this.resizeTimer = null
    this.state = {
      modalWidth: 0,
      delaySetState: false,
    }
  }

  componentDidMount() {
    this.getModalWidth()
    if (window) {
      window.addEventListener('resize', this.resizeHandler)
    }
  }

  UNSAFE_componentWillUpdate(nextProps, nextState) {
    if (nextState.modalWidth > 0 && this.state.modalWidth === 0 && !this.state.delaySetState) {
      setTimeout(() => {
        this.setState({
          delaySetState: true,
        })
      }, 200)
    }
  }

  componentWillUnmount() {
    if (window) {
      window.removeEventListener('resize', this.resizeHandler)
    }
  }

  getModalWidth = () => {
    const viewportWidth = document.documentElement.clientWidth
    const modalWidth = (viewportWidth >= 1000 ? 1000 : Math.ceil((viewportWidth * 0.95))) - 2 - 80
    this.setState({
      modalWidth,
    })
  }

  resizeHandler = () => {
    clearTimeout(this.resizeTimer)
    this.resizeTimer = setTimeout(() => {
      this.getModalWidth()
    }, RESIZE_TIMEOUT)
  }

  render() {
    const items = map(this.props.allPhotoItems, photoItem => <CarouselModalItem key={`key-${photoItem.id}`} photoItem={photoItem} width={this.state.modalWidth} />)

    return (
      <TransitionModal {...this.props} closeOnBodyClick extraClasses="carousel-modal-wrapper" extraModalClasses="carousel-modal-dialog" fullHeightModal={false}>
        <div className="carousel-modal" ref={this.modalRef}>
          <div className="carousel-container">
            {this.state.delaySetState &&
              <Carousel
                allowDelete={false}
                buttonSize={24}
                buttonTop="calc(50% - 28px)"
                buttonXOffset={16}
                containerWidth={this.state.modalWidth}
                handlePhotoDelete={this.props.handlePhotoDelete}
                hideScrollBar
                items={items}
                scrollSpeed={7}
                startItem={this.props.indexClicked}
              />}
          </div>
          <h2 className="carousel-title prevent-modal-close">{`${this.props.photoItem?.container} Photos`}</h2>
        </div>
      </TransitionModal>
    )
  }
}

export default CarouselModal
