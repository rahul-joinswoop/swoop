import createReactClass from 'create-react-class'
import React from 'react'
import { extend, find, get } from 'lodash'
import Utils from 'utils'
import Dispatcher from 'lib/swoop/dispatcher'
ModalStore = require('stores/modal_store')
EditForm = React.createFactory(require('components/edit_form'))
UserStore = require('stores/user_store')
CompanyStore = require('stores/company_store').default

#TODO: if a change comes in, show an error that something has changed and to refresh

TaxCompanyForm = createReactClass(
  displayName: 'TaxCompanyForm'
  submitCompany: (company) =>
    if company.id?
      CompanyStore.updateItem(company)
    else
      CompanyStore.addItem(company)
    Dispatcher.send(ModalStore.CLOSE_MODALS)

  render: ->
    enabled = not @props.enabled? or enabled == true
    data = [
      {
        name: "name"
        label: "Name*"
        required: true
        isValid: (col) ->
          value = get(@record, col.name)
          db_com = find(CompanyStore.getAllIds(), (companyId) =>
            return CompanyStore.get(companyId, 'name') == value && ""+@record.id != ""+companyId
          )
          if db_com?
            return "Name Exists"
      }
      {
        name: "tax_id"
        label: "Tax ID*"
        classes: "right"
        required: true
        containerStyle:
          width: "48%"
        placeholder: "00-0000000"
        onChange: (e) ->
          number = e.target.value.replace(/[^0-9]/g, '')
          sel = e.target.selectionStart
          if number.length > 2
            number = number.substr(0, 2) + '-' + number.substr(2, 7)
            if sel==3
              sel++

          @setrecordData(e.target.name, number)
          if e.target?
            target = e.target
            target.setSelectionRange(sel, sel)
            setTimeout ->
              target.setSelectionRange(sel, sel)
            , 0

        isValid: (col) ->

          value = get(@record, col.name)
          if !/^[0-9]{2}-[0-9]{7}$/.test(value)
            return "Please use format 00-0000000"
          db_com = find(CompanyStore.getAllIds(), (companyId) =>
            return CompanyStore.get(companyId, 'tax_id') == value && @record.id != companyId
          )
          if db_com?
            return "Tax Id Exists"
      }
    ]
    title = "Create Company"
    if @props.record? and @props.record.id?
      title = "Edit Company"

    EditForm extend({}, @props,
      cols: [{
        style:
          marginTop: 15
        fields:data}]
      title: title
      showSubmit: enabled
      onConfirm: @submitCompany
      submitButtonName: if @props?.record then "Save" else "Add"
    )


)

module.exports = TaxCompanyForm
