import Consts from 'consts'
import React from 'react'
import EventRegistry from 'EventRegistry'
ModalStore = require('stores/modal_store')
import moment from 'moment-timezone'
BC_ID_ITTR = 1
import { takeRight } from 'lodash'

class BaseComponent extends React.Component
    constructor: (props) ->
      super(props)
      @_id = @getNewWatcherId()

      if @state?
        @state.rev = 0
      else
        @state = {rev: 0}

      @waiting_for_rerender = false

      @_event_triggers = []

      #Used to ensure we aren't rendering too much
      @_render_times = []
      @_additional_watchers = []
      @_trigged_warning = false
    #TO BE CALLED IN CONSTRUCTOR
    #ENSURES LISTENERS ARE SET UP AND DESTROYED APPROPRIATELY
    register: (store, name, callback=@scheduleRerender) =>
      if !@_event_triggers
        @_event_triggers = []
        console.error("Didn't call constructor for ", @getDisplayName())
      @_event_triggers.push({
        store: store,
        name: name,
        callback: callback
      })

    getId: => @_id

    getDisplayName: =>
      @displayName || @constructor.name

    getNewWatcherId: => BC_ID_ITTR++

    createNewWatcher: (callback=@render) =>
      watcher_id = @getNewWatcherId()
      @register(EventRegistry, EventRegistry.getTrigger(watcher_id), callback)
      @_additional_watchers.push(watcher_id)
      return watcher_id

    _rerender: =>
      if !@_mounted?
        console.warn("Super not called in componentDidMount for ", @getDisplayName())
      if @_mounted
        @setState {rev: @state.rev+1 || 0} #set state and force rerender asynchronously

    rerenderWhenModalCloses: =>
      if ModalStore.getModal()?
        if !@waiting_for_rerender
          ModalStore.bind( ModalStore.CHANGE, @rerender )
        @waiting_for_rerender = true

      else
        if @waiting_for_rerender
          ModalStore.unbind( ModalStore.CHANGE, @rerender )
          @waiting_for_rerender = false
          setTimeout(@rerender, 1000)
        else
          @_rerender()


    rerender: => @_rerender()

    scheduleRerender: =>
      EventRegistry.addPendingTriggerId(@getId())

    isMounted: -> @_mounted

    shouldComponentUpdate: (nextProps, nextState) ->
      update = !@_shallowEqual(@props, nextProps) || !@_shallowEqual(@state, nextState)

      if window?.logging?.updateChecks
        console.log("Checking shouldComponentUpdate for ", @getDisplayName(), ":", "Will update: ", update, nextProps, nextState, @props, @state)

      return update
    componentDidMount: ->
      EventRegistry.bind(EventRegistry.getTrigger(@getId()), @rerender)
      if @_event_triggers
        for trigger in @_event_triggers
          trigger.store.bind(trigger.name, trigger.callback)

      @_mounted = true

    componentWillUnmount: ->
      EventRegistry.clearListeners(@getId())
      if @_additional_watchers
        for watcher_id in @_additional_watchers
          EventRegistry.clearListeners(watcher_id)
      else
        console.warn("additional watchers is empty, scoping problem perhaps in ", @getDisplayName())

      EventRegistry.unbind(EventRegistry.getTrigger(@getId()), @rerender)
      if @_event_triggers
        for trigger in @_event_triggers
          trigger.store.unbind(trigger.name, trigger.callback)
      else
        console.warn("_event_triggers is empty, scoping problem perhaps in ", @getDisplayName())

      @_mounted = false

    UNSAFE_componentWillReceiveProps: ->
      #usefull in case super is accidentally called

    componentDidUpdate: ->
      #usefull in case super is accidentally called

    render: ->
      if !@_mounted
        if !@_previouslyRendered
          @_previouslyRendered = true
        else
          console.warn("Super not called in componentDidMount for ", @getDisplayName())
          if !@_warnedAboutComponentDidMount
            @_warnedAboutComponentDidMount = true
            Rollbar.warning("Super not called in componentDidMount for ", @getDisplayName())
      if window?.logging?.renders
        console.log("Rendering: ", @getDisplayName())
      if Consts.JS_ENV != "prod"
        @_render_times.push(moment.now())
        if @_render_times.length >= 30
          @_render_times = takeRight(@_render_times, 30)
          if @_render_times[29] - @_render_times[0] < 1000
            if !@_trigged_warning
              console.warn("Rendering ", @getDisplayName(), " more than 30 times a second.")
          else
            @_trigged_warning = false
      EventRegistry.clearListeners(@getId())

    _shallowEqual: (objA, objB) ->
      if objA == objB
        return true

      if (objA and !objB?) || (!objA? and objB)
        return false
      # Test for A's keys different from B.
      for key of objA
        if objA.hasOwnProperty(key) and (!objB.hasOwnProperty(key) or objA[key] != objB[key])
          return false
      # Test for B's keys missing from A.
      for key of objB
        if objB.hasOwnProperty(key) and !objA.hasOwnProperty(key)
          return false
      true

module.exports = BaseComponent
