import { first } from 'lodash'
import { getDirectionsService } from 'lib/google-maps'

const requests = {}

function makeDirectionsHash({ origin, destination, waypoints = [] }) {
  const hash = `${origin.lat}-${origin.lng}-${destination.lat}-${destination.lng}`

  const wayPointsHash = waypoints.map(({ location }) => `${location.lat}-${location.lng}`).
  join('-')

  return `${hash}:${wayPointsHash}`
}

export function loadRoute(request) {
  const hash = makeDirectionsHash(request)

  if (!requests[hash]) {
    requests[hash] = new Promise((resolve, reject) => {
      /* eslint-disable-next-line */
      request.travelMode = google.maps.TravelMode.DRIVING

      getDirectionsService().route(request, (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          resolve(first(result.routes))
        } else {
          reject(status, result)
        }
      })
    })
  }

  return requests[hash]
}
