import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { includes } from 'lodash'
import Consts from 'consts'
import { checkIsOnPath } from 'lib/google-maps'
import { Bounds, GlobalBoundsContext } from './Bounds'
import { loadRoute } from './DirectionsService'
import Marker from './Marker'
import Directions from './Directions'

const isDriving = job => includes([Consts.ENROUTE, Consts.TOWING], job.status)
const isNotDriving = job => includes([Consts.DISPATCHED, Consts.ONSITE], job.status)
const isFullRoute = job => includes([Consts.ENROUTE, Consts.DISPATCHED], job.status)
const isRouteToDropOff = job => includes([Consts.TOWING, Consts.ONSITE], job.status)
const isTowDestination = job => job.status === Consts.TOWDESTINATION

function createRouteRequest(job, truck) {
  let request
  if (isFullRoute(job)) {
    if (job.drop_location) {
      request = {
        destination: job.drop_location,
        origin: truck,
        waypoints: [{ location: job.service_location }],
      }
    } else {
      request = {
        destination: job.service_location,
        origin: truck,
      }
    }
  } else if (isRouteToDropOff(job)) {
    request = {
      origin: truck,
      destination: job.drop_location,
    }
  }

  return request
}

class JobRoute extends Component {
  static contextType = GlobalBoundsContext

  static propTypes = {
    isSelected: PropTypes.bool,
    job: PropTypes.object,
    path: PropTypes.array,
    setSelectedJobId: PropTypes.func,
    setTruckRoute: PropTypes.func,
    truck: PropTypes.object,
  }

  /* eslint-disable-next-line */
  state = {
    route: null,
  }

  async componentDidMount() {
    const { job } = this.props

    if (!isTowDestination(job)) {
      const route = await this.loadRoute()

      // context is addRoute function from GlobalBounds
      // push up route on initial route load
      this.context(job.id, route)
    }
  }

  componentDidUpdate(prevProps) {
    const {
      job, path, setTruckRoute, truck,
    } = this.props
    const { route } = this.state

    const pathToCheck = path || route?.overview_path || []
    if ((isDriving(job) && !checkIsOnPath(truck, pathToCheck)) || isNotDriving(job)) {
      this.loadRoute()
    }

    // if job status transitioned to isDriving push route up to animate along it
    if (!isDriving(prevProps.job) && isDriving(job)) {
      setTruckRoute(route)
    }

    // if job status transitioned from isDriving disable animation along route
    if (isDriving(prevProps.job) && !isDriving(job)) {
      setTruckRoute(null)
    }
  }

  componentWillUnmount() {
    const { job, setTruckRoute } = this.props

    if (isDriving(job)) {
      setTruckRoute(null)
    }
  }

  async loadRoute() {
    const { job, setTruckRoute, truck } = this.props

    const request = createRouteRequest(job, truck)

    const route = await loadRoute(request)

    // push route to Truck to animate truck along this route
    if (isDriving(job)) {
      setTruckRoute(route)
    }

    if (route !== this.state.route) {
      this.setState({ route })
    }

    return route
  }

  render() {
    const {
      job, isSelected, path, setSelectedJobId, truck,
    } = this.props
    const { route } = this.state

    const showBMarker = (isFullRoute(job) && job.drop_location && isSelected) ||
      isRouteToDropOff(job) || isTowDestination(job)

    return (
      <>
        {isFullRoute(job) && <Marker
          icon={`/assets/images/marker_${isSelected ? 'green' : 'grey'}.png`}
          id={job.id}
          isSelected={isSelected}
          label="A"
          location={job.service_location}
          setSelectedId={setSelectedJobId}
        />}

        {showBMarker && <Marker
          icon={`/assets/images/marker_${isSelected ? 'red' : 'grey'}.png`}
          id={job.id}
          isSelected={isSelected}
          label="B"
          location={job.drop_location}
          setSelectedId={setSelectedJobId}
        />}

        {!isTowDestination(job) && route && <Directions
          isSelected={isSelected}
          path={isDriving(job) ? path : null}
          route={route}
        />}

        {/* fit selected route bounds */}
        {isSelected &&
          (
            (isTowDestination(job) && <Bounds path={[truck, job.drop_location]} />) ||
            (route && <Bounds directions={route} />)
          )}
      </>
    )
  }
}

export default JobRoute
