import React, { useContext } from 'react'
import { useGoogleMap } from '@react-google-maps/api'
import Consts from 'consts'
import SwoopRMap, { MapContext as RMapContext } from 'components/rmap/map'
import SwoopMap from 'components/maps/SwoopMap'
import { withSplitIO } from 'components/split.io'

export const MapContext = React.createContext(null)

const MapProvider = ({ children, provider }) =>
  <MapContext.Provider value={provider()}>
    {children}
  </MapContext.Provider>

const useNewGoogleMap = () => useGoogleMap()
const useOldGoogleMap = () => useContext(RMapContext)

const Map = ({ children, options, treatments }) => {
  if (!treatments?.[Consts.SPLITS.DISABLE_NEW_PARTNER_FLEET_MAP]) {
    return (
      <SwoopMap onLoad={map => map.setOptions(options)}>
        <MapProvider provider={useNewGoogleMap}>
          {children}
        </MapProvider>
      </SwoopMap>
    )
  } else {
    return (
      <SwoopRMap options={options}>
        <MapProvider key="MapProvider" provider={useOldGoogleMap}>
          {children}
        </MapProvider>
      </SwoopRMap>
    )
  }
}

export default withSplitIO(Map)
