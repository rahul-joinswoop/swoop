import React from 'react'
import BaseComponent from 'components/base_component'
import UsersStore from 'stores/users_store'
import JobStore from 'stores/job_store'
import VehicleStore from 'stores/vehicle_store'
import Consts from 'consts'
import { map, sortBy, filter, partition, flatMap } from 'lodash'
import moment from 'moment-timezone'
import MicroEvent from 'microevent-github'
import Loading from 'componentLibrary/Loading'
import DriverList from 'components/modals/DriverMapModal/DriverList'
import SwoopMap from './Map'
import 'stylesheets/components/PartnerFleetMap.scss'
import MapConfiguration, { withSettings } from './MapConfiguration'
import Truck from './Truck'
import { GlobalBounds } from './Bounds'

const MAP_LOAD_DELAY = 500
const GPS_ALERT_TIMEOUT_SECONDS = 5 * 60

class PartnerFleetMap extends BaseComponent {
  alertGpsUpdateTimeout = {}

  eventHub = new MicroEvent()

  constructor(props) {
    super(props)

    this.state = {
      selectedDriverId: null,
      showMap: false,
      search: '',
      savedVehicles: {},
      isSideBarOpen: true,
      map: null,
      selectedJobId: null,
    }

    setTimeout(() => this.setState({
      showMap: true,
    }), MAP_LOAD_DELAY)

    this.register(VehicleStore, VehicleStore.CHANGE)
    this.register(JobStore, JobStore.CHANGE)
  }

  setMapRef = (mapRef) => {
    if (!this.state.map) {
      this.setState({ map: mapRef })
    }
  }

  componentDidUpdate() {
    // reset selectedJobId when Show Routes setting is disabled
    if (!this.props.showRoutes && this.state.selectedJobId) {
      this.setSelectedJobId(null)
    }

    // reset selectedJobId when there is no such job in list
    const sortedTrucksAndDrivers = this.getSortedTrucksAndDrivers()

    const listChanged = this.sortedTrucksAndDrivers?.length !== sortedTrucksAndDrivers.length
    this.sortedTrucksAndDrivers = sortedTrucksAndDrivers

    if (listChanged) {
      this.resetSelectedJobId(sortedTrucksAndDrivers, this.state.selectedJobId)
    }
  }

  componentWillUnmount() {
    if (this.alertGpsUpdateTimeout) {
      clearTimeout(this.alertGpsUpdateTimeout)
      this.alertGpsUpdateTimeout = null
    }
    super.componentWillUnmount()
  }

  handleTruckClickInList = truckId => this.eventHub.trigger(`truckClick:${truckId}`, truckId)

  handleTruckClickOnMap = (truckId) => {
    const driverId = VehicleStore.get(truckId, 'driver_id', this.getId())
    this.setSelectedDriverId(driverId)
  }

  getDriverJobs = (id) => {
    JobStore.jobsLoaded(this.getId())
    return filter(JobStore.getJobs(), job => (
      job.rescue_driver_id === id &&
      Consts.DISPATCHED_STATUSES_AND_ASSIGNED.includes(job.status)
    ))
  }

  filterTrucksAndDriversBySearch = (trucksAndDrivers) => {
    const { search } = this.state
    if (search.length === 0) {
      return trucksAndDrivers
    } else {
      return filter(trucksAndDrivers, truck => (
        truck.driver_name?.toLowerCase().indexOf(search.trim().toLowerCase()) >= 0 ||
        truck.name?.toLowerCase().indexOf(search.trim().toLowerCase()) >= 0
      ))
    }
  }

  filterTrucksAndDriversByOnDuty = (trucksAndDrivers) => {
    if (this.props.showOffDuty) {
      return trucksAndDrivers
    } else {
      return filter(trucksAndDrivers, truck => (
        truck.driver_id && UsersStore.get(truck.driver_id, 'on_duty', this.getId())
      ))
    }
  }

  getSortedTrucksAndDrivers = () => {
    const allTrucks = VehicleStore.getSortedVehicles(this.getId())
    const [trucksWithNoDrivers, trucksWithDrivers] = partition(allTrucks, ['driver_id', null])

    let trucksAndDrivers = sortBy(trucksWithDrivers, ['name'])
    trucksAndDrivers = this.filterTrucksAndDriversByOnDuty(trucksAndDrivers)

    if (this.props.showWithoutDrivers) {
      trucksAndDrivers = trucksAndDrivers.concat(sortBy(trucksWithNoDrivers, ['name']))
    }

    trucksAndDrivers = this.filterTrucksAndDriversBySearch(trucksAndDrivers)

    return map(trucksAndDrivers, truck => (
      {
        id: truck.driver_id,
        on_duty: truck.driver_id ? UsersStore.get(truck.driver_id, 'on_duty', this.getId()) : null,
        full_name: truck.driver_name,
        jobs: truck.driver_id ? sortBy(this.getDriverJobs(truck.driver_id), job => +moment(job.last_status_changed_at)) : [],
        truck,
      }
    ))
  }

  setSearch = searchInput => this.setState({ search: searchInput })

  setSelectedDriverId = driverId => this.setState(({ selectedJobId }) => {
    const newState = { selectedDriverId: driverId }
    // deselect job if other driver was selected
    if (driverId !== JobStore.getJobById(selectedJobId)?.rescue_driver_id) {
      newState.selectedJobId = null
    }
    return newState
  })

  setVehicles = newVehicles => this.setState({ savedVehicles: newVehicles })

  setSelectedJobId = (jobId) => {
    // if click again to same job deselect it
    this.setState(({ selectedJobId }) => ({ selectedJobId: jobId === selectedJobId ? null : jobId }))
  }

  resetSelectedJobId = (drivers, selectedJobId) => {
    if (selectedJobId) {
      const jobIds = flatMap(drivers, driver => map(driver.jobs, 'id'))
      if (!jobIds.includes(selectedJobId)) {
        this.setSelectedJobId(null)
        return null
      }
    }

    return selectedJobId
  }

  handleToggleSideBar = () => this.setState(({ isSideBarOpen }) => ({ isSideBarOpen: !isSideBarOpen }))

  setAlertGpsTimeout(trucks) {
    const maxDiff = trucks.reduce((result, { truck }) => {
      if (truck.location_updated_at) {
        const diffSec = moment().diff(truck.location_updated_at, 'second')
        if (diffSec < GPS_ALERT_TIMEOUT_SECONDS) {
          /* eslint-disable-next-line */
          result = Math.max(result, diffSec)
        }
      }
      return result
    }, 0)

    if (!maxDiff) {
      return
    }

    if (this.alertGpsUpdateTimeout) {
      clearTimeout(this.alertGpsUpdateTimeout)
      this.alertGpsUpdateTimeout = null
    }

    this.alertGpsUpdateTimeout = setTimeout(this.rerender, (1 + GPS_ALERT_TIMEOUT_SECONDS - maxDiff) * 1000)
  }

  render() {
    super.render()
    if (!this.state.showMap) {
      return <Loading padding={40} size={60} />
    }

    const {
      search, selectedDriverId, isSideBarOpen, savedVehicles, selectedJobId,
    } = this.state

    const {
      getSortedTrucksAndDrivers, setSelectedDriverId, setSearch, setVehicles,
    } = this

    const vehicles = VehicleStore.getSortedVehicles(this.getId())
    const sortedTrucksAndDrivers = this.getSortedTrucksAndDrivers()

    this.setAlertGpsTimeout(sortedTrucksAndDrivers)

    // show trucks on map only if they have coordinates
    const driversWithCoordinates = sortedTrucksAndDrivers.filter(({ truck }) => truck.lat && truck.lng)

    return (
      <div className={`partner-fleet-map-page side-bar-${isSideBarOpen ? 'open' : 'closed'}`}>
        <div className="side-bar">
          <div className="content">
            <MapConfiguration />
            <DriverList
              assignDriverMethods={{
                getSortedTrucksAndDrivers, setSelectedDriverId, setSearch, setVehicles,
              }}
              getId={this.getId}
              isPartnerFleetMap
              onTruckClick={this.handleTruckClickInList}
              savedVehicles={savedVehicles}
              search={search}
              selectedDriverId={selectedDriverId}
              selectedJob={{
                id: selectedJobId,
                update: this.setSelectedJobId,
              }}
              showButton={false}
              vehicles={vehicles}
            />
          </div>
          {/* eslint-disable-next-line */}
          <div
            className="expander-container"
            onClick={this.handleToggleSideBar}
          >
            <div className="expander" />
          </div>
        </div>
        <SwoopMap
          options={{
            center: { lat: 37.7753281516829, lng: -122.414474487305 },
            streetViewControlOptions: { position: google.maps.ControlPosition.LEFT_BOTTOM },
            zoom: 16,
            zoomControlOptions: { position: google.maps.ControlPosition.LEFT_BOTTOM },
          }}
        >
          <GlobalBounds drivers={driversWithCoordinates} key="PartnerFleetMap">
            {driversWithCoordinates.map(({ jobs, truck }) =>
              <Truck
                eventHub={this.eventHub}
                jobs={jobs}
                key={truck.id}
                onClick={this.handleTruckClickOnMap}
                selectedJobId={selectedJobId}
                setSelectedJobId={this.setSelectedJobId}
                showRoutes={this.props.showRoutes}
                truck={truck}
              />
            )}
          </GlobalBounds>
        </SwoopMap>
      </div>
    )
  }
}

PartnerFleetMap.displayName = 'PartnerFleetMap'
export default withSettings(PartnerFleetMap)
