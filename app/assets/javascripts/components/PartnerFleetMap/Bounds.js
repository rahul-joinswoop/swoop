import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { createLatLngBounds } from 'lib/google-maps'
import { debounce, isFunction } from 'lodash'
import { MapContext } from './Map'

class LatLngBounds {
  bounds = {}

  getLatLng(location) {
    return {
      lat: isFunction(location.lat) ? location.lat() : location.lat,
      lng: isFunction(location.lng) ? location.lng() : location.lng,
    }
  }

  extend = (location) => {
    const latLng = this.getLatLng(location)

    return ['min', 'max'].forEach((type) => {
      if (this.bounds[type]) {
        this.bounds[type].lat = Math[type](this.bounds[type].lat, latLng.lat)
        this.bounds[type].lng = Math[type](this.bounds[type].lng, latLng.lng)
      } else {
        // initialize bounds
        this.bounds[type] = { lat: latLng.lat, lng: latLng.lng }
      }
    })
  }

  createGoogleMapBounds() {
    if (this.bounds.min && this.bounds.max) {
      return createLatLngBounds(this.bounds.min, this.bounds.max)
    }
    return null
  }
}

export class Bounds extends Component {
  static contextType = MapContext

  static propTypes = {
    directions: PropTypes.shape({
      legs: PropTypes.arrayOf(PropTypes.shape({
        steps: PropTypes.array,
      })),
    }),
    path: PropTypes.arrayOf(PropTypes.object),
  }

  padding = 40

  componentDidMount() {
    const { directions, path } = this.props

    let bounds = new LatLngBounds()

    if (directions) {
      directions.legs.forEach(leg =>
        leg.steps.forEach((step) => {
          bounds.extend(step.start_location)
          bounds.extend(step.end_location)
        })
      )
    }

    if (path) {
      path.forEach(bounds.extend)
    }

    bounds = bounds.createGoogleMapBounds()
    if (bounds) {
      // context is google map here
      this.context.fitBounds(bounds, {
        top: this.padding,
        bottom: this.padding,
        left: this.padding,
        right: this.padding,
      })
    }
  }

  render() {
    return null
  }
}

export const GlobalBoundsContext = React.createContext()

export class GlobalBounds extends Component {
  static contextType = MapContext

  static debounceDelay = 500

  static propTypes = {
    drivers: PropTypes.arrayOf(PropTypes.shape({
      jobs: PropTypes.arrayOf(PropTypes.object),
      truck: PropTypes.object,
    })),
  }

  bounds = new LatLngBounds()

  jobMap = null

  maxZoom = 15

  padding = 0

  fitBounds = debounce(() => {
    const bounds = this.bounds.createGoogleMapBounds()

    if (bounds) {
      // context is google map here
      const map = this.context

      // limit maxZoom
      map.setOptions({ maxZoom: this.maxZoom })

      // reset maxZoom after fitBounds is finished
      /* eslint-disable-next-line */
      google.maps.event.addListenerOnce(map, 'idle', () => map.setOptions({ maxZoom: null }))

      map.fitBounds(bounds, {
        top: this.padding,
        bottom: this.padding,
        left: this.padding,
        right: this.padding,
      })
    }
  }, GlobalBounds.debounceDelay)

  componentDidMount() {
    const { drivers } = this.props

    drivers.forEach(driver => this.bounds.extend(driver.truck))

    this.fitBounds()

    this.createJobMap(drivers)
  }

  componentDidUpdate() {
    const { drivers } = this.props

    if (!this.jobMap) {
      this.createJobMap(drivers)
    }
  }

  addRoute = (jobId, route) => {
    if (this.jobMap[jobId]) {
      this.jobMap[jobId] = false

      route.legs.forEach(leg =>
        leg.steps.forEach((step) => {
          this.bounds.extend(step.start_location)
          this.bounds.extend(step.end_location)
        })
      )

      this.fitBounds()
    }
  }

  createJobMap(drivers) {
    // create original job map
    const jobMap = drivers.reduce((jobs, driver) => {
      /* eslint-disable-next-line */
      driver.jobs.forEach(job => jobs[job.id] = true)
      return jobs
    }, {})

    this.jobMap = Object.keys(jobMap).length > 0 ? jobMap : null
  }

  render() {
    return (
      <GlobalBoundsContext.Provider value={this.addRoute}>
        {this.props.children}
      </GlobalBoundsContext.Provider>
    )
  }
}
