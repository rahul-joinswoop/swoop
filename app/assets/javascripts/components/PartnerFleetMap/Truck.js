import React, { Component } from 'react'
import MicroEvent from 'microevent-github'
import PropTypes from 'prop-types'
import Consts from 'consts'
import TruckAnimator from 'components/rmap/animation/TruckAnimator'
import JobRoute from './JobRoute'
import TruckMarker from './TruckMarker'

class Truck extends Component {
  static propTypes = {
    eventHub: PropTypes.instanceOf(MicroEvent),
    jobs: PropTypes.array,
    onClick: PropTypes.func,
    selectedJobId: PropTypes.number,
    setSelectedJobId: PropTypes.func,
    showRoutes: PropTypes.bool,
    truck: PropTypes.object,
  }

  marker = React.createRef()

  state = {
    route: null,
  }

  setRoute = (route) => {
    if (route !== this.state.route) {
      this.setState({ route })
    }
  }

  renderJob = (job, remaining) => {
    const { truck, selectedJobId, setSelectedJobId } = this.props

    const isSelected = selectedJobId === job.id

    switch (job.status) {
      case Consts.DISPATCHED:
      case Consts.ENROUTE:
        return <JobRoute
          isSelected={isSelected}
          job={job}
          key={job.id}
          path={remaining}
          setSelectedJobId={setSelectedJobId}
          setTruckRoute={this.setRoute}
          truck={truck}
        />

      case Consts.ONSITE:
      case Consts.TOWING:
      case Consts.TOWDESTINATION:
        return job.drop_location && <JobRoute
          isSelected={isSelected}
          job={job}
          key={job.id}
          path={remaining}
          setSelectedJobId={setSelectedJobId}
          setTruckRoute={this.setRoute}
          truck={truck}
        />

      default:
        return null
    }
  }

  render() {
    const {
      eventHub, onClick, jobs, showRoutes, truck,
    } = this.props
    const { route } = this.state

    return (
      <TruckAnimator route={route} truck={truck}>
        {({ heading, location, remaining }) => (
          <>
            <TruckMarker
              eventHub={eventHub}
              heading={heading}
              location={location}
              onClick={onClick}
              ref={this.marker}
              truck={truck}
            />

            {showRoutes && jobs.map(job => this.renderJob(job, remaining))}
          </>
        )}
      </TruckAnimator>
    )
  }
}

export default Truck
