import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import $ from 'jquery'
import MicroEvent from 'microevent-github'
import { createPoint, createMarker, createSize } from 'lib/google-maps'
import { GPSAlertHover, isLocationUpdateOutdated } from 'components/modals/DriverMapModal/DriverListElement'
import { MapContext } from './Map'
import InfoWindow from './InfoWindow'

class TruckMarker extends Component {
  static contextType = MapContext

  static propTypes = {
    eventHub: PropTypes.objectOf(MicroEvent),
    heading: PropTypes.number,
    location: PropTypes.object,
    onClick: PropTypes.func,
    truck: PropTypes.object,
  }

  imageElement = null

  infoWindow = null

  marker = null

  constructor(props) {
    super(props)

    const { truck } = props

    this.infoWindow = React.createRef()

    this.marker = createMarker({
      icon: {
        url: `/assets/images/truck.png?id=${truck.id}`,
        scaledSize: createSize(50, 22),
        anchor: createPoint(25, 11),
      },
      position: truck,
      optimized: false,
      title: truck.name,
    })

    this.marker.addListener('click', this.onMarkerClick)

    this.state = {
      infoWindowContent: null,
      showAlertLastGPSUpdate: false,
    }
  }

  componentDidMount() {
    const { eventHub, truck } = this.props

    eventHub.bind(`truckClick:${truck.id}`, this.onOutsideSelect)
    this.marker.setMap(this.context)

    this.renderInfoWindowContent()
  }

  componentDidUpdate(prevProps, prevState) {
    const { truck } = this.props

    // rerender if name has changed, location_updated_at has changed, or location_updated_at has outdated
    if (
      truck.name !== prevProps.truck.name ||
      truck.location_updated_at !== prevProps.truck.location_updated_at ||
      prevState.showAlertLastGPSUpdate !== isLocationUpdateOutdated(truck)
    ) {
      this.renderInfoWindowContent()
    }
  }

  componentWillUnmount() {
    this.marker.setVisible(false)
    this.marker.setPosition(null)
    this.marker.setMap(null)

    const { eventHub, truck } = this.props

    eventHub.unbind(`truckClick:${truck.id}`, this.onOutsideSelect)
  }

  onMarkerClick = () => {
    const { truck, onClick } = this.props

    onClick(truck.id)

    this.showInfoWindow()
  }

  onOutsideSelect = () => {
    this.marker.getMap().panTo(this.marker.getPosition())

    this.showInfoWindow()
  }

  rotateMarker(heading) {
    if (!this.imageElement) {
      // the trick with icon url is it contains truck id
      // please see constructor where icon url is created
      const iconUrl = this.marker.getIcon().url
      this.imageElement = $(`img[src="${iconUrl}"]`).first()
    }

    this.imageElement.parent().addClass('marker-visible')
    this.imageElement.css('transform', `rotate(${heading}deg)`)
  }

  showInfoWindow() {
    this.marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1)

    /* eslint-disable-next-line */
    this.infoWindow.current.open(google.maps.Marker.MAX_ZINDEX + 1)
  }

  renderInfoWindowContent() {
    const { truck } = this.props

    const infoWindowContent = (
      <div className="main-section">
        <div className="driver-and-truck-section">
          <div className="select-truck-container">
            <GPSAlertHover
              isNotAssignDriver
              listItemIndex={1}
              vehicle={truck}
            />
            <span>{truck.name}</span>
          </div>
        </div>
      </div>
    )

    const container = document.createElement('div')

    ReactDOM.render(infoWindowContent, container, () => {
      // this is hack to save ref to element in callback because for some reason this rendered content is deleted
      // TODO investigate why rendered content is deleted after sometime
      this.setState({
        infoWindowContent: container.firstChild,
        showAlertLastGPSUpdate: isLocationUpdateOutdated(truck),
      })
    })
  }

  render() {
    const { heading, location, truck } = this.props

    this.marker.setVisible(true)
    this.marker.setPosition(location || truck)

    if (heading) {
      this.rotateMarker(heading)
    }

    return <InfoWindow
      content={this.state.infoWindowContent}
      disableAutoPan
      marker={this.marker}
      ref={this.infoWindow}
    />
  }
}

export default TruckMarker
