import React from 'react'
import BaseComponent from 'components/base_component'
import Consts from 'consts'
import UserSettingStore from 'stores/user_setting_store'
import 'stylesheets/components/PartnerFleetMap/MapConfiguration.scss'

function getSetting(name, watcherId = null) {
  return UserSettingStore.getUserSettingByKeyAsBool(name, watcherId, true)
}

export function withSettings(PartnerFleetMap) {
  return class extends BaseComponent {
    static displayName = PartnerFleetMap.displayName

    render() {
      const props = {
        ...this.props,
        showRoutes: getSetting(Consts.USER_SETTING.SHOW_ROUTES_PARTNER_FLEET_MAP, this.getId()),
        showOffDuty: getSetting(Consts.USER_SETTING.SHOW_OFF_DUTY_PARTNER_FLEET_MAP, this.getId()),
        showWithoutDrivers: getSetting(Consts.USER_SETTING.SHOW_WITHOUT_DRIVERS_PARTNER_FLEET_MAP, this.getId()),
      }

      return <PartnerFleetMap {...props} />
    }
  }
}

class MapConfiguration extends BaseComponent {
  settings = [
    {
      name: Consts.USER_SETTING.SHOW_OFF_DUTY_PARTNER_FLEET_MAP,
      text: 'Show Off Duty Drivers',
    },
    {
      name: Consts.USER_SETTING.SHOW_WITHOUT_DRIVERS_PARTNER_FLEET_MAP,
      text: 'Show Trucks Without Drivers',
    },
    {
      name: Consts.USER_SETTING.SHOW_ROUTES_PARTNER_FLEET_MAP,
      text: 'Show Routes on Map',
    },
  ]

  constructor(props) {
    super(props)

    this.state = {
      open: false,
    }
  }

  handleToggle = () => this.setState(({ open }) => ({ open: !open }))

  handleUpdateSetting = (event) => {
    const { name, checked } = event.target
    UserSettingStore.setUserSettings(name, checked)
  }

  render() {
    const { open } = this.state
    const arrow = open ? 'up' : 'down'

    return (
      <div className="map-configuration">
        {/* eslint-disable-next-line */}
        <div className="map-configuration-title" onClick={this.handleToggle}>
          <i className={`fa fa-chevron-${arrow}`} />
          <span>CONFIGURE MAP</span>
          <i className={`fa fa-chevron-${arrow}`} />
        </div>
        {open &&
          <div className="map-configuration-content">
            {this.settings.map(({ name, text }) => (
              <div className="map-configuration-item" key={name}>
                <label htmlFor={name}>
                  <input
                    checked={getSetting(name, this.getId())}
                    id={name}
                    name={name}
                    onChange={this.handleUpdateSetting}
                    type="checkbox"
                  />
                  {text}
                </label>
              </div>
            ))}
          </div>}
      </div>
    )
  }
}

export default MapConfiguration
