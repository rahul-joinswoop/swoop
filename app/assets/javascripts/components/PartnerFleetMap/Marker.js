import React, { useContext, useEffect, useState } from 'react'
import { pick } from 'lodash'
import { createPoint, createMarker } from 'lib/google-maps'
import InfoWindow from './InfoWindow'
import { MapContext } from './Map'

const Marker = ({
  icon,
  id,
  isSelected,
  label,
  location,
  setSelectedId,
}) => {
  const map = useContext(MapContext)

  const [marker] = useState(() => createMarker({
    defaultAnimation: 2,
    label: {
      color: '#FFF',
      text: label,
    },
    map,
  }))

  useEffect(() => {
    marker.addListener('click', () => setSelectedId(id))

    return () => marker.setMap(null)
    /* eslint-disable-next-line */
  }, [])

  useEffect(() => marker.setIcon({
    url: icon,
    labelOrigin: createPoint(13, 15),
    /* eslint-disable-next-line */
  }), [icon])

  /* eslint-disable-next-line */
  useEffect(() => marker.setPosition(pick(location, 'lat', 'lng')), [location.lat, location.lng])

  return (isSelected ?
    <InfoWindow disableAutoPan marker={marker}>
      {location.address}
    </InfoWindow> :
    null)
}

export default Marker
