import { useContext, useEffect, useState } from 'react'
import { first } from 'lodash'
import { createPolyLine } from 'lib/google-maps'
import { MapContext } from './Map'

/* eslint-disable no-param-reassign, no-shadow */

const Directions = ({ isSelected, path, route }) => {
  const map = useContext(MapContext)

  const [polyline] = useState(() => createPolyLine({
    map,
    strokeColor: '#cccccc',
  }))

  /* eslint-disable-next-line */
  useEffect(() => () => polyline.setMap(null), [])

  // show path or first leg
  path = path || first(route.legs).steps.reduce((path, step) => {
    path.push(...step.path)
    return path
  }, [])

  // add second leg if it presents
  if (isSelected && route.legs[1]) {
    path = [...path, ...route.legs[1].steps.reduce((path, step) => {
      path.push(...step.path)
      return path
    }, [])]
  }

  polyline.setOptions({
    path,
    strokeColor: isSelected ? '#90bad4' : '#cccccc',
    zIndex: isSelected ? 1 : 0,
  })

  return null
}

export default Directions
