import { Component } from 'react'
import $ from 'jquery'
import { extend } from 'lodash'
import { createInfoWindow } from 'lib/google-maps'

class InfoWindow extends Component {
  constructor(props) {
    super(props)

    const { children } = props

    const options = extend({}, props, {
      content: children || props.content,
    })

    this.infoWindow = createInfoWindow(options)

    // TODO dirty hack to apply custom styles
    // use custom InfoWindow implementation like https://github.com/joinswoop/swoop/blob/master/app/assets/javascripts/components/maps/googleMapsPopup.js
    this.infoWindow.addListener('domready', () => $('.gm-style-iw').parent().addClass('info-window-container'))
  }

  componentDidMount() {
    this.open()
  }

  componentWillUnmount() {
    this.infoWindow.close()
  }

  open(zIndex) {
    const { marker } = this.props

    /* eslint-disable-next-line */
    this.infoWindow.open(marker.getMap(), marker)

    this.infoWindow.setZIndex(zIndex)
  }

  render() {
    const { children, content } = this.props

    this.infoWindow.setContent(children || content)

    return null
  }
}

export default InfoWindow
