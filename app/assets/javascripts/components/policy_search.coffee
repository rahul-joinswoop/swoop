import React from 'react'
policy_search_styles = require('stylesheets/components/policy_search.scss')
BaseComponent = require('components/base_component')
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
AsyncRequestStore = require('stores/async_request_store').default
import { partial, sortBy } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'
import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)

class PolicySearch extends BaseComponent
  constructor: () ->
    super arguments...
    @state =
      searching: false
      value: null
      popupOpen: false

  showError: () =>
    @setState
      error: "Policy lookup system current unavailable"
      searching: false

  searchForPolicy: () =>
    search = @props.getValue()
    if !search?
      return

    value = search.value

    @setState
      searching: true
      value: value
      popupOpen: true
      stage: 1

    @props.onShow?()
    AsyncRequestStore.searchPolicy(value, @props.jobId, {
      success: (data) =>
        if data.error?
          @showError()
          return
        @setState
          results: data.policies
          searching: false
      error: @showError
    })
    return


  searchForVehicle: (result) =>

    @setState
      searching: true
      popupOpen: true
      stage: 2
      results: null

    AsyncRequestStore.getPolicyDetails(result.id, {
      success: (data) =>
        mapped = []

        pcc_vehicles = data.pcc_drivers_and_cars.pcc_vehicles
        pcc_drivers  = data.pcc_drivers_and_cars.pcc_drivers

        for vehicle in pcc_vehicles
          for driver in pcc_drivers
            mapped.push({
              full_name: driver.name
              vehicle: vehicle.year + " " + vehicle.make + " " + vehicle.model
              vehicle_obj: vehicle
              driver_obj: driver
              policy_number: result.policy_number
              policy_id: result.id
            })

        @setState
          results: mapped
          searching: false
      error: @showError
    })

  submitResult: (result) =>
    @setState
      searching: true
      popupOpen: true
      stage: 3
      results: null

    AsyncRequestStore.getPolicyCoverage(result.policy_id, result.vehicle_obj.id, result.driver_obj.id, {
      success: (data) =>
        #Make sure the user hasn't exited out (if they do ignore results)
        if @state.popupOpen
          coverage = data.pcc_coverage
          coverage.vehicle_obj = result.vehicle_obj
          coverage.driver_obj = result.driver_obj
          @props.onSubmit(coverage)
          @cancel()
      error: @showError
    })

  enterManually: =>
    #TODO: This may need to set policy lookup type on job
    @props.enterManually()
    @cancel()

  cancel: =>
    @setState
      searching: false
      popupOpen: false
      value: null

    @props.onHide?()

  renderLoading: =>
    Loading
      message: 'Looking up policy info...'

  renderHeaderItem: (number, label, focused) =>
    div
      className: 'headerItem '+ if focused then "focused" else ""
      span
        className: 'header_number'
        number
      span
        className: 'header_name'
        label

  renderResultsHeader: =>
    div
      className: 'header'
      @renderHeaderItem("1", "Select Policy", @state.stage == 1)
      @renderHeaderItem("2", "Select Driver and Vehicle", @state.stage == 2)



  renderResult: (result, showVins) =>
    li
      className: 'result'
      onClick: if @state.stage == 1 then partial(@searchForVehicle, result) else partial(@submitResult, result)
      span null,
        result.policy_number
      span null,
        result.full_name
      if @state.stage == 1
        span null,
          result.address
      else
        span null,
          result.vehicle
      if showVins
        span null,
          "VIN *"+result.vehicle_obj.vin?.slice(-4)

  areVehicleDuplicated: (results) =>
    hash = {}
    duplicated = false
    error = ''
    for result in results
      if not result.vehicle_obj
        error = 'Error search for vehicle. Missing vehicle_obj'
        continue
      if hash[result.vehicle]? and hash[result.vehicle] != result.vehicle_obj.vin
        duplicated = true
        break
      hash[result.vehicle] = result.vehicle_obj.vin

    if error
      Rollbar.error(error, results)

    return duplicated

  getSortedResults: (results, stage) ->
    if stage == 1
      return sortBy(@state.results, "policy_number")
    return sortBy(@state.results, "full_name")

  renderResults: (results) =>
    showVins = false

    if @state.stage == 2 && @areVehicleDuplicated(results)
        showVins = true

    div null,
      @renderResultsHeader()
      div null,
        ul
          className: 'result_list '+(if showVins then "with_vins" else "")
          for result in @getSortedResults(results, @state.stage)
            @renderResult(result, showVins)
          if @state.stage == 1
            li
              className: 'result'
              onClick: @enterManually
              "Policy not found - Enter manually"
          else if @state.stage == 2
            li
              className: 'result'
              onClick: @enterManually
              "Driver and vehicle not found - Enter manually"

  renderError: (errorText, secondaryText, secondaryAction, primaryText, primaryAction) ->
    div
      className: "error"
      div null,
        ReactDOMFactories.i
          className: 'fa fa-exclamation-triangle'
        errorText
      div
        className: 'action_buttons'
        Button
          color: "secondary"
          onClick: secondaryAction
          style: {
            marginRight: 10,
          }
          secondaryText
        Button
          color: "primary"
          onClick: primaryAction
          primaryText

  render: ->
    div
      className: "policy_search"
      if @state.popupOpen
        span
          className: 'info_text'
          "Searching for \""+@state.value + "\""
      else
        Button
          color: "green"
          disabled: !@props.enabled
          onClick: =>
            if @props.enabled
              @searchForPolicy()
          "Search"
      if @state.popupOpen
        div
          className: "popup"
          span
            className: 'cancel'
            onClick: @cancel
            "×"
          if @state.searching
            @renderLoading()
          else if @state.results
            if @state.results.length > 0
              @renderResults(@state.results)
            else
              @renderError("No results found", "Update Search", @cancel, "Enter Manually", @enterManually)
          else if @state.error
            @renderError(@state.error, "Try Again", @searchForPolicy, "Enter Manually", @enterManually )

module.exports = PolicySearch
