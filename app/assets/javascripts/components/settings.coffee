import React, {createFactory} from 'react'
import createReactClass from 'create-react-class'
import Dispatcher from 'lib/swoop/dispatcher'
import Logger from 'lib/swoop/logger'
import StoreWrapper from 'StoreWrapper'
import Utils from 'utils'
import { compose } from 'recompose'
import { extend, forEach, map, partial, values } from 'lodash'
import Context from 'Context'
import 'stylesheets/components/settings.scss'
import Consts from 'consts'
import { withModalContext } from 'componentLibrary/Modal'
import './Settings.scss'

AccountFormModal = createFactory(require('components/account_form'))
AccountStore = require('stores/account_store')
BaseComponent = require('components/base_component')
CompanyFormModal = createFactory(require('components/modals/company_form'))
CompanyStore = require('stores/company_store').default
FeatureStore = require('stores/feature_store')
FleetSiteModal = createFactory(require('components/fleet_site_form'))
PartnerFormModal = createFactory(require('components/modals/partner_form'))
PoiSiteFormModal = createFactory(require('components/modals/poi_site_form'))
PoiStore = require('stores/poi_store').default
ProviderStore = require('stores/provider_store')
Rates = createFactory(require('components/settings/rates'))
SiteModal = createFactory(require('components/site_form'))
SiteStore = require('stores/site_store').default
Storage = require('stores/storage')
TrailerFormModal = createFactory(require('components/modals/trailer_form'))
TrailerStore = require('stores/trailer_store')
TransitionModal = createFactory(require 'components/modals')
UserFormModal = createFactory(require('components/modals/UserForm').default)
UserStore = require('stores/user_store')
UsersStore = require('stores/users_store').default
import VehicleFormModal from 'components/vehicle_form'
VehicleStore = require('stores/vehicle_store')
VersionStore = require('stores/version_store')

import AppVersionsImport from 'components/AppVersions'
AppVersions = React.createFactory(AppVersionsImport)

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)

import ConfigurationImport from 'components/settings/configuration.js'
Configuration = React.createFactory(ConfigurationImport)

import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

import TabsImport from 'components/tabs'
Tabs = createFactory(TabsImport)

createFactoryStoreWrapper = compose(createFactory, StoreWrapper)

ACCOUNTS = 1
BRANCHES = 12
COMPANIES = 5
CONFIGURE = 13
FEATURES = 9
INTEGRATIONS = 18
PARTNERS = 2
POI_SITES = 16
RATES = 10
SITES = 11
TOOLS = 15
TRAILERS = 14
TRUCKS = 4
USERS = 3
APPVERSIONS = 20

ICON_COL_WIDTH = 40
SMALL_WIDTH = 30

AccountList = createFactory(require('components/settings/account_list'))
CompanyList = createFactory(require('components/settings/company_list'))
FTable = createFactory(require 'components/resizableTable/FTable')
Integrations = createFactory(require('components/settings/integrations/integrations').default)
PartnerList = createFactory(require('components/settings/partner_list'))
PoiSiteList = createFactory(require('components/settings/poi_site_list').default)
SiteList = createFactory(require('components/settings/site_list'))
ToolsList = createFactory(require('components/settings/tools_list'))
TrailerList = createFactory(require('components/settings/trailer_list'))
TruckList = createFactory(require('components/settings/truck_list'))
UserList = createFactory(require('components/settings/user_list'))

FeatureList = createReactClass(
  displayName: 'FeatureList'
  cols: [
    { header: "ID", key: "id" }
    { header: "Name", key: "name" }
    { header: "Description", key: "description" }
  ]
  componentDidMount: ->
  componentWillUnmount: ->
  generalChanged: ->
    @setState {
      rev: if @state?['rev']? then ++@state?['rev'] else 0 #set state and force rerender asynchronously
    }

  render: ->
    FTable extend({}, @props,
      rows: values(@props.features)
      cols: @cols
      sortKey: "name"
      sortDir: 1
    )
)

FeatureList = createFactoryStoreWrapper(FeatureList, () ->
  return {
    features: FeatureStore.getAll()
  }
)

ACCOUNTS_TAB =
  'id': ACCOUNTS
  'name': 'Accounts'
  'url': '#accountstabcontents'
  'buttonText' : 'Create Account'
  'modal' : 'account'
  'modal_obj': AccountFormModal
  'store': AccountStore
  'component': AccountList

BRANCH_TAB =
  'id': BRANCHES
  'name': if UserStore.isEnterprise() then 'Branches' else 'Sites'
  'modal': 'site'
  'modal_obj': FleetSiteModal
  'store': SiteStore
  'component': SiteList
  'buttonText': 'Add ' + (if UserStore.isEnterprise() then 'Branch' else 'Site')

COMPANIES_TAB =
  'id': COMPANIES
  'name': 'Companies'
  'url': '#companiestabcontents'
  'modal': "company"
  'modal_obj': CompanyFormModal
  'store': CompanyStore
  'component': CompanyList
  'buttonText': 'Create Company'
  'extra_props': defaultType: 'RescueCompany'

CONFIGURE_TAB =
  'id' : CONFIGURE
  'name' : 'Configure'
  'component': Configuration

FEATURES_TAB =
  'id': FEATURES
  'name': 'Features'
  'url': '#featurestabcontents'
  'modal': 'feature'
  'store': FeatureStore
  'component': FeatureList

INTEGRATIONS_TAB =
  'id': INTEGRATIONS
  'name': 'Integrations'
  'component': Integrations

PARTNERS_TAB =
  'id': PARTNERS
  'name': 'Partners'
  'url': '#partnerstabcontents'
  'modal' : 'partner'
  'modal_obj': PartnerFormModal
  'store': ProviderStore
  'component': PartnerList
  'addCallback': -> Utils.showModal('search_partner')

POI_SITES_TAB =
  'id': POI_SITES
  'name': 'Places'
  'modal': 'poiSite'
  'modal_obj': PoiSiteFormModal
  'store': PoiStore
  'component': PoiSiteList
  'buttonText': 'Add Place'

RATES_TAB =
  'id': RATES
  'name': 'Rates'
  'component': Rates

SITES_TAB =
  'id': SITES
  'name': 'Sites'
  'modal': 'site'
  'modal_obj': SiteModal
  'store': SiteStore
  'component': SiteList
  'buttonText': 'Add Site'

TOOLS_TAB =
  'id': TOOLS
  'name': 'Tools'
  'url': '#toolstabcontents'
  'component': ToolsList

TRAILERS_TAB =
  'id': TRAILERS
  'name': 'Trailers'
  'url': '#trailerstabcontents'
  'buttonText' : 'Create Trailer'
  'modal': 'trailer'
  'modal_obj': TrailerFormModal
  'store': TrailerStore
  'component': TrailerList

TRUCKS_TAB =
  'id': TRUCKS
  'name': 'Trucks'
  'url': '#truckstabcontents'
  'buttonText' : 'Create Truck'
  'modal': 'vehicle'
  'modal_obj': (props) => <VehicleFormModal {...props}/>
  'store': VehicleStore
  'component': TruckList

USERS_TAB =
  'id': USERS
  'name': 'Users'
  'url': '#userstabcontents'
  'buttonText' : 'Create User'
  'modal': 'user'
  'modal_obj': UserFormModal
  'store': UsersStore
  'component': UserList

APPVERSIONS_TAB =
  'id': APPVERSIONS
  'name': 'App Versions'
  'component': AppVersions
  'url': '#appversionstabcontents'
  'store': VersionStore

TABS = {}
TABS[ACCOUNTS] = ACCOUNTS_TAB
TABS[BRANCHES] = BRANCH_TAB
TABS[COMPANIES] = COMPANIES_TAB
TABS[CONFIGURE] = CONFIGURE_TAB
TABS[FEATURES] = FEATURES_TAB
TABS[INTEGRATIONS] = INTEGRATIONS_TAB
TABS[PARTNERS] = PARTNERS_TAB
TABS[POI_SITES] = POI_SITES_TAB
TABS[RATES] = RATES_TAB
TABS[SITES] = SITES_TAB
TABS[TOOLS] = TOOLS_TAB
TABS[TRAILERS] = TRAILERS_TAB
TABS[TRUCKS] = TRUCKS_TAB
TABS[USERS] = USERS_TAB
TABS[APPVERSIONS] = APPVERSIONS_TAB

Content = createFactory(createReactClass(
  displayName: 'Tabs'
  render: ->
    if TABS[@props.currentTab].component?
      TABS[@props.currentTab].component @props
))

Settings = createReactClass(
  displayName: 'Settings'

  getInitialState: ->
    {sentRSCConnectSuccess: false}

  generalChanged: ->
    @setState {
      rev: if @state?['rev']? then ++@state?['rev'] else 0 #set state and force rerender asynchronously
    }

  handleStatusChange: (e) ->

  changeTab: (tab) ->
    if tab?
      @setState currentTab: tab.id
    return

  showModal: (key, entry, modalProps = {}) ->
    obj = {}
    obj['show_' + key + '_modal'] = true
    obj['record'] = entry
    obj['modalProps'] = modalProps
    @setState obj


  closeModal: (key) ->
    obj = {}
    obj['show_' + key + '_modal'] = false
    @setState obj
    $('.modal-backdrop.fade.in').remove()
    $('body').removeClass('modal-open')


  sendEvent: (type, obj) ->
    if TABS[@state.currentTab][type]?
      Dispatcher.send(TABS[@state.currentTab][type], obj)

  newSubmit: (obj) ->
    callbacks = obj.callbacks
    if obj.id
      if TABS[@state.currentTab].store?.updateItem?
        TABS[@state.currentTab].store?.updateItem(obj)
      else
        @sendEvent("update_event", obj)
    else
      if TABS[@state.currentTab].store?.addItem?
        TABS[@state.currentTab].store?.addItem(obj)
        #This is a hack, backend needs some time to add to ES
        TABS[@state.currentTab].store?.reloadSearch?(2000)
      else
        @sendEvent("add_event", obj)

    if TABS[@state.currentTab].modal? && !callbacks
      @closeModal(TABS[@state.currentTab].modal)

    if obj.vehicle_category_id?
      Tracking.track('Add Truck', {
        category: 'Onboarding'
      })

  confirmRemoveEntry: (obj) ->
    if TABS[@state.currentTab].store?.deleteItem?
      TABS[@state.currentTab].store?.deleteItem(obj)
      #This is a hack, backend needs some time to add to ES
      TABS[@state.currentTab].store?.reloadSearch?(2000)
    else
      @sendEvent("delete_event", obj)
    @closeModal('remove')
    @props.hideModals()

  editEntry: (entry) ->
    if TABS[@state.currentTab].modal?
      @showModal(TABS[@state.currentTab].modal, entry)

  removeEntry: (entry, modalProps) ->
    if entry.type == 'FleetCompany'
      @removeFleetCompany(entry, modalProps)
    else if entry.type == 'RescueCompany'
      @removeRescueCompany(entry, modalProps)
    else # current case
      @showModal('remove', entry, modalProps)

  removeFleetCompany: (entry, modalProps) ->
    @props.showModal(
      TransitionModal extend({}, @props,
        show: true
        callbackKey: 'refresh'
        extraClasses: 'Settings-removeModal-warn'
        transitionName: 'refreshModalTransition'
        confirm: "Delete Client"
        cancel: "Cancel"
        onConfirm: partial(@confirmRemoveEntry, entry)
        onCancel: @props.hideModals
        title: 'Delete Client'
        message: null
        providerCompanies: modalProps?.providerCompanies
      ),
        div
          className: 'inline-block'
          div
            className: 'Settings-removeModal-paragraph'
            'Deleting this client company will remove them from the entire Swoop platform and prevent new jobs from being created for the company.'
          div
            className: 'Settings-removeModal-paragraph'
            'Are you sure you want to delete this company?'

        if @state.modalProps?.providerCompanies
          ul
            className: 'siteProviderCompanies'
            map(@state.modalProps?.providerCompanies, (company) =>
              li null,
                company.name
            )
    )

  removeRescueCompany: (entry, modalProps) ->
    @props.showModal(
      TransitionModal extend({}, @props,
        show: true
        callbackKey: 'refresh'
        extraClasses: 'Settings-removeModal-warn'
        transitionName: 'refreshModalTransition'
        confirm: "Delete Partner"
        cancel: "Cancel"
        onConfirm: partial(@confirmRemoveEntry, entry)
        onCancel: @props.hideModals
        title: 'Delete Partner'
        message: null
        providerCompanies: modalProps?.providerCompanies
      ),
        div
          className: 'inline-block'
          div
            className: 'Settings-removeModal-paragraph'
            'Deleting this partner company will remove them from the entire Swoop platform and exclude them from all job assignments. To temporarily suspend the company, unselect the Live checkbox on their company profile.'
          div
            className: 'Settings-removeModal-paragraph'
            'Are you sure you want to delete this company?'

        if @state.modalProps?.providerCompanies
          ul
            className: 'siteProviderCompanies'
            map(@state.modalProps?.providerCompanies, (company) =>
              li null,
                company.name
            )
    )

  populateTabList: () ->
    tabList = []

    if @props.user.isPartner and @props.user.isOnlyDispatcher
      tabList = [ACCOUNTS_TAB]
      tabList.push TRUCKS_TAB
    else if @props.user.isPartner
      tabList = [ACCOUNTS_TAB, RATES_TAB, SITES_TAB]
      tabList.push POI_SITES_TAB if @props.user.isAdmin
      tabList.push TRAILERS_TAB if @props.featuresEnabled.trailers
      tabList.push TRUCKS_TAB
    else if @props.user.isFleet
      tabList = []
      if @props.user.isFleetInHouse
        tabList.push PARTNERS_TAB
        tabList.push RATES_TAB if @props.user.isAdmin
      if @props.user.isAdmin
        tabList.push BRANCH_TAB if @props.featuresEnabled.fleetSites
        tabList.push POI_SITES_TAB

    if @props.user.isAdmin
      tabList.push USERS_TAB

    if @props.user.isRoot
      tabList = [
        COMPANIES_TAB, USERS_TAB, APPVERSIONS_TAB, FEATURES_TAB, PARTNERS_TAB, TOOLS_TAB
      ]

    if (!@props.user.roles.isAdmin && !@props.user.roles.isDispatcher && !@props.user.isRoot)
      tabList = []

    tabList.push(CONFIGURE_TAB)

    if (UserStore.isAdmin() && ((UserStore.isPartner()) || (UserStore.isFleetManaged() && !UserStore.isMotorClub())))
      tabList.push(INTEGRATIONS_TAB)

    return tabList

  render: ->
    if not @props.user
      return Loading()

    tabList = @populateTabList()
    currentTab = @state.currentTab

    if TABS[currentTab]?
      buttonText = TABS[currentTab].buttonText

    if currentTab == PARTNERS && @props.user.isFleetInHouse
      buttonText = "Add Partner"

    if tabList?
      for tab in tabList
        tab.props = {
          editEntry: @editEntry
          removeEntry: @removeEntry
        }

    div
      id: 'content-wrapper'
      className: 'content-wrapper dashboard settings_container'
      if buttonText?
        Button
          color: 'primary'
          onClick: if TABS[currentTab].addCallback then TABS[currentTab].addCallback else partial(@editEntry, null)
          size: 'qnav-button'
          style: {
            float: 'right',
          }
          buttonText

      div
        className: "tabs"
        if tabList? and tabList.length > 0
          forcedTab = null
          if window.location.href.includes('?integrationstab=true')
            if !window.location.href.includes('&no-rsc-connect')
              if !@state.sentRSCConnectSuccess
                Tracking.trackRSCEvent('RSC Connect Success')
                @setState sentRSCConnectSuccess: true
            else
              Context.deleteParam('no-rsc-connect')

            forEach(tabList, (tab, index) =>
              if tab.name == "Integrations"
                forcedTab = index
                Context.replaceParam('integrationstab', 'return-from-rsc-connect')
            )

          if window.location.href.includes('?configuretab')
            forEach(tabList, (tab, index) =>
              if tab.name == "Configure"
                forcedTab = index
            )

          Logger.debug("Tabs: ", Tabs)

          Tabs
            forcedTab: forcedTab
            tabList: tabList
            changeTab: @changeTab

      for t, val in tabList
        if t.modal_obj? and @state["show_"+t.modal+"_modal"]
          Logger.debug(t.modal,  @state["show_"+t.modal+"_modal"])
          t.modal_obj(extend({}, @props, t.extra_props,
            key: t.modal
            show: @state["show_"+t.modal+"_modal"]
            onCancel: partial(@closeModal, t.modal)
            onConfirm: @newSubmit
            record: @state.record
            submitButtonName: if @state.record then 'Save' else 'Add')
          )

      TransitionModal extend({}, @props,
        show: @state.show_remove_modal
        callbackKey: 'refresh'
        transitionName: 'refreshModalTransition'
        confirm: "Delete"
        cancel: "Cancel"
        onConfirm: partial(@confirmRemoveEntry, @state.record)
        onCancel: partial(@closeModal, "remove")
        title: @state.modalProps?.title || 'Delete Record'
        message: @state.modalProps?.message || null
        providerCompanies: @state.modalProps?.providerCompanies
      ),
        span
          className: 'inline-block'
          "Are you sure you want to delete this " + (@state.modalProps?.noun or "entry") + "?"
          if @state.modalProps?.message
            " " + @state.modalProps?.message

        if @state.modalProps?.providerCompanies
          ul
            className: 'siteProviderCompanies'
            map(@state.modalProps?.providerCompanies, (company) =>
              li null,
                company.name
            )
)

Settings = StoreWrapper(withModalContext(Settings), (() ->
  if UserStore.getUser() and FeatureStore.hasLoaded()
    user:
      isOnlyDispatcher: UserStore.isOnlyDispatcher()
      isPartner: UserStore.isPartner()
      isAdmin: UserStore.isAdmin()
      isRoot: UserStore.isRoot()
      isFleet: UserStore.isFleet()
      isFleetInHouse: UserStore.isFleetInHouse()
      roles:
        isDispatcher: UserStore.hasRole("dispatcher")
        isAdmin: UserStore.hasRole("admin")
    featuresEnabled:
      trailers: FeatureStore.isFeatureEnabled('Trailers')
      fleetSites: FeatureStore.isFeatureEnabled(Consts.FEATURES_FLEET_SITES)
  ), ({
    constructor: (props) ->
      @register(UserStore, UserStore.LOGIN)
      @register(FeatureStore, FeatureStore.CHANGE)
  })
)

module.exports = Settings
