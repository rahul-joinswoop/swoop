import React from 'react'
import BaseComponent from 'components/base_component'
import classNames from 'classnames'
import './style.scss'

const Attributes = ({ attributes = [] }) => (
  <div className="attributes-column">
    { attributes.map((attr) => {
      if (attr.value) {
        return (
          <div
            className={classNames('attribute', attr.className)}
            key={attr.header}
          >
            <span className="attribute-name">{attr.header}</span>
            <span className="attribute-value">{attr.value}</span>
          </div>
        )
      }
      return null
    })}
  </div>
)

class JobDetails extends BaseComponent {
  state = {
    showDetails: true,
  }

  toggleDetails = () => this.setState(prevState => ({
    showDetails: !prevState.showDetails,
  }))

  render() {
    return (
      <div className="component-job-details-dropdown">
        <div
          className="job-details-toggle"
          onClick={this.toggleDetails}
        >
          <i className={`fa ${this.state.showDetails ? 'fa-chevron-up' : 'fa-chevron-down'}`} />
          <h2>{`${this.state.showDetails ? 'Hide' : 'View'} Job Details`}</h2>
          <i className={`fa ${this.state.showDetails ? 'fa-chevron-up' : 'fa-chevron-down'}`} />
        </div>
        { this.props.message &&
          <div className="message">{this.props.message}</div>}
        { this.state.showDetails &&
          <div className="job-details">
            <Attributes attributes={this.props.attrLeft} />
            <Attributes attributes={this.props.attrRight} />
          </div>}
      </div>
    )
  }
}

export default JobDetails
