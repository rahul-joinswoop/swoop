import AuctionStore from 'stores/auction_store'
import Consts from 'consts'
import CustomerTypeStore from 'stores/customer_type_store'
import InvoiceStore from 'stores/invoice_store'
import JobAttributes from 'job_attributes'
import JobDetails from 'components/JobDetailsOverlay'
import JobStore from 'stores/job_store'
import LocationTypeStore from 'stores/location_type_store'
import StoreWrapper from 'StoreWrapper'
import UserStore from 'stores/user_store'
import Utils from 'utils'

const JobDetailsOverlay = StoreWrapper(JobDetails, function getProps(props) {
  const job = JobStore.getById(props.job_id)
  const attrLeft = JobAttributes.getJobAttributes(job, ['details', 'questions'], this.getId())

  if (Utils.isEnterpriseFleetManagementJob(UserStore, job)) {
    attrLeft.push({
      className: 'exceptions',
      header: 'Exceptions:',
      value: 'Do not change drop off address without Swoop Dispatch authorization: 847-796-6763.',
    })
  }

  const paymentTypeName = CustomerTypeStore.get(job.customer_type_id, 'name', this.getId()) || job.customer_type_name
  const attrRight = [
    {
      header: 'Location Type:',
      value: (() => {
        const locationTypeId = job?.service_location?.location_type_id
        return locationTypeId ?
          LocationTypeStore.getLocationTypeById(locationTypeId)?.name :
          job.location_type
      })(),
    },
    {
      header: 'Location: ',
      value: (() => {
        if (UserStore.isPartner() && job.status === Consts.AUTO_ASSIGNING) {
          const {
            street_name, city, state, zip,
          } = job?.service_location
          return [street_name, city, state, zip].filter(el => !!el).join(', ')
        } else {
          return Utils.renderAddress(job.service_location?.address) + (
            (!job.service_location?.lat || !job.service_location?.lng) ? '(no exact address, confirm with customer)' : ''
          )
        }
      })(),
    },
    {
      header: 'Drop Off: ',
      value: (() => {
        if (UserStore.isPartner() && job.status === Consts.AUTO_ASSIGNING && job?.drop_location) {
          const {
            street_name, city, state, zip,
          } = job?.drop_location
          return [street_name, city, state, zip].filter(el => !!el).join(', ')
        } else {
          return job.drop_location?.address ? Utils.renderAddress(job.drop_location?.address) : ''
        }
      })(),
    },
    {
      header: 'Pickup to Drop: ',
      value: job.distance ? `${job.distance} miles` : '',
    },
    {
      header: 'Service: ',
      value: job.service + (job.will_store ? ', Storage' : ''),
    },
    {
      header: 'Payment Type: ',
      value: Utils.getPaymentTypeSpan(CustomerTypeStore, InvoiceStore, job, this.getId()),
      show: () => !!CustomerTypeStore.getById(job.customer_type_id),
      className: paymentTypeName === Consts.CUSTOMERPAY ? 'payment customer' : '',
    },
    {
      header: 'Vehicle: ',
      value: Utils.getVehicleName(job),
    },
  ]

  return {
    attrLeft,
    attrRight,
    message: Utils.isPartnerOnSwoopJob(UserStore, job) && AuctionStore.jobHasLiveAuction(job) ? 'Exact location available if ETA accepted' : null,
  }
})

export default JobDetailsOverlay
