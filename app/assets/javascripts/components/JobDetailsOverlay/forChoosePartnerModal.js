import JobAttributes from 'job_attributes'
import JobDetails from 'components/JobDetailsOverlay'
import JobStore from 'stores/job_store'
import StoreWrapper from 'StoreWrapper'
import Utils from 'utils'

const JobDetailsOverlay = StoreWrapper(JobDetails, function getProps(props) {
  const job = JobStore.getById(props.job_id)
  const attrLeft = JobAttributes.getJobAttributes(job, ['details', 'questions'], this.getId())
  const attrRight = JobAttributes.getJobAttributes(job, ['service_location', 'drop_location', 'pickup_to_dropoff', 'symptom', 'service', 'vehicle', 'tire_size', 'payment_type', 'pickup_contact_name', 'pickup_contact_phone'], this.getId())

  if (job.customer && job.customer.phone) {
    attrRight.unshift({
      header: 'Customer Phone:',
      value: Utils.renderPhoneNumber(job.customer.phone),
    })
  }

  if (job.customer && job.customer.full_name) {
    attrRight.unshift({
      header: 'Customer Name:',
      value: job.customer.full_name,
    })
  }

  return {
    attrLeft,
    attrRight,
  }
})

export default JobDetailsOverlay
