import React, { PureComponent } from 'react'
import Button from 'componentLibrary/Button'
import PropTypes from 'prop-types'
import Utils from 'utils'

class AssignDriverButton extends PureComponent {
  static propTypes = {
    record: PropTypes.object,
  }

  handleOnClick = (e) => {
    e.preventDefault()
    Utils.showModal('assign', e, this.props)
  }

  render() {
    if (this.props.record) {
      return (
        <Button
          color="primary-o"
          onClick={this.handleOnClick}
          size="narrow"
        >
          Assign Driver
        </Button>
      )
    }
    return null
  }
}

export default AssignDriverButton
