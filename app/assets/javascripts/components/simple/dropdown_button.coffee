import React from 'react'
UserStore = require ('stores/user_store')
UsersStore = require('stores/users_store').default
BaseComponent = require('components/base_component')
onClickOutside = require('react-onclickoutside').default
import ReactDOMFactories from 'react-dom-factories'

class DropDownButton extends BaseComponent
  displayName: 'DropDownButton'
  constructor: ->
    super(arguments...)
    @state = {
      open: false
    }

  UNSAFE_componentWillReceiveProps: (props) ->
    super(arguments...)
    @setState
      rescue_driver_id: props.value
      record: props.record

  componentDidMount: ->
    super(arguments...)

    @setState
      rescue_driver_id: @props.value
      record: @props.record


  handleClickOutside: (e) =>
    if $(e.target).closest(".dropdown_button")[0]!=@refs["container"]
      @setState
        open: false

  render: ->
    super(arguments...)

    div
      className: 'dropdown_button'
      ref: "container"
      style:
        display: "inline-block"
        verticalAlign: "middle"
        position: "relative"
        borderRadius: 3
        backgroundColor: "#81BDE0"
        width: if @props.width? then @props.width else "100%"
        color: "white"
        whiteSpace: "nowrap"
      div
        className: "button"
        style:
          width: "calc(100% - 35px)"
          display: "inline-block"
          padding: "0 10px"
          textAlign: "center"
          cursor: "pointer"
          overflow: "hidden"
          textOverflow: "ellipsis"
          verticalAlign: "middle"
        onClick: @props.buttonClick
        if @state.rescue_driver_id? then UsersStore.get(@state.rescue_driver_id, "full_name", @getId()) else "Assign Driver"
      div
        style:
          display: "inline-block"
          borderLeft: "1px solid white"
          borderRadius: "0 3px 3px 0"
          padding: "4px 5px 6px"
          cursor: "pointer"
        onClick: =>
          @setState
            open: !@state.open
        ReactDOMFactories.i
          className: "fa " + (if !@state.open then "fa-chevron-down" else "fa-chevron-up")
          style:
            color: "white"
            margin: "0 3px"
            fontSize: 18
      if @state.open
        div
          style:
            display: "block"
            position: "absolute"
            top: 30
            zIndex: 999
            backgroundColor: "white"
            borderRadius: "0 0 4px 4px"
            border: "1px solid #E2E2E2"
            boxShadow: "0px 2px 4px 0px rgba(0,0,0,0.23)"
            whiteSpace: "nowrap"
            left: 0
            right: 0
          span
            style:
              textTransform: "uppercase"
              fontSize: 11
              color: "#777777"
            @props.filterText
          ul
            style:
              margin: 0
              padding: 0
              color: "black"
            li
              style:
                margin: 0
                padding: 5
                cursor: "pointer"

              onClick: (e) =>
                @props.onChange(null)
                @setState
                  open: false
              "- Select -"
            for id in UsersStore.getSortedDriverIds(@getId())
              li
                style:
                  margin: 0
                  borderTop: "1px solid #CCCCCC"
                  padding: 5
                  cursor: "pointer"
                  overflow: "hidden"
                  textOverflow: "ellipsis"

                onClick: ((id) => (e) =>
                  Tracking.trackJobEvent('Dispatch to Driver', @props?.record?.id, {
                    category: 'Dispatch'
                  })
                  @props.onChange(id)
                  @setState
                    open: false
                )(id)
                UsersStore.get(id, "full_name", @getId())


module.exports = onClickOutside(DropDownButton)
