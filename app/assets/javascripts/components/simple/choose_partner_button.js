import React from 'react'
import BaseComponent from 'components/base_component'
import Button from 'componentLibrary/Button'
import CustomerTypeStore from 'stores/customer_type_store'
import UserStore from 'stores/user_store'
import Utils from 'utils'

class ChoosePartnerButton extends BaseComponent {
  handleOnClick = (e) => {
    if (this.shouldShowSetCustomerType()) {
      Utils.showModal('set_customer_type', e, {
        callback: this.showPartnerModal,
        record: this.props.record,
      })
      return
    }
    this.showPartnerModal(e)
  }

  shouldShowSetCustomerType = () => {
    const name = CustomerTypeStore.get(this.props.record.customer_type_id, 'name', this.getId())
    return UserStore.isTesla() && name === 'TBD'
  }

  showPartnerModal = (e) => {
    Utils.showModal('partner', e, {
      record: this.props.record,
    })
  }

  render() {
    return (
      <Button
        color="green-o"
        onClick={this.handleOnClick}
        size="narrow"
      >
        Assign Partner
      </Button>
    )
  }
}

export default ChoosePartnerButton
