import React from 'react'
import Utils from 'utils'
require('stylesheets/components/download_button_async.scss')

AsyncRequestStore = require('stores/async_request_store').default
BaseComponent = require('components/base_component')

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

class DownloadButtonAsync extends BaseComponent
  download: =>
    @setState
      loading: true
    AsyncRequestStore.genericRequest(@props.apiCall, {
      success: (data) =>
        if data.report_result?.s3_filename?
          Utils.downloadFile(data.report_result.s3_filename)
        @setState
          loading: false
        @props.onComplete?()
      error: (error) =>
        @setState
          loading: false
        Utils.sendNotification("Download Failed", Consts.ERROR)
    })

  render: ->
    div
      className: "download_button_async"
      if @state.loading
        Loading
          message: 'Preparing report ...'
          size: 24
      else
        Button
          color: "secondary"
          onClick: @download
          style: {
            float: 'left'
          }
          @props.text

module.exports = DownloadButtonAsync
