import React from 'react'
import ReactDOM from 'react-dom'
BaseComponent = require('components/base_component')
import FontAwesomeButtonImport from 'componentLibrary/FontAwesomeButton'
FontAwesomeButton = React.createFactory(FontAwesomeButtonImport)
onClickOutside = require('react-onclickoutside').default
import { partial } from 'lodash'
require('stylesheets/components/multiselect/multiselect.scss')

class MultiSelect extends BaseComponent
  displayName: 'MultiSelect'
  constructor: (props) ->
    super(props)
    @state = {isDropDownOpen: false}

  handleClickOutside: () =>
    @setState
      isDropDownOpen: false

  @defaultProps = {
    emptyIsAll: false
    alwaysShowSelections: false
    showAll: true
  }

  componentDidUpdate: ->
    super(arguments...)
    @updateShowingItems()

  toggleDropDown: () =>
    @setState
      isDropDownOpen: !@state.isDropDownOpen

  onChange: (value, checked) =>
    values = @props.values

    if (value == 'All' and @props.showAll )
      #TODO: if emptyIsAll is false we need to add all to array
      if checked
        values = []
      else
        values = undefined
    else
      if values
        if !values.length and @props.emptyIsAll
          if checked
            if value not in values
              values.push value
          else
            @props.suggestions.forEach (suggestion) ->
              if suggestion.value != value
                values.push suggestion.value
        else
          if checked
            if value not in values
              values.push value
          else
            if value in values
              index = values.indexOf value
              values.splice index, 1

            if !values.length
              values = undefined
      else
        values = []
        if checked
          values.push value

    @props.onChange values

  renderSelectedValue: (value) =>
    filterBy = @props.suggestions.filter (suggestion) -> suggestion.value == value

    if filterBy.length
      return li
        key: filterBy[0].text
        filterBy[0].text

  renderSelectedValues : =>
    values = @props.values

    if values
      if values.length
        return div
          className: 'selected_values_container'
          ul null,
            values.map (value) => @renderSelectedValue(value)
      else if values.length == 0 and @props.showAll and @props.emptyIsAll
        return 'All'

    @props.placeholder

  isSuggestionChecked: (suggestion) =>
    values = @props.values
    if values
      if !@props.values.length and @props.emptyIsAll
        return true

      return suggestion.value in values
    return false
  renderDropDownItem: (suggestion) =>
    li null,
      label null,
        if !@isSuggestionDisabled(suggestion)
          input
            type: 'checkbox'
            checked: @isSuggestionChecked(suggestion)
            onChange: (e) => @onChange(suggestion.value, e.target.checked)
        suggestion.text

  isSuggestionDisabled: (suggestion) =>
    suggestion.disabled == true
  renderSelections: =>
    ul
      className: 'list-group'
      style:
        marginTop: 10
      @props.suggestions.map (suggestion) =>
        if @isSuggestionChecked(suggestion)
          li
            className: 'list-group-item clearfix'
            suggestion.text
            if !@isSuggestionDisabled(suggestion)
              FontAwesomeButton
                color: "alert"
                icon: "fa-times-circle"
                onClick: partial(@onChange, suggestion.value, false)
                style: {
                  float: "right"
                }
  renderDropDown: =>
    div
      className: 'dropdown-container'
      ul null,
        if @props.showDropdownTitle
          li
            className: 'title'
            @props.dropdownTitle || 'SELECT'
        if @props.isLoading
          li null,
            'loading more...'
        if @props.showAll
          @renderDropDownItem(
            text: 'All'
            value: 'All'
          )
        @props.suggestions.map (suggestion) => @renderDropDownItem(suggestion)

  updateShowingItems: =>
    if @props.values and @props.values.length
      elm = ReactDOM.findDOMNode @
      elm = $ elm
      ul =  elm.find('.selected_values_container').find 'ul'
      li = ul.find 'li'

      [0...li.length].forEach (i) ->
        $(li[i]).css
          display: 'inline-block'

      for i, index in li
        i = $ i
        if ul.offset().left + ul.width() - ( if index + 2 <= @props.values.length then 65 else 0) < i.offset().left + i.width()
          break

      if index < @props.values.length
        [index...li.length].forEach (i) ->
          $(li[i]).css
            display: 'none'

        more = ul.find('li.more')
        if (more.length == 0)
          more = $('<li class="more">')
        else
          more.css
            display: 'inline-block'
        moreCount = @props.values.length - index
        moreText = if index == 0 then moreCount + ' Selected' else '& ' + moreCount + ' More'
        ul.append more.html(moreText)
      else
        ul.find('li.more').remove()

  render: ->
    super(arguments...)
    div null,
      div
        className: 'multi_select_control'
        div
          className: 'form-control'
          style: @props.style
          onClick: @toggleDropDown
          @renderSelectedValues()
        if @state.isDropDownOpen
          @renderDropDown()
      if @props.alwaysShowSelections
        @renderSelections()

module.exports = onClickOutside(MultiSelect)
