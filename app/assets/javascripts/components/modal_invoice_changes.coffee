import createReactClass from 'create-react-class'
import React from 'react'
TransitionModal = React.createFactory(require 'components/modals')
import Utils from 'utils'
InvoiceStore = require('stores/invoice_store')
FTable = React.createFactory(require 'components/resizableTable/FTable')
import { extend, filter, map } from 'lodash'
import { getCurrencyClass } from 'lib/localize'
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)

AccountModal = createReactClass(
  displayName: 'AccountModal'

  onConfirm: ->
    if @state.email != null and @state.email.length > 0 and Utils.isValidEmail(@state.email)
      @props.onConfirm
        id: @props.account.id
        accounting_email: @state.email
    else
      @setState
        error: "Invalid Email"

  getInitialState: ->
    email: null
    error: null


  handleChange: (e) ->
    @setState
      email: e.target.value
      error: null
  render: ->
    if @props.show
      record_id = @props.record
      if record_id?
        record = InvoiceStore.getInvoiceById(record_id)

    TransitionModal $.extend({}, @props,
      callbackKey: 'invoiceChange'
      transitionName: 'InvoiceChangeModalTransition'
      confirm: 'OK'
      cancel: null
      onCnacel: null
      title: "Invoice total"
      extraClasses: "invoices_change_modal"
      onConfirm: @props.onCancel),
      div
        className: "change_container"
        if record?

          #TOOD: remove tax
          tax = 0
          additions = 0
          subtotal = 0
          updatedPrice = false
          updatedQty = false
          items = filter(record.line_items, (item) ->
            if item.deleted_at?
              return false
            if item.calc_type == "percent"
              return false
            if item.quantity != item.original_quantity
              updatedQty = true
            if item.original_unit_price != item.unit_price
              updatedPrice = true

            if item.description == "Tax"
              if not isNaN(parseFloat(item.tax_amount))
                tax += parseFloat(item.tax_amount)
              return false
            else
              if not isNaN(parseFloat(item.net_amount))
                subtotal += parseFloat(item.net_amount)
              return true
          )
          col_width = 85
          cols = [
            {header: "Item", key: "description"}
            {header: "Calculated Qty.", key: "original_quantity", width: col_width, fixedWidth: true, className: "textright", func: (item, props) ->
              span
                className: if item.original_quantity != item.quantity then "gray" else ""
                if item.original_quantity
                  item.original_quantity
                else
                  "-"
            }
          ]


          cols.push(
            {header: "Updated Qty.", width: col_width, fixedWidth: true, key: "quantity", className: "textright",  func: (item, props) ->
              if item.original_quantity != item.quantity
                item.quantity
              else
                "-"
            }
          )
          cols.push(
            {header: "Price", width: col_width, fixedWidth: true, key: "original_unit_price", className: "textright", func: (item, props) ->
              span null,
                # className: if item.original_unit_price != item.unit_price then "gray" else ""
                if item.unit_price?
                  Utils.formatNumber item.unit_price
                else
                  Utils.formatNumber item.original_unit_price

            }
          )
          # if updatedPrice
          #   cols.push(
          #     {header: "Updated Price", width: 80, className: "textright", key: "unit_price", func: (item, props) ->
          #       if item.original_unit_price != item.unit_price
          #         item.unit_price
          #       else
          #         "-"
          #     }
          #   )
          cols.push(
            {header: "Total",  className:"money textright" + getCurrencyClass(record.currency), fixedWidth: true,  width: col_width, key: "net_amount", func: (item) ->
              Utils.formatNumber item.net_amount
            }
          )

          div null,
            FTable extend({}, @props,
              rows: items
              cols: cols
              maxWidth: 538
              sortKey: "id"
              switchWidth: 50
              sortDir: 1
              )
            div
              className: "totals"
              div null,
                h4 null,
                  "Subtotal:"
                span
                  className: "money" + getCurrencyClass(record.currency)
                  Utils.formatNumber(Utils.toFixed(subtotal,2))
              map(record.line_items, (line_item) ->
                if line_item.calc_type != "percent"
                  return false
                div null,
                  h4 null,
                    line_item.description + " @ "+line_item.quantity+"%:"
                  span
                    className: "money" + getCurrencyClass(record.currency)
                    if !isNaN(parseFloat(line_item.net_amount))
                      additions += parseFloat(line_item.net_amount)
                      Utils.formatNumber(Utils.toFixed(parseFloat(line_item.net_amount),2))
              )
              div null,
                h4 null,
                  "Tax:"
                span
                  className: "money" + getCurrencyClass(record.currency)
                  Utils.formatNumber(Utils.toFixed(tax,2))
              div null,
                h4 null,
                  "Total:"
                span
                  className: "money" + getCurrencyClass(record.currency)
                  Utils.formatNumber(Utils.toFixed((subtotal + tax + additions),2))
        else
          Loading null
)

module.exports = AccountModal
