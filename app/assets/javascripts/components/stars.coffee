import createReactClass from 'create-react-class'
import React from 'react'
import ReactDOMFactories from 'react-dom-factories'

Stars = createReactClass(
  render: ->
    starNum = 5 - @props.stars
    div
      className: 'reviewed'
      [5..1].map (i) ->
        ReactDOMFactories.i
          className: if starNum < i then 'fa fa-star' else 'fa fa-star fa-star-o'
)

module.exports = Stars
