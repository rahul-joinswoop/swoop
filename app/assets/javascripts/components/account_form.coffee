import Utils from 'utils'
import createReactClass from 'create-react-class'
import React from 'react'
import ReactDOMFactories from 'react-dom-factories'

require('stylesheets/components/account_form.scss')
EditForm = React.createFactory(require('components/edit_form'))
AccountStore = require('stores/account_store')
CompanyStore = require('stores/company_store').default
FeatureStore = require('stores/feature_store')
SiteStore = require('stores/site_store').default
import Consts from 'consts'
DepartmentStore = require('stores/department_store')
AgeroWarningSection = React.createFactory(require('components/settings/integrations/rsc/agero_warning_section').default)
AgeroLocationsVendorList = React.createFactory(require('components/settings/integrations/rsc/agero_locations_vendor_list').default)
AgeroLogo = require('public/images/Agero_Logo_Vector.svg')
require('stylesheets/components/account_form.scss')
import StoreWrapper from 'StoreWrapper'
#TODO: if a change comes in, show an error that something has changed and to refresh

import { clone, defer, difference, extend, filter, find, flatten, forEach, includes, intersection, isEmpty, isEqual, map, partial, pull, reject, uniqBy, values, without } from 'lodash'

import ColorSelector from 'componentLibrary/ColorSelector'

ToggleButton = React.createFactory(require('react-toggle-button'))
BaseComponent = require('components/base_component')

EmailInput = React.createFactory(createReactClass(
  displayName: 'EmailInput'
  render: ->
    div
      className: "form_group"
      style:
        marginTop: 8
      label
        style:
          display: "inline-block"
          width: "7%"
        @props.label
      input
        name: @props.name
        disabled: if @props.enabled? then !@props.enabled
        style:
          width: "calc(93%)"
          display: "inline-block"
          border: if @props.errors?.billing_emails? and !Utils.areValidEmails(@props.value) then "1px solid red"
        value: @props.value || ''
        className: "form-control"
        onChange: (e) =>
          val = e.target.value

          @props.setrecordData(e.target.name, e.target.value)
))

class AccountForm extends BaseComponent
  displayName: 'AccountForm'

  isDisabledForISSCStatus: (isAgeroRSCAccount, account_id, site_id) ->
    result = {
      disabledWhileRegistered: false,
      disabledForDeregistering: false
    }
    if isAgeroRSCAccount
      return result
    current_account = AccountStore.getById(account_id)
    if current_account?.dispatchable_sites?
      for site in current_account?.dispatchable_sites
        if site.site_id == site_id
          if site.isscs
            for issc in site.isscs
              if Consts.ISSC_REGISTERED_STATUSES.includes(issc.status?.toLowerCase())
                result.disabledWhileRegistered = true
              if issc.delete_pending_at?
                result.disabledForDeregistering = true
    return result

  isVendorIDDisabledHelper: (id, index, site) ->
    if index >= site.isscs.length
      return false
    for connection in site.isscs
      if id = connection.contractorid
        return true
    return false

  isVendorIDDisabled: (disabledForDeregistering, disabledWhileRegistered, isDisabledWhileRegisteredAndDeletedVendorID, id, index, site) ->
    if disabledForDeregistering || isDisabledWhileRegisteredAndDeletedVendorID
      return true
    if disabledWhileRegistered
      return this.isVendorIDDisabledHelper(id, index, site)
    return false

  isDisabledWhileRegisteredAndDeletedVendorID: (disabledWhileRegistered, site) ->
    if !disabledWhileRegistered || isEmpty(site.isscs)
      return false
    for connection in site.isscs
      if !site.contractorids.includes(connection.contractorid)
        return true

  constructor: (props) ->
    super(props)

    componentId = @getId()

    @state = {
      selectedAgeroLocations: []
    }

    @data = [
      {
        name: "type"
        label: "Type"
        type: "selector"
        onChange: (e) ->
          @setrecordData("name", "")
          @setrecordData("type", e.target.value)

        enabled: (record) ->
          return !record.id?
        isValid: (account) ->
          checkAccount = =>
            if AccountStore.doesAccountNameExist(@record.name, componentId)
              return "Account already exists"
            if @record.name in Consts.FLEETS
              return "Contact Swoop to add this account"

          if !@record.id
            return checkAccount()
          else
            accountName = AccountStore.get(@record.id, 'name') # this is a callback function, no need to pass componentId
            if accountName != @record.name
              return checkAccount()

        getValue: (account) ->
          if account.name == null
            delete account.dispatchable_sites
          if account.type == "Motor Club" || (account.id && account.name in Consts.MOTORCLUBS)
            return "Motor Club"
          else if account.company?.name == "Tesla" || account.company?.name == "Swoop"
            return "Fleet"
          else
            return "Account"
        options: ((account) =>
          motorClubAccounts = reject AccountStore.getMotorClubAccounts(@getId(), 'name'), (account) -> account?.deleted_at
          if motorClubAccounts.length < Consts.MOTORCLUBS.length || account.name in Consts.MOTORCLUBS
            return ["Account", "Motor Club"]
          else
            ["Account"]
        )
      }
      {
        name: "name"
        label: "Name*"
        type: "text"
        classes: "right"
        updateErrorsImmediately: true
        enabled: (record) -> return !record.company?.name? and AccountStore.getAccountKey(record?.id, "name", componentId) not in Consts.MOTORCLUBS and record.name != Consts.CASHCALL
        show: (record) -> record.type != "Motor Club"
        isValid: (record) ->
          input = @record
          if !input
            input = record
          if input.id? && (input.name in Consts.MOTORCLUBS || input.name == Consts.TESLA)
            return
          normalizedInput = input?.name?.trim().toLowerCase()
          enabledMotorClubs = difference(Consts.MOTORCLUBS, Consts.DISABLED_MOTORCLUBS)
          if enabledMotorClubs.map((name) => name.toLowerCase()).includes(normalizedInput)
            return "Please select Type 'Motor Club'"
          if normalizedInput == 'tesla'
            return "Cannot create 'Tesla' account"
        required: true
      }
      {
        name: "name"
        label: "Name*"
        type: "selector"
        classes: "right"
        enabled: (record) -> return !record.company?.name? and AccountStore.getAccountKey(record?.id, "name", componentId) not in Consts.MOTORCLUBS
        show: (record) -> record.type == "Motor Club"
        options: (() =>
          options = [{text: "- Select -", value: ''}]
          accounts = reject AccountStore.getMotorClubAccounts(@getId(), 'name'), (account) -> account?.deleted_at
          for name in difference(Consts.MOTORCLUBS, map(accounts, (account) -> account?.name))
            options.push({text: name, value: name})
          return options
        )
        required: true
      }
      {
        name: "location"
        label: "Billing Address"
        type: "address"
        send_all_addresses: true
        hide_map: true
        show: (record) -> !record.company?.location?.address?
        getValue: (record) -> record.location?.address
        enabled: (record) -> return !record.company?.location?.address?
      }
      {
        name: "company.location"
        label: "Billing Address"
        type: "address"
        send_all_addresses: true
        hide_map: true
        show: (record) -> record.company?.location?.address?
        getValue: (record) ->  record.location?.address
        enabled: (record) -> return !record.company?.location?.address?
      }
      {
        name: "physical_location"
        label: "Physical Address"
        type: "address"
        send_all_addresses: true
        hide_map: true
        classes: "right"
      }
      {
        name: "phone"
        label: "Phone"
        type: "phone"
        enabled: (record) -> return !record.company?.phone?
      }
      {
        name: "fax"
        label: "Fax"
        type: "phone"
        classes: "right"
        enabled: (record) -> return !record.company?.fax?
      }
      {
        name: "primary_email"
        label: "Email"
        enabled: (record) -> return !record.company?.primary_contact?.email?
        type: "email"
      }
      {
        name: "color"
        label: "Priority"
        type: "custom"
        classes: "right"
        getView: (record) ->
          <ColorSelector
            color={record.color}
            setColor={(color) => @setrecordData("color", color)}
          />
      }

      {
        name: "contact_name"
        label: "Contact Name"
        type: "capitalized"
      }
      {
        name: "contact_phone"
        label: "Contact Number"
        classes: "right"
        type: "phone"
      }
      {
        name: "contact_email"
        label: "Contact Email"
        type: "email"
      }
      {
        name: "po_required"
        label: -> "Require PO"
        type: "selector"
        show: -> FeatureStore.isFeatureEnabled("Job PO Number")
        onChange: (e) ->
          @setrecordData('po_required_at_job_creation', false, true)
          @setrecordData('po_required_before_sending_invoice', false, true)
          @setrecordData(e.target.value, true)
        getValue: (record) ->
          if record.po_required_before_sending_invoice
            return "po_required_before_sending_invoice"
          else if record.po_required_at_job_creation
            return "po_required_at_job_creation"

        options: (() ->
          [{text: "- Select -", value: ''},{text: "at job creation", value: 'po_required_at_job_creation'},{text: "before sending invoice", value: 'po_required_before_sending_invoice'}]
        )
        classes: "right"
      }
      {
        name: "billing_emails"
        type: "custom"
        classes: "col"
        isValid: () ->
          if !Utils.areNonRequiredEmailsValid(@record.accounting_email) || !Utils.areNonRequiredEmailsValid(@record.accounting_email_cc) || !Utils.areNonRequiredEmailsValid(@record.accounting_email_bcc)
            return "Invalid email address"
        getView: (record, errors) ->
          fleetAccount = record.company? or record?.name in Consts.MOTORCLUBS
          div
            style:
              backgroundColor: "#F2F2F2"
              border: "#EEEEEE"
              padding: 15
              marginTop: 15
              marginBottom: 15
              borderRadius: 3
            div null,
              label
                style:
                  display: "inline"
                  marginRight: 10
                "Billing Email"
              span null,
                "(add multiple with a comma or semicolon)"
            EmailInput
              name: "accounting_email"
              value: record.accounting_email
              label: "To:"
              setrecordData: @setrecordData
              errors: errors
            EmailInput
              name: "accounting_email_cc"
              value: record.accounting_email_cc
              label: "Cc:"
              setrecordData: @setrecordData
              errors: errors
            EmailInput
              name: "accounting_email_bcc"
              value: record.accounting_email_bcc
              label: "Bcc:"
              setrecordData: @setrecordData
              errors: errors
      }
      {
        name: "disable_qb_import"
        type: "checkbox"
        classes: "col disable_qb"
        label: "Do not import jobs from this account to QuickBooks"
        show: (account) ->
          FeatureStore.isFeatureEnabled(Consts.FEATURE_QUICKBOOK)
      }
      {
        name: "hide_timestamps_on_invoice"
        type: "checkbox"
        classes: "col disable_qb"
        label: "Do not include timestamps on invoices for this account"
        show: (account) ->
          FeatureStore.isFeatureEnabled(Consts.FEATURE_HIDE_TIMESTAMPS_FOR_ACCOUNT) || account.hide_timestamps_on_invoice
      }
      {
        name: "department_id"
        label: -> if UserStore.isSouthern() then "Division" else "Department"
        type: "selector"
        show: (record) -> FeatureStore.isFeatureEnabled("Departments")
        options: (() ->
          options = [{text: "- Select -", value: ''}]
          for id, dept of DepartmentStore.getDepartments(componentId)
            options.push({text: dept.name, value: dept.id})
          return options
        )
        classes: "right"
      }

      {
        name: "notes"
        label: "Notes"
        type: "textarea"
        classes: "col"
      }

      {
        name: "motorClub_notice"
        type: "custom"
        classes: "col"
        show: (account) ->
          account.company?.name in Consts.DISABLED_MOTORCLUBS || account.name in Consts.DISABLED_MOTORCLUBS
        getView: (account) ->
          div
            style:
              color: "red"
              marginTop: 15
              textAlign: "center"
            "Swoop does not currently have a digital integration with " + account.name + "."
      }

      {
        name: "auto_accept"
        type: "custom"
        classes: "col"
        containerStyle:
          border: "1px solid #E2E2E2"
          backgroundColor: "#F2F2F2"
          borderRadius: "3px"
          marginTop: 15
          padding: "10px"

        isValid: (record) ->
          if @record.auto_accept and isNaN(parseInt(@record.auto_eta))
            return "Must set an automatic eta"
        show: (account) ->
          enabledMotorClubs = difference(Consts.MOTORCLUBS, Consts.DISABLED_MOTORCLUBS)
          account.company?.name in enabledMotorClubs || account.name in enabledMotorClubs
        getView: (record) ->
          div null,
            input
              type: "checkbox"
              style:
                marginRight: "10px"
                verticalAlign: "middle"
              onChange: (e) =>
                @setrecordData("auto_accept", e.target.checked)
              checked: record.auto_accept == true

            span
              style:
                verticalAlign: "sub"
                paddingTop: "5px"
              "Automatically accept job before job expires with an ETA of:"
            input
              value: record.auto_eta
              onChange: (e) =>
                @setrecordData("auto_eta", e.target.value)
              type: "input"
              className: "form-control"
              placeholder: "minutes"
              style:
                display: "inline-block"
                width: "150px"
                marginLeft: "10px"
                verticalAlign: "middle"
      }
    ]

  renderAgeroWarningsSection: () =>
    AgeroWarningSection
      selectedLocationIds: map(@refs.account_form?.record?.dispatchable_sites, "locationid")
      allAgeroLocations: @props.allAgeroLocations

  getAgeroIntegration: (endpoint) =>
    return [
      {
        fields:[
          classes: 'col'
          type: 'custom'
          getView: => @renderAgeroWarningsSection()
          ]
      }
    ]

  getSitesFields: (endpoint, isAgeroRSCAccount) =>
    allContractorids = {}
    that = @

    dispatchSites = SiteStore.getDispatchSites(@getId(), [
      'name',
      'owner_company_id',
      'company_id',
      'dispatchable',
      'deleted_at'
      ])

    map values(dispatchSites), (site, index) ->
      siteId = site.id

      {
        label: ""
        fields: [
          {
            name: "site_info."+index
            type: "custom"
            classes: "col"
            alwaysBeforeSend: (record, newRecord, col) ->
              dispatchableSites = newRecord?.dispatchable_sites || []
              for site_info in dispatchableSites
                site_info?.contractorids = without(site_info.contractorids, "")
            isValid: ->
              companyId = null
              siteOwnerCompanyId = site.owner_company_id
              siteCompanyId = site.company_id

              if siteOwnerCompanyId?
                companyId = siteOwnerCompanyId
              else if siteCompanyId?
                companyId = siteCompanyId

              if @record.dispatchable_sites?
                oneEnabled = false
                for dsite, index in @record.dispatchable_sites
                  oneEnabled ||= (dsite.enabled and site.dispatchable and !site.deleted_at)
                  if dsite.site_id == site.id
                    dsite_index = index
                    site_info = dsite

                #Throw error on swoop or inhouse fleets

                if !oneEnabled and (@record?.name == Consts.SWOOP || (@record?.company?.in_house && CompanyStore.get(@record.company.id, 'in_house', that.getId())))
                  return Consts.MESSAGE_NEED_SITE_ENABLED


                if ((CompanyStore.get(companyId, 'tax_id', that.getId())? || @record.name == Consts.AGERO) && site_info?.enabled && @record?.name in Consts.MOTORCLUBS)
                  contractorids = site_info.contractorids
                  if includes(contractorids, "")
                    return "Vendor ID Required"

                  if contractorids.length > 0 and endpoint.showLocationId and (!site_info.locationid? || site_info.locationid.length == 0)
                    return if !isAgeroRSCAccount then "Location Id Required" else "Agero Location Required"

                  for cid in contractorids
                    if @record?.name != Consts.AGERO && @record?.name != Consts.GEICO && allContractorids[cid] == true
                      return "Vendor ID must be unique across all enabled sites"

                    if @record?.name == Consts.TESLA_MOTORS
                      if not cid.match /^[0-9A-Za-z\-]+$/
                        return "Vendor ID's must be letters, numbers, and -'s only"
                    else if not cid.match /^[0-9A-Za-z]+$/
                      return "Vendor ID's must be letters and numbers only"

                    allContractorids[cid] = true
                    switch @record?.name
                      when Consts.GEICO, Consts.ALLSTATE, Consts.ALLIED
                        if not (includes(Consts.STATE_ABREVS, cid.slice(0,2)) and cid.match /^[A-Z]{2}[0-9]+$/)
                          return "Vendor IDs for #{@record?.name} must begin with two letter state code in all caps"
                      when Consts.AGERO, Consts.NATIONSAFE, Consts.QUEST
                        if not cid.match /^[0-9]+$/
                          return "Vendor IDs must be all digits for #{@record?.name}"
                      when Consts.ROADAMERICA
                        if not cid.match /^M?[0-9]+$/
                          return "Vendor IDs must either be all digits, or begin with an M followed by all digits for #{@record?.name}"

                  if endpoint.showLocationId and @record?.name == Consts.ROADAMERICA and !(site_info.locationid? && site_info.locationid.length == 3 && /^[0-9]+$/.test(site_info.locationid))
                    return Consts.ROADAMERICA + " Location Id's must be three digits"
                  if endpoint.showLocationId and @record?.name == Consts.AGERO and !(site_info.locationid? && /^([1-9][0-9]*)$/.test(site_info.locationid))
                    return if !isAgeroRSCAccount then "Location Id Required" else "Agero Location Required"

                  if contractorids.length > 0 and endpoint.showLogin and (!site_info.password? || !site_info.username? || site_info.username.length == 0 || site_info.password.length == 0)
                    return "Login Info Required"

                  if site_info.incorrect == true
                    return "Username / Password Incorrect"

            show: (account) ->
              enabledMotorClubs = difference(Consts.MOTORCLUBS, Consts.DISABLED_MOTORCLUBS)
              account.company?.name in enabledMotorClubs || account.name in enabledMotorClubs

            getView: (account) ->
              find_site_in_record = (site_id) =>
                if @orig_record.dispatchable_sites
                  for dsite in @orig_record.dispatchable_sites
                    if dsite.site_id==site_id
                      return dsite

              site_info = null
              dsite_index = -1
              if account.dispatchable_sites?
                for dsite, index in account.dispatchable_sites
                  if dsite.site_id == siteId
                    dsite_index = index
                    site_info = dsite
              else
                @record.dispatchable_sites = []

              companyId = null
              siteOwnerCompanyId = site.owner_company_id
              siteCompanyId = site.company_id

              if siteOwnerCompanyId?
                companyId = siteOwnerCompanyId
              else if siteCompanyId
                companyId = siteCompanyId

              requireTaxId = !CompanyStore.get(companyId, 'tax_id')? && !(account.name == Consts.AGERO)

              if not site_info? and @record.type != 'Account'
                site_info = {
                  site_id: siteId
                  enabled: (account.name in Consts.MOTORCLUBS || account.company?.name in Consts.MOTORCLUBS) and !requireTaxId,
                  contractorids: [""]
                }

                @record.dispatchable_sites.push(site_info)
                dsite_index = @record.dispatchable_sites.length-1

              if AccountStore.isAgero(account) and !site_info?.locationid and SiteStore.getDispatchSites(that.getId()).length == 1
                site_info.locationid = AccountStore.defaultLocationIdForAgero()

              enabled = (site_info?.enabled) || false

              if isAgeroRSCAccount && that.props.vendorConnections?.length == 0
                enabled = false
              else if account.name in Consts.MOTORCLUBS  || account.company?.name in Consts.MOTORCLUBS
                enabled = (!requireTaxId && site_info?.enabled) || false

              vendorIds = null
              if isAgeroRSCAccount && !isEmpty(that.props.vendorConnections)
                vendorIds = map(that.props.vendorConnections, (connection) -> connection.vendorId)

              if site_info.locationid? && !that.state.selectedAgeroLocations.includes(site_info.locationid)
                selectedAgeroLocations = clone(that.state.selectedAgeroLocations)
                selectedAgeroLocations.push(site_info.locationid)
                that.setState({selectedAgeroLocations: selectedAgeroLocations})

              div
                key: 'site_index_' + dsite_index
                style:
                  border: "1px solid #E2E2E2"
                  backgroundColor: "#F2F2F2"
                  borderRadius: "3px"
                div
                  style:
                    borderBottom: "1px solid #E2E2E2"
                    padding: "10px"
                    height: "40px"
                    whiteSpace: "no-wrap"
                    display: "flex"
                    flexDirection: "row"
                    flexWrap: "nowrap"
                    alignItems: "stretch"
                    alignContent: "stretch"

                  span
                    style:
                      fontWeight: "bold"
                      flex: "none"
                    Utils.getSiteName(CompanyStore, SiteStore, siteId, that.getId())
                  div
                    style:
                      textAlign: "right"
                      flex: "1 1 auto"
                      whiteSpace: "nowrap"
                      textOverflow: "ellipsis"
                      overflow: "hidden"
                    span
                      style:
                        textOverflow: "ellipsis"
                      endpoint.name + " can dispatch jobs to this site"
                  div
                    style:
                      display: "inline-block"
                      marginLeft: 10
                      float: "right"
                      flex: "none"
                    ToggleButton
                      colors:
                        inactive:
                          base: 'rgb(170,170,170)',
                          hover: 'rgb(170,170,170)',
                        inactiveThumb:
                          base: if requireTaxId then '#C5C5C5'
                      value: enabled
                      onToggle: (partial((dsite_index, checked) =>
                        @setrecordData("dispatchable_sites."+dsite_index+".enabled", !checked)
                      , dsite_index))

                if !enabled && (account.name in Consts.MOTORCLUBS || account.company?.name in Consts.MOTORCLUBS) && requireTaxId
                  div
                    style:
                      padding: 10
                      color: "#F00"
                    className: "error"
                    "Please set Company Tax ID by going to Settings > Configure > Companies"

                if enabled and endpoint.showIds
                  { disabledForDeregistering, disabledWhileRegistered } = that.isDisabledForISSCStatus(isAgeroRSCAccount, @record.id, site_info.site_id)
                  isDisabledWhileRegisteredAndDeletedVendorID = that.isDisabledWhileRegisteredAndDeletedVendorID(disabledWhileRegistered, @record?.dispatchable_sites?[dsite_index])

                  if isDisabledWhileRegisteredAndDeletedVendorID
                    ids = @record?.dispatchable_sites?[dsite_index]?.contractorids
                    allSavedIds = map(@record?.dispatchable_sites?[dsite_index]?.isscs, (connection) => connection.contractorid)
                    newIds = intersection(allSavedIds, ids)
                    if !isEqual(ids, newIds)
                      @setrecordData("dispatchable_sites."+dsite_index+".contractorids", newIds)

                  div
                    className: if isAgeroRSCAccount then 'site-content-container' else ''
                    style:
                      padding: "10px"
                    if disabledForDeregistering
                      span
                        style:
                          color: 'red'
                          marginBottom: 10
                          display: "block"
                        "Deregistration in progress. Cannot reregister this site until complete."

                    if endpoint.showLogin
                      div
                        style:
                          marginBottom: "10px"
                        div
                          style:
                            display: "inline-block"
                            marginRight: 35
                          label null,
                            "Username"
                          input
                            type: "text"
                            className: "form-control"
                            disabled: disabledForDeregistering || disabledWhileRegistered
                            style:
                              width: "150px"
                            value: site_info.username
                            onChange: (partial((dsite_index, e) =>
                              @setrecordData("dispatchable_sites."+dsite_index+".username", e.target.value)
                            , dsite_index))
                        div
                          style:
                            display: "inline-block"
                          label null,
                            "Password"
                          input
                            type: "password"
                            className: "form-control"
                            disabled: disabledForDeregistering || disabledWhileRegistered
                            style:
                              width: "150px"
                            value: site_info.password
                            onChange: (partial((dsite_index, e) =>
                              @setrecordData("dispatchable_sites."+dsite_index+".password", e.target.value)
                              @setrecordData("dispatchable_sites."+dsite_index+".incorrect", false)
                            , dsite_index))

                    if endpoint.showLocationId
                      disabledForContractorId = !isAgeroRSCAccount
                      if site_info
                        original_site=find_site_in_record(site_info.site_id)
                        if original_site
                          if original_site.contractorids.length==0
                            disabledForContractorId=false
                        else
                          disabledForContractorId=false
                      div
                        style:
                          marginBottom: "10px"
                        label null,
                          if !isAgeroRSCAccount then "Location ID" else "Agero Location"
                        if !isAgeroRSCAccount
                          input
                            type: "text"
                            className: "form-control"
                            style:
                              width: "150px"
                            disabled: disabledForContractorId || disabledForDeregistering || disabledWhileRegistered
                            value: site_info.locationid
                            onMouseEnter: () =>
                              if disabledForContractorId
                                @setState(clicked_location_id_when_contractorid_zero:true)
                            onMouseLeave: () =>
                              if disabledForContractorId
                                @setState(clicked_location_id_when_contractorid_zero:false)
                            onChange: (partial((dsite_index, e) =>
                              @setrecordData("dispatchable_sites."+dsite_index+".locationid", e.target.value)
                            , dsite_index))
                        else
                          availableLocations = filter(that.props.allAgeroLocations, (location) -> location.locationId == parseInt(site_info.locationid) || !that.state.selectedAgeroLocations.map((location) -> parseInt(location)).includes(location.locationId))
                          options = map(availableLocations, (location) -> return {value: location.locationId, address: location.address, zip: location.zip})
                          selectString = '- Select -'
                          options.unshift({value: selectString, address: null, zip: null})
                          select
                            className: 'form-control short-select'
                            name: 'location-select'
                            disabled: disabledForDeregistering || disabledWhileRegistered
                            value: if site_info.locationid? then site_info.locationid else selectString
                            placeholder: "Agero Location"
                            onChange: (partial((dsite_index, e) =>
                              if site_info.locationid?
                                that.setState({
                                  selectedAgeroLocations: pull(clone(that.state.selectedAgeroLocations), site_info.locationid)
                                })
                              recordValue = if e.target.value == selectString then null else e.target.value
                              @setrecordData("dispatchable_sites."+dsite_index+".locationid", recordValue)
                              newVendorIds = []
                              if recordValue != selectString
                                forEach(that.props.vendorConnections, (connection) ->
                                  if connection.allAuthorizedLocations.includes(recordValue)
                                    newVendorIds.push(connection.vendorId)
                                  )
                              @setrecordData("dispatchable_sites."+dsite_index+".contractorids", newVendorIds)
                            , dsite_index))
                            for item in options
                              displayValue = if (item.address && item.zip) then (item.address + ', ' + item.zip) else item.value
                              option
                                key: item.value + '_' + dsite_index
                                value: item.value
                                className: 'option-row'
                                displayValue
                        if @state.clicked_location_id_when_contractorid_zero
                          span
                            style:
                              color:"red"
                            "Remove Vendor IDs and Save before editing location, then re-enter vendor IDs"
                    if isAgeroRSCAccount
                      AgeroLocationsVendorList
                        vendorStatuses: site_info?.isscs
                    else
                      div null,
                        label
                          style:
                            display: "inline"
                            marginRight: "5px"
                          "Vendor IDs"
                        if !disabledForDeregistering && !isDisabledWhileRegisteredAndDeletedVendorID
                          ReactDOMFactories.i
                           className: 'fa fa-plus edit inline'
                           style:
                             color: "white"
                             padding: 3
                             borderRadius: 1
                             top: -2
                             position: "relative"
                             verticalAlign: "middle"
                             fontSize: 8
                             backgroundColor: "#AAAAAA"
                             cursor: "pointer"

                           onClick: ((dsite_index) => () =>
                             ids = @record?.dispatchable_sites?[dsite_index]?.contractorids
                             if not ids?
                              ids = []

                             ids.push("")

                             @setrecordData("dispatchable_sites."+dsite_index+".contractorids", ids)
                           )(dsite_index)
                           name: 'edit'
                        div null,
                          if site_info?.contractorids?
                             for id, index in site_info?.contractorids # note: for Agero BE needs to set site_infor.contractorids to the selected vendor ids for the site
                               div
                                style:
                                  display: "inline-block"
                                  width: "185px"
                                input
                                   type: "text"
                                   className: "form-control"
                                   style:
                                    display: "inline-block"
                                    width: "150px"
                                    marginRight: "5px"
                                   value: id
                                   disabled: that.isVendorIDDisabled(disabledForDeregistering, disabledWhileRegistered, isDisabledWhileRegisteredAndDeletedVendorID, id, index, @record?.dispatchable_sites?[dsite_index])
                                   onChange: (partial((dsite_index, index, e) =>
                                      ids = @record?.dispatchable_sites?[dsite_index]?.contractorids
                                      ids[index] = e.target.value.toUpperCase()
                                      @setrecordData("dispatchable_sites."+dsite_index+".contractorids", ids)
                                    , dsite_index, index))
                                if !disabledForDeregistering
                                  ReactDOMFactories.i
                                    onClick: partial((dsite_index, index) =>
                                        ids = @record?.dispatchable_sites?[dsite_index]?.contractorids
                                        ids.splice(index, 1)
                                        @setrecordData("dispatchable_sites."+dsite_index+".contractorids", ids)
                                      , dsite_index, index)
                                    className: 'fa fa-times-circle edit inline'
                                    style:
                                      color: "#AAAAAA"
                                      cursor: "pointer"
                                      fontSize: 18
                                    name: 'remove'
          }
        ]
      }

  getExtraCols: (account) =>
    if account.name not in Consts.MOTORCLUBS && account.company?.name not in Consts.MOTORCLUBS && account.company?.name != "Tesla" && account.company?.name != "Swoop"
      return []

    endpoints = [{
      name:"Tesla"
      showIds: false
    }]
    if account.company?.name == "Swoop"
      endpoints = [{
        name:"Swoop"
        showIds: false
      }]

    name = if account.company?.name? then account.company?.name else account.name
    if name in difference(Consts.MOTORCLUBS, Consts.DISABLED_MOTORCLUBS)
      endpoints = [
        {
          name: name
          showIds: true
          showLocationId: (name in [Consts.ROADAMERICA, Consts.AGERO])
          showLogin: name in [Consts.NATIONSAFE]
        }
      ]

    sites = []

    for endpoint in endpoints
      ageroDashBoard = []
      isAgeroRSCAccount = false
      if endpoint.name == "Agero"
        isAgeroRSCAccount = true
        ageroDashBoard = @getAgeroIntegration(endpoint)
      sites = sites.concat(ageroDashBoard.concat(@getSitesFields(endpoint, isAgeroRSCAccount)))
    return sites

  UNSAFE_componentWillReceiveProps: (props) ->
    if !@props.show && props.show
      defer => @refs.account_form.isValid()

      # force loading accounts to be used during validation phase
      # TODO we should be doing such validations on BE
      AccountStore.loadAndGetAll(@getId())

  render: ->
    super(arguments...)

    title = "Add Account"

    if @props.record? and @props.record.id?
      title = "Edit Account"

    record = clone(@props.record)

    if record?.id
      if !record.phone
        record.phone = AccountStore.getAccountKey(record.id, "phone", @getId())

      if !record.name
        record.name = AccountStore.getAccountKey(record.id, "name", @getId())

      if !record.fax
        record.fax = AccountStore.getAccountKey(record.id, "fax", @getId())

      if !record.location
        record.location = AccountStore.getAccountKey(record.id, "location", @getId())

      if !record.accounting_email
        record.accounting_email = AccountStore.getAccountKey(record.id, "accounting_email", @getId())

      if !record.primary_email
        record.primary_email = AccountStore.getAccountPrimaryEmail(record.id, @getId())

    EditForm extend({}, @props,
      ref: "account_form",
      extraClasses: "account_form"
      cols: [{fields: @data}]
      title: title
      getExtraCols: @getExtraCols
      record: record
    )

module.exports = StoreWrapper(AccountForm, () ->
  if @props.record?.name == 'Agero'
    account = find(AccountStore.getMotorClubAccounts(this.getId()), (club) => club?.name == 'Agero')
    vendorConnections = if account then account.accountIntegrations else []
    allAgeroLocations = uniqBy(flatten(map(vendorConnections, (connection) => connection.allAuthorizedLocations)), "locationId")

    return {
      vendorConnections,
      allAgeroLocations
    }
  return {}
,({
  constructor: (props) ->
    @register(FeatureStore, FeatureStore.CHANGE)
}))
