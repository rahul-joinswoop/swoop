import React from 'react'
import Logger from 'lib/swoop/logger'
import Storage from 'stores/storage'
import { isSafeInteger } from 'lodash'

const TabList = ({ props, currentTab, handleClick }) => {
  if (props.tabList.length > 1) {
    return (
      <ul className="qnav qnav-tabs" key="qnav-tabs">
        {props.tabList?.map((tab, index) => (
          <Tab
            currentTab={currentTab}
            handleClick={handleClick}
            index={index}
            key={tab.name}
            tab={tab}
          />
        ))}
      </ul>
    )
  }
  return null
}

const Tab = ({
  tab, index, currentTab, handleClick,
}) => {
  if (tab.hideTab) {
    return null
  }
  return (
    <li className={currentTab === index ? 'active' : undefined}>
      <a href={tab.url} onClick={() => { handleClick(index) }}>
        {typeof tab.name === 'function' ?
          tab.name(currentTab === index) :
          tab.name}
      </a>
    </li>
  )
}

class Tabs extends React.Component {
  constructor(props) {
    super(props)

    this.state = this.getInitialState()
  }

  getInitialState = () => {
    let tab = this.props.defaultTab || 0

    if (isSafeInteger(this.props.forcedTab)) {
      tab = this.props.forcedTab
    } else if (!this.props.disableSavedTabIndex && this.getLocalStorage(`${this.getTabsKey()}-tabs`)) {
      tab = parseInt(this.getLocalStorage(`${this.getTabsKey()}-tabs`))
    }

    if (this.props.tabList) {
      if (tab >= this.props.tabList.length) {
        tab = 0
      } else if (tab < 0) {
        tab = 0
      }
    }

    if (this.props.tabList?.[tab]?.onEnter) {
      this.props.tabList[tab].onEnter()
    }

    if (this.props.changeTab) {
      this.props.changeTab(this.props.tabList[tab])
    }

    return { currentTab: tab }
  }

  componentDidMount() {
    if (this.props.changeTab) {
      return this.props.changeTab(this.props.tabList[this.state.currentTab])
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.forcedTab) {
      this.setState({
        currentTab: nextProps.forcedTab,
      })
    }

    if (this.props?.tabList?.length !== nextProps?.tabList?.length) {
      if (nextProps.tabList?.[this.state.currentTab]?.onEnter) {
        Logger.debug('tabs hit enter with ', this.state.currentTab)
        nextProps.tabList[this.state.currentTab].onEnter()
      }

      if (nextProps.changeTab) {
        return nextProps.changeTab(nextProps.tabList[this.state.currentTab])
      }
    }
  }

  setLocalStorage = (key, value) => Storage.set(key, value)

  getLocalStorage = key => Storage.get(key)

  getTabsKey = () => this?.props?.tabs_key || location.hash

  handleClick = (index) => {
    this.setLocalStorage(`${this.getTabsKey()}-tabs`, index)

    const oldTab = this.props.tabList[this.state.currentTab]
    const newTab = this.props.tabList[index]

    if (oldTab && typeof oldTab.onExit === 'function') {
      oldTab.onExit()
    }

    if (newTab && typeof newTab.onEnter === 'function') {
      newTab.onEnter()
    }

    if (this.props.onSwitch) {
      this.props.onSwitch()
    }

    this.setState({
      currentTab: index,
    })

    if (this.props.changeTab) {
      this.props.changeTab(this.props.tabList[index])
    }
  }

  goToTab = (id) => {
    for (let index = 0; index < this.props.tabList.length; index++) {
      const tab = this.props.tabList[index]

      if (tab.id === id) {
        this.handleClick(index)
        return
      }
    }
  }

  render() {
    const { currentTab } = this.state
    const {
      className, sideComponents, bottomComponents, tabList,
    } = this.props
    const currentTabComponent = tabList[currentTab]

    return (
      <div className={className}>
        <nav>
          <TabList currentTab={this.state.currentTab} handleClick={this.handleClick} props={this.props} />
          {sideComponents && (typeof sideComponents === 'function' ? sideComponents(currentTab) : sideComponents)}
        </nav>
        {bottomComponents && (typeof bottomComponents === 'function' ? bottomComponents(currentTab) : bottomComponents)}
        {currentTabComponent && currentTabComponent.component(currentTabComponent.props)}
      </div>
    )
  }
}

export default Tabs
