import React from 'react'
import createReactClass from 'create-react-class'
import Logger from 'lib/swoop/logger'
SiteStore = require('stores/site_store').default
Autosuggest = React.createFactory(require('components/autosuggest/autosuggest'))
import { map, uniq, values } from 'lodash'

storeSelector = createReactClass(
  componentDidMount: ->
    if @props.populateEvent?
      @props.store.bind(@props.populateEvent,this.updateData)
  componentWillUnmount: ->
    if @props.populateEvent?
      @props.store.unbind(@props.populateEvent,this.updateData)
  getDefaultProps: ->
    showSelect: true
  updateData: ->
    @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously
  clear: ->
    Logger.debug("Clearing ", @refs, @refs.autosuggest)
    @refs.autosuggest?.clear()
  onSuggestSelect: (value) ->
    id = null
    for account_id, account of @props.populateFunc()
      if value? and account?.name? and account.name.toUpperCase() == value.toUpperCase()
        id = parseInt(account_id)
        if isNaN(id)
          id = account_id
        value = account.name
        break
    @props.onChange(id, value)

  render: ->
    accounts = uniq(values(@props.populateFunc()), "name")
    Logger.debug("accounts UUU: ", accounts)
    Autosuggest
      ref: "autosuggest"
      style: @props.style
      placeholder: @props.placeholder
      suggestions:  map(accounts, (account) -> account.name)
      id: 'InputAccount'
      className: ""
      onSuggestSelect: @onSuggestSelect
      initialValue: @props.value
      sortFunc: @props.sortFunc
      disabled: @props.disabled
)

module.exports = storeSelector
