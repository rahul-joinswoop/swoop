import Api from 'api'
import React from 'react'
require('stylesheets/components/filters.scss')
import Logger from 'lib/swoop/logger'
import moment from 'moment-timezone'
UserStore = require('stores/user_store')
UsersStore = require('stores/users_store').default
CompanyStore = require('stores/company_store').default
SiteStore = require('stores/site_store').default
ServiceStore = require('stores/service_store')
StoreSelector = React.createFactory(require('components/storeSelector'))
import BetweenDateSelectorImport from 'components/time_pickers/between_date_selector.js'
BetweenDateSelector = React.createFactory(BetweenDateSelectorImport)
ProviderStore = require ('stores/provider_store')
VehicleStore = require('stores/vehicle_store')
BaseComponent = require('components/base_component')
BackendSuggest = React.createFactory require('components/autosuggest/backend_suggest').default
import { map, partial } from 'lodash'

class Filters extends BaseComponent
  displayName: 'Filters'
  touched: false

  constructor: (props) ->
    super(props)
    @state = @calcInitialState()

  calcInitialState: =>
    obj = {}
    if @props.initialFrom

      m = moment(@props.initialFrom)
      year = m.get('year')
      if year < 2000
        year += 100
      m.set('year', year)


      if @props.filters?
        for prop_filter in @props.filters
          if prop_filter.type == "dates"
            obj[prop_filter.keys[1]] = m
    for prop_filter in @props.filters
      if prop_filter.type == "dates"
        if prop_filter.initialFrom?
          obj[prop_filter.keys[0]] = prop_filter.initialFrom
        if prop_filter.initialTo?
          obj[prop_filter.keys[1]] = prop_filter.initialTo

        setTimeout(@sendFilter, 0)

    obj


  getName: (name, secondary="All") =>
    if @state[name+"_name"]? then @state[name+"_name"] else secondary

  getRecordId: (name) =>
    if @state[name+"_id"]? then @state[name+"_id"] else null

  handleSelectChange: (name, id) =>
    obj = {}
    obj[name+"_id"] = id
    @setState obj
    setTimeout(@sendFilter, 0)

  handleOptionsChange: (options, name, id, value) =>
    obj = {}
    if options[id]?
      obj[name+"_id"] = options[id].value
      obj[name+"_name"] = value
    else
      obj[name+"_id"] = null
      obj[name+"_name"] = null
    @setState obj
    setTimeout(@sendFilter, 0)

  handleSelectorChange: (name, id, value) =>
    obj = {}
    obj[name+"_id"] = id
    obj[name+"_name"] = value
    @setState obj
    setTimeout(@sendFilter, 0)
  handleDateChange: (name, m) =>

    obj = {}
    obj[name] = m
    @setState obj
    setTimeout(@sendFilter, 0)

  handleDatesChange: (names, dates) =>

    obj = {}
    for date, i in dates
      obj[names[i]] = date

    @setState obj
    setTimeout(@sendFilter, 0)

  addSelectOptions: (options) =>
    map(options, (option) ->
      name: option.text
      id: option.value
    )

  addAll: (fetch) =>
    objs = fetch()
    objs[""] = name: "All", id: ""
    return objs


  hasChanged: () =>
    JSON.stringify(@getFilters()) != @state.last_filter

  getFilters: () =>
    filter = {}


    if @props.filters?
      for prop_filter in @props.filters
        if prop_filter.type == "partner"
          if !isNaN(parseInt(@getRecordId(prop_filter.key)))
            filter[prop_filter.key] = parseInt(@getRecordId(prop_filter.key))
        else if prop_filter.type == "dates"
          dates = prop_filter.keys
          for d, index in dates
            if @state[d]?
              if @state[d].isValid()
                filter[d] = @state[d].toISOString()
        else
          id = parseInt(@getRecordId(prop_filter.key))
          if !isNaN(id)
            filter[prop_filter.key] = id


    filter
  sendFilter: () =>
    filter = @getFilters()
    if JSON.stringify(filter) != @state.last_filter
      @setState
        error: null
        last_filter: JSON.stringify(filter)
      if @props.filterList?
        @props.filterList(filter)

  renderFilter: (filter) =>
    if filter.type == "partner"
      div
        className: "selector extended"
        span null,
          "Partner:"
        BackendSuggest
          placeholder: 'All'
          style:
            height: 32
          onSuggestSelect: (suggest) =>
            if suggest?
              @handleSelectorChange(filter.key, suggest.id, suggest.label)
            else
              @handleSelectorChange(filter.key, null, null)
          getSuggestions: (options, callback) ->
            if !UserStore.isSwoop() && !UserStore.isFleetInHouse()
              return

            Api.autocomplete_rescue_providers(options.input, 10, UserStore.getEndpoint(), {
              success: (data) =>
                locationMap = map(data, (rescue_provider) ->
                  id: rescue_provider.rescue_company_id
                  label: rescue_provider.name
                )
                callback(locationMap)
              error: (data, textStatus, errorThrown) =>
               console.warn("Autocomplete failed")
            })
    else if filter.type == "fleet"
      div
        key: "fleet"
        className: "selector"
        span null,
          "Fleet:"
        StoreSelector
          onChange: partial(@handleSelectorChange, filter.key)
          value: @getName(filter.key)
          populateFunc: partial(@addAll, partial(CompanyStore.getFleetsForStore, @getId()))
          store: CompanyStore
          populateEvent: CompanyStore.CHANGE
    else if filter.type == "company"
      div
        key: "company"
        className: "selector extended"
        span null,
          "Company:"
        BackendSuggest
          placeholder: 'All'
          style:
            height: 32
          onSuggestSelect: (suggest) =>
            if suggest?
              @handleSelectorChange(filter.key, suggest.id, suggest.label)
            else
              @handleSelectorChange(filter.key, null, null)

          getSuggestions: (options, callback) ->
            Api.autocomplete_company('Company',options.input,10,{
              success: (data) =>
                locationMap = map(data, (company) ->
                    id: company.id
                    label: company.name
                  )
                callback(locationMap)
              error: (data, textStatus, errorThrown) =>
                console.warn("Autocomplete failed")
            })
    else if filter.type == "account"
      div
        key: "account"
        className: "selector extended"
        span null,
          "Account:"
        BackendSuggest
          placeholder: 'All'
          style:
            height: 32
          onSuggestSelect: (suggest) =>
            if suggest?
              @handleSelectorChange(filter.key, suggest.id, suggest.label)
            else
              @handleSelectorChange(filter.key, null, null)

          getSuggestions: (options, callback) ->
            if !UserStore.isPartner() && !UserStore.isSwoop()
              return
            Api.autocomplete_accounts(options.input, 10, UserStore.getEndpoint(), {
              success: (data) =>
                locationMap = map(data, (account) ->
                    id: account.id
                    label: account.name
                  )
                callback(locationMap)
              error: (data, textStatus, errorThrown) =>
                console.warn("Autocomplete failed")
            })
    else if filter.type == "service"
      div
        key: "service"
        className: "selector"
        span null,
          "Service:"
        StoreSelector
          onChange: partial(@handleSelectorChange, filter.key)
          value: @getName(filter.key)
          populateFunc: partial(@addAll, ServiceStore.getServicesForStore)
          store: ServiceStore
          populateEvent: ServiceStore.CHANGE
    else if filter.type == "driver"
      div
        key: "driver"
        className: "selector"
        span null,
          "Driver:"
        StoreSelector
          onChange: partial(@handleSelectorChange, filter.key)
          value: @getName(filter.key)
          populateFunc: partial(@addAll, partial(UsersStore.getDriversForStore, @getId()))
          store: UsersStore
          populateEvent: UsersStore.CHANGE
    else if filter.type == "vehicle"
      div
        key: "vehicle"
        className: "selector"
        span null,
          "Vehicle:"
        StoreSelector
          onChange: partial(@handleSelectorChange, filter.key)
          value: @getName(filter.key)
          populateFunc: partial(@addAll, VehicleStore.getAll) # TODO refactor to Backendsuggest
          store: VehicleStore
          populateEvent: VehicleStore.CHANGE
    else if filter.type == "dates"
      Logger.debug("INITIALS: ", filter)
      div
        key: "dates"
        className: "selector dates"
        span
          className: "date_title"
          "Date:"
        div null,
          BetweenDateSelector
            onChangeFrom: partial(@handleDateChange, filter.keys[0])
            onChangeTo: partial(@handleDateChange, filter.keys[1])
            initialFrom: filter.initialFrom
            initialTo: filter.initialTo
    else if filter.type == "options"
      div
        key: filter.key
        className: "selector"
        span null,
          filter.label

        StoreSelector
          onChange: partial(@handleOptionsChange, filter.options, filter.key)
          value: @getName(filter.key, filter.options[0].text)
          populateFunc: partial(@addSelectOptions, filter.options)
          sortFunc: (x) -> x

  render: ->
    #selectors = ["account",  "service", "truck", "rescue_driver_id", "partner"]
    div
      className: [
        "filters"
        "invoice_filter" if @props.filters? and @props.filters.length > 0
        ].join(' ')
      div
        className: "filters"
        if @props.filters?
          for filter in @props.filters
            @renderFilter(filter)
        if @state.error?
          label
            className: "error"
            @state.error


module.exports = Filters
