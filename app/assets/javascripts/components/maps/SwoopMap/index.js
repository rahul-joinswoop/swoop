import 'stylesheets/components/maps/SwoopMap.scss'
import React, { useState, useEffect } from 'react'
import { GoogleMap } from '@react-google-maps/api'
import Loading from 'componentLibrary/Loading'
import MapStore from 'stores/map_store'
import './style.scss'

// Contains the callbacks to set maps on the components
const setters = {}
// Contains an ongoing count of components to be used as an id into the setters hash
let count = 0
// Contains the shared map instance once google is loaded
let mapInstance = null

// Checks if google is loaded every 300 seconds and returns true when it is
// This can be removed once we remove the script tag from the head and useLoadScript instead
function useWaitForGoogle() {
  const [attempts, setAttempts] = useState(0)
  const loadError = attempts > 50
  useEffect(() => {
    if (!window.google && !loadError) {
      const id = setTimeout(() => {
        setAttempts(attempts + 1)
      }, 300)
      return () => {
        clearTimeout(id)
      }
    }
  }, [attempts, loadError])

  return [!!window.google, loadError]
}
// This builds up a queue of components
// The last component in the queue has access to the map
// If the last component unmounts it is given to the prior component
// Returns the map instance and a stealMap function to jump to the end of the line
function useMap() {
  /*
    // TODO: eventually we will want to load in google dynamically
    // For now just assume it's loaded in from a script
    const { isLoaded, loadError } = useLoadScript({
      googleMapsApiKey: '',
    })
  */
  const [isLoaded, loadError] = useWaitForGoogle()

  const [map, setMap] = useState(mapInstance)

  // Use a count to rerun the steal effect
  const [stealCount, setStealCount] = useState(0)
  function triggerSteal() {
    setStealCount(countState => countState + 1)
  }

  function setMapOnLastComponent(newMapValue) {
    const setterArr = Object.values(setters)
    if (setterArr.length > 0) {
      setterArr[setterArr.length - 1](newMapValue)
    }
  }

  // Something else took our map away,
  // so show steal map button again
  // can remove this once everything is using SwoopMap
  useEffect(() => {
    const onMapStolen = () => {
      setMapOnLastComponent(null)
    }
    MapStore.bind(MapStore.MAP_STOLEN, onMapStolen)
    return () => {
      MapStore.unbind(MapStore.MAP_STOLEN, onMapStolen)
    }
  }, [])


  useEffect(() => {
    if (!isLoaded) {
      if (loadError) {
        Rollbar.error('Problem loading map library', loadError)
      }
      return
    }
    if (!mapInstance) {
      // Once MapStore isn't being used anywhere else, these lines can be commented back in and MapStore can be removed
      // const div = document.createElement('div')
      mapInstance = MapStore.stealMap(null) // new google.maps.Map(div)
    } else {
      // Let external views know I am taking the map
      MapStore.stealMap()
    }

    // Add myself onto the end of the line and steal the map
    const myCountId = count
    setters[myCountId] = setMap
    count += 1
    setMap(mapInstance)

    return () => {
      // If I am unmounting or resetting, remove my position, and give back map to last in line
      delete setters[myCountId]
      setMapOnLastComponent(mapInstance)
    }
  }, [isLoaded, loadError, stealCount])

  return [map, triggerSteal, isLoaded]
}

class GoogleMapShim extends GoogleMap {
  getInstance = () => this.props.map

  componentDidMount() {
    this.mapRef.appendChild(this.props.map.getDiv())
    this.props.map.setOptions(this.props.options)
    super.componentDidMount()
  }
}


export default function SwoopMap(props) {
  const [map, stealMap, isLoaded] = useMap()
  return (
    <div className="GMap" style={props.style}>
      {map ? (
        <GoogleMapShim
          map={map}
          mapContainerClassName="map_canvas"
          mapContainerStyle={props.style}
          {...props}
        />
      ) :
        (
          <div className="map_alternate" onClick={stealMap} style={props.style}>
            {isLoaded ? (
              <>
                <div className="background" />
                <div className="steal-map-button">View Map</div>
              </>
            ) : (
              <Loading />
            )}
          </div>
        )}
    </div>
  )
}
