import VehicleStore from 'stores/vehicle_store'
import $ from 'jquery'

function setInfoWindowCSS(node) {
  node.css({
    marginLeft: '-12px',
    padding: '0',
  })

  node.find('.driver-and-truck-section').css({
    height: '30px',
    marginTop: '-7px',
    borderBottom: '1px solid #f2f2f2',
  })

  node.find('.driver-and-truck-section div').css({
    display: 'block',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    padding: '3px',
  })

  node.find('.driver-name-and-toggle').css({
    padding: '7px',
  })

  node.find('.select-truck-container').css({
    padding: '7px',
  })

  node.find('.driver-name-and-toggle').css({
    fontWeight: 'bold',
    borderRight: '1px solid #f2f2f2',
    overflow: 'hidden',
  })

  node.find('.driver-name-and-toggle .on-off-circle').css({
    display: 'inline-block',
  })

  node.find('.driver-name-and-toggle .truck-toggle').css({
    display: 'none',
  })

  node.find('.select-truck-container .font-awesome-hover-bubble').css({
    margin: '0 5px 0 0',
  })

  node.find('.select-truck-container .font-awesome-hover-bubble .bubble').css({
    right: '-37px',
    top: '-65px',
  })

  node.find('.select-truck-container .font-awesome-hover-bubble .bubble > span').css({
    padding: '3px',
    marginLeft: '92px',
    boxShadow: '0 0 0 1px rgba(0, 0, 0, 0.25)',
    border: 'none',
  })

  node.find('.select-truck-container .font-awesome-hover-bubble .error-bubble-container').css({
    display: 'none',
    padding: 0,
  })

  node.find('.select-truck-container .font-awesome-hover-bubble .error-bubble-container .alert-last-gps-update-bubble').css({
    color: 'black',
    padding: 0,
  })

  node.find('.select-truck-container .font-awesome-hover-bubble .error-bubble-container .alert-last-gps-update-bubble .title').css({
    marginBottom: '5px',
    fontWeight: 'bold',
  })

  node.find('.select-truck-container .font-awesome-hover-bubble .error-bubble-container .alert-last-gps-update-bubble div').css({
    textAlign: 'center',
  })

  if (!node.find('.last-history-item-container .last-history-item').length) {
    node.find('.last-history-item-container').remove()
    return node[0]
  }

  node.find('.last-history-item-container').css({
    maxHeight: '81px',
    overflow: 'auto',
    padding: '5px 5px 0 5px',
    width: '307px',
  })

  node.find('.last-history-item-container .last-history-item').css({
    margin: '3px',
  })

  node.find('.last-history-item-container .last-history-item span').css({
    background: '#f2f2f2',
    padding: '5px',
    borderRadius: '5px',
  })

  node.find('.last-history-item-container .last-history-item .last-history-item-details').css({
    display: 'block',
  })

  return node[0]
}

function createTruckInfoWindowNodeModification(node, name, isFleet) {
  node.removeClass('col-xs-10')
  // need to remove id so can clone from driver's list again if replacing existing pop-up
  node.removeAttr('id')
  node.removeAttr('data-reactid')
  /* eslint-disable-next-line */
  for (const child of node.find('*')) {
    $(child).removeAttr('data-reactid')
  }
  node.find('select').replaceWith(`<span>${name}</span>`)
  node.find('.font-awesome-hover-bubble').removeClass('flip')

  if (isFleet) {
    node.find('.driver-and-truck-section').removeClass('equal-cols')
    node.find('.select-truck-container').removeClass('col-xs-6')
    node.find('.driver-name-and-toggle').remove()
    node.find('.last-history-item-container').remove()
  }
}

export function createTruckInfoWindowContent(id, nameOrNode, driverId, getId) {
  if (driverId && $(`#driver_jobs_${driverId}`).length > 0) {
    const node = $(`#driver_jobs_${driverId}`).clone()
    const name = VehicleStore.get(id, 'name', getId())
    createTruckInfoWindowNodeModification(node, name, false)
    // Note: we have to use inline styles here to get rid of the jump with google maps info windows.
    return setInfoWindowCSS(node)
  } else {
    return VehicleStore.get(id, 'name', getId()) || nameOrNode
  }
}

export default { createTruckInfoWindowContent }
