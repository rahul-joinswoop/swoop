import React, { useEffect } from 'react'
import { useGoogleMap } from '@react-google-maps/api'
import { forEach, isFunction, upperFirst } from 'lodash'
import { useInstance, usePrevious } from 'componentLibrary/Hooks'

const propNames = [
  // 'animation', don't allow animation, use bounceTimes instead
  'clickable',
  'cursor',
  'draggable',
  'icon',
  'label',
  // 'map', don't allow to override map
  'opacity',
  'options',
  'position',
  'shape',
  'title',
  'visible',
  'zIndex',
]

function updateProps(marker, props) {
  // I know it is not allowed to call hooks in loop. But we have static array
  forEach(propNames, (propName) => {
    const value = props[propName]
    const prevValue = usePrevious(value)

    // originally all prevValues are `undefined`, and if no prop is passed the value is `undefined` too
    // so if no prop is passed we don't set the value for it
    if (value !== prevValue) {
      const method = `set${upperFirst(propName)}`
      marker[method](value)
    }
  })
}

const eventMap = {
  onAnimationChanged: 'animation_changed',
  onClick: 'click',
  onClickableChanged: 'clickable_changed',
  onCursorChanged: 'cursor_changed',
  onDblClick: 'dblclick',
  onDrag: 'drag',
  onDragEnd: 'dragend',
  onDraggableChanged: 'draggable_changed',
  onDragStart: 'dragstart',
  onFlatChanged: 'flat_changed',
  onIconChanged: 'icon_changed',
  onMouseDown: 'mousedown',
  onMouseOut: 'mouseout',
  onMouseOver: 'mouseover',
  onMouseUp: 'mouseup',
  onPositionChanged: 'position_changed',
  onRightClick: 'rightclick',
  onShapeChanged: 'shape_changed',
  onTitleChanged: 'title_changed',
  onVisibleChanged: 'visible_changed',
  onZindexChanged: 'zindex_changed',
}

function updateListeners(marker, props) {
  // I know it is not allowed to call hooks in loop. But we have static eventMap
  forEach(eventMap, (googleEventName, reactEventName) => {
    const handler = props[reactEventName]

    useEffect(() => {
      const listener = isFunction(handler) ?
        marker.addListener(googleEventName, handler) :
        null

      return () => listener?.remove()
    }, [googleEventName, handler])
  })
}

const Marker = (props) => {
  const {
    bounceTimes, children, position, options = {},
  } = props
  const map = useGoogleMap()

  const marker = useInstance(() => {
    const markerOptions = {
      ...options,
      map,
      position,
    }

    if (bounceTimes) {
      markerOptions.animation = google.maps.Animation.BOUNCE
    }

    return new google.maps.Marker(markerOptions)
  })

  // clean up on unmount
  useEffect(() => () => marker.setMap(null), [marker])

  useEffect(() => {
    if (bounceTimes) {
      setTimeout(() => marker.setAnimation(null), 750 * bounceTimes)
    }
  }, [bounceTimes, marker])

  updateProps(marker, props)
  updateListeners(marker, props)

  let child = null
  if (children) {
    child = React.Children.only(children)
    child = React.cloneElement(child, { anchor: marker })
  }

  return child
}

export default Marker
