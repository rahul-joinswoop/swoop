import React from 'react'
import PropTypes from 'prop-types'
import { InfoWindow } from '@react-google-maps/api'
import { useToggle } from 'componentLibrary/Hooks'
import Marker from 'components/maps/Marker'

const InfoMarker = ({
  children, icon = '/assets/images/marker_blue_site.png', position,
  onClick, onMouseOver, onMouseOut, markerProps, defaultInfoWindowToggle = false,
}) => {
  const [infoWindow, toggleInfoWindow] = useToggle(defaultInfoWindowToggle)

  return (
    <Marker
      icon={icon}
      onClick={(onClick && (() => {
        if (onClick) {
          onClick({ toggleInfoWindow })
        }
      })) || toggleInfoWindow}
      onMouseOut={(onMouseOut && (() => {
        onMouseOut({ toggleInfoWindow })
      }))}
      onMouseOver={(onMouseOver && (() => {
        onMouseOver({ toggleInfoWindow })
      }))}
      position={position}
      {...markerProps}
    >
      { infoWindow && children && (
        <InfoWindow onCloseClick={toggleInfoWindow}>
          <div>
            {children}
          </div>
        </InfoWindow>
      )}
    </Marker>
  )
}

InfoMarker.propTypes = {
  icon: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  position: PropTypes.exact({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
}

export default InfoMarker
