import React, { useLayoutEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { OverlayView } from '@react-google-maps/api'
import './style.scss'

const OverlayBox = ({
  callback, children, className,
}) => {
  const theOverlay = useRef()
  const [opacity, setOpacity] = useState(0)

  useLayoutEffect(() => {
    const box = theOverlay.current.getBoundingClientRect()
    callback({
      height: box.height,
      width: box.width,
    })
    // give the overlay time enough to calculate its correct position; hides the "jump"
    setTimeout(() => {
      setOpacity(1)
    }, 250)
  }, [callback])

  return (
    <div
      className={classnames('google-maps-overlay-view', className)}
      ref={theOverlay}
      style={{
        opacity,
      }}
    >
      {children}
    </div>
  )
}

const Overlay = ({
  children, className, mapPaneName = OverlayView.OVERLAY_MOUSE_TARGET, offset = { x: 0, y: 0 }, position, ...props
}) => {
  const { lat, lng } = position
  const [overlayBox, setOverlayBox] = useState({ height: 0, width: 0 })

  const getPixelPositionOffset = () => {
    const { height, width } = overlayBox
    return {
      x: -(width / 2 + offset.x),
      y: -(height + 10 + offset.y), // +10 to accommodate the overlay's arrow
    }
  }

  return (
    <OverlayView
      getPixelPositionOffset={getPixelPositionOffset}
      mapPaneName={mapPaneName}
      position={{ lat, lng }}
    >
      <OverlayBox
        callback={setOverlayBox}
        className={className}
        {...props}
      >
        {children}
      </OverlayBox>
    </OverlayView>
  )
}

Overlay.propTypes = {
  className: PropTypes.string,
  mapPaneName: PropTypes.oneOf([
    OverlayView.FLOAT_PANE,
    OverlayView.MAP_PANE,
    OverlayView.MARKER_LAYER,
    OverlayView.OVERLAY_LAYER,
    OverlayView.OVERLAY_MOUSE_TARGET,
  ]),
  offset: PropTypes.exact({
    x: PropTypes.number,
    y: PropTypes.number,
  }),
  position: PropTypes.exact({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }).isRequired,
  style: PropTypes.object,
}

export default Overlay
