import React from 'react'
import PropTypes from 'prop-types'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import Overlay from 'components/maps/Overlay'
import { Marker } from '@react-google-maps/api'
import { useToggle } from 'componentLibrary/Hooks'
import './style.scss'

const OverlayMarker = ({
  children, className, hideClose, icon = '/assets/images/marker_blue_site.png', position, overlayOffset = { x: 0, y: 10 },
}) => {
  const { lat, lng } = position
  const [overlayView, toggleOverlayView] = useToggle(false)

  return (
    <Marker
      icon={icon}
      onClick={toggleOverlayView}
      position={{ lat, lng }}
    >
      { overlayView && (
        <Overlay
          className={className}
          offset={overlayOffset}
          position={{ lat, lng }}
        >
          <div className="google-maps-overlay-marker-ui">
            {children}
            { !hideClose && (
              <FontAwesomeButton
                className="close-overlay"
                color="secondary-o"
                icon="fa-times"
                onClick={() => toggleOverlayView(false)}
              />
            )}
          </div>
        </Overlay>
      )}
    </Marker>
  )
}

OverlayMarker.propTypes = {
  className: PropTypes.string,
  hideClose: PropTypes.bool,
  icon: PropTypes.string,
  position: PropTypes.exact({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
}

export default OverlayMarker
