import React, { useEffect, useState } from 'react'
import setupMapPadding from 'setupMapPadding'
import SiteStore from 'stores/site_store'
import PoiStore from 'stores/poi_store'
import VehicleStore from 'stores/vehicle_store'
import { filter, map } from 'lodash'
import moment from 'moment-timezone'
import { usePrevious, useToggle } from 'componentLibrary/Hooks'
// import SwoopMap from 'components/maps/SwoopMap'
import InfoMarker from 'components/maps/InfoMarker'
import Marker from 'components/maps/Marker'
import { Circle, InfoWindow, OverlayView, DirectionsRenderer } from '@react-google-maps/api'
import { useRenderWatcherId } from 'stores/storeHooks'
import { getDirections } from 'lib/google-maps'

setupMapPadding()

export function DirectionsPath({ origin, destination, lineColor }) {
  const [directions, setDirections] = useState(null)

  // load new directions only when origin or destination changes
  useEffect(
    () => getDirections(origin, destination, (result, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        setDirections(result)
      } else {
        Rollbar.warning('Got bad response back looking up directions', { status, origin, destination })
      }
    }),
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
    [destination?.lat, destination?.lng, origin?.lat, origin?.lng]
  )

  if (!origin || !destination) {
    return null
  }

  return directions &&
    <DirectionsRenderer
      options={{
        directions,
        preserveViewport: true,
        suppressInfoWindows: false,
        suppressMarkers: true,
        polylineOptions: {
          strokeColor: lineColor || '#90BAD4',
        },
      }}
    />
}

export function SiteOrPlaceMarker({
  id,
  store = SiteStore,
  onClick,
  markerIcon = '/assets/images/marker_blue_site.png',
}) {
  const watcherId = useRenderWatcherId()
  const name = store.get(id, 'name', watcherId)
  const location = store.get(id, 'location', watcherId)

  return (
    <InfoMarker
      icon={markerIcon}
      key={id}
      onClick={() => {
        if (onClick) {
          onClick(location.lat, location.lng, { id })
        }
      }}
      onMouseOut={({ toggleInfoWindow }) => {
        toggleInfoWindow(false)
      }}
      onMouseOver={({ toggleInfoWindow }) => {
        toggleInfoWindow(true)
      }}
      position={{
        lat: location.lat,
        lng: location.lng,
      }}
    >
      <div>
        {name}
      </div>
    </InfoMarker>
  )
}

export function SiteMarkers({
  useSitesAndPlacesTreshold, sites, poiSites, onClick,
}) {
  if (useSitesAndPlacesTreshold) {
    return null
  }

  let newSitesIds = []
  let newPlacesIds = []

  // TODO: don't render if the lat/lng is the same as a origin/destination that we are showing

  if (sites) {
    newSitesIds = map(sites, 'id')
    newSitesIds = filter(newSitesIds, siteId => SiteStore.get(siteId, 'location')?.address)
  }

  if (poiSites) {
    newPlacesIds = map(poiSites, 'id')
    newPlacesIds = filter(newPlacesIds, placeId => PoiStore.get(placeId, 'location')?.address)
  }

  // TODO: only use place ids that don't exist in site ids

  // TODO: At one point we showed a threshhold of sites instead of all of them
  // Probably want to bring that concept back

  return (
    <>
      { newSitesIds.map(siteId =>
        <SiteOrPlaceMarker id={siteId} key={siteId} onClick={onClick} />
      )}
      { newPlacesIds.map(placeId =>
        <SiteOrPlaceMarker
          id={placeId}
          key={placeId}
          markerIcon="/assets/images/marker_yellow.png"
          onClick={onClick}
          store={PoiStore}
        />
      )}
    </>
  )
}

export function TruckMarker({
  id, children, showInfoWindow, setSelectedId, showInfoWindowAlways, hideOldTrucks, onClick, showTruckNames = false,
}) {
  // TODO: Make this a Sliding Marker  with the following props
  /*
        #return new google.maps.Marker
        preserveViewport: true
        key: "Truck_id",
        icon: @createTruckImage()
        duration: 20000
        easing: "linear"
        #defaultAnimation: 2
    */

  const watcherId = useRenderWatcherId()
  const [infoWindow, toggleInfoWindow] = useToggle(showInfoWindowAlways)

  const prevShowInfoWindow = usePrevious(showInfoWindow)
  useEffect(() => {
    if (showInfoWindow !== prevShowInfoWindow) {
      toggleInfoWindow(showInfoWindow)
    }
  }, [showInfoWindow, prevShowInfoWindow, toggleInfoWindow])

  if (!id) {
    return null
  }

  if (hideOldTrucks) {
    const vehicleLocationUpdatedAt = VehicleStore.get(id, 'location_updated_at', watcherId)
    if (moment().diff(vehicleLocationUpdatedAt) > 1000 * 60 * 30) {
      return null
    }
  }

  return (
    <Marker
      icon={{
        url: '/assets/images/truck.png',
        scaledSize: new google.maps.Size(50, 22),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(25, 11),
      }}
      key={`Truck_${id}`}
      onClick={() => {
          onClick?.(id)

          if (!showTruckNames) {
            return
          }

          // TODO showInfoWindowAlways is not used anywhere. Delete this logic
          if (showInfoWindowAlways) {
            // TODO: probably want to pas in marker here and do the following
            // TOOD: marker.setZIndex google.maps.Marker.MAX_ZINDEX + 1

            // TODO: check to see if this is needed, used for bringing to top
            toggleInfoWindow(false)
            setTimeout(() => toggleInfoWindow(true), 0)
            // TODO: infoWindow.setZIndex google.maps.Marker.MAX_ZINDEX + 1
          } else {
            toggleInfoWindow()

            // if we open info window close others by pushing opened id to parent component
            if (infoWindow) {
              setSelectedId?.(null)
            } else {
              setSelectedId?.(id)
            }
          }
      }}
      position={{
        lat: VehicleStore.get(id, 'lat', watcherId),
        lng: VehicleStore.get(id, 'lng', watcherId),
      }}
    >
      {showTruckNames && infoWindow && (
        <InfoWindow
          onCloseClick={toggleInfoWindow}
          options={{
            maxWidth: 307,
          }}
        >
          {children || (
            <div>
              {VehicleStore.get(id, 'name', watcherId)}
            </div>
          )}
        </InfoWindow>
      )}
    </Marker>
  )
}


export const UserMarker = ({ lat, lng }) => {
  if (lat && lng) {
    return <Marker
      icon={{
        url: '/assets/images/user.png',
        scaledSize: new google.maps.Size(22, 22),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(11, 11),
      }}
      position={{ lat, lng }}
    />
  }
  return null
}

// This should be done by passing trucks in, instead of candidates as they result in the same thing
/* renderCandidates = () => {
    const { candidates = [], showInfoWindowAlways = false } = this.props
    return candidates.map((candidate) => {
      return this.renderTruck(candidate.vehicle)
    })
  } */

export function TruckMarkers({
  trucks, hideOldTrucks, onClick, showTruckNames, showInfoWindowAlways,
}) {
  const [selectedId, setSelectedId] = useState()

  return trucks.map(id => (
    <TruckMarker
      hideOldTrucks={hideOldTrucks}
      id={id}
      key={id}
      onClick={onClick}
      setSelectedId={setSelectedId}
      showInfoWindow={selectedId === id}
      showInfoWindowAlways={showInfoWindowAlways}
      showTruckNames={showTruckNames}
    />
  ))
}

export function RouteMarker({ routeNode, icon, label }) {
  const { address, lat, lng } = routeNode

  if (!lat || !lng) {
    return null
  }

  return (
    <InfoMarker
      icon={icon}
      markerProps={{
        animation: 2,
        label: {
          color: '#FFF',
          text: label,
        },
      }}
      position={{ lat, lng }}
    >
      {address && <div>{address}</div>}
    </InfoMarker>
  )
}

export function HiddenRouteMarker({
  color, lat, lng, zoneBackground, zoneLabel,
}) {
  if (!lat || !lng) {
    return null
  }
  return (
    <>
      <Circle
        center={{ lat, lng }}
        options={{
          fillColor: color,
          fillOpacity: 0.35,
          radius: 805,
          strokeColor: color,
          strokeOpacity: 0.8,
          strokeWeight: 2,
        }}
      />
      <OverlayView
        mapPaneName={OverlayView.FLOAT_PANE}
        position={{ lat, lng }}
      >
        <div className="popup-container">
          <div className="popup-bubble-anchor">
            <div
              className="google-maps-custom-popup popup-bubble"
              style={{
                backgroundColor: zoneBackground,
              }}
            >
              {zoneLabel}
            </div>
          </div>
        </div>
      </OverlayView>
    </>
  )
}

export function Route({ route, obfuscate = false, markerImage }) {
  if (!route.id) {
    return null
  }
  if (obfuscate) {
    return (
      <>
        <HiddenRouteMarker
          color="#42B756"
          lat={route.origin.lat}
          lng={route.origin.lng}
          zoneBackground="rgba(66, 183, 86, 0.35)"
          zoneLabel="Pickup Zone"
        />
        {route.dest && <HiddenRouteMarker
          color="#F00"
          lat={route.dest.lat}
          lng={route.dest.lng}
          zoneBackground="rgba(255, 0, 0, 0.35)"
          zoneLabel="Drop Off Zone"
        />}
      </>
    )
  } else {
    return (
      <>
        {route.origin && <RouteMarker
          icon={markerImage || {
            url: route?.originMarker,
            labelOrigin: new google.maps.Point(13, 15),
          }}
          label={route?.origin?.label || 'A'}
          routeNode={route.origin}
        />}
        {route.dest &&
          <>
            <RouteMarker
              icon={markerImage || {
                url: route?.destMarker,
                labelOrigin: new google.maps.Point(13, 15),
              }}
              label={route?.dest?.label || 'B'}
              routeNode={route.dest}
            />
            <DirectionsPath destination={route.dest} origin={route.origin} />
          </>}
      </>
    )
  }
}

export function MapRoutes({ routes, obfuscate }) {
  return routes.map(route => <Route key={route.id} obfuscate={obfuscate} route={route} />)
}


// export default function mapNew({
//   children, forceHeight, width, height,
//   routes, autoAssigning,
//   useSitesAndPlacesTreshold, sites, poiSites, onClick,
//   trucks, hideOldTrucks, showInfoWindowAlways,
// }) {
//   const style = {}
//   if (forceHeight) {
//     style.height = forceHeight
//   }
//   if (width) {
//     style.width = width
//   }
//   if (height) {
//     style.height = height
//   }
//   let center = null
//   if (routes?.[0]?.origin) {
//     const { lat, lng } = routes?.[0]?.origin
//     center = {
//       lat,
//       lng,
//     }
//   }
//
//   // TODO: bind to truck changes
//   //    VehicleStore.bind(VehicleStore.CHANGE+"_"+truck, @vehiclesChanged)
//
//   // TODO: set up listeners passed in
//   // TODO: set up onclick and onbounds changed
//   /*
//       if @props.onClick?
//         google.maps.event.addListener(theMap, 'click', (e) =>
//           @props?.onClick?(e.latLng.lat(), e.latLng.lng(), e)
//         )
//
//       if @props.onBoundsChanged?
//         google.maps.event.addListener(theMap, 'bounds_changed', (e) =>
//           @props?.onBoundsChanged?(theMap.getBounds())
//         )
//
//     */
//   return (
//     <SwoopMap
//       center={center}
//       style={{
//         position: 'relative',
//         ...style,
//       }}
//       zoom={13}
//       {...mapOptions}
//     />
//   )
// }
