/* eslint react/no-multi-comp: 0 */
/* eslint no-lonely-if: 0 */

import React, { useEffect, useRef, useState, useCallback } from 'react'
import classnames from 'classnames'
import { drawCircle, fitBounds, MapControls, PolygonWithListeners } from 'components/maps/Utils'
import { DrawingManager } from '@react-google-maps/api'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import InfoMarker from 'components/maps/InfoMarker'
import OverlayMarker from 'components/maps/OverlayMarker'
import SearchBox from 'components/maps/Territories/SearchBox'
import SiteStore from 'stores/site_store'
import StoreWrapper from 'StoreWrapper'
import SwoopMap from 'components/maps/SwoopMap'
import TerritoriesApi from 'components/maps/Territories/territories_api'
import UserStore from 'stores/user_store'
import { useOnClickOutside } from 'componentLibrary/Hooks'
import { flatten, forEach } from 'lodash'
import './style.scss'

let useGetAllTerritoriesCount = 0 // keep track of asynchronous calls, so that we can control invocations of fitBounds

const useGetAllTerritories = (companyId) => {
  const [territories, setTerritories] = useState([])
  const apiGetAllTerritories = useCallback((id = companyId) => {
    TerritoriesApi.getAllTerritories(id, {
      success: (data) => {
        const response = data.territories
        setTerritories(response)
      },
      error: (error) => {
        Rollbar.error(`Failure to fetch territories from BE: ${error}`)
      },
    })
    useGetAllTerritoriesCount += 1
  }, [companyId, setTerritories])
  useEffect(apiGetAllTerritories, [])
  return [territories, apiGetAllTerritories]
}

const parseCoordinatesForPolygon = (polygon) => {
  const path = polygon.getPath()
  const coordinates = []
  for (let i = 0; i < path.length; i++) {
    const coord = {
      lat: path.getAt(i).lat(),
      lng: path.getAt(i).lng(),
    }
    coordinates.push(coord)
  }
  return coordinates
}

let cancelDraw = false
let nextOverlay = null

const Territories = (props) => {
  const territoriesComponent = useRef()
  const theMap = useRef()
  const [drawingMode, setDrawingMode] = useState(null)
  const [focus, setFocus] = useState(false)
  const [map, setMap] = useState(null)
  const [saving, setSaving] = useState(false)
  const [selected, setSelected] = useState(null)
  const [territories, apiGetAllTerritories] = useGetAllTerritories(props.company_id)
  const { sites } = props

  const handleFitBounds = useCallback(() => {
    const compileBounds = () => {
      const sitesPoints = sites.map(site => {
        const { lat, lng } = site.location
        return { lat, lng }
      })
      const territoriesPoints = flatten(territories.map(territory => territory.paths))
      return [...sitesPoints, ...territoriesPoints]
    }
    fitBounds(map, compileBounds())
  }, [map, sites, territories])

  const handleSaving = useCallback(() => {
    setSaving(true)
    setTimeout(() => setSaving(false), 2500)
  }, [setSaving])

  const apiPostTerritory = (newTerritory, companyId = props.company_id) => {
    handleSaving()
    TerritoriesApi.postTerritory(companyId, newTerritory, {
      success: (data) => {
        const response = data.territory
        apiGetAllTerritories()
        setSelected(response.id)
      },
      error: (error) => {
        nextOverlay.setMap(null)
        nextOverlay = null
        console.error(error)
      },
    })
  }

  const apiDeleteTerritory = useCallback(() => {
    if (selected) {
      handleSaving()
      TerritoriesApi.deleteTerritory(selected, {
        success: (data) => {
          apiGetAllTerritories()
          setSelected(null)
        },
        error: (error) => {
          console.log(error)
        },
      })
    }
  }, [handleSaving, apiGetAllTerritories, selected])

  const apiDeleteAllTerritories = useCallback((company_id) => {
    handleSaving()
    TerritoriesApi.deleteAllTerritories(company_id, {
      success: (data) => {
        apiGetAllTerritories()
        setSelected(null)
      },
      error: (error) => {
        console.error(error)
      },
    })
  }, [handleSaving, apiGetAllTerritories])

  const apiUpdateTerritory = (territoryPolygon, territoryId) => {
    const { company_id } = props
    const updatedPaths = {
      territory: {
        paths: parseCoordinatesForPolygon(territoryPolygon),
      },
    }
    handleSaving()
    TerritoriesApi.postTerritory(company_id, updatedPaths, {
      success: (data) => {
        const postResponse = data.territory
        TerritoriesApi.deleteTerritory(territoryId, {
          success: (data) => {
            apiGetAllTerritories()
            setSelected(postResponse.id)
          },
          error: (error) => {
            console.log(error)
          },
        })
      },
      error: (error) => {
        console.error(error)
      },
    })
  }

  // Not currently being used, but we want to keep this;
  // might use it as we continue to fine-tune the Territories implementation
  const createRadius = (site) => {
    const { location } = site
    const circle = drawCircle(new window.google.maps.LatLng(location.lat, location.lng), 5)
    const paths = []
    forEach(circle, (coordinate) => {
      paths.push({
        lat: coordinate.lat(),
        lng: coordinate.lng(),
      })
    })
    const territory = {
      territory: {
        paths,
      },
    }
    apiPostTerritory(territory)
  }

  const handleBlur = () => {
    setFocus(false)
    setSelected(null)
  }

  const handleDelete = (e) => {
    if (e.altKey) {
      apiDeleteAllTerritories(props.company_id)
    } else {
      apiDeleteTerritory()
    }
  }

  const handleFocus = () => {
    setFocus(true)
    setSelected(null)
  }

  const handleClickOnPencil = () => {
    handleFocus()
    if (drawingMode === 'polygon') {
      setDrawingMode(null)
    } else {
      setDrawingMode('polygon')
    }
  }

  const handleClickOnPolygon = (e, id) => {
    handleFocus()
    setSelected(id)
  }

  const onPolygonComplete = (e) => {
    const overlay = e
    const territory = {
      territory: {
        paths: parseCoordinatesForPolygon(overlay),
      },
    }

    if (cancelDraw) {
      overlay.setMap(null)
    } else
    if (selected === null) { // creating a new territory
      nextOverlay = overlay
      apiPostTerritory(territory)
    }
    cancelDraw = false
    setDrawingMode(null)
  }

  useOnClickOutside(theMap, () => {
    if (drawingMode === 'polygon') {
      cancelDraw = true
      setDrawingMode(null)
    }
    handleBlur()
  })

  useEffect(() => {
    if (nextOverlay) {
      nextOverlay.setMap(null)
    }
  }, [territories])

  useEffect(() => {
    // use useGetAllTerritoriesCount to prevent zooming for every new territory drawn
    if (map && props.sites.length > 0 && useGetAllTerritoriesCount <= 1) {
      handleFitBounds()
    }
  }, [handleFitBounds, map, props.sites])

  useEffect(() => {
    const handleKeypress = (e) => {
      if (focus) {
        if (e.key === 'Backspace' && selected) {
          e.preventDefault()
          apiDeleteTerritory()
        }

        if (e.key === 'Escape') {
          e.preventDefault()
          if (drawingMode === 'polygon') {
            cancelDraw = true
            setDrawingMode(null)
          } else {
            setSelected(null)
          }
        }
      }
    }

    document.addEventListener('keyup', handleKeypress)
    return () => {
      document.removeEventListener('keyup', handleKeypress)
    }
  }, [apiDeleteTerritory, drawingMode, focus, selected])

  useEffect(() => {
    const fullscreenEvents = [
      'fullscreenchange',
      'webkitfullscreenchange',
      'mozfullscreenchange',
      'MSFullscreenChange',
    ]
    const handleFullScreenToggle = () => {
      const wrapper = document.scrollingElement || document.documentElement
      wrapper.scrollTo({
        top: (territoriesComponent.current.offsetTop + 120),
      })
    }
    fullscreenEvents.map(e => document.addEventListener(e, handleFullScreenToggle))
    return () => fullscreenEvents.map(e => document.removeEventListener(e, handleFullScreenToggle))
  }, [])

  return (
    <div className="configure-component" id="territoriesMap_in_companySettings" ref={territoriesComponent}>
      <h3 className="configure-section-heading">Territory</h3>
      <p className="territories-description">
        Use the Draw tool ( <i className="fa fa-pencil" />  ) to define where you’re able to receive Swoop jobs.
      </p>
      <div ref={theMap}>
        <SwoopMap
          id="territories-partner-map"
          onClick={handleFocus}
          onLoad={(newMap) => {
            setMap(newMap)
          }}
          options={{
            clickableIcons: false,
            fullscreenControl: true,
            gestureHandling: 'greedy',
            mapTypeControl: false,
            rotateControl: false,
            scaleControl: false,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
              position: google.maps.ControlPosition.RIGHT_BOTTOM,
            },
          }}
          zoom={12}
        >
          <DrawingManager
            drawingMode={drawingMode}
            onLoad={(drawingManager) => {
              window.google.maps.event.addListener(
                drawingManager, 'polygoncomplete', onPolygonComplete
              )
            }}
            onUnmount={(drawingManager) => {
              window.google.maps.event.clearInstanceListeners(drawingManager)
            }}
            options={{
              drawingControl: false,
              drawingControlOptions: {
                drawingModes: ['polygon'],
              },
              polygonOptions: {
                fillColor: '#FF0000',
                strokeColor: '#FF0000',
              },
            }}
          />

          { Array.isArray(props.sites) && props.sites.map(site => (
            <OverlayMarker
              icon="/assets/images/marker_blue_site.png"
              key={site.id}
              position={{
                lat: site.location.lat,
                lng: site.location.lng,
              }}
            >
              {site.name}
            </OverlayMarker>
          ))}

          { territories.map(territory => (
            <PolygonWithListeners
              editable={selected === territory.id}
              key={territory.id}
              onClick={e => handleClickOnPolygon(e, territory.id)}
              onInsert={polygon => apiUpdateTerritory(polygon, territory.id)}
              onRemove={polygon => apiUpdateTerritory(polygon, territory.id)}
              onSet={polygon => apiUpdateTerritory(polygon, territory.id)}
              options={selected === territory.id ? {
                fillColor: '#FF0000',
                strokeColor: '#FF0000',
              } : {
                fillColor: '#000000',
                strokeColor: '#000000',
              }}
              paths={territory.paths}
            />
          ))}

          <MapControls className="territories-ui" position="LEFT_TOP">
            <SearchBox />

            <div className="drawing-modes">
              <FontAwesomeButton
                className={classnames('draw-tool', { active: drawingMode === 'polygon' })}
                icon="fa-pencil"
                onClick={handleClickOnPencil}
                title="Draw territory"
              />
              <div className="divider" />
              <FontAwesomeButton
                className="delete-territories-button"
                icon="fa-trash"
                onClick={handleDelete}
                style={{
                  color: selected ? 'red' : '#333',
                  opacity: selected ? 1 : 0.25,
                }}
                title="Delete selected territory"
              />
            </div>
          </MapControls>

          {/* this section commented-out intentionally; do not delete, or see Matt */}
          {/* <MapControls className="territories-zoom-to-sites" position="RIGHT_BOTTOM">
            <FontAwesomeButton
              className="fit-bounds-on-markers"
              icon="fa-map-marker"
              onClick={handleFitBounds}
              title="Scale map to sites"
            />
          </MapControls> */}

          <MapControls className="saving-territories" position="RIGHT_TOP">
            <div className={saving ? 'saving' : undefined}>
              Saved!
            </div>
          </MapControls>
        </SwoopMap>
      </div>
    </div>
  )
}

const TerritoriesPartnerMap = StoreWrapper(Territories, function handleProps() {
  const { company_id } = UserStore.getUser()
  const sites = SiteStore.getUserSitesOrAll(this.getId(), ['name', 'location.lat', 'location.lng'])
  return {
    company_id,
    sites,
  }
})

export default TerritoriesPartnerMap
