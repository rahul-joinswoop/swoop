import React from 'react'
import BaseComponent from 'components/base_component'
import classnames from 'classnames'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import onClickOutside from 'react-onclickoutside'
import SiteStore from 'stores/site_store'
import StoreWrapper from 'StoreWrapper'
import TerritoriesApi from 'components/settings/configure/company_settings/TerritoriesMap/territories_api'
import UserStore from 'stores/user_store'
import { find, forEach, partition } from 'lodash'
import 'style.scss'

class Territories extends BaseComponent {
  constructor(props) {
    super(props)
    this.fullscreenEvents = [
      'fullscreenchange',
      'webkitfullscreenchange',
      'mozfullscreenchange',
      'MSFullscreenChange',
    ]
    this.message = React.createRef()
    this.refMap = React.createRef()
    this.searchInput = React.createRef()
    this.searchMarkers = []
    this.sites = []
    this.shapeOptions = {
      strokeColor: '#000000',
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: '#000000',
      fillOpacity: 0.35,
    }
    this.ui = React.createRef()
  }

  state = {
    cancelDraw: false,
    drawingMode: null,
    fitBoundsFired: false,
    overlays: [],
    paths: [],
    saving: false,
    search: false,
    selectedTerritoryId: null,
    territories: [],
  }

  componentDidMount() {
    this.initializeMap()
    document.addEventListener('keyup', this.handleKeypress)
    this.overlayComplete = window.google.maps.event.addListener(
      this.drawingManager, 'overlaycomplete', this.handleOverlayComplete
    )

    this.handleMessaging(null)
  }

  componentDidUpdate(prevProps, prevState) {
    this.handleMessaging(prevState)
    this.updateSiteMarkers()
  }

  componentWillUnmount() {
    document.removeEventListener('keyup', this.handleKeypress)
    window.google.maps.event.removeListener(this.overlayComplete)
  }

  apiDeleteAllTerritories = (company_id) => {
    TerritoriesApi.deleteAllTerritories(company_id, {
      success: (data) => {
        if (this.state.territories) {
          for (let i = 0; i < this.state.territories.length; i++) {
            const territory = this.state.territories[i]
            territory.overlay.setMap(null)
          }
          this.setState({
            territories: [],
          }, () => {
            this.displaySavingMessage()
            this.selectionClear()
          })
        }
      },
      error: (error) => {
        console.log(error)
      },
    })
  }

  apiDeleteTerritory = () => {
    if (this.state.selectedTerritoryId) {
      TerritoriesApi.deleteTerritory(this.state.selectedTerritoryId, {
        success: (data) => {
          const [selected, rest] = partition(this.state.territories, territory =>
            territory.id === this.state.selectedTerritoryId
          )
          selected[0].overlay.setMap(null)
          this.setState({
            territories: [...rest],
            selectedTerritoryId: null,
          }, () => {
            this.territory = null
            this.displaySavingMessage()
          })
        },
        error: (error) => {
          console.log(error)
        },
      })
    }
  }

  apiGetAllTerritories = (company_id) => {
    TerritoriesApi.getAllTerritories(company_id, {
      success: (data) => {
        const { territories } = data

        if (territories) {
          for (let i = 0; i < territories.length; i++) {
            const { id, paths } = territories[i]
            const overlay = new window.google.maps.Polygon({
              paths,
              ...this.shapeOptions,
            })
            overlay.setMap(this.map)

            window.google.maps.event.addListener(overlay, 'click', () => {
              this.selectionSet(overlay)
            })

            const territory = {
              id,
              paths,
              overlay,
            }

            this.setState(prevState => ({
              overlays: [...prevState.overlays, overlay],
              paths: [...prevState.paths, territories[i]],
              territories: [...prevState.territories, territory],
            }))
          }
        }
      },
      error: (error) => {
        console.log(error)
      },
    })
  }

  apiPostTerritory = (company_id, newTerritory, overlay) => {
    TerritoriesApi.postTerritory(company_id, newTerritory, {
      success: (data) => {
        const { territory } = data
        territory.overlay = overlay

        this.setState(prevState => ({
          territories: [...prevState.territories, territory],
        }), () => {
          this.displaySavingMessage()
          this.handleDrawingMode(null)

          window.google.maps.event.addListener(territory.overlay, 'click', () => {
            this.selectionSet(territory.overlay)
          })
          this.selectionSet(territory.overlay)
        })
      },
      error: (error) => {
        console.log(error)
        // TO-DO
        // 1. Display error message
        // 2. Remove shape from map
      },
    })
  }

  apiUpdateTerritory = () => {
    const { selectedTerritoryId } = this.state

    if (selectedTerritoryId) {
      const updatedPaths = {
        territory: {
          paths: this.parseCoordinatesForPolygon(this.territory),
        },
      }

      TerritoriesApi.postTerritory(this.props.company_id, updatedPaths, {
        success: (data) => {
          const [prevTerritory, rest] = partition(this.state.territories, territory =>
            territory.id === selectedTerritoryId
          )

          const nextTerritory = {
            id: data.territory.id,
            overlay: this.territory,
            paths: data.territory.paths,
          }

          this.setState({
            selectedTerritoryId: nextTerritory.id,
            territories: [...rest, nextTerritory],
          }, () => {
            this.displaySavingMessage()
            this.handleDrawingMode(null)
          })
        },
        error: (error) => {
          console.log(error)
        },
      })

      TerritoriesApi.deleteTerritory(selectedTerritoryId, {
        success: data => null,
        error: (error) => {
          console.log(error)
        },
      })
    }
  }

  compareSite = (prevSite, nextSite) => {
    const { lat, lng } = prevSite.getPosition()
    return (
      prevSite.title === nextSite.name &&
      lat().toFixed(6) === nextSite.location.lat.toFixed(6) &&
      lng().toFixed(6) === nextSite.location.lng.toFixed(6)
    )
  }

  createDrawManager = () => {
    this.drawingManager = new window.google.maps.drawing.DrawingManager({
      drawingMode: window.google.maps.drawing.OverlayType.HAND,
      drawingControl: false,
      drawingControlOptions: {
        drawingModes: ['polygon'],
        position: window.google.maps.ControlPosition.TOP_CENTER,
      },
      polygonOptions: {
        fillColor: '#FF0000',
        strokeColor: '#FF0000',
      },
    })
    this.drawingManager.setMap(this.map)

    this.map.controls[window.google.maps.ControlPosition.TOP_LEFT].push(this.message.current)
    this.map.controls[window.google.maps.ControlPosition.LEFT_CENTER].push(this.ui.current)
  }

  createSearchBox = () => {
    // Setup Search Box for Map
    // @ref: https://developers.google.com/maps/documentation/javascript/examples/places-searchbox
    const searchInput = this.searchInput.current
    const searchBox = new window.google.maps.places.SearchBox(searchInput)

    this.map.addListener('bounds_changed', () => {
      searchBox.setBounds(this.map.getBounds())
    })

    searchBox.addListener('places_changed', () => {
      const places = searchBox.getPlaces()

      if (places.length === 0) {
        return
      }

      // Clear out the old markers.
      console.log('searchMarkers', this.searchMarkers)
      this.searchMarkers.forEach((marker) => {
        marker.setMap(null)
      })
      this.searchMarkers = []

      // For each place, get the icon, name and location.
      const bounds = new window.google.maps.LatLngBounds()
      places.forEach((place) => {
        if (!place.geometry) {
          console.log('Returned place contains no geometry')
          return
        }

        // Create a marker for each place.
        this.searchMarkers.push(new window.google.maps.Marker({
          map: this.map,
          title: place.name,
          position: place.geometry.location,
        }))

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport)
        } else {
          bounds.extend(place.geometry.location)
        }
      })

      this.map.fitBounds(bounds)
    })
  }

  createSiteMarker = (site) => {
    const { name } = site
    const { lat, lng } = site.location

    const infoWindow = new window.google.maps.InfoWindow({
      content: `<span className="territories-site-name">${name}</span>`,
      hideCloseButton: true,
    })

    const Marker = new window.google.maps.Marker({
      icon: '/assets/images/marker_blue_site.png',
      map: this.map,
      position: { lat, lng },
      title: name,
    })

    Marker.addListener('mouseover', () => infoWindow.open(this.map, Marker))
    Marker.addListener('mouseout', () => infoWindow.close(this.map, Marker))

    return Marker
  }

  displaySavingMessage = () => {
    this.setState({
      saving: true,
    }, () => {
      setTimeout(() => {
        this.setState({
          saving: false,
        })
      }, 1500)
    })
  }

  initializeMap = () => {
    this.map = new window.google.maps.Map(this.refMap.current, {
      center: { lat: 37.773972, lng: -122.431297 },
      zoom: 12,

      fullscreenControl: true,
      mapTypeControl: false,
      rotateControl: false,
      scaleControl: false,
      streetViewControl: false,
      zoomControl: true,
    })

    this.map.addListener('click', this.handleFocus)

    this.apiGetAllTerritories(this.props.company_id)
    this.createDrawManager()
    this.createSearchBox()
  }

  handleClickOutside = () => {
    if (this.state.drawingMode === 'polygon') {
      this.setState({
        cancelDraw: true,
      })
      this.handleDrawingMode(null)
    } else {
      this.selectionClear()
    }
    this.setState({
      isInFocus: false,
    })
  }

  handleDrawingMode = (drawingMode) => {
    this.drawingManager.setDrawingMode(drawingMode)
    this.handleFocus()
    this.setState({
      drawingMode,
    }, () => {
      if (this.state.drawingMode !== null) {
        this.selectionClear()
      }
    })
  }

  handleDelete = (e) => {
    if (e.altKey) {
      this.apiDeleteAllTerritories(this.props.company_id)
    } else {
      this.apiDeleteTerritory()
    }
  }

  handleFitBounds = () => {
    if (this.sites.length === 1) {
      this.map.setCenter(
        new window.google.maps.LatLng(
          this.sites[0].getPosition().lat(), this.sites[0].getPosition().lng()
        ), 12
      )
    } else
    if (this.sites.length > 1) {
      const bounds = new window.google.maps.LatLngBounds()
      forEach(this.sites, site => bounds.extend(site.getPosition()))
      this.map.fitBounds(bounds)
    }
  }

  handleFocus = () => {
    this.setState({
      isInFocus: true,
    })
  }

  handleKeypress = (e) => {
    if (this.state.isInFocus) {
      if (e.key === 'Backspace' && this.state.selectedTerritoryId) {
        e.preventDefault()
        this.apiDeleteTerritory()
      }

      if (e.key === 'Escape') {
        e.preventDefault()
        if (this.state.drawingMode === 'polygon') {
          this.setState({
            cancelDraw: true,
          })
          this.handleDrawingMode(null)
        } else {
          this.selectionClear()
        }
      }
    }
  }

  handleMessaging = (prevState) => {
    if (prevState) {
      if (
        this.state.drawingMode !== prevState.drawingMode ||
        this.state.overlays.length !== prevState.overlays.length ||
        this.state.selectedTerritoryId !== prevState.selectedTerritoryId
      ) {
        if (this.state.drawingMode === 'polygon') {
          // Drawing a new shape
          this.setState({
            message: 'Click to make points on the map to create a new shape.',
          })
        } else
        if (this.state.selectedTerritoryId && this.state.drawingMode === null) {
          // Editing an existing shape
          this.setState({
            message: 'Drag the white points to adjust your shape.',
          })
        } else
        if (this.state.drawingMode === null) {
          if (this.state.overlays.length === 0) {
            // Idle; no territories
            this.setState({
              message: 'Draw the eligible area where you\'re able to receive Swoop jobs.',
            })
          } else {
            // Idle; has territories
            this.setState({
              message: 'Draw the eligible area where you\'re able to receive Swoop jobs.',
            })
          }
        }
      }
    } else {
      if (this.state.overlays.length === 0) {
        // Initial; no territories
        this.setState({
          message: 'Draw the eligible area where you\'re able to receive Swoop jobs.',
        })
      } else {
        // Initial; has territories
        this.setState({
          message: 'Draw the eligible area where you\'re able to receive Swoop jobs.',
        })
      }
    }
  }

  handleOverlayComplete = (e) => {
    const { overlay } = e
    const territory = {
      territory: {
        paths: this.parseCoordinatesForPolygon(overlay),
      },
    }

    if (this.state.cancelDraw) {
      overlay.setMap(null)
    } else
    if ( // is creating a new territory
      !this.state.selectedTerritoryId
    ) {
      this.apiPostTerritory(this.props.company_id, territory, overlay)
    }

    this.setState({
      cancelDraw: false,
    })
  }

  handleToggleSearch = () => {
    this.setState(prevState => ({
      search: !prevState.search,
    }), () => {
      if (this.state.search) {
        this.searchInput.current.focus()
      }
    })
  }

  parseCoordinatesForPolygon = (polygon) => {
    const path = polygon.getPath()
    const coordinates = []
    for (let i = 0; i < path.length; i++) {
      const coord = {
        lat: path.getAt(i).lat(),
        lng: path.getAt(i).lng(),
      }
      coordinates.push(coord)
    }
    return coordinates
  }

  selectionAddEventListeners = () => {
    const { territory } = this
    this.insertAt = window.google.maps.event.addListener(territory.getPath(), 'insert_at', this.apiUpdateTerritory)
    this.removeAt = window.google.maps.event.addListener(territory.getPath(), 'remove_at', this.apiUpdateTerritory)
    this.setAt = window.google.maps.event.addListener(territory.getPath(), 'set_at', this.apiUpdateTerritory)
  }

  selectionClear = () => {
    this.insertAt && window.google.maps.event.removeListener(this.insertAt)
    this.removeAt && window.google.maps.event.removeListener(this.removeAt)
    this.setAt && window.google.maps.event.removeListener(this.setAt)

    if (this.state.selectedTerritoryId) {
      this.territory.setOptions({
        editable: false,
        fillColor: '#000000',
        strokeColor: '#000000',
      })

      this.setState({
        selectedTerritoryId: null,
      }, () => {
        this.territory = null
      })
    }
  }

  getTerritoryId = (state, overlay) => find(state.territories, territory => territory.overlay === overlay).id

  selectionSet = (overlay) => {
    this.selectionClear()

    overlay.setOptions({
      editable: true,
      fillColor: '#FF0000',
      strokeColor: '#FF0000',
    })

    this.setState(prevState => ({
      isInFocus: true,
      selectedTerritoryId: this.getTerritoryId(prevState, overlay),
    }), () => {
      this.territory = overlay
      this.selectionAddEventListeners()
    })
  }

  updateSiteMarkers = () => {
    const prevSites = this.sites
    const nextSites = this.props.sites

    const [nextSame, nextDiff] = partition(nextSites, nextSite =>
      find(prevSites, prevSite => this.compareSite(prevSite, nextSite))
    )

    const [prevSame, prevDiff] = partition(prevSites, prevSite =>
      find(nextSites, nextSite => this.compareSite(prevSite, nextSite))
    )

    const newSites = []
    forEach(nextDiff, newSite => newSites.push(this.createSiteMarker(newSite)))

    forEach(prevDiff, (obsoleteSite) => {
      obsoleteSite.setMap(null)
    })

    this.sites = [...prevSame, ...newSites]

    // On mount, scale map to fit all markers;
    // not in componentDidMount b/c we need to wait for sites to populate
    if (this.state.fitBoundsFired === false) {
      this.setState({
        fitBoundsFired: true,
      }, () => {
        setTimeout(() => {
          this.handleFitBounds()
        }, 1000)
      })
    }
  }

  render() {
    return (
      <div className="configure-component" id="territoriesMap_in_companySettings">
        <h3 className="configure-section-heading">Territory</h3>
        <p className="territories-description">Use the Draw Tool ( <i className="fa fa-pencil" />  ) to define the eligible area where you’re able to receive Swoop jobs. Connect points on the map to create a complete shape of your desired territory.</p>
        <div
          className={classnames('territories-message', {
            searchMap: this.state.search,
            saving: this.state.saving,
          })}
          ref={this.message}
        >
          <input id="search-territories-map" placeholder="Find Location" ref={this.searchInput} type="text" />
          <p className="message">{this.state.message}</p>
          <p className="saving-message"><span className="fa fa-check" /> Saved!</p>
          <div className="vertical-divider" />
          <FontAwesomeButton
            className="territories-search-button"
            icon={this.state.search ? 'fa-times' : 'fa-search'}
            onClick={this.handleToggleSearch}
            title={this.state.search ? 'Hide search' : 'Search location'}
          />
        </div>

        <div className="territories-ui" ref={this.ui}>
          <div className="drawing-modes">
            <FontAwesomeButton
              className={classnames('hand-tool', { active: this.state.drawingMode === null })}
              icon="fa-hand-paper-o"
              onClick={() => this.handleDrawingMode(null)}
              title="Interact with map"
            />
            <div className="divider" />
            <FontAwesomeButton
              className={classnames('draw-tool', { active: this.state.drawingMode === 'polygon' })}
              icon="fa-pencil"
              onClick={() => this.handleDrawingMode('polygon')}
              title="Draw territory"
            />
          </div>

          <FontAwesomeButton
            className="fit-bounds-on-markers"
            icon="fa-map-marker"
            onClick={this.handleFitBounds}
            title="Scale map to sites"
          />

          <FontAwesomeButton
            className="delete-territories-button"
            icon="fa-eraser"
            onClick={this.handleDelete}
            style={{
              display: this.state.selectedTerritoryId ? 'block' : 'none',
            }}
            title="Delete selected territory"
          />

        </div>

        <div id="territories-map" ref={this.refMap} />
      </div>
    )
  }
}

const TerritoriesMap = StoreWrapper(onClickOutside(Territories), function handleProps() {
  const { company_id } = UserStore.getUser()
  const sites = SiteStore.getUserSitesOrAll(this.getId(), ['name', 'location.lat', 'location.lng'])
  return {
    company_id,
    sites,
  }
})

export default TerritoriesMap
