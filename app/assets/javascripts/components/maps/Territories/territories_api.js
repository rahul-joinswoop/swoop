import Api from 'api'

const TerritoriesApi = {

  deleteAllTerritories(companyId, response) {
    Api.request(Api.DELETE, `partner/companies/${companyId}/territories/destroy_all`, null, response, {
      ajaxOptions: { dataType: 'text' },
    })
  },

  deleteTerritory(territoryId, response) {
    Api.request(Api.DELETE, `partner/territories/${territoryId}`, null, response, {
      ajaxOptions: { dataType: 'text' },
    })
  },

  getAllTerritories(companyId, response) {
    Api.request(Api.GET, `partner/companies/${companyId}/territories`, null, response)
  },

  postTerritory(companyId, territory, response) {
    Api.request(Api.POST, `partner/companies/${companyId}/territories`, territory, response)
  },

  getAllTerritoriesInNetwork(response) {
    console.log('Endpoint not ready yet!')
    // Api.request(Api.GET, `territories`, null, response)
  },

}

export default TerritoriesApi
