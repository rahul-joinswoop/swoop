import React, { useEffect, useRef, useState } from 'react'
import classnames from 'classnames'
import FontAwesomeButton from 'componentLibrary/FontAwesomeButton'
import { fitBounds } from 'components/maps/Utils'
import { StandaloneSearchBox, useGoogleMap } from '@react-google-maps/api'
import { useToggle } from 'componentLibrary/Hooks'
import { forEach } from 'lodash'
import './style.scss'

const SearchBox = () => {
  const map = useGoogleMap()
  const input = useRef()
  const [locations, setLocations] = useState([])
  const [searchBox, setSearchBox] = useState(null)
  const [expanded, toggleExpanded] = useToggle(false)

  const handleSearch = () => {
    const places = searchBox.getPlaces()
    if (places.length > 0) {
      forEach(places, (place) => {
        if (!place.geometry) {
          console.log('Returned place contains no geometry')
          return
        }

        setLocations([{
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng(),
          viewport: place.geometry.viewport,
        }])
      })
    }
  }

  useEffect(() => {
    if (expanded && input) {
      input.current.focus()
    }
  }, [expanded, input])

  useEffect(() => {
    input.current.value = ''
  }, [expanded])

  useEffect(() => {
    if (map && locations.length > 0) {
      fitBounds(map, locations)
    }
  }, [locations, map])

  return (
    <div className={classnames('search-box', { expanded })}>
      <StandaloneSearchBox
        onLoad={ref => setSearchBox(ref)}
        onPlacesChanged={handleSearch}
      >
        <>
          <input
            placeholder="Search"
            ref={input}
            type="text"
          />
          <FontAwesomeButton
            icon={expanded ? 'fa-times' : 'fa-search'}
            onClick={toggleExpanded}
            title={expanded ? 'Collapse Search' : 'Search'}
          />
        </>
      </StandaloneSearchBox>
    </div>
  )
}

export default SearchBox
