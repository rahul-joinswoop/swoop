import React from 'react'
import BaseComponent from 'components/base_component'
import onClickOutside from 'react-onclickoutside'
import StoreWrapper from 'StoreWrapper'
import TerritoriesApi from 'components/maps/Territories/territories_api'
import './style.scss'

let bounds = null

function drawCircle(point, radius) {
  const d2r = Math.PI / 180   // degrees to radians
  const r2d = 180 / Math.PI   // radians to degrees
  const earthsradius = 3963 // 3963 is the radius of the earth in miles

  const points = 16

  // find the raidus in lat/lon
  const rlat = (radius / earthsradius) * r2d
  const rlng = rlat / Math.cos(point.lat() * d2r)

  const extp = []
  for (let i = 0; i < points + 1; i++) { // one extra here makes sure we connect the
    const theta = Math.PI * (i / (points / 2))
    const ey = point.lng() + (rlng * Math.cos(theta)) // center a + radius x * cos(theta)
    const ex = point.lat() + (rlat * Math.sin(theta)) // center b + radius y * sin(theta)
    extp.push(new window.google.maps.LatLng(ex, ey))
    bounds.extend(extp[extp.length - 1])
  }

  return extp
}

class Territories extends BaseComponent {
  constructor(props) {
    super(props)
    this.refMap = React.createRef()
    this.searchInput = React.createRef()
    this.searchMarkers = []
    this.shapeOptions = {
      clickable: false,
      fillColor: '#000000',
      fillOpacity: 0.35,
      strokeColor: '#000000',
      strokeOpacity: 0.8,
      strokeWeight: 3,
    }
    this.territories = []
  }

  state = {
    markerPosition: {},
    polygonCount: 0,
    territoriesAtPosition: [],
  }

  componentDidMount() {
    this.initializeMap()
  }

  addMarker = (position, map) => {
    if (this.marker) {
      this.marker.setMap(null)
    }
    this.marker = new window.google.maps.Marker({
      position,
      map,
    })
    this.setState({
      markerPosition: this.marker.getPosition(),
      territoriesAtPosition: [],
    }, () => {
      this.queryTerritoriesHere()
    })
  }

  addMarkerListener = (el) => {
    window.google.maps.event.addListener(el, 'click', (event) => {
      this.addMarker(event.latLng, this.map)
    })
  }

  queryTerritoriesHere = () => {
    this.territories.forEach((territory) => {
      if (window.google.maps.geometry.poly.containsLocation(this.state.markerPosition, territory)) {
        this.setState(prevState => ({
          territoriesAtPosition: [...prevState.territoriesAtPosition, 'placeholder'],
        }))
      }
    })
  }

  initializeMap = () => {
    this.map = new window.google.maps.Map(this.refMap.current, {
      center: { lat: 37.773972, lng: -122.431297 },
      clickableIcons: false,
      draggableCursor: 'crosshair',
      zoom: 5,

      fullscreenControl: true,
      mapTypeControl: false,
      rotateControl: false,
      scaleControl: false,
      streetViewControl: false,
      zoomControl: true,
    })

    this.addMarkerListener(this.map)
    this.createSearchBox()

    const centers = [
      { lat: 37.773972, lng: -122.431297 },
      { lat: 37.5665, lng: 126.9780 },
      { lat: 13.7563, lng: 100.5018 },
      { lat: 10.8231, lng: 106.6297 },
      { lat: 12.0499, lng: 120.1782 },
      { lat: -37.8136, lng: 144.9631 },
      { lat: 22.3193, lng: 114.1694 },
      { lat: 47.8864, lng: 106.9057 },
      { lat: 31.2304, lng: 121.4737 },
      { lat: 13.3550, lng: 103.8552 },
      { lat: 43.7696, lng: 11.25580 },
    ]
    centers.forEach((location) => {
      this.createCircles(location)
    })

    // ----
    // Waiting for BE support ...
    // ----
      // TerritoriesApi.getAllTerritories({
      //   success: (data) => {
      //     console.log('Success', data)
      //   },
      //   error: (error) => {
      //     console.log(error)
      //   },
      // })
    // ----
  }

  createCircles = (location) => {
    bounds = new window.google.maps.LatLngBounds()

    const bigCircle = drawCircle(new window.google.maps.LatLng(location.lat, location.lng), 500)
    const circles = []
    for (let i = 0; i < bigCircle.length - 1; i++) {
      const Circle = drawCircle(bigCircle[i], 40)
      circles.push(new window.google.maps.Polygon({
        ...this.shapeOptions,
        paths: drawCircle(Circle[i], 10),
      }))
      circles[circles.length - 1].setMap(this.map)
      this.territories.push(circles[circles.length - 1])

      for (let j = 0; j < Circle.length - 1; j++) {
        circles.push(new window.google.maps.Polygon({
          ...this.shapeOptions,
          paths: drawCircle(Circle[j], 5),
        }))
        circles[circles.length - 1].setMap(this.map)
        this.territories.push(circles[circles.length - 1])
      }
    }

    const biggerCircle = drawCircle(new window.google.maps.LatLng(location.lat, location.lng), 200)
    const moreCircles = []
    for (let i = 0; i < biggerCircle.length - 1; i++) {
      moreCircles.push(new window.google.maps.Polygon({
        ...this.shapeOptions,
        paths: drawCircle(biggerCircle[i], 10),
      }))
      moreCircles[i].setMap(this.map)
      this.territories.push(moreCircles[i])
    }

    this.setState(prevState => ({
      polygonCount: this.territories.length,
    }))

    // map.fitBounds(bounds)
  }

  createSearchBox = () => {
    const searchInput = this.searchInput.current
    const searchBox = new window.google.maps.places.SearchBox(searchInput)

    this.map.addListener('bounds_changed', () => {
      searchBox.setBounds(this.map.getBounds())
    })

    searchBox.addListener('places_changed', () => {
      const places = searchBox.getPlaces()

      if (places.length === 0) {
        return
      }

      // Clear out the old markers.
      // this.searchMarkers.forEach((marker) => {
      //   marker.setMap(null)
      // })
      // this.searchMarkers = []

      // For each place, get the icon, name and location.
      const searchBounds = new window.google.maps.LatLngBounds()
      places.forEach((place) => {
        if (!place.geometry) {
          console.log('Returned place contains no geometry')
          return
        }

        // Create a marker for each place.
        this.searchMarkers.push(new window.google.maps.Marker({
          map: this.map,
          title: place.name,
          position: place.geometry.location,
        }))

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          searchBounds.union(place.geometry.viewport)
        } else {
          searchBounds.extend(place.geometry.location)
        }
        console.log(place.geometry.location)
        this.createCircles({
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng(),
        })
      })

      this.map.fitBounds(searchBounds)
      this.map.setZoom(5)
    })
  }

  render() {
    return (
      <div className="component-territories-network-map">

        <main ref={this.refMap} />

        <aside>
          <input id="search-territories-map" placeholder="Find Location" ref={this.searchInput} type="text" />
          <p
            style={{
              margin: 12,
            }}
          >
            <span style={{ fontWeight: 'bold' }}># of Polygons</span>: <span style={{ color: 'red' }}>{this.state.polygonCount}</span>
          </p>

          <p
            style={{
              margin: 12,
            }}
          >
            <span style={{ fontWeight: 'bold' }}># of Territories @ Marker</span>: <span style={{ color: 'red' }}>{this.state.territoriesAtPosition.length}</span>
          </p>

        </aside>

      </div>
    )
  }
}

const TerritoriesNetworkMap = StoreWrapper(onClickOutside(Territories), () => ({
}))

export default TerritoriesNetworkMap
