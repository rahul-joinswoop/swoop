import Logger from 'lib/swoop/logger'
import setupMapPadding from 'setupMapPadding'
import React from 'react'
setupMapPadding()
SlidingMarker = require('marker-animate-unobtrusive')
UserStore = require('stores/user_store')
SiteStore = require('stores/site_store').default
PoiStore = require('stores/poi_store').default
MapStore = require ('stores/map_store')
VehicleStore = require('stores/vehicle_store')
import { difference, extend, filter, forEach, isInteger, isObject, keys, map, partial, values } from 'lodash'
import Utils from 'utils'
import moment from 'moment-timezone'
import MapContext from 'components/maps/MapContext'

import Consts from 'consts'
#TODO: need to make this into a singleton
BaseComponent = require('components/base_component')

gCircles = {}
gMarkers = {}
gPopups = {}
truckMarkers = {}
myMarker = null
truckInfo = {}
addressInfo = {}
gDirections = {}
sitesAndPlacesMarkers = {}


class SwoopMapClass extends BaseComponent
  constructor: (props) ->
    super(props)
    @state = @getStartState()
    @state.rev = 0
  getStartState: ->
    @boundSet = false
    @bounds = null
    @zoom = null
    @map = null

    gCircles = {}
    gMarkers = {}
    gPopups = {}
    truckMarkers = {}
    myMarker = null
    truckInfo = {}
    addressInfo = {}
    gDirections = {}

    @testOffset = {x: 0, y: 0}
    @infowindowListeners = {}

    #FOR DETERMINING IF NEEDING TO UPDATE
    @checkObj = null
    return {
      staticMap: false,
      map: null,
    }

  reinitialize: =>
    if @state.staticMap
      @setState @getStartState()
      #wait for render to complete so map container is available
      setTimeout(@componentDidMount.bind(@),100)

  componentDidMount: ->
    super(arguments...)

    if @boundFunc
      MapStore.unbind(MapStore.MAP_STOLEN, @boundFunc)

    @map = @createMap(@props)
    @setState(map: @map)

    sitesAndPlacesMarkers = {}

    @addCandidatesToMap()

    @updateMapComponents(@props)

    #HACK TO GET MAP TO ALWAYS SHOW UP INSTEAD OF GRAY
    google?.maps.event.addListenerOnce(@map, 'idle', =>
      if @map? and !@props.disableSetBounds
        google.maps.event.trigger(@map, 'resize')
        @setBounds(@props)
    )

    @boundFunc = partial(@unloadMap, @getId())
    MapStore.bind(MapStore.MAP_STOLEN, @boundFunc)
    if @props?.setMapRef?
      @props.setMapRef(@)

  areMarkersDifferent: (props, props2) =>
    if props?.routes and props2?.routes
      for route, index in props.routes
        if props.routes[index]?.id != props2.routes[index]?.id || props.routes[index]?.origin?.lat != props2.routes[index]?.origin?.lat || props.routes[index]?.dest?.lat != props2.routes[index]?.dest?.lat
          return true
      return false

    if !props?.routes? and !props2?.routes?
      return false

    return true

  updateMapComponents: (props, check) =>
    #TODO: Check to make sure difference is actually working here
    for truck in difference(@props.trucks, props.trucks)
      if truck?
        VehicleStore.unbind(VehicleStore.CHANGE+"_"+truck, @vehiclesChanged)
      if truckMarkers? and truckMarkers[truck]?
        truckMarkers[truck].setMap(null)
        delete truckMarkers[truck]


    for truck in difference(props.trucks, @props.trucks)
      if truck
        VehicleStore.bind(VehicleStore.CHANGE+"_"+truck, @vehiclesChanged)

    Logger.debug("Creating props: ", props)

    if props.showMeAt? and props.showMeAt.lat and props.showMeAt.lng
      latlng = new google?.maps.LatLng(props.showMeAt.lat, props.showMeAt.lng)
      if myMarker?
        myMarker.setPosition(latlng)
      else
        myMarker = @createMyMarker(latlng)
        myMarker.setPosition(latlng)


    @updateTrucks(props)

    #only do these things if the markers have changed:
    if !check? || @areMarkersDifferent(props, @props)
      #@checkObj = JSON.parse(JSON.stringify(props))
      @updateMarkers(props)

      if props.routes?
        setTimeout(=>
          @setBounds(props)
        , 100)

    if @props.showInfoWindow? && !@props.showInfoWindowAlways
      id = @props.showInfoWindow
      if truckInfo[id]
        truckInfo[id].close()

    if props.showInfoWindow?
      id = props.showInfoWindow
      @createInfoWindowFromId(id)
      if truckInfo[id]
        truckMarkers?[id]?.setZIndex google.maps.Marker.MAX_ZINDEX + 1
        truckInfo[id].close()
        truckInfo[id].open @map, marker
        truckInfo[id].setZIndex google.maps.Marker.MAX_ZINDEX + 1


  removeMapComponents: (props) =>
    MapStore.cleanup()

    gCircles = {}
    gMarkers = {}
    gPopups = {}
    gDirections = {}
    @infowindowListeners = {}

    if props?.listeners?
      for listener, callback of props.listeners
        google?.maps.event.removeListener(@map, listener, callback)

  setZoom: (zoom) =>
    if @map? and zoom?
      @map.setZoom(zoom)

  setOptions: (opts) =>
    @map.setOptions(opts)

  setCenter: (lat, lng) =>
    if @map? and lat? and lng?
      @map.panTo(new google?.maps.LatLng(lat, lng))

  setCenterWithPixelOffset: (lat, lng, xPixelOffset, yPixelOffset) =>
    @setCenter(lat, lng)
    @map.panBy((if isInteger(xPixelOffset) then xPixelOffset else 0), (if isInteger(yPixelOffset) then yPixelOffset else 0))

  getCenter: =>
    @map?.getCenter()

  unloadMap: =>
    @setState
      staticMap: true

  UNSAFE_componentWillReceiveProps: (props) ->
    if !@state.staticMap
      @updateMapComponents(props, true)

  componentWillUnmount: ->
    super(arguments...)

    MapStore.unbind(MapStore.MAP_STOLEN, @boundFunc)
    if @props.trucks?
      for truck in @props.trucks
        if truck?
          VehicleStore.unbind(VehicleStore.CHANGE+"_"+truck, @vehiclesChanged)

    sitesAndPlacesMarkers = {}

  vehiclesChanged: =>
    if !@state.staticMap
      @updateTrucks(@props)

  createAddrInfoWindowFromId: (id, address) =>
    addrMarker = gMarkers[id]
    if addrMarker? and @props.showAddressNames and not addressInfo[id]?
      infoWindow = MapStore.createInfoWindow({content: address}, false)
      addrMarker.addListener('click', =>
        if @isInfoWindowOpen(infoWindow)
          infoWindow.close()
        else
          infoWindow.open(@map, addrMarker)
      )
      addressInfo[id] = infoWindow

  createInfoWindowFromId: (id) =>
    truckMarker = truckMarkers[id]
    if truckMarker?
      vehicleName = VehicleStore.get(id, 'name', @getId())
      if @props.showTruckNames && !truckInfo[id]?
        truckInfo[id] = @createTruckInfoWindow(id, truckMarker, vehicleName)

  moveTrucks: (x, y) =>
    @testOffset.x += x
    @testOffset.y += y
    @updateTrucks(@props)


  placeTruck: (truckMarker, vehicle) =>
    if truckMarker?
      if vehicle?
        lat = vehicle.lat+@testOffset.x
        lng = vehicle.lng+@testOffset.y
        if !isNaN(lat) and !isNaN(lng)
          latlng = new google?.maps.LatLng(lat, lng)
          truckMarker.setIcon(@createTruckImage())
          truckMarker.setPosition(latlng)
          truckMarker.setVisible(true)
          return

      truckMarker.setVisible(false)

  rerenderInfoWindows: () =>
    for id, info of truckInfo
      if @props.truckInfoWindow
        window.info = info
        info.setContent(@props.truckInfoWindow(id))

  updateTrucks: (props = @props) =>
    Logger.debug("in update truck with: ", props)
    if props.trucks?

      for id in props.trucks
        if not id?
          continue
        vehicleLocationUpdatedAt = VehicleStore.get(id, 'location_updated_at', @getId())
        if vehicleLocationUpdatedAt?
          if @props.hideOldTrucks and moment().diff(vehicleLocationUpdatedAt) > 1000*60*30
            continue

        if id? and not truckMarkers[id]?
          truckMarkers[id] = @createTruckMarker(id)
        @createInfoWindowFromId(id)
        truckMarker = truckMarkers[id]

        @placeTruck(truckMarker, {
          lat: VehicleStore.get(id, 'lat', @getId()),
          lng: VehicleStore.get(id, 'lng', @getId())
        })

  addCandidatesToMap: =>
    if @props.candidates
      for candidate in @props.candidates
        marker=@createTruckMarker(candidate.vehicle.id)
        info=@createInfoWindowFromId(candidate.vehicle.id)
        @placeTruck(marker, candidate.vehicle)

  # Adds sites and places as markers in the map
  addSitesToMap: =>
    # using this prop to not load any site/place when job form loads as required by ENG-4610
    # (originally IT IS used as a threshold, just remove this conditional to bring back the original threshold behaviour)
    if @props.useSitesAndPlacesTreshold
      return

    newSitesIds = []
    newPlacesIds = []

    if @props.sites
      # we only add new sites fetched by the store
      newSitesIds = difference(map(@props.sites, 'id'), map(keys(sitesAndPlacesMarkers), Number))
      newSitesIds = filter(newSitesIds, (siteId) => SiteStore.get(siteId, 'location')?.address?)

    if @props.poiSites
      # we only add new places fetched by the store
      newPlacesIds = difference(map(@props.poiSites, 'id'), map(keys(sitesAndPlacesMarkers), Number))
      newPlacesIds = filter(newPlacesIds, (placeId) => PoiStore.get(placeId, 'location')?.address?)

    # it means it has reached the threshold limit, no more sites or places allowed on the map (will likely happen on collection updates on the fly)
    if @props.useSitesAndPlacesTreshold && keys(sitesAndPlacesMarkers).length >= Consts.SITES_AND_PLACES_LOAD_THRESHOLD
      sitesIdsCollection  = []
      placesIdsCollection = []
    else if @props.useSitesAndPlacesTreshold # it means there's a threshold but it hasn't reached its limit; we can add sites/places to the map until reaching the threshold's limit
      balanced_fraction = @balancedFractionOfSitesAndPlacesBasedOnThreshold(newSitesIds, newPlacesIds)

      sitesIdsCollection  = newSitesIds.slice(0, Utils.toFixed(balanced_fraction['sites'], 0))
      placesIdsCollection = newPlacesIds.slice(0, Utils.toFixed(balanced_fraction['places'], 0))
    else # it means it is not limited by a threshold; all sites and places can be added to the map
      sitesIdsCollection = newSitesIds
      placesIdsCollection = newPlacesIds

    for siteId in sitesIdsCollection
      if not sitesAndPlacesMarkers[siteId]
        @createSiteMarkerOnMap(siteId, '/assets/images/marker_blue_site.png', SiteStore, 'Site')

    for placeId in placesIdsCollection
      if not sitesAndPlacesMarkers[placeId]
        @createSiteMarkerOnMap(placeId, '/assets/images/marker_yellow.png', PoiStore, 'Place')

  createSiteMarkerOnMap: (siteOrPlaceId, imageUrl, store, type) =>
      siteOrPlaceMarker = MapStore.createMarker(
        icon:
          url: imageUrl
      )

      infowindow = MapStore.createInfoWindow()
      siteOrPlaceName = store.get(siteOrPlaceId, 'name')

      google.maps.event.addListener siteOrPlaceMarker, 'mouseover', do (marker = siteOrPlaceMarker, name = siteOrPlaceName, infowindow) ->
        ->
          infowindow.setContent name
          infowindow.open @map, marker
          return

      google.maps.event.addListener siteOrPlaceMarker, 'mouseout', do (infowindow) ->
        ->
          infowindow.close()
          return

      google.maps.event.addListener siteOrPlaceMarker, 'click', do (marker = siteOrPlaceMarker, id = siteOrPlaceId) =>
        =>
          @props?.onClick?(marker.position.lat(), marker.position.lng(), {id: id})
          return

      google.maps.event.addListener(infowindow, 'domready', () ->
        $(".gm-style-iw").parent().addClass('info-window-container-without-close-x')
      )

      siteOrPlaceMarker.setPosition(
        lat: store.get(siteOrPlaceId, 'location').lat
        lng: store.get(siteOrPlaceId, 'location').lng
      )
      siteOrPlaceMarker.setVisible(true)

      sitesAndPlacesMarkers[siteOrPlaceId] = siteOrPlaceMarker

  updateMarkers: (props) =>
    for id, marker of gMarkers
      marker.setVisible(false)
    for id, direction of gDirections
      direction.setMap(null)
    for id, popup of gPopups
      popup.setMap(null)
      delete gPopups[id]

    Logger.debug("SETTING UP MARKERS FOR ", props.routes)

    if props.routes?
      for route in props.routes
        Logger.debug("SETTING UP MARKERS FOR 1", route)
        if not route.id?
          continue
        Logger.debug("SETTING UP MARKERS FOR 2", route)

        @plotLocation(
          props,
          route?.id+'o',
          route?.origin,
          route?.origin?.label,
          route?.originMarker,
          '#42B756',
          'A',
          'Pickup Zone'
        )

        @plotLocation(
          props,
          route?.id+'d',
          route?.dest,
          route?.dest?.label,
          route?.destMarker,
          '#F00',
          'B',
          'Drop Off Zone'
        )

        if !(UserStore.isPartner() && props.autoAssigning)
          @updateDirections(route)

        @hideShowSiteMarker()

  # This will guarantee we'll have both sites and places on the map when using a threshold.
  # Otherwise we could end up with sites only if the number of sites is equal o higher than the threshold
  balancedFractionOfSitesAndPlacesBasedOnThreshold: (sitesIds, placesIds) ->
    fraction = Consts.SITES_AND_PLACES_LOAD_THRESHOLD / (sitesIds.length + placesIds.length)

    return {
      sites:  sitesIds.length  * fraction
      places: placesIds.length * fraction
    }

  getLastDistanceInMiles: () => @_distance

  showDistanceOnDropLocation: (route, destMarker) =>
    origin = route.origin
    dest = route.dest
    if ! @props.showDistanceOnDropLocation || ! @getLocationPoint(origin)
      @removeInfowindowListener(['mouseOverShowDistance', 'mouseOutHideDistance', 'domReadyApplyClass'])
      return

    originLatLng = @getLocationPoint(origin)
    destLatLng   = @getLocationPoint(dest)

    currentRouteDistance = MapStore.getCurrentRouteDistance(gDirections[route.id])

    if currentRouteDistance?
      distanceInMiles = parseFloat(currentRouteDistance.distance.text.slice(0, -3))
      @_distance = distanceInMiles
      if @props.onDistanceCalculated?
         @props.onDistanceCalculated(@_distance)
      infowindow = MapStore.createInfoWindow()

      @removeInfowindowListener(['mouseOverShowDistance', 'mouseOutHideDistance', 'domReadyApplyClass'])

      @infowindowListeners['mouseOverShowDistance'] = @addMouseOverListenerToInfoWindow(destMarker, "#{currentRouteDistance.distance.text}", infowindow)
      @infowindowListeners['mouseOutHideDistance'] = @addMouseOutListenerToInfoWindow(destMarker, infowindow)
      @infowindowListeners['domReadyApplyClass'] = @addDomReadyListenerToInfoWindow(infowindow)

  addMouseOverListenerToInfoWindow: (marker, content, infowindow) =>
    google.maps.event.addListener marker, 'mouseover', do (marker = marker, content = content, infowindow) ->
      ->
        infowindow.setContent content
        infowindow.open @map, marker
        return

  addMouseOutListenerToInfoWindow: (marker, infowindow) =>
    google.maps.event.addListener marker, 'mouseout', do (infowindow) ->
      ->
        infowindow.close()
        return

  addDomReadyListenerToInfoWindow: (infowindow) =>
    google.maps.event.addListener(infowindow, 'domready', () ->
      $(".gm-style-iw").parent().addClass('info-window-container-without-close-x')
    )

  removeInfowindowListener: (listeners) =>
    for listener in listeners
      if @infowindowListeners[listener]?
        google.maps.event.removeListener(@infowindowListeners[listener])
        @infowindowListeners[listener] = null

  hideShowSiteMarker: =>
    locationMarkersLatLng = []

    for locationMarker in values(gMarkers)
      locationMarkersLatLng.push
        lat: locationMarker.getPosition()?.lat()
        lng: locationMarker.getPosition()?.lng()

    for siteMarker in values(sitesAndPlacesMarkers)
      position = siteMarker.getPosition()
      if position
        siteMarkerLatLng =
          lat: position.lat()
          lng: position.lng()

        if locationMarkersLatLng.some((location) -> location.lat == siteMarkerLatLng.lat && location.lng == siteMarkerLatLng.lng)
          siteMarker.setVisible(false)
        else
          siteMarker.setVisible(true)

    return

  updateDirections: (route) =>
    if route.id?
      directions = gDirections[route.id]
      if @getLocationPoint(route.origin)? and @getLocationPoint(route.dest)?
        if not directions?
          directions = gDirections[route.id] = @createDirections(route)

        @getDirections(directions, @getLocationPoint(route.origin),@getLocationPoint(route.dest), route)
        if directions?
          directions.setMap(@map)
      else
        if directions?
          directions.setMap(null)

  getLocationPoint: (loc) =>
    if loc?.lat and loc?.lng
      lat=loc?.lat
      lng=loc?.lng
      if lat? and lng?
        origin=new google?.maps.LatLng(lat,lng)
      return origin

  plotLocation: (props, id, routeNode, routeLabel, routeMarker, color, routeLetter, zoneLabel) ->
    if @getLocationPoint(routeNode)?
      if UserStore.isPartner() && props.autoAssigning
        if not gCircles[id]
          circle = gCircles[id] = @createCircle({
            lat: routeNode.lat,
            lng: routeNode.lng,
          }, color)
        else
          circle = gCircles[id]
        circle?.setVisible(true)
        circle?.setCenter({
          lat: routeNode.lat,
          lng: routeNode.lng,
        })

        popup = gPopups[id] = @createPopup({
          lat: routeNode.lat,
          lng: routeNode.lng,
        }, zoneLabel)

        popup?.setMap(@map)

      else
        gCircles[id]?.setVisible(false)
        marker = gMarkers[id]
        if not marker?
          marker = gMarkers[id] = @createMarker((if routeLabel? then routeLabel else routeLetter), routeMarker)
        marker.setPosition(@getLocationPoint(routeNode))
        marker.setVisible(true)
        @createAddrInfoWindowFromId(id, routeNode.address)
        marker.setVisible(true)
    else
      gCircles[id]?.setVisible(false)
      if gMarkers[id]?
        gMarkers[id].setVisible(false)
        gMarkers[id].setPosition(null)

  getDirections:(directions, origin, dest, route) =>
    MapStore.getDirections(origin, dest, (result, status) =>
      if status == google.maps.DirectionsStatus.OK
        if directions?
          directions.setDirections(result)
          id = route.id+"d"
          marker = gMarkers[id]
          if marker
            @showDistanceOnDropLocation(route, marker)
    )

  getBoundsZoomLevel: (bounds, mapDim) =>
    latRad = (lat) ->
      sin = Math.sin(lat * Math.PI / 180)
      radX2 = Math.log((1 + sin) / (1 - sin)) / 2
      Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2

    zoom = (mapPx, worldPx, fraction) ->
      Math.floor Math.log(mapPx / worldPx / fraction) / Math.LN2

    WORLD_DIM =
      height: 256
      width: 256
    ZOOM_MAX = 21
    ne = bounds.getNorthEast()
    sw = bounds.getSouthWest()
    if isObject(ne) and isObject(sw)
      latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI
      lngDiff = ne.lng() - sw.lng()
      lngFraction = (if lngDiff < 0 then lngDiff + 360 else lngDiff) / 360
      latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction)
      lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction)
      return Math.min latZoom, lngZoom, ZOOM_MAX
    ZOOM_MAX

  getMapDims: =>
    width = $(@refs.map_canvas).width()
    height = null
    if width? and !isNaN(width)
      height = $(@refs.map_canvas).height()
    else
      width = $(@refs.map_alternate).width()
      height = $(@refs.map_alternate).height()

    return  {
      width: width,
      height: height
    }


  fitBounds: (locs, force=false) =>
    #bounds = new (google.maps.LatLngBounds)
    #for loc in locs
    #  bounds.extend (new (google.maps.LatLng)(loc.lat, loc.lng))
    props = {}
    if !force
      if @props.paddingTop
        props.top = @props.paddingTop
      zoom = @getBoundsZoomLevel(locs, @getMapDims())
      props.zoom = zoom

    @map?.fitBounds(locs, props)

  #only called initially so we can use passed in props
  setBounds: (props) =>
    LatLngList = []
    bounds = new (google.maps.LatLngBounds)
    latlng = null
    setAmount = 0
    if props.routes?
      for route in props.routes
        if route.origin?.lat and route.origin?.lng
          latlng = (new (google.maps.LatLng)(route.origin.lat, route.origin.lng))
          bounds.extend latlng
          setAmount++
        if route.dest?.lat and route.dest?.lng
          latlng =  (new (google.maps.LatLng)(route.dest.lat, route.dest.lng))
          bounds.extend latlng
          setAmount++
    if props.showMeAt?.lat and props.showMeAt?.lng
      latlng =  (new (google.maps.LatLng)(props.showMeAt.lat, props.showMeAt.lng))
      bounds.extend latlng
      setAmount++
    if props.trucks
      for id in props.trucks
        if id?
          vehicleLat = VehicleStore.get(id, 'lat', @getId())
          vehicleLng = VehicleStore.get(id, 'lng', @getId())
          if vehicleLat and vehicleLng
            latlng = (new (google.maps.LatLng)(vehicleLat, vehicleLng))
            bounds.extend latlng
            setAmount++

    if UserStore.isPartner() && props.autoAssigning
      bounds = new google.maps.LatLngBounds()
      for popup in Object.keys(gPopups)
        if popup.slice(-1) == 'o'
          @map?.setCenter({
            lat: gPopups[popup].position.lat(),
            lng: gPopups[popup].position.lng(),
          })

    else
      #If there's only one marker, the map will automatically jump to it, but not zoom in (whcih setBounds will do)
      if setAmount > 1
        boundsProps = {}
        if props.paddingTop
          boundsProps.top = props.paddingTop


        zoom = @getBoundsZoomLevel(bounds, @getMapDims()) or @map.getZoom()
        if isNaN(zoom)
          return

        boundsProps.zoom = zoom
        @map?.fitBounds(bounds, boundsProps)

        #Making sure we don't zoom too far in after setting bounds
        setTimeout( =>
          if @map?
            zoomOverride = @map.getZoom()
            if zoomOverride > 16
              zoomOverride = 16
              @map?.setZoom(zoomOverride)
        , 100)

      else if setAmount == 0
        Logger.debug "Updating bounds to center"

        if props.center?
          Logger.debug("setting center to ", props.center)
          @setCenter(props.center.lat, props.center.lng)
        else
          user = UserStore.getUser()

          if user?.company?.location?
            @setCenter(user.company.location.lat, user.company.location.lng)

      else if setAmount == 1
        Logger.debug(bounds)
        @setCenter(latlng.lat(), latlng.lng())
        Logger.debug "Not updating bounds because no map ref available mapsLoaded:#{@state.mapsLoaded} map: #{this.map}"



  createMap: (props) =>
    coords = @props.coords

    mapOptions =
      zoom: 13

    if props.mapOptions
      mapOptions = props.mapOptions

    if @getLocationPoint(props?.routes?[0]?.origin)
      mapOptions.center =
        lat: @getLocationPoint(props?.routes?[0]?.origin).lat()
        lng: @getLocationPoint(props?.routes?[0]?.origin).lng()

    theMap = MapStore.stealMap(@refs.map_canvas, mapOptions)

    if props?.listeners?
      for listener, callback of props.listeners
        google.maps.event.addListener(theMap, listener, callback)

    if props.forceCenter and mapOptions.center?
      setTimeout(=>
        @setCenter(mapOptions.center.lat, mapOptions.center.lng)
      , 0)

    if @props.onClick?
      google.maps.event.addListener(theMap, 'click', (e) =>
        @props?.onClick?(e.latLng.lat(), e.latLng.lng(), e)
      )

    if @props.onBoundsChanged?
      google.maps.event.addListener(theMap, 'bounds_changed', (e) =>
        @props?.onBoundsChanged?(theMap.getBounds())
      )

    return theMap

  createDirections: (route) =>
    MapStore.createDirectionsRenderer
      preserveViewport: true
      suppressInfoWindows: false
      suppressMarkers: true
      polylineOptions: {
        strokeColor: if route.lineColor then route.lineColor else "#90BAD4"
      }
      map: @map

  createInfoWindow: (content, isTruck) =>
    infoWindow = MapStore.createInfoWindow({content: content, disableAutoPan: @props.disableAutoPan}, isTruck)
    google.maps.event.addListener infoWindow,
      "domready",
      () ->
        $ ".gm-style-iw"
          .parent()
          .addClass "info-window-container"
    return infoWindow


  makeIcon: (url, width, height, angle) =>
    options = options || {}
    rImg = options.img || new Image()
    rImg.src = url
    options.width = Math.max(width, height)
    options.height = Math.max(width, height)
    canvas = document.createElement("canvas")
    canvas.width = options.width
    canvas.height = options.height
    context = canvas.getContext("2d")
    centerX = options.width/2
    centerY = options.height/2
    ctx = canvas.getContext("2d")
    ctx.clearRect(0, 0, options.width, options.height)
    ctx.save()
    ctx.translate(centerX, centerY)
    ctx.rotate(angle)
    ctx.translate(-centerX, -centerY)
    ctx.drawImage(rImg, 0, (width-height)/2)
    ctx.restore()
    #Logger.debug( canvas.toDataURL('image/png'))
    return canvas.toDataURL('image/png')





  createTruckImage: (rotation) =>
    scale = 1
    if @props.truckScale?
      scale = @props.truckScale

    url: '/assets/images/truck.png'
    scaledSize: new google.maps.Size(50*scale, 22*scale)
    origin: new google.maps.Point(0, 0)
    anchor: new google.maps.Point(25*scale, 11*scale)

#    if not @imageElemLoaded?
#      $("<img/>").on('load', () =>
#        @imageElemLoaded = true
#        @createTruckImage(rotation)
#        )
#        .on('error', () => imageElemLoaded = false )
#        .attr("src", '/assets/images/truck.png')
#      image =
#        url: '/assets/images/truck.png'
#        scaledSize: new google.maps.Size(50*scale, 22*scale)
#        origin: new google.maps.Point(0, 0)
#        anchor: new google.maps.Point(25*scale, 11*scale)
#    else
#      Logger.debug("rotation: ", rotation)
#      image =
#        url: '/assets/images/truck.png' # @makeIcon('/assets/images/truck.png', 124, 51, 0) # rotation * Math.PI / 180 )
#        #url: '/assets/images/truck.png'
#        scaledSize: new google.maps.Size(50*scale, 50*scale)
#        origin: new google.maps.Point(0, 0)
#        anchor: new google.maps.Point(25*scale, 25*scale)
#
#    return image

  createTruckInfoWindow: (id, marker, vehicleName) =>
    nameOrNode = ""
    if vehicleName?
      nameOrNode = vehicleName
    if @props.truckInfoWindow
      nameOrNode = @props.truckInfoWindow(id, nameOrNode)
    return @createInfoWindowWithName(id, marker, nameOrNode, true)

  isInfoWindowOpen: (infoWindow) =>
    theMap = infoWindow.map
    return (theMap != null && typeof theMap != "undefined")

  showTruckInfoWindow: (id, center = true) =>
    infoWindow = truckInfo[id]
    marker = truckMarkers[id]
    truckLat = marker?.getPosition()?.lat()
    truckLng =  marker?.getPosition()?.lng()
    if infoWindow? and marker?
      for index, info of truckInfo
        info.close()
      infoWindow.open(@map, marker)
      if center && truckLat && truckLng
        offset = -200
        @setCenterWithPixelOffset(truckLat, truckLng, 0, offset)

  createInfoWindowWithName: (id, marker, textOrNode, isTruck) =>
    infoWindow = @createInfoWindow(textOrNode, isTruck)

    marker.addListener('click', =>
      if(@props.showInfoWindowAlways)
        marker.setZIndex google.maps.Marker.MAX_ZINDEX + 1
        infoWindow.close()
        infoWindow.open @map, marker
        infoWindow.setZIndex google.maps.Marker.MAX_ZINDEX + 1
      else if @isInfoWindowOpen(infoWindow)
        infoWindow.close()
      else
        #close all info windows
        if isTruck
          if @props.truckClick?
            @props.truckClick(id)
          for index, info of truckInfo
            info.close()
        infoWindow.open(@map, marker)
    )

    if @props.showInfoWindowAlways
      infoWindow.open(@map, marker)

    return infoWindow

  createMyMarker: (pos) =>
    image =
      url: '/assets/images/user.png',
      scaledSize: new google.maps.Size(22, 22),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(11, 11)

    Logger.debug("Creating my marker")
    return MapStore.createMarker(
      preserveViewport: true
      icon: image
    )

  createCircle: (center, color) =>
    return MapStore.createCircle(
      center: center
      fillColor: color
      fillOpacity: 0.35
      radius: 805
      strokeColor: color
      strokeOpacity: 0.8
      strokeWeight: 2
    )

  createMarker: (text, marker_url) =>
    return MapStore.createMarker(
      defaultAnimation: 2
      icon: if @props.markerImage then @props.markerImage else
        url: if marker_url? then marker_url else "/assets/images/marker_blue.png"
        labelOrigin: new google.maps.Point(13, 15)
      label: if @props.markerLabel then @props.markerImage else
        color: "#FFF"
        text: text
    )

  createPopup: (options, zoneLabel) =>
    backgroundColor = if (zoneLabel == 'Pickup Zone') then 'rgba(66, 183, 86, 0.35)' else 'rgba(255, 0, 0, 0.35)'
    popupContent = document.createElement('div')
    popupContent.classList.add('google-maps-custom-popup')
    popupText = document.createTextNode(zoneLabel)
    popupContent.appendChild(popupText)
    popupContent.setAttribute('style', 'background-color: ' + backgroundColor + ';')
    options.content = popupContent
    return MapStore.createPopup(options)

  createTruckMarker: (id) =>

    #TODO: if truck appeared then set it, otherwise create a new one
    sm = new SlidingMarker(
    #return new google.maps.Marker
      preserveViewport: true
      #key: "Truck",
      icon: @createTruckImage()
      duration: 20000
      easing: "linear"
      #defaultAnimation: 2
      map: @map
    )

    MapStore.addMarker(sm)
    return sm

  render: ->
    super(arguments...)

    @addSitesToMap()

    style = {}
    if @props.forceHeight?
      style = height: @props.forceHeight
    if @props.width?
      style.width = @props.width
    if @props.height?
      style.height = @props.height

    div
      className: "GMap"
      style: extend({}, style, {
        position: 'relative'
      })
      if @state.staticMap
        div
          style: extend({}, style, {
            cursor: "pointer"
          })
          onClick: @reinitialize
          ref: "map_alternate"
          div
            style:
              backgroundImage: 'url("https://s3-us-west-1.amazonaws.com/joinswoop.static/blurry_map.png")'
              backgroundSize: 'cover'
              position: 'absolute'
              left: 0
              top: 0
              rigth: 0
              bottom: 0
              borderRadius: 3
          div
            style:
              width: 150
              height: 40
              left: "calc(50% - 75px)"
              top:  "calc(50% - 40px)"
              border: "1px solid #63B4E2"
              color: "#63B4E2"
              padding: "10px"
              background: "white"
              display: "block"
              textAlign: "center"
              position: 'absolute'
              borderRadius: 3
            "View Map"
      else
        div
          className: "map_canvas"
          ref: "map_canvas"
          style: style
          if @state.map
            React.createFactory(MapContext.Provider)
              value: @state.map
              @props.children



module.exports = SwoopMapClass
