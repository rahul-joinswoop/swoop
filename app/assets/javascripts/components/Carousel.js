import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CarouselPhotoDeleteBtn from 'components/CarouselPhotoDeleteBtn'
import UserStore from 'stores/user_store'
import Utils from 'utils'
import { map, isEmpty } from 'lodash'
import 'stylesheets/components/Carousel.scss'

const RESIZE_TIMEOUT = 16
const SLIDER_POSITION_TIMEOUT = 16

class Carousel extends Component {
  static defaultProps = {
    allowDelete: true,
    buttonSize: 20,
    buttonTop: '50%',
    buttonXOffset: 4,
    scrollSpeed: 3,
    startItem: 0,
  }

  static propTypes = {
    allowDelete: PropTypes.bool,
    buttonSize: PropTypes.number,
    buttonTop: PropTypes.string,
    buttonXOffset: PropTypes.number,
    containerWidth: PropTypes.number,
    hideScrollBar: PropTypes.bool,
    items: PropTypes.array.isRequired,
    scrollManyOnClick: PropTypes.bool,
    scrollSpeed: PropTypes.number,
    startItem: PropTypes.number,
  }

  constructor(props) {
    super(props)
    this.carouselRef = React.createRef()
    this.sliderRef = React.createRef()
    this.itemRef = React.createRef()

    this.resizeTimer = null
    this.scrollListenerTimer = null

    this.state = {
      slider: null,
      sliderItemWidth: 0,
      sliderScrollPosition: 0,
      parentNodeWidth: null,
    }
  }

  componentDidMount() {
    if (window && !isEmpty(this.props.items)) {
      window.addEventListener('resize', this.resizeHandler)
      this.resizeHandler()
    }
    if (this.sliderRef.current) {
      this.setState({
        slider: this.sliderRef.current,
        sliderItemWidth: this.props.containerWidth || this.itemRef.current?.clientWidth,
      }, () => {
        this.sliderRef.current.scrollLeft = this.props.startItem * this.state.sliderItemWidth
      })
      this.sliderRef.current.addEventListener('scroll', this.sliderScrollPosListener)
    }
  }

  componentWillUnmount() {
    if (window) {
      window.removeEventListener('resize', this.resizeHandler)
    }
    if (this.state.slider) {
      this.state.slider.removeEventListener('scroll', this.sliderScrollPosListener)
    }
  }

  setRenderStyles = (items, buttonSize, buttonTop, buttonXOffset) => {
    const {
      parentNodeWidth, sliderScrollPosition, slider, sliderItemWidth,
    } = this.state
    const carouselWidth = (Math.floor(parentNodeWidth / sliderItemWidth) || 1) * sliderItemWidth
    const displayButtonWidth = `${buttonSize + buttonXOffset}px`
    const buttonStyle = {
      fontSize: `${buttonSize}px`,
      width: displayButtonWidth,
      top: buttonTop,
    }
    const leftButtonDisplay = sliderScrollPosition === 0 ? 'none' : 'flex'
    const rightBoundaryExceeded = slider &&
      sliderScrollPosition >= slider.scrollWidth - slider.clientWidth
    const rightButtonDisplay = rightBoundaryExceeded ? 'none' : 'flex'
    return {
      carouselWidth,
      displayButtonWidth,
      buttonStyle,
      leftButtonDisplay,
      rightButtonDisplay,
    }
  }

  resizeHandler = () => {
    clearTimeout(this.resizeTimer)
    this.resizeTimer = setTimeout(() => {
      if (this.props.containerWidth && this.props.containerWidth !== this.state.sliderItemWidth) {
        this.setState({
          parentNodeWidth: this.carouselRef?.current?.parentNode?.clientWidth,
          sliderItemWidth: this.props.containerWidth || this.itemRef?.current?.clientWidth,
        })
      } else {
        this.setState({
          parentNodeWidth: this.carouselRef?.current?.parentNode?.clientWidth,
        })
      }
    }, RESIZE_TIMEOUT)
  }

  handleOnClick = (carouselWidth, isMovingRight) => {
    const { slider, sliderItemWidth } = this.state
    const currentPos = this.centerSliderPosition(slider.scrollLeft)
    const moveAmount = this.props.scrollManyOnClick ? carouselWidth : sliderItemWidth
    const destination = currentPos + (isMovingRight ? moveAmount : -moveAmount)
    Utils.smoothScrollHorizontal(slider, destination, isMovingRight, this.props.scrollSpeed)
  }

  // centerSliderPosition helps to center the slider after mouse/touch scrolling.
  centerSliderPosition = (currentPos) => {
    const remainder = currentPos % (this.state.sliderItemWidth)
    if (remainder === 0) {
      return currentPos
    }
    return Math.round(currentPos / this.state.sliderItemWidth) * this.state.sliderItemWidth
  }

  sliderScrollPosListener = () => {
    clearTimeout(this.scrollListenerTimer)
    const { scrollLeft } = this.state.slider
    this.scrollListenerTimer = setTimeout(() => {
      this.setState({
        sliderScrollPosition: scrollLeft,
      })
    }, SLIDER_POSITION_TIMEOUT)
  }

  render() {
    const {
      items, buttonSize, buttonTop, buttonXOffset, hideScrollBar,
    } = this.props

    if (isEmpty(items)) {
      return null
    }

    const {
      carouselWidth, displayButtonWidth, buttonStyle, leftButtonDisplay, rightButtonDisplay,
    } = this.setRenderStyles(items, buttonSize, buttonTop, buttonXOffset)

    return (
      <div className="carousel" ref={this.carouselRef} style={{ width: `${carouselWidth}px` }}>
        <div className="items-container-outer">
          <div className="slider-button fa fa-chevron-circle-left prevent-modal-close" onClick={() => { this.handleOnClick(carouselWidth, false) }} role="button" style={Utils.combineStyles(buttonStyle, { left: `-${displayButtonWidth}` }, { display: leftButtonDisplay })} />
          <div className="slider-button fa fa-chevron-circle-right prevent-modal-close" onClick={() => { this.handleOnClick(carouselWidth, true) }} role="button" style={Utils.combineStyles(buttonStyle, { right: `-${displayButtonWidth}` }, { display: rightButtonDisplay })} />
          <div className={`items-container-middle${hideScrollBar ? ' hide-scrollbar' : ''}`} ref={this.sliderRef}>
            <div className="items-container-inner">
              {map(items, (item, index) => (
                <div className={`item-container item-${index + 1}`} key={`item-${item.key}`} ref={this.itemRef}>
                  {item}
                  {(this.props.allowDelete && (UserStore.isSwoop() || UserStore.isAdmin())) &&
                    <CarouselPhotoDeleteBtn
                      handlePhotoDelete={this.props.handlePhotoDelete}
                      item={item.props.photoItem}
                    />}
                </div>
              ))}
            </div>
          </div>
          {hideScrollBar && <div className="scrollbar-mask"><div className="mask" /></div>}
        </div>
      </div>
    )
  }
}


export default Carousel
