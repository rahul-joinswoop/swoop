import React from 'react'
UserStore = require ('stores/user_store')
UsersStore = require('stores/users_store').default
import Consts from 'consts'
BaseComponent = require('components/base_component')
JobStore = require('stores/job_store')
Backendsuggest = React.createFactory(require('components/autosuggest/backend_suggest').default)
import { filter, map } from 'lodash'

class DipsatcherInput extends BaseComponent
  constructor: ->
    super(arguments...)
    @state = @getInitialState()

    lastSelectedWatcherId = this.createNewWatcher(@setLastSelectedToMatchServer)
    @lastSelected = @getCurrentDispatcherId(lastSelectedWatcherId)
    @register(UserStore, UserStore.LOGIN)

  rerender: -> @rerenderWhenModalCloses()

  setLastSelectedToMatchServer: =>
    @lastSelected = @getCurrentDispatcherId()

  getInitialState: ->
    {}

  componentWillUnmount: ->
    super(arguments...)
    @backend_suggest = null

  setLastSelected: () =>

    id = @lastSelected

    if !id?
      id = null

    if id != @getCurrentDispatcherId()
      obj = {
        id: @props.job_id
      }
      name = if UserStore.isSwoop() then 'root_dispatcher_id' else if UserStore.isFleet() then "fleet_dispatcher_id" else "partner_dispatcher_id"

      if id
        obj[name] = id
        @backend_suggest.setInputValue(UsersStore.get(id , "full_name"))
      else
        obj[name] = null
        @backend_suggest.setInputValue("")
      # no need to pass componentId since it's a callback function that PATCHes the job to BE
      JobStore.jobChanged(obj, false, JobStore.get(@props.job_id, 'status') == Consts.DRAFT)
    else
      if !id
        @backend_suggest.setInputValue("")


  onFocus: () =>
    @focused = true
    @backend_suggest.selectAll()

  onBlur: () =>
    if @focused
      @focused = false
      #TODO: HACK: This is to give the click listener time to call onChange on the clicked list item
      setTimeout(=>
        if @isMounted()
          @setLastSelected()
      , 250)

  onEnter: () =>
    #TODO: HACK: This is to give the click listener time to call onChange on the clicked list item
    setTimeout(=>
      if @isMounted()
        @setLastSelected()
    , 250)

  onChange: (e) =>
    @lastSelected = e?.id

  onClick: (e) =>
    e.preventDefault()
    e.stopPropagation()

  getCurrentDispatcherId: (watcher) ->
    if UserStore.isSwoop()
      if window.logging?.showDispatcherGet
        console.log("Input Dispatcher: ", @props.job_id, watcher)
      return JobStore.get(@props.job_id, "root_dispatcher_id", watcher)
    else if UserStore.isFleet()
      return JobStore.get(@props.job_id, "fleet_dispatcher_id", watcher)
    else if UserStore.isPartner()
      return JobStore.get(@props.job_id, "partner_dispatcher_id", watcher)
    return null


  render: ->
    super(arguments...)
    fullName = ""
    comp_id = @getId()

    dispatcher_id = @getCurrentDispatcherId(comp_id)
    if dispatcher_id
      fullName = UsersStore.get(dispatcher_id, "full_name", comp_id)

    Backendsuggest
      placeholder: 'Type or Select'
      className: "downarrow"
      getSuggestions: (options, callback) =>
        #TODO: determine if these watchers actually work, as they won't cause a change in the proprties passed into backendsuggest
        dispatchers = UsersStore.getSortedDispatcherIds(comp_id)
        objs = map(dispatchers, (id) -> {id: id, label: UsersStore.get(id, "full_name", comp_id) || "..loading"})

        if options.input.length > 0 && options.input != fullName
          objs = filter(objs, (obj) -> obj.label.toLowerCase().indexOf(options.input.toLowerCase()) >= 0)

        objs.unshift({id: null, label: "Unassigned"})
        objs.unshift({id: UserStore.getUser().id, label: "Assign to me"})
        callback(objs)
      id: 'InputDispatcher'
      ref: (ref) => @backend_suggest = ref?.instanceRef
      onClick: @onClick
      setHeight: false
      onBlur: @onBlur
      onEnter: @onEnter
      onFocus: @onFocus
      minCharacters: 0
      onSuggestSelect: @onChange
      initialValue: fullName
module.exports = DipsatcherInput
