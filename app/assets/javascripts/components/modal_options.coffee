import React from 'react'
ModalStore = require('stores/modal_store')
BaseComponent = require('components/base_component')
onClickOutside = require('react-onclickoutside').default
import Dispatcher from 'lib/swoop/dispatcher'
import { withModalContext } from 'componentLibrary/Modal'

class OptionsList extends BaseComponent
  constructor: (props) ->
    super(props)
    @state = {hidden: true}

  toggleHidden: (state) =>
    if not state?
      state = !@state.hidden
    @setState hidden: state

  handleClickOutside: (e) =>
    window.active_options_id = null
    Dispatcher.send(ModalStore.CLOSE_MODALS)
    @props.hideModals()
    if @props.onUnfocus
      @props.onUnfocus()

  render: ->
    super(arguments...)
    window.active_options_id = @props.id
    div
      className: "optionsList"
      style:
        top: (if @props.target? then $(@props.target).offset().top else 0) + (if @props.offsetTop then @props.offsetTop else 0) + $('.wrapper').scrollTop()
        left: (if @props.target? then $(@props.target).offset().left else 0) + (if @props.offsetLeft then @props.offsetLeft else 0)
      if @props.options
        ul null,
          if @props.options?
            for option in @props.options
              option
      else if @props.getView?
        @props.getView()

module.exports = withModalContext(onClickOutside(OptionsList))
