import React from 'react'
import ReactDOM from 'react-dom'
import { partial } from 'lodash'
{Table, Column, Cell, Row} = require ('components/tables/BaseTable')
UserSettingStore = require("stores/user_setting_store")

MIN_WIDTH = 10

ResizeCell = class ResizeCell extends Cell
  displayName: 'ResizeCell'
  resizing: false
  origX: null
  origWidth: null
  constructor: ->
    super(arguments...)
    @state =
      resizing: false
  handleMove: (e) ->
    @origX = e.clientX
  onResizeDown: (e) ->
    @setState
      resizing: true
    if typeof @props.onResizingCallback == "function"
      @props.onResizingCallback(true)
    $(window).on("mousemove", @onResizeMove.bind(@))
    $(window).on("mouseup", @onResizeUp.bind(@))
    @handleMove(e)
    @origWidth = $(ReactDOM.findDOMNode(@refs.cell)).outerWidth()
    @preventDefault(e)

  preventDefault: (e) ->
    e.stopPropagation()
    e.preventDefault()

  getClassName: ->
    super(arguments...)+" resizableCell "+(if @state.resizing then " resizing" else "")

  onResizeUp: (e) ->
    #HACK: Allows us to check line state on click capture before ending resize
    setTimeout(=>
      @setState
        resizing: false
      if typeof @props.onResizingCallback == "function"
        @props.onResizingCallback(false, @origWidth + (e.clientX - @origX))
    , 10)
    $(window).off("mousemove")
    $(window).off("mouseup")
    @preventDefault(e)
  onResizeMove: (e) ->
    if typeof @props.onColumnResizeEndCallback == "function"
      @props.onColumnResizeEndCallback(@origWidth + (e.clientX - @origX), e.clientX)
  onMouseOver: (e) ->
    #@props.toggleLine(true, e.clientX)
    @props.onColumnResizeEndCallback(null, e.clientX)
  onMouseOut: (e) ->
    @props.toggleLine(false, e.clientX)

  renderValue: ->
    [
      @props.children,
      if true || ((not @props.resizable? or @props.resizable) and @props.onResizingCallback? and @props.onColumnResizeEndCallback?)
        div
          key: 'resize_handle'
          className: "resize_handle" #+ (if not @props.resizable then " disabled" else "")
          onMouseDown: @onResizeDown.bind(@)
          onClick: @preventDefault.bind(@)
          onMouseOver: @onMouseOver.bind(@)
          onMouseOut: @onMouseOut.bind(@)
          ""
    ]


ResizeColumn = class ResizeColumn extends Column
  _width: null
  getWidth: -> @_width || super(arguments...)
  setWidth: (x) ->
    @_width = x
  getCellProps: ->
    props = super(arguments...)
    props.onResizingCallback = partial(@props.onResizingCallback, @_key)
    props.onColumnResizeEndCallback = partial(@props.onColumnResizeEndCallback, @_key)
    props.toggleLine = @props.toggleLine
    props

ResizeRow = class ResizeRow extends Row
  getColumnProps: (column, props) ->
    props = super(column, props)
    props.onResizingCallback = @props.onResizingCallback
    props.onColumnResizeEndCallback = @props.onColumnResizeEndCallback
    props.toggleLine = @props.toggleLine
    props.onClickCapture = @props.onClickCapture
    props
  getRowProps: () ->
    props = super(arguments...)
    if @props.onClickCapture?
      props.onClickCapture = @props.onClickCapture
    props

RCell = React.createFactory(ResizeCell)
RRow = React.createFactory(ResizeRow)


class RenderLine extends React.Component
  left: 0
  showing: false
  setLeft: (x) ->
    @left = x
    @showing = true
    @forceUpdate()
  setShowing: (val, x) ->
    @setLeft(x)
    @showing = val

  isShowing: ->
    @showing

  render: ->
    if !@showing
      return null
    div
      style:
        cursor: "ew-resize"
        pointerEvents: "none"
        height: "100%"
        width: 3
        position: "absolute"
        top: 0
        left: @left
        backgroundColor: "#7FBCE2"


Line = React.createFactory(RenderLine)

ResizeTable = class Resizetable extends Table
  getRow: () ->
    @props.Row || RRow

  waterfallWidthChange: (remaining, dir, index) ->
    #TODO: THIS WILL FAIL IF THE WATERFALL NEED TO GO PASSED THE END
    index+= dir
    while remaining > 0 and index >= 0 and index < @getCols().length
      if @getCols()[index].getWidth() > MIN_WIDTH + remaining
        @getCols()[index].setWidth(@getCols()[index].getWidth() - remaining)
        return 0

      remaining -= @getCols()[index].getWidth() - MIN_WIDTH
      @getCols()[index].setWidth(MIN_WIDTH)
      index+= dir

    return remaining

  getResizeSettingsKey: -> "col_width_"+@getTableKey()

  getSavedSettings: () ->
    settings = UserSettingStore.getUserSettingByKey(@getResizeSettingsKey())
    if settings?
      return JSON.parse(settings)
    else
      return {}
  commitWidths: () ->
    settings = @getSavedSettings()
    for col in @getCols()
      settings[col.getKey()] = parseFloat((100*col.getWidth()/@getWidth()).toFixed(3))
    UserSettingStore.setUserSettings(@getResizeSettingsKey(), settings)

  onResizingCallback: (key, resizing, width) ->
    resizeCol = @getColWithKey(key)
    orig_width = resizeCol.getWidth()
    if !resizing
      index = @getIndex(key)

      #IF the column is growing then eat into columns to the right
      if width > resizeCol.getWidth()
        remaining = width - resizeCol.getWidth()
        unable_to_steal = @waterfallWidthChange(remaining, 1, index)
        resizeCol.setWidth(width-unable_to_steal)

      else
        #IF THE COLUMN IS SHRINKING MAKE COL ON THE RIGHT BIG
        remaining = orig_width - width
        if width >= MIN_WIDTH
          resizeCol.setWidth(width)
        else
          #TODO: take away from left cols waterfall style until width > minWIDTH
          resizeCol.setWidth(MIN_WIDTH)
          left_to_take_away = -width + MIN_WIDTH
          unable_to_steal = @waterfallWidthChange(left_to_take_away, -1, index)
          remaining -= unable_to_steal

        if @getCols()[@getIndex(key)+1]?
          @getCols()[@getIndex(key)+1].setWidth(@getCols()[@getIndex(key)+1].getWidth() + remaining)
        else
          @getCols()[@getIndex(key)-1].setWidth(@getCols()[@getIndex(key)-1].getWidth() + remaining)


      @hideLine()
      @commitWidths()
      if @_isMounted
        @forceUpdate()

  showLine: (x) ->
    @refs.line?.setShowing(true, x)

  hideLine: (x) ->
    @refs.line?.setShowing(false, x)

  toggleLine: (val, x) ->
    if val then @showLine(x) else @hideLine(x)

  setColumnWidths: () ->
    full_width = @getWidth()
    settings = @getSavedSettings()

    used_width = 0
    unsetCols = []
    setCols = []
    orig_cols = @getCols()
    for col in orig_cols
      #Grab Original Width
      colWidth = col.getOriginalWidth()


      settingWidth = settings?[col.getKey()]

      #TODO: Override if manual set

      #TODO: switch out if percentage
      #Always assumes percentage if in settings
      if settingWidth?
        colWidth = settingWidth*full_width/100

      #set width #if set
      if colWidth
        col.setWidth(colWidth)
        setCols.push(col)
        used_width += colWidth
      else
        unsetCols.push(col)

    ####### OPTION 1 TAKE UP REMAINIGN WIDTH FOR UNSET COLUMNS
    ##Check if there's enough width
    #remaining = (full_width - used_width)
    #remaining_widths = remaining / unsetCols.length
    ##If there's not enough left, steal evenly from the other cols (maybe not the best solution)

    ##Distribute evenly across remaining columns
    #for col in unsetCols
    #  col.setWidth(Math.max(remaining_widths, MIN_WIDTH))

    ######## OPTION 2 TAKE UP AN EVEN NUMBER OF WIDTH FOR UNSET COLUMNS
    remaining_widths = @getWidth()/orig_cols.length
    for col in unsetCols
      col.setWidth(Math.max(remaining_widths, MIN_WIDTH))

    #####################

    @trueUpWidths()

    if @_isMounted
      @forceUpdate()

  trueUpWidths: ->
    #TODO: Put testing around this

    ###### OPTION 1 TAKE AWAY WIDTHS EVENLY
    width = 0
    for col in @getCols()
      width += col.getWidth()

    distributable = @getWidth() - width
    #FOR NOW GOING TO KEEP THIS SIMPLE AND STEAL WIDTH FROM THE LARGE

    if distributable > 0
      cols = @getCols()
      toGiveToEach = distributable / cols.length
      for col in cols
        col.setWidth(col.getWidth() + toGiveToEach)
    else
      cols = @getCols()
      bigCols = []
      for col in cols
        if col.getWidth() > MIN_WIDTH
          bigCols.push(col)
      toSteal = -distributable
      maxIterations = cols.length
      iterations = 0
      while toSteal > 0 and bigCols.length > 0 and iterations <= maxIterations
        iterations++
        currentTheft = toSteal/bigCols.length
        newBigCols = []
        for col in bigCols
          newWidth = col.getWidth() - currentTheft
          if newWidth <= MIN_WIDTH
            newWidth = MIN_WIDTH
          else
            newBigCols.push(col)
          toSteal -= col.getWidth() - newWidth
          col.setWidth(newWidth)
        bigCols = newBigCols

    ######## OPTION 2 ####### take away widths based on percentage of total
    #TODO: try this out and see if it behaves

  resize: () ->
    #TODO: Put testing around this

    full_width = @getWidth()
    #get all set Widths
    taken_width = 0
    for col in @getCols()
      width = col.getWidth()
      if typeof(width) == "string" && width.indexOf("%") > -1
        width = parseFloat(width)*full_width
      else
        width = parseFloat(width)

      col.setWidth(width)
      taken_width += width
  UNSAFE_componentWillReceiveProps: (props) ->
    super(props)
    @resize()

  onColumnResizeEndCallback: (key, width, pos) ->
    @refs.line?.setLeft(pos - $(@refs.tableContainer).offset().left)

  getTableStyle: () ->
    style = super(arguments...)
    style.position = "relative"
    style

  onClickCapture: (e) ->
    if @refs.line?.isShowing()
      e.stopPropagation()
      e.preventDefault()
      return false

  getRowProps: (row) ->
    props = super(row)
    props.Cell = RCell
    props.onResizingCallback = @onResizingCallback.bind(@)
    props.toggleLine = @toggleLine.bind(@)
    props.onColumnResizeEndCallback = @onColumnResizeEndCallback.bind(@)
    props.onClickCapture = @onClickCapture.bind(@)
    props

  renderRows: -> [super(arguments...), Line(ref: "line", key: "resize_line" )]

module.exports =
  Table: ResizeTable
  Column: ResizeColumn
  Cell: ResizeCell
  Row: ResizeRow
