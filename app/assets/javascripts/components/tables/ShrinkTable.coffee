import { Table, Column, Cell, Row } from 'components/tables/ResizeTable'
import { partial } from 'lodash'

SWITCH_WIDTH = 688

class HeaderCol extends Column
  _header: "Header", _key: "header"

class ValueCol extends Column
  _header: "Value", _key: "value"

headerCol = new HeaderCol()
valueCol = new ValueCol()

ShrinkTable = class ShrinkTable extends Table
  getRows: ->
    if @getWidth() < SWITCH_WIDTH
      rows = []
      #TODO: need to memoize this
      for row in super(arguments...)
        for col in @cols
          col.record = row
          rows.push
            id: row.id
            col: col
            header: col.getHeader()
            value: col.getValue(row)
      return rows
    else
      super(arguments...)

  clickTransformer: (index, value) ->
    realIndex = index / @cols.length
    @props.rowClick(realIndex, @props.rows[realIndex])

  setRowPropsWithRealIndex: (props, realIndex) ->
    if @getWidth() < SWITCH_WIDTH && @props.fetchUpdatedRecord
      props.fetchUpdatedRecord = (row) =>
        if row?
          job = @props.fetchUpdatedRecord(row)
          col = row.col
          if col?
            col.record = job
            return {
              id: row.id
              col: col.getHeader()
              header: col.getHeader()
              value: col.getValue(job)
            }
        return row
      #if @props.rowWillReceiveProps?
      #  props.rowWillReceiveProps = partial(@props.rowWillUnmount, partial.placeholder, realIndex, @props.rows[realIndex])

  getRowProps: (props) ->
    rowProps = super(props)

    if @getWidth() < SWITCH_WIDTH
      realIndex = Math.floor(props.index/@cols.length)
      @setRowPropsWithRealIndex(rowProps, realIndex)
      if @props.rowStyleGetter?
        props.rowStyleGetter = partial(@props.rowStyleGetter, realIndex, @props.rows[realIndex])
      if @props.rowClassNameGetter?
        props.rowClassNameGetter = partial(@props.rowClassNameGetter, realIndex, @props.rows[realIndex])

      if @props.rowFooter?
        props.rowFooter = partial(@props.rowFooter, realIndex, @props.rows[realIndex])

      if @props.rowDidMount?
        props.rowDidMount = partial(@props.rowDidMount, partial.placeholder, realIndex, @props.rows[realIndex])
      if @props.rowWillUnmount?
        props.rowWillUnmount = partial(@props.rowWillUnmount, partial.placeholder, realIndex, @props.rows[realIndex])
      if @props.rowClick?
        props.rowClick = partial(@props.rowClick, realIndex, @props.rows[realIndex])

      if (props.index+1) % @cols.length != 0
        rowProps.rowFooter = null
    rowProps

  getCols: ->
    if @getWidth() < SWITCH_WIDTH
      [headerCol, valueCol]
    else
      super(arguments...)

  getIndex: (key) ->
    if @getWidth() < SWITCH_WIDTH
      return if key == "header" then 0 else 1
    else
      super(arguments...)

module.exports =
  Table: ShrinkTable
  Column: Column
  Cell: Cell
  Row: Row
