import React from 'react'
# Table > Row > Column > Cell
ModalStore = require('stores/modal_store')
import { filter, get, map, partial, zipObject, isString } from 'lodash'
import Utils from 'utils'

BaseCell = class BaseCell extends React.Component
  displayName: 'BaseCell'
  componentWillUnmount: ->
    if @unmountCallback?
      @unmountCallback()
  getClassName: ->
    "cell"+(if @props.header then " header" else "")+" "+@props.className
  getStyle: ->
    height: 40
    width: @props.column.getWidth()
  renderValue: ->
    @props.children
  setUnmountCallback: (callback) ->
    @unmountCallback = callback
  getCellProps: ->
    className: @getClassName()
    ref: "cell"
    style: @getStyle()
    title: @props.title

  render: ->
    div @getCellProps(), @renderValue()


BaseColumn = class BaseColumn
  _header: ""
  _key: ""
  _cellClassName: ""
  _showing: true
  _cells: {}
  isShowing: -> @_showing
  getKey: -> @_key
  setRecord: (@record) ->
  getValue: (record=@props?.record) ->
    if !@
      Rollbar.error("Calling get value on an undefined BaseColumn object ", {record: record})
      return
    return get(@record, @_key)
  getHeader: -> @_header
  getWidth: -> @props?.width
  getOriginalWidth: -> @_originalWidth
  getCellClassName: -> @_cellClassName
  getCellProps: ->
    header: @props.header
    column: @
    key: @getKey()
    colKey: @getKey()
    className: @getCellClassName()
    title: if not @props.header and isString(@getValue(@record)) then @getValue(@record)
  getCellClass: () ->
    @props.Cell
  render: (@props) ->
    @record = @props.record
    if @isShowing()
      @getCellClass() @getCellProps(),
        if @props.header then @getHeader() else @getValue(@record)
    else
      null


BaseRow = class Row extends React.Component
  constructor: (props) -> super(props)
  getRowStyle: (row) -> if @props.rowStyleGetter? then @props.rowStyleGetter(@props.index, @getValue()) else {}
  getRowClassName: ->
    className = "row_container "
    if @props.rowClassNameGetter?
      className += @props.rowClassNameGetter(@props.index, @getValue())
    if @props.index % 2 == 0
      className += " even"
    else
      className += " odd"
    className
  componentDidMount: -> if !@props.header? then @props.rowDidMount?(@, @props.index, @getValue())
  componentWillUnmount: -> if !@props.header? then @props.rowWillUnmount?(@, @props.index, @getValue())
  UNSAFE_componentWillReceiveProps: (newProps) ->
    if !@props.header?
      @props.rowWillReceiveProps?(@, newProps, @props)
  renderHeader: () -> if !@props.header? then @props.rowHeader?(@props.index, @getValue())
  renderFooter: () -> if !@props.header? then @props.rowFooter?(@props.index, @getValue())

  generalChanged: =>
    if !@waiting_for_rerender?
      @waiting_for_rerender = false

    if ModalStore.getModal()?
      if !@waiting_for_rerender
        ModalStore.bind( ModalStore.CHANGE, @generalChanged )
      @waiting_for_rerender = true

    else
      if @waiting_for_rerender
        ModalStore.unbind( ModalStore.CHANGE, @generalChanged )
        @waiting_for_rerender = false
        setTimeout(@generalChanged, 0)
      @setState {rev: if @state?['rev']? then ++@state?['rev'] else 0} #set state and force rerender asynchronously

  getColumnProps: (column, props) ->
    props.header = @props.header
    props.Cell = @props.Cell
    props.width = @props.width/(@props.columns.length)
    props
  getValue: () ->
    if @props.fetchUpdatedRecord?
      @props.fetchUpdatedRecord(@props.row)
    else
      @props.row
  getRowProps: () ->
    className: "row "+if @props.header? then "row_header" else ""
    onClick: if !@props.header? then partial(@props.rowClick, @props.index, @getValue())

  render: ->
    values = false
    cells = map(@props.columns, (column, i) =>
      val = @getValue()
      if val? || @props.header
        values = true
      column.render(@getColumnProps(column, {record: val}))
    )
    if values
      div
        key: "row_"+@getValue()?.id
        #key: row + JSON.stringify(style)
        className: @getRowClassName()
        style: @getRowStyle()
        @renderHeader()
        div @getRowProps(),
          cells
        @renderFooter()
    else
      null


Cell = React.createFactory(BaseCell)
Row = React.createFactory(BaseRow)
Column = React.createFactory(BaseColumn)


Table = class Table extends React.Component
  displayName: 'BaseTable'

  constructor: (props) ->
    super(arguments...)
    @state = {}
    @width = $(window).width()
    @UNSAFE_componentWillReceiveProps(@props)

  componentDidMount: ->
    window.addEventListener('resize', @windowChange)
    @_isMounted = true

  componentWillUnmount: ->
    window.removeEventListener('resize', @windowChange)
    @_isMounted = false

  getTableKey: () -> @props.tableKey

  windowChange: () =>
    if @width != $(window).width()
      @width = $(window).width()
      @setColumnWidths()

  UNSAFE_componentWillReceiveProps: (props) ->
    @cols = filter(props.cols, (column) ->
      if column
        column.isShowing()
    )

    @cols_index = zipObject(map(@cols, (col) -> col.getKey()), [0...@cols.length])
    @setColumnWidths()

  # This code below fixes the width of the columns when scrollbar appears, but unfortunately breaks job loading
  # because getSnapshotBeforeUpdate kills UNSAFE_componentWillReceiveProps -- thank you React gods
  # TODO: fix this

  # getSnapshotBeforeUpdate: (prevProps, prevState) ->
  #   return @getWidth()
  #
  # componentDidUpdate: (prevProps, prevState, snapshot) ->
  #   if snapshot && @getWidth() != snapshot
  #     @setColumnWidths()


  getClassName: -> "swoopTable " + if @props.className then @props.className else ""
  getRowProps: (props) ->
    #cols = map(cols, (column) ->
    #  React.createFactory(column)
    #)
    props.table = @
    props.columns = @getCols()
    props.Cell = @props.Cell || Cell
    props.key = "row_key_"+(props.row?.id || props.index)+(if props.row?.header? then '_' + props.row.header)
    props.index = props.index
    props.width = @getWidth()
    props.fetchUpdatedRecord = @props.fetchUpdatedRecord
    if @props.rowClick?
      props.rowClick = @props.rowClick
    if @props.rowFooter?
      props.rowFooter = @props.rowFooter
    if @props.rowDidMount?
      props.rowDidMount = @props.rowDidMount
    if @props.rowWillReceiveProps?
      props.rowWillReceiveProps = @props.rowWillReceiveProps
    if @props.rowWillUnmount?
      props.rowWillUnmount = @props.rowWillUnmount
    if @props.rowClassNameGetter?
      props.rowClassNameGetter = @props.rowClassNameGetter
    if @props.rowStyleGetter?
      props.rowStyleGetter = @props.rowStyleGetter


    props

  setColumnWidths: () ->
    for col in @getCols()
      col.setWidth(@getWidth()/@getCols().length)
    if @_isMounted
      @forceUpdate()

  getRow: () ->
    @props.Row || Row

  getRows: () -> @props.rows
  getCols: () -> @cols
  getIndex: (key) -> @cols_index[key]
  getColWithKey: (key) -> @getCols()[@getIndex(key)]

  getWidth: ->
    if @props.width?
      @props.width
    else if @refs.tableContainer?
      $(@refs.tableContainer).width()
    else
      width = $(window).width()
      if width?
        width-100
      else
        1000
  getTableStyle: ->
    width: if @props.width then @props.width else "100%"
    height: @props.height

  getTableProps: ->
    ref: "tableContainer"
    className: @getClassName()
    style: @getTableStyle()

  renderRows: ->
    [
      @getRow() @getRowProps(header: true)
      map(@getRows(), (row, index) =>
        @getRow() @getRowProps(row: row, index: index)
      )
    ]
  render: ->
    div @getTableProps(), @renderRows()

module.exports =
  Table: Table
  Column: BaseColumn
  Cell: BaseCell
  Row: BaseRow
