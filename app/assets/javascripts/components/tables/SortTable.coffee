import React from 'react'
import { Table, Column, Cell, Row } from 'components/tables/ShrinkTable'
import UserSettingStore from 'stores/user_setting_store'
import { difference, map, partial } from 'lodash'
import Utils from 'utils'

ASC = -1
DESC = 1

SortCell = class SortCell extends Cell
  displayName: 'SortCell'

  getStyle: ->
    style = super(arguments...)
    style.cursor = "pointer"
    style
  getClassName: ->
    classes = super(arguments...) + " sortHeaderCell"
    if @props.sortKey == @props.colKey
      classes += " sorting"
      if @props.sortDir == DESC
        classes += " down"
      else
        classes += " up"
      if @props.reverseSort?
        classes += " reversed"
    classes

  reverseSortValue: ->
    if @props.sortKey == @props.colKey
      if @props.sortDir == DESC
        return ASC

    return DESC

  onClick: ->
    @props?.onSortChange?(@reverseSortValue())

  getCellProps: ->
    props = super(arguments...)
    props.onClick = @onClick.bind(@)
    props


SCell = React.createFactory(SortCell)

SortColumn = class SortColumn extends Column
  _sortable: false
  getBackendKey: ->
    if @_backend_key? and !@props.frontEndSort
      return @_backend_key
    return @getKey()
  isSortable: -> @_sortable
  getCellClass: ->
    if @isSortable() and @props.onSortChange? then SCell else super(arguments...)
  getCellProps: (column, props) ->
    props = super(column, props)
    if @isSortable()
      props.onSortChange = if @props.onSortChange? then partial(@props.onSortChange, @getBackendKey())
      props.sortKey = @props.sortKey
      props.sortDir = @props.sortDir
      props.colKey = @getBackendKey()
    props

SortRow = class SortRow extends Row
  getColumnProps: (column, props) ->
    colProps = super(column, props)
    colProps.onSortChange = @props.onSortChange
    colProps.sortKey = @props.sortKey
    colProps.sortDir = @props.sortDir
    colProps

SRow = React.createFactory(SortRow)

SortTable = class SortTable extends Table
  constructor: ->
    super(arguments...)
    @sortKey = null
    @sortDir = null
    @lastSort = null


  UNSAFE_componentWillReceiveProps: (props) ->
    if @sortKey == null
      if props.originalSortKey?
        @sortKey = props.originalSortKey
    if @sortDir == null
      if props.originalSortDir?
        @sortDir = props.originalSortDir
    #TODO: SHould do a better check here

    if props.rows.length != @props.rows.length
      @lastSort = null
    else if props.rows != @props.rows
      old_ids = map(@props.rows, "id")
      new_ids = map(props.rows, "id")
      if difference(old_ids, new_ids).length > 0 || difference(new_ids, old_ids).length > 0
        @lastSort = null

    super(arguments...)

  #renderRows: () -> super(arguments...)
  getRow: () ->
    @props.Row || SRow

  getSettingsKey: ->
    @getTableKey()+"_sortby"

  getSortKey: () ->
    setting = UserSettingStore.getUserSettingByKey(@getSettingsKey())
    if setting
      return JSON.parse(setting).key
    return @sortKey
  getSortOrder: () ->
    setting = UserSettingStore.getUserSettingByKey(@getSettingsKey())
    if setting
      return JSON.parse(setting).order
    return @sortDir
  commitSort: (sortBy, order) ->
    UserSettingStore.setUserSettings(@getSettingsKey(), {
      key: sortBy
      order: order
    })


  getRows: () ->
    key = @getSortKey()
    if @props.frontEndSort and key and @getWidth() >= 688 #This is a hack so we don't do sorting
      dir = @getSortOrder()
      if @lastSort != null and @lastKey != null and @lastDir != null and @lastKey == key and @lastDir == dir
        return @lastSort

      col = @getColWithKey(key)
      if !col?
        key = @props.originalSortKey
        @lastKey = key
        col = @getColWithKey(key)

      if !col?
        return super(arguments...)

      @lastKey = key
      @lastDir = dir
      data = @props.rows.slice()
      if key? and dir?
        data = data.sort((a, b) =>
          if col.sortFunc?
            str1 = col.sortFunc(a)
            str2 = col.sortFunc(b)
          else
            col.setRecord(a)
            str1 = col.getValue(a)
            col.setRecord(b)
            str2 = col.getValue(b)
          return Utils.compare(str1, str2, dir)
      )
      @lastSort = data
      return @lastSort
    else
      super(arguments...)


  onSortChange: (key, order) ->
    @sortKey = key
    @sortDir = order
    @commitSort(key, order)
    if @props.onSortChange?
      @props.onSortChange(key, order)
  getRowProps: (row) ->
    props = super(row)
    if row.header
      props.onSortChange = @onSortChange.bind(@)
      props.sortKey = @getSortKey()
      props.sortDir = @getSortOrder()
      props.frontEndSort = @props.frontEndSort
    else
      if @props.frontEndSort and @getSortKey() and @lastSort
        record = @lastSort[props.index]
        if record?
          realIndex = props.index
          for row, index in @getRows()
            if row.id == record.id
              realIndex = index
              break

        @setRowPropsWithRealIndex(props, realIndex)


    props

module.exports =
  Table: SortTable
  Column: SortColumn
  Cell: SortCell
  Row: SortRow
  ASC: ASC
  DESC: DESC
