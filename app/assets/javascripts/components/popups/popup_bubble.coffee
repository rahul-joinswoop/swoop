import React from 'react'
import ReactDOMFactories from 'react-dom-factories'
BaseComponent = require('components/base_component')
ModalStore = require('stores/modal_store')

import ButtonImport from 'componentLibrary/Button'
Button = React.createFactory(ButtonImport)

class PopupBubble extends BaseComponent
  onCancel: =>
    if @props.onCancel?
      @props.onCancel()
    else
      ModalStore.hidePopup(arguments...)

  onSubmit: =>
    if @props.onSubmit?
      @props.onSubmit(arguments...)
    else
      ModalStore.hidePopup()

  render: ->
    showHeader = @props.showHeader? || true

    div
      className: @props.className
      div
        className: 'arrow'
      @props.overlay
      if showHeader
        div
          className: 'modal-header',
          h3 null,
            @props.title
          ReactDOMFactories.span
            onClick: @onCancel
            "×"
      div
        className: 'modal-body body'
        @props.body
      if @props.submitText
        div
          className: 'modal-bottom'
          style: {
            padding: 10
          }
          Button
            color: 'primary'
            disabled: @props.submitButtonDisabled if @props.submitButtonDisabled?
            onClick: @onSubmit
            style: {
              margin: '10px 0',
              width: '100%'
            }
            @props.submitText

module.exports = PopupBubble
