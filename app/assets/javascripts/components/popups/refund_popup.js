import React from 'react'
import AsyncRequestStore from 'stores/async_request_store'
import BaseComponent from 'components/base_component'
import CreditCard from 'components/credit_card'
import Fields from 'fields/fields'
import JobStore from 'stores/job_store'
import Loading from 'componentLibrary/Loading'
import onClickOutside from 'react-onclickoutside'
import PopupBubble from 'components/popups/popup_bubble'
import Renderer from 'form_renderer'
import Utils from 'utils'
import 'stylesheets/components/popups/invoices/refund_popup.scss'

const RefundFields = {

  CreditCardField: class CreditCardField extends Fields.Field {
    _name = 'card'

    _label = 'Card Number'

    renderInput() {
      return <CreditCard
        brand={this.record?.brand}
        last4={this.record?.last4}
      />
    }
  },

  ChargeAmountField: class ChargeAmountField extends Fields.Field {
    _name = 'amount'

    _label = 'Refund Amount'

    _editable = false

    getValue(...props) {
      return Utils.formatNumber(Utils.toFixed(Math.abs(
        super.getValue(...props)
      ), 2))
    }
  },

  SendReceiptToField: class SendReceiptToField extends Fields.EmailField {
    _name = 'send_recipient_to'

    _placeholder = 'Email (optional)'

    getLabel() {
      if (this.isEditable()) {
        return 'Send Receipt To'
      } else {
        return 'Receipt Sent To'
      }
    }

    forcePrefill() {
      return this.setValue(JobStore.get(this.record.job_id, 'email'))
    }
  },
}

class RefundPopup extends BaseComponent {
  constructor(props) {
    super(props)

    this.form = new Renderer({
      amount: props.amount,
      brand: props.brand,
      job_id: props.job_id,
      last4: props.last4,
    }, this.rerender, {
      submit_attempts: 1,
    })
    this.form.registerFields(RefundFields)
  }

  handleClickOutside = () => this.props.onCancel()

  handleOnSubmit = () => {
    if (!this.state.showSpinner && this.form.isValid()) {
      this.setState({
        showSpinner: true,
      })

      const refund_obj = this.form.getRecordChanges()
      AsyncRequestStore.invoicePaymentRefund(this.props.invoice_id, this.props.payment_id, refund_obj, {
        success: (data) => {
          this.sendTracking(data)
          this.form.setEditable(false)
          this.setState({
            error: null,
            showSpinner: false,
          })
          if (this.props.onRefund) {
            this.props.onRefund(data)
          }
        },

        error: (data) => {
          const error = data?.error?.refund || 'Problem making refund'
          this.setState({
            error,
            showSpinner: false,
          })
        },
      })

      if (this.props.onSubmit) {
        this.props.onSubmit(...this.props)
      }
    }
  }

  renderBody = () =>
    <div>
      { this.state.error && <span className="error">{this.state.error}</span> }
      { this.form.renderInputs([
        ['card', { containerClass: 'half_width' }],
        ['amount', { containerClass: 'half_width refund_money' }],
      ])}
    </div>

  sendTracking = responseData => Tracking.track('Refund', {
    category: 'Payments',
    value: responseData.refund?.amount,
    service: JobStore.get(this.props.job_id, 'service'),
    account: JobStore.get(this.props.job_id, 'account')?.name,
    jobId: this.props.job_id,
  })

  render() {
    return (
      <div className="modalPopup refund_popup">
        <PopupBubble
          body={this.renderBody()}
          className="refund_payment"
          onCancel={this.props.onCancel}
          onSubmit={this.handleOnSubmit}
          overlay={this.state.showSpinner && <Loading
            message="Don't refresh your browser or leave the page."
            size={52}
            title="Processing Refund"
          />}
          submitButtonClasses="primary"
          submitButtonDisabled={!this.form.isValid()}
          submitText="Send Refund"
          title="Refund Charge"
        />
      </div>
    )
  }
}

export default onClickOutside(RefundPopup)
