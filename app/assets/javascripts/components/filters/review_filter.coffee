import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'

class ReviewFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_REVIEWS

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getName: (id) -> id

  setItems: (items, response) =>
    UserSettingStore.setUserSettings(@setting_name, items, callbacks:response)

  render: ->
    super(arguments...)
    AllOptionMultiSelect
      className: 'filter_reviews'
      objectName: "NPS Score"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: [1..10]
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded()
      withUnset: true

module.exports = React.createFactory(ReviewFilter)
