import React from 'react'
import createReactClass from 'create-react-class'
UserStore = require('stores/user_store')
BaseComponent = require('components/base_component')
UsersStore = require('stores/users_store').default
onClickOutside = require('react-onclickoutside').default
UsersStore = (require('stores/users_store')).default
import { difference, intersection, map, partial, sortBy, uniq, without } from 'lodash'
import ReactDOMFactories from 'react-dom-factories'

FilterOption = React.createFactory(createReactClass(
  render: ->
    li
      key: @props.name
      style:
        displayType: "none"
        margin: 0
        padding: 5
        whiteSpace: "nowrap"
      input
        type: "checkbox"
        checked: @props.checked
        onChange: @props.onChange
        style:
          marginRight: "10px"
      span
        style:
          color: "black"
        @props.name
))

class MultiFilter extends BaseComponent
  displayName: 'MultiFilter'
  constructor: (props) ->
    super(props)
    @state = {open: false}
    @register(UserStore, UserStore.CHANGE)

  @defaultProps = {
    getObjectValid: -> true
  }

  handleClickOutside: (e) =>
    if $(e.target).closest(".#{@getClassName()}")[0]!=@refs["container"]
      @setState
        open: false

  addAll: (e) =>
    sites = []
    if !e.target.checked
      sites = uniq(@props.getAll())

    user = UserStore.getUser()
    if !user?
      return


    obj = {
      id: user.id
      silent: true
    }
    obj[@props.name] = uniq(sites)
    UsersStore.updateItem(obj)

  toggleSiteOnUser: (id, e) =>
    user = UserStore.getUser()
    if !user?
      return

    sites = user[@props.name]
    sites = without(sites, id)
    if !e.target.checked
      sites.push(id)

    obj = {
      id: user.id
      silent: true
    }
    obj[@props.name] = uniq(sites)
    UsersStore.updateItem(obj)

  getClassName: ->
    if @props.className? then @props.className else 'dispatch_sites'

  render: ->
    super(arguments...)

    if @props.getCheckedValues?
      user_sites = @props.getCheckedValues()
    else
      user_sites = UserStore.getUser()?[@props.name]
    all_sites = @props.getAll()
    if !user_sites?
      user_sites = []
    all_checked = if @props.allChecked then @props.allChecked() else intersection(user_sites, all_sites).length == 0

    if (all_sites.length <= 1 || !@props.show()) && not @props.isLoading?
      return null

    div
      className: @getClassName()
      ref: "container"
      style:
        display: "inline-block"
        verticalAlign: "middle"
        position: "relative"
      div
        style:
          display: "inline-block"
          border: "1px solid #CCCCCC"
          borderRadius: "3px"
          padding: "3px 5px 6px"
        onClick: =>
          @setState
            open: !@state.open
        ul
          style:
            display: "inline-block"
            margin: 0
            padding: 0
            height: 23
            listStyle: "none"
          if (user_sites.length == 0 || all_checked) && (if @props.allChecked? then @props.allChecked() else true)
            #Show all because none are checked
            li
              className: "gray_box"
              @props.allText
          else
            if @props.getCheckedValues?
              ids = @props.getCheckedValues()
            else
              ids = difference(all_sites, user_sites)
            count = 0
            if ids.length == 0
              li
                className: "gray_box"
                if @props.noneSelectedText? then @props.noneSelectedText else "Unassigned"
            else
              for id in ids
                if @props.getObjectValid(id) and count <= 2
                  count++
                  name = @props.getObjectName(id)
                  if count > 2 and ids.length > 3
                    name = "& "+(ids.length-2)+" more"
                  li
                    key: id
                    className: "gray_box"
                    name
        ReactDOMFactories.i
          className: "fa " + (if !@state.open then "fa-chevron-down" else "fa-chevron-up")
          style:
            color: "#E2E2E2"
            margin: "0 10px"
            fontSize: 18
      if @state.open
        if @props.getCheckedValues?
          ids = @props.getCheckedValues()
        else
          ids = difference(all_sites, user_sites)
        div
          style:
            display: "block"
            position: "absolute"
            top: 40
            padding: 10
            zIndex: 999
            backgroundColor: "white"
            borderRadius: "4px"
            border: "1px solid #E2E2E2"
            boxShadow: "0px 2px 4px 0px rgba(0,0,0,0.23)"
            whiteSpace: "nowrap"
          span
            style:
              textTransform: "uppercase"
              fontSize: 11
              color: "#777777"
            @props.filterText
          ul
            style:
              margin: 0
              padding: 0
            if @props.isLoading? and @props.isLoading()
              li
                style:
                  padding: 5
                'loading more...'
            if !@props.showAll? || @props.showAll
              FilterOption
                key: "All"
                checked: all_checked
                name: "All"
                onChange: if @props.addAll? then @props.addAll else @addAll
            map(sortBy(map(all_sites, (id) =>
              checked: id in ids
              name: if @props.getObjectLongName then  @props.getObjectLongName(id) else @props.getObjectName(id)
              onChange: partial((if @props.onChange? then @props.onChange else @toggleSiteOnUser), id)
              key: id
              id: id
            ), (options) =>
              if @props.sortValue then @props.sortValue(options) else options.name.toLowerCase()
            ), (options) =>
              FilterOption(options)
            )


module.exports = onClickOutside(MultiFilter)
