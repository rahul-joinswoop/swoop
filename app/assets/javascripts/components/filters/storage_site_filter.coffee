import React from 'react'
import Consts from 'consts'
import { map } from 'lodash'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
SiteStore = require('stores/site_store').default
UserSettingStore = require('stores/user_setting_store')

class StorageSiteFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_STORAGE_SITES

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)
    if !SiteStore.isAllLoaded()
      @register(SiteStore, SiteStore.LOADED, @rerender)

  getName: (id) -> SiteStore.get(id, 'name')

  setItems: (ids, response) =>
    UserSettingStore.setUserSettings(@setting_name, ids, callbacks:response)

  render: ->
    super(arguments...)

    AllOptionMultiSelect
      objectName: "Site"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: map(SiteStore.loadAndGetAll(@getId(), 'name'), 'id')
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded() || !SiteStore.isAllLoaded()

module.exports = React.createFactory(StorageSiteFilter)
