import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
UsersStore = require('stores/users_store').default
UserStore = require('stores/user_store')
SettingStore = require('stores/setting_store')
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'

class DispatchFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_DISPATCHERS

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getName: (id) => UsersStore.get(id, "full_name", @getId())

  setItems: (items, response) =>
    UserSettingStore.setUserSettings(@setting_name, items, callbacks:response)

  render: ->
    super(arguments...)
    # Need to add check to see if UsersStore is loaded. Quinn says we need a special base store function to do this.
    AllOptionMultiSelect
      isVisible: SettingStore.getSettingByKey("Dispatcher Filter")?
      objectName: "Dispatcher"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: UsersStore.getDispatcherIds(@getId())
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded()
      withUnset: true
      unsetText: 'Unassigned'
      assignedToMeId: UserStore.getUser()?.id

module.exports = React.createFactory(DispatchFilter)
