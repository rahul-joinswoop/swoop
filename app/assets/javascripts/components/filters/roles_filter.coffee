import React from 'react'
import { capitalize, invert } from 'lodash'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
CompanyStore = require('stores/company_store').default
UserStore = require('stores/user_store')

class RolesFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)

  componentDidMount: ->
    super(arguments...)

  getName: (id) => capitalize(invert(@props?.roleIds)?[id])

  setItems: (items, response) =>
    CompanyStore.updateRolePermissions({
      permission_type: @props?.permissionType,
      role_ids: items?.selected || []
    }, response)

  getSelectedItems: =>
    companyRolePermissions = CompanyStore.get(UserStore.getCompany().id, "role_permissions", @getId())
    permittedRoleIds = []
    companyRolePermissions.forEach (permission) =>
      permittedRoleIds = permission?.role_ids if permission?.type == @props?.permissionType
    return {
      isAllSelected: permittedRoleIds?.length == Object.values(@props?.roleIds)?.length
      selected: permittedRoleIds
    }

  render: ->
    super(arguments...)

    AllOptionMultiSelect
      objectName: "User"
      selectedItems: @getSelectedItems()
      allItems: Object.values(@props?.roleIds)
      getName: @getName
      setItems: @setItems
      loading: false
      noSearch: true

module.exports = React.createFactory(RolesFilter)
