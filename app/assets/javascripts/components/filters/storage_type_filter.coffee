import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
UserSettingStore = require('stores/user_setting_store')
StorageTypeStore = require('stores/storage_type_store')
import Consts from 'consts'
import { map } from 'lodash'

class StorageTypeFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_STORAGE_TYPES

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)
    if !StorageTypeStore.isAllLoaded()
      @register(StorageTypeStore, StorageTypeStore.LOADED, @rerender)

  getName: (id) => StorageTypeStore.get(id, 'name', @getId())

  setItems: (ids, response) =>
    UserSettingStore.setUserSettings(@setting_name, ids, callbacks:response)

  render: ->
    super(arguments...)

    AllOptionMultiSelect
      objectName: "Storage Type"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: map(StorageTypeStore.loadAndGetAll(@getId(), 'name'), 'id')
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded() || !StorageTypeStore.isAllLoaded()
      withUnset: true

module.exports = React.createFactory(StorageTypeFilter)
