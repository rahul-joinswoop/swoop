import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
SiteStore = require('stores/site_store').default
UserStore = require('stores/user_store')
SettingStore = require('stores/setting_store')
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'
import { map } from 'lodash'

class DispatchSitesFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_DISPATCH_SITES

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getName: (id) => SiteStore.get(id, 'name')

  setItems: (items, response) =>
    UserSettingStore.setUserSettings(@setting_name, items, callbacks:response)

  render: ->
    super(arguments...)
    # Need to add check to see if SiteStore is loaded. Quinn says we need a special base store function to do this.
    AllOptionMultiSelect
      isVisible: UserStore.isPartner() and SettingStore.getSettingByKey("Dispatch Filter")?
      objectName: "Dispatch Site"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: map(SiteStore.getDispatchSites(@getId(), ['name', 'dispatchable']), 'id')
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded()
      withUnset: true

module.exports = React.createFactory(DispatchSitesFilter)
