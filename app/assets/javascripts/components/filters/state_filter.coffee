import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'
import { indexOf } from 'lodash'

class StateFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_STATES

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getName: (id) -> Consts.STATES[indexOf(Consts.STATE_ABREVS, id)]

  setItems: (items, response) =>
    UserSettingStore.setUserSettings(@setting_name, items, callbacks:response)

  render: ->
    super(arguments...)

    AllOptionMultiSelect
      objectName: "State"
      className: 'filter_states'
      allItems: Consts.STATE_ABREVS
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded()

module.exports = React.createFactory(StateFilter)
