import Consts from 'consts'
import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
UserSettingStore = require('stores/user_setting_store')
import { map } from 'lodash'

class InvoiceStatusFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_INVOICE_STATUS

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getName: (id) => if id? then (@props.options)[id]

  setItems: (items, response) =>
    UserSettingStore.setUserSettings(@setting_name, items, callbacks:response)

  render: ->
    super(arguments...)
    AllOptionMultiSelect
      isVisible: true
      objectName: "Invoice Status"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: map(@props.options, (value, key) -> key)
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded()
      defaultSelectedItem: 'Partner Sent'

module.exports = React.createFactory(InvoiceStatusFilter)
