import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
ServiceStore = require('stores/service_store')
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'

class ServicesFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_SERVICES

  componentDidMount: ->
    super(arguments...)
    #Force load all the services so we have them available in the drop down
    ServiceStore.getServices()
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getAll: =>
    non_addons = []
    ids = ServiceStore.getAllIds(true, @getId())
    if ids
      for id in ids
        if ServiceStore.get(id, "addon") != true
          non_addons.push(id)
    return non_addons
    #TODO non_addons should be made into a list in the ServicStore that you can just grab instead of filtering everytime.

  getName: (id) => ServiceStore.get(id, 'name', @getId())

  setItems: (items, response) =>
    UserSettingStore.setUserSettings(@setting_name, items, callbacks:response)

  render: ->
    super(arguments...)
    services = ServiceStore.getAllIds(true, @getId())

    AllOptionMultiSelect
      # Need to add check to see if ServiceStore is loaded. Quinn says we need a special base store function to do this.
      isVisible: services.length > 1
      objectName: "Service"
      className: "filter_services"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: @getAll()
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded()

module.exports = React.createFactory(ServicesFilter)
