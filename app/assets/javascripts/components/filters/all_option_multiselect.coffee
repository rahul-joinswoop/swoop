import React from 'react'
MultiSelect = React.createFactory(require ('components/multiselect'))
BaseComponent = require('components/base_component')
import { isEmpty, map, sortBy, union, uniq, without } from 'lodash'

class AllOptionMultiselect extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @unsetText = @props.unsetText || "Unselected"
    @state =
      changing_item: null

  getAllAndUnsetItems: (items, ids, data) =>
    if @props.withUnset
      data.unshift({
        name: @unsetText
        selected: isEmpty(items) || items?.isUnsetSelected
        loading: @unsetText == @state.changing_item
        disabled: @state.changing_item?
      })

    if @props.assignedToMeId
      data.unshift({
        name: 'Assigned To Me'
        selected: isEmpty(items) || items?.isAssignedToMeSelected
        loading: 'Assigned To Me' == @state.changing_item
        disabled: @state.changing_item?
      })

    data.unshift({
      name: "All"
      selected: (isEmpty(items) && !@props.defaultSelectedItem) || items.isAllSelected
      loading: "All" == @state.changing_item
      disabled: @state.changing_item?
    })

    return data

  getItems: =>
    items = @props.selectedItems || {}
    ids = @props.allItems

    data = sortBy(map(ids, (id) =>
      name = @props.getName(id)
      isSelected = false
      if isEmpty(items) && @props.defaultSelectedItem?
        isSelected = true if @props.defaultSelectedItem == name
      else if isEmpty(items) || items.isAllSelected || items.selected?.indexOf(id) != -1
        isSelected = true
      {
        name: name
        id: id
        selected: isSelected
        disabled: @state.changing_item?
        loading: name == @state.changing_item
      }
    ), (value) ->
      name = value.name
      if typeof name == 'string' || name instanceof String
        return name.toLowerCase()
      return name
    )
    return @getAllAndUnsetItems(items, ids, data)

  handleAllClick: (items, item) =>
    if item.selected
      items.isAllSelected = false
      items.isAssignedToMeSelected = false if @props.assignedToMeId
      items.isUnsetSelected = false if @props.withUnset
      items.selected = []
    else
      items.isAllSelected = true
      items.isUnsetSelected = true if @props.withUnset
      items.isAssignedToMeSelected = true if @props.assignedToMeId
      items.selected = @props.allItems
    return items

  handleAssignedToMeClick: (items, item) =>
    # If All is selected we want to have Assigned To Me and the id=item for assigned to me be the only selecteded item
    if items.isAllSelected || isEmpty(items)
      items.isAssignedToMeSelected = true
      items.isAllSelected = false
      items.isUnsetSelected = false if @props.withUnset
      items.selected = [@props.assignedToMeId]
    else
      if items.isAssignedToMeSelected
        items.isAssignedToMeSelected = false
        items.selected = without(items.selected, @props.assignedToMeId)
      else
        items.isAssignedToMeSelected = true
        items.selected = union(items.selected, [@props.assignedToMeId])
    return items

  handleUnselectedClick: (items, item) =>
    # If All is selected we want to have Unselected be the only selected item
    if items.isAllSelected || isEmpty(items)
      items.isUnsetSelected = true
      items.isAllSelected = false
      items.isAssignedToMeSelected = false if @props.assignedToMeId
      items.selected = []
    else
      items.isUnsetSelected = !items.isUnsetSelected
    return items

  handleNormalItemClick: (items, item) =>
    all_ids = @props.allItems
    #If All is selected we want to have the clicked on item be the only selected item

    if items.isAllSelected || isEmpty(items)
      items.selected = [item.id]
      items.isAllSelected = false
      items.isUnsetSelected = false if @props.withUnset
      if @props.assignedToMeId
        items.isAssignedToMeSelected = item.id == @props.assignedToMeId
    else
      if item.selected #Deselecting Item
        # new user so all items are checked, so selected should be all items except this one
        if isEmpty(items)
          items.isAllSelected = false
          items.selected = without(all_ids, item.id)
        else
          # not all items are check so just remove the id from the existing list
          items.selected = without(items.selected, item.id)
          items.isAssignedToMeSelected = false if @props.assignedToMeId && item.id == @props.assignedToMeId
      else #Selecting Item
        items.selected.push(item.id)
        # Ensure we don't have duplicates
        items.selected = uniq(items.selected)
        items.isAssignedToMeSelected = true if @props.assignedToMeId && item.id == @props.assignedToMeId
    return items

  toggleEntry: (item) =>
    items = @props.selectedItems || {}

    if item.name == "All"
      items = @handleAllClick(items, item)
    else if @props.assignedToMeId and item.name == 'Assigned To Me'
      items = @handleAssignedToMeClick(items, item)
    else if @props.withUnset and item.name == @unsetText
      items = @handleUnselectedClick(items, item)
    else
      items = @handleNormalItemClick(items, item)

    @setState
      changing_item: item.name

    @props.setItems(items, {
      success: =>
        @setState
          changing_item: null
      error: =>
        @setState
          changing_item: null
    })

  render: ->
    super(arguments...)
    return null if @props.isVisible == false
    MultiSelect
      className: @props.className
      objectName: @props.objectName
      items: @getItems()
      toggleEntry: @toggleEntry
      loading: @props.loading
      noSearch: @props.noSearch


module.exports = React.createFactory(AllOptionMultiselect)
