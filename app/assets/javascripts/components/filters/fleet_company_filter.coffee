import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
CompanyStore = require('stores/company_store').default
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'

class FleetCompanyFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_FLEET_COMPANIES_FOR_JOBS

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getName: (id) =>  CompanyStore.get(id, "name", @getId())

  setItems: (ids, response) =>
    UserSettingStore.setUserSettings(@setting_name, ids, callbacks:response)

  render: ->
    super(arguments...)
    AllOptionMultiSelect
      objectName: "Company"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: CompanyStore.getFleetManagedIds(@getId())
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded() || CompanyStore.isLoadingFleetManaged()

module.exports = React.createFactory(FleetCompanyFilter)
