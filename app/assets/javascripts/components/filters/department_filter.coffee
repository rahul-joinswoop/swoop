import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require('components/filters/all_option_multiselect'))
DepartmentStore = require('stores/department_store')
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'
import { map } from 'lodash'

class DepartmentFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_DEPARTMENTS

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getName: (id) => DepartmentStore.get(id, 'name', @getId())

  setItems: (items, response) =>
    UserSettingStore.setUserSettings(@setting_name, items, callbacks:response)

  render: ->
    super(arguments...)
    # Need to add check to see if all DepartmentStore ids are loaded. Quinn says we need a special base store function to do this.
    allDepartments = map(DepartmentStore.getDepartments(@getId()), 'id')
    AllOptionMultiSelect
      isVisible: allDepartments?.length > 1
      objectName: "Department"
      allItems: allDepartments
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded()

module.exports = React.createFactory(DepartmentFilter)
