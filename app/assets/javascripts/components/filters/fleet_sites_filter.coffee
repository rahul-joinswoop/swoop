import React from 'react'
BaseComponent = require('components/base_component')
AllOptionMultiSelect = React.createFactory(require ('components/filters/all_option_multiselect'))
UserStore = require('stores/user_store')
SiteStore = require('stores/site_store').default
FeatureStore = require('stores/feature_store')
UserSettingStore = require('stores/user_setting_store')
import Consts from 'consts'
import { map } from 'lodash'

class FleetSitesFilter extends BaseComponent
  constructor: (props) ->
    super(arguments...)
    @setting_name = props.setting_name || Consts.USER_SETTING.FILTERED_FLEET_SITES

  componentDidMount: ->
    super(arguments...)
    if !UserSettingStore.isAllLoaded()
      @register(UserSettingStore, UserSettingStore.LOADED, @rerender)

  getName: (id) -> SiteStore.get(id, 'name')

  setItems: (items, response) =>
    UserSettingStore.setUserSettings(@setting_name, items, callbacks:response)

  render: ->
    super(arguments...)
    filteredSites = []

    if UserStore.getUser()
      filteredSites = SiteStore.getFilteredSites(null, @getId(), 'name')

    AllOptionMultiSelect
      # Need to add check to see if SiteStore is loaded. Quinn says we need a special base store function to do this.
      isVisible: filteredSites.length > 1 and FeatureStore.isFeatureEnabled(Consts.FEATURES_FLEET_SITES) and !UserStore.isTesla()
      objectName: "User Site"
      selectedItems: UserSettingStore.getUserSettingByKeyAsObject(@setting_name, @getId())
      allItems: map(filteredSites, 'id')
      getName: @getName
      setItems: @setItems
      loading: !UserSettingStore.isAllLoaded()
      withUnset: true

module.exports = React.createFactory(FleetSitesFilter)
