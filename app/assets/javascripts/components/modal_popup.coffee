import React from 'react'
require('stylesheets/components/modal_popup.scss')
ModalStore = require('stores/modal_store')
BaseComponent = require('components/base_component')
onClickOutside = require('react-onclickoutside').default

class ModalPopup extends BaseComponent

  handleClickOutside: (e) ->
    ModalStore.hidePopup()

  render: ->
    div
      className: 'modalPopup'
      style:
        top: (if @props.target? then $(@props.target).offset().top else 0) + (if @props.offsetTop then @props.offsetTop else 0) + $('.wrapper').scrollTop()
        left: (if @props.target? then $(@props.target).offset().left else 0) + (if @props.offsetLeft then @props.offsetLeft else 0)
      @props?.getView()

module.exports = onClickOutside(ModalPopup)
