import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Loading from 'componentLibrary/Loading'
import Utils from 'utils'
import 'stylesheets/components/JobDetailCarouselItem.scss'

class JobDetailCarouselItem extends PureComponent {
  static propTypes = {
    onClick: PropTypes.func,
    photoItem: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      isLoaded: false,
    }
  }

  handleOnLoad = () => {
    this.setState({ isLoaded: true })
  }

  render() {
    const { photoItem, onClick } = this.props
    const containerProps = {
      className: 'job-detail-carousel-item-container',
    }
    if (!photoItem) {
      return null
    }
    if (onClick) {
      containerProps.onClick = onClick
    }

    return (
      <div {...containerProps}>
        <div className="image-container">
          <img
            alt={photoItem.created_at}
            className={this.state.isLoaded ? null : 'hidden'}
            height="120"
            onLoad={this.handleOnLoad}
            src={Utils.carouselImgURL(photoItem.url, 120, 120, true)}
            srcSet={Utils.carouselImgURL(photoItem.url, 240, 240, true, true)}
            width="120"
          />
          {!this.state.isLoaded && <Loading />}
        </div>
        <span className="date-time">{ Utils.formatDateTime(photoItem.created_at, true) }</span>
      </div>
    )
  }
}

export default JobDetailCarouselItem
