import React from 'react'
import { STRIPE_PUBLIC_KEY } from 'consts'
import { StripeProvider } from 'react-stripe-elements'

let stripeIsLoading = false
let stripeIsFullyLoaded = false

const StripeLoaded = Component => class StripeIsLoading extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      stripeIsFullyLoaded,
    }
  }

  componentDidMount() {
    if (!stripeIsLoading) {
      stripeIsLoading = true
      const fetchData = async () => {
        await this.loadScript('https://js.stripe.com/v3/')
        stripeIsFullyLoaded = true
        this.setState({
          stripeIsFullyLoaded,
        })
      }
      fetchData()
    }
  }

    loadScript = src => new Promise((resolve, reject) => {
      const script = document.createElement('script')
      script.src = src
      script.addEventListener('load', () => {
        resolve({ successful: true })
      })
      script.addEventListener('error', (error) => {
        reject(error)
      })
      document.head.appendChild(script)
    })

    render() {
      if (this.state.stripeIsFullyLoaded) {
        return (
          <StripeProvider apiKey={STRIPE_PUBLIC_KEY}>
            <Component {...this.props} />
          </StripeProvider>
        )
      } else {
        return null
      }
    }
}

export default StripeLoaded
