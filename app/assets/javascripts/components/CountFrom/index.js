
import React from 'react'
import BaseComponent from 'components/base_component'
import TimeStore from 'stores/time_store'
import moment from 'moment-timezone'

class CountFrom extends BaseComponent {
  render() {
    super.render(...arguments)

    TimeStore.watchMinute(this.getId())

    let el

    if (this.props.time) {
      const minutes = Math.ceil(moment().diff(this.props.time, 'minutes', true))

      if (!isNaN(minutes)) {
        if (minutes >= 60) {
          const hours = Math.floor(Math.abs(Math.floor(minutes)) / 60)
          el = <span className="Countdown-withHoursAndMinutes">{`${hours} hr ${Math.abs(minutes) % 60} min`}</span>
        } else if (minutes > 0) {
          el = <span className="Countdown-withMinutes">{`${minutes} min`}</span>
        } else {
          el = '--'
        }
      }
    }

    return (
      <span className="Countdown">
        {el}
      </span>
    )
  }
}

export default CountFrom
