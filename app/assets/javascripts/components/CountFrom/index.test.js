import React from 'react'
import { mount } from 'enzyme'
import CountFrom from 'components/CountFrom'
import moment from 'moment'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('CountFrom Component', () => {
  let wrapper

  it('renders the minutes only', () => {
    wrapper = mount(<CountFrom time={moment().subtract(3, 'minutes')} />)
    expect(wrapper.find('.Countdown-withHoursAndMinutes').exists()).toBe(false)
    expect(wrapper.find('.Countdown-withMinutes').exists()).toBe(true)
  })

  it('renders the hours and minutes', () => {
    wrapper = mount(<CountFrom time={moment().subtract(3, 'hours')} />)
    expect(wrapper.find('.Countdown-withHoursAndMinutes').exists()).toBe(true)
    expect(wrapper.find('.Countdown-withMinutes').exists()).toBe(false)
  })
})
