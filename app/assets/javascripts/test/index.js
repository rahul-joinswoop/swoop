import React from 'react'
import window from 'window'
import Application from 'components/Application'
import ReactDOM from 'react-dom'
import {
  render as testRender,
  fireEvent,
} from '@testing-library/react'

export default function render(node) {
  const { container } = testRender(<Application>{node}</Application>, {
    container: document.createElement('div'),
  })
  return container
}

export function contains(node, container) {
  const child = render(node)
  const a = container.innerHTML
  const b = child.innerHTML
  return a.indexOf(b) != -1
}

export function simulate(type, data, element) {
  fireEvent[type](element, data)
}
