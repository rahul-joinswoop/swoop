RateStore = require ('stores/rate_store')
PaymentStore = require('stores/payment_store')
FeatureStore = require('stores/feature_store')
InvoiceStore = require('stores/invoice_store')
UserStore = require('stores/user_store')
import PaymentMethodStore from 'stores/payment_method_store'
CompanyStore = require('stores/company_store').default
ServiceStore = require('stores/service_store')
import Consts from 'consts'
import moment from 'moment-timezone'
import DataUtils from 'DataUtils'
import Utils from 'utils'
import { isEmpty, filter, keys, map, sortBy } from 'lodash'

class InvoiceHelper
  constructor: (@form, @component, @rerender) ->
    if !@rerender
      @rerender = @component.rerender

  removeLineItem: (index) =>
    @form.record.line_items[index]?.deleted_at = moment().toISOString()
    @rerender()

  removePayment: (index) =>
    @form.record.payments[index]?.deleted_at = moment().toISOString()
    @rerender()

  isPaymentEditable: (index) =>
    payment = @form.record.payments[index]
    isExternalPayment = payment.charged_by_external_provider
    isExternalRefund = payment.refunded_by_external_provider
    return !isExternalRefund && !isExternalPayment

  shouldShowRefund: (index) =>
    payment = @form.record.payments[index]
    payment.charged_by_external_provider and !payment.refund_id? and DataUtils.shouldShowRefund(@component.getId())


  addTransaction: (transaction) =>
    #paymentStatus = PaymentStore.getPaymentStatusByName(paymentMethod)
    if !@form.record.payments?
      @form.record.payments = []

    @form.record.payments.push({
      memo: "",
      mark_paid_at: moment().toISOString(),
      type: transaction
      amount: null,
    })
    @rerender()

  addItemByValue: (value) =>
    @addItem(ServiceStore.getAddonByName(value)?.id, value, "")

  addItem: (id, value, qty=1, force=false) =>
    if value?
      for item in @form.record.line_items
        if not item.deleted_at? and item.description?.toLowerCase() == value.toLowerCase()
          return
      flat = null
      if id? && id != 0
        account_id = null
        if @form.record.job?.account?.id?
          account_id = @form.record.job.account.id
        rate = RateStore.getRateByValue(id, account_id)
        if !rate? and !force
          #fetch the rate for the service if its needed and force add when finished
          #if a fetch has already been attempted and no rate exists force add
          response = RateStore.getApiRates(account_id, id, () =>
            @addItem(id, value, qty, true)
          , () =>
            @addItem(id, value, qty, true)
          )
          if response != null
            @addItem(id, value, qty, true)
          return
        if rate? and rate.flat?
          flat = rate.flat
      obj = {
        description: value
        unit_price: flat,
        quantity: qty
        net_amount: flat
        estimated: false,
        user_created: true,
        job_id: @form.record.job_id
        unlocked: !flat
      }
      if id == 0 || id == '0'
        obj.custom = true
      @form.record.line_items.push(obj)
    else
      @form.record.line_items.push({
        net_amount: 0,
        estimated: false,
        user_created: true,
        job_id: @form.record.job_id
      })
    @rerender()


  getRegularLineItems: =>
    regs = {}
    if @form?.record?.line_items?
      for item, index in @form.record.line_items
        if !item.deleted_at? && (item.description != "Tax" || item.custom) && item.calc_type != "percent"
          regs[index] = item
    return regs


  getPercentLineItems: =>
    percents = {}
    if @form?.record?.line_items?
      for item, index in @form.record.line_items
        if !item.deleted_at? && item.calc_type == "percent"
          percents[index] = item
    return percents

  getTaxLineItems: =>
    taxes = {}
    if @form?.record?.line_items?
      for item, index in @form.record.line_items
        if !item.deleted_at? && item.description == "Tax" && !item.custom
          taxes[index] = item
    return taxes

  getAdditionalItems: (watcherId = null) =>
    addons = null
    if @form.record?.recipient?.company_id? and UserStore.isPartner() and !CompanyStore.isMotorClubId(@form.record.recipient.company_id, watcherId)
      addons = ServiceStore.getAddons(watcherId, @form.record.recipient.company_id)
    else
      addons = ServiceStore.getAddons(watcherId)

    add_items = []
    descriptions = map(@form.record.line_items, (item) ->
      if not item.deleted_at?
        item.description
    )

    for id, val of addons
      if val.name in descriptions
        continue

      add_items.push(val)

    add_items = sortBy(add_items, (addon) -> addon.name.toLowerCase())

    if FeatureStore.isFeatureEnabled(Consts.FEATURE_INVOICE_CUSTOM_ITEMS) && !InvoiceStore.isRecipientTesla(@form.record) && !Utils.isPartnerOnSwoopJob(UserStore, @form.record.job) && @form.record.job.type != Consts.FLEET_IN_HOUSE_JOB
      add_items.push({
        id: 0,
        name: "Custom Item"
      })

    return add_items

  prepareForSend: =>
    if @form.record.forceFullObject
      newrecord = @form.record
    else
      newrecord = @form.getRecordChanges()

    if newrecord.job?
      for prop of newrecord.job
        if prop not in ["id", "driver_notes", "invoice_vehicle_category_id"]
          delete newrecord.job[prop]

      if keys(newrecord.job).length == 1 and newrecord.job.id?
        delete newrecord.job.id

      if isEmpty(newrecord.job)
        delete newrecord.job

    if newrecord? and newrecord.payments?
      #Remove payments that aren't created yet and deleted
      newrecord.payments = filter(newrecord.payments, (payment) ->
        if !payment.id? && payment.deleted_at?
          return false
        return true
      )

      for item in newrecord.payments
        delete item.status
        if item.deleted_at
          #remove all other attributes if deleted
          for prop of item
            if prop != "id" && prop != "deleted_at"
              delete item[prop]

        if item.payment_method_id?
          item.payment_method = PaymentMethodStore.getById(item.payment_method_id).name
          delete item.payment_method_id

      if isEmpty(newrecord.payments)
        delete newrecord.payments

    delete newrecord.recipient
    if newrecord?.job? and not newrecord.job.id?
      if @form.record.job_id?
        newrecord.job.id = @form.record.job_id

    delete newrecord.forceFullObject
    if newrecord.shouldDeleteId
      delete newrecord.id
    delete newrecord.shouldDeleteId
    return newrecord

module.exports = InvoiceHelper
