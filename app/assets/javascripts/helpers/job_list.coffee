UserSettingStore = require('stores/user_setting_store')
SettingStore = require('stores/setting_store')
JobStore = require('stores/job_store')
FeatureStore = require('stores/feature_store')
UserStore = require('stores/user_store')
import Consts from 'consts'
import { filter, get, isArray, values } from 'lodash'
import Utils from 'utils'

class JobListHelper
  constructor: (watcherId) ->
    @watcherId = watcherId || null

  sortJobs: (jobs) =>
    return jobs.sort((a, b) ->
      a_date = Utils.getJobCreatedAt(a)
      b_date = Utils.getJobCreatedAt(b)
      if a.scheduled_for?
          a_date = a.scheduled_for
      if b.scheduled_for?
          b_date = b.scheduled_for
      return (new Date(a_date)).getTime() - (new Date(b_date)).getTime()
    )

  filterOnProp: (jobs, jobsProp, filteredItems) =>
    if filteredItems?
      jobs = filter(jobs, (job) ->
        jobPropValue = get(job, jobsProp)
        if !jobPropValue?
          jobPropValue = null
        return filteredItems.includes(jobPropValue)
      )
    return jobs

  filterOnUserSites: (jobs) =>
    mySites = UserStore.getMySiteIds()
    if !mySites? || mySites.length == 0
      return jobs
    jobs = filter(jobs, (job) ->
      if job?.fleet_site_id? and job.fleet_site_id in mySites
        return true
      return false
    )
    return jobs

  filterOnDispatchSites: (jobs, jobProp, doneTab=false) =>
    return jobs if doneTab
    userSettingsKey = Consts.USER_SETTING.FILTERED_DISPATCH_SITES
    if jobProp == "fleet_site_id"
      userSettingsKey = Consts.USER_SETTING.FILTERED_FLEET_SITES
    filteredDispatchSites = UserSettingStore.getUserSettingByKeyForFilterSearch(userSettingsKey, @watcherId)
    return @filterOnProp(jobs, jobProp, filteredDispatchSites)

  #TODO: Move site filter to job store
  #TODO: need to watch user
  filterOnSite: (jobs, doneTab=false, partnerDispatchSiteProp=null) =>
    if UserStore.isPartner() and !doneTab and SettingStore.getSettingByKey("Dispatch Filter")?
      jobs = @filterOnDispatchSites(jobs, partnerDispatchSiteProp)
    else if UserStore.isFleet() and FeatureStore.isFeatureEnabled(Consts.FEATURES_FLEET_SITES)
      jobs = @filterOnUserSites(jobs)
      jobs = @filterOnDispatchSites(jobs, "fleet_site_id", doneTab)
    return jobs

  filterOnServices: (jobs) =>
    if SettingStore.getSettingByKey("Filter by Service")?
      filteredServices = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_SERVICES, @watcherId)
      return @filterOnProp(jobs, "service_code_id", filteredServices)
    return jobs

  filterOnStates: (jobs) =>
    if SettingStore.getSettingByKey("Filter by State")?
      filteredStates = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_STATES, @watcherId)
      return @filterOnProp(jobs, "service_location.state", filteredStates)
    return jobs

  filterOnDispatcher: (jobs) =>
    user_id = UserStore.getUser()?.id

    filteredDispatchers = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_DISPATCHERS, @watcherId)
    #TODO PERFORMANCE PUT CHECK IN TO SEE IF ALL SITES ARE CHECKED AND SKIP
    if filteredDispatchers? and SettingStore.getSettingByKey("Dispatcher Filter")?
      jobProp = 'partner_dispatcher_id'
      if UserStore.isFleet()
        jobProp = 'fleet_dispatcher_id'
      else if UserStore.isSwoop()
        jobProp = 'root_dispatcher_id'
      return @filterOnProp(jobs, jobProp, filteredDispatchers)
    return jobs

  filterOnDepartments: (jobs) =>
    if FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS) and SettingStore.getSettingByKey("Filter by Department")?
      filteredDepartments = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_DEPARTMENTS, @watcherId)
      jobProp = "department_id"
      if UserStore.isPartner()
        jobProp = "partner_department_id"
      return @filterOnProp(jobs, jobProp, filteredDepartments)
    return jobs

  filterOnFleetCompanies: (jobs) =>
    if UserStore.isSwoop() and SettingStore.getSettingByKey("Filter by Fleet Company")?
      filteredFleetCompanies = UserSettingStore.getUserSettingByKeyForFilterSearch(Consts.USER_SETTING.FILTERED_FLEET_COMPANIES_FOR_JOBS, @watcherId)
      return @filterOnProp(jobs, "owner_company_id", filteredFleetCompanies)
    return jobs

  filterOnSearch: (jobs, activeSearch) =>
    if activeSearch?
      jobs = filter(jobs, (job) =>
        searchAttrs = Consts.JOB_SEARCH_ATTRIBUTES
        searches = []
        for attr in searchAttrs
          val = get(job, attr)

          #Ignore the keys if we have an object that we are search on
          if val? and typeof val == 'object'
            val = values(val)

          if val?
            str = JSON.stringify(val).toLowerCase()
            searches.push(str)
            if str.indexOf(activeSearch) >= 0
              if window.logging.frontendSearch
                console.log("Matched with ", attr, val, job)
              return true
          else
            if window.logging.frontendSearch
              console.log("Could not find match for ", searches, job)

        return false
      )
    jobs

  getTabTitle: (title, jobs) ->
    div null,
      span null,
        "Scheduled"
      span null,
        jobs.length

  getTabs: () =>
    tabList = []

    #TODO: pass watcher into these
    showingScheduled = SettingStore.getSettingByKey("Scheduled Tab")?
    showingPending = SettingStore.getSettingByKey("Pending Tab")?
    if showingScheduled
      #TODO: perhaps move filtering to job store
      jobs = @filterOnSite(JobStore.getScheduledJobs(@watcherId))
      tabList.push({
        name: "Scheduled"
        id: "scheduled"
        orderByDriver: UserStore.isPartner()
        showHeader: false
        jobs: jobs
      })

    pending_jobs = @filterOnSite(JobStore.getPendingJobs(showingScheduled, @watcherId))
    dispatched_jobs = @filterOnSite(JobStore.getDispatchedJobs(@watcherId))

    if showingPending
      tabList.push({
        name: "Pending"
        id: "pending"
        orderByDriver: false
        showHeader: false
        jobs: pending_jobs
      })

      tabList.push({
        name: "In-Progress"
        id: "active"
        jobs: dispatched_jobs
      })
    else
      jobs = @filterOnSite(JobStore.getActiveJobs(@watcherId))
      tabList.push({
        name: "Active"
        id: "active"
        jobs: pending_jobs.concat(dispatched_jobs)
      })

    return tabList


module.exports = JobListHelper
