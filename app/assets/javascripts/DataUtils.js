import React from 'react'
import Utils from 'utils'
import Consts from 'consts'
import UserStore from 'stores/user_store'
import CustomAccountStore from 'stores/custom_account_store'
import SiteStore from 'stores/site_store'
import LocationTypeStore from 'stores/location_type_store'
import InvoiceStore from 'stores/invoice_store'

// This is a place to put utility functions that merge data across multiple stores
// WARNING: DO NOT USE IN STORES OR UTILS, YOU WILL CREATE A CYCLE AND THINGS WILL BREAK

export function isBankingSetUp(watcherId) {
  return CustomAccountStore.isAccountLinked(watcherId)
}

export function shouldShowChargeCard(invoice_id, watcherId) {
  const invoice = InvoiceStore.getInvoiceById(invoice_id, true, watcherId)
  return (
    isBankingSetUp(watcherId) &&
      UserStore.hasPermissionsToCharge() &&
      Utils.getInvoiceBalance(invoice) > 0
  )
}

export function shouldShowRefund(watcherId) {
  return isBankingSetUp(watcherId) && UserStore.hasPermissionsToRefund()
}

export function renderDropLocationFieldForJobDetails(job, watcher) {
  let address, location_type

  if (job?.drop_location) {
    if (UserStore.isPartner() && job.status === Consts.AUTO_ASSIGNING) {
      const {
        street_name,
        city,
        state,
        zip,
      } = job.drop_location
      address = [street_name, city, state, zip].filter(el => !!el).join(', ')
    } else {
      address = SiteStore.getAddressWithSiteName(job.drop_location, watcher)
    }
  }

  if (!address) {
    address = null
  }

  if (job.drop_location?.location_type_id) {
    location_type = LocationTypeStore.getLocationTypeById(job.drop_location.location_type_id)?.name
  }

  if (location_type) {
    location_type = <span><br />({location_type})</span>
  }

  if (!location_type) {
    location_type = null
  }

  return <span>{address}{location_type}</span>
}

export function renderPickupLocationFieldForJobDetails(job, watcher) {
  let address, location_type

  if (job.service_location) {
    if (UserStore.isPartner() && job.status === Consts.AUTO_ASSIGNING) {
      const {
        street_name,
        city,
        state,
        zip,
      } = job.service_location
      address = [street_name, city, state, zip].filter(el => !!el).join(', ')
    } else {
      address = SiteStore.getAddressWithSiteName(job.service_location, watcher)
    }
  }

  if (!address) {
    address = null
  }

  if (job?.service_location?.location_type_id) {
    location_type = LocationTypeStore.getLocationTypeById(job.service_location.location_type_id)?.name
  } else {
    location_type = job?.location_type
  }

  if (location_type) {
    location_type = <span><br />({location_type})</span>
  }

  if (!location_type) {
    location_type = null
  }

  return <span>{address}{location_type}</span>
}

export function renderDropLocationFieldForChoosePartner(job, watcher) {
  let address, location_type

  if (job?.drop_location) {
    address = SiteStore.getAddressWithSiteName(job.drop_location, watcher)
  }

  if (!address) {
    address = null
  }

  if (job.drop_location?.location_type_id) {
    location_type = LocationTypeStore.getLocationTypeById(job.drop_location.location_type_id)?.name
  }

  if (location_type) {
    location_type = <span><br />({location_type})</span>
  }

  if (!location_type) {
    location_type = null
  }

  return <span>{address}{location_type}</span>
}

export function renderPickupLocationFieldForChoosePartner(job, watcher) {
  let address, location_type

  if (job.service_location) {
    address = SiteStore.getAddressWithSiteName(job.service_location, watcher)
  }

  if (!address) {
    address = null
  }

  if (job?.service_location?.location_type_id) {
    location_type = LocationTypeStore.getLocationTypeById(job.service_location.location_type_id)?.name
  } else {
    location_type = job?.location_type
  }

  if (location_type) {
    location_type = <span><br />({location_type})</span>
  }

  if (!location_type) {
    location_type = null
  }

  return <span>{address}{location_type}</span>
}

export default {
  isBankingSetUp,
  shouldShowChargeCard,
  shouldShowRefund,
  renderDropLocationFieldForJobDetails,
  renderPickupLocationFieldForJobDetails,
  renderDropLocationFieldForChoosePartner,
  renderPickupLocationFieldForChoosePartner,
}
