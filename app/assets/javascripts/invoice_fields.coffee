import React from 'react'
import Api from 'api'
import Button from 'componentLibrary/Button'
import Consts from 'consts'
import moment from 'moment-timezone'
import Utils from 'utils'
import { getCurrencyClass } from 'lib/localize'
UserStore = require('stores/user_store')
EdmundsStore = require('stores/edmunds_store')
RateStore = require ('stores/rate_store')
ServiceStore = require('stores/service_store')
FeatureStore = require('stores/feature_store')
CustomerTypeStore = require('stores/customer_type_store')
import PaymentMethodStore from 'stores/payment_method_store'
PaymentStore = require ('stores/payment_store')
CompanyStore = require('stores/company_store').default
InvoiceStore = require('stores/invoice_store')
VehicleCategoryStore = require('stores/vehicle_category_store')
AccountStore = require('stores/account_store')
JobStore = require('stores/job_store')
Fields = require('fields/fields')
AsyncRequestStore = require('stores/async_request_store').default
CreditCard = React.createFactory(require('components/credit_card'))
import { clone, cloneDeep, extend, filter, keys, isDate, map, partial, reject, uniq } from 'lodash'
import { mixin } from 'fields/common/field'
import RefundPopupImport from 'components/popups/refund_popup'
RefundPopup = React.createFactory(RefundPopupImport)

RecalcInvoiceMixin =
  handleSampleRate: (data) ->
    #This creates a new invoice to send up to the server
    if not data?
      data = {line_items: []}
    @props.form.removeMatchingFields("line_items.")

    line_items = @record.line_items
    if line_items?
      for item in line_items
        if @record.job?
          item.job_id=@record.job.id
        if item.user_created
          delete item.id
          data.line_items.push(item)

    data.state = @record.state
    data.qb_pending_import = @record.qb_pending_import
    data.qb_imported_at = @record.qb_imported_at

    if @record.job?
      data.job = {}
      data.job.id = @record.job.id
      data.job.driver_notes = @record.job.driver_notes
      data.job.invoice_vehicle_category_id = @record.job.invoice_vehicle_category_id
      data.job.account = @record.job.account
      data.job.history = @record.job.history


    data.payments = @record.payments
    if data.payments
      for payment in data.payments
        delete payment.id

    data.id = @record.id
    data.paid = @record.paid

    data.loading = false
    data.error = false
    data.forceFullObject = true
    data.shouldDeleteId = true

    @props.form.replaceRecord(data)

NetAmountMixin =
  alwaysBeforeSend: (newrecord) ->
    #If the actual value for the line item has not changed (based on id), don't send it up (delete it from the newrecord)
    original_line_item = @original?.line_items?[@_index]
    if original_line_item?
      id = original_line_item.id
      if id? and newrecord.line_items?
        for item, itemIndex in newrecord.line_items
          if item.id == id
            if Utils.toFixed(item.net_amount, 2) == Utils.toFixed(@original?.line_items?[@_index]?.net_amount, 2)
              delete item.net_amount
              if item.id? and keys(item).length == 1
                delete newrecord.line_items.splice(itemIndex, 1)
                if newrecord.line_items.length == 0
                  delete newrecord.line_items
            return

IsStorageMixin =
  isStorage: (index) ->
      @record.line_items[index].description == "Storage" and !@record.line_items[index].user_created

module.exports =

EmailToField : class EmailToField extends Fields.Field
  _name: "email_to"
  _label: "To:"
  findErrors: () -> Utils.multipleEmailsValidation(@getValue(), true)
  isShowing: -> UserStore.isPartner()
  prefill: =>
    if !@record.email_to and !@record.email_cc and !@record.email_bcc
      if @record.recipient_type == 'Account'
        @setValue(AccountStore.get(@record.recipient_id, 'accounting_email', @props.componentId))
      else if @record.recipient_type == 'User'
        job = JobStore.getJobById(@record.job_id)
        @setValue(job.email)

EmailCcField : class EmailCcField extends Fields.Field
  _name: "email_cc"
  _label: "Cc:"
  findErrors: () -> Utils.multipleEmailsValidation(@getValue(), false)
  isShowing: -> UserStore.isPartner()
  prefill: =>
    if !@record.email_to and !@record.email_cc and !@record.email_bcc and @record.recipient_type == 'Account'
      @setValue(AccountStore.get(@record.recipient_id, 'accounting_email_cc', @props.componentId))


EmailBccField : class EmailBccField extends Fields.Field
  _name: "email_bcc"
  _label: "Bcc:"
  isShowing: -> UserStore.isPartner()
  findErrors: () -> Utils.multipleEmailsValidation(@getValue(), false)
  prefill: =>
    if !@record.email_to and !@record.email_cc and !@record.email_bcc and @record.recipient_type == 'Account'
      @setValue(AccountStore.get(@record.recipient_id, 'accounting_email_bcc', @props.componentId))

DriverNotesField: class DriverNotesField extends Fields.TextareaField
  _name: "job.driver_notes"
  _label: "Invoice Notes"
  findErrors: () ->
    if @isRequired() && (!@getValue()? || @getValue().length == 0)
      return "Invoice Notes required when additional items added"
  isRequired: ->
    lineItems = filter(reject(@record.line_items, 'deleted_at'), 'user_created')
    if lineItems.length > 0 && Utils.isPartnerOnSwoopJob(UserStore, @record.job)
      return true
    if @record.rate_type == Consts.TESLA_RATE
      for item in @record.line_items
        if item.description in Consts.TESLA_ADDITIONS and !item.deleted_at?
          return true

RateTypeField : class RateTypeField extends Fields.SelectField
  _name: "rate_type"
  _label: "Rate Type"
  isEnabled: ->
    return Utils.isInvoiceEditable(@record) && !(Utils.isPartnerOnSwoopJob(UserStore, @record.job))
  alwaysBeforeSend: (newRecord) ->
    delete newRecord.loading
    delete newRecord.error
    if @record.shouldDeleteId
      delete newRecord.id
    delete @record.shouldDeleteId
    delete @record.forceFullObject

  forcePrefill: =>
    if !@allowedTypes and @record?.job?
      account_id = @record.job?.account_id
      service_code_id = @record.job?.service_code_id
      RateStore.getApiRates(account_id, service_code_id, partial(@setRateTypes.bind(@), account_id))
      RateStore.getApiRates(account_id, null, partial(@setRateTypes.bind(@), account_id))
      #If we don't need to load callback isn't called, so call setRateTypes immediately for that case
      @setRateTypes.call(@, account_id)
  onChange: (ratetype) ->
    @render_props?.onLoad?()

    super(ratetype)
    if ratetype?.target?
      ratetype = ratetype.target.value
    if !ratetype?
      ratetype = Consts.HOURLY_P2P

    companyId = if UserStore.isSwoop() then @record.sender.id else null

    Api.getSampleRates(@record.id, UserStore.getEndpoint(), companyId, ratetype, (if @record?.job?.invoice_vehicle_category_id? and @record.job.invoice_vehicle_category_id != "" then @record.job.invoice_vehicle_category_id else null),
      success: (data) =>
        @handleSampleRate(data)
        @render_props?.onComplete?()
      error: () =>
        @setValue(true, false, "error")
        @setValue(false, true, "loading")
        @props?.onError?()

    )
    @setValue(true, true, "loading")

  setRateTypes: (account_id) =>
    rates = RateStore.getRatesByObject(
      account_id:  account_id
    )
    @allowedTypes = uniq(map(rates, (rate) => rate.type))

  getOptions: =>
    options = []
    if UserStore.isPartner() and @record?.job?.account and AccountStore.isTeslaAccount(@record.job.account.id, @props.componentId)
      options.push({value: Consts.TESLA_RATE, text: Consts.RATE_TYPE_MAP[Consts.TESLA_RATE]})
      if @record?.rate_type? && @record?.rate_type != Consts.TESLA_RATE
        options.push({value: @record?.rate_type, text: Consts.RATE_TYPE_MAP[@record?.rate_type]})

      account_id = @record.job?.account_id
      service_code_id = @record.job?.service_code_id

      if @allowedTypes?
        for type in @allowedTypes
          if type != Consts.TESLA_RATE and type != @record?.rate_type
            options.push({value: type, text: Consts.RATE_TYPE_MAP[type]})

      return options

    for key, val of Consts.RATE_TYPE_MAP
      if key == Consts.STORAGE
        continue
      if key == Consts.TESLA_RATE && (UserStore.isSwoop() || (UserStore.isPartner() && @record?.job?.account && (!AccountStore.isTeslaMotorsAccount(@record.job.account.id, @props.componentId) || UserStore.isTesla())))
        continue
      options.push({value: key, text: val})

    return options

  mixin(@, RecalcInvoiceMixin)




InvoiceVehicleCategoryIdField : class InvoiceVehicleCategoryIdField extends Fields.SelectField
  _name: "job.invoice_vehicle_category_id"
  _label: "Class Type"
  isEnabled: ->
      return Utils.isInvoiceEditable(@record) && !(Utils.isPartnerOnSwoopJob(UserStore, @record.job))
  mixin(@, RecalcInvoiceMixin)
  onChange: (invoice_vehicle_category_id) ->
    super(invoice_vehicle_category_id)
    if invoice_vehicle_category_id?.target?
      invoice_vehicle_category_id = invoice_vehicle_category_id.target.value

    rate_type = @record.rate_type
    if !rate_type?
      rate_type = Consts.HOURLY_P2P

    @render_props?.onLoad?()

    companyId = if UserStore.isSwoop() then @record.sender.id else null

    Api.getSampleRates(@record.id, UserStore.getEndpoint(), companyId, rate_type, (if invoice_vehicle_category_id? and invoice_vehicle_category_id != "" then invoice_vehicle_category_id else null),
      success: (data) =>
        @handleSampleRate(data)
        @render_props?.onComplete?()
      error: (one, two, three) =>
        @setValue(true, false, "error")
        @setValue(false, true, "loading")
        @render_props?.onError?()

    )

    @setValue(true, true, "loading")

  getOptions: ->
    if UserStore.isSwoop() # when Swoop we use Partner's specific
      truckTypeIds = CompanyStore.get(@record?.sender?.id, 'vehicle_categories', @props.componentId)

    options = VehicleCategoryStore.getAllSorted(@props.componentId, truckTypeIds)


    if @record?.job?.invoice_vehicle_category_id?
      if map(options, (option) -> option.id).indexOf(@record.job.invoice_vehicle_category_id) == -1
        options.unshift(VehicleCategoryStore.getById(@record.job.invoice_vehicle_category_id, true, @props.componentId))

    options = @optionMapper(options, clone(super(arguments...)))
    return options

LineItemNameFactory: (item, index) ->
  name = "line_items."+index+".description"

  class LineItemNameField extends Fields.Field
    _name: name
    isRequired: ->
      !@record.line_items[index].deleted_at

    getErrorStyle: ->
      props = super(arguments...)
      props.left = "calc(50% + 5px)"

      props

    findErrors: ->
      if @record.line_items[index].custom && @getValue() == "Tax"
        return "Tax can not be added as a custom item"

    isEditable: ->
      editable = FeatureStore.isFeatureEnabled(Consts.FEATURE_INVOICE_CUSTOM_ITEMS) && @record.line_items[index].custom

      return editable

    #For Mobile
    allowEdits: ->
      @isEditable() == true

  LineItemNameField._name = name
  return LineItemNameField


#TODO: make this work for mobile

LineItemEstimatedFactory: (item, index) ->
  name = "line_items."+index+".auto_calculated_quantity"

  class LineItemEstimatedField extends Fields.ToggleButtonField
    mixin(@, IsStorageMixin)

    _name: name

    isShowing: ->
      return Utils.isInvoiceEditable(@record) and @isStorage(index)

    onToggle: (disabled) =>
      if !disabled
        @setValue(@record.line_items[index].original_quantity, false, "line_items."+index+".quantity")

      @setValue(!disabled)

    renderInput: ->
      #TODO: upgrade reactand replace with fragment
      [
        span null,
          "Auto Calculate Days"
        super(
          colors:
            inactive:
              base: 'rgb(170,170,170)',
              hover: 'rgb(170,170,170)',
            inactiveThumb:
              base: if !Utils.isInvoiceEditable(@record) then '#C5C5C5'
          value: @record.line_items[index].auto_calculated_quantity
          onToggle: @onToggle
        )
      ]

  LineItemEstimatedField._name = name
  return LineItemEstimatedField




LineItemQtyFactory: (item, index) ->
  name = "line_items."+index+".quantity"

  class LineItemQtyField extends Fields.Field
    mixin(@, IsStorageMixin)

    _name: name

    isEnabled: ->
      return Utils.isInvoiceEditable(@record) &&
        (if Utils.isPartnerOnSwoopJob(UserStore, @record.job) then item.user_created else true)

    onChange: (e) ->
      @setValue(false, false, "line_items."+index+".estimated")

      if @isStorage(index)
        @setValue(false, false, "line_items."+index+".auto_calculated_quantity")

      super(e)

    findErrors: ->
      if !@record.line_items[index].deleted_at && (isNaN(@getValue()) || @getValue() == "")
        return "Invalid value"

    getErrorStyle: ->
      props = super(arguments...)
      props.left = -25
      props.bottom = 7

      props

    getValue: ->
      val = super(arguments...)
      if val?
        return ""+val

  LineItemQtyField._name = name
  return LineItemQtyField


LineItemPriceFactory: (item, index) ->
  name = "line_items."+index+".unit_price"
  class LineItemPriceField extends Fields.Field
    _name: name
    getContainerClasses: ->
      super() + getCurrencyClass(@record.currency)

    isEnabled: ->
      return Utils.isInvoiceEditable(@record) &&
        (if Utils.isPartnerOnSwoopJob(UserStore, @record.job) then (item.user_created && item.unlocked) else true)

    findErrors: ->
      if !@record.line_items[index].deleted_at && isNaN(@getValue())
        return "Invalid value"

    getErrorStyle: ->
      props = super(arguments...)
      props.left = -25
      props.bottom = 7

      props

    onChange: (e) =>
      @setValue(false, false, "line_items."+index+".estimated")

      super(e)

    onBlur: =>
      val = @getValue()
      fval = parseFloat(val)
      if !isNaN(fval)
        @setValue(Utils.toFixed(fval, 2))

  LineItemPriceField._name = name
  return LineItemPriceField

LineItemTotalFactory: (item, index) ->
  name = "line_items."+index+".net_amount"
  class LineItemTotalField extends Fields.SpanField
    _name: name
    _index: index
    getContainerClasses: ->
      super() + getCurrencyClass(@record.currency)

    getValue: ->
      net_amount = parseFloat(@record.line_items[index].quantity)*parseFloat(@record.line_items[index].unit_price)
      if !isNaN(net_amount)
        @record.line_items[index].net_amount = Utils.toFixed(net_amount, 2)
      else
        @record.line_items[index].net_amount = 0


      endValue = super(arguments...)
      if endValue? && !isNaN(parseFloat(endValue))
        return Utils.toFixed(endValue, 2)
      else
        return endValue
    mixin(@, NetAmountMixin)


  LineItemTotalField._name = name
  return LineItemTotalField

SubTotalField: class SubTotalField extends Fields.SpanField
  _label: ""
  _name: "subtotal"
  getEditableClass: ->
    super() + getCurrencyClass(@record?.currency)

  getValue: ->
    total = 0
    if @record?.line_items?
      for line_item in @record.line_items
        if line_item.description != "Tax" && line_item.calc_type != "percent" && !line_item.deleted_at?
          amount = parseFloat(line_item.net_amount)
          if !isNaN(amount)
            total += amount

    return Utils.toFixed(total, 2)

LineItemPercentFactory: (item, index) ->
  name = "line_items."+index+".quantity"
  class LineItemPercentField extends Fields.Field
    _name: name
    isEnabled: ->
      return Utils.isInvoiceEditable(@record) &&
        (if Utils.isPartnerOnSwoopJob(UserStore, @record.job) then item.user_created else true)

    findErrors: ->
      if isNaN(@getValue())
        return "Invalid value"

    getErrorStyle: ->
      props = super(arguments...)
      props.left = -25
      props.bottom = 7

      props

    getValue: ->
      val = super(arguments...)
      return ""+val

    onChange: (e) ->
      @setValue(false, false, "line_items."+index+".estimated")
      super(e)

  LineItemPercentField._name = name
  return LineItemPercentField

LineItemPercentTotalFactory: (item, index) ->
  name =  "line_items."+index+".net_amount"
  class LineItemPercentTotalField extends Fields.SpanField
    _name: name
    _index: index
    mixin(@, NetAmountMixin)
    getEditableClass: ->
      super() + getCurrencyClass(@record?.currency)

    getValue: ->
      subtotal = 0
      percent = parseFloat(@record.line_items[index].quantity)
      if !isNaN(percent)
        if @record.line_items?
          for line_item in @record.line_items
            if (line_item.description == "Tax" || line_item.description == "Storage") && !line_item.user_created
              continue
            if line_item.calc_type == "percent" || line_item.deleted_at
              continue
            amount = parseFloat(line_item.net_amount)
            if !isNaN(amount)
              subtotal += amount
        total = subtotal * percent/100

        if !isNaN(total)
          @record.line_items[index].net_amount = Utils.toFixed(total, 2)
        return Utils.toFixed(total, 2)
      else
        @record.line_items[index].net_amount = 0

      val = super(arguments...)
      if val?
        return (val? && Utils.toFixed(val,2) || val)
      return "0.00"

  LineItemPercentTotalField._name = name
  return LineItemPercentTotalField


LineItemTaxInputFactory: (item, index) ->
  name = "line_items."+index+".tax_amount"
  class LineItemTaxInputField extends Fields.Field
    _name: name
    getEditableClass: ->
      super() + getCurrencyClass(@record?.currency)

    isEnabled: ->
      return Utils.isInvoiceEditable(@record) and CustomerTypeStore.get(@record.job?.customer_type_id, "name", @props.componentId) != Consts.CUSTOMER_TYPE_TAX_EXEMPT

    findErrors: ->
      if isNaN(@getValue())
        return "Invalid value"

    getErrorStyle: ->
      props = super(arguments...)
      props.left = -25
      props.bottom = 7

      props

    onBlur: =>
      val = @getValue()
      fval = parseFloat(val)
      if !isNaN(fval)
        @setValue(Utils.toFixed(fval,2))

    onChange: (e) ->
      @setValue(false, false, "line_items."+index+".estimated")
      super(e)

  LineItemTaxInputField._name = name
  return LineItemTaxInputField

LineItemTaxFactory: (item, index) ->
  name = "line_items."+index+".tax_amount"
  class LineItemTaxField extends Fields.SpanField
    _name: name
  LineItemTaxField._name = name
  return LineItemTaxField

TotalField: class TotalField extends Fields.SpanField
  _label: ""
  _name: "total_field"
  getEditableClass: ->
    super() + getCurrencyClass(@record?.currency)

  getValue: ->
    total = 0
    if @record?.line_items?
      for line_item in @record.line_items
        if !line_item.deleted_at?
          amount = parseFloat(line_item.net_amount)
          if !isNaN(amount)
            total += amount
          tax_amount = parseFloat(line_item.tax_amount)
          if !isNaN(tax_amount)
            total += tax_amount

    return Utils.toFixed(total,2)

StoredDateField: class StoredDateField extends Fields.SpanField
  _label: "Stored Date"
  _name: "job.history.Stored"
  isShowing: -> @getValue()?
  getValue: ->
    times = super(arguments...)
    if times?
      val = times.adjusted_dttm || times.last_set
      if val?
        Utils.formatDateTime(val, true)

ReleasedDateField: class ReleasedDateField extends Fields.SpanField
  _label: "Released Date"
  _name: "job.history.Released"
  isShowing: ->
    @getValue()?

  getValue: ->
    times = super(arguments...)
    if times?
      val = times.adjusted_dttm || times.last_set
      if val?
        Utils.formatDateTime(val, true)

PaymentTypeFactory: (item, index) ->
  name = "payments." + index + ".type"

  class PaymentTypeField extends Fields.SelectField
    _name: name
    _index: index
    _required: true
    isRequired: -> !@record?.payments?[index]?.deleted_at?
    getOptions: -> Consts.TRANSACTIONS
    getStyle: ->
      if @isEditable()
        return super(arguments...)
      else
        return {marginTop: 0}
    onChange: ->
      super(arguments...)
      @props.form.fields["payments.#{@_index}.amount"].fixDirection()
    isEditable: -> @render_props.helper.isPaymentEditable(index)

  PaymentTypeField._name = name
  return PaymentTypeField


PaymentNameFactory: (item, index) ->
  name = "payments." + index + ".payment_method"

  class PaymentNameField extends Fields.SelectField
    _name: name
    _index: index
    showRefund: false
    isShowing: -> @record.payments[index].type in Consts.TRANSACTIONS_WITH_METHOD
    getOptions: -> @optionMapper(map(PaymentMethodStore.getAll(), "name"), clone(super(arguments...)))
    onRefund: (data) =>
      refund = data.refund
      @record.payments[index].refund_id = refund.id
      @original.payments.push(refund)
      @record.payments.push(cloneDeep(refund))
      @showRefund = false
      @changeCallback?()
    onRefundCancel: =>
      @showRefund = false
      @changeCallback?()
    renderInput: ->
      if @render_props.helper.isPaymentEditable(index)
        super(arguments...)
      else
        payment =  @record.payments[index]
        [
          CreditCard
            brand: payment.card_brand
            last4: payment.card_charge_last_4_digits
          if payment.charged_by_external_provider
            span
              className: 'charged',
              "Charged"
          else if payment.refunded_by_external_provider
            span
              className: 'refunded',
              "Refunded"
          if @render_props.helper.shouldShowRefund(index)
            Button
              color: 'cancel'
              label: 'Refund'
              onClick: =>
                @showRefund = true
                @changeCallback?()
              size: 'small narrow'
          if @showRefund
            RefundPopup
              payment_id: payment.id
              job_id: @record.job_id
              invoice_id: @record.id
              last4: payment.card_charge_last_4_digits
              brand: payment.card_brand
              amount: payment.amount
              onCancel: @onRefundCancel
              onRefund: @onRefund

        ]
  PaymentNameField._name = name
  return PaymentNameField

PaymentMemoFactory: (item, index) ->
  name = "payments." + index + ".memo"

  class PaymentMemoField extends Fields.Field
    _name: name
    _index: index

  PaymentMemoField._name = name
  return PaymentMemoField

PaymentDateFactory: (item, index) ->
  name = "payments." + index + ".mark_paid_at"

  class PaymentDateField extends Fields.DateField
    _name: name
    _index: index
    _className: 'payment-date-field'
    isRequired: -> !@record?.payments?[index]?.deleted_at?
    onChange: (value) ->
      @setValue(if moment.isMoment(value) then value.toISOString() else value)
    getStyle: ->
      if @isEditable()
        return super(arguments...)
      else
        return {marginTop: 0}
    getValue: ->
      if @isEditable()
        moment(super(arguments...))
      else
        Utils.formatDate(super(arguments...), true)
    isEditable: -> @render_props?.helper?.isPaymentEditable(index)

  PaymentDateField._name = name
  return PaymentDateField

PaymentAmountFactory: (item, index) ->
  name = "payments." + index + ".amount"

  class PaymentAmountField extends Fields.Field
    _name: name
    _index: index
    isRequired: -> !@record?.payments?[index]?.deleted_at?
    forcePrefill: ->
      @onBlur()

    getEditableClass: ->
      super() + getCurrencyClass(@record?.currency)

    getStyle: ->
      if @isEditable()
        return super(arguments...)
      else
        return {marginTop: 0}

    onBlur: =>
      val = parseFloat(@record.payments[index].amount)
      if !isNaN(val)
        @setValue(Utils.toFixed(val,2))

    getValue: ->
      val = super(arguments...)

      if val
        val = ""+val
        if val.startsWith("-")
          return val.substring(1)
        else if Utils.toFixed(val, 2) == '0.00'
          return Utils.toFixed(val, 2)
        else
          return "-"+val


    findErrors: ->
      if @isRequired() && isNaN(@getValue())
        return "Invalid value"

      if Utils.toFixed(@getValue(), 2) == '0.00'
        return Consts.PAYMENT_CANNOT_BE_ZERO

    getErrorStyle: ->
      props = super(arguments...)
      props.left = -25
      props.bottom = 7

      props

    onChange: (e) ->
      super(e)

      @fixDirection()

    alwaysBeforeSend: (newrecord) ->
      original_payment = @original?.payments?[index]
      if original_payment?
        id = original_payment.id
        if id? and newrecord.payments?
          for item, itemIndex in newrecord.payments
            if item.id == id
              if Math.abs(Math.abs(parseFloat(item.amount)) - Math.abs(parseFloat(@original?.payments?[index]?.amount))) < 0.001
                delete item.amount
                if item.id? and keys(item).length == 1
                  delete newrecord.payments.splice(itemIndex, 1)
                  if newrecord.payments.length == 0
                    delete newrecord.payments
              return
    fixDirection: ->
      val = @record.payments[index].amount
      if val
        if(@record.payments[index].type in Consts.TRANSACTIONS_NEGATIVE)
          if val.startsWith("-")
            @setValue(val.substring(1))
        else
          if !val.startsWith('-')
            @setValue("-"+val)

    isEditable: -> @render_props.helper.isPaymentEditable(index)



  PaymentAmountField._name = name
  return PaymentAmountField

BalanceField: class BalanceField extends Fields.SpanField
  _name: "balance"
  getStyle: ->
    if @getValue() < 0
      extend({}, super(arguments...), { color: "red" })
    else
      extend({}, super(arguments...))

  addErrorBorder: (style) -> style

  getErrorStyle: ->
    props = super(arguments...)
    props.right = 105
    props.bottom = 0

    props

  getValue: -> Utils.getInvoiceBalanceStr(@record)

  renderInput: ->
    span
      className: "field-span" + getCurrencyClass(@record.currency)
      style: Utils.combineStyles(display: "block", @getStyle())
      if @getViewValue then @getViewValue() else @getValue()

BillingTypeField: class BillingTypeField extends Fields.SelectField
  _className: "BillingTypeField form-control"
  _name: 'billing_type'
  _label: 'Billing Type'

  isEditable: () -> UserStore.isSwoop() and @render_props?.editable

  getOptions: ->
    options = []
    if !Consts.JOB.BILLING_TYPE[@getValue()]
      options.push(disabled: true, hidden: true, text: 'Select', value: 'Not Set')

    options.concat(Object.values(Consts.JOB.BILLING_TYPE))

  getStyle: ->
    if UserStore.isSwoop() and @getValue() == 'Not Set'
      Utils.combineStyles(super(arguments...), { color: '#c4c4c4' })
    else
      super(arguments...)

  getValue: -> super(arguments...) or 'Not Set'
