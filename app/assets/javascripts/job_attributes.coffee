import Utils from 'utils'
UserStore = require('stores/user_store')
LocationTypeStore = require('stores/location_type_store')
TireTypeStore = require('stores/tire_type_store')
SiteStore = require('stores/site_store').default
import { renderDropLocationFieldForChoosePartner, renderPickupLocationFieldForChoosePartner } from 'DataUtils'
import { filter, map } from 'lodash'

Attrs = {
  details: {header: "Job Details:", value: ((job, watcher) -> job?.notes)}
  questions: {header: "Questions:", value: ((job, watcher) ->
    answers = []
    if job?.question_results?
      for answer in job.question_results
        if answer.question and answer.answer
          answers.push(answer)

    if answers.length == 0
      return

    ul null,
      for answer in answers
        li
          key: answer.answer_id
          Utils.getQuestionResultSpan(job?.owner_company?.name, answer)
  )}
  symptom: {header: Utils.getSymptomLabel(UserStore, ":"), value: ((job, watcher) -> job.symptom)}
  service: {header: "Service:", value: ((job, watcher) -> job.service)}
  service_location: {header: "Location:", value: ((job, watcher) ->
    renderPickupLocationFieldForChoosePartner(job, watcher)
  )}
  drop_location : {
    header: "Drop Off:",
    value: (job, watcher) ->
      renderDropLocationFieldForChoosePartner(job, watcher)
  }
  pickup_to_dropoff : {
    header: "Pickup to Drop Off:"
    value: (job) ->
      if job.distance?
        job.distance + ' miles'
  }

  vehicle: {header: "Vehicle:", value: (job, watcher) ->
    Utils.getVehicleName(job, watcher)
  }

  tire_size: {header: "Tire Size:", show: ((job, watcher) -> job?.tire_type_id?), value: (job, watcher) ->
    return TireTypeStore.getTireTypeName(job?.tire_type_id, watcher)
  }

  payment_type: {header: "Payment Type:", value: ((job, watcher) ->
      name = CustomerTypeStore.get(job?.customer_type_id, "name", watcher)
      if type?
        span
          style:
            fontWeight: if type.name == "Customer Pay" then 900
            color:  if type.name == "Customer Pay" then "#f00"
          type.name
    ), show: job?.customer_type_id?}


  pickup_contact_name: {header: "Pickup Contact:", value: ((job, watcher) -> job?.pickup_contact?.full_name)}
  pickup_contact_phone: {header: "Pickup Phone:", value: ((job, watcher) -> Utils.renderPhoneNumber(job.pickup_contact.phone)), show: ((job, watcher) -> job?.pickup_contact?.phone?)}
  customer_name: {
    header: ((job, watcher) -> Utils.getCustomerNameLabel(UserStore, job, ": ")),
    value: ((job, watcher) -> job.customer.full_name),
    show: (job, watcher) -> (job?.customer? and not job?.pickup_contact?)
  } #hid if pickup iss set
  customer_phone: {
    header: ((job, watcher) -> Utils.getCustomerPhoneLabel(UserStore, job, ": ")),
    value: ((job, watcher) -> Utils.renderPhoneNumber(job.customer.phone)),
    show: (job, watcher) -> (job?.customer? and not job?.pickup_contact?)}  #hid if pickup iss set
}

getOrExecute = (func, job) ->
  if typeof(func) == "function"
    func = func(job)
  return func

module.exports = {
  getJobAttributes: (job, props, watcher) ->
    filter(map(props, (prop) ->
      if Attrs[prop]?
        show = getOrExecute(Attrs[prop].show, job, watcher)
        if !show?
          show = true
        if !show
          return {
            show: false
          }

        value = getOrExecute(Attrs[prop].value, job, watcher)
        if !value? || value == "" || value == "null"
          return {
            show: false
          }

        return {
          header: getOrExecute(Attrs[prop].header, job, watcher)
          value: value
        }
      console.warn("MISSING JOB ATTRIBUTE ", prop)
    ), (val) -> val.show != false)
}
