import Consts, { WEBSOCKET_URL, JS_ENV } from 'consts'
import $ from 'jquery'
import EventStore from 'stores/event_store'
import UserStore from 'stores/user_store'
import UsersStore from 'stores/users_store'
import CompanyStore from 'stores/company_store'
import JobStore from 'stores/job_store'
import ServiceStore from 'stores/service_store'
import InvoiceStore from 'stores/invoice_store'
import EdmundsStore from 'stores/edmunds_store'
import DownloadStore from 'stores/download_store'
import NotificationStore from 'stores/notification_store'
import AuctionStore from 'stores/auction_store'
import CustomAccountStore from 'stores/custom_account_store'
import PoiStore from 'stores/poi_store'
import SiteStore from 'stores/site_store'
import ProviderStore from 'stores/provider_store'
import AccountStore from 'stores/account_store'
import TireStore from 'stores/tire_store'
import VehicleStore from 'stores/vehicle_store'
import RateStore from 'stores/rate_store'
import tracking from 'tracking'
import TrailerStore from 'stores/trailer_store'
import UserSettingStore from 'stores/user_setting_store'
import AsyncRequestStore from 'stores/async_request_store'
import DepartmentStore from 'stores/department_store'
import VersionStore from 'stores/version_store'
import TagStore from 'stores/tag_store'
import LocationTypeStore from 'stores/location_type_store'
import Dispatcher from 'lib/swoop/dispatcher'
import { clone } from 'lodash'

class ApplicationEvents {
  static initClass() {
    this.registerForServerUpdates = (user) => {
      const url = `${WEBSOCKET_URL}/partner_fleet/event_${user.company.channel_name}`
      return EventStore.connect(url)
    }

    this.jobUpdate = (msg) => {
      if (msg.changes?.status != null) {
        console.log('Job Status Change: ', msg.changes, msg)
      }

      return JobStore.addRemoteItem(msg)
    }

    this.serverTimeSet = () => {
      this.serverTime = true

      if (this.registered === false) {
        return this.userLoggedIn()
      }
    }

    this.onMessage = (msg) => {
      let obj

      switch (msg.class) {
        case 'FleetInHouseJob':
        case 'FleetManagedJob':
        case 'RescueJob':
        case 'Job':
        case 'FleetMotorClubJob':
          return this.jobUpdate(msg)

        case 'Rate':
        case 'HoursP2PRate':
        case 'MilesTowedRate':
        case 'MilesEnRouteRate':
        case 'MilesEnRouteOnlyRate':
        case 'MilesP2PRate':
        case 'FlatRate':
        case 'StorageRate':
        case 'TeslaRate':
          // TODO once we have a RateStore, bind the added event to ProviderStore
          if (UserStore.isFleet()) {
            if (msg.target.company_id != null) {
              const provider = ProviderStore.getProviderByCompanyId(msg.target.company_id)

              if (provider) {
                msg.target.provider_id = provider.id
              }
            } else {
              const rate = RateStore.getById(msg.target.id)

              if (rate) {
                msg.target.provider_id = rate.provider_id
              }
            }
          }

          return RateStore.addRemoteItem(msg)

        case 'RescueVehicle':
          return VehicleStore.addRemoteItem(msg)

        case 'TrailerVehicle':
          return TrailerStore.addRemoteItem(msg)

        case 'Version':
          return VersionStore.addRemoteItem(msg)

        case 'Invoice':
          return Dispatcher.send(InvoiceStore.INVOICE_UPDATE, msg.target)

        case 'PartialObjectArray':
          if (msg.target.partial_invoices != null) {
            return InvoiceStore.addPartialInvoices(msg.target.partial_invoices)
          }

          break

        case 'LocationType':
          switch (msg.operation) {
            case 'create':
              return LocationTypeStore.locationTypeAdded(msg.target)
          }

          break

        case 'Account':
          switch (msg.operation) {
            case 'create':
              return AccountStore.addRemoteItem(msg)

            case 'update':
              return AccountStore.addRemoteItem(msg)
          }

          break

        case 'User':
          return UsersStore.addRemoteItem(msg)

        case 'FleetCompany':
        case 'RescueCompany':
        case 'Company':
        case 'SuperCompany':
          return CompanyStore.addRemoteItem(msg)

        case 'CompaniesService':
          return ServiceStore.addRemoteItem(msg)

        case 'RescueProvider':
          return ProviderStore.addRemoteItem(msg)

        case 'PartnerSite':
        case 'Site':
        case 'FleetSite':
          return SiteStore.addRemoteItem(msg)

        case 'PoiSite':
          return PoiStore.addRemoteItem(msg)

        case 'Explanation':
          return JobStore.addRemoteExplanation(msg.target)

        case 'Image':
          return JobStore.addRemoteImage(msg.target)

        case 'AttachedDocument':
        case 'AttachedDocument::LocationImage':
        case 'AttachedDocument::SignatureImage':
          return JobStore.addRemoteDocument(msg.target)

        case 'InventoryItem':
          if (msg.target.item.type === 'Vehicle') {
            // bypass it, since we don't use it for now
          } else if (msg.target.item.type === 'Tire') {
            return TireStore.updateTiresBySiteList(msg.target)
          }

          break

        case 'Review':
        case 'NpsReview':
        case 'NpsChooseReview':
        case 'NpsLinkReview':
        case 'NpsPureReview':
        case 'Reviews::Typeform':
        case 'Reviews::SurveyReview':
          return JobStore.addRemoteReview(msg.target)

        case 'Issc':
          switch (msg.operation) {
            case 'error':
              obj = {
                type: Consts.ERROR,
                message: msg.target.error,
                dismissable: true,
              }
              return Dispatcher.send(NotificationStore.NEW_NOTIFICATION, obj)
          }

          break

        case 'TimeOfArrival':
          switch (msg.operation) {
            case 'create':
              return JobStore.handleRemoteTOA(msg.target, false)

            case 'update':
              return JobStore.handleRemoteTOA(msg.target, true)
          }

          break

        case 'TwilioReply':
          return JobStore.addRemoteTwilioReply(msg.target, true)

        case 'ReportResult':
          return DownloadStore.addReportResult(msg.target)

        case 'Notification':
          return Dispatcher.send(NotificationStore.NEW_NOTIFICATION, msg.target)

        case 'UserSetting':
          if (UserStore.getUser()?.id === msg.target.user_id) {
            return UserSettingStore.addRemoteItem(msg)
          }

          break

        case 'Auction::Auction':
          switch (msg.operation) {
            case 'create':
              return Dispatcher.send(AuctionStore.REMOTE_AUCTION_ADDED, msg)

            case 'update':
              return Dispatcher.send(AuctionStore.REMOTE_AUCTION_UPDATED, msg)
          }

          break

        case 'Auction::Bid':
          switch (msg.operation) {
            case 'update':
              return Dispatcher.send(AuctionStore.REMOTE_BID_UPDATED, msg)
          }

          break

        case 'CustomAccount':
          return CustomAccountStore.addRemoteItem(msg)

        case 'Department':
          return DepartmentStore.addRemoteItem(msg)

        case 'AsyncRequest':
          return AsyncRequestStore.addRemoteItem(msg)

        case 'Tag':
          return TagStore.addRemoteItem(msg)

        case 'ServiceCodeIdList': {
          obj = msg.target
          let company = CompanyStore.getById(obj.company_id)

          if (company != null) {
            company = clone(company)

            if (obj.addon) {
              company.addon_ids = obj.ids
            } else {
              company.service_ids = obj.ids
            }

            return CompanyStore.addRemoteItem({
              target: company,
            })
          }

          break
        }
        default:
          if (JS_ENV !== 'production') {
            return alert(`unknown message received: ${msg.class}`)
          }
      }
    }

    this.userLoggedIn = () => {
      const user = UserStore.getUser()

      if (user && this.serverTime) {
        this.registered = true
        this.registerForServerUpdates(user)
        return EdmundsStore.setupEdmunds()
      } else {
        return (this.registered = false)
      }
    }

    this.userLoggedOut = () => {
      'userLoggedOutCalled'

      return EventStore.close()
    }
  }
}

ApplicationEvents.initClass()
UserStore.bind(UserStore.LOGIN, ApplicationEvents.userLoggedIn)
UserStore.bind(UserStore.LOGOUT, ApplicationEvents.userLoggedOut)
EventStore.bind(EventStore.CHANGE, ApplicationEvents.onMessage)
$(window).focus(() => tracking.track('App Opened'))

export default ApplicationEvents
