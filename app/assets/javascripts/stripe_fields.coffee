import React from 'react'
import Consts from 'consts'
import moment from 'moment-timezone'
import { extend } from 'lodash'
import { mixin } from 'fields/common/field'
Fields = require 'fields/fields'

MaxLengthMixin=(length) ->
  getInputProps: ->
    #Hack so I can call super(arguments...)
    props = Object.getPrototypeOf(@constructor).prototype.getInputProps.apply(this, arguments)
    props.maxLength = length
    props

module.exports =
  BusinessNameField: class BusinessNameField extends Fields.Field
    _name: 'business_name'
    _label: 'Business Name'
    _required: true

  EmployerIdNumber: class EmployerIdNumber extends Fields.Field
    _name: 'business_tax_id'
    _label: 'Employer ID Number (EIN)'
    _required: true

  FirstNameField: class FirstNameField extends Fields.Field
    _name: 'first_name'
    _label: 'First Name'
    _required: true

  LastNameField: class LastNameField extends Fields.Field
    _name: 'last_name'
    _label: 'Last Name'
    _required: true

  SSNLastFourDigitsField: class SSNLastFourDigitsField extends Fields.Field
    _name: 'ssn_last_4'
    _label: 'Last 4 of Social Security Number'
    _required: true
    findErrors: () ->
      if @getValue()?.length < 4
        return 'Enter the last four digits of the Social Security Number'
      super(arguments...)
    mixin(@, MaxLengthMixin(4))

  DateOfBirthField: class DateOfBirthField extends Fields.DateField
    _name: 'date_of_birth'
    _label: 'Date of Birth'
    _required: true
    _className: 'date-of-birth'
    _originalValueChanged: false
    getInputProps: ->
      extend({}, super(arguments...), {
        maxDate: moment()
      })
    onChange: (value) ->
      if value?
        @setValue(value.toISOString())
      else
        @setValue(null)
    getValue: ->
      val = super(arguments...)

      if(val)
        return moment(val)
      else
        return null

  BusinessStreetAddress: class BusinessStreetAddress extends Fields.Field
    _name: 'address_line1'
    _label: 'Business Street Address'
    _required: true

  BusinessCity: class BusinessCity extends Fields.Field
    _name: 'address_city'
    _label: 'City'
    _required: true

  BusinessState: class BusinessState extends Fields.SelectField
    _name: 'address_state'
    _label: 'State'
    _required: true
    getOptions: ->
      @optionMapper(
        Consts.STATE_ABREVS,
        [{ value: null, text: "- Select -" }]
      )

  BusinessZipCode: class BusinessZipCode extends Fields.Field
    _name: 'address_postal_code'
    _label: 'Zip Code'
    _required: true
    mixin(@, MaxLengthMixin(10))

  PersonalStreetAddress: class PersonalStreetAddress extends Fields.Field
    _name: 'personal_address_line1'
    _label: 'Personal Street Address'
    _required: true

  PersonalCity: class PersonalCity extends Fields.Field
    _name: 'personal_address_city'
    _label: 'City'
    _required: true

  PersonalState: class PersonalState extends Fields.SelectField
    _name: 'personal_address_state'
    _label: 'State'
    _required: true
    getOptions: ->
      @optionMapper(
        Consts.STATE_ABREVS,
        [{ value: null, text: "- Select -" }]
      )

  PersonalZipCode: class PersonalZipCode extends Fields.Field
    _name: 'personal_address_postal_code'
    _label: 'Zip Code'
    _required: true
    mixin(@, MaxLengthMixin(10))

  BankRoutingNumber: class BankRoutingNumber extends Fields.Field
    _name: 'bank_routing_number'
    _label: 'Routing Number'
    _className: 'form-control stripe-bank-routing-number-field'
    _required: true
    mixin(@, MaxLengthMixin(9))

  BankAccountNumber: class BankAccountNumber extends Fields.Field
    _name: 'bank_account_number'
    _label: 'Account Number'
    _required: true
    _className: 'form-control stripe-bank-account-field'
    _requiredFieldBorderStyle: "2px solid #b3e3a0"
