/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import Api from 'api'

import Dispatcher from 'lib/swoop/dispatcher'

import MicroEvent from 'microevent-github'
import MobileUtils from 'mobile/MobileUtils'
import VehicleStore from 'stores/vehicle_store'
import Logger from 'lib/swoop/logger'
import UserStore from 'stores/user_store'

const CHANGE = 'change'
const JOB_CHANGE = 'job_change'
const LOADED = 'loaded'

const LocationStore = function LocationStore() {
  let job = null
  let location = null
  let address = null
  const store = {
    CHANGE,
    JOB_CHANGE,
    LOADED,
    load() {
      return Api.getCustomerJob(MobileUtils.getUUIDFromPath(), {
        success: (data) => {
          job = data

          if (job.recipient != null) {
            UserStore.segmentIdentify(job.recipient, job)
          }

          VehicleStore._add(job.rescue_vehicle)

          trigger(CHANGE)
          return trigger(LOADED)
        },
      })
    },

    getJob() {
      return job
    },

    setJobStatus(new_job) {
      job.status = new_job.status
      return trigger(JOB_CHANGE)
    },

    setLocation(loc) {
      return (location = loc)
    },

    getLocation() {
      return location
    },

    getAddress() {
      return address
    },

    setAddress(a) {
      return (address = a)
    },

    updateTimeOfArrival(data) {
      if (job == null) {
        job = {}
      }

      if (job.toa == null) {
        job.toa = {}
      }

      switch (data.eba_type) {
        case 0:
          job.toa.latest = data.time
          break

        case 1:
          job.toa.live = data.time
          break

        case 2:
          job.toa.actual = data.time
          break

        case 3:
          job.toa.latest = data.time
          break

        case 4:
          job.toa.latest = data.time
          break
      }

      return trigger(CHANGE)
    },

    updateLocation(pos) {
      Logger.debug('updating location in store')
      location = {
        lat: pos?.coords?.latitude,
        lng: pos?.coords?.longitude,
      }
      trigger(CHANGE)

      if (pos && location == null) {
        window?.analytics?.track('Mobile Location Accept')
      }
    },

    watchLocation(errorCallback) {
      window?.analytics?.track('Mobile Location Request')
      if (navigator.geolocation?.getCurrentPosition != null) {
        navigator.geolocation.getCurrentPosition(this.updateLocation, errorCallback)
      }

      if (navigator.geolocation?.watchPosition != null) {
        return navigator.geolocation.watchPosition(this.updateLocation, errorCallback)
      }
    },
  }

  MicroEvent.mixin(store)
  const trigger = store.trigger.bind(store)
  Dispatcher.register((payload) => {
    Logger.debug('in payload of vehicle store dispatcher', payload)

    if (!payload.eventName) {
      throw new Error('empty Event')
    }

    if (typeof this[payload.eventName] === 'function') {
      this[payload.eventName](payload.data)
    } else {
      Logger.debug('unknown event received in LocationStore', payload)
    }

    return true
  })
  return store
}.bind({})()

export default LocationStore
