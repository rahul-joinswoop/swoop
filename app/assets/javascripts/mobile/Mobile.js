import Dispatcher from 'lib/swoop/dispatcher'
import ReactDOMFactories from 'react-dom-factories'
import MobileEvents from 'mobile/MobileEvents'
import $ from 'jquery'
import './Mobile.scss'

// YUCK
window.$ = $
window.jQuery = $

const tags = [
  'del',
  'div',
  'table',
  'span',
  'strong',
  'textarea',
  'thead',
  'tbody',
  'tr',
  'th',
  'td',
  'input',
  'h1',
  'h2',
  'h3',
  'h4',
  'hr',
  'form',
  'label',
  'select',
  'option',
  'button',
  'aside',
  'ul',
  'li',
  'footer',
  'header',
  'img',
  'nav',
  'br',
]

tags.forEach(tag => window[tag] = ReactDOMFactories[tag])
window.Tracking = require('tracking').default

// pretty sure we don't need this, it's identical to what's in dispatcher already
Dispatcher.send = (eventName, data) =>
  Dispatcher.dispatch({
    eventName,
    data,
  })


MobileEvents.registerForServerUpdates()
// These prevent Iphone from scrolling / overscrolling or resizing
document.addEventListener('gesturestart', e => e.preventDefault())
document.addEventListener('touchmove', e => e.preventDefault())
