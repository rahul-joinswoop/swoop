import React from 'react'
import BaseConsts from 'base_consts'
import LocationStore from 'mobile/LocationStore'

class ThankYou extends React.Component {
  handleClick = () => {
    // TODO - we can actually do this?.props?.callback?.() but first we have
    // to get eslint happy about it
    if (this.props.callback != null) {
      this.props.callback()
    }
    window?.analytics?.track('Click Change Address Button')
  }

  render() {
    const address = LocationStore.getAddress()
    return (
      <div className="wrapper">
        <div className="thankyou">
          <h2>{BaseConsts.MOBILE.THANK_YOU}</h2>
          {address &&
            <>
              <p>
                {BaseConsts.MOBILE.ADDRESS_SHARED('')}<br />
                {address.replace(', USA', '')}
              </p>
            </>}
          <p className="change-address">
            <a onClick={this.handleClick}>
              {BaseConsts.MOBILE.CHANGE_ADDRESS}
            </a>
          </p>
          <p>
            {BaseConsts.MOBILE.EN_ROUTE_MESSAGE}
          </p>
        </div>
      </div>
    )
  }
}

export default ThankYou
