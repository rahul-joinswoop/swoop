import React from 'react'
import BaseConsts from 'base_consts'
import Api from 'api'
import moment from 'moment-timezone'
import Backbone from 'backbone'
import MobileUtils from 'mobile/MobileUtils'
import utils from 'utils'
import LocationStore from 'mobile/LocationStore'
import SwoopMap from 'components/maps/map'
import SwoopMapNew from 'components/maps/SwoopMap'
import { UserMarker } from 'components/maps/map_new'
import Logger from 'lib/swoop/logger'
import Loading from 'componentLibrary/Loading'
import { partial } from 'lodash'
import { SplitIOProvider, withSplitIO } from 'components/split.io'
import LocationSearch from './LocationSearch'
import './GetLocation.scss'

const NewMap = ({
  forceHeight, listeners, options, setRef, showMeAt,
}) => {
  const onLoad = (map) => {
    map.setOptions(options)

    // workaround getCenter and setCenter of old components/maps/map component
    setRef({
      getCenter: () => map.getCenter(),
      setCenter: (lat, lng) => {
        if (lat && lng) {
          map.panTo({ lat, lng })
        }
      },
    })
  }

  return (
    <SwoopMapNew
      onCenterChanged={listeners.center_changed}
      onIdle={listeners.idle}
      onLoad={onLoad}
      style={{ height: forceHeight }}
    >
      {showMeAt?.lat && showMeAt?.lng && <UserMarker lat={showMeAt.lat} lng={showMeAt.lng} />}
    </SwoopMapNew>
  )
}

const Map = withSplitIO(({
  forceHeight, listeners, mapOptions, setRef, showMeAt, treatments,
}) => {
  if (!treatments?.[BaseConsts.SPLITS.DISABLE_NEW_GET_LOCATION_MAP]) {
    return (
      <NewMap
        forceHeight={forceHeight}
        listeners={listeners}
        options={mapOptions}
        setRef={setRef}
        showMeAt={showMeAt}
      />
    )
  } else {
    return (
      <SwoopMap
        forceHeight={forceHeight}
        listeners={listeners}
        mapOptions={mapOptions}
        ref={setRef}
        showMeAt={showMeAt}
      />
    )
  }
})

class GetLocation extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      address: null,
      rev: 0,
    }
  }

  componentDidMount() {
    LocationStore.bind(LocationStore.CHANGE, this.update)
    return setTimeout(() => this.setState({
      showMap: true,
    }), 2000)
  }

  componentWillUnmount() {
    LocationStore.bind(LocationStore.CHANGE, this.update)
  }

  onAddressChange = (suggest) => {
    Logger.debug('here: ', suggest)
    this.setState({
      address: suggest.label,
      searching: false,
      forcedAddress: suggest.label,
    })
    this?.map?.setCenter(suggest.location.lat, suggest.location.lng)
  }

  setSearch = (searching, e) => {
    this.setState({
      searching,
    })

    window?.analytics?.track('Click Search Button')
    e.preventDefault()
    return e.stopPropagation()
  }

  centerChange = () => {
    Logger.debug('Map center')
    if (this?.map?.getCenter) {
      return this.updateAddress(this.map.getCenter())
    }
  }

  clearAddress = () => this.setState({
    address: false,
    forcedAddress: null,
    place_id: null,
  })

  handleCenterOnLocation = (e) => {
    const loc = LocationStore.getLocation()
    Logger.debug('in recenter on location', loc)
    this?.map?.setCenter(loc.lat, loc.lng)
    this.updateAddress(loc)
    window?.analytics?.track('Click Recenter Button')
    e.preventDefault()
    return e.stopPropagation()
  }

  sendAddress = (e) => {
    const { address } = this.state
    e.preventDefault()
    window?.analytics?.track('Click Send Address Button')

    if (address != null) {
      const loc = this?.map?.getCenter() // TODO: convert to api call

      LocationStore.setAddress(address)
      Backbone.history.navigate('thankyou', {
        trigger: true,
      })
      return Api.sendLocation(
        MobileUtils.getUUIDFromPath(),
        loc.lat(),
        loc.lng(),
        address,
        this.state.place_id,
        true,
        {
          success: () => Logger.debug('just sent up location'),
          error: () => Logger.debug('share submit error'),
        },
      )
    } else {
      return Logger.debug('share submitted, but no address')
    }
  }

  update = () =>
    this.setState(prevState => ({ rev: prevState.rev + 1 }))

  updateAddress = (loc) => {
    let preRequestTime

    if (this.timeout_id) {
      clearTimeout(this.timeout_id)
    }

    if (this.state.forcedAddress == null) {
      preRequestTime = moment.now()
      return MobileUtils.reverseGeocode(loc, (address) => {
        if (address != null) {
          this.setState({
            address: address.formatted_address,
            place_id: address.place_id,
          })
          window?.analytics?.track('Loaded address', {
            seconds: (moment.now() - preRequestTime) / 1000,
          })
        } else {
          // Try again in a second
          // TODO: implement backoff and cancelation
          window?.analytics?.track('Failed to load address')
          this.timeout_id = setTimeout(partial(this.updateAddress, loc), 1000)
        }
      })
    }
  }

  render() {
    let height
    let center = {
      lat: 39.7577,
      lng: -122.4376,
    } // Hack so we don't recenter every time render is called

    const loc = LocationStore.getLocation()
    const job = LocationStore.getJob()

    if (loc != null) {
      center = loc
    } else if (job != null) {
      if (job.service_location != null) {
        center = job.service_location
      } else if (job.site != null) {
        center = job.site
      }
    }

    height = null

    if (utils.isIOS) {
      height = window.innerHeight
    }

    Logger.debug('setting me location to ', LocationStore.getLocation())

    return (
      <div className="map-wrapper">
        <div className="row map-row">
          <div id="section-autoShareLocation">
            <form id="auto-form">
              <div className="share-location-map" id="map-canvas">
                {this.state.showMap &&
                <Map
                  forceHeight={height}
                  listeners={{
                    center_changed: this.clearAddress,
                    idle: this.centerChange,
                  }}
                  mapOptions={{
                    center,
                    mapTypeId: window?.google?.maps?.MapTypeId?.ROADMAP,
                    zoom: 15,
                    gestureHandling: 'greedy',
                    mapTypeControl: false,
                    disableDefaultUI: true,
                    forceCenter: true,
                  }}
                  setRef={map => this.map = map}
                  showMeAt={loc}
                />}
              </div>
              <div id="footer">
                <button
                  onClick={this.state.address ? this.sendAddress : null}
                  type="submit"
                >
                  {!this.state.address ?
                    <Loading padding="4px 0" /> :
                    BaseConsts.MOBILE.SEND_LOCATION}
                </button>
                <p className="send_information">
                  {BaseConsts.MOBILE.LOCATION_WILL_BE_SEND(job?.owner_company?.name || 'your provider')}
                </p>
              </div>
              <div className="Absolute-Center" />
              {loc &&
                <div>
                  <button
                    id="center"
                    onClick={this.handleCenterOnLocation}
                    type="button"
                  />
                </div>}
            </form>

            <div id="service-location-box" onClick={partial(this.setSearch, true)}>
              {this.state.searching ?
                <LocationSearch
                  center={this?.map?.getCenter()}
                  close={partial(this.setSearch, false)}
                  onAddressChange={this.onAddressChange}
                /> :
                <>
                  <p className="service-title">
                    {BaseConsts.MOBILE.SERVICE_LOCATION}
                  </p>
                  <p className="service-location" id="address">
                    {this?.state?.address ? this.state.address.replace(', USA', '') :
                      BaseConsts.MOBILE.FINDING_LOCATION}
                  </p>
                  <i className="fa fa-search" />
                </>}
            </div>

          </div>
        </div>
      </div>
    )
  }
}

// wrap on this level to avoid unnecessary loading of split.io data on mobile devices
// because internet speed is usually lower on mobile devices
const GetLocationWrapper = () => {
  let splitId
  try {
    splitId = localStorage.getItem('temporarySplitId')
    if (!splitId) {
      splitId = Date.now()
      localStorage.setItem('temporarySplitId', splitId)
    }
  } catch (e) {
    splitId = Date.now()
  }

  return (
    <SplitIOProvider id={splitId}>
      <GetLocation />
    </SplitIOProvider>
  )
}

export default GetLocationWrapper
