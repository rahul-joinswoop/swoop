import BaseConsts from 'base_consts'
import React from 'react'
import LocationStore from 'mobile/LocationStore'
import Logger from 'lib/swoop/logger'
import Loading from 'componentLibrary/Loading'

class LocationRequest extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      requesting: false,
    }
  }

  componentDidMount() {
    LocationStore.bind(LocationStore.CHANGE, this.locationReceived)
  }

  componentWillUnmount() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId)
    }
    return LocationStore.unbind(LocationStore.CHANGE, this.locationReceived)
  }

  locationDecline = () => {
    Logger.debug('Location Declined')
    window?.analytics?.track('Mobile Location Reject')

    if (this.props.callback != null) {
      return this.props.callback(null)
    }
  }

  locationReceived = () => {
    Logger.debug('Location Received')
    const loc = LocationStore.getLocation()
    if (loc != null) {
      const seconds = new Date().getTime() / 1000 - this.state.showTime
      window?.analytics?.track('Mobile Location Locate Time', {
        seconds,
      })
      Logger.debug('Calling callback')
      if (this.props.callback != null) {
        return this.props.callback(null)
      }
    }
  }

  handleClick = () => {
    Logger.debug('Clicking next')
    window?.analytics?.track('Click Allow Location Button')
    this.setState({
      requesting: true,
    })

    if (navigator.geolocation) {
      Logger.debug('Watching location')
      LocationStore.watchLocation(this.locationDecline)
      this.timeoutId = setTimeout(() => {
        window?.analytics?.track('Mobile Location Locate Timeout')
        if (this.props.callback != null) {
          return this.props.callback(null)
        }
      }, 10000)
    } else if (this.props.callback != null) {
      Logger.debug('Calling callback')
      return this.props.callback()
    }
  }

  render() {
    return (
      <div className="wrapper">
        <div className="request-location">
          <h2>{BaseConsts.MOBILE.LOCATION_NEEDED}</h2>
          <p>{this.props.message ? this.props.message : BaseConsts.MOBILE.LOCATION_REQUEST_MESSAGE}</p>
        </div>
        {this.state.requesting ?
          <Loading /> :
          <button
            onClick={this.handleClick}
            type="button"
          >
            {BaseConsts.MOBILE.CONFIRM_LOCATION}
          </button>}
      </div>
    )
  }
}
export default LocationRequest
