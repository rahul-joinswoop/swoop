import Consts from 'consts'
import React from 'react'
import LocationStore from 'mobile/LocationStore'
import Logger from 'lib/swoop/logger'
import { partial } from 'lodash'
import Backbone from 'backbone'
import MobileWrapper from 'mobile/MobileWrapper'
import Header from 'mobile/Header'
import Completed from 'mobile/Completed'
import Loading from 'componentLibrary/Loading'
import GetLocation from './GetLocation'
import LocationRequest from './LocationRequest'
import ThankYou from './ThankYou'

class GetLocationContainer extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      page: 'loading',
      rev: 0,
    }
  }

  componentDidMount() {
    const routes = ['loading', 'completed', 'loc_request', 'get_location', 'thankyou']
    const routeDict = routes.reduce((p, c) => {
      Object.assign(p, { [c]: partial(this.setPage, c) })
      return p
    }, {})

    routeDict['*path'] = 'notFound'
    const Router = Backbone.Router.extend({
      routes: routeDict,
    })
    window.Router = new Router()
    Backbone.history.start()
    Backbone.history.navigate('loading', {
      trigger: true,
    })
    LocationStore.load()
    LocationStore.bind(LocationStore.LOADED, this.jobLoaded)
  }

  componentWillUnmount() {
    LocationStore.unbind(LocationStore.LOADED, this.jobLoaded)
  }

  setPage = page => this.setState({ page })

  generalChanged = () => this.setState(prevState => ({ rev: prevState.rev + 1 }))

  jobChanged() {
    const job = LocationStore.getJob()
    if (Consts.COMPLETED_STATUSES.includes(job?.status)) {
      Backbone.history.navigate('completed', {
        trigger: true,
      })
    }
  }

  jobLoaded() {
    const job = LocationStore.getJob()
    if (Consts.COMPLETED_STATUSES.includes(job?.status)) {
      Backbone.history.navigate('completed', {
        trigger: true,
      })
    } else {
      Backbone.history.navigate('loc_request', {
        trigger: true,
      })
    }
  }

  locationReceived() {
    Backbone.history.navigate('get_location', {
      trigger: true,
    })
  }

  notFound(path) {
    Logger.debug(path)
  }

  refresh() {
    location.reload()
  }

  render() {
    window?.analytics?.page(this.state.page)

    let component
    switch (this.state.page) {
      case 'loading':
        component = <Loading />
        break

      case 'completed':
        component = <Completed />
        break

      case 'loc_request':
        component = <LocationRequest callback={this.locationReceived} />
        break

      case 'get_location':
        component = <GetLocation />
        break

      case 'thankyou':
        component = <ThankYou callback={this.locationReceived} />
        break

      default:
        component = <div>test</div>
    }
    return (
      <MobileWrapper>
        <Header />
        {component}
      </MobileWrapper>
    )
  }
}

export default GetLocationContainer
