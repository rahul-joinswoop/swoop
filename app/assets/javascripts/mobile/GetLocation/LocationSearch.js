import React from 'react'
import Geosuggest from 'components/geosuggest/Geosuggest'
import { isEmpty } from 'lodash'

const GeosuggestItem = (suggest = {}) => {
  let street = ''
  let locale = ''

  if (isEmpty(suggest)) {
    return null
  }

  for (let i = 0; i < suggest.suggest.terms.length; i++) {
    const obj = suggest.suggest.terms[i]

    if (i < suggest.suggest.terms.length - 3) {
      const sub = street.substring(2)

      if (!isNaN(parseInt(sub, 10)) && parseInt(sub, 10) > 0) {
        street += ` ${obj.value}`
      } else {
        street += `, ${obj.value}`
      }
    } else {
      locale += `, ${obj.value}`
    }
  }

  street = street.substring(2)
  locale = locale.substring(2)
  return (
    <div>
      <span className="street">
        {street}
      </span>
      <span className="locale">
        {locale}
      </span>
    </div>
  )
}
class LocationSearch extends React.Component {
  componentDidMount() {
    document.getElementsByClassName('geosuggest__input')[0]?.focus()
  }

  render() {
    return (
      <div className="search_box">
        <div className="transparent_box" onClick={this.props.close} />
        <Geosuggest
          always_show_map={false}
          className="InputVehicleAddress geosuggest-input address"
          hide_map
          hideIndicator
          location={this.props.center}
          onChange={this.props.props}
          onSuggestSelect={this.props.onAddressChange}
          placeholder="Type and select address"
          renderItem={GeosuggestItem}
        />
        <i className="fa fa-times" onClick={this.props.close} />
      </div>
    )
  }
}

export default LocationSearch
