import React from 'react'
import BaseConsts from 'base_consts'
import Utils from 'utils'
import LocationStore from 'mobile/LocationStore'
import SwoopLogo from 'images/Swoop_Logo-white-transparent.png'
import './Header.scss'

class MobileHeader extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rev: 0,
      showId: false,
    }
  }

  componentDidMount() {
    LocationStore.load()
    LocationStore.bind(LocationStore.CHANGE, this.update)
  }

  componentWillUnmount() {
    LocationStore.unbind(LocationStore.CHANGE, this.update)
  }

  handleClick = () => this.setState({ showId: true })

  update = () => this.setState(prevState => ({ rev: prevState.rev + 1 }))

  render() {
    const job = LocationStore.getJob()
    const number = job?.owner_company?.phone ?
      Utils.renderPhoneNumber(job.owner_company.phone) : BaseConsts.CONTACT_NUMBER
    const fallbackLogo = job?.type === 'FleetManagedJob' ? 'none' : SwoopLogo
    return (
      <header className="mobile-header">
        <button
          className="mobile-logo"
          onClick={this.handleClick}
          style={{
            backgroundImage: `url(${job?.owner_company?.logo_url || fallbackLogo})`,
          }}
          type="button"
        >
          {job?.owner_company?.name}
        </button>
        <span>
          {this.state.showId && <span className="job-id">{job?.id}</span>}
          {number &&
            <a href={`tel:${number}`}>Call Support</a>}
        </span>
      </header>
    )
  }
}
export default MobileHeader
