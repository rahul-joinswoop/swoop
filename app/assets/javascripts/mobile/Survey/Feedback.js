import React from 'react'
import BaseComponent from 'components/base_component'
import './Feedback.scss'

class Feedback extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      text: '',
    }
  }

  handleChange = e => this.setState({ text: e.target.value })

  handleSubmit = () => this.props.submit({ [this.props.question_key]: this.state.text })

  render() {
    return (
      <>
        <div className="question feedback">
          <p>{this.props.question}</p>
          <textarea
            onChange={this.handleChange}
            placeholder="Type your answer here..."
            value={this.state.text}
          />
        </div>
        <button
          className="submit"
          key="button"
          onClick={this.handleSubmit}
          type="submit"
        >
          Submit
        </button>
      </>
    )
  }
}

export default Feedback
