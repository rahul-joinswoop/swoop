import React from 'react'
import BaseComponent from 'components/base_component'
import Backbone from 'backbone'
import Logger from 'lib/swoop/logger'
import { partial } from 'lodash'
import Tracking from 'tracking'
import Api from './Api'
import SurveyPage from './SurveyPage'
import ThankYou from './ThankYou'

class Survey extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      page: 'survey',
      index: '0',
    }

    Tracking.track('NPS Survey Visit', {
      job_id: window?.gon?.job_id,
    })
  }

  componentDidMount() {
    Api.setBearerToken(window?.gon?.token)
    const routeDict = {}
    routeDict['survey/:index'] = partial(this.setPage, 'survey')
    routeDict.thankyou = partial(this.setPage, 'thankyou')
    routeDict['*path'] = 'notFound'
    const Router = Backbone.Router.extend({
      routes: routeDict,
    })
    window.Router = new Router()
    Backbone.history.start()
    Backbone.history.navigate('survey/0', {
      trigger: true,
    })
  }

  componentDidUpdate() {
    window?.analytics?.page(this.state.page)
  }

  notFound(path) {
    Logger.debug(path)
  }

  refresh() {
    location.reload()
  }

  setPage = (page, index) => this.setState({ page, index })

  render() {
    const { page } = this.state
    if (page === 'survey') {
      return (
        <SurveyPage
          index={parseInt(this.state.index, 10)}
          survey_questions={window?.gon?.survey_questions}
        />
      )
    } else
    if (page === 'thankyou') {
      return <ThankYou />
    }
    return null
  }
}

export default Survey
