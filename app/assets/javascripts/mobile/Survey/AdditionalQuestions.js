import React from 'react'
import BaseComponent from 'components/base_component'
import { extend, forEach, partial } from 'lodash'
import Question from './Question'

class AdditionalQuestions extends BaseComponent {
  setSelected = (key, i) => {
    const obj = {}
    obj[key] = i
    return this.setState(obj)
  }

  handleSubmit = () => {
    const obj = {}

    if (this.props.questions != null) {
      forEach(this.props.questions, (val) => {
        if (this.state[val.question_key] !== null) {
          obj[val.question_key] = this.state[val.question_key]
        }
      })
    }

    return this.props.submit(obj)
  }

  render() {
    return (
      <div className={`additional-questions ${this.props.className}`}>
        {this.props.questions && this.props.questions.map(q =>
          <Question
            key={q.question_key}
            {...extend({}, q, {
              onSelect: partial(this.setSelected, q.question_key),
              selected: this.state[q.question_key],
            })}
          />
        )}
        <button
          className="submit"
          onClick={this.handleSubmit}
          type="button"
        >
          Next
        </button>
      </div>
    )
  }
}

export default AdditionalQuestions
