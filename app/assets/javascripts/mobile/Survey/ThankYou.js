import React from 'react'
import './ThankYou.scss'

const ThankYou = () =>
  <div className="thankyou">
    <p>Thanks for your feedback!</p>
  </div>

export default ThankYou
