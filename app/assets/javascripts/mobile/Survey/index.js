import React from 'react'
import MobileWrapper from 'mobile/MobileWrapper'
import Header from 'mobile/Header'
import Survey from './Survey'

const SurveyContainer = () =>
  <MobileWrapper>
    <Header />
    <Survey />
  </MobileWrapper>

export default SurveyContainer
