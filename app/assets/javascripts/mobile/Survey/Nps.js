import React from 'react'
import BaseComponent from 'components/base_component'
import Question from './Question'

class NPS extends BaseComponent {
  constructor(props) {
    super(props)
    this.state = {
      selected: null,
    }
  }

  handleSelected = (i) => {
    this.setState({
      selected: i,
    })
  }

  handleSubmit = () => {
    const obj = {}

    if (this.state.selected != null) {
      obj[this.props.question_key] = this.state.selected
    }

    return this.props.submit(obj)
  }

  render() {
    return (
      <>
        <div className="question-wrapper">
          <Question
            key={this.props.question_key}
            leftKey="unlikely"
            onSelect={this.handleSelected}
            question={this.props.question}
            questionKey={this.props.question_key}
            rightKey="likely"
            selected={this.state.selected}
          />
        </div>
        <button
          className="submit"
          onClick={this.handleSubmit}
          type="button"
        >
          Next
        </button>
      </>
    )
  }
}

export default NPS
