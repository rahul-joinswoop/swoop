import React from 'react'
import BaseComponent from 'components/base_component'
import Backbone from 'backbone'
import { forEach, partial } from 'lodash'
import AdditionalQuestions from './AdditionalQuestions'
import Api from './Api'
import Feedback from './Feedback'
import NPS from './Nps'
import './SurveyPage.scss'

const KEY_PREFIX = 'question_'

class SurveyPage extends BaseComponent {
  submitCallback = (nextQuestion, questions) => {
    const results = []

    forEach(questions, (val, key) => {
      const index = parseInt(key.substring(KEY_PREFIX.length), 10)
      if (val != null) {
        const question = this.props.survey_questions[index] // make request here

        results.push({
          survey_question_id: question.id,
          value: val,
        })
      }
    })

    if (results.length > 0) {
      Api.sendSurveyResults(results)
    }

    if (nextQuestion < this.props.survey_questions.length) {
      return Backbone.history.navigate(`survey/${nextQuestion}`, {
        trigger: true,
      })
    } else {
      return Backbone.history.navigate('thankyou', {
        trigger: true,
      })
    }
  }

  mapQuestion = (index, leftKey, middleKey, rightKey) => {
    const q_obj = this.props.survey_questions[index]
    if (q_obj && q_obj.type === 'Survey::TenStarQuestion') {
      return {
        question: q_obj.question,
        leftKey,
        middleKey,
        rightKey,
        key: KEY_PREFIX + index,
        question_key: KEY_PREFIX + index,
      }
    }
    return {}
  }

  renderMultiPageQuestions(index) {
    let next = index + 1
    const questions = [this.mapQuestion(index, 'not at all', 'a little', 'well')]
    const question = this.mapQuestion(index + 1, 'very late', 'little late', 'on time')

    if (question) {
      questions.push(question)
      next += 1
    }

    return <AdditionalQuestions
      key={`questions_${index}`}
      questions={questions}
      submit={partial(this.submitCallback, next)}
    />
  }

  renderQuestion() {
    const { index, survey_questions } = this.props

    if (Array.isArray(survey_questions) && survey_questions.length > 0) {
      const q_obj = survey_questions[index]

      if (index === 0 && q_obj.type === 'Survey::TenStarQuestion') {
        return <NPS
          key={KEY_PREFIX + index}
          question={q_obj.question}
          question_key={KEY_PREFIX + index}
          submit={partial(this.submitCallback, 1)}
        />
      } else if (q_obj.type === 'Survey::TenStarQuestion') {
        return this.renderMultiPageQuestions(index)
      } else if (q_obj.type === 'Survey::LongTextQuestion') {
        return <Feedback
          key={KEY_PREFIX + index}
          question={q_obj.question}
          question_key={KEY_PREFIX + index}
          submit={partial(this.submitCallback, index + 1)}
        />
      }
    }
    return null
  }

  render() {
    const showDispatcher = window?.gon?.dispatcher_name && this.props.index === 0 ?
      <div className="dispatcher">
        <p>Your agent today was {window.gon.dispatcher_name.split(' ')[0]}.</p>
      </div> : null
    return (
      <div className="survey-page">
        {showDispatcher}
        {this.renderQuestion()}
      </div>
    )
  }
}

export default SurveyPage
