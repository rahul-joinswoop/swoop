import Consts from 'consts'
import Dispatcher from 'lib/swoop/dispatcher'
import $ from 'jquery'

$.ajaxSetup({
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json; charset=utf-8',
  },
})

// TODO: Have a base API that we extend
let _token = null

const Api = {
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
  UPDATE: 'PATCH',
  setup: false,
  setBearerToken(token) {
    this.setup = true
    _token = token
    return $.ajaxSetup({
      headers: {
        Authorization: `Bearer ${token}`,
      },
      dataType: 'json',
    })
  },

  _request401() {
    if (this.setup) {
      Dispatcher.send('new_notification', {
        type: Consts.ERROR,
        message: Consts.MESSAGE_AUTH_ERR,
        dismissable: true,
        timeout: Consts.N_TIME,
        id: 'auth_error',
      })
      return Dispatcher.send('refreshToken', null)
    }
  },

  _request403() {
    return Dispatcher.send(Consts.LOGOUT, null)
  },

  request(type, url, data, response, options) {
    let apiUrl = Consts.ROOT_PATH + url

    if (options?.overrideUrl) {
      apiUrl = url
    }

    return $.ajax({
      type,
      url: apiUrl,
      data: data ? JSON.stringify(data) : null,
      success: response?.success,
      error: response?.error,
      jsonp: false,
    }).fail((jqXHR) => {
      if (jqXHR.status === 401) {
        return Api._request401()
      } else if (jqXHR.status === 403) {
        return Api._request403()
      } else {
        return console.debug(jqXHR.status, 'received')
      }
    })
  },

  sendSurveyResults(responses) {
    // HACK: Fail safe in case the user is logged in
    Api.setBearerToken(_token)
    return Api.request(Api.POST, '/api/v1/fleet/jobs/survey_results', {
      survey: {
        responses,
      },
    })
  },
}
export default Api
