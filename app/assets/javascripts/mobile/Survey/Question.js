import React from 'react'
import BaseComponent from 'components/base_component'
import { partial, isNumber } from 'lodash'
import classNames from 'classnames'
import './Question.scss'

const QuestionKey = ({ text } = {}) =>
  (text ?
    <div className="nps_key">
      <span className="text">
        {text}
      </span>
    </div> :
    null
  )

const Option = ({ i, handleClick, selected }) =>
  <li className={classNames(`nps_option nps_option_${i}`, { selected })} onClick={handleClick}>
    <span>
      <span>{i}</span>
    </span>
  </li>

class Question extends BaseComponent {
  renderIndicators() {
    const { leftKey, rightKey } = this.props
    if (leftKey || rightKey) {
      return (
        <div className="options-indicators">
          {leftKey && <div className="left-indicator"><div /></div>}
          {rightKey && <div className="right-indicator"><div /></div>}
        </div>
      )
    }
    return null
  }

  verbalScale() {
    switch (this.props.selected) {
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
        return <>We're sorry to hear that. We hope to<br />provide better service next time.</>
      case 7:
      case 8:
        return <>Thank you. We will strive to<br />do even better next time!</>
      case 9:
      case 10:
        return <>Great! We love serving our customers.</>
      default:
        return null
    }
  }

  render() {
    const options = []
    for (let i = 0; i <= 10; i++) {
      const handleClick = partial(this.props.onSelect, i)
      const key = `nps_option_${i}`
      const selected = this.props.selected === i
      options.push(<Option {...{
        key, i, handleClick, selected,
      }}
      />)
    }

    return (
      <>
        <div className="question">
          <p>{this.props.question}</p>

          <ul className="options">
            {options}
          </ul>

          <div className="options-keys">
            <QuestionKey text={this.props.leftKey} />
            <QuestionKey text={this.props.middleKey} />
            <QuestionKey text={this.props.rightKey} />
          </div>

          {this.props.questionKey === 'question_0' && isNumber(this.props.selected) &&
            <div className="current-selection">
              <p>
                <span className={`selected-${this.props.selected}`}>{this.props.selected}</span>
                <br />
                {this.verbalScale()}
              </p>
            </div>}
        </div>
      </>
    )
  }
}

export default Question
