/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS206: Consider reworking classes to avoid initClass
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import { WEBSOCKET_URL } from 'consts'

import EventStore from 'stores/event_store'
import Mutils from 'mobile/MobileUtils'
import LocationStore from 'mobile/LocationStore'
import VehicleStore from 'stores/vehicle_store'
import Logger from 'lib/swoop/logger'

let UUID = null

class MobileEvents {
  static initClass() {
    this.onOpen = () => setTimeout(() => {
      if (UUID != null) {
        return EventStore.send({
          uuid: UUID,
        })
      }
    }, 100)

    this.registerForServerUpdates = () => {
      UUID = Mutils.getUUIDFromPath()

      if (UUID != null && UUID.length > 0) {
        const url = `${WEBSOCKET_URL}/track_technician/event_${UUID}`
        return EventStore.connect(
          url,
          this.onOpen,
        )
      }
    }

    this.onMessage = (msg) => {
      Logger.debug('switching on ', msg)

      switch (msg.class) {
        case 'RescueVehicle':
          // HACK: currently I only care about one truck so clear out the others
          VehicleStore.resetVehicles()
          return VehicleStore.addRemoteItem(msg)

        case 'TimeOfArrival':
          return LocationStore.updateTimeOfArrival(msg.target)

        case 'CustomerJob':
          return LocationStore.setJobStatus(msg.target)
      }
    }
  }
}

MobileEvents.initClass()
EventStore.bind(EventStore.CHANGE, MobileEvents.onMessage)
export default MobileEvents
