import BaseConsts from 'base_consts'
import React from 'react'
import Utils from 'utils'
import LocationStore from 'mobile/LocationStore'

class ThankYou extends React.Component {
  nextClick = () => {
    if (this.props.callback != null) {
      this.props.callback()
    }
    window?.analytics?.track('Click Change Address Button')
  }

  render() {
    const job = LocationStore.getJob()
    const number = job?.owner_company?.phone ?
      Utils.renderPhoneNumber(job.owner_company.phone) :
      BaseConsts.CONTACT_NUMBER

    return (
      <div className="wrapper">
        <div className="thankyou">
          <h2>{BaseConsts.MOBILE.SERVICE_COMPLETED}</h2>
          <p>
            <span>{BaseConsts.MOBILE.PLEASE_CONTACT}</span><br /><a href={`tel:${number}`}>{number}</a>
          </p>
        </div>
      </div>
    )
  }
}
export default ThankYou
