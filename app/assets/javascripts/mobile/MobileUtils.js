/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import Logger from 'lib/swoop/logger'

const MobileUtils = {
  getUUIDFromPath() {
    const url = location.pathname.replace(location.hash, '')
    return url.substr(url.lastIndexOf('/') + 1)
  },

  getGeocoder() {
    if (!this.geocoder) {
      this.geocoder = new window.google.maps.Geocoder()
    }

    return this.geocoder
  },

  reverseGeocode(loc, callback) {
    return this.getGeocoder().geocode(
      {
        location: loc,
      },
      (results, status) => {
        if (status === window.google.maps.GeocoderStatus.OK) {
          Logger.debug('results', results)

          if (results[0] != null) {
            Logger.debug('Found results', results[0])
            return callback(results[0])
          } else {
            Logger.debug('No results found')
            return callback(null)
          }
        } else {
          Logger.debug(`Geocoder failed due to: ${status}`)
          return callback(null)
        }
      },
    )
  },
}

export default MobileUtils
