import React from 'react'
import classNames from 'classnames'
import './MobileWrapper.scss'

const MobileWrapper = props => <div className={classNames('mobile-wrapper')}>{props.children}</div>

export default MobileWrapper
