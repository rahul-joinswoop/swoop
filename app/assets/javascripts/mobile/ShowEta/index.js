import React from 'react'
import Consts from 'consts'
import Backbone from 'backbone'
import LocationStore from 'mobile/LocationStore'
import Completed from 'mobile/Completed'
import MobileWrapper from 'mobile/MobileWrapper'
import Header from 'mobile/Header'
import Logger from 'lib/swoop/logger'
import { partial } from 'lodash'
import Loading from 'componentLibrary/Loading'
import ShowEta from './ShowEta'

class ShowEtaContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 'loading',
      rev: 0,
    }
  }

  componentDidMount() {
    let i, len, route
    const routes = ['loading', 'completed', 'show_eta']
    const routeDict = {}

    for (i = 0, len = routes.length; i < len; i++) {
      route = routes[i]
      routeDict[route] = partial(this.setPage, route)
    }

    routeDict['*path'] = 'notFound'
    Logger.debug('ROUTES: ', routeDict)
    const Router = Backbone.Router.extend({
      routes: routeDict,
    })
    window.Router = new Router()
    Backbone.history.start()
    Backbone.history.navigate('loading', {
      trigger: true,
    })
    LocationStore.load()
    LocationStore.watchLocation()

    LocationStore.bind(LocationStore.LOADED, this.jobLoaded)
    LocationStore.bind(LocationStore.JOB_CHANGE, this.jobChanged)
  }

  componentWillUnmount() {
    LocationStore.unbind(LocationStore.LOADED, this.jobLoaded)
    LocationStore.unbind(LocationStore.JOB_CHANGE, this.jobChanged)
  }

  setPage = (page) => {
    this.setState({
      page,
    })
  }

  generalChanged = () => {
    this.setState(prevState => ({ rev: prevState.rev + 1 }))
  }

  jobChanged = () => {
    const job = LocationStore.getJob()
    if (Consts.COMPLETED_STATUSES.includes(job?.status)) {
      Backbone.history.navigate('completed', {
        trigger: true,
      })
    }
  }

  jobLoaded = () => {
    const job = LocationStore.getJob()
    if (Consts.COMPLETED_STATUSES.includes(job?.status)) {
      Backbone.history.navigate('completed', {
        trigger: true,
      })
    } else {
      Backbone.history.navigate('show_eta', {
        trigger: true,
      })
    }
  }

  notFound = (path) => {
    Logger.debug(path)
  }

  refresh = () => {
    location.reload()
  }

  render() {
    window?.analytics?.page(this.state.page)

    let component
    switch (this.state.page) {
      case 'loading':
        component = <Loading />
        break

      case 'completed':
        component = <Completed />
        break

      case 'show_eta':
        component = <ShowEta />
        break

      default:
        component = <div>test</div>
    }

    return (
      <MobileWrapper>
        <Header />
        {component}
      </MobileWrapper>
    )
  }
}

export default ShowEtaContainer
