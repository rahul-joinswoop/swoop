import React from 'react'
import moment from 'moment-timezone'
import { pick } from 'lodash'
import Consts from 'consts'
import LocationStore from 'mobile/LocationStore'
import SwoopMap from 'components/maps/map'
import SwoopMapNew from 'components/maps/SwoopMap'
import { Route, TruckMarkers, UserMarker } from 'components/maps/map_new'
import { fitBounds } from 'components/maps/Utils'
import VehicleStore from 'stores/vehicle_store'
import Utils from 'utils'
import { SplitIOProvider, withSplitIO } from 'components/split.io'
import '../GetLocation/GetLocation.scss'

class ShowEta extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rev: 0,
    }
  }

  componentDidMount() {
    VehicleStore.bind(VehicleStore.CHANGE, this.update)
    LocationStore.bind(LocationStore.CHANGE, this.update)
    LocationStore.bind(LocationStore.JOB_CHANGE, this.update)
    return (this.interval = setInterval(() => this.update(), 60 * 1000))
  }

  componentWillUnmount() {
    VehicleStore.unbind(VehicleStore.CHANGE, this.generalChanged)
    LocationStore.unbind(LocationStore.CHANGE, this.update)
    LocationStore.unbind(LocationStore.JOB_CHANGE, this.update)
    return clearInterval(this.interval)
  }

  handleRecenterLocation = (e) => {
    const loc = LocationStore.getLocation()
    if (loc) {
      this?.map?.setCenter(loc.lat, loc.lng)
    }

    e.preventDefault()
    e.stopPropagation()
    window?.analytics?.track('Click Recenter Button')
  }

  update() {
    this.setState(prevState => ({ rev: prevState.rev + 1, showMap: true }))
  }

  renderMap(job) {
    // TODO migrate to load the collection dynamically (and maybe ES?) once we
    // stop loading vehicles on userChanged
    const vehicles = VehicleStore.getAll()

    let height
    if (Utils.isIOS) {
      height = window.innerHeight
    }

    const origin = pick(job?.service_location, 'lat', 'lng')
    origin.label = ' '
    const routes = [{ id: job?.id, origin }]

    const mapOptions = {
      mapTypeControl: false,
      disableDefaultUI: true,
      mapTypeId: window?.google?.maps?.MapTypeId?.ROADMAP,
      gestureHandling: 'greedy',
    }

    const markerImage = {
      url: '/assets/images/marker.png',
      scaledSize: new window.google.maps.Size(58, 79),
      origin: new window.google.maps.Point(0, 0),
      anchor: new window.google.maps.Point(30, 69),
    }

    if (!this.props.treatments?.[Consts.SPLITS.DISABLE_NEW_SHOW_ETA_MAP]) {
      const userLocation = LocationStore.getLocation()

      return (
        <SwoopMapNew
          onLoad={(map) => {
            map.setOptions(mapOptions)

            // workaround setCenter method of old map
            this.map = {
              setCenter(lat, lng) {
                if (lat && lng) {
                  map.panTo({ lat, lng })
                }
              },
            }

            fitBounds(map, [
              origin,
              userLocation,
              vehicles[0],
            ])
          }}
          style={{ height }}
        >
          <Route
            markerImage={markerImage}
            route={routes[0]}
          />
          <TruckMarkers
            showTruckNames={false}
            trucks={Object.keys(vehicles)}
          />
          {userLocation?.lat && userLocation?.lng && <UserMarker
            lat={userLocation.lat}
            lng={userLocation.lng}
          />}
        </SwoopMapNew>
      )
    } else {
      return (
        <SwoopMap
          forceHeight={height}
          mapOptions={mapOptions}
          markerImage={markerImage}
          ref={map => this.map = map}
          routes={routes}
          showMeAt={LocationStore.getLocation()}
          showTruckNames={false}
          trucks={Object.keys(vehicles)}
          zoom={30}
        />
      )
    }
  }

  render() {
    const job = LocationStore.getJob()
    let eta

    if (job?.status && job?.status !== Consts.ENROUTE) {
      eta = job.status
    } else if (job?.toa?.live) {
      eta = `${moment(job.toa.live).diff(moment(), 'minutes')} minutes`
    } else if (job?.toa?.latest) {
      eta = `${moment(job.toa.latest).diff(moment(), 'minutes')} minutes`
    } else {
      eta = 'Calculating ETA...'
    }

    return (
      <div className="content-wrapper map-wrapper eta_wrapper">
        <div className="row map-row">
          <div id="section-autoShareLocation">
            <form id="auto-form">
              <div
                className="share-location-map"
                id="map-canvas"
                style={{
                  width: '100%',
                  height: '100%',
                }}
              >
                {this.state.showMap && this.renderMap(job)}
              </div>
              <div id="service-location-box">
                <p className="service-title">
                  {[Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION].includes(job?.status) ?
                    'SERVICE IN PROGRESS' :
                    'ESTIMATED ARRIVAL TIME'}
                </p>
                <p className="service-location" id="address">{eta}</p>
              </div>
              <div>
                {LocationStore.getLocation() &&
                <div>
                  <button
                    className="lower"
                    id="center"
                    onClick={this.handleRecenterLocation}
                    type="button"
                  />
                </div>}
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

// wrap on this level to avoid unnecessary loading of split.io data on mobile devices
// because internet speed is usually lower on mobile devices
const ShowEtaWrapper = () => {
  let splitId
  try {
    splitId = localStorage.getItem('temporarySplitId')
    if (!splitId) {
      splitId = Date.now()
      localStorage.setItem('temporarySplitId', splitId)
    }
  } catch (e) {
    splitId = Date.now()
  }

  const ShowEtaWithSplitIO = withSplitIO(ShowEta)

  return (
    <SplitIOProvider id={splitId}>
      <ShowEtaWithSplitIO />
    </SplitIOProvider>
  )
}

export default ShowEtaWrapper
