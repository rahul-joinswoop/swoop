import React from 'react'
require('stylesheets/dashboard_cols.scss')
import moment from 'moment-timezone'
import ReactDOMFactories from 'react-dom-factories'

JobStore = require('stores/job_store')
UserStore = require('stores/user_store')
FeatureStore = require 'stores/feature_store'
AccountStore = require('stores/account_store')
UsersStore = require('stores/users_store').default
SiteStore = require('stores/site_store').default
CustomerTypeStore = require('stores/customer_type_store')
ServiceAliasStore = require('stores/service_alias_store')
DepartmentStore = require('stores/department_store')
VehicleStore = require('stores/vehicle_store')
VehicleCategoryStore = require('stores/vehicle_category_store')
{Column, Row} = (require 'components/tables/SortTable')
import JobStatusImport from 'components/job/status'
JobStatus = React.createFactory(JobStatusImport)
import JobAction from 'components/job/action'
JobActions = React.createFactory(require('components/job/actions'))
DispatcherInput = React.createFactory(require('components/input_dispatcher'))
import Utils from 'utils'
import Consts from 'consts'
DashboardETATime = React.createFactory(require('components/dashboard_eta_time'))
Countdown = React.createFactory(require 'components/countdown')
import CountFrom from 'components/CountFrom'
FleetEta = React.createFactory(require 'components/fleet_eta')
import AssignDriverButtonImport from 'components/simple/assign_driver_button.js'
AssignDriverButton = React.createFactory(AssignDriverButtonImport)
Timer = React.createFactory(require('components/timer').default)
AuctionStore = require('stores/auction_store').default
ChoosePartnerButton = React.createFactory(
  require('components/simple/choose_partner_button').default
)
InfoBubble = React.createFactory(
  require('components/info_bubble/info_bubble').default
)

class DashboardColumn extends Column
  getUsedProperties: -> [@getKey()]

module.exports = getCols: (props) ->
  "alerts" : class ID extends DashboardColumn
    _header: "Alerts", _key: "alerts", _originalWidth: 70,
    isSortable: props?.isSortable
    customerSaysNotComplete: (job=@record) -> JobStore.get(job?.id, "customer_says_not_complete", props.componentId)
    customerSaysNotOnSite: (job=@record) -> JobStore.get(job?.id, "customer_says_not_on_site", props.componentId)
    shouldShowAlert: (job) -> @customerSaysNotOnSite(job) || @customerSaysNotComplete(job)
    isShowing: -> props?.isFrontendSort?()
    sortFunc: (job, props) ->
      time = null
      if job?.scheduled_for?
        time = moment(job.scheduled_for)
      else
        time = moment(Utils.getJobCreatedAt(job))
      time = time.unix()


      if @shouldShowAlert(job)
        return time-10000000000
        #dispatched timestamp as secondary sort
      return time
    getValue: ->
      return div
        className: "text-center"
        if @shouldShowAlert(@record)
          InfoBubble
            style:
              color: "#FFD426"
            icon: "fa-exclamation-triangle"
            content: if @customerSaysNotOnSite(@record) then "Customer Replied Not On Site" else "Customer Replied Not Complete"

  "id" : class ID extends DashboardColumn
    _header: "ID", _key: "id", _originalWidth: 70,
    isSortable: props?.isSortable
    getValue: -> Utils.getJobId(@record)

  "l6vin" : class L6Vin extends DashboardColumn
    _header: "L6 VIN", _key: "vin", _className: "uppercase", _originalWidth: 79,
    isSortable: props?.isFrontendSort
    getValue: ->
      if @record?.vin? then @record?.vin.slice(-6) else ""

  "partner" : class Partner extends DashboardColumn
    _header: "Partner", _key: "partner",
    isSortable: props?.isSortable
    getUsedProperties: -> ["status", "rescue_company", "customized_rescue_company_name"]
    isShowing: ->
      (UserStore.isFleet() and UserStore.isFleetInHouse()) ||
      UserStore.isSwoop() ||
      (UserStore.isFleetManaged() and FeatureStore.isFeatureEnabled(Consts.FEATURES_ADD_PARTNER_COLUMN_TO_FLEET_MANAGED))
    getValue: ->
      if UserStore.isSwoop() && AuctionStore.jobHasAuction(@record) && @record.status == Consts.AUTO_ASSIGNING
        ChoosePartnerButton
          record: @record
      else if @record?.status == Consts.CANCELED
        if @record?.customized_rescue_company_name?
          @record.customized_rescue_company_name
        else if @record?.rescue_company?.name?
          JobStore.partnerNameFor(@record)
        else
          null
      else if @record?.status == Consts.AUTO_ASSIGNING
        null
      else if @record?.rescue_company?.name?
        JobStore.partnerNameFor(@record)
      else if @record?.customized_rescue_company_name?
        @record.customized_rescue_company_name
      else if (UserStore.isSwoop() and @record?.status != Consts.DRAFT) || (((UserStore.isFleet() and UserStore.isFleetInHouse())) and not @record?.rescue_company? and @record?.status in [Consts.PENDING, Consts.UNASSIGNED, Consts.REASSIGN])
        ChoosePartnerButton
          record: @record

  "customer_type" : class CustomerType extends DashboardColumn
    _key: "customer_type"
    getUsedProperties: -> ["customer_type_id"]
    getHeader: ->
      if UserStore.isEnterprise()
        return "Claim Type"
      else
        return "Payment Type"
    isSortable: props?.isFrontendSort
    isShowing: -> UserStore.isFleet() and FeatureStore.isFeatureEnabled(Consts.FEATURES_PAYMENT_TYPE)
    getValue: -> CustomerTypeStore.get(@record?.customer_type_id, "name", props.componentId)

  "department" : class Department extends DashboardColumn
    _header: "Department", _key: "department"
    getUsedProperties: -> ["department_id"]
    isSortable: props?.isFrontendSort
    isShowing: ->
      FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS) && UserStore.isFleet()
    getValue: ->
      DepartmentStore.get(@record?.department_id, "name", props.componentId)

  "partner_department" : class Department extends DashboardColumn
    _header: "Department", _key: "partner_department",
    getUsedProperties: -> ["partner_department_id"]
    isSortable: props?.isFrontendSort
    isShowing: ->
      FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS) && UserStore.isPartner()
    getValue: ->
      DepartmentStore.get(@record?.partner_department_id, 'name', props.componentId)

  "invoice_vehicle_category_id" : class ClassType extends DashboardColumn
    _header: "Class Type", _key: "invoice_vehicle_category_id"
    isSortable: props?.isFrontendSort
    getValue: ->
      if @record?.invoice_vehicle_category_id
         return VehicleCategoryStore.get(@record?.invoice_vehicle_category_id, "name", props.componentId)
      return div
        style: textAlign: 'center'
        "--"

  "company" : class CompanyName extends DashboardColumn
    _header: "Company", _key: "company"
    isSortable: props?.isFrontendSort
    getUsedProperties: -> ["owner_company"]
    isShowing: -> UserStore.isSwoop()
    getValue: -> @record?.owner_company?.name

  "customer_name" : class CustomerName extends DashboardColumn
    _header: "Customer", _key: "customer_name"
    getUsedProperties: -> ["customer"]
    isSortable: props?.isFrontendSort
    getValue: -> @record?.customer?.full_name

  "customer_phone" : class CustomerName extends DashboardColumn
    _header: "Customer Phone", _key: "customer_phone"
    getUsedProperties: -> ["customer"]
    isSortable: props?.isFrontendSort
    getValue: -> @record?.customer?.phone
    isShowing: -> props?.parentType == "review"

  "created_time" : class CreatedTime extends DashboardColumn
    _key: "time", _originalWidth: 120,
    isSortable: props?.isFrontendSort
    getHeader: -> (if UserStore.isFinishLine() then "Time" else "Created") + (if !UserStore.isPartner() then " (" + Utils.getTimezoneStr()+")" else "")
    isShowing: -> not Utils.isMobile() and props?.isFrontendSort?()
    sortFunc: (job, props) ->
      if job?.scheduled_for? and UserStore.isFinishLine()
        return moment(job.scheduled_for).unix()
      datetime = moment(Utils.getJobCreatedAt(job))
      datetime.unix()
    getValue: ->
      if @record?.scheduled_for? and UserStore.isFinishLine()
        return Utils.formatDateTime(@record.scheduled_for)

      datetime = Utils.formatDateTime(Utils.getJobCreatedAt(@record))
      datetime

  "wait_time" : class CreatedTime extends DashboardColumn
    _key: "wait_time", _header: "Wait Time", _originalWidth: 120,
    isSortable: props?.isFrontendSort
    DASH_VALUE: -2499900704
    isShowing: -> props?.isFrontendSort?()
    getTime: (job = @record) ->
      if @record?.status == Consts.DRAFT
        return ""

      if job?.status in [Consts.ONSITE,Consts.TOWING,Consts.TOWDESTINATION]
        return @DASH_VALUE #Large number to ensure these are at the bottom

      if job?.scheduled_for?
        return job.scheduled_for
      return Utils.getJobCreatedAt(job)

    sortFunc: (job, props) ->
      time = @getTime(job)
      if time == @DASH_VALUE
        return time
      return -moment(@getTime(job)).unix()

    getValue: ->
      time = @getTime()
      if time == @DASH_VALUE
        return "--"
      <CountFrom time={time}/>

  "total_time" : class CreatedTime extends DashboardColumn
    _key: "total_time", _header: "Total Time", _originalWidth: 120,
    isSortable: props?.isFrontendSort
    isShowing: -> props?.isFrontendSort?()
    getTime: (job=@record) ->
      if @record?.status == Consts.DRAFT
        return ""

      if job?.scheduled_for?
        return job.scheduled_for
      return Utils.getJobCreatedAt(job)

    sortFunc: (job, props) ->
      return -moment(@getTime(job)).unix()

    getValue: ->
      <CountFrom time={@getTime()}/>

  "created_time" : class CreatedTime extends DashboardColumn
    _key: "time", _originalWidth: 120,
    isSortable: props?.isFrontendSort
    getHeader: -> (if UserStore.isFinishLine() then "Time" else "Created") + (if !UserStore.isPartner() then " (" + Utils.getTimezoneStr()+")" else "")
    isShowing: -> not Utils.isMobile() and props?.isFrontendSort?()
    sortFunc: (job, props) ->
      if job?.scheduled_for? and UserStore.isFinishLine()
        return moment(job.scheduled_for).unix()
      datetime = moment(Utils.getJobCreatedAt(job))
      datetime.unix()
    getValue: ->
      if @record?.status == Consts.DRAFT
        return ""

      if @record?.scheduled_for? and UserStore.isFinishLine()
        return Utils.formatDateTime(@record.scheduled_for)

      datetime = Utils.formatDateTime(Utils.getJobCreatedAt(@record))
      datetime

  "completed_date" : class CompletedDate extends DashboardColumn
    _key: "last_status_changed_at", _originalWidth: 120,
    isSortable: props?.isSortable
    getHeader: -> "Completed Date" + (if !UserStore.isPartner() then " (" + Utils.getTimezoneStr()+")" else "")
    isShowing: ->
      !props?.isFrontendSort?() and not Utils.isMobile()
    getValue: ->
      return Utils.formatDateTime(@record?.last_status_changed_at)

  "fleetSite" : class FleetSite extends DashboardColumn
    _header: "Site"
    _key: "fleet_site_id"
    isSortable: props?.isFrontendSort
    isShowing: -> UserStore.isTravelCar()
    getValue: =>
      if @record?.fleet_site_id
        SiteStore.get(@record.fleet_site_id, 'name', props.componentId) || Consts.LOADING
      else
        ''

  "driver" : class Driver extends DashboardColumn
    _header: "Driver", _key: "driver"
    getUsedProperties: -> ["rescue_driver_id"]
    isSortable: props?.isFrontendSort
    getCellClassName: -> super(arguments...) + " bold"
    isShowing: -> UserStore.isPartner()
    getValue: ->
      #TODO: add loader here if rescue_driver_id isn't loaded yet
      driver = UsersStore.get(@record?.rescue_driver_id, "full_name", props.componentId)
      if driver?
        driver
      else if @record?.status in [Consts.PENDING, Consts.ACCEPTED, Consts.DISPATCHED, Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION]
        AssignDriverButton
          record: @record

  "truck" : class Truck extends DashboardColumn
    _header: "Truck", _key: "truck"
    getUsedProperties: -> ["rescue_vehicle_id"]
    isSortable: props?.isFrontendSort
    isShowing: -> UserStore.isPartner() and not Utils.isMobile()
    getValue: ->
      if @record?.rescue_vehicle_id?
        vehicleName = VehicleStore.get(@record.rescue_vehicle_id, 'name', props.componentId)

        if vehicleName then vehicleName else ""

  "account" : class Account extends DashboardColumn
    _header: "Account", _key: "Account", _originalWidth: 130,
    getUsedProperties: -> ["account_id"]
    isSortable: props?.isFrontendSort
    isShowing: -> UserStore.isPartner()
    sortFunc: (job) ->
      if job?.type == "FleetManagedJob"
        ret = "Swoop"
      else
        if job?.account_id
          ret = AccountStore.get(job.account_id, 'name', props.componentId)
      return ret

    getValue: ->
      if @record?.type == "FleetManagedJob"
        return "Swoop"
      else if @record?.account_id
        accountColor = AccountStore.get(@record.account_id, 'color', props.componentId)
        accountName  = AccountStore.get(@record.account_id, 'name', props.componentId)
        if accountColor
          span
            className: "account_wrapper"
            style:
              backgroundColor: accountColor
            accountName
        else
          accountName

  "po" : class PO extends DashboardColumn
    _key: "po_number", _originalWidth: 90,
    isSortable: props?.isFrontendSort
    getHeader: -> Utils.getPOLabel(UserStore)
    isShowing: ->
      FeatureStore.isFeatureEnabled("Job PO Number") and UserStore.isPartner()

  "service" : class Service extends DashboardColumn
    _header: "Service", _key: "service", _originalWidth: 90,
    getUsedProperties: -> ["service_code_id", "service_alias_id", "will_store"]
    isSortable: props?.isFrontendSort
    sortFunc: (job) -> ServiceAliasStore.getServiceNameOrAlias(job?.service_code_id, job?.service_alias_id)
    getValue: ->
        span null,
          if @record?.priority_response && !UserStore.isPartner()
            span
              className: "fa fa-flag"
              style:
                color: "red"
                marginRight: 5
          span null,
            ServiceAliasStore.getServiceNameOrAlias(@record?.service_code_id, @record?.service_alias_id, props.componentId) + (if @record?.will_store then ", Storage" else "")

  "location" : class Location extends DashboardColumn
    _header: "Location", _key: "location", _originalWidth: 190,
    getUsedProperties: -> ["service_location"]
    isSortable: props?.isFrontendSort
    getValue: ->
      if @record?.service_location?.address
        if UserStore.isPartner() && @record.status == Consts.AUTO_ASSIGNING
          { street_name, city, state, zip } = @record.service_location
          ([street_name, city, state, zip].filter (el) -> !!el).join(', ')
        else
          SiteStore.getAddressWithSiteName(@record.service_location, props.componentId)

  "drop" : class DropLocation extends DashboardColumn
    _header: "Drop Off", _key: "drop", _originalWidth: 190,
    getUsedProperties: -> ["drop_location"]
    isSortable: props?.isFrontendSort
    getValue: ->
      if @record?.drop_location?.address?
        if UserStore.isPartner() && @record.status == Consts.AUTO_ASSIGNING
          { street_name, city, state, zip } = @record.drop_location
          ([street_name, city, state, zip].filter (el) -> !!el).join(', ')
        else
          SiteStore.getAddressWithSiteName(@record.drop_location, props.componentId)

  "status_age": class StatusAge extends DashboardColumn
    _header: "Status Age", _key: "status_age",
    isShowing: -> props?.isFrontendSort?()
    isSortable: props?.isFrontendSort
    getTime: (job=@record) ->
      if job?.status == "Pending" ||  job?.status == "Unassigned"
        Utils.getJobCreatedAt(job)
      else if job?.history?[job.status]?
        job.history?[job.status]?.adjusted_dttm || job.history?[job.status]?.last_set_dttm

    sortFunc: (job, props) ->
      return -moment(@getTime(job)).unix()

    getValue: ->
      <CountFrom time={@getTime()}/>

  "eta" : class Eta extends DashboardColumn
    getUsedProperties: -> ["toa"]
    _header: "ETA", _key: "eta", _originalWidth: 120,
    isShowing: -> props?.isFrontendSort?() and (UserStore.isFleet() or UserStore.isSwoop())
    isSortable: props?.isFrontendSort
    getValue: ->
      FleetEta
        job_id: @record?.id
        enableDashboardFormatting: true

    sortFunc: (job, props) ->
      ret = Utils.getLastEtaObj(job)
      if job?.status in [Consts.CREATED, Consts.PENDING, Consts.REJECTED, Consts.UNASSIGNED, Consts.REASSIGN, Consts.ASSIGNED, Consts.ACCEPTED, Consts.DISPATCHED, Consts.ENROUTE]
        if ret?
          ret.unix()
        else
          0
      else
        -1

  "eta2" : class Eta2 extends DashboardColumn
    getUsedProperties: -> ["toa"]
    _header: "ETA", _key: "eta2", _originalWidth: 120,
    isSortable: props?.isFrontendSort
    isShowing: ->
      props?.isFrontendSort?() and UserStore.isPartner()
    getValue: ->
      DashboardETATime
        jobId: @record?.id
    sortFunc: (job, props) ->
      ret = Utils.getLastEtaObj(job)
      if job?.status in [Consts.CREATED, Consts.PENDING, Consts.REJECTED, Consts.UNASSIGNED, Consts.REASSIGN, Consts.ASSIGNED, Consts.ACCEPTED, Consts.SUBMITTED, Consts.DISPATCHED, Consts.ENROUTE]
        if ret?
          ret.unix()
        else
          0
      else
        -1

  "status" : class Status extends DashboardColumn
    _header: "Status", _key: "status", _cellClassName: "status", _originalWidth: 145,
    getUsedProperties: -> ["status", "fleet_manual", "rescue_company", "scheduled_for"]
    isSortable: props?.isFrontendSort
    isShowing: -> not UserStore.isOnlyDriver()
    sortFunc: props?.sortStatus
    getValue: ->
      ReactDOMFactories.div
        className: ''
        JobStatus
          job: @record
          getStatusStyle: props.getStatusStyle

  "dispatcher" : class Dispatcher extends DashboardColumn
    _header: "Dispatcher", _key: "dispatcher"
    isSortable: props?.isFrontendSort
    getCellClassName: -> super(arguments...) + " dispatcher"
    getUsedProperties: ->
      if UserStore.isSwoop()
        return ["root_dispatcher_id"]
      else if UserStore.isFleet()
        return ["fleet_dispatcher_id"]
      else if UserStore.isPartner()
        return ["partner_dispatcher_id"]

    sortFunc: (job, colProps) ->
      if UserStore.isSwoop()
        return UsersStore.get(job.root_dispatcher_id, "full_name", props.componentId)
      else if UserStore.isFleet()
        return UsersStore.get(job.fleet_dispatcher_id, "full_name", props.componentId)
      else if UserStore.isPartner()
        return UsersStore.get(job.partner_dispatcher_id, "full_name", props.componentId)
    getValue: ->
      DispatcherInput
        job_id: @record?.id

  "year": class Year extends DashboardColumn
    _header: "Year", _key: "year"
    isSortable: props?.isFrontendSort
  "make": class Make extends DashboardColumn
    _header: "Make", _key: "make"
    isSortable: props?.isFrontendSort
  "model": class Model extends DashboardColumn
    _header: "Model", _key: "model"
    isSortable: props?.isFrontendSort
  "style": class Style extends DashboardColumn
    _header: "Style", _key: "vehicle_type"
    isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_HEAVY)
    isSortable: props?.isFrontendSort
  "serial": class Serial extends DashboardColumn
    _header: "Serial", _key: "serial_number"
    isShowing: -> FeatureStore.isFeatureEnabled("Serial Number")
    isSortable: props?.isFrontendSort
  "color": class Color extends DashboardColumn
    _header: "Color", _key: "color"
    isSortable: props?.isFrontendSort
  "license": class License extends DashboardColumn
    _header: "License", _key: "license"
    isSortable: props?.isFrontendSort
  "phone": class Phone extends DashboardColumn
    getUsedProperties: -> ["phone"]
    _header: "Phone", _key: "phone"
    isSortable: props?.isFrontendSort
  "odometer": class Odometer extends DashboardColumn
    _header: "Odometer", _key: "odometer"
    isSortable: props?.isFrontendSort
  "unit_number": class Unit extends DashboardColumn
    _header: "Unit Number", _key: "unit_number"
    isSortable: props?.isFrontendSort
    isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_UNIT_NUMBER)
  "policy_number": class Policy extends DashboardColumn
    _key: "policy_number"
    _header: "Policy Number"
    isSortable: props?.isFrontendSort
    isShowing: -> UserStore.isMetromile()
  "ref_number": class Ref extends DashboardColumn
    _key: "ref_number"
    getHeader: -> Utils.getRefLabel(UserStore)
    isSortable: props?.isFrontendSort
    isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_REF_NUMBER)

  "q1": class Q1 extends DashboardColumn
    getUsedProperties: -> ["review"]
    _key: "review.nps_question1"
    _header: "Customer Score"
    isShowing: -> props?.parentType == "review"
  "q2": class Q2 extends DashboardColumn
    getUsedProperties: -> ["review"]
    _key: "review.nps_question2"
    _header: "Q2"
    isShowing: -> props?.parentType == "review" && UserStore.isSwoop()
  "q3": class Q3 extends DashboardColumn
    getUsedProperties: -> ["review"]
    _key: "review.nps_question3"
    _header: "Q3"
    isShowing: -> props?.parentType == "review" && UserStore.isSwoop()
  "feedback": class Feedback extends DashboardColumn
    getUsedProperties: -> ["review"]
    _key: "review.feedback"
    _header: "Customer Comments"
    isShowing: -> props?.parentType == "review"
  "estimated_toa": class EstimatedToa extends DashboardColumn
    getUsedProperties: -> ["review"]
    _key: "review.estimated_toa"
    _header: "ETA"
    isShowing: -> props?.parentType == "review"
  "actual_toa": class ActualToa extends DashboardColumn
    getUsedProperties: -> ["review"]
    _key: "review.actual_toa"
    _header: "ATA"
    isShowing: -> props?.parentType == "review"

  "action" : class Action extends DashboardColumn
    _header: "Action", _key: "action", _originalWidth: 170,
    isShowing: -> UserStore.isDriver() and Utils.isMobile()
    getUsedProperties: -> ['rescue_driver_id', 'status']
    getValue: ->  JobAction job: @record
  "actions" : class Action extends DashboardColumn
    _header: "",  sortable: false, _key: "expand", className: "right expand_action", _originalWidth: 95,
    getUsedProperties: -> ["owner_company"]
    getValue: ->
      JobActions
        job: @record
        update: @generalChanged
        hideEdit: props?.parentType == "review"
        expanded: props.isExpanded(@record?.id)
        parentType: props?.parentType
