import BaseConsts from 'base_consts'

/* eslint-disable-next-line import/no-mutable-exports */
let SITE_URL, WEBSOCKET_URL
const EXT = '.mobile'
const DEVICE = 'web'
const { STRIPE_PUBLIC_KEY } = process.env
// TODO - this won't be necessary if we can setup these variables in our
// postDeploy for review apps
if (process.env.HEROKU_PARENT_APP_NAME) {
  SITE_URL = `https://${process.env.HEROKU_APP_NAME}.herokuapp.com`
  WEBSOCKET_URL = `wss://${process.env.HEROKU_APP_NAME}.herokuapp.com`
} else {
  SITE_URL = process.env.SITE_URL
  WEBSOCKET_URL = process.env.WEBSOCKET_URL

  if (!WEBSOCKET_URL) {
    const wsUrl = new URL(SITE_URL)

    if (wsUrl.origin === 'https:') {
      wsUrl.origin = 'wss:'
    } else {
      wsUrl.origin = 'ws:'
    }
    WEBSOCKET_URL = wsUrl.toString()
  }
}

// TODO - when / where do we use this? if we can get it setup right in heroku
// this won't be necessary
const JS_ENV = SITE_URL?.match(/demo/) ? 'demo' : process.env.JS_ENV

const Consts = {
  ...BaseConsts,
  EXT,
  DEVICE,
  STRIPE_PUBLIC_KEY,
  SITE_URL,
  WEBSOCKET_URL,
  JS_ENV,
}

export {
  EXT,
  DEVICE,
  STRIPE_PUBLIC_KEY,
  SITE_URL,
  WEBSOCKET_URL,
  JS_ENV,
}

export default Consts
