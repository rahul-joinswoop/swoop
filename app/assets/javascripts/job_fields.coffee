import Api from 'api'
import moment from 'moment'
import React from 'react'
import Consts from 'consts'
import Utils from 'utils'
import ReactDOMFactories from 'react-dom-factories'
import { clone, extend, filter, find, forEach, get, includes, isEmpty, map, partial, sortBy, union, unionBy, uniq, values } from 'lodash'
UserStore = OrigUserStore = require('stores/user_store')
UsersStore = OrigUsersStore = require('stores/users_store').default
CompanyStore = require('stores/company_store').default
FeatureStore = require('stores/feature_store')
EdmundsStore = require('stores/edmunds_store')
InvoiceStore = require ('stores/invoice_store')
ServiceStore = require ('stores/service_store')
SymptomStore = require ('stores/symptom_store')
QuestionStore = require ('stores/question_store')
TrailerStore = require('stores/trailer_store')
CustomerTypeStore = require('stores/customer_type_store')
AccountStore = require('stores/account_store')
DepartmentStore = require('stores/department_store')
JobStore = require('stores/job_store')
LocationTypeStore = require('stores/location_type_store')
PoiStore = require('stores/poi_store').default
SiteStore = require('stores/site_store').default
StorageTypeStore = require('stores/storage_type_store')
ServiceAliasStore = require('stores/service_alias_store')
SettingStore = require('stores/setting_store')
VehicleStore = require('stores/vehicle_store')
VehicleCategoryStore = require('stores/vehicle_category_store')
TireTypeStore = require('stores/tire_type_store')
UserSettingStore = require('stores/user_setting_store')
AsyncRequestStore = require('stores/async_request_store').default
import LoadingImport from 'componentLibrary/Loading'
Loading = React.createFactory(LoadingImport)
Fields = require('fields/fields')
import { mixin } from 'fields/common/field'

NotesMixin =
  _tabGroup: "notes"
  renderLabel: -> null

MaxLengthMixin=(length) ->
  getInputProps: ->
    if typeof(length) == "function"
      length = length()
    #Hack so I can call super(arguments...)
    props = Object.getPrototypeOf(@constructor).prototype.getInputProps.apply(this, arguments)
    if length?
      props.maxLength = length
    props

InheritMixin=(prop) ->
  beforeSend: () ->
    if get(@record, @getName())? and get(@original, prop)?
      @record[prop].id = get(@original, prop).id

CallerMixin =
  isShowing: -> not UserStore.isFleet() && !Utils.isPartnerOnSwoopJob(UserStore, @record)
  isEnabled: -> !isRootDispatcherShown(@original) && !isFleetDispatcherShown(@original)
  getToggleName: -> if !FeatureStore.isFeatureEnabled("Always Show Caller") then "callerToggle"

isLincolnForm = (record) =>
  UserStore.isLincoln() || (record?.acts_as_company_id && CompanyStore.get(record.acts_as_company_id, 'name') == Consts.LINCOLN)

isShowAccountEditable = (original_job) ->
  UserStore.isPartner() && not Utils.isAssignedFromFleet(original_job) and (not original_job?.account || not original_job.owner_company? || not original_job.rescue_company?.id? || original_job.owner_company.id == original_job.rescue_company.id)

isFleetDispatcherShown = (job) ->
  job?.type in ["FleetInHouseJob", "FleetMotorClubJob"] or UserStore.isSwoop()

isRootDispatcherShown = (job) ->
  job?.type in ["FleetManagedJob"]

fleetFirstAttrs = (original_job) ->
  !UserStore.isPartner() or (UserStore.isPartner() and not Utils.isAssignedFromFleet(original_job))

isSendingText = (job) ->
  (job.review_sms || job.partner_sms_track_driver || job.text_eta_updates || job.get_location_sms)

getCustomerTypeName = (job, watcherId) ->
  if job.customer_type_id
    return CustomerTypeStore.get(job?.customer_type_id, "name", watcherId)

isFeatureEnabledForPartnerOrSwoopAsPartner = (flag, acts_as_company_id, componentId) =>
  return FeatureStore.isFeatureEnabled(flag) ||
  (OrigUserStore.isSwoop() && acts_as_company_id && FeatureStore.isFeatureEnabled(flag, componentId, CompanyStore.getById(acts_as_company_id)))

requestDropLocationOptions = (drop_location_information, jobId, form, dropoffField) ->
  if jobId && drop_location_information.serviceLocation?.place_id
    payload = {
      job: {
        id: jobId,
        service: {
          name: drop_location_information.service
        },
        location: {
          service_location: {
              lat: drop_location_information.serviceLocation.lat,
              lng: drop_location_information.serviceLocation.lng,
              google_place_id: drop_location_information.serviceLocation.place_id,
          }
        }
      }
    }

    drop_location_information.loading = true
    dropoffField.changeCallback()
    AsyncRequestStore.requestDropLocationOptions(payload, {
        success: (data) ->
          drop_location_information.loading = false
          drop_location_information.dropOffSites = data.dropoff_sites
          dropoffField.changeCallback()
        error: (data) ->
          drop_location_information.loading = false
          drop_location_information.dropOffSites = []
          errorMessage = 'An error occurred retreiving drop off location options from the back end.'
          Rollbar.error(errorMessage, {payload: JSON.stringify(payload), dataError: data?.error, responseError: JSON.stringify(data?.responseJSON)})
          dropoffField.changeCallback()
      })
  else
    drop_location_information.loading = false
    dropoffField.changeCallback()

isFeatureEnabled = (name) =>
  FeatureStore.isFeatureEnabled(name)

module.exports =

FleetField: class FleetField extends Fields.StoreSelectorField
  _name: 'acts_as_company_id'
  _label: 'Client'
  getPopulateFunc: =>
    partial(CompanyStore.getFleetManagedForStore, @props.componentId)
  getValue: -> CompanyStore.get(@record.acts_as_company_id, "name")
  _required: true
  onChange: (id, val) ->
    if id?
      @render_props.onChange(id)
      @setValue(id)
  _placeholder: "Select a Client"
  isEditable: -> (!@record.id? || @record.status == Consts.PREDRAFT)
  isShowing: -> (UserStore.isSwoop() || @record.acts_as_company_id?)
  updateStores: (stores) =>
    UserStore = stores.UserStore
    UsersStore = stores.UsersStore
    ServiceStore = stores.ServiceStore
    SettingStore = stores.SettingStore
    CustomerTypeStore = stores.CustomerTypeStore
    ServiceAliasStore = stores.ServiceAliasStore
    FeatureStore = stores.FeatureStore
    SymptomStore = stores.SymptomStore
    QuestionStore = stores.QuestionStore
    LocationTypeStore = stores.LocationTypeStore
    DepartmentStore = stores.DepartmentStore
    VehicleCategoryStore = stores.VehicleCategoryStore
    SiteStore = stores.SiteStore
    PoiStore = stores.PoiStore

AccountField: class AccountField extends Fields.BackendSuggestField
  _name: "account"
  _label: "Account"
  _required: true
  _placeholder: 'Type to search'
  _labelClick: ->
    if not UserStore.isFleet() and not Utils.isAssignedFromFleet(@original) and not FeatureStore.isFeatureEnabled("Always Show Caller")
       @setToggle("callerToggle", "caller")
  isEditable: -> isShowAccountEditable(@original)
  getExample: -> values(AccountStore.getAll())?[0]
  getValue: -> @record.account?.name || JobStore.getAccountNameByJobId(@record?.id, @props.componentId) || ""
  isShowing: -> (isShowAccountEditable(@original) || @original?.account?) && not UserStore.isFleet() && not UserStore.isSwoop()
  alwaysBeforeSend: (newrecord) ->
    #BE doesn't need this as we are sending it up as account{ id: ? }
    delete newrecord.account_id

  getSuggestions: (options, callback) ->
    if !UserStore.isPartner() && !UserStore.isSwoop()
      return

    Api.autocomplete_accounts(options.input, 10, UserStore.getEndpoint(), {
      success: (data) =>
        # force lazy loading the account if needed
        # no need to pass the componentId since it's a callback function
        forEach(data, (account) -> AccountStore.get(account.id, 'id'))

        locationMap = map(data, (account) ->
            id: account.id
            label: account.name
          )
        callback(locationMap)
      error: (data, textStatus, errorThrown) =>
        console.warn("Autocomplete failed", data, textStatus, errorThrown)
    })

  customFindErrors: ->
    if @isEditable()
      accountId = @record.account?.id
      return "Required" if !accountId

      accountName = AccountStore.get(accountId, 'name')

      if accountName in [Consts.SWOOP] and @isEditable() and @original.account_id != accountId
        name = AccountStore.get(accountId, 'company', @props.componentId)?.name || accountName
        return "Please call #{name} and have them assign the job to you"

  onChange: (e) ->
    if e.target?.value
      id = e.target.value
      accoundDepartmentId = AccountStore.get(id, 'department_id')

      if UserStore.isPartner() and accoundDepartmentId
        @setValue(accoundDepartmentId, true, "partner_department_id")

      @setValue(id, true, "account_id")
      @setValue({
        id: id
        name: AccountStore.get(id, 'name')
      })
    else
      @setValue(null)

DepartmentIdField: class DepartmentIdField extends Fields.SelectField
  _name: "department_id"
  _label: "Department"
  _required: true
  onChange: (e) ->
    if UserStore.isFleetResponse() && DepartmentStore.get(e.target.value, "name", @props.componentId) == 'MEMA-MASS EMERGENCY MANAGEMENT'
      taxExemptOption = CustomerTypeStore.getCustomerTypeByName("Tax Exempt")?.id
      if taxExemptOption?
        @setValue(taxExemptOption, true, "customer_type_id")
    super(e)
  isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS) and UserStore.isFleet()
  getExample: -> values(DepartmentStore.getDepartments(@props.componentId))?[0]?.id
  getOptions: -> @optionMapper(sortBy(DepartmentStore.getDepartments(@props.componentId), "sort_order"), clone(super(arguments...)))
  prefill: ->
    if UserStore.isTravelCar() and !@record?.department_id? and DepartmentStore.getDepartmentByName("Roadside")?
      @setValue(DepartmentStore.getDepartmentByName("Roadside")?.id)

PartnerDepartmentIdField: class PartnerDepartmentIdField extends Fields.SelectField
  _name: "partner_department_id"
  getLabel: -> if UserStore.isSouthern() then "Division" else "Department"
  isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_DEPARTMENTS) and UserStore.isPartner()
  getExample: -> values(DepartmentStore.getDepartments(@props.componentId))?[0]?.id
  getOptions: -> @optionMapper(sortBy(DepartmentStore.getDepartments(@props.componentId), "sort_order"), clone(super(arguments...)))

CustomerLookupTypeField: class CustomerLookupTypeField extends Fields.SelectField
  _name: "customer_lookup_type"
  _label: "Customer Lookup Type"
  isRequired: ->
    !@record?.pcc_coverage?.data[0]?.name == Consts.COVERAGE.CLIENT_API_COVERED

  getClassName: -> super(arguments...) + " customer-lookup-field"
  isShowing: -> !UserStore.isPartner() && FeatureStore.isFeatureEnabled(Consts.FEATURE_JOB_FORM_CUSTOMER_LOOKUP)
  prefill: ->
    if @record?.pcc_coverage?.data[0]?.name == Consts.COVERAGE.CLIENT_API_COVERED
      return
    else if @record?.customer_lookup_type
      @setValue(@record.customer_lookup_type)
    else
      customerLookupTypes = SettingStore.getSettingByKey(Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.CUSTOMER_LOOKUP_TYPES, true)

      if includes(customerLookupTypes, Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER)
        @setValue(Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER)
      else if includes(customerLookupTypes, Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER)
        @setValue(Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER)
      else if includes(customerLookupTypes, Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN)
        @setValue(Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN)

  getOptions: ->
    customerLookupTypes = SettingStore.getSettingByKey(Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.CUSTOMER_LOOKUP_TYPES, true) or []

    @optionMapper(customerLookupTypes, clone(super(arguments...)))

LookupTypeVinField: class LookupTypeVinField extends Fields.UppercaseField
  _name: "lookup_type_vin"
  _label: "Last 8 Characters of VIN"
  _example: "3EO97436"
  prefill: ->
    if @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN && @record?.vin?.length > 7
      @setValue(@record.vin.slice(-8))

  isShowing: -> !UserStore.isPartner() && @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN
  getClassName: -> super(arguments...) + " fs-exclude"
  mixin(@, MaxLengthMixin(8))

LookupTypeLastNameField: class LookupTypeLastNameField extends Fields.CapitalizedField
  _name: "lookup_type_last_name"
  _label: "Last Name"
  isShowing: -> !UserStore.isPartner() && @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP
  getRedact: ->
    if @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP && @record?.pcc_coverage
      return Consts.SECRET_FIELD_DOTS

LookupTypeZipField: class LookupTypeZipField extends Fields.ZipField
  _name: "lookup_type_zip"
  _label: "Zip"
  isShowing: ->  !UserStore.isPartner() && @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP
  getRedact: ->
    if @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP && @record?.pcc_coverage
      return Consts.SECRET_FIELD_DOTS

LookupTypePhoneNumberField: class LookupTypePhoneNumberField extends Fields.PhoneField
  _name: "lookup_type_phone_number"
  _label: "Phone"
  _example: "+12315555555"
  _autocomplete: "off"
  _validatePhone: false
  allowDisableMasking: true

  forcePrefill: ->
    if @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER && @record?.customer?.phone
      @setValue(@record.customer.phone)

  isShowing: -> !UserStore.isPartner() && @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER
  getClassName: -> super(arguments...) + " fs-exclude"

LookupTypeUnitNumberField: class LookupTypeUnitNumberField extends Fields.UppercaseField
  _name: "lookup_type_unit_number"
  _label: "Unit Number"
  _example: "2AB074"
  prefill: ->
    if @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER && @record?.unit_number?
      @setValue(@record.unit_number)

  isShowing: -> !UserStore.isPartner() && @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER
  getClassName: -> super(arguments...) + " fs-exclude"

LookupTypePolicyNumberField: class LookupTypePolicyNumberField extends Fields.UppercaseField
  _name: "lookup_type_policy_number"
  _label: "Policy Number"
  _example: "L3DWBM"
  forcePrefill: ->
    if @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER && @record?.pcc_policy?.policy_number
      @setValue(@record.pcc_policy.policy_number)

  isShowing: -> !UserStore.isPartner() && @record?.customer_lookup_type == Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER
  getClassName: -> super(arguments...) + " fs-exclude"
  mixin(@, MaxLengthMixin(6))

CustomerTypeIdField: class CustomerTypeIdField extends Fields.SelectField
  _name: "customer_type_id"
  getLabel: ->
    (if Utils.isEnterpriseJob(UserStore, @original) then "Claim Type" else "Payment Type")
  isShowing: ->
    if UserStore.isTesla()
      return DepartmentStore.get(@record?.department_id, "name", @props.componentId) in [Consts.DEPARTMENT_ROADSIDE, Consts.DEPARTMENT_AUS_NZ_ROADSIDE]
    if @record?.customer_type_id?
      return true
    if @record?.id && CustomerTypeStore.getCompanyItems(@props.componentId, null, null, @record?.owner_company_id).length > 0
      return true
    FeatureStore.isFeatureEnabled(Consts.FEATURES_PAYMENT_TYPE)

  alwaysBeforeSend: (newrecord) ->
    if UserStore.isTesla() and DepartmentStore.get(@record?.department_id, "name", @props.componentId) not in [Consts.DEPARTMENT_ROADSIDE, Consts.DEPARTMENT_AUS_NZ_ROADSIDE]
      newrecord["customer_type_id"] = null

  isRequired: -> UserStore.isFleet() and FeatureStore.isFeatureEnabled(Consts.FEATURES_PAYMENT_TYPE)
  getExample: -> values(@getOptions())?[0]?.id
  prefill: ->
    if (UserStore.isFarmersOwned() || UserStore.isTravelCar() || UserStore.isMetromile() || UserStore.isUSAA()) and !@record?.customer_type_id? and CustomerTypeStore.getCustomerTypeByName("Covered")?
      @setValue(CustomerTypeStore.getCustomerTypeByName("Covered")?.id)

  getOptions: ->
    options = []
    if @record?.owner_company?.customer_types
      options = clone(@record?.owner_company?.customer_types)
      #TODO: add support for loading scoped objects in api, remove owner_company? from api, and just use this \/
      #CustomerTypeStore.getCompanyItems(@props.componentId, null, null, @record?.owner_company_id)
    else if UserStore.isPartner() || UserStore.isFleet()
      options = CustomerTypeStore.getCompanyItems(@props.componentId)

    if @record?.customer_type_id?
      #TODO: this if statement doesn't seem to be working but fixed by the dedupe below
      if map(options, (option) -> option?.id).indexOf(@record.customer_type_id) == -1
        options.unshift(CustomerTypeStore.getById(@record.customer_type_id, true, @props.componentId))

    #alphabetize
    options = sortBy(options, (ct) ->  ct?.name)
    options = uniq(options, (ct) -> ct?.id)
    @optionMapper(options, clone(super(arguments...)))

AccountNotesField: class AccountNotesField extends Fields.SpanField
  _name: "account_notes"
  _label: "Account Notes"
  getValue: ->
    if @record?.account?.id
      AccountStore.get(@record.account.id, 'notes', @props.componentId)
  isShowing: ->
    UserStore.isPartner() and @record?.account?.id and AccountStore.get(@record.account.id, 'notes', @props.componentId)?.length > 0

CallerNameField: class CallerNameField extends Fields.CapitalizedField
  _label: "Caller Name"
  _name: "caller.full_name"
  getClassName: -> super(arguments...) + " fs-exclude"
  getName: ->
    if isFleetDispatcherShown(@original)
      "fleet_dispatcher.full_name"
    else if isRootDispatcherShown(@original)
      "root_dispatcher.full_name"
    else
      "caller.full_name"
  mixin(@, InheritMixin("caller"))
  mixin(@, CallerMixin)

CallerPhoneField: class CallerPhoneField extends Fields.PhoneField
  _label: "Caller Phone"
  _name: "caller.phone"
  getClassName: -> super(arguments...) + " fs-exclude"
  getName: -> if isFleetDispatcherShown(@original) then "fleet_dispatcher.phone" else "caller.phone"
  mixin(@, InheritMixin("caller"))
  mixin(@, CallerMixin)
  getValue: ->
    if isRootDispatcherShown(@original)
      Consts.DISPATCH_NUMBER
    super(arguments...)
  getName: ->
    if isFleetDispatcherShown(@original)
      "fleet_dispatcher.phone"
    else if isRootDispatcherShown(@original)
      "root_dispatcher.phone"
    else
      "caller.phone"

PONumberField : class PONumberField extends Fields.Field
  _name: "po_number"
  getLabel: -> Utils.getPOLabel(UserStore, @original)
  getClassName: -> super(arguments...) + " fs-exclude"
  isRequired: ->
    if FeatureStore.isFeatureEnabled("Job PO Number")
      if UserStore.isPartner()
        account_id = @record?.account?.id || @record?.account_id
        if account_id and AccountStore.get(account_id, 'po_required_at_job_creation', @props.componentId)
          return true
      if Utils.isEnterpriseJob(UserStore, @original)
        customerTypeName = getCustomerTypeName(@record, @props.componentId)
        return customerTypeName in [Consts.CUSTOMER_TYPE_BRANCH, Consts.CUSTOMER_TYPE_WARRANTY, Consts.CUSTOMER_TYPE_CUSTOMER_PAY, Consts.CUSTOMER_TYPE_MECHANICAL, Consts.CUSTOMER_TYPE_SERVICE_CENTER]
      return false
  isShowing: -> FeatureStore.isFeatureEnabled("Job PO Number")

RefNumberField: class RefNumberField extends Fields.Field
  _name: "ref_number"
  getLabel: -> Utils.getRefLabel(UserStore, @original)
  getClassName: -> super(arguments...) + " fs-exclude"
  isRequired: ->
    if Utils.isTuroJob(UserStore, @original) || Utils.isMetromileJob(UserStore, @original) || (!@original?.id? && Utils.isFleetResponseJob(UserStore, @original))
      return true
    if Utils.isEnterpriseJob(UserStore, @original)
      customerTypeName = getCustomerTypeName(@record, @props.componentId)
      if customerTypeName == Consts.CUSTOMER_TYPE_AUCTION || customerTypeName == Consts.CUSTOMER_TYPE_DAMAGE
        return true
    return false
  isEditable: ->
    if @original?.account?.name?
      return @original?.account?.name != 'Agero' && fleetFirstAttrs(@original)
    return fleetFirstAttrs(@original)
  isShowing: ->
    if @original?.account?.name == 'Agero'
      return FeatureStore.isFeatureEnabled(Consts.FEATURES_REF_NUMBER) && !isEmpty(@original?.ref_number)
    return @original?.ref_number? || (FeatureStore.isFeatureEnabled(Consts.FEATURES_REF_NUMBER) && fleetFirstAttrs(@original))

MemberNumberField: class MemberNumberField extends Fields.Field
  _name: "customer.member_number"
  _label: "Member Number"
  getClassName: -> super(arguments...) + " fs-exclude"
  isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURE_MEMBER_NUMBER) && !UserStore.isPartner()


UnitNumberField: class UnitNumberField extends Fields.Field
  _name: "unit_number"
  _label: "Unit"

  isRequired: ->
    (
      FeatureStore.isFeatureEnabled(Consts.FEATURES_JOB_FORM_UNIT_REQUIRED)
    )

  isEditable: -> fleetFirstAttrs(@original)
  isShowing: -> (FeatureStore.isFeatureEnabled(Consts.FEATURES_UNIT_NUMBER) and fleetFirstAttrs(@original)) or @original?.unit_number?

PolicyHolderToggle: class PolicyHolderToggle extends Fields.ToggleField
  _name: "policy_holder"
  _label: "Policy Holder"
  _onText: "Address"
  _offText: "Number"
  prefill: ->
    if @record?.policy_location?
      @toggleOn?()
  isShowing: ->
    return UserStore.isUSAA()
  onChange: ->
    #Will be toggled off in isOff
    if @isOn()
      @record["policy_number"] = null
    else
      @record["policy_location"] = null
    super(arguments...)


PolicyNumberField: class PolicyNumberField extends Fields.Field
  _name: "policy_number"
  _label: "Policy Number"
  getClassName: -> super(arguments...) + " fs-exclude"
  mixin(@, MaxLengthMixin(20))
  _example: "1234567"
  findErrors: (ignoreRequired) ->
    val = @getValue(true)
    if val?.indexOf(" ") >= 0
      return "Space not allowed in policy number"
    if UserStore.isFarmersOwned() and @isRequired() and !ignoreRequired and (!val? || val.length < 7 || val.length > 12 || not val.match /^[0-9]+$/)
      return "Must be 7-12 numbers"
    super(arguments...)
  isRequired: -> UserStore.isUSAA() || (UserStore.isFarmersOwned() && @record.policy_search_type == "policy")
  isShowing: ->
    UserStore.isMetromile() || UserStore.isMTS() ||
    (UserStore.isUSAA() && !@toggles["policy_holder"]) ||
    (UserStore.isFarmersOwned())

#FarmersLastNameField: class FarmersLastNameField extends Fields.Field
#  _name: "farmers_last_name"
#  _label: "Last Name"
#  _required: true
#  isShowing: -> UserStore.isFarmersOwned && @record.policy_search_type == "zip"
#
#FarmersZipField: class FarmersZipField extends Fields.NumberField
#  _name: "farmers_zip"
#  _label: "Zip"
#  _required: true
#  findErrors: ->
#    val = @getValue()
#    if !val? or val.length < 5 or not val.match /^[0-9]+$/
#      return "Must be 5 numbers"
#  isShowing: -> UserStore.isFarmersOwned && @record.policy_search_type == "zip"

PolicySearchTypeField: class PolicySearchTypeField extends Fields.SelectField
  _name: "policy_search_type"
  _label: "Policy Search Type"
  isShowing: -> UserStore.isFarmersOwned()
  prefill: ->
    if @isShowing()
      if !@record?.id || @record.covered == true || @record.covered == false
        @setValue("policy")
      else
        @setValue("manual")
  onChange: (e) =>
    if @render_props?.onChange?
      @render_props.onChange(e)

    super(arguments...)
  getOptions: ->
    [
      {value: "policy", text: "Search by policy number"},
      {value: "manual", text: "Skip and enter manually"},
      # {value: "zip", text: "Search by Last Name & Zip"}
    ]

PolicyAddressField: class PolicyAddressField extends Fields.GeoSuggestField
  _name: "policy_location"
  _label: "Policy Address"
  isRequired: -> UserStore.isUSAA()
  customFindErrors: ->
    if !@getValue(true, @getName())?.address?
      return "Field Required"
  getToggleName: ->
    if UserStore.isUSAA()
      return "policy_holder"
  isShowing: -> UserStore.isUSAA() && @toggles["policy_holder"]

ContractorIdField: class CustomerIdField extends Fields.SpanField
  _name: "issc_dispatch_request.contractor_id"
  _label: "Vendor ID"
  isShowing: -> @record?.issc_dispatch_request?.contractor_id?
CustomerNameField: class CustomerNameField extends Fields.CapitalizedField
  _name: "customer.full_name"
  _label: "Customer Name"
  _autocomplete: "off"
  getClassName: -> super(arguments...) + " fs-exclude"
  isRequired: ->
    !FeatureStore.isFeatureEnabled(Consts.FEATURES_NOT_REQUIRE_CUSTOMER_INFO)
  getLabel: -> if Utils.isEnterpriseJob(UserStore, @original) then "Branch Contact" else @_label
  _placeholder: "First Last"
  _example: "Quinn Baetz"
  isShowing: ->
    return !(UserStore.isPartner() && @record?.status == Consts.AUTO_ASSIGNING)

CustomerPhoneFactory: (isInternational) ->
  BaseClass = if isInternational then Fields.IntlPhoneField else Fields.PhoneField

  class CustomerPhoneField extends BaseClass
    _name: "customer.phone"
    _label: "Customer Phone"
    _autocomplete: "off"
    allowDisableMasking: true
    getCompanyLocation: () -> UserStore.getCompany()?.location
    getClassName: -> super(arguments...) + " fs-exclude"
    getLabel: -> if Utils.isEnterpriseJob(UserStore, @original) then "Branch Contact Phone" else @_label
    _example: "+12315555555"
    getInfo: ->
      if UserStore.isFarmersOwned()
        "I'd like to text you your service provider's name and ETA. Is this a number that can receive text messages?"

    isRequired: ->
      if FeatureStore.isFeatureEnabled(Consts.FEATURES_NOT_REQUIRE_CUSTOMER_INFO)
        return false
      return UserStore.isFleet() || (@record?.send_sms_to_pickup == false and isSendingText(@record))
    isShowing: ->
      !(UserStore.isPartner() && @record?.status == Consts.AUTO_ASSIGNING)

OwnerCompanyEmailField: class OwnerCompanyEmailField extends Fields.EmailField
  _name: "dispatch_email"
  _label: "Email Details:"
  _placeholder: "Dispatch Email"

CustomerEmailField: class CustomerEmailField extends Fields.EmailField
  _name: "email"
  _label: "Customer Email"
  _autocomplete: "off"
  getClassName: -> super(arguments...) + " fs-exclude"
  isRequired: -> UserStore.isTravelCar()
  isShowing: ->
    if isLincolnForm(@record)
      return true
    if SettingStore.getSettingByKey("always_show_customer_email")?
      return true
    if CustomerTypeStore.get(@record?.customer_type_id, "name", @props.componentId) == Consts.CUSTOMERPAY
      return true
    if UserStore.isTravelCar() || UserStore.isTuro()
      return true
    return @record?.account?.name == Consts.CASHCALL

CopyCustomerField: class CopyCustomerField extends Fields.ButtonField
  _name: "copy_customer"
  _label: "Copy Customer"
  canCopy: ->
    an = @record?.customer?.full_name
    ap = @record?.customer?.phone
    bn = @record?.pickup_contact?.full_name
    bp = @record?.pickup_contact?.phone
    an = "" if not an?
    ap = "" if not ap?
    bn = "" if not bn?
    bp = "" if not bp?
    return (an != bn) or (ap != bp)
  onChange: (field, record) =>
    if @canCopy()
      @setValue(@getValue(true, "customer.full_name"), true, "pickup_contact.full_name")
      @setValue(@getValue(true, "customer.phone"), false, "pickup_contact.phone")
    else
      @setValue("", true, "pickup_contact.full_name")
      @setValue("", false, "pickup_contact.phone")


NotesField: class NotesField extends Fields.TextareaField
  _name: "notes"
  _label: "Job Details"
  _placeholder: "Job Details"
  isShowing: -> OrigUserStore.isFleet() or OrigUserStore.isSwoop() or @record?.notes?.length > 0
  isEnabled: -> OrigUserStore.isFleet() or OrigUserStore.isSwoop()
  mixin(@, NotesMixin)

FleetNotesField: class FleetNotesField extends Fields.TextareaField
  _name: "fleet_notes"
  _placeholder: "Dispatcher Only Notes"
  getLabel: ->  if OrigUserStore.isSwoop() then "Client" else "Internal"
  isShowing: -> OrigUserStore.isFleet() or OrigUserStore.isSwoop()
  mixin(@, NotesMixin)

PartnerNotesField: class PartnerNotesField extends Fields.TextareaField
  _name: "partner_notes"
  _placeholder: "Dispatcher and Driver Only Notes"
  getLabel: ->  "Internal"
  isShowing: -> OrigUserStore.isPartner()
  mixin(@, NotesMixin)

DispatchNotesField: class DispatchNotesField extends Fields.TextareaField
  _name: "dispatch_notes"
  _placeholder: "Dispatcher Only Notes"
  getLabel: ->  "Dispatcher"
  isShowing: -> OrigUserStore.isPartner() and not UserStore.isOnlyDriver()
  mixin(@, NotesMixin)

DriverNotesField: class DriverNotesField extends Fields.TextareaField
  _name: "driver_notes"
  _placeholder: "Notes for Invoice"
  _label: "Invoice"
  isShowing: ->
    ((@record?.driver_notes? or @record?.rescue_driver_id?) and OrigUserStore.isFleet() and OrigUserStore.isFleetInHouse() ) or OrigUserStore.isPartner() or (OrigUserStore.isSwoop() and @record?.driver_notes?.length > 0 )
  isEnabled: -> OrigUserStore.isPartner()
  mixin(@, NotesMixin)

SwoopNotesField: class SwoopNotesField extends Fields.TextareaField
  _name: "swoop_notes"
  _placeholder: "Swoop Only Notes"
  _label: "Swoop"
  isShowing: -> OrigUserStore.isSwoop()
  mixin(@, NotesMixin)

ServiceTimeToggle: class ServiceTimeToggle extends Fields.ToggleField
  _name: "service_time"
  _label: "Service Time"
  _onText: "Scheduled"
  _offText: "ASAP"
  prefill: ->
    if @record?.scheduled_for
      @toggleOn?()
  isShowing: ->
    return @props?.form_type != "Storage" and @record?.service != "Storage" and @record?.status not in ["Released", "Stored"]
  onChange: ->
    #Will be toggled off in isOff
    if @isOn()
      @record["scheduled_for"] = null
    super(arguments...)

ScheduledForField: class ScheduledForField extends Fields.DateTimeField
  _name: "scheduled_for"
  _label: "Scheduled For"
  _toggleName: "service_time"
  _showTimezone: true
  _className: "inside-scheduled-form-section"
  _disableValidation: true
  isShowing: -> @props?.form_type != "Storage"
  findErrors: ->
    errorMessage = 'Job must be scheduled after the current time'
    isInDraftStatus = @record?.status in [Consts.PREDRAFT, Consts.DRAFT]
    scheduledDateString = @record?.scheduled_for

    if isInDraftStatus || (!isInDraftStatus && @original?.scheduled_for != scheduledDateString)
      if !scheduledDateString || scheduledDateString == 'Invalid date'
        return errorMessage

      scheduledMoment = moment(scheduledDateString)
      minuteDiffNowAndScheduledTime = Math.ceil(scheduledMoment.diff(moment(), "minutes", true))

      if !moment.isMoment(scheduledMoment)
        return errorMessage

      if minuteDiffNowAndScheduledTime < 0
        return errorMessage

      if not JobStore.isFleetManagedJob(@record)
        # Qualifying jobs eject from further valid time checks
        return

      errorMessage = 'Job must be scheduled at least 2 hours after the current time'
      if minuteDiffNowAndScheduledTime < 120
        return errorMessage

TempEtaField: class TempEtaField extends Fields.Field
  _name: "temp_eta_mins"
  _classes: "col eta_time"
  _label: "ETA"
  _antiToggleName: "service_time"
  getRealName: -> "toa.latest"
  getRedact: -> if !@isEnabled() then  "--" else false
  isEnabled: ->
    if !@record?.id? || @record?.status in [Consts.PREDRAFT, Consts.AUTO_ASSIGNING, Consts.UNASSIGNED,Consts.PENDING, Consts.ACCEPTED, Consts.DISPATCHED, Consts.ENROUTE]
      return true
    return false

  isShowing: ->
    if @props?.form_type == "Storage" || @record?.scheduled_for?
      return false

    return  OrigUserStore.isSwoop() or ( UserStore.isFleet() and UserStore.isFleetInHouse() and @record?.id) or UserStore.isPartner()

  aboutToSend: (job) ->
    if job.temp_eta_mins? and not isNaN(parseInt(job.temp_eta_mins))
      m = moment().add(job.temp_eta_mins, "minutes")
      Utils.setValue(job, @getRealName(), m.toISOString())
    delete job.temp_eta_mins

  prefill: () ->
    datetime = get(@record, @getRealName())
    if datetime?
      mins = Math.ceil(moment(datetime).diff(moment(), "minutes", true))
      @setValue(mins, true, "original_temp_eta_mins")
      @setValue(mins)

  findErrors: () ->
    if @record?.temp_eta_mins?.length > 0 and isNaN(parseInt(@record?.temp_eta_mins))
      return "invalid"
    super(arguments...)

  beforeSend: () ->
    if @record?.temp_eta_mins != @record?.original_temp_eta_mins and not isNaN(parseInt(@record?.temp_eta_mins))
      m = moment().add(@record?.temp_eta_mins, "minutes")
      Utils.setValue(@record, @getRealName(), m.toISOString())

    delete @record["temp_eta_mins"]
    delete @record["original_temp_eta_mins"]

LiveEtaField: class LiveEtaField extends Fields.SpanField
  _name: "toa.live"
  _antiToggleName: "service_time"
  _label: "Live ETA"
  _enabled: false
  isShowing: () -> @props?.form_type != "Storage" and @getValue() and !@record?.scheduled_for? and @record?.status in [Consts.ENROUTE]
  getValue: () ->
    datetime = super(arguments...)
    if datetime?
      mins = Math.ceil(moment(datetime).diff(moment(), "minutes", true))
      return mins + " min"


SymptomField: class SymptomField extends Fields.SelectField
  _name: "symptom_id"
  getLabel: -> Utils.getSymptomLabel(UserStore)
  isShowing: -> FeatureStore.isFeatureEnabled("Show Symptoms")
  isRequired: ->  FeatureStore.isFeatureEnabled("Show Symptoms") and UserStore.isFleet()
  onChange: (e) ->
    super(arguments...)

    if !@record.service_code_id
      symptom_name = SymptomStore.get(@record.symptom_id, "name", @props.componentId)
      symptom_service_map = {
        "Dead battery":"Battery Jump",
        "Flat tire":"Tire Change",
        "Locked out":"Lock Out",
        "Long distance tow":"Tow",
        "Out of fuel":"Fuel Delivery",
      }
      if symptom_service_map[symptom_name]?
        @props?.form.fields.service.onChange(target: value: symptom_service_map[symptom_name])

  getExample: -> values(SymptomStore.getCompanyItems(@props.componentId))?[0]?.id
  getOptions: -> @optionMapper(sortBy(SymptomStore.getCompanyItems(@props.componentId), "name"), clone(super(arguments...)))

ServiceHackField: class ServiceHackField extends Fields.Field
  _name: "service_hack"
  _label: "Service"
  getValue: ->
    return ServiceAliasStore.getServiceNameOrAlias(@record?.service_code_id, @record?.service_alias_id) + if @record?.will_store then ", Storage" else ""

ServiceField: class ServiceField extends Fields.SelectField
  _name: "service"
  _label: "Service"
  _required: true
  _example: values(ServiceStore.getServices())?[0]?.id

  isEditable: -> @props?.form_type != "Storage"
  forcePrefill: ->
    if @props?.form_type == "Storage" and !@original?.service_code_id?
      @setValue(ServiceStore.getServiceByName("Storage", @props.componentId)?.id, false, "service_code_id")
      @setValue("Storage")

  alwaysBeforeSend: (newrecord) ->
    #I'm sending up service_code_id so I don't need to send service as well
    delete newrecord.service
    if newrecord['service_alias_id'] == null
      delete newrecord['service_alias_id']
  getValue: -> ServiceAliasStore.getServiceNameOrAlias(@record?.service_code_id, @record?.service_alias_id)
  findErrors: ->
    service = ServiceStore.getById(@record.service_code_id)

    if service?.name == Consts.TIRE_CHANGE and FeatureStore.isFeatureEnabled("Questions")
      answer = QuestionStore.getQuestionAnswer(@record.service_code_id, @record?.question_results, "Have a spare in good condition?")
      if answer?.answer == "No"
        return "Change to Tow if no spare tire"
    super(arguments...)

  setQuestion: (question_key, answer_name) ->
    #TODO: after defaults are set we don't unset them when changing services
    questions = QuestionStore.getQuestionsForServiceId(@record.service_code_id)
    for question in questions
      if question.name == question_key
        for answer in question.answers
          if answer.answer == answer_name
            if !@record.question_results?
              @record.question_results = []
            for res in @record.question_results
              if ""+res.question_id == ""+question.question_id
                #If the question already exists in the set we shouldn't default it to something else
                return
            @record.question_results.push({
              question_id: parseInt(question.question_id)
              answer_id: parseInt(answer.id)
            })

  onChange: (e) ->
    service_name = e
    if e?.target?.value?
      service_name = e.target.value

    if !service_name || service_name.length == 0
      @setValue("", true, "service")
      @setValue(null, false, "service_code_id")
      return

    alias = ServiceAliasStore.getServiceAliasByName(service_name)
    if alias?
      alias_id = alias.id
      service_id = alias.service_code_id
    else
      service_id = ServiceStore.getServiceByName(service_name, @props.componentId)?.id

    service = ServiceStore.getById(service_id)

    if service
      if service.name not in Consts.SERVICES_THAT_ENABLE_STORAGE
        @setValue(false, true, "will_store")
        @setValue(false, true, "second_tow")
      if service.name in ["Impound"]
        @setValue(false, true, "will_store")
      @setValue(service.name, true, "service")

    @setValue(alias_id, true, "service_alias_id")
    @setValue(service_id, false, "service_code_id")

    super(e)

  getOptions: ->
    options = []
    serviceIdsToRemove = []
    services = ServiceStore.getServices(@props.componentId)

    aliases = values(ServiceAliasStore.getAll())
    for alias in aliases
      if services[alias.service_code_id]?
        serviceIdsToRemove.push(alias.service_code_id)
        options.push(alias.alias)

    for service in values(services)
      if service.name == "Storage" and @record?.status not in ["Released", "Stored"]
        continue
      if service.name == "GOA"
        continue

      if serviceIdsToRemove.indexOf(service.id) == -1
        options.push(service.name)

    currentValue = @getValue()
    if currentValue and currentValue not in options
      options.push(currentValue)

    options = options.sort()
    Array.prototype.unshift.apply(options, super(arguments...))
    return options

PriorityMessage: class PriorityMessage extends Fields.ButtonField
  _name: "priority_response"
  getLabel: ->
    span
      className: 'fa ' + (if @getValue() then "fa-flag red" else "fa-flag-o")
      style:
        color: (if @getValue() then "red")
  isShowing: ->
    FeatureStore.isFeatureEnabled(Consts.ADDITION_PRIORITY_RESPONSE) and !@record?.scheduled_for?
  prefill: -> @setIfEmpty(false)
  alwaysBeforeSend: (newRecord) ->
    if @record?.scheduled_for?
      newRecord.priority_response = false

RescueDriverIdField: class RescueDriverIdField extends Fields.SelectField
  _name: "rescue_driver_id"
  _label: "Driver Assigned"
  isEditable: -> @record?.status not in [Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION]
  isShowing: ->
    UserStore.isPartner() and @props?.form_type != "Storage" and (not @original?.status? or @original?.status in [Consts.PREDRAFT, Consts.DRAFT, Consts.PENDING, Consts.DISPATCHED, Consts.ACCEPTED])
  getOptions: ->
    drivers = UsersStore.getSortedDriverIds(@props.componentId)
    driver = @getValue()
    if driver? and parseInt(driver) not in drivers
        drivers.push(driver)

    options = clone(super(arguments...))
    for id in drivers
      options.push({
        text: UsersStore.get(id, "full_name", @props.componentId) || "..loading"
        value: id
      })

    #TODO: Sort this by full_name

    return options

RescueVehicleIdField: class RescueVehicleIdField extends Fields.SelectField
  _name: "rescue_vehicle_id"
  _label: "Truck Assigned"
  isShowing: ->
    return @props?.form_type != "Storage" and
      UserStore.isPartner() and
      (not @original?.status? or @original?.status != Consts.SUBMITTED) and
      (not Utils.isAssignedFromFleet(@original) or @original?.status != Consts.ASSIGNED or @original?.rescue_vehicle_id != null )
  getOptions: -> @optionMapper(VehicleStore.getSortedVehicles(@props.componentId), clone(super(arguments...)))

TrailerIdField: class TrailerIdField extends Fields.SelectField
  _name: "trailer_vehicle_id"
  _label: "Trailer Assigned"
  isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_TRAILER)
  getOptions: -> @optionMapper(TrailerStore.getAll(false, @props.componentId), clone(super(arguments...)))

StorageTypeId: class StorageTypeId extends Fields.SelectField
  _name: "storage_type_id"
  _label: "Storage Type"
  isShowing: ->
    (@record?.will_store || @record?.service == "Storage" || @props?.form_type == "Storage") and values(StorageTypeStore.loadAndGetAll(@props.componentId)).length > 0
  getOptions: -> @optionMapper(values(StorageTypeStore.loadAndGetAll(@props.componentId)), clone(super(arguments...)))

DropLocationSiteIdField: class DropLocationSiteIdField extends Fields.SelectField
  _name: "drop_location.site_id"
  _label: "Site"
  _required: true
  isShowing: -> UserStore.isPartner() and (@record?.will_store || @record?.service == "Storage" || @props?.form_type == "Storage")

  onChange: (e) ->
    id = e
    if id?.target?.value?
      id = id.target.value

    if SiteStore.exists(id)
      loc = clone(SiteStore.get(id, 'location'))
      loc.site_id = id
      delete loc['id']

      if @record?.storage?
        @setValue(id, true, "storage.site_id")

      @setValue(loc, false, "drop_location")
    else
      @setValue(null, false, "drop_location")

  getOptions: ->
    siteOptions = map(SiteStore.loadAndGetAll(@props.componentId, 'name'), (site) =>
      { id: site.id, name: site.name }
    )

    @optionMapper(sortBy(siteOptions, 'name'), clone(super(arguments...)))

SecondTowDropoffField: class SecondTowDropoffField extends Fields.GeoSuggestField
  _name: "second_tow_dropoff"
  _label: "Tow Destination Address"
  _required: true
  isShowing: -> @record?.second_tow


SecondTowScheduledForField : class SecondTowScheduledForField extends Fields.DateTimeField
  _name: "second_tow_scheduled_for"
  _label: "Scheduled For"
  _showTimezone: true
  _className: "inside-service-form-section"
  isShowing: -> @record?.second_tow
  validate: (field, record, str) ->
    #m = moment(str)
    #if !record.second_tow
    #  return true
    #if m.isSame(@original?.second_tow_scheduled_for)
    #  return true
    #return (m.isValid() && m.isBetween(moment(), moment().add(2, 'years'))) || str.length == 0
    return true

SecondTowField : class SecondTowField extends Fields.ButtonField
  _name: "second_tow"
  getLabel: -> if @record?.second_tow then "Remove Tow Out" else "Add Tow Out"
  isShowing: ->
    return (not @record?.id? or @record?.status in [Consts.DRAFT, Consts.PREDRAFT]) and FeatureStore.isFeatureEnabled("Storage") and UserStore.isPartner() and (@record?.will_store or @record?.service == "Storage")
  alwaysBeforeSend: (newRecord) ->
    delete newRecord["second_tow"]
    if !@isShowing()
      delete newRecord["second_tow_dropoff"]
      delete newRecord["second_tow_scheduled_for"]

WillStoreField: class WillStoreField extends Fields.ButtonField
  _name: "will_store"
  getLabel: -> if @record?.will_store then "Remove Storage" else "Add Storage"
  isShowing: ->
    UserStore.isPartner() and @props?.form_type != Consts.STORAGE and ServiceStore.get(@record?.service_code_id, "support_storage", @props.componentId) and ServiceStore.isStorageEnabled(@props.componentId)
  onChange: ->
    super(arguments...)

    if !@getValue()
      @setValue(null, false, "second_tow")
      @setValue(null, false, "drop_location.site_id")
  alwaysBeforeSend: (newRecord) ->
    if !@isShowing()
      newRecord.will_store = false

MakeField: class MakeField extends Fields.AutosuggestField
  _name: "make"
  _label: "Make"
  _placeholder: "Type or select"
  _example: "Lincoln"
  getClassName: -> super(arguments...) + (if @record.loadedMakeFromVIN then "field-populated-by-another-field-highlight" else "")
  isRequired: -> !UserStore.isPartner()
  isEditable: -> !UserStore.isTesla()
  prefill: -> if UserStore.isTesla() then @setValue("Tesla")
  getSuggestions: -> EdmundsStore.getMakeNames(@record?.vehicle_type)

VehicleTypeField: class VehicleTypeField extends Fields.AutosuggestField
  _name: "vehicle_type"
  _label: "Style"
  _placeholder: "Type or select"
  _example: "Compactor"
  isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_HEAVY)
  getSuggestions: -> EdmundsStore.getTypeNames(@record?.make)

ModelField: class ModelField extends Fields.AutosuggestField
  _name: "model"
  _label: "Model"
  _example: "Continental"
  _placeholder: "Type or select"
  getClassName: -> super(arguments...) + (if @record.loadedModelFromVIN then "field-populated-by-another-field-highlight" else "")
  isRequired: -> (!UserStore.isPartner() and @record.make in EdmundsStore.getMakeNames()) || isLincolnForm(@record)
  getSuggestions: -> EdmundsStore.getModelNamesByMakeName(@record.make, @record.vehicle_type)
  findErrors: ->
    # this is temporary change https://swoopme.atlassian.net/browse/ENG-11012
    if !UserStore.isPartner() and @record.make == 'Porsche' and @record.model in ['918 Spyder', 'Carrera GT']
      return <>To dispatch this model, <br/> please call (800) 767-7243</>
    super(arguments...)

VehicleYearField: class VehicleYearField extends Fields.AutosuggestField
  _name: "year"
  _label: "Year"
  _example: "2018"
  findErrors: ->
    if @record.year? and @record.year.length > 0 and @record.year.length != 4
      return "Invalid year"
    super(arguments...)
  getClassName: -> super(arguments...) + (if @record.loadedYearFromVIN then "field-populated-by-another-field-highlight" else "")
  onChange: (value) ->
    @setValue((new Fields.YearField).onChange(target:value:value)) #Hack to use year fields listener
  isRequired: -> (UserStore.isFleet() && !UserStore.isTrekker()) || isLincolnForm(@record)
  getSuggestions: () ->
    return map(EdmundsStore.getYearsByName(@record?.make,@record?.model), (year) ->
      ""+year
      )

InvoiceFieldCategoryIdField: class InvoiceFieldCategoryIdField extends Fields.SelectField
  _name: "invoice_vehicle_category_id"
  _label: "Class Type"
  isRequired: -> FeatureStore.isFeatureEnabled(Consts.FEATURE_CLASS_TYPE_REQUIRED) and VehicleCategoryStore.getAllSorted(@props.componentId)?.length > 0
  isShowing: -> UserStore.isPartner() || FeatureStore.isFeatureEnabled(Consts.FEATURE_FLEET_CLASS_TYPE)
  getOptions: ->
    options = VehicleCategoryStore.getAllSorted(@props.componentId)

    if @record?.invoice_vehicle_category_id?
      idAsNumber = Number(@record.invoice_vehicle_category_id)
      if idAsNumber and map(options, (option) -> option.id).indexOf(idAsNumber) == -1
        options.unshift(VehicleCategoryStore.getById(@record.invoice_vehicle_category_id, true, @props.componentId))

    @optionMapper(options, clone(super(arguments...)))

VehicleColorField: class VehicleColorField extends Fields.AutosuggestField
  _name: "color"
  _label: "Color"
  _example: "Black"
  _suggestions: ["White", "Silver", "Black", "Grey", "Blue", "Red", "Brown", "Green", "Yellow", "Orange", "Purple"]
  isRequired: -> UserStore.isFleet() && !UserStore.isTrekker() && !UserStore.isMTS()
  onChange: (value) -> @setValue((new Fields.CapitalizedField).onChange(target:value:value)) #Hack to use year fields listener

VehicleSerialField: class VehicleSerialField extends Fields.Field
  _name: "serial_number"
  _label: "Serial Number"
  _placeholder: "Optional"
  isShowing: -> FeatureStore.isFeatureEnabled(Consts.FEATURES_SERIAL)

LicenseField: class LicenseField extends Fields.UppercaseField
  _name: "license"
  _label: "License"
  getClassName: -> super(arguments...) + " fs-exclude"
  mixin(@, MaxLengthMixin(=>
    if FeatureStore.isFeatureEnabled(Consts.FEATURE_DISABLE_LICENSE_RESTRICTIONS)
      return null
    return 7
  ))
  isShowing: -> !(UserStore.isPartner() && @record.status == Consts.AUTO_ASSIGNING)

VinField: class VinField extends Fields.UppercaseField
  _name: "vin"
  _label: "VIN"
  _example: "1HGCM82633A004352"
  getClassName: -> super(arguments...) + " fs-exclude"
  getPlaceholder: ->
    if FeatureStore.isFeatureEnabled(Consts.FEATURES_AUTOFILL_VIN)
      return Consts.MESSAGE_AUTOFILL_VIN
  mixin(@, MaxLengthMixin(17))
  isRequired: -> UserStore.isTesla()
  getTooltipText: ->
    if @_vinLookupSuccess
      return ''
    else if @_vinLookupError
      return 'Vehicle details unable to be pulled from VIN'
    else if @_invalidVin
      return 'Invalid VIN'
    else if @_loadingFromVIN
      return ''
  getInfoIconClassName: ->
    if @_vinLookupSuccess
      return super(arguments...) + ' vin-field-icon-success'
    else if @_vinLookupError || @_invalidVin
      return super(arguments...) + 'vin-field-icon-failure'
    else if @_loadingFromVIN
      return super(arguments...) + 'vin-field-icon-spinner'

  customFindErrors: ->
    if not @_vinLookupError and not @_invalidVin
      if UserStore.isTesla() and not Utils.validateVin(@getValue())
        return "Not a valid vin"

      if @isRequired() and @getValue()?.length > 0 and @getValue()?.length != 17
        return "Invalid Vin"
    super(arguments...)

  alwaysBeforeSend: (diff) ->
    delete diff['loadedMakeFromVIN']
    delete diff['loadedModelFromVIN']
    delete diff['loadedYearFromVIN']

  autofillVin: (response) ->
    vehicleInfo = response.Results[0]
    @_vinLookupSuccess = true
    @_loadingFromVIN = false

    make = @record.make
    if !@record.make and vehicleInfo.Make?
      #sets make to same casing as it is in the DB
      make = Utils.getCorrectCasing(EdmundsStore.getMakeNames(), vehicleInfo.Make)
      @setValue(make, true, "make")
      @setValue(true, true, "loadedMakeFromVIN")
    if !@record.model and vehicleInfo.Model?
      model = Utils.getCorrectCasing(EdmundsStore.getModelNamesByMakeName(make), vehicleInfo.Model)
      @setValue(model, true, "model")
      @setValue(true, true, "loadedModelFromVIN")
    if !@record.year and vehicleInfo.ModelYear?
      @setValue(vehicleInfo.ModelYear, true, "year")
      @setValue(true, true, "loadedYearFromVIN")
    @changeCallback?()

  autofillError: (e) ->
    @_vinLookupError = true
    @_loadingFromVIN = false
    @changeCallback?()
    console.warn("Vin lookup failed")

  onChange: (e) ->
    super(e)
    @_loadingFromVIN = false
    @_vinLookupSuccess = false
    @_vinLookupError = false
    @_invalidVin = false
    vin = @getValue()
    if FeatureStore.isFeatureEnabled(Consts.FEATURES_AUTOFILL_VIN)
      if(Utils.validateVin(vin))
        @_loadingFromVIN = true
        @setValues({
          loadedMakeFromVIN: false
          loadedModelFromVIN: false
          loadedYearFromVIN: false
        })
        Api.vinLookup(vin, {success: @autofillVin.bind(@), error: @autofillError})
      else if (vin.length == 17)
        @_invalidVin = true
  isShowing: -> !(UserStore.isPartner() && @record.status == Consts.AUTO_ASSIGNING)

TireSizeField: class TireSizeField extends Fields.SelectField
  _name: "tire_type_id"
  _label: "Tire Size"
  isShowing: -> UserStore.isTesla()
  isRequired: => UserStore.isTesla() and @record?.service == Consts.LOANERWHEEL
  getOptions: =>
    options = clone(super(arguments...))
    tire_types = TireTypeStore.getAll(false, @props.componentId)
    tire_types = map(tire_types, (type) =>
      {
        id: type.id
        name: TireTypeStore.getTireTypeName(type?.id, @props.componentId)
      }
    )
    for id,type of sortBy(tire_types, "name")
      options.push({text: type.name, value: type.id})
    return options

OdometerField: class OdometerField extends Fields.FloatField
  _name: "odometer"
  _label: "Odometer"
  _example: "1337"
  isRequired: -> UserStore.isTesla() || isLincolnForm(@record)


SendSmsToPickupField: class SendSmsToPickupField extends Fields.RadioField
  _name: "send_sms_to_pickup"
  _label: "Send text for jobs to"
  _options: [{text: "Pickup Contact", value: true},{text: "Customer", value: false}]
  prefill: -> @setIfEmpty(false)

ReviewSmsField: class ReviewSmsField extends Fields.CheckboxField
  _name: "review_sms"
  _label: "Text review when job complete"
  isShowing: ->
    FeatureStore.isFeatureEnabled(Consts.FEATURES_SMS_REVIEWS) or FeatureStore.isFeatureEnabled(Consts.FEATURES_REVIEW_NPS_SURVEY)
  prefill: -> @setIfEmpty(true)

ReviewDelayField: class ReviewDelayField extends Fields.SelectField
  _name: "review_delay"
  _label: "Text review"
  isShowing: ->
    FeatureStore.isFeatureEnabled(Consts.FEATURES_SMS_REVIEWS) and OrigUserStore.isSwoop()
  _options: [
    {text: "when job complete", value: "complete"},
    {text: "after 3 hours", value: "three_hours"},
    {text: "7PM local time", value: "seven_pm"},
    {text: "9AM local time", value: "nine_am"},
    {text: "don't send", value: "never"}
  ]

PartnerSmsTrackDriverField: class PartnerSmsTrackDriverField extends Fields.CheckboxField
  _name: "partner_sms_track_driver"
  _label: "Text link to track driver when En Route"
  isShowing: ->  FeatureStore.isFeatureEnabled("SMS_Track Driver")
  prefill: -> @setIfEmpty(true)

TextEtaUpdatesField: class TextEtaUpdatesField extends Fields.CheckboxField
  _name: "text_eta_updates"
  _label: "Text ETA updates"
  isShowing: ->  FeatureStore.isFeatureEnabled("SMS_ETA Updates")
  prefill: -> @setIfEmpty(true)

GetLocationSmsButtonField: class GetLocationSmsButtonField extends Fields.ButtonField
  _name: "get_location_sms_button"
  getName: -> "get_location_sms"
  _label: "Request Location"
  onChange: ->
    super(arguments...)
    @render_props.onChange()
  isShowing: ->  FeatureStore.isFeatureEnabled("SMS_Request Location")

GetLocationSmsField: class GetLocationSmsField extends Fields.CheckboxField
  _name: "get_location_sms"
  _label: "Text link to request exact location"
  isShowing: ->  FeatureStore.isFeatureEnabled("SMS_Request Location")

AbstractLocationTypeField: class AbstractLocationTypeField extends Fields.SelectField
  #ABSTRACT CLASS
  isShowing: -> @props?.form_type != "Storage"
  onChange: (e) ->
    super(e)
    location = @record[@_locationClass]
    if location?.address? && !isEmpty(location.address) && !location.site_id
      { lat: lat, lng: lng } = location
      @props.form.fields[@_locationClass].setLatLng(lat, lng, false)

  getOptions: ->
    options = clone(super(arguments...))

    locationTypesCollection = clone(LocationTypeStore.getCompanyLocationTypes())

    # If the User is a Partner or Root and the job is from Fleet,
    #   retrieve a collection with the FleetJob specific location types if they differ from
    #   the ones already available for <Partner|Root> company.
    #
    # All the code inside this if block is to keep retrocompatibility with
    # the deprecated location_type and dropoff_location_type attributes
    if !UserStore.isFleet() && JobStore.isFleetJob(@record)
      pickup_location_type = null

      if @original.location_type?
        pickup_location_type = LocationTypeStore.findLocationTypeByName(@original.location_type)
      else if @original.service_location?.location_type_id?
        pickup_location_type = LocationTypeStore.getLocationTypeById(@original.service_location.location_type_id)

      if pickup_location_type? && ! locationTypesCollection[pickup_location_type.id]
        locationTypesCollection[pickup_location_type.id] = pickup_location_type

      if @original.dropoff_location_type?
        dropoff_location_type = LocationTypeStore.findLocationTypeByName(@original.dropoff_location_type)
      else if @original.drop_location?.location_type_id?
        dropoff_location_type = LocationTypeStore.getLocationTypeById(@original.drop_location.location_type_id)

      if dropoff_location_type? && ! locationTypesCollection[dropoff_location_type.id]
        locationTypesCollection[dropoff_location_type.id] = dropoff_location_type

    for id, location_type of sortBy(locationTypesCollection, 'name')
      options.push({value: location_type.id, text: location_type.name })

    return options

PickupLocationTypeIdField: class PickupLocationTypeIdField extends AbstractLocationTypeField
  _name: "service_location.location_type_id"
  _placeholder: "Pickup Location Type"
  _label: "Pickup Location Type"
  _locationClass: 'service_location'
  _example: 1
  isRequired: -> UserStore.isFleet()
  getValue: ->
    # the code inside this if/else block is to keep retrocompatibility with
    # the deprecated location_type attribute
    if @record?.service_location?.location_type_id?
      return @record.service_location.location_type_id
    else if @record?.location_type?
      return LocationTypeStore.findLocationTypeByName(@record.location_type)?.id

DropoffLocationTypeIdField: class DropoffLocationTypeIdField extends AbstractLocationTypeField
  _name: "drop_location.location_type_id"
  _placeholder: "Drop Off Location Type"
  _label: "Drop Off Location Type"
  _locationClass: 'drop_location'
  _example: 2
  isRequired: -> @record.service == Consts.ACCIDENT_TOW
  getValue: ->
    # the code inside this if/else block is to keep retrocompatibility with
    # the deprecated dropoff_location_type attribute
    if @record?.drop_location?.location_type_id?
      return @record?.drop_location?.location_type_id
    else if @record?.dropoff_location_type?
      return LocationTypeStore.findLocationTypeByName(@record.dropoff_location_type)?.id

LocationField: class LocationField extends Fields.GeoSuggestField
  _fixedOptionsLabel: () => if isFeatureEnabledForPartnerOrSwoopAsPartner(Consts.FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF, @record?.acts_as_company_id, @props.componentId) then 'RECOMMENDED LOCATIONS' else 'SITES & PLACES'

  formatInputValue: (currentInputValue) =>
    formattedInputValue = currentInputValue
    location = @getValue()

    if location?.site_id
      siteOrPlaceName = SiteStore.get(location.site_id, 'name', @props.componentId)
      siteOrPlaceLocation = SiteStore.get(location.site_id, 'location', @props.componentId)

      if !siteOrPlaceName # we try Place
        siteOrPlaceName = PoiStore.get(location.site_id, 'name', @props.componentId)
        siteOrPlaceLocation = PoiStore.get(location.site_id, 'location', @props.componentId)

    if siteOrPlaceName and siteOrPlaceLocation
      formattedInputValue = Utils.formatAddressWithSite(siteOrPlaceName, siteOrPlaceLocation)

    formattedInputValue

  sitesOrPlaceLoader: ->
    return [{
      sortKey: 1
      address: ''
      text:
        Loading
          display: "inline-block"
          message: 'Loading Recommended Locations'
          size: 32
    }]

  mapSitesOrPlaces: (siteOrPlaceId, store, markerColor, siteObj) ->
    siteOrPlaceName = store.get(siteOrPlaceId, 'name') || siteObj?.site?.name
    siteOrPlaceLocation = store.get(siteOrPlaceId, 'location') || siteObj?.site?.location
    distance = siteObj?.miles

    return {
      sortKey: Utils.formatAddressWithSite(siteOrPlaceName, siteOrPlaceLocation)
      address: siteOrPlaceLocation?.address
      text:
        div
          className: "site-option-container"
          div
            className: "marker-and-name"
            span null,
              ReactDOMFactories.i
                className: "fa fa-map-marker #{markerColor}"
            span
              style:
                fontWeight: 'bold'
                paddingRight: 3
              "  #{siteOrPlaceName}"
            span null,
              siteOrPlaceLocation?.address
          if typeof distance == 'number'
            distance = distance.toFixed(1)
            span
              className: "distance"
              style:
                color: '#ccc'
              "#{distance} miles away"
      value: siteOrPlaceId
      location: siteOrPlaceLocation
    }

  getFixedOptions: ->
    if UserStore.isSwoop()
      return

    sites = SiteStore.getFilteredSites(null, @props.componentId, 'location')

    sites = filter(sites, (site) =>
      site.location?.address?
    )

    sites = map(sites, (site) => @mapSitesOrPlaces(site.id, SiteStore, 'blue'))

    poiSites = PoiStore.loadAndGetAll(@props.componentId, 'location')
    poiSites = filter(poiSites, (poiSite) ->
      poiSite.location?.address?
    )

    poiSites = map poiSites, (poiSite) => @mapSitesOrPlaces(poiSite.id, PoiStore, 'yellow')


    if poiSites? || sites?
      return sortBy(unionBy(sites, poiSites, 'value'), (site) => @proximitySort(site))

  # sort the list of sites/places based on some locations related to the job, following this order:
  #
  # pickup location (if any already selected)
  # fleet site location (if any already selected and if it has a location)
  # company location
  proximitySort: (site) =>
    originLatLng =
      lat: site.location.lat
      lng: site.location.lng

    comparisionLatLng  = null

    if @record?.service_location?.lat? and @record?.service_location?.lng?
      comparisionLatLng =
        lat: @record.service_location.lat
        lng: @record.service_location.lng
    else if @record?.fleet_site_id?
      fleetSiteLocation = SiteStore.get(@record.fleet_site_id, 'location', @props.componentId)

      if fleetSiteLocation?
        comparisionLatLng =
          lat: fleetSiteLocation.lat
          lng: fleetSiteLocation.lng

    if comparisionLatLng == null
      company = UserStore.getUser().company
      comparisionLatLng =
        lat: company.location.lat
        lng: company.location.lng

    return Utils.calculateDistanceBetween(originLatLng, comparisionLatLng)

  isSuggestSet: (suggest) ->
    suggest?.location?.lat && suggest?.location?.lng

  onChange: (e, suggest, showMap, fixedOption) ->
    if !suggest?
      if @getValue()?.location_type_id?
        @setValue(location_type_id: @getValue().location_type_id)
      else
        @setValue(null)
      return

    address = @buildAddressValue(suggest)
    address.site_id = null

    if fixedOption
      siteId = fixedOption.value
      # check if it's a site before set record.fleet_site_id
      # (if it's not a site, its a place, therefore should not be set as record.fleet_site_id
      if Utils.isSiteAndNotPlace(SiteStore, siteId)
        @record.fleet_site_id = siteId if !@record.fleet_site_id

      address.site_id = siteId
    @setValue(address)
    if isFeatureEnabledForPartnerOrSwoopAsPartner(Consts.FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF, @record?.acts_as_company_id, @props.componentId)
      @props.form.renderInput('drop_location')

ServiceLocationField: class ServiceLocationField extends LocationField
  _name: "service_location"
  _placeholder: "Service Location"
  _required: true
  _label: "Pickup Location"

  isShowing: ->
    !@props?.form_type != "Storage" and @record?.service != "Storage"

  onChange: (e, suggest, showMap, fixedOption) ->
    if fixedOption and fixedOption.location?.location_type_id?
      @setValue { location_type_id: LocationTypeStore.getLocationTypeById(fixedOption.location.location_type_id)?.id }

    # if a service location is appropriately set, set dropoff_location with the closest site
    if @isSuggestSet(suggest) && !isFeatureEnabledForPartnerOrSwoopAsPartner(Consts.FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF, @record?.acts_as_company_id, @props.componentId)
      @autoSetDropLocation(suggest)

    super(arguments...)

  getFixedOptions: ->
    if isFeatureEnabledForPartnerOrSwoopAsPartner(Consts.FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF, @record?.acts_as_company_id, @props.componentId)
      return []

    return super(arguments...)

  autoSetDropLocation: (fixedOption) ->
    if JobStore.canAutoSetDropLocation(@record.service)
      originLatLng =
        lat: fixedOption.location.lat
        lng: fixedOption.location.lng

      closestSiteLocation = Utils.getClosestSiteTo(SiteStore, originLatLng, @props.componentId, 'location')

      if closestSiteLocation?
        @record.drop_location = closestSiteLocation

  isEnabled: -> @props?.form_type != "Storage" and @record?.service != "Storage"

DropLocationField: class DropLocationField extends LocationField
  _name: "drop_location"
  _placeholder: "Drop Off Location"
  _label: "Drop Off Location"

  getFeatureFlags: () ->
    recommendedDropOff: isFeatureEnabledForPartnerOrSwoopAsPartner(Consts.FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF, @record?.acts_as_company_id, @props.componentId)
  isEnabled: () ->
    loc = get(@record, @getName())
    if loc?.site_id? and @record?.will_store
      return false
    return true
  isRequired: (job) ->
    !isNaN(parseInt(@record?.drop_location?.location_type_id)) ||
    (@record.service in [Consts.IMPOUND, Consts.ACCIDENT_TOW]) ||
    (UserStore.isFleet() && (@record.service in [Consts.DECKING, Consts.SECONDARYTOW, Consts.TOW, Consts.TOWIFJUMPFAILS, Consts.UNDECKING]))
  aboutToSend: (newrecord, name) ->
    if not newrecord.drop_location?
      newrecord.drop_location_id = null

  getFixedOptions: ->
    if isFeatureEnabledForPartnerOrSwoopAsPartner(Consts.FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF, @record?.acts_as_company_id, @props.componentId)
      options = []
      usedSelectedSite = null
      drop_location_information = @props?.form?.drop_location_information

      if drop_location_information?.loading
        return @sitesOrPlaceLoader()

      if @record?.fleet_site_id
        selectedSite = SiteStore.getById(@record.fleet_site_id, false, @props.componentId)
        if selectedSite?.location?.address?
          usedSelectedSite = @mapSitesOrPlaces(selectedSite.id, SiteStore, 'blue')
          options.push(usedSelectedSite)

      if @props?.form && @record
        if drop_location_information
          if (drop_location_information.service != @record.service ||
          @record.service_location?.place_id != drop_location_information.serviceLocation?.place_id)
            drop_location_information.service = @record.service
            drop_location_information.serviceLocation = @record.service_location
            requestDropLocationOptions(drop_location_information, @record.id, @props.form, @)
        else
          drop_location_information = {
            serviceLocation: @record.service_location,
            service: @record.service,
            dropOffSites: [],
          }
          @props.form.drop_location_information = drop_location_information
          requestDropLocationOptions(@props.form.drop_location_information, @record.id, @props.form, @)

        options = options.concat(map(drop_location_information.dropOffSites, (siteObj) => @mapSitesOrPlaces(siteObj.site.id, SiteStore, 'blue', siteObj)))

        # remove duplicate site if selected site from form is same as one of the recommended sites
        if usedSelectedSite && drop_location_information.dropOffSites
          recommendedOptionIds = {}
          drop_location_information.dropOffSites.forEach((option) => recommendedOptionIds[option.site?.id] = true)
          if recommendedOptionIds[options[0]?.value]
            options = options.slice(1)
      @props.form?.drop_location_options = options
      return options
    else
      return super(arguments...)

  onChange: (e, suggest, showMap, fixedOption) ->
    if fixedOption
      if fixedOption.location?.location_type_id?
        @record.drop_location = { location_type_id: LocationTypeStore.getLocationTypeById(fixedOption.location.location_type_id)?.id }
    else if suggest?.location?.lat && isFeatureEnabledForPartnerOrSwoopAsPartner(Consts.FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF, @record?.acts_as_company_id, @props.componentId)
      fixedOptions = @getFixedOptions()
      chosenFixedOption = find(fixedOptions, (option) => return Utils.isSameToDecimal(option.location?.lat, suggest.location.lat, 7) && Utils.isSameToDecimal(option.location?.lng, suggest.location.lng, 7))
      if chosenFixedOption?.location
        super(e, suggest, showMap, chosenFixedOption)
        return
    super(arguments...)

PickupContactNameField: class PickupContactNameField extends Fields.CapitalizedField
  _name: "pickup_contact.full_name"
  _placeholder: "Pickup Name"
  _label: "Pickup Name"
  _example: "Quinn Pickup"
  _autocomplete: "off"
  getClassName: -> super(arguments...) + " fs-exclude"
  mixin(@, InheritMixin("pickup_contact"))
  isRequired: -> UserStore.isTesla()
  isShowing: -> !(UserStore.isPartner() && @record.status == Consts.AUTO_ASSIGNING)

PickupContactPhoneField: class PickupContactPhoneField extends Fields.PhoneField
  _name: "pickup_contact.phone"
  _example: "+12315555555"
  _placeholder: "Pickup Number"
  _label: "Pickup Number"
  _autocomplete: "off"
  allowDisableMasking: true
  getClassName: -> super(arguments...) + " fs-exclude"
  mixin(@, InheritMixin("pickup_contact"))
  isRequired: -> UserStore.isTesla() || (@record?.send_sms_to_pickup and isSendingText(@record))
  isShowing: -> !(UserStore.isPartner() && @record.status == Consts.AUTO_ASSIGNING)

DropoffContactNameField: class DropoffContactNameField extends Fields.CapitalizedField
  _name: "dropoff_contact.full_name"
  _placeholder: "Drop Off Name"
  _label: "Drop Off Name"
  getClassName: -> super(arguments...) + " fs-exclude"
  mixin(@, InheritMixin("dropoff_contact"))
  isShowing: -> !(UserStore.isPartner() && @record.status == Consts.AUTO_ASSIGNING)

DropOffContactPhoneField: class DropOffContactPhoneField extends Fields.PhoneField
  _name: "dropoff_contact.phone"
  _placeholder: "Drop Off Number"
  _label: "Drop Off Number"
  allowDisableMasking: true
  getClassName: -> super(arguments...) + " fs-exclude"
  mixin(@, InheritMixin("dropoff_contact"))
  isShowing: -> !(UserStore.isPartner() && @record.status == Consts.AUTO_ASSIGNING)

DispatcherIdField: class DispatcherIdField extends Fields.BackendSuggestField
  _name: "dispatcher_id"
  _placeholder: 'Type or Select'
  getName: ->
    if OrigUserStore.isSwoop() then "root_dispatcher_id" else (if OrigUserStore.isFleet() then "fleet_dispatcher_id" else "partner_dispatcher_id")
  prefill: ->
    if !OrigUserStore.isSwoop()
      @setIfEmpty(""+OrigUserStore.getUser()?.id)

  getInputProps: ->
    extend({}, super(arguments...), {
      minCharacters: 0
    })

  getValue: =>
    if @record[@getName()]?
      OrigUsersStore.get(@record[@getName()], "full_name", @props.componentId) || "..loading"


  getSuggestions: (options, callback) =>
    dispatchers = UsersStore.getSortedDispatcherIds(@props.componentId)
    objs = map(dispatchers, (id) => {id: id, label: UsersStore.get(id, "full_name", @props.componentId) || "..loading"})

    if options.input.length > 0 && options.input != @getValue()
      objs = filter(objs, (obj) -> obj.label.toLowerCase().indexOf(options.input.toLowerCase()) >= 0)

    objs.unshift({id: null, label: "Unassigned"})
    objs.unshift({id: UserStore.getUser().id, label: "Assign to me"})
    callback(objs)



SiteIdField: class SiteIdField extends Fields.SelectField
  _name: "site_id"

  isShowing: ->
    UserStore.isPartner() && SiteStore.getDispatchSites(@props.componentId, 'name').length > 1

  prefill: ->
    if not @record?.site_id? and @isShowing()
      lastSiteIdSetting = UserSettingStore.getUserSettingByKey(Consts.USER_SETTING.LAST_SITE_ID_ON_JOB_FORM, @props.componentId)

      if lastSiteIdSetting
        @setValue(parseInt(lastSiteIdSetting))
      else
        @setValue(SiteStore.getDispatchSites(@props.componentId, 'name')[0].id)

  onChange: (id, value) ->
    super(arguments...)

    UserSettingStore.setUserSettings(Consts.USER_SETTING.LAST_SITE_ID_ON_JOB_FORM, parseInt(@record.site_id))

  getOptions: ->
    siteOptions = map(SiteStore.getDispatchSites(@props.componentId, 'name'), (site) =>
      { id: site.id, name: site.name }
    )

    @optionMapper(siteOptions, clone(super(arguments...)))

FleetSiteIdField: class FleetSiteIdField extends Fields.StoreSelectorField
  _name: 'fleet_site_id'
  _label: "Site"
  _placeholder: 'Select Site'
  isShowing: ->
    # case 1: When Swoop user, we check if the selected FleetManaged company has FEATURES_SHOW_CLIENT_SITES_TO_SWOOP and FEATURES_FLEET_SITES enabled.
    if @record?.acts_as_company_id
      jobFleetCompanyFeatures = CompanyStore.get(@record.acts_as_company_id, 'features', @props.componentId)

      showClientSitesToSwoopEnabled = FeatureStore.isFeatureEnabled(
        Consts.FEATURES_SHOW_CLIENT_SITES_TO_SWOOP, @props.componentId, { features: jobFleetCompanyFeatures }
      )

      clientSitesEnabled = FeatureStore.isFeatureEnabled(
        Consts.FEATURES_FLEET_SITES, @props.componentId, { features: jobFleetCompanyFeatures }
      )

      showClientSitesToSwoopEnabled && clientSitesEnabled

    # case 2: When Fleet company, check if FEATURES_FLEET_SITES is enabled
    else
      return UserStore.isFleet() && FeatureStore.isFeatureEnabled(Consts.FEATURES_FLEET_SITES)

  isRequired: ->
    FeatureStore.isFeatureEnabled(Consts.FEATURES_JOB_FORM_SITE_REQUIRED) || UserStore.getMySiteIds()?.length > 0

  onChange: (id, value) ->
    @setValue(id)
    if isFeatureEnabledForPartnerOrSwoopAsPartner(Consts.FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF, @record?.acts_as_company_id, @props.componentId)
      @props?.form?.renderInput('drop_location')
  getValue: ->
    SiteStore.get(@record?.fleet_site_id, 'name', @props.componentId)

  alwaysBeforeSend: (newRecord) ->
    if UserStore.isTesla() and DepartmentStore.get(@record?.department_id, "name", @props.componentId) not in ["Service Center", "Delivery", "Sales"]
      newRecord["fleet_site_id"] = null

  getSites: ->
    if UserStore.isSwoop()
      return
    return values(SiteStore.getUserSitesOrAll(@props.componentId, 'name'))

  prefill: ->
    sites = @getSites()

    if not @record?.fleet_site_id? and @isShowing() and sites.length == 1
      @setValue(sites[0].id)

  _populateFunc: =>
    options = {}

    forEach(@getSites(), (site) =>
      options[site.id] = { id: site.id, name: site.name }
    )

    options

ReleaseToAccountId: class ReleaseToAccountId extends Fields.BackendSuggestField
  _name: "storage.released_to_account_id"
  _label: "Release To"
  _placeholder: "Type or select"

  getSuggestions: (options, callback) ->
    if !UserStore.isPartner() && !UserStore.isSwoop()
      return

    Api.autocomplete_accounts(options.input, 10, UserStore.getEndpoint(), {
      success: (data) =>
        # force lazy loading the account if needed
        # no need to pass the componentId since it's a callback function
        forEach(data, (account) -> AccountStore.get(account.id, 'id'))

        forEach(["Lienholder", "Owner", "Customer"], (fixedSuggestion) ->
          data.unshift(
            id: fixedSuggestion
            name: fixedSuggestion
          )
        )

        locationMap = map(data, (account) ->
            id: account.id
            label: account.name
          )
        callback(locationMap)
      error: (data, textStatus, errorThrown) =>
        console.warn("Autocomplete failed")
    })

  findErrors: ->
    if @record?.storage?.replace_invoice_info and not @record?.storage?.released_to_account_id? and not @record?.storage?.released_to_user?
      return "Required When Replacing Invoice Info"
    super(arguments...)

  onChange: (e) ->
    if e.target?.value
      id = e.target.value
      if id in ["Lienholder", "Owner", "Customer"]
        @setValue(id, true, "storage.released_to_user.storage_relationship")
        @setValue(null, false, "storage.released_to_account_id")
        showReleaseTouser = true
      else
        @setValue(null, true, "storage.released_to_user.storage_relationship")
        @setValue(id, false, "storage.released_to_account_id")
    else
      @setValue(null)

  getValue: ->
    if @record?.storage?.released_to_user?.storage_relationship?
      return @record?.storage.released_to_user.storage_relationship

    if @record?.storage?.released_to_account_id
      accountName = AccountStore.get(@record.storage.released_to_account_id, 'name', @props.componentId)

    if not accountName
      return ""
    return accountName

RelaseToUserName: class RelaseToUserName extends Fields.CapitalizedField
  _name: "storage.released_to_user.full_name"
  _label: "Release Name"
  getClassName: -> super(arguments...) + " fs-exclude"
  isShowing: -> !@record?.storage?.released_to_account_id?

RelaseToAddress: class RelaseToAddress extends Fields.GeoSuggestField
  _name: "storage.released_to_user.location"
  _label: "Release Address"
  getClassName: -> super(arguments...) + " fs-exclude"
  findErrors: ->
  isShowing: -> !@record?.storage?.released_to_account_id?

ReleaseToUserPhone: class ReleaseToUserPhone extends Fields.PhoneField
  _name: "storage.released_to_user.phone"
  _label: "Release Phone"
  getClassName: -> super(arguments...) + " fs-exclude"
  isShowing: -> !@record?.storage?.released_to_account_id?

ReleaseToEmail: class ReleaseToEmail extends Fields.EmailField
  _name: "storage.released_to_email"
  _label: "Release Email"
  getClassName: -> super(arguments...) + " fs-exclude"
  isShowing: -> !@record?.storage?.released_to_account_id?

ReleaseToLicenseState: class ReleaseToLicenseState extends Fields.SelectField
  _name: "storage.released_to_user.license_state"
  _label: "License State"
  getOptions: -> @optionMapper(Consts.STATE_ABREVS, clone(super(arguments...)))
  isShowing: -> !@record?.storage?.released_to_account_id?

ReleaseToLicenseId: class ReleaseToLicenseId extends Fields.Field
  _name: "storage.released_to_user.license_id"
  _label: "Driver License"
  getClassName: -> super(arguments...) + " fs-exclude"
  isShowing: -> !@record?.storage?.released_to_account_id?

ReplaceInvoiceInfo: class ReplaceInvoiceInfo extends Fields.CheckboxField
  _name: "storage.replace_invoice_info"
  _label: "Replace Customer Info on Invoice"

InvoiceTotalField: class InvoiceTotalField extends Fields.SpanField
  _name: "invoices.0.total_amount"
  _label: "Invoice Total"
  getClassName: -> super(arguments...) + " fs-exclude"
  getValue: ->
    if @record?.invoices?[0]?.id
      invoice = InvoiceStore.getInvoiceById(@record.invoices[0].id)
      if invoice
        val = invoice.total_amount
      else
        val = @record?.invoices?[0]?.total_amount
      if val?
        return '$'+val

    return "Loading..."

TempReleasedField: class TempReleasedField extends Fields.DateTimeField
    _future: false
    _name: "release_date"
    _label: "Released"
    _showing: true
    _editable: true
    _required: false
    _className: 'inside-history-form-section'
    prefill: ->
      @original_time = moment().toISOString()
      @setValue(@original_time)
    alwaysBeforeSend: (newRecord) ->
      if moment(newRecord.release_date).startOf('minute').toISOString() == moment(@original_time).startOf('minute').toISOString()
        delete newRecord.release_date

QuestionFactory: (question) ->
  class QuestionField extends Fields.SelectField
    _name: "question_"+question.question_id
    _label: question.question
    _example: question.answers[0].id
    _forceInfo: true
    isRequired: -> !UserStore.isTesla()
    alwaysBeforeSend: (newrecord) ->
      #Removes a question from fleet when it is no longer visible
      if UserStore.isFleet() && !@isShowing() && newrecord?.question_results
        for q, i in newrecord.question_results
          if ""+q.question_id == ""+question.question_id
            newrecord.question_results.splice(i, 1)
            break

      #Makes sure to not pass up question results if nothing has changed
      if newrecord?.question_results and @original?.question_results and newrecord.question_results.length == @original?.question_results.length
        newComp = {}
        oldComp = {}
        for question in newrecord.question_results
          newComp[question.question_id] = ""+ question.answer_id + question.answer_info
        for question in @original?.question_results
          oldComp[question.question_id] = ""+ question.answer_id + question.answer_info
        for key, val of newComp
          if newComp[key] != oldComp[key]
            return
        delete newrecord.question_results
    isShowing: ->
      if @render_props?.forceShow
        return true
      questions_arr = map(QuestionStore.getQuestionsForServiceId(@record?.service_code_id), (q) -> ""+q.question_id)
      FeatureStore.isFeatureEnabled("Questions") and ""+question.question_id in questions_arr
    getExample: -> question.answers[0].id
    setValue: (answer_id) ->
      results = @record?.question_results
      skip = false

      if !results?
        results = []
      for result in results
        if ""+result.question_id == ""+question.question_id
          result.answer_id = answer_id
          skip = true
          break
      if !skip
        results.push({
          question_id: parseInt(question.question_id)
          answer_id: parseInt(answer_id)
        })
      super(results, false, "question_results")
    getValue: () ->
      if @record?.question_results?
        for result in @record?.question_results
          if ""+result.question_id == ""+question.question_id
            return result.answer_id
      return ''

    getInfo: ->
      if question.name == Consts.QUESTION_KEY_LOCKED_IN
        for answer in question.answers
          if parseInt(answer.id) == parseInt(@getValue()) and answer.answer == Consts.ANSWERS_YES
            return Consts.MESSAGE_LOCKED_IN
      else if question.question == Consts.QUESTION_CUSTOMER_SAFE
        for answer in question.answers
          if parseInt(answer.id) == parseInt(@getValue()) and answer.answer == Consts.ANSWERS_NO
            return "Advise customer to hang up and dial 911."

    getOptions: () -> @optionMapper(question.answers, clone(super(arguments...)), "answer")

ClearanceQuestionFactory: (question) ->
  class ClearanceField extends Fields.Field
    _name: "question_extras_"+question.question_id
    _required: true
    _label: null
    _example: "6' 10''"
    _placeholder: "Height"
    getValue: ->
      if @record?.question_results?
        for result in @record?.question_results
          if ""+result.question_id == ""+question.question_id
            return result.answer_info
      return ''
    setValue: (extras) ->
      results = @record?.question_results
      skip = false

      if !results?
        results = []
      for result in results
        if ""+result.question_id == ""+question.question_id
          result.answer_info = extras
          skip = true
          break
      if !skip
        results.push({
          question_id: parseInt(question.question_id)
          answer_info: extras
        })

      Utils.setValue(@record, "question_results", results)
      @changeCallback?()

    isShowing: ->
      FeatureStore.isFeatureEnabled(Consts.FEATURES_QUESTIONS)

    getAnswer: () ->
      if @record?.question_results?
        for result in @record?.question_results
          if ""+result.question_id == ""+question.question_id
            return result.answer_id
      return ''

    findErrors: ->
      #Require
      answer_id = @getAnswer()
      for answer in question.answers
        if answer.id == parseInt(answer_id)
          if answer.answer == "Yes" and  (!@getValue()? || @getValue().length == 0)
            return "Height required when low clearance."

HistoryFactory: (history, history_key) ->
  class HistoryField extends Fields.DateTimeField
    _future: false
    _name: "history."+history_key+".adjusted_dttm"
    _label: if history.type == "ScheduledFor" then 'Scheduled For' else history.name
    _className: "inside-history-form-section"

    getValue: () =>
      value = super(arguments...)
      if !value?
        value = @record?.history[history_key]?.last_set

      if !@isEditable()
        return Utils.formatDateTime(value, true)
      return value

    aboutToSend: (job) ->
      for name, status of job.history
        if status?
          delete job.history[name].id

    alwaysBeforeSend: (newRecord) ->
      toDelete = []
      for name, status of newRecord.history
        if status?
          compare_date = @original?.history[name].last_set
          if @original?.history[name].adjusted_dttm?
            compare_date = @original?.history[name].adjusted_dttm
          if moment(status.adjusted_dttm).startOf('minute').toISOString() == moment(compare_date).startOf('minute').toISOString()
            toDelete.push(name)
        else
          toDelete.push(name)

      for del in toDelete
        delete newRecord.history[del]

      if isEmpty(newRecord.history)
        delete newRecord.history

    isShowing: ->
      status = history.name
      if not status? || status.name == "" || status.charAt(0) == "*"
        return false
      if status in [Consts.AUTO_ASSIGNING, Consts.SCHEDULED_FOR]
        return false
      if OrigUserStore.isSwoop() || OrigUserStore.isFleetInHouse()
        return true
      if OrigUserStore.isPartner()
        if Utils.isAssignedFromFleet(@original)
          if status in [Consts.REASSIGN]
            return false
        else
          if status in [Consts.UNASSIGNED, Consts.ASSIGNED, Consts.ACCEPTED, Consts.REJECTED, Consts.REASSIGN, Consts.REASSIGNED, Consts.ETAREJECTED]
            return false
      if OrigUserStore.isFleetManaged()
        if status.name in [Consts.UNASSIGNED, Consts.ASSIGNED, Consts.ACCEPTED, Consts.REJECTED, Consts.REASSIGN, Consts.REASSIGNED, Consts.ETAREJECTED, Consts.AUTO_ASSIGNING]
          return false
      return true

    isEditable: ->
      status = history
      if status.type == "ScheduledFor"
        return false
      if status.deleted_at?
        return false

      return true if OrigUserStore.isSwoop()

      if OrigUserStore.isFleet()
        if OrigUserStore.isFleetInHouse()
          return true
        if OrigUserStore.isFleetManaged()
          return false
         id = @original?.rescue_company?.id
         if id?
           if CompanyStore.get(id, 'live', @props.componentId)
             return false
      else if OrigUserStore.isPartner()
        if !Utils.isAssignedFromFleet(@original) and status.name == Consts.CREATED
          return true
        if status.name not in [Consts.DISPATCHED, Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION, Consts.COMPLETED, Consts.STORED, Consts.RELEASED]
          return false
      return true
