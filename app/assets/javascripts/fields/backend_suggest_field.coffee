import React from 'react'
Backendsuggest = React.createFactory(require('components/autosuggest/backend_suggest').default)
BackendSuggestField = require('fields/common/backend_suggest_field')
import { extend } from 'lodash'

class WebBackendSuggestField extends BackendSuggestField
  focused: false
  getElement: -> Backendsuggest
  #This is to prevent a new function from being created every getInputProps call

  forceError: ->
    !@focused && @hasSelectedSuggest?

  customFindErrors: ->
    #console.log("COMPONENT:", @component, !@getValue()?, @isRequired(), @component?.state?.userInput?.length > 0)

    if !@getValue()? && (@isRequired() || @component?.state?.userInput?.length > 0)
      return "Please select from dropdown"

  onBlur: =>
    if @focused
      @focused = false
      setTimeout(@changeCallback, 100)

  onFocus: =>
    @focused = true

  onSuggestSelect: (suggest) =>
    @hasSelectedSuggest = true
    if suggest?
      @onChange(target:value:suggest.id)
    else
      @onChange(target:value:null)

  getInputProps: ->
    extend({}, super(arguments...), {
      onFocus: @onFocus
      onBlur: @onBlur
      ref: (backendSuggest) =>
        @component = backendSuggest?.instanceRef
      onSuggestSelect: @onSuggestSelect
      initialValue: @getValue()
      onChange: ->
      getSuggestions: @getSuggestions
    })

module.exports = WebBackendSuggestField
