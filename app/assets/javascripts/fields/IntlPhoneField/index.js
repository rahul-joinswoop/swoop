import React from 'react'
import { extend, get, startsWith } from 'lodash'
import TextMask from 'react-text-mask'
import Select from 'componentLibrary/SimpleSelect'
import { convertToLatLng, getDistanceBetween } from 'lib/google-maps'
import Utils from 'utils'
import Field from 'fields/field'
import './style.scss'

const defaultCountry = {
  anchor: convertToLatLng({
    lat: 37.0902,
    lng: -95.7129,
  }),
  code: 'us',
  prefix: '+1',
  name: 'United States',
  // XXX-XXX-XXXX
  mask: [/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
}

const countries = [
  defaultCountry,
  {
    anchor: convertToLatLng({
      lat: 51.1657,
      lng: 10.4515,
    }),
    code: 'de',
    prefix: '+49',
    name: 'Germany',
    // 12 digits unformatted
    mask: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
  },
]

class IntlPhoneNumber {
  constructor(country, phone) {
    this.country = country
    this.phone = phone
  }
}

class USPhoneNumber {
  constructor(phone) {
    this.country = defaultCountry
    this.phone = phone
  }
}

const options = [{ label: '-Select-' }, ...countries.map((country) => {
  /* eslint-disable-next-line no-param-reassign */
  country.label = `${country.code.toUpperCase()} ${country.prefix}`
  return country
})]

function PhoneNumberInput({ value, onChange, ...inputProps }) {
  let mask, pipe
  let showSelect = false

  if (value instanceof IntlPhoneNumber) {
    showSelect = true

    mask = value.country?.mask ||
      // 12 digits unformatted default mask
      [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]

    pipe = (newValue) => {
      if (newValue !== value.phone) {
        /* eslint-disable-next-line no-param-reassign */
        value.phone = newValue
        onChange(value)
      }
    }
  } else if (!value) {
    mask = [/[+1-9]/]
    pipe = (newValue) => {
      if (newValue) {
        if (newValue === '+') {
          // just switch to international mode
          onChange(new IntlPhoneNumber(null, ''))
        } else {
          onChange(new USPhoneNumber(newValue))
        }
      }
    }
  } else {
    mask = value.country.mask
    pipe = (newValue) => {
      if (newValue !== value.phone) {
        /* eslint-disable-next-line no-param-reassign */
        value.phone = newValue
        // if new value is empty switch to choose mode
        onChange(newValue ? value : '')
      }
    }
  }

  return (
    <div className={showSelect ? 'PhoneNumberInput-container' : ''}>
      {showSelect && <Select
        autoFocus
        defaultValue={options[0]}
        onChange={(newCountry) => {
          /* eslint-disable-next-line no-param-reassign */
          value.country = newCountry.code ? newCountry : null
          onChange(value)
        }}
        options={options}
        renderSelected={(selected) => (
          selected.code ?
            <>
              <span className={`flag-icon flag-icon-${selected.code}`} />{selected.prefix}
            </> :
            selected.label
        )}
        value={value.country}
      />}
      <TextMask
        guide={false}
        mask={mask}
        pipe={(conformedValue) => {
          const newValue = conformedValue.replace(/[^+0-9]/g, '')
          pipe(newValue)
          return conformedValue
        }}
        value={value?.phone}
        {...inputProps}
      />
    </div>
  )
}

// dirty hack to determine where is company located
// use kinda middle point of country as anchor
// find closest country for company location
function getDefaultCountry(companyLocation) {
  if (companyLocation?.lat && companyLocation?.lng) {
    return countries.reduce((closest, country) => {
      const location = convertToLatLng(companyLocation)
      if (getDistanceBetween(country.anchor, location) < getDistanceBetween(closest.anchor, location)) {
        return country
      }
      return closest
    })
  } else {
    return defaultCountry
  }
}

class IntlPhoneField extends Field {
  autoFocus = true

  name = 'phone'

  _placeholder = 'xxx-xxx-xxxx'

  _phoneNumber = null

  getPhoneNumber() {
    if (!this._phoneNumber) {
      const value = this.getValue()
      if (value) {
        const country = countries.find(({ prefix }) => startsWith(value, prefix))
        const localPhone = value.substring(country.prefix.length)
        this._phoneNumber = new IntlPhoneNumber(country, localPhone)
      } else {
        // getCompanyLocation must be implemented in child class
        const companyLocation = this.getCompanyLocation()
        const country = companyLocation ?
          getDefaultCountry(companyLocation) :
          null
        this._phoneNumber = new IntlPhoneNumber(country, '')
      }
    }

    return this._phoneNumber
  }

  findErrors(...props) {
    if (this._phoneNumber) {
      if (!this._phoneNumber.country) {
        return 'Country is required'
      }
      if (!this._phoneNumber.phone) {
        return 'Invalid Phone'
      }
    }

    return super.findErrors(...props)
  }

  alwaysBeforeSend = (newRecord) => {
    const obj = get(newRecord, this.getName())

    if (obj && obj.charAt(0) !== '+') {
      return Utils.setValue(
        newRecord,
        this.getName(),
        `+1${obj.split('-').join('')}`,
      )
    }
  }

  getElement() {
    return React.createFactory(PhoneNumberInput)
  }

  getInputProps() {
    return extend({}, super.getInputProps(), {
      autoFocus: this.autofocus,
      value: this.getPhoneNumber(),
    })
  }

  onChange(value) {
    this._phoneNumber = value
    if (value) {
      this.setValue(`${value.country?.prefix || ''}${value.phone}`)
    } else {
      this.setValue('')
    }
  }
}

export default IntlPhoneField
