import React from 'react'
MultiSelectField = require 'fields/common/multiselect_field'
MultiFilter = React.createFactory(require('components/filters/multifilter'))
import { difference, extend, isEmpty, union, without } from 'lodash'

class WebFilterOutMultiFilterField extends MultiSelectField
  getElement: -> MultiFilter

  onChange: (id) =>
    if @getValue().indexOf(id) > -1
      super(without(@getValue(), id))
    else
      super(union(@getValue(), [id]))

  allChecked: =>
    isEmpty(@getValue())

  allText: -> @_allText || 'All'

  filterText: -> @_filterText || 'Select'

  noneSelectedText: -> @_noneSelectedText || 'none selected'

  getInputProps: ->
    extend({}, super(arguments...), {
      getAll: => @getSuggestions()
      allText: @allText()
      filterText: @filterText()
      noneSelectedText: @noneSelectedText()
      name: @getName()
      getObjectName: (id) => @getObjectName(id)
      getObjectLongName: (id) => @getObjectLongName(id)
      show: -> true
      getCheckedValues: => difference(@getSuggestions(), @getValue())
      onChange: (id) => @onChange(id)
      className: 'filterout_multiselect'
      isLoading: => @isLoading()
      allChecked: =>
        @allChecked()
      addAll: =>
        if @allChecked()
          @setValue(@getSuggestions())
        else
          @setValue([])
    })

  alwaysBeforeSend: (newRecord) ->
    if !@record[@getName()]? or (@record[@getName()]?.length == 0 and @original[@getName()]?.length > 0)
      newRecord[@getName()] = []

module.exports = WebFilterOutMultiFilterField
