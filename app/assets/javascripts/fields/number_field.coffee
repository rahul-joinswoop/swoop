import NumberField from 'fields/common/number_field'
class WebNumberField extends NumberField
  _inputType: "number"
  getStep: -> @_step
  getInputProps: ->
    props = super(arguments...)
    if @getStep()?
      props.step = @getStep()
    return props

module.exports = WebNumberField
