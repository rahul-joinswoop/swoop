UppercaseField = require('fields/common/uppercase_field')
class WebUppercaseField extends UppercaseField
  customFindErrors: (ignoreRequired) ->
    super(ignoreRequired)

module.exports = WebUppercaseField
