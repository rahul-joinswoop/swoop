import React from 'react'
Autosuggest = React.createFactory(require('components/autosuggest/autosuggest'))
AutosuggestField = require('fields/common/autosuggest_field')
import { extend } from 'lodash'

class WebbAutosuggestField extends AutosuggestField
  getErrorStyle: -> extend({}, super(arguments...), {right: 23})
  getElement: -> Autosuggest
  #This is to prevent a new function from being created every getInputProps call
  getSuggestSelect: ->
    if @_onSuggestSelect?
      return @_onSuggestSelect
    @_onSuggestSelect = @onChange.bind(@)
    return @_onSuggestSelect

  getInputProps: ->
    extend({}, super(arguments...), {
      onSuggestSelect: @getSuggestSelect()
      initialValue: @getValue()
      suggestions: @getSuggestions()
    })

module.exports = WebbAutosuggestField
