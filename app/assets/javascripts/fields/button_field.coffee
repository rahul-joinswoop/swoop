ButtonField = require('fields/common/button_field')
class WebButtonField extends ButtonField
  _className: ''
  getInputProps: ->
    props = super(arguments...)
    props.onClick = props.onChange
    props
  render: (@render_props) ->
    if !@isShowing() or !@isVisible()
      return null
    button @getInputProps(),
      @getLabel()


module.exports = WebButtonField
