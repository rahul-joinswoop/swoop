Field = require('fields/field')
class ZipField extends Field

  onChange: (e) ->
    modifiedInput = ''
    if e.target.value?.replace?
      modifiedInput = e.target.value.replace(/\D/g,'')
      if modifiedInput.length > 5
        modifiedInput = modifiedInput.slice(0, 5) + '-' + modifiedInput.slice(5)
      if modifiedInput.length > 10
        modifiedInput = modifiedInput.slice(0, 10)
    @setValue(modifiedInput)

  findErrors: ->
    value = @getValue()
    if value? && (value.length != 0 && value.length != 5 && value.length != 10)
      return "Invalid Zip Code"
    super(arguments...)

module.exports = ZipField
