import Utils from 'utils'
import { get } from 'lodash'

moduleKeywords = ['extended', 'included']

export mixin = (obj, mixin) ->
  for key, value of mixin when key not in moduleKeywords
    # Assign properties to the prototype
    obj::[key] = value

  mixin.included?.apply(obj)
  obj

class Field
  _tooltipText: null
  _showing: true
  _required: false
  _editable: true
  _enabled: true
  _placeholder: ""
  #ADDITIONAL OPTIONS
  #_name
  #_placeholder

  #Possible MIXINS???
  #_tabGroup
  #_toggleName
  #_antiToggleName
  #_redact

  constructor: (@record, @changeCallback, @original, @toggles, @tabs, @props) ->
    # see https://github.com/jashkenas/coffeescript/pull/4530 - these methods may
    # not be bound correctly
    if @isShowing.call(@) and !@getValue.call(@)?
      @prefill.call(@)
    @forcePrefill.call(@)
  #OVERWRITTABLE

  replaceRecord: (record, original) ->
    @record = record
    @original = original

  customFindErrors: (ignoreRequired) ->

  setValue: (value, silent=false, name=@getName(), record=@record) ->
    @setForcedError()
    Utils.setValue(record, name, value)
    @props?.form?.lastFieldChanged = name
    if !silent
      @changeCallback?()
    return value
    #that.form.setrecordData(@getName(), value)
  setValues: (items, silent=false, record=@record) ->
    @setValue(value, true, name, record) for name, value of items
    if !silent
      @changeCallback?()

  getValue: (skipTransforms=false, name=@getName()) ->
    if !skipTransforms
      if @getRedact()
        return @getRedact()

    get(@record, name)

    #TODO: THIS IS THROWING OFF THE PREFILLER
    #if !skipSet
    #  if !val?
    #    val =  ""
    #return val

  getRedact: -> @_redact
  getInfo: -> @_info
  getName: -> @_name
  getExample: -> @_example
  isShowing: -> @_showing
  isEditable: -> @_editable and @render_props?.editable
  isEnabled: -> @_enabled and !@render_props?.disabled
  isRequired: -> @_required
  getTabGroup: -> @_tabGroup
  getToggleName: -> @_toggleName
  getFeatureFlags: -> @_featureFlags
  getAntiToggleName: -> @_antiToggleName
  getElement: -> input
  getPlaceholder: -> @_placeholder
  getLabel: -> @_label
  getAutocomplete: -> @_autocomplete
  getTooltipText: -> @_tooltipText
  prefill: () ->
  forcePrefill: () ->
  setError: (error) ->
    @_error = error
  setToDisabled: () ->
    @_enabled = false
  setToEnabled: () ->
    @_enabled = true
  getError: () ->
    @_error
  clearError: ->
    @_error = null

  #This is used as the error until setValue is called on the field
  getForcedError: -> @_forced_error
  setForcedError: (error) ->
    @_forced_error = error

  findErrors: (ignoreRequired = false) ->
    value = @getValue()
    valueIsEmpty = !value? || value == ""
    flaggedAsRequired = false

    if valueIsEmpty and !ignoreRequired and @isRequired()
      flaggedAsRequired = true

    if typeof @customFindErrors == "function"
      customError = @customFindErrors(ignoreRequired)

      if customError and !(valueIsEmpty and ignoreRequired)
        return customError

    if flaggedAsRequired
      return "Required"

    if @getForcedError()?
      return @getForcedError()

  isValid: (ignoreRequired = false) ->
    @clearError()
    if !@isShowing() || !@isVisible()
      return true
    errors = @findErrors(ignoreRequired)
    @setError(errors)
    return !@getError()?

  fillExample: () ->
    if @isShowing()
      example = @getExample()
      if example?
        @setValue(example)

  #Helpers
  setIfEmpty: (value) ->
    if !@getValue()?
      @setValue(value)

  setToggle: (name, value) -> @toggles[name] = value
  getToggle: (name, value) -> @toggles[name]

  isVisible: (toggles, tabs) ->
    toggleName = @getToggleName()
    if @render_props?.toggleName?
      toggleName = @render_props.toggleName

    if (toggleName? and not @toggles[toggleName])
      return false

    if (@getTabGroup()? and @getName() != @tabs[@getTabGroup()])
      return false

    if (@_antiToggleName? and @toggles[@_antiToggleName])
      return false
    return true

  hideInfo: (e) ->
    @_forceInfo = false
    @_forceHideInfo = true
    @changeCallback?()

  toggleInfo: () ->
    @_forceInfo = !@_forceInfo
    @changeCallback?()

  showInfo: () ->
    @_forceInfo = true
    @_forceHideInfo = false
    @changeCallback?()


export default Field
