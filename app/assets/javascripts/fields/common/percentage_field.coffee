import React from 'react'
Field = require('fields/field')

class PercentageField extends Field
  name: "percentage"

  getValue: ->
    value = super(arguments...)

    if value
      value = ('' + value).replace(/[^0-9]/g, '')
    else
      value = ''

module.exports = PercentageField
