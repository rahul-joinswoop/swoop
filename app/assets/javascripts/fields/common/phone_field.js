import Utils from 'utils'
import { get } from 'lodash'

import Field from 'fields/field'

class PhoneField extends Field {
  name = 'phone'

  _placeholder = 'xxx-xxx-xxxx'

  findErrors(...props) {
    const value = this.getValue()

    if (value !== '' && value.charAt(0) !== '+' && this._validatePhone && !Utils.isValidPhone(value)) {
      return 'Invalid Phone'
    }

    return super.findErrors(...props)
  }

  getValue(...props) {
    let val = super.getValue(...props)
    val = Utils.renderPhoneNumber(val)
    return val || ''
  }

  alwaysBeforeSend = (newrecord) => {
    const obj = get(newrecord, this.getName())

    if (obj && obj.charAt(0) !== '+') {
      return Utils.setValue(
        newrecord,
        this.getName(),
        `+1${obj.split('-').join('')}`,
      )
    }
  }
}

export default PhoneField
