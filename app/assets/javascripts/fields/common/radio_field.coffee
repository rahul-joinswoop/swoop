import Utils from 'utils'
SelectField = require('fields/select_field')
class RadioField extends SelectField
  getValue: -> Utils.fixBool(super(arguments...))

module.exports = RadioField
