import { isString, map } from 'lodash'
Field = require('fields/field')

class CapitalizedField extends Field
  onChange: (e) ->
    data = if e.target? then e.target.value else e
    if isString(data)
      data = map(data.split(" "), (str) -> str.capitalize()).join(" ")
    @setValue(data)

module.exports = CapitalizedField
