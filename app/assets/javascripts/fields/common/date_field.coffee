import React from 'react'
import Field from 'fields/field'

class DateField extends Field
  _required: true
  onChange: (value) -> @setValue(value)
  onDateError: -> @setError('Date is invalid')

module.exports = DateField
