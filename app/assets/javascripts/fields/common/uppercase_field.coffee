Field = require('fields/field')
class UppercaseField extends Field
  customFindErrors: (ignoreRequired) ->
    super(ignoreRequired)
  onChange: (e) ->
    if e.target?
      @setValue(e.target.value.toUpperCase())
    else
      @setValue(e.toUpperCase())
module.exports = UppercaseField
