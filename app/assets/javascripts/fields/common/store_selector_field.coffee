Field = require('fields/field')
class StoreSelectorField extends Field
  #_sortFunc:
  #_populateFunc
  _store: null
  _populateEvent: null
  getSortFunc: -> @_sortFunc
  getPopulateFunc: -> @_populateFunc
  getStore: -> @_store
  getPopulateEvent: -> @_populateEvent
  onChange: (id, value) ->
    if value? and value.length > 0
      @setValue({
        id: id
        name: value
      })
    else
      @setValue(null)

module.exports = StoreSelectorField
