Field = require('fields/field')

class MultiSelectField extends Field
  _placeholder: "Select"
  _className: ""
  _showAll: true
  _alwaysShowSelections: false
  _emptyIsAll: false
  _dropdownTitle: ''
  onChange: (val) -> @setValue(val)
  getDropdownTitle: -> @_dropdownTitle
  showAll: -> @_showAll
  alwaysShowSelections: -> @_alwaysShowSelections
  emptyIsAll: -> @_emptyIsAll
  isLoading: -> false
module.exports = MultiSelectField
