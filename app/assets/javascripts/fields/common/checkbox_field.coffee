Field = require('fields/field')
import Utils from 'utils'
import { extend } from 'lodash'

class CheckboxField extends Field
  _inputType: "checkbox"
  _className: ""
  getValue: -> Utils.fixBool(super(arguments...))

  getLabelStyle: -> extend({}, super(arguments...), { marginLeft: 5, display: "inline-block", width: "initial" })


module.exports = CheckboxField
