Field = require('fields/field')
import Utils from 'utils'
import { some } from 'lodash'

class EmailField extends Field
  name: 'email'

  findErrors: ->
    if @_allowMultiple
      emails = @getValue()?.split(',')

      return 'Invalid email' if some(emails, (email) ->
        !Utils.isValidEmail(email)
      )

    else
      if !Utils.isValidEmail(@getValue())
        return 'Invalid email'
    super(arguments...)

module.exports = EmailField
