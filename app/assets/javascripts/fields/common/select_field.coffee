Field = require('fields/field')
import { values } from 'lodash'

class SelectField extends Field
  _options: [text: "- Select -", value: ""]
  _placeholder: "- Select -"
    #The way this is compiles into JS makes this required
  onChange:(e) ->
    super(e)
  getOptions: -> @_options
  optionMapper: (objs, initial={}, nameAttr="name", idAttr="id") ->
    for type in values(objs)
      if typeof type == "object"
        initial.push({text: type[nameAttr], value: type[idAttr]})
      else
        initial.push({text: type, value: type})
    initial

module.exports = SelectField
