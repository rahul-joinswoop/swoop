Field = require('fields/field')
class YearField extends Field
  onChange: (e) ->
    data = ''
    if e.target.value?.replace?
      data = e.target.value.replace(/\D/g,'')
    @setValue(data)
module.exports = YearField
