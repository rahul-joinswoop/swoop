Field = require('fields/field')

class BackendSuggestField extends Field
  _className: ""
  getSuggestions: ->
    return []

module.exports = BackendSuggestField
