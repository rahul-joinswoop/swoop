Field  = require('fields/field')
class FloatField extends Field
  onChange: (e) ->
    if e.target?
      @setValue(e.target.value.replace(/[^0-9]/g,''))
    else
      @setValue(e.replace(/[^0-9]/g,''))

module.exports = FloatField
