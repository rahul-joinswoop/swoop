Field = require('fields/field')
PoiStore = require('stores/poi_store').default
SiteStore = require('stores/site_store').default
import { extend } from 'lodash'
class GeoSuggestField extends Field
  _hideMap: true

  isMapHidden: -> @_hideMap
  getDefaultLat: -> @_defaultLat
  getDefaultLng: -> @_defaultLng

  customFindErrors: (ignoreRequired) ->
    lat = @getValue(true, @getName()+".lat")
    lng = @getValue(true, @getName()+".lng")

    if !ignoreRequired and @isRequired() and !(lat? and lng?)
      return "Address Not Set"

    if @inputRef?.isLoading?()
      return "Still Loading"

    #check to make sure we have locations for service_location
    isAddressSet = !! @getValue(true, @getName())?.address
    if isAddressSet and (not lat or not lng)
      return "Select an exact address or drop a pin with the pin icon."

    super(ignoreRequired)

  alwaysBeforeSend: (newrecord) ->
    if newrecord[@getName()]?
      if @original?[@getName()]?

        if @original?[@getName()]?.site_id? and !@record?[@getName()]?.site_id?
          newrecord[@getName()] = extend({}, @original[@getName()], newrecord[@getName()])
          delete newrecord[@getName()].site_id
        else if !@original?[@getName()]?.site_id? and @record?[@getName()]?.site_id?
          siteLocation = SiteStore.get(@record[@getName()].site_id, 'location') || PoiStore.get(@record[@getName()].site_id, 'location')

          newrecord[@getName()] = extend({}, siteLocation, newrecord[@getName()])
        else
          newrecord[@getName()] = extend({}, @original[@getName()], newrecord[@getName()])

      delete newrecord[@getName()].id
      delete newrecord[@getName()].state
      delete newrecord[@getName()].city
      delete newrecord[@getName()].street
      delete newrecord[@getName()].zip
      delete newrecord[@getName()].updated_at


module.exports = GeoSuggestField
