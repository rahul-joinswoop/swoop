Field = require('fields/field')
class ToggleField extends Field
  _offText: "off"
  _onText: "on"
  getOnText: -> @_onText
  getOffText: -> @_offText
  isOn: -> @getToggle(@getName())
  isOff: -> !@isOn()
  toggleOn: ->
    @setToggle(@getName(), true)
  toggleOff: ->
    @setToggle(@getName())

  onChange: ->
    if @isOn() then @toggleOff() else @toggleOn()
    @changeCallback?()


module.exports = ToggleField
