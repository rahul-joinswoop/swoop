Field = require('fields/field')
import moment from 'moment-timezone'
import React from 'react'

class DateTimeField extends Field
  _showTimezone: false
  _future: true
  _required: true
  _disableValidation: false
  getDisableValidation: -> @_disableValidation
  getShowTimezone: -> @_showTimezone
  getFuture: -> @_future
  onChange: (value) -> @setValue(moment(value).format())
  onDateError: -> @setError("Time is out of acceptable range")

module.exports = DateTimeField
