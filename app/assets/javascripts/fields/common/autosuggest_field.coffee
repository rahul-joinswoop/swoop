Field = require('fields/field')

class AutosuggestField extends Field
  _placeholder: "Select"
  _className: ""
  getSuggestions: -> @_suggestions
  onChange: (val) -> @setValue(val)

module.exports = AutosuggestField
