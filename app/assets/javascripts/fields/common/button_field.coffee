Field = require('fields/field')
class ButtonField extends Field
  onChange: -> @setValue(!@getValue())

module.exports = ButtonField
