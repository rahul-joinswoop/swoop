CheckboxField = require('fields/common/checkbox_field')
import { extend } from 'lodash'

class WebCheckboxField extends CheckboxField
  _inputType: "checkbox"
  _className: ""

  getLabelStyle: -> extend({}, super(arguments...), { marginLeft: 5, display: "inline-block", width: "initial" })
  onChange: (e) -> @setValue(e.target.checked)
  getContainerClasses: ->
    classes = super(arguments...)
    classes = classes.split("form-group").join("")
    classes += " checkbox_control"
    return classes

  getInputProps: ->
    props = super(arguments...)
    props.checked = @getValue()
    return props

  render: (@render_props) ->
    if !@isShowing() or !@isVisible()
      return null
    @renderContainer div
      className: "inputWrapper"
      @renderInput()
      @renderLabel()


module.exports = WebCheckboxField
