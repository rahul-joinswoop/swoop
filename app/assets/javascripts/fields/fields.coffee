import CapitalizedField from 'fields/capitalized_field'
import YearField from 'fields/year_field'
import ZipField from 'fields/zip_field'
import PhoneField from 'fields/phone_field'
import IntlPhoneField from 'fields/IntlPhoneField'
import TextareaField from 'fields/textarea_field'

module.exports =
  AutosuggestField: require('fields/autosuggest_field')
  BackendSuggestField: require('fields/backend_suggest_field')
  ButtonField: require('fields/button_field')
  CapitalizedField: CapitalizedField
  CheckboxField: require('fields/checkbox_field')
  CreditCardField: require('fields/credit_card_field')
  DateField: require('fields/date_field')
  DateTimeField: require('fields/datetime_field')
  EmailField: require('fields/email_field')
  Field: require('fields/field')
  FilterOutMultiSelectField: require('fields/filterout_multiselect_field')
  FloatField: require('fields/float_field')
  GeoSuggestField: require('fields/geosuggest_field')
  MoneyField: require('fields/MoneyField').default
  MultiSelectField: require('fields/multiselect_field')
  NumberField: require('fields/number_field')
  PercentageField: require('fields/percentage_field')
  PhoneField: PhoneField
  IntlPhoneField: IntlPhoneField
  RadioField: require('fields/radio_field')
  SelectField: require('fields/select_field')
  SpanField: require('fields/span_field')
  StoreSelectorField: require('fields/store_selector_field')
  TextareaField: TextareaField
  ToggleButtonField: require('fields/toggle_button_field')
  ToggleField: require('fields/toggle_field')
  UppercaseField: require('fields/uppercase_field')
  YearField: YearField
  ZipField: ZipField
