import React from 'react'
StoreSelector = React.createFactory(require('components/storeSelector'))
StoreSelectorField = require('fields/common/store_selector_field')
import { extend } from 'lodash'

class WebStoreSelectorField extends StoreSelectorField
  #_sortFunc:
  #_populateFunc
  getErrorStyle: -> extend({}, super(arguments...), {right: 23})

  getElement: -> StoreSelector
  onChange: (id, value) ->
    if value? and value.length > 0
      @setValue({
        id: id
        name: value
      })
    else
      @setValue(null)

  getInputProps: ->
    extend({}, super(arguments...), {
      sortFunc: @getSortFunc()
      populateFunc:  @getPopulateFunc()
      store: @getStore()
      populateEvent: @getPopulateEvent()
    })

module.exports = WebStoreSelectorField
