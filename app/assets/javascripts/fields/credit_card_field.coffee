import React from 'react'
Field = require 'fields/field'
{ CardElement } = require('react-stripe-elements')

import StripeLoaded from 'components/StripeLoaderHoC'

CardElement = React.createFactory(StripeLoaded(CardElement))

class WebCreditCardField extends Field
  constructor: ->
    super(arguments...)
    @complete = false
    @empty = true

  wrappedCreditCardElement: (props) ->
    CardElement(props)

  getElement: () => @wrappedCreditCardElement

  findErrors:() ->
    if !(@complete || (@empty && !@isRequired()))
      return "Not Complete"

    if @getForcedError()?
      return @getForcedError()

  getInputProps: =>

    props =  super(arguments...)

    {
      onChange: (status) =>
        @setForcedError()
        if status.complete != @complete || status.empty != @empty
          triggerRerender = true

        @complete = status.complete
        @empty = status.empty

        if triggerRerender
          @changeCallback?()

      className: props.className
      hidePostalCode: @render_props.hidePostalCode
    }
  alwaysBeforeSend: (newRecord) ->
    if !@record[@getName()]? or (@record[@getName()]?.length == 0 and @original[@getName()]?.length > 0)
      newRecord[@getName()] = []

module.exports = WebCreditCardField
