SelectField = require('fields/common/select_field')
import { extend } from 'lodash'
class WebSelectField extends SelectField
  getErrorStyle: -> extend({}, super(arguments...), {right: 17})
  renderInput: ->
    if !@isEditable()
      return super(arguments...)
    index = 1
    select @getInputProps(),
      for o in @getOptions()
          if typeof o == "object"
            value = o.value
            text = o.text
          else
            value = o
            text = o
          options =
            key: "option_"+text+index++
            value: if not value? then '' else value
            className: 'option_row'
            disabled: o.disabled
            hidden: o.hidden
          #if value == ''
          #  options.disabled = true
          option options,
            text



module.exports = WebSelectField
