import React from 'react'
import Utils from 'utils'
import Field from 'fields/common/field'
import ReactDOMFactories from 'react-dom-factories'

class WebField extends Field
  _inputType: "text"
  _className: "form-control"
  _animate: false
  _forceError: false
  #ADDITIONAL OPTIONS
  #_name
  #_placeholder
  #_hoverText

  #Possible MIXINS???
  #_tabGroup
  #_toggleName
  #_antiToggleName
  #_redact

  getHoverText: -> @_hoverText

  onChange: (e) -> @setValue(e.target.value)

  getOnChange: ->
    if @_onChange?
      return @_onChange
    @_onChange = @onChange.bind(@)
    return @_onChange

    #TODO: THIS IS THROWING OFF THE PREFILLER
    #if !skipSet
    #  if !val?
    #    val =  ""
    #return val
  getInputType: -> @_inputType
  getClassName: -> @_className + (if @_animate then " field-populated-by-another-field-highlight " else "") + (if !@isEnabled() then " disabled " else "")
  animate: ->
    @_animate = true
    setTimeout(=>
      @_animate = false
    , 3000)


  getLabelStyle: ->
    @render_props?.labelStyle

  getContainerStyle: ->
    Utils.combineStyles({position: "relative"}, @render_props?.containerStyle)

  getStyle: ->
    Utils.combineStyles({boxSizing: "border-box"}, @render_props?.style, @addErrorBorder({}))

  hideInfo: (e) ->
    super(e)
    if e
      e.preventDefault()
      e.stopPropagation()

  getInputProps: ->
    id = 'Input'+Utils.turnIntoClassName(@getName())
    props =
      id: id
      type: @getInputType()
      key: @getClassName()
      className: @getClassName()
      name: @getName() #record[driver_attributes[user_attributes[first_name]]] value="<%=@record.driver.user.first_name%>" required
      value: (if @getValue()? then @getValue() else '')
      onChange: @getOnChange()
      placeholder: @getPlaceholder()
      style: @getStyle()

    if !@isEnabled()
      props.enabled = false
      props.disabled = true

    autocomplete = @getAutocomplete()

    if autocomplete == 'off'
      props.autoComplete = 'swoop-' + Date.now() + '-' + Math.floor(Math.random() * 10000000)

    if @onBlur
      props.onBlur = @onBlur
    props

  getContainerClasses: ->
    'form-group ' + (if @render_props?.containerClass? then @render_props.containerClass else "")

  getBottomError: () -> return null

  renderLabel: () ->
    labelName = @getLabel()
    info = @getInfo()
    if labelName?
      label
        htmlFor: 'Input'+Utils.turnIntoClassName(@getName())
        onClick: @_labelClick?.bind(this)
        className: 'control-label ' + (if @render_props?.labelClassName then @render_props.labelClassName else "")
        style: Utils.combineStyles({fontSize: 12, marginBottom: 1}, @getLabelStyle())
        labelName
        if info?
          @renderInfo(info)

  addErrorBorder: (style) ->
    error = @findErrors(@render_props?.ignoreRequired)
    @setError(error)
    if error?
      style.border = @getRequiredFieldBorderStyle()
      if @render_props?.submit_attempts > 0 || @forceError()
        style.border = "2px solid #F00"
    return style

  getRequiredFieldBorderStyle: ->
    @_requiredFieldBorderStyle || "2px solid #7FBCE2"

  renderBottomError: ->
    bottomError = @getBottomError()
    if bottomError
      div
        className: "bottom_error"
        bottomError

  renderContainer: (children) ->
    id = "Container"+Utils.turnIntoClassName(@getName())
    props =
      className:  @getContainerClasses()
      id: id
      key: @getName()
      style: @getContainerStyle()

    if @getHoverText()
      props['data-container'] = 'body'
      props['data-toggle'] = 'popover'
      props['data-placement'] = 'left'
      props['data-content'] = @getHoverText()

      #HACK: TODO: convert to componentDidMount after conversion to react components
      if !@popoverSetUp
        setTimeout( ((id) =>
          =>
            console.log("Setting up popover: ", id)
            $("#" + id).popover({
              trigger: 'hover',
              html: true
            })
          )(id)
        , 1000)
        @popoverSetUp = true

    div props, children

  getInfoStyle: ->
    style = {
      textAlign: "center"
      border: "1px solid #AAAAAA"
      color: "#AAAAAA"
      width: 18
      height: 18
      marginLeft: 5
      borderRadius: 9
      display: "inline-block"
      position: "relative"}
    return Utils.combineStyles(style, @render_props?.info_style)

  getErrorStyle: ->
    position: "absolute"
    right: 5
    bottom: 7
    textAlign: "center"
    border: "1px solid #F00"
    color: "#f00"
    width: 20
    height: 20
    borderRadius: 10
    fontWeight: "bold"

  forceError: -> @_forceError
  setForceError: (@_forceError) ->
  getInfoIconText: ->
    "i"
  renderInfo: (info) ->
    div
      className: "error_bubble info_window "+(if @_forceInfo then " force_show ")+(if @_forceHideInfo then " force_hide")
      onClick: @showInfo.bind(@)
      style: @getInfoStyle()
      @getInfoIconText()
      div
        className: "bubble"
        style:
          color: "#AAAAAA"
        button
          style:
            position: "absolute"
            top: 0
            right: 5
            outline: 0
            border: 0
            fontSize: 22
            background: "none"
            cursor: "pointer"
          onClick: @hideInfo.bind(@)
          "×"
        span
          style:
            cursor: "auto"
          info

  customFindErrors: (ignoreRequired) ->
    super(ignoreRequired)

  renderInput: ->
    #MIXIN PERHAPS?
    if !@isEditable()
      return span
        className: 'field-span'
        style: Utils.combineStyles(display: "block", @getStyle())
        if @getViewValue then @getViewValue() else @getValue()
    @getElement()(@getInputProps())

  renderError: ->

    if !@getError()? || (@render_props?.submit_attempts == 0 && !@forceError())
      return null

    div
      className: "error_bubble"
      style: @getErrorStyle()
      "!"
      div
        className: "bubble"
        span null,
          @getError()

  renderTooltip: ->
    tooltipText = @getTooltipText()
    if tooltipText?
      ReactDOMFactories.i
        className: @getInfoIconClassName()
        div
          className: "field-icon-tooltip"
          tooltipText

  getInfoIconClassName: ->
    return ''

  getFieldErrorClass: ->
    if @_error && (@render_props?.submit_attempts > 0 || @forceError()) then "field-error " else ""

  getEditableClass: ->
    if @isEditable() then "editable " else ""

  render: (@render_props) ->
    if !@isShowing() or !@isVisible()
      return null

    @renderContainer div
      className: "inputWrapper " + @getEditableClass() + @getFieldErrorClass()
      @renderLabel()
      @renderInput()
      @renderError()
      @renderTooltip()
      @renderBottomError()

module.exports = WebField
