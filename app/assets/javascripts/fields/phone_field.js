import React from 'react'
import { extend, get } from 'lodash'
import PhoneField from 'fields/common/phone_field'

const MaskedInput = React.createFactory(require('react-text-mask').default)

class WebPhoneField extends PhoneField {
  allowDisableMasking = false

  autoFocus = true

  constructor(...props) {
    super(...props)
    this.disableMasking = false

    if (this.allowDisableMasking) {
      const value = get(this.record, this.getName())

      if (
        (value != null ? value.charAt(0) : undefined) === '+' &&
        value.length !== 12
      ) {
        this.disableMasking = true
      }
    }
  }

  getPlaceholder() {
    if (this.disableMasking) {
      return ''
    } else {
      return 'xxx-xxx-xxxx'
    }
  }

  getElement() {
    if (this.disableMasking) {
      return input
    } else {
      return MaskedInput
    }
  }

  getValue(...props) {
    if (this.disableMasking) {
      return get(this.record, this.getName())
    } else {
      return super.getValue(...props)
    }
  }

  useMasking(enabled) {
    if (enabled) {
      this.allowDisableMasking = false
      this.disableMasking = false
      this.autoFocus = true // true is the default for this field, so make sure it's back to true
    } else {
      this.allowDisableMasking = true
      this.disableMasking = true
      this.autoFocus = false // otherwise it will move the focus to it if you edit any other field (not sure why)
    }
  }

  onChange(e, ...props) {
    if (this.disableMasking) {
      return super.onChange(e, ...props)
    }

    const number = e.target.value.replace(/[^0-9]/g, '')
    const prevNumber = this.getValue().replace(/[^0-9]/g, '')

    if (number.length === 10 && prevNumber.length === 10) {
      this.inputRef.textMaskInputElement.update(this.getValue())
      const sel = this.inputRef.inputElement.selectionStart

      if (sel < 11) {
        this.inputRef.inputElement.setSelectionRange(sel - 1, sel - 1)
      }

      e.preventDefault()
      e.stopPropagation()
      return
    }

    return super.onChange(e, ...props)
  }

  getInputProps(...props) {
    let maskProps = {
      autoFocus: this.autofocus,
    }
    const inputValue = this.getValue()

    if (!this.disableMasking) {
      if (this.inputRef != null) {
        this.inputRef.textMaskInputElement.update(inputValue)
      }

      let mask
      if (inputValue.trim()) {
        // if we have at least first char we can decide if it is international number or US
        mask = inputValue[0] === '+' ?
          [/\+/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/] :
          [/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
      } else {
        // allow to start phone number with + or non 0 digit
        mask = [/[+1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
      }

      maskProps = {
        mask,
        guide: false,
        pipe: (conformedValue, config) => {
          if (this.allowDisableMasking) {
            if (this.disableMasking || config.rawValue === '+') {
              this.disableMasking = true
              return config.rawValue
            }
          }

          return conformedValue
        },
      }
    }

    return extend({}, super.getInputProps(...props), maskProps, {
      ref: (inputRef) => {
        this.inputRef = inputRef
      },
    })
  }
}

export default WebPhoneField
