import Utils from 'utils'
RadioField = require('fields/common/radio_field')

class WebRadioField extends RadioField
  _inputType: "radio"
  onChange: (e) -> @setValue(Utils.fixBool(e.target.value))
  renderInput: ->
    inputProps =
      style: @getStyle()
      id:  'Input'+Utils.turnIntoClassName(@getName())

    index = 1
    div inputProps,
      for o in @getOptions()
        if typeof o == "object"
          value = o.value
          text = o.text
        else
          value = o
          text = o

        inputProps =
          type: @getInputType()
          name: @getName()
          value: value
          onChange: @onChange.bind(this)
          id: @getName()+"_"+value
        if value == @getValue()
          inputProps.defaultChecked=true

        span
          style:
            display: "inline-block"
            marginRight: 15
          input inputProps
          label
            style:
              marginLeft: 5
              cursor: "pointer"
              fontSize: 12 #TODO: figure out how to support twolabels
            htmlFor: inputProps.id
            text

module.exports = WebRadioField
