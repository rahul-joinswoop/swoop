import React from 'react'
import DateSelectorImport from 'components/time_pickers/date_selector.js'
DateSelector = React.createFactory(DateSelectorImport)
DateField = require('fields/common/date_field')
import { extend } from 'lodash'

class WebDateField extends DateField
  getElement: -> DateSelector
  getContainerStyle: -> super(arguments...)
  getContainerClasses: -> super(arguments...)

  getInputProps: ->
    extend({}, super(arguments...), {
      defaultDate: @getValue()
      onError: @onDateError.bind(this)
    })

module.exports = WebDateField
