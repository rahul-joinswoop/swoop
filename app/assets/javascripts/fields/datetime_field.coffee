DateTimeField = require('fields/common/datetime_field')
import React from 'react'
import DateTimeSelectorImport from 'components/time_pickers/date_time_selector.js'
DateTimeSelector = React.createFactory(DateTimeSelectorImport)

import DateTimeSelectorWithTimezoneImport from 'components/time_pickers/date_time_selector_with_timezone.js'
DateTimeSelectorWithTimezone = React.createFactory(DateTimeSelectorWithTimezoneImport)

import moment from 'moment-timezone'
import { extend, isString } from 'lodash'


class WebDateTimeField extends DateTimeField
  getElement: ->
    if @getShowTimezone()
      DateTimeSelectorWithTimezone
    else
      DateTimeSelector
  getContainerStyle: -> @addErrorBorder(super(arguments...))
  getContainerClasses: -> super(arguments...) + " JobDateTimeField"

  getInputProps: ->
    modifiedArguments = super(arguments...)
    if isString(modifiedArguments?.value)
      modifiedArguments.value = moment(modifiedArguments.value)
    extend({}, modifiedArguments, {
      disableValidation: @getDisableValidation()
      defaultDate: @getValue()
      showTimezone: @getShowTimezone()
      validate: @validate
      onError: @onDateError.bind(this)
      future: @getFuture()
    })

module.exports = WebDateTimeField
