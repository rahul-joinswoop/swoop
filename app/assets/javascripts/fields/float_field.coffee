FloatField = require('fields/common/float_field')

class WebFloatField extends FloatField
  onChange: (e) -> @setValue(e.target.value.replace(/[^0-9]/g,''))


module.exports = WebFloatField
