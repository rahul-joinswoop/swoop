import React from 'react'
MaskedInput = React.createFactory(require('react-text-mask').default)
import { extend } from 'lodash'
PercentageField = require('fields/common/percentage_field')

class WebPercentageField extends PercentageField
  @_maxLength: null

  getInputProps: =>
    extend({}, super(arguments...), {
      maxLength: @_maxLength
    })

module.exports = WebPercentageField
