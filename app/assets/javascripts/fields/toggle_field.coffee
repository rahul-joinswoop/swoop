ToggleField = require('fields/common/toggle_field')
class WebToggleField extends ToggleField
  renderInput: ->
    selectedStyle = {
      background: "#FFF"
      border: "1px solid #CCCCCC"
      boxShadow: "0px 1px 1px 0px rgba(0,0,0,0.25)"
      borderRadius: 4
    }
    div
      style:
        background: "#F2F2F2"
        border: "1px solid #E2E2E2"
        boxShadow: "inset 0px 1px 3px 0px rgba(0,0,0,0.25)"
        borderRadius: 4
        userSelect: 'none'
        height: 34
      div
        className: "select_toggle_option off_option"
        style: if @toggles[@getName()] then {} else selectedStyle
        onClick: if @toggles[@getName()] then @onChange.bind(@)
        @getOffText()
      div
        style: if @toggles[@getName()] then selectedStyle else {}
        onClick: if !@toggles[@getName()] then @onChange.bind(@)
        #onClick: =>
        #  @setrecordData("policyNumberToggle", false)
        className: "select_toggle_option on_option"
        @getOnText()


module.exports = WebToggleField
