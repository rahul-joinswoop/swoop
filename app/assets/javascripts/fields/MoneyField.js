import { isNaN } from 'lodash'
import Field from 'fields/field'

class MoneyField extends Field {
  /* eslint-disable-next-line */
  static MONEY_REGEX = new RegExp('^\\d{0,6}(\\.|(\\.\\d{1,2})?)$')

  onBlur() {
    let val = this.getValue()
    // it is possible user leave value like '44.' without decimal part, so we need to fix this value
    val = parseFloat(val)
    if (!isNaN(val)) {
      this.setValue(val.toFixed(2))
    }
  }

  onChange(e) {
    if (this.constructor.MONEY_REGEX.test(e.target.value)) {
      this.setValue(e.target.value)
    }
  }
}

export default MoneyField
