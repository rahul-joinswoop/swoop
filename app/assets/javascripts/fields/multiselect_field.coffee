import React from 'react'
MultiSelect = React.createFactory require 'components/multiselect/multiselect'
MultiSelectField = require 'fields/common/multiselect_field'
import { extend } from 'lodash'

class WebMultiFilterField extends MultiSelectField
  _showDropdownTitle: true

  getElement: -> MultiSelect

  getInputProps: ->
    extend({}, super(arguments...), {
      values: @getValue()
      suggestions: @getSuggestions() || []
      showDropdownTitle: @_showDropdownTitle
      dropdownTitle: 'Select ' + if @getLabel() then @getLabel() else @getDropdownTitle()
      isLoading: @isLoading()
      showAll: @showAll()
      alwaysShowSelections: @alwaysShowSelections()
      emptyIsAll: @emptyIsAll()
    })

  alwaysBeforeSend: (newRecord) ->
    if !@record[@getName()]? or (@record[@getName()]?.length == 0 and @original[@getName()]?.length > 0)
      newRecord[@getName()] = []

module.exports = WebMultiFilterField
