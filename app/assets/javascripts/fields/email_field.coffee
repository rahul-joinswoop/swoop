EmailField = require('fields/common/email_field')

class WebEmailField extends EmailField
  _allowMultiple: false
  onChange: (e) -> @setValue(e.target.value.toLowerCase().trim())

module.exports = WebEmailField
