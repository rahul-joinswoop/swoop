import React from 'react'
ToggleButton = React.createFactory(require('react-toggle-button'))
ToggleButtonField = require('fields/common/toggle_button_field')
class WebToggleButtonField extends ToggleButtonField
  renderInput: (props) ->
    ToggleButton(props)


module.exports = WebToggleButtonField
