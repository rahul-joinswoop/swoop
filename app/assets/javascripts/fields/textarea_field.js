import TextareaField from 'fields/common/textarea_field'

class WebTextareaField extends TextareaField {
  getElement() {
    return textarea
  }
}

export default WebTextareaField
