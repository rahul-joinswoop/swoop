import Utils from 'utils'
import React from 'react'
Geosuggest = React.createFactory(require('components/geosuggest/Geosuggest').default)
GeoSuggestField = require('fields/common/geosuggest_field')
import { extend, get, partial } from 'lodash'

class WebGeoSuggestField extends GeoSuggestField
  _className: 'InputVehicleAddress geosuggest-input address'
  _defaultLat: 37.7577
  _defaultLng: -122.4376

  customFindErrors: (ignoreRequired) ->
    super(ignoreRequired)

  getElement: -> Geosuggest
  setLatLng: (lat, lng, fixedOption) ->
    @inputRef.setLatLng(lat, lng, fixedOption)
  getErrorStyle: -> extend({}, super(arguments...), {right: 27})

  getViewValue: ->
    value = @getValue()
    if value then value.address else ''

  onChange: (e, suggest) ->
    if !suggest?
      if @getValue()?['location_type_id']?
        @setValue({location_type_id: @getValue()['location_type_id']})
      else
        @setValue(null)
      return

    address = @buildAddressValue(suggest)

    @setValue(address)

  buildAddressValue: (suggest) ->
    address = {}

    if @getValue()?['location_type_id']?
      address.location_type_id = @getValue()['location_type_id']

    if suggest.location?
      address.lat = suggest.location.lat
      address.lng = suggest.location.lng

    address.place_id = suggest.placeId
    address.address = suggest.label

    if suggest.exact
      address.exact = suggest.exact
    else
      address.exact = false

    address

  getInputProps: ->
    extend({}, super(arguments...), {
      onSuggestSelect: partial(@onChange, null).bind(@)
      initialValue: get(@record, @getName()+".address") || ''
      initialLat: get(@record, @getName()+".lat") || @getDefaultLat()
      initialLng: get(@record, @getName()+".lng") || @getDefaultLng()
      ref: (input) =>
        @inputRef = input?.instanceRef
      _defaultLat: 37.7577
      _defaultLng: -122.4376
      location: new google.maps.LatLng(@getDefaultLat(), @getDefaultLng())
      always_show_map: false
      featureFlags: @getFeatureFlags()
      send_all_addresses: true
      callback_on_enter: @render_props?.callback_on_enter?.bind(this)
      hide_map: @isMapHidden()
      bounds: @render_props?.bounds
      fixedOptions: @getFixedOptions?()
      fixedOptionsLabel: if typeof @_fixedOptionsLabel == 'function' then @_fixedOptionsLabel() else (@_fixedOptionsLabel || '')
      formatInputValue: @formatInputValue
    })

module.exports = WebGeoSuggestField
