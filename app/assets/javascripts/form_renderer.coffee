import Utils from 'utils'
import { extend, filter, isArray, isDate, isFunction, isString, isObject, keys, map, unset, values } from 'lodash'

#TODO: check if we can replace this with _.cloneDeep
_deepClone = (obj, depth) ->
  clone = undefined
  key = undefined
  depth = depth or 1
  if typeof obj != 'object' or obj == null
    return obj
  if isString(obj)
    return obj.splice()
  if isDate(obj)
    return new Date(obj.getTime())
  if isFunction(obj.clone)
    return obj.clone()
  clone = if isArray(obj) then obj.slice() else extend({}, obj)
  if depth != undefined and depth > 0
    for key of clone
      clone[key] = _deepClone(clone[key], depth - 1)
  clone

#Responsible for keeping track of the state of the form, instantiating fields,
# hiding/showing fields based on record changes, and determining what has changed


class Renderer
  constructor: (record, callback, options) ->
    @toggles = {}
    @tabs = {}
    @options = options
    @record = _deepClone(record, 5)
    @orig_record = _deepClone(record, 5)
    @callback = callback
    window.rForm = @
    @submit_attempts = options?.submit_attempts || 0
    @fields = {}
    @_ignoreRequired = false
    @_editable = true

  setEditable: (editable) ->
    @_editable = editable

  isEditable: () -> @_editable

  replaceRecord: (record) ->
    #TODO: Only replace differecnces
    @record = _deepClone(record, 5)
    @orig_record = _deepClone(record, 5)
    for name, field of @fields
      field?.replaceRecord?(@record, @orig_record)
    @callback()

  isShowingTab: (group, name) ->
    #Hack to select first tab
    if !@tabs?[group]?
      @tabs[group] = name
      setTimeout(=>
        @callback()
      , 0)

    @tabs?[group] == name

  getToggle: (name) ->
    @toggles[name]

  setToggle: (name, value) ->
    if value?
      @toggles[name] = value
    else
      delete @toggles[name]

  setIgnoreRequired: (value) =>
    @_ignoreRequired = value

  getIgnoreRequired: =>
    @_ignoreRequired

  hasAttemptedSubmit: () ->
    @submit_attempts > 0

  attemptedSubmit: () ->
    if !@submit_attempts?
      @submit_attempts = 0
    @submit_attempts++

  setAttemptedSubmits: (x) ->
    @submit_attempts = x

  isValidField: (field) ->
    if field.getBottomError?()
      return false

    field.setError()
    valid = field.isValid(@getIgnoreRequired())

    if valid != true
      valid = false
    return valid

  isValid: (log = false) ->
    isValid = true

    for field in values(@fields)
      #Clear previous error
      valid = @isValidField(field)
      if valid != true
        if log
          console.log(field.getName(), "is invalid: ", valid, field.getError())
        isValid = false

    return isValid

  setFirstTab: (group, name) ->
    if !@tabs?[group]?
      @tabs[group] = name

  setTab: (tabGroup, name) ->
    @tabs[tabGroup] = name
    @onChange()

  getTabGroupCols: (tabGroup) ->
    filter(@fields, (field) ->
      field.getTabGroup() == tabGroup
    )

  onChange: (event) ->
    @callback()

    if @submit_attempts
      @isValid()

  register: (clz, name) ->
    if name? and @fields[name]
      return @fields[name]

    if !@options?
      @options = {}

    @options['form'] = @
    instance = new clz(@record, @onChange.bind(@), @orig_record, @toggles, @tabs, @options)
    if !@fields[instance._name]?
      @fields[instance._name] = instance
    @fields[instance._name]

  registerFields: (classes, validate=false) ->
    if !@fields?
      @fields = {}

    for name, clz of classes
      # Cast `name` to typeof string, to ensure we can use String.prototype.indexOf
      if (name + '').indexOf("Factory") == -1
        @register(clz)

    if validate
      @isValid()
      #instance = new classes[name](@record, @onChange.bind(@), @orig_record, @toggles, @tabs)
      #@fields[instance._name] = instance


  removeMatchingFields: (name) ->
    for fieldName in keys(@fields)
      if fieldName.indexOf(name) > -1
        delete @fields[fieldName]

  getField: (name) ->
    return @fields[name]

  renderAllInputs: (props) ->
    map(@fields, (col, name) =>
      @renderInput(name, props)
    )

  renderInputs: (cols, extraOptions={}) ->
    map(cols, (col, i) =>
      name = col
      addons = {}
      if Array.isArray(col)
        name = col[0]
        addons = col[1] || addons
      addons.key = i

      @renderInput(name, addons)
    )

  fillExamples: () ->
    for key, field of @fields
      field.fillExample()

  renderInput: (name, addons) ->
    #Allows passing cols or strings
    if !name?
      return

    instance = @fields[name]

    if !addons?
      addons = {}

    addons.submit_attempts = @submit_attempts
    addons.ignoreRequired = @getIgnoreRequired()
    addons.editable = @_editable

    if instance?
      instance.render(addons)
    else
      console.warn("Tried to render an invalid field: ", name)

  getRecordChanges: () ->
    newrecord = {}
    if @orig_record?.id?
      newrecord.id = @orig_record.id
    for key, field of @fields
      if typeof(field.beforeSend) == "function"
        field.beforeSend()

    if @options?.forceFullObject
      newrecord = @record
    else
      newrecord = Utils.compareObjs({}, @record, @orig_record)
      Utils.addMissingIds(newrecord, @orig_record)

      #TODO:Move this onto the col
      if @options?.ignoreFields?
        for field in @props.ignoreFields
          unset(newrecord, field)

      #TODO:Move this onto the col
      for key, field of @fields

        if typeof field.aboutToSend == "function"
          if Utils.hasKey(newrecord, field.getName())
            field.aboutToSend(newrecord)
        if typeof field.alwaysBeforeSend == "function"
          field.alwaysBeforeSend(newrecord)

      if @options?.submitCallback?
        callback = @options.submitCallback

      if @options?.aboutToSend?
        @options.aboutToSend(newrecord)

    return newrecord

  attemptSubmit: (submit, close, setShowLoader) ->

    if @isValid()
      if setShowLoader?
        setShowLoader(true)
      newRecord = @getRecordChanges()
      newRecord.callbacks =
        success: =>
          close?()
          return true
        error: (errorResponse) =>
          if setShowLoader?
            setShowLoader(false)
          @setResponseError errorResponse.responseJSON
          return true
      submit(newRecord)
    else
      @attemptedSubmit()
      @callback()

  setResponseError: (error) ->
    if error? && isObject(error)
      errors = Object.keys error
      errors.forEach (key) =>
        fieldErrors = error[key]
        field = @getField(key)

        if field && fieldErrors.length
          field.setForcedError fieldErrors[0]

    @attemptedSubmit()
    @callback()

module.exports = Renderer
