import Consts from 'consts'
import UserStore from 'stores/user_store'
import ServiceStore from 'stores/service_store'
import JobStore from 'stores/job_store'

const MAP_COMPANY_TYPE_TO_TRACKING_TYPE = {
  SuperCompany: 'Swoop',
  RescueCompany: 'Partner',
  FleetCompany: 'FleetCompany',
}

class BaseTracking {
  // TODO: pull up shared logic here
  identify(user) {}

  track(name, options = {}, extra_options = {}) {
    const user = UserStore.getUser()

    if (user?.company && !options.companyName) {
      options.companyName = user.company.name
    }

    const userCompanyType = options.company?.type || user?.company?.type

    let companyType = MAP_COMPANY_TYPE_TO_TRACKING_TYPE[userCompanyType]

    if (companyType === 'FleetCompany') {
      companyType = UserStore.isFleetManaged() ? 'Fleet Managed' : 'Fleet InHouse'
    }

    options.companyType = companyType

    if (user?.id && !options.userId) {
      options.userId = user.id?.toString()
    }

    const subscriptionStatus =
      Consts.SUBSCRIPTION_STATUS[(UserStore.getCompany()?.subscription_status_id)]

    if (subscriptionStatus) {
      options.subscriptionStatus = subscriptionStatus
    }

    return this._sendEvent(name, options, extra_options)
  }

  _sendEvent() {}

  trackJobEvent(name, job_id, options = {}) {
    options.job_id = job_id

    if (UserStore.isPartner()) {
      const account = JobStore.getAccountNameByJobId(job_id)

      if (account != null && account !== '') {
        options.account = account
      }
    }

    const service = ServiceStore.get(JobStore.get(job_id, 'service_code_id'), 'name')

    if (service != null) {
      options.service = service
    }

    if (!options.category) {
      options.category = 'Jobs'
    }

    if (job_id != null) {
      return this.track(name, options)
    }
  }
}

export default BaseTracking
