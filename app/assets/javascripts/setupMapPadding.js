/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// https://github.com/chuckhendo/fitBoundsPadding/blob/master/fitBoundsPadding.min.js
import Logger from 'lib/swoop/logger'

function setupMapPadding() {
  if (!window?.google) {
    setTimeout(setupMapPadding, 1000)
    return
  }

  if (!window?.google) {
    return
  }

  const oldFitBounds = window?.google?.maps?.Map.prototype.fitBounds

  window.google.maps.Map.prototype.fitBounds = function fitBounds(bounds, opts) {
    Logger.debug('FITTING Map opts: ', opts)

    if (opts) {
      const zoom = opts.zoom || this.getZoom()

      const map = this
      const scale = 2 ** zoom

      const _convertLatLngToPixel = function _convertLatLngToPixel(latlng) {
        const proj = map.getProjection()
        const point = proj.fromLatLngToPoint(latlng)
        return {
          x: point.x * scale,
          y: point.y * scale,
        }
      }

      const _convertPixelToLatLng = function _convertPixelToLatLng(pixel) {
        const proj = map.getProjection()
        const point = new window.google.maps.Point(pixel.x / scale, pixel.y / scale)
        return proj.fromPointToLatLng(point)
      }

      const _getPixelBounds = function _getPixelBounds(bounds, cb) {
        if (map.getProjection()) {
          const returnVal = {
            sw: _convertLatLngToPixel(bounds.getSouthWest()),
            ne: _convertLatLngToPixel(bounds.getNorthEast()),
          }
          cb(returnVal)
        } else {
          window.google.maps.event.addListener(map, 'projection_changed', () => {
            _getPixelBounds(bounds, cb)
          })
        }
      }

      const _extendBoundsByPaddingValue = function _extendBoundsByPaddingValue(bounds, opts) {
        _getPixelBounds(bounds, (pxbounds) => {
          Object.keys(opts).forEach((prop) => {
            switch (prop) {
              case 'left':
                pxbounds.sw.x -= opts.left
                break

              case 'top':
                pxbounds.ne.y -= opts.top
                break

              case 'right':
                pxbounds.ne.x += opts.right
                break

              case 'bottom':
                pxbounds.sw.y += opts.bottom
                break

              default:
                break
            }
          })

          oldFitBounds.call(
            map,
            new window.google.maps.LatLngBounds(
              _convertPixelToLatLng(pxbounds.sw),
              _convertPixelToLatLng(pxbounds.ne),
            ),
          )
        })
      }
      _extendBoundsByPaddingValue(bounds, opts)
    } else {
      oldFitBounds.call(this, bounds)
    }
  }
}

export default setupMapPadding
