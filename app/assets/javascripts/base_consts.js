import { union, zipObject } from 'lodash'

const Consts = {
  TERRITORIES: false, // Hiding [Territories Network Map] PoC only
  SPLITS: {
    GENERAL_SETTING_GEOFENCING: 'FE-generalSettingGeofencing',
    TERRITORIES_PARTNER_MAP: 'product_territoriesPartnerMap_ENG-10559',
    DISABLE_AUTO_ASSIGN_RADIUS: 'product_DisableAutoAssignRadiusInputs_ENG-11448',
    DISABLE_AUTO_ASSIGN_RADIUS_ON_PARTNERS: 'product_DisableAutoAssignRadius_Partners_ENG-11448',
    DISABLE_AUTO_ASSIGN_DETAILS: 'disable_autoAssignDetails_ENG-11680',
    DISABLE_MAP_ENDPOINT: 'disable_mapEndpoint_ENG-11814',
    DISABLE_NEW_FLEET_MAP: 'disable_new_fleet_map_ENG-12195',
    DISABLE_NEW_CHOOSE_PARTNER_MAP: 'disable_new_choose_partner_map_ENG-12196',
    DISABLE_NEW_PARTNER_FLEET_MAP: 'disable_new_partner_fleet_map_ENG-12198',
    DISABLE_NEW_JOB_DETAILS_MAP: 'disable_new_job_details_map_ENG-12199',
    DISABLE_NEW_JOB_FORM_MAP: 'disable_new_job_form_map_ENG-12200',
    DISABLE_NEW_GET_LOCATION_MAP: 'disable_new_get_location_map_ENG-12201',
    DISABLE_NEW_SHOW_ETA_MAP: 'disable_new_show_eta_map_ENG-12202',
    DISABLE_NEW_DRIVER_MAP_MODALS: 'disable_new_driver_map_modals_ENG-12203',
  },
  ROOT_PATH: '',
  API_PATH: '/api/v1/',
  LOGIN_PATH: 'login',
  WS_RESPONSE_TIMEOUT: 10000,
  // job status
  CREATED: 'Created',
  PREDRAFT: 'Predraft',
  DRAFT: 'Draft',
  PENDING: 'Pending',
  UNASSIGNED: 'Unassigned',
  ASSIGNED: 'Assigned',
  AUTO_ASSIGNING: 'Auto Assigning',
  ACCEPTED: 'Accepted',
  ETAEXTENDED: 'ETA Extended',
  REJECTED: 'Rejected',
  REASSIGN: 'Reassign',
  DISPATCHED: 'Dispatched',
  ENROUTE: 'En Route',
  SCHEDULED_FOR: 'Scheduled For',
  ONSITE: 'On Site',
  TOWING: 'Towing',
  TOWDESTINATION: 'Tow Destination',
  STORED: 'Stored',
  SUBMITTED: 'Submitted',
  COMPLETED: 'Completed',
  RELEASED: 'Released',
  CANCELED: 'Canceled',
  GOA: 'GOA',
  REASSIGNED: 'Reassigned',
  ETAREJECTED: 'ETA Rejected',
  EXPIRED: 'Expired',
  DELETED: 'Deleted',
  // not a status; a string to display
  SCHEDULED_JOB: 'Scheduled Job ',
  // auction status
  AUCTION_LIVE: 'live',
  AUCTION_SUCCESSFUL: 'successful',
  AUCTION_UNSUCCESSFUL: 'unsuccessful',
  AUCTION_CANCELED: 'canceled',
  // bid status
  BID_NEW: {
    // original state
    STATUS: 'new',
    FORMATTED: 'New',
  },
  BID_REQUESTED: {
    // bid created but partner has not interacted with it; auction is LIVE
    STATUS: 'requested',
    FORMATTED: 'Requested',
  },
  BID_SUBMITTED: {
    // a bid was submitted and awaiting auction end
    STATUS: 'submitted',
    FORMATTED: 'Submitted',
  },
  BID_EXPIRED: {
    // the auction finished before the partner submit the bid
    STATUS: 'expired',
    FORMATTED: 'Expired',
  },
  BID_PROVIDER_REJECTED: {
    // partner rejected the job
    STATUS: 'provider_rejected',
    FORMATTED: 'Provider Rejected',
  },
  BID_AUTO_REJECTED: {
    // the bid was received but the partner lost the auction
    STATUS: 'auto_rejected',
    FORMATTED: 'Rejected',
  },
  BID_WON: {
    // partner is the successful bidder!
    STATUS: 'won',
    FORMATTED: 'Won',
  },
  SITE_CANDIDATE: new Set(['AutoAssign::SiteCandidate', 'Job::Candidate::Site']),
  TRUCK_CANDIDATE: new Set(['AutoAssign::TruckCandidate', 'Job::Candidate::Truck']),
  FLEET_IN_HOUSE_JOB: 'FleetInHouseJob',
  FLEET_MANAGED_JOB: 'FleetManagedJob',
  // NOT A REAL STATUS
  SCHEDULED: 'Scheduled',
  SMS: {
    ETA: 'Eta',
    REVIEW_LINK: 'ReviewLink',
    ETA_UPDATE: 'ReviseEta',
    CONFIRMATION: 'Confirmation',
    CONFIRM_LOCATION: 'ConfirmLocation',
    REASSIGN: 'Reassign',
  },
  SOURCE_APPLICATION: {
    CMW: 'CMW',
    SMS: 'SMS',
  },
  ERROR: 'error',
  WARNING: 'warning',
  INFO: 'info',
  SUCCESS: 'success',
  SECRET_FIELD_DOTS: '•••••',
  BULLET: ' • ',
  HOURLY_P2P: 'HoursP2PRate',
  MILEAGE_TOWED: 'MilesTowedRate',
  MILEAGE_EN_ROUTE: 'MilesEnRouteRate',
  MILEAGE_EN_ROUTE_ONLY: 'MilesEnRouteOnlyRate',
  MILEAGE_P2P: 'MilesP2PRate',
  FLAT: 'FlatRate',
  STORAGE: 'StorageRate',
  TESLA_RATE: 'TeslaRate',
  PORT_TO_PORT_HOURS: 'Port To Port Hours',
  // Service Types
  ACCIDENT_TOW: 'Accident Tow',
  BASE_TRANSPORT: 'Transport',
  BATTERYJUMP: 'Battery Jump',
  DECKING: 'Decking',
  HEAVYHAUL: 'Heavy Haul',
  IMPOUND: 'Impound',
  LAW_ENFORCEMENT: 'Law Enforcement',
  LOANERWHEEL: 'Loaner Wheel',
  LOCKOUT: 'Lock Out',
  OTHER: 'Other',
  FUEL_DELIVERY: 'Fuel Delivery',
  RECOVERY: 'Recovery',
  SECONDARYTOW: 'Secondary Tow',
  TIRE_CHANGE: 'Tire Change',
  TOW: 'Tow',
  TOW_FAILED_BATTERY_BOOST: 'Tow - Failed Battery Boost',
  TOWIFJUMPFAILS: 'Tow if Jump Fails',
  TOWIFTIRECHANGEFAILS: 'Tow if Tire Change Fails',
  TRANSPORT: 'Transport',
  UNDECKING: 'Undecking',
  WINCHOUT: 'Winch Out',
  // Invoice statuses
  ICREATED: 'created',
  IPARTNERNEW: 'partner_new',
  IPARTNERAPPROVED: 'partner_approved',
  IPARTNERSENT: 'partner_sent',
  IPARTNERUNDOAPPROVAL: 'partner_undo_approval',
  IPARTNERMOVEBACKTONEW: 'partner_move_back_to_new',
  IPARTNERMARKUNPAID: 'partner_mark_unpaid',
  IPARTNERMARKSENT: 'partner_mark_sent',
  IFLEETAPPROVED: 'fleet_approved',
  IFLEETDOWNLOADED: 'fleet_downloaded',
  IFLEETUNDOAPPROVAL: 'fleet_undo_approval',
  IFLEETMOVEBACKFROMDOWNLOADED: 'fleet_move_back_from_downloaded',
  IFLEETREJECTED: 'fleet_rejected',
  IPARTNEROFFLINENEW: 'partner_offline_new',
  ISWOOPNEW: 'swoop_new',
  // ISWOOPAPPROVED: "swoop_approved"
  // ISWOOPSENT: "swoop_sent"
  ISWOOPSENDINGFLEET: 'swoop_sending_fleet',
  ISWOOPSENTFLEET: 'swoop_sent_fleet',
  ISWOOPDOWNLOADEDPARTNER: 'swoop_downloaded_partner',
  ISWOOPUNDOAPPROVALPARTNER: 'swoop_undo_approval_partner',
  ISWOOPUNDOAPPROVALFLEET: 'swoop_undo_approval_fleet',
  ISWOOPAPPROVEDFLEET: 'swoop_approved_fleet',
  ISWOOPAPPROVEDPARTNER: 'swoop_approved_partner',
  ISWOOPREJECTEDPARTNER: 'swoop_rejected_partner',
  IONSITEPAYMENT: 'on_site_payment',
  INVOICE: {
    ALREADY_BEEN_SENT:
      'This invoice has already been sent. Are you sure you want to move it back to the New tab?',
    CAN_BE_EDITED_AGAIN: 'It can be edited and sent again once moved back.',

    FLEET_OR_SWOOP_HAS_ALREADY_RECEIVED(recipient_name) {
      return `${recipient_name} has already received this invoice. Are you sure you want to move it back to the New tab?`
    },

    FLEET_OR_SWOOP_HAS_ALREADY_APPROVED(recipient_name) {
      return `This invoice cannot be moved back as ${recipient_name} has already approved it.`
    },

    FLEET_OR_SWOOP_HAS_ALREADY_PAID(recipient_name) {
      return `This invoice cannot be moved back as ${recipient_name} has already sent a payment for it.`
    },

    CONTACT_FLEET_OR_SWOOP(recipient_name) {
      return `If you need to make changes to it, please contact ${recipient_name} and ask them to undo the approval of the invoice.`
    },

    REMOVE_FROM_APPROVAL_QUEUE:
      'It will be removed from their approval queue, and it can be edited and sent again once moved back.',
    UNDO_APPROVAL_WARNING:
      'An associated invoice for a Fleet Managed account has already been approved. Unapproving this invoice will delete the Fleet Managed invoice. Would you like to proceed?',
    IT_CAN_BE_EDITED_AGAIN: 'It can be edited and approved again once moved back.',
    ALREADY_BEEN_DOWNLOADED:
      'This invoice has already been downloaded. Are you sure you want to move it back to the Approved tab?',
    WAS_PAID_ON_SITE: 'This invoice cannot be moved back as it has already been paid for on site.',
    ARE_YOU_SURE_TO_MARK_UNPAID:
      'This invoice was paid on site. Are you sure you want to mark unpaid and move it back to the New tab?',

    CANNOT_EDIT_AFTER_APPROVED(recipient_name) {
      return `Cannot edit the invoice after ${recipient_name} has approved it. Contact ${recipient_name} to undo approval of invoice.`
    },

    CANNOT_EDIT_AFTER_DOWNLOADED(recipient_name) {
      return `Cannot edit the invoice after ${recipient_name} has downloaded it. Contact ${recipient_name} to undo approval of invoice.`
    },
  },
  INVOICE_REJECTION: {
    CONFIRMATION:
      'Are you sure you wish to reject this invoice? You will still retain a record of the job in the Done tab and the partner will receive an email notifying them.',
  },
  DATE_FORMAT: 'MM/DD/YY h:mm A',
  SPLIT_KEY: process.env.SPLIT_KEY || null,
  JOBS_PER_PAGE_COUNT: process.env.JOBS_PER_PAGE_COUNT || 12,
  MAX_ACTIVE_JOB_LOAD_COUNT: process.env.MAX_ACTIVE_JOB_LOAD_COUNT || 400,
  REPORTING_JOBS_LOAD_COUNT: 100,
  COMPANY_LOAD_COUNT_AT_USER_LOGIN: 300,
  USER_MAX_LOAD_COUNT_AT_USER_LOGIN: 500,
  DEFAULT_PAGE_SIZE: 50,
  COMPANY_PAGE_SIZE: 50,
  PARTNER_PAGE_SIZE: 50,
  ACCOUNT_PAGE_SIZE: 50,
  USER_PAGE_SIZE: 50,
  N_TIME: 10000,
  JS_ENV: '',
  GEICO: 'Geico',
  ALLSTATE: 'Allstate',
  AGERO: 'Agero',
  ALLIED: 'Allied Dispatch Solutions',
  NATIONSAFE: 'Nation Safe Drivers',
  ROADAMERICA: 'Road America',
  QUEST: 'Quest',
  USAC: 'USAC',
  SWOOP: 'Swoop',
  SWOOP_TELEPHONE: '847-796-6763',
  TESLA_MOTORS: 'Tesla Motors Inc',
  TESLA: 'Tesla',
  TESLA_MOBILE_TIRE_SERVICE: 'Tesla Mobile Tire Service',
  // this is a partner that attends to Tesla exclusively
  TURO: 'Turo',
  ENTERPRISE_FLEET_MANAGEMENT: 'Enterprise Fleet Management',
  FARMERS: 'Farmers Insurance',
  FORD: 'Ford',
  LINCOLN: 'Lincoln',
  CENTURY: '21st Century Insurance',
  FLEET_RESPONSE: 'Fleet Response',
  STATES: [
    'Alabama',
    'Alaska',
    'American Samoa',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'District Of Columbia',
    'Federated States Of Micronesia',
    'Florida',
    'Georgia',
    'Guam',
    'Hawaii',
    'Idaho',
    'Illinois',
    'Indiana',
    'Iowa',
    'Kansas',
    'Kentucky',
    'Louisiana',
    'Maine',
    'Marshall Islands',
    'Maryland',
    'Massachusetts',
    'Michigan',
    'Minnesota',
    'Mississippi',
    'Missouri',
    'Montana',
    'Nebraska',
    'Nevada',
    'New Hampshire',
    'New Jersey',
    'New Mexico',
    'New York',
    'North Carolina',
    'North Dakota',
    'Northern Mariana Islands',
    'Ohio',
    'Oklahoma',
    'Oregon',
    'Palau',
    'Pennsylvania',
    'Puerto Rico',
    'Rhode Island',
    'South Carolina',
    'South Dakota',
    'Tennessee',
    'Texas',
    'Utah',
    'Vermont',
    'Virgin Islands',
    'Virginia',
    'Washington',
    'West Virginia',
    'Wisconsin',
    'Wyoming',
  ],
  STATE_ABREVS: [
    'AL',
    'AK',
    'AS',
    'AZ',
    'AR',
    'CA',
    'CO',
    'CT',
    'DE',
    'DC',
    'FM',
    'FL',
    'GA',
    'GU',
    'HI',
    'ID',
    'IL',
    'IN',
    'IA',
    'KS',
    'KY',
    'LA',
    'ME',
    'MH',
    'MD',
    'MA',
    'MI',
    'MN',
    'MS',
    'MO',
    'MT',
    'NE',
    'NV',
    'NH',
    'NJ',
    'NM',
    'NY',
    'NC',
    'ND',
    'MP',
    'OH',
    'OK',
    'OR',
    'PW',
    'PA',
    'PR',
    'RI',
    'SC',
    'SD',
    'TN',
    'TX',
    'UT',
    'VT',
    'VI',
    'VA',
    'WA',
    'WV',
    'WI',
    'WY',
  ],
  CASHCALL: 'Cash Call',
  CUSTOMERPAY: 'Customer Pay',
  ANSWERS_YES: 'Yes',
  ANSWERS_NO: 'No',
  QUESTION_KEY_LOCKED_IN: 'locked-in',
  QUESTION_CUSTOMER_SAFE: 'Customer in a safe location?',
  DISTANCE_ROUND_NEAREST_TENTH: 'Nearest Tenth',
  DISTANCE_ROUND_NEAREST: 'Round Nearest',
  DISTANCE_ROUND_UP: 'Round Up',
  ADDITION_LOANER_WHEEL: 'Loaner Wheel Pickup',
  ADDITION_PRIORITY_RESPONSE: 'Priority Response',
  ADDITION_TOLL: 'Toll',
  ADDITION_OVERNIGHT_STORAGE: 'Overnight Storage',
  ADDITION_REIMBURSEMENT: 'Reimbursement',
  ADDITION_DOLLY: 'Dolly',
  ADDITION_GOJAK: 'GoJak',
  ADDITION_TIRE_SKATES: 'Tire Skates',
  ADDITION_WAIT_TIME: 'Wait Time',
  CUSTOMER_TYPE_WARRANTY: 'Warranty',
  CUSTOMER_TYPE_GOODWILL: 'Goodwill',
  CUSTOMER_TYPE_CUSTOMER_PAY: 'Customer Pay',
  CUSTOMER_TYPE_PCARD: 'P Card',
  CUSTOMER_TYPE_MECHANICAL: 'Mechanical',
  CUSTOMER_TYPE_SERVICE_CENTER: 'Service Center',
  CUSTOMER_TYPE_AUCTION: 'Auction',
  CUSTOMER_TYPE_DAMAGE: 'Claim/Damage',
  CUSTOMER_TYPE_BODY_SHOP: 'Body Shop',
  CUSTOMER_TYPE_TAX_EXEMPT: 'Tax Exempt',
  CUSTOMER_TYPE_BRANCH: 'Branch',
  DEPARTMENT_ROADSIDE: 'Roadside',
  DEPARTMENT_AUS_NZ_ROADSIDE: 'AUS/NZ Roadside',
  FEATURES_AUTO_ASSIGN: 'Auto Assign',
  FEATURES_AUTO_SELECT_SITE_FROM_LOOKUP: 'Auto Select Site from Lookup',
  FEATURES_HEAVY: 'Heavy Duty Equipment',
  FEATURES_JOB_DETAILS_RENTAL_CAR: 'Job Details Rental Car',
  FEATURES_JOB_FORM_SITE_REQUIRED: 'Job Form Site Required',
  FEATURES_JOB_FORM_UNIT_REQUIRED: 'Job Form Unit Required',
  FEATURES_SERIAL: 'Serial Number',
  FEATURES_TRAILER: 'Trailers',
  FEATURES_DEPARTMENTS: 'Departments',
  FEATURES_FLEET_SITES: 'Client Sites',
  FEATURES_PAYMENT_TYPE: 'Payment Type',
  FEATURE_ENABLE_AUTO_DISPATCH_TO_TRUCK: 'Partner Dispatch to Truck',
  FEATURES_TESLA_NEW_JOB: 'Tesla New Job',
  FEATURES_SMS_REVIEWS: 'SMS_Reviews',
  FEATURES_REVIEW_NPS_SURVEY: 'Review NPS Survey',
  FEATURES_ISSUES: 'Issues',
  FEATURES_UNIT_NUMBER: 'Job Unit Number',
  FEATURES_REF_NUMBER: 'Job Reference Number',
  FEATURES_SHOW_DRIVER_INVOICE: 'Show Drivers Invoice',
  FEATURES_AUTOFILL_VIN: 'Autofill VIN',
  FEATURES_VIN_FIELD_FIRST: 'VIN Field First',
  FEATURES_ISSC_NETWORK_STATUS: 'ISSC Network Status',
  FEATURES_MC_JOB_AUTOREJECT_OVERRIDE: 'MC Job AutoReject Override',
  FEATURES_ADD_PARTNER_COLUMN_TO_FLEET_MANAGED: 'Add Partner Column to Fleet Managed',
  FEATURES_QUESTIONS: 'Questions',
  FEATURES_DISABLE_FORCE_PHOTOS: 'Disable Force Photos',
  FEATURES_NOT_REQUIRE_CUSTOMER_INFO: "Don't require customer name and phone",
  FEATURE_DISABLE_APP_CREATE: 'Disable App Create Jobs',
  FEATURE_DISABLE_APP_EDIT: 'Disable App Edit Jobs',
  FEATURE_DISABLE_APP_DRIVER_CREATE: 'Disable App Drivers Create Jobs',
  FEATURE_DISABLE_APP_DRIVER_EDIT: 'Disable App Drivers Edit Jobs',
  FEATURES_DISABLE_DELETE_JOB: 'Disable Delete Job',
  FEATURE_LOOKER: 'Looker',
  FEATURE_DISABLE_STRIPE_INTEGRATION: 'Disable Stripe Integration',
  FEATURE_DISABLE_MOBILE_VIN_LOOKUP: 'Disable Mobile VIN Lookup',
  FEATURE_DISABLE_BANK_SETTINGS: 'Disable Bank Settings',
  FEATURE_DISABLE_DRIVER_VIEW_INVOICE: 'Disable driver view invoice',
  FEATURE_DISABLE_DRIVER_EDIT_INVOICE: 'Disable driver edit invoice',
  FEATURE_QUICKBOOK: 'Quickbooks',
  FEATURE_FLEET_SERVICE_MAP: 'Fleet Service Map',
  FEATURE_CHOOSE_PARTNER_MAP: 'Choose Partner Map',
  FEATURE_OPTIONAL_CREDIT_CARD_ZIP: 'Optional Credit Card Zip',
  FEATURE_JOB_FORM_CUSTOMER_LOOKUP: 'Job Form Customer Lookup',
  FEATURE_JOB_FORM_COVERAGE: 'Job Form Coverage',
  FEATURE_JOB_FORM_RECOMMENDED_DROP_OFF: 'Job Form Recommended Drop Off',
  FEATURE_STORAGE: 'Storage',
  FEATURE_SYMPTOMS: 'Show Symptoms',
  FEATURE_HIDE_TIMESTAMPS_FOR_ACCOUNT: 'Remove invoice timestamps per account',
  FEATURE_INVOICE_CUSTOM_ITEMS: 'Invoice Custom Items',
  FEATURES_SHOW_CLIENT_SITES_TO_SWOOP: 'Show Client Sites to Swoop',
  FEATURE_SHOW_ETA_ON_MOBILE_DETAILS: 'Show ETA on mobile job details',
  FEATURE_CLASS_TYPE_REQUIRED: 'Class Type Required',
  FEATURE_DEPARTMENT_FILTER: 'Department Filter',
  FEATURE_REVIEW_FEED: 'Review Feed',
  FEATURE_REVIEW_FEED_Q1_FILTER: 'Reviews Feed Q1 Filter',
  FEATURE_CONFIRM_JOB_DETAILS: 'Confirm Job Details',
  FEATURE_FLEET_CLASS_TYPE: 'Fleet Class Type',
  FEATURE_DISABLE_FORCE_SERVICE_RESOLUTION: 'Disable force service resolution',
  FEATURE_MEMBER_NUMBER: 'Job Member Number',
  FEATURE_DISABLE_LICENSE_RESTRICTIONS: 'Disable License Restrictions',
  FEATURE_INTERNATIONAL_PHONE_NUMBERS: 'International Phone Numbers',

  CUSTOMER: 'Customer',

  ISSC_REGISTERED_STATUSES: ['registered', 'logged_in', 'logged_out', 'logging_in'],
  MESSAGE_AUTH_ERR: 'An authentication error occured and your last request did not go through',
  MESSAGE_PAYMENT_TYPE_TBD:
    'Payment Type is currently "TBD". Please update to another option before assigning a Partner.',

  MESSAGE_NEED_ONE_DISPATCH_SITE(context) {
    return `${context.site_name} is the only digital dispatch site for ${
      context.company_name
    }. Please enable an additional digital dispatch site for ${
      context.company_name
    } if you'd like to turn off ${context.site_name}.`
  },

  MESSAGE_NEED_SITE_ENABLED: 'Must have at least 1 Site enabled for this account.',
  MESSAGE_SELECT_ISSUE: 'Select an issue and reason to add it to the Job Detail screen.',
  MESSAGE_LOCKED_IN: 'Advise customer to call 911 to ensure the child or pet is out of danger.',
  MESSAGE_AUTOFILL_VIN: 'Valid VIN Autofills Vehicle',
  MESSAGE_INVALID_PHONE: 'Invalid Phone',
  MESSAGE_PARTNER_DISPATCH_NUMBER: 'Partner Dispatch Number',
  MESSAGE_ADD_ADDITIONAL_SITE: 'Add Additional Site',

  MESSAGE_EDIT(name) {
    return `Edit ${name}`
  },

  MESSAGE_CREATE(name) {
    return `Create New ${name}`
  },

  COMPANY_SETTINGS: {
    POLICY_LOOKUP_SERVICE: {
      FIELDS_REQUIRED_FOR_COVERAGE: 'Policy Lookup Service fields required for Coverage',
      RUN_COVERAGE_ON_NEW_JOB_FORM_LOAD: 'Run Coverage on New Job Form load',
      CUSTOMER_LOOKUP_TYPES: 'Policy Lookup Service lookup types',
    },
    RUN_COVERAGE_MANUAL: {
      RUN_COVERAGE_ON_NEW_JOB_FORM_LOAD: 'Run Coverage on New Job Form load',
    },
  },

  TRANSACTION_PAYMENT: 'Payment',
  TRANSACTION_DISCOUNT: 'Discount',
  TRANSACTION_REFUND: 'Refund',
  TRANSACTION_WRITEOFF: 'Write Off',
  PAYMENT_CANNOT_BE_ZERO: 'Payment cannot be $0.00',
  SYMPTOM_FLAT_TIRE: 'Flat tire',
  SETTTING_DISABLE_JOB_ALERTS: 'Disable Job Alerts',
  SETTING_QB_IGNORE_CANCELED: 'QB Ignore Canceled Invoices',
  SETTING_QB_SYNC_PAYMENTS: 'QB Sync payments',
  SETTING_DISABLE_NOTIFICATIONS: 'DISABLE_NOTIFICATIONS',

  USER_NOTIFICATION_SETTINGS: {
    DISPATCHED_TO_DRIVER: 'dispatched_to_driver',
  },
  // user password request
  PASSWORD_REQUEST: {
    USERNAME: 'username',
    EMAIL_ADDRESS: 'email address',

    EMAIL_OR_USERNAME_NOT_RECOGNIZED(type) {
      return `Sorry, we didn't recognize that ${type}. Please double-check and try again.`
    },

    HELP: 'For help, contact your team admin or Swoop support.',
    ENTER_USERNAME_OR_PASSWORD:
      "Enter your username or email and we'll send you a link to reset your password.",
    NO_CONTACT_CHANNEL: 'Oops! Your account does not have an e-mail address or phone number.',
    CONTACT_YOUR_TEAM: 'Contact your team admin to reset your password.',

    SUCCESS(method, location) {
      return `Success! A link to reset your Swoop password has been ${method} ${location}`
    },

    EMAILED_TO: ' emailed to ',
    TEXTED_TO: ' texted to ',
    FAIL: 'Oops! Sorry, but an error has occurred. Please try again later.',
  },
  // user change password
  CHANGE_PASSWORD: {
    UUID_NOT_FOUND: 'Oops! This link is not valid.',
    INVALID_PASSWORD: 'Oops! Password must be at least 8 characters.',
    PASSWORDS_DONT_MATCH: 'Oops! The passwords must match.',
    PASSWORD_PLACEHOLDER: '8 characters minimum',
    CONFIRMATION_PLACEHOLDER: 'Must match',
  },

  COVERAGE: {
    CLIENT_API_COVERED: 'Client API Covered',
  },

  // Partner Search
  PARTNER_SEARCH: {
    TITLE: 'Add Partner',
    BUTTON: {
      ADD_PARTNER: 'Add New Partner',
      SELECT_SITE: 'Add',
    },
    NO_RESULTS: 'No results for that dispatch number.',
    CREATE_NEW: 'Click Add New Partner to create a new partner company.',
    ENTER_NUMBER_LONG:
      "Enter the dispatch phone number for the partner you'd like to add.  We'll attempt to pull their details.",
    ENTER_NUMBER: 'Enter the dispatch number to view results',
    SEARCH: 'Search',
    ERROR: {
      BAD_NUMBER: 'Enter full 10-digit phone number to view results',
      SERVER_ERROR: 'Problem looking up phone',
    },
  },
  ADDITIONAL_SITE: {
    TITLE: 'Add Additional Site',
    BUTTONS: {
      ADD_SITE: 'Add Site',
      CREATE_SITE: 'Create New Site',
    },

    COMPANY_HAS_SITES(company_name) {
      return `${company_name} has sites that you have not added yet.  Select a site to add it to your account`
    },
  },
  TYPE_LOCATION_IMAGE: 'AttachedDocument::LocationImage',
  TYPE_ATTACHED_DOCUMENT: 'AttachedDocument::Document',
  TYPE_SIGNATURE_IMAGE: 'AttachedDocument::SignatureImage',
  DIGITAL_DISPATCH: {
    RSC: 'rsc',
    ISSC: 'issc',
  },
  JOB: {
    TYPE: {
      RESCUE: 'RescueJob',
      MOTORCLUB: 'FleetMotorClubJob',
    },

    SELECT_NEW_PARTNER(fleet_company_name) {
      return `Please select the new Partner to assign this job to. The job will remain in the done tab for ${fleet_company_name} and be place in the Done tab for the new partner.`
    },

    EDIT_PAYMENT_TYPE:
      'Are you sure you want to edit the Payment Type? The Invoice will be moved to the Partner New tab on the Invoices page to review and approve.',
    MANAGE_STATUS_WARN: 'Are you sure you would like to manage the status for this job?',

    BILLING_TYPE: {
      VCC: 'VCC',
      ACH: 'ACH',
    },
  },
  POI_SITE: {
    HELPER_TEXT:
      'Places are common points of interest that are frequently used as Pickup or Drop Off locations.',
  },
  COMMISSION_PERCENTAGE: {
    HELPER_TEXT: 'Commission percentage will be used to calculate driver commissions',
    SETTING_TEXT:
      'These settings define how driver commissions will be calculated in the Commissions Report',

    NON_COMMISSIONABLE_MODAL_TEXT(serviceOrAddonItem) {
      return `Select the ${serviceOrAddonItem} that should not be included when calculating driver commissions`
    },
  },
  STRIPE_BANK_ACCOUNT: {
    LINK: 'Link your bank account in order to receive payments through Swoop',
    FINISH_LINKING: 'Add a bank account to complete your setup',
  },
  STRIPE_BUSINESS_AND_PERSONAL_INFO: {
    HELPER_TEXT:
      'We need to verify a few details so that you can begin accepting payments. Swoop takes your privacy seriously, and will never expose this information.',
  },
  STRIPE_BANK_INFO: {
    HELPER_TEXT: 'Link your bank account to accept payments directly through Swoop.',
  },
  SITE: {
    HELPER_TEXT:
      'Sites are company locations that can be linked to jobs and can act as Pickup or Drop Off locations.',
  },
  CONTACT_INFO:
    'Questions? Chat us in the blue bubble, or reach us at support@joinswoop.com or (866) 219-8136',
  CONTACT_URL: 'https://info.agero.com/network',
  TAB: {
    SENT: 'sent',
    PAID: 'paid',
  },
  MOBILE: {
    LOCATION_REQUEST_MESSAGE:
      'Please confirm your location so we know where to send help. Tap below to allow us to access your location.',
    CONFIRM_LOCATION: 'Confirm Location',
    LOCATION_NEEDED: 'Location Needed',
    SEND_LOCATION: 'Send Location',

    LOCATION_WILL_BE_SEND(companyName) {
      return `Location will be sent to ${companyName}`
    },

    SERVICE_LOCATION: 'Service Location',
    FINDING_LOCATION: 'Finding Location...',
    THANK_YOU: 'Thank You',

    ADDRESS_SHARED: address => `Address Shared: ${address}`,
    CHANGE_ADDRESS: 'Change Address',
    EN_ROUTE_MESSAGE: 'Your provider will be en route shortly.',
    SERVICE_COMPLETED: 'Service Completed',
    PLEASE_CONTACT: 'If you have any questions, please call:',
  },
  CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS: {
    MANUAL: 'Skip and enter manually',
    NAME_AND_ZIP: 'Last Name & Zip',
    PHONE_NUMBER: 'Phone Number',
    POLICY_NUMBER: 'Policy Number',
    UNIT_NUMBER: 'Unit Number',
    VIN: 'Last 8 Vin',
  },
  CUSTOMER_LOOKUP_RETRY: {
    UNIT_NUMBER: 'Try Unit Number',
    LAST_NAME_AND_ZIP: 'Try Last Name & Zip',
    VIN: 'Try VIN',
  },
  CONTACT_NUMBER: '866-219-8136',
  SWOOP_CONTACT_NUMBER: '(866) 219-8136',
  ACCEPT_BEST_ETA_BASED_ON: 'Based on live location, traffic, and availability of Swoop Network',
  LOADING: 'loading...',
  UTM_KEYS: ['campaign', 'source', 'medium', 'term', 'content'],
  USER_SETTING: {
    FILTERED_STORAGE_TYPES: 'filtered_storage_types',
    FILTERED_STORAGE_SITES: 'filtered_storage_sites',
    FILTERED_DISPATCHERS: 'filtered_dispatchers',
    FILTERED_STATES: 'filtered_states',
    FILTERED_FLEET_COMPANIES_FOR_JOBS: 'filtered_fleet_companies_for_jobs',
    FILTERED_FLEET_COMPANIES_FOR_REVIEWS: 'filtered_fleet_companies_for_reviews',
    FILTERED_DEPARTMENTS: 'filtered_departments',
    FILTERED_DISPATCH_SITES: 'filtered_dispatch_sites',
    FILTERED_FLEET_SITES: 'filtered_fleet_sites',
    FILTERED_SERVICES: 'filtered_services',
    FILTERED_INVOICE_STATUS: 'filtered_invoice_status',
    FILTERED_REVIEWS: 'filtered_reviews',
    ACTIVE_TAB: 'active_tab',
    DONE_TAB: 'done_tab',
    LAST_SITE_ID_ON_JOB_FORM: 'last_site_id_on_job_form',
    INVOICES_SORTBY: 'invoices_sortby',
    DONE_JOBS_SORTBY: 'doneJobs_sortby',
    COMPANIES_SORTBY: 'companies_sortby',
    USERS_SORTBY: 'users_sortby',
    PARTNER_SORTBY: 'partners_sortby',
    ACCOUNT_SORTBY: 'accounts_sortby',
    SHOW_OFF_DUTY_ASSIGN_DRIVER_MAP: 'show_off_duty_assign_driver_map',
    SHOW_OFF_DUTY_PARTNER_FLEET_MAP: 'show_off_duty_partner_fleet_map',
    SHOW_ROUTES_PARTNER_FLEET_MAP: 'show_routes_partner_fleet_map',
    SHOW_WITHOUT_DRIVERS_PARTNER_FLEET_MAP: 'show_without_drivers_partner_fleet_map',
  },
  SITES_AND_PLACES_LOAD_THRESHOLD: 100,
  JOB_SEARCH_ATTRIBUTES: [
    'customer',
    'pickup_contact',
    'dropoff_contact',
    'service_location',
    'drop_location',
    'owner_company',
    'rescue_company',
    'id',
    'status',
    'email',
    'root_dispatcher.first_name',
    'root_dispatcher.last_name',
    'root_dispatcher.full_name',
    'dispatcher.first_name',
    'dispatcher.last_name',
    'dispatcher.full_name',
    'service',
    'make',
    'model',
    'year',
    'color',
    'vin',
    'license',
    'notes',
    'partner_notes',
    'dispatch_noters',
    'fleet_notes',
    'driver_notes',
    'swoop_notes',
    'account.name',
    'po_number',
    'unit_number',
    'ref_number',
    'location_type',
    'dropoff_location_type',
    'symptom',
    'first_name',
    'last_name',
    'phone',
    'policy_number',
    'customized_rescue_company_name',
    'pcc_claim_number',
    'serial_number',
  ],
  GENERIC_MARKER_PATH:
    'M19,11.8894533 C19,10.2253484 18.4140625,8.80457971 17.2421875,7.62714706 C16.0703125,6.44971441 14.65625,5.86099809 13,5.86099809 C11.34375,5.86099809 9.9296875,6.44971441 8.7578125,7.62714706 C7.5859375,8.80457971 7,10.2253484 7,11.8894533 C7,13.5535581 7.5859375,14.9743268 8.7578125,16.1517595 C9.9296875,17.3291921 11.34375,17.9179084 13,17.9179084 C14.65625,17.9179084 16.0703125,17.3291921 17.2421875,16.1517595 C18.4140625,14.9743268 19,13.5535581 19,11.8894533 Z M26,11.7219962 C26,19.4141201 13.609375,37 13,37 C12.390625,37 0,19.4141201 0,11.7219962 C1.39228677e-16,8.48623681 1.26953125,5.72363094 3.80859375,3.43417857 C6.34765625,1.14472619 9.41145833,0 13,0 C16.5885417,0 19.6523438,1.14472619 22.1914063,3.43417857 C24.7304688,5.72363094 26,8.48623681 26,11.7219962 Z',
  USER_STORE: {
    RECEIVED_USER: 'receivedUser',
    RECEIVED_API_KEY: 'receivedApiKey',
    RECEIVED_LOGIN: 'receivedLoginInfo',
    CHANGE: 'change',
    USERME_CHANGED: 'user_me_changed',
    LOGIN: 'login',
    LOGOUT: 'logout',
    REFRESH_TOKEN: 'refreshToken',
    READY: 'ready',
    FORGOT_PASSWORD: 'forgotPassword',
    NEW_PASSWORD_REQUEST: 'newPasswordRequest',
    FORGOT_PASSWORD_SUCCESS: 'forgotPasswordSuccess',
    USER_NOT_FOUND: 'userNotFound',
    PASSWORD_REQUEST_FAIL: 'passwordRequestFail',
    VALIDATE_PASSWORD_REQUEST: 'validatePasswordRequest',
    PASSWORD_REQUEST_IS_NOT_VALID: 'passwordRequestIsNotValid',
    PASSWORD_REQUEST_IS_VALID: 'passwordRequestIsValid',
    CHANGE_PASSWORD: 'changePassword',
    INVALID_PASSWORD: 'invalidPassword',
    CHANGE_PASSWORD_FAIL: 'changePasswordFail',
  },
}
Consts.RATE_TYPE_MAP = {}
Consts.RATE_TYPE_MAP[Consts.HOURLY_P2P] = 'Hourly (Port to Port)'
Consts.RATE_TYPE_MAP[Consts.MILEAGE_TOWED] = 'Mileage (Towed)'
Consts.RATE_TYPE_MAP[Consts.MILEAGE_EN_ROUTE_ONLY] = 'Mileage (En Route)'
Consts.RATE_TYPE_MAP[Consts.MILEAGE_EN_ROUTE] = 'Mileage (En Route + Towed)'
Consts.RATE_TYPE_MAP[Consts.MILEAGE_P2P] = 'Mileage (Port to Port)'
Consts.RATE_TYPE_MAP[Consts.FLAT] = 'Flat'
Consts.RATE_TYPE_MAP[Consts.STORAGE] = 'Storage'
Consts.RATE_TYPE_MAP[Consts.TESLA_RATE] = 'Tesla'
Consts.PARTNER_INVOICE_SENT_STATES = [
  Consts.IPARTNERSENT,
  Consts.IFLEETAPPROVED,
  Consts.IFLEETDOWNLOADED,
  Consts.IPARTNEROFFLINENEW,
  Consts.ISWOOPNEW,
  Consts.ISWOOPDOWNLOADEDPARTNER,
  Consts.ISWOOPAPPROVEDPARTNER,
]
Consts.PARTNER_INVOICE_APPROVED_AND_SENT_STATES = union(
  [Consts.IPARTNERAPPROVED],
  Consts.PARTNER_INVOICE_SENT_STATES,
)
Consts.TRANSACTIONS = [
  Consts.TRANSACTION_PAYMENT,
  Consts.TRANSACTION_DISCOUNT,
  Consts.TRANSACTION_REFUND,
  Consts.TRANSACTION_WRITEOFF,
]
Consts.TRANSACTIONS_NEGATIVE = [Consts.TRANSACTION_REFUND]
Consts.TRANSACTIONS_POSITIVE = [
  Consts.TRANSACTION_PAYMENT,
  Consts.TRANSACTION_WRITEOFF,
  Consts.TRANSACTION_DISCOUNT,
]
Consts.TRANSACTIONS_WITH_METHOD = [Consts.TRANSACTION_PAYMENT, Consts.TRANSACTION_REFUND]
Consts.PAYOUT_INTERVAL = [
  {
    id: 'daily',
    name: 'Daily',
  },
  {
    id: 'weekly',
    name: 'Weekly (Friday)',
  },
  {
    id: 'monthly',
    name: 'Monthly (1st of Month)',
  },
]
Consts.SUBSCRIPTION_STATUS = {
  1: 'Network Only',
  2: 'Free Trial',
  3: 'Subscribed - Essential',
  4: 'Subscribed - Premium',
  5: 'Subscribed - Professional',
  6: 'Subscribed - Enterprise',
  7: 'Subscribed - Free',
  8: 'Subscribed - Custom',
  9: 'Inactive',
  10: 'Pending Cancelation',
}

Consts.TESLA_ALLOWED_ADDONS = [
  Consts.ADDITION_PRIORITY_RESPONSE,
  Consts.ADDITION_REIMBURSEMENT,
  Consts.ADDITION_OVERNIGHT_STORAGE,
  Consts.ADDITION_DOLLY,
  Consts.ADDITION_GOJAK,
  Consts.ADDITION_TIRE_SKATES,
  Consts.ADDITION_WAIT_TIME,
]
Consts.TESLA_ADDITIONS = [
  Consts.ADDITION_OVERNIGHT_STORAGE,
  Consts.ADDITION_TOLL,
  Consts.ADDITION_PRIORITY_RESPONSE,
  Consts.ADDITION_LOANER_WHEEL,
]
Consts.MOTORCLUBS = [
  Consts.GEICO,
  Consts.AGERO,
  Consts.ALLSTATE,
  Consts.ALLIED,
  Consts.NATIONSAFE,
  Consts.TESLA_MOTORS,
  Consts.ROADAMERICA,
  Consts.QUEST,
  Consts.USAC,
]
Consts.ISSUES_CLAIM_VALUE = '5'
Consts.ISSUES_DAMAGE_VALUE = '34'

Consts.DISABLED_MOTORCLUBS = [
  Consts.GEICO,
  Consts.ALLSTATE,
  Consts.ALLIED,
  Consts.ROADAMERICA,
]

Consts.FLEETS = [
  Consts.SWOOP,
  Consts.TESLA,
  Consts.TURO,
  'Enterprise',
  'FlightCar',
  Consts.FARMERS,
  Consts.CENTURY,
  'Silvercar',
  'SpoonRocket',
  'Getaround',
  'Metromile',
]
Consts.JOB_STATUS_ORDER = [
  Consts.CREATED,
  Consts.DRAFT,
  Consts.PENDING,
  Consts.DISPATCHED,
  Consts.ENROUTE,
  Consts.ONSITE,
  Consts.TOWING,
  Consts.TOWDESTINATION,
  Consts.COMPLETED,
  Consts.STORED,
  Consts.CANCELED,
  Consts.GOA,
]
Consts.ALL_STATUSES = [
  Consts.CREATED,
  Consts.DRAFT,
  Consts.PENDING,
  Consts.UNASSIGNED,
  Consts.ASSIGNED,
  Consts.AUTO_ASSIGNING,
  Consts.ACCEPTED,
  Consts.REJECTED,
  Consts.REASSIGN,
  Consts.DISPATCHED,
  Consts.ENROUTE,
  Consts.ONSITE,
  Consts.TOWING,
  Consts.TOWDESTINATION,
  Consts.STORED,
  Consts.SUBMITTED,
  Consts.COMPLETED,
  Consts.RELEASED,
  Consts.CANCELED,
  Consts.GOA,
  Consts.REASSIGNED,
  Consts.ETAREJECTED,
  Consts.EXPIRED,
]
Consts.ORDERED_STATUSES = [
  Consts.CREATED,
  Consts.CANCELED,
  Consts.GOA,
  Consts.DRAFT,
  Consts.PENDING,
  Consts.REJECTED,
  Consts.UNASSIGNED,
  Consts.ETAREJECTED,
  Consts.REASSIGNED,
  Consts.REASSIGN,
  Consts.ASSIGNED,
  Consts.ACCEPTED,
  Consts.DISPATCHED,
  Consts.ENROUTE,
  Consts.ONSITE,
  Consts.TOWING,
  Consts.TOWDESTINATION,
  Consts.COMPLETED,
]
Consts.ACTIVE_STATUSES = [
  Consts.CREATED,
  Consts.DRAFT,
  Consts.PENDING,
  Consts.UNASSIGNED,
  Consts.ASSIGNED,
  Consts.AUTO_ASSIGNING,
  Consts.ACCEPTED,
  Consts.SUBMITTED,
  Consts.REASSIGN,
  Consts.DISPATCHED,
  Consts.ENROUTE,
  Consts.ONSITE,
  Consts.TOWING,
  Consts.TOWDESTINATION,
]
Consts.SCHEDULED_STATUSES = [
  Consts.CREATED,
  Consts.PENDING,
  Consts.UNASSIGNED,
  Consts.ASSIGNED,
  Consts.AUTO_ASSIGNING,
  Consts.ACCEPTED,
  Consts.SUBMITTED,
  Consts.REASSIGN,
]
Consts.DISPATCHED_STATUSES = [
  Consts.DISPATCHED,
  Consts.ENROUTE,
  Consts.ONSITE,
  Consts.TOWING,
  Consts.TOWDESTINATION,
]
Consts.MONITORING_STATUSES = [
  Consts.ONSITE,
  Consts.TOWING,
  Consts.TOWDESTINATION,
]
Consts.DISPATCHED_STATUSES_AND_ASSIGNED = [
  Consts.ASSIGNED,
  Consts.DISPATCHED,
  Consts.ENROUTE,
  Consts.ONSITE,
  Consts.TOWING,
  Consts.TOWDESTINATION,
]
Consts.ALL_BID_STATUSES = [
  Consts.BID_REQUESTED,
  Consts.BID_EXPIRED,
  Consts.BID_PROVIDER_REJECTED,
  Consts.BID_AUTO_REJECTED,
  Consts.BID_WON,
  Consts.BID_SUBMITTED,
]
Consts.DONE_STATUSES = [
  Consts.REJECTED,
  Consts.ETAREJECTED,
  Consts.CANCELED,
  Consts.GOA,
  Consts.COMPLETED,
  Consts.REASSIGNED,
  Consts.STORED,
  Consts.RELEASED,
]
Consts.FLEET_DONE_STATUSES = [
  Consts.CANCELED,
  Consts.GOA,
  Consts.COMPLETED,
  Consts.STORED,
  Consts.RELEASED,
  Consts.REASSIGNED,
]
Consts.PENDING_STATUSES = [
  Consts.CREATED,
  Consts.DRAFT,
  Consts.PENDING,
  Consts.UNASSIGNED,
  Consts.ASSIGNED,
  Consts.AUTO_ASSIGNING,
  Consts.ACCEPTED,
  Consts.SUBMITTED,
  Consts.REASSIGN,
]
Consts.FORCE_ONSITE_AFTER = [
  Consts.DISPATCHED,
  Consts.ACCEPTED,
  Consts.SUBMITTED,
  Consts.AUTO_ASSIGNING,
  Consts.ASSIGNED,
  Consts.PENDING,
  Consts.DRAFT,
  Consts.PREDRAFT,
  Consts.CREATED,
]
Consts.STATE_MAP = zipObject(Consts.STATE_ABREVS, Consts.STATES)
Consts.STANDARD_TIMEZONES = [
  'Europe/Berlin',
  'US/Arizona',
  'US/Central',
  'US/East-Indiana',
  'US/Eastern',
  'US/Hawaii',
  'US/Michigan',
  'US/Mountain',
  'US/Pacific',
]
Consts.TOW_SERVICES = [
  Consts.IMPOUND,
  Consts.DECKING,
  Consts.SECONDARYTOW,
  Consts.TOW,
  Consts.TOWIFJUMPFAILS,
  Consts.UNDECKING,
]

Consts.SERVICES_WITHOUT_DROPOFF = [Consts.BATTERYJUMP, Consts.FUEL_DELIVERY, Consts.TIRE_CHANGE, Consts.LOCKOUT, Consts.WINCHOUT]

Consts.COMPLETED_STATUSES = [
  Consts.GOA, Consts.EXPIRED, Consts.COMPLETED, Consts.CANCELED, Consts.STORED, Consts.RELEASED,
]

Consts.SERVICES_THAT_ENABLE_STORAGE = [
  Consts.ACCIDENT_TOW,
  Consts.DECKING,
  Consts.HEAVYHAUL,
  Consts.IMPOUND,
  Consts.LAW_ENFORCEMENT,
  Consts.RECOVERY,
  Consts.SECONDARYTOW,
  Consts.TOW,
  Consts.TRANSPORT,
  Consts.UNDECKING,
  Consts.WINCHOUT,
]

Consts.DRIVER_ALERT_AFTER = 15.5 // 15.5 minutes
Consts.ON_SITE_POPUP_THRESHOLD_IN_MINUTES = 3

export default Consts
