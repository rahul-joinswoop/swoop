import SearchStore from 'stores/search_store'
import UserStore from 'stores/user_store'
import SiteStore from 'stores/site_store'
import { filter, findIndex, map, reduce, sortBy, union, values, zipObject } from 'lodash'
import EventRegistry from 'EventRegistry'
import Consts from 'consts'
import Utils from 'utils'
import Api from 'api'
// LIST_ALL constant is already being used for a more abstract concept
// LIST_COMPANIES constant will only be triggered once the collection is all loaded for Swoop
const LIST_COMPANIES = 'list_companies'
const LIST_SUB_COMPANIES = 'list_sub_companies'
const LIST_FLEETS = 'list_fleets'
const LIST_MANAGED_FLEETS = 'list_managed_fleets'
const LIST_IN_HOUSE_FLEETS = 'list_in_house_fleets'
const LIST_RESCUE_COMPANIES = 'list_rescue_companies'

const FLEET_COMPANY = 'FleetCompany'
const RESCUE_COMPANY = 'RescueCompany'

const FLEET_IDS = '_fleetIds'
const MANAGED_FLEET_IDS = '_managedFleetIds'
const IN_HOUSE_FLEET_IDS = '_inHouseFleetIds'
const RESCUE_COMPANY_IDS = '_rescueCompanyIds'
const ALL_COMPANY_IDS = '_allCompanyIds'

class CompanyStore extends SearchStore {
  constructor() {
    super({
      modelName: 'Company',
    })

    this[FLEET_IDS] = null
    this[MANAGED_FLEET_IDS] = null
    this[IN_HOUSE_FLEET_IDS] = null
    this[RESCUE_COMPANY_IDS] = null
    this[ALL_COMPANY_IDS] = null
    this._subCompanyIds = null

    this._loadingFleetIds = false
    this._loadingManagedFleetIds = false
    this._loadingAllIds = false
    this._loadingSubCompanyIds = false
    this._loadingRescueCompanyIds = false
    this._loadedSubCompanies = false

    if (SiteStore) { // incredibly ugly, but geosuggest spec fails because of circular reference with SiteStore :(
      SiteStore.bind(SiteStore.REMOTE_SITE_ADDED, this._updateSiteIfNeeded)
    }

    this._allCompanySelectorData = null
    this._selectStoreWatcherId = Utils.create_UUID()
    this._registerListListener(this._selectStoreWatcherId)
    this._registerEventListener(this._selectStoreWatcherId, null, 'name')
    EventRegistry.bind(EventRegistry.getTrigger(this._selectStoreWatcherId), this._clearAllCompanySelectorDataCache)
  }

  isMotorClubId(id, watcherId) {
    name = this.get(id, 'name', watcherId)
    return Consts.MOTORCLUBS.indexOf(name) >= 0
  }

  _userChanged() {
    this._add(this.stores.UserStore.getCompany(), true)
    super._userChanged()
  }

  _addRowToInternalCollection(company) {
    const userCompany = UserStore.getCompany()
    if (userCompany && userCompany.id == company.id) {
      UserStore.setCompany(company)
    }
    super._addRowToInternalCollection(company)
  }

  _updateSiteIfNeeded(remoteSite) {
    if (remoteSite.company_id && this.getById(remoteSite.company_id) !== undefined) {
      const company = this.getById(remoteSite.company_id)
      const companySiteIndex = findIndex(company.sites, site => site.id === remoteSite.id)

      if (companySiteIndex > -1) {
        company.sites[companySiteIndex] = remoteSite
      } else {
        company.sites.push(remoteSite)
      }

      this._update(company)
    }
  }

  _getListsObject() {
    return [
      { list: this[FLEET_IDS], validator: this.isFleet, triggerName: LIST_FLEETS },
      { list: this[MANAGED_FLEET_IDS], validator: this.isFleetManaged, triggerName: LIST_MANAGED_FLEETS },
      { list: this[IN_HOUSE_FLEET_IDS], validator: this.isFleetInHouse, triggerName: LIST_IN_HOUSE_FLEETS },
      { list: this[ALL_COMPANY_IDS], validator: () => true, triggerName: LIST_COMPANIES },
      { list: this._subCompanyIds, validator: this.isSelfOrSubCompany, triggerName: LIST_SUB_COMPANIES },
      { list: this[RESCUE_COMPANY_IDS], validator: this.isRescueCompany, triggerName: LIST_RESCUE_COMPANIES },
    ]
  }

  isLazyLoaded() {
    return true
  }

  loadCollectionOnUserChanged() {
    return false
  }

  shouldAddUserTypeToApiUrl() {
    return true
  }

  isEditable() {
    return true
  }

  hasPermissions() {
    return UserStore.isPartner() || UserStore.isRoot()
  }

  getPartnerDeleteType() {
    return Api.DELETE
  }

  // deprecated, use getSubCompaniesIds
  getSubCompanies(companyId, includeSelf = true) {
    if (!companyId) {
      return []
    }
    return filter(this.getAll(), company => company.parent_company_id === companyId || (includeSelf && company.id === companyId))
  }

  getBaseCompanies() {
    return filter(this.getAll(), company => company.parent_company_id === null || company.parent_company_id === undefined)
  }

  isFleet(company) {
    return company.type === 'FleetCompany'
  }

  isFleetManaged(company) {
    return this.isFleet(company) && !company.in_house && !company.issc_client_id
  }

  isFleetInHouse(company) {
    return this.isFleet(company) && company.in_house && !company.issc_client_id
  }

  isRescueCompany(company) {
    return company.type === 'RescueCompany'
  }

  isParentCompany(company) {
    return company.parent_company_id === null
  }

  isSubCompany(company) {
    return company.parent_company_id === UserStore.getUser().company.id
  }

  isSelfOrSubCompany(company) {
    if (!company || !UserStore.getUser()) {
      return false
    }

    return UserStore.getUser().company.id === company.id || this.isSubCompany(company)
  }

  // This uses ES endpoint on backend. And this endpoint is only available for Swoop,
  // and the lists that use this function are only loaded for Swoop.
  _loadList(isLoadingControl, idsCollection, type, listTrigger) {
    if (!UserStore.isSwoop()) {
      return
    }

    if (this[isLoadingControl]) {
      return
    }

    this[isLoadingControl] = true

    let page = 1
    const filters = {}

    if (idsCollection == MANAGED_FLEET_IDS) {
      filters.subtype = 'FleetManaged'
    } else if (idsCollection == IN_HOUSE_FLEET_IDS) {
      filters.subtype = 'FleetInHouse'
    }

    const loadCompaniesList = (page) => {
      Api.searchPaginated(
        this.getSearchUrl(),
        `type:${type}`,
        { filters },
        page,
        this.getPageSize(),
        'name,asc',
        this.getModelName().toLowerCase(),
        response
      )
    }

    const response = {
      success: (data) => {
        const remoteSearchCollection = data[this.getPlural()]


        if (remoteSearchCollection.length == 0) {
          return
        }

        for (const index in remoteSearchCollection) {
          this._add(remoteSearchCollection[index])
        }

        this[idsCollection] = union(this[idsCollection], map(remoteSearchCollection, 'id'))

        // TODO this can be removed once ES has in_house filter capabilities
        if (idsCollection == FLEET_IDS) {
          this[MANAGED_FLEET_IDS] = union(this[MANAGED_FLEET_IDS], map(filter(remoteSearchCollection, remoteData => this.isFleetManaged(remoteData)), 'id'))
          this[IN_HOUSE_FLEET_IDS] = union(this[IN_HOUSE_FLEET_IDS], map(filter(remoteSearchCollection, remoteData => this.isFleetInHouse(remoteData)), 'id'))
        }

        if (this[idsCollection].length < data.total) {
          this._triggerListListeners(listTrigger)

          // TODO this can be removed once ES has in_house filter capabilities
          if (idsCollection === FLEET_IDS) {
            this._triggerListListeners(LIST_MANAGED_FLEETS)
            this._triggerListListeners(LIST_IN_HOUSE_FLEETS)
          }

          loadCompaniesList(++page)
        } else {
          this[isLoadingControl] = false
          this._triggerListListeners(listTrigger)

          if (idsCollection === FLEET_IDS) {
            this._triggerListListeners(LIST_MANAGED_FLEETS)
            this._triggerListListeners(LIST_IN_HOUSE_FLEETS)
          }
        }
      },
      error: (data, textStatus, errorThrown) => {
        this[isLoadingControl] = false

        this._handleError(data, textStatus, errorThrown)
      },
    }

    loadCompaniesList(page)
  }

  // TODO this needs to be refactored once ES has in_house filter capabilities
  //      to accept the correct inhouse/managed fleet_ids collection and list_fleets trigger
  _loadFleetIds() {
    this._loadList('_loadingFleetIds', FLEET_IDS, FLEET_COMPANY, LIST_FLEETS)
  }

  _loadManagedFleetIds() {
    this._loadList('_loadingManagedFleetIds', MANAGED_FLEET_IDS, FLEET_COMPANY, LIST_MANAGED_FLEETS)
  }

  loadFleetManagedIfNeeded(watcherId = null) {
    if (this[MANAGED_FLEET_IDS] == null) {
      this._registerListListener(watcherId, LIST_MANAGED_FLEETS)
      this._loadManagedFleetIds()
      return true
    }
    return false
  }

  loadFleetsIfNeeded(watcherId = null) {
    if (this[FLEET_IDS] == null) {
      this._registerListListener(watcherId, LIST_FLEETS)
      this._loadFleetIds()
      return true
    }
    return false
  }

  isLoadingFleetManaged(callback) {
    if (this[MANAGED_FLEET_IDS] == null) {
      return true
    }

    return false
  }

  getFleetIds(watcherId = null) {
    if (!UserStore.isSwoop()) {
      return []
    }

    this._registerListListener(watcherId, LIST_FLEETS)

    if (this[FLEET_IDS] == null) {
      this._loadFleetIds()
    }

    if (this[FLEET_IDS]) {
      return this[FLEET_IDS]
    }

    // GO THROUGH COMPANIES AND FIND FLEETS
    const companies = values(this.getAll(false, watcherId))

    const filteredCompanies = filter(companies, company => this.isFleet(company))

    return map(filteredCompanies, 'id')
  }

  idsToNamedObjects(ids, watcherId) {
    return reduce(ids, (obj, companyId) => {
      obj[companyId] = { name: this.get(companyId, 'name', watcherId) }
      return obj
    }, {})
  }

  getFleetsForStore(watcherId = null) {
    return this.idsToNamedObjects(this.getFleetIds(watcherId), watcherId)
  }

  getFleetManagedForStore(watcherId = null) {
    return this.idsToNamedObjects(this.getFleetManagedIds(watcherId), watcherId)
  }

  getFleetManagedIds(watcherId = null) {
    if (!UserStore.isSwoop()) {
      return []
    }

    this._registerListListener(watcherId, LIST_MANAGED_FLEETS)

    if (this[MANAGED_FLEET_IDS] == null) {
      this._loadManagedFleetIds()
    }

    if (this[MANAGED_FLEET_IDS]) {
      return this[MANAGED_FLEET_IDS]
    }

    const companies = values(this.getAll(false, watcherId))

    const filteredCompanies = filter(companies, company => this.isFleetManaged(company))

    return map(filteredCompanies, 'id')
  }

  getFleetInHouseIds(watcherId = null) {
    if (!UserStore.isSwoop()) {
      return []
    }

    this._registerListListener(watcherId, LIST_IN_HOUSE_FLEETS)

    if (this[IN_HOUSE_FLEET_IDS] == null) {
      this._loadFleetIds()
    }

    if (this[IN_HOUSE_FLEET_IDS]) {
      return this[IN_HOUSE_FLEET_IDS]
    }

    // GO THROUGH COMPANIES AND FIND FLEETS
    const companies = values(this.getAll(false, watcherId))

    const filteredCompanies = filter(companies, company => this.isFleetInHouse(company))

    return map(filteredCompanies, 'id')
  }

  getSortedFleetManagedIds(watcherId = null, attribute = 'name') {
    return sortBy(this.getFleetManagedIds(watcherId), id => this.get(id, attribute, watcherId))
  }

  _loadRescueCompanyIds() {
    this._loadList('_loadingRescueCompanyIds', RESCUE_COMPANY_IDS, RESCUE_COMPANY, LIST_RESCUE_COMPANIES)
  }

  getRescueCompaniesIds(watcherId = null) {
    if (!UserStore.isSwoop()) {
      return []
    }

    this._registerListListener(watcherId, LIST_RESCUE_COMPANIES)

    if (this[RESCUE_COMPANY_IDS] == null) {
      this._loadRescueCompanyIds()
    }

    if (this[RESCUE_COMPANY_IDS]) {
      return filter(this[RESCUE_COMPANY_IDS], (rescueCompanyId) => {
        const parentCompanyId = this.get(rescueCompanyId, 'parent_company_id', watcherId)
        if (parentCompanyId === null) {
          return true
        }

        return false
      })
    }

    // GO THROUGH COMPANIES AND FIND RESCUE_COMPANIES
    const companies = values(this.getAll(false, watcherId))

    const filteredCompanies = filter(companies, company => this.isRescueCompany(company) && this.isParentCompany(company))

    return map(filteredCompanies, 'id')
  }

  getSortedRescueCompanyIds(watcherId = null, attribute = 'name') {
    return sortBy(this.getRescueCompaniesIds(watcherId), id => this.get(id, attribute, watcherId))
  }

  getRescueCompaniesForStore(watcherId = null) {
    return zipObject(map(this.getSortedRescueCompanyIds(watcherId), (companyId, key) => [key, { id: companyId, name: this.get(companyId, 'name', watcherId) }]))
  }

  // deprecated, please use getFleetIds (and it seems it's not been used anywhere)
  getFleetCompanies() {
    const companies = values(this.getAll())

    return filter(companies, company => company.type === 'FleetCompany')
  }

  _loadAllIds() {
    this._loadList('_loadingAllIds', ALL_COMPANY_IDS, '', LIST_COMPANIES)
  }

  getSwoopId() {
    return parseInt(map(
      filter(this.getAll(), company => company.name === 'Swoop'),
      'id'
    ))
  }

  // this overrides super.getAllIds only for Swoop
  getAllIds(watcherId = null) {
    if (!UserStore.isSwoop()) {
      return super.getAllIds(watcherId)
    }

    this._registerListListener(watcherId, LIST_COMPANIES)

    if (this[ALL_COMPANY_IDS] == null) {
      this._loadAllIds()
    }

    if (this[ALL_COMPANY_IDS]) {
      return this[ALL_COMPANY_IDS]
    }

    const companies = values(this.getAll(false, watcherId))

    return map(companies, 'id')
  }

  _loadSubCompanyIds() {
    if (!UserStore.isPartner()) {
      return
    }


    if (!this._loadingSubCompanyIds && !this._loadedSubCompanies) {
      this._loadedSubCompanies = true
      this._loadingSubCompanyIds = true

      Api.request(Api.GET, `${UserStore.getUserType()}/companies`, null, {
        success: (data) => {
          for (const index in data) {
            this._add(data[index])
          }

          this._loadingSubCompanyIds = false
          this._triggerListListeners(LIST_SUB_COMPANIES)
        },
        error: (data, textStatus, errorThrown) => {
          this._loadingSubCompanyIds = false
          this._loadedSubCompanies = false
          this._handleError(data, textStatus, errorThrown)
        },
      })
    }
  }

  getSubCompaniesIds(watcherId = null) {
    if (!UserStore.isPartner()) {
      return []
    }

    this._registerListListener(watcherId, LIST_SUB_COMPANIES)

    if (this._subCompanyIds == null) {
      this._loadSubCompanyIds()
    }

    if (this._subCompanyIds) {
      return this._subCompanyIds
    }

    const companies = values(filter(this.getAll(false, watcherId), company => this.isSelfOrSubCompany(company)))

    return map(companies, 'id')
  }

  _clearAllCompanySelectorDataCache() {
    this._allCompanySelectorData = null
  }

  getAllForStore(watcherId = null) {
    // Set up watcherId for list and name changes
    if (watcherId) {
      this._registerListListener(watcherId)
      this._registerEventListener(watcherId, null, 'name')
    }

    // We don't need to pass watcherId down below, because the general watchers above will take care of it
    if (!this._allCompanySelectorData) {
      if (this[ALL_COMPANY_IDS] == null) {
        this._loadAllIds()
      }
      this._allCompanySelectorData = this.getAll()
    }
    return this._allCompanySelectorData
  }

  forceTrigger(type) {
    this.trigger(type)
  }

  updateEmailReview(localData) {
    this._update(localData)

    Api.objectsRequest(
      `${UserStore.getEndpoint()}/company/`, Api.POST, null, {
        success: () => {
          Utils.sendSuccessNotification('Email Review saved')
        },
        error: () => {
          Utils.sendErrorNotification('Email Review failed to update')
        },
      },
      localData
    )
  }

  updateCommission(company) {
    Api.updateCommission(company, {
      success: () => {
        Utils.sendSuccessNotification('Commission percentage updated')
      },
      error: () => {
        Utils.sendErrorNotification('Commission percentage failed to update')
      },
    })
  }

  getBatchUrl() {
    return `${UserStore.getEndpoint()}/${super.getBatchUrl()}`
  }

  updateRolePermissions(permissions, callbacks) {
    const body = {
      company: {
        role_permissions: permissions,
      },
    }

    Api.updateRolePermissions(body, {
      success: (remoteData) => {
        if (callbacks && callbacks.success) {
          callbacks.success(remoteData)
        }
      },
      error: (remoteData, textStatus, errorThrown) => {
        let handled = false
        if (callbacks && callbacks.error) {
          handled = callbacks.error(remoteData, textStatus, errorThrown)
        }
        if (!handled) {
          this._handleError(remoteData, textStatus, errorThrown, true)
        }
      },
    })
  }
}

const store = new CompanyStore()

export default store
