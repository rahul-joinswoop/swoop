BaseStore    = require('stores/base_store').default
import { filter, find } from 'lodash'

class QuestionStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Question'
    })

  isEditable: -> false
  loadCollectionOnUserChanged: -> false

  getQuestionsForServiceId: (service_id) =>
    filter(this.stores.UserStore.getUser()?.company.questions, (question) ->
      return question.service_id == service_id
    )

  getQuestion: (questionText) =>
    #TODO: memoize this lookup
    find(this.stores.UserStore.getUser()?.company.questions, (question) ->
      return question.question == questionText
    )

  getAnswerFromQuestion: (question, answer_id) =>
    find(question.answers, (answer) ->
      answer.id == answer_id
    )
  getQuestionById: (id) =>
    find(this.stores.UserStore.getUser()?.company.questions, (question) ->
      return question.question_id == id
    )


  #TODO: THis can definately be optimized
  getQuestionAnswer: (service_id, question_results, question_text) =>
    questions = @getQuestionsForServiceId(service_id)
    for question in questions
      if question.question == question_text
        results = question_results
        if results?
          for result in results
            if ""+result.question_id == ""+question.question_id
              for answer in question.answers
                if ""+result.answer_id == ""+answer.id
                    return answer

store = new QuestionStore()

module.exports = store
