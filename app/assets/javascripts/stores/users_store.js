import Api from 'api'
import SearchStore from 'stores/search_store'
import CompanyStore from 'stores/company_store'
import UserStore from 'stores/user_store'
import Utils from 'utils'
import { filter, includes, intersection, map, sortBy, values, zipObject } from 'lodash'
import EventRegistry from 'EventRegistry'
/*
  This store owns all users of a Company.
  Partners and Fleet can only see their own Users.
  Swoop admins can see users from all companies.

  If you want something from the current logged in user, you'll find it on this.stores.UserStore.
*/

const LIST_DRIVERS = 'list_drivers'
const LIST_DISPATCHERS = 'list_dispatchers'
const DISPATCHER_ROLES = ['dispatcher', 'admin', 'root', 'answering_service', 'swoop_dispatcher']
const DRIVER_ROLES = ['driver']

class UsersStore extends SearchStore {
  constructor(props = {}) {
    props.modelName = 'User'
    super(props)
    this._driverIds = null
    this._dispatcherIds = null
    this._loadingDisaptcherIds = false
    this._loadingDriverIds = false

    // Caching for getSortedDispatchersById
    this._sortedDispatcherCache = null
    this._selectStoreWatcherId = Utils.create_UUID()
    this._registerListListener(this._selectStoreWatcherId, LIST_DISPATCHERS, 'full_name')
    EventRegistry.bind(EventRegistry.getTrigger(this._selectStoreWatcherId), this._clearSortedDispatcherCache)
  }

  _addRowToInternalCollection(data) {
    super._addRowToInternalCollection(data)
    this._checkDispatcherStatus(data)
    this._checkDriverStatus(data)

    // TDOO: this.stores.UserStore should be listening to my users (shouldn't be a push)
    this.stores.UserStore.updateUser(data)
  }

  _removeRowFromInternalCollection(id) {
    const data = this.getById(id)
    // The object will get removed from collection, this is just for the list checks
    data.deleted_at = true
    this._checkDispatcherStatus(data)
    this._checkDriverStatus(data)

    super._removeRowFromInternalCollection(id)
  }

  _remove(data, triggerChange) {
    super._remove(data, triggerChange)

    this._checkDispatcherStatus(data)
    this._checkDriverStatus(data)
  }

  getBatchUrl() {
    if (this.stores.UserStore.isSwoop()) {
      return `${this.stores.UserStore.getEndpoint()}/users/batch?user_ids=`
    }
    return super.getBatchUrl()
  }

  isEditable() {
    return true
  }

  isLazyLoaded() {
    return true
  }

  loadCollectionOnUserChanged() {
    return false
  }

  getUrl() {
    return 'users?include_deleted=true'
  }

  isCompanyDispatcher(user, companyId) {
    const myUser = UserStore.getUser()
    if (myUser && !companyId) {
      companyId = myUser.company_id
    }

    return !user.deleted_at &&
      intersection(user.roles, DISPATCHER_ROLES).length > 0 &&
      user.company_id === companyId
  }

  isDriver(user) {
    return !user.deleted_at && intersection(user.roles, DRIVER_ROLES).length > 0
  }

  addItem(obj) {
    let callbackSuccess = null
    if (obj.callbacks && obj.callbacks.success) {
      callbackSuccess = obj.callbacks.success
    }
    if (!obj.callbacks) {
      obj.callbacks = {}
    }
    obj.callbacks.success = function (data) {
      if (callbackSuccess) {
        callbackSuccess(data)
      }
      Tracking.track('Add User', {
        category: 'Onboarding',
      })
    }
    super.addItem(obj)
  }

  _loadDispatcherIds() {
    if (!this._loadingDisaptcherIds) {
      this._loadingDisaptcherIds = true
      Api.request(Api.GET, '/users/dispatchers', null, {
        success: (data) => {
          for (const index in data) {
            this._add(data[index])
          }
          this._dispatcherIds = map(data, 'id')
          this._triggerListListeners(LIST_DISPATCHERS)
        },
        error: (data, textStatus, errorThrown) => {
          this._loadingDisaptcherIds = false
          this._handleError(data, textStatus, errorThrown)
        },
      })
    }
  }

  _loadDriverIds() {
    if (!this._loadingDriverIds) {
      this._loadingDriverIds = true
      Api.request(Api.GET, '/users/drivers', null, {
        success: (data) => {
          for (const index in data) {
            this._add(data[index])
          }
          this._driverIds = map(data, 'id')
          this._triggerListListeners(LIST_DRIVERS)
        },
        error: (data, textStatus, errorThrown) => {
          this._loadingDriverIds = false
          this._handleError(data, textStatus, errorThrown)
        },
      })
    }
  }

  getDispatcherIds(watcherId = null) {
    this._registerListListener(watcherId, LIST_DISPATCHERS)

    if (this._dispatcherIds == null) {
      this._loadDispatcherIds()
    }
    if (this._dispatcherIds) {
      return this._dispatcherIds
    }
    // GO THROUGH USERS AND FIND DISPATCHERS
    // SHould not need to pass user id in as we are watching for the disaptcher list abbove
    const userValues = values(this.getAll(false))

    const company_id = this.stores.UserStore.getUser() && this.stores.UserStore.getUser().company_id || null
    const filteredUsers = filter(userValues, (dispatcher) => {
      if (dispatcher.company_id !== company_id) {
        return false
      }
      return this.isCompanyDispatcher(dispatcher)
    })


    return map(filteredUsers, 'id')
  }

  _clearSortedDispatcherCache() {
    this._sortedDispatcherCache = null
  }

  getSortedDispatcherIds(watcherId = null) {
    if (!this._sortedDispatcherCache) {
      this._sortedDispatcherCache = sortBy(this.getDispatcherIds(watcherId), id => this.get(id, 'full_name', watcherId))
    } else {
      this._registerListListener(watcherId, LIST_DISPATCHERS, 'full_name')
    }
    return this._sortedDispatcherCache
  }

  getDriverIds(watcherId = null) {
    this._registerListListener(watcherId, LIST_DRIVERS)

    if (this._driverIds == null) {
      this._loadDriverIds()
    }
    if (this._driverIds) {
      return this._driverIds
    }
    // GO THROUGH USERS AND FIND DISPATCHERS
    const userValues = values(this.getAll(false))
    const company_id = this.stores.UserStore.getUser() && this.stores.UserStore.getUser().company_id || null

    const filteredUsers = filter(userValues, (dispatcher) => {
      if (dispatcher.company_id !== company_id) {
        return false
      }
      return this.isDriver(dispatcher)
    })


    return map(filteredUsers, 'id')
  }

  getSortedDriverIds(watcherId = null, attribute = 'full_name') {
    return sortBy(this.getDriverIds(watcherId), id => this.get(id, attribute, watcherId))
  }

  getSortedDrivers() {
    // Depricated Use getSortedDriverIds instead
    const userValues = values(this.getAll())

    const filteredUsers = filter(userValues, (driver) => {
      if (driver.deleted_at) {
        return false
      }

      if (driver.roles !== undefined && driver.roles !== null) {
        return includes(driver.roles, 'driver')
      }

      return false
    })

    return sortBy(filteredUsers, (driver) => {
      if (driver && driver.full_name) {
        return driver.full_name.toLowerCase()
      }
    })
  }

  getDriversForStore(watcherId = null) {
    const driver_ids = this.getSortedDriverIds(watcherId)
    return zipObject(driver_ids, map(driver_ids, driver_id => ({ name: this.get(driver_id, 'full_name', watcherId) })))
  }

  _userChanged() {
    super._userChanged()

    const loggedInUser = this.stores.UserStore.getUser()
    CompanyStore._add(loggedInUser.company)
  }

  _checkList(data, ids, shouldBelong, listName) {
    if (ids) {
      if (shouldBelong && ids.indexOf(data.id) == -1) {
        ids.push(data.id)
        this._triggerListListeners(listName)
      }

      const index = ids.indexOf(data.id)
      if (!shouldBelong && index > -1) {
        ids.splice(index, 1)
        this._triggerListListeners(listName)
      }
    }
  }

  _checkDispatcherStatus(data) {
    this._checkList(data, this._dispatcherIds, this.isCompanyDispatcher(data), LIST_DISPATCHERS)
  }

  _checkDriverStatus(data) {
    this._checkList(data, this._driverIds, this.isDriver(data), LIST_DRIVERS)
  }
}

const store = new UsersStore()

export default store
