import MicroEvent from 'microevent-github'
import moment from 'moment-timezone'
import Logger from 'lib/swoop/logger'
import Api from 'api'
import ErrorStore from 'stores/error_store'
import UserStore from 'stores/user_store'
import Dispatcher from 'lib/swoop/dispatcher'
import {
  each, extend, filter, forEach, get, intersection, keys, map, partial, pickBy, reduce, some, sortBy, values, without, zipObject,
} from 'lodash'
import Consts from 'consts'
import Utils from 'utils'
import EventRegistry from 'EventRegistry'

const ADD = 'add'
const UPDATE = 'update'
const DELETE = 'delete'
const LIST_ALL = 'list_all'

class BaseStore {
  /* Triggers */
  CHANGE = 'change'

  LOADED = 'loaded'

  /* controlls for loading entire collections; @see BaseStore.loadAndGetAll() */
  _allIdsLoaded = false

  _loadingAllIds = false

  /* External Methods */
  getAllIds(showDeleted = false, watcherId = null) {
    this._registerEventListener(watcherId)

    if (showDeleted) {
      return map(keys(this._getInternalCollection()), Number)
    }

    const allIdsNotDeleted = filter(keys(this._getInternalCollection()), id => !this.get(id, 'deleted_at', watcherId, true))

    return map(allIdsNotDeleted, Number)

    // TODO: add watcher on adds or removes from list
  }

  get(id, property, watcherId = null) {
    if (!id) {
      console.warn('Calling get with empty id for property: ', property)
      return
    }

    this._registerEventListener(watcherId, id, property)

    let obj = null

    obj = this.getById(id)

    if (!obj && this.isLazyLoaded()) {
      this._lazyLoad(id)
    }

    return get(obj, property)
  }

  getCompanyAttribute() {
    return this.getModelName()
  }


  // TODO: Cache this and break when company changes
  getCompanyItems(watcherId, company_attribute, filter_company_id, primary_company_id) {
    if (!company_attribute) {
      company_attribute = this.getCompanyAttribute()
    }

    if (!primary_company_id) {
      const company = this.stores.UserStore.getCompany(watcherId)
      if (company) {
        primary_company_id = company.id
      }
    }
    if (primary_company_id) {
      let items = this.stores.CompanyStore.get(primary_company_id, company_attribute, watcherId)

      if (filter_company_id) {
        items = intersection(items, this.stores.CompanyStore.get(filter_company_id, company_attribute, watcherId))
      }

      return without(map(items, (id) => {
        const item = this.getById(id, true, watcherId)
        if (item && !item.deleted_at) {
          return item
        }
        return null
      }), null)
    }
    return []
  }

  // TODO: Cache this and break when company changes
  companyContains(id, watcherId, company_attribute) {
    if (!company_attribute) {
      company_attribute = this.getCompanyAttribute()
    }
    const company = this.stores.UserStore.getCompany(watcherId)
    if (company && company.id) {
      const item_ids = this.stores.CompanyStore.get(company.id, company_attribute, watcherId)
      if (item_ids) {
        return item_ids.indexOf(id) > -1
      }
      return false
    }
  }

  _searchAndLazyLoadBy(filterBy, property, watcherId = null, showDeleted = false) {
    // this tries to search on the current getAll collection first
    const records = filter(this.getAll(showDeleted), record => reduce(
      map(keys(filterBy), key => record[key] === filterBy[key]),
      (memo, next) => memo && next, true
    ))

    if (records.length > 0) {
      return records
    }
    this._lazyLoadByFilter(filterBy, property, watcherId) // lazy load if not loaded yet
  }

  _lazyLoadByFilter(filterBy, property = null, watcherId = null) {
    if (!filterBy || !watcherId) {
      return
    }

    const filterHash = Utils.hashString(JSON.stringify(filterBy))

    this._pushWatcherIdByFilterHash(filterHash, watcherId)

    if (this._isLoadingByFilter([filterHash])) {
      return
    }

    this._loadingByFilter[filterHash] = true
    this._addBatchFilterWithWatcherId(filterBy, watcherId)

    if (this._isSearchBatchTimeoutByFilterSet(filterBy)) {
      this._clearSearchBatchTimeoutByFilter(filterBy)
    }

    if (keys(this._batchFilter).length >= 100) {
      this._searchByFilterBatch(filterHash, property)
    } else {
      this._searchBatchTimeoutByFilter[sortBy(keys(filterBy)).toString()] = setTimeout(partial(this._searchByFilterBatch, filterBy, property), 20)
    }
  }

  _searchByFilterBatch = (filterBy, property = null) => {
    const filterKey = keys(filterBy)
    const batchFilterByKey = this._getBatchFilterByKey(filterKey)
    const filterValuesForUrl = keys(batchFilterByKey)

    if (filterValuesForUrl) {
      Api.objectsRequest(
        this.getSearchBatchUrl() + this._getBatchFilterUrlParams(filterKey, filterValuesForUrl),
        Api.GET, null, {
          success: (remoteData) => {
            const filterIds = keys(batchFilterByKey)

            for (let i = 0; i < remoteData.length; i++) {
              const row = remoteData[i]
              // we register the listener only on success because only here we'll have the record id
              for (let j = 0; j < filterIds.length; j++) {
                const filterValue = filterIds[j]

                if (!isNaN(filterValue)) {
                  if (parseInt(filterValue) === parseInt(row[filterKey[0]])) {
                    forEach(batchFilterByKey[filterIds[j]], (watcherId) => {
                      this._registerEventListener(watcherId, row.id, property)
                    })
                    delete this._batchFilter[filterKey[0]][parseInt(filterValue)]
                  }
                } else if (filterValue === row[filterKey[0]]) {
                  forEach(batchFilterByKey[filterIds[j]], (watcherId) => {
                    this._registerEventListener(watcherId, row.id, property)
                  })
                  delete this._batchFilter[filterKey[0]][parseInt(filterValue)]
                }
              }

              this._add(row, false)
            }
          },
          error: this._handleError,
        }
      )
    }
  }

  _isSearchBatchTimeoutByFilterSet(filterBy) {
    return !!this._searchBatchTimeoutByFilter[sortBy(keys(filterBy)).toString()]
  }

  _clearSearchBatchTimeoutByFilter(filterBy) {
    clearTimeout(this._searchBatchTimeoutByFilter[sortBy(keys(filterBy)).toString()])
  }

  _pushWatcherIdByFilterHash(filterHash, watcherId) {
    if (this._watcherIdsByFilterHash[filterHash]) {
      this._watcherIdsByFilterHash[filterHash].push(watcherId)
    } else {
      this._watcherIdsByFilterHash[filterHash] = [watcherId]
    }
  }


  _isLoadingByFilter(filterHash) {
    return !!this._loadingByFilter[filterHash]
  }

  _addBatchFilterWithWatcherId(filterBy, watcherId) {
    const filterKey = sortBy(keys(filterBy)).toString()
    const filterValues = sortBy(values(filterBy)).toString()

    const doesBatchFilterContainsKey = some(keys(this._batchFilter[filterKey]))

    if (doesBatchFilterContainsKey) {
      const doesBatchFilterContainsValues = some(keys(this._batchFilter[filterKey][filterValues]))
      if (doesBatchFilterContainsValues) {
        this._batchFilter[filterKey][filterValues].push(watcherId)
      } else {
        this._batchFilter[filterKey][filterValues] = [watcherId]
      }
    } else {
      this._batchFilter[filterKey] = {}
      this._batchFilter[filterKey][filterValues] = [watcherId]
    }
  }

  _getBatchFilterByKey(filterKey) {
    return this._batchFilter[sortBy(filterKey).toString()]
  }

  _getBatchFilterUrlParams(filterKey, filterValues) {
    return encodeURIComponent(`${filterKey[0]}=${filterValues.toString()}`)
  }

  isAllLoaded(watcherId = null) {
    if (watcherId) {
      this._registerListListener(watcherId, LIST_ALL)
    }

    return this._allIdsLoaded || this._loaded
  }

  shouldSetCompanyDefaultsOnObjectLoad() {
    return false
  }

  _setCompanyDefaultsToObject(record) {
    if (!record.company) {
      return
    }

    const defaultProps = ['name', 'dispatch_email', 'phone', 'fax']

    for (const prop of defaultProps) {
      if (!record[prop] && record.company[prop]) {
        Logger.debug(`${this.getModelName()} id ${record.id} doesn't have ${prop} set, copying from its company`)
        record[prop] = record.company[prop]
      }
    }

    if (!record.primary_contact && record.company.primary_contact) {
      Logger.debug(`${this.getModelName()} id ${record.id} doesn't have primary_contact set, copying from its company`)
      record.primary_contact = record.company.primary_contact.name
    }

    if (!record.primary_email && record.company.primary_contact) {
      Logger.debug(`${this.getModelName()} id ${record.id} doesn't have primary_email set, copying from its company`)
      record.primary_email = record.company.primary_contact.email
    }

    if (!record.primary_phone && record.company.primary_contact) {
      Logger.debug(`${this.getModelName()} id ${record.id} doesn't have primary_phone set, copying from its company`)
      record.primary_phone = record.company.primary_contact.phone
    }

    if (!record.location && record.company.location) {
      Logger.debug(`${this.getModelName()} id ${record.id} doesn't have location set, copying from its company`)
      record.location = extend({}, record.company.location)
    }

    if (!record.accounting_email && record.company.accounting_email) {
      Logger.debug(`${this.getModelName()} id ${record.id} doesn't have accounting_email set, copying from its company`)
      record.accounting_email = record.company.accounting_email
    }
  }

  /* This works as a watcher for getting entire collections.
   * This returns entire objects so we don't need to do <>Store.get on lists.
   *
   * _allIdsLoaded is a flag that starts as false and must be set as true once it
   * has finished loading the collection in _loadAllIds().
   */
  loadAndGetAll(watcherId = null, properties = null) {
    // Set up watcherId for list and name changes
    if (watcherId) {
      this._registerListListener(watcherId, LIST_ALL, properties)
    }

    // We don't need to pass watcherId down below, because the general watchers above will take care of it
    if (!this._allIdsLoaded) {
      this._loadAllIds()
    }

    return this.getAll()
  }

  _mapIds(ids) {
    if (ids) {
      return map(ids, id => this.getById(id))
    }

    return []
  }

  getBatchUrl() {
    return `${this.getEditUrl()}/batch?${this._getJSONName()}_ids=`
  }

  getSearchBatchUrl() {
    return `${this.getEditUrl()}/search_batch?filter=`
  }

  _loadAllIds() {
    if (!this._loadingAllIds && this.hasPermissions()) {
      this._loadingAllIds = true

      let url = this.getUrl()

      if (this.shouldAddUserTypeToApiUrl()) {
        url = this.addUserTypeToApiUrl(url)
      }

      Api.request(
        Api.GET, url, {},
        {
          success: (data) => {
            for (const index in data) {
              this._add(data[index], false)
            }

            this._allIdsLoaded = true
            this._loadingAllIds = false
            this._triggerListListeners()
          },
          error: (data, textStatus, errorThrown) => {
            this._loadingAllIds = false
          },
        }
      )
    }
  }

  processBatch(batchResponse) {
    return batchResponse
  }

  _fetchBatch() {
    if (this._batch.length > 0) {
      const calledBatch = this._batch.slice(0)
      Api.objectsRequest(this.getBatchUrl() + this._batch.join(','), Api.GET, null, {
        success: (batchResponse) => {
          const remoteData = this.processBatch(batchResponse)
          for (const row of remoteData) {
            this._add(row)
          }
          for (const row of remoteData) {
            each(this._loadingById[row.id], (callback) => {
              if (callback) {
                callback(row)
              }
              delete this._loadingById[row.id]
            })
            delete this._loadingById[row.id]
          }
        },
        error: (...args) => {
          // Clear out loading items as they failed
          for (const index in calledBatch) {
            this._loadingById[calledBatch[index]] = null
          }

          this._handleError(...args)
        },
      })
    }
    this._batch = []
  }

  _lazyLoad(id, callback) {
    if (this._loadingById[id]) {
      this._loadingById[id].push(callback)
      return
    }

    this._loadingById[id] = [callback]

    if (this.batch_timeout) {
      clearTimeout(this.batch_timeout)
    }
    this._batch.push(id)
    if (this._batch.length >= 100) {
      this._fetchBatch()
    } else {
      this.batch_timeout = setTimeout(this._fetchBatch, 20)
    }
  }

  getAll(showDeleted = false, watcherId = null, property) {
    // console.warn("Deprecated: use getAllIds instead")
    if (watcherId) {
      this._registerListListener(watcherId, LIST_ALL, property)
    }

    if (showDeleted) {
      return this._getInternalCollection()
    }

    const allNotDeleted = pickBy(this._getInternalCollection(), (value, key) => value.deleted_at === undefined || value.deleted_at === null)

    return allNotDeleted || {}
  }

  getById(id, force = false, watcherId = null) {
    // console.warn("Deprecated: use get on individual properties instead")
    this._registerEventListener(watcherId, id)

    const data = this.getAll(true)[id]

    // TODO: Slow this down and make it a batch request
    if (data == undefined && force) {
      if (this.isLazyLoaded()) {
        this._lazyLoad(id)
      } else {
        if (this._loadingById[id]) {
          return
        }
        this._loadingById[id] = []

        const userType = this.stores.UserStore.getEndpoint()
        const apiName = `${this.getModelName()}Request`

        Api[apiName](Api.GET, userType, id, {
          success: (data) => {
            this._add(data)
          },
          error: (...args) => {
            this._loadingById[id] = null
            this._handleError(...args)
          },
        })
      }
    }

    return data
  }

  exists(id, watcherId = null) {
    if (id === null || id === undefined) {
      return false
    }

    this._registerEventListener(watcherId, id)

    if (watcherId) {
      return !!this.get(id, 'id', watcherId)
    }
    return !!this._getInternalCollection()[id]
  }

  getItemTrigger(obj = {}) {
    // TODO: perhaps put in warning about object not having ID
    return `${this.CHANGE}_${obj.id}`
  }

  hasLoaded(watcherId = null) {
    EventRegistry.register(watcherId, this.getModelName(), LIST_ALL)

    return this._loaded
  }

  addRemoteItem(remoteData) {
    Logger.debug(`modelName added: ${remoteData.target.id}`)
    const data = remoteData.target

    this._add(data)
    this.triggerItemChange(data)
  }

  addItem(localData) {
    delete localData.id

    this._sendItem(localData, ADD, '')
  }

  updateItem(localData, request) {
    this._sendItem(localData, UPDATE, localData.id, request)
  }

  deleteItem(localData) {
    if (!this.isEditable()) {
      console.warn('Attempt to delete non editable: ', this.getModelName())

      return
    }

    this._remove(localData)

    this._sendItem({
      callbacks: localData.callbacks,
      deleted_at: moment().toISOString(),
      silent: localData.silent,
    }, DELETE, localData.id)
  }

  triggerItemChange(obj) {
    this.trigger(this.getItemTrigger(obj), obj)
  }

  getPageSize() {
    // TODO: seems like this should go in the search store or have a default
    return this.getSearch().defaultPageSize
  }

  getAddedMessage(record) {
    return `A ${this.getModelName()} was added`
  }

  getChangedMessage(record) {
    return `A ${this.getModelName()} was changed`
  }

  getRemovedMessage(record) {
    return `A ${this.getModelName()} was deleted`
  }

  getFailedToAddMessage() {
    return `A ${this.getModelName()} failed to add`
  }

  getFailedToUpdateMessage() {
    return `A ${this.getModelName()} failed to update`
  }

  getFailedToDeleteMessage() {
    return `A ${this.getModelName()} failed to delete`
  }

  getEditUrl() {
    return this.getPlural()
  }

  getUrl() {
    return this.getPlural()
  }

  getCustomizedEditUrl(method) {
    let url = this.getEditUrl()

    if (this.shouldAddUserTypeToApiUrl()) {
      url = this.addUserTypeToApiUrl(url, method)
    }

    return url
  }

  getDeleteHTTPMethod() {
    return Api.UPDATE
  }

  getModelName() {
    return this._modelName
  }

  isEditable() {
    return false
  }

  isLazyLoaded() {
    return false
  }

  shouldAddUserTypeToApiUrl() {
    return false
  }

  addUserTypeToApiUrl(url) {
    return `${this.stores.UserStore.getEndpoint()}/${url}`
  }

  showNotification() {
    return true
  }

  getPlural() {
    return Utils.getPlural(this.getModelName())
  }

  loadCollectionOnUserChanged() {
    return true
  }

  hasPermissions() {
    return true
  }

  paginationPowered() {
    return false
  }

  getLoadingPaginationSize() {
    return 50
  }

  /* Semi-Private Methods */
  constructor({ modelName, skipBinds, stores }) {
    if (!modelName || modelName.length === 0) {
      throw new Error('"modelName" param cannot be empty. Please add this to supper call object: {modelName: <modelName>}')
    }
    if (!this.stores) {
      this.stores = {}
    }
    if (!this.orig_stores) {
      this.orig_stores = {}
    }
    this.stores.UserStore = this.orig_stores.UserStore = UserStore

    if (stores) {
      for (const key in stores) {
        this.stores[key] = this.orig_stores[key] = stores[key]
      }
    }

    this._userChanged = ::this._userChanged
    this._loggedOut = ::this._loggedOut
    this._fetchBatch = ::this._fetchBatch
    this._add = ::this._add
    this._addRowToInternalCollection = ::this._addRowToInternalCollection

    this._modelName = modelName.toLowerCase()
    this.reset()
    this._id = Utils.create_UUID()
    if (!skipBinds) {
      this.stores.UserStore.bind(this.stores.UserStore.LOGIN, this._userChanged)
      this.stores.UserStore.bind(this.stores.UserStore.LOGOUT, this._loggedOut)
      if (window) {
        if (!window.stores) {
          window.stores = {}
        }
        window.stores[modelName] = this
      }
    }
  }

  loadData(data) {
    for (const row of data) {
      this._addRowToInternalCollection(row)
    }
    this._allIdsLoaded = true
    this._loaded = true
  }

  reset() {
    this._loaded = false
    this._modelCollection = {}
    this._loadingById = {}
    this._batch_timeout = 0
    this._batch = []
    this._watcherIdsByFilterHash = {}
    this._loadingByFilter = {}
    this._batchFilter = {}
    this._searchBatchTimeoutByFilter = []
    this._collectionForStore = {}
  }

  createInstance(data) {
    return new this.constructor({
      modelName: this.getModelName(),
      skipBinds: true,
    })
  }

  updateStores(stores) {
    for (const key in stores) {
      if (this.stores[key]) {
        this.stores[key] = stores[key]
      }
    }
  }

  resetStores() {
    for (const key of stores) {
      this.stores[key] = this.orig_stores[key]
    }
  }

  resetStores() {

  }

  getId() {
    return this._id
  }

  _getInternalCollection() {
    return this._modelCollection
  }

  _addRowToInternalCollection(data) {
    if (!data || !data.id) {
      return undefined
    }
    const { id } = data

    if (this.shouldSetCompanyDefaultsOnObjectLoad()) {
      this._setCompanyDefaultsToObject(data)
    }

    EventRegistry.triggerObjListeners(this.getModelName(), LIST_ALL, id)

    const oldObj = this.getById(id)
    this._modelCollection[id] = data
    this._triggerPropsListeners(data, oldObj)

    // If we are only updating a previous object, the list size hasn't changed (unless it's been deleted)
    if ((!oldObj && !data.deleted_at) || (oldObj && (oldObj.deleted_at != data.deleted_at))) {
      this._triggerListListeners()
    }

    // check if data should be added into one of the current child lists
    forEach(this._getListsObject(), (listObject) => {
      this._updateLists(data, listObject.list, listObject.validator(data), listObject.triggerName, oldObj, listObject.finallyCallback)
    })
  }

  _updateLists(data, ids, shouldBelong, listName, oldObj, finallyCallback) {
    if (ids) {
      if (shouldBelong) {
        this._updateCollectionForStore(listName, data)

        if (ids.indexOf(data.id) === -1) {
          ids.push(data.id)
          this._triggerListListeners(listName)
        } else {
          this._triggerPropsListeners(data, oldObj, listName)
        }
      }

      const index = ids.indexOf(data.id)

      if (!shouldBelong && index > -1) {
        ids.splice(index, 1)
        this._triggerListListeners(listName)
      }

      if (finallyCallback) {
        finallyCallback(data)
      }
    }
  }

  _triggerPropsListeners(newObj, oldObj, listName = LIST_ALL) {
    const { id } = newObj
    const props = EventRegistry.getWatchedProperties(this.getModelName(), listName, id)

    for (const propId in props) {
      const prop = props[propId]

      if (get(oldObj, prop) !== get(newObj, prop)) {
        EventRegistry.triggerPropListeners(this.getModelName(), listName, id, prop)
        EventRegistry.triggerPropListeners(this.getModelName(), listName, null, prop)
      }
    }
  }

  _setupCollectionForStore(listName, idsCollection) {
    this._collectionForStore[listName] = zipObject(idsCollection, map(idsCollection, id => ({ name: this.getById(id).name })))
  }

  _updateCollectionForStore(listName, object) {
    const collectionForStore = this._collectionForStore[listName]

    if (collectionForStore) {
      if (object.deleted_at) {
        delete collectionForStore[object.id]
      } else {
        const name = collectionForStore[object.id]

        if (name !== object.name) {
          collectionForStore[object.id].name = object.name
        }
      }
    }
  }

  _getCollectionForStore(listName) {
    return this._collectionForStore[listName]
  }

  // must be implemented by child class, CompanyStore is an exemple
  _getListsObject() {
    return []
  }

  _removeRowFromInternalCollection(id) {
    if (!id) {
      return undefined
    }

    EventRegistry.triggerObjListeners(this.getModelName(), LIST_ALL, id)

    this._triggerListListeners()

    delete this._modelCollection[id]
  }

  _triggerListListeners(list = LIST_ALL) {
    EventRegistry.triggerListListeners(this.getModelName(), list)
  }

  _registerEventListener(watcherId, id, property, list = LIST_ALL) {
    EventRegistry.register(watcherId, this.getModelName(), list, id, property)
  }

  _registerListListener(watcherId, list = LIST_ALL, property = null) {
    if (watcherId) {
      EventRegistry.register(watcherId, this.getModelName(), list)

      // TODO: should these properties be set up on the specific list
      if (property) {
        if (property.constructor === Array) {
          for (const prop of property) {
            this._registerEventListener(watcherId, null, prop, list)
          }
        } else {
          this._registerEventListener(watcherId, null, property, list)
        }
      }
    }
  }

  _add(data, triggerChange = true) {
    if (data && data.id) {
      this._addRowToInternalCollection(data)
      if (triggerChange) {
        this.triggerItemChange(data)
        this.trigger(this.CHANGE)
      }
    }
  }

  _update(data, triggerChange = true) {
    if (data && data.id && this.getAll()[data.id]) {
      const updatedData = extend({}, this.getById(data.id), data)

      this._addRowToInternalCollection(updatedData)

      if (triggerChange) {
        this.triggerItemChange(data)
        this.trigger(this.CHANGE)
      }
    }
  }

  _remove(data) {
    if (data && data.id && this.getAll()[data.id]) {
      this._removeRowFromInternalCollection(data.id)
      this.triggerItemChange(data)
      this.trigger(this.CHANGE)
    }
  }

  _sendItem(localData, method, id, request = Api.objectsRequest, extraObjectsToRequest = null) {
    Logger.debug('About to ', method, '', this.getModelName())

    if (!this.isEditable()) {
      console.warn('Attempt to ', method, ' non editable: ', this.getModelName())

      return
    }

    const { callbacks } = localData
    delete localData.callbacks

    let notify = true
    if (localData.silent) {
      notify = false
    }
    delete localData.silent

    const patchDataObject = {}
    patchDataObject[this._getJSONName()] = localData

    if (extraObjectsToRequest) {
      Logger.debug('extraObjectsToRequest', extraObjectsToRequest)

      extend(patchDataObject, extraObjectsToRequest)
    }
    request(this.getCustomizedEditUrl(method), this._getHttpMethod(method), id, {
      success: (remoteData) => {
        if (notify && this.showNotification()) {
          Utils.sendNotification(this._getSuccessMessage(method, remoteData), Consts.SUCCESS)
        }
        const action = this._getSuccessAction(method)

        if (action) {
          action(remoteData)
        }

        if (callbacks && callbacks.success) {
          callbacks.success(remoteData)
        }
      },
      error: (remoteData, textStatus, errorThrown) => {
        let handled = false
        if (callbacks && callbacks.error) {
          handled = callbacks.error(remoteData, textStatus, errorThrown)
        }

        if (!handled) {
          Utils.sendNotification(this._getFailedMessage(), Consts.ERROR)
          this._handleError(remoteData, textStatus, errorThrown, true)
        }
      },
    }, patchDataObject)
  }

  _getJSONName() {
    return this.getModelName()
  }

  _getHttpMethod(type) {
    if (type === ADD) {
      return Api.POST
    } if (type === UPDATE) {
      return Api.UPDATE
    } if (type === DELETE) {
      return this.getDeleteHTTPMethod()
    }
  }

  _getSuccessMessage(type, record) {
    if (type === ADD) {
      return this.getAddedMessage(record)
    } if (type === UPDATE) {
      return this.getChangedMessage(record)
    } if (type === DELETE) {
      return this.getRemovedMessage(record)
    }
  }

  _getSuccessAction(type) {
    if (type === ADD) {
      return this._add
    } if (type === UPDATE) {
      return this._update
    }
  }

  _getFailedMessage(type) {
    if (type === ADD) {
      return this.getFailedToAddMessage()
    } if (type === UPDATE) {
      return this.getFailedToUpdateMessage()
    } if (type === DELETE) {
      return this.getFailedToDeleteMessage()
    }
  }

  _handleError(data, textStatus, errorThrown, force) {
    const errorObj = {
      data,
      textStatus,
      errorThrown,
    }

    if (force) {
      errorObj.forceMsg = 'Uh oh! There was an issue saving your change. Please refresh and try again.'
    }

    Dispatcher.send(ErrorStore.SHOW_ERROR, errorObj)
  }

  _paginationEndpoint() {
    return Api.requestPaginated
  }

  _loadWithPagination(callbacks = {}, page = 1, per_page = this.getLoadingPaginationSize(), request = this._paginationEndpoint()) {
    request(this.getUrl(), Api.GET, {
      success: (remoteData) => {
        callbacks?.preAddSuccess?.(remoteData, page)

        for (const row of remoteData) {
          this._add(row, false)
        }

        if (remoteData.length === per_page) {
          if (callbacks.success) {
            callbacks.success(remoteData, false)
          }

          this._loadWithPagination(callbacks, ++page, per_page)
        } else {
          if (callbacks.success) {
            callbacks.success(remoteData, true)
          }

          this._loaded = true

          if (callbacks.allLoaded) {
            callbacks.allLoaded()
          }
        }
      },
      error: (data, textStatus, errorThrown) => {
        this._handleError(data, textStatus, errorThrown)
      },
    }, page, per_page)
  }

  _getGlobalName() {
    return this.constructor.name
  }

  _loggedOut = () => {
    this.reset()
  }

  _userChanged() {
    if (!this.loadCollectionOnUserChanged()) {
      return
    }

    if (this.hasPermissions()) {
      if (this.paginationPowered()) {
        this._loadWithPagination()
      } else {
        Logger.debug(`${this.getPlural()} will be loaded in ${this.getModelName()}Store`)

        let url = this.getUrl()

        if (this.shouldAddUserTypeToApiUrl()) {
          url = this.addUserTypeToApiUrl(url)
        }

        const apiCall = partial(Api.request, Api.GET, url, {})

        apiCall({
          success: (remoteData) => {
            this._loaded = true

            if (Array.isArray(remoteData)) {
              for (const row of remoteData) {
                this._add(row, false)
              }
            } else {
              this._add(remoteData)
            }

            this.trigger(this.LOADED)
            this.trigger(this.CHANGE)
            this._triggerListListeners() // added cos list listeners were not getting triggered when remoteData is empty
          },
          error: (data, textStatus, errorThrown) => {
            console.log('ERROR FEATURES: ', data, textStatus, errorThrown)
            this._handleError(data, textStatus, errorThrown)
          },
        })
      }
    }
  }
}


// Add microevent to BaseStore so its children can call .trigger method
MicroEvent.mixin(BaseStore)

export default BaseStore
