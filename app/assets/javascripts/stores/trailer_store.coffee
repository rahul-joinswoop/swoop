BaseStore = require('stores/base_store').default
UserStore = require('stores/user_store')

{name: "trailer", permissions: "partner", editable: true, getUrl: "partner/trailers", editUrl: "partner/trailers"}
class TrailerStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Trailer'
    })

  isEditable: -> true

  shouldAddUserTypeToApiUrl: -> true

  hasPermissions: ->
    return UserStore.isPartner()

store = new TrailerStore()

module.exports = store
