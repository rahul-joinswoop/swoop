MicroEvent = require('microevent-github')
import Dispatcher from 'lib/swoop/dispatcher'
$=require('jquery')
triggers =
    CHANGE : "change"
    SHOW_MODAL: "show_modal"
    CLOSE_MODALS: "open_modal"

window[key] = val for key, val of triggers

ModalStore = (->
    modal=null
    props=null
    popup_props = null
    modal_component = null
    store =
      getModal:() =>
        modal
      getProps:() =>
        props

      showPopup: (props) =>
        popup_props = props
        trigger(CHANGE)

      getPopupProps: () =>
        popup_props

      hidePopup: () =>
        popup_props = null
        trigger(CHANGE)

      closeModals: ->
        $('.modal-backdrop.fade.in').remove()
        $('body').removeClass('modal-open')

        modal_component = null
        trigger(CHANGE)

    store[key] = val for key, val of triggers
    MicroEvent.mixin( store )
    trigger = store.trigger.bind(store)


    @[SHOW_MODAL] = (data) =>
      if modal != data.key
        $('.modal-backdrop.fade.in').remove()
        $('body').removeClass('modal-open')

      modal = data.key
      props = data.props
      trigger(CHANGE)

    @[CLOSE_MODALS] = (data) =>
      modal = null
      props = null
      store.hidePopup()
      trigger(CHANGE)
      $('.modal-backdrop.fade.in').remove()
      $('body').removeClass('modal-open')

    Dispatcher.register(( payload ) =>
       if not payload.eventName
         throw new Error('empty Event')
       if typeof @[payload.eventName] is 'function'
           @[payload.eventName](payload.data)

       true
    )

    return store
).bind({})()

if !window.stores?
  window.stores = {}
window.stores.ModalStore = ModalStore
module.exports = ModalStore
