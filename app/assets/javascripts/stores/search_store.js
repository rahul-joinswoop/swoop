import Api from 'api'
import BaseStore from 'stores/base_store'
import Consts from 'consts'
import { compact, keys, map, throttle, union } from 'lodash'

class SearchStore extends BaseStore {
  // OVERRIDES//
  // TODO: set up watcher to look at search list
  SEARCH_LOADED = 'search_loaded'

  SEARCH_CHANGED = 'search_changed'

  constructor(...args) {
    super(...args)
    this.resetSearchResult()
    this.search = throttle(this.search, 1000).bind(this)
  }
  // //////////////

  resetSearchResult() {
    this._search = {
      term: null,
      order: null,
      collection: [],
      filters: null,
      total: 0,
      lastSearchKey: null,
      lastSearchId: null,
      loading: false,
      allLoaded: false,
      searchPage: 1,
      pagesLoaded: {},
      maxLoadCount: Consts[`${this.getModelName().toUpperCase()}_MAX_LOAD_COUNT`],
      defaultPageSize: Consts.DEFAULT_PAGE_SIZE,
    }
  }

  reset = () => {
    super.reset()
    this.resetSearchResult()
  }

  getRecordIdsOfCurrentSearchPage() {
    const currentSearchPage = this.getSearch().searchPage
    return this.getSearch().pagesLoaded[currentSearchPage]
  }


  getRecordsOfCurrentSearchPage() {
    return map(this.getRecordIdsOfCurrentSearchPage(), id => this.getById(id))
  }

  getSizeofCurrentSearchPage() {
    if (!this.isLoadingSearch()) {
      const ids = this.getRecordIdsOfCurrentSearchPage()
      if (ids) {
        return ids.length
      }
    }
    return 0
  }

  getTotalResultSize() {
    return this._search.total
  }

  handleNextPage() {
    this.search(this._search.term, this._search.filters, this._search.order, this._search.searchPage + 1)
    this.trigger(this.CHANGE)
    this.trigger(this.SEARCH_CHANGED)
  }

  handlePrevPage() {
    this.search(this._search.term, this._search.filters, this._search.order, this._search.searchPage - 1)
    this.trigger(this.CHANGE)
    this.trigger(this.SEARCH_CHANGED)
  }

  getSearchUrl() {
    return `${this.addUserTypeToApiUrl(this.getPlural())}/search/`
  }

  getSearchCollection(customFilter = undefined) {
    const searchResultObjects = compact(map(this.getSearch().collection, id => this.getById(id)))

    return searchResultObjects.filter((row) => {
      if (customFilter) {
        return customFilter(row)
      }
      return true
    })
  }

  reloadSearch(timeout = 0) {
    if (timeout > 0) {
      this.setSearchLoading(true)
      this.trigger(this.SEARCH_CHANGED)
      setTimeout(this.reloadSearch, timeout)
      return
    }
    const currentSearchTerm = this.getSearch().term
    const currentPage = this.getSearch().searchPage
    const currentOrder = this.getSearch().order
    const currentFilters = this._search.filters
    this.resetSearchResult()
    this.search(currentSearchTerm, currentFilters, currentOrder, currentPage)
  }

  collectionStartAt() {
    return this.getPageSize() * (this._search.searchPage - 1)
  }

  collectionEndAt() {
    return this.getPageSize() * this._search.searchPage
  }

  pageStartAtToBeDisplayed() {
    return this.collectionStartAt() + 1
  }

  pageEndAtToBeDisplayed() {
    if (this.isLoadingSearch() || this.getSizeofCurrentSearchPage() < this.getPageSize()) {
      return this.collectionStartAt() + this.getSizeofCurrentSearchPage()
    }
    return this.collectionEndAt()
  }

  isLoadingSearch() {
    return this.getSearch().loading
  }

  search(value = '', filters = {}, order, page = 1, callback = null) {
    const key = JSON.stringify(value) + JSON.stringify(filters) + JSON.stringify(order)

    if (key !== this.getSearch().lastSearchKey) {
      this.resetSearchResult()
    }

    if (this.getSearch().loading) {
      return
    }

    this.setSearchTerm(value)
    this.setSearchFilters(filters)
    this.setSearchOrder(order)
    this.setLastSearchKey(key)
    this.setLastSearchId(Math.random())

    if (keys(this.getSearch().pagesLoaded).indexOf(`${page}`) > -1) {
      this.setSearchPage(page)
      this.trigger(this.CHANGE)
      return
    }

    this.setSearchLoading(true)
    this.trigger(this.SEARCH_CHANGED)

    const response = {
      success: (originalSearchId => (
        (remoteData) => {
          const remoteSearchCollection = remoteData[this.getPlural()]

          for (const row of remoteSearchCollection) {
            const prevRow = this.getById(row.id)
            if (filters && filters.fields) {
              if (prevRow) {
                row.partial = prevRow.partial
              } else {
                row.partial = true
              }
            }
            // Only add a partial object if we don't already have the full object
            if (!prevRow) {
              // This overwrites what's currently there if anything at all
              this._add(row, false)
            } else {
              // This splices in the new information
              this._update(row, false)
            }
          }

          if (originalSearchId === this.getSearch().lastSearchId) {
            if (remoteData.meta) {
              this.setSearchTotal(remoteData.meta.total)
            } else {
              this.setSearchTotal(remoteData.total)
            }

            const searchCollection = map(remoteSearchCollection, row => row.id)
            this.pushToSearchCollection(searchCollection)
            this.addPageLoaded(page, searchCollection)
            if (remoteSearchCollection &&
                  (this.getSearch().total <= this.getSearch().collection.length || this.getSearch().maxLoadCount <= this.getSearch().collection.length)) {
              this.setAllLoaded(true)
            }

            this.setSearchPage(page)
            this.setSearchLoading(false)

            this.trigger(this.SEARCH_LOADED)
            this.trigger(this.CHANGE)
          }
          if (callback) {
            callback(remoteData)
          }
        }
      ))(this.getSearch().lastSearchId, this),
      error: (remoteData, textStatus, errorThrown) => {
        this._handleError(remoteData, textStatus, 'Error occured while searching')
      },
    }

    Api.searchPaginated(this.getSearchUrl(), value, filters, page, this.getPageSize(), order, this.getModelName().toLowerCase(), response)
  }

  getSearch() {
    return this._search
  }

  setSearchTerm(term) { this._search.term = term }

  setSearchFilters(filters) { this._search.filters = filters }

  setSearchOrder(order) { this._search.order = order }

  setLastSearchKey(key) { this._search.lastSearchKey = key }

  setSearchLoading(loading) { this._search.loading = loading }

  setLastSearchId(lastSearchId) { this._search.lastSearchId = lastSearchId }

  setSearchTotal(total) { this._search.total = total }

  setAllLoaded(value) { this._search.allLoaded = value }

  setSearchPage(pageNumber) { this._search.searchPage = pageNumber }

  getSearchPage() { return this._search.searchPage }

  pushToSearchCollection(resultSearchIds) { this._search.collection = union(this._search.collection, resultSearchIds) }

  addPageLoaded(page, searchCollection) { this._search.pagesLoaded[page] = searchCollection }
}

module.exports = SearchStore
