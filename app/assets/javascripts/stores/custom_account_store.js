import BaseStore from 'stores/base_store'
import UserStore from 'stores/user_store'
import { find } from 'lodash'
import Api from 'api'

class CustomAccountStore extends BaseStore {
  constructor() {
    super({
      modelName: 'Custom_Account',
    })
  }

  hasPermissions() {
    return UserStore.isPartner()
  }

  shouldAddUserTypeToApiUrl() {
    return true
  }

  isEditable() {
    return true
  }

  getDeleteHTTPMethod() {
    return Api.DELETE
  }

  isAccountLinked(watcherId = null) {
    const account = this.getCustomAccount(watcherId)
    if (account) {
      return (account.complete && account.linked)
    }
    return false
  }

  getCustomAccount(watcherId = null) {
    if (this.getAll(watcherId).length === 0) {
      return null
    }

    const currentCustomAccountId = Object.keys(customAccountStore.getAll())[0]

    return this.getById(currentCustomAccountId)
  }

  loadGoogleAddressIfCompanyHasPlaceId() {
    if (this.businessAddress) {
      console.log('business address already loaded')

      return
    }

    const placeId = UserStore.getUser().company.location.place_id

    if (placeId) {
      new google.maps.Geocoder().geocode({ placeId }, this.setSplitBusinessAddress)
    }
  }

  setSplitBusinessAddress(results, status) {
    if (status == 'OK') {
      if (results[0]) {
        const businessAddress = {}
        const { address_components } = results[0]

        let street_number = find(address_components, component => component.types[0] === 'street_number')
        if (street_number) {
          street_number = street_number.long_name
        } else {
          street_number = ''
        }

        let street = find(address_components, component => component.types[0] == 'route')
        if (street) {
          street = street.long_name
        } else {
          street = ''
        }

        let city = find(address_components, component => component.types[0] == 'locality')
        if (city) {
          city = city.long_name
        } else {
          city = ''
        }

        let state = find(address_components, component => component.types[0] == 'administrative_area_level_1')
        if (state) {
          state = state.short_name
        } else {
          state = ''
        }

        let zipcode = find(address_components, component => component.types[0] == 'postal_code')
        if (zipcode) {
          zipcode = zipcode.long_name
        } else {
          zipcode = ''
        }

        businessAddress.address_line1 = `${street_number} ${street}`
        businessAddress.address_city = city
        businessAddress.address_state = state
        businessAddress.address_postal_code = zipcode

        this.bussinessAddress = businessAddress

        this.trigger(this.CHANGE)
      }
    }
  }

  getBusinessAddress() {
    return this.bussinessAddress || {}
  }
}

const store = new CustomAccountStore()

window.customAccountStore = store

export default store
