import Consts from 'consts'
import Api from 'api'
import React from 'react'
import Logger from 'lib/swoop/logger'
MicroEvent = require('microevent-github')
ModalStore = require('./modal_store')
NotificationStore = require('stores/notification_store')
import Dispatcher from 'lib/swoop/dispatcher'
import moment from 'moment-timezone'
UserStore = require('stores/user_store')
triggers =
    CHANGE : "change"

import SwoopWebsocket from 'SwoopWebsocket'
open = 1
attempts = 1

window[key] = val for key, val of triggers
eventStoreLoadTime = moment()
EventStore = (->
    @events={}
    @lastEventId=null
    @socket=null
    @events={}
    @pingCount = 0
    @lastTime = null
    @pingInterval = null
    notification_id = "connection_problem"
    store =
        createWebsocket: (url) =>
          return SwoopWebsocket.connect(url)

        shouldSendPing: () =>
          return Consts.JS_ENV !='local'

        closeNotification: (notification_id) =>
          Dispatcher.send(NotificationStore.CLOSE_NOTIFICATION, {
            id: notification_id
          })
        onRefresh: () =>
          Dispatcher.send(ModalStore.SHOW_MODAL,
            key: "simple",
            props: {
              title: "Refresh Page"
              confirm: 'Refresh'
              body: "You missed some updates while you were away, please refresh to get the updated information."
              onConfirm: =>
                location.reload()
                Dispatcher.send(ModalStore.CLOSE_MODALS)
            }
          )


        showError: =>
          if moment().diff(eventStoreLoadTime, 'seconds', true) > 5
            #allows previous event to continue dispatching
            setTimeout(->
              Dispatcher.send(NotificationStore.NEW_NOTIFICATION, {
                type: Consts.ERROR
                message: "You are experiencing problems connecting to the server"
                dismissable: false
                id: notification_id
              })
            , 0)


        getEvents: =>
            @events

        getSocket: =>
          @socket
        setSocket: (socket) =>
          @socket = socket

        setInitialEventId:(id) =>
          if not @lastEventId
            @lastEventId=id
          else
            raise new Error('Attempt to set initial event id, when already set')

        addEvent:(event) =>
          @lastEventId=event.id
          @events[event.id]=event




        connect: (url,onOpen=null) =>
          if @socket and @socket.readyState==1
            @socket.close()
          Logger.debug "Connecting to #{url}"
          @socket = store.createWebsocket(url)
          @socket.onopen = () =>
            Logger.debug('ws connected to websocket for ',url)
            if onOpen
              onOpen()

            if store.shouldSendPing()
              @pingInterval = setInterval( =>
                store.send({"ping": @pingCount++})
              , 5*1000)


            if !@lastTime
              @lastTime = moment()

            store.send({
              "sync":{"last_updated_at":@lastTime.toISOString(),"current_local_time": moment().toISOString(),"token":Api.getBearerToken(),user_id:UserStore.getUser()?.id,device: Consts.DEVICE}})

            store.closeNotification(notification_id)

          @socket.onmessage = (event) =>
            data=event.data
            try
              obj=JSON.parse(data)
            catch e
              Rollbar.error('Unexpected token in JSON', e, data)
              return

            if obj.refresh?
              store.onRefresh()
              return

            if obj.ws_event_id?
              if @events[obj.ws_event_id]
                return

            if obj.ws_event_created_at?
              if obj.ws_event_created_at
                @lastTime = moment(obj.ws_event_created_at)

            if obj.ws_event_id?
              @events[obj.ws_event_id] = true

            store.addEvent(obj)
            trigger(CHANGE,obj)

          @socket.onclose = (close) =>
            Logger.debug("ws close occured", close)
            if @pingInterval?
              clearInterval(@pingInterval)
            if @socket
              store.showError()

          @socket.onerror = (error) =>
            Logger.debug("ws error occured", error)
            store.showError()

        send:(msg) =>
          try
            json_msg=JSON.stringify(msg)
            if @socket? and @socket.readyState == open
              test = @socket.send(json_msg)
              return true
          catch error
            Rollbar.error(error, { msg: msg })

          return false

        close: =>
          Logger.debug "cancelServerUpdatesRegistration called"
          if @socket
            Logger.debug "cancelServerUpdatesRegistration closing socket"
            @socket.close()
            @socket=null

    store[key] = val for key, val of triggers
    MicroEvent.mixin( store )
    trigger = store.trigger.bind(store)


    return store

).bind({})()

module.exports = EventStore
