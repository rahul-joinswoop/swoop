import BaseStore from 'stores/base_store'

class EtaExplanationStore extends BaseStore {
  constructor() {
    super({
      modelName: 'EtaExplanation',
    })
  }

  getUrl() {
    return 'eta_explanations'
  }
}

export default new EtaExplanationStore()
