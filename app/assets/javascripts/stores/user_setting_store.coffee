BaseStore = require('stores/base_store').default
UserStore = require('stores/user_store')
import Api from 'api'
import EventRegistry from 'EventRegistry'
import Consts from 'consts'
import { keys, map, values, zipObject } from 'lodash'

class UserSettingStore extends BaseStore
  constructor: ->
    super({
      modelName: 'UserSetting'
    })

    #This stores a key -> Value mapping so we don't have to do a lookup everytime
    @_userSettingByKeyIndex = null

    #This stores the watchers looking for keys that don't exist yet, when they
    #get created we will trigger them
    #TODO: EventRegistry needs a way to watch for property values changing
    #HACK: This will fill up with old watchers and isn't a good way to do this
    @_noncreatedWatchers = {}
  isEditable: -> true

  getUrl: -> 'user/settings'

  getEditUrl: -> @getUrl()

  getModelName: -> 'userSetting'

  showNotification: -> false

  getDeleteHTTPMethod: -> Api.DELETE

  getUserSettingByKey: (key, watcherId = null) =>
    if !@_userSettingByKeyIndex && @isAllLoaded()
      @_userSettingByKeyIndex = zipObject(
        map(values(@getAll()), 'key'), keys(@getAll())
      )

    if @_userSettingByKeyIndex
      id = @_userSettingByKeyIndex[key]

      if id
        return @get(id, 'value', watcherId)

    #Key wasn't found so wait for it to arrive
    if watcherId?
      if !@_noncreatedWatchers[key]?
        @_noncreatedWatchers[key] = []
      if @_noncreatedWatchers[key].indexOf(watcherId) == -1
        @_noncreatedWatchers[key].push(watcherId)

    return

  getUserSettingByKeyAsArray: (key, watcherId) =>
    value = @getUserSettingByKey(key, watcherId)

    if value?
      arr = JSON.parse(value)
      if Array.isArray(arr)
        return arr

    return null

  getUserSettingByKeyAsObject: (key, watcherId) =>
    value = @getUserSettingByKey(key, watcherId)

    if value?
      obj = JSON.parse(value)
      if typeof obj == 'object'
        return obj
    return null

  getUserSettingByKeyAsBool: (key, watcherId, defaultVal = null) =>
    value = @getUserSettingByKey(key, watcherId)

    if value?
      obj = JSON.parse(value)
      if typeof obj == 'boolean'
        return obj
    return defaultVal

  getUserSettingByKeyForFilterSearch: (key, watcherId) =>
    obj = @getUserSettingByKeyAsObject(key, watcherId)
    if obj?
      return null if obj.isAllSelected
      obj.selected = [] if !obj.selected
      if obj.isUnsetSelected
        return obj.selected.concat([null])
      else
        return obj.selected

  setUserSettings: (key, value, options) =>
    #TODO: perhaps update locally here if performance becomes an issue
    obj = {
      key: key
      value: JSON.stringify(value)
    }
    if options?.callbacks?
      obj.callbacks = options.callbacks
    @addItem(obj)

  _addRowToInternalCollection: (data) ->
    if @_userSettingByKeyIndex?
      @_userSettingByKeyIndex[data.key] = data.id

    super(data)

    #HACK: should build up event registry to handle this, will trigger orphaned watchers
    if @_noncreatedWatchers[data.key]?
      for watcher in @_noncreatedWatchers[data.key]
        EventRegistry.triggerRerender(watcher)
      delete @_noncreatedWatchers[data.key]

  getActiveTabSettings: (watcherId = null) ->
    #Grab from settings if available
    setting = @getUserSettingByKey(Consts.USER_SETTING.ACTIVE_TAB, watcherId)
    if setting?
      value = JSON.parse(setting)
      if value?
        return value

    if UserStore.isSwoop()
      return ["id", "total_time", "company", "customer_name", "service", "invoice_vehicle_category_id", "partner", "alerts", "eta", "status_age", "status", "dispatcher"]
    else if UserStore.isPartner()
      return ["id", "created_time", "driver", "truck", "account", "po", "service", "location", "drop", "eta2", "status"]
    else if UserStore.isFarmersOwned()
      return ["id", "eta", "created_time", "customer_name", "service", "location", "drop", "status"]
    else if UserStore.isTesla()
      return ["id", "l6vin", "eta", "partner", "customer_name", "service", "location", "drop", "status", "dispatcher"]
    else if UserStore.isFleetInHouse()
      return ["id", "eta", "created_time", "customer_name", "service", "location", "drop", "partner", "status"]
    else
      return ["id", "eta", "created_time", "customer_name", "service", "location", "drop", "status"]

  getDoneTabSettings: (watcherId = null) ->
    #Grab from settings if available
    setting = @getUserSettingByKey(Consts.USER_SETTING.DONE_TAB, watcherId)
    if setting?
      value = JSON.parse(setting)
      if value?
        return value

    if UserStore.isSwoop()
      return ["id", "completed_date", "company", "customer_name", "service", "invoice_vehicle_category_id", "partner", "status", "dispatcher"]
    else if UserStore.isPartner()
      return ["id", "completed_date", "driver", "truck", "account", "po", "service", "location", "drop", "status"]
    else if UserStore.isFarmersOwned()
      return ["id", "completed_date", "customer_name", "service", "location", "drop", "status"]
    else if UserStore.isTesla()
      return ["id", "l6vin", "partner", "customer_name", "service", "location", "drop", "status", "dispatcher"]
    else if UserStore.isEnterprise()
      return ["id", "partner", "customer_name", "service", "location", "drop", "status"]
    else
      return ["id", "completed_date", "customer_name", "service", "location", "drop", "status"]


store = new UserSettingStore()

module.exports = store
