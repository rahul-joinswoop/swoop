

import BaseStore from 'stores/base_store'
import UserStore from 'stores/user_store'

class LookerDashboardStore extends BaseStore {
  constructor() {
    super({
      modelName: 'LookerDashboard',
    })
  }

  getUrl() {
    return 'looker/dashboards'
  }

  shouldAddUserTypeToApiUrl() {
    return true
  }

  hasPermissions() {
    return !UserStore.isSwoop() || UserStore.isRoot()
  }
}

const store = new LookerDashboardStore()

window.lookerDashboardStore = store

export default store
