BaseStore = require('stores/base_store').default
UserStore = require('stores/user_store')

class StorageTypeStore extends BaseStore
  constructor: ->
    super({
      modelName: 'StorageType'
    })

  hasPermissions: ->
    UserStore.isPartner()

  isEditable: -> true

  getUrl: ->
    if UserStore.isPartner()
      "partner/storage_types"
    else
      "fleet/storage_types"

  getEditUrl: -> @getUrl()

  getModelName: ->
    'storage_type'

store = new StorageTypeStore()

module.exports = store
