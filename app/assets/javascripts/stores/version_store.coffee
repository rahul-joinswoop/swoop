BaseStore = require('stores/base_store').default
ServiceStore = require('stores/version_store')
import Api from 'api'
import { map, maxBy, values } from 'lodash'

#{name: "version", permissions: "root", editable: true,  editUrl: "root/versions"}
class VersionStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Version'
    })

    @current_version = window?.VERSION
    @swoop_env = ''

    @REFRESH_REQUIRED = 'refresh_required'

    @_listAllByName = {}

    @WEB_LIST = 'web_list'
    @LOADING_WEB_LIST = false

    @ANDROID_LIST = 'android_list'
    @LOADING_ANDROID_LIST = false

    @MOBILE_LIST = 'mobile_list'
    @LOADING_MOBILE_LIST = false

  hasPermissions: ->
    UserStore.isSwoop()

  isEditable: ->
    UserStore.isSwoop()

  loadCollectionOnUserChanged: -> false

  getEditUrl: ->
    "root/#{@getUrl()}"

  paginationPowered: -> true

  getWebVersions: (watcherId) =>
    @_getVersionsByName(watcherId, 'web')

  getMobileVersions: (watcherId) =>
    @_getVersionsByName(watcherId, 'mobile')

  getIOSVersions: (watcherId) =>
    @_getVersionsByName(watcherId, 'ios')

  getAndroidVersions: (watcherId) =>
    @_getVersionsByName(watcherId, 'android')

  _getVersionsByName: (watcherId, name) =>
    upperCaseName = name.toUpperCase()

    if watcherId
      @_registerListListener(watcherId, @["#{upperCaseName}_LIST"], ['version', 'description', 'forced'])

    if !@_listAllByName[upperCaseName]
      @_listAllByName[upperCaseName] = @_loadAllByName(name)

    map(@_listAllByName[upperCaseName], (versionId) => @getById(versionId))

  _loadAllByName: (name, page = 1, per_page = @getLoadingPaginationSize()) =>
    upperCaseName = name.toUpperCase()

    if !@["LOADING_#{upperCaseName}_LIST"] && UserStore.isSwoop()
      @["LOADING_#{upperCaseName}_LIST"] = true

      Api.versionsRequestPaginated({
        success: (versionsByName) =>
          for version in versionsByName
            @_add(version)

          if versionsByName.length == per_page
            @_loadAllByName(name, ++page, per_page)

          @_listAllByName[upperCaseName] = map(versionsByName, 'id')

          @_triggerListListeners(@["#{upperCaseName}_LIST"])

          @["LOADING_#{upperCaseName}_LIST"] = false
        ,
        error: (data, textStatus, errorThrown) =>
          @["LOADING_#{upperCaseName}_LIST"] = false

          @_handleError(data, textStatus, errorThrown)
      }, page, per_page, name)

      null

  loadingVersion: (name) =>
    @["LOADING_#{name.toUpperCase()}_LIST"]

  addRemoteItem: (data) ->
    object = data.target

    if UserStore.isSwoop() || object?.name == 'web'
      super(arguments...)

    if object?.name == 'web' && object.version != @current_version
      @trigger(@REFRESH_REQUIRED)

  fetchVersion: ->
    Api.getVersion(
      success: (data) =>
        @_add(data)

        if not @current_version
          @current_version = data.version
        else if @current_version != data.version
          @trigger(@REFRESH_REQUIRED)

        @swoop_env=data.swoop_env
        console.log '%c Swoop ' + @swoop_env + ' ' + @current_version + ' ', 'background: #E91E63; color: #FFF; font-family: monospace;'

        @trigger(@CHANGE)

      error: (data, textStatus, errorThrown) =>
        @_handleError(data, textStatus, errorThrown)
        @trigger(@CHANGE)
    )

  getMostRecentVersion: ->
    maxBy(values(@getAll()), (version) ->
      version.id)

  isWeb: (version) ->
    version.name == 'web'

  isAndroid: (version) ->
    version.name == 'android'

  isMobile: (version) ->
    version.name == 'mobile'

  _getListsObject: =>
    return [
      {
        list: @_listAllByName['WEB'],
        validator: @isWeb,
        triggerName: @WEB_LIST
      }
      {
        list: @_listAllByName['ANDROID'],
        validator: @isAndroid,
        triggerName: @ANDROID_LIST
      }
      {
        list: @_listAllByName['MOBILE'],
        validator: @isMobile,
        triggerName: @MOBILE_LIST
      }
    ]

store = new VersionStore()

module.exports = store
