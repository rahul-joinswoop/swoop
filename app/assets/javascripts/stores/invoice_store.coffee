import Logger from 'lib/swoop/logger'
import Dispatcher from 'lib/swoop/dispatcher'
MicroEvent = require('microevent-github')
import Api from 'api'
UserStore = require('stores/user_store')
import Consts from 'consts'
import Utils from 'utils'
NotificationStore = require('stores/notification_store')
ProviderStore = require('stores/provider_store')
JobStore = require('stores/job_store')
import { difference, filter, forEach, isEmpty, keys, map, reduce, sortBy, union, uniq, values, without } from 'lodash'
import moment from 'moment-timezone'
PaymentStore = require('stores/payment_store')
triggers =
    CHANGE : "change"
    CHANGE_BY_JOB: "changeByJob"
    INVOICE_UPDATE : "remote_update"
    INVOICE_CHANGED: "invoice_changed"
    SHOULD_RELOAD: "reload"
    SEARCH_FINISHED: "search_finished"
    SWOOP_UNDO_APPROVAL_FOR_PARTNER: 'swoop_undo_approval_for_partner'
    GENERATE_PDF: 'generate_pdf'
    INVOICE_PDF_URL_READY: 'invoice_pdf_url_ready'
    POLL_FOR_PDF_READINESS: 'poll_for_pdf_readiness'

window[key] = val for key, val of triggers


INVOICE_STATES = [Consts.ICREATED, Consts.IPARTNERNEW, Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT, Consts.IFLEETAPPROVED, Consts.IFLEETDOWNLOADED, Consts.ISWOOPAPPROVED, Consts.IPARTNEROFFLINENEW, Consts.ISWOOPNEW, Consts.ISWOOPSENT]

#TODO: include include_job=false or true if we want the whole job object
InvoiceStore = (->
    invoices={}
    loading=[]
    loaded=[]
    searchInvoices=[]
    searchTotal=0
    searchPage=1
    loading_search_invoices=false
    allSearchInvoices=false
    lastQueryKey=null
    updatingInvoices = {}
    downloadingInvoices = {}
    store =
      _setCost: (invoice) ->
        invoice.cost = reduce(invoice.line_items, ((a, b) ->
          sum = a
          if not b.deleted_at?
            sum += (parseFloat(b.tax_amount) || 0) + (parseFloat(b.net_amount) || 0)
          sum
        ), 0)

      addInvoice: (invoice) ->
        if invoice? and invoices[invoice.id]? and moment(invoice.updated_at).isBefore(moment(invoices[invoice.id].updated_at))
          return

        store._setCost(invoice)
        Logger.debug("adding invoice ", invoice)
        invoices[invoice.id] = invoice

        trigger(CHANGE+"_"+invoice.id)
        trigger(CHANGE_BY_JOB+"_"+invoice.job_id)

      addPartialInvoices: (invoices) ->
        for invoice in invoices
          full_invoice = @updateInvoice(invoice)
          if full_invoice?
            trigger(CHANGE+"_"+full_invoice.id)
            trigger(CHANGE_BY_JOB+"_"+full_invoice.job_id)

        trigger(CHANGE)

      deleteInvoice: (invoice) =>
        if invoices[invoice.id]?
          delete invoices[invoice.id]

      updateInvoice: (invoice) =>
        if invoice.id && invoices[invoice.id]
          invoices[invoice.id] = $.extend(invoices[invoice.id], invoice)

      getInvoiceByUUID: (uuid) =>
        rets = filter(invoices, (invoice) -> invoice.uuid == uuid)
        if rets.length > 0
          return rets[0]

      loadInvoiceByUuid: (uuid) =>
         Api.invoice(uuid, {
            success: (data) =>
              invoices[data.id] = data
              trigger(CHANGE)
              trigger(CHANGE+"_"+data.id)
              trigger(CHANGE_BY_JOB+"_"+data.job_id)

            error: (data) =>
              Logger.debug("Got Invoice error",data)
          })
      isUpdatingInvoice: (id) =>
        return updatingInvoices[id]

      isDownloadingInvoice: (id) =>
        return downloadingInvoices[id]

      getInvoiceById: (id, fetch) =>
        if !id?
          return null
        if invoices[id]?
          return invoices[id]
        else if (not fetch? or fetch) and (UserStore.isPartner() || UserStore.isSwoop())
          if !store.invoiceRequests
              store.invoiceRequests = []
          if id not in store.invoiceRequests
            store.invoiceRequests.push(id)
            Api.invoiceRequest(Api.GET, Utils.getEndpoint(UserStore),id, {
              success: (data) =>
                store.addInvoice(data)
                trigger(CHANGE)
              error: (data) =>
                Logger.debug("Got Invoice error",data)
                store.invoiceRequests = without(store.invoiceRequests, id)
            })
        return null

      findLineItemByType: (invoiceId, lineItemType) =>
        invoice = store.getInvoiceById(invoiceId)

        lineItems = sortBy(
          filter(invoice?.line_items, (line_item) -> line_item.description == lineItemType),
          'id'
        )

        lineItems?.slice(-1)[0]

      isRecipientSwoop: (invoice) ->
        invoice.recipient?.name == Consts.SWOOP

      isRecipientTesla: (invoice) ->
        invoice.recipient?.name == Consts.TESLA

      loadInvoicesPaginated: (toLoad, page) =>
        Api.invoicesPaginated(Utils.getEndpoint(UserStore), page, toLoad, {
            success: (data) =>
              for invoice in data.invoices
                store.addInvoice(invoice)
              if data.invoices.length < 25
                loading = difference(loading, toLoad)
                loaded = union(loaded, toLoad)
              else
                setTimeout(( =>
                  store.loadInvoicesPaginated(toLoad, page+1)
                ), 100)
              trigger(CHANGE)
            error: (data) =>
              Logger.debug("Got Invoice error",data)
          })
      loadInvoices:(states) =>
        if not states?
          return
        toLoad = []
        Logger.debug("fff, ", states, loading)
        for state in states
          if state not in loaded and state not in loading
            toLoad.push(state)
            loading.push(state)
        if toLoad.length > 0
          store.loadInvoicesPaginated(toLoad, 1)

      finishedLoadingSearchJobs: =>
        allSearchInvoices

      clearSearchInvoices: () =>
        Logger.debug("ddd clearing search page")
        loading_search_invoices = false
        allSearchInvoices = false
        searchInvoices = []
        searchPage = 1
        searchTotal = 0
        trigger(CHANGE)

      getSearchInvoices: () =>
        if searchInvoices?
          map(searchInvoices, (invoice_id) ->
            store.getInvoiceById(invoice_id)
          )
        else
          []
      getSearchTotal: () =>
        searchTotal
      getSearchPage: () =>
        searchPage


      loadSearchInvoices:(states, filters, search, orders, page) =>
        if not states?
          return


        queryKey = JSON.stringify(states)+JSON.stringify(filters)+search+JSON.stringify(orders)
        Logger.debug("CHECKING QUERIES: ", queryKey, lastQueryKey)
        if queryKey != lastQueryKey
          store.clearSearchInvoices()
        else if loading_search_invoices
          return
        else if allSearchInvoices
          return


        spage = page
        if not spage?
          spage = searchPage

        lastQueryKey = queryKey
        loading_search_invoices = true
        extras = {}
        for name, thisFilter of filters
          extras[name] = thisFilter
        if search?
          extras["job_search"] = search
        if orders?
          for name, order of orders
            extras[name] = order

        Logger.debug("MAKING REQUEST QUERIES")
        Api.invoicesPaginated(Utils.getEndpoint(UserStore), spage, states, {
            success: (data) =>

              for invoice in data.invoices
                store.addInvoice(invoice)

              if queryKey == lastQueryKey

                searchTotal = data.total

                for invoice in data.invoices
                  searchInvoices.push(invoice.id)
                searchInvoices = uniq(searchInvoices)
                if data.invoices.length < Consts.JOBS_PER_PAGE_COUNT
                  allSearchInvoices = true
                else
                  loading_search_invoices = false
                if not page?
                  searchPage++

              trigger(CHANGE)
              trigger(SEARCH_FINISHED)
            error: (data) =>
              Logger.debug("Got Invoice error",data)
          }, extras)


      areLoaded: (states) =>
        for state in states
          if state not in loaded
            return false
        return true

      # TODO check if it's being used
      getInvoicesByAccount: (states, account_id, componentId = null) =>
        if states? and account_id?
          accountName = AccountStore.get(account_id, 'name', componentId)

          return filter(values(invoices), (invoice) =>
            matches = false
            if accountName == "Cash Call"
              if invoice.recipient.recipient_type == "User"
                matches = true
            else
              matches = invoice.recipient.id == account_id

            invoice.state in states and matches
          )
        return []

      # TODO check if it's being used
      getInvoicesByPartner: (states, partner_id, componentId = null) =>
        providerCompany = ProviderStore.getProviderById(partner_id, 'company', componentId)
        if states? and provider? and provider.company?
          return filter(values(invoices), (invoice) => invoice.state in states and invoice.sender.id == provider.company.id)
        return []

      getInvoices: (states) =>

        if states?
          return filter(values(invoices), (invoice) => invoice.state in states)
        return values(invoices)
    store[key] = val for key, val of triggers
    MicroEvent.mixin( store )
    trigger = store.trigger.bind(store)

    sendNotification = (msg, type, perpetual) =>
      setTimeout (=>
        obj = {
          type: if type then type else Consts.INFO
          message: msg
          dismissable: true
          timeout: if type == Consts.ERROR then 0 else 2000
        }
        if perpetual
          delete obj["timeout"]

        Dispatcher.send(NotificationStore.NEW_NOTIFICATION, obj)
      ), 0


    handleError = (data, textStatus, errorThrown) =>
      Dispatcher.send(NotificationStore.NEW_NOTIFICATION,
          data: data
          textStatus: textStatus
          errorThrown: errorThrown
      )

    @[INVOICE_UPDATE] = (invoice) =>
      Logger.debug("In invoice update")

      if invoice.deleted_at?
        Logger.debug("In invoice delete")
        store.deleteInvoice(invoice)
      else
        Logger.debug("In invoice add")
        store.addInvoice(invoice)

      if invoice.state in [
        Consts.IFLEETREJECTED, Consts.ISWOOPREJECTEDPARTNER
        ] and UserStore.isPartner()
        Utils.sendWarningNotification(
          "Invoice #" + invoice.job_id + " has been rejected by " +
          invoice.recipient?.name + "!"
        )
        trigger(SHOULD_RELOAD)

      trigger(CHANGE)

    @[INVOICE_CHANGED] = (data) =>
        Logger.debug("INVOICE CHANGED: ", data)
        type = Api.POST

        manualPayments = []

        if data.payments
          forEach(data.payments, (payment) =>
            if payment.type
              manualPayments.push(payment)
          )


        if data.id?
          #store.updateInvoice(data)
          updatingInvoices[data.id] = data
          type = Api.UPDATE
          trigger(CHANGE)
          trigger(CHANGE+"_"+data.id)
          trigger(CHANGE_BY_JOB+"_"+data.job_id)


        apiMethod = Api.invoiceRequest
        if data.id? && data.paid? && keys(data).length == 2
          type = Api.POST
          apiMethod = Api.invoicePaidRequest

        if data.rate_type
          clearSearchResults = true
          store.clearSearchInvoices()
        apiMethod(type, Utils.getEndpoint(UserStore), data.id,{
            success: (data) =>
                delete updatingInvoices[data.id]

                sendNotification("Invoice #"+data.job_id+" has been edited.", Consts.SUCCESS)
                store.addInvoice(data)

                if clearSearchResults
                  store.clearSearchInvoices()
                  trigger(SHOULD_RELOAD)

                trigger(CHANGE)
                trigger(CHANGE+"_"+data.id)
                trigger(CHANGE_BY_JOB+"_"+data.job_id)

                Tracking.trackJobEvent("Edit Invoice", data.job_id, {
                  category: "Invoices"
                })

                if !isEmpty(manualPayments)
                  forEach(manualPayments, (payment) =>
                    label = payment.type
                    if payment.payment_method?
                      label += ' - ' + payment.payment_method
                    Tracking.track('Manual Payment', {
                      category: 'Payments'
                      label: payment.type + ' - ' + payment.payment_method
                      value: (-payment.amount).toString()
                      service: JobStore.get(data.job_id, "service")
                      account: JobStore.get(data.job_id, "account")?.name
                      jobId: data.job_id
                    })
                  )


            error: (one, two, three) =>
              delete updatingInvoices[data.id]
              invoice = store.getInvoiceById(data.id)
              sendNotification("Invoice #"+invoice?.job_id+" update failed.  Please try again.", Consts.ERROR)
              handleError(one, two, three)
              trigger(CHANGE)
            },
            invoice: data, companyId: data?.sender?.id
        )

    @[GENERATE_PDF] = (invoiceOptions) ->
      invoice = invoiceOptions.invoice
      responseContentDisposition = invoiceOptions.responseContentDisposition

      Logger.debug('Triggering Invoice PDF generation', invoice.uuid)

      downloadingInvoices[invoice.id] = true
      trigger(CHANGE+"_"+invoice.id)
      trigger(CHANGE_BY_JOB+"_"+invoice.job_id)

      Api.generateInvoicePdf(invoice.uuid, {
        success: (invoicePdf) =>
          Dispatcher.send(
            POLL_FOR_PDF_READINESS,
            invoicePdf: invoicePdf
            responseContentDisposition: responseContentDisposition
            attempts: 1
          )
        error: (data, textStatus, errorThrown) =>
          Logger.debug('Error occurred while trying to download invoice', invoice.uuid)

          delete downloadingInvoices[invoice.id]
          trigger(CHANGE+"_"+invoice.id)
          trigger(CHANGE_BY_JOB+"_"+invoice.job_id)

          Utils.sendNotification('Failed to download the invoice. Please try again.', Consts.WARNING)
        }
      )

    @[POLL_FOR_PDF_READINESS] = (invoicePdfPolling) ->
      TOTAL_ATTEMPTS = 10

      invoicePdf = invoicePdfPolling.invoicePdf
      attempts   = invoicePdfPolling.attempts
      responseContentDisposition = invoicePdfPolling.responseContentDisposition

      Logger.debug('Polling for Invoice PDF readiness', invoicePdf.invoice_id)

      Api.showInvoicePdf(invoicePdf.invoice_id, invoicePdf.id, responseContentDisposition, {
        success: (invoicePdf) =>
          if invoicePdf.public_url?
            Dispatcher.send(INVOICE_PDF_URL_READY,
              invoiceId: invoicePdf.invoice_id
              invoicePdfPublicUrl: invoicePdf.public_url
            )
          else if attempts >= TOTAL_ATTEMPTS
            Logger.debug('Error occurred while polling for Invoice PDF readiness', invoicePdf.invoice_id)

            delete downloadingInvoices[invoicePdf.invoice_id]
            trigger(CHANGE+"_"+invoicePdf.invoice_id)

            Utils.sendNotification('Failed to download the invoice. Please try again.', Consts.WARNING)
          else
            setTimeout(( =>
              Dispatcher.send(
                POLL_FOR_PDF_READINESS,
                invoicePdf: invoicePdf
                attempts: ++attempts
                responseContentDisposition: responseContentDisposition)
              ), 2000) # if the PDF url is not ready, retry in 2 seconds
        error: (data, textStatus, errorThrown) =>
          Logger.debug('Error occurred while polling for Invoice PDF readiness', invoicePdf.invoice_id)

          delete downloadingInvoices[invoicePdf.invoice_id]
          trigger(CHANGE+"_"+invoicePdf.invoice_id)

          Utils.sendNotification('Failed to download the invoice. Please try again.', Consts.WARNING)
      })

    @[INVOICE_PDF_URL_READY] = (invoicePdfReadyObject) ->
      delete downloadingInvoices[invoicePdfReadyObject.invoiceId]
      trigger(CHANGE+"_"+invoicePdfReadyObject.invoiceId)

      link = document.createElement("a")
      link.download = "download"
      link.href = invoicePdfReadyObject.invoicePdfPublicUrl

      if Utils.isIE() # hack for IE11
        Utils.injectIEFileDownloadHack(link, invoicePdfReadyObject.invoicePdfPublicUrl)

      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link)

    @[SWOOP_UNDO_APPROVAL_FOR_PARTNER] = (data) =>
      Logger.debug('Swoop undo approval for partner', data)

      Api.invoiceRequest(Api.UPDATE, Utils.getEndpoint(UserStore), data.id, {
          success: (data) =>
            trigger(CHANGE)
          error: (data, textStatus, errorThrown) =>
            Logger.debug('Error occurred while trying swoop tried to undo approval for invoice', data.id)
            handleError(data, textStatus, errorThrown)
            trigger(CHANGE)
          },
          invoice_id: data.id
      )

    Dispatcher.register(( payload ) =>
       Logger.debug("in payload of vehicle store dispatcher",payload)
       if not payload.eventName
         throw new Error('empty Event')
       if typeof @[payload.eventName] is 'function'
           @[payload.eventName](payload.data)
       else
         Logger.debug('unknown event received in InvoiceStore', payload)
       true
    )

    PaymentStore.bind(PaymentStore.CHANGE, (payment) ->
      #TODO: recalculate balance
      if payment
        trigger = false
        invoice = getInvoiceById(payment.invoice_id)
        if inovice
          if invoice.payment_ids.indexOf(payment.id) == -1 && payment.deleted_at == null
            invoice.payment_ids.push(payment.id)
            trigger = true
          else if invoice.payment_ids.indexOf(payment.id) > -1 && payment.deleted_at?
            invoice.payment_ids = without(invoice.payment_ids, payment.id)
            trigger = true
          if trigger
            trigger(CHANGE, invoice)
            trigger(CHANGE+"_"+invoice.id, invoice)
            trigger(CHANGE_BY_JOB+"_"+invoice.job_id)
    )

    window.istore = store
    return store
).bind({})()


module.exports = InvoiceStore
