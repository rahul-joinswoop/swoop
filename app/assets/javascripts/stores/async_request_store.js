import Api from 'api'
import UserStore from 'stores/user_store'
import { partial, pickBy } from 'lodash'
import Consts from 'consts'

class AsyncRequestStore {
  constructor() {
    // Responsible for looking up ws callback
    this.lookup = {}
    // Responsible for looking up data on http response (if WS came back first)
    this.dataLookup = {}
  }

  genericRequest = (apiCall, response) => {
    const timeout = setTimeout(() => {
      response.error()
    }, Consts.WS_RESPONSE_TIMEOUT)

    // Reponsible for clearing timeout on passing through success to calling component
    const wsSuccess = (wsData) => {
      clearTimeout(timeout)
      response.success(wsData)
    }

    // Reponsible for clearing timeout on passing through error to calling component
    const error = (data) => {
      clearTimeout(timeout)
      response.error(data)
    }

    // Makes the api request to get the async request id and processes it
    apiCall({
      success: (data) => {
        const wsResponse = { success: wsSuccess, error }
        // Check if WS came back first
        const previousWsResponseLookup = this.dataLookup[data?.async_request?.id]
        if (previousWsResponseLookup) {
          // If WS data did come back first, then handle it immediately
          this.handleWSResponse(previousWsResponseLookup.data, wsResponse)
        } else {
          // If no WS response yet, set up a callback on a lookup for when it does
          this.lookup[data.async_request.id] = wsResponse
        }
      },
      error,
    })
  }

  lookupCoverage = (payload, response) => {
    const apiCall = partial(Api.lookupCoverage, UserStore.getEndpoint(), payload)
    this.genericRequest(apiCall, response)
  }

  requestDropLocationOptions = (payload, response) => {
    const apiCall = partial(Api.requestDropLocationOptions, UserStore.getEndpoint(), payload)
    this.genericRequest(apiCall, response)
  }

  searchPoliciesNew = (jobId, search_terms, response) => {
    const apiCall = partial(Api.searchPoliciesNew, UserStore.getEndpoint(), jobId, search_terms)
    this.genericRequest(apiCall, response)
  }

  searchPolicy = (number, jobId, response) => {
    const apiCall = partial(Api.searchPolicies, UserStore.getEndpoint(), number, jobId)
    this.genericRequest(apiCall, response)
  }

  getPolicyDetails = (number, response) => {
    const apiCall = partial(Api.policyDetails, UserStore.getEndpoint(), number)
    this.genericRequest(apiCall, response)
  }

  getPolicyCoverage = (number, pcc_vehicle_id, pcc_driver_id, response) => {
    const apiCall = partial(Api.policyCoverage, UserStore.getEndpoint(), number, pcc_vehicle_id, pcc_driver_id)
    this.genericRequest(apiCall, response)
  }

  chargeInvoice = (id, charge_obj, response) => {
    const apiCall = partial(Api.invoiceCharge, id, charge_obj)
    return this.genericRequest(apiCall, response)
  }

  deleteStripeAccount = (data, response) => {
    const apiCall = partial(Api.deleteStripeAccount, data)
    return this.genericRequest(apiCall, response)
  }

  invoicePaymentRefund = (invoice_id, payment_id, refund_obj, response) => {
    const apiCall = partial(Api.invoicePaymentRefund, invoice_id, payment_id, refund_obj)
    return this.genericRequest(apiCall, response)
  }

  rscRequestMoreTime = (job_id, response) => {
    const apiCall = partial(Api.rscRequestMoreTime, job_id)
    return this.genericRequest(apiCall, response)
  }

  handleWSResponse = (asyncRequest, response) => {
    if (asyncRequest?.error) {
      response.error(asyncRequest)
    } else {
      response.success(asyncRequest?.data)
    }
    // Clean up any data that we had stored
    delete this.lookup[asyncRequest.id]
    delete this.dataLookup[asyncRequest.id]
  }

  // Since users get WS data for all messages we should clean up to make sure we're not
  // keeping around to many unnecessary ones
  cleanupDataLookup = () => {
    // Remove any data that's been around for longer than Consts.WS_RESPONSE_TIMEOUT
    this.dataLookup = pickBy(this.dataLookup, (value) => {
      const limitDate = new Date(value.date?.getTime() + Consts.WS_RESPONSE_TIMEOUT)
      return new Date() < limitDate
    })
  }

  addRemoteItem = (msg) => {
    const asyncRequest = msg.target
    const response = this.lookup[asyncRequest.id]
    if (response != null) {
      // If the http request has set up a lookup id for a response, callback
      this.handleWSResponse(asyncRequest, response)
    } else {
      this.cleanupDataLookup()
      // Potentially WS came back before http response
      // Store for later
      this.dataLookup[asyncRequest.id] = {
        data: asyncRequest,
        time: new Date(),
      }
    }
  }
}

export default new AsyncRequestStore()
