import Consts from 'consts'
import Api from 'api'
import { filter, min, partial, some, sortBy, union, without } from 'lodash'

BaseStore = require('stores/base_store').default
AuctionStore = require('stores/auction_store').default
UserStore = require('stores/user_store')
SymptomStore = require('stores/symptom_store')
import TagStore from 'stores/tag_store'

class RecommendedProvidersStore extends BaseStore
  constructor: ->
    super({
      modelName: 'RecommendedProvider'
    })

    @SORT_BY_JOB_DISTANCE = 'job_distance'
    @SORT_BY_SUBMITTED_ETA = 'submitted_eta'
    @SORT_BY_NETWORK_STATUS = 'status'
    @SORT_BY_CUSTOM_AMY = "amy_sort"

    @_recommendedProviders = []
    @_providersForStore = null
    @_currentSortKey = @SORT_BY_JOB_DISTANCE
    @_isLoaded = false
    @_loading = false
    @_jobWithAuction = null
    @_bidsEnqueuedForSort = false

  isEditable: ->
    false

  loadCollectionOnUserChanged: ->
    false

  watchBidChangesFor: (job) ->
    @_jobWithAuction = job

    if @_jobWithAuction.auction?.bids?.length
      for bid in @_jobWithAuction.auction?.bids
        AuctionStore.bind(AuctionStore.BID_UPDATED_ON_CLIENT + '_' + bid.id, partial(@sortIfNeeded, job))
    else
      Rollbar.error('Attempt to watch nonexistent bids', { job: job })

  sortIfNeeded: (job) =>
    if @_currentSortKey == @SORT_BY_SUBMITTED_ETA || @_currentSortKey == @SORT_BY_CUSTOM_AMY
      if !@_bidsEnqueuedForSort
        @_bidsEnqueuedForSort = true
        setTimeout(partial(@sortNewBids, job), 1000) # wait for bids during 1 sec, then trigger sort
    else
      @_triggerListListeners()

  sortNewBids: (job) =>
    @_sortRecommendedProviders(@getRecommendedProviders(), @_currentSortKey, job)
    @_triggerListListeners()
    @_bidsEnqueuedForSort = false

  getSortedList: (sortKey = @SORT_BY_JOB_DISTANCE, job, watcherId = null, chosenProviderId = null) =>
    # this is only applicable for Fleet in House or Swoop
    if UserStore.isPartner() || UserStore.isFleetManaged()
      return

    # register the watcherId
    if(watcherId)
      @_registerListListener(watcherId)

      if UserStore.isTesla() and job?.symptom_id
        # In this case ^, force loading the symptom to be able to do the check on @_shouldPushTeslaMobileTireServiceToTheTop
        SymptomStore.get(job.symptom_id, 'id', watcherId)

    # return the collection if it's currently loading
    if @_loading
      return @getRecommendedProviders()

    # if already loaded by the given key, just return the collection
    if @isLoaded() && (@_currentSortKey == sortKey)
      return @getRecommendedProviders()

    # if loaded but sort key is different from the current one, means we only need to sort it again and trigger list listeners
    if @isLoaded() && @_currentSortKey != sortKey
      @_sortRecommendedProviders(@getRecommendedProviders(), sortKey, job)

      @_triggerListListeners()

      return

    # if it reaches this, it means we need to load the collection for the first time
    if job?
      @_loading = true

      Api.fetchRecommendedProviders(job.id, UserStore.getUserType(),
        success: (recommendedProviders) =>
          @_sortRecommendedProviders(recommendedProviders, sortKey, job)

          @_isLoaded = true
          @_loading = false

          @_triggerListListeners()
        error: (error) =>
          @_loading = false
          @_handleError(error)
      )

      @getRecommendedProviders()

  # if there's a chosen provider id not loaded in the collection:
  # go fetch it, add to the collection, sort again and trigger list listener
  loadChosenProviderIdIfNeeded: (job, chosenProviderId) =>
    if !@loadingIds
      @loadingIds = {}

    if chosenProviderId && !@_loading && !@loadingIds[job.id+"_"+chosenProviderId]? && !some(@getRecommendedProviders(), (provider) -> provider.id == chosenProviderId)

      @loadingIds[job.id+"_"+chosenProviderId] = true

      Api.getRecommendedProvider(UserStore.getEndpoint(), job.id, chosenProviderId, {
        success: (recommendedProvider) =>
          delete @loadingIds[job.id+"_"+chosenProviderId]
          @_recommendedProviders.push(recommendedProvider)

          @_sortRecommendedProviders(@getRecommendedProviders(), @_currentSortKey, job)

          @_triggerListListeners()
        error: (data) =>
          delete @loadingIds[job.id+"_"+chosenProviderId]
          console.log('error loading a unique provider as from recommended_provider endpoint')
      })

  getRecommendedProviders: ->
    @_recommendedProviders

  setRecommendedProviders: (recommendedProviders) ->
    @_recommendedProviders = recommendedProviders

  clearRecommendedProviders: () ->
    @_recommendedProviders = []
    @_providersForStore = null
    @_isLoaded = false
    @_bidsEnqueuedForSort

    if @_jobWithAuction
      for bid in @_jobWithAuction.auction?.bids
        #TODO: @Rodrigo unbinding needs the same params as the bind, the partial will return a new function
        AuctionStore.unbind(AuctionStore.BID_UPDATED_ON_CLIENT + '_' + bid.id, partial(@sortIfNeeded, @_jobWithAuction))

    @_jobWithAuction = null

  isLoaded: ->
    @_isLoaded

  addIfNotLoaded: (recommendedProvider, job) =>
    if(some(@getRecommendedProviders(), (_recommendedProvider) -> _recommendedProvider.id == recommendedProvider.id))
      return

    @_recommendedProviders.push(recommendedProvider)
    @_sortRecommendedProviders(@getRecommendedProviders(), @_currentSortKey, job)

    @_triggerListListeners()

  _sortRecommendedProviders: (recommendedProviders, sortKey, job) ->
    if sortKey == @SORT_BY_SUBMITTED_ETA
      @setRecommendedProviders(@_sortBySubmittedETA(recommendedProviders, job))
    else if sortKey == @SORT_BY_NETWORK_STATUS
      @setRecommendedProviders(@_sortByNetworkStatus(recommendedProviders))
    else if sortKey == @SORT_BY_CUSTOM_AMY
      @setRecommendedProviders(@_sortByCustomAmy(recommendedProviders, job))
    else if sortKey == @SORT_BY_JOB_DISTANCE || !sortKey
      @setRecommendedProviders(@_sortByJobDistance(recommendedProviders, job))

    @_currentSortKey = sortKey

  _sortByJobDistance: (recommendedProviders, job) =>
    sortBy(recommendedProviders, (provider) =>
      if @_shouldPushTeslaMobileTireServiceToTheTop(provider, job)
        return -1

      return provider.job_distance
      )

  _sortBySubmittedETA: (recommendedProviders, job) ->
    auction   = AuctionStore.getAuctionByJob(job)

    providersWithSubmittedETA = filter(recommendedProviders, (provider) =>
      rescueCompanyId = provider.provider_id

      return auction.bids.find((bid) -> bid.company_id == rescueCompanyId)?.submitted_eta
    )

    providersWithoutSubmittedETA = without(recommendedProviders, providersWithSubmittedETA)

    providersWithSubmittedETASorted = sortBy(providersWithSubmittedETA, (provider) ->
      rescueCompanyId = provider.provider_id

      return auction.bids.find((bid) -> bid.company_id == rescueCompanyId).submitted_eta
    )

    providersWithoutSubmittedETASorted = sortBy(providersWithoutSubmittedETA, (provider) =>
      truckAutoETA = AuctionStore.getTruckETA(provider, job)
      siteAutoETA  = AuctionStore.getSiteETA(provider, job)

      if !truckAutoETA && !siteAutoETA
        return undefined

      if !truckAutoETA
        return siteAutoETA
      else if !siteAutoETA
        return truckAutoETA

      return min([truckAutoETA, siteAutoETA])
    )

    union(providersWithSubmittedETASorted, providersWithoutSubmittedETASorted)

  _sortByNetworkStatus: (recommendedProviders) ->
    primaryProviders = filter(recommendedProviders, (provider) ->
      if provider.tags?.length > 0
        return TagStore.getById(provider.tags[0]).name.indexOf('Primary') >= 0
    )

    secondaryProviders = filter(recommendedProviders, (provider) ->
      if provider.tags?.length > 0
        return TagStore.getById(provider.tags[0]).name.indexOf('Secondary') >= 0
    )

    otherProviders = without(recommendedProviders, secondaryProviders..., primaryProviders...)

    union(primaryProviders, secondaryProviders, otherProviders)

  _sortByCustomAmy: (recommendedProviders, job) =>
    sortBy(recommendedProviders, (provider) =>
      if @_shouldPushTeslaMobileTireServiceToTheTop(provider, job)
        return -1

      auction   = AuctionStore.getAuctionByJob(job)
      #This is to ensure that bidded auction jobs appear at the top
      non_auction_penalty = 1000

      if auction?
        bid = auction.bids?.find((bid) -> bid.company_id == provider.provider_id)

        if bid?
          if bid.submitted_eta?
            return bid.submitted_eta
          else
            non_auction_penalty = 250

      if provider.job_distance <= 15
        tagName = TagStore.getById(provider.tags[0])?.name

        if tagName && (tagName.indexOf("Primary") >= 0 || tagName.indexOf("Secondary") >= 0)
          return non_auction_penalty+provider.job_distance
        else
          return non_auction_penalty+15+provider.job_distance

      return non_auction_penalty+30+provider.job_distance
    )

  _shouldPushTeslaMobileTireServiceToTheTop: (provider, job) ->
    if !job?.symptom_id
      return false

    UserStore.isTesla() and
    provider.name.substring(0, 25) == Consts.TESLA_MOBILE_TIRE_SERVICE and
    SymptomStore.isFlatTire(job?.symptom_id) and
    provider.job_distance <= 15

store = new RecommendedProvidersStore()

module.exports = store
