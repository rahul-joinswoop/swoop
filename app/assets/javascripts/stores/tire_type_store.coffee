BaseStore = require('stores/base_store').default
import { without } from 'lodash'

#TODO: SHOULD SET UP LAZY LOADING FOR PARTNERS
class TireTypeStore extends BaseStore
  constructor: ->
    super({
      modelName: 'TireType'
    })

  getUrl: -> 'tire_types'
  getEditUrl: ->
    'tire_types'

  _getJSONName: ->
    'tire_type'

  isLazyLoaded: -> true
  getTireTypeName: (typeId, watcherId = null) ->
    if !typeId
      return ''

    type = @getById(typeId, true, watcherId)
    if type?
      name = without([type.car, type.position, type.extras, type.size], null)
      name = name.join(" ")
    else
      name = "...loading"
    return name

  getBaseTireTypeName: (typeId, watcherId = null) ->
    if !typeId
      return null

    type = @getById(typeId, true, watcherId)
    if type?
      name = without([type.extras, type.size], null)
      name.join(" ")
    else
      "...loading"
store = new TireTypeStore()

module.exports = store
