
import Api from 'api'
import BaseStore from 'stores/base_store'
import PoiStore from 'stores/poi_store'
import { filter, isEmpty, map, some, sortBy, values, find } from 'lodash'

import Utils from 'utils'

const LIST_DISPATCHABLE = 'list_dispatchable'
const LIST_TIRE_PROGRAM = 'list_tire_program'

const DISPATCHABLE_IDS = 'dispatchable_ids'
const TIRE_PROGRAM_IDS = 'tire_program_ids'

const LOADING_DISPATCHABLE_IDS = '_loading_dispatchable_ids'
const LOADING_TIRE_PROGRAM_IDS = '_loading_tire_program_ids'

class SiteStore extends BaseStore {
  REMOTE_SITE_ADDED = 'remoteSiteAdded'

  constructor() {
    super({
      modelName: 'Site',
      stores: {
        PoiStore,
      },
    })

    this[DISPATCHABLE_IDS] = null
    this[TIRE_PROGRAM_IDS] = null

    this[LOADING_DISPATCHABLE_IDS] = false
    this[LOADING_TIRE_PROGRAM_IDS] = false
  }

  _getListsObject() {
    return [
      { list: this[DISPATCHABLE_IDS], validator: this.isDispatchable, triggerName: LIST_DISPATCHABLE },
      { list: this[TIRE_PROGRAM_IDS], validator: this.isTireProgram, triggerName: LIST_TIRE_PROGRAM },
    ]
  }

  isLazyLoaded() {
    return true
  }

  loadCollectionOnUserChanged() {
    return false
  }

  isEditable() {
    return true
  }

  hasPermissions() {
    return this.stores.UserStore.isPartner() || this.stores.UserStore.isFleet()
  }

  shouldAddUserTypeToApiUrl() {
    return true
  }

  getAddressWithSiteName(location, watcherId = null) {
    let address = ''
    const locationAddress = location.address
    let siteOrPoiName = null
    if (location.site_id) {
      siteOrPoiName = this.get(location.site_id, 'name', watcherId) || this.stores.PoiStore.get(location.site_id, 'name', watcherId)
      if (siteOrPoiName) {
        address = `${siteOrPoiName} `
      }
    }

    if (locationAddress) {
      address = Utils.renderAddress(locationAddress, siteOrPoiName)
    }

    return address
  }

  addRemoteItem(remoteData) {
    super.addRemoteItem(remoteData)

    const remoteObject = remoteData.target

    this.trigger(this.REMOTE_SITE_ADDED, remoteObject)
  }

  isDispatchable(site) {
    return site.dispatchable === true && !site.deleted_at
  }

  isTireProgram(site) {
    return site.tire_program === true && !site.deleted_at
  }

  inTireProgram() {
    return some(this.getAll(), site => site.tire_program)
  }

  hasTeslaAccount() {
    return some(values(this.getAll()), site => some(site.providerCompanies, providerCompany => providerCompany.name === 'Tesla'))
  }

  _loadDispatchableIds() {
    if (!this[LOADING_DISPATCHABLE_IDS] && this.hasPermissions()) {
      this[LOADING_DISPATCHABLE_IDS] = true

      Api.getDispatchableSites({
        success: (data) => {
          for (const index in data) {
            this._add(data[index])
          }

          this[DISPATCHABLE_IDS] = map(data, 'id')

          this._triggerListListeners(LIST_DISPATCHABLE)

          this[LOADING_DISPATCHABLE_IDS] = false
        },
        error: (data, textStatus, errorThrown) => {
          this[LOADING_DISPATCHABLE_IDS] = false
        },
      })
    }
  }

  _loadTireProgramSitesIds() {
    if (!this[LOADING_TIRE_PROGRAM_IDS] && this.hasPermissions()) {
      this[LOADING_TIRE_PROGRAM_IDS] = true

      Api.getTireProgramSites({
        success: (data) => {
          for (const index in data) {
            this._add(data[index])
          }

          this[TIRE_PROGRAM_IDS] = map(data, 'id')

          this._triggerListListeners(LIST_TIRE_PROGRAM)

          this[LOADING_TIRE_PROGRAM_IDS] = false
        },
        error: (data, textStatus, errorThrown) => {
          this[LOADING_TIRE_PROGRAM_IDS] = false
        },
      })
    }
  }

  getDispatchSites(watcherId = null, property = null) {
    if (!this.stores.UserStore.isPartner()) {
      return []
    }

    if (watcherId) {
      this._registerListListener(watcherId, LIST_DISPATCHABLE, property)
    }

    if (this[DISPATCHABLE_IDS] === null) {
      this._loadDispatchableIds()
    }

    if (this[DISPATCHABLE_IDS]) {
      return sortBy(map(this[DISPATCHABLE_IDS], siteId => this.getById(siteId)), 'name')
    }

    // GO THROUGH SITES AND FIND DISPATCHABLE ONES
    const siteValues = values(this.getAll())

    const dispatchableSites = filter(siteValues, site => site.dispatchable)

    return sortBy(dispatchableSites, 'name')
  }

  getClosedDispatchSites(watcherId = null, property = null) {
    return filter(this.getDispatchSites(watcherId, property), site => site && !site.open)
  }

  getTireProgramSites(watcherId = null, property = null) {
    if (!this.stores.UserStore.isPartner()) {
      return []
    }

    if (watcherId) {
      this._registerListListener(watcherId, LIST_TIRE_PROGRAM, property)
    }

    if (this[TIRE_PROGRAM_IDS] === null) {
      this._loadTireProgramSitesIds()
    }

    if (this[LIST_TIRE_PROGRAM]) {
      return map(this[TIRE_PROGRAM_IDS], siteId => this.getById(siteId))
    }

    // GO THROUGH SITES AND FIND TIRE PROGRAM ONES
    const siteValues = values(this.getAll())

    return filter(siteValues, site => site.tire_program)
  }

  getUserSitesOrAll(watcherId = null, properties = null) {
    const allSites = this.loadAndGetAll(watcherId, properties)
    const userSites = filter(allSites, 'user_site')
    return isEmpty(userSites) ? allSites : userSites
  }

  getFilteredSites(allowedSitesIds = [], watcherId = null, properties = null) {
    const allowedSitesIdsAsInt = map(allowedSitesIds, Number)
    const filteredSiteIds = filter(map(this.loadAndGetAll(watcherId, properties), 'id'), (siteId) => {
      const siteIdAsInt = parseInt(siteId)

      if (allowedSitesIdsAsInt.indexOf(siteIdAsInt) > -1) {
        return true
      }

      const mySitesIds = this.stores.UserStore.getMySiteIds()
      if (mySitesIds === null || mySitesIds === undefined || mySitesIds.length === 0) {
        return true
      }

      if (mySitesIds.indexOf(siteIdAsInt) < 0) {
        return false
      }

      return true
    })

    return map(filteredSiteIds, siteId => this.getById(siteId))
  }

  getBySiteCode(siteCode, watcherId = null, properties = null) {
    if (!siteCode) {
      return null
    }

    const sites = values(this.loadAndGetAll(watcherId, properties))
    return find(sites, site => site.site_code === siteCode)
  }
}

const store = new SiteStore()

export default store
