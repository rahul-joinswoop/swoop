import Api from 'api'
import Consts from 'consts'
import UserStore from 'stores/user_store'
MicroEvent = require('microevent-github')
import Dispatcher from 'lib/swoop/dispatcher'
NotificationStore = require('stores/notification_store')
ErrorStore = require('stores/error_store')
LocationTypeStore = require('stores/location_type_store')
ServiceStore = require('stores/service_store')
import Utils from 'utils'
import { clone, difference, filter, includes, intersection, keys, map, partial, sortBy, values } from 'lodash'
$ = require('jquery')
Storage = require('stores/storage')
import moment from 'moment-timezone'
import Logger from 'lib/swoop/logger'
AuctionStore = require('stores/auction_store').default
AccountStore = require('stores/account_store')
RejectReasonStore = require('stores/reject_reason_store')
CustomerTypeStore = require('stores/customer_type_store')
import React from 'react'

triggers =
    JOBS_LOADED : "jobs_loaded"
    UPDATE_JOBS : "updateJobs"
    JOB_ADDED : "jobAdded"
    KNOWN_STATE_CHANGE: "knownStateChange"
    JOB_REMOVED : "jobRemoved"
    JOB_CHANGED : "jobChanged"
    LOGOUT : "logout"
    CHANGE : "change"
    CONTEXT_CHANGED : "context_changed"
    REMOVED_AS_DRIVER: "removed_as_driver"
    ACTIVE_JOBS_LOADED: "active_jobs_loaded"

TransitionModal = React.createFactory(require 'components/modals')
#TODO: don't put these on the window

window[key] = val for key, val of triggers


SearchStore = require('stores/search_store')

LIST_ACTIVE = "list_active"
LIST_DONE = "list_done"
LIST_PENDING = "list_pending"
LIST_PENDING_NONSCHEDULED_AND_NONDRAFTS = "list_pending_nonscheduled_and_nondrafts"
LIST_PENDING_NONSCHEDULED = "list_pending_nonscheduled"
LIST_PENDING_NONDRAFTS = "list_pending_nondrafts"
LIST_DISPATCHED = "list_dispatched"
LIST_MONITORING = "list_monitoring"
LIST_PENDING_ON_TOWER = "list_pending_on_tower"
LIST_SCHEDULED = "list_scheduled"
LIST_DRAFT = 'list_draft'

LOADING_ACTIVE_JOBS = "loading_active_jobs"


PENDING_TYPE = "Pending"

class JobStore extends SearchStore
  constructor: ->
    super({
      modelName: 'Job'
    })

    @ACTIVE_JOB_IDS = []
    @PENDING_JOB_IDS = []
    @PENDING_NONSCHEDULED_JOB_IDS = []
    @PENDING_NONDRAFTS_JOB_IDS = []
    @PENDING_NONSCHEDULED_AND_NONDRAFTS_JOB_IDS = []
    @DISPATCHED_JOB_IDS = []
    @MONITORING_JOB_IDS = []
    @PENDING_ON_TOWER_JOB_IDS = []
    @SCHEDULED_JOB_IDS = []
    @DRAFT_JOB_IDS = []

    @loadingJobsCallbacks = {}


    @loadedActiveJobs = false
    #TODO: this list can be adjusted to reflect users settings / initialized when call is made
    @lists = [
      {list: @ACTIVE_JOB_IDS, validator: @isActive, triggerName: LIST_ACTIVE},
      {list: @PENDING_JOB_IDS, validator: @isPending, triggerName: LIST_PENDING, type: PENDING_TYPE},
      {list: @PENDING_NONSCHEDULED_AND_NONDRAFTS_JOB_IDS, validator: @isPendingNonscheduledAndNonDraft, triggerName: LIST_PENDING_NONSCHEDULED_AND_NONDRAFTS, type: PENDING_TYPE},
      {list: @PENDING_NONSCHEDULED_JOB_IDS, validator: @isPendingNonscheduled, triggerName: LIST_PENDING_NONSCHEDULED, type: PENDING_TYPE},
      {list: @PENDING_NONDRAFTS_JOB_IDS, validator: @isPendingNonDraft, triggerName: LIST_PENDING_NONDRAFTS, type: PENDING_TYPE},
      {list: @DISPATCHED_JOB_IDS, validator: @isDispatched, triggerName: LIST_DISPATCHED},
      {list: @MONITORING_JOB_IDS, validator: @isMonitoring, triggerName: LIST_MONITORING},
      {list: @PENDING_ON_TOWER_JOB_IDS, validator: @isPendingOnTower, triggerName: LIST_PENDING_ON_TOWER},
      {list: @SCHEDULED_JOB_IDS, validator: @isScheduled, triggerName: LIST_SCHEDULED},
      {list: @DRAFT_JOB_IDS, validator: @isDraft, triggerName: LIST_DRAFT},
    ]

    AuctionStore.bind(AuctionStore.PARTNER_BID_REJECTED, @removeRejectedBidFromLists)
    AuctionStore.bind(AuctionStore.PARTNER_LOST_AUCTION, @removeRejectedBidFromLists)

  isLazyLoaded: -> true
  getBatchUrl: ->
    return @getCustomizedEditUrl() + "/?" + @._getJSONName() + "_ids="

  processBatch: (batchResponse) ->
    if batchResponse
      return batchResponse.jobs


  getMaxBatchSize: -> return 25
  isEditable: -> true
  loadCollectionOnUserChanged: -> false
  shouldAddUserTypeToApiUrl: -> true
  addUserTypeToApiUrl:(url, method) ->
    #HACK TO MAKE SURE ROOT POSTS TO FLEET WHEN CREATING JOBS
    if method == "add" && UserStore.isSwoop()
      return "fleet/"+url
    return UserStore.getEndpoint() + '/' + url


  getSearchUrl: -> UserStore.getEndpoint()+"/partial_jobs/"
  _getListsObject: ->
    return @lists

  isPending: (job) =>
    @isVisible(job) && includes(Consts.PENDING_STATUSES, job.status)

  isPendingNonDraft: (job) =>
    @isPending(job) and (job.status != Consts.DRAFT)

  isPendingNonscheduled: (job) =>
    @isPending(job) && (!job.scheduled_for? || (job.status == Consts.ASSIGNED and Utils.isAssignedFromFleet(job)))

  isPendingNonscheduledAndNonDraft: (job) =>
    @isPendingNonscheduled(job) and @isPendingNonDraft(job)

  isVisible: (job) ->
    if job.deleted_at?
      return false
    if UserStore.isSwoop()
      if job.type != "FleetManagedJob"
        return false
    if UserStore.isPartner() && AuctionStore.jobShouldBeHiddenFromPartner(job, UserStore.getCompany().id)
      return false

    return true

  isActive: (job) =>
    return @isVisible(job) && includes(Consts.ACTIVE_STATUSES, job.status)

  isDispatched: (job) =>
    return @isVisible(job) && includes(Consts.DISPATCHED_STATUSES, job.status)

  isMonitoring: (job) =>
    return @isVisible(job) && includes(Consts.MONITORING_STATUSES, job.status)

  isPendingOnTower: (job) =>
    return @isVisible(job) && includes([Consts.DISPATCHED, Consts.ENROUTE], job.status)

  isScheduled: (job) =>
    return job.scheduled_for && @isVisible(job) && includes(Consts.SCHEDULED_STATUSES, job.status) && !(job.status == Consts.ASSIGNED and Utils.isAssignedFromFleet(job))

  isDraft: (job) =>
    return job.status == Consts.DRAFT

  isPredraft: (job) =>
    job.status == Consts.PREDRAFT

  jobsLoaded: (watcher) ->
    @_registerListListener(watcher, triggers.ACTIVE_JOBS_LOADED)
    @loadedActiveJobs

  removeRejectedBidFromPendingLists: (auction) =>
    for cacheList in @lists
      if cacheList.type == PENDING_TYPE
        @removeRejectedBidFromList(auction, cacheList.list, cacheList.triggerName)

  removeRejectedBidFromList: (auction, list, trigger) =>
    jobIdIndex = list.indexOf(auction.job_id)
    if jobIdIndex > -1
      list.splice(jobIdIndex, 1)
      @_triggerListListeners(trigger)

  removeRejectedBidFromLists: (auction) =>
    @removeRejectedBidFromPendingLists(auction)

  getJobs: ->
    #Load pending jobs here if needed
    @loadActiveJobsIfNeeded()

    #TODO: convert callers to just use getAll or a more specific function
    values(@getAll())

  _setEventId: (event_id) ->
    @eventId = event_id



  _loadWithPagination: (page=1, per_page=@getLoadingPaginationSize(), callbacks={}, request=@paginationEndpoint(), maxPage=1000) ->
    request(Api.GET, UserStore.getEndpoint(), {
      success: (remoteData) =>
        callbacks?.preAddSuccess?(remoteData, page)

        for row in remoteData.jobs
          @_add(row, false)


        @trigger(@CHANGE)


        if remoteData.jobs.length == per_page && page < maxPage
          callbacks?.success(remoteData, false)
          @_loadWithPagination(page+1, per_page, callbacks, request, maxPage)

        else
          if page == maxPage && remoteData.jobs.length == per_page
            Utils.sendNotification("Too many jobs to load on dashboard", Consts.ERROR)

          callbacks?.success(remoteData, true)

          @_loaded = true

          @trigger(@LOADED)

      error: (data, textStatus, errorThrown) =>
        @_handleError(data, textStatus, errorThrown)

    }, page, per_page)


  loadActiveJobsIfNeeded: =>
    user = UserStore.getUser()
    #TODO: figure out why this is being called before the user is loaded and remove this logic
    if !user?
      setTimeout(@loadActiveJobsIfNeeded, 1000)
      return

    if !this[LOADING_ACTIVE_JOBS] and !@loadedActiveJobs
      this[LOADING_ACTIVE_JOBS] = true

      request = Api.activeJobsRequest
      if UserStore.isOnlyDriver()
        request = Api.activeDriverJobsRequest

      callbacks = {
        success: (data, finished) =>
          @loadedActiveJobs = true
          @[LOADING_ACTIVE_JOBS] = false
          @_triggerListListeners(triggers.ACTIVE_JOBS_LOADED)
          @_triggerListListeners(LIST_ACTIVE)
          @setEventId(data.last_event_id)
      }

      @_loadWithPagination(1, Consts.JOBS_PER_PAGE_COUNT, callbacks, request, Math.ceil(Consts.MAX_ACTIVE_JOB_LOAD_COUNT/Consts.JOBS_PER_PAGE_COUNT))

  isLoadingActiveJobs: (watcherId=null) =>
    if watcherId
      @_registerListListener(watcherId, LIST_ACTIVE)

    @[LOADING_ACTIVE_JOBS]

  updateJobs: ->
    @[LOADING_ACTIVE_JOBS] = false
    @loadedActiveJobs = false
    @loadActiveJobsIfNeeded()

  jobChangeSubmit: (job, saveAsDraft = false) ->
    if !job.id?
      @jobAdded(job, {save_as_draft: saveAsDraft})
    else
      @jobChanged(job, false, saveAsDraft)

  jobAdded: (data, options={}) =>
    delete data.id

    success_callback = data.callbacks?.success

    if !data.callbacks?
      data.callbacks = {}

    data.callbacks.success = (data) =>
      success_callback?(data)
      id = @getId(data)
      strId = ""
      if id? and (""+id).length > 0
        strId = " #"+id

      if data.status != Consts.PREDRAFT
        Utils.sendNotification("Job"+strId+" has been created.", Consts.SUCCESS)

    data.silent = true

    @addItem(data, options)

    #TODO: maybe trigger JOB_ADDED

  addItem: (localData, options={save_as_draft: false, save_as_predraft: false}) =>
    delete localData.id
    @_sendItem(localData, "add", '', Api.objectsRequest, options)

  sendJobDetailsEmail: (id, email_to, email_cc, email_bcc) ->

    Api.sendJobDetailsEmail(
      UserStore.getEndpoint(),
      {
        id: id,
        email_recipients: {
          to: email_to,
          cc: email_cc,
          bcc: email_bcc
        }
      }
    )

    Utils.sendNotification("Job is sending", Consts.SUCCESS)

  createDemoJob: ->
    Api.createDemoJob()

  jobRemoved: (data) =>
    @deleteItem(data)

  jobChanged: (data, remove_status = false, saveAsDraft = false) =>
    orig_data = data

        #Hack so front end sees change immediately
    if remove_status and data.status
      delete data.status

    prev_job = @getJobById(data.id)

    request = Api.jobsRequest
    if data.status?
      if data.status == Consts.REJECTED
        request = Api.rejectJob
      else if data.status == Consts.RELEASED
        request = Api.releaseJob
      else
        if keys(data).length > 2
          # these are values allowed for job status change: ["id", "status", "job_status_reason_types", "goa_amount"]
          if difference(keys(data), ["id", "status", "job_status_reason_types", "goa_amount"]).length > 0
            throw new Error('Updating status with other props for job: ' + JSON.stringify(data))
        request = Api.jobsStatusRequest

    success_callback = data.callbacks?.success

    if !data.callbacks?
      data.callbacks = {}

    data.callbacks.success = (data) =>
      success_callback?(data)
      if orig_data?.status == "GOA"
        Utils.sendNotification("Job #{@getId(data)} has been marked as GOA",Consts.SUCCESS)
      else if orig_data?.status == "Canceled"
        Utils.sendNotification("Job #"+@getId(data)+" has been canceled.", Consts.SUCCESS)
      else if orig_data?.status == Consts.REJECTED
        Utils.sendNotification("Job #"+@getId(data)+" has been rejected.", Consts.SUCCESS)
      else if orig_data?.status == Consts.COMPLETED
        Utils.sendNotification("Job #"+@getId(data)+" has been completed.", Consts.SUCCESS)
      else if orig_data?.status == Consts.REASSIGNED
        Logger.debug("Move to reassigned")
        Utils.sendNotification("Job #"+@getId(data)+" has been reassigned.", Consts.SUCCESS)
      else if orig_data?.status == Consts.ACCEPTED || (data?.status == Consts.ACCEPTED && orig_data?.bta? and keys(orig_data).length == 2)
        Logger.debug("Move to accepted")
        Utils.sendNotification("Job #"+@getId(data)+" has been edited.", Consts.SUCCESS)
      else if orig_data?.status == Consts.DELETED
        Utils.sendNotification("Job #{@getId(data)} has been deleted",Consts.SUCCESS)
      else if prev_job.status == Consts.PREDRAFT
        Tracking.trackJobEvent("Create Job", data.id, {
          category: 'Dispatch'
        })
        if data?.status == Consts.DRAFT
          Utils.sendNotification("Job #"+@getId(data)+" draft has been created.", Consts.SUCCESS)
        else
          Utils.sendNotification("Job #"+@getId(data)+" has been created.", Consts.SUCCESS)
      else
        # ...
        if data?.status == Consts.DRAFT
          Utils.sendNotification("Job #"+@getId(data)+" draft has been edited.", Consts.SUCCESS)
        else
          Utils.sendNotification("Job #"+@getId(data)+" has been edited.", Consts.SUCCESS)

    data.silent = true

    @updateItem(data, ((url, method, id, response, obj) -> request(method, UserStore.getEndpoint(), id, response, obj)), saveAsDraft)

  updateItem: (data, request, saveAsDraft = false) =>
    extraObjectsToRequest = null

    if saveAsDraft
      extraObjectsToRequest = { save_as_draft: saveAsDraft }

    @_sendItem(data, "update", data.id, request, extraObjectsToRequest)

  getJobsWhere: (condition) =>
    filter(@getAll(), condition)

  # returns true if job is a Partner job (aka RescueCompany job)
  isPartner: (job) ->
    @isJobOfType(job, Consts.JOB.TYPE.RESCUE)

  # returns true if job is a Motorclub job
  isMotorClub: (job) ->
    @isJobOfType(job, Consts.JOB.TYPE.MOTORCLUB)

  # check job.type attribute and returns true if it matches job_type
  isJobOfType: (job, job_type) ->
    return false if not job?
    return false if not job_type?
    job.type == job_type

  getFullJob: (job, callback) =>
    if job.partial
      @forceReload(job.id, callback)
    else
      callback(job)

  forceReload: (id, callback) =>
    @_lazyLoad(id, callback)

  # return false if user is not from Tesla
  # return false if no job is given
  # return false if job status is not CANCELED, GOA or COMPLETED
  # return true if payment type (aka customer_type_name) if allowed to be changed; false otherwise
  allowChangePaymentMethodFor: (job) ->
    return false if not UserStore.isTesla()
    return false if not job?
    #TODO: get object property here
    return false if job.status not in [Consts.CANCELED, Consts.GOA, Consts.COMPLETED]

    #TODO: get object property here
    job.customer_type_name in [
      Consts.CUSTOMER_TYPE_WARRANTY,
      Consts.CUSTOMER_TYPE_GOODWILL,
      Consts.CUSTOMER_TYPE_PCARD,
      Consts.CUSTOMER_TYPE_CUSTOMER_PAY,
      Consts.CUSTOMER_TYPE_BODY_SHOP
    ]

  partnerNameFor: (job) ->
    if UserStore.isSwoop() || UserStore.isFleetManaged()
      #TODO: get object property here
      job.rescue_company.name
    else
      #TODO: get object property here
      job.customized_rescue_company_name


  partnerNameKey: ->
    if UserStore.isSwoop()
      'job.rescue_company.name'
    else
      'job.customized_rescue_company_name'


  fetchJobByVin: (vin, callback) =>
    if not @loadingJobsCallbacks[vin]?
      @loadingJobsCallbacks[vin] = []
      type = UserStore.getEndpoint()

      Api.jobsVinRequest(Api.Get, type, vin, {

        success: (data) =>
          @addJob(data)
          for callback in @loadingJobsCallbacks[vin]
            if typeof callback == "function"
              callback(data)
          delete @loadingJobsCallbacks[vin]
        error: @_handleError #TODO: remove the job from loading
      })
    if callback?
      @loadingJobsCallbacks[vin].push(callback)

  getJobById: (id, force, callback)  =>
    #TODO: Map to get with load and watcher
    all = @getAll(true)
    if all[id]
      return all[id]
    else if force
      @forceReload(id, callback)

  getFullJobById: (id, force, callback) =>
    all = @getAll(true)

    if all[id]
      if all[id].partial && force
        @forceReload(id, callback)
      return all[id]
    else if force
      @forceReload(id, callback)

  getJobsById: (ids, callback)  =>
    #TODO: convert this to lazy load id
    toLoad = []
    for id in ids
      if not @loadingJobsCallbacks[id]?
        toLoad.push(id)
        @loadingJobsCallbacks[id] = true

    type = "partner"
    if UserStore.isFleet()
        type = "fleet"
    if UserStore.isSwoop()
      type = "root"


    if toLoad.length > 0
        Api.jobsListRequest(Api.Get, type, toLoad, {
          success: (data) =>
            for job in data.jobs
              @addJob(job)
              delete @loadingJobsCallbacks[job.id]
            callback(data)
          error: @_handleError
        })

  getJobsByStatus: (statuses) =>
    #TODO: set up lists instead of doing this live

    return (store.getJobs().filter (data) ->
        if data.deleted_at?
          return false
        if UserStore.isSwoop()
            if data.type != "FleetManagedJob"
                return false
        if UserStore.isPartner() && AuctionStore.jobShouldBeHiddenFromPartner(data, UserStore.getCompany().id)
          return false
        return data.status in statuses
    )

  sortJobs: (jobs) =>
    return jobs.sort((a, b) ->
      a_date = Utils.getJobCreatedAt(a)
      b_date = Utils.getJobCreatedAt(b)
      if a.scheduled_for?
          a_date = a.scheduled_for
      if b.scheduled_for?
          b_date = b.scheduled_for
      return (new Date(a_date)).getTime() - (new Date(b_date)).getTime()
    )


  getActiveJobIds: (watcher=null) =>
    @loadActiveJobsIfNeeded()
    if watcher
      @_registerListListener(watcher, LIST_ACTIVE)

    return @ACTIVE_JOB_IDS

  removeActiveJobs: =>
    jobs = @getJobsByStatus [Consts.PENDING, Consts.UNASSIGNED, Consts.ASSIGNED, Consts.AUTO_ASSIGNING, Consts.ACCEPTED, Consts.REASSIGN, Consts.DISPATCHED,Consts.ENROUTE,Consts.ONSITE,Consts.TOWING,Consts.TOWDESTINATION]
    for job in jobs
      store.removeJob(job)

  getScheduledJobIds: (watcher=null) =>
    if watcher
      @_registerListListener(watcher, LIST_SCHEDULED)
    return @SCHEDULED_JOB_IDS

  getDraftJobIds: (watcher=null) =>
    if watcher
      @_registerListListener(watcher, LIST_DRAFT)
    return @DRAFT_JOB_IDS

  getDispatchedJobIds: (watcher=null, onlyPendingOnTower = false) =>
    if onlyPendingOnTower
      if watcher
        @_registerListListener(watcher, LIST_PENDING_ON_TOWER)
      return @PENDING_ON_TOWER_JOB_IDS

    if watcher
      @_registerListListener(watcher, LIST_DISPATCHED)
    return @DISPATCHED_JOB_IDS

  getMonitoringJobIds: (watcher = null) =>
    if watcher
      @_registerListListener(watcher, LIST_MONITORING)
    return @MONITORING_JOB_IDS

  getPendingJobIds: (excludeScheduled, excludeDrafts, watcher=null) =>
    if excludeScheduled and excludeDrafts
      if watcher
        @_registerListListener(watcher, LIST_PENDING_NONSCHEDULED_AND_NONDRAFTS)

      return @PENDING_NONSCHEDULED_AND_NONDRAFTS_JOB_IDS

    if excludeScheduled
      if watcher
        @_registerListListener(watcher, LIST_PENDING_NONSCHEDULED)

      return @PENDING_NONSCHEDULED_JOB_IDS

    if excludeDrafts
      if watcher
        @_registerListListener(watcher, LIST_PENDING_NONDRAFTS)

      return @PENDING_NONDRAFTS_JOB_IDS

    if watcher
      @_registerListListener(watcher, LIST_PENDING)

    return @PENDING_JOB_IDS


  #SHOULD PROBABLY MOVE AWAY FROM THESE$
  getDraftsJobs: (watcher=null) =>
    return @_mapIds(@getDraftJobIds(watcher))

  getScheduledJobs: (watcher=null) =>
    return @_mapIds(@getScheduledJobIds(watcher))

  getPendingJobs: (excludeScheduled, excludeDrafts, watcher=null) =>
    return @_mapIds(@getPendingJobIds(excludeScheduled, excludeDrafts, watcher))

  getDispatchedJobs:(watcher=null, onlyPendingOnTower = false) =>
    return @_mapIds(@getDispatchedJobIds(watcher, onlyPendingOnTower))

  getMonitoringJobs:(watcher=null) =>
    return @_mapIds(@getMonitoringJobIds(watcher))

  getDispatchableJobs: =>
    return @getJobsByStatus [Consts.PENDING, Consts.ASSIGNED,Consts.AUTO_ASSIGNING,Consts.ACCEPTED,Consts.REASSIGN, Consts.SUBMITTED]

  getActiveJobs: (watcher) =>
    return @_mapIds(@getActiveJobIds(watcher))
  #########################################

  getActiveDriverJobs: (watcher) =>
    if UserStore.getUser()?
      filter(store.getActiveJobs(watcher), (job) =>
        if job and job.rescue_driver_id
          job.rescue_driver_id == UserStore.getUser()?.id
      )
    else
      []


  getDoneJobs: =>
    if UserStore.isFleet()
      return @getJobsByStatus [Consts.CANCELED, Consts.GOA, Consts.COMPLETED, Consts.STORED, Consts.RELEASED, Consts.REASSIGNED]
    else if UserStore.isPartner()
      return @getJobsByStatus [Consts.REJECTED, Consts.CANCELED, Consts.GOA, Consts.COMPLETED, Consts.STORED, Consts.RELEASED]
    else
      return @getJobsByStatus [Consts.REJECTED, Consts.ETAREJECTED, Consts.CANCELED, Consts.GOA, Consts.COMPLETED, Consts.REASSIGNED, Consts.STORED, Consts.RELEASED]

  getDoneDriverJobs: =>
    if UserStore.getUser()?
      filter(@getDoneJobs(), (job) =>
        job.rescue_driver_id == UserStore.getUser()?.id
      )
    else
      []

  finishedLoadingDoneJobs: =>
    allDoneJobs

  totalTime: (jobId, watcherId) =>
    job = @getById(jobId, true, watcherId)

    if job
      totalTime = 0

      if job.mins_ab? then totalTime += job.mins_ab # HQ to Pick Up
      if job.mins_on_site? then totalTime += job.mins_on_site # Time On Site
      if job.mins_bc? then totalTime += job.mins_bc # Pick Up to Drop Off
      if job.mins_at_dropoff? then totalTime += job.mins_at_dropoff # Time at Drop Off
      if job.mins_ca? then totalTime += job.mins_ca # Drop Off to HQ
      if job.mins_ba? then totalTime += job.mins_ba # Pick Up to HQ

      totalTime

  totalTimeFormatted: (jobId, watcherId) =>
    totalTime = @totalTime(jobId, watcherId)

    Utils.formatMinsInTotalHours(totalTime)

  addRemoteItem: (remoteData) =>
    data = remoteData.target
    changes = remoteData.changes || {}
    if data.deleted_at?
      if data.id and @getJobById(data.id)
        @trigger(CHANGE)
        @trigger(CHANGE+"_"+data.id)
        @_remove(data)
        @trigger(triggers.KNOWN_STATE_CHANGE)
      return
    if UserStore.isOnlyDriver()
      myId = UserStore.getUser()?.id
      if myId != data.rescue_driver_id and @getJobById(data.id)?.rescue_driver_id != myId
          return

    showNotification = =>
      if UserStore.isSwoop() && changes?.status?[0] == 'pending' && changes?.status?[1] == 'assigned'
        Utils.sendNotification("Job #"+@getId(data)+" has been edited.", Consts.SUCCESS)

      if UserStore.isSwoop() and data.type in ["FleetMotorClubJob", "FleetInHouseJob", "RescueJob"]
        return

      if UserStore.isFleetManaged() and changes.status? and data.status in [Consts.REJECTED, Consts.UNASSIGNED]
        return

      if UserStore.isPartner() and !@getJobById(data.id) and changes?.status?[0] == "auto_assigning" and  changes?.status?[1] == "dispatched"
        Utils.playDing()
        Utils.sendNotification("Job #"+@getId(data)+" has been auto dispatched.", null, {perpetual: true})
        return

      #SUPER HACK!  Waits for parent job so we have the reject info before showing notification
      if data.status == Consts.CREATED and data.parent_id? and not UserStore.isPartner() and not UserStore.isFleetManaged() and changes.status?
        setTimeout(partial(((data, changes) =>
          parent_job = @getJobById(data.parent_id)
          if parent_job? and parent_job.status == Consts.REJECTED
            Utils.playDing()
            name = ""
            if !UserStore.isFleetManaged() and parent_job.rescue_company?.name?
              name = " by " + parent_job.rescue_company.name
            str = "Job #"+@getId(parent_job)+" has been rejected#{name} and updated to #"+@getId(data)+"."
            if parent_job.reject_reason_id?
              reasonText = RejectReasonStore.get(parent_job.reject_reason_id, 'text')
              if reasonText
                str = "Job #"+@getId(parent_job)+" has been rejected#{name} ("+reasonText+") and updated to #"+@getId(data)+"."
                if parent_job.reject_reason_info?
                    if parent_job.reject_reason_id == 3
                      str = "Job #"+@getId(parent_job)+" has been rejected#{name} (need "+parent_job.reject_reason_info+" min to service) and updated to #"+@getId(data)+"."
                    else if parent_job.reject_reason_id == 5
                      str = "Job #"+@getId(parent_job)+" has been rejected#{name} ("+parent_job.reject_reason_info+") and updated to #"+@getId(data)+"."

           Utils.sendNotification(str, null, {perpetual: true})
        ), data, changes), 1000)


      if changes.status and data.status == Consts.UNASSIGNED and UserStore.isFleet() and parseInt(data.last_touched_by?.company_id) == parseInt(UserStore.getUser()?.company_id)
         Utils.sendNotification("Job #"+data.parent_id+" was unassigned and updated to #"+@getId(data)+".",Consts.INFO, {perpetual: true})

      if changes.status? and data.status is Consts.GOA and (not data.last_touched_by? || parseInt(data.last_touched_by?.id) != parseInt(UserStore.getUser()?.id))
       Utils.sendNotification("Job ##{@getId(data)} has been marked as GOA",Consts.INFO, {perpetual: true})
      else if changes.status? and data.status is Consts.ACCEPTED and data.type == "FleetMotorClubJob" and (not data.last_touched_by? || parseInt(data.last_touched_by?.id) != parseInt(UserStore.getUser()?.id))
        accountName = @getAccountNameFromJob(data)
        Utils.sendNotification("Job ##{@getId(data)} has been accepted" + (if accountName? then " by " + accountName else ""),Consts.INFO, {perpetual: true})
      else if changes["service_location_id"] and data.last_touched_by? and not data.last_touched_by?.company_id?
        person = "Customer"
        if data.send_sms_to_pickup
          person = "Pickup Contact"

        #if timeouts["service_"+data.id]?
        #  clearTimeout(timeouts["service_"+data.id])


        #Logger.debug("setting timeout", data)
        #timeouts["service_"+data.id] = setTimeout( ->
        #  Logger.debug("in timeout with person: ", person)
        Utils.sendNotification(person + " updated their pickup location for Job #"+@getId(data), null, {perpetual: true})
        #, 90*1000)
      else if UserStore.isPartner() && !@getJobById(data.id) && data.status == Consts.AUTO_ASSIGNING && data.auction? && !AuctionStore.isAutoAssignToTruckEnabled(data.auction)
        Utils.sendNotification("##{@getId(data)} is available for ETA submissions", null, {perpetual: true})
        Utils.playDing()
      else
        if not data.last_touched_by? || parseInt(data.last_touched_by?.id) != parseInt(UserStore.getUser()?.id)

            if UserStore.isDriver() and data.rescue_driver_id == UserStore.getUser()?.id and "rescue_driver_id" in keys(changes)
              Utils.playDing()


            if data.status is Consts.ASSIGNED
                if UserStore.isPartner() and not @getJobById(data.id)
                  Utils.playDing()
                  data.shouldFlash = moment()
            if not (UserStore.isRoot() and (data.type == "FleetInHouseJob" or data.type == "FleetMotorClubJob")) and not (UserStore.isFleet() and data.type == "FleetInHouseJob")
                if changes.status?
                    if data.status in [Consts.COMPLETED]
                        Utils.sendNotification("Job #"+@getId(data)+" has been "+data.status+".", Consts.SUCCESS)
                        return
        if not data.last_touched_by? or parseInt(data.last_touched_by?.company_id) != parseInt(UserStore.getUser()?.company.id)
            #TODO check if the status has changed
            if changes.status?
                if data.type in ["FleetInHouseJob", "FleetMotorClubJob", "FleetManagedJob" ]
                    if UserStore.isPartner()
                        if data.status in [Consts.EXPIRED]
                         Utils.sendNotification(" Job #"+@getId(data)+ " has " + data.status, null, {perpetual: true})
                        else if data.status == Consts.REASSIGNED && data.type == "FleetManagedJob"
                           Utils.sendNotification("Job #"+@getId(data)+" has been canceled.", null, {perpetual: true})
                        else if data.status in [Consts.REASSIGNED, Consts.CANCELED, Consts.ETAREJECTED]
                          companyName = if data.auction then companyName = 'Swoop' else companyName = @getAccountNameFromJob(data)

                          Utils.sendNotification(companyName + " has " + data.status + " Job #" + @getId(data), null, {perpetual: true})
                        else if data.status == Consts.ASSIGNED && store.getJobById(data.id)?.status != Consts.ASSIGNED
                          Utils.sendNotification(@getAccountNameFromJob(data) + " has " + data.status + " Job #" + @getId(data), null, {perpetual: true})
                    if (UserStore.isFleet() and (data.type == "FleetInHouseJob" or data.type == "FleetMotorClubJob")) or (UserStore.isRoot() and data.type == "FleetManagedJob")
                        if data.status in [Consts.COMPLETED]
                          Utils.sendNotification("Job #"+@getId(data)+" has been "+data.status+".", Consts.SUCCESS)
            else
                if (UserStore.isFleet() and data.type == "FleetManagedJob")
                    #theMap = {
                    #    "eta" : "Eta"
                    #}
                else if not (UserStore.isRoot() and (data.type == "FleetInHouseJob" or data.type == "FleetMotorClubJob"))
                    theMap = {
                        "service_location_id" : "Service Location",
                        "drop_location_id" : "Dropoff Location",
                        "vip": "VIP",
                        "uber": "Uber",
                        "alternate_transportation": "Alternative Transportation",
                        "underground": "Underground",
                        "no_keys": "No Keys",
                        "keys_in_trunk": "Keys in Trunk",
                        "four_wheel_drive": "4WD",
                        "roadside": "Roadside",
                        "residence": "Residence",
                        "accident": "Accident",
                        "scheduled_for": "Scheduled Time"
                    }
                    alert_changes = keys(theMap)
                    changedKeys = intersection(alert_changes, keys(changes))
                    if changedKeys.length > 0
                      Utils.sendNotification("Job #"+@getId(data)+" has had edits to: "+map(changedKeys, (key) => theMap[key]).join(", ")+".", null, {perpetual: true})

    showNotification()
    if data.status == Consts.CREATED
      return

    super(remoteData)

  replaceOrInsertJobs: (jobs) =>
    for job in jobs
      @addJob(job)

  # Should be used only internally by JobStore, as it receives the job object and no watcherId.
  getAccountNameFromJob: (job) ->
    if !job
      return ''

    if job.account?.name
      return job.account.name

    return AccountStore.getAccountNameByAccountId(job.id)

  getAccountNameByJobId: (jobId, watcherId = null) ->
    if jobId
      accountIdFromJob = @get(jobId, 'account_id', watcherId)
      accountFromJob = @get(jobId, 'account', watcherId)

      if accountIdFromJob
        AccountStore.getAccountNameByAccountId(accountIdFromJob, watcherId)
      else if accountFromJob
        AccountStore.getAccountNameByAccountId(accountFromJob.id, watcherId)
      else
        ""

  canAutoSetDropLocation: (service) ->
    false

  storageReleasedToUserName: (job_id) =>
    job = @getJobById(job_id)

    if job?.storage?.released_to_user?
      released_to_user = job.storage.released_to_user

      if released_to_user.full_name
        return released_to_user.full_name

      if released_to_user.first_name?
        full_name = released_to_user.first_name

        if released_to_user.last_name?
          full_name += ' ' + released_to_user.last_name

        return full_name

    return null

  #MOVE DOCUMENTS AND ISSUES TO THEIR OWN STORE
  removeDocumentUploadRequest: (job_id, fileName) =>
    job = @getJobById(job_id)
    if job?.attached_documents?
      for i in [0..job.attached_documents.length-1]
        upload = job.attached_documents[i]
        if upload? && !upload.url? and upload.original_filename == fileName
          job.attached_documents?.splice?(i, 1)
          i=i-1

  documentUploadRequest: (job_id, file, callback, failedCallback) =>
    job = @getJobById(job_id)
    if !job?
      failedCallback?()
      return
    if !job.attached_documents
      job.attached_documents = []
    job.attached_documents.push({
      url: null,
      original_filename: file.name
    })

    Api.documentUploadRequest(UserStore.getEndpoint(), job_id, null, {
      success: (data) =>
        callback({
          signedUrl: data.url
          document: {
            id: data.id
            storage: data.storage
            metadata: {
              size: file.size
              filename: file.name
              mime_type: file.type
            }
          }
        })
      ,
      error: (data) ->
        failedCallback?(data)
        Utils.sendNotification("Document failed to be removed from Job.", Consts.ERROR)
    })
    @trigger(CHANGE+"_"+job_id)

  createDocument: (job_id, doc_meta, obj={}, callbacks) =>
    obj.document = JSON.stringify(doc_meta)

    Api.documentCreate(UserStore.getEndpoint(), job_id, attached_document: obj, {
      success: (data) =>
        #HANDLED ADDING TO THE JOB FROM WEBSOCKETS
        callbacks?.success?(data)
        Utils.sendNotification("Document has been added to Job.", Consts.SUCCESS)
      ,
      error: (data) ->
        callbacks?.error?(data)
        Utils.sendNotification("Document failed to add to Job.", Consts.ERROR)
    })

  updateDocument: (job_id, obj) =>

    Api.documentUpdate(UserStore.getEndpoint(), job_id, obj, {
      success: (data) =>
        #HANDLED ADDING TO THE JOB FROM WEBSOCKETS
        callbacks?.success?(data)
        Utils.sendNotification("Document has been updated.", Consts.SUCCESS)
      ,
      error: (data) ->
        callbacks?.error?(data)
        Utils.sendNotification("Document failed to update.", Consts.ERROR)
    })

  removeDocument: (job_id, document_id) =>
    job = @getJobById(job_id)
    if job?.attached_documents?
      #TODO use a key instead of the name
      job.attached_documents = filter(job.attached_documents, (doc) ->
        doc.id != document_id
      )

      @trigger(CHANGE+"_"+job_id)

    Api.updateJobDocument(store.getEndpoint(), job_id, {id: document_id, deleted_at: moment().toISOString()}, {
      success: (data) =>
        Utils.sendNotification("Document has been removed from Job.", Consts.SUCCESS)
      ,
      error: () ->
        Utils.sendNotification("Document failed to be removed from Job.", Consts.ERROR)
    })

  addIssue: (job_id, changes) =>
    if !UserStore.isSwoop() && !UserStore.isFleetInHouse()
      return

    Api.addJobIssue(UserStore.getEndpoint(), job_id, changes, {
      success: (data) =>
        @addSubObject("explanations", data)
        if changes.email
          Utils.sendNotification("Claim form has been sent to " + changes.email + " for Job ID " + job_id, Consts.SUCCESS)
        else
          Utils.sendNotification("Issue has been added to Job.", Consts.SUCCESS)
      ,
      error: () ->
        Utils.sendNotification("Issue failed to be added to Job.", Consts.ERROR)
     })
    #TODO: may want to stick it on the job here

  addHistoryEvent: (job_id, changes) =>

    Api.addJobHistoryEvent(job_id, changes, {
      success: (data) =>
        @addSubObject("explanations", data)
        Utils.sendNotification("Issue has been added to Job.", Consts.SUCCESS)
      ,
      error: () ->
        Utils.sendNotification("Issue failed to be added to Job.", Consts.ERROR)
     })

  removeIssue: (job_id, issue_id) =>
     job = @getJobById(job_id)
     if job?.explanations?
       #TODO use a key instead of the name
       job.explanations = filter(job.explanations, (issue) ->
         issue.id != issue_id
       )
       @trigger(CHANGE+"_"+job_id)

     Api.updateJobIssue(store.getEndpoint(), job_id, {id: issue_id, deleted_at: moment().toISOString()}, {
        success: (data) =>
          Utils.sendNotification("Issue has been removed from Job.", Consts.SUCCESS)
        ,
        error: () ->
          Utils.sendNotification("Issue failed to be removed from Job.", Consts.ERROR)
     })
  #######################################


  replaceJob: (job) =>
    @addJob(job)

  addJob: (job) =>
    @_add(job, true)

  _addRowToInternalCollection: (job) =>

    if job?.id?
      triggerAdd = false
      #TODO: check if job exists
      storeJob = @getAll(true)[job.id]
      if !@exists(job.id) || storeJob.deleted_at != job.deleted_at || storeJob.status != job.status
        triggerAdd = true

      #Track revision of job
      job.rev = storeJob?.rev || 1

      myId = UserStore.getUser()?.id

      if storeJob?.rescue_driver_id == myId and job.rescue_driver_id != myId
        @trigger(REMOVED_AS_DRIVER, job.id)

      if job?.owner_company?.customer_types?
        for ct in job.owner_company.customer_types
          CustomerTypeStore.addRemoteItem?(target: ct, true)


      if job.service_location?.location_type_id || job.location_type?
        LocationTypeStore.forceLoadLocationType(job.service_location?.location_type_id, job.location_type)
      if job.drop_location?.location_type_id || job.dropoff_location_type?
        LocationTypeStore.forceLoadLocationType(job.drop_location?.location_type_id, job.dropoff_location_type)

      super(job)
      #TODO: perhaps make add a deep merge if it already exists
      #@jobs[job.id] = Utils.deepMerge(@jobs[job.id], clone)

      #HACK: THIS LOADS THE SERVICE FOR THE SERVICE DROP DOWN FILTER FOR ACTIVE JOBS
      if job.status in Consts.ACTIVE_STATUSES
        ServiceStore.getById(job.service_code_id, true)

      if triggerAdd
        @trigger(triggers.KNOWN_STATE_CHANGE)


  updateJob: (job) =>
    if job.id
      storeJob = @getAll(true)[job.id]
      if storeJob?
        clonedJob = JSON.parse(JSON.stringify(job))
        delete clonedJob.images
        @addJob(clonedJob)

        if Promise?
          return Promise.resolve()

  isFleetJob: (job) =>
    if !job?
      return false
    #Todo: Check if prop exists
    job.type in ["FleetInHouseJob", "FleetMotorClubJob", "FleetManagedJob" ]

  isFleetManagedJob: (job) =>
    #Todo: Check if prop exists
    job.type == "FleetManagedJob"

  removeJob: (job) =>
    @_remove(job)

  getEventId:() =>
    @eventId

  setEventId: (id) =>
    @eventId=id

  getEndpoint: =>
    Utils.getEndpoint(UserStore)

  shouldShowRequestCallback: (job) =>
    (job.type == "FleetMotorClubJob" and job.status == Consts.ASSIGNED)

  shouldShowRequestMoreTimeToAccept: (job) ->
    (
      job.type == Consts.JOB.TYPE.MOTORCLUB &&
      job.status == Consts.ASSIGNED &&
      job.issc_dispatch_request?.system == Consts.DIGITAL_DISPATCH.RSC &&
      !job.issc_dispatch_request?.more_time_requested
    )

  copyJob: (id) =>
    Api.jobCopy(store.getEndpoint(), id, success: (data) =>
          @addJob(data)
          Utils.sendNotification("Job #"+data.id+" has been created from Job #"+id+".", Consts.SUCCESS)
        error: @_handleError
    )

  requestCallbackRequest: (job, requestCallBackObj) =>
    Api.requestCallbackRequest(store.getEndpoint(), job.id, requestCallBackObj, {
        success: (data) =>
          Utils.sendNotification("Call back request sent to "+@getAccountNameFromJob(job)+" for Job #"+job.id+".", Consts.SUCCESS)
        ,
        error: () =>
          Utils.sendNotification("Call back request failed to "+@getAccountNameFromJob(job)+" for Job #"+job.id+".", Consts.ERROR)
      }
    )

  getAttachedDocuments: (job_id, type, container) ->
    docs = @getJobById(job_id)?.attached_documents

    if type? || container?
      docs = filter(docs, (doc) ->
        valid = true
        if type?
          if type == Consts.TYPE_ATTACHED_DOCUMENT
            valid &= (!doc.type? || doc.type.length == 0 || doc.type == type)
          else
            valid &= (doc.type == type)
        if container?
          valid &= (doc.container == container)
        return valid
      )
    return docs

  addOnJob: (id, callback) ->

    Api.partnerCommand({command: "tow_out", "job": "id": id}, success: (data) =>
          @addJob(data)
          Utils.sendNotification("Job #"+data.id+" has been created from Job #"+id+".", Consts.SUCCESS)
          @trigger(CHANGE)
          callback(data)
        error: @_handleError
    )

  setupJobsOfUser: ->
    #TODO: Probably can get rid of this
    if UserStore.getUser()?
      if store.getEndpoint()
        @[triggers.UPDATE_JOBS]()
      else
        Logger.debug "warning, setupJobsOfUser called, but endpoint not ready"
    else
      Logger.debug('warning, setupJobsOfUser called but no user present')

  getId: (job) ->
    return (if job.original_job_id? then job.original_job_id+":B" else job.id) + (if job.child_job? then ":A" else "")


  getWatchedId: (job_id, watcher=null) ->
    original_job_id = @get(job_id, "original_job_id", watcher)
    child_job = @get(job_id, "child_job", watcher)

    return (if original_job_id? then original_job_id+":B" else job_id) + (if child_job then ":A" else "")


  reset: () =>
    @error = null
    @type = null
    this[LOADING_ACTIVE_JOBS] = null
    @loadedActiveJobs = false
    try
      super.reset()
    catch e
      #This works most of the time, sometimes it does not, unsure why but won't cause an error
      #Will remove these classes when we switch over to graphql
      Rollbar.error("derived class calls super from method called from constructor", e)

  addSubObject: (listName, subObject, tempCallback, clonedObject) =>
    job = store.getJobById(subObject.job_id)
    foundDeletedObject = false
    if job? || clonedObject?
      if !clonedObject?
        clonedObject = JSON.parse(JSON.stringify(job))
      if !clonedObject[listName]?
        clonedObject[listName] = []

      for i in [0..clonedObject[listName].length-1]
        item = clonedObject[listName][i]
        if item? and item.id == subObject.id
          if not subObject.deleted_at?
            clonedObject[listName][i] = subObject
          else
            foundDeletedObject = true
            clonedObject?[listName]?.splice?(i, 1)
          @addRemoteItem({target: clonedObject})
          return
        else if tempCallback?
          i = tempCallback(i)

      if not subObject.deleted_at?
        clonedObject[listName].push(subObject)
        if !clonedObject.rev?
          clonedObject.rev = 1
        clonedObject.rev++

        @addRemoteItem({target: clonedObject})
      else if !foundDeletedObject
        @trigger(CHANGE)
        @trigger(CHANGE+"_"+job.id)

    #TODO: Make helper function for the complexity below
  addRemoteDocument: (subObject) =>
      job = store.getJobById(subObject.job_id)

      if job?
        clonedObject = JSON.parse(JSON.stringify(job))

        listName = "attached_documents"
        @addSubObject(listName, subObject, (i) =>
          item = clonedObject[listName][i]
          if item? and item.original_filename == subObject.original_filename and !item.url?
            clonedObject[listName]?.splice?(i, 1)
            return i-1
          return i
        , clonedObject)

  addRemoteExplanation: (subObject) =>
    @addSubObject("explanations", subObject)

  addRemoteImage: (subObject) =>
    @addSubObject("images", subObject)

  remoteImageAdded: (subObject) =>
    console.warn("Called remoteImageAdded in job_store.coffee, use addRemoteImage instead")
    @addRemoteImage("images", subObject)

  addRemoteReview: (data) =>
      job = store.getJobById(data.job_id)
      if job?
        clonedObject = JSON.parse(JSON.stringify(job))
        clonedObject.review = data
        if !clonedObject.rev?
          clonedObject.rev = 1
        clonedObject.rev++
        @addRemoteItem({target: clonedObject})

  addRemoteTwilioReply: (data) =>
      job = store.getJobById(data.job_id)
      if job?
        clonedObject = JSON.parse(JSON.stringify(job))
        if not clonedObject.review?
          clonedObject.review = {}
        if not clonedObject.review?.replies?
          clonedObject.review.replies = []
        clonedObject.review.replies.push(data)
        if !clonedObject.rev?
          clonedObject.rev = 1
        clonedObject.rev++
        @addRemoteItem({target: clonedObject})

  handleRemoteTOA: (data, showNotification=false) =>
      job = store.getJobById(data.job_id)
      if job?
        clonedObject = JSON.parse(JSON.stringify(job))
        if not clonedObject.toa?
          clonedObject.toa = {}
        switch(data.eba_type)
          when 0
            if showNotification
              Utils.sendNotification("Job #"+data.job_id+" has had the ETA updated to "+Utils.formatTime(data.time), null, {perpetual: true})
            clonedObject.toa.latest = data.time
          when 1
            clonedObject.toa.live = data.time
          when 2
            clonedObject.toa.actual = data.time
          when 3
            clonedObject.toa.latest = data.time
          when 4
            clonedObject.toa.latest = data.time
        @addRemoteItem({target: clonedObject})

  updateRemoteTOA: (data) =>
    @handleRemoteTOA(data, true)

  addRemoteTOA: (data) =>
    @handleRemoteTOA(data, true)

  getVisibleHistory: (job_id, showExtended, watcherId) =>
    job = store.getById(job_id, false, watcherId)
    history = $.map(job?.history, (value) -> value)

    getViewProps = (status) ->
      ret = {
        visible: true,
        editable: true
      }

      name = status.name
      if not name?
        ret.visible = false
        ret.editable = false
        return ret

      if name.charAt(0) == '*'
        ret.editable = false
        if UserStore.isPartner()
          ret.visible = false
          return ret
        ret.editable = false
        ret.visible = showExtended
        name = name.replace(/^\*+|$/,'')

      if name.indexOf("Job Details Emailed") > -1
        ret.visible = showExtended
        ret.editable = false
        return ret

      if UserStore.isPartner() and name.indexOf("Partner Reassign") > -1
        ret.visible = false
        ret.editable = false
        return ret

      if name == ""
        ret.visible = false
        ret.editable = false
        return ret

      if status.type == "Edited"
        ret.visible = showExtended
        ret.editable = false
        return ret

      if status.type == 'SMS' || status.type == 'TOA'
        ret.visible  = showExtended
        ret.editable = false
        return ret

      if status.type == 'SMSReply'
        ret.visible = showExtended
        ret.editable = false
        return ret

      if status.type == 'InvoiceSentAt'
        ret.visible  = not UserStore.isFleetManaged()
        ret.editable = false
        return ret

      if not UserStore.isRoot()
        if UserStore.isFleet()
          if not UserStore.isFleetInHouse()
            ret.editable = false
          else if job?.rescue_company?
            if !job?.fleet_manual
              ret.editable = false
          if not UserStore.isFleetInHouse()
            if name in [Consts.UNASSIGNED, Consts.ASSIGNED, Consts.ACCEPTED, Consts.REJECTED, Consts.REASSIGN, Consts.REASSIGNED, Consts.ETAREJECTED, Consts.AUTO_ASSIGNING]
              ret.visible = false
        else if UserStore.isPartner()
            if name not in [Consts.DISPATCHED, Consts.ENROUTE, Consts.ONSITE, Consts.TOWING, Consts.TOWDESTINATION, Consts.COMPLETED, Consts.STORED]
              ret.editable = false
            if Utils.isAssignedFromFleet(job)
              if name in [Consts.REASSIGN]
                ret.visible = false
            else
              if name in [Consts.UNASSIGNED, Consts.ASSIGNED, Consts.ACCEPTED, Consts.REJECTED, Consts.REASSIGN, Consts.REASSIGNED, Consts.ETAREJECTED]
                ret.visible = false
      if status.deleted_at?
        ret.editable = false

      if UserStore.isFleet() and !UserStore.isFleetInHouse() and name in [Consts.AUTO_ASSIGNING]
        ret.visible = false

      return ret

    history = map(history, (status) =>
      viewProps = getViewProps(status)
      ret = clone(status)
      ret.visible = viewProps.visible
      ret.editable = viewProps.editable
      return ret
    )

    history = filter(history, (status) -> status.visible)
    history = sortBy(history, (s) -> (s.adjusted_dttm || s.last_set))
    return history



  assignNewProvider: (data) =>
    callback = data.callback
    delete data.callback
    @trigger(CHANGE)
    @trigger(CHANGE+"_"+data.id)
    orig_data = data

    Api.jobAssignNewJobProvider(
      data,
      store.getEndpoint(),
      success: (data) =>
        if callback?
          callback(data)

       Utils.sendNotification("Job ##{@getId(data)} has been reassigned.", Consts.SUCCESS)
      error: (data, textStatus, errorThrown) =>
        if callback?
          callback()
        if data.status?
          Dispatcher.send(ErrorStore.SHOW_ERROR,
            data: data
            textStatus: textStatus
            errorThrown: errorThrown
            forceMsg: "Uh oh! There was an issue with your connection. Please refresh and try again.")
        else
          @handleError(data, textStatus, errorThrown)
    )

  choosePartner: (data) =>
    callback = data.callback
    delete data.callback

    @trigger(CHANGE)
    @trigger(CHANGE+"_"+data.id)
    orig_data = data

    Api.jobChoosePartner(
      data,
      store.getEndpoint(),
      success: (data) =>
        this.addJob(data)
        if callback?
          callback(data)
      error: (data, textStatus, errorThrown) =>
        if callback?
          callback()
        if data.status?
          Dispatcher.send(ErrorStore.SHOW_ERROR,
            data: data
            textStatus: textStatus
            errorThrown: errorThrown
            forceMsg: "Uh oh! There was an issue with your connection. Please refresh and try again.")
        else
          @handleError(data, textStatus, errorThrown)
    )

  changePaymentMethod: (data) =>
      callback = data.callback
      delete data.callback
      @updateJob(data)
      @trigger(CHANGE)
      @trigger(CHANGE+"_"+data.id)
      orig_data = data

      Api.jobChangePaymentMethod(
        data,
        store.getEndpoint(),
        success: (data) =>
          if callback?
            callback(data)

         Utils.sendNotification("Payment Type changed for job ##{@getId(data)}.", Consts.SUCCESS)
        error: (data, textStatus, errorThrown) =>
          if callback?
            callback()
          if data.status?
            Dispatcher.send(ErrorStore.SHOW_ERROR,
              data: data
              textStatus: textStatus
              errorThrown: errorThrown
              forceMsg: "Uh oh! There was an issue with your connection. Please refresh and try again.")
          else
            @handleError(data, textStatus, errorThrown)
      )


store = new JobStore()

store[key] = val for key, val of triggers

module.exports = store


#
