BaseStore    = require('stores/base_store').default
UserStore    = require('stores/user_store').default
CompanyStore = require('stores/company_store').default
import PaymentMethodStore from 'stores/payment_method_store'

class PaymentStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Payment'
    })

  isLazyLoaded: -> true

  isEditable: -> false

  loadCollectionOnUserChanged: -> false

  getPaymentStatusByName: (paymentMethodName) ->

    if paymentMethodName in ['ACH', 'Cash', 'Check', 'Credit Card']
      return 'Received'
    else if paymentMethodName in ['Discount', 'Write Off']
      return 'Applied'
    else if paymentMethodName == 'Refund'
      return 'Refunded'

    return ' '

  getPaymentStatusBy: (paymentMethodId) ->
    paymentMethodName = PaymentMethodStore.get(paymentMethodId, 'name')
    return @getPaymentStatusByName(paymentMethodName)

store = new PaymentStore()

module.exports = store
