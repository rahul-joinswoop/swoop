BaseStore = require('stores/base_store').default
CompanyStore = require('stores/company_store').default
import Consts from 'consts'

class SymptomStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Symptom'
      stores: {
        CompanyStore: CompanyStore
      }
    })

  shouldAddUserTypeToApiUrl: -> true
  getCompanyAttribute: -> "symptoms"
  isEditable: -> true

  loadCollectionOnUserChanged: -> return false

  isLazyLoaded: -> return true

  hasPermissions: ->
    return this.stores.UserStore.isFleet()

  isFlatTire: (symptomId, watcherId = null) ->
    if !symptomId then return false

    @getById(symptomId, true, watcherId)?.name == Consts.SYMPTOM_FLAT_TIRE

store = new SymptomStore()

module.exports = store
