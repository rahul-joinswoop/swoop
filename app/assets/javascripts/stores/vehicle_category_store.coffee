import Consts from 'consts'
BaseStore = require('stores/base_store').default
UserStore = require('stores/user_store')
CompanyStore = require('stores/company_store').default
import Api from 'api'
import { filter, sortBy } from 'lodash'

class VehicleCategoryStore extends BaseStore
  constructor: ->
    super({
      modelName: 'VehicleCategory'
      stores: {
        CompanyStore: CompanyStore
      }
    })

    @_sortedVehicleCategoriesList = null

  loadCollectionOnUserChanged: -> return false
  isLazyLoaded: -> true

  getModelName: -> 'vehicle_category'
  getCompanyAttribute: -> "vehicle_categories"
  getUrl: -> 'vehicle_categories'

  getEditUrl: -> @getUrl()


  getAllSorted: (watcherId = null, filterByIds = null) =>
    # TODO: this should be cached, and cache broken when the company changes

    cats = @getCompanyItems(watcherId)
    if filterByIds
      cats = filter(cats, (type) ->
        return type.id in filterByIds
      )
    return sortBy(cats, 'order_num')

  getPartnerVehicleCategories: (partner_id, callback) ->
    Api.getPartnerVehicleCategories(partner_id, {
      success: (data) =>
        ids = data.map((vehicleCategory) -> vehicleCategory.id)

        callback(ids)
      error: (data, textStatus, errorThrown) =>
        sendNotification("Failed to get truck types", Consts.ERROR)

        @_handleError(data, textStatus, errorThrown)
      })

store = new VehicleCategoryStore()

module.exports = store
