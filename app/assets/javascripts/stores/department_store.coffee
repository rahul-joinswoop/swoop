BaseStore = require('stores/base_store').default
CompanyStore = require('stores/company_store').default
import { filter, map, without } from 'lodash'
import Api from 'api'

class DepartmentStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Department'
      stores: {
        CompanyStore: CompanyStore
      }
    })

  loadCollectionOnUserChanged: -> return false

  isLazyLoaded: -> return true

  getDeleteHTTPMethod: -> Api.DELETE

  isEditable: ->
    UserStore.isFleet() || UserStore.isPartner()

  getDepartments: (watcherId) =>
    company_id = this.stores.UserStore.getCompany(watcherId)?.id
    if company_id?
      departments = this.stores.CompanyStore.get(company_id, "departments", watcherId)
      cats = map(departments, (id) =>
        cat = @getById(id, true, watcherId)
        if cat? and not cat.deleted_at?
          return cat
        return null
      )
      return without(cats, null)

  getDepartmentByName: (name, watcherId) =>
    #TODO: cache this
    #TODO: Broken because it won't update watcherId when departments are loaded.
    arr = filter(@getAll(), {name: name})
    if arr.length > 0
      return arr[0]

store = new DepartmentStore()

module.exports = store
