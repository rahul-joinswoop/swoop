import moment from 'moment'
import Api from 'api'
import Logger from 'lib/swoop/logger'
import { filter, keys, map, uniq, zipObject } from 'lodash'
MicroEvent = require('microevent-github')

triggers =
    CHANGE : "change"

window[key] = val for key, val of triggers

EdmundsStore = (->
  @makes={}
  @types={}

  #Used as caches for quicker lookups
  @typeToMake={}
  @makeToType={}
  @typeToModels = {}
  @edmundsData=null
  @ed = null

  @types_success = (res) =>
    for type in res
      @types[type.name] = {}

  @makes_success = (res) =>
    @edmundsData=res
    for make in res
      @makes[make.id]=
        id:make.id
        name:make.name
        models:zipObject(map(make.models, "id"), make.models)

      for model in make.models
        if model.vehicle_type?
          if !@types[model.vehicle_type]?
            @types[model.vehicle_type] = []
            @typeToMake[model.vehicle_type] = []
          if !@makeToType[make.name]?
            @makeToType[make.name] = []
          if !@typeToModels[model.vehicle_type]?
            @typeToModels[model.vehicle_type] = []

          @typeToModels[model.vehicle_type].push(model)
          @makeToType[make.name].push(model.vehicle_type)
          @typeToMake[model.vehicle_type].push(make.name)
          @types[model.vehicle_type].push(
            id: make.id
            name: make.name
            model: model
          )

    for key of @typeToMake
      @typeToMake[key] = uniq(@typeToMake[key])

    for key of @makeToType
      @makeToType[key] = uniq(@makeToType[key])

    trigger(CHANGE)

  @makes_failure = (res) ->
    Logger.debug('edmunds makes failed',res)

  @types_failure = (res) ->
    Logger.debug('edmunds types failed',res)

  store =
      getMakes: =>
        @makes
      getMakeNames: (vtype) =>
        if vtype? and @typeToMake[vtype]?
          @typeToMake[vtype]
        else
          map(@makes, (make) ->
            make.name
          )

      getTypeNames: (make) =>
        if make? and @makeToType[make]?
          @makeToType[make]
        else
          keys(@types)

      getModelNamesByMakeName: (make_name, vtype) ->
        map(store.getModelsByMakeName(make_name, vtype), (make) ->
          make.name
        )


      getModels: (make_id, vtype) =>
        ret=@makes[make_id]?.models
        if !ret? and vtype?
          ret = @typeToModels[vtype]
        else
          if vtype? and vtype.length > 0
            ret = filter(ret, (model) ->
              model.vehicle_type == vtype
            )
        ret

      getModelsByMakeName: (make_name, vtype) ->

        make_id = store.getMakeIdByName(make_name)
        store.getModels(make_id, vtype)

      getYears: (make_id,model_id) ->
        model = store.getModels(make_id)?[model_id]
        ret = []
        if model?
          ret = model.years
        if !ret? || ret.length == 0
          current = moment().year()
          ret = [(current-30)..current]

        ret

      getYearsByName: (make_name,model_name) ->
        make_id = store.getMakeIdByName(make_name)
        model_id = store.getModelIdByName(make_id, model_name)
        store.getYears(make_id, model_id)

      getMakeName: (make_id) =>
        @makes[make_id].name

      getModelName: (make_id,model_id) ->
        store.getModels(make_id)[model_id].name

      getMakeIdByName: (make_name) =>
        for k,make of @makes
          if make.name == make_name
            return make.id
      getModelIdByName: (make_id,model_name) ->
        for k,model of store.getModels(make_id)
          if model.name == model_name
            return model.id
      setupEdmunds: () ->
        setupEdmunds()


    store[key] = val for key, val of triggers
    MicroEvent.mixin( store )
    trigger = store.trigger.bind(store)

    setupEdmunds = =>
      Api.vehicleMakes
        success: @makes_success
        error: @makes_fail
      Api.vehicleExtraTypes
        success: @types_success
        error: @types_fail



    #initial setup
    window.estore = store
    return store
).bind({})()


module.exports = EdmundsStore
