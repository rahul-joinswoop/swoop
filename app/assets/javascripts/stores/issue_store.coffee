BaseStore = require('stores/base_store').default
UserStore = require('stores/user_store')

class IssueStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Issue'
    })

  getEditUrl: -> @getUrl()

  shouldAddUserTypeToApiUrl: -> true

  hasPermissions: ->
    UserStore.isSwoop() || UserStore.isFleetInHouse()

  loadCollectionOnUserChanged: -> false

store = new IssueStore()

module.exports = store
