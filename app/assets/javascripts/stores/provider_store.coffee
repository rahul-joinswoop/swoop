import Api from 'api'
import { filter, find, map, values } from 'lodash'
SearchStore = require('stores/search_store')
UserStore = require('stores/user_store')
class ProviderStore extends SearchStore
  constructor: ->
    super({
      modelName: 'Provider'
    })

    @_providerBySiteIndex = {}

    @LIST_TIRE_PROGRAM = 'list_tire_program'
    @LIST_GROUPED_BY_PARTNER = 'list_grouped_by_partner'

    @TIRE_PROGRAM_IDS = null
    @IDS_GROUPED_BY_PARTNER = null

    @LOADING_TIRE_PROGRAM_IDS = false
    @LOADING_IDS_GROUPED_BY_PARTNER = false

  isLazyLoaded: -> true

  loadCollectionOnUserChanged: -> false

  shouldSetCompanyDefaultsOnObjectLoad: -> true

  isEditable: -> true

  hasPermissions: ->
    return UserStore.isSwoop() || UserStore.isFleet()

  # TODO not used, we should cache this list if used by some component
  getLiveProviders: =>
    filter(values(@getAll()), (provider) ->
      return provider.live
    )

  # only used during WS message on application_events.coffee
  #Including deleted providers because occasionally the linked provider will be deleted
  getProviderByCompanyId: (id) ->
    find(
      values(@getAll(true)), (provider) -> provider.company.id == id
    )

  getPropertyBySiteId: (siteId, property, watcherId = null, showDeleted = false) =>
    if !siteId
      return null

    if @_providerBySiteIndex[siteId]
      return this.get(@_providerBySiteIndex[siteId], property)

    providers = this._searchAndLazyLoadBy({site_id: siteId}, property, watcherId, showDeleted)

    if providers
      provider = providers[0]
      # we cache the result id in the 'providerId by siteId index'
      @_providerBySiteIndex[siteId] = provider.id

      provider[property]

  getTireProgramProviders: (watcherId = null, property = null) =>
    if !UserStore.isTesla()
      return []

    if watcherId
      @_registerListListener(watcherId, @LIST_TIRE_PROGRAM, property)

    if @TIRE_PROGRAM_IDS == null
      @_loadTireProgramProvidersIds()
    else
      return map(@TIRE_PROGRAM_IDS, (providerId) => this.getById(providerId))

    # GO THROUGH PROVIDERS AND FIND TIRE PROGRAM ONES
    providers = values(@getAll())

    filter(providers, (provider) -> provider.tire_program)

  isTireProgram: (provider) ->
    !!provider.tire_program

  getListGroupedByPartner: (watcherId = null, property = null) ->
    if !UserStore.isFleetInHouse()
      return []

    if watcherId
      @_registerListListener(watcherId, @LIST_GROUPED_BY_PARTNER, property)

    if @IDS_GROUPED_BY_PARTNER == null
      @_loadIdsGroupedByPartner()
    else
      return map(@IDS_GROUPED_BY_PARTNER, (providerId) => this.getById(providerId))

  _getListsObject: =>
    return [
      { list: @TIRE_PROGRAM_IDS, validator: @isTireProgram, triggerName: @LIST_TIRE_PROGRAM }
      { list: @IDS_GROUPED_BY_PARTNER, validator: (() -> true), triggerName: @LIST_GROUPED_BY_PARTNER }
    ]

  _loadTireProgramProvidersIds: =>
    if !UserStore.isTesla()
      return

    if !@LOADING_TIRE_PROGRAM_IDS
      @LOADING_TIRE_PROGRAM_IDS = true

      Api.getTireProgramProviders(
        success: (data) =>
          for row in data
            @_add(row)

          @TIRE_PROGRAM_IDS = map(data, 'id')

          @_triggerListListeners(@LIST_TIRE_PROGRAM)

          @LOADING_TIRE_PROGRAM_IDS = false

        error: (data, textStatus, errorThrown) =>
          @LOADING_TIRE_PROGRAM_IDS = false
      )

  _loadIdsGroupedByPartner: =>
    if !UserStore.isFleetInHouse()
      return

    if !@LOADING_IDS_GROUPED_BY_PARTNER
      @LOADING_IDS_GROUPED_BY_PARTNER = true

      Api.getProvidersGroupedByPartner(
        success: (data) =>
          for row in data
            @_add(row)

          @IDS_GROUPED_BY_PARTNER = map(data, 'id')

          @_triggerListListeners(@LIST_GROUPED_BY_PARTNER)

          @LOADING_IDS_GROUPED_BY_PARTNER = false

        error: (data, textStatus, errorThrown) =>
          @LOADING_IDS_GROUPED_BY_PARTNER = false
      )

store = new ProviderStore()

module.exports = store
