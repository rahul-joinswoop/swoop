import Logger from 'lib/swoop/logger'
MicroEvent = require('microevent-github')
import Dispatcher from 'lib/swoop/dispatcher'

triggers =
    CHANGE : "change"
    SHOW_ERROR: "show_error"
    CLEAR_ERRORS: "clear_errors"

window[key] = val for key, val of triggers

ErrorStore = (->
  error=null
  store =
    getError:() ->
      error

  store[key] = val for key, val of triggers
  MicroEvent.mixin( store )
  trigger = store.trigger.bind(store)


  @[SHOW_ERROR] = (pdata) ->
    Logger.debug("forceMsg", pdata.forceMsg)
    if pdata?.forceMsg?
      error = pdata.forceMsg
    else
      data = pdata.data
      textStatus = pdata.textStatus
      errorThrown = pdata.errorThrown


      if data? and data.responseJSON? and data.responseJSON.message?
        error = div null,
          data.responseJSON.message
          br(),
          br(),
          data.responseJSON.action
        setTimeout(( ->
          trigger(CHANGE)
        ),1000)
        return
      else if data?.statusText?
        error = data.statusText
      else if textStatus?
        error = textStatus
      else if errorThrown?
        error = errorThrown
      else
        error = "Unknown error"

      if data?.status?
        error = data.status + " " + error

      if data?.status == 0 && data?.statusText == "error"
        error = "Problem connecting to server"

      if textStatus == "timeout"
        error = "Request Timeout"
      if textStatus == "parsererror"
        error = "Problem parsing the response"

      error = "Error: " + error
      #QUICK HACK TO MAKE THIS WORK
    setTimeout(( ->
      trigger(CHANGE)
    ),1000)

  @[CLEAR_ERRORS] = (data) ->
    error = null
    trigger(CHANGE)

  Dispatcher.register(( payload ) =>
    Logger.debug("in payload of vehicle store dispatcher",payload)
    if not payload.eventName
      console.warn("EMPTY EVENT: ", payload)
      #throw new Error('empty Event')
    if typeof @[payload.eventName] is 'function'
      @[payload.eventName](payload.data)
    true
  )

  return store
).bind({})()


module.exports = ErrorStore
