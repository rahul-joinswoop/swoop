import Consts from 'consts'
import Api from 'api'
BaseStore = require('stores/base_store').default
UserStore = require('stores/user_store')
TireTypeStore = require('stores/tire_type_store')
import Utils from 'utils'

import Logger from 'lib/swoop/logger'
import { difference, find, forEach, keys, map, some, zipObject } from 'lodash'

# InventoryItem can be of Tire or Vehicle types.
# This store works with tire type only.
class TireStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Tire'
    })

    @TIRE_BY_SITE_AND_TYPE_LIST_NAME = 'tireBySiteAndTypeList'
    @TIRE_BY_SITE_AND_TYPE_LIST = null
    @LOADING_TIRE_BY_SITE_AND_TYPE_LIST = false

  hasPermissions: ->
    return UserStore.isPartner() || UserStore.isFleet()

  isEditable: -> true

  loadCollectionOnUserChanged: ->
    false

  isLazyLoaded: ->
    false

  getUrl: ->
    if UserStore.isPartner()
      "partner/inventory_items"
    else
      "fleet/inventory_items"

  getEditUrl: -> @getUrl()

  getModelName: ->
    'inventory_item'

  updateTiresBySiteList: (tireInventoryItem) ->
    # only update the list if it has been loaded
    if @TIRE_BY_SITE_AND_TYPE_LIST
      siteId = tireInventoryItem.site_id
      tireTypeId = tireInventoryItem.item.tire_type_id

      tire = @_findBySiteIdAndTypeId(siteId, tireTypeId)

      if tire
        if tireInventoryItem.deleted_at
          tire.tire_quantity -= 1
        else
          tire.tire_quantity += 1

      else
        # In this case the site had 0 tires when the collection was loaded
        # thus this has not been loaded so far; we add it.
        #
        # (The other option would go to BE and fetch the list again. I don't think it's needed though.)
        @TIRE_BY_SITE_AND_TYPE_LIST.push(
          site_id: tireInventoryItem.site_id
          tire_type_id: tireInventoryItem.item.tire_type_id
          tire_quantity: 1
        )

      @_triggerListListeners(@TIRE_BY_SITE_AND_TYPE_LIST_NAME)

  # The id of the inventoryItem doesn't matter, we just need to delete one
  # BE will take the last one filtered by siteId, typeId, companyId and set deleted_at.
  decreaseTireQuantity: (siteId, typeId) ->
    Api.decreaseTireQuantity(UserStore.getEndpoint(), siteId, typeId,
      success: (record) =>
        message = "Tire size #{TireTypeStore.getBaseTireTypeName(record.item.tire_type_id)} was deleted"

        Utils.sendNotification(message, Consts.SUCCESS)
      error: (data, textStatus, errorThrown) =>
        Logger.debug('tire quantity could not be decreased')
    )

  getTireQuantityBySiteAndType: (siteId = null, tireTypeId, watcherId = null) ->
    if UserStore.isPartner() || UserStore.isFleetInHouse()
      @_loadTireBySiteAndTypeListIfNeeded(watcherId)

      return @_findBySiteIdAndTypeId(siteId, tireTypeId)?.tire_quantity || null

    return {}

  partnerHasSiteWithType: (tireTypeId, watcherId = null) ->
    @_loadTireBySiteAndTypeListIfNeeded(watcherId)

    if !UserStore.isPartner()
      return false

    some(@TIRE_BY_SITE_AND_TYPE_LIST, (tire) => tire.tire_type_id == tireTypeId)

  getNotAddedTiresListForStore: (watcherId = null, excludeIds = null) ->
    tireTypesIds = map(keys(TireTypeStore.getAll()), (tireTypeKey) -> parseInt(tireTypeKey))

    @_loadTireBySiteAndTypeListIfNeeded(watcherId)

    tireTypesIds = difference(tireTypesIds, excludeIds)

    forEach(@TIRE_BY_SITE_AND_TYPE_LIST, (tire) ->
      tireTypeIndex = tireTypesIds.indexOf(tire.tire_type_id)

      if tireTypeIndex > -1
        tireTypesIds.splice(tireTypeIndex, 1)
    )

    zipObject(
      tireTypesIds, map(tireTypesIds, (id) ->
        {name: TireTypeStore.getTireTypeName(id)}
      )
    )

  _findBySiteIdAndTypeId: (siteId, tireTypeId) ->
    find(@TIRE_BY_SITE_AND_TYPE_LIST, (tire) ->
      tire.site_id == siteId and tire.tire_type_id == tireTypeId)

  _loadTireBySiteAndTypeListIfNeeded: (watcherId) ->
    if(watcherId)
      @_registerListListener(watcherId, @TIRE_BY_SITE_AND_TYPE_LIST_NAME)

    if @TIRE_BY_SITE_AND_TYPE_LIST == null
      if !@LOADING_TIRE_BY_SITE_AND_TYPE_LIST
        @LOADING_TIRE_BY_SITE_AND_TYPE_LIST = true

        Api.getTireQuantityBySiteAndType(
          UserStore.getEndpoint(),
          success: (data) =>
            @TIRE_BY_SITE_AND_TYPE_LIST = data
            @LOADING_TIRE_BY_SITE_AND_TYPE_LIST = false

            @_triggerListListeners(@TIRE_BY_SITE_AND_TYPE_LIST_NAME)
          ,
          error: (data, textStatus, errorThrown) =>
            @LOADING_TIRE_BY_SITE_AND_TYPE_LIST = false
        )

  getAddedMessage: (record) ->
    "Tire size #{TireTypeStore.getBaseTireTypeName(record.item.tire_type_id)} was added"

store = new TireStore()

module.exports = store
