import Api from 'api'
import Consts from 'consts'
import UserStore from 'stores/user_store'

class JobBillingInfoStore {
  getRequests = {}

  getBillingInfo(jobId) {
    if (this.getRequests[jobId]) {
      return this.getRequests[jobId]
    }

    return this.getRequests[jobId] = new Promise((resolve, reject) => {
      Api.request(Api.GET, `${UserStore.getEndpoint()}/jobs/${jobId}/billing_info`, null, {
        success: resolve,
        error: reject,
      }).always(() => delete this.getRequests[jobId])
    })
  }

  isVcc(billingInfo) {
    return billingInfo.billing_type === Consts.JOB.BILLING_TYPE.VCC
  }
}

const jobBillingInfoStore = new JobBillingInfoStore()

export default jobBillingInfoStore
