import BaseStore from 'stores/base_store'
import CompanyStore from 'stores/company_store'

class TagStore extends BaseStore {
  constructor() {
    super({
      modelName: 'Tag',
      // TODO: Shoulnd't need to do this, should pull in automatically if getCompanyAttribute is defined
      stores: {
        CompanyStore,
      },
    })
  }

  shouldAddUserTypeToApiUrl = () => false

  isLazyLoaded = () => true

  loadCollectionOnUserChanged = () => false

  isEditable = () => true

  hasPermissions = () => this.stores.UserStore.isSwoop() || this.stores.UserStore.isFleetInHouse()
}

export default new TagStore()
