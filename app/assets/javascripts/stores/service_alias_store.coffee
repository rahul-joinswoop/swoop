BaseStore = require('stores/base_store').default
ServiceStore = require('stores/service_store')
import { find } from 'lodash'

class ServiceAliasStore extends BaseStore
  constructor: ->
    super({
      modelName: 'ServiceAlias'
      stores: {
        ServiceStore: ServiceStore
      }
    })

  getUrl: -> "service_aliases"

  getServiceNameOrAlias: (service_code_id, alias_id, watcherId) =>
    #TODO: Cache this
    if !service_code_id?
      return ""

    if alias_id?
      alias = find(@getAll(), (alias) => alias.id == alias_id)
      if alias?
        return alias.alias
    service = this.stores.ServiceStore.getById(service_code_id, true, watcherId)

    if service?
      return service.name

    return "Loading"

  getServiceAliasByName: (alias_name) =>
    #TODO: Cache this
    return find(@getAll(), (alias) => alias.alias == alias_name)

store = new ServiceAliasStore()

module.exports = store
