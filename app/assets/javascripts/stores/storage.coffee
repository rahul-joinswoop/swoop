import Logger from 'lib/swoop/logger'

Storage = (->
  cache = {}
  store =
    setSession: (value) ->
      if sessionStorage?
        sessionStorage.setItem("uid", value)

    getSession: () ->
      try
        if sessionStorage? and sessionStorage.getItem("uid")?
          return sessionStorage.getItem("uid")
        return ""
      catch e
        # if seeing lots of new Rollbar bugs with (locale || "").split is not a function or
        # Failed prop type: Invalid prop `locale` of type `array` supplied to `IntlProvider`, expected `string`.
        # then try uncommenting the commented code below and comment out lines: Logger.debug , return ""

        # alert('Please enable cookies on your browser to use Swoop.')
        # window?.location = 'https://www.google.com/search?q=how+to+enable+cookies'
        # throw new Error("No window.sessionStorage available. Probably because user disabled browser cookies. Exiting Swoop.")

        Logger.debug("storage Unable to access sessionStorage", e)
        return ""

    set: (key, value) ->
      key = store.getSession() + key

      cache[key] = value
      try
        localStorage.setItem(key, value)
      catch e
        Logger.debug("storage QUOTA EXCEEDED or inaccessable", e)
        window?.analytics?.track 'Local Storage Write Fail',
          error: JSON.stringify(e)

    get: (key) ->
      key = store.getSession() + key

      if cache[key]?
        return cache[key]
      else
        try
          value = localStorage.getItem(key)
          cache[key] = value
          return value
        catch e
          Logger.debug("storage Unable to access local storage ", e)
          window?.analytics?.track 'Local Storage Get Fail',
            error: JSON.stringify(e)

    remove: (key) ->
      key = store.getSession() + key

      delete cache[key]
      try
        localStorage.removeItem(key)
      catch e
        Logger.debug("storage unable to remove from local storage", e)
        window?.analytics?.track 'Local Storage Delete Fail',
          error: JSON.stringify(e)

  return store
).bind({})()

module.exports = Storage
