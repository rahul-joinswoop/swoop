BaseStore = require('stores/base_store').default
CompanyStore = require('stores/company_store').default
import { filter } from 'lodash'

class CustomerTypeStore extends BaseStore
  constructor: ->
    super({
      modelName: 'CustomerType'
      stores: {
        CompanyStore: CompanyStore
      }
    })

  hasPermissions: ->
    return true

  shouldAddUserTypeToApiUrl: ->
    true

  loadCollectionOnUserChanged: -> return false
  isLazyLoaded: -> true

  isEditable: ->
     return this.stores.UserStore.isSwoop()

  getUrl: ->
    'customer_types'

  getEditUrl: ->
    'customer_types'

  _getJSONName: ->
    'customer_type'
  getCompanyAttribute: ->
    'customer_type_ids'

  getCustomerTypeByName: (name) =>
    #TODO: Cache this and make it a find
    arr = filter(@getAll(), {name: name})
    if arr.length > 0
      return arr[0]



store = new CustomerTypeStore()

module.exports = store
