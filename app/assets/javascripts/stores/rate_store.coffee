import Consts from 'consts'
import Utils from 'utils'

BaseStore = require('stores/base_store').default
UserStore = require('stores/user_store')
ServiceStore = require('stores/service_store') # TODO check if method that uses this can be refactored
import Logger from 'lib/swoop/logger'
import Api from 'api'
import { filter, map, pickBy, union, uniq, values, without, zipObject } from 'lodash'

class RateApi
  endpointBase: ->
    "#{UserStore.getEndpoint()}/rates"

  markRateAsLive: (rate, response) =>
    Api.request(Api.POST, "#{@endpointBase()}/live", { rate: rate }, response)

  deleteRates: (rateType, response) =>
    Api.request(Api.DELETE, @endpointBase(), rateType, response)

  fetchRate: (provider_id, service_id, response) ->
    Api.request(Api.GET, "/fleet/rates/provider/#{provider_id}/service/#{service_id}", null, response)

  getAccountsWithRates: (response) ->
    Api.request(Api.GET, "partner/accounts/with_rates", null, response)

  getRatesByAccountAndService: (type, account_id, service_id, response) ->
    Api.request(Api.GET, type+"/rates/accounts/"+account_id+"/services/"+service_id, null, response)

  createServiceRates: (account_id, service_id, response) ->
    Api.request(Api.POST, "partner/rates/accounts/"+account_id+"/services/"+service_id, null, response)

  createServiceRatesForPartner: (partner_id, service_id, response) ->
    Api.request(Api.POST,
      "fleet/rates/provider/" + partner_id + "/services/" + service_id,
      null, response)

  getServicesByPartnerWithRates: (proivder_id, response) ->
    Api.request(Api.GET, "fleet/services/with_rates/provider/"+proivder_id, null, response)

  getServicesWithRates: (account_id, response) ->
    Api.request(Api.GET, "partner/services/with_rates/accounts/"+account_id, null, response)

  createAccountRates: (id, response) ->
    Api.request(Api.POST, "partner/rates/accounts/"+id, null, response)

  getProviderAddons: (id, response) ->
    Api.request(Api.GET
      "fleet/rates/provider/"+id+"/addons"
      {}
      response)

# {name: "rate", permissions: "partner", editable: true, load: false, getUrl: "partner/rates", editUrl: "partner/rates"}
class RateStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Rate'
    })

    @rateApi = new RateApi()

    @ratesByAccountLoading = []
    @accountsWithRates = null

    @servicesLoading = []
    @servicesWithRates = []

    @ratesLoading = []

    @accountsLoading = false

    @CHANGED_RATES = 'changed_rates'
    @ADD_HOURLY_RATES = 'add_hourly_rates'

  loadCollectionOnUserChanged: -> false

  getUrl: ->
    'partner/rates'

  editUrl: ->
    'partner/rates'

  hasPermissions: ->
    UserStore.isPartner()

  markLive: (rate) =>
    response =
      success: (data) =>
        @trigger(@CHANGED_RATES, data)
      ,
      error: (data) ->
        Logger.debug('POST mark rate live error', data)
        Utils.sendErrorNotification('Problem marking the rate as live')

    @rateApi.markRateAsLive(rate, response)

  deleteRates: (rateType) =>
    response =
      success: (data) =>
        @trigger(@CHANGED_RATES, data)
      ,
      error: (data) ->
        Logger.debug('DELETE service rates error', data)
        Utils.sendErrorNotification('Failed to delete service rates')

    @rateApi.deleteRates(rateType, response)

  getRateByValue: (service_id, account_id) =>
    arr = filter(@getAll(), {service_code_id: service_id, account_id: account_id})
    if arr.length > 0
      return arr[0]

    arr = filter(@getAll(), {service_code_id: service_id, account_id: null})
    if arr.length > 0
      return arr[0]

  getRateByPartner: (service_id, partner_id) =>
    arr = filter(@getAll(), {service_code_id: service_id, partner_id: partner_id})

    if arr.length > 0
      return arr[0]
    else
      @rateApi.fetchRate(partner_id, service_id, {
        success: (data) =>
          if data
            rate = data
            rate.partner_id = partner_id
            rate.service_code_id = service_id
            rate.id = rate.id + "_" + partner_id + "_" + service_id
            @_add(rate)
        error: (remoteData, textStatus, errorThrown) ->
          @_handleError(remoteData, textStatus, errorThrown)
      })
    return null

  getAccountIdsFromRates: (callback) =>
    if !@accountsWithRates and !@accountsLoading
      @accountsLoading = true

      @rateApi.getAccountsWithRates({
          success: (data) =>
            @accountsLoading = false
            @accountsWithRates = map(data, (account) -> account.id)
            Logger.debug("book got back: ", @accountsWithRates)
            if callback
              callback()
          error: (data, textStatus, errorThrown) =>
              Logger.debug("book got back: ", data, textStatus, errorThrown)

              Utils.sendNotification("Failed to get accounts with rates", Consts.ERROR)
              @_handleError(data, textStatus, errorThrown)
      })

    if @accountsWithRates
      accounts = uniq(map(@getAll(), (rate) -> rate.account_id)) # TODO avoid it, so we can have a more efficient cache
      @accountsWithRates = union(accounts, @accountsWithRates)

    return @accountsWithRates

  getApiRates: (account_id, service_id, callback, errorCallack) =>
    if !account_id
      account_id = null

    if !service_id
      service_id = null

    if !@ratesLoading[account_id]
      @ratesLoading[account_id] = []

    if !@ratesLoading[account_id][service_id]
      @ratesLoading[account_id][service_id] = 1

      @rateApi.getRatesByAccountAndService(UserStore.getEndpoint(), account_id, service_id, {
        success: (data) =>
          @ratesLoading[account_id][service_id] = 2

          for rate in data
            @_add(rate, false)

          if callback
            callback()

          @trigger(RateStore.CHANGED)

        error: (data, textStatus, errorThrown) =>
            errorCallack?()

            Utils.sendNotification("Failed to get rates with rates", Consts.ERROR)
            @_handleError(data, textStatus, errorThrown)
      })

      return null

    return @getRatesByObject(
      account_id: account_id
      service_code_id: service_id
    )

  getRatesByObject: (obj) =>
    rates = filter(values(@getAll()), obj)
    rates = filter(rates, (rate) ->
      typeof(rate.deleted_at) == "undefined" || rate.deleted_at == null
    )

  getRatesBykey: (account_id, site_id, service_code_id, rate_type, vehicle_type) =>
    @getRatesByObject(
      {
        account_id:  account_id
        site_id: site_id
        service_code_id: service_code_id
        type: rate_type
        vehicle_type: vehicle_type
      }
    )

  addTempServiceToAccount: (account_id, service_id) ->
    if !@servicesWithRates[account_id]
      @servicesWithRates[account_id] = []

    @servicesWithRates[account_id].push(service_id)

  addTempServiceToPartner: (provider_id, service_id) ->
    if !@servicesWithRates[provider_id]
      @servicesWithRates[provider_id] = []

    @servicesWithRates[provider_id].push(service_id)

  createServiceRates: (account_id, service_id, callback) =>
    @rateApi.createServiceRates(account_id, service_id, {
      success: (data) =>
        @addTempServiceToAccount(account_id, service_id)
        callback()
        #Should a change be triggered here?
      error: (data, textStatus, errorThrown) =>
        Utils.sendNotification("Failed to create account rates", Consts.ERROR)
        @_handleError(data, textStatus, errorThrown)
      })

  createServiceRatesForPartner: (partner_id, service_id, callback) =>
    @rateApi.createServiceRatesForPartner(partner_id, service_id, {
      success: (data) =>
        @addTempServiceToAccount(partner_id, service_id)
        callback()
      error: (data, textStatus, errorThrown) =>
        Utils.sendNotification("Failed to create account rates", Consts.ERROR)
        @_handleError(data, textStatus, errorThrown)
      })

  getRatesByPartner: (partner_id, service_id, callback) =>
    if not partner_id?
      partner_id = null

    if not service_id?
      service_id = null

    if !@ratesLoading[partner_id]
      @ratesLoading[partner_id] = []

    if !@ratesLoading[partner_id][service_id]
      @ratesLoading[partner_id][service_id] = 1

      @rateApi.fetchRate(partner_id, service_id, {
          success: (data) =>
            @ratesLoading[partner_id][service_id] = 2
            for rate in data
              rate.provider_id = partner_id
              @_add(rate)
            if callback
              callback()
          error: (data, textStatus, errorThrown) =>
            Logger.debug("book got back: ", data, textStatus, errorThrown)

            Utils.sendNotification("Failed to get rates with rates", Consts.ERROR)
            @_handleError(data, textStatus, errorThrown)
      })

      return null

    return @getRatesByObject(
      provider_id: partner_id
      service_code_id: service_id
    )

  markRatesAsDeleted: (obj) =>
    rates = @getRatesByObject(obj)

    if rates
      for rate in rates
        rate.deleted_at = true
        @_update(rate)

  deleteAccountRates: (account_id, callback) =>
    obj = {account_id: account_id}

    @rateApi.deleteRates({ rates: obj }, {
      success: (data) =>
        @markRatesAsDeleted(obj)

        if @accountsWithRates
          @accountsWithRates = without(@accountsWithRates, account_id)

        if callback
          callback()
      error: (data, textStatus, errorThrown) =>
        Logger.debug("book got back: ", data, textStatus, errorThrown)

        Utils.sendNotification("Failed to delete account rates", Consts.ERROR)
        @_handleError(data, textStatus, errorThrown, true)
    })

  deleteServiceRates: (entity_id, service_id, callback) =>
    if UserStore.isPartner()
      obj = { account_id: entity_id, service_code_id: service_id }
    else
      obj = { provider_id: entity_id, service_code_id: service_id }

    @rateApi.deleteRates({ rates: obj }, {
        success: (data) =>
          @markRatesAsDeleted(obj)

          if @servicesWithRates and @servicesWithRates[entity_id]
            @servicesWithRates[entity_id] = without(@servicesWithRates[entity_id], service_id)

          if callback
            callback()
        error: (data, textStatus, errorThrown) =>
            Utils.sendNotification("Failed to delete service rates", Consts.ERROR)

            @_handleError(data, textStatus, errorThrown, true)
    })

  getFleetServicesByPartner: (provider_id, callback) =>
    if !@servicesWithRates[provider_id] and !@servicesLoading[provider_id]
      @servicesLoading[provider_id] = true

      @rateApi.getServicesByPartnerWithRates(provider_id, {
        success: (data) =>
          @servicesLoading[provider_id] = false

          @servicesWithRates[provider_id] = map(data, (service) ->
            if service?
              ServiceStore._add(service)
              return service.id
            else
              return null
          )

          Logger.debug("book got back: ", @servicesWithRates[provider_id])

          if callback
            callback()
        error: (data, textStatus, errorThrown) =>
          Logger.debug("book got back: ", data, textStatus, errorThrown)

          Utils.sendNotification("Failed to get services with rates", Consts.ERROR)
          @_handleError(data, textStatus, errorThrown)
      })

    return @servicesWithRates[provider_id]

  getServiceIdsFromRates: (account_id, callback) =>
    if !@servicesWithRates[account_id] and !@servicesLoading[account_id]
      @servicesLoading[account_id] = true

      @rateApi.getServicesWithRates(account_id, {
          success: (data) =>
            Logger.debug("book got back: ", @servicesWithRates[account_id])

            @servicesLoading[account_id] = false
            @servicesWithRates[account_id] = map(data, (service) -> service.id)

            if callback
              callback()
          error: (data, textStatus, errorThrown) =>
              Logger.debug("book got back: ", data, textStatus, errorThrown)

              Utils.sendNotification("Failed to get services with rates", Consts.ERROR)
              @_handleError(data, textStatus, errorThrown)
      })

    if @servicesWithRates[account_id]
      services = uniq(map(@getRatesByObject({account_id: account_id}), (rate) -> rate.service_code_id))
      @servicesWithRates[account_id] = union(services, @servicesWithRates[account_id])

    return @servicesWithRates[account_id]

  addTempAccount: (account_id) ->
    @accountsWithRates.push(account_id)

  createAccountRates: (id, callback) ->
    @rateApi.createAccountRates(id, {
      success: (data) =>
        if !@accountsWithRates?
          @accountsWithRates = []
        @accountsWithRates.push(id)
        callback()
        #Should a change be triggered here?
      error: (data, textStatus, errorThrown) =>
        Utils.sendNotification("Failed to create account rates", Consts.ERROR)
        @_handleError(data, textStatus, errorThrown, true)
    })

  getPartnerServices: (partnerId, callback) =>
    Api.getPartnerServices(partnerId, {
      success: (data) =>
        services = pickBy(data, (value, key) ->
          return not value.deleted_at? and not value.addon
        )

        services = zipObject(map(services, (service) -> service.id), values(services))

        callback(services)
      error: (data, textStatus, errorThrown) =>
        Utils.sendNotification("Could not get partner services", Consts.ERROR)
        @_handleError(data, textStatus, errorThrown)
      })

  getPartnerAddons: (partner_id) =>
    if @addons[partner_id]
      return @addons[partner_id]
    else
      @rateApi.getProviderAddons(partner_id, {
        success: (data) =>
          @addons[partner_id] = data

          for rate in data
            @_add(rate)

          #@trigger(CHANGE+"_ADDONS")
          #Should a change be triggered here?
        error: (data, textStatus, errorThrown) =>
          Utils.sendNotification("Failed to get provider equipment", Consts.ERROR)
          handleError(data, textStatus, errorThrown)
        }
      )

      return null

  addRates: (requestObject) =>
    if requestObject.rates?.length > 0
      type = Api.POST

    if requestObject.rates[0].id?
      type = Api.UPDATE

    Api.objectsRequest(UserStore.getEndpoint()+"/rates", type, null, {
      success: (data) =>
        if requestObject.rates[0].id
          Utils.sendNotification("Rates were successfully edited", Consts.SUCCESS)
        else
          Utils.sendNotification("Rates were successfully added", Consts.SUCCESS)

        for rate in data
          if requestObject?.rescue_provider_id
            rate.provider_id = requestObject.rescue_provider_id
          @_add(rate, false)

        @trigger(@CHANGE)

      error: (data, textStatus, errorThrown) =>
        Utils.sendNotification("The rate failed to add", Consts.ERROR)
        @_handleError(data, textStatus, errorThrown)
      }, requestObject
    )

  addHourlyRates:(obj) =>
    Api.objectsRequest("partner/company", Api.POST, null, {
      success: (data) =>
        Utils.sendNotification("Hourly Rate Settings were successfully edited", Consts.SUCCESS)
        UserStore.setCompany(data)

        @trigger(@CHANGE)
      error: (data, textStatus, errorThrown) =>
        Utils.sendNotification("The rate failed to add", Consts.ERROR)

        @_handleError(data, textStatus, errorThrown)
      },
      company: obj
    )

  setMileageRules: (obj) =>
    Api.objectsRequest("partner/company", Api.POST, null, {
      success: (data) =>
        Utils.sendNotification("Mileage Rate Rules were successfully edited", Consts.SUCCESS)
        UserStore.setCompany(data)

        @trigger(@CHANGE)
      error: (data, textStatus, errorThrown) =>
        Utils.sendNotification("The Rules failed to add", Consts.ERROR)
        @_handleError(data, textStatus, errorThrown)
      },
      company: obj
    )

store = new RateStore()

window.rateStore = store

module.exports = store
