import Api from 'api'
SearchStore = require('stores/search_store')
UserStore = require('stores/user_store')
import Consts from 'consts'
import Utils from 'utils'
import { filter, get, map, values } from 'lodash'

class AccountStore extends SearchStore
  constructor: ->
    super({
      modelName: 'Account'
    })

    @LIST_MOTORCLUB = 'list_motorclub'
    @MOTORCLUB_IDS = null
    @LOADING_MOTORCLUB_IDS = false

    @LIST_ALL_WITHOUT_RATES = 'list_all_without_rates'
    @ALL_IDS_WITHOUT_RATES = null
    @LOADING_ALL_IDS_WITHOUT_RATES = false

  #TODO IMPLEMENT SHARED DATA:

  #onUpdate: (data) ->
    # copyCompanyDefaults(@account, data.id)
    #
    # if data and data.company and data.company.customer_types
    #   for ct in data.company.customer_types
    #     CustomerTypeStore._add(ct)

  shouldAddUserTypeToApiUrl: -> true

  hasPermissions: -> UserStore.isPartner()

  isEditable: -> UserStore.isPartner()

  loadCollectionOnUserChanged: -> false

  isLazyLoaded: -> true

  shouldSetCompanyDefaultsOnObjectLoad: -> true

  isTeslaAccount: (accountId, watcherId = null) ->
    @get(accountId, 'name', watcherId) == Consts.TESLA

  isTeslaMotorsAccount: (accountId, watcherId = null) ->
    @get(accountId, 'name', watcherId) == Consts.TESLA_MOTORS

  isTeslaAccountOrTeslaMotors: (accountId, watcherId = null) ->
    @isTeslaAccount(accountId, watcherId) || @isTeslaMotorsAccount(accountId, watcherId)

  getMotorClubCount: (watcherId = null) =>
    @getMotorClubAccounts(watcherId).length

  getMotorClubAccounts: (watcherId, property = null) =>
    if !UserStore.isPartner()
      return []

    if watcherId
      @_registerListListener(watcherId, @LIST_MOTORCLUB, property)

    if @MOTORCLUB_IDS == null
      @_loadMotorClubIds()
    else
      return map(@MOTORCLUB_IDS, (accountId) => this.getById(accountId))

    # GO THROUGH PROVIDERS AND FIND TIRE PROGRAM ONES
    accounts = values(@getAll())

    filter(accounts, (account) => @isMotorClub(account))

  getAllWithoutRatesForStore: (watcherId) =>
    if !UserStore.isPartner() || !watcherId
      return []

    if watcherId
      @_registerListListener(watcherId, @LIST_ALL_WITHOUT_RATES)

    if @ALL_IDS_WITHOUT_RATES == null
      @_loadAllIdsWithoutRates()
    else
      return @_getCollectionForStore(@LIST_ALL_WITHOUT_RATES)

    return []

  clearAllWithoutRatesList: =>
    @ALL_IDS_WITHOUT_RATES = null

  isMotorClubId: (id, watcherId) =>
    name = @get(id, 'name', watcherId)
    Consts.MOTORCLUBS.indexOf(name) >= 0

  #Depricated
  isMotorClub: (account) =>
    @isMotorClubId(account.id)

  isDisabledMotorClub: (account) =>
    account.company?.name in Consts.DISABLED_MOTORCLUBS or account.name in Consts.DISABLED_MOTORCLUBS

  doesAccountNameExist: (accountName, watcherId) ->
    accountName in map(values(@loadAndGetAll(watcherId, 'name')), 'name')

  getAccountNameByAccountId: (accountId, watcherId) ->
    if accountId
      accountCompanyName = @get(accountId, 'company', watcherId)?.name
      accountName = @get(accountId, 'name', watcherId)

      if accountCompanyName
        return accountCompanyName
      else if accountName
        return accountName

    return ""

  getName: (accountId, watcherId) ->
    if !accountId
      return ""

    accountName = @get(accountId, 'name', watcherId)

    if accountName
      return accountName

    accountCompany = @get(accountId, 'company', watcherId)

    if accountCompany && accountCompany.name
      return accountCompany.name

    return ""

  getAccountPrimaryEmail: (accountId, watcherId) ->
    if !accountId
      return ''

    company = @get(accountId, 'company', watcherId)

    if company?.primary_contact?.email?
      return company.primary_contact.email
    else
      return @get(accountId, 'primary_email', watcherId)

  getAccountKey: (accountId, key, watcherId) ->
    if !accountId
      return undefined

    company = @get(accountId, 'company', watcherId)

    if get(company, key)
      return get(company, key)
    else
      return @get(accountId, key, watcherId)

  addItem: (obj) ->
    callbackSuccess = null
    if obj.callbacks?.success?
      callbackSuccess = obj.callbacks.success

    if !obj.callbacks
      obj.callbacks = {}

    obj.callbacks.success = (data) =>
      callbackSuccess?(data)
      params = {
        category: "Onboarding"
        account: data.name
      }
      if @isMotorClubId(data.id)
        params.label = "Motorclub"


      Tracking.track 'Add Account', params

    super(obj)

  _loadMotorClubIds: =>
    if !UserStore.isPartner()
      return

    if !@LOADING_MOTORCLUB_IDS
      @LOADING_MOTORCLUB_IDS = true

      Api.getMotorClubAccounts(
        success: (data) =>
          for row in data
            @_add(row)

          @MOTORCLUB_IDS = map(data, 'id')

          @_triggerListListeners(@LIST_MOTORCLUB)

          @LOADING_MOTORCLUB_IDS = false

        error: (data, textStatus, errorThrown) =>
          @LOADING_MOTORCLUB_IDS = false
      )

  _loadAllIdsWithoutRates: =>
    if !UserStore.isPartner()
      return

    if !@LOADING_ALL_IDS_WITHOUT_RATES
      @LOADING_ALL_IDS_WITHOUT_RATES = true

      Api.getAccountsWithoutRate(
        success: (data) =>
          for row in data
            @_add(row)

          @ALL_IDS_WITHOUT_RATES = map(data, 'id')

          @_setupCollectionForStore(@LIST_ALL_WITHOUT_RATES, @ALL_IDS_WITHOUT_RATES)

          @_triggerListListeners(@LIST_ALL_WITHOUT_RATES)

          @LOADING_ALL_IDS_WITHOUT_RATES = false

        error: (data, textStatus, errorThrown) =>
          @LOADING_ALL_IDS_WITHOUT_RATES = false
      )

  _getListsObject: =>
    return [
      { list: @MOTORCLUB_IDS, validator: @isMotorClub, triggerName: @LIST_MOTORCLUB }
    ]

  getBatchUrl: =>
    UserStore.getEndpoint() + '/' + super(arguments...)

  isAgero: (account) ->
    if !account then return false

    name = if account.company?.name? then account.company?.name else account.name

    name == Consts.AGERO

  defaultLocationIdForAgero: -> 1

store = new AccountStore()

module.exports = store
