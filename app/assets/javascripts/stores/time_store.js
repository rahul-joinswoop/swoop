import BaseStore from 'stores/base_store' // TODO: SHOULD SET UP LAZY LOADING FOR PARTNERS

class TimeStore extends BaseStore {
  LIST_MINUTE = 'Minute'

  constructor() {
    super({
      modelName: 'Time',
      skipBinds: true,
    })
    setInterval(this.triggerMinute, 60 * 1000)
  }

  triggerMinute = () => this._triggerListListeners(this.LIST_MINUTE)

  watchMinute = watcherId => this._registerListListener(watcherId, this.LIST_MINUTE)
}

export default new TimeStore()
