import { sortBy, values } from 'lodash'
BaseStore = require('stores/base_store').default
AccountStore = require('stores/account_store')

class RejectReasonStore extends BaseStore
  constructor: ->
    super({
      modelName: 'RejectReason'
    })

  getUrl: -> "reject_reasons"

  getRejectReasonsForJob: (job, componentId, JobStore) =>
    options = []
    reasons = @getAll()
    account_name = JobStore.getAccountNameByJobId(job?.id, componentId)

    if job?.issc_dispatch_request?.system == "rsc"
      return @reasons_for_rsc_exclusive()

    for i, reason of reasons
      if parseInt(i) not in [16, 8, 5, 2, 6] and job?.issc_dispatch_request?.client_id in ["TSLA"]
        continue
      if parseInt(i) not in [2, 6, 7, 9] and job?.issc_dispatch_request?.client_id in ["ALLS", "USAC", "NSD"]
        continue
      if parseInt(i) not in [2, 6, 8, 11] and job?.issc_dispatch_request?.client_id == "QUEST"
        continue
      if parseInt(i) not in [2, 5, 6, 7, 8, 15, 16] and job?.issc_dispatch_request?.client_id == "ADS"
        continue
      if parseInt(i) not in [2, 12, 13, 6, 7, 14, 1, 8] and job?.issc_dispatch_request?.client_id == "RDAM"
        continue
      if parseInt(i) not in [9, 6, 2, 5] and job?.issc_dispatch_request?.client_id == "AGO"
        continue
      if parseInt(i) not in [2, 6, 7, 8] and account_name=="Geico" and job?.issc_dispatch_request?.client_id == "GCOAPI"
        continue
      if parseInt(i) not in [2, 6, 7, 9, 10] and account_name=="Geico" and job?.issc_dispatch_request?.client_id == "GCO"
        continue
      if parseInt(i) not in [1, 2, 3, 4, 5] and account_name=="Tesla"
        continue
      if parseInt(i) not in [2, 11, 13, 4, 5] and not job?.issc_dispatch_request? and account_name != "Tesla"
        continue
      options.push({
        id: i,
        name: reason.text
      })
    options

  reasons_for_rsc_exclusive: =>
    options = []

    all_values = values(@getAll())
    rsc_reasons = all_values.filter (reason) -> reason.id in [5, 17, 18, 19, 20]

    for rsc_reason in rsc_reasons
      options.push({
        id: rsc_reason.id,
        name: rsc_reason.text
      })

    sortBy(options, 'name')

store = new RejectReasonStore()

module.exports = store
