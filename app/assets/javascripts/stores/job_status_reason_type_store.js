import { filter, isEmpty, keys } from 'lodash'
import BaseStore from 'stores/base_store'

class JobStatusReasonTypeStore extends BaseStore {
  constructor() {
    super({
      modelName: 'JobStatusReasonType',
    })

    this.cacheByJobStatusName = {}
  }

  getUrl() {
    return 'job_status_reason_types'
  }

  loadCollectionOnUserChanged() {
    return false
  }

  // jobStatusName - JobStatus human name, like 'Canceled' or 'GOA'
  getByJobStatusName(jobStatusName, watcherId) {
    if (!jobStatusName) {
      return []
    }

    if (this.cacheByJobStatusName[jobStatusName]) {
      return this.cacheByJobStatusName[jobStatusName]
    }

    const all = this.loadAndGetAll(watcherId)

    if (isEmpty(keys(all))) {
      return []
    }

    // we use a simple cache in this case, as we don't expect these data to be changed
    this.cacheByJobStatusName[jobStatusName] = filter(all, jobStatusReason => jobStatusReason.job_status_name === jobStatusName)

    return this.cacheByJobStatusName[jobStatusName]
  }
}

const store = new JobStatusReasonTypeStore()
window.jobStatusReasonTypeStore = store

export default store
