import BaseStore from 'stores/base_store'

// POI stands for 'Place of Interest', aka Place
class PoiStore extends BaseStore {
  constructor() {
    super({
      modelName: 'Place',
    })
  }

  isLazyLoaded() {
    return true
  }

  loadCollectionOnUserChanged() {
    return false
  }

  isEditable() {
    return true
  }

  getUrl() {
    return 'poi_sites'
  }

  getEditUrl() {
    return 'poi_sites'
  }

  _getJSONName() {
    return 'poiSite'
  }
}

const store = new PoiStore()

export default store
