import Logger from 'lib/swoop/logger'
MicroEvent = require('microevent-github')
import Api from 'api'
import Dispatcher from 'lib/swoop/dispatcher'
import { sortBy, values } from 'lodash'

triggers =
    NEW_NOTIFICATION : "new_notification"
    CLOSE_NOTIFICATION: "close_notifcation"
    CHANGE: "change"

window[key] = val for key, val of triggers

NotificationStore = (->
    notifications={}
    id = 1
    store =
        getNotifications: =>
          notifications
        getMostRecentNotification: =>
          sorted = sortBy(values(notifications), "id")
          return sorted[sorted.length-1]

        removeNotification: (checkText) =>
          for k, v of notifications
            if v?.message? and typeof(v.message) is "string" and v.message.indexOf(checkText) > -1
              delete notifications[k]
          trigger(CHANGE)

    store[key] = val for key, val of triggers
    MicroEvent.mixin( store )
    trigger = store.trigger.bind(store)

    setupDrivers = =>
        Api.drivers({
                success: (data) =>
                    for driver in data
                        @drivers[driver.id]=driver
                    trigger(CHANGE)
                 error: (data) =>
                    Logger.debug("Got drivers error",data)}
        )

    @[NEW_NOTIFICATION] = (notification) =>
      #Websocket for drivers added to the systems
      if not notification.id?
        notification.id = id++
      else
        #Already displaying this notification
        if notifications[notification.id]?
          return


      if notification.timeout? && notification.timeout > 0
        ((notification) =>
          setTimeout (=>
            @[CLOSE_NOTIFICATION](notification)
          ), notification.timeout
        )(notification)

      notifications[notification.id] = notification
      trigger(CHANGE)

    @[CLOSE_NOTIFICATION] = (notification) =>
      #Websocket for drivers added to the systems
      if notifications[notification.id]?
        delete notifications[notification.id]

      trigger(CHANGE)


    Dispatcher.register(( payload ) =>
       if not payload.eventName
         console.warn('empty Event in payload: ', payload)
       if typeof @[payload.eventName] is 'function'
           @[payload.eventName](payload.data)
       else
         Logger.debug('unknown event received in VehicleStore',payload)
       true
    )


    return store

).bind({})()


module.exports = NotificationStore
