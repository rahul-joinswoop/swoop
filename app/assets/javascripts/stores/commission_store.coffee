import Api from 'api'
BaseStore = require('stores/base_store').default
CompanyStore = require('stores/company_store').default
import { map } from 'lodash'

class CommissionStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Commission'
      stores: {
        CompanyStore: CompanyStore
      }
    })

  getEditUrl: -> @getUrl()

  shouldAddUserTypeToApiUrl: -> true

  hasPermissions: ->
    UserStore.isPartner()

  loadCollectionOnUserChanged: -> false

  companyContainsCommissionExclusionService: (id, watcherId) =>
    @companyContains(id, watcherId, "commission_exclusions_services_ids")

  companyContainsCommissionExclusionAddon: (id, watcherId) =>
    @companyContains(id, watcherId, "commission_exclusions_addons_ids")

  updateAddonCommissionsExclusions: (data, callback) =>
    commissionExclusionsObject = @formatCommissionsExclusionsObject(data)

    Api.updateAddonCommissionsExclusions(commissionExclusionsObject, callback)

  updateServiceCommissionsExclusions: (data, callback) =>
    commissionExclusionsObject = @formatCommissionsExclusionsObject(data)

    Api.updateServiceCommissionsExclusions(commissionExclusionsObject, callback)

  formatCommissionsExclusionsObject: (data) ->
    commissionExclusionsToAdd = map(data['add'], (service_code_id) =>
      { service_code_id: service_code_id }
    )

    commissionExclusionsToRemove = map(data['remove'], (service_code_id) =>
      { service_code_id: service_code_id }
    )

    commissionExclusionsObject = { commission_exclusions:
      {
        change: {}
      }
    }

    if commissionExclusionsToAdd.length > 0
      commissionExclusionsObject.commission_exclusions.change['add'] = commissionExclusionsToAdd

    if commissionExclusionsToRemove.length > 0
      commissionExclusionsObject.commission_exclusions.change['remove'] = commissionExclusionsToRemove

    return commissionExclusionsObject

store = new CommissionStore()

module.exports = store
