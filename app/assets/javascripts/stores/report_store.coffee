BaseStore = require('stores/base_store').default

class ReportStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Report'
    })

  shouldAddUserTypeToApiUrl: -> true
  isEditable: -> true
  hasPermissions: ->
    if this.stores.UserStore.isSwoop()
      return this.stores.UserStore.isRoot()
    return true

  loadCollectionOnUserChanged: ->
    false

store = new ReportStore()

module.exports = store
