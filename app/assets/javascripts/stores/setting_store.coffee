BaseStore = require('stores/base_store').default
import Api from 'api'
import { find } from 'lodash'

class SettingStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Setting'
    })

  isEditable: -> true

  getDeleteHTTPMethod: -> Api.DELETE

  showNotification: -> false

  getSettingByKey: (key, parseAsJson = false) =>
    #TODO: make this a hash lookup
    settingsByKey = find(@getAll(), (setting) ->
      setting.key == key
    )

    if parseAsJson && settingsByKey?
      JSON.parse(settingsByKey.value)
    else
      settingsByKey

  getJobTabs: (watcher=null) =>
    #TODO: set up watcher
    tabs = []
    if @getSettingByKey("Drafts Tab")?
      tabs.push("drafts")

    if @getSettingByKey("Scheduled Tab")?
      tabs.push("scheduled")

    if @getSettingByKey("Pending Tab")?
      tabs.push("pending")
      tabs.push("in-progress")
    else
      tab.push("active")


store = new SettingStore()

module.exports = store
