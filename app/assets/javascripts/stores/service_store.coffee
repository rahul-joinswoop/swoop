import Api from 'api'
BaseStore = require('stores/base_store').default
CompanyStore = require('stores/company_store').default
import Utils from 'utils'
import EventRegistry from 'EventRegistry'
import { find, map, zipObject } from 'lodash'

class ServiceStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Service'
      stores: {
        CompanyStore: CompanyStore
      }
    })
    @_loadingNames = {
      true: {}
      false: {}
    }

  getServiceByName: (name, watcherId) =>
    @_getByName(name, false, watcherId)

  getAddonByName: (name, watcherId) =>
    @_getByName(name, true, watcherId)

  loadCollectionOnUserChanged: -> return false

  isLazyLoaded: -> return true

  getServices: (watcherId, filter_company_id=null) =>
    return this.getCompanyItems(watcherId, "service_ids", filter_company_id)

  getAddons: (watcherId, filter_company_id=null) =>
    return this.getCompanyItems(watcherId, "addon_ids", filter_company_id)

  getServicesForStore: (watcherId, filter_company_id=null) =>
    services = @getServices(watcherId, filter_company_id)

    return zipObject(map(services, (service) -> service.id), services)

  companyContains: (id, watcherId) ->
    super(id, watcherId, "service_ids") || super(id, watcherId, "addon_ids")

  #TODO: cache this and break it when company changes
  isStorageEnabled: (watcherId) ->
    for service in @getServices(watcherId)
      if service.name == "Storage"
        return true
    return false

  _getByName: (name, addon, watcherId) =>
    if name?
      if @_loadingNames[addon][name]?
        if Utils.isNumeric(@_loadingNames[addon][name])
          return @getById(@_loadingNames[addon][name])
        else
          return

      @_loadingNames[addon][name] = false

      service = find(@_getInternalCollection(), (value, key) ->
        return value.name == name and value.addon != true
      )

      if service
        @_loadingNames[addon][name] = service.id
        return service
      else
        @_loadByName(name, false, watcherId)

  _loadByName: (name, addon, watcherId) =>
    console.log("Loading", name, addon)
    Api.loadServiceByName(name, addon,
      success: (response) =>
        for service in response
          @_add(service, false)
          @_loadingNames[service.addon][service.name] = service.id
        EventRegistry.trigger(watcherId)
      error: (error) ->
        console.error("Unable to find service: ", name, addon)
    )

store = new ServiceStore()

module.exports = store
