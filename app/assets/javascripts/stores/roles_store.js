import BaseStore from 'stores/base_store'

class RolesStore extends BaseStore {
  constructor() {
    super({
      modelName: 'Role',
    })
  }
}

export default new RolesStore()
