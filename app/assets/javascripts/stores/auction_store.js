

import Logger from 'lib/swoop/logger'
import MicroEvent from 'microevent-github'
import Dispatcher from 'lib/swoop/dispatcher'
import Api from 'api'
import NotificationStore from 'stores/notification_store'
import UserStore from 'stores/user_store'
import { assign, extend, find, map, min, some, zipObject } from 'lodash'
import Consts from 'consts'

const triggers = {
  DONE_LOADING: 'auctionsDoneLoading',
  REMOTE_AUCTION_ADDED: 'remoteAuctionAdded',
  REMOTE_AUCTION_UPDATED: 'remoteAuctionUpdated',
  REMOTE_BID_UPDATED: 'remoteBidUpdated',

  BID_TO_ETA_SUBMITTED_BY_PARTNER: 'bidToETASubmittedByPartner',
  BID_TO_ETA_REJECTED_BY_PARTNER: 'bidToETARejectedByPartner',

  PARTNER_LOST_AUCTION: 'partnerLostAuction',
  AUCTION_RAN_OUT: 'auctionRanOut',
  BID_UPDATED_ON_CLIENT: 'bidUpdatedOnClient',
  TARGET_ETA_UPDATED: 'targetETAUpdated',

  PARTNER_BID_REJECTED: 'partnerBidRejected',
  PARTNER_BID_ACCEPTED: 'partnerBidAccepted',
  PARTNER_EXPIRED_AUCTION: 'partnerExpiredAuction',
}

class AuctionApi {
  static endpointBase() {
    return `${UserStore.getEndpoint()}/auctions`
  }

  static fetchFromServer(response) {
    Api.request(Api.GET, `${this.endpointBase()}`, null, response)
  }

  static postBidToETA(bidToETAData, response) {
    Api.request(Api.POST, `partner/jobs/${bidToETAData.job_id}/bids`, { minutes: bidToETAData.minutes }, response)
  }

  static postBidRejected(rejectData, response) {
    Api.request(
      Api.POST, `partner/jobs/${rejectData.id}/bid/reject`,
      {
        reject_reason_id: rejectData.reject_reason_id,
        reject_reason_info: rejectData.reject_reason_info,
      }, response
    )
  }
}

function AuctionStore() {
  let auctions = {}

  this[triggers.DONE_LOADING] = () => {
    Logger.debug('auctions ready')
  }

  this[triggers.REMOTE_AUCTION_ADDED] = (remoteAuctionData) => {
    Logger.debug(`remote auction added ${remoteAuctionData.target.id}`)

    auctions[remoteAuctionData.target.id] = remoteAuctionData.target
  }

  /*
  /  This updates the Auction locally.
  /
  / If auction is no LIVE anymore, it means the auction has ran out! In this case we call auctionRanOut
  / to manage local notifications.
  */
  this[triggers.REMOTE_AUCTION_UPDATED] = (remoteAuctionData) => {
    Logger.debug(`remote auction updated ${remoteAuctionData.target.id}`)

    let previousStatus = ''
    let previousTargetETA = ''

    if (auctions[remoteAuctionData.target.id]) {
      previousStatus = auctions[remoteAuctionData.target.id].status
      previousTargetETA = auctions[remoteAuctionData.target.id].target_eta
    }

    auctions[remoteAuctionData.target.id] = remoteAuctionData.target

    // If auction is no LIVE anymore, it means the auction has ran out!
    if (remoteAuctionData && remoteAuctionData.target.status != Consts.AUCTION_LIVE && remoteAuctionData.target.status != previousStatus) {
      Logger.debug(`remote auction ran out ${remoteAuctionData.target.id}`)
      auctionRanOut(remoteAuctionData.target.id)
    } else if (previousTargetETA != remoteAuctionData.target.target_eta) {
      this.trigger(triggers.TARGET_ETA_UPDATED)
    }
  }

  this[triggers.REMOTE_BID_UPDATED] = (remoteBidData) => {
    Logger.debug(`remote bid updated ${remoteBidData.target.id}`)

    const remoteBid = remoteBidData.target

    const auction = this.getAuctionById(remoteBid.auction_id)

    if (!auction) {
      return
    }

    // updates local bid
    let bid = this.getBidById(remoteBid.id)

    if (bid) {
      extend(bid, remoteBid)
    } else {
      bid = remoteBid
      auction.bids.push(bid)
      Logger.debug(`Another partner has changed the Bid id ${remoteBid.id}`)
    }

    // updates local auction.target_eta in case 'auction.target_eta > remoteBid.submitted_eta'
    if (auction && remoteBid.submitted_eta && auction.target_eta > remoteBid.submitted_eta) {
      auction.target_eta = remoteBid.submitted_eta
    }

    if (this.isBidInRunningStatus(bid)) {
      this.trigger(`${triggers.BID_UPDATED_ON_CLIENT}_${bid.id}`, auction)
      this.trigger(triggers.BID_UPDATED_ON_CLIENT)
    }

    if (auction.status === Consts.AUCTION_LIVE && this.hasPartnerRejectedJob({ id: auction.job_id })) {
      NotificationStore.removeNotification(`#${auction.job_id} is available for ETA submissions`)
      this.trigger(triggers.PARTNER_BID_REJECTED, auction)
    }
  }

  this[triggers.BID_TO_ETA_SUBMITTED_BY_PARTNER] = (bidToETAData) => {
    AuctionApi.postBidToETA(bidToETAData, {
      success: (data) => {
        sendNotification(`Job #${bidToETAData.job_id} ETA has been submitted.`, Consts.SUCCESS, 2000)
        Logger.debug('bid submitted with success')
      },
      error: (jqXHR, textStatus, errorThrown) => {
        if (/2\d\d$/.test(jqXHR.status)) {
          sendNotification(`Job #${bidToETAData.job_id} ETA has been submitted.`, Consts.SUCCESS)

          return
        }

        sendNotification(`Job #${bidToETAData.job_id} submit ETA has failed. Please try again.`, Consts.ERROR)

        if (!errorThrown) {
          errorThrown = ''
        }
        Logger.warn('Error in postBidToETA:', errorThrown)
      },
    })
  }

  this[triggers.BID_TO_ETA_REJECTED_BY_PARTNER] = (bidToETAData) => {
    const auction = this.getAuctionByJob({ id: bidToETAData.id })
    if (auction) {
      const bid = this.getBidByPartnerId(auction.id, UserStore.getCompany().id)
      bid.status = Consts.BID_PROVIDER_REJECTED.STATUS
    }
    AuctionApi.postBidRejected(bidToETAData, {
      success: (data) => {
        // TODO: copy data into auction bid
        sendNotification(`Job #${bidToETAData.id} has been rejected.`, Consts.SUCCESS)

        Logger.debug('bid rejected with success')
      },
      error: (jqXHR, textStatus, errorThrown) => {
        if (/2\d\d$/.test(jqXHR.status)) {
          sendNotification(`Job #${bidToETAData.id} has been rejected.`, Consts.SUCCESS)
          return
        }
        if (!errorThrown) {
          errorThrown = ''
        }
        sendNotification(`Job #${bidToETAData.id} reject has failed. Please try again.`, Consts.ERROR)
        Logger.warn('Error in postBidToETA:', errorThrown)
      },
    })
  }

  this.getAuctions = () => {
    Logger.debug('getAuctions called')

    return auctions
  }

  this.getAuctionById = (id) => {
    Logger.debug(`getAuctionById called with id ${id}`)

    if (!id) return null

    return this.getAuctions()[id]
  }

  this.getBidById = (bidId) => {
    Logger.debug(`getAuctionByBidId called with bid id ${bidId}`)

    if (!bidId) return null

    for (const auctionId in this.getAuctions()) {
      const auction = this.getAuctionById(auctionId)
      const bid = auction.bids.find(bid => bid.id == bidId)

      if (bid) return bid
    }

    Logger.debug('getAuctionByBidId - bid not found')

    return null
  }

  this.getAuctionByJob = (job) => {
    if (!job || !job.id) {
      return null
    }

    Logger.debug(`getAuctionByJob called with job id ${job.id}`)

    let auction = find(this.getAuctions(), auction => auction && auction.job_id == job.id)

    // job comes with auction nested. *if for any reason* this store doesn't have the auction loaded,
    // we guarattee the one that comes with the job will be added and returned.
    if (!auction && job.auction) {
      Logger.debug(`getAuctionByJob will add the job (id ${job.id}) auction id ${job.auction.id}`)

      auctions[job.auction.id] = auction = job.auction
    }

    if (this.isAutoAssignToTruckEnabled(auction)) {
      return null
    }

    return auction
  }

  // Checks if the job has an auction with LIVE status.
  // Means that partners can still send their ETAs.
  this.jobHasLiveAuction = (job) => {
    const auction = this.getAuctionByJob(job)

    if (!auction) {
      return false
    }

    return auction.status === Consts.AUCTION_LIVE
  }

  this.isBidInRunningStatus = bid => bid.status == Consts.BID_NEW.STATUS ||
      bid.status == Consts.BID_SUBMITTED.STATUS ||
      bid.status == Consts.BID_REQUESTED.STATUS ||
      bid.status == Consts.BID_PROVIDER_REJECTED.STATUS

  this.jobFinishedAndLost = job => (this.jobHasFinishedAuction(job) && (!job.rescue_company || job.rescue_company.id !== UserStore.getCompany().id))

  this.jobShouldBeHiddenFromPartner = job => (job.status == Consts.AUTO_ASSIGNING && this.hasPartnerRejectedJob(job)) || this.jobFinishedAndLost(job)

  this.jobHasFinishedAuction = (job) => {
    const auction = this.getAuctionByJob(job)

    if (!auction) {
      return false
    }

    return auction.status != Consts.AUCTION_LIVE
  }

  // Checks if the job has an auction with UNSUCCESSFUL status.
  // Means that the Auction has finished without winners.
  // In this case, Swoop dispatcher will manually select a Partner.
  this.jobHasUnsuccessfulAuction = (job) => {
    const auction = this.getAuctionByJob(job)

    if (!auction) {
      return false
    }

    return auction.status == Consts.AUCTION_UNSUCCESSFUL
  }

  // Checks if the job has an auction.
  // Means that job is/was under a bidding process for reaching a BestETA.
  this.jobHasAuction = job => this.getAuctionByJob(job)

  this.getTargetETAByJob = (job) => {
    const auction = this.getAuctionByJob(job)

    if (!auction) {
      return null
    }

    return auction.target_eta
  }

  this.getExpiresAtByJob = (job) => {
    const auction = this.getAuctionByJob(job)

    if (!auction) {
      return null
    }

    return auction.expires_at
  }

  this.formattedBidStatus = (bid) => {
    const bidStatus = Consts.ALL_BID_STATUSES.find(bidStatus => bidStatus.STATUS === bid.status)

    if (bidStatus) {
      return bidStatus.FORMATTED
    }

    return '--'
  }

  this.getFormattedSubmittedETA = (partnerProvider, job) => {
    if (!partnerProvider || !job) {
      return '--'
    }

    const auction = this.getAuctionByJob(job)
    let bid = null
    if (partnerProvider && partnerProvider.provider_id) {
      bid = auction.bids.find(bid => bid.company_id === partnerProvider.provider_id)
    }

    // return the submitted_eta if it exists or show the current status otherwise
    if (!bid) {
      return '--'
    }

    if (bid.submitted_eta) {
      return `${bid.submitted_eta} mins`
    }
    return this.formattedBidStatus(bid)
  }

  this.getSiteETA = (partnerProvider, job) => {
    // cleanup w/ ENG-9792
    const candidates = job.candidates || job.auto_assign_candidates

    if (!job || !candidates || !partnerProvider) return null

    const candidateETA = candidates.find(candidate => Consts.SITE_CANDIDATE.has(candidate.type) && candidate.site.id === partnerProvider.site_id)

    if (candidateETA === null || candidateETA === undefined) return null

    return candidateETA.eta
  }

  this.getTruckETA = (partnerProvider, job) => {
    // cleanup w/ ENG-9792
    const candidates = job.candidates || job.auto_assign_candidates

    if (!job || !candidates || !partnerProvider || !partnerProvider.provider_id) return null

    const truckCandidates = candidates.filter(candidate => Consts.TRUCK_CANDIDATE.has(candidate.type) && candidate.rescue_company_id === partnerProvider.provider_id)

    if (truckCandidates.length === 0) return null

    return min(map(truckCandidates, truckCandidate => truckCandidate.eta))
  }

  // If the user is ready, it's time to fetch the Auctions from the server!
  this.userIsReady = () => {
    if (UserStore.isSwoop() || UserStore.isPartner()) {
      Logger.debug('AuctionStore has received a userChanged trigger')

      AuctionApi.fetchFromServer({
        success: (auctionsFromServer) => {
          Logger.debug('auctions loaded from server')

          auctions = zipObject(
            map(auctionsFromServer, auction => auction.id),
            auctionsFromServer
          )

          this.trigger(triggers.DONE_LOADING)
        },
        error: (error) => {
          Logger.error(`error while loading auctions from server ${error}`)
        },
      })
    }
  }

  this.hasPartnerSentBidToJob = (job) => {
    if (!job || !job.id) {
      return undefined
    }

    const auction = this.getAuctionByJob(job)

    if (!auction) {
      return undefined
    }

    const user = UserStore.getUser()
    if (!user || !user.company) {
      return undefined
    }

    return find(auction.bids, bid => bid.company_id === user.company.id && (bid.status === Consts.BID_SUBMITTED.STATUS || bid.submitted_eta))
  }

  this.hasPartnerRejectedJob = (job) => {
    if (!job || !job.id) {
      return undefined
    }

    const auction = this.getAuctionByJob(job)

    if (!auction || !auction.bids) {
      return false
    }

    const user = UserStore.getUser()
    if (!user || !user.company) {
      return undefined
    }

    const rejected = some(auction.bids, bid => bid.company_id === user.company.id && bid.status === Consts.BID_PROVIDER_REJECTED.STATUS)
    return rejected
  }


  this.hasPartnerRejectedBidToAuction = (auction) => {
    if (!auction || !auction.bids) {
      return false
    }

    const user = UserStore.getUser()
    if (!user || !user.company) {
      return false
    }

    return auction.bids.find(bid => bid.company_id === user.company.id && bid.status === Consts.BID_PROVIDER_REJECTED.STATUS)
  }

  this.getBidByPartnerId = (auctionId, partnerId) => {
    if (!auctionId || !partnerId) {
      return null
    }

    const auction = this.getAuctionById(auctionId)

    if (auction) {
      return auction.bids.find(bid => bid.company_id === partnerId)
    }

    return null
  }

  this.getWonBid = (auctionId) => {
    const auction = this.getAuctionById(auctionId)

    const wonBid = find(auction.bids, bid => bid.status === Consts.BID_WON.STATUS)

    return wonBid?.submitted_eta
  }

  // This checks if the Auction was created with Vehicle candidate to be auto assigned to.
  // In the backend it can happen when auction.duration_secs == 0.
  // Some specific treatment is then needed in this case, like not showing
  // some of the common Auction notifications.
  this.isAutoAssignToTruckEnabled = (auction) => {
    if (!auction || auction.duration_secs == 0) {
      return true
    }

    return false
  }

  // HELPER FUNCTIONS

  const sendNotification = (message, type, timeout = 0) => {
    const notification = {
      type: (type || Consts.INFO),
      message,
      dismissable: true,
      timeout,
    }

    setTimeout(() => Dispatcher.send(NotificationStore.NEW_NOTIFICATION, notification), 0)
  }

  /** *
    This treats the auction ran out event.
    if partner logged in, it will display notification accordingly and triggers PARTNER_LOST_AUCTION when needed.
    if swoop logged in, it will display notification accordingly.
  ** */

  const auctionRanOut = (auctionId) => {
    const auction = this.getAuctionById(auctionId)
    if (!auction) {
      return
    }
    const user = UserStore.getUser()
    if (!user || !user.company) {
      return
    }
    let mobileTriggered = false
    const lostAuction = () => {
      console.log('Partner lost auction')
      sendNotification(`Job #${auction.job_id} was not accepted by Swoop`)
      this.trigger(triggers.PARTNER_BID_REJECTED, auction)
      mobileTriggered = true
    }

    const wonAuction = () => {
      console.log('Partner won auction')
      sendNotification(`Job #${auction.job_id} ETA has been accepted by Swoop`)
      this.trigger(triggers.PARTNER_BID_ACCEPTED, auction)
      mobileTriggered = true
    }

    if (auction.status == Consts.AUCTION_SUCCESSFUL) {
      if (UserStore.isPartner()) {
        NotificationStore.removeNotification(`#${auction.job_id} is available for ETA submissions`)

        const partnerBid = this.getBidByPartnerId(auction.id, user.company.id)
        if (partnerBid) {
          if (partnerBid.status == Consts.BID_WON.STATUS) {
            wonAuction()
          } else {
            if (partnerBid.status == Consts.BID_AUTO_REJECTED.STATUS) {
              lostAuction()
            } else if (partnerBid.status == Consts.BID_EXPIRED.STATUS && !auction.manual_dttm && !this.isAutoAssignToTruckEnabled(auction)) {
              sendNotification(`Job #${auction.job_id} has expired`)
            }

            this.trigger(triggers.PARTNER_LOST_AUCTION, auction)
          }
        }
      } else if (UserStore.isSwoop()) {
        const winnerBid = auction.bids.find(bid => bid.status == Consts.BID_WON.STATUS)

        let company_name = 'a partner'
        if (winnerBid && winnerBid.company_name) {
          company_name = winnerBid.company_name
        }

        const winnerEta = this.getWonBid(auction.id)
        if (!winnerEta) {
          Rollbar.error('Won auction is missing winner', { auction_id: auction.id, winnerBid, winnerCompany: company_name })
          sendNotification(`Job #${auction.job_id} assigned to ${company_name}`)
        } else {
          sendNotification(`Job #${auction.job_id} assigned to ${company_name} with an ETA of ${this.getWonBid(auction.id)} mins`)
        }
      }
    } else if (auction.status == Consts.AUCTION_UNSUCCESSFUL) {
      if (UserStore.isPartner()) {
        NotificationStore.removeNotification(`#${auction.job_id} is available for ETA submissions`)

        const partnerBid = this.getBidByPartnerId(auction.id, user.company.id)

        if (auction.manual_dttm) {
          if (partnerBid.status == Consts.BID_AUTO_REJECTED.STATUS) {
            lostAuction()
          }
        } else if (partnerBid.status == Consts.BID_AUTO_REJECTED.STATUS) {
          lostAuction()
        } else if (partnerBid.status == Consts.BID_EXPIRED.STATUS && !this.isAutoAssignToTruckEnabled(auction)) {
          sendNotification(`Job #${auction.job_id} has expired`)
        }

        this.trigger(triggers.PARTNER_LOST_AUCTION, auction)
      } else if (UserStore.isSwoop() && !this.isAutoAssignToTruckEnabled(auction)) {
        if (!auction.manual_dttm) { // if manual_dttm, it means that Swoop has assigned a Partner, so no fail message is displayed.
          sendNotification(`Job #${auction.job_id} auction failed, please check if manual dispatch is needed`)
        }
      }
    } else if (auction.status == Consts.AUCTION_CANCELED && UserStore.isPartner()) {
      NotificationStore.removeNotification(`#${auction.job_id} is available for ETA submissions`)
      sendNotification(`Swoop has Canceled Job #${auction.job_id}`)

      // delete auctions[auction.id];
      this.trigger(triggers.PARTNER_LOST_AUCTION, auction)
    }

    this.trigger(triggers.AUCTION_RAN_OUT, auction)
    this.trigger(`${triggers.AUCTION_RAN_OUT}_${auction.id}`, auction)

    if (!mobileTriggered) {
      this.trigger(triggers.PARTNER_EXPIRED_AUCTION, auction)
    }
  }

  // UserStore.LOGIN bind to start loading the auctions.
  UserStore.bind(UserStore.LOGIN, this.userIsReady)
}

MicroEvent.mixin(AuctionStore)

const store = new AuctionStore()
assign(store, triggers)

Dispatcher.register((payload) => {
  if (!payload.eventName) {
    console.warn('Empty event', payload)
    // throw new Error('empty Event');
  }

  if (typeof store[payload.eventName] === 'function') {
    store[payload.eventName](payload.data)
  } else {
    Logger.debug('unknown event received in AuctionStoreStore', payload)
  }

  return true
})

// TODO BestETA - this is for testing purposes only, should be removed
window.auctionStore = store

export default store
