UserStore = require('stores/user_store')
import Api from 'api'
import Utils from 'utils'
import Dispatcher from 'lib/swoop/dispatcher'
MicroEvent = require('microevent-github')
import Consts from 'consts'

triggers =
  CHANGED_REPORT_RESULT : "change_report_result"
  DOWNLOADING_REPORT : "downloading_report"

DownloadStore = (->
  report_runs = {}
  last_result_id = {}
  named_results = {}
  report_requests = []
  store =

    reset: ->
      report_runs = {}
      last_result_id = {}
      named_results = {}
      report_requests = []

    knownReport: (result) ->
      result.id in report_requests

    stateChanged: (result) ->
      store.getCurrentReportResult(result)?.state != result.state
    addReportResult: (result, silent=false) ->
      #Prevent getting updates from others,
      #and optimization to not do anything if nothings changed
      if !store.knownReport(result) || !store.stateChanged(result)
        return false
      if !silent
        store.downloadIfNeeded(result)
      if not report_runs[result.report_id]?
        report_runs[result.report_id] = {}
      report_runs[result.report_id]?[result.id] = result
      if !silent
        trigger(triggers.CHANGED_REPORT_RESULT)
      return true

    getLastReportStateByName: (name) ->
      if named_results[name] == "error"
        return "error"
      if named_results[name] == true
        return "Loading"
      if named_results[name]?
        report_id = named_results[name].report_id
        result_id = named_results[name].result_id
        return report_runs[report_id]?[result_id]?.state

    getLastReportId: (id) ->
      last_result_id[id]

    getCurrentReportResult: (result) ->
      report_runs[result.report_id]?[result.id]

    getLastReportState: (id) ->
      if last_result_id[id] == "error"
        return "error"
      if last_result_id[id] == true
        return "Loading"
      if report_runs[id]?[last_result_id[id]]?.empty and report_runs[id]?[last_result_id[id]]?.state == "Finished"
        return "Empty"

      report_runs[id]?[last_result_id[id]]?.state

    downloadIfNeeded: (new_result) ->
      old_report_state = store.getCurrentReportResult(new_result)?.state
      if new_result.state == "Finished" and old_report_state != "Finished" and !new_result.empty
        trigger(triggers.DOWNLOADING_REPORT)
        Utils.downloadFile(new_result.s3_filename)

    fetchReportResult: (id, callback) ->
      url = UserStore.getEndpoint()+"/report_results"
      Api.objectsRequest(url, Api.GET, id, {
        success: (data) ->
          store.addReportResult(data)
          callback?(data)
        error: (data, textStatus, errorThrown) ->
          msg = "Report Unavailable Please Refresh"
          Utils.sendNotification(msg, Consts.ERROR)
          Utils.handleError(data, textStatus, errorThrown, true)
      })

    pollForUpdates: (report_id, result_id) ->
      interval = setInterval(->
        if report_runs[report_id]?[result_id]?
          store.fetchReportResult(result_id, (data) ->
            if data.state in ["Failed", "Finished"]
              clearInterval(interval)
          )
        else
          clearInterval(interval)
      , 60000)


    downloadReport: (id, filters, format, apiCall=Api.runReport, name=null) ->
      if not report_runs[id]?
        report_runs[id] = {}

      if !name?
        name = id

      if id?
        last_result_id[id] = true
        named_results[name] = true
        trigger(triggers.CHANGED_REPORT_RESULT)

      apiCall(UserStore.getEndpoint(), id, filters, format, {
        success: (data, f) ->
          if !id?
            id = data.report_id

          last_result_id[id] = data.id
          named_results[name] = {report_id: data.report_id, result_id: data.id}
          report_requests.push(data.id)
          store.addReportResult(data)
          store.pollForUpdates(id, data.id)
        error: () ->
          last_result_id[id] = "error"
          named_results[name] = "error"
      })


  store[key] = val for key, val of triggers
  MicroEvent.mixin( store )
  trigger = store.trigger.bind(store)

  return store

).bind({})()

module.exports = DownloadStore
