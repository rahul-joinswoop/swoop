import Utils from 'utils'
import Logger from 'lib/swoop/logger'
import MicroEvent from 'microevent-github'
import Dispatcher from 'lib/swoop/dispatcher'
import UserStore from 'stores/user_store'
import Api from 'api'
import { assign, uniq, without } from 'lodash'

const triggers = {
  REASONS_LOADED: 'reasonsLoaded',
  REJECT_INVOICE: 'rejectInvoice',
  REJECTED_INVOICE: 'rejectedInvoice',
  INVOICE_REJECTION_LOADED: 'invoiceRejectionLoaded',
}

function InvoiceRejectionStore() {
  this.invoiceRejections = {}
  this.reasons = []
  this.invoiceRejectionsToLoad = []

  const addInvoiceRejection = (invoiceRejection) => {
    const invoiceId = invoiceRejection.invoice_id

    this.invoiceRejections[invoiceId] = invoiceRejection
    this.invoiceRejectionsToLoad = without(this.invoiceRejectionsToLoad, invoiceId)
  }

  // Used to delay loading an InvoiceRejection by id and loading multiple ones
  // with one query.
  let timeoutLoading

  const fetchFromServer = () => {
    const success = (invoiceRejections) => {
      invoiceRejections.map((invoiceRejection) => {
        addInvoiceRejection(invoiceRejection)
        this.trigger(triggers.INVOICE_REJECTION_LOADED, invoiceRejection)
      })
    }

    const response = {
      success,
      error(data) {
        Logger.debug('GET invoice rejection error', data)
      },
    }

    Api.invoiceRejectionsListRequest(
      Api.GET,
      Utils.getEndpoint(UserStore),
      uniq(this.invoiceRejectionsToLoad),
      response
    )
  }

  this[triggers.REJECT_INVOICE] = (invoiceRejection) => {
    const response = {
      success: (data) => {
        this.trigger(triggers.REJECTED_INVOICE, data)
      },
      error(data) {
        Logger.debug('POST invoice rejection error', data)
        Utils.sendErrorNotification('Problem rejecting invoice')
      },
    }

    Api.invoiceRejectionRequest(
      Api.POST, Utils.getEndpoint(UserStore), null, response,
      { invoice_rejection: invoiceRejection }
    )
  }

  this.getReasons = () => {
    if (this.reasons.length > 0) {
      this.trigger(triggers.REASONS_LOADED, this.reasons)
    }

    const success = (data) => {
      this.reasons = data
      this.trigger(triggers.REASONS_LOADED, this.reasons)
    }

    const response = {
      success,
      error(data) {
        Logger.debug('GET invoice rejection error', data)
      },
    }

    Api.invoiceRejectionReasons(Api.GET, Utils.getEndpoint(UserStore), null, response)

    return this
  }

  this.getInvoiceRejectionByInvoiceId = (invoiceId) => {
    const invoiceRejection = this.invoiceRejections[invoiceId]

    if (invoiceRejection) {
      this.trigger(triggers.INVOICE_REJECTION_LOADED, invoiceRejection)
    }

    this.invoiceRejectionsToLoad.push(invoiceId)

    clearTimeout(timeoutLoading)
    timeoutLoading = setTimeout(fetchFromServer, 200)
  }
}

MicroEvent.mixin(InvoiceRejectionStore)

const store = new InvoiceRejectionStore()
assign(store, triggers)

Dispatcher.register((payload) => {
  if (!payload.eventName) {
    throw new Error('empty Event')
  }

  if (typeof store[payload.eventName] === 'function') {
    store[payload.eventName](payload.data)
  } else {
    Logger.
    debug('unknown event received in InvoiceRejectionStoreStore', payload)
  }

  return true
})

module.exports = store
