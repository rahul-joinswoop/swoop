class LoginStore {
  constructor() {
    this.company = {}
    this.name = null
    this.phone = null
  }

  setInfo(name, phone) {
    this.name = name
    this.phone = phone
  }

  setSite(site) {
    this.company = {
      name: site.company.name || site.name,
      phone: site.company.phone || site.phone,
      address: site.company.location?.address || site.location.address,
    }
  }

  getCompany() {
    return this.company
  }

  getCompanyName() {
    return this.company.name
  }

  getCompanyPhone() {
    return this.company.phone
  }

  getName() {
    return this.name
  }

  getPhone() {
    return this.phone
  }

  getCompanyAddress() {
    return this.company.address
  }

  clearInfo() {
    this.company = {}
    this.name = null
    this.phone = null
  }
}

export default new LoginStore()
