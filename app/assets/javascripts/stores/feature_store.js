import BaseStore from 'stores/base_store'
import { includes } from 'lodash'

class FeatureStore extends BaseStore {
  constructor() {
    super({
      modelName: 'Feature',
    })
    this._nameMap = {}
  }

  isEditable() {
    return this.stores.UserStore.isRoot()
  }

  getEditUrl() {
    return 'root/features'
  }

  isFeatureEnabled(featureName, watcherId, company) {
    // TODO: set up watcherID to watch for list changes
    const id = this._nameMap[featureName]
    if (!company) {
      const user = this.stores.UserStore.getUser()
      if (user) {
        company = user.company
      }
    }
    if (id && company && company.features) {
      return includes(company.features, id)
    }
    return false
  }

  _addRowToInternalCollection(data) {
    super._addRowToInternalCollection(data)
    if (data && data.id) {
      this._nameMap[data.name] = data.id
    }
  }
}

const store = new FeatureStore()

module.exports = store
