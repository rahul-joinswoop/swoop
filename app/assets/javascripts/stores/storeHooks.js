import { useRef, useEffect } from 'react'
import EventRegistry from 'EventRegistry'
import { useRerender } from 'componentLibrary/Hooks'

let BC_ID_ITTR = 1
export function useWatcherId() {
  const watcherRef = useRef(() => {
    BC_ID_ITTR += 1
    return BC_ID_ITTR
  })
  const rerender = useRerender()
  const watcherId = watcherRef.current

  // Bind our watcher to the event registry
  useEffect(() => {
    const trigger = EventRegistry.getTrigger(watcherId)
    EventRegistry.bind(trigger, rerender)
    return () => {
      EventRegistry.clearListeners(watcherId)
      EventRegistry.unbind(trigger, rerender)
    }
  }, [watcherId, rerender])

  return watcherId
}

export function useRenderWatcherId() {
  const watcherId = useWatcherId()
  // Clear any bound events from the registry every render as they will be set back up
  useEffect(() => {
    EventRegistry.clearListeners(watcherId)
  })

  return watcherId
}

export function useStoreBind(store, eventName, callback) {
  const rerender = useRerender()
  useEffect(() => {
    if (eventName) {
      let tempCallback = callback
      if (!tempCallback) {
        tempCallback = rerender
      }
      store.bind(eventName, tempCallback)
      return () => {
        store.unbind(eventName, tempCallback)
      }
    }
  }, [store, eventName, callback, rerender])
}
