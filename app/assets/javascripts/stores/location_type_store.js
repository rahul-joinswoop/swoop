import Logger from 'lib/swoop/logger'
import Api from 'api'
import { extend, find, isEmpty, map, memoize, values, zipObject } from 'lodash'
import BaseStore from 'stores/base_store'


//
// Location Type Store
//
class LocationTypeStore extends BaseStore {
  // all company location types
  constructor() {
    super({
      modelName: 'LocationType',
    })
    this.companyLocationTypes = {}
    this.customLocationTypes = {}


    /*
    /  If Partner or Root user, jobs might have fleet specific LocationTypes.
    /  If so, that specific LocationType will be loaded.
    /
    /  It's called when jobs are added in JobStore. @see JobStore.addJob.
    /
    /  We only need to load each locationTypeId, locationTypeName combo once, so
    /  I am memoizing to prevent mulitple calls
    */
    this.forceLoadLocationType = memoize((locationTypeId, locationTypeName) => {
      const success = (customLocationType) => {
        this.customLocationTypes[customLocationType.id] = customLocationType
      }

      const response = {
        success,
        error(data) {
          Logger.debug('GET location_type error', data)
        },
      }

      if (locationTypeId !== null && locationTypeId !== undefined) {
        if (this.getLocationTypeById(locationTypeId) === undefined) {
          Api.locationTypeRequest(
            Api.GET,
            locationTypeId,
            response
          )
        }
      } else if (locationTypeName !== null && locationTypeName !== undefined) {
        if (this.findLocationTypeByName(locationTypeName) === undefined) {
          Api.locationTypeSearch(
            {
              name: locationTypeName,
            },
            response
          )
        }
      }
    })
  }

  isEditable() {
    return false
  }

  loadCollectionOnUserChanged() {
    return false
  }

  /*
  / Custom location types will be the ones that Fleet has
  / in their company. This will be used basically by Partner and Swoop users
  / when rendering / editing Fleet jobs.
  /
  / A Fleet user will always have it empty.
  */


  // takes all this.stores.UserStore.getUser().company.location_types and sets as objects in this.companyLocationTypes.
  addAllLocationTypesFromUserCompany() {
    if (this.stores.UserStore.getUser() && this.stores.UserStore.getUser().company) {
      this.companyLocationTypes = zipObject(
        map(
          this.stores.UserStore.getUser().company.location_types,
          location_type => location_type.id
        ),
        this.stores.UserStore.getUser().company.location_types
      )
    }
  }


  getLocationTypeById(locationTypeId) {
    if (locationTypeId === null || locationTypeId === undefined) {
      return null
    }

    if (isEmpty(this.companyLocationTypes)) {
      this.addAllLocationTypesFromUserCompany()
    }

    return extend({}, this.companyLocationTypes, this.customLocationTypes)[locationTypeId]
  }

  // Returns only the company location types
  getCompanyLocationTypes() {
    if (isEmpty(this.companyLocationTypes)) {
      this.addAllLocationTypesFromUserCompany()
    }

    return this.companyLocationTypes
  }

  findLocationTypeByName(locationTypeName) {
    if (locationTypeName === null || locationTypeName === undefined) {
      return null
    }

    if (isEmpty(this.companyLocationTypes)) {
      this.addAllLocationTypesFromUserCompany()
    }

    return find(
      values(this.getAllLocationTypes()),
      locationType => locationType.name === locationTypeName
    )
  }

  getAllLocationTypes() {
    return extend({}, this.companyLocationTypes, this.customLocationTypes)
  }

  locationTypeAdded() {
    console.warn('Support for adding a remote location type does not exist yet')
  }
}


const store = new LocationTypeStore()

window.LocationTypeStore = store
module.exports = store
