import Api from 'api'
import { find, map, values } from 'lodash'
BaseStore = require('stores/base_store').default
import EventRegistry from 'EventRegistry'
JobStore  = require('stores/job_store')
UserStore = require('stores/user_store')
import Utils from 'utils'

class VehicleStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Vehicle'
    })

    # collections
    @VEHICLES_WITH_DRIVER_LIST_ID = null
    @VEHICLES_WITH_DRIVER_LIST_NAME = 'vehiclesWithDriverList'
    @LOADING_VEHICLES_WITH_DRIVER_LIST = false

    @SORTED_VEHICLES_LIST_NAME = 'sortedVehiclesList'
    @SORTED_DRIVER_VEHICLES_LIST_NAME = 'sortedDriverVehiclesList'

    @_sortedVehiclesIds = null
    @_sortedDriverVehiclesIds = null

    @_sortedVehiclesIdsWatcherId = Utils.create_UUID()
    @_registerListListener(@_sortedVehiclesIdsWatcherId, @SORTED_VEHICLES_LIST_NAME, 'name')
    EventRegistry.bind(EventRegistry.getTrigger(@_sortedVehiclesIdsWatcherId), @_clearSortedVehiclesIds)

    @_sortedDriverVehiclesIdsWatcherId = Utils.create_UUID()
    @_registerListListener(@_sortedDriverVehiclesIdsWatcherId, @SORTED_DRIVER_VEHICLES_LIST_NAME, 'driver_name')
    EventRegistry.bind(EventRegistry.getTrigger(@_sortedDriverVehiclesIdsWatcherId), @_clearSortedDriverVehiclesIds)

    # triggers
    @SWITCHED_VEHICLES = 'switched_vehicles'

  isEditable: -> true

  getBatchUrl: ->
    if this.stores.UserStore.isFleet()
      return "fleet/rescue_vehicles/batch?vehicle_ids="

    return super(arguments...)

  getUrl: ->
    if UserStore.isPartner()
      return 'partner/vehicles'
    else if UserStore.isSwoop()
      return 'root/rescue_vehicles'
    else if UserStore.isFleetManaged()
      return 'fleet/rescue_vehicles'

  getEditUrl: -> @getUrl()

  hasPermissions: ->
    return UserStore.isPartner() || UserStore.isSwoop()

  isLazyLoaded: -> true

  getMyVehicleId: (watcherId = null) ->
    user = UserStore.getUser()
    if user
      myVehicle = @getVehicleIdByDriverId(user.id, watcherId)

      return myVehicle

  getVehicleIdByDriverId: (driverId, watcherId = null) ->
    if !UserStore.isPartner()
      return null

    if watcherId
      @_registerListListener(watcherId, @VEHICLES_WITH_DRIVER_LIST_NAME)

    if @VEHICLES_WITH_DRIVER_LIST_ID == null
      @_loadVehiclesWithDriverListIds()


    find(@getAll(), (vehicle) -> vehicle.driver_id == driverId)?.id

  getSortedVehicles: (watcherId = null) =>
    if watcherId
      @_registerListListener(watcherId, @SORTED_VEHICLES_LIST_NAME)

    if !@_sortedVehiclesIds
      sortedVehicles = values(@getAll()).sort((a, b) ->
        if a?.name? and b?.name?
          a.name.localeCompare(b.name)
      )

      @_sortedVehiclesIds = map(sortedVehicles, 'id')

    return @_mapIds(@_sortedVehiclesIds)

  getSortedDriverVehicles: (watcherId = null) =>
    if watcherId
      @_registerListListener(watcherId, @SORTED_DRIVER_VEHICLES_LIST_NAME)

    if !@_sortedDriverVehiclesIds
      sortedVehicles = values(@getAll()).sort((a, b) ->
        if a.driver_name? and b.driver_name?
          return a.driver_name.localeCompare(b.driver_name)
        if a.driver_name?
          return -1
        if b.driver_name?
          return 1
        a.name.localeCompare(b.name)
      )

      @_sortedDriverVehiclesIds = map(sortedVehicles, 'id')

    return @_mapIds(@_sortedDriverVehiclesIds)

  changeVehicle: (data) ->
    if data.driver_id?
      for id, vehicle of @getAll()
        if vehicle.driver_id == data.driver_id
          obj = {
            driver_id: null
            driver_name: null
            id: vehicle.id
          }
          @updateItem(obj)


    data.callbacks = {
      success: =>
        @trigger(@SWITCHED_VEHICLES)
    }
    @updateItem(data)



  resetVehicles: =>
    @VEHICLES_WITH_DRIVER_LIST_ID = null
    @LOADING_VEHICLES_WITH_DRIVER_LIST = false
    @_sortedVehiclesIds = null
    @_sortedDriverVehiclesIds = null
    @reset()

  _getListsObject: ->
    return [
      {
        list: @VEHICLES_WITH_DRIVER_LIST_ID,
        validator: @_hasDriverId,
        triggerName: @VEHICLES_WITH_DRIVER_LIST_NAME,
        finallyCallback: @_updateDriversWithVehicles
      }
      {
        list: @_sortedVehiclesIds,
        validator: (vehicle) -> !vehicle.deleted_at,
        triggerName: @SORTED_VEHICLES_LIST_NAME
      }
      {
        list: @_sortedDriverVehiclesIds,
        validator: (vehicle) -> !vehicle.deleted_at,
        triggerName: @SORTED_DRIVER_VEHICLES_LIST_NAME
      }
    ]

  _hasDriverId: (vehicle) ->
    !!vehicle.driver_id


  _clearSortedVehiclesIds: =>
    @_sortedVehiclesIds = null

  _clearSortedDriverVehiclesIds: =>
    @_sortedDriverVehiclesIds = null

  _loadVehiclesWithDriverListIds: =>
    if !@LOADING_VEHICLES_WITH_DRIVER_LIST && !@VEHICLES_WITH_DRIVER_LIST_ID && UserStore.isPartner()
      @LOADING_VEHICLES_WITH_DRIVER_LIST = true

      Api.getVehiclesWithDriver(
        success: (vehiclesWithDriver) =>
          for vehicle in vehiclesWithDriver
            @_add(vehicle)

          @VEHICLES_WITH_DRIVER_LIST_ID = map(vehiclesWithDriver, 'id')

          @_triggerListListeners(@VEHICLES_WITH_DRIVER_LIST_NAME)

          @LOADING_VEHICLES_WITH_DRIVER_LIST = false
        ,
        error: (data, textStatus, errorThrown) =>
          @LOADING_VEHICLES_WITH_DRIVER_LIST = false
      )

store = new VehicleStore()

module.exports = store
