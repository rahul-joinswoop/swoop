import { isFunction } from 'lodash'
import MicroEvent from 'microevent-github'
import $ from 'jquery'
import UserStore from 'stores/user_store'
import createPopupClass from 'components/maps/googleMapsPopup'
import * as GoogleMaps from 'lib/google-maps'
Popup = null

class MapStore extends MicroEvent

  map: null
  markers: []
  directions: []
  infos: []
  objects: []
  popups: []
  directionService: null
  #TRIGGERS
  MAP_STOLEN: "map_stolen"


  constructor: -> super arguments...

  cleanup: ->
    for marker in @markers
      google.maps.event.clearInstanceListeners(marker)
      marker.setMap(null)
    @markers = []

    for direction in @directions
      direction.setMap(null)
    @directions = []

    for info in @infos
      info.setMap(null)
    @infos = []

    for object in @objects
      object.setMap(null)
    @objects = []

    for popup in @popups
      popup.setMap(null)
    @popups = []

    #Clearing instance listeners causes the zoom the get messed up
    #google.maps.event.clearInstanceListeners(@map)

  addMarker: (marker) ->
    @markers.push(marker)

  createPopup: (options) ->
    if !Popup?
      Popup = createPopupClass()
    popup = new Popup(
      GoogleMaps.createLatLng(options.lat, options.lng),
      options.content
    )
    @popups.push(popup)
    return popup

  createCircle: (options) ->
    options.map = @map
    options.strokeWeight = 0
    circle = new google.maps.Circle(options)
    @objects.push(circle)
    return circle

  createMarker: (options) ->
    options.map = @map
    marker = GoogleMaps.createMarker(options)
    @markers.push(marker)
    return marker

  createPolyLine: (options, withMap = true) ->
    if withMap
      options.map = @map
    polyline = GoogleMaps.createPolyLine(options)
    @objects.push(polyline)
    return polyline

  createInfoWindow: (options, isTruck) ->
    if options && isTruck
      options.maxWidth = 266
    else if isTruck
      options = {maxWidth: 266}
    infoWindow = GoogleMaps.createInfoWindow(options)
    @infos.push(infoWindow)
    return infoWindow

  createDirectionsRenderer: (options) ->
    options.map = @map
    renderer = new google.maps.DirectionsRenderer(options)
    @directions.push(renderer)
    return renderer

  # TODO - refactor all callers of this code to use GoogleMaps directly
  createLatLngBounds: (sw = undefined , ne = undefined) ->
    GoogleMaps.createLatLngBounds(sw, ne)

  createPoint: (x, y) ->
    GoogleMaps.createPoint(x, y)

  createSize: (width, height) ->
    GoogleMaps.createSize(width, height)

  createLatLng: (lat, lng) ->
    GoogleMaps.createLatLng(lat, lng)

  getDirectionsService: () ->
    GoogleMaps.getDirectionsService()

  getDirections: (origin, dest, callback) ->
    GoogleMaps.getDirections(origin, dest, callback)

  getCurrentRouteDistance: (directionsRenderer) ->
    GoogleMaps.getCurrentRouteDistance(directionsRenderer)

  # end TODO
  stealMap: (container, mapOptions) ->
    if !@map?
      div = document.createElement('div')
      $(container).append(div)
      @map = new google.maps.Map(div, mapOptions)

      window?.analytics?.track?('Map Load', {
        component: "SwoopMap"
        user_id: UserStore.getUser()?.id
      })
    else
      @trigger(@MAP_STOLEN)
      @cleanup()

      mapNode = @map.getDiv()
      $(container).append(mapNode)
      @map.setOptions(mapOptions)
      google.maps.event.trigger(@map, "resize")

    return @map

store = new MapStore
window.mapStore = store
module.exports = store
