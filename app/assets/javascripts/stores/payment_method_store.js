import { find } from 'lodash'
import BaseStore from 'stores/base_store'

class PaymentMethodStore extends BaseStore {
  static ACH = 'ACH'

  static CASH = 'Cash'

  static CHECK = 'Check'

  static CREDIT_CARD = 'Credit Card'

  static AMERICAN_EXPRESS = 'American Express'

  static DISCOVER = 'Discover'

  static MASTERCARD = 'MasterCard'

  static VISA = 'Visa'

  static DEBIT_CARD = 'Debit Card'

  constructor() {
    super({
      modelName: 'PaymentType',
    })

    const values = [
      PaymentMethodStore.CASH,
      PaymentMethodStore.CHECK,
      PaymentMethodStore.CREDIT_CARD,
      PaymentMethodStore.AMERICAN_EXPRESS,
      PaymentMethodStore.DISCOVER,
      PaymentMethodStore.MASTERCARD,
      PaymentMethodStore.VISA,
      PaymentMethodStore.DEBIT_CARD,
      PaymentMethodStore.ACH,
    ]

    for (let index = 0; index < values.length; index++) {
      const value = values[index]

      this._add({
        id: parseInt(index) + 1,
        name: value,
      })
    }
  }

  getByName = name => find(this._modelCollection, method => method.name === name)

  isEditable = () => false

  loadCollectionOnUserChanged = () => false
}

export default new PaymentMethodStore()
