BaseStore = require('stores/base_store').default
import { sortBy, values } from 'lodash'

class RefererStore extends BaseStore
  constructor: ->
    super({
      modelName: 'Referer'
    })

  loadCollectionOnUserChanged: -> return false

  isLazyLoaded: -> true

  getAllForStore: (watcherId) =>
    sortBy(values(@loadAndGetAll(watcherId, ['name'])), (referer) =>
      if referer?.name == "Other"
        return referer?.name?.toLowerCase() #Lower Case ensures it's sorted after everthing else
      return referer?.name?.toUpperCase()
    )

module.exports = new RefererStore()
