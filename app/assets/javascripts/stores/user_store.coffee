import Api from 'api'
import moment from 'moment-timezone'
import Consts from 'consts'
import ReactDOMFactories from 'react-dom-factories'
import React from 'react'
import Logger from 'lib/swoop/logger'
MicroEvent = require('microevent-github')
Storage = require('./storage')
import Dispatcher from 'lib/swoop/dispatcher'
import Context from 'Context'
import Utils from 'utils'
import { extend, find, intersection, partial } from 'lodash'
$=require('jquery')
NotificationStore = require('stores/notification_store')
import { isUnitKm } from 'lib/localize'

triggers = Consts.USER_STORE

if window?
  window[key] = val for key, val of triggers

UserStoreFunc = ( (init_data=null) ->
    #Variables on Store
    @user = init_data
    @error = null

    bearer_token = null
    refresh_token = null
    #Visible Variables
    store =
        setCompany: (data) =>
          # dirty hack to override `RATE_TYPE_MAP`
          @user.company = data
          if isUnitKm(UserStore.getCompany()?.distance_unit)
            Consts.RATE_TYPE_MAP[Consts.MILEAGE_TOWED] = 'Kilometers (Towed)'
            Consts.RATE_TYPE_MAP[Consts.MILEAGE_EN_ROUTE_ONLY] = 'Kilometers (En Route)'
            Consts.RATE_TYPE_MAP[Consts.MILEAGE_EN_ROUTE] = 'Kilometers (En Route + Towed)'
            Consts.RATE_TYPE_MAP[Consts.MILEAGE_P2P] = 'Kilometers (Port to Port)'
          else
            Consts.RATE_TYPE_MAP[Consts.MILEAGE_TOWED] = 'Mileage (Towed)'
            Consts.RATE_TYPE_MAP[Consts.MILEAGE_EN_ROUTE_ONLY] = 'Mileage (En Route)'
            Consts.RATE_TYPE_MAP[Consts.MILEAGE_EN_ROUTE] = 'Mileage (En Route + Towed)'
            Consts.RATE_TYPE_MAP[Consts.MILEAGE_P2P] = 'Mileage (Port to Port)'

        getUser: =>
          @user

        getCompany: =>
          #TODO: This should return the ID and companyStore should be used to get attributes
          @user?.company

        getTimezone: =>
          tz = @user?.timezone
          if !tz?
            tz = moment.tz.guess()
          return tz

        getShortName: =>
          if @user?
            if @user.first_name?
                return @user.first_name
            if @user.username?
                return @user.username
            if @user.email?
                return @user.email
            return @user.full_name
          return ""

        createInstance: (data) =>
          return UserStoreFunc.bind({})(data)

        getError: =>
            @error
        hasRole: (role, user = @user) =>
            user? and user.roles? and role in user.roles
        isRoot: =>
            store.hasRole('root')
        isAdmin: (user = undefined) =>
            store.hasRole('admin', user)
        isDispatcher: =>
            store.hasRole('dispatcher')
        isOnlyDispatcher: =>
            store.isDispatcher() and not store.isAdmin() and not store.isDriver()
        isDriver: =>
            store.hasRole('driver')
        isOnlyDriver: =>
            store.isDriver() and not store.isAdmin() and not store.isDispatcher()
        isPartner: =>
            @user? and 'rescue' in @user.roles

        isFleet: =>
            @user? and 'fleet' in @user.roles

        isFleetManaged: =>
            store.isFleet() and @user?.company?.in_house != true

        isRescueDriver: (job) =>
          return @user?.id == job?.rescue_driver_id

        updateUser: (data) =>
          if(store.getUser() && store.getUser().id == data.id)
            store.setUser(extend({}, @user, data))

        isOnDuty: () => @user?.on_duty == true

        setUser: (data) =>
            @user = data
            Storage.set("user", data)
            store.trigger(CHANGE)
            store.trigger(USERME_CHANGED)
            if Utils.isIE() and !store.isEnterprise()
              setTimeout(=>
                Dispatcher.send(NotificationStore.NEW_NOTIFICATION, {
                  type: Consts.ERROR
                  message: ReactDOMFactories.a
                    target: "_blank"
                    href: "https://www.google.com/chrome/browser/desktop/"
                    "IE is an unsupported browser. Click to upgrade to Chrome."
                  dismissable: true
                })
              , 1000)

            v = navigator?.userAgent?.match(/Chrome\/(\S+)/)
            if v?
              if parseFloat(v[1]) < 53
                setTimeout(=>
                  Dispatcher.send(NotificationStore.NEW_NOTIFICATION, {
                    type: Consts.ERROR
                    message: ReactDOMFactories.a
                      target: "_blank"
                      href: "https://support.google.com/chrome/answer/95414?co=GENIE.Platform%3DDesktop&hl=en"
                      "You are using an unsupported version of Chrome. Click for instructions to update your Chrome browser."
                    dismissable: true
                  })
                , 1000)

        isFlightCar: =>
            if @user? and @user.company? and @user.company.name?
                return @user.company.name == "FlightCar"
            return false

        isFleetInHouse: =>
            store.isFleet() and @user?.company?.in_house == true

        isMotorClub: =>
            store.isFleet() and @user?.company?.name in Consts.MOTORCLUBS

        isTools: =>
          if @user
            @user.home == "#partner/tools"

        isCompany: (name) =>
          if @user?
            return @user?.company?.name == name
          return false

        userHasPaymentSettingsEnabled: =>
          return store.isAdmin() && @user.custom_account_visible

        hasPermissionsToCharge: =>
          perm = find(@user.company.role_permissions, (perm) -> perm.type == "charge")
          if !perm?
            return false
          return intersection(@user.role_ids, perm.role_ids).length > 0

        hasPermissionsToRefund: =>
          perm = find(@user.company.role_permissions, (perm) -> perm.type == "refund")
          if !perm?
            return false
          return intersection(@user.role_ids, perm.role_ids).length > 0
        isEnterprise: => store.isCompany("Enterprise")
        isTravelCar: => store.isCompany("TravelCar")
        isTesla: => store.isCompany("Tesla")
        isLincoln: => store.isCompany("Lincoln")
        isSouthern: => store.isCompany("Southern Wrecker & Recovery")
        isFarmersOwned: => store.isCompany(Consts.FARMERS) || store.isCompany(Consts.CENTURY)
        isFarmers: => store.isCompany(Consts.FARMERS)
        isCentury: => store.isCompany(Consts.CENTURY)
        isTuro: => store.isCompany("Turo")
        isTrekker: => store.isCompany("Trekker Group")
        isMTS: => store.isCompany("Motorcycle Towing Services, L.C.")
        isMetromile: => store.isCompany("Metromile")
        isFleetResponse: => store.isCompany("Fleet Response")
        isUSAA: => store.isCompany("USAA")
        isFinishLine: => store.isCompany("Finish Line Towing")
        isSwoop: => store.isCompany("Swoop")
        isEnterpriseFleetManagement: => store.isCompany(Consts.ENTERPRISE_FLEET_MANAGEMENT)
        isSwoopDispatcher: =>
          store.isSwoop() and not store.isRoot() and store.hasRole('swoop_dispatcher')
        isSwoopRoot: =>
          store.isSwoop() and store.isRoot()

        getMySiteIds: =>
          @user?.site_ids

        siteCodeLabel: =>
          if store.isEnterprise()
            return "Branch Code"
          else if store.isTesla()
            return "Location ID"
          else
            return "Site Code"

        getUserType: =>
            if store.isRoot() or store.isSwoopDispatcher()
              return "root"
            if store.isPartner()
              return "partner"
            if store.isFleet()
              return "fleet"

        getEndpoint: =>
            if store.isRoot() or store.isSwoopDispatcher()
                return "root"
            if store.isFleet()
                return "fleet"
            return "partner"

        getUserUrl: =>
            if store.isRoot() || store.isPartner() || store.isSwoopDispatcher()
              return "partner/dispatch"
            if store.isFleet()
              return "fleet"
            return "login"

        getPasswordRequest: =>
          return Storage.get('password_request')

        forgotPasswordUrl: =>
          return 'forgotPassword'

        forgotPasswordSuccessUrl: =>
          return 'forgotPasswordSuccess'

        routeUser: (data) =>
            Logger.debug("LOGIN: routing user", data, data.home)
            @user = data
            Context.navigate UserStore.getUserUrl()
            return

        routeLogin: () =>
            Logger.debug("LOGIN: routing login")
            Context.navigate UserStore.getUserUrl()
            return

        routeForgotPassword: () =>
          Logger.debug("LOGIN: routing forgot password")
          Context.navigate UserStore.forgotPasswordUrl()
          return

        routeForgotPasswordSuccess: (password_request) =>
          Logger.debug("LOGIN: routing forgot password success")
          setPasswordRequest(password_request)
          Context.navigate UserStore.forgotPasswordSuccessUrl()
          return

        authenticateAs: (user_id) =>
          Api.authenticateAs(user_id,
            success: (data) =>
                prevSession = Storage.getSession()
                prev_bearer = bearer_token
                prev_refresh = refresh_token
                randomSession = Math.floor(Math.random()*100000000)

                Storage.setSession(randomSession)
                bearer_token = data.access_token
                refresh_token = data.refresh_token
                setBearerCookie bearer_token, data.expires_in
                Api.setBearerToken bearer_token
                window.open( window.location.origin + "#session_id="+Storage.getSession(), '_blank')

                Storage.setSession(prevSession)
                bearer_token = prev_bearer
                refresh_token = prev_refresh
                setBearerCookie(bearer_token, data.expires_in)
                #Logger.debug(data, bearer_token)
                #Dispatcher.dispatch
                #    eventName: RECEIVED_API_KEY
                #    data: data
          )

        getVehicleCategories: =>
          return @user?.company?.vehicle_categories

        hasAuthToken: =>
          bearer_token = getBearerToken()
          if bearer_token?
            return true
          else
            return false

        getMe: (cb) ->
            Api.userMeRequest({
              success: (data) =>
                receivedUser(data)
                if cb?
                  cb(data)

              error: (data) =>
                console.warn(data)

            })

        tokenLogin: (token, callbacks) =>
          Api.clearBearerToken()
          Api.loginWithToken(token, callbacks)

        getBearerToken: () =>
          getBearerToken()

    store[key] = val for key, val of triggers
    MicroEvent.mixin( store )
    trigger = store.trigger

    #Any helper functions
    setLocalStorage = (key, value) ->
        Storage.set(key, value)

    getBearerToken = () ->
        return Storage.get("bearer")

    getRefreshToken = () ->
        return Storage.get("refresh")
        token
    setBearerToken = (token) ->
        setLocalStorage("bearer", bearer_token)

    setRefreshToken = (token) ->
        setLocalStorage("refresh", refresh_token)

    setPasswordRequest = (password_request) ->
        setLocalStorage("password_request", password_request)

    removeBearerCookie = =>
        Logger.debug("clear tokens")
        Storage.remove("bearer")
        Storage.remove("refresh")

    refreshToken = (grabUser, isSuccessfullCallback) =>
        if refresh_token?
            Api.refreshTokenRequest(refresh_token,
                success: (data) =>
                    if not grabUser? || grabUser == false
                        data.keep_user = true
                    Dispatcher.send(RECEIVED_API_KEY, data)
                    isSuccessfullCallback?(true)
                error: (data) =>
                  isSuccessfullCallback?(false)
            )
        else
          removeBearerCookie()
          Dispatcher.send(LOGOUT,null)

    setBearerCookie = (b, expires_in) =>
        setBearerToken(bearer_token)
        setRefreshToken(refresh_token)
        grabBearer_token()

    grabBearer_token = =>
        b_token = getBearerToken()
        if b_token?
            Api.setBearerToken b_token
            bearer_token = b_token
        r_token = getRefreshToken()
        if r_token?
            refresh_token = r_token

    setupTokenRefresh = (data, force, grabUser) =>
        #just refresh every four hour for the heck of it to ensure no breaks
        expiration = 4 * 60 * 60
        #if data? && data.expires_in?
            #expiration = parseInt(data.expires_in)
        if @refreshInterval?
            clearInterval(@refreshInterval)

        @refreshInterval = setInterval(refreshToken, expiration * 1000)
        if force
            #wait until everything is set up to refresh
            setTimeout(partial(refreshToken, grabUser), 60*1000)
            #refreshToken(grabUser)
    grabAndSetUser = (cb) =>
        Api.getUser(
            success: (data) =>
              @[RECEIVED_USER](data)
              if cb?
                  cb(data)

            error: (data) =>
                @error = data
                store.trigger(CHANGE)
        )

    clearError = =>
        @error = null

    receivedUser = @[RECEIVED_USER] = (pdata) =>
        @user = extend({}, @user, pdata)

        if store.isRoot()
            $("body").addClass("root")

        if pdata?.timezone?
          moment.tz?.setDefault?(pdata.timezone)
        else
          setTimeout( =>
              #Hack to get reference to usersstore
              stores.User.updateItem({
                id: pdata.id,
                timezone: moment.tz.guess()
              })
          ,0)
        segmentIdentify()
        try
          Tracking.track("App Opened")
        catch error
          Rollbar.error('Tracking is not defined in user store App open tracking', error)
        store.trigger(LOGIN)

    @[RECEIVED_API_KEY] = (pdata) =>
      bearer_token = pdata.access_token
      refresh_token = pdata.refresh_token
      setBearerCookie bearer_token, pdata.expires_in
      Api.setBearerToken bearer_token
      setupTokenRefresh(pdata)
      clearError()

      if not pdata.keep_user
          grabAndSetUser( (data) =>
              if pdata.preventRouting
                return
              store.routeUser data
          )

    @[REFRESH_TOKEN] = (isSuccessfullCallback) =>
      refreshToken(false, isSuccessfullCallback)


    @[RECEIVED_LOGIN] = ({ username, password, preventRouting }) =>
        clearError()
        removeBearerCookie()
        Api.clearBearerToken()
        Api.login(username, password,
          success: (data) =>
            data.preventRouting = preventRouting
            Dispatcher.send(RECEIVED_API_KEY, data)
          ,
          error: (data) =>
            @error = data
            store.trigger(CHANGE)
        )

    @[LOGIN] = () =>
      clearError()
      store.routeLogin()

    @[FORGOT_PASSWORD] = () =>
      clearError()
      store.routeForgotPassword()

    @[NEW_PASSWORD_REQUEST] = (pdata) =>
      clearError()

      Api.newPasswordRequest(pdata.userInput,
        success: (data, textStatus, xhr) =>
          if xhr.status is 201
            @[FORGOT_PASSWORD_SUCCESS](data)
          else if xhr.status is 200
            store.trigger USER_NOT_FOUND
          else
            store.trigger PASSWORD_REQUEST_FAIL
        error: (data) =>
          store.trigger PASSWORD_REQUEST_FAIL
      )

    @[FORGOT_PASSWORD_SUCCESS] = (password_request) =>
      store.routeForgotPasswordSuccess(password_request)

    @[VALIDATE_PASSWORD_REQUEST] = (pdata) =>
      clearError()

      Api.validatePasswordRequest(pdata.uuid,
        success: (data) =>
          store.trigger PASSWORD_REQUEST_IS_VALID, data
        error: (data) =>
          if data.status is 404
            store.trigger PASSWORD_REQUEST_IS_NOT_VALID
          else
            store.trigger CHANGE_PASSWORD_FAIL
      )

    @[CHANGE_PASSWORD] = (pdata) =>
      clearError()

      Api.changePassword(pdata,
        success: (data) =>
          Dispatcher.send(UserStore.RECEIVED_LOGIN, {
            username: pdata.user.username,
            password: pdata.user.password
          })
        error: (data) =>
          if data.status is 422
            store.trigger INVALID_PASSWORD
          else if data.status is 404
            store.trigger PASSWORD_REQUEST_IS_NOT_VALID
          else
            store.trigger CHANGE_PASSWORD_FAIL
      )

    store.logout = @[LOGOUT] = (pdata) =>
      Api.revokeToken =>
        Api.clearBearerToken()

        #hack to get rid of back drop added by bootstrap
        $('.modal-backdrop.fade.in').remove()
        location.reload()

      removeBearerCookie()
      clearError()
      Context.navigate Consts.LOGIN_PATH
      @user = null
      @error = null
      store.trigger(LOGOUT)

    store.segmentIdentify = segmentIdentify = (user, job=null) =>
      if !user?
        user=UserStore.getUser()
      if user
        if window?.analytics
          Tracking?.identify(user)

        else
          Logger.debug "Segment analytics not init'd"

        if window?.Rollbar && window.FS
          window.Rollbar.configure
            transform: (payload) ->
              payload.custom = {
                fullStoryUrl: window.FS.getCurrentSessionURL(true)
              }
              return payload

    if !init_data?
      #Listen for events
      Dispatcher.register(( payload ) =>
          Logger.debug("in payload of user dispatcher",payload)
          if not payload.eventName
            console.warn('empty Event '+JSON.stringify(payload))
          if typeof @[payload.eventName] is 'function'
              @[payload.eventName](payload.data)
          else
            Logger.debug('unknown event received in UserStore',payload.eventName)

          true
      )

      #Check if session_id in HASH
      matches = window.location.hash.match(new RegExp('session_id=([^&]*)'))
      session_id = null
      if matches?
        window.location.hash = ''
        session_id = matches[1]
        Storage.setSession(session_id)

      #initial setup
      grabBearer_token()
      if bearer_token? and bearer_token.length > 0
        Logger.debug("bearer token exists")
        grabAndSetUser()

    return store
)

UserStore = UserStoreFunc.bind({})()

if window?
    window.UserStore = UserStore
module.exports = UserStore
