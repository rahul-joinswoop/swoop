/*
SwoopEmitter - Add event types to MicroEvent.
---
USE:
    import SwoopEmitter from 'lib/swoop/emitter'

    class StoreClass extends SwoopEmitter
      constructor: ->
      someMethod: ->
        @trigger StoreClass.CHANGE
        * OR:
        @trigger SwoopEmitter.CHANGE

    store = new StoreClass
    store.bind StoreClass.CHANGE, ->
      * Handle update event
*/
import MicroEvent from 'microevent-github'

class SwoopEmitter {
  static CHANGE = 'CHANGE'
}

// MicroEvent assigns a new object to its prototype, which appears to break
// inheritance in CoffeeScript. We can work around this by using MicroEvent's
// mixin() to extend our class.

MicroEvent.mixin(SwoopEmitter)
export default SwoopEmitter
