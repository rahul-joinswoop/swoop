import Logger from 'js-logger'

/*
This module sets the default global log level and adds level constants to the
object returned by `get()` to allow the following patterns.

Use the global logger:

    Logger = require('js-logger')
    Logger.info "I will NOT print" # Default global level is WARN
    Logger.warn "I will print"

Enable logging in the current module:

    Logger = require('js-logger').get('my_module') # <- Contextual logger
    Logger.setLevel(Logger.INFO)
    Logger.info "I will print"

*/
// Set (global) default log level

Logger.useDefaults({
  defaultLevel: Logger.WARN,
}) // Add level constants to the object returned by `get()`.

const { get } = Logger

const wrapper = function wrapper(name) {
  const logger = get(name)
  logger.DEBUG = Logger.DEBUG
  logger.INFO = Logger.INFO
  logger.TIME = Logger.TIME
  logger.WARN = Logger.WARN
  logger.ERROR = Logger.ERROR
  logger.OFF = Logger.OFF
  return logger
}

Logger.get = wrapper
export default Logger
