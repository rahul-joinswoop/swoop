import { Dispatcher as FluxDispatcher } from 'flux'

const Dispatcher = new FluxDispatcher()

Dispatcher.send = (eventName, data) =>
  Dispatcher.dispatch({
    eventName,
    data,
  })

export default Dispatcher
