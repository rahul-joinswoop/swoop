// Pass any number of objects to flatten into a single, new object instance.
// Example:
//     React.createElement SomeElement, makeProps({ rows: [] }, {cols: []})
export function makeProps(...objectsToSquash) {
  const props = {}
  objectsToSquash.reduce((p, c) => {
    Object.assign(p, { ...c })
    return p
  }, {})
  return props
}
