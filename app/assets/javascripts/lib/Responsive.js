import React from 'react'
import MediaQuery from 'react-responsive'

const MOBILE_BREAKPOINT = 787

const Mobile = (props = {}) => <MediaQuery maxWidth={MOBILE_BREAKPOINT - 1} {...props} />
const Desktop = (props = {}) => <MediaQuery minWidth={MOBILE_BREAKPOINT} {...props} />

export { MOBILE_BREAKPOINT, Mobile, Desktop }
