// based on https://github.com/pgarciacamou/react-context-consumer-hoc/blob/master/src/index.js
// but takes a mapping of prop name -> context api as input, ie:
//
// withContext({ foo: FooContext, bar: BarContext })
//
// will provide "foo" and "bar" props with the appropriate context
import React from 'react'

const consumeContext = (ChildConsumer, [key, ContextAPI]) =>
  React.forwardRef((props = {}, ref) =>
    <ContextAPI.Consumer>
      {context => (
        <ChildConsumer
          {...props}
          {...{ [key]: context, ref }}
        />
      )}
    </ContextAPI.Consumer>
  )

function ContextConsumerHOC(contextMap = {}) {
  return ComposedComponent =>
    // Recursively consume the APIs only once.
    Object.entries(contextMap).reduce(consumeContext, ComposedComponent)
}

export default ContextConsumerHOC
