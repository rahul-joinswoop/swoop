const phoneRe = /^1?[2-9]\d{2}[2-9]\d{2}\d{4}$/

export function validate(number) {
  return phoneRe.test(number.replace(/\D/g, ''))
}

export default { validate }
