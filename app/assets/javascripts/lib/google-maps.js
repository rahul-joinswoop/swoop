/* globals google */

import UserStore from 'stores/user_store'
import { isFunction } from 'lodash'
import { getUnitSystem } from 'lib/localize'

export const createLatLngBounds = (sw, ne) =>
  new google.maps.LatLngBounds(sw, ne)

export const createPoint = (x, y) =>
  new google.maps.Point(x, y)

export const createSize = (width, height) =>
  new google.maps.Size(width, height)

export const createLatLng = (lat, lng) =>
  new google.maps.LatLng(lat, lng)

export const createPolyLine = options =>
  new google.maps.Polyline(options)

export const createMarker = options =>
  new google.maps.Marker(options)

export const createInfoWindow = options =>
  new google.maps.InfoWindow(options)

export const convertToLatLng = (location = {}) =>
  createLatLng(
    isFunction(location.lat) ? location.lat() : location.lat,
    isFunction(location.lng) ? location.lng() : location.lng,
  )

export const computeHeading = (from, to) =>
  google.maps.geometry.spherical.computeHeading(from, to)

let directionsService
export const getDirectionsService = () => {
  directionsService ||= new google.maps.DirectionsService()
  return directionsService
}

export const getDirections = (origin, dest, callback) => {
  const unit = getUnitSystem(UserStore.getCompany()?.distance_unit)
  return getDirectionsService().route(
    {
      origin,
      destination: dest,
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem[unit],
    },
    callback,
  )
}

export const getCurrentRouteDistance = (directionsRenderer) => {
  let distanceMeters
  if (directionsRenderer != null) {
    const directions = directionsRenderer.getDirections()

    if (directions != null) {
      const route = directions.routes[directionsRenderer.getRouteIndex() || 0]

      if (route != null) {
        distanceMeters = route.legs.reduce((prev, leg) => prev + leg.distance.value)
      }
    }
  }
  return distanceMeters
}

export const getDistanceBetween = (from, to) =>
  google.maps.geometry.spherical.computeDistanceBetween(from, to)

export const isLocationOnEdge = (latLng, polyline, tolerance) =>
  google.maps.geometry.poly.isLocationOnEdge(
    latLng,
    polyline,
    tolerance,
  )

// The tolerance is the decimal degrees for accuracy. 0.0001 is roughly 11m while 0.001 is 111m.
// https://en.wikipedia.org/wiki/Decimal_degrees#Precision

export const checkIsOnPolyline = (location, polyline, tolerance = 0.001) =>
  isLocationOnEdge(convertToLatLng(location), polyline, tolerance)

export const checkIsOnPath = (location, path) => {
  const polyline = createPolyLine({
    geodesic: true,
    path,
    visible: false,
  })

  return checkIsOnPolyline(location, polyline)
}

export const checkIsOnRoute = (location, route) =>
  // probably we need not overview_path but more accurate path
  (route ?
    checkIsOnPath(location, route.overview_path) :
    false)


// Returns the minimum distance from a point to a line
// This is just regular geometry but not spherical geometry. I think it is fine for small distances
// TODO implement it using spherical geometry
export const getDistanceToStep = (location, step) => {
  const d1 = getDistanceBetween(step.start_location, location)
  const d2 = getDistanceBetween(location, step.end_location)
  const d3 = getDistanceBetween(step.start_location, step.end_location)

  // alpha is the angle between the line from start to point, and from start to end
  const alpha = Math.acos((d1 * d1 + d3 * d3 - d2 * d2) / (2 * d1 * d3))

  // beta is the angle between the line from end to point and from end to start //
  const beta = Math.acos((d2 * d2 + d3 * d3 - d1 * d1) / (2 * d2 * d3))

  // if the angle is greater than 90 degrees, then the minimum distance is the
  // line from the start to the point
  if (alpha > Math.PI / 2) {
    return d1
  }

  if (beta > Math.PI / 2) {
    // same for the beta
    return d2
  }

  // otherwise the minimum distance is achieved through a line perpendicular
  // to the start-end line, which goes from the start-end line to the point
  return Math.sin(alpha) * d1
}
