import PhoneNumber from 'lib/phonenumber'
import createReactClass from 'create-react-class'
import React from 'react'
import moment from 'moment-timezone'
import { clone, difference, filter, find, forEach, get, has, isArray, isNumber, isEmpty, isObject, map, reduce, sortBy, unset } from 'lodash'
import fp from 'lodash/fp'
import Consts from 'consts'
import ReactDOMFactories from 'react-dom-factories'
import Dispatcher from 'lib/swoop/dispatcher'

# TODO - figure out which of these are in use, which aren't, which are only used by a single
# component, etc

# TODO - this is a standard, should use the version from @babel/polyfill / @babel/preset-env
# instead
String::startsWith = (prefix) ->
  @indexOf(prefix) == 0

String::capitalize = ->
  @charAt(0).toUpperCase() + @slice(1)

String::capitalizeAllFirstLetters = ->
  _string = @toLowerCase()
  _string.replace(/(?:^|\s)\S/g, (firstLetter) -> firstLetter.toUpperCase())

String::upcaseFirstLetters = ->
  @.replace(/\w\S*/g, (txt) -> txt.charAt(0).toUpperCase() + txt.substr(1))

String::capitalizeWords = (words) ->
  _string = @
  words.forEach((word) -> _string = _string.replace(word, word.toUpperCase()))
  _string


#TODO: move these to render Utils so we can split out mobile
if window
  window.ll = require('react-intl')
  {injectIntl, FormattedNumber} = require('react-intl')
  FNumber =  React.createFactory(FormattedNumber)

Utils = {

    shape:
      isArray: (shape) ->
        Object.prototype.toString.call(shape) == "[object Array]"
      isFunction: (shape) ->
        Object.prototype.toString.call(shape) == "[object Function]"
      isNumber: (shape) ->
        Object.prototype.toString.call(shape) == "[object Number]"
      isObject: (shape) ->
        Object.prototype.toString.call(shape) == "[object Object]"
      isString: (shape) ->
        Object.prototype.toString.call(shape) == "[object String]"

    isSameToDecimal: (value1, value2, fixedToAmount) ->
      return @safeToFixed(value1, fixedToAmount) == @safeToFixed(value2, fixedToAmount)

    safeToFixed: (value, fixedToAmount) ->
      if isNumber(value)
        return value.toFixed(fixedToAmount)
      return value

    isTimerExpired: (job, AuctionStore) ->
      if job.issc_dispatch_request?
        return job.status != Consts.ASSIGNED
      else
        return AuctionStore.jobHasAuction(job) && !AuctionStore.jobHasLiveAuction(job)

    getJobsLastHistoryItemTimeAndName: (job) ->
      name = null
      time = null
      if job.status == Consts.DISPATCHED && job.scheduled_for
        name = 'Scheduled'
        time = @formatDateTime(job.scheduled_for)
      else
        status = @getJobsLastHistoryItem(job)
        if status?
          name = status.name
          timeStr = status.last_set
          if status.adjusted_dttm?
            timeStr = status.adjusted_dttm
          time = @formatDateTime(timeStr)
      return {
        name: name,
        time: time,
      }

    getJobsLastHistoryItem: (job) ->
      # pick only last dispatched status
      fp.flow(
        fp.pick(Consts.DISPATCHED_STATUSES),
        fp.sortBy('last_set'),
        fp.last,
      )(job.history)


    renderPhoneNumber: (phoneNumber) ->
      if phoneNumber?
        phoneNumber = ""+phoneNumber
        if phoneNumber.length == 12 && phoneNumber.substring(0, 2) == "+1"
          phoneNumber = phoneNumber.replace(/(\+\d+)(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$2-$3-$4')
          phoneNumber = phoneNumber.replace(/(\+)(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$2-$3-$4')
          phoneNumber = phoneNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1-$2-$3')
      return phoneNumber

    getDisplayInvoice: (job, isRootUser) ->
      if isRootUser
        return if job.rescue_company?.id then find(job.invoices, (j) -> j.sender_id == job.rescue_company?.id) else null
      return job.invoices?[0]

    isscHumanStatus: (issc) ->
      switch issc.status
        when "disconnected" then "Disconnected"
        when "unregistered" then "Unregistered"
        when "registering" then "Registering"
        when "registered" then "Logged Out"
        when "logging_in"
          if issc.logged_in_at
            "Logging In"
          else
            "Waiting for MC Auth"
        when "logged_in" then "Logged In"
        when "logging_out" then "Logging Out"
        when "deregistering" then "Deregistering"
        when "register_failed" then "Register Failed"
        when "password_incorrect" then "Password Incorrect"
        else "Unknown State"

    isscStatusColor: (status) ->
      switch status
        when "disconnected" then "red"
        when "unregistered" then "red"
        when "registering" then "orange"
        when "registered" then "black"
        when "logging_in" then "orange"
        when "logged_in" then "green"
        when "logging_out" then "orange"
        when "deregistering" then "orange"
        when "register_failed" then "red"
        when "password_incorrect" then "red"
        else "black"

    combineStyles: (...styles) ->
      newCombinedStyles = {}
      styles.forEach((styleObject) ->
        if isObject(styleObject) && !isArray(styleObject)
          forEach(styleObject, (value, key) ->
            newCombinedStyles[key] = value
          )
        if isArray(styleObject)
          stylesFromArray = Utils.combineStyles(...styleObject)
          forEach(stylesFromArray, (value, key) ->
            newCombinedStyles[key] = value
          )
      )
      return newCombinedStyles


    getCookie: (name) ->
      nameEQ = name + '='
      ca = document.cookie.split(';')
      i = 0
      while i < ca.length
        c = ca[i]
        while c.charAt(0) == ' '
          c = c.substring(1, c.length)
        if c.indexOf(nameEQ) == 0
          return c.substring(nameEQ.length, c.length)
        i++
      null

    # pxlMovePerTick is the speed of the smooth scroll. 1 is slow. 10 is fast.
    smoothScrollVertical: (moveableElement, destination, isMovingDown, pxlMovePerTick = 1) ->
      timeout = setInterval((->
        currentTopPos = moveableElement.scrollTop
        # Don't scroll past the borders.
        borderBottom = moveableElement.scrollHeight - (moveableElement.clientHeight)
        if destination > borderBottom
          destination = borderBottom
        if destination < 0
          destination = 0
        # Ensures that if pxlMovePerTick isn't divisible by destination, we don't scroll past destination by the remainder.
        tickToDestination = Math.abs(destination - currentTopPos)
        if tickToDestination < pxlMovePerTick
          pxlMovePerTick = tickToDestination
        # This controls the movement per tick in both directions
        if isMovingDown and destination > currentTopPos
          moveableElement.scrollTop = currentTopPos + pxlMovePerTick
        else if !isMovingDown and destination < currentTopPos
          moveableElement.scrollTop = currentTopPos - pxlMovePerTick
        else
          clearInterval timeout
        return
      ), 1)
      return

    # pxlMovePerTick is the speed of the smooth scroll. 1 is slow. 10 is fast.
    smoothScrollHorizontal: (moveableElement, destination, isMovingRight, pxlMovePerTick = 1) ->
      timeout = setInterval((->
        currentLeftPos = moveableElement.scrollLeft
        # Don't scroll past the borders.
        borderRight = moveableElement.scrollWidth - moveableElement.clientWidth
        if destination > borderRight
          destination = borderRight
        if destination < 0
          destination = 0
        # Ensures that if pxlMovePerTick isn't divisible by destination, we don't scroll past destination by the remainder.
        tickToDestination = Math.abs(destination - currentLeftPos)
        if tickToDestination < pxlMovePerTick
          pxlMovePerTick = tickToDestination
        # This controls the movement per tick in both directions
        if isMovingRight and destination > currentLeftPos
          moveableElement.scrollLeft = currentLeftPos + pxlMovePerTick
        else if !isMovingRight and destination < currentLeftPos
          moveableElement.scrollLeft = currentLeftPos - pxlMovePerTick
        else
          clearInterval timeout
        return
      ), 1)
      return

    escapeRegExp: (str) ->
      return str?.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&")

    getPlural: (word) ->
      lastLetter  = word.slice(-1)

      pluralMap =
        y: =>  return word.slice(0, -1) + "ies"
        s: =>  return word += "es"


      if pluralMap[lastLetter]
        return pluralMap[lastLetter]()

      return word+ 's'

    getInvoiceBalance: (invoice) ->
      total = 0
      if invoice?.line_items?
        for line_item in invoice.line_items
          if !line_item.deleted_at?
            amount = parseFloat(line_item.net_amount)
            if !isNaN(amount)
              total += amount
            tax_amount = parseFloat(line_item.tax_amount)
            if !isNaN(tax_amount)
              total += tax_amount
      if invoice?.payments?
        for payment in invoice.payments
          if !payment.deleted_at?
            amount = parseFloat(payment.amount)
            if !isNaN(amount)
              #amount is negative so add it
              total += amount

      return Utils.toFixed(total, 2)

    getInvoiceBalanceStr: (invoice) ->
      Utils.toFixed(Utils.getInvoiceBalance(invoice),2)

    functionize: (obj, prop) ->
      ret = obj?[prop]
      if typeof(ret) == "function"
        return ret
      else
        return -> ret
    getOrCall: (obj, prop, params...) ->
      ret = obj?[prop]
      if typeof(ret) == "function"
        return ret(params...)
      else
        return ret

    splitIntoThree: (arr) ->
      first_list_size = Math.ceil(arr.length / 3) - 1
      second_list_size = first_list_size + Math.ceil((arr.length - 1) / 3)
      first_list = arr[0..first_list_size]
      second_list = arr[(first_list_size+1)..second_list_size]
      third_list = arr[(second_list_size+1)..]
      return [first_list, second_list, third_list]

    deepMerge: (target, source) ->
      for key of source
        original = target[key]
        next = source[key]
        if original and next and typeof next == 'object'
          @deepMerge original, next
        else
          target[key] = next
      target

    playDing: (callback) ->
      if ding?
        promise = ding.play()
        if promise?
          promise.then(callback).catch((error) =>
            #TODO: ENG-6691 Safari: Handle this case better (the user hasn't clicked on the site yet, but had an alert)
            console.warn(error)
          )
    getJobDisplayService: (job, GeneralStore) =>
      service = GeneralStore.getServiceNameOrAlias(job?.service_code_id, job?.service_alias_id)
      if job?.will_store
        return service + ", Storage"
      return service

    sendNotification: (msg, type, opts = {}) =>
      timeout = 2000

      if opts.timeout
        timeout = opts.timeout
      else if type == Consts.ERROR
        timeout = 0
      else if opts.perpetual
        timeout = 0

      if Dispatcher? and Dispatcher.send?
        setTimeout (=>
          #TODO: would be better to put in consts and grab from there
          Dispatcher.send("new_notification", {
            type: if type then type else Consts.INFO
            message: msg
            dismissable: true
            timeout: timeout
          })
        ), 0

    #Browser toFixed can't be trusted, rounds .5 to 0 instead of 1
    #WARNING: Not true rounding as it accounts for floating point inconsistencies
    toFixed:( num, precision ) ->
      #In case we were passed a floating point number,
      #round the number after the precision we are looking for to cut off any tiny discrepencies
      num = parseFloat(num).toFixed(precision+1)
      return (+(Math.round(+(num + 'e' + precision)) + 'e' + -precision)).toFixed(precision)

    sendErrorNotification: (msg, opts = {}) =>
      Utils.sendNotification(msg, Consts.ERROR, opts)

    sendSuccessNotification: (msg, opts = {}) =>
      Utils.sendNotification(msg, Consts.SUCCESS, opts)

    sendWarningNotification: (msg, opts = {}) =>
      Utils.sendNotification(msg, Consts.WARNING, opts)

    getSymptomLabel: (UserStore, opts = {}) ->
      plural = opts.plural || false
      postfix = opts.postfix || ""
      if UserStore.isEnterprise()
        ret = "Tow Category"
      else
        ret = "Symptom"

      if plural
        ret = Utils.getPlural(ret)

      return ret + postfix

    getCustomerNameLabel: (UserStore, job, postfix="") =>
      if Utils.isEnterpriseJob(UserStore, job)
        return "Branch Rep"+postfix
      else
        return "Customer Name"+postfix

    getCustomerPhoneLabel: (UserStore, job, postfix="") =>
      if Utils.isEnterpriseJob(UserStore, job)
        return "Rep Phone"+postfix
      else
        return "Customer Phone"+postfix

    getPOLabel: (UserStore, job, postfix="") ->
      if Utils.isEnterpriseJob(UserStore, job)
        return "Branch/RA #"+postfix
      else
        return "PO"+postfix

    getRefLabel: (UserStore, job, postfix="") ->
      if Utils.isTuroJob(UserStore, job)
        return "Reservation"+postfix
      else if Utils.isEnterpriseJob(UserStore, job) || Utils.isMetromileJob(UserStore, job)
        return "Claim Number"+postfix
      else if Utils.isEnterpriseFleetManagementJob(UserStore, job)
        return "Claim Number/RO"+postfix
      else
        return "Reference"+postfix

    isSwoopJob: (UserStore, job) ->
      # type=='FleetManagedJob' is always indicative of a Swoop job;
      # this alternative because job.account.name is not available in all areas of the app
      UserStore.isSwoop() || job?.account?.name == "Swoop" || job?.type == 'FleetManagedJob'

    isPartnerOnSwoopJob: (UserStore, job) ->
      UserStore.isPartner() && Utils.isSwoopJob(UserStore, job)

    isTuroJob: (UserStore, job) ->
      UserStore.isTuro() || job?.owner_company?.name == "Turo"

    isEnterpriseJob: (UserStore, job) ->
      UserStore.isEnterprise() || job?.owner_company?.name == "Enterprise"

    isMetromileJob: (UserStore, job) ->
      UserStore.isMetromile() || job?.owner_company?.name == "Metromile"

    isFleetResponseJob: (UserStore, job) ->
      UserStore.isFleetResponse() || job?.owner_company?.name == "Fleet Response"

    isEnterpriseFleetManagementJob: (UserStore, job) ->
      UserStore.isEnterpriseFleetManagement() || job?.owner_company?.name == Consts.ENTERPRISE_FLEET_MANAGEMENT

    handleError: (data, textStatus, errorThrown, force) =>
      if Dispatcher? and Dispatcher.send?
        obj = {
          data: data
          textStatus: textStatus
          errorThrown: errorThrown
        }
        if force
          obj.forceMsg = "Uh oh! There was an issue saving your change. Please refresh and try again."
        #TODO: would be better to put in consts and grab from there
        Dispatcher.send("show_error", obj)

    fixBool: (str) ->
      if str == "true"
        return true
      if str == "false"
        return false
      return str
    getStatusOrder: (job_status) ->
      #TODO: Turn this into a HASH
      for status, i in Consts.JOB_STATUS_ORDER
        if job_status == status
          return i
      return Consts.JOB_STATUS_ORDER.length
    isValidPhone: (phone) -> PhoneNumber.validate(phone)

    renderAddressLink: (address) ->
        loc = address.address
        if loc == null || loc.length == 0
            loc = address.lat + '+' + address.lng

        ReactDOMFactories.a
          href: 'http://maps.google.com/maps?z=12&t=m&q=loc:' + loc
          target: '_blank'
          className: 'pointercursor newtab'
          address.address

    getCorrectCasing: (list, word) ->
      ret = find(list, (item) ->
        item.toLowerCase() == word.toLowerCase()
      )
      if !ret?
        return Utils.toTitleCase(word)
      return ret

    getColor: (SettingStore, setting, def, transparent) ->
      setting = SettingStore.getSettingByKey(setting)
      color = def
      if !transparent?
        transparent = .5
      if setting?.value?
        color = setting.value
      if color == "transparent"
        return {}
      result = Utils.hexToRgb(color)
      return "rgba("+result.r+","+result.g+","+result.b+", "+transparent+")"

    downloadFile: (sUrl) ->
      if Utils.isIE()
        #window.location.assign(sUrl)
        win = window.open(sUrl, '_blank')
        if win?
          win.focus()
      else
        link = document.createElement('a')
        link.href = sUrl
        if link.download != undefined
          #Set HTML5 download attribute. This will prevent file from opening if supported.
          fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length)
          link.download = fileName
        #Dispatching click event.
        if document.createEvent
          e = document.createEvent('MouseEvents')
          e.initEvent 'click', true, true
          link.dispatchEvent e
          return true
        query = '?download'
        window.open sUrl + query

    isDoubleOverdue: (job) ->
      ret = Utils.getLastEtaObj(job)
      if ret? and job.toa.original
        original = moment(job.toa.original)
        return Math.ceil(ret.diff(moment(), "minutes", true)) <= -120 and Math.ceil(original.diff(moment(), "minutes", true)) <= -120
      return false

    # checks if the given site_id is from a Site (and not from a Place for instance)
    isSiteAndNotPlace: (SiteStore, siteId) ->
      if !siteId?
        return false

      if SiteStore.exists(siteId)
        return true

      return false

    formatAddressWithSite: (siteName, siteLocation) ->
      siteName + " " + siteLocation?.address

    hashString: (str) ->
      hash = 0

      for i in [0...str.length]
        hash += Math.pow(str.charCodeAt(i) * 31, str.length - i)
        hash = hash & hash; # Convert to 32bit integer

      return hash

    getSiteName: (CompanyStore, SiteStore, siteId, include_company = true, watcherId = null) ->
      if not siteId?
        return ""

      if include_company
        siteOwnerCompanyId = SiteStore.get(siteId, 'owner_company_id', watcherId)
        siteCompanyId = SiteStore.get(siteCompanyId, 'company_id', watcherId)

        if siteOwnerCompanyId?
          companyName = CompanyStore.get(siteOwnerCompanyId, 'name', watcherId)
        else
          companyName = CompanyStore.get(siteCompanyId, 'name', watcherId)

      siteName = SiteStore.get(siteId, 'name', watcherId)

      if companyName?
        return companyName + (if siteName? then " " + siteName else "")
      else
        return (if siteName? then " " + siteName else "")

    getName: (record) ->
        name = ""
        if(record.first_name?)
            name = record.first_name + " "
        if(record.last_name?)
            name += record.last_name
        name

    readableColorOnBG: (colour) ->
      if !colour?
        return "#000000"
      if colour.indexOf("#") >= -1
        hex = colour.replace(/#/, '')
        r = parseInt(hex.substr(0, 2), 16)
        g = parseInt(hex.substr(2, 2), 16)
        b = parseInt(hex.substr(4, 2), 16)
      else
        rgb = colour.substring(4, rgb.length-1).replace(/ /g, '').split(',')
        r   = parseInt(rgb[0], 16)
        g   = parseInt(rgb[1], 16)
        b   = parseInt(rgb[2], 16)

      brightness = [
          0.299 * r,
          0.587 * g,
          0.114 * b
      ].reduce((a, b) => a + b) / 255
      if brightness < .5
        return "#ffffff"
      else
        return "#000000"
    getJobStatusText: (UserStore, job) ->
      if Utils.isScheduled(UserStore, job)
        return "Scheduled"
      return job.status

    getTransformedJobStatus: (UserStore, job) ->
      #TODO: take in watcher
      if UserStore.isFleetManaged()
        if job.status == Consts.AUTO_ASSIGNING
          return Consts.PENDING
      return Utils.getJobStatusText(UserStore, job)

    getJobId: (job) ->
      if job?
        (if job.original_job_id? then job.original_job_id+":B" else job.id) + (if job.child_job? then ":A" else "")
      else
        ""

    getJobCreatedAt: (job) ->
      if job?.history?["Created"]?
        created = job.history["Created"].last_set
        if job.history["Created"].adjusted_dttm?
          created = job.history["Created"].adjusted_dttm
        return created

      return job?.created_at

    isInvoiceEditable: (invoice) ->
      #If created by partner let them edit it
      if UserStore.isPartner() && !invoice.recipient?.company?
        return true

      if invoice.state not in [
          Consts.IONSITEPAYMENT, Consts.ICREATED, Consts.IPARTNERNEW,
          Consts.IPARTNERAPPROVED,  Consts.IFLEETREJECTED,
          Consts.ISWOOPREJECTEDPARTNER, Consts.ISWOOPNEW
        ]
          return false
        #TODO: This code will never be executed.... figure out why it's here and simplify
        if invoice.state in [Consts.IONSITEPAYMENT] and invoice.job?.status in [Consts.COMPLETED, Consts.CANCELED, Consts.GOA]
          return false
      else if UserStore.isSwoop()
        if invoice.state in [Consts.IPARTNERAPPROVED, Consts.IPARTNERSENT, Consts.IONSITEPAYMENT, Consts.ISWOOPSENDINGFLEET, Consts.ISWOOPSENTFLEET, Consts.ISWOOPAPPROVEDPARTNER, Consts.ISWOOPDOWNLOADEDPARTNER]
          return false
      else if UserStore.isPartner() and invoice.state in [Consts.IPARTNERSENT] # and check if its fleet inhouse/fleet managed
        return false
      return true

    turnIntoClassName: (name) ->
      if name?
        return name.replace(/\./g, "").replace(/\ /g, "")
      return ""
    preventPropogation: (e) ->
        e.stopPropagation()

    getOrderString: (order) -> if order == 1 then "asc" else "desc"

    formatDate: (timeStr) ->
      moment(timeStr).format("MM/DD/YY")

    formatDateTime: (timeStr, forceDate) ->
        #if same day
        if not timeStr?
            return ""

        if forceDate? || moment().format("MM/DD/YY") != moment(timeStr).format("MM/DD/YY")
            return moment(timeStr).format("MM/DD/YY HH:mm")
        else
            return moment(timeStr).format("HH:mm")

    compare: (str1, str2, dir) ->
        if not str1 || typeof str1 == "object"
          return dir
        if not str2 || typeof str2 == "object"
          return dir * -1

        regex = /^\d+$/
        if regex.test(str1) and regex.test(str2) and !isNaN(parseFloat(str1)) and !isNaN(parseFloat(str2))
          str1 = parseFloat(str1)
          str2 = parseFloat(str2)

        if typeof str1 == "string"
          return str1.toLowerCase().localeCompare(str2.toLowerCase())*dir
        else
          return (str1 - str2)*dir

    sortBy: (property, a, b, dir) ->
        str1 = get(a, property)
        str2 = get(b, property)
        return @compare(str1, str2, dir)

    # @see https://stackoverflow.com/questions/9716468/is-there-any-function-like-isnumeric-in-javascript-to-validate-numbers
    isNumeric: (n) ->
      !isNaN(parseFloat(n)) && isFinite(n)

    # Deprecated: use UserStore.getEndpoint()
    getEndpoint: (UserStore) ->
      if UserStore.isRoot() or UserStore.isSwoopDispatcher()
        return 'root'
      if UserStore.isPartner()
        return 'partner'
      if UserStore.isFleet()
        return 'fleet'

    getEditPencilIcon: (onClickCallback) ->
      ReactDOMFactories.i
        key: 'edit_pencil_icon'
        className: 'fa fa-pencil with-margin-left inline'
        onClick: (e) -> onClickCallback(e)
        name: 'edit'

    getPaymentTypeSpan: (CustomerTypeStore, InvoiceStore, job, watcerId) ->
      CustomerTypeStore.get(job?.customer_type_id, "name", watcerId)
      if !name?
        name = job?.customer_type_name

      if name?
        if name == "Customer Pay" and !UserStore.isFleet()
          ReactDOMFactories.span
            key: 'payment_type_span'
            style:
              fontWeight: 900
              color: "#f00"
            name
        else
          name

    getQuestionResultSpan: (owner_company_name, questionResult) ->
      questionResult.question + " " + questionResult.answer + (if questionResult.answer_info? then ": " + questionResult.answer_info else "")

    getValueMoney: (obj, path) ->
      value = get(obj, path)
      if value?
        f = parseFloat(value)
        if not isNaN(f)
          return Utils.toFixed(f,2)
      return "0.00"
    hasKey: (obj, path) ->
        #TODO: could do this in a loop if will be nesting more than 1 level deep
        if obj? and path?
          name = path.split(".")
          if name.length == 1
            return has(obj, name[0])
          else if name.length == 2
            return has(obj, name[0]) and has(obj[name[0]], name[1])
          else if name.length == 3
            return has(obj, name[0]) and has(obj[name[0]], name[1]) and has(obj[name[0]][name[1]], name[2])
          else
            return has(obj, name[0]) and has(obj[name[0]], name[1]) and has(obj[name[0]][name[1]], name[2]) and has(obj[name[0]][name[1]][name[2]], name[3])

    setValue: (obj, path, val) ->
        #TODO: could do this in a loop if will be nesting more than 1 level deep
        if obj? and path?
          name = path.split(".")
          tempObj = obj
          for i in [0..name.length-1]
            p = name[i]
            #TODO: mayb convet numbers to ints here
            if i == name.length-1
              tempObj[p] = val
            else
              if not tempObj[p]
                tempObj[p] = {}
              tempObj = tempObj[p]

    isAssignedFromFleet: (job) ->
        if !job?
          return false
        return job.owner_company? and job.rescue_company? and job.rescue_company.id != job.owner_company.id

    formatDateTimeWithTimeZone: (timeStr, forceDate) ->
        #if same day
        if not timeStr?
            return ""

        if forceDate? || moment().format("MM/DD/YY") != moment(timeStr).format("MM/DD/YY")
            return moment(timeStr).format("MM/DD/YY HH:mm") + " " + @getTimezoneStr()
        else
            return moment(timeStr).format("HH:mm") + " " + @getTimezoneStr()

        #date=new Date(timeStr)
        #checkDate = new Date().getTime() - (1000*60*60*24)
        #ret = ""
#
        #minutes = date.getMinutes()
        #if minutes < 10
        #  minutes = "0" + minutes
        #ret += date.getHours()+":"+minutes
#
        #if date.getTime() > checkDate
        #    #if it is older than a day
        #    pad = (n) ->
        #      if n < 10
        #        "0"+n
        #      else
        #        n
#
        #    dateStr = pad(date.getDate())+"/"+pad(date.getMonth()+1)+"/"+(""+date.getFullYear()).substring(2)
        #    ret += " " + dateStr
        #return ret

    formatTime: (timeStr) ->
        return moment(timeStr).format("HH:mm")

    formatMinsInTotalHours:  (minsStr, keepZero) ->
      if keepZero and minsStr == 0 then return "0 hr"

      if minsStr == 0 then return undefined

      if minsStr < 60
        Utils.toFixed(minsStr, 0) + ' min'
      else
        hours = (minsStr / 60)
        rhours = Math.floor(hours)
        minutes = (hours - rhours) * 60
        rminutes = Math.round(minutes)

        (rhours + ' hr') + if minutes > 0 then (' ' + rminutes + ' min') else ''

    farInTheFuture: (timeStr) ->
      if !timeStr?
        return false
      return moment().diff(timeStr, 'hours') < -8

    formatMomentDuration: (duration) ->
      # TODO improve it to accept format
      plural = (val, text) ->
        s = if val > 1 then 's' else ''
        "#{val} #{text}#{s}"

      result = []

      if duration.days()
        result.push(plural(duration.days(), 'day'))

      if duration.hours()
        result.push(plural(duration.hours(), 'hour'))

      if duration.minutes()
        result.push(plural(duration.minutes(), 'min'))

      result.join(' ')

    create_UUID: ->
      dt = (new Date).getTime()
      uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) ->
        r = (dt + Math.random() * 16) % 16 | 0
        dt = Math.floor(dt / 16)
        (if c == 'x' then r else r & 0x3 | 0x8).toString 16
      )
      uuid

    formatNumber: (number, options) ->
      React.createFactory(injectIntl( createReactClass(
        render: () ->
          num = @props.intl.formatNumber(@props.value, {
              style: "currency"
              currency: "USD"
            })
          if not options? or not options.keepSymbol?
            num = num.replace("$", "")

          span null,
            num

      )))(key: number, value: number)

    formatMoney: (amount, currency = 'USD', precision = 2) ->
      amount = if isNaN(parseFloat(amount)) then 0 else amount
      amount = Utils.toFixed(amount, precision)

      Formatted = injectIntl ({ intl }) ->
        <span>
          {intl.formatNumber amount,
            style: 'currency'
            currency: currency}
        </span>

      <Formatted key={amount} />

    renderAddress: (address, prepend=null) ->
        a = @shortenAddress(address)
        if !prepend?
          prepend = ""
        else
          prepend += " "

        if not @isMobile()
            return prepend+a

        href = "google.navigation:q="+address+"&navigate=yes"

        if @isIOS()
            href = "maps://maps.apple.com?q="+address+"&navigate=yes"
        return ReactDOMFactories.a
            href: href
            onClick: (e) ->
                e.stopPropagation()
            prepend+a

    shortenAddress: (address) ->
        if address?
            address = address.replace(", United States", "")
                    .replace(", USA", "")

        #remove trailing zip code if it exists
        address

        #TODO: remove zip code if at end

    shortenLocationAddress: (location) ->
      if location.street and location.city and location.state
        "#{location.street}, #{location.city}, #{location.state}"
      else
        @shortenAddress(location.address)

    # Based on this: https://stackoverflow.com/a/31142914
    #
    # credits to:
    # author: Carlos Machado
    # version: 0.1
    # year: 2015
    #
    # It takes a dynamically created anchor and downloads the requested file
    # for IE11. Works with different domains, as long as the server supports
    # the Access-Control-Allow-Origin header.
    #
    # Tested with IE11 version 11.0.9600.18697 on Windows 7 Enterprise.
    #
    # example on how to created a anchor for this:
    #
    #   anchorLinkVariable = document.createElement("a")
    #   anchorLinkVariable.download = "whatever"
    #   anchorLinkVariable.href = <url of the file>
    #
    injectIEFileDownloadHack: (anchorLinkVariable, url) ->
      anchorLinkVariable.href = url
      anchorLinkVariable.addEventListener('click', (e) ->
        f_ref = @getAttribute("href")
        f_name = f_ref.slice(f_ref.lastIndexOf("/") + 1, f_ref.length)
        e.preventDefault()

        oReq1 = new XMLHttpRequest()
        oReq1.addEventListener("load", () ->
          blobObject = this.response
          window.navigator.msSaveBlob(blobObject, f_name)
        , false)
        oReq1.open("get", this, true)
        oReq1.responseType = 'blob'
        oReq1.send()
      , false)

    hexToRgb: (hex) ->
      shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i
      hex = hex.replace(shorthandRegex, (m, r, g, b) ->
          return r + r + g + g + b + b
      )
      result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
      if result?
        return {
          r: parseInt(result[1], 16)
          g: parseInt(result[2], 16)
          b: parseInt(result[3], 16)
        }
      return {
        r: 0
        g: 0
        b: 0
      }

    isIE: (userAgent) ->
        userAgent = userAgent || navigator.userAgent
        if userAgent?
          return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1
        else
          return false
    isChrome: -> /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)
    isSafari: ->  /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor)
    isWebkit: -> Utils.isChrome() || Utils.isSafari()

    getTimezoneStr: () ->
        return moment().format("z")

    isETA: (record) ->
      if record? and record.toa? and not record.scheduled_for? and record.toa.eta?
        return true
      return false
    getLastEtaObj: (record, use_scheduled=true) ->
        ret = null
        if record? and record.toa?
            if use_scheduled && record.scheduled_for?
              ret = moment(record.scheduled_for)
            else if record.toa.live?
              ret = moment(record.toa.live)
            else if !use_scheduled && record.scheduled_for
              ret = moment(record.scheduled_for)
            else if record.toa.latest?
              ret = moment(record.toa.latest)
        return ret


    getLastEta: (record) ->
        ret = @getLastEtaObj(record)
        if ret?
            return @formatDateTime(ret)
        return "-"

    wasRecent: (time) ->
      return Math.abs(moment(time) - moment()) < 5000

    getClosestSiteTo: (SiteStore, originLatLng, watcherId = null, properties = null) ->
      sites = filter(SiteStore.getFilteredSites(null, watcherId, properties), (site) ->
        site.location?.address? && (originLatLng.lat != site.location?.lat && originLatLng.lng != site.location?.lng))

      sitesLocations = map(sites, (site) -> site.location)

      sortBy(sitesLocations, (siteLocation) ->
        siteLatLng =
          lat: siteLocation.lat
          lng: siteLocation.lng

        Utils.calculateDistanceBetween(originLatLng, siteLatLng)
      )?[0]

    # Calculates distance between two lat/lng points.
    # This algorithm follows the Haversine Formula and is based on:
    # http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
    calculateDistanceBetween: (originLatLng, comparisionLatLng) ->
      RADIUS = 6371 # Radius of the earth in km

      originLat = originLatLng.lat
      originLng = originLatLng.lng

      comparisionLat = comparisionLatLng.lat
      comparisionLng = comparisionLatLng.lng

      dLat = @deg2rad(comparisionLat - originLat)
      dLon = @deg2rad(comparisionLng - originLng)

      haversine_calculus_a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(@deg2rad(originLat)) * Math.cos(@deg2rad(comparisionLat)) *
        Math.sin(dLon/2) * Math.sin(dLon/2)

      haversine_calculus = 2 * Math.atan2(Math.sqrt(haversine_calculus_a),
        Math.sqrt(1 - haversine_calculus_a))

      return RADIUS * haversine_calculus

    # helper function to Utils.calculateDistanceBetween
    deg2rad: (deg) ->
      return deg * (Math.PI/180)

    metersToMiles: (meters, decimalDigits = 1) ->
      number = Number(meters)

      if isNaN(number)
        return 0

      miles = number * 0.000621371192
      return Utils.toFixed(miles,decimalDigits)

    multipleEmailsValidation: (input, required) ->
      errorText = "Please Enter Valid Email(s)"
      if required && (!input? || input == '' || !Utils.areValidEmails(input))
        return errorText
      if !required && !Utils.areNonRequiredEmailsValid(input)
        return errorText

    areNonRequiredEmailsValid: (emails) =>
      if emails
        return Utils.areValidEmails(emails)
      return true

    areValidEmails: (emails) ->
      if !emails
        emails = ''
      emailsArray = emails.split(/[,;]/)

      validatedEmails = map(emailsArray, (email) -> Utils.isValidEmail(email))

      reduce(validatedEmails, ((memo, validationResult) -> validationResult && memo), true)


    isValidEmail: (email) ->
        re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return (not email?) || email == "" || re.test(email.trim())

    isIOS: () ->
        return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream
    isMobile: () ->
      return (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
    handleJobChange: (JobStore, id, e) ->
      obj = {
        id: id
      }
      obj[e.target.name] = e.target.value
      JobStore.jobChanged(obj)
      return

    isScheduled: (UserStore, job) ->
      if job.scheduled_for != null
        if UserStore.isPartner() and (job.status in [Consts.PENDING,  Consts.UNASSIGNED, Consts.REASSIGN, Consts.ACCEPTED])
          return true
        else if (UserStore.isFleet() or UserStore.isSwoop()) and (job.status in [Consts.ACCEPTED, Consts.PENDING])
          return true



    showModal: (key, e, props) ->
      Dispatcher.send("show_modal",
        key: key
        props: props
      )

      e?.preventDefault?()
      e?.stopPropagation?()

    ignoreEvents: (e) ->
      e.preventDefault()
      e.stopPropagation()

    showInvoice: (invoice, e) ->
      if !invoice?
        Rollbar.error("Attempt to show an undefined invoice")
      else
        win = window.open("/ticket/"+invoice.uuid, '_blank')
        if win?
          win.focus()
      e?.preventDefault()
      e?.stopPropagation()

    transliterate: (c) ->
      return '0123456789.ABCDEFGH..JKLMN.P.R..STUVWXYZ'.indexOf(c) % 10

    get_check_digit: (vin) ->
      theMap = '0123456789X'
      weights = '8765432X098765432'
      sum = 0
      i = 0
      while i < 17
        sum += @transliterate(vin[i]) * theMap.indexOf(weights[i])
        ++i

      return theMap[sum % 11]

    getParamsByName: (name, url) ->
      if !url
        url = window.location.href
      name = name.replace(/[\[\]]/g, '\\$&')
      regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)')
      results = regex.exec(url)
      if !results
        return null
      if !results[2]
        return ''
      decodeURIComponent results[2].replace(/\+/g, ' ')

    validateVin: (vin) ->
      if vin?.length != 17
        return false
      @get_check_digit(vin) == vin[8]

    isCashCall: (job) ->
      if job?
        if job.account_id == null || !job.account? || job.account.name == "Cash Call"
          return true
      return false

    nextState: (job) ->
        switch job.status
          when Consts.DISPATCHED
            return Consts.ENROUTE
          when Consts.ENROUTE
            return Consts.ONSITE
          when Consts.ONSITE
            if job.drop_location?
              return Consts.TOWING
            return Consts.COMPLETED
          when Consts.TOWING
            return Consts.TOWDESTINATION
          when Consts.TOWDESTINATION
            return Consts.COMPLETED

    addMissingIds: (obj1, obj2) ->
      if typeof obj1 is "object" && typeof obj2 is "object" and obj1 != null and obj2 != null and obj1.constructor != Array
        if not obj1.id?
          for prop of obj1
            @addMissingIds(obj1[prop], obj2[prop])
          obj1.id = obj2.id
    compareObjs: (newObj, obj1, obj2) ->

      if obj1 and obj1.constructor == Array
        #if obj is an array (TODO: currently this assumes all primitive values in an array will fail for arrays of objects)

        for elem1 in obj1
          if typeof elem1 is "object"
            if not elem1.id?
              if not newObj?
                newObj = []
              newObj.push(elem1)
            else
              if obj2? and obj2.constructor == Array
                for elem2 in obj2
                  if not elem2? or elem1.id == elem2.id
                    ret = @compareObjs({}, elem1, elem2)
                    if not isEmpty(ret)
                      ret.id = elem2.id
                      if not newObj?
                        newObj = []
                      newObj.push(ret)
                    break
          else
            if not obj2 || difference(obj1, obj2).length > 0 || difference(obj2, obj1).length > 0
              newObj = obj1
      else
        #if obj1 is an object
        for prop of obj1
          if typeof obj1[prop] is "object" && obj1[prop] != null
            if typeof newObj[prop] is "undefined"
              if obj1[prop].constructor == Array
                newObj[prop] = @compareObjs(null, obj1[prop], obj2?[prop])
                if newObj[prop] == null
                  delete newObj[prop]
              else
                newObj[prop] = @compareObjs({}, obj1[prop], obj2?[prop])
                if isEmpty(newObj[prop]) and newObj[prop].constructor == Object
                  delete newObj[prop]

          else
            if (typeof obj2 == "undefined" or obj2 == null) or (typeof obj2[prop] == "undefined" or obj2[prop] == null and obj1[prop] != null) || obj1[prop] != obj2[prop]
              newObj[prop] = obj1[prop]
              if newObj[prop] == ""
                newObj[prop] = null
      return newObj

    getVehicleName: (job) ->
      if !job?
        return ""
      vehicle_info = ""
      vinfo_arr = [job.make, job.model, job.year, job.color]
      for i in vinfo_arr
        if i?
          vehicle_info += i + " "
      vehicle_info

    toTitleCase: (str) ->
      return str.replace(/\w\S*/g, (txt) ->
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
      )

    carouselImgURL: (photoURL, w, h, t, x) ->
      if Consts.JS_ENV == 'local'
        return photoURL
      w = '&w=' + w || '' # desired image width, number
      h = '&h=' + h || '' # desired image height, number
      t = '&t=square' || '' # crops image square, bool
      x = ' 2x' || '' # srcset resolution, bool
      return '//images.weserv.nl/?url=' + photoURL + w + h + t + x

    #This is needed because IE doesn't support the below code
    #dateWithouthSecond.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'})
    #formatTimeHHMMA: (d) ->
    #    z = (n) ->
    #        if n < 10
    #            '0'+n
    #        else
    #            n
    #    ampm = (h) ->
    #        if h < 12
    #            'AM'
    #        else
    #            'PM'
#
#
    #    h = d.getHours()
    #    return (h%12 || 12) + ':' + z(d.getMinutes()) + ' ' + ampm(h)
}
# if window?
#   window.Utils = Utils
module.exports = Utils
