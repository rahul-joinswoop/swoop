import React from 'react'
import BaseComponent from 'components/base_component'

const getDisplayName = WrappedComponent =>
  WrappedComponent.displayName || WrappedComponent.name || 'Component'

export default (WrappedComponent, mapStateToProps, options = {}) => {
  class StoreWrapper extends BaseComponent {
    constructor(props) {
      super(props)
      // TODO - why do we bind here? seems like it would be better
      // to be totally functional here and not depend on context to get what
      // we want? otherwise the same mapStateToProps with the same input could
      // end up with different output
      this.mapStateToProps = mapStateToProps.bind(this)
      if (options && options.constructor) {
        options.constructor.call(this, props)
      }
    }

    componentDidMount() {
      super.componentDidMount()
      if (options && options.componentDidMount) {
        options.componentDidMount.call(this)
      }
    }

    componentWillUnmount() {
      super.componentWillUnmount()
      if (options && options.componentWillUnmount) {
        options.componentWillUnmount.call(this)
      }
    }

    static displayName = `StoreWrapper(${getDisplayName(WrappedComponent)})`

    render() {
      const { props } = this
      super.render()
      return (
        <WrappedComponent
          {...props}
          {...this.mapStateToProps(props)}
          wrapperRev={this.state.rev}
        />
      )
    }
  }

  return StoreWrapper
}
