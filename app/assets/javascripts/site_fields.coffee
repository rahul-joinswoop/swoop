UserStore = require('stores/user_store')
Fields = require 'fields/fields'

module.exports =
  NameField: class NameField extends Fields.Field
    _name: 'name'
    _label: 'Site Name'
    _required: true

  LocationTypeField: class LocationTypeField extends Fields.SelectField
    _name: 'location.location_type_id'
    _label: 'Location Type'
    isShowing: ->
      if @render_props?.hideLocationType == true
        return false
      return true
    getOptions: ->
      @optionMapper(
        UserStore.getUser().company.location_types,
        [{ value: null, text: "- Select -" }]
      )

  LocationField: class LocationField extends Fields.GeoSuggestField
    _name: 'location'
    _label: 'Address or Lat Long Coordinates'
    _hideMap: false
    _required: true

  PhoneField: class PhoneField extends Fields.PhoneField
    _name: "phone"
    _label: "Phone"
    allowDisableMasking: true

  SiteCodeField: class SiteCodeField extends Fields.Field
    _name: 'site_code'
    getLabel: UserStore.siteCodeLabel
    isRequired: ->
      UserStore.isEnterprise() || UserStore.isTesla() ? true : false
