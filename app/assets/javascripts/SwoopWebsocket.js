import ReconnectingWebSocket from 'reconnecting-websocket'

import Logger from 'lib/swoop/logger'

const SwoopWebsocket = {
  connect(url) {
    Logger.debug('Connecting to ws: ', url)
    this.ws = new ReconnectingWebSocket(url, [], {
      debug: false,
    })

    this.ws.onmessage = (message) => {
      if (this?.onmessage) {
        this.onmessage(message)
      } else {
        Logger.debug('No onmessage defined', this)
      }
    }

    this.ws.onopen = () => this?.onopen?.()

    this.ws.onerror = (error) => {
      Logger.debug('ws error occured 1', error)
      this?.onerror?.(error)
    }

    this.ws.onclose = (close) => {
      Logger.debug('ws close occured 1', close)
      this?.onclose?.(close)
    }

    return this.ws
  },
}
export default SwoopWebsocket
