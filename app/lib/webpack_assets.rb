# frozen_string_literal: true

# Helper class for accessing asset paths from webpack manifest.
class WebpackAssets

  # This is the manifest webpack writes when it's done building
  WEBPACK_ASSETS_PATH = (Rails.root + 'public' + 'assets' + 'webpack-assets.json').freeze

  attr_reader

  class << self

    def instance
      # The manifest will change in development, so read it every time, but in production
      # it will be static so let's just cache it.
      if Rails.env.production?
        @instance ||= new
      else
        new
      end
    end

  end

  def initialize
    @payload = MultiJson.load(manifest_json).freeze
  end

  def js(bundle)
    Array(@payload.dig(bundle, 'js')).freeze
  end

  def css(bundle)
    Array(@payload.dig(bundle, 'css')).freeze
  end

  def manifest_json
    if WEBPACK_ASSETS_PATH.exist?
      WEBPACK_ASSETS_PATH.read
    else
      "{}"
    end
  end

end
