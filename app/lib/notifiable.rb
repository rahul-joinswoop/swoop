# frozen_string_literal: true

module Notifiable

  # this is a god object that gives access to trigger various notifications (extended below).
  # pattern copied from Subscribable
  #
  # after_commit { Notifiable.notify_job_assigned self }

  extend EtaAccepted
  extend EtaRequested
  extend FleetInHouseJobAssigned
  extend FleetManagedJobAssigned
  extend GeofenceCompleted
  extend GeofenceOnsite
  extend GeofenceTowDestination
  extend GeofenceTowing
  extend JobAssigned
  extend JobCanceled
  extend JobDispatched
  extend JobLocationChanged
  extend JobReassigned
  extend JobServiceTypeChanged

end
