# frozen_string_literal: true

module LoggableJob

  # TODO - this should be extend, right?
  include ActiveSupport::Concern

  def log_msg(message, **options)
    prefix = ''
    options.each_pair do |k, v|
      prefix = "#{prefix}#{k.upcase}(#{v}) "
    end

    "#{prefix}Origin:#{self.class.name} Message:#{message}"
  end

  module_function :log_msg

  def log(level, message, **options)
    Rails.logger.send level, log_msg(message, options)
  end

end
