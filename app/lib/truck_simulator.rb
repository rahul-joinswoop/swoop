# frozen_string_literal: true

class TruckSimulator

  # we currently get location updates every 15s so that's how often we make updates.
  # the lower this is the more our route on maps will actually align with streets / etc
  UPDATE_FREQUENCY_SECONDS = 15.0

  class << self

    # given a truck_id, a destination, and optional waypoints, simulate the location activity we'd normally see for a truck
    # in the real world. we use google's location api to figure out a start and end point for each UPDATE_FREQUENCY_SECONDS
    # chunk (this isn't exact but it's what we'd get from customers)
    def simulate(truck:, destination:, waypoints: [], origin: nil, duration: nil)
      # if we don't have an origin use our truck's current location
      origin ||= truck

      # call google
      response = External::GoogleDirectionsService.get origin, destination, waypoints

      # iterate through the directions response and create a route to our destination.
      routes = response['routes']
      route = routes.first
      legs = route['legs']

      # compute our scaling factor
      time = 0.0
      total_time = legs.reduce(0.0) { |memo, cur| memo + cur.dig('duration', 'value').to_f }
      duration ||= total_time
      scale = total_time / duration.to_f

      # get redis stuff setup
      directions_id = "#{self}:#{SecureRandom.uuid}"
      redis = RedisClient[:default]

      # make sure our truck's current location is the same as the starting location for our
      # computed route
      current = [legs.dig(0, 'start_location', 'lat'), legs.dig(0, 'start_location', 'lng')]
      if current[0] && current[1] && truck.lat != current[0] || truck.lng != current[1]
        UpdateVehicleLocationWorker.perform_async(truck.company_id, truck.id, current[0], current[1], Time.current)
      end

      while legs.present?
        leg = legs.shift
        steps = leg['steps']
        while steps.present?
          step = steps.shift
          duration_s = step['duration']['value'].to_f
          distance_m = step['distance']['value'].to_f
          speed = distance_m / duration_s
          # speed is in m/s, now convert to km/s because that's what our distance code uses
          speed /= 1000.0
          # potentialy scale our speed
          speed *= scale
          # decode our polylines
          points = FastPolylines::Decoder.decode(step['polyline']['points'])
          while points.present?
            prev = current
            current = points.shift
            delta = Geocoder::Calculations.distance_between(prev, current, units: :km)
            # TODO - sometimes this overshoots UPDATE_FREQUENCY_SECONDS by a large amount,
            # depends on the length of this segment on our polyline. we could be clever
            # and interpolate so that we correctly show motion on the map but whatever, this
            # is close enough for a first pass.
            time += delta / speed
            if time > UPDATE_FREQUENCY_SECONDS
              # if we pass an edge of UPDATE_FREQUENCY_SECONDS stash our current coordinates in redis and reset our
              # time so we start computing the next step
              redis.pipelined do |pipeline|
                pipeline.rpush(directions_id, MultiJson.dump(current))
                pipeline.expire(directions_id, (duration + 600).to_i)
              end
              time = 0.0
            end
          end
        end
      end

      # if we have a remaining segment make sure to push it into our list
      if time != 0
        redis.pipelined do |pipeline|
          pipeline.rpush(directions_id, MultiJson.dump(current))
          pipeline.expire(directions_id, (duration + 600).to_i)
        end
      end

      # start our recursive worker
      TruckSimulatorWorker.perform_async(truck.id, directions_id)
    end

  end

end
