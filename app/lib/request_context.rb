# frozen_string_literal: true

# This class is used to track information parsed from the request that needs to be made
# accessible to classes that do not have direct access to the request object. The context
# is preserved to Sidekiq workers as well.
class RequestContext

  attr_reader :uuid, :user_id, :source_application_id

  class << self

    # Return the current application that has been set in scope by the `use` method.
    def current
      Thread.current[:swoop_request_context] || new
    end

    # Set an application as being the current application inside the provide block.
    def use(context, &block)
      saved_value = Thread.current[:swoop_request_context]
      begin
        Thread.current[:swoop_request_context] = context
        yield
      ensure
        Thread.current[:swoop_request_context] = saved_value
      end
    end

    private

    def mobile_app_platform(request)
      match = request.user_agent.to_s.match(MOBILE_APP_MATCHER)
      platform = match["platform"].downcase if match
      if platform == "ios"
        SourceApplication::PLATFORM_IOS
      elsif platform == "android"
        SourceApplication::PLATFORM_ANDROID
      else
        nil
      end
    end

    def mobile_app_version(request)
      match = request.user_agent.to_s.match(MOBILE_APP_MATCHER)
      match["version"] if match
    end

  end

  def initialize(uuid: nil, user_id: nil, source_application_id: nil)
    @uuid = uuid
    @user_id = user_id.to_i if user_id.present?
    @source_application_id = source_application_id.to_i if source_application_id.present?
  end

  def user_id=(value)
    @user = nil
    @user_id = value
  end

  def source_application_id=(value)
    @source_application = nil
    @source_application_id = value
  end

  def source_application
    if @source_application_id.present?
      @source_application ||= SourceApplication.find_by(id: @source_application_id)
    else
      nil
    end
  end

  def user
    if user_id
      @user ||= User.find_by(id: user_id)
    else
      nil
    end
  end

  def empty?
    uuid.blank? && user_id.blank? && source_application_id.blank?
  end

end
