# frozen_string_literal: true

# common logger functionality used by Subscribable and Notifiable
module CommonLogger

  def add_class_logger(logger_klass, *methods)
    logger_name = logger_klass.name
    methods.each do |m|
      # Rename original method
      send(:alias_method, "#{m}_orig", m)

      # Redefine old method with instrumentation code added
      define_method m do |*args|
        callee = nil
        caller.each do |c|
          Rails.application.config.autoload_paths.each do |p|
            if c.start_with?(p) && !c.match(/#{logger_name.downcase}/)
              callee = c.sub(/#{p}\//, '').sub(/\.rb.*\z/, '')
              break
            end
          end
          break if callee
        end
        ids = []
        klass = nil
        arg = args.first
        if arg.is_a?(::ActiveRecord::Relation)
          # TODO - this breaks two specific specs if i enable it, i have no idea
          # why? some sort of schroedinger's query, if i do anything to arg it's
          # enough to cause the specs to fail. for now this means we won't have
          # ids of multiple model calls, oh well
          #   ids = arg.map(&:id)
          klass = arg.first.class if arg.first.present?
        elsif arg.is_a?(::ActiveRecord::Base)
          ids = [arg.id]
          klass = arg.class
        end

        ActiveSupport::Notifications.instrument "#{m}.#{logger_name.downcase}", callee: callee, ids: ids, klass: klass do
          send "#{m}_orig", *args
        end
      rescue => e
        Rails.logger.error "#{m}.#{logger_name.downcase}: Caught #{e}"
      end

      ActiveSupport::Notifications.subscribe "#{m}.#{logger_name.downcase}" do |name, started, finished, unique_id, data|
        klass = data[:klass]
        ids = data[:ids].present? ? "(#{data[:ids].join(",")})" : ""
        duration_ms = "(#{(1000 * (finished - started)).round(2)}ms)"
        from = data[:callee].present? ? "from #{data[:callee].classify} " : ""
        Rails.logger.debug "#{logger_name}.#{m}(#{klass}#{ids}) called #{from}#{duration_ms}"
      end
    end
  rescue => e
    Rails.logger.error "#{m}.#{logger_name.downcase}: Caught #{e}"
  end

end
