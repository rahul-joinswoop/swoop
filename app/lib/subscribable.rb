# frozen_string_literal: true

module Subscribable

  # this is a god object that gives access to trigger various graphql subscriptions (extended below).
  # initially these were included directly into various models but it resulted in some weird
  # bugs that i couldn't resolve (associated records wouldn't update without a reload) and i gave
  # up and did it this way instead since this method resolves Subscribable when after_commit
  # fires. usage is something like this in a model:
  #
  # after_commit { Subscribable.trigger_user_updated user }

  extend AuctionBidStatusChanged
  extend JobDocumentChanged
  extend JobUpdated
  extend JobStatusChanged
  extend PartnerVehicleChanged
  extend UserUpdated

end
