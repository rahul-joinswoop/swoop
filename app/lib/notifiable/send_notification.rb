# frozen_string_literal: true

module Notifiable
  module SendNotification

    include LoggableJob

    def send_notification_to(job:, sms_msg:, users:, job_status_name:, also_send_to_secondary_phone: false)
      users.each do |user|
        if user.phone
          send_to(job, user.phone, user, sms_msg, job_status_name)
        else
          log :debug, "has no phone", job: job.id, user: user.id
        end

        if also_send_to_secondary_phone
          # user.pager is exposed as secondary_phone in the UI
          if user.pager
            send_to(job, user.pager, user, sms_msg, job_status_name)
          else
            log :debug, "has no secondary phone", job: job.id, user: user.id
          end
        end
      end
    end

    def filter_users_for(rescue_company:, supported_roles:)
      rescue_company.users.not_deleted
        .joins(:roles, :notification_settings).where(roles: { name: supported_roles })
        .select(:id, :phone).distinct
    end

    def send_to(job, user_phone, user, sms_msg, job_status_name)
      if user.call_notification_enabled_by?(job_status_name: job_status_name)
        Job.new_job_call(user_phone)

        log :debug, "phone call triggered", job: job.id, user: user.id
      else
        log :debug, "phone call disabled by user.user_notification_settings", job: job.id, user: user.id
      end

      if user.sms_notification_enabled_by?(job_status_name: job_status_name)
        Job.send_sms(user_phone, sms_msg, user_id: user.id, job_id: job.id, type: 'JobAuditSms')

        log :debug, "new sms triggered", job: job.id, user: user.id
      else
        log :debug, "sms disabled by user.user_notification_settings", job: job.id, user: user.id
      end
    end

  end
end
