# frozen_string_literal: true

module Notifiable::JobLocationChanged

  extend Notifiable::Links
  extend Notifiable::Logger

  def self.build_title(job)
    # Location Changed: [Service] • [Account]
    "Location Changed: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  def self.build_msg
    'Review job details'
  end

  def self.build_link(job)
    my_job_link job
  end

  add_logger def trigger_job_location_changed(job)
    if job.present? && job.service_location_id.present?
      Notification::JobLocationChangedWorker.perform_async(job.id)
    end
  end

end
