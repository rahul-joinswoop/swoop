# frozen_string_literal: true

module Notifiable
  module Links

    def dispatch_job_link(job)
      "swoopapp://dispatchjob/#{job.to_ssid}"
    end

    def dispatch_jobs_link
      "swoopapp://dispatchjobs"
    end

    def my_job_link(job)
      "swoopapp://myjob/#{job.to_ssid}"
    end

    def my_jobs_link
      "swoopapp://myjobs"
    end

  end
end
