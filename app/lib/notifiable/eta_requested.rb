# frozen_string_literal: true

module Notifiable
  module EtaRequested

    extend Notifiable::Links
    extend Notifiable::Logger

    # we build our message here because maybe we'd send it off via sms as well?
    def self.build_title(job)
      #  ETA Request: [Service] • [Account]
      "ETA Request: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
    end

    def self.build_msg
      "Review job details and submit an ETA"
    end

    def self.build_link(job)
      dispatch_job_link job
    end

    def self.build_sms_msg(job)
      client_name = job.fleet_managed_job? ? 'Swoop' : job.fleet_company.name

      "Swoop (#{job.id}): ETA Request • #{job.service_code.name} • #{client_name} • Review job details and submit an ETA."
    end

    add_logger def trigger_eta_requested(arg)
      if arg.is_a?(::FleetMotorClubJob) && arg.assigned? && arg.rescue_company_id.present?
        Notification::EtaRequestedWorker.perform_async(arg.id)
      elsif arg.is_a?(::Auction::Bid) && arg.status == ::Auction::Bid::REQUESTED
        Notification::EtaRequestedWorker.perform_async(nil, arg.id)
      else
        Rails.logger.debug("Notifiable::EtaRequested(#{arg.id}) arg.saved_changes=#{arg.saved_changes.inspect}, fell through with #{arg&.class&.name}(#{arg&.id}")
      end
    end

  end
end
