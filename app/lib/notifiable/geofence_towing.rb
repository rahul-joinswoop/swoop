# frozen_string_literal: true

module Notifiable::GeofenceTowing

  extend Notifiable::Links
  extend Notifiable::Logger

  def self.build_title(job)
    "Towing: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  def self.build_msg
    'Job marked Towing because you left Pickup'
  end

  def self.build_link(job)
    my_job_link job
  end

  add_logger def trigger_geofence_towing(job)
    if job.present? && job.rescue_driver.present?
      Notification::GeofenceTowingWorker.perform_async(job.id)
    end
  end

end
