# frozen_string_literal: true

module Notifiable
  module FleetManagedJobAssigned

    extend Notifiable::Links
    extend Notifiable::Logger

    # we build our message here because maybe we'd send it off via sms as well?
    def self.build_title(job)
      # New Job: [Service] • [Account]
      "New Job: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
    end

    def self.build_msg
      "Review job details and submit an ETA"
    end

    def self.build_link(job)
      dispatch_job_link job
    end

    add_logger def trigger_fleet_managed_job_assigned(job)
      if job.present? && job.is_a?(::FleetManagedJob) && job.assigned? && job.rescue_company_id.present?
        Notification::FleetManagedJobAssignedWorker.perform_async(job.id)
      end
    end

  end
end
