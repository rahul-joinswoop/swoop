# frozen_string_literal: true

module Notifiable
  module EtaAccepted

    extend Notifiable::Links
    extend Notifiable::Logger

    # we build our message here because maybe we'd send it off via sms as well?
    def self.build_title(job)
      #  ETA Accepted: [Service] • [Account]
      "ETA Accepted: #{service_and_account_names(job)}"
    end

    def self.build_sms_msg(job)
      "Swoop (#{job.id}): ETA Accepted • #{service_and_account_names(job)} • Review job details and assign a driver."
    end

    def self.build_msg
      'Dispatch a driver'
    end

    def self.service_and_account_names(job)
      [job.service_name, job.account_name].reject(&:blank?).join(" • ")
    end

    def self.build_link(job)
      dispatch_job_link job
    end

    add_logger def trigger_eta_accepted(arg)
      job_id = if arg.is_a?(::FleetMotorClubJob) && arg.accepted? && arg.rescue_company_id.present?
                 arg.id
               elsif arg.is_a?(::Auction::Bid) && arg.status == ::Auction::Bid::WON
                 # handle an auction
                 arg.job_id
               end

      return if job_id.blank?

      Notification::EtaAcceptedWorker.perform_async(job_id)
    end

  end
end
