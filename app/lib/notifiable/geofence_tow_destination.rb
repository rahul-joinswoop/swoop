# frozen_string_literal: true

module Notifiable::GeofenceTowDestination

  extend Notifiable::Links
  extend Notifiable::Logger

  def self.build_title(job)
    'Tow Destination: ' + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  def self.build_msg
    'Job marked Tow Destination because you are at drop off.'
  end

  def self.build_link(job)
    my_job_link job
  end

  add_logger def trigger_geofence_tow_destination(job)
    if job.present? && job.rescue_driver.present?
      Notification::GeofenceTowDestinationWorker.perform_async(job.id)
    end
  end

end
