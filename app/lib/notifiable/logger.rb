# frozen_string_literal: true

module Notifiable::Logger

  include CommonLogger

  def add_logger(*methods)
    add_class_logger Notifiable, *methods
  end

end
