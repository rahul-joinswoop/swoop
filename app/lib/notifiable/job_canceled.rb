# frozen_string_literal: true

module Notifiable::JobCanceled

  extend Notifiable::Links
  extend Notifiable::Logger

  def self.build_title(job)
    # Job Canceled: [Service] • [Account]
    "Job Canceled: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  def self.build_msg
    'Job removed from your list'
  end

  def self.build_link(job, role)
    role == :dispatcher ? dispatch_jobs_link : my_jobs_link
  end

  add_logger def trigger_job_canceled(job)
    if job.present? && job.canceled?
      Notification::JobCanceledWorker.perform_async(job.id)
    end
  end

end
