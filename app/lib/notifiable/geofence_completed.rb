# frozen_string_literal: true

module Notifiable::GeofenceCompleted

  extend Notifiable::Links
  extend Notifiable::Logger

  SUPPORTED_LOCATION_STRINGS = ['Drop Off', 'Pickup'].freeze

  def self.build_title(job)
    'Completed: ' + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  def self.build_msg(location)
    "Job marked Completed because you left #{location}"
  end

  def self.build_link(job)
    my_job_link job
  end

  add_logger def trigger_geofence_completed_without_dropoff(job)
    trigger_geofence_completed(job, 'Pickup')
  end

  add_logger def trigger_geofence_completed_with_dropoff(job)
    trigger_geofence_completed(job, 'Drop Off')
  end

  def trigger_geofence_completed(job, location_departed)
    if SUPPORTED_LOCATION_STRINGS.exclude?(location_departed)
      raise ArgumentError, "'Drop Off' or 'Pickup' required!"
    end

    if job.present? && job.rescue_driver.present?
      Notification::GeofenceCompletedWorker.perform_async(job.id, location_departed)
    end
  end

  private :trigger_geofence_completed

end
