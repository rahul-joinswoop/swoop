# frozen_string_literal: true

module Notifiable::JobAssigned

  include LoggableJob

  extend Notifiable::Links

  def self.build_sms_msg(job)
    "Swoop (#{job.id}): New Job • #{job.service_code&.name} • #{job.account&.name} • Review job details and submit an ETA."
  end

  def trigger_job_assigned(job)
    if job&.assigned? && job.rescue_company
      log :debug, "About to schedule Notification::JobAssignedWorker", job: job.id

      Notification::JobAssignedWorker.perform_async(job.id)
    end
  end

end
