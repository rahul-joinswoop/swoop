# frozen_string_literal: true

module Notifiable::GeofenceOnsite

  extend Notifiable::Links
  extend Notifiable::Logger

  def self.build_title(job)
    "On Site: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  def self.build_msg
    'Job marked On Site because you are at Pickup'
  end

  def self.build_link(job)
    my_job_link job
  end

  add_logger def trigger_geofence_onsite(job)
    if job.present? && job.rescue_driver.present?
      Notification::GeofenceOnsiteWorker.perform_async(job.id)
    end
  end

end
