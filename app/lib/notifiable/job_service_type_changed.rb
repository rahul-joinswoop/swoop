# frozen_string_literal: true

module Notifiable::JobServiceTypeChanged

  extend Notifiable::Links
  extend Notifiable::Logger

  def self.build_title(job)
    # Service Type Changed: [Service] • [Account]
    "Service Type Changed: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  def self.build_msg
    'Review job details'
  end

  def self.build_link(job)
    my_job_link job
  end

  add_logger def trigger_job_service_type_changed(job)
    if job.present? && job.service_code.present?
      Notification::JobServiceTypeChangedWorker.perform_async(job.id)
    end
  end

end
