# frozen_string_literal: true

module Notifiable::JobReassigned

  extend Notifiable::Links
  extend Notifiable::Logger

  def self.build_title(job)
    # Job Reassigned: [Service] • [Account]
    "Job Reassigned: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  def self.build_msg
    'Job removed from your list'
  end

  def self.build_link(job)
    my_jobs_link
  end

  add_logger def trigger_job_reassigned(job, rescue_driver_id)
    if job.present? && rescue_driver_id.present?
      Notification::JobReassignedWorker.perform_async(job.id, rescue_driver_id)
    end
  end

end
