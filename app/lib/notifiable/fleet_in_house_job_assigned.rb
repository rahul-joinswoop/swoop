# frozen_string_literal: true

module Notifiable
  module FleetInHouseJobAssigned

    extend Notifiable::Links
    extend Notifiable::Logger

    # we build our message here because maybe we'd send it off via sms as well?
    def self.build_title(job)
      # New Job: [Service] • [Account]
      "New Job: " + [job.service_name, job.account_name].reject(&:blank?).join(" • ")
    end

    def self.build_msg
      "Review job details and assign a driver"
    end

    def self.build_link(job)
      dispatch_job_link job
    end

    add_logger def trigger_fleet_in_house_job_assigned(job)
      if job.present? && job.is_a?(::FleetInHouseJob) && job.assigned? && job.rescue_company_id.present?
        Notification::FleetInHouseJobAssignedWorker.perform_async(job.id)
      end
    end

  end
end
