# frozen_string_literal: true

module Notifiable::JobDispatched

  extend Notifiable::Links
  extend Notifiable::Logger

  REVIEW_AND_GO_EN_ROUTE = 'Review job details and go En Route'

  def self.build_title(job)
    # Job Dispatched: [Service] • [Account]
    "Job Dispatched: " + service_and_account_names(job)
  end

  def self.build_msg
    REVIEW_AND_GO_EN_ROUTE
  end

  def self.build_sms_msg(job)
    "Swoop (#{job.id}): Job Dispatched • #{job.service_code&.name} • #{job.account&.name} • #{REVIEW_AND_GO_EN_ROUTE}."
  end

  def self.build_link(job)
    my_job_link job
  end

  def self.service_and_account_names(job)
    [job.service_name, job.account_name].reject(&:blank?).join(" • ")
  end

  add_logger def trigger_job_dispatched(job)
    if job.present? && job.dispatched? && job.rescue_driver_id.present?
      Notification::JobDispatchedWorker.perform_async(job.id)
    end
  end

end
