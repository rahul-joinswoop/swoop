# frozen_string_literal: true

module Subscribable::JobUpdated

  extend Subscribable::Logger
  ERROR = "Fell through in #{name}#trigger_job_updated"

  add_logger def trigger_job_updated(arg)
    delta = {}
    finder = []
    if arg.is_a? ActiveRecord::Relation
      finder = arg
    elsif arg.is_a?(::Job)
      delta = arg.saved_changes.deep_symbolize_keys.slice(:rescue_driver_id, :rescue_company_id)
      finder = ::Job.where(id: arg.id)
    else
      if arg.present?
        Rollbar.error ERROR, { arg: arg }
        Rails.logger.error "#{ERROR} with arg #{arg.inspect}"
      end
      # we don't raise here because we don't want to block the transaction
      finder = ::Job.none
    end

    # The job update worker can have severe problems if too many ids are passed in, so group into smaller batches.
    finder.filter_active.order(updated_at: :desc).pluck(:id).each_slice(25) do |ids|
      GraphQL::JobUpdatedWorker.perform_async({ ids: ids, delta: delta }.deep_stringify_keys)
    end
  end

end
