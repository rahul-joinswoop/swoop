# frozen_string_literal: true

module Subscribable::Logger

  include CommonLogger

  def add_logger(*methods)
    add_class_logger Subscribable, *methods
  end

end
