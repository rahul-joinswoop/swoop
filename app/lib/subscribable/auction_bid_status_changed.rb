# frozen_string_literal: true

module Subscribable::AuctionBidStatusChanged

  extend Subscribable::Logger

  ERROR = "Fell through in #{name}#trigger_auction_bid_status_changed"

  add_logger def trigger_auction_bid_status_changed(arg)
    if arg.present?
      if arg.is_a?(::Auction::Bid)
        delta = arg.saved_changes.deep_symbolize_keys.slice(:status)
        GraphQL::AuctionBidStatusChangedWorker.perform_async(arg.id, delta) if delta.present?
      else
        Rollbar.error ERROR, { arg: arg }
        Rails.logger.error "#{ERROR} with arg #{arg.inspect}"
      end
    end
  end

end
