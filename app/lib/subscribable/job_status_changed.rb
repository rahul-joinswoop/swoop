# frozen_string_literal: true

module Subscribable::JobStatusChanged

  extend Subscribable::Logger

  ERROR = "Fell through in #{name}#trigger_job_status_changed"

  add_logger def trigger_job_status_changed(arg)
    if arg.present?
      if arg.is_a?(::Job)
        delta = arg.saved_changes.deep_symbolize_keys.slice(:status, :rescue_driver_id, :rescue_company_id)
        GraphQL::JobStatusChangedWorker.perform_async(arg.id, delta) if delta.present?
      else
        Rollbar.error ERROR, { arg: arg }
        Rails.logger.error "#{ERROR} with arg #{arg.inspect}"
      end
    end
  end

end
