# frozen_string_literal: true

module Subscribable::PartnerVehicleChanged

  extend Subscribable::Logger

  ERROR = "Fell through in #{name}#trigger_partner_vehicle_changed"

  add_logger def trigger_partner_vehicle_changed(arg)
    if arg.present? && arg.is_a?(::RescueVehicle)
      GraphQL::PartnerVehicleChangedWorker.perform_async(arg.id)
    else
      Rollbar.error ERROR, { arg: arg }
      Rails.logger.error "#{ERROR} with arg #{arg.inspect}"
    end
  end

end
