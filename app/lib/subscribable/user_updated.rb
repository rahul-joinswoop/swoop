# frozen_string_literal: true

module Subscribable::UserUpdated

  extend Subscribable::Logger

  ERROR = "Fell through in #{name}#trigger_user_updated"

  add_logger def trigger_user_updated(arg)
    if arg.is_a? ActiveRecord::Relation
      arg
    elsif arg.is_a?(::User)
      ::User.where(id: arg.id)
    else
      if arg.present?
        Rollbar.error ERROR, { arg: arg }
        Rails.logger.error "#{ERROR} with arg #{arg.inspect}"
      end
      ::User.none
    end.not_deleted.select(:id).find_in_batches(batch_size: 100) do |users|
      GraphQL::UserUpdatedWorker.perform_async(*users.map(&:id))
    end
  end

end
