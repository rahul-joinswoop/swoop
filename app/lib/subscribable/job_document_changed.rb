# frozen_string_literal: true

module Subscribable::JobDocumentChanged

  extend Subscribable::Logger

  ERROR = "Fell through in #{name}#trigger_job_document_changed"

  add_logger def trigger_job_document_changed(arg)
    if arg.is_a? ::AttachedDocument
      if arg.job.present?
        GraphQL::JobDocumentChangedWorker.perform_async(arg.id)
      end
    else
      if arg.present?
        Rollbar.error ERROR, { arg: arg }
        Rails.logger.error "#{ERROR} with arg #{arg.inspect}"
      end
    end
  end

end
