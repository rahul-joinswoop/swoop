# frozen_string_literal: true

# Centralized logic to authenticate a user/company from an access token.
# The access token will be returned from doorkeeper using the OAuth2 spec.
class SwoopAuthenticator

  # This request header can be passed on company level access tokens to specify
  # the user that the request should be considered for. This allows external applications
  # to use the API at a company level, but still invoke endpoints as a user for
  # authorization and auditing purposes.
  AUTHENTICATED_USER_HEADER = "HTTP_X_AUTHENTICATED_USER"

  # Property that caches the authenticator in the request env.
  SWOOP_AUTHENTICATOR = "swoop.authenticator"

  COMPANY_SCOPE = "company"
  FLEET_ROLE = "fleet"
  RESCUE_ROLE = "rescue"
  SUPER_FLEET_MANAGED_ROLE = "super_fleet_managed"
  CUSTOMER_ROLE = "customer"

  # Mapping of implicit roles for an access token scope
  # TODO - i don't think we use dispatcher/driver/admin on the fleet side? not sure, we should verify
  ALL_FLEET_ROLES = [FLEET_ROLE, "admin", "dispatcher", "driver"].freeze
  ALL_RESCUE_ROLES = [RESCUE_ROLE, "admin", "dispatcher", "driver"].freeze
  ALL_SUPER_FLEET_MANAGED_ROLES = [SUPER_FLEET_MANAGED_ROLE, *ALL_FLEET_ROLES].freeze

  class InvalidTokenError < StandardError
  end

  class << self

    # Get an authenticator for the current request. Subsequent calls to this
    # method will return the same authenticator so that the logic to lookup
    # token, user, and company does not need to happen again.
    def for(request)
      authenticator = request.env[SWOOP_AUTHENTICATOR]
      unless authenticator
        authenticator = new(request: request)
        request.env[SWOOP_AUTHENTICATOR] = authenticator
      end
      authenticator
    end

    # Clear the memoized authenticator for a request. It will be reloaded the next
    # time you call `for(request)`.
    def clear(request)
      request.env.delete(SWOOP_AUTHENTICATOR)
    end

    # Instantiate an authenticator from a GraphQL context. This is used when deserializing
    # a context to push to a subscription and is used to ensure the the authentication
    # and authorization for the context is kept in sync with any changes after the subscription
    # is created.
    def from_context(context)
      context = context.with_indifferent_access if context.is_a?(Hash) && !context.is_a?(HashWithIndifferentAccess)
      new(access_token: context[:api_access_token], company: context[:api_company], user: context[:api_user])
    end

    # Authenticate a user from request parameters.
    def authenticate_user(request)
      request.env["devise.allow_params_authentication"] = true
      user = nil
      if request.params[:email].present? || request.params[:username].present?
        user = authenticate_user_login(request.params[:email], request.params[:username], request.params[:password])
      elsif request.params[:token].present?
        user = authenticate_user_token(request.params[:token])
      end

      if user
        AnalyticsData::Identify.new(user).call
        AnalyticsData::LogIn.new(
          user_agent: request.headers["User-Agent"],
          referer: request.headers["Referer"],
          api_user: user
        ).call
        Rails.logger.debug("Authenticate user: success #{user.id}")
      else
        Rails.logger.debug("Authenticate user: failed")
      end

      user
    end

    # Dereference scopes on the token into roles. If the scopes are just "fleet" or "rescue",
    # then assume they should have all roles. Similarly, if the scope is just "company",
    # return all roles for the company type. If there are additional scopes, then assume they
    # are just a list of roles, but still dereference "company" to the correct role based on
    # company type.
    def scope_roles(scopes, company)
      # TODO what if we don't have a company?
      if scopes == [CUSTOMER_ROLE]
        [CUSTOMER_ROLE]
      elsif scopes == [SUPER_FLEET_MANAGED_ROLE]
        ALL_SUPER_FLEET_MANAGED_ROLES
      elsif scopes == [FLEET_ROLE]
        # TODO what if our company isn't a fleet company
        ALL_FLEET_ROLES
      elsif scopes == [RESCUE_ROLE]
        # TODO what if our company isn't a rescue company
        ALL_RESCUE_ROLES
      elsif scopes.include?(COMPANY_SCOPE)
        roles = scopes.reject { |scope| scope == COMPANY_SCOPE }
        # we don't support super_fleet_managed here - you have to explicitly ask for it
        if company.fleet?
          if roles.empty?
            roles = ALL_FLEET_ROLES
          else
            roles << FLEET_ROLE
          end
        elsif company.rescue?
          if roles.empty?
            roles = ALL_RESCUE_ROLES
          else
            roles << RESCUE_ROLE
          end
        end
        roles.uniq
      else
        scopes
      end
    end

    # Force using the factory methods to instantiate instances.
    private :new

    private

    def authenticate_user_login(email, username, password)
      # TODO the queries on username/email should be case insensitive and should
      # be smart enough to dynamically determine if the field is an email or username
      # based on the presence of and @ symbol.
      user = nil
      if email.present?
        user = User.find_by(email: email.strip, deleted_at: nil)
      elsif username.present?
        user = User.find_by(username: username.strip, deleted_at: nil)
      end

      if user && !user.valid_password?(password)
        Rails.logger.debug "Invalid password for user #{user.id}"
        nil
      else
        user
      end
    end

    def authenticate_user_token(token)
      token = SingleUseAccessToken.use(token)
      if token && token.user && token.user.deleted_at.nil?
        token.user
      else
        nil
      end
    end

  end

  def initialize(request: nil, access_token: nil, user: nil, company: nil)
    if request
      if access_token || user || company
        raise ArgumentError, "if request is provided, it must be the only argument"
      end
    elsif access_token.nil?
      raise ArgumentError, "request or access_token is required"
    end

    @request = request

    # Important: these instance variables must set be inside blocks since they are only lazy
    # loaded if the variable has not bee previously set.
    if access_token
      @access_token = access_token
    end
    if company
      @company = company
    end
    if user
      @user = user
    end
  end

  # Return true if the access token is valid.
  def authenticated?
    access_token.present? && access_token.accessible?
  end

  # Return the doorkeeper access token for the request.
  def access_token
    unless defined?(@access_token)
      token = Doorkeeper::OAuth::Token.authenticate(@request, *Doorkeeper.configuration.access_token_methods)
      @access_token = token if token&.accessible?
    end
    @access_token
  end

  # Return the user for the request if the token represents a user or
  # if the token is a company wide token and the X-Authenticated-User
  # request header is set.
  def user
    unless defined?(@user)
      token = access_token
      if token.nil?
        @user = nil
      elsif token.includes_scope?(COMPANY_SCOPE) || token.resource_owner_id.nil?
        request_user_id = @request.env[AUTHENTICATED_USER_HEADER] if @request
        company_id = company_id_from_token(token) if request_user_id.present?
        if request_user_id.present? && company_id.present?
          # TODO need to decide on source of user ids exposed to and passed from external system (ssid or external id)
          # TODO should be found from cache like other requests
          token_user = SomewhatSecureID.load(request_user_id)
          if token_user && token_user.company_id == company_id
            @user = token_user
          else
            raise InvalidTokenError, "user not found for company token"
          end
        else
          @user = nil
        end
      else
        token_user = User.find_by_id_in_cache(token.resource_owner_id)
        if token_user
          @user = token_user
        else
          raise InvalidTokenError, "user not found"
        end
      end
    end
    @user
  end

  # Return the company associated with the token.
  def company
    unless defined?(@company)
      token = access_token
      if token.nil?
        @company = nil
      elsif user
        @company = user.company
      else
        company_id = company_id_from_token(token)
        if company_id
          token_company = Company.find_by_id_in_cache(company_id)
          if token_company
            @company = token_company
          else
            raise InvalidTokenError, "company not found"
          end
        else
          @company = nil
        end
      end
    end
    @company
  end

  # Return the oauth application for the token. This can be nil or an application
  def application
    access_token&.application
  end

  # Return the GraphQL viewer for the token. This can be a user, company, or application
  # depending on the token type.
  def viewer
    if user
      user
    elsif access_token.nil?
      nil
    elsif access_token.includes_scope?(COMPANY_SCOPE)
      company
    else
      access_token.application
    end
  end

  # Return the list of roles associated with the token. If there is a user,
  # then this will be the list of roles for that user. If there is not a user,
  # then this will be a list of all roles for the company.
  def roles
    unless defined?(@roles)
      roles = []
      if user
        # if our user has roles then use them. otherwise try our access token if possible
        # TODO - this doesn't let us issue access tokens with fewer privs than the user
        # has.
        if user.roles.present?
          roles = user.roles.map(&:name)
        elsif access_token.scopes.present?
          roles = scope_roles(access_token.scopes)
        end
      elsif company
        scopes = access_token.scopes.to_a
        if scopes.empty? && access_token.application
          scopes = access_token.application.scopes.to_a
        end
        roles = scope_roles(scopes)
      end
      @roles = roles
    end
    @roles
  end

  # Helper method to determine if any of the specified role is in the list of
  # roles for the authenticated user/company.
  def has_role?(required_role)
    roles.include?(required_role.to_s)
  end

  # Helper method to determine if any of the specified roles matches the list of
  # roles for the authenticated user/company.
  def has_any_role?(*required_roles)
    required_roles.any? { |role| roles.include?(role.to_s) }
  end

  # Helper method to determine if all of the specified roles match the list of
  # roles for the authenticated user/company.
  def has_all_roles?(*required_roles)
    required_roles.present? && required_roles.all? { |role| roles.include?(role.to_s) }
  end

  # Returns true the access token belongs to an application with the :rescue scope.
  def partner_application?
    scopes = application&.scopes
    scopes.present? && scopes.include?(RESCUE_ROLE)
  end

  # Returns true the access token belongs to an application with the :fleet scope.
  def client_application?
    scopes = application&.scopes
    scopes.present? && scopes.include?(FLEET_ROLE)
  end

  def super_client_managed_application?
    scopes = application&.scopes
    scopes.present? && scopes.include?(SUPER_FLEET_MANAGED_ROLE)
  end

  private

  # Look up the company id from a company level token. If there is no resource owner
  # on the token, then the company is the owner of the application. If there is a resource
  # owner and the token has the company scope, then this is a company level token authorized
  # by an admin user.
  def company_id_from_token(token)
    if token.resource_owner_id.nil?
      if token.application && token.application.owner_type == Company.name
        return token.application.owner_id
      end
    elsif token.includes_scope?(COMPANY_SCOPE)
      admin_user = User.select([:id, :company_id, :deleted_at]).includes(:roles).find_by(id: token.resource_owner_id)
      if admin_user && admin_user.deleted_at.nil? && admin_user.admin?
        return admin_user.company_id
      end
    end
    nil
  end

  def scope_roles(scopes)
    self.class.scope_roles(scopes, company)
  end

end
