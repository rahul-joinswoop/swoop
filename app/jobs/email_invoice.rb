# frozen_string_literal: true

class EmailInvoice

  include LoggableJob
  include Utils

  def initialize(invoice_id, invoice_pdf_id, to: nil, cc: nil, bcc: nil)
    @invoice_id = invoice_id
    @invoice_pdf_id = invoice_pdf_id
    @to = to
    @cc = cc
    @bcc = bcc
  end

  def emailable?(invoice)
    customized_recipient_email? || invoice.emailable?
  end

  def customized_recipient_email?
    !(@to.blank? && @cc.blank? & @bcc.blank?)
  end

  def accounting_to_email(invoice)
    return @to if customized_recipient_email?
    invoice.accounting_to_email
  end

  def accounting_cc_email(invoice)
    return @cc if customized_recipient_email?
    invoice.accounting_cc_email
  end

  def accounting_bcc_email(invoice)
    return @bcc if customized_recipient_email?
    invoice.accounting_bcc_email
  end

  def perform
    invoice = Invoice.find_by!(id: @invoice_id)
    if invoice.recipient_type == "Account"

      if emailable?(invoice)
        sender = invoice.sender
        sender_email = sender.accounting_email_or_dispatch_email

        subject = "Invoice from #{sender.name} for Job ID: #{invoice.job_id}"

        if invoice.paid
          subject = "Paid Invoice from #{sender.name} for Job ID: #{invoice.job_id}"
        end

        if invoice.job.po_number
          subject += " | PO: #{invoice.job.po_number}"
        end

        args = {
          to: accounting_to_email(invoice),
          from: sender_email,
          sender: SWOOP_OPERATIONS_EMAIL,
          template_name: 'invoice',
          subject: subject,
        }

        cc = accounting_cc_email(invoice)
        args[:cc] = cc if cc.present?

        args[:bcc] = [ENV['INVOICE_BCC_EMAIL']]
        bcc = accounting_bcc_email(invoice)
        args[:bcc] << bcc if bcc.present?

        if sender_email
          sender_accounting_email = sender.accounting_email
          args[:reply_to] = sender_accounting_email
          args[:bcc] << sender_accounting_email
        end

        args[:locals] = {
          :@invoice => invoice,
          :@logo => sender.logo_url,
          :@invoice_pdf_id => @invoice_pdf_id,
        }

        mail = SystemMailer.send_mail(
          args,
          nil,
          invoice.job,
          invoice.recipient_client_company,
          "Invoice",
          invoice
        )
        mail.deliver_now
      else
        log :warn, "Not sending invoice to null email for #{invoice.job.inspect}", job: invoice.job.id
      end
    else
      msg = "Attempt to send invoice with non account - #{invoice.inspect}"
      log :error, msg, job: invoice.job.id

      Rollbar.error(StandardError.new(msg))
    end
  end

end
