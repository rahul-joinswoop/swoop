# frozen_string_literal: true

# Sends an email to both the Partner and the Fleet to notify them that
# one Site (+ RescueProvider) of the Partner has enabled dispatch to a Fleet, by the Fleet.
# It also sends the email to sales@joinswoop.com
# @example Delayed::Job.enqueue EmailFleetEnabledDispatchToSite.new(
#            fleet_company_id, rescue_provider_id, user_id)
class EmailFleetEnabledDispatchToSite

  include Utils

  SWOOP_SALES_EMAIL = 'sales@joinswoop.com'

  # @attr fleet_company_id the id of the fleet company that has added the site
  # @attr rescue_company_id the id of the rescue provider that has been added to the fleet
  # @attr fleet_user_id the id of the fleet user who has added the rescue provider to the fleet
  def initialize(fleet_company_id, rescue_provider_id, fleet_user_id)
    raise ArgumentError if [fleet_company_id, rescue_provider_id, fleet_user_id].include? nil

    @fleet_company_id = fleet_company_id
    @rescue_provider_id = rescue_provider_id
    @fleet_user_id = fleet_user_id
  end

  def perform
    @fleet_company = Company.find(@fleet_company_id)
    @rescue_provider = RescueProvider.find(@rescue_provider_id)
    @rescue_company = @rescue_provider.provider
    @fleet_user = User.find(@fleet_user_id)

    SystemMailer.send_mail(
      email_args,
      @fleet_user,
      nil,
      @fleet_company,
      'Site Added by Fleet',
      nil
    ).deliver_now
  end

  private

  def email_list
    [SWOOP_SALES_EMAIL, @fleet_company.support_email, @rescue_company.dispatch_email].compact
  end

  def subject
    "#{@fleet_company.name} has added #{@rescue_provider.provider.name} #{@rescue_provider.site.name} as a dispatchable site"
  end

  def email_args
    email_args = {
      to: email_list,
      from: swoop_operations_email_as('Swoop'),
      subject: subject,
      template_name: 'fleet_enabled_dispatch_to_site',
      locals: {
        :@fleet_company => @fleet_company,
        :@rescue_provider => @rescue_provider,
        :@fleet_user => @fleet_user,
      },
    }

    if @fleet_company.support_email.present?
      email_args[:reply_to] = @fleet_company.support_email
    end

    email_args
  end

end
