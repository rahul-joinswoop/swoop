# frozen_string_literal: true

# Swoop root will receive new job phone alert for FleetManaged jobs
# if not dispatched (partner assigned). Frequency set in
# FREQUENCY_TO_ALERT_SWOOP_IF_JOB_NOT_DISPATCHED constant.
#
# @see https://swoopme.atlassian.net/browse/ENG-4444
class SendSwoopAlertIfFleetManagedJobNotDispatched

  include LoggableJob

  FREQUENCY_TO_ALERT_SWOOP_IF_JOB_NOT_DISPATCHED = 30.minutes

  def initialize(job_id)
    @job_id = job_id
  end

  def perform
    @job = Job.find(@job_id)

    if @job.blank?
      log :debug, "SYSTEM_ALERT [SendSwoopAlertIfFleetManagedJobNotDispatched async job] - No job found for id: #{@job_id}", job: @job_id

      return
    end

    if !@job.assigned_to_partner?
      swoop_root_users = SystemEmailRecipientCalculator.new(@job).calculate

      swoop_root_users.each do |root|
        log :debug, "SYSTEM_ALERT - Not Dispatched escalation - queueing phone #{root.full_name}:#{root.phone}", job: @job.id

        Job.delay.not_dispatched_job_call(root.phone)
      end
    else
      log :debug,
          "SYSTEM_ALERT not queueing phone call on SendSwoopAlertIfFleetManagedJobNotDispatched, " \
          "because partner is assigned: #{@job.rescue_company.id}",
          job_id: @job_id
    end
  end

end
