# frozen_string_literal: true

class StorageInvoices < BatchBase

  include LoggableJob
  include Utils

  def perform
    log :debug, "StorageInvoices::perform called"

    Job.not_deleted
      .eager_load(:invoice, :storage)
      .joins(:invoice, :storage)
      .left_joins(:inventory_items)
      .where(status: JobStatus::NAMES[:STORED], inventory_items: { deleted_at: nil })
      .joins(invoice: :line_items)
      .where(invoicing_line_items: { deleted_at: nil, description: InvoicingLineItem::STORAGE, auto_calculated_quantity: true })
      .find_each(batch_size: 100) do |job|
      log_command job
      log :debug, "processing", job_id: job.id

      JobError.with_job_errors(job, JobError::CREATE_OR_UPDATE_INVOICE) do |job_errors|
        if StorageTick.new(job).call
          job.invoice.save!
        end
        log_command_applied
      rescue ArgumentError => e
        raise JobError::JobException.new("Unable storage tick invoice due to #{e}", true)
      end
    end
  end

  def success()
  end

end
