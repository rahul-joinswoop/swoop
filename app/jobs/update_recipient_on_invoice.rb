# frozen_string_literal: true

class UpdateRecipientOnInvoice

  include LoggableJob
  include Utils

  def initialize(job_id)
    log :debug,
        "UpdateRecipientOnInvoice initialize called",
        job_id: job_id

    @job_id = job_id
    job = Job.find(@job_id)

    raise ArgumentError, "UpdateRecipientOnInvoice: Invoice not found" unless job.invoice
    raise ArgumentError, "UpdateRecipientOnInvoice: Invoice not editable" unless job.invoice.editable?
  end

  def perform
    log :debug,
        "UpdateRecipientOnInvoice perform called",
        job_id: @job_id

    job = Job.find(@job_id)
    PartnerChangeRecipientOnInvoice.new(job).call
    job.invoice.save!
    job.save!
  end

end
