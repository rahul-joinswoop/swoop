# frozen_string_literal: true

module Stripe
  class EmailRefundFailed

    include Utils

    def initialize(company_id, job_id, refund_date_in_secs)
      @company_id  = company_id
      @job_id      = job_id
      @refund_date = DateTime.strptime(refund_date_in_secs.to_s, '%s')
    end

    def perform
      company = Company.find(@company_id)

      to = [STRIPE_SUPPORT_EMAIL_ADDRESS, company.accounting_email]

      SystemMailer.send_mail(
        {
          template_name: 'stripe_refund_failed',
          to: to,
          from: swoop_operations_email_as('Swoop'),
          subject: "Swoop Refund Failed - #{@job_id}",
          locals: { :@company => company, :@job_id => @job_id, :@refund_date => @refund_date },
        }, nil, nil, company, "StripeRefundFailed", nil
      ).deliver_now
    end

  end
end
