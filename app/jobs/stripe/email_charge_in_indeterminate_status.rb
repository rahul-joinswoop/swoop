# frozen_string_literal: true

module Stripe
  class EmailChargeInIndeterminateStatus

    include Utils

    def initialize(charge_id)
      @charge_id = charge_id
    end

    def perform
      charge = InvoicePaymentCharge.find(@charge_id)

      to = [STRIPE_SUPPORT_EMAIL_ADDRESS]

      SystemMailer.send_mail(
        {
          template_name: 'stripe_charge_issue',
          to: to,
          from: swoop_operations_email_as('Swoop'),
          subject: "Stripe Charge Issue - Action Required",
          locals: { :@charge => charge },
        }, nil, nil, charge.invoice.sender, "StripeChargeIssue", nil
      ).deliver_now
    end

  end
end
