# frozen_string_literal: true

module Stripe
  class EmailChargeDisputeCreated

    include Utils

    def initialize(invoice_payment_charge_id)
      @invoice_payment_charge_id = invoice_payment_charge_id
    end

    def perform
      charge = InvoicePaymentCharge.find(@invoice_payment_charge_id)

      to = [STRIPE_SUPPORT_EMAIL_ADDRESS]

      SystemMailer.send_mail(
        {
          template_name: 'stripe_charge_dispute_created',
          to: to,
          from: swoop_operations_email_as('Swoop'),
          subject: "Stripe Charge Disputed - Job ##{charge.invoice.job_id}",
          locals: { :@charge => charge },
        }, nil, nil, Company.swoop, "StripeChargeDisputeCreated", nil
      ).deliver_now
    end

  end
end
