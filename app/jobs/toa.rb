# frozen_string_literal: true

class Toa

  include LoggableJob
  include Utils

  def enqueue
  end

  def perform
    log :debug, "TOA::perform called"

    updated_count = 0
    Job.where(status: "En Route", deleted_at: nil).find_each do |job|
      log :debug, "TOA processing", job_id: job.id

      JobError.with_job_errors(job, JobError::GOOGLE_MATRIX_NO_RESULTS) do |job_errors|
        if job.updated_at > 1.day.ago
          location_updated_at = job&.rescue_vehicle&.location_updated_at
          if location_updated_at.present? && (location_updated_at > 1.hour.ago) && job.service_location
            log :debug, "calculating", job_id: job.id

            origin = Location.new(lat: job.rescue_vehicle.lat, lng: job.rescue_vehicle.lng)
            destination = job.service_location

            if origin && origin.has_coords? && destination && destination.has_coords?
              meters, seconds = External::GoogleDistanceMatrixService.calculate(origin, destination).first

              if !meters.nil? && !seconds.nil?
                TimeOfArrival.create!(
                  job: job,
                  time: Time.now + seconds,
                  eba_type: TimeOfArrival::ESTIMATE,
                  vehicle: job.rescue_vehicle,
                  lat: job.rescue_vehicle.lat,
                  lng: job.rescue_vehicle.lng,
                  source_application_id: RequestContext.current.source_application_id
                )
                Job.send_track_technician_sms_and_save(job.id)
                updated_count += 1
              else
                log :debug,
                    "unable to calc TOA because of nil meters or seconds",
                    job_id: job.id

                raise JobError::JobException.new("Unable to calculate En Route time because no estimates returned from Google for truck origin:(#{origin.lat},#{origin.lng}) and job service location:(#{job.service_location.lat},#{job.service_location.lng}),#{job.service_location.try(:name)}", true)
              end
            else
              log :debug,
                  "missing origin,destination or coords",
                  job_id: job.id
            end
          else
            log :debug,
                "not calculating, no rv, rv not updated in last hour or no service location",
                job_id: job.id
          end
        else
          log :debug,
              "job in ETA for more than 1 day",
              job_id: job.id
        end
      end
    rescue => e
      log :error, "TOA failed: #{e.inspect}", job_id: job.id
      Rollbar.error(e)
    end

    Rails.logger.info("TOA updated ETA's on #{updated_count} jobs")
  end

  def success
  end

end
