# frozen_string_literal: true

class EmailAlert

  include Utils

  def initialize(to, subject, msg, target)
    @to = to
    @target = target
    @subject = subject
    @msg = msg
  end

  def perform
    SystemMailer.send_mail({
      template_name: 'alert',
      to: @to,
      from: swoop_operations_email_as('Swoop'),
      subject: @subject,
      locals: { :@msg => @msg, :@target => @target },
    }, nil, nil, Company.swoop, "Alert", nil).deliver_now
  end

end
