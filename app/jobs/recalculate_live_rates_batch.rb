# frozen_string_literal: true

class RecalculateLiveRatesBatch

  include LoggableJob

  attr_reader :from_rate
  attr_reader :to_rate

  def initialize(from_rate_id, to_rate_id)
    raise ArgumentError, "RATES RecalculateLiveRates - from rate nil" unless from_rate_id
    raise ArgumentError, "RATES RecalculateLiveRates - to rate nil" unless to_rate_id
    # TODO - we handle both AR objects and ids here but after this deploys we'll only ever receive ids
    # so we can remove these checks
    @from_rate = from_rate_id.is_a?(Rate) ? from_rate_id : Rate.find(from_rate_id)
    @to_rate = to_rate_id.is_a?(Rate) ? to_rate_id : Rate.find(to_rate_id)
    log :debug, "RATES INVOICE RecalculateLiveRates called with #{@from_rate.id} #{@to_rate.id}"
  end

  def perform
    log :debug, "RATES INVOICE RecalculateLiveRates called with #{@from_rate.id} #{@to_rate.id}"

    Job.find_by_sql([
      "select jobs.*
      from jobs
        left join invoicing_ledger_items on jobs.invoice_id=invoicing_ledger_items.id
        left join invoicing_line_items on invoicing_line_items.ledger_item_id = invoicing_ledger_items.id
      where
       invoicing_line_items.rate_id = ? and
       invoicing_ledger_items.state='partner_new' and
       invoicing_ledger_items.deleted_at is null and
       invoicing_line_items.job_id=jobs.id
      group by jobs.id
      order by jobs.id desc
      ", @from_rate.id,
    ]).each do |job|
      log :debug,
          "RATES INVOICE RecalculateLiveRates called with #{@from_rate.id} #{@to_rate.id}",
          job_id: job.id

      JobError.with_job_errors(job,
                               JobError::CREATE_OR_UPDATE_INVOICE) do |job_errors|
        Job.transaction do
          if job.invoice&.new_state?
            UpdateInvoiceForJob.new(job.invoice,
                                    job,
                                    job.get_rate).call
            job.invoice.save!
          end
        end
      rescue ArgumentError => e
        raise JobError::JobException.new("Unable to update invoice due to #{e}",
                                         true)
      end
    end
  end

end
