# frozen_string_literal: true

# Swoop root will receive new job phone alert for FleetManaged jobs
# until a root_dispatcher has been assigned or root_dispatcher is unassigned. Frequency set in
# FREQUENCY_TO_ALERT_SWOOP_DISPATCHERS_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET constant.
#
# @see https://swoopme.atlassian.net/browse/ENG-4410
class SendSwoopDispatchersAlertIfNoRootDispatcher

  include LoggableJob

  FREQUENCY_TO_ALERT_SWOOP_DISPATCHERS_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET = 3.minutes

  REPEAT_MAX = 5

  def initialize(job_id, repeat_counter = 1)
    @job_id = job_id
    @repeat_counter = repeat_counter
  end

  def perform
    @job = Job.find(@job_id)

    if @job.blank?
      log :debug, "SYSTEM_ALERT [SendSwoopDispatchersAlertIfNoRootDispatcher async job] - No job found", job: @job.id

      return
    end

    if can_system_alert_swoop_if_root_dispatcher_has_never_been_set?
      swoop_root = SystemEmailRecipientCalculator.new(@job).calculate

      swoop_root.each do |root|
        log :debug, "SYSTEM_ALERT Queueing phone #{root.full_name}:#{root.phone}", job: @job_id

        Job.delay.new_job_call(root.phone)
      end

      if @repeat_counter < REPEAT_MAX
        next_round = @repeat_counter + 1

        log :debug,
            "SYSTEM_ALERT Requeueing SendSwoopDispatchersAlertIfNoRootDispatcher (round #{next_round}) to run in " \
            "#{FREQUENCY_TO_ALERT_SWOOP_DISPATCHERS_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET} seconds",
            job_id: @job_id

        Delayed::Job.enqueue(
          SendSwoopDispatchersAlertIfNoRootDispatcher.new(@job_id, next_round),
          run_at: FREQUENCY_TO_ALERT_SWOOP_DISPATCHERS_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET.from_now
        )
      else
        log :debug,
            "SYSTEM_ALERT Stopping SendSwoopDispatchersAlertIfNoRootDispatcher, it reached the limit of #{REPEAT_MAX} runs",
            job_id: @job_id
      end
    else
      log :debug,
          "SYSTEM_ALERT Stop queueing SendSwoopDispatchersAlertIfNoRootDispatcher, " \
          "root_dispatcher: #{@job.root_dispatcher_id}, has_or_had_a_root_dispatcher_assigned? #{@job.has_or_had_a_root_dispatcher_assigned}",
          job_id: @job_id
    end
  end

  private

  def can_system_alert_swoop_if_root_dispatcher_has_never_been_set?
    @job.can_system_alert_swoop? && !@job.has_or_had_a_root_dispatcher_assigned? && @job.root_dispatcher_id.blank?
  end

end
