# frozen_string_literal: true

# Sends an email to both the Partner and the Fleet to notify them that one Rate has been changed.
# @example Delayed::Job.enqueue EmailRateChangeToPartnerAndFleet.new(
#            company_id_that_changed_the_rate, company_id_that_will_be_notified)
class EmailRateChangeToPartnerAndFleet

  include LoggableJob
  include Utils

  # @attr company_id the id of the company that changed the rate; can be the partner or the fleet;
  # @attr company_id_to_be_notified the company that will be mailed; can be the partner or the fleet;
  # @attr user_id the id of the user who changed the rate
  def initialize(company_id, company_id_to_be_notified, user_id)
    log :debug, "EmailRateChangeToPartnerAndFleet called"

    @company_id = company_id
    @company_id_to_be_notified = company_id_to_be_notified
    @user_id = user_id
  end

  def perform
    @company_that_changed_the_rate = Company.find(@company_id)
    @company_to_be_notified = Company.find(@company_id_to_be_notified)
    @user_that_changed_the_rate = User.find(@user_id)

    SystemMailer.send_mail(
      args,
      @user_that_changed_the_rate,
      nil,
      @company_that_changed_the_rate,
      'Rate changed by Partner or Fleet',
      nil
    ).deliver_now
  end

  private

  def email_list
    [@company_that_changed_the_rate, @company_to_be_notified].map(&:accounting_email).compact
  end

  def subject
    "#{@company_that_changed_the_rate.name} has edited #{@company_to_be_notified.name} rates"
  end

  def args
    email_args = {
      to: email_list,
      from: swoop_operations_email_as('Swoop'),
      subject: subject,
      template_name: 'rate_change_to_partner_and_fleet',
      locals: {
        :@company_that_changed_the_rate => @company_that_changed_the_rate,
        :@company_that_will_be_notified => @company_to_be_notified,
        :@user_that_changed_the_rate => @user_that_changed_the_rate,
      },
    }

    if @company_that_changed_the_rate.accounting_email.present?
      email_args[:reply_to] = @company_that_changed_the_rate.accounting_email
    end

    email_args
  end

end
