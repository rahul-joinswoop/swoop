# frozen_string_literal: true

class EmailRateChange

  include Utils

  SUBJECT = 'Rate Change'

  def initialize(rate_id, user_ids)
    @user_ids = Array(user_ids)
    @rate_id = rate_id
  end

  def perform
    users = User.find(@user_ids)
    rate = Rate.find_by!({ id: @rate_id })
    emails = self.emails(users)

    SystemMailer.send_mail({
      template_name: 'rate_change',
      to: emails,
      from: swoop_operations_email_as('Swoop'),
      subject: SUBJECT,
      locals: { :@rate => rate },
    }, nil, nil, rate.company, SUBJECT, rate).deliver_now
  end

end
