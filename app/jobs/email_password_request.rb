# frozen_string_literal: true

# Sends a new password reset link, requested by the user.
# It will be usually called asynchronously by a delayed job call.
#
# @example Delayed::Job.enqueue EmailPasswordRequest.new(<password_request_id>)
class EmailPasswordRequest

  include LoggableJob
  include Utils

  def initialize(password_request_id)
    @password_request_id = password_request_id
  end

  def perform
    password_request = PasswordRequest.find @password_request_id

    if password_request && password_request.user.email && password_request.valid_request
      args = {
        template_name: 'password_request',
        to: password_request.user.email,
        from: 'Swoop <hello@joinswoop.com>',
        subject: "Reset Swoop Password",
        locals: {
          :@password_request => password_request,
          :@smtp_api_values => { "filters.clicktrack.settings.enable" => 0 },
        },
      }

      SystemMailer
        .send_mail(args, nil, nil, nil, 'Password Request', password_request)
        .deliver_now
    else
      log :warn, "Could not send password request id #{@password_request_id}. It might probably not be a valid request anymore."
    end
  end

end
