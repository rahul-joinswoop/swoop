# frozen_string_literal: true

class EmailDispatch

  include Utils
  def initialize(job_id, company_id)
    @job_id = job_id
    @company_id = company_id
  end

  def perform
    company = Company.find_by!(id: @company_id)
    @job = Job.find_by!({ id: @job_id })
    dispatch_email = company.dispatch_email
    tz = tz_from_or_la(@job.site.try(:tz))

    SystemMailer.send_mail({
      template_name: 'dispatch',
      to: dispatch_email,
      from: swoop_operations_email_as('Swoop'),
      subject: 'Job Request - Swoop',
      locals: { :@tz => tz, :@job => @job, :@job_requested_by => job_requested_by },
    }, nil, @job, company, "Dispatch", @job).deliver_now
  end

  private

  def job_requested_by
    if @job.fleet_company.fleet_managed?
      "Swoop"
    else
      @job.fleet_company.name
    end
  end

end
