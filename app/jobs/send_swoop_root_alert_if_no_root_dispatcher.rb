# frozen_string_literal: true

# Swoop root users will receive new job phone escalation alert for FleetManaged jobs
# until a root_dispatcher has been assigned or root_dispatcher is unassigned. Frequency set in
# FREQUENCY_TO_ALERT_SWOOP_ROOT_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET constant.
#
# @see https://swoopme.atlassian.net/browse/ENG-4443
class SendSwoopRootAlertIfNoRootDispatcher

  include LoggableJob

  FREQUENCY_TO_ALERT_SWOOP_ROOT_IF_ROOT_DISPATCHER_HAS_NEVER_BEEN_SET = 10.minutes

  def initialize(job_id)
    @job_id = job_id
  end

  def perform
    @job = Job.find(@job_id)

    if @job.blank?
      log :debug,
          "SYSTEM_ALERT [SendSwoopRootAlertIfNoRootDispatcher async job] - No job found",
          job_id: @job_id

      return
    end

    if can_system_alert_swoop_if_root_dispatcher_has_never_been_set?
      swoop_root_users = SystemEmailRecipientCalculator.new(@job).calculate

      swoop_root_users.each do |root|
        log :debug,
            "SYSTEM_ALERT - no Root Dispatcher ESCALATION - Queueing phone #{root.full_name}:#{root.phone}",
            job_id: @job_id

        Job.delay.dispatcher_not_assigned_to_job_call(root.phone)
      end
    else
      log :debug,
          "SYSTEM_ALERT not queueing phone call for SendSwoopRootAlertIfNoRootDispatcher, " \
          "because root_dispatcher: #{@job.root_dispatcher_id}",
          job_id: @job_id
    end
  end

  private

  def can_system_alert_swoop_if_root_dispatcher_has_never_been_set?
    @job.can_system_alert_swoop? && @job.root_dispatcher_id.blank?
  end

end
