# frozen_string_literal: true

class JobStatusEmail

  include Utils

  class MissingRecipientArgumentError < StandardError; end

  TEMPLATE_NAME = 'job_status'
  MAX_AGE = 2.hours

  def self.preference
    raise NotImplementedError
  end

  def initialize(job_id:, timestamp: Time.zone.now, user_id: nil, company_id: nil, rescue_provider_id: nil, validate: true, recipients: {})
    raise MissingRecipientArgumentError unless user_id || company_id || rescue_provider_id

    @job_id = job_id
    @timestamp = timestamp
    @user_id = user_id
    @company_id = company_id
    @rescue_provider_id = rescue_provider_id
    @validate = validate
    @recipients = recipients
  end

  def perform
    return if expired?

    @job = Job.find_by!(id: @job_id)

    return if recipient_emails_blank? || duplicate?

    opts = {
      template_name: TEMPLATE_NAME,
      to: to_email_addresses,
      cc: cc_email_addresses,
      bcc: bcc_email_addresses,
      from: SWOOP_OPERATIONS_EMAIL,
      subject: subject,
      locals: locals,
    }

    SystemMailer.send_mail(
      opts,
      logged_user,
      @job,
      logged_company,
      logged_category,
      @job
    ).deliver_now
  end

  def eta_to_dropoff
    nil
  end

  def eta_to_pickup
    nil
  end

  def original_eta_to_pickup
    nil
  end

  def orig_eta_mins
    nil
  end

  def extended_eta_mins
    nil
  end

  def logged_category
    JobStatus::ID_MAP[status_for_timestamp]
  end

  def scheduled_for
    nil
  end

  def status_display_name
    JobStatus::ID_MAP[status_for_timestamp]
  end

  def status_for_timestamp
    # Throwing an exception for sending `in_time_zone` to nil
    # https://swoopme.atlassian.net/browse/ENG-5847
    #
    # app/views/system_mailer/job_status.html.erb
    # <%= @job.status_time(@status_for_timestamp).in_time_zone(@tz).strftime("%a, %e %b %Y %k:%M:%S (%Z)") %>
    self.class.preference
  end

  def title
    raise NotImplementedError
  end

  protected

  def locals
    {
      '@original_eta_to_pickup': original_eta_to_pickup,
      '@orig_eta_mins': orig_eta_mins,
      '@extended_eta_mins': extended_eta_mins,
      '@eta_to_dropoff': eta_to_dropoff,
      '@eta_to_pickup': eta_to_pickup,
      '@job': @job,
      '@scheduled_for': scheduled_for,
      '@status_for_timestamp': status_for_timestamp,
      '@status_display_name': status_display_name,
      '@title': title,
      '@tz': tz,
    }
  end

  def recipient_emails_blank?
    (to_email_addresses.blank? && cc_email_addresses.blank? && bcc_email_addresses.blank?)
  end

  def expired?
    @validate && MAX_AGE.ago > @timestamp
  end

  def duplicate?
    @validate && AuditEmail.where({
      job: @job,
      category: logged_category,
      to: "[\"#{to_email_addresses}\"]",
    }).count > 0
  end

  def subject
    "#{title}#{@job.job_status_email_subject}"
  end

  def recipient
    @recipient ||= if @user_id
                     User.find_by!(id: @user_id)
                   elsif @company_id
                     Company.find_by!(id: @company_id)
                   else
                     RescueProvider.find_by!(id: @rescue_provider_id)
                   end
  end

  def to_email_addresses
    @to_email_addresses ||=
      [[@recipients[:to]] + [recipient.job_update_email]].flatten.compact.uniq
  end

  def cc_email_addresses
    @cc_email_addresses ||=
      [@recipients[:cc]].flatten.compact.uniq - to_email_addresses
  end

  def bcc_email_addresses
    @bcc_email_addresses ||=
      [@recipients[:bcc]].flatten.compact.uniq - to_email_addresses - cc_email_addresses
  end

  def tz
    @tz ||= if recipient.is_a?(User)
              recipient.timezone || @job.site&.tz || tz_from_or_la(recipient.company&.location&.timezone&.name)
            else
              tz_from_or_la(@job.site.try(:tz))
            end
  end

  def logged_user
    @logged_user ||= recipient if recipient.is_a?(User)
  end

  def logged_company
    @logged_company ||= if recipient.is_a?(User)
                          recipient.company
                        elsif recipient.is_a?(RescueProvider)
                          recipient.provider
                        else
                          recipient
                        end
  end

end
