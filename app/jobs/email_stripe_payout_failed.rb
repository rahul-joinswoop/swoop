# frozen_string_literal: true

class EmailStripePayoutFailed

  include Utils

  def initialize(company_id)
    @company_id = company_id
  end

  def perform
    company = Company.find(@company_id)

    to = [STRIPE_SUPPORT_EMAIL_ADDRESS, company.accounting_email]

    SystemMailer.send_mail(
      {
        template_name: 'stripe_payout_failed',
        to: to,
        from: swoop_operations_email_as('Swoop'),
        subject: "Swoop Payout Failed - #{company.name}",
        locals: { :@company => company },
      }, nil, nil, company, "StripePayoutFailed", nil
    ).deliver_now
  end

end
