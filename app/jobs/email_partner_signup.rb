# frozen_string_literal: true

class EmailPartnerSignup

  include Utils

  SUBJECT = 'New Partner Company'

  def initialize(company_id, user_id)
    @company_id = company_id
    @user_id = user_id
  end

  def perform
    company = Company.find(@company_id)
    user = User.find(@user_id)

    SystemMailer.send_mail(
      {
        template_name: 'partner_signup',
        to: SWOOP_ACCOUNT_CREATED_EMAIL,
        from: SWOOP_OPERATIONS_EMAIL,
        subject: SUBJECT,
        locals: { :@company => company, :@user => user },
      }, user, nil, company, SUBJECT, company
    ).deliver_now
  end

end
