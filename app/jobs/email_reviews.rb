# frozen_string_literal: true

class EmailReviews

  include LoggableJob

  include Utils

  def enqueue()
  end

  def send_mails(review)
    log :debug,
        "Processing review email",
        job_id: review.job.id,
        review_id: review.id

    company = review.job.rescue_company

    unless company.disable_review_emails?
      review.emailed_rating_at = Time.now
      review.emailed_feedback_at = Time.now if review.replies.any?
      review.save!

      recipients = Array(company)
      recipients = company.admins if company.email_admins_review?

      send_emails_to_users(review, recipients)
    end
  end

  def send_emails_to_users(review, recipients)
    recipients.each do |recipient|
      recipient_class = recipient.class.name

      log :debug,
          "Sending to #{recipient.review_email}(#{recipient_class} #{recipient.id})",
          job_id: review.job_id

      Delayed::Job.enqueue EmailReview.new(review.id, recipient.id, recipient_class)
    end
  end

  def perform
    reviews = Review.where("nps_question1 IS NOT NULL AND emailed_rating_at IS NULL AND reviews.created_at > ?", 2.hours.ago)
      .joins(:job)

    reviews.each do |review|
      send_mails review
    end

    Review.find_by_sql(
      <<~SQL
        select reviews.* from
          reviews
          left join jobs on reviews.job_id=jobs.id
          left join companies on jobs.rescue_company_id=companies.id
        where
          rating is not null and
          emailed_rating_at is null and
          NOW() - INTERVAL '2 hours' > reviews.created_at
          and companies.disable_review_emails is not true
      SQL
    ).each do |review|
      send_mails review
    end

    Review.find_by_sql(
      <<~SQL
        select reviews.* from
          reviews
          left join jobs on reviews.job_id=jobs.id
          left join companies on jobs.rescue_company_id=companies.id
        where
          rating is not null and
          (select count(*) as cnt from twilio_replies where target_type='Review' and target_id=reviews.id)>1 and
          emailed_feedback_at is null and
          companies.disable_review_emails is not true
      SQL
    ).each do |review|
      send_mails review
    end
  end

  def success
  end

end
