# frozen_string_literal: true

class JobStatusEmailSender

  def initialize(job_id, email_class_name, timestamp: Time.zone.now)
    @job_id = job_id
    @job_update_email = email_class_name.constantize
    @job_status_id_email_preference = @job_update_email.preference
    @timestamp = timestamp
  end

  def perform
    recipients.each do |recipient|
      args = { job_id: @job_id, timestamp: @timestamp }
      id_key = :user_id
      id_key = :company_id if recipient.is_a?(Company)
      id_key = :rescue_provider if recipient.is_a?(RescueProvider)

      args[id_key] = recipient.id

      Delayed::Job.enqueue @job_update_email.new(args)
    end
  end

  protected

  def job
    @job ||= Job.find(@job_id)
  end

  def recipients
    calculator = JobStatusEmailRecipientCalculator.new(job, @job_status_id_email_preference)
    calculator.execute
  end

end
