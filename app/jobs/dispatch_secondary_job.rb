# frozen_string_literal: true

class DispatchSecondaryJob

  include LoggableJob

  include Utils

  def initialize(job_id, second_tow_payload, user_id)
    @job_id = job_id
    @payload = second_tow_payload
    @user_id = user_id
  end

  def perform
    log :debug, "DispatchSecondaryJob::perform running", job: @job_id

    if @payload[:second_tow_dropoff] && @payload[:second_tow_scheduled_for]
      job = Job.find(@job_id)
      user = User.find(@user_id)
      copy = job.create_additional_job([
        "rescue_company_id",
        "account_id",
        "partner_notes",
        "dispatch_notes",
        "driver_notes",
        "site_id",
        "partner_dispatcher_id",
      ])
      copy.user = user
      copy.last_touched_by = user

      location = Location.new(extract([:address, :lat, :lng, :exact, :site_id], @payload[:second_tow_dropoff]))
      location.save!

      copy[:drop_location_id] = location.id
      copy.scheduled_for = @payload[:second_tow_scheduled_for]
      copy.service_code_id = ServiceCode.find_by(name: "Tow").id
      copy.pending_copy

      copy.save!
    end
  end

end
