# frozen_string_literal: true

class JobStatusEmail::Goa < JobStatusEmail

  def self.preference
    JobStatus::GOA
  end

  def title
    'Customer GOA'
  end

end
