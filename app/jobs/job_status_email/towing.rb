# frozen_string_literal: true

class JobStatusEmail::Towing < JobStatusEmail

  def self.preference
    JobStatus::TOWING
  end

  def eta_to_dropoff
    @job.dropoff_eta
  end

  def title
    'Towing'
  end

end
