# frozen_string_literal: true

class JobStatusEmail::OnSite < JobStatusEmail

  def self.preference
    JobStatus::ON_SITE
  end

  def eta_to_dropoff
    @job.dropoff_eta
  end

  def title
    'On Site'
  end

end
