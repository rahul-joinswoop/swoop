# frozen_string_literal: true

class JobStatusEmail::Accepted < JobStatusEmail

  def self.preference
    JobStatus::ACCEPTED
  end

  def eta_to_dropoff
    @job.dropoff_eta
  end

  def eta_to_pickup
    @job.pickup_eta
  end

  def title
    'ETA Provided'
  end

end
