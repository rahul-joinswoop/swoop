# frozen_string_literal: true

class JobStatusEmail::Details < JobStatusEmail

  def initialize(job_id:, user_id: nil, company_id: nil, recipients: nil, job_status_email: nil)
    super(job_id: job_id, user_id: user_id, company_id: company_id, validate: false, recipients: recipients)

    @job_status_email = job_status_email
  end

  def preference
    if @job_status_email.present?
      @job_status_email.class.preference
    else
      @job.current_job_status_id
    end
  end

  def logged_category
    @job_status_email&.logged_category
  end

  def scheduled_for
    @job_status_email&.scheduled_for
  end

  def status_for_timestamp
    @job_status_email&.status_for_timestamp || preference
  end

  def status_display_name
    JobStatus::ID_MAP[@job_status_email&.status_display_name || preference]
  end

  def title
    'Job Details'
  end

  def eta_to_dropoff
    @job_status_email&.eta_to_dropoff
  end

  def eta_to_pickup
    @job_status_email&.eta_to_pickup
  end

end
