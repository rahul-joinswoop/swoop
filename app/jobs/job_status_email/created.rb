# frozen_string_literal: true

class JobStatusEmail::Created < JobStatusEmail

  def self.preference
    JobStatus::CREATED
  end

  def title
    'Job Created'
  end

end
