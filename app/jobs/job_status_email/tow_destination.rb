# frozen_string_literal: true

class JobStatusEmail::TowDestination < JobStatusEmail

  def self.preference
    JobStatus::TOW_DESTINATION
  end

  def title
    'Tow Destination'
  end

end
