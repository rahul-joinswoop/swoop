# frozen_string_literal: true

class JobStatusEmail::EnRoute < JobStatusEmail

  def self.preference
    JobStatus::EN_ROUTE
  end

  def eta_to_dropoff
    @job.dropoff_eta
  end

  def eta_to_pickup
    @job.pickup_eta
  end

  def title
    'En Route'
  end

end
