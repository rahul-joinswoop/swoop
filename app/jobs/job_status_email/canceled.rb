# frozen_string_literal: true

class JobStatusEmail::Canceled < JobStatusEmail

  def self.preference
    JobStatus::GOA
  end

  def status_for_timestamp
    JobStatus::CANCELED
  end

  def title
    'Job Canceled'
  end

end
