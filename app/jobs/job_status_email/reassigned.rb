# frozen_string_literal: true

class JobStatusEmail::Reassigned < JobStatusEmail

  def self.preference
    JobStatus::REASSIGNED
  end

  def status_display_name
    JobStatus::ID_MAP[JobStatus::CANCELED]
  end

  def title
    "Job Canceled"
  end

end
