# frozen_string_literal: true

class JobStatusEmail::Scheduled < JobStatusEmail

  def self.preference
    JobStatus::CREATED
  end

  def logged_category
    'Scheduled'
  end

  def scheduled_for
    @job.scheduled_for
  end

  def status_for_timestamp
    @job.current_job_status_id
  end

  def status_display_name
    if @job.current_job_status_id == JobStatus::PENDING
      'Created'
    else
      super
    end
  end

  def title
    'Job Scheduled'
  end

  protected

  def duplicate?
    false
  end

end
