# frozen_string_literal: true

class JobStatusEmail::Dispatched < JobStatusEmail

  def self.preference
    JobStatus::DISPATCHED
  end

  def eta_to_pickup
    @job.pickup_eta
  end

  def title
    'ETA Provided'
  end

end
