# frozen_string_literal: true

class JobStatusEmail::EtaExtended < JobStatusEmail

  def self.preference
    JobStatus::ETA_EXTENDED
  end

  def original_eta_to_pickup
    @job.original_eta_to_pickup
  end

  # This returns the latest ETA
  def eta_to_pickup
    @job.pickup_eta
  end

  def orig_eta_mins
    @job.orig_eta_mins
  end

  def extended_eta_mins
    @job.extended_eta_mins
  end

  def title
    'ETA Extended'
  end

end
