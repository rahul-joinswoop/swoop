# frozen_string_literal: true

class JobStatusEmail::Completed < JobStatusEmail

  def self.preference
    JobStatus::COMPLETED
  end

  def title
    'Job Completed'
  end

end
