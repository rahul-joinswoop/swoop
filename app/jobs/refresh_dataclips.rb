# frozen_string_literal: true

class RefreshDataclips

  def perform
    Report.find_each do |dataclip|
      dataclip.refresh!
    end
  end

end
