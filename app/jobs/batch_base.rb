# frozen_string_literal: true

require 'securerandom'

class BatchBase

  def log_command(target, *args)
    @command = Command.create!(target: target,
                               request: SecureRandom.uuid,
                               service_type: self.class.name,
                               json: args.to_json)
  end

  def log_command_applied
    @command.update_attribute(:applied, true)
  end

end
