# frozen_string_literal: true

class EmailDispatchableAdminNotification

  include Utils

  def initialize(user_ids, site_id: nil, rescue_provider_id: nil)
    @site_id = site_id
    @rescue_provider_id = rescue_provider_id
    @user_ids = Array(user_ids)
  end

  def perform
    users = User.find(@user_ids)
    tz = tz_from_or_la(users.first.try(:timezone))
    email = 'network@joinswoop.com'

    if @site_id
      site = Site.find_by!(id: @site_id, dispatchable: false)
      subject = "Site marked as NOT dispatchable"
      SystemMailer.send_mail({
        template_name: 'alert_dispatchable_disabled',
        to: email,
        from: swoop_operations_email_as('Swoop'),
        subject: subject,
        locals: { :@tz => tz, :@site => site, :@rescue_provider => site.company },
      },
                             nil, nil, Company.swoop, "SiteNonDispatchable", site).deliver_now
    elsif @rescue_provider_id
      rescue_provider = RescueProvider.find(@rescue_provider_id)
      subject = "Rescue Provider marked as NOT dispatchable"
      SystemMailer.send_mail({
        template_name: 'alert_dispatchable_disabled',
        to: email,
        from: swoop_operations_email_as('Swoop'),
        subject: subject,
        locals: { :@tz => tz, :@rescue_provider => rescue_provider, :@site => nil },
      },
                             nil, nil, Company.swoop, "RescueProviderNonDispatchable", rescue_provider).deliver_now
    end
  end

end
