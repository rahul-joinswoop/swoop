# frozen_string_literal: true

class EmailStripeVerificationFields

  include Utils

  DEFAULT_TO = 'stripesupport@joinswoop.com'

  def initialize(custom_account_id)
    @custom_account_id = custom_account_id
  end

  def perform
    @custom_account = CustomAccount.find(@custom_account_id)
    company = @custom_account.company

    SystemMailer.send_mail(
      {
        template_name: 'stripe_verification_fields_needed',
        to: STRIPE_SUPPORT_EMAIL_ADDRESS,
        from: swoop_operations_email_as('Swoop'),
        subject: "Stripe Verification Required - #{company.name}",
        locals: { :@company => company },
      }, nil, nil, company, "StripeVerificationFieldsNeeded", nil
    ).deliver_now
  end

end
