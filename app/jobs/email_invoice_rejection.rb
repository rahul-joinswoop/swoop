# frozen_string_literal: true

# Send invoice rejection notification. It sends the notification to both the
# fleet assigning company and the partner.
#
# @example Delayed::Job.enqueue EmailInvoiceRejection.new(invoice_rejection_id)
class EmailInvoiceRejection

  include Utils

  SWOOP_BILLING_EMAIL = 'billing@joinswoop.com'

  def initialize(invoice_rejection_id)
    @invoice_rejection = InvoiceRejection.find invoice_rejection_id
    @invoice = @invoice_rejection.invoice
    @job = @invoice.job
    @partner = @job.rescue_company
    @fleet = @job.assigning_company
  end

  def perform
    args = {
      to: [partner_email, fleet_email],
      from: "#{@fleet.name} <#{SWOOP_BILLING_EMAIL}>",
      reply_to: from_email,
      sender: SWOOP_BILLING_EMAIL,
      subject: subject,
      template_name: "invoice_rejection",
      locals: {
        :@job => @job,
        :@invoice_rejection => @invoice_rejection,
        :@partner => @partner,
        :@fleet => @fleet,
      },
    }

    SystemMailer.send_mail(
      args, nil, @job, nil, "Invoice Rejection", @invoice
    ).deliver_now
  end

  private

  def partner_email
    @partner.accounting_email_or_dispatch_email
  end

  def fleet_email
    @fleet.accounting_email_or_dispatch_email
  end

  def subject
    "Invoice Rejected by #{@fleet.name} for Job ID: #{@invoice.job_id}"
  end

  def from_email
    fleet_support_email = @fleet.support_email

    return "#{@fleet.name} <#{fleet_support_email}>" if fleet_support_email

    SWOOP_BILLING_EMAIL
  end

end
