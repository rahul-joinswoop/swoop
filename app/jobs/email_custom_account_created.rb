# frozen_string_literal: true

class EmailCustomAccountCreated

  include Utils

  def initialize(custom_account_id)
    @custom_account_id = custom_account_id
  end

  def perform
    @custom_account = CustomAccount.find(@custom_account_id)
    company = @custom_account.company

    SystemMailer.send_mail(
      {
        template_name: 'custom_account_created',
        to: STRIPE_SUPPORT_EMAIL_ADDRESS,
        from: swoop_operations_email_as('Swoop'),
        subject: "Stripe Account Created - #{company.name}",
        locals: { :@company => company },
      }, nil, nil, company, "CustomAccountCreated", nil
    ).deliver_now
  end

end
