# frozen_string_literal: true

class EmailInvoiceReceipt

  include LoggableJob

  include Utils

  def initialize(
    invoice_id, invoice_pdf_id, invoice_class = Invoice,
    to: nil, cc: nil, bcc: nil, customize_for_credit_card_charge: false
  )
    @invoice_id = invoice_id
    @invoice_pdf_id = invoice_pdf_id
    @invoice_class = invoice_class
    @to = to
    @cc = cc
    @bcc = bcc
    @customize_for_credit_card_charge = customize_for_credit_card_charge
  end

  def customized_recipient_email?
    !(@to.blank? && @cc.blank? & @bcc.blank?)
  end

  def to_email(invoice)
    return @to if customized_recipient_email?
    invoice.job_email
  end

  def cc_email
    return @cc if customized_recipient_email?
  end

  def bcc_email
    return @bcc if customized_recipient_email?
  end

  def perform
    @invoice = @invoice_class.find_by!(id: @invoice_id)
    log :debug, "invoice_recipient - #{@invoice.recipient.inspect}", job: @invoice.job.id

    if not_allowed_to_send?
      msg = "Attempt to send invoice receipt with account - #{@invoice.inspect}"
      log :error, msg, job: @invoice.job.id
      Rollbar.error(StandardError.new(msg))

      return
    end

    if @invoice.recipient.nil?
      log :warn, "Not sending invoice receipt to null recipient for #{@invoice.inspect}", job: @invoice.job.id

      return
    end

    unless @invoice.job_email || customized_recipient_email?
      log :warn, "Not sending invoice receipt to null email for #{@invoice.job.inspect}", job: @invoice.job.id

      return
    end

    args = {
      to: to_email(@invoice),
      from: @invoice.sender.accounting_email_or_dispatch_email,
      sender: SWOOP_OPERATIONS_EMAIL,
      subject: subject,
      template_name: 'invoice',
      locals: {
        :@receipt => true,
        :@customize_for_credit_card_charge => @customize_for_credit_card_charge,
        :@invoice => @invoice,
        :@invoice_pdf_id => @invoice_pdf_id,
      },
    }

    cc = cc_email
    args[:cc] = cc if cc.present?

    if @invoice.sender.accounting_email_or_dispatch_email
      args[:reply_to] = @invoice.sender.accounting_email
      args[:bcc] = [@invoice.sender.accounting_email, ENV['INVOICE_BCC_EMAIL']]
    else
      args[:bcc] = [ENV['INVOICE_BCC_EMAIL']]
    end

    bcc = bcc_email
    args[:bcc] << bcc if bcc.present?

    SystemMailer.send_mail(
      args, customized_invoice_recipient, @invoice.job, nil, "Invoice Receipt", @invoice
    ).deliver_now
  end

  private

  def customized_invoice_recipient
    if @customize_for_credit_card_charge
      @invoice.job.customer
    else
      @invoice.recipient
    end
  end

  def subject
    subject = "Receipt from #{@invoice.sender.name} for Job ID: #{@invoice.job_id}"

    if @invoice.job.po_number
      subject += " | PO: #{@invoice.job.po_number}"
    end

    subject
  end

  def not_allowed_to_send?
    @invoice.recipient_type != "User" && !@customize_for_credit_card_charge
  end

end
