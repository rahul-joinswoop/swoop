# frozen_string_literal: true

class EmailReview

  include Utils

  def initialize(review_id, recipient_id, recipient_class = nil)
    @review_id = review_id
    @recipient_id = recipient_id
    @recipient_class = recipient_class || User.name
  end

  def perform
    review = Review.find_by!({ id: @review_id })
    job = review.job

    # PDW bug we were accidently sending to Tesla
    return if job.fleet_company.is_tesla?

    recipient = @recipient_class.constantize.find_by!({ id: @recipient_id })

    if recipient.review_email
      # PDW: never send review to Farmers or 21st Century
      return if recipient.is_farmers_or_century?

      user = recipient if recipient.is_a?(User)
      company = recipient.is_a?(User) ? recipient.company : recipient
      SystemMailer.send_mail({
        template_name: 'review',
        to: recipient.review_email,
        from: swoop_operations_email_as('Swoop'),
        subject: "New Review for ID ##{job.id}",
        locals: { :@job => job, :@review => review },
      }, user, job, company, "Review", review).deliver_now
    end
  end

end
