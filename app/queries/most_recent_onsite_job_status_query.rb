# frozen_string_literal: true

class MostRecentOnsiteJobStatusQuery

  class << self

    def call(job_id, relation = AuditJobStatus.all)
      relation =
        relation
          .where(job_id: job_id, job_status_id: JobStatus::ON_SITE)
          .order(created_at: :desc)

      ret_val = relation

      # If multiple statuses, grab the most recent
      if relation.count >= 1
        ret_val = relation.first
      end

      ret_val
    end

  end

end
