# frozen_string_literal: true

module Jobs
  class RecentlyBid

    class << self

      # Company needs to be able to view jobs for which it has recently bid
      # even if lost in order for subscriptions to work.
      def call(context, relation = Job.all)
        rescue_company = context[:api_company]
        return Job.none unless rescue_company && rescue_company.respond_to?(:id)
        return Job.none unless authorized?(context)
        return relation unless relation.respond_to?(:bid_jobs)

        relation
          .bid_jobs(rescue_company)
          .joins(bids: :auction)
          .where([
            'auctions.status = ? OR ? < auctions.expires_at',
            Auction::Auction::LIVE,
            15.minutes.ago,
          ])
      end

      private

      def authorized?(context)
        if context[:api_user]
          # dispatchers and admin users can see every job for their company
          context.has_any_role?(:admin, :dispatcher)
        else
          # TODO - check application role(s)? right now allowing everything
          # from applications
          true
        end
      end

    end

  end
end
