# frozen_string_literal: true

class JobAncestorsWithStatusesQuery

  RECURSION_LIMIT = 10

  def self.call(id, relation = Job.all, statuses: [])
    new(id, relation, statuses).call
  end

  def initialize(id, relation, statuses)
    @id = id
    @relation = relation
    @statuses = statuses
  end

  def call
    relation = @relation.where(id: @id)
    relation = relation.where(status: @statuses) if @statuses.present?
    return relation if relation.blank?

    @job = relation.first
    return relation.none if @job.nil? || @job.parent.nil?

    ancestors = []
    recursion_count = 1

    while @job.present? && @job.parent.present?
      ancestors << @job.parent
      @job = next_valid_parent

      recursion_count += 1
      break if recursion_count >= RECURSION_LIMIT
    end

    Job.where(id: [ancestors.map(&:id)])
  end

  private

  def next_valid_parent
    return @job.parent if @job&.parent && valid_status?
    nil
  end

  def invalid_status?
    return false if @statuses.include?(@job.status.downcase)
    true
  end

  def valid_status?
    !invalid_status?
  end

end
