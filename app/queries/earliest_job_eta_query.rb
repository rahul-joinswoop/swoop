# frozen_string_literal: true

class EarliestJobEtaQuery

  class << self

    def call(job_id, relation = TimeOfArrival.all)
      job = job_or_eldest_parent(job_id)
      return nil if job.blank?

      relation
        .where(
          job_id: job.id,
          eba_type: [
            TimeOfArrival::BID,
            TimeOfArrival::PARTNER,
            TimeOfArrival::SWOOP_ESTIMATE,
          ]
        )
        .order(created_at: :asc)
        .first
    end

    private

    def job_or_eldest_parent(job_id)
      parent = JobAncestorsWithStatusesQuery
        .call(job_id, statuses: [Job::STATUS_UNASSIGNED, Job::STATUS_ASSIGNED])
        .last

      return parent if parent.present?
      Job.find(job_id)
    end

  end

end
