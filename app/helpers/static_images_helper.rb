# frozen_string_literal: true

# Helper class to treat static images
module StaticImagesHelper

  include ActionView::Helpers::AssetTagHelper

  RESIZE_IMAGES_SERVICE_URL = "http://images.weserv.nl/?url="

  # This is based on images.weserv.nl service, and is mainly used on pdf templates @see invoice.pdf.erb
  # The URL of the service (http://images.weserv.nl) needs to be http; if https wicked_pdf doesnt work.
  def url_for_external_image_resized(url:, height: '', width: '', class: '')
    raise ArgumentError if (url =~ URI.regexp).blank?

    # TODO - should probably use Addressable::URI here?
    url_parsed = url.sub(/^https?\:\/\//, '')
    h = height.present? ? "&h=#{height}" : ''
    w = width.present? ? "&w=#{width}" : ''

    service_url = "#{RESIZE_IMAGES_SERVICE_URL}#{url_parsed}#{h}#{w}"

    image_tag service_url, height: height, width: width, class: ''
  end

end
