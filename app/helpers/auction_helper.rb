# frozen_string_literal: true

module AuctionHelper

  # Examine the bids for an auction, and if no eligible bidders remain,
  # end the auction early
  def expire_auction_immediately_if_no_bidders_remain(bid, job)
    expire_auction = false
    Auction::Bid.transaction do
      # Step 1: Is this an auction that's allowed to expire?
      if Auction::Bid.where(job: job).count == Auction::Bid.where(job: job).where(status: Auction::Bid::NEW).count
        raise CannotExpireAuctionImmediatelyWhereAllBidsAreNewError
      end

      # Step 2: If this auction has even one terminal bid (won, auto-rejected,
      # expired, cancelled), then the auction is over or about to be over, and
      # no one should be calling this method trying to end it
      if Auction::Bid.indicating_auction_ended(job).count.nonzero?
        raise CannotExpireAuctionImmediatelyIfAuctionHasAnyTerminalBidsError
      end

      # Step 3: Check if the auction is eligible to be expired
      if Auction::Bid.where(job: job).count == Auction::Bid.considered_silent_at_going_once_going_twice(job).count

        # Step 3.1: Flag that we need to run a new ExpireAuctionTimer that ends the auction immediately
        expire_auction = true

        # Step 3.2: Stop any other pre-scheduled ExpireAuctionTimer's for the auction
        #
        # We don't do this! (Intentionally.) Why?
        #
        # Yes -- any pre-scheduled ExpireAuctionTimer's will still run unnecessarily,
        # after the auction ends, but that's OK since ExpireAuctionTimer invokes
        # ExpireAuctionService, which invokes its parent's EndAuctionService#call,
        # which obtains a pessimistic lock and can only run serially anyway... so if
        # it runs twice, that's OK
        #
        # In the future, we could kill the pre-scheduled ExpireAuctionTimer here, but
        # there's no compelling reason to do so now
      end
    end
    Auction::ExpireAuctionTimer.perform_async(bid.auction.id) if expire_auction
  end

  class CannotExpireAuctionImmediatelyWhereAllBidsAreNewError < StandardError
  end
  class CannotExpireAuctionImmediatelyIfAuctionHasAnyTerminalBidsError < StandardError
  end

end
