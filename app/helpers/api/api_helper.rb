# frozen_string_literal: true

module API::APIHelper

  def authenticated_export_url(dataclip)
    if dataclip.last_result
      filename = "swoop_export-#{dataclip.name.parameterize}-#{dataclip.created_at.to_formatted_s(:number)}"
      api_v1_root_dataclip_results_url(token: dataclip.last_result_token, filename: filename)
    end
  end

end
