# frozen_string_literal: true

#
# INVOICABLE INTERFACE
#
# Implement these methods in a class in order to make
# it capable of generating an invoice using the rate system
#
# N.B. this module establishes a collection of methods in
# the runtime method lookup chain by defining each method
# with the body of 'super' -- a poor man's interface -- so
# that:
# 1. if the method exists directly on the class which includes
# this interface, Ruby will use it and never reach super
# 2. if the method exists on a parent class of the class
# which includes this interface, this interface will pass along
# the method call via super and "bubble up" to it
# 3. if the method doesn't exist, super will bubble up to
# BasicObject, and raise an exception just like any other
# missing method
#
module Invoicable

  # Which fleet company has requested the tow job for
  # the invoicable event?
  def fleet_company
    super
  end

  # Which rescue company will perform the tow job for the
  # invoicable event?
  def rescue_company
    super
  end

  # Which site of the rescue company will perform the invoicable
  # event?
  def site
    super
  end

  # What account should be billed for performing the invoicable event?
  def account
    super
  end

  # How many minutes does it take a provider to perform
  # the invoicable event?
  def mins_p2p
    super
  end

  # How many miles will a provider have to travel in order
  # to perform the invoicable event?
  def miles_p2p
    super
  end

  # Approximately how many minutes and miles will it take this
  # provider to perform the invoicable event?
  def estimate
    super
  end

  # Where will the provider drop off the vehicle at the end of
  # the invoicable event?
  def drop_location
    super
  end

  # What is the Job ID of the invoicable event?
  def job_id
    super
  end

  # What is the ISSC contractor ID of the invoicable event?
  def issc_contractor_id
    super
  end

end
