# frozen_string_literal: true

module ReportHelper

  attr_accessor :colated_results

  def user_name_for_filter(user_id, default = 'All')
    default_filter_wrapper(user_id, default) { User.find(user_id).full_name }
  end

  def truck_name_for_filter(truck_id, default = 'All')
    default_filter_wrapper(truck_id, default) { RescueVehicle.find(truck_id).name }
  end

  def account_name_for_filter(account_id, default = 'All')
    default_filter_wrapper(account_id, default) { Account.find(account_id).name }
  end

  def site_name_for_filter(site_id, default = 'All')
    default_filter_wrapper(site_id, default) { Site.find(site_id).name }
  end

  def storage_type_name_for_filter(type_id, default = 'All')
    default_filter_wrapper(type_id, default) { StorageType.find(type_id).name }
  end

  def payment_method_for_filter(payment_method_id, default = 'All')
    default_filter_wrapper(payment_method_id, default) { PaymentMethod.find(payment_method_id).name }
  end

  def date_range_string_for_filter(start_date, end_date, user)
    return 'All Time' if start_date.blank? && end_date.blank?
    return format_date(end_date, user) if start_date.blank?
    return "#{format_date(start_date, user)} - #{current_time_for(user).strftime('%m/%d/%Y')}" if end_date.blank?
    "#{format_date(start_date, user)} - #{format_date(end_date, user)}"
  end

  def invoice_status_for_filter(filters)
    if filters['unsent']
      'Unsent'
    elsif filters['outstanding']
      'Outstanding'
    elsif filters['paid']
      'Paid'
    else
      'All'
    end
  end

  def payment_type_for_filter(payment_type_id, default = 'All')
    default_filter_wrapper(payment_type_id, default) { CustomerType.find(payment_type_id).name }
  end

  def current_time_for(user)
    tz = user ? user.tz_name : 'America/Los_Angeles'
    Time.now.in_time_zone(tz).end_of_day
  end

  def format_date(date, user)
    tz = user ? user.tz_name : 'America/Los_Angeles'
    Time.use_zone(tz) do
      time = if date["/"].present?
               Time.strptime(date, "%m/%d/%Y").in_time_zone
             else
               Time.parse(date).in_time_zone
             end

      time.strftime("%m/%d/%Y")
    end
  end

  def duration_to_seconds(hms_duration)
    return 0 if hms_duration.blank?
    pieces = %w(hours minutes seconds)

    is_negative = hms_duration["-"].present?
    dur = hms_duration.gsub('-', '')

    ret = 0

    dur.split(':').each_with_index do |val, ind|
      ret += val.to_i.send(pieces[ind])
    end

    is_negative ? ret * -1 : ret
  rescue => e
    Rollbar.error(e)
    Rails.logger.error e.message
    0
  end

  def seconds_to_duration(secs)
    return '00:00' if secs.to_i.zero?

    is_negative = secs < 0
    seconds     = secs.abs

    hours   = (seconds / 3600.00).floor
    minutes = ((seconds - hours.hours) / 60).floor
    minutes = 0 if minutes < 0

    ret = [hours, minutes].map { |t| t.floor.to_s.rjust(2, '0') }.join(':')
    is_negative ? "-#{ret}" : ret
  end

  def seconds_to_human(secs)
    return "0 min" if !secs || secs.zero?

    negative = secs < 0 ? '-' : ''
    seconds  = secs.abs

    hours   = (seconds / 3600.00).floor
    minutes = ((seconds - hours.hours) / 60).floor
    minutes = 0 if minutes < 0

    if hours.zero? && minutes < 1
      return "1 min"
    end

    "#{negative}#{hours} hrs #{minutes} min"
  end

  def build_colated_results(results, result_field, &block)
    @colated_results ||= {}

    results.each do |result|
      key = result[result_field]

      hsh = @colated_results[key] || { results: [], totals: {} }

      block.call(hsh, result)
      hsh[:results] << result

      @colated_results[key] = hsh
    end
  end

  def format_phone(phone_number)
    formatted_phone = number_to_phone(phone_number)

    if formatted_phone.starts_with? '+'
      return formatted_phone.insert(2, ' ')
    end

    formatted_phone
  end

  private

  def default_filter_wrapper(id, default)
    return default unless id
    yield
  rescue => e
    puts e.message
    default
  end

end
