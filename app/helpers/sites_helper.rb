# frozen_string_literal: true

# Helper class to prepare Site model attributes before rendering in views.
#--
# This is how Rails helpers work. No need for:
# :reek:UtilityFunction:
module SitesHelper

  def dispatchable_by_fleet(rescue_provider = nil)
    return false unless rescue_provider
    !rescue_provider.deleted_at
  end

end
