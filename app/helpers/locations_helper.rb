# frozen_string_literal: true

module LocationsHelper

  class OriginOrDestinationDoesNotHaveLatAndLong < StandardError; end

  # Do all of the locations in an array have lat's + lng's?
  def do_all_locations_have_coords?(locations)
    locations.each do |location|
      raise OriginOrDestinationDoesNotHaveLatAndLong unless location.lat && location.lng
    end
  end

end
