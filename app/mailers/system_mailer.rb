# frozen_string_literal: true

# http://localhost:5000/rails/mailers
class SystemMailer < ApplicationMailer

  helper :utils
  include Utils

  X_SMTPAPI = 'X-SMTPAPI'

  def send_mail(args, user, job, company, category, target)
    args = report_invalid_emails(args, user&.id, job&.id, company&.id, category)

    if args
      args[:locals].each do |name, value|
        instance_variable_set(name, value)
      end

      args.delete(:locals)

      if @smtp_api_values.present?
        smtp_api(@smtp_api_values)
      end

      ret = mail(args)

      track_email(user, job, company, ret, category, target)
    end
    ret
  end

  def track_email(user, job, company, email, category, target)
    AuditEmail.create user: user,
                      job: job,
                      company: company,
                      body: email.body,
                      subject: email.subject,
                      to: email.to,
                      from: email.from,
                      bcc: email.bcc,
                      category: category,
                      target: target
  end

  # Helper for setting the X-SMTPAPI header to control how SendGrid sends the email.
  # Deep values can be set by with dot notation in the key (i.e. "filters.clicktrack.settings.enable").
  # See https://sendgrid.com/docs/API_Reference/SMTP_API/index.html
  def smtp_api(values)
    existing_json = headers[X_SMTPAPI]&.value
    payload = (existing_json.present? ? MultiJson.load(existing_json) : {})
    values.each do |key, value|
      val = payload
      parts = key.to_s.split('.')
      parts[0, parts.length - 1].each do |k|
        v = val[k]
        unless v
          v = {}
          val[k] = v
        end
        val = v
      end
      val[parts.last] = value
    end
    json = MultiJson.dump(payload)
    if headers[X_SMTPAPI]
      headers[X_SMTPAPI].value = json
    else
      headers[X_SMTPAPI] = json
    end
  end

  private

  # Filter out the invalid email addresses and report the request in Rollbar
  def report_invalid_emails(args, user_id, job_id, company_id, category)
    valid_emails = []
    invalid_emails_hash = {}

    recipient_keys = [:to, :cc, :bcc]

    recipient_keys.each do |key|
      email_addresses = args[key]

      if email_addresses
        email_addresses = email_addresses.split(/,|\;/) unless email_addresses.is_a?(Array)

        invalid_emails = email_addresses.select { |email| !email_valid?(email) }

        invalid_emails_hash[key] = invalid_emails if invalid_emails.present?
        args[key] = (email_addresses - invalid_emails_hash[key].to_a)

        valid_emails += args[key] if args[key].present?
      end
    end

    if invalid_emails_hash.present?
      Rollbar.warning(
        "Includes invalid email address(es)",
        invalid_emails: invalid_emails_hash,
        user_id: user_id,
        job_id: job_id,
        company_id: company_id,
        category: category
      )
    end

    valid_emails.any? ? args : false
  end

end
