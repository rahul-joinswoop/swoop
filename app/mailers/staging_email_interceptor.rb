# frozen_string_literal: true

# This email interceptor allows overriding all messages to be sent before they
# are handed to delivery agents.
#
# To be used for the Staging environment.
class StagingEmailInterceptor

  def self.delivering_email(message)
    message.subject = "Staging #{message.subject}"
  end

end
