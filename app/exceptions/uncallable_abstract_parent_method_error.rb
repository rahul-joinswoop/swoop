# frozen_string_literal: true

class UncallableAbstractParentMethodError < StandardError

  def initialize(msg = "You are calling an abstract parent method; you must implement it in a subclass in order to invoke it")
    super
  end

end
