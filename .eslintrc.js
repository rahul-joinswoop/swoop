module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    'jest/globals': true,
    jasmine: true,
  },
  extends: [
    'airbnb',
    'plugin:jest/recommended',
    'plugin:react/all',
  ],
  globals: {
    __DEV__: false,
    __TEST__: false,
    __PROD__: false,
    Rollbar: false,
    google: false,
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 9,
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true,
    },
  },
  plugins: [
    'babel',
    'import',
    'jest',
    'jsx-a11y',
    'react',
    'react-hooks',
    'security',
  ],
  settings: {
    'import/resolver': {
      webpack: {
        config: 'webpack/config.dev.js',
      },
    },
    react: {
      version: 'detect',
    },
  },
  rules: {
    'consistent-return': 0,
    // there's no autofix for this so let's disable it
    'max-len': 0,
    'jsx-a11y/no-noninteractive-element-interactions': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/mouse-events-have-key-events': 0,
    // this way we can use "role" as a prop in our own components
    'jsx-a11y/aria-role': [2, {
      ignoreNonDOM: true,
    }],
    'jsx-a11y/alt-text': [
      'error', {
        elements: [
          'img', 'object', 'area', 'input[type="image"]',
        ],
      }],
    // our changes to the airbnb defaults
    // we don't need this, we're using babel to add this everywhere
    strict: 0,
    // all of our deps are devDependencies now so we explicitly set devDependencies: true
    // here to allow us to pull them in from anywhere
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    // disable this because we need to review what it prevents
    'no-restricted-globals': 0,
    // don't enforce this
    'import/prefer-default-export': 0,
    // include .json files in the list of extensions we can skip
    'import/extensions': [
      'error',
      'always', {
        js: 'never',
        jsx: 'never',
        json: 'never',
        coffee: 'never',
      },
    ],
    'function-paren-newline': 0,
    'object-curly-newline': ['error', {
      ObjectExpression: { minProperties: 4, multiline: true, consistent: true },
      ObjectPattern: { minProperties: 4, multiline: true, consistent: true },
      ImportDeclaration: { minProperties: 10, multiline: true, consistent: true },
      ExportDeclaration: { minProperties: 10, multiline: true, consistent: true },
    }],
    'react/boolean-prop-naming': 0,
    // disable this for now since it can't distinguish between a webpack require
    // and a regular module require
    'global-require': 0,
    // eslint-plugin-react
    // need to fix and enable these too:
    'react/require-default-props': 0,
    'react/forbid-prop-types': 0,
    'react/destructuring-assignment': 0,
    'react/prop-types': 0,
    // We aren't following this throughout most of the app, hence disabling
    // Fear is people will just create wrappers to satisfy this lint rule and create uglier code
    // Potential to reenable later but we'll need to do a full app sweep
    'react/jsx-handler-names': 0,
    // if you are using more than one line in a jsx epression then each prop needs
    // to be on its own line
    'react/jsx-max-props-per-line': [
      'error', {
        when: 'multiline',
      },
    ],
    // this is a stupid rule
    'react/jsx-no-literals': 0,
    // this is annoying and broken
    'react/jsx-one-expression-per-line': 0,
    // this is annoying too
    'react/jsx-child-element-spacing': 0,
    // sort props in abc order
    'react/jsx-sort-props': [
      'error', {
        reservedFirst: false,
        ignoreCase: true,
      },
    ],
    // i don't think there's a problem with using setState?
    'react/no-set-state': 0,
    'react/no-children-prop': 0,
    'react/no-multi-comp': 0,
    // this can't handle sfc:
    // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unused-prop-types.md#false-positives-sfc
    'react/no-unused-prop-types': 0,
    // the react class said we could get rid of this so bye felicia
    'react/jsx-no-bind': 0,
    // i think prop spreading is legit?
    'react/jsx-props-no-spreading': 0,
    // initializing state in a class property to avoid a constructore is a reasonable pattern
    'react/state-in-constructor': 0,
    'prefer-destructuring': ['error', {
      VariableDeclarator: {
        array: true,
        object: true,
      },
      AssignmentExpression: {
        array: true,
        object: false,
      },
    }, {
      enforceForRenamedProperties: false,
    }],
    // disable this rule for now - would be good to enable in the future once we
    // have some good examples?

    'react/require-optimization': 0,
    // jsx should only be in .js files
    'react/jsx-filename-extension': [2, { extensions: ['.js'] }],
    // not necessary, sort of annoying, doesn't always match consistently
    'react/jsx-wrap-multilines': 0,
    // end eslint-plugin-react

    // disable because it makes many things more difficult, see
    //
    // https://github.com/facebookincubator/create-react-app/pull/529
    // https://github.com/eslint/eslint/issues/5150
    'no-return-assign': 0,
    // http://eslint.org/docs/user-guide/migrating-to-4.0.0#-the-no-multi-spaces-rule-is-more-strict-by-default
    'no-multi-spaces': [
      'error', {
        ignoreEOLComments: true,
      },
    ],
    // allow these, _foo is a legit naming convention as far as i'm concerned
    'no-underscore-dangle': 0,
    // allow ++/-- in loops
    'no-plusplus': [
      'error', {
        allowForLoopAfterthoughts: true,
      },
    ],
    // if we're declaring uninitialized variables, force them to be on one line, ie
    //
    //   let foo, bar baz
    //
    // if we're declaring variables with values they need to be on their own line
    //
    //   let foo = 1
    //   const bar = 2
    'one-var': [
      'error', {
        initialized: 'never',
        uninitialized: 'always',
      },
    ],
    'one-var-declaration-per-line': [
      'error', 'initializations',
    ],
    // this is the airbnb indent rule with changes:
    indent: [
      'error',
      2, {
        SwitchCase: 1,
        VariableDeclarator: 1,
        outerIIFEBody: 1,
        // change #1: MemberExpressions don't get indented. good:
        //
        // foo
        // .bar
        // .baz
        //
        // bad:
        //
        // foo
        //   .bar
        //   .baz
        MemberExpression: 0,
        // change #2: call expression arguments have to align with the first arg
        // if they're multiline.
        //
        // good:
        //
        // foo(bar, baz, bat,
        //     blah, blah, asdf)
        //
        //  bad:
        //
        //  foo(bar,
        //   baz)

        CallExpression: {
          arguments: 'first',
        },
        // same with function declarations
        //
        // function foo(bar, baz,
        //              bat, asdf)
        FunctionDeclaration: {
          parameters: 'first',
          body: 1,
        },
        // and function expressions
        // same with function declarations
        //
        // var f = function foo(bar, baz,
        //                      bat, asdf)

        FunctionExpression: {
          parameters: 'first',
          body: 1,
        },
        ObjectExpression: 'first',
      },
    ],

    'comma-dangle': [
      'error', 'always-multiline',
    ],
    // disabled - having an else block is usually clearer than not having one
    'no-else-return': 0,

    // eslint-plugin-babel rules - for each of these we have to disable the
    // original first, then use the babel version with the original's settings,
    // all of which come from the airbnb ruleset:
    //
    //  https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb-base
    //

    'new-cap': 0,
    'babel/new-cap': [
      'error', {
        newIsCap: true,
        newIsCapExceptions: [],
        capIsNew: false,
        capIsNewExceptions: ['Immutable.Map', 'Immutable.Set', 'Immutable.List'],
      },
    ],
    'object-curly-spacing': 0,
    'babel/object-curly-spacing': [
      'error', 'always',
    ],
    'no-invalid-this': 0,
    'babel/no-invalid-this': 1,
    quotes: 0,
    'babel/quotes': [
      'error',
      'single',
      { avoidEscape: true },
    ],
    semi: 0,
    'babel/semi': [
      2, 'never',
    ],
    'no-unused-expressions': 'off',
    'babel/no-unused-expressions': ['error', {
      allowShortCircuit: false,
      allowTernary: false,
      allowTaggedTemplates: false,
    }],
    // end eslint-plugin-babel

    // eslint-plugin-security stuff, wish there was a way to extend these
    'security/detect-unsafe-regex': 2,
    'security/detect-disable-mustache-escape': 2,
    'security/detect-eval-with-expression': 2,
    'security/detect-non-literal-fs-filename': 0,
    'security/detect-non-literal-regexp': 2,
    'security/detect-non-literal-require': 1,
    'security/detect-possible-timing-attacks': 2,
    'security/detect-pseudoRandomBytes': 2,
    // end eslint-plugin-security

    // disable camelcase for now - hopefully with graphql we can turn it back on
    camelcase: 0,

    // TODO - we use the opposite in rails, we should standardize one way or the other
    'dot-location': ['error', 'object'],
    'no-param-reassign': ['error', { props: true }],
    'linebreak-style': [
      'error',
      'unix',
    ],

    // annoying to enforce
    'react/jsx-max-depth': 0,
    'implicit-arrow-linebreak': 0,
    // put ternary operators at the end of a line, not the beginning
    'operator-linebreak': ['error', 'after'],
    // react-hooks
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',

    // out-of-sync with CodeClimate
    'arrow-parens': 0,
  },
}
