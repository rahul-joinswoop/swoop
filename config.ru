# frozen_string_literal: true

# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment', __FILE__)

# log = File.new("log/rack.log", "a+")
# $stdout.reopen(log)
# $stderr.reopen(log)

run Rails.application
