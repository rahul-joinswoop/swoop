const webpack = require('webpack')
const merge = require('webpack-merge')
const Dotenv = require('dotenv-webpack')
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')
const { buildCommon } = require('./common')

const commonConfig = buildCommon({ cssSourceMaps: false })

commonConfig.entry.dev = ['@babel/polyfill', 'url-polyfill', 'raf/polyfill', 'isomorphic-fetch']
// in development we use webpack-hot-middleware
Object.values(commonConfig.entry).forEach(entry => entry.unshift('webpack-hot-middleware/client'))

module.exports = merge.smart(commonConfig, {
  devtool: 'source-map',
  output: {
    filename: '[name].js',
  },
  mode: 'development',
  optimization: {
    nodeEnv: process.env.BABEL_ENV || process.env.NODE_ENV || 'development',
  },
  plugins: [
    new Dotenv(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      debug: true,
    }),
    new HardSourceWebpackPlugin({
      cacheDirectory: '../../tmp/webpack/hard-source/[confighash]',
      configHash(webpackConfig) {
        return [
          process.env.NODE_ENV,
          process.env.BABEL_ENV,
          require('node-object-hash')({ sort: false }).hash(webpackConfig),
        ].join('-')
      },
      environmentHash: {
        root: process.cwd(),
        directories: [],
        files: ['package.json', 'yarn.lock', '.babelrc'],
      },
    }),
  ],
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },

})
