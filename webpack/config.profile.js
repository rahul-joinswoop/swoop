const merge = require('webpack-merge')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const prodConfig = require('./config.production')

module.exports = merge.smart(prodConfig, {
  plugins: [
    // same as production but with the bundle analyzer plugin
    new BundleAnalyzerPlugin({
      analyzerMode: 'disabled',
      generateStatsFile: true,
      statsOptions: {
        source: false,
      },
    }),
  ],
})
