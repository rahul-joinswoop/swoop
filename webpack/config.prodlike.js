const Dotenv = require('dotenv-webpack')
const prodConfig = require('./config.production')

// remove the DefinePlugin from production
prodConfig.plugins = prodConfig.plugins.filter(p =>
  p.constructor.name !== 'DefinePlugin'
)

// and add dotenv instead
prodConfig.plugins.unshift(new Dotenv())

module.exports = prodConfig
