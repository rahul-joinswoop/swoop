const webpack = require('webpack')
const merge = require('webpack-merge')
const Dotenv = require('dotenv-webpack')
const { buildCommon } = require('./common')

module.exports = merge.smart(buildCommon({ cssSourceMaps: false }), {
  devtool: 'eval',
  output: {
    filename: '[name].js',
  },
  mode: 'development',
  optimization: {
    nodeEnv: process.env.BABEL_ENV || process.env.NODE_ENV || 'development',
  },
  plugins: [
    new Dotenv(),
    new webpack.LoaderOptionsPlugin({
      debug: true,
    }),
  ],
  watch: true,
  watchOptions: {
    aggregateTimeout: 1000,
    ignored: /node_modules/,
    poll: 1000,
  },
})
