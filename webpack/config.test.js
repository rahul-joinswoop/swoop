/* eslint-disable no-console */

const webpack = require('webpack')
const merge = require('webpack-merge')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const Dotenv = require('dotenv-webpack')
const { buildCommon, buildEnv } = require('./common')

const config = merge.smart(buildCommon(), {
  devtool: 'eval',
  output: {
    filename: '[name].js',
  },
  mode: 'development',
  optimization: {
    nodeEnv: 'test',
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true,
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ],
})

if (process.env.CIRCLECI) {
  config.plugins.push(new webpack.DefinePlugin(buildEnv()))
} else {
  config.plugins.push(new Dotenv())
}

module.exports = config
