/* eslint-disable no-console */
const path = require('path')
const deepcopy = require('deepcopy')
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const AssetsPlugin = require('assets-webpack-plugin')

const ROOT_DIR = path.resolve(path.join(__dirname, '..'))
const PUBLIC_PATH = '/assets/'
const LOCAL_PATH = 'public/assets'
const JS_DIR = path.resolve(ROOT_DIR, 'app/assets/javascripts')

const buildEnv = () => ({
  'process.env': {
    ABLY_ENCRYPTION_KEY: JSON.stringify(process.env.ABLY_ENCRYPTION_KEY),
    HEROKU_PARENT_APP_NAME: JSON.stringify(process.env.HEROKU_PARENT_APP_NAME),
    HEROKU_APP_NAME: JSON.stringify(process.env.HEROKU_APP_NAME),
    JS_ENV: JSON.stringify(process.env.JS_ENV),
    RENTAL_CAR_BASE_URL: JSON.stringify(process.env.RENTAL_CAR_BASE_URL),
    SITE_URL: JSON.stringify(process.env.SITE_URL),
    STRIPE_PUBLIC_KEY: JSON.stringify(process.env.STRIPE_PUBLIC_KEY),
    WEBSOCKET_URL: JSON.stringify(process.env.WEBSOCKET_URL),
    JOBS_PER_PAGE_COUNT: Number.parseInt(process.env.JOBS_PER_PAGE_COUNT, 10),
    MAX_ACTIVE_JOB_LOAD_COUNT: Number.parseInt(process.env.MAX_ACTIVE_JOB_LOAD_COUNT, 10),
    SPLIT_KEY: JSON.stringify(process.env.SPLIT_KEY),
  },
})

function isEnvDevelopment() {
  let env = process.env.NODE_ENV
  if (!env) {
    env = 'development'
    /* eslint-disable-next-line no-console */
    console.error(`WARNING: NODE_ENV not set, webpack building for default env ${JSON.stringify(env)}`)
  }
  return env === 'development'
}

const allEntries = {
  application: [path.join(JS_DIR, 'routes/Application.js')],
  get_location: [path.join(JS_DIR, 'routes/GetLocation.js')],
  show_eta: [path.join(JS_DIR, 'routes/ShowEta.js')],
  survey: [path.join(JS_DIR, 'routes/Survey.js')],
}

const polyfills = ['@babel/polyfill', 'url-polyfill', 'raf/polyfill', 'isomorphic-fetch']

function buildEntries() {
  const entries = deepcopy(allEntries)
  // we need the babel polyfill everywhere
  Object.values(entries).forEach((entry) => entry.unshift(...polyfills))
  return entries
}

module.exports = {
  buildCommon: ({ cssSourceMaps = true } = {}) => ({
    entry: buildEntries(),
    output: {
      path: path.join(ROOT_DIR, LOCAL_PATH),
      publicPath: PUBLIC_PATH,
    },
    stats: true,
    target: 'web',
    resolve: {
      // don't list .css / .scss files here or we'll run into issues when
      // .coffee and .sass versions of the same file exist
      extensions: ['.js', '.jsx', '.mjs', '.coffee'],
      alias: {
        // we have js-related aliases in .babelrc.js but webpack won't pick those
        // up for assets so we have to add asset-related aliases here
        stylesheets: path.join(ROOT_DIR, 'app/assets/stylesheets/'),
        images: path.join(ROOT_DIR, 'app/assets/images'),
        public: path.join(ROOT_DIR, 'public'),
        fonts: path.join(ROOT_DIR, 'app/assets/fonts'),
        vendor: path.join(ROOT_DIR, 'vendor/assets'),
        jquery: path.resolve(path.join(__dirname, '../node_modules/jquery')),
        moment: path.resolve(path.join(__dirname, '../node_modules/moment')),
      },
      modules: [
        'node_modules',
        'app/assets/stylesheets/',
        'app/assets/images/',
        'vendor/assets/stylesheets/',
        'vendor/assets/javascripts/',
        'vendor/assets/img/',
        'vendor/assets/',
        // This is a bad practice
        'app/assets/javascripts/components',
        'app/assets/javascripts/lib',
        'app/assets/javascripts',
      ],
    },
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            {
              loader: isEnvDevelopment() ? 'style-loader' : MiniCssExtractPlugin.loader,
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: cssSourceMaps,
                importLoaders: 3,
                // enable css modules but make global the default - you have to explicitly do :local()
                // in css to get exports
                modules: 'global',
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: cssSourceMaps,
                config: {
                  path: `${__dirname}/postcss.config.js`,
                },
              },
            },
            {
              loader: 'resolve-url-loader',
              options: {
                sourceMap: cssSourceMaps,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                // it is required to have sourceMap always true because resolve-url-loader is based on source maps
                // please refer to docs for details https://github.com/bholloway/resolve-url-loader/blob/master/packages/resolve-url-loader/README.md#configure-webpack
                sourceMap: true,
              },
            },
          ],
        },
        {
          test: /\.coffee$/,
          use: [
            {
              loader: 'babel-loader',
              query: {
                // This is a feature of `babel-loader` for webpack (not Babel itself).
                // It enables caching results in ./node_modules/.cache/babel-loader/
                // directory for faster rebuilds.
                cacheDirectory: true,
              },
            },
            {
              loader: 'coffee-loader',
            },
          ],
        },
        // See: https://github.com/aws/aws-amplify/issues/686#issuecomment-387710340
        {
          test: /\.mjs$/,
          include: /node_modules\/deepcopy/,
          type: 'javascript/auto',
        },
        {
          test: /\.(js|jsx)$/,
          include: path.join(ROOT_DIR, 'app/assets/javascripts'),
          loader: 'babel-loader',
          query: {
            // This is a feature of `babel-loader` for webpack (not Babel itself).
            // It enables caching results in ./node_modules/.cache/babel-loader/
            // directory for faster rebuilds.
            cacheDirectory: true,
          },
        },
        { test: /\.(png|jpg|gif)$/, loader: 'file-loader?name=images/[name].[ext]' },
        {
          // eslint-disable-next-line
          test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
              },
            },
          ],
        },
      ],
    },
    optimization: {
      noEmitOnErrors: true,
      runtimeChunk: 'single',
      splitChunks: {
        cacheGroups: {
          // TODO - probably want to make separate bundles with common entries of
          // eta + location, survey, and application
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all',
          },
        },
      },
    },
    plugins: [
      new CaseSensitivePathsPlugin(),
      // this drops all locales from moment except en
      new MomentLocalesPlugin(),
      new AssetsPlugin({ path: path.join(ROOT_DIR, LOCAL_PATH) }),
    ],
  }),
  buildEnv,
  JS_DIR,
  LOCAL_PATH,
  PUBLIC_PATH,
  ROOT_DIR,
}
