/* eslint-disable no-console */

const webpack = require('webpack')
const merge = require('webpack-merge')
const RollbarSourceMapPlugin = require('rollbar-sourcemap-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const { URL } = require('url')
const { buildCommon, PUBLIC_PATH, buildEnv } = require('./common')

const buildRollbarPublicPath = () => {
  // rollbar doesn't want a trailing slash but everything else in webpack does
  const publicPath = PUBLIC_PATH.replace(/\/$/, '')
  if (process.env.SITE_URL) {
    const url = new URL(process.env.SITE_URL)
    url.pathname = publicPath
    return url.toString()
  }
  return publicPath
}

const getRollbarPlugin = () => {
  if (!process.env.ROLLBAR_ACCESS_TOKEN) {
    console.error('WARNING: ROLLBAR_ACCESS_TOKEN not set, skipping RollbarSourceMapPlugin')
    return []
  }
  if (!(process.env.HEROKU_SLUG_COMMIT || process.env.SOURCE_VERSION)) {
    console.error('WARNING: one of HEROKU_SLUG_COMMIT / SOURCE_VERSION not set, skipping RollbarSourceMapPlugin')
    return []
  }
  return [
    new RollbarSourceMapPlugin({
      accessToken: process.env.ROLLBAR_ACCESS_TOKEN,
      // SOURCE_VERSION is available on review apps, HEROKU_SLUG_COMMIT is
      // available on staging/production.
      version: process.env.HEROKU_SLUG_COMMIT || process.env.SOURCE_VERSION,
      publicPath: buildRollbarPublicPath(),
      ignoreErrors: true,
      silent: false,
    }),
  ]
}

module.exports = merge.smart(buildCommon(), {
  devtool: 'source-map',
  output: {
    filename: '[name].[chunkhash].js',
  },
  mode: 'production',
  optimization: {
    minimizer: [
      new TerserPlugin({
        sourceMap: true,
        cache: true,
        parallel: true,
        terserOptions: {
          ecma: 8,
          safari10: true,
          output: {
            comments: false,
            beautify: false,
          },
          compress: {
            warnings: false,
          },
        },
      }),
      new OptimizeCssAssetsPlugin({
        cssProcessorOptions: {
          map: {
            inline: false,
            annotation: true,
          },
          discardComments: { removeAll: true },
          // disabled because this seems to break on some of our css, see ENG-6803
          calc: false,
        },
      }),
    ],
    nodeEnv: 'production',
  },
  plugins: [
    new webpack.DefinePlugin(buildEnv()),
    new CleanWebpackPlugin(),
    new webpack.HashedModuleIdsPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
    ...getRollbarPlugin(),
  ],
})
