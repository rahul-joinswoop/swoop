# encoding: utf-8
# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __FILE__)
require 'swoop/version'

Gem::Specification.new do |s|
  s.name          = "swoop"
  s.version       = Swoop::VERSION
  s.authors       = ["Paul Widden"]
  s.email         = ["paul@joinswoop.com"]
  s.homepage      = "https://github.com/joinswoop/swoop"
  s.summary       = "TODO: summary"
  s.description   = "TODO: description"

  s.files         = `git ls-files app lib`.split("\n")
  s.platform      = Gem::Platform::RUBY
  s.require_paths = ['lib']
end
