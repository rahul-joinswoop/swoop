/* eslint-disable no-console */
const express = require('express')
const webpack = require('webpack')

const proxy = require('proxy-middleware')
const { once } = require('lodash')

const isProdlike = process.env.NODE_ENV === 'production'
const isTest = process.env.NODE_ENV === 'test'
const isDevelopment = process.env.NODE_ENV === 'development' || !process.env.NODE_ENV

/* eslint-disable global-require */
let config
if (isProdlike) {
  config = require('./webpack/config.prodlike')
} else if (isTest) {
  config = require('./webpack/config.test')
} else {
  config = require('./webpack/config.dev')
}

const app = express()
const compiler = webpack(config)

const listenPort = parseInt(process.env.LISTEN_PORT || 3000, 10)
const appPort = parseInt(process.env.APP_PORT || 5000, 10)
const hostname = process.env.SWOOP_HOSTNAME || 'localhost'

// add a plugin that will output on build only once - this is becuase webpack
// doesn't log when it starts it's initial build. each subsequent build does
// get logged, which is why we only want to get called once here.
compiler.hooks.compile.tap('Startup Logger', once(() => {
  console.log('webpack building...')
}))

app.use(require('webpack-dev-middleware')(compiler, {
  stats: true,
  publicPath: config.output.publicPath,
}))

if (isDevelopment) {
  /* eslint-disable-next-line global-require */
  app.use(require('webpack-hot-middleware')(compiler))
}

app.all('*', proxy({ host: hostname, port: appPort, preserveHost: true }))

app.listen(listenPort, '0.0.0.0', (err) => {
  if (err) {
    console.log(err)
    return
  }

  console.log(`Listening at http://${hostname}:${listenPort}`)
})
