/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS202: Simplify dynamic range loops
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import ReactTestUtils from 'react-dom/test-utils'

import '../../app/assets/javascripts/loader' // Mock out the set of user
import { Table, Column } from 'components/tables/ResizeTable'
import React from 'react'
import mocker from '../mocker'

describe('ResizeTable', () => {
  mocker.mock('stores/user_setting_store', {
    setUserSettings() {},
  })

  let cols = []
  let instance = null
  beforeEach(() => {
    jest.resetModules()
    cols = []

    for (let i = 0; i <= 9; i++) {
      class TestCol extends Column {
        static initClass() {
          this.prototype._header = `header_${i}`
          this.prototype._key = `key_${i}`
        }
      }

      TestCol.initClass()
      cols.push(new TestCol())
    }

    instance = React.createFactory(Table)({
      rows: [],
      cols,
      width: 1000,
    })
    instance = ReactTestUtils.renderIntoDocument(instance)
    return instance
  })
  describe('testing initial state', () => {
    it('Should start out with even columns', () => {
      let prevValue = cols[0].getWidth()
      let allEqual = true

      for (
        let col_index = 1, end = cols.length - 1, asc = end >= 1;
        asc ? col_index <= end : col_index >= end;
        asc ? col_index++ : col_index--
      ) {
        const col = cols[col_index]
        const newVal = col.getWidth()

        if (newVal !== prevValue) {
          allEqual = false
        }

        prevValue = newVal
      }

      expect(allEqual).to.be.true
    })
    it('Should start out with set columns', () => {
      expect(cols[0].getWidth()).not.to.be.null
    })
  })
  describe('resizing a col', () => {
    it('Should leave things the same on no change', () => {
      instance.onResizingCallback('key_1', false, 100)
      expect(cols[0].getWidth()).to.equal(100)
      expect(cols[1].getWidth()).to.equal(100)
      expect(cols[2].getWidth()).to.equal(100)
    })
    it('Should be able to grow into right column', () => {
      instance.onResizingCallback('key_1', false, 120)
      expect(cols[0].getWidth()).to.equal(100)
      expect(cols[1].getWidth()).to.equal(120)
      expect(cols[2].getWidth()).to.equal(80)
      expect(cols[3].getWidth()).to.equal(100)
    })
    it('Should be able to shrink and right column grow', () => {
      instance.onResizingCallback('key_1', false, 50)
      expect(cols[0].getWidth()).to.equal(100)
      expect(cols[1].getWidth()).to.equal(50)
      expect(cols[2].getWidth()).to.equal(150)
      expect(cols[3].getWidth()).to.equal(100)
    })
    it('Should properly waterfall into past columns', () => {
      instance.onResizingCallback('key_1', false, 350)
      expect(cols[0].getWidth()).to.equal(100)
      expect(cols[1].getWidth()).to.equal(350)
      expect(cols[2].getWidth()).to.equal(10)
      expect(cols[3].getWidth()).to.equal(10)
      expect(cols[4].getWidth()).to.equal(30)
      expect(cols[5].getWidth()).to.equal(100)
    })
    it('Should be able to steal if past min size', () => {
      instance.onResizingCallback('key_1', false, 5)
      expect(cols[0].getWidth()).to.equal(95)
      expect(cols[1].getWidth()).to.equal(10)
      expect(cols[2].getWidth()).to.equal(195)
      expect(cols[3].getWidth()).to.equal(100)
    })
    it('Should be able to waterfall to the left', () => {
      instance.onResizingCallback('key_2', false, -50)
      expect(cols[0].getWidth()).to.equal(100)
      expect(cols[1].getWidth()).to.equal(40)
      expect(cols[2].getWidth()).to.equal(10)
      expect(cols[3].getWidth()).to.equal(250)
    })
    it('Should handle waterfalling off left side', () => {
      instance.onResizingCallback('key_1', false, -150)
      expect(cols[0].getWidth()).to.equal(10)
      expect(cols[1].getWidth()).to.equal(10)
      expect(cols[2].getWidth()).to.equal(280)
      expect(cols[3].getWidth()).to.equal(100)
    })
    it('Should handle waterfalling off right side', () => {
      instance.onResizingCallback('key_6', false, 450)
      expect(cols[6].getWidth()).to.equal(370)
      expect(cols[7].getWidth()).to.equal(10)
      expect(cols[8].getWidth()).to.equal(10)
      expect(cols[9].getWidth()).to.equal(10)
    })
    describe('resizing first col', () => {
      it('Should handle first col - resize', () => {
        instance.onResizingCallback('key_0', false, -50)
        expect(cols[0].getWidth()).to.equal(10)
        expect(cols[1].getWidth()).to.equal(190)
        expect(cols[2].getWidth()).to.equal(100)
      })
      it('Should handle first col less then min width resize', () => {
        instance.onResizingCallback('key_0', false, 5)
        expect(cols[0].getWidth()).to.equal(10)
        expect(cols[1].getWidth()).to.equal(190)
        expect(cols[2].getWidth()).to.equal(100)
      })
      it('Should handle first col lower', () => {
        instance.onResizingCallback('key_0', false, 50)
        expect(cols[0].getWidth()).to.equal(50)
        expect(cols[1].getWidth()).to.equal(150)
        expect(cols[2].getWidth()).to.equal(100)
      })
    })
    describe('resizing last col', () => {
      it('Should handle last col past table', () => {
        instance.onResizingCallback('key_9', false, 150)
        expect(cols[8].getWidth()).to.equal(100)
        expect(cols[9].getWidth()).to.equal(100)
      })
      it('Should handle last col smaller resize', () => {
        instance.onResizingCallback('key_9', false, 50)
        expect(cols[8].getWidth()).to.equal(150)
        expect(cols[9].getWidth()).to.equal(50)
      })
    })
  })
})
