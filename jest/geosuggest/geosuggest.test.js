import 'loader'
import { Geosuggest } from 'components/geosuggest/Geosuggest'
import mocker from '../mocker'

describe('geosuggest test', () => {
  describe('.inputValueIsLatLng()', () => {
    it('returns true when format is lat,lng', () => {
      const geosuggest = new Geosuggest({ initialValue: '40.783060,-73.971248' })
      expect(geosuggest.inputValueIsLatLng()).to.be.true
    })

    it('returns true when format is "lat,(space)lng"', () => {
      const geosuggest = new Geosuggest({ initialValue: '40.783060, -73.971248' })
      expect(geosuggest.inputValueIsLatLng()).to.be.true
    })

    it('returns false when a regular address', () => {
      const geosuggest = new Geosuggest({ initialValue: 'București' })
      expect(geosuggest.inputValueIsLatLng()).to.be.false
    })

    it('returns false for null', () => {
      const geosuggest = new Geosuggest({ initialValue: null })
      expect(geosuggest.inputValueIsLatLng()).to.be.false
    })
  })
})

describe('filterMapResults', () => {
  const results = [
    {
      address_components: [
        {
          long_name: '1098',
          short_name: '1098',
          types: [
            'street_number',
          ],
        },
        {
          long_name: 'Harrison Street',
          short_name: 'Harrison St',
          types: [
            'route',
          ],
        },
        {
          long_name: 'SoMa',
          short_name: 'SoMa',
          types: [
            'neighborhood',
            'political',
          ],
        },
        {
          long_name: 'San Francisco',
          short_name: 'SF',
          types: [
            'locality',
            'political',
          ],
        },
        {
          long_name: 'San Francisco County',
          short_name: 'San Francisco County',
          types: [
            'administrative_area_level_2',
            'political',
          ],
        },
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
        {
          long_name: '94103',
          short_name: '94103',
          types: [
            'postal_code',
          ],
        },
        {
          long_name: '4521',
          short_name: '4521',
          types: [
            'postal_code_suffix',
          ],
        },
      ],
      formatted_address: '1098 Harrison St, San Francisco, CA 94103, USA',
      geometry: {
        bounds: {
          south: 37.7756876,
          west: -122.40656869999998,
          north: 37.7764118,
          east: -122.4056516,
        },
        location: {
          lat: 37.7761179,
          lng: -122.40610989999999,
        },
        location_type: 'ROOFTOP',
        viewport: {
          south: 37.77470071970851,
          west: -122.4074591302915,
          north: 37.7773986802915,
          east: -122.40476116970848,
        },
      },
      place_id: 'ChIJJU4dCSp-j4ARsDvvwzENCBs',
      types: [
        'premise',
      ],
    },
    {
      address_components: [
        {
          long_name: 'Harrison St & 7th St',
          short_name: 'Harrison St & 7th St',
          types: [
            'establishment',
            'point_of_interest',
            'transit_station',
          ],
        },
        {
          long_name: 'SoMa',
          short_name: 'SoMa',
          types: [
            'neighborhood',
            'political',
          ],
        },
        {
          long_name: 'San Francisco',
          short_name: 'SF',
          types: [
            'locality',
            'political',
          ],
        },
        {
          long_name: 'San Francisco County',
          short_name: 'San Francisco County',
          types: [
            'administrative_area_level_2',
            'political',
          ],
        },
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
        {
          long_name: '94103',
          short_name: '94103',
          types: [
            'postal_code',
          ],
        },
      ],
      formatted_address: 'Harrison St & 7th St, San Francisco, CA 94103, USA',
      geometry: {
        location: {
          lat: 37.7755824,
          lng: -122.40647530000001,
        },
        location_type: 'GEOMETRIC_CENTER',
        viewport: {
          south: 37.7742334197085,
          west: -122.40782428029149,
          north: 37.7769313802915,
          east: -122.40512631970853,
        },
      },
      place_id: 'ChIJFaATBSp-j4AR9sZhBN1olN0',
      plus_code: {
        compound_code: 'QHGV+6C SoMa, San Francisco, CA, United States',
        global_code: '849VQHGV+6C',
      },
      types: [
        'establishment',
        'point_of_interest',
        'transit_station',
      ],
    },
    {
      address_components: [
        {
          long_name: '409',
          short_name: '409',
          types: [
            'street_number',
          ],
        },
        {
          long_name: '7th Street',
          short_name: '7th St',
          types: [
            'route',
          ],
        },
        {
          long_name: 'SoMa',
          short_name: 'SoMa',
          types: [
            'neighborhood',
            'political',
          ],
        },
        {
          long_name: 'San Francisco',
          short_name: 'SF',
          types: [
            'locality',
            'political',
          ],
        },
        {
          long_name: 'San Francisco County',
          short_name: 'San Francisco County',
          types: [
            'administrative_area_level_2',
            'political',
          ],
        },
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
        {
          long_name: '94103',
          short_name: '94103',
          types: [
            'postal_code',
          ],
        },
      ],
      formatted_address: '409 7th St, San Francisco, CA 94103, USA',
      geometry: {
        location: {
          lat: 37.7753635,
          lng: -122.40610300000003,
        },
        location_type: 'RANGE_INTERPOLATED',
        viewport: {
          south: 37.7740145197085,
          west: -122.40745198029151,
          north: 37.77671248029149,
          east: -122.40475401970849,
        },
      },
      place_id: 'Eig0MDkgN3RoIFN0LCBTYW4gRnJhbmNpc2NvLCBDQSA5NDEwMywgVVNBIhsSGQoUChIJfx4UESp-j4ARWXjJGrXQVm8QmQM',
      types: [
        'street_address',
      ],
    },
    {
      address_components: [
        {
          long_name: 'Dwight D. Eisenhower Highway',
          short_name: 'Interstate 80',
          types: [
            'route',
          ],
        },
        {
          long_name: 'Mission Bay',
          short_name: 'Mission Bay',
          types: [
            'neighborhood',
            'political',
          ],
        },
        {
          long_name: 'San Francisco',
          short_name: 'SF',
          types: [
            'locality',
            'political',
          ],
        },
        {
          long_name: 'San Francisco County',
          short_name: 'San Francisco County',
          types: [
            'administrative_area_level_2',
            'political',
          ],
        },
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
        {
          long_name: '94103',
          short_name: '94103',
          types: [
            'postal_code',
          ],
        },
      ],
      formatted_address: 'Dwight D. Eisenhower Hwy, San Francisco, CA 94103, USA',
      geometry: {
        bounds: {
          south: 37.7752459,
          west: -122.40615960000002,
          north: 37.7754218,
          east: -122.40602690000003,
        },
        location: {
          lat: 37.775333,
          lng: -122.40609139999998,
        },
        location_type: 'GEOMETRIC_CENTER',
        viewport: {
          south: 37.7739848697085,
          west: -122.40744223029151,
          north: 37.7766828302915,
          east: -122.40474426970849,
        },
      },
      place_id: 'ChIJV9dOESp-j4ARElOdy_GkNyA',
      types: [
        'route',
      ],
    },
    {
      address_components: [
        {
          long_name: 'Mission Bay',
          short_name: 'Mission Bay',
          types: [
            'neighborhood',
            'political',
          ],
        },
        {
          long_name: 'San Francisco',
          short_name: 'SF',
          types: [
            'locality',
            'political',
          ],
        },
        {
          long_name: 'San Francisco County',
          short_name: 'San Francisco County',
          types: [
            'administrative_area_level_2',
            'political',
          ],
        },
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
      ],
      formatted_address: 'Mission Bay, San Francisco, CA, USA',
      geometry: {
        bounds: {
          south: 37.7656442,
          west: -122.40668399999998,
          north: 37.781992,
          east: -122.38145589999999,
        },
        location: {
          lat: 37.7730963,
          lng: -122.3929225,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          south: 37.7656442,
          west: -122.40668399999998,
          north: 37.781992,
          east: -122.38145589999999,
        },
      },
      place_id: 'ChIJB3qJC9F_j4ARsE8kZ77OBp8',
      types: [
        'neighborhood',
        'political',
      ],
    },
    {
      address_components: [
        {
          long_name: '94103',
          short_name: '94103',
          types: [
            'postal_code',
          ],
        },
        {
          long_name: 'San Francisco',
          short_name: 'SF',
          types: [
            'locality',
            'political',
          ],
        },
        {
          long_name: 'San Francisco County',
          short_name: 'San Francisco County',
          types: [
            'administrative_area_level_2',
            'political',
          ],
        },
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
      ],
      formatted_address: 'San Francisco, CA 94103, USA',
      geometry: {
        bounds: {
          south: 37.7635609,
          west: -122.42865710000001,
          north: 37.78805,
          east: -122.39781640000001,
        },
        location: {
          lat: 37.7726402,
          lng: -122.40991539999999,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          south: 37.7635609,
          west: -122.42865710000001,
          north: 37.78805,
          east: -122.39781640000001,
        },
      },
      place_id: 'ChIJ09mpM52AhYARm2WOMfyfxhs',
      types: [
        'postal_code',
      ],
    },
    {
      address_components: [
        {
          long_name: 'San Francisco County',
          short_name: 'San Francisco County',
          types: [
            'administrative_area_level_2',
            'political',
          ],
        },
        {
          long_name: 'San Francisco',
          short_name: 'SF',
          types: [
            'locality',
            'political',
          ],
        },
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
      ],
      formatted_address: 'San Francisco County, San Francisco, CA, USA',
      geometry: {
        bounds: {
          south: 37.6398299,
          west: -123.17382499999997,
          north: 37.9298239,
          east: -122.28178000000003,
        },
        location: {
          lat: 37.7749073,
          lng: -122.41938779999998,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          south: 37.6398299,
          west: -123.17382499999997,
          north: 37.9298239,
          east: -122.28178000000003,
        },
      },
      place_id: 'ChIJIQBpAG2ahYARUksNqd0_1h8',
      types: [
        'administrative_area_level_2',
        'political',
      ],
    },
    {
      address_components: [
        {
          long_name: 'San Francisco',
          short_name: 'SF',
          types: [
            'locality',
            'political',
          ],
        },
        {
          long_name: 'San Francisco County',
          short_name: 'San Francisco County',
          types: [
            'administrative_area_level_2',
            'political',
          ],
        },
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
      ],
      formatted_address: 'San Francisco, CA, USA',
      geometry: {
        bounds: {
          south: 37.6398299,
          west: -123.17382499999997,
          north: 37.9298239,
          east: -122.28178000000003,
        },
        location: {
          lat: 37.7749295,
          lng: -122.41941550000001,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          south: 37.6398299,
          west: -123.17382499999997,
          north: 37.9298239,
          east: -122.28178000000003,
        },
      },
      place_id: 'ChIJIQBpAG2ahYAR_6128GcTUEo',
      types: [
        'locality',
        'political',
      ],
    },
    {
      address_components: [
        {
          long_name: 'California',
          short_name: 'CA',
          types: [
            'administrative_area_level_1',
            'political',
          ],
        },
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
      ],
      formatted_address: 'California, USA',
      geometry: {
        bounds: {
          south: 32.528832,
          west: -124.48200300000002,
          north: 42.0095169,
          east: -114.13121100000001,
        },
        location: {
          lat: 36.778261,
          lng: -119.41793239999998,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          south: 32.528832,
          west: -124.48200300000002,
          north: 42.0095169,
          east: -114.13121100000001,
        },
      },
      place_id: 'ChIJPV4oX_65j4ARVW8IJ6IJUYs',
      types: [
        'administrative_area_level_1',
        'political',
      ],
    },
    {
      address_components: [
        {
          long_name: 'United States',
          short_name: 'US',
          types: [
            'country',
            'political',
          ],
        },
      ],
      formatted_address: 'United States',
      geometry: {
        bounds: {
          south: 18.7763,
          west: 170.59569999999997,
          north: 71.5388001,
          east: -66.88541700000002,
        },
        location: {
          lat: 37.09024,
          lng: -95.71289100000001,
        },
        location_type: 'APPROXIMATE',
        viewport: {
          south: 18.7763,
          west: 170.59569999999997,
          north: 71.5388001,
          east: -66.88541700000002,
        },
      },
      place_id: 'ChIJCzYy5IS16lQRQrfeQ5K5Oxw',
      types: [
        'country',
        'political',
      ],
    },
  ]

  it('Results is array', () => {
    expect(Array.isArray(results)).to.equal(true)
  })

  it('Geosuggest.filterMapResults() is defined', () => {
    const geosuggest = new Geosuggest({
      value: {
        address: '',
        exact: true,
        lat: 0,
        lng: 0,
        location_type_id: null,
        place_id: '',
        side_id: null,
      },
    })
    expect(geosuggest.filterMapResults(results)).toBeDefined()
  })

  it('Highway request returns first address with type "route"', () => {
    mocker.mock('stores/location_type_store', {
      getLocationTypeById: () => ({ name: 'Highway' }),
    })
    const geosuggest = new Geosuggest({
      value: {
        address: '',
        exact: true,
        lat: 0,
        lng: 0,
        location_type_id: 5,
        place_id: '',
        side_id: null,
      },
    })
    expect(geosuggest.filterMapResults(results)).toHaveLength(1)
    expect(geosuggest.filterMapResults(results)[0].formatted_address).toBe('Dwight D. Eisenhower Hwy, San Francisco, CA 94103, USA')
  })

  it('Intersection request returns first address with type "transit_station"', () => {
    mocker.mock('stores/location_type_store', {
      getLocationTypeById: () => ({ name: 'Intersection' }),
    })
    const geosuggest = new Geosuggest({
      value: {
        address: '',
        exact: true,
        lat: 0,
        lng: 0,
        location_type_id: 5,
        place_id: '',
        side_id: null,
      },
    })
    expect(geosuggest.filterMapResults(results)).toHaveLength(2)
    expect(geosuggest.filterMapResults(results)[0].formatted_address).toBe('Harrison St & 7th St, San Francisco, CA 94103, USA')
  })

  it('Fallback returns all results and presents first address', () => {
    mocker.mock('stores/location_type_store', {
      getLocationTypeById: () => ({ name: 'Residence' }),
    })
    const geosuggest = new Geosuggest({
      value: {
        address: '',
        exact: true,
        lat: 0,
        lng: 0,
        location_type_id: 5,
        place_id: '',
        side_id: null,
      },
    })
    expect(geosuggest.filterMapResults(results)).toHaveLength(10)
    expect(geosuggest.filterMapResults(results)[0].formatted_address).toBe('1098 Harrison St, San Francisco, CA 94103, USA')
  })
})
