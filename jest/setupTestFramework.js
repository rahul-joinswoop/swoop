import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import chaiJestSnapshot from 'chai-jest-snapshot'
import chaiSubset from 'chai-subset'
import chaiJestDiff from 'chai-jest-diff'
import chaiAsPromised from 'chai-as-promised'
import { MOBILE_BREAKPOINT } from 'lib/Responsive'

const resizeEvent = document.createEvent('Event')
resizeEvent.initEvent('resize', true, true)

global.window.resizeTo = (width, height) => {
  global.window.innerWidth = width || global.window.innerWidth
  global.window.innerHeight = height || global.window.innerHeight
  global.window.dispatchEvent(resizeEvent)
}

global.resizeDesktop = () => global.window.resizeTo(MOBILE_BREAKPOINT)
global.resizeMobile = () => global.window.resizeTo(MOBILE_BREAKPOINT - 1)

global.google = {
  maps: {
    geometry: {
      poly: {
        isLocationOnEdge: jest.fn(),
      },
      spherical: {
        computeDistanceBetween: jest.fn(),
        computeHeading: jest.fn(),
      },
    },
    ControlPosition: {
      LEFT_BOTTOM: 6,
    },
    DirectionsService: jest.fn(() => ({
      route: jest.fn(),
    })),
    DirectionsStatus: {
      OK: 'OK',
    },
    InfoWindow: jest.fn(() => ({
      addListener: jest.fn(),
      open: jest.fn(),
      setContent: jest.fn(),
      setZIndex: jest.fn(),
    })),
    Marker: jest.fn(() => ({
      addListener: jest.fn(),
      getMap: jest.fn(),
      setIcon: jest.fn(),
      setPosition: jest.fn(),
      setVisible: jest.fn(),
    })),
    LatLng: jest.fn(),
    LatLngBounds: jest.fn(),
    Map() { return { addListener: jest.fn(), fitBounds: jest.fn() } },
    event: {
      trigger: jest.fn(),
      addListenerOnce: jest.fn(),
    },
    Point: class Point {},
    Polyline: jest.fn(() => ({
      setOptions: jest.fn(),
    })),
    Size: class Size {},
    TravelMode: {
      DRIVING: 'DRIVING',
    },
  },
}
Enzyme.configure({ adapter: new Adapter() })

// TODO - this entire file is run once per test :-(
// look at this solution to speed all of this up
// https://medium.com/airbnb-engineering/unlocking-test-performance-migrating-from-mocha-to-jest-2796c508ec50

// use chai matchers for enzyme
chai.use(chaiEnzyme())

// add promise helpers
chai.use(chaiAsPromised)

// enable snapshot matching
chai.use(chaiJestSnapshot)

// enable nicer diffs in jest
chai.use(chaiJestDiff())

// chai-subset
chai.use(chaiSubset)

// Load Chai assertions
// chai + jest via https://gist.github.com/pahund/3abcc5212431cef3dae455d5285b7bd7

// Make sure chai and jasmine ".not" play nice together
const originalNot = Object.getOwnPropertyDescriptor(chai.Assertion.prototype, 'not').get
Object.defineProperty(chai.Assertion.prototype, 'not', {
  get() {
    Object.assign(this, this.assignedNot)
    return originalNot.apply(this)
  },
  set(newNot) {
    this.assignedNot = newNot
    return newNot
  },
})

// Combine both jest and chai matchers on expect
const jestExpect = global.expect

global.expect = (actual) => {
  const originalMatchers = jestExpect(actual)
  const chaiMatchers = chai.expect(actual)
  const combinedMatchers = Object.assign(chaiMatchers, originalMatchers)
  return combinedMatchers
}

// also grab static methods from jest
Object.entries(jestExpect).reduce((memo, [k, v]) => {
  Object.assign(memo, { [k]: v })
  return memo
}, global.expect)
