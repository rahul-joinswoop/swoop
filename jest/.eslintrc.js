module.exports = {
  plugins: [
    'chai-friendly',
  ],
  rules: {
    // TODO - do we actually want to enforce this?
    'no-plusplus': 0,
    // since we are using chai expecations to disable this rule
    'jest/valid-expect': 0,
    // disable original rule and use chai-friendly version instead
    'babel/no-unused-expressions': 0,
    'chai-friendly/no-unused-expressions': 2,
  },
  env: {
    jest: true,
  },
}
