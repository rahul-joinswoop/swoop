import Logger from 'lib/swoop/logger'

describe('Logger', () => it('Wrapper should set up keys', () => {
  const L = Logger.get('Storage')
  expect(L.DEBUG).to.exist
}))
