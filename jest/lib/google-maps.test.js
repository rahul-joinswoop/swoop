/* globals google */

import { getDistanceToStep } from 'lib/google-maps'

describe('google maps utils', () => {
  afterAll(() => {
    google.maps.geometry.spherical.computeDistanceBetween.mockClear()
  })

  it('getDistanceToStep works correctly', () => {
    const location = { lat: 345, lng: 234 }
    const step = {
      start_location: { lat: 536, lng: 554 },
      end_location: { lat: 626, lng: 826 },
    }

    google.maps.geometry.spherical.computeDistanceBetween.
    mockImplementation((from, to) => Math.sqrt((from.lat - to.lat) ** 2 + (from.lng - to.lng) ** 2))

    const distance = getDistanceToStep(location, step)
    expect(distance).toBe(372.6674120445736)
  })
})
