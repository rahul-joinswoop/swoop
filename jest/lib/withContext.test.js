import React from 'react'
import { mount } from 'enzyme'
import withContext from 'lib/withContext'

describe('withContext', () => {
  const a = { a: 1 }
  const b = { b: 2 }
  const ContextA = React.createContext()
  const ContextB = React.createContext()
  const Foo = () => <div id="foo" />
  const props = {
    a: 3, b: 4, c: 5, d: 6,
  }
  describe('with a single context', () => {
    const WrappedFoo = withContext({ a: ContextA })(Foo)
    const wrapAndMount = children =>
      mount(
        <ContextA.Provider value={a}>
          {children}
        </ContextA.Provider>
      )

    it('works without props', () => {
      const wrapper = wrapAndMount(<WrappedFoo />)
      expect(wrapper.find('Foo').props()).to.deep.equal({ a })
    })
    it('works with props', () => {
      const wrapper = wrapAndMount(<WrappedFoo {...props} />)
      expect(wrapper.find('Foo').props()).to.deep.equal({ ...props, a })
    })
  })

  describe('with multiple contexts', () => {
    const WrappedFoo = withContext({ a: ContextA, b: ContextB })(Foo)
    const wrapAndMount = children =>
      mount(
        <ContextA.Provider value={a}>
          <ContextB.Provider value={b}>
            {children}
          </ContextB.Provider>
        </ContextA.Provider>
      )

    it('works without props', () => {
      const wrapper = wrapAndMount(<WrappedFoo />)
      expect(wrapper.find('Foo').props()).to.deep.equal({ a, b })
    })
    it('works with props', () => {
      const wrapper = wrapAndMount(<WrappedFoo {...props} />)
      expect(wrapper.find('Foo').props()).to.deep.equal({ ...props, a, b })
    })
  })
})
