/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import '../../app/assets/javascripts/loader' // Mock out the set of user

import { mount } from 'enzyme'
import Status from 'components/job/status'
import React from 'react'
import mocker from '../mocker'

describe('JobStatus', () => {
  let instance = null

  const resetModules = (props) => {
    mocker.mock('stores/user_store', {})
    jest.resetModules()
    instance = mount(<Status {...props} />)
    return instance
  } // TODO: Test to make sure PO Works

  describe('Root should see scheduled', () => {
    it('should say scheduled when scheduled for root', () => {
      const job = {
        id: 1,
        scheduled_for: true,
        status: 'Pending',
      }
      mocker.mock('stores/user_store', {
        isSwoop() {
          return true
        },
      })
      mocker.mock('stores/job_store', {
        getJobById() {
          return job
        },
      }) // TODO: create fake job creator

      resetModules({
        getStatusStyle() {
          return {}
        },

        job,
      })
      expect(instance.find('.JobStatus-text').text()).to.equal('Scheduled')
    })
    it('should say pending when not scheduled for root', () => {
      const job = {
        id: 1,
        status: 'Pending',
        scheduled_for: null,
      }
      mocker.mock('stores/user_store', {
        isSwoop() {
          return true
        },
      })
      mocker.mock('stores/job_store', {
        getJobById() {
          return job
        },
      }) // TODO: create fake job creator

      resetModules({
        getStatusStyle() {
          return {}
        },

        job,
      })
      expect(instance.find('.JobStatus-text').text()).to.equal('Pending')
    })
    it('should say Choose Partner when Tesla and in pending with feature enabled', () => {
      const job = {
        id: 1,
        status: 'Pending',
        type: 'FleetInHouseJob',
        scheduled_for: null,
      }
      mocker.mock('stores/user_store', {
        isTesla() {
          return true
        },

        isFleetInHouse() {
          return true
        },

        isFleet() {
          return true
        },

        isRoot() {
          return false
        },
      })
      mocker.mock('stores/job_store', {
        getJobById() {
          return job
        },
      }) // TODO: create fake job creator

      resetModules({
        getStatusStyle() {
          return {}
        },

        job,
      })
      expect(instance.find('.JobStatus-text').text()).to.equal('Pending')
    })
  })
})
