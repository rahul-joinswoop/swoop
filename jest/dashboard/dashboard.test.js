import mocker from '../mocker'
import '../../app/assets/javascripts/loader' // Mock out the set of user
import Dashboard_cols from '../../app/assets/javascripts/dashboard_cols' // TODO: Test to make sure PO Works

global.google = {}
describe('Dashboard', () => {
  describe('checking partner for Tesla', () => it('should say choose partner when feature is turned on', () => {
    mocker.mock('stores/user_store', {
      isTesla() {
        return true
      },

      isFleet() {
        return true
      },

      isFleetInHouse() {
        return true
      },
    })
    mocker.mock('stores/feature_store', {
      isFeatureEnabled() {
        return true
      },
    })
    const Col = Dashboard_cols.getCols({
      isSortable() {
        return false
      },

      isFrontendSort() {
        return false
      },
    }).partner
    const col = new Col()
    col.setRecord({
      status: 'Pending',
      rescue_company_id: null,
    })
    expect(col.getValue()).to.exist
  }))
  describe('Checking scheduled time column', () => {
    it('should say created in header for non finish line', () => {
      mocker.mock('stores/user_store', {
        isFinishLine() {
          return false
        },
      })
      const Col = Dashboard_cols.getCols({
        isSortable() {
          return false
        },

        isFrontendSort() {
          return false
        },
      }).created_time
      const col = new Col()
      expect(col.getHeader()).to.equal('Created ()')
    })
    it('should say time in header for finish line', () => {
      mocker.mock('stores/user_store', {
        isFinishLine() {
          return true
        },
      })
      const Col = Dashboard_cols.getCols({
        isSortable() {
          return false
        },

        isFrontendSort() {
          return false
        },
      }).created_time
      const col = new Col()
      expect(col.getHeader()).to.equal('Time ()')
    })
    it('should say created value for non finish line', () => {
      mocker.mock('stores/user_store', {
        isFinishLine() {
          return false
        },
      })
      const Col = Dashboard_cols.getCols({
        isSortable() {
          return false
        },

        isFrontendSort() {
          return false
        },
      }).created_time
      const col = new Col()
      col.setRecord({
        history: {
          Created: {
            last_set: '10/10/86',
            scheduled_for: '10/28/89',
          },
        },
      })
      expect(col.getValue()).to.equal('10/10/86 00:00')
      col.setRecord({
        history: {
          Created: {
            adjusted_dttm: '10/10/87',
            last_set: '10/10/86',
            scheduled_for: '10/28/89',
          },
        },
      })
      expect(col.getValue()).to.equal('10/10/87 00:00')
    })
    it('should say scheduled value for finish line', () => {
      mocker.mock('stores/user_store', {
        isFinishLine() {
          return true
        },
      })
      const Col = Dashboard_cols.getCols({
        isSortable() {
          return false
        },

        isFrontendSort() {
          return false
        },
      }).created_time
      const col = new Col()
      col.setRecord({
        created_at: '10/10/86',
        scheduled_for: '10/28/89',
      })
      expect(col.getValue()).to.equal('10/28/89 00:00')
    })
  })
}) // mocker.mock(('stores/job_store'),
//    getJobById: (original, key) -> job
//  )
// #TODO: create fake job creator
// resetModules(
//  getStatusStyle: -> {}
//  job: job
// )
// expect(instance.getStatus()).to.equal("Scheduled")
