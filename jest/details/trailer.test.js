import React from 'react'
import TestUtils from 'react-dom/test-utils'
import { JobDetails } from 'components/job_details'
import '../../app/assets/javascripts/loader' // Mock out the set of user
import mocker from '../mocker'

jest.mock('components/maps/SwoopMap', () => () => null)

const Details = React.createFactory(JobDetails)


describe('JobDetails', () => {
  let instance = null

  const resetModules = (props) => {
    mocker.mock('stores/user_store', {
      getUser() {
        return {
          id: 1,
          company: {
            location_types: [
              {
                id: 10,
                name: 'Location Type 1',
              },
              {
                id: 20,
                name: 'Location Type 2',
              },
            ],
          },
        }
      },
    })
    jest.resetModules()
    instance = new Details(props)
    instance = TestUtils.renderIntoDocument(instance)
    return instance
  } // TODO: Test to make sure PO Works

  describe('Partner details', () => it('should show the trailer name', () => {
    const job = {
      id: 1,
      scheduled_for: true,
      trailer_vehicle_id: 12,
      status: 'Pending',
      toa: {
        latest: '2016-12-31T00:48:57.723Z',
      },
      issc_dispatch_request: {
        contractor_id: 12345,
      },
    }
    mocker.mock('stores/user_store', {
      getCompany() {
        return {
          id: 1,
        }
      },
      isPartner() {
        return true
      },
    })
    mocker.mock('stores/trailer_store', {
      get() {
        return 'Trailer 1'
      },
    })
    mocker.mock('stores/job_store', {
      getFullJobById() {
        return job
      },
    })
    mocker.mock('stores/map_store', {
      stealMap() {
        return job
      },
    }) // TODO: create fake job creator

    resetModules({
      id: job.id,
    })
    const row = TestUtils.findRenderedDOMComponentWithClass(instance, 'job_details')
    const text = row.textContent
    expect(text).to.match(/Trailer:/)
    expect(text).to.match(/Trailer 1/)
  }))
  describe('Non Partner details', () => it('should show the trailer name', () => {
    const job = {
      id: 1,
      scheduled_for: true,
      trailer_vehicle_id: 12,
      status: 'Pending',
      toa: {
        latest: '2016-12-31T00:48:57.723Z',
      },
      issc_dispatch_request: {
        contractor_id: 12345,
      },
    }
    mocker.mock('stores/user_store', {
      getCompany() {
        return {
          id: 1,
        }
      },
      isPartner() {
        return false
      },
    })
    mocker.mock('stores/trailer_store', {
      get() {
        return 'Trailer 1'
      },
    })
    mocker.mock('stores/job_store', {
      getFullJobById() {
        return job
      },
    }) // TODO: create fake job creator

    resetModules({
      id: job.id,
    })
    const row = TestUtils.findRenderedDOMComponentWithClass(instance, 'job_details')
    const text = row.textContent
    expect(text).not.to.match(/Trailer:/)
    expect(text).not.to.match(/Trailer 1/)
  }))
})
