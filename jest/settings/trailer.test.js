/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import '../../app/assets/javascripts/loader'

import React from 'react'
import TestUtils from 'react-dom/test-utils'

import mocker from '../mocker'

const TrailerList = React.createFactory(require('components/settings/trailer_list'))

describe('Invoice List', () => {
  mocker.mock('stores/trailer_store', {
    getAll() {
      return {
        1: {
          deleted_at: null,
          id: 62,
          make: 'Trailer mkae',
          model: 'Traile Model',
          name: 'Trailer 2',
          vehicle_category_id: 10,
        },
      }
    },
  })
  mocker.mock('stores/vehicle_category_store', {
    get() {
      return 'Category'
    },
  })
  let instance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new TrailerList(props)
    instance = TestUtils.renderIntoDocument(instance)
    return instance
  }

  describe('Trailers List', () => it('Trailers should show up', () => {
    resetModules({})
    const row = TestUtils.findRenderedDOMComponentWithClass(instance, 'swoopTable')
    const text = row.textContent
    expect(text).to.match(/Trailer mkae/)
    expect(text).to.match(/Traile Model/)
    expect(text).to.match(/Trailer 2/)
    expect(text).to.match(/Category/)
  }))
})
