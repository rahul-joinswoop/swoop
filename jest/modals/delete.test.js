/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import '../../app/assets/javascripts/loader' // Mock out the set of user

import ReactTestUtils from 'react-dom/test-utils'
import React from 'react'
import Consts from 'consts'

import mocker from '../mocker'

const DeleteModal = React.createFactory(require('components/modals/delete_job'))

describe('Job Delete', () => {
  mocker.mock('stores/user_store', {})
  let instance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new DeleteModal(props)
    instance = ReactTestUtils.renderIntoDocument(instance)
    return instance
  }

  describe('Delete Modal', () => {
    it('should say id in header', () => {
      const job = {
        id: 14,
      }
      resetModules({
        record: job,
      })
      expect(instance.getTitle()).to.equal('14: cancel job')
    })
    it('Should update title when blocked', () => {
      const job = {
        id: 14,
        child_job: 3,
        type: 'FleetInHouseJob',
        owner_company: {
          id: 1,
        },
        rescue_company: {
          id: 2,
        },
      }
      resetModules({
        record: job,
      })
      mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },
      })
      expect(instance.isBlocked()).to.be.true
      expect(instance.getTitle()).to.match(/Unable to /)
    })
    it('should handle parent jobs in header', () => {
      const job = {
        id: 3,
        original_job_id: 14,
      }
      resetModules({
        record: job,
      })
      expect(instance.getTitle()).to.equal('14:B: cancel job')
    })
    it('should handle child jobs in header', () => {
      const job = {
        id: 14,
        child_job: 3,
      }
      resetModules({
        record: job,
      })
      expect(instance.getTitle()).to.equal('14:A: cancel job')
    })
    it('return reject if from fleet and assigned', () => {
      const job = {
        id: 14,
        child_job: 3,
        type: 'FleetInHouseJob',
        status: Consts.ASSIGNED,
        owner_company: {
          id: 1,
        },
        rescue_company: {
          id: 2,
        },
      }
      resetModules({
        record: job,
      })
      mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },
      })
      expect(instance.getStatus()).to.equal('reject')
    })
    it('return cancel if rescue job', () => {
      const job = {
        id: 14,
        child_job: 3,
        type: 'FleetInHouseJob',
        status: Consts.ASSIGNED,
        owner_company: {
          id: 1,
        },
        rescue_company: {
          id: 1,
        },
      }
      resetModules({
        record: job,
      })
      mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },
      })
      expect(instance.getStatus()).to.equal('cancel')
    })
    it('Should be blocked if partner and not assigned', () => {
      const job = {
        id: 14,
        child_job: 3,
        type: 'FleetInHouseJob',
        owner_company: {
          id: 1,
        },
        rescue_company: {
          id: 2,
        },
      }
      resetModules({
        record: job,
      })
      mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },
      })
      expect(instance.isBlocked()).to.be.true
      expect(JSON.stringify(instance.render())).to.match(/Please contact/)
    })
    it('Should not be blocked if not partner and not assigned', () => {
      const job = {
        id: 14,
        child_job: 3,
        type: 'FleetInHouseJob',
        owner_company: {
          id: 1,
        },
        rescue_company: {
          id: 2,
        },
      }
      resetModules({
        record: job,
      })
      mocker.mock('stores/user_store', {
        isPartner() {
          return false
        },
      })
      expect(instance.isBlocked()).to.be.false
      expect(JSON.stringify(instance.render())).to.match(/Are you sure/)
    })
    it('Should properly call props onConfirm', () => {
      const job = {
        id: 14,
        child_job: 3,
        type: 'FleetInHouseJob',
        status: Consts.ASSIGNED,
        owner_company: {
          id: 1,
        },
        rescue_company: {
          id: 1,
        },
      }
      const func = jest.fn()
      resetModules({
        record: job,
        onConfirm: func,
      })
      mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },
      })
      instance.onConfirm()
      expect(func).toHaveBeenCalled()
    })
    it('Should properly call props onConfirm even if partnerview', () => {
      const job = {
        id: 14,
        child_job: 3,
        type: 'FleetInHouseJob',
        status: Consts.ASSIGNED,
        owner_company: {
          id: 1,
        },
        rescue_company: {
          id: 2,
        },
      }
      const func = jest.fn()
      resetModules({
        record: job,
        onConfirm: func,
      })
      mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },
      })
      instance.onConfirm()
      expect(func).toHaveBeenCalled()
    })
  })
})
