/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'

import TestUtils from 'react-dom/test-utils'

import mocker from '../mocker'
import '../../app/assets/javascripts/loader'

const SiteForm = React.createFactory(require('components/fleet_site_form')) // Mock out the set of user

describe('Job Site Form', () => {
  mocker.mock('stores/user_store', {
    isPartner() {
      return true
    },

    getUser() {
      return {
        id: 1,
        company: {
          location_types: [
            {
              id: 10,
              name: 'Location Type 1',
            },
            {
              id: 20,
              name: 'Location Type 2',
            },
          ],
        },
      }
    },
  })
  let instance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new SiteForm(props)
    return TestUtils.renderIntoDocument(instance)
  }

  describe('Site Form Modal', () => {
    it.skip('Should allow editing name, address, phone, and location id', () => {
      const job = {
        id: 14,
        type: 'FleetMotorClubJob',
      }
      resetModules({
        record: job,
      })

      const fields = instance.render().props.cols[0].fields.map(col => col.name)

      expect(fields).to.include('name')
      expect(fields).to.include('location')
      expect(fields).to.include('phone')
      expect(fields).to.include('site_code')
      expect(fields).to.include('location.location_type_id')
    })
    it.skip('Should only require name and site_code', () => {
      const job = {
        id: 14,
        type: 'FleetMotorClubJob',
      }
      resetModules({
        record: job,
      })

      const fields = instance.render().props.cols[0].fields.map(col => col.name + col.required)

      expect(fields).to.include('nametrue')
      expect(fields).to.include('locationtrue')
      expect(fields).to.include('phonefalse')
      expect(fields).to.include('site_codetrue')
      expect(fields).to.include('location.location_type_idfalse')
    })
  })
})
