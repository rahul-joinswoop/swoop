import React from 'react'
import { shallow } from 'enzyme'
import DateSelector from 'components/time_pickers/date_selector'

describe('Date Selector', () => {
  let component

  it('<DateSelector /> exists', () => {
    component = shallow(<DateSelector />)
    expect(component).toBeDefined()
  })
})
