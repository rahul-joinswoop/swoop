import React from 'react'
import { shallow } from 'enzyme'
import TimeSelector from 'components/time_pickers/time_selector'

describe('Time Selector', () => {
  let component

  it('<TimeSelector /> exists', () => {
    component = shallow(<TimeSelector />)
    expect(component).toBeDefined()
  })
})
