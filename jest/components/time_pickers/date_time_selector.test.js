import React from 'react'
import { shallow } from 'enzyme'
import DateTimeSelector from 'components/time_pickers/date_time_selector'
import moment from 'moment-timezone'

describe('Date Time Selector', () => {
  let component

  it('<DateTimeSelector /> exists', () => {
    component = shallow(<DateTimeSelector />)
    expect(component).toBeDefined()
  })

  it('handleDateChange updates date', () => {
    component = shallow(<DateTimeSelector />)
    component.setState({
      date: null,
      time: null,
      previousValue: null,
    })
    component.instance().handleDateChange(moment(new Date()))
    expect(component.state().date).not.toBeNull()
    expect(component.state().time).toBeNull()
  })

  it('handleTimeChange updates time', () => {
    component = shallow(<DateTimeSelector />)
    component.setState({
      date: null,
      time: null,
      previousValue: null,
    })
    component.instance().handleTimeChange(moment(new Date()))
    expect(component.state().date).toBeNull()
    expect(component.state().time).not.toBeNull()
  })

  it('_getDate defined ONLY when supplied both date and time', () => {
    const timeStamp = moment(new Date())
    component = shallow(<DateTimeSelector />)
    expect(component.instance()._getDate(timeStamp, timeStamp)).toBeDefined()
    expect(component.instance()._getDate(null, timeStamp)).toBeNull()
    expect(component.instance()._getDate(timeStamp, null)).toBeNull()
    expect(component.instance()._getDate(null, null)).toBeNull()
  })
})
