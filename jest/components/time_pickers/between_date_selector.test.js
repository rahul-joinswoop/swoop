import React from 'react'
import { shallow } from 'enzyme'
import BetweenDateSelector from 'components/time_pickers/between_date_selector'

describe('Between Date Selector', () => {
  let component

  it('<BetweenDateSelector /> exists', () => {
    component = shallow(<BetweenDateSelector />)
    expect(component).toBeDefined()
  })

  it('Has two DateSelectors', () => {
    component = shallow(<BetweenDateSelector />)
    expect(component.find('.between_date_selector').children()).to.have.lengthOf(2)
  })
})
