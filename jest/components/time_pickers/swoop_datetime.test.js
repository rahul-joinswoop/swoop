import React from 'react'
import { shallow, mount } from 'enzyme'
import SwoopDateTime from 'components/time_pickers/swoop_datetime'

describe('Swoop Date Time', () => {
  let component

  it('<SwoopDateTime /> exists', () => {
    component = shallow(<SwoopDateTime />)
    expect(component).toBeDefined()
  })

  it('Events mount and unmount w/ component', () => {
    const events = {}
    document.addEventListener = jest.fn((e, cb) => {
      events[e] = cb
    })
    component = mount(<SwoopDateTime />)
    expect(events.keydown).to.exist

    document.removeEventListener = jest.fn((e) => {
      delete events[e]
    })
    component.unmount()
    expect(events.keydown).not.to.exist
  })

  it('getValue() fires', () => {
    component = shallow(<SwoopDateTime />)
    component.setState({ value: 1 })
    expect(component.instance().getValue()).to.equal(1)
  })

  it('State handles invalid input', () => {
    component = shallow(<SwoopDateTime />)
    component.setState({ value: 1 })
    component.instance().setNewState('Bad Input')
    expect(component.state().value).toBeNull()
  })
})
