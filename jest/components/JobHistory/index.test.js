import React from 'react'
import { mount } from 'enzyme'
import JobHistory from 'components/JobHistory'
import JobStore from 'stores/job_store'
import 'loader' // delete this file when we have converted coffeescript to JS

JobStore.addRemoteItem({
  target: {
    id: 232499,
    created_at: '2019-06-19T00:27:46.827Z',
    uuid: 'c975806d09',
    history: {
      Created: {
        id: 1193310,
        last_set: '2019-06-19T00:27:46.827Z',
        last_set_dttm: '2019-06-19T00:27:46.827Z',
        type: 'History',
        name: 'Created',
        sort_date: '2019-06-19T00:27:46.827Z',
      },
      'Auto Assigning': {
        id: 1193312,
        last_set: '2019-06-19T00:27:46.832Z',
        last_set_dttm: '2019-06-19T00:27:46.832Z',
        type: 'History',
        name: 'Auto Assigning',
        sort_date: '2019-06-19T00:27:46.832Z',
      },
      'Confirmation Text Undelivered_160125': {
        id: 160125,
        last_set: '2019-06-19T00:27:47.313Z',
        last_set_dttm: '2019-06-19T00:27:47.313Z',
        adjusted_dttm: '2019-06-19T00:27:47.313Z',
        type: 'SMS',
        name: 'Confirmation Text Undelivered',
        sort_date: '2019-06-19T00:27:47.313Z',
      },
    },
    distance: 1.8,
  },
})

describe('JobHistory Component', () => {
  let wrapper

  it('renders when items passed in', () => {
    wrapper = mount(<JobHistory record={{ id: 232499 }} />)
    expect(wrapper).toBeDefined()
  })

  it('shows history items', () => {
    wrapper = mount(<JobHistory record={{ id: 232499 }} />)
    expect(wrapper.find('.JobHistory-row')).to.have.lengthOf(2)
  })
})
