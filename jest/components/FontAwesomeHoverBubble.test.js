import React from 'react'
import { shallow } from 'enzyme'
import FontAwesomeHoverBubble from 'components/FontAwesomeHoverBubble'

describe('FontAwesomeHoverBubble Component', () => {
  let wrapper

  const bubbleContents = "I'm in a bubble!"
  const className = 'fa-times-circle'

  it('it renders', () => {
    wrapper = shallow(<FontAwesomeHoverBubble />)
    expect(wrapper).toBeDefined()
  })

  it('it renders with className that is passed in', () => {
    wrapper = shallow(<FontAwesomeHoverBubble className={className} />)
    expect(wrapper.find('.font-awesome-hover-bubble').hasClass(className)).to.equal(true)
  })

  it('it renders with bubbleContents that is passed in', () => {
    wrapper = shallow(<FontAwesomeHoverBubble bubbleContents={bubbleContents} />)
    expect(wrapper.find('.bubble')).to.have.text(bubbleContents)
  })

  it('it renders with flipToBottom if prop passed in', () => {
    wrapper = shallow(<FontAwesomeHoverBubble flipToBottom />)
    expect(wrapper.find('.font-awesome-hover-bubble').hasClass('flip')).to.equal(true)
  })
})
