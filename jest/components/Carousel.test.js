import React from 'react'
import { shallow, mount } from 'enzyme'
import Carousel from 'components/Carousel'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('Carousel Component', () => {
  let wrapper

  const items = [
    <div className="item" key="item-1">Item 1</div>,
    <div className="item" key="item-2">Item 2</div>,
  ]

  it('renders when items passed in', () => {
    wrapper = mount(<Carousel items={items} />)
    expect(wrapper).toBeDefined()
  })

  it('does not render when no items passed in', () => {
    wrapper = shallow(<Carousel items={[]} />)
    expect(wrapper.type()).toEqual(null)
  })

  it('displays the same number of items as passed in', () => {
    wrapper = mount(<Carousel items={items} />)
    expect(wrapper.find('.item')).to.have.lengthOf(2)
  })

  describe('scrollbar mask', () => {
    it('does not render by default', () => {
      wrapper = mount(<Carousel items={items} />)
      expect(wrapper.find('.scrollbar-mask').exists()).to.equal(false)
    })

    it('renders when hideScrollBar prop passed in', () => {
      wrapper = mount(<Carousel hideScrollBar items={items} />)
      expect(wrapper.find('.scrollbar-mask').exists()).to.equal(true)
    })
  })

  describe('buttons', () => {
    it('two are rendered', () => {
      wrapper = mount(<Carousel items={items} />)
      expect(wrapper.find('.slider-button')).to.have.lengthOf(2)
    })

    it('default button size is 20px', () => {
      wrapper = mount(<Carousel items={items} />)
      const buttonStyle = wrapper.find('.slider-button').get(0).props.style
      expect(buttonStyle).toHaveProperty('fontSize', '20px')
    })

    it('button width is 4px larger than its size', () => {
      wrapper = mount(<Carousel items={items} />)
      const buttonStyle = wrapper.find('.slider-button').get(0).props.style
      expect(buttonStyle).toHaveProperty('width', '24px')
    })

    it('default button top is 50%', () => {
      wrapper = mount(<Carousel items={items} />)
      const buttonStyle = wrapper.find('.slider-button').get(0).props.style
      expect(buttonStyle).toHaveProperty('top', '50%')
    })

    it('button size prop sets button size', () => {
      wrapper = mount(<Carousel buttonSize={50} items={items} />)
      const buttonStyle = wrapper.find('.slider-button').get(0).props.style
      expect(buttonStyle).toHaveProperty('fontSize', '50px')
    })

    it('button top prop sets button top', () => {
      wrapper = mount(<Carousel buttonTop="10px" items={items} />)
      const buttonStyle = wrapper.find('.slider-button').get(0).props.style
      expect(buttonStyle).toHaveProperty('top', '10px')
    })
  })

  describe('componentDidMount', () => {
    it('if carousel renders with items, resize listener should be added to window', () => {
      const map = {}
      window.addEventListener = jest.fn((event, cb) => {
        map[event] = cb
      })
      wrapper = mount(<Carousel buttonSize={50} items={items} />)
      expect(map.resize).to.exist
    })
    it('if carousel renders without items, resize listener should not be added to window', () => {
      const map = {}
      window.addEventListener = jest.fn((event, cb) => {
        map[event] = cb
      })
      wrapper = mount(<Carousel buttonSize={50} items={[]} />)
      expect(map.resize).not.to.exist
    })
    it('if carousel renders with items, slider should be set in state', () => {
      wrapper = mount(<Carousel buttonSize={50} items={items} />)
      expect(wrapper.instance().state.slider).toBeTruthy()
    })
    it('if carousel renders without items, slider in state will be null', () => {
      wrapper = mount(<Carousel buttonSize={50} items={[]} />)
      expect(wrapper.instance().state.slider).toEqual(null)
    })
  })
  describe('componentWillUnmount', () => {
    it('if carousel mounts with items, resize listener should be added to window, then should be removed on unmount', () => {
      const map = {}
      window.addEventListener = jest.fn((event, cb) => {
        map[event] = cb
      })
      window.removeEventListener = jest.fn((event) => {
        delete map[event]
      })
      wrapper = mount(<Carousel buttonSize={50} items={items} />)
      expect(map.resize).to.exist
      wrapper.unmount()
      expect(map.resize).not.to.exist
    })
  })
})
