import React from 'react'
import { shallow, mount } from 'enzyme'
import Tabs from 'components/tabs'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('Tabs Component', () => {
  let wrapper
  const className = ''
  let tabList = [
    {
      name: 'Companies',
      component: () => {},
    },
    {
      name: 'Users',
      component: () => {},
    },
  ]

  it('exists upon mounting', () => {
    wrapper = shallow(<Tabs className={className} tabList={tabList} />)
    expect(wrapper).toBeDefined()
  })

  it('receives initial state', () => {
    wrapper = shallow(<Tabs className={className} tabList={tabList} />)
    expect(wrapper.state).toBeDefined()
  })

  describe('Tabs list', () => {
    it('is same length as number of tabs passed in as props', () => {
      wrapper = mount(<Tabs className={className} tabList={tabList} />)
      expect(wrapper.find('li')).to.have.lengthOf(2)
    })

    it('tab names appear in order of tabList prop', () => {
      wrapper = shallow(<Tabs className={className} tabList={tabList} />)
      wrapper.find('li').forEach((node, index) => {
        expect(node.text()).to.equal(tabList[index].name)
      })
    })

    it('does not show when only one tab passed into props', () => {
      tabList = [
        {
          name: 'Companies',
          component: () => {},
        },
      ]
      wrapper = mount(<Tabs className={className} tabList={tabList} />)
      expect(wrapper.find('li')).to.have.lengthOf(0)
    })

    it('hidden tabs do not appear in list', () => {
      tabList = [
        {
          name: 'Companies',
          component: () => {},
        },
        {
          name: 'Users',
          component: () => {},
        },
        {
          name: 'Tools',
          component: () => {},
          hideTab: true,
        },
      ]
      wrapper = mount(<Tabs className={className} tabList={tabList} />)
      expect(wrapper.find('li')).to.have.lengthOf(2)
    })
  })

  describe('Tab', () => {
    tabList = [
      {
        name: 'Companies',
        component: () => {},
      },
      {
        name: 'Users',
        component: () => {},
      },
    ]

    it('renders with a className', () => {
      wrapper = shallow(<Tabs className={className} tabList={tabList} />)
      wrapper.find('li').forEach((node) => {
        expect(node).to.have.className(className)
      })
    })

    it('is active when clicked on', () => {
      wrapper = shallow(<Tabs className={className} tabList={tabList} />)
      wrapper.find('li').forEach((node) => {
        node.find('a').simulate('click')
        expect(node).to.have.className('active')
      })
    })
  })

  describe('Bottom Components', () => {
    it('render when passed in as props', () => {
      const BottomComponent = props => <div className={props.className}>Hello</div>
      wrapper = shallow(<Tabs bottomComponents={<BottomComponent className="bottom-component-test" />} className={className} tabList={tabList} />)
      expect(wrapper.find('.bottom-component-test').exists()).to.equal(true)
    })

    it('do not render if none passed in', () => {
      wrapper = shallow(<Tabs className={className} tabList={tabList} />)
      expect(wrapper.find('.bottom-component-test').exists()).to.equal(false)
    })
  })

  describe('Side Components', () => {
    it('render when passed in as props', () => {
      const SideComponent = props => <div className={props.className}>Hello</div>
      wrapper = shallow(<Tabs className={className} sideComponents={<SideComponent className="side-component-test" />} tabList={tabList} />)
      expect(wrapper.find('.side-component-test').exists()).to.equal(true)
    })

    it('do not render if none passed in', () => {
      wrapper = shallow(<Tabs className={className} tabList={tabList} />)
      expect(wrapper.find('.side-component-test').exists()).to.equal(false)
    })
  })


  describe('Component Passed into Tabs', () => {
    it('renders if it is under current tab', () => {
      tabList = [
        {
          name: 'Companies',
          component: () => <span className="tab-component-0">Hello</span>,
        },
        {
          name: 'Users',
          component: () => <span className="tab-component-1">Hello</span>,
        },
      ]
      wrapper = shallow(<Tabs className={className} tabList={tabList} />)
      wrapper.setState({ currentTab: 0 })
      expect(wrapper.find('.tab-component-0').exists()).to.equal(true)
      wrapper.setState({ currentTab: 1 })
      expect(wrapper.find('.tab-component-1').exists()).to.equal(true)
    })
    it('does not render if it is not under current tab', () => {
      tabList = [
        {
          name: 'Companies',
          component: () => <span className="tab-component-0">Hello</span>,
        },
        {
          name: 'Users',
          component: () => <span className="tab-component-1">Hello</span>,
        },
      ]
      wrapper = shallow(<Tabs className={className} tabList={tabList} />)
      wrapper.setState({ currentTab: 0 })
      expect(wrapper.find('.tab-component-1').exists()).to.equal(false)
      wrapper.setState({ currentTab: 1 })
      expect(wrapper.find('.tab-component-0').exists()).to.equal(false)
    })
  })
})
