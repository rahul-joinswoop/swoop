import React from 'react'
import { mount } from 'enzyme'
import CarouselModal from 'components/CarouselModal'
import CarouselModalItem from 'components/CarouselModalItem'

jest.mock('components/modals_new', () => {
  const mockTransitionModal = (props) => {
    const {
      allPhotoItems, photoItem, extraClasses, fullHeightModal, ...rest
    } = props
    return (
      <div {...rest} />
    )
  }
  return mockTransitionModal
})

describe('Modal Carousel Component', () => {
  const wrapper = mount(<CarouselModal allPhotoItems={[{}, {}, {}]} photoItem={{}} />)

  // This is a fake test, to inform you that TransitionModal cannot be mounted by Jest.
  it('Is difficult to test, because TransitionModal crashes Jest', () => {
    expect(true).toBe(true)
  })

  it('Component Mounts', () => {
    expect(wrapper).toBeDefined()
  })

  it('Updates its width in state', () => {
    expect(wrapper.state().modalWidth).not.toBe(0)
  })

  it('Receives props', () => {
    expect(wrapper.props().allPhotoItems).toHaveLength(3)
  })
})

describe('Modal Carousel Item Component', () => {
  const photoItem = {
    created_at: '2018-10-05T19:07:16.800Z',
    id: 10,
    location: {
      lat: 37.7786953,
      lng: -122.4112873,
    },
    url: './image.jpg',
  }
  const photoItemWithoutLocation = {
    created_at: '2018-10-05T19:07:16.800Z',
    id: 11,
    url: './image.jpg',
  }
  const completePhotoItem = mount(<CarouselModalItem photoItem={photoItem} width={1024} />)
  const incompletePhotoItem = mount(<CarouselModalItem photoItem={photoItemWithoutLocation} width={1024} />)

  it('Receives `photoItem` as prop', () => {
    expect(completePhotoItem.props().photoItem).toBeDefined()
  })

  it('Renders Maps Link for Location', () => {
    expect(completePhotoItem.find('span.maps-link')).toHaveLength(1)
  })

  it('Does NOT Render Maps Link when Missing Location', () => {
    expect(incompletePhotoItem.find('span.maps-link')).toHaveLength(0)
  })
})
