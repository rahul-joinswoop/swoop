import React from 'react'
import render, { contains, simulate } from 'test'
import APICredentials from 'components/settings/integrations/API_Credentials/api_credentials'
import Api from 'api'

jest.mock('api', () => ({
  getApiCredentials: jest.fn(),
  generateApiCredentials: jest.fn(),
  deleteApiCredentials: jest.fn(),
}))

jest.mock('stores/modal_store', () => ({
  showModal: jest.fn(),
  closeModals: jest.fn(),
}))

beforeEach(() => {
  Api.getApiCredentials.mockReset()
  Api.generateApiCredentials.mockReset()
  Api.deleteApiCredentials.mockReset()
})

describe('API Credentials Widget', () => {
  let component

  it('<APICredentials /> renders', () => {
    component = render(<APICredentials />)
    expect(component).toBeDefined()
  })

  it('mounting calls getApiCredentials and sets credentials into state', () => {
    const mockResp = []
    Api.getApiCredentials.mockImplementationOnce((req) => {
      req.success(mockResp)
    })
    component = render(<APICredentials />)

    expect(Api.getApiCredentials).toHaveBeenCalled()
    expect(component.querySelector('.generateKeys')).toBeTruthy()
    expect(component.querySelector('.api-error')).toBe(null)
  })

  it('mounting calls getApiCredentials and handles errors appropriately', () => {
    const mockResp = []
    Api.getApiCredentials.mockImplementationOnce((req) => {
      req.error(mockResp)
    })
    component = render(<APICredentials />)

    expect(Api.getApiCredentials).toHaveBeenCalled()
    expect(component.querySelector('.api-error')).toBeTruthy()
  })

  it('clicking generate button calls generateApiCredentials and updates component', () => {
    const mockResp = []
    const mockGenerateResp = {
      id: 1,
      name: 'Guy Hassaname',
      client_id: '12345',
      client_secret: '67890',
    }
    Api.getApiCredentials.mockImplementation((req) => {
      req.success(mockResp)
    })
    component = render(<APICredentials />)
    document.body.appendChild(component)

    Api.generateApiCredentials.mockImplementation((req) => {
      req.success(mockGenerateResp)
    })

    simulate('click', {}, component.querySelector('.generateKeys'))

    expect(Api.generateApiCredentials).toHaveBeenCalled()
    expect(component.querySelector('.apiDocs')).toBeTruthy()
    expect(component.querySelector('.revoke')).toBeTruthy()
    expect(contains(<ul><li><span>Client ID:</span> 12345</li><li><span>Client Secret:</span> 67890</li></ul>, component)).toBeTruthy()

    document.body.removeChild(component)
  })

  it('revokes keys', () => {
    const mockResp = [{
      id: 1,
      name: 'Guy Hassaname',
      client_id: '12345',
      client_secret: '67890',
    }]
    Api.getApiCredentials.mockImplementation((req) => {
      req.success(mockResp)
    })
    component = render(<APICredentials />)
    document.body.appendChild(component)

    Api.deleteApiCredentials.mockImplementation((id, req) => {
      req.success()
    })
    simulate('click', {}, component.querySelector('.revoke'))
    simulate('click', {}, component.querySelector('.component-button.primary'))

    expect(Api.deleteApiCredentials).toHaveBeenCalled()
    expect(component.querySelector('.generateKeys')).toBeTruthy()

    document.body.removeChild(component)
  })

  it('handles errors when revokes keys', () => {
    const mockResp = [{
      id: 1,
      name: 'Guy Hassaname',
      client_id: '12345',
      client_secret: '67890',
    }]
    Api.getApiCredentials.mockImplementation((req) => {
      req.success(mockResp)
    })
    component = render(<APICredentials />)
    document.body.appendChild(component)

    Api.deleteApiCredentials.mockImplementation((id, req) => {
      req.error()
    })
    simulate('click', {}, component.querySelector('.revoke'))
    simulate('click', {}, component.querySelector('.component-button.primary'))

    expect(Api.deleteApiCredentials).toHaveBeenCalled()
    expect(contains(<ul><li><span>Client ID:</span> 12345</li><li><span>Client Secret:</span> 67890</li></ul>, component)).toBeTruthy()

    document.body.removeChild(component)
  })
})
