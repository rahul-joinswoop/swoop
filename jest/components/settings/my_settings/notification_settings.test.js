import React from 'react'
import { shallow } from 'enzyme'
import NotificationSettings from 'components/settings/configure/my_settings/notification_settings'
import mocker from '../../../mocker'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('NotificationSettings component', () => {
  let user = null

  beforeEach(() => mocker.mock('stores/user_store', {
    isPartner() {
      return true
    },
    getUser() {
      return user
    },
    getCompany() {
      return {
        id: 10,
        available_user_notification_settings: [
          {
            notification_code: 'new_job',
            available_notification_channels: ['Call', 'Text'],
            default_notification_channels: ['Text'],
            description: 'New job assigned to ABC Towing',
            supported_roles: ['admin', 'dispatcher'],
          },
          {
            notification_code: 'eta_accepted',
            available_notification_channels: ['Call', 'Text'],
            default_notification_channels: ['Text'],
            description: 'ETA Accepted by Swoop',
            supported_roles: ['admin', 'dispatcher'],
          },
          {
            notification_code: 'job_dispatched',
            available_notification_channels: ['Call', 'Text'],
            default_notification_channels: ['Text'],
            description: 'Job dispatched to driver',
            supported_roles: ['driver'],
          },
        ],
      }
    },
  }))

  describe('when user has dispatcher and driver roles', () => {
    it('renders all available_notification_channels', () => {
      user = {
        id: 1,
        notification_settings: [],
        roles: ['rescue', 'dispatcher', 'driver'],
      }

      const setRefMock = () => {}
      const component = shallow(<NotificationSettings setRef={setRefMock} />)

      expect(component.find('.form-group')).to.have.lengthOf(3)
      expect(component.find('.form-group').at(0).children().find('label').
      text()).to.eq('New job assigned to ABC Towing')
      expect(component.find('.form-group').at(1).children().find('label').
      text()).to.eq('ETA Accepted by Swoop')
      expect(component.find('.form-group').at(2).children().find('label').
      text()).to.eq('Job dispatched to driver')
    })
  })

  describe('when user has only dispatcher role', () => {
    it('renders only the notification_settings that are supported by dispatchers', () => {
      user = {
        id: 1,
        notification_settings: [],
        roles: ['rescue', 'dispatcher'],
      }

      const setRefMock = () => {}
      const component = shallow(<NotificationSettings setRef={setRefMock} />)

      expect(component.find('.form-group')).to.have.lengthOf(2)
      expect(component.find('.form-group').at(0).children().find('label').
      text()).to.eq('New job assigned to ABC Towing')
      expect(component.find('.form-group').at(1).children().find('label').
      text()).to.eq('ETA Accepted by Swoop')
    })
  })

  describe('when user has only driver role', () => {
    it('renders the only notification_settings that is supported by drivers', () => {
      user = {
        id: 1,
        notification_settings: [],
        roles: ['rescue', 'driver'],
      }

      const setRefMock = () => {}
      const component = shallow(<NotificationSettings setRef={setRefMock} />)

      expect(component.find('.form-group')).to.have.lengthOf(1)
      expect(component.find('.form-group').at(0).children().find('label').
      text()).to.eq('Job dispatched to driver')
    })
  })
})
