import React from 'react'
import { shallow, mount } from 'enzyme'
import JobDetailCarouselItem from 'components/JobDetailCarouselItem'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('JobDetailCarouselItem Component', () => {
  let wrapper
  const item = <div className="item">Item</div>
  const handleOnClick = jest.fn(() => 'clicked')

  it('renders with photoItem prop passed in', () => {
    wrapper = shallow(<JobDetailCarouselItem photoItem={item} />)
    expect(wrapper).toBeDefined()
  })

  it('does not render when no item passed in', () => {
    wrapper = shallow(<JobDetailCarouselItem />)
    expect(wrapper.type()).toEqual(null)
  })

  it('if onClick passed in, clicking on the component calls onClick', () => {
    wrapper = mount(<JobDetailCarouselItem onClick={handleOnClick} photoItem={item} />)
    wrapper.find('.job-detail-carousel-item-container').simulate('click')
    expect(handleOnClick).toHaveBeenCalled()
  })
})
