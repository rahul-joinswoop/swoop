import React from 'react'
import { shallow } from 'enzyme'
import PolicyItem from 'components/JobForm/PolicyItem'
import 'utils'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('PolicyItem', () => {
  const policyItemProps = {
    policyId: 123,
    policyVehicle: {
      id: 456, vin: 'ABCDE', make: 'MakeBla', model: 'ModelBle', color: 'Black', year: 2017,
    },
    policyCustomer: { id: 789, name: 'John', postal_code: 12345 },
  }

  describe('#onHandleItemSelect()', () => {
    it('calls props.onHandleVehicleListItemSelect with expected params', () => {
      const onHandleVehicleListItemSelectMock = jest.fn()
      const props = {
        ...policyItemProps,
        onHandleVehicleListItemSelect: onHandleVehicleListItemSelectMock,
      }

      const wrapper = new PolicyItem(props)
      wrapper.onHandleItemSelect()

      expect(onHandleVehicleListItemSelectMock.mock.calls).toHaveLength(1)
      expect(onHandleVehicleListItemSelectMock).toHaveBeenCalledWith(
        expect.objectContaining(policyItemProps)
      )
    })
  })

  describe('#render()', () => {
    it('renders it correctly', () => {
      const wrapper = shallow(<PolicyItem {...policyItemProps} />)

      expect(wrapper.find('.policy-number')).to.have.lengthOf(0)

      expect(wrapper.find('.vin')).to.have.lengthOf(1)
      expect(wrapper.find('.vin').text()).to.eq('ABCDE')

      expect(wrapper.find('.car')).to.have.lengthOf(1)

      const carValueCleaned = wrapper.find('.car').text().replace(/[\n\r]+/g, '').replace(/\s{2,20}/g, ' ')
      expect(carValueCleaned).to.eq('2017 Makebla Modelble Black')

      expect(wrapper.find('.name')).to.have.lengthOf(1)
      expect(wrapper.find('.name').text()).to.eq('John')

      expect(wrapper.find('.zip')).to.have.lengthOf(1)
      expect(wrapper.find('.zip').text()).to.eq('12345')
    })

    describe('when there is a poliyNumber in the properties', () => {
      const propsWithPolicyNumber = {
        ...policyItemProps, policyNumber: 'ABC123',
      }
      const wrapper = shallow(<PolicyItem {...propsWithPolicyNumber} />)

      expect(wrapper.find('.policy-number')).to.have.lengthOf(1)
      expect(wrapper.find('.policy-number').text()).to.eq('ABC123')
    })
  })
})
