import React from 'react'
import NewPolicySearch from 'components/JobForm/NewPolicySearch'
import { mount } from 'enzyme'
import Consts from 'consts'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('NewPolicySearch', () => {
  describe('#lockSavedFields', () => {
    describe('when pcc_coverage.data[0].site_code is present', () => {
      it('sets fleet_site_id to disabled', () => {
        const setToDisabledMock = jest.fn()

        const form = {
          record: {
            pcc_coverage: {
              data: [{ site_code: '12345T' }],
            },
          },
          fields: {
            fleet_site_id: {
              setToDisabled: setToDisabledMock,
            },
          },
        }

        const wrapper = new NewPolicySearch({})
        wrapper.lockSavedFields(form)

        expect(setToDisabledMock.mock.calls).toHaveLength(1)
      })
    })

    describe('when pcc_coverage.data[0].site_code is NOT present', () => {
      it('sets fleet_site_id to disabled', () => {
        const setToDisabledMock = jest.fn()

        const form = {
          record: {
            pcc_coverage: { data: [{}] },
          },
          fields: {
            fleet_site_id: {
              setToDisabled: setToDisabledMock,
            },
          },
        }

        const wrapper = new NewPolicySearch({})
        wrapper.lockSavedFields(form)

        expect(setToDisabledMock.mock.calls).toHaveLength(0)
      })
    })
  })

  describe('#disableSearchInputUnlessClientAPICovered', () => {
    it('sets the right fields as disabled', () => {
      const setToDisabledMock = jest.fn()

      const form = {
        fields: {
          customer_lookup_type: {
            setToDisabled: setToDisabledMock,
          },
          lookup_type_last_name: {
            setToDisabled: setToDisabledMock,
          },
          lookup_type_phone_number: {
            setToDisabled: setToDisabledMock,
          },
          lookup_type_policy_number: {
            setToDisabled: setToDisabledMock,
          },
          lookup_type_unit_number: {
            setToDisabled: setToDisabledMock,
          },
          lookup_type_vin: {
            setToDisabled: setToDisabledMock,
          },
          lookup_type_zip: {
            setToDisabled: setToDisabledMock,
          },
        },
      }

      const wrapper = new NewPolicySearch({ rerender: jest.fn() })
      wrapper.disableSearchInputUnlessClientAPICovered(form)

      expect(setToDisabledMock.mock.calls).toHaveLength(7)
    })

    describe('when coverage is "Client API Covered"', () => {
      it('doe not set the lookup types fields as disabled', () => {
        const setToDisabledMock = jest.fn()

        const form = {
          fields: {
            customer_lookup_type: {
              setToDisabled: setToDisabledMock,
            },
            lookup_type_last_name: {
              setToDisabled: setToDisabledMock,
            },
            lookup_type_phone_number: {
              setToDisabled: setToDisabledMock,
            },
            lookup_type_policy_number: {
              setToDisabled: setToDisabledMock,
            },
            lookup_type_unit_number: {
              setToDisabled: setToDisabledMock,
            },
            lookup_type_vin: {
              setToDisabled: setToDisabledMock,
            },
            lookup_type_zip: {
              setToDisabled: setToDisabledMock,
            },
          },
          record: {
            pcc_coverage: {
              data: [{ name: Consts.COVERAGE.CLIENT_API_COVERED }],
            },
          },
        }

        const wrapper = new NewPolicySearch({ rerender: jest.fn(), form })
        wrapper.disableSearchInputUnlessClientAPICovered(form)

        expect(setToDisabledMock.mock.calls).toHaveLength(0)
      })
    })
  })

  describe('#enableSearchInput', () => {
    it('sets the right fields as enabled', () => {
      const setToEnabledMock = jest.fn()

      const form = {
        fields: {
          customer_lookup_type: {
            setToEnabled: setToEnabledMock,
          },
          lookup_type_last_name: {
            setToEnabled: setToEnabledMock,
          },
          lookup_type_phone_number: {
            setToEnabled: setToEnabledMock,
          },
          lookup_type_policy_number: {
            setToEnabled: setToEnabledMock,
          },
          lookup_type_unit_number: {
            setToEnabled: setToEnabledMock,
          },
          lookup_type_vin: {
            setToEnabled: setToEnabledMock,
          },
          lookup_type_zip: {
            setToEnabled: setToEnabledMock,
          },
        },
      }

      const wrapper = new NewPolicySearch({ rerender: jest.fn(), form })
      wrapper.enableSearchInput()

      expect(setToEnabledMock.mock.calls).toHaveLength(7)
    })
  })

  describe('#buildSearchPayload', () => {
    const buildSearchPayloadHelper = (record) => {
      const form = { record }

      return new NewPolicySearch({ form }).buildSearchPayload()
    }

    describe('record.customer_lookup_type == 8 VIN', () => {
      it('returns the correct payload and search string for VIN', () => {
        const [searchPayload, searchString] = buildSearchPayloadHelper(
          {
            id: 1,
            lookup_type_vin: '1234VIN',
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN,
          }
        )

        expect(searchPayload.job_id).to.eq(1)
        expect(searchPayload.search_terms.vin).to.eq('1234VIN')
        expect(searchString).to.eq("Search for '1234VIN'")
      })
    })

    describe('record.customer_lookup_type == Name and Zip', () => {
      it('returns the correct payload and search string for Name and Zip', () => {
        const [searchPayload, searchString] = buildSearchPayloadHelper(
          {
            id: 1,
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP,
            lookup_type_last_name: 'Jackson',
            lookup_type_zip: '12345-5',
          }
        )

        expect(searchPayload.job_id).to.eq(1)
        expect(searchPayload.search_terms.last_name).to.eq('Jackson')
        expect(searchPayload.search_terms.postal_code).to.eq('12345-5')
        expect(searchString).to.eq("Search for 'Jackson 12345-5'")
      })
    })

    describe('record.customer_lookup_type == Unit Number', () => {
      it('returns the correct payload and search string for UNIT_NUMBER', () => {
        const [searchPayload, searchString] = buildSearchPayloadHelper(
          {
            id: 1,
            lookup_type_unit_number: 'UNIT7766',
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER,
          }
        )

        expect(searchPayload.job_id).to.eq(1)
        expect(searchPayload.search_terms.unit_number).to.eq('UNIT7766')
        expect(searchString).to.eq("Search for 'UNIT7766'")
      })
    })

    describe('record.customer_lookup_type == Policy Number', () => {
      it('returns the correct payload and search string for POLICY_NUMBER', () => {
        const [searchPayload, searchString] = buildSearchPayloadHelper(
          {
            id: 1,
            lookup_type_policy_number: 'POLICY_ABCD123',
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER,
          }
        )

        expect(searchPayload.job_id).to.eq(1)
        expect(searchPayload.search_terms.policy_number).to.eq('POLICY_ABCD123')
        expect(searchString).to.eq("Search for 'POLICY_ABCD123'")
      })
    })

    describe('record.customer_lookup_type == Phone Number', () => {
      it('returns the correct payload and search string for PHONE_NUMBER', () => {
        const [searchPayload, searchString] = buildSearchPayloadHelper(
          {
            id: 1,
            lookup_type_phone_number: '40544443333',
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER,
          }
        )

        expect(searchPayload.job_id).to.eq(1)
        expect(searchPayload.search_terms.phone_number).to.eq('40544443333')
        expect(searchString).to.eq("Search for '40544443333'")
      })
    })
  })

  describe('#fillFormWithSelectedPolicyItem(policyItem)', () => {
    it('sets the job form fields accordingly', () => {
      const rerenderMock = jest.fn()

      const makeSetValueMock = jest.fn()
      const makeSetToDisabledMock = jest.fn()

      const modelSetValueMock = jest.fn()
      const modelSetToDisabledMock = jest.fn()

      const yearSetValueMock = jest.fn()
      const yearSetToDisabledMock = jest.fn()

      const colorSetValueMock = jest.fn()
      const colorSetToDisabledMock = jest.fn()

      const licenseSetValueMock = jest.fn()
      const licenseSetToDisabledMock = jest.fn()

      const vinSetValueMock = jest.fn()
      const vinSetToDisabledMock = jest.fn()

      const unitNumberSetValueMock = jest.fn()
      const unitNumberSetToDisabledMock = jest.fn()

      const customerFullNameSetValueMock = jest.fn()
      const customerPhoneSetValueMock = jest.fn()

      const form = {
        form: {
          fields: {
            make: {
              setValue: makeSetValueMock,
              setToDisabled: makeSetToDisabledMock,
              animate: jest.fn(),
            },
            model: {
              setValue: modelSetValueMock,
              setToDisabled: modelSetToDisabledMock,
              animate: jest.fn(),
            },
            year: {
              setValue: yearSetValueMock,
              setToDisabled: yearSetToDisabledMock,
              animate: jest.fn(),
            },
            color: {
              setValue: colorSetValueMock,
              setToDisabled: colorSetToDisabledMock,
              animate: jest.fn(),
            },
            license: {
              setValue: licenseSetValueMock,
              setToDisabled: licenseSetToDisabledMock,
              animate: jest.fn(),
            },
            vin: {
              setValue: vinSetValueMock,
              setToDisabled: vinSetToDisabledMock,
              animate: jest.fn(),
            },
            unit_number: {
              setValue: unitNumberSetValueMock,
              setToDisabled: unitNumberSetToDisabledMock,
              animate: jest.fn(),
            },
            'customer.full_name': {
              setValue: customerFullNameSetValueMock,
              animate: jest.fn(),
            },
            'customer.phone': {
              setValue: customerPhoneSetValueMock,
              useMasking: jest.fn(),
              animate: jest.fn(),
            },
          },
        },
      }

      const policyItem = {
        policyVehicle: {
          make: 'MakeName',
          model: 'ModelName',
          year: 2018,
          color: 'black',
          license: 'ABC12345',
          vin: 'YYZ001',
          unit_number: 'UNIT001',
        },
        policyCustomer: {
          name: 'Caesar roma',
          phone_number: '+14053334444',
        },
      }

      const props = {
        ...form, rerender: rerenderMock,
      }

      const wrapper = new NewPolicySearch(props)
      wrapper.fillFormWithSelectedPolicyItem(policyItem)

      expect(makeSetValueMock.mock.calls).toHaveLength(1)

      // we want it to keep the N in uppercase, otherwise it will break makes like McLaren
      expect(makeSetValueMock.mock.calls[0][0]).to.eq('MakeName')
      expect(makeSetToDisabledMock.mock.calls).toHaveLength(1)

      expect(modelSetValueMock.mock.calls).toHaveLength(1)
      expect(modelSetValueMock.mock.calls[0][0]).to.eq('ModelName')
      expect(modelSetToDisabledMock.mock.calls).toHaveLength(1)

      expect(yearSetValueMock.mock.calls).toHaveLength(1)
      expect(yearSetValueMock.mock.calls[0][0]).to.eq(2018)
      expect(yearSetToDisabledMock.mock.calls).toHaveLength(1)

      expect(colorSetValueMock.mock.calls).toHaveLength(1)
      expect(colorSetValueMock.mock.calls[0][0]).to.eq('Black')
      expect(colorSetToDisabledMock.mock.calls).toHaveLength(1)

      expect(licenseSetValueMock.mock.calls).toHaveLength(1)
      expect(licenseSetValueMock.mock.calls[0][0]).to.eq('ABC12345')
      expect(licenseSetToDisabledMock.mock.calls).toHaveLength(1)

      expect(vinSetValueMock.mock.calls).toHaveLength(1)
      expect(vinSetValueMock.mock.calls[0][0]).to.eq('YYZ001')
      expect(vinSetToDisabledMock.mock.calls).toHaveLength(1)

      expect(unitNumberSetValueMock.mock.calls).toHaveLength(1)
      expect(unitNumberSetValueMock.mock.calls[0][0]).to.eq('UNIT001')
      expect(unitNumberSetToDisabledMock.mock.calls).toHaveLength(1)

      expect(customerFullNameSetValueMock.mock.calls).toHaveLength(1)
      expect(customerFullNameSetValueMock.mock.calls[0][0]).to.eq('Caesar Roma')

      expect(customerPhoneSetValueMock.mock.calls).toHaveLength(1)
      expect(customerPhoneSetValueMock.mock.calls[0][0]).to.eq('+14053334444')
    })
  })

  describe('#handleVehicleListItemSelect', () => {
    describe('it calls expected functions', () => {
      const selectedPolicyItem = {
        policyVehicle: { license_number: '12345ABC' },
      }

      const setSelectedPolicyItemMock = jest.fn()

      const wrapper = new NewPolicySearch({})
      wrapper.setSelectedPolicyItem = setSelectedPolicyItemMock
      wrapper.handleVehicleListItemSelect(selectedPolicyItem)

      expect(setSelectedPolicyItemMock.mock.calls).toHaveLength(1)
      expect(setSelectedPolicyItemMock).toHaveBeenCalledWith(
        expect.objectContaining({
          policyVehicle: expect.objectContaining({ license_number: '12345ABC', license: '12345ABC' }),
        }), true
      )
    })
  })

  describe('#isButtonEnabled', () => {
    describe('record.customer_lookup_type == Name and Zip', () => {
      describe('when Last Name is present', () => {
        describe('when Zip is present', () => {
          const record = {
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP,
            lookup_type_last_name: 'Jackson',
            lookup_type_zip: '1234-5',
          }

          const form = { record }

          const wrapper = new NewPolicySearch({ form })
          expect(wrapper.isButtonEnabled()).to.be.true
        })

        describe('when Zip is not present', () => {
          const record = {
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP,
            lookup_type_last_name: 'Jackson',
          }

          const form = { record }

          const wrapper = new NewPolicySearch({ form })
          expect(wrapper.isButtonEnabled()).to.be.false
        })
      })

      describe('when Last Name is not present', () => {
        const record = {
          customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.NAME_AND_ZIP,
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.false
      })
    })

    describe('record.customer_lookup_type == Phone Number', () => {
      describe('when Phone Number is present', () => {
        const record = {
          customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER,
          lookup_type_phone_number: '4054444333',
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.true
      })

      describe('when Phone Number is not present', () => {
        const record = {
          customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER,
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.false
      })
    })

    describe('record.customer_lookup_type == Policy Number', () => {
      describe('when Policy Number is present', () => {
        const record = {
          customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER,
          lookup_type_policy_number: 'POL123',
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.true
      })

      describe('when Policy Number is not present', () => {
        const record = {
          lookup_type_policy_number: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER,
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.false
      })
    })

    describe('record.customer_lookup_type == Unit Number', () => {
      describe('when Unit Number is present', () => {
        const record = {
          customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER,
          lookup_type_unit_number: 'UNIT123',
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.true
      })

      describe('when Unit Number is not present', () => {
        const record = {
          lookup_type_policy_number: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER,
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.false
      })
    })

    describe('record.customer_lookup_type == VIN', () => {
      describe('when VIN is present', () => {
        const record = {
          customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN,
          lookup_type_vin: 'VIN12345',
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.true
      })

      describe('when VIN is not present', () => {
        const record = {
          lookup_type_policy_number: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN,
        }

        const form = { record }

        const wrapper = new NewPolicySearch({ form })
        expect(wrapper.isButtonEnabled()).to.be.false
      })
    })
  })
})
