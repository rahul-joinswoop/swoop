import React from 'react'
import { shallow, mount } from 'enzyme'
import CoverageSectionContainer from 'components/JobForm/CoverageSectionContainer'
import AsyncRequestStore from 'stores/async_request_store'
import Consts from 'consts'
import 'loader' // delete this file when we have converted coffeescript to JS

jest.mock('stores/async_request_store')

describe('CoverageSectionContainer', () => {
  let wrapper

  const initialRecord = {
    acts_as_company_id: 108,
    customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN,
    fleet_site_id: 62,
    id: 1389,
    lastFieldChanged: 'lookup_type_vin',
    lookup_type_vin: '10484384',
  }

  const missingFieldsRecord = {
    acts_as_company_id: 108,
    customer: { full_name: 'Deloras Ferry' },
    customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN,
    fleet_site_id: 62,
    id: 1393,
    lastFieldChanged: 'customer.full_name',
    lookup_type_vin: '10484384',
    make: 'Lincoln',
    model: 'MKC',
    vin: '5LMTJ3DH710484384',
    year: 2019,
  }

  const manualMissingFieldsRecord = {
    acts_as_company_id: 108,
    customer: { full_name: 'Deloras Ferry' },
    customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.MANUAL,
    fleet_site_id: 62,
    id: 1393,
    lastFieldChanged: 'customer.full_name',
    lookup_type_vin: '10484384',
    make: 'Lincoln',
    model: 'MKC',
    vin: '5LMTJ3DH710484384',
    year: 2019,
  }

  const completedFieldsRecord = {
    acts_as_company_id: 108,
    customer: { full_name: 'Deloras Ferry' },
    customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN,
    fleet_site_id: 62,
    id: 1393,
    lastFieldChanged: 'customer.full_name',
    lookup_type_vin: '10484384',
    make: 'Lincoln',
    model: 'MKC',
    vin: '5LMTJ3DH710484384',
    year: 2019,
    service: 'Accident Tow',
    odometer: '123',
    drop_location: {
      address: '321 Van Ness Avenue, San Francisco, CA',
      lat: 37.7786365,
      lng: -122.4208067,
      location_type_id: 21,
      place_id: 'EiszMjEgVmFuIE5lc3MgQXZlbnVlLCBTYW4gRnJhbmNpc2NvLCBDQSwgVVNBIjESLwoUChIJE4z4DJmAhYARLCADAPd_zp4QwQIqFAoSCV_ChJTqgIWAESEUN2AudU0S',
      site_id: 65,
    },
    service_location: {
      address: '1498 Webster St, San Francisco, CA 94115, USA',
      lat: 37.784462075018496,
      lng: -122.4310271388672,
      place_id: 'Ei0xNDk4IFdlYnN0ZXIgU3QsIFNhbiBGcmFuY2lzY28sIENBIDk0MTE1LCBVU0EiGxIZChQKEgn96TE9uYCFgBGRllpQPMcDSBDaCw',
      site_id: null,
    },
  }

  const coverageDataError = {
    error: 'you hit an api error',
  }

  const coverageFailed = {
    failed_reasons: ['Missing: vehicle.odometer'],
    name: 'Warranty',
    result: 'Failed',
  }

  const coverageSuccess = {
    coverage_issues: ['Service not covered: Accident Tow'],
    name: 'Warranty',
    result: 'Covered',
  }

  const coverageSuccessWithNotes = {
    coverage_issues: ['Service not covered: Accident Tow'],
    name: 'Warranty',
    result: 'Covered',
    coverage_notes: 'You have good coverage. You should crash your car and find out how good',
  }

  const coveragePartial = {
    coverage_issues: ['Service not covered: Accident Tow'],
    limitations: ['vehicle.odometer < 70000'],
    customer_payment: ['You pay for stuff', 'You pay for more stuff sucker'],
    name: 'Warranty',
    result: 'Partial',
  }

  const coverageNotCovered = {
    coverage_issues: ['Service not covered: Accident Tow'],
    limitations: [],
    name: 'Warranty',
    result: 'NotCovered',
  }

  const coverageErrorState = {
    isLoading: false,
    coverageError: true,
    coverage: null,
  }

  const selectedPolicyItem = {
    policyId: '823b8fe3080e910a1b019677565550fe',
    policyCustomer: {
      business: false,
      city: 'PUNTA GORDA',
      country_code: 'US',
      name: 'DELORAS FERRY',
      phone_number: '+15559549406',
      postal_code: '33980',
      region: 'FL',
      street_address: '26155 KIHN UNION',
      id: 'a1808a6edb72214c684cda285fd9f2d8',
    },
    policyVehicle: {
      color: null,
      license_issuer: null,
      license_number: null,
      make: 'LINCOLN',
      model: 'MKC',
      purchase_date: '2018-12-01',
      id: '21d5c75810593bdf27b30f9fe1e67ae0',
      vin: '5LMTJ3DH710484384',
      year: 2019,
    },
  }

  const requiredFieldsForCoverage = ['customer_lookup_type', 'odometer', 'year', 'service', 'drop_location', 'service_location']

  const initialProps = {
    selectedPolicyItem: null,
    requiredFieldsForCoverage,
    form: {
      record: initialRecord,
    },
  }

  const setCoverageIncompleteError = jest.fn(() => false)
  const isCoverageDeterminationIncomplete = jest.fn(() => false)

  const selectedPolicyItemProps = {
    form: { renderInput: () => {}, record: completedFieldsRecord },
    requiredFieldsForCoverage,
    selectedPolicyItem,
    setCoverageIncompleteError,
    isCoverageDeterminationIncomplete,
  }

  const emptyRecordProps = {
    form: { renderInput: () => {}, record: {} },
    requiredFieldsForCoverage,
    selectedPolicyItem,
    setCoverageIncompleteError,
    isCoverageDeterminationIncomplete,
  }

  const noSelectedPolicyItemProps = {
    form: { renderInput: () => {}, record: completedFieldsRecord },
    requiredFieldsForCoverage,
    selectedPolicyItem: null,
    setCoverageIncompleteError,
    isCoverageDeterminationIncomplete,
  }

  const pccCoverage = {
    pcc_coverage: {
      pcc_policy_id: '123',
      pcc_vehicle_id: '1234',
      pcc_driver_id: '1235',
      data: [coverageSuccess],
    },
  }

  const noSelectedPolicyItemRecordCoverageProps = {
    form: { renderInput: () => {}, record: Object.assign({}, completedFieldsRecord, pccCoverage) },
    requiredFieldsForCoverage,
    selectedPolicyItem: null,
    setCoverageIncompleteError,
    isCoverageDeterminationIncomplete,
  }

  const missingFieldsNoSelectedPolicyItemProps = {
    form: { renderInput: () => {}, record: missingFieldsRecord },
    requiredFieldsForCoverage,
    selectedPolicyItem: null,
    setCoverageIncompleteError,
    isCoverageDeterminationIncomplete,
  }

  const selectedPolicyItemMissingFieldsProps = {
    form: { renderInput: () => {}, record: missingFieldsRecord },
    requiredFieldsForCoverage,
    selectedPolicyItem,
    setCoverageIncompleteError,
    isCoverageDeterminationIncomplete,
  }

  const manualLookupMissingFieldsProps = {
    form: { renderInput: () => {}, record: manualMissingFieldsRecord },
    requiredFieldsForCoverage,
    selectedPolicyItem: null,
    setCoverageIncompleteError,
    isCoverageDeterminationIncomplete,
  }

  const loadingState = {
    coverage: null,
    isLoading: true,
    coverageError: false,
  }

  it('renders with shell props', () => {
    wrapper = shallow(<CoverageSectionContainer {...initialProps} />)
    expect(wrapper).toBeDefined()
  })

  describe('coverageResultValueToEnglish function', () => {
    it('returns Fully Covered if coverage result is Covered', () => {
      wrapper = mount(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.setState({ coverage: coverageSuccess })
      expect(wrapper.find('.coverage-result .coverage .message').text()).toEqual('Fully Covered')
    })
    it('returns Not Covered if coverage result is not covered', () => {
      wrapper = mount(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.setState({ coverage: coverageNotCovered })
      expect(wrapper.find('.coverage-result .coverage .message').text()).toEqual('Not Covered')
    })
    it('returns coverage.result for other coverage results', () => {
      wrapper = mount(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.setState({ coverage: coveragePartial })
      expect(wrapper.find('.coverage-result .coverage .message').text()).toEqual(coveragePartial.result)
    })
  })

  describe('CoverageResult component', () => {
    it('displays what customer pays for under Customer Payment subsection ', () => {
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.setState({ coverage: coveragePartial })
      wrapper.find('.coverage-result .customer-payment ul li').forEach((node, index) => {
        expect(node.text()).toEqual(coveragePartial.limitations[index])
      })
    })
    it('displays coverage notes if they are provided', () => {
      wrapper = mount(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.setState({ coverage: coverageSuccessWithNotes })
      expect(wrapper.find('.coverage-result .notes-section .notes').text()).toEqual(coverageSuccessWithNotes.coverage_notes)
    })
  })

  describe('getContainerClass', () => {
    it('called with a covered result, returns "coverage-section-container covered"', () => {
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      expect(wrapper.instance().getContainerClass(coverageSuccess, false)).toEqual('coverage-section-container covered')
    })
    it('called with a coverage failed, returns "coverage-section-container failed"', () => {
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      expect(wrapper.instance().getContainerClass(coverageFailed, false)).toEqual('coverage-section-container failed')
    })
    it('called with a coverage failed and this.props.coverageIncompleteError, returns "coverage-section-container failed"', () => {
      wrapper = shallow(<CoverageSectionContainer {...Object.assign({}, selectedPolicyItemProps, { coverageIncompleteError: true })} />)
      expect(wrapper.instance().getContainerClass(coverageFailed, false)).toEqual('coverage-section-container incomplete-error')
    })
    it('called with a coverage failed and loading, returns "coverage-section-container "', () => {
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      expect(wrapper.instance().getContainerClass(coverageFailed, true)).toEqual('coverage-section-container ')
    })
  })

  describe('renderBody', () => {
    it('called with a covered result, returns CoverageResult component', () => {
      wrapper = mount(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.setState({ coverage: coverageSuccess })
      expect(wrapper.find('.coverage-result').exists()).toEqual(true)
    })
    it('called with a coverage failed, and no missingFields, and no selectedPolicyItem, returns LookupCoverageMessage', () => {
      wrapper = mount(<CoverageSectionContainer {...noSelectedPolicyItemProps} />)
      wrapper.setState({ coverage: coverageFailed })
      expect(wrapper.find('.default-message').exists()).toEqual(true)
    })
    it('called with a coverage failed, and missingFields, and a selectedPolicyItem, returns a MissingFieldList with program name Warranty', () => {
      wrapper = mount(<CoverageSectionContainer {...selectedPolicyItemMissingFieldsProps} />)
      wrapper.setState({ coverage: coverageFailed })
      expect(wrapper.find('.missing-field-section').exists()).toEqual(true)
      expect(wrapper.find('.missing-field-section').html().includes('Warranty')).toEqual(true)
    })
    it('called with a coverage failed, and missingFields, and no selectedPolicyItem, returns a MissingFieldList with program name Manual Entry', () => {
      wrapper = mount(<CoverageSectionContainer {...manualLookupMissingFieldsProps} />)
      wrapper.setState({ coverage: coverageFailed })
      expect(wrapper.find('.missing-field-section').exists()).toEqual(true)
      expect(wrapper.find('.missing-field-section').html().includes('Manual Entry')).toEqual(true)
    })
    it('called with a coverage failed, and missingFields, and no selectedPolicyItem, and vin lookup, returns a LookupCoverageMessage', () => {
      wrapper = mount(<CoverageSectionContainer {...missingFieldsNoSelectedPolicyItemProps} />)
      wrapper.setState({ coverage: coverageFailed })
      expect(wrapper.find('.default-message').exists()).toEqual(true)
    })
    it('called with a coverage failed, and no missingFields, and a selectedPolicyItem, returns a loader', () => {
      wrapper = mount(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.setState({ coverage: coverageFailed })
      expect(wrapper.find('.loadingContainer').exists()).toEqual(true)
    })
  })

  describe('getMissingFields', () => {
    it('called with no missingFields, returns empty array', () => {
      wrapper = mount(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      expect(wrapper.instance().getMissingFields(requiredFieldsForCoverage, completedFieldsRecord)).toEqual([])
    })
    it('called with missingFields, returns missingFields', () => {
      wrapper = mount(<CoverageSectionContainer {...missingFieldsNoSelectedPolicyItemProps} />)
      expect(wrapper.instance().getMissingFields(requiredFieldsForCoverage, missingFieldsRecord)).toEqual(['odometer', 'service', 'drop_location', 'service_location'])
    })
    it('called with missingFields and a record that has coverage, adds missingFields to record and retrns them', () => {
      wrapper = mount(<CoverageSectionContainer {...missingFieldsNoSelectedPolicyItemProps} />)
      const missingFieldsRecordWithCoverage = Object.assign({}, missingFieldsRecord, { coverage: {} })
      expect(wrapper.instance().getMissingFields(requiredFieldsForCoverage, missingFieldsRecordWithCoverage)).toEqual(['odometer', 'service', 'drop_location', 'service_location'])
    })
  })

  describe('requestCoverage', () => {
    AsyncRequestStore.lookupCoverage = jest.fn()
    const fakePreviousPayload = { fake: 'previous payload' }
    const fakeNewPayload = { fake: 'new payload' }
    beforeEach(() => {
      jest.resetAllMocks()
    })
    it('called with retryCoverageCall argument as true when previousPayload exists, calls AsyncRequestStore.lookupCoverage with previousPayload (i.e. retries the call)', () => {
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.instance().previousPayload = fakePreviousPayload
      wrapper.instance().requestCoverage(true)
      expect(AsyncRequestStore.lookupCoverage).toHaveBeenCalledWith(
        fakePreviousPayload,
        expect.objectContaining({
          error: expect.any(Function),
          success: expect.any(Function),
        }),
      )
    })
    it('called without retryCoverageCall argument when previousPayload exists, calls AsyncRequestStore.lookupCoverage with newly created payload, and expect setState for loading to be called', () => {
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      const fakeHandleNoAPIResponse = jest.fn()
      wrapper.instance().previousPayload = fakePreviousPayload
      wrapper.instance().createCoveragePayload = jest.fn(() => fakeNewPayload)
      wrapper.instance().handleNoAPIResponse = fakeHandleNoAPIResponse
      wrapper.instance().requestCoverage()
      expect(AsyncRequestStore.lookupCoverage).toHaveBeenCalledWith(
        fakeNewPayload,
        expect.objectContaining({
          error: expect.any(Function),
          success: expect.any(Function),
        }),
      )
      expect(fakeHandleNoAPIResponse).toHaveBeenCalled()
      expect(wrapper.instance().state).toEqual(loadingState)
    })
    it('if payload is empty, does not call AsyncRequestStore.lookupCoverage', () => {
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.instance().createCoveragePayload = jest.fn(() => {})
      wrapper.instance().requestCoverage()
      expect(AsyncRequestStore.lookupCoverage).toHaveBeenCalledTimes(0)
    })
  })

  describe('AsyncRequestStore.lookupCoverage in requestCoverage', () => {
    it('if AsyncRequestStore.lookupCoverage call is successful, it updates state with coverage data', () => {
      AsyncRequestStore.lookupCoverage = jest.fn((payload, cb) => {
        cb.success(coverageSuccess)
      })
      const onCoverageResponse = jest.fn()
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} onCoverageResponse={onCoverageResponse} />)
      wrapper.instance().requestCoverage()
      expect(wrapper.instance().state).toEqual({
        coverage: coverageSuccess,
        isLoading: false,
        coverageError: false,
      })
      expect(onCoverageResponse).toHaveBeenCalledWith(coverageSuccess)
    })
    it('if AsyncRequestStore.lookupCoverage call is results in a response error, it updates state to error state and calls rollbar error', () => {
      AsyncRequestStore.lookupCoverage = jest.fn((payload, cb) => {
        cb.success(coverageDataError)
      })
      global.Rollbar = {
        error: jest.fn(),
      }
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.instance().requestCoverage()
      expect(wrapper.instance().state).toEqual(coverageErrorState)
      expect(global.Rollbar.error).toHaveBeenCalled()
    })
    it('if AsyncRequestStore.lookupCoverage call is api error, it updates state to error state and calls rollbar error', () => {
      AsyncRequestStore.lookupCoverage = jest.fn((payload, cb) => {
        cb.error('api error')
      })
      global.Rollbar = {
        error: jest.fn(),
      }
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.instance().requestCoverage()
      expect(wrapper.instance().state).toEqual(coverageErrorState)
      expect(global.Rollbar.error).toHaveBeenCalled()
    })
  })

  describe('handleNoAPIResponse', () => {
    it('call setTimeout', () => {
      jest.useFakeTimers()
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.instance().handleNoAPIResponse()
      expect(setTimeout).toHaveBeenCalledTimes(1)
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000 * 30)
    })
    it('sets state to coverage error, if coverage loading for 30 seconds and no coverage result exists at end of 30 seconds', () => {
      global.setTimeout = jest.fn(cb => cb())
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      wrapper.setState({ isLoading: true, coverage: null })
      wrapper.instance().handleNoAPIResponse()
      expect(wrapper.instance().state).toEqual(coverageErrorState)
    })
  })

  describe('createCoveragePayload', () => {
    const completePayload = {
      job: {
        id: completedFieldsRecord.id,
        location: {
          dropoff_location: {
            lat: completedFieldsRecord.drop_location.lat,
            lng: completedFieldsRecord.drop_location.lng,
            google_place_id: completedFieldsRecord.drop_location.place_id,
            site_id: completedFieldsRecord.drop_location.site_id,
          },
          service_location: {
            lat: completedFieldsRecord.service_location.lat,
            lng: completedFieldsRecord.service_location.lng,
            google_place_id: completedFieldsRecord.service_location.place_id,
          },
        },
        service: { name: completedFieldsRecord.service },
        vehicle: {
          odometer: parseInt(completedFieldsRecord.odometer, 10),
          year: completedFieldsRecord.year,
        },
      },
    }

    const selectedCoveragePCC = {
      pcc_policy_id: selectedPolicyItem.policyId,
      pcc_driver_id: selectedPolicyItem.policyCustomer.id,
      pcc_vehicle_id: selectedPolicyItem.policyVehicle.id,
    }

    const recordCoveragePCC = {
      pcc_policy_id: pccCoverage.pcc_coverage.pcc_policy_id,
      pcc_driver_id: pccCoverage.pcc_coverage.pcc_driver_id,
      pcc_vehicle_id: pccCoverage.pcc_coverage.pcc_vehicle_id,
    }

    it('if no id in record, returns null', () => {
      wrapper = shallow(<CoverageSectionContainer {...emptyRecordProps} />)
      expect(wrapper.instance().createCoveragePayload()).toEqual(null)
    })
    it('if complete record, returns complete payload', () => {
      wrapper = shallow(<CoverageSectionContainer {...selectedPolicyItemProps} />)
      expect(wrapper.instance().createCoveragePayload()).toEqual(Object.assign({}, completePayload, selectedCoveragePCC))
    })
    it('if complete record with pcc_coverage and no selectedPolicyItem, returns complete payload with record pcc_coverage', () => {
      wrapper = shallow(<CoverageSectionContainer {...noSelectedPolicyItemRecordCoverageProps} />)
      expect(wrapper.instance().createCoveragePayload()).toEqual(Object.assign({}, completePayload, recordCoveragePCC))
    })
  })
})
