import UserStore from 'stores/user_store'
import SiteStore from 'stores/site_store'
import FeatureStore from 'stores/feature_store'
import JobForm from 'components/job_form'
import Consts from 'consts'
import 'loader' // delete this file when we have converted coffeescript to JS


describe('JobForm', () => {
  describe('#onCoverageResponse', () => {
    const resetUser = (features) => {
      const userCompanyfeatures = features.map(feature => feature.id)

      // mock user with a company with features
      UserStore.setUser({
        id: 1,
        full_name: 'Test User',
        roles: ['rescue'],
        company: {
          features: userCompanyfeatures,
        },
      })
    }

    const resetSite = (siteCode) => {
      const site = {
        id: 1,
        type: 'FleetSite',
        site_code: siteCode,
      }

      SiteStore._add(site)

      return site
    }

    const resetForm = () => {
      const rerenderMock = jest.fn()
      const animateMock = jest.fn()
      const setToDisabledMock = jest.fn()
      const wrapper = new JobForm({})
      wrapper.rerender = rerenderMock
      wrapper.form = {
        record: {
          fleet_site_id: null,
        },
        fields: {
          fleet_site_id: {
            animate: animateMock,
            setToDisabled: setToDisabledMock,
          },
        },
      }

      return [wrapper, rerenderMock, animateMock, setToDisabledMock]
    }

    const featureFleetSites = { id: 1, name: Consts.FEATURES_FLEET_SITES }
    const featureAutoSelectSite = { id: 2, name: Consts.FEATURES_AUTO_SELECT_SITE_FROM_LOOKUP }
    const featureShowClientSitesToSwoop = { id: 3, name: Consts.FEATURES_SHOW_CLIENT_SITES_TO_SWOOP }

    // mock existent features
    FeatureStore._addRowToInternalCollection(featureFleetSites)
    FeatureStore._addRowToInternalCollection(featureAutoSelectSite)
    FeatureStore._addRowToInternalCollection(featureShowClientSitesToSwoop)

    describe('and Auto Select Site from lookup is enabled', () => {
      describe('when FleetSites Feature is enabled', () => {
        describe('and there is a match by site.site_code', () => {
          it('auto selects the site on the job form', () => {
            resetUser([featureFleetSites, featureAutoSelectSite])
            const site = resetSite('12345T')

            const [wrapper, rerenderMock, animateMock, setToDisabledMock] = resetForm()

            expect(wrapper.form.record.fleet_site_id).to.eq(null)

            wrapper.onCoverageResponse({ site_code: site.site_code })

            expect(wrapper.form.record.fleet_site_id).to.eq(site.id)
            expect(rerenderMock.mock.calls).toHaveLength(1)
            expect(animateMock.mock.calls).toHaveLength(1)
            expect(setToDisabledMock.mock.calls).toHaveLength(1)
          })
        })

        describe('and there is NO match by site.site_code', () => {
          it('does not auto select the site on the job form', () => {
            resetUser([featureFleetSites, featureAutoSelectSite])
            resetSite('12345T')

            const [wrapper, rerenderMock, animateMock, setToDisabledMock] = resetForm()

            expect(wrapper.form.record.fleet_site_id).to.eq(null)

            wrapper.onCoverageResponse({ site_code: 'NO_MATCH_CODE' })

            expect(wrapper.form.record.fleet_site_id).to.eq(null)
            expect(rerenderMock.mock.calls).toHaveLength(0)
            expect(animateMock.mock.calls).toHaveLength(0)
            expect(setToDisabledMock.mock.calls).toHaveLength(0)
          })
        })
      })

      describe('when Show Client Sites To Swoop Feature is enabled', () => {
        describe('and there is a match by site.site_code', () => {
          it('auto selects the site on the job form', () => {
            resetUser([featureShowClientSitesToSwoop, featureAutoSelectSite])
            const site = resetSite('12345T')

            const [wrapper, rerenderMock, animateMock, setToDisabledMock] = resetForm()

            expect(wrapper.form.record.fleet_site_id).to.eq(null)

            wrapper.onCoverageResponse({ site_code: site.site_code })

            expect(wrapper.form.record.fleet_site_id).to.eq(site.id)
            expect(rerenderMock.mock.calls).toHaveLength(1)
            expect(animateMock.mock.calls).toHaveLength(1)
            expect(setToDisabledMock.mock.calls).toHaveLength(1)
          })
        })

        describe('and there is NO match by site.site_code', () => {
          it('does not auto select the site on the job form', () => {
            resetUser([featureShowClientSitesToSwoop, featureAutoSelectSite])
            resetSite('12345T')

            const [wrapper, rerenderMock, animateMock, setToDisabledMock] = resetForm()

            expect(wrapper.form.record.fleet_site_id).to.eq(null)

            wrapper.onCoverageResponse({ site_code: 'NO_MATCH_CODE' })

            expect(wrapper.form.record.fleet_site_id).to.eq(null)
            expect(rerenderMock.mock.calls).toHaveLength(0)
            expect(animateMock.mock.calls).toHaveLength(0)
            expect(setToDisabledMock.mock.calls).toHaveLength(0)
          })
        })
      })
    })

    describe('and Auto Select Site from lookup is disabled', () => {
      it('does not auto select the site on the job form', () => {
        resetUser([featureFleetSites, featureShowClientSitesToSwoop])
        const site = resetSite('12345T')

        const [wrapper, rerenderMock, animateMock, setToDisabledMock] = resetForm()

        expect(wrapper.form.record.fleet_site_id).to.eq(null)

        wrapper.onCoverageResponse({ site_code: site.site_code })

        expect(wrapper.form.record.fleet_site_id).to.eq(null)
        expect(rerenderMock.mock.calls).toHaveLength(0)
        expect(animateMock.mock.calls).toHaveLength(0)
        expect(setToDisabledMock.mock.calls).toHaveLength(0)
      })
    })
  })
})
