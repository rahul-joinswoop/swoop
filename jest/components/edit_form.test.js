import React from 'react'
import { shallow } from 'enzyme'
import EditForm from 'components/edit_form'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('edit_form component', () => {
  describe('realtime pattern validation', () => {
    test('prev value is taken from original value', () => {
      const e = {
        target: {
          value: '',
          pattern: 'blabla',
          dataset: {
            // prevVal: '',
            originalValue: 'originalValue',
          },
        },
      }

      const editForm = shallow(<EditForm />).instance()

      editForm.realTimePatternValidation(e)

      expect(e.target.dataset.prevVal).toBe(e.target.dataset.originalValue)
      expect(e.target.value).toBe(e.target.dataset.prevVal)
    })

    test('if new value is invalid set prev value', () => {
      const newValue = 'newValue'

      const e = {
        target: {
          value: newValue,
          pattern: 'blabla',
          dataset: {
            prevVal: 'prevVal',
            originalValue: 'originalValue',
          },
        },
      }

      const editForm = shallow(<EditForm />).instance()

      editForm.realTimePatternValidation(e)

      expect(e.target.value).toBe(e.target.dataset.prevVal)
      expect(e.target.value).not.toBe(newValue)
    })

    test('update value if new value is valid', () => {
      const newValue = '123.45'


      const prevValue = '34.01'

      const e = {
        target: {
          value: newValue,
          pattern: '^\\d{0,6}(\\.|(\\.\\d{1,2})?)$',
          dataset: {
            prevVal: prevValue,
            originalValue: '0.00',
          },
        },
      }

      const editForm = shallow(<EditForm />).instance()

      editForm.realTimePatternValidation(e)

      expect(e.target.value).toBe(newValue)
      expect(e.target.dataset.prevVal).toBe(newValue)
      expect(e.target.dataset.prevVal).not.toBe(prevValue)
    })
  })
})
