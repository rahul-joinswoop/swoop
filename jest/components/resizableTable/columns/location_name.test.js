import LocationName from 'components/resizableTable/columns/location_name'
import LocationTypeStore from 'stores/location_type_store'

jest.mock('stores/location_type_store')

describe('Location name', () => {
  it('if no object passed to LocationName, return empty string', () => {
    expect(LocationName(null)).toEqual('')
  })
  it('if propObject with no location passed in, LocationName returns empty string', () => {
    const propObject = {}
    expect(LocationName(propObject)).toEqual('')
  })
  it('if propObject with location but no location_type_id passed in, LocationName returns empty string', () => {
    const propObject = {}
    propObject.location = { location_type_id: null }
    expect(LocationName(propObject)).toEqual('')
  })
  it('if propObject with location and location_type_id passed in, but LocationTypeStore.getLocationTypeById returns null, LocationName returns empty string', () => {
    const propObject = {}
    propObject.location = { location_type_id: 1 }
    expect(LocationName(propObject)).toEqual('')
  })
  it('if propObject with location and location_type_id passed in, and LocationTypeStore.getLocationTypeById is provided, LocationName returns string', () => {
    const propObject = {}
    propObject.location = { location_type_id: 1 }
    LocationTypeStore.getLocationTypeById.mockReturnValue({ name: 'HQ' })
    expect(LocationName(propObject)).toEqual('HQ')
  })
})
