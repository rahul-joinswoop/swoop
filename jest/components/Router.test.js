import React from 'react'
import { shallow, mount } from 'enzyme'
import { UnwrappedRouter as Router } from 'components/Router'
import { SplitIOProvider } from 'components/split.io'
import RealRouter from 'components/Router'
import Context from 'Context'
import Storage from 'stores/storage'
import UserStore from 'stores/user_store'
import EnterpriseBorder from 'components/EnterpriseBorder'
import Settings from 'components/settings'
import JobIndex from 'components/job/jobs_index'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('<Router /> root component', () => {
  function login() {
    // mock bearer token
    Storage.set('bearer', 'some value')

    // mock user
    UserStore.setUser({
      id: 1,
      full_name: 'Test User',
      roles: ['rescue'],
      company_id: 1,
    })
  }

  afterEach(() => {
    Storage.remove('bearer')
    UserStore.setUser(null)
    // UserStore.logout()

    // reset jsdom url
    window.history.pushState({}, '', '/')
  })

  it('renders login by default', () => {
    const wrapper = shallow(<Router />)
    expect(wrapper.exists('Login')).toBeTruthy()
  })

  it('navigates to default user url', () => {
    login()
    const wrapper = mount(<SplitIOProvider><RealRouter /></SplitIOProvider>)

    expect(wrapper.exists(EnterpriseBorder)).toBeTruthy()
    expect(wrapper.exists(JobIndex)).toBeTruthy()
  })

  it('navigates to user/settings', () => {
    login()
    const wrapper = mount(<SplitIOProvider><RealRouter /></SplitIOProvider>)

    Context.navigate('user/settings', { configuretab: null })
    wrapper.update()

    expect(wrapper.exists(Settings)).toBe(true)
  })

  it('redirect to login when not logged in', () => {
    const wrapper = shallow(<Router />)

    Context.navigate('partner/dispatch')
    wrapper.update()

    expect(wrapper.exists('Login')).toBeTruthy()
  })

  it('redirects from old hash route', () => {
    login()

    window.history.pushState({}, '', '/#user/settings')

    jest.useFakeTimers()

    const wrapper = mount(<SplitIOProvider><RealRouter /></SplitIOProvider>)

    expect(wrapper.exists('Loading')).toBeTruthy()

    jest.runAllTimers()
    wrapper.update()

    expect(wrapper.exists(Settings)).toBe(true)
  })
})
