import Tracking from 'tracking'
import UserStore from 'stores/user_store'
import JobStore from 'stores/job_store'
import ServiceStore from 'stores/service_store'
import { clone } from 'lodash'

jest.mock('stores/user_store')
jest.mock('stores/job_store')
jest.mock('stores/service_store')

describe('Tracking', () => {
  global.analytics = jest.fn()
  const { analytics } = global
  analytics.track = jest.fn()
  analytics.identify = jest.fn()
  analytics.page = jest.fn()
  const testName = 'testName'
  const testUser = {
    id: 1,
    full_name: 'John Doe',
    company: {
      name: 'ACME Corp',
      type: 'Anvil manufacturer',
    },
  }
  const testOptions = {
    a: 1,
    b: 2,
  }
  const defaultSentTrackOptions = {
    companyType: undefined,
    platform: 'Web',
    user_type: 'Dispatcher',
    version: undefined,
  }

  describe('_sendEvent', () => {
    it('to send tracking event with passed in name, options, and extra_options', () => {
      Tracking._sendEvent(testName, {}, {})
      expect(analytics.track).toHaveBeenCalledWith(testName, {}, {})
      Tracking._sendEvent(testName, testOptions, testOptions)
      expect(analytics.track).toHaveBeenCalledWith(testName, testOptions, testOptions)
    })
    it('if options has referrer even if Tracking has referrer, then send options referrer', () => {
      const pageOptions = {
        referrer: 'facebook',
      }
      Tracking.referrer = 'instagram'
      Tracking._sendEvent(testName, pageOptions, {})
      expect(analytics.track).toHaveBeenCalledWith(testName, pageOptions, {})
    })
    it('if Tracking has referrer and options has no referrer, then send Tracking referrer', () => {
      const pageOptions = {}
      Tracking.referrer = 'instagram'
      Tracking._sendEvent(testName, pageOptions, {})
      const newPageOptions = {
        referrer: 'instagram',
      }
      expect(analytics.track).toHaveBeenCalledWith(testName, newPageOptions, {})
      Tracking.referrer = null
    })
  })

  describe('identify', () => {
    it('to send identify event with passed in name, options, and extra_options', () => {
      Tracking.identify(testUser, testOptions)
      const identifyOptions = {
        userId: testUser.id?.toString(),
        name: testUser.full_name,
        company: testUser.company?.name,
        companyType: testUser.company?.type,
      }
      expect(analytics.identify).toHaveBeenCalledWith(testUser.id, identifyOptions, testOptions)
    })
  })

  describe('identifyForSalesforce', () => {
    const data = {
      user_id: 1,
      name: 'ACME Corp',
      type: 'Anvil maker',
      phone: '555-5555',
    }
    const newrecord = {
      user: {
        first_name: 'John',
        last_name: 'Doe',
        email: 'johnD1@gmail.com',
      },
    }
    const integrations = {
      integrations: {
        Salesforce: true,
      },
    }
    it('to send identify event with passed in arguments', () => {
      Tracking.identifyForSalesforce(data, newrecord, 'referrer')
      const identifyOptions = {
        userId: data.user_id?.toString(),
        name: `${newrecord.user.first_name} ${newrecord.user.last_name}`,
        company: data.name,
        companyType: data.type,
        email: newrecord.user.email,
        phone: data.phone,
        utmSource: '',
        utmCampaign: '',
        utmMedium: '',
        utmTerm: '',
        utmContent: '',
      }
      expect(analytics.identify).toHaveBeenCalledWith(data.user_id, identifyOptions, integrations)
    })
  })

  describe('page', () => {
    it('to send page event with passed in name and options', () => {
      Tracking.page(testName, testOptions)
      expect(analytics.page).toHaveBeenCalledWith(testName, testOptions)
    })
    it('if options has referrer even if Tracking has referrer, then send options referrer', () => {
      const pageOptions = {
        referrer: 'facebook',
      }
      Tracking.referrer = 'instagram'
      Tracking.page(testName, pageOptions)
      expect(analytics.page).toHaveBeenCalledWith(testName, pageOptions)
    })
    it('if Tracking has referrer and options has no referrer, then send Tracking referrer', () => {
      const pageOptions = {}
      Tracking.referrer = 'instagram'
      Tracking.page(testName, pageOptions)
      const newPageOptions = {
        referrer: 'instagram',
      }
      Tracking.referrer = null
      expect(analytics.page).toHaveBeenCalledWith(testName, newPageOptions)
    })
    it('if user from user store has an id, and options have no userId, to send options with user.id', () => {
      const testUserFromStore = {
        id: 1,
      }
      const pageOptions = {}
      pageOptions.userId = testUserFromStore.id.toString()
      Tracking.page(testName, pageOptions)
      expect(analytics.page).toHaveBeenCalledWith(testName, pageOptions)
    })
    it('if user from user store has an id, and options has a userId, to send options original userId', () => {
      const pageOptions = {
        userId: 2,
      }
      Tracking.page(testName, pageOptions)
      expect(analytics.page).toHaveBeenCalledWith(testName, pageOptions)
    })
  })

  describe('track', () => {
    it('to send track event with default options', () => {
      Tracking.track(testName, {}, {})
      expect(analytics.track).toHaveBeenCalledWith(testName, defaultSentTrackOptions, {})
    })

    it('if user is an admin, to send track event options with user_type Admin', () => {
      UserStore.isAdmin.mockReturnValue(true)
      const newOptions = clone(defaultSentTrackOptions)
      newOptions.user_type = 'Admin'
      Tracking.track(testName, {}, {})
      expect(analytics.track).toHaveBeenCalledWith(testName, newOptions, {})
      UserStore.isAdmin.mockReturnValue(false)
    })

    it('if user is isOnlyDriver, to send track event options with user_type Driver', () => {
      UserStore.isOnlyDriver.mockReturnValue(true)
      const newOptions = clone(defaultSentTrackOptions)
      newOptions.user_type = 'Driver'
      Tracking.track(testName, {}, {})
      expect(analytics.track).toHaveBeenCalledWith(testName, newOptions, {})
      UserStore.isOnlyDriver.mockReturnValue(false)
    })
  })

  describe('trackDispatchEvent', () => {
    it('to send track event with default options, and category Dispatch', () => {
      const newOptions = defaultSentTrackOptions
      newOptions.category = 'Dispatch'
      Tracking.trackDispatchEvent(testName, defaultSentTrackOptions, {})
      expect(analytics.track).toHaveBeenCalledWith(testName, newOptions, {})
    })
  })

  describe('trackRSCEvent', () => {
    it('to send track event with default options, and category RSC', () => {
      const newOptions = defaultSentTrackOptions
      newOptions.category = 'RSC'
      Tracking.trackRSCEvent(testName, defaultSentTrackOptions, {})
      expect(analytics.track).toHaveBeenCalledWith(testName, newOptions, {})
    })
  })

  describe('trackJobEvent', () => {
    const testJobId = 75
    const testAccount = 'testAccount'
    const testService = 'testService'
    const defaultSentOptions = clone(defaultSentTrackOptions)
    defaultSentOptions.job_id = testJobId

    it('to send tracking event with passed in name and job id', () => {
      Tracking.trackJobEvent(testName, testJobId)
      expect(analytics.track).toHaveBeenCalledWith(testName, defaultSentOptions, {})
    })

    it('to send explicitly passed job id instead of options job id', () => {
      Tracking.trackJobEvent(testName, testJobId, { job_id: 1 })
      expect(analytics.track).toHaveBeenCalledWith(testName, defaultSentOptions, {})
    })

    it('when logged in as partner, to send account from job store (when available) and to add source as ISSC (when account is not Agero)', () => {
      UserStore.isPartner.mockReturnValue(true)
      JobStore.getAccountNameByJobId.mockReturnValue(testAccount)
      const modifiedSentOptions = clone(defaultSentOptions)
      modifiedSentOptions.account = testAccount
      modifiedSentOptions.source = 'ISSC'
      Tracking.trackJobEvent(testName, testJobId, { job_id: 1 })
      expect(analytics.track).toHaveBeenCalledWith(testName, modifiedSentOptions, {})
    })

    it('when not logged in as partner, to not send account or source in options', () => {
      UserStore.isPartner.mockReturnValue(false)
      Tracking.trackJobEvent(testName, testJobId, { job_id: 1 })
      expect(analytics.track).toHaveBeenCalledWith(testName, defaultSentOptions, {})
    })

    it('when not logged in as partner, to not send account but to send source in options', () => {
      UserStore.isPartner.mockReturnValue(true)
      JobStore.getAccountNameByJobId.mockReturnValue(null)
      const modifiedSentOptions = clone(defaultSentOptions)
      modifiedSentOptions.source = 'ISSC'
      Tracking.trackJobEvent(testName, testJobId, { job_id: 1 })
      expect(analytics.track).toHaveBeenCalledWith(testName, defaultSentOptions, {})
    })

    it('if service store return a service name, send serve as part of options', () => {
      UserStore.isPartner.mockReturnValue(false)
      ServiceStore.get.mockReturnValue(testService)
      const modifiedSentOptions = clone(defaultSentOptions)
      modifiedSentOptions.service = testService
      Tracking.trackJobEvent(testName, testJobId, { job_id: 1 })
      expect(analytics.track).toHaveBeenCalledWith(testName, modifiedSentOptions, {})
    })
  })
})
