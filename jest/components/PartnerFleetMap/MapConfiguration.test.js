import React from 'react'
import { shallow } from 'enzyme'
import MapConfiguration from 'components/PartnerFleetMap/MapConfiguration'
import UserSettingStore from 'stores/user_setting_store'
import Consts from 'consts'
import Api from 'api'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('MapConfiguration Component', () => {
  describe('menu title', () => {
    it('is hidden by default', () => {
      const wrapper = shallow(<MapConfiguration />)
      expect(wrapper.exists('.map-configuration-content')).toBe(false)
    })

    it('shows menu by clicking', () => {
      const wrapper = shallow(<MapConfiguration />)

      wrapper.find('.map-configuration-title').simulate('click')

      expect(wrapper.exists('.map-configuration-content')).toBe(true)
    })
  })

  describe('menu', () => {
    const settings = [
      Consts.USER_SETTING.SHOW_OFF_DUTY_PARTNER_FLEET_MAP,
      Consts.USER_SETTING.SHOW_WITHOUT_DRIVERS_PARTNER_FLEET_MAP,
      Consts.USER_SETTING.SHOW_ROUTES_PARTNER_FLEET_MAP,
    ]

    afterEach(() => {
      UserSettingStore.reset()
      jest.restoreAllMocks()
    })

    it('checks all settings are true be default', () => {
      const wrapper = shallow(<MapConfiguration />)
      wrapper.find('.map-configuration-title').simulate('click')

      settings.forEach(setting =>
        expect(wrapper.find(`input[name="${setting}"]`).prop('checked')).toBe(true)
      )
    })

    it('reads correct values from store', () => {
      let id = 1

      UserSettingStore._loaded = true
      settings.forEach(setting => UserSettingStore._add({
        id: id++,
        key: setting,
        value: 'false',
      }))

      const wrapper = shallow(<MapConfiguration />)
      wrapper.find('.map-configuration-title').simulate('click')

      settings.forEach(setting =>
        expect(wrapper.find(`input[name="${setting}"]`).prop('checked')).toBe(false)
      )
    })

    it('saves settings to store', () => {
      let id = 1
      // mock request to server
      jest.spyOn(Api, 'objectsRequest').
      mockImplementation((url, method, _, response, { userSetting: { key, value } }) => {
        response.success({ id: id++, key, value })
      })

      const wrapper = shallow(<MapConfiguration />)
      wrapper.find('.map-configuration-title').simulate('click')

      // simulate checkbox uncheck event
      settings.forEach(setting =>
        wrapper.find(`input[name="${setting}"]`).simulate('change', {
          target: {
            name: setting,
            checked: false,
          },
        })
      )

      settings.forEach((setting) => {
        const value = UserSettingStore.getUserSettingByKeyAsBool(setting, 1, true)
        expect(value).toBe(false)
      })
    })
  })
})
