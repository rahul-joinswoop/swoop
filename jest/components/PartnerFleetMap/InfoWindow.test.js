import React from 'react'
import { mount } from 'enzyme'
import InfoWindow from 'components/PartnerFleetMap/InfoWindow'
import * as GoogleMaps from 'lib/google-maps'

describe('PartnerFleetMap.InfoWindow component', () => {
  afterAll(() => {
    jest.restoreAllMocks()
  })

  it('renders fine', () => {
    const address = 'Test Address'
    const markerMock = {
      getMap() {
        return 'map'
      },
    }
    const infoWindowMock = {
      addListener: jest.fn(),
      open: jest.fn(),
      setContent: jest.fn(),
      setZIndex: jest.fn(),
    }

    const createInfoWindow = jest.spyOn(GoogleMaps, 'createInfoWindow').
    mockImplementation(() => infoWindowMock)

    mount(<InfoWindow marker={markerMock}>{address}</InfoWindow>)

    expect(createInfoWindow).toHaveBeenCalledWith({
      children: address,
      content: address,
      marker: markerMock,
    })
    /* eslint-disable-next-line security/detect-non-literal-fs-filename */
    expect(infoWindowMock.open).toHaveBeenCalledWith('map', markerMock)
    expect(infoWindowMock.addListener).toHaveBeenCalledWith('domready', expect.any(Function))
    expect(infoWindowMock.setContent).toHaveBeenCalledWith(address)
    expect(infoWindowMock.setZIndex).toHaveBeenCalledWith(undefined)
  })
})
