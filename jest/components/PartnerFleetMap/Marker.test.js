import React from 'react'
import { mount } from 'enzyme'
import Marker from 'components/PartnerFleetMap/Marker'
import * as GoogleMaps from 'lib/google-maps'

jest.mock('components/PartnerFleetMap/InfoWindow')

describe('PartnerFleetMap.Marker component', () => {
  afterAll(() => {
    jest.restoreAllMocks()
  })

  it('renders correctly', () => {
    const markerMock = {
      addListener: jest.fn(),
      setIcon: jest.fn(),
      setMap: jest.fn(),
      setPosition: jest.fn(),
      setVisible: jest.fn(),
    }

    const location = {
      lat: 1,
      lng: 1,
      address: 'Test Address',
    }

    const createMarker = jest.spyOn(GoogleMaps, 'createMarker').
    mockImplementation(() => markerMock)

    const props = {
      icon: '/marker_green.png',
      id: 1,
      label: 'A',
      location,
      isSelected: true,
      setSelectedId: () => {},
    }

    const wrapper = mount(<Marker {...props} />)

    expect(createMarker).toHaveBeenCalledWith({
      defaultAnimation: 2,
      label: {
        color: '#FFF',
        text: props.label,
      },
      map: null,
    })

    expect(markerMock.setIcon).toHaveBeenCalledWith({ url: props.icon, labelOrigin: new google.maps.Point() })
    expect(markerMock.setPosition).toHaveBeenCalledWith({ lat: location.lat, lng: location.lng })

    expect(wrapper.exists('InfoWindow')).toBe(true)

    props.isSelected = false

    wrapper.setProps(props)

    expect(wrapper.exists('InfoWindow')).toBe(false)
  })
})
