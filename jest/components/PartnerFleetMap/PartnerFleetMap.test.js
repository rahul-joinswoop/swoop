import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment-timezone'
import PartnerFleetMap from 'components/PartnerFleetMap'
import VehicleStore from 'stores/vehicle_store'
import JobStore from 'stores/job_store'
import UsersStore from 'stores/users_store'
import UserSettingStore from 'stores/user_setting_store'
import Consts from 'consts'
import 'loader' // delete this file when we have converted coffeescript to JS

jest.mock('stores/vehicle_store')
jest.mock('stores/users_store')

describe('PartnerFleetMap Component', () => {
  let wrapper

  const initialProps = {
    assignDriverState: {
      showMap: jest.fn(),
      search: 'KonPartner',
    },
    assignDriverMethods: {
      getSortedDrivers: jest.fn(() => []),
      setSelectedDriverId: jest.fn(),
      setSearch: jest.fn(),
      setVehicles: jest.fn(),
    },
    record: {
      id: 1,
    },
  }

  it('renders without props because it is a route level component', () => {
    wrapper = shallow(<PartnerFleetMap />).dive()
    expect(wrapper).toBeDefined()
  })

  describe('setSelectedDriverId', () => {
    const newSelectedDriverId = 4
    it('set selectedDriverId in the state', () => {
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setSelectedDriverId(newSelectedDriverId)
      expect(wrapper.instance().state.selectedDriverId).toEqual(newSelectedDriverId)
    })
  })

  describe('setSearch', () => {
    const newSearchInput = 'blahblah'
    it('set searchInput in the state', () => {
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setSearch(newSearchInput)
      expect(wrapper.instance().state.search).toEqual(newSearchInput)
    })
  })

  describe('setVehicles', () => {
    const newVehicles = [5, 6, 7]
    it('set selectedDriverId in the state', () => {
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setVehicles(newVehicles)
      expect(wrapper.instance().state.savedVehicles).toEqual(newVehicles)
    })
  })

  describe('constructor', () => {
    it('calls setTimeout in 500ms to set state showMap to true', () => {
      jest.useFakeTimers()
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setState = jest.fn()
      expect(wrapper.instance().state.showMap).toEqual(false)
      expect(setTimeout).toHaveBeenCalledTimes(1)
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500)
    })
  })

  describe('Swoop Map', () => {
    let vehicleStoreGet

    afterAll(() => {
      vehicleStoreGet.mockRestore()
    })

    it('handleTruckClickOnMap calls VehicleStore.get with truck id', () => {
      vehicleStoreGet = jest.spyOn(VehicleStore, 'get').mockImplementation(id => id)

      const testDriverId = 1
      wrapper = shallow(<PartnerFleetMap {...initialProps} />).dive()
      const instance = wrapper.instance()
      // instance.getVehicleDriverId = jest.fn(id => id)
      instance.setSelectedDriverId = jest.fn()
      instance.handleTruckClickOnMap(testDriverId)
      expect(vehicleStoreGet).toHaveBeenCalledWith(testDriverId, 'driver_id', instance.getId())
      expect(instance.setSelectedDriverId).toHaveBeenCalledWith(testDriverId)
    })
  })

  describe('getDriverJobs', () => {
    const testId = 6
    it('returns job that has matching rescue_driver_id and dispatched and assigned status', () => {
      const matchingJob = {
        rescue_driver_id: testId,
        status: 'Assigned',
      }
      JobStore.getJobs = jest.fn(() => ([
        {
          rescue_driver_id: 3321,
          status: 'Bad status! Bad!',
        },
        matchingJob,
      ]))
      wrapper = shallow(<PartnerFleetMap />).dive()
      expect(wrapper.instance().getDriverJobs(testId)[0]).toEqual(matchingJob)
    })
    it('returns empty array where no matching job', () => {
      JobStore.getJobs = jest.fn(() => ([
        {
          rescue_driver_id: 3321,
          status: 'Bad status! Bad!',
        },
      ]))
      wrapper = shallow(<PartnerFleetMap />).dive()
      expect(wrapper.instance().getDriverJobs(testId)).toEqual([])
    })
  })

  describe('filterTrucksAndDriversBySearch', () => {
    let searchString = ''
    const crusherTruck = {
      driver_name: 'bob',
      name: 'The Crusher!',
    }
    const punisherTruck = {
      driver_name: 'sally',
      name: 'Punisher!',
    }
    const trucksAndDrivers = [crusherTruck, punisherTruck]
    const crusherTruckNoDriver = {
      name: 'The Crusher!',
    }
    const punisherTruckNoDriver = {
      name: 'Punisher!',
    }
    const trucksAndNoDrivers = [crusherTruckNoDriver, punisherTruckNoDriver]
    it('returns all trucksAndDrivers if search string is empty', () => {
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setSearch(searchString)
      expect(wrapper.instance().filterTrucksAndDriversBySearch(trucksAndDrivers)).toEqual(trucksAndDrivers)
    })
    it('returns trucks filtered by search string match to driver_name if that is present', () => {
      searchString = 'bob'
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setSearch(searchString)
      expect(wrapper.instance().filterTrucksAndDriversBySearch(trucksAndDrivers)).toEqual([crusherTruck])
    })
    it('returns trucks filtered by search string match to truck name if that is present', () => {
      searchString = 'punisher'
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setSearch(searchString)
      expect(wrapper.instance().filterTrucksAndDriversBySearch(trucksAndNoDrivers)).toEqual([punisherTruckNoDriver])
    })
  })

  describe('filterTrucksAndDriversByOnDuty', () => {
    const crusherTruck = {
      driver_id: 1,
      driver_name: 'bob',
      name: 'The Crusher!',
    }
    const punisherTruck = {
      driver_name: 'sally',
      name: 'Punisher!',
    }
    const trucksAndDrivers = [crusherTruck, punisherTruck]

    afterAll(() => {
      UserSettingStore.reset()
    })

    it('returns all trucksAndDrivers if showOffDuty is true', () => {
      wrapper = shallow(<PartnerFleetMap />).dive()
      expect(wrapper.instance().filterTrucksAndDriversByOnDuty(trucksAndDrivers)).toEqual(trucksAndDrivers)
    })

    it('returns on duty trucksAndDrivers if showOffDuty is false', () => {
      const onDutyPartner = {
        on_duty: true,
        name: 'KonPartner',
      }
      UsersStore.get = jest.fn(() => onDutyPartner)

      // set setting in UserStore
      UserSettingStore._loaded = true
      UserSettingStore._add({
        id: 7,
        key: Consts.USER_SETTING.SHOW_OFF_DUTY_PARTNER_FLEET_MAP,
        value: 'false',
      })

      wrapper = shallow(<PartnerFleetMap />).dive()

      expect(wrapper.instance().filterTrucksAndDriversByOnDuty(trucksAndDrivers)).toEqual([crusherTruck])
      expect(UsersStore.get).toHaveBeenCalled()
    })
  })

  describe('getSortedTrucksAndDrivers', () => {
    const crusherTruck = {
      driver_id: 1,
      driver_name: 'bob',
      name: 'The Crusher!',
    }
    const punisherTruck = {
      driver_name: 'sally',
      name: 'Punisher!',
    }
    const trucksAndDrivers = [crusherTruck, punisherTruck]

    VehicleStore.getSortedVehicles = jest.fn(() => trucksAndDrivers)

    it('calls VehicleStore.getSortedVehicles', () => {
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().getSortedTrucksAndDrivers()
      expect(VehicleStore.getSortedVehicles).toHaveBeenCalled()
    })
  })

  describe('driver routes', () => {
    beforeAll(() => {
      VehicleStore.getSortedVehicles = jest.fn().mockReturnValue([
        {
          id: 1,
          name: 'truck1',
          driver_id: 1,
          driver_name: 'driver1',
          lat: 1,
          lng: 1,
          model: 'Vehicle Model',
        },
        {
          id: 2,
          name: 'truck2',
          driver_id: 2,
          driver_name: 'driver2',
          location_updated_at: moment().subtract(280, 'seconds').toISOString(),
        },
      ])

      JobStore.getJobs = jest.fn().mockReturnValue([
        {
          id: 1,
          rescue_driver_id: 1,
          status: Consts.ENROUTE,
          notes: 'Notes',
          no_keys: false,
          service_location: {
            lat: 2,
            lng: 2,
            address: 'Test Address',
            state: 'CA',
            city: 'San Francisco',
            street: '1121 Mission St',
          },
        },
      ])
    })

    afterAll(() => {
      UserSettingStore.reset()
      jest.restoreAllMocks()
    })

    it('shows driver routes by default', () => {
      jest.useFakeTimers()
      JobStore.loadedActiveJobs = true
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.setState({ showMap: true })
      jest.runOnlyPendingTimers()
      expect(wrapper.instance().props.showRoutes).toBe(true)
      const firstTruck = wrapper.find('Truck').first().shallow().dive()
      expect(firstTruck.exists('JobRoute')).toBe(true)
      wrapper.unmount()
    })

    it('hides driver routes by clicking on Hide Routes button', () => {
      JobStore.loadedActiveJobs = false
      wrapper = shallow(<PartnerFleetMap />)

      UserSettingStore._loaded = true
      UserSettingStore._add({
        id: 6,
        key: Consts.USER_SETTING.SHOW_ROUTES_PARTNER_FLEET_MAP,
        value: 'false',
      })

      const map = wrapper.dive()
      map.setState({ showMap: true })

      const firstTruck = map.find('Truck').first().shallow()
      expect(firstTruck.exists('FullRoute')).toBe(false)
    })
  })

  describe('selected job', () => {
    it('sets selectedJobId', () => {
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setSelectedJobId(1)
      expect(wrapper.state('selectedJobId')).toBe(1)

      wrapper.instance().setSelectedJobId(1)
      expect(wrapper.state('selectedJobId')).toBeNull()
    })

    it('resets selectedJobId', () => {
      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setSelectedJobId(1)
      expect(wrapper.state('selectedJobId')).toBe(1)

      wrapper.instance().resetSelectedJobId([], 1)
      expect(wrapper.state('selectedJobId')).toBeNull()
    })

    it('keeps selectedJobId', () => {
      const jobId = 1

      wrapper = shallow(<PartnerFleetMap />).dive()
      wrapper.instance().setSelectedJobId(jobId)
      expect(wrapper.state('selectedJobId')).toBe(jobId)

      const drivers = [{
        jobs: [{ id: jobId }],
      }]
      wrapper.instance().resetSelectedJobId(drivers, jobId)
      expect(wrapper.state('selectedJobId')).toBe(jobId)
    })
  })
})
