import React from 'react'
import { mount } from 'enzyme'
import InfoWindow from 'components/PartnerFleetMap/InfoWindow'
import * as GoogleMaps from 'lib/google-maps'
import MicroEvent from 'microevent-github'
import TruckMarker from 'components/PartnerFleetMap/TruckMarker'
import VehicleStore from 'stores/vehicle_store'

describe('PartnerFleetMap.TruckMarker component', () => {
  afterAll(() => {
    VehicleStore.reset()
    jest.restoreAllMocks()
  })

  it('renders correctly', () => {
    const gMapMock = {
      panTo: jest.fn(),
    }

    let onMarkerClick
    const markerMock = {
      addListener: jest.fn((event, callback) => onMarkerClick = callback),
      getPosition: jest.fn(() => ({
        equals: () => true,
      })),
      // setIcon: jest.fn(),
      getMap: () => gMapMock,
      setMap: jest.fn(),
      setPosition: jest.fn(),
      setVisible: jest.fn(),
      setZIndex: jest.fn(),
    }

    const createMarker = jest.spyOn(GoogleMaps, 'createMarker').
    mockImplementation(() => markerMock)

    const eventHub = new MicroEvent()

    const truckMock = {
      id: 5748,
      lat: 9485,
      lng: 14748,
      driver_id: 8928,
      name: 'Truck Name',
    }

    VehicleStore.loadData([truckMock])

    const markerClickMock = jest.fn()

    const infoWindowMock = {
      addListener: jest.fn(),
      open: jest.fn(),
      setContent: jest.fn(),
      setZIndex: jest.fn(),
    }

    jest.spyOn(GoogleMaps, 'createInfoWindow').
    mockImplementation(() => infoWindowMock)

    const wrapper = mount(<TruckMarker
      animation={{}}
      eventHub={eventHub}
      onClick={markerClickMock}
      truck={truckMock}
    />)
    const instance = wrapper.instance()

    expect(createMarker).toHaveBeenCalledWith({
      icon: {
        anchor: new google.maps.Point(),
        scaledSize: new google.maps.Size(),
        url: `/assets/images/truck.png?id=${truckMock.id}`,
      },
      position: truckMock,
      optimized: false,
      title: truckMock.name,
    })

    expect(markerMock.addListener).toHaveBeenCalledWith('click', instance.onMarkerClick)
    expect(markerMock.setPosition).toHaveBeenCalledWith(truckMock)
    expect(markerMock.setVisible).toHaveBeenCalledWith(true)

    expect(wrapper.contains(<InfoWindow content={wrapper.state('infoWindowContent')} disableAutoPan marker={markerMock} />)).toBe(true)

    onMarkerClick()

    expect(markerClickMock).toHaveBeenCalledWith(truckMock.id)
    expect(markerMock.setZIndex).toHaveBeenCalledWith(NaN)
    /* eslint-disable security/detect-non-literal-fs-filename */
    expect(infoWindowMock.open).toHaveBeenCalledWith(gMapMock, markerMock)

    eventHub.trigger(`truckClick:${truckMock.id}`)

    expect(markerMock.getPosition).toHaveBeenCalled()
    expect(gMapMock.panTo).toHaveBeenCalled()
    expect(markerMock.setZIndex).toHaveBeenCalledWith(NaN)
    expect(infoWindowMock.open).toHaveBeenCalledWith(gMapMock, markerMock)
    /* eslint-enable security/detect-non-literal-fs-filename */
  })
})
