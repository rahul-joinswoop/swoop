import React from 'react'
import MicroEvent from 'microevent-github'
import { shallow } from 'enzyme'
import Consts from 'consts'
import Truck from 'components/PartnerFleetMap/Truck'

describe('PartnerFleetMap.Truck component', () => {
  it('renders correctly', () => {
    const evenHub = new MicroEvent()
    const jobs = [
      {
        id: 1,
        drop_location: { lat: 1, lng: 1 },
        service_location: { lat: 2, lng: 2 },
        status: Consts.ENROUTE,
      },
      {
        id: 2,
        drop_location: { lat: 435, lng: 833 },
        service_location: { lat: 2627, lng: 5473 },
        status: Consts.ONSITE,
      },
      {
        id: 3,
        drop_location: { lat: 584, lng: 241 },
        service_location: { lat: 124, lng: 937 },
        status: Consts.TOWDESTINATION,
      },
    ]

    const wrapper = shallow(<Truck
      eventHub={evenHub}
      jobs={jobs}
      onClick={() => {}}
      selectedJobId={3}
      setSelectedJobId={() => {}}
      showRoutes
      truck={{
        lat: 234, lng: 345,
      }}
    />).dive()

    expect(wrapper.exists('TruckMarker')).toBe(true)

    const routes = wrapper.find('JobRoute')

    const jobRoute1 = routes.at(0).shallow()
    expect(jobRoute1.exists('Marker[id=1][label="A"]')).toBe(true)

    const jobRoute2 = routes.at(1).shallow()
    expect(jobRoute2.exists('Marker[id=2][label="B"]')).toBe(true)

    const jobRoute3 = routes.at(2).shallow()
    expect(jobRoute3.exists('Marker[id=3][label="B"]')).toBe(true)
    expect(jobRoute3.exists('Bounds')).toBe(true)
  })
})
