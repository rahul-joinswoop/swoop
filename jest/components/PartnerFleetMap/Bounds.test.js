import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import ReactTestUtils from 'react-dom/test-utils'
import { MapContext } from 'components/PartnerFleetMap/Map'
import { GlobalBounds, GlobalBoundsContext } from 'components/PartnerFleetMap/Bounds'

class Wrapper extends Component {
  render() {
    return this.props.children
  }
}

const gMapMock = {
  fitBounds: jest.fn(),
  setOptions: jest.fn(),
}

const container = document.body.appendChild(document.createElement('div'))

function renderGlobalBounds(props, addRoute) {
  let component

  ReactTestUtils.act(() => {
    component = ReactDOM.render(
      <Wrapper>
        <MapContext.Provider value={gMapMock}>
          <GlobalBounds {...props}>
            <GlobalBoundsContext>
              {addRoute}
            </GlobalBoundsContext>
          </GlobalBounds>
        </MapContext.Provider>
      </Wrapper>,
      container
    )
  })

  return ReactTestUtils.findRenderedComponentWithType(component, GlobalBounds)
}

// hack to execute following code on next event loop iteration
const delay = (timeout = 0) => new Promise(resolve => setTimeout(resolve, timeout))

describe('PartnerFleetMap.GlobalBounds component', () => {
  afterAll(() => {
    jest.restoreAllMocks()
    google.maps.LatLngBounds.mockClear()
  })

  it('renders correctly', async () => {
    google.maps.LatLngBounds.
    mockImplementation((sw, ne) => ({ sw, ne }))

    const drivers = [
      {
        truck: { lat: 1, lng: 1 },
        jobs: [
          // { id: 345 }, { id: 783 },
        ],
      },
      {
        truck: { lat: 3, lng: 3 },
        jobs: [
          // { id: 484 }, { id: 252 },
        ],
      },
    ]

    renderGlobalBounds({ drivers }, (addRoute) => {
      setTimeout(() => {
        addRoute(345, {
          legs: [{
            steps: [
              {
                start_location: { lat: 264, lng: 635 },
                end_location: { lat: 824, lng: 253 },
              },
              {
                start_location: { lat: 432, lng: 254 },
                end_location: { lat: 974, lng: 374 },
              },
            ],
          }],
        })
      }, 0)
    })

    drivers[0].jobs = [
      { id: 345 }, { id: 783 },
    ]
    drivers[1].jobs = [
      { id: 484 }, { id: 252 },
    ]

    const instance = renderGlobalBounds({ drivers }, (addRoute) => {
      setTimeout(() => {
        addRoute(252, {
          legs: [{
            steps: [
              {
                start_location: { lat: 274, lng: 843 },
                end_location: { lat: 719, lng: 826 },
              },
              {
                start_location: { lat: 947, lng: 574 },
                end_location: { lat: 364, lng: 273 },
              },
            ],
          }],
        })
      }, 0)
    })

    await (delay(600))

    expect(instance.jobMap).toEqual({
      252: false, 345: false, 484: true, 783: true,
    })
    expect(gMapMock.setOptions).toHaveBeenCalledWith({ maxZoom: 15 })
    expect(gMapMock.fitBounds).toHaveBeenCalledWith(
      {
        ne: { lat: 974, lng: 843 },
        sw: { lat: 1, lng: 1 },
      },
      {
        bottom: 0, left: 0, right: 0, top: 0,
      },
    )
  })
})
