import React from 'react'
import { mount } from 'enzyme'
import { first } from 'lodash'
import Consts from 'consts'
import JobRoute from 'components/PartnerFleetMap/JobRoute'
import { GlobalBoundsContext } from 'components/PartnerFleetMap/Bounds'
import { MapContext } from 'components/PartnerFleetMap/Map'

describe('PartnerFleetMap.JobRoutes components', () => {
  const directionsServiceMock = {
    route: jest.fn(),
  }

  const gMapMock = {
    fitBounds: jest.fn(),
  }

  // hack to execute following code on next event loop iteration
  const delay = () => new Promise(resolve => setTimeout(resolve, 0))

  beforeAll(() => {
    // mock google.maps.DirectionsService
    google.maps.DirectionsService.mockImplementationOnce(() => directionsServiceMock)
  })

  afterEach(() => {
    google.maps.DirectionsService.mockClear()
  })

  describe('PartnerFleetMap.JobRoutes.JobRoute component', () => {
    it('renders correctly when enroute', async () => {
      const directions = {
        routes: [{
          legs: [
            {
              steps: [
                {
                  start_location: { lat: 1, lng: 1 },
                  end_location: { lat: 3, lng: 3 },
                  path: [{ lat: 1, lng: 1 }, { lat: 2, lng: 2 }, { lat: 3, lng: 3 }],
                },
              ],
            },
          ],
        }],
      }

      directionsServiceMock.route.
      mockImplementationOnce((request, cb) => cb(directions, 'OK'))

      // I use `mount` here because I could not make `context` working with `shallow`
      const wrapper = mount(
        <MapContext.Provider value={gMapMock}>
          <GlobalBoundsContext.Provider
            value={(jobId, route) => {
              expect(jobId).toBe(363)
              expect(route).toBe(first(directions.routes))
            }}
          >
            <JobRoute
              isSelected
              job={{
                id: 363,
                drop_location: { lat: 10, lng: 11 },
                service_location: { lat: 1, lng: 1 },
                status: Consts.ENROUTE,
              }}
              path={[]}
              setSelectedJobId={jest.fn()}
              setTruckRoute={jest.fn()}
              truck={{ id: 7273, lat: 234, lng: 673 }}
            />
          </GlobalBoundsContext.Provider>
        </MapContext.Provider>
      )

      await delay()

      wrapper.update()

      expect(wrapper.exists('Marker[label="A"]')).toBe(true)
      expect(wrapper.exists('Marker[label="B"]')).toBe(true)
      expect(wrapper.exists('Directions')).toBe(true)
      expect(wrapper.exists('Bounds')).toBe(true)
    })

    it('renders correctly when towing', async () => {
      const directions = {
        routes: [{
          legs: [
            {
              steps: [
                {
                  start_location: { lat: 1, lng: 1 },
                  end_location: { lat: 3, lng: 3 },
                  path: [{ lat: 1, lng: 1 }, { lat: 2, lng: 2 }, { lat: 3, lng: 3 }],
                },
              ],
            },
          ],
        }],
      }

      directionsServiceMock.route.
      mockImplementationOnce((request, cb) => cb(directions, 'OK'))

      // I use `mount` here because I could not make `context` working with `shallow`
      const wrapper = mount(
        <MapContext.Provider value={gMapMock}>
          <GlobalBoundsContext.Provider value={(jobId, route) => {
            expect(jobId).toBe(363)
            expect(route).toBe(first(directions.routes))
          }}
          >
            <JobRoute
              isSelected
              job={{
                id: 363,
                drop_location: { lat: 10, lng: 11 },
                service_location: { lat: 1, lng: 1 },
                status: Consts.TOWING,
              }}
              path={[]}
              setSelectedJobId={jest.fn()}
              setTruckRoute={jest.fn()}
              truck={{ id: 7273, lat: 234, lng: 673 }}
            />
          </GlobalBoundsContext.Provider>
        </MapContext.Provider>
      )

      await delay()

      wrapper.update()

      expect(wrapper.exists('Marker[label="A"]')).toBe(false)
      expect(wrapper.exists('Marker[label="B"]')).toBe(true)
      expect(wrapper.exists('Directions')).toBe(true)
      expect(wrapper.exists('Bounds')).toBe(true)
    })
  })
})
