import { first } from 'lodash'
import MapStore from 'stores/map_store'
import { loadRoute } from 'components/PartnerFleetMap/DirectionsService'

describe('PartnerFleetMap.DirectionsService', () => {
  const service = MapStore.getDirectionsService()

  afterEach(() => {
    service.route.mockClear()
  })

  it('works correctly', async () => {
    const directions1 = { routes: [{ legs: [{ steps: [] }] }] }
    const directions2 = { routes: [{ legs: [{ steps: [] }] }] }

    service.route.
    mockImplementationOnce((request, cb) => {
      cb(directions1, 'OK')
    }).
    mockImplementationOnce((request, cb) => {
      cb(directions2, 'OK')
    })

    const request1 = {
      origin: { lat: 1, lng: 1 },
      destination: { lat: 2, lng: 2 },
    }

    let directions = await loadRoute(request1)

    expect(service.route).toHaveBeenCalledWith(request1, expect.any(Function))
    expect(directions).toBe(first(directions1.routes))
    expect(directions).not.toBe(first(directions2.routes))

    directions = await loadRoute(request1)
    expect(service.route).toHaveBeenCalledTimes(1)
    expect(directions).toBe(first(directions1.routes))
    expect(directions).not.toBe(first(directions2.routes))

    const request2 = {
      origin: { lat: 3, lng: 3 },
      destination: { lat: 4, lng: 4 },
    }

    directions = await loadRoute(request2)

    expect(service.route).toHaveBeenCalledWith(request2, expect.any(Function))
    expect(service.route).toHaveBeenCalledTimes(2)
    expect(directions).toBe(first(directions2.routes))
    expect(directions).not.toBe(first(directions1.routes))
  })
})
