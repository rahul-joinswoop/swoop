import React from 'react'
import { mount } from 'enzyme'
import * as GoogleMaps from 'lib/google-maps'
import Directions from 'components/PartnerFleetMap/Directions'

describe('PartnerFleetMap.Directions component', () => {
  afterAll(() => {
    jest.restoreAllMocks()
  })

  it('renders fine', () => {
    const polylineMock = {
      setOptions: jest.fn(),
    }

    const route = {
      legs: [
        {
          steps: [{
            path: [1, 2, 3],
          }],
        },
        {
          steps: [{
            path: [11, 12, 13],
          }],
        },
      ],
    }

    const createPolyline = jest.spyOn(GoogleMaps, 'createPolyLine').
    mockImplementation(() => polylineMock)

    mount(<Directions route={route} />)

    expect(createPolyline).toHaveBeenCalledWith({
      map: null,
      strokeColor: '#cccccc',
    })

    expect(polylineMock.setOptions).toHaveBeenCalledWith({
      path: [1, 2, 3], strokeColor: '#cccccc', zIndex: 0,
    })

    mount(<Directions isSelected route={route} />)

    expect(polylineMock.setOptions).toHaveBeenCalledWith({
      path: [1, 2, 3, 11, 12, 13], strokeColor: '#90bad4', zIndex: 1,
    })

    mount(<Directions path={[111, 112, 113]} route={route} />)

    expect(polylineMock.setOptions).toHaveBeenCalledWith({
      path: [111, 112, 113], strokeColor: '#cccccc', zIndex: 0,
    })
  })
})
