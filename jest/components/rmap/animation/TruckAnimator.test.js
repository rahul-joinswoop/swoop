import React from 'react'
import { shallow } from 'enzyme'
import TruckAnimator from 'components/rmap/animation/TruckAnimator'

jest.mock('components/rmap/animation/LocationAnimator')
jest.mock('components/rmap/animation/RouteAnimator')

class LatLngMock {
  constructor(lat, lng) {
    this.lat = lat
    this.lng = lng
  }

  lat() {
    return this.lat
  }

  lng() {
    return this.lng
  }
}

describe('rmap/animation/TruckAnimator component', () => {
  TruckAnimator.DEFAULT_DURATION = 2000

  afterEach(() => {
    jest.restoreAllMocks()
    google.maps.LatLng.mockClear()
  })

  it('renders correctly WITH route', () => {
    const latLngRegistry = []

    google.maps.LatLng.
    mockImplementation((lat, lng) => {
      const latLng = new LatLngMock(lat, lng)
      latLngRegistry.push(latLng)
      return latLng
    })

    const route = {}

    const truck = {
      lat: 5,
      lng: 5,
      location_updated_at: '2018-09-20T23:00:00Z',
    }

    const wrapper = shallow(
      <TruckAnimator route={route} truck={truck}>
        {() => null}
      </TruckAnimator>
    )

    const truck2 = {
      lat: 8,
      lng: 8,
      location_updated_at: '2018-09-20T23:00:05Z',
    }

    wrapper.setProps({ truck: truck2 })

    expect(wrapper.instance().animator.newLocation).toHaveBeenCalledWith({}, latLngRegistry[0], latLngRegistry[1], 2)
  })

  it('renders correctly WITHOUT route', () => {
    const latLngRegistry = []

    google.maps.LatLng.
    mockImplementation((lat, lng) => {
      const latLng = new LatLngMock(lat, lng)
      latLngRegistry.push(latLng)
      return latLng
    })

    const truck = {
      lat: 5,
      lng: 5,
      location_updated_at: '2018-09-20T23:00:00Z',
    }

    const wrapper = shallow(
      <TruckAnimator route={null} truck={truck}>
        {() => null}
      </TruckAnimator>
    )

    const truck2 = {
      lat: 8,
      lng: 8,
      location_updated_at: '2018-09-20T23:00:05Z',
    }

    wrapper.setProps({ truck: truck2 })

    const instance = wrapper.instance()
    expect(instance.animator.newLocation).not.toHaveBeenCalled()
    expect(instance.locationAnimator.animate).toHaveBeenCalledWith(latLngRegistry[0], latLngRegistry[1], 2000)
  })
})
