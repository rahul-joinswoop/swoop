import LocationAnimator from 'components/rmap/animation/LocationAnimator'

describe('rmap/animation/LocationAnimator class', () => {
  // hack to execute following code on next event loop iteration
  const delay = (timeout = 0) => new Promise(resolve => setTimeout(resolve, timeout))

  const RealDate = Date

  const mockDate = (isoDate) => {
    let count = 0
    const baseTime = (new RealDate(isoDate)).getTime()

    global.Date = class extends Date {
      constructor() {
        const delta = count++ * 16
        return super(baseTime + delta)
      }
    }
  }

  afterAll(() => {
    jest.restoreAllMocks()
    google.maps.LatLng.mockClear()
    global.Date = RealDate
  })

  it('animates correctly', async () => {
    mockDate('2018-09-20T23:00:00Z')
    const animator = new LocationAnimator()

    const from = { lat: () => 1, lng: () => 1 }
    const to = { lat: () => 10, lng: () => 10 }
    const duration = 100

    google.maps.LatLng.
    mockImplementation((lat, lng) => ({ lat: () => lat, lng: () => lng }))

    const updatesArr = [2.44, 3.88, 5.32, 6.76, 8.2, 9.64, 10]

    let count = 0
    animator.
    onComplete((location) => {
      expect(location.lat()).toBe(10)
      expect(location.lng()).toBe(10)
    }).
    onUpdate((location) => {
      expect(location.lat()).toBe(updatesArr[count])
      expect(location.lng()).toBe(updatesArr[count])
      count++
    })

    animator.animate(from, to, duration)

    await delay(500)
  })
})
