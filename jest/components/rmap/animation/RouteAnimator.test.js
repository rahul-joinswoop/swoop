import { first } from 'lodash'
import RouteAnimator from 'components/rmap/animation/RouteAnimator'

describe('rmap/animation/RouteAnimator class', () => {
  // hack to execute following code on next event loop iteration
  const delay = (timeout = 0) => new Promise(resolve => setTimeout(resolve, timeout))

  const RealDate = Date

  const mockDate = (isoDate) => {
    let count = 0
    const baseTime = (new RealDate(isoDate)).getTime()

    global.Date = class extends Date {
      constructor() {
        const delta = count++ * 20
        return super(baseTime + delta)
      }
    }
  }

  afterAll(() => {
    jest.restoreAllMocks()
    google.maps.LatLng.mockClear()
    google.maps.geometry.poly.isLocationOnEdge.mockClear()
    google.maps.geometry.spherical.computeDistanceBetween.mockClear()
    global.Date = RealDate
  })

  it('works correctly', async () => {
    const updatesArr = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5, 5.1, 5.199999999999999, 5.300000000000001, 5.4, 5.5, 5.6, 5.7, 5.8, 5.9, 6, 6.08, 6.16, 6.24, 6.32, 6.4, 6.48, 6.5600000000000005, 6.64, 6.72, 6.8, 6.88, 6.96, 7.04, 7.12, 7.2, 7.28, 7.36, 7.4399999999999995, 7.52, 7.6, 7.68, 7.76, 7.84, 7.92, 8, 8.08, 8.16, 8.24, 8.32, 8.4, 8.48, 8.56, 8.64, 8.72, 8.8, 8.879999999999999, 8.96, 9.04, 9.120000000000001, 9.2, 9.28, 9.36, 9.44, 9.52, 9.6, 9.68, 9.76, 9.84, 9.92, 10]

    const route = {
      legs: [{
        distance: { value: 100 },
        duration: { value: 5 },
        start_location: { lat: () => 1, lng: () => 1 },
        end_location: { lat: () => 10, lng: () => 10 },
        steps: [
          {
            distance: { value: 3 },
            duration: { value: 1 },
            start_location: { lat: () => 1, lng: () => 1 },
            end_location: { lat: () => 3, lng: () => 3 },
            path: [{ lat: () => 1, lng: () => 1 }, { lat: () => 3, lng: () => 3 }],
          },
          {
            distance: { value: 3 },
            duration: { value: 1 },
            start_location: { lat: () => 3, lng: () => 3 },
            end_location: { lat: () => 6, lng: () => 6 },
            path: [{ lat: () => 3, lng: () => 3 }, { lat: () => 6, lng: () => 6 }],
          }, {
            distance: { value: 4 },
            duration: { value: 3 },
            start_location: { lat: () => 6, lng: () => 6 },
            end_location: { lat: () => 10, lng: () => 10 },
            path: [{ lat: () => 6, lng: () => 6 }, { lat: () => 10, lng: () => 10 }],
          },
        ],
      }],
    }

    mockDate('2018-09-20T23:00:00Z')

    google.maps.geometry.poly.isLocationOnEdge.
    mockImplementation(() => true)

    google.maps.LatLng.
    mockImplementation((lat, lng) => ({
      equals: latLng => latLng.lat() === lat && latLng.lng() === lng,
      lat: () => lat,
      lng: () => lng,
    }))

    google.maps.geometry.spherical.computeDistanceBetween.
    mockImplementation((from, to) => Math.sqrt((from.lat() - to.lat()) ** 2 + (from.lng() - to.lng()) ** 2))

    const animator = new RouteAnimator()

    let count = 0
    animator.onUpdate(({ location }) => {
      expect(location.lat()).toBe(updatesArr[count])
      expect(location.lng()).toBe(updatesArr[count])
      count++
    })

    const location1 = { lat: () => 5, lng: () => 5 }
    animator.newLocation(route, first(route.legs.start_location), location1, 1)

    const location2 = { lat: () => 8, lng: () => 8 }
    animator.newLocation(route, location1, location2, 1)

    await delay(3000)
  })
})
