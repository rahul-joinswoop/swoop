import React from 'react'
import { shallow } from 'enzyme'
import MapStore from 'stores/map_store'
import SiteMarker from 'components/rmap/SiteMarker'
import { set } from 'lodash'

describe('Component rmap/SiteMarker', () => {
  afterAll(() => {
    delete global.google
    jest.restoreAllMocks()
  })

  it('works correctly', () => {
    const removeListener = jest.fn()

    set(global, 'google.maps.event', { removeListener })

    const site = {
      name: 'Site',
      location: {
        lat: 1,
        lng: 1,
      },
    }

    const mapMock = {
      addListener: jest.fn().mockReturnValue(() => {}),
      getZoom: jest.fn().mockReturnValue(4),
    }

    const mouseEventCallbacks = {}
    const markerMock = {
      addListener: (event, cb) => {
        mouseEventCallbacks[event] = cb
      },
      setMap: jest.fn(),
      setPosition: jest.fn(),
      setVisible: jest.fn(),
    }

    const createMarker = jest.spyOn(MapStore, 'createMarker').
    mockImplementation(() => markerMock)

    const wrapper = shallow(<SiteMarker map={mapMock} site={site} />)

    expect(createMarker).toHaveBeenCalledWith({ icon: { url: '/assets/images/marker_blue_site.png' } })

    expect(markerMock.setPosition).toHaveBeenCalledWith(site.location)
    expect(markerMock.setVisible).toHaveBeenCalledWith(true)
    expect(wrapper.exists('InfoWindow')).toBe(false)
    expect(markerMock.setMap).toHaveBeenCalledWith(mapMock)

    expect(mouseEventCallbacks).toHaveProperty('mouseover')
    expect(mouseEventCallbacks).toHaveProperty('mouseout')

    expect(mapMock.addListener).not.toHaveBeenCalled()
    expect(removeListener).not.toHaveBeenCalled()

    expect(wrapper.instance().zoomListener).toBeUndefined()

    mouseEventCallbacks.mouseover()
    const infoWindow = wrapper.find('InfoWindow')
    expect(infoWindow).toBeDefined()
    expect(infoWindow.prop('getContent')()).toBe(site.name)

    mouseEventCallbacks.mouseout()
    expect(wrapper.exists('InfoWindow')).toBe(false)

    mapMock.getZoom.mockReturnValue(10)

    wrapper.setProps({ map: mapMock, site, showOnZoom: 8 })

    expect(markerMock.setVisible).toHaveBeenCalledWith(true)
    expect(mapMock.addListener).toHaveBeenCalled()
    expect(wrapper.instance().zoomListener).toBeDefined()

    wrapper.setProps({ map: mapMock, site, showOnZoom: 0 })

    expect(markerMock.setVisible).toHaveBeenCalledWith(true)
    expect(removeListener).toHaveBeenCalled()
    expect(wrapper.instance().zoomListener).toBeUndefined()

    wrapper.setProps({ map: mapMock, site, showOnZoom: 13 })
    expect(markerMock.setVisible).toHaveBeenCalledWith(false)
  })
})
