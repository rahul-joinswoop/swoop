import React from 'react'
import { shallow } from 'enzyme'
import AssignDriverButton from 'components/simple/assign_driver_button'

describe('Assign Driver Button', () => {
  let wrapper

  it('renders when record is passed', () => {
    wrapper = shallow(<AssignDriverButton record={{}} />)
    expect(wrapper).toBeDefined()
  })

  it('does not render when record is missing', () => {
    wrapper = shallow(<AssignDriverButton />)
    expect(wrapper.type()).toEqual(null)
  })
})
