import React from 'react'
import { shallow } from 'enzyme'
import jquery from 'jquery'
import CheckboxSetting from 'components/settings/configure/CheckboxSetting'
import SettingStore from 'stores/setting_store'

describe('CheckboxSetting component', () => {
  afterEach(() => {
    jest.restoreAllMocks()
    SettingStore.reset()
  })

  it('renders', () => {
    SettingStore.loadData([{
      id: 1,
      key: 'test_setting',
      name: 'Important Setting',
      value: true,
    }])

    const wrapper = shallow(<CheckboxSetting
      id="html_id"
      name="test_setting"
      subtext="Setting Description"
      text="Test Setting"
    />)

    const deleteSettingItem = jest.spyOn(SettingStore, 'deleteItem')

    expect(wrapper.find('input#html_id').prop('checked')).toBe(true)

    wrapper.find('input#html_id').simulate('change', { target: { checked: false } })

    expect(wrapper.find('input#html_id').prop('checked')).toBe(false)

    expect(deleteSettingItem).toHaveBeenCalledWith({ id: 1 })
  })

  it('uses inverted correctly', () => {
    const wrapper = shallow(<CheckboxSetting
      id="html_id"
      inverted
      name="test_setting"
      subtext="Setting Description"
      text="Test Setting"
    />)

    jest.spyOn(jquery, 'ajax').mockImplementation(({ success }) => {
      success({
        id: 1,
        key: 'test_setting',
        name: 'Important Setting',
        value: true,
      })

      return { fail: () => {} }
    })

    const addSettingItem = jest.spyOn(SettingStore, 'addItem')
    const deleteSettingItem = jest.spyOn(SettingStore, 'deleteItem')

    expect(wrapper.find('input#html_id').prop('checked')).toBe(true)

    wrapper.find('input#html_id').simulate('change', { target: { checked: false } })

    expect(wrapper.find('input#html_id').prop('checked')).toBe(false)

    expect(addSettingItem).toHaveBeenCalledWith({
      value: true,
      key: 'test_setting',
    })

    expect(deleteSettingItem).not.toHaveBeenCalled()
  })
})
