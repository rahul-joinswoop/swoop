import React from 'react'
import { shallow } from 'enzyme'
import EnterpriseBorder from 'components/EnterpriseBorder'
import UserStore from 'stores/user_store'

describe('EnterpriseBorder component', () => {
  afterEach(() => {
    jest.restoreAllMocks()
    UserStore.setUser(null)
  })

  it('renders MobileAppPrompt', () => {
    UserStore.setUser({ roles: ['rescue'] })
    jest.spyOn(document.documentElement, 'clientWidth', 'get').mockReturnValue(500)
    jest.spyOn(navigator, 'userAgent', 'get').mockReturnValue('Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1')

    let called = 0
    function show() {
      called++
    }

    const wrapper = shallow(<EnterpriseBorder showModal={show}><div>Test</div></EnterpriseBorder>)
    const border = wrapper.find('ContextConsumer').dive().
    find('EnterpriseBorder').dive()
    expect(called).toBe(1)
  })
})
