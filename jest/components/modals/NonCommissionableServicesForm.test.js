import React from 'react'
import { shallow } from 'enzyme'
import Api from 'api'
import CompanyStore from 'stores/company_store'
import Loading from 'componentLibrary/Loading'
import { Component as NonCommissionableServicesForm } from 'components/modals/NonCommissionableServicesForm'
import ServiceStore from 'stores/service_store'
import UserStore from 'stores/user_store'
import Utils from 'utils'

describe('NonCommissionableServicesForm modal component', () => {
  afterEach(() => {
    jest.restoreAllMocks()
    UserStore.setUser(null)
    CompanyStore.reset()
    ServiceStore.reset()
  })

  it('renders', () => {
    const company = {
      id: 1,
      service_ids: [1, 2, 3, 4, 5, 6],
      commission_exclusions_services_ids: [2, 4],
    }

    UserStore.setUser({ company })

    CompanyStore.loadData([company])

    const services = [
      { id: 1, name: 'Service1' },
      { id: 2, name: 'Service2' },
      { id: 3, name: 'Service3' },
      { id: 4, name: 'Service4' },
      { id: 5, name: 'Service5' },
      { id: 6, name: 'Service6' },
    ]

    ServiceStore.loadData(services)

    const hideModals = jest.fn()

    const wrapper = shallow(<NonCommissionableServicesForm hideModals={hideModals} />)

    expect(wrapper.containsMatchingElement(
      <p>
        Select the services that should not be included when calculating driver commissions
      </p>)
    ).toBe(true)

    const serviceGroups = Utils.splitIntoThree(services)

    expect(wrapper.containsMatchingElement(
      <div className="standard_obj_list">
        {serviceGroups.map((services, index) => (
          <div key={index}>
            {services.map(service => (
              <div key={service.id}>
                <input
                  checked={company.commission_exclusions_services_ids.includes(service.id)}
                  id={service.name}
                  type="checkbox"
                />
                <label htmlFor={service.name}>
                  {service.name}
                </label>
              </div>
            ))}
          </div>
        ))}
      </div>
    )).toBe(true)

    wrapper.find('#Service3').simulate('change', { target: { checked: true } })
    wrapper.find('#Service2').simulate('change', { target: { checked: false } })

    expect(wrapper.find('#Service3').prop('checked')).toBe(true)
    expect(wrapper.find('#Service2').prop('checked')).toBe(false)

    let callbacks
    jest.spyOn(Api, 'updateServiceCommissionsExclusions').
    mockImplementation(({ commission_exclusions: { change } }, { success, error }) => {
      expect(change).toEqual({
        add: [{ service_code_id: 3 }],
        remove: [{ service_code_id: 2 }],
      })

      callbacks = { success, error }
    })

    wrapper.instance().onConfirm()

    expect(wrapper.containsMatchingElement(<Loading />)).toBe(true)
    expect(wrapper.state('loading')).toBe(true)

    callbacks.success()
  })
})
