import React from 'react'
import { shallow } from 'enzyme'
import { Component as SetupCardModal } from 'components/modals/CreditCard/SetupCardModal'
import Context from 'Context'
import Tracking from 'tracking'

jest.mock('stores/modal_store', () => ({
  closeModals: jest.fn(),
}))

jest.mock('Context', () => ({
  navigate: jest.fn(),
}))

jest.mock('tracking', () => ({
  track: jest.fn(),
}))

describe('SetupCardModal Component', () => {
  let wrapper

  it('render method renders component with isAdminWithPayPermission prop passed in or not', () => {
    wrapper = shallow(<SetupCardModal isAdminWithPayPermission />)
    expect(wrapper).toBeDefined()
    wrapper = shallow(<SetupCardModal />)
    expect(wrapper).toBeDefined()
  })

  it('componentDidMount method call, calls sendTracking with Visit argument', () => {
    wrapper = shallow(<SetupCardModal isAdminWithPayPermission />)
    wrapper.instance().sendTracking = jest.fn()
    wrapper.instance().componentDidMount()
    expect(wrapper.instance().sendTracking).toHaveBeenCalledWith('Visit')
  })

  it('sendTracking method call, calls Tracking.track', () => {
    wrapper = shallow(<SetupCardModal isAdminWithPayPermission />)
    const trackingName = 'Click'
    wrapper.instance().sendTracking(trackingName)
    expect(Tracking.track).toHaveBeenCalledWith(`Payment Setup Prompt ${trackingName}`, {
      category: 'Payments',
    })
  })

  it('navigateToConfigure method call, calls Context.navigate, modalContext.hideModals, and sendTracking', () => {
    const hideModals = jest.fn()
    wrapper = shallow(<SetupCardModal hideModals={hideModals} isAdminWithPayPermission />)
    wrapper.instance().sendTracking = jest.fn()
    wrapper.instance().navigateToConfigure()
    expect(Context.navigate).toHaveBeenCalledWith('user/settings', { configuretab: null })
    expect(hideModals).toHaveBeenCalled()
    expect(wrapper.instance().sendTracking).toHaveBeenCalledWith('Click')
  })

  it('launchEmail method call, creates a link with mailto href, attaches it to document.body, calls it, and removes it from body, and calls sendTracking', () => {
    wrapper = shallow(<SetupCardModal isAdminWithPayPermission />)
    const mockLinkNode = {
      href: jest.fn(),
      click: jest.fn(),
    }
    global.document.createElement = jest.fn(() => mockLinkNode)
    global.document.body.appendChild = jest.fn()
    global.document.body.removeChild = jest.fn()
    wrapper.instance().sendTracking = jest.fn()
    wrapper.instance().launchEmail()
    expect(document.createElement).toHaveBeenCalledWith('a')
    expect(mockLinkNode.href).toContain('mailto')
    expect(mockLinkNode.href).toContain('subject')
    expect(mockLinkNode.href).toContain('body')
    expect(document.body.appendChild).toHaveBeenCalledWith(mockLinkNode)
    expect(document.body.removeChild).toHaveBeenCalledWith(mockLinkNode)
    expect(wrapper.instance().sendTracking).toHaveBeenCalledWith('Click')
  })
})
