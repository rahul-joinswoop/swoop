import React from 'react'
import { shallow } from 'enzyme'
import AllObjectsForm from 'components/modals/AllObjectsForm'

describe('AllObjectsForm base component', () => {
  it('renders', () => {
    const wrapper = shallow(<AllObjectsForm />)

    expect(wrapper.containsMatchingElement(
      <p>
        Select the Custom Object Name that should not be included when calculating driver commissions
      </p>)
    ).toBe(true)

    expect(wrapper.find('.standard_obj_list div').length).toBe(3)
  })
})
