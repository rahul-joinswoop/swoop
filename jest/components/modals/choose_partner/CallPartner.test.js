import React from 'react'
import render from 'test'
import CallPartner from 'components/modals/choose_partner_map/CallPartner'
import JobStore from 'stores/job_store'

jest.mock('stores/job_store')
jest.mock('stores/notification_store')
jest.mock('stores/auction_store')
jest.mock('lib/swoop/dispatcher')
jest.mock('tracking')
jest.mock('stores/modal_store')

const partner = {
  open: true,
  name: 'partner',
  phone: '2313357896',
}

describe('CallPartner Component', () => {
  it('Shows the job has already been assigned if the rescue_company is set on the job', () => {
    JobStore.get.mockReturnValueOnce(null)
    const a = render(<CallPartner dispatch_email="quinnbaetz@gmail.com" partner={partner} />)
    expect(a.querySelector('.CallPartner')).toBeTruthy()
    JobStore.get.mockReturnValueOnce({ id: 1, name: 'Set Rescue Company', rescue_company: true })
    const b = render(<CallPartner partner={partner} />)
    expect(b.querySelector('.CallPartner-jobAlreadyAssigned')).toBeTruthy()
  })
})
