import React from 'react'
import { shallow, render } from 'enzyme'
import 'loader' // delete this file when we have converted coffeescript to JS
import AcceptJob from 'components/modals/AcceptJob'
import AuctionStore from 'stores/auction_store'
import JobStore from 'stores/job_store'
import Dispatcher from 'lib/swoop/dispatcher'
import ModalStore from 'stores/modal_store'
import Tracking from 'tracking'
import NotificationStore from 'stores/notification_store'
import { SplitIOProvider } from 'components/split.io'

jest.mock('stores/job_store')
jest.mock('stores/notification_store')
jest.mock('stores/auction_store')
jest.mock('lib/swoop/dispatcher')
jest.mock('tracking')

describe('AcceptJob Component', () => {
  let wrapper

  const initialState = {
    eta: null,
    reason: 0,
    error: null,
  }

  const theId = 10
  let record = {
    liveAuction: false,
    id: theId,
  }

  it('renders even if empty job passed in', () => {
    JobStore.getById.mockReturnValue({})
    wrapper = render(
      <SplitIOProvider id="AcceptJob">
        <AcceptJob record={record} />
      </SplitIOProvider>
    )
    expect(wrapper).toBeDefined()
  })

  describe('constructor', () => {
    it('has initial state as defined', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      expect(wrapper.instance().state).toEqual(initialState)
    })
    it('calls NotificationStore.removeNotification if there is a record passed in with record id', () => {
      record = {
        id: 3,
      }
      wrapper = shallow(<AcceptJob record={record} />)
      expect(NotificationStore.removeNotification).toHaveBeenCalledWith(`Assigned Job #${record.id}`)
      expect(NotificationStore.removeNotification).toHaveBeenCalledWith(`#${record.id} is available for ETA submissions`)
    })
  })

  describe('componentDidMount', () => {
    it('calls AuctionStore.bind', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      expect(AuctionStore.bind).toHaveBeenCalled()
    })
  })

  describe('componentWillUnmount', () => {
    it('calls AuctionStore.unbind', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.unmount()
      expect(AuctionStore.unbind).toHaveBeenCalled()
    })
  })

  describe('goodEta function', () => {
    it('returns false and sets error to You must set an ETA, if eta in state is null,', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      expect(wrapper.instance().goodEta()).toEqual(false)
      expect(wrapper.instance().state.error).toEqual('You must set an ETA')
    })
    it('returns false and sets error to ETA must be greater than 0, if eta in state is 0,', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().state.eta = '0'
      expect(wrapper.instance().goodEta()).toEqual(false)
      expect(wrapper.instance().state.error).toEqual('ETA must be greater than 0')
    })
    it('returns true, if eta is set,', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().state.eta = '20'
      expect(wrapper.instance().goodEta()).toEqual(true)
      expect(wrapper.instance().state.error).toEqual(null)
    })
  })

  describe('sendTracking function', () => {
    let theLabel = 'test-label'
    it('is called with correct arguments and label if passed in', () => {
      record = {
        id: theId,
      }
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().sendTracking(theLabel)
      expect(Tracking.trackJobEvent).toHaveBeenCalledWith('Accept Job', theId, { label: theLabel })
    })
    it('is called with correct arguments when label not passed in', () => {
      theLabel = null
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().sendTracking(theLabel)
      expect(Tracking.trackJobEvent).toHaveBeenCalledWith('Accept Job', theId, { label: 'Standard' })
    })
    it('is called with correct arguments when label not passed in and job account is a motor club', () => {
      record.account = {
        name: 'Geico',
      }
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().sendTracking(theLabel)
      expect(Tracking.trackJobEvent).toHaveBeenCalledWith('Accept Job', theId, { label: 'Motor Club' })
    })
  })

  describe('handleEtaInputChange function', () => {
    it('is called with correct arguments when label not passed in and job account is a motor club', () => {
      const newEta = '5'
      const newEtaEvent = {
        target: {
          value: newEta,
        },
      }
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().handleEtaInputChange(newEtaEvent)
      expect(wrapper.instance().state.eta).toEqual(newEta)
      expect(wrapper.instance().state.error).toEqual(null)
    })
  })

  describe('handleConfirm function', () => {
    const onConfirm = jest.fn()
    const theEta = '20'
    it('sets state error_reason to null', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().handleConfirm()
      expect(wrapper.instance().state.error_reason).toEqual(null)
    })
    it('sets state error to null if eta is valid', () => {
      wrapper = shallow(<AcceptJob onConfirm={onConfirm} record={record} />)
      wrapper.instance().state.eta = '20'
      wrapper.instance().handleConfirm()
      expect(wrapper.instance().state.error).toEqual(null)
    })
    it('where there is an live auction and a valid eta, call sendTracking with Auction and call onConfirm with job_id and minutes ', () => {
      AuctionStore.jobHasLiveAuction.mockReturnValue(true)
      wrapper = shallow(<AcceptJob onConfirm={onConfirm} record={record} />)
      const instance = wrapper.instance()
      instance.state.eta = theEta
      instance.sendTracking = jest.fn()
      instance.handleConfirm()
      expect(instance.sendTracking).toHaveBeenCalledWith('Auction')
      expect(onConfirm).toHaveBeenCalledWith({
        job_id: theId,
        minutes: theEta,
      })
    })
    it('where there is no live auction and a scheduled job, call sendTracking with null and call onConfirm with status and id ', () => {
      AuctionStore.jobHasLiveAuction.mockReturnValue(false)
      record.scheduled_for = true
      wrapper = shallow(<AcceptJob onConfirm={onConfirm} record={record} />)
      const instance = wrapper.instance()
      instance.sendTracking = jest.fn()
      instance.handleConfirm()
      expect(instance.sendTracking).toHaveBeenCalledWith(null)
      expect(onConfirm).toHaveBeenCalledWith({
        id: theId,
        status: 'Accepted',
      }, true)
    })
    it('where there is no live auction and no scheduled job, but good eta, call sendTracking with null and call onConfirm with id and bta ', () => {
      AuctionStore.jobHasLiveAuction.mockReturnValue(false)
      record.scheduled_for = false
      wrapper = shallow(<AcceptJob onConfirm={onConfirm} record={record} />)
      const instance = wrapper.instance()
      instance.state.eta = theEta
      instance.sendTracking = jest.fn()
      instance.handleConfirm()
      expect(instance.sendTracking).toHaveBeenCalledWith(null)
      // this test is inconsistent as there is a time difference theEta and time run through node
      // expect(onConfirm).toHaveBeenCalledWith({
      //   id: theId,
      //   bta: moment().add(theEta, 'minutes').toISOString(),
      // }, true)
      expect(onConfirm).toHaveBeenCalled()
    })
    it('where there is no live auction and no scheduled job, and bad eta and reason id is 0, error reason is You must specify a reason ', () => {
      const longEta = '91'
      AuctionStore.jobHasLiveAuction.mockReturnValue(false)
      record.scheduled_for = false
      wrapper = shallow(<AcceptJob onConfirm={onConfirm} record={record} />)
      const instance = wrapper.instance()
      instance.state.eta = longEta
      instance.state.reason = 0
      instance.sendTracking = jest.fn()
      instance.handleConfirm()
      expect(instance.sendTracking).toHaveBeenCalledWith(null)
      expect(onConfirm).toHaveBeenCalled()
      expect(instance.state.error_reason).toEqual('You must specify a reason')
    })
  })

  describe('selectEtaReason function', () => {
    it('sets state reason based on event input', () => {
      const theReason = 'because'
      const theEvent = {
        target: {
          value: theReason,
        },
      }
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().selectEtaReason(theEvent)
      expect(wrapper.instance().state.reason).toEqual(theReason)
    })
  })

  describe('requestCallback function', () => {
    it('calls Dispatcher.send ModalStore.SHOW_MODAL with request_phone_call_back modal, if button passed in', () => {
      Dispatcher.send = jest.fn()
      wrapper = shallow(<AcceptJob record={record} />)
      wrapper.instance().requestCallback('key', 'button')
      expect(Dispatcher.send).toHaveBeenCalledWith(ModalStore.SHOW_MODAL, {
        key: 'request_phone_call_back',
        props: {
          record,
        },
      })
    })
    it('calls props.onCancel, if no button passed in', () => {
      const onCancel = jest.fn()
      wrapper = shallow(<AcceptJob onCancel={onCancel} record={record} />)
      wrapper.instance().requestCallback('key', null)
      expect(onCancel).toHaveBeenCalled()
    })
  })

  describe('showEtaReasons function', () => {
    it('returns true if eta in state is 90 or more', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      const instance = wrapper.instance()
      instance.state.eta = '91'
      expect(instance.showEtaReasons()).toEqual(true)
    })
    it('returns true if eta in state is less than 90 but eta is greater than max eta and client is Gieco Agero or Quest', () => {
      record.issc_dispatch_request = {
        max_eta: 60,
        client_id: 'AGO',
      }
      wrapper = shallow(<AcceptJob record={record} />)
      const instance = wrapper.instance()
      instance.state.eta = '70'
      expect(instance.showEtaReasons()).toEqual(true)
    })
    it('returns false if eta in state is less than 90 and eta is greater than max eta and client is not Gieco Agero or Quest', () => {
      record.issc_dispatch_request = {
        max_eta: 60,
        client_id: 'Enterprise',
      }
      wrapper = shallow(<AcceptJob record={record} />)
      const instance = wrapper.instance()
      instance.state.eta = '70'
      expect(instance.showEtaReasons()).toEqual(false)
    })
    it('returns false if eta in state is less than 90 but no max eta', () => {
      record.issc_dispatch_request = {
        max_eta: null,
      }
      wrapper = shallow(<AcceptJob record={record} />)
      const instance = wrapper.instance()
      instance.state.eta = '70'
      expect(instance.showEtaReasons()).toBeFalsy()
    })
  })

  describe('enableSend function', () => {
    it('returns true, if no job passed in', () => {
      wrapper = shallow(<AcceptJob record={record} />)
      expect(wrapper.instance().enableSend()).toEqual(true)
    })
    it('returns true, if job status is auto assigning and the job has a live auction', () => {
      AuctionStore.jobHasLiveAuction.mockReturnValue(true)
      wrapper = shallow(<AcceptJob record={record} />)
      expect(wrapper.instance().enableSend({ status: 'Auto Assigning' })).toEqual(true)
    })
    it('returns false, if job status is auto assigning and the job does not have a live auction', () => {
      AuctionStore.jobHasLiveAuction.mockReturnValue(false)
      wrapper = shallow(<AcceptJob record={record} />)
      expect(wrapper.instance().enableSend({ status: 'Auto Assigning' })).toEqual(false)
    })
  })

  describe('getCancelButton function', () => {
    it('returns null if there is a job auction', () => {
      AuctionStore.jobHasLiveAuction.mockReturnValue(true)
      wrapper = shallow(<AcceptJob record={record} />)
      expect(wrapper.instance().getCancelButton()).toEqual(null)
    })
    it('returns cancel if there is a job auction', () => {
      AuctionStore.jobHasLiveAuction.mockReturnValue(false)
      wrapper = shallow(<AcceptJob record={record} />)
      expect(wrapper.instance().getCancelButton()).toEqual('Cancel')
    })
    it('returns REquest Call Back if there is a job auction and owner company is a motorclub', () => {
      record.owner_company = {
        name: 'Geico',
      }
      AuctionStore.jobHasLiveAuction.mockReturnValue(false)
      wrapper = shallow(<AcceptJob record={record} />)
      expect(wrapper.instance().getCancelButton()).toEqual('Request Call Back')
    })
  })
})
