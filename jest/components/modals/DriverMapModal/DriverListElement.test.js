import React from 'react'
import { shallow, mount } from 'enzyme'
import DriverListElement from 'components/modals/DriverMapModal/DriverListElement'
import VehicleStore from 'stores/vehicle_store'
import UsersStore from 'stores/users_store'

jest.mock('stores/vehicle_store')
jest.mock('stores/users_store')

describe('DriverListElement Component', () => {
  let wrapper

  const defaultSelectedDriverId = 1
  const initialProps = {
    isAcceptModal: true,
    driver: {
      id: 1,
      full_name: 'KonPartner',
      on_duty: true,
      jobs: [],
    },
    getId: jest.fn(),
    onTruckClick: jest.fn(),
    listItemIndex: 1,
    selectedDriverId: 1,
    setSelectedDriverId: jest.fn(),
    setVehicles: jest.fn(),
    vehicles: [
      {
        id: 1,
        name: 'Crusher',
        location_updated_at: '2018-05-25 15:24:44.22136',
      },
      {
        id: 2,
        name: 'Destroyer',
        location_updated_at: '2018-05-25 15:24:44.22136',
      },
    ],
    savedVehicles: [1, 2, 3],
  }

  describe('render list item', () => {
    it('renders when props passed in', () => {
      wrapper = shallow(<DriverListElement {...initialProps} />)
      expect(wrapper).toBeDefined()
    })
    it('calls VehicleStore.getVehicleIdByDriverId to get vehicle_id when no savedVehicles', () => {
      VehicleStore.getVehicleIdByDriverId = jest.fn()
      initialProps.savedVehicles = null
      wrapper = shallow(<DriverListElement {...initialProps} />)
      expect(VehicleStore.getVehicleIdByDriverId).toHaveBeenCalled()
    })
    it('calls VehicleStore.getVehicleIdByDriverId to get vehicle_id when savedVehicles exists but driver id not in savedVehicles', () => {
      VehicleStore.getVehicleIdByDriverId = jest.fn()
      initialProps.savedVehicles = [1, 2, 3]
      initialProps.driver.id = 4
      wrapper = shallow(<DriverListElement {...initialProps} />)
      expect(VehicleStore.getVehicleIdByDriverId).toHaveBeenCalled()
    })
    it('does not call VehicleStore.getVehicleIdByDriverId to get vehicle_id when driver id is in savedVehicles', () => {
      VehicleStore.getVehicleIdByDriverId = jest.fn()
      initialProps.savedVehicles = [1, 2, 3]
      initialProps.driver.id = defaultSelectedDriverId
      wrapper = shallow(<DriverListElement {...initialProps} />)
      expect(VehicleStore.getVehicleIdByDriverId).not.toHaveBeenCalled()
    })
    it('clicking on list element calls setSelectedDriverId with the driver_id (i.e. click selects the driver)', () => {
      wrapper = shallow(<DriverListElement {...initialProps} />)
      wrapper.find('.driver-list-element').simulate('click')
      expect(initialProps.setSelectedDriverId).toHaveBeenCalledWith(defaultSelectedDriverId)
    })
    it('clicking on list element calls onTruckClick to center vehicle on map, if onTruckClick and vehicleId are present', () => {
      initialProps.onTruckClick = jest.fn()
      wrapper = shallow(<DriverListElement {...initialProps} />)
      wrapper.find('.driver-list-element').simulate('click')
      expect(initialProps.onTruckClick).toHaveBeenCalled()
    })
    it('clicking on toggle truck, calls UsersStore.updateItem with driver id and on_duty', () => {
      UsersStore.updateItem = jest.fn()
      wrapper = mount(<DriverListElement {...initialProps} />)
      wrapper.find('.truck-toggle').childAt(0).instance().props.onToggle()
      expect(UsersStore.updateItem).toHaveBeenCalledWith(
        {
          id: initialProps.driver.id,
          on_duty: !initialProps.driver.on_duty,
        }
      )
    })
    it('truck select dropdown works', () => {
      const targetValue = 1
      const testEvent = {
        target: {
          value: targetValue,
        },
      }
      wrapper = mount(<DriverListElement {...initialProps} />)
      wrapper.find('.select-truck').props().onChange(testEvent)
      expect(initialProps.setVehicles).toHaveBeenCalledWith([initialProps.vehicles[0], targetValue])
    })
    it('truck options from VehicleStore.getSortedVehicles are rendered and display truck name', () => {
      const testDrivers = [
        {
          name: 'John Doe',
          id: 1,
        },
        {
          name: 'Bob Smith',
          id: 2,
        },
      ]
      VehicleStore.getSortedVehicles = jest.fn(() => testDrivers)
      wrapper = mount(<DriverListElement {...initialProps} />)
      wrapper.find('.option_row').forEach((node, index) => {
        expect(node.text()).toEqual(testDrivers[index].name)
      })
      expect(wrapper.find('.option_row')).toHaveLength(testDrivers.length)
    })
  })
})
