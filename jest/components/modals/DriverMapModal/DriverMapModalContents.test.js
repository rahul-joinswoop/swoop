import React from 'react'
import { shallow as baseShallow } from 'enzyme'
import VehicleStore from 'stores/vehicle_store'
import ModalContents from 'components/modals/DriverMapModal/DriverMapModalContents'
import { SplitIOContext } from 'components/split.io'

const DriverMapModalContents = (props) =>
  <SplitIOContext.Provider value={{ treatments: {} }}>
    <ModalContents {...props} />
  </SplitIOContext.Provider>

function shallow(component) {
  let result = baseShallow(component)
  while (!(result.instance() instanceof ModalContents)) {
    result = result.dive()
  }
  return result.dive().dive()
}

jest.mock('stores/vehicle_store')

describe('DriverMapModalContents Component', () => {
  let wrapper

  const initialProps = {
    assignDriverState: {
      showMap: jest.fn(),
      search: 'KonPartner',
    },
    assignDriverMethods: {
      getSortedDrivers: jest.fn(() => []),
      setSelectedDriverId: jest.fn(),
      setSearch: jest.fn(),
      setShowOffDuty: jest.fn(),
      setVehicles: jest.fn(),
    },
    getId: jest.fn(),
    record: {
      id: 1,
    },
  }

  describe('getVehicleDriverId function', () => {
    VehicleStore.get = jest.fn()
    const testVehicleId = 15
    it('calls VehicleStore.get with vehicle_id passed in', () => {
      wrapper = shallow(<DriverMapModalContents {...initialProps} />)
      wrapper.instance().getVehicleDriverId(testVehicleId)
      expect(VehicleStore.get).toHaveBeenCalledWith(testVehicleId, 'driver_id', initialProps.getId())
    })
  })

  describe('render', () => {
    VehicleStore.getSortedDriverVehicles = jest.fn()
    it('calls VehicleStore.getSortedDriverVehicles', () => {
      wrapper = shallow(<DriverMapModalContents {...initialProps} />)
      expect(VehicleStore.getSortedDriverVehicles).toHaveBeenCalledWith(initialProps.get)
    })

    describe('Swoop Map', () => {
      it('handleTruckClick calls getVehicleDriverId with truck id', () => {
        const testDriverId = 1
        const { setSelectedDriverId } = initialProps.assignDriverMethods
        wrapper = shallow(<DriverMapModalContents {...initialProps} />)
        const instance = wrapper.instance()
        instance.getVehicleDriverId = jest.fn(id => id)
        instance.handleTruckClick(testDriverId, setSelectedDriverId)
        expect(instance.getVehicleDriverId).toHaveBeenCalledWith(testDriverId)
        expect(setSelectedDriverId).toHaveBeenCalledWith(testDriverId)
      })
    })
  })
})
