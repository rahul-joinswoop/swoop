import React from 'react'
import { shallow } from 'enzyme'
import DriverSearch from 'components/modals/DriverMapModal/DriverSearch'

describe('DriverSearch Component', () => {
  let wrapper

  const initialProps = {
    isAcceptModal: false,
    search: 'KonPartner',
    setSearch: jest.fn(),
  }

  it('render renders component', () => {
    wrapper = shallow(<DriverSearch {...initialProps} />)
    expect(wrapper).toBeDefined()
  })
  it('input onchange calls setSearch with input text', () => {
    const testEvent = {
      target: {
        value: 'Kon',
      },
    }
    wrapper = shallow(<DriverSearch {...initialProps} />)
    wrapper.find('input').props().onChange(testEvent)
    expect(initialProps.setSearch).toHaveBeenCalledWith(testEvent.target.value)
  })
  it('handleClearInputClick clears the search and sets focus on the input box', () => {
    wrapper = shallow(<DriverSearch {...initialProps} />)
    wrapper.instance().refs = {
      searchInput: {
        focus: jest.fn(),
      },
    }
    wrapper.instance().handleClearInputClick()
    expect(initialProps.setSearch).toHaveBeenCalledWith('')
    expect(wrapper.instance().refs.searchInput.focus).toHaveBeenCalled()
  })
})
