import React from 'react'
import { shallow } from 'enzyme'
import DriverList from 'components/modals/DriverMapModal/DriverList'

describe('DriverList Component', () => {
  let wrapper

  const testName = 'John Doe'
  const testName2 = 'Bob Smith'
  const testName3 = 'The Dude'
  const testSortedDrivers = [
    {
      full_name: testName,
      id: 1,
      jobs: [],
      on_duty: true,
    },
    {
      full_name: testName2,
      id: 2,
      jobs: [],
      on_duty: true,
    },
    {
      full_name: testName3,
      id: 3,
      jobs: [],
      on_duty: true,
    },
  ]

  const initialProps = {
    assignDriverMethods: {
      getSortedDrivers: jest.fn(() => testSortedDrivers),
      setSelectedDriverId: jest.fn(),
      setSearch: jest.fn(),
      setShowOffDuty: jest.fn(),
      setVehicles: jest.fn(),
    },
    getId: jest.fn(),
    isAcceptModal: true,
    mapRef: React.createRef(),
    search: 'KonPartner',
    selectedDriverId: 1,
    showOffDuty: true,
    vehicles: [1, 2, 3],
    savedVehicles: [1, 2, 3],
  }

  it('renders when props passed in', () => {
    wrapper = shallow(<DriverList {...initialProps} />)
    expect(wrapper).toBeDefined()
  })

  it('calls getSortedDrivers and renders DriverListElement for each driver', () => {
    wrapper = shallow(<DriverList {...initialProps} />)
    expect(initialProps.assignDriverMethods.getSortedDrivers).toHaveBeenCalled()
    expect(wrapper.find('ul').children()).to.have.lengthOf(testSortedDrivers.length + 1)
  })

  it('click on off duty button calls setShowOffDuty with negation of showOffDuty', () => {
    wrapper = shallow(<DriverList {...initialProps} />)
    wrapper.find('.off-duty-button').simulate('click')
    expect(initialProps.assignDriverMethods.setShowOffDuty).
    toHaveBeenCalledWith(!initialProps.showOffDuty)
  })
})
