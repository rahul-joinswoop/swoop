import React from 'react'
import { shallow, mount } from 'enzyme'
import ETAInputContainer from 'components/modals/DriverMapModal/ETAInputContainer'
import EtaExplanationStore from 'stores/eta_explanation_store'
import JobStore from 'stores/job_store'
import AuctionStore from 'stores/auction_store'

jest.mock('stores/eta_explanation_store')
jest.mock('stores/auction_store')
jest.mock('stores/job_store')

describe('ETAInputContainer Component', () => {
  let wrapper

  const answerByEta = '40minutes'

  const initialProps = {
    acceptJobMethods: {
      handleEtaInputChange: jest.fn(),
      selectEtaReason: jest.fn(),
      showEtaReasons: jest.fn(() => true),
    },
    acceptJobState: {
      eta: null,
      reason: 0,
      error: null,
      job: {
        id: 1,
        status: 'TestStatus',
        answer_by: answerByEta,
      },
    },
    getId: jest.fn(),
    record: {
      id: 1,
      issc_dispatch_request: {
        client_id: 'Test',
        system: 'rsc',
      },
    },
  }

  it('render renders component with initial props', () => {
    wrapper = shallow(<ETAInputContainer {...initialProps} />)
    expect(wrapper).toBeDefined()
  })

  describe('"Exact location available once ETA accepted" label is', () => {
    AuctionStore.jobHasAuction = jest.fn(() => true)
    it('visible if there is a live auction and status is auto assigning', () => {
      initialProps.record.issc_dispatch_request.system = 'whatsystem'
      initialProps.acceptJobState.job.status = 'Auto Assigning'
      wrapper = mount(<ETAInputContainer {...initialProps} />)
      expect(wrapper.find('.no-job-eta-message').exists()).to.equal(true)
    })
    it('invisible if there is no live auction', () => {
      initialProps.record.issc_dispatch_request.system = 'rsc'
      AuctionStore.jobHasAuction = jest.fn(() => false)
      wrapper = shallow(<ETAInputContainer {...initialProps} />)
      expect(wrapper.find('.no-job-eta-message').exists()).to.equal(false)
    })
  })

  describe('scheduled job', () => {
    it('information is not rendered if job is not a scheduled one', () => {
      initialProps.record.scheduled_for = true
      wrapper = mount(<ETAInputContainer {...initialProps} />)
      expect(wrapper.find('.scheduled-time').exists()).to.equal(true)
    })
    it('information is rendered if job is a scheduled one', () => {
      initialProps.record.scheduled_for = false
      wrapper = mount(<ETAInputContainer {...initialProps} />)
      expect(wrapper.find('.scheduled-time').exists()).to.equal(false)
    })
  })

  describe('eta explanation options', () => {
    const explanationOptions = [
      {
        text: 'Weather',
      },
      {
        text: 'Explosion',
      },
    ]

    EtaExplanationStore.getAll = jest.fn(() => explanationOptions)

    it('should see all eta explanation options for clients/companies that do not have special conditions', () => {
      wrapper = mount(<ETAInputContainer {...initialProps} />)
      // length is 3 because first option is '- Select -'
      expect(wrapper.find('option')).to.have.lengthOf(3)
      expect(wrapper.find('option').get(1).props.children).toEqual(explanationOptions[0].text)
      expect(wrapper.find('option').get(2).props.children).toEqual(explanationOptions[1].text)
    })
    it('should see limited eta explanation options for clients/companies that have special conditions', () => {
      initialProps.record.issc_dispatch_request.client_id = 'AGO'
      wrapper = mount(<ETAInputContainer {...initialProps} />)
      expect(wrapper.find('option')).to.have.lengthOf(2)
    })
  })

  describe('timer', () => {
    const auctionExpiration = '30minutes'
    it('no timer if no live auction and not a motor club job live', () => {
      AuctionStore.jobHasLiveAuction = jest.fn(() => false)
      wrapper = mount(<ETAInputContainer {...initialProps} />)
      expect(wrapper.find('.accept-job-timer').exists()).toEqual(false)
    })
    it('expires at calls AuctionStore.getExpiresAtByJob for expiration time if there is a live auction', () => {
      AuctionStore.jobHasLiveAuction = jest.fn(() => true)
      AuctionStore.getExpiresAtByJob = jest.fn(() => auctionExpiration)
      wrapper = mount(<ETAInputContainer {...initialProps} />)
      expect(AuctionStore.getExpiresAtByJob).toHaveBeenCalledWith(initialProps.acceptJobState.job)
      expect(wrapper.find('.accept-job-timer').exists()).toEqual(true)
      expect(wrapper.find('.accept-job-timer').get(0).props.expiresAt).toEqual(auctionExpiration)
    })
    it('expires at calls JobStore.isMotorClub if there is no live auction', () => {
      AuctionStore.jobHasLiveAuction = jest.fn(() => false)
      JobStore.isMotorClub = jest.fn(() => true)
      wrapper = mount(<ETAInputContainer {...initialProps} />)
      expect(wrapper.find('.accept-job-timer').exists()).toEqual(true)
      expect(wrapper.find('.accept-job-timer').get(0).props.expiresAt).toEqual(answerByEta)
    })
  })
})
