import React from 'react'
import { shallow, render } from 'enzyme'
import AssignDriver from 'components/modals/AssignDriver'
import 'loader' // delete this file when we have converted coffeescript to JS
import UsersStore from 'stores/users_store'
import JobStore from 'stores/job_store'
import VehicleStore from 'stores/vehicle_store'
import Logger from 'lib/swoop/logger'
import Tracking from 'tracking'
import { map } from 'lodash'

jest.mock('stores/users_store')

describe('AssignDriver Component', () => {
  let wrapper

  const recordId = 1
  const recordRescueDriverId = 10
  const record = {
    id: recordId,
    rescue_driver_id: recordRescueDriverId,
  }

  const initialState = {
    confirmError: null,
    selectedDriverId: record.rescue_driver_id,
    showMap: false,
    search: '',
    showOffDuty: false,
    savedVehicles: {},
  }

  const returnedDriversIds = [1, 2, 3]
  beforeEach(() => {
    jest.resetAllMocks()
    UsersStore.getDriverIds.mockReturnValue(returnedDriversIds)
  })

  it('renders even if no driver ids are present', () => {
    wrapper = render(<AssignDriver record={record} />)
    expect(wrapper).toBeDefined()
  })

  describe('constructor', () => {
    it('has initial state as defined', () => {
      wrapper = shallow(<AssignDriver record={record} />)
      expect(wrapper.instance().state).toEqual(initialState)
    })
    it('calls VehicleStore._loadVehiclesWithDriverListIds()', () => {
      VehicleStore._loadVehiclesWithDriverListIds = jest.fn()
      wrapper = shallow(<AssignDriver record={record} />)
      expect(VehicleStore._loadVehiclesWithDriverListIds).toHaveBeenCalled()
    })
    it('calls setTimeout in 500ms to set state showMap to true', () => {
      jest.useFakeTimers()
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().setState = jest.fn()
      expect(wrapper.instance().state.showMap).toEqual(false)
      expect(setTimeout).toHaveBeenCalledTimes(1)
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500)
    })
  })
  describe('handleConfirm', () => {
    global.Tracking = Tracking
    Tracking.trackJobEvent = jest.fn()
    const onConfirm = jest.fn()
    const testSelectedDriverId = 3
    it('returns nothing if selectedDriverId is falsy', () => {
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().state.selectedDriverId = null
      expect(wrapper.instance().handleConfirm()).toBeFalsy()
    })

    it('calls props.onConfirm with record prop id and rescue_driver_id,', () => {
      wrapper = shallow(<AssignDriver onConfirm={onConfirm} record={record} />)
      wrapper.instance().handleConfirm()
      expect(onConfirm).toHaveBeenCalledWith({
        id: recordId,
        rescue_driver_id: recordRescueDriverId,
      })
    })

    it('calls Tracking.trackJobEvent with record prop id and rescue_driver_id,', () => {
      Tracking.trackJobEvent = jest.fn()
      wrapper = shallow(<AssignDriver onConfirm={onConfirm} record={record} />)
      wrapper.instance().handleConfirm()
      expect(Tracking.trackJobEvent).toHaveBeenCalledWith(
        'Dispatch to Driver',
        record.id,
        { category: 'Dispatch' }
      )
    })

    it('calls props.onConfirm with record prop id and updated rescue_driver_id if selectedDriverId set in state,', () => {
      wrapper = shallow(<AssignDriver onConfirm={onConfirm} record={record} />)
      wrapper.instance().state.selectedDriverId = testSelectedDriverId
      wrapper.instance().handleConfirm()
      expect(onConfirm).toHaveBeenCalledWith({
        id: recordId,
        rescue_driver_id: testSelectedDriverId,
      })
    })

    it('calls props.onConfirm with rescue_vehicle_id from savedVehicles object it has key with selectedDriverId,', () => {
      const vehicleIdForSelectedDriverId = 33
      wrapper = shallow(<AssignDriver onConfirm={onConfirm} record={record} />)
      wrapper.instance().state.selectedDriverId = testSelectedDriverId
      wrapper.instance().state.savedVehicles = {
        [testSelectedDriverId]: vehicleIdForSelectedDriverId,
      }
      wrapper.instance().handleConfirm()
      expect(onConfirm).toHaveBeenCalledWith({
        id: recordId,
        rescue_driver_id: testSelectedDriverId,
        rescue_vehicle_id: vehicleIdForSelectedDriverId,
      })
    })
    it('calls Logger.debug if record has a rescue_vehicle_id,', () => {
      Logger.debug = jest.fn()
      record.rescue_vehicle_id = 15
      wrapper = shallow(<AssignDriver onConfirm={onConfirm} record={record} />)
      wrapper.instance().state.vehicles = null
      wrapper.instance().state.selectedDriverId = testSelectedDriverId
      wrapper.instance().handleConfirm()
      expect(Logger.debug).toHaveBeenCalledWith('Rescue Vehicle already set, so not defaulting')
    })
    it('assigns VehicleStore.getVehicleIdByDriverId(selectedDriverId) to rescue_vehicle_id when no vehicles and rescue_vehicle_id,', () => {
      record.rescue_vehicle_id = null
      const getVehicleIdByDriverIdReturn = 4
      VehicleStore.getVehicleIdByDriverId = jest.fn((id) => {
        if (id === testSelectedDriverId) {
          return getVehicleIdByDriverIdReturn
        }
        return null
      })
      wrapper = shallow(<AssignDriver onConfirm={onConfirm} record={record} />)
      wrapper.instance().state.vehicles = null
      wrapper.instance().state.selectedDriverId = testSelectedDriverId
      wrapper.instance().handleConfirm()
      expect(onConfirm).toHaveBeenCalledWith({
        id: recordId,
        rescue_driver_id: testSelectedDriverId,
        rescue_vehicle_id: getVehicleIdByDriverIdReturn,
      })
    })
  })
  describe('getDriverJobs', () => {
    const testId = 6
    it('returns job that has matching rescue_driver_id and dispatched and assigned status', () => {
      const matchingJob = {
        rescue_driver_id: testId,
        status: 'Assigned',
      }
      JobStore.getJobs = jest.fn(() => ([
        {
          rescue_driver_id: 3321,
          status: 'Bad status! Bad!',
        },
        matchingJob,
      ]))
      wrapper = shallow(<AssignDriver record={record} />)
      expect(wrapper.instance().getDriverJobs(testId)[0]).toEqual(matchingJob)
    })
    it('returns empty array where no matching job', () => {
      JobStore.getJobs = jest.fn(() => ([
        {
          rescue_driver_id: 3321,
          status: 'Bad status! Bad!',
        },
      ]))
      wrapper = shallow(<AssignDriver record={record} />)
      expect(wrapper.instance().getDriverJobs(testId)).toEqual([])
    })
  })
  describe('getDrivers', () => {
    it('calls UsersStore.getDriverIds and returns its return, if no search and showOffDuty is true', () => {
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().state.search = null
      wrapper.instance().state.showOffDuty = true
      expect(wrapper.instance().getDrivers()).toEqual(returnedDriversIds)
      expect(UsersStore.getDriverIds).toHaveBeenCalled()
    })
    it('calls UsersStore.getDriverIds and UsersStore.get and returns drivers filtered by name and offDuty', () => {
      const testName = 'KonPartner'
      UsersStore.get = jest.fn(() => testName)
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().state.search = testName
      wrapper.instance().state.showOffDuty = true
      expect(wrapper.instance().getDrivers()).toEqual(returnedDriversIds)
      expect(UsersStore.get).toHaveBeenCalled()
      expect(UsersStore.getDriverIds).toHaveBeenCalled()
    })
    it('calls UsersStore.getDriverIds and UsersStore.get and returns drivers filtered by name and offDuty, including filtering out onDuty drivers that dont match', () => {
      const testName = 'KonPartner'
      const returnedDriversIdsWithOneFilteredOut = [2, 3]
      UsersStore.get = jest.fn((driver_id, onDutyStatus) => {
        if (driver_id === 1 && onDutyStatus === 'on_duty') {
          return false
        }
        return testName
      })
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().state.search = testName
      wrapper.instance().state.showOffDuty = false
      expect(wrapper.instance().getDrivers()).toEqual(returnedDriversIdsWithOneFilteredOut)
      expect(UsersStore.get).toHaveBeenCalled()
      expect(UsersStore.getDriverIds).toHaveBeenCalled()
    })
  })
  describe('getSortedDrivers', () => {
    it('calls getDrivers and returns sorted drivers', () => {
      const testName = 'KonPartner'
      const returnedSortedDrivers = map(returnedDriversIds, id => ({
        full_name: testName,
        id,
        jobs: [],
        on_duty: testName,
      }))
      UsersStore.get = jest.fn(() => testName)
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().state.search = testName
      wrapper.instance().state.showOffDuty = true
      expect(wrapper.instance().getSortedDrivers()).toEqual(returnedSortedDrivers)
      expect(UsersStore.get).toHaveBeenCalled()
      expect(UsersStore.getDriverIds).toHaveBeenCalled()
    })
  })
  describe('setSelectedDriverId', () => {
    const newSelectedDriverId = 4
    it('set selectedDriverId in the state', () => {
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().setSelectedDriverId(newSelectedDriverId)
      expect(wrapper.instance().state.selectedDriverId).toEqual(newSelectedDriverId)
    })
  })
  describe('setSearch', () => {
    const newSearchInput = 'blahblah'
    it('set searchInput in the state', () => {
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().setSearch(newSearchInput)
      expect(wrapper.instance().state.search).toEqual(newSearchInput)
    })
  })
  describe('setShowOffDuty', () => {
    const newShowOffDuty = true
    it('set showOffDuty in the state', () => {
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().setShowOffDuty(newShowOffDuty)
      expect(wrapper.instance().state.showOffDuty).toEqual(newShowOffDuty)
    })
  })
  describe('setVehicles', () => {
    const newVehicles = [5, 6, 7]
    it('set selectedDriverId in the state', () => {
      wrapper = shallow(<AssignDriver record={record} />)
      wrapper.instance().setVehicles(newVehicles)
      expect(wrapper.instance().state.savedVehicles).toEqual(newVehicles)
    })
  })
})
