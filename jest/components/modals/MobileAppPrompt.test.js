import React from 'react'
import render, { contains, simulate } from 'test'
import MobileAppPrompt from 'components/modals/MobileAppPrompt'
import 'loader' // delete this file when we have converted coffeescript to JS
import 'bootstrap-sass'

describe('MobileAppPrompt component', () => {
  afterEach(() => {
    jest.restoreAllMocks()
  })

  it('opens correct app url', () => {
    const windowOpen = jest.spyOn(window, 'open').mockImplementation()

    function hide() {

    }

    const el = render(<MobileAppPrompt hideModals={hide} />)

    document.body.appendChild(el)

    expect(contains(<h4>Get the Swoop Towing App</h4>, el)).toBe(true)

    el.querySelector('.primary').click()
    expect(windowOpen).toHaveBeenCalledWith('https://play.google.com/store/apps/details?id=com.swoopmobile')

    windowOpen.mockClear()
    jest.spyOn(navigator, 'userAgent', 'get').mockReturnValue('Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1')

    el.querySelector('.primary').click()
    expect(windowOpen).toHaveBeenCalledWith('https://appsto.re/us/meEoeb.i')

    document.body.removeChild(el)
  })

  it('renders feedback', () => {
    const el = render(<MobileAppPrompt />)
    document.body.appendChild(el)

    el.querySelector('.cancel').click()
    expect(contains(<h4>We Appreciate Your Feedback</h4>, el)).toBe(true)

    const textarea = el.querySelector('textarea')
    expect(textarea).toBeTruthy()

    simulate('change', { target: { value: 'Feedback Content' } }, textarea)
    expect(el.querySelector('a.component-button').getAttribute('href')).toBe('mailto:feedback@joinswoop.com?subject=Mobile Feedback&body=Feedback Content')
    // el.querySelector('.primary').click()
    // el.querySelector('.cancel').click()

    document.body.removeChild(el)
  })
})
