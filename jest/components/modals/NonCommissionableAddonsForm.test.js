import React from 'react'
import { shallow } from 'enzyme'
import Api from 'api'
import CompanyStore from 'stores/company_store'
import Loading from 'componentLibrary/Loading'
import { Component as NonCommissionableAddonsForm } from 'components/modals/NonCommissionableAddonsForm'
import ServiceStore from 'stores/service_store'
import UserStore from 'stores/user_store'
import Utils from 'utils'

describe('NonCommissionableAddonsForm modal component', () => {
  afterEach(() => {
    jest.restoreAllMocks()
    UserStore.setUser(null)
    CompanyStore.reset()
    ServiceStore.reset()
  })

  it('renders', () => {
    const company = {
      id: 1,
      addon_ids: [1, 2, 3, 4, 5, 6],
      commission_exclusions_addons_ids: [2, 4],
    }

    UserStore.setUser({ company })

    CompanyStore.loadData([company])

    const addons = [
      { id: 1, name: 'Addon1' },
      { id: 2, name: 'Addon2' },
      { id: 3, name: 'Addon3' },
      { id: 4, name: 'Addon4' },
      { id: 5, name: 'Addon5' },
      { id: 6, name: 'Addon6' },
    ]

    ServiceStore.loadData(addons)

    const hideModals = jest.fn()

    const wrapper = shallow(<NonCommissionableAddonsForm hideModals={hideModals} />)

    expect(wrapper.containsMatchingElement(
      <p>
        Select the items that should not be included when calculating driver commissions
      </p>)
    ).toBe(true)

    const addonGroups = Utils.splitIntoThree(addons)

    expect(wrapper.containsMatchingElement(
      <div className="standard_obj_list">
        {addonGroups.map((addons, index) => (
          <div key={index}>
            {addons.map(addon => (
              <div key={addon.id}>
                <input
                  checked={company.commission_exclusions_addons_ids.includes(addon.id)}
                  id={addon.name}
                  type="checkbox"
                />
                <label htmlFor={addon.name}>
                  {addon.name}
                </label>
              </div>
            ))}
          </div>
        ))}
      </div>
    )).toBe(true)

    wrapper.find('#Addon3').simulate('change', { target: { checked: true } })
    wrapper.find('#Addon2').simulate('change', { target: { checked: false } })

    expect(wrapper.find('#Addon3').prop('checked')).toBe(true)
    expect(wrapper.find('#Addon2').prop('checked')).toBe(false)

    let callbacks
    jest.spyOn(Api, 'updateAddonCommissionsExclusions').
    mockImplementation(({ commission_exclusions: { change } }, { success, error }) => {
      expect(change).toEqual({
        add: [{ service_code_id: 3 }],
        remove: [{ service_code_id: 2 }],
      })

      callbacks = { success, error }
    })

    wrapper.instance().onConfirm()

    expect(wrapper.containsMatchingElement(<Loading />)).toBe(true)
    expect(wrapper.state('loading')).toBe(true)

    callbacks.success()
  })
})
