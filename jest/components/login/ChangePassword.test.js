import React from 'react'
import { shallow } from 'enzyme'
import Api from 'api'
import ChangePassword from 'components/login/change_password'
import 'loader' // delete this file when we have converted coffeescript to JS

describe('ChangePassword Component', () => {
  afterEach(() => {
    jest.restoreAllMocks()

    // reset jsdom url
    window.history.pushState({}, '', '/')
  })

  it('tests https://swoopme.atlassian.net/browse/ENG-10280', () => {
    jest.useFakeTimers()

    jest.spyOn(Api, 'validatePasswordRequest').
    mockImplementation((uuid, { error }) => error({}, 'error'))

    window.history.pushState({}, '', '/user/reset?uuid=4347d260-ba54-4064-b78d-f5a48b51915d')
    shallow(<ChangePassword />)

    jest.runOnlyPendingTimers()
  })
})
