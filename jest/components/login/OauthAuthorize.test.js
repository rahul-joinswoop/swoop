import React from 'react'
import { mount } from 'enzyme'
import createRouter from 'router5'
import { RouterProvider } from 'react-router5'
import OauthAuthorizeComponent from 'components/login/OauthAuthorize'
import 'loader' // delete this file when we have converted coffeescript to JS

const OauthAuthorize = props =>
  <RouterProvider router={createRouter()}>
    <OauthAuthorizeComponent {...props} />
  </RouterProvider>

describe('OauthAuthorize Component', () => {
  it('renders without errors', () => {
    window.gon = {
      app: { name: 'Test App' },
    }

    const wrapper = mount(<OauthAuthorize />)
    const userMock = wrapper.find('UserMock')/* .shallow() */

    expect(userMock.exists('Button[label="Connect"]')).toBe(true)

    const fields = userMock.find('Fields')/* .shallow() */
    expect(fields.containsMatchingElement([
      <div>
        <label>Username or Email:</label>
        <input id="user_username_or_email" required />
      </div>,
      <div>
        <label>Password:</label>
        <input id="user_password" required type="password" />
      </div>,
    ])).toBe(true)
  })
})
