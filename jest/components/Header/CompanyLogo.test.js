import React from 'react'
import CompanyLogo from 'components/Header/CompanyLogo'
import { shallow } from 'enzyme'

// setup jest mocks
import UserStore from 'stores/user_store'
import CompanyStore from 'stores/company_store'

jest.mock('stores/user_store')
jest.mock('stores/company_store')

describe('CompanyLogo', () => {
  let wrapper
  const user = {
    full_name: 'Full Name',
    username: 'User Name',
    company: {
      id: 1,
      name: 'A Company Name ',
    },
  }

  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('without a company id and logo_url', () => {
    it('works', () => {
      // this is what UserStore returns as a default
      const url = '#login'
      UserStore.getUserUrl.mockReturnValue(url)
      wrapper = shallow(<CompanyLogo />)
      const logo = wrapper.find('a.logo')
      expect(logo).to.have.attr('href', url)
      expect(logo.find('span.logo-mini img')).to.have.attr('alt', 'Swoop')
      expect(logo.find('span.logo-lg img')).to.exist
    })
  })

  describe('with a company id and logo_url', () => {
    it('works', () => {
      const url = '#partner/dispatch'
      const logoUrl = '/foo.jpg'
      UserStore.getUser.mockReturnValue(user)
      CompanyStore.get.mockReturnValue(logoUrl)
      UserStore.getUserUrl.mockReturnValue(url)
      wrapper = shallow(<CompanyLogo />)
      const logo = wrapper.find('a.logo')
      expect(logo).to.have.attr('href', url)
      const img = logo.find('img.company_logo')
      expect(img).to.have.attr('alt', 'Company Logo')
      expect(img).to.have.attr('src', logoUrl)
    })
  })
})
