import React from 'react'
import Session, { DEFAULT_SESSIONS } from 'components/Header/Session'
import { shallow } from 'enzyme'

describe('Session', () => {
  let wrapper

  it('works without a session', () => {
    wrapper = shallow(<Session />)
    expect(wrapper.find('select')).to.not.have.prop('value')
    const options = wrapper.find('select option')
    expect(options).to.have.length(DEFAULT_SESSIONS.length)
    options.forEach((option, index) => {
      expect(option).to.have.attr('value', String(index - 1))
      expect(option).to.have.text(`${index - 1} - ${DEFAULT_SESSIONS[index]}`)
    })
  })

  it('works without a session < 10', () => {
    const session = '9'
    wrapper = shallow(<Session {...{ session }} />)
    expect(wrapper.find('select')).to.have.prop('value', session)
    const options = wrapper.find('select option')
    expect(options).to.have.length(DEFAULT_SESSIONS.length)
    options.forEach((option, index) => {
      expect(option).to.have.attr('value', String(index - 1))
      expect(option).to.have.text(`${index - 1} - ${DEFAULT_SESSIONS[index]}`)
    })
  })

  it('works without a session >= 10', () => {
    const session = '11'
    wrapper = shallow(<Session {...{ session }} />)
    expect(wrapper.find('select')).to.have.prop('value', session)
    const options = wrapper.find('select option')
    expect(options).to.have.length(DEFAULT_SESSIONS.length + 1)
    options.forEach((option, index) => {
      if (index === 0) {
        expect(option).to.have.attr('value', session)
        expect(option).to.have.text(session)
      } else {
        expect(option).to.have.attr('value', String(index - 2))
        expect(option).to.have.text(`${index - 2} - ${DEFAULT_SESSIONS[index - 1]}`)
      }
    })
  })
})
