import React from 'react'
import {
  Header as HeaderComponent, DISPATCH_MENU,
  TOOLS_MENU, FLEET_MENU, ROOT_MENU, SWOOP_DISPATCH_MENU,
  FLEET_SERVICE_MAP_MENU_ITEM, INVOICING_MENU_ITEM, REVIEW_FEED_MENU_ITEM,
  REPORTING_MENU_ITEM, STORAGE_MENU_ITEM,
} from 'components/Header'
import { mount } from 'enzyme'
import deepcopy from 'deepcopy'
import Consts from 'consts'
import createRouter from 'router5'
import { RouterProvider } from 'react-router5'

// setup jest mocks
import UserStore from 'stores/user_store'
import FeatureStore from 'stores/feature_store'
import SiteStore from 'stores/site_store'

jest.mock('stores/user_store')
jest.mock('stores/feature_store')
jest.mock('stores/site_store')

const Header = () =>
  <RouterProvider router={createRouter()}>
    <HeaderComponent />
  </RouterProvider>


describe('Header', () => {
  let wrapper, user
  const _user = {
    full_name: 'Full Name',
    username: 'User Name',
    company: {
      id: 1,
      name: 'A Company Name ',
    },
  }

  beforeAll(() => {
    global.resizeDesktop()
  })

  beforeEach(() => {
    jest.resetAllMocks()
    user = deepcopy(_user)
    // otherwise <AfterHours> blows up
    SiteStore.getClosedDispatchSites.mockReturnValue([])
  })

  describe('menu', () => {
    describe('menu items', () => {
      describe('as a partner', () => {
        it('works', () => {
          UserStore.isPartner.mockReturnValue(true)
          wrapper = mount(<Header />)
          const links = wrapper.find('ul.app-nav li BaseLink')
          expect(links.length).to.equal(DISPATCH_MENU.length)
          DISPATCH_MENU.forEach(({ title, href }, index) => {
            const link = links.at(index)
            expect(link.prop('routeName')).toBe(`partner/${href}`)
            expect(link.prop('title')).toBe(title)
            expect(link.text()).toBe(title)
          })
        })
      })
      describe('as tools', () => {
        it('works', () => {
          UserStore.isTools.mockReturnValue(true)
          wrapper = mount(<Header />)
          const links = wrapper.find('ul.app-nav li BaseLink')
          expect(links.length).to.equal(TOOLS_MENU.length)
          TOOLS_MENU.forEach(({ title, href }, index) => {
            const link = links.at(index)
            expect(link.prop('routeName')).toBe(href)
            expect(link.prop('title')).toBe(title)
            expect(link.text()).toBe(title)
          })
        })
      })
      describe('as fleet', () => {
        beforeEach(() => {
          UserStore.isFleet.mockReturnValue(true)
        })
        it('works', () => {
          wrapper = mount(<Header />)
          const links = wrapper.find('ul.app-nav li BaseLink')
          expect(links.length).to.equal(FLEET_MENU.length)
          FLEET_MENU.forEach(({ title, href }, index) => {
            const link = links.at(index)
            expect(link.prop('routeName')).toBe(`fleet/${href}`)
            expect(link.prop('title')).toBe(title)
            expect(link.text()).toBe(title)
          })
        })
      })
      describe('as root', () => {
        beforeEach(() => UserStore.isRoot.mockReturnValue(true))
        it('works', () => {
          wrapper = mount(<Header />)
          const links = wrapper.find('ul.app-nav li BaseLink')

          // a root user always sees the reporting menu by default
          const rootMenu = ROOT_MENU.concat([REPORTING_MENU_ITEM]).filter(item => item.show !== false)

          expect(links.length).to.equal(rootMenu.length)
          rootMenu.forEach(({ title, href }, index) => {
            const link = links.at(index)
            expect(link.prop('routeName')).toBe(href)
            expect(link.prop('title')).toBe(title)
            expect(link.text()).toBe(title)
          })
        })
      })
      describe('as dispatcher', () => {
        it('works', () => {
          UserStore.isPartner.mockReturnValue(true)
          wrapper = mount(<Header />)
          const links = wrapper.find('ul.app-nav li BaseLink')
          SWOOP_DISPATCH_MENU.forEach(({ title, href }, index) => {
            const link = links.at(index)
            expect(link.prop('routeName')).toBe(`partner/${href}`)
            expect(link.prop('title')).toBe(title)
            expect(link.text()).toBe(title)
          })
        })
      })

      describe('FLEET_SERVICE_MAP_MENU_ITEM', () => {
        it('works as fleet with FEATURE_FLEET_SERVICE_MAP enabled', () => {
          UserStore.isFleet.mockReturnValue(true)
          FeatureStore.isFeatureEnabled.mockImplementation(
            feature => feature === Consts.FEATURE_FLEET_SERVICE_MAP
          )
          wrapper = mount(<Header />)
          const { title, href } = FLEET_SERVICE_MAP_MENU_ITEM
          const link = wrapper.find(`ul.app-nav li BaseLink[routeName="fleet/${href}"][title="${title}"]`)
          expect(link).to.exist
          expect(link).to.have.text(title)
        })
      })
      describe('INVOICING_MENU_ITEM', () => {
        describe('with invoicing enabled', () => {
          beforeEach(() => FeatureStore.isFeatureEnabled.mockImplementation(
            feature => feature === 'Invoicing'
          ))
          it('works as root', () => {
            UserStore.hasRole.mockImplementation(role => role === 'root')
            wrapper = mount(<Header />)
            const { title, href } = INVOICING_MENU_ITEM
            const link = wrapper.find(`ul.app-nav li BaseLink[routeName="${href}"][title="${title}"]`)
            expect(link).to.exist
            expect(link).to.have.text(title)
          })
          it('works as non-swoop admin', () => {
            UserStore.hasRole.mockImplementation(role => role === 'admin')
            UserStore.isSwoop.mockReturnValue(false)
            wrapper = mount(<Header />)
            const { title, href } = INVOICING_MENU_ITEM
            const link = wrapper.find(`ul.app-nav li BaseLink[routeName="${href}"][title="${title}"]`)
            expect(link).to.exist
            expect(link).to.have.text(title)
          })
        })
      })
      describe('REVIEW_FEED_MENU_ITEM', () => {
        it('works with FEATURE_REVIEW_FEED enabled', () => {
          UserStore.isFleet.mockReturnValue(true)
          FeatureStore.isFeatureEnabled.mockImplementation(
            feature => feature === Consts.FEATURE_REVIEW_FEED
          )
          wrapper = mount(<Header />)
          const { title, href } = REVIEW_FEED_MENU_ITEM
          const link = wrapper.find(`ul.app-nav li BaseLink[routeName="fleet/${href}"][title="${title}"]`)
          expect(link).to.exist
          expect(link).to.have.text(title)
        })
      })

      describe('REPORTING_MENU_ITEM', () => {
        // we already checked in root above that this shows up by default
        describe('with Hide Reporting enabled', () => {
          beforeEach(() => FeatureStore.isFeatureEnabled.mockImplementation(
            feature => feature === 'Hide Reporting'
          ))
          it('works as root', () => {
            UserStore.hasRole.mockImplementation(role => role === 'root')
            wrapper = mount(<Header />)
            const { title, href } = REPORTING_MENU_ITEM
            const link = wrapper.find(`ul.app-nav li BaseLink[routeName="${href}"][title="${title}"]`)
            expect(link).to.not.exist
          })
          it('works as admin', () => {
            UserStore.hasRole.mockImplementation(role => role === 'admin')
            UserStore.isSwoop.mockReturnValue(false)
            wrapper = mount(<Header />)
            const { title, href } = INVOICING_MENU_ITEM
            const link = wrapper.find(`ul.app-nav li BaseLink[routeName="${href}"][title="${title}"]`)
            expect(link).to.not.exist
          })
        })
      })
      describe('STORAGE_MENU_ITEM', () => {
        it('works as tesla', () => {
          UserStore.isTesla.mockReturnValue(true)
          wrapper = mount(<Header />)
          const { title, href } = STORAGE_MENU_ITEM
          const link = wrapper.find(`ul.app-nav li BaseLink[routeName="${href}"][title="${title}"]`)
          expect(link).to.exist
          expect(link).to.have.text(title)
        })
        describe('as a partner', () => {
          beforeEach(() => UserStore.isPartner.mockReturnValue(true))

          it('works with FEATURE_STORAGE enabled', () => {
            FeatureStore.isFeatureEnabled.mockImplementation(
              feature => feature === Consts.FEATURE_STORAGE
            )
            wrapper = mount(<Header />)
            const { title, href } = STORAGE_MENU_ITEM
            const link = wrapper.find(`ul.app-nav li BaseLink[routeName="partner/${href}"][title="${title}"]`)
            expect(link).to.exist
            expect(link).to.have.text(title)
          })
          it('works when inTireProgram', () => {
            SiteStore.inTireProgram.mockReturnValue(true)
            wrapper = mount(<Header />)
            const { title, href } = STORAGE_MENU_ITEM
            const link = wrapper.find(`ul.app-nav li BaseLink[routeName="partner/${href}"][title="${title}"]`)
            expect(link).to.exist
            expect(link).to.have.text(title)
          })
        })
      })
    })
  })
})
