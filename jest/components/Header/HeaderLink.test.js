import React from 'react'
import HeaderLinkComponent from 'components/Header/HeaderLink'
import { shallow, mount } from 'enzyme'
import createRouter from 'router5'
import { RouterProvider } from 'react-router5'

// setup jest mocks
import UserStore from 'stores/user_store'

jest.mock('stores/user_store')

const HeaderLink = props =>
  <RouterProvider router={createRouter()}>
    <HeaderLinkComponent {...props} />
  </RouterProvider>

describe('HeaderLink', () => {
  let wrapper

  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('without a menuItem', () => {
    it('works', () => {
      wrapper = shallow(<HeaderLink />).find('HeaderLink')
      expect(wrapper).to.be.empty
    })
  })

  describe('with a menuItem', () => {
    const title = 'title'
    const href = 'href'
    const menuItem = { title, href }

    it('works', () => {
      wrapper = mount(<HeaderLink {...{ menuItem }} />).find('BaseLink')
      expect(wrapper.prop('routeName')).toBe(href)
      expect(wrapper.prop('title')).toBe('title', title)
      expect(wrapper.text()).toBe(title)
    })
    it('works with a type', () => {
      const type = 'type'
      wrapper = mount(<HeaderLink {...{ menuItem, type }} />).find('BaseLink')
      expect(wrapper.prop('routeName')).toBe(`${type}${href}`)
      expect(wrapper.prop('title')).toBe('title', title)
      expect(wrapper.text()).toBe(title)
    })
    it('works as a partner', () => {
      UserStore.isPartner.mockReturnValue(true)
      wrapper = mount(<HeaderLink {...{ menuItem }} />).find('BaseLink')
      expect(wrapper.prop('routeName')).toBe(`partner/${href}`)
      expect(wrapper.prop('title')).toBe('title', title)
      expect(wrapper.text()).toBe(title)
    })
    it('works as a fleet', () => {
      UserStore.isFleet.mockReturnValue(true)
      wrapper = mount(<HeaderLink {...{ menuItem }} />).find('BaseLink')
      expect(wrapper.prop('routeName')).toBe(`fleet/${href}`)
      expect(wrapper.prop('title')).toBe('title', title)
      expect(wrapper.text()).toBe(title)
    })
  })
})
