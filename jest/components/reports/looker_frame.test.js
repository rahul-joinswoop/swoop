import React from 'react'
import { mount } from 'enzyme'
import LookerFrame from 'components/reports/looker_frame'
import Api from 'api'
import 'loader'
import LookerDashboardStore from 'stores/looker_dashboard_store'

window.Tracking = {
  track: jest.fn(),
}
jest.mock('api', () => ({
  lookerReport: jest.fn(),
}))
jest.mock('utils', () => ({
  getEndpoint: jest.fn(() => 'endpoint'),
}))


jest.mock('stores/looker_dashboard_store', () => ({
  getAll: jest.fn(),
  bind: jest.fn(),
  unbind: jest.fn(),
}))

describe('Looker Frame', () => {
  let wrapper

  it('Fetches the first frame on mount', () => {
    LookerDashboardStore.getAll.mockReturnValue({ 2: { id: 2 } })
    wrapper = mount(<LookerFrame />)
    expect(Api.lookerReport).toHaveBeenCalled()
  })


  it('No fetch if no dashboards', () => {
    Api.lookerReport.mockReset()
    LookerDashboardStore.getAll.mockReturnValue({})
    wrapper = mount(<LookerFrame />)
    expect(Api.lookerReport).not.toHaveBeenCalled()
  })

  it('Clears timeout on unmount', () => {
    LookerDashboardStore.getAll.mockReturnValue({ 2: { id: 2 } })
    wrapper = mount(<LookerFrame />)
    window.clearTimeout = jest.fn()
    expect(window.clearTimeout).not.toHaveBeenCalled()
    wrapper.unmount()
    expect(window.clearTimeout).toHaveBeenCalled()
  })
})
