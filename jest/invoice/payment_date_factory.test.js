import React from 'react'

import TestUtils from 'react-dom/test-utils'
import '../../app/assets/javascripts/loader'
import InvoiceFields from 'invoice_fields'
import moment from 'moment-timezone'
import Wrapper from '../support/wrapper'

describe('PaymentDate', () => {
  let instance = null
  let renderedInstance = null

  const paymentItem = {
    id: 1,
    mark_paid_at: '2019-05-01T10:00:00.000Z',
  }

  const resetModules = (_paymentItem, props) => {
    jest.resetModules()

    const PaymentDate = InvoiceFields.PaymentDateFactory(_paymentItem, 1)
    instance = new PaymentDate(props, (() => {}), {}, {}, {}, {})

    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )

    return renderedInstance
  }

  describe('onChange()', () =>
    it('sets it correctly', () => {
      resetModules(paymentItem, {})

      const newDate = '2019-06-31T07:00:00.000Z'
      const momentDate = moment(new Date(newDate))

      instance.onChange(momentDate)
      expect(instance.record.payments['1'].mark_paid_at).to.equal('2019-07-01T07:00:00.000Z')
    })
  )
})
