import ReactTestUtils from 'react-dom/test-utils'
import React from 'react'

import '../../app/assets/javascripts/loader'
import InvoiceStatus from 'components/invoice/status'
import UserStore from 'stores/user_store'
import Utils from 'utils'

function getResultFromRender(invoice) {
  jest.resetModules()

  const instance = ReactTestUtils.renderIntoDocument(
    <InvoiceStatus handleInvoiceChange={() => {}} invoice={invoice} />
  )
  // renderer.render(<InvoiceStatus invoice={invoice} />);
  // return renderer.getRenderOutput();
  return instance.render()
}

describe('when user is partner', () => {
  beforeAll(() => {
    UserStore.isPartner = () => true
  })

  describe('when job is assigned from fleet', () => {
    beforeAll(() => {
      Utils.isAssignedFromFleet = () => true
    })

    describe('when invoice state is partner_sent', () => {
      test('shows "Account Received"', () => {
        const result = getResultFromRender({ state: 'partner_sent' })

        expect(result.type).to.equal('span')
        expect(result.props.children).to.equal('Account Received')
      })
    })

    describe('when invoice state is swoop_approved_partner', () => {
      test('shows "Account Approved"', () => {
        const result = getResultFromRender({ state: 'swoop_approved_partner' })

        expect(result.type).to.equal('span')
        expect(result.props.children).to.equal('Account Approved')
      })
    })

    describe('when invoice state is fleet_approved', () => {
      test('shows "Account Approved"', () => {
        const result = getResultFromRender({ state: 'fleet_approved' })

        expect(result.type).to.equal('span')
        expect(result.props.children).to.equal('Account Approved')
      })
    })

    describe('when invoice state is fleet_downloaded', () => {
      test('shows "Mark Paid" button"', () => {
        const result = getResultFromRender({ state: 'fleet_downloaded' })
        expect(result.props.children).to.equal('Mark Paid')
        // expect(result.props.children).to.equal('Mark Paid');
      })
    })

    describe('when invoice state is swoop_downloaded_partner', () => {
      test('shows "Mark Paid" button"', () => {
        const result = getResultFromRender({ state: 'swoop_downloaded_partner' })

        expect(result.props.children).to.equal('Mark Paid')
        // expect(result.props.children).to.equal('Mark Paid');
      })
    })
  })

  describe('when job is not assigned from fleet', () => {
    beforeAll(() => {
      Utils.isAssignedFromFleet = () => false
    })

    describe('when invoice state is partner_sent', () => {
      test('shows "Mark Paid" button', () => {
        const result = getResultFromRender({ state: 'partner_sent' })

        expect(result.props.children).to.equal('Mark Paid')
        // expect(result.props.children).to.equal('Mark Paid');
      })
    })

    describe('when invoice state is swoop_approved_partner', () => {
      test('shows "Mark Paid" button', () => {
        const result = getResultFromRender({ state: 'swoop_approved_partner' })

        expect(result.props.children).to.equal('Mark Paid')
        // expect(result.props.children).to.equal('Mark Paid');
      })
    })

    describe('when invoice state is fleet_approved', () => {
      test('shows "Mark Paid" button', () => {
        const result = getResultFromRender({ state: 'fleet_approved' })

        expect(result.props.children).to.equal('Mark Paid')
        // expect(result.props.children).to.equal('Mark Paid');
      })
    })
  })
})
