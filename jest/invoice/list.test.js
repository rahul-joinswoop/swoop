/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import '../../app/assets/javascripts/loader'

import React from 'react'
import TestUtils from 'react-dom/test-utils'
import { Component as InvoiceList } from 'components/invoice/list'
import UserStore from 'stores/user_store'
import mocker from '../mocker'

const invoice = {
  id: 1,
  currency: 'USD',
  job: {
    id: 1,
    rescue_driver_id: 1,
    customer_type_name: 'Warranty',
    status: 'En Route',
  },
}
describe('Invoice List', () => {
  const InvoiceList = React.createFactory(require('components/invoice/list').Component)
  let instance = null

  const resetModules = (props) => {
    mocker.mock('stores/user_store', {
      getUser() {
        return {
          id: 1,
        }
      },

      isDriver() {
        return true
      },
    })
    mocker.mock('stores/invoice_store', {
      getSearchInvoices() {
        return [invoice]
      },

      getInvoiceById() {
        return invoice
      },
    })
    mocker.mock('stores/job_store', {
      getJobById() {
        return invoice.job
      },

      getFullJobById() {
        return invoice.job
      },
    })
    mocker.mock('utils.coffee', {
      formatNumber() {
        return 'Hack to get around intl needing parent'
      },
    })
    mocker.mock('stores/department_store', {})
    jest.resetModules()
    instance = new InvoiceList(props)
    instance = TestUtils.renderIntoDocument(instance)
    return instance
  }

  describe('payment type', () => it('Should always show up', () => {
    resetModules({
      types: ['fake'],
      empty_message: 'no message',
    })
    const row = TestUtils.findRenderedDOMComponentWithClass(instance, 'swoopTable')
    const text = row.textContent
    expect(text.indexOf('Warranty')).not.to.equal(-1)
    expect(text.indexOf('Customer Pay')).to.equal(-1)
  }))
  describe('Department', () => {
    it('Should not show up for non tesla', () => {
      resetModules({
        types: ['fake'],
        empty_message: 'no message',
      })
      const row = TestUtils.findRenderedDOMComponentWithClass(instance, 'swoopTable')
      const text = row.textContent
      expect(text.indexOf('Department')).to.equal(-1)
    })
    it('Should not show up for tesla without new job feature', () => {
      resetModules({
        types: ['fake'],
        empty_message: 'no message',
      })
      mocker.mock('stores/user_store', {
        isTesla() {
          return true
        },
      })
      const row = TestUtils.findRenderedDOMComponentWithClass(instance, 'swoopTable')
      const text = row.textContent
      expect(text.indexOf('Department')).to.equal(-1)
    })
    it('Should show up for tesla with new job feature', () => {
      mocker.mock('stores/user_store', {
        isTesla() {
          return true
        },
      })
      mocker.mock('stores/feature_store', {
        isFeatureEnabled() {
          return true
        },
      })
      mocker.mock('stores/department_store', {
        get() {
          return 'testdepartment'
        },
      })
      resetModules({
        types: ['fake'],
        empty_message: 'no message',
      })
      const row = TestUtils.findRenderedDOMComponentWithClass(instance, 'swoopTable')
      const text = row.textContent
      expect(text.indexOf('Department')).not.to.equal(-1)
      expect(text.indexOf('testdepartment')).not.to.equal(-1)
    })
  })
})

describe('showMoveBackToNew()', () => {
  it('returns true when user is partner and tab is sent', () => {
    const invoiceListInstance = new InvoiceList({ tab: 'sent' })
    UserStore.isPartner = () => true
    expect(invoiceListInstance.showMoveBackToNew(invoice)).to.be.true
  })
})

describe('showRejectInvoice()', () => {
  it('returns false when invoice state is not partner_sent', () => {
    const invoiceListInstance = new InvoiceList({})
    expect(invoiceListInstance.showRejectInvoice(invoice)).to.be.false
  })

  describe('when invoice state is partner_sent', () => {
    const invoiceListInstance = new InvoiceList({})

    beforeAll(() => {
      invoice.state = 'partner_sent'
    })

    it('returns true when user is fleet in house', () => {
      UserStore.isFleetInHouse = () => true
      expect(invoiceListInstance.showRejectInvoice(invoice)).to.be.true
    })

    it('returns true when user is swoop', () => {
      UserStore.isSwoop = () => true
      expect(invoiceListInstance.showRejectInvoice(invoice)).to.be.true
    })
  })
})

describe('showMarkJobPaid()', () => {
  const invoiceListInstance = new InvoiceList({ tab: 'paid' })

  const invoice = {
    id: 1,
    state: 'partner_approved',
    paid: false,
    job: {
      id: 1,
      type: 'RescueJob',
    },
  }

  describe('when job is of partner type', () => {
    it('returns true', () => {
      UserStore.isPartner = () => true

      expect(invoiceListInstance.showMarkJobPaid(invoice)).to.be.true
    })
  })

  describe('when job is of motorclub type', () => {
    const invoice = {
      id: 1,
      state: 'partner_approved',
      paid: false,
      job: {
        id: 1,
        type: 'FleetMotorClubJob',
      },
    }

    it('returns true', () => {
      UserStore.isPartner = () => true

      expect(invoiceListInstance.showMarkJobPaid(invoice)).to.be.true
    })
  })

  describe('when paid is undefined', () => {
    const invoice = {
      id: 1,
      state: 'partner_approved',
      paid: undefined,
      job: {
        id: 1,
        type: 'RescueJob',
      },
    }

    it('returns true', () => {
      UserStore.isPartner = () => true

      expect(invoiceListInstance.showMarkJobPaid(invoice)).to.be.true
    })
  })

  describe('when paid is true', () => {
    const invoice = {
      id: 1,
      state: 'partner_approved',
      paid: true,
      job: {
        id: 1,
        type: 'RescueJob',
      },
    }

    it('returns false', () => {
      UserStore.isPartner = () => true

      expect(invoiceListInstance.showMarkJobPaid(invoice)).to.be.false
    })
  })

  describe('when User is not Partner', () => {
    it('returns false', () => {
      UserStore.isPartner = () => false

      expect(invoiceListInstance.showMarkJobPaid(invoice)).to.be.false
    })
  })

  describe('when invoice.state is not partner_approved', () => {
    const invoice = {
      id: 1,
      state: 'other_than_partner_approved',
      paid: true,
      job: {
        id: 1,
        type: 'RescueJob',
      },
    }

    it('returns false', () => {
      UserStore.isPartner = () => true

      expect(invoiceListInstance.showMarkJobPaid(invoice)).to.be.false
    })
  })
})
