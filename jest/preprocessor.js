const coffee = require('coffeescript')
const path = require('path')
const crypto = require('crypto')
const fs = require('fs')

const babelConfig = {
  presets: [
    ['@babel/preset-env', { useBuiltIns: 'entry', modules: false, corejs: 2 }],
    ['@babel/preset-react', { development: true, useBuiltIns: true }],
  ],
  plugins: [
    '@babel/plugin-transform-modules-commonjs',
  ],
}

// from https://github.com/facebook/fbjs/blob/master/packages/fbjs-scripts/jest/createCacheKeyFunction.js
function buildCacheKey(files, base) {
  return files.reduce(
    /* eslint-disable-next-line */
    (src, fileName) => src + fs.readFileSync(fileName),
    base
  )
}

function createCacheKeyFunction(files) {
  const coffeeVersion = coffee.VERSION
  const cacheKey = buildCacheKey(files, coffeeVersion + JSON.stringify(babelConfig))
  return (src, file, configString, options) => crypto.
  createHash('md5').
  update(cacheKey).
  update(src + file + configString).
  update(options && options.instrument ? 'instrument' : '').
  digest('hex')
}


// this file is loaded without babel so we can't ues es6 modules
module.exports = {
  // slightly altered version of
  // https://github.com/nicolindemann/jest-coffee-preprocessor/blob/master/index.js
  // with the transpile: option set
  process(src, file) {
    if (coffee.helpers.isCoffee(file)) {
      return coffee.compile(src, {
        bare: true,
        header: false,
        inlineMap: true,
        transpile: babelConfig,
        generatedFile: path.basename(file),
        sourceFiles: [path.basename(file)],
      })
    }
    return null
  },
  getCacheKey: createCacheKeyFunction([
    __filename,
    path.join(__dirname, '..', 'package.json'),
  ]),

}
