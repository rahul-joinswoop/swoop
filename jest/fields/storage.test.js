/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'

import '../../app/assets/javascripts/loader' // Mock out the set of user
import { WillStoreField } from 'job_fields'
import TestUtils from 'react-dom/test-utils'
import Wrapper from '../support/wrapper'
import mocker from '../mocker'

describe('Job Delete', () => {
  let instance = null
  let renderedInstance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new WillStoreField(props, (() => {}), {}, {}, {}, {})
    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )
    return renderedInstance
  }

  describe('Will Store Field Partner', () => {
    beforeEach(() => {
      mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },
      })
      return mocker.mock('stores/service_store', {
        getServiceByName: () => ({
          support_storage: true,
        }),
        isStorageEnabled: () => true,
      })
    })
    describe('visibility', () => {
      it('should not show for unset', () => {
        mocker.mock('stores/service_store', {
          getServiceByName: () => ({
            support_storage: false,
          }),
          isStorageEnabled: () => true,
        })
        resetModules({})
        expect(instance.isShowing()).not.to.be.true
      })
      it('should not show for other things', () => {
        mocker.mock('stores/service_store', {
          getServiceByName: () => ({
            support_storage: true,
          }),
          isStorageEnabled: () => false,
        })
        resetModules({
          service: 'Other',
        })
        expect(instance.isShowing()).not.to.be.true
      })
    })
  })
})
