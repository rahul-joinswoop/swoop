/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'

import '../../app/assets/javascripts/loader'
import { DropoffLocationTypeIdField, PickupLocationTypeIdField } from 'job_fields'
import TestUtils from 'react-dom/test-utils'
import Wrapper from '../support/wrapper'
import mocker from '../mocker'

describe('Job Location Types', () => {
  mocker.mock('stores/user_store', {
    isPartner() {
      return true
    },

    getUser() {
      return {
        id: 1,
        company: {
          location_types: [
            {
              id: 10,
              name: 'Partner Location Type 1',
            },
            {
              id: 20,
              name: 'Partner Location Type 2',
            },
          ],
        },
      }
    },
  })
  describe('PickupLocationTypeIdField', () => {
    beforeEach(() => jest.resetModules())
    const job = {
      id: 1,
      service_location: {
        location_type_id: 1,
      },
    }
    const pickupLocationType = new PickupLocationTypeIdField(job, (() => {}), {}, {}, {}, {})
    TestUtils.renderIntoDocument(
      <Wrapper>
        {pickupLocationType.render()}
      </Wrapper>
    )
    it('should include partner company location types', () => {
      const location_type_names = pickupLocationType.getOptions().map(obj => obj.text)

      expect(location_type_names).to.include('Partner Location Type 1')
      expect(location_type_names).to.include('Partner Location Type 2')
    })
  })
  describe('DropoffLocationTypeIdField', () => {
    beforeEach(() => jest.resetModules())
    const job = {
      id: 1,
      dropoff_location: {
        location_type_id: 1,
      },
    }
    const dropoffLocationType = new DropoffLocationTypeIdField(job, (() => {}), {}, {}, {}, {})
    TestUtils.renderIntoDocument(
      <Wrapper>
        {dropoffLocationType.render()}
      </Wrapper>
    )
    it('should include partner company location types', () => {
      const location_type_names = dropoffLocationType.getOptions().map(obj => obj.text)

      expect(location_type_names).to.include('Partner Location Type 1')
      expect(location_type_names).to.include('Partner Location Type 2')
    })
  })
})
