import '../../app/assets/javascripts/loader' // Mock out the set of user
import { LookupTypePolicyNumberField } from 'job_fields'
import UserStore from 'stores/user_store'
import Consts from 'consts'

describe('LookupTypePolicyNumberField', () => {
  let instance = null

  const resetModules = (record) => {
    jest.resetModules()
    instance = new LookupTypePolicyNumberField(record, (() => {}), {}, {}, {}, {})

    return instance
  }

  describe('#forcePrefill', () => {
    describe('when @record.customer_lookup_type == Policy Number', () => {
      describe('when @record.policy_number is present', () => {
        it('prefills it with @record.policy_number', () => {
          resetModules({
            id: 1,
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER,
            pcc_policy: {
              policy_number: '123ABC',
            },
          })

          instance.forcePrefill()

          expect(instance.getValue()).to.equal('123ABC')
        })
      })

      describe('when @record.policy_number is not present', () => {
        it('set it as undefined', () => {
          resetModules({
            id: 1,
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER,
          })

          instance.forcePrefill()

          expect(instance.getValue()).to.be.undefined
        })
      })
    })

    describe('when @record.customer_lookup_type is not Policy Number', () => {
      it('set it as undefined', () => {
        resetModules({ id: 1 })

        instance.forcePrefill()

        expect(instance.getValue()).to.be.undefined
      })
    })
  })

  describe('#isShowing', () => {
    describe('when logged in user is not Partner', () => {
      describe('and @record.customer_lookup_type == Policy Number', () => {
        it('returns true', () => {
          UserStore.setUser({
            id: 1,
            full_name: 'Test User',
            roles: ['fleet'],
          })

          resetModules({
            id: 1,
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER,
          })

          expect(instance.isShowing()).to.be.true
        })
      })

      describe('and @record.customer_lookup_type is not Policy Number', () => {
        it('returns false', () => {
          UserStore.setUser({
            id: 1,
            full_name: 'Test User',
            roles: ['fleet'],
          })

          resetModules({ id: 1 })

          expect(instance.isShowing()).to.be.false
        })
      })
    })

    describe('when logged in user is Partner', () => {
      it('returns false', () => {
        UserStore.setUser({
          id: 1,
          full_name: 'Test User',
          roles: ['rescue'],
        })

        resetModules({
          id: 1,
          customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER,
        })

        expect(instance.isShowing()).to.be.false
      })
    })
  })
})
