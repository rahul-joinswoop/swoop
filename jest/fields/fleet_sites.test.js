/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'

import TestUtils from 'react-dom/test-utils'
import '../../app/assets/javascripts/loader' // Mock out the set of user
import { FleetSiteIdField } from 'job_fields'
import FeatureStore from 'stores/feature_store'
import UserStore from 'stores/user_store'
import Consts from 'consts'
import Wrapper from '../support/wrapper'
import mocker from '../mocker'

describe('Job Fleet Site', () => {
  let instance = null
  let renderedInstance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new FleetSiteIdField(props, (() => {}), {}, {}, {}, {})
    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )
    return renderedInstance
  }

  describe('when it is a Fleet user', () => {
    beforeEach(() => mocker.mock('stores/user_store', {
      isFleet() {
        return true
      },
    }))

    describe('#isRequired', () => {
      const testSetup = (siteRequired = true) => {
        const userCompanyfeatures = []

        const siteRequiredFeature = { id: 1, name: Consts.FEATURES_JOB_FORM_SITE_REQUIRED }
        FeatureStore._addRowToInternalCollection(siteRequiredFeature)

        if (siteRequired) {
          userCompanyfeatures.push(siteRequiredFeature.id)
        }

        // mock user with a company with features
        UserStore.setUser({
          id: 1,
          full_name: 'Test User',
          roles: ['fleet'],
          company: {
            features: userCompanyfeatures,
          },
        })

        return new FleetSiteIdField({}, (() => {}), {}, {}, {}, {})
      }

      describe('when Job Form Site Required feature is enabled', () => {
        it('returns true', () => {
          const fieldInstance = testSetup()

          expect(fieldInstance.isRequired()).to.be.true
        })
      })

      describe('when Job Form Site Required feature is disabled', () => {
        it('returns false', () => {
          const fieldInstance = testSetup(false)

          expect(fieldInstance.isRequired()).to.be.false
        })
      })
    })

    describe('#isShowing()', () => {
      it('should show if feature enabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_FLEET_SITES) {
              return true
            }

            return cb(feature)
          },
        })

        resetModules({})
        expect(instance.isShowing()).to.be.true
      })
      it('should not show if feature disabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_FLEET_SITES) {
              return false
            }

            return cb(feature)
          },
        })

        resetModules({})
        expect(instance.isShowing()).to.be.false
      })
    })
  })

  describe('when it is Swoop user', () => {
    describe('#isShowing()', () => {
      it('should show if "Client Sites" and "Show Client Sites to Swoop" feature enabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_FLEET_SITES || feature === Consts.FEATURES_SHOW_CLIENT_SITES_TO_SWOOP) {
              return true
            }

            return cb(feature)
          },
        })

        resetModules({})
        expect(instance.isShowing()).to.be.true
      })

      it('should not show if "Client Sites" feature disabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_FLEET_SITES) {
              return false
            } else if (feature === Consts.FEATURES_SHOW_CLIENT_SITES_TO_SWOOP) {
              return true
            }

            return cb(feature)
          },
        })

        resetModules({ acts_as_company_id: 123 })
        expect(instance.isShowing()).to.be.false
      })

      it('should not show if "Show Client Sites to Swoop" feature disabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_FLEET_SITES) {
              return true
            } else if (feature === Consts.FEATURES_SHOW_CLIENT_SITES_TO_SWOOP) {
              return false
            }

            return cb(feature)
          },
        })

        resetModules({ acts_as_company_id: 123 })
        expect(instance.isShowing()).to.be.false
      })
    })
  })
})
