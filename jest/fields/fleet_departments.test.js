/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'

import TestUtils from 'react-dom/test-utils'
import '../../app/assets/javascripts/loader' // Mock out the set of user
import { DepartmentIdField } from 'job_fields'
import Consts from 'consts'
import Wrapper from '../support/wrapper'
import mocker from '../mocker'

describe('Job Departments', () => {
  let instance = null
  let renderedInstance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new DepartmentIdField(props, (() => {}), {}, {}, {}, {})
    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )
    return renderedInstance
  }

  describe('Departments Field Partner', () => {
    beforeEach(() => mocker.mock('stores/user_store', {
      isFleet() {
        return true
      },
    }))
    describe('visibility', () => {
      it('should show if feature enabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_DEPARTMENTS) {
              return true
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.true
      })
      it('should not show if feature disabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_DEPARTMENTS) {
              return false
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.false
      })
    })
  })
  describe('Departments Field Other', () => {
    beforeEach(() => mocker.mock('stores/user_store', {
      isFleet() {
        return false
      },
    }))
    describe('visibility', () => {
      it('should not show if feature enabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_DEPARTMENTS) {
              return true
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.false
      })
      it('should not show if feature disabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_DEPARTMENTS) {
              return false
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.false
      })
    })
  })
})
