import { mount } from 'enzyme'
import FormRenderer from 'form_renderer'
import MoneyField from 'fields/MoneyField'
import 'loader' // delete this file when we have converted coffeescript to JS

/** MONEY FORMAT is 6 digits before dot and 2 digits after dot are allowed.
 * Values without dot or without any digits after dot are normalized to parseFloat(val).toFixed(2) */

class TestMoney extends MoneyField {
  _label = 'Test Money'

  _name = 'money'

  _required = true
}

describe('MoneyField component', () => {
  it('does not allow non money value', () => {
    const record = { money: '' }
    const callback = jest.fn()

    const form = new FormRenderer(record, callback)
    form.register(TestMoney, 'money')

    const wrapper = mount(form.renderInput('money'))
    const input = wrapper.find('#Inputmoney')

    input.simulate('change', { target: { name: '', value: 'hgfhg' } })
    expect(form.record.money).toBe('')

    input.simulate('change', { target: { name: '', value: '30' } })
    expect(form.record.money).toBe('30')

    input.simulate('change', { target: { name: '', value: '12.345' } })
    expect(form.record.money).toBe('30')

    input.simulate('change', { target: { name: '', value: '12.' } })
    expect(form.record.money).toBe('12.')

    input.simulate('blur')
    expect(form.record.money).toBe('12.00')

    wrapper.unmount()
  })
})
