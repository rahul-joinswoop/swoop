/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'

import TestUtils from 'react-dom/test-utils'
import Consts from 'consts'
import { TrailerIdField } from 'job_fields'
import Wrapper from '../support/wrapper'
import mocker from '../mocker'
import '../../app/assets/javascripts/loader' // Mock out the set of user

describe('Job Style', () => {
  let instance = null
  let renderedInstance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new TrailerIdField(props, (() => {}), {}, {}, {}, {})
    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )
    return renderedInstance
  }

  describe('Style Field Partner', () => {
    beforeEach(() => mocker.mock('stores/user_store', {
      isPartner() {
        return true
      },
    }))
    describe('visibility', () => {
      it('should show if feature enabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_TRAILER) {
              return true
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.true
      })
      it('should not show if feature disabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_TRAILER) {
              return false
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.false
      })
    })
  })
})
