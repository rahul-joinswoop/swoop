import { ServiceLocationField } from 'job_fields'

describe('Service Field', () => {
  let instance = null
  it('should be required if location type is set', () => {
    const job = {
      id: 1,
      service_location: {
        location_type_id: 1,
      },
    }

    instance = new ServiceLocationField(job, (() => {}), {}, {}, {}, {})
    expect(instance.isRequired()).toBe(true)
  })
})
