import '../../app/assets/javascripts/loader' // Mock out the set of user
import { LookupTypePhoneNumberField } from 'job_fields'
import UserStore from 'stores/user_store'
import Consts from 'consts'

describe('LookupTypePhoneNumberField', () => {
  let instance = null

  const resetModules = (record) => {
    jest.resetModules()
    instance = new LookupTypePhoneNumberField(record, (() => {}), {}, {}, {}, {})

    return instance
  }

  describe('#forcePrefill', () => {
    describe('when @record.customer_lookup_type == Phone Number', () => {
      describe('when @record.policy_number is present', () => {
        it('prefills it with @record.policy_number', () => {
          resetModules({
            id: 1,
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER,
            customer: {
              phone: '+14053334444',
            },
          })

          instance.forcePrefill()

          expect(instance.getValue()).to.equal('405-333-4444')
        })
      })

      describe('when @record.policy_number is not present', () => {
        it('set it as an empty string', () => {
          resetModules({
            id: 1,
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER,
          })

          instance.forcePrefill()

          expect(instance.getValue()).to.eq('')
        })
      })
    })

    describe('when @record.customer_lookup_type is not Phone Number', () => {
      it('set it as an empty string', () => {
        resetModules({ id: 1 })

        instance.forcePrefill()

        expect(instance.getValue()).to.eq('')
      })
    })
  })

  describe('#isShowing', () => {
    describe('when logged in user is not Partner', () => {
      describe('and @record.customer_lookup_type == Phone Number', () => {
        it('returns true', () => {
          UserStore.setUser({
            id: 1,
            full_name: 'Test User',
            roles: ['fleet'],
          })

          resetModules({
            id: 1,
            customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER,
          })

          expect(instance.isShowing()).to.be.true
        })
      })

      describe('and @record.customer_lookup_type is not Phone Number', () => {
        it('returns false', () => {
          UserStore.setUser({
            id: 1,
            full_name: 'Test User',
            roles: ['fleet'],
          })

          resetModules({ id: 1 })

          expect(instance.isShowing()).to.be.false
        })
      })
    })

    describe('when logged in user is Partner', () => {
      it('returns false', () => {
        UserStore.setUser({
          id: 1,
          full_name: 'Test User',
          roles: ['rescue'],
        })

        resetModules({
          id: 1,
          customer_lookup_type: Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.PHONE_NUMBER,
        })

        expect(instance.isShowing()).to.be.false
      })
    })
  })

  describe('#_validatePhone property', () => {
    it('is false', () => {
      jest.resetModules()
      instance = new LookupTypePhoneNumberField({}, (() => {}), {}, {}, {}, {})

      expect(instance._validatePhone).to.be.false
    })
  })

  describe('#useMasking(enabled)', () => {
    describe('when enabled is false', () => { // this is the only case we use
      it('sets instance attributes accordingly', () => {
        jest.resetModules()
        instance = new LookupTypePhoneNumberField({}, (() => {}), {}, {}, {}, {})
        instance.useMasking(false)

        expect(instance.allowDisableMasking).to.be.true
        expect(instance.disableMasking).to.be.true
        expect(instance.autoFocus).to.be.false
      })
    })
  })
})
