/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'

import TestUtils from 'react-dom/test-utils'
import '../../app/assets/javascripts/loader' // Mock out the set of user
import { AccountField } from 'job_fields'
import Wrapper from '../support/wrapper'
import mocker from '../mocker'

describe('Job Account', () => {
  let instance = null
  let renderedInstance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new AccountField(props, (() => {}), {}, {}, {}, {})
    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )
    return renderedInstance
  }

  describe('Account', () => {
    beforeEach(() => mocker.mock('stores/user_store', {
      isPartner() {
        return true
      },
    }))
    describe('OnChange', () => it('Should set department_id if on account', () => {
      const record = {}
      mocker.mock('stores/account_store', {
        get() {
          return 4
        },
      })
      resetModules(record)
      expect(record.partner_department_id).to.be.undefined
      instance.onChange({
        target: {
          value: 2,
        },
      })
      expect(record.account.id).to.equal(2)
      expect(record.partner_department_id).to.equal(4)
    }))
  })
})
