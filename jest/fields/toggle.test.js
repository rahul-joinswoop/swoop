/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import TestUtils from 'react-dom/test-utils'

import '../../app/assets/javascripts/loader' // Mock out the set of user
import React from 'react'
import createReactClass from 'create-react-class'
import ToggleField from 'fields/toggle_field'

describe('Job Delete', () => {
  const Wrapper = React.createFactory(
    createReactClass({
      render() {
        return this.props.children
      },
    }),
  )
  let instance = null
  let renderedInstance = null

  const resetModules = () => {
    jest.resetModules()
    instance = new ToggleField({}, (() => {}), {}, {}, {}, {})
    renderedInstance = TestUtils.renderIntoDocument(Wrapper(null, instance.render()))
    return renderedInstance
  }

  describe('Toggle Field', () => {
    let onButton = null
    let offButton = null

    const clickAndRerender = (button) => {
      TestUtils.Simulate.click(button)
      renderedInstance = TestUtils.renderIntoDocument(Wrapper(null, instance.render()))
      onButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'on_option')
      offButton = TestUtils.findRenderedDOMComponentWithClass(
        renderedInstance,
        'off_option',
      )
      return offButton
    }

    it('Clicking on button a bunch should keep it on', () => {
      resetModules()
      onButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'on_option')
      offButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'off_option')
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(onButton)
      expect(instance.isOn()).to.be.true
      expect(instance.isOff()).not.to.be.true
      clickAndRerender(onButton)
      expect(instance.isOn()).to.be.true
      expect(instance.isOff()).not.to.be.true
      clickAndRerender(onButton)
      expect(instance.isOn()).to.be.true
      expect(instance.isOff()).not.to.be.true
    })
    it('Clicking off button a bunch should keep it off', () => {
      resetModules()
      onButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'on_option')
      offButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'off_option')
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
    }) // TestUtils.Simulate.click(onButton)
    // expect(instance.isOn()).to.be.true
    // expect(instance.isOff()).not.to.be.true

    it('should switch when clicking on opposite', () => {
      resetModules()
      onButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'on_option')
      offButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'off_option')
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(onButton)
      expect(instance.isOn()).to.be.true
      expect(instance.isOff()).not.to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
    })
    it('should switch and stay when double clicking', () => {
      resetModules()
      onButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'on_option')
      offButton = TestUtils.findRenderedDOMComponentWithClass(renderedInstance, 'off_option')
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(onButton)
      expect(instance.isOn()).to.be.true
      expect(instance.isOff()).not.to.be.true
      clickAndRerender(onButton)
      expect(instance.isOn()).to.be.true
      expect(instance.isOff()).not.to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
      clickAndRerender(offButton)
      expect(instance.isOn()).not.to.be.true
      expect(instance.isOff()).to.be.true
    })
  })
})
