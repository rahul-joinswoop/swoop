import '../../../app/assets/javascripts/loader' // Mock out the set of user
import { UnitNumberField } from 'job_fields'
import FeatureStore from 'stores/feature_store'
import UserStore from 'stores/user_store'
import Consts from 'consts'

describe('Job Unit field', () => {
  describe('#isRequired', () => {
    const testSetup = (unitRequired = true) => {
      const userCompanyfeatures = []

      const unitRequiredFeature = { id: 1, name: Consts.FEATURES_JOB_FORM_UNIT_REQUIRED }
      FeatureStore._addRowToInternalCollection(unitRequiredFeature)

      if (unitRequired) {
        userCompanyfeatures.push(unitRequiredFeature.id)
      }

      // mock user with a company with features
      UserStore.setUser({
        id: 1,
        full_name: 'Test User',
        roles: ['fleet'],
        company: {
          features: userCompanyfeatures,
        },
      })

      return new UnitNumberField({}, (() => {}), {}, {}, {}, {})
    }

    describe('when Job Form Unit Required feature is enabled', () => {
      it('returns true', () => {
        const fieldInstance = testSetup()

        expect(fieldInstance.isRequired()).to.be.true
      })
    })

    describe('when Job Form Unit Required feature is disabled', () => {
      it('returns false', () => {
        const fieldInstance = testSetup(false)

        expect(fieldInstance.isRequired()).to.be.false
      })
    })
  })
})
