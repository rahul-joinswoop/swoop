import '../../app/assets/javascripts/loader' // Mock out the set of user
import { CustomerLookupTypeField } from 'job_fields'
import SettingStore from 'stores/setting_store'
import Consts from 'consts'

describe('CustomerLookupTypeField', () => {
  let instance = null

  const resetModules = (props) => {
    jest.resetModules()

    SettingStore._modelCollection = {}
    instance = new CustomerLookupTypeField(props, (() => {}), {}, {}, {}, {})

    return instance
  }

  describe('#prefill', () => {
    describe('when company has VIN available as a search field', () => {
      it('prefills it with VIN search', () => {
        resetModules({})

        SettingStore._addRowToInternalCollection({
          id: 123,
          key: Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.CUSTOMER_LOOKUP_TYPES,
          value: JSON.stringify(['Skip and enter manually', 'Last 8 Vin']),
        })

        instance.prefill()

        expect(instance.getValue()).to.equal(Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.VIN)
      })

      describe('when company also has Unit Number available as a search field', () => {
        it('prefills it with Unit Number search', () => {
          resetModules({})

          SettingStore._addRowToInternalCollection({
            id: 123,
            key: Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.CUSTOMER_LOOKUP_TYPES,
            value: JSON.stringify(['Skip and enter manually', 'Last 8 Vin', 'Unit Number']),
          })

          instance.prefill()

          expect(instance.getValue()).to.equal(Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.UNIT_NUMBER)
        })

        describe('when company also has Policy Number available as a search field', () => {
          it('prefills it with Policy Number search', () => {
            resetModules({})

            SettingStore._addRowToInternalCollection({
              id: 123,
              key: Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.CUSTOMER_LOOKUP_TYPES,
              value: JSON.stringify(['Skip and enter manually', 'Last 8 Vin', 'Unit Number', 'Policy Number']),
            })

            instance.prefill()

            expect(instance.getValue()).to.equal(Consts.CUSTOMER_LOOKUP_TYPE_FIELD_OPTIONS.POLICY_NUMBER)
          })
        })
      })
    })

    describe('when company does not have VIN available as a search field', () => {
      it('leaves it undefined', () => {
        resetModules({})

        SettingStore._addRowToInternalCollection({
          id: 456,
          key: Consts.COMPANY_SETTINGS.POLICY_LOOKUP_SERVICE.CUSTOMER_LOOKUP_TYPES,
          value: JSON.stringify(['Skip and enter manually']),
        })

        instance.prefill()

        expect(instance.getValue()).to.be.undefined
      })
    })
  })
})
