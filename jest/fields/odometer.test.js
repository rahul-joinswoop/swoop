/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'
import Consts from 'consts'
import { OdometerField } from 'job_fields'
import TestUtils from 'react-dom/test-utils'
import '../../app/assets/javascripts/loader' // Mock out the set of user
import Wrapper from '../support/wrapper'
import mocker from '../mocker'

describe('Job Fleet Site', () => {
  let instance = null
  let renderedInstance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new OdometerField(props, (() => {}), {}, {}, {}, {})
    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )
    return renderedInstance
  }

  describe('Fleet Site Field Tesla', () => {
    beforeEach(() => mocker.mock('stores/user_store', {
      isFleet() {
        return true
      },

      isTesla() {
        return true
      },
    }))
    describe('required', () => {
      it('should not be required if not Tesla or Lincoln', () => {
        mocker.mock('stores/user_store', {
          isFleet() {
            return true
          },

          isTesla() {
            return false
          },
        })
        mocker.mock('stores/company_store', {
          get() {
            return Consts.LINCOLN
          },
        })
        resetModules({})
        const { record } = instance
        record.acts_as_company_id = 1
        expect(instance.isRequired()).to.be.false
      })
      it('should be if Tesla', () => {
        mocker.mock('stores/user_store', {
          isFleet() {
            return true
          },

          isTesla() {
            return true
          },
        })
        resetModules({})
        expect(instance.isRequired()).to.be.true
      })
    })
  })
})
