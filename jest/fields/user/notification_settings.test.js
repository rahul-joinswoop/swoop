import React from 'react'

import TestUtils from 'react-dom/test-utils'
import '../../../app/assets/javascripts/loader'
import UserFields from 'user_fields'
import UserStore from 'stores/user_store'
import Wrapper from '../../support/wrapper'

describe('UserNotifications', () => {
  let instance = null
  let renderedInstance = null

  const availableNotificationSetting = {
    notification_code: 'new_job',
    available_notification_channels: ['Call', 'Text'],
    default_notification_channels: ['Text'],
    description: 'New job assigned to ABC Towing',
    supported_roles: ['admin', 'dispatcher'],
  }

  const userRecord = {
    id: 1,
    roles: ['admin', 'rescue'],
    notification_settings: [''],
  }

  const resetModules = (notificationSetting, props) => {
    jest.resetModules()

    const UserNotificationSetting = UserFields.NotificationSettingFactory(notificationSetting)
    instance = new UserNotificationSetting(props, (() => {}), {}, {}, {}, {})

    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )

    return renderedInstance
  }

  describe('getName()', () =>
    it('set it correctly', () => {
      resetModules(availableNotificationSetting, userRecord)

      expect(instance.getName()).to.equal('notification_settings.1.notification_channels')
    })
  )

  describe('getSuggestions()', () => {
    it('builds it correctly', () => {
      resetModules(availableNotificationSetting, userRecord)

      expect(instance.getSuggestions()).to.eql([
        { text: 'Call', value: 'Call' },
        { text: 'Text', value: 'Text' },
      ])
    })
  })

  describe('isShowing()', () => {
    describe('when user has any role that is supported by the notificationSetting', () => {
      it('builds it correctly', () => {
        resetModules(availableNotificationSetting, userRecord)

        expect(instance.isShowing()).to.be.true
      })
    })

    describe('when user does not have a role that is supported by the notificationSetting', () => {
      it('builds it correctly', () => {
        const userDriverRecord = { id: 1, roles: ['driver', 'rescue'] }

        resetModules(availableNotificationSetting, userDriverRecord)

        expect(instance.isShowing()).to.be.false
      })
    })

    describe('when user.roles is null', () => {
      it('builds it correctly', () => {
        const userDriverRecord = { id: 1, roles: null }

        resetModules(availableNotificationSetting, userDriverRecord)

        expect(instance.isShowing()).to.be.false
      })
    })
  })

  describe('showAll()', () => {
    it('returns false', () => {
      resetModules(availableNotificationSetting, userRecord)

      expect(instance.showAll()).to.be.false
    })
  })

  describe('onChange()', () => {
    resetModules(availableNotificationSetting, userRecord)

    it('sets it correctly', () => {
      expect(instance.getValue()).to.eql([])

      instance.onChange(['Text', 'Call'])

      expect(instance.getValue()).to.eql(['Call', 'Text'])
    })
  })

  describe('getLabel()', () => {
    describe('when notificationSetting.notification_code is not dispatched_to_driver', () => {
      resetModules(availableNotificationSetting, userRecord)

      it('returns availableNotificationSetting.description', () => {
        expect(instance.getLabel()).to.eql('New job assigned to ABC Towing')
      })
    })

    describe('when notificationSetting.notification_code is dispatched_to_driver', () => {
      const dispatchedToDriverSetting = {
        notification_code: 'dispatched_to_driver',
        available_notification_channels: ['Call', 'Text'],
        default_notification_channels: ['Text'],
        description: 'Job dispatched to driver',
        supported_roles: ['driver'],
      }

      describe('when userRecord.id is present', () => {
        describe('when userRecord.first_name is present', () => {
          const driverRecord = {
            id: 1,
            first_name: 'Jimmy',
            roles: ['driver'],
            notification_settings: [''],
          }

          describe('when user is editing himself', () => {
            it('customizes it with me', () => {
              UserStore.setUser({ id: 1 })
              resetModules(dispatchedToDriverSetting, driverRecord)

              expect(instance.getLabel()).to.eql('Job dispatched to me')
            })
          })

          describe('when user is not editing himself', () => {
            it('returns returns customized label value', () => {
              UserStore.setUser({ id: 10 })
              resetModules(dispatchedToDriverSetting, driverRecord)

              expect(instance.getLabel()).to.eql('Job dispatched to Jimmy')
            })
          })
        })

        describe('when userRecord.first_name is not present', () => {
          const driverRecord = {
            id: 1,
            first_name: '',
            roles: ['driver'],
            notification_settings: [''],
          }

          describe('when user is editing himself', () => {
            it('customizes it with me', () => {
              UserStore.setUser({ id: 1 })
              resetModules(dispatchedToDriverSetting, driverRecord)

              expect(instance.getLabel()).to.eql('Job dispatched to me')
            })
          })

          describe('when user is not editing himself', () => {
            it('returns returns "Job dispatched to this user"', () => {
              UserStore.setUser({ id: 10 })
              resetModules(dispatchedToDriverSetting, driverRecord)

              expect(instance.getLabel()).to.eql('Job dispatched to this user')
            })
          })
        })

        describe('when userRecord.first_name is undefined', () => {
          const driverRecord = {
            id: 1,
            first_name: undefined,
            roles: ['driver'],
            notification_settings: [''],
          }

          describe('when user is editing himself', () => {
            it('customizes it with me', () => {
              UserStore.setUser({ id: 1 })
              resetModules(dispatchedToDriverSetting, driverRecord)

              expect(instance.getLabel()).to.eql('Job dispatched to me')
            })
          })

          describe('when user is not editing himself', () => {
            it('returns returns "Job dispatched to this user"', () => {
              UserStore.setUser({ id: 10 })
              resetModules(dispatchedToDriverSetting, driverRecord)

              expect(instance.getLabel()).to.eql('Job dispatched to this user')
            })
          })
        })
      })

      describe('when userRecord.id is not present', () => {
        const driverRecord = {
          first_name: 'Jimmy',
          roles: ['driver'],
          notification_settings: [''],
        }

        it('returns returns "Job dispatched to this user"', () => {
          resetModules(dispatchedToDriverSetting, driverRecord)

          expect(instance.getLabel()).to.eql('Job dispatched to this user')
        })
      })
    })
  })
})
