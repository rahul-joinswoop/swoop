/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import React from 'react'

import '../../app/assets/javascripts/loader' // Mock out the set of user
import { PartnerDepartmentIdField } from 'job_fields'
import TestUtils from 'react-dom/test-utils'
import Consts from 'consts'
import Wrapper from '../support/wrapper'
import mocker from '../mocker'

describe('Job Departments', () => {
  let instance = null
  let renderedInstance = null

  const resetModules = (props) => {
    jest.resetModules()
    instance = new PartnerDepartmentIdField(props, (() => {}), {}, {}, {}, {})
    renderedInstance = TestUtils.renderIntoDocument(
      <Wrapper>
        {instance.render()}
      </Wrapper>
    )
    return renderedInstance
  }

  describe('Departments Field Partner', () => {
    beforeEach(() => mocker.mock('stores/user_store', {
      isPartner() {
        return true
      },
    }))
    describe('visibility', () => {
      it('should show if feature enabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_DEPARTMENTS) {
              return true
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.true
      })
      it('should not show if feature disabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_DEPARTMENTS) {
              return false
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.false
      })
    })
  })
  describe('Departments Field Other', () => {
    beforeEach(() => mocker.mock('stores/user_store', {
      isPartner() {
        return false
      },
    }))
    describe('visibility', () => {
      it('should not show if feature enabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_DEPARTMENTS) {
              return true
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.false
      })
      it('should not show if feature disabled', () => {
        mocker.mock('stores/feature_store', {
          isFeatureEnabled(cb, feature) {
            if (feature === Consts.FEATURES_DEPARTMENTS) {
              return false
            }

            return cb(feature)
          },
        })
        resetModules({})
        expect(instance.isShowing()).to.be.false
      })
    })
  })
  describe('Label check', () => {
    beforeEach(() => {
      mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },
      })
      return resetModules({})
    })
    it('should have label departments', () => {
      expect(instance.getLabel()).to.equal('Department')
    })
    it('should have label division for SW', () => {
      mocker.mock('stores/user_store', {
        isSouthern() {
          return true
        },
      })
      expect(instance.getLabel()).to.equal('Division')
    })
  })
})
