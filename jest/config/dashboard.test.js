/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import Consts from 'consts'

import ReactTestUtils from 'react-dom/test-utils'
import React from 'react'
import { intersection, union, without } from 'lodash' // TODO: TEST ORDER

import '../../app/assets/javascripts/loader' // Mock out the set of user
import TabConfiguration from 'components/settings/configure/my_settings/TabConfiguration'
import mocker from '../mocker'

const partnerDoneSelectable = [
  'color',
  'customer_name',
  'phone',
  'dispatcher',
  'l6vin',
  'license',
  'make',
  'model',
  'odometer',
  'year',
  'actions',
  'invoice_vehicle_category_id',
]
const partnerDoneDefault = [
  'driver',
  'truck',
  'account',
  'po',
  'completed_date',
  'drop',
  'id',
  'location',
  'service',
  'status',
]
const farmersDoneSelectable = [
  'color',
  'phone',
  'dispatcher',
  'l6vin',
  'license',
  'make',
  'model',
  'odometer',
  'year',
  'actions',
  'invoice_vehicle_category_id',
]
const farmersDoneDefault = [
  'customer_name',
  'completed_date',
  'drop',
  'id',
  'location',
  'service',
  'status',
]
const teslaDoneSelectable = [
  'completed_date',
  'color',
  'phone',
  'license',
  'make',
  'model',
  'odometer',
  'year',
  'actions',
  'invoice_vehicle_category_id',
]
const teslaDoneDefault = [
  'customer_name',
  'dispatcher',
  'drop',
  'id',
  'l6vin',
  'partner',
  'location',
  'service',
  'status',
]
const enterpriseDoneSelectable = [
  'completed_date',
  'color',
  'phone',
  'dispatcher',
  'l6vin',
  'license',
  'make',
  'model',
  'odometer',
  'year',
  'actions',
  'invoice_vehicle_category_id',
]
const enterpriseDoneDefault = [
  'customer_name',
  'drop',
  'id',
  'partner',
  'location',
  'service',
  'status',
]
const swoopDoneSelectable = [
  'l6vin',
  'location',
  'drop',
  'year',
  'make',
  'model',
  'color',
  'license',
  'phone',
  'odometer',
  'actions',
]
const swoopDoneDefault = [
  'id',
  'completed_date',
  'company',
  'customer_name',
  'service',
  'invoice_vehicle_category_id',
  'partner',
  'status',
  'dispatcher',
]
const partnerActiveSelectable = [
  'alerts',
  'wait_time',
  'total_time',
  'status_age',
  'color',
  'customer_name',
  'phone',
  'dispatcher',
  'l6vin',
  'license',
  'make',
  'model',
  'odometer',
  'year',
  'actions',
  'invoice_vehicle_category_id',
]
const partnerActiveDefault = [
  'account',
  'created_time',
  'driver',
  'drop',
  'eta2',
  'id',
  'location',
  'po',
  'service',
  'status',
  'truck',
]
const farmersActiveSelectable = [
  'alerts',
  'wait_time',
  'total_time',
  'status_age',
  'color',
  'phone',
  'dispatcher',
  'l6vin',
  'license',
  'make',
  'model',
  'odometer',
  'year',
  'actions',
  'invoice_vehicle_category_id',
]
const farmersActiveDefault = [
  'created_time',
  'customer_name',
  'drop',
  'eta',
  'id',
  'location',
  'service',
  'status',
]
const teslaActiveSelectable = [
  'alerts',
  'wait_time',
  'total_time',
  'status_age',
  'created_time',
  'color',
  'phone',
  'license',
  'make',
  'model',
  'odometer',
  'year',
  'actions',
  'invoice_vehicle_category_id',
]
const teslaActiveDefault = [
  'customer_name',
  'dispatcher',
  'drop',
  'eta',
  'id',
  'l6vin',
  'partner',
  'location',
  'service',
  'status',
]
const enterpriseActiveSelectable = [
  'alerts',
  'wait_time',
  'total_time',
  'status_age',
  'color',
  'phone',
  'dispatcher',
  'l6vin',
  'license',
  'make',
  'model',
  'odometer',
  'year',
  'actions',
  'invoice_vehicle_category_id',
]
const enterpriseActiveDefault = [
  'id',
  'eta',
  'created_time',
  'customer_name',
  'service',
  'location',
  'drop',
  'partner',
  'status',
]
const swoopActiveSelectable = [
  'l6vin',
  'created_time',
  'wait_time',
  'location',
  'drop',
  'year',
  'make',
  'model',
  'color',
  'license',
  'phone',
  'odometer',
  'actions',
]
const swoopActiveDefault = [
  'id',
  'total_time',
  'company',
  'customer_name',
  'service',
  'invoice_vehicle_category_id',
  'partner',
  'alerts',
  'eta',
  'status_age',
  'status',
  'dispatcher',
]

describe('DashboardTest', () => {
  mocker.mock('stores/feature_store', {
    isFeatureEnabled(original, name) {
      if (name === 'Job PO Number') {
        return true
      }
      return original(name)
    },
  })


  let instance = null

  const resetModules = (props) => {
    mocker.mock('stores/user_store', {})
    mocker.mock('stores/setting_store', {})
    jest.resetModules()
    instance = React.createFactory(TabConfiguration)(props)
    instance = ReactTestUtils.renderIntoDocument(instance)
    return instance
  }

  const checkDefaults = (expecting) => {
    expect(intersection(expecting, instance.getCurrentCols())).to.have.lengthOf(expecting.length)
    expect(instance.getCurrentCols()).to.have.lengthOf(expecting.length)
  }

  const checkSelectables = (expecting) => {
    const selectables = instance.getSelectableCols()
    expect(intersection(expecting, selectables)).to.have.lengthOf(expecting.length)
    expect(selectables).to.have.lengthOf(expecting.length)
  } // TODO: Test to make sure PO Works

  describe('header title', () => {
    afterAll(() => mocker.mock('stores/setting_store', {
      getSettingByKey() {
        return false
      },
    }))
    it('should say active when no tabs are enabled', () => {
      resetModules({
        active: true,
      })
      expect(instance.getTitle()).to.equal('Active Tab')
    })
    it('should say pending in progress when pending tab enabled', () => {
      mocker.mock('stores/setting_store', {
        getSettingByKey(original, key) {
          if (key === 'Pending Tab') {
            return true
          }
        },
      })
      resetModules({
        active: true,
      })
      expect(instance.getTitle()).to.equal('Pending, In-Progress Tab')
    })
    it('should say scheduled when scheduled', () => {
      mocker.mock('stores/setting_store', {
        getSettingByKey(original, key) {
          return (key === 'Pending Tab') || (key === 'Scheduled Tab')
        },
      })
      resetModules({
        active: true,
      })
      expect(instance.getTitle()).to.equal('Scheduled, Pending, In-Progress Tab')
    })
    it('should say scheduled when only scheduled', () => {
      mocker.mock('stores/setting_store', {
        getSettingByKey(original, key) {
          if (key === 'Scheduled Tab') {
            return true
          }
        },
      })
      resetModules({
        active: true,
      })
      expect(instance.getTitle()).to.equal('Scheduled, Active Tab')
    })
    it('should say done tab regardlesss', () => {
      mocker.mock('stores/setting_store', {
        getSettingByKey(original, key) {
          if (key === 'Pending Tab') {
            return true
          }

          if (key === 'Scheduled Tab') {
            return true
          }
        },
      })
      resetModules({
        active: false,
      })
      expect(instance.getTitle()).to.equal('Done Tab')
    })
  })
  describe('testing all columns', () => {
    describe('test partners', () => {
      beforeEach(() => mocker.mock('stores/user_store', {
        isPartner() {
          return true
        },

        isSwoop() {
          return false
        },

        isFleet() {
          return false
        },
      }))
      describe('test Style', () => describe('feature on', () => {
        beforeEach(() => {
          mocker.mock('stores/feature_store', {
            isFeatureEnabled(original, name) {
              if (name === Consts.FEATURES_HEAVY) {
                return true
              }
              return original(name)
            },
          })
          return resetModules({
            active: true,
          })
        })
        it('should be in the list', () => checkSelectables(union(partnerActiveSelectable, ['style'])))
      }))
      describe('test serial', () => describe('feature on', () => {
        beforeEach(() => {
          mocker.mock('stores/feature_store', {
            isFeatureEnabled(original, name) {
              if (name === Consts.FEATURES_SERIAL) {
                return true
              }
              return original(name)
            },
          })
          return resetModules({
            active: true,
          })
        })
        it('should be in the list', () => checkSelectables(union(partnerActiveSelectable, ['serial'])))
      }))
      describe('test PO', () => {
        beforeEach(() => mocker.mock('stores/feature_store', {
          isFeatureEnabled(original, name) {
            if (name === 'Job PO Number') {
              return true
            }
            return original(name)
          },
        }))
        describe('feature on', () => {
          beforeEach(() => mocker.mock('stores/feature_store', {
            isFeatureEnabled(original, name) {
              if (name === 'Job PO Number') {
                return true
              }
              return original(name)
            },
          }))
          describe('test active', () => {
            beforeEach(() => resetModules({
              active: true,
            }))
            it('should be in defaults', () => checkDefaults(partnerActiveDefault))
          })
          describe('test done', () => {
            beforeEach(() => resetModules({
              active: false,
            }))
            it('should be in selectables', () => {
              checkDefaults(partnerDoneDefault)
              return checkSelectables(partnerDoneSelectable)
            })
          })
        })
        describe('feature off', () => {
          beforeEach(() => mocker.mock('stores/feature_store', {
            isFeatureEnabled(original, name) {
              if (name === 'Job PO Number') {
                return false
              }
              return original(name)
            },
          }))
          describe('test active', () => {
            beforeEach(() => resetModules({
              active: true,
            }))
            it('should not be in defaults', () => checkDefaults(without(partnerActiveDefault, 'po')))
          })
          describe('test done', () => {
            beforeEach(() => resetModules({
              active: false,
            }))
            it('should not be in selectables', () => checkSelectables(without(partnerDoneSelectable, 'po')))
          })
        })
      })
      describe('general tests', () => {
        beforeEach(() => mocker.mock('stores/feature_store', {
          isFeatureEnabled(original, name) {
            if (name === 'Job PO Number') {
              return true
            }
            return original(name)
          },
        }))
        describe('test active', () => {
          beforeEach(() => resetModules({
            active: true,
          }))
          it('defaults', () => checkDefaults(partnerActiveDefault))
          it('addables', () => checkSelectables(partnerActiveSelectable))
        })
        describe('test done', () => {
          beforeEach(() => resetModules({
            active: false,
          }))
          it('defaults', () => checkDefaults(partnerDoneDefault))
          it('addables', () => checkSelectables(partnerDoneSelectable))
        })
      })
    })
    describe('test farmers', () => {
      beforeEach(() => mocker.mock('stores/user_store', {
        isPartner() {
          return false
        },

        isSwoop() {
          return false
        },

        isFleet() {
          return true
        },

        isFarmers() {
          return true
        },
      }))
      describe('test active', () => {
        beforeEach(() => resetModules({
          active: true,
        }))
        it('defaults', () => checkDefaults(farmersActiveDefault))
        it('addables', () => checkSelectables(farmersActiveSelectable))
      })
      describe('test done', () => {
        beforeEach(() => resetModules({
          active: false,
        }))
        it('defaults', () => checkDefaults(farmersDoneDefault))
        it('addables', () => checkSelectables(farmersDoneSelectable))
      })
    })
    describe('test tesla', () => {
      beforeEach(() => mocker.mock('stores/user_store', {
        isPartner() {
          return false
        },

        isSwoop() {
          return false
        },

        isFleet() {
          return true
        },

        isTesla() {
          return true
        },

        isFleetInHouse() {
          return true
        },

        isFarmers() {
          return false
        },
      }))
      describe('test active', () => {
        beforeEach(() => resetModules({
          active: true,
        }))
        it('defaults', () => checkDefaults(teslaActiveDefault))
        it('addables', () => checkSelectables(teslaActiveSelectable))
      })
      describe('test done', () => {
        beforeEach(() => resetModules({
          active: false,
        }))
        it('defaults', () => checkDefaults(teslaDoneDefault))
        it('addables', () => checkSelectables(teslaDoneSelectable))
      })
    })
    describe('test enterprise', () => {
      beforeEach(() => mocker.mock('stores/user_store', {
        isPartner() {
          return false
        },

        isSwoop() {
          return false
        },

        isFleet() {
          return true
        },

        isTesla() {
          return false
        },

        isFarmers() {
          return false
        },

        isEnterprise() {
          return true
        },

        isFleetInHouse() {
          return true
        },
      }))
      describe('test active', () => {
        beforeEach(() => resetModules({
          active: true,
        }))
        it('defaults', () => checkDefaults(enterpriseActiveDefault))
        it('addables', () => checkSelectables(enterpriseActiveSelectable))
      })
      describe('test done', () => {
        beforeEach(() => resetModules({
          active: false,
        }))
        it('defaults', () => checkDefaults(enterpriseDoneDefault))
        it('addables', () => checkSelectables(enterpriseDoneSelectable))
      })
    })
    describe('test swoop', () => {
      beforeEach(() => mocker.mock('stores/user_store', {
        isPartner() {
          return false
        },

        isSwoop() {
          return true
        },

        isFleet() {
          return false
        },
      }))
      describe('test active', () => {
        beforeEach(() => resetModules({
          active: true,
        }))
        it('defaults', () => checkDefaults(swoopActiveDefault))
        it('addables', () => checkSelectables(swoopActiveSelectable))
      })
      describe('test done', () => {
        beforeEach(() => resetModules({
          active: false,
        }))
        it('defaults', () => checkDefaults(swoopDoneDefault))
        it('addables', () => checkSelectables(swoopDoneSelectable))
      })
    })
  })
})
