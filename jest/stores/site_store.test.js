import '../../app/assets/javascripts/loader'
import SiteStore from 'stores/site_store'

describe('SiteStore', () => {
  describe('.getBySiteCode', () => {
    it('returns the site when it has a match', () => {
      const site = {
        id: 1,
        type: 'FleetSite',
        site_code: '12345T',
      }

      SiteStore._add(site)

      expect(SiteStore.getBySiteCode(site.site_code)).to.eq(site)
    })

    it('returns undefined when it does not have a match', () => {
      const site = {
        id: 1,
        type: 'FleetSite',
        site_code: '12345T',
      }

      SiteStore._add(site)

      expect(SiteStore.getBySiteCode('009988')).to.be.undefined
    })

    it('returns null when siteCode is null', () => {
      const site = {
        id: 1,
        type: 'FleetSite',
        site_code: '12345T',
      }

      SiteStore._add(site)

      expect(SiteStore.getBySiteCode(null)).to.be.null
    })

    it('returns null when siteCode is undefined', () => {
      const site = {
        id: 1,
        type: 'FleetSite',
        site_code: '12345T',
      }

      SiteStore._add(site)

      expect(SiteStore.getBySiteCode(undefined)).to.be.null
    })
  })
})
