/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import '../../app/assets/javascripts/loader'

import Dispatcher from 'lib/swoop/dispatcher'
import ErrorStore from 'stores/error_store'

describe('Error Store tests', () => describe('Error Store', () => {
  it('Start out with no errors or refresh', () => {
    expect(ErrorStore.getError()).to.be.null
  })
  it('Should show error if sending error', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      data: {},
    })
    expect(ErrorStore.getError()).to.equal('Error: Unknown error')
  })
  it('Should be able to clear errors', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      data: {},
    })
    Dispatcher.send(ErrorStore.CLEAR_ERRORS, {
      data: {},
    })
    expect(ErrorStore.getError()).to.be.null
  })
  it('Should be able to user responseJSON message', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      errorThrown: 'errorThrown',
      textStatus: 'textStatus',
      data: {
        responseJSON: {
          message: 'Hello',
        },
        statusText: 'statusText',
        textStatus: 'textStatus',
      },
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/Hello/)
  })
  it('Should be able to use statusText', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      errorThrown: 'errorThrown',
      textStatus: 'textStatus',
      data: {
        statusText: 'statusText',
      },
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/statusText/)
  })
  it('Should be able to use textStatus', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      errorThrown: 'errorThrown',
      textStatus: 'textStatus',
      data: {},
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/textStatus/)
  })
  it('Should be able to use errorThrown', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      errorThrown: 'errorThrown',
      data: {},
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/errorThrown/)
  })
  it('Should always show status if not responseJSON', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      errorThrown: 'errorThrown',
      textStatus: 'textStatus',
      data: {
        status: 'status',
        statusText: 'statusText',
        textStatus: 'textStatus',
      },
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/status/)
  })
  it('Status 0 and textStatus error should say problem with server', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      errorThrown: 'errorThrown',
      data: {
        status: 0,
        statusText: 'error',
        textStatus: 'textStatus',
      },
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/Problem connecting to server/)
  })
  it('textStatus timeout should say request timeout', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      errorThrown: 'errorThrown',
      textStatus: 'timeout',
      data: {
        status: 0,
        textStatus: 'textStatus',
      },
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/Request Timeout/)
  })
  it('parsererror  should say problem parsing', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      errorThrown: 'errorThrown',
      textStatus: 'parsererror',
      data: {
        status: 0,
        statusText: 'parsererror',
        textStatus: 'textStatus',
      },
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/Problem parsing the response/)
  })
  it('should be able to forceMsg', () => {
    Dispatcher.send(ErrorStore.SHOW_ERROR, {
      forceMsg: 'forceMsg',
      errorThrown: 'errorThrown',
      textStatus: 'parsererror',
      data: {
        status: 0,
        statusText: 'parsererror',
        textStatus: 'textStatus',
      },
    })
    expect(JSON.stringify(ErrorStore.getError())).to.match(/forceMsg/)
  })
}))
