/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import '../../app/assets/javascripts/loader'

import Dispatcher from 'lib/swoop/dispatcher'
import InvoiceStore from 'stores/invoice_store'

describe('Invoice Store tests', () => describe('Invoice Store', () => {
  beforeEach(() => {
    const invoices = InvoiceStore.getInvoices()
    return invoices.map(invoice => InvoiceStore.deleteInvoice(invoice))
  })
  describe('setting cost', () => it('Should properly set cost and ignore deleted items', () => {
    const invoice = {
      line_items: [
        {
          net_amount: 1,
        },
        {
          description: 'Tax',
          net_amount: 2,
        },
        {
          net_amount: 3,
          deleted_at: true,
        },
        {
          description: 'Tax',
          deleted_at: true,
          net_amount: 4,
        },
      ],
    }

    InvoiceStore._setCost(invoice)

    expect(invoice.cost).to.equal(3)
  }))
  it('Should properly add an invoice with cost set', () => {
    let invoice = {
      id: 1,
      line_items: [
        {
          net_amount: 1,
        },
        {
          description: 'Tax',
          net_amount: 2,
        },
        {
          net_amount: 3,
          deleted_at: true,
        },
        {
          description: 'Tax',
          deleted_at: true,
          net_amount: 4,
        },
      ],
    }
    InvoiceStore.addInvoice(invoice)
    invoice = InvoiceStore.getInvoiceById(1)
    expect(invoice.cost).to.equal(3)
  })
  it('Shouldnt add if older', () => {
    let invoice = {
      id: 1,
      updated_at: 2,
      line_items: [
        {
          net_amount: 1,
        },
        {
          description: 'Tax',
          net_amount: 2,
        },
        {
          net_amount: 3,
          deleted_at: true,
        },
        {
          description: 'Tax',
          deleted_at: true,
          net_amount: 4,
        },
      ],
    }
    InvoiceStore.addInvoice(invoice)
    invoice = {
      id: 1,
      updated_at: 1,
      line_items: [
        {
          net_amount: 2,
        },
        {
          description: 'Tax',
          net_amount: 2,
        },
        {
          net_amount: 3,
          deleted_at: true,
        },
        {
          description: 'Tax',
          deleted_at: true,
          net_amount: 4,
        },
      ],
    }
    InvoiceStore.addInvoice(invoice)
    invoice = InvoiceStore.getInvoiceById(1)
    expect(invoice.cost).to.equal(3)
  })
  it('Should properly delete an invoice', () => {
    let invoice = {
      id: 1,
      line_items: [
        {
          net_amount: 1,
        },
        {
          description: 'Tax',
          net_amount: 2,
        },
        {
          net_amount: 3,
          deleted_at: true,
        },
        {
          description: 'Tax',
          deleted_at: true,
          net_amount: 4,
        },
      ],
    }
    InvoiceStore.addInvoice(invoice)
    InvoiceStore.deleteInvoice(invoice)
    invoice = InvoiceStore.getInvoiceById(1)
    expect(invoice).to.be.null
  })
  it('Should update without overwritting', () => {
    let invoice = {
      id: 1,
      check: true,
      line_items: [
        {
          net_amount: 1,
        },
        {
          description: 'Tax',
          net_amount: 2,
        },
        {
          net_amount: 3,
          deleted_at: true,
        },
        {
          description: 'Tax',
          deleted_at: true,
          net_amount: 4,
        },
      ],
    }
    InvoiceStore.addInvoice(invoice)
    InvoiceStore.updateInvoice({
      id: 1,
      check2: true,
    })
    invoice = InvoiceStore.getInvoiceById(1)
    expect(invoice.check).to.be.true
    expect(invoice.check2).to.be.true
  })
  it('Should be able to get an invoice by uuid', () => {
    let invoice = {
      id: 1,
      uuid: 'uuid',
      line_items: [
        {
          net_amount: 1,
        },
        {
          description: 'Tax',
          net_amount: 2,
        },
        {
          net_amount: 3,
          deleted_at: true,
        },
        {
          description: 'Tax',
          deleted_at: true,
          net_amount: 4,
        },
      ],
    }
    InvoiceStore.addInvoice(invoice)
    invoice = InvoiceStore.getInvoiceByUUID('uuid')
    expect(invoice.id).to.equal(1)
  })
  it('should thrown error on unknown event', () => {
    expect(() => Dispatcher.send()).toThrow(new Error('empty Event'))
  })
}))
