import DownloadStore from 'stores/download_store'
import Utils from 'utils'
import Api from 'api'

jest.mock('api')
jest.mock('utils')

describe('Download Store tests', () => {
  // TODO: Test to make sure PO Works
  beforeEach(() => {
    Api.runReport.mockImplementation((one, two, three, four, callbacks) =>
      callbacks.success({
        id: 2,
        report_id: 1,
        state: 'Created',
      })
    )
    DownloadStore.reset()
  })
  describe('downloading', () => {
    beforeEach(() => Utils.downloadFile.mockReset())
    it('Should not download if it doesnt know about report', () => {
      const added = DownloadStore.addReportResult({
        state: 'Finished',
      })
      expect(added).to.be.false
    })
    it('Should download if it does know about the report', () => {
      DownloadStore.downloadReport(null, {}, null)
      DownloadStore.addReportResult({
        id: 2,
        state: 'Finished',
      })
      expect(Utils.downloadFile).toHaveBeenCalled()
    })
  })
  describe('fetch report result', () => {
    it('should call fetchReportResult', () => {
      const testFunc = jest.fn()
      const testFunc2 = jest.fn()

      Api.objectsRequest.mockImplementation((one, two, three, callbacks) => {
        testFunc()
        return callbacks.success({
          id: 1,
        })
      })
      DownloadStore.fetchReportResult(1, testFunc2)
      expect(testFunc).toHaveBeenCalled()
      expect(testFunc2).toHaveBeenCalled()
    })
    it('should call utils error on fail', () => {
      const testFunc = jest.fn()
      const testFunc2 = jest.fn()
      Api.objectsRequest.mockImplementation((one, two, three, callbacks) => {
        testFunc()
        return callbacks.error()
      })
      DownloadStore.fetchReportResult(1, testFunc2)
      expect(Utils.sendNotification).toHaveBeenCalled()
      expect(Utils.handleError).toHaveBeenCalled()
    })
  })
  describe('last run report', () => {
    it('last run report should update on running a new report', () => {
      expect(DownloadStore.getLastReportId()).to.be.undefined
      DownloadStore.downloadReport(1, {}, null)
      expect(DownloadStore.getLastReportId(1)).to.equal(2)
      Api.runReport.mockImplementation((one, two, three, four, callbacks) =>
        callbacks.success({
          id: 3,
          report_id: 1,
          state: 'Created',
        })
      )

      DownloadStore.downloadReport(1, {}, null)
      expect(DownloadStore.getLastReportId(1)).to.equal(3)
    })
    it('last run report by name should update on running a new report', () => {
      expect(DownloadStore.getLastReportId()).to.be.undefined
      DownloadStore.downloadReport(1, {}, null, Api.runReport, 'testName')
      expect(DownloadStore.getLastReportStateByName('testName')).to.equal('Created')
      Api.runReport.mockImplementation((one, two, three, four, callbacks) =>
        callbacks.success({
          id: 3,
          report_id: 1,
          state: 'Running',
        })
      )
      DownloadStore.downloadReport(1, {}, null, Api.runReport, 'testName')
      expect(DownloadStore.getLastReportStateByName('testName')).to.equal('Running')
    })
    it('last run report by name should update state if it updates', () => {
      expect(DownloadStore.getLastReportId()).to.be.undefined
      DownloadStore.downloadReport(1, {}, null, Api.runReport, 'testName')
      expect(DownloadStore.getLastReportStateByName('testName')).to.equal('Created')
      DownloadStore.addReportResult({
        id: 2,
        report_id: 1,
        state: 'Running',
      })
      expect(DownloadStore.getLastReportStateByName('testName')).to.equal('Running')
      DownloadStore.addReportResult({
        id: 2,
        report_id: 1,
        state: 'Finished',
      })
      expect(DownloadStore.getLastReportStateByName('testName')).to.equal('Finished')
    })
  })
  describe('fake states should work', () => {
    it('Should show error if it fails', () => {
      Api.runReport.mockImplementation((one, two, three, four, callbacks) =>
        callbacks.error()
      )
      DownloadStore.downloadReport(1, {}, null, Api.runReport, 'testName')
      expect(DownloadStore.getLastReportState(1)).to.equal('error')
      expect(DownloadStore.getLastReportStateByName('testName')).to.equal('error')
    })
    it('should show Loading if its loading', () => {
      Api.runReport.mockReturnValue({})
      DownloadStore.downloadReport(1, {}, null, Api.runReport, 'testName')
      expect(DownloadStore.getLastReportState(1)).to.equal('Loading')
      expect(DownloadStore.getLastReportStateByName('testName')).to.equal('Loading')
    })
  })
  it('should show report state otherwise', () => {
    Api.runReport.mockImplementation((one, two, three, four, callbacks) =>
      callbacks.success({
        id: 3,
        report_id: 1,
        state: 'Created',
      })
    )
    DownloadStore.downloadReport(1, {}, null, Api.runReport, 'testName')
    expect(DownloadStore.getLastReportState(1)).to.equal('Created')
    expect(DownloadStore.getLastReportStateByName('testName')).to.equal('Created')
  })
  // TODO - can't get this working with jest's mock, why?
  describe.skip('polling', () => {
    const clearFunc = jest.fn()
    const setFunc = jest.fn()
    let DScallback = null
    const INTERVAL_ID = 5
    beforeEach(() => {
      jest.resetAllMocks()
      global.clearInterval = clearFunc

      global.setInterval = (callback) => {
        DScallback = callback
        callback()
        setFunc()
        return INTERVAL_ID
      }
    })
    it('Should stop polling when report is finished', () => {
      Api.objectsRequest.mockImplementationOnce((one, two, three, callbacks) =>
        callbacks.success({
          id: 2,
          report_id: 1,
          state: 'Running',
        })
      ).mockImplementationOnce((one, two, three, callbacks) =>
        callbacks.success({
          id: 2,
          report_id: 1,
          state: 'Finished',
        })
      )
      expect(setFunc).not.toHaveBeenCalled()
      DownloadStore.downloadReport(1, {}, null)
      expect(setFunc).toHaveBeenCalled()
      expect(clearFunc).not.toHaveBeenCalled()
      DScallback()
      expect(clearFunc).not.toHaveBeenCalled()
      DScallback()
      expect(clearFunc).toHaveBeenCalledWith(INTERVAL_ID)
    })
    it('Should stop polling when report is Failed', () => {
      Api.objectsRequest.mockImplementationOnce((one, two, three, callbacks) =>
        callbacks.success({
          id: 2,
          report_id: 1,
          state: 'Running',
        })
      ).mockImplementationOnce((one, two, three, callbacks) =>
        callbacks.success({
          id: 2,
          report_id: 1,
          state: 'Failed',
        })
      )
      expect(setFunc).not.toHaveBeenCalled()
      DownloadStore.downloadReport(1, {}, null)
      expect(setFunc).toHaveBeenCalled()
      expect(clearFunc).not.toHaveBeenCalled()
      DScallback()
      expect(clearFunc).not.toHaveBeenCalled()
      DScallback()
      expect(clearFunc).toHaveBeenCalledWith(INTERVAL_ID)
    })
    it('Should stop polling if report is lost', () => {
      Api.objectsRequest.mockImplementation((one, two, three, callbacks) =>
        callbacks.success({
          id: 2,
          report_id: 1,
          state: 'Running',
        })
      )
      expect(setFunc).not.toHaveBeenCalled()
      DownloadStore.downloadReport(1, {}, null)
      expect(setFunc).toHaveBeenCalled()
      expect(clearFunc).not.toHaveBeenCalled()
      DScallback()
      expect(clearFunc).not.toHaveBeenCalled()
      DownloadStore.reset()
      DScallback()
      expect(clearFunc).toHaveBeenCalledWith(INTERVAL_ID)
    })
    it('Should keep polling if report is still there', () => {
      Api.objectsRequest.mockImplementationOnce((one, two, three, callbacks) =>
        callbacks.success({
          id: 2,
          report_id: 1,
          state: 'Running',
        })
      ).mockImplementationOnce((one, two, three, callbacks) =>
        callbacks.success({
          id: 2,
          report_id: 1,
          state: 'Running',
        })
      )
      expect(setFunc).not.toHaveBeenCalled()
      DownloadStore.downloadReport(1, {}, null)
      expect(setFunc).toHaveBeenCalled()
      expect(clearFunc).not.toHaveBeenCalled()
      DScallback()
      expect(clearFunc).not.toHaveBeenCalled()
      DScallback()
      expect(clearFunc).not.toHaveBeenCalledWith(INTERVAL_ID)
    })
  })
  describe('download report', () => it('should allow taking an aribtray api call', () => {
    const call = jest.fn()
    DownloadStore.downloadReport(1, {}, null, call)
    expect(call).toHaveBeenCalled()
  }))
  describe('triggers', () => {
    const downloadCallback = jest.fn()
    const changeResultCallback = jest.fn()
    beforeEach(() => {
      downloadCallback.mockReset()
      changeResultCallback.mockReset()
      return DownloadStore.reset()
    })
    it('should trigger on download', () => {
      DownloadStore.bind(DownloadStore.DOWNLOADING_REPORT, downloadCallback)
      DownloadStore.downloadIfNeeded({
        state: 'Running',
      })
      expect(downloadCallback).not.toHaveBeenCalled()
      DownloadStore.downloadIfNeeded({
        state: 'Finished',
      })
      expect(downloadCallback).toHaveBeenCalled()
    })
    it('should trigger on change report result', () => {
      DownloadStore.bind(DownloadStore.CHANGED_REPORT_RESULT, changeResultCallback)
      DownloadStore.downloadReport(1, {}, null)
      expect(changeResultCallback).toHaveBeenCalled()
      changeResultCallback.mockReset()
      expect(changeResultCallback).not.toHaveBeenCalled()
      DownloadStore.addReportResult({
        id: 2,
        state: 'Finished',
      })
      expect(changeResultCallback).toHaveBeenCalled()
    })
    it('should not trigger if silent', () => {
      DownloadStore.bind(DownloadStore.CHANGED_REPORT_RESULT, changeResultCallback)
      DownloadStore.downloadReport(1, {}, null)
      expect(changeResultCallback).toHaveBeenCalled()
      changeResultCallback.mockReset()
      expect(changeResultCallback).not.toHaveBeenCalled()
      DownloadStore.addReportResult(
        {
          id: 2,
          state: 'Finished',
        },
        true,
      )
      expect(changeResultCallback).not.toHaveBeenCalled()
    })
    it('should not trigger if unknwon report added', () => {
      DownloadStore.bind(DownloadStore.CHANGED_REPORT_RESULT, changeResultCallback)
      DownloadStore.downloadReport(1, {}, null)
      expect(changeResultCallback).toHaveBeenCalled()
      changeResultCallback.mockReset()
      expect(changeResultCallback).not.toHaveBeenCalled()
      DownloadStore.addReportResult({
        id: 3,
        state: 'Finished',
      })
      expect(changeResultCallback).not.toHaveBeenCalled()
    })
    it('should not trigger if state doesnt update', () => {
      Api.runReport.mockImplementation((one, two, three, four, callbacks) =>
        callbacks.success({
          id: 2,
          report_id: 1,
          state: 'Created',
        })
      )
      DownloadStore.bind(DownloadStore.CHANGED_REPORT_RESULT, changeResultCallback)
      DownloadStore.downloadReport(1, {}, null)
      expect(changeResultCallback).toHaveBeenCalled()
      changeResultCallback.mockReset()
      expect(changeResultCallback).not.toHaveBeenCalled()
      DownloadStore.addReportResult({
        id: 2,
        report_id: 1,
        state: 'Created',
      })
      expect(changeResultCallback).not.toHaveBeenCalled()
      DownloadStore.addReportResult({
        id: 2,
        report_id: 1,
        state: 'Running',
      })
      expect(changeResultCallback).toHaveBeenCalled()
      changeResultCallback.mockReset()
      DownloadStore.addReportResult({
        id: 2,
        report_id: 1,
        state: 'Running',
      })
      expect(changeResultCallback).not.toHaveBeenCalled()
    })
  })
})
