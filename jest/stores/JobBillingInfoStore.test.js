import jquery from 'jquery'
import Consts from 'consts'
import JobBillingInfoStore from 'stores/JobBillingInfoStore'

describe('JobBillingInfoStore', () => {
  afterEach(() => {
    jest.restoreAllMocks()
  })

  it('no concurrent requests', () => {
    let deletePromise

    jest.spyOn(jquery, 'ajax').
    // mock implmentation for first job
    mockImplementationOnce((data) => {
      expect(data).toMatchObject({
        type: 'GET',
        url: '/api/v1/partner/jobs/1/billing_info',
        data: null,
      })

      return {
        fail: () => ({
          always: func => deletePromise = func,
        }),
      }
    }).
    // mock implmentation for second job
    mockImplementationOnce((data) => {
      expect(data).toMatchObject({
        type: 'GET',
        url: '/api/v1/partner/jobs/2/billing_info',
        data: null,
      })

      return {
        fail: () => ({ always: () => {} }),
      }
    }).
    // mock implmentation for first job second call
    mockImplementationOnce((data) => {
      expect(data).toMatchObject({
        type: 'GET',
        url: '/api/v1/partner/jobs/1/billing_info',
        data: null,
      })

      return {
        fail: () => ({ always: () => {} }),
      }
    })

    const jobId = 1

    const promise1 = JobBillingInfoStore.getBillingInfo(jobId)
    const promise2 = JobBillingInfoStore.getBillingInfo(jobId)
    const promise3 = JobBillingInfoStore.getBillingInfo(2)

    expect(promise1).toBe(promise2)
    expect(promise1).not.toBe(promise3)

    deletePromise()

    const promise4 = JobBillingInfoStore.getBillingInfo(jobId)
    expect(promise1).not.toBe(promise4)
  })

  it('checks isVcc', () => {
    expect(JobBillingInfoStore.isVcc({ billing_type: Consts.JOB.BILLING_TYPE.VCC })).toBe(true)
    expect(JobBillingInfoStore.isVcc({ billing_type: Consts.JOB.BILLING_TYPE.ACH })).toBe(false)
  })
})
