import { keys } from 'lodash'
import Edmunds from 'stores/edmunds_store'
import Api from 'api'

jest.mock('api')

const sampleData = [
  {
    id: 153,
    name: 'Clark Michigan',
    models: [
      {
        id: 4771,
        name: '75C',
        years: [2016],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 4770,
        name: '55C',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 4769,
        name: '475C',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 4768,
        name: '45C',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 4767,
        name: '35B',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 4766,
        name: '275C',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 4765,
        name: '175C',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 4764,
        name: '125C',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
    ],
  },
  {
    id: 154,
    name: 'CUKUROVA',
    models: [
      {
        id: 4772,
        name: '940',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
    ],
  },
  {
    id: 155,
    name: 'Ranger',
    models: [
      {
        id: 5280,
        name: '979',
        years: [],
        vehicle_type: 'Digger',
      },
      {
        id: 5279,
        name: '969',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5278,
        name: '958',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5277,
        name: '938',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5276,
        name: '936',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5275,
        name: '918',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5274,
        name: '915',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
    ],
  },
  {
    id: 156,
    name: 'Schaffer',
    models: [
      {
        id: 5295,
        name: '9300Z',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5294,
        name: '9100ZS',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5293,
        name: '9100Z',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5292,
        name: '5390Z',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5291,
        name: '5370Z',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5290,
        name: '5058ZS',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5289,
        name: '5050Z',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5288,
        name: '3150',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5287,
        name: '2345',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5286,
        name: '2027',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5285,
        name: '2026',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5284,
        name: '2022(S)',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
    ],
  },
  {
    id: 157,
    name: 'TCM',
    models: [
      {
        id: 5304,
        name: 'L9-2',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5303,
        name: 'L6-2',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5302,
        name: 'L5-2',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5301,
        name: 'L4-2',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5300,
        name: 'L3-2',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5299,
        name: 'L20',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5298,
        name: 'L16',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5297,
        name: 'L13',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5296,
        name: '840',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
    ],
  },
  {
    id: 158,
    name: 'XCMG',
    models: [
      {
        id: 5383,
        name: 'LW820G',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5382,
        name: 'LW620G',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5381,
        name: 'LW540G',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
      {
        id: 5380,
        name: 'LW320G',
        years: [],
        vehicle_type: 'Wheel Loader',
      },
    ],
  },
]

describe('EdmundsStore tests', () => {
  describe('Inital Load', () => {
    it('should call comapnies for root swoop', () => {
      Api.vehicleMakes.mockReturnValue([])
      Api.vehicleExtraTypes.mockReturnValue([])
      Edmunds.setupEdmunds()
      expect(Api.vehicleMakes).toHaveBeenCalled()
      expect(Api.vehicleExtraTypes).toHaveBeenCalled()
    })
  })
  describe('base calls', () => {
    beforeAll(() => {
      Api.vehicleMakes.mockImplementation(res =>
        res.success(sampleData)
      )
      Edmunds.setupEdmunds()
    })
    it('should return proper makes', () => {
      const makes = Edmunds.getMakes()
      expect(keys(makes)).to.have.lengthOf(6)
    })
    it('should return proper make names', () => {
      const makeNames = Edmunds.getMakeNames()
      expect(makeNames).to.have.lengthOf(6)
    })
    it('should return proper make names given a vehicle type', () => {
      let makeNames = Edmunds.getMakeNames('Digger')
      expect(makeNames).to.have.lengthOf(1)
      makeNames = Edmunds.getMakeNames('Wheel Loader')
      expect(makeNames).to.have.lengthOf(6)
    })
    it('should return All when there is no match', () => {
      const makeNames = Edmunds.getMakeNames('Othery')
      expect(makeNames).to.have.lengthOf(6)
    })
    it('should return All type names properly', () => {
      let typeNames = Edmunds.getTypeNames()
      expect(typeNames).to.have.lengthOf(2)
      typeNames = Edmunds.getTypeNames('CUKUROVA')
      expect(typeNames).to.have.lengthOf(1)
      typeNames = Edmunds.getTypeNames('Ranger')
      expect(typeNames).to.have.lengthOf(2)
      typeNames = Edmunds.getTypeNames('No Match')
      expect(typeNames).to.have.lengthOf(2)
    })
    it('should return Models properly', () => {
      let models = Edmunds.getModelNamesByMakeName()
      expect(models).to.have.lengthOf(0)
      models = Edmunds.getModelNamesByMakeName('CUKUROVA')
      expect(models).to.have.lengthOf(1)
      models = Edmunds.getModelNamesByMakeName('Ranger')
      expect(models).to.have.lengthOf(7)
      models = Edmunds.getModelNamesByMakeName('Ranger', 'Digger')
      expect(models).to.have.lengthOf(1)
    })
    it('should return Years properly', () => {
      let years = Edmunds.getYearsByName('Clark Michigan', '75C')
      expect(years).to.have.lengthOf(1)
      years = Edmunds.getYearsByName('invalid', 'invalid')
      expect(years).to.have.lengthOf(31)
      years = Edmunds.getYearsByName('Clark Michigan', '55C')
      expect(years).to.have.lengthOf(31)
    })
  })
})
