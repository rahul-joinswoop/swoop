import '../../app/assets/javascripts/loader' // Mock out the set of user
import SiteStore from 'stores/site_store'

describe('GeneralStore tests', () => describe('getAddressWithSiteName()', () => {
  it('returns an empty string when location param has no address', () => {
    expect(
      SiteStore.getAddressWithSiteName({
        address: null,
      }),
    ).to.be.empty
  })
  it('includes the name of the site associated with the address', () => {
    SiteStore.get = () => 'Za site'

    const siteName = SiteStore.getAddressWithSiteName({
      site_id: 6,
    }).substr(0, 8)
    expect(siteName).to.equal('Za site ')
  })
}))
