import UsersStore from 'stores/users_store'
import UserStore from 'stores/user_store'

jest.mock('stores/user_store')

describe('UsersStore', () => {
  beforeEach(() => {
    jest.resetAllMocks()

    UserStore.getUser.mockReturnValue({ company_id: 10 })
    UsersStore._clearSortedDispatcherCache()
    UsersStore._add({
      id: 1, full_name: 'First User', roles: ['dispatcher'], company_id: 10,
    })
    UsersStore._add({
      id: 2, full_name: 'Second User', roles: ['admin'], company_id: 10,
    })
    UsersStore._add({
      id: 3, full_name: 'Third User', roles: ['answering_service'], company_id: 10,
    })
    UsersStore._add({
      id: 4, full_name: 'Fourth User', roles: ['driver'], company_id: 10,
    })
  })

  describe('#_add', () => {
    test('it calls UserStore.updateUser', () => {
      const data = { id: 1 }
      UsersStore._add(data)

      expect(UserStore.updateUser).toHaveBeenCalled()
    })
  })

  describe('#_update', () => {
    test('it calls UserStore.updateUser', () => {
      const data = { id: 1 }
      UsersStore._update(data)

      expect(UserStore.updateUser).toHaveBeenCalled()
    })
  })

  describe('#isEditable', () => {
    test('it returns true', () => {
      expect(UsersStore.isEditable()).to.be.ok
    })
  })

  describe('#getUrl', () => {
    test('it returns specific URL for users', () => {
      expect(UsersStore.getUrl()).to.equal('users?include_deleted=true')
    })
  })

  describe('#getSortedDispatcherIds', () => {
    test('it returns the current users sorted by full_name', () => {
      expect(UsersStore.getSortedDispatcherIds()).to.deep.equal([1, 2, 3])
    })

    describe('when User is Swoop', () => {
      test('it only returns Swoop dispatchers', () => {
        UserStore.isSwoop.mockReturnValue(true)
        UserStore.getUser.mockReturnValue({
          id: 1,
          roles: ['root'],
          company_id: 1,
        })

        UsersStore._clearSortedDispatcherCache()
        UsersStore._add({
          id: 10, full_name: 'Swoop user', roles: ['swoop_dispatcher'], company_id: 1,
        })
        UsersStore._add({
          id: 11, full_name: 'Another Swoop user', roles: ['answering_service'], company_id: 1,
        })

        expect(UsersStore.getSortedDispatcherIds()).to.deep.equal([11, 10])

        UsersStore._remove({ id: 10 })
        UsersStore._remove({ id: 11 })
      })
    })
  })

  describe('#getSortedDrivers', () => {
    test('it returns only drivers sorted by full_name', () => {
      UsersStore._clearSortedDispatcherCache()
      UsersStore._add({
        id: 40, full_name: 'Another Driver', roles: ['driver'], company_id: 10,
      })

      expect(UsersStore.getSortedDriverIds()).to.deep.equal([40, 4])

      UsersStore._remove({ id: 40 })
    })
  })

  describe('#getDriversForStore', () => {
    test('it returns an object with drivers sorted by full_name', () => {
      UsersStore._clearSortedDispatcherCache()
      UsersStore._add({
        id: 40, full_name: 'Another Driver', roles: ['driver'], company_id: 10,
      })

      expect(UsersStore.getDriversForStore()).to.deep.equal({
        40: { name: 'Another Driver' },
        4: { name: 'Fourth User' },
      })

      UsersStore._remove({ id: 40 })
    })
  })
})
