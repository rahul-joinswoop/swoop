import AuctionStore from 'stores/auction_store'
import UserStore from 'stores/user_store'

beforeAll(() => {
  AuctionStore.getAuctions = () => ({
    1: {
      id: 1,
      status: 'live',
      job_id: 10,
      bids: [
        {
          id: 40, status: 'submitted', submitted_eta: 20, company_id: 1,
        },
      ],
      target_eta: 40,
      expires_at: '2017-07-13T04:25:29.990Z',
    },
    2: {
      id: 2,
      status: 'unsuccessful',
      job_id: 20,
      bids: [
        {
          id: 50, status: 'expired', submitted_eta: null, company_id: 2,
        },
      ],
      expires_at: '2017-07-14T09:25:29.990Z',
    },
    3: {
      id: 3,
      status: 'successful',
      job_id: 30,
      bids: [
        {
          id: 55, status: 'auto_rejected', submitted_eta: 40, company_id: 3,
        },
      ],
      expires_at: '2017-07-14T09:25:29.990Z',
    },
    4: {
      id: 4,
      status: 'successful',
      job_id: 40,
      bids: [
        {
          id: 60, status: 'provider_rejected', submitted_eta: null, company_id: 4,
        },
      ],
      expires_at: '2017-07-14T09:25:29.990Z',
    },
  })
})

describe('getAuctionById(id)', () => {
  describe('when id is null', () => {
    test('it returns null', () => {
      expect(AuctionStore.getAuctionById(null)).to.be.null
    })
  })

  describe('when id is Number', () => {
    test('it returns the respective Auction', () => {
      expect(AuctionStore.getAuctionById(1)).to.deep.equal({
        id: 1,
        status: 'live',
        job_id: 10,
        bids: [
          {
            id: 40, status: 'submitted', submitted_eta: 20, company_id: 1,
          },
        ],
        target_eta: 40,
        expires_at: '2017-07-13T04:25:29.990Z',
      })
    })
  })

  describe('when the Auction doesnt exist', () => {
    test('it returns null', () => {
      expect(AuctionStore.getAuctionById(100)).to.be.undefined
    })
  })
})

describe('getAuctionByJob(job)', () => {
  describe('when job is null', () => {
    test('it returns null', () => {
      expect(AuctionStore.getAuctionByJob(null)).to.be.null
    })
  })

  describe('when job has Auction', () => {
    test('it returns the respective Auction', () => {
      const job = { id: 10 }

      expect(AuctionStore.getAuctionByJob(job)).to.deep.equal({
        id: 1,
        status: 'live',
        job_id: 10,
        bids: [
          {
            id: 40, status: 'submitted', submitted_eta: 20, company_id: 1,
          },
        ],
        target_eta: 40,
        expires_at: '2017-07-13T04:25:29.990Z',
      })
    })
  })

  describe('when job doesnt have Auction', () => {
    const job = { id: 100 }

    test('it returns null', () => {
      expect(AuctionStore.getAuctionByJob(job)).to.be.null
    })
  })

  describe('when AuctionStore doesnt have the Auction yet', () => {
    const job = {
      id: 55,
      auction: {
        id: 5,
        status: 'live',
        job_id: 10,
      },
    }

    test('it returns the Auction from the Job', () => {
      expect(AuctionStore.getAuctionByJob(job)).to.deep.equal({ id: 5, status: 'live', job_id: 10 })
    })
  })
})

describe('getBidById(id)', () => {
  describe('when id is null', () => {
    test('it returns null', () => {
      expect(AuctionStore.getBidById(null)).to.be.null
    })
  })

  describe('when id is Number', () => {
    test('it returns the respective Bid', () => {
      expect(AuctionStore.getBidById(40)).to.deep.equal({
        id: 40,
        status: 'submitted',
        submitted_eta: 20,
        company_id: 1,
      })
    })
  })

  describe('when Bid doesnt exist', () => {
    test('it returns null', () => {
      expect(AuctionStore.getBidById(400)).to.be.null
    })
  })
})

describe('jobHasLiveAuction', () => {
  describe('when auction.status == "live"', () => {
    test('it returns true', () => {
      const job = { id: 10 }

      expect(AuctionStore.jobHasLiveAuction(job)).to.be.ok
    })
  })

  describe('when auction.status != "live"', () => {
    test('it returns false', () => {
      const job = { id: 20 }

      expect(AuctionStore.jobHasLiveAuction(job)).to.not.be.ok
    })
  })

  describe('when auction doesnt exist', () => {
    test('it returns false', () => {
      const job = { id: 200 }

      expect(AuctionStore.jobHasLiveAuction(job)).to.not.be.ok
    })
  })
})

describe('jobHasAuction(job)', () => {
  test('it returns the Auction', () => {
    const job = { id: 10 }

    expect(AuctionStore.jobHasAuction(job)).to.deep.equal({
      id: 1,
      status: 'live',
      job_id: 10,
      bids: [
        {
          id: 40, status: 'submitted', submitted_eta: 20, company_id: 1,
        },
      ],
      target_eta: 40,
      expires_at: '2017-07-13T04:25:29.990Z',
    })
  })

  describe('when the Auction doesnt exist', () => {
    test('it returns null', () => {
      const job = { id: 100 }

      expect(AuctionStore.jobHasAuction(job)).to.be.null
    })
  })
})

describe('getTargetETAByJob(job)', () => {
  test('it returns auction.target_eta', () => {
    const job = { id: 10 }

    expect(AuctionStore.getTargetETAByJob(job)).to.equal(40)
  })

  describe('when Auction doesnt exist', () => {
    test('it returns null', () => {
      const job = { id: 100 }

      expect(AuctionStore.getTargetETAByJob(job)).to.be.null
    })
  })
})

describe('getExpiresAtByJob(job)', () => {
  test('it returns auction.expires_at', () => {
    const job = { id: 10 }

    expect(AuctionStore.getExpiresAtByJob(job)).to.equal('2017-07-13T04:25:29.990Z')
  })

  describe('when Auction doesnt exist', () => {
    test('it returns null', () => {
      const job = { id: 100 }

      expect(AuctionStore.getExpiresAtByJob(job)).to.be.null
    })
  })
})

describe('formattedBidStatus(bid)', () => {
  test('it returns the respective formatted Bid status', () => {
    const bid = { id: 40, status: 'submitted' }

    expect(AuctionStore.formattedBidStatus(bid)).to.equal('Submitted')
  })

  describe('when formatted Bid doesnt exist', () => {
    const bid = { id: 40, status: 'unknown' }

    expect(AuctionStore.formattedBidStatus(bid)).to.equal('--')
  })
})

describe('getFormattedSubmittedETA(partnerProvider, job)', () => {
  test('it returns the formatted bid.submitted_eta', () => {
    const partnerProvider = {
      id: 1,
      company: { id: 1 },
      provider_id: 1,
    }

    const job = { id: 10 }

    expect(AuctionStore.getFormattedSubmittedETA(partnerProvider, job)).to.equal('20 mins')
  })

  describe('when Partner hasnt submitted and Auction expires', () => {
    test('it returns "Expired"', () => {
      const partnerProvider = {
        id: 2,
        company: { id: 2 },
        provider_id: 2,
      }

      const job = { id: 20 }

      expect(AuctionStore.getFormattedSubmittedETA(partnerProvider, job)).to.equal('Expired')
    })
  })
})

describe('getSiteETA(partnerProvider, job)', () => {
  test('it returns the respective provider site.eta', () => {
    const partnerProvider = {
      id: 1,
      company: { id: 1 },
      site_id: 99,
    }

    const job = {
      id: 10,
      candidates: [
        {
          id: 1,
          type: 'Job::Candidate::Site',
          site: {
            id: 99,
          },
          eta: 22,
        },
      ],
    }

    expect(AuctionStore.getSiteETA(partnerProvider, job)).to.equal(22)
  })

  describe('when there is no site ETA for the given provider', () => {
    test('it returns null', () => {
      const partnerProvider = {
        id: 1,
        company: { id: 1 },
        site_id: 99,
      }

      const job = { id: 10, candidates: [] }

      expect(AuctionStore.getSiteETA(partnerProvider, job)).to.be.null
    })
  })
})

describe('getTruckETA(partnerProvider, job)', () => {
  test('it returns the lower truck eta', () => {
    const partnerProvider = { id: 1, company: { id: 1 }, provider_id: 1 }

    const job = {
      id: 10,
      candidates: [
        {
          id: 1, type: 'Job::Candidate::Truck', rescue_company_id: 1, eta: 22,
        },
        {
          id: 2, type: 'Job::Candidate::Truck', rescue_company_id: 1, eta: 7,
        },
      ],
    }

    expect(AuctionStore.getTruckETA(partnerProvider, job)).to.equal(7)
  })

  describe('when there is no truck ETA for the given provider', () => {
    test('it returns null', () => {
      const partnerProvider = { id: 1, company: { id: 1 } }

      const job = { id: 10, candidates: [] }

      expect(AuctionStore.getTruckETA(partnerProvider, job)).to.be.null
    })
  })
})

describe('hasPartnerSentBidToJob(job)', () => {
  beforeEach(() => {
    UserStore.getUser = () => ({
      company: { id: 1 },
    })
  })

  describe('when partner has sent a bid', () => {
    test('it returns true', () => {
      const job = { id: 10 }

      expect(AuctionStore.hasPartnerSentBidToJob(job)).to.be.ok
    })
  })

  describe('when partner sent the bid but lost the auction', () => {
    beforeEach(() => {
      UserStore.getUser = () => ({
        company: { id: 3 },
      })
    })

    test('it returns true', () => {
      const job = { id: 30 }

      expect(AuctionStore.hasPartnerSentBidToJob(job)).to.be.ok
    })
  })

  describe('when partner has not sent a bid', () => {
    test('it returns false', () => {
      const job = { id: 20 }

      expect(AuctionStore.hasPartnerSentBidToJob(job)).to.not.be.ok
    })
  })
})

describe('hasPartnerRejectedJob(job)', () => {
  beforeEach(() => {
    UserStore.getUser = () => ({
      company: { id: 4 },
    })
  })

  describe('when partner has rejected a bid', () => {
    test('it returns true', () => {
      const job = { id: 40 }

      expect(AuctionStore.hasPartnerRejectedJob(job)).to.be.ok
    })
  })

  describe('when partner has not rejected a bid', () => {
    beforeEach(() => {
      UserStore.getUser = () => ({
        company: { id: 1 },
      })
    })

    test('it returns false', () => {
      const job = { id: 10 }

      expect(AuctionStore.hasPartnerRejectedJob(job)).to.not.be.ok
    })
  })
})

describe('getBidByPartnerId(auctionId, partnerId)', () => {
  test('it returns the respective Bid', () => {
    expect(AuctionStore.getBidByPartnerId(1, 1)).to.deep.equal({
      id: 40,
      status: 'submitted',
      submitted_eta: 20,
      company_id: 1,
    })
  })

  describe('when there is no Auction', () => {
    test('it returns null', () => {
      expect(AuctionStore.getBidByPartnerId(14, 1)).to.be.null
    })
  })
})

describe('isAutoAssignToTruckEnabled(auction)', () => {
  describe('when auction.duration_secs == 0', () => {
    test('it returns true', () => {
      const auction = { id: 33, duration_secs: 0 }

      expect(AuctionStore.isAutoAssignToTruckEnabled(auction)).to.be.ok
    })
  })

  describe('when auction.duration_secs != 0', () => {
    const auction = { id: 34, duration_secs: 120 }

    expect(AuctionStore.isAutoAssignToTruckEnabled(auction)).to.not.be.ok
  })
})
