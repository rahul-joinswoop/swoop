import CompanyStore from 'stores/company_store'
import mocker from '../mocker'

describe('CompanyStore', () => {
  beforeAll(() => {
    CompanyStore._add({ id: 100, name: 'First Company' }, false)
    CompanyStore._add({ id: 200, name: 'Second Company', parent_company_id: 100 }, false)
  })

  describe('#getSubCompanies()', () => {
    test('it returns all companies with parent_company_id == parentCompanyId', () => {
      const parentCompanyId = 100

      expect(CompanyStore.getSubCompanies(parentCompanyId)).to.deep.equal([
        {
          id: 100,
          name: 'First Company',
        },
        {
          id: 200,
          name: 'Second Company',
          parent_company_id: 100,
        },
      ])
    })
  })

  describe('#getBaseCompanies()', () => {
    test('it returns only parent companies', () => {
      expect(CompanyStore.getBaseCompanies()).to.deep.equal([
        {
          id: 100,
          name: 'First Company',
        }])
    })
  })

  describe('#getFleetCompanies()', () => {
    test('it returns only FleetCompanies', () => {
      CompanyStore._add({ id: 100, name: 'A Fleet Company', type: 'FleetCompany' }, false)

      expect(CompanyStore.getFleetCompanies()).to.deep.equal([{
        id: 100,
        name: 'A Fleet Company',
        type: 'FleetCompany',
      }])
    })
  })

  describe('#hasPermissions()', () => {
    describe('when user is Root', () => {
      test('it is true', () => {
        mocker.mock('stores/user_store', {
          isRoot: () => true,
          isPartner: () => false,
        }, false)

        expect(CompanyStore.hasPermissions()).to.be.ok
      })
    })

    describe('when user is Partner', () => {
      test('it is true', () => {
        mocker.mock('stores/user_store', {
          isPartner: () => true,
          isRoot: () => false,
        }, false)

        expect(CompanyStore.hasPermissions()).to.be.ok
      })
    })

    describe('when user is not Root and is not Partner', () => {
      test('it is false', () => {
        mocker.mock('stores/user_store', {
          isPartner: () => false,
          isRoot: () => false,
        }, false)

        expect(CompanyStore.hasPermissions()).to.not.be.ok
      })
    })
  })
})
