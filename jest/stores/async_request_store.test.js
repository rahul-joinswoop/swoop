import AsyncRequestStore from 'stores/async_request_store'
import Consts from 'consts'

jest.mock('api')
jest.mock('utils')

describe('Async Request Store tests', () => {
  it('Should clear out old WS data', () => {
    AsyncRequestStore.dataLookup = {
      9: {
        date: new Date(),
      },
      11: {
        date: new Date(new Date() - 20000),
      },
    }
    AsyncRequestStore.cleanupDataLookup()
    expect(Object.keys(AsyncRequestStore.dataLookup)[0]).toBe('9')
  })
  it('Should fail eventually when no WS message ever comes', async () => new Promise(async (resolve) => {
    const successTest = jest.fn()
    const failureTest = jest.fn()
    Consts.WS_RESPONSE_TIMEOUT = 1
    AsyncRequestStore.genericRequest(((callbacks) => {
      callbacks.success({
        async_request: {
          id: 1,
        },
      })
    }), {
      success: successTest,
      error: failureTest,
    })

    expect(successTest).not.toHaveBeenCalled()
    expect(failureTest).not.toHaveBeenCalled()

    await new Promise(innerResolve => setTimeout(innerResolve, 100))

    expect(successTest).not.toHaveBeenCalled()
    expect(failureTest).toHaveBeenCalled()
    Consts.WS_RESPONSE_TIMEOUT = 10000
    resolve()
  }))
  describe('When WS comes after http response', () => {
    it('Should trigger success response on Success once', () => {
      const successTest = jest.fn()
      const failureTest = jest.fn()

      AsyncRequestStore.genericRequest(((callbacks) => {
        callbacks.success({
          async_request: {
            id: 1,
          },
        })
      }), {
        success: successTest,
        error: failureTest,
      })

      AsyncRequestStore.addRemoteItem({
        target: { id: 1 },
      })
      expect(successTest).toHaveBeenCalled()
      expect(failureTest).not.toHaveBeenCalled()
      successTest.mockClear()
      // Ensure a duplicate WS doesn't cause the success message to be called Again
      // And that things have been cleaned up properly
      expect(successTest).not.toHaveBeenCalled()
      AsyncRequestStore.addRemoteItem({
        target: { id: 1 },
      })
      expect(successTest).not.toHaveBeenCalled()
    })
    it('Should trigger error response on Error once', () => {
      const successTest = jest.fn()
      const failureTest = jest.fn()

      AsyncRequestStore.genericRequest(((callbacks) => {
        callbacks.success({
          async_request: {
            id: 2,
          },
        })
      }), {
        success: successTest,
        error: failureTest,
      })

      AsyncRequestStore.addRemoteItem({
        target: { id: 2, error: {} },
      })
      expect(successTest).not.toHaveBeenCalled()
      expect(failureTest).toHaveBeenCalled()
      failureTest.mockClear()
      // Ensure a duplicate WS doesn't cause the success message to be called Again
      // And that things have been cleaned up properly
      expect(failureTest).not.toHaveBeenCalled()
      AsyncRequestStore.addRemoteItem({
        target: { id: 2, error: {} },
        error: {},
      })
      expect(failureTest).not.toHaveBeenCalled()
    })
  })
  describe('When WS comes before http response', () => {
    it('Should trigger error response on error once', () => {
      const successTest = jest.fn()
      const failureTest = jest.fn()

      AsyncRequestStore.addRemoteItem({
        target: { id: 3 },
      })

      AsyncRequestStore.genericRequest(((callbacks) => {
        callbacks.success({
          async_request: {
            id: 3,
          },
        })
      }), {
        success: successTest,
        error: failureTest,
      })


      expect(successTest).toHaveBeenCalled()
      expect(failureTest).not.toHaveBeenCalled()
    })
    it('Should trigger error response on error', () => {
      const successTest = jest.fn()
      const failureTest = jest.fn()

      AsyncRequestStore.addRemoteItem({
        target: { id: 4, error: {} },
      })

      AsyncRequestStore.genericRequest(((callbacks) => {
        callbacks.success({
          async_request: {
            id: 4,
          },
        })
      }), {
        success: successTest,
        error: failureTest,
      })


      expect(successTest).not.toHaveBeenCalled()
      expect(failureTest).toHaveBeenCalled()
    })
  })
})
