import '../../app/assets/javascripts/loader'
import LocationTypeStore from 'stores/location_type_store'

describe('LocationTypeStore', () => {
  const location_type_1 = {
    id: 10,
    name: 'Location Type 1',
  }

  const location_type_2 = {
    id: 20,
    name: 'Location Type 2',
  }


  // YUCK
  global.UserStore.isPartner = () => true
  global.UserStore.getUser = () => ({
    id: 1,
    company: {
      location_types: [
        location_type_1,
        location_type_2,
      ],
    },
  })

  describe('addAllLocationTypesFromUserCompany()', () => {
    it("adds all location types from user's company into LocationTypeStore.companyLocationTypes", () => {
      LocationTypeStore.addAllLocationTypesFromUserCompany()

      expect(LocationTypeStore.companyLocationTypes[10].id).to.equal(10)
      expect(LocationTypeStore.companyLocationTypes[10].name).to.equal('Location Type 1')
      expect(LocationTypeStore.companyLocationTypes[20].id).to.equal(20)
      expect(LocationTypeStore.companyLocationTypes[20].name).to.equal('Location Type 2')
    })
  })

  describe('getLocationTypeById()', () => {
    it('returns the expected location type', () => {
      LocationTypeStore.getLocationTypeById(10)

      expect(LocationTypeStore.companyLocationTypes[10].id).to.equal(10)
      expect(LocationTypeStore.companyLocationTypes[10].name).to.equal('Location Type 1')
    })
  })

  describe('getCompanyLocationTypes()', () => {
    it("constains all location types from current logged in user's company", () => {
      const companyLocationTypes = LocationTypeStore.getCompanyLocationTypes()

      expect(companyLocationTypes[10].id).to.equal(10)
      expect(companyLocationTypes[10].name).to.equal('Location Type 1')
      expect(companyLocationTypes[20].id).to.equal(20)
      expect(companyLocationTypes[20].name).to.equal('Location Type 2')
    })
  })
})
