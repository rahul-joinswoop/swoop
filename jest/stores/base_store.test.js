import BaseStore from 'stores/base_store'
import Dispatcher from 'lib/swoop/dispatcher'

jest.mock('lib/swoop/dispatcher')

class FooStore extends BaseStore {
  // BaseStore needs the child class to define its model name in its constructor.
  constructor() {
    super({
      modelName: 'Foo',
    })
  }

  isEditable() {
    return true
  }
}

const fooStore = new FooStore()

// For this we use FooStore as the child class ^.
describe('BaseStore', () => {
  describe('when a child class is instantiated', () => {
    test('it defines its child model name', () => {
      expect(fooStore.getModelName()).to.equal('foo')
    })
  })

  describe('#_add()', () => {
    test('it adds new items to its internal collection', () => {
      fooStore._add({ id: 1, name: 'Foo One' })

      expect(fooStore.getById(1)).to.deep.equal({ id: 1, name: 'Foo One' })
    })
  })

  describe('#_update()', () => {
    test('it updates current loaded items in the internal collection', () => {
      fooStore._update({ id: 1, name: 'Foo Ein' })

      expect(fooStore.getById(1)).to.deep.equal({ id: 1, name: 'Foo Ein' })
    })

    describe('when the item doesnt exist in the internal collection', () => {
      test('it doesnt add the item while updating', () => {
        fooStore._update({ id: 1000, name: 'Thousand' })

        expect(fooStore.getById(1000)).to.be.undefined
      })
    })
  })

  describe('#_remove()', () => {
    test('it removes the existent item from the internal collection', () => {
      fooStore._remove({ id: 1 })

      expect(fooStore.getById(1)).to.be.undefined
    })
  })

  describe('#getAll()', () => {
    test('it returns all items of the internal collection', () => {
      fooStore._add({ id: 1, name: 'Foo One' })
      fooStore._add({ id: 2, name: 'Foo Two' })

      expect(fooStore.getAll()).to.deep.equal({
        1: { id: 1, name: 'Foo One' },
        2: { id: 2, name: 'Foo Two' },
      })
    })
  })

  describe('#_searchAndLazyLoadBy()', () => {
    test('it returns items of the internal collection by given filters', () => {
      fooStore._add({ id: 1, name: 'Foo One' })
      fooStore._add({ id: 2, name: 'Foo Two', deleted_at: '2018-01-05' })

      expect(fooStore._searchAndLazyLoadBy({ name: 'Foo One' }, 'id')).
      to.deep.equal(
        [{ id: 1, name: 'Foo One' }]
      )
    })

    describe('when showDeleted is set as true', () => {
      test('it returns items of the internal collection by given filters', () => {
        fooStore._add({ id: 1, name: 'Foo', deleted_at: '2018-01-06' })
        fooStore._add({ id: 2, name: 'Foo', deleted_at: '2018-01-05' })

        expect(fooStore._searchAndLazyLoadBy({ name: 'Foo' }, 'id', null, true)).
        to.deep.equal(
          [
            { id: 1, name: 'Foo', deleted_at: '2018-01-06' },
            { id: 2, name: 'Foo', deleted_at: '2018-01-05' },
          ]
        )
      })
    })
  })

  describe('#getAddedMessage()', () => {
    test('it should be defined with the childClass name', () => {
      expect(fooStore.getAddedMessage()).to.equal('A foo was added')
    })
  })

  describe('#getChangedMessage()', () => {
    test('it should be defined with the childClass name', () => {
      expect(fooStore.getChangedMessage()).to.equal('A foo was changed')
    })
  })

  describe('#getRemovedMessage()', () => {
    test('it should be defined with the childClass name', () => {
      expect(fooStore.getRemovedMessage()).to.equal('A foo was deleted')
    })
  })

  describe('#getFailedToAddMessage()', () => {
    test('it should be defined with the childClass name', () => {
      expect(fooStore.getFailedToAddMessage()).to.equal('A foo failed to add')
    })
  })

  describe('#getFailedToUpdateMessage()', () => {
    test('it should be defined with the childClass name', () => {
      expect(fooStore.getFailedToUpdateMessage()).to.equal('A foo failed to update')
    })
  })

  describe('#getFailedToDeleteMessage()', () => {
    test('it should be defined with the childClass name', () => {
      expect(fooStore.getFailedToDeleteMessage()).to.equal('A foo failed to delete')
    })
  })

  describe('#_handleError', () => {
    test('it calls Dispatcher.send with the error message', () => {
      fooStore._handleError({ status: 500 }, 'TextStatus message', 'ErrorThrown message')

      expect(Dispatcher.send).toHaveBeenCalledWith('show_error', {
        data: { status: 500 },
        textStatus: 'TextStatus message',
        errorThrown: 'ErrorThrown message',
      })
    })
  })

  // TODO move it to SearchStore.test (yet to be created)
  // describe('#search', () => {
  //   beforeEach(() => {
  //     const mockedResponse = {
  //       total: 500,
  //       foos: [
  //         {id: 1, name: 'My name is first foo'},
  //         {id: 2, name: 'My name is second foo'}
  //       ]
  //     };
  //
  //     Api = mocker.mock(('api'), {
  //       searchPaginated: (searchUrl, searchValue, filters, searchPage, paginationSize, order, type, response) => {
  //         response.success(mockedResponse);
  //       }
  //     }, false);
  //
  //     fooStore.search('foo', null, 'id,asc', 1);
  //   });
  //
  //   test('it adds the returned collection to the internal search["collection"]', () => {
  //     expect(fooStore.getSearch().collection).to.deep.equal([1,2]);
  //   });
  //
  //   test('it adds the returned collection to the store collection', () => {
  //     expect(fooStore.getAll()).to.deep.equal({
  //       1: {id: 1, name: 'My name is first foo'},
  //       2: {id: 2, name: 'My name is second foo'}
  //     });
  //   });
  //
  //   test('it sets search total accordingly', () => {
  //     expect(fooStore.getSearch().total).to.equal(500);
  //   });
  //
  //   test('it adds page 1 to pages loaded', () => { // because the last param on fooStore.search ^ is the page, meaning we requested page 1
  //     expect(fooStore.getSearch().pagesLoaded).to.deep.equal({"1": [1, 2]});
  //   });
  // });
})
