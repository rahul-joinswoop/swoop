import Storage from 'stores/storage'

describe('Storage tests', () => {
  describe('Storage', () => {
    beforeEach(() => {
      global.window.localStorage.clear()
    })
    const value = '123'
    it('should return empty session initially', () => {
      expect(Storage.getSession()).to.be.empty
    })
    it('should be able to set a session', () => {
      Storage.setSession(value)
      expect(Storage.getSession()).to.equal(value)
    })
    it('should throw error on unknown key', () => {
      expect(Storage.get('key2')).to.be.null
    })
    it('should be able to set a local storage', () => {
      Storage.set('key', value)
      expect(Storage.get('key')).to.equal(value)
    })
    it('should be able to remove a local storage', () => {
      Storage.set('key', value)
      Storage.remove('key')
      expect(Storage.get('key')).to.be.null
    })
    // can't get this working with jsdom 11.12, skipping for now
    describe.skip('should handle localstorage errors', () => {
      beforeEach(() => {
        window.localStorageOrig = window.localStorage
        window.localStorage = {
          getItem: jest.fn().mockImplementation(() => {
            throw new Error('Quota Error')
          }),
          setItem: jest.fn().mockImplementation(() => {
            throw new Error('Quota Error')
          }),
          removeItem: jest.fn().mockImplementation(() => {
            throw new Error('Quota Error')
          }),
        }
        window.analytics = {
          track: jest.fn(),
        }
      })
      afterEach(() => {
        window.locaStorage = window.localStorageOrig
        delete window.localStorageOrig
      })
      it('should handle errors on set', () => {
        Storage.set('key', value)
        expect(window.analytics.track).toHaveBeenCalled()
      })
      it('should handle errors on get', () => {
        Storage.get('key1')
        expect(window.analytics.track).toHaveBeenCalled()
      })
      it('should handle errors on remove', () => {
        Storage.remove('key')
        expect(window.analytics.track).toHaveBeenCalled()
      })
    })
  })
})
