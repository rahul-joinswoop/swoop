import '../../app/assets/javascripts/loader'
import JobStore from 'stores/job_store'
import UserStore from 'stores/user_store'
import Api from 'api'

describe('JobStore', () => {
  describe('isPartner(job)', () => {
    it('is true when job.type == RescueJob', () => {
      const job = {
        id: 1,
        type: 'RescueJob',
      }

      expect(JobStore.isPartner(job)).to.be.true
    })

    it('is false when job.type != RescueJob', () => {
      const job = {
        id: 1,
        type: 'Whatever',
      }

      expect(JobStore.isPartner(job)).to.be.false
    })

    it('is true when job.type == FleetMotorClubJob', () => {
      const job = {
        id: 1,
        type: 'FleetMotorClubJob',
      }

      expect(JobStore.isMotorClub(job)).to.be.true
    })

    it('is false when job.type != FleetMotorClubJob', () => {
      const job = {
        id: 1,
        type: 'Whatever',
      }

      expect(JobStore.isMotorClub(job)).to.be.false
    })
  })

  describe('shouldShowRequestMoreTimeToAccept(job)', () => {
    describe('when job.type is FleetMotorClubJob', () => {
      describe('and job.status is Assigned', () => {
        describe('and job has issc_dispatch_request', () => {
          describe('and issc_dispatch_request.system is rsc', () => {
            describe('and issc_dispatch_request.more_time_requested is true', () => {
              const job = {
                type: 'FleetMotorClubJob',
                status: 'Assigned',
                issc_dispatch_request: {
                  system: 'rsc',
                  more_time_requested: true,
                },
              }

              it('is false', () => {
                expect(JobStore.shouldShowRequestMoreTimeToAccept(job)).to.be.false
              })
            })

            describe('and issc_dispatch_request.more_time_requested is false', () => {
              const job = {
                type: 'FleetMotorClubJob',
                status: 'Assigned',
                issc_dispatch_request: {
                  system: 'rsc',
                  more_time_requested: false,
                },
              }

              it('is true', () => {
                expect(JobStore.shouldShowRequestMoreTimeToAccept(job)).to.be.true
              })
            })
          })
        })
      })
    })
  })

  describe('allowChangePaymentMethodFor(job)', () => {
    beforeAll(() => {
      UserStore.isTesla = () => true
    })

    describe('when payment type is Customer Pay', () => {
      const job = {
        status: 'Completed',
        customer_type_name: 'Customer Pay',
      }

      it('returns true', () => {
        expect(JobStore.allowChangePaymentMethodFor(job)).to.be.true
      })
    })

    describe('when payment type is P Card', () => {
      const job = {
        status: 'Completed',
        customer_type_name: 'P Card',
      }

      it('returns true', () => {
        expect(JobStore.allowChangePaymentMethodFor(job)).to.be.true
      })
    })

    describe('when payment type is Warranty', () => {
      const job = {
        status: 'Completed',
        customer_type_name: 'Warranty',
      }

      it('returns true', () => {
        expect(JobStore.allowChangePaymentMethodFor(job)).to.be.true
      })
    })

    describe('when payment type is Goodwill', () => {
      const job = {
        status: 'Completed',
        customer_type_name: 'Goodwill',
      }

      it('returns true', () => {
        expect(JobStore.allowChangePaymentMethodFor(job)).to.be.true
      })
    })

    describe('when status is GOA', () => {
      const job = {
        status: 'GOA',
        customer_type_name: 'Customer Pay',
      }

      it('returns true', () => {
        expect(JobStore.allowChangePaymentMethodFor(job)).to.be.true
      })
    })

    describe('when status is Canceled', () => {
      const job = {
        status: 'Canceled',
        customer_type_name: 'Customer Pay',
      }

      it('returns true', () => {
        expect(JobStore.allowChangePaymentMethodFor(job)).to.be.true
      })
    })

    describe('when status is not allowed', () => {
      const job = {
        status: 'Whatever',
        customer_type_name: 'Customer Pay',
      }

      expect(JobStore.allowChangePaymentMethodFor(job)).to.be.false
    })

    describe('when job is null', () => {
      expect(JobStore.allowChangePaymentMethodFor(null)).to.be.false
    })

    describe('when job is undefined', () => {
      expect(JobStore.allowChangePaymentMethodFor(undefined)).to.be.false
    })

    describe('when user is not Tesla', () => {
      beforeAll(() => {
        UserStore.isTesla = () => false
      })

      const job = {
        status: 'Completed',
        customer_type_name: 'Customer Pay',
      }

      it('returns false', () => {
        expect(JobStore.allowChangePaymentMethodFor(job)).to.be.false
      })
    })
  })

  describe('storageReleasedToUserName(job_id)', () => {
    describe('when job.storage.released_to_user has full_name', () => {
      JobStore._modelCollection[10000] = {
        id: 10000,
        storage: {
          released_to_user: {
            full_name: 'Full User Name',
          },
        },
      }

      it('returns Full User Name', () => {
        expect(JobStore.storageReleasedToUserName(10000)).to.eq('Full User Name')
      })
    })

    describe('when job.storage.released_to_user has first_name only', () => {
      JobStore._modelCollection[10001] = {
        id: 10001,
        storage: {
          released_to_user: {
            first_name: 'First Name Only',
          },
        },
      }

      it('returns First Name Only', () => {
        expect(JobStore.storageReleasedToUserName(10001)).to.eq('First Name Only')
      })
    })

    describe('when job.storage.released_to_user has first_name and last_name', () => {
      JobStore._modelCollection[10002] = {
        id: 10002,
        storage: {
          released_to_user: {
            first_name: 'First Name',
            last_name: 'And Last Name',
          },
        },
      }

      it('returns First Name And Last Name', () => {
        expect(JobStore.storageReleasedToUserName(10002)).to.eq('First Name And Last Name')
      })
    })

    describe('when job.storage.released_to_user has no name at all', () => {
      JobStore._modelCollection[10003] = {
        id: 10003,
        storage: {
          released_to_user: { },
        },
      }

      it('returns null', () => {
        expect(JobStore.storageReleasedToUserName(10003)).to.be.null
      })
    })

    describe('when job.storage has no released_to_user', () => {
      JobStore._modelCollection[10004] = {
        id: 10004,
        storage: { },
      }

      it('returns null', () => {
        expect(JobStore.storageReleasedToUserName(10004)).to.be.null
      })
    })

    describe('when job has no storage', () => {
      JobStore._modelCollection[10005] = {
        id: 10005,
      }

      it('returns null', () => {
        expect(JobStore.storageReleasedToUserName(10005)).to.be.null
      })
    })
  })

  describe('createDemoJob()', () => {
    it('calls Api.createDemoJob', () => {
      Api.createDemoJob = jest.fn()

      JobStore.createDemoJob()

      expect(Api.createDemoJob).toHaveBeenCalled()
    })
  })
})
