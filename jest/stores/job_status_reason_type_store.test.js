import JobStatusReasonTypeStore from 'stores/job_status_reason_type_store'

beforeAll(() => {
  JobStatusReasonTypeStore.loadAndGetAll = () => ({
    8: {
      id: 8,
      key: 'cancel_prior_job_delayed',
      text: 'Traffic/service vehicle problem',
      job_status_id: 8,
      job_status_name: 'Canceled',
    },
    14: {
      id: 14,
      key: 'goa_customer_cancel_after_deadline',
      text: 'Customer cancel after deadline',
      job_status_id: 21,
      job_status_name: 'GOA',
    },
  })
})

describe('getByJobStatusName(jobStatusName, watcherId)', () => {
  describe('when jobStatusName is null', () => {
    test('it returns the respective Auction', () => {
      expect(JobStatusReasonTypeStore.getByJobStatusName(null)).toEqual([])
    })
  })

  describe('when jobStatusName is Canceled', () => {
    test('it returns only Canceled types', () => {
      expect(JobStatusReasonTypeStore.getByJobStatusName('Canceled')).to.deep.equal([
        {
          id: 8,
          key: 'cancel_prior_job_delayed',
          text: 'Traffic/service vehicle problem',
          job_status_id: 8,
          job_status_name: 'Canceled',
        },
      ])
    })
  })

  describe('when jobStatusName is GOA', () => {
    test('it returns only GOA types', () => {
      expect(JobStatusReasonTypeStore.getByJobStatusName('GOA')).to.deep.equal([
        {
          id: 14,
          key: 'goa_customer_cancel_after_deadline',
          text: 'Customer cancel after deadline',
          job_status_id: 21,
          job_status_name: 'GOA',
        },
      ])
    })
  })
})
