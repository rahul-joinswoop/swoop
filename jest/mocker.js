/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS205: Consider reworking code to avoid use of IIFEs
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import { partial } from 'lodash'

const requires = {}
const origs = {}

export default {
  mock(path, funcs, passBackOriginal = true) {
    /* eslint-disable-next-line */
    let obj = require(path)

    if (requires[path] != null) {
      obj = requires[path]
    }

    Object.keys(funcs).forEach((name) => {
      const func = funcs[name]

      if (origs[path] == null) {
        origs[path] = {}
      }

      if (!origs[path][name]) {
        origs[path][name] = obj[name]
      }

      obj[name] = passBackOriginal ? partial(func, origs[path][name]) : func
    })

    requires[path] = obj
    return obj
  },

  stop(path) {
    return Object.keys(origs[path]).map((name) => {
      requires[path][name] = origs[path][name]
      return requires[path][name]
    })
  },

  // delete requires[path]
  // delete origs[path]
  stopAll() {
    return Object.keys(origs).map(name => this.stop(name))
  },
}
