import React from 'react'
import Utils from 'utils'
import { render } from 'enzyme'
import 'loader' // delete this file when we have converted coffeescript to JS
import { IntlProvider } from 'react-intl'
import Storage from 'stores/storage'

describe('Utils methods', () => {
  describe('formatMoney', () => {
    const locale = Storage.get('language') || 'en'

    function renderWithIntlProvider(component) {
      return render(
        <IntlProvider locale={locale}>
          {component}
        </IntlProvider>
      )
    }

    test('invalid non Number values are converted to 0/zero', () => {
      const toTest = [null, undefined, '', true, false, 'asdasd', {}, () => {}]
      toTest.forEach((amount) => {
        const result = Utils.formatMoney(amount)
        const wrapper = renderWithIntlProvider(result)
        expect(wrapper.text()).toBe('$0.00')
      })
    })

    test('render EUR currency', () => {
      const result = Utils.formatMoney('100', 'EUR')
      const wrapper = renderWithIntlProvider(result)
      expect(wrapper.text()).toBe('€100.00')
    })

    test('valid amount formatting', () => {
      const toTest = [['200.45', '$200.45'], [8483.00, '$8,483.00'], [1284, '$1,284.00'], [2371.2345, '$2,371.24']]
      toTest.forEach(([amount, expected]) => {
        const result = Utils.formatMoney(amount)
        const wrapper = renderWithIntlProvider(result)
        expect(wrapper.text()).toBe(expected)
      })
    })
  })

  describe('getInvoiceBalance(invoice)', () => {
    it('calculates it correctly', () => {
      const invoice = {
        line_items: [
          { net_amount: '19.99', tax_amount: null, deleted_at: null },
          { net_amount: '20.00', tax_amount: null, deleted_at: null },
          { net_amount: '20.00', tax_amount: null, deleted_at: '2019-04-17T23:39:35.000Z' },
        ],
        payments: [
          { amount: '-10.00', deleted_at: null },
          { amount: '-15.00', deleted_at: '2019-04-17T23:39:35.000Z' },
        ],
      }

      expect(Utils.getInvoiceBalance(invoice)).to.eq('29.99')
    })
  })
})
