/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS205: Consider reworking code to avoid use of IIFEs
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import Api from 'api'

import mocker from '../mocker'
import '../../app/assets/javascripts/loader'


describe('ResizeTable', () => describe('Api', () => {
  describe('Bearer Token', () => it('should be set on ajax', () => {
    const callback = jest.fn()
    let passedObj = null
    mocker.mock(
      'jquery',
      {
        ajaxSetup(obj) {
          callback()
          passedObj = obj
          return passedObj
        },
      },
      false,
    )
    Api.setBearerToken('test')
    expect(callback).toHaveBeenCalled()
    expect(JSON.stringify(passedObj)).to.match(/test/)
  }))
  describe('Api helper calls', () => {
    const helperCalls = [
      'createDemoJob',
      'objectsRequest',
      'track',
      'userMeRequest',
      'addFleet',
      'removeFleet',
      'providersRequest',
      'vehiclesRequest',
      'requestCallbackRequest',
      'jobCopy',
      'partnerCommand',
      'addOnJob',
      'rejectJob',
      'releaseJob',
      'jobsStatusRequest',
      'jobsRequest',
      'invoiceRequest',
      'metricsRequest',
      'reportsRequest',
      'reportsPaginated',
      'reportsDownload',
      'jobsRequestPaginated',
      'doneDriverJobsRequest',
      'runReport',
      'downloadStorageList',
      'downloadFleetUnpaid',
      'downloadPartnerUnpaid',
      'activeJobsRequest',
      'activeDriverJobsRequest',
      'doneJobsRequest',
      'adminUsersRequest',
      'getUser',
      'getVersion',
      'requestPartnerJobs',
      'getPartnerJobs',
      'createPartnerJob',
      'updatePartnerJob',
      'deletePartnerJob',
      'requestFleetJobs',
      'getFleetJobs',
      'createFleetJob',
      'updateFleetJob',
      'deleteFleetJob',
      'sendJobNotification',
      'sendLocation',
      'getCustomerJob',
      'companies',
      'fetchRecommendedProviders',
      'providers',
      'accounts',
      'vehicleCategories',
      'checkUsername',
      'customerTypes',
      'invoice',
      'drivers',
      'vehicleMakes',
      'versions',
    ]

    const testApiCallUsesRequest = call => it(`${call} should call request`, () => {
      Api.request = jest.fn()
      Api[call]()
      const result = expect(Api.request).toHaveBeenCalled()
      Api.request.mockReset()
      return result
    })

    helperCalls.map(call => testApiCallUsesRequest(call))
  })
}))
