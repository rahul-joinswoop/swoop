/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
import '../../app/assets/javascripts/loader' // Mock out the set of user

import React from 'react'
import Button from 'components/simple/choose_partner_button'
import ReactTestUtils from 'react-dom/test-utils'
import mocker from '../mocker'

describe('JobStatus', () => {
  let instance = null

  const resetModules = (props) => {
    mocker.mock('stores/user_store', {})
    mocker.mock('stores/customer_type_store', {})
    mocker.mock('utils.coffee', {})
    jest.resetModules()
    instance = React.createFactory(Button)(props)
    instance = ReactTestUtils.renderIntoDocument(instance)
    return instance
  } // TODO: Test to make sure PO Works

  describe('Modal popup', () => {
    it('Should typically call partner modal', () => {
      const job = {
        id: 1,
        scheduled_for: true,
        status: 'Pending',
      }
      let pass_key = null
      mocker.mock('utils.coffee', {
        showModal(original, key) {
          pass_key = key
          return pass_key
        },
      }) // TODO: create fake job creator

      resetModules({
        record: job,
      })
      instance.handleOnClick(null)
      expect(pass_key).to.equal('partner')
    })
    it('Should call set_customer_type when customer_type is TBD and tesla', () => {
      const job = {
        id: 1,
        scheduled_for: true,
        status: 'Pending',
        customer_type_id: 1,
      }
      let pass_key = null
      mocker.mock('stores/user_store', {
        isTesla() {
          return true
        },
      })
      mocker.mock('stores/customer_type_store', {
        getById() {
          return {
            name: 'TBD',
          }
        },
      })
      mocker.mock('utils.coffee', {
        showModal(original, key) {
          pass_key = key
          return pass_key
        },
      }) // TODO: create fake job creator

      resetModules({
        record: job,
      })
      instance.handleOnClick(null)
      expect(pass_key).to.equal('set_customer_type')
    })
    it('Should not call set_customer_type when customer_type is TBD and not tesla', () => {
      const job = {
        id: 1,
        scheduled_for: true,
        status: 'Pending',
        customer_type_id: 1,
      }
      let pass_key = null
      mocker.mock('stores/user_store', {
        isTesla() {
          return false
        },
      })
      mocker.mock('stores/customer_type_store', {
        getById() {
          return {
            name: 'TBD',
          }
        },
      })
      mocker.mock('utils.coffee', {
        showModal(original, key) {
          pass_key = key
          return pass_key
        },
      }) // TODO: create fake job creator

      resetModules({
        record: job,
      })
      instance.handleOnClick(null)
      expect(pass_key).to.equal('partner')
    })
    it('Should not call set_customer_type when customer_type is not TBD and tesla', () => {
      const job = {
        id: 1,
        scheduled_for: true,
        status: 'Pending',
        customer_type_id: 1,
      }
      let pass_key = null
      mocker.mock('stores/user_store', {
        isTesla() {
          return true
        },
      })
      mocker.mock('stores/customer_type_store', {
        getById() {
          return {
            name: 'Something Else',
          }
        },
      })
      mocker.mock('utils.coffee', {
        showModal(original, key) {
          pass_key = key
          return pass_key
        },
      }) // TODO: create fake job creator

      resetModules({
        record: job,
      })
      instance.handleOnClick(null)
      expect(pass_key).to.equal('partner')
    })
  })
})
