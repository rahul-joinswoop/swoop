import React from 'react'
import { mount } from 'enzyme'
import GetLocation from 'mobile/GetLocation'

describe('get_location', () => {
  it('works', () => {
    const wrapper = mount(<GetLocation />)
    expect(wrapper).to.be.ok
  })
})
