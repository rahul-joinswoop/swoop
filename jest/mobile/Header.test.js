import React from 'react'
import { shallow } from 'enzyme'
import Header from 'mobile/Header'

describe('<Header />', () => {
  it('<Header /> works', () => {
    const component = shallow(<Header />)
    expect(component).to.be.ok
  })

  it('<Header /> includes two children, logo and call button', () => {
    const component = shallow(<Header />)
    expect(component.children()).to.have.lengthOf(2)
  })

  it('<Header /> update() method works', () => {
    const component = shallow(<Header />)
    expect(component.state().rev).to.equal(0)
    component.instance().update()
    expect(component.state().rev).to.equal(1)
  })

  it('<Header /> displays Job ID when clicked', () => {
    const component = shallow(<Header />)
    expect(component.find('.job-id').exists()).to.equal(false)
    component.find('.mobile-logo').simulate('click')
    expect(component.find('.job-id').exists()).to.equal(true)
  })
})
