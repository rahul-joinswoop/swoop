import React from 'react'
import { shallow } from 'enzyme'
import Question from 'mobile/Survey/Question'

describe('<Question />', () => {
  const theQuestion = <Question
    key="question_0"
    leftKey="unlikely"
    onSelect={jest.fn()}
    question="How likely are you to recommend us to family, friends, or colleagues?"
    questionKey="question_0"
    rightKey="likely"
    selected={9}
  />

  it('<Question /> works', () => {
    const component = shallow(theQuestion)
    expect(component).to.be.ok
  })

  it('<Question /> verbalScale() method returns correct string', () => {
    const component = shallow(theQuestion)
    const verbal = shallow(<div>{component.instance().verbalScale()}</div>)
    expect(verbal.text()).to.equal('Great! We love serving our customers.')
  })

  it('<Question /> does not render indicators', () => {
    const component = shallow(theQuestion)
    expect(component.find('.options-indicators').exists()).to.equal(false)
  })
})
