import React from 'react'
import { shallow } from 'enzyme'
import ThankYou from 'mobile/Survey/ThankYou'

describe('<ThankYou />', () => {
  it('<ThankYou /> works', () => {
    const component = shallow(<ThankYou />)
    expect(component).to.be.ok
  })

  it('<ThankYou /> renders children', () => {
    const component = shallow(<ThankYou />)
    expect(component.find('.thankyou').exists()).to.equal(true)
  })
})
