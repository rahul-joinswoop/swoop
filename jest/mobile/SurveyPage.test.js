import React from 'react'
import { shallow } from 'enzyme'
import SurveyPage from 'mobile/Survey/SurveyPage'

describe('<SurveyPage />', () => {
  beforeAll(() => {
    global.meh = {
      token: 'token',
      survey_questions: [
        {
          id: 1,
          type: 'Survey::TenStarQuestion',
          question: 'How likely are you to recommend us to family, friends, or colleagues?',
        },
      ],
    }
    global.gon = {
      dispatcher_name: 'Doctor Dispatch',
      token: 'token',
      survey_questions: [
        {
          id: 1,
          type: 'Survey::TenStarQuestion',
          question: 'How likely are you to recommend us to family, friends, or colleagues?',
        },
        {
          id: 2,
          type: 'Survey::TenStarQuestion',
          question: 'How well were you kept informed while waiting for your towing or roadside service provider to arrive?',
        },
        {
          id: 3,
          type: 'Survey::TenStarQuestion',
          question: 'Did the tow or roadside service provider arrive within the initial service response time promised?',
        },
        {
          id: 4,
          type: 'Survey::LongTextQuestion',
          question: 'Thanks for your rating! How do you think we can improve?',
        },
      ],
    }
  })

  it('<SurveyPage /> works', () => {
    const component = shallow(<SurveyPage index={0} survey_questions={window.gon.survey_questions} />)
    expect(component).to.be.ok
  })

  it('<SurveyPage /> displays dispatcher name', () => {
    const component = shallow(<SurveyPage index={0} survey_questions={window.gon.survey_questions} />)
    component.setState({ page: 'survey' })
    expect(component.state().page).to.equal('survey')
    expect(component.find('.dispatcher')).to.have.lengthOf(1)
  })

  it('<SurveyPage /> does not display dispatcher if missing', () => {
    window.gon = window.meh
    const component = shallow(<SurveyPage index={0} survey_questions={window.gon.survey_questions} />)
    component.setState({ page: 'survey' })
    expect(component.state().page).to.equal('survey')
    expect(component.find('.dispatcher')).to.have.lengthOf(0)
  })
})
