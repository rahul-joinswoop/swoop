import React from 'react'
import { shallow } from 'enzyme'
import NPS from 'mobile/Survey/Nps'

describe('<NPS />', () => {
  beforeAll(() => {
    global.gon = {
      dispatcher_name: 'Doctor Dispatch',
      token: 'token',
      survey_questions: [
        {
          id: 1,
          type: 'Survey::TenStarQuestion',
          question: 'How likely are you to recommend us to family, friends, or colleagues?',
        },
        {
          id: 2,
          type: 'Survey::TenStarQuestion',
          question: 'How well were you kept informed while waiting for your towing or roadside service provider to arrive?',
        },
        {
          id: 3,
          type: 'Survey::TenStarQuestion',
          question: 'Did the tow or roadside service provider arrive within the initial service response time promised?',
        },
        {
          id: 4,
          type: 'Survey::LongTextQuestion',
          question: 'Thanks for your rating! How do you think we can improve?',
        },
      ],
    }
  })

  it('<NPS /> works', () => {
    const component = shallow(<NPS />)
    expect(component).to.be.ok
  })

  it('<NPS /> handleSelected method updates state', () => {
    const questions = global.gon.survey_questions
    const component = shallow(
      <NPS
        question={questions.question}
        question_key="question_0"
      />
    )
    expect(component.state().selected).toBeNull()
    component.instance().handleSelected(0)
    expect(component.state().selected).to.equal(0)
  })
})
