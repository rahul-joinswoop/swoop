import React from 'react'
import { shallow } from 'enzyme'
import Survey from 'mobile/Survey/Survey'
import Backbone from 'backbone'
import Tracking from 'tracking'

Backbone.history.start = jest.fn()
Tracking.track = jest.fn()

describe('<Survey />', () => {
  beforeAll(() => {
    global.gon = {
      dispatcher_name: 'Doctor Dispatch',
      token: 'token',
      survey_questions: [
        {
          id: 1,
          type: 'Survey::TenStarQuestion',
          question: 'How likely are you to recommend us to family, friends, or colleagues?',
        },
        {
          id: 2,
          type: 'Survey::TenStarQuestion',
          question: 'How well were you kept informed while waiting for your towing or roadside service provider to arrive?',
        },
        {
          id: 3,
          type: 'Survey::TenStarQuestion',
          question: 'Did the tow or roadside service provider arrive within the initial service response time promised?',
        },
        {
          id: 4,
          type: 'Survey::LongTextQuestion',
          question: 'Thanks for your rating! How do you think we can improve?',
        },
      ],
    }
  })

  it('<Survey /> works', () => {
    const component = shallow(<Survey />)
    expect(component).to.be.ok
  })

  it('<Survey /> has state on mount', () => {
    const component = shallow(<Survey />)
    expect(component.state().page).to.equal('survey')
    expect(component.state().index).to.equal('0')
  })

  it('<Survey /> setPage method updates state', () => {
    const component = shallow(<Survey />)
    component.instance().setPage('thankyou', '3')
    expect(component.state().page).to.equal('thankyou')
    expect(component.state().index).to.equal('3')
  })
})
