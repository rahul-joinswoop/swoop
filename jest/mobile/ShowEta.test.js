import React from 'react'
import { mount } from 'enzyme'
import ShowEta from 'mobile/ShowEta'
import LocationStore from 'mobile/LocationStore'
import BaseConsts from 'base_consts'

jest.mock('mobile/LocationStore')

describe('show_eta', () => {
  beforeEach(() => {
    jest.resetAllMocks()
    LocationStore.getJob.mockReturnValue({
      status: BaseConsts.ENROUTE,
      toa: {
        live: (new Date()).toJSON(),
      },
    })
  })
  it('works', () => {
    const wrapper = mount(<ShowEta />)
    expect(wrapper).to.be.ok
  })
})
