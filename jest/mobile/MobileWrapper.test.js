import React from 'react'
import { shallow } from 'enzyme'
import MobileWrapper from 'mobile/MobileWrapper'

describe('<MobileWrapper />', () => {
  it('<MobileWrapper /> works', () => {
    const component = shallow(<MobileWrapper />)
    expect(component).to.be.ok
  })

  it('<MobileWrapper /> renders children', () => {
    const component = shallow(
      <MobileWrapper>
        <div className="test">Test</div>
      </MobileWrapper>
    )
    expect(component.find('.test').exists()).to.equal(true)
  })
})
