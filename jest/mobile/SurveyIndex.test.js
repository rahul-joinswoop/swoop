import React from 'react'
import { shallow } from 'enzyme'
import Survey from 'mobile/Survey'

describe('<Survey /> index.js', () => {
  it('<Survey /> works', () => {
    const component = shallow(<Survey />)
    expect(component).to.be.ok
  })
})
