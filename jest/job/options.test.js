import '../../app/assets/javascripts/loader'
import { Component as JobOptions } from 'components/job/options'
import UserStore from 'stores/user_store'

const job = {
  id: 1,
  fleet_manual: false,
  rescue_company: {},
}

describe('showManageStatus()', () => {
  it('returns true when user user is Swoop', () => {
    const jobOptionsInstance = new JobOptions({ job })
    UserStore.isSwoop = () => true

    expect(jobOptionsInstance.showManageStatus(job)).to.be.true
  })

  it('returns true when user is FleetInHouse', () => {
    const jobOptionsInstance = new JobOptions({ job })
    UserStore.isFleetInHouse = () => true

    expect(jobOptionsInstance.showManageStatus(job)).to.be.true
  })

  it('returns false', () => {
    const jobOptionsInstance = new JobOptions({ job })
    UserStore.isSwoop = () => false
    UserStore.isFleetInHouse = () => false

    expect(jobOptionsInstance.showManageStatus(job)).to.be.false
  })
})

describe('showEmailJobDetails()', () => {
  it('returns true when user user is Admin', () => {
    const jobOptionsInstance = new JobOptions({ job })
    UserStore.isAdmin = () => true

    expect(jobOptionsInstance.showEmailJobDetails(job)).to.be.true
  })

  it('returns true when user is Root', () => {
    const jobOptionsInstance = new JobOptions({ job })
    UserStore.isRoot = () => true

    expect(jobOptionsInstance.showEmailJobDetails(job)).to.be.true
  })

  it('returns true when user is Dispatcher', () => {
    const jobOptionsInstance = new JobOptions({ job })
    UserStore.isDispatcher = () => true

    expect(jobOptionsInstance.showEmailJobDetails(job)).to.be.true
  })

  it('returns false when user is only Driver', () => {
    const jobOptionsInstance = new JobOptions({ job })
    UserStore.isAdmin = () => false
    UserStore.isRoot = () => false
    UserStore.isDispatcher = () => false

    expect(jobOptionsInstance.showEmailJobDetails(job)).to.be.false
  })
})

describe('showMarkAsGOA()', () => {
  it('returns false when user is FleetManaged', () => {
    const jobOptionsInstance = new JobOptions({ job })
    UserStore.isFleetManaged = () => true

    expect(jobOptionsInstance.showMarkAsGOA(job)).to.be.false
  })
})
