## Ably Setup
If you need to test out subscription stuff for mobile locally you'll need to setup a way for Ably to send webhooks to you.

1. Install [ngrok](https://ngrok.com/)
2. Ask @lamtha to send you an invite to our company account
3. Setup your ngrok account via the emailed link
4. Go to https://dashboard.ngrok.com/reserved and add a new reserved subdomain for yourself (pls use your github handle to make it clearer who is who)
5. Now you should be able to start up ngrok:
```bash
$ ngrok http -subdomain=modosc 5000
```
6. Now go to Ably (ask @modosc or @lamtha for access) and click into the Sandbox app
7. Click on the Reactor tab and add a new WebHook. Make it identical to the existing `modosc` webhook but with your ngrok hostname instead.
8. Now start up your local development servers and mobile build and do something on mobile that triggers a subscription.
9. Go to [ngrok's dashboard](http://localhost:4040/) - you should see webhooks coming in from ably.

### Ngrok as a service
If you don't want to run `ngrok` manually you can set it up to run in `launchctl`. Copy the text from [this gist](https://gist.github.com/modosc/6cbe6913cef74759974ebbd5b8985479) and write it to `~/Library/LaunchAgents/ngrok.plist` (make sure you chante the `subdomain` argument to match your subdomain from step 4). Now load up the new service:
```bash
$ launchctl load ~/Library/LaunchAgents/ngrok.plist
```
And verify it's running:
```bash
$ launchctl list ngrok
{
	"LimitLoadToSessionType" = "Aqua";
	"Label" = "ngrok";
	"TimeOut" = 30;
	"OnDemand" = true;
	"LastExitStatus" = 0;
	"PID" = 14241;
	"Program" = "/usr/local/bin/ngrok";
	"ProgramArguments" = (
		"/usr/local/bin/ngrok";
		"http";
		"-subdomain=modosc";
		"5000";
	);
};
```
You can also go to the [dashboard](http://localhost:4040) to verify.
