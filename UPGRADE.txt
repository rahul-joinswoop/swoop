1 minor version at a time (Major.Minor.Patch)
https://rubygems.org/gems/rails/versions


Write tests and make sure they pass.
Move to the latest patch version after your current version.
Fix tests and deprecated features.
Move to the latest patch version of the next minor version.



4.2.9 -> 4.2.10
5.0.6


update rails version in gemfile
run:
  rake rails:update (4.2)
  rake app:update (5)


https://hashrocket.com/blog/posts/how-to-upgrade-to-rails-5
http://edgeguides.rubyonrails.org/upgrading_ruby_on_rails.html#upgrading-from-rails-4-2-to-rails-5-0


Fix 'drive' name association (see rename_drive_driver branch)





