CREATE OR REPLACE FUNCTION public.qbi_adjusted_ajs (jobid integer, status_id integer)
  RETURNS text
  LANGUAGE plpgsql
  IMMUTABLE
  AS $function$
BEGIN
  RETURN qbidt (adjusted_ajs (jobid,
      status_id));
END;
$function$
