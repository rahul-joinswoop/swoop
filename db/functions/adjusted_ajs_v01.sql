CREATE OR REPLACE FUNCTION public.adjusted_ajs (jobid integer, status_id integer)
  RETURNS timestamp without time zone
  LANGUAGE plpgsql
  IMMUTABLE
  AS $function$
BEGIN
  RETURN CASE WHEN adjusted_dttm IS NOT NULL THEN
    adjusted_dttm
  ELSE
    last_set_dttm
  END
FROM
  audit_job_statuses
WHERE
  job_status_id = status_id
  AND audit_job_statuses.job_id = jobid
  AND audit_job_statuses.deleted_at IS NULL
ORDER BY
  id DESC
LIMIT 1;
END;
$function$
