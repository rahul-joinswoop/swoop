CREATE OR REPLACE FUNCTION public.utc_now ()
  RETURNS timestamp without time zone
  LANGUAGE plpgsql
  IMMUTABLE
  AS $function$
BEGIN
  RETURN timezone('UTC', now())::timestamp;
END;
$function$
