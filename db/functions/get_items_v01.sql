CREATE OR REPLACE FUNCTION public.get_items (uuid)
  RETURNS json
  LANGUAGE sql
  STRICT
  AS $function$
  SELECT
    COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]') AS json
  FROM ( SELECT DISTINCT
      lines.description AS item_name
    FROM
      invoicing_line_items lines
    WHERE (lines.ledger_item_id IN (
        SELECT
          "list_sent_invoices".id
        FROM
          "list_sent_invoices"
        WHERE
          sender_id = (
            SELECT
              company_id
            FROM
              qb_company_tokens
            WHERE
              token = $1)))
      GROUP BY
        item_name) t
$function$
