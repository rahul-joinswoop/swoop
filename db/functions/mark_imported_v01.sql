CREATE OR REPLACE FUNCTION public.mark_imported (uuid, integer)
  RETURNS void
  LANGUAGE sql
  AS $function$
  UPDATE
    invoicing_ledger_items
  SET
    qb_imported_at = NOW(),
    qb_pending_import = NULL
  WHERE
    id = $2
    AND sender_id = (
      SELECT
        company_id
      FROM
        qb_company_tokens
      WHERE
        token = $1)
$function$
