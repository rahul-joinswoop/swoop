CREATE OR REPLACE FUNCTION public.add_pdf_to_report (old_name character varying)
  RETURNS void
  LANGUAGE plpgsql
  AS $function$
BEGIN
  UPDATE
    reports
  SET
    pdf = TRUE
  WHERE
    name = old_name;
  IF found <> TRUE THEN
    RETURN;
  END IF;
END;
$function$
