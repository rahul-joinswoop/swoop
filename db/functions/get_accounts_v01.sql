CREATE OR REPLACE FUNCTION public.get_accounts (uuid)
  RETURNS json
  LANGUAGE sql
  STRICT
  AS $function$
  SELECT
    COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]') AS json
  FROM ((
      SELECT
        MAX(accts.id) AS id,
        MAX(accts.name) AS name,
        MAX(accts.accounting_email) AS email,
        MAX(accts.phone) AS phone,
        MAX(locs.state) AS state,
        MAX(locs.city) AS city,
        MAX(locs.street) AS street,
        MAX(locs.zip) AS zip,
        MAX(locs.country) AS country
      FROM
        invoicing_ledger_items invs
        JOIN accounts accts ON accts.id = invs.recipient_id
          AND invs.recipient_type::text = 'Account'::text
          AND (disable_qb_import IS NULL
            OR disable_qb_import IS FALSE)
        LEFT JOIN locations locs ON accts.location_id = locs.id
      WHERE (invs.id IN (
          SELECT
            "list_sent_invoices".id
          FROM
            "list_sent_invoices"
          WHERE
            sender_id = (
              SELECT
                company_id
              FROM
                qb_company_tokens
              WHERE
                token = $1)))
        GROUP BY
          (name)
        UNION
        /* A rather complex way for getting the cash call account ... */
        SELECT
          MAX(accts.id) AS id,
          MAX(accts.name) AS name,
          MAX(accts.accounting_email) AS email,
          MAX(accts.phone) AS phone,
          MAX(locs.state) AS state,
          MAX(locs.city) AS city,
          MAX(locs.street) AS street,
          MAX(locs.zip) AS zip,
          MAX(locs.country) AS country
        FROM
          invoicing_ledger_items invs
          JOIN users ON users.id = invs.recipient_id
            AND invs.recipient_type::text = 'User'::text
          JOIN accounts accts ON accts.company_id = invs.sender_id
            AND accts.name::text = 'Cash Call'::text
            AND invs.recipient_type::text = 'User'::text
        LEFT JOIN locations locs ON accts.location_id = locs.id
      WHERE (invs.id IN (
          SELECT
            "list_sent_invoices".id
          FROM
            "list_sent_invoices"
          WHERE
            sender_id = (
              SELECT
                company_id
              FROM
                qb_company_tokens
              WHERE
                token = $1)))
        GROUP BY
          (name))
      ORDER BY
        id) t
$function$
