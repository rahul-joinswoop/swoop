CREATE OR REPLACE FUNCTION public.dispatch_to_all_partners (fleet_company_id integer)
  RETURNS void
  LANGUAGE plpgsql
  AS $function$
DECLARE
  new_rescue_providers_row RECORD;
  DECLARE deleted_rescue_providers_row RECORD;
BEGIN
  IF EXISTS (
    SELECT
      1
    FROM
      companies
    WHERE
      id = fleet_company_id
      AND TYPE = 'FleetCompany'
      AND in_house IS NOT TRUE) THEN
  FOR new_rescue_providers_row IN (
    SELECT
      companies.id rescue_company_id,
      sites.id rescue_site_id,
      fleet_company_id fleet_managed_id
    FROM
      companies
      JOIN sites ON sites.company_id = companies.id
        AND sites.dispatchable IS TRUE
        AND sites.deleted_at IS NULL
    WHERE
      companies.type = 'RescueCompany'
      AND companies.deleted_at IS NULL
      AND NOT EXISTS (
        SELECT
          id
        FROM
          rescue_providers
        WHERE
          provider_id = companies.id
          AND company_id = fleet_company_id
          AND site_id = sites.id)
      ORDER BY
        rescue_company_id,
        rescue_site_id)
    LOOP
      INSERT INTO rescue_providers (provider_id, site_id, company_id, created_at, updated_at)
    VALUES (new_rescue_providers_row.rescue_company_id, new_rescue_providers_row.rescue_site_id, fleet_company_id, now(), now());
    END LOOP;
  FOR deleted_rescue_providers_row IN (
    SELECT
      rescue_provider.id id
    FROM
      rescue_providers rescue_provider
      JOIN companies rescue_company ON rescue_company.id = rescue_provider.provider_id
        AND rescue_company.deleted_at IS NULL
      JOIN sites site ON site.company_id = rescue_company.id
        AND site.dispatchable IS TRUE
        AND site.deleted_at IS NULL
    WHERE
      rescue_provider.deleted_at IS NOT NULL
      AND rescue_provider.company_id = fleet_company_id)
  LOOP
    UPDATE
      rescue_providers
    SET
      deleted_at = NULL
    WHERE
      id = deleted_rescue_providers_row.id;
  END LOOP;
END IF;
END;
$function$
