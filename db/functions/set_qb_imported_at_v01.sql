CREATE OR REPLACE FUNCTION public.set_qb_imported_at (uuid, timestamp WITH time zone)
  RETURNS void
  LANGUAGE sql
  AS $function$
  UPDATE
    invoicing_ledger_items AS invs
  SET
    qb_imported_at = $2,
    qb_pending_import = NULL
  WHERE
    sender_id = (
      SELECT
        company_id
      FROM
        qb_company_tokens
      WHERE
        token = $1)
$function$
