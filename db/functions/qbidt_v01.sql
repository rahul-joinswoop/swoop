CREATE OR REPLACE FUNCTION public.qbidt (ts timestamp without time zone)
  RETURNS text
  LANGUAGE plpgsql
  IMMUTABLE
  AS $function$
DECLARE
  var_result text;
BEGIN
  var_result := to_char(ts, 'YYYY-MM-DD"T"HH24:MI:SS.MS"+00:00"');
  RETURN var_result;
END;
$function$
