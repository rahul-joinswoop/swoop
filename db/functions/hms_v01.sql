CREATE OR REPLACE FUNCTION public.hms (i interval)
  RETURNS text
  LANGUAGE plpgsql
  IMMUTABLE
  AS $function$
BEGIN
  RETURN to_char(i, 'HH24:MI:SS');
END;
$function$
