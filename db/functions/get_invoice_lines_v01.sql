CREATE OR REPLACE FUNCTION public.get_invoice_lines (uuid)
  RETURNS json
  LANGUAGE sql
  STRICT
  AS $function$
  SELECT
    COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]') AS json
  FROM (
    SELECT
      lines.ledger_item_id AS invoice_id,
      lines.type,
      lines.net_amount,
      lines.tax_amount,
      lines.description AS item_name,
      lines.quantity,
      lines.notes AS item_descr
    FROM
      invoicing_line_items lines
    WHERE (lines.ledger_item_id IN (
        SELECT
          "list_sent_invoices".id
        FROM
          "list_sent_invoices"
        WHERE
          sender_id = (
            SELECT
              company_id
            FROM
              qb_company_tokens
            WHERE
              token = $1)))
        AND NOT (description = 'Tax'
          AND tax_amount = 0)
        AND lines.deleted_at IS NULL
      ORDER BY
        invoice_id,
        created_at,
        tax_amount) t
$function$
