CREATE OR REPLACE FUNCTION public.get_user (uuid)
  RETURNS json
  LANGUAGE sql
  STRICT
  AS $function$
  SELECT
    COALESCE(row_to_json(t.*), '{}') AS json
  FROM (
    SELECT
      name AS first_name,
      business AS username,
      phone
    FROM
      companies cos
      INNER JOIN qb_company_tokens qcts ON cos.id = qcts.company_id
    WHERE (qcts.token = $1)) t
$function$
