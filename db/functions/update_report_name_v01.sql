CREATE OR REPLACE FUNCTION public.update_report_name (old_name character varying, new_name character varying)
  RETURNS void
  LANGUAGE plpgsql
  AS $function$
BEGIN
  UPDATE
    reports
  SET
    name = new_name
  WHERE
    name = old_name;
  IF found <> TRUE THEN
    RETURN;
  END IF;
END;
$function$
