CREATE OR REPLACE FUNCTION public.merge_reports (KEY character varying, c_type character varying, report_sql text, def_params json, a_params json, FILTER json, m_params json, avail_params json, display_ord integer DEFAULT NULL::integer, display_nm character varying DEFAULT NULL::character varying)
  RETURNS character varying
  LANGUAGE plpgsql
  AS $function$
DECLARE
  report_version VARCHAR;
BEGIN
  LOOP
    SELECT
      SUBSTR(MD5(report_sql), 0, 7) INTO report_version;
    UPDATE
      reports
    SET
      sql = report_sql,
      default_params = def_params,
      all_params = a_params,
      filter_sql = FILTER,
      company_type = c_type,
      mandatory_params = m_params,
      available_params = avail_params,
      updated_at = now(),
      version = report_version,
      display_order = display_ord,
      display_name = display_nm
    WHERE
      name = KEY;
    IF found THEN
      -- We updated a report go ahead and let the consumer know.
      RETURN CONCAT(KEY, CONCAT(' ', CONCAT('Updated to ', report_version)));
    END IF;
    -- not there, so try to insert the key
    -- if someone else inserts the same key concurrently,
    -- we could get a unique-key failure
    BEGIN
      INSERT INTO reports (name, sql, default_params, all_params, filter_sql, mandatory_params, available_params, company_type, created_at, updated_at, version, display_order, display_name)
      VALUES (KEY, report_sql, def_params, a_params, FILTER, m_params, avail_params, c_type, now(), now(), SUBSTR(MD5(report_sql), 0, 7), display_ord, display_nm);
    -- Start providing Feed back of the report being instered
    RETURN CONCAT(KEY, CONCAT(' ', CONCAT('Inserted at ', report_version)));
    EXCEPTION
      WHEN unique_violation THEN
        -- do nothing, and loop to try the UPDATE again
    END;
  END LOOP;
END;

$function$
