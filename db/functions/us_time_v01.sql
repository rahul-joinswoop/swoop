CREATE OR REPLACE FUNCTION public.us_time (ts timestamp without time zone, tz text)
  RETURNS text
  LANGUAGE plpgsql
  IMMUTABLE
  AS $function$
BEGIN
  RETURN to_char(ts at time zone 'UTC' AT TIME ZONE COALESCE(tz, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI');
END;
$function$
