CREATE OR REPLACE FUNCTION public.get_invoice_headers (uuid)
  RETURNS json
  LANGUAGE sql
  STRICT
  AS $function$
  SELECT
    COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]') AS json
  FROM ( SELECT DISTINCT ON (invs.id)
      invs.id,
      concat('SW', invs.job_id) AS job_id,
      invs.issue_date,
      invs.total_amount,
      invs.state,
      invs.identifier,
      invs.description,
      invs.invoice_notes,
      invs.due_date,
      CASE WHEN (jobs.status = 'Completed') THEN
        qbi_adjusted_ajs (jobs.id,
          7)
      ELSE
        CASE WHEN (jobs.status = 'GOA') THEN
          qbi_adjusted_ajs (jobs.id,
            21)
        ELSE
          CASE WHEN (jobs.status = 'Released') THEN
            qbi_adjusted_ajs (jobs.id,
              18)
          ELSE
            CASE WHEN (jobs.status = 'Canceled') THEN
              qbi_adjusted_ajs (jobs.id,
                8)
            ELSE
              qbidt (utc_now ())
            END
          END
        END
      END AS created_at,
      accts.name AS account_name,
      accts.accounting_email AS email,
      accts.phone,
      locs.state,
      locs.city,
      locs.street,
      locs.zip,
      locs.country,
      btrim(concat(users.first_name, ' ', users.last_name))::character varying AS customer_name,
      users.phone AS customer_phone,
      jobs.po_number
    FROM
      invoicing_ledger_items invs
    LEFT JOIN jobs ON invs.id = jobs.invoice_id
    LEFT JOIN settings ON invs.sender_id = settings.company_id
      AND (settings.key = 'QB Ignore Canceled Invoices')
    LEFT JOIN drives ON jobs.driver_id = drives.id
    LEFT JOIN users ON users.id = drives.user_id
    JOIN accounts accts ON accts.id = invs.recipient_id
      AND invs.recipient_type::text = 'Account'::text
      AND (disable_qb_import IS NULL
        OR disable_qb_import IS FALSE)
    LEFT JOIN locations locs ON accts.location_id = locs.id
  WHERE (invs.id IN (
      SELECT
        "list_sent_invoices".id
      FROM
        "list_sent_invoices"
      WHERE
        sender_id = (
          SELECT
            company_id
          FROM
            qb_company_tokens
          WHERE
            token = $1)))
      AND invs.deleted_at IS NULL
      AND (settings.value IS NULL
        OR (settings.key = 'QB Ignore Canceled Invoices'
          AND jobs.status != 'Canceled'))
      AND invs.total_amount > 0
    UNION
    /* joins invoice with users instead for cash calls */
    SELECT DISTINCT ON (invs.id)
      invs.id,
      concat('SW', invs.job_id) AS job_id,
      invs.issue_date,
      invs.total_amount,
      invs.state,
      invs.identifier,
      invs.description,
      invs.invoice_notes,
      invs.due_date,
      CASE WHEN (jobs.status = 'Completed') THEN
        qbi_adjusted_ajs (jobs.id,
          7)
      ELSE
        CASE WHEN (jobs.status = 'GOA') THEN
          qbi_adjusted_ajs (jobs.id,
            21)
        ELSE
          CASE WHEN (jobs.status = 'Released') THEN
            qbi_adjusted_ajs (jobs.id,
              18)
          ELSE
            CASE WHEN (jobs.status = 'Canceled') THEN
              qbi_adjusted_ajs (jobs.id,
                8)
            ELSE
              qbidt (utc_now ())
            END
          END
        END
      END AS created_at,
      accts.name AS account_name,
      accts.accounting_email AS email,
      accts.phone,
      locs.state,
      locs.city,
      locs.street,
      locs.zip,
      locs.country,
      btrim(concat(users.first_name, ' ', users.last_name))::character varying AS customer_name,
      users.phone AS customer_phone,
      jobs.po_number
    FROM
      invoicing_ledger_items invs
    LEFT JOIN jobs ON invs.id = jobs.invoice_id
    LEFT JOIN settings ON invs.sender_id = settings.company_id
      AND (settings.key = 'QB Ignore Canceled Invoices')
    JOIN users ON users.id = invs.recipient_id
      AND invs.recipient_type::text = 'User'::text
    JOIN accounts accts ON accts.company_id = invs.sender_id
      AND accts.name::text = 'Cash Call'::text
      AND invs.recipient_type::text = 'User'::text
  LEFT JOIN locations locs ON accts.location_id = locs.id
WHERE (invs.id IN (
    SELECT
      "list_sent_invoices".id
    FROM
      "list_sent_invoices"
    WHERE
      sender_id = (
        SELECT
          company_id
        FROM
          qb_company_tokens
        WHERE
          token = $1)))
    AND invs.deleted_at IS NULL
    AND (settings.value IS NULL
      OR (settings.key = 'QB Ignore Canceled Invoices'
        AND jobs.status != 'Canceled'))
    AND invs.total_amount > 0) t
$function$
