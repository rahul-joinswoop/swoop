class RemoveRoadsideFromJob < ActiveRecord::Migration[5.0]
  def change
    remove_column :jobs, :roadside, :boolean
  end
end
