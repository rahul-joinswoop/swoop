class AddClubJobTypeToIsscDispatchRequest < ActiveRecord::Migration[4.2]
  def change
    add_column :issc_dispatch_requests, :club_job_type, :string
  end
end
