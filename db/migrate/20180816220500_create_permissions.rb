class CreatePermissions < ActiveRecord::Migration[5.1]

  def change
    create_table :permissions do |t|
      t.references :role, foreign_key: true
      t.string :action
      t.string :subject_type
      t.integer :subject_id
      t.string :name

      t.timestamps
    end
    add_index :permissions, :action
    add_index :permissions, :subject_type
    add_index :permissions, :subject_id
  end

end
