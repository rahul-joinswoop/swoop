class AddSystemToIssc < ActiveRecord::Migration[5.0]
  def change
    add_column :isscs, :system, :string
  end
end
