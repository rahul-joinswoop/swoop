class AddStreetAddressAndStreetName < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :street_number, :string, limit: 12
    add_column :locations, :street_name, :string, limit: 60
  end
end
