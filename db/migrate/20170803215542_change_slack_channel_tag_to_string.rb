class ChangeSlackChannelTagToString < ActiveRecord::Migration[4.2]
  def change
    change_column :slack_channels, :tag, :string
  end
end
