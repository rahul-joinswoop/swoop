class CreateSurveyQuestions < ActiveRecord::Migration[4.2]
  def change
    create_table :survey_questions do |t|
      t.references :survey, index: true, foreign_key: true
      t.string :type
      t.string :question
      t.string :description
      t.integer :order
      t.string :ref

      t.timestamps null: false
    end
  end
end
