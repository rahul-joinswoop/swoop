class AddWaiversToCompany < ActiveRecord::Migration[4.2]
  def change
    add_column :companies, :pickup_waiver, :text
    add_column :companies, :dropoff_waiver, :text
  end
end
