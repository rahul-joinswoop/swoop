class CreateAssignments < ActiveRecord::Migration[5.0]
  def change
    create_table :assignments do |t|
      t.references :job, foreign_key: true, null: false
      t.references :partner, foreign_key: { to_table: :companies }, null: false
      t.references :site, foreign_key: true, null: false
      t.references :job_status, foreign_key: true
      t.boolean  :partner_live
      t.boolean  :partner_open
      t.timestamp :deleted_at
      t.timestamps
    end
  end
end
