class AddResponseAndFollowUpToAuditSms < ActiveRecord::Migration[4.2]
  def change
    add_column :audit_sms, :response, :string
    add_column :audit_sms, :parsed_response, :integer
    add_index :audit_sms, :parsed_response
    add_column :audit_sms, :followed_up_at, :datetime
    add_index :audit_sms, :followed_up_at
  end
end
