class AddSourceApplicationToAuditSms < ActiveRecord::Migration[5.2]
  def change
    add_reference :audit_sms, :source_application, index: false, foreign_key: true
  end
end
