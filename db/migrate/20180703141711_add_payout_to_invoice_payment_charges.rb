class AddPayoutToInvoicePaymentCharges < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_payment_charges, :upstream_payout_id, :string
    add_column :invoice_payment_charges, :upstream_payout_created_at, :datetime
  end
end
