class AddUniqueIndexToRatings < ActiveRecord::Migration[5.0]
  def change
    add_index :ratings,
              [:client_company_id, :date, :rescue_company_id],
              unique: true,
              name: 'index_client_date_rescue'
  end
end
