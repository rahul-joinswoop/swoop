class CreateJoinTableCompanyGroup < ActiveRecord::Migration[5.0]
  def change
    create_join_table :companies, :groups do |t|
      t.index [:company_id, :group_id]
      t.index [:group_id, :company_id]
    end
  end
end
