class AddAddonIntoUniqueIndexOnServiceCodes < ActiveRecord::Migration[4.2]
  def change
    remove_index :service_codes, :name

    add_index :service_codes, [:name, :addon], unique: true
  end
end
