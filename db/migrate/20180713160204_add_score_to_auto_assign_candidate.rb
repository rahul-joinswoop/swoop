class AddScoreToAutoAssignCandidate < ActiveRecord::Migration[5.0]
  def change
    add_column :auto_assign_candidates, :score, :float
  end
end
