class AddIndexToApplicationSourceOnAjs < ActiveRecord::Migration[5.2]

  # DDL transactions need to be disabled in order to create an index concurrently
  disable_ddl_transaction!

  def up
    add_index :audit_job_statuses, [:source_application_id], algorithm: :concurrently, where: 'source_application_id IS NOT NULL'
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:audit_job_statuses, [:source_application_id])
      remove_index :audit_job_statuses, [:source_application_id]
    end

    raise e
  end

  def down
    remove_index :audit_job_statuses, [:source_application_id]
  end
end
