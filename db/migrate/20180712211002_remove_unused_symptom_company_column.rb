class RemoveUnusedSymptomCompanyColumn < ActiveRecord::Migration[5.0]
  def change
    remove_reference :symptoms, :company, foreign_key: true, index: true
    remove_index :company_symptoms, [:company_id]
  end
end
