class AddIsStandardToServiceCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :service_codes, :is_standard, :boolean
  end
end
