class AddRankToClientProgram < ActiveRecord::Migration[5.1]

  def change
    add_column :client_programs, :rank, :float, null: false, default: 0
  end

end
