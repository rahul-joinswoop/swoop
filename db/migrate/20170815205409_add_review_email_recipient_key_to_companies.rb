class AddReviewEmailRecipientKeyToCompanies < ActiveRecord::Migration[4.2]
  def change
    add_column :companies, :review_email_recipient_key, :string
  end
end
