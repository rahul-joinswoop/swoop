class AddDeletedAtToDepartments < ActiveRecord::Migration[4.2]
  def change
    add_column :departments, :deleted_at, :datetime
  end
end
