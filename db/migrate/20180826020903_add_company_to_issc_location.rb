class AddCompanyToIsscLocation < ActiveRecord::Migration[5.1]
  def change
    add_reference :issc_locations, :company, foreign_key: true
  end
end
