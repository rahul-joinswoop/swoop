class AddDateToRatings < ActiveRecord::Migration[5.0]
  def change
    add_column :ratings, :date, :date
  end
end
