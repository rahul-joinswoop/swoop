class AddDispatchToAllPartnersFunction < ActiveRecord::Migration[4.2]
  def up
    connection.execute(%q{
      CREATE OR REPLACE FUNCTION dispatch_to_all_partners(fleet_company_id INTEGER) RETURNS VOID AS
      $$
      DECLARE new_rescue_providers_row RECORD;
      DECLARE deleted_rescue_providers_row RECORD;
      BEGIN
          IF EXISTS(SELECT 1 FROM companies WHERE id = fleet_company_id AND type = 'FleetCompany' AND in_house IS NOT TRUE) THEN

              FOR new_rescue_providers_row IN (
                  SELECT companies.id rescue_company_id, sites.id rescue_site_id, fleet_company_id fleet_managed_id
                  FROM companies
                  JOIN sites ON sites.company_id = companies.id AND sites.dispatchable IS TRUE AND sites.deleted_at IS NULL
                  WHERE companies.type = 'RescueCompany'
                  AND companies.deleted_at IS NULL
                  AND NOT EXISTS(
                      SELECT id FROM rescue_providers
                      WHERE provider_id = companies.id
                      AND company_id = fleet_company_id
                      AND site_id = sites.id)
                  ORDER BY rescue_company_id, rescue_site_id
              )

              LOOP
                  INSERT INTO rescue_providers(provider_id, site_id, company_id, created_at, updated_at)
                  VALUES(new_rescue_providers_row.rescue_company_id, new_rescue_providers_row.rescue_site_id, fleet_company_id, now(), now());
              END LOOP;

              FOR deleted_rescue_providers_row IN (
                  SELECT rescue_provider.id id
                  FROM rescue_providers rescue_provider
                  JOIN companies rescue_company ON rescue_company.id = rescue_provider.provider_id AND rescue_company.deleted_at IS NULL
                  JOIN sites site ON site.company_id = rescue_company.id AND site.dispatchable IS TRUE AND site.deleted_at IS NULL
                  WHERE rescue_provider.deleted_at IS NOT NULL
                  AND rescue_provider.company_id = fleet_company_id
              )

              LOOP
                  UPDATE rescue_providers SET deleted_at = NULL where id = deleted_rescue_providers_row.id;
              END LOOP;
          END IF;
      END;
      $$
      LANGUAGE plpgsql;
    })
  end

  def down
    connection.execute(%q{
      DROP FUNCTION IF EXISTS dispatch_to_all_partners(integer);
    })
  end
end
