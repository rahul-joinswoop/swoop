class RemoveAcceptJobMappingFeatureDb < ActiveRecord::Migration[5.2]
  def up
    accept_job_map_feature = Feature.find_by(
      name: 'Accept Job Map',
      description: 'Make Accept Job Map Feature'
    )

    if accept_job_map_feature
      connection.execute(
        %{
          DELETE
          FROM companies_features
          WHERE feature_id = #{accept_job_map_feature.id}
        }
      )

      accept_job_map_feature.destroy
    end
  end

  # This migration is irreversible completely
  def down; end
end
