class AddDispatchToAllPartnersToCompanies < ActiveRecord::Migration[4.2]
  def change
    add_column :companies, :dispatch_to_all_partners, :bool
  end
end
