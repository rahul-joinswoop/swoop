class RenamePCCUserIdOnPCCClaims < ActiveRecord::Migration[5.0]
  def change
    rename_column :pcc_claims, :pcc_user_id, :pcc_driver_id
  end
end
