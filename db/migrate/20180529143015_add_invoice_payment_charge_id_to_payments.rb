class AddInvoicePaymentChargeIdToPayments < ActiveRecord::Migration[5.0]
  def change
    add_reference :invoicing_ledger_items, :invoice_payment_charge, index: true, foreign_key: {
      to_table: :invoice_payment_charges,
    }
  end
end
