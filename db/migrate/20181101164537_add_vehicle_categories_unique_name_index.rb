class AddVehicleCategoriesUniqueNameIndex < ActiveRecord::Migration[5.1]
  def change
    add_index :vehicle_categories, :name, unique: true
  end
end
