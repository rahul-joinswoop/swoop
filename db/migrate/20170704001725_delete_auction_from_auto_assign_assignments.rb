class DeleteAuctionFromAutoAssignAssignments < ActiveRecord::Migration[4.2]
  def change
    remove_column :auto_assign_assignments, :max_distance
    remove_column :auto_assign_assignments, :service_code_id
    remove_column :auto_assign_assignments, :expires_at
    remove_column :auto_assign_assignments, :min_target_eta
    remove_column :auto_assign_assignments, :max_target_eta
    remove_column :auto_assign_assignments, :max_eta
    remove_column :auto_assign_assignments, :duration_secs
  end
end
