class AddSitesAutoAssignDistance < ActiveRecord::Migration[5.0]
  def change
    add_column :rescue_providers, :auto_assign_distance, :integer, null: true, limit: 1
  end
end
