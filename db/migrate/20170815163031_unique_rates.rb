class UniqueRates < ActiveRecord::Migration[4.2]
  def change
     ActiveRecord::Base.connection.execute "CREATE UNIQUE INDEX index_rates_unique ON rates (company_id,type,service_code_id,account_id,site_id,vehicle_category_id) WHERE live"
  end
end
