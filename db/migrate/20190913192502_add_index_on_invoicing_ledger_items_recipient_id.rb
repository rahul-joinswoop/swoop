# frozen_string_literal: true

class AddIndexOnInvoicingLedgerItemsRecipientId < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    add_index :invoicing_ledger_items, :recipient_id, algorithm: :concurrently
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:invoicing_ledger_items, :recipient_id)
      remove_index :invoicing_ledger_items, :recipient_id, algorithm: :concurrently
    end
    raise e
  end

  def down
    remove_index :invoicing_ledger_items, :recipient_id, algorithm: :concurrently
  end

end
