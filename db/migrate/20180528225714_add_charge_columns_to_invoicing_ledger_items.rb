class AddChargeColumnsToInvoicingLedgerItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoicing_ledger_items, :charge_status, :string
  end
end
