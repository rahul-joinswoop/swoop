class AddResultAndClientProgramToPCCCoverage < ActiveRecord::Migration[5.1]

  def change
    add_column :pcc_coverages, :result, :string
    add_reference :pcc_coverages, :client_program, foreign_key: true
  end

end
