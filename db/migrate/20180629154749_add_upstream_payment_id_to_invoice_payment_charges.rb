class AddUpstreamPaymentIdToInvoicePaymentCharges < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_payment_charges, :upstream_payment_id, :string
  end
end
