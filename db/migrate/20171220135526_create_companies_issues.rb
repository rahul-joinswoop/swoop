class CreateCompaniesIssues < ActiveRecord::Migration[4.2]
  def change
    create_table :companies_issues do |t|
      t.references :company, index: true, foreign_key: true, null: false
      t.references :issue, index: true, foreign_key: true, null: false
      t.timestamps null: false
    end

    add_index :companies_issues, [:company_id, :issue_id], unique: true
  end
end
