class AddMaxEtaToAutoAssignAssignments < ActiveRecord::Migration[4.2]
  def change
    remove_column :auto_assign_assignments, :max_target_eta
    add_column :auto_assign_assignments, :min_target_eta, :integer
    add_column :auto_assign_assignments, :max_target_eta, :integer
    add_column :auto_assign_assignments, :max_eta, :integer
  end
end
