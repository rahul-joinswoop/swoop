class CreateGraphQLSubscriptions < ActiveRecord::Migration[5.2]

  def change
    create_table :graphql_subscriptions do |t|
      t.string :query_string, null: false
      t.jsonb :variables, default: {}, null: false
      t.jsonb :context, default: {}, null: false
      t.string :operation_name
      t.timestamps
      t.references :viewer, polymorphic: true, index: true, null: false
    end
    add_index :graphql_subscriptions, [:query_string, :variables, :context], unique: true, name: 'index_subscriptions_on_query_string_and_variables_and_context'
  end

end
