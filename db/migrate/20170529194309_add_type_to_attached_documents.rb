class AddTypeToAttachedDocuments < ActiveRecord::Migration[4.2]
  def change
    add_column :attached_documents, :type, :string
    add_index :attached_documents, :type
  end
end
