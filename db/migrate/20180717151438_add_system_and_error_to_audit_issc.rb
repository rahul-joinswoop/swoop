class AddSystemAndErrorToAuditIssc < ActiveRecord::Migration[5.0]
  def change
    add_column :audit_isscs, :system, :string
    add_column :audit_isscs, :error, :string
  end
end
