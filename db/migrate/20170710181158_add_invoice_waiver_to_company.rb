class AddInvoiceWaiverToCompany < ActiveRecord::Migration[4.2]
  def change
    add_column :companies, :invoice_waiver, :text
  end
end
