class CreateAuctions < ActiveRecord::Migration[4.2]
  def change
    create_table :auctions do |t|
      t.references :job, index: true, foreign_key: true
      t.string :status
      t.string :result
      t.references :service_code, index: true, foreign_key: true
      t.integer :max_bidders
      t.integer :max_candidate_distance
      t.integer :max_candidate_eta
      t.integer :min_target_eta
      t.integer :max_target_eta
      t.integer :duration_secs
      t.datetime :expires_at

      t.timestamps null: false
    end
  end
end
