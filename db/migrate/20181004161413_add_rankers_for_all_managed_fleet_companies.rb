class AddRankersForAllManagedFleetCompanies < ActiveRecord::Migration[5.1]
  def up
    FleetCompany.where("in_house IS NOT TRUE").each do |managed_fleet_company|
      managed_fleet_company.get_or_create_ranker
    end
  end
  def down
  end
end
