# frozen_string_literal: true

class CaseInsensitiveSitesNameIndex < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    # Make sure there are no duplicates
    last_name = nil
    num = 1
    transaction do
      select_all("SELECT DISTINCT s2.id, s2.company_id, s2.name, s2.type FROM sites s1, sites s2 WHERE s1.company_id = s2.company_id AND s1.name = s2.name AND s1.id < s2.id AND s1.deleted_at IS NULL AND s2.deleted_at IS NULL AND s1.type = s2.type order by s2.company_id, s2.name").each do |row|
        name = "#{row['company_id']}:#{row['name']}:#{row['type']}".downcase
        if name == last_name
          num += 1
        else
          num = 1
          last_name = name
        end
        update("UPDATE sites SET name = CONCAT(name, ' (#{num})') WHERE id = #{row['id']}")
      end
    end

    add_index :sites, [:company_id, :name, :type], unique: true, name: "index_sites_on_unique_name", where: "deleted_at IS NULL", algorithm: :concurrently
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:sites, [:company_id, :name, :type], name: "index_sites_on_unique_name")
      remove_index :sites, name: "index_sites_on_unique_name", algorithm: :concurrently
    end
    raise e
  end

  def down
    remove_index :sites, name: "index_sites_on_unique_name", algorithm: :concurrently
  end

end
