class AddBrowserVersionBuildToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :browser, :string
    add_column :users, :version, :string
    add_column :users, :build, :string
  end
end
