class AddCostAndRatingToAutoAssignCandidate < ActiveRecord::Migration[5.0]
  def change
    add_column :auto_assign_candidates, :cost, :integer
    add_column :auto_assign_candidates, :rating, :integer
  end
end
