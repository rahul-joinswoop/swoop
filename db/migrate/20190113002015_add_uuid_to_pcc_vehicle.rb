class AddUuidToPCCVehicle < ActiveRecord::Migration[5.1]

  def change
    add_column :pcc_vehicles, :uuid, :string, limit: 32
  end

end
