class AddCoverageCustomIssuesToClientPrograms < ActiveRecord::Migration[5.2]
  def change
    add_column :client_programs, :coverage_custom_issues, :text, array: true
  end
end
