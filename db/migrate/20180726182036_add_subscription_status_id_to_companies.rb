class AddSubscriptionStatusIdToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :subscription_status_id, :integer, foreign_key: true
  end
end
