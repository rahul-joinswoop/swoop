class AddUniqueUserNameConstraintWithConditions < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    add_index :users, [:company_id, :last_name, :first_name], unique: true, algorithm: :concurrently, where: "(deleted_at IS NULL AND company_id IS NOT NULL)"

    # Remove index from aborted migration.
    if index_exists?(:users, [:first_name, :last_name, :company_id])
      remove_index :users, [:first_name, :last_name, :company_id]
    end
  end

  def down
    remove_index :users, [:company_id, :last_name, :first_name]
  end

end
