class AddRefererToCompanies < ActiveRecord::Migration[4.2]
  def change
    add_reference :companies, :referer, index: true, foreign_key: true
  end
end
