class AddDescriptionAndVinToPCCVehicles < ActiveRecord::Migration[5.0]
  def change
    add_column :pcc_vehicles, :description, :string, not_null: true
    add_column :pcc_vehicles, :vin, :string, not_null: true
  end
end
