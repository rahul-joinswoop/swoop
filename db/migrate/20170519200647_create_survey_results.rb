class CreateSurveyResults < ActiveRecord::Migration[4.2]
  def change
    create_table :survey_results do |t|
      t.references :job, index: true, foreign_key: true
      t.references :review, index: true, foreign_key: true
      t.references :survey_question, index: true, foreign_key: true
      t.integer :int_value
      t.string :string_value

      t.timestamps null: false
    end
  end
end
