class AddExportErrorMsgToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :invoicing_ledger_items, :export_error_msg, :string
  end
end
