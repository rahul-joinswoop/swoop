#
# This migration updates the legacy WeightedSumRanker's to use the new
# names and data types
#
class UpdateRankersToFloatsAndIntsAndNewName < ActiveRecord::Migration[5.1]
  def up
    Auction::WeightedSumRanker.all.each do |wsr|
      # Step 0: Keep track of whether this WeightedSumRanker has been updated
      record_changed = false
      # Step 1: Update the name of the WeightedSumRanker (we no longer call it 'speed > quality = cost')
      if wsr.name == "speed > quality = cost" || wsr.name == "speed > quality = cost "
        wsr.name = "Fully speed weighted (ETA)"
        record_changed = true
      end
      # Step 2: Cast the weights to floats, and mark the field as dirty due to https://github.com/rails/rails/issues/15146
      unless wsr.weights.nil? || wsr.weights.empty?
        wsr.weights = wsr.weights.transform_values { |v| v.to_f }
        wsr.weights_will_change!
        record_changed = true
      end
      # Step 3: Cast the constraints to floats, and mark the field as dirty due to https://github.com/rails/rails/issues/15146
      unless wsr.constraints.nil? || wsr.constraints.empty?
        wsr.constraints = wsr.constraints.transform_values { |v| v.to_i }
        wsr.constraints_will_change!
        record_changed = true
      end
      # Step 4: Save the updated WeightedSumRanker if we changed any part of it
      wsr.save! if record_changed
    end
  end
  def down
    # Intentionally blank
  end
end
