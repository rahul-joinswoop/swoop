class AddDispatchidIndexToAuditIssc < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :audit_isscs, :dispatchid, algorithm: :concurrently
  end
end
