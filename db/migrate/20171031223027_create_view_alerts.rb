class CreateViewAlerts < ActiveRecord::Migration[4.2]
  def change
    create_table :view_alerts do |t|
      t.references :job, index: true, foreign_key: true
      t.boolean :customer_says_not_on_site
      t.boolean :customer_says_not_complete

      t.timestamps null: false
    end
  end
end
