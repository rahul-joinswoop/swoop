class CreateInvoicePaymentCharges < ActiveRecord::Migration[5.0]
  def change
    create_table :invoice_payment_charges do |t|
      t.references :payment, index: true, foreign_key: { to_table: :invoicing_ledger_items }
      t.references :invoice, index: true, foreign_key: { to_table: :invoicing_ledger_items }
      t.string :status, null: false
      t.string :last_4
      t.string :upstream_charge_id
      t.json :data
      t.timestamps
    end

    remove_index :invoice_payment_charges, :payment_id
    add_index :invoice_payment_charges, [:payment_id, :invoice_id], unique: true
  end
end
