class CreateCompaniesLocationTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :companies_location_types do |t|
      t.references  :company,        index: true, foreign_key: true
      t.references  :location_type,  index: true, foreign_key: true

      t.timestamp   :deleted_at
      t.timestamps  null: false
    end

    add_index :companies_location_types, [ :company_id, :location_type_id ], unique: true,
              name: :index_companies_location_types_on_company_and_location_type
  end
end
