class ChangeDataAndUrlNullInBoschJobs < ActiveRecord::Migration[4.2]
  def change
    change_column_null :bosch_jobs, :data, true
    change_column_null :bosch_jobs, :url, true
  end
end
