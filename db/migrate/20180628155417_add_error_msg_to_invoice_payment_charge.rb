class AddErrorMsgToInvoicePaymentCharge < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_payment_charges, :error_msg, :string
  end
end
