class AddSupportStorageToServiceCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :service_codes, :support_storage, :boolean

    change_column :service_codes, :support_storage, :boolean, default: true
  end
end
