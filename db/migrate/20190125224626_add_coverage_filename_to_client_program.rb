class AddCoverageFilenameToClientProgram < ActiveRecord::Migration[5.1]

  def change
    add_column :client_programs, :coverage_filename, :string
  end

end
