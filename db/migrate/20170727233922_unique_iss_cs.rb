class UniqueIssCs < ActiveRecord::Migration[4.2]
  def change
    ActiveRecord::Base.connection.execute "CREATE UNIQUE INDEX index_isscs_unique_live ON isscs (contractorid,clientid,issc_location_id) WHERE (deleted_at is null)"
  end
end
