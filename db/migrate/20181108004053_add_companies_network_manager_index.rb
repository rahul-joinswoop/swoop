class AddCompaniesNetworkManagerIndex < ActiveRecord::Migration[5.1]

  disable_ddl_transaction!

  def up
    add_index :companies, :network_manager_id, algorithm: :concurrently
  end

  def down
    remove_index :companies, :network_manager_id
  end

end
