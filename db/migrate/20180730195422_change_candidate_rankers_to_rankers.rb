class ChangeCandidateRankersToRankers < ActiveRecord::Migration[5.0]
  def change
    rename_table :candidate_rankers, :rankers
  end
end
