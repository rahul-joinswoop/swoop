class BackfillDistanceUnits < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  DEFAULT_DISTANCE_UNIT = ENV.fetch('DEFAULT_DISTANCE_UNIT', 'mi')
  def change
    Company.unscoped.in_batches do |relation|
      relation.update_all distance_unit: DEFAULT_DISTANCE_UNIT
      sleep(0.1)
    end
  end
end
