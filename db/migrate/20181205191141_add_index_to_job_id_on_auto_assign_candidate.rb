class AddIndexToJobIdOnAutoAssignCandidate < ActiveRecord::Migration[5.1]

  disable_ddl_transaction!

  def change
    add_index :auto_assign_candidates, :job_id, algorithm: :concurrently
  end

end
