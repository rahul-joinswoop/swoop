class RemoveChargeStatusFromInvoice < ActiveRecord::Migration[5.0]
  def change
    remove_column :invoicing_ledger_items, :charge_status
  end
end
