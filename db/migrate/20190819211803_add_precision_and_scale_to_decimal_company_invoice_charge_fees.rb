class AddPrecisionAndScaleToDecimalCompanyInvoiceChargeFees < ActiveRecord::Migration[5.2]
  def up
    change_column :company_invoice_charge_fees, :fixed_value, :decimal, null: false, precision: 20, scale: 4
  end

  def down
    change_column :company_invoice_charge_fees, :fixed_value, :decimal, null: false
  end
end
