class CreateLookerDashboards < ActiveRecord::Migration[4.2]
  def change
    create_table :looker_dashboards do |t|
      t.string :name
      t.string :path
      t.timestamps null: false
      t.datetime :deleted_at
    end
    
    create_table :companies_looker_dashboards do |t|
      t.references  :company,          index: true, foreign_key: true
      t.references  :looker_dashboard, index: true, foreign_key: true
      t.timestamps null: false
      t.datetime :deleted_at
    end
  end
end
