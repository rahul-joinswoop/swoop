class AddInvoiceAttachmentToAttachedDocuments < ActiveRecord::Migration[4.2]
  def change
    add_column :attached_documents, :invoice_attachment, :boolean
  end
end
