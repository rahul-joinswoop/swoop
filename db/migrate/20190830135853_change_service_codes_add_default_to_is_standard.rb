class ChangeServiceCodesAddDefaultToIsStandard < ActiveRecord::Migration[5.2]
  def change
    safety_assured { change_column :service_codes, :is_standard, :boolean, default: false }
  end
end
