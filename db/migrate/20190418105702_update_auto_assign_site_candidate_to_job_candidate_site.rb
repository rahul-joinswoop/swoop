class UpdateAutoAssignSiteCandidateToJobCandidateSite < ActiveRecord::Migration[5.1]
  def change
    Job::Candidate.where(type: 'AutoAssign::SiteCandidate').find_in_batches(batch_size: 100) do |batch|
      Job::Candidate.transaction do
        batch.each do |candidate|
          candidate.update_column(:type, 'Job::Candidate::Site')
        end
      end
    end
  end
end
