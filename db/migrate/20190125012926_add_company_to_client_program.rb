class AddCompanyToClientProgram < ActiveRecord::Migration[5.1]

  def change
    add_reference :client_programs, :company, foreign_key: true
  end

end
