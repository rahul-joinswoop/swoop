class RenameAuctionCandidateRankerToRanker < ActiveRecord::Migration[5.0]
  def change
    rename_column :auctions, :candidate_ranker_id, :ranker_id
  end
end
