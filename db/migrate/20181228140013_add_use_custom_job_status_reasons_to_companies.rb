class AddUseCustomJobStatusReasonsToCompanies < ActiveRecord::Migration[5.1]

  def change
    add_column :companies, :use_custom_job_status_reasons, :boolean
  end

end
