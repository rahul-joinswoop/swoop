class CreateSystemEmailFilters < ActiveRecord::Migration[4.2]
  def change
    create_table :system_email_filters do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :company, index: true, foreign_key: true, null: false
      t.timestamps null: false
    end

    add_index :system_email_filters, [:user_id, :company_id], unique: true
  end
end
