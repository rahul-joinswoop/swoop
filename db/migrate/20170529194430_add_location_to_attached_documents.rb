class AddLocationToAttachedDocuments < ActiveRecord::Migration[4.2]
  def change
    add_reference :attached_documents, :location, index: true, foreign_key: true
  end
end
