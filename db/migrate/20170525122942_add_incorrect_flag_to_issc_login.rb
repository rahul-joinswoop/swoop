class AddIncorrectFlagToIsscLogin < ActiveRecord::Migration[4.2]
  def change
    add_column :issc_logins, :incorrect, :boolean
  end
end
