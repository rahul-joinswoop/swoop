class CreateAuditCompanySubscriptionStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :audit_company_subscription_statuses do |t|
      t.references :company, foreign_key: true, null: false, index: { name: 'idx_companies_status_subscriptions' }
      t.references :subscription_status, foreign_key: true, null: false, index: { name: 'idx_status_subscriptions_companies' }
      t.timestamps
    end
  end
end
