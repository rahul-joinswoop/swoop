class ChangeAutoAssignCandidateColumnVehicleNull < ActiveRecord::Migration[4.2]
  def change
    change_column_null :auto_assign_candidates, :vehicle_id, true
  end
end
