class AddCustomerLookupTypeToJob < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :customer_lookup_type, :string
  end
end
