class AddColumnsToAPIAsyncRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :api_async_requests, :external_request_url, :string
    add_column :api_async_requests, :external_request_headers, :string
    add_column :api_async_requests, :external_request_body, :text

    add_column :api_async_requests, :external_response_code, :string
    add_column :api_async_requests, :external_response_headers, :string
    add_column :api_async_requests, :external_response_body, :text

    add_column :api_async_requests, :target_id, :integer
    add_column :api_async_requests, :system, :string
  end
end