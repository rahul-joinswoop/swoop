class CreateSystemAlerts < ActiveRecord::Migration[4.2]
  def change
    create_table :system_alerts do |t|
      t.string :type
      t.references :job, index: true, foreign_key: true
      t.datetime :send_at
      t.datetime :sent_at
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
