class AddManualDttmToAuction < ActiveRecord::Migration[4.2]
  def change
    add_column :auctions, :manual_dttm, :datetime
  end
end
