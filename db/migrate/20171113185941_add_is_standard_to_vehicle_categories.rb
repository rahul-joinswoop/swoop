class AddIsStandardToVehicleCategories < ActiveRecord::Migration[4.2]
  def change
    add_column :vehicle_categories, :is_standard, :boolean
  end
end
