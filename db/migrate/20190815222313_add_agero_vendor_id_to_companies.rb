# frozen_string_literal: true

class AddAgeroVendorIdToCompanies < ActiveRecord::Migration[5.2]

  def change
    add_column :companies, :agero_vendor_id, :integer, null: true
  end

end
