class CreateRolePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :company_role_permissions do |t|
      t.string :permission_type, null: false
      t.references :company, null: false
      t.references :role, nul: false

      t.timestamps
    end

    add_index :company_role_permissions, [:permission_type, :company_id, :role_id],
              unique: true,
              name: :index_company_role_permissions_on_permission_company_and_role
  end
end
