class AddQBPropertiesToInvoicingLedgerItems < ActiveRecord::Migration[4.2]
  def change
    add_column :invoicing_ledger_items, :qb_pending_import, :bool
  end
end
