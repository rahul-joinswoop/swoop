class AddServiceCodeIdToCommissionExclusions < ActiveRecord::Migration[4.2]
  def change
    add_reference :commission_exclusions, :service_code, index: true, foreign_key: true
  end
end
