class UniqueIsscDispatchRequests < ActiveRecord::Migration[5.1]
  def up
    remove_duplicates!
    remove_index :issc_dispatch_requests, :dispatchid
    add_index :issc_dispatch_requests, [:dispatchid, :issc_id], unique: true
  end

  def down
    add_index :issc_dispatch_requests, :dispatchid
    remove_index :issc_dispatch_requests, [:dispatchid, :issc_id]
  end

  private

  def remove_duplicates!
    duplicates_sql = <<~SQL
      SELECT DISTINCT r1.*
      FROM issc_dispatch_requests r1, issc_dispatch_requests r2
      WHERE r1.dispatchid = r2.dispatchid
        AND r1.issc_id = r2.issc_id
        AND r1.id <> r2.id
      ORDER BY r1.id
    SQL

    duplicates = {}
    IsscDispatchRequest.find_by_sql(duplicates_sql).each do |issc_dispatch_request|
      dups = duplicates[issc_dispatch_request.dispatchid]
      unless dups
        dups = []
        duplicates[issc_dispatch_request.dispatchid] = dups
      else
        dups << issc_dispatch_request
      end
    end

    duplicates.each do |dispatchid, dups|
      puts "Found #{dups.size} duplicates of dispatchid #{dispatchid}"
      index = 0
      dups.each do |issc_dispatch_request|
        index += 1
        new_dispatchid = "#{issc_dispatch_request.dispatchid}-dup#{index}"
        puts "  Update duplicate dispatchid on issc_dispatch_requests(#{issc_dispatch_request.id}) to #{new_dispatchid}"
        issc_dispatch_request.update_column(:dispatchid, new_dispatchid)
      end
    end
  end
end
