class AddSitesUniqueIndex < ActiveRecord::Migration[5.1]

  disable_ddl_transaction!

  def up
    transaction do
      rename_duplicate_sites!
    end
    add_index :sites, [:company_id, :type, :name], unique: true, algorithm: :concurrently, where: "deleted_at IS NULL"
    remove_index :sites, :company_id
  end

  def down
    add_index :sites, :company_id, algorithm: :concurrently
    remove_index :sites, [:company_id, :type, :name]
  end

  private

  def rename_duplicate_sites!
    select_all(
      <<~SQL
        SELECT * FROM (
          SELECT company_id, count(distinct sites.name) AS unique_count, count(*) AS count
          FROM sites
          JOIN companies ON sites.company_id = companies.id
          WHERE sites.deleted_at IS NULL AND companies.deleted_at IS NULL
          GROUP BY company_id, sites.type
        ) AS tmp WHERE unique_count != count;
      SQL
    ).each do |row|
      dup_count = 1
      last_name = nil
      Site.where(company_id: row['company_id']).order(:name).each do |site|
        site.name = "[Unknown]" if site.name.blank?
        if site.name == last_name
          dup_count += 1
          site.name = "#{site.name} (#{dup_count})"
        else
          dup_count = 1
          last_name = site.name
        end
        if site.name_changed?
          site.save!
          puts "changed #{site.company.name} site name: #{site.name}"
        end
      end
    end
  end

end
