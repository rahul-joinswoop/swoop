class AddDisableQBImportToAccount < ActiveRecord::Migration[4.2]
  def change
    add_column :accounts, :disable_qb_import, :boolean
    add_index :accounts, :disable_qb_import
  end
end
