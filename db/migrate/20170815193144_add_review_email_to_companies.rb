class AddReviewEmailToCompanies < ActiveRecord::Migration[4.2]
  def change
    add_column :companies, :review_email, :string
  end
end
