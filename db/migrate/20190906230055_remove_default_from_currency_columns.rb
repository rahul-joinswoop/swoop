class RemoveDefaultFromCurrencyColumns < ActiveRecord::Migration[5.2]
  def change
    [
      :company_invoice_charge_fees,
      :invoice_payment_charges,
      :invoicing_line_items,
      :job_billing_infos,
      :rate_additions,
      :rates,
    ].each do |table|
      change_column_default table, :currency, nil
    end
  end
end
