class AddPayoutDelayDaysToCompanyInvoiceChargeFees < ActiveRecord::Migration[5.0]
  def change
    add_column :company_invoice_charge_fees, :payout_delay_days, :integer
  end
end
