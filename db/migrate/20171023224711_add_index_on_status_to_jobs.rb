class AddIndexOnStatusToJobs < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :jobs, :status, algorithm: :concurrently
  end
end
