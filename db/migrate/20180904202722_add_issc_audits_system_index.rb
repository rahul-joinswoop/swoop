class AddIsscAuditsSystemIndex < ActiveRecord::Migration[5.1]
  disable_ddl_transaction!
  
  def change
    add_index :audit_isscs, :system, algorithm: :concurrently
  end
end
