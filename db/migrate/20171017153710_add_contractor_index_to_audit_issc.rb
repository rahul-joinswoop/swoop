class AddContractorIndexToAuditIssc < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :isscs, :contractorid, algorithm: :concurrently
  end
end
