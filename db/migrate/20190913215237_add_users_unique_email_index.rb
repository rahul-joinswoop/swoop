# frozen_string_literal: true

class AddUsersUniqueEmailIndex < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    add_index :users, :_x_email, name: "index_users_on_unique_email", unique: true, where: "_x_email IS NOT NULL AND deleted_at IS NULL", algorithm: :concurrently
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:users, [:_x_email], name: "index_users_on_unique_email")
      remove_index :users, name: "index_users_on_unique_email"
    end
    raise e
  end

  def down
    remove_index :users, name: "index_users_on_unique_email"
  end

end
