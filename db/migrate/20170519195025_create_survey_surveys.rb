class CreateSurveySurveys < ActiveRecord::Migration[4.2]
  def change
    create_table :surveys do |t|
      t.string :name
      t.string :type
      t.string :ref
      t.references :company, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
