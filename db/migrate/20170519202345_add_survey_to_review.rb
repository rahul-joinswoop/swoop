class AddSurveyToReview < ActiveRecord::Migration[4.2]
  def change
    add_reference :reviews, :survey, index: true, foreign_key: true
  end
end
