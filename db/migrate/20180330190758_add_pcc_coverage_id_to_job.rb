class AddPCCCoverageIdToJob < ActiveRecord::Migration[5.0]
  def change
    add_reference :jobs, :pcc_coverage, foreign_key: true, column: :pcc_coverage_id
  end
end
