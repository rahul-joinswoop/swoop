class AddClientCompanyIdToAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :accounts, :client_company_id, :integer
    add_foreign_key :accounts, :companies, column: :client_company_id

    add_index :accounts, [:company_id, :client_company_id], unique: true
  end
end
