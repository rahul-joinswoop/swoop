class AddVendorIdToRates < ActiveRecord::Migration[5.0]
  def change
    add_column :rates, :vendor_id, :string
  end
end
