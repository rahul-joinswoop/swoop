class AddAutoAssignedToAutoAssignedAssignments < ActiveRecord::Migration[4.2]
  def change
    add_column :auto_assign_assignments, :auto_assigned, :boolean, null: false, default: false
  end
end
