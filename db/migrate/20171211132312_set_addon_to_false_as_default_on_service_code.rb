class SetAddonToFalseAsDefaultOnServiceCode < ActiveRecord::Migration[4.2]
  def change
    change_column :service_codes, :addon, :boolean, default: false, null: false
  end
end
