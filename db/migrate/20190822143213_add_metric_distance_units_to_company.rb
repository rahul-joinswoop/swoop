# frozen_string_literal: true

class AddMetricDistanceUnitsToCompany < ActiveRecord::Migration[5.2]

  # This will blow up if not set as it is difficult to change after the fact
  DEFAULT_DISTANCE_UNIT = ENV.fetch('DEFAULT_DISTANCE_UNIT', 'mi')

  def up
    add_column :companies, :distance_unit, :string
    change_column_default :companies, :distance_unit, DEFAULT_DISTANCE_UNIT
  end

  def down
    remove_column :companies, :distance_unit
  end

end
