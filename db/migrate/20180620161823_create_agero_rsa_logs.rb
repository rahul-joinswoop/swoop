class CreateAgeroRsaLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :agero_rsa_logs do |t|
      t.string :request_type
      t.references :job, foreign_key: true
      t.string :partner_company_id
      t.string :path
      t.string :external_rsa_id
      t.boolean :incoming_request
      t.json :request_data
      t.json :response_data
      t.string :http_response_code

      t.timestamps
    end
  end
end
