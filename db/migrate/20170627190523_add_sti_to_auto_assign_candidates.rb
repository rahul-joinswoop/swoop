class AddStiToAutoAssignCandidates < ActiveRecord::Migration[4.2]
  def change
    add_column :auto_assign_candidates, :type, :string
    add_reference :auto_assign_candidates, :site, index: true, foreign_key: true
    add_reference :auto_assign_candidates, :rescue_provider, index: true, foreign_key: true
  end
end
