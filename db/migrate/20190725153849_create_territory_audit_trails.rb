class CreateTerritoryAuditTrails < ActiveRecord::Migration[5.2]
  def up
    execute <<-DDL
      CREATE TYPE territory_audit_trails_status AS ENUM (
        'created',
        'destroyed'
      );
    DDL

    create_table :territory_audit_trails do |t|
      t.references :user, foreign_key: true, null: false
      t.references :territory, foreign_key: true, null: false
      t.column :action, :territory_audit_trails_status, null: false, index: true

      t.datetime :created_at, null: false
    end
  end

  def down
    drop_table :territory_audit_trails
    execute 'DROP TYPE territory_audit_trails_status;'
  end
end
