class AddInvoiceToInvoicingLedgerItems < ActiveRecord::Migration[4.2]
  def change
    add_reference :invoicing_ledger_items, :invoice, references: :invoicing_ledger_items, index: true
    add_foreign_key :invoicing_ledger_items, :invoicing_ledger_items, column: :invoice_id
  end
end
