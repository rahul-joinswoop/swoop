# Per ENG-5081 and ENG-6852, these tables haven't been used since
# we moved invoicing onto Sidekiq, and are now safe to drop
class DropOldUnusedInvoiceLockTables < ActiveRecord::Migration[5.1]
  def change
    drop_table :new_invoice_locks, id: false do |t|
      t.integer "job_id", null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["job_id"], name: "index_new_invoice_locks_on_job_id", unique: true
    end
    drop_table :update_invoice_locks, id: false do |t|
      t.integer "job_id", null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["job_id"], name: "index_update_invoice_locks_on_job_id", unique: true
    end
  end
end
