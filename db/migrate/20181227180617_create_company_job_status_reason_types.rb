class CreateCompanyJobStatusReasonTypes < ActiveRecord::Migration[5.1]

  def change
    create_table :company_job_status_reason_types do |t|
      t.references :company, foreign_key: true, null: false
      t.references :job_status_reason_type, foreign_key: true, null: false, index: { name: 'index_company_job_status_reason_type_on_status_reason_id' }

      t.timestamps
    end

    add_index :company_job_status_reason_types, [:company_id, :job_status_reason_type_id], unique: true, name: 'index_unique_company_job_status_reason_type'
  end

end
