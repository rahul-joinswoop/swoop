class AddToIndexToAuditSms < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :audit_sms, :to, algorithm: :concurrently
  end
end
