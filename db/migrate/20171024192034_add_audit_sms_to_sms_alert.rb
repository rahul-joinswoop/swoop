class AddAuditSmsToSmsAlert < ActiveRecord::Migration[4.2]
  def change
    add_reference :sms_alerts, :audit_sms, index: true, foreign_key: true
  end
end
