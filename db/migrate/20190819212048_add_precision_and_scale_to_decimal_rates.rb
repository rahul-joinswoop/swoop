# frozen_string_literal: true

class AddPrecisionAndScaleToDecimalRates < ActiveRecord::Migration[5.2]

  COLUMNS = [
    :hourly_p2p,
    :hourly_completion,
    :hookup,
    :miles_towed,
    :miles_towed_free,
    :miles_enroute,
    :miles_enroute_free,
    :special_dolly,
    :gone,
    :storage_daily,
    :hourly,
    :flat,
    :miles_p2p,
    :miles_p2p_free,
    :miles_deadhead,
    :miles_deadhead_free,
  ].freeze

  def up
    change_table(:rates) do |t|
      COLUMNS.each do |col|
        t.change col, :decimal, precision: 20, scale: 4
      end
    end
  end

  def down
    change_table(:rates) do |t|
      COLUMNS.each do |col|
        t.change col, :decimal
      end
    end
  end

end
