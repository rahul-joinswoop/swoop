# frozen_string_literal: true

class AddUniqueIndexOnAnswers < ActiveRecord::Migration[5.2]

  def change
    add_index :answers, [:question_id, :answer], unique: true
  end

end
