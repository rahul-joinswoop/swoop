class RemoveCompanyFromIssues < ActiveRecord::Migration[4.2]
  def change
    remove_column :issues, :company_id
  end
end
