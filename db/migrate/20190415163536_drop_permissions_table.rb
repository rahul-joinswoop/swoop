class DropPermissionsTable < ActiveRecord::Migration[5.1]
  def change
    drop_table :permissions do |t|
      t.references :role, foreign_key: true
      t.string :action
      t.string :subject_type
      t.integer :subject_id
      t.string :name

      t.timestamps
    end
  end

end
