class RemoveCodeIndexOnClientProgram < ActiveRecord::Migration[5.1]
  def change
    remove_index :client_programs, column: :code
  end
end
