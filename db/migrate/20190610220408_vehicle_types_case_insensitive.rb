# frozen_string_literal: true

class VehicleTypesCaseInsensitive < ActiveRecord::Migration[5.2]

  def up
    change_column :vehicle_types, :name, :citext, null: false
    add_index :vehicle_types, :name, unique: true
  end

  def down
    change_column :vehicle_types, :name, :string, null: true
    remove_index :vehicle_types, :name
  end

end
