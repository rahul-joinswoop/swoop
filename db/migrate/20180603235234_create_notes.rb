class CreateNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :notes do |t|
      t.references :job, foreign_key: true
      t.string :type
      t.references :company, foreign_key: true
      t.references :user, foreign_key: true
      t.string :text
      t.timestamp :deleted_at

      t.timestamps
    end
    add_index :notes, :deleted_at
  end
end
