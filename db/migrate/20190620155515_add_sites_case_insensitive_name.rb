# frozen_string_literal: true

class AddSitesCaseInsensitiveName < ActiveRecord::Migration[5.2]

  def up
    add_column :sites, :ci_name, :citext
  end

  def down
    remove_column :sites, :ci_name
  end

end
