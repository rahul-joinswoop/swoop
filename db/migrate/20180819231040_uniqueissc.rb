class Uniqueissc < ActiveRecord::Migration[5.1]
  def change
    ActiveRecord::Base.connection.execute "update isscs set deleted_at=now() where clientid in ('GCO', 'GCOAPI','ALLS') and deleted_at is null"

    add_index :isscs, [:contractorid, :clientid, :issc_location_id], name: 'isscs_unique_index', unique: true, where: "deleted_at is null"
  end
end
