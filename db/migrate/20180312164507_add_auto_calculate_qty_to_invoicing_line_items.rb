class AddAutoCalculateQtyToInvoicingLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoicing_line_items, :auto_calculated_quantity, :boolean, default: true
  end
end
