class AddFeeToInvoicePaymentCharges < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_payment_charges, :fee, :decimal
  end
end
