# frozen_string_literal: true

class AddRescueVehicleCaseInsensitiveName < ActiveRecord::Migration[5.2]
  
  def up
    add_column :vehicles, :ci_name, :citext, null: true
  end
  
  def down
    remove_column :vehicles, :ci_name
  end
  
end
