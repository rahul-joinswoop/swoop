class CreateGraphQLTopicsSubscriptions < ActiveRecord::Migration[5.2]

  def change
    create_join_table :graphql_subscriptions, :graphql_topics
    add_index :graphql_subscriptions_topics, [:graphql_subscription_id, :graphql_topic_id], unique: true, name: 'index_graphql_subscriptions_topics_on_subscription_and_topic'
  end

end
