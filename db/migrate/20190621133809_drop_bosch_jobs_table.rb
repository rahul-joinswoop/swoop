class DropBoschJobsTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :bosch_jobs
  end
end
