class AddApplicationSourceToHistoryItem < ActiveRecord::Migration[5.2]
  def change
    add_reference :history_items, :source_application, index: false, foreign_key: true
  end
end
