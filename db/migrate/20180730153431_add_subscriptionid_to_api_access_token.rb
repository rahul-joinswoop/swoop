class AddSubscriptionidToAPIAccessToken < ActiveRecord::Migration[5.0]
  def change
    add_column :api_access_tokens, :subscriptionid, :string
  end
end
