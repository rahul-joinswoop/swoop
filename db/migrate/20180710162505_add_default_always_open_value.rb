class AddDefaultAlwaysOpenValue < ActiveRecord::Migration[5.0]
  def up
    change_column :sites, :always_open, :boolean, null: false, default: false
    change_column :sites, :force_open, :boolean, null: false, default: false
    change_column :sites, :within_hours, :boolean, null: false, default: true

    add_column :sites, :next_open_check, :datetime
  end

  def down
    change_column :sites, :always_open, :boolean, null: true, default: nil
    change_column :sites, :force_open, :boolean, null: true, default: nil
    change_column :sites, :within_hours, :boolean, null: true, default: true

    remove_column :sites, :next_open_check, :datetime
  end
end
