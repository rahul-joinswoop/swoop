class AddLatLngIndexesToLocations < ActiveRecord::Migration[5.1]
  disable_ddl_transaction!

  def change
    add_index :locations, :lat, algorithm: :concurrently
    add_index :locations, :lng, algorithm: :concurrently
  end
end
