class CreateSiteFacilityMaps < ActiveRecord::Migration[5.1]
  def change
    create_table :site_facility_maps do |t|
      t.references :company, foreign_key: true
      t.references :fleet_company, foreign_key: { to_table: :companies }
      t.references :site, foreign_key: true
      t.references :facility, foreign_key: { to_table: :locations }
      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
