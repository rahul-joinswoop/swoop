class RemoveUnusedTimeFieldFromAutoAssign < ActiveRecord::Migration[5.0]
  def change
    remove_column :auto_assign_assignments, :status_check_at, :time
  end
end
