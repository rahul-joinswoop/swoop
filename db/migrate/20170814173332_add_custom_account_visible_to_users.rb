class AddCustomAccountVisibleToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :custom_account_visible, :boolean
  end
end
