class AddModelTypesTable < ActiveRecord::Migration[5.0]
  def change
    create_table :model_types do |t|
      t.string :name, unique: true
    end
  end
end
