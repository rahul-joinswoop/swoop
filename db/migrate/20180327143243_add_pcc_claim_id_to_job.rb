class AddPCCClaimIdToJob < ActiveRecord::Migration[5.0]
  def change
    add_reference :jobs, :pcc_claim, foreign_key: true, column: :pcc_claim_id
  end
end
