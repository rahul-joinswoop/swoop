class AddCreateAtIndexToReview < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :reviews, :created_at, algorithm: :concurrently
  end
end
