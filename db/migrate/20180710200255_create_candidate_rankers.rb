class CreateCandidateRankers < ActiveRecord::Migration[5.0]
  def change
    create_table :candidate_rankers do |t|
      t.string :type
      t.integer :version
      t.references :client, foreign_key: { to_table: :companies }
      t.string :name, limit: 64
      t.boolean :live
      t.json :weights
      t.json :constraints

      t.timestamps
    end
  end
end
