class AddUniqueIndexToCompaniesIsscClient < ActiveRecord::Migration[5.1]

  def change
    add_index :companies, :issc_client_id, unique: true
  end

end
