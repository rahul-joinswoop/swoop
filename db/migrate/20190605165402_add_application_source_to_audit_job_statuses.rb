class AddApplicationSourceToAuditJobStatuses < ActiveRecord::Migration[5.2]
  def change
    add_reference :audit_job_statuses, :source_application, index: false, foreign_key: true
  end
end
