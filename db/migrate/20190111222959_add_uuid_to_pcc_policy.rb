class AddUuidToPCCPolicy < ActiveRecord::Migration[5.1]

  def change
    add_column :pcc_policies, :uuid, :string, limit: 32
  end

end
