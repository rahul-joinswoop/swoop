class AddCompaniesNetworkManager < ActiveRecord::Migration[5.1]

  def up
    add_column :companies, :network_manager_id, :integer, null: true
    add_foreign_key :companies, :users, column: :network_manager_id
  end

  def down
    remove_column :companies, :network_manager_id
  end

end
