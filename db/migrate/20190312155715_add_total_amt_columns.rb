class AddTotalAmtColumns < ActiveRecord::Migration[5.1]
  def change
    add_column :audit_invoice_statuses, :total_amount_before_state_change, :decimal, precision: 20, scale: 4, null: true
    add_column :audit_invoice_statuses, :total_amount_after_state_change, :decimal, precision: 20, scale: 4, null: true
  end
end
