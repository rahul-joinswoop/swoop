class AddVehicleEligibleToAutoAssignCandidate < ActiveRecord::Migration[4.2]
  def change
    add_column :auto_assign_candidates, :vehicle_eligible, :string
  end
end
