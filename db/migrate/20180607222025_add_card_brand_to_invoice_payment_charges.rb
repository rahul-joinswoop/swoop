class AddCardBrandToInvoicePaymentCharges < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_payment_charges, :card_brand, :string
  end
end
