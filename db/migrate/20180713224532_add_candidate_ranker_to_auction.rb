class AddCandidateRankerToAuction < ActiveRecord::Migration[5.0]
  def change
    add_reference :auctions, :candidate_ranker, foreign_key: true
  end
end
