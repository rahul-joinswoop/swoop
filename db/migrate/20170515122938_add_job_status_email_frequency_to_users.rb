class AddJobStatusEmailFrequencyToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :job_status_email_frequency, :string, default: "dispatcher", null: false
  end
end
