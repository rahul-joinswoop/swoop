class CreateAPIAccessTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :api_access_tokens do |t|
      t.references :company, foreign_key: true
      t.string :vendorid, null: false
      t.string :callback_token, null: true, unique: true
      t.string :access_token, null: false, unique: true
      t.boolean :connected, default: true, null: false
      t.string :source, null: false

      t.timestamps
    end

    add_index :api_access_tokens, [:source, :vendorid]
  end
end
