class RemoveUndergroundFromJob < ActiveRecord::Migration[5.0]
  def change
    remove_column :jobs, :underground, :boolean
  end
end
