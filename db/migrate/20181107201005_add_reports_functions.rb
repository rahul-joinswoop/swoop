class AddReportsFunctions < ActiveRecord::Migration[5.1]
  def up
    execute <<~SQL
      CREATE OR REPLACE FUNCTION adjusted_ajs(jobid int, status_id int)
        RETURNS timestamp
        AS $function$
          BEGIN
            RETURN
              CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END
              FROM audit_job_statuses
              WHERE job_status_id = status_id
                AND audit_job_statuses.job_id = jobid
                AND audit_job_statuses.deleted_at IS NULL
              ORDER BY id DESC LIMIT 1;
          END;
        $function$
      LANGUAGE 'plpgsql' IMMUTABLE;
    SQL

    execute <<~SQL
      CREATE OR REPLACE FUNCTION US_time(ts timestamp, tz text)
        RETURNS text
        AS $function$
          BEGIN
            RETURN to_char(ts at time zone 'UTC' AT TIME ZONE COALESCE(tz, 'America/Los_Angeles'),'MM/DD/YYYY HH24:MI');
          END;
        $function$
      LANGUAGE 'plpgsql' IMMUTABLE;
    SQL

    execute <<~SQL
      CREATE OR REPLACE FUNCTION hms(i interval)
        RETURNS text
        AS $function$
          BEGIN
            RETURN to_char(i,'HH24:MI:SS');
          END;
        $function$
      LANGUAGE 'plpgsql' IMMUTABLE;
    SQL

    execute <<~SQL
      drop view if exists fleet_all_data_view;
      create view "fleet_all_data_view" as
      select
        jobs.id as "Job ID",
        to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY') as "Date (Pacific)",
        to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','HH24:MI') as "Time (Pacific)",
        partner.name as "Partner",
        CONCAT(customer.first_name, ' ', customer.last_name) as "Customer",
        customer.phone as "Phone",
        pickup_location_type.name as "Location Type",
        vehicles.make  as "Make",
        vehicles.model as "Model",
        pickup.address as "pickup location",
        pickup.zip,
        case when (pickup.city is not null) then pickup.city else (case when original_pickup.city is not null then original_pickup.city else (provider_location.city) end) end as "City",
        case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
        case when (pickup.zip is not null) then pickup.zip else (case when original_pickup.zip is not null then original_pickup.zip else provider_location.zip end) end as "Zip",
        CONCAT(pickup.lat, ',', pickup.lng) as "Pickup",
        CONCAT(dropoff.lat, ',', dropoff.lng) as "Dropoff",
        service_codes.name as "Service",
        jobs.scheduled_for as "Scheduled",
        round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
        round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
        round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",
        round((estimates.meters_ab+(CASE when estimates.drop_location_id is not null THEN estimates.meters_bc+estimates.meters_ca else estimates.meters_ba END))*0.000621371,1) as "P2P Miles",


      round(cast((Extract(EPOCH FROM
          (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3,4) order by id asc limit 1)
      )/60) as numeric),0) as "ETA",

      round(cast((Extract(EPOCH FROM
          (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
          (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3,4) order by id asc limit 1)
      )/60) as numeric),0) as "ATA",

        round(
          cast((Extract(EPOCH FROM
            (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
            (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3,4) order by id asc limit 1)
          )/60) as numeric)
          -
          cast((Extract(EPOCH FROM
            (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3,4) order by id asc limit 1)
          )/60) as numeric)
        ,0) as "ATA - ETA",

      TO_CHAR((
          (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0,3,4) ORDER BY id ASC LIMIT 1) -
          (
            WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
                SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
                FROM jobs AS inner_jobs
                WHERE inner_jobs.id = jobs.id
              UNION
                SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
                FROM jobs AS inner_jobs, parents
                WHERE inner_jobs.id = parents.parent_id
            )
            SELECT adjusted_created_at
            FROM parents
            ORDER BY id ASC
            LIMIT 1
          )
        ),'HH24:MI:SS') as "Handle Time",

          jobs.status as "Status",
          customer_types.name AS "Payment Type",
        CAST(
          (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
           from invoicing_line_items
           where invoicing_ledger_items.id=invoicing_line_items.ledger_item_id
             and invoicing_line_items.deleted_at is null
           group by invoicing_ledger_items.id)
        AS DECIMAL(14, 2)) as "Invoiced",
         jobs.partner_live as "Partner Live",
         jobs.partner_open as "Partner Open",
         case when (jobs.partner_live and jobs.partner_open) then true else null end as "Partner Live and Open",
         jobs.platform as "Platform",

         invoicing_ledger_items.state as "Invoice Status",
         vehicles.vin,
         jobs.unit_number as "Unit Number",
         jobs.ref_number as "Claim Number",
         symptoms.name as "Symptom",

        case when (jobs.drop_location_id is not null) then
          (estimates.seconds_ab +
          25*60 +
          estimates.seconds_bc +
          25*60 +
          estimates.seconds_ca)/60
        else
          (estimates.seconds_ab +
          25*60 +
          estimates.seconds_ba)/60
        end as "P2P Job Time Estimate",
        case when (jobs.drop_location_id is not null) then
          (estimates.seconds_ab +
          25*60 +
          estimates.seconds_bc +
          25*60)/60
        else
          (estimates.seconds_ab +
          25*60)/60
        end as "Port-to-Complete Job Time Estimate",
        departments.name as "Department",
        CONCAT_WS(' ',fleet_dispatchers.first_name, fleet_dispatchers.last_name) as "Fleet Dispatcher",
        CONCAT_WS(' ',partner_dispatchers.first_name, partner_dispatchers.last_name) as "Partner Dispatcher",

        US_time(adjusted_ajs(jobs.id,9), 'America/Los_Angeles') AS "Created",
        US_time(adjusted_ajs(jobs.id,10), 'America/Los_Angeles') AS "Assigned",
        US_time(adjusted_ajs(jobs.id,11), 'America/Los_Angeles') AS "Accepted",
        US_time(adjusted_ajs(jobs.id,2), 'America/Los_Angeles') AS "Dispatched",
        US_time(adjusted_ajs(jobs.id,3), 'America/Los_Angeles') AS "En Route",
        US_time(adjusted_ajs(jobs.id,4), 'America/Los_Angeles') AS "On Site",
        US_time(adjusted_ajs(jobs.id,5), 'America/Los_Angeles') AS "Towing",
        US_time(adjusted_ajs(jobs.id,6), 'America/Los_Angeles') AS "Tow Destination",
        US_time(adjusted_ajs(jobs.id,7), 'America/Los_Angeles') AS "Completed",
        US_time(adjusted_ajs(jobs.id,21), 'America/Los_Angeles') AS "GOA",
        US_time(adjusted_ajs(jobs.id,18), 'America/Los_Angeles') AS "Released",
        US_time(adjusted_ajs(jobs.id,8), 'America/Los_Angeles') AS "Canceled",

        hms(adjusted_ajs(jobs.id,11)-adjusted_ajs(jobs.id,9))  as "Digital Dispatch Time",
        hms(adjusted_ajs(jobs.id,3)-adjusted_ajs(jobs.id,9))  as "Digital En Route",
        hms(adjusted_ajs(jobs.id,7)-adjusted_ajs(jobs.id,3))  as "En Route -> Completed",

        jobs.notes as "Job Details",
        jobs.fleet_notes as "Fleet Notes",
        jobs.driver_notes as "Invoice Notes",
        job_reject_reasons.text as "Reject Reason",
        jobs.reject_reason_info as "Reject Comment",
        rescue_vehicle.name as "Rescue Vehicle Name",

      /* These are for filtering on the actual reports */
        jobs.created_at,
        jobs.fleet_company_id,
        jobs.rescue_company_id

      FROM   jobs
        left join estimates on jobs.estimate_id=estimates.id
        left join companies partner on partner.id = jobs.rescue_company_id
        left join locations as pickup on pickup.id=jobs.service_location_id
        left join locations as dropoff on dropoff.id=jobs.drop_location_id
        left join location_types as pickup_location_type on pickup_location_type.id = pickup.location_type_id
        left join service_codes on service_codes.id=jobs.service_code_id
        left join symptoms on jobs.symptom_id=symptoms.id
        left join invoicing_ledger_items on (jobs.id=invoicing_ledger_items.job_id and invoicing_ledger_items.sender_id=jobs.rescue_company_id and invoicing_ledger_items.deleted_at is null AND invoicing_ledger_items.type='Invoice')
        left join drives on jobs.driver_id=drives.id
        left join vehicles on drives.vehicle_id=vehicles.id
        left join departments on jobs.department_id=departments.id
        left join users as customer on customer.id=drives.user_id
        left join customer_types on customer_types.id=jobs.customer_type_id
        left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
        left join locations provider_location on partner.location_id=provider_location.id
        left join users as fleet_dispatchers on jobs.fleet_dispatcher_id = fleet_dispatchers.id
        left join users as partner_dispatchers on jobs.partner_dispatcher_id = partner_dispatchers.id
        left join job_reject_reasons on jobs.reject_reason_id=job_reject_reasons.id
        left join vehicles as rescue_vehicle on jobs.rescue_vehicle_id = rescue_vehicle.id

      order by jobs.id DESC;
    SQL
  end
end
