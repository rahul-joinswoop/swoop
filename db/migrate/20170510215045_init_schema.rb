class InitSchema < ActiveRecord::Migration[4.2]
  def up

    # These are extensions that must be enabled in order to support this database
    enable_extension "plpgsql"

    create_table "accounts", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at",                         null: false
      t.datetime "updated_at",                         null: false
      t.integer  "company_id"
      t.string   "accounting_email"
      t.integer  "location_id"
      t.integer  "fleet_company_id"
      t.datetime "deleted_at"
      t.string   "phone"
      t.string   "primary_email"
      t.string   "notes"
      t.boolean  "auto_accept"
      t.integer  "auto_eta"
      t.string   "color"
      t.integer  "department_id"
      t.boolean  "po_required_at_job_creation"
      t.boolean  "po_required_before_sending_invoice"
      t.integer  "physical_location_id"
      t.string   "fax"
      t.string   "contact_name"
      t.string   "contact_phone"
      t.string   "accounting_email_cc"
      t.string   "accounting_email_bcc"
      t.string   "contact_email"
    end

    add_index "accounts", ["company_id", "fleet_company_id"], name: "index_accounts_on_company_id_and_fleet_company_id", unique: true, using: :btree
    add_index "accounts", ["company_id"], name: "index_accounts_on_company_id", using: :btree
    add_index "accounts", ["department_id"], name: "index_accounts_on_department_id", using: :btree
    add_index "accounts", ["fleet_company_id"], name: "index_accounts_on_fleet_company_id", using: :btree
    add_index "accounts", ["location_id"], name: "index_accounts_on_location_id", using: :btree
    add_index "accounts", ["name", "company_id", "deleted_at"], name: "index_accounts_on_name_and_company_id_and_deleted_at", unique: true, using: :btree
    add_index "accounts", ["physical_location_id"], name: "index_accounts_on_physical_location_id", using: :btree

    create_table "answers", force: :cascade do |t|
      t.integer  "question_id"
      t.string   "answer"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
    end

    add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree

    create_table "ar_changes", force: :cascade do |t|
      t.integer  "target_id"
      t.string   "target_type"
      t.string   "callback_name"
      t.string   "commit_type"
      t.string   "ar_changes"
      t.string   "ar_previous_changes"
      t.datetime "created_at",          null: false
      t.datetime "updated_at",          null: false
      t.integer  "user_id"
      t.string   "request"
      t.integer  "txn_id"
      t.boolean  "committed"
      t.integer  "command_id"
      t.integer  "job_id"
    end

    add_index "ar_changes", ["command_id"], name: "index_ar_changes_on_command_id", using: :btree
    add_index "ar_changes", ["job_id"], name: "index_ar_changes_on_job_id", using: :btree
    add_index "ar_changes", ["request"], name: "index_ar_changes_on_request", using: :btree
    add_index "ar_changes", ["target_type", "target_id"], name: "index_ar_changes_on_target_type_and_target_id", using: :btree
    add_index "ar_changes", ["txn_id"], name: "index_ar_changes_on_txn_id", using: :btree
    add_index "ar_changes", ["user_id"], name: "index_ar_changes_on_user_id", using: :btree

    create_table "attached_documents", force: :cascade do |t|
      t.text     "document_data"
      t.integer  "job_id",        null: false
      t.integer  "user_id",       null: false
      t.datetime "deleted_at"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
    end

    add_index "attached_documents", ["job_id"], name: "index_attached_documents_on_job_id", using: :btree
    add_index "attached_documents", ["user_id"], name: "index_attached_documents_on_user_id", using: :btree

    create_table "audit_emails", force: :cascade do |t|
      t.integer  "job_id"
      t.integer  "user_id"
      t.integer  "company_id"
      t.string   "from"
      t.string   "to"
      t.string   "bcc"
      t.string   "category"
      t.integer  "target_id"
      t.string   "target_type"
      t.string   "subject"
      t.string   "body"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
    end

    add_index "audit_emails", ["company_id"], name: "index_audit_emails_on_company_id", using: :btree
    add_index "audit_emails", ["job_id"], name: "index_audit_emails_on_job_id", using: :btree
    add_index "audit_emails", ["target_type", "target_id"], name: "index_audit_emails_on_target_type_and_target_id", using: :btree
    add_index "audit_emails", ["user_id"], name: "index_audit_emails_on_user_id", using: :btree

    create_table "audit_isscs", force: :cascade do |t|
      t.integer  "job_id"
      t.string   "path"
      t.string   "data"
      t.string   "clientid"
      t.string   "contractorid"
      t.string   "dispatchid"
      t.string   "locationid"
      t.boolean  "incoming"
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.string   "http_response"
      t.integer  "http_status_code"
      t.integer  "company_id"
    end

    add_index "audit_isscs", ["company_id"], name: "index_audit_isscs_on_company_id", using: :btree
    add_index "audit_isscs", ["job_id"], name: "index_audit_isscs_on_job_id", using: :btree

    create_table "audit_job_statuses", force: :cascade do |t|
      t.integer  "job_status_id"
      t.integer  "job_id"
      t.datetime "last_set_dttm"
      t.datetime "adjusted_dttm"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
      t.integer  "company_id"
      t.datetime "deleted_at"
    end

    add_index "audit_job_statuses", ["company_id"], name: "index_audit_job_statuses_on_company_id", using: :btree
    add_index "audit_job_statuses", ["job_id", "job_status_id"], name: "index_audit_job_statuses_on_job_id_and_job_status_id", using: :btree
    add_index "audit_job_statuses", ["job_id"], name: "index_audit_job_statuses_on_job_id", using: :btree
    add_index "audit_job_statuses", ["job_status_id"], name: "index_audit_job_statuses_on_job_status_id", using: :btree

    create_table "audit_searches", force: :cascade do |t|
      t.string   "options",    limit: 4096
      t.string   "queries",    limit: 4096
      t.string   "filters",    limit: 4096
      t.datetime "created_at",              null: false
      t.datetime "updated_at",              null: false
      t.string   "original",   limit: 4096
    end

    create_table "audit_sms", force: :cascade do |t|
      t.integer  "job_id"
      t.integer  "user_id"
      t.datetime "clicked_dttm"
      t.string   "body"
      t.datetime "created_at",            null: false
      t.datetime "updated_at",            null: false
      t.string   "from"
      t.string   "to"
      t.string   "type"
      t.string   "uuid"
      t.string   "messaging_service_sid"
      t.string   "status"
      t.time     "status_update_at"
      t.integer  "error_code"
      t.string   "error_message"
    end

    add_index "audit_sms", ["job_id"], name: "index_audit_sms_on_job_id", using: :btree
    add_index "audit_sms", ["user_id"], name: "index_audit_sms_on_user_id", using: :btree

    create_table "auto_assign_assignments", force: :cascade do |t|
      t.integer  "job_id"
      t.json     "available_providers"
      t.integer  "chosen_provider_id"
      t.integer  "primary_count"
      t.integer  "secondary_count"
      t.string   "status",                   limit: 32
      t.time     "status_check_at"
      t.datetime "created_at",                                          null: false
      t.datetime "updated_at",                                          null: false
      t.integer  "chosen_provider_distance"
      t.boolean  "auto_dispatched",                     default: false
    end

    add_index "auto_assign_assignments", ["chosen_provider_id"], name: "index_auto_assign_assignments_on_chosen_provider_id", using: :btree
    add_index "auto_assign_assignments", ["created_at", "status", "job_id"], name: "created_at_status_idx", using: :btree
    add_index "auto_assign_assignments", ["job_id"], name: "index_auto_assign_assignments_on_job_id", using: :btree

    create_table "auto_assign_candidate_jobs", force: :cascade do |t|
      t.string   "status"
      t.integer  "auto_assign_candidate_id"
      t.integer  "job_id"
      t.datetime "created_at",               null: false
      t.datetime "updated_at",               null: false
    end

    create_table "auto_assign_candidates", force: :cascade do |t|
      t.integer  "job_id",                                                         null: false
      t.integer  "vehicle_id",                                                     null: false
      t.decimal  "crowflies_miles",                  precision: 2
      t.integer  "google_eta"
      t.decimal  "google_miles",                     precision: 2
      t.boolean  "can_be_auto_dispatched_to"
      t.boolean  "has_auto_dispatchable_job"
      t.boolean  "service_ends_at_service_location"
      t.boolean  "service_ends_at_drop_location"
      t.boolean  "is_onsite"
      t.integer  "time_onsite"
      t.boolean  "no_jobs"
      t.boolean  "assigned_for_job",                               default: false, null: false
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "bosch_jobs", force: :cascade do |t|
      t.integer  "job_id"
      t.datetime "created_at",        null: false
      t.string   "country_code"
      t.string   "vin"
      t.string   "license_plate"
      t.string   "brand"
      t.string   "series"
      t.string   "basic_model"
      t.string   "body"
      t.string   "model_range"
      t.string   "model_key"
      t.string   "model_year"
      t.string   "color"
      t.string   "type_of_drive"
      t.string   "type_of_hybrid"
      t.string   "drive_type"
      t.string   "first_name"
      t.string   "last_name"
      t.string   "gender"
      t.string   "cell_phone_number"
      t.string   "email"
      t.string   "alias"
      t.string   "home_address"
      t.string   "home_town"
      t.string   "country"
      t.string   "hmi_language"
      t.string   "position"
      t.string   "heading"
      t.string   "altitude"
      t.text     "data"
      t.text     "url"
    end

    add_index "bosch_jobs", ["job_id"], name: "index_bosch_jobs_on_job_id", using: :btree

    create_table "commands", force: :cascade do |t|
      t.integer  "event_operation_id"
      t.integer  "user_id"
      t.integer  "company_id"
      t.string   "json"
      t.integer  "target_id"
      t.string   "target_type"
      t.boolean  "applied"
      t.datetime "created_at",         null: false
      t.datetime "updated_at",         null: false
      t.string   "request"
      t.string   "type"
      t.string   "service_type"
    end

    add_index "commands", ["company_id"], name: "index_commands_on_company_id", using: :btree
    add_index "commands", ["event_operation_id"], name: "index_commands_on_event_operation_id", using: :btree
    add_index "commands", ["request"], name: "index_commands_on_request", using: :btree
    add_index "commands", ["target_type", "target_id"], name: "index_commands_on_target_type_and_target_id", using: :btree
    add_index "commands", ["user_id"], name: "index_commands_on_user_id", using: :btree

    create_table "companies", force: :cascade do |t|
      t.string   "name"
      t.string   "business"
      t.string   "side"
      t.datetime "created_at",                                                               null: false
      t.datetime "updated_at",                                                               null: false
      t.string   "type"
      t.integer  "primary_contact_id"
      t.integer  "support_contact_id"
      t.string   "support_email"
      t.string   "accounting_email"
      t.integer  "payment_contact_id"
      t.string   "parse_id"
      t.string   "uuid"
      t.text     "get_location_text"
      t.text     "track_technician_text"
      t.text     "review_text"
      t.boolean  "in_house"
      t.string   "logo_url"
      t.integer  "location_id"
      t.string   "phone"
      t.boolean  "demo"
      t.string   "dispatch_email"
      t.string   "fax"
      t.integer  "invited_by_user_id"
      t.integer  "invited_by_company_id"
      t.boolean  "live"
      t.boolean  "e2e"
      t.integer  "invoice_minimum_mins"
      t.integer  "invoice_increment_mins"
      t.integer  "invoice_roundup_mins"
      t.string   "tax_id"
      t.integer  "parent_company_id"
      t.string   "issc_client_id"
      t.boolean  "permission_to_send_reviews_from",                          default: false
      t.string   "review_url"
      t.integer  "auto_assign_distance"
      t.string   "survey_host"
      t.string   "survey_template"
      t.integer  "auto_truck_id"
      t.integer  "auto_driver_id"
      t.string   "invoice_mileage_rounding"
      t.decimal  "invoice_mileage_minimum",         precision: 20, scale: 4
      t.datetime "deleted_at"
      t.string   "issc_status"
      t.string   "sync_uuid"
    end

    add_index "companies", ["deleted_at"], name: "index_companies_on_deleted_at", using: :btree
    add_index "companies", ["invited_by_company_id"], name: "index_companies_on_invited_by_company_id", using: :btree
    add_index "companies", ["invited_by_user_id"], name: "index_companies_on_invited_by_user_id", using: :btree
    add_index "companies", ["location_id"], name: "index_companies_on_location_id", using: :btree
    add_index "companies", ["parent_company_id"], name: "index_companies_on_parent_company_id", using: :btree
    add_index "companies", ["payment_contact_id"], name: "index_companies_on_payment_contact_id", using: :btree
    add_index "companies", ["phone"], name: "index_companies_on_phone", using: :btree
    add_index "companies", ["primary_contact_id"], name: "index_companies_on_primary_contact_id", using: :btree
    add_index "companies", ["support_contact_id"], name: "index_companies_on_support_contact_id", using: :btree
    add_index "companies", ["sync_uuid"], name: "index_companies_on_sync_uuid", using: :btree
    add_index "companies", ["uuid"], name: "index_companies_on_uuid", using: :btree

    create_table "companies_customer_types", force: :cascade do |t|
      t.integer  "company_id"
      t.integer  "customer_type_id"
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.datetime "deleted_at"
    end

    add_index "companies_customer_types", ["company_id"], name: "index_companies_customer_types_on_company_id", using: :btree
    add_index "companies_customer_types", ["customer_type_id"], name: "index_companies_customer_types_on_customer_type_id", using: :btree

    create_table "companies_features", force: :cascade do |t|
      t.integer  "company_id"
      t.integer  "feature_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "companies_features", ["company_id"], name: "index_companies_features_on_company_id", using: :btree
    add_index "companies_features", ["feature_id"], name: "index_companies_features_on_feature_id", using: :btree

    create_table "companies_products", id: false, force: :cascade do |t|
      t.integer "company_id"
      t.integer "product_id"
    end

    add_index "companies_products", ["company_id"], name: "index_companies_products_on_company_id", using: :btree
    add_index "companies_products", ["product_id"], name: "index_companies_products_on_product_id", using: :btree

    create_table "companies_services", force: :cascade do |t|
      t.integer  "company_id"
      t.integer  "service_code_id"
      t.datetime "created_at",      null: false
      t.datetime "updated_at",      null: false
    end

    add_index "companies_services", ["company_id"], name: "index_companies_services_on_company_id", using: :btree
    add_index "companies_services", ["service_code_id"], name: "index_companies_services_on_service_code_id", using: :btree

    create_table "companies_vehicle_categories", force: :cascade do |t|
      t.integer  "company_id"
      t.integer  "vehicle_category_id"
      t.datetime "created_at",          null: false
      t.datetime "updated_at",          null: false
    end

    add_index "companies_vehicle_categories", ["company_id"], name: "index_companies_vehicle_categories_on_company_id", using: :btree
    add_index "companies_vehicle_categories", ["vehicle_category_id"], name: "index_companies_vehicle_categories_on_vehicle_category_id", using: :btree

    create_table "company_reports", force: :cascade do |t|
      t.integer  "report_id"
      t.integer  "company_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "company_reports", ["company_id"], name: "index_company_reports_on_company_id", using: :btree
    add_index "company_reports", ["report_id"], name: "index_company_reports_on_report_id", using: :btree

    create_table "company_service_questions", force: :cascade do |t|
      t.integer  "company_id"
      t.integer  "service_code_id"
      t.integer  "question_id"
      t.datetime "created_at",      null: false
      t.datetime "updated_at",      null: false
    end

    add_index "company_service_questions", ["company_id"], name: "index_company_service_questions_on_company_id", using: :btree
    add_index "company_service_questions", ["question_id"], name: "index_company_service_questions_on_question_id", using: :btree
    add_index "company_service_questions", ["service_code_id"], name: "index_company_service_questions_on_service_code_id", using: :btree

    create_table "company_symptoms", force: :cascade do |t|
      t.integer  "company_id"
      t.integer  "symptom_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "company_symptoms", ["company_id"], name: "index_company_symptoms_on_company_id", using: :btree
    add_index "company_symptoms", ["symptom_id"], name: "index_company_symptoms_on_symptom_id", using: :btree

    create_table "customer_types", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "delayed_jobs", force: :cascade do |t|
      t.integer  "priority",   default: 0, null: false
      t.integer  "attempts",   default: 0, null: false
      t.text     "handler",                null: false
      t.text     "last_error"
      t.datetime "run_at"
      t.datetime "locked_at"
      t.datetime "failed_at"
      t.string   "locked_by"
      t.string   "queue"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

    create_table "departments", force: :cascade do |t|
      t.string   "name"
      t.string   "code"
      t.integer  "company_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer  "sort_order"
    end

    add_index "departments", ["company_id"], name: "index_departments_on_company_id", using: :btree

    create_table "drives", force: :cascade do |t|
      t.integer  "user_id"
      t.integer  "vehicle_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "drives", ["user_id"], name: "index_drives_on_user_id", using: :btree
    add_index "drives", ["vehicle_id"], name: "index_drives_on_vehicle_id", using: :btree

    create_table "estimates", force: :cascade do |t|
      t.integer  "job_id"
      t.integer  "meters_ab"
      t.integer  "meters_bc"
      t.integer  "meters_ca"
      t.integer  "meters_ba"
      t.integer  "seconds_ab"
      t.integer  "seconds_bc"
      t.integer  "seconds_ca"
      t.integer  "seconds_ba"
      t.datetime "created_at",          null: false
      t.datetime "updated_at",          null: false
      t.integer  "site_location_id"
      t.integer  "service_location_id"
      t.integer  "drop_location_id"
      t.integer  "version"
      t.datetime "start_at"
    end

    add_index "estimates", ["drop_location_id"], name: "index_estimates_on_drop_location_id", using: :btree
    add_index "estimates", ["job_id"], name: "index_estimates_on_job_id", using: :btree
    add_index "estimates", ["service_location_id"], name: "index_estimates_on_service_location_id", using: :btree
    add_index "estimates", ["site_location_id"], name: "index_estimates_on_site_location_id", using: :btree

    create_table "event_operations", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "event_operations", ["name"], name: "index_event_operations_on_name", using: :btree

    create_table "experiments", force: :cascade do |t|
      t.string   "name"
      t.json     "data"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "explanations", force: :cascade do |t|
      t.integer  "job_id",      null: false
      t.integer  "reason_id",   null: false
      t.string   "reason_info"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.datetime "deleted_at"
    end

    add_index "explanations", ["job_id"], name: "index_explanations_on_job_id", using: :btree
    add_index "explanations", ["reason_id"], name: "index_explanations_on_reason_id", using: :btree

    create_table "features", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at",   null: false
      t.datetime "updated_at",   null: false
      t.string   "description"
      t.json     "supported_by"
    end

    create_table "hidden_users_dispatchers", force: :cascade do |t|
      t.integer  "user_id"
      t.integer  "dispatcher_id"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
    end

    add_index "hidden_users_dispatchers", ["dispatcher_id"], name: "index_users_dispatchers_on_dispatcher_id", using: :btree
    add_index "hidden_users_dispatchers", ["user_id"], name: "index_users_dispatchers_on_user_id", using: :btree

    create_table "hidden_users_sites", force: :cascade do |t|
      t.integer  "site_id"
      t.integer  "user_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "hidden_users_sites", ["site_id"], name: "index_sites_users_on_site_id", using: :btree
    add_index "hidden_users_sites", ["user_id"], name: "index_sites_users_on_user_id", using: :btree

    create_table "hidden_users_storage_sites", force: :cascade do |t|
      t.integer  "user_id"
      t.integer  "site_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "hidden_users_storage_sites", ["site_id"], name: "index_hidden_users_storage_sites_on_site_id", using: :btree
    add_index "hidden_users_storage_sites", ["user_id"], name: "index_hidden_users_storage_sites_on_user_id", using: :btree

    create_table "hidden_users_storage_types", force: :cascade do |t|
      t.integer  "user_id"
      t.integer  "storage_type_id"
      t.datetime "created_at",      null: false
      t.datetime "updated_at",      null: false
    end

    add_index "hidden_users_storage_types", ["storage_type_id"], name: "index_hidden_users_storage_types_on_storage_type_id", using: :btree
    add_index "hidden_users_storage_types", ["user_id"], name: "index_hidden_users_storage_types_on_user_id", using: :btree

    create_table "images", force: :cascade do |t|
      t.string   "url"
      t.integer  "job_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.datetime "deleted_at"
      t.integer  "user_id"
      t.float    "lat"
      t.float    "lng"
    end

    add_index "images", ["job_id"], name: "index_images_on_job_id", using: :btree
    add_index "images", ["user_id"], name: "index_images_on_user_id", using: :btree

    create_table "inventory_items", force: :cascade do |t|
      t.string   "type"
      t.integer  "item_id"
      t.string   "item_type"
      t.integer  "site_id"
      t.integer  "job_id"
      t.datetime "stored_at"
      t.string   "time"
      t.datetime "released_at"
      t.integer  "released_to_user_id"
      t.string   "released_to_email"
      t.integer  "released_to_account_id"
      t.integer  "released_to_job_id"
      t.datetime "deleted_at"
      t.datetime "created_at",             null: false
      t.datetime "updated_at",             null: false
      t.integer  "company_id"
      t.integer  "user_id"
      t.boolean  "replace_invoice_info"
    end

    add_index "inventory_items", ["company_id"], name: "index_inventory_items_on_company_id", using: :btree
    add_index "inventory_items", ["item_type", "item_id"], name: "index_inventory_items_on_item_type_and_item_id", using: :btree
    add_index "inventory_items", ["job_id"], name: "index_inventory_items_on_job_id", using: :btree
    add_index "inventory_items", ["released_to_account_id"], name: "index_inventory_items_on_released_to_account_id", using: :btree
    add_index "inventory_items", ["released_to_job_id"], name: "index_inventory_items_on_released_to_job_id", using: :btree
    add_index "inventory_items", ["released_to_user_id"], name: "index_inventory_items_on_released_to_user_id", using: :btree
    add_index "inventory_items", ["site_id"], name: "index_inventory_items_on_site_id", using: :btree
    add_index "inventory_items", ["user_id"], name: "index_inventory_items_on_user_id", using: :btree

    create_table "invoice_pdfs", force: :cascade do |t|
      t.integer  "invoice_id", null: false
      t.string   "public_url"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "invoice_pdfs", ["invoice_id"], name: "index_invoice_pdfs_on_invoice_id", using: :btree

    create_table "invoice_reject_reasons", force: :cascade do |t|
      t.integer  "company_id"
      t.string   "text"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "invoice_reject_reasons", ["company_id"], name: "index_invoice_reject_reasons_on_company_id", using: :btree

    create_table "invoice_rejections", force: :cascade do |t|
      t.integer  "invoice_reject_reason_id", null: false
      t.text     "notes"
      t.datetime "created_at",               null: false
      t.datetime "updated_at",               null: false
      t.integer  "invoice_id"
    end

    add_index "invoice_rejections", ["invoice_id"], name: "index_invoice_rejections_on_invoice_id", using: :btree
    add_index "invoice_rejections", ["invoice_reject_reason_id"], name: "index_invoice_rejections_on_invoice_reject_reason_id", using: :btree

    create_table "invoicing_ledger_items", force: :cascade do |t|
      t.integer  "sender_id"
      t.integer  "recipient_id"
      t.string   "type"
      t.datetime "issue_date"
      t.string   "currency",              limit: 3,                                             null: false
      t.decimal  "total_amount",                       precision: 20, scale: 4
      t.decimal  "tax_amount",                         precision: 20, scale: 4
      t.string   "status",                limit: 20
      t.string   "identifier",            limit: 50
      t.string   "description"
      t.datetime "period_start"
      t.datetime "period_end"
      t.string   "uuid",                  limit: 40
      t.datetime "due_date"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "sent_at"
      t.integer  "job_id"
      t.string   "recipient_type"
      t.string   "state"
      t.integer  "incoming_invoice_id"
      t.integer  "recipient_company_id"
      t.string   "rate_type"
      t.datetime "deleted_at"
      t.string   "error"
      t.integer  "rate_id"
      t.boolean  "partner_alert"
      t.boolean  "fleet_alert"
      t.decimal  "fleet_ui_total_amount",              precision: 20, scale: 4
      t.integer  "version"
      t.boolean  "paid",                                                        default: false
      t.string   "email_to"
      t.string   "email_cc"
      t.string   "email_bcc"
      t.string   "s3_filename"
      t.datetime "qb_imported_at"
      t.string   "payment_method"
      t.string   "invoice_notes",         limit: 4095
      t.datetime "mark_paid_at"
    end

    add_index "invoicing_ledger_items", ["incoming_invoice_id"], name: "index_invoicing_ledger_items_on_incoming_invoice_id", using: :btree
    add_index "invoicing_ledger_items", ["job_id"], name: "index_invoicing_ledger_items_on_job_id", using: :btree
    add_index "invoicing_ledger_items", ["rate_id"], name: "index_invoicing_ledger_items_on_rate_id", using: :btree
    add_index "invoicing_ledger_items", ["recipient_company_id"], name: "index_invoicing_ledger_items_on_recipient_company_id", using: :btree

    create_table "invoicing_line_items", force: :cascade do |t|
      t.integer  "ledger_item_id"
      t.string   "type"
      t.decimal  "net_amount",                     precision: 20, scale: 4
      t.decimal  "tax_amount",                     precision: 20, scale: 4
      t.string   "description"
      t.string   "uuid",                limit: 40
      t.datetime "tax_point"
      t.decimal  "quantity",                       precision: 20, scale: 4
      t.integer  "creator_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "notes"
      t.boolean  "estimated"
      t.boolean  "user_created"
      t.integer  "order"
      t.decimal  "unit_price",                     precision: 20, scale: 4
      t.decimal  "original_unit_price",            precision: 20, scale: 4
      t.decimal  "original_quantity",              precision: 20, scale: 4
      t.datetime "deleted_at"
      t.boolean  "free"
      t.integer  "job_id"
      t.integer  "rate_id"
      t.integer  "version"
      t.string   "calc_type"
    end

    add_index "invoicing_line_items", ["job_id"], name: "index_invoicing_line_items_on_job_id", using: :btree
    add_index "invoicing_line_items", ["ledger_item_id"], name: "index_invoicing_line_items_on_ledger_item_id", using: :btree
    add_index "invoicing_line_items", ["rate_id"], name: "index_invoicing_line_items_on_rate_id", using: :btree

    create_table "invoicing_tax_rates", force: :cascade do |t|
      t.string   "description"
      t.decimal  "rate",           precision: 20, scale: 4
      t.datetime "valid_from",                              null: false
      t.datetime "valid_until"
      t.integer  "replaced_by_id"
      t.boolean  "is_default"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "issc_dispatch_requests", force: :cascade do |t|
      t.string   "dispatchid"
      t.string   "jobid"
      t.string   "responseid"
      t.integer  "issc_id"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
      t.string   "status"
      t.integer  "max_eta"
      t.integer  "preferred_eta"
    end

    add_index "issc_dispatch_requests", ["dispatchid"], name: "index_issc_dispatch_requests_on_dispatchid", using: :btree
    add_index "issc_dispatch_requests", ["issc_id"], name: "index_issc_dispatch_requests_on_issc_id", using: :btree

    create_table "issc_locations", force: :cascade do |t|
      t.integer  "site_id"
      t.string   "locationid"
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.integer  "fleet_company_id"
      t.datetime "deleted_at"
      t.integer  "location_id"
    end

    add_index "issc_locations", ["fleet_company_id"], name: "index_issc_locations_on_fleet_company_id", using: :btree
    add_index "issc_locations", ["location_id"], name: "index_issc_locations_on_location_id", using: :btree
    add_index "issc_locations", ["site_id"], name: "index_issc_locations_on_site_id", using: :btree

    create_table "issc_logins", force: :cascade do |t|
      t.integer  "site_id"
      t.integer  "fleet_company_id"
      t.datetime "deleted_at"
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.string   "username"
      t.string   "password"
    end

    add_index "issc_logins", ["fleet_company_id"], name: "index_issc_logins_on_fleet_company_id", using: :btree
    add_index "issc_logins", ["site_id"], name: "index_issc_logins_on_site_id", using: :btree

    create_table "isscs", force: :cascade do |t|
      t.integer  "company_id"
      t.integer  "site_id"
      t.string   "contractorid"
      t.string   "clientid"
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.string   "token"
      t.string   "status"
      t.datetime "deleted_at"
      t.integer  "issc_location_id"
      t.string   "error"
      t.datetime "error_at"
      t.integer  "issc_login_id"
      t.datetime "logged_in_at"
    end

    add_index "isscs", ["company_id"], name: "index_isscs_on_company_id", using: :btree
    add_index "isscs", ["issc_location_id"], name: "index_isscs_on_issc_location_id", using: :btree
    add_index "isscs", ["issc_login_id"], name: "index_isscs_on_issc_login_id", using: :btree
    add_index "isscs", ["site_id"], name: "index_isscs_on_site_id", using: :btree

    create_table "issues", force: :cascade do |t|
      t.integer  "company_id", null: false
      t.string   "key",        null: false
      t.string   "name",       null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.datetime "deleted_at"
    end

    add_index "issues", ["company_id"], name: "index_issues_on_company_id", using: :btree

    create_table "job_errors", force: :cascade do |t|
      t.integer  "job_id"
      t.integer  "code"
      t.string   "msg"
      t.boolean  "failed_permanently"
      t.datetime "created_at",         null: false
      t.datetime "updated_at",         null: false
    end

    add_index "job_errors", ["code"], name: "index_job_errors_on_code", using: :btree
    add_index "job_errors", ["failed_permanently"], name: "index_job_errors_on_failed_permanently", using: :btree
    add_index "job_errors", ["job_id"], name: "index_job_errors_on_job_id", using: :btree

    create_table "job_eta_explanations", force: :cascade do |t|
      t.string   "text"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "job_events", force: :cascade do |t|
      t.integer  "user_id"
      t.integer  "job_id"
      t.string   "json"
      t.string   "uuid"
      t.string   "crud"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "job_events", ["job_id"], name: "index_job_events_on_job_id", using: :btree
    add_index "job_events", ["user_id"], name: "index_job_events_on_user_id", using: :btree
    add_index "job_events", ["uuid"], name: "index_job_events_on_uuid", using: :btree

    create_table "job_reject_reasons", force: :cascade do |t|
      t.string   "text"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer  "reason_id"
    end

    create_table "job_statuses", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "jobs", force: :cascade do |t|
      t.integer  "service_code_id"
      t.integer  "driver_id"
      t.integer  "fleet_company_id"
      t.integer  "rescue_company_id"
      t.datetime "completed_at"
      t.datetime "created_at",                                          null: false
      t.datetime "updated_at",                                          null: false
      t.string   "status"
      t.string   "notes"
      t.integer  "service_location_id"
      t.integer  "drop_location_id"
      t.string   "uuid"
      t.integer  "user_id"
      t.integer  "rescue_vehicle_id"
      t.integer  "drupal_id"
      t.integer  "get_location_attempts",                   default: 0, null: false
      t.date     "track_technician_sent_at"
      t.integer  "review_id"
      t.float    "price"
      t.integer  "account_id"
      t.integer  "owner_sequence"
      t.integer  "rescue_sequence"
      t.string   "email"
      t.string   "partner_notes"
      t.float    "distance"
      t.datetime "scheduled_for"
      t.string   "po_number"
      t.integer  "invoice_id"
      t.integer  "caller_id"
      t.string   "unit_number"
      t.string   "ref_number"
      t.string   "dispatch_notes"
      t.integer  "last_touched_by_id"
      t.integer  "rescue_driver_id"
      t.float    "odometer"
      t.boolean  "vip"
      t.boolean  "uber"
      t.boolean  "alternate_transportation"
      t.boolean  "send_eta"
      t.integer  "customer_type_id"
      t.string   "type"
      t.integer  "parent_id"
      t.integer  "pickup_contact_id"
      t.integer  "dropoff_contact_id"
      t.boolean  "underground"
      t.boolean  "no_keys"
      t.boolean  "keys_in_trunk"
      t.boolean  "four_wheel_drive"
      t.boolean  "roadside"
      t.boolean  "residence"
      t.boolean  "accident"
      t.string   "location_type"
      t.boolean  "blocking_traffic"
      t.boolean  "unattended"
      t.integer  "reject_reason_id"
      t.string   "reject_reason_info"
      t.string   "fleet_notes"
      t.string   "driver_notes"
      t.integer  "site_id"
      t.integer  "estimate_id"
      t.boolean  "fleet_manual"
      t.boolean  "partner_live"
      t.boolean  "partner_open"
      t.boolean  "partner_e2e"
      t.string   "swoop_notes"
      t.integer  "invoice_vehicle_category_id"
      t.integer  "symptom_id"
      t.integer  "tire_type_id"
      t.integer  "fleet_dispatcher_id"
      t.boolean  "get_location_sms"
      t.integer  "original_service_location_id"
      t.boolean  "location_updated"
      t.boolean  "review_sms"
      t.integer  "partner_dispatcher_id"
      t.datetime "deleted_at"
      t.boolean  "get_location_clicked"
      t.string   "platform"
      t.boolean  "partner_sms_track_driver"
      t.boolean  "send_sms_to_pickup"
      t.boolean  "will_store"
      t.integer  "original_job_id"
      t.integer  "issc_dispatch_request_id"
      t.datetime "answer_by"
      t.integer  "storage_type_id"
      t.integer  "root_dispatcher_id"
      t.datetime "last_status_changed_at"
      t.integer  "eta_explanation_id"
      t.boolean  "priority_response"
      t.string   "policy_number",                limit: 32
      t.integer  "policy_location_id"
      t.boolean  "text_eta_updates"
      t.integer  "fleet_site_id"
      t.integer  "department_id"
      t.string   "dropoff_location_type"
      t.integer  "trailer_vehicle_id"
      t.integer  "partner_department_id"
      t.datetime "adjusted_created_at",                                 null: false
      t.string   "review_delay"
    end

    add_index "jobs", ["account_id"], name: "index_jobs_on_account_id", using: :btree
    add_index "jobs", ["caller_id"], name: "index_jobs_on_caller_id", using: :btree
    add_index "jobs", ["customer_type_id"], name: "index_jobs_on_customer_type_id", using: :btree
    add_index "jobs", ["department_id"], name: "index_jobs_on_department_id", using: :btree
    add_index "jobs", ["driver_id"], name: "index_jobs_on_driver_id", using: :btree
    add_index "jobs", ["drop_location_id"], name: "index_jobs_on_drop_location_id", using: :btree
    add_index "jobs", ["dropoff_contact_id"], name: "index_jobs_on_dropoff_contact_id", using: :btree
    add_index "jobs", ["estimate_id"], name: "index_jobs_on_estimate_id", using: :btree
    add_index "jobs", ["eta_explanation_id"], name: "index_jobs_on_eta_explanation_id", using: :btree
    add_index "jobs", ["fleet_company_id"], name: "index_jobs_on_fleet_company_id", using: :btree
    add_index "jobs", ["fleet_dispatcher_id"], name: "index_jobs_on_fleet_dispatcher_id", using: :btree
    add_index "jobs", ["fleet_site_id"], name: "index_jobs_on_fleet_site_id", using: :btree
    add_index "jobs", ["invoice_id"], name: "index_jobs_on_invoice_id", using: :btree
    add_index "jobs", ["invoice_vehicle_category_id"], name: "index_jobs_on_invoice_vehicle_category_id", using: :btree
    add_index "jobs", ["issc_dispatch_request_id"], name: "index_jobs_on_issc_dispatch_request_id", using: :btree
    add_index "jobs", ["last_touched_by_id"], name: "index_jobs_on_last_touched_by_id", using: :btree
    add_index "jobs", ["original_job_id"], name: "index_jobs_on_original_job_id", using: :btree
    add_index "jobs", ["original_service_location_id"], name: "index_jobs_on_original_service_location_id", using: :btree
    add_index "jobs", ["parent_id"], name: "index_jobs_on_parent_id", using: :btree
    add_index "jobs", ["partner_department_id"], name: "index_jobs_on_partner_department_id", using: :btree
    add_index "jobs", ["partner_dispatcher_id"], name: "index_jobs_on_partner_dispatcher_id", using: :btree
    add_index "jobs", ["pickup_contact_id"], name: "index_jobs_on_pickup_contact_id", using: :btree
    add_index "jobs", ["rescue_company_id"], name: "index_jobs_on_rescue_company_id", using: :btree
    add_index "jobs", ["rescue_driver_id"], name: "index_jobs_on_rescue_driver_id", using: :btree
    add_index "jobs", ["rescue_vehicle_id"], name: "index_jobs_on_rescue_vehicle_id", using: :btree
    add_index "jobs", ["review_id"], name: "index_jobs_on_review_id", using: :btree
    add_index "jobs", ["root_dispatcher_id"], name: "index_jobs_on_root_dispatcher_id", using: :btree
    add_index "jobs", ["service_code_id"], name: "index_jobs_on_service_code_id", using: :btree
    add_index "jobs", ["service_location_id"], name: "index_jobs_on_service_location_id", using: :btree
    add_index "jobs", ["site_id"], name: "index_jobs_on_site_id", using: :btree
    add_index "jobs", ["storage_type_id"], name: "index_jobs_on_storage_type_id", using: :btree
    add_index "jobs", ["symptom_id"], name: "index_jobs_on_symptom_id", using: :btree
    add_index "jobs", ["tire_type_id"], name: "index_jobs_on_tire_type_id", using: :btree
    add_index "jobs", ["trailer_vehicle_id"], name: "index_jobs_on_trailer_vehicle_id", using: :btree
    add_index "jobs", ["user_id"], name: "index_jobs_on_user_id", using: :btree
    add_index "jobs", ["uuid"], name: "index_jobs_on_uuid", using: :btree

    create_table "locations", force: :cascade do |t|
      t.integer  "user_id"
      t.float    "lat"
      t.float    "lng"
      t.datetime "created_at",           null: false
      t.datetime "updated_at",           null: false
      t.string   "address"
      t.string   "state"
      t.string   "city"
      t.string   "street"
      t.string   "zip"
      t.boolean  "exact"
      t.string   "country",    limit: 2
      t.integer  "site_id"
      t.integer  "job_id"
      t.boolean  "autosplit"
      t.string   "place_id"
    end

    add_index "locations", ["country"], name: "index_locations_on_country", using: :btree
    add_index "locations", ["job_id"], name: "index_locations_on_job_id", using: :btree
    add_index "locations", ["site_id"], name: "index_locations_on_site_id", using: :btree
    add_index "locations", ["user_id"], name: "index_locations_on_user_id", using: :btree

    create_table "metrics", force: :cascade do |t|
      t.integer  "target_id"
      t.string   "target_type"
      t.string   "action"
      t.string   "label"
      t.integer  "value"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.integer  "user_id"
      t.integer  "company_id"
    end

    add_index "metrics", ["company_id"], name: "index_metrics_on_company_id", using: :btree
    add_index "metrics", ["target_type", "target_id"], name: "index_metrics_on_target_type_and_target_id", using: :btree
    add_index "metrics", ["user_id"], name: "index_metrics_on_user_id", using: :btree

    create_table "new_invoice_locks", id: false, force: :cascade do |t|
      t.integer  "job_id",     null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "new_invoice_locks", ["job_id"], name: "index_new_invoice_locks_on_job_id", unique: true, using: :btree

    create_table "notifications", force: :cascade do |t|
      t.integer  "job_id"
      t.string   "message"
      t.boolean  "dismissable", default: true
      t.integer  "timeout",     default: 2000
      t.string   "msg_type",    default: "warning"
      t.datetime "created_at",                      null: false
      t.datetime "updated_at",                      null: false
    end

    create_table "oauth_access_grants", force: :cascade do |t|
      t.integer  "resource_owner_id", null: false
      t.integer  "application_id",    null: false
      t.string   "token",             null: false
      t.integer  "expires_in",        null: false
      t.text     "redirect_uri",      null: false
      t.datetime "created_at",        null: false
      t.datetime "revoked_at"
      t.string   "scopes"
    end

    add_index "oauth_access_grants", ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree

    create_table "oauth_access_tokens", force: :cascade do |t|
      t.integer  "resource_owner_id"
      t.integer  "application_id"
      t.string   "token",             null: false
      t.string   "refresh_token"
      t.integer  "expires_in"
      t.datetime "revoked_at"
      t.datetime "created_at",        null: false
      t.string   "scopes"
    end

    add_index "oauth_access_tokens", ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
    add_index "oauth_access_tokens", ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
    add_index "oauth_access_tokens", ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree

    create_table "oauth_applications", force: :cascade do |t|
      t.string   "name",                      null: false
      t.string   "uid",                       null: false
      t.string   "secret",                    null: false
      t.text     "redirect_uri",              null: false
      t.string   "scopes",       default: "", null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "owner_id"
      t.string   "owner_type"
    end

    add_index "oauth_applications", ["owner_id", "owner_type"], name: "index_oauth_applications_on_owner_id_and_owner_type", using: :btree
    add_index "oauth_applications", ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree

    create_table "password_requests", force: :cascade do |t|
      t.uuid     "uuid"
      t.string   "user_input",                    null: false
      t.boolean  "valid_request",  default: true
      t.integer  "invalidated_by"
      t.datetime "used_at"
      t.integer  "user_id"
      t.datetime "created_at",                    null: false
      t.datetime "updated_at",                    null: false
    end

    add_index "password_requests", ["user_id"], name: "index_password_requests_on_user_id", using: :btree
    add_index "password_requests", ["uuid"], name: "index_password_requests_on_uuid", using: :btree

    create_table "price_types", force: :cascade do |t|
      t.string   "type"
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "prices", force: :cascade do |t|
      t.integer  "rate_id"
      t.integer  "price_type_id"
      t.decimal  "cost"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
      t.string   "type"
    end

    add_index "prices", ["price_type_id"], name: "index_prices_on_price_type_id", using: :btree
    add_index "prices", ["rate_id"], name: "index_prices_on_rate_id", using: :btree

    create_table "products", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "qb_company_tokens", force: :cascade do |t|
      t.integer "company_id"
      t.uuid    "token"
      t.integer "created_by_user_id"
    end

    add_index "qb_company_tokens", ["company_id"], name: "unique_company_id", unique: true, using: :btree
    add_index "qb_company_tokens", ["token"], name: "unique_token", unique: true, using: :btree

    create_table "question_results", force: :cascade do |t|
      t.integer  "job_id"
      t.integer  "question_id"
      t.integer  "answer_id"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.string   "answer_info"
    end

    add_index "question_results", ["answer_id"], name: "index_question_results_on_answer_id", using: :btree
    add_index "question_results", ["job_id"], name: "index_question_results_on_job_id", using: :btree
    add_index "question_results", ["question_id"], name: "index_question_results_on_question_id", using: :btree

    create_table "questions", force: :cascade do |t|
      t.string   "question"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string   "name"
      t.integer  "order_num"
    end

    create_table "rate_additions", force: :cascade do |t|
      t.integer  "rate_id"
      t.integer  "service_code_id"
      t.string   "name"
      t.string   "calc_type"
      t.decimal  "amount"
      t.datetime "deleted_at"
      t.datetime "created_at",      null: false
      t.datetime "updated_at",      null: false
    end

    add_index "rate_additions", ["rate_id"], name: "index_rate_additions_on_rate_id", using: :btree
    add_index "rate_additions", ["service_code_id"], name: "index_rate_additions_on_service_code_id", using: :btree

    create_table "rate_type_price_types", force: :cascade do |t|
      t.integer  "rate_type_id"
      t.integer  "price_type_id"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
    end

    add_index "rate_type_price_types", ["price_type_id"], name: "index_rate_type_price_types_on_price_type_id", using: :btree
    add_index "rate_type_price_types", ["rate_type_id"], name: "index_rate_type_price_types_on_rate_type_id", using: :btree

    create_table "rate_types", force: :cascade do |t|
      t.string   "type"
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "rates", force: :cascade do |t|
      t.integer  "company_id"
      t.string   "type"
      t.integer  "service_code_id"
      t.integer  "account_id"
      t.decimal  "hourly_p2p"
      t.decimal  "hourly_completion"
      t.decimal  "hookup"
      t.decimal  "miles_towed"
      t.decimal  "miles_towed_free"
      t.decimal  "miles_enroute"
      t.decimal  "miles_enroute_free"
      t.decimal  "special_dolly"
      t.decimal  "gone"
      t.decimal  "storage_daily"
      t.datetime "created_at",          null: false
      t.datetime "updated_at",          null: false
      t.integer  "rate_type_id"
      t.integer  "site_id"
      t.datetime "deleted_at"
      t.integer  "fleet_company_id"
      t.decimal  "hourly"
      t.decimal  "flat"
      t.decimal  "miles_p2p"
      t.decimal  "miles_p2p_free"
      t.string   "import_name"
      t.integer  "import_id"
      t.boolean  "live"
      t.integer  "vehicle_category_id"
      t.integer  "service_group_id"
      t.integer  "account_group_id"
      t.string   "second_day_charge"
      t.string   "beyond_charge"
      t.integer  "seconds_free_window"
      t.string   "round"
      t.string   "nearest"
      t.decimal  "miles_deadhead"
      t.decimal  "miles_deadhead_free"
    end

    add_index "rates", ["account_id"], name: "index_rates_on_account_id", using: :btree
    add_index "rates", ["company_id"], name: "index_rates_on_company_id", using: :btree
    add_index "rates", ["fleet_company_id"], name: "index_rates_on_fleet_company_id", using: :btree
    add_index "rates", ["rate_type_id"], name: "index_rates_on_rate_type_id", using: :btree
    add_index "rates", ["service_code_id"], name: "index_rates_on_service_code_id", using: :btree
    add_index "rates", ["site_id"], name: "index_rates_on_site_id", using: :btree
    add_index "rates", ["vehicle_category_id"], name: "index_rates_on_vehicle_category_id", using: :btree

    create_table "reasons", force: :cascade do |t|
      t.integer  "issue_id",   null: false
      t.string   "name",       null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.datetime "deleted_at"
    end

    add_index "reasons", ["issue_id"], name: "index_reasons_on_issue_id", using: :btree

    create_table "report_results", force: :cascade do |t|
      t.integer  "report_id"
      t.text     "serialized_fields"
      t.text     "serialized_values"
      t.float    "executed_in"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "token"
      t.integer  "company_id"
      t.integer  "user_id"
      t.json     "optional_params"
      t.string   "s3_filename"
      t.string   "executed_sql"
      t.string   "state"
      t.string   "format"
      t.boolean  "collated"
    end

    add_index "report_results", ["company_id"], name: "index_report_results_on_company_id", using: :btree
    add_index "report_results", ["report_id"], name: "index_report_results_on_report_id", using: :btree
    add_index "report_results", ["token"], name: "index_report_results_on_token", unique: true, using: :btree
    add_index "report_results", ["user_id"], name: "index_report_results_on_user_id", using: :btree

    create_table "reports", force: :cascade do |t|
      t.string   "name"
      t.text     "sql"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.json     "default_params"
      t.json     "all_params"
      t.json     "filter_sql"
      t.json     "mandatory_params"
      t.json     "available_params"
      t.string   "company_type"
      t.string   "version"
      t.boolean  "pdf",              default: false
      t.datetime "deleted_at"
      t.integer  "display_order"
      t.string   "display_name"
    end

    create_table "rescue_providers", force: :cascade do |t|
      t.integer  "company_id"
      t.integer  "provider_id"
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.string   "name"
      t.string   "accounting_email"
      t.string   "phone"
      t.string   "dispatch_email"
      t.integer  "location_id"
      t.string   "primary_contact"
      t.string   "primary_email"
      t.string   "primary_phone"
      t.string   "status"
      t.string   "notes"
      t.string   "fax"
      t.datetime "deleted_at"
      t.integer  "site_id"
      t.boolean  "live"
      t.string   "location_code"
      t.string   "vendor_code"
    end

    add_index "rescue_providers", ["company_id", "provider_id", "site_id"], name: "index_rescue_providers_on_company_provider_site", unique: true, using: :btree
    add_index "rescue_providers", ["company_id"], name: "index_rescue_providers_on_company_id", using: :btree
    add_index "rescue_providers", ["live"], name: "index_rescue_providers_on_live", using: :btree
    add_index "rescue_providers", ["location_id"], name: "index_rescue_providers_on_location_id", using: :btree
    add_index "rescue_providers", ["provider_id"], name: "index_rescue_providers_on_provider_id", using: :btree
    add_index "rescue_providers", ["site_id"], name: "index_rescue_providers_on_site_id", using: :btree

    create_table "rescue_providers_tags", force: :cascade do |t|
      t.integer  "rescue_provider_id"
      t.integer  "tag_id"
      t.datetime "created_at",         null: false
      t.datetime "updated_at",         null: false
    end

    add_index "rescue_providers_tags", ["rescue_provider_id"], name: "index_rescue_providers_tags_on_rescue_provider_id", using: :btree
    add_index "rescue_providers_tags", ["tag_id"], name: "index_rescue_providers_tags_on_tag_id", using: :btree

    create_table "reviews", force: :cascade do |t|
      t.string   "phone"
      t.string   "response"
      t.string   "uuid"
      t.integer  "company_id"
      t.datetime "created_at",          null: false
      t.datetime "updated_at",          null: false
      t.integer  "user_id"
      t.string   "text"
      t.integer  "job_id"
      t.integer  "rating"
      t.datetime "emailed_rating_at"
      t.datetime "emailed_feedback_at"
      t.integer  "nps_question1"
      t.integer  "nps_question2"
      t.integer  "nps_question3"
      t.text     "nps_feedback"
      t.string   "type"
      t.string   "form_id"
      t.boolean  "branding"
      t.integer  "nps_question4"
    end

    add_index "reviews", ["company_id"], name: "index_reviews_on_company_id", using: :btree
    add_index "reviews", ["job_id"], name: "index_reviews_on_job_id", using: :btree
    add_index "reviews", ["user_id"], name: "index_reviews_on_user_id", using: :btree

    create_table "roles", force: :cascade do |t|
      t.string   "name"
      t.integer  "resource_id"
      t.string   "resource_type"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

    create_table "searches", force: :cascade do |t|
      t.string   "term"
      t.string   "target"
      t.integer  "company_id"
      t.integer  "user_id"
      t.string   "url"
      t.string   "scope"
      t.datetime "created_at",      null: false
      t.datetime "updated_at",      null: false
      t.integer  "page"
      t.integer  "per_page"
      t.string   "order"
      t.string   "order_direction"
    end

    add_index "searches", ["company_id"], name: "index_searches_on_company_id", using: :btree
    add_index "searches", ["user_id"], name: "index_searches_on_user_id", using: :btree

    create_table "service_codes", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer  "order"
      t.boolean  "addon"
      t.boolean  "variable"
    end

    create_table "settings", force: :cascade do |t|
      t.integer  "company_id"
      t.string   "key"
      t.string   "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "settings", ["company_id", "key"], name: "index_settings_on_company_id_and_key", unique: true, using: :btree
    add_index "settings", ["company_id"], name: "index_settings_on_company_id", using: :btree

    create_table "single_use_access_tokens", force: :cascade do |t|
      t.integer  "user_id"
      t.string   "jwt",        null: false
      t.integer  "iat",        null: false
      t.integer  "exp",        null: false
      t.string   "jti",        null: false
      t.string   "sub",        null: false
      t.datetime "used_at"
      t.datetime "created_at", null: false
    end

    add_index "single_use_access_tokens", ["jwt"], name: "index_single_use_access_tokens_on_jwt", unique: true, using: :btree
    add_index "single_use_access_tokens", ["user_id"], name: "index_single_use_access_tokens_on_user_id", using: :btree

    create_table "sites", force: :cascade do |t|
      t.integer  "location_id"
      t.integer  "company_id"
      t.datetime "created_at",                      null: false
      t.datetime "updated_at",                      null: false
      t.string   "phone"
      t.time     "open_time"
      t.time     "close_time"
      t.integer  "manager_id"
      t.string   "site_code"
      t.string   "description"
      t.string   "type"
      t.string   "name"
      t.string   "import_num"
      t.datetime "deleted_at"
      t.string   "tz"
      t.string   "old_site_code"
      t.boolean  "tire_program"
      t.boolean  "storage_lot"
      t.boolean  "dispatchable"
      t.integer  "owner_company_id"
      t.time     "open_time_sat"
      t.time     "close_time_sat"
      t.time     "open_time_sun"
      t.time     "close_time_sun"
      t.boolean  "always_open"
      t.boolean  "force_open"
      t.boolean  "within_hours",     default: true
    end

    add_index "sites", ["company_id"], name: "index_sites_on_company_id", using: :btree
    add_index "sites", ["location_id"], name: "index_sites_on_location_id", using: :btree
    add_index "sites", ["manager_id"], name: "index_sites_on_manager_id", using: :btree
    add_index "sites", ["owner_company_id"], name: "index_sites_on_owner_company_id", using: :btree
    add_index "sites", ["phone"], name: "index_sites_on_phone", using: :btree

    create_table "slack_channels", force: :cascade do |t|
      t.string   "name"
      t.string   "bot_name",   limit: 32
      t.string   "tag",        limit: 32
      t.string   "channel",    limit: 32
      t.boolean  "active",                 default: true
      t.datetime "created_at",                            null: false
      t.datetime "updated_at",                            null: false
      t.string   "excludes",   limit: 255, default: ""
    end

    create_table "statement_invoicing_ledger_items", force: :cascade do |t|
      t.integer  "statement_id"
      t.integer  "invoicing_ledger_item_id"
      t.datetime "created_at",               null: false
      t.datetime "updated_at",               null: false
    end

    add_index "statement_invoicing_ledger_items", ["invoicing_ledger_item_id"], name: "invoicing_ledger_items_ili_id", using: :btree
    add_index "statement_invoicing_ledger_items", ["statement_id"], name: "index_statement_invoicing_ledger_items_on_statement_id", using: :btree

    create_table "statements", force: :cascade do |t|
      t.string   "uuid"
      t.datetime "sent_at"
      t.string   "period"
      t.datetime "period_start"
      t.datetime "period_end"
      t.integer  "period_value"
      t.integer  "period_year"
      t.datetime "created_at",       null: false
      t.datetime "updated_at",       null: false
      t.integer  "recipient_id"
      t.string   "recipient_type"
      t.integer  "sender_id"
      t.string   "sender_type"
      t.integer  "customer_type_id"
    end

    add_index "statements", ["customer_type_id"], name: "index_statements_on_customer_type_id", using: :btree
    add_index "statements", ["recipient_type", "recipient_id"], name: "index_statements_on_recipient_type_and_recipient_id", using: :btree
    add_index "statements", ["sender_type", "sender_id"], name: "index_statements_on_sender_type_and_sender_id", using: :btree

    create_table "storage_types", force: :cascade do |t|
      t.integer  "company_id"
      t.string   "name"
      t.datetime "deleted_at"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "storage_types", ["company_id"], name: "index_storage_types_on_company_id", using: :btree

    create_table "symptoms", force: :cascade do |t|
      t.integer  "company_id"
      t.string   "name"
      t.datetime "deleted_at"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "symptoms", ["company_id"], name: "index_symptoms_on_company_id", using: :btree

    create_table "tags", force: :cascade do |t|
      t.string   "name"
      t.string   "color"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "time_of_arrivals", force: :cascade do |t|
      t.integer  "job_id"
      t.integer  "eba_type"
      t.datetime "time"
      t.integer  "vehicle_id"
      t.float    "lat"
      t.float    "lng"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer  "user_id"
    end

    add_index "time_of_arrivals", ["job_id"], name: "index_time_of_arrivals_on_job_id", using: :btree
    add_index "time_of_arrivals", ["user_id"], name: "index_time_of_arrivals_on_user_id", using: :btree
    add_index "time_of_arrivals", ["vehicle_id"], name: "index_time_of_arrivals_on_vehicle_id", using: :btree

    create_table "tire_types", force: :cascade do |t|
      t.string   "car"
      t.string   "position"
      t.string   "extras"
      t.string   "size"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.datetime "deleted_at"
    end

    create_table "tires", force: :cascade do |t|
      t.integer  "tire_type_id"
      t.datetime "created_at",   null: false
      t.datetime "updated_at",   null: false
    end

    add_index "tires", ["tire_type_id"], name: "index_tires_on_tire_type_id", using: :btree

    create_table "tt_accounts", force: :cascade do |t|
      t.string   "prefix"
      t.integer  "company_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "tt_messages", force: :cascade do |t|
      t.integer  "msgid",           limit: 8
      t.string   "msg_time"
      t.integer  "msg_class"
      t.integer  "msg_type"
      t.string   "msg_text"
      t.string   "objectno"
      t.integer  "odometer"
      t.integer  "pos_latitude"
      t.integer  "pos_longitude"
      t.string   "pos_text"
      t.string   "pos_postcode"
      t.string   "pos_params"
      t.string   "pos_time"
      t.integer  "speed"
      t.string   "status"
      t.string   "objectuid"
      t.integer  "course"
      t.integer  "direction"
      t.string   "orderno"
      t.integer  "order_state"
      t.integer  "order_type"
      t.integer  "ign"
      t.string   "start_time"
      t.integer  "start_latitude"
      t.integer  "start_longitude"
      t.integer  "start_odometer"
      t.string   "end_time"
      t.integer  "end_latitude"
      t.integer  "end_longitude"
      t.integer  "end_odometer"
      t.string   "trip_type"
      t.integer  "pndconn"
      t.string   "dest_text"
      t.string   "eta"
      t.integer  "distance"
      t.datetime "created_at",                null: false
      t.datetime "updated_at",                null: false
      t.string   "driverno"
      t.string   "driveruid"
      t.float    "dest_latitude"
      t.float    "dest_longitude"
      t.integer  "trip_mode"
      t.string   "inputs"
      t.integer  "workstate"
      t.integer  "source_device"
      t.string   "event_time"
      t.string   "pos_addrno"
      t.string   "user_status"
      t.string   "user_text"
      t.text     "order_addrno"
    end

    add_index "tt_messages", ["msgid"], name: "index_tt_messages_on_msgid", using: :btree

    create_table "tt_objects", force: :cascade do |t|
      t.string   "objectno"
      t.string   "objectname"
      t.string   "objectclassname"
      t.string   "objecttype"
      t.string   "description"
      t.integer  "lastmsgid",                 limit: 8
      t.boolean  "deleted"
      t.string   "msgtime"
      t.string   "longitude"
      t.string   "latitude"
      t.string   "postext"
      t.string   "postext_short"
      t.string   "status"
      t.integer  "driver_currentworkstate"
      t.integer  "codriver_currentworkstate"
      t.integer  "odometer"
      t.integer  "ignition"
      t.integer  "tripmode"
      t.integer  "standstill"
      t.integer  "pndconn"
      t.string   "ignition_time"
      t.string   "pos_time"
      t.integer  "longitude_mdeg"
      t.integer  "latitude_mdeg"
      t.string   "objectuid"
      t.string   "driveruid"
      t.datetime "created_at",                          null: false
      t.datetime "updated_at",                          null: false
      t.integer  "tt_scrape_id"
      t.string   "driver"
      t.string   "drivername"
      t.string   "drivertelmobile"
      t.integer  "speed"
      t.integer  "course"
      t.integer  "direction"
      t.float    "dest_latitude"
      t.float    "dest_longitude"
      t.string   "dest_text"
      t.string   "dest_eta"
      t.boolean  "dest_isorder"
      t.integer  "dest_distance"
      t.integer  "orderno"
    end

    add_index "tt_objects", ["tt_scrape_id"], name: "index_tt_objects_on_tt_scrape_id", using: :btree

    create_table "tt_scrapes", force: :cascade do |t|
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
      t.integer  "tt_account_id"
      t.boolean  "success"
      t.string   "raw"
    end

    add_index "tt_scrapes", ["tt_account_id"], name: "index_tt_scrapes_on_tt_account_id", using: :btree

    create_table "twilio_replies", force: :cascade do |t|
      t.string   "sms_message_sid"
      t.string   "sms_sid"
      t.string   "from_state"
      t.string   "from_country"
      t.string   "sms_status"
      t.string   "body"
      t.string   "to"
      t.string   "message_sid"
      t.string   "account_sid"
      t.string   "from"
      t.datetime "created_at",      null: false
      t.datetime "updated_at",      null: false
      t.integer  "user_id"
      t.text     "service"
      t.integer  "target_id"
      t.string   "target_type"
      t.integer  "job_id"
    end

    add_index "twilio_replies", ["job_id"], name: "index_twilio_replies_on_job_id", using: :btree
    add_index "twilio_replies", ["target_type", "target_id"], name: "index_twilio_replies_on_target_type_and_target_id", using: :btree
    add_index "twilio_replies", ["user_id"], name: "index_twilio_replies_on_user_id", using: :btree

    create_table "update_invoice_locks", id: false, force: :cascade do |t|
      t.integer  "job_id",     null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "update_invoice_locks", ["job_id"], name: "index_update_invoice_locks_on_job_id", unique: true, using: :btree

    create_table "user_experiments", force: :cascade do |t|
      t.integer  "experiment_id"
      t.integer  "user_id"
      t.integer  "variant_id"
      t.boolean  "conversion"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
      t.integer  "job_id"
    end

    add_index "user_experiments", ["experiment_id"], name: "index_user_experiments_on_experiment_id", using: :btree
    add_index "user_experiments", ["job_id"], name: "index_user_experiments_on_job_id", using: :btree
    add_index "user_experiments", ["user_id"], name: "index_user_experiments_on_user_id", using: :btree
    add_index "user_experiments", ["variant_id"], name: "index_user_experiments_on_variant_id", using: :btree

    create_table "user_settings", force: :cascade do |t|
      t.integer  "user_id"
      t.string   "key"
      t.string   "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    add_index "user_settings", ["user_id", "key"], name: "index_user_settings_on_user_id_and_key", unique: true, using: :btree
    add_index "user_settings", ["user_id"], name: "index_user_settings_on_user_id", using: :btree

    create_table "user_sites", force: :cascade do |t|
      t.integer  "user_id",    null: false
      t.integer  "site_id",    null: false
      t.datetime "created_at", null: false
    end

    add_index "user_sites", ["site_id"], name: "index_user_sites_on_site_id", using: :btree
    add_index "user_sites", ["user_id", "site_id"], name: "index_user_sites_on_user_id_and_site_id", unique: true, using: :btree
    add_index "user_sites", ["user_id"], name: "index_user_sites_on_user_id", using: :btree

    create_table "users", force: :cascade do |t|
      t.string   "email"
      t.string   "encrypted_password",      default: "", null: false
      t.string   "reset_password_token"
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer  "sign_in_count",           default: 0,  null: false
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.string   "current_sign_in_ip"
      t.string   "last_sign_in_ip"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "role"
      t.string   "phone"
      t.string   "uuid"
      t.string   "first_name"
      t.string   "last_name"
      t.integer  "company_id"
      t.boolean  "receive_system_alerts"
      t.string   "parse_id"
      t.integer  "drupal_id"
      t.string   "username"
      t.integer  "location_id"
      t.integer  "vehicle_id"
      t.datetime "deleted_at"
      t.integer  "invited_by_user_id"
      t.integer  "invited_by_company_id"
      t.boolean  "new_job_call_alert"
      t.boolean  "new_job_email_alert"
      t.boolean  "new_job_sms_alert"
      t.boolean  "rate_change_email_alert"
      t.string   "timezone"
      t.string   "license_id"
      t.string   "license_state"
      t.string   "storage_relationship"
      t.string   "pager"
      t.boolean  "on_duty"
    end

    add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree
    add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
    add_index "users", ["invited_by_company_id"], name: "index_users_on_invited_by_company_id", using: :btree
    add_index "users", ["invited_by_user_id"], name: "index_users_on_invited_by_user_id", using: :btree
    add_index "users", ["last_name"], name: "index_users_on_last_name", using: :btree
    add_index "users", ["location_id"], name: "index_users_on_location_id", using: :btree
    add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree
    add_index "users", ["uuid"], name: "index_users_on_uuid", using: :btree
    add_index "users", ["vehicle_id"], name: "index_users_on_vehicle_id", using: :btree

    create_table "users_roles", id: false, force: :cascade do |t|
      t.integer "user_id"
      t.integer "role_id"
    end

    add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

    create_table "variants", force: :cascade do |t|
      t.integer  "experiment_id"
      t.string   "name"
      t.json     "data"
      t.datetime "created_at",    null: false
      t.datetime "updated_at",    null: false
      t.integer  "weight"
    end

    add_index "variants", ["experiment_id"], name: "index_variants_on_experiment_id", using: :btree
    add_index "variants", ["name"], name: "index_variants_on_name", using: :btree

    create_table "vehicle_categories", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer  "order_num"
    end

    create_table "vehicle_makes", force: :cascade do |t|
      t.string   "name"
      t.integer  "edmunds_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.boolean  "original"
    end

    create_table "vehicle_models", force: :cascade do |t|
      t.string   "name"
      t.string   "edmunds_id"
      t.integer  "years",           default: [],              array: true
      t.datetime "created_at",                   null: false
      t.datetime "updated_at",                   null: false
      t.integer  "vehicle_make_id"
      t.boolean  "heavy_duty"
      t.integer  "vehicle_type_id"
    end

    add_index "vehicle_models", ["vehicle_make_id"], name: "index_vehicle_models_on_vehicle_make_id", using: :btree
    add_index "vehicle_models", ["vehicle_type_id"], name: "index_vehicle_models_on_vehicle_type_id", using: :btree

    create_table "vehicle_types", force: :cascade do |t|
      t.string   "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end

    create_table "vehicles", force: :cascade do |t|
      t.string   "make"
      t.string   "model"
      t.string   "color"
      t.string   "photo"
      t.string   "vin"
      t.datetime "created_at",                          null: false
      t.datetime "updated_at",                          null: false
      t.string   "name"
      t.string   "license"
      t.integer  "company_id"
      t.string   "company_type"
      t.string   "description"
      t.integer  "year"
      t.string   "uuid"
      t.float    "lat"
      t.float    "lng"
      t.datetime "location_updated_at"
      t.float    "dest_lat"
      t.float    "dest_long"
      t.string   "type"
      t.string   "parse_id"
      t.integer  "number"
      t.datetime "deleted_at"
      t.integer  "vehicle_category_id"
      t.string   "tire_size"
      t.string   "vehicle_type"
      t.string   "serial_number"
      t.boolean  "dedicated_to_swoop",  default: false
    end

    add_index "vehicles", ["company_type", "company_id"], name: "index_vehicles_on_company_type_and_company_id", using: :btree
    add_index "vehicles", ["uuid"], name: "index_vehicles_on_uuid", using: :btree
    add_index "vehicles", ["vehicle_category_id"], name: "index_vehicles_on_vehicle_category_id", using: :btree

    create_table "versions", force: :cascade do |t|
      t.string   "name"
      t.string   "version"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.string   "description"
      t.boolean  "forced"
      t.integer  "delay"
    end

    create_table "ws_events", force: :cascade do |t|
      t.string   "json"
      t.integer  "target_id"
      t.string   "target_type"
      t.datetime "created_at",  null: false
      t.datetime "updated_at",  null: false
      t.integer  "user_id"
      t.string   "request"
    end

    add_index "ws_events", ["created_at", "target_type", "target_id"], name: "index_ws_events_on_created_at_and_target_type_and_target_id", using: :btree
    add_index "ws_events", ["request"], name: "index_ws_events_on_request", using: :btree
    add_index "ws_events", ["user_id"], name: "index_ws_events_on_user_id", using: :btree

    add_foreign_key "accounts", "companies"
    add_foreign_key "accounts", "companies", column: "fleet_company_id"
    add_foreign_key "accounts", "departments"
    add_foreign_key "accounts", "locations"
    add_foreign_key "accounts", "locations", column: "physical_location_id"
    add_foreign_key "answers", "questions"
    add_foreign_key "ar_changes", "commands"
    add_foreign_key "ar_changes", "jobs"
    add_foreign_key "ar_changes", "users"
    add_foreign_key "attached_documents", "jobs"
    add_foreign_key "attached_documents", "users"
    add_foreign_key "audit_emails", "companies"
    add_foreign_key "audit_emails", "jobs"
    add_foreign_key "audit_emails", "users"
    add_foreign_key "audit_isscs", "companies"
    add_foreign_key "audit_isscs", "jobs"
    add_foreign_key "audit_job_statuses", "companies"
    add_foreign_key "audit_job_statuses", "job_statuses"
    add_foreign_key "audit_job_statuses", "jobs"
    add_foreign_key "audit_sms", "jobs"
    add_foreign_key "audit_sms", "users"
    add_foreign_key "auto_assign_assignments", "jobs"
    add_foreign_key "bosch_jobs", "jobs"
    add_foreign_key "commands", "companies"
    add_foreign_key "commands", "event_operations"
    add_foreign_key "commands", "users"
    add_foreign_key "companies", "companies", column: "invited_by_company_id"
    add_foreign_key "companies", "companies", column: "parent_company_id"
    add_foreign_key "companies", "locations"
    add_foreign_key "companies", "users", column: "invited_by_user_id"
    add_foreign_key "companies", "users", column: "payment_contact_id"
    add_foreign_key "companies", "users", column: "primary_contact_id"
    add_foreign_key "companies", "users", column: "support_contact_id"
    add_foreign_key "companies_customer_types", "companies"
    add_foreign_key "companies_customer_types", "customer_types"
    add_foreign_key "companies_features", "companies"
    add_foreign_key "companies_features", "features"
    add_foreign_key "companies_services", "companies"
    add_foreign_key "companies_services", "service_codes"
    add_foreign_key "companies_vehicle_categories", "companies"
    add_foreign_key "companies_vehicle_categories", "vehicle_categories"
    add_foreign_key "company_reports", "companies"
    add_foreign_key "company_reports", "reports"
    add_foreign_key "company_service_questions", "companies"
    add_foreign_key "company_service_questions", "questions"
    add_foreign_key "company_service_questions", "service_codes"
    add_foreign_key "company_symptoms", "companies"
    add_foreign_key "company_symptoms", "symptoms"
    add_foreign_key "departments", "companies"
    add_foreign_key "drives", "users"
    add_foreign_key "drives", "vehicles"
    add_foreign_key "estimates", "jobs"
    add_foreign_key "estimates", "locations", column: "drop_location_id"
    add_foreign_key "estimates", "locations", column: "service_location_id"
    add_foreign_key "estimates", "locations", column: "site_location_id"
    add_foreign_key "explanations", "jobs"
    add_foreign_key "explanations", "reasons"
    add_foreign_key "hidden_users_dispatchers", "users"
    add_foreign_key "hidden_users_dispatchers", "users", column: "dispatcher_id"
    add_foreign_key "hidden_users_sites", "sites"
    add_foreign_key "hidden_users_sites", "users"
    add_foreign_key "hidden_users_storage_sites", "sites"
    add_foreign_key "hidden_users_storage_sites", "users"
    add_foreign_key "hidden_users_storage_types", "storage_types"
    add_foreign_key "hidden_users_storage_types", "users"
    add_foreign_key "images", "jobs"
    add_foreign_key "images", "users"
    add_foreign_key "inventory_items", "accounts", column: "released_to_account_id"
    add_foreign_key "inventory_items", "companies"
    add_foreign_key "inventory_items", "jobs"
    add_foreign_key "inventory_items", "jobs", column: "released_to_job_id"
    add_foreign_key "inventory_items", "sites"
    add_foreign_key "inventory_items", "users"
    add_foreign_key "inventory_items", "users", column: "released_to_user_id"
    add_foreign_key "invoice_pdfs", "invoicing_ledger_items", column: "invoice_id"
    add_foreign_key "invoice_reject_reasons", "companies"
    add_foreign_key "invoice_rejections", "invoice_reject_reasons"
    add_foreign_key "invoice_rejections", "invoicing_ledger_items", column: "invoice_id"
    add_foreign_key "invoicing_ledger_items", "companies", column: "recipient_company_id"
    add_foreign_key "invoicing_ledger_items", "companies", column: "sender_id"
    add_foreign_key "invoicing_ledger_items", "invoicing_ledger_items", column: "incoming_invoice_id"
    add_foreign_key "invoicing_ledger_items", "jobs"
    add_foreign_key "invoicing_ledger_items", "rates"
    add_foreign_key "invoicing_line_items", "jobs"
    add_foreign_key "invoicing_line_items", "rates"
    add_foreign_key "issc_dispatch_requests", "isscs"
    add_foreign_key "issc_locations", "companies", column: "fleet_company_id"
    add_foreign_key "issc_locations", "locations"
    add_foreign_key "issc_locations", "sites"
    add_foreign_key "issc_logins", "companies", column: "fleet_company_id"
    add_foreign_key "issc_logins", "sites"
    add_foreign_key "isscs", "companies"
    add_foreign_key "isscs", "issc_locations"
    add_foreign_key "isscs", "issc_logins"
    add_foreign_key "isscs", "sites"
    add_foreign_key "issues", "companies"
    add_foreign_key "job_errors", "jobs"
    add_foreign_key "job_events", "jobs"
    add_foreign_key "job_events", "users"
    add_foreign_key "job_reject_reasons", "reasons"
    add_foreign_key "jobs", "accounts"
    add_foreign_key "jobs", "companies", column: "fleet_company_id"
    add_foreign_key "jobs", "companies", column: "rescue_company_id"
    add_foreign_key "jobs", "customer_types"
    add_foreign_key "jobs", "departments"
    add_foreign_key "jobs", "departments", column: "partner_department_id"
    add_foreign_key "jobs", "drives", column: "driver_id"
    add_foreign_key "jobs", "estimates"
    add_foreign_key "jobs", "invoicing_ledger_items", column: "invoice_id"
    add_foreign_key "jobs", "issc_dispatch_requests"
    add_foreign_key "jobs", "job_eta_explanations", column: "eta_explanation_id"
    add_foreign_key "jobs", "jobs", column: "original_job_id"
    add_foreign_key "jobs", "jobs", column: "parent_id"
    add_foreign_key "jobs", "locations", column: "drop_location_id"
    add_foreign_key "jobs", "locations", column: "original_service_location_id"
    add_foreign_key "jobs", "locations", column: "service_location_id"
    add_foreign_key "jobs", "reviews"
    add_foreign_key "jobs", "service_codes"
    add_foreign_key "jobs", "sites"
    add_foreign_key "jobs", "sites", column: "fleet_site_id"
    add_foreign_key "jobs", "storage_types"
    add_foreign_key "jobs", "symptoms"
    add_foreign_key "jobs", "tire_types"
    add_foreign_key "jobs", "users"
    add_foreign_key "jobs", "users", column: "caller_id"
    add_foreign_key "jobs", "users", column: "dropoff_contact_id"
    add_foreign_key "jobs", "users", column: "fleet_dispatcher_id"
    add_foreign_key "jobs", "users", column: "last_touched_by_id"
    add_foreign_key "jobs", "users", column: "partner_dispatcher_id"
    add_foreign_key "jobs", "users", column: "pickup_contact_id"
    add_foreign_key "jobs", "users", column: "rescue_driver_id"
    add_foreign_key "jobs", "users", column: "root_dispatcher_id"
    add_foreign_key "jobs", "vehicle_categories", column: "invoice_vehicle_category_id"
    add_foreign_key "jobs", "vehicles", column: "rescue_vehicle_id"
    add_foreign_key "jobs", "vehicles", column: "trailer_vehicle_id"
    add_foreign_key "locations", "jobs"
    add_foreign_key "locations", "sites"
    add_foreign_key "locations", "users"
    add_foreign_key "metrics", "companies"
    add_foreign_key "metrics", "users"
    add_foreign_key "new_invoice_locks", "jobs"
    add_foreign_key "password_requests", "users"
    add_foreign_key "prices", "price_types"
    add_foreign_key "prices", "rates"
    add_foreign_key "question_results", "answers"
    add_foreign_key "question_results", "jobs"
    add_foreign_key "question_results", "questions"
    add_foreign_key "rate_additions", "rates"
    add_foreign_key "rate_additions", "service_codes"
    add_foreign_key "rate_type_price_types", "price_types"
    add_foreign_key "rate_type_price_types", "rate_types"
    add_foreign_key "rates", "accounts"
    add_foreign_key "rates", "companies"
    add_foreign_key "rates", "companies", column: "fleet_company_id"
    add_foreign_key "rates", "rate_types"
    add_foreign_key "rates", "service_codes"
    add_foreign_key "rates", "sites"
    add_foreign_key "rates", "vehicle_categories"
    add_foreign_key "reasons", "issues"
    add_foreign_key "report_results", "companies"
    add_foreign_key "report_results", "reports"
    add_foreign_key "report_results", "users"
    add_foreign_key "rescue_providers", "companies"
    add_foreign_key "rescue_providers", "companies", column: "provider_id"
    add_foreign_key "rescue_providers", "locations"
    add_foreign_key "rescue_providers", "sites"
    add_foreign_key "rescue_providers_tags", "rescue_providers"
    add_foreign_key "rescue_providers_tags", "tags"
    add_foreign_key "reviews", "companies"
    add_foreign_key "reviews", "jobs"
    add_foreign_key "reviews", "users"
    add_foreign_key "searches", "companies"
    add_foreign_key "searches", "users"
    add_foreign_key "settings", "companies"
    add_foreign_key "single_use_access_tokens", "users"
    add_foreign_key "sites", "companies"
    add_foreign_key "sites", "companies", column: "owner_company_id"
    add_foreign_key "sites", "locations"
    add_foreign_key "sites", "users", column: "manager_id"
    add_foreign_key "statement_invoicing_ledger_items", "invoicing_ledger_items"
    add_foreign_key "statement_invoicing_ledger_items", "statements"
    add_foreign_key "statements", "customer_types"
    add_foreign_key "storage_types", "companies"
    add_foreign_key "symptoms", "companies"
    add_foreign_key "time_of_arrivals", "jobs"
    add_foreign_key "time_of_arrivals", "users"
    add_foreign_key "time_of_arrivals", "vehicles"
    add_foreign_key "tires", "tire_types"
    add_foreign_key "tt_objects", "tt_scrapes"
    add_foreign_key "tt_scrapes", "tt_accounts"
    add_foreign_key "twilio_replies", "jobs"
    add_foreign_key "twilio_replies", "users"
    add_foreign_key "update_invoice_locks", "jobs"
    add_foreign_key "user_experiments", "experiments"
    add_foreign_key "user_experiments", "jobs"
    add_foreign_key "user_experiments", "users"
    add_foreign_key "user_experiments", "variants"
    add_foreign_key "user_settings", "users"
    add_foreign_key "user_sites", "sites"
    add_foreign_key "user_sites", "users"
    add_foreign_key "users", "companies"
    add_foreign_key "users", "companies", column: "invited_by_company_id"
    add_foreign_key "users", "locations"
    add_foreign_key "users", "users", column: "invited_by_user_id"
    add_foreign_key "users", "vehicles"
    add_foreign_key "variants", "experiments"
    add_foreign_key "vehicle_models", "vehicle_makes"
    add_foreign_key "vehicle_models", "vehicle_types"
    add_foreign_key "vehicles", "vehicle_categories"
    add_foreign_key "ws_events", "users"
  end

  def down
    raise ActiveRecord::IrreversibleMigration, "The initial migration is not revertable"
  end
end
