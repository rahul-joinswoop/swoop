class CreateSourceApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :source_applications, id: :integer do |t|
      t.string :source, limit: 60, null: false
      t.string :platform, limit: 30, null: false
      t.string :version, limit: 60, null: true
      t.integer :oauth_application_id, null: true, index: true
    end

    add_foreign_key :source_applications, :oauth_applications
    add_index :source_applications, [:source, :platform, :version, :oauth_application_id], unique: true, name: "idx_source_applications_unique"
    add_index :source_applications, [:platform, :version]
  end
end
