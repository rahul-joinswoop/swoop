class AddNotNullDistance < ActiveRecord::Migration[5.2]
  def change
    change_column_null :companies, :distance_unit, false
  end
end
