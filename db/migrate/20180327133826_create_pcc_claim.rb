class CreatePCCClaim < ActiveRecord::Migration[5.0]
  def change
    create_table :pcc_claims do |t|
      t.references :pcc_policy
      t.references :pcc_vehicle
      t.references :pcc_user
      t.references :pcc_coverage
      t.references :company

      t.string  :claim_number
      t.json :data
    end
  end
end
