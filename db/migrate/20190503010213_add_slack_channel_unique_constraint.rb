class AddSlackChannelUniqueConstraint < ActiveRecord::Migration[5.2]

  def change
    add_index :slack_channels, [:tag, :channel], unique: true, where: "active"
  end

end
