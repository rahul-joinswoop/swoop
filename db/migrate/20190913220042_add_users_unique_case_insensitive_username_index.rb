# frozen_string_literal: true

class AddUsersUniqueCaseInsensitiveUsernameIndex < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    add_index :users, :_x_username, name: "index_users_on_unique_username", unique: true, where: "_x_username IS NOT NULL AND deleted_at IS NULL", algorithm: :concurrently
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:users, [:_x_username], name: "index_users_on_unique_username")
      remove_index :users, name: "index_users_on_unique_username"
    end
    raise e
  end

  def down
    remove_index :users, name: "index_users_on_unique_username"
  end

end
