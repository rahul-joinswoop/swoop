class AddJobBillingInfoGoaAmount < ActiveRecord::Migration[5.1]

  def up
    add_column :job_billing_infos, :goa_amount, :decimal, null: true, precision: 20, scale: 4
    add_column :job_billing_infos, :goa_agent_id, :bigint, null: true
    change_column :job_billing_infos, :agent_id, :bigint, null: true
  end

  def down
    remove_column :job_billing_infos, :goa_amount, :decimal, null: true, precision: 20, scale: 4
    remove_column :job_billing_infos, :goa_agent_id, :bigint, null: true
    change_column :job_billing_infos, :agent_id, :bigint, null: false
  end

end
