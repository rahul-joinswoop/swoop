class RemoveJobSequences < ActiveRecord::Migration[5.1]
  def change
    remove_column :jobs, :owner_sequence, :integer
    remove_column :jobs, :rescue_sequence, :integer
  end
end
