class AddHideTimestampsOnInvoiceToAccount < ActiveRecord::Migration[4.2]
  def change
    add_column :accounts, :hide_timestamps_on_invoice, :boolean
  end
end
