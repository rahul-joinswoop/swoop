class RemoveJobNullConstraintFromAttachedDocuments < ActiveRecord::Migration[4.2]
  def change
    change_column_null :attached_documents, :job_id, true
  end
end
