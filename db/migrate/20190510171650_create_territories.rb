class CreateTerritories < ActiveRecord::Migration[5.2]
  def change
    create_table :territories do |t|
      t.references :company, foreign_key: true, null: false
      t.st_polygon :polygon_area, null: false
      t.timestamp :deleted_at, null: true

      t.timestamps
    end
  end
end
