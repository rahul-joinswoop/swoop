class AddCompanyToAutoAssignCandidates < ActiveRecord::Migration[4.2]
  def change
    add_reference :auto_assign_candidates, :company, index: true, foreign_key: true
  end
end
