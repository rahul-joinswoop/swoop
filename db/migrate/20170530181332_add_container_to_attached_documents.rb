class AddContainerToAttachedDocuments < ActiveRecord::Migration[4.2]
  def change
    add_column :attached_documents, :container, :string
  end
end
