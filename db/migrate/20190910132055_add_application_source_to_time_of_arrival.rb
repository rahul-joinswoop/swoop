class AddApplicationSourceToTimeOfArrival < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_of_arrivals, :source_application, index: false, foreign_key: true
  end
end
