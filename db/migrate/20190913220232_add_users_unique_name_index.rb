# frozen_string_literal: true

class AddUsersUniqueNameIndex < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    begin
      add_index :users, [:company_id, :_x_last_name, :_x_first_name], name: "index_users_on_unique_full_name", unique: true, where: "company_id IS NOT NULL AND deleted_at IS NULL", algorithm: :concurrently
    rescue => e
      # With DDL transactions disabled, we need to manually remove the index if something goes wrong
      if index_exists?(:users, [:company_id, :_x_last_name, :_x_first_name], name: "index_users_on_unique_full_name")
        remove_index :users, name: "index_users_on_unique_full_name"
      end
      raise e
    end

    # This index is superfluous
    remove_index :users, :company_id
  end

  def down
    add_index :users, :company_id, algorithm: :concurrently
    remove_index :users, name: "index_users_on_unique_full_name"
  end

end
