class AddFriendlyTypeAndIsStandardToRateTypes < ActiveRecord::Migration[5.1]
  def change
    add_column :rate_types, :friendly_type, :string
    add_column :rate_types, :is_standard, :boolean

    add_index :rate_types, :friendly_type, unique: true
    add_index :rate_types, :type, unique: true
    add_index :rate_types, :name, unique: true
  end
end
