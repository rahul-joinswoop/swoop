class AddPhoneIndexToReview < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :reviews, :phone, algorithm: :concurrently
  end
end
