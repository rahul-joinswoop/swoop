class AddIsStandardToIssues < ActiveRecord::Migration[4.2]
  def change
    add_column :issues, :is_standard, :boolean
  end
end
