class CreateExperimentMetrics < ActiveRecord::Migration[5.1]

  def change
    create_table :experiment_metrics do |t|
      t.references :target, polymorphic: true
      t.references :variant, foreign_key: true
      t.string :label
      t.integer :value

      t.timestamps
    end
  end

end
