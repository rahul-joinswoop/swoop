class AddEtaToAutoAssignAssignments < ActiveRecord::Migration[4.2]
  def change
    add_column :auto_assign_assignments, :eta, :integer
  end
end
