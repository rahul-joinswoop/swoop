class LanguageNotNullOnCompany < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def up
    fail 'DEFAULT_LANGUAGE not set!' unless ENV['DEFAULT_LANGUAGE'].present?

    Company.unscoped.where(language: nil).in_batches(of: 100) do |relation|
      relation.update_all(language: ENV['DEFAULT_LANGUAGE'])

      sleep(0.1)
    end

    transaction do
      Company.unscoped.where(language: nil).update_all(language: ENV['DEFAULT_LANGUAGE'])

      change_column_null :companies, :language, false
    end
  end

  def down
    change_column_null :companies, :language, true
  end
end
