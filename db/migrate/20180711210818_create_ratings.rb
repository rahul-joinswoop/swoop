class CreateRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :ratings do |t|
      t.references :rescue_company, foreign_key: { to_table: :companies }
      t.references :client_company, foreign_key: { to_table: :companies }
      t.integer :num_reviews
      t.integer :num_jobs
      t.float :score

      t.timestamps
    end
  end
end
