class CreateCompanyRatingSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :company_rating_settings do |t|
      t.references :company, foreign_key: true
      t.json :settings

      t.timestamps
    end
  end
end
