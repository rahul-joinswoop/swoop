class AddSendAtToSmsAlert < ActiveRecord::Migration[4.2]
  def change
    add_column :sms_alerts, :send_at, :datetime
    add_index :sms_alerts, :send_at
  end
end
