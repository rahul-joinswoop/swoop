# frozen_string_literal: true

class AddShaKeysForUsherToUsers < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def change
    add_column :users, :username_key, :string, limit: 64
    add_column :users, :email_key, :string, limit: 64
  end

end
