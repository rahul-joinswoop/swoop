class RemovePayoutDelayDaysFromCompanyFees < ActiveRecord::Migration[5.0]
  def change
    remove_column :company_invoice_charge_fees, :payout_delay_days, :integer
  end
end
