class CreateTargetVariants < ActiveRecord::Migration[5.1]

  def change
    create_table :target_variants do |t|
      t.references :target, polymorphic: true
      t.references :variant, foreign_key: true

      t.timestamps
    end
  end

end
