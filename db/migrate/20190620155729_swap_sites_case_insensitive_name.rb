# frozen_string_literal: true

class SwapSitesCaseInsensitiveName < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    Site.unscoped.in_batches(of: 50) do |batch|
      ids = batch.pluck(:id)
      transaction do
        update "UPDATE sites SET ci_name = COALESCE(name, 'HQ') where name != ci_name AND id IN (#{ids.join(', ')})"
      end
    end

    transaction do
      # Run a final update command inside a transaction to ensure the data is in sync
      execute "UPDATE sites SET ci_name = COALESCE(name, 'HQ') where name != ci_name"
      rename_column :sites, :name, :_x_name
      rename_column :sites, :ci_name, :name
    end
  end

  def down
    execute "UPDATE sites SET _x_name = name where _x_name != name"
    transaction do
      execute "UPDATE sites SET _x_name = name where _x_name != name"
      rename_column :sites, :name, :ci_name
      rename_column :sites, :_x_name, :name
    end
  end

end
