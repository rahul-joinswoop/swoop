# frozen_string_literal: true

class AddCurrencyToModels < ActiveRecord::Migration[5.2]

  TABLES = [
    :companies,
    :company_invoice_charge_fees,
    :invoice_payment_charges,
    :invoicing_line_items,
    :invoicing_tax_rates,
    :job_billing_infos,
    :prices,
    :rate_additions,
    :rates,
  ].freeze

  # if ENV['DEFAULT_CURRENCY'] is set when we build the db then we'll use that value as our
  # default, otherwise USD
  DEFAULT = ENV.fetch("DEFAULT_CURRENCY", "USD")

  def change
    TABLES.each do |table|
      add_column table, :currency, :string, limit: 3, null: false, default: DEFAULT
    end

    reversible do |dir|
      # ISO codes for currency are uppercase so convert our existing entries
      dir.up do
        InvoicingLedgerItem.update_all currency: 'USD'
      end
      dir.down do
        InvoicingLedgerItem.update_all currency: 'usd'
      end
    end
  end

end
