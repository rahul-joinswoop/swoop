class ChangeCompanyCommissionsCompanyIdToNotNull < ActiveRecord::Migration[4.2]
  def change
    change_column :company_commissions, :company_id, :integer, null: false
  end
end
