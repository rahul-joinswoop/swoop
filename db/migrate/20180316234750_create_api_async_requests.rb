class CreateAPIAsyncRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :api_async_requests do |t|
      t.references :company, index: true, foreign_key: true, null: false
      t.references :user, index: true, foreign_key: true, null: false
      t.json :request
      t.timestamps
    end
  end
end
