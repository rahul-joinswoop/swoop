class AddAuctionToAutoAssignAssignment < ActiveRecord::Migration[4.2]
  def change
    add_column :auto_assign_assignments, :max_target_eta, :integer
    add_column :auto_assign_assignments, :max_distance, :integer
    add_reference :auto_assign_assignments, :service_code, index: true, foreign_key: true
    add_column :auto_assign_assignments, :expires_at, :datetime
  end
end
