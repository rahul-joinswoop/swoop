class UpdateAutoAssignTruckCandidateToJobCandidateTruck < ActiveRecord::Migration[5.1]
  def change
    Job::Candidate.where(type: 'AutoAssign::TruckCandidate').find_in_batches(batch_size: 100) do |batch|
      Job::Candidate.transaction do
        batch.each do |candidate|
          candidate.update_column(:type, 'Job::Candidate::Truck')
        end
      end
    end
  end
end
