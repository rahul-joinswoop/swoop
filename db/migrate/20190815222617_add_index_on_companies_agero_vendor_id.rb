class AddIndexOnCompaniesAgeroVendorId < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    add_index :companies, [:agero_vendor_id], algorithm: :concurrently, unique: true, where: "agero_vendor_id IS NOT NULL AND deleted_at IS NULL"
  rescue => e
    if index_exists?(:companies, [:agero_vendor_id])
      remove_index :companies, [:agero_vendor_id]
    end

    raise e
  end

  def down
    remove_index :companies, [:agero_vendor_id]
  end

end
