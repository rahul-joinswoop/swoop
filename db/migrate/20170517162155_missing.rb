class Missing < ActiveRecord::Migration[4.2]
  def change
   add_column :features, :deleted_at, :datetime
   add_index :features, [:name], unique: true
   add_column :service_codes, :deleted_at, :datetime
   add_index :service_codes, [:name], unique: true
  end
end
