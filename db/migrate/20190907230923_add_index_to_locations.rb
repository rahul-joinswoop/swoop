# frozen_string_literal: true

class AddIndexToLocations < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    add_index :locations, [:lnglat], using: :gist, algorithm: :concurrently
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:locations, [:lnglat])
      remove_index :locations, [:lnglat]
    end
    raise e
  end

  def down
    remove_index :locations, [:lnglat]
  end

end
