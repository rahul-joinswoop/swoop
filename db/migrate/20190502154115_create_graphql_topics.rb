class CreateGraphQLTopics < ActiveRecord::Migration[5.2]

  def change
    create_table :graphql_topics do |t|
      t.string :name, index: true, unique: true, null: false
      t.timestamps
    end
  end

end
