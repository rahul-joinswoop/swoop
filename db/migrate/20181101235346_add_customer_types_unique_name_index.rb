class AddCustomerTypesUniqueNameIndex < ActiveRecord::Migration[5.1]
  def change
    add_index :customer_types, :name, unique: true
  end
end
