class CreateDriverCommissions < ActiveRecord::Migration[4.2]
  def change
    create_table :driver_commissions do |t|
      t.references :user, index: true, foreign_key: true
      t.references :company, index: true, foreign_key: true
      t.integer :commission_pct

      t.timestamps null: false
    end
  end
end
