class AddLanguageToCompany < ActiveRecord::Migration[5.2]
  def up
    add_column :companies, :language, :string, null: true
  end

  def down
    remove_column :companies, :language
  end
end
