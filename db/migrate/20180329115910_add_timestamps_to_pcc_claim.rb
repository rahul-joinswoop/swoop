class AddTimestampsToPCCClaim < ActiveRecord::Migration[5.0]
  def change
    add_column :pcc_claims, :created_at, :datetime, null: false
    add_column :pcc_claims, :updated_at, :datetime, null: false
  end
end
