class AddDeletedAtToCustomAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :custom_accounts, :deleted_at, :datetime

    remove_index :custom_accounts, :company_id # to remove current uniqueness
    add_index :custom_accounts, :company_id
  end
end
