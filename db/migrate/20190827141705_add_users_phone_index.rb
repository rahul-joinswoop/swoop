# frozen_string_literal: true

class AddUsersPhoneIndex < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    add_index :users, [:phone], algorithm: :concurrently, where: "phone IS NOT NULL"
  rescue => e
    if index_exists?(:users, [:phone])
      remove_index :users, [:phone]
    end

    raise e
  end

  def down
    remove_index :users, [:phone]
  end

end
