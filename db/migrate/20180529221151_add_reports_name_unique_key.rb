class AddReportsNameUniqueKey < ActiveRecord::Migration[5.0]
  def change
    add_index :reports, :name, :unique => true
  end
end
