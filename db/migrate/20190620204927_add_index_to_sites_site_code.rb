# frozen_string_literal: true

class AddIndexToSitesSiteCode < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    # Make sure there are no duplicates. Verified already that there are no dups in production,
    # but don't want to blow up on invalid data in development environments.
    last_site_code = nil
    num = 1
    select_all("SELECT DISTINCT s2.id, s2.company_id, s2.site_code, s2.type FROM sites s1, sites s2 WHERE s1.company_id = s2.company_id AND s1.site_code = s2.site_code AND s1.id < s2.id AND s1.deleted_at IS NULL AND s2.deleted_at IS NULL AND s1.type = s2.type order by s2.company_id, s2.site_code").each do |row|
      site_code = "#{row['company_id']}:#{row['site_code']}:#{row['type']}".downcase
      if name == last_site_code
        num += 1
      else
        num = 1
        last_site_code = site_code
      end
      update("UPDATE sites SET site_code = CONCAT(site_code, '.#{num}') WHERE id = #{row['id']}")
    end

    add_index :sites, [:company_id, :site_code, :type], unique: true, name: "index_sites_on_unique_site_code", where: "deleted_at IS NULL AND site_code IS NOT NULL", algorithm: :concurrently
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:sites, [:company_id, :site_code, :type], unique: true, name: "index_sites_on_unique_site_code")
      remove_index :sites, name: "index_sites_on_unique_site_code", algorithm: :concurrently
    end
    raise e
  end

  def down
    remove_index :sites, name: "index_sites_on_unique_site_code", algorithm: :concurrently
  end

end
