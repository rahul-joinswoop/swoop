class CreateCompanyCommissions < ActiveRecord::Migration[4.2]
  def change
    create_table :company_commissions do |t|
      t.references :company, index: true, foreign_key: true
      t.integer :default_pct

      t.timestamps null: false
    end
  end
end
