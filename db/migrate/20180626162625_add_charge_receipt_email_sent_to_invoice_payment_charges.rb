class AddChargeReceiptEmailSentToInvoicePaymentCharges < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_payment_charges, :charge_receipt_email_sent, :boolean, default: false
  end
end
