class CreatePCCVehicles < ActiveRecord::Migration[5.0]
  def change
    create_table :pcc_vehicles do |t|
      t.references :pcc_policy
      t.json :data
      
      t.timestamps
    end
  end
end
