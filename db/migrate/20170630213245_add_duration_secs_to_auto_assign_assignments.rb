class AddDurationSecsToAutoAssignAssignments < ActiveRecord::Migration[4.2]
  def change
    add_column :auto_assign_assignments, :duration_secs, :integer
  end
end
