class AddRefundIdToInvoicePaymentCharge < ActiveRecord::Migration[5.0]
  def change
    add_reference :invoice_payment_charges, :refund, index: true, foreign_key: {
      to_table: :invoicing_ledger_items,
    }

    remove_index :invoice_payment_charges, :refund_id
    add_index :invoice_payment_charges, :refund_id, unique: true
  end
end
