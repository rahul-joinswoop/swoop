class DropPriceAndInvoicingTaxRates < ActiveRecord::Migration[5.2]
  def up
    drop_table :prices
    drop_table :invoicing_tax_rates
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
