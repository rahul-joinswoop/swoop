class AddAttrsToAutoAssignCandidates < ActiveRecord::Migration[4.2]
  def change
    add_column :auto_assign_candidates, :lat, :float
    add_column :auto_assign_candidates, :lng, :float

    add_reference :auto_assign_candidates, :driver, references: :users, index: true
    add_foreign_key :auto_assign_candidates, :users, column: :driver_id


    
    add_column :auto_assign_candidates, :location_updated_at, :datetime
  end
end
