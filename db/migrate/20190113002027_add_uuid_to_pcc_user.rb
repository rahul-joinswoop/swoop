class AddUuidToPCCUser < ActiveRecord::Migration[5.1]

  def change
    add_column :pcc_users, :uuid, :string, limit: 32
  end

end
