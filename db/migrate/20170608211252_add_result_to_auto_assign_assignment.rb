class AddResultToAutoAssignAssignment < ActiveRecord::Migration[4.2]
  def change
    add_column :auto_assign_assignments, :result, :string
  end
end
