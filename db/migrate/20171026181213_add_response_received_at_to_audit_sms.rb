class AddResponseReceivedAtToAuditSms < ActiveRecord::Migration[4.2]
  def change
    add_column :audit_sms, :response_received_at, :datetime
  end
end
