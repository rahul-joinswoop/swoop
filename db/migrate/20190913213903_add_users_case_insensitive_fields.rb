# frozen_string_literal: true

class AddUsersCaseInsensitiveFields < ActiveRecord::Migration[5.2]

  def change
    safety_assured do
      change_table :users do |t|
        t.column :_x_first_name, :citext, null: true
        t.column :_x_last_name, :citext, null: true
        t.column :_x_email, :citext, null: true
        t.column :_x_username, :citext, null: true
      end
    end
  end

end
