class AddEmptyToReportResult < ActiveRecord::Migration[5.0]
  def change
    add_column :report_results, :empty, :boolean
  end
end
