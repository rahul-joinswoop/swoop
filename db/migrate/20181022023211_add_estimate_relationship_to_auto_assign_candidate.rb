class AddEstimateRelationshipToAutoAssignCandidate < ActiveRecord::Migration[5.1]
  def change
    # An auto assign candidate says, "This is the estimate of time and distance for me to do the job"
    add_reference :auto_assign_candidates, :estimate, foreign_key: true
  end
end
