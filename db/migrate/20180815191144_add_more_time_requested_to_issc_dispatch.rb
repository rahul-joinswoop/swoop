class AddMoreTimeRequestedToIsscDispatch < ActiveRecord::Migration[5.1]
  def change
    add_column :issc_dispatch_requests, :more_time_requested, :boolean, default: false
  end
end
