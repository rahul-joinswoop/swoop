# frozen_string_literal: true

class AddUniqueIndexOnQuestions < ActiveRecord::Migration[5.2]

  def change
    add_index :questions, :question, unique: true
  end

end
