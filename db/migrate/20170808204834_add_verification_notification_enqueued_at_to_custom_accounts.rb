class AddVerificationNotificationEnqueuedAtToCustomAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :custom_accounts, :verification_notification_enqueued_at, :datetime
  end
end
