class AddIsStandardToSymptoms < ActiveRecord::Migration[4.2]
  def change
    add_column :symptoms, :is_standard, :boolean
  end
end
