class ChangeCandidateMilesColumnTypes < ActiveRecord::Migration[4.2]
  def change
    change_column :auto_assign_candidates, :google_miles, :integer
    change_column :auto_assign_candidates, :crowflies_miles, :integer
  end
end
