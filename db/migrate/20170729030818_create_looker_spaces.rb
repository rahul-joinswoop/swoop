class CreateLookerSpaces < ActiveRecord::Migration[4.2]
  def change
    create_table :looker_spaces do |t|
      t.integer :spaceid
      t.string :name

      t.timestamps null: false
    end
  end
end
