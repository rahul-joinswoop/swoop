class AddHasOrHadARootDispatcherAssignedToJobs < ActiveRecord::Migration[4.2]
  def change
    add_column :jobs, :has_or_had_a_root_dispatcher_assigned, :boolean, default: false
  end
end
