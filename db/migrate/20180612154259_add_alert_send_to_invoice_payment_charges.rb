class AddAlertSendToInvoicePaymentCharges < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_payment_charges, :alert_sent, :boolean, default: false
  end
end
