class AddLookerDashboardIdToLookerDashboard < ActiveRecord::Migration[4.2]
  def change
    add_column :looker_dashboards, :dashboardid, :integer
  end
end
