class AddAacToBid < ActiveRecord::Migration[5.0]
  def change
    add_reference :bids, :candidate, foreign_key: {
      to_table: :auto_assign_candidates,
    }
    add_column :bids, :cost, :integer
    add_column :bids, :rating, :integer
    add_column :bids, :score, :float
  end
end
