class AddChosenVehicleToAutoAssignAssignment < ActiveRecord::Migration[4.2]
  def change
    add_reference :auto_assign_assignments, :vehicle, index: true, foreign_key: true
  end
end
