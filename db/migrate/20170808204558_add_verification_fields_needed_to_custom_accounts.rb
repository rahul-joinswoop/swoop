class AddVerificationFieldsNeededToCustomAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :custom_accounts, :verification_fields_needed, :text
  end
end
