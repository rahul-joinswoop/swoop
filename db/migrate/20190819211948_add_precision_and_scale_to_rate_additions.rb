class AddPrecisionAndScaleToRateAdditions < ActiveRecord::Migration[5.2]
  def up
    change_column :rate_additions, :amount, :decimal, precision: 20, scale: 4
  end

  def down
    change_column :rate_additions, :amount, :decimal
  end
end
