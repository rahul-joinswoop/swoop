class CreateSmsAlerts < ActiveRecord::Migration[4.2]
  def change
    create_table :sms_alerts do |t|
      t.string :type
      t.references :job, index: true, foreign_key: true
      t.datetime :sent_at
      t.datetime :canceled_at

      t.timestamps null: false
    end
    add_index :sms_alerts, :type
    add_index :sms_alerts, :sent_at
    add_index :sms_alerts, :canceled_at
  end
end
