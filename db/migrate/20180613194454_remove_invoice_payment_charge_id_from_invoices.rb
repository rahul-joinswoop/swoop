class RemoveInvoicePaymentChargeIdFromInvoices < ActiveRecord::Migration[5.0]
  def change
    remove_column :invoicing_ledger_items, :invoice_payment_charge_id
  end
end
