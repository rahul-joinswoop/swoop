class AddFullNameToPCCUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :pcc_users, :full_name, :string, not_null: true
  end
end
