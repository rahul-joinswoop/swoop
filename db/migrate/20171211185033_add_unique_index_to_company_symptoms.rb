class AddUniqueIndexToCompanySymptoms < ActiveRecord::Migration[4.2]
  def change
    add_index :company_symptoms, [:company_id, :symptom_id], unique: true
  end
end
