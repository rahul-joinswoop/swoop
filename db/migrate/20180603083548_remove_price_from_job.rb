class RemovePriceFromJob < ActiveRecord::Migration[5.0]
  def change
    remove_column :jobs, :price, :double
  end
end
