class RemoveDrupalIdFromJob < ActiveRecord::Migration[5.0]
  def change
    remove_column :jobs, :drupal_id, :integer
  end
end
