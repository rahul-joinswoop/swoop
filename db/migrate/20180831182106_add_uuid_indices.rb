class AddUuidIndices < ActiveRecord::Migration[5.1]
  def change
    add_index :audit_sms, :uuid
    add_index :invoicing_ledger_items, :uuid
    add_index :reviews, :uuid
    add_index :statements, :uuid
  end
end
