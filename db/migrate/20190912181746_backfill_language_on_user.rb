class BackfillLanguageOnUser < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def up
    default_language = ENV['DEFAULT_LANGUAGE']
    fail 'Check DEFAULT_LANGUAGE' unless ['en', 'de'].include?(default_language)

    # Update all users without associated companies ( 5 mil + )
    User.unscoped.where(language: nil).where(company_id: nil).in_batches(of: 1000) do |relation|
      relation.update_all(language: default_language)
      sleep(0.01)
    end

    # Update all users with company ( ~ 33k )
    User.unscoped.where(language: nil).select(:id).in_batches(of: 1000) do |relation|
      ActiveRecord::Base.connection.execute(<<-SQL.squish)
        UPDATE users
        SET language = companies.language
        FROM companies
        WHERE users.company_id = companies.id
        AND users.id IN ( #{relation.pluck(:id).join(',')} )
      SQL

      sleep(0.01)
    end

    transaction do
      User.unscoped.where(language: nil).preload(:company).each do |user|
        new_language = user.company ? user.company.language : default_language
        user.update_attributes(language: new_language)
      end

      change_column_null :users, :language, false
    end
  end

  def down
    change_column_null :users, :language, true
  end
end
