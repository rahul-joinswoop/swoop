class CreateCustomAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :custom_accounts do |t|
      t.references :company, index: true, foreign_key: true
      t.string :upstream_account_id, null: false
      t.timestamps null: false
    end

    add_index :custom_accounts, :upstream_account_id, unique: true
  end
end
