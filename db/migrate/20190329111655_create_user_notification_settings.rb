class CreateUserNotificationSettings < ActiveRecord::Migration[5.1]
  def up
    create_table :user_notification_settings do |t|
      t.references :user, null: false
      t.string :notification_channels, array: true, default: []
      t.string :notification_code, null: false
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end

    add_index :user_notification_settings,
              [:user_id, :notification_code],
              name: :index_user_notification_settings_unique_code_by_user,
              unique: true
  end

  def down
    drop_table :user_notification_settings
  end
end
