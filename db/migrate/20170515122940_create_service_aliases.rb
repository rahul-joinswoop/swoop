class CreateServiceAliases < ActiveRecord::Migration[4.2]
  def change
    create_table :service_aliases do |t|
      t.references :company, index: true, foreign_key: true, null: false
      t.references :service_code, index: true, foreign_key: true, null: false
      t.string :alias, null: false
      t.timestamps null: false
    end

    add_index :service_aliases, [:company_id, :alias], unique: true
  end
end
