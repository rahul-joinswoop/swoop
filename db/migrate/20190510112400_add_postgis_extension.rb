class AddPostgisExtension < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'postgis' unless extension_enabled?('postgis')
  end
end
