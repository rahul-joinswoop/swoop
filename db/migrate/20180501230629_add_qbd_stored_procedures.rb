class AddQbdStoredProcedures < ActiveRecord::Migration[5.0]
  def up
    # Special case: these statements were already run as special one off directly in production when
    # they were first created. They should be idempotent, but seeing as they are incredibly complicated,
    # it's probably best not to find out the hard way.
    return if Rails.env.production?
    
    execute <<-SQL
    CREATE OR REPLACE FUNCTION utc_now()
      RETURNS timestamp
      AS $function$
        BEGIN
          RETURN timezone('UTC',now())::timestamp;
        END;
      $function$
    LANGUAGE 'plpgsql' IMMUTABLE;
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION qbidt(ts timestamp)
        RETURNS text
    AS $function$
    DECLARE 
      var_result text;
    BEGIN
      var_result :=  to_char(ts, 'YYYY-MM-DD"T"HH24:MI:SS.MS"+00:00"');
    RETURN var_result;
    END;
    $function$
    LANGUAGE 'plpgsql' IMMUTABLE;
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION qbi_adjusted_ajs(jobid int, status_id int)
      RETURNS text
      AS $function$
        BEGIN
          RETURN qbidt(adjusted_ajs(jobid,status_id));
        END;
      $function$
    LANGUAGE 'plpgsql' IMMUTABLE;
    SQL

    execute <<-SQL
    CREATE OR REPLACE VIEW public.list_sent_invoices AS
     SELECT invs.id,
        invs.sender_id
       FROM invoicing_ledger_items invs
      WHERE invs.type::text = 'Invoice'::text 
      AND invs.deleted_at IS NULL
      /* PDW: If you mofiy this, also modify clause in list_sent_payments */
      AND (invs.state IN ('partner_sent','paid','on_site_payment','fleet_approved','fleet_downloaded','swoop_approved_partner','swoop_downloaded_partner') OR (invs.paid IS TRUE) )
      AND (invs.qb_imported_at IS NULL or invs.qb_pending_import is true);
    SQL

    execute <<-SQL
    COMMENT ON VIEW public.list_sent_invoices
        IS 'Returns ''invoice_id'' and ''user_id'' for all un-imported Invoices in the ''sent'' stage or beyond';-- View: public.list_sent_payments
    SQL

    execute <<-SQL
    CREATE OR REPLACE VIEW public.list_sent_payments AS
     SELECT pays.id,
        pays.sender_id
       FROM invoicing_ledger_items pays
         JOIN invoicing_ledger_items as invoice on pays.invoice_id=invoice.id
      WHERE
      (pays.type::text = 'Payment'::text)
    /* PDW: These are failing in QBI because it's not supported yet (12/6/17), so disabling in API
       (pays.type::text = 'Payment'::text OR pays.type::text = 'WriteOff'::text  OR pays.type::text = 'Discount'::text) */     
      AND pays.deleted_at IS NULL 
      AND (pays.qb_imported_at IS NULL or pays.qb_pending_import is true)
      AND invoice.deleted_at is null
      AND (invoice.state IN ('partner_sent','paid','on_site_payment','fleet_approved','fleet_downloaded','swoop_approved_partner','swoop_downloaded_partner') OR (invoice.paid IS TRUE))
    SQL

    execute <<-SQL
    COMMENT ON VIEW public.list_sent_payments
        IS 'Returns ''invoice_id'' and ''user_id'' for all ''sent'' Payments';
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION public.get_accounts(
    	uuid)
        RETURNS json
        LANGUAGE 'sql'
        COST 100.0
        VOLATILE NOT LEAKPROOF STRICT 
    AS $function$
     SELECT COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]')  AS json FROM ( 
        (
            SELECT
    	    MAX(accts.id) AS id,
    	    MAX(accts.name) AS name,
    	    MAX(accts.accounting_email) AS email,
    	    MAX(accts.phone) AS phone,
    	    MAX(locs.state) AS state,
    	    MAX(locs.city) AS city,
    	    MAX(locs.street) AS street,
    	    MAX(locs.zip) AS zip,
    	    MAX(locs.country) AS country
    	FROM invoicing_ledger_items invs
         JOIN accounts accts ON accts.id = invs.recipient_id AND invs.recipient_type::text = 'Account'::text AND (disable_qb_import is null or disable_qb_import is false)
         LEFT JOIN locations locs ON accts.location_id = locs.id
    	WHERE (
    	    invs.id IN ( 
    		SELECT "list_sent_invoices".id
    		FROM "list_sent_invoices"
    		WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)
    	    )    
    	)
    	GROUP BY (name)
         UNION
    /* A rather complex way for getting the cash call account ... */
            SELECT
    	    MAX(accts.id) AS id,
    	    MAX(accts.name) AS name,
    	    MAX(accts.accounting_email) AS email,
    	    MAX(accts.phone) AS phone,
    	    MAX(locs.state) AS state,
    	    MAX(locs.city) AS city,
    	    MAX(locs.street) AS street,
    	    MAX(locs.zip) AS zip,
    	    MAX(locs.country) AS country
            FROM invoicing_ledger_items invs
              JOIN users ON users.id = invs.recipient_id AND invs.recipient_type::text = 'User'::text
              JOIN accounts accts ON accts.company_id = invs.sender_id AND accts.name::text = 'Cash Call'::text AND invs.recipient_type::text = 'User'::text
              LEFT JOIN locations locs ON accts.location_id = locs.id
    	WHERE (
    	    invs.id IN ( 
    		SELECT "list_sent_invoices".id
    		FROM "list_sent_invoices"
    		WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)
    	    )
    	)
    	GROUP BY (name)
    )
        ORDER BY id
    ) t
    $function$;
    SQL

    execute <<-SQL
    COMMENT ON FUNCTION public.get_accounts(uuid)
        IS 'Returns all Accounts/Fleets for this User''s ''sent'' Invoices';
    SQL

    execute <<-SQL
    -- PDW: Changing this also needs a change to get_payments (copy and paste, replace list_sent_invoices & add payment_method)
    CREATE OR REPLACE FUNCTION public.get_invoice_headers(
    	uuid)
        RETURNS json
        LANGUAGE 'sql'
        COST 100.0
        VOLATILE NOT LEAKPROOF STRICT 
    AS $function$
       SELECT COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]')  AS json
       FROM (SELECT distinct on (invs.id)
                invs.id,
                concat('SW',invs.job_id) as job_id,
                invs.issue_date,
                invs.total_amount,
                invs.state,
                invs.identifier,
                invs.description,
                invs.invoice_notes, 
                invs.due_date,
                case when (jobs.status='Completed') then
                    qbi_adjusted_ajs(jobs.id,7)
                  else
                    case when (jobs.status='GOA') then
                      qbi_adjusted_ajs(jobs.id,21)
                    else
                      case when (jobs.status='Released') then
                        qbi_adjusted_ajs(jobs.id,18)
                      else
                        case when (jobs.status='Canceled') then
                          qbi_adjusted_ajs(jobs.id,8)
                        else
                          qbidt(utc_now())
                        end
                      end
                    end
                  end
                 AS created_at,
                accts.name AS account_name,
                accts.accounting_email AS email,
                accts.phone,
                locs.state,
                locs.city,
                locs.street,
                locs.zip,
                locs.country,
                btrim(concat(users.first_name, ' ', users.last_name))::character varying as customer_name,
                users.phone  as customer_phone,
                jobs.po_number
             FROM invoicing_ledger_items invs
                 LEFT JOIN jobs ON invs.id = jobs.invoice_id
                 LEFT JOIN settings on invs.sender_id=settings.company_id AND (settings.key='QB Ignore Canceled Invoices')
                 LEFT JOIN drives on jobs.driver_id=drives.id
                 LEFT JOIN users ON users.id=drives.user_id
                 JOIN accounts accts ON accts.id = invs.recipient_id AND invs.recipient_type::text = 'Account'::text AND (disable_qb_import is null or disable_qb_import is false)
                 LEFT JOIN locations locs ON accts.location_id = locs.id
             WHERE (invs.id IN ( SELECT "list_sent_invoices".id
                       FROM "list_sent_invoices"
                       WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)
              )
              )
             AND invs.deleted_at is null
             AND (settings.value is null or
                  (settings.key='QB Ignore Canceled Invoices' AND jobs.status!='Canceled'))
             AND invs.total_amount > 0
             UNION
             /* joins invoice with users instead for cash calls */
             SELECT distinct on (invs.id)
                invs.id,
                concat('SW',invs.job_id) as job_id,
                invs.issue_date,
                invs.total_amount,
                invs.state,
                invs.identifier,
                invs.description,
                invs.invoice_notes, 
                invs.due_date,
                case when (jobs.status='Completed') then
                    qbi_adjusted_ajs(jobs.id,7)
                  else
                    case when (jobs.status='GOA') then
                      qbi_adjusted_ajs(jobs.id,21)
                    else
                      case when (jobs.status='Released') then
                        qbi_adjusted_ajs(jobs.id,18)
                      else
                        case when (jobs.status='Canceled') then
                          qbi_adjusted_ajs(jobs.id,8)
                        else
                          qbidt(utc_now())
                        end
                      end
                    end
                  end
                 AS created_at,
                accts.name  AS account_name,
                accts.accounting_email AS email,
                accts.phone,
                locs.state,
                locs.city,
                locs.street,
                locs.zip,
                locs.country,
                btrim(concat(users.first_name, ' ', users.last_name))::character varying AS customer_name,
                users.phone as customer_phone,
                jobs.po_number
             FROM invoicing_ledger_items invs
                 LEFT JOIN jobs ON invs.id = jobs.invoice_id
                 LEFT JOIN settings on invs.sender_id=settings.company_id and (settings.key='QB Ignore Canceled Invoices')
                 JOIN users ON users.id = invs.recipient_id AND invs.recipient_type::text = 'User'::text
                 JOIN accounts accts ON accts.company_id = invs.sender_id AND accts.name::text = 'Cash Call'::text AND invs.recipient_type::text = 'User'::text
                 LEFT JOIN locations locs ON accts.location_id = locs.id
             WHERE (invs.id IN ( SELECT "list_sent_invoices".id
                       FROM "list_sent_invoices"
                       WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)
              )
             )
             AND invs.deleted_at is null
             AND (settings.value is null or
                  (settings.key='QB Ignore Canceled Invoices' AND jobs.status!='Canceled'))
             AND invs.total_amount > 0
    ) t
    $function$;
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION public.get_invoice_lines(
    	uuid)
        RETURNS json
        LANGUAGE 'sql'
        COST 100.0
        VOLATILE NOT LEAKPROOF STRICT 
    AS $function$
       SELECT COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]')  AS json
       FROM ( SELECT lines.ledger_item_id AS invoice_id,
                lines.type,
                lines.net_amount,
                lines.tax_amount,
                lines.description AS item_name,
                lines.quantity,
                lines.notes AS item_descr
               FROM invoicing_line_items lines
              WHERE (lines.ledger_item_id IN ( SELECT "list_sent_invoices".id
                       FROM "list_sent_invoices"
                       WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)))
              AND NOT (description = 'Tax' AND tax_amount = 0)
              AND lines.deleted_at is null
              ORDER BY invoice_id, created_at, tax_amount) t
    $function$;
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION public.get_items(
    	uuid)
        RETURNS json
        LANGUAGE 'sql'
        COST 100.0
        VOLATILE NOT LEAKPROOF STRICT 
    AS $function$

       SELECT COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]')  AS json
       FROM ( SELECT DISTINCT 
                lines.description AS item_name
              FROM invoicing_line_items lines
              WHERE (lines.ledger_item_id IN ( SELECT "list_sent_invoices".id
                       FROM "list_sent_invoices"
                       WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)))
    		  GROUP BY item_name
        ) t
    $function$;
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION public.get_payments(
    	uuid)
        RETURNS json
        LANGUAGE 'sql'
        COST 100.0
        VOLATILE NOT LEAKPROOF STRICT 
    AS $function$
       SELECT COALESCE(array_to_json(array_agg(row_to_json(t.*))), '[]')  AS json
       FROM (SELECT distinct on (invs.id)
                invs.id,
                concat('SW',invs.job_id) as job_id,
                invs.issue_date,
                -invs.total_amount as "total_amount",
                invs.state,
                invs.identifier,
                invs.description,
                invs.invoice_notes, 
                invs.due_date,
                to_char(invs.mark_paid_at, 'YYYY-MM-DD"T"HH24:MI:SS.MS"+00:00"') as created_at,
                case when invs.payment_method='ACH' then 'E-Check' else invs.payment_method end as payment_method,
                accts.name AS account_name,
                accts.accounting_email AS email,
                accts.phone,
                locs.state,
                locs.city,
                locs.street,
                locs.zip,
                locs.country,
                btrim(concat(users.first_name, ' ', users.last_name))::character varying as customer_name,
                users.phone  as customer_phone,
                jobs.po_number
             FROM invoicing_ledger_items invs
                 LEFT JOIN jobs ON invs.id = jobs.invoice_id
                 LEFT JOIN settings on invs.sender_id=settings.company_id and (settings.key='QB Sync payments')
                 LEFT JOIN drives on jobs.driver_id=drives.id
                 LEFT JOIN users ON users.id=drives.user_id
                 JOIN accounts accts ON accts.id = invs.recipient_id AND invs.recipient_type::text = 'Account'::text AND (disable_qb_import is null or disable_qb_import is false)
                 LEFT JOIN locations locs ON accts.location_id = locs.id
             WHERE (invs.id IN ( SELECT "list_sent_payments".id
                       FROM "list_sent_payments"
                       WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)
              )
              )
             AND invs.deleted_at is null
             AND (settings.key='QB Sync payments') /* PDW this approach wont work when combined with other clauses would need to move this join to 'sync' alias and create another settings */
             AND -invs.total_amount > 0

           UNION
           /* joins invoice with users instead for cash calls */
             SELECT distinct on (invs.id)
                invs.id,
                concat('SW',invs.job_id) as job_id,
                invs.issue_date,
                -invs.total_amount as "total_amount",
                invs.state,
                invs.identifier,
                invs.description,
                invs.invoice_notes, 
                invs.due_date,
                to_char(invs.mark_paid_at, 'YYYY-MM-DD"T"HH24:MI:SS.MS"+00:00"') as created_at,
                case when invs.payment_method='ACH' then 'E-Check' else invs.payment_method end as payment_method,
                accts.name  AS account_name,
                accts.accounting_email AS email,
                accts.phone,
                locs.state,
                locs.city,
                locs.street,
                locs.zip,
                locs.country,
                btrim(concat(users.first_name, ' ', users.last_name))::character varying AS customer_name,
                users.phone as customer_phone,
                jobs.po_number
             FROM invoicing_ledger_items invs
                 LEFT JOIN jobs ON invs.id = jobs.invoice_id
                 LEFT JOIN settings on invs.sender_id=settings.company_id and (settings.key='QB Sync payments')
                 JOIN users ON users.id = invs.recipient_id AND invs.recipient_type::text = 'User'::text
                 JOIN accounts accts ON accts.company_id = invs.sender_id AND accts.name::text = 'Cash Call'::text AND invs.recipient_type::text = 'User'::text
                 LEFT JOIN locations locs ON accts.location_id = locs.id
             WHERE (invs.id IN ( SELECT "list_sent_payments".id
                       FROM "list_sent_payments"
                       WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)
              )
             )
             AND invs.deleted_at is null
             AND (settings.key='QB Sync payments')
             AND -invs.total_amount > 0
    ) t
    $function$;
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION public.get_user(
    	uuid)
        RETURNS json
        LANGUAGE 'sql'
        COST 100.0
        VOLATILE NOT LEAKPROOF STRICT 
    AS $function$
       SELECT COALESCE(row_to_json(t.*), '{}')  AS json
       FROM ( SELECT 
                name AS first_name, 
                business AS username, 
                phone
              FROM companies cos
              INNER JOIN qb_company_tokens qcts ON cos.id = qcts.company_id
              WHERE (qcts.token = $1)) t
    $function$;
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION public.mark_imported(
    	uuid,
    	integer)
        RETURNS void
        LANGUAGE 'sql'
        COST 100.0
        VOLATILE NOT LEAKPROOF 
    AS $function$
    UPDATE invoicing_ledger_items
    SET qb_imported_at = NOW(), qb_pending_import=null
    WHERE id = $2
    AND sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)
    $function$;
    SQL

    execute <<-SQL
    CREATE OR REPLACE FUNCTION public.set_qb_imported_at(
    	uuid,
    	timestamp with time zone)
        RETURNS void
        LANGUAGE 'sql'
        COST 100.0
        VOLATILE NOT LEAKPROOF 
    AS $function$
      UPDATE invoicing_ledger_items AS invs
      	SET qb_imported_at = $2,  qb_pending_import=null
    	WHERE sender_id = (SELECT company_id FROM qb_company_tokens WHERE token = $1)
    $function$;
    SQL
  end
end
