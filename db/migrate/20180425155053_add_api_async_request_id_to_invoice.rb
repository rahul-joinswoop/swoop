class AddAPIAsyncRequestIdToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_reference :invoicing_ledger_items, :api_async_request, foreign_key: true
  end
end
