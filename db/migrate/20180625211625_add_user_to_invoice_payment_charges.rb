class AddUserToInvoicePaymentCharges < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_payment_charges, :charge_user_id, :integer, index: true
    add_column :invoice_payment_charges, :refund_user_id, :integer, index: true

    add_foreign_key :invoice_payment_charges, :users, column: :charge_user_id
    add_foreign_key :invoice_payment_charges, :users, column: :refund_user_id
  end
end
