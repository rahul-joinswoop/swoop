class AddCustomersTable < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.references :company, foreign_key: true
      t.string :member_number, index: true
      t.timestamps
      t.index [:company_id, :member_number], unique: true
    end
    add_reference :users, :customer, foreign_key: true
  end
end
