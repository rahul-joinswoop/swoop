class AddDeletedAtAndCompanyIdToTags < ActiveRecord::Migration[5.0]
  def change
    add_reference :tags, :company, foreign_key: true, column: :company_id
    add_column :tags, :deleted_at, :datetime
  end
end
