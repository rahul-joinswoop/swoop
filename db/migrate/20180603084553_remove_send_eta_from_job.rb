class RemoveSendEtaFromJob < ActiveRecord::Migration[5.0]
  def change
    remove_column :jobs, :send_eta, :boolean
  end
end
