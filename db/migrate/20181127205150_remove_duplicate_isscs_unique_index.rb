class RemoveDuplicateIsscsUniqueIndex < ActiveRecord::Migration[5.1]

  disable_ddl_transaction!

  def change
    remove_index :isscs, name: :isscs_unique_index, algorithm: :concurrently
  end

end
