class CreatePCCCoverage < ActiveRecord::Migration[5.0]
  def change
    create_table :pcc_coverages do |t|
      t.references :pcc_vehicle
      t.string  :coverage_number
      t.boolean :covered
      t.boolean :exceeded_service_count_limit
      t.json :data

      t.timestamps
    end
  end
end
