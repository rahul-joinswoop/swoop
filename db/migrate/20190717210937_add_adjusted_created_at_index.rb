class AddAdjustedCreatedAtIndex < ActiveRecord::Migration[5.2]

  # DDL transactions need to be disabled in order to create an index concurrently
  disable_ddl_transaction!

  def up
    add_index :jobs, [:adjusted_created_at], algorithm: :concurrently
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:jobs, [:adjusted_created_at])
      remove_index :jobs, [:adjusted_created_at]
    end
    raise e
  end

  def down
    remove_index :jobs, [:adjusted_created_at]
  end

end
