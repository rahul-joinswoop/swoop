class CreateJobBillingInfos < ActiveRecord::Migration[5.1]

  def change
    create_table :job_billing_infos do |t|
      t.references :job, null: false, index: :unique
      t.bigint :rescue_company_id, null: false
      t.bigint :agent_id, null: false
      t.string :billing_type, null: false, limit: 20
      t.decimal :vcc_amount, null: true, precision: 20, scale: 4
      t.timestamps
    end
  end

end
