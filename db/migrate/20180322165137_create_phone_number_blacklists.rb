class CreatePhoneNumberBlacklists < ActiveRecord::Migration[5.0]
  def change
    create_table :phone_number_blacklists do |t|
      t.string :phone_number, null: false
      t.timestamps
    end
    add_index :phone_number_blacklists, :phone_number, unique: true
  end
end
