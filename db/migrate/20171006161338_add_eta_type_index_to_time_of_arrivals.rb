class AddEtaTypeIndexToTimeOfArrivals < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :time_of_arrivals, :eba_type, algorithm: :concurrently
  end
end
