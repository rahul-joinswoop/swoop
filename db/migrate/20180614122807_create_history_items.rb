class CreateHistoryItems < ActiveRecord::Migration[5.0]
  def change
    create_table :history_items do |t|
      t.string :title
      t.string :type
      t.references :company, foreign_key: true
      t.references :user, foreign_key: true
      t.references :job, foreign_key: true
      t.references :target, polymorphic: true
      t.timestamp :adjusted_created_at

      t.timestamps
    end
  end
end
