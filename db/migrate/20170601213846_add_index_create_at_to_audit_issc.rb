class AddIndexCreateAtToAuditIssc < ActiveRecord::Migration[4.2]
  def change
    add_index :audit_isscs, :created_at
  end
end

