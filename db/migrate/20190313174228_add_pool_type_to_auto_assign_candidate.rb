class AddPoolTypeToAutoAssignCandidate < ActiveRecord::Migration[5.1]
  def up
    # Naming the type as `job_candidate_pool_type` keeping in mind that in near future,
    # we will rename `auto_assign_candidates` table to `job_candidates`
    execute <<-DDL
          CREATE TYPE job_candidate_pool_type AS ENUM (
            'auto_assign',
            'manual_assign_with_swoop_account',
            'manual_assign_oon_preferred',
            'manual_assign_without_oon_preferred_without_swoop_account'
          );
    DDL

    add_column :auto_assign_candidates, :pool_type, :job_candidate_pool_type, null: true
  end

  def down
    remove_column :auto_assign_candidates, :pool_type
    execute "DROP TYPE job_candidate_pool_type"
  end
end
