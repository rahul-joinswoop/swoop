class CreateClientPrograms < ActiveRecord::Migration[5.1]

  def change
    create_table :client_programs do |t|
      t.string :name
      t.string :code
      t.string :client_identifier

      t.timestamps
    end
    add_index :client_programs, :code
  end

end
