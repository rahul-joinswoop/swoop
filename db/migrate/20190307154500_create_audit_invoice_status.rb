class CreateAuditInvoiceStatus < ActiveRecord::Migration[5.1]
  def up
    execute <<-DDL
          CREATE TYPE invoice_status AS ENUM (
            'created',
            'partner_new',
            'partner_approved',
            'partner_sent',
            'fleet_approved',
            'fleet_downloaded',
            'fleet_rejected',
            'on_site_payment',
            'partner_offline_new',
            'swoop_new',
            'swoop_approved_fleet',
            'fleet_customer_new',
            'swoop_sending_fleet',
            'swoop_sent_fleet',
            'swoop_deleted_fleet',
            'swoop_approved_partner',
            'swoop_downloading_partner',
            'swoop_downloaded_partner',
            'swoop_rejected_partner'
          );
    DDL

    create_table :audit_invoice_statuses do |t|
      t.column :from_invoice_status, :invoice_status, index: true
      t.column :to_invoice_status, :invoice_status, index: true, null: false
      t.integer :invoice_id, index: true, null: false
      t.datetime :transition_happened_at, null: false
    end

    add_foreign_key :audit_invoice_statuses, :invoicing_ledger_items, column: :invoice_id
  end

  def down
    drop_table :audit_invoice_statuses
    remove_foreign_key :audit_invoice_statuses, column: :invoice_id

    execute 'DROP TYPE invoice_status;'
  end
end
