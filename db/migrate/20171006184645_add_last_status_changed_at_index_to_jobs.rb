class AddLastStatusChangedAtIndexToJobs < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :jobs, :last_status_changed_at, algorithm: :concurrently
  end
end
