class CreateReferer < ActiveRecord::Migration[4.2]
  def change
    create_table :referers do |t|
      t.string :name, null: false
      t.timestamps null: false
    end

    add_index :referers, :name, unique: true
  end
end
