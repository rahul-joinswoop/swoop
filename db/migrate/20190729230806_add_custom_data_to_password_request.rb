# frozen_string_literal: true

class AddCustomDataToPasswordRequest < ActiveRecord::Migration[5.2]

  def change
    add_column :password_requests, :custom_data, :json, default: {}
  end

end
