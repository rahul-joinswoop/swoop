class AddInvoiceBillingType < ActiveRecord::Migration[5.1]

  def change
    add_column :invoicing_ledger_items, :billing_type, :string, limit: 20, null: true
  end

end
