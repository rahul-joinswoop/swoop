class AddUniqueIndexToCompanyIdOnCustomAccounts < ActiveRecord::Migration[4.2]
  def change
    remove_index :custom_accounts, :company_id
    add_index :custom_accounts, :company_id, unique: true
  end
end
