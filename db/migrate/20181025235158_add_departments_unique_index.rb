class AddDepartmentsUniqueIndex < ActiveRecord::Migration[5.1]

  disable_ddl_transaction!

  def up
    transaction do
      rename_duplicate_departments!
    end
    add_index :departments, [:company_id, :name], unique: true, algorithm: :concurrently, where: "deleted_at IS NULL"
    remove_index :departments, :company_id
  end

  def down
    add_index :departments, :company_id, algorithm: :concurrently
    remove_index :departments, [:company_id, :name]
  end

  private

  def rename_duplicate_departments!
    select_all(
      <<~SQL
        SELECT * FROM (
          SELECT company_id, count(distinct departments.name) AS unique_count, count(*) AS count
          FROM departments
          JOIN companies ON departments.company_id = companies.id
          WHERE departments.deleted_at IS NULL AND companies.deleted_at IS NULL
          GROUP BY company_id
        ) AS tmp WHERE unique_count != count;
      SQL
    ).each do |row|
      dup_count = 1
      last_name = nil
      Department.where(company_id: row['company_id']).order(:name).each do |department|
        department.name = "[Unknown]" if department.name.blank?
        if department.name == last_name
          dup_count += 1
          department.name = "#{department.name} (#{dup_count})"
        else
          dup_count = 1
          last_name = department.name
        end
        if department.name_changed?
          # department.save!
          puts "changed #{site.company.name} department name: #{department.name}"
        end
      end
    end
  end

end
