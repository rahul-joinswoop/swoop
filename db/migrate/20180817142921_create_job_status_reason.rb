class CreateJobStatusReason < ActiveRecord::Migration[5.1]
  def change
    create_table :job_status_reasons do |t|
      t.references :job, foreign_key: true, null: false
      t.references :audit_job_status, foreign_key: true, null: false
      t.references :job_status_reason_type, foreign_key: true, null: false
      t.timestamps null: false
    end

    add_index(
      :job_status_reasons,
      [:job_id, :job_status_reason_type_id],
      unique: true,
      name: :index_job_status_reasons_on_job_id_and_job_status_reas_type_id
    )

    add_index :job_status_reasons, [:job_id, :audit_job_status_id], unique: true
  end
end
