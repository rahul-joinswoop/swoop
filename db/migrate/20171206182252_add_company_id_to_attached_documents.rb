class AddCompanyIdToAttachedDocuments < ActiveRecord::Migration[4.2]
  def change
    add_reference :attached_documents, :company, index: true, foreign_key: true
  end
end
