class AddAuctionToBid < ActiveRecord::Migration[4.2]
  def change
    add_reference :bids, :auction, index: true, foreign_key: true
  end
end
