class CreateBids < ActiveRecord::Migration[4.2]
  def change
    create_table :bids do |t|
#      t.comment "Join table between jobs and Rescue Companies representing the bid made (or about to be made)"
      t.references :auto_assign_assignment, index: true, foreign_key: true
      t.references :job, index: true, foreign_key: true
      t.references :company, index: true, foreign_key: true
      t.string :status, :comment => "[requested, expired, provider_rejected, auto_rejected, won]"
      t.integer :eta_mins, :comment => "The actual submitted value from the frontend (e.g. 30m)"
      t.datetime :bid_submission_dttm, :comment => "The time the bid was submitted"
      t.datetime :eta_dttm, :comment => "The bid_submission_dttm + eta_mins"
      t.references :job_reject_reason, index: true, foreign_key: true
      t.text :job_reject_reason_info

      t.timestamps null: false
    end
  end
end
