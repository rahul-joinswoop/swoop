class CreateZipCodeSubsetToTerritories < ActiveRecord::Migration[5.2]
  def change
    create_table :zip_code_subset_to_territories do |t|
      t.string :zip
      t.st_polygon :polygon_area

      t.timestamps
    end
  end
end
