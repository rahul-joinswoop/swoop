class NullTrueDataAndUrlInBoschJobs < ActiveRecord::Migration[4.2]
  def change
    change_column :bosch_jobs, :data, :text, null: true
    change_column :bosch_jobs, :url, :text, null: true
  end
end
