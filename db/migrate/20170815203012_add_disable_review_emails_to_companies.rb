class AddDisableReviewEmailsToCompanies < ActiveRecord::Migration[4.2]
  def change
    add_column :companies, :disable_review_emails, :boolean
  end
end
