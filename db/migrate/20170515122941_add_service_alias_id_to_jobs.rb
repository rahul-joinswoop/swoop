class AddServiceAliasIdToJobs < ActiveRecord::Migration[4.2]
  def change
    add_reference :jobs, :service_alias, index: true, foreign_key: true
  end
end
