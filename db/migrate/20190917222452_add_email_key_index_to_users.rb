# frozen_string_literal: true

class AddEmailKeyIndexToUsers < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    add_index :users, :email_key, algorithm: :concurrently, unique: true, where: "deleted_at IS NULL AND email_key IS NOT NULL"

  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:users, :email_key)
      down
    end
    raise e
  end

  def down
    remove_index :users, :email_key
  end

end
