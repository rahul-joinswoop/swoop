class AddLocationTypeIdToLocation < ActiveRecord::Migration[4.2]
  def change
    add_column :locations, :location_type_id, :integer
    add_foreign_key :locations, :location_types, column: :location_type_id
  end
end
