class AddChosenCompanyToAssignment < ActiveRecord::Migration[4.2]
  def change
    add_reference :auto_assign_assignments, :chosen_company, references: :companies, index: true
    add_foreign_key :auto_assign_assignments, :companies, column: :chosen_company_id
  end
end
