class AddUniqueUserFullNameIndexForCompany < ActiveRecord::Migration[5.1]

  def change
    # This migration has been aborted without reaching production and replaced with a more efficient index.
    # add_index :users, [:first_name, :last_name, :company_id], unique: true
  end

end
