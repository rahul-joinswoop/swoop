class AddServiceCommissionsExclusions < ActiveRecord::Migration[4.2]
  def change
    create_table :service_commissions_exclusions do |t|
      t.references :company, index: true, foreign_key: true
      t.references :service_code, index: true, foreign_key: true

      t.timestamps null: false
    end

    add_index :service_commissions_exclusions, [:company_id, :service_code_id], unique: true, name: :index_service_commissions_excl_on_company_id_and_service_id
  end
end
