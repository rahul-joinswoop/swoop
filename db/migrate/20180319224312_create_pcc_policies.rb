class CreatePCCPolicies < ActiveRecord::Migration[5.0]
  def change
    create_table :pcc_policies do |t|
      t.references :company
      t.string :policy_number
      t.string :full_name
      t.string :address
      t.json :data
      
      t.timestamps
    end
  end
end
