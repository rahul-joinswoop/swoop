class AddUserAgentAndPlatformToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :user_agent, :string
    add_column :users, :platform, :string
    add_index :users, :platform
  end
end
