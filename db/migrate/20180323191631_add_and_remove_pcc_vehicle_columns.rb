class AddAndRemovePCCVehicleColumns < ActiveRecord::Migration[5.0]
  def change
    remove_column :pcc_vehicles, :description
    add_column :pcc_vehicles, :make, :string
    add_column :pcc_vehicles, :model, :string
    add_column :pcc_vehicles, :year, :string
  end
end
