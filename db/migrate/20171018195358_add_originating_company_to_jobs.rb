class AddOriginatingCompanyToJobs < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_column :jobs, :originating_company_id, :integer, index: true, algorithm: :concurrently
    add_foreign_key :jobs, :companies, column: :originating_company_id
  end
end
