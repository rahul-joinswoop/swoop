class AddDetetedAtToAPIAccessTokens < ActiveRecord::Migration[5.1]
  def change
    add_column :api_access_tokens, :deleted_at, :datetime
    add_index :api_access_tokens, [:company_id, :vendorid], name: 'unique_index_to_avoid_duplicate_tokens', unique: true, where: "deleted_at is null"
  end
end
