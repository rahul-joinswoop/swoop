class AddColumnsToPCCCoverage < ActiveRecord::Migration[5.0]
  def change
    add_reference :pcc_coverages, :pcc_policy, index: true, foreign_key: true
    add_reference :pcc_coverages, :pcc_user, index: true, foreign_key: true
  end
end
