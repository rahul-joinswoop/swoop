class AddIndexToPoliciesUuid < ActiveRecord::Migration[5.1]

  def change
    add_index :pcc_policies, :uuid
    add_index :pcc_users, :uuid
    add_index :pcc_vehicles, :uuid
  end

end
