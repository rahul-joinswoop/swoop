class AddClientProgramsPriority < ActiveRecord::Migration[5.1]

  def change
    add_column :client_programs, :priority, :boolean, null: false, default: false
  end

end
