class DropTTtables < ActiveRecord::Migration[5.2]
  def up
    drop_table :tt_objects
    drop_table :tt_scrapes
    drop_table :tt_accounts
    drop_table :tt_messages
  end
end
