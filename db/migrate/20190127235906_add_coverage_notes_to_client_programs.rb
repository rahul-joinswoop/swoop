class AddCoverageNotesToClientPrograms < ActiveRecord::Migration[5.1]

  def change
    add_column :client_programs, :coverage_notes, :string
  end

end
