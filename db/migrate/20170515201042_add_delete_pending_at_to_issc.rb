class AddDeletePendingAtToIssc < ActiveRecord::Migration[4.2]
  def change
    add_column :isscs, :delete_pending_at, :datetime
  end
end
