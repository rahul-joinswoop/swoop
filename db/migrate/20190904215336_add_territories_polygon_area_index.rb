# frozen_string_literal: true

class AddTerritoriesPolygonAreaIndex < ActiveRecord::Migration[5.2]

  def up
    safety_assured { add_index :territories, [:polygon_area], using: :gist }
  end

  def down
    safety_assured { remove_index :territories, [:polygon_area] }
  end

end
