# frozen_string_literal: true

class IndexVehiclesName < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    # Name is required on RescueVehicle
    update("UPDATE vehicles SET name = 'Truck' WHERE company_id IS NOT NULL AND name IS NULL AND deleted_at IS NULL")

    # Make sure there are no duplicates
    last_name = nil
    num = 1
    transaction do
      select_all("SELECT DISTINCT v2.id, v2.company_id, v2.name FROM vehicles v1, vehicles v2 WHERE v1.company_id = v2.company_id AND v1.name = v2.name AND v1.id < v2.id AND v1.deleted_at IS NULL AND v2.deleted_at IS NULL order by v2.company_id, v2.name").each do |row|
        name = "#{row['company_id']}:#{row['name']}".downcase
        if name == last_name
          num += 1
        else
          num = 1
          last_name = name
        end
        update("UPDATE vehicles SET name = CONCAT(name, ' (#{num})') WHERE id = #{row['id']}")
      end
    end

    add_index :vehicles, [:company_id, :name, :type], unique: true, where: "company_id IS NOT NULL AND deleted_at IS NULL", algorithm: :concurrently

    # This index is now superfluous
    remove_index :vehicles, [:company_id]
  rescue => e
    # With DDL transactions disabled, we need to manually remove the index if something goes wrong
    if index_exists?(:vehicles, [:company_id, :name, :type])
      remove_index :vehicles, [:company_id, :name, :type]
    end
    raise e
  end

  def down
    add_index :vehicles, [:company_id], algorithm: :concurrently
    remove_index :vehicles, [:company_id, :name, :type]
  end

end
