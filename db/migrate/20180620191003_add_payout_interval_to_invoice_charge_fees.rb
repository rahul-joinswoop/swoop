class AddPayoutIntervalToInvoiceChargeFees < ActiveRecord::Migration[5.0]
  def change
    add_column :company_invoice_charge_fees, :payout_interval, :string
  end
end
