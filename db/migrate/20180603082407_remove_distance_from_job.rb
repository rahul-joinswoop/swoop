class RemoveDistanceFromJob < ActiveRecord::Migration[5.0]
  def change
    remove_column :jobs, :distance, :double
  end
end
