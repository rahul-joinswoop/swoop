class AddNumJobsToAutoAssignCandidate < ActiveRecord::Migration[5.0]
  def change
    add_column :auto_assign_candidates, :num_jobs, :integer
  end
end
