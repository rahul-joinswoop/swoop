class CreateLocationType < ActiveRecord::Migration[4.2]
  def change
    create_table :location_types do |t|
      t.string    :name, null: false
      t.timestamp :deleted_at

      t.timestamps null: false
    end

    add_index :location_types, :name, unique: true
  end
end
