class DropHiddenUsersTables < ActiveRecord::Migration[5.0]
  def change
    drop_table :hidden_users_dispatchers
    drop_table :hidden_users_sites
    drop_table :hidden_users_storage_sites
    drop_table :hidden_users_storage_types
  end
end
