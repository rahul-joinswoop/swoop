class OptimizeVehiclesCompanyIndex < ActiveRecord::Migration[5.1]

  disable_ddl_transaction!

  def up
    add_index :vehicles, :company_id, algorithm: :concurrently
    remove_index :vehicles, name: "index_vehicles_on_company_type_and_company_id", algorithm: :concurrently
  end

  def down
    add_index :vehicles, [:company_type, :company_id], algorithm: :concurrently
    remove_index :vehicles, name: "index_vehicles_on_company_id", algorithm: :concurrently
  end

end
