# frozen_string_literal: true

class UpdateStorageInvoices < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    # find all storage jobs > 90 days old which are auto_calculating invoices and disable auto_calculated_quantity. this is
    # a one-time thing - from here out we'll something similar on a daily basis for jobs exactly 90 days old so that if a
    # company manually switches auto_calculated_quantity back on it'll stay
    InvoicingLineItem
      .active_storage
      .where(auto_calculated_quantity: true)
      .where(Job.arel_table[:created_at].lt(90.days.ago))
      .in_batches(of: 100) do |items|
        items.each do |item|
          # disable auto_calculating
          item.update! auto_calculated_quantity: false
        end
        sleep(0.1) # throttle
      end
  end

end
