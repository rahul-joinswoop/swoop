class CreateUserVisits < ActiveRecord::Migration[5.2]

  def change
    create_table :user_visits do |t|
      t.bigint :user_id, null: false, index: true
      t.date :visit_date, null: false
      t.integer :source_application_id, null: false
      t.integer :session_count, null: false, default: 0, limit: 1
      t.index [:visit_date, :source_application_id, :user_id], name: :index_user_visits_unique, unique: true
    end

    add_foreign_key :user_visits, :users
    add_foreign_key :user_visits, :source_applications
  end

end
