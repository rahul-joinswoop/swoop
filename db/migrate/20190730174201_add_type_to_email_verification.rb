# frozen_string_literal: true

class AddTypeToEmailVerification < ActiveRecord::Migration[5.2]

  def up
    add_column :password_requests, :type, :string
    PasswordRequest.reset_column_information
    PasswordRequest.update_all(type: 'PasswordRequest')
  end

  def down
    remove_column :password_requests, :type
  end

end
