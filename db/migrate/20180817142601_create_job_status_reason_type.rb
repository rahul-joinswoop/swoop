class CreateJobStatusReasonType < ActiveRecord::Migration[5.1]
  def change
    create_table :job_status_reason_types do |t|
      t.references :job_status, foreign_key: true, null: false
      t.string :text, null: false
      t.string :key, null: false
      t.timestamps null: false
    end

    add_index :job_status_reason_types, [:job_status_id, :text], unique: true
    add_index :job_status_reason_types, :key, unique: true
  end
end
