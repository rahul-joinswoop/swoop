class AddJobIdToPCCPolicy < ActiveRecord::Migration[5.0]
  def change
    add_reference :pcc_policies, :job, foreign_key: true, null: false
  end
end
