class CreateClientProgramServiceNotes < ActiveRecord::Migration[5.2]
  def up
    create_table :client_program_service_notes do |t|
      t.string :notes, null: false
      t.references :client_program, foreign_key: true, index: true, null: false
      t.references :service_code, foreign_key: true, index: false, null: false

      t.index [:client_program_id, :service_code_id], unique: true, name: :index_coverage_notes_on_client_program_and_service_code
    end
  end

  def down
    drop_table :client_program_service_notes
  end
end
