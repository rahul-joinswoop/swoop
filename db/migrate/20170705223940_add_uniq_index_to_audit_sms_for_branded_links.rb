class AddUniqIndexToAuditSmsForBrandedLinks < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    add_index :audit_sms, [:job_id, :type], name: "index_audit_sms_on_job_id_and_branded_type", unique: true, algorithm: :concurrently, where: "type = 'Sms::BrandedReviewLink'"
  end
end
