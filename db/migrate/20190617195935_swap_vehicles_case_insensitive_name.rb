# frozen_string_literal: true

class SwapVehiclesCaseInsensitiveName < ActiveRecord::Migration[5.2]

  def up
    execute "UPDATE vehicles SET ci_name = name WHERE name IS NOT NULL"
    rename_column :vehicles, :name, :_x_name
    rename_column :vehicles, :ci_name, :name
  end

  def down
    execute "UPDATE vehicles SET _x_name = name WHERE name IS NOT NULL"
    rename_column :vehicles, :name, :ci_name
    rename_column :vehicles, :_x_name, :name
  end

end
