# frozen_string_literal: true

class PopulateLocationsGeoValue < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
    # disabled for production, will run this by hand!
    # Location.unscoped.in_batches(of: 100) do |relation|
    #   relation.update_all("lnglat=ST_Point(lng, lat)::geography")
    #   sleep(0.1)
    # end
  end

end
