class AddIsStandardToJobStatusReasonTypes < ActiveRecord::Migration[5.1]

  def change
    add_column :job_status_reason_types, :is_standard, :boolean
  end

end
