class CreateJobStatusEmailPreferences < ActiveRecord::Migration[4.2]
  def change
    create_table :job_status_email_preferences do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :job_status, index: true, foreign_key: true, null: false
      t.timestamps null: false
    end

    add_index :job_status_email_preferences, [:user_id, :job_status_id], unique: true
  end
end
