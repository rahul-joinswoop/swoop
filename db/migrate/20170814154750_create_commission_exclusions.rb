class CreateCommissionExclusions < ActiveRecord::Migration[4.2]
  def change
    create_table :commission_exclusions do |t|
      t.references :company, index: true, foreign_key: true
      t.string :item

      t.timestamps null: false
    end
  end
end
