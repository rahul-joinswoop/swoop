class AddPrecisionAndScaleToDecimalInInvoicePayments < ActiveRecord::Migration[5.2]
  def up
    change_column :invoice_payment_charges, :fee, :decimal, precision: 20, scale: 4
  end

  def down
    change_column :invoice_payment_charges, :fee, :decimal
  end
end
