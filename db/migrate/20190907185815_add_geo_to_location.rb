# frozen_string_literal: true

class AddGeoToLocation < ActiveRecord::Migration[5.2]

  def change
    add_column :locations, :lnglat, :st_point, geographic: true
  end

end
