class RemovePartnerE2E < ActiveRecord::Migration[5.1]
  def change
    remove_column :jobs, :partner_e2e, :boolean
  end
end
