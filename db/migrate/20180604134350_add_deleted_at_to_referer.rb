class AddDeletedAtToReferer < ActiveRecord::Migration[5.0]
  def change
    add_column :referers, :deleted_at, :datetime
  end
end
