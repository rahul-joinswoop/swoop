class AddCodeCompanyUniqueIndexToClientProgram < ActiveRecord::Migration[5.1]

  def change
    add_index :client_programs, [:code, :company_id], unique: true
  end

end
