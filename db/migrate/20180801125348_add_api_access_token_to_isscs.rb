class AddAPIAccessTokenToIsscs < ActiveRecord::Migration[5.0]
  def change
    add_reference :isscs, :api_access_token, foreign_key: true
  end
end
