class EnableTablefunc < ActiveRecord::Migration[5.1]
  def up
    # This extension is needed by some reports.
    unless select_one("SELECT extname FROM pg_extension WHERE extname = 'tablefunc'")
      execute("CREATE extension tablefunc")
    end
  end
end
