class RenamePCCUserToPCCDriverOnPCCCoverage < ActiveRecord::Migration[5.0]
  def change
    rename_column :pcc_coverages, :pcc_user_id, :pcc_driver_id
  end
end
