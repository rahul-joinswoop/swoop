class RemoveResidenceFromJob < ActiveRecord::Migration[5.0]
  def change
    remove_column :jobs, :residence, :boolean
  end
end
