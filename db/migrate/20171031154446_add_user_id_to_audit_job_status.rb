class AddUserIdToAuditJobStatus < ActiveRecord::Migration[4.2]
  def change
    add_reference :audit_job_statuses, :user, index: true, foreign_key: true
  end
end
