class AddLastLoginAttemptedAtToIssc < ActiveRecord::Migration[5.1]
  def change
    add_column :isscs, :last_login_attempted_at, :datetime
  end
end
