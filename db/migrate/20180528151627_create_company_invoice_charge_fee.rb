class CreateCompanyInvoiceChargeFee < ActiveRecord::Migration[5.0]
  def change
    create_table :company_invoice_charge_fees do |t|
      t.references :company, null: false
      t.decimal :percentage, null: false
      t.decimal :fixed_value, null: false
      t.timestamps
    end

    remove_index :company_invoice_charge_fees, :company_id
    add_index :company_invoice_charge_fees, :company_id, unique: true
  end
end
