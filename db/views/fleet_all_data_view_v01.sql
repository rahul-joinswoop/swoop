SELECT
  jobs.id AS "Job ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles', 'MM/DD/YYYY') AS "Date (Pacific)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles', 'HH24:MI') AS "Time (Pacific)",
  partner.name AS "Partner",
  CONCAT(customer.first_name, ' ', customer.last_name) AS "Customer",
  customer.phone AS "Phone",
  pickup_location_type.name AS "Location Type",
  vehicles.make AS "Make",
  vehicles.model AS "Model",
  pickup.address AS "pickup location",
  pickup.zip,
  CASE WHEN (pickup.city IS NOT NULL) THEN
    pickup.city
  ELSE
    (
      CASE WHEN original_pickup.city IS NOT NULL THEN
        original_pickup.city
      ELSE
        (provider_location.city)
      END)
  END AS "City",
  CASE WHEN (pickup.state IS NOT NULL) THEN
    pickup.state
  ELSE
    (
      CASE WHEN original_pickup.state IS NOT NULL THEN
        original_pickup.state
      ELSE
        (provider_location.state)
      END)
  END AS "State",
  CASE WHEN (pickup.zip IS NOT NULL) THEN
    pickup.zip
  ELSE
    (
      CASE WHEN original_pickup.zip IS NOT NULL THEN
        original_pickup.zip
      ELSE
        provider_location.zip
      END)
  END AS "Zip",
  CONCAT(pickup.lat, ',', pickup.lng) AS "Pickup",
  CONCAT(dropoff.lat, ',', dropoff.lng) AS "Dropoff",
  service_codes.name AS "Service",
  jobs.scheduled_for AS "Scheduled",
  round(estimates.meters_ab * 0.000621371, 1) AS "EN Route Miles",
  round(estimates.meters_bc * 0.000621371, 1) AS "Pick Up To Drop Off",
  round((
    CASE WHEN estimates.drop_location_id IS NOT NULL THEN
      estimates.meters_ca
    ELSE
      estimates.meters_ba
    END) * 0.000621371, 1) AS "Return Miles",
  round((estimates.meters_ab + (
      CASE WHEN estimates.drop_location_id IS NOT NULL THEN
        estimates.meters_bc + estimates.meters_ca
      ELSE
        estimates.meters_ba
      END)) * 0.000621371, 1) AS "P2P Miles",
  round(cast((Extract(EPOCH FROM (
          SELECT
            (time - created_at)
          FROM time_of_arrivals
        WHERE
          time_of_arrivals.job_id = jobs.id
          AND time_of_arrivals.eba_type IN (0, 3, 4)
        ORDER BY
          id ASC
        LIMIT 1)) / 60) AS numeric), 0) AS "ETA",
  round(cast((Extract(EPOCH FROM (
          SELECT
            CASE WHEN adjusted_dttm IS NOT NULL THEN
              adjusted_dttm
            ELSE
              last_set_dttm
            END FROM (
              SELECT
                last_set_dttm, adjusted_dttm FROM audit_job_statuses
              WHERE
                job_status_id = 4
                AND job_id = jobs.id
              ORDER BY
                id DESC
              LIMIT 1) AS onsite_ajs) - (
        SELECT
          created_at FROM time_of_arrivals
        WHERE
          time_of_arrivals.job_id = jobs.id
          AND time_of_arrivals.eba_type IN (0, 3, 4)
      ORDER BY
        id ASC
      LIMIT 1)) / 60) AS numeric), 0) AS "ATA",
  round(cast((Extract(EPOCH FROM (
          SELECT
            CASE WHEN adjusted_dttm IS NOT NULL THEN
              adjusted_dttm
            ELSE
              last_set_dttm
            END FROM (
              SELECT
                last_set_dttm, adjusted_dttm FROM audit_job_statuses
              WHERE
                job_status_id = 4
                AND job_id = jobs.id
              ORDER BY
                id DESC
              LIMIT 1) AS onsite_ajs) - (
        SELECT
          created_at FROM time_of_arrivals
        WHERE
          time_of_arrivals.job_id = jobs.id
          AND time_of_arrivals.eba_type IN (0, 3, 4)
      ORDER BY
        id ASC
      LIMIT 1)) / 60) AS numeric) - cast((Extract(EPOCH FROM (
      SELECT
        (time - created_at)
      FROM time_of_arrivals
    WHERE
      time_of_arrivals.job_id = jobs.id
      AND time_of_arrivals.eba_type IN (0, 3, 4)
    ORDER BY
      id ASC
    LIMIT 1)) / 60) AS numeric), 0) AS "ATA - ETA",
  TO_CHAR(((
    SELECT
      created_at FROM time_of_arrivals
    WHERE
      time_of_arrivals.job_id = jobs.id
      AND time_of_arrivals.eba_type IN (0, 3, 4)
  ORDER BY
    id ASC
  LIMIT 1) - ( WITH RECURSIVE parents (
    id, parent_id, adjusted_created_at
) AS (
  SELECT
    inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at FROM jobs AS inner_jobs
  WHERE
    inner_jobs.id = jobs.id
  UNION
  SELECT
    inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at FROM jobs AS inner_jobs, parents
  WHERE
    inner_jobs.id = parents.parent_id)
SELECT
  adjusted_created_at FROM parents
ORDER BY
  id ASC
LIMIT 1)), 'HH24:MI:SS') AS "Handle Time",
  jobs.status AS "Status",
  customer_types.name AS "Payment Type",
  CAST((
    SELECT
      sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) AS sum_amount FROM invoicing_line_items
  WHERE
    invoicing_ledger_items.id = invoicing_line_items.ledger_item_id
    AND invoicing_line_items.deleted_at IS NULL
  GROUP BY
    invoicing_ledger_items.id) AS DECIMAL(14, 2)) AS "Invoiced",
  jobs.partner_live AS "Partner Live",
  jobs.partner_open AS "Partner Open",
  CASE WHEN (jobs.partner_live
    AND jobs.partner_open) THEN
    TRUE
  ELSE
    NULL
  END AS "Partner Live and Open",
  jobs.platform AS "Platform",
  invoicing_ledger_items.state AS "Invoice Status",
  vehicles.vin,
  jobs.unit_number AS "Unit Number",
  jobs.ref_number AS "Claim Number",
  symptoms.name AS "Symptom",
  CASE WHEN (jobs.drop_location_id IS NOT NULL) THEN
  (estimates.seconds_ab + 25 * 60 + estimates.seconds_bc + 25 * 60 + estimates.seconds_ca) / 60
ELSE
  (estimates.seconds_ab + 25 * 60 + estimates.seconds_ba) / 60
  END AS "P2P Job Time Estimate",
  CASE WHEN (jobs.drop_location_id IS NOT NULL) THEN
  (estimates.seconds_ab + 25 * 60 + estimates.seconds_bc + 25 * 60) / 60
ELSE
  (estimates.seconds_ab + 25 * 60) / 60
  END AS "Port-to-Complete Job Time Estimate",
  departments.name AS "Department",
  CONCAT_WS(' ', fleet_dispatchers.first_name, fleet_dispatchers.last_name) AS "Fleet Dispatcher",
  CONCAT_WS(' ', partner_dispatchers.first_name, partner_dispatchers.last_name) AS "Partner Dispatcher",
  US_time (adjusted_ajs (jobs.id, 9), 'America/Los_Angeles') AS "Created",
  US_time (adjusted_ajs (jobs.id, 10), 'America/Los_Angeles') AS "Assigned",
  US_time (adjusted_ajs (jobs.id, 11), 'America/Los_Angeles') AS "Accepted",
  US_time (adjusted_ajs (jobs.id, 2), 'America/Los_Angeles') AS "Dispatched",
  US_time (adjusted_ajs (jobs.id, 3), 'America/Los_Angeles') AS "En Route",
  US_time (adjusted_ajs (jobs.id, 4), 'America/Los_Angeles') AS "On Site",
  US_time (adjusted_ajs (jobs.id, 5), 'America/Los_Angeles') AS "Towing",
  US_time (adjusted_ajs (jobs.id, 6), 'America/Los_Angeles') AS "Tow Destination",
  US_time (adjusted_ajs (jobs.id, 7), 'America/Los_Angeles') AS "Completed",
  US_time (adjusted_ajs (jobs.id, 21), 'America/Los_Angeles') AS "GOA",
  US_time (adjusted_ajs (jobs.id, 18), 'America/Los_Angeles') AS "Released",
  US_time (adjusted_ajs (jobs.id, 8), 'America/Los_Angeles') AS "Canceled",
  hms (adjusted_ajs (jobs.id, 11) - adjusted_ajs (jobs.id, 9)) AS "Digital Dispatch Time",
  hms (adjusted_ajs (jobs.id, 3) - adjusted_ajs (jobs.id, 9)) AS "Digital En Route",
  hms (adjusted_ajs (jobs.id, 7) - adjusted_ajs (jobs.id, 3)) AS "En Route -> Completed",
  jobs.notes AS "Job Details",
  jobs.fleet_notes AS "Fleet Notes",
  jobs.driver_notes AS "Invoice Notes",
  job_reject_reasons.text AS "Reject Reason",
  jobs.reject_reason_info AS "Reject Comment",
  rescue_vehicle.name AS "Rescue Vehicle Name",
  /* These are for filtering on the actual reports */
  jobs.created_at,
  jobs.fleet_company_id,
  jobs.rescue_company_id
FROM
  jobs
  LEFT JOIN estimates ON jobs.estimate_id = estimates.id
  LEFT JOIN companies partner ON partner.id = jobs.rescue_company_id
  LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
  LEFT JOIN locations AS dropoff ON dropoff.id = jobs.drop_location_id
  LEFT JOIN location_types AS pickup_location_type ON pickup_location_type.id = pickup.location_type_id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN symptoms ON jobs.symptom_id = symptoms.id
  LEFT JOIN invoicing_ledger_items ON (jobs.id = invoicing_ledger_items.job_id
      AND invoicing_ledger_items.sender_id = jobs.rescue_company_id
      AND invoicing_ledger_items.deleted_at IS NULL
      AND invoicing_ledger_items.type = 'Invoice')
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN departments ON jobs.department_id = departments.id
  LEFT JOIN users AS customer ON customer.id = drives.user_id
  LEFT JOIN customer_types ON customer_types.id = jobs.customer_type_id
  LEFT JOIN locations AS original_pickup ON original_pickup.id = jobs.original_service_location_id
  LEFT JOIN locations provider_location ON partner.location_id = provider_location.id
  LEFT JOIN users AS fleet_dispatchers ON jobs.fleet_dispatcher_id = fleet_dispatchers.id
  LEFT JOIN users AS partner_dispatchers ON jobs.partner_dispatcher_id = partner_dispatchers.id
  LEFT JOIN job_reject_reasons ON jobs.reject_reason_id = job_reject_reasons.id
  LEFT JOIN vehicles AS rescue_vehicle ON jobs.rescue_vehicle_id = rescue_vehicle.id
ORDER BY
  jobs.id DESC;
