SELECT
  invs.id,
  invs.sender_id
FROM
  invoicing_ledger_items invs
WHERE (((invs.type)::text = 'Invoice'::text)
  AND (invs.deleted_at IS NULL)
  AND (((invs.state)::text = ANY (ARRAY[('partner_sent'::character varying)::text, ('paid'::character varying)::text, ('on_site_payment'::character varying)::text, ('fleet_approved'::character varying)::text, ('fleet_downloaded'::character varying)::text, ('swoop_approved_partner'::character varying)::text, ('swoop_downloaded_partner'::character varying)::text]))
    OR (invs.paid IS TRUE))
  AND ((invs.qb_imported_at IS NULL)
    OR (invs.qb_pending_import IS TRUE)));
