SELECT
  invs.id AS invoice_id,
  accts.id,
  accts.name,
  accts.accounting_email AS email,
  accts.phone,
  locs.state,
  locs.city,
  locs.street,
  locs.zip,
  locs.country,
  (
    SELECT
      sites.tz
    FROM
      sites
    WHERE ((sites.company_id = invs.sender_id)
      AND ((sites.type)::text = 'PartnerSite'::text)
      AND (sites.tz IS NOT NULL))
  LIMIT 1) AS timezone,
(btrim(concat(users.first_name, ' ', users.last_name)))::character varying AS customer_name,
users.phone AS customer_phone
FROM (((((invoicing_ledger_items invs
        LEFT JOIN jobs ON ((invs.job_id = jobs.id)))
      LEFT JOIN drives ON ((jobs.driver_id = drives.id)))
    LEFT JOIN users ON ((users.id = drives.user_id)))
  JOIN accounts accts ON (((accts.id = invs.recipient_id)
        AND ((invs.recipient_type)::text = 'Account'::text))))
  LEFT JOIN locations locs ON ((accts.location_id = locs.id)))
UNION
SELECT
  invs.id AS invoice_id,
  accts.id,
  accts.name,
  accts.accounting_email AS email,
  accts.phone,
  locs.state,
  locs.city,
  locs.street,
  locs.zip,
  locs.country,
  (
    SELECT
      sites.tz
    FROM
      sites
    WHERE ((sites.company_id = invs.sender_id)
      AND ((sites.type)::text = 'PartnerSite'::text)
      AND (sites.tz IS NOT NULL))
  LIMIT 1) AS timezone,
(btrim(concat(users.first_name, ' ', users.last_name)))::character varying AS customer_name,
users.phone AS customer_phone
FROM (((invoicing_ledger_items invs
      JOIN users ON (((users.id = invs.recipient_id)
            AND ((invs.recipient_type)::text = 'User'::text))))
    JOIN accounts accts ON (((accts.company_id = invs.sender_id)
          AND ((accts.name)::text = 'Cash Call'::text)
          AND ((invs.recipient_type)::text = 'User'::text))))
  LEFT JOIN locations locs ON ((accts.location_id = locs.id)));
