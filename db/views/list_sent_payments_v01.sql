SELECT
  pays.id,
  pays.sender_id
FROM (invoicing_ledger_items pays
  JOIN invoicing_ledger_items invoice ON ((pays.invoice_id = invoice.id)))
WHERE (((pays.type)::text = 'Payment'::text)
  AND (pays.deleted_at IS NULL)
  AND ((pays.qb_imported_at IS NULL)
    OR (pays.qb_pending_import IS TRUE))
  AND (invoice.deleted_at IS NULL)
  AND (((invoice.state)::text = ANY (ARRAY[('partner_sent'::character varying)::text, ('paid'::character varying)::text, ('on_site_payment'::character varying)::text, ('fleet_approved'::character varying)::text, ('fleet_downloaded'::character varying)::text, ('swoop_approved_partner'::character varying)::text, ('swoop_downloaded_partner'::character varying)::text]))
    OR (invoice.paid IS TRUE)));
