class DeletePaymentsAssociatedWithDeletedInvoices < SeedMigration::Migration

  def up
    ActiveRecord::Base.connection.execute(
      "UPDATE invoicing_ledger_items as payment 
       SET deleted_at = invoice.deleted_at
       FROM invoicing_ledger_items as invoice
       WHERE 
         payment.invoice_id=invoice.id
         AND payment.type='Payment'
         AND payment.deleted_at is null
         AND invoice.deleted_at IS NOT NULL")
  end

  def down

  end

end
