# frozen_string_literal: true

class AddRoles < SeedMigration::Migration

  def up
    Role.init_all
  end

  def down
  end

end
