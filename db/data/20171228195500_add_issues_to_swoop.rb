class AddIssuesToSwoop < SeedMigration::Migration
  def up
    swoop = Company.swoop

    Issue.all.each do |issue|
      CompaniesIssues.create(
        company_id: swoop.id,
        issue_id: issue.id
      )
    end
  end

  def down

  end
end
