class RemoveFeatureUserViews < SeedMigration::Migration
  def up
    user_views = Feature.where(name: 'user_views')
    if user_views.length > 0
      CompaniesFeature.where(feature_id: user_views.first.id)
    end
    user_views.destroy_all
  end

  def down

  end
end
