class SetTowIfTireChangeFailsAsStandard < SeedMigration::Migration
  def up
    tow_if_tire_changes_fail = ServiceCode.where(
      name: ServiceCode::TOW_IF_TIRE_CHANGE_FAILS,
      addon: false
    ).first_or_create

    tow_if_tire_changes_fail.is_standard = true

    tow_if_tire_changes_fail.save!
  end

  def down

  end
end
