class AddModel3TireType < SeedMigration::Migration
  def up
    TireType.where(car: 'Model 3', size: '235').first_or_create
  end

  def down

  end
end
