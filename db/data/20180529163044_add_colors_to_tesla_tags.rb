class AddColorsToTeslaTags < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("
      UPDATE tags set color = '#ADB8FE'
      WHERE name = 'Active - Preferred'
      AND company_id = (SELECT id FROM companies WHERE name = 'Tesla')
    ")

    ActiveRecord::Base.connection.execute("
      UPDATE tags set color = '#FDEB8F'
      WHERE name = 'Active'
      AND company_id = (SELECT id FROM companies WHERE name = 'Tesla')
    ")

    ActiveRecord::Base.connection.execute("
      UPDATE tags set color = '#FF7171'
      WHERE name = 'Deactivated'
      AND company_id = (SELECT id FROM companies WHERE name = 'Tesla')
    ")

    ActiveRecord::Base.connection.execute("
      UPDATE tags set color = '#6C6C6C'
      WHERE name = 'Out of Network'
      AND company_id = (SELECT id FROM companies WHERE name = 'Tesla')
    ")
  end

  def down
    ActiveRecord::Base.connection.execute("
      UPDATE tags set color = NULL
      WHERE name IN ('Active - Preferred', 'Active', 'Deactivated', 'Out of Network')
      AND company_id = (SELECT id FROM companies WHERE name = 'Tesla')
    ")
  end
end
