class FlipEditInvoiceFeatures < SeedMigration::Migration
  def up
    disableView = Feature.create(name:"Disable driver view invoice",
       description:"disables viewing mobile invoices for drivers on the react native app",
       supported_by:['RescueCompany'])
    disableEdit = Feature.create(name:"Disable driver edit invoice",
       description:"disables editing mobile invoices for drivers on the react native app",
       supported_by:['RescueCompany'])
  end

  def down

  end
end
