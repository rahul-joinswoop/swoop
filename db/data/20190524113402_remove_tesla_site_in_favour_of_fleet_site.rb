class RemoveTeslaSiteInFavourOfFleetSite < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("UPDATE sites SET deleted_at = now() where type = 'TeslaSite'")
  end

  def down

  end
end
