class RemoveFleetActivityReportFromTuro < SeedMigration::Migration
  def up
    turo = Company.find_by(name: 'Turo')

    return if turo.nil?

    fleet_activity_report = turo.reports.
      where(name: 'Fleet Managed Activity Report').
      first

    return unless fleet_activity_report

    turo.reports.delete(fleet_activity_report)
  end
end
