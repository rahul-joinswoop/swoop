class AddAllPartnersAsProvidersForUsaa < SeedMigration::Migration
  def up
    usaa=FleetCompany.find_by(name:'USAA')
    RescueCompany.where(deleted_at:nil).find_each(batch_size: 100) do |rescue_company|
      Rails.logger.debug "Linking #{rescue_company.name}"
      CompanyService::LinkProviderAndClient.new(
        rescue_company: rescue_company,
        fleet_company: usaa).call
    end
  end

  def down

  end
end
