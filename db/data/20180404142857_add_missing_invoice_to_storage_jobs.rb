class AddMissingInvoiceToStorageJobs < SeedMigration::Migration
  def up
    # we just load job ids here
    Job.where(
      service_code_id: ServiceCode.find_by_name(ServiceCode::STORAGE).id,
      invoice_id: nil).pluck(:id)
    .each do |id|
      # and then enqueue CreateOrUpdateInvoice for each id
      CreateOrUpdateInvoice.perform_async(id)
    end
  end

  def down

  end
end
