class UpdateSafetyQuestion < SeedMigration::Migration
  def up
    question = Question.where(
      question: Question::CUSTOMER_IN_A_SAFE_LOCATION
    ).first_or_create!

    question.update! order_num: 0

    Answer::DEFAULT_ANSWERS_PER_QUESTION[Question::CUSTOMER_IN_A_SAFE_LOCATION].each do |answer|
      Answer.where(
        answer: answer,
        question: question
      ).first_or_create!
    end

  end

  def down

  end
end
