class RenameEspPrograms < SeedMigration::Migration

  def up
    ClientProgram.where("code LIKE 'lincoln_esp_%'").update_all(name: "ESP")
  end

end
