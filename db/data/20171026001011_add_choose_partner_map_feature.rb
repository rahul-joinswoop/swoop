class AddChoosePartnerMapFeature < SeedMigration::Migration
  def up
    Feature.create(name:"Choose Partner Map",
                   description:"Show map in the choose partner modal",
                   supported_by:['FleetCompany',"SuperCompany"])
  end

  def down

  end
end
