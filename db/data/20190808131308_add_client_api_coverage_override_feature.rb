class AddClientAPICoverageOverrideFeature < SeedMigration::Migration
  def up
    feature = Feature.where(name: Feature::CLIENT_API_COVERAGE_OVERRIDE).first_or_create!

    feature.update!(
      description: Feature::DESCRIPTIONS[Feature::CLIENT_API_COVERAGE_OVERRIDE],
      supported_by: ['FleetCompany'],
    )
  end

  def down

  end
end
