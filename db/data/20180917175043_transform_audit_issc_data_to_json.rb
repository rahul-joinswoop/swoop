class TransformAuditIsscDataToJson < SeedMigration::Migration
  def up
    scope = AuditIssc
      .where.not(path: "/digital_dispatch/handle_rsc_event")
      .where(system: "rsc")

    scope.find_each do |audit|
      next if audit.data.nil?

      # Never use eval. I'm only doing this because we control the input absolutely.
      new_data = eval(audit.data).to_json
      Rails.logger.debug("AuditIssc - transforming data from #{audit.data} to #{new_data}")
      audit.data = new_data
      audit.save!
    end
  end

  def down
  end
end
