class AddFleetResponseCompany < SeedMigration::Migration
  def up
    fr = Company.find_by(name: FleetCompany::FLEET_RESPONSE)
    FleetCompany.init_managed(FleetCompany::FLEET_RESPONSE) unless fr
  end

  def down
  end
end
