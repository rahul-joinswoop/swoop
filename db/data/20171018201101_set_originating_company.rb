class SetOriginatingCompany < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute "update jobs set originating_company_id=fleet_company_id where originating_company_id is null"
  end

  def down
  end
end
