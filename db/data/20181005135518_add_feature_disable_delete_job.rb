class AddFeatureDisableDeleteJob < SeedMigration::Migration
  def up
    Feature.create(
      name: "Disable Delete Job",
      description: "Disable Permanently Delete option in job menu",
      supported_by: ['RescueCompany']
    )
  end

  def down

  end
end
