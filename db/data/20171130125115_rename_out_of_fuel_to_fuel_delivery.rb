class RenameOutOfFuelToFuelDelivery < SeedMigration::Migration
  def up
    # updates DEV env because we already had FUEL DELIVERY in seeds.rb
    if Rails.env.development?
      existent_fuel_delivery = ServiceCode.find_by_name(ServiceCode::FUEL_DELIVERY)

      if existent_fuel_delivery
        existent_fuel_delivery.name = 'Legacy Fuel Delivery'

        existent_fuel_delivery.save!
      end
    end

    out_of_fuel = ServiceCode.where(name: 'Out Of Fuel').first

    if out_of_fuel
      out_of_fuel.name = ServiceCode::FUEL_DELIVERY

      out_of_fuel.save!
    end
  end

  def down

  end
end
