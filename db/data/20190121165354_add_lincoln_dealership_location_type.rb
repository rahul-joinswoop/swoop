class AddLincolnDealershipLocationType < SeedMigration::Migration

  def up
    lincoln = Company.find_by!(name: 'Lincoln')
    dealership = LocationType.find_or_create_by(name: LocationType::DEALERSHIP)
    clt = CompaniesLocationType.find_by(company: lincoln,
                                        location_type: dealership)
    if !clt
      CompaniesLocationType.create!(company: lincoln,
                                    location_type: dealership)
    end
  end

  def down
  end

end
