class ChangeBmwQuestions < SeedMigration::Migration
  def up
    # change Trip Interrupt description
    trip_question = Question.find_by(name: 'trip-interrupt')

    if trip_question
      trip_question.question = '100+ miles from destination?'

      trip_question.save!
    end

    # remove Tow Pin question
    pin_question = Question.find_by(name: 'pin-present')
    bmw = Company.find_by(name: 'BMW NA')

    if pin_question && bmw
      company_service_question = CompanyServiceQuestion.where(company_id: bmw.id, question_id: pin_question.id)

      company_service_question.each do |service_question|
        service_question.delete
      end
    end
  end

  def down
    # change back Trip Interrupt description
    trip_question = Question.find_by(name: 'trip-interrupt')

    if trip_question
      trip_question.question = 'Trip interruption?'

      trip_question.save!
    end

    # re-add pin_question to BMW
    pin_question = Question.find_by(name: 'pin-present')
    bmw = Company.find_by(name: 'BMW NA')

    if pin_question && bmw
      company_service_question = CompanyServiceQuestion.find_or_create_by company_id: bmw.id, question_id: pin_question.id
    end
  end
end
