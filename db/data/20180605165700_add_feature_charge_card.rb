class AddFeatureChargeCard < SeedMigration::Migration
  def up
    Feature.create(
      name: 'Charge Card',
      description: 'Allow users to charge payments on customers card',
      supported_by: ['RescueCompany']
    )
  end

  def down; end # not needed
end
