class AddShowEtaOnMobileJobFeature < SeedMigration::Migration
  def up
    Feature.create(name:"Show ETA on mobile job details",
                   description:"Shows eta on mobile job details",
                   supported_by:['RescueCompany'])
  end

  def down

  end
end
