class AddCanceledToServiceCodes < SeedMigration::Migration
  LATE_CANCEL_FEE = 'Late Cancel Fee'.freeze
  GOA_FEE = 'Gone On Arrival Fee'.freeze

  def up
    [LATE_CANCEL_FEE, GOA_FEE].each do |service_name|
      existing = ServiceCode.find_by(name: service_name)
      if !existing
        ServiceCode.create!(name: service_name, addon: true, is_standard: false)
      end
    end
  end

  def down
  end
end
