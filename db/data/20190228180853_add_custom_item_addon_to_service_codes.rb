class AddCustomItemAddonToServiceCodes < SeedMigration::Migration
  def up
    ServiceCode.where(name: 'Custom Item', addon: true).first_or_create!
  end

  def down

  end
end
