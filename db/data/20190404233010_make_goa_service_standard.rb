class MakeGoaServiceStandard < SeedMigration::Migration

  def up
    goa = ServiceCode.find_by(name: "GOA")
    goa.update!(is_standard: true)

    # The list of service codes to add to new rescue companies is copied from the Swoop company.
    swoop = SuperCompany.find_by(name: "Swoop")
    swoop.service_codes << goa unless swoop.service_codes.include?(goa)

    RescueCompany.preload(:service_codes).find_in_batches(batch_size: 50) do |batch|
      RescueCompany.transaction do
        batch.each do |company|
          company.service_codes << goa unless company.service_codes.include?(goa)
        end
      end
    end
  end

  def down
  end

end
