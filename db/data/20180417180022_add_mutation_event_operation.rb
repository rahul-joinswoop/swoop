class AddMutationEventOperation < SeedMigration::Migration
  def up
    EventOperation.where(name: EventOperation::MUTATION).first_or_create!
  end

  def down

  end
end
