class CleanRateAdditions < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("update rate_additions set deleted_at=now() where amount is null and name is null")
    ActiveRecord::Base.connection.execute("update rate_additions set amount=0 where amount is null")
  end

  def down

  end
end
