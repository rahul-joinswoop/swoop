class UpdateClientProgramCoverageNotesForRootInsuranceManualEntry < SeedMigration::Migration
  def up
    name = 'Root Insurance'
    root_insurance = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !root_insurance # to support dev envs
      root_insurance = FleetCompany.init_managed(name).company
    end

    # Create Manual Entry ClientProgram
    manual_entry = ClientProgram.where(
      name: 'Manual Entry',
      code: "ric_rrs_me",
      client_identifier: 'rrs',
      company_id: root_insurance.id,
    ).first_or_create!

    coverage_note  = "If customer insists they are covered, kindly cold transfer them to Root Member Services at 866-980-9431. If covered a Root Agent will call to confirm."
    manual_entry.update! coverage_notes: coverage_note
  end

  def down

  end
end
