class AddPlsAndCoverageSettingsForMyles < SeedMigration::Migration
  def up
    name = 'Myles'
    myles = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !myles # to support dev envs
      myles = FleetCompany.init_managed(name).company
    end

    PCC::NewClientSetup::Organizer.call(
      company: myles,
      policy_lookup_client_settings: {
        Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE => 'myles',
        Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => [],
        Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE => ['service', 'drop_location', 'service_location'],
      }
    )

    myles.settings << Setting.new(key: Setting::RUN_COVERAGE_ON_NEW_JOB_FORM_LOAD, value: true)

    not_covered = ClientProgram.where(
      name: 'Myles',
      code: "manual_entry",
      client_identifier: 'myles',
      company_id: myles.id,
      coverage_filename: 'myles'
    ).first_or_create!

    fuel_delivery = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::FUEL_DELIVERY)
    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: fuel_delivery, notes: '2 gallon fuel delivery limit'
    )

    winch_out = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::WINCH_OUT)
    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: winch_out, notes: 'Covered if vehicle within 10 ft of paved surface'
    )

  end

  def down
    myles = Company.not_deleted.find_by(name: 'Myles', type: 'FleetCompany', in_house: [nil, false])

    if myles
      myles.update! deleted_at: Time.now
    end
  end
end
