class AddJobStatusDeleted < SeedMigration::Migration
  def up
    # (Why does JobStatus class have ids hardcoded on it?)
    # The id is forced here because JobStatus class relies on hardcoded IDs, so it's likely (and hopefully!) all envs must have the same ones.
    JobStatus.find_or_create_by(id: 25, name: JobStatus::ID_MAP[JobStatus::DELETED])

    # Then the pk sequence is updated to the to the maximum value
    ActiveRecord::Base.connection.reset_pk_sequence!('job_statuses')
  end

  def down
    JobStatus.find_by_name(JobStatus::ID_MAP[JobStatus::DELETED]).delete
  end
end
