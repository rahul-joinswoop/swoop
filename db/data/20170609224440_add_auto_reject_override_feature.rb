class AddAutoRejectOverrideFeature < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute "INSERT INTO FEATURES (name, description, created_at, updated_at) VALUES('MC Job AutoReject Override', 'Before job expires auto-reject it instead of auto-callback', NOW(), NOW())"
  end

  def down

  end
end
