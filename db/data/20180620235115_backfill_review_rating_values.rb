class BackfillReviewRatingValues < SeedMigration::Migration
  def up
    sql = 'UPDATE reviews SET nps_question1 = rating WHERE rating IS NOT NULL AND type IS NULL AND nps_question1 IS NULL'
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    sql = 'UPDATE reviews SET nps_question1 = NULL WHERE rating IS NOT NULL AND type IS NULL AND nps_question1 = rating'
    ActiveRecord::Base.connection.execute(sql)
  end
end
