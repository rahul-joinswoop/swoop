class UpdateRescueCompaniesWithNewReports < SeedMigration::Migration
  def up
    RescueCompany.find_each do |company|
      company.add_reports_by_name [Report::COMMISSIONS_REPORT, Report::PAYMENTS_REPORT]
    end
  end

  def down
  end
end
