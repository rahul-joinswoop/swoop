class FixStandardServiceCodes < SeedMigration::Migration
  def up
    fuel_delivery = ServiceCode.find_by_name(ServiceCode::FUEL_DELIVERY)
    tire_change   = ServiceCode.find_by_name(ServiceCode::TIRE_CHANGE)

    [fuel_delivery, tire_change].each do |service|
      service.is_standard = true

      service.save!
    end
  end

  def down

  end
end
