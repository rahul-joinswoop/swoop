class AddZone2AsClassTypeToJimTow < SeedMigration::Migration
  def up
    zone_2 = VehicleCategory.find_or_create_by name: "Zone 2"

    company = Company.find_by name: "Jim's Towing Service, Inc."

    if company
      CompaniesVehicleCategory.find_or_create_by company_id: company.id, vehicle_category_id: zone_2.id
    end
  end

  def down

  end
end
