class RemoveRecoveryDepartmentFromSouthernWrecker < SeedMigration::Migration
  def up
    company = Company.find_by name: "Southern Wrecker & Recovery"
    if company
      d = Department.where(company: company).where(name: "Recovery").first
      d.destroy if d
    end
  end

  def down
  end
end
