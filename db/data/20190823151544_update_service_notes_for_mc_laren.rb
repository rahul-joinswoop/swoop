class UpdateServiceNotesForMcLaren < SeedMigration::Migration
  def up
    name = 'McLaren'
    mclaren = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !mclaren # to support dev envs
      mclaren = FleetCompany.init_managed(name).company
    end

    tow = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::TOW)

    # Update MCW (McLaren Warranty) ClientProgram Service Notes for Tow
    mclaren_mcw = ClientProgram.where(
      name: 'MCW',
      code: 'mclaren_mcw',
      client_identifier: 'mcw',
      company_id: mclaren.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'mclaren_mcw'
    ).first_or_create!

    mcw_service_notes = ClientProgramServiceNotes.where(
      client_program_id: mclaren_mcw.id,
      service_code: tow).first_or_initialize

    mcw_service_notes.notes = 'Use Covered Flatbed over 100mi / 160km. Costs over $2000 requires authorization.'

    mcw_service_notes.save!

    # Update MWT (Service Contract) ClientProgram Service Notes for Tow
    mclaren_mwt = ClientProgram.where(
      name: 'MWT',
      code: 'mclaren_mwt',
      client_identifier: 'mwt',
      company_id: mclaren.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'mclaren_mwt'
    ).first_or_create!

    mwt_service_notes = ClientProgramServiceNotes.where(
      client_program_id: mclaren_mwt.id,
      service_code: tow).first_or_initialize

    mwt_service_notes.notes = 'Use Covered Flatbed over 100mi / 160km. Costs over $2000 requires authorization.'
    mwt_service_notes.save!

    # Update McLaren Client Program Manual Entry Service Notes for Tow
    manual_entry = ClientProgram.where(
      name: 'Manual Entry',
      code: "mclaren_manual",
      company_id: mclaren.id,
      coverage_filename: 'mclaren_manual'
    ).first_or_create!

    manual_entry_service_notes = ClientProgramServiceNotes.where(
      client_program_id: manual_entry.id,
      service_code: tow).first_or_initialize

    manual_entry_service_notes.notes = 'Use Covered Flatbed over 100mi / 160km.'
    manual_entry_service_notes.save!

  end

  def down

  end
end
