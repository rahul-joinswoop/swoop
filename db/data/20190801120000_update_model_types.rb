# frozen_string_literal: true

class UpdateModelTypes < SeedMigration::Migration

  def up
    ModelType.import_all!
    # remove the old EmailVerification entry if it exists
    ModelType.where(name: 'EmailVerification').delete_all
  end

  def down
  end

end
