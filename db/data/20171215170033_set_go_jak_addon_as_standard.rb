class SetGoJakAddonAsStandard < SeedMigration::Migration
  def up
    ServiceCode.find_by_name(ServiceCode::GOJAK).update(is_standard: true)
  end

  def down

  end
end
