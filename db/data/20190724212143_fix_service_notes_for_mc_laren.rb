#
# This is a fix to 20190722131853_add_manual_client_program_to_mc_laren.rb
# since that seed didn't check if the loaded services were real services or actually addons (addon: true),
# and it caused bad behaviour in the UI.
#
class FixServiceNotesForMcLaren < SeedMigration::Migration
  def up
    name = 'McLaren'
    mclaren = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !mclaren # to support dev envs
      mclaren = FleetCompany.init_managed(name).company
    end

    manual_entry = ClientProgram.where(
      name: 'Manual Entry',
      code: "mclaren_manual",
      company_id: mclaren.id,
      coverage_filename: 'mclaren_manual'
    ).first_or_create!

    # delete all previous service notes for tow, fuel and lock out,
    # as we can't make sure those are services or addons
    # as 20190722131853_add_manual_client_program_to_mc_laren.rb didn't check for it
    ClientProgramServiceNotes.joins(:service_code).where(
      client_program_id: manual_entry.id,
      service_codes: { name: [ServiceCode::TOW, ServiceCode::FUEL, ServiceCode::LOCK_OUT] }
    ).delete_all

    # make sure these are not addons now by adding "where.not(addon: true)",
    # and load 'Fuel Delivery' this time, since this is the correct 'Fuel service name' we're talking about,
    # and not only 'Fuel' as originally required by the ticket
    tow = ServiceCode.where.not(addon: true).find_by_name(ServiceCode::TOW)
    fuel_delivery = ServiceCode.where.not(addon: true).find_by_name(ServiceCode::FUEL_DELIVERY)
    lock_out = ServiceCode.where.not(addon: true).find_by_name(ServiceCode::LOCK_OUT)

    # Finally, recreate specific coverage notes for these services
    manual_entry.service_notes << ClientProgramServiceNotes.new(
      service_code: tow, notes: 'Use Enclosed Carriers equipment over 100mi / 160km'
    )

    manual_entry.service_notes << ClientProgramServiceNotes.new(
      service_code: fuel_delivery, notes: 'Two(2) gallon limit'
    )

    manual_entry.service_notes << ClientProgramServiceNotes.new(
      service_code: lock_out, notes: 'Must be Towed'
    )
  end

  def down

  end
end
