class AddFleetServiceMapFeature < SeedMigration::Migration
  def up
    Feature.create(name:'Fleet Service Map',
                   description:'Shows map to fleets',
                   supported_by:['FleetCompany'])
  end

  def down

  end
end
