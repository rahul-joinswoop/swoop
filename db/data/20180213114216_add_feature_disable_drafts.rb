class AddFeatureDisableDrafts < SeedMigration::Migration
  def up
    Feature.create(
      name: 'Disable Drafts',
      description: 'User should not have the ability to save a job as draft or add Drafts tab to dashboard',
      supported_by: ['RescueCompany', 'FleetCompany', 'SuperCompany']
    )
  end

  def down

  end
end
