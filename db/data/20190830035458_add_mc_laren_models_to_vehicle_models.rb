class AddMcLarenModelsToVehicleModels < SeedMigration::Migration
  def up
    vehicle_make = 'McLaren'
    current_models = ['540C', '570S Coupe', '570S Spider', '570GT', '600LT',
                      '600LT Spider', 'GT', '720S', '720S Spider', 'Senna',
                      'Speedtail'].freeze
    legacy_models = ['P1LM', 'P1', '675LT Spider', '675LT', '650S Spider', '650S',
                     '12C Spider', '12C', 'F1 GT', 'F1 LM', 'F1', 'M6GT'].freeze

    # The `original: true` is a legacy toggle for active as far as I can see
    mclaren = VehicleMake.find_by(name: vehicle_make, original: true)

    if mclaren
      (current_models + legacy_models).each do |model_name|
        mclaren.vehicle_models.find_or_create_by(name: model_name)
      end
    end

  end

  def down

  end
end
