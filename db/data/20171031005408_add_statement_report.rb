class AddStatementReport < SeedMigration::Migration
  def up
    statement_report = Report.find_by_name('Statement')

    RescueCompany.where(deleted_at: nil).find_in_batches(batch_size: 300) do |rescue_companies|
      rescue_companies.each {|rescue_company| rescue_company.reports << statement_report }
    end

    deprecated_statements_report = Report.find_by_name('Statements')
    deprecated_statements_report.deleted_at = Time.now

    deprecated_statements_report.save!
  end

  def down

  end
end
