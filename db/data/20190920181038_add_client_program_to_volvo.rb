class AddClientProgramToVolvo < SeedMigration::Migration
  def up
    name = 'Volvo'
    volvo = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !volvo # to support dev envs
      volvo = FleetCompany.init_managed(name).company
    end

    # Create VAN (Volvo Assistance) ClientProgram
    volvo_van = ClientProgram.where(
      name: 'Volvo Assistance (VAN)',
      code: 'van',
      client_identifier: 'van',
      company_id: volvo.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagengarantie EZ ab 01.01.2009"
    ).first_or_create!

    # Create VOG / VIG (Volvo Assistance) ClientProgram
    volvo_vog = ClientProgram.where(
      name: 'Volvo Assistance (VOG/VIG)',
      code: 'vog/vig',
      client_identifier: 'vog/vig',
      company_id: volvo.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Gebrauchtwagengarantie (VOG) ab 01.01.2011 Inspektionsgarantie (VIG)  alle Fahrzeuge außer VLT"
    ).first_or_create!

    # Create ARC (Volvo Diplomat, Military, Expad Sales & Overseas Delivery) ClientProgram
    volvo_arc = ClientProgram.where(
      name: 'Volvo Diplomat, Military, Expad Sales & Overseas Delivery (ARC)',
      code: 'arc_1',
      client_identifier: 'arc',
      company_id: volvo.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagengarantie EZ ab 01.10.2016"
    ).first_or_create!

    # Create ARC (Volvo Tourist and Diplomat Sales) ClientProgram
    volvo_arc = ClientProgram.where(
      name: 'Volvo Tourist and Diplomat Sales (ARC)',
      code: 'arc_2',
      client_identifier: 'arc',
      company_id: volvo.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagengarantie EZ bis 30.09.2016"
    ).first_or_create!

  end

  def down

  end
end
