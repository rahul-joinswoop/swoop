class AddClassTypesToBigValley < SeedMigration::Migration
  def up
    categories = ['Light Duty After Hours', 'Medium Duty After Hours', 'Heavy Duty After Hours']

    db_categories = []

    for category in categories
      db_categories << VehicleCategory.find_or_create_by(name: category)
    end

    company = RescueCompany.find_by(name: 'Big Valley Towing')

    for category in db_categories
      sc = company.vehicle_categories.find_by(name: category.name)

      if not sc
        company.vehicle_categories << category
      end
    end

    company.save!
  end

  def down

  end
end
