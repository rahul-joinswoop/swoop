class SetSupportStorageServiceCodes < SeedMigration::Migration
  def up
    service_codes = [
      ServiceCode::ACCIDENT_TOW,
      ServiceCode::DECKING,
      ServiceCode::HEAVY_HAUL,
      ServiceCode::IMPOUND,
      ServiceCode::LAW_ENFORCEMENT,
      ServiceCode::RECOVERY,
      ServiceCode::SECONDARY_TOW,
      ServiceCode::TOW,
      ServiceCode::TRANSPORT,
      ServiceCode::UNDECKING,
      ServiceCode::WINCH_OUT
    ]

    ServiceCode.where(name: service_codes).each do |service_code|
      service_code.support_storage = true

      service_code.save!
    end
  end

  def down

  end
end
