class RenameReviewSurveys < SeedMigration::Migration
  def up
    Feature.find_by(name: "Review Surveys").update!(name: "Review NPS Survey")
  end

  def down
  end
end
