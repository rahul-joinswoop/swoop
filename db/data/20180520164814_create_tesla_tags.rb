class CreateTeslaTags < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("INSERT INTO TAGS (name, company_id, created_at, updated_at) VALUES ('Active', (SELECT id FROM companies WHERE name = 'Tesla'), now(), now())")
    ActiveRecord::Base.connection.execute("INSERT INTO TAGS (name, company_id, created_at, updated_at) VALUES ('Active - Preferred', (SELECT id FROM companies WHERE name = 'Tesla'), now(), now())")
    ActiveRecord::Base.connection.execute("INSERT INTO TAGS (name, company_id, created_at, updated_at) VALUES ('Deactivated', (SELECT id FROM companies WHERE name = 'Tesla'), now(), now())")
    ActiveRecord::Base.connection.execute("INSERT INTO TAGS (name, company_id, created_at, updated_at) VALUES ('Out of Network', (SELECT id FROM companies WHERE name = 'Tesla'), now(), now())")
  end

  def down

  end
end
