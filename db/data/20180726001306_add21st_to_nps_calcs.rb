class Add21stToNpsCalcs < SeedMigration::Migration
  def up
    a21st = Company.find_by(name: '21st Century Insurance')

    if a21st
      CompanyRatingSetting.create(company: a21st,
                                  settings: {
                                    min_num_jobs: 5,
                                    type: :nps,
                                    nps_question: :nps_question1,
                                  })
    end
  end

  def down
  end
end
