class SetServiceCodeIdOnCommissionExclusions < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION setServiceCodeIdOnCommissionExclusions() RETURNS void AS
      $BODY$
      DECLARE
          commission_exclusion_row RECORD;
          service_code_row RECORD;
      BEGIN
          FOR commission_exclusion_row IN SELECT id, item FROM commission_exclusions
          LOOP
              
              FOR service_code_row IN SELECT id FROM service_codes WHERE name LIKE commission_exclusion_row.item LIMIT 1
              LOOP

                  UPDATE commission_exclusions SET service_code_id = service_code_row.id WHERE commission_exclusions.id = commission_exclusion_row.id;

              END LOOP;



          END LOOP;
      END;

      $BODY$
      LANGUAGE plpgsql;

      SELECT * FROM setServiceCodeIdOnCommissionExclusions();

      DROP FUNCTION setServiceCodeIdOnCommissionExclusions();
    }
  end

  def down

  end
end
