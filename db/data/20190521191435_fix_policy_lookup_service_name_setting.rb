class FixPolicyLookupServiceNameSetting < SeedMigration::Migration
  def up
    Setting.where(key: Setting::PCC_SERVICE_NAME, value: 'PolicyLookupSystem').update_all(value: PCC::PCCService::POLICY_LOOKUP_SERVICE)
  end

  def down

  end
end
