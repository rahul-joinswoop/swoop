class AddFeatureDepartmentFilter < SeedMigration::Migration
  def up
    Feature.create(name:"Department Filter",
                   description:"Adds a department filter to the dashboard",
                   supported_by:['FleetCompany'])
  end

  def down

  end
end
