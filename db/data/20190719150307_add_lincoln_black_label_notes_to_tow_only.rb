class AddLincolnBlackLabelNotesToTowOnly < SeedMigration::Migration
  def up
    lincoln = Company.find_by(name: 'Lincoln')

    black_label = ClientProgram.where(code: 'lincoln_black_label', company: lincoln.id).first_or_create!
    black_label.update!(coverage_notes: nil)

    tow_service = ServiceCode.find_by_name(ServiceCode::TOW)
    black_label.service_notes << ClientProgramServiceNotes.new(
      service_code: tow_service, notes: 'Offer tow to nearest Black Label dealer first (BL in Dealer Name)'
    )
  end

  def down

  end
end
