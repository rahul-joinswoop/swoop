class AddQuickbooksFeature < SeedMigration::Migration
  def up
    Feature.create(name:'Quickbooks',
                   description:'Allow quickbooks sync and configure options',
                   supported_by:['RescueCompany'])
  end

  def down

  end
end
