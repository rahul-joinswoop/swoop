class AddServiceAddOnToActionTowing < SeedMigration::Migration
  def up
    name = 'Fuel Surchage'

    service = ServiceCode.find_or_create_by name: name, addon: true

    company = Company.find_by name: 'Action Towing - Tampa'

    if company
      CompaniesService.find_or_create_by company_id: company.id, service_code_id: service.id
    end
  end

  def down

  end
end
