class ChangeSiteTeslaSitesToFleetSites < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("UPDATE sites SET type = 'FleetSite' where type in ('TeslaSite', 'ServiceCenterSite')")

    # and let's also remove their model types, they don't exist anymore after all
    ActiveRecord::Base.connection.execute("DELETE FROM model_types WHERE name in ('TeslaSite', 'ServiceCenterSite')")
  end

  def down

  end
end
