class DeleteInvalidAjs < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute "delete from audit_job_statuses where job_id is null"
  end

  def down

  end
end
