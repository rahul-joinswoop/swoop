class MakeCreditCardZipOptional < SeedMigration::Migration
  def up
    Feature.create(
      name: 'Optional Credit Card Zip',
      description: 'Make Zip Field Optional For Credit Card Charges',
      supported_by: ['RescueCompany','FleetCompany',"SuperCompany"]
    )
  end

  def down; end # not needed
end
