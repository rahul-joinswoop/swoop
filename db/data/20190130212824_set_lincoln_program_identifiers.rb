class SetLincolnProgramIdentifiers < SeedMigration::Migration

  def up
    lincoln = Company.find_by(name: "Lincoln")
    return unless lincoln

    {
      "lincoln_for_life" => "1596",
      "lincoln_esp_99986" => "99986",
      "lincoln_esp_99987" => "99987",
      "lincoln_esp_99994" => "99994",
      "lincoln_esp_99992" => "99992",
      "lincoln_esp_99995" => "99995",
      "lincoln_esp_99988" => "99988",
      "lincoln_esp_99998" => "99998",
      "lincoln_esp_99991" => "99991",
      "lincoln_esp_99989" => "99989",
      "lincoln_esp_99999" => "99999",
      "lincoln_esp_99997" => "99997",
      "lincoln_esp_99993" => "99993",
      "lincoln_esp_99990" => "99990",
      "lincoln_esp_99996" => "99996",
      "lincoln_warranty" => "991",
      "lincoln_black_label" => "1595",
      "lincoln_ford_company_vehicle" => "992",
      "lincoln_major_daily_rental" => "990",
      "lincoln_manual" => "991",
    }.each do |code, identifier|
      program = ClientProgram.find_by(code: code, company_id: lincoln.id)
      program.update!(client_identifier: identifier) if program
    end
  end

end
