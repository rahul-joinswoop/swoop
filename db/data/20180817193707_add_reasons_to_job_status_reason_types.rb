class AddReasonsToJobStatusReasonTypes < SeedMigration::Migration
  def up
    # CANCEL Reasons
    JobStatusReasonType.where(job_status_id: JobStatus::CANCELED, text: 'Prior job delayed', key: :cancel_prior_job_delayed).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::CANCELED, text: 'Traffic/service vehicle problem', key: :cancel_traffic_service_vehicle_problem).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::CANCELED, text: 'Out of area', key: :cancel_out_of_area).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::CANCELED, text: 'Another job priority', key: :cancel_another_job_priority).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::CANCELED, text: 'No reason given', key: :cancel_no_reason_given).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::CANCELED, text: 'Customer found alternate solution', key: :customer_found_alternate_solution).first_or_create!

    # GOA Reasons
    JobStatusReasonType.where(job_status_id: JobStatus::GOA, text: 'Customer cancel after deadline', key: :goa_customer_cancel_after_deadline).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::GOA, text: 'Wrong location given', key: :goa_wrong_location_given).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::GOA, text: 'Customer not with vehicle', key: :goa_customer_not_with_vehicle).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::GOA, text: 'Incorrect service', key: :goa_incorrect_service).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::GOA, text: 'Incorrect equipment', key: :goa_incorrect_equipment).first_or_create!
    JobStatusReasonType.where(job_status_id: JobStatus::GOA, text: 'Unsuccessful service attempt', key: :goa_unsuccessful_service_attempt).first_or_create!
  end

  def down

  end
end
