class RemoveFeatureDisableDrafts < SeedMigration::Migration
  def up
    Feature.where(name: 'Disable Drafts').destroy_all
  end

  def down

  end
end
