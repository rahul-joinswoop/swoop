class AddHqLocationTypeToLincoln < SeedMigration::Migration

  def up
    lincoln = Company.find_by!(name: 'Lincoln')
    hq = LocationType.find_or_create_by(name: 'HQ')
    CompaniesLocationType.create!(company: lincoln,
                                  location_type: hq)
  end

  def down
  end

end
