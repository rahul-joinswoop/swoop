class AddDefaultQuestionsToMtsServices < SeedMigration::Migration
  def up
    mts = Company.find_by_name('Motorcycle Towing Services, L.C.')

    mts_services_filtered =
      ServiceCode.standard_items.
      where.not(
        name: [
        ServiceCode::TOW,
        'Double Tow (Swap Out)', # MTS specific
        ServiceCode::BATTERY_JUMP,
        ServiceCode::FUEL_DELIVERY
        ],
        addon: true
      )

    default_questions = Question.where(question: ['Can your phone receive texts?', 'Will bike be attended at pick-up?'])

    mts_services_filtered.each do |service|
      CompanyServiceQuestion.where(company: mts, service_code: service).delete_all

      default_questions.each do |question|
        CompanyServiceQuestion.where(
          company: mts,
          service_code: service,
          question: question
        ).first_or_create
      end
    end
  end

  def down

  end
end
