# frozen_string_literal: true

class SuperFleetToSuperFleetManaged < SeedMigration::Migration

  def up
    Role.find_by(name: :super_fleet)&.update! name: Role::SUPER_FLEET_MANAGED
  end

  def down
    Role.find_by(name: Role::SUPER_FLEET_MANAGED)&.update! name: :super_fleet
  end

end
