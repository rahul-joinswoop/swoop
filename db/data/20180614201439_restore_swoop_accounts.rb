class RestoreSwoopAccounts < SeedMigration::Migration
  def up
    account_scope = Account.where.not(deleted_at: nil).where(name: "Swoop")

    # Companies with a deleted swoop account
    Company.joins(:accounts).merge(account_scope).find_each do |company|
      RestoreCompanySwoopAccounts.call(company: company)
    end
  end

  def down
  end
end
