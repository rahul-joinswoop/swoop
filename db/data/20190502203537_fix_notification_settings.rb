class FixNotificationSettings < SeedMigration::Migration
  def up
    delete_from_dispatchers_and_admins = "
      DELETE
        FROM user_notification_settings uns
        WHERE uns.notification_code IN ('eta_accepted', 'new_job')
      "

    ActiveRecord::Base.connection.execute(delete_from_dispatchers_and_admins)

    update_for_drivers = "
      UPDATE user_notification_settings
        SET notification_channels = '{Call, Text}'
        WHERE notification_code = 'dispatched_to_driver'
    "

    ActiveRecord::Base.connection.execute(update_for_drivers)
  end

  def down

  end
end
