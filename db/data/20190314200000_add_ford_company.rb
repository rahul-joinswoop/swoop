class AddFordCompany < SeedMigration::Migration

  def up
    ford = Company.find_by(name: "Ford")
    unless ford
      ford = CompanyService::Create.new(
        company_attributes:  {
          name: 'Ford',
          type: 'FleetCompany',
          in_house: false,
          dispatch_to_all_partners: true,
          phone: '+15551234567',
          accounting_email: 'lincoln+accounting@joinswoop.com',
          support_email: 'lincoln+support@joinswoop.com'
        },
        location_attributes: {
          address: '16800 Executive Plaza Dr, Dearborn, MI 48126',
          exact: true,
          lat: 42.374998,
          lng: -83.0087533
        },
        feature_ids: []
      ).call.company
    end
  end

  def down
  end

end
