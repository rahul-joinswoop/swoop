class InsertQuestions < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("INSERT INTO questions (question, name, order_num, created_at, updated_at) VALUES ('Alternate transportation?', 'alt-trans-bmw', COALESCE((SELECT MAX(order_num) FROM questions), 0) + 1, NOW(), NOW())")
    ActiveRecord::Base.connection.execute("INSERT INTO answers (question_id, answer, created_at, updated_at) VALUES ((SELECT id FROM questions WHERE name = 'alt-trans-bmw'), 'Yes', NOW(), NOW())")
    ActiveRecord::Base.connection.execute("INSERT INTO answers (question_id, answer, created_at, updated_at) VALUES ((SELECT id FROM questions WHERE name = 'alt-trans-bmw'), 'No', NOW(), NOW())")

    ActiveRecord::Base.connection.execute("INSERT INTO questions (question, name, order_num, created_at, updated_at) VALUES ('Trip interruption?', 'trip-interrupt', COALESCE((SELECT MAX(order_num) FROM questions), 0) + 1, NOW(), NOW())")
    ActiveRecord::Base.connection.execute("INSERT INTO answers (question_id, answer, created_at, updated_at) VALUES ((SELECT id FROM questions WHERE name = 'trip-interrupt'), 'Yes', NOW(), NOW())")
    ActiveRecord::Base.connection.execute("INSERT INTO answers (question_id, answer, created_at, updated_at) VALUES ((SELECT id FROM questions WHERE name = 'trip-interrupt'), 'No', NOW(), NOW())")

    ActiveRecord::Base.connection.execute("INSERT INTO questions (question, name, order_num, created_at, updated_at) VALUES ('Tow Pin present?', 'pin-present', COALESCE((SELECT MAX(order_num) FROM questions), 0) + 1, NOW(), NOW())")
    ActiveRecord::Base.connection.execute("INSERT INTO answers (question_id, answer, created_at, updated_at) VALUES ((SELECT id FROM questions WHERE name = 'pin-present'), 'Yes', NOW(), NOW())")
    ActiveRecord::Base.connection.execute("INSERT INTO answers (question_id, answer, created_at, updated_at) VALUES ((SELECT id FROM questions WHERE name = 'pin-present'), 'No', NOW(), NOW())")
  end

  def down
  end
end
