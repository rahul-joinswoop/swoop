class AddSurveyBackgroundSettingToCentury < SeedMigration::Migration
  def up
    company = Company.find_by name: "21st Century Insurance"
    if company
      Setting.create(company: company, key: "Survey Background", value: "https://s3-us-west-1.amazonaws.com/joinswoop.static/tfc_logo_background.png" )
    end
  end

  def down

  end
end
