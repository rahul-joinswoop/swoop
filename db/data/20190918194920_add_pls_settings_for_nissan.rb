class AddPlsSettingsForNissan < SeedMigration::Migration
  def up
    name = 'Nissan'
    nissan = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !nissan # to support dev envs
      nissan = FleetCompany.init_managed(name).company
    end

    PCC::NewClientSetup::Organizer.call(
      company: nissan,
      policy_lookup_client_settings: {
        Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE => 'nissan',
        Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => [PCC::Policy::VIN, PCC::Policy::MANUAL],
        Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE => [],
      }
    )
  end

  def down

  end
end
