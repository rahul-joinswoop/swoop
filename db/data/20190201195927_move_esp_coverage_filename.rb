class MoveEspCoverageFilename < SeedMigration::Migration

  def up
    lincoln = Company.find_by(name: 'Lincoln')

    [
      'lincoln_esp_99996',
      'lincoln_esp_99990',
      'lincoln_esp_99993',
      'lincoln_esp_99997',
      'lincoln_esp_99999',
      'lincoln_esp_99989',
      'lincoln_esp_99991',
      'lincoln_esp_99998',
      'lincoln_esp_99988',
      'lincoln_esp_99995',
      'lincoln_esp_99992',
      'lincoln_esp_99994',
      'lincoln_esp_99987',
      'lincoln_esp_99986',
    ].each do |code|

      program = ClientProgram.find_by(code: code,
                                      company: lincoln)
      program.update_columns(coverage_filename: 'lincoln_esp')
    end
  end

  def down
  end

end
