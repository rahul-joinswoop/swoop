class AddSubscriptionStatuses < SeedMigration::Migration
  NEW_SUBSCRIPTION_STATUSES = [
    'Subscribed - Free',
    'Subscribed - Custom',
    'Inactive',
    'Pending Cancelation',
    'Options',
  ]

  def up
    NEW_SUBSCRIPTION_STATUSES.each do |status_name|
      SubscriptionStatus.create!(name: status_name)
    end
  end

  def down
  end
end
