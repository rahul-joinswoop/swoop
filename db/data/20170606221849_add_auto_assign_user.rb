class AddAutoAssignUser < SeedMigration::Migration
  def up
    sad=User.find_by(username:'swoop-auto-dispatch')
    if !sad
      User.create!(username:'swoop-auto-dispatch',
                   email:'swoopdispatch+auto@joinswoop.com',
                   first_name:'Swoop',
                   last_name:'Dispatch',
                   company:Company.swoop,
                   password:SecureRandom.hex(15))
    end
  end

  def down

  end
end
