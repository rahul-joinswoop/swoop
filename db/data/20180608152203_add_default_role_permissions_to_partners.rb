class AddDefaultRolePermissionsToPartners < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION addDefaultRolePermissionsToPartners() RETURNS void AS
        $BODY$
        DECLARE
            _role_admin_id integer := (SELECT id FROM roles WHERE name = 'admin' LIMIT 1);
            _role_dispatcher_id integer := (SELECT id FROM roles WHERE name = 'dispatcher' LIMIT 1);
            _partner_row RECORD;
        BEGIN
            FOR _partner_row IN SELECT id FROM companies WHERE type = 'RescueCompany' and deleted_at IS NULL
            LOOP

              INSERT INTO company_role_permissions (permission_type, company_id, role_id, created_at, updated_at)
              SELECT 'charge', _partner_row.id, _role_admin_id, now(), now()
              WHERE NOT EXISTS(
                SELECT id FROM company_role_permissions
                WHERE
                  permission_type = 'charge' AND
                  company_id = _partner_row.id AND
                  role_id = _role_admin_id
              );

              INSERT INTO company_role_permissions (permission_type, company_id, role_id, created_at, updated_at)
              SELECT 'charge', _partner_row.id, _role_dispatcher_id, now(), now()
              WHERE NOT EXISTS(
                SELECT id FROM company_role_permissions
                WHERE
                  permission_type = 'charge' AND
                  company_id = _partner_row.id AND
                  role_id = _role_dispatcher_id
              );

              INSERT INTO company_role_permissions (permission_type, company_id, role_id, created_at, updated_at)
              SELECT 'refund', _partner_row.id, _role_admin_id, now(), now()
              WHERE NOT EXISTS(
                SELECT id FROM company_role_permissions
                WHERE
                  permission_type = 'refund' AND
                  company_id = _partner_row.id AND
                  role_id = _role_admin_id
              );
            END LOOP;
        END;

        $BODY$
        LANGUAGE plpgsql;

        SELECT * FROM addDefaultRolePermissionsToPartners();

        DROP FUNCTION addDefaultRolePermissionsToPartners();
    }
  end

  def down; end # not needed
end
