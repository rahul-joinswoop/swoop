class AddJimsClassTypes < SeedMigration::Migration
  def up
    cats = [
      "Rig",
      "Bus",
      "Motorhome",
      "Exotic Car",
      "Winch Off",
      "Winch On"
    ].map do |name|
      VehicleCategory.find_or_create_by name: name
    end

    company = Company.find_by name: "Jim's Towing Service, Inc."
    if company
      cats.each do |cat|
        CompaniesVehicleCategory.find_or_create_by company_id: company.id, vehicle_category_id: cat.id
      end
    end
  end

  def down
  end
end
