class AddPlsAndCoverageSettingsForAstonMartin < SeedMigration::Migration
  def up
    name = 'Aston Martin'
    aston_martin = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !aston_martin # to support dev envs
      aston_martin = FleetCompany.init_managed(name).company
    end

    PCC::NewClientSetup::Organizer.call(
      company: aston_martin,
      policy_lookup_client_settings: {
        Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE => 'aston_martin',
        Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => [PCC::Policy::VIN, PCC::Policy::NAME_AND_ZIP, PCC::Policy::PHONE_NUMBER, PCC::Policy::MANUAL],
        Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE => [],
      }
    )

  end

  def down

  end
end
