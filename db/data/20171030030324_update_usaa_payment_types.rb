class UpdateUsaaPaymentTypes < SeedMigration::Migration
  def up
    coveredWarranty = CustomerType.find_or_create_by name: "Covered - Warranty"
    customerPay = CustomerType.find_or_create_by name: "Customer Pay"
    
    company = Company.find_by name: "USAA"
    if company
      CompaniesCustomerType.where(company_id:company.id, customer_type_id: customerPay.id).update_all(deleted_at: Time.now)
      CompaniesCustomerType.find_or_create_by company_id: company.id, customer_type_id: coveredWarranty.id
    end

  end

  def down

  end
end
