class RemoveTeslaNamedRescueProviderTags < SeedMigration::Migration
  def up
    tag_ids = Tag.where(name: 'Tesla').pluck(:id)

    RescueProvidersTag.joins(:rescue_provider).where(
      tag: tag_ids, rescue_providers: {
        company_id: Company.tesla.id
      }
    ).delete_all
    # delete_all ^ will not trigger AR callbacks, so no WS pushed
  end

  def down

  end
end
