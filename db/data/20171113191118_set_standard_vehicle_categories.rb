class SetStandardVehicleCategories < SeedMigration::Migration
  def up
    standard_vehicle_categories = [
      VehicleCategory::FLATBED,
      VehicleCategory::WHEEL_LIFT,
      VehicleCategory::MEDIUM_DUTY,
      VehicleCategory::LIGHT_DUTY,
      VehicleCategory::HEAVY_DUTY,
      VehicleCategory::SUPER_HEAVY,
      VehicleCategory::SERVICE_TRUCK
    ]

    VehicleCategory.where(name: standard_vehicle_categories).each do |vehicle_category|
      vehicle_category.is_standard = true

      vehicle_category.save!
    end
  end

  def down

  end
end
