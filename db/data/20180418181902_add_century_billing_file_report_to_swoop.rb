class AddCenturyBillingFileReportToSwoop < SeedMigration::Migration
  def up
    swoop = Company.swoop

    billing_file_report = Report.find_by_name('21st Century Billing File')

    if billing_file_report
      swoop.reports << billing_file_report unless swoop.reports.include?(billing_file_report)
    end
    
  end

  def down

  end
end
