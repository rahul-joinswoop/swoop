# frozen_string_literal: true

class AddSuperFleetRole < SeedMigration::Migration

  def up
    Role.init_all
  end

  def down
  end

end
