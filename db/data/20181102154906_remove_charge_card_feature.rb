class RemoveChargeCardFeature < SeedMigration::Migration
  def up
    CompaniesFeature.where(feature: Feature.find_by_name('Charge Card')).delete_all

    Feature.find_by_name('Charge Card').update!(deleted_at: Time.now)
  end

  def down

  end
end
