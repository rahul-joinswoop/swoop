class AddPolicyLookupTypesSettingsToClients < SeedMigration::Migration
  def up
    Company.not_deleted.find_by_name('Lincoln').tap do |company|
      pls_fields_required = Setting.where(
        company: company, key: Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES
      ).first_or_create!

      pls_fields_required.update!(
        value: [ PCC::Policy::VIN, PCC::Policy::NAME_AND_ZIP, PCC::Policy::MANUAL ]
      )
    end
  end

  def down

  end
end
