class FixAutoCalculateQuantityOnInvoicingLineItems < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("
      UPDATE invoicing_line_items
      SET auto_calculated_quantity = FALSE
      WHERE description = 'Storage' AND (estimated IS NOT TRUE)
    ")
  end

  def down

  end
end
