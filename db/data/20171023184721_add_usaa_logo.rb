class AddUsaaLogo < SeedMigration::Migration
  def up
    usaa=FleetCompany.find_by(name:'USAA')
    usaa.update_column :logo_url, 'https://s3-us-west-2.amazonaws.com/joinswoop.logos/USAA_Logo.png'
  end

  def down

  end
end
