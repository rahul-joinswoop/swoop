class UpdateNullRateAdditionsToMisc < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("update rate_additions set name='Misc' where name is null")
  end

  def down

  end
end
