class RemovePhoneSearchFromMcLaren < SeedMigration::Migration
  def up
    name = 'McLaren'
    mclaren = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !mclaren # to support dev envs
      mclaren = FleetCompany.init_managed(name).company
    end

    PCC::NewClientSetup::AddPolicyLookupTypesSetting.call(
      company: mclaren,
      policy_lookup_client_settings: {
        Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => [PCC::Policy::VIN, PCC::Policy::NAME_AND_ZIP, PCC::Policy::MANUAL],
      }
    )
  end

  def down

  end
end
