class AddFleetClassTypeFeature < SeedMigration::Migration
  def up
    Feature.create!(
      name: "Fleet Class Type",
      description:"Enable Class Type to Fleets on JobForm and settings/configure",
      supported_by:[FleetCompany.name]
    )
  end

  def down

  end
end
