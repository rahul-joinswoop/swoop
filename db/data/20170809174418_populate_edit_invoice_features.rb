class PopulateEditInvoiceFeatures < SeedMigration::Migration
  def up
    disableView = Feature.find_by(name:"Disable driver view invoice")
    disableEdit = Feature.find_by(name:"Disable driver edit invoice")
  
    companiesNotShowingInvoices = Company.find_by_sql("select * from companies 
      where id not in (
        select companies.id 
          from companies 
            left join companies_features on companies_features.company_id = companies.id 
            left join features on companies_features.feature_id=features.id 
          where features.name = 'Show Drivers Invoice' 
            and companies.type='RescueCompany'
        ) 
        and type='RescueCompany';")

    companiesNotShowingEditInvoices = Company.find_by_sql("select * from companies 
      where id not in (
        select companies.id 
          from companies 
            left join companies_features on companies_features.company_id = companies.id 
            left join features on companies_features.feature_id=features.id 
          where features.name = 'Show Drivers Edit Invoice' 
            and companies.type='RescueCompany'
        ) 
        and type='RescueCompany';")


    companiesNotShowingInvoices.each do |company|
      ActiveRecord::Base.connection.execute("INSERT INTO companies_features (company_id, feature_id, created_at, updated_at) VALUES (#{company.id},#{disableView.id},NOW(),NOW())")
    end

    companiesNotShowingEditInvoices.each do |company|
      ActiveRecord::Base.connection.execute("INSERT INTO companies_features (company_id, feature_id, created_at, updated_at) VALUES (#{company.id},#{disableEdit.id},NOW(),NOW())")
    end

  end

  def down

  end
end
