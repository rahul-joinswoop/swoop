class FixMissingRescueProviders < SeedMigration::Migration
  def up
    # Sync fleet managed companies on all dispatchable sites that are missing some
    # that have been added for the company.
    providers_with_messed_up_sites_ids.each do |id|
      provider = RescueCompany.find(id)
      provider.sites.each do |site|
        SiteService::SyncRescueProviders.new(site).call if site.dispatchable?
      end
    end

    # Ensure that Swoop is enabled for all dispatchable sites.
    add_swoop_to_dispatchable_sites!
  end

  def down
  end

  private

  def companies_with_multiple_sites_ids
    ids = []
    Company.connection.select_all(<<~SQL
      SELECT companies.id FROM (
        SELECT companies.id AS company_id, COUNT(*) AS count
        FROM companies
        JOIN sites ON sites.company_id = companies.id
        WHERE companies.type = 'RescueCompany'
          AND sites.dispatchable = true
        GROUP BY companies.id
      ) AS companies_with_counts
      JOIN companies ON companies.id = company_id
      WHERE count > 1
    SQL
    ).each do |row|
      ids << row["id"]
    end
    ids
  end

  def providers_with_messed_up_sites_ids
    provider_counts = {}
    companies_with_multiple_sites_ids.in_groups_of(100, false).each do |company_ids|
      RescueProvider.connection.select_all(<<~SQL
        SELECT provider_id, site_id, COUNT(*) AS count
        FROM rescue_providers
        WHERE provider_id IN (#{company_ids.join(',')})
        GROUP BY provider_id, site_id
      SQL
      ).each do |row|
        counts = provider_counts[row["provider_id"]]
        unless counts
          counts = []
          provider_counts[row["provider_id"]] = counts
        end
        counts << row["count"]
      end
    end

    problematic_ids = []
    provider_counts.each do |provider_id, counts|
      problematic_ids << provider_id if counts.uniq.size > 1
    end
    problematic_ids
  end

  def add_swoop_to_dispatchable_sites!
    swoop = Company.swoop
    Site.find_by_sql(<<~SQL
      SELECT sites.*
      FROM sites
      LEFT OUTER JOIN rescue_providers
        ON rescue_providers.site_id = sites.id AND rescue_providers.company_id = #{swoop.id}
      WHERE sites.dispatchable = true
        AND rescue_providers.id IS NULL
    SQL
    ).each do |site|
      site.rescue_providers.create!(provider: site.company, company: swoop)
    end
  end
end
