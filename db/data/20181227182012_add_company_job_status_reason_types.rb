class AddCompanyJobStatusReasonTypes < SeedMigration::Migration

  def up
    FleetCompany.init_all_isscs
    CompanyJobStatusReasonType.init_all
  end

  def down
  end

end
