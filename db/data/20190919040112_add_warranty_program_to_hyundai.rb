class AddWarrantyProgramToHyundai < SeedMigration::Migration
  def up
    name = 'Hyundai'
    hyundai = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !hyundai # to support dev envs
      hyundai = FleetCompany.init_managed(name).company
    end

    # Create HYA (Hyundai Mobilitätsgarantie Neufahrzeuge) ClientProgram
    hyundai_hya = ClientProgram.where(
      name: 'Hyundai Mobilitätsgarantie Neufahrzeuge (HYA)',
      code: 'HYA',
      client_identifier: 'hya',
      company_id: hyundai.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagen-Mobilitätsgarantie EZ ab 01.01.2013 und bis 31.12.2017"
    ).first_or_create!

    # Create HYE (Hyundai Mobilitätsgarantie Neufahrzeuge) ClientProgram
    hyundai_hye = ClientProgram.where(
      name: 'Hyundai Mobilitätsgarantie Neufahrzeuge (HYE)',
      code: 'HYE',
      client_identifier: 'hye',
      company_id: hyundai.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagen-Mobilitätsgarantie EZ ab 01.01.2018"
    ).first_or_create!

    # Create HYW (Hyundai Mobilitätsgarantie Inspektion) ClientProgram
    hyundai_hyw = ClientProgram.where(
      name: 'Hyundai Mobilitätsgarantie Inspektion (HYW)',
      code: 'HYW',
      client_identifier: 'hyw',
      company_id: hyundai.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Langzeit-Mobilitätsgarantie | Inspektionen ab 01.01.2018"
    ).first_or_create!

    # Create HYP (Hyundai Premium Assistance) ClientProgram
    hyundai_hyp = ClientProgram.where(
      name: 'Hyundai Premium Assistance (HYP)',
      code: 'HYP',
      client_identifier: 'hyp',
      company_id: hyundai.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagen-Mobilitätsgarantie für Modell Genesis | EZ ab 01.01.2014 und bis 31.12.2017"
    ).first_or_create!

    # Create HYG (Hyundai Premium Assistance ClientProgram
    hyundai_hyg = ClientProgram.where(
      name: 'Hyundai Premium Assistance (HYG)',
      code: 'HYG',
      client_identifier: 'hyg',
      company_id: hyundai.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagen-Mobilitätsgarantie für Modell Genesis | EZ ab 01.01.2018"
    ).first_or_create!

    # Create HYN (Hyundai Nutzfahrzeuge) ClientProgram
    hyundai_hyn = ClientProgram.where(
      name: 'Hyundai Nutzfahrzeuge (HYN)',
      code: 'HYN',
      client_identifier: 'hyn',
      company_id: hyundai.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Hyundai Neuwagen-Mobilitätsgarantie für Modell H-350 | EZ ab 01.01.2015 und bis 31.12.2017"
    ).first_or_create!

    # Create HYT (Hyundai Nutzfahrzeuge) ClientProgram
    hyundai_hyt = ClientProgram.where(
      name: 'Hyundai Nutzfahrzeuge (HYT)',
      code: 'HYT',
      client_identifier: 'hyt',
      company_id: hyundai.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Hyundai Neuwagen-Mobilitätsgarantie für Nutzfahrzeuge LCV (H 350) | EZ ab 01.01.2018"
    ).first_or_create!

  end

  def down

  end
end
