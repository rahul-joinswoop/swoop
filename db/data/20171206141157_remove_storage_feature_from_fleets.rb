class RemoveStorageFeatureFromFleets < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(
      "DELETE FROM companies_features
       WHERE company_id IN (SELECT id FROM companies WHERE type = 'FleetCompany')
       AND feature_id = (SELECT id FROM features WHERE name = 'Storage')"
    )

    ActiveRecord::Base.connection.execute(
      "DELETE FROM companies_services
       WHERE company_id IN (SELECT id FROM companies WHERE type = 'FleetCompany')
       AND service_code_id = (SELECT id FROM service_codes WHERE name = 'Storage')"
    )
  end

  def down

  end
end
