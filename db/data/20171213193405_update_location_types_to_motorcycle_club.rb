class UpdateLocationTypesToMotorcycleClub < SeedMigration::Migration
  def up
    location_type_names = [
      LocationType::BUSINESS,
      LocationType::DEALERSHIP,
      LocationType::HIGHWAY,
      LocationType::HOME,
      LocationType::IMPOUND_LOT,
      LocationType::INTERSECTION,
      LocationType::NON_DEALERSHIP_REPAIR_FACILITY,
      LocationType::OTHER_EXPLAIN_IN_NOTES,
      LocationType::PARKING_GARAGE,
      LocationType::PARKING_LOT,
      LocationType::POINT_OF_INTEREST,
      LocationType::RESIDENCE
    ]

    company = Company.find_by_name('Motorcycle Towing Services, L.C.')

    if company.present?
      company.location_types = []

      location_types_to_add = []

      location_type_names.each do |location_type_name|
        location_types_to_add << LocationType.where(name: location_type_name).first_or_create
      end

      company.location_types << location_types_to_add
    end
  end

  def down

  end
end
