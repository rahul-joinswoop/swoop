class BackfillGoaEmailPreference < SeedMigration::Migration
  def up
    conn = ActiveRecord::Base.connection
    User.
    where("email IS NOT NULL OR username IS NOT NULL").
    where("(SELECT COUNT(*) FROM job_status_email_preferences WHERE job_status_email_preferences.user_id = users.id AND job_status_email_preferences.job_status_id = #{JobStatus::GOA}) = 0").
    find_in_batches do |batch|
      batch.each do |user|
        conn.execute "INSERT INTO job_status_email_preferences (user_id, job_status_id, created_at, updated_at) VALUES (#{user.id}, #{JobStatus::GOA}, NOW(), NOW())"
      end
    end
  end

  def down
  end
end
