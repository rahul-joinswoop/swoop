class SetTantalumRatesLive < SeedMigration::Migration
  def up
    tantalum=Company.find_by(name:'Tantalum') || FleetCompany.init_managed(FleetCompany::TANTALUM).company

    tantalum_customer_account=tantalum.accounts.find_by(name:'Customer')
    
    base_rate=FlatRate.find_by(company:tantalum,account:tantalum_customer_account, service_code:nil)
    base_rate.live=true
    base_rate.save!
    
    tow_rate=FlatRate.find_by(company:tantalum,account:tantalum_customer_account,service_code:ServiceCode.find_by(name:'Tow'))
    tow_rate.live=true
    tow_rate.save!
  end

  def down

  end
end

