class DetachDuplicatedSwoopUsers < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("
      UPDATE users
      SET company_id = NULL
      WHERE
        company_id = #{Company.swoop_id}
        AND username IS NULL
        AND email IS NULL
    ")
  end

  def down
  end
end
