class AddPartnerColumnToFleetManagedFeature < SeedMigration::Migration
  def up
    Feature.create(
      name: Feature::ADD_PARTNER_COLUMN_TO_FLEET_MANAGED,
      description: Feature::DESCRIPTIONS[Feature::ADD_PARTNER_COLUMN_TO_FLEET_MANAGED]
    )
  end

  def down
    Feature.find_by_name('Add Partner Column to Fleet Managed').delete
  end
end
