class SetAuctionVarsForUsaaAndFleetResponse < SeedMigration::Migration

  def update_setting(company,setting,value)
    setting_row=Setting.find_by(company:company,key:setting)
    if setting_row
      setting_row.value=value
      setting_row.save!
    else
      Setting.create(company:company,key:setting,value:value)
    end
  end

  def up

    max_candidate_eta='Auction Max Candidate ETA'
    max_bidders='Auction Max Bidders'
    
    company=Company.find_by(name:'USAA')
    update_setting(company,max_candidate_eta,75)
    update_setting(company,max_bidders,15)

    company=Company.find_by(name:'Fleet Response')
    update_setting(company,max_candidate_eta,75)

    
    
  end

  def down

  end
end
