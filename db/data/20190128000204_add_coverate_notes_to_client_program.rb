class AddCoverateNotesToClientProgram < SeedMigration::Migration

  def up
    lincoln = Company.find_by(name: 'Lincoln')

    [
      ['lincoln_black_label', 'Offer tow to nearest Black Label dealer first (BL in Dealer Name)'],
      ['lincoln_major_daily_rental', 'Not covered for roadside service or accident tows'],
      ['lincoln_ford_company_vehicle', 'If within 35 miles, tow to WHQ Garage: 4751 Mercury Drive, Dearborn, MI 48126. Otherwise tow to nearest dealership'],
    ].each do |code, notes|
      program = ::ClientProgram.find_by(code: code,
                                        company_id: lincoln.id)
      program.update_columns(coverage_notes: notes)
    end
  end

  def down
  end

end
