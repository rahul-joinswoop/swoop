class AddClassTypesToJimsTow < SeedMigration::Migration
  def up
    cats = [
      "Zone 3",
      "Zone 5",
      "Zone 6",
      "Zone 7",
      "Zone 8",
      "Zone 9",
      "Zone 11",
      "Zone 12",
      "Zone 13",
      "Zone 24",
      "Zone 37",
      "Zone 45",
      "Zone 54"
    ].map do |name|
      VehicleCategory.find_or_create_by name: name
    end

    company = Company.find_by name: "Jim's Towing Service, Inc."
    if company
      cats.each do |cat|
        CompaniesVehicleCategory.find_or_create_by company_id: company.id, vehicle_category_id: cat.id
      end
    end
  end

  def down
  end
end
