class AddDealershipLocationTypeToAllCompanies < SeedMigration::Migration

  def up
    dealership = LocationType.find_by!(name: "Dealership")
    Company.preload(:location_types).find_in_batches(batch_size: 50) do |batch|
      Company.transaction do
        batch.each do |company|
          next unless company.is_a?(RescueCompany) || company.is_a?(FleetCompany)
          next if company.location_types.include?(dealership)
          begin
            CompaniesLocationType.create!(company: company, location_type: dealership)
          rescue ActiveRecord::RecordNotUnique
            # ignore race condition
          end
        end
      end
    end
  end

  def down
  end

end
