class UniqueAjs < SeedMigration::Migration

  def up
    ActiveRecord::Base.connection.execute('select job_id,job_status_id,count(*) from audit_job_statuses group by 1,2 having count(*)>1').each do |res|
      job_id = res["job_id"]
      job_status_id = res["job_status_id"]

      ajss = AuditJobStatus.where(job_id: job_id, job_status_id: job_status_id).order(updated_at: :desc)

      first = nil
      ajss.each do |ajs|
        if first
          ajs.destroy
        else
          first = ajs
        end
      end
    end
  end

  def down
  end

end
