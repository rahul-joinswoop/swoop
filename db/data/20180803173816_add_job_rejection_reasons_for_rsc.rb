class AddJobRejectionReasonsForRsc < SeedMigration::Migration
  def up
    JobRejectReason.where(
      text: JobRejectReason::DO_NOT_ACCEPT_PAYMENT_TYPE
    ).first_or_create!

    JobRejectReason.where(
      text: JobRejectReason::PROPER_EQUIPMENT_NOT_AVAILABLE
    ).first_or_create!

    JobRejectReason.where(
      text: JobRejectReason::NO_LONGER_OFFER_SERVICE
    ).first_or_create!

    JobRejectReason.where(
      text: JobRejectReason::OUT_OF_MY_COVERAGE_AREA
    ).first_or_create!

    JobRejectReason.where(
      text: JobRejectReason::OTHER
    ).first_or_create!
  end

  def down; end
end
