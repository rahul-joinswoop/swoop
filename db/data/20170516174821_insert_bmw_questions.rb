class InsertBmwQuestions < SeedMigration::Migration
  def up
    bmw = ActiveRecord::Base.connection.execute("SELECT id FROM companies WHERE name = 'BMW NA'")

    if bmw.any?
      copy_farmers

      sql = "SELECT service_code_id FROM companies_services WHERE company_id = (SELECT id FROM companies WHERE name = 'BMW NA')"
      res = ActiveRecord::Base.connection.execute(sql)

      link_alt_transport(res)
      link_trip_interrupt(res)
      link_pin_present
    end
  end

  def down
  end

  def copy_farmers
    sql = "SELECT company_service_questions.service_code_id, company_service_questions.question_id
    FROM company_service_questions
    JOIN service_codes ON company_service_questions.service_code_id = service_codes.id
    WHERE company_id = (SELECT id FROM companies WHERE name = 'Farmers Insurance')
    AND company_service_questions.service_code_id IN (SELECT service_code_id FROM companies_services WHERE company_id = (SELECT id FROM companies WHERE name = 'BMW NA'))"

    res = ActiveRecord::Base.connection.execute(sql)

    inserts = res.map do |row|
      "((SELECT id from companies WHERE name = 'BMW NA'), #{row["service_code_id"]}, #{row["question_id"]}, NOW(), NOW())"
    end

    ActiveRecord::Base.connection.execute("INSERT INTO company_service_questions (company_id, service_code_id, question_id, created_at, updated_at) VALUES #{inserts.join(",")}")
  end

  def link_alt_transport(services)
    services.each do |row|
      ActiveRecord::Base.connection.execute("INSERT INTO company_service_questions (company_id, service_code_id, question_id, created_at, updated_at) VALUES ((SELECT id FROM companies WHERE name = 'BMW NA'), #{row["service_code_id"]}, (SELECT id FROM questions WHERE name = 'alt-trans-bmw'), NOW(), NOW())")
    end
  end

  def link_trip_interrupt(services)
    services.each do |row|
      ActiveRecord::Base.connection.execute("INSERT INTO company_service_questions (company_id, service_code_id, question_id, created_at, updated_at) VALUES ((SELECT id FROM companies WHERE name = 'BMW NA'), #{row["service_code_id"]}, (SELECT id FROM questions WHERE name = 'trip-interrupt'), NOW(), NOW())")
    end
  end

  def link_pin_present
    sql = "SELECT id FROM service_codes WHERE name IN ('Tow', 'Accident Tow', 'Tow if Jump Fails')"
    services = ActiveRecord::Base.connection.execute(sql)
    services.each do |row|
      ActiveRecord::Base.connection.execute("INSERT INTO company_service_questions (company_id, service_code_id, question_id, created_at, updated_at) VALUES ((SELECT id FROM companies WHERE name = 'BMW NA'), #{row["id"]}, (SELECT id FROM questions WHERE name = 'pin-present'), NOW(), NOW())")
    end
  end
end
