class AddBlacklistTagToTags < SeedMigration::Migration
  def up
    Tag.create do |tag|
      tag.name = 'Blacklist/Do Not Call'
    end
  end

  def down
    Tag.find_by(name: 'Blacklist/Do Not Call').destroy
  end
end
