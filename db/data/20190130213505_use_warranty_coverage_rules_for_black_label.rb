class UseWarrantyCoverageRulesForBlackLabel < SeedMigration::Migration

  def up
    lincoln = Company.find_by(name: 'Lincoln')
    program = ClientProgram.find_by(code: 'lincoln_black_label',
                                    company: lincoln)
    program.update_columns(coverage_filename: 'lincoln_warranty')
  end

  def down
  end

end
