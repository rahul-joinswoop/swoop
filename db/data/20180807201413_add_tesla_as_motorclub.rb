class AddTeslaAsMotorclub < SeedMigration::Migration
  def up
    FleetCompany.find_or_create_by(name: 'Tesla Motors Inc') do |company|
      company.support_email = 'roadsideadminna@tesla.com'
      company.accounting_email = 'roadsideadminna@tesla.com'
      company.phone = '+15102492508'
      company.issc_client_id = 'TSLA'
    end

    Location
      .find_or_create_by(address: '6800 dumbarton circle, fremont, CA 94555') do |loc|
        loc.address = '6800 dumbarton circle, fremont, CA 94555'
        loc.state = "CA"
        loc.city = "Freemont"
        loc.street = "6800 Dumbarton Circle"
        loc.zip = "94555"
        loc.exact = true
        loc.country = "US"
    end
  end

  def down
    FleetCompany.find_by(name: 'Tesla Motors Inc').destroy
  end
end
