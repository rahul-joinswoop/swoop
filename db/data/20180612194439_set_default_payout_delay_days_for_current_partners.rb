class SetDefaultPayoutDelayDaysForCurrentPartners < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    "UPDATE company_invoice_charge_fees SET payout_delay_days = 3"
  end

  def down; end
end
