class PopulateIsscLocationCompany < SeedMigration::Migration
  def up
    # clear out all unsupported issc_locations
    unsupported = FleetCompany.where(issc_client_id: Issc::UNSUPPORTED)
    IsscLocation.where(fleet_company: unsupported).update_all(deleted_at: Time.now)

    # populate the new issc_location with the company from the site
    IsscLocation.where.not(site: nil).find_each(batch_size: 100) do |loc|
      Rails.logger.debug "dealing with #{loc.id}"
      loc.company_id = loc.site.company_id
      loc.save!
    end
  end

  def down
  end
end
