class DeleteSmsDefaultRequestLocationOn < SeedMigration::Migration
  def up
    feature=Feature.find_by(name:'SMS_Default Request Location On')
    feature.deleted_at=Time.now
    feature.save!    
  end

  def down

  end
end
