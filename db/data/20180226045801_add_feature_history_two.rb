class AddFeatureHistoryTwo < SeedMigration::Migration
  def up
        Feature.create(name:"History 2.0",
                   description:"Adds History as it's own tab in details",
                   supported_by:['RescueCompany','FleetCompany',"SuperCompany"])
  end

  def down

  end
end
