class AddSmsConfirmOnSiteFeature < SeedMigration::Migration
  def up

    Feature.create!(name:Feature::SMS_CONFIRM_ON_SITE,
                   description:'Send an SMS follow up if the job exceeds the last ETA (of any type) to the customer asking if provider arrived',
                   supported_by:["FleetCompany"])
    company=Company.find_by(name:'USAA')
    company.add_features_by_name([Feature::SMS_CONFIRM_ON_SITE])
  end

  def down

  end
end
