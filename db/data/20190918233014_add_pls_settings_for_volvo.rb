class AddPlsSettingsForVolvo < SeedMigration::Migration
  def up
    name = 'Volvo'
    volvo = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !volvo # to support dev envs
      volvo = FleetCompany.init_managed(name).company
    end

    PCC::NewClientSetup::Organizer.call(
      company: volvo,
      policy_lookup_client_settings: {
        Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE => 'volvo',
        Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => [PCC::Policy::VIN, PCC::Policy::MANUAL],
        Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE => [],
      }
    )
  end

  def down

  end
end
