class UpdateCoverageNotesForEtr < SeedMigration::Migration
  def up
    updated_coverage_notes = 'During business hours, have the customer call Enterprise. 1-888-736-8287 option 1 for business rental or 2 for personal rental.

   **Do NOT collect payment from Customer if a job is created**'

    etr_name = 'Enterprise Truck Rental'

    etr = Company.find_by(name: etr_name, deleted_at: nil)

    if !etr
      etr = FleetCompany.init_managed(etr_name).company
    end

    client_program = ClientProgram.where(
      code: "not_covered",
      company_id: etr.id).first_or_create!

    client_program.update! coverage_notes: updated_coverage_notes

  end

  def down

  end
end
