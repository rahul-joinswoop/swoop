class ResetFleetResponseSentStatus < SeedMigration::Migration
  # This will move all invoices that were sent to FleetResponse
  # between 2018/Nov/13 and 06/Feb/2019 back to 'swoop_new' state,
  # in batches of 500. (in prod we have a total of ~3000 rows)
  def up
    API::AsyncRequest.select(:target_id, :id)
      .where(system: API::AsyncRequest::FLEET_RESPONSE_INVOICE)
      .where("created_at BETWEEN '2018-11-13 00:00:00' AND '2019-02-06 23:59:00'")
      .find_in_batches(batch_size: 500) do |group|
        invoice_ids = group.pluck(:target_id)

        Invoice.where(id: invoice_ids).update_all(state: 'swoop_new', updated_at: Time.now.utc)
      end
  end

  def down

  end
end
