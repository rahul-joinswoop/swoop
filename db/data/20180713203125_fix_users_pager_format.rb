class FixUsersPagerFormat < SeedMigration::Migration
  def up
    User.where("pager IS NOT NULL").find_each do |user|
      user.pager = Phonelib.parse(user.pager).e164
    end
  end

  def down
  end
end
