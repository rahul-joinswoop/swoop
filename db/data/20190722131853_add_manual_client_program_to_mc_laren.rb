class AddManualClientProgramToMcLaren < SeedMigration::Migration
  def up
    name = 'McLaren'
    mclaren = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !mclaren # to support dev envs
      mclaren = FleetCompany.init_managed(name).company
    end

    # Create Manual Entry ClientProgram
    not_covered = ClientProgram.where(
      name: 'Manual Entry',
      code: "mclaren_manual",
      company_id: mclaren.id,
      coverage_filename: 'mclaren_manual'
    ).first_or_create!

    # load services
    tow = ServiceCode.find_by_name(ServiceCode::TOW)
    fuel = ServiceCode.find_by_name(ServiceCode::FUEL)
    lock_out = ServiceCode.find_by_name(ServiceCode::LOCK_OUT)
    # this is not a standard service, so we create it in case it does not exist:
    reunite = ServiceCode.where(name: 'Reunite', is_standard: [false, nil], addon: false).first_or_create!

    # Create specific coverage notes for these services
    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: tow, notes: 'Use Enclosed Carriers equipment over 100mi / 160km'
    )

    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: fuel, notes: 'Two(2) gallon limit'
    )

    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: lock_out, notes: 'Must be Towed'
    )

    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: reunite, notes: 'Requires McLaren approval'
    )

    # Setup Policy Lookup System as the Policy provider app for McLaren
    policy_lookup_service_setting = Setting.where(
      company: mclaren, key: Setting::PCC_SERVICE_NAME
    ).first_or_create!

    policy_lookup_service_setting.update!(value: PCC::PCCService::POLICY_LOOKUP_SERVICE)

    # Add Policy Lookup Service client code to McLaren
    policy_lookup_service_client_code = Setting.where(
      company: mclaren, key: Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE
    ).first_or_create!

    policy_lookup_service_client_code.update! value: 'mclaren'

    # Add Policy Lookup Types to be used on Job Form
    pls_fields_required = Setting.where(
      company: mclaren, key: Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES
    ).first_or_create!

    pls_fields_required.update!(value: [PCC::Policy::MANUAL])

    # Set fields required for Coverage (only for Manual entry for now)
    pls_fields_required = Setting.where(
      company: mclaren, key: Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE
    ).first_or_create!

    pls_fields_required.update!(
      value: ['customer_lookup_type', 'year', 'service']
    )
  end

  def down

  end
end
