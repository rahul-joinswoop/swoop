class UpdateAutoAssignModelTypesToJobCandidate < SeedMigration::Migration
  KLASSES = ['Job::Candidate', 'Job::Candidate::Assignment', 'Job::Candidate::Truck', 'Job::Candidate::Site',
             'CandidateJob']

  def up
    KLASSES.each do |name|
      ModelType.where(name: name).first_or_create!
    end
  end

  def down
    KLASSES.each do |name|
      ModelType.find_by(name: name).destroy
    end
  end
end
