class AddAcceptReasonsToJobStatusReasonTypes < SeedMigration::Migration

  def up
    JobStatusReasonType.init_all
  end

  def down
  end

end
