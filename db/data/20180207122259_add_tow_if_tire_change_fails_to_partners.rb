class AddTowIfTireChangeFailsToPartners < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION addTowIfTireChangeFailsToPartners() RETURNS void AS
        $BODY$
        DECLARE
            rescue_company_row RECORD;
            tow_if_tire_change_fails_row service_codes%ROWTYPE;
        BEGIN
            SELECT * INTO tow_if_tire_change_fails_row FROM service_codes WHERE name = 'Tow if Tire Change Fails' AND addon != TRUE;
            FOR rescue_company_row IN SELECT id FROM companies WHERE type = 'RescueCompany'
            LOOP
              INSERT INTO companies_services (company_id, service_code_id, created_at, updated_at)
              SELECT rescue_company_row.id, tow_if_tire_change_fails_row.id, now(), now()
              WHERE NOT EXISTS(
                SELECT id FROM companies_services
                WHERE
                  company_id = rescue_company_row.id AND
                  service_code_id = tow_if_tire_change_fails_row.id
              );
            END LOOP;
        END;

        $BODY$
        LANGUAGE plpgsql;

        SELECT * FROM addTowIfTireChangeFailsToPartners();

        DROP FUNCTION addTowIfTireChangeFailsToPartners();
    }
  end

  def down

  end
end
