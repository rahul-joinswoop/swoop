class SetBlackLabelPriority < SeedMigration::Migration
  
  def up
    ClientProgram.where(code: "lincoln_black_label").update_all(priority: true)
  end

  def down
  end
  
end
