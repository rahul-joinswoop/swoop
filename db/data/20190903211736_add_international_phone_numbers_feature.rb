class AddInternationalPhoneNumbersFeature < SeedMigration::Migration
  def up
    additional_attr = {
      description: Feature::DESCRIPTIONS[Feature::INTERNATIONAL_PHONE_NUMBERS],
      supported_by: ['RescueCompany', 'FleetCompany', 'SuperCompany'],
    }

    feature = Feature.where(name: Feature::INTERNATIONAL_PHONE_NUMBERS).first_or_create!(additional_attr)

    feature.update!(additional_attr)
  end

  def down
    Feature.where(name: Feature::INTERNATIONAL_PHONE_NUMBERS).delete_all
  end
end
