class AddPaymentMethods < SeedMigration::Migration
  def up
    PaymentMethod::ALL_PAYMENT_METHODS.each do |payment_method_name|
      PaymentMethod.where(name: payment_method_name).first_or_create!
    end
  end

  def down

  end
end
