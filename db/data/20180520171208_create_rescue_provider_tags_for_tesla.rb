class CreateRescueProviderTagsForTesla < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION migrateTeslaStatusToRescueProviderTags() RETURNS void AS
      $BODY$
      DECLARE
          tesla_rescue_provider_row RECORD;
      BEGIN
          FOR tesla_rescue_provider_row IN SELECT * FROM rescue_providers
            WHERE company_id = (SELECT id FROM companies WHERE name = 'Tesla')
              AND deleted_at IS NULL
              AND trim(status) ILIKE ANY(array[
                'active','active ','Active','Active ',
                'Active; Email Intro sent','Active; Email Intro Sent','Active; Into Email Sent',
                'Active; intro email sent','Active; Intro email sent','Active; Intro Email sent',
                'Active ; Intro Email Sent','Active; Intro Email Sent','Active - Preferred',
                'Active -  Preferred','Active-Preferred','Active - Preferred; Intro Email Sent',
                'do not use','DO NOT USE','Intro email sent','Intro Email Sent',
                'Pending Training','Pending Training','Recruiting'])
          LOOP
            INSERT INTO rescue_providers_tags (rescue_provider_id, tag_id, created_at, updated_at)
            VALUES(
              tesla_rescue_provider_row.id,
            CASE
              WHEN trim(tesla_rescue_provider_row.status) ILIKE ANY(ARRAY[
                'active','active ','Active','Active ','Active; Email Intro sent','Active; Email Intro Sent',
                'Active; Into Email Sent','Active; intro email sent','Active; Intro email sent',
                'Active; Intro Email sent','Active ; Intro Email Sent','Active; Intro Email Sent']) THEN (SELECT id FROM tags WHERE company_id = (SELECT id FROM companies WHERE name = 'Tesla') AND name = 'Active')
              WHEN trim(tesla_rescue_provider_row.status) ILIKE ANY(ARRAY[
                'Active - Preferred','Active -  Preferred','Active-Preferred','Active - Preferred; Intro Email Sent']) THEN (SELECT id FROM tags WHERE company_id = (SELECT id FROM companies WHERE name = 'Tesla') AND name = 'Active - Preferred')
              WHEN trim(tesla_rescue_provider_row.status) ILIKE ANY(ARRAY['do not use','DO NOT USE']) THEN (SELECT id FROM tags WHERE company_id = (SELECT id FROM companies WHERE name = 'Tesla') AND name = 'Deactivated')
              WHEN trim(tesla_rescue_provider_row.status) ILIKE ANY(ARRAY[
                'Intro email sent','Intro Email Sent','Pending Training','Pending Training ','Recruiting']) THEN (SELECT id FROM tags WHERE company_id = (SELECT id FROM companies WHERE name = 'Tesla') AND name = 'Out of Network')
            END,
            now(),
            now());
          END LOOP;
      END;

      $BODY$
      LANGUAGE plpgsql;

      SELECT * FROM migrateTeslaStatusToRescueProviderTags();

      DROP FUNCTION migrateTeslaStatusToRescueProviderTags();
    }
  end

  def down

  end
end
