class CreateDemoFleetCompany < SeedMigration::Migration
  def up
    demo_company = FleetCompany.where(name: FleetCompany::DEMO_FLEET_COMPANY).not_deleted.first

    # current stg and prod envs are supposed to have this company, we just make sure it's flagged as demo.
    if demo_company
      demo_company.demo = true

      demo_company.save!
    else
      # ...but in case they don't (and for dev envs):
      # (data extracted from STG)
      demo_company = CompanyService::Create.new(
        company_attributes: {
          name: FleetCompany::DEMO_FLEET_COMPANY,
          phone: "+15047229063",
          support_email: Utils::SWOOP_OPERATIONS_EMAIL,
          accounting_email: Utils::SWOOP_OPERATIONS_EMAIL,
          type: "FleetCompany",
          in_house: true,
          demo: true,
          customer_type_ids: [],
          features: [],
          fleet_in_house_client_ids: [],
          fleet_managed_client_ids: [],
        },
        location_attributes: {
          address: "333 Bush Street, San Francisco, CA",
          city: "San Francisco",
          street: "333 Bush St",
          zip: "94104",
          country: "US",
          exact: true,
          lat: 37.7906078,
          lng: -122.4030875,
          place_id: "ChIJZ7LgkYmAhYARQnfC5WvRghw",
        },
        feature_ids: []
      ).call.company
    end
  end

  def down
  end
end
