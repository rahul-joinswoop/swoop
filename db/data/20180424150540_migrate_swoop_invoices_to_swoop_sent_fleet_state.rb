class MigrateSwoopInvoicesToSwoopSentFleetState < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("
      UPDATE invoicing_ledger_items invoices
      SET state = 'swoop_sent_fleet', updated_at = now()
      WHERE invoices.type = 'Invoice'
      AND invoices.state = 'swoop_downloaded_fleet'
    ")
  end

  def down

  end
end
