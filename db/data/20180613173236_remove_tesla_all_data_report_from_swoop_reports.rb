class RemoveTeslaAllDataReportFromSwoopReports < SeedMigration::Migration
  def up
    swoop = Company.find_by(name: 'Swoop')
    tesla_report = Report.find_by(name: 'Tesla All Data Report')

    if swoop && tesla_report
      cr = CompanyReport.find_by(company_id: swoop.id, report_id: tesla_report.id)
      cr.delete if cr
    end
  end

  def down
    swoop = Company.find_by(name: 'Swoop')
    tesla_report = Report.find_by(name: 'Tesla All Data Report')

    if swoop && tesla_report
      CompanyReport.create(company_id: swoop.id, report_id: tesla_report.id)
    end
  end
end
