class StripWhitespaceFromCompanyName < SeedMigration::Migration
  def up
    Company.where("name LIKE '% ' OR name LIKE ' %'").find_each do |company|
      company.update!(name: company.name.strip)
    end
  end

  def down

  end
end
