class CreateTantalumCompany < SeedMigration::Migration
  def up
    return if Company.find_by(name:'Tantalum')

    required_features = [
      Feature::REVIEW_NPS_SURVEYS,
      Feature::SMS_REVIEWS,
    ]

    feature_ids=[]
    required_features.each do |feature_name|
      feature_ids << Feature.find_by_name(feature_name).id
    end

    tantalum = CompanyService::Create.new(
      company_attributes:  {
        name: 'Tantalum',
        type: 'FleetCompany',
        in_house: false,
        dispatch_to_all_partners: true,
        phone: '+14155551234',
        accounting_email: 'tantalum+accounting@joinswoop.com',
        support_email: 'tantalum+support@joinswoop.com'
      },
      location_attributes: {
        address: '2 Finsbury Avenue, London EC2M 2PP, UK',
        exact: true,
        lat: 51.5194199,
        lng: -0.08523000000002412,
        place_id: 'ChIJ0VzgMqwcdkgRVSXd6GUjnjA'
      },
      feature_ids: feature_ids
    ).call.company

  end

  def down

  end
end
