class RemoveActiveSearchFeature < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("
      DELETE FROM companies_features WHERE feature_id IN(
        SELECT id FROM features WHERE name = 'Active Search')
    ")

    ActiveRecord::Base.connection.execute("
      DELETE FROM features WHERE name = 'Active Search'
    ")
  end

  def down

  end
end
