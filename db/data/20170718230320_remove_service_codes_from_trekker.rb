class RemoveServiceCodesFromTrekker < SeedMigration::Migration
  def up
    company = Company.find_by name: "Trekker Group"
    if company
      included_codes = ServiceCode.where("name IN (?)", [ServiceCode::TOW, ServiceCode::TRANSPORT])
      excluded_codes = company.service_codes - included_codes
      CompaniesService.
        where(company: company).
        where("service_code_id IN (?)", excluded_codes).
        each {|cs| cs.destroy }
    end
  end

  def down
  end
end
