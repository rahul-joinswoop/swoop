class AddFeatureConfirmJobDetails < SeedMigration::Migration
  def up
    # moved from db/migrate/20180416024022_add_feature_confirm_job_details.rb
    Feature.find_or_create_by(name:"Confirm Job Details") do |f|
      f.description = "Adds a confirmation modal to the create job modal"
      f.supported_by = ['RescueCompany','FleetCompany',"SuperCompany"]
    end
  end

  def down

  end
end
