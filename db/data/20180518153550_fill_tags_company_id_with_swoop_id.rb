class FillTagsCompanyIdWithSwoopId < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("
      UPDATE tags SET company_id = (SELECT id FROM companies WHERE name = 'Swoop')
    ")
  end

  def down

  end
end
