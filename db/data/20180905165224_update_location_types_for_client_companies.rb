class UpdateLocationTypesForClientCompanies < SeedMigration::Migration
  COMPANIES_TO_UPDATE = [
    'Automatic',
    'Go Insurance',
    'Root Insurance',
    'Harman Spark',
  ].freeze

  LOCATION_TYPES = [
    'Blocking Traffic',
    'Business',
    'Gated Community',
    'Highway',
    'Intersection',
    'Local Roadside',
    'Parking Garage',
    'Parking Lot',
    'Residence',
  ].freeze

  def up
    current_time = Time.now
    location_type_ids = LocationType.where(name: LOCATION_TYPES).pluck(:id)

    Company.where(name: COMPANIES_TO_UPDATE).each do |company|
      company_location_types = CompaniesLocationType
        .where(company_id: company.id)
        .where.not(location_type_id: location_type_ids)

      company_location_types.map { |loc| loc.update_attributes!(deleted_at: current_time) }
    end
  end

  def down
  end
end
