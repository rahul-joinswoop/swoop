class AddMcLarenRequiredFieldsForCoverage < SeedMigration::Migration
  def up
    name = 'McLaren'
    mclaren = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !mclaren # to support dev envs
      mclaren = FleetCompany.init_managed(name).company
    end

    PCC::NewClientSetup::AddRequiredFieldsForCoverageSetting.call(
      company: mclaren,
      policy_lookup_client_settings: {
        Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE => ['customer_lookup_type', 'service'],
      }
    )
  end

  def down

  end
end
