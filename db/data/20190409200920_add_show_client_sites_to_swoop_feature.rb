class AddShowClientSitesToSwoopFeature < SeedMigration::Migration
  def up
    Feature.create!(
      name: Feature::SHOW_CLIENT_SITES_TO_SWOOP,
      description: Feature::DESCRIPTIONS[Feature::SHOW_CLIENT_SITES_TO_SWOOP],
      supported_by: ["FleetCompany"]
    )
  end

  def down

  end
end
