class AddSouthernWreckerClassTypes < SeedMigration::Migration
  def up
    cats = [
      "Lowboy Zone 1 (15mi)",
      "Lowboy Zone 2 (25mi)",
      "Lowboy Zone 3 (35mi)",
      "Sliding Axle Zone 1 (15mi)",
      "Sliding Axle Zone 2 (25mi)",
      "Sliding Axle Zone 3 (35mi)",
      "Carrier Zone 1 (15mi)",
      "Carrier Zone 2 (25mi)",
      "Carrier Zone 3 (35mi)"
    ].map do |name|
      VehicleCategory.find_or_create_by name: name
    end

    company = Company.find_by name: "Southern Wrecker & Recovery"
    if company
      cats.each do |cat|
        CompaniesVehicleCategory.find_or_create_by company_id: company.id, vehicle_category_id: cat.id
      end
    end
  end

  def down
  end
end
