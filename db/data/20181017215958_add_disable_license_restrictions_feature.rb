class AddDisableLicenseRestrictionsFeature < SeedMigration::Migration
  def up
    Feature.create_with(description: "Disables License Restrictions on the job form", supported_by: '["SuperCompany, FleetCompany, RescueCompany"]').find_or_create_by(name: "Disable License Restrictions")
  end

  def down

  end
end
