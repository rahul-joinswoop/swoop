class AddFeatureReviewFeedQ1Filter < SeedMigration::Migration
  def up
    Feature.create(name:"Reviews Feed Q1 Filter",
      description:"Shows filter to filter reviews feed by NPS score",
      supported_by:['RescueCompany','FleetCompany',"SuperCompany"])
  end

  def down

  end
end
