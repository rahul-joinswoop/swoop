class AddFeatureClassTypeRequired < SeedMigration::Migration
  def up
    Feature.create(name:"Class Type Required",
                   description:"Require class type on the job form",
                   supported_by:['RescueCompany'])


  end

  def down

  end
end
