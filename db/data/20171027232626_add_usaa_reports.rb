class AddUsaaReports < SeedMigration::Migration
  def up

    company=Company.find_by(name:'USAA')

    ['USAA Activity Report','USAA CSAT','USAA ETA Variance Report'].each do |report_name|
      report=Report.find_by(name:report_name)

      existing=CompanyReport.find_by(company:company,report:report)
      if not existing
        CompanyReport.create(company:company,report:report)
      end
    end

  end

  def down

  end
end
