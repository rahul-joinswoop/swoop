class UpdateRankInClientProgramsForAstonMartin < SeedMigration::Migration
  def up
    name = 'Aston Martin'
    aston_martin = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !aston_martin # to support dev envs
      aston_martin = FleetCompany.init_managed(name).company
    end

    aston_martin_aml = ClientProgram.where(
      name: 'AML',
      code: 'aston_martin_aml',
      client_identifier: 'aml',
      company_id: aston_martin.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'aston_martin_aml'
    ).first_or_create!

    aston_martin_aml.update! rank: 1

    aston_martin_ams = ClientProgram.where(
      name: 'Service Contract',
      code: 'aston_martin_ams',
      client_identifier: 'ams',
      company_id: aston_martin.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'aston_martin_ams'
    ).first_or_create!

    aston_martin_ams.update! rank: 2

    PCC::NewClientSetup::AddRequiredFieldsForCoverageSetting.call(
      company: aston_martin,
      policy_lookup_client_settings: {
        Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE => ["customer_lookup_type", "service", "drop_location", "service_location"],
      }
    )
  end

  def down

  end
end
