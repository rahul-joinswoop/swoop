class AddVehicleTypes < SeedMigration::Migration
  def up
    cats = [
      "Front End Loader",
      "Rear Loader",
      "Roll-Off",
      "Concrete Mixer",
      "Tractor",
      "Tractor & Trailer",
      "Automated Side Loader",
      "Pump Truck",
      "Vacuum Truck",
      "Straight Truck",
      "Box Truck",
      "Stake Body"
    ].map do |name|
      VehicleType.find_or_create_by name: name
    end
  end

  def down

  end
end
