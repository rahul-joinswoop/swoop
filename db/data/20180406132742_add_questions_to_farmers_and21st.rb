class AddQuestionsToFarmersAnd21st < SeedMigration::Migration
  def up
    question = Question.where(
      question: 'Any risk factors?',
      order_num: 20 # give it some buffer so it will be shown as the last question
    ).first_or_create!

    answers = [
      'None',
      'Eldery',
      'Extreme temperatures',
      'Handicapped driver/passenger',
      'Long customer wait',
      'Medical condition',
      'Pregnant driver/passenger',
      'Unsafe location'
    ]

    answers.each do |answer|
      Answer.where(
        answer: answer,
        question: question
      ).first_or_create!
    end

    farmers = Company.farmers
    century = Company.find_by_name('21st Century Insurance')

    [century, farmers].each do |company|
      company.service_codes.where(addon: [nil, false]).each do |service|
        CompanyServiceQuestion.where(
          service_code: service,
          question: question,
          company: company
        ).first_or_create!
      end
    end
  end

  def down

  end
end
