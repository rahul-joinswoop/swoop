class ChangeRefererSources < SeedMigration::Migration
  def up
    advertisement = Referer.find_by(name: "Advertisement")
    advertisement&.soft_delete!

    Referer.create!(name: "Facebook")
    Referer.create!(name: "Instagram")
  end
end
