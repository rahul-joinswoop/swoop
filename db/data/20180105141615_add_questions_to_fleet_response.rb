class AddQuestionsToFleetResponse < SeedMigration::Migration
  def up
    fleet_response = Company.find_by_name('Fleet Response')

    questions = [
      'Keys present?',
      'Customer with vehicle?',
      'Which tire is damaged?',
      'Have a spare in good condition?',
      'Low clearance?',
      'Vehicle can be put in neutral?',
      'Vehicle 4 wheel drive?'
    ]

    tow_if_tire_changes_fail = ServiceCode.find_by_name(
      ServiceCode::TOW_IF_TIRE_CHANGE_FAILS
    )

    questions.each do |question|
      CompanyServiceQuestion.where(
        question: Question.find_by_question(question),
        company: fleet_response,
        service_code: tow_if_tire_changes_fail
      ).first_or_create
    end
  end

  def down

  end
end
