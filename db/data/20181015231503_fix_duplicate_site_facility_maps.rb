class FixDuplicateSiteFacilityMaps < SeedMigration::Migration
  def up
    SiteFacilityMap.distinct(:company_id).pluck(:company_id).each do |company_id|
      canonical = Set.new
      SiteFacilityMap.where(company_id: company_id).each do |site_facility_map|
        next unless site_facility_map.facility
        lat = site_facility_map.facility.lat
        lng = site_facility_map.facility.lng
        matched = canonical.detect { |location| location.matches?(latitude: lat, longitude: lng) }
        if matched
          issc_locations = IsscLocation.where(
            fleet_company_id: site_facility_map.fleet_company_id,
            company_id: site_facility_map.company_id,
            location_id: site_facility_map.facility_id,
          ).to_a
          site_facility_map.transaction do
            site_facility_map.update!(facility_id: matched.id)
            issc_locations.each { |issc_location| issc_location.update!(location_id: matched.id) }
          end
        else
          canonical << site_facility_map.facility
        end
      end
    end
  end
end
