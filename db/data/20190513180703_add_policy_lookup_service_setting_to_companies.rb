class AddPolicyLookupServiceSettingToCompanies < SeedMigration::Migration
  def up
    ['Lincoln', 'Enterprise Fleet Management'].each do |company_name|
      Company.not_deleted.find_by_name(company_name).tap do |company|
        policy_lookup_service_setting = Setting.where(company: company, key: Setting::PCC_SERVICE_NAME).first_or_create!

        policy_lookup_service_setting.update!(value: 'PolicyLookupSystem')
      end
    end

    ['Farmers Insurance', '21st Century Insurance'].each do |company_name|
      Company.not_deleted.find_by_name(company_name).tap do |company|
        policy_lookup_service_setting = Setting.where(company: company, key: Setting::PCC_SERVICE_NAME).first_or_create!

        policy_lookup_service_setting.update!(value: 'Farmers')
      end
    end
  end

  def down

  end
end
