class AddJobFormSiteRequiredFeature < SeedMigration::Migration
  def up
    feature = Feature.where(
      name: Feature::JOB_FORM_SITE_REQUIRED,
      description: Feature::DESCRIPTIONS[Feature::JOB_FORM_SITE_REQUIRED],
    ).first_or_create!

    feature.update! supported_by: ['FleetCompany']
  end

  def down
    Feature.where(name: Feature::JOB_FORM_SITE_REQUIRED).delete_all
  end
end
