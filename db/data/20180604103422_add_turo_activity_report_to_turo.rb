class AddTuroActivityReportToTuro < SeedMigration::Migration
  def up
    turo = Company.find_by(name: 'Turo')

    return if turo.nil?

    turo_report = Report.find_by!(name: 'Turo Activity Report')

    return if turo.reports.include?(turo_report)

    turo.reports << turo_report
  end
end
