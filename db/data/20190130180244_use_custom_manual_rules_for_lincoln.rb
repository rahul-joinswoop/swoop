class UseCustomManualRulesForLincoln < SeedMigration::Migration

  def up
    lincoln = Company.find_by(name: 'Lincoln')
    program = ClientProgram.find_by(code: 'lincoln_manual',
                                    company: lincoln)
    program.update_columns(coverage_filename: 'lincoln_manual')
  end

  def down
  end

end
