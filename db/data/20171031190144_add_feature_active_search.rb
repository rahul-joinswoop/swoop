class AddFeatureActiveSearch < SeedMigration::Migration
  def up
    Feature.create(name:"Active Search",
                   description:"Adds search to the active tab",
                   supported_by:['RescueCompany','FleetCompany',"SuperCompany"])

  end

  def down

  end
end
