# set these as default notification_code and notification_channel for rescue_users:
#
# admins and dispatchers:
# - new_job: 'Text'
# - eta_accepted: 'Text'
#
# drivers
# - dispatched_to_driver: 'Text
#
class SetUserNotificationSettingDefaults < SeedMigration::Migration
  def up
    # admins and dispatchers
    rescue_users_by_roles(['admin', 'dispatcher']).find_in_batches(batch_size: 50) do |users_group|
      users_group.each do |user|
        user.notification_settings.build([
          { notification_code: :new_job, notification_channels: ['Text'] },
          { notification_code: :eta_accepted, notification_channels: ['Text'] }
        ])

        user.save!
      end
    end

    # drivers
    rescue_users_by_roles(['driver']).find_in_batches(batch_size: 50) do |users_group|
      users_group.each do |user|
        user.notification_settings.build(
          [{ notification_code: :dispatched_to_driver, notification_channels: ['Text'] }]
        )

        user.save!
      end
    end
  end

  def down

  end

  def rescue_users_by_roles(roles)
    User.joins(:roles, :company).not_deleted.where(
      companies: { deleted_at: nil, type: 'RescueCompany', roles: { name: roles } }
    ).distinct
  end
end
