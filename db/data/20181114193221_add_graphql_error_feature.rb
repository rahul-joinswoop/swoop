class AddGraphQLErrorFeature < SeedMigration::Migration

  def up
    # create our feature
    f = Feature.find_or_create_by!(name: Feature::LEGACY_GRAPHQL_ERRORS)
    f.update! description: Feature::DESCRIPTIONS[Feature::LEGACY_GRAPHQL_ERRORS]

    # and enable it for any existing companies with oauth applications
    Doorkeeper::Application.where(owner_type:"Company").distinct(:owner).each do |app|
      app.owner.features << f
    end
  end

  def down
  end

end
