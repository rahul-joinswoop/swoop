class ChangeAlternateTransportationQuestionForBmw < SeedMigration::Migration
  def up
    bmw_alternate_question = Question.find_by_name('alt-trans-bmw')

    if bmw_alternate_question
      bmw_alternate_question.question = 'Alternate ride requested?'

      bmw_alternate_question.save!
    end
  end

  def down

  end
end
