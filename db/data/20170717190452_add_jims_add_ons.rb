class AddJimsAddOns < SeedMigration::Migration
  def up
    services = [
      "Special Straps",
      "Air Cushions",
      "Spreader Bars",
      "Skilled Labor",
      "Supervisor",
      "Standby Time",
      "Haz-mat Fee",
      "Permits",
      "Driveline Removal",
      "Driveline Installation",
      "Pilot Car",
      "Skip Loader",
      "Reach Forklift"
    ].map do |name|
      ServiceCode.find_or_create_by name: name, addon: true
    end

    company = Company.find_by name: "Jim's Towing Service, Inc."
    if company
      services.each do |service|
        CompaniesService.find_or_create_by company_id: company.id, service_code_id: service.id
      end
    end
  end

  def down
  end
end
