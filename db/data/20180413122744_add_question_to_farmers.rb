class AddQuestionToFarmers < SeedMigration::Migration
  def up
    farmers = Company.farmers

    question = Question.where(
      question: "Customer in a safe location?"
    ).first_or_create!

    ["Yes", "No"].each do |answer|
      Answer.where(
        answer: answer,
        question: question
      ).first_or_create!
    end

    farmers.service_codes.where(addon: [nil, false]).each do |service|
      CompanyServiceQuestion.where(
        company_id: farmers.id,
        service_code_id: service.id,
        question_id: question.id
      ).first_or_create!
    end
  end

  def down

  end
end
