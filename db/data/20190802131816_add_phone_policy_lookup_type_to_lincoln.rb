class AddPhonePolicyLookupTypeToLincoln < SeedMigration::Migration
  def up
    name = 'Lincoln'
    lincoln = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !lincoln # to support dev envs
      lincoln = FleetCompany.init_managed(name).company
    end

    setting = lincoln.settings.where(key: Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES).first_or_create!
    setting.update!(
      value: [
        PCC::Policy::VIN,
        PCC::Policy::PHONE_NUMBER,
        PCC::Policy::NAME_AND_ZIP,
        PCC::Policy::POLICY_NUMBER,
        PCC::Policy::MANUAL,
      ]
    )
  end

  def down

  end
end
