class AddLincolnClientPrograms < SeedMigration::Migration

  def up
    lincoln = Company.find_by(name: 'Lincoln')

    [
      ['Major Daily Rental', 'lincoln_major_daily_rental', 'lincoln_major_daily_rental', 1],
      ['Ford Company Vehicle', 'lincoln_ford_company_vehicle', 'lincoln_ford_company_vehicle', 2],
      ['Black Label', 'lincoln_black_label', 'lincoln_black_label', 3],
      ['Warranty', 'lincoln_warranty', 'lincoln_warranty', 4],
      ['ESP 99996', 'lincoln_esp_99996', 'esp', 5],
      ['ESP 99990', 'lincoln_esp_99990', 'esp', 6],
      ['ESP 99993', 'lincoln_esp_99993', 'esp', 7],
      ['ESP 99997', 'lincoln_esp_99997', 'esp', 8],
      ['ESP 99999', 'lincoln_esp_99999', 'esp', 9],
      ['ESP 99989', 'lincoln_esp_99989', 'esp', 10],
      ['ESP 99991', 'lincoln_esp_99991', 'esp', 11],
      ['ESP 99998', 'lincoln_esp_99998', 'esp', 12],
      ['ESP 99988', 'lincoln_esp_99988', 'esp', 13],
      ['ESP 99995', 'lincoln_esp_99995', 'esp', 14],
      ['ESP 99992', 'lincoln_esp_99992', 'esp', 15],
      ['ESP 99994', 'lincoln_esp_99994', 'esp', 16],
      ['ESP 99987', 'lincoln_esp_99987', 'esp', 17],
      ['ESP 99986', 'lincoln_esp_99986', 'esp', 18],
      ['Lincoln for Life', 'lincoln_for_life', 'lincoln_for_life', 19],
      ['Manual Entry', 'lincoln_manual', 'lincoln_warranty', 20],
    ].each do |name, code, coverage_filename, rank|
      program = ::ClientProgram.find_by(code: code,
                                        company_id: lincoln.id)
      if program.nil?
        begin
          program = ::ClientProgram.create!(name: name,
                                            code: code,
                                            company_id: lincoln.id)
        rescue ActiveRecord::RecordNotUnique
          program = ::ClientProgram.find_by(code: code,
                                            company_id: lincoln.id)
        end
      end
      program.update_columns(name: name,
                             coverage_filename: coverage_filename,
                             rank: rank)
    end
  end

  def down
  end

end
