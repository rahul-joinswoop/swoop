class SymptomsDataCleanup < SeedMigration::Migration
  def up
    @con = ActiveRecord::Base.connection.raw_connection

    job_stmt = 'UPDATE jobs SET symptom_id=$1 WHERE symptom_id = $2'
    @con.prepare('job_stmt', job_stmt)

    company_symptoms_stmt = 'UPDATE company_symptoms SET symptom_id = $1 WHERE symptom_id = $2'
    @con.prepare('company_symptoms_stmt', company_symptoms_stmt)

    clean_to_customer_unsafe
    clean_to_flat_tire
  end

  def clean_to_customer_unsafe
    # 'Customer feels unsafe driving' and 'Customer not safe driving' to 'Customer Unsafe'

    customer_unsafe = Symptom.where(name: Symptom::CUSTOMER_UNSAFE, is_standard: true).first_or_create

    customer_feels_unsafe_driving = Symptom.find_by_name('Customer feels unsafe driving')
    customer_not_safe_driving = Symptom.find_by_name('Customer not safe driving')

    if customer_feels_unsafe_driving.present? && customer_not_safe_driving.present?

      values = [{:value => customer_unsafe.id}, {:value => customer_feels_unsafe_driving.id}]
      @con.exec_prepared('job_stmt', values)
      @con.exec_prepared('company_symptoms_stmt', values)

      values = [{:value => customer_unsafe.id}, {:value => customer_not_safe_driving.id}]
      @con.exec_prepared('job_stmt', values)
      @con.exec_prepared('company_symptoms_stmt', values)

      # delete deprecated
      Symptom.delete customer_feels_unsafe_driving
      Symptom.delete customer_not_safe_driving
    end
  end

  def clean_to_flat_tire
    #####
    #  'Flat Tire' to 'Flat tire'
    #####

    flat_tire = Symptom.where(name: Symptom::FLAT_TIRE, is_standard: true).first_or_create

    old_casing_flat_tire = Symptom.find_by_name('Flat Tire')

    if old_casing_flat_tire.present?
      values = [{:value => flat_tire.id}, {:value => old_casing_flat_tire.id}]
      @con.exec_prepared('job_stmt', values)
      @con.exec_prepared('company_symptoms_stmt', values)

      # delete deprecated
      Symptom.delete old_casing_flat_tire
    end
  end

  def down
  end
end
