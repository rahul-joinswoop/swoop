class AddStripeAccountFeatureToPartners < SeedMigration::Migration
  def up
    Feature.create(
      name: 'Disable Stripe Integration',
      description:'The whole “link bank account” section is hidden from Partners on settings',
      supported_by:['RescueCompany']
    )
  end

  def down

  end
end
