class SetMobileMiniToInHouse < SeedMigration::Migration
  def up
    mm = Company.find_by(name: FleetCompany::MOBILE_MINI)
    mm.in_house = true
    mm.save!
  end

  def down
  end
end
