class DisableFleetDispatchToTruckFeature < SeedMigration::Migration
  def up
    fdtt=Feature.find_by(name:'Fleet Dispatch to Truck')
    fdtt.deleted_at=Time.now
    fdtt.save!
  end

  def down

  end
end
