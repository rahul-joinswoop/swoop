class AddLocationTypesToCompanies < SeedMigration::Migration
  def up
    ##
    # Feed LocationType with deprecated UI values
    ##

    default_location_types = [
      "Auto Body Shop",
      "Blocking Traffic",
      "Business",
      "Gated Community",
      "Highway",
      "Local Roadside",
      "Low Clearance",
      "Parking Garage",
      "Parking Lot",
      "Residence",
      "Point of Interest",
      "Intersection"
    ]

    tesla_custom_location_types = [
      "3rd Party Tire Shop",
      "Tesla Service Center",
      "Supercharger"
    ]

    enterprise_custom_location_types = [
      "Auction",
      "Service Center"
    ]

    farmers_exclude_location_types = [
      "Impound Lot",
      "Storage Facility",
      "Tow Company"
    ]

    bmw_location_type = [
      "Dealership"
    ]

    all_location_types =
      default_location_types +
      tesla_custom_location_types +
      farmers_exclude_location_types +
      enterprise_custom_location_types +
      bmw_location_type

    all_location_types.each do |location_type|
      LocationType.find_or_create_by(name: location_type)
    end

    ##
    # Create association between companies and the default values
    # Farmers does not include all, so we dont apply it to Farmers.
    ##
    default_location_types_in_db = LocationType.where(name: default_location_types + farmers_exclude_location_types)

    Company.where.not(name: 'Farmers Insurance').find_in_batches(batch_size: 100) do |companies|
      companies.each { |company| company.location_types << default_location_types_in_db }
    end


    ##
    # Create association between Farmers and custom location_types
    ##

    farmers_location_types_in_db = LocationType.where(name: (default_location_types))

    farmers = Company.find_by_name('Farmers Insurance')
    farmers.location_types << farmers_location_types_in_db


    ##
    # Create association between Tesla and custom location_types
    ##

    tesla_location_types_in_db = LocationType.where(name: tesla_custom_location_types)

    tesla = Company.find_by_name('Tesla')
    tesla.location_types << tesla_location_types_in_db


    ##
    # Create association between Enterprise and custom location_types
    ##

    enterprise_custom_location_types_in_db = LocationType.where(name: enterprise_custom_location_types)

    enterprise = Company.find_by_name('Enterprise')
    enterprise.location_types << enterprise_custom_location_types_in_db

    ##
    # Create association between BMW and custom location_type
    ##

    bmw_custom_location_types_in_db = LocationType.where(name: bmw_location_type)

    bmw = Company.find_by_name('BMW NA')
    bmw.location_types << bmw_custom_location_types_in_db
  end

  def down
    CompaniesLocationType.delete_all
    LocationType.delete_all
  end
end
