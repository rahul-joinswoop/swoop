class AddClientProgramToNissan < SeedMigration::Migration
  def up
    name = 'Nissan'
    nissan = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !nissan # to support dev envs
      nissan = FleetCompany.init_managed(name).company
    end

    # Create NIS (Nissan Assistance) ClientProgram
    nissan_nis = ClientProgram.where(
      name: 'Nissan Assistance (NIS)',
      code: 'NIS',
      client_identifier: 'nis',
      company_id: nissan.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagengarantie und Garantieverlängerungen"
    ).first_or_create!

    # Create NIL (Nissan Assistance) ClientProgram
    nissan_nil = ClientProgram.where(
      name: 'Nissan Assistance (NIL)',
      code: 'NIL',
      client_identifier: 'nil',
      company_id: nissan.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Inspektionsgarantie ab 01.04.2017"
    ).first_or_create!

  end

  def down

  end
end
