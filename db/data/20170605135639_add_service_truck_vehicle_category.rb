# frozen_string_literal: true

# Seed database with "Service Truck" as a Truck & Class Type
class AddServiceTruckVehicleCategory < SeedMigration::Migration
  def up
    vehicle_category = VehicleCategory.find_or_create_by(
      name: VehicleCategory::SERVICE_TRUCK, order_num: 7
    )

    return unless vehicle_category

    Company.find_each(batch_size: 100) do |company|
      next if company.vehicle_categories.include?(vehicle_category)

      # Create entry on the association table, no need to trigger WS events
      CompaniesVehicleCategory.create(
        company: company,
        vehicle_category: vehicle_category
      )
    end
  end

  def down
    vehicle_category =
      VehicleCategory.find_by(name: VehicleCategory::SERVICE_TRUCK)

    return unless vehicle_category

    CompaniesVehicleCategory.
      where(vehicle_category_id: vehicle_category.id).
      delete_all

    vehicle_category.delete
  end
end
