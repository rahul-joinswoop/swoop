class AddPolicyLookupServiceClientCodeToCompanies < SeedMigration::Migration
  def up
    Company.not_deleted.find_by_name('Lincoln').tap do |company|
      setting = Setting.where(company: company, key: Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE).first_or_create!

      setting.update! value: 'lincoln'
    end

    Company.not_deleted.find_by_name('Enterprise Fleet Management').tap do |company|
      setting = Setting.where(company: company, key: Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE).first_or_create!

      setting.update! value: 'efm'
    end
  end

  def down

  end
end
