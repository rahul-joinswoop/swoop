class SetFmcPriority < SeedMigration::Migration

  def up
    ClientProgram.where(code: "lincoln_ford_company_vehicle").update_all(priority: true)
  end

  def down
  end

end
