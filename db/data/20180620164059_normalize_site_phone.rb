class NormalizeSitePhone < SeedMigration::Migration
  def up
    Site.where("phone NOT LIKE '+%'").find_each do |site|
      site.phone_will_change!
      site.save
    end
  end

  def down
  end
end
