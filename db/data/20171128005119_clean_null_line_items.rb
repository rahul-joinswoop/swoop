class CleanNullLineItems < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute('update invoicing_line_items set deleted_at=now() where description is null and unit_price is null and net_amount = 0 and deleted_at is null')
  end

  def down

  end
end
