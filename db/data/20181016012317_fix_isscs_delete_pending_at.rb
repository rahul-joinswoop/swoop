class FixIsscsDeletePendingAt < SeedMigration::Migration
  def up
    ApplicationRecord.connection.update <<~SQL
      UPDATE isscs
      SET delete_pending_at = NULL
      WHERE deleted_at IS NOT NULL
        AND delete_pending_at IS NOT NULL
    SQL
  end
end
