# Adds all custom service codes from Fleets to their respective accounts
class AddCustomServiceCodesFromFleetToPartners < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def down

  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION addCustomServiceCodesToPartners( ) RETURNS void AS
        $BODY$
        DECLARE
            rescue_company_row RECORD;
            account_row RECORD;
            custom_service_from_fleet RECORD;

        BEGIN
            FOR rescue_company_row IN SELECT id FROM companies WHERE type = 'RescueCompany'
            LOOP
                FOR account_row IN
                    SELECT accounts.fleet_company_id FROM accounts
                    WHERE accounts.company_id = rescue_company_row.id
                LOOP

                    FOR custom_service_from_fleet IN
                        SELECT service_codes.id FROM service_codes
                        INNER JOIN companies_services ON companies_services.service_code_id = service_codes.id AND companies_services.company_id = account_row.fleet_company_id
                        WHERE service_codes.is_standard = 'f' OR service_codes.is_standard IS NULL

                        EXCEPT

                        SELECT service_codes.id FROM service_codes
                        INNER JOIN companies_services ON companies_services.service_code_id = service_codes.id AND companies_services.company_id = rescue_company_row.id
                    LOOP

                        INSERT INTO companies_services (company_id, service_code_id, created_at, updated_at)
                        VALUES (rescue_company_row.id, custom_service_from_fleet.id, now(), now());

                    END LOOP;
                END LOOP;
            END LOOP;
        END;

        $BODY$
        LANGUAGE plpgsql;

        SELECT * FROM addCustomServiceCodesToPartners();

        DROP FUNCTION addCustomServiceCodesToPartners();
    }
  end
end
