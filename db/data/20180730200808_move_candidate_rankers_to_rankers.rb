class MoveCandidateRankersToRankers < SeedMigration::Migration
  def up
    sql = "update rankers set type='Auction::WeightedSumRanker' where type='Auction::WeightedSumCandidateRanker'"
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
  end
end
