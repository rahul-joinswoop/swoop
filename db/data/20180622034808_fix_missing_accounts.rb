class FixMissingAccounts < SeedMigration::Migration
  def up
    RescueCompany.where(deleted_at: nil).find_each do |rc|
      swoop_accounts = rc.accounts.where(client_company_id: Company.swoop_id, deleted_at:nil)
      if swoop_accounts.length == 0
        puts "#{rc.name} has no live Swoop accounts"

        latest_swoop_account = rc.accounts.where(client_company_id: Company.swoop_id).last

        if latest_swoop_account.nil?
          puts "#{rc.name} has no Swoop accounts at all, creating new account"
          rc.accounts.create(name: "Swoop",
                             accounting_email: 'invoices@joinswoop.com',
                             primary_email: 'operations@joinswoop.com',
                             client_company: Company.swoop,
                             phone: '+18477966763'
                            )
          next
        end

        puts "Undeleting #{rc.name}'s Swoop account."

        latest_swoop_account.update(name: "Swoop", deleted_at: nil)
      end
    end
  end

  def down

  end
end
