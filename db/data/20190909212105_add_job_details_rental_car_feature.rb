class AddJobDetailsRentalCarFeature < SeedMigration::Migration
  def up
    feature = Feature.where(
      name: Feature::JOB_DETAILS_RENTAL_CAR,
      description: Feature::DESCRIPTIONS[Feature::JOB_DETAILS_RENTAL_CAR],
    ).first_or_create!

    feature.update! supported_by: ['SuperCompany']
  end

  def down

  end
end
