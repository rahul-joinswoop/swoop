class AddManualClientProgramToAstonMartin < SeedMigration::Migration
  def up
    name = 'Aston Martin'
    aston_martin = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !aston_martin # to support dev envs
      aston_martin = FleetCompany.init_managed(name).company
    end

    # Create Manual Entry ClientProgram
    not_covered = ClientProgram.where(
      name: 'Aston Martin Manual Entry',
      code: "aston_martin_manual",
      company_id: aston_martin.id,
      coverage_filename: 'aston_martin_manual',
      client_identifier: 'AML'
    ).first_or_create!

    # load services
    tow = ServiceCode.find_by_name(ServiceCode::TOW)
    tire_change = ServiceCode.find_by_name(ServiceCode::TIRE_CHANGE)
    fuel = ServiceCode.find_by_name(ServiceCode::FUEL)
    lock_out = ServiceCode.find_by_name(ServiceCode::LOCK_OUT)
    # this is not a standard service, so we create it in case it does not exist:
    reunite = ServiceCode.where(name: 'Reunite', is_standard: [false, nil], addon: false).first_or_create!

    # Create specific coverage notes for these services
    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: tow, notes: 'Use Enclosed Carriers equipment over 200mi / 321km'
    )

    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: tire_change, notes: 'Use Fix-a-Flat or tow to nearest dealer'
    )

    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: fuel, notes: 'Two(2) gallon limit'
    )

    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: lock_out, notes: 'Must be Towed'
    )

    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: reunite, notes: 'Requires approval'
    )

  end

  def down

  end
end
