class RenameFlatTireToTireChange < SeedMigration::Migration
  def up
    # updates DEV env because we already had FUEL DELIVERY in seeds.rb
    if Rails.env.development?
      existent_tire_change = ServiceCode.find_by_name(ServiceCode::TIRE_CHANGE)

      if existent_tire_change
        existent_tire_change.name = 'Legacy Tire Change'

        existent_tire_change.save!
      end
    end

    flat_tire = ServiceCode.where(name: 'Flat Tire').first

    if flat_tire
      flat_tire.name = ServiceCode::TIRE_CHANGE

      flat_tire.save!
    end
  end

  def down

  end
end
