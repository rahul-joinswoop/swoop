class FixCancelJobStatusReason < SeedMigration::Migration
  def up
    cancel_reason = JobStatusReasonType.where(key: :customer_found_alternate_solution).first

    return if cancel_reason.blank?

    cancel_reason.update! key: :cancel_customer_found_alternate_solution
  end

  def down; end
end
