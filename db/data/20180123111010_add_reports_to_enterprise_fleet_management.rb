class AddReportsToEnterpriseFleetManagement < SeedMigration::Migration
  def up
    fleet_company = Company.find_by_name('Enterprise Fleet Management')

    if fleet_company.present?
      FleetCompany::DEFAULT_REPORTS_FOR_MANAGED.each do |report_name|
        CompanyReport.where(
          company_id: fleet_company.id,
          report_id: Report.find_by_name!(report_name).id
        ).first_or_create
      end
    end
  end

  def down

  end
end
