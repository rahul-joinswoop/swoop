class FlagCompaniesWithUseCustomJobStatusReasons < SeedMigration::Migration

  def up
    companies = FleetCompany.where(issc_client_id: ['AGO', 'GCOAPI', 'QUEST', 'TSLA', 'ALLS', 'USAC', 'NSD', 'ADS', 'RDAM']).not_deleted
    companies.update_all(use_custom_job_status_reasons: true)

    Company.tesla.update!(use_custom_job_status_reasons: true)
  end

  def down
  end

end
