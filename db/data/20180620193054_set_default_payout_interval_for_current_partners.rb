class SetDefaultPayoutIntervalForCurrentPartners < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    "UPDATE company_invoice_charge_fees SET payout_interval = 'weekly'"
  end

  def down; end
end
