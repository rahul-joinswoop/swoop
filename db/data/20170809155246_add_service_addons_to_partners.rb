class AddServiceAddonsToPartners < SeedMigration::Migration
  def up
    new_service_codes_names = [
      'Accident Clean Up',
      'Release Fees',
      'After Hours Fee',
      'CC Processing Fees',
      'Ramps',
      'Reimbursement',
      'Second Truck',
      'Vehicle Release Fees'
    ]

    # make sure all service_codes exist and create if missing
    new_service_codes_names.each do |service_code_name|
      service_code = ServiceCode.find_or_create_by name: service_code_name, addon: true
    end

    service_codes = ServiceCode.where(name: new_service_codes_names)

    # add them to all current rescue_companies in batches
    RescueCompany.all.find_in_batches(batch_size: 200) do |group|
      inserts = []

      group.each do |rescue_company|
        inserts << service_codes.map do |service_code|
          "(#{rescue_company.id}, #{service_code.id}, NOW(), NOW())"
        end
      end

      ActiveRecord::Base.connection.execute "INSERT INTO companies_services (company_id, service_code_id, created_at, updated_at) VALUES #{inserts.flatten.join(",")}"
    end
  end

  def down
    new_service_codes_names = [
      'Accident Clean Up'
    ]

    RescueCompany.all.find_in_batches(batch_size: 200) do |group|
      group.each do |rescue_company|
        service_codes = ServiceCode.where(name: new_service_codes_names)

        service_codes.each do |service_code|
          company_service = CompaniesService.find_by company_id: rescue_company.id, service_code_id: service_code.id

          if company_service
            company_service.delete
          end
        end
      end
    end

    new_service_codes_names.each do |service_code_name|
      service_code = ServiceCode.find_by name: service_code_name, addon: true

      if service_code
        service_code.delete
      end
    end
  end
end
