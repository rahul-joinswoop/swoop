class FillCurrentAsyncRequestsWithFarmersCoverageSystem < SeedMigration::Migration
  def up
    API::AsyncRequest.where(system: nil).update_all(system: API::AsyncRequest::FARMERS_COVERAGE)
  end

  def down

  end
end
