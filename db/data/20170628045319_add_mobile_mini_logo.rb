class AddMobileMiniLogo < SeedMigration::Migration
  def up
    mm = Company.find_by(name: FleetCompany::MOBILE_MINI)
    mm.logo_url = "https://s3-us-west-2.amazonaws.com/joinswoop.logos/Mobile_Mini_Logo.png"
    mm.save!
  end

  def down
  end
end
