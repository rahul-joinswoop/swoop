class AddLookerFeature < SeedMigration::Migration
  def up
    Feature.create(name:"Looker",
                   description:"Enable Looker Reporting on the Insights tab",
                   supported_by:['RescueCompany','FleetCompany',"SuperCompany"])
  end

  def down

  end
end
