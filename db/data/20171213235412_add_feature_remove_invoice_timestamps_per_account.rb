class AddFeatureRemoveInvoiceTimestampsPerAccount < ActiveRecord::Migration[4.2]
  def up
      Feature.create(name:"Remove invoice timestamps per account",
                 description:"Allows partners to remove timestamps on their invoices",
                 supported_by:['RescueCompany'])
  end

  def down

  end
end
