class FixAddonTypo < SeedMigration::Migration
  def up
    typo = ServiceCode.find_by_name('Fuel Surchage')

    if typo
      typo.name = 'Fuel Surcharge'

      typo.save!
    end
  end

  def down

  end
end
