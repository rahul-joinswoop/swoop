class AddSwoopAutoDispatcherFeature < SeedMigration::Migration
  def up
    Feature.create(name:'Swoop Auto Dispatcher',
                   description:'Calculate truck with quickest ETA and assign job to owning company. This needs to be on for both fleet and partner to enable it',
                   supported_by:['FleetCompany','RescueCompany'])
  end

  def down
    
  end
end
