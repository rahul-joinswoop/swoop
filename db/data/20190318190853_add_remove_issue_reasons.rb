class AddRemoveIssueReasons < SeedMigration::Migration

  def up
    issue_claim = Issue.where(name: 'Claim', key: 'claim', is_standard: true).first_or_create!

    #### Update reasons to issue 'Claim'

    Reason.where(
      issue_id: issue_claim.id,
      name: ['Customer complaint', 'Other']
    ).update_all(deleted_at: Time.now)

    issue_claim.reasons << Reason.new(name: 'Reimbursement')
    issue_claim.reasons << Reason.new(name: 'Egregious Event')

    #### Create issue 'Customer Complaint' and add Reasons to it

    issue_customer_complaint = Issue.create!(
      name: 'Customer Complaint',
      key: 'customer_complaint',
      is_standard: true
    )

    issue_customer_complaint.reasons << Reason.new(name: 'Missed ETA')
    issue_customer_complaint.reasons << Reason.new(name: 'Long ETA')
    issue_customer_complaint.reasons << Reason.new(name: 'Long handle time')
    issue_customer_complaint.reasons << Reason.new(name: 'Redispatched')
    issue_customer_complaint.reasons << Reason.new(name: 'Service provider issue')
    issue_customer_complaint.reasons << Reason.new(name: 'Dispatcher')

    #### Create issue 'External Complaint' and add Reasons to it

    issue_external_complaint = Issue.create!(
      name: 'External Complaint',
      key: 'external_complaint',
      is_standard: true
    )

    issue_external_complaint.reasons << Reason.new(name: 'Missed ETA')
    issue_external_complaint.reasons << Reason.new(name: 'Long ETA')
    issue_external_complaint.reasons << Reason.new(name: 'Long handle time')
    issue_external_complaint.reasons << Reason.new(name: 'Redispatched')
    issue_external_complaint.reasons << Reason.new(name: 'Service provider issue')
    issue_external_complaint.reasons << Reason.new(name: 'Dispatcher')

    # update Swoop and fleet_in_houses that have Feature 'Issues' enabled with these two new Issues ^
    swoop = Company.swoop
    swoop.issues << issue_customer_complaint
    swoop.issues << issue_external_complaint

    # STG has only 6 fleet companies with this SQL
    # PROD has only 14 fleet companies with this SQL
    FleetCompany.not_deleted.joins(:features).where(features: { name: 'Issues' }).each do |fleet_company|
      fleet_company.issues << issue_customer_complaint
      fleet_company.issues << issue_external_complaint
    end
  end

  def down
  end

end
