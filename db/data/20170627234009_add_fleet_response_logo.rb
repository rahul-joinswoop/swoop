class AddFleetResponseLogo < SeedMigration::Migration
  def up
    fr = Company.find_by(name: FleetCompany::FLEET_RESPONSE)
    fr.logo_url = "https://s3-us-west-2.amazonaws.com/joinswoop.logos/fleet-services-10.png"
    fr.save!
  end

  def down
  end
end
