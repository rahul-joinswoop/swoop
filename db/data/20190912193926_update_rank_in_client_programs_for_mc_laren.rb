class UpdateRankInClientProgramsForMcLaren < SeedMigration::Migration
  def up
    name = 'McLaren'
    mclaren = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !mclaren # to support dev envs
      mclaren = FleetCompany.init_managed(name).company
    end

    mclaren_mcw = ClientProgram.where(
      name: 'MCW',
      code: 'mclaren_mcw',
      client_identifier: 'mcw',
      company_id: mclaren.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'mclaren_mcw'
    ).first_or_create!

    mclaren_mcw.update! rank: 1

    mclaren_mwt = ClientProgram.where(
      name: 'MWT',
      code: 'mclaren_mwt',
      client_identifier: 'mwt',
      company_id: mclaren.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'mclaren_mwt'
    ).first_or_create!

    mclaren_mwt.update! rank: 2

  end

  def down

  end
end
