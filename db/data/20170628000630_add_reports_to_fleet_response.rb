class AddReportsToFleetResponse < SeedMigration::Migration
  def up
    fr = Company.find_by(name: FleetCompany::FLEET_RESPONSE)
    fr.add_reports_by_name([
      Report::FLEET_ETA_VARIANCE,
      Report::FLEET_ACTIVITY,
      Report::FLEET_CSAT
    ])
  end

  def down
  end
end
