class AddBasicGroups < SeedMigration::Migration
  def up
    Group.create(name: Group::SWOOP_ONLY_CLIENT)
    Group.create(name: Group::SWOOP_PRIMARY_CLIENT)
    Group.create(name: Group::AGERO_ONLY_CLIENT)
    Group.create(name: Group::AGERO_PRIMARY_CLIENT)
    Group.create(name: Group::SWOOP_PRIMARY_PARTNER)
    Group.create(name: Group::AGERO_PRIMARY_PARTNER)
  end

  def down
  end
end
