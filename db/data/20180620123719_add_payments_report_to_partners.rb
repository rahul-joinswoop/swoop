class AddPaymentsReportToPartners < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION addPaymentsReportToAllPartners() RETURNS void AS
      $BODY$
      DECLARE
          _payments_report_id integer := (SELECT id from reports WHERE name = 'Payments Report' LIMIT 1);
          partner_row RECORD;
      BEGIN
          FOR partner_row IN SELECT id FROM companies where type = 'RescueCompany' and deleted_at IS NULL
          LOOP

            INSERT INTO company_reports(report_id, company_id, created_at, updated_at)
            VALUES(_payments_report_id, partner_row.id, now(), now());

          END LOOP;
      END;

      $BODY$
      LANGUAGE plpgsql;

      SELECT * FROM addPaymentsReportToAllPartners();

      DROP FUNCTION addPaymentsReportToAllPartners();
    }
  end

  def down
    CompanyReport.where(report_id: Report.find_by!(name: Report::PAYMENTS_REPORT)).delete_all
  end
end
