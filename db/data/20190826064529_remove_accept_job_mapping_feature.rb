class RemoveAcceptJobMappingFeature < SeedMigration::Migration
  def up
    Feature.find_by(
      name: 'Accept Job Map',
      description: 'Make Accept Job Map Feature'
    ).try(:destroy)
  end

  def down
    Feature.create!(
      name: 'Accept Job Map',
      description: 'Make Accept Job Map Feature'
    )
  end
end
