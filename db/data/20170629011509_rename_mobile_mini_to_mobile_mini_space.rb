class RenameMobileMiniToMobileMiniSpace < SeedMigration::Migration
  def up
    busted_mm = Company.find_by(name: "Mobile Mini")
    if busted_mm
      busted_mm.name = FleetCompany::MOBILE_MINI
      busted_mm.save!
    end
  end

  def down
  end
end
