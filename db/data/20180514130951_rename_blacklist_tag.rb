class RenameBlacklistTag < SeedMigration::Migration
  def up
    tag = Tag.find_by!(name: 'Blacklist/Do Not Call')
    tag.update(name: 'Do Not Call')
  end

  def down
    tag = Tag.find_by!(name: 'Do Not Call')
    tag.update(name: 'Blacklist/Do Not Call')
  end
end
