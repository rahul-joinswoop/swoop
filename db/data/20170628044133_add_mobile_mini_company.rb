class AddMobileMiniCompany < SeedMigration::Migration
  def up
    mm = Company.find_by(name: FleetCompany::MOBILE_MINI)
    FleetCompany.init_managed(FleetCompany::MOBILE_MINI) unless mm
  end

  def down
  end
end
