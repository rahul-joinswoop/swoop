class AddFeatureReviewFeed < SeedMigration::Migration
  def up
     Feature.create(name:"Review Feed",
       description:"Adds review tab",
       supported_by:['RescueCompany','FleetCompany',"SuperCompany"])

  end

  def down

  end
end
