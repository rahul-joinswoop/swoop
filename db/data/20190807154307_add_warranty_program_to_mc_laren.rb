class AddWarrantyProgramToMcLaren < SeedMigration::Migration
  def up
    name = 'McLaren'
    mclaren = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !mclaren # to support dev envs
      mclaren = FleetCompany.init_managed(name).company
    end

    # Create MCW (McLaren Warranty) ClientProgram
    mclaren_mcw = ClientProgram.where(
      name: 'MCW',
      code: 'mclaren_mcw',
      client_identifier: 'mcw',
      company_id: mclaren.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'mclaren_mcw'
    ).first_or_create!

    tow = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::TOW)
    mclaren_mcw.service_notes << ClientProgramServiceNotes.new(
      service_code: tow, notes: 'Use Enclosed Carriers equipment over 100mi / 160km. Costs over $2000 requires authorization.'
    )

    fuel_delivery = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::FUEL_DELIVERY)
    mclaren_mcw.service_notes << ClientProgramServiceNotes.new(
      service_code: fuel_delivery, notes: 'Two(2) gallon limit'
    )

    lock_out = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::LOCK_OUT)
    mclaren_mcw.service_notes << ClientProgramServiceNotes.new(
      service_code: lock_out, notes: 'Must be Towed'
    )

    # this is not a standard service, so we create it in case it does not exist:
    reunite = ServiceCode.where(name: 'Reunite', is_standard: [false, nil], addon: false).first_or_create!
    mclaren_mcw.service_notes << ClientProgramServiceNotes.new(
      service_code: reunite, notes: 'Requires McLaren approval'
    )
  end

  def down

  end
end
