class AddPlsAndCoverageSettingsForRootInsurance < SeedMigration::Migration
  def up
    name = 'Root Insurance'
    root_insurance = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !root_insurance # to support dev envs
      root_insurance = FleetCompany.init_managed(name).company
    end

    PCC::NewClientSetup::Organizer.call(
      company: root_insurance,
      policy_lookup_client_settings: {
        Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE => 'ric',
        Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => [PCC::Policy::VIN, PCC::Policy::NAME_AND_ZIP, PCC::Policy::POLICY_NUMBER, PCC::Policy::MANUAL],
        Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE => ['customer_lookup_type', 'service', 'drop_location', 'service_location'],
      }
    )

    not_covered = ClientProgram.where(
      name: 'Root Insurance RRS',
      code: "ric_rrs",
      client_identifier: 'rrs',
      company_id: root_insurance.id,
      coverage_filename: 'root_insurance_rrs'
    ).first_or_create!

    fuel_delivery = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::FUEL_DELIVERY)
    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: fuel_delivery, notes: '2 gallon fuel delivery limit'
    )

    winch_out = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::WINCH_OUT)
    not_covered.service_notes << ClientProgramServiceNotes.new(
      service_code: winch_out, notes: 'Covered if vehicle within 10 ft of paved surface'
    )
  end

  def down
    name = 'Root Insurance'
    root_insurance = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    ClientProgram.find_by(
      name: 'Root Insurance RRS',
      code: "ric_rrs",
      client_identifier: 'rrs',
      company_id: root_insurance.id,
    ).destroy
  end
end
