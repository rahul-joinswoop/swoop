class DeleteChoosePartnerMapFeature < SeedMigration::Migration
  def up
    feature=Feature.find_by(name:'Choose Partner Map')
    feature.deleted_at=Time.now
    feature.save!
    CompaniesFeature.where(feature:feature).delete_all
  end

  def down

  end
end
