class FixRscAddresses < SeedMigration::Migration

  def up
    FleetMotorClubJob.joins(issc_dispatch_request: :issc).where(isscs: {system: "rsc"}).preload(:service_location, :original_service_location, :drop_location).find_each do |job|
      fix_address(job.service_location)
      fix_address(job.original_service_location)
      fix_address(job.drop_location)
    end
  end

  def down
  end

  def fix_address(loc)
    if loc && loc.address.present? && loc.street.blank? && !loc.address.include?(loc.city)
      loc.street = loc.address
      loc.address = [loc.street, loc.city, [loc.state, loc.zip].join(" ").strip].join(', ')
      puts "  #{loc.address}"
      loc.save!
    end
  end

end
