class RemoveTeslaClaimIssueOneMoreTime < SeedMigration::Migration
  def up
    issue = Issue.find_by(name: "Claim")
    Company.tesla.issues.delete(issue) if issue
  end

  def down
  end
end
