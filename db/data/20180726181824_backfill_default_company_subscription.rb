class BackfillDefaultCompanySubscription < SeedMigration::Migration
  DEFAULT_STATUS_NAME = 'Network Only'.freeze

  def up
    # Populate default subscription on all companies
    id = SubscriptionStatus.find_by(name: DEFAULT_STATUS_NAME).id
    ApplicationRecord.
      execute_query("UPDATE companies SET subscription_status_id = ?", id)
  end

  def down

    ApplicationRecord.
      execute_query("UPDATE companies SET subscription_status_id = ?", id)
  end
end
