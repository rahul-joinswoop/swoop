class AddBodyShopCustomerTypeToTesla < SeedMigration::Migration
  def up
    customer_type = CustomerType.find_or_create_by name: "Body Shop"
    company = Company.find_by name: "Tesla"
    if company
      CompaniesCustomerType.find_or_create_by company_id: company.id, customer_type_id: customer_type.id
    end
  end

  def down
  end
end
