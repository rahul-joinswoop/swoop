class AddClientProgramToOpel < SeedMigration::Migration
  def up
    name = 'Opel'
    opel = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !opel # to support dev envs
      opel = FleetCompany.init_managed(name).company
    end

    # Create OAS (Opel Mobilservice) ClientProgram
    opel_oas = ClientProgram.where(
      name: 'Opel Mobilservice (OAS)',
      code: 'OAS',
      client_identifier: 'oas',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagengarantie EZ bis 31.12.2018"
    ).first_or_create!

    # Create OPN (Opel Mobilservice) ClientProgram
    opel_opn = ClientProgram.where(
      name: 'Opel Mobilservice (OPN)',
      code: 'OPN',
      client_identifier: 'opn',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Neuwagengarantie EZ ab 01.01.2019"
    ).first_or_create!

    # Create OAV (Opel Neufahrzeug Verlängerung OnStar) ClientProgram
    opel_oav = ClientProgram.where(
      name: 'Opel Neufahrzeug Verlängerung OnStar (OAV)',
      code: 'OAV',
      client_identifier: 'oav',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Anschlussgarantie ab 01.01.2018"
    ).first_or_create!

    # Create OPV (Opel Neufahrzeug Verlängerung OnStar) ClientProgram
    opel_opv = ClientProgram.where(
      name: 'Opel Neufahrzeug Verlängerung OnStar (OPV)',
      code: 'OPV',
      client_identifier: 'opv',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Anschlussgarantie ab 01.01.2019 - erste Leistungen ab 01.01.2020"
    ).first_or_create!

    # Create OFC (Opel Flex Care) ClientProgram
    opel_ofc = ClientProgram.where(
      name: 'Opel Flex Care (OFC)',
      code: 'OFC',
      client_identifier: 'ofc',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Anschlussgarantie bis EZ 31.12.2018"
    ).first_or_create!

    # Create OPC (Opel Flex Care) ClientProgram
    opel_opc = ClientProgram.where(
      name: 'Opel Flex Care (OPC)',
      code: 'OPC',
      client_identifier: 'opc',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Anschlussgarantie ab EZ 01.01.2019 - erste Leistungen ab 01.01.2020"
    ).first_or_create!

    # Create OAW/OAF (Opel Inspektionsgarantie & Verlängerungen für Flotten) ClientProgram
    opel_oaw = ClientProgram.where(
      name: 'Opel Inspektionsgarantie & Verlängerungen für Flotten (OAW/OAF)',
      code: 'OAW/OAF',
      client_identifier: 'oaw/oaf',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Vertragsabschluss ab 01.01.2018"
    ).first_or_create!

    # Create OVF (Opel Verlängerungen für Flotten) ClientProgram
    opel_ovf = ClientProgram.where(
      name: 'Opel Verlängerungen für Flotten (OVF)',
      code: 'OVF',
      client_identifier: 'ovf',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Vertragsabschluss bis 31.12.2017"
    ).first_or_create!

    # Create OG2 (Opel Gebrauchtwagengarantie) ClientProgram
    opel_og2 = ClientProgram.where(
      name: 'Opel Gebrauchtwagengarantie (OG2)',
      code: 'OG2',
      client_identifier: 'og2',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Vertragsabschluss bis 31.12.2017"
    ).first_or_create!

    # Create OAG (Opel Gebrauchtwagengarantie) ClientProgram
    opel_oag = ClientProgram.where(
      name: 'Opel Gebrauchtwagengarantie (OAG)',
      code: 'OAG',
      client_identifier: 'oag',
      company_id: opel.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'covered',
      coverage_notes: "Vertragsabschluss ab 01.01.2018"
    ).first_or_create!

  end

  def down

  end
end
