class AddAmsCoverageProgramToAstonMartin < SeedMigration::Migration
  def up
    name = 'Aston Martin'
    aston_martin = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !aston_martin # to support dev envs
      aston_martin = FleetCompany.init_managed(name).company
    end

    # Create AMS (Aston Martin Service Contract) ClientProgram
    aston_martin_ams = ClientProgram.where(
      name: 'Service Contract',
      code: 'aston_martin_ams',
      client_identifier: 'ams',
      company_id: aston_martin.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_filename: 'aston_martin_ams'
    ).first_or_create!

    tow = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::TOW)
    aston_martin_ams.service_notes << ClientProgramServiceNotes.new(
      service_code: tow, notes: 'Use Covered Flatbed equipment over 200mi / 321km.'
    )

    tire_change = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::TIRE_CHANGE)
    aston_martin_ams.service_notes << ClientProgramServiceNotes.new(
      service_code: tire_change, notes: 'Use Fix-a-Flat or tow to nearest dealer'
    )

    fuel_delivery = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::FUEL_DELIVERY)
    aston_martin_ams.service_notes << ClientProgramServiceNotes.new(
      service_code: fuel_delivery, notes: 'Two(2) gallon limit'
    )

    lock_out = ServiceCode.not_deleted.where(addon: false).find_by_name(ServiceCode::LOCK_OUT)
    aston_martin_ams.service_notes << ClientProgramServiceNotes.new(
      service_code: lock_out, notes: 'Must be towed to nearest dealer'
    )

    # this is not a standard service, so we create it in case it does not exist:
    reunite = ServiceCode.where(name: 'Reunite', is_standard: [false, nil], addon: false).first_or_create!
    aston_martin_ams.service_notes << ClientProgramServiceNotes.new(
      service_code: reunite, notes: 'Requires Aston Martin approval'
    )

  end

  def down

  end
end
