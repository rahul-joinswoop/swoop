# This will migrate all current:
#
# job.location_type to job.service_location.location_type
#
#  and
#
# job.dropoff_location_type to job.dropoff_service_location.location_type
#

class AddLocationTypesToJobs < SeedMigration::Migration
  def up
=begin
    location_types = LocationType.all

    Job.find_in_batches(batch_size: 200) do |jobs|
      jobs.each do |job|
        if job.location_type && job.service_location
          job.service_location.location_type_id = LocationType.find_by(name:job.location_type).id
        end

        if job.dropoff_location_type && job.drop_location
          job.drop_location.location_type_id = LocationType.find_by(name:job.dropoff_location_type).id
        end

        job.save!
      end
     end
=end
    ActiveRecord::Base.connection.execute "update locations set location_type_id=(select id from location_types where location_types.name=jobs.location_type) from jobs where jobs.service_location_id is not null and jobs.location_type is not null and locations.id=jobs.service_location_id"
    ActiveRecord::Base.connection.execute "update locations set location_type_id=(select id from location_types where location_types.name=jobs.dropoff_location_type) from jobs where jobs.drop_location_id is not null and jobs.dropoff_location_type is not null and locations.id=jobs.drop_location_id"
    
  end

  def down
  end
end
