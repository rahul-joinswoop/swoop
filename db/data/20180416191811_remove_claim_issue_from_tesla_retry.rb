class RemoveClaimIssueFromTeslaRetry < SeedMigration::Migration
  def up
    Company.tesla.issues.delete(Issue.find_by_name('Claim'))
  end

  def down

  end
end
