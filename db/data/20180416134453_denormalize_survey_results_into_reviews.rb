class DenormalizeSurveyResultsIntoReviews < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION denormilizeSurveyResultsIntoReviews() RETURNS void AS
      $BODY$
      DECLARE
          review_row RECORD;
      BEGIN
          FOR review_row IN SELECT DISTINCT review.id
            FROM reviews review
            JOIN survey_results results ON results.review_id = review.id
            WHERE review.nps_question1 IS NULL
            AND review.nps_question2 IS NULL
            AND review.nps_question3 IS NULL
            AND review.nps_feedback  IS NULL
            AND results.int_value IS NOT NULL OR results.string_value IS NOT NULL
          LOOP

            UPDATE reviews SET
              nps_question1 = (
                SELECT res FROM (
                  SELECT int_value AS res
                  FROM survey_results
                  JOIN survey_questions question ON question.id = survey_results.survey_question_id
                  WHERE review_id = review_row.id AND question.order = 1
                  ORDER BY survey_results.created_at DESC
                ) survey_result_value WHERE res IS NOT NULL LIMIT 1
              ),
              nps_question2 = (
                SELECT res FROM (
                  SELECT int_value AS res
                  FROM survey_results
                  JOIN survey_questions question ON question.id = survey_results.survey_question_id
                  WHERE review_id = review_row.id AND question.order = 2
                  ORDER BY survey_results.created_at DESC
                ) survey_result_value WHERE res IS NOT NULL LIMIT 1
              ),
              nps_question3 = (
                SELECT res FROM (
                  SELECT int_value AS res
                  FROM survey_results
                  JOIN survey_questions question ON question.id = survey_results.survey_question_id
                  WHERE review_id = review_row.id AND question.order = 3
                  ORDER BY survey_results.created_at DESC
                ) survey_result_value WHERE res IS NOT NULL LIMIT 1
              ),
              nps_feedback = (
                SELECT * FROM (
                  SELECT string_value AS res
                  FROM survey_results
                  JOIN survey_questions question ON question.id = survey_results.survey_question_id
                  WHERE review_id = review_row.id AND question.order = 4
                  ORDER BY survey_results.created_at DESC
                ) survey_result_value WHERE res <> '' AND res IS NOT NULL LIMIT 1
              )
            WHERE reviews.id = review_row.id;

          END LOOP;
      END;

      $BODY$
      LANGUAGE plpgsql;

      SELECT * FROM denormilizeSurveyResultsIntoReviews();

      DROP FUNCTION denormilizeSurveyResultsIntoReviews();
    }
  end

  def down

  end
end
