class AddNameToNeutralQuestion < SeedMigration::Migration
  def up
    q = Question.find_by question: "Vehicle can be put in neutral?"
    q.update_column :name, 'neutral'
  end

  def down
  end
end
