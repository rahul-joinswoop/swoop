class FixRscSiteMappings < SeedMigration::Migration

  def up
    Issc.where(system: "rsc", site_id: nil, deleted_at: nil).preload(:issc_location).each do |issc|
      issc_location = issc.issc_location
      if issc_location&.location_id
        Issc.where(system: "rsc", deleted_at: nil, contractorid: issc.contractorid).where("site_id IS NOT NULL").each do |cmp|
          if cmp.issc_location && cmp.issc_location.location_id == issc_location.location_id
            puts "set issc.#{issc.id} site_id = #{cmp.site_id}"
            issc.update!(site_id: cmp.site_id)
            break
          end
        end
      end
    end
  end

end
