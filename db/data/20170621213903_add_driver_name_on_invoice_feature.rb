class AddDriverNameOnInvoiceFeature < SeedMigration::Migration
  def up
    Feature.create(
      name: Feature::ADD_DRIVER_NAME_ON_INVOICE,
      description: Feature::DESCRIPTIONS[Feature::ADD_DRIVER_NAME_ON_INVOICE]
    )
  end

  def down
    feature = Feature.find_by_name(Feature::ADD_DRIVER_NAME_ON_INVOICE)

    if feature
      feature.delete
    end
  end
end
