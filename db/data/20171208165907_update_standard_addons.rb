class UpdateStandardAddons < SeedMigration::Migration
  def up
    update_go_jak_name

    update_standard_addons
  end

  def update_go_jak_name
    go_jak = ServiceCode.find_by_name('Go Jak')

    if go_jak.present?
      go_jak.name = 'GoJak'

      go_jak.save!
    end
  end

  def update_standard_addons
    standard_addons = [
      ServiceCode::DOLLY,
      ServiceCode::FUEL,
      ServiceCode::GOJAK,
      ServiceCode::LABOR,
      ServiceCode::REIMBURSEMENT,
      ServiceCode::TIRE_SKATES,
      ServiceCode::TOLL,
      ServiceCode::WAIT_TIME
    ]

    deprecated_standard_addons =
      ServiceCode.
        where.not(name: standard_addons).
        where(addon: true, is_standard: true)

    deprecated_standard_addons.update_all(is_standard: false)
  end

  def down

  end
end
