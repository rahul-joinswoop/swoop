class AddTransportToTrekker < SeedMigration::Migration
  def up
    transport = ServiceCode.find_by! name: ServiceCode::TRANSPORT
    company = Company.find_by name: "Trekker Group"
    if company
      CompaniesService.find_or_create_by company: company, service_code: transport
    end
  end

  def down
  end
end
