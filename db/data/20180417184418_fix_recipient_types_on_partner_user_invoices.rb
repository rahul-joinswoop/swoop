class FixRecipientTypesOnPartnerUserInvoices < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("
      UPDATE invoicing_ledger_items SET recipient_type = 'User', recipient_id = subquery.customer_id
        FROM (
            SELECT invoice.id invoice_id, driver.user_id customer_id
                FROM invoicing_ledger_items invoice
                JOIN jobs ON jobs.id = invoice.job_id
                JOIN drives driver ON jobs.driver_id = driver.id
                JOIN accounts account ON account.id = jobs.account_id
                WHERE invoice.type = 'Invoice'
                AND invoice.deleted_at IS NULL
                AND invoice.recipient_type = 'Account'
                AND jobs.fleet_company_id = jobs.rescue_company_id
                AND account.name = 'Cash Call') AS subquery
        WHERE invoicing_ledger_items.id = subquery.invoice_id;"
    )
  end

  def down

  end
end
