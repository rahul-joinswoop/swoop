class AddRscIntegrationToFeatures < SeedMigration::Migration
  def up
    Feature.create(
      name: "RSC Integration",
      description: "Uses Agero's RSC system instead of ISSC for all Agero jobs.",
      supported_by: ["RescueCompany"],
    )
  end

  def down
  end
end
