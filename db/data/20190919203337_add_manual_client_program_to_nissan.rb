class AddManualClientProgramToNissan < SeedMigration::Migration
  def up
    name = 'Nissan'
    nissan = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !nissan # to support dev envs
      nissan = FleetCompany.init_managed(name).company
    end

    # Create Manual Entry ClientProgram
    not_covered = ClientProgram.where(
      name: 'Manual Entry',
      code: "manual_entry",
      company_id: nissan.id,
      coverage_filename: 'not_covered',
      client_identifier: nil
    ).first_or_create!

  end

  def down

  end
end
