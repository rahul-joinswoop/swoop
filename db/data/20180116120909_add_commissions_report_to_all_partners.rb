class AddCommissionsReportToAllPartners < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION addCommissionsReportToAllPartners() RETURNS void AS
      $BODY$
      DECLARE
          partner_row RECORD;
      BEGIN
          FOR partner_row IN SELECT id FROM companies where type = 'RescueCompany' and deleted_at IS NULL
          LOOP

            INSERT INTO company_reports(report_id, company_id, created_at, updated_at)
            VALUES((SELECT id from reports WHERE name = 'Commissions' LIMIT 1), partner_row.id, now(), now());

          END LOOP;
      END;

      $BODY$
      LANGUAGE plpgsql;

      SELECT * FROM addCommissionsReportToAllPartners();

      DROP FUNCTION addCommissionsReportToAllPartners();
    }
  end

  def down

  end
end
