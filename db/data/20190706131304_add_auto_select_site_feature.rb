class AddAutoSelectSiteFeature < SeedMigration::Migration
  def up
    Feature.where(
      name: Feature::AUTO_SELECT_SITE_FROM_LOOKUP,
      description: Feature::DESCRIPTIONS[Feature::AUTO_SELECT_SITE_FROM_LOOKUP]
    ).first_or_create!
  end

  def down
    Feature.where(name: Feature::AUTO_SELECT_SITE_FROM_LOOKUP).delete_all
  end
end
