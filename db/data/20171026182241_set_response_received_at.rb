class SetResponseReceivedAt < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute("update audit_sms set response_received_at=updated_at where response_received_at is null and parsed_response is not null")
  end

  def down

  end
end
