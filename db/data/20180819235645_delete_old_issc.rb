class DeleteOldIssc < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute "update isscs set deleted_at=now() where clientid in ('GCO', 'GCOAPI','ALLS') and deleted_at is null"
  end

  def down
  end
end
