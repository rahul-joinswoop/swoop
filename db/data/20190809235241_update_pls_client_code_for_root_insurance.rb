class UpdatePlsClientCodeForRootInsurance < SeedMigration::Migration
  def up
    name = 'Root Insurance'
    root_insurance = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !root_insurance # to support dev envs
      root_insurance = FleetCompany.init_managed(name).company
    end

    key = Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE
    value = 'root_insurance'

    setting = root_insurance.settings.where(key: key).first_or_create!
    setting.update! value: value
  end

  def down

  end
end
