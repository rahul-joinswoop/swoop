class AddTantalumCustomerRates < SeedMigration::Migration
  def up
    tantalum=Company.find_by(name:'Tantalum') || FleetCompany.init_managed(FleetCompany::TANTALUM).company

    tantalum_customer_account=tantalum.accounts.find_or_create_by(name:'Customer')
    
    FlatRate.create_with(flat:75).find_or_create_by(company:tantalum,account:tantalum_customer_account)
    FlatRate.create_with(flat:95).find_or_create_by(company:tantalum,account:tantalum_customer_account,service_code:ServiceCode.find_by(name:'Tow'))
  end

  def down

  end
end
