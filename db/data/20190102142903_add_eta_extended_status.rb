class AddEtaExtendedStatus < SeedMigration::Migration
  def up
    JobStatus.create!(id: 28, name: "ETA Extended")
  end

  def down
    JobStatus.delete(28)
  end
end
