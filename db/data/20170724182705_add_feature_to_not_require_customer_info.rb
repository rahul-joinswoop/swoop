class AddFeatureToNotRequireCustomerInfo < SeedMigration::Migration
  def up
        Feature.create(name:"Don't require customer name and phone",
           description:"When set won't require customer name and phone number",
           supported_by:['RescueCompany'])

  end

  def down

  end
end
