class InsertBmwServiceAliases < SeedMigration::Migration
  def up
    bmw = ActiveRecord::Base.connection.execute("SELECT id FROM companies WHERE name = 'BMW NA'")

    if bmw.any?
      names = {
        "Battery Jump" => "Battery Boost",
        "Lock Out" => "Mechanical Lockout",
        "Tow if Jump Fails" => "Tow - Failed Battery Boost",
        "Winch Out" => "Winch Recovery"
      }
      inserts = names.map do |name, srv_alias|
        "((SELECT id FROM companies WHERE name = 'BMW NA'), (SELECT id FROM service_codes WHERE name = '#{name}'), '#{srv_alias}', NOW(), NOW())"
      end
      ActiveRecord::Base.connection.execute "INSERT INTO service_aliases (company_id, service_code_id, alias, created_at, updated_at) VALUES #{inserts.join(",")}"
    end
  end

  def down
  end
end
