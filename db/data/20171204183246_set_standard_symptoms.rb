class SetStandardSymptoms < SeedMigration::Migration
  def up
    [
      Symptom::ACCIDENT,
      Symptom::CUSTOMER_UNSAFE,
      Symptom::DEAD_BATTERY,
      Symptom::FLAT_TIRE,
      Symptom::LOCKED_OUT,
      Symptom::LONG_DISTANCE_TOW,
      Symptom::MECHANICAL_ISSUE,
      Symptom::OTHER,
      Symptom::OUT_OF_FUEL
    ].each do |symptom_name|
      symptom = Symptom.where(name: symptom_name).first_or_create

      symptom.is_standard = true

      symptom.save!
    end
  end

  def down

  end
end
