class AddDealershipLocationTypeToBmwSites < SeedMigration::Migration
  def up
    bmw = Company.find_by_name('BMW NA')
    dealership_location_type = LocationType.find_by_name(LocationType::DEALERSHIP)

    bmw.sites.each do |site|
      if site.location
        site.location.location_type = dealership_location_type

        site.save!
      end
    end
  end

  def down
    bmw = Company.find_by_name('BMW NA')

    bmw.sites.each do |site|
      site.location.location_type = nil

      site.save
    end
  end
end
