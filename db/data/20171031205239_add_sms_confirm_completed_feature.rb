class AddSmsConfirmCompletedFeature < SeedMigration::Migration
  def up
    Feature.create!(name:Feature::SMS_CONFIRM_COMPLETE,
                    description:'Send an SMS 30 mins after the job goes to onsite to the customer to confirm job completion',
                    supported_by:["FleetCompany"])
    company=Company.find_by(name:'USAA')
    company.add_features_by_name([Feature::SMS_CONFIRM_COMPLETE])
  end

  def down

  end
end
