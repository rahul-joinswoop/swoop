class MakeAcceptJobMap < SeedMigration::Migration
  def up
     Feature.create(
       name: Feature::ACCEPT_JOB_MAP,
       description: Feature::DESCRIPTIONS[Feature::ACCEPT_JOB_MAP],
       supported_by: ['RescueCompany','FleetCompany',"SuperCompany"]
     )
   end

   def down; end # not needed
end
