class AddInvoiceCustomItemsFeature < ActiveRecord::Migration[4.2]
  def up
    Feature.create(name:"Invoice Custom Items",
               description:"Allows company to add custom items to invoice",
               supported_by:['RescueCompany',"SuperCompany"])

  end
  
  def down

  end
end
