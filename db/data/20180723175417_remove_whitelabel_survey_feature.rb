class RemoveWhitelabelSurveyFeature < SeedMigration::Migration
  def up
    Feature.find_by(name: "Whitelabel Survey").
      update!(deleted_at: Time.zone.now)
  end

  def down
  end
end
