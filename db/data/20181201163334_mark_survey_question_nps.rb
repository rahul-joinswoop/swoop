class MarkSurveyQuestionNps < SeedMigration::Migration

  def up
    Survey::Question.where(description: 'Question 1 - Recommend (NPS)').each do |sq|
      sq.category = 'nps'
      sq.save!
    end
  end

  def down
    Survey::Question.all.update_all(category: nil)
  end

end
