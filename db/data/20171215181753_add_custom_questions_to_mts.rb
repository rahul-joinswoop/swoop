class AddCustomQuestionsToMts < SeedMigration::Migration
  def up
    company = Company.find_by_name('Motorcycle Towing Services, L.C.')

    # SETUP SERVICES
    service_names = [
      ServiceCode::TOW,
      'Double Tow (Swap Out)', # MTS specific
      ServiceCode::BATTERY_JUMP,
      ServiceCode::FUEL_DELIVERY
    ]

    service_names.each do |service_name|
      ServiceCode.where(name: service_name).where.not(addon: true).first_or_create
    end

    # SETUP QUESTIONS AND ANSWERS
    question_questions = [
      'Can your phone receive texts?',
      'Will the bike roll?',
      'Are the forks locked?',
      'Will bike be attended at pick-up?',
      'Are the keys with the bike?'
    ]

    question_questions.each do |question|
      question = Question.where(question: question).first_or_create

      Answer.where(answer: 'Yes', question: question).first_or_create
      Answer.where(answer: 'No', question: question).first_or_create
    end

    ### TOW

    tow = ServiceCode.where.not(addon: true).find_by(name: ServiceCode::TOW)

    # remove current tow questions
    CompanyServiceQuestion.where(company: company, service_code: tow).delete_all

    # add custom tow questions
    ['Can your phone receive texts?', 'Will the bike roll?', 'Are the forks locked?',
     'Will bike be attended at pick-up?', 'Are the keys with the bike?'].each do |question|

       CompanyServiceQuestion.where(
         company: company,
         service_code: tow,
         question: Question.find_by_question(question)
       ).first_or_create
    end

    ### DOUBLE TOW

    double_tow = ServiceCode.where.not(addon: true).find_by(name: 'Double Tow (Swap Out)')

    # remove current double tow questions
    CompanyServiceQuestion.where(company: company, service_code: double_tow).delete_all

    # add custom double tow questions
    ['Can your phone receive texts?', 'Will the bike roll?', 'Are the forks locked?',
     'Will bike be attended at pick-up?', 'Are the keys with the bike?'].each do |question|
       CompanyServiceQuestion.where(
         company: company,
         service_code: double_tow,
         question:Question.find_by_question(question)
       ).first_or_create
    end

    ### BATTERY JUMP

    battery_jump = ServiceCode.where.not(addon: true).find_by(name: ServiceCode::BATTERY_JUMP)

    # remove current battery jump questions
    CompanyServiceQuestion.where(company: company, service_code: battery_jump).delete_all

    # add custom battery jump questions
    ['Can your phone receive texts?', 'Will bike be attended at pick-up?'].each do |question|
       CompanyServiceQuestion.where(
         company: company,
         service_code: battery_jump,
         question: Question.find_by_question(question)
       ).first_or_create
    end

    ### FUEL DELIVERY

    fuel_delivery = ServiceCode.where.not(addon: true).find_by(name: ServiceCode::FUEL_DELIVERY)

    # remove current fuel delivery questions
    CompanyServiceQuestion.where(company: company, service_code: fuel_delivery).delete_all

    # add custom fuel delivery questions
    ['Can your phone receive texts?', 'Will bike be attended at pick-up?'].each do |question|
       CompanyServiceQuestion.where(
         company: company,
         service_code: fuel_delivery,
         question: Question.find_by_question(question)
       ).first_or_create
    end
  end

  def down

  end
end
