class AddDisableMobileVinLookupFeature < SeedMigration::Migration
  def up
    Feature.create(
      name: 'Disable Mobile VIN Lookup',
      description:'Disables mobile vin lookup to autofill vehicle information',
      supported_by:['RescueCompany']
    )
  end

  def down

  end
end
