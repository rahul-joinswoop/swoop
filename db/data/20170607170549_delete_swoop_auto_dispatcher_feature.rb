class DeleteSwoopAutoDispatcherFeature < SeedMigration::Migration
  def up
    feature=Feature.find_by(name:'Swoop Auto Dispatcher')
    feature.deleted_at=Time.now
    feature.save!
    CompaniesFeature.where(feature:feature).delete_all
  end

  def down

  end
end
