class PolicyLookupAndCoverageSetupForEtr < SeedMigration::Migration
  def up
    etr_name = 'Enterprise Truck Rental'

    etr = Company.find_by(name: etr_name, deleted_at: nil)

    if !etr
      etr = FleetCompany.init_managed(etr_name).company
    end

    # 1 - Add Policy Lookup Types to be used on Job Form
    pls_fields_required = Setting.where(
      company: etr, key: Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES
    ).first_or_create!

    pls_fields_required.update!(
      value: [ PCC::Policy::VIN, PCC::Policy::UNIT_NUMBER, PCC::Policy::MANUAL ]
    )

    # 2 - Setup Policy Lookup System as the Policy provider app for ETR
    policy_lookup_service_setting = Setting.where(
      company: etr, key: Setting::PCC_SERVICE_NAME
    ).first_or_create!

    policy_lookup_service_setting.update!(value: PCC::PCCService::POLICY_LOOKUP_SERVICE)

    # 3 - Add Policy Lookup Service client code to ETR
    policy_lookup_service_client_code = Setting.where(
      company: etr, key: Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE
    ).first_or_create!

    policy_lookup_service_client_code.update! value: 'etr'

    # 4 - Add Coverage programs
    client_program = ClientProgram.where(
      code: 'etr_enterprise_truck_rental',
      name: 'Enterprise Truck Rental',
      coverage_notes: 'Load Transfer requests:

During Business Hours: Call the site associated with the vehicle to manage load transfer.

Outside Business Hours: Tow vehicle to customer final destination (no load transfer).',
      company_id: etr.id,
    ).first_or_create!

    client_program.update! coverage_filename: 'covered'

    not_covered = ClientProgram.where(
      code: "not_covered",
      name: 'Manual Entry',
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_notes: 'During business hours, have the customer call Enterprise. 1-888-736-8287 option 1 for business rental or 2 for personal rental.

• Do NOT collect payment from Customer if a job is created**',
      company_id: etr.id,
    ).first_or_create!

    not_covered.update! coverage_filename: 'not_covered'
  end

  def down

  end
end
