class DefaultConfirmPartnerOnSiteForFleetManaged < SeedMigration::Migration
  def up

    FleetCompany.where(in_house:nil).each do |company|
      company.add_features_by_name([Feature::SMS_CONFIRM_ON_SITE])
    end
  end
  
  def down

  end
end
