class AddJimsTowingServices < SeedMigration::Migration
  def up
    services = [
      "Transport",
      "4x4 Recovery",
      "6x6 Recovery",
      "Service Call",
      "Decking",
      "Undecking",
      "Load Shift",
      "Sublet Services",
      "Law Enforcement"
    ].map do |name|
      ServiceCode.find_or_create_by name: name
    end

    company = Company.find_by name: "Jim's Towing Service, Inc."
    if company
      services.each do |service|
        CompaniesService.find_or_create_by company_id: company.id, service_code_id: service.id
      end
    end
  end

  def down
  end
end
