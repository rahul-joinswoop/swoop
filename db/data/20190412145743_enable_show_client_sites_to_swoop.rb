class EnableShowClientSitesToSwoop < SeedMigration::Migration
  def up
    turo = Company.not_deleted.find_by_name('Turo')
    fleet_response = Company.not_deleted.find_by_name('Fleet Response')

    show_client_sites_to_swoop = Feature.where(name: Feature::SHOW_CLIENT_SITES_TO_SWOOP).first_or_create!
    client_sites = Feature.where(name: Feature::CLIENT_SITES).first_or_create!

    if !turo.features.include?(show_client_sites_to_swoop)
      turo.features << show_client_sites_to_swoop
    end

    if !turo.features.include?(client_sites)
      turo.features << client_sites
    end

    if !fleet_response.features.include?(show_client_sites_to_swoop)
      fleet_response.features << show_client_sites_to_swoop
    end

    if !fleet_response.features.include?(client_sites)
      fleet_response.features << client_sites
    end
  end

  def down

  end
end
