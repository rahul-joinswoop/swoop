class AddPaymentMethods < SeedMigration::Migration

  def up
    PaymentMethod.init_all
  end

  def down
  end

end
