class RemoveFeatureHistory2 < SeedMigration::Migration
  def up
    Feature.where(name: 'History 2.0').destroy_all
  end

  def down

  end
end
