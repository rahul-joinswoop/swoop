class DeleteObsoleteFeatures < SeedMigration::Migration
  def up
    CompaniesFeature.where(feature:Feature.find_by(name:'ISSC Network Status')).delete_all
    CompaniesFeature.where(feature:Feature.find_by(name:'Fleet Dispatch to Truck')).delete_all
  end

  def down

  end
end
