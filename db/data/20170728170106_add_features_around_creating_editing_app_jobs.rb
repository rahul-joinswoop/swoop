class AddFeaturesAroundCreatingEditingAppJobs < SeedMigration::Migration
  def up
    Feature.create(name:"Disable App Edit Jobs",
       description:"disables editing mobile jobs for all users on the react native app",
       supported_by:['RescueCompany'])

    Feature.create(name:"Disable App Drivers Edit Jobs",
       description:"disables editing mobile jobs for users with just the driver permission on the react native app",
       supported_by:['RescueCompany'])

    Feature.create(name:"Disable App Create Jobs",
       description:"disables creating mobile jobs for all users on the react native app",
       supported_by:['RescueCompany'])

    Feature.create(name:"Disable App Drivers Create Jobs",
       description:"disables creating mobile jobs for users with just the driver permission on the react native app",
       supported_by:['RescueCompany'])
  end

  def down

  end
end
