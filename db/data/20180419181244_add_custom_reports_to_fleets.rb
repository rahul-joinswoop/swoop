class AddCustomReportsToFleets < SeedMigration::Migration
  def up
    eta_variance_report = Report.find_by(name: 'Fleet Managed ETA Variance Report', deleted_at: nil)
    csat_report         = Report.find_by(name: 'Fleet Managed CSAT Report', deleted_at: nil)
    activity_report     = Report.find_by(name: 'Fleet Managed Activity Report', deleted_at: nil)

    fleet_response =
      FleetCompany.where(
        name: 'Fleet Response',
        in_house: [false, nil],
        deleted_at: nil).first_or_create!

    enterprise_fleet_management =
      Company.where(
        name: 'Enterprise Fleet Management',
        in_house: [false, nil],
        deleted_at: nil).first_or_create!

    [fleet_response, enterprise_fleet_management].each do |company|
      company.reports.delete([eta_variance_report, csat_report, activity_report])
    end

    fleet_response.reports << Report.where(name: [
      'Fleet Response ETA Variance Report',
      'Fleet Response CSAT Report',
      'Fleet Response Activity Report'
    ])

    enterprise_fleet_management.reports << Report.where(name: [
      'Enterprise Fleet Management ETA Variance Report',
      'Enterprise Fleet Management CSAT Report',
      'Enterprise Fleet Management Activity Report'
    ])
  end

  def down

  end
end
