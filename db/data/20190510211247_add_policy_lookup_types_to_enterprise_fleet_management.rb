class AddPolicyLookupTypesToEnterpriseFleetManagement < SeedMigration::Migration
  def up
    Company.not_deleted.find_by_name('Enterprise Fleet Management').tap do |company|
      # Add Policy Lookup Types to be used on Job Form
      pls_fields_required = Setting.where(company: company, key: Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES).first_or_create!
      pls_fields_required.update!(
        value: [ PCC::Policy::VIN, PCC::Policy::UNIT_NUMBER, PCC::Policy::MANUAL ]
      )
    end
  end

  def down

  end
end
