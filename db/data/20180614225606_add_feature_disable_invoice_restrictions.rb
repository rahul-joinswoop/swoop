class AddFeatureDisableInvoiceRestrictions < SeedMigration::Migration
  def up
    Feature.create!(name: "Disable Invoice Restrictions", description: "Allows partners to appove invoices for fleet managed companies that don't handle invoicing", supported_by: ["FleetInHouse"])
  end

  def down
    Feature.where(name: "Disable Invoice Restrictions").destroy_all
  end
end
