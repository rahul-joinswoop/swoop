class AddLincolnBlackLabelNotesToAccidentTow < SeedMigration::Migration
  def up
    name = 'Lincoln'
    lincoln = Company.find_by(name: name)

    if !lincoln # to support dev envs
      lincoln = FleetCompany.init_managed(name).company
    end

    black_label = ClientProgram.where(code: 'lincoln_black_label', company: lincoln.id).first_or_create!
    accident_tow_service = ServiceCode.find_by_name(ServiceCode::ACCIDENT_TOW)

    black_label.service_notes << ClientProgramServiceNotes.new(
      service_code: accident_tow_service, notes: 'Offer tow to nearest Black Label dealer first (BL in Dealer Name)'
    )
  end

  def down

  end
end
