class CastJsonOnExternalRequestValueForStripeAsyncRequests < SeedMigration::Migration
  def up
    API::AsyncRequest.where(
      system: [
        API::AsyncRequest::INVOICE_CHARGE_CARD,
        API::AsyncRequest::INVOICE_REFUND_CARD,
      ]
    ).where.not(external_request_body: nil).find_each do |async_request|

      # we are safe to eval here as external_request_body value
      # was created by us.
      json_string = eval(async_request.external_request_body).to_json
      async_request.update!(external_request_body: json_string)
    end
  end

  def down; end
end
