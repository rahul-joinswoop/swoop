class RenameFleetSitesFeatureToClientSites < SeedMigration::Migration
  def up
    Feature.find_by_name('Fleet Sites').update!(name: Feature::CLIENT_SITES)
  end

  def down
    Feature.find_by_name(Feature::CLIENT_SITES).update!(name: 'Fleet Sites')
  end
end
