class AddMotorcycleTowToCityTowingAndIslandTowing < SeedMigration::Migration
  def up
    service = ServiceCode.find_or_create_by name: "Motorcycle Tow"
    company_names = ["City Towing Inc", "Island Towing"]

    company_names.each do |name|
      company = Company.find_by name: name
      if company
        CompaniesService.find_or_create_by company_id: company.id, service_code_id: service.id
      end
    end
  end

  def down
  end
end
