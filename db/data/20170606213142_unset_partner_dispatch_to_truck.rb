class UnsetPartnerDispatchToTruck < SeedMigration::Migration
  def up
    CompaniesFeature.where(feature:Feature.find_by(name:'Partner Dispatch to Truck')).delete_all
  end

  def down

  end
end
