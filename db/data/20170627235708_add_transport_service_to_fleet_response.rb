class AddTransportServiceToFleetResponse < SeedMigration::Migration
  def up
    fr = FleetCompany.find_by(name: FleetCompany::FLEET_RESPONSE)
    fr.add_services([ServiceCode.find_by_name:(ServiceCode::TRANSPORT)])
  end

  def down
  end
end
