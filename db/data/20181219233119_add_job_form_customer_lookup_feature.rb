class AddJobFormCustomerLookupFeature < SeedMigration::Migration
  def up
    Feature.create(
      name: 'Job Form Customer Lookup',
      description: 'Feature to include Customer Lookup flow on job form',
      supported_by: ['RescueCompany', 'FleetCompany', 'SuperCompany']
    )
  end

  def down

  end
end
