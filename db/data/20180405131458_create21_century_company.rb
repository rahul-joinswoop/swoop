class Create21CenturyCompany < SeedMigration::Migration
  def up
    farmers = Company.farmers

    feature_ids = farmers.features.map(&:id)

    required_features = [
      Feature::REVIEW_NPS_SURVEYS,
      Feature::SMS_REVIEWS,
    ]

    required_features.each do |feature_name|
      feature_ids << Feature.find_by_name(feature_name).id
    end

    feature_ids.uniq!

    century = CompanyService::Create.new(
      company_attributes:  {
        name: '21st Century Insurance',
        type: 'FleetCompany',
        in_house: false,
        dispatch_to_all_partners: true,
        phone: '+18006188500',
        accounting_email: '21st+accounting@joinswoop.com',
        support_email: '21st+support@joinswoop.com'
      },
      location_attributes: {
        address: 'Los Angeles, CA 90189',
        exact: true,
        lat: 34.0599302,
        lng: -118.24978599999997,
        place_id: 'ChIJf7qlPFLGwoARR7ajal6Q9bo'
      },
      feature_ids: feature_ids
    ).call.company

    century.symptoms.delete_all
    century.symptoms << farmers.symptoms

    century.service_codes.delete_all
    century.service_codes << farmers.service_codes

    century.location_types.delete_all
    century.location_types << farmers.location_types

    century.reports.delete_all
    century.reports << Report.where(name: [
      '21st Century CSAT',
      '21st Century Activity Report',
      '21st Century ETA Variance Report'
    ])

    farmers.company_service_questions.each do |company_service_question|
      CompanyServiceQuestion.where(
        company_id: century.id,
        service_code_id: company_service_question.service_code_id,
        question_id: company_service_question.question.id
      ).first_or_create!
    end

    auction_max_eta = Setting.where(
      key: 'Auction Max Candidate ETA',
      company: century
    ).first_or_create!

    auction_max_eta.value = 45
    auction_max_eta.save!
  end

  def down

  end
end
