class AddAusDepartmentToTesla < SeedMigration::Migration
  def up
    # set its sort order as 100 to keep it at the end of the list even if more depts are added to Tesla
    aus_nz_roadside_department = Department.find_or_create_by(name: 'AUS/NZ Roadside', sort_order: 100)
    tesla = Company.find_by_name('Tesla')

    tesla.departments << aus_nz_roadside_department
  end

  def down
    tesla = Company.find_by_name('Tesla')
    tesla.departments.delete(Department.find_by_name('AUS/NZ Roadside'))

    Department.find_by_name('AUS/NZ Roadside').delete
  end
end
