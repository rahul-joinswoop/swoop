class AddSwoopGoaRate < SeedMigration::Migration

  def up
    goa = ServiceCode.find_by(name: "GOA")
    swoop_id = SuperCompany.find_by(name: "Swoop").id
    Account.where(client_company_id: swoop_id).preload(:company).find_in_batches(batch_size: 50) do |batch|
      Rate.transaction do
        batch.each do |account|
          company = account.company
          goa_rate = company.rates.find_by(type: FlatRate.name, account: account, service_code: goa, site_id: nil, vendor_id: nil, vehicle_category_id: nil, live: true)
          if goa_rate && goa_rate.deleted_at
            goa_rate.update!(live: false)
            goa_rate = nil
          end

          unless goa_rate
            company.rates.create!(type: FlatRate.name, account: account, service_code: goa, site_id: nil, vendor_id: nil, vehicle_category_id: nil, flat: 0, live: true)
          end
        end
      end
    end
  end

  def down
  end

end
