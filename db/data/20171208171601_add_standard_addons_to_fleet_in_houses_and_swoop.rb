class AddStandardAddonsToFleetInHousesAndSwoop < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION addStandardAddonsToFleetInHouses() RETURNS void AS
        $BODY$
        DECLARE
            fleet_company_row RECORD;
            standard_addon_row RECORD;
        BEGIN
            FOR fleet_company_row IN SELECT id FROM companies WHERE (type = 'FleetCompany' AND in_house IS TRUE) OR (name = 'Swoop')
            LOOP
                FOR standard_addon_row IN
                    SELECT id FROM service_codes where is_standard IS TRUE and addon IS TRUE
                LOOP

                INSERT INTO companies_services (company_id, service_code_id, created_at, updated_at)
                SELECT fleet_company_row.id, standard_addon_row.id, now(), now()
                WHERE NOT EXISTS(
                  SELECT id FROM companies_services
                  WHERE
                    company_id = fleet_company_row.id AND
                    service_code_id = standard_addon_row.id
                );

                END LOOP;
            END LOOP;
        END;

        $BODY$
        LANGUAGE plpgsql;

        SELECT * FROM addStandardAddonsToFleetInHouses();

        DROP FUNCTION addStandardAddonsToFleetInHouses();
    }
  end

  def down

  end
end
