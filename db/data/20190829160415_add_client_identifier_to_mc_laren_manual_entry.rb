class AddClientIdentifierToMcLarenManualEntry < SeedMigration::Migration
  def up
    name = 'McLaren'
    mclaren = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !mclaren # to support dev envs
      mclaren = FleetCompany.init_managed(name).company
    end

    # Create Manual Entry ClientProgram
    manual_entry = ClientProgram.where(
      name: 'Manual Entry',
      code: "mclaren_manual",
      company_id: mclaren.id,
      coverage_filename: 'mclaren_manual'
    ).first_or_create!

    manual_entry.update! client_identifier: 'mcw'
  end

  def down

  end
end
