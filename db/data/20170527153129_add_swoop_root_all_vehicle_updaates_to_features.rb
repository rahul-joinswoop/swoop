class AddSwoopRootAllVehicleUpdaatesToFeatures < SeedMigration::Migration
  def up
    Feature.create(name:'Swoop Root All Vehicle Updates', description:'Should swoop root receive all Rescue Vehicle updates via Websocket?')
  end

  def down

  end
end
