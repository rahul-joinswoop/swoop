class InsertTeslaAddons < SeedMigration::Migration
  def up
    tesla = Company.find_by_name('Tesla')
    for addon in ["Dolly", "Go Jak", "Tire Skates", "Wait Time"]
      addonDB = ServiceCode.where(name: addon, addon: true).first_or_create
      CompaniesService.where(company_id: tesla.id, service_code_id: addonDB.id).first_or_create
    end
  end

  def down
  end
end
