class AddManualClientProgramToRootInsurance < SeedMigration::Migration
  def up
    name = 'Root Insurance'
    root_insurance = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !root_insurance # to support dev envs
      root_insurance = FleetCompany.init_managed(name).company
    end

    # Create Manual Entry ClientProgram
    coverage_note  = "If customer insists they are covered, kindly instruct them to call Root Member Services at 866-980-9431. If covered a Root Agent will call to confirm."
    not_covered = ClientProgram.where(
      name: 'Manual Entry',
      code: "ric_rrs_me",
      client_identifier: 'rrs',
      company_id: root_insurance.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
      coverage_notes: coverage_note
    ).first_or_create!

    not_covered.update! coverage_filename: 'not_covered'
  end

  def down
    name = 'Root Insurance'
    root_insurance = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])
    ClientProgram.find_by(
      name: 'Manual Entry',
      code: "ric_rrs_me",
      client_identifier: 'rrs',
      company_id: root_insurance.id,
      coverage_custom_issues: ['Policy Lookup Failed'],
    ).destroy
  end
end
