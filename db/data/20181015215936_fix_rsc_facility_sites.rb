class FixRscFacilitySites < SeedMigration::Migration
  def up
    ApplicationRecord.connection.update <<~SQL
      UPDATE issc_locations
      SET site_id = site_facility_maps.site_id,
          updated_at = NOW()
      FROM site_facility_maps
      WHERE site_facility_maps.fleet_company_id = issc_locations.fleet_company_id
        AND site_facility_maps.company_id = issc_locations.company_id
        AND site_facility_maps.facility_id = issc_locations.location_id
        AND (issc_locations.site_id IS NULL OR issc_locations.site_id <> site_facility_maps.site_id)
    SQL

    ApplicationRecord.connection.update <<~SQL
      UPDATE isscs
      SET site_id = issc_locations.site_id,
          updated_at = NOW()
      FROM issc_locations
      WHERE isscs.issc_location_id = issc_locations.id
        AND (isscs.site_id IS NULL OR isscs.site_id <> issc_locations.site_id)
    SQL
  end
end
