class FixAutoSelectSiteFeatureSupportedBy < SeedMigration::Migration
  def up
    feature = Feature.where(
      name: Feature::AUTO_SELECT_SITE_FROM_LOOKUP,
      description: Feature::DESCRIPTIONS[Feature::AUTO_SELECT_SITE_FROM_LOOKUP]
    ).first_or_create!

    feature.update! supported_by: ['FleetCompany']
  end

  def down

  end
end
