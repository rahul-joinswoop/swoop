class AddDefaultQuestionsToMetromile < SeedMigration::Migration
  def up
    company = Company.find_by name: FleetCompany::METROMILE

    CompanyService::CreateDefaultQuestions.
      new(company: company, service_codes: company.service_codes).
      execute
  end

  def down
    company = Company.find_by name: FleetCompany::METROMILE
    company.service_codes.each do |service_code|
      questions = Question.defaults_per_service_code(service_code)
      CompanyServiceQuestion.where(
        company: company, service_code: service_code, question: questions
      ).delete_all
    end
  end
end
