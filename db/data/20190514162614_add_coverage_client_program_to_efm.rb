class AddCoverageClientProgramToEfm < SeedMigration::Migration
  def up
    name = 'Enterprise Fleet Management'

    efm = Company.not_deleted.find_by(name: name, type: 'FleetCompany', in_house: [nil, false])

    if !efm # to support dev envs only, this company already exists in beta, stg and prod
      efm = FleetCompany.init_managed(name).company
    end

    [:cmx, :fmx, :mmx].each do |code|
      client_program = ClientProgram.where(
        code: "efm_#{code}",
        name: code.upcase,
        company_id: efm.id,
      ).first_or_create!

      client_program.update! coverage_filename: 'covered'
    end

    not_covered = ClientProgram.where(
      code: "not_covered",
      name: 'Manual Entry',
      coverage_custom_issues: ['Policy Lookup Failed'],
      company_id: efm.id,
    ).first_or_create!

    not_covered.update! coverage_filename: 'not_covered'
  end

  def down

  end
end
