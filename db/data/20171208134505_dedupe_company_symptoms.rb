class DedupeCompanySymptoms < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(
      "DELETE FROM company_symptoms
        WHERE id IN (
            SELECT company_symptoms.id FROM company_symptoms
            LEFT OUTER JOIN (
               SELECT MIN(id) AS RowId, company_id, symptom_id
               FROM company_symptoms
               GROUP BY company_id, symptom_id
            ) AS KeepRows ON company_symptoms.id = KeepRows.RowId
        WHERE
           KeepRows.RowId IS NULL)"
    )
  end

  def down

  end
end
