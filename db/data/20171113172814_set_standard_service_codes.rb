class SetStandardServiceCodes < SeedMigration::Migration
  def up
    standard_service_codes = [
      ServiceCode::ACCIDENT_TOW,
      ServiceCode::BATTERY_JUMP,
      ServiceCode::FUEL_DELIVERY,
      ServiceCode::GOA,
      ServiceCode::IMPOUND,
      ServiceCode::LOCK_OUT,
      ServiceCode::OTHER,
      ServiceCode::RECOVERY,
      ServiceCode::REPO,
      ServiceCode::SECONDARY_TOW,
      ServiceCode::STORAGE,
      ServiceCode::TIRE_CHANGE,
      ServiceCode::TOW,
      ServiceCode::TOW_IF_JUMP_FAILS,
      ServiceCode::TRANSPORT,
      ServiceCode::WINCH_OUT
    ]

    standard_addons = [
      ServiceCode::ACCIDENT_CLEANUP,
      ServiceCode::AFTER_HOURS_FEE,
      ServiceCode::CC_PROCESSING_FEES,
      ServiceCode::DOLLY,
      ServiceCode::FUEL,
      ServiceCode::GATE_FEE,
      ServiceCode::GOJAK,
      ServiceCode::LABOR,
      ServiceCode::LIEN_FEE,
      ServiceCode::OVERNIGHT_STORAGE,
      ServiceCode::RAMPS,
      ServiceCode::REIMBURSEMENT,
      ServiceCode::RELEASE_FEES,
      ServiceCode::SECOND_TRUCK,
      ServiceCode::TIRE_SKATES,
      ServiceCode::TOLL,
      ServiceCode::WAIT_TIME
    ]

    ServiceCode.where(name: (standard_service_codes + standard_addons)).each do |service_code|
      service_code.is_standard = true

      service_code.save!
    end
  end

  def down

  end
end
