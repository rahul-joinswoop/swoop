class AddFeatureDisableTowIfBatteryJumpFailsSelection < SeedMigration::Migration
  def up
    Feature.create(name:"Disable force service resolution",
                   description:"No longer requires company to select a specific service on completing a job",
                   supported_by:['RescueCompany','FleetCompany',"SuperCompany"])

  end

  def down

  end
end
