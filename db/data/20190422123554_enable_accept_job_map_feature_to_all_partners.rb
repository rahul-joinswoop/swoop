class EnableAcceptJobMapFeatureToAllPartners < SeedMigration::Migration
  def up
    # already created by 20181029215615_make_accept_job_map seed migration,
    # but we double check it here again just to avoid errors on local envs
    accept_job_map_feature = Feature.where(
      name: Feature::ACCEPT_JOB_MAP,
      description: Feature::DESCRIPTIONS[Feature::ACCEPT_JOB_MAP]
    ).first_or_create!

    RescueCompany.not_deleted.find_in_batches(batch_size: 50) do |rescue_companies_group|
      rescue_companies_group.each do |rescue_company|
        if !rescue_company.features.include?(accept_job_map_feature)
          rescue_company.features << accept_job_map_feature
        end
      end
    end
  end

  def down

  end
end
