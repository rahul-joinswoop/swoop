class SetClientCompanyIdValuesOnAccounts < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION setClientCompanyIdValuesOnAccounts() RETURNS void AS
        $BODY$
        DECLARE
            account_row RECORD;
        BEGIN
            FOR account_row IN SELECT id, fleet_company_id FROM accounts WHERE fleet_company_id IS NOT null
            LOOP
              UPDATE accounts SET client_company_id = account_row.fleet_company_id where id = account_row.id;
            END LOOP;
        END;

        $BODY$
        LANGUAGE plpgsql;

        SELECT * FROM setClientCompanyIdValuesOnAccounts();

        DROP FUNCTION setClientCompanyIdValuesOnAccounts();
    }
  end

  def down

  end
end
