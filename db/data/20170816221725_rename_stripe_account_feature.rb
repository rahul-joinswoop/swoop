class RenameStripeAccountFeature < SeedMigration::Migration
  def up
    f = Feature.find_by name: 'Disable Stripe Integration'
    f.update_attribute :name, 'Disable Bank Settings' if f
  end

  def down
  end
end
