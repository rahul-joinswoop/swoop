class AddExtraTimeToCityTowing < SeedMigration::Migration
  def up
    service = ServiceCode.find_or_create_by name: "Extra Time", addon: true
    company = Company.find_by name: "City Towing Inc"
    if company
      CompaniesService.find_or_create_by company_id: company.id, service_code_id: service.id
    end
  end

  def down
  end
end
