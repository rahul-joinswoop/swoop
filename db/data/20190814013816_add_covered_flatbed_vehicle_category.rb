class AddCoveredFlatbedVehicleCategory < SeedMigration::Migration
  def up
    vehicle_category = VehicleCategory.find_or_create_by!(
      name: VehicleCategory::COVERED_FLATBED, order_num: 8, is_standard: true
    )

  end

  def down

  end
end
