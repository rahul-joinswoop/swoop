class InsertBmwServices < SeedMigration::Migration
  def up
    bmw = ActiveRecord::Base.connection.execute("SELECT id FROM companies WHERE name = 'BMW NA'")

    if bmw.any?
      ActiveRecord::Base.connection.execute("DELETE FROM companies_services WHERE company_id = (SELECT id FROM companies WHERE name = 'BMW NA')")
      names = [
        "Accident Tow",
        "Battery Jump",
        ServiceCode::FUEL_DELIVERY,
        "Lock Out",
        ServiceCode::TIRE_CHANGE,
        "Tow",
        "Tow if Jump Fails",
        "Winch Out"
      ]
      inserts = names.map do |name|
        "((SELECT id FROM companies WHERE name = 'BMW NA'), (SELECT id FROM service_codes WHERE name = '#{name}'), NOW(), NOW())"
      end
      ActiveRecord::Base.connection.execute "INSERT INTO companies_services (company_id, service_code_id, created_at, updated_at) VALUES #{inserts.join(",")}"
    end
  end

  def down
  end
end
