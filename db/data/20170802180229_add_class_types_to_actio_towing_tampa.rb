class AddClassTypesToActioTowingTampa < SeedMigration::Migration
  def up
    cats = [
      "Motorcycle",
      "RV"
    ].map do |name|
      VehicleCategory.find_or_create_by name: name
    end

    company = Company.find_by name: "Action Towing - Tampa"
    if company
      cats.each do |cat|
        CompaniesVehicleCategory.find_or_create_by company_id: company.id, vehicle_category_id: cat.id
      end
    end
  end

  def down
  end
end
