class RemoveGoaAsServiceForCustomers < SeedMigration::Migration
  def up
    service_code = ServiceCode.find_by(name: "GOA")

    service_code.update!(is_standard: false)

    companies = ["Tantalum", "Go Insurance"]

    Company.where(name: companies).find_each do |company|
      company.companies_services.find_by(service_code: service_code).destroy!
    end
  end

  def down
  end
end
