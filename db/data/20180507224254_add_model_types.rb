class AddModelTypes < SeedMigration::Migration
  def up
    ModelType.import_all!
  end

  def down

  end
end
