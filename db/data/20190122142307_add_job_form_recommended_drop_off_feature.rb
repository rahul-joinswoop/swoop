class AddJobFormRecommendedDropOffFeature < SeedMigration::Migration

  def up
    Feature.create(
      name: 'Job Form Recommended Drop Off',
      description: 'On the job form, make a feature to get back end recommended drop off sites',
      supported_by: ['RescueCompany', 'FleetCompany', 'SuperCompany']
    )
  end

  def down # not needed
  end

end
