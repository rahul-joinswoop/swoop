class AddDefaultInvoiceChargeFeesToPartners < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(sql)
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION addDefaultFeeToPartners() RETURNS void AS
      $BODY$
      DECLARE
          partner_row RECORD;
      BEGIN
          FOR partner_row IN SELECT id FROM companies WHERE
            type = 'RescueCompany' AND deleted_at IS NULL
          LOOP
            INSERT INTO company_invoice_charge_fees (
              company_id, percentage, fixed_value, created_at, updated_at
            ) VALUES(partner_row.id, 2.9, 0.30, now(),now());
          END LOOP;
      END;

      $BODY$
      LANGUAGE plpgsql;

      SELECT * FROM addDefaultFeeToPartners();

      DROP FUNCTION addDefaultFeeToPartners();
    }
  end

  def down
    # not implemented
  end
end
