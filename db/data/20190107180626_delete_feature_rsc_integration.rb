class DeleteFeatureRscIntegration < SeedMigration::Migration
  def up
    feature = Feature.find_by(name: "RSC Integration")

    if feature
      feature.companies.delete_all
      feature.delete
    end
  end

  def down
    Feature.create(
      id: 320,
      name: 'RSC Integration',
      description: "Uses Agero's RSC system instead of ISSC for all Agero jobs.",
      supported_by: ['RescueCompany']
    )
  end
end
