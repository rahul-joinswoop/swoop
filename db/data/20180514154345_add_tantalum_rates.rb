class AddTantalumRates < SeedMigration::Migration
  def up
    
    tantalum=Company.find_by(name:'Tantalum') || FleetCompany.init_managed(FleetCompany::TANTALUM).company

    tantalum_account=Company.swoop.accounts.find_or_create_by(name:'Tantalum',client_company:tantalum)
    
    FlatRate.create_with(flat:75).find_or_create_by(company:Company.swoop,account:tantalum_account)
    FlatRate.create_with(flat:95).find_or_create_by(company:Company.swoop,account:tantalum_account,service_code:ServiceCode.find_by(name:'Tow'))
    
  end

  def down

  end
end
