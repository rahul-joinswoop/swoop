class AddQuestionToCentury < SeedMigration::Migration
  def up
    century = Company.find_by(name: FleetCompany::CENTURY)

    question = Question.where(
      question: "Customer in a safe location?"
    ).first_or_create!

    ["Yes", "No"].each do |answer|
      Answer.where(
        answer: answer,
        question: question
      ).first_or_create!
    end

    century.service_codes.where(addon: [nil, false]).each do |service|
      CompanyServiceQuestion.where(
        company_id: century.id,
        service_code_id: service.id,
        question_id: question.id
      ).first_or_create!
    end
  end

  def down

  end
end
