class UpdateClientIdentifierForEfmAndEtr < SeedMigration::Migration
  def up
    # Update Client Identifier Field for EFM
    name_efm = 'Enterprise Fleet Management'
    efm = Company.not_deleted.find_by(name: name_efm, type: 'FleetCompany', in_house: [nil, false])

    if !efm # to support dev envs
      efm = FleetCompany.init_managed(name_efm).company
    end

    [:cmx, :fmx, :mmx].each do |code|
      client_program_efm = ClientProgram.where(
        code: "efm_#{code}",
        name: code.upcase,
        company_id: efm.id,
        ).first_or_create!

      client_program_efm.update! client_identifier: code
    end

    # Update Client Identifier Field for ETR
    name_etr = 'Enterprise Truck Rental'
    etr = Company.not_deleted.find_by(name: name_etr, type: 'FleetCompany', in_house: [nil, false])

    if !etr # to support dev envs
      etr = FleetCompany.init_managed(name_etr).company
    end

    client_program_etr = ClientProgram.where(
      code: "etr_enterprise_truck_rental",
      name: "Enterprise Truck Rental",
      company_id: etr.id,
      ).first_or_create!

    client_program_etr.update! client_identifier: "etr"

    additional_attr = {
      name: 'Manual Entry',
      coverage_custom_issues: ['Policy Lookup Failed'],
      client_identifier: 'etr'
    }

    client_pgm_etr_manual_entry = ClientProgram.where(
      code: "not_covered",
      company_id: etr.id,
      ).first_or_create!(additional_attr)

    client_pgm_etr_manual_entry.update!(additional_attr)
  end

  def down

  end
end
