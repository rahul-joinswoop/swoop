class AddFordDealershipLocationType < SeedMigration::Migration

  def up
    ford = FleetCompany.find_by(name: 'Ford')
    return unless ford
    dealership = LocationType.find_or_create_by(name: LocationType::DEALERSHIP)
    clt = CompaniesLocationType.find_by(company: ford, location_type: dealership)
    unless clt
      CompaniesLocationType.create!(company: ford, location_type: dealership)
    end
  end

  def down
  end

end
