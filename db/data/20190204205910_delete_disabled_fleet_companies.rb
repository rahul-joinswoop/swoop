class DeleteDisabledFleetCompanies < SeedMigration::Migration

  def up
    FleetCompany::UNSUPPORTED_ISSC_COMPANIES.each do |isscco|
      FleetCompany.find_by(name: isscco, issc_client_id: FleetCompany::ISSC_CLIENT_IDS[isscco]).update! deleted_at: DateTime.current
    end
  end

  def down
  end

end
