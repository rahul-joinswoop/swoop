class AddUsaa < SeedMigration::Migration
  def up

    existing=FleetCompany.find_by(name:'USAA')
    if not existing
      company = CompanyService::Create.new(
        company_attributes:  {"type": "FleetCompany",
                              "name": "USAA",
                              "phone": "+14155551234",
                              "support_email": "paul+fm1support@joinswoop.com",
                              "accounting_email": "paul+fm1accounting@joinswoop.com"
                             },
        location_attributes: {"lat": 37.7786953,
                              "lng": -122.4112873,
                              "place_id": "ChIJsRjKQoOAhYARwXo4SeJkExQ",
                              "address": "1121 Mission Street, San Francisco, CA",
                              "exact": true
                             },
        feature_ids: ['Auto Assign',
                      'Settings',
                      'Show Symptoms',
                      'Review Surveys',
                      'Questions',
                      'SMS_ETA Updates',
                      'SMS_Request Location',
                      'SMS_Track Driver',
                      'SMS_Reviews',
                      'Whitelabel Survey',
                      'Payment Type',
                      'Looker'].map { |feature| Feature.find_by(name:feature).id }
      ).call.company

      company.add_customer_types_by_name(['Covered','Customer Pay'])
      company.add_vehicle_categories_by_name(['Flatbed','Wheel Lift','Medium Duty','Light Duty','Heavy Duty','Super Heavy','Service Truck'])
      company.save!
    end

  end

  def down

  end
end
