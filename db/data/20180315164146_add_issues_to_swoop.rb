class AddIssuesToSwoop < SeedMigration::Migration
  def up
    issue = Issue.where(
      key: 'overage_override',
      name: 'Overage Override',
    ).first_or_create

    Reason.where(
      name: 'Client Authorization',
      issue_id: issue.id
    ).first_or_create

    Reason.where(
      name: 'Customer Goodwill',
      issue_id: issue.id
    ).first_or_create

    CompaniesIssues.where(
      company_id: Company.swoop_id,
      issue_id: issue.id
    ).first_or_create
  end

  def down

  end
end
