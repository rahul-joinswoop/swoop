class AddVehicleMakes < SeedMigration::Migration
  def up
    make_names = [
      'Ducati',
      'Harley Davidson',
      'MV Agusta',
      'URAL',
      'Indian',
      'Triumph',
      'Yamaha',
      'Kawasaki',
      'Suzuki',
      'Brammo',
      'Genzee',
      'Vespa',
      'Polaris (Slingshot)',
      'CanAm',
      'ATK',
      'Motus',
      'Victory'
    ]

    make_names.each do |make_name|
      VehicleMake.where(name: make_name, original: true).first_or_create
    end
  end

  def down

  end
end
