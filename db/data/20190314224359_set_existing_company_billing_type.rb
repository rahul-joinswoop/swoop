class SetExistingCompanyBillingType < SeedMigration::Migration

  def up
    finder = RescueProvider.where(company_id: Company.swoop_id).preload([:tags, { site: { company: :settings } }])
    finder.find_in_batches(batch_size: 50) do |rescue_providers|
      Setting.transaction do
        rescue_providers.each do |rescue_provider|
          company = rescue_provider.site&.company
          next unless company
          setting = company.settings.find_or_initialize_by(key: Setting::BILLING_TYPE)
          if rescue_provider.tags.any? { |tag| tag.name.include?("DD") }
            setting.value = "ACH"
          else
            setting.value = "VCC"
          end
          setting.save!
        end
      end
    end
  end

  def down
  end

end
