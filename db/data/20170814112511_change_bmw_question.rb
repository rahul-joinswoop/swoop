class ChangeBmwQuestion < SeedMigration::Migration
  def up
    # change Trip Interrupt description
    trip_question = Question.find_by(name: 'trip-interrupt')

    if trip_question
      trip_question.question = '100+ miles from home?'

      trip_question.save!
    end
  end

  def down

  end
end
