class AddDefaultPartnerReports < SeedMigration::Migration
  def up
    RescueCompany.all.each do |rc|
      rc.add_reports_by_name(RescueCompany::DEFAULT_REPORTS_FOR_NEW_COMPANY)
    end
  end

  def down

  end
end
