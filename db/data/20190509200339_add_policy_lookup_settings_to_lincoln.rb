class AddPolicyLookupSettingsToLincoln < SeedMigration::Migration
  def up
    Company.not_deleted.find_by_name('Lincoln').tap do |company|
      pls_fields_required = Setting.where(
        company: company, key: Setting::POLICY_LOOKUP_SERVICE_FIELDS_REQUIRED_FOR_COVERAGE
      ).first_or_create!

      pls_fields_required.update!(
        value: ['customer_lookup_type', 'odometer', 'year', 'service', 'drop_location', 'service_location']
      )
    end
  end

  def down

  end
end
