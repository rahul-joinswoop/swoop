class AddJobFormCoverage < SeedMigration::Migration
  def up
    Feature.create(
      name: 'Job Form Coverage',
      description: 'Feature to include Coverage section on job form',
      supported_by: ['RescueCompany', 'FleetCompany', 'SuperCompany']
    )
  end

  def down

  end
end
