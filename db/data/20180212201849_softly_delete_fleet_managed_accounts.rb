class SoftlyDeleteFleetManagedAccounts < SeedMigration::Migration
  def up
    account_to_be_deleted = Account.
      joins('INNER JOIN companies ON companies.id = accounts.client_company_id').
      where(companies: { type: 'FleetCompany', in_house: [false, nil] }).select(:id)

    ActiveRecord::Base.connection.execute(sql)

    account_to_be_deleted.each do |account|
      Indexer.perform_async(:index, Account.name, account.id)
    end
  end

  def sql
    %{
      CREATE OR REPLACE FUNCTION softlyDeleteFleetManagedAccounts() RETURNS void AS
      $BODY$
      DECLARE
          account_row RECORD;
      BEGIN
          FOR account_row IN SELECT accounts.id FROM accounts
            INNER JOIN companies ON companies.id = accounts.client_company_id
            WHERE companies.type = 'FleetCompany'
            AND issc_client_id IS NULL
            AND (companies.in_house IS FALSE OR companies.in_house IS NULL)
          LOOP

            UPDATE accounts SET deleted_at = now() WHERE id = account_row.id;

          END LOOP;
      END;

      $BODY$
      LANGUAGE plpgsql;

      SELECT * FROM softlyDeleteFleetManagedAccounts();

      DROP FUNCTION softlyDeleteFleetManagedAccounts();
    }
  end

  def down

  end
end
