class AddSubscriptionStatuses < SeedMigration::Migration
  STATUSES = [
   'Network Only',
   'Free Trial',
   'Subscribed - Essential',
   'Subscribed - Premium',
   'Subscribed - Professional',
   'Subscribed - Enterprise',
  ].freeze

  def up
    STATUSES.each do |name|
      SubscriptionStatus.find_or_create_by!(name: name)
    end
  end

  def down
    SubscriptionStatus.where(name: STATUSES).destroy_all!
  end
end
