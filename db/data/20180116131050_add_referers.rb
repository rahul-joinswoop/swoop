class AddReferers < SeedMigration::Migration
  def up
    referer_names = [
      'Advertisement',
      'Agero',
      'App Store',
      'Geico',
      'Motor Club',
      'Press / News',
      'Referral',
      'Swoop Dispatch',
      'Swoop Email',
      'Web Search',
      'Other'
    ]

    referer_names.each do |referer_name|
      Referer.where(name: referer_name).first_or_create
    end
  end

  def down

  end
end
