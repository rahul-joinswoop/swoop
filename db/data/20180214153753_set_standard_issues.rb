class SetStandardIssues < SeedMigration::Migration
  def up
    Issue::ALL_ISSUES.each do |issue_name|
      issue = Issue.where(name: issue_name).first_or_create
      issue.update! is_standard: true, key: Issue::ISSUE_KEY_MAP[issue_name]
    end
  end

  def down

  end
end
