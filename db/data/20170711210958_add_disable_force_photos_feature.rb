class AddDisableForcePhotosFeature < SeedMigration::Migration
  def up
    Feature.create_with(description: "Disabled forced photos on mobile", supported_by: '["RescueCompany"]').find_or_create_by(name: "Disable Force Photos")
  end

  def down
  
  end
end
