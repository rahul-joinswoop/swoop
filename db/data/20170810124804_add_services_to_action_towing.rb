class AddServicesToActionTowing < SeedMigration::Migration
  def up
    services = [
      "RV Tow",
      "Light Service GOA",
      "Luxury Tow",
      "Exotic Tow",
      "Roll Off"
    ].map do |name|
      ServiceCode.find_or_create_by name: name
    end

    company = Company.find_by name: "Action Towing - Tampa"

    if company
      services.each do |service|
        CompaniesService.find_or_create_by company_id: company.id, service_code_id: service.id
      end
    end
  end

  def down

  end
end
