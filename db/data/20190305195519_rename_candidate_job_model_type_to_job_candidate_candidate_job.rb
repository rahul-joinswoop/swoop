class RenameCandidateJobModelTypeToJobCandidateCandidateJob < SeedMigration::Migration
  def up
    ModelType.find_by(name: 'CandidateJob').update(name: 'Job::Candidate::CandidateJob')
  end

  def down
    ModelType.find_by(name: 'Job::Candidate::CandidateJob').update(name: 'CandidateJob')
  end
end
