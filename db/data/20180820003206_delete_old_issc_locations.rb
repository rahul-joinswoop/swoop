class DeleteOldIsscLocations < SeedMigration::Migration
  def up
    IsscLocation.where(fleet_company_id: Company.where(issc_client_id: ['GCOAPI', 'GCO', 'ALLS', 'ADS', 'RDAM']).pluck(:id)).update_all(deleted_at: Time.now)
  end

  def down
  end
end
