class SetIsStandardToFalseWhenNilOnServiceCodes < SeedMigration::Migration
  def up
    ActiveRecord::Base.connection.execute(
      "UPDATE service_codes SET is_standard = FALSE WHERE is_standard IS NULL"
    )
  end

  def down

  end
end
