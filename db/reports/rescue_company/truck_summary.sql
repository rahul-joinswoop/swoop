
  SELECT
    "Truck",
    "Total Jobs",
    CONCAT(COALESCE((EXTRACT(DAY FROM total_time) * 24) + (EXTRACT(HOUR FROM total_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM total_time)::TEXT, '0'), 2, '0'), ':00') AS "Total Time",
    CONCAT(COALESCE((EXTRACT(DAY FROM drive_time) * 24) + (EXTRACT(HOUR FROM drive_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM drive_time)::TEXT, '0'), 2, '0'), ':00') AS "Drive Time",
    "Total Miles",
    "En Route Miles",
    "Towing Miles",
    "Deadhead Miles",
    CAST((CASE WHEN "Total Miles" = 0 THEN NULL ELSE ("Total Revenue" / "Total Miles") END) AS DECIMAL(14, 2)) AS "Avg. per Mile",
    "Avg. Revenue per Job",
    "Total Revenue"
  FROM (
    SELECT
      trucks.name AS "Truck",
      COUNT(jobs.id) AS "Total Jobs",
      JUSTIFY_HOURS(COALESCE(SUM(
        (SELECT COALESCE(adjusted_dttm, last_set_dttm) FROM audit_job_statuses WHERE job_status_id = 7 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC limit 1) -
        (SELECT COALESCE(adjusted_dttm, last_set_dttm) FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC limit 1)
      ), INTERVAL '0')) AS total_time,
      JUSTIFY_HOURS(COALESCE(SUM(
        COALESCE((
          (SELECT DATE_TRUNC('minute', COALESCE(adjusted_dttm, last_set_dttm)) FROM audit_job_statuses WHERE job_status_id = 6 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1) -
          (SELECT DATE_TRUNC('minute', COALESCE(adjusted_dttm, last_set_dttm)) FROM audit_job_statuses WHERE job_status_id = 5 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1)
        ), INTERVAL '0') + (
          (SELECT DATE_TRUNC('minute', COALESCE(adjusted_dttm, last_set_dttm)) FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1) -
          (SELECT DATE_TRUNC('minute', COALESCE(adjusted_dttm, last_set_dttm)) FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1)
        )
      ), INTERVAL '0')) AS drive_time,
      COALESCE(SUM(ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1)), 0) AS "Total Miles",
      COALESCE(SUM(ROUND(estimates.meters_ab * 0.000621371, 1)), 0) AS "En Route Miles",
      COALESCE(SUM(ROUND(estimates.meters_bc * 0.000621371, 1)), 0) AS "Towing Miles",
      COALESCE(SUM(ROUND((CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_ca ELSE estimates.meters_ba END) * 0.000621371, 1)), 0) AS "Deadhead Miles",
      CAST(AVG((
        SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
        FROM invoicing_line_items
        WHERE
          invoices.id = invoicing_line_items.ledger_item_id
          AND invoicing_line_items.description != 'Storage'
          and invoicing_line_items.deleted_at is null
        GROUP BY invoices.id
      )) AS DECIMAL(14, 2)) as "Avg. Revenue per Job",
      CAST(COALESCE(SUM((
        SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
        FROM invoicing_line_items
        WHERE
          invoices.id = invoicing_line_items.ledger_item_id
          AND invoicing_line_items.description != 'Storage'
          and invoicing_line_items.deleted_at is null
        GROUP BY invoices.id
      )), 0) AS DECIMAL(14, 2)) as "Total Revenue"
    FROM vehicles AS trucks
    LEFT OUTER JOIN jobs ON jobs.rescue_vehicle_id = trucks.id
    LEFT JOIN estimates ON jobs.estimate_id = estimates.id
    LEFT JOIN invoicing_ledger_items AS invoices ON
      jobs.id = invoices.job_id
      AND invoices.deleted_at IS NULL
      AND invoices.sender_id = ?
      AND invoices.type = 'Invoice'
    WHERE
      jobs.deleted_at IS NULL
      AND trucks.company_id = ?
      AND (jobs.status != 'Reassigned' OR jobs.status IS NULL)
      AND (
        (
          jobs.id IS NULL AND trucks.deleted_at IS NULL
        ) OR (
          (
            SELECT COALESCE(adjusted_dttm, last_set_dttm, jobs.created_at)
            FROM audit_job_statuses
            WHERE
              (job_status_id = 2 OR (jobs.status = 'Rejected' AND job_status_id = 10))
              AND job_id = jobs.id
              AND deleted_at IS NULL
            ORDER BY id DESC
            LIMIT 1
          ) >= ?
          AND (
            SELECT COALESCE(adjusted_dttm, last_set_dttm, jobs.created_at)
            FROM audit_job_statuses
            WHERE
              (job_status_id = 2 OR (jobs.status = 'Rejected' AND job_status_id = 10))
              AND job_id = jobs.id
              AND deleted_at IS NULL
            ORDER BY id DESC
            LIMIT 1
          ) < ?
        )
      )
      {{vehicle}}
    GROUP BY trucks.id
    ORDER BY trucks.name ASC
  ) AS results;
