
  SELECT
    job.id AS "Job ID",
    to_char(job.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE((SELECT timezone FROM users WHERE id = ? LIMIT 1), 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Job Created",
    account.name "Account",
    service.name "Service",
    CASE
      WHEN (payment_or_credit.type = 'Refund' AND charge.refund_id IS NOT NULL) THEN CONCAT(refund_user.first_name, ' ', refund_user.last_name)
      WHEN charge.payment_id IS NOT NULL THEN CONCAT(charge_user.first_name, ' ', charge_user.last_name)
      ELSE ''
    END AS "User",
    to_char(payment_or_credit.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE((SELECT timezone FROM users WHERE id = ? LIMIT 1), 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Payment Date",
    payment_or_credit.type AS "Transaction Type",
    payment_or_credit.payment_method AS "Payment Method",
    CAST((- payment_or_credit.total_amount) AS DECIMAL(14, 2)) AS "Amount",
    CAST((
      CASE
        WHEN payment_or_credit.type = 'Refund' AND charge.refund_id IS NOT NULL THEN (- COALESCE(charge.fee, 0))
        WHEN charge.payment_id IS NOT NULL THEN COALESCE(charge.fee, 0)
        ELSE 0
      END
    ) AS DECIMAL(14, 2)) AS "Fee",
    CAST((
      CASE
        WHEN payment_or_credit.type = 'Refund' THEN (-(payment_or_credit.total_amount - COALESCE(charge.fee, 0)))
        ELSE ((-payment_or_credit.total_amount) - COALESCE(charge.fee, 0))
      END
    ) AS DECIMAL(14, 2)) AS "Net",
    CASE
      WHEN payment_or_credit.type = 'Refund' AND charge.refund_id IS NOT NULL THEN 'Refunded'
      WHEN charge.payment_id IS NOT NULL THEN 'Charged'
    END AS "Charge Status",
    to_char(charge.upstream_payout_created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE((SELECT timezone FROM users WHERE id = ? LIMIT 1), 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Payout Date",
    charge.upstream_charge_id AS "Charge ID",
    charge.upstream_payment_id AS "Payment ID",
    charge.upstream_payout_id AS "Payout ID"
    FROM invoicing_ledger_items payment_or_credit
    LEFT JOIN invoicing_ledger_items invoice
      ON invoice.id = payment_or_credit.invoice_id
      AND invoice.deleted_at IS NULL
    LEFT JOIN invoice_payment_charges charge
      ON (charge.payment_id = payment_or_credit.id OR charge.refund_id = payment_or_credit.id)
    LEFT JOIN jobs job ON job.id = invoice.job_id
    LEFT JOIN accounts account ON account.id = job.account_id
    LEFT JOIN service_codes service ON service.id = job.service_code_id
    LEFT JOIN users charge_user ON charge_user.id = charge.charge_user_id
    LEFT JOIN users refund_user ON refund_user.id = charge.refund_user_id
    WHERE payment_or_credit.type IN ('Payment', 'Discount', 'Refund', 'WriteOff')
    AND payment_or_credit.deleted_at IS NULL
    AND invoice.deleted_at IS NULL
    AND job.deleted_at IS NULL
    AND invoice.sender_id = ?
    AND job.created_at >= ?
    AND job.created_at < ?
    {{account}}
    {{only_payments}}
    {{only_discounts}}
    {{only_refunds}}
    {{only_writeoffs}}
    {{only_swoop_charges}}
    {{invoice_payment_method}}
    {{charged}}
    {{refunded}}
    ORDER BY payment_or_credit.created_at;
