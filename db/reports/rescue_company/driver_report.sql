
SELECT
  "Swoop ID",
  "Dispatched Date",
  "Status",
  "Driver",
  "Truck",
  "Account",
  "Dispatched",
  "En Route",
  "On Site",
  "Completed",
  CONCAT(COALESCE((EXTRACT(DAY FROM total_time) * 24) + (EXTRACT(HOUR FROM total_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM total_time)::TEXT, '0'), 2, '0'), ':00') AS "Total Time",
  CONCAT(COALESCE((EXTRACT(DAY FROM drive_time) * 24) + (EXTRACT(HOUR FROM drive_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM drive_time)::TEXT, '0'), 2, '0'), ':00') AS "Drive Time",
  "Total Miles",
  "Payment Type",
  "Invoice Total"
FROM (
  Select
        jobs.id as "Swoop ID",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) as "Dispatched Date",
        jobs.status as "Status",
        CONCAT(driver.first_name, ' ', driver.last_name) as "Driver",
        truck.name as "Truck",
        accounts.name as "Account",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) as "Dispatched",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1) as "En Route",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc  limit 1) as "On Site",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) as "Completed",
        ((select CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) -
        (select CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1)) as total_time,
        (
           (coalesce(((select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=6 and job_id=jobs.id order by id desc limit 1) -
           (select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=5 and job_id=jobs.id order by id desc  limit 1)),interval '0')
         +
           ((select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) -
           (select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1)))
        ) as drive_time,
        ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) AS "Total Miles",
        customer_types.name AS "Payment Type",
         CAST(
           (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
              from invoicing_line_items
              where invoices.id=invoicing_line_items.ledger_item_id
               and invoicing_line_items.description!='Storage'
               and invoicing_line_items.deleted_at is null
           group by invoices.id) AS DECIMAL(14, 2)
          ) as "Invoice Total"

        from jobs
        left join users as customer on jobs.user_id=customer.id
        left join users as driver on jobs.rescue_driver_id=driver.id
        left join vehicles as truck on jobs.rescue_vehicle_id=truck.id
        left join invoicing_ledger_items as invoices on invoices.job_id=jobs.id and invoices.deleted_at is NULL and invoices.sender_id=? and invoices.type='Invoice'
        left join accounts on accounts.id=jobs.account_id
        left join customer_types on jobs.customer_type_id = customer_types.id
        left join estimates ON estimates.id = jobs.estimate_id
        left join users on users.id=?

        Where
          jobs.rescue_company_id=?
          AND jobs.deleted_at is null
          AND (select (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) >= ?
          AND (select (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) < ?
          AND jobs.status != 'Reassigned'
    {{driver}}
    {{vehicle}}
    {{account}}
    {{payment_type}}
    ORDER BY (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1)
  ) AS results;
