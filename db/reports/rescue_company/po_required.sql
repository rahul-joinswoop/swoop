
  SELECT
    jobs.id AS "Swoop ID",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatch Date",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatched",
    accounts.name AS "Account",
    accounts.contact_phone AS "Phone",
    service_codes.name AS "Service",
    CONCAT(customers.first_name, ' ', customers.last_name) AS "Customer",
    CONCAT_WS(' ', vehicles.make, vehicles.model, vehicles.year) as "Vehicle",
    jobs.status AS "Status",
    jobs.po_number AS "PO",
    CAST((
      SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount) AS sum_amount
      FROM invoicing_line_items
      WHERE
        invoices.id = invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
     GROUP BY invoices.id) AS DECIMAL(14, 2)
    ) AS "Invoice Total"
  FROM jobs
  LEFT JOIN users ON users.id = ?
  LEFT JOIN users AS customers ON jobs.user_id = customers.id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN accounts ON accounts.id = jobs.account_id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN invoicing_ledger_items AS invoices ON
    invoices.job_id = jobs.id
    AND invoices.deleted_at IS NULL
    AND invoices.sender_id = ?
    AND invoices.type='Invoice'
  WHERE
    jobs.rescue_company_id=?
    AND jobs.deleted_at IS NULL
    AND jobs.status != 'Reassigned'
    AND accounts.po_required_before_sending_invoice = TRUE
    AND (SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) FROM audit_job_statuses WHERE job_status_id = 2 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) >= ?
    AND (SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) FROM audit_job_statuses WHERE job_status_id = 2 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) < ?
    {{account}}
ORDER BY jobs.id DESC;
