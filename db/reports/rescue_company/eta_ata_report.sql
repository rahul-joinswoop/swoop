
  SELECT
    jobs.id AS "Swoop ID",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatch Date",
    jobs.status AS "Status",
    CONCAT(drivers.first_name, ' ', drivers.last_name) AS "Driver",
    trucks.name AS "Truck",
    accounts.name AS "Account",
    CONCAT_WS(' ', partner_dispatchers.first_name, partner_dispatchers.last_name) AS "Dispatcher",
    service_codes.name AS "Service",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatched",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 3
        AND job_id = jobs.id
      ORDER BY id DESC
      LIMIT 1
    ) as "En Route",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 4
        AND job_id = jobs.id
      ORDER BY id DESC
      LIMIT 1
    ) as "On Site",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (
        SELECT (time - created_at)
        FROM time_of_arrivals
        WHERE
          time_of_arrivals.job_id = jobs.id
          AND time_of_arrivals.eba_type IN (0, 3)
        ORDER BY id ASC
        LIMIT 1
      )
    ) / 60) AS numeric), 0) AS "ETA",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (
        SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END
        FROM (
          SELECT last_set_dttm, adjusted_dttm
          FROM audit_job_statuses
          WHERE
            job_status_id = 4
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) AS onsite_ajs
      ) -
      (
        CASE WHEN (
          SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1
        ) IS NULL THEN (
          SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END
          FROM (
            SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 9 AND job_id = jobs.id ORDER BY id DESC LIMIT 1
          ) AS created_ajs
        ) ELSE (
          SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1
        ) END
      )
    ) / 60) AS numeric), 0) AS "ATA",
    CAST((
      SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount) AS sum_amount
      FROM invoicing_line_items
      WHERE
        invoices.id = invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
     GROUP BY invoices.id) AS DECIMAL(14, 2)
    ) AS "Invoice Total"
  FROM jobs
  LEFT JOIN users ON users.id = ?
  LEFT JOIN accounts ON accounts.id = jobs.account_id
  LEFT JOIN users AS drivers ON jobs.rescue_driver_id = drivers.id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN users AS partner_dispatchers ON jobs.partner_dispatcher_id = partner_dispatchers.id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN invoicing_ledger_items AS invoices ON invoices.job_id = jobs.id AND invoices.deleted_at IS NULL AND invoices.sender_id = ? AND invoices.type='Invoice'
  WHERE
    jobs.rescue_company_id=?
    AND jobs.deleted_at IS NULL
    AND jobs.status != 'Reassigned'
    AND jobs.scheduled_for IS NULL
    AND (SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) FROM audit_job_statuses WHERE job_status_id = 2 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) >= ?
    AND (SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) FROM audit_job_statuses WHERE job_status_id = 2 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) < ?
    {{driver}}
    {{vehicle}}
    {{account}}
ORDER BY jobs.id DESC;
