
SELECT
accounts.name  AS "Account",
jobs.id  AS "ID",
DATE((jobs.created_at )::timestamptz AT TIME ZONE 'America/Los_Angeles') AS "Date",
driver.first_name || ' ' || driver.last_name  AS "Driver",
service.name AS "Primary Service",
COALESCE(COALESCE(CAST((SUM (DISTINCT (CAST(FLOOR(COALESCE(invoicing_line_items.net_amount ,0)*(1000000*1.0)) AS DECIMAL(65,0))) + ('x'
  || MD5(invoicing_line_items.id ::varchar))::bit(64)::bigint::DECIMAL(65,0)  *18446744073709551616 + ('x'
  || SUBSTR(MD5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::DECIMAL(65,0) ) - SUM(DISTINCT ('x'
  || MD5(invoicing_line_items.id ::varchar))::bit(64)::bigint::DECIMAL(65,0)  *18446744073709551616 + ('x'
  || SUBSTR(MD5(invoicing_line_items.id ::varchar),17))::bit(64)::bigint::DECIMAL(65,0)) )  AS DOUBLE PRECISION) / CAST((1000000*1.0) AS DOUBLE PRECISION), 0), 0) AS "Net Amount",
CASE WHEN COUNT(service_exclusion.id) > 0 THEN '0.00' ELSE (
  COALESCE(COALESCE(CAST((SUM (DISTINCT (CAST(FLOOR(COALESCE(invoicing_line_items_minus_exclusions.net_amount*(CAST((SELECT default_pct FROM company_commissions WHERE company_commissions.company_id = rescue_company.id) AS DECIMAL) / 100), 0)*(1000000*1.0)) AS DECIMAL(65,0))) + ('x'
    || MD5(invoicing_line_items_minus_exclusions.id ::varchar))::bit(64)::bigint::DECIMAL(65,0)  *18446744073709551616 + ('x'
    || SUBSTR(MD5(invoicing_line_items_minus_exclusions.id ::varchar),17))::bit(64)::bigint::DECIMAL(65,0) ) - SUM(DISTINCT ('x'
    || MD5(invoicing_line_items_minus_exclusions.id ::varchar))::bit(64)::bigint::DECIMAL(65,0)  *18446744073709551616 + ('x'
    || SUBSTR(MD5(invoicing_line_items_minus_exclusions.id ::varchar),17))::bit(64)::bigint::DECIMAL(65,0)) )  AS DOUBLE PRECISION) / CAST((1000000*1.0) AS DOUBLE PRECISION), 0), 0)
  ) END AS "Commission",
COALESCE(CAST((
    SELECT sum(invoicing_tax_items.tax_amount) AS tax_amount
    FROM invoicing_line_items AS invoicing_tax_items
    WHERE invoicing_tax_items.ledger_item_id = jobs.invoice_id
    AND invoicing_tax_items.description!='Storage') AS DECIMAL(14, 2)), '0.00') AS "Tax"
FROM jobs  AS jobs
LEFT JOIN accounts AS accounts ON jobs.account_id = accounts.id
LEFT JOIN companies AS rescue_company ON jobs.rescue_company_id = rescue_company.id
LEFT JOIN users AS driver ON jobs.rescue_driver_id = driver.id
LEFT JOIN service_codes AS service ON jobs.service_code_id = service.id
LEFT JOIN service_commissions_exclusions AS service_exclusion ON jobs.service_code_id = service_exclusion.service_code_id
      AND rescue_company.id = service_exclusion.company_id
LEFT JOIN invoicing_ledger_items  AS invoicing_ledger_items ON jobs.id = invoicing_ledger_items.job_id
      AND invoicing_ledger_items.sender_id = jobs.rescue_company_id
      AND invoicing_ledger_items.deleted_at IS NULL
      AND invoicing_ledger_items.type = 'Invoice'
LEFT JOIN invoicing_line_items  AS invoicing_line_items ON invoicing_ledger_items.id = invoicing_line_items.ledger_item_id
      AND invoicing_line_items.deleted_at IS NULL
LEFT JOIN invoicing_line_items  AS invoicing_line_items_minus_exclusions ON invoicing_ledger_items.id = invoicing_line_items_minus_exclusions.ledger_item_id
      AND invoicing_line_items_minus_exclusions.deleted_at IS NULL
      AND
        (
          (CASE WHEN EXISTS (SELECT id from service_commissions_exclusions WHERE company_id = rescue_company.id AND service_code_id = (SELECT id FROM service_codes where name = 'Storage' AND is_standard = true))
            THEN invoicing_line_items_minus_exclusions.description != 'Storage' AND (lower(invoicing_line_items_minus_exclusions.description) NOT IN (SELECT lower(commission_exclusions.item) FROM commission_exclusions WHERE company_id=invoicing_ledger_items.sender_id))
            ELSE (lower(invoicing_line_items_minus_exclusions.description) NOT IN (SELECT lower(commission_exclusions.item) FROM commission_exclusions WHERE company_id=invoicing_ledger_items.sender_id))
          END)
        )
WHERE (jobs.status = 'Completed' OR jobs.status = 'GOA' OR jobs.status = 'Stored' OR jobs.status = 'Released')
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
  AND rescue_company.id = ?
  {{driver}}
  {{account}}
GROUP BY 1,2,3,4,5
ORDER BY 1
