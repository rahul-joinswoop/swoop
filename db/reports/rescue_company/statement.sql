
  SELECT
    jobs.id AS "Swoop ID",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY')
      FROM audit_job_statuses
      WHERE
        job_status_id = CASE
          WHEN jobs.status = 'Completed' THEN 7
          WHEN jobs.status = 'Canceled' THEN 8
          WHEN jobs.status = 'Released' THEN 18
          WHEN jobs.status = 'GOA' THEN 21
        END
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Service Date",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = CASE
          WHEN jobs.status = 'Completed' THEN 7
          WHEN jobs.status = 'Canceled' THEN 8
          WHEN jobs.status = 'Released' THEN 18
          WHEN jobs.status = 'GOA' THEN 21
        END
        AND job_id = jobs.id
      ORDER BY id DESC LIMIT 1
    ) AS "Time",
    accounts.name AS "Account",
    jobs.po_number AS "PO",
    service_codes.name AS "Service",
    CONCAT_WS(' ', vehicles.make, vehicles.model, vehicles.year) as "Vehicle",
    vehicles.vin as "Vin",
    jobs.status AS "Status",
    ( /* PDW: If you modify this then you also need to modify the filter clauses */
      CASE
        WHEN invoices.state IN ('created', 'partner_new', 'partner_approved') AND invoices.paid != 't' THEN 'Unsent'
        WHEN ((
          (COALESCE(invoices.total_amount, 0) + (
            SELECT COALESCE(SUM(total_amount), 0) AS "payments_or_credits_sum"
            FROM invoicing_ledger_items AS "payments_or_credits"
            WHERE payments_or_credits.invoice_id = invoices.id
            AND payments_or_credits.deleted_at IS NULL
            AND payments_or_credits.type IN ('Payment', 'Discount', 'Refund', 'WriteOff')
          )) > 0) OR (invoices.paid != TRUE AND invoices.state != 'on_site_payment')) THEN 'Outstanding'
        WHEN invoices.state = 'on_site_payment' OR invoices.paid = TRUE THEN 'Paid'
        ELSE NULL
      END
    ) AS "Invoice Status",
    CAST((
      SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount) AS sum_amount
      FROM invoicing_line_items
      WHERE invoices.id = invoicing_line_items.ledger_item_id
        AND invoicing_line_items.deleted_at IS NULL
     GROUP BY invoices.id) AS DECIMAL(14, 2)
    ) AS "Invoice Total",
    (
      SELECT
      (
        CASE
          WHEN MAX(payments.total) > 1 THEN 'Multiple'
          ELSE MAX(payments.payment_method)
        END
      )
      FROM (
        SELECT COUNT(sub_payments.payment_method) AS "total", MAX(payment_method) AS "payment_method"
        FROM invoicing_ledger_items AS "sub_payments"
        WHERE sub_payments.invoice_id = invoices.id
        AND sub_payments.deleted_at IS NULL
        AND sub_payments.type = 'Payment'
      ) AS "payments"
    ) AS "Payment Method",
    CAST((
        SELECT ABS(COALESCE(SUM(total_amount), 0))
        FROM invoicing_ledger_items AS "payments"
        WHERE payments.invoice_id = invoices.id
        AND payments.deleted_at IS NULL
        AND payments.type IN ('Payment')
      ) AS DECIMAL(14, 2)
    ) AS "Payment",
    CAST((
      COALESCE(invoices.total_amount, 0) + (
        SELECT COALESCE(SUM(total_amount), 0) AS "payments_or_credits_sum"
        FROM invoicing_ledger_items AS "payments_or_credits"
        WHERE payments_or_credits.invoice_id = invoices.id
        AND payments_or_credits.deleted_at IS NULL
        AND payments_or_credits.type IN ('Payment', 'Discount', 'Refund', 'WriteOff')
      )) AS DECIMAL(14, 2)
    ) AS "Balance Due"
  FROM jobs
  LEFT JOIN users ON users.id = ?
  LEFT JOIN accounts ON accounts.id = jobs.account_id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN users AS customers ON drives.user_id = customers.id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN invoicing_ledger_items AS invoices ON invoices.job_id = jobs.id AND invoices.deleted_at IS NULL AND invoices.sender_id = ? AND invoices.type='Invoice' AND invoices.deleted_at IS NULL
  WHERE
    jobs.rescue_company_id=?
    AND jobs.deleted_at IS NULL
    AND jobs.status IN ('Completed', 'GOA', 'Canceled', 'Released')
    AND (SELECT
          (CASE
            WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
            FROM audit_job_statuses WHERE job_status_id = CASE
              WHEN jobs.status = 'Completed' THEN 7
              WHEN jobs.status = 'Canceled' THEN 8
              WHEN jobs.status = 'Released' THEN 18
              WHEN jobs.status = 'GOA' THEN 21
          END
          AND job_id = jobs.id ORDER BY id DESC LIMIT 1) >= ?
    AND (SELECT
          (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses WHERE job_status_id = CASE
            WHEN jobs.status = 'Completed' THEN 7
            WHEN jobs.status = 'Canceled' THEN 8
            WHEN jobs.status = 'Released' THEN 18
            WHEN jobs.status = 'GOA' THEN 21
          END
          AND job_id = jobs.id ORDER BY id DESC LIMIT 1) < ?
    {{account}}
    {{unsent}}
    {{outstanding}}
    {{paid}}
    {{invoice_payment_method}}
ORDER BY jobs.id DESC;
