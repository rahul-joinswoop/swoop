
  SELECT
    "Driver Name",
    "Total Jobs",
    CONCAT(COALESCE((EXTRACT(DAY FROM total_time) * 24) + (EXTRACT(HOUR FROM total_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM total_time)::TEXT, '0'), 2, '0'), ':00') AS "Total Time",
    CONCAT(COALESCE((EXTRACT(DAY FROM drive_time) * 24) + (EXTRACT(HOUR FROM drive_time)), '0'), ':', LPAD(COALESCE(EXTRACT(MINUTE FROM drive_time)::TEXT, '0'), 2, '0'), ':00') AS "Drive Time",
    "Total Miles",
    "Avg. ETA",
    "Avg. ATA",
    "Avg. Revenue per Job",
    "Total Revenue"
  FROM (
    SELECT
      CONCAT(drivers.first_name, ' ', drivers.last_name) AS "Driver Name",
      COUNT(jobs.id) AS "Total Jobs",
      JUSTIFY_HOURS(COALESCE(SUM(
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM audit_job_statuses WHERE job_status_id = 7 AND job_id = jobs.id ORDER BY id DESC limit 1) -
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id ORDER BY id DESC limit 1)
      ), INTERVAL '0')) AS total_time,
      JUSTIFY_HOURS(COALESCE(SUM(
        COALESCE((
          (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN DATE_TRUNC('minute', adjusted_dttm) ELSE DATE_TRUNC('minute', last_set_dttm) END FROM audit_job_statuses WHERE job_status_id = 6 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) -
          (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN DATE_TRUNC('minute', adjusted_dttm) ELSE DATE_TRUNC('minute', last_set_dttm) END FROM audit_job_statuses WHERE job_status_id = 5 AND job_id = jobs.id ORDER BY id DESC LIMIT 1)
        ), INTERVAL '0') + (
          (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN DATE_TRUNC('minute', adjusted_dttm) ELSE DATE_TRUNC('minute', last_set_dttm) END FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) -
          (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN DATE_TRUNC('minute', adjusted_dttm) ELSE DATE_TRUNC('minute', last_set_dttm) END FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id ORDER BY id DESC LIMIT 1)
        )
      ), INTERVAL '0')) AS drive_time,
      COALESCE(SUM(ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1)), 0) AS "Total Miles",
      ROUND(AVG(CAST((EXTRACT(EPOCH FROM
        (SELECT (time - created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS numeric)), 0) AS "Avg. ETA",
      ROUND(AVG(CAST((EXTRACT(EPOCH FROM
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
        (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS numeric)), 0) AS "Avg. ATA",
      CAST(COALESCE(AVG((
        SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
        FROM invoicing_line_items
        WHERE
          invoices.id = invoicing_line_items.ledger_item_id
          AND invoicing_line_items.description != 'Storage'
          and invoicing_line_items.deleted_at is null
        GROUP BY invoices.id
      )), 0) AS DECIMAL(14, 2)) as "Avg. Revenue per Job",
      CAST(
        COALESCE(
          SUM((
            SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
            FROM invoicing_line_items
            WHERE
              invoices.id = invoicing_line_items.ledger_item_id
              AND invoicing_line_items.description != 'Storage'
              and invoicing_line_items.deleted_at is null
            GROUP BY invoices.id
          ))
        , 0)
      AS DECIMAL(14, 2)) as "Total Revenue"
    FROM jobs
    RIGHT JOIN users AS drivers ON jobs.rescue_driver_id = drivers.id
    LEFT JOIN companies ON drivers.company_id = companies.id
    LEFT JOIN users_roles ON drivers.id = users_roles.user_id
    LEFT JOIN roles ON users_roles.role_id = roles.id
    LEFT JOIN estimates ON jobs.estimate_id = estimates.id
    LEFT JOIN invoicing_ledger_items AS invoices ON
      jobs.id = invoices.job_id
      AND invoices.deleted_at IS NULL
      AND invoices.sender_id = ?
      AND invoices.type='Invoice'
    WHERE
      jobs.deleted_at IS NULL
      AND roles.name = 'driver'
      AND drivers.company_id = ?
      AND (jobs.status != 'Reassigned' OR jobs.status IS NULL)
      AND (
        (
          SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses
          WHERE
            job_status_id = 2
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) >= ?
        OR (
          SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses
          WHERE
            job_status_id = 2
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) IS NULL
      )
      AND (
        (
          SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses
          WHERE
            job_status_id = 2
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) < ?
        OR (
          SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
          FROM audit_job_statuses
          WHERE
            job_status_id = 2
            AND job_id = jobs.id
          ORDER BY id DESC
          LIMIT 1
        ) IS NULL
      )
    {{driver}}
    GROUP BY drivers.id
    ORDER BY drivers.last_name ASC
  ) AS results;
