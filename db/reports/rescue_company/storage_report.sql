
  SELECT
    jobs.id as "Swoop ID",
    to_char(inventory_items.stored_at  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')  as "Date",
    FLOOR(invoicing_line_items.quantity) as "Days",
    sites.name as "Site",
    storage_types.name as "Type",
    (CASE WHEN jobs.type='RescueJob' THEN COALESCE(account_company.name, accounts.name, 'Cash Call') ELSE COALESCE(fleet_company.name, accounts.name) END) as "Account",
    vehicles.year as "Year",
    vehicles.make as "Make",
    vehicles.model as "Model",
    vehicles.color as "Color",
    vehicles.license as "License",
    vehicles.vin as "VIN"

    FROM jobs
    LEFT JOIN inventory_items
      ON jobs.id = inventory_items.job_id AND inventory_items.item_type = 'Vehicle'
    LEFT JOIN sites
      ON inventory_items.site_id = sites.id
    LEFT JOIN storage_types
      ON storage_types.id = jobs.storage_type_id
    LEFT JOIN accounts
      ON accounts.id = jobs.account_id
    LEFT JOIN companies fleet_company
      ON jobs.fleet_company_id = fleet_company.id
    LEFT JOIN companies account_company
      ON accounts.client_company_id = account_company.id
    LEFT JOIN drives
      ON jobs.driver_id = drives.id
    LEFT JOIN vehicles
      ON vehicles.id = drives.vehicle_id
    LEFT JOIN invoicing_ledger_items
      ON invoicing_ledger_items.job_id = jobs.id
        AND invoicing_ledger_items.sender_id = jobs.rescue_company_id
    LEFT JOIN invoicing_line_items
      ON invoicing_line_items.ledger_item_id = invoicing_ledger_items.id
        AND invoicing_line_items.description = 'Storage'
        AND invoicing_line_items.deleted_at IS NULL
    LEFT JOIN users
      ON users.id = ?
    WHERE inventory_items.deleted_at IS NULL
      AND (invoicing_ledger_items.type is null or invoicing_ledger_items.type = 'Invoice')
      AND invoicing_ledger_items.deleted_at IS NULL
      AND jobs.deleted_at IS NULL
      AND jobs.status = 'Stored'
      AND inventory_items.item_type='Vehicle'
      AND inventory_items.released_at IS NULL
      AND jobs.rescue_company_id = ?
      {{account}}
      {{type}}
      {{site}}
    ORDER BY jobs.id ASC
