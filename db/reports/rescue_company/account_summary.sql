
  WITH jobs AS (
    SELECT
      jobs.id,
      jobs.account_id,
      invoices.id AS invoice_id,
      invoices.state AS invoice_state,
      invoices.paid AS invoice_paid,
      CAST((
        SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount)
        FROM invoicing_line_items
        WHERE
          invoices.id = invoicing_line_items.ledger_item_id
          and invoicing_line_items.deleted_at is null
        GROUP BY invoices.id
      ) AS DECIMAL(14, 2)) as amount,
      ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) AS total_miles
    FROM jobs
    LEFT JOIN estimates ON
      jobs.estimate_id = estimates.id
    LEFT JOIN invoicing_ledger_items AS invoices ON
      jobs.id = invoices.job_id
      AND invoices.deleted_at IS NULL
      AND invoices.sender_id = ?
      AND invoices.type='Invoice'
    WHERE
      jobs.deleted_at IS NULL
      AND jobs.rescue_company_id = ?
      AND jobs.status != 'Reassigned'
      AND (
        SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
        FROM audit_job_statuses
        WHERE
          job_status_id = 2
          AND job_id = jobs.id
        ORDER BY id DESC
        LIMIT 1) >= ?
      AND (
        SELECT (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END)
        FROM audit_job_statuses
        WHERE
          job_status_id = 2
          AND job_id = jobs.id
        ORDER BY id DESC
        LIMIT 1) < ?
  )
  SELECT
    accounts.name AS "Account",
    accounts.contact_phone AS "Phone",
    (
      SELECT COUNT(*)
      FROM jobs
      WHERE jobs.account_id = accounts.id
    ) AS "Total Jobs",
    (
      SELECT COALESCE(SUM(total_miles), 0)
      FROM jobs
      WHERE jobs.account_id = accounts.id
    ) AS "Total Miles",
    (
      SELECT COUNT(*)
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_state NOT IN ('partner_sent', 'fleet_approved', 'fleet_downloaded', 'on_site_payment', 'swoop_downloaded_partner', 'swoop_approved_partner')
    ) AS "Unsent Invoices",
    (
      SELECT CAST(COALESCE(SUM(amount), 0) AS DECIMAL(14, 2))
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_state NOT IN ('partner_sent', 'fleet_approved', 'fleet_downloaded', 'on_site_payment', 'swoop_downloaded_partner', 'swoop_approved_partner')
    ) AS "Unsent Total",
    (
      SELECT COUNT(*)
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_state IN ('partner_sent', 'fleet_approved', 'fleet_downloaded', 'on_site_payment', 'swoop_downloaded_partner', 'swoop_approved_partner')
        AND jobs.invoice_paid = FALSE
    ) AS "Outstanding Invoices",
    (
      SELECT CAST(COALESCE(SUM(amount), 0) AS DECIMAL(14, 2))
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_state IN ('partner_sent', 'fleet_approved', 'fleet_downloaded', 'on_site_payment', 'swoop_downloaded_partner', 'swoop_approved_partner')
        AND jobs.invoice_paid = FALSE
    ) AS "Outstanding Total",
    (
      SELECT COUNT(*)
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_paid = TRUE
    ) AS "Paid Invoices",
    (
      SELECT CAST(COALESCE(SUM(amount), 0) AS DECIMAL(14, 2))
      FROM jobs
      WHERE
        jobs.account_id = accounts.id
        AND jobs.invoice_paid = TRUE
    ) AS "Paid Total",
    (
      SELECT CAST(AVG(amount) AS DECIMAL(14, 2))
      FROM jobs
      WHERE jobs.account_id = accounts.id
    ) AS "Avg. Revenue per Job",
    (
      SELECT CAST(COALESCE(SUM(amount), 0) AS DECIMAL(14, 2))
      FROM jobs
      WHERE jobs.account_id = accounts.id
    ) AS "Total Revenue"
  FROM accounts
  WHERE accounts.company_id = ?
  {{account}}
  ORDER BY accounts.name ASC;
