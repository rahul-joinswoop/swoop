
  SELECT
    jobs.id AS "Swoop ID",
    (
      SELECT TO_CHAR(COALESCE(adjusted_dttm, last_set_dttm) AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
        AND deleted_at IS NULL
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatch Date",
    sites.name AS "Site",
    accounts.name AS "Account",
    jobs.po_number AS "PO",
    jobs.status AS "Status",
    CONCAT(drivers.first_name, ' ', drivers.last_name) AS "Driver",
    trucks.name AS "Truck",
    vehicle_categories.name AS "Class Type",
    CONCAT_WS(' ', partner_dispatchers.first_name, partner_dispatchers.last_name) AS "Dispatcher",
    service_codes.name AS "Service",
    (
      SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI')
      FROM audit_job_statuses
      WHERE
        job_status_id = 2
        AND job_id = jobs.id
        AND deleted_at IS NULL
      ORDER BY id DESC LIMIT 1
    ) AS "Dispatched",
    TO_CHAR(
      ((SELECT COALESCE(adjusted_dttm, last_set_dttm) FROM audit_job_statuses WHERE job_status_id = 7 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1) -
      (SELECT COALESCE(adjusted_dttm, last_set_dttm) FROM audit_job_statuses WHERE job_status_id = 3 AND job_id = jobs.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1)),
      'DD:HH24:MI'
    ) AS "Total Job Time",
    ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) AS "Total Miles",
    CAST(COALESCE((
      SELECT SUM(invoicing_line_items.net_amount) + SUM(invoicing_line_items.tax_amount) AS sum_amount
      FROM invoicing_line_items
      WHERE
        invoices.id = invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
     GROUP BY invoices.id), 0) AS DECIMAL(14, 2)
    ) AS "Invoice Total",
    job_reject_reasons.text as "Reject Reason",
    jobs.reject_reason_info as "Reject Comment"
  FROM jobs
  LEFT JOIN users ON users.id = ?
  LEFT JOIN accounts ON accounts.id = jobs.account_id
  LEFT JOIN users AS drivers ON jobs.rescue_driver_id = drivers.id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN users AS partner_dispatchers ON jobs.partner_dispatcher_id = partner_dispatchers.id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN invoicing_ledger_items AS invoices ON invoices.job_id = jobs.id AND invoices.deleted_at IS NULL AND invoices.sender_id = ? AND invoices.type='Invoice'
  LEFT JOIN job_reject_reasons on jobs.reject_reason_id = job_reject_reasons.id
  LEFT JOIN sites on jobs.site_id = sites.id
  LEFT JOIN vehicle_categories on jobs.invoice_vehicle_category_id = vehicle_categories.id
  WHERE
    jobs.rescue_company_id=?
    AND jobs.deleted_at IS NULL
    AND jobs.status != 'Reassigned'
    AND (
      (
        SELECT COALESCE(adjusted_dttm, last_set_dttm, jobs.created_at)
        FROM audit_job_statuses
        WHERE
          (job_status_id = 2 OR (jobs.status = 'Rejected' AND job_status_id = 10))
          AND job_id = jobs.id
          AND deleted_at IS NULL
        ORDER BY id DESC
        LIMIT 1
      ) >= ?
      AND (
        SELECT COALESCE(adjusted_dttm, last_set_dttm, jobs.created_at)
        FROM audit_job_statuses
        WHERE
          (job_status_id = 2 OR (jobs.status = 'Rejected' AND job_status_id = 10))
          AND job_id = jobs.id
          AND deleted_at IS NULL
        ORDER BY id DESC
        LIMIT 1
      ) < ?
    )
    {{driver}}
    {{vehicle}}
    {{account}}
    {{service}}
    {{dispatcher}}
    {{site}}
    {{vehicle_category}}
ORDER BY jobs.id DESC;
