select
invoices.id as "Invoice Id",
provider.vendor_code as "Vendor",
to_char((CASE WHEN (jobs.scheduled_for is not null) THEN jobs.scheduled_for ELSE jobs.created_at END) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YY') as "Doc Date",
to_char(now() AT TIME ZONE 'America/Los_Angeles','MM/DD/YY') as "Posting Date",
concat('SW', jobs.id) as "Invoice Number",

CAST((select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
  from invoicing_line_items
      where invoices.id=invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
    group by invoices.id)
  AS DECIMAL(14, 2)) as "Invoice Amount",
(CASE WHEN (select count(*) as cnt
                from companies_features
                left join features on companies_features.feature_id=features.id
                where companies_features.company_id=jobs.rescue_company_id
                and features.name='Canadian AP')>0
   THEN 'CAD' else 'USD' END) as "Currency",
-- purposley like this.
'' as "Tax Code",
'0.00' as "Tax Amount",
'' as "Text field",
'' as "Payment Reference",
(CASE
  WHEN departments.name = 'Roadside' AND customer_types.name = 'Body Shop' THEN '30150300'
  WHEN departments.name = 'Logistics' THEN '30167000'
  WHEN departments.name = 'Service Center' THEN '30150100'
  WHEN departments.name = 'Other' THEN '30153000'
  WHEN customer_types.name IN ('Warranty', 'Goodwill') THEN '30153000'
  WHEN customer_types.name = 'Body Shop' THEN '30150001'
  WHEN customer_types.name =  'P Card' THEN 'COGSSVR'
  ELSE '30150000'
END) AS "Cost Center",
(CASE
  WHEN customer_types.name in ('Warranty', 'P Card') THEN '224520'
  WHEN customer_types.name = 'Goodwill' THEN '640001'
  WHEN customer_types.name = 'Body Shop' THEN '685150'
  ELSE '610600'
END) AS "GL Account",
(CASE WHEN customer_types.name in ('Warranty', 'P Card', 'Body Shop') THEN 'S5200' ELSE 'C9000' END) as "SIO",
(CASE
  WHEN customer_types.name = 'Body Shop' THEN '787'
  WHEN sites.site_code is NULL THEN provider.location_code
  ELSE sites.site_code
END) as "Location",
vehicles.vin as "VIN Number",
'' as "Reservation Number",
(CASE
  WHEN customer_types.name = 'Body Shop' THEN 'AUBNW'
  ELSE departments.code
END) AS "Budget Code"
from
 invoicing_ledger_items as invoices
LEFT JOIN jobs ON invoices.job_id=jobs.id

LEFT JOIN rescue_providers AS provider
     ON provider.site_id = jobs.site_id
     AND provider.company_id = jobs.fleet_company_id
     AND provider.deleted_at is NULL
LEFT JOIN drives ON jobs.driver_id=drives.id
LEFT JOIN vehicles ON drives.vehicle_id=vehicles.id
LEFT JOIN departments ON jobs.department_id = departments.id
LEFT JOIN sites ON jobs.fleet_site_id = sites.id
LEFT JOIN customer_types ON customer_types.id = jobs.customer_type_id
WHERE
  invoices.state = 'fleet_approved'
  AND invoices.type='Invoice'
  AND invoices.deleted_at IS NULL
  AND invoices.created_at BETWEEN ? AND ?
  AND jobs.fleet_company_id = ?
  {{rescue_company}}
order by jobs.id
