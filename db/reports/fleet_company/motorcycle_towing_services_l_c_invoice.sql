SELECT
invoices.id as "Invoice Id",
jobs.id AS "Job Id",
to_char((CASE WHEN (jobs.scheduled_for IS NOT null) THEN jobs.scheduled_for ELSE jobs.created_at END) AT TIME zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YY') AS "Date",
service_codes.name AS "Service",
jobs.policy_number AS "Policy Number",
departments.name AS "Department",
CONCAT(customer.first_name, ' ', customer.last_name) AS "Customer Name",
pickup_location.address AS "Pickup Location",
rescue_company.name AS "Partner",
jobs.po_number AS "PO#",
jobs.ref_number AS "Reference Number",
customers.member_number AS "Member Number",
(CASE WHEN (dropoff_place.type = 'PoiSite' AND dropoff_place.name IS NOT NULL) THEN (dropoff_place.name || ' - ' || dropoff_location.address) ELSE dropoff_location.address END) AS "Drop Location",
CAST((SELECT sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) AS sum_amount
  FROM invoicing_line_items
      WHERE invoices.id=invoicing_line_items.ledger_item_id
        AND invoicing_line_items.deleted_at IS NULL
    GROUP BY invoices.id)
  AS DECIMAL(14, 2)) AS "Invoice Amount",
payment_type.name AS "Payment Type"
FROM
invoicing_ledger_items AS invoices
LEFT JOIN jobs ON invoices.job_id=jobs.id
LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
LEFT JOIN departments ON jobs.department_id = departments.id
LEFT JOIN locations AS pickup_location ON pickup_location.id=jobs.service_location_id
LEFT JOIN locations AS dropoff_location ON dropoff_location.id=jobs.drop_location_id
LEFT JOIN sites AS dropoff_place ON dropoff_place.id = dropoff_location.site_id
LEFT JOIN rescue_providers AS provider
  ON provider.site_id = jobs.site_id
  AND provider.company_id  = jobs.fleet_company_id
  AND provider.deleted_at IS NULL
LEFT JOIN companies AS rescue_company ON rescue_company.id = provider.provider_id
LEFT JOIN drives ON jobs.driver_id=drives.id
LEFT JOIN users AS customer ON customer.id = drives.user_id
LEFT JOIN customers ON customer.customer_id=customers.id
LEFT JOIN customer_types AS payment_type ON payment_type.id = jobs.customer_type_id
where
  invoices.state = 'fleet_approved'
  AND invoices.type='Invoice'
  AND invoices.deleted_at is null
  AND invoices.created_at BETWEEN ? AND ?
  AND jobs.fleet_company_id = ?
  {{rescue_company}}
order by jobs.id
