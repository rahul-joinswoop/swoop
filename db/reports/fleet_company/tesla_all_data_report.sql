
SELECT
  "Job ID",
  "Date (Pacific)",
  "Time (Pacific)",
  "Customer",
  "Phone",
  "Location Type",
  "Partner",
  "Make",
  "Model",
  "pickup location",
  "City",
  "State",
  "Zip",
  "Pickup",
  "Dropoff",
  dropoff_location_type.name AS "Drop Off Location Type",
  "Service",
  "Scheduled",
  "EN Route Miles",
  "Pick Up To Drop Off",
  "Return Miles",
  "P2P Miles",
  "ETA",
  "ATA",
  "ATA - ETA",
  "Handle Time",
  "Status",
  "Payment Type",
  "Invoiced",
  "Partner Live",
  "Partner Open",
  "Partner Live and Open",
  "Platform",
  "Invoice Status",
  vin,
  "Symptom",
  "P2P Job Time Estimate",
  "Port-to-Complete Job Time Estimate",
  "Department",
  "Fleet Dispatcher",
  "Partner Dispatcher",
  "Created",
  "Assigned",
  "Accepted",
  "Dispatched",
  "En Route",
  "On Site",
  "Towing",
  "Tow Destination",
  "Completed",
  "GOA",
  "Released",
  "Canceled",
  "Digital Dispatch Time",
  "Digital En Route",
  "En Route -> Completed",
  "Job Details",
  "Fleet Notes",
  "Invoice Notes",
  "Reject Reason",
  "Reject Comment",
  "Rescue Vehicle Name",
  (CASE
    WHEN departments.name = 'Roadside' AND customer_types.name = 'Body Shop' THEN '30150300'
    WHEN departments.name = 'Logistics' THEN '30167000'
    WHEN departments.name = 'Service Center' THEN '30150100'
    WHEN departments.name = 'Other' THEN '30153000'
    WHEN customer_types.name IN ('Warranty', 'Goodwill') THEN '30153000'
    WHEN customer_types.name = 'Body Shop' THEN '30150001'
    WHEN customer_types.name = 'P Card' THEN 'COGSSVR'
    ELSE '30150000'
  END) AS "Cost Center",
  (CASE
    WHEN customer_types.name IN ('Warranty', 'P Card') THEN '224520'
    WHEN customer_types.name = 'Goodwill' THEN '640001'
    WHEN customer_types.name = 'Body Shop' THEN '685150'
    ELSE '610600'
  END) AS "GL Account",
  (CASE WHEN customer_types.name in ('Warranty', 'P Card', 'Body Shop') THEN 'S5200' ELSE 'C9000' END) as "SIO",
  (CASE
    WHEN customer_types.name = 'Body Shop' THEN 'AUBNW'
    ELSE departments.code
  END) AS "Budget Code",
  (CASE
    WHEN customer_types.name = 'Body Shop' THEN '787'
    WHEN sites.site_code is NULL THEN provider.location_code
    ELSE sites.site_code
  END) AS "Location"
FROM fleet_all_data_view
LEFT JOIN jobs ON jobs.id = "Job ID"
LEFT JOIN departments ON departments.id = jobs.department_id
LEFT JOIN customer_types ON customer_types.id = jobs.customer_type_id
LEFT JOIN rescue_providers AS provider
  ON  provider.site_id = jobs.site_id
  AND provider.company_id = jobs.fleet_company_id
  AND provider.deleted_at IS NULL
LEFT JOIN sites ON jobs.fleet_site_id = sites.id
LEFT JOIN locations AS dropoff_location ON jobs.drop_location_id = dropoff_location.id
LEFT JOIN location_types AS dropoff_location_type ON dropoff_location.location_type_id = dropoff_location_type.id
WHERE "Status" in ('Completed', 'GOA', 'Released', 'Canceled')
  AND fleet_all_data_view.created_at >= ?
  AND fleet_all_data_view.created_at < ?
  AND fleet_all_data_view.fleet_company_id = ?
  {{rescue_company}};
