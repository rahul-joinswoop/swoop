
SELECT
  "Job ID",
  "Date",
  "Time",
  "Site",
  "Class Type",
  "Service Requested",
  "Status",
  "Pickup City",
  "ETA",
  "ATA",
  "ATA" - "ETA" AS "ATA-ETA",
  CASE WHEN ("ATA" - "ETA") < 0 THEN 1 ELSE 0 END as "ATA-ETA < 0",
  CASE WHEN ("ATA" - "ETA") <= 10 THEN 1 ELSE 0 END as "ATA-ETA <= 10"
FROM (
  SELECT
    jobs.id AS "Job ID",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
    sites.name AS "Site",
    service_codes.name AS "Service Requested",
    jobs.status AS "Status",
    pickup.city AS "Pickup City",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (SELECT (time - created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
    ) / 60) AS NUMERIC), 0) AS "ETA",
    ROUND(CAST((EXTRACT(EPOCH FROM
      (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
      (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
    ) / 60) AS NUMERIC), 0) as "ATA",
    '' as "Variance",
    vehicle_categories.name AS "Class Type"
  FROM jobs
    LEFT JOIN users ON users.id = ?
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
    LEFT JOIN sites ON sites.id = jobs.fleet_site_id
    LEFT JOIN vehicle_categories on vehicle_categories.id = jobs.invoice_vehicle_category_id
  WHERE
    jobs.deleted_at IS NULL
    AND (
      jobs.fleet_site_id IS NULL
      OR (
        jobs.fleet_site_id = ANY (
          CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
            ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
          ELSE
            ARRAY(SELECT id FROM sites WHERE company_id = ?)
          END
        )
      )
    )
    AND jobs.status IN ('Completed','Released')
    AND jobs.scheduled_for IS NULL
    AND fleet_company_id = ?
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    {{site}}
  ORDER BY jobs.id DESC
) as t1
