
SELECT
  DISTINCT(jobs.id),
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
  TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
  jobs.email AS "Customer Email",
  sites.name AS "Site",
  class_type.name AS "Class Type",
  pickup.address AS "Pickup Address",
  dropoff.address AS "Drop off Address",
  vehicles.make AS "Make",
  vehicles.model AS "Model",
  vehicles.year AS "Year",
  vehicles.vin AS "VIN",
  service_codes.name AS "Service Requested",
  symptoms.name AS "Symptom",
  partner.name AS "Partner",
  (CASE WHEN issues.key = 'redispatched' THEN 'Y' ELSE 'N' END) AS "Redispatched",
  vehicle_categories.name AS "Truck Type",
  jobs.status AS "Status",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 10
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Assigned",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 11
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Accepted",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 2
      AND job_id = jobs.id
    ORDER BY id DESC LIMIT 1
  ) AS "Dispatched",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 3
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "En Route",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 4
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "On Site",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 6
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Tow Destination",
  (
    SELECT TO_CHAR(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY HH24:MI')
    FROM audit_job_statuses
    WHERE
      job_status_id = 7
      AND job_id = jobs.id
    ORDER BY id DESC
    LIMIT 1
  ) AS "Completed",
  ROUND(estimates.meters_ab * 0.000621371, 1) AS "EN Route Miles",
  ROUND(estimates.meters_bc * 0.000621371, 1) AS "Pick Up To Drop Off",
  ROUND((CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_ca ELSE estimates.meters_ba END) * 0.000621371, 1) AS "Return Miles",
  ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) as "P2P Miles"
FROM jobs
  LEFT JOIN locations as pickup ON pickup.id = jobs.service_location_id
  LEFT JOIN locations as dropoff ON dropoff.id = jobs.drop_location_id
  LEFT JOIN service_codes ON service_codes.id = jobs.service_code_id
  LEFT JOIN drives ON jobs.driver_id = drives.id
  LEFT JOIN vehicles ON drives.vehicle_id = vehicles.id
  LEFT JOIN locations policy_addr ON policy_addr.id = jobs.policy_location_id
  LEFT JOIN estimates ON estimates.id = jobs.estimate_id
  LEFT JOIN symptoms ON jobs.symptom_id = symptoms.id
  LEFT JOIN companies partner ON partner.id = jobs.rescue_company_id
  LEFT JOIN vehicles AS trucks ON jobs.rescue_vehicle_id = trucks.id
  LEFT JOIN vehicle_categories ON trucks.vehicle_category_id = vehicle_categories.id
  LEFT JOIN vehicle_categories class_type on class_type.id = jobs.invoice_vehicle_category_id
  LEFT JOIN explanations ON explanations.job_id = jobs.id
  LEFT JOIN reasons ON reasons.id = explanations.reason_id
  LEFT JOIN issues ON issues.id = reasons.issue_id
  LEFT JOIN sites ON sites.id = jobs.fleet_site_id
  LEFT JOIN users ON users.id = ?
WHERE
  jobs.deleted_at IS NULL
  AND (
    jobs.fleet_site_id IS NULL
    OR (
      jobs.fleet_site_id = ANY (
        CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
          ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
        ELSE
          ARRAY(SELECT id FROM sites WHERE company_id = ?)
        END
      )
    )
  )
  AND jobs.status IN ('Completed', 'Released', 'Canceled', 'GOA')
  AND jobs.fleet_company_id = ?
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
  {{site}}
ORDER BY jobs.id DESC;
