
select
  DISTINCT(jobs.id),
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  pickup.address as "Full Address",

  -- Policy Addr
  jobs.policy_number   as "Policy Number",
  pcc_claim.claim_number AS "Claim Number",

  CONCAT_WS(' ',vehicles.make,vehicles.model,vehicles.year) as "Vehicle Info",
  service_codes.name as "Service Requested",
  jobs.status as "Status",


  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
  round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",

  ROUND((estimates.meters_ab + (CASE WHEN estimates.drop_location_id IS NOT NULL THEN estimates.meters_bc + estimates.meters_ca ELSE estimates.meters_ba END)) * 0.000621371, 1) as "P2P Miles"
from jobs
  LEFT JOIN locations as pickup
    ON pickup.id=jobs.service_location_id
  LEFT JOIN service_codes
    ON service_codes.id=jobs.service_code_id
  LEFT JOIN drives
    ON jobs.driver_id=drives.id
  LEFT JOIN vehicles
    ON drives.vehicle_id=vehicles.id
  LEFT JOIN estimates
    ON estimates.id = jobs.estimate_id
  LEFT JOIN pcc_claims pcc_claim
    ON pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.status IN ('Completed', 'Released', 'Canceled', 'GOA') and
  jobs.fleet_company_id=(SELECT id FROM companies WHERE name = '21st Century Insurance')
  AND jobs.created_at >= ?
  AND jobs.created_at < ?
order by jobs.id desc;