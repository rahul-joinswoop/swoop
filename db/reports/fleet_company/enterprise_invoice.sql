select
invoices.id as "Invoice Id",
partner.name as "TOW VENDOR",
jobs.id as "INVOICE NUMBER",
to_char((CASE WHEN (jobs.scheduled_for is not null) THEN jobs.scheduled_for ELSE jobs.created_at END) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YY') as "DATE OF TOW",
CONCAT_WS(' ', vehicles.year, vehicles.make, vehicles.model) as "VEHICLE DESCRIPTION",

jobs.unit_number as "UNIT",
vehicles.license as "LICENSE",
vehicles.vin as "VIN",
jobs.ref_number as "CLAIM",
CAST((select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
  from invoicing_line_items
      where invoices.id=invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
    group by invoices.id)
  AS DECIMAL(14, 2)) as "INVOICE TOTAL",
jobs.accounting_code as "ACCOUNTING CODE"
from
 invoicing_ledger_items as invoices
left join jobs on invoices.job_id=jobs.id
left join drives on jobs.driver_id=drives.id
left join vehicles on drives.vehicle_id=vehicles.id
left join companies AS partner on jobs.rescue_company_id = partner.id
left join customer_types as claims on jobs.customer_type_id = claims.id
where
  invoices.state = 'fleet_approved'
  AND invoices.type='Invoice'
  AND invoices.deleted_at is null
  AND invoices.created_at BETWEEN ? AND ?
  AND jobs.fleet_company_id = ?
  {{rescue_company}}
order by jobs.id
