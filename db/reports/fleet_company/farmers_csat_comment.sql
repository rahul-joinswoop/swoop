
select
  jobs.id as "Dispatch ID",
  reviews.nps_question1 as "Question 1 - How well informed",
  reviews.nps_question2 as "Question 2 - How on time",
  reviews.nps_question3 as "Question 3 - NPS",
  case when (reviews.type='Reviews::Typeform') then reviews.nps_feedback else (case when (reviews.type='NpsReview') then
    (select string_agg(body,', ')
     from (select body
           from reviews
           left join twilio_replies on reviews.id=twilio_replies.target_id and twilio_replies.target_type='Review'
           where reviews.job_id=jobs.id order by twilio_replies.id) as reply_bodies)
   else null end) end as "Feedback/Comments"
from jobs
    join reviews
      ON jobs.id = reviews.job_id
where
  jobs.deleted_at is null and
  reviews.nps_feedback is not null and
  reviews.nps_feedback != '' and
  jobs.created_at >= ? and
  jobs.created_at < ? and
    jobs.fleet_company_id = (SELECT id FROM companies WHERE name = 'Farmers Insurance')
order by reviews.created_at desc;
