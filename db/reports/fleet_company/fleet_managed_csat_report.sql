
  SELECT
    jobs.id AS "Dispatch ID",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') AS "Date",
    TO_CHAR(jobs.created_at AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'HH24:MI') AS "Time",
    sites.name AS "Site",
    CASE WHEN (pickup.state IS NOT NULL) THEN pickup.state ELSE (CASE WHEN original_pickup.state IS NOT NULL THEN original_pickup.state ELSE (provider_location.state) END) END AS "State",
    CONCAT(customer.first_name, CONCAT(' ', customer.last_name)) AS "Customer",
    customer.phone AS "Phone",
    jobs.status AS "Status",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN 'Scheduled' ELSE NULL END AS "Scheduled",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC limit 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ETA",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
        (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ATA",
    (CASE WHEN (reviews.type = 'Reviews::Typeform' AND reviews.branding IS TRUE) THEN 'Branded Link' ELSE
      (CASE WHEN (reviews.type = 'Reviews::Typeform') THEN 'Web Link' ELSE
        (CASE WHEN (reviews.type = 'NpsReview') THEN 'SMS' ELSE NULL END)
      END)
    END) AS "Review Type",
/*    fleet_dispatchers.email AS "Fleet Dispatcher", */
    reviews.rating as "5* Rating",
    reviews.nps_question1 AS "Question 1",
    reviews.nps_question2 AS "Question 2",
    reviews.nps_question3 AS "Question 3",
    reviews.nps_question4 AS "Question 4",
    CASE
    WHEN (reviews.type = 'Reviews::Typeform') THEN reviews.nps_feedback
    WHEN (reviews.type = 'Reviews::SurveyReview') THEN reviews.nps_feedback
    ELSE (CASE WHEN (reviews.type = 'NpsReview') THEN
      (SELECT string_agg(body, ', ')
      FROM (
       SELECT body
       FROM reviews
       LEFT JOIN twilio_replies ON reviews.id = twilio_replies.target_id AND twilio_replies.target_type = 'Review'
       WHERE reviews.job_id = jobs.id
       ORDER BY twilio_replies.id
      ) AS reply_bodies)
    ELSE NULL END) END AS "Feedback/Comments",
    TO_CHAR(audit_sms.clicked_dttm AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY HH24:MI') AS "Link Clicked"
  FROM jobs
    JOIN reviews ON jobs.id = reviews.job_id
    LEFT JOIN users ON users.id = ?
    LEFT JOIN drives ON drives.id = jobs.driver_id
    LEFT JOIN users customer ON customer.id = drives.user_id
    LEFT JOIN users fleet_dispatchers ON jobs.fleet_dispatcher_id = fleet_dispatchers.id
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN locations AS original_pickup ON original_pickup.id = jobs.original_service_location_id
    LEFT JOIN companies provider ON jobs.rescue_company_id = provider.id
    LEFT JOIN locations provider_location ON provider.location_id = provider_location.id
    LEFT JOIN jobs parent ON jobs.parent_id = parent.id
    LEFT JOIN sites ON sites.id = jobs.fleet_site_id
    LEFT JOIN audit_sms ON audit_sms.job_id = jobs.id AND audit_sms.type = 'Sms::BrandedReviewLink'
  WHERE
    jobs.deleted_at IS NULL
    AND (
      jobs.fleet_site_id IS NULL
      OR (
        jobs.fleet_site_id = ANY (
          CASE WHEN (SELECT site_id FROM user_sites WHERE user_id = ? LIMIT 1) IS NOT NULL THEN
            ARRAY(SELECT site_id FROM user_sites WHERE user_id = ?)
          ELSE
            ARRAY(SELECT id FROM sites WHERE company_id = ?)
          END
        )
      )
    )
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    AND jobs.fleet_company_id = ?
    AND jobs.status != 'GOA'
    AND jobs.status != 'Canceled'
    {{site}}
  ORDER BY reviews.created_at DESC;
