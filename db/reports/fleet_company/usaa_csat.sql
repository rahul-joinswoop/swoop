
  SELECT
    jobs.id AS "Dispatch ID",
    to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
    to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
    jobs.policy_number    as "Policy Number",
    CASE WHEN (pickup.state IS NOT NULL) THEN pickup.state ELSE (CASE WHEN original_pickup.state IS NOT NULL THEN original_pickup.state ELSE (provider_location.state) END) END AS "State",
    CONCAT(customer.first_name, CONCAT(' ', customer.last_name)) AS "Customer",
    customer.phone AS "Phone",
    jobs.status AS "Status",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN 'Scheduled' ELSE NULL END AS "Scheduled",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC limit 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ETA",
    CASE WHEN jobs.scheduled_for IS NOT NULL THEN NULL ELSE
      ROUND(CAST((EXTRACT(EPOCH FROM
        (SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END FROM (SELECT last_set_dttm, adjusted_dttm FROM audit_job_statuses WHERE job_status_id = 4 AND job_id = jobs.id ORDER BY id DESC LIMIT 1) AS onsite_ajs) -
        (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1)
      ) / 60) AS NUMERIC), 0)
    END AS "ATA",
    'Web Link'  AS "Review Type",
    fleet_dispatchers.email as "Fleet Dispatcher",
    TO_CHAR(audit_sms.clicked_dttm AT TIME ZONE 'UTC' AT TIME ZONE COALESCE(users.timezone, 'US/Central'), 'MM/DD/YYYY HH24:MI') AS "Link Clicked",
    (
      SELECT int_value
      FROM survey_results
      JOIN survey_questions ON survey_results.survey_question_id = survey_questions.id
      WHERE
        survey_questions.order=1
        AND survey_results.job_id = jobs.id
        AND survey_questions.survey_id = reviews.survey_id
      ORDER BY survey_results.created_at DESC
      LIMIT 1
    ) AS "Question 1 - Phone Rep",
    (
      SELECT int_value
      FROM survey_results
      JOIN survey_questions ON survey_results.survey_question_id = survey_questions.id
      WHERE
        survey_questions.order=2
        AND survey_results.job_id = jobs.id
        AND survey_questions.survey_id = reviews.survey_id
      ORDER BY survey_results.created_at DESC
      LIMIT 1
    ) AS "Question 2 - Provider Courtesy",
    (
      SELECT int_value
      FROM survey_results
      JOIN survey_questions ON survey_results.survey_question_id = survey_questions.id
      WHERE
        survey_questions.order=3
        AND survey_results.job_id = jobs.id
        AND survey_questions.survey_id = reviews.survey_id
      ORDER BY survey_results.created_at DESC
      LIMIT 1
    ) AS "Question 3 - Satisfaction",
    (
      SELECT string_value
      FROM survey_results JOIN survey_questions ON survey_results.survey_question_id = survey_questions.id
      WHERE
        survey_questions.order=4
        AND survey_results.job_id = jobs.id
        AND survey_questions.survey_id = reviews.survey_id
      ORDER BY survey_results.created_at DESC
      LIMIT 1
    ) AS "Question 4 - Feedback/Comments",
    (
      SELECT
        STRING_AGG(issues.name, E'\n')
      FROM explanations
        JOIN reasons ON reasons.id = explanations.reason_id
        JOIN issues ON issues.id = reasons.issue_id
      WHERE explanations.job_id = jobs.id
      GROUP BY explanations.job_id
    ) AS "Issues",
    (
      SELECT
        STRING_AGG(CASE WHEN explanations.reason_info IS NOT NULL THEN CONCAT(reasons.name, ': ', explanations.reason_info) ELSE reasons.name END, E'\n')
      FROM explanations
        JOIN reasons ON reasons.id = explanations.reason_id
        JOIN issues ON issues.id = reasons.issue_id
      WHERE explanations.job_id = jobs.id
      GROUP BY explanations.job_id
    ) AS "Reasons"
  FROM jobs
    JOIN reviews ON jobs.id = reviews.job_id
    LEFT JOIN users ON users.id = ?
    LEFT JOIN drives ON drives.id = jobs.driver_id
    LEFT JOIN users customer ON customer.id = drives.user_id
    LEFT JOIN users fleet_dispatchers ON jobs.fleet_dispatcher_id = fleet_dispatchers.id
    LEFT JOIN locations AS pickup ON pickup.id = jobs.service_location_id
    LEFT JOIN locations AS original_pickup ON original_pickup.id = jobs.original_service_location_id
    LEFT JOIN companies provider ON jobs.rescue_company_id = provider.id
    LEFT JOIN locations provider_location ON provider.location_id = provider_location.id
    LEFT JOIN jobs parent ON jobs.parent_id = parent.id
    LEFT JOIN audit_sms ON audit_sms.job_id = jobs.id AND audit_sms.type = 'Sms::BrandedReviewLink'
  WHERE
    jobs.deleted_at IS NULL
    AND jobs.created_at >= ?
    AND jobs.created_at < ?
    AND jobs.fleet_company_id = (SELECT id FROM companies WHERE name = 'USAA')
    AND jobs.status != 'GOA'
    AND jobs.status != 'Canceled'
  ORDER BY reviews.created_at DESC;
