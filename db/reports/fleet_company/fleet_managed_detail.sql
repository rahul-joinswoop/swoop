
select
  jobs.id as "Job ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY') as "Date(Pacific)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','HH24:MI') as "Time(Pacific)",
  fleet_company.name as "Fleet Company",
  pickup.city as "City",
  case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
  trim(both ' ' from concat(provider.name, ' ', sites.name)) as "Towing Company",
  to_char(((SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
 - jobs.created_at),'HH24:MI:SS') as "Handle Time",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ETA",
  round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ATA",
  service_codes.name as "Service",
  jobs.status,
  CAST(
    (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
     from invoicing_line_items
     where invoicing_ledger_items.id=invoicing_line_items.ledger_item_id
       and invoicing_line_items.deleted_at is null
     group by invoicing_ledger_items.id)
  AS DECIMAL(14, 2)) as "Invoiced",
  reviews.rating as "5 Star",
  reviews.nps_question1 as "Customer review: Q1",
  reviews.nps_question2 as "Customer review: Q2",
  reviews.nps_question3 as "Customer review: Q3",
  reviews.nps_feedback as "Customer review comments"
from jobs
  left join reviews on jobs.id=reviews.job_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join companies as provider on provider.id=jobs.rescue_company_id
  left join companies as fleet_company on fleet_company.id=jobs.fleet_company_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
  left join locations provider_location on provider.location_id=provider_location.id
  left join invoicing_ledger_items on (jobs.id=invoicing_ledger_items.job_id and invoicing_ledger_items.sender_id=jobs.rescue_company_id AND invoicing_ledger_items.type='Invoice')
  left join sites on sites.company_id=provider.id and sites.name is not null
where
  invoicing_ledger_items.deleted_at is null and
  jobs.status in ('Completed','Canceled','GOA','Released') and
  fleet_company.type='FleetCompany' and
  fleet_company.in_house is null and
  fleet_company_id not in (1,6,9,41,45,46,47,48,49,472,473,474)
and
  jobs.created_at >= ? and
  jobs.created_at < ?
order by jobs.id desc;
