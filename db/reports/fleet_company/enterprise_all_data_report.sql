
select
  "Job ID",
  "Date (Pacific)",
  "Time (Pacific)",
  "Partner",
  "Make",
  "Model",
  "pickup location",
  zip,
  "Pickup",
  "Dropoff",
  "Service",
  "EN Route Miles",
  "Pick Up To Drop Off",
  "Return Miles",
  "P2P Miles",
  "ETA",
  "ATA",
  "Status",
  "Payment Type",
  "Invoiced",
  "Invoice Status",
  vin,
  "Unit Number",
  "Claim Number",
  "Symptom" as "Tow Category",
  "P2P Job Time Estimate",
  "Port-to-Complete Job Time Estimate",
  "Fleet Dispatcher"
FROM fleet_all_data_view
where
  "Status" in ('Completed', 'GOA', 'Released', 'Canceled')
  and created_at >= ?
  and created_at < ?
  and fleet_company_id=?
  {{rescue_company}};
