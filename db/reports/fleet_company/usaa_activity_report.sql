


WITH audit_job_status_latest_pivot AS (SELECT *
      FROM crosstab(
            '
      WITH  status_name as ( SELECT id, name FROM job_statuses group by 1 ),
      audit_job_status_latest as (
            select job_id, job_status_id, status_name.name, MAX(audit_job_statuses.id) as max_id
            FROM status_name
            LEFT JOIN public.audit_job_statuses on audit_job_statuses.job_status_id = status_name.id
            GROUP BY 1, 2, 3
            order by 4 DESC ),

      audit_2 as (SELECT audit_job_statuses.*, audit_job_status_latest.name as status_name
            , CASE WHEN adjusted_dttm is not null THEN adjusted_dttm      ELSE last_set_dttm END as dtmm
            FROM audit_job_statuses
            LEFT JOIN audit_job_status_latest ON max_id = audit_job_statuses.id
             where deleted_at is null
      )

      SELECT job_id, status_name, dtmm FROM audit_2 ORDER BY 1,2 '
            , 'VALUES (''Pending''), (''Dispatched''), (''En Route''), (''On Site''), (''Towing''), (''Tow Destination''), (''Completed''), (''Canceled''), (''Created''), (''Assigned''), (''Accepted''), (''Rejected''), (''Reassign''), (''Initial''), (''Unassigned''), (''Reassigned''), (''Stored''), (''Released''), (''Submitted''), (''ETA Rejected''), (''GOA''), (''Expired''), (''Auto Assigning'')')
      as dttm ("job_id" int, "pending" timestamp, "dispatched" timestamp, "en_route" timestamp, "on_site" timestamp, "towing" timestamp, "tow_Destination" timestamp, "completed" timestamp, "canceled" timestamp, "created" timestamp, "assigned" timestamp, "accepted" timestamp, "rejected" timestamp, "reassign" timestamp, "initial" timestamp, "unassigned" timestamp, "reassigned" timestamp, "stored" timestamp, "released" timestamp, "submitted" timestamp, "eta_rejected" timestamp, "goa" timestamp, "expired" timestamp, "auto_assigning" text)
       )
  ,  survey_scores AS (SELECT *
FROM crosstab( 'select distinct on (job_id,survey_questions.order) job_id, survey_questions.order, int_value from survey_results left join survey_questions on survey_results.survey_question_id=survey_questions.id order by job_id,survey_questions.order,survey_results.id DESC')
     AS final_result(job_id integer, "q1" integer,"q2"  integer, "q3" integer,"q4" integer,"q5" integer,"q6" integer)
       )
SELECT
  jobs.id  AS "Swoop ID",
  TO_CHAR((audit_job_status_latest_pivot.created )::timestamptz AT TIME ZONE 'US/Central', 'YYYY-MM-DD HH24:MI:SS') AS "Created (Central)",
  jobs.policy_number  AS "Policy Number",
  customer.first_name || ' ' || customer.last_name  AS "Customer Name",
  customer.phone  AS "Customer Phone",
  pickup.state  AS "Pickup State",
  pickup.city  AS "Pickup City",
  pickup.zip  AS "Pickup Zip",
        pickup_location_type.name as "Location Type",
  COALESCE(estimates.meters_bc,0)*0.000621371  AS "Pickup To Drop Off Mileage",
  vehicles.make  AS "Vehicle Make",
  vehicles.model  AS "Vehicle Model",
  vehicles.year  AS "Vehicle Year",
  service_codes.name  AS "Service Type",
  symptoms.name  AS "Symptom",
  jobs.status  AS "Job Status",
  rescue_company.name  AS "Service Provider",
  CASE WHEN (select case when count(*) > 0 then true else false end as cnt from explanations
left join reasons on explanations.reason_id=reasons.id
where
  job_id = jobs.id
  and issue_id=(select id from issues where key = 'redispatched'))  THEN 'Yes' ELSE 'No' END
 AS "Re-Dispatched",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0)  AS "Partner ETA",
  round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0)  AS "Partner ATA",
TO_CHAR((
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1) -
    (
      WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs
          WHERE inner_jobs.id = jobs.id
        UNION
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs, parents
          WHERE inner_jobs.id = parents.parent_id
      )
      SELECT adjusted_created_at
      FROM parents
      ORDER BY id ASC
      LIMIT 1
    )
  ),'HH24:MI:SS') as "Handle Time",
TO_CHAR
((
(select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=7 and job_id=jobs.id order by id desc limit 1) as completed_ajs)
-
(select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs)
)::interval,
'HH24:MI:SS')
 AS "Service Time",
  survey_scores.q1  AS "Q1 Customer Review",
  survey_scores.q2  AS "Q2 Customer Review",
  survey_scores.q3  AS "Q3 Customer Review"
FROM public.jobs  AS jobs
LEFT JOIN public.companies  AS fleet_companies ON jobs.fleet_company_id = fleet_companies.id
LEFT JOIN public.companies  AS rescue_company ON jobs.rescue_company_id = rescue_company.id
LEFT JOIN public.locations  AS pickup ON jobs.service_location_id = pickup.id
left join location_types as pickup_location_type on pickup_location_type.id = pickup.location_type_id
LEFT JOIN public.service_codes  AS service_codes ON jobs.service_code_id = service_codes.id
LEFT JOIN public.estimates  AS estimates ON jobs.estimate_id = estimates.id
LEFT JOIN public.drives  AS drives ON jobs.driver_id = drives.id
LEFT JOIN public.vehicles  AS vehicles ON drives.vehicle_id = vehicles.id
LEFT JOIN public.symptoms  AS symptoms ON jobs.symptom_id = symptoms.id
LEFT JOIN public.users  AS customer ON drives.user_id = customer.id
LEFT JOIN audit_job_status_latest_pivot ON jobs.id = audit_job_status_latest_pivot.job_id
LEFT JOIN survey_scores ON jobs.id=survey_scores.job_id

WHERE (jobs.status = 'Completed' OR jobs.status = 'Released' OR jobs.status = 'GOA' OR jobs.status = 'Canceled') AND (fleet_companies.name = 'USAA')
ORDER by jobs.id
