select
 "Job ID",
 "Date (Central)",
 "Time (Central)",
 "Service Requested",
 "Policy Number",
 "Claim Number",
 "Status",
 "Pickup City",
 "ETA",
 "ATA",
 "ATA"-"ETA" as "ATA-ETA",
 CASE WHEN ("ATA"-"ETA") < 0 THEN 1 ELSE 0 END as "ATA-ETA < 0",
 CASE WHEN ("ATA"-"ETA") <= 10 THEN 1 ELSE 0 END as "ATA-ETA <= 10"
from
(
select
  jobs.id as "Job ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  service_codes.name as "Service Requested",
  jobs.status as "Status",
  pickup.city as "Pickup City",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ETA",
  round(cast((Extract(EPOCH FROM
  (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
  (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ATA",
  '' as "Variance",

    -- Policy Addr
  jobs.policy_number   as "Policy Number",
  policy_addr.street  as "Policy Street",
  policy_addr.city    as "Policy City",
  policy_addr.state   as "Policy State",
  policy_addr.zip     as "Policy Zip",

  pcc_claim.claim_number AS "Claim Number"

from jobs
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join locations policy_addr on policy_addr.id = jobs.policy_location_id
  LEFT JOIN pcc_claims pcc_claim on pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.status in ('Completed','Released') and
  jobs.scheduled_for is null and
  fleet_company_id = (SELECT id FROM companies WHERE name = '21st Century Insurance') and
  jobs.created_at >= ? and
  jobs.created_at < ?
order by jobs.id desc) as t1