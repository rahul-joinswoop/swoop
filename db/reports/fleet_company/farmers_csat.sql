
select
  jobs.id as "Dispatch ID",
  /*jobs.status as "Status",
  jobs.parent_id,
  parent.status "Paretn Status",
  to_char(jobs.created_at  at time zone 'UTC' at time zone 'US/Central' , 'MM/DD/YYYY HH24:MI') as "Date (Central)", */
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','MM/DD/YYYY') as "Date (Central)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'US/Central','HH24:MI') as "Time (Central)",
  jobs.policy_number    as "Policy Number",
  pcc_claim.claim_number as "Claim Number",
  case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
  /*pickup.state as "Pickup State",
  original_pickup.state as "Original Pickup State",
  provider_location.state as "Provider State", */
  CONCAT(customer.first_name,
    CONCAT(' ', customer.last_name)) as "Customer",
  customer.phone        as "Phone",
  jobs.status as "Status",
  case when jobs.scheduled_for is not null then 'Scheduled' else null end as "Scheduled",
  case when jobs.scheduled_for is not null then null else
    round(cast((Extract(EPOCH FROM
      (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric),0)
  end as "ETA",
  case when jobs.scheduled_for is not null then null else
    round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric),0)
  end  as "ATA",
  (case when (reviews.type='Reviews::Typeform' and reviews.branding is true) then 'Branded Link' else
     (case when (reviews.type='Reviews::Typeform') then 'Web Link' else
       (case when (reviews.type='NpsReview') then 'SMS' else null end)
      end)
   end) as "Review Type",
  case when (fleet_dispatchers.email like '%farmersinsurance.com') then 'Farmers' else 'Swoop' end as "Call Taker",
  fleet_dispatchers.email as "Fleet Dispatcher",
  reviews.nps_question1 as "Question 1",
  reviews.nps_question2 as "Question 2",
  reviews.nps_question3 as "Question 3",
  CASE
  WHEN (reviews.type='Reviews::Typeform') THEN reviews.nps_feedback
  WHEN (reviews.type='NpsReview')
    THEN (SELECT string_agg(body,', ')
      FROM (SELECT body FROM reviews
        LEFT JOIN twilio_replies ON reviews.id = twilio_replies.target_id AND twilio_replies.target_type='Review'
        WHERE reviews.job_id=jobs.id
        ORDER BY twilio_replies.id) AS reply_bodies)
  WHEN (reviews.type = 'Reviews::SurveyReview') THEN reviews.nps_feedback
  ELSE NULL
  END AS "Feedback/Comments"
from jobs
    join reviews   /* ONLY include jobs with reviews */
      ON jobs.id = reviews.job_id
    left join drives    ON drives.id = jobs.driver_id
    left join users  customer ON customer.id=drives.user_id
    left join users fleet_dispatchers on jobs.fleet_dispatcher_id=fleet_dispatchers.id
    left join locations as pickup on pickup.id=jobs.service_location_id
    left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
    left join companies provider on jobs.rescue_company_id=provider.id
    left join locations provider_location on provider.location_id=provider_location.id
    left join jobs parent on jobs.parent_id=parent.id
    LEFT JOIN pcc_claims pcc_claim on pcc_claim.id = jobs.pcc_claim_id
where
  jobs.deleted_at is null and
  jobs.created_at >= ? and
  jobs.created_at < ? and
  jobs.fleet_company_id = (SELECT id FROM companies WHERE name = 'Farmers Insurance')
  and jobs.status!='GOA'
  and jobs.status !='Canceled'
order by reviews.created_at desc;
