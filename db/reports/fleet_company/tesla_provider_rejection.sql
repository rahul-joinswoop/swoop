
select
  jobs.id as "Swoop ID",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY') as "Date (Pacific)",
  trim(both ' ' from concat(provider.name, ' ', sites.name)) as "Partner",
  service_codes.name as "Service",
  jobs.status as "Status",
  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  CONCAT(pickup.lat, ',', pickup.lng) as "Lat/Lng Pickup",
  job_reject_reasons.text as "Reject Reason",
  reject_reason_info as "Reject Comment"
from jobs
  left join companies provider on jobs.rescue_company_id=provider.id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join sites on sites.company_id=provider.id and sites.name is not null
  left join estimates on estimates.id=jobs.estimate_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join job_reject_reasons on jobs.reject_reason_id=job_reject_reasons.id
where
  status='Rejected'
  and jobs.fleet_company_id=(SELECT id FROM companies WHERE name = 'Tesla') and
  jobs.created_at >= ? and
  jobs.created_at < ?
  {{rescue_company}}
order by jobs.id DESC;
