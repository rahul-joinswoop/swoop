select
  jobs.id,
  TO_CHAR((
    WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
        SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
        FROM jobs AS inner_jobs
        WHERE inner_jobs.id = jobs.id
      UNION
        SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
        FROM jobs AS inner_jobs, parents
        WHERE inner_jobs.id = parents.parent_id
    )
    SELECT adjusted_created_at
    FROM parents
    ORDER BY id ASC
    LIMIT 1
  ) AT TIME ZONE 'UTC' AT TIME ZONE 'America/Los_Angeles', 'MM/DD/YYYY') AS "Date (Pacific)",
  TO_CHAR((
    WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
        SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
        FROM jobs AS inner_jobs
        WHERE inner_jobs.id = jobs.id
      UNION
        SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
        FROM jobs AS inner_jobs, parents
        WHERE inner_jobs.id = parents.parent_id
    )
    SELECT adjusted_created_at
    FROM parents
    ORDER BY id ASC
    LIMIT 1
  ) AT TIME ZONE 'UTC' AT TIME ZONE 'America/Los_Angeles', 'HH24:MI') AS "Time (Pacific)",
  jobs.status,
  rescue_company.name as "Rescue",
  fleet_company.name as "Fleet",
  CONCAT(customer.first_name, ' ', customer.last_name) as "Customer",
  customer.phone as "Phone",
  pickup_location_type.name as "Location Type",
  pickup.address as "pickup location",
  case when (pickup.city is not null) then pickup.city else (case when original_pickup.city is not null then original_pickup.city else (provider_location.city) end) end as "City",
  case when (pickup.state is not null) then pickup.state else (case when original_pickup.state is not null then original_pickup.state else (provider_location.state) end) end as "State",
  case when (pickup.zip is not null) then pickup.zip else (case when original_pickup.zip is not null then original_pickup.zip else provider_location.zip end) end as "Zip",
  case when (pickup.state is null) and (original_pickup.state is null) then false else true end as "Pickup Location Exact",
  CONCAT(pickup.lat, ',', pickup.lng) as "Pickup",
  CONCAT(dropoff.lat, ',', dropoff.lng) as "Dropoff",
  service_codes.name as "Service",
  customer_types.name as "Payment Type",
  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
  round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",
  round((estimates.meters_ab+(CASE when estimates.drop_location_id is not null THEN estimates.meters_bc+estimates.meters_ca else estimates.meters_ba END))*0.000621371,1) as "P2P Miles",
  round(cast((Extract(EPOCH FROM
    (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ETA",
  round(cast((Extract(EPOCH FROM
    (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
)/60) as numeric),0) as "ATA",
  round(
    cast((Extract(EPOCH FROM
      (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) -
      (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric)
    -
    cast((Extract(EPOCH FROM
      (SELECT (time-created_at) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type in (0,3) order by id asc limit 1)
    )/60) as numeric)
  ,0) as "ATA - ETA",
  case when (jobs.track_technician_sent_at is not null) then 1 else 0 end as "Tracking Link Sent",
   (select array_agg(body) as sms_replies  from reviews left join twilio_replies on reviews.id=twilio_replies.target_id and twilio_replies.target_type='Review' where reviews.job_id=jobs.id group by reviews.job_id),
   invoicing_ledger_items.state,
  CAST(
    (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
     from invoicing_line_items
     where invoicing_ledger_items.id=invoicing_line_items.ledger_item_id
       and invoicing_line_items.deleted_at is null
     group by invoicing_ledger_items.id)
  AS DECIMAL(14, 2)) as "Invoiced",
   jobs.partner_live,
   jobs.partner_open,
   case when (jobs.partner_live and jobs.partner_open) then 1 else 0 end as "Live and Open",
   jobs.platform,
   vehicles.vin,
   jobs.scheduled_for,
   jobs.accident,
   symptoms.name as "Symptom",
  CONCAT_WS(' ',fleet_dispatchers.first_name, fleet_dispatchers.last_name) as "Fleet Dispatcher",
  CONCAT_WS(' ',swoop_dispatchers.first_name, swoop_dispatchers.last_name) as "Swoop Dispatcher",
  CONCAT_WS(' ',partner_dispatchers.first_name, partner_dispatchers.last_name) as "Partner Dispatcher",
  US_time(adjusted_ajs(jobs.id,9), 'America/Los_Angeles') AS "Created",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=10 and job_id=jobs.id order by id desc limit 1) as assigned_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "Assigned (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=11 and job_id=jobs.id order by id desc limit 1) as accepted_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "Accepted (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=2 and job_id=jobs.id order by id desc limit 1) as dispatch_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "Dispatched (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=3 and job_id=jobs.id order by id desc limit 1) as enroute_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "En Route (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=4 and job_id=jobs.id order by id desc limit 1) as onsite_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "On Site (Pacific)",
  to_char((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=7 and job_id=jobs.id order by id desc limit 1) as completed_ajs) at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY HH24:MI') as "Completed (Pacific)",
  to_char(((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=11 and job_id=jobs.id order by id desc limit 1) as accepted_ajs) - jobs.created_at),'HH24:MI:SS') as "Digital Dispatch Time",

to_char(((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from (select last_set_dttm,adjusted_dttm from audit_job_statuses where deleted_at is null and job_status_id=3 and job_id=jobs.id order by id desc limit 1) as accepted_ajs) - jobs.created_at),'HH24:MI:SS') as "Digital En Route",

to_char(((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from audit_job_statuses where deleted_at is null and job_status_id=7 and job_id=jobs.id order by id desc limit 1) -
  (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from audit_job_statuses where deleted_at is null and job_status_id=3 and job_id=jobs.id order by id desc limit 1)),'HH24:MI') as "enroute->completed",

TO_CHAR((
    (SELECT created_at FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id AND time_of_arrivals.eba_type IN (0, 3) ORDER BY id ASC LIMIT 1) -
    (
      WITH RECURSIVE parents(id, parent_id, adjusted_created_at) AS (
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs
          WHERE inner_jobs.id = jobs.id
        UNION
          SELECT inner_jobs.id, inner_jobs.parent_id, inner_jobs.adjusted_created_at
          FROM jobs AS inner_jobs, parents
          WHERE inner_jobs.id = parents.parent_id
      )
      SELECT adjusted_created_at
      FROM parents
      ORDER BY id ASC
      LIMIT 1
    )
  ),'HH24:MI:SS') as "Handle Time",
  /* Track Driver Funnel */
  to_char(track_driver_audit_sms.created_at   at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Text Sent",
  to_char(track_driver_audit_sms.clicked_dttm at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Link Clicked",
  null as "Track Driver Page Viewed",
  null as "Track Driver Page Session",

  reviews.rating as "5 Star Review",
  reviews.nps_question1 as "NPS Informed ",
  reviews.nps_question2 as "NPS ETA",
  reviews.nps_question3 as "NPS Recommend",
  reviews.nps_feedback as "NPS: Comments",
  jobs.policy_number as "Policy Number",
  jobs.ref_number as "Reference Number",
  customers.member_number as "Member Number",
  jobs.notes,
  jobs.fleet_notes,
  job_reject_reasons.text as "Reject Reason",
  jobs.reject_reason_info as "Reject Comment",
  rescue_vehicle.name as "Rescue Vehicle Name",
  case when (auto_assign.status = 'assigned') then true else false end as "Auto Assigned",
  auto_assign.auto_dispatched as "Dedicated Dispatched",
  jobs.deleted_at as "Deleted At",
  TO_CHAR(audit_sms.clicked_dttm AT TIME ZONE 'UTC' AT TIME ZONE 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') AS "Link Clicked"
from jobs
  left join companies as rescue_company on rescue_company.id=jobs.rescue_company_id
  left join companies as fleet_company on fleet_company.id=jobs.fleet_company_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join location_types as pickup_location_type on pickup_location_type.id = pickup.location_type_id
  left join locations as dropoff on dropoff.id=jobs.drop_location_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join estimates on estimates.id=jobs.estimate_id
  left join drives on jobs.driver_id=drives.id
  left join vehicles on drives.vehicle_id=vehicles.id
  left join users as fleet_dispatchers on jobs.fleet_dispatcher_id = fleet_dispatchers.id
  left join users as swoop_dispatchers on jobs.root_dispatcher_id = swoop_dispatchers.id
  left join users as partner_dispatchers on jobs.partner_dispatcher_id = partner_dispatchers.id
  left join symptoms on jobs.symptom_id=symptoms.id
  left join users as customer on customer.id=drives.user_id
  left join customers on customer.customer_id=customers.id
  left join customer_types on customer_types.id=jobs.customer_type_id
  left join locations as original_pickup on original_pickup.id=jobs.original_service_location_id
  left join locations provider_location on rescue_company.location_id=provider_location.id
  left join job_reject_reasons on jobs.reject_reason_id=job_reject_reasons.id
  left join vehicles as rescue_vehicle on jobs.rescue_vehicle_id = rescue_vehicle.id
  /* the following joins have the potential to cause duplicates */
  left join audit_sms as track_driver_audit_sms on track_driver_audit_sms.job_id = jobs.id and track_driver_audit_sms.type='EtaAuditSms'
  left join reviews on jobs.id=reviews.job_id
  left join invoicing_ledger_items on (jobs.id=invoicing_ledger_items.job_id and invoicing_ledger_items.sender_id=jobs.rescue_company_id and invoicing_ledger_items.deleted_at is null and invoicing_ledger_items.type='Invoice')
  left join auto_assign_assignments as auto_assign on jobs.id=auto_assign.job_id
  left join jobs parent ON jobs.parent_id = parent.id
  LEFT JOIN audit_sms ON audit_sms.job_id = jobs.id AND audit_sms.type = 'Sms::BrandedReviewLink'
where
   jobs.status != 'Reassigned' and
   (
    reviews.id = (SELECT MAX(id) FROM reviews WHERE reviews.job_id = jobs.id) or
    reviews.id is NULL
   ) and
   (rescue_company.demo is not true or jobs.rescue_company_id is null) and
   fleet_company.demo is not true
   and jobs.created_at >= ?
   and jobs.created_at < ?
   {{rescue_company}}
   {{fleet_company}}
   {{service}}
order by id desc;
