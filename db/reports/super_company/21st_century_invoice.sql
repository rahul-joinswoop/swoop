SELECT
  '' AS "BatchCreated",
  'InboundPaymentRequestCCAS' AS "TransType",
  'CCAS' AS "ReceivedBy",
  pcc_claim.data->'table'->>'claimUnitId' AS "EntityID",
  '' AS "ReceivedDate",
  pcc_claim.data->'table'->>'claimUnitNumber' AS "ClaimUnitNumber",
  'Security Advantage Towing' AS "PaymentClass",
  CASE
    WHEN symptom.name = 'Accident' THEN 'ACCDT'
    WHEN symptom.name = 'Flat tire' THEN 'FLTSP'
    WHEN symptom.name = 'Out of fuel' THEN 'GAS'
    WHEN symptom.name = 'Locked out' THEN 'LCKSI'
    WHEN (symptom.name = 'Inoperable Problem' OR symptom.name = 'Specialty Vehicle/RV Disablement') THEN 'VINOP'
    WHEN symptom.name = 'Stuck' THEN 'WINCH'
    WHEN symptom.name = 'Would not Start' THEN 'WNTST'
  END AS "ProblemType",
  'N' AS "Accident",
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN NULL
        WHEN pcc_coverage.covered IS TRUE THEN 'Y'
    ELSE 'N'
  END AS "TowCoverageStatus", -- override CASE
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN 'Y'
    ELSE 'N'
  END AS "SupervisorApproval", -- override CASE
  '' AS "CCASBatch Number",
  '57.56' AS "Dispatch Fee",
  '0' AS "Excess Claim Amount",
  '1' AS "TaskNumber",
  LPAD(job.id::text, 10, '0') AS "CaseNumber", -- leading 0
  job.id AS "CCASPONumber",
  TO_CHAR((SELECT
      CASE
          WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
          ELSE last_set_dttm
      END
   FROM (
     SELECT last_set_dttm, adjusted_dttm
     FROM audit_job_statuses
     WHERE deleted_at IS NULL
     AND job_status_id = 9 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY') AS "ServiceStartDate", -- AJS created date
  CASE
    WHEN job.status = 'Completed' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 9 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'GOA' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 21 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'Canceled' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 8 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     WHEN job.status = 'Released' THEN
      TO_CHAR((SELECT
          CASE
              WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm
              ELSE last_set_dttm
          END
       FROM (
         SELECT last_set_dttm, adjusted_dttm
         FROM audit_job_statuses
         WHERE deleted_at IS NULL
         AND job_status_id = 18 AND job_id=job.id ORDER BY id DESC LIMIT 1) AS created_ajs), 'MM/DD/YYYY')
     ELSE ''
     END AS "ServiceEndDate", -- AJS end date
  CASE
    WHEN job.status = 'goa' THEN 'GOA'
    WHEN job.status = 'canceled' THEN 'CAN'
    WHEN service.name = 'Tow' OR service.name = 'Tow if Jump Fails' OR service.name = 'Tow if Tire Change Fails' THEN 'TOW'
    WHEN service.name = 'Battery Jump' OR service.name = 'Tire Change' OR service.name = 'Fuel Delivery' OR service.name = 'Winch Out' THEN 'ROAD'
    WHEN service.name = 'Lock Out' THEN 'LOCK'
    WHEN service.name = 'Accident Tow' THEN 'ACCDT'
  END AS "TypeOfService",
  'Final Payment' AS "PaymentType",
  job.rescue_company_id AS "ServiceProviderID",
  CASE
    WHEN ((SELECT id FROM (
        SELECT MAX(explanations.id) AS id
        FROM explanations
        JOIN reasons ON explanations.reason_id = reasons.id
        JOIN issues ON issues.id = reasons.issue_id
        WHERE explanations.job_id = job.id
        AND issues.name = 'Overage Override'
        AND explanations.deleted_at IS NULL)
      AS issue_subquery) ) IS NOT NULL THEN 'HP'
    ELSE NULL
  END AS "CoverageVerificationSource", -- override CASE
  'N' AS "Resubmitted",
  '21' AS "IGI",
  '' AS "VehicleType"
FROM invoicing_ledger_items invoice
JOIN jobs job ON job.id = invoice.job_id
JOIN companies fleet_company ON job.fleet_company_id = fleet_company.id
LEFT JOIN pcc_claims pcc_claim ON job.pcc_claim_id = pcc_claim.id
LEFT JOIN pcc_coverages pcc_coverage ON pcc_claim.pcc_coverage_id = pcc_coverage.id
LEFT JOIN service_codes service ON service.id = job.service_code_id
LEFT JOIN symptoms symptom ON symptom.id = job.symptom_id
WHERE fleet_company.name = '21st Century Insurance'
AND invoice.deleted_at IS NULL
AND invoice.type IN ('Invoice')
AND job.status IN ('Completed', 'Released', 'Canceled', 'GOA')
AND job.created_at >= ?
AND job.created_at < ?
{{invoice_ids}}
order by job.id desc 