
  SELECT
    invoice.job_id AS "Job Id",
    invoice.id AS "Invoice Id",
    to_char((CASE WHEN (job.scheduled_for is not null) THEN job.scheduled_for ELSE job.created_at END) AT TIME ZONE (COALESCE((SELECT timezone FROM users WHERE id = ?), 'America/Los_Angeles')), 'MM/DD/YY') as "Service Date",
    (to_char(
      (CASE WHEN (job.scheduled_for IS NOT null)
        THEN job.scheduled_for
        ELSE job.created_at END
      ) AT TIME ZONE (COALESCE((SELECT timezone FROM users WHERE id = ?), 'America/Los_Angeles')), 'HH24:MI')) || ' (' || (COALESCE((SELECT timezone FROM users WHERE id = ?), 'America/Los_Angeles')) || ')' AS "Time",
    client_company.name AS "Company",
    partner_company.name AS "Partner",
    service_location.address AS "Pickup Address",
    drop_location.address AS "Drop Off Address",
    job.policy_number AS "Policy Number",
    job.ref_number AS "Reference Number",
    customers.member_number as "Member Number",
    CONCAT_WS(' ', vehicle.year, vehicle.make, vehicle.model) AS "Vehicle Info",
    service_code.name AS "Service",
    job.status AS "Status",
    customer_type.name AS "Payment Type",
    CAST(invoice.total_amount AS DECIMAL(14, 2)) AS "Invoice Amount"
  FROM invoicing_ledger_items invoice
  LEFT JOIN jobs job ON job.id = invoice.job_id
  LEFT JOIN companies client_company ON client_company.id = invoice.recipient_company_id
  LEFT JOIN companies partner_company ON partner_company.id = job.rescue_company_id
  LEFT JOIN locations service_location ON service_location.id = job.service_location_id
  LEFT JOIN locations drop_location ON drop_location.id = job.drop_location_id
  LEFT JOIN drives driver ON driver.id = job.driver_id
  LEFT JOIN users customer ON customer.id=driver.user_id
  LEFT JOIN customers ON customer.customer_id=customers.id
  LEFT JOIN vehicles vehicle ON vehicle.id = driver.vehicle_id
  LEFT JOIN service_codes service_code ON service_code.id = job.service_code_id
  LEFT JOIN customer_types customer_type ON customer_type.id = job.customer_type_id
  WHERE
    invoice.type IN ('Invoice')
    AND invoice.deleted_at IS NULL
    AND job.type = 'FleetManagedJob'
    AND invoice.created_at BETWEEN ? AND ?
    AND invoice.sender_id = ?
    {{invoice_ids}}
    {{client_company_id}}
