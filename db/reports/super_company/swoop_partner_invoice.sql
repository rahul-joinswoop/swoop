
SELECT
  *,
  "Invoice Amount"-"Payments" as "Balance Due"
FROM
(
 SELECT
 concat('SW', job.id) as "Invoice Number",
 to_char(job.created_at  at time zone 'UTC' at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Date/Time",

 service.name as "Service",
 job.status as "Status",

 fleet.name   as "Fleet",
 partner.name as "Partner",

 '' as "Payment Type",

  CAST((select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
     from invoicing_line_items
         where inv.id=invoicing_line_items.ledger_item_id
         and invoicing_line_items.deleted_at is null
       group by inv.id)
     AS DECIMAL(14, 2)) as "Invoice Amount",

 round(estimate.meters_ab*0.000621371,1) as "EN Route Miles",
 round(estimate.meters_bc*0.000621371,1) as "Towed Miles",
 round((CASE when estimate.drop_location_id is not null THEN estimate.meters_ca else estimate.meters_ba END)*0.000621371,1) as "Return Miles",

 inv.id as "Invoice Id",

 CAST (
 (SELECT -1*COALESCE(SUM(total_amount), 0) AS "Payments"
  FROM invoicing_ledger_items AS "payments_or_credits"
  WHERE payments_or_credits.invoice_id = inv.id
  AND payments_or_credits.deleted_at IS NULL
  AND payments_or_credits.type IN ('Payment', 'Discount', 'Refund', 'WriteOff'))
 AS DECIMAL(14, 2))

 FROM invoicing_ledger_items inv

 LEFT JOIN jobs    job           ON job.id        = inv.job_id
 LEFT JOIN drives  cdrive        ON job.driver_id = cdrive.id
 LEFT JOIN users   customer      ON customer.id   = cdrive.user_id
 LEFT JOIN companies  partner    ON inv.sender_id = partner.id
 LEFT JOIN companies  fleet      ON job.fleet_company_id = fleet.id
 LEFT JOIN vehicles vehicle      ON vehicle.id    = cdrive.vehicle_id
 LEFT JOIN locations policy_addr ON job.service_location_id = policy_addr.id
 LEFT JOIN service_codes service ON job.service_code_id     = service.id
 LEFT JOIN estimates   estimate  ON job.estimate_id = estimate.id

 LEFT JOIN rescue_providers AS provider
  ON    provider.site_id = job.site_id
    AND provider.company_id  = (Select id from companies where name='Swoop')
    AND provider.deleted_at IS NULL

 LEFT JOIN companies  company ON company.id = job.rescue_company_id

 WHERE inv.type IN ('Invoice')
   AND job.type = 'FleetManagedJob'
   AND inv.created_at BETWEEN ? AND ?
   AND inv.recipient_company_id =  ?
   AND inv.state = ?
   {{provider_id}}
   {{sender_id}}
) as t1
