select
  to_char(reviews.created_at at time zone 'UTC' at time zone 'America/Los_Angeles', 'YYYY/MM/DD HH24:MI:SS') as "Date PDT",
  creating_company.name as "Fleet Company",
  rescue_company.name as "Rescue Company",
  job_id as "JobID",
  reviews.type,
  reviews.rating,
  reviews.nps_question1 as "NPS Recommend",
  reviews.nps_question2 as "NPS Informed",
  reviews.nps_question3 as "NPS ETA",
  reviews.nps_feedback,
  (select array_agg(body) as sms_replies  from reviews left join twilio_replies on reviews.id=twilio_replies.target_id and twilio_replies.target_type='Review' where reviews.job_id=jobs.id group by reviews.job_id),
  reviews.id as "ReviewID"
from jobs
    inner join reviews on jobs.id=reviews.job_id
    left join companies as creating_company on jobs.fleet_company_id=creating_company.id
    left join companies as rescue_company on jobs.rescue_company_id=rescue_company.id
where
  not (reviews.nps_question1 is null and reviews.nps_question2 is null and reviews.nps_question3 is null) and
  jobs.created_at >= ? and
  jobs.created_at < ?
  {{rescue_company}}
  {{fleet_company}}
order by reviews.created_at desc;