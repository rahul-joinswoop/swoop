select
  jobs.id,
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','MM/DD/YYYY') as "Date (Pacific)",
  to_char(jobs.created_at at time zone 'UTC' AT TIME ZONE 'America/Los_Angeles','HH24:MI') as "Time (Pacific)",
  rescue_company.name as "Rescue",
  fleet_company.name as "Fleet",
  pickup_location_type.name as "Location Type",
  pickup.address as "pickup location",
  CONCAT(pickup.lat, ',', pickup.lng) as "Pickup",
  CONCAT(dropoff.lat, ',', dropoff.lng) as "Dropoff",
  service_codes.name as "Service",
  round(estimates.meters_ab*0.000621371,1) as "EN Route Miles",
  round(estimates.meters_bc*0.000621371,1) as "Pick Up To Drop Off",
  round((CASE when estimates.drop_location_id is not null THEN estimates.meters_ca else estimates.meters_ba END)*0.000621371,1) as "Return Miles",
    round((estimates.meters_ab+(CASE when estimates.drop_location_id is not null THEN estimates.meters_bc+estimates.meters_ca else estimates.meters_ba END))*0.000621371,1) as "P2P Miles",
  round(cast((Extract(EPOCH FROM
  (select last_set_dttm from audit_job_statuses where job_status_id=7 and audit_job_statuses.job_id=jobs.id order by id desc limit 1) -
  (select last_set_dttm from audit_job_statuses where job_status_id=9 and audit_job_statuses.job_id=jobs.id order by id desc  limit 1)
)/60) as numeric),0) as "Created To Completed",
   round(cast((Extract(EPOCH FROM
  time_of_arrivals.time - (select last_set_dttm from audit_job_statuses where job_status_id=10 and audit_job_statuses.job_id=jobs.id order by id desc limit 1)
)/60) as numeric),0) as "ETA",
   round(cast((Extract(EPOCH FROM
  (select last_set_dttm from audit_job_statuses where job_status_id=4 and audit_job_statuses.job_id=jobs.id order by id desc limit 1) -
  (select last_set_dttm from audit_job_statuses where job_status_id=10 and audit_job_statuses.job_id=jobs.id order by id desc limit 1)
)/60) as numeric),0) as "ATA",
   jobs.status,

   to_char(audit_job_statuses.created_at at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI'),

   reviews.rating,
   (select array_agg(body) as sms_replies  from reviews left join twilio_replies on reviews.id=twilio_replies.target_id and twilio_replies.target_type='Review' where reviews.job_id=jobs.id group by reviews.job_id),
   invoicing_ledger_items.state,
   CAST(
           (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
      from invoicing_line_items
      where invoicing_ledger_items.id=invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
   group by invoicing_ledger_items.id)

          AS DECIMAL(14, 2)) as "Invoiced",

   fleet_invoice.state,
   CAST(
           (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
      from invoicing_line_items
      where fleet_invoice.id=invoicing_line_items.ledger_item_id
        and invoicing_line_items.deleted_at is null
   group by fleet_invoice.id)
          AS DECIMAL(14, 2)) as "Fleet Invoiced",
   partner_live,
   partner_open,
   vehicles.vin,
   jobs.scheduled_for,
   symptoms.name as "Symptom",
  CONCAT_WS(' ',fleet_dispatchers.first_name, fleet_dispatchers.last_name) as "Fleet Dispatcher",
  CONCAT_WS(' ',partner_dispatchers.first_name, partner_dispatchers.last_name) as "Partner Dispatcher",
  to_char(((select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) -
  (select case when adjusted_dttm is not null then adjusted_dttm else last_set_dttm end from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc limit 1)),'HH24:MI') as "enroute->completed",
  notes,
  fleet_notes
from jobs

  left join audit_job_statuses
    ON audit_job_statuses.job_id = jobs.id
      AND audit_job_statuses.job_status_id IN (SELECT id FROM job_statuses WHERE name IN ('Completed', 'GOA', 'Released', 'Canceled')) /* PDW: this looks wrong */


  left join companies as rescue_company on rescue_company.id=jobs.rescue_company_id
  left join companies as fleet_company on fleet_company.id=jobs.fleet_company_id
  left join locations as pickup on pickup.id=jobs.service_location_id
  left join location_types as pickup_location_type on pickup_location_type.id = pickup.location_type_id
  left join locations as dropoff on dropoff.id=jobs.drop_location_id
  left join service_codes on service_codes.id=jobs.service_code_id
  left join estimates on estimates.id=jobs.estimate_id
  left join time_of_arrivals on time_of_arrivals.job_id=jobs.id
  left join reviews on jobs.id = reviews.job_id

  left join invoicing_ledger_items
    ON jobs.id=invoicing_ledger_items.job_id
    AND invoicing_ledger_items.recipient_id =(SELECT id FROM companies WHERE name = 'Swoop' LIMIT 1)
    AND invoicing_ledger_items.type='Invoice'

  left join invoicing_ledger_items fleet_invoice
    ON jobs.id=fleet_invoice.job_id
       AND fleet_invoice.deleted_at IS NULL
       AND fleet_invoice.sender_id =(SELECT id FROM companies WHERE name = 'Swoop' LIMIT 1)
       AND fleet_invoice.type='Invoice'

  left join drives on jobs.driver_id=drives.id
  left join vehicles on drives.vehicle_id=vehicles.id
  left join users as fleet_dispatchers on jobs.fleet_dispatcher_id = fleet_dispatchers.id
  left join users as partner_dispatchers on jobs.partner_dispatcher_id = partner_dispatchers.id
  left join symptoms on jobs.symptom_id = symptoms.id
  where
   jobs.status in ('Completed', 'Released', 'Canceled', 'GOA') and
   jobs.type = 'FleetManagedJob' and
   (
         time_of_arrivals.id= (SELECT MAX(id) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type = 0) or
         (
           (SELECT MAX(id) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id and time_of_arrivals.eba_type = 0) is NULL and
           time_of_arrivals.id= (SELECT MAX(id) FROM time_of_arrivals WHERE time_of_arrivals.job_id = jobs.id)
         ) or
         time_of_arrivals.id is NULL
   ) and
   (
     reviews.id = (SELECT MAX(id) FROM reviews WHERE reviews.job_id = jobs.id) or
     reviews.id is NULL
   ) and
   invoicing_ledger_items.deleted_at is null and
   fleet_company_id not in (1,6,41,45,46,47,48,49,472,473,474) and
   rescue_company_id not in (1,6,41,45,46,47,48,49,472,473,474) and
   rescue_company.demo is not true and
   fleet_company.demo is not true and
   jobs.created_at >= ? and
   jobs.created_at < ?
   {{rescue_company}}
   {{fleet_company}}
   {{service}}
order by id desc;