
          select jobs.id as "ID",
                 to_char(jobs.created_at  at time zone 'UTC' at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Created time (LA)",
                 fleet_company.name as "Fleet Company",
                 service.name as "Service",
                 (select array_agg(questions.question || ': ' || answers.answer) from question_results
                    left join questions on questions.id=question_results.question_id
                    left join answers on answers.id=question_results.answer_id
                    where question_results.job_id = jobs.id
                    group by job_id) as "All Question Answers",
                 symptoms.name as "Symptom",
                 jobs.policy_number as "Policy Number",
                 (CASE WHEN policy_location.street IS NOT NULL THEN CONCAT(policy_location.street, ', ',policy_location.city, ', ', policy_location.state, ', ', policy_location.zip) ELSE null END) as "Policy Address",
                 customer_type.name as "Payment Type"
          from jobs
            left join symptoms on jobs.symptom_id=symptoms.id
            left join companies  as fleet_company on fleet_company.id=jobs.fleet_company_id
            left join service_codes as service on jobs.service_code_id=service.id
            left join locations as policy_location on policy_location.id=jobs.policy_location_id
            left join customer_types as customer_type on jobs.customer_type_id=customer_type.id
          where
            jobs.type='FleetManagedJob' and
            jobs.created_at >= ? and
            jobs.created_at < ?
           order by jobs.created_at desc