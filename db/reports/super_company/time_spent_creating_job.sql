
   select to_char(metrics.created_at  at time zone 'UTC' at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Created At",
       companies.type,
       companies.name,
       metrics.value
from metrics
left join companies on companies.id=metrics.company_id
where
  action='complete'
  and label='job_form'
  and metrics.created_at >= ?
  and metrics.created_at < ?
  {{company_id}};
