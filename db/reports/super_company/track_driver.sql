SELECT
  jobs.id as "Job ID",

  to_char(created_audit.created_at at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Job Created Date/Time",

  CASE
    WHEN rescue_company.id = fleet_company.id THEN ''
    ELSE fleet_company.name
  END as "Fleet Company",
  rescue_company.name as "Partner Company",

  to_char(audit_sms.created_at   at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Text Sent",
  to_char(audit_sms.clicked_dttm at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Link Clicked",
  to_char(metrics.created_at     at time zone 'UTC' at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Track Driver Page Viewed",

  -- We dont have a way to track this at the moment so leave blank
  null as "Track Driver Page Session"
from jobs
  LEFT JOIN audit_sms
  ON audit_sms.job_id = jobs.id and audit_sms.type='EtaAuditSms'

LEFT JOIN metrics
  ON metrics.target_type = 'Job'
     AND metrics.target_id = audit_sms.job_id
     AND metrics.action    = 'viewed'
     AND metrics.label     = 'track_driver_page_view' /* PDW: this doesn't exist */

JOIN job_statuses created
 ON created.name = 'Created'

LEFT JOIN audit_job_statuses created_audit
 ON created.id = created_audit.job_status_id
    AND created_audit.job_id = audit_sms.job_id

JOIN companies as rescue_company ON rescue_company.id = jobs.rescue_company_id
JOIN companies as fleet_company  ON fleet_company.id   = jobs.fleet_company_id

where
  audit_sms.created_at >= ?
  and audit_sms.created_at < ?
  {{company_id}};
