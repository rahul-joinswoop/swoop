
   SELECT jobs.id as "ID",

  to_char(jobs.created_at  at time zone 'UTC' at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Created At",

  fleet_company.name as "Creating Company",

  to_char(sms.created_at   at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "SMS Sent At",
  to_char(sms.clicked_dttm  at time zone 'America/Los_Angeles' , 'MM/DD/YYYY HH24:MI') as "Link Clicked",


  '' as "Rejected Location",
  '' as "10 Second Limit",

  to_char(command.created_at  at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Location Updated At",
  to_char(metrics.created_at  at time zone 'America/Los_Angeles', 'MM/DD/YYYY HH24:MI') as "Page Viewed At"


  FROM jobs
  JOIN companies fleet_company
    ON fleet_company.id = jobs.fleet_company_id

  JOIN event_operations eo
     ON eo.name = 'update'

  JOIN audit_sms sms
    ON sms.job_id = jobs.id AND sms.type='LocationAuditSms'

  LEFT JOIN drives
    ON jobs.driver_id = drives.id

  LEFT JOIN users customer
     ON drives.user_id = customer.id

 LEFT JOIN commands command
    ON command.event_operation_id = eo.id AND command.user_id = customer.id

  LEFT JOIN metrics
    ON metrics.target_id = jobs.id
        AND metrics.target_type = 'Job'
        AND metrics.action      = 'viewed'
  {{company}}
