Select
        jobs.id as "Swoop ID",
        jobs.status as "Status",
        to_char(jobs.adjusted_created_at at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'MM/DD/YYYY') as "Created Date",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles'), 'MM/DD/YYYY') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) as "Dispatched Date",
        CONCAT(driver.first_name, ' ', driver.last_name) as "Driver",
        truck.name as "Truck",
        accounts.name as "Account Name",
        to_char(jobs.adjusted_created_at at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') as "Created",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1) as "Dispatched",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1) as "En Route",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc  limit 1) as "On Site",
        (select to_char(CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') , 'HH24:MI') from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) as "Completed",


          to_char(
            ((select CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END from audit_job_statuses where job_status_id=7 and job_id=jobs.id order by id desc limit 1) -
            (select CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1)),
          'DD:HH24:MI') as "Total Time (min)",

          -- (Towdest - towing) + (On - En Rout)
          to_char(
                 (coalesce(((select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=6 and job_id=jobs.id order by id desc limit 1) -
                 (select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=5 and job_id=jobs.id order by id desc  limit 1)),interval '0')
               +
                 ((select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=4 and job_id=jobs.id order by id desc limit 1) -
                 (select CASE WHEN adjusted_dttm IS NOT NULL THEN date_trunc('minute',adjusted_dttm) ELSE date_trunc('minute',last_set_dttm) END from audit_job_statuses where job_status_id=3 and job_id=jobs.id order by id desc  limit 1))),
          'DD:HH24:MI') as "Drive Time (min)",



         CAST(
           (select sum(invoicing_line_items.net_amount) + sum(invoicing_line_items.tax_amount) as sum_amount
              from invoicing_line_items
              where invoices.id=invoicing_line_items.ledger_item_id
               and invoicing_line_items.deleted_at is null
               and invoicing_line_items.description != 'Storage'
           group by invoices.id) AS DECIMAL(14, 2)
          ) as "Amount"


        from jobs
        left join users as customer on jobs.user_id=customer.id
        left join users as driver on jobs.rescue_driver_id=driver.id
        left join vehicles as truck on jobs.rescue_vehicle_id=truck.id
        left join invoicing_ledger_items as invoices on invoices.job_id=jobs.id and invoices.deleted_at is NULL and invoices.sender_id=(select id from companies where name='EHI Truck') and invoices.type='Invoice'
        left join accounts on accounts.id=jobs.account_id
        left join users on users.id=?

        Where
          jobs.fleet_company_id=(select id from companies where name='Enterprise')
          AND jobs.rescue_company_id=(select id from companies where name='EHI Truck')
          AND jobs.deleted_at is null
          AND coalesce((select (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1),jobs.adjusted_created_at) >= ?
          AND coalesce((select (CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END) from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1),jobs.adjusted_created_at) < ?
          AND jobs.status != 'Reassigned'
    ORDER BY coalesce((SELECT CASE WHEN adjusted_dttm IS NOT NULL THEN adjusted_dttm ELSE last_set_dttm END  at time zone 'UTC' at time zone COALESCE(users.timezone, 'America/Los_Angeles') from audit_job_statuses where job_status_id=2 and job_id=jobs.id order by id desc  limit 1),jobs.adjusted_created_at)
