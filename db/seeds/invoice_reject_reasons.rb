# TODO - if we depend on this everywhere we really should have it be in seeds.rb
# instead of here? afaict we're not explicitly using this anywhere

# Add data for InvoiceRejectReason model
def find_or_create_reasons(company_name)
  company = Company.select(:id).find_by(name: company_name)

  Proc.new { |reason|
    InvoiceRejectReason.where(company: company, text: reason).first_or_create!
  }
end

# Add Swoop specific reasons
swoop_reasons = [
  "Incorrect base rate",
  "Incorrect loaded mileage rate",
  "Incorrect en route mileage rate",
  "Incorrect special equipment/labor rate",
  "Explanation needed for additional charges",
  "Already paid"
]
swoop_reasons.map(&find_or_create_reasons("Swoop"))

# Add Tesla reasons
tesla_reasons = [
  "Rates outside of rate agreement",
  "More explanation needed for charges",
  "GOA rate outside of agreement",
  "Event was Customer Pay",
  "Canceled job",
]
tesla_reasons.map(&find_or_create_reasons("Tesla"))
