# encoding: UTF-8
# This file is auto-generated from the current content of the database. Instead
# of editing this file, please use the migrations feature of Seed Migration to
# incrementally modify your database, and then regenerate this seed file.
#
# If you need to create the database on another system, you should be using
# db:seed, not running all the migrations from scratch. The latter is a flawed
# and unsustainable approach (the more migrations you'll amass, the slower
# it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Base.transaction do
  Group.import([

    {"deleted_at"=>nil, "id"=>1, "name"=>"Swoop Only Client"},

    {"deleted_at"=>nil, "id"=>2, "name"=>"Swoop Primary Client"},

    {"deleted_at"=>nil, "id"=>3, "name"=>"Agero Only Client"},

    {"deleted_at"=>nil, "id"=>4, "name"=>"Agero Primary Client"},

    {"deleted_at"=>nil, "id"=>5, "name"=>"Swoop Primary Partner"},

    {"deleted_at"=>nil, "id"=>6, "name"=>"Agero Primary Partner"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('groups')
  ServiceCode.import([

    {"addon"=>false, "deleted_at"=>nil, "id"=>165, "is_standard"=>true, "name"=>"Accident Tow", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>166, "is_standard"=>true, "name"=>"Battery Jump", "order"=>1, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>167, "is_standard"=>false, "name"=>"Decking", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>168, "is_standard"=>true, "name"=>"Dolly", "order"=>nil, "support_storage"=>nil, "variable"=>false},

    {"addon"=>true, "deleted_at"=>nil, "id"=>169, "is_standard"=>false, "name"=>"Driveline", "order"=>nil, "support_storage"=>nil, "variable"=>true},

    {"addon"=>false, "deleted_at"=>nil, "id"=>170, "is_standard"=>true, "name"=>"Tire Change", "order"=>4, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>171, "is_standard"=>true, "name"=>"Fuel", "order"=>nil, "support_storage"=>nil, "variable"=>true},

    {"addon"=>false, "deleted_at"=>"2017-05-05T16:28:31.769Z", "id"=>172, "is_standard"=>true, "name"=>"Legacy Fuel Delivery", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>173, "is_standard"=>false, "name"=>"Gate Fee", "order"=>nil, "support_storage"=>nil, "variable"=>true},

    {"addon"=>false, "deleted_at"=>nil, "id"=>174, "is_standard"=>false, "name"=>"GOA", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>175, "is_standard"=>true, "name"=>"GoJak", "order"=>nil, "support_storage"=>nil, "variable"=>false},

    {"addon"=>false, "deleted_at"=>nil, "id"=>176, "is_standard"=>false, "name"=>"Heavy Haul", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>177, "is_standard"=>false, "name"=>"Illegally Parked", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>178, "is_standard"=>true, "name"=>"Impound", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>179, "is_standard"=>true, "name"=>"Labor", "order"=>nil, "support_storage"=>nil, "variable"=>true},

    {"addon"=>true, "deleted_at"=>nil, "id"=>180, "is_standard"=>false, "name"=>"Lien Fee", "order"=>nil, "support_storage"=>nil, "variable"=>true},

    {"addon"=>false, "deleted_at"=>nil, "id"=>181, "is_standard"=>false, "name"=>"Loaner Wheel", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>182, "is_standard"=>false, "name"=>"Loaner Wheel Pickup", "order"=>nil, "support_storage"=>nil, "variable"=>true},

    {"addon"=>false, "deleted_at"=>nil, "id"=>183, "is_standard"=>true, "name"=>"Lock Out", "order"=>3, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>184, "is_standard"=>true, "name"=>"Other", "order"=>6, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>185, "is_standard"=>true, "name"=>"Fuel Delivery", "order"=>5, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>186, "is_standard"=>false, "name"=>"Overnight Storage", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>187, "is_standard"=>false, "name"=>"Priority Response", "order"=>nil, "support_storage"=>nil, "variable"=>false},

    {"addon"=>false, "deleted_at"=>nil, "id"=>188, "is_standard"=>true, "name"=>"Recovery", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>189, "is_standard"=>true, "name"=>"Reimbursement", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>190, "is_standard"=>true, "name"=>"Repo", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>191, "is_standard"=>false, "name"=>"Rollover", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>192, "is_standard"=>true, "name"=>"Secondary Tow", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>193, "is_standard"=>false, "name"=>"Service Call", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>194, "is_standard"=>true, "name"=>"Storage", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>195, "is_standard"=>false, "name"=>"Swap Fee", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>"2017-05-05T16:28:31.771Z", "id"=>196, "is_standard"=>false, "name"=>"Legacy Tire Change", "order"=>nil, "support_storage"=>nil, "variable"=>false},

    {"addon"=>true, "deleted_at"=>nil, "id"=>197, "is_standard"=>true, "name"=>"Tire Skates", "order"=>nil, "support_storage"=>nil, "variable"=>false},

    {"addon"=>true, "deleted_at"=>nil, "id"=>198, "is_standard"=>true, "name"=>"Toll", "order"=>nil, "support_storage"=>nil, "variable"=>true},

    {"addon"=>false, "deleted_at"=>nil, "id"=>199, "is_standard"=>true, "name"=>"Tow", "order"=>0, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>200, "is_standard"=>true, "name"=>"Tow if Jump Fails", "order"=>2, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>201, "is_standard"=>true, "name"=>"Transport", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>202, "is_standard"=>false, "name"=>"Undecking", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>203, "is_standard"=>false, "name"=>"Up-Righting", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>204, "is_standard"=>true, "name"=>"Wait Time", "order"=>nil, "support_storage"=>nil, "variable"=>true},

    {"addon"=>false, "deleted_at"=>nil, "id"=>205, "is_standard"=>true, "name"=>"Winch Out", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>206, "is_standard"=>false, "name"=>"teemp", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>207, "is_standard"=>false, "name"=>"4x4 Recovery", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>208, "is_standard"=>false, "name"=>"6x6 Recovery", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>209, "is_standard"=>false, "name"=>"Load Shift", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>210, "is_standard"=>false, "name"=>"Sublet Services", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>211, "is_standard"=>false, "name"=>"Law Enforcement", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>212, "is_standard"=>false, "name"=>"Special Straps", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>213, "is_standard"=>false, "name"=>"Air Cushions", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>214, "is_standard"=>false, "name"=>"Spreader Bars", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>215, "is_standard"=>false, "name"=>"Skilled Labor", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>216, "is_standard"=>false, "name"=>"Supervisor", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>217, "is_standard"=>false, "name"=>"Standby Time", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>218, "is_standard"=>false, "name"=>"Haz-mat Fee", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>219, "is_standard"=>false, "name"=>"Permits", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>220, "is_standard"=>false, "name"=>"Driveline Removal", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>221, "is_standard"=>false, "name"=>"Driveline Installation", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>222, "is_standard"=>false, "name"=>"Pilot Car", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>223, "is_standard"=>false, "name"=>"Skip Loader", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>224, "is_standard"=>false, "name"=>"Reach Forklift", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>225, "is_standard"=>false, "name"=>"Extra Time", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>226, "is_standard"=>false, "name"=>"Motorcycle Tow", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>227, "is_standard"=>false, "name"=>"Accident Clean Up", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>228, "is_standard"=>false, "name"=>"Release Fees", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>229, "is_standard"=>false, "name"=>"After Hours Fee", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>230, "is_standard"=>false, "name"=>"CC Processing Fees", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>231, "is_standard"=>false, "name"=>"Ramps", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>232, "is_standard"=>false, "name"=>"Second Truck", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>233, "is_standard"=>false, "name"=>"Vehicle Release Fees", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>234, "is_standard"=>false, "name"=>"RV Tow", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>235, "is_standard"=>false, "name"=>"Light Service GOA", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>236, "is_standard"=>false, "name"=>"Luxury Tow", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>237, "is_standard"=>true, "name"=>"Exotic Tow", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>238, "is_standard"=>false, "name"=>"Roll Off", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>239, "is_standard"=>false, "name"=>"Fuel Surcharge", "order"=>nil, "support_storage"=>nil, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>265, "is_standard"=>false, "name"=>"Swoop Add Item", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>282, "is_standard"=>false, "name"=>"Double Tow (Swap Out)", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>283, "is_standard"=>true, "name"=>"Tow if Tire Change Fails", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>284, "is_standard"=>false, "name"=>"Late Cancel Fee", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>285, "is_standard"=>false, "name"=>"Gone On Arrival Fee", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>true, "deleted_at"=>nil, "id"=>286, "is_standard"=>false, "name"=>"Custom Item", "order"=>nil, "support_storage"=>true, "variable"=>nil},

    {"addon"=>false, "deleted_at"=>nil, "id"=>287, "is_standard"=>false, "name"=>"Reunite", "order"=>nil, "support_storage"=>true, "variable"=>nil},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('service_codes')
  EventOperation.import([

    {"id"=>25, "name"=>"create"},

    {"id"=>26, "name"=>"read"},

    {"id"=>27, "name"=>"read all"},

    {"id"=>28, "name"=>"update"},

    {"id"=>29, "name"=>"delete"},

    {"id"=>30, "name"=>"location update"},

    {"id"=>31, "name"=>"mutation"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('event_operations')
  Feature.import([

    {"deleted_at"=>"2017-05-05T16:28:31.678Z", "description"=>nil, "id"=>217, "name"=>"NPS Review", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>218, "name"=>"Review 4 Texts", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>219, "name"=>"Review Choose", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>220, "name"=>"Review NPS Survey", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>221, "name"=>"Review Pure NPS", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>222, "name"=>"Job PO Number", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>223, "name"=>"Job Unit Number", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>224, "name"=>"Job Reference Number", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>225, "name"=>"Customer Pays", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>226, "name"=>"Odometer", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>227, "name"=>"VIP", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>228, "name"=>"Uber", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>229, "name"=>"Alternate Transportation", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>230, "name"=>"Invoicing", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>231, "name"=>"Settings", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>232, "name"=>"Location Contacts", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>233, "name"=>"Auto Assign", "supported_by"=>nil},

    {"deleted_at"=>"2017-07-11T21:14:53.474Z", "description"=>"Enables Fleet to Dispatch To Truck", "id"=>234, "name"=>"Fleet Dispatch to Truck", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Enables Partner to Dispatch To Truck", "id"=>235, "name"=>"Partner Dispatch to Truck", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>236, "name"=>"Payment Type", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>237, "name"=>"Allow Invoice Add Item", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>238, "name"=>"Always Show Caller", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>239, "name"=>"Show Drivers Invoice", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>240, "name"=>"Show Drivers Edit Invoice", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Allows Partners to Store Vehicles", "id"=>241, "name"=>"Storage", "supported_by"=>nil},

    {"deleted_at"=>"2018-07-25T19:58:59.276Z", "description"=>nil, "id"=>242, "name"=>"Whitelabel Survey", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>243, "name"=>"SMS_Request Location", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Company has option to send user a review sms", "id"=>244, "name"=>"SMS_Reviews", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Company has option to send user a tracking sms", "id"=>245, "name"=>"SMS_Track Driver", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>246, "name"=>"SMS_ETA Updates", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>247, "name"=>"Departments", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>248, "name"=>"Client Sites", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>249, "name"=>"Auto Assign Driver", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>250, "name"=>"Issues", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>251, "name"=>"Hide 4WD", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>252, "name"=>"Invoice Warning", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>253, "name"=>"Show Symptoms", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>254, "name"=>"Hide Reporting", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"PO column in the jobs list", "id"=>256, "name"=>"PO On Dashboard", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>257, "name"=>"Canadian AP", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>258, "name"=>"Hide Partner ETA", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Allows company to edit priority response on a job", "id"=>259, "name"=>"Priority Response", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Determines if a company sees required questions on their new job form", "id"=>260, "name"=>"Questions", "supported_by"=>nil},

    {"deleted_at"=>"2017-10-30T03:11:42.200Z", "description"=>nil, "id"=>261, "name"=>"SMS_Default Request Location On", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>262, "name"=>"Hide Location Contacts", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Shows heavy duty vehicle info in drop downs", "id"=>263, "name"=>"Heavy Duty Equipment", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Shows Serial Number on form", "id"=>264, "name"=>"Serial Number", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Shows Trailers on form", "id"=>265, "name"=>"Trailers", "supported_by"=>nil},

    {"deleted_at"=>"2017-05-05T16:28:31.680Z", "description"=>"Enables the new job form for Tesla (only applicable to Tesla)", "id"=>266, "name"=>"Tesla New Job", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Shows plane animation on sending invoice", "id"=>267, "name"=>"Plane Animation", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>268, "name"=>"VIN Field First", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>269, "name"=>"Autofill VIN", "supported_by"=>nil},

    {"deleted_at"=>"2017-05-05T16:28:31.681Z", "description"=>nil, "id"=>270, "name"=>"ISSC Network Status", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Should swoop root receive all Rescue Vehicle updates via Websocket?", "id"=>271, "name"=>"Swoop Root All Vehicle Updates", "supported_by"=>nil},

    {"deleted_at"=>"2017-07-11T21:14:53.704Z", "description"=>"Calculate truck with quickest ETA and assign job to owning company. This needs to be on for both fleet and partner to enable it", "id"=>272, "name"=>"Swoop Auto Dispatcher", "supported_by"=>["FleetCompany", "RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Enables a Fleet Managed company to add Partner (the partner assigned to the job) as a column on the Active tab or Done tab.", "id"=>273, "name"=>"Add Partner Column to Fleet Managed", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Before job expires auto-reject it instead of auto-callback", "id"=>274, "name"=>"MC Job AutoReject Override", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Applicable to partners: it add the driver assigned to the job to the invoice", "id"=>275, "name"=>"Add Driver Name on Invoice", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Disabled forced photos on mobile", "id"=>276, "name"=>"Disable Force Photos", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"When set won't require customer name and phone number", "id"=>277, "name"=>"Don't require customer name and phone", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"disables editing mobile jobs for all users on the react native app", "id"=>278, "name"=>"Disable App Edit Jobs", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"disables editing mobile jobs for users with just the driver permission on the react native app", "id"=>279, "name"=>"Disable App Drivers Edit Jobs", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"disables creating mobile jobs for all users on the react native app", "id"=>280, "name"=>"Disable App Create Jobs", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"disables creating mobile jobs for users with just the driver permission on the react native app", "id"=>281, "name"=>"Disable App Drivers Create Jobs", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Enable Looker Reporting on the Insights tab", "id"=>282, "name"=>"Looker", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"disables viewing mobile invoices for drivers on the react native app", "id"=>289, "name"=>"Disable driver view invoice", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"disables editing mobile invoices for drivers on the react native app", "id"=>290, "name"=>"Disable driver edit invoice", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"The whole “link bank account” section is hidden from Partners on settings", "id"=>291, "name"=>"Disable Bank Settings", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Disables mobile vin lookup to autofill vehicle information", "id"=>292, "name"=>"Disable Mobile VIN Lookup", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Allow quickbooks sync and configure options", "id"=>293, "name"=>"Quickbooks", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Shows map to fleets", "id"=>294, "name"=>"Fleet Service Map", "supported_by"=>["FleetCompany"]},

    {"deleted_at"=>nil, "description"=>"Send an SMS follow up if the job exceeds the last ETA (of any type) to the customer asking if provider arrived", "id"=>295, "name"=>"SMS Confirm On Site", "supported_by"=>["FleetCompany"]},

    {"deleted_at"=>"2018-10-29T16:39:22.532Z", "description"=>"Show map in the choose partner modal", "id"=>296, "name"=>"Choose Partner Map", "supported_by"=>["FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"Send an SMS 30 mins after the job goes to onsite to the customer to confirm job completion", "id"=>298, "name"=>"SMS Confirm Complete", "supported_by"=>["FleetCompany"]},

    {"deleted_at"=>nil, "description"=>"Allows partners to remove timestamps on their invoices", "id"=>299, "name"=>"Remove invoice timestamps per accounta", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Allows company to add custom items to invoice", "id"=>300, "name"=>"Invoice Custom Itemsa", "supported_by"=>["RescueCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"Allows partners to remove timestamps on their invoices", "id"=>303, "name"=>"Remove invoice timestamps per account", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Allows company to add custom items to invoice", "id"=>305, "name"=>"Invoice Custom Items", "supported_by"=>["RescueCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"Shows eta on mobile job details", "id"=>306, "name"=>"Show ETA on mobile job details", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Require class type on the job form", "id"=>309, "name"=>"Class Type Required", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Adds a department filter to the dashboard", "id"=>310, "name"=>"Department Filter", "supported_by"=>["FleetCompany"]},

    {"deleted_at"=>nil, "description"=>"Adds review tab", "id"=>311, "name"=>"Review Feed", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"Shows filter to filter reviews feed by NPS score", "id"=>312, "name"=>"Reviews Feed Q1 Filter", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"Adds a confirmation modal to the create job modal", "id"=>313, "name"=>"Confirm Job Details", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"Enable Class Type to Fleets on JobForm and settings/configure", "id"=>314, "name"=>"Fleet Class Type", "supported_by"=>["FleetCompany"]},

    {"deleted_at"=>nil, "description"=>"No longer requires company to select a specific service on completing a job", "id"=>315, "name"=>"Disable force service resolution", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>"2018-11-13T23:52:11.164Z", "description"=>"Allow users to charge payments on customers card", "id"=>316, "name"=>"Charge Card", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Allows partners to appove invoices for fleet managed companies that don't handle invoicing", "id"=>317, "name"=>"Disable Invoice Restrictions", "supported_by"=>["FleetInHouse"]},

    {"deleted_at"=>nil, "description"=>"Make Zip Field Optional For Credit Card Charges", "id"=>318, "name"=>"Optional Credit Card Zip", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>nil, "id"=>319, "name"=>"Job Member Number", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Disable Permanently Delete option in job menu", "id"=>321, "name"=>"Disable Delete Job", "supported_by"=>["RescueCompany"]},

    {"deleted_at"=>nil, "description"=>"Disables License Restrictions on the job form", "id"=>322, "name"=>"Disable License Restrictions", "supported_by"=>"[\"SuperCompany, FleetCompany, RescueCompany\"]"},

    {"deleted_at"=>nil, "description"=>nil, "id"=>323, "name"=>"Consumer SMS", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>nil, "id"=>325, "name"=>"Legacy GraphQL Errors", "supported_by"=>nil},

    {"deleted_at"=>nil, "description"=>"Feature to include Customer Lookup flow on job form", "id"=>326, "name"=>"Job Form Customer Lookup", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"Feature to include Coverage section on job form", "id"=>327, "name"=>"Job Form Coverage", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"On the job form, make a feature to get back end recommended drop off sites", "id"=>328, "name"=>"Job Form Recommended Drop Off", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"If enabled on a client company, the Site field should be shown to Swoop users in new/edit job form for that client", "id"=>329, "name"=>"Show Client Sites to Swoop", "supported_by"=>["FleetCompany"]},

    {"deleted_at"=>nil, "description"=>"Automatically apply coverage rule to all jobs that arrive via Client API", "id"=>334, "name"=>"Client API Coverage Override", "supported_by"=>["FleetCompany"]},

    {"deleted_at"=>nil, "description"=>"Enable country specific phone number formatting.", "id"=>335, "name"=>"International Phone Numbers", "supported_by"=>["RescueCompany", "FleetCompany", "SuperCompany"]},

    {"deleted_at"=>nil, "description"=>"Display Rental Car form button on Job Details", "id"=>336, "name"=>"Job Details Rental Car", "supported_by"=>["SuperCompany"]},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('features')
  VehicleCategory.import([

    {"id"=>105, "is_standard"=>nil, "name"=>"Carrier", "order_num"=>nil},

    {"id"=>106, "is_standard"=>true, "name"=>"Flatbed", "order_num"=>1},

    {"id"=>107, "is_standard"=>true, "name"=>"Heavy Duty", "order_num"=>5},

    {"id"=>108, "is_standard"=>nil, "name"=>"Heavy Duty After Hours", "order_num"=>nil},

    {"id"=>109, "is_standard"=>nil, "name"=>"Heavy Duty Combo", "order_num"=>nil},

    {"id"=>110, "is_standard"=>nil, "name"=>"Landoll", "order_num"=>nil},

    {"id"=>111, "is_standard"=>true, "name"=>"Light Duty", "order_num"=>3},

    {"id"=>112, "is_standard"=>nil, "name"=>"Light Duty After Hours", "order_num"=>nil},

    {"id"=>113, "is_standard"=>nil, "name"=>"Lowboy", "order_num"=>nil},

    {"id"=>114, "is_standard"=>nil, "name"=>"Mariposa", "order_num"=>nil},

    {"id"=>115, "is_standard"=>true, "name"=>"Medium Duty", "order_num"=>4},

    {"id"=>116, "is_standard"=>nil, "name"=>"Medium Duty After Hours", "order_num"=>nil},

    {"id"=>117, "is_standard"=>nil, "name"=>"Medium Duty Combo", "order_num"=>nil},

    {"id"=>118, "is_standard"=>nil, "name"=>"Motorcycle", "order_num"=>nil},

    {"id"=>119, "is_standard"=>nil, "name"=>"Multi-Axle Lowboy", "order_num"=>nil},

    {"id"=>120, "is_standard"=>nil, "name"=>"Oakhurst", "order_num"=>nil},

    {"id"=>121, "is_standard"=>nil, "name"=>"Rotator", "order_num"=>nil},

    {"id"=>122, "is_standard"=>nil, "name"=>"Service", "order_num"=>nil},

    {"id"=>123, "is_standard"=>nil, "name"=>"Sliding Axle", "order_num"=>nil},

    {"id"=>124, "is_standard"=>nil, "name"=>"Stepdeck", "order_num"=>nil},

    {"id"=>125, "is_standard"=>nil, "name"=>"Stretch", "order_num"=>nil},

    {"id"=>126, "is_standard"=>true, "name"=>"Super Heavy", "order_num"=>6},

    {"id"=>127, "is_standard"=>nil, "name"=>"Super Heavy After Hours", "order_num"=>nil},

    {"id"=>128, "is_standard"=>nil, "name"=>"Super Heavy Combo", "order_num"=>nil},

    {"id"=>129, "is_standard"=>nil, "name"=>"Tractor Only", "order_num"=>nil},

    {"id"=>130, "is_standard"=>true, "name"=>"Wheel Lift", "order_num"=>2},

    {"id"=>131, "is_standard"=>true, "name"=>"Service Truck", "order_num"=>7},

    {"id"=>132, "is_standard"=>nil, "name"=>"Rig", "order_num"=>nil},

    {"id"=>133, "is_standard"=>nil, "name"=>"Bus", "order_num"=>nil},

    {"id"=>134, "is_standard"=>nil, "name"=>"Motorhome", "order_num"=>nil},

    {"id"=>135, "is_standard"=>nil, "name"=>"Exotic Car", "order_num"=>nil},

    {"id"=>136, "is_standard"=>nil, "name"=>"Winch Off", "order_num"=>nil},

    {"id"=>137, "is_standard"=>nil, "name"=>"Winch On", "order_num"=>nil},

    {"id"=>138, "is_standard"=>nil, "name"=>"Lowboy Zone 1 (15mi)", "order_num"=>nil},

    {"id"=>139, "is_standard"=>nil, "name"=>"Lowboy Zone 2 (25mi)", "order_num"=>nil},

    {"id"=>140, "is_standard"=>nil, "name"=>"Lowboy Zone 3 (35mi)", "order_num"=>nil},

    {"id"=>141, "is_standard"=>nil, "name"=>"Sliding Axle Zone 1 (15mi)", "order_num"=>nil},

    {"id"=>142, "is_standard"=>nil, "name"=>"Sliding Axle Zone 2 (25mi)", "order_num"=>nil},

    {"id"=>143, "is_standard"=>nil, "name"=>"Sliding Axle Zone 3 (35mi)", "order_num"=>nil},

    {"id"=>144, "is_standard"=>nil, "name"=>"Carrier Zone 1 (15mi)", "order_num"=>nil},

    {"id"=>145, "is_standard"=>nil, "name"=>"Carrier Zone 2 (25mi)", "order_num"=>nil},

    {"id"=>146, "is_standard"=>nil, "name"=>"Carrier Zone 3 (35mi)", "order_num"=>nil},

    {"id"=>147, "is_standard"=>nil, "name"=>"Zone 3", "order_num"=>nil},

    {"id"=>148, "is_standard"=>nil, "name"=>"Zone 5", "order_num"=>nil},

    {"id"=>149, "is_standard"=>nil, "name"=>"Zone 6", "order_num"=>nil},

    {"id"=>150, "is_standard"=>nil, "name"=>"Zone 7", "order_num"=>nil},

    {"id"=>151, "is_standard"=>nil, "name"=>"Zone 8", "order_num"=>nil},

    {"id"=>152, "is_standard"=>nil, "name"=>"Zone 9", "order_num"=>nil},

    {"id"=>153, "is_standard"=>nil, "name"=>"Zone 11", "order_num"=>nil},

    {"id"=>154, "is_standard"=>nil, "name"=>"Zone 12", "order_num"=>nil},

    {"id"=>155, "is_standard"=>nil, "name"=>"Zone 13", "order_num"=>nil},

    {"id"=>156, "is_standard"=>nil, "name"=>"Zone 24", "order_num"=>nil},

    {"id"=>157, "is_standard"=>nil, "name"=>"Zone 37", "order_num"=>nil},

    {"id"=>158, "is_standard"=>nil, "name"=>"Zone 45", "order_num"=>nil},

    {"id"=>159, "is_standard"=>nil, "name"=>"Zone 54", "order_num"=>nil},

    {"id"=>160, "is_standard"=>nil, "name"=>"RV", "order_num"=>nil},

    {"id"=>161, "is_standard"=>nil, "name"=>"Zone 2", "order_num"=>nil},

    {"id"=>162, "is_standard"=>true, "name"=>"Covered Flatbed", "order_num"=>8},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('vehicle_categories')
  Symptom.import([

    {"deleted_at"=>nil, "id"=>153, "is_standard"=>true, "name"=>"Accident"},

    {"deleted_at"=>nil, "id"=>154, "is_standard"=>nil, "name"=>"Breakdown: Dealer"},

    {"deleted_at"=>nil, "id"=>155, "is_standard"=>nil, "name"=>"Breakdown: Shop"},

    {"deleted_at"=>nil, "id"=>156, "is_standard"=>nil, "name"=>"Car drives but is experiencing issue"},

    {"deleted_at"=>nil, "id"=>157, "is_standard"=>nil, "name"=>"Car will not charge – not enough range to reach service center"},

    {"deleted_at"=>nil, "id"=>158, "is_standard"=>nil, "name"=>"Car will not drive"},

    {"deleted_at"=>nil, "id"=>159, "is_standard"=>nil, "name"=>"Collision: Auction"},

    {"deleted_at"=>nil, "id"=>160, "is_standard"=>true, "name"=>"Collision: Body Shop"},

    {"deleted_at"=>nil, "id"=>163, "is_standard"=>true, "name"=>"Dead battery"},

    {"deleted_at"=>nil, "id"=>164, "is_standard"=>true, "name"=>"Engine will not turn over-low battery"},

    {"deleted_at"=>nil, "id"=>165, "is_standard"=>true, "name"=>"Flat tire"},

    {"deleted_at"=>nil, "id"=>167, "is_standard"=>nil, "name"=>"Flat tire- Runflat cust refuses to drive"},

    {"deleted_at"=>nil, "id"=>168, "is_standard"=>nil, "name"=>"Flat tire- Runflat sidewall damaged"},

    {"deleted_at"=>nil, "id"=>169, "is_standard"=>nil, "name"=>"Flat tire - Standard no spare, no kit"},

    {"deleted_at"=>nil, "id"=>170, "is_standard"=>nil, "name"=>"Flat tire - Standard with spare or kit"},

    {"deleted_at"=>nil, "id"=>171, "is_standard"=>nil, "name"=>"Keys locked inside passenger compartment"},

    {"deleted_at"=>nil, "id"=>172, "is_standard"=>nil, "name"=>"Keys locked in the trunk"},

    {"deleted_at"=>nil, "id"=>173, "is_standard"=>true, "name"=>"Locked out"},

    {"deleted_at"=>nil, "id"=>174, "is_standard"=>nil, "name"=>"Lockout Assistance"},

    {"deleted_at"=>nil, "id"=>175, "is_standard"=>true, "name"=>"Long distance tow"},

    {"deleted_at"=>nil, "id"=>176, "is_standard"=>nil, "name"=>"Mechanical Disablement"},

    {"deleted_at"=>nil, "id"=>177, "is_standard"=>true, "name"=>"Mechanical issue"},

    {"deleted_at"=>nil, "id"=>178, "is_standard"=>nil, "name"=>"Multiple flat tires"},

    {"deleted_at"=>nil, "id"=>179, "is_standard"=>nil, "name"=>"No issue reported"},

    {"deleted_at"=>nil, "id"=>180, "is_standard"=>true, "name"=>"Other"},

    {"deleted_at"=>nil, "id"=>181, "is_standard"=>nil, "name"=>"Out of charge"},

    {"deleted_at"=>nil, "id"=>182, "is_standard"=>true, "name"=>"Out of fuel"},

    {"deleted_at"=>nil, "id"=>183, "is_standard"=>nil, "name"=>"Ran out of range"},

    {"deleted_at"=>nil, "id"=>184, "is_standard"=>nil, "name"=>"Recovery"},

    {"deleted_at"=>nil, "id"=>185, "is_standard"=>nil, "name"=>"Reunite"},

    {"deleted_at"=>nil, "id"=>186, "is_standard"=>nil, "name"=>"Roadside"},

    {"deleted_at"=>nil, "id"=>187, "is_standard"=>nil, "name"=>"Service Center"},

    {"deleted_at"=>nil, "id"=>188, "is_standard"=>nil, "name"=>"Unauthorized use"},

    {"deleted_at"=>nil, "id"=>189, "is_standard"=>nil, "name"=>"Unknown"},

    {"deleted_at"=>nil, "id"=>190, "is_standard"=>nil, "name"=>"Vehicle abandoned"},

    {"deleted_at"=>nil, "id"=>204, "is_standard"=>true, "name"=>"Customer unsafe"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('symptoms')
  SubscriptionStatus.import([

    {"id"=>1, "name"=>"Network Only"},

    {"id"=>2, "name"=>"Free Trial"},

    {"id"=>3, "name"=>"Subscribed - Essential"},

    {"id"=>4, "name"=>"Subscribed - Premium"},

    {"id"=>5, "name"=>"Subscribed - Professional"},

    {"id"=>6, "name"=>"Subscribed - Enterprise"},

    {"id"=>7, "name"=>"Subscribed - Free"},

    {"id"=>8, "name"=>"Subscribed - Custom"},

    {"id"=>9, "name"=>"Inactive"},

    {"id"=>10, "name"=>"Pending Cancelation"},

    {"id"=>11, "name"=>"Options"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('subscription_statuses')
  CustomerType.import([

    {"id"=>73, "name"=>"Warranty"},

    {"id"=>74, "name"=>"Non-Warranty"},

    {"id"=>75, "name"=>"P Card"},

    {"id"=>76, "name"=>"Customer Pay"},

    {"id"=>77, "name"=>"Covered"},

    {"id"=>78, "name"=>"Claim/Damage"},

    {"id"=>79, "name"=>"Mechanical"},

    {"id"=>80, "name"=>"Auction"},

    {"id"=>81, "name"=>"For Review"},

    {"id"=>82, "name"=>"TBD"},

    {"id"=>83, "name"=>"Account"},

    {"id"=>84, "name"=>"Cash"},

    {"id"=>85, "name"=>"Check"},

    {"id"=>86, "name"=>"Comchek/EFS Check"},

    {"id"=>87, "name"=>"Credit Card"},

    {"id"=>88, "name"=>"PO"},

    {"id"=>89, "name"=>"Goodwill"},

    {"id"=>90, "name"=>"Service Center"},

    {"id"=>91, "name"=>"Body Shop"},

    {"id"=>92, "name"=>"Covered - Warranty"},

    {"id"=>99, "name"=>"Tax Exempt"},

    {"id"=>100, "name"=>"New Payment Typeasdf"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('customer_types')
  JobStatus.import([

    {"id"=>1, "name"=>"Pending"},

    {"id"=>2, "name"=>"Dispatched"},

    {"id"=>3, "name"=>"En Route"},

    {"id"=>4, "name"=>"On Site"},

    {"id"=>5, "name"=>"Towing"},

    {"id"=>6, "name"=>"Tow Destination"},

    {"id"=>7, "name"=>"Completed"},

    {"id"=>8, "name"=>"Canceled"},

    {"id"=>9, "name"=>"Created"},

    {"id"=>10, "name"=>"Assigned"},

    {"id"=>11, "name"=>"Accepted"},

    {"id"=>12, "name"=>"Rejected"},

    {"id"=>13, "name"=>"Reassign"},

    {"id"=>14, "name"=>"Initial"},

    {"id"=>15, "name"=>"Unassigned"},

    {"id"=>16, "name"=>"Reassigned"},

    {"id"=>17, "name"=>"Stored"},

    {"id"=>18, "name"=>"Released"},

    {"id"=>19, "name"=>"Submitted"},

    {"id"=>20, "name"=>"ETA Rejected"},

    {"id"=>21, "name"=>"GOA"},

    {"id"=>22, "name"=>"Expired"},

    {"id"=>23, "name"=>"Auto Assigning"},

    {"id"=>25, "name"=>"Deleted"},

    {"id"=>26, "name"=>"Draft"},

    {"id"=>27, "name"=>"Predraft"},

    {"id"=>28, "name"=>"ETA Extended"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('job_statuses')
  JobEtaExplanation.import([

    {"id"=>1, "text"=>"None"},

    {"id"=>2, "text"=>"Traffic"},

    {"id"=>3, "text"=>"Event In Area"},

    {"id"=>4, "text"=>"Construction"},

    {"id"=>5, "text"=>"Rural Disablement"},

    {"id"=>6, "text"=>"Service Provider Issue"},

    {"id"=>7, "text"=>"Weather"},

    {"id"=>8, "text"=>"Other"},

    {"id"=>9, "text"=>"none"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('job_eta_explanations')
  JobRejectReason.import([

    {"id"=>1, "reason_id"=>nil, "text"=>"Do not have requested tire size"},

    {"id"=>2, "reason_id"=>nil, "text"=>"Out of Service Area"},

    {"id"=>3, "reason_id"=>nil, "text"=>"Can not service within 60 mins"},

    {"id"=>4, "reason_id"=>nil, "text"=>"Not Interested"},

    {"id"=>5, "reason_id"=>nil, "text"=>"Other"},

    {"id"=>6, "reason_id"=>nil, "text"=>"Equipment Not Available"},

    {"id"=>7, "reason_id"=>nil, "text"=>"Restricted Roadway"},

    {"id"=>8, "reason_id"=>nil, "text"=>"Weather"},

    {"id"=>9, "reason_id"=>nil, "text"=>"Payment Issue"},

    {"id"=>10, "reason_id"=>nil, "text"=>"Unsafe Location"},

    {"id"=>11, "reason_id"=>nil, "text"=>"Service Not Available"},

    {"id"=>12, "reason_id"=>nil, "text"=>"Quote Not Accepted"},

    {"id"=>13, "reason_id"=>nil, "text"=>"No Drivers Available"},

    {"id"=>14, "reason_id"=>nil, "text"=>"Refused Call"},

    {"id"=>15, "reason_id"=>nil, "text"=>"Event"},

    {"id"=>16, "reason_id"=>nil, "text"=>"Traffic"},

    {"id"=>17, "reason_id"=>nil, "text"=>"Do not accept payment type"},

    {"id"=>18, "reason_id"=>nil, "text"=>"Proper equipment not available"},

    {"id"=>19, "reason_id"=>nil, "text"=>"No longer offer service"},

    {"id"=>20, "reason_id"=>nil, "text"=>"Out of my coverage area"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('job_reject_reasons')
  TireType.import([

    {"car"=>"Model S", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>nil, "id"=>1, "position"=>nil, "size"=>"245/45R19"},

    {"car"=>"Model S", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>nil, "id"=>2, "position"=>nil, "size"=>"245/35R21"},

    {"car"=>"Model S", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>nil, "id"=>3, "position"=>"REAR", "size"=>"265/35R21"},

    {"car"=>"Model S", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>"Winter", "id"=>4, "position"=>nil, "size"=>"245/45R19"},

    {"car"=>"Model X", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>nil, "id"=>5, "position"=>"FRONT", "size"=>"255/45R20"},

    {"car"=>"Model X", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>nil, "id"=>6, "position"=>"REAR", "size"=>"275/45R20"},

    {"car"=>"Model X", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>nil, "id"=>7, "position"=>"FRONT", "size"=>"265/35R22"},

    {"car"=>"Model X", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>nil, "id"=>8, "position"=>"REAR", "size"=>"285/35R22"},

    {"car"=>"Model X", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>"Winter", "id"=>9, "position"=>"FRONT", "size"=>"265/45R20"},

    {"car"=>"Model X", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>"Winter", "id"=>10, "position"=>"REAR", "size"=>"275/45R20"},

    {"car"=>"Model S", "deleted_at"=>"2016-12-13T20:19:50.288Z", "extras"=>nil, "id"=>11, "position"=>nil, "size"=>"245"},

    {"car"=>"Model S", "deleted_at"=>nil, "extras"=>nil, "id"=>12, "position"=>nil, "size"=>"265"},

    {"car"=>"Model X", "deleted_at"=>nil, "extras"=>nil, "id"=>13, "position"=>nil, "size"=>"255"},

    {"car"=>"Model 3", "deleted_at"=>nil, "extras"=>nil, "id"=>14, "position"=>nil, "size"=>"235"},

    {"car"=>"Model S", "deleted_at"=>nil, "extras"=>nil, "id"=>15, "position"=>nil, "size"=>nil},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('tire_types')
  LocationType.import([

    {"deleted_at"=>nil, "id"=>1, "name"=>"Auto Body Shop"},

    {"deleted_at"=>nil, "id"=>2, "name"=>"Blocking Traffic"},

    {"deleted_at"=>nil, "id"=>3, "name"=>"Business"},

    {"deleted_at"=>nil, "id"=>4, "name"=>"Gated Community"},

    {"deleted_at"=>nil, "id"=>5, "name"=>"Highway"},

    {"deleted_at"=>nil, "id"=>6, "name"=>"Local Roadside"},

    {"deleted_at"=>nil, "id"=>7, "name"=>"Low Clearance"},

    {"deleted_at"=>nil, "id"=>8, "name"=>"Parking Garage"},

    {"deleted_at"=>nil, "id"=>9, "name"=>"Parking Lot"},

    {"deleted_at"=>nil, "id"=>10, "name"=>"Residence"},

    {"deleted_at"=>nil, "id"=>11, "name"=>"Point of Interest"},

    {"deleted_at"=>nil, "id"=>12, "name"=>"Intersection"},

    {"deleted_at"=>nil, "id"=>13, "name"=>"3rd Party Tire Shop"},

    {"deleted_at"=>nil, "id"=>14, "name"=>"Tesla Service Center"},

    {"deleted_at"=>nil, "id"=>15, "name"=>"Supercharger"},

    {"deleted_at"=>nil, "id"=>16, "name"=>"Impound Lot"},

    {"deleted_at"=>nil, "id"=>17, "name"=>"Storage Facility"},

    {"deleted_at"=>nil, "id"=>18, "name"=>"Tow Company"},

    {"deleted_at"=>nil, "id"=>19, "name"=>"Auction"},

    {"deleted_at"=>nil, "id"=>20, "name"=>"Service Center"},

    {"deleted_at"=>nil, "id"=>21, "name"=>"Dealership"},

    {"deleted_at"=>nil, "id"=>22, "name"=>"HQ"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('location_types')
  ModelType.import([

    {"id"=>1, "name"=>"Delayed::Backend::ActiveRecord::Job"},

    {"id"=>2, "name"=>"Doorkeeper::AccessGrant"},

    {"id"=>3, "name"=>"Doorkeeper::AccessToken"},

    {"id"=>4, "name"=>"Doorkeeper::Application"},

    {"id"=>5, "name"=>"HABTM_Companies"},

    {"id"=>6, "name"=>"HABTM_Roles"},

    {"id"=>7, "name"=>"HABTM_HiddenSites"},

    {"id"=>8, "name"=>"HABTM_HiddenDispatchers"},

    {"id"=>9, "name"=>"HABTM_HiddenStorageTypes"},

    {"id"=>10, "name"=>"HABTM_HiddenStorageSites"},

    {"id"=>11, "name"=>"ActiveRecord::InternalMetadata"},

    {"id"=>12, "name"=>"SeedMigration::DataMigration"},

    {"id"=>13, "name"=>"HABTM_Products"},

    {"id"=>14, "name"=>"HABTM_Features"},

    {"id"=>15, "name"=>"HABTM_Reports"},

    {"id"=>16, "name"=>"HABTM_Addons"},

    {"id"=>17, "name"=>"HABTM_FleetInHouseClients"},

    {"id"=>18, "name"=>"HABTM_FleetManagedClients"},

    {"id"=>19, "name"=>"HABTM_LookerDashboards"},

    {"id"=>20, "name"=>"HABTM_Tags"},

    {"id"=>21, "name"=>"HABTM_Users"},

    {"id"=>22, "name"=>"HABTM_RescueProviders"},

    {"id"=>23, "name"=>"ServiceCode"},

    {"id"=>24, "name"=>"EventOperation"},

    {"id"=>25, "name"=>"Feature"},

    {"id"=>26, "name"=>"VehicleCategory"},

    {"id"=>27, "name"=>"Symptom"},

    {"id"=>28, "name"=>"CustomerType"},

    {"id"=>29, "name"=>"JobStatus"},

    {"id"=>30, "name"=>"JobEtaExplanation"},

    {"id"=>31, "name"=>"JobRejectReason"},

    {"id"=>32, "name"=>"TireType"},

    {"id"=>33, "name"=>"ModelType"},

    {"id"=>34, "name"=>"StorageType"},

    {"id"=>35, "name"=>"AuditJobStatus"},

    {"id"=>36, "name"=>"AddExpiredStatus"},

    {"id"=>37, "name"=>"Answer"},

    {"id"=>38, "name"=>"API::AsyncRequest"},

    {"id"=>39, "name"=>"ArChange"},

    {"id"=>40, "name"=>"AuditEmail"},

    {"id"=>41, "name"=>"AuditIssc"},

    {"id"=>42, "name"=>"AuditSearch"},

    {"id"=>43, "name"=>"AuditSms"},

    {"id"=>44, "name"=>"AutoAssign::Assignment"},

    {"id"=>45, "name"=>"AutoAssign::AutoAssignCandidate"},

    {"id"=>46, "name"=>"AutoAssign::AutoAssignCandidateJob"},

    {"id"=>47, "name"=>"Command"},

    {"id"=>48, "name"=>"CommissionExclusion"},

    {"id"=>49, "name"=>"CompaniesCustomerType"},

    {"id"=>50, "name"=>"CompaniesIssues"},

    {"id"=>51, "name"=>"CompaniesLocationType"},

    {"id"=>52, "name"=>"CompaniesService"},

    {"id"=>53, "name"=>"CompaniesVehicleCategory"},

    {"id"=>54, "name"=>"CompanyCommission"},

    {"id"=>55, "name"=>"CompanyReport"},

    {"id"=>56, "name"=>"CompanyServiceQuestion"},

    {"id"=>57, "name"=>"CompanySymptom"},

    {"id"=>58, "name"=>"SmsAlert"},

    {"id"=>59, "name"=>"Device"},

    {"id"=>60, "name"=>"Drive"},

    {"id"=>61, "name"=>"DriverCommission"},

    {"id"=>62, "name"=>"Estimate"},

    {"id"=>63, "name"=>"Experiment"},

    {"id"=>64, "name"=>"ExternalRequest"},

    {"id"=>65, "name"=>"Report"},

    {"id"=>66, "name"=>"HiddenUsersDispatcher"},

    {"id"=>67, "name"=>"HiddenUsersStorageSite"},

    {"id"=>68, "name"=>"HiddenUsersStorageType"},

    {"id"=>69, "name"=>"InvoiceRejectReason"},

    {"id"=>70, "name"=>"InvoiceRejection"},

    {"id"=>71, "name"=>"InvoicingTaxRate"},

    {"id"=>72, "name"=>"IsscLocation"},

    {"id"=>73, "name"=>"IsscLogin"},

    {"id"=>74, "name"=>"JobError"},

    {"id"=>75, "name"=>"LookerDashboard"},

    {"id"=>76, "name"=>"LookerSpace"},

    {"id"=>77, "name"=>"Metric"},

    {"id"=>78, "name"=>"NewInvoiceLock"},

    {"id"=>79, "name"=>"OauthAccessToken"},

    {"id"=>80, "name"=>"PasswordRequest"},

    {"id"=>81, "name"=>"PaymentMethod"},

    {"id"=>82, "name"=>"PCC::Claim"},

    {"id"=>83, "name"=>"PCC::Coverage"},

    {"id"=>84, "name"=>"PCC::Policy"},

    {"id"=>85, "name"=>"PCC::User"},

    {"id"=>86, "name"=>"PCC::Vehicle"},

    {"id"=>87, "name"=>"PhoneNumberBlacklist"},

    {"id"=>88, "name"=>"Price"},

    {"id"=>89, "name"=>"PriceType"},

    {"id"=>90, "name"=>"Product"},

    {"id"=>91, "name"=>"QB::Token"},

    {"id"=>92, "name"=>"Question"},

    {"id"=>93, "name"=>"RateAddition"},

    {"id"=>94, "name"=>"Referer"},

    {"id"=>95, "name"=>"RescueDriver"},

    {"id"=>96, "name"=>"Role"},

    {"id"=>97, "name"=>"SystemAlert"},

    {"id"=>98, "name"=>"Search"},

    {"id"=>99, "name"=>"ServiceAlias"},

    {"id"=>100, "name"=>"ServiceCommissionsExclusions"},

    {"id"=>101, "name"=>"SlackChannel"},

    {"id"=>102, "name"=>"Survey"},

    {"id"=>103, "name"=>"Survey::Question"},

    {"id"=>104, "name"=>"Survey::Result"},

    {"id"=>105, "name"=>"Truck"},

    {"id"=>106, "name"=>"TruckDriver"},

    {"id"=>107, "name"=>"TruckType"},

    {"id"=>108, "name"=>"TtAccount"},

    {"id"=>109, "name"=>"TtMessage"},

    {"id"=>110, "name"=>"TtObject"},

    {"id"=>111, "name"=>"TtScrape"},

    {"id"=>112, "name"=>"UpdateInvoiceLock"},

    {"id"=>113, "name"=>"UserExperiment"},

    {"id"=>114, "name"=>"Variant"},

    {"id"=>115, "name"=>"VehicleDriver"},

    {"id"=>116, "name"=>"VehicleMake"},

    {"id"=>117, "name"=>"VehicleModel"},

    {"id"=>118, "name"=>"VehicleType"},

    {"id"=>119, "name"=>"VehicleUser"},

    {"id"=>120, "name"=>"ViewAlert"},

    {"id"=>121, "name"=>"LocationType"},

    {"id"=>122, "name"=>"User"},

    {"id"=>123, "name"=>"Version"},

    {"id"=>124, "name"=>"Account"},

    {"id"=>125, "name"=>"Company"},

    {"id"=>126, "name"=>"Job"},

    {"id"=>127, "name"=>"Department"},

    {"id"=>128, "name"=>"InventoryItem"},

    {"id"=>129, "name"=>"InvoicingLedgerItem"},

    {"id"=>130, "name"=>"Rate"},

    {"id"=>131, "name"=>"ReportResult"},

    {"id"=>132, "name"=>"Site"},

    {"id"=>133, "name"=>"Vehicle"},

    {"id"=>134, "name"=>"CustomAccount"},

    {"id"=>135, "name"=>"RescueProvider"},

    {"id"=>136, "name"=>"Setting"},

    {"id"=>137, "name"=>"UserSetting"},

    {"id"=>138, "name"=>"AttachedDocument"},

    {"id"=>139, "name"=>"Auction::Auction"},

    {"id"=>140, "name"=>"Auction::Bid"},

    {"id"=>142, "name"=>"CompaniesFeature"},

    {"id"=>143, "name"=>"Explanation"},

    {"id"=>144, "name"=>"HiddenUsersSite"},

    {"id"=>145, "name"=>"Image"},

    {"id"=>146, "name"=>"InvoicePdf"},

    {"id"=>147, "name"=>"InvoicingLineItem"},

    {"id"=>148, "name"=>"Issc"},

    {"id"=>149, "name"=>"IsscDispatchRequest"},

    {"id"=>150, "name"=>"Issue"},

    {"id"=>151, "name"=>"JobStatusEmailPreference"},

    {"id"=>152, "name"=>"Location"},

    {"id"=>153, "name"=>"Notification"},

    {"id"=>154, "name"=>"Review"},

    {"id"=>155, "name"=>"QuestionResult"},

    {"id"=>156, "name"=>"Reason"},

    {"id"=>157, "name"=>"RescueProvidersTag"},

    {"id"=>158, "name"=>"SingleUseAccessToken"},

    {"id"=>159, "name"=>"SystemEmailFilter"},

    {"id"=>160, "name"=>"Tag"},

    {"id"=>161, "name"=>"TimeOfArrival"},

    {"id"=>162, "name"=>"Tire"},

    {"id"=>163, "name"=>"TwilioReply"},

    {"id"=>164, "name"=>"UserSite"},

    {"id"=>165, "name"=>"FleetCompany"},

    {"id"=>166, "name"=>"RescueCompany"},

    {"id"=>167, "name"=>"SuperCompany"},

    {"id"=>168, "name"=>"RescueJob"},

    {"id"=>169, "name"=>"FleetJob"},

    {"id"=>170, "name"=>"FleetInHouseJob"},

    {"id"=>171, "name"=>"FleetManagedJob"},

    {"id"=>172, "name"=>"FleetMotorClubJob"},

    {"id"=>173, "name"=>"VehicleInventoryItem"},

    {"id"=>174, "name"=>"Invoice"},

    {"id"=>175, "name"=>"CreditNote"},

    {"id"=>176, "name"=>"Payment"},

    {"id"=>177, "name"=>"AccountInvoice"},

    {"id"=>178, "name"=>"EndUserInvoice"},

    {"id"=>179, "name"=>"Discount"},

    {"id"=>180, "name"=>"WriteOff"},

    {"id"=>181, "name"=>"Refund"},

    {"id"=>182, "name"=>"FlatRate"},

    {"id"=>183, "name"=>"HoursP2PRate"},

    {"id"=>184, "name"=>"MilesEnRouteOnlyRate"},

    {"id"=>185, "name"=>"MilesEnRouteRate"},

    {"id"=>186, "name"=>"MilesP2PRate"},

    {"id"=>187, "name"=>"MilesTowedRate"},

    {"id"=>188, "name"=>"StorageRate"},

    {"id"=>189, "name"=>"TeslaRate"},

    {"id"=>190, "name"=>"FleetSite"},

    {"id"=>191, "name"=>"PartnerSite"},

    {"id"=>192, "name"=>"PoiSite"},

    {"id"=>195, "name"=>"ConsumerVehicle"},

    {"id"=>196, "name"=>"RescueVehicle"},

    {"id"=>197, "name"=>"StrandedVehicle"},

    {"id"=>198, "name"=>"TrailerVehicle"},

    {"id"=>199, "name"=>"AttachedDocument::Document"},

    {"id"=>200, "name"=>"AttachedDocument::Image"},

    {"id"=>201, "name"=>"AttachedDocument::LocationImage"},

    {"id"=>202, "name"=>"AttachedDocument::SignatureImage"},

    {"id"=>203, "name"=>"NpsReview"},

    {"id"=>204, "name"=>"NpsLinkReview"},

    {"id"=>205, "name"=>"Reviews::WebReview"},

    {"id"=>206, "name"=>"NpsChooseReview"},

    {"id"=>207, "name"=>"NpsPureReview"},

    {"id"=>208, "name"=>"Reviews::SurveyReview"},

    {"id"=>209, "name"=>"Reviews::Typeform"},

    {"id"=>210, "name"=>"DriverDispatchSms"},

    {"id"=>211, "name"=>"EtaAuditSms"},

    {"id"=>212, "name"=>"JobAuditSms"},

    {"id"=>213, "name"=>"LocationAuditSms"},

    {"id"=>214, "name"=>"PasswordRequestAuditSms"},

    {"id"=>215, "name"=>"ReviewAuditSms"},

    {"id"=>216, "name"=>"ReviewFollowup2AuditSms"},

    {"id"=>217, "name"=>"ReviewFollowupAuditSms"},

    {"id"=>218, "name"=>"Sms::Base"},

    {"id"=>219, "name"=>"Sms::BrandedReviewLink"},

    {"id"=>220, "name"=>"Sms::ConfirmLocation"},

    {"id"=>221, "name"=>"Sms::ConfirmLocationEta"},

    {"id"=>222, "name"=>"Sms::ConfirmLocationSched"},

    {"id"=>223, "name"=>"Sms::ConfirmPartnerCompleted"},

    {"id"=>224, "name"=>"Sms::ConfirmPartnerOnSite"},

    {"id"=>225, "name"=>"Sms::ConfirmPartnerOnSiteFollowUp"},

    {"id"=>226, "name"=>"Sms::Confirmation"},

    {"id"=>227, "name"=>"Sms::ConfirmationEta"},

    {"id"=>228, "name"=>"Sms::ConfirmationSched"},

    {"id"=>229, "name"=>"Sms::Eta"},

    {"id"=>230, "name"=>"Sms::GreatThanks"},

    {"id"=>231, "name"=>"Sms::PartnerNotCompleted"},

    {"id"=>232, "name"=>"Sms::PartnerNotOnSite"},

    {"id"=>233, "name"=>"Sms::Reassign"},

    {"id"=>234, "name"=>"Sms::ReviewLink"},

    {"id"=>235, "name"=>"Sms::ReviseEta"},

    {"id"=>236, "name"=>"Sms::ScheduleReminder"},

    {"id"=>237, "name"=>"AutoAssign::SiteCandidate"},

    {"id"=>238, "name"=>"AutoAssign::TruckCandidate"},

    {"id"=>239, "name"=>"Commands::TowOut"},

    {"id"=>240, "name"=>"ConfirmPartnerCompletedSmsAlert"},

    {"id"=>241, "name"=>"ConfirmPartnerOnSiteSmsAlert"},

    {"id"=>242, "name"=>"ScheduledSystemAlert"},

    {"id"=>243, "name"=>"Survey::MobileSwoop"},

    {"id"=>244, "name"=>"Survey::Smiley"},

    {"id"=>245, "name"=>"Survey::Typeform"},

    {"id"=>246, "name"=>"Survey::FiveStarQuestion"},

    {"id"=>247, "name"=>"Survey::LongTextQuestion"},

    {"id"=>248, "name"=>"Survey::TenStarQuestion"},

    {"id"=>249, "name"=>"HABTM_Groups"},

    {"id"=>250, "name"=>"Group"},

    {"id"=>251, "name"=>"AgeroRSALog"},

    {"id"=>252, "name"=>"Assignment"},

    {"id"=>253, "name"=>"CompanyInvoiceChargeFee"},

    {"id"=>254, "name"=>"CompanyRolePermission"},

    {"id"=>255, "name"=>"Note"},

    {"id"=>256, "name"=>"HistoryItem"},

    {"id"=>257, "name"=>"InvoicePaymentCharge"},

    {"id"=>258, "name"=>"FleetInternalNote"},

    {"id"=>259, "name"=>"FleetPublicNote"},

    {"id"=>260, "name"=>"PartnerDispatchNote"},

    {"id"=>261, "name"=>"PartnerInternalNote"},

    {"id"=>262, "name"=>"PartnerInvoiceNote"},

    {"id"=>263, "name"=>"SwoopInternalNote"},

    {"id"=>264, "name"=>"Customer"},

    {"id"=>265, "name"=>"APIAccessToken"},

    {"id"=>266, "name"=>"Auction::CandidateRanker"},

    {"id"=>267, "name"=>"Rating"},

    {"id"=>268, "name"=>"Auction::WeightedSumCandidateRanker"},

    {"id"=>269, "name"=>"SubscriptionStatus"},

    {"id"=>270, "name"=>"AuditCompanySubscriptionStatus"},

    {"id"=>271, "name"=>"CompanyRatingSetting"},

    {"id"=>272, "name"=>"Auction::Ranker"},

    {"id"=>273, "name"=>"Auction::WeightedSumRanker"},

    {"id"=>274, "name"=>"JobStatusReason"},

    {"id"=>275, "name"=>"JobStatusReasonType"},

    {"id"=>276, "name"=>"SiteFacilityMap"},

    {"id"=>277, "name"=>"Permission"},

    {"id"=>278, "name"=>"GraphQL::Pro::OperationStore::ActiveRecordAdapter::GraphQLClient"},

    {"id"=>279, "name"=>"GraphQL::Pro::OperationStore::ActiveRecordAdapter::GraphQLClientOperation"},

    {"id"=>280, "name"=>"GraphQL::Pro::OperationStore::ActiveRecordAdapter::GraphQLIndexEntry"},

    {"id"=>281, "name"=>"GraphQL::Pro::OperationStore::ActiveRecordAdapter::GraphQLIndexReference"},

    {"id"=>282, "name"=>"GraphQL::Pro::OperationStore::ActiveRecordAdapter::GraphQLOperation"},

    {"id"=>283, "name"=>"PublicNote"},

    {"id"=>284, "name"=>"ExperimentMetric"},

    {"id"=>285, "name"=>"TargetVariant"},

    {"id"=>286, "name"=>"UsersRoles"},

    {"id"=>287, "name"=>"CompanyJobStatusReasonType"},

    {"id"=>288, "name"=>"ClientProgram"},

    {"id"=>289, "name"=>"Job::Candidate"},

    {"id"=>290, "name"=>"Job::Candidate::Assignment"},

    {"id"=>291, "name"=>"Job::Candidate::Truck"},

    {"id"=>292, "name"=>"Job::Candidate::Site"},

    {"id"=>293, "name"=>"Job::Candidate::CandidateJob"},

    {"id"=>294, "name"=>"RateType"},

    {"id"=>295, "name"=>"Job::BillingInfo"},

    {"id"=>296, "name"=>"AuditInvoiceStatus"},

    {"id"=>297, "name"=>"CompanyRole"},

    {"id"=>298, "name"=>"UserNotificationSetting"},

    {"id"=>299, "name"=>"HABTM_GraphQLTopics"},

    {"id"=>300, "name"=>"HABTM_GraphQLSubscriptions"},

    {"id"=>301, "name"=>"GraphQLSubscription"},

    {"id"=>302, "name"=>"GraphQLTopic"},

    {"id"=>303, "name"=>"SourceApplication"},

    {"id"=>304, "name"=>"User::Visit"},

    {"id"=>305, "name"=>"Territory"},

    {"id"=>306, "name"=>"ClientProgramServiceNotes"},

    {"id"=>308, "name"=>"PartnerAPIEmailVerification"},

    {"id"=>309, "name"=>"TerritoryAuditTrail"},

    {"id"=>310, "name"=>"PartnerAPIPasswordRequest"},

    {"id"=>311, "name"=>"ZipCodeSubsetToTerritory"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('model_types')
  JobStatusReasonType.import([

    {"id"=>1, "is_standard"=>nil, "job_status_id"=>8, "key"=>"cancel_prior_job_delayed", "text"=>"Prior job delayed"},

    {"id"=>2, "is_standard"=>nil, "job_status_id"=>8, "key"=>"cancel_traffic_service_vehicle_problem", "text"=>"Traffic/service vehicle problem"},

    {"id"=>3, "is_standard"=>nil, "job_status_id"=>8, "key"=>"cancel_out_of_area", "text"=>"Out of area"},

    {"id"=>4, "is_standard"=>nil, "job_status_id"=>8, "key"=>"cancel_another_job_priority", "text"=>"Another job priority"},

    {"id"=>5, "is_standard"=>nil, "job_status_id"=>8, "key"=>"cancel_no_reason_given", "text"=>"No reason given"},

    {"id"=>6, "is_standard"=>nil, "job_status_id"=>8, "key"=>"cancel_customer_found_alternate_solution", "text"=>"Customer found alternate solution"},

    {"id"=>7, "is_standard"=>nil, "job_status_id"=>21, "key"=>"goa_customer_cancel_after_deadline", "text"=>"Customer cancel after deadline"},

    {"id"=>8, "is_standard"=>nil, "job_status_id"=>21, "key"=>"goa_wrong_location_given", "text"=>"Wrong location given"},

    {"id"=>9, "is_standard"=>nil, "job_status_id"=>21, "key"=>"goa_customer_not_with_vehicle", "text"=>"Customer not with vehicle"},

    {"id"=>10, "is_standard"=>nil, "job_status_id"=>21, "key"=>"goa_incorrect_service", "text"=>"Incorrect service"},

    {"id"=>11, "is_standard"=>nil, "job_status_id"=>21, "key"=>"goa_incorrect_equipment", "text"=>"Incorrect equipment"},

    {"id"=>12, "is_standard"=>nil, "job_status_id"=>21, "key"=>"goa_unsuccessful_service_attempt", "text"=>"Unsuccessful service attempt"},

    {"id"=>13, "is_standard"=>true, "job_status_id"=>11, "key"=>"accepted_rural_disablement", "text"=>"Rural Disablement"},

    {"id"=>14, "is_standard"=>true, "job_status_id"=>11, "key"=>"accepted_service_provider_issue", "text"=>"Service Provider Issue"},

    {"id"=>15, "is_standard"=>true, "job_status_id"=>11, "key"=>"accepted_weather", "text"=>"Weather"},

    {"id"=>16, "is_standard"=>true, "job_status_id"=>11, "key"=>"accepted_other", "text"=>"Other"},

    {"id"=>17, "is_standard"=>true, "job_status_id"=>11, "key"=>"accepted_none", "text"=>"None"},

    {"id"=>18, "is_standard"=>true, "job_status_id"=>11, "key"=>"accepted_traffic", "text"=>"Traffic"},

    {"id"=>19, "is_standard"=>true, "job_status_id"=>11, "key"=>"accepted_event_in_area", "text"=>"Event In Area"},

    {"id"=>20, "is_standard"=>true, "job_status_id"=>11, "key"=>"accepted_construction", "text"=>"Construction"},

    {"id"=>21, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_tire_size", "text"=>"Do not have requested tire size"},

    {"id"=>22, "is_standard"=>true, "job_status_id"=>12, "key"=>"rejected_out_of_service_area", "text"=>"Out of Service Area"},

    {"id"=>23, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_more_than_60_mins", "text"=>"Can not service within 60 mins"},

    {"id"=>24, "is_standard"=>true, "job_status_id"=>12, "key"=>"rejected_not_interested", "text"=>"Not Interested"},

    {"id"=>25, "is_standard"=>true, "job_status_id"=>12, "key"=>"rejected_other", "text"=>"Other"},

    {"id"=>26, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_equipment_not_available", "text"=>"Equipment Not Available"},

    {"id"=>27, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_restricted_roadway", "text"=>"Restricted Roadway"},

    {"id"=>28, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_weather", "text"=>"Weather"},

    {"id"=>29, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_payment_issue", "text"=>"Payment Issue"},

    {"id"=>30, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_unsafe_location", "text"=>"Unsafe Location"},

    {"id"=>31, "is_standard"=>true, "job_status_id"=>12, "key"=>"rejected_service_not_available", "text"=>"Service Not Available"},

    {"id"=>32, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_quote_not_accepted", "text"=>"Quote Not Accepted"},

    {"id"=>33, "is_standard"=>true, "job_status_id"=>12, "key"=>"rejected_no_drivers_available", "text"=>"No Drivers Available"},

    {"id"=>34, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_refused_call", "text"=>"Refused Call"},

    {"id"=>35, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_event", "text"=>"Event"},

    {"id"=>36, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_traffic", "text"=>"Traffic"},

    {"id"=>37, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_dont_accept_payment_type", "text"=>"Do not accept payment type"},

    {"id"=>38, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_proper_equipment_not_available", "text"=>"Proper equipment not available"},

    {"id"=>39, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_no_longer_offer_service", "text"=>"No longer offer service"},

    {"id"=>40, "is_standard"=>nil, "job_status_id"=>12, "key"=>"rejected_out_of_coverage_area", "text"=>"Out of my coverage area"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('job_status_reason_types')
  PaymentMethod.import([

    {"id"=>1, "name"=>"ACH"},

    {"id"=>2, "name"=>"American Express"},

    {"id"=>3, "name"=>"Cash"},

    {"id"=>4, "name"=>"Check"},

    {"id"=>5, "name"=>"Credit Card"},

    {"id"=>6, "name"=>"Debit Card"},

    {"id"=>7, "name"=>"Discover"},

    {"id"=>8, "name"=>"MasterCard"},

    {"id"=>9, "name"=>"Visa"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('payment_methods')
  Role.import([

    {"id"=>1, "name"=>"driver", "resource_id"=>nil, "resource_type"=>nil},

    {"id"=>2, "name"=>"rescue", "resource_id"=>nil, "resource_type"=>nil},

    {"id"=>3, "name"=>"fleet", "resource_id"=>nil, "resource_type"=>nil},

    {"id"=>4, "name"=>"admin", "resource_id"=>nil, "resource_type"=>nil},

    {"id"=>5, "name"=>"root", "resource_id"=>nil, "resource_type"=>nil},

    {"id"=>6, "name"=>"dispatcher", "resource_id"=>nil, "resource_type"=>nil},

    {"id"=>9, "name"=>"answering_service", "resource_id"=>nil, "resource_type"=>nil},

    {"id"=>10, "name"=>"swoop_dispatcher", "resource_id"=>nil, "resource_type"=>nil},

    {"id"=>11, "name"=>"super_fleet_managed", "resource_id"=>nil, "resource_type"=>nil},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('roles')
  Referer.import([

    {"deleted_at"=>nil, "id"=>1, "name"=>"Advertisement"},

    {"deleted_at"=>nil, "id"=>2, "name"=>"Agero"},

    {"deleted_at"=>nil, "id"=>3, "name"=>"App Store"},

    {"deleted_at"=>nil, "id"=>4, "name"=>"Facebook"},

    {"deleted_at"=>nil, "id"=>5, "name"=>"Geico"},

    {"deleted_at"=>nil, "id"=>6, "name"=>"Instagram"},

    {"deleted_at"=>nil, "id"=>7, "name"=>"Motor Club"},

    {"deleted_at"=>nil, "id"=>8, "name"=>"Other"},

    {"deleted_at"=>nil, "id"=>9, "name"=>"Press / News"},

    {"deleted_at"=>nil, "id"=>10, "name"=>"Referral"},

    {"deleted_at"=>nil, "id"=>11, "name"=>"Swoop Dispatch"},

    {"deleted_at"=>nil, "id"=>12, "name"=>"Swoop Email"},

    {"deleted_at"=>nil, "id"=>13, "name"=>"Web Search"},
  ], validate: false)
  ActiveRecord::Base.connection.reset_pk_sequence!('referers')
end

SeedMigration::Migrator.bootstrap(20190920181038)