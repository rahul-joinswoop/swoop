# frozen_string_literal: true

# Container for redis clients that are configured per environment in config/redis.yml
# There will only be a single instance of each client wrapped inside ConnectionPool.
class RedisClient

  # Redis like class that can be persisted in external libraries and make connections
  # automatically when used.
  class WrappedRedis < Delegator

    def initialize(name)
      @name = name
    end

    def __getobj__
      RedisClient.client(@name)
    end

  end

  # ConnectionPool like class that can be persisted in external libraries and make connections
  # automatically when used.
  class WrappedConnectionPool < ConnectionPool

    def initialize(name)
      @name = name.to_s
    end

    def with(*args, &block)
      connection_pool.with(*args, &block)
    end

    def checkout(*args)
      connection_pool.checkout(*args)
    end

    def checkin
      connection_pool.checkin
    end

    def shutdown(&block)
      # no-op
    end

    def size
      connection_pool.size
    end

    def available
      connection_pool.available
    end

    private

    def connection_pool
      RedisClient.connection_pool(@name)
    end

  end

  @clients = {}
  @mutex = Mutex.new

  class << self

    # Return a ConnectionPool for the named client. Each client will be a singleton
    # connection pool.
    def connection_pool(name)
      get_client(name)
    end

    # Return a ConnectionPool::Wrapper that can be used just like a Redis object.
    def client(name)
      client = get_client(name)
      if client.is_a?(ConnectionPool)
        client = ConnectionPool::Wrapper.new(pool: client)
      end
      client
    end
    alias_method :[], :client

    # Yields a redia connection from the named pool inside the block.
    # Shorthand for connection_pool(name).with { }
    def with(name, &block)
      connection_pool(name).with(&block)
    end

    # Disconnects all redis clients. The clients will automatically reconnect if they are used again.
    # This should be called after forking a process.
    def disconnect!
      connections = nil
      @mutex.synchronize do
        connections = @clients.values
        @clients = {}
      end
      connections.each do |connection|
        close_connection(connection)
      end
      nil
    end

    # Return the redis configuration for the named configuration.
    def options(name)
      load_config unless defined?(@config) && @config
      options = @config[name.to_s]
      options.dup if options
    end

    # Return the names of all clients
    def names
      load_config unless defined?(@config) && @config
      keys = []
      @config.each do |key, values|
        keys << key if values[:url].present? || values[:host].present?
      end
      keys
    end

    def each
      names.each do |name|
        redis = client(name)
        yield(redis) if redis
      end
    end

    def url(name)
      redis_url(options(name))
    end

    private

    def load_config
      config_file = Rails.root + "config" + "redis.yml"
      config = YAML.load(ERB.new(config_file.read).result)[Rails.env].deep_symbolize_keys
      pool_config = {}
      client_configs = {}
      config.each do |key, values|
        pool_values = values.delete(:pool)
        pool_config[key.to_s] = pool_values.freeze if pool_values
        client_configs[key.to_s] = values.freeze
      end
      @pool_config = pool_config.freeze
      @config = client_configs.freeze
    end

    def redis_url(options)
      url = options[:url] || "redis://#{options[:host] || 'localhost'}:#{options[:port] || 6379}"
      uri = URI.parse(url)
      uri.path = "/#{options[:db]}" if options[:db].present?
      uri.user = options[:username] if options[:username]
      uri.password = options[:password] if options[:password]
      reserved_options = [:url, :host, :port, :db, :username, :password]
      uri.query = options.reject { |key, value| reserved_options.include?(key) }.to_query
      uri.to_s
    end

    # Return the connection pool configuration for the named configuration.
    def pool_options(key)
      load_config unless defined?(@pool_config) && @pool_config
      @pool_config[key] || { size: 1, timeout: 2 }
    end

    def get_client(name)
      name = name.to_s
      client = @clients[name]
      unless client
        client_options = options(name)
        if client_options && (client_options[:url].present? || client_options[:host].present?)
          @mutex.synchronize do
            # Doublecheck if the client exists inside the mutex in case of race condition
            client = @clients[name]
            if client.nil?
              client = open_connection(name, client_options)
              @clients[name] = client if client
            end
          end
        end
      end
      client
    end

    def open_connection(name, options)
      if options[:url].blank? && options[:host].blank?
        nil
      else
        ConnectionPool.new(pool_options(name)) { Redis.new(options) }
      end
    end

    def close_connection(connection)
      connection.shutdown { |conn| conn.quit }
    end

  end

end
