# frozen_string_literal: true

# This module hooks into the methods that begin database transactions
# and reports stack traces on any transactions kept open for too long and throws any deadlock exception.
# Once identified, spots in the code that catch dead lock exceptions and pass to rollbar.

module LogDeadLockTransactions

  def begin_transaction(*args)
    rescue_deadlock_transaction do
      super
    end
  end

  def commit_transaction(*args)
    rescue_deadlock_transaction do
      super
    end
  end

  def rollback_transaction(*args)
    rescue_deadlock_transaction do
      super
    end
  end

  def current_transaction(*args)
    rescue_deadlock_transaction do
      super
    end
  end

  def within_new_transaction(*args)
    rescue_deadlock_transaction do
      super
    end
  end

  def open_transactions(*args)
    rescue_deadlock_transaction do
      super
    end
  end

  def after_failure_actions(*args)
    rescue_deadlock_transaction do
      super
    end
  end

  def rescue_deadlock_transaction
    yield if block_given?
  rescue ActiveRecord::Deadlocked => e
    error_message = "Deadlock Detected. #{e.backtrace.join("\n")}"
    Rollbar.error error_message
    Rails.logger.error error_message
    raise e
  end

end

unless ActiveRecord::ConnectionAdapters::TransactionManager.include?(LogDeadLockTransactions)
  ActiveRecord::ConnectionAdapters::TransactionManager.prepend(LogDeadLockTransactions)
end
