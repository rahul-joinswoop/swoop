# frozen_string_literal: true

class Rest

  def self.request(method, url, http_headers, timeout)
    request = RestClient::Request.new(
      method: method,
      url: url,
      headers: http_headers,
      timeout: timeout
    )
    request.execute
  rescue => e
    if e.try(:response).present?
      error_code = e.response.code
      error_body = JSON.parse(e.response.body, object_class: OpenStruct).message

      Rails.logger.debug("REST #{method} response error: #{error_code} - #{error_body}, params URL: (#{url})")

      e.response
    else
      Rails.logger.debug("REST #{method} response error - #{e.class.name} #{e.message}")

      OpenStruct.new(code: 500, body: "#{e.class.name}: #{e.message}")
    end
  end

end
