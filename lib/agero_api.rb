# frozen_string_literal: true

class AgeroApi

  class ResponseError < StandardError

    def initialize(msg)
      super(msg)
    end

  end

  SERVICE_URL = Configuration.agero_farmers_api.url
  SERVICE_TIMEOUT = Configuration.agero_farmers_api.timeout
  ACCESS_KEY_ID = Configuration.agero_farmers_api.access_key_id
  SECRET_ACCESS_KEY = Configuration.agero_farmers_api.secret_access_key
  SERVICE_TO_SIGN = Configuration.agero_farmers_api.service_to_sign
  REGION_TO_SIGN = Configuration.agero_farmers_api.region_to_sign
  PROXY_URL = Configuration.agero_farmers_api.proxy_url

  class << self

    # resource: will be a static constant on an implementation class, e.g.
    #           PCC::PolicyLookup::Farmers::AgeroApiRequest::API_RESOURCE
    #
    # json: a json string ready to be used. e.g.
    #       JSON.generate({ "policyNumber" => "123123123" })
    def post(resource:, json:)
      resource_with_slash = resource
      resource_with_slash = "/#{resource}" unless resource.starts_with?("/")

      url = "#{SERVICE_URL}#{resource_with_slash}"
      headers = headers_for(:post, url: url, json: json)

      if ENV['AGERO_PCC_PROXY_URL_ENABLED']
        request = RestClient::Request.new(
          method: :post,
          url: url,
          payload: json,
          headers: headers,
          timeout: SERVICE_TIMEOUT,
          proxy: PROXY_URL
        )
      else
        request = RestClient::Request.new(
          method: :post,
          url: url,
          payload: json,
          headers: headers,
          timeout: SERVICE_TIMEOUT
        )
      end

      Rails.logger.debug "AgeroAPI::POST request inspect: #{request.inspect}, payload: #{request.payload&.to_s}"

      request.execute

    rescue => e
      if e.try(:response).present?
        error_code = e.response.code
        error_body = JSON.parse(e.response.body, object_class: OpenStruct).message

        Rollbar.error("AgeroAPI.post - response error: #{error_code} - #{error_body}, params RESOURCE: (#{resource}), JSON: #{json} ")

        e.response
      else
        Rollbar.error("AgeroAPI.post error - #{e.class.name} #{e.message}")

        OpenStruct.new(code: 500, body: "#{e.class.name}: #{e.message}")
      end
    end

    # resource: will be a static constant on an implementation class, e.g.
    #           PCC::PolicyDetails::Farmers::AgeroApiRequest::API_RESOURCE_TEMPLATE
    #
    # query_string_hash: a hash containing the querystring to be appended to the url, e.g.
    #                    {:loss_date=>"2018-03-21T13:06:43-03:00", :policy_state_code=>96, :coverage_system_source=>"APPSCE"}
    def get(resource:, query_string_hash:)
      resource_with_slash = resource
      resource_with_slash = "/#{resource}" unless resource.starts_with?("/")

      url_with_query_string = "#{SERVICE_URL}#{resource_with_slash}?" + query_string_hash.to_query

      headers = headers_for(:get, url: url_with_query_string)

      Rails.logger.debug "AgeroApi.get about to call AgeroAPI endpoint with url #{url_with_query_string}"

      request = RestClient::Request.new(
        method: :get,
        url: url_with_query_string,
        headers: headers,
        timeout: SERVICE_TIMEOUT,
        proxy: PROXY_URL
      )

      Rails.logger.debug "AgeroAPI::GET request inspect: #{request.inspect}, payload: #{request.payload&.to_s}"

      request.execute

    rescue => e
      if e.try(:response).present?
        error_code = e.response.code
        error_body = JSON.parse(e.response.body, object_class: OpenStruct).message

        Rollbar.error("AgeroApi.get - response error: #{error_code} - #{error_body}, params URL: (#{url_with_query_string})")

        e.response
      else
        Rollbar.error("AgeroApi.get error - #{e.class.name} #{e.message}")

        OpenStruct.new(code: 500, body: "#{e.class.name}: #{e.message}")
      end
    end

    private

    def headers_for(http_method, url:, json: nil)
      aws_signature = generate_aws_signature(http_method: http_method, url: url, json: json)

      {
        "Authorization" => aws_signature.headers["authorization"],
        "host" => aws_signature.headers["host"],
        "x-amz-date" => aws_signature.headers["x-amz-date"],
        "x-amz-content-sha256" => aws_signature.headers["x-amz-content-sha256"],
        "Content-Type" => "application/json; charset=utf-8",
      }
    end

    def generate_aws_signature(http_method:, url:, json: nil)
      raise ArgumentError, "AgeroAPI.generate_aws_signature http_method and url must be set" if
        [http_method, url].any? { |param| param.nil? }

      signer = Aws::Sigv4::Signer.new(
        service: SERVICE_TO_SIGN,
        region: REGION_TO_SIGN,
        # static credentials
        access_key_id: ACCESS_KEY_ID,
        secret_access_key: SECRET_ACCESS_KEY
      )

      signer.sign_request(
        http_method: http_method.to_s.upcase,
        url: url,
        headers: {
          'Content-Type' => 'application/json; charset=utf-8',
        },
        body: json
      )
    end

  end

end
