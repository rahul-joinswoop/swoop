# frozen_string_literal: true

# #deep_dup doesn't do the preferable thing with activerecord models:
#
# https://stackoverflow.com/questions/10183370/whats-the-difference-between-rubys-dup-and-clone-methods
#
# this makes life harder when we're dealing with mixed Hashes that may contain
# rails models (ie, in graphql params). this works around the problem by adding
# a new #deeper_dup method that's identical to #deep_dup _except_ that it calls
# #clone on any activerecord models. i could have patched #deep_dup but who knows
# what other code depends on this behavior...

require "active_support/core_ext/object/deep_dup"

module DeeperDup

  module Object

    def deeper_dup
      duplicable? ? dup : self
    end

  end

  module ActiveRecord

    def deeper_dup
      clone
    end

  end

  module Array

    def deeper_dup
      map(&:deeper_dup)
    end

  end

  module Hash

    def deeper_dup
      hash = dup
      each_pair do |key, value|
        if key.frozen? && ::String === key
          hash[key] = value.deeper_dup
        else
          hash.delete(key)
          hash[key.deeper_dup] = value.deeper_dup
        end
      end
      hash
    end

  end

  def self.init!
    ::Object.include DeeperDup::Object
    ::Array.include DeeperDup::Array
    ::Hash.include DeeperDup::Hash

    ActiveSupport.on_load(:active_record) do
      send :include, DeeperDup::ActiveRecord
    end
  end

end
