# frozen_string_literal: true

class Numeric

  def decimals
    value = self
    num = 0
    while value != value.to_i
      num += 1
      value *= 10
    end
    num
  end

end
