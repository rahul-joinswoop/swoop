# frozen_string_literal: true

# Populate the makes and models data for the "Create Job" tool
class NHTSA::MakesAndModels

  def self.persist_new_data
    makes_and_models = new(persist: true, verbose: true)
    makes_and_models.update!
  end

  def initialize(options = {})
    @client = ::NHTSA::Client.new
    @persist_data = options[:persist] ? true : false
    @verbose = options[:verbose] ? true : false
    @years = get_years
    @model_records_created, @model_year_records_updated, @model_records_skipped = 0, 0, 0
    output "Dry Run! No data will persist. Pass `options[:persist]` to toggle persisting." unless persist_data
  end

  def update!
    output "Dry Run, no persisting!" unless persist_data

    get_makes_from_db.each do |make|
      make_name = make.name&.strip
      output "Swoop make: '#{make_name}'..."

      years.each do |year|
        models = get_models_from_external_api(make_name, year)
        next unless models
        output "Found #{models.count} models for make #{make_name} and year #{year}..."

        models.each do |model|
          model_name = model['Model_Name']&.strip
          output "Looking up model name: #{model_name.inspect}..."
          next unless model_name
          swoop_model = lookup_db_model(model_name, make.id)

          if swoop_model
            output "Found existing Model for #{model_name}, checking years..."
            if swoop_model.years.include? year
              output "Found existing Model #{model_name} for year #{year}, skipping..."
              @model_records_skipped += 1
            else
              output "No existing year #{year} for Model #{model_name}, adding..."
              persist_model_year(swoop_model, year)
              @model_year_records_updated += 1
            end
          else
            output "No existing Model, creating #{model_name} for year #{year}..."
            persist_model(model_name, make, year)
            @model_records_created += 1
          end
        end
      end
    end

    print_report
  end

  private

  attr_reader :client, :persist_data, :verbose, :years
  attr_reader :model_records_created, :model_records_skipped, :model_year_records_updated

  def get_makes_from_db
    @get_makes_from_db ||= VehicleMake.where({ original: true })
  end

  def get_models_from_external_api(make_name, year)
    models = client.get_models_for_make_and_year(make_name, year)
    audit_result_count(models)
    models['Results']
  rescue HTTP::TimeoutError => e
    Rails.logger.warn "TimeoutError: unable to reach NHTSA API: #{e}"
    nil
  end

  def lookup_db_model(model_name, make_id)
    VehicleModel.where('lower(name) = ? AND vehicle_make_id = ?', model_name.downcase, make_id).first
  end

  def audit_result_count(result)
    output "count mismatch! #{result['Count']} != #{result['Results'].count}" if result['Count'] != result['Results'].count
  end

  def persist_model(model_name, make, year)
    return unless persist_data
    new_model = VehicleModel.find_or_create_by!(name: model_name, vehicle_make: make)
    persist_model_year(new_model, year)
    new_model
  end

  def persist_model_year(model, year)
    return unless persist_data
    model.years << year
    model.years.sort!.uniq!
    model.save!
  end

  def get_years
    # rolling window of years, ex. [2016, 2017, 2018, 2019, 2020]
    (0..4).map { |n| (Time.now.year + 1) - n }.sort
  end

  def print_report
    output "* Total MAKE records searched: #{get_makes_from_db.count}"
    output "* Total new MODEL records created: #{model_records_created}"
    output "* Total existing MODEL records skipped: #{model_records_skipped}"
    output "* Total existing MODEL year records updated: #{model_year_records_updated}"
  end

  def output(message)
    return unless verbose
    puts message
  end

end
