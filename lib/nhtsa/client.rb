# frozen_string_literal: true

# Client for NHTSA (National Highway Trafic Saftey Administration)
class NHTSA::Client

  include HTTPClient
  BASE_URI = ENV.fetch('NHTSA_API_URL', 'https://vpic.nhtsa.dot.gov/api/vehicles/')

  class Error < StandardError; end

  def initialize
    @nhtsa_url = BASE_URI
  end

  # There are 169 pages with 100 records per page
  def get_all_manufacturers
    manufacturers = { "Count": 0, "Pages": 0, "Message": '', "SearchCriteria": nil, "Results": [] }.stringify_keys
    record_count = 100
    current_record_count = record_count
    page = 1

    while current_record_count == record_count
      response = get_request('GetAllManufacturers', params: { format: 'json', page: page }, timeout: 30)
      puts "RESPONSE: #{response}"
      next if response[:timeout]

      current_record_count = response['Count']
      manufacturers['Count'] += current_record_count
      manufacturers['Pages'] = page
      manufacturers['Results'].concat response['Results']

      page += 1
      return manufacturers if response['Count'] < record_count
    end
  end

  def get_manufacturer_makes_by(manufacturer, year)
    all_makes_request = URI.encode("GetMakesForManufacturerAndYear/#{manufacturer}")
    get_request(all_makes_request, params: { year: year, format: 'json' })
  end

  def get_all_makes
    get_request('getallmakes')
  end

  def get_models_for_make(make)
    models_for_make_request = URI.encode("getmodelsformake/#{make.downcase}")
    get_request(models_for_make_request)
  end

  def get_models_for_make_and_year(make, year)
    models_for_make_and_year_request = URI.encode("GetModelsForMakeYear/make/#{make}/modelyear/#{year}")
    get_request(models_for_make_and_year_request)
  end

  private

  attr_reader :nhtsa_url

  DEFAULT_TIMEOUT = Integer(ENV.fetch("NHTSA_API_TIMEOUT", "15"))

  def get_request(request_path, params: { format: 'json' }, timeout: DEFAULT_TIMEOUT)
    request_url = "#{nhtsa_url}#{request_path}"
    response = http_client.timeout(timeout).get(request_url, params: params)

    unless ok?(response)
      raise Error, "NHTSA API Request was not successful: #{response.status}"
    end
    Oj.load(response.to_s)
  end

  def ok?(response)
    response.status.between?(200, 309)
  end

end
