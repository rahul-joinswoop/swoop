# frozen_string_literal: true

# various shared behaviors among http.rb clients

module HTTPClient

  DEFAULT_HEADERS = {
    "Accept" => "application/json",
    "Content-Type" => "application/json; charset=utf-8",
    "Accept-Encoding" => "gzip",
  }.freeze

  DEFAULT_TIMEOUT = 10.0

  def http_client
    HTTP.use(:auto_inflate)
      .timeout(DEFAULT_TIMEOUT)
      .headers(DEFAULT_HEADERS)
  end

  def ok?(response)
    response.status.between?(200, 299)
  end

end
