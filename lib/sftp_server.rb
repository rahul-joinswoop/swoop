# frozen_string_literal: true

class SftpServer

  attr_reader :host, :user, :root, :port

  def initialize(uri, ssh_key: nil, password: nil)
    uri = URI.parse(uri) unless uri.is_a?(URI)
    raise ArgumentError, "URI must be in the form sftp://user@host/path" unless uri.scheme == "sftp" && uri.host && uri.user
    @host = uri.host
    @user = uri.user
    @port = uri.port
    path = (uri.path || "/").chomp("/")
    path = path[1, path.length] if path.start_with?("/")
    @root = (path.blank? ? "." : path)

    @password = (password || uri.password)
    @ssh_key = ssh_key
    if ssh_key.blank? && @password.blank?
      raise ArgumentError, "One of ssh_key or password option is required"
    end
  end

  def upload(stream, path)
    connect_to_server do |sftp|
      sftp.upload!(stream, remote_path(path))
    end
  end

  def connect_to_server
    connection_options = {}
    connection_options[:port] = Integer(port) if port.present?
    if @ssh_key.present?
      connection_options[:key_data] = Array(@ssh_key).map { |v| (v.end_with?("\n") ? v : "#{v}\n") }
      connection_options[:keys] = []
      connection_options[:keys_only] = true
    elsif @password.present?
      connection_options[:password] = @password
    end
    Net::SFTP.start(host, user, connection_options) do |sftp|
      yield(sftp)
    end
  end

  private

  # Prepend the default path specified in the root to a relative path.
  def remote_path(relative_path)
    if root.present? && !relative_path.start_with?("/")
      "#{root}/#{relative_path}"
    else
      relative_path
    end
  end

end
