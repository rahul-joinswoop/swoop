# frozen_string_literal: true

# rake job_flags
task job_flags: :environment do |t, args|
  puts "starting up..."

  Job.all.where("created_at > '2016-02-13 00:00:00'").order(:created_at).each do |job|
    p "#{job.id}, #{job.created_at}, #{job.partner_live}, #{job.partner_open}"
    if job.partner_live.nil?
      p 'partner live nil'
      if job.rescue_company
        if job.rescue_company.live
          job.partner_live = true
        else
          job.partner_live = false
        end
      end
    end
    if job.partner_open.nil?
      p 'partner open nil'
      if job.site
        if job.site.open_for_business?
          job.partner_open = true
        else
          job.partner_open = false
        end
      end
    end
    if job.fleet_manual.nil?
      if job.partner_live && job.partner_open
        job.fleet_manual = false
      else
        job.fleet_manual = true
      end
    end
    job.save!
  end

  Job.all.where("created_at > '2016-02-13 00:00:00'").order(:created_at).each do |job|
    if !job.invoice
      job.schedule_create_invoice_estimate
    end
  end

  Job.all.where("created_at > '2016-02-13 00:00:00'").order(:created_at).each do |job|
    if job.rescue_company && !job.site
      job.site = job.rescue_company.hq
      job.save!
    end
    if job.invoice && (job.invoice.net_amount == 0)
      job.schedule_create_invoice_estimate
    end
  end

  [
    2544,
    2543,
    2542,
    2541,
    2540,
    2539,
    2538,
    2537,
    2535,
    2533,
    2532,
    2527,
    2525,
    2524,
    2521,
  ].each do |job_id|
    job = Job.find(job_id)
    job.schedule_create_invoice_estimate
  end
end
