# frozen_string_literal: true

require 'street_address'

namespace :one_time do
  desc "Migrate all current street data into `street_number` and `street_address`"
  task migrate_street_data: :environment do
    Location.where(street_number: nil, street_name: nil).where.not(address: nil).find_each do |location|
      addr = location.address.gsub(', USA', '')
      sa = StreetAddress::US.parse(addr)

      if sa
        location.street_name = "#{sa.prefix} #{sa.street} #{sa.street_type}".strip
        location.street_number = sa.number
        location.save!
      end
    end
  end
end
