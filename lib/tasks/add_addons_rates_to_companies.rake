# frozen_string_literal: true

# rake add_sites_to_companies_and_rp
task add_addons_rates_to_companies: :environment do |t, args|
  RescueCompany.all.each do |company|
    p "working with #{company.name}"
    Rate.select(:account_id).where(company: company).where.not(account_id: nil).group(:account_id).each do |rate|
      p "Found account #{rate.account.try(:name)}"
      ServiceCode.where(addon: true).where.not(variable: true).each do |addon|
        p "service: #{addon.name}"
        if !Rate.find_by(company: company, account: nil, service_code: addon)
          p "creating nil account rate"
          Rate.create!(type: "FlatRate", company: company, account: nil, service_code: addon, flat: 0)
        end
        if !Rate.find_by(company: company, account: rate.account, service_code: addon)
          p "creating rate"
          Rate.create!(type: "FlatRate", company: company, account: rate.account, service_code: addon, flat: 0)
        end
      end
    end
  end
end
