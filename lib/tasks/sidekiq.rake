# frozen_string_literal: true

module SidekiqQueues

  require "terminal-table"
  class << self

    def print_table
      rows = Sidekiq::Queue.all.map do |q|
        [q.name, q.count, q.paused? ? "paused" : "running"]
      end
      table = Terminal::Table.new headings: %w(queue count status), rows: rows
      puts table
    end

  end

end

begin
  # make sure we have a valid redis connection, error is handled at the bottom
  Redis.new.ping

  desc "Shows status of all queues"
  task sidekiq: :environment do
    SidekiqQueues.print_table
  end

  namespace :sidekiq do
    desc "Pause all queues"
    task pause: :environment do
      Sidekiq::Queue.all.map(&:pause!)
      SidekiqQueues.print_table
    end

    desc "Run all queues"
    task start: :environment do
      Sidekiq::Queue.all.map(&:unpause!)
      SidekiqQueues.print_table
    end

    Sidekiq::Queue.all.each do |q|
      namespace :pause do
        desc "Pause the #{q.name} queue"
        task q.name => :environment do
          Sidekiq::Queue.new(q.name).pause!
          SidekiqQueues.print_table
        end
      end

      namespace :start do
        desc "Run the #{q.name} queue"
        task q.name => :environment do
          Sidekiq::Queue.new(q.name).unpause!
          SidekiqQueues.print_table
        end
      end
    end
  end

rescue Redis::CannotConnectError
  # noop - no redis server available, just don't make these tasks available but
  # don't explode (this is a workaround to allow us to run a single rake task in
  # ci without requiring redis to be running)
end
