# frozen_string_literal: true

# rake jobtype
task jobtype: :environment do |t, args|
  puts "starting up..."
  ActiveRecord::Base.connection.execute("update jobs set type='FleetManagedJob' from companies as c where  jobs.fleet_company_id=c.id and c.type='FleetCompany' and c.in_house is null")
  ActiveRecord::Base.connection.execute("update jobs set type='FleetInHouseJob' from companies as c where  jobs.fleet_company_id=c.id and c.type='FleetCompany' and c.in_house is true")
  ActiveRecord::Base.connection.execute("update jobs set type='RescueJob' from companies as c where  jobs.fleet_company_id=c.id and c.type='RescueCompany'")
end
