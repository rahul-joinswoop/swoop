# frozen_string_literal: true

namespace :state_json_file_to_polygon_creator do
  desc <<~HEREDOC
      Creates polygons for each zipcode of a specific state geojson data file.

      This rake task expects one state at a time (out of 51 US states) to be passed.

      Example:
        rake state_json_file_to_polygon_creator:create_polygons state_file='nh_new_hampshire_zip_codes_geo.min.json'

  HEREDOC

  task create_polygons: :environment do
    puts "Starting to create polygons."

    if ENV['state_file'].present?
      puts "Running polygon creation for zipcodes for '#{ENV['state_file']}'."
      ZipCodes::StateJsonFileToPolygonCreator.new.call(ENV['state_file'])
      puts "Done with creating territories for the provided state file"
    else
      puts "state_file is required to initiate polygon creation."
    end
  end
end
