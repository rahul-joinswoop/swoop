# frozen_string_literal: true

# ruby bin/scripts/redis_cleanup.rb <REDIS_URL>

require "redis"

namespace :redis do
  desc "Clean out all websocket rescue company data that is older than 1d"
  task :cleanup, [:url] => :environment do |t, args|
    Rails.logger.info "REDIS RAKE t:#{t},args:#{args}"

    url = args[:url] || ENV['REDIS_URL']

    cleanup = External::Redis::Cleanup.new(url)
    cleanup.call
  end
end
