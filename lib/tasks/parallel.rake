# frozen_string_literal: true

if Rake::Task.task_defined?("parallel:prepare")
  Rake::Task["parallel:prepare"].clear
end

desc "Run the rspec unit tests in parallel"
namespace :parallel do
  task :unit do
    Rake::Task['parallel:spec'].invoke('spec\/(?!features)')
  end

  task :prepare do
    processes = ENV["PARALLEL_TEST_PROCESSORS"].to_i
    processes = 1 if processes <= 0
    processes.times do |num|
      suffix = (num == 0 ? "" : (num + 1).to_s)
      fork do
        system("TEST_ENV_NUMBER=#{suffix} RAILS_ENV=test bin/rails db:test:prepare elasticsearch:test:create")
      end
    end
    Process.wait
  end
end
