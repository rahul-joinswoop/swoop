# frozen_string_literal: true

# rake add_sites_to_companies_and_rp
task add_sites_to_companies_and_rp: :environment do |t, args|
  RescueCompany.all.each do |company|
    if company.sites.length == 0
      p "Creating Site"
      PartnerSite.create!(company: company, location: company.location)
    end
  end
  RescueProvider.all.each do |rp|
    if !rp.site
      p "Adding site"
      rp.site = rp.provider.hq
      rp.save!
    else
      p "site found for this rp"
    end
  end
end
