# frozen_string_literal: true

# Dump the schema.rb file in addition to the structure.sql file since it works better as documentation.
Rake::Task["db:structure:dump"].enhance(["db:schema:dump"])

namespace :db do
  desc 'Make migration with output'
  task(migrate_with_sql: :environment) do
    ActiveRecord::Base.logger = Logger.new(STDOUT)
    Rake::Task['db:migrate'].invoke
  end

  namespace :reports do
    desc "Load reports from config/reports.yml"
    task load: :environment do
      Report.load_reports
    end
  end

  # Replaced with bin/bootstrap due to conflict with seed_migrations attempting to run before rake task and causing dbconnection error
  #  desc('Bootstrap entire database: Create, Migrate, Seed')
  #  task(:bootstrap => :environment) do
  #    Rake::Task['db:create'].invoke
  #    Rake::Task['db:migrate'].invoke
  #    Rake::Task['db:seed_reports'].invoke
  #    Rake::Task['db:seed'].invoke
  #    Rake::Task['db:seed_base'].invoke
  #    Rake::Task['db:seed_prod'].invoke
  #  end

  task(seed_dev: :environment) do
    load('db/seeds_dev.rb')
  end

  task(seed_prod: :environment) do
    load('db/seeds_prod.rb')
  end

  task(seed: :environment) do
    Rake::Task["db:reports:load"].invoke
    if Rails.env.development?
      Rake::Task['db:seed_dev'].invoke
    end
    Rake::Task['db:seed_prod'].invoke
  end
end
