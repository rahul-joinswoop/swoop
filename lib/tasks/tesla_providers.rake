# frozen_string_literal: true

# rake tesla_providers[db/tesla_providers.csv]

# TODO
# make sure flt has right name
# limit provider/recommended and provider/?live=true to live0
# affects: accounts,rescue_providers,sites,users.companies
# derive rate_type
#

# Status,Provider,Dispatch phone,Dispatch Email,Fax,Address: Street,City,State/Province,Zip Code,Country,Manager/Owner,Manager/Owner Phone,Manager/Owner Email,Additional company details,,Special Dispatch Instructions,Dispatch Email 2,Dispatch Email 3,Vendor ID,Manager/Owner Email 2,Manager/Owner Email 3 ,PO Warranty,PO Non-Warranty,PO Service Center Transports,Available Services,Hours of Operation,Billing Address (if unique): Street,Business Address: City,Billing Address: State/Province,Billing Address: Zip Code,DOT Number,Total Locations,Normal Area of Coverage (Zip codes),Average response time,Flatbed trucks,Wheel lifts,Drivers Trained for Tesla,Rate type,Hourly rate,Hook up rate (1),Hook up rate (2),Enroute rate ($/mi),Free en route miles,Loaded rate ($/mi),Free loaded miles,"Customer rate, loaded ($/mile)",Fuel surcharge ,Dolly rate ($),Service call base rate ($),Service Call mileage rate ($/mile),Storage rate ($/day),Extra labor rate ($/hour),GOA/Cancellation Fee,Recovery/Winching ,Additional Rate details ,,,,,

# Active,
# 1st Class Transport,
# 954-782-1869,
# 1stclass4362@gmail.com,
# ,
# 2421 Northeast 5th Ave,
# Pompano Beach,
# FL,
# 33064,
# USA,
# Jeff Gaines,
# 0,
# 1stclass4362@gmail.com,
# "authorized provider for audi, jag. Done repairs for Roadsters"

# ,
# ,
# ,
# ,
# ,
# 112798,
# ,
# ,
# 4900005051,
# 4900005050,
# ,
# ,
# 24/7,
# ,
# ,
# ,
# ,
# ,
# 1,
# Unlimited,
# < 30 - 45,
# 3,
# 0,
# ,
# Mileage,
# ,
# 75,
# ,
# 2,
# 10,
# 3,
# 5,
# ,
# ,
# no dolly,
# "40,
# 2/mile enroute after 10",
# ,
# "20/day,
# 40/day inside",
# ,
# 20 after 10 minutes after dispatch,
# "50/first half hour,
# 25/quarter hour after",
# ,
# ,
# ,
# ,
# ,

# Manager Notes

require 'csv'

task :tesla_providers, [:file_name] => [:environment] do |t, args|
  puts "Reading file #{args[:file_name]}"

  tesla = Company.find_by(name: Company::TESLA)
  if !tesla
    p "no tesla"
    return
  end
  lisa = User.find_by(email: "lcahillane@teslamotors.com")
  if !lisa
    p "no lisa"
    return
  end
  tow = ServiceCode.find_by(name: "Tow")
  if !tow
    p "no tow"
    return
  end

  #  hours_p2p=RateType.find_by(name:'HoursP2P')
  #  hours_complete=RateType.find_by(name:'HoursComplete')
  #  miles_towed=RateType.find_by(name:'MilesTowed')
  #  miles_towed_er=RateType.find_by(name:'MilesTowedER')
  #  miles_p2p=RateType.find_by(name:'MilesP2P')

  #  hookup_pt=Hookup.find_by
  #  miles_towed_pt=MilesTowed
  #  miles_towed_free_pt=MilesTowedFree
  #  miles_en_route_pt=MilesEnRoute
  #  miles_en_route_free=MilesEnRouteFree
  #  miles_p2p=MilesP2P
  #  flat=Flat

  #  if not (hours_p2p and hours_complete and miles_towed and miles_towed_er and miles_p2p)
  #    rt=[hours_p2p,hours_complete,miles_towed,miles_towed_er,miles_p2p]
  #    p "unable to find all RateTypes #{rt}"
  #    return
  #  end

  dummy_phone = 14155550000

  CSV.foreach(args[:file_name], headers: true) do |row|
    # p row
    phone = Phonelib.parse(row["Dispatch phone"]).e164

    if ENV['SWOOP_ENV'] == 'staging'
      dummy_phone += 1
      phone = Phonelib.parse("#{dummy_phone}").e164
    end

    site = Site.find_by(import_num: row['import_id'])

    if site

      if !site.company
        p "Found site, not linked to company #{company}, #{site}"
        return
      end
      company = site.company
      location = site.location
    # user=company.primary_contact
    else

      company = Company.find_by(name: row['Provider'])
      if !company
        company = Company.find_by(phone: phone)
      end
      if row['Country'] == "USA"
        country = "US"
      elsif row['Country'] == "CANADA"
        country = "CA"
      end
      location = Location.create!(address: row['Full Address'], street: row['Address: Street'], city: row['City'], state: row['State/Province'], zip: row['Zip Code'], country: country, lat: row['Latitude'], lng: row['Longitude'])
      if !company
        if ENV['SWOOP_ENV'] == 'staging'
          dispatch_email = "dispatch+.#{dummy_phone}@joinswoop.com"
        else
          dispatch_email = row['Dispatch Email']
        end
        company = RescueCompany.new(name: row['Provider'], phone: phone, dispatch_email: dispatch_email, fax: row['Fax'], location: location, invited_by_user: lisa, invited_by_company: tesla)
        company.defaults
        company.save!
      end
      manager = nil
      if row['Manager Email']
        if ENV['SWOOP_ENV'] == 'staging'
          manager_email = "manager+.#{dummy_phone}@joinswoop.com"
        else
          manager_email = row['Manager Email'].downcase
        end
        p "manager_email:#{manager_email}"
        manager = User.find_by(email: manager_email)
        if !manager
          manager = User.create!(full_name: row['Manager'], phone: Phonelib.parse(row["Manager/OwnerPhone"]).e164, company: company, email: manager_email, password: row['Manager Password'], invited_by_user: lisa, invited_by_company: tesla)
        end
      end

      site = Site.create!(company: company, phone: phone, location: location, type: "PartnerSite", name: row['Provider'], manager: manager, import_num: row['import_id'])

    end
    rp = RescueProvider.find_by(company: tesla, provider: company, site: site)
    if !rp
      vid = row['Vendor ID']
      if vid == 0
        vid = nil
      end
      RescueProvider.create!(site: site, company: tesla, provider: company, location: location, primary_contact: row['Manager'], primary_phone: Phonelib.parse(row["Manager/OwnerPhone"]).e164, status: row['Status'], notes: "#{row['Additional company details']} #{row['Manager Notes']}", vendor_code: vid)
      RescueProvider.create!(site: site, company: Company.swoop, provider: company, location: location)
    end
    account = Account.find_by(company: company, client_company: tesla)
    if !account
      account = Account.create!(name: "Tesla", company: company, client_company: tesla)
    end
    if row['Rate Type'] && (row['Rate Type'].strip != "")
      account_rates = Rate.where(company: company, account: account, site: site)
      if account_rates.length > 0
        p "skipping existing rates"
      else

        type = nil
        rateargs = {
          company: company, account: account, site: site, service_code: tow,
          fleet_company: account.fleet_company,
          hourly: row['Hourly Rate (P2P)'],
          miles_towed: row['$ / Towed Miles'],
          miles_towed_free: row['Free Towed Miles'],
          miles_enroute: row['$ / En Route Miles'],
          miles_enroute_free: row['Free En Route Miles'],
          special_dolly: row['Special Equipment (ex. Dolly)'],
          gone: row['GOA Fee'],
          storage_daily: row['Storage Rate / Day'],
          import_name: "tesla_providers",
          import_id: row['import_id'],
        }

        miles_towed = row['$ / Towed Miles']
        if !miles_towed
          miles_towed = 0
        end

        miles_towed_free = row['Free Towed Miles']
        if !miles_towed_free
          miles_towed_free = 0
        end

        miles_enroute_free = row['Free En Route Miles']
        if !miles_enroute_free
          miles_enroute_free = 0
        end

        flat = row['Hookup Rate']
        if !flat
          flat = 0
        end

        if row['Hourly Rate (P2P)']
          type = HoursP2PRate
          rateargs[:hookup] = row['Hookup Rate']
        elsif row['$ / Towed Miles'] && row['$ / En Route Miles']
          type = MilesEnRouteRate
          rateargs[:hookup] = row['Hookup Rate']
          rateargs[:miles_towed_free] = miles_towed_free
          rateargs[:miles_enroute_free] = miles_enroute_free
        elsif row['$ / Towed Miles']
          type = MilesTowedRate
          rateargs[:hookup] = row['Hookup Rate']
          rateargs[:miles_towed_free] = miles_towed_free
        elsif row['Rate Type'] == "Flat"
          type = FlatRate
          rateargs[:flat] = flat
        elsif row['Rate Type'] == "Hourly"
          type = HoursP2PRate
          rateargs[:hookup] = row['Hookup Rate']
          rateargs[:hourly] = 0
        elsif row['Rate Type'] == "Mileage"
          type = MilesTowedRate
          rateargs[:hookup] = row['Hookup Rate']
          rateargs[:miles_towed] = miles_towed
          rateargs[:miles_towed_free] = miles_towed_free
        end

        if !type
          p "type not found for #{row}"
          return
        end

        if row['Hourly Rate (P2P)'] != row['Hourly Rate (Completion)']
          p "hourly rates dont match #{row}"
          return
        end
        begin
          Rate.transaction do
            ServiceCode.all.each do |sc|
              rateargs[:service_code] = sc
              type.create!(rateargs)
            end
          end
        rescue StandardError
          rateargs.delete :company
          rateargs.delete :account
          rateargs.delete :fleet_company
          rateargs.delete :site
          rateargs.delete :service_code

          p "Invalid rate for: #{company.name} #{type} #{rateargs}"
          #          raise Exception.new
        end
      end
    end
  end
end
