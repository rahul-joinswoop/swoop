# frozen_string_literal: true

# identical to the original but we ignore errors when doing review app deploys
namespace :seed do
  Rake::Task["seed:migrate"].clear
  desc "Run new data migrations."
  task migrate: :environment do
    SeedMigration::Migrator.run_migrations(ENV['MIGRATION'])
  rescue StandardError => e
    if ENV.key?('HEROKU_PARENT_APP_NAME')
      puts "Caught seed:migrate error in review app, continuing"
      puts e
    else
      raise e
    end
  end

  desc "Run updates to seeds. This task will rollback any changes made to db/seeds.rb with the assumption that what is in that file is canonical."
  task :update do
    seed_changes = `git status -s db/seeds.rb`.strip
    if seed_changes.present?
      warn "\033[0;31mCannot run seed:update while changes are pending to db/seeds.rb\033[0m"
      exit 1
    end

    seed_migration_changes = `git status -s db/data/`.strip
    if seed_migration_changes.present?
      warn "\033[0;31mCannot run seed:update while changes are pending in db/data/\033[0m"
      exit 1
    end

    Rake::Task['seed:migrate'].invoke

    # Rollback changes to db/seeds.rb; this file can get out of sync in development
    # environments and will not need to be checked in if you're just running an update.
    `git checkout db/seeds.rb`
  end
end
