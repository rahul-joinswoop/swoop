# frozen_string_literal: true

# rake companies_service
task companies_service: :environment do |t, args|
  puts "starting up..."
  RescueCompany.all.each do |company|
    ServiceCode.all.each do |service|
      args = { company: company, service_code: service }
      cs = CompaniesService.find_by(args)
      if !cs
        CompaniesService.create!(args)
      end
    end
  end
end
