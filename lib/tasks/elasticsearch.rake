# frozen_string_literal: true

require 'elasticsearch/rails/tasks/import'

namespace :elasticsearch do
  desc "Create an empty index for CLASS with name INDEX"
  task create_index: :environment do
    klass = ENV['CLASS'].constantize
    index_name = (ENV["INDEX"] || klass.index_name)
    klass.__elasticsearch__.create_index!(index: index_name)
  end

  desc "Delete the index named INDEX; as a safety measure, the index won't be deleted if it is currently aliased."
  task delete_index: :environment do
    client = Elasticsearch::Model.client
    index_name = ENV["INDEX"]
    if index_name.blank?
      raise ArgumentError, "You must supply the index name to delete in the INDEX environment variable"
    end
    aliases = client.indices.get_aliases[index_name]
    if aliases && aliases["aliases"].present?
      raise ArgumentError, "You cannot delete an index that is currently aliased"
    end
    client.indices.delete(index: index_name)
  end

  desc "Create an elasticsearch alias for a model"
  task create_alias: :environment do
    index_name = ENV["INDEX"]
    if index_name.blank?
      raise ArgumentError, "You must supply the index name to alias in the INDEX environment variable"
    end
    klass = ENV['CLASS'].constantize
    client = klass.__elasticsearch__.client
    alias_name = klass.index_name.to_s
    unless index_name.include?(alias_name)
      raise ArgumentError, "The index name #{index_name.inspect} does not include the alias name #{alias_name.inspect}"
    end

    aliases = {}
    client.indices.get_aliases.each do |aliased_index_name, info|
      info["aliases"].keys.each do |name|
        aliases[name] ||= []
        aliases[name] << aliased_index_name
      end
    end

    actions = []
    Array(aliases[alias_name]).each do |remove_index|
      actions << { remove: { index: remove_index, alias: alias_name } }
    end
    actions << { add: { index: index_name, alias: alias_name } }

    client.indices.update_aliases(body: { actions: actions })
  end

  namespace :import do
    task set_default_scope: :environment do
      if ENV['SCOPE'].nil? && ENV['CLASS']
        klass = ENV['CLASS'].constantize
        if klass.respond_to?(:elastic_search_backfill)
          ENV['SCOPE'] = "elastic_search_backfill"
        elsif klass.respond_to?(:elastic_search_preload)
          ENV['SCOPE'] = "elastic_search_preload"
        end
      end
    end
  end

  namespace :development do
    # Bootstrap a development environment with brand new indexes.
    task prepare: "elasticsearch:test:prepare"
  end

  namespace :test do
    # these are the models we need in tests
    SWOOP_ES_MODELS = ['Account', 'Company', 'Job', 'RescueProvider', 'User'].freeze

    SWOOP_ES_MODELS.each do |class_name|
      task class_name.underscore.to_sym => :environment do
        ENV['CLASS'] = class_name
        ENV['FORCE'] = 'true'
        Rake::Task["elasticsearch:import:model"].reenable
        Rake::Task["elasticsearch:import:model"].invoke
      end
    end

    task :create do
      SWOOP_ES_MODELS.each do |class_name|
        ENV['FORCE'] = 'true'
        klass = class_name.constantize
        index_name = (ENV["INDEX"] || klass.index_name)
        klass.__elasticsearch__.create_index!(index: index_name)
      end
    end

    desc "import required indexes for test (#{SWOOP_ES_MODELS}) to elasticsearch in parallel"
    task :prepare do
      SWOOP_ES_MODELS.each do |klass|
        Rake::Task["elasticsearch:test:#{klass.underscore}"].invoke
      end
    end

    namespace :parallel do
      desc "create search indexes for testing"
      task :prepare do
        raise "Environment already loaded" if Rake::Task[:environment].already_invoked
        ENV["RAILS_ENV"] = "test"
        Rails.env = "test"
        Rake::Task[:environment].invoke
        processes = ENV["PARALLEL_TEST_PROCESSORS"].to_i
        processes = 1 if processes <= 0
        processes.times do |i|
          suffix = (i == 0 ? "" : (i + 1).to_s)
          SWOOP_ES_MODELS.each do |class_name|
            klass = class_name.constantize
            index_name = "#{klass.index_name}#{suffix}"
            klass.__elasticsearch__.create_index!(index_name: index_name, force: true)
            puts "Created ElasticSerch index #{index_name}"
          end
        end
      end
    end
  end
end

Rake::Task['elasticsearch:import:model'].enhance ['elasticsearch:import:set_default_scope']

Rake::Task['db:test:prepare'].enhance ['elasticsearch:test:create']
