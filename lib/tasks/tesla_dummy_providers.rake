# frozen_string_literal: true

# rake tesla_providers[db/tesla_providers.csv]

# TODO
# make sure flt has right name
# limit provider/recommended and provider/?live=true to live

# Status,Provider,Dispatch phone,Dispatch Email,Fax,Address: Street,City,State/Province,Zip Code,Country,Manager/Owner,Manager/Owner Phone,Manager/Owner Email,Additional company details,,Special Dispatch Instructions,Dispatch Email 2,Dispatch Email 3,Vendor ID,Manager/Owner Email 2,Manager/Owner Email 3 ,PO Warranty,PO Non-Warranty,PO Service Center Transports,Available Services,Hours of Operation,Billing Address (if unique): Street,Business Address: City,Billing Address: State/Province,Billing Address: Zip Code,DOT Number,Total Locations,Normal Area of Coverage (Zip codes),Average response time,Flatbed trucks,Wheel lifts,Drivers Trained for Tesla,Rate type,Hourly rate,Hook up rate (1),Hook up rate (2),Enroute rate ($/mi),Free en route miles,Loaded rate ($/mi),Free loaded miles,"Customer rate, loaded ($/mile)",Fuel surcharge ,Dolly rate ($),Service call base rate ($),Service Call mileage rate ($/mile),Storage rate ($/day),Extra labor rate ($/hour),GOA/Cancellation Fee,Recovery/Winching ,Additional Rate details ,,,,,

# Active,
# 1st Class Transport,
# 954-782-1869,
# 1stclass4362@gmail.com,
# ,
# 2421 Northeast 5th Ave,
# Pompano Beach,
# FL,
# 33064,
# USA,
# Jeff Gaines,
# 0,
# 1stclass4362@gmail.com,
# "authorized provider for audi, jag. Done repairs for Roadsters"

# ,
# ,
# ,
# ,
# ,
# 112798,
# ,
# ,
# 4900005051,
# 4900005050,
# ,
# ,
# 24/7,
# ,
# ,
# ,
# ,
# ,
# 1,
# Unlimited,
# < 30 - 45,
# 3,
# 0,
# ,
# Mileage,
# ,
# 75,
# ,
# 2,
# 10,
# 3,
# 5,
# ,
# ,
# no dolly,
# "40,
# 2/mile enroute after 10",
# ,
# "20/day,
# 40/day inside",
# ,
# 20 after 10 minutes after dispatch,
# "50/first half hour,
# 25/quarter hour after",
# ,
# ,
# ,
# ,
# ,

# Manager Notes

require 'csv'

task :tesla_dummy_providers, [:file_name] => [:environment] do |t, args|
  puts "Reading file #{args[:file_name]}"
  dummy_phone = 14155550000
  (1..100).each do |i|
    p "starting csv read"
    CSV.foreach(args[:file_name], headers: true) do |row|
      tesla = Company.find_by(name: Company::TESLA)
      if !tesla
        p "no tesla"
        return
      end
      lisa = User.find_by(email: "lcahillane@teslamotors.com")
      if !lisa
        p "no lisa"
        return
      end
      dummy_phone += 1

      phone = Phonelib.parse("#{dummy_phone}").e164
      p phone
      if phone

        site = Site.find_by(phone: phone)

        company = Company.find_by(name: row['Provider'])

        if site

          if !company = site.company
            p "Found site, not linked to company #{company}, #{site}"
            return
          end
          company = site.company
          location = site.location
        # user=company.primary_contact
        else
          if row['Country'] == "USA"
            country = "US"
          elsif row['Country'] == "CANADA"
            country = "CA"
          end
          location = Location.create!(address: row['Full Address'], street: row['Address: Street'], city: row['City'], state: row['State/Province'], zip: row['Zip Code'], country: country, lat: row['lat'], lng: row['lng'])
          if !company
            company = RescueCompany.create!(name: row['Provider'], phone: phone, dispatch_email: row['Dispatch Email'], fax: row['Fax'], location: location, invited_by_user: lisa, invited_by_company: tesla)
          end
          site = Site.create!(company: company, phone: phone, location: location)
          # have site and company, create manager
          # user=User.create(name:row['Manager'],Phonelib.parse(row["Manager/OwnerPhone"]).e164, company:company)
        end
        rp = RescueProvider.find_by(site: site, provider: company)
        if !rp
          RescueProvider.create!(name: row['Provider'], site: site, company: tesla, provider: company, location: location, primary_contact: row['Manager'], primary_phone: Phonelib.parse(row["Manager/OwnerPhone"]).e164, status: row['Status'], notes: "#{row['Additional company details']} #{row['Manager Notes']}")
        end

      else
        p "no phone for #{row['Provider']}"
      end
    end
  end
end
