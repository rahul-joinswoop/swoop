# frozen_string_literal: true

namespace :review_app do
  # This task is invoked as a postdeploy script in app.json, which is triggered when a new Heroku review app is created
  # (once the 'release' commands defined in Procfile have been executed)
  #
  # - Updates relevant ENV vars
  # - Updates addons to suffiencent plans for seed data.
  # - Seeds the database
  # - Creates the Elasticsearch indices.
  # - Scales the dynos from 0 to 1 per process.

  def heroku
    heroku_token = ENV["HEROKU_PLATFORM_TOKEN"]
    PlatformAPI.connect_oauth(heroku_token)
  end

  def heroku_app_name
    ENV["HEROKU_APP_NAME"]
  end

  task :sanity_check do
    if heroku_app_name != 'secure-bayou-4357' && ENV['REVIEW_APP_READY'] == 'true' || heroku_app_name == 'swoopmebeta' && ENV['BACKUPS_ENABLED'] == 'true'
      puts "The sanity check passed."
    else
      fail "The sanity check did not pass."
    end
  end

  task setup: :sanity_check do
    `heroku run:detached rake review_app:setup_addons_and_data -a #{heroku_app_name}`
  end

  task setup_addons_and_data: :sanity_check do
    `curl -X POST -H 'Content-type: application/json' --data '{"text":"Seeding data and configuring review app #{heroku_app_name}"}' #{ENV['SLACK_HOOK']}`
    Rake::Task["review_app:update_heroku_app_env_vars"].invoke
    Rake::Task["review_app:update_addons"].invoke
    `heroku run --size=standard-2x rake review_app:setup_db -a #{heroku_app_name}`
    Rake::Task["review_app:setup_elasticsearch"].invoke
    Rake::Task["review_app:scale_up_dynos"].invoke
    `curl -X POST -H 'Content-type: application/json' --data '{"text":"Review app for #{heroku_app_name} has been created! Visit https://#{heroku_app_name}.herokuapp.com, or go to the Heroku Swoop Pipeline to access it."}' #{ENV['SLACK_HOOK']}`
    heroku.config_var.update(heroku_app_name, "REVIEW_APP_SEEDED" => 'true')
  end

  task update_heroku_app_env_vars: :sanity_check do
    heroku.config_var.update(heroku_app_name, "SITE_URL" => "https://#{heroku_app_name}.herokuapp.com")
    heroku.config_var.update(heroku_app_name, "WEBSOCKET_URL" => "https://#{heroku_app_name}.herokuapp.com")
    heroku.config_var.update(heroku_app_name, "ELASTICSEARCH_PROVIDER" => ENV['BONSAI_URL'])
    heroku.config_var.update(heroku_app_name, "ELASTICSEARCH_URL" => ENV['BONSAI_URL'])
    heroku.config_var.update(heroku_app_name, "REDIS_CACHE_URL" => ENV['REDIS_URL'])
  end

  task update_addons: :sanity_check do
    addons = heroku.addon.list_by_app(heroku_app_name)
    addons.each do |addon|
      if addon["name"].include? "bonsai"
        heroku.addon.update(heroku_app_name, addon["id"], { "plan": "bonsai:standard-sm" })
      elsif addon["name"].include? "redis"
        heroku.addon.update(heroku_app_name, addon["id"], { "plan": "heroku-redis:premium-2" })
      elsif addon["name"].include? "postgres"
        heroku.addon.delete(heroku_app_name, addon["id"])
        `heroku pg:wait -a #{heroku_app_name}`
        heroku.addon.create(heroku_app_name, { "plan": "heroku-postgresql:standard-0" })
        `heroku pg:wait -a #{heroku_app_name}`
      end
    end
  end

  task setup_db: :sanity_check do
    `heroku pg:backups:restore swoopmebeta::#{ENV['BETA_BACKUP_TAG']} DATABASE_URL --app #{heroku_app_name} --confirm #{heroku_app_name}`
    Rake::Task["db:migrate"].invoke
    Rake::Task["seed:migrate"].invoke
    Rake::Task["db:reports:load"].invoke
  end

  task setup_elasticsearch: :sanity_check do
    # Elasticsearch url isnt set up in this container so need to spin up another dyno to run the indexing
    # which will have the newly minted env var from the previous setup_env_vars step.
    `heroku run --size=standard-2x rake elasticsearch:test:prepare -a #{heroku_app_name}`
  end

  task scale_up_dynos: :sanity_check do
    `heroku ps:scale web=1 -a #{heroku_app_name}`
    `heroku ps:scale worker=1 -a #{heroku_app_name}`
    `heroku ps:scale sidekiq=1 -a #{heroku_app_name}`
    `heroku ps:scale clock=1 -a #{heroku_app_name}`
  end

  task create_backups_and_tag: :sanity_check do
    `heroku pg:backups:capture --app #{heroku_app_name}`
    backup_tags = `heroku pg:backups --app #{heroku_app_name}`
    latest_backup_tag = backup_tags.scan(/([abc]\d\d\d\d?)/).first.first
    heroku.config_var.update(heroku_app_name, "BETA_BACKUP_TAG" => latest_backup_tag)
  end
end
