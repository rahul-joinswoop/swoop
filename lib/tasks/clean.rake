# frozen_string_literal: true

# rake clean
require 'fileutils'

task clean: :environment do |t, args|
  puts "starting up..."
  FileUtils.rm_rf('node_modules')
end
