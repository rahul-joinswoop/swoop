# frozen_string_literal: true

# rake vmakes
task vmakes: :environment do |t, args|
  puts "starting up..."
  seen = {}
  VehicleMake.all.each do |make|
    if !(seen[make.edmunds_id])
      make.original = true
      make.save!
      seen[make.edmunds_id] = true
    end
  end
end
