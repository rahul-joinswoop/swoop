# frozen_string_literal: true

Rake::Task["default"].clear
task :default do
  if ENV["PARALLEL_TEST_PROCESSORS"].to_i > 1
    Rake::Task['parallel:spec'].invoke
  else
    Rake::Task['spec'].invoke
  end
end
