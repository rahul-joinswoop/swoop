# frozen_string_literal: true

# rake rolling_release
require './lib/swoop/version'

task rolling_release: :environment do |t, args|
  version = Swoop::VERSION
  print "***********************"
  print version
  print "***********************"

  if ActiveRecord::Base.connection.table_exists? 'versions'

    args = { name: 'web', version: version }
    if !Version.find_by(args)
      Rails.logger.info "Adding Rolling Release #{version} to version table and publishing via WS"
      args['delay'] = 0
      Version.create!(args)
    else
      Rails.logger.info "Already at latest version: #{version}"
    end
    sleep 1

  else
    Rails.logger.info "DB hasn't been created yet"
  end
end
