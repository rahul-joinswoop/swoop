# frozen_string_literal: true

# rake companies_service
task company_defaults: :environment do |t, args|
  puts "starting up..."
  RescueCompany.all.each do |rc|
    RescueCompany.transaction do
      rc.defaults
      rc.save!
    end
  end
end
