# frozen_string_literal: true

# foreman run rake issc:connect

require 'issc'

namespace :issc do
  desc "Connect to ISSC to receive dispatch requests, uses the webhook URL defined in ISSC_WEBOOK_URL (#{ENV['ISSC_WEBHOOK_URL']}) environment. Set this up using ngrok!"
  task connect: :environment do |t, args|
    IsscConnect.new.perform
  end

  desc "Disconnect from ISSC to receive dispatch requests, uses the webhook URL defined in ISSC_WEBOOK_URL (#{ENV['ISSC_WEBHOOK_URL']}) environment. Set this up using ngrok!"
  task disconnect: :environment do |t, args|
    IsscDisconnect.new.perform
  end

  desc "Enable ISSC"
  task enable: :environment do |t, args|
    cm = ISSC::ConnectionManager.new
    cm.enable_issc
  end

  desc "Disable ISSC"
  task disable: :environment do |t, args|
    cm = ISSC::ConnectionManager.new
    cm.disable_issc
  end
end
