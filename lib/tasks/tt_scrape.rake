# frozen_string_literal: true

# foreman run rake tt_scrape[<company_id>]

task :tt_scrape, [:company_id] => [:environment] do |t, args|
  company_id = args[:company_id]
  puts "Reading file #{company_id}"
  rescue_company = RescueCompany.find(company_id)
  puts "Scraping Company: #{rescue_company.name}"
  rescue_company.tt_account.scrape
end
