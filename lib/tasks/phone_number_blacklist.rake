# frozen_string_literal: true

namespace :phone_number_blacklist do
  desc "add phone_number to blacklist"
  task :add, [:phone_number] => :environment do |_, args|
    PhoneNumberBlacklist.create! args.to_h
    true
  end

  desc "remove phone_number from blacklist"
  task :remove, [:phone_number] => :environment do |_, args|
    PhoneNumberBlacklist.find_by(args.to_h).destroy!
    true
  end

  desc "list blacklisted phone_numbers"
  task list: :environment do
    puts PhoneNumberBlacklist.all.pluck(:phone_number)
  end
end
