# frozen_string_literal: true

# rake job_sequence
task job_sequence: :environment do |t, args|
  puts "starting up..."
  Company.all.each do |fc|
    Job.where({ fleet_company: fc }).order(id: :asc).each_with_index do |job, index|
      puts "Owner:#{fc.name}:#{job.id} => #{index}"
      job.owner_sequence = index + 1
      job.save!
    end
  end

  RescueCompany.all.each do |rc|
    Job.where({ rescue_company: rc }).order(id: :asc).each_with_index do |job, index|
      puts "Rescue:#{rc.name}:#{job.id} => #{index}"
      job.rescue_sequence = index + 1
      job.save!
    end
  end
end
