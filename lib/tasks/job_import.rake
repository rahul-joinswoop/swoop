# frozen_string_literal: true

# rake job_import[db/job_import.csv]
#

# jid,uid,client,status,name,cell,lat,lng,address,city,state,veh_year,veh_make,veh_model,service,provider_id,hookup_fee,mileage_fee,notes,created,dispatched,completed,changed,partner_id
# 2,3,FlightCar,completed,Grant Long,4159991824,37.79762,-122.428852,2767 Octavia St,San Francisco,CA,2009,Volkswagen ,Jetta,jump,,30,0,"Drop-off Location:323 S Canal StreetSouth San Francisco, CAPossible tow if jump doesn't work (only jump required)Job requested at 7:30pmJob completed at 8:22pm",1430361046,0,1430365149,1430365149,

# "jid":"150"
# "uid":"3"

# "client":"LuxeValet"
# "status":"completed"

# "name":"Julian Kasow"
# "cell":"3126597059"

# "lat":"41.88066"
# "lng":"-87.633798"
# "address":"98 S Wells"
# "city":"Chicago"
# "state":"IL"

# "veh_year":"2013"
# "veh_make":"Porsche"
# veh_model":"Cayenne"

# "service":"tire"

# "provider_id":nil

# "hookup_fee":"NULL"
# "mileage_fee":"NULL"

# "notes":"Drop-off Location:\r\r98 S Wells\r\rChicago, IL\r\r\r\rFlat tire, spare is in trunk\r\r\r\r**provider should arrive in 45mins at 5:45pm\r\rcompleted"

# "created":"1440633295"

# "dispatched":"0"

# "completed":"1440637834"

# "changed":"1440637834"

# "partner_id":nil

require 'csv'

task :job_import, [:file_name] => [:environment] do |t, args|
  puts "Reading file #{args[:file_name]}"
  CSV.foreach(args[:file_name], headers: true) do |row|
    p row["client"]

    service_map = {
      "jump" => "Battery Jump",
      "lockout" => "Lock Out",
      "tow" => "Tow",
      "tire" => ServiceCode::TIRE_CHANGE,
      "fuel" => ServiceCode::FUEL_DELIVERY,
    }
    ServiceCode.find_by(name: service_map[row["service"]])

    phone = row["cell"]
    user = User.find_by(phone: phone)
    if !user
      user = User.new(phone: phone,
                      full_name: row["name"])
      user.fake_creds
      user.save!
    end

    Vehicle.create!(make: row["veh_make"], model: row["veh_model"], year: row["veh_year"])

    # Service Representative
    sr_uid = row["uid"]
    sr = User.find_by(drupal_id: sr_uid)
    if !sr
      sr = User.new(full_name: "Drupal User #{sr_uid}",
                    drupal_id: sr_uid)
      sr.fake_creds
      sr.save!
    end

    Location.create!(lat: row["lat"], long: row["lng"], address: "#{row["address"]}, #{row["city"]}, #{row["state"]}", user: user)
  end
end
