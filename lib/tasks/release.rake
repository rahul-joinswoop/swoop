# frozen_string_literal: true

# rake release
require './lib/swoop/version'

task release: :environment do |t, args|
  Rails.logger.debug t
  Rails.logger.debug args

  version = Swoop::VERSION
  print "***********************"
  print version
  print "***********************"

  if ActiveRecord::Base.connection.table_exists? 'versions'

    args = { name: 'web', version: version }
    if !Version.find_by(args)
      Rails.logger.info "Adding release #{version} to version table and publishing via WS"
      args['delay'] = 0
      Version.create!(args)
    else
      Rails.logger.info "Already at latest version: #{version}"
    end
    sleep 1
  else
    Rails.logger.info "DB hasn't been created yet"
  end
end
