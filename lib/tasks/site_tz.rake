# frozen_string_literal: true

# rake site_tz
task site_tz: :environment do |t, args|
  puts "starting up..."
  Site.all.each do |site|
    if site.location && site.location.lat && site.location.lng
      site.google_timezone
      site.save!
    end
  end
end
