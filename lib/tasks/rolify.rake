# frozen_string_literal: true

# rake rolify
task rolify: :environment do |t, args|
  puts "starting up..."
  User.where({ role: [2, 3, 4] }).each do |user|
    puts "dealing with user #{user} #{user.role}"
    case user.role
    when 2
      user.add_role :root
      puts "added root"
    when 3
      user.add_role :fleet
      puts "added fleet"
    when 4
      user.add_role :rescue
      puts "added rescue"
    end
    user.save!
  end
end
