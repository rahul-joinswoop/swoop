# frozen_string_literal: true

def circle_ci_trigger(branch)
  uri = URI.parse('https://circleci.com/api/v1.1/project/github/joinswoop/system_tests/build')
  https = Net::HTTP.new(uri.host, uri.port)
  https.use_ssl = true
  req = Net::HTTP::Post.new(uri.path.concat("?circle-token=#{ENV["CIRCLE_API_KEY"]}"))
  req['Content-Type'] = 'application/json'
  req.body = { branch: branch }.to_json
  res = https.request(req)
  puts "Post for starting smoke tests finished with code #{res.code}"
rescue StandardError => e
  puts e
end

# trigger on staging only for now.
task :run_smoke_tests do |t, args|
  if ENV['HEROKU_APP_NAME'] == 'swoopmestaging'
    circle_ci_trigger('staging')
  end
end
