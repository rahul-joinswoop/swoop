# frozen_string_literal: true

namespace :slack do
  desc "Create the standard client Slack channel feeds"
  # e.g. foreman run rake slack:client["21st Century Insurance","21stBot","21st_client"]
  task :client, [:company, :bot, :channel] => :environment do |t, args|
    puts "slack::client - company:#{args[:company]} bot:#{args[:bot]} channel:#{args[:channel]}"
    cfg = Slack::Config.new(args[:company], args[:bot], args[:channel])
    cfg.create_client
  end

  desc "Create the Ops Slack channel for a client"
  # e.g. foreman run rake slack:ops["21st Century Insurance","21stBot","21st_ops"]
  task :ops, [:company, :bot, :channel] => :environment do |t, args|
    puts "slack::ops - company:#{args[:company]} bot:#{args[:bot]} channel:#{args[:channel]}"
    cfg = Slack::Config.new(args[:company], args[:bot], args[:channel])
    cfg.create_ops
  end

  desc "Create the reviews channel for a client"
  # e.g. foreman run rake slack:reviews["21st Century Insurance","21stBot","21st_reviews"]
  task :reviews, [:company, :bot, :channel] => :environment do |t, args|
    puts "slack::reviews - company:#{args[:company]} bot:#{args[:bot]} channel:#{args[:channel]}"
    cfg = Slack::Config.new(args[:company], args[:bot], args[:channel])
    cfg.create_reviews
  end
end
