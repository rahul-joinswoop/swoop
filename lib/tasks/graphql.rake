# frozen_string_literal: true

require "graphql/rake_task"
require "graphql/rake_task/validate"

# import default graphql rake task
# TODO - we need to setup our context / filters / etc here!
task = GraphQL::RakeTask.new(
  schema_name: "SwoopSchema",
  dependencies: [:environment],
  directory: File.expand_path("../../../app/assets/javascripts/graphql", __FILE__),
)

# and append our enum task to the regular graphql:schema:json task
namespace :graphql do
  namespace :schema do
    task json: task.dependencies do
      # read in our newly generated schema
      json_schema = JSON[File.read(File.join(task.directory, task.json_outfile))]

      # find all ENUM types that aren't system level (ie, start with '__')
      # and convert these into a key-value mapping like so:
      #
      # "JobsStatus":{
      #   "Active": "Active Jobs",
      #   "Done":"Done Jobs",
      #   "Map":"Map Jobs",
      #   "All":"All Jobs"
      # },
      # "JobStatus": {
      #  ...etc
      # }
      #
      # this way we can easily lookup the human readable name via the enum
      # value and we can get a nice list for drop-downs, etc
      descriptions = json_schema["data"]["__schema"]["types"]
        .select { |t| t['kind'] == "ENUM" && !t['name'].starts_with?('__') }
        .reduce({}) do |p, c|
          p[c['name']] = c['enumValues']
            .reduce({}) do |_p, _c|
              _p[_c['name']] = _c['description']
              _p
            end
          p
        end

      enums = json_schema["data"]["__schema"]["types"]
        .select { |t| t['kind'] == "ENUM" && !t['name'].starts_with?('__') }
        .reduce({}) do |p, c|
          p[c['name']] = c['enumValues']
            .reduce({}) do |_p, _c|
              _p[_c['name']] = _c['name']
              _p
            end
          p
        end

      # and write them in our schema directory
      enums_path = File.join(task.directory, 'enums.json')
      descriptions_path = File.join(task.directory, 'descriptions.json')

      File.write(enums_path, enums.to_json)
      puts "Enums JSON dumped into #{enums_path}"

      File.write(descriptions_path, descriptions.to_json)
      puts "Enums JSON dumped into #{descriptions_path}"
    end
  end
end
