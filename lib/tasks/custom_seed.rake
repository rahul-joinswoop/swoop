# frozen_string_literal: true

# Create seed tasks for all files under `./db/seeds/`. It allows running
# specific seed files with:
#
#   bundle exec rake db:seed:name_of_seed_file_without_dot_rb
#
# From on: http://stackoverflow.com/a/19872375/279026
namespace :db do
  namespace :seed do
    Dir[Rails.root.join('db', 'seeds', '*.rb')].each do |filename|
      task_name = File.basename(filename, '.rb').intern
      task task_name => :environment do
        load(filename) if File.exist?(filename)
      end
    end
  end
end
