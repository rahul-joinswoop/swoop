# frozen_string_literal: true

# rake state
task state: :environment do |t, args|
  puts "starting up..."
  puts "Set Assigned from Pending " + ActiveRecord::Base.connection.execute("update jobs set status='Assigned' from companies as c where  jobs.fleet_company_id=c.id and c.type='FleetCompany' and rescue_company_id is not null  and status='Pending'").cmd_status
  puts "Set Cancelled where null" + ActiveRecord::Base.connection.execute("update jobs set status='Canceled' where status is null").cmd_status
end
