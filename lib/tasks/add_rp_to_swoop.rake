# frozen_string_literal: true

# rake add_rp_to_swoop
task add_rp_to_swoop: :environment do |t, args|
  swoop = Company.swoop
  RescueCompany.all.each do |company|
    args = { company: swoop, provider: company }
    rp = RescueProvider.find_by(args)
    if rp
      p "found rp"
    else
      if company.sites.length > 0
        company.sites.each do |site|
          args[:site] = site
          p "creating RP"
          RescueProvider.create!(args)
        end
      else
        p "Creating Site and RP"
        site = PartnerSite.create!(company: company, location: company.location)
        args[:site] = site
        RescueProvider.create!(args)
      end
    end
  end
end
