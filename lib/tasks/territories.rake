# frozen_string_literal: true

namespace :territories do
  desc <<~HEREDOC
      Backfills the territories for the rescue companies. It does this by finding each rescue company's sites and creating a circle-shaped ('circleoid') polygon around it.

      Specifying the Companies to backfill
      ------------------------------------
      If 'company_ids' are specifyed then the Backfill will run only for the specified companies.

      Example:
        rake territories:back_fill company_ids='12 234'


      Just simulate the Backfill
      --------------------------
      If the 'simulate' is specified then the Territories won't be created, but the script will run properly and will log every action.

      Example:
        rake territories:back_fill simulate=true
    HEREDOC

  task back_fill: :environment do |t, args|
    puts "Starting to backfill the territories."
    simulation = ENV.key?('simulate') ? ActiveRecord::Type::Boolean.new.deserialize(ENV['simulate']) : true
    if simulation
      puts "This is a simulation."
      puts "Everything will be logged as supposed to be but no records will be altered."
    else
      puts "This is not a simulation."
      puts "This will create Territory records."
    end

    initial_number_of_territories = Territory.count
    puts "Initial number of Territory records:#{initial_number_of_territories}"

    if ENV['company_ids'].present?
      puts "Company Ids are specified: '#{ENV['company_ids']}'."
      puts "Running only on a subset of territories."

      ENV['company_ids'].split.map(&:to_i).each do |company_id|
        Territories::BackfillTerritoriesForCompany.new.call(company_id, simulation: simulation)
      end
    else
      puts "Running the backfill for every rescue company."

      RescueCompany.find_each do |rescue_company|
        Territories::BackfillTerritoriesForCompany.new.call(rescue_company.id, simulation: simulation)
      end
    end

    number_of_territories_at_the_end = Territory.count
    puts "Finished with number of Territory records:#{number_of_territories_at_the_end}"
    puts "Finished with number of Territories created:#{number_of_territories_at_the_end - initial_number_of_territories}"

    puts "Done with the backfilling of the territories."
  end
end
