# frozen_string_literal: true

if ENV['ENABLE_COVERAGE'] || ENV['CIRCLECI']
  # this will only work (and should only be used) in ci or locally with
  # ENABLE_COVERAGE set.
  require 'active_support/inflector'
  require "simplecov"

  namespace :simplecov do
    task :merge do
      coverage_dir = './tmp/codeclimate'
      SimpleCov.coverage_dir(coverage_dir)
      merged_result = SimpleCov::ResultMerger.merged_result
      merged_result.command_name = 'RSpec'
      SimpleCovHelper.report_coverage
    end
  end

  class SimpleCovHelper

    def self.report_coverage(base_dir: './tmp/codeclimate')
      require_relative '../../spec/support/simplecov'
      # make sure to use the html formatter here
      SimpleCov.formatters = [SimpleCov::Formatter::HTMLFormatter]
      new(base_dir: base_dir).merge_results
    end

    attr_reader :base_dir

    def initialize(base_dir:)
      @base_dir = base_dir
    end

    def all_results
      Dir["#{base_dir}/.resultset*.json"]
    end

    def merge_results
      results = all_results.map do |file|
        SimpleCov::Result.from_hash(JSON.parse(File.read(file)))
      end
      SimpleCov::ResultMerger.merge_results(*results).tap do |result|
        SimpleCov::ResultMerger.store_result(result)
        result.format!
      end
    end

  end
end
