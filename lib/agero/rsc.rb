# frozen_string_literal: true

module Agero::Rsc

  class << self

    # A helper that takes the Authorization header and
    # strips the Bearer substring from it
    def clean_authorization_header(authorization_header)
      return if authorization_header.blank?

      authorization_header.to_s.sub(/\ABearer +/i, '')
    end

  end

end
