# frozen_string_literal: true

# Helper class for mapping a Job to an RSC DispatchStatus.
class Agero::Rsc::Status

  attr_reader :job, :code, :eta, :latitude, :longitude

  def initialize(job, code: nil, eta: nil, latitude: nil, longitude: nil, gps: false)
    @job = job

    if code.blank?
      code = Agero::Rsc::Data.current_job_status_code(job)
      raise ArgumentError, "Job status #{job.status.inspect} does not auto match to an Agero status" if code.nil?
    end
    if code.to_s.to_i == 0
      @code = Agero::Rsc::Data.status_code_id(code)
    else
      @code = code.to_i
    end

    @eta = eta
    @latitude = latitude || job.rescue_vehicle&.lat
    @longitude = longitude || job.rescue_vehicle&.lng
    @gps = gps
  end

  def to_hash
    hash = HashWithIndifferentAccess.new(
      code: code.to_s,
      source: "Swoop",
      extJobId: job.id.to_s,
    )

    # GPS tracking and status changes are nearly identical except for what the timestamp field is called.
    hash[gps? ? :timestamp : :statusTime] = Agero::Rsc::Data.utc_timestamp(job.updated_at)

    if latitude.present? && longitude.present?
      hash[:latitude] = latitude.to_f
      hash[:longitude] = longitude.to_f
    end

    hash[:eta] = eta.to_i if eta.present?

    if job.rescue_driver
      hash[:externalDriverId] = Agero::Rsc::Data.swoop_driver_id(job.rescue_driver_id)
    end

    hash
  end
  alias_method :to_h, :to_hash

  def gps?
    @gps
  end

end
