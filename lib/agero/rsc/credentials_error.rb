# frozen_string_literal: true

# This error is thrown if the client id and secret are rejected by the RSC API.
class Agero::Rsc::CredentialsError < Agero::Rsc::Error
end
