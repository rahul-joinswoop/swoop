# frozen_string_literal: true

# This error is thrown if a response returns a 403 status indicating
# the action was forbidden. This usually indicates the action was not
# allowed because of the state.
class Agero::Rsc::ForbiddenError < Agero::Rsc::Error
end
