# frozen_string_literal: true

# Wrapper around the raw HTTP response. If the response was a JSON document,
# the parsed body will be available in the `payload` attribute.
class Agero::Rsc::Response

  attr_reader :code, :body, :payload, :headers, :content_type

  def initialize(response)
    @raw_response = response
    @code = response.code.to_i
    @body = response.to_s
    @headers = response.headers
    @content_type = response.content_type&.mime_type&.downcase
    if @body.present?
      if @content_type == "application/json"
        @payload = MultiJson.load(body)
      elsif error?
        begin
          @payload = MultiJson.load(body)
        rescue
          # Ignore parsing errors on errors
        end
      end
    end
  end

  def success?
    code >= 200 && code < 300
  end

  def redirect?
    code >= 300 && code < 400
  end

  def client_error?
    code >= 400 && code < 500
  end

  def server_error?
    code >= 500 && code < 600
  end

  def error?
    client_error? || server_error?
  end

  def url
    @raw_response.uri.to_s
  end

  def to_s
    @raw_response.status.to_s
  end

end
