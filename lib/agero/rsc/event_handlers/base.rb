# frozen_string_literal: true

# This is the super class that must be implemented by RSC event handlers.
#
# During the initialization:
#   - it validates the given callback_token and raises
#     Agero::Rsc::CallbackTokenError in case it is not valid.
#
#   - it audits the event by adding an AuditIssc row
#     when the instance is ready to handle! the event,
#     or when the access_token is not valid.
#
#   - it adds @event and @oauth_access_token instance attributes, so subclasses can use them
module Agero::Rsc
  module EventHandlers
    class Base

      attr_reader :callback_token, :event

      def initialize(event:, callback_token:)
        @event = event
        @callback_token = callback_token
        Rails.logger.debug "#{self.class.name} - initialized"
      end

      def handle!
        raise NotImplementedError, "Subclasses must implement this method"
      end

      protected

      def api_access_token
        @api_access_token ||= APIAccessToken.find_by(callback_token: callback_token)
      end

      def notification_event_id
        @notification_event_id ||= @event[:notificationEventId]
      end

      def vendor_id
        @vendor_id ||= @event[:vendorId]
      end

      def location_id
        @location_id ||= @event[:facilityId]
      end

      def dispatchid
        @dispatchid ||= @event[:dispatchRequestNumber]
      end

      def job
        @job ||= api_access_token.find_job_by_dispatchid(dispatchid)
      end

    end
  end
end
