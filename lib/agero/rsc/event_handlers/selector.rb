# frozen_string_literal: true

# This is a Rsc Handler selector; it relies on event[:notificationCode]
# to select the correct Handler if we have it implemented, otherwise
# it will return a EventHandlers::NotHandled instance.
#
module Agero::Rsc
  module EventHandlers
    class Selector

      def self.find_handler_to(event:, callback_token:)
        notification_code = event[:notificationCode]
        handler_class = Agero::Rsc::Data.event_handler(notification_code)
        if handler_class
          handler_class.new(event: event, callback_token: callback_token)
        else
          raise ArgumentError, "no handler registered for #{notification_code.inspect}"
        end
      end

    end
  end
end
