# frozen_string_literal: true

# This Handler treats NewJobOffered notification event from Agero::Rsc,
# which means: new job coming from Agero so let's create it in our side and dispatch
# it to the corresponding vendor_id + location_id (aka rescue_company + respective site).
#
# We do it asynchronously with Agero::Rsc::NewJobOfferedWorker.
module Agero::Rsc
  module EventHandlers
    class NewJobOffered < EventHandlers::Base

      def handle!
        Rails.logger.info "#{self.class.name} - #{notification_event_id} - " \
          "the RSC job offered will be handled async by Agero::Rsc::NewJobOfferedWorker"

        Agero::Rsc::NewJobOfferedWorker.perform_async(callback_token, event)
      end

    end
  end
end
