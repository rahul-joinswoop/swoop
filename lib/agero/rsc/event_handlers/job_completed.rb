# frozen_string_literal: true

# This Handler treats JobComplete notification event (id: 29) from Agero::Rsc,
# which means the job has been completed
#
# We do it asynchronously with Agero::Rsc::JobCompletedWorker.
module Agero::Rsc
  module EventHandlers
    class JobCompleted < EventHandlers::Base

      def handle!
        Rails.logger.info "#{self.class.name} - #{notification_event_id} - " \
          "the RSC job has been completed, and it will be handled async by " \
          "Agero::Rsc::JobCompletedWorker"

        Agero::Rsc::JobCompletedWorker.perform_async(callback_token, event)
      end

    end
  end
end
