# frozen_string_literal: true

# It's likely to be used only when no Handler is found to treat an incoming Agero::Rsc event.
#
# This handler does not extend from Base, as we don't do any other thing but logging it.
module Agero::Rsc
  module EventHandlers
    class NotHandled < EventHandlers::Base

      def handle!
        Rails.logger.info(
          "Agero::Rsc::EventHandlers::NotHandled for event " \
          "#{JSON.pretty_generate(event)}"
        )
      end

    end
  end
end
