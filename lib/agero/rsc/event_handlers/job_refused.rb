# frozen_string_literal: true

# This Handler treats JobRejected notification event (id: 7) from Agero::Rsc,
# which means: the partner has sent an ETA to RSC, and Swoop job is in Job::STATUS_SUBMITTED
# status, but Agero has not accepted the ETA so let's set job.status = ETA Rejected.
#
# We do it asynchronously with Agero::Rsc::EtaRejectedWorker.
module Agero::Rsc
  module EventHandlers
    class JobRefused < EventHandlers::Base

      def handle!
        Rails.logger.info "#{self.class.name} - #{notification_event_id} - " \
          "the RSC jobhas been rejected by the provider, and it will be handled async by " \
          "Agero::Rsc::JobRefusedWorker"

        Agero::Rsc::JobRefusedWorker.perform_async(callback_token, event)
      end

    end
  end
end
