# frozen_string_literal: true

# This Handler treats Cancel notification event from Agero,
#
# We do it asynchronously with Agero::Rsc::AgeroCanceledWorker.
module Agero::Rsc
  module EventHandlers
    class AgeroCanceled < EventHandlers::Base

      def handle!
        Rails.logger.info "#{self.class.name} - #{notification_event_id} - " \
          "Agero has canceled the job, and it will handled async by " \
          "Agero::Rsc::AgeroCanceledWorker"

        Agero::Rsc::AgeroCanceledWorker.perform_async(callback_token, event)
      end

    end
  end
end
