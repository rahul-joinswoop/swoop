# frozen_string_literal: true

# This Handler treats JobRejected notification event (id: 7) from Agero::Rsc,
# which means: the partner has sent an ETA to RSC, and Swoop job is in Job::STATUS_SUBMITTED
# status, but Agero has not accepted the ETA so let's set job.status = ETA Rejected.
#
# We do it asynchronously with Agero::Rsc::EtaRejectedWorker.
module Agero::Rsc
  module EventHandlers
    class EtaRejected < EventHandlers::Base

      def handle!
        Rails.logger.info "#{self.class.name} - #{notification_event_id} - " \
          "the RSC job ETA has been rejected, and it will handled async by " \
          "Agero::Rsc::EtaRejectedWorker"

        Agero::Rsc::EtaRejectedWorker.perform_async(callback_token, event)
      end

    end
  end
end
