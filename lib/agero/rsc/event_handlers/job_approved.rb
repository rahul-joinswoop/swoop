# frozen_string_literal: true

# This Handler treats JobAccepted notification event (id: 8) from Agero::Rsc,
# which means: the partner has sent an ETA to RSC, and Swoop job is in Job::STATUS_SUBMITTED
# status, so let's move it to accepted.
#
# We do it asynchronously with Agero::Rsc::JobAcceptedWorker.
module Agero::Rsc
  module EventHandlers
    class JobApproved < EventHandlers::Base

      def handle!
        Rails.logger.info "#{self.class.name} - #{notification_event_id} - " \
          "the RSC job ETA has been accepted, and it will handled async by " \
          "Agero::Rsc::JobApprovedWorker"

        Agero::Rsc::JobApprovedWorker.perform_async(callback_token, event)
      end

    end
  end
end
