# frozen_string_literal: true

# Handle service timeout messages from an RSC subscription which indicates
# that a job offer has expired on Agero's end.
module Agero::Rsc
  module EventHandlers
    class OfferExpired < EventHandlers::Base

      def handle!
        Rails.logger.info "#{self.class.name} - #{notification_event_id}"
        job = api_access_token.find_job_by_dispatchid(dispatchid)
        if job
          Agero::Rsc::ExpireJobWorker.perform_async(job.id)
        end
      end

    end
  end
end
