# frozen_string_literal: true

# Helper for populating Swoop Job fields from an Agero Dispatch.
# For RSC jobs Agero is the source of record and jobs in Swoop
# should not be altering the fields set here.
module Agero
  module Rsc
    class JobBuilder

      attr_reader :job

      def initialize(job)
        raise(ArgumentError, "job must be a FleetMotorClubJob") unless job.is_a?(FleetMotorClubJob)
        @job = job
      end

      def build(dispatch)
        build_driver_from_dispatch(dispatch)
        build_drop_location_from_dispatch(dispatch)
        build_service_location_from_dispatch(dispatch)
        build_dropoff_contact_from_dispatch(dispatch)

        normalized_phone = Phonelib.parse(
          dispatch.dig("disablementLocation", "dispatcherContactNumber")
        ).e164
        if normalized_phone
          dispatcher = User.find_by(phone: normalized_phone)
          job.fleet_dispatcher = dispatcher if dispatcher
        end

        job.notes = notes_from_dispatch(dispatch)
        job.odometer = odometer_from_dispatch(dispatch)
        job.po_number = po_number_from_dispatch(dispatch)
        job.ref_number = reference_number_from_dispatch(dispatch)
        job.service_code_id = service_id_from_dispatch(dispatch)

        job.adjusted_created_at ||= dispatch["receivedTime"]

        job.fixup_locations

        job
      end

      def update!(dispatch)
        build(dispatch)
        save!
        job
      end

      def save!
        job.transaction do
          if job.new_record?
            job.save!
          else
            job.save!
            if job.driver
              job.driver.user.save! if job.driver.user && job.driver.user.changed?
              job.driver.vehicle.save! if job.driver.vehicle && job.driver.vehicle.changed?
              job.driver.save! if job.driver.changed?
            end
            job.drop_location.save! if job.drop_location && job.drop_location.changed?
            job.service_location.save! if job.service_location && job.service_location.changed?
            job.dropoff_contact.save! if job.dropoff_contact && job.dropoff_contact.changed?
          end
        end
      end

      private

      def build_driver_from_dispatch(dispatch)
        driver = (job.driver || job.build_driver)
        user = (driver.user || driver.build_user)
        vehicle = (driver.vehicle || driver.build_vehicle)

        update_user_from_dispatch(user, dispatch.dig("disablementLocation", "contactInfo"))

        vehicle.vin = dispatch.dig("vehicle", "vin")
        vehicle.year = dispatch.dig("vehicle", "year")
        vehicle.make = dispatch.dig("vehicle", "make")
        vehicle.model = dispatch.dig("vehicle", "model")
        vehicle.color = dispatch.dig("vehicle", "color")
        vehicle.license = dispatch.dig("vehicle", "plate")

        driver.user = user
        driver.vehicle = vehicle
      end

      def build_drop_location_from_dispatch(dispatch)
        if dispatch["towDestination"].present?
          location = (job.drop_location || job.build_drop_location)
          update_location_from_dispatch(location, dispatch["towDestination"])
        end
      end

      def build_service_location_from_dispatch(dispatch)
        if dispatch["disablementLocation"].present?
          location = (job.service_location || job.build_service_location)
          update_location_from_dispatch(location, dispatch["disablementLocation"])
        end
      end

      def update_location_from_dispatch(location, dispatch_location)
        location.street = dispatch_location["address1"]
        location.city = dispatch_location["city"]
        location.state = dispatch_location["state"]
        location.zip = dispatch_location["postalCode"]
        location.lat = (dispatch_location["latitude"] || dispatch_location["lat"])
        location.lng = (dispatch_location["longitude"] || dispatch_location["lng"])

        # Display address
        location.address = [location.street, location.city, "#{location.state} #{location.zip}"].reject(&:blank?).join(", ")

        agero_location_type = Agero::Rsc::Data.location_type(dispatch_location["locationType"])
        location_type_code = Agero::Rsc::Data.location_type(agero_location_type)
        location.location_type = LocationType.find_by(name: location_type_code) if location_type_code
      end

      def build_dropoff_contact_from_dispatch(dispatch)
        contact_info = dispatch.dig("towDestination", "contactInfo")
        if contact_info && %w(name callbackNumber).any? { |field| contact_info[field].present? }
          user = (job.dropoff_contact || job.build_dropoff_contact)
          update_user_from_dispatch(user, contact_info) if contact_info
        end
      end

      def update_user_from_dispatch(user, contact_info)
        user.full_name = contact_info["name"]
        user.phone = Phonelib.parse(contact_info["callbackNumber"]).to_s
        user
      end

      def odometer_from_dispatch(dispatch)
        dispatch["vehicle"]["mileage"]
      end

      def po_number_from_dispatch(dispatch)
        dispatch["poNumber"]
      end

      def reference_number_from_dispatch(dispatch)
        dispatch["referenceNumber"]
      end

      def service_id_from_dispatch(dispatch)
        service_name = Agero::Rsc::Data.service_type(dispatch["serviceType"])
        ServiceCode.find_by!(name: service_name)&.id if service_name
      end

      def notes_from_dispatch(dispatch)
        notes = (job.notes.present? ? job.notes.chomp.split("\n") : [])
        dispatch_notes = [
          service_comments_from_dispatch(dispatch),
          urgency_from_dispatch(dispatch),
          one_note_line_for(dispatch.dig("equipment", "type"), 'Engine Type'),
          one_note_line_for(dispatch.dig("equipment", "equipmentId"), 'Engine ID'),
          one_note_line_for(dispatch.dig("disablementLocation", "contactInfo", "phone"), 'Additional Phone Number'),
          one_note_line_for(dispatch.dig("vehicle", "state"), 'Vehicle State'),
          one_note_line_for(dispatch.dig("vehicle", "VehicleType"), 'Vehicle Type'),
          one_note_line_for(dispatch.dig("vehicle", "driveTrainType"), 'Vehicle Drive Train Type'),
          one_note_line_for(dispatch.dig("vehicle", "fuelType"), 'Vehicle Fuel Type'),
          one_note_line_for(dispatch['coverage'], 'Coverage'),
          one_note_line_for(dispatch.dig("disablementLocation", "garageLevel"), "Garage Level"),
          one_note_line_for(dispatch.dig("disablementLocation", "garageClearanceHeight"), "Garage Clearance"),
          one_note_line_for(dispatch.dig("disablementLocation", "isDriverWithVehicle"), "Driver With Vehicle"),
          one_note_line_for(dispatch.dig("disablementLocation", "dropInstructions"), "Drop Instructions"),
          one_note_line_for(dispatch.dig("disablementLocation", "parked"), "Parked"),
          one_note_line_for(dispatch.dig("disablementLocation", "nightDropOff"), "Pickup Night drop off"),
          one_note_line_for(dispatch.dig("disablementLocation", "crossStreet"), "Pickup Cross Street"),
          one_note_line_for(dispatch.dig("disablementLocation", "landmark"), "Pickup Landmark"),
          one_note_line_for(dispatch.dig("disablementLocation", "poi1"), "Pickup Point of Interest"),
          one_note_line_for(dispatch.dig("disablementLocation", "poi2"), "Pickup Point of Interest"),
          one_note_line_for(dispatch.dig("towDestination", "nightDropOff"), "Drop-Off Night drop off"),
          one_note_line_for(dispatch.dig("towDestination", "crossStreet"), "Drop-Off Cross Street"),
          one_note_line_for(dispatch.dig("towDestination", "landmark"), "Drop-Off Landmark"),
          one_note_line_for(dispatch.dig("towDestination", "poi1"), "Drop-Off Point of Interest"),
          one_note_line_for(dispatch.dig("towDestination", "poi2"), "Drop-Off Point of Interest"),
        ].reject(&:blank?)
        dispatch_notes.each do |note|
          notes << note unless note.blank? || notes.include?(note)
        end
        if notes.empty?
          nil
        else
          "#{notes.join("\n")}\n"
        end
      end

      def service_comments_from_dispatch(dispatch)
        service_comments = dispatch["comments"].map { |comment| comment["commentText"] }.join(", ")
        "Service Comments: #{service_comments}"
      end

      def one_note_line_for(attribute, label = "")
        return "" if attribute.blank?

        # If attribute is a boolen, convert to Yes/No
        if !!attribute == attribute
          attribute = yes_no(attribute)
        end

        "#{label}: #{attribute}"
      end

      def yes_no(attribute)
        attribute ? "Yes" : "No"
      end

      def urgency_from_dispatch(dispatch)
        dispatch["urgency"] unless dispatch["urgency"] == "STANDARD"
      end

    end
  end
end
