# frozen_string_literal: true

# This error is thrown if a callback token does not match with
# a corresponding vendorid from RSC received messages.
class Agero::Rsc::CallbackTokenError < Agero::Rsc::Error; end
