# frozen_string_literal: true

# This is a concern for RSC classes that work with RSC events.
# The class that includes this concern must have these attribuets defined:
#
#   - @callback_token
#   - @event
#
# These attributes will also automatically be added to the context output so
# anything that includes this module can be chained together.
module Agero::Rsc
  module EventConcern

    extend ActiveSupport::Concern

    included do
      include(Interactor::Swoop) unless include?(Interactor::Swoop)

      attr_reader :callback_token, :event

      before do
        Rails.logger.debug("#{self.class.name} called #{context}")

        @callback_token = context.input[:callback_token]
        raise(ArgumentError, "callback_token is blank") if @callback_token.blank?

        @event = context.input[:event].with_indifferent_access
        raise(ArgumentError, "event is blank") if @event.blank?
      end

      after do
        context[:output] ||= {}
        context[:output][:callback_token] = @callback_token
        context[:output][:event] = @event
      end
    end

    def rsc_api
      api_access_token.api
    end

    # Return the RSC access token associated with the event.
    def api_access_token
      @api_access_token ||= APIAccessToken.find_by!(
        callback_token: @callback_token, vendorid: @event[:vendorId], deleted_at: nil
      )
    end

    # Return the Issc record associated with the event.
    def issc
      @issc ||= api_access_token.isscs
        .joins(:issc_location)
        .where(issc_locations: { locationid: @event[:facilityId] })
        .first
    end

    def dispatch_request_number
      @event[:dispatchRequestNumber]
    end

    def notification_event_id
      @event[:notificationEventId]
    end

    def dispatch_details
      unless defined?(@dispatch_details)
        @dispatch_details = rsc_api.get_dispatch(dispatch_request_number) if dispatch_request_number.present?
      end
      @dispatch_details
    end

    # Return the job (if any) associated with the request dispatch request number
    def job
      unless defined?(@job)
        @job = api_access_token.find_job_by_dispatchid(@event[:dispatchRequestNumber])
      end
      @job
    end

  end
end
