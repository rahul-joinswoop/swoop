# frozen_string_literal: true

# This class provides wrappers around the methods in the Agero RSC API.
# https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
module Agero
  module Rsc
    class API

      attr_reader :client
      private :client

      class MissingAccessTokenError < ArgumentError
      end

      # Helper class to expose hash keys with indifferent access and as methods.
      class ResponsePayload < Hash

        include Hashie::Extensions::MethodAccess
        include Hashie::Extensions::IndifferentAccess

        def initialize(attributes, time_fields: nil)
          super(nil)
          set_attributes(attributes, time_fields)
        end

        private

        def set_attributes(attributes, time_fields)
          attributes.each do |key, value|
            tf = time_fields[key.to_sym] if time_fields.is_a?(Hash)
            if value.is_a?(Hash)
              value = self.class.new(value, time_fields: tf)
            elsif value.is_a?(Array)
              value = value.map { |v| v.is_a?(Hash) || v.is_a?(Array) ? self.class.new(v, time_fields: tf) : v }
            elsif tf
              value = utc_time(value)
            end
            self[key.to_s] = value
          end
        end

        def utc_time(time)
          Time.find_zone("UTC").parse(time) if time
        end

      end

      class << self

        # Get an instance of the API initialized with the access token associated with the specified job.
        # The job can be passed as either a Job or a job id. Raises Agero::Rsc::API::MissingAccessTokenError
        # if the job is not associated with a valid access token.
        def for_job(job_or_id)
          job = (job_or_id.is_a?(Job) ? job_or_id : Job.find(job_or_id))
          api_access_token = job.issc_dispatch_request&.issc&.api_access_token

          if api_access_token.nil? || api_access_token.deleted_at.present?
            raise MissingAccessTokenError, "JOB(#{job.id}) does not have a valid issc_dispatch_request.issc.api_access_token"
          else
            new(api_access_token.access_token)
          end
        end

        # Helper method to lookup the dispatch request number for a job.
        def dispatch_request_number(job)
          job.issc_dispatch_request&.dispatchid
        end

      end

      def initialize(access_token = nil)
        @client = Agero::Rsc::Client.new(access_token: access_token)
      end

      # 5.2 Get A Single Dispatch by Dispatch Request Number
      def get_dispatch(dispatch_request_number)
        response = client.get!("/dispatches/#{dispatch_request_number}/detail")
        wrap_payload(response.payload, time_fields: {
          receivedTime: true,
          acceptedTime: true,
          eta: true,
          revisedEta: true,
          currentStatus: { statusTime: true },
          assignedDriver: { profileLastUpdatedAt: true },
        })
      end

      # 5.4 Submit Dispatch Acceptance
      # If the reason matches a known reason code in Agero::Rsc::Data, provide it as as symbol, otherwise as a string
      def accept_dispatch(dispatch_request_number, eta:, reason: nil)
        params = { eta: eta }
        if reason
          params[:reasonCode] = Agero::Rsc::Data.eta_reason_code_id(reason)
          params[:reason] = Agero::Rsc::Data.eta_reason_description(reason)
        end
        client.post!("/dispatches/#{dispatch_request_number}/acceptDispatchRequest", params: params)
        nil
      end

      # 5.5 Submit Dispatch Refusal
      # Provide the reason as a symbol as one of the reason codes defined in Agero::Rsc::Data
      def refuse_dispatch(dispatch_request_number, reason:)
        params = { reasonCode: Agero::Rsc::Data.refusal_reason_code_id(reason) }
        client.post!("/dispatches/#{dispatch_request_number}/refuseDispatchRequest", params: params)
        nil
      end

      # 5.6 Request More Time to Accept
      def request_more_time(dispatch_request_number)
        response = client.post!("/dispatches/#{dispatch_request_number}/requestExtension")
        wrap_payload(response.payload)
      end

      # 5.9 Assign External Driver
      # The driver argument should be a ::Driver instance
      def assign_external_driver(dispatch_request_number, driver:, vehicle: nil, site: nil)
        params = {
          externalDriverId: Agero::Rsc::Data.swoop_driver_id(driver.id),
          driverProfile: Agero::Rsc::Data.driver_profile(driver, vehicle: vehicle, site: site),
        }
        client.put!("/dispatches/#{dispatch_request_number}/assignExternalDriver", params: params)
        nil
      end

      # 5.10 Cancel a Dispatch
      # Status and reason should be symbols defined in Agero::Rsc::Data
      def cancel_dispatch(dispatch_request_number, status:, reason:)
        params = {
          statusCode: Agero::Rsc::Data.status_code_id(status).to_s,
          reasonCode: Agero::Rsc::Data.cancel_reason_code_id(reason),
        }
        client.post!("/dispatches/#{dispatch_request_number}/cancel", params: params)
        nil
      end

      # 5.19 Request Phone Call
      def request_phone_call(dispatch_request_number, callback_phone_number:, name:, alternate_phone: nil)
        params = {
          callbackNumber: callback_phone_number,
          name: name,
        }
        params[:phone] = alternate_phone if alternate_phone.present?
        client.put!("/dispatches/#{dispatch_request_number}/requestPhoneCall", params: params)
        nil
      end

      # 6.4 Change Status.
      # The status argument is a Agero::Rsc::Status object
      def change_status(dispatch_request_number, status:)
        params = status.to_hash
        response = client.post!("/dispatches/#{dispatch_request_number}/status", params: params)
        wrap_payload(response.payload)
      end

      # 6.5 Undo Current Status
      def undo_status(dispatch_request_number)
        response = client.post!("/dispatches/#{dispatch_request_number}/status/undo")
        wrap_payload(response.payload)
      end

      # 7.12 Get Facility list
      def facility_list(vendor_id)
        response = client.get!("/vendors/#{vendor_id}/facilities")
        wrap_payload(response.payload)
      end

      # 12.x Get enumeration list. This endpoint is more generic than other ones on purpose
      # since not all enumeration endpoints are documented and we only will be calling them
      # from consoles for debugging to fetch the latest lists.
      def enumeration(enumeration_type)
        response = client.get("/enumerations/#{enumeration_type}")
        wrap_payload(response.payload)
      end

      # 13.3 Provide Driver/Truck Locations
      # The params args should be hashes with keys for :driver_id, :latitude, :longitude, and :timestamp.
      def update_driver_locations(*params)
        payload = []
        params.flatten.each do |data|
          data = data.with_indifferent_access
          if data[:driver_id] && data[:latitude] && data[:longitude] && data[:timestamp]
            payload << {
              latitude: data[:latitude],
              longitude: data[:longitude],
              externalDriverId: data[:driver_id].to_s,
              locationDateTime: data[:timestamp].to_time.utc,
            }
          else
            raise ArgumentError, "missing location values in #{data.inspect}"
          end
        end

        # This operation is called frequently and is not critical, so use a lower timeout
        # to protect our systems in case something is horribly wrong on the other end.
        timeout = Configuration.rsc_api.location_timeout.to_f
        response = client.post!("/drivers/currentLocations", params: payload, timeout: timeout)
        wrap_payload(response.payload)
      end

      # 15.1 Authorize 3rd Party App - returns a URL to the Agero signon page.
      # The value of the state argument will be passed back to the callback URL registered with the app.
      def authorize_url(state)
        # TODO we need scope WRITE, but that doesn't work with test logins
        params = { client_id: client_id, state: state, scope: "READ", response_type: "code", external_Driver_Mgmt: "false" }
        response = client.get!("/oauth/authorize", params: params, auth: :basic)
        response.headers["Location"] if response.redirect?
      end

      # 15.4 Get Access Token - return an access token and vendor info given an Oauth2 authorization code.
      def get_access_token(authorization_code)
        # TODO need to validate role returned to ensure it is an approved list that has the access needed.
        response = client.get!("/oauth/accesstoken", params: { grant_type: "authorization_code", code: authorization_code }, auth: :basic)
        wrap_payload(response.payload)
      end

      # 15.6 SignIn - sign the user in to indicate they are available to accept jobs.
      def sign_in(clientid)
        response = client.post!("/oauth/signIn", params: { clientId: clientid.to_s })
        wrap_payload(response.payload)
      end

      # 15.7 SignOut - sign the user out to indicate they are no longer available to accept jobs.
      def sign_out(clientid)
        client.post!("/oauth/signOut", params: { clientId: clientid.to_s })
        nil
      end

      # 16.1 Subscribe To Server Notifications
      def subscribe_to_server_notifications(callback_token)
        handle_rsc_event_url = Rails.application.routes.url_helpers.api_digital_dispatch_handle_rsc_event_url
        params = {
          callBackUrl: handle_rsc_event_url,
          notificationToken: callback_token,
          notificationEvents: Agero::Rsc::Data.notification_type_ids.join(','),
        }
        response = client.post!("/serverNotifications/subscribe", params: params)
        wrap_payload(response.payload)
      end

      # 16.5 Unsubscribe Server Notifications
      def unsubscribe_from_server_notifications(subscription_id)
        client.delete!("/serverNotifications/subscriptions/#{subscription_id}")
        nil
      end

      # 16.7 Get a Server Notifications Subscription
      def subscription_info(subscription_id)
        response = client.get!("/serverNotifications/subscriptions/#{subscription_id}")
        wrap_payload(response.payload)
      end

      # TODO These methods probably don't need to be implemented. Keeping this as a working list
      # while we develop RSC integration just so we know they are available
      # 5.1 Get Dispatch List
      # 5.3 Get A Single Dispatch By PO Number
      # 5.7 Get Dispatch Assignment
      # 5.8 Assign Driver
      # 5.11 Add a Signature
      # 5.12 Get Signatures
      # 5.13 Get a Signature
      # 5.14 Get Dispatch Summary
      # 5.15 Add a Dispatch Document
      # 5.16 Get Dispatch Documents
      # 5.17 Get a Dispatch Document
      # 5.18 Delete a Dispatch Document
      # 6.2 Get Current Status
      # 6.3 Get Status History
      # 6.6 Track Status GPS Location
      # 6.7 Track PTO Event
      # 7.4 Upload Equipment picture
      # 7.6 Update Equipment picture
      # 7.8 Get List of Drivers
      # 13.1 Get Notify Current Location
      # 13.2 Provide Current Location of a driver
      # 16.3 Acknowledge Notification
      # 16.4 Update Subscription

      private

      def client_id
        client.client_id
      end

      # Helper method to wrap the response payload in a Hashie::Mash
      def wrap_payload(payload, time_fields: nil)
        if payload.is_a?(Hash)
          ResponsePayload.new(payload, time_fields: time_fields)
        elsif payload.is_a?(Array)
          payload.map { |value| wrap_payload(value, time_fields: time_fields) }
        else
          payload
        end
      end

    end
  end
end
