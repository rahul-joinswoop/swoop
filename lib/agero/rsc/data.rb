# frozen_string_literal: true

# Translation tables for Agero data identifiers.
class Agero::Rsc::Data

  AGERO_TIME_FORMAT = "%Y%m%dT%H: %M: %SZ"
  AGERO_UTC_TIMESTAMP_FORMAT = "%Y-%m-%d %I:%M %p"

  NOTIFICATION_TYPES = {
    new_job_offered: 1,
    job_refused: 2,
    job_accepted: 3,
    job_assigned: 4,
    job_cancelled: 5,
    job_reassigned: 6,
    eta_rejected: 7,
    job_approved: 8,
    goa_accepted: 15,
    test_notificaton: 17,
    eta_evaluation: 18,
    job_auto_assigned: 19,
    job_accepted_already: 21,
    job_refused_already: 22,
    job_cancelled_dispatcher: 23,
    job_cancelled_already: 24,
    job_cancel_evaluation: 25,
    job_cancel_evaluation_dispatcher: 26,
    job_complete: 29,
    service_timeout: 30,
    en_route: 31,
    on_scene: 32,
    tow_in_progress: 33,
    destination_arrival: 34,
    job_undo: 35,
    job_extended: 36,
    sp_request_phone_call: 37,
  }.with_indifferent_access.freeze

  NOTIFICATION_TYPE_IDS = NOTIFICATION_TYPES.invert.freeze

  # Handlers defined for notification types
  EVENT_NOTIFICATION_CODE_MAPPER = {
    new_job_offered: Agero::Rsc::EventHandlers::NewJobOffered,
    job_refused: Agero::Rsc::EventHandlers::JobRefused,
    eta_rejected: Agero::Rsc::EventHandlers::EtaRejected,
    job_approved: Agero::Rsc::EventHandlers::JobApproved,
    job_cancelled: Agero::Rsc::EventHandlers::AgeroCanceled,
    job_cancelled_dispatcher: Agero::Rsc::EventHandlers::AgeroCanceled,
    service_timeout: Agero::Rsc::EventHandlers::OfferExpired,
    job_completed: Agero::Rsc::EventHandlers::JobCompleted,
  }.with_indifferent_access.freeze

  STATUS_CODES = {
    new: 1,
    po_needed: 2,
    expired: 4,
    pending: 3,
    rejected: 5,
    refused: 6,
    unassigned: 0,
    assigned: 10,
    in_route: 20,
    on_scene: 30,
    tow_in_progress: 40,
    destination_arrival: 50,
    job_complete: 60,
    service_provider_cancel: 70,
    customer_cancel: 80,
    unsuccessful_no_vehicle: 90,
    unsuccessful_no_customer: 95,
    service_attempt_failed: 99,
  }.with_indifferent_access.freeze

  STATUS_CODE_IDS = STATUS_CODES.invert.freeze

  SWOOP_TO_AGERO_STATUSES = {
    JobStatus::PENDING => :pending,
    JobStatus::EN_ROUTE => :in_route,
    JobStatus::ON_SITE => :on_scene,
    JobStatus::TOWING => :tow_in_progress,
    JobStatus::TOW_DESTINATION => :destination_arrival,
    JobStatus::COMPLETED => :job_complete,
    JobStatus::ACCEPTED => :unassigned,
    JobStatus::EXPIRED => :expired,
    JobStatus::REASSIGN => :rejected,
    JobStatus::REJECTED => :refused,
  }.with_indifferent_access.freeze

  SWOOP_TO_AGERO_ETA_EXPLANATIONS = {
    JobEtaExplanation::NONE => :other,
    JobEtaExplanation::TRAFFIC => :traffic,
    JobEtaExplanation::WEATHER => :weather,
  }.freeze

  ETA_REASON_CODES = {
    weather: 401,
    traffic: 403,
    other: 0,
  }.with_indifferent_access.freeze

  ETA_REASON_CODE_IDS = ETA_REASON_CODES.invert.freeze

  ETA_REASON_DESCRIPTIONS = {
    weather: "Weather Emergency",
    traffic: "Extreme traffic in the area",
  }.with_indifferent_access.freeze

  SWOOP_TO_AGERO_REJECT_REASONS = {
    JobRejectReason::DO_NOT_ACCEPT_PAYMENT_TYPE => :payment_type,
    JobRejectReason::NO_LONGER_OFFER_SERVICE => :no_longer_offering_service,
    JobRejectReason::OTHER => :other,
    JobRejectReason::OUT_OF_MY_COVERAGE_AREA => :out_of_coverage_area,
    JobRejectReason::PROPER_EQUIPMENT_NOT_AVAILABLE => :equipment_not_available,
  }.freeze

  REFUSAL_REASON_CODES = {
    payment_type: 400,
    equipment_not_available: 406,
    no_longer_offering_service: 501,
    out_of_coverage_area: 409,
    other: 503,
  }.with_indifferent_access.freeze

  REFUSAL_REASON_CODE_IDS = REFUSAL_REASON_CODES.invert.freeze

  # TODO confirm what the 3000 series codes are for
  CANCEL_REASON_CODES = {
    prior_job_delayed: 201,
    traffic_vehicle_problem: 202,
    out_of_area: 203,
    another_job_priority: 204,
    no_reason_given: 205,
    found_alternate_solution: 101,
    problem_self_corrected: 102,
    customer_changed_mind: 103,
    customer_cancel: 210,
    service_provider_customer_cancel: 3005,
    wrong_location_given: 207,
    service_provider_wrong_location_given: 3002,
    customer_not_with_vehicle: 206,
    service_provider_customer_not_with_vehicle: 3001,
    incorrect_service: 208,
    incorrect_equipment: 209,
    wrong_equipment: 214,
    jump_did_not_work: 215,
    wrong_service: 216,
    need_additional_equipment: 217,
    service_provider_incorrect_service: 3003,
    service_provider_incorrect_equipment: 3004,
    goa_incorrect_service: 0,
  }.with_indifferent_access.freeze

  CANCEL_REASON_CODE_IDS = CANCEL_REASON_CODES.invert.freeze

  SWOOP_TO_AGERO_REASONS = {
    cancel_prior_job_delayed: :prior_job_delayed,
    cancel_traffic_service_vehicle_problem: :traffic_vehicle_problem,
    cancel_out_of_area: :out_of_area,
    cancel_another_job_priority: :another_job_priority,
    cancel_no_reason_given: :no_reason_given,
    cancel_customer_found_alternate_solution: :found_alternate_solution,
    goa_customer_cancel_after_deadline: :customer_cancel, # status 80, reason 210
    goa_wrong_location_given: :wrong_location_given, # status 90, reason 207
    goa_customer_not_with_vehicle: :customer_not_with_vehicle, # status 95, reason 206
    goa_incorrect_service: :incorrect_service, # status 99, reason 208
    goa_incorrect_equipment: :incorrect_equipment, # status 99, reason 209
    goa_unsuccessful_service_attempt: :need_additional_equipment, # status 99, reason 217
  }.with_indifferent_access.freeze

  REASON_TO_STATUS = {
    # primarily used for CANCEL:
    prior_job_delayed: :service_provider_cancel,
    traffic_vehicle_problem: :service_provider_cancel,
    out_of_area: :service_provider_cancel,
    another_job_priority: :service_provider_cancel,
    no_reason_given: :service_provider_cancel,
    found_alternate_solution: :customer_cancel,
    # primarily used for GOA:
    customer_cancel: :customer_cancel,
    wrong_location_given: :unsuccessful_no_vehicle,
    customer_not_with_vehicle: :unsuccessful_no_customer,
    incorrect_service: :service_attempt_failed,
    incorrect_equipment: :service_attempt_failed,
    need_additional_equipment: :service_attempt_failed,
  }.with_indifferent_access.freeze

  SERVICE_MAPPING = {
    tow_to_storage: ServiceCode::TOW,
    door_unlock: ServiceCode::LOCK_OUT,
    fuel_delivery: ServiceCode::FUEL_DELIVERY,
    jump_start: ServiceCode::BATTERY_JUMP,
    flat_tire: ServiceCode::TIRE_CHANGE,
    winch: ServiceCode::WINCH_OUT,
    reunite: ServiceCode::TOW,
    tow: ServiceCode::TOW,
  }.with_indifferent_access.freeze

  CLUB_JOB_TYPE_MAP = {
    "eDispatch" => "Digital",
    "LivePhone" => "Phone Transcript",
    "IVR" => "Phone Transcript",
  }.with_indifferent_access.freeze

  AGERO_LOCATION_MAPPINGS = {
    "Roadside -Highway" => LocationType::HIGHWAY,
    "Roadside -Street" => LocationType::LOCAL_ROADSIDE,
    "Residence" => LocationType::RESIDENCE,
    "Home" => LocationType::RESIDENCE,
    "Work" => LocationType::BUSINESS,
    "Parking Garage" => LocationType::PARKING_GARAGE,
    "Parking Lot" => LocationType::PARKING_LOT,
    "Driveway" => LocationType::RESIDENCE,
    "Other" => LocationType::LOCAL_ROADSIDE,
    "Residence/work-Street" => LocationType::LOCAL_ROADSIDE,
    "Residence/work-Driveway" => LocationType::RESIDENCE,
    "Residence/work-Parking garage" => LocationType::PARKING_GARAGE,
    "Residence/work-Parking Lot" => LocationType::PARKING_LOT,
    "General Parking Lot" => LocationType::PARKING_LOT,
    "General Parking Garage" => LocationType::PARKING_GARAGE,
    "Ramp" => LocationType::HIGHWAY,
    "Military Base" => LocationType::GATED_COMMUNITY,
  }.freeze

  class << self

    def notification_type_ids(*codes)
      if codes.blank?
        NOTIFICATION_TYPES.values
      else
        codes.flatten.map { |code| notification_type_id(code) }
      end
    end

    def notification_type(id)
      NOTIFICATION_TYPE_IDS[Integer(id)] || raise(ArgumentError, "invalid notification type id")
    end

    def notification_type_id(symbol)
      NOTIFICATION_TYPES[symbol] || raise(ArgumentError, "invalid notification type symbol")
    end

    def status_code(id)
      STATUS_CODE_IDS[Integer(id)] || raise(ArgumentError, "invalid status code id")
    end

    def status_code_id(symbol)
      STATUS_CODES[symbol] || raise(ArgumentError, "invalid status code symbol")
    end

    def eta_reason_code(id)
      ETA_REASON_CODE_IDS[Integer(id)] || raise(ArgumentError, "invalid eta reason code id")
    end

    def eta_reason_code_id(symbol)
      ETA_REASON_CODES[symbol] || raise(ArgumentError, "invalid eta reason code symbol")
    end

    def eta_reason_description(symbol)
      ETA_REASON_DESCRIPTIONS[symbol] || "None"
    end

    def refusal_reason_code(id)
      REFUSAL_REASON_CODE_IDS[Integer(id)] || raise(ArgumentError, "invalid refusal reason code id")
    end

    def refusal_reason_code_id(symbol)
      REFUSAL_REASON_CODES[symbol] || raise(ArgumentError, "invalid refusal reason code symbol")
    end

    def cancel_reason_code(id)
      CANCEL_REASON_CODE_IDS[Integer(id)] || raise(ArgumentError, "invalid cancel reason code id")
    end

    def cancel_reason_code_id(symbol)
      CANCEL_REASON_CODES[symbol] || raise(ArgumentError, "invalid cancel reason code symbol")
    end

    # Notification code should be an Agero numeric code.
    def event_handler(notification_code)
      type = notification_type(notification_code)
      EVENT_NOTIFICATION_CODE_MAPPER[type] || Agero::Rsc::EventHandlers::NotHandled
    rescue ArgumentError
      nil
    end

    # Map *current* job status to an Agero job status.
    # This can return nil if there the status doesn't map.
    def current_job_status_code(job)
      job_status_id = job.current_job_status_id
      job_status_code(job_status_id)
    end

    # Map the given job_status_id to an Agero job status.
    # This can return nil if there the status doesn't map.
    def job_status_code(job_status_id)
      code = SWOOP_TO_AGERO_STATUSES[job_status_id]

      if code.nil?
        if job_status_id == JobStatus::GOA
          # TODO
        elsif job_status_id == JobStatus::CANCELED
          # TODO
        end
      end
      code
    end

    # Takes the job_reason and the job_status and maps them
    # with a corresponding RSC status and reason.
    #
    # Primarily used by Cancel and GOA cases.
    def rsc_status_and_reason_code_hash(job_reason_key, job_status)
      rsc_reason_code = rsc_reason_code(job_reason_key, job_status)
      job_status_code = rsc_status_code_by_reason_code(rsc_reason_code)

      { reason_code: rsc_reason_code, status_code: job_status_code }
    end

    # Maps a job_reason_key with a RSC reason.
    # Uses the job_status to choose a fallback in case no reason found.
    def rsc_reason_code(job_reason_key, job_status)
      rsc_reason_code = SWOOP_TO_AGERO_REASONS[job_reason_key&.to_sym]

      if rsc_reason_code.nil?
        rsc_reason_code =
          case job_status
          when JobStatus::CANCELED
            :no_reason_given
          when JobStatus::GOA
            :incorrect_service
          else
            raise ArgumentError, 'invalid job reason'
          end
      end

      rsc_reason_code
    end

    # Returns a RSC status_code by a given RSC reason_code.
    #
    # Primarily used by Cancel and GOA cases, where the dispatch status
    # depend on the reason_code.
    def rsc_status_code_by_reason_code(reason_code)
      job_status_code = REASON_TO_STATUS[reason_code]

      if job_status_code.nil?
        job_status_code =
          case job_status
          when JobStatus::CANCELED
            :service_provider_cancel
          when JobStatus::GOA
            :service_attempt_failed
          else
            raise ArgumentError, 'invalid job status'
          end
      end

      job_status_code
    end

    # Map the given job_eta_explanation_id to an Agero job eta explanation code.
    def job_eta_explanation_code(job_eta_explanation_id)
      return :other unless job_eta_explanation_id

      eta_explanation = JobEtaExplanation.select(:text).find(job_eta_explanation_id)

      SWOOP_TO_AGERO_ETA_EXPLANATIONS[eta_explanation.text] || :other
    end

    def job_refusal_explanation_code(job_reject_reason_id)
      return :other unless job_reject_reason_id

      job_reject_reason = JobRejectReason.select(:text).find(job_reject_reason_id)

      SWOOP_TO_AGERO_REJECT_REASONS[job_reject_reason.text] || :other
    end

    # Map call method type (12.48) in the dispatchSource field to an ISSC club job type.
    def club_job_type(call_method_type)
      CLUB_JOB_TYPE_MAP.fetch(call_method_type, "Digital")
    end

    # Translate Agero location type to Swoop location type
    def location_type(agero_location_type)
      AGERO_LOCATION_MAPPINGS[agero_location_type]
    end

    # Translate Agero service type to Swoop service type
    def service_type(agero_service_type)
      agero_service_type = agero_service_type.to_s.parameterize.underscore
      Agero::Rsc::Data::SERVICE_MAPPING[agero_service_type] || ServiceCode::OTHER
    end

    def format_time(time)
      return nil if time.nil?
      time.utc.strftime(AGERO_TIME_FORMAT)
    end

    def utc_timestamp(time)
      return nil if time.nil?
      time.utc.strftime(AGERO_UTC_TIMESTAMP_FORMAT)
    end

    def swoop_driver_id(driver_id)
      driver_id.to_s if driver_id.present?
    end

    # Serialize a Swoop driver (User) to an Agero driver profile hash.
    def driver_profile(user, vehicle: nil, site: nil)
      site ||= user.company&.hq
      rescue_provider = site.rescue_providers.detect(&:swoop?) if site
      phone = (rescue_provider ? rescue_provider.phone : user.company&.phone)
      dispatch_email = (rescue_provider ? rescue_provider.dispatch_email : user.company&.dispatch_email)
      hash = {
        externalDriverId: swoop_driver_id(user.id),
        name: user.name,
        gpsTracking: (vehicle.present? && vehicle.live_updates?),
      }
      hash[:cellPhone] = user.phone if user.phone
      hash[:officePhone] = phone if phone
      hash[:officeEmail] = dispatch_email if dispatch_email
      hash[:onDuty] = user.on_duty unless user.on_duty.nil?
      if vehicle && vehicle.live_updates?
        hash[:currentLocation] = location(Location.new(lat: vehicle.lat, lng: vehicle.lng))
      end
      hash
    end

    # Serialize a Swoop location to a location hash.
    def location(loc)
      return nil if loc.nil?
      hash = { lat: loc.lat, lng: loc.lng }
      if loc.street.present?
        hash[:address1] = loc.street
        hash[:city] = loc.city
        hash[:state] = loc.state
        hash[:postalCode] = loc.zip
      elsif loc.address.present?
        hash[:address1] = loc.address
      end
      hash
    end

  end

end
