# frozen_string_literal: true

# This error is thrown if an access token is rejected by the RSC API.
class Agero::Rsc::AccessTokenError < Agero::Rsc::Error
end
