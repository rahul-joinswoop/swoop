# frozen_string_literal: true

# Client for calling the Agero RSC API.
#
# The client must be configured with a base_url, client_id, and client_secret.
# It can optionally also specify a user_agent and timeouts. For development
# you can also supply an ssl_context and proxy values.
class Agero::Rsc::Client

  include HTTPClient

  class_attribute :base_url, :client_id, :client_secret, :ssl_context, :user_agent,
                  :timeout, :connect_timeout, :read_timeout, :write_timeout, :proxy

  self.timeout = 10.0

  class << self

    # Load environment configuration from config/rsc_api.yml file if it exists.
    def configure!
      config = Configuration.rsc_api.config
      if config
        config.each do |key, value|
          send("#{key}=", value) if respond_to?("#{key}=")
        end
      end
    end

    def url(path)
      path = "/#{path}" unless path.start_with?("/")
      "#{base_url.chomp('/')}#{path}"
    end

  end
  configure!

  def initialize(access_token: nil)
    @access_token = access_token
  end

  def url(path)
    path = "/#{path}" unless path.start_with?("/")
    "#{base_url.chomp('/')}#{path}"
  end

  def get(path, headers: nil, params: nil, auth: :bearer, timeout: nil)
    api_request(:get, path, headers: headers, params: params, auth: auth, timeout: timeout)
  end

  def get!(path, headers: nil, params: nil, auth: :bearer, timeout: nil)
    raise_response_error!(get(path, headers: headers, params: params, auth: auth, timeout: timeout), auth)
  end

  def post(path, headers: nil, params: nil, auth: :bearer, timeout: nil)
    api_request(:post, path, headers: headers, params: params, auth: auth, timeout: timeout)
  end

  def post!(path, headers: nil, params: nil, auth: :bearer, timeout: nil)
    raise_response_error!(post(path, headers: headers, params: params, auth: auth, timeout: timeout), auth)
  end

  def put(path, headers: nil, params: nil, auth: :bearer, timeout: nil)
    api_request(:put, path, headers: headers, params: params, auth: auth, timeout: timeout)
  end

  def put!(path, headers: nil, params: nil, auth: :bearer, timeout: nil)
    raise_response_error!(put(path, headers: headers, params: params, auth: auth, timeout: timeout), auth)
  end

  def delete(path, headers: nil, params: nil, auth: :bearer, timeout: nil)
    api_request(:delete, path, headers: headers, params: params, auth: auth, timeout: timeout)
  end

  def delete!(path, headers: nil, params: nil, auth: :bearer, timeout: nil)
    raise_response_error!(delete(path, headers: headers, params: params, auth: auth, timeout: timeout), auth)
  end

  private

  def api_request(method, path, headers:, params:, auth:, timeout: nil)
    request = http_client.headers(headers).timeout(request_timeouts(timeout))

    if auth == :bearer
      access_token = access_token!
      request = request.auth("Bearer #{access_token}")
    elsif auth == :basic
      request = request.basic_auth(user: client_id!, pass: client_secret!)
    elsif auth != :none
      raise ArgumentError, "invalid value for auth: #{auth.inspect}"
    end

    request = proxy_request(request, proxy) if proxy

    options = request_options(method, params)
    request = request.headers("Content-Type" => "application/json; charset=utf-8") if options.include?(:json)

    response = make_request_with_logging(request, method, path, options)

    Agero::Rsc::Response.new(response)
  end

  def request_timeouts(request_timeout)
    return request_timeout if request_timeout.is_a?(Numeric)

    request_timeout_hash = (request_timeout.is_a?(Hash) ? request_timeout : {})
    request_connect_timeout = (request_timeout_hash[:connect] || connect_timeout)
    request_read_timeout = (request_timeout_hash[:read] || read_timeout)
    request_write_timeout = (request_timeout_hash[:write] || write_timeout)
    if request_connect_timeout.nil? && request_read_timeout.nil? && request_write_timeout.nil?
      timeout
    else
      {
        connect: (request_connect_timeout || timeout),
        read: (request_read_timeout || timeout),
        write: (request_write_timeout || timeout),
      }
    end
  end

  # Raise an error only if the response code indicates an error status.
  # Otherwise just return self.
  def raise_response_error!(response, auth)
    return response unless response.error?

    Rails.logger.debug(
      "Agero::Rsc::API Response Error: #{response}"
    )

    response_class = Agero::Rsc::Error
    if response.code == 401
      if auth == :bearer
        response_class = Agero::Rsc::AccessTokenError

        Agero::Rsc::InvalidateToken.call(
          access_token_string: @access_token
        )
      elsif auth == :basic
        response_class = Agero::Rsc::CredentialsError
      end
    elsif response.code == 403
      response_class = Agero::Rsc::ForbiddenError
    end

    error = response_class.new(response.to_s)
    error.response = response
    raise error
  end

  def client_id!
    if client_id.present?
      client_id
    else
      raise ArgumentError, "client_id not set"
    end
  end

  def client_secret!
    if client_secret.present?
      client_secret
    else
      raise ArgumentError, "client_secret not set"
    end
  end

  def access_token!
    if @access_token.present?
      @access_token
    else
      raise ArgumentError, "access_token not set"
    end
  end

  def merged_headers(headers)
    headers ||= {}
    headers = headers.reverse_merge("User-Agent" => user_agent) if user_agent.present?
    headers = headers.reverse_merge(DEFAULT_HEADERS)
    headers
  end

  def proxy_request(request, proxy)
    return request if proxy.nil?
    if proxy.is_a?(String)
      username = nil
      password = nil
      if proxy.include?("@")
        auth, proxy = proxy.split("@", 2)
        username, password = auth.split(":", 2)
      end
      host, port = proxy.split(":", 2)
      proxy = { host: host, port: port.to_i, user: username, password: password }
    end
    request.via(proxy[:host], proxy[:port], proxy[:user], proxy[:password])
  end

  def request_options(method, params)
    options = {}
    options[:ssl_context] = ssl_context if ssl_context

    if params.present?
      params_method = ([:post, :put, :patch].include?(method) ? :json : :params)
      options[params_method] = params
    end

    options
  end

  # Perform the HTTP request while logging it to the audit_isscs table.
  def make_request_with_logging(request, method, path, options)
    audit_issc = nil
    token = APIAccessToken.find_by(access_token: @access_token) if @access_token.present?
    params = (options[:json] || options[:params])
    if token
      company_id = token.company_id
      contractorid = token.vendorid
      clientid = 'AGO'

      db_body = if params.nil? || params.is_a?(String)
                  params
                else
                  params.to_json
                end

      job = nil
      locationid = nil
      if path.start_with?('/dispatches/')
        dispatchid = path.split('/', 4)[2]
        issc_dispatch_req = IsscDispatchRequest.find_by(dispatchid: dispatchid)
        if issc_dispatch_req
          job = Job.find_by(issc_dispatch_request_id: issc_dispatch_req.id)
          locationid = issc_dispatch_req.issc.issc_location.locationid
        end
      end
      audit_issc = AuditIssc.create!(job: job,
                                     path: path,
                                     data: db_body,
                                     company_id: company_id,
                                     clientid: clientid,
                                     contractorid: contractorid,
                                     locationid: locationid,
                                     incoming: false,
                                     dispatchid: dispatchid,
                                     system: Issc::RSC_SYSTEM)
      Rails.logger.debug "Agero::Rsc::Client company:#{company_id}, token:#{@access_token[0..4]}"
    end

    Rails.logger.debug "Agero::Rsc::Client:#{" (Job #{job.id})" if job} #{method.to_s.upcase} #{path}, DATA: #{params.inspect}"

    start_time = Time.now
    response = request.send(method, url(path), options)
    elapsed_time = Time.now - start_time

    Rails.logger.debug "Agero::Rsc::Client: #{method.to_s.upcase} #{path}, RESPONSE: #{response.code}, TIME: #{(elapsed_time * 1000).round}ms, BODY: #{response}"

    if audit_issc
      audit_issc.update_columns(http_status_code: response.code.to_i, http_response: response.to_s)
    end

    response
  end

end
