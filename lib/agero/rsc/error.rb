# frozen_string_literal: true

# Error thrown by ! request calls to Agero::Rsc::Client.
class Agero::Rsc::Error < StandardError

  attr_accessor :response

end
