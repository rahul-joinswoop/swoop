# frozen_string_literal: true

# Centralized place for getting runtime settings for the application.
# These are things that are coming from the environment, but which require
# a bit a logic that needs to be tested.
class RuntimeSettings

  class << self

    # Return the current short git SHA. In production we read this from a Heroku environment
    # variable. In development, we get it from the git command line. If the revision is not
    # available, this method can return nil.
    def revision
      unless defined?(@revision)
        commit = ENV['HEROKU_SLUG_COMMIT'].to_s[0, 8].freeze
        @revision = commit if commit.present?
      end
      if @revision.nil? && Rails.env.development?
        revision = `git rev-parse HEAD 2>/dev/null`.chomp
        revision.present? ? revision[0, 8].freeze : nil
      else
        @revision
      end
    end

    # Number of threads puma should spawn.
    def puma_thread_count
      Integer(ENV.fetch("MAX_THREADS", "5"))
    end

    # Number of worker threads sidekiq should spawn.
    def sidekiq_thread_count
      Sidekiq.options[:concurrency]
    end

    # Calculate the number of threads that should be running that need a database connection.
    # This method is referenced from config/database.yml. The value will be different depending
    # on the binary that started up the application. We want this so that we don't open up
    # too many connections to the database when we don't need to.
    #
    # The value is determined by the script that started the process. If the script was either
    # sidekiq or puma, then it set the number of connections equal to the number of threads.
    # Otherwise, it will default to one connection.
    #
    # The number of connections can be augmented by setting the environment variable
    # `#{scriptname.upcase}_EXTRA_DB_CONNECTIONS`. This will add the specified number of connections
    # to the calculated number. So setting `PUMA_EXTRA_DB_CONNECTIONS=5` will allocate an
    # additional 5 database connections on top of the number allocated for the number of puma threads.
    #
    # Finally, the `DB_POOL` environment variable can be used to specify the minimum number of
    # connections for the pool.
    def max_db_connections
      process_max = 1
      app_binary = File.basename($PROGRAM_NAME)
      app_binary = "console" if app_binary == "rails" && Rails.const_defined?("Console")
      if app_binary == "puma"
        process_max = puma_thread_count
      elsif app_binary == "sidekiq"
        process_max = sidekiq_thread_count
      end
      process_max += Integer(ENV.fetch("#{app_binary.upcase}_EXTRA_DB_CONNECTIONS", 0))
      [process_max, Integer(ENV.fetch("DB_POOL", 1))].max
    end

  end

end
