# frozen_string_literal: true

# Helpers for specifying logic that should only be executed after any open
# database transactions are committed.
#
# Usage: `AfterCommit.call { your_code_here }`
#
# If there is no open database transaction, then the block will be executed inline.
# If there is an open transaction, then the block will not run until the transaction is
# committed.
module AfterCommit

  extend AfterCommitEverywhere

  # This module can be mixed into a Sidekiq Worker to add a method `perform_after_commit`
  # that will only schedule the job after any open database transaction has been committed.
  module Worker

    extend ActiveSupport::Concern

    module ClassMethods

      # Perform the job asynchronously after any open database transactions have been committed.
      # If no transactions are open, the job will be scheduled immediately.
      def perform_after_commit(*args)
        AfterCommit.call { perform_async(*args) }
      end

    end

  end

  class << self

    # Specify a block of code that will be executed only once the current database transaction
    # is committed. If there is no open transaction, then the block will be called immediately.
    # Note that you cannot rely on when the block of code will be called or retrieve any return
    # value from it.
    def call(&block)
      after_commit(&block)
      nil
    end

    # Specify a block of code that should be run if the current transaction is rolled back.
    # This method can only be called from inside a transaction.
    def rollback(&block)
      after_commit(&block)
      nil
    end

  end

end
