# frozen_string_literal: true

# Global configuration repository that automatically loads YAML files from the config directory.
# Configuration files can use ERB within them to provide dynamic values loaded from the environment.
# The configuration files should have a key matching the Rails environment.
#
# You can get a configuration file by calling Configuration.name where name is the base name
# of the configuration file. Each of the keys in the file can be reference from a Configuration
# object by calling config.name where name is a key in the config hash. Alternatively, you can
# use the `[]` method as an accessor as well.
class Configuration

  attr_reader :config

  class << self

    @@configurations = {}
    @@lock = Monitor.new

    # Get a configuration object with the specified name. The name is the base name
    # of the YAML file in the Rails config directory. The file must have the suffix `.yml`
    # If no such file exists, then nil will be returned.
    def [](name)
      name = name.to_s
      config = @@configurations[name]
      unless config
        values = load_config(name)
        return nil unless values

        @@lock.synchronize do
          config = @@configurations[name]
          # Doublecheck within the synchronize block to protect against race conditions.
          unless config
            config = new(values)
            @@configurations[name] = config
          end
        end
      end
      config
    end

    # Return a list of of all loaded configuration file names. This can be useful
    # for sanity checking.
    def names
      @@lock.synchronize { @@configurations.keys }
    end

    # Load a configuration YAML file (evaluating any embedded ERB) and return the hash
    # for the key matching the specified environment.
    def load_config(name, environment = Rails.env)
      filename = (name.end_with?('.yml') ? name : "#{name}.yml")
      config_file = Rails.root + "config" + filename
      return nil unless config_file.exist?

      config = YAML.load(ERB.new(config_file.read).result)
      config[environment] || {}
    end

    private

    def method_missing(method_name, *_)
      self[method_name.to_s] || super
    end

    def respond_to_missing?(method_name, *_)
      self[method_name.to_s].present? || super
    end

  end

  def initialize(config_hash)
    @config = deep_freeze(config_hash)
  end

  def [](name)
    config[name]
  end

  def keys
    config.keys
  end

  # Return an new Configuration with values merged with the provided hash.
  def merge(hash)
    self.class.new(config.merge(hash))
  end

  alias_method :to_hash, :config
  alias_method :to_h, :config

  private

  def method_missing(method_name, *_)
    config[method_name]
  end

  def respond_to_missing?(method_name, *_)
    config[method_name].present? || super
  end

  # Freeze the object and all descendants so application code doesn't mung it up.
  def deep_freeze(value)
    if value.is_a?(Hash)
      frozen_hash = HashWithIndifferentAccess.new
      value.each_pair { |k, v| frozen_hash[k] = deep_freeze(v) }
      value = frozen_hash
    elsif value.is_a?(Array)
      value = value.collect { |v| deep_freeze(v) }
    end
    value.freeze unless value.frozen?
    value
  end

end
