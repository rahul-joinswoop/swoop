# frozen_string_literal: true

# Logic to automatically run a job. This logic is only enabled when the
# JOB_SIMULATOR_ENABLED environment variable is set to true. The simulator
# will execute a series of transitions from a configuration and run them
# at specified intervals.
class JobSimulator

  NAME_MATCHER = /^Simulator Job\s+([^{]+)(\{.*)?$/i.freeze

  attr_reader :config

  class << self

    # Load a named simulator from a string that specifies that simulator and
    # any configurration overrides. The string must include a line that begins
    # with "Job Simulator" followed by the scenario name.
    #
    # Options can be specified as a JSON hash after the scenario name.
    # Options will be merge into the configuration specified in the scenario.
    # The transitions in the scenario cannot be overridden in this way, though.
    def simulator(name_with_options)
      return nil unless enabled?

      name, options = parse(name_with_options)
      config = configuration[name]
      return nil unless config

      new(config.merge(options).merge("scenario" => name))
    end

    # This flag is used to keep the job simulator from running in production.
    def enabled?
      ENV["JOB_SIMULATOR_ENABLED"] == "true"
    end

    def configuration
      unless defined?(@configuration)
        config_file = Rails.root + "config" + "job_simulator.yml"
        config = {}
        YAML.load(ERB.new(config_file.read).result).each do |key, values|
          config[key.to_s.strip.downcase] = values
        end
        @configuration = config
      end
      @configuration
    end

    private

    def parse(name_with_options)
      match = name_with_options.match(NAME_MATCHER)
      return nil unless match

      options = {}
      if match[2].present?
        options_json = match[2].strip
        if options_json.match(/\A\{.*\}\z/)
          begin
            options = JSON.load(options_json)
          rescue
            begin
              # Try parsing as YAML just to be nice about missing quotes around keys, etc.
              options = YAML.load(options_json)
            rescue
              Rails.logger.debug("Ignoring malformed JobSimulator JSON options: #{options_json}")
            end
          end
        end
        # Don't allow overriding transitions for security reasons.
        options.delete("transitions")
      end

      [match[1].strip.downcase, options]
    end

  end

  # Sidekiq worker that transitions a job for the simulator. This worker will
  # also enqueue the next worker to transition the job to the next state. If the
  # job has entered a "traveling" state (en route or towing), then a TravelWorker
  # will be enqueued instead to simulate the vehicle traveling between locations.
  class TransitionWorker

    include Sidekiq::Worker

    sidekiq_options retry: false, lock: :until_executing, unique_args: ->(args) { [args[0], args[1]] }, on_conflict: :replace

    def perform(job_id, transition, config)
      return unless JobSimulator.enabled?

      job = Job.not_deleted.find_by(id: job_id)
      return unless job

      simulator = JobSimulator.new(config)

      Job.transaction do
        if job.rescue_company.nil? && simulator.partner
          job.assign_rescue_company(simulator.site)
        end
        simulator.transition_job(job, transition)
        if job.dispatched?
          if job.rescue_company&.live?
            assign_truck_and_driver(job, simulator)
          end
        end
        job.save!
      end

      next_transition = simulator.next_transition(transition)
      if next_transition
        if track_travel?(job)
          start_truck_travel(job, next_transition, config)
        else
          Rails.logger.debug("JobSimulator: job #{job.id}; scheduling transition to #{next_transition} in #{simulator.status_delay}s")
          self.class.perform_in(simulator.status_delay, job_id, next_transition, config)
        end
      else
        Rails.logger.debug("JobSimulator: job #{job.id}; simulator finished")
      end
    rescue AASM::InvalidTransition
      Rails.logger.warn("JobSimulator: job #{job_id}; terminating cannot transition #{job.status} -> #{transition}")
    end

    private

    def assign_truck_and_driver(job, simulator)
      job.rescue_vehicle ||= simulator.available_truck(job.rescue_company)
      job.rescue_driver ||= simulator.available_driver(job.rescue_company)
      job.rescue_vehicle.update!(driver: job.rescue_driver)
    end

    def start_truck_travel(job, next_transition, config)
      Rails.logger.debug("JobSimulator: job #{job.id}; enqueuing travel worker")
      p1 = nil
      p2 = nil
      if job.enroute?
        p1 = job.rescue_company.hq.location
        p2 = job.service_location
      else
        p1 = job.service_location
        p2 = job.drop_location
      end
      TravelWorker.perform_async(job.id, [p1.lat, p1.lng], [p2.lat, p2.lng], 0, next_transition, config)
    end

    def track_travel?(job)
      return false unless job.rescue_vehicle && job.rescue_company&.live?
      if job.enroute?
        p1 = job&.rescue_company&.hq&.location
        p2 = job.service_location
        (p1 && p1.lat && p1.lng).present? && (p2 && p2.lat && p2.lng).present?
      elsif job.towing?
        p1 = job.service_location
        p2 = job.drop_location
        (p1 && p1.lat && p1.lng).present? && (p2 && p2.lat && p2.lng).present?
      else
        false
      end
    end

  end

  # Sidekiq worker to simulate a rescue vehicle traveling between locations.
  # This worker takes starting and ending coordinates as well as a segment index.
  #
  # The location of the rescue vehicle will be set to the point on the crow flies
  # path between the two coordinates corresponding to the segment. The number of segments
  # is determined by how long the travel time is in the simulator configuration.
  # Updates will be performed every 5 seconds so the number of segments is
  # travel time in seconds / 5.
  #
  # The worker will schedule another TravelWorker until the rescue vehicle has
  # reached it's destination. If the vehicle has reached the destination, then the
  # next transition will be scheduled with a TransitionWorker.
  class TravelWorker

    include Sidekiq::Worker

    sidekiq_options lock: :until_executing, unique_args: ->(args) { [args.first] }

    UPDATE_PERIOD = 5.seconds

    def perform(job_id, start_point, end_point, segment, next_transition, config)
      return unless JobSimulator.enabled?

      job = Job.not_deleted.find_by(id: job_id)
      return unless job

      unless job.enroute? || job.towing?
        Rails.logger.warn("JobSimulator: job #{job.id}; travel aborted with job status #{job.status}")
        return
      end

      vehicle = job.rescue_vehicle
      return unless vehicle

      simulator = JobSimulator.new(config)

      segment_count = simulator.travel_time(job) / UPDATE_PERIOD.to_i
      if segment >= 0 && segment <= segment_count
        lat, lng = segment_coordinates(start_point, end_point, segment_count, segment)
        UpdateVehicleLocationWorker.perform_async(vehicle.company_id, vehicle.id, lat, lng, Time.now.utc.iso8601)
        Rails.logger.debug("JobSimulator: job #{job.id}; vehicle #{vehicle.id} at #{lat}, #{lng}")
      end

      if segment < segment_count
        Rails.logger.debug("JobSimulator: job #{job.id}; scheduling #{job.status} travel update #{segment + 1} in #{UPDATE_PERIOD.to_i}s")
        self.class.perform_in(UPDATE_PERIOD, job_id, start_point, end_point, segment + 1, next_transition, config)
      else
        Rails.logger.debug("JobSimulator: job #{job.id}; #{job.status} travel finished")
        if next_transition
          Rails.logger.debug("JobSimulator: job #{job.id}; enqueuing transition to #{next_transition}")
          TransitionWorker.perform_async(job_id, next_transition, config)
        else
          Rails.logger.debug("JobSimulator: job #{job.id}; simulator finished")
        end
      end
    end

    private

    def segment_coordinates(start_point, end_point, segment_count, index)
      percent = (segment_count == 0 ? 0.0 : index.to_f / segment_count)
      distance = Geocoder::Calculations.distance_between(start_point, end_point, units: :mi)
      bearing = Geocoder::Calculations.bearing_between(start_point, end_point, method: :spherical)
      Geocoder::Calculations.endpoint(start_point, bearing, distance * percent, units: :mi).map { |coord| coord.round(9) }
    end

  end

  def initialize(config)
    raise "#{self.class.name} not enabled" unless self.class.enabled?
    @config = config
  end

  def start(job)
    if partner
      TransitionWorker.perform_in(status_delay, job.id, transitions.first, config)
    else
      Rails.logger.warn("JobSimulator: partner company not found with name #{config['partner'].inspect}")
    end
  end

  # Return number of seconds to transition between statuses.
  def status_delay
    config["status_delay"].present? ? config["status_delay"].to_i : 20
  end

  # Return number of seconds for truck to travel to and from the service location.
  # The configuration supports three variables to describe this. The en_route_time
  # will be used if the job is enroute, the towing_time will be used if the job
  # is towing. If these are not defined, then the generic travel_time will be used.
  # In any case, the travel time cannot be less than the status_delay time.
  def travel_time(job)
    time = (config["travel_time"].present? ? config["travel_time"].to_i : 60)
    if job.enroute? && config["en_route_time"].present?
      time = config["en_route_time"].to_i
    elsif job.towing? && config["towing_time"].present?
      time = config["towing_time"].to_i
    end
    time < status_delay ? status_delay : time
  end

  def transitions
    config["transitions"]
  end

  def partner
    company_name = config["partner"]
    return nil if company_name.blank?
    @partner ||= RescueCompany.not_deleted.where(name: company_name).order(:id).first
  end

  def site
    unless defined?(@site)
      site = partner.sites.find_by(name: config["site"]) if config["site"].present?
      @site = (site || partner.hq)
    end
    @site
  end

  def available_driver(rescue_company)
    return nil unless rescue_company

    done_statuses = JobStatus::DONE.map { |s| JobStatus::ID_MAP[s] }
    driver = rescue_company.users.not_deleted.detect do |user|
      if user.has_role?(:driver)
        job = user.jobs.last
        job.nil? || done_statuses.include?(job.status) || job.updated_at < 1.hour.ago
      end
    end

    unless driver
      ::User.transaction do
        phone = "#{rand(2..9)}#{rand(0..9)}#{rand(0..9)}55501#{rand(0..9)}#{rand(0..9)}"
        driver = rescue_company.users.build(first_name: "Driver", last_name: (rescue_company.users.count + 1).to_s, phone: phone, password: SecureRandom.hex)
        driver.add_role(:driver)
        driver.add_role(:rescue)
        driver.save!
      end
    end

    driver
  end

  def available_truck(rescue_company)
    return nil unless rescue_company

    done_statuses = JobStatus::DONE.map { |s| JobStatus::ID_MAP[s] }
    truck = rescue_company.vehicles.not_deleted.detect do |vehicle|
      job = vehicle.jobs.last
      job.nil? || done_statuses.include?(job.status) || job.updated_at < 1.hour.ago
    end

    truck ||= rescue_company.vehicles.create!(name: "Truck #{rescue_company.vehicles.count + 1}")

    truck
  end

  def next_transition(transition)
    index = transitions.index(transition)
    transitions[index + 1] if index
  end

  def transition_job(job, trans)
    if respond_to?("transition_#{trans}")
      send("transition_#{trans}", job)
    else
      aasm_events = FleetManagedJob.aasm.events.map(&:name).map(&:to_s)
      if aasm_events.include?(trans)
        job.send(trans)
      else
        job.status = trans
      end
    end
  end

  def transition_assign_and_dispatch(job)
    job.add_sta(15.minutes.from_now)
    job.swoop_auto_assign
    job.swoop_auto_dispatch
  end

  def transition_partner_accepted(job)
    job.bid(15.minutes.from_now)
    job.partner_accepted
  end

end
