# frozen_string_literal: true

class Constant

  METERS_MILES = 0.00062137
  MILES_METERS = 1609.34708789
  MILES_PER_LAT = 69.0
  MILES_PER_LNG = 55.0 # ROUGHLY AT 40DEG

end
