# frozen_string_literal: true

# Log job arguments, timing, and errors to the Rails log.
# You can control the logging with the `:logging` sidekiq option.
# To turn off logging entirely, use `sidekiq_options logging: false`
# To turn off logging the job arguments, use `sidekiq_options logging: :without_args`
module Sidekiq
  class LogJobInfoMiddleware

    LONG_RUNNING_WORKER_THRESHOLD = Float(ENV.fetch("LONG_RUNNING_WORKER_THRESHOLD", "7"))

    def call(worker, job, queue)
      options = worker.class.sidekiq_options_hash
      long_running_threshold = options[:long_running_threshold] if options
      long_running_threshold ||= LONG_RUNNING_WORKER_THRESHOLD
      mode = options["logging"] if options
      if mode == false
        yield
      else
        info = worker_info(job, mode)
        logger = ::Rails.logger
        t = Time.now

        begin
          logger.debug("Worker start: #{info}")
          yield
        rescue => e
          logger.error("Worker error: #{e.inspect}\n  #{e.backtrace.join("  \n")}")
          raise e
        ensure
          worker_run_time = ((Time.now - t) * 1000).round
          logger.info("Worker finish: #{info} in #{worker_run_time}ms")
          if worker_run_time >= long_running_threshold * 1000
            Rollbar.warn("Long running worker: #{worker.class} took #{worker_run_time}ms")
          end
        end
      end
    end

    private

    def worker_info(job, mode)
      args = nil
      if mode.to_s == "without_args"
        args = "---"
      else
        args = Array(job['args']).map { |arg| arg.inspect }.join(", ")
      end

      info = "#{job['class']}.perform(#{args})"
      retry_count = job['retry_count'].to_i
      if retry_count > 0
        info = "#{info}; retry count: #{retry_count}"
      end

      info
    end

  end
end
