# frozen_string_literal: true

# The ActiveRecord query cache is enabled by default starting with Rails 5.
# This Sidekiq middleware allows you to turn if off with a simple declaration
# in a worker's sidekiq_options:
#
#   sidekiq_options query_cache: false
#
# Any workers that might load a lot of records should use either `find_each`
# or `find_in_batches` and disable the query cache so that they don't cause
# to memory bloat.
module Sidekiq
  class ActiveRecordQueryCacheControlMiddleware

    def call(worker, job, queue)
      options = worker.class.sidekiq_options_hash
      if options && options["query_cache"] == false
        ActiveRecord::Base.uncached { yield }
      else
        yield
      end
    end

  end
end
