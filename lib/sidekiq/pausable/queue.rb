# frozen_string_literal: true

module Sidekiq
  module Pausable
    # This overwrites the behavior in Sidekiq to expose pausable behavior on queues.
    module Queue

      def self.included(base)
        # Use alias_method since these methods may already be defined in Sidekiq::Queue
        # and simply including them won't override those implementations.
        base.alias_method :pause!, :pausable_pause!
        base.alias_method :unpause!, :pausable_unpause!
        base.alias_method :paused?, :pausable_paused?
      end

      def pausable_pause!
        Sidekiq.redis { |conn| conn.sadd(Sidekiq::Pausable::PAUSED_QUEUES_KEY, "queue:#{name}") }
      end

      def pausable_unpause!
        Sidekiq.redis { |conn| conn.srem(Sidekiq::Pausable::PAUSED_QUEUES_KEY, "queue:#{name}") }
      end

      def pausable_paused?
        Sidekiq.redis { |conn| conn.sismember(Sidekiq::Pausable::PAUSED_QUEUES_KEY, "queue:#{name}") }
      end

    end
  end
end
