# frozen_string_literal: true

module Sidekiq
  module Pausable
    module Web

      class << self

        def install!
          Sidekiq::Web.register self
          Sidekiq::Web.tabs['Pause'] = 'pausable'
        end

        def registered(app)
          app.get('/pausable') do
            @queues = Sidekiq::Queue.all
            erb Sidekiq::Pausable::Web.template
          end

          app.post('/pausable/pause/:queue') do
            Sidekiq::Queue.new(params['queue']).pause!
            sleep(1) # Give the queue a chance to pause
            redirect("#{root_path}pausable")
          end

          app.post('/pausable/unpause/:queue') do
            Sidekiq::Queue.new(params['queue']).unpause!
            sleep(1) # Give the queue a chance to unpause
            redirect("#{root_path}pausable")
          end
        end

        def template
          <<~ERB
            <h3><%= t('Queues') %></h3>

            <div class="table_container">
              <table class="queues table table-hover table-bordered table-striped table-white">
                <thead>
                  <th><%= t('Queue') %></th>
                  <th><%= t('Size') %></th>
                  <th><%= t('Latency') %></th>
                  <th><%= t('Actions') %></th>
                </thead>
                <% @queues.each do |queue| %>
                  <tr>
                    <td>
                      <a href="<%= root_path %>queues/<%= CGI.escape(queue.name) %>"><%= h queue.name %></a>
                    </td>
                    <td><%= number_with_delimiter(queue.size) %> </td>
                    <td><%= number_with_delimiter(queue.latency.round(2)) %> </td>
                    <td>
                      <% if queue.paused? %>
                        <form action="<%=root_path %>pausable/unpause/<%= CGI.escape(queue.name) %>" method="post">
                          <%= csrf_tag %>
                          <button type="submit" class="btn btn-danger btn-xs">Unpause</button>
                        </form>
                      <% else %>
                        <form action="<%=root_path %>pausable/pause/<%= CGI.escape(queue.name) %>" method="post">
                          <%= csrf_tag %>
                          <button type="submit" class="btn btn-xs">Pause</button>
                        </form>
                      <% end %>
                    </td>
                  </tr>
                <% end %>
              </table>
            </div>
          ERB
        end

      end

    end
  end
end
