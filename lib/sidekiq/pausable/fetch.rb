# frozen_string_literal: true

require "sidekiq/fetch"

module Sidekiq
  module Pausable
    class Fetch < Sidekiq::BasicFetch

      def queues_cmd
        queues = super
        paused = paused_queues
        if paused.empty?
          queues
        else
          queues.reject { |queue| paused.include?(queue) }
        end
      end

      private

      # Fetch a list of queues that are paused. The value is memoized and refreshed
      # every second. No mutexes are needed because the assignments should be atomic.
      def paused_queues
        paused = @paused_queues if defined?(@paused_queues)
        if paused.nil? || !defined?(@next_pause_check) || @next_pause_check.to_f <= Time.now.to_f
          paused_list = Sidekiq.redis { |conn| conn.smembers(Sidekiq::Pausable::PAUSED_QUEUES_KEY) }
          paused = Set.new(paused_list).freeze
          @next_pause_check = Time.now.to_f + 1
          @paused_queues = paused
        end
        paused
      end

    end
  end
end
