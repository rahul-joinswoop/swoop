# frozen_string_literal: true

# Middleware that will persist the RequestContext from web requests
# through to workers spawned by the web request. This information can
# then be used for audit trails and logging.
module Sidekiq
  module RequestContextMiddleware

    class Client

      def call(worker_class, job, queue, redis_pool = nil)
        context = RequestContext.current
        if context
          job["request_context"] = {
            "uuid" => context.uuid,
            "source_application_id" => context.source_application_id,
            "user_id" => context.user_id,
          }
        end
        yield
      end

    end

    class Server

      def call(worker, job, queue)
        request_context = job["request_context"]
        if request_context.is_a?(Hash)
          context = RequestContext.new(
            uuid: request_context["uuid"],
            user_id: request_context["user_id"],
            source_application_id: request_context["source_application_id"],
          )
          RequestContext.use(context) { yield }
        else
          yield
        end
      end

    end

  end
end
