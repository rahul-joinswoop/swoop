# frozen_string_literal: true

# Consistently tag a worker throughout it's life
# Deterministically create a UUID based on the worker and sidekiq job id
# to have easily searchable Sidekiq logs consistent across all retries of a job.
module Sidekiq
  class WorkerUuidMiddleware

    def call(worker, job, queue)
      jid = job["jid"]
      log_tag = Digest::UUID.uuid_v5(worker.class.name, jid)
      ::Rails.logger.tagged(log_tag) { yield }
    end

  end
end
