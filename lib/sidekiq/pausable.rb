# frozen_string_literal: true

# Patch to Sidekiq 5 to implement queue pausing. This feature is
# available in Sidekiq Pro.
require "sidekiq/fetch"
require "sidekiq/api"
require_relative "./pausable/fetch"
require_relative "./pausable/queue"
require_relative "./pausable/web"

module Sidekiq
  module Pausable

    PAUSED_QUEUES_KEY = "ext:paused_queues"

    class << self

      def install!
        Sidekiq::Queue.include(Sidekiq::Pausable::Queue)
      end

    end

  end
end
