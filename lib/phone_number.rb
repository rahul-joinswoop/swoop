# frozen_string_literal: true

module PhoneNumber

  # adapted from https://stackoverflow.com/a/23299989
  E164_REGEX = /\A\+[1-9]\d{1,14}\z/.freeze

end
