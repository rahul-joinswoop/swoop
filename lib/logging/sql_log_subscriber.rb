# frozen_string_literal: true

# Custom log subscriber suitable for production environment that only logs SQL statements that take too long.
module Logging

  class LongRunningQuery < StandardError
  end

  class SqlLogSubscriber < ActiveSupport::LogSubscriber

    LONG_RUNNING_QUERY_MILLISECONDS = Integer(ENV.fetch("LONG_RUNNING_QUERY_MILLISECONDS", "1000"))
    ROLLBAR_LONG_RUNNING_QUERY_MILLISECONDS = Integer(ENV["ROLLBAR_LONG_RUNNING_QUERY_MILLISECONDS"] || LONG_RUNNING_QUERY_MILLISECONDS)

    # Don't log columns that might contain sensitive data
    DO_NOT_LOG_COLUMNS = /email|phone|name|password|credit|token|uuid/.freeze

    def sql(event)
      return if event.duration < LONG_RUNNING_QUERY_MILLISECONDS && LONG_RUNNING_QUERY_MILLISECONDS >= 0

      payload = event.payload
      name = payload[:name]
      return if name == "CACHE" || name == "SCHEMA" || name == "EXPLAIN"

      sql = payload[:sql]
      binds = nil
      payload_binds = payload[:binds]
      if payload_binds.present?
        binds = ' ' + payload_binds.map { |col| [col.name, (col.name.to_s.match(DO_NOT_LOG_COLUMNS) ? 'FILTERED' : col.value)] }.inspect
      end

      long_running_query(sql, binds, event.duration)
    end

    def long_running_query(sql, binds, duration)
      sql = "#{sql.gsub(/\s+/, ' ')}#{binds}"
      warn("Long running SQL: (#{duration.round(1)}ms) #{sql}")
      if duration >= ROLLBAR_LONG_RUNNING_QUERY_MILLISECONDS && ROLLBAR_LONG_RUNNING_QUERY_MILLISECONDS >= 0
        Rollbar.warn(Logging::LongRunningQuery.new("FROM #{table_names(sql).join(', ')} #{duration.round(1)}ms"), sql: sql)
      end
    end

    def logger
      Rails.logger
    end

    private

    def table_names(sql)
      tables = []
      sql.scan(/\b(?:from|join)\s+"?([a-z0-9_]+)/i) do |table|
        tables << table
      end
      tables << "<unknown>" if tables.empty?
      tables.flatten.uniq
    end

  end

end
