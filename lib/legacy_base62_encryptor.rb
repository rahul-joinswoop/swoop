# frozen_string_literal: true

#
# code based on:
#   https://github.com/rails/globalid/blob/master/lib/global_id/global_id.rb
#   https://github.com/rmosolgo/graphql-ruby/blob/master/lib/graphql/schema/unique_within_type.rb

require 'base62'

class LegacyBase62Encryptor < Base62Encryptor

  class Error < StandardError; end

  DEFAULT_SEPARATOR = "-" unless defined?(DEFAULT_SEPARATOR)

  def initialize(key:, iv:, separator: nil)
    super key: key, iv: iv
    @separator = separator || DEFAULT_SEPARATOR
  end

  attr_reader :separator

  def decrypt(data)
    super.split(separator)
  end

  def encrypt(*data)
    cryptor = cipher(:encrypt)
    data = *data.flatten

    if data.any? { |d| d.to_s.include?(separator) }
      # This shouldn't ever happen - raise an error because any attempt to decrypt
      # this value will result in bad data
      e = Error.new("Plaintext contains separator #{separator}")
      Rollbar.error(e, data)
      raise e
    end

    # rubocop:disable Rails/SaveBang
    (cryptor.update(data.join(separator)) + cryptor.final).unpack("H*").first.to_i(16).base62_encode
    # rubocop:enable Rails/SaveBang
  end

end
