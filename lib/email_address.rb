# frozen_string_literal: true

module EmailAddress

  EMAIL_REGEX = /\A.+@.+\z/.freeze

end
