# frozen_string_literal: true

# code based on:
#   https://github.com/rails/globalid/blob/master/lib/global_id/global_id.rb
#   https://github.com/rmosolgo/graphql-ruby/blob/master/lib/graphql/schema/unique_within_type.rb

require 'base62'

class Base62Encryptor

  class Error < StandardError; end

  def initialize(key:, iv:)
    @key = [key].pack("H*")
    @iv = [iv].pack("H*")
  end

  attr_reader :key, :iv

  # our cipher expects a blocksize of 32 bytes
  BLOCKSIZE = 32 unless defined?(BLOCKSIZE)

  def decrypt(data)
    # go from base62-encoded to a hex string
    decoded = data.base62_decode.to_s(16)

    # make sure this block is padded to a multiple of BLOCKSIZE, otherwise we
    # get spurious openssl errors
    decoded = decoded.rjust((Float(decoded.length) / BLOCKSIZE).ceil * BLOCKSIZE, "0")

    encrypted = [decoded].pack("H*")
    decryptor = cipher(:decrypt)
    # rubocop:disable Rails/SaveBang
    decryptor.update(encrypted) + decryptor.final
    # rubocop:enable Rails/SaveBang
  end

  def encrypt(data)
    cryptor = cipher(:encrypt)
    # rubocop:disable Rails/SaveBang
    (cryptor.update(data) + cryptor.final).unpack("H*").first.to_i(16).base62_encode
    # rubocop:enable Rails/SaveBang
  end

  def inspect
    "#<#{self.class.name}:#{object_id}>"
  end

  private def cipher(mode)
    cipher = OpenSSL::Cipher.new('aes-256-cbc')
    cipher.send(mode)
    cipher.key = key
    cipher.iv = iv
    cipher
  end

end
