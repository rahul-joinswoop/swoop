# frozen_string_literal: true

module UUID

  # via https://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex
  UUID_REGEX = /\A[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89AB][0-9a-f]{3}-[0-9a-f]{12}\Z/i.freeze

  module_function def uuid?(uuid)
    uuid.to_s.match?(UUID_REGEX)
  end

end
