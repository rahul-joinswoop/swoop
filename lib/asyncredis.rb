# frozen_string_literal: true

require 'thread'
require 'em-hiredis'

# Wrapper around an EventMachine backed redis connection for publishing and subscribing
# to the web socket.
class AsyncRedis

  @@instance = nil
  @@mutex = Mutex.new

  delegate :zadd, :zremrangebyrank, :zcard, to: :async_redis

  class << self

    def instance
      unless @@instance
        @@mutex.synchronize do
          # Check again inside the synchronized block to guard against race condition
          # where the instance was set while this thread was waiting on the mutex.
          @@instance ||= AsyncRedis.send(:new)
        end
      end
      @@instance
    end

    private :new

  end

  def initialize
    start_redis
  end

  def publish(channel, msg)
    # Rails.logger.debug "REDIS: publishing #{msg} to channel #{channel} "
    EM.schedule do
      async_redis.publish(channel, msg)
      # Rails.logger.debug "REDIS: published  to channel #{channel} "
    end
  rescue Exception => e
    Rails.logger.error "REDIS: publish: Inner Inner Caught Exception in EM.run  thread #{e}"
    Rails.logger.error e.backtrace.join("\n")
    Rollbar.error(e)
  end

  def unsubscribe(channel)
    EM.schedule do
      Rails.logger.debug "REDIS: unsubscribing #{channel}"
      pubsub = async_redis.pubsub
      pubsub.punsubscribe(channel)
    rescue Exception => e
      Rails.logger.error "REDIS: unsubscribe: Exception #{e}"
      Rails.logger.error e.backtrace.join("\n")
      Rollbar.error(e)
    end
  end

  # websockets must implement a send method, to which messages on this channel will be passed
  def subscribe(channel, websockets)
    Rails.logger.debug "REDIS: AsyncRedis subscribe called on #{channel}"
    EM.schedule do
      pubsub = async_redis.pubsub
      pubsub.psubscribe(channel) do |chan, msg|
        # Rails.logger.debug "REDIS:  Async received msg from redis on chan: #{chan}, msg: #{msg}, msgclass: #{msg.class}"
        websockets.publish(chan, msg)
      # Rails.logger.debug "REDIS: Async SENT"
      rescue Exception => e
        Rails.logger.error "REDIS: Subscribe Inner Caught Exception in EM.run subscriber thread #{e}"
        Rails.logger.error e.backtrace.join("\n")
        Rollbar.error(e)
      end
    end
    Rails.logger.debug "REDIS: AsyncRedis subscribe finished"
  rescue Exception => e
    Rails.logger.error "REDIS: Caught Exception in EM.run subscriber thread #{e}"
    Rails.logger.error e.backtrace.join("\n")
    Rollbar.error(e)
  end

  private

  def start_redis
    @async_redis = nil
    error = nil

    thread = Thread.new do
      EM.run do
        @async_redis = EM::Hiredis.connect(RedisClient.url(:default))
        Rails.logger.info("AsyncRedis started")
      rescue => e
        error = e
      end
    rescue => e
      error = e
    end

    # Wait for the thread to start up and initialize the redis connection
    while @async_redis.nil? && error.nil? && thread.alive?
      sleep(0.001)
    end

    if error
      raise error
    elsif @async_redis.nil?
      raise "AsyncRedis thread unexpectedly died"
    end
  end

  attr_reader :async_redis

end
