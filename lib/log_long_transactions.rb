# frozen_string_literal: true

# This module hooks into the methods that begin and end database transactions
# and reports stack traces on any transactions kept open for too long.
# Once identified, spots in the code that keep open long running transactions
# should be fixed to keep transaction windows as short as possible.
module LogLongTransactions

  class Warning < StandardError
  end

  class << self

    def max_transaction_seconds
      @max_transaction_seconds || 0.0
    end

    def max_transaction_seconds=(max)
      @max_transaction_seconds = max.to_f
    end

  end

  def begin_transaction(*args)
    if open_transactions == 0
      @transaction_start_time = Time.now
    end
    super
  end

  def commit_transaction(*args)
    super
  ensure
    report_long_running_transaction if open_transactions == 0
  end

  def rollback_transaction(*args)
    super
  ensure
    report_long_running_transaction if open_transactions == 0
  end

  private

  def report_long_running_transaction
    return unless @transaction_start_time && LogLongTransactions.max_transaction_seconds > 0
    elapsed_time = Time.now - @transaction_start_time
    @transaction_start_time = nil
    if elapsed_time >= LogLongTransactions.max_transaction_seconds
      message = "Database transaction open for #{elapsed_time.round(3)}s"

      line_matcher = /\/app\/(controllers|workers|jobs|graphql|mailers)\/.*\.rb/
      if caller.detect { |line| line.match(line_matcher) }
        message = "#{message} from #{$LAST_MATCH_INFO[0]}"
      end
      Rollbar.warn(Warning.new(message))
    end
  rescue => e
    Rails.logger.warn(e) if Rails.logger
  end

end

unless ActiveRecord::ConnectionAdapters::TransactionManager.include?(LogLongTransactions)
  ActiveRecord::ConnectionAdapters::TransactionManager.prepend(LogLongTransactions)
end

# Set this environment variable to the default maximum number of seconds allowed for a transaction.
LogLongTransactions.max_transaction_seconds = ENV.fetch("MAX_TRANSACTION_SECONDS", "1.0").to_f
