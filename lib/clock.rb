# frozen_string_literal: true

require 'clockwork'
require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)

module Clock

  include Clockwork

  issc_heartbeat_interval = ENV.fetch('ISSC_HEARTBEAT_INTERVAL', 60)
  every(issc_heartbeat_interval.seconds, 'Queing ISSC Connection Check') do
    IsscConnectionCheck.perform_async
  end

  every(1.minutes, "Fix RSC logins") { Agero::Rsc::LoginWorker.perform_async }
  every(1.minutes, "Fix RSC logouts") { Agero::Rsc::LogoutWorker.perform_async }

  if Rails.env.production?
    every(2.minutes, 'Queueing TOA') { Delayed::Job.enqueue Toa.new }

    # do this with plenty of time before the StorageInvoices job runs
    every(1.day, 'Disabling storage auto_calculation for old jobs', at: "23:45") do
      DisableStorageAutoCalculationWorker.perform_async(Date.current)
    end

    every(1.hour, 'Queueing storage invoices', at: '**:01') do
      Delayed::Job.enqueue StorageInvoices.new
    end
    every(1.minutes, 'Queueing Review emails') { Delayed::Job.enqueue EmailReviews.new }
    every(1.minutes, 'Queueing Check Site Times') { CheckSiteTimesWorker.perform_async }
    every(5.minutes, 'Queueing Check Issc Statuses') { IsscCheckStatus.perform_async }

    every(61.seconds, 'Queueing JobStatus Check') { JobStatusNotifications.perform_async }
    every(61.seconds, 'Queueing JobStatusAlert Check') { JobStatusAlert.perform_async }
    every(61.seconds, 'Queueing SmsAlertWorker Check') { SmsAlertWorker.perform_async }

    every(1.day, 'Queueing EndOfSlackUpdate Check', at: '03:59') do
      EndOfDaySlackUpdate.perform_async
    end

    every(5.minutes, "Checking stale truck locations") { RescueVehicleLocationAlert.perform_async }

    every(5.minutes, "Checking ISSC connection status") { IsscServiceRequesterStatus.perform_async }

    every(5.minutes, "Checking ISSC Fix Unregistered") { IsscFixUnregistered.perform_async }

    every(1.minutes, "Publishing GPS to ISSC") { IsscDispatchStatusGps.perform_async }
    every(1.minutes, "Publishing GPS to RSC") { Agero::Rsc::TrackActiveJobsLocationsWorker.perform_async }

    every(15.minutes, "Sync dashboards with Looker") { LookerFetchDashboards.perform_async }

    every(1.minutes, "Fix logins") { IsscFixLogins.perform_async }
    every(1.minutes, "Fix logouts") { IsscFixLogouts.perform_async }

    every(1.day, 'Queueing Redis Cleanup', at: '08:00') { RedisCleanup.perform_async }

    every(10.minutes, "Check and alert Stripe charges in indeterminate state") do
      Stripe::CheckChargesInIndeterminateStatusWorker.perform_async
    end

    every(1.day, 'Calculate NPS averages into ratings table', at: '00:01') { PopulateRatingsWorker.perform_async }

    # Lincoln Reports
    every(1.day, "Generate FMC360 report for Lincoln", at: '11:00', tz: "America/New_York") { Fmc360ReportWorker.perform_async(Date.yesterday) }
    every(1.week, "Generate RDA report for Lincoln", at: 'Monday 05:00', tz: "America/New_York") { LincolnRdaReportWorker.perform_async(Date.yesterday) }

    every(1.day, "Check and fix invoices", at: '01:00', tz: "America/New_York") do
      UpdateInvoiceWorker.perform_async
    end
  elsif ENV.key? 'CANCEL_OLD_JOBS'
    every(1.day, 'Canceling stale jobs') { CancelOldJobsWorker.perform_async }
  end

  if ENV['ISSC_CANCEL_OVERDUE_JOBS']
    every(10.minutes, 'Queueing Cancel Issc Overdue Jobs') do
      IsscCancelOverdueJobs.perform_async
    end
  end

end
