# frozen_string_literal: true

# fb0de0f979504b73bd32e4cfa5be4e17
require 'pagerduty'
class PagerdutyAlert

  class << self

    def create(description, incident_key: gen_incident_key, details: {})
      return nil unless enabled?

      Rails.logger.debug "PD Incident(#{incident_key}) - Create: #{description}"
      client.trigger(description,
                     incident_key: incident_key,
                     client_url: ENV['SITE_URL'],
                     details: details)
    end

    def resolve(incident)
      Rails.logger.debug "PD Incident(#{incident.incident_key}) - resolved"
      incident.resolve
    end

    private

    def gen_incident_key
      SecureRandom.hex(12)
    end

    def client
      Pagerduty.new(ENV['PAGERDUTY_TOKEN'])
    end

    def enabled?
      ENV['PAGERDUTY_TOKEN'].present?
    end

  end

end
