# frozen_string_literal: true

module ReportFilters

  ALL = 'All'
  CHARGED = 'Charged'
  REFUNDED = 'Refunded'
  SWOOP_CHARGES = 'Swoop Charges'

  TRANSACTION_TYPE_FILTERS = [
    { text: ALL, value: '0' },
    { text: Payment::PAYMENT, value: '1' },
    { text: Payment::DISCOUNT, value: '2' },
    { text: Payment::REFUND, value: '3' },
    { text: Payment::WRITE_OFF, value: '4' },
    { text: SWOOP_CHARGES, value: '5' },
  ].freeze

  PAYMENT_STATUS_FILTERS = [
    { text: ALL, value: '0' },
    { text: CHARGED, value: '1' },
    { text: REFUNDED, value: '2' },
  ].freeze

  def transaction_type(user, company_report)
    { label: "Transaction Type:", options: TRANSACTION_TYPE_FILTERS, type: "options" }
  end

  def payment_status(user, company_report)
    { label: "Payment Status:", options: PAYMENT_STATUS_FILTERS, type: "options" }
  end

end
