# frozen_string_literal: true

require 'open-uri'

module Utils

  include ActionView::Helpers::NumberHelper

  SWOOP_OPERATIONS_EMAIL = 'operations@joinswoop.com' unless
    defined? SWOOP_OPERATIONS_EMAIL

  SWOOP_ACCOUNT_CREATED_EMAIL = 'accountcreated@joinswoop.com' unless
    defined? SWOOP_ACCOUNT_CREATED_EMAIL

  unless defined? STRIPE_SUPPORT_EMAIL_ADDRESS
    STRIPE_SUPPORT_EMAIL_ADDRESS =
      ENV['STRIPE_NOTIFICATION_EMAIL'] || 'stripesupport@joinswoop.com'
  end

  def self.upcoming_time(time, offset)
    hour_offset = offset.hours
    beginning_of_day = time.beginning_of_day
    offset_today = beginning_of_day + hour_offset

    return offset_today if time <= offset_today
    beginning_of_day.tomorrow + hour_offset
  end

  def emails(users)
    emails = []
    users.each do |user|
      if user.email
        emails << user.email
      end
    end
    emails
  end

  def email_valid?(email_address)
    begin
      email = Mail::Address.new(email_address)
    rescue Mail::Field::IncompleteParseError
      nil
    end

    !!(email&.address =~ /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/)
  end

  def tz_from_or_la(timezone)
    if timezone
      timezone
    else
      'America/Los_Angeles'
    end
  end

  def format_us_phone_number(phone)
    return nil unless phone

    phone.gsub(/(\+\d+)(\d\d\d)(\d\d\d)(\d\d\d\d)/, '\2\3\4')
  end

  # ENG-8568: Utils#extract can likely be replaced with dict.to_h.slice(*keys)
  # PDW says "i think this whole thing can be replaced by dict.to_h.slice(*keys)"
  def extract(keys, dict)
    # Guard here if dict is nil
    # then just return empty hash cause things hasnt changed
    return {} if dict.blank?

    ret = {}
    keys.each do |key|
      if dict.key?(key)
        ret[key] = dict[key]
      end
    end
    ret
  end

  def convert_hash(hash, path = "")
    hash.each_with_object({}) do |(k, v), ret|
      key = path + k

      if v.is_a? Hash
        ret.merge! convert_hash(v, key + ".")
      else
        ret[key] = v
      end
    end
  end

  def remove_param(param, hash)
    ret = hash[param]
    if ret
      hash.delete(param)
    end
    Rails.logger.debug("ret:#{ret},#{hash}")
    ret
  end

  class APIException < StandardError

    attr_reader :reason
    def initialize(reason)
      @reason = reason
    end

  end

  def attributize(args, keys)
    keys.each do |key|
      if args[key]
        args["#{key}_attributes".to_sym] = args[key]
        args.delete(key)
      end
    end
  end

  def nwp2(num)
    number_with_precision(num, precision: 2)
  end

  def nwp1(num)
    number_with_precision(num, precision: 1)
  end

  def round2(num)
    num.round(2)
  end

  def parse_param(param)
    if param == 'null'
      param = nil
    end
    param
  end

  def parse_bool(params, key)
    param = params[key]
    if param == "true"
      return true
    elsif param == "false"
      return false
    else
      raise ArgumentError, "Parameter must be bool: #{key}"
    end
  end

  def human_localtime(utc_dttm, time_zone)
    utc_dttm.in_time_zone(time_zone).strftime("%m/%d/%Y %H:%M (%Z)")
  end

  def human_localdate(utc_dttm, time_zone)
    utc_dttm.in_time_zone(time_zone).strftime("%d %b %Y (%Z)")
  end

  # distance in meters of 2 gps points
  def distance_in_meters(loc1, loc2)
    rad_per_deg = Math::PI / 180 # PI / 180
    rkm = 6371                  # Earth radius in kilometers
    rm = rkm * 1000             # Radius in meters

    dlat_rad = (loc2[0] - loc1[0]) * rad_per_deg # Delta, converted to rad
    dlon_rad = (loc2[1] - loc1[1]) * rad_per_deg

    lat1_rad, _lon1_rad = loc1.map { |i| i * rad_per_deg }
    lat2_rad, _lon2_rad = loc2.map { |i| i * rad_per_deg }

    a = Math.sin(dlat_rad / 2)**2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin(dlon_rad / 2)**2
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

    rm * c # Delta in meters
  end

  def pp_ams(serialization)
    puts JSON.pretty_generate(serialization.as_json)
  end

  def swoop_operations_email_as(company_name)
    "#{company_name} <#{SWOOP_OPERATIONS_EMAIL}>"
  end

  def self.slack_rating_text(tag, text)
    "\t#{tag}:\t#{text}\n"
  end

  def self.slack_rating_stars(tag, tag_rating, scale)
    stars = ([':star:'] * tag_rating.to_i).join(' ')
    "\t#{tag}:\t#{tag_rating.to_i}/#{scale} #{stars}\n"
  end

end
