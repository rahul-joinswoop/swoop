# frozen_string_literal: true

# Wrapper around the Policy Lookup Service
module PolicyLookupService
  class Client

    include HTTPClient

    class Error < StandardError

      attr_accessor :response

    end

    class AuthorizationError < Error; end
    class ForbiddenError < Error; end
    class NotFoundError < Error; end

    def initialize(client_code)
      raise ArgumentError, "client code is required" if client_code.blank?
      @client_code = client_code
    end

    def get(path, params: nil)
      url = base_url.chomp("/")
      http_get("#{url}/#{@client_code}#{path}", params: params, headers: { "Authorization" => "Bearer #{access_token}" })
    end

    def pii(url)
      http_get(url)
    end

    def search(params)
      params = params.with_indifferent_access
      if params[:vin].present? || params[:license_number].present? || params[:unit_number].present?
        get("/vehicles", params: params)
      elsif params[:last_name].present? && params[:postal_code].present?
        get("/customers", params: params)
      # we're not currently requiring a country on the job form and only use US numbers.
      # Once we setup our first client from another country, we'll double check how it
      # should be used.
      elsif params[:phone_number].present? # && params[:country].present?
        get("/customers", params: params)
      elsif params[:policy_number].present?
        get("/policies", params: params)
      else
        raise ArgumentError, "Expected one of: [vin, license_number, last_name&postal_code, phone_number&country]"
      end
    end

    def policy(uuid)
      get("/policies/#{uuid}")
    end

    def vehicle(uuid)
      get("/vehicles/#{uuid}")
    end

    def customer(uuid)
      get("/customers/#{uuid}")
    end

    def processing_statuses(last_id = nil)
      params = {}
      params[:last_id] = last_id if last_id.present?
      get("/processing_statuses.json", params)["processing_statuses"]
    end

    private

    def raise_response_error!(response)
      return if response.code >= 200 && response.code < 300

      response_class = Error
      if response.code == 401
        response_class = AuthorizationError
      elsif response.code == 403
        response_class = ForbiddenError
      elsif response.code == 404
        response_class = NotFoundError
      end

      error = response_class.new(response.to_s)
      error.response = response
      raise error
    end

    def timeout
      client_config = Configuration.pls_api[@client_code]
      time = client_config["timeout"] if client_config
      time || Configuration.pls_api.timeout
    end

    def base_url
      client_config = Configuration.pls_api[@client_code]
      url = client_config["url"] if client_config
      url || Configuration.pls_api.url
    end

    def access_token
      client_config = Configuration.pls_api[@client_code]
      token = client_config["access_token"] if client_config
      token || Configuration.pls_api.access_token
    end

    def http_get(url, params: nil, headers: {})
      request = http_client.timeout(timeout).headers(headers)
      url = "#{url}#{url.include?('?') ? '&' : '?'}#{params.to_query}" if params.present?

      Rails.logger.debug("#{self.class.name} about to call PolicyLookupService endpoint with url #{safe_url(url)}")
      response = request.get(url)
      raise_response_error!(response)

      payload = response.to_s
      MultiJson.load(payload) if payload.present?
    end

    # Strip access and auth tokens from the query parameters so they aren't logged.
    def safe_url(url)
      url.gsub(/([?&](auth|access)_token=)[^&]+/, '\1=xxxx')
    end

  end
end
