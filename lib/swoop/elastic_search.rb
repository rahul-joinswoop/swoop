# frozen_string_literal: true

module Swoop
  class ElasticSearch

    class << self

      # Return environment specific index name
      def index_name(base_name)
        base_name = base_name.to_s.freeze
        if Rails.env.test?
          base_name = "#{base_name}_test#{ENV["TEST_ENV_NUMBER"]}"
        end
        base_name
      end

    end

  end
end
