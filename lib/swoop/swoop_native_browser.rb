# frozen_string_literal: true

module Swoop
  class SwoopNativeBrowser < Browser::Platform::Base

    MATCHER = /(?<platform>ios|android).+Swoop\/(?<version>\d+(\.\d+)+)( \((?<build>\h+)\))?/.freeze

    def match?
      ua.include?('Swoop')
    end

    def id
      @id ||= (details['platform']&.to_sym || :other)
    end

    def name
      "#{id} (native)"
    end

    def version
      details['version']
    end

    def build
      details['build']
    end

    def swoop?
      true
    end

    private

    def details
      @details ||= (MATCHER.match(ua) || {})
    end

  end
end
