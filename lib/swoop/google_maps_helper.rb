# frozen_string_literal: true

module Swoop
  class GoogleMapsHelper

    DISTANCE_MATRIX = 'distancematrix'
    TIMEZONE = 'timezone'
    DIRECTIONS = 'directions'

    class << self

      def query_url(service, params)
        path = "/maps/api/#{service}/json?" + premium_query_url(params)
        "https://maps.googleapis.com#{path}&signature=#{sign(path)}"
      end

      private

      def premium_query_url(query)
        query.merge(
          client: ENV['GOOGLE_PREMIUM_CLIENT_ID'],
          channel: ENV['GOOGLE_PREMIUM_CHANNEL']
        ).to_query
      end

      def sign(string)
        raw_private_key = url_safe_base64_decode(ENV['GOOGLE_PREMIUM_SECRET_KEY'])
        digest = OpenSSL::Digest.new('sha1')
        raw_signature = OpenSSL::HMAC.digest(digest, raw_private_key, string)
        url_safe_base64_encode(raw_signature)
      end

      def url_safe_base64_decode(base64_string)
        Base64.decode64(base64_string.tr('-_', '+/'))
      end

      def url_safe_base64_encode(raw)
        Base64.encode64(raw).tr('+/', '-_').strip
      end

    end

  end
end
