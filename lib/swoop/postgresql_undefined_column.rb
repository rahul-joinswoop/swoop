# frozen_string_literal: true

class ActiveRecord::UndefinedColumn < StandardError; end

module Swoop
  module PostgreSQLUndefinedColumn

    UNDEFINED_COLUMN_PATTERN ||= Regexp.new("\\AERROR:\\s+column\\s+(\\w+)\\.(\\w+) does not exist")

    def get_root_cause(e)
      e&.cause ? get_root_cause(e.cause) : e
    end

    def handle_undefined_column(sql, name, binds)
      yield
    rescue ActiveRecord::StatementInvalid => e
      # idea from https://medium.com/@clausti/destructive-migrations-dont-have-to-destroy-your-rails-app-1bbc964d8304
      root_cause = get_root_cause(e)
      # bail if our error is something else
      raise unless root_cause.is_a? PG::UndefinedColumn
      match_data = UNDEFINED_COLUMN_PATTERN.match(root_cause.message)
      # bail if we can't figure out our table class
      raise unless match_data
      # otherwise reload our model (and any subclasses)
      Rails.logger.debug "FINDING TABLE: #{match_data[1]}"
      table_class = match_data[1].classify.constantize
      table_class.reset_column_information
      table_class.subclasses.each(&:reset_column_information)
      if in_transaction?
        # we catch this in ApplicationRecord and retry our txn, this logic is
        # copied from how rails handles ActiveRecord::PreparedStatementCacheExpired
        raise ActiveRecord::UndefinedColumn, root_cause.message
      else
        # we can't handle this so an error rises up
        raise
      end
    end

    # we need to wrap both exec_cache and exec_no_cache
    def exec_cache(*args)
      handle_undefined_column(*args) do
        super
      end
    end

    def exec_no_cache(*args)
      handle_undefined_column(*args) do
        super
      end
    end

  end
end
