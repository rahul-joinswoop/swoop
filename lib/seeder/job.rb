# frozen_string_literal: true

module Seeder
  class Job

    JOB_TYPES = [FleetInHouseJob, FleetMotorClubJob, RescueJob, FleetManagedJob].freeze
    STATUSES  = ["Pending", "Completed", "En Route", "Dispatched", "On Site", "Towing", "Tow Destination"].freeze

    class << self

      def seed_for_company_n_times(company, n, log = false)
        n.times do
          job = JOB_TYPES.shuffle.pop
          # Filter out non-partner companies here.
          job.create! payload(company, fleet_assignee(job, company))
        end
      end

      def payload(company, fleet_company)
        {
          fleet_company: fleet_company, # Originating Company
          rescue_company: company, # Assigned Company
          service_code: ServiceCode.all.shuffle.pop,
          odometer: rand(15000..120000),
          answer_by: Faker::Date.forward(days: 23),
          po_number: Faker::Number.number(digits: 10),
          accident: Faker::Boolean.boolean(0.2),
          four_wheel_drive: Faker::Boolean.boolean(0.2),
          driver_attributes: driver_attributes(company),
          status: STATUSES.shuffle.pop,
          service_location_attributes: address_hash.merge({
            address: "#{address_hash[:street]}, #{address_hash[:city]}, #{address_hash[:state]}, #{address_hash[:zip]}",
          }),
          issc_dispatch_request_attributes: issc_dispatch_attributes,
          notes: '',
        }
      end

      def fleet_assignee(job, company)
        case job
        when RescueJob
          company
        else
          FleetCompany.all.shuffle.pop
        end
      end

      private

      def vehicle_attributes
        {
          make: Faker::Vehicle.manufacture,
          model: ["Truck", "Sadan", "Satan", "MiniVan"].shuffle.pop,
          color: Faker::Color.color_name,
          vin: Faker::Vehicle.vin,
          year: rand(1970..2016),
          license: Faker::Number.hexadecimal(digits: 7),
          type: "StrandedVehicle",
        }
      end

      def driver_attributes(company)
        {
          user_attributes: {
            full_name: Faker::Name.name,
            # NB - PG - IS this the correct phone number?
            phone: Faker::PhoneNumber.phone_number,
            company: company,
          },
          vehicle_attributes: vehicle_attributes,
        }
      end

      def issc_dispatch_attributes
        {
          dispatchid: Faker::Number.number(digits: 10),
          responseid: Faker::Number.number(digits: 10),
          jobid: Faker::Number.number(digits: 10),
          max_eta: Faker::Number.number(digits: 2),
          preferred_eta: nil,
        }
      end

      def address_hash
        {
          street: Faker::Address.street_address,
          city: Faker::Address.city,
          state: Faker::Address.state,
          zip: Faker::Address.zip_code,
          lat: Faker::Address.latitude,
          lng: Faker::Address.longitude,
        }
      end

    end

  end
end
