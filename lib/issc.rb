# frozen_string_literal: true

# NOTE: don't use httparty anywhere else, use lib/http_client instead
require 'httparty'

module ISSC

  def self.phone10(phone_number)
    phone = Phonelib.parse(phone_number)
    phone.national(false)
  end

  class NotConnected < StandardError; end

  class UnsupportedServiceRequestor < StandardError; end

  class UnsupportedEvent < StandardError; end

  class DispatchJobNotFound < StandardError; end

  class Unroutable < StandardError

    attr_accessor :client_id, :location_id, :contractor_id
    def initialize(client_id, location_id, contractor_id)
      @client_id = client_id
      @location_id = location_id
      @contractor_id = contractor_id
    end

  end

  # This is a very lightweight class right now that assumes
  # that we pass things in the right format
  class DigitalDispatchClient

    # TODO - remove this in favor of HTTPClient
    include HTTParty
    default_timeout 2 # PDW: fast fail is ISSC is down

    base_uri ENV['ISSC_URL']
    format :json
    debug_output $stderr

    ## For use with Charles
    # http_proxy '127.0.0.1', 8888

    def self.configure(base_uri)
      self.base_uri base_uri
    end

    # Base64.encode64("#{ISSC_USERNAME}:")
    def initialize(auth_token)
      if auth_token.nil?
        auth_token = DigitalDispatchClient.auth_token
      end

      @options = {
        headers: {
          'Authorization' => "Basic #{auth_token}",
          'Content-Type' => "application/json",
          'Accept' => 'application/json',
        },
        verify: false,
      }
    end

    def connect(data)
      issc_post('/v1/connect', data)
    end

    def disconnect()
      issc_post('/v1/disconnect', @options)
    end

    def register(data)
      Rails.logger.debug "Calling register #{data}"
      issc_post('/v1/register', data)
    end

    def deregister(data)
      issc_post('/v1/deregister', data)
    end

    def login(data)
      issc_post('/v1/login', data)
    end

    def logout(data)
      issc_post('/v1/logout', data)
    end

    def preregistrations(data)
      issc_post('/v1/preregistrations', data)
    end

    def serviceproviders()
      issc_post('/v1/serviceproviders', {})
    end

    def servicerequesters()
      issc_post('/v1/servicerequesters', {})
    end

    def dispatchresponse(job, data)
      issc_post('/v1/dispatchresponse', data, job: job)
    end

    def dispatchstatus(job, data)
      issc_post('/v1/dispatchstatus', data, job: job)
    end

    def issc_post(path, data, job: nil)
      Rails.logger.debug "ISSCLOG(#{job.try(:id)}) POST: #{path}, DATA: #{data}, #{data.class.name}"
      db_body = nil
      hash = nil
      if data && (data.class == Hash) && data[:body]
        hash = data[:body]
        db_body = JSON.pretty_generate(hash)
      elsif data && (data.class == Hash)
        hash = data
        db_body = JSON.pretty_generate(hash)
      elsif data && (data.class == Array)
        if (data.length == 1) && (data[0].class == Hash)
          hash = data[0]
          db_body = JSON.pretty_generate(hash)
        else
          hash = nil
          db_body = JSON.pretty_generate({ "a": data })
        end
      else
        hash = nil
        db_body = data
      end
      Rails.logger.debug "ISSC hash #{hash}"
      if hash
        clientid = hash[:ClientID]
        contractorid = hash[:ContractorID]
        locationid = hash[:LocationID]
        dispatchid = hash[:DispatchID]
      else
        clientid = contractorid = locationid = dispatchid = nil
      end

      if contractorid && clientid
        company = Issc.where(clientid: clientid, contractorid: contractorid).first.try(:company)
      end

      audit_issc = AuditIssc.create!(job: job, path: path, data: db_body, company_id: company.try(:id),
                                     clientid: clientid, contractorid: contractorid, locationid: locationid, incoming: false, dispatchid: dispatchid)
      # Rails.logger.debug "ISSCLOG sending #{path}, #{@options}"
      if data
        response = self.class.post(path, @options.merge({ body: data.to_json }))
      else
        response = self.class.post(path, @options)
      end
      Rails.logger.debug "ISSCLOG(#{job.try(:id)}) Code: #{response.code}, Response: #{response.parsed_response}"
      audit_issc.update_column(:http_response, response.parsed_response.to_json)
      audit_issc.update_column(:http_status_code, response.code)

      response
    end

  end

  #
  #   #enable issc
  #   foreman run rake issc:enable
  #
  #   #disable issc
  #   foreman run rake issc:disable
  #
  class ConnectionManager

    CONNECTED = "connected"
    DISCONNECTED = "disconnected"
    CONNECTING = "connecting"

    # Suggested by MSearles 09/11/2017
    HEARTBEAT_WAIT = 3.1

    def issc_client
      ISSC::DigitalDispatchClient.new(ENV['ISSC_AUTH_TOKEN'])
    end

    def connected?
      !(connecting? || _status.nil?)
    end

    def connecting?
      _status == "connecting"
    end

    def set_connection_status(value)
      if value == 'disconnected'
        _clear_status
      else
        _set_status(value, nil)
      end
    end

    def heartbeat_vars
      heartbeat_interval = ENV.fetch('ISSC_HEARTBEAT_INTERVAL', 60).seconds
      heartbeat_time_string = _heartbeat_time
      heartbeat_time = Time.parse(heartbeat_time_string) if heartbeat_time_string
      heartbeat_wait = heartbeat_interval * HEARTBEAT_WAIT
      now = Time.now.utc
      delta = Integer((now - heartbeat_time)) if heartbeat_time
      status = _status
      Rails.logger.debug("ISSCLOG_CHECK: Issc::ensure_connected status:#{status}, heartbeat_time:#{heartbeat_time}, now:#{now}, delta:#{delta}s, heartbeat_wait:#{heartbeat_wait}, interval:#{heartbeat_interval}")
      [delta, heartbeat_time, heartbeat_wait, status]
    end

    # ISSC::ConnectionManager.new.heartbeat_connected?
    def heartbeat_connected?
      delta, _heartbeat_time, heartbeat_wait, _status = heartbeat_vars
      delta <= heartbeat_wait
    end

    def ensure_connected(&block)
      if _client.get('issc_enabled')

        delta, heartbeat_time, heartbeat_wait, status = heartbeat_vars

        if heartbeat_time.nil?
          Rails.logger.debug "ISSCLOG_CHECK: No heartbeats - reconnecting"
          status = DISCONNECTED

        elsif delta > heartbeat_wait
          Rails.logger.debug "ISSCLOG_CHECK: Heartbeat timeout - reconnecting"
          status = DISCONNECTED
        end

        if status == CONNECTED
          Rails.logger.debug("ISSCLOG_CHECK: Connected, calling block")
          block.call issc_client
        elsif ENV['ISSC_AUTOCONNECT'] == 'true'
          Rails.logger.debug("ISSCLOG_CHECK: Disconnected, attempting to start")

          Rails.logger.debug("ISSCLOG_CHECK: #{status}")

          if status == CONNECTING
            Rails.logger.debug("ISSCLOG_CHECK: Already connecting, leave alone")
          else
            _client.setex('connection_status', 60 * 2, CONNECTING)
            Rails.logger.debug "ISSCLOG_CHECK: Kicking off"
            IsscConnect.new.perform
          end

          raise NotConnected
        else
          Rails.logger.debug "ISSCLOG_CHECK: Autoconnect disabled, skipping connect, dropping message"
        end

      else
        Rails.logger.debug "ISSCLOG_CHECK: ISSC disabled, dropping message"
      end
    end

    LOGGING_IN_LOCK_TTL = 58

    def _client
      if @_dc.nil?
        @_dc = RedisClient.client(:default)
      end
      @_dc
    end

    def _disconnect
      @_dc = nil
    end

    def update_heartbeat(seqno, utc)
      Rails.logger.debug "ISSCLOG_CHECK: heartbeat received #{seqno}"
      _client.set('heartbeat_time', utc.iso8601)
      _client.set('heartbeat_sequence', seqno)
      _set_status(CONNECTED, nil)
    end

    def _status
      _client.get('connection_status')
    end

    def _clear_status
      _client.del('connection_status')
    end

    def _set_status(value, timeout)
      # TODO: Have this timeout match the configuration timeout sent up to ISSC in Connect
      if timeout
        _client.setex('connection_status', timeout, value)
      else
        _client.set('connection_status', value)
      end
    end

    def _heartbeat_time
      _client.get('heartbeat_time')
    end

    def enable_issc
      _client.set('issc_enabled', true)
    end

    def disable_issc
      _client.del('issc_enabled')
    end

    def logging_in_lock
      _client.get('issc_logging_in')
    end

    def set_logging_in_lock
      _client.setex('issc_logging_in', LOGGING_IN_LOCK_TTL, true)
    end

  end

  class Event

    attr_accessor :data
    class_attribute :allowed_parameters, :event_name, :all_events
    self.allowed_parameters = [:Event]
    self.all_events = {}

    def initialize(args, connection_manager: ConnectionManager.new)
      @data = args.slice(*allowed_parameters)

      if @data.key?('UTC')
        @data['UTC'] = Time.parse(@data['UTC'])
      end

      @issccon = connection_manager
    end

    def self.parameters(*args)
      self.allowed_parameters += args
    end

    def self.define_event(name)
      self.event_name = name
      all_events[name] = self
    end

    def self.create(event)
      event_name = event[:Event]
      unless all_events.key?(event_name)
        raise UnsupportedEvent, "Event '#{event_name}' is not supported"
      end
      all_events[event_name].new(event)
    end

    def dispatch()
      Rails.logger.debug "Dispatching #{@data}"
    end

  end

end
