# frozen_string_literal: true

begin
  require 'rails/railtie'
rescue LoadError
else
  require_relative '../somewhat_secure_id'
  require 'active_support'
  require 'active_support/core_ext/string/inflections'

  # done like this in case we want to split this out into a gem - i suppose we
  # could have just put this in an initializer..
  class SomewhatSecureID
    # = SomewhatSecureID Railtie
    #  include Active Record support.
    class Railtie < ::Rails::Railtie # :nodoc:

      initializer 'somewhat_secure_id' do |app|
        ActiveSupport.on_load(:active_record) do
          require 'somewhat_secure_id/identification'
          unless include?(SomewhatSecureID::Identification)
            send(:include, SomewhatSecureID::Identification)
          end
        end
      end

    end
  end
end
