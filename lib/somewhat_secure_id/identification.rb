# frozen_string_literal: true

require 'active_support/concern'

class SomewhatSecureID
  module Identification

    extend ActiveSupport::Concern

    def to_somewhat_secure_id(options = {})
      @somewhat_secure_id ||= SomewhatSecureID.encode(self)
    end
    alias to_ssid to_somewhat_secure_id

  end
end
