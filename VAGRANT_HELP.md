# Vagrant Help

## Debugging
Is the provisioner failing to install everything properly? Run `DEBUG=true vagrant up` to see the full provisioner STDOUT and STDERR in the console. By default, we supress STDOUT and STDERR because it's annoying.

## Wiping out your vagrant and starting over
1. Run `vagrant destroy -f`
2. Run `vagrant up`

## Compatibility
This environment _can_ easily support Mac, PC, & Linux hosts. However, to date, it's only been tested with:

* Mac OS X (10.13.5)
* VirtualBox (5.2.12)
* Vagrant (2.1.1)

## Help & Support
For help or compatibility, please contact @jerzy on Slack or jerzy@joinswoop.com.
