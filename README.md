[![Circle CI](https://circleci.com/gh/joinswoop/swoop.svg?style=shield&circle-token=fca9145317a128b0a96dad3ac02aebdd4fcb8d60)](https://circleci.com/gh/joinswoop/swoop)
[![Maintainability](https://api.codeclimate.com/v1/badges/2df14f8818b0317aa62d/maintainability)](https://codeclimate.com/repos/5af129d88889ed4b1a007406/maintainability)

```
 ________  ___       __   ________  ________  ________
|\   ____\|\  \     |\  \|\   __  \|\   __  \|\   __  \
\ \  \___|\ \  \    \ \  \ \  \|\  \ \  \|\  \ \  \|\  \
 \ \_____  \ \  \  __\ \  \ \  \\\  \ \  \\\  \ \   ____\
  \|____|\  \ \  \|\__\_\  \ \  \\\  \ \  \\\  \ \  \___|
    ____\_\  \ \____________\ \_______\ \_______\ \__\
   |\_________\|____________|\|_______|\|_______|\|__|
   \|_________|
```

Welcome to the main Swoop repo. A Rails app with a little bit of Node.js sprinkled in. It uses React and Jest for testing on the front end, GraphQL, and PostgreSQL. Hosted on Heroku. We have a lot of old CoffeeScript files but we're [porting them to JS](https://github.com/joinswoop/depercolator) as we go.

## Development Environment Install

* First, [install the Swoop Development Environment](docker/README.md)
* For iOS or Android development, [install the Mobile Development Environment](https://github.com/joinswoop/mobile/blob/master/README.md).

## Start Your Engines

To get the two terminal windows going, just run these commands.

```bash
docker-compose up
```

```
./bin/start
```

## Login

```
developer@joinswoop.com
swoopme
```

## More Information

More information can be found at the [wiki](https://github.com/joinswoop/swoop/wiki).
