# How to interact with ISSC

Setup ngrok to receive incomming connections
```
/Applications/ngrok http 5000
```


## Connect
```
%> foreman run rake issc:connect

```

## Disconnet
```
%> foreman run rake issc:disconnect

```

## For a ContractorID / TaxID / Location_ID pair.

### If it exists in our system

```
%> i = Issc.joins(:issc_locations).where(:contractorID = > cid, :taxid => tid, :issc_locations => {locationid => lid})
```

or

```
%> i = Issc.new(:company => company, :site => site, :contractorID = > cid, :taxid => tid, :location => location)
%> i.save
```

```
%> i.provider_register
%> i.register
```

```
%> i.provider_deregister
%> i.deregister
```

```
%> i.provider_login
%> i.login
```

```
%> i.provider_logout
%> i.logout
```

## If it doesn't exists in our system
