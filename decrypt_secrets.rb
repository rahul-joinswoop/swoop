# frozen_string_literal: true

require 'json'

def decrypt_secrets
  JSON.parse(`ejson decrypt secrets.ejson`)
end

def write_secrets_to_env(secrets_file)
  s = String.new ""
  secrets_file["data"].each_pair do |category, secrets|
    s << "##################\n# #{category}\n##################\n"
    secrets.each_pair do |key, value|
      key = key[1..-1] if key[0] == '_'
      s << "#{key}=#{value}\n"
    end
  end

  env = '.env'

  puts 'This will overwrite your .env file'
  puts 'Would you like to create a backup of your current .env? It will back it up to .env.bak (y|n)'
  answer = gets.chomp

  if answer == 'y' && File.exist?(env)
    `mv .env .env.bak`
    puts 'backed up your .env to .env.bak'
  end
  File.open(env, 'w+') { |f| f.write(s) }
  puts 'created .env'
end

def base64_decode(value)
  `echo #{value} | base64 -D`
end

secrets_file = decrypt_secrets
write_secrets_to_env(secrets_file)
