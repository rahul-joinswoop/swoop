To install:

1. Copy the HTML file, the images, the IPA, and the manifest.plist file to a server with HTTPS.

2. Edit the manifest.plist file and change the URLs to point to the correct files on the server

3. Edit download.html and change the link to the point to the correct manifest file location

4. Open download.html from the server, behind HTTPS, in mobile Safari and click on the link.

