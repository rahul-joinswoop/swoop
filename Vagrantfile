# frozen_string_literal: true

# -*- mode: ruby -*-
# vi: set ft=ruby :

#
# Swoop's Vagrant setup for development
# Run with `DEBUG=true vagrant up` to see the full output of the provisioner (defaults to false)
#

Vagrant.configure(2) do |config|
  # Step 1: Check for the DEBUG environment variable, and determine which mode we're in
  debugmode = (!ENV['DEBUG'].nil? && ENV['DEBUG'] == "true") ? "true" : "false"

  # Step 2: Define the VM we'll create
  config.vm.box = "geerlingguy/centos7"
  config.vm.network "forwarded_port", guest: 3000, host: 3000
  config.vm.network "forwarded_port", guest: 5000, host: 5000
  config.vm.network "forwarded_port", guest: 9200, host: 9200
  config.vm.network "forwarded_port", guest: 5432, host: 5432
  config.vm.network "private_network", type: "dhcp"
  config.vm.synced_folder ".", "/swoop", type: "nfs"

  # Step 3: When vagrant up is run, greet the developer
  if ARGV[0] == "up"
    # Step 3.1: Display the welcome message
    puts "
        WELCOME TO...
   _____
  / ____|
 | (_____      _____   ___  _ __
  \\___ \\ \\ /\\ / / _ \\ / _ \\| '_ \\
  ____) \\ V  V / (_) | (_) | |_) |
 |_____/ \\_/\\_/ \\___/ \\___/| .__/
                           | |
                           |_| ®
 "

    # Step 3.2: Assess system RAM and CPU cores, and provision the VM with the largest
    # reasonable resources: We take half of the ram, and all but 1 of the cores
    ram_as_bytes = /(hw.memsize: )(\d*)/.match(`sysctl hw.memsize`)[2].to_i
    ram_as_megabytes = ram_as_bytes / 1024 / 1024 # a.k.a., bytes/kilobytes/megabytes
    vm_ram = ram_as_megabytes / 2
    cpu_core_count = /(hw.physicalcpu: )(\d*)/.match(`sysctl hw.physicalcpu`)[2].to_i
    vm_cpu_cores = cpu_core_count - 1
    config.vm.provider "virtualbox" do |box|
      box.cpus = vm_cpu_cores
      box.memory = vm_ram
    end
  end

  # Step 4: Define the VM we'll create
  config.vm.provision(
    "shell",
    privileged: false,
    path: "./provisioner",
    args: [debugmode]
  )
end
