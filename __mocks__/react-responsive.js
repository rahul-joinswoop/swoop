// based on https://github.com/contra/react-responsive/issues/63

import React from 'react'

// have to use require.requireActual here or we infinitely loop
const MediaQuery = require.requireActual('react-responsive').default

const MockMediaQuery = (props = {}) => {
  const defaultWidth = window.innerWidth
  const defaultHeight = window.innerHeight
  const values = {
    width: defaultWidth,
    height: defaultHeight,
    ...(props?.values || {}),
  }
  const newProps = Object.assign({}, props, { values })
  return <MediaQuery {...newProps} />
}

export default MockMediaQuery
