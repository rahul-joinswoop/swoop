#!/bin/bash

heroku ps:scale web=4 -a secure-bayou-4357
heroku ps:scale clock=1 -a secure-bayou-4357
heroku ps:scale sidekiq=4 -a secure-bayou-4357
heroku ps:scale worker=4 -a secure-bayou-4357
heroku maintenance:off -a secure-bayou-4357
