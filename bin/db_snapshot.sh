#!/bin/bash
# bin/db_snapshot.sh

app_name=secure-bayou-4357
outfile=db_snapshot.log.$(date +"%Y_%m_%d__%H_%M_%S")

echo Analyzing $app_name to $outfile

echo 'heroku pg:info -a $app_name' >> $outfile
heroku pg:info -a $app_name >> $outfile
echo .

echo 'heroku pg:diagnose -a $app_name' >> $outfile
heroku pg:diagnose -a $app_name >> $outfile
echo .

echo 'heroku pg:locks -a $app_name' >> $outfile
heroku pg:locks -a $app_name >> $outfile
echo .

echo 'heroku pg:ps DATABASE_URL -a $app_name' >> $outfile
heroku pg:ps DATABASE_URL -a $app_name >> $outfile
echo .


echo 'echo "SELECT * FROM pg_stat_activity WHERE client_addr IS NULL;" | heroku pg:psql -a $app_name' >> $outfile
echo "SELECT * FROM pg_stat_activity WHERE client_addr IS NULL;" | heroku pg:psql -a $app_name >> $outfile
