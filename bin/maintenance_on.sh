#!/bin/bash


read -p "Do you want to put PRODUCTION into maintenance (y/n)? " answer
case ${answer:0:1} in
    y|Y )
        heroku maintenance:on -a secure-bayou-4357
        heroku ps:scale web=0 -a secure-bayou-4357
        heroku ps:scale clock=0 -a secure-bayou-4357
        heroku ps:scale sidekiq=0 -a secure-bayou-4357
        heroku ps:scale worker=0 -a secure-bayou-4357
    ;;
    * )
        echo No
    ;;
esac

