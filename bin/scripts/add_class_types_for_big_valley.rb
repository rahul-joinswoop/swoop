categories = ["Medium Duty Combo", "Heavy Duty Combo", "Super Heavy Combo"]
db_cats = []
categories.each do |cat|
  db_cats << VehicleCategory.find_or_create_by!(name: cat)
end

company = RescueCompany.find_by!(name: "Big Valley Towing")
db_cats.each do |cat|
  sc = company.vehicle_categories.find_by(name: cat.name)
  if !sc
    company.vehicle_categories << cat
    company.save!
  end
end
