update jobs set deleted_at=now() where jobs.id in
(select jobs.id
from
  jobs full outer join locations as drop_location on jobs.drop_location_id = drop_location.id
  full outer join locations as service_location on jobs.service_location_id = service_location.id
  full outer join estimates on jobs.estimate_id=estimates.id
  full outer join customer_types on jobs.customer_type_id=customer_types.id
  left join invoicing_ledger_items on jobs.invoice_id=invoicing_ledger_items.id
  left join accounts on invoicing_ledger_items.recipient_id=accounts.id,
  companies,
  drives,
  users as customer,
  vehicles,
  service_codes
where
  jobs.service_code_id=service_codes.id and
  drives.vehicle_id=vehicles.id and
  jobs.rescue_company_id=companies.id and
  jobs.driver_id = drives.id and
  drives.user_id=customer.id and
  jobs.fleet_company_id=jobs.rescue_company_id and
  accounts.client_company_id=38)
