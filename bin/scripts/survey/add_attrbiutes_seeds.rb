# seeds/staging

farmers = Company.find_by(name: 'Farmers Insurance')
farmers.update_attributes(review_url: 'https://swoopme.typeform.com/to/wITZCb',
                          survey_host: 'staging.farmerssurvey.com',
                          survey_template: 'api/v1/review/whitelabel/farmers')
farmers.save!

turo = Company.find_by(name: 'Turo')
turo.update_attributes(review_url: 'https://swoopme.typeform.com/to/Rjx7Yl',
                       survey_template: 'api/v1/review/whitelabel/turo')
turo.save!

geico = Company.find_by(name: 'Geico')
geico.update_attributes(review_url: 'https://swoopme.typeform.com/to/cs2H2U',
                        survey_template: 'api/v1/review/whitelabel/geico')
geico.save!

# production

# farmers=Company.find_by(name:'Farmers Insurance')
# farmers.update_attributes(survey_host:'farmerssurvey.com',
#                           survey_template:'api/v1/review/whitelabel/farmers')
# farmers.save!
#
# turo=Company.find_by(name:'Turo')
# turo.update_attributes(survey_host:'turo.joinswoop.com',
#                        survey_template:'api/v1/review/whitelabel/turo')
# turo.save!
#
# geico=Company.find_by(name:'Geico')
# geico.update_attributes(survey_host:'geico.joinswoop.com',
#                         survey_template:'api/v1/review/whitelabel/geico')
# geico.save!
