sites = [
  ["19", "Retail Store", "Toronto-Yorkdale Shopping Centre"],
  ["20", "Retail Store", "Scottsdale-Fashion Square"],
  ["22", "Retail Store", "Corte Madera-The Village at Corte Madera"],
  ["24", "Retail Store", "Canoga Park-Topanga"],
  ["26", "Retail Store", "Mission Viejo-Shops At Mission Viejo"],
  ["27", "Retail Store", "Newport Beach-Fashion Island"],
  ["28", "Retail Store", "Brea-Brea Mall"],
  ["30", "Retail Store", "Pasadena-Colorado Blvd"],
  ["32", "Retail Store", "San Diego-UTC"],
  ["34", "Retail Store", "San Jose-Santana Row"],
  ["36", "Retail Store", "Santa Monica-3rd Street Promenade"],
  ["38", "Retail Store", "Lone Tree-Park Meadows"],
  ["39", "Retail Store", "Washington DC-K Street"],
  ["40", "Retail Store", "Boca Raton-Town Center at Boca Raton"],
  ["41", "Retail Store", "Miami-Dadeland"],
  ["44", "Retail Store", "Lenox Square"],
  ["45", "Retail Store", "Skokie-Old Orchard Mall"],
  ["46", "Retail Store", "Oak Brook-Oakbrook Center"],
  ["47", "Retail Store", "Indianapolis-The Fashion Mall at Keystone"],
  ["48", "Retail Store", "Prudential Center"],
  ["49", "Retail Store", "Natick-Natick Mall"],
  ["53", "Retail Store", "Short Hills-Short Hills Mall"],
  ["54", "Retail Store", "Paramus-Garden State Plaza"],
  ["56", "Retail Store", "Manhattan-West 25th Street"],
  ["57", "Retail Store", "White Plains-The Westchester"],
  ["58", "Retail Store", "Easton Town Center"],
  ["60", "Retail Store", "Portland-Washington Square"],
  ["61", "Retail Store", "King of Prussia-King of Prussia Mall"],
  ["68", "Service", "Vancouver-Powell Street"],
  ["72", "Service", "Scottsdale-Hayden Road"],
  ["74", "Service Plus", "Burlingame"],
  ["77", "Sales and Delivery", "Fremont"],
  ["78", "Service", "Los Angeles-Centinela"],
  ["80", "Service", "West Los Angeles-Santa Monica Blvd."],
  ["82", "Service Plus", "Palo Alto"],
  ["83", "Service Plus", "Sacramento-Rocklin"],
  ["86", "Service", "San Rafael"],
  ["88", "Service Plus", "Sunnyvale"],
  ["89", "Service", "Los Angeles-Torrance"],
  ["90", "Service", "Los Angeles-Van Nuys"],
  ["91", "Service", "Denver-Evans Avenue"],
  ["92", "Service", "Milford"],
  ["97", "Service Plus", "Tampa"],
  ["98", "Service Plus", "West Palm Beach"],
  ["99", "Service", "Marietta"],
  ["100", "Service", "Waikele-Waipahu"],
  ["101", "Service Plus", "Chicago-West Grand Avenue"],
  ["102", "Service Plus", "Chicago-Highland Park"],
  ["103", "Service", "Chicago-Villa Park"],
  ["107", "Service", "Boston-Watertown"],
  ["108", "Service", "Rockville"],
  ["110", "Service Plus", "Minneapolis-Eden Prairie"],
  ["112", "Service Plus", "St.  Louis-University City"],
  ["113", "Service", "Raleigh-Service"],
  ["115", "Service", "Springfield Township"],
  ["117", "Service Plus", "Las Vegas"],
  ["118", "Service", "Long Island-Syosset"],
  ["119", "Service Plus", "Westchester-Mt. Kisco"],
  ["120", "Service", "Long Island City-Queens -Battery Repair"],
  ["123", "Service Plus", "Cleveland-Lyndhurst"],
  ["124", "Service", "Columbus-Morse Road"],
  ["127", "Service", "Portland-Tigard"],
  ["129", "Service Plus", "Nashville-Brentwood"],
  ["133", "Service", "Houston-Westchase"],
  ["134", "Sales and Delivery", "South Salt Lake City-S. State Street"],
  ["136", "Service", "Seattle-SoDo"],
  ["137", "Retail Store", "Seattle-Westlake"],
  ["151", "Retail Store", "Robson Street-Vancouver BC"],
  ["153", "Retail Store", "Sarasota-University Town Center"],
  ["156", "Service Plus", "Montreal-Ferrier"],
  ["228", "Service", "San Diego-Miramar"],
  ["230", "Service Plus", "Palm Springs-Cathedral City"],
  ["315", "Service Plus", "Devon Service Plus"],
  ["325", "Service Plus", "Dublin-Amador Plaza"],
  ["328", "Service Plus", "Toronto-Lawrence Avenue"],
  ["343", "Service Plus", "Tysons Corner-Tyco Road"],
  ["345", "Service", "Costa Mesa Service"],
  ["349", "Service Plus", "Atlanta-Decatur"],
  ["351", "Service Plus", "Dania Beach"],
  ["353", "Service", "Austin-Burnet Road"],
  ["368", "Sales and Delivery", "Costa Mesa Sales &amp; Delivery"],
  ["378", "Service Plus", "Paramus-Route 17"],
  ["460", "Service Plus", "Santa Barbara-Hitchcock Way"],
  ["461", "Service Plus", "Orlando-Eatonville"],
  ["470", "Service", "Austin-Burnet Expansion"],
  ["498", "Retail Store", "Bellevue-Bellevue Square"],
  ["544", "Sales and Delivery", "Marietta Sales and Delivery"],
  ["545", "Sales and Delivery", "Raleigh Sales &amp; Delivery"],
  ["584", "Service and Gallery", "Charlotte-Matthews"],
  ["627", "Service and Gallery", "Houston-North"],
  ["636", "Service Plus", "Dedham"],
  ["651", "Service Plus", "Burbank"],
  ["658", "Service Plus", "Austin-Pond Springs"],
  ["663", "Service Plus", "Kansas City-Manchester"],
  ["669", "Service Plus", "Cincinnati Blue Ash Service Plus"],
  ["671", "Service", "Van Nuys Expansion"],
  ["677", "Service Plus", "San Rafael Expansion"],
  ["705", "Service Plus", "Monterey-Seaside"],
  ["755", "Service Plus", "Brooklyn-Van Brunt Street"],
  ["777", "Service Plus", "Vancouver-West 4th Avenue"],
  ["825", "Service Plus", "San Francisco-Van Ness"],
  ["826", "Service Plus", "Buena Park Service Plus"],
  ["847", "Service", "Costa Mesa Service Expansion"],
  ["856", "Service Plus", "Bellevue-20th Street"],
  ["912", "Retail Store", "Century City Temp"],
  ["913", "Retail Store", "Broadway Plaza Relocation"],
  ["921", "Service", "Gigafactory-Service"],
  ["940", "Service and Gallery", "Dallas-Cedar Springs Road"],
  ["944", "Retail Store", "Calgary-Chinook Centre"],
  ["953", "Retail Store", "San Diego-Fashion Valley"],
  ["957", "Retail Store", "County Club Plaza"],
  ["959", "Delivery", "Portland-SW 72nd Avenue"],
  ["964", "Service Plus", "Tempe-E. University Drive"],
  ["1002", "Retail Store", "Avalon-Alpharetta"],
  ["1006", "Retail Store", "The Gardens on El Paseo"],
  ["1011", "Retail Store", "Tampa-International Plaza"],
  ["1020", "Retail Store", "Palo Alto-Stanford Shopping Center"],
  ["1046", "Service", "Salt Lake City-1038 South 300 West"],
  ["1059", "Retail Store", "Derby Street Shoppes"],
  ["1254", "Retail Store", "Honolulu-Ala Moana Center"],
  ["1255", "Retail Store", "Jacksonville-St. Johns Town Center-Permanent"],
  ["1256", "Retail Store", "Americana Manhasset"],
  ["1258", "Service", "Toronto-Mississauga-Ambassador Drive"],
  ["1270", "Service", "Fremont-Factory Service"],
  ["1378", "Service", "Buena Park-Pre-Owned"],
]
Tesla = Company.find_by!(name: "Tesla")
sites.each do |site|
  FleetSite.where(name: site[2], site_code: site[0], company: Tesla).first_or_create
end
