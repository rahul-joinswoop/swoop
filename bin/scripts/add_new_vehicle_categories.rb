company = RescueCompany.find_by!(name: "Quik Pik Towing")
cat = VehicleCategory.find_by!(name: "Service")
sc = company.vehicle_categories.find_by(name: cat.name)
if !sc
  company.vehicle_categories << cat
  company.save!
end

company = RescueCompany.find_by!(name: "Big Valley Towing")
serv = ServiceCode.find_by!(name: "Driveline")
sc = company.service_codes.find_by(name: serv.name)
if !sc
  company.service_codes << serv
  company.save!
end
