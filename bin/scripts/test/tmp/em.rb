require 'eventmachine'
require 'thread'
require 'em-hiredis'

class QueueWithTimeout

  def initialize
    @mutex = Mutex.new
    @queue = []
    @recieved = ConditionVariable.new
  end

  def <<(x)
    @mutex.synchronize do
      @queue << x
      p "length: #{@queue.length}"
      @recieved.signal
      p "signal sent"
    end
  end

  def length
    @queue.length
  end

  def num_waiting
    -1
  end

  def pop(non_block = false)
    pop_with_timeout(non_block ? 0 : nil)
  end

  def pop_with_timeout(timeout = nil)
    @mutex.synchronize do
      if @queue.empty?
        p "waiting for signal"
        @recieved.wait(@mutex)
        # if we're still empty after the timeout, raise exception
        p "signal received"
        raise ThreadError, "queue empty" if @queue.empty?
      end
      @queue.shift
    end
  end

end

q = QueueWithTimeout.new

Thread.new do
  i = 0
  while true
    q << i
    i += 1
    sleep 1
  end
end

r1 = Thread.new do
  EM.run do
    redis = EM::Hiredis.connect

    #  EM.add_periodic_timer(1) {
    #    redis.publish("foo", "Hello")
    #  }

    #  EM.add_timer(5) {
    #    puts "Unsubscribing sub1"
    #    redis.pubsub.unsubscribe("foo")
    #  }

    # EM.defer do
    while true
      puts "calling pop"
      a = q.pop
      puts "pop returned a:#{a}"
      EM.next_tick do
        redis.pubsub.psubscribe("fo*") do |msg|
          p [:sub3, msg]
        end
        # subscribe("fo*",nil)
        puts "NextTick #{Thread.current} #{a}"
      end

    end
    # end

    puts "ere"
  end
end

r2 = Thread.new do
  EM.run do
    redis = EM::Hiredis.connect

    EM.add_periodic_timer(1) do
      redis.publish("foo", "Hello")
      p "pub"
    end

    #  EM.add_timer(5) {
    #    puts "Unsubscribing sub1"
    #    redis.pubsub.unsubscribe("foo")
    #  }

    # EM.defer do
    while false
      puts "calling pop"
      a = q.pop
      puts "pop returned a:#{a}"
      EM.next_tick do
        redis.pubsub.psubscribe("fo*") do |msg|
          p [:sub3, msg]
        end
        # subscribe("fo*",nil)
        puts "NextTick #{Thread.current} #{a}"
      end

    end
    # end

    puts "ere2"
  end
end

puts "exiting"

r1.join
r2.join
