service_name = 'Winch Out'
service = ServiceCode.find_by(name: service_name)
company = FleetCompany.find_by!(name: "Turo")
sc = company.service_codes.find_by(name: service_name)
if !sc
  company.service_codes << service
  company.save!
end
company = FleetCompany.find_by!(name: "Farmers Insurance")
sc = company.service_codes.find_by(name: service_name)
if !sc
  company.service_codes << service
  company.save!
end

farmers = FleetCompany.find_by!(name: "Farmers Insurance")
turo = FleetCompany.find_by!(name: "Turo")

q = Question.where(question: "Keys present?").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: service, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: service, question: q).first_or_create

q = Question.where(question: "Customer with vehicle?").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: service, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: service, question: q).first_or_create

q = Question.where(question: "Vehicle can be put in neutral?").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: service, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: service, question: q).first_or_create

q = Question.where(question: "Vehicle 4 wheel drive?").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: service, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: service, question: q).first_or_create

q = Question.where(question: "Within 10 ft of a paved surface?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
q.answers.where(answer: "Unknown").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: service, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: service, question: q).first_or_create

q = Question.where(question: "Low clearance?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
q.answers.where(answer: "Unknown").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: service, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: service, question: q).first_or_create
