duplicates = User.joins(:company).select("company_id, first_name, last_name, count(users.id)").group(:company_id, :first_name, :last_name).having("count(users.id) > 1").to_a
duplicates.each do |duplicate|
  dupes = User.where(company_id: duplicate.company_id, first_name: duplicate.first_name, last_name: duplicate.last_name).order(created_at: :asc)
  dupes.each_with_index do |dupe, i|
    next if i == 0
    dupe.update! last_name: "#{dupe.last_name} (#{i+1})"
  end
end
