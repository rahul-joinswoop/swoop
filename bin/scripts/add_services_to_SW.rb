services = ["Illegally Parked", "Winch Out", "Up-Righting", "Transport", "Heavy Haul"]
southern = RescueCompany.find_by!(name: "Southern Wrecker & Recovery")
services.each do |service_name|
  service = ServiceCode.where(name: service_name).first_or_create
  if !southern.service_codes.find_by(name: service_name)
    southern.service_codes << service
  end
end

southern.save!
