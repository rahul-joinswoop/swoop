Job.uncached do
  Job.where(site: nil).each do |job|
    if !job.rescue_company.nil? && !job.site
      job.site = job.rescue_company.hq
      job.save
    end
  end
end

Job.all.joins(:invoice).where(invoicing_ledger_items: { total_amount: 0 }).each do |job|
  Job.create_invoice_estimate(job.id)
end
