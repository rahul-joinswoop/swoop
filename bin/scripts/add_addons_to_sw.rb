services = ["Clean Up", "Roll Over", "Oil Dry", "Gate Fee", "Portering", "Rotator", "Axle Removal", "Driveshaft Removal", "Road Tractor", "Brake Release", "Wheel Loader", "Skid Steer", "Excavator", "Roll-Off Container", "Light Tower", "Scene Supervisor", "Air Cushion Recovery Unit", "Crane", "Equipment Prep.", "Load Shift", "Load Transfer", "Forklift", "Materials", "Rigging", "Spill Response Unit", "Traffic Management", "Escort", "Permits"]
southern = RescueCompany.find_by!(name: "Southern Wrecker & Recovery")
services.each do |service_name|
  service = ServiceCode.where(name: service_name, addon: true).first_or_create
  if !southern.service_codes.find_by(name: service_name)
    southern.service_codes << service
  end
end

southern.save!
