arel = User.where("email IS NOT NULL OR username IS NOT NULL")
count = arel.count
puts "Starting backfill of #{count} users."
sql = "INSERT INTO job_status_email_preferences (user_id, job_status_id, created_at, updated_at) VALUES"
arel.find_in_batches(batch_size: 500) do |batch|
  batch.each do |user|
    JobStatus::DEFAULT_EMAIL_STATUSES.map do |id|
      ApplicationRecord.connection.execute("#{sql} (#{user.id}, #{id}, NOW(), NOW())")
    rescue Exception => e
      puts "Error: User: #{user.id} Status: #{id} Msg: #{e.message}"
    end
  end
end
