Invoice.where(paid: nil).find_in_batches do |invoices|
  invoices.each do |i|
    i.update_column(:paid, false)
  end
end
