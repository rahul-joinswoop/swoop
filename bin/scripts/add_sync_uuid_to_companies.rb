# frozen_string_literal: true

batch_size = 200

def generate_unique_uuid
  loop do
    uuid = SecureRandom.hex(5)
    break uuid unless Company.exists?(uuid: uuid)
  end
end

Company.find_each(batch_size: batch_size) do |company|
  uuid = company.uuid

  puts "Using existing UUID for #{company.name}: #{uuid}" if uuid

  # Don't run validations or callbacks on update
  company.update_column :sync_uuid, uuid || generate_unique_uuid
end
