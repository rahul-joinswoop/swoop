# (2027..2037).each do |job_id|
#  Job.create_invoice_estimate(job_id,create_invoice=true)
# end

# Job.find_by_sql("select jobs.id, jobs.created_at,jobs.service_location_id, jobs.drop_location_id, jobs.site_id, jobs.fleet_company_id,jobs.rescue_company_id from jobs left outer join invoicing_ledger_items on jobs.invoice_id=invoicing_ledger_items.id where invoicing_ledger_items.id is null and jobs.service_location_id is not null and jobs.drop_location_id is not null and jobs.site_id is not null and jobs.rescue_company_id is not null and jobs.rescue_company_id!=471 and jobs.id>=1676").each do |job|
#  Job.create_invoice_estimate(job.id,create_invoice=true)
# end

# Job.find_by_sql("select jobs.* from jobs left join invoicing_ledger_items on jobs.invoice_id=invoicing_ledger_items.id where invoicing_ledger_items.rate_type='MilesP2PRate' and invoicing_ledger_items.state='partner_new'").each do |job|
#  Job.create_invoice_always_and_save(job.id)
# end

# Job.find_by_sql("select jobs.* from jobs left join invoicing_ledger_items on jobs.invoice_id=invoicing_ledger_items.id").include(invoice:[:line_items])each do |job|

# 3mins
#  heroku run --size=performance-l rails c -a
Job.joins(:invoice).includes(invoice: [:line_items]).find_each do |job|
  if job.invoice
    Job.transaction do
      job.invoice.line_items.each do |li|
        li.update_columns(rate_id: job.invoice.rate.try(:id), job_id: job.id)
      end
    end
  end
end

# fix duplicate invoices (do in maint mode!)
# now=now()
#
# update invoicing_ledger_items set deleted_at='now' where state='partner_new' and deleted_at is null;
#
# update invoicing_ledger_items set deleted_at=null
#  from jobs
# where jobs.invoice_id=invoicing_ledger_items.id and invoicing_ledger_items.deleted_at='now';

# !!!!!!!!! danger !!!!!!!!!!!!!!!!
Job.joins(:invoice).where(invoicing_ledger_items: { state: 'partner_new' }).includes(invoice: [:line_items]).find_each do |job|
  if job.invoice.new_state?
    job.update_column(:invoice_id, nil)
    InvoicingLedgerItem.where(job_id: job.id)
      .InvoicingLineItem.where(job_id: job.id).delete_all

    job.invoice = CreateInvoiceForJob.new(job).call
    UpdateInvoiceForJob.new(job.invoice,
                            job,
                            job.get_rate).call
    job.save!
  end
end

now = Time.now
Job.joins(:invoice).where(invoicing_ledger_items: { state: 'partner_new' }).includes(invoice: [:line_items]).find_each do |job|
  if job.invoice.new_state?
    job.update_column(:invoice_id, nil)
    InvoicingLedgerItem.where(job_id: job.id, deleted_at: nil).update_all(deleted_at: now)

    job.invoice = CreateInvoiceForJob.new(job).call
    UpdateInvoiceForJob.new(job.invoice,
                            job,
                            job.get_rate).call
    job.save!
  end
end

Job.where(fleet_company_id: 38, id: [9813, 9771, 9767, 9737, 9736, 9713, 9712, 9707, 9684, 9682, 9674, 9667, 9665, 9662, 9651, 9645, 9643, 9639, 9621, 9616, 9612, 9611, 9603, 9600, 9589, 9588, 9587, 9586, 9585, 9577, 9564, 9551, 9542, 9539, 9532, 9528, 9523, 9521, 9512, 9471, 9466, 9459, 9458, 9442, 9437, 9433, 9431, 9428, 9423, 9420, 9418, 9415, 9414, 9413, 9412, 9410, 9401, 9397, 9392, 9391, 9387, 9386, 9382, 9379, 9376, 9367, 9362, 9350, 9343, 9342, 9340, 9338, 9332, 9330, 9327, 9324, 9319, 9318, 9312, 9311, 9310, 9300, 9293, 9290, 9282, 9273, 9272, 9269, 9260, 9258, 9255, 9253, 9249, 9231, 9230, 9228, 9226, 9225, 9224, 9220, 9216, 9214, 9209, 9206, 9205, 9203, 9200, 9199, 9193, 9191, 9188, 9182, 9177, 9176, 9166, 9162, 9160, 9159, 9146, 9145, 9141, 9137, 9134, 9133, 9131, 9129, 9122, 9121, 9119, 9115, 9113, 9112, 9108, 9106, 9105, 9101, 9100, 9098, 9097, 9093, 9091, 9089, 9082, 9078, 9074, 9071, 9070, 9069, 9063, 9061, 9059, 9058, 9056, 9055, 9054, 9053, 9052, 9051, 9045, 9032, 9029, 9028, 9027, 9023, 9018, 9009, 8997, 8986, 8982, 8980, 8978, 8974, 8970, 8957, 8955, 8951, 8945, 8941, 8940, 8939, 8933, 8928, 8925, 8923, 8919, 8913, 8912, 8910, 8909, 8908, 8904, 8895, 8894, 8893, 8892, 8889, 8888, 8881, 8870, 8866, 8864, 8860, 8858, 8827, 8806, 8619, 8618, 8608, 8608, 8591, 8589, 8583, 8575, 8562, 8539, 8526, 8474, 8449, 8427, 8359, 8342, 8331, 8313, 8289, 8266, 8120, 7937, 7931, 7904, 7902, 7858, 7810, 7719, 7578, 7405, 7393, 7383, 7379, 7352, 7347, 7340, 7327, 7197, 7188, 7150, 7147, 7116, 7074, 7035, 6936, 6896, 6875, 6825, 6713, 6669, 6659]).find_each do |job|
  if job.invoice && (job.invoice.state == 'fleet_approved')
    job.invoice.update_column(:state, 'fleet_downloaded')
  end
end
