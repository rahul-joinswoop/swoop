def update_typeform_review(job_id, feedback)
  review = Reviews::Typeform.find_by_job_id(job_id)
  return unless review

  review.update_column(:nps_feedback, feedback)
end

update_typeform_review(26563, 'Faster')
update_typeform_review(26476, 'Get rid of the tow company you sent to me')
update_typeform_review(26398, 'I think you are doing a great job already.  Thanks')
update_typeform_review(26354, 'Information given to tow company was not correct.')
update_typeform_review(26525, 'No suggestions for Improvement. Service was wonderful. Thanks for being there!')
update_typeform_review(26718, 'Everything went as well or better than expected - cannot think of anything to improve at this point')
update_typeform_review(26192, 'Eta time')
update_typeform_review(26451, 'You guys are doing well')
