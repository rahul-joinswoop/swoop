# ENG-4287
# ENG-5537

[
  6439,
  15697,
  21519,
  30663,
  44723,
  65823,
  67077,
  69316,
  85928,
  103431,
  105580,
  112703,
  144628,
  172721,
  220541,
  299168,
  304681,
  308075,
  308421,
  308723,
].each do |job_id|
  job = Job.find(job_id)
  raise ArgumentError, "Job not affected by this bug ENG-4287: (account matches) #{job_id}" unless job.rescue_company_id != job.account.company_id
  raise ArgumentError, "Job not affected by this bug ENG-4287: (account not Other) #{job_id} #{job.account.company.name}" unless job.account.company.name == 'Other'
  Rails.logger.debug "JOB(#{job.id}) Looking..."
  new_account = job.rescue_company.accounts.where(name: 'Tesla', deleted_at: nil).first
  Rails.logger.debug "JOB(#{job_id}) Setting account from #{job.account_id} to #{new_account.id}"
  job.account = new_account
  job.save!
end
