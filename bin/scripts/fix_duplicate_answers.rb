vals = {}
Answer.all.order(:id).each do |answer|
  original_answer = vals["#{answer.question_id}_#{answer.answer}"]
  if original_answer
    qr = QuestionResult.find_by(answer_id: answer.id)
    p "Reassigning #{qr.id}: #{qr.answer_id} -> #{original_answer.id}"
    qr.answer = original_answer
    p "Deleting #{answer.id}"
    qr.save!
    answer.delete
  else
    p "Setting original #{answer.id}"
    vals["#{answer.question_id}_#{answer.answer}"] = answer
  end
end
