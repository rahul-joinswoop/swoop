RescueCompany.find_each(batch_size: 100) do |rc|
  token = QB::Token.find_by(company: rc)
  if !token
    QB::Token.create(company: rc, token: SecureRandom.uuid)
  end
end
