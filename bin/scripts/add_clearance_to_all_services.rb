farmers = FleetCompany.find_by!(name: "Farmers Insurance")
turo = FleetCompany.find_by!(name: "Turo")

services = [ServiceCode::TIRE_CHANGE, "Lock Out", "Other", ServiceCode::FUEL_DELIVERY, "Battery Jump", "Tow", "Tow if Jump Fails", "Impound", "Repo", "Other"]
services.each do |service_name|
  service = ServiceCode.find_by(name: service_name)
  q = Question.where(question: "Low clearance?").first
  CompanyServiceQuestion.where(company: farmers, service_code: service, question: q).first_or_create
  CompanyServiceQuestion.where(company: turo, service_code: service, question: q).first_or_create
end
