Job.where(adjusted_created_at: nil).find_in_batches do |jobs|
  jobs.each do |job|
    calc = AdjustedCreatedAtCalculator.new(job)
    job.update_column(:adjusted_created_at, calc.calculate)
    sleep 0.01 # 53000 jobs * 0.01 = 8 m of sleeping
  end
  sleep 1 # 53000 / 1000 batch size = 53 s of sleeping
end
