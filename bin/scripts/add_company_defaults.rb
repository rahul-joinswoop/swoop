[33].each do |company_id|
  company = Company.find(company_id)
  [
    'Tow',
    'Battery Jump',
    ServieCode::TIRE_CHANGE,
    'Lock Out',
    ServieCode::FUEL_DELIVERY,
    'Other',
    'Tow if Jump Fails',
    'Impound',
    'Repo',
    'Winch Out',
    'Recovery',
    'Secondary Tow',
  ].each do |service_name|
    sc = company.service_codes.find_by(name: service_name)
    if !sc
      service = ServiceCode.find_by(name: service_name)
      company.service_codes << service
    end
  end

  settings_feature = Feature.find_by(name: "Settings")
  settings_cf = CompaniesFeature.find_by(company: company, feature: settings_feature)
  if !settings_cf
    CompaniesFeature.create(company: company, feature: settings_feature)
  end

  location_feature = Feature.find_by(name: "Location Contacts")
  location_cf = CompaniesFeature.find_by(company: company, feature: location_feature)
  if !location_cf
    CompaniesFeature.create(company: company, feature: location_feature)
  end

  company.save
end
