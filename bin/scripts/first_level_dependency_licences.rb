require 'active_support/core_ext/hash/indifferent_access'

licenses = {}.with_indifferent_access
File.open('licenses.txt', "r") do |f|
  f.each_line do |line|
    library_name = line.split(',')[0]
    licenses[library_name] = line
  end
end

File.open('packages.sorted.fld', "r") do |f|
  f.each_line do |line|
    library_name = line.strip
    #    puts "'#{library_name}'"
    if licenses[library_name]
      puts licenses[library_name]
    else
      puts "NOT FOUND !!!! #{library_name}"
    end
  end
end
