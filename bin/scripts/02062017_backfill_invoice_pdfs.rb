Invoice.where(s3_filename: nil).find_in_batches do |invoices|
  invoices.each do |i|
    InvoicePDFWorker.perform_async(i.id)
  end
end
