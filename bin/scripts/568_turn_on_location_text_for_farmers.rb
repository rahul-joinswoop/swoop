farmers = FleetCompany.find_by!(name: "Farmers Insurance")
default_true = Feature.find_by!(name: "SMS_ETA Updates")

farmers.features << default_true
farmers.save!
