# select jobs.id, fleet_company_id, rescue_company_id, invoicing_ledger_items.state from jobs left join invoicing_ledger_items on jobs.invoice_id=invoicing_ledger_items.id where jobs.id in (6769, 6865, 8211, 8961, 9268, 8726, 4890)

def move_job(job_id, fleet_company_id, old_rescue_company_id, new_rescue_company_id)
  fleet_company = Company.find(fleet_company_id)
  new_rescue_company = Company.find(new_rescue_company_id)
  old = Company.find(old_rescue_company_id)

  Job.transaction do
    job = Job.find(job_id)
    raise ArgumentError if job.rescue_company != old
    job.rescue_company = new_rescue_company
    job.site = new_rescue_company.sites.first
    if fleet_company.fleet_in_house?
      new_account = new_rescue_company.accounts.create_with(company: new_rescue_company).find_or_create_by(fleet_company: fleet_company)
    else
      new_account = new_rescue_company.accounts.create_with(company: new_rescue_company).find_or_create_by(fleet_company: Company.swoop)
    end
    raise ArgumentError if !new_account
    job.invoice.sender = new_rescue_company
    job.invoice.recipient = new_account
    job.invoice.state = 'partner_new'
    job.invoice.recipient_company = fleet_company
    job.invoice_dirty = true
    job.save!
  end
end

def move_tesla_job(job_id, old_rescue_company_id, new_rescue_company_id)
  tesla = Company.find_by(name: "Tesla")
  move_job(job_id, tesla.id, old_rescue_company_id, new_rescue_company_id)
end

move_job(17747, 508, 680, 652)

move_tesla_job(49371, 471, 593)

CreateOrUpdateEstimateForJob.perform_async(8249)

move_tesla_job(10563, 143)

[
  10687,
  10463,
  8711,
  8436,
  8249,
  9596,
  9337,
  9336,
  9303,
  6377,
  6654,
  6572,
  7892,
].each do |job_id|
  move_tesla_job(job_id, 471, 480)
end

[
  10298,
  9829,
  6431,
  6010,
  3006,
].each do |job_id|
  move_tesla_job(job_id, 216, 480)
end

[26017, 26235, 24223].each do |job_id|
  move_tesla_job(job_id, 471, 878)
end
