Job.find_by_sql("select * from jobs where id not in (select job_id from audit_job_statuses where job_status_id=9 and job_id > 417891) and id > 417891 and status not in ('Draft','Predraft','Deleted')").each do |job|
  js = JobStatus.find_by(name: 'Created')
  job.audit_job_statuses.build(job_status: js, last_set_dttm: job.created_at, company: job.fleet_company, user: job.user)
  job.save!
end
