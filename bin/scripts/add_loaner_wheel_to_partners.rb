service = ServiceCode.find_by(name: 'Loaner Wheel')
RescueCompany.all.each do |company|
  sc = company.service_codes.find_by(name: 'Loaner Wheel')
  if !sc
    company.service_codes << service
    company.save!
  end
end
