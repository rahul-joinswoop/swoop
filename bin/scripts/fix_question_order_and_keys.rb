def add_question_order(question, order, key = nil)
  q = Question.where(question: question).first_or_create
  q.order_num = order
  if key
    q.name = key
  end
  q.save
end

count = 0
add_question_order("Keys present?", count += 1)
add_question_order("Customer with vehicle?", count += 1)
add_question_order("Has customer been contacted?", count += 1)
add_question_order("Is the vehicle on or running?", count += 1)
add_question_order("Where are the keys located?", count += 1)
add_question_order("Children or pets locked inside?", count += 1)
add_question_order("Vehicle can be put in neutral?", count += 1, "neutral")
add_question_order("What is the gasoline type?", count += 1)
add_question_order("Did the vehicle stop when running?", count += 1)
add_question_order("Anyone attempted to jump it yet?", count += 1)
add_question_order("Which tire is damaged?", count += 1)
add_question_order("Have a spare in good condition?", count += 1)
add_question_order("Has vehicle moved today?", count += 1)
add_question_order("GPS tracking on vehicle?", count += 1)
add_question_order("Vehicle driveable after winch out?", count += 1)
add_question_order("Vehicle 4 wheel drive?", count += 1)
add_question_order("Within 10 ft of a paved surface?", count += 1)
add_question_order("Low clearance?", count += 1, "low-clearance")
add_question_order("Will customer ride with tow truck?", count += 1)
add_question_order("Shuttle service?", count += 1, 'shuttle')
add_question_order("Alternate transportation?", count += 1, 'alt-transport')
add_question_order("VIP customer?", count += 1, 'vip')
