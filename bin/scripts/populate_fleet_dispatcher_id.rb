tesla = Company.find_by(name: "Tesla")

Job.where(fleet_company: tesla).each do |job|
  job.update_columns(fleet_dispatcher_id: job.user_id)
end
