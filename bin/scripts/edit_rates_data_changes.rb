###
# Remove Impound Service from Turo Company
# puts "Removing Impound Service from Turo Company"
turo_company = Company.find_by_name('Turo')
puts "Company found #{turo_company.name}"

impound_service_code = ServiceCode.find_by_name('Impound')
puts "Service Code found #{impound_service_code.name}"

CompaniesService.where(company: turo_company, service_code: impound_service_code).each do |company_service|
  puts "Destroying Company Service id #{company_service.id}"

  company_service.destroy
end

###
# Add Services to Big Valley Company
puts "Adding Services to Big Valley Towing"

services_names_to_big_valley = ['Service Call', 'Rollover', 'Decking', 'Undecking']
big_valley_company = Company.find_by_name('Big Valley Towing')

services_names_to_big_valley.each do |new_service_name|
  service = ServiceCode.where(name: new_service_name).first_or_create

  if !big_valley_company.service_codes.include?(service)
    puts "Service Name to be associated #{new_service_name}"
    big_valley_company.service_codes << service
  end
end

swap_fee_addon = ServiceCode.where(name: 'Swap Fee', addon: true).first_or_create
if !big_valley_company.service_codes.include?(swap_fee_addon)
  puts "Service Addon to be associated #{swap_fee_addon}"
  big_valley_company.service_codes << swap_fee_addon
end

big_valley_company.save!

###
# Create Kenworth make
VehicleMake.where(name: 'Kenworth', original: true).first_or_create

###
# Create "Reimbursement" and "Overnight Storage" as Service addons
puts 'Create "Reimbursement" and "Overnight Storage" as Service addons'

reimbursement = ServiceCode.where(name: 'Reimbursement', addon: true).first_or_create
overnight_storage = ServiceCode.where(name: 'Overnight Storage', addon: true).first_or_create
priority_response = ServiceCode.where(name: 'Priority Response', addon: true).first_or_create

###
# Add "Reimbursement", "Overnight Storage" and "Priority Response" addons for Tesla
puts 'Add "Reimbursement", "Overnight Storage" and "Priority Response" addons for Tesla'

tesla = Company.find_by_name('Tesla')
CompaniesService.where(company_id: tesla.id, service_code_id: reimbursement.id).first_or_create
CompaniesService.where(company_id: tesla.id, service_code_id: overnight_storage.id).first_or_create
CompaniesService.where(company_id: tesla.id, service_code_id: priority_response.id).first_or_create

###
# Add "Reimbursement", "Overnight Storage" and "Priority Response" addons for all RescueCompanies
puts 'Add "Reimbursement" and "Overnight Storage" addons for all RescueCompanies'

RescueCompany.all.each do |rescue_company|
  CompaniesService.where(company_id: rescue_company.id, service_code_id: reimbursement.id).first_or_create
  CompaniesService.where(company_id: rescue_company.id, service_code_id: overnight_storage.id).first_or_create
  sleep 0.01 # .01 * 6000 = 1 minute of waiting to spread things out
end

puts 'Adds invoice reject reasons for enterprise'

enterprise = Company.find_by_name('Enterprise')
InvoiceRejectReason.where(company_id: enterprise.id, text: 'Rates outside of rate agreement').first_or_create
InvoiceRejectReason.where(company_id: enterprise.id, text: 'More explanation needed for charges').first_or_create
InvoiceRejectReason.where(company_id: enterprise.id, text: 'GOA rate outside of agreement').first_or_create
InvoiceRejectReason.where(company_id: enterprise.id, text: 'Canceled job').first_or_create
