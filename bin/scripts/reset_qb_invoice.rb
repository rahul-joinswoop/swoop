[234461, 238184, 234919, 239000, 233233, 234377, 244280, 24245, 243122, 232823, 236186, 235890].each do |job_id|
  Invoice.where(job_id: job_id, deleted_at: nil).each do |invoice_or_payment|
    invoice_or_payment.qb_imported_at = nil
    invoice_or_payment.save!
  end
end
