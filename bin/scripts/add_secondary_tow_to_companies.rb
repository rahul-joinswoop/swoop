service_name = 'Secondary Tow'
service = ServiceCode.find_by(name: service_name)
Company.all.each do |company|
  sc = company.service_codes.find_by(name: service_name)
  if !sc
    company.service_codes << service
    company.save!
  end
end
