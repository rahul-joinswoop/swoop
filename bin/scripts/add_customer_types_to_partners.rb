types = ["Account", "Cash", "Check", "Comchek/EFS Check", "Credit Card", "PO"]
db_types = []
types.each do |type|
  db_types << CustomerType.find_by!(name: type)
end

RescueCompany.all.each do |company|
  db_types.each do |type|
    sc = company.customer_types.find_by(name: type.name)
    if !sc
      company.customer_types << type
    end
  end
end
