settings = Setting.find_by_sql("
	with dupes as (
		select company_id, key, count(key) as c, max(created_at) as created_at
		from settings
		group by company_id, key
		having count(key) > 1
	)
	select settings.*
	from settings
	join dupes on
		settings.company_id = dupes.company_id
		and settings.key = dupes.key
		and settings.created_at = dupes.created_at;
")
settings.each do |s|
  Setting.where(company_id: s.company_id, key: s.key).where.not(id: s.id).delete_all
end

user_settings = UserSetting.find_by_sql("
	with dupes as (
		select user_id, key, count(key) as c, max(created_at) as created_at
		from user_settings
		group by user_id, key
		having count(key) > 1
	)
	select user_settings.*
	from user_settings
	join dupes on
		user_settings.user_id = dupes.user_id
		and user_settings.key = dupes.key
		and user_settings.created_at = dupes.created_at;
")
user_settings.each do |s|
  UserSetting.where(user_id: s.user_id, key: s.key).where.not(id: s.id).delete_all
end
