tesla = Company.find_by(name: "Tesla")
tsc_name = "Tesla Service Center"

# !!!!!!!!!!!!!!!!!
# accounting_email="paul+tscaccounting@joinswoop.com"
accounting_email = "towing@teslamotors.com"

Account.where(name: "Tesla").each do |account|
  rc = account.company
  existing = rc.accounts.find_by(name: tsc_name)
  if !existing
    rc.accounts.build(company: rc, name: tsc_name, accounting_email: accounting_email)
    rc.save
  end
  tsc_account = rc.accounts.find_by!(name: tsc_name)
  tesla_account = account
  if Rate.where(company: rc, account: tsc_account, deleted_at: nil).length == 0
    Rate.transaction do
      Rate.where(company: rc, account: tesla_account, deleted_at: nil).each do |rate|
        args = rate.attributes
        args[:id] = nil
        args[:account] = tsc_account
        args[:import_id] = -2
        args[:import_name] = "Tesla -> Tesla Service Center Copy"
        args[:no_invoice_updates] = true
        args[:no_mail] = true
        Rate.create(args)
      end
    end
  end
end
