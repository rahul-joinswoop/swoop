# There are about 750 of these records on production
# The purpose of this file is to split out user settings around tables (their col widths and sorts)
# Currently we are saving all active tables as the same,
# this will split out users previous single setting into all of the possible tables someone could turn on
# NOTE: these do go out to all companies users so we need to not send them all out at once
UserSetting.where(key: "activeJobs_sortby").find_in_batches do |settings|
  settings.each do |setting|
    setting.update_column(:key, "pendingJobs_sortby")
    UserSetting.create(key: "dispatchedJobs_sortby", value: setting.value, user_id: setting.user_id)
    UserSetting.create(key: "scheduledJobs_sortby", value: setting.value, user_id: setting.user_id)
    sleep 0.1 # 750 settings * 0.01 = 75 s of sleeping
  end
end

UserSetting.where(key: "col_width_activeJobs").find_in_batches do |settings|
  settings.each do |setting|
    setting.update_column(:key, "col_width_pendingJobs")
    UserSetting.create(key: "col_width_dispatchedJobs", value: setting.value, user_id: setting.user_id)
    UserSetting.create(key: "col_width_scheduledJobs", value: setting.value, user_id: setting.user_id)
    sleep 0.1 # 750 settings * 0.01 = 75 s of sleeping
  end
end
