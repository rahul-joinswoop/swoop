# Job.all.find_in_batches do |jobs|

def fixup
  fixed = 0
  Job.order(:id).find_in_batches do |jobs|
    jobs.each do |job|
      new_ajs_created = AdjustedCreatedAtCalculator.new(job).calculate
      if job.adjusted_created_at != new_ajs_created
        Rails.logger.debug("AJS_FIXUP Bad adjusted_created_at: #{job.id} expected:#{new_ajs_created} got:#{job.adjusted_created_at}")
        job.update_column(:adjusted_created_at, new_ajs_created)
        fixed += 1
      else
        # Rails.logger.debug("AJS_FIXUP OK: #{job.id}")
      end
    end
  end
  fixed
end

fixup
