tesla = Company.find_by(name: 'Tesla')
flt = Company.find_by(name: 'Finish Line Towing')
warranty = CustomerType.find_by(name: 'Warranty')
roadside = Department.find_by(name: 'Roadside', company: tesla)
tesla_service_center_account = Account.find_by(company: flt, name: 'Tesla Service Center')

[5057, 5336, 5678, 5628, 5703, 5764, 6112, 6677, 7337, 7334, 7397, 7548, 7642, 7706, 8082, 8398, 8680, 8694, 9184, 12275, 13081, 20335, 21793, 22972, 26093, 26456, 34136, 34137, 48181, 48561, 49416, 49413, 49415, 53596, 55729, 56034, 25521, 61402, 62443, 63645, 63846, 63924, 66321, 66281, 67251, 78983, 68059, 68963, 73000, 73213, 78518, 76828]

[135920, 135958, 135917, 135915, 135921, 135913, 135909, 135916, 135911, 135923, 135910, 135926, 135961, 135964, 135925, 135918, 135919, 135912]

[5872].each do |job_id|
  Job.transaction do
    job = Job.find(job_id)
    raise ArgumentError, "Not created by FLT" unless job.fleet_company == flt
    raise ArgumentError, "Not assigned to Tesla Service Center" unless job.account == tesla_service_center_account
    job.fleet_company = tesla
    job.invoice.deleted_at = Time.now
    job.invoice.save
    job.account = CreateAccountForJobService.new(job, tesla).call
    job.department = roadside
    job.customer_type = warranty
    job.invoice = nil
    job.type = 'FleetInHouseJob'
    job.save!
    CreateOrUpdateInvoice.perform_async(job_id)
    # PDW need to take account of state, if it's in Pending then should be moved to Assigned or set fleet_manual
  end
end

[5872, 5057, 5336, 5678, 5628, 5703, 5764, 6112, 6677, 7337, 7334, 7397, 7548, 7642, 7706, 8082, 8398, 8680, 8694, 9184, 12275, 13081, 20335, 21793, 22972, 26093, 26456, 34136, 34137, 48181, 48561, 49416, 49413, 49415, 53596, 55729, 56034, 25521, 61402, 62443, 63645, 63846, 63924, 66321, 66281, 67251, 78983, 68059, 68963, 73000, 73213, 78518, 76828].each do |job_id|
  job = Job.find(job_id)
  job.type = 'FleetInHouseJob'
  job.save!
end

Invoice.where(job_id: [5872, 5057, 5336, 5678, 5628, 5703, 5764, 6112, 6677, 7337, 7334, 7397, 7548, 7642, 7706, 8082, 8398, 8680, 8694, 9184, 12275, 13081, 20335, 21793, 22972, 26093, 26456, 34136, 34137, 48181, 48561, 49416, 49413, 49415, 53596, 55729, 56034, 25521, 61402, 62443, 63645, 63846, 63924, 66321, 66281, 67251, 78983, 68059, 68963, 73000, 73213, 78518, 76828]).each do |invoice|
  p "state: #{invoice.job_id} #{invoice.state}"
end
