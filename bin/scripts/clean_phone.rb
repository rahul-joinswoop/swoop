invalid = []

Company.find_each do |c|
  p "#{c.id}:#{c.name}"
  if !c.valid?
    invalid << [c.id, c.name]
  end
  if c.changed?
    c.save!
    sleep(1)
  end
end
