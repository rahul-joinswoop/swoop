departments = ["Light Duty", "Heavy Duty", "Recovery", "Transport"]
southern = RescueCompany.find_by!(name: "Southern Wrecker & Recovery")
departments.each do |dept|
  service = Department.where(name: dept, company: southern).first_or_create
end
