farmers = FleetCompany.find_by!(name: "Farmers Insurance")

symptom1 = Symptom.find_by!(name: "Unauthorized use")
symptom2 = Symptom.find_by!(name: "Vehicle abandoned")

farmers.symptoms.delete(symptom1)
farmers.symptoms.delete(symptom2)
