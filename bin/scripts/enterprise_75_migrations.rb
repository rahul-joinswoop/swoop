### Add new ClassTypes for Technique Towing
# https://swoopme.atlassian.net/browse/ENG-1532 and
# https://swoopme.atlassian.net/browse/ENG-1598
#
db_categories_for_technique_towing = []

['Oakhurst', 'Mariposa', 'Motorcycle'].each do |new_category|
  db_categories_for_technique_towing << VehicleCategory.find_or_create_by!(name: new_category)
end

technique_towing_company = RescueCompany.find_by!(name: 'Technique Towing')

db_categories_for_technique_towing.each do |cat|
  sc = technique_towing_company.vehicle_categories.find_by(name: cat.name)

  if !sc
    technique_towing_company.vehicle_categories << cat

    technique_towing_company.save!
  end
end

### Add new ClassTypes for Finish Line Towing
# https://swoopme.atlassian.net/browse/ENG-1598
#
db_categories_for_finish_line = []

[
  'Light Duty After Hours', 'Medium Duty After Hours', 'Heavy Duty After Hours',
  'Super Heavy After Hours',
].each do |new_category|
  db_categories_for_finish_line << VehicleCategory.find_or_create_by!(name: new_category)
end

finish_line_company = RescueCompany.find_by!(name: 'Finish Line Towing')

db_categories_for_finish_line.each do |cat|
  sc = finish_line_company.vehicle_categories.find_by(name: cat.name)

  if !sc
    finish_line_company.vehicle_categories << cat

    finish_line_company.save!
  end
end

### Add new ClassTypes for Big Valley Towing
# https://swoopme.atlassian.net/browse/ENG-1534
#
db_categories_for_big_valley_towing = []

['Rotator', 'Tractor Only', 'Landoll'].each do |new_category|
  db_categories_for_big_valley_towing << VehicleCategory.find_or_create_by!(name: new_category)
end

big_valley_company = RescueCompany.find_by!(name: 'Big Valley Towing')

db_categories_for_big_valley_towing.each do |cat|
  sc = big_valley_company.vehicle_categories.find_by(name: cat.name)

  if !sc
    big_valley_company.vehicle_categories << cat

    big_valley_company.save!
  end
end
