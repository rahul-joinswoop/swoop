# LOCAl
company_name = '21st Century Insurance'
s = SurveyService::SetupSmiley.new(company_name,
                                   "#{company_name} Smiley Survey",
                                   'localhost:3000',
                                   [
                                     ['TenStarQuestion', 'How likely are you to recommend us to family, friends, or colleagues?', 'Question 1 - Recommend (NPS)'],
                                     ['TenStarQuestion', 'How well were you kept informed while waiting for your towing or roadside service provider to arrive?', 'Question 2 - Kept Informed'],
                                     ['TenStarQuestion', 'Did the tow or roadside service provider arrive within the initial service response time promised?', 'Question 3 - ATA < ETA'],
                                     ['LongTextQuestion', 'Thanks for your rating! How do you think we can improve?', 'Question 5 - Improvements'],
                                   ]).call

# STAGING

company_name = '21st Century Insurance'
s = SurveyService::SetupSmiley.new(company_name,
                                   "#{company_name} Smiley Survey",
                                   'staging.21stcenturysurvey.com',
                                   [
                                     ['TenStarQuestion', 'How likely are you to recommend us to family, friends, or colleagues?', 'Question 1 - Recommend (NPS)'],
                                     ['TenStarQuestion', 'How well were you kept informed while waiting for your towing or roadside service provider to arrive?', 'Question 2 - Kept Informed'],
                                     ['TenStarQuestion', 'Did the tow or roadside service provider arrive within the initial service response time promised?', 'Question 3 - ATA < ETA'],
                                     ['LongTextQuestion', 'Thanks for your rating! How do you think we can improve?', 'Question 5 - Improvements'],
                                   ]).call

# PROD
company_name = '21st Century Insurance'
s = SurveyService::SetupSmiley.new(company_name,
                                   "#{company_name} Smiley Survey",
                                   '21stcenturysurvey.com',
                                   [
                                     ['TenStarQuestion', 'How likely are you to recommend us to family, friends, or colleagues?', 'Question 1 - Recommend (NPS)'],
                                     ['TenStarQuestion', 'How well were you kept informed while waiting for your towing or roadside service provider to arrive?', 'Question 2 - Kept Informed'],
                                     ['TenStarQuestion', 'Did the tow or roadside service provider arrive within the initial service response time promised?', 'Question 3 - ATA < ETA'],
                                     ['LongTextQuestion', 'Thanks for your rating! How do you think we can improve?', 'Question 5 - Improvements'],
                                   ]).call
