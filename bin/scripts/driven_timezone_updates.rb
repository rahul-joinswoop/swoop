timezone = 'America/New_York'
usernames = ['Alba.Gusija', 'Alison.Roybal', 'Alyse.Takacs', 'Andrea.Vachon', 'Andrew.Worden', 'Aria.Booth', 'Asia.Lukasiewicz', 'Cassie.Schaefer', 'Charity.Labranche', 'Christine.Hartley', 'Damon.Ataei', 'David.Palacio', 'Dean.Schram', 'Denise.Marentette', 'Emily.Booth', 'Emma.Gamelin', 'Farida.Elham', 'Irene.Gervais', 'Jakob.Konrad', 'James.Carr', 'Jessica.Revenberg', 'John.Williamson', 'Julianne.Hood', 'Kat..Caine', 'Kevin.Cracknell', 'Kevin.Mourant', 'Kevin.Smith', 'Kyle.Moore', 'Lane.Sargeant', 'Margaret.Salvona', 'Mariam.Elnazali', 'Marianna.Elnazali', 'Matt.Gibson', 'Mike.Grant', 'Nicolas.Dubois', 'Nicole.Lukasiewicz', 'Rabia.Nasrat', 'Rachel.White', 'Rahmat.Nasrat', 'Sarah.Caddey', 'Shane.Smith', 'Shania.Ardiel', 'Thiago.Kitahara', 'Adam.Crann', 'Alec.Martin', 'Dan.Bengough', 'David.Rawlings', 'Jason.Hastings', 'Jose.Lopez', 'Lana.Gillanders', 'Nathan.Jardine', 'Cory.Shapwaykeesic', 'Geoffrey.Brooks']

timezone = 'America/Chicago'
usernames = ['Brenda.Guzman', 'Eduardo.Duran', 'Elizabeth.Silerio', 'Jordan.Richards', 'Kay.Cumberbatch', 'Larry.Newburg', 'LaTanya.Carroll', 'Michael.Kephart', 'Sergio.Lopez']

missed = []
usernames.each do |username|
  user = User.find_by(username: username.downcase)
  if user
    user.timezone = timezone
    user.save!
  else
    missed << username
  end
end

p "Missed #{missed}"

# Central 'America/Chicago'
# ['Abigail.Sanchez','Amanda.Jackson','Amber McCrary','Aria.Washington','Arleen.Sproull','Berenise.Garcia','Brenda.Alvarado','Brenda.Guzman','Brittany.Iosua','Carrie.Washington','Chloe.Shaw-Mitchell','Davia.Small','Elizabeth.Silerio','Eric.Reyes','Erika.Bonal','Felicia.Lewis','Frank.Chavez','Jasmine.Juarez','Jasmine.Keller','Jazmine.Morales','Jimmy .Deluna','Jordan.Richards','Jorge.Silverio','Joselyn.Medrano','Josue.Alarcon','Kecia.Hayes-Powell','LaCrezha.Jones','LaTanya.Carroll','Linda.Saiyasith','Lissette.Mancebo','Lucy.Vazquez','Lynda.Means','Maria.Ojeda','Maria.Sanchez-Nava','Marlene.Chavez','Mary.Giles','Nawal.Amous','Nicole.Davis-Moore','Phillip.Carter','Sarai.Lujan','Sheila.Lewis','Susana.Nieto','Tania.Sanchez','Therissia.Robinson','Tina.Vogt','Tomas.Gallegos','Tory.Clark','Xiomara.Benavidez','breana.keller','david.monye','daniel.perez','heidi.escobar','Jessica.Evaschuk','Samantha.Clark','Christine.Rowe','Martisha.Mayombi','Fermin.Sanchez','Philip.Verney']
