expected =
  Job.joins(:invoice).where(deleted_at: nil,
                            status: ['Completed', 'GOA', 'Stored', 'Canceled'],
                            invoicing_ledger_items: {
                              deleted_at: nil,
                              state: 'partner_new',
                              qb_imported_at: nil,
                            }).where.not(invoicing_ledger_items: { paid: true }).count

count = 0
Job.joins(:invoice).where(deleted_at: nil,
                          status: ['Completed', 'GOA', 'Stored', 'Canceled'],
                          invoicing_ledger_items: {
                            deleted_at: nil,
                            state: 'partner_new',
                            qb_imported_at: nil,
                          }).where.not(invoicing_ledger_items: { paid: true }).where("invoicing_ledger_items.updated_at < '2018-01-26 18:25:43.641224+00'").order(:id).find_each(batch_size: 100) do |job|
  invoice = job.invoice
  Rails.logger.debug "ENG-4290(#{count}) Updating invoice for rounding: INVOICE #{invoice.id}, JOB(#{invoice.job_id})"
  CreateOrUpdateInvoice.new.perform(job.id)
  count += 1
end

Rails.logger.debug "ENG-4290 updated #{count} records"
