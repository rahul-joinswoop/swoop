categories = ["Flatbed", "Wheel Lift", "Medium Duty", "Light Duty", "Heavy Duty", "Super Heavy"]
db_cats = []
categories.each do |cat|
  db_cats << VehicleCategory.find_by!(name: cat)
end

Company.all.each do |company|
  db_cats.each do |cat|
    sc = company.vehicle_categories.find_by(name: cat.name)
    if !sc
      company.vehicle_categories << cat
      company.save!
    end
  end
end

categories = ["Rotator", "Carrier", "Sliding Axle", "Lowboy", "Multi-Axle Lowboy", "Stepdeck", "Stretch"]
db_cats = []
categories.each do |cat|
  db_cats << VehicleCategory.find_by!(name: cat)
end

company = RescueCompany.find_by!(name: "Southern Wrecker & Recovery")
db_cats.each do |cat|
  sc = company.vehicle_categories.find_by(name: cat.name)
  if !sc
    company.vehicle_categories << cat
    company.save!
  end
end
