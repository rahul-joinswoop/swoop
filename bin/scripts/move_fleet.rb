# fleet managed move

source = Company.find_by!(name: 'Farmers Insurance')
dest = Company.find_by!(name: 'Farmers 2017')

Job.where(fleet_company: source).find_each(batch_size: 100) do |job|
  Job.transaction do
    raise ArgumentError, "Not created by FLT" unless job.fleet_company == source
    job.fleet_company = dest
    job.save!
  end
end

source = Company.find_by!(name: 'Farmers Insurance')
dest = Company.find_by!(name: 'Farmers 2017')

acct = Account.where(company: Company.swoop, client_company: dest).first
if !acct
  acct = Account.create(company: Company.swoop, client_company: dest, name: dest.name)
end

Invoice.where(recipient_company: source, sender: Company.swoop).find_each(batch_size: 100) do |invoice|
  invoice.recipient_company = dest
  invoice.recipient = acct
  invoice.save!
end
