Job.where(user_id: nil).each do |job|
  if job.parent
    job.user_id = job.parent.user_id
    job.save
  end
end
