User.where("users.phone not like '+1%'").each do |user|
  if user.phone && (user.phone.length == 11)
    new_phone = "+1#{user.phone[1..-1]}"
    p "#{user.phone} -> #{new_phone}"
    user.update_column(:phone, new_phone)
  else
    p "#{user.phone} not valid"
  end
end
