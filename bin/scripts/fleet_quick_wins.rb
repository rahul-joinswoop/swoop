# Add departments to Turo
ApplicationRecord.transaction do
  turo = Company.find_by_name('Turo')

  Department.find_or_create_by(
    name: 'Roadside',
    company: turo,
    sort_order: 0
  )

  Department.find_or_create_by(
    name: 'Special Situations',
    company: turo,
    sort_order: 1
  )
end

# ******** TravelCar setup ********#
ApplicationRecord.transaction do
  travelcar_name = 'TravelCar'

  travelcar = FleetCompany.find_by_name(travelcar_name)

  unless travelcar
    travelcar = FleetCompany.new(name: travelcar_name)
    travelcar.defaults

    travelcar.save!
  end

  # add departments
  Department.find_or_create_by(
    name: 'Roadside',
    company: travelcar,
    sort_order: 0
  )

  Department.find_or_create_by(
    name: 'Claims',
    company: travelcar,
    sort_order: 1
  )

  # add sites
  FleetSite.find_or_create_by(
    name: 'LA',
    company: travelcar
  )

  FleetSite.find_or_create_by(
    name: 'SF',
    company: travelcar
  )

  # Add Payment Type Feature
  unless travelcar.features.map(&:name).include? Feature::CUSTOMER_TYPE
    customer_type_feature = Feature.find_by_name(Feature::CUSTOMER_TYPE)

    travelcar.features << customer_type_feature
  end

  # Add the default Payment Types
  CompanyService::IncludePaymentTypes.new(
    company: travelcar,
    payment_type_names: Company::DEFAULT_PAYMENT_TYPE_NAMES
  ).call

  # Add 'Vehicle abandoned' symptom (the defaults are set through FleetCompany#defaults call)
  vehicle_abandoned_symptom = Symptom.find_or_create_by(name: 'Vehicle abandoned')

  unless travelcar.symptoms.map(&:name).include? vehicle_abandoned_symptom.name
    travelcar.symptoms << vehicle_abandoned_symptom
  end

  # Add services (the defaults are set through FleetCompany#defaults call)
  travelcar_services_names = travelcar.service_codes.map(&:name)

  [ServiceCode::ACCIDENT_TOW, ServiceCode::REPO, ServiceCode::TRANSPORT].each do |service_name|
    db_service = ServiceCode.find_or_create_by(name: service_name)

    unless travelcar_services_names.include? service_name
      travelcar.service_codes << db_service if db_service
    end
  end

  # Add questions per service
  travelcar.service_codes.each do |service|
    question_per_specific_service = Question::DEFAULT_QUESTIONS_PER_SERVICE[service.name]

    if question_per_specific_service
      question_per_specific_service.each do |question|
        db_question = Question.find_or_create_by(question: question)

        travelcar.company_service_questions.where(
          service_code_id: service.id,
          question_id: db_question.id
        ).first_or_create
      end
    end
  end

  # Make sure all the answers are available
  uniq_questions = Question::DEFAULT_QUESTIONS_PER_SERVICE.values.flatten.uniq

  uniq_questions.each do |question|
    db_question = Question.find_by_question(question)

    default_answers_for_specific_question = Answer::DEFAULT_ANSWERS_PER_QUESTION[question]

    default_answers_for_specific_question.each do |answer|
      Answer.find_or_create_by(
        answer: answer,
        question_id: db_question.id
      )
    end
  end
end
