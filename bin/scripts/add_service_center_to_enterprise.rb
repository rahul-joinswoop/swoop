service_center = "Service Center"
symptom = Symptom.where(name: service_center).first_or_create
enterprise = FleetCompany.find_by!(name: "Enterprise")
enterprise.add_symptoms_by_name([service_center])

CustomerType.where(name: service_center).first_or_create
enterprise.add_customer_types_by_name([service_center])
