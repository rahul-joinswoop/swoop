require 'street_address'
Location.where(street: nil, state: nil, city: nil, zip: nil, country: nil).where.not(address: nil).find_each do |loc|
  p loc
  if loc.address && !(loc.street || loc.state || loc.city || loc.zip || loc.country)
    address = StreetAddress::US.parse(loc.address)

    if address
      args = []
      if address.number
        args << address.number
      end
      if address.unit_prefix
        args << address.unit_prefix
      end
      if address.unit
        args << address.unit
      end
      if address.street
        args << address.street
      end
      if address.street_type
        args << address.street_type
      end
      street = args.join(" ")

      postal_args = []
      if address.postal_code
        postal_args << address.postal_code
      end
      if address.postal_code_ext
        postal_args << address.postal_code_ext
      end
      zip = postal_args.join("-")

      loc.update_columns({
        street: street,
        city: address.city,
        state: address.state,
        zip: zip,
        country: 'US',
        autosplit: true,
      })
      true
    end
  end
end
