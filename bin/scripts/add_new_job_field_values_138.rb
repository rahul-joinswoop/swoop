tow = ServiceCode.find_by!(name: "Tow")
battery = ServiceCode.find_by!(name: "Battery Jump")
towifjumpfails = ServiceCode.find_by!(name: "Tow if Jump Fails")
flattire = ServiceCode.find_by!(name: ServiceCode::TIRE_CHANGE)
repo = ServiceCode.find_by!(name: "Repo")
fuel = ServiceCode.find_by!(name: ServiceCode::FUEL_DELIVERY)
lockout = ServiceCode.find_by!(name: "Lock Out")
winch = ServiceCode.find_by!(name: "Winch Out")

farmers = FleetCompany.find_by!(name: "Farmers Insurance")
turo = FleetCompany.find_by!(name: "Turo")

symptoms = ["Accident", "Customer not safe driving", "Dead battery", "Flat tire", "Locked out", "Long distance tow", "Mechanical issue", "Out of fuel", "Recovery", "Unauthorized use", "Unknown", "No issue reported", "Vehicle abandoned", "Other"]
symptoms.each do |symptom|
  Symptom.where(name: symptom).first_or_create
end

customer_pay = CustomerType.where(name: "Customer Pay").first_or_create
covered = CustomerType.where(name: "Covered").first_or_create

settings = Feature.find_by!(name: "Settings")
invoicing = Feature.find_by!(name: "Invoicing")
storage = Feature.find_by!(name: "Storage")
paymentType = Feature.find_by!(name: "Payment Type")
questions = Feature.find_by!(name: "Questions")
symptomsFeature = Feature.find_by!(name: "Show Symptoms")

farmers.symptoms = []
symptoms.each do |symptom|
  s = Symptom.find_by!(name: symptom)
  farmers.symptoms << s
end

farmers.features << questions # Put these in seeds
farmers.features << symptomsFeature # Put in seeds
farmers.features << paymentType # Put these in seeds
farmers.customer_types << customer_pay # Put these in seeds
farmers.customer_types << covered # Put these in seeds

farmers.save!

symptoms.each do |symptom|
  s = Symptom.find_by!(name: symptom)
  turo.symptoms << s
end
turo.features << questions
turo.features << symptomsFeature # Put in seeds
turo.features << paymentType
turo.save!

q = Question.where(question: "Keys present?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: battery, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: battery, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: towifjumpfails, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: towifjumpfails, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: flattire, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: flattire, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: repo, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: repo, question: q).first_or_create
q = Question.where(question: "Customer with vehicle?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: battery, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: battery, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: towifjumpfails, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: towifjumpfails, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: flattire, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: flattire, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: fuel, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: fuel, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: lockout, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: lockout, question: q).first_or_create
q = Question.where(question: "Did the vehicle stop when running?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: battery, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: battery, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: towifjumpfails, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: towifjumpfails, question: q).first_or_create
q = Question.where(question: "Anyone attempted to jump it yet?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: battery, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: battery, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: towifjumpfails, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: towifjumpfails, question: q).first_or_create
q = Question.where(question: "Which tire is damaged?").first_or_create
q.answers.where(answer: "Driver Front").first_or_create
q.answers.where(answer: "Passenger Front").first_or_create
q.answers.where(answer: "Driver Rear").first_or_create
q.answers.where(answer: "Passenger Rear").first_or_create
q.answers.where(answer: "Multiple").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: flattire, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: flattire, question: q).first_or_create
q = Question.where(question: "Have a spare in good condition?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: flattire, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: flattire, question: q).first_or_create
q = Question.where(question: "Is the vehicle on or running?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: lockout, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: lockout, question: q).first_or_create
q = Question.where(question: "Where are the keys located?").first_or_create
q.answers.where(answer: "Trunk (inaccessible)").first_or_create
q.answers.where(answer: "Trunk (accessbile)").first_or_create
q.answers.where(answer: "Inside Vehicle").first_or_create
q.answers.where(answer: "Visor").first_or_create
q.answers.where(answer: "Under Floor Mat").first_or_create
q.answers.where(answer: "Unknown").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: lockout, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: lockout, question: q).first_or_create
q = Question.where(question: "Customer requesting a ride?").first_or_create
q.answers.where(answer: "No").first_or_create
q.answers.where(answer: "Yes - 1 person").first_or_create
q.answers.where(answer: "Yes - 2 people").first_or_create
q.answers.where(answer: "Yes - 3+ people").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: tow, question: q).first_or_create
q = Question.where(question: "Car can be put in neutral?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
q.answers.where(answer: "Unknown").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: tow, question: q).first_or_create
q = Question.where(question: "Has customer been contacted?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: repo, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: repo, question: q).first_or_create
q = Question.where(question: "Has vehicle moved today?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: repo, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: repo, question: q).first_or_create
q = Question.where(question: "GPS tracking on vehicle?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: repo, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: repo, question: q).first_or_create
q = Question.where(question: "What is the gasoline type?").first_or_create
q.answers.where(answer: "Regular").first_or_create
q.answers.where(answer: "Plus").first_or_create
q.answers.where(answer: "Premium").first_or_create
q.answers.where(answer: "Diesel").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: fuel, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: fuel, question: q).first_or_create
q = Question.where(question: "Vehicle driveable after winch out?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: winch, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: winch, question: q).first_or_create
q = Question.where(question: "Vehicle near a paved service?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: winch, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: winch, question: q).first_or_create
