tesla = Company.find_by(name: "Tesla")
new_rates = 0
RescueCompany.all.each do |rc|
  p "Rescue company: #{rc.id}:#{rc.name}"
  rc.accounts.where(client_company: tesla).each do |account|
    p "Account #{account.id}:#{account.name}"
    changed = false
    ServiceCode.where.not(name: ['Tow', ServiceCode::TIRE_CHANGE, 'GOA', 'Battery Jump']).where(addon: false).each do |sc|
      p "Dealing with service: #{sc.name}"
      Rate.transaction do
        Rate.where(company: rc, account: account, service_code: sc, deleted_at: nil).each do |rate|
          p "Deleting #{rate.company.name} (#{rate.account.name}): #{rate.service_code.name}"
          rate.deleted_at = Time.now
          rate.save
          changed = true
        end
      end
    end
    tow_rate = Rate.find_by(company: rc, account: account, service_code: ServiceCode.find_by!(name: "Tow"), deleted_at: nil)
    default_rates = Rate.where(company: rc, account: account, service_code: nil, deleted_at: nil)
    if tow_rate
      p "Found tow rate"
      if default_rates.length == 0
        p "Copying tow to default"
        new_default = Rate.new(tow_rate.attributes)
        new_default.id = nil
        new_default.import_id = -1
        new_default.created_at = Time.now
        new_default.updated_at = nil
        new_default.service_code = nil
        new_default.save
        new_rates += 1
      end
    end
    if changed
      p "scheduling rate change for #{account.id}"
      Job.schedule_all_rate_invoice_updates(account)
    end
  end
end
p new_rates
