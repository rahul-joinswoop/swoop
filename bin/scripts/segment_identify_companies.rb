Company.where(deleted_at: nil).find_in_batches(batch_size: 500) do |company_group|
  company_group.each { |company| company.identify_on_analytics }
end

Analytics.flush
