Invoice.where(deleted_at: nil).each do |invoice|
  invoice.calculate_total_amount
  invoice.update_column(:total_amount, invoice.total_amount)
  invoice.update_column(:fleet_ui_total_amount, invoice.fleet_ui_total_amount)
end
