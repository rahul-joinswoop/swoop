#!/usr/bin/env ruby

APP = 'swoopmestaging'.freeze

DEBUG = true

PRIMARY = "HEROKU_POSTGRESQL_ROSE_URL".freeze
FOLLOWER = "HEROKU_POSTGRESQL_TEAL_URL".freeze

UPGRADE = true

DYNOS = [[:web, 2], [:clock, 1], [:sidekiq, 1], [:worker, 1]].freeze

def run(cmd)
  if DEBUG
    p cmd
  else
    system "bash", "-c", cmd

    if $CHILD_STATUS.exitstatus == 0
      puts "success: #{cmd}"
    else
      raise Exception, "fail #{$CHILD_STATUS.exitstatus}: #{cmd}"
    end
  end
end

def run_app(cmd)
  run("#{cmd} -a #{APP}")
end

def create_follower
  run_app("heroku addons:create heroku-postgresql:standard-2 --follow #{PRIMARY}")
end

def shutdown_dynos
  DYNOS.each do |dyno|
    num = 0
    run_app("heroku ps:scale #{dyno}=#{num}")
  end
end

def startup_dynos
  DYNOS.each do |dyno, num|
    run_app("heroku ps:scale #{dyno}=#{num}")
  end
end

def upgrade_follower
  run_app("heroku pg:upgrade #{FOLLOWER}")
end

def wait_on_db
  run_app("heroku pg:wait")
end

def promote
  run_app("heroku pg:promote #{FOLLOWER}")
end

def delay(x)
  print "Sleeping for #{x}s: "
  x.times do
    sleep(1)
    print '.'
  end
  p ''
end

def pg_info
  run_app("heroku pg:info")
end

def main
  start = Time.now
  p start
  run_app('heroku maintenance:on')
  shutdown_dynos
  delay(DEBUG ? 3 : 30)
  p Time.now
  pg_info
  p Time.now
  upgrade_follower
  p Time.now
  wait_on_db
  p Time.now
  promote
  pg_info
  p Time.now
  startup_dynos
  delay(DEBUG ? 1 : 15)
  run_app('heroku maintenance:off')
  finish = Time.now
  p "Finished at #{finish} in #{finish - start}s"
end

main
