map = {
  "Miles Towed" => "Towed Per Mile",
  "Free Miles Towed" => "Towed Free Miles",
  "En Route Miles" => "En Route Per Mile",
  "Free En Route Miles" => "En Route Free Miles",
  "Miles Deadhead" => "Deadhead Per Mile",
  "Free Miles Deadhead" => "Deadhead Free Miles",
  "Miles Port to Port" => "Port To Port Miles",
  "Port-To-Port Hours" => "Port To Port Hours",
}

(map.keys << "Hookup").each do |desc|
  InvoicingLineItem.where(description: desc).find_in_batches do |line_items|
    line_items.each do |line_item|
      if map[desc]
        puts "#{line_item.id} #{line_item.description} -> #{map[desc]}"
        line_item.update_column(:description, map[desc])
      elsif line_item.ledger_item.respond_to?(:rate_type) &&
            line_item.ledger_item.rate_type == "MilesEnRouteOnlyRate"
        puts "#{line_item.id} #{line_item.description} -> #{"Service Fee"}"
        line_item.update_column(:description, "Service Fee")
      end
    end
  end
end
