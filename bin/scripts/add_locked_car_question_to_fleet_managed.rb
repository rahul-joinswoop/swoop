turo = Company.find_by(name: 'Turo')
farmers = Company.find_by(name: 'Farmers Insurance')
luxe = Company.find_by(name: 'LuxeValet')
silver = Company.find_by(name: 'Silvercar')
q = Question.where(question: "Children or pets locked inside?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
lockout = ServiceCode.find_by!(name: "Lock Out")

CompanyServiceQuestion.where(company: turo, service_code: lockout, question: q).first_or_create
CompanyServiceQuestion.where(company: farmers, service_code: lockout, question: q).first_or_create
CompanyServiceQuestion.where(company: luxe, service_code: lockout, question: q).first_or_create
CompanyServiceQuestion.where(company: silver, service_code: lockout, question: q).first_or_create
