tco = Company.find_by(name: 'Tesla')

q = Question.where(question: "Shuttle service?").first_or_create
q.answers.where(answer: "Not Offered").first_or_create
q.answers.where(answer: "Uber").first_or_create
q.answers.where(answer: "Taxi").first_or_create
q.answers.where(answer: "Decline Service").first_or_create
ServiceCode.all.each do |service_code|
  CompanyServiceQuestion.where(company: tco, service_code: service_code, question: q).first_or_create
end

q = Question.where(question: "Alternate transportation?").first_or_create
q.answers.where(answer: "Request sent to SC").first_or_create
q.answers.where(answer: "Reservation Made by RS").first_or_create
q.answers.where(answer: "Reservation Made by Customer").first_or_create
q.answers.where(answer: "Not Applicable").first_or_create
q.answers.where(answer: "Declined").first_or_create
ServiceCode.all.each do |service_code|
  CompanyServiceQuestion.where(company: tco, service_code: service_code, question: q).first_or_create
end

q = Question.where(question: "VIP customer?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
ServiceCode.all.each do |service_code|
  CompanyServiceQuestion.where(company: tco, service_code: service_code, question: q).first_or_create
end

q = Question.where(question: "Keys present?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
ServiceCode.all.each do |service_code|
  CompanyServiceQuestion.where(company: tco, service_code: service_code, question: q).first_or_create
end

q = Question.where(question: "Customer with vehicle?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
ServiceCode.all.each do |service_code|
  CompanyServiceQuestion.where(company: tco, service_code: service_code, question: q).first_or_create
end
