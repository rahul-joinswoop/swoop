Invoice.all.each do |invoice|
  invoice.calculate_alert
  invoice.update_column(:partner_alert, invoice.partner_alert)
  invoice.update_column(:fleet_alert, invoice.fleet_alert)
end
