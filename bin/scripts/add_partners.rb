batch_size = 200
sleep_time = 0.04

PartnerSite.where(phone: nil).find_in_batches(batch_size: batch_size) do |partner_sites_group|
  puts "Adding company phone in batches of #{batch_size}"
  partner_sites_group.each do |site|
    if site.company.phone.present?
      site.phone = site.company.phone
      site.save!
      sleep(sleep_time)
    end
  end
end
