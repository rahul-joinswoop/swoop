tesla = Company.find_by(name: "Tesla")
RescueCompany.all.each do |rc| # PDW !!!!!!!!!!!!!!!!!!!!
  p "Rescue company: #{rc.id}:#{rc.name}"
  rc.accounts.where(client_company: tesla).each do |account|
    p "Account #{account.id}:#{account.name}"
    rates = Rate.where(company: rc, account: account).order(:site_id)
    if rates.length > 0
      keep_site_id = rates.first.site_id
      Rate.transaction do
        rates.each do |rate|
          p "#{rate.id} #{rate.site_id}"
          if rate.site_id == keep_site_id
            p "nil"
            rate.update_column(:site_id, nil)
          else
            if !rate.site_id.nil?
              p "destroy"
              rate.destroy
            end
          end
        end
      end
    else
      p "no site rates for #{rc.name}(#{rc.id}) #{account.name}"
    end
    p "scheduling rate change for #{account.id}"
    Job.schedule_all_rate_invoice_updates(account)
  end
end
