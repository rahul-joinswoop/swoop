tow = ServiceCode.find_by!(name: "Tow")
farmers = FleetCompany.find_by!(name: "Farmers Insurance")
turo = FleetCompany.find_by!(name: "Turo")
luxe = FleetCompany.find_by!(name: "LuxeValet")
silver = FleetCompany.find_by!(name: "Silvercar")
q = Question.where(question: "Vehicle 4 wheel drive?").first_or_create
q.answers.where(answer: "Yes").first_or_create
q.answers.where(answer: "No").first_or_create
q.answers.where(answer: "Unknown").first_or_create

CompanyServiceQuestion.where(company: farmers, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: turo, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: luxe, service_code: tow, question: q).first_or_create
CompanyServiceQuestion.where(company: silver, service_code: tow, question: q).first_or_create

q = Question.find_by(question: "Car can be put in neutral?")
if q.present?
  q.update(question: "Vehicle can be put in neutral?")
end
