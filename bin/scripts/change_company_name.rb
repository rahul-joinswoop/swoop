# frozen_string_literal: true

old = 'Woodstock Sunoco Tire & Auto'
new = 'Twinline Towing & Recovery'

company = Company.find_by!(name: old)
company.name = new
company.save!

User.where(company: company).each do |user|
  Indexer.new.perform('index', 'User', user.id)
end

if company.fleet?
  # update accounts set name='Ferrari-Maserati of Fort Lauderdale' where name='The Experience Auto Group' and fleet_company_id=4619;
  Account.where(client_company_id: company.id).each do |account|
    if account.name == old
      account.name = new
      account.save!
    end
    Indexer.new.perform('index', 'Account', account.id)
  end
elsif company.rescue?
  RescueProvider.where(provider_id: company.id).each do |rp|
    if rp.name == old
      rp.name = new
      rp.save!
    end
    Indexer.new.perform('index', 'RescueProvider', rp.id)
  end

  Job.select(:id).where(rescue_company_id: company.id).each do |job|
    Indexer.new.perform('index', 'Job', job.id)
  end
end

Indexer.new.perform('index', 'Company', company.id)
