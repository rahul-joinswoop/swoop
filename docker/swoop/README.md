# Docker Development Environment

You can run your local development environment using docker. This environment does not include the Rails or NodeJs applications. Those should still be installed locally.

The environment does include all required services like Postgres, Redis, ElasticSearch, etc. These services will be run in containers with versions that match production, so you don't need to worry about installing them via homebrew and having version mismatch with production. You should still install Postgres locally via homebrew just to get the `psql` application installed.

To start the environment install [Docker for Mac](https://docs.docker.com/docker-for-mac/install/).

Then change to this directory in a terminal window and run

```bash
docker-compose up
```

This will download all the images you need and bring them up in containers. In order to not have conflicts with other installed versions of the servers, the service ports are all remapped to the `20000` range. For instance, the Postgres server in the container is available at `localhost:25432` instead of the default `5432` port.

You will need to add the following to your `.env` file:

```
# Docker configs
DB_PORT=25432
DB_NAME=swoop_development
DB_USER=towuser
DB_PASSWORD=towpass
REDIS_URL=redis://localhost:26379/0
REDIS_CACHE_URL=redis://localhost:26379/1
ELASTICSEARCH_URL=http://localhost:29200
S3_PORT=24567
```

The docker containers will all be mounted with persistent volumes so data will be persisted across restarts.
