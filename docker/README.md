# Docker Environment

The application and all of its dependencies are containerized.

**NOTE:** All the commands need to be run at the root of the swoop repo.


## Cheatsheet
I want to... | Run this:
--- | ---
Start the app | `docker-compose up`
Shut down the app | `docker-compose down`
Wipe PostgreSQL, Redis, and Memcached data | `docker-compose down && docker volume prune && docker system prune -a`
Remove PostgreSQL data | `docker volume rm swoop_postgres_data`
Remove the webpack cache | `docker volume rm swoop_webpack_cache`
Run a test | `docker-compose run --rm web bin/rspec ./spec/...`
Run an arbitrary command | `docker-compose run --rm [service_name] bin/rails -T`
Restart the rails server | `docker-compose restart web`
Restart the node server | `docker-compose restart node`
Restart another service | `docker-compose restart [service_name]`
"ssh" to a running container | `docker-compose exec [service_name] /bin/sh`
exit a "ssh" on that container | `CTRL+d`
stop `docker-compose up` | `CTRL+c` or `docker-compose kill`


## Installation

1. **Docker Installation** 👷🛠
   - Install [Docker for Mac](https://docs.docker.com/docker-for-mac/install/)
   - Recommended Mac configuration:
     - 4 CPUs
     - 8GB memory

2. **Secrets and ejson** 🤐

   The secrets are managed by ejson [encrypted json file](https://github.com/Shopify/ejson) and are stored in the repo in the secrets.ejson file

   - **Prerequisites**
     - Ruby installed on the machine
     - Get EJSON private key from **Myke (@myke)**

   - **Install `ejson` and create key**

       ```
       gem install ejson
       mkdir ~/.keys
       touch ~/.keys/0de09735b2d454d04e1196338fd83b3fe96e79ce9a8139f43f06bef1adab7864
       ```

        - Paste the private key (you got as prerequisites) and save into the file created with the `touch` command
        - Add `EJSON_KEYDIR` to your path. If using bash, execute following from your terminal:

            ```
            export EJSON_KEYDIR=~/.keys
            ```

  - **Decrypt secrets**

     Run following from root of the repo.
     - Whenever `secrets.ejson` gets updated.

        ```
        ruby decrypt_secrets.rb
        ```

     - In case the above script throws any error run the below command:

        ```
        ejson decrypt secrets.ejson
        ```
3. **Update Seeds File (New Employees Only)**

    In [db/seeds_dev.rb](../db/seeds_dev.rb), find the `SWOOP_USERS` constant (line 229 as of this writing) and append your information.


4. **Docker Compose**

   The container structure is defined by the docker-compose.yml file in the swoop root directory.

   - **Initialize/Seed the database** (only when you are running this setup first time)

        ```
        docker-compose build
        docker-compose run web docker/initialize.sh
        ```

        This may take a while the first time as it builds the image.

     For future migrations and other rake commands, use:

     - `docker-compose run web bin/rake <command>`

   - **Bring it all together - Get the stack up and running**

     This command is used to bring up the stack **every time**. It may take a moment to boot the first time.

     - `docker-compose up`

5. **Fakes3** (only needed if you work with S3)

   To get invoices to work in a local browser, edit your hosts file to map the container DNS to your localhost.

    ```
    echo '127.0.0.1 fakes3' | sudo tee -a /etc/hosts

    # On Mac, also execute:
    sudo killall -HUP mDNSResponder
    ```

Congratulations! Your app is up and running at [http://localhost:3001](http://localhost:3001). To login, use the email you entered in Step 3 above and `joinswoop` as the password.


------
### More Information
1. **Container Port Mappings**
   List of ports exposed to the host you'll need to access services.

   ```
    web:5001
    node:3001
    postgres:5433
    redis:6380
   ```

  For more details, check [docker-compose.yml](https://github.com/joinswoop/swoop/blob/master/docker-compose.yml)
