#!/bin/bash  
./wait-for-it.sh -t 300 postgres:5432

bundle install
rake db:create
rake db:migrate
rake db:seed
rake seed:migrate
rake db:test:prepare
rake elasticsearch:test:prepare