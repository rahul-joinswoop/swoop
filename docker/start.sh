#!/bin/bash
wait_for_things()
{
  echo "Waiting 5 minutes for postgres to come up...."
  ./wait-for-it.sh -t 300 postgres:5432
  echo "Waiting 5 minutes for elasticsearch to come up...."
  ./wait-for-it.sh -t 300 elasticsearch:9200
  echo "Waiting 5 minutes for web to come up...."
  ./wait-for-it.sh -t 300 web:5000
}

verify_and_save_pkg_md5()
{
  local b=$(md5sum package.json | cut -d ' ' -f 1)
  if [[ -f "tmp/package.json.md5" ]]; then
    local a=$(cat tmp/package.json.md5)
    if [ "$a" == "$b" ]; then
      echo "No new dependencies!"
    else
      yarn install
    fi
  else
    yarn install
  fi
  echo -n $b > tmp/package.json.md5
}

if [ "$WEB" == "true" ]; then
  bundle install
  echo "Waiting 5 minutes for postgres to come up...."
  ./wait-for-it.sh -t 300 postgres:5432
  rake db:migrate
  rake seed:migrate
  bin/puma -C config/puma.rb
elif [ "$NODE" == "true" ]; then
  verify_and_save_pkg_md5
  npm start
elif [ "$CLOCK" == "true" ]; then
  wait_for_things
  bin/clockwork lib/clock.rb
elif [ "$WORKER" == "true" ]; then
  wait_for_things
  bin/rake jobs:work DEV_CACHE_CLASSES=true
elif [ "$SIDEKIQ" == "true" ]; then
  wait_for_things
  bin/sidekiq --verbose  -C config/sidekiq.yml
elif [ "$RELAY" == "true" ]; then
  echo "Waiting 5 minutes for node to install deps and become available...."
  ./wait-for-it.sh -t 300 node:3000
fi
