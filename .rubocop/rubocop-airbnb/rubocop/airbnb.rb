# frozen_string_literal: true

Dir.glob(File.expand_path('airbnb/**/*.rb', File.dirname(__FILE__))).map(&method(:require))

module RuboCop::Airbnb; end
