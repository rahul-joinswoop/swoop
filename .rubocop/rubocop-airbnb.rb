# frozen_string_literal: true

# Load original rubocop gem
require 'rubocop'

require_relative './rubocop-airbnb/rubocop/airbnb'
