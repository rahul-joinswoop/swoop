#Job ENG-6195

job.owner_sequence
job.rescue_sequence


Job json

json.first_name job.customer.first_name
json.last_name job.customer.last_name
json.phone job.customer.phone

Job TODO

job.driver_notes (don't delete before migrating this to an invoice)


#Invoice

rate_id (believe this was used on invoicing v1 (hasn't been set since June 2016)
invoice_notes (null in prod)

#Line Items

line_item.notes ? (last entry march 2016) ENG-5897


#rates ENG-5895
hourly_completion 
hourly_p2p (-> hourly) 
fleet_company_id

#accoounts
fleet_company_id ENG-5894

#vehicle ENG-5899
  tire_size #should be all null in prod

#Company ENG-5900
  primary_contact_id (not used)
