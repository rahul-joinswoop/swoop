##### Installing Homebrew

On a fresh machine, you won't have homebrew installed (or git). Run this command to install both:

``` bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew doctor
```

Accept when it asks if you want to install command line tools.

##### Setting up git

You'll need to setup git/GitHub with your SSH keys (unless you want to enter your username and password every time you make a change). Follow these instructions:

http://burnedpixel.com/blog/setting-up-git-and-github-on-your-mac/

##### Clone this Repo

```bash
$ git clone git@github.com:joinswoop/swoop.git
```

#### Installing Dependencies

##### direnv
```bash
brew install direnv
```
make sure that `eval "$(direnv hook bash)"` is the last line in your shell file (ie `~/.profile`, `~/.bashrc`, etc)

##### Redis

```bash
brew install redis
brew services start redis
```

##### Watchman

Used by Relay.

```bash
brew install watchman
```

##### Chromedriver

```bash
brew tap caskroom/cask
brew cask install chromedriver
```

##### Elastic Search

For a new laptop, install java before elasticsearch

```bash
brew cask install caskroom/versions/java8
```

```bash

brew install elasticsearch

# To automatically start elasticsearch run the following command.
brew services start elasticsearch

# Build the indices for all models
rake elasticsearch:development:prepare
```

##### Postgres

1. Download [Postgres.app](https://postgresapp.com/downloads.html) from the 'Additional Releases' Section since we're still on 9.6.
2. Drag `Postgres.app` into `Applications`
3. in a shell:
 ```bash
sudo mkdir -p /etc/paths.d
echo /Applications/Postgres.app/Contents/Versions/9.6/bin | sudo tee /etc/paths.d/postgresapp
  ```
4. Open `Postgres.app`, open the sidebar (click on the button on the bottom), and delete the initially created server.
5. Click the `+` to create a new server and select `9.6` as the version
```bash
# If this shows something else than UTC, change the timezone:
# https://ruk.si/notes/postgresql/initialize_server
psql -d postgres -c "SHOW timezone;"

# If changing the timezone, to locate the config file, run:
psql -d postgres -c "SHOW config_file;"

# Copy the path, then:
vim PATH

# In vim, type "/timezone" to locate the timezone setting. Change to:
timezone='UTC'

# Save with :wq. Stop and restart Postgres with the menubar app, then verify the change.
psql -d postgres -c "SHOW timezone;"

# Configure Swoop database users.
# SUPERUSER Needed so test fixtures can be loaded with referential integrity turned off
psql -d postgres -c " \
CREATE USER towuser WITH PASSWORD 'towpass' CREATEDB SUPERUSER; \
CREATE USER towtestuser WITH PASSWORD 'towtestpass' CREATEDB SUPERUSER;"
```

Suggestion: For a great postgres utility, install [Postico](https://eggerapps.at/postico/) It is also available on the App Store. To connect to the database on local in Postico: un: towuser, pw: towpass, db: swoop.

##### Adding correct Environment configuration

We keep a lot of local secrets in an `.env` file (we don't check this into source control). Copy the environment variables from https://docs.google.com/document/d/1ErAuJKRhRn84DGNTbSdtghsqRs89pLmuZhkno7-u1Wc/edit?usp=sharing, then copy and paste the contents into an `.env` file in the root of the project directory.

##### Ruby

Install [`rvm`](https://rvm.io/) by following the instructions on their page.

Open a new terminal window and verify `rvm` works by `cd`-ing into wherever you checked out this repo - you should get a message similar to this:

```bash
$ cd ~/git/swoop #or wherever
Required ruby-2.5.6 is not installed.
To install do: 'rvm install "ruby-2.5.6"'
```
Follow the instructions to install the correct version of ruby.

If you're using a new laptop, make sure you've done ['git SSH keygen'](https://help.github.com/articles/connecting-to-github-with-ssh/)

Make sure you're using `rvm`
```bash
# change out of the swoop directory, then back so rvm picks up the correct ruby version / bundle name
cd ..
cd -
```
##### GraphqlPro

We use [Graphql-Pro](http://graphql.pro/) which requires a license to install. You'll need to configure it like so:
```
bundle config gems.graphql.pro BUNDLE_GEMS__GRAPHQL__PRO
```
where BUNDLE_GEMS__GRAPHQL__PRO is the corresponding value in your `.env` file from above (it has to be added manually because `.env` isn't loaded up by `dotenv-rails` until after `Gemfile` is pared by `bundler` - we don't have this problem on `heroku` or `circleci` because all of our secrets are actual `ENV` variables there).

##### Bundler
Then you should be able to install your gems.

```
gem install bundler --version $(grep -A1 'BUNDLED WITH' Gemfile.lock|tail -1|sed 's/ //g')
bundle install
```

Note: `nokogiri` takes a _long_ time to build because it compiles its own version of `libxml2`. If you have `xcode` installed correctly you can speed it up by doing:
```
bundle config build.nokogiri --use-system-libraries=true --with-xml2-include="$(xcrun --show-sdk-path)"/usr/include/libxml2
```
Then `nokogiri` will use the system version of `libxml2`.


### Setup User for dev

Add yourself to the `SWOOP_USERS` section of [db/seeds_dev.rb](db/seeds_dev.rb)

Then, setup DB including the db seeds to generate your users.

### Setup your database

```bash
$ rake db:create
$ rake db:migrate

$ rake db:seed
$ rake seed:migrate
$ rake db:test:prepare
```

##### Local S3

In order to develop a feature that requires S3 (e.g. pdf generation, csv reports, etc.) we use [fakes3](https://github.com/jubos/fake-s3). This requires some certificate changes.

1. Add [`s3_server.pem`](https://github.com/joinswoop/swoop/blob/master/certs/s3_server.pem) to your Keychain.

2. Add [`s3_server.crt`](https://github.com/joinswoop/swoop/blob/master/s3_server.crt) to your browser's certificates (go to `Keychain.app` -> `File` -> `Import`). If this step is forgotten, downloads will fail with `Network error`.

`fakes3` is started automatically when you run `foreman start -f Procfile.dev`

##### Node

Install NVM to manage node installations. See https://github.com/creationix/nvm#install-script

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```

Install the supported versions of node, npm, and yarn.

```bash
export NODE_VERSION=$(grep '"node":' package.json|awk {'print $2'}|sed 's/[",]//g')
nvm install $NODE_VERSION
nvm use $NODE_VERSION
nvm alias default $NODE_VERSION
node -v
npm install -g npm@$(grep '"npm":' package.json|awk {'print $2'}|sed 's/[",]//g')
curl -o- -L https://yarnpkg.com/install.sh | bash -s -- --version $(grep '"yarn":' package.json|awk {'print $2'}|sed 's/[",]//g')
```

Or you can also simply run this script:

```bash
bin/yarn_install
```

Install modules.

```bash
yarn install
```

##### Watchman

Install watchman to handle auto reloading when javascript files change.

```bash
brew install watchman
```

Start development server on http://localhost:3000/

##### Starting development

You should now be able to run a development server:
```
foreman start -f Procfile.dev
```

##### Updating development environment

 When you checkout new code, you can update your development environment by running:

 ```bash
bin/update
```

This will apply all updates to the database, ruby gems, and javascript packages.

If your npm/node/yarn frameworks are out of date, you can update them to the supported versions with:

```bash
bin/yarn_install
```
