# PCC (Policy, Coverage & Claims)

## API

### Policy

You can search using the following methods:

* Policy Number
* VIN / Last 8 VIN
* Last Name and Zip

The initial request returns an async_request_id which can be used to match up a subsequent async response received via websockets.

Search

```
# 21st Existing
POST /policy/
{
  job_id: <job_id>
  policy_number: <policy_number>
}
->
{"async_request":{"id":<async_request_id>}}

```


```
# 21st new
POST /search/
{
  job_id: <job_id>
  search_terms:{"policy_number": <policy_number>}
}
->
{"async_request":{"id":<async_request_id>}}

# Async Response as is today





# Lincoln new VIN search
POST /search/
{
  job_id: <job_id>
  search_terms:{"vin": [<last8 vin>|<full vin>]}
}
->
{"async_request":{"id":<async_request_id>}}


# Async Response over websockets:
    {
      "class": "AsyncRequest",
      "operation": "update",
      "target": {
        "id": 1,
        "data": {
          "policies": [
            {
              "id": "1234",
              "pcc_vehicles": [
                {
                  "id": "4567657",
                  "vin": "5LMTJ3DH710484384",
                  "license_number": "LX8890",
                  "license_issuer": "WI",
                  "make": "LINCOLN",
                  "model": "MKC",
                  "year": 2019,
                  "color": "BLAC",
                  "purchase_date": "2018-12-01",
                },
              ],
              "pcc_drivers": [
                {
                  "id": "3456",
                  "name": "DELORAS FERRY",
                  "street_address": "26155 KIHN UNION",
                  "city": "PUNTA GORDA",
                  "region": "FL",
                  "postal_code": "33980",
                  "country_code": "US",
                  "phone_number": "+15559549406",
                  "business": false,
                }
              ]
            }
          ]
        }
      }





# Lincoln new Postal Code + Last Name Search
POST /search/
{
  job_id: <job_id>
  search_terms:{"postal_code": <zip>,
                "last_name": <last_name>}
}
->
{"async_request":{"id": <async_request_id>}}

# Response same as for VIN search


````



## Coverage


### 21st Century

```GET  '/policy/:policy_id/pcc_vehicle/:pcc_vehicle_id/pcc_driver/:pcc_driver_id/coverage'```


### Lincoln


```
POST '/policy/coverage' {
  pcc_policy_id:<Selected Policy>,
  pcc_vehicle_id:<Selected Policy>,
  pcc_driver_id:<Selected Policy>,
  client:<Company Name>,
  job:{
    id:<job_id:Integer>
    service:{
      name: <Service Name:String>
    },
    vehicle:{
      year:<Registration Year:Integer>,
      odometer:<Odometer Reading:Integer>
    },
    location:{
      service_location:{
        lat: <Latitude:Double>,
        lng: <Longitude:Double>,
        google_place_id:<PlaceID:String>
      },
      dropoff_location:{
        lat: <Latitude:Double>,
        lng: <Longitude:Double>,
        google_place_id:<PlaceID:String>
        site_id:<Swoop Site ID:integer>
      }
    }
  }
}

=>

{"async_request":{"id": <async_request_id>}}



# Async Response over websockets:

{
  "class": "AsyncRequest",
  "operation": "update",
  "target": {
    "id": 1,
    "data": {
      "pcc_coverage_id":<id:Integer>,
      "pcc_policy_id":<id:Integer>,
      "pcc_vehicle_id":<id:Integer>,
      "pcc_driver_id":<id:Integer>,
      "programs": [
        {
          "name":"warranty",
          "result": <Covered|PartiallyCovered|NotCovered>,
          "limitations": [<Description:String>,...],
          "coverage_notes": <notes:String>,
          "successful_conditions": [<Description:String>,...],
          "failed_conditions": [<Description:String>,...],
          "customer_payment": [<Description:String>,...],
          "coverage_notes": [<Description:String>,...]
        }
      ]
    }
  }
}


```


### Drop Off Location

```
# Lincoln new Drop Off Location site options search 

POST /root/dropoff_sites { 
  job:{
    id:<job_id:Integer>,
    program_code:<program_code from policy call:String>
    service:{
      name: <Service Name:String>
    },
    location: {
      service_location:{
        lat: <Latitude:Double>,
        lng: <Longitude:Double>,
        google_place_id:<PlaceID:String>
      }
    }
}

->
{"async_request":{"id": <async_request_id>}}

# Async Response over websockets [top 5 results - BE algorithm determines top 5]:

{
  "class": "AsyncRequest",
  "operation": "update",
  "target": {
    "id": 683,
    "data": {
      "dropoff_sites": [
        {
          "site": {
            "id": 6,
            "name": "Burlingame Dealer",
            "phone": null,
            "location": {
              "id": 31,
              "address": "1304 Howard Ave, Burlingame, CA 94010",
              "lat": 37.577121343372,
              "lng": -122.34592277512,
              "street": "1304 Howard Ave",
              "city": "Burlingame",
              "state": "CA",
              "zip": "94010",
              "exact": true,
              "updated_at": "2019-01-21T17:00:03.363Z",
              "place_id": "ChIJyy7BNfCdj4ARj3mw62mYIuo",
              "site_id": 6,
              "location_type_id": 21
            }
          },
          "miles": 85.49554104
        },
        {
          "site": {
            "id": 5,
            "name": "SF Dealer1",
            "phone": null,
            "location": {
              "id": 30,
              "address": "1030 Sacramento St, San Francisco, CA 94108",
              "lat": 37.793160174729,
              "lng": -122.41003470362,
              "street": "1030 Sacramento St",
              "city": "San Francisco",
              "state": "CA",
              "zip": "94108",
              "exact": true,
              "updated_at": "2019-01-21T16:59:35.768Z",
              "place_id": "ChIJ1XiVII2AhYAR-_DRHPETrps",
              "site_id": 5,
              "location_type_id": 21
            }
          },
          "miles": 86.77867009
        }
      ]
    }
  }
}```
