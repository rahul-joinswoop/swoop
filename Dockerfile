# adapted from https://github.com/CircleCI-Public/circleci-dockerfiles/blob/master/ruby/images/2.5.1-stretch/Dockerfile
ARG RUBY_VERSION=2.5.6
FROM ruby:${RUBY_VERSION}-stretch
SHELL ["/bin/bash", "-c"]
USER root
WORKDIR /app/

ENV DOCKER_RUN true
ENV DEPENDENCIES autoconf automake build-essential python-dev libtool libssl-dev git postgresql-client dnsutils
ENV WATCHMAN_VERSION v4.9.0

# Install dependencies
RUN apt-get update && apt-get install -y ${DEPENDENCIES}

# Set timezone to UTC by default
RUN ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime

# Use unicode
RUN locale-gen C.UTF-8 || true
ENV LANG=C.UTF-8

# Install Watchman
RUN git clone https://github.com/facebook/watchman.git && \
  cd watchman/ && \
  git checkout ${WATCHMAN_VERSION} && \
  ./autogen.sh && \
  ./configure && \
  make && \
  make install

# Install Node
# Fix for Issue importing gpg key: gpg: keyserver receive failed: Cannot assign requested address
# from https://github.com/inversepath/usbarmory-debian-base_image/issues/9
RUN mkdir ~/.gnupg
RUN chmod 600 ~/.gnupg
RUN echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf
# gpg keys listed at https://github.com/nodejs/node#release-team
RUN set -ex \
  && for key in \
    4ED778F539E3634C779C87C6D7062848A1AB005C \
    94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
    B9AE9905FFD7803F25714661B63B535A4C206CA9 \
    77984A986EBC2AA786BC0F66B01FBB92821C587A \
    71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
    FD3A5288F042B6850C66B31F09FE44734EB7990E \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
    DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
    A48C2BEE680E841632CD4E44F07496B3EB3C1762 \
  ; do \
    gpg --no-tty --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" || \
    gpg --no-tty --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" || \
    gpg --no-tty --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ; \
  done

COPY package.json yarn.lock  /app/

RUN grep '"node":' package.json|awk {'print $2'}|sed 's/[",]//g' >> /root/node_version
RUN export NODE_VERSION=$(cat /root/node_version); ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && grep " node-v$NODE_VERSION-linux-$ARCH.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs

# Install our defined version of npm
RUN grep '"npm":' package.json|awk {'print $2'}|sed 's/[",]//g' >> /root/npm_version
RUN NPM_VERSION=$(cat /root/npm_version); npm install -g npm@$NPM_VERSION
# Basic smoke test
RUN npm --version

# Install Yarn
RUN grep '"yarn":' package.json|awk {'print $2'}|sed 's/[",]//g' >> /root/yarn_version
RUN export YARN_VERSION=$(cat /root/yarn_version); set -ex \
  && for key in \
    6A010C5166006599AA17F08146C2130DFD2497F5 \
  ; do \
    gpg --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" || \
    gpg --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" || \
    gpg --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" \
  && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz.asc" \
  && gpg --batch --verify yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz \
  && mkdir -p /opt \
  && tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/ \
  && ln -s /opt/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn \
  && ln -s /opt/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg \
  && rm yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz

ENV BUNDLE_PATH /gems
ENV GEM_PATH /gems
ENV GEM_HOME /gems

# Setup bundler
COPY Gemfile Gemfile.lock /app/
RUN grep -a1 "BUNDLED WITH" Gemfile.lock |tail -1|awk {'print $1'} >> /root/bundler_version
RUN BUNDLER_VERSION=$(cat /root/bundler_version); gem install bundler -v $BUNDLER_VERSION

# Setup graphql
ARG BUNDLE_GEMS__GRAPHQL__PRO
RUN bundle config gems.graphql.pro $BUNDLE_GEMS__GRAPHQL__PRO

# Setup access to gems on private github repo
ARG GITHUB__COM
RUN bundle config GITHUB__COM $GITHUB__COM:x-oauth-basic

EXPOSE 3000 5000
CMD ["/bin/bash"]
