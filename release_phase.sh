#!/bin/bash

set -o errexit

# To skip release phase in production, set the SKIP_RELEASE_PHASE env variable to "1".
if [ "$SKIP_RELEASE_PHASE" == "1" ]; then
  echo "Release phase disabled with SKIP_RELEASE_PHASE flag"
  exit 0
fi

# PLEASE DO NOT ADD THE REVIEW_APP_READY OR REVIEW_APP_SEEDED ENV VARS TO ANY OF OUR REGULAR APPS.
if [ -z ${REVIEW_APP_READY} ] || [ ! -v ${REVIEW_APP_SEEDED} ]; then
  rake rolling_release db:migrate seed:migrate db:reports:load run_smoke_tests

  # Notify NewRelic, but don't abort the release if the command fails.
  # Otherwise we are dependent on NewRelic being up in order to release
  newrelic deployments || true
fi
