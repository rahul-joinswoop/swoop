# frozen_string_literal: true

FactoryBot.define do
  factory :tire do
    tire_type
  end
end
