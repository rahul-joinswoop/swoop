# frozen_string_literal: true

FactoryBot.define do
  factory :phone_number_blacklist do
    phone_number { Faker::PhoneNumber.phone_number }
  end
end
