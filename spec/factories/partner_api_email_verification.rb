# frozen_string_literal: true

FactoryBot.define do
  factory :partner_api_email_verification, parent: :password_request, class: "PartnerAPIEmailVerification" do
    client_id { create(:doorkeeper_application).uid }
    redirect_uri { Doorkeeper::Application.find_by(uid: client_id).redirect_uri }
  end
end
