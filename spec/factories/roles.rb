# frozen_string_literal: true

FactoryBot.define do
  factory :role do
    name { "name" }
    initialize_with { Role.find_or_create_by(name: name) }

    Role::STATUS_TO_NAME_MAP.keys.each do |r|
      trait r do
        name { r.to_s }
      end
    end
  end
end
