# frozen_string_literal: true

FactoryBot.define do
  factory :invoicing_ledger_item do
    api_async_request_id { nil }
    currency { "USD" }
    description { nil }
    due_date { nil }
    email_bcc { nil }
    email_cc { nil }
    email_to { nil }
    error { nil }
    export_error_msg { nil }
    fleet_alert { false }
    fleet_ui_total_amount { nil }
    identifier { nil }
    incoming_invoice_id { nil }
    invoice
    invoice_notes { nil }
    issue_date { nil }
    job_id { invoice.job.id }
    mark_paid_at { nil }
    paid { false }
    partner_alert { false }
    payment_method { nil }
    period_end { nil }
    period_start { nil }
    qb_imported_at { nil }
    qb_pending_import { false }
    rate_id { nil }
    rate_type { nil }
    recipient { nil }
    recipient_company_id { nil }
    recipient_type { nil }
    s3_filename { nil }
    sender { nil }
    sent_at { nil }
    state { nil }
    status { nil }
    tax_amount { nil }
    total_amount { nil }
    type { 'Payment' }
    uuid { SecureRandom.uuid }
    version { nil }
  end
end
