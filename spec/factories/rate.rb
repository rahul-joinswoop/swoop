# frozen_string_literal: true

FactoryBot.define do
  factory :rate do
    currency { Money.default_currency.iso_code }

    trait :miles_enroute do
      miles_enroute { 2 }
      miles_enroute_free { 10 }
    end

    trait :miles_towed do
      miles_towed { 1 }
      miles_towed_free { 5 }
    end

    trait :miles_deadhead do
      miles_deadhead { 4 }
      miles_deadhead_free { 2 }
    end

    company { any(:company) }

    # PDW:there's currently a flet_rate factory,
    # but it's using a ton of defaults
    #  TODO: remove other flat_rate and replace with this
    factory :clean_flat_rate, class: "FlatRate" do
      flat { 75 }
    end

    factory :hourly_p2p, class: "HoursP2PRate" do
      hourly { 50 }
      type { 'HoursP2PRate' }

      account { nil }
      site { nil }
      vendor_id { nil }
      service_code { nil }

      trait :rate_account_level do
        account
        hourly { 20.0 }
      end

      trait :rate_site_level do
        account
        site
        hourly { 30.0 }
      end
    end

    factory :miles_p2p_rate, class: "MilesP2PRate" do
      hookup { 100 }

      # Default values
      account { nil }
      site { nil }
      vendor_id { nil }
      service_code { nil }
      hourly { 15.0 }
      miles_enroute { 10 }
      miles_enroute_free { 1 }
      miles_towed { 100 }
      miles_towed_free { 10 }
      miles_deadhead { 50 }
      miles_deadhead_free { 5 }

      trait :rate_account_level do
        account
        hourly { 25.0 }
      end

      trait :rate_site_level do
        account
        site
        hourly { 35.0 }
      end

      trait :rate_vendor_id_level do
        account
        site
        vendor_id { 'CA12345' }
        hourly { 40.0 }
      end

      trait :rate_service_code_level do
        account
        site
        vendor_id { 'CA12345' }
        service_code
        hourly { 50.0 }
      end

      trait :rate_account_with_service_code_level do
        account
        site { nil }
        vendor_id { nil }
        service_code
        hourly { 50.0 }
      end
    end
  end

  factory :storage_rate, class: "StorageRate" do
    company { any(:company) }

    after(:build) do |job, params|
      job.storage_daily = 100
    end
  end
end
