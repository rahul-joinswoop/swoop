# frozen_string_literal: true

FactoryBot.define do
  factory :vehicle_category do
    initialize_with { VehicleCategory.find_or_create_by(name: name) }
    name { VehicleCategory::FLATBED }
    order_num { 1 }
  end
end
