# frozen_string_literal: true

FactoryBot.define do
  factory :vehicle do
    company { nil }
  end

  factory :stranded_vehicle, parent: :vehicle, class: "StrandedVehicle" do
    color { "23" }
    license { "234" }
    make { "Alfa Romeo" }
    model { "Giulia" }
    type { "StrandedVehicle" }
    year { 2017 }
    vin { "1HGCM82633A004352" }
  end

  factory :consumer_vehicle, parent: :vehicle do
  end

  factory :rescue_vehicle, parent: :vehicle, class: "RescueVehicle" do
    sequence(:name) { |n| "RescueVehicle#{n}" }
    association :company, factory: :rescue_company
    lat { 37.788511 }
    lng { -122.468831 }

    trait :with_driver do
      driver { create :user, :phone, :driver_on_duty }
    end
  end

  factory :trailer_vehicle, parent: :vehicle, class: "TrailerVehicle" do
    sequence(:name) { |n| "Trailer#{n}" }
    association :company, factory: :rescue_company
    lat { Faker::Address.latitude }
    lng { Faker::Address.longitude }
    make { Faker::Lorem.word }
    model { Faker::Lorem.word }
    vehicle_category
  end

  factory :rescue_vehicle_with_coords, parent: :rescue_vehicle do
    has_set_coords
    with_driver
  end

  trait :has_set_coords do
    lat do
      c = Faker::Address.latitude
      # Mercator projections crap out at 85 degrees latitude.
      c = 0 if c.abs > 85
      c
    end
    lng { Faker::Address.longitude }

    after(:create) do |vehicle, evaluator|
      vehicle.update_location!(lat: vehicle.lat, lng: vehicle.lng, timestamp: Time.current) if vehicle.lat && vehicle.lng
    end
  end
end
