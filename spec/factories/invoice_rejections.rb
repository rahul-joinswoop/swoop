# frozen_string_literal: true

FactoryBot.define do
  factory :invoice_rejection do
    invoice
    invoice_reject_reason
    notes { "Is not correct" }
  end
end
