# frozen_string_literal: true

FactoryBot.define do
  factory :job_status_email_preference do
    user_id { nil }
  end

  trait :eta_extended do
    job_status_id { JobStatus::ETA_EXTENDED }
  end
end
