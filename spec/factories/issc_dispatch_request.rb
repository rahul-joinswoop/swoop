# frozen_string_literal: true

FactoryBot.define do
  factory :issc_dispatch_request, class: "IsscDispatchRequest" do
    dispatchid { '123123123' }
    job
    issc { any(:isscs) }
    max_eta { 90 } # minutes, it's an int value on DB
  end
end
