# frozen_string_literal: true

FactoryBot.define do
  factory :flat_rate do
    account_group_id { nil }
    account
    company
    deleted_at { nil }
    flat { BigDecimal.new("80.0") }
    association :fleet_company, factory: [:fleet_company, :fleet_demo]
    gone { nil }
    hookup { nil }
    hourly { nil }
    import_id { nil }
    import_name { nil }
    miles_enroute { nil }
    miles_enroute_free { nil }
    miles_p2p { nil }
    miles_p2p_free { nil }
    miles_towed { nil }
    miles_towed_free { nil }
    rate_type_id { nil }
    service_code
    service_group_id { nil }
    site
    special_dolly { nil }
    storage_daily { nil }
    type { "FlatRate" }
    vehicle_category_id { nil }
  end
end
