# frozen_string_literal: true

FactoryBot.define do
  factory :history_item do
    title { "Something Happened" }
    job

    association :target, factory: :job
  end
end
