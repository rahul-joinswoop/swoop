# frozen_string_literal: true

FactoryBot.define do
  factory :metric do
    action { "sent" }
    label { "sms_eta" }
    target_id { 44 }
    target_type { "Job" }
    value { 1 }
  end
end
