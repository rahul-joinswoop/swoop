# frozen_string_literal: true

FactoryBot.define do
  factory :job_candidate, class: "Job::Candidate" do
    factory :site_candidate, class: "Job::Candidate::Site", parent: :job_candidate do
    end
    factory :truck_candidate, class: "Job::Candidate::Truck", parent: :job_candidate do
    end
  end
end
