# frozen_string_literal: true

FactoryBot.define do
  # NOTE: SuperCompany has to be 'SuperCompany' here or else it causes db errors because SuperCompany
  # references Doorkeeper::Application (and the db may not be setup yet when we're evaluating our
  # factorybot factories)
  factory :super_company, class: "SuperCompany", parent: :company, aliases: [:swoop_company] do
    initialize_with { SuperCompany.find_or_create_by(name: name) }
    name { Company::SWOOP }
    phone { "231-335-7896" }
    support_email { "quinnbaetz@gmail.com" }
    type { "SuperCompany" }
    # don't change uuid - seeds require it
    uuid { "uuid_company_swoop" }
    trait :swoop do
      name { Company::SWOOP }
    end
  end
end
