# frozen_string_literal: true

FactoryBot.define do
  factory :audit_company_subscription_status do
    company
    subscription_status_id { Company::DEFAULT_SUBSCRIPTION_STATUS_ID }
  end
end
