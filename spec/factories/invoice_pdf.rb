# frozen_string_literal: true

FactoryBot.define do
  factory :invoice_pdf do
    association :invoice
    public_url { 'https://s3host.com/INVOICE_BUCKET/7-02bf2a2d5e267baaf53d14fa-34.pdf' }
  end
end
