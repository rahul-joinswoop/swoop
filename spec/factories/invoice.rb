# frozen_string_literal: true

FactoryBot.define do
  factory :invoice do
    currency { Money.default_currency.iso_code }
    fleet_alert { false }
    fleet_ui_total_amount { BigDecimal("32.0") }
    partner_alert { false }
    state { "partner_new" }
    tax_amount { BigDecimal("0.0") }
    total_amount { BigDecimal("32.0") }
    type { "Invoice" }
    uuid { SecureRandom.hex(8) }

    factory :fleet_managed_job_invoice do
      association :job, factory: :fleet_managed_job
      association :recipient, factory: :user
      association :sender, factory: :rescue_company
      uuid { SecureRandom.hex(10) }
    end

    factory :fleet_in_house_job_invoice do
      association :job, factory: :fleet_in_house_job
      association :recipient, factory: :user
      association :sender, factory: [:fleet_company, :tesla]
      uuid { SecureRandom.hex(10) }
    end

    job { create :job }

    association :recipient, factory: :user # polymorphic, can also be Account
    association :sender, factory: :company

    transient do
      history_items { 2 }
    end

    trait :storage do
      after(:create) do |invoice, params|
        invoice.line_items << create(:invoicing_line_item, :storage, ledger_item: invoice, job: invoice.job)
      end
    end

    trait :with_history_items do
      after(:create) do |invoice, params|
        params.history_items.times do
          invoice.target_history_items << create(:history_item, target: invoice, title: HistoryItem::INVOICE_EDITED, job: invoice.job)
        end
      end
    end

    trait :with_line_items do
      line_items do
        create_list(
          :invoicing_line_item,
          2,
          net_amount: BigDecimal("50"),
          unit_price: BigDecimal("50"),
          job: job
        )
      end
    end

    # TODO - should we pass job_id in here?
    trait :with_payments do
      payments do
        create_list(:payment, 1)
      end
    end

    trait :with_payments_with_history_items do
      payments do
        create_list(:payment, 1, :with_history_items, job_id: job.id)
      end
    end

    # TODO - should we pass job_id in here?
    trait :with_refunds do
      refunds do
        create_list(:refund, 1)
      end
    end

    trait :auto_approvable do
      job do
        create :job,
               :with_ajs,
               status: InvoiceAutoApprover::CheckJobStatus::VALID_STATES.sample,
               rescue_company: recipient_company
      end
      billing_type { InvoiceAutoApprover::CheckInvoiceBillingType::VALID_BILLING_TYPES.sample }
      recipient_company { create :swoop_company }
      sender { create :rescue_company }
      sent_at { DateTime.current }
      state { :partner_sent }
      after(:create) do |invoice, params|
        li = create :invoicing_line_item,
                    net_amount: invoice.auto_approval_limit,
                    unit_price: invoice.auto_approval_limit,
                    job: invoice.job
        invoice.line_items << li
        invoice.calculate_total_amount!
      end
    end
  end
end
