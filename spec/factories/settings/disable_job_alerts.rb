# frozen_string_literal: true

FactoryBot.define do
  factory :disable_job_alerts, class: "Setting" do
    key { Setting::DISABLE_JOB_ALERTS }
    value { true }
  end
end
