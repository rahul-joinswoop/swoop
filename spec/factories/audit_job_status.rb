# frozen_string_literal: true

FactoryBot.define do
  factory :audit_job_status do
    adjusted_dttm { nil }
    company
    job
    job_status do
      # this needs to be something not in JobStatus::HIDE_FROM_HISTORY, otherwise most of the code that uses
      # ajs will skip over this
      create(:job_status, :created)
    end
    last_set_dttm { DateTime.current }
    trait :with_source_application do
      source_application do
        SourceApplication.instance(
          platform: SourceApplication::PLATFORM_CLIENT_API,
          oauth_application_id: create(:doorkeeper_application, owner: company).id
        )
      end
    end
  end
end
