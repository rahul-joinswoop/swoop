# frozen_string_literal: true

FactoryBot.define do
  factory :estimate do
    drop_location_id { nil }
    job
    meters_ab { 19091 }
    meters_ba { 19893 }
    meters_bc { nil }
    meters_ca { nil }
    seconds_ab { 1907 }
    seconds_ba { 1774 }
    seconds_bc { nil }
    seconds_ca { nil }
    association :service_location, factory: :location
    association :site_location, factory: :location
    start_at { nil }
    version { 1 }
  end
end
