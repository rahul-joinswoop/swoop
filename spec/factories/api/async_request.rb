# frozen_string_literal: true

FactoryBot.define do
  factory :api_async_request, class: "API::AsyncRequest" do
    request { nil }

    external_request_url { nil }
    external_request_body { nil }
    external_request_headers { nil }

    external_response_code { nil }
    external_response_body { nil }
    external_response_headers { nil }

    system { nil }

    association :company
    association :user
  end
end
