# frozen_string_literal: true

FactoryBot.define do
  factory :company_invoice_charge_fee do
    company

    percentage { '2.9' }
    fixed_value { '0.3' }
    payout_interval { 'weekly' }
  end
end
