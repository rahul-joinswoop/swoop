# frozen_string_literal: true

FactoryBot.define do
  factory :api_access_token, class: "APIAccessToken" do
    company
    subscriptionid { "1" }
    sequence(:vendorid) { |n| "vendor-#{n}" }
    sequence(:callback_token) { |n| "SomeCallbackToken-#{n}" }
    access_token { "SomeRandomToken" }
    connected { true }
    source { :agero }
  end
end
