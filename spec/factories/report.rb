# frozen_string_literal: true

FactoryBot.define do
  factory :report do
    initialize_with { Report.find_or_create_by(name: name) }
    sequence(:name) { |n| "Company #{n}" }
  end
end
