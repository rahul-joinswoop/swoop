# frozen_string_literal: true

FactoryBot.define do
  factory :department do
    initialize_with { Department.find_or_create_by(name: name, company: company) }

    company
    sequence(:name) { |n| "Department#{n}" }
  end
end
