# frozen_string_literal: true

FactoryBot.define do
  factory :search do
    company { any(:company) }
    order { nil }
    order_direction { nil }
    page { nil }
    per_page { nil }
    scope { "{\"search\":\"as\",\"active\":false}" }
    target { "Job" }
    term { "as" }
    url { "ToFactory: RubyParser exception parsing this attribute" }
    user { any(:user) }
  end
end
