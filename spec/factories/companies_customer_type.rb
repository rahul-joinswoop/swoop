# frozen_string_literal: true

FactoryBot.define do
  factory :companies_customer_type do
    company
    customer_type
  end
end
