# frozen_string_literal: true

FactoryBot.define do
  factory :issc_login do
    site
    fleet_company
    username { "username" }
    password { "password" }
    incorrect { false }
  end
end
