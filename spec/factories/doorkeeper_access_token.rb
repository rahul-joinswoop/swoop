# frozen_string_literal: true

FactoryBot.define do
  factory :doorkeeper_access_token, class: "Doorkeeper::AccessToken" do
    scopes { :fleet }
    trait :with_application do
      application { create :doorkeeper_application, scopes: scopes }
    end
    trait :with_resource_owner do
      resource_owner_id { create(:user).id }
    end
  end
end
