# frozen_string_literal: true

FactoryBot.define do
  factory :typeform_review, class: "Reviews::Typeform" do
    company
    job

    phone { "4155559999" }

    # model refactoring is probably required
    initialize_with do
      new(type: 'Reviews::Typeform',
          company: company,
          job: job)
    end
  end
end
