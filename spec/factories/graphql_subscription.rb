# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_subscription do
    operation_name { Faker::Hipster.word }
    query_string do
      <<~GRAPHQL.strip
        subscription #{operation_name} {
          jobStatusLeft(states: Active) {
            id
          }
        }
      GRAPHQL
    end
    viewer { create :user }
    variables { { a: Faker::Hipster.sentence, b: Faker::Hipster.sentence } }
    # rubocop:disable RSpec/EmptyExampleGroup, RSpec/MissingExampleGroupArgument
    context do
      {
        c: Faker::Hipster.sentence,
        d: Faker::Hipster.sentence,
        request_id: SecureRandom.uuid,
        viewer: viewer,
        webhook_url: Faker::Internet.url,
      }
    end
    # rubocop:enable RSpec/EmptyExampleGroup, RSpec/MissingExampleGroupArgument
  end
end
