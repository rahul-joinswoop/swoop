# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    name { Faker::Company.name }
    transient do
      fleet { nil }
      skip_account_setup { nil }
    end

    company { create :rescue_company, :finish_line, :with_sites, sites_type: "PartnerSite" }
    client_company { nil }

    accounting_email { nil }
    deleted_at { nil }
    location { create :location, :johns_cabin }
    notes { nil }
    phone { nil }
    primary_email { nil }

    after(:build) do |account, params|
      if account.client_company.nil? && params.fleet.present?
        account.client_company = create :fleet_company, params.fleet
      end

      if account.client_company.present? && !params.skip_account_setup
        account.name = "#{account.client_company.name} #{account.name}"
      end
    end
  end
end
