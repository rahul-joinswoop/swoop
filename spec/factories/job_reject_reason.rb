# frozen_string_literal: true

FactoryBot.define do
  factory :job_reject_reason do
    initialize_with { JobRejectReason.find_or_create_by(text: text) }

    text { "Traffic" }
  end
end
