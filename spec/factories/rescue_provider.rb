# frozen_string_literal: true

FactoryBot.define do
  factory :rescue_provider do
    association :company, factory: :fleet_company
    association :provider, factory: :rescue_company

    trait :default do
      location
      site
    end
  end
end
