# frozen_string_literal: true

FactoryBot.define do
  factory :answer do
    question
    answer { Faker::Hipster.sentence }
  end
end
