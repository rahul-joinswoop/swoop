# frozen_string_literal: true

FactoryBot.define do
  factory :referer do
    initialize_with { Referer.find_or_create_by(name: name) }

    name { "Facebook" }
    deleted_at { nil }
  end
end
