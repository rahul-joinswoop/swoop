# frozen_string_literal: true

FactoryBot.define do
  factory :rate_type do
    initialize_with do
      RateType.find_or_create_by(
        friendly_type: friendly_type,
        type: type,
        name: name,
        is_standard: is_standard
      )
    end

    # metaprogram our traits
    RateType::ALL_RATE_TYPES.each do |rate_type_hash|
      trait "#{rate_type_hash[:friendly_type].underscore}".to_sym do
        friendly_type { rate_type_hash[:friendly_type] }
        type { rate_type_hash[:type] }
        name { rate_type_hash[:name] }
        is_standard { rate_type_hash[:is_standard] }
      end
    end
  end
end
