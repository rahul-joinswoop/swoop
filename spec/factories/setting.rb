# frozen_string_literal: true

FactoryBot.define do
  factory :setting do
    key { Faker::Lorem.unique.word }
    value { Faker::Lorem.word }
  end
end
