# frozen_string_literal: true

FactoryBot.define do
  factory :js_file, class: "String" do
    file { File.join('/assets', Faker::Lorem.words(number: 3), [Faker::Lorem.word, 'js'].join('.')) }
    initialize_with { file }
    to_create {}
  end

  factory :css_file, class: "String" do
    file { File.join('/assets', Faker::Lorem.words(number: 3), [Faker::Lorem.word, 'css'].join('.')) }
    initialize_with { file }
    to_create {}
  end

  factory :assets, class: "Hash" do
    js { create_list :js_file, 3 }
    css { create_list :css_file, 3 }
    initialize_with { attributes }
    to_create {}
  end

  factory :manifest, class: "Hash" do
    application { create :assets }
    get_location { create :assets }
    show_eta { create :assets }
    survey { create :assets }
    initialize_with { attributes }
    to_create {}
  end
end
