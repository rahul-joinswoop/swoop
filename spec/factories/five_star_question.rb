# frozen_string_literal: true

FactoryBot.define do
  factory :five_star_question, class: "Survey::FiveStarQuestion" do
    survey
    ref { 'asd' }
    question { 'default question' }
  end
end
