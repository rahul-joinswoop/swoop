# frozen_string_literal: true

FactoryBot.define do
  factory :time_of_arrival do
    eba_type { TimeOfArrival::BID }
    job_id { nil }
    lat { nil }
    lng { nil }
    time { "2016-01-12T00:00 UTC" }
    vehicle_id { nil }
    trait :with_source_application do
      source_application do
        SourceApplication.instance(
          platform: SourceApplication::PLATFORM_CLIENT_API,
          oauth_application_id: create(:doorkeeper_application, owner: company).id
        )
      end
    end
  end
end
