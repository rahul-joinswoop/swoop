# frozen_string_literal: true

FactoryBot.define do
  factory :slack_channel do
    name { 'Slackiest' }
    bot_name { 'Slacker' }
    channel { 'testing' }
    active { true }
    tag { '%' }
    excludes { 'exclude-this-%' }
  end
end
