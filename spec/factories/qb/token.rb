# frozen_string_literal: true

FactoryBot.define do
  factory :qb_token, class: "QB::Token" do
    initialize_with { QB::Token.find_or_create_by(company: company) }

    token { SecureRandom.uuid }
    company { nil }
  end
end
