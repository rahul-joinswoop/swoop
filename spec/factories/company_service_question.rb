# frozen_string_literal: true

FactoryBot.define do
  factory :company_service_question do
    service_code
    company
    question { create :question_with_answers }
    after(:create) do |csq, evaluator|
      if !evaluator.company.service_codes.include? evaluator.service_code
        evaluator.company.service_codes << evaluator.service_code
      end
    end
  end
end
