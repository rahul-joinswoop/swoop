# frozen_string_literal: true

FactoryBot.define do
  factory :invoice_reject_reason do
    company
    text { "Incorrect base rate" }
  end
end
