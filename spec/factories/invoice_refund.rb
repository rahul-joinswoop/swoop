# frozen_string_literal: true

FactoryBot.define do
  factory :refund, class: "Refund", parent: :invoicing_ledger_item do
    currency { Money.default_currency.iso_code }
    total_amount { BigDecimal("10.0") }
    type { "Refund" }
    uuid { SecureRandom.hex(8) }
    payment_method { 'Credit Card' }
    mark_paid_at { Time.current }

    association :recipient, factory: :user # polymorphic, can also be Account
    association :sender, factory: :company
  end
end
