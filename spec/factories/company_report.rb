# frozen_string_literal: true

FactoryBot.define do
  factory :company_report do
    company
    report
  end
end
