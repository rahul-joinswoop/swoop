# frozen_string_literal: true

FactoryBot.define do
  factory :feature do
    initialize_with { Feature.find_or_create_by(name: name) }
    description { nil }
    name { "Consumer SMS" }
  end
end
