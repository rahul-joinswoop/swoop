# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_topic do
    name { Faker::Hipster.sentence }
  end
end
