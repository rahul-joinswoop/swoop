# frozen_string_literal: true

FactoryBot.define do
  factory :twilio_reply do
    job
    target { create :review, job: job }
  end
end
