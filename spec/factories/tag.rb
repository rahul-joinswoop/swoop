# frozen_string_literal: true

FactoryBot.define do
  factory :tag do
    initialize_with { Tag.find_or_create_by(name: name, company: company) }

    name { 'Primary' }
    association :company, factory: :super_company
  end
end
