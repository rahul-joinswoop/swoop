# frozen_string_literal: true

FactoryBot.define do
  factory :job_status_reason do
    initialize_with do
      JobStatusReason.find_or_create_by(
        job: job,
        audit_job_status: audit_job_status,
        job_status_reason_type: job_status_reason_type
      )
    end

    job
    audit_job_status
    job_status_reason_type
  end
end
