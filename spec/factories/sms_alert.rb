# frozen_string_literal: true

FactoryBot.define do
  factory :sms_alert do
    job

    trait :with_audit_sms do
      audit_sms { create audit_sms, job: job }
    end
  end

  factory :confirm_partner_on_site_sms_alert, class: 'ConfirmPartnerOnSiteSmsAlert', parent: :sms_alert do
    type { 'ConfirmPartnerOnSiteSmsAlert' }
  end

  factory :confirm_partner_completed_sms_alert, class: 'ConfirmPartnerCompletedSmsAlert', parent: :sms_alert do
    type { 'ConfirmPartnerCompletedSmsAlert' }
  end
end
