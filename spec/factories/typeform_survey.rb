# frozen_string_literal: true

FactoryBot.define do
  factory :typeform_survey, class: "Survey::Typeform" do
    company
    questions { [] }
    ref { 'defaultRef' }

    initialize_with do
      new(type: 'Survey::Typeform',
          name: 'Fleet Demo TypeForm Survey',
          company: company,
          ref: ref,
          questions: questions)
    end
  end
end
