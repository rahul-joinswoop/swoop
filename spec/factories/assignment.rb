# frozen_string_literal: true

FactoryBot.define do
  factory :assignment, class: "Job::Candidate::Assignment" do
    job
  end
end
