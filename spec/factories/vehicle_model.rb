# frozen_string_literal: true

FactoryBot.define do
  factory :vehicle_model do
    edmunds_id { "AM_General_Hummer" }
    name { "Hummer" }
    vehicle_make { any(:vehicle_make) }
    years { [1998, 1999, 2000] }
    trait :random do
      edmunds_id { rand 1000000000 }
      name { Faker::Lorem.word }
      years { 3.times.map { rand 2020 } }
    end
  end
end
