# frozen_string_literal: true

FactoryBot.define do
  factory :question do
    question { Faker::Lorem.question }
    sequence(:order_num)

    transient do
      with_answers { [] }
    end

    after(:create) do |question, evaluator|
      evaluator.with_answers.each do |answer_text|
        create :answer, answer: answer_text, question: question
      end
    end

    factory :question_with_answers do
      after(:create) do |question|
        create_list :answer, 2, question: question
      end
    end
  end
end
