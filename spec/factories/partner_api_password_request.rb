# frozen_string_literal: true

FactoryBot.define do
  factory :partner_api_password_request, parent: :password_request, class: "PartnerAPIPasswordRequest" do
    user { create(:user) }
    user_input { nil }
    client_id do
      app = create :doorkeeper_application
      create :doorkeeper_access_grant,
             resource_owner_id: user.id,
             application_id: app.id
      app.uid
    end
  end
end
