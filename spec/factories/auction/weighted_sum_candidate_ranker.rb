# frozen_string_literal: true

FactoryBot.define do
  factory :weighted_sum_ranker, class: "Auction::WeightedSumRanker" do
    client { nil }
    weights { nil }
    constraints { nil }
    live { nil }

    trait :eta_only do |obj|
      obj.weights { { "cost" => 0.0, "quality" => 0.0, "speed" => 1.0 } }
    end

    trait :loose_constraints do |obj|
      obj.constraints { { min_eta: 0, max_eta: 1000000, min_cost: 0, max_cost: 1000000, min_rating: -100, max_rating: 100 } }
    end

    trait :sensible_constraints do |obj|
      obj.constraints { { min_eta: 0, max_eta: 120, min_cost: 0, max_cost: 250, min_rating: 0, max_rating: 100 } }
    end
  end
end
