# frozen_string_literal: true

FactoryBot.define do
  factory :bid, class: "Auction::Bid" do
    auction
    job
    status { Auction::Bid::REQUESTED }
    company { create :rescue_company }
    eta_mins { SecureRandom.rand(20) }
    bid_submission_dttm { DateTime.now }
    eta_dttm { bid_submission_dttm + eta_mins.to_i.minutes }
  end
end
