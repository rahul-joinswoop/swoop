# frozen_string_literal: true

FactoryBot.define do
  factory :auction, class: "Auction::Auction" do
    job { create :fleet_managed_job }
    initialize_with { Auction::Auction.build_for_job(job: job) }

    trait :live do
      status { Auction::Auction::LIVE }
    end

    transient do
      bids { 0 }
    end

    after(:create) do |auction, params|
      create_list(:bid, params.bids, auction: auction, job: auction.job)
    end
  end
end
