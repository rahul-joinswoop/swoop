# frozen_string_literal: true

FactoryBot.define do
  factory :version do
    delay { nil }
    description { nil }
    forced { nil }
    name { nil }
    version { "1" }
  end
end
