# frozen_string_literal: true

FactoryBot.define do
  factory :survey_question, class: "Survey::Question" do
    survey
    question { Faker::Lorem.question }
    sequence(:order)
  end
end
