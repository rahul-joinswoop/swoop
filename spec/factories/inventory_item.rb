# frozen_string_literal: true

FactoryBot.define do
  factory :inventory_item do
    company
    deleted_at { "2016-04-14T19:59 UTC" }
    item { create(:tire) }

    site
    user

    trait :undeleted do
      deleted_at { nil }
    end

    trait :one do
      stored_at { Time.parse "2016-04-05 11:22:36-0700" }
    end

    trait :before_noon do
      stored_at { Time.parse '2016-04-05 11:59:00-0700' }
      time { Time.parse '2016-04-05 11:22:36-0700' }
    end

    trait :with_vehicle do
      item { create(:stranded_vehicle) }
      item_type { 'Vehicle' }
    end

    trait :not_released do
      released_at { nil }
      deleted_at { nil }
    end
  end

  factory :storage, parent: :inventory_item do
    deleted_at { nil }
    item_type { 'Vehicle' }
    item { create :vehicle }
    stored_at { '2019-03-19T16:38:44.659Z' }
  end
end
