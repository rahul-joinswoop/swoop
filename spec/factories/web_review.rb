# frozen_string_literal: true

FactoryBot.define do
  factory :web_review, class: "Reviews::WebReview" do
    phone { Faker::PhoneNumber.phone_number_e164 }
    association :company, factory: :fleet_company
    # yuck can't refertence company here so we do it in the after(:create)
    job
    user
    association :survey, factory: :survey
    after(:create) do |web_review|
      web_review.job.update! fleet_company: web_review.company
      web_review.survey.update! company: web_review.company
    end

    initialize_with { new(attributes) }
  end
end
