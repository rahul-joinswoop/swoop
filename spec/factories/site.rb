# frozen_string_literal: true

FactoryBot.define do
  factory :site do
    association :company, factory: [:rescue_company, :finish_line]
    deleted_at { nil }
    description { nil }
    import_num { nil }
    manager_id { nil }
    name { Faker::Address.unique.community }
    old_site_code { nil }
    open_time { nil }
    close_time { nil }
    open_time_sat { nil }
    close_time_sat { nil }
    open_time_sun { nil }
    close_time_sun { nil }
    within_hours { true }
    always_open { true }
    phone { Faker::PhoneNumber.phone_number_e164 }
    site_code { nil }
    tire_program { nil }
    tz { "America/Los_Angeles" }
    location

    # `Site` belongs_to `Location`, `Location` belongs_to `Site`
    after(:build) do |site, _params|
      if !site.location
        site.location = create(:location, site_id: site.id)
      end
    end

    trait :closed do
      always_open { false }
      within_hours { false }
      open_time { "00:00" }
      close_time { "00:00" }
      open_time_sat { "00:00" }
      close_time_sat { "00:00" }
      open_time_sun { "00:00" }
      close_time_sun { "00:00" }
    end

    factory :partner_site, class: "PartnerSite" do
      type { "PartnerSite" }
    end

    factory :fleet_site, class: "FleetSite" do
      type { "FleetSite" }

      trait :advance_auto_site do
        name { "Advance Auto Repair" }
        location { create(:location, :advance_auto_repair, :dealership) }
      end

      trait :fell_street_site do
        name { "Fell Street Auto Repair" }
        location { create(:location, :fell_street_auto_repair, :dealership) }
      end

      trait :sunrise_auto_site do
        name { "Sunrise Auto Service" }
        location { create(:location, :sunrise_auto_service, :dealership) }
      end

      trait :dr_auto_site do
        name { "Dr Auto Las vegas" }
        location { create(:location, :dr_auto_las_vegas, :dealership) }
      end
    end

    factory :poi_site, class: "PoiSite" do
      type { "PoiSite" }
      name { "First place" }
      association :manager, factory: :user

      trait :domi_autobody_site do
        name { "Domi Autobody" }
        location { create(:location, :domi_autobody, :auto_body_shop) }
      end
    end
  end
end
