# frozen_string_literal: true

FactoryBot.define do
  factory :company_role_permission do
    company
    role
    permission_type { CompanyRolePermission::CHARGE }
  end
end
