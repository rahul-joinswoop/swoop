# frozen_string_literal: true

FactoryBot.define do
  factory :territory do
    polygon_area { 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5, -10.5 -10.5, -10.5 10.5))' }

    trait :soft_deleted do
      deleted_at { 1.day.ago }
    end

    trait :san_francisco do
      polygon_area { 'POLYGON ((37.8176496 -122.4779892, 37.8051727 -122.3801422, 37.7291811 -122.3973083, 37.7850968 -122.5263977, 37.8176496 -122.4779892))' }
    end
  end
end
