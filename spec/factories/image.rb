# frozen_string_literal: true

FactoryBot.define do
  factory :image do
    deleted_at { "2016-02-24T20:53 UTC" }
    job
    lat { nil }
    lng { nil }
    url { "cow" }
    user_id { nil }
  end
end
