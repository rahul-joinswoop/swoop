# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    transient do
      roles { [] }
      notification_settings { [] }
    end

    sequence(:first_name) { |n| "Name#{n}" }
    sequence(:last_name)  { |n| "Surname#{n}" }
    sequence(:email)      { |n| "user#{n}@example.com" }
    sequence(:username)   { |n| "Username#{n}" }
    password { "testpassword" }
    language { 'en' }

    after(:create) do |user, evaluator|
      Array(evaluator.roles).each do |role|
        user.add_role role
      end

      evaluator.notification_settings.each do |settings|
        settings.each do |notification_code, notification_channels|
          user.notification_settings.create!(notification_code: notification_code, notification_channels: notification_channels)
        end
      end
    end

    customer { nil }

    trait :with_customer do
      customer
    end

    trait :deleted do
      deleted_at { 5.days.ago }
    end

    trait :quinn do
      first_name { "Quinn" }
      last_name { "Baetz" }
      email { "quinnbaetz@gmail.com" }
      username { "quinnbaetz@gmail.com" }
    end

    trait :lisa do
      first_name { "Lisa" }
      last_name { "Lisa" }
      email { "Lisa@gmail.com" }
      username { "Lisa@gmail.com" }
    end

    trait :sameer do
      first_name { "Sameer" }
      last_name { "Bhalla" }
      email { "Sameer@gmail.com" }
      username { "Sameer@gmail.com" }
    end

    trait :tesla do
      after(:create) { |u| u.add_role(:fleet) }

      association :company, factory: [:fleet_company, :tesla]
    end

    trait :fleet do
      after(:create) { |u| u.add_role(:fleet) }
    end

    trait :fleet_demo do
      fleet
      association :company, factory: [:fleet_managed_company, :fleet_demo, :with_ranker]
    end

    trait :fleet_in_house do
      fleet
      association :company, factory: [:fleet_managed_company, :fleet_in_house]
    end

    trait :root do
      after(:create) { |u| u.add_role(:root) }

      association :company, factory: :super_company
    end

    trait :rescue do
      after(:create) { |u| u.add_role(:rescue) }

      association :company, factory: [:rescue_company, :finish_line]
    end

    trait :rescue_with_sites do
      after(:create) { |u| u.add_role(:rescue) }

      association :company, factory: [:rescue_company, :finish_line, :with_sites]
    end

    trait :admin do
      after(:create) do |user|
        user.add_role(:admin)
      end
    end

    trait :dispatcher do
      after(:create) do |user|
        user.add_role(:dispatcher)
      end
    end

    trait :driver do
      after(:create) do |user|
        user.add_role(:driver)
      end
    end

    trait :rescue_driver do
      rescue_with_sites
      driver
    end

    trait :rescue_driver_with_vehicle do
      rescue_driver
      after(:create) do |user|
        user.vehicle = create :rescue_vehicle_with_coords, company: user.company, driver: user
      end
    end

    trait :phone do
      phone { Faker::PhoneNumber.phone_number_e164 }
    end

    trait :driver_on_duty do
      on_duty { true }
    end

    factory :swoopuser do
      root
    end
  end

  factory :person, parent: :user do
    phone
  end

  factory :driver_on_duty, parent: :user do
    on_duty { true }
  end

  factory :swoop_auto_dispatch, parent: :user do
    username { 'swoop-auto-dispatch' }
    initialize_with { User.find_or_create_by(username: username) }
  end
end
