# frozen_string_literal: true

FactoryBot.define do
  factory :survey, class: "Survey" do
    company
    questions { [] }

    trait :with_questions do
      after(:create) do |survey|
        survey.questions << create_list(:survey_question, 2, survey: survey)
      end
    end
  end
end
