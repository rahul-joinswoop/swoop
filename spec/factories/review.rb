# frozen_string_literal: true

FactoryBot.define do
  factory :review do
    company
    job { create :rescue_job, rescue_company: company }
    phone { "4155559999" }

    trait :with_rating do
      rating { rand 10 }
    end

    trait :with_answers do
      nps_question1 { rand 10 }
      nps_question2 { rand 10 }
      nps_question3 { rand 10 }
    end

    trait :with_twilio_replies do
      with_rating
      after(:create) do |review|
        create_list :twilio_reply, 2, job: review.job, target: review
      end
    end
  end
end
