# frozen_string_literal: true

FactoryBot.define do
  factory :drive do
    user { create :person }
    vehicle { create :stranded_vehicle }
  end
end
