# frozen_string_literal: true

FactoryBot.define do
  factory :companies_feature do
    company
    feature
  end
end
