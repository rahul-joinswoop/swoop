# frozen_string_literal: true

FactoryBot.define do
  factory :job_eta_explanation do
    text { "none" }
    initialize_with { JobEtaExplanation.find_or_create_by(text: text) }
  end
end
