# frozen_string_literal: true

FactoryBot.define do
  factory :custom_account do
    company
    upstream_account_id { 'acct_1CWnkwF0C1gUdX8T' }
  end
end
