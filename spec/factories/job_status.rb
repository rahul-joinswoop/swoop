# frozen_string_literal: true

FactoryBot.define do
  # our default value is pending, hence the alias
  factory :job_status do
    initialize_with { JobStatus.find_or_create_by(id: id, name: name) }

    id { JobStatus::PENDING }
    name { JobStatus::ID_MAP[JobStatus::PENDING] }

    # metaprogram our traits
    JobStatus::ALL_STATUSES.each do |js_id, js_name|
      trait "#{js_name.tr("- ", "_").underscore}".to_sym do
        id { js_id }
        name { js_name }
      end
    end
  end
end
