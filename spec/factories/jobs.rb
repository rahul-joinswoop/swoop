# frozen_string_literal: true

FactoryBot.define do
  factory :job do
    transient do
      helper { nil }
    end

    # default attrs suggested in RN-65
    # some can be missing or not needed
    association :symptom, name: "Accident"
    last_status_changed_at { Time.now }
    uuid { SecureRandom.hex(5) }
    review_sms { true }
    pickup_contact { create :user, :phone }
    dropoff_contact { create :user, :phone }
    association :customer_type, name: "Customer Pay"
    odometer { 1337 }
    send_sms_to_pickup { false }
    text_eta_updates { true }
    policy_number { "1234567" }
    get_location_attempts { 0 }
    # status "Auto Assigning" # maybe it would be better to let it be assigned by workflow?
    user { any(:user) }
    association :fleet_dispatcher, factory: :user
    association :root_dispatcher, factory: :user
    association :fleet_company
    association :rescue_company
    association :service_code, name: "Tow"
    association :driver, factory: :drive

    trait :with_email do
      email { Faker::Internet.email }
    end

    # Sub-optimal, but there's a two-way dependency here. We need to make sure
    # both objects are populated correctly.
    trait :with_invoice do
      after(:create) do |job|
        job.invoice = create(:invoice, job: job)
        job.save!
      end
    end

    trait :stored_with_invoice do
      with_account
      status { :stored }
      after(:create) do |job|
        job.invoice = create :invoice, :storage, job: job
        job.storage = create :storage, :not_released, job: job
        create :audit_job_status, job: job, job_status: create(:job_status, :stored)
        job.save!
      end
    end

    trait :with_invoice_and_line_items do
      after(:create) do |job|
        job.invoice = create(:invoice, :with_line_items, job: job)
        job.save!
      end
    end

    trait :with_partner_new_invoice do
      after(:create) do |job|
        job.invoice = create(:invoice, job: job, state: Invoice::PARTNER_NEW)
        job.save!
      end
    end

    trait :with_rescue_driver do
      rescue_driver do
        create :user, :phone, :driver_on_duty, company: rescue_company
      end
    end

    trait :with_storage_site do
      storage_site do
        create :partner_site, company: rescue_company
      end
    end

    trait :with_rescue_vehicle do
      with_rescue_driver

      rescue_vehicle do
        create :rescue_vehicle_with_coords, company: rescue_company,
                                            driver: rescue_driver
      end
    end

    trait :with_trailer_vehicle do
      with_rescue_driver

      trailer_vehicle do
        create :trailer_vehicle, company: rescue_company,
                                 driver: rescue_driver,
                                 lat: rescue_vehicle.lat,
                                 lng: rescue_vehicle.lng
      end
    end

    trait :with_estimate do
      after(:create) do |job|
        job.estimate = create(:estimate, job: job)
        job.save!
      end
    end

    trait :completed do
      after(:create) do |job|
        job.status = Job::COMPLETED
        job.save!

        JobService::StatusChange.call(
          job: job,
          new_status: Job::COMPLETED,
        )
      end
    end

    trait :goa do
      after(:create) do |job|
        job.status = Job::GOA
        job.save!

        JobService::StatusChange.call(
          job: job,
          new_status: Job::GOA,
        )
      end
    end

    transient do
      bids { 0 }
      style { 'Articulated Dump Truck' }
    end

    trait :with_auction do
      rescue_company { nil }
      rescue_driver { nil }
      after(:create) do |job, params|
        job.auction = create :auction, :live, bids: params.bids, job: job
      end
    end

    trait :with_ajs do
      after(:create) do |job, params|
        job.audit_job_statuses << create(:audit_job_status, :with_source_application, company: job.fleet_company, job: job)
      end
    end

    trait :with_child do
      after(:create) do |job, params|
        child = job.create_additional_job
        child.save!
      end
    end

    trait :with_driver_vehicle_style do
      after(:create) do |job, params|
        stranded_vehicle = job.driver.vehicle
        stranded_vehicle.vehicle_type = params.style

        job.save!
      end
    end

    trait :with_service_location do
      service_location { create :location, :random }
    end

    trait :with_drop_location do
      drop_location { create :location, :random }
    end

    trait :with_account do
      account { create :account, company: rescue_company }
    end
  end

  factory :fleet_in_house_job, class: "FleetInHouseJob", parent: :job do
    association :fleet_company, name: "Fleet Demo", type: "FleetCompany", in_house: true
  end

  factory :fleet_motor_club_job, class: "FleetMotorClubJob", parent: :job do
    association :fleet_company, name: "MotorClub Demo", type: "FleetCompany", in_house: false
  end

  factory :fleet_managed_job, class: "FleetManagedJob", parent: :job do
    association :fleet_company, name: "Fleet Demo", type: "FleetCompany", in_house: false

    trait :in_san_francisco do
      service_location { create(:location, :mission_safeway) }
    end
  end

  factory :farmers_job, class: "FleetManagedJob", parent: :job do
    association :fleet_company, :farmers, type: "FleetCompany"
    with_service_location
  end

  factory :rescue_job, class: "RescueJob", parent: :job do
    association :rescue_company, name: "Finish Line Towing"
    fleet_company { rescue_company }
  end
end
