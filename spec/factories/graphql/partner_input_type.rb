# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_partner_input_type, class: "Hash" do
    transient do
      company { nil }
    end
    trait :with_department do
      department { { name: create(:department, company: company).name } }
    end
    trait :with_vehicle do
      vehicle { { name: create(:rescue_vehicle, company: company).name } }
    end
    trait :with_trailer do
      trailer { { name: create(:trailer_vehicle, company: company).name } }
    end
    initialize_with { attributes.reject { |k, v| k == :service_code } }
    to_create {}
  end
end
