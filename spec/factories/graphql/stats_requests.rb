# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_stats_requests, class: "Array" do
    transient do
      target { nil }
    end

    initialize_with do
      # this factory creates the appropriate data in redis and returns the value we'd expect
      # to see from our #graphql_stats_requests call
      date_range = target.send :graphql_stats_dates

      # build up our keys for our date range
      keys = date_range.map do |d|
        [
          Utils::Stats::Queries::KEY,
          target.send(:graphql_stats_key),
          d.strftime('%Y%m%d'),
        ].join(':')
      end

      # build up GraphQLStats::NUM_DAYS random values
      values = GraphQLStats::NUM_DAYS.times.map do
        rand(1000).to_s
      end

      # pick 3 random indexes to not set in redis - this way we're testing
      # our stats code's ability to handle missing keys
      skipped = 3.times.map { rand(GraphQLStats::NUM_DAYS - 1) }

      # filter our redis input by rejecting the key,value pairs at our skipped indexes
      filtered_input = keys
        .zip(values)
        .reject
        .with_index { |v, i| i.in?(skipped) }
        .flatten

      # create our stats in redis
      ::Swoop.graphql_redis_client.mset(*filtered_input)

      # build up our expected response which contains all of our dates in our date range
      # with nil values on the indexes we skipped
      date_range.zip(
        values.map.with_index { |v, i| i.in?(skipped) ? nil : v }
      )
    end
    to_create {}
  end
end
