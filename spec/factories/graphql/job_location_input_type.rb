# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_job_location_input_type, class: "Hash" do
    serviceLocation { create :graphql_location_input_type, :with_google_place_id }
    dropoffLocation { create :graphql_location_input_type, :with_google_place_id }
    pickupContact { create :graphql_contact_input_type }
    dropoffContact { create :graphql_contact_input_type }

    initialize_with { attributes }
    to_create {}
  end
end
