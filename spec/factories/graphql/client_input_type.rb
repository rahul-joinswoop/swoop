# frozen_string_literal: true

# this builds input data for graphql, it doesn't actually create any models in
# the db
FactoryBot.define do
  factory :graphql_client_input_type, class: "Hash" do
    transient do
      company { nil }
    end

    trait :with_client_department do
      department do
        company.departments << create(:department)
        company.departments.last.name
      end
      after(:create) do |c, params|
        # we can't see this field unless our company has this feature enabled
        feature = create :feature, name: Feature::DEPARTMENTS
        (params.company.features << feature) unless params.company.features.include?(feature)
      end
    end

    trait :with_client_site do
      site do
        company.sites << create(:site)
        company.sites.last.name
      end

      after(:create) do |c, params|
        # we can't see this field unless our company has this feature enabled
        feature = create :feature, name: Feature::CLIENT_SITES
        (params.company.features << feature) unless params.company.features.include?(feature)
      end
    end

    initialize_with { attributes }
    to_create {}
  end
end
