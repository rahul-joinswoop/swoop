# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_texts_input_type, class: "Hash" do
    sendLocation { false }
    sendEtaUpdates { true }
    sendDriverTracking { true }
    sendReview { true }
    sendTextsToPickup { true }

    initialize_with { attributes }
    to_create {}
  end
end
