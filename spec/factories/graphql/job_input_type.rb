# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_job_input_type, class: "Hash" do
    transient do
      company { create :company, :with_service_codes }
      storage_site { nil }
    end
    trait :with_caller do
      caller { create :graphql_caller_input_type }
    end
    trait :with_account do
      account { create :graphql_account_input_type, company: company }
    end
    trait :with_po_number do
      poNumber { Faker::Company.po_number }
    end
    trait :with_client_department do
      client { create :graphql_client_input_type, :with_client_department, company: company }
    end
    trait :with_client_site do
      client { create :graphql_client_input_type, :with_client_site, company: company }
    end
    trait :with_client_site_and_department do
      client { create :graphql_client_input_type, :with_client_department, :with_client_site, company: company }
    end
    customer { create :graphql_customer_input_type }
    trait :with_member_number do
      customer { create :graphql_customer_input_type, :with_member_number, company: company }
    end
    trait :with_customer_full_name do
      customer { create :graphql_customer_input_type, :with_customer_full_name, company: company }
    end
    notes { create :graphql_notes_input_type }
    service { create :graphql_service_input_type, company: company }
    trait :with_answers do
      service { create :graphql_service_input_type, :with_answers, company: company }
    end
    trait :with_class_type do
      service { create :graphql_service_input_type, :with_class_type, company: company }
    end

    trait :with_storage_site do # it only makes sense if company is Partner
      service { create :graphql_service_input_type, :with_storage_site, company: company, storage_site: storage_site }
    end
    trait :with_partner_department do
      partner { create :graphql_partner_input_type, :with_department, company: company }
    end
    trait :with_partner_vehicle do
      partner { create :graphql_partner_input_type, :with_vehicle, company: company }
    end
    trait :with_partner_trailer do
      partner { create :graphql_partner_input_type, :with_trailer, company: company }
    end

    vehicle { create :graphql_vehicle_input_type }
    trait :with_partner_vehicle_fields do
      vehicle { create :graphql_vehicle_input_type, :with_serial_number, :with_tire_size, :with_unit_number, :with_style }
    end

    location { create :graphql_job_location_input_type }
    texts { create :graphql_texts_input_type }
    trait :with_priority_response do
      priorityResponse { Faker::Boolean.boolean }
      after(:create) do |c, params|
        # we can't see this field unless our company has this feature enabled
        feature = create :feature, name: Feature::PRIORITY_RESPONSE
        (params.company.features << feature) unless params.company.features.include?(feature)
      end
    end
    trait :with_payment_type do
      paymentType do
        company.customer_types << create(:customer_type)
        company.customer_types.last.name
      end
      after(:create) do |c, params|
        # we can't see this field unless our company has this feature enabled
        feature = create :feature, name: Feature::CUSTOMER_TYPE
        (params.company.features << feature) unless params.company.features.include?(feature)
      end
    end
    trait :with_payment_type_name do
      paymentTypeName do
        company.customer_types << create(:customer_type)
        company.customer_types.last.name
      end
      after(:create) do |c, params|
        # we can't see this field unless our company has this feature enabled
        feature = create :feature, name: Feature::CUSTOMER_TYPE
        (params.company.features << feature) unless params.company.features.include?(feature)
      end
    end

    initialize_with { attributes }
    to_create {}

    trait :draft do
      status { "Draft" }
    end
  end
end
