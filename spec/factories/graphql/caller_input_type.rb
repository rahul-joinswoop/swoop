# frozen_string_literal: true

# this builds input data for graphql, it doesn't actually create any models in
# the db
FactoryBot.define do
  factory :graphql_caller_input_type, class: "Hash" do
    transient do
      company { nil }
    end

    name { Faker::Name.name }
    phone { Faker::PhoneNumber.phone_number_e164 }

    initialize_with { attributes }
    to_create {}
  end
end
