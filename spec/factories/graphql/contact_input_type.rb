# frozen_string_literal: true

# this builds input data for graphql, it doesn't actually create any models in
# the db
FactoryBot.define do
  factory :graphql_contact_input_type, class: "Hash" do
    name { Faker::Name.name }
    trait :with_full_name do
      fullName { Faker::Name.name }
      name { nil }
    end
    phone { Faker::PhoneNumber.phone_number_e164 }
    initialize_with { attributes }
    to_create {}
  end
end
