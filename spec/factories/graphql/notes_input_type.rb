# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_notes_input_type, class: "Hash" do
    customer { Faker::Hipster.sentence }
    internal { Faker::Hipster.sentence }
    initialize_with { attributes }
    to_create {}
  end
end
