# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_location_input_type, class: "Hash" do
    address do
      "#{Faker::Address.street_address}, " \
      "#{Faker::Address.city}, " \
      "#{Faker::Address.state_abbr} #{Faker::Address.zip}, USA"
    end
    lat { Float(Faker::Address.latitude).round(12) }
    lng { Float(Faker::Address.longitude).round(12) }
    locationType { create(:location_type).name }
    exact { Faker::Boolean.boolean_with_nil }
    initialize_with { attributes.reject { |k, v| v.nil? } }
    to_create {}

    trait :with_google_place_id do
      googlePlaceId { Faker::Lorem.words(number: 2).join("-") }
    end

    trait :with_google_place_id_only do
      address {}
      lat {}
      lng {}
      locationType {}
      exact {}
      with_google_place_id
    end

    trait :with_lat_and_lng_only do
      address {}
      locationType {}
      exact {}
    end

    trait :with_address_only do
      lat {}
      lng {}
      locationType {}
      exact {}
      googlePlaceId {}
    end
  end
end
