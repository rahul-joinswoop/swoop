# frozen_string_literal: true

# this builds input data for graphql, it doesn't actually create any models in
# the db
FactoryBot.define do
  factory :graphql_customer_input_type, class: "Hash" do
    transient do
      company { nil }
    end

    name { Faker::Name.name }
    phone { Faker::PhoneNumber.phone_number_e164 }

    trait :with_customer_full_name do
      fullName { Faker::Name.name }
      name { nil }
    end

    trait :with_member_number do
      memberNumber { Faker::Bank.iban }
      after(:create) do |c, params|
        # we can't see this field unless our company has this feature enabled
        feature = create :feature, name: Feature::JOB_MEMBER_NUMBER
        (params.company.features << feature) unless params.company.features.include?(feature)
      end
    end
    initialize_with { attributes }
    to_create {}
  end
end
