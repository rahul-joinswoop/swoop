# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_service_input_type, class: "Hash" do
    transient do
      company { nil }
      answer_count { 3 }
      storage_site { nil }
    end
    service_code { company&.service_codes&.first }
    name { service_code.name }

    trait :with_symptom do
      symptom { create(:symptom).name }
    end

    trait :with_class_type do
      classType do
        company.vehicle_categories << create(:vehicle_category)
        company.vehicle_categories.last.name
      end
    end

    trait :with_storage_site do
      storageSite do
        if storage_site.present?
          company.sites << storage_site
          company.sites.last.name
        end
      end
    end

    trait :with_scheduled_for do
      scheduled_for { Time.current.iso8601 + 3.hours }
    end
    willStore { Faker::Boolean.boolean }
    storageType { create(:storage_type, company: company).name }

    trait :with_answers do
      answers do
        if company&.company_service_questions&.where(service_code: service_code).length >= answer_count
          company.company_service_questions.where(service_code: service_code).first(answer_count)
            .map { |c| create :graphql_answer_input_type, company_service_question: c }
        else
          create_list(:graphql_answer_input_type,
                      answer_count,
                      company: company,
                      service_code: service_code)
        end
          .tap { |a| a.first[:extra] = Faker::Hipster.sentence }
      end
    end

    initialize_with { attributes.reject { |k, v| k == :service_code } }
    to_create {}
  end
end
