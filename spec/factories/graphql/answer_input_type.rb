# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_answer_input_type, class: "Hash" do
    transient do
      company { create :company }
      service_code { create :service_code }
      company_service_question { nil }
    end
    after(:create) do |gqa, params|
      csq = if params.company_service_question
              params.company_service_question
            else
              create :company_service_question,
                     company: params.company,
                     service_code: params.service_code
            end
      q = csq.question
      gqa[:answer] = q.answers.sample.answer
      gqa[:question] = q.question
    end

    initialize_with { attributes }
    to_create {}
  end
end
