# frozen_string_literal: true

# this builds input data for graphql, it doesn't actually create any models in
# the db
FactoryBot.define do
  factory :graphql_account_input_type, class: "Hash" do
    transient do
      company { nil }
    end

    name { create(:account, name: Faker::Company.name, company: company).name }
    initialize_with { attributes }
    to_create {}
  end
end
