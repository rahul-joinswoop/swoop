# frozen_string_literal: true

FactoryBot.define do
  factory :graphql_vehicle_input_type, class: "Hash" do
    make { "Tesla" }
    model { "Model S" }
    year { 2017 }
    color { "Red" }
    license { "W00T" }
    odometer { 1337 }
    vin { "1GNSKBE04DR265715" }

    trait :with_serial_number do
      serialNumber { SecureRandom.hex }
    end
    trait :with_style do
      style { Faker::Hipster.word }
    end
    trait :with_tire_size do
      tireSize { SecureRandom.hex }
    end
    trait :with_unit_number do
      unitNumber { SecureRandom.hex }
    end

    initialize_with { attributes }
    to_create {}
  end
end
