# frozen_string_literal: true

FactoryBot.define do
  factory :vehicle_make do
    edmunds_id { 200347864 }
    name { "AM General" }
    original { true }
    trait :random do
      edmunds_id { rand 1000000000 }
      name { Faker::Lorem.words(number: 2).join(" ") }
    end
    trait :with_models do
      after(:create) do |vm|
        create_list :vehicle_model, 3, :random, vehicle_make_id: vm.id
      end
    end
  end
end
