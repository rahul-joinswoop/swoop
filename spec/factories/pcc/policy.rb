# frozen_string_literal: true

FactoryBot.define do
  factory :pcc_policy, class: "PCC::Policy" do
    association :company, factory: :fleet_company
    association :job, factory: :farmers_job
    policy_number { "0103293256" }
    uuid { SecureRandom.hex }
    data do
      {
        "policyType" => "Auto",
        "policyNumber" => "0103293256",
        "policyOwner" =>
      {
        "table" =>
                {
                  "lastName" => "DIXON",
                  "firstName" => "KENNETH",
                  "middleName" => "",
                  "contactInfo" =>
                  {
                    "table" =>
                                {
                                  "phoneNumber" => "",
                                  "address" => { "table" => { "street" => "20052 PINEVILLE CT # 60", "state" => "CA", "zip" => "92886", "city" => "YORBA LINDA", "addressType" => "" }, "modifiable" => true },
                                },
                    "modifiable" => true,
                  },
                },
        "modifiable" => true,
      },
        "policyStateCode" => "97",
        "coverageSystemSource" => "APPSCE",
        "loss_date" => "2018-04-02T13:04:59.454Z",
        "companyCode" => "1",
        "policyHolder" =>
      {
        "table" =>
                {
                  "firstName" => "KENNETH",
                  "middleName" => "",
                  "lastName" => "DIXON",
                  "contactInfo" =>
                  {
                    "table" =>
                                { "address" => { "table" => { "street" => "20052 PINEVILLE CT # 60", "city" => "YRBA LNDA", "state" => "CA", "zip" => "92886" }, "modifiable" => true }, "phoneNumber" => nil, "faxNumber" => nil },
                    "modifiable" => true,
                  },
                },
        "modifiable" => true,
      },
        "drivers" => [{ "table" => { "firstName" => "KENNETH", "lastName" => "DIXON", "relation" => "H" }, "modifiable" => true }],
        "vehicles" => [{ "table" => { "policyUnit" => "1", "year" => "2013", "make" => "HONDA", "model" => "ACCORD 2D LX-S", "vin" => "1HGCT1B32DA021662", "propertyType" => "Auto" }, "modifiable" => true }],
        "agent" =>
      {
        "table" =>
                {
                  "agentCode" => "38E",
                  "firstName" => "DEBRA",
                  "lastName" => "MCCAMISH",
                  "stateCode" => "97",
                  "districtCode" => "18",
                  "contactInfo" =>
                  { "table" => { "address" => { "table" => { "street" => "", "city" => "", "state" => "", "zip" => "" }, "modifiable" => true }, "phoneNumber" => "", "faxNumber" => "" }, "modifiable" => true },
                },
        "modifiable" => true,
      },
        "transactionId" => "G0x7f2d876ae808",
      }
    end

    after(:create) do |poliz, evaluator|
      poliz.job.pcc_coverage ||= create(:pcc_coverage)

      poliz.job.pcc_coverage.pcc_policy_id = poliz.id
      poliz.job.pcc_coverage.pcc_vehicle.pcc_policy_id = poliz.id
      poliz.job.pcc_coverage.pcc_driver.pcc_policy_id = poliz.id

      poliz.job.pcc_coverage.save!
      poliz.job.save!
    end

    trait(:lincoln) do
      policy_number { nil }
      data do
        {
          "uuid" => "6edc45931ec45ebac78e75578145fd5e",
          "client_code" => "lincoln",
          "client_name" => "Lincoln",
          "expired" => false,
          "programs" => [
            {
              "uuid" => "25a3aa7db524836b561ab56e2f4d9387",
              "program_code" => "lincoln_for_life",
              "program_name" => "Lincoln For Life",
              "program_identifier" => "1596",
              "start_date" => "2018-12-01",
              "end_date" => "2118-12-01",
              "cancel_date" => nil,
              "expired" => false,
              "details" => {
                "dealer_code" => "L25261",
              },
            },
            {
              "uuid" => "c431db21ccfcd71ad6cfd9db0485f09b",
              "program_code" => "lincoln_warranty",
              "program_name" => "Warranty",
              "program_identifier" => "991",
              "start_date" => (Date.today - 7).iso8601,
              "end_date" => "2024-12-01",
              "cancel_date" => nil,
              "expired" => false,
              "details" => {
                "dealer_code" => "L25261",
              },
            },
          ],
          "vehicles" => [
            {
              "uuid" => "fed8be5d84994fc9a185874c2f7403f5",
            },
          ],
          "customers" => [
            {
              "uuid" => "8353058444390c679bcc02437fbed5b8",
            },
          ],
        }
      end
    end
  end
end
