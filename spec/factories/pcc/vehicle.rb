# frozen_string_literal: true

FactoryBot.define do
  factory :pcc_vehicle, class: "PCC::Vehicle" do
    pcc_policy { nil }
    vin { "1HGCT1B32DA021662" }
    make { "HONDA" }
    model { "ACCORD 2D LX-S" }
    year { "2013" }
    data do
      {
        "table" => {
          "policyUnit" => "1",
          "year" => "2013",
          "make" => "HONDA",
          "model" => "ACCORD 2D LX-S",
          "vin" => "1HGCT1B32DA021662",
          "propertyType" => "Auto",
        },
        "modifiable" => true,
      }
    end
  end
end
