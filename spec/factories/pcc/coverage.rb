# frozen_string_literal: true

FactoryBot.define do
  factory :pcc_coverage, class: "PCC::Coverage" do
    association :pcc_vehicle, factory: :pcc_vehicle
    association :pcc_driver, factory: :pcc_user
    pcc_policy { nil }
    coverage_number { "HX875942" }
    covered { true }
    exceeded_service_count_limit { false }
    data do
      {
        table: {
          coverageReferenceNumber: "HX875942",
          coverageStatus: true,
          coverageLimit: 150.0,
          coverageDescription: "TOW COVERED BY TOW COVERAGE",
          exceededFuelServiceCountLimit: false,
          exceededRoadsideServiceCountLimit: false,
        },
        modifiable: true,
      }
    end

    trait :lincoln do
      coverage_number { nil }
      covered { nil }
      result { "Covered" }
      data do
        [
          {
            "result" => "Covered",
            "limitations" => [],
            "successful_conditions" => [
              "dropoff.type == \"Dealership\"",
              "job.service == \"Tow\"",
              "vehicle.odometer < 70000",
              "program.expired == false",
            ],
            "failed_conditions" => [],
            "name" => "Warranty",
            "code" => "lincoln_warranty",
            "rank" => 4.0,
          },
          {
            "result" => "Covered",
            "limitations" => [],
            "successful_conditions" => [],
            "failed_conditions" => [],
            "name" => "Lincoln For Life",
            "code" => "lincoln_for_life",
            "rank" => 19.0,
          },
        ]
      end
    end

    trait :lincoln_partially_covered do
      lincoln
      result { 'PartiallyCovered' }
      data do
        [
          {
            "result" => "PartiallyCovered",
            "coverage_issues" => ["Cost limit: $100"],
            "limitations" => ["Customer pays overage"],
            "successful_conditions" => [
              "dropoff.type == \"Dealership\"",
              "vehicle.odometer < 70000",
              "program.expired == false",
            ],
            "failed_conditions" => ["job.service == \"Tow\""],
            "name" => "Warranty",
            "code" => "lincoln_warranty",
            "rank" => 4.0,
          },
        ]
      end
    end

    trait :lincoln_not_covered do
      lincoln
      result { 'NotCovered' }
      data do
        [
          {
            "result" => "NotCovered",
            "coverage_issues" => ["Program Expired"],
            "limitations" => ["Customer pays full cost of service"],
            "successful_conditions" => [],
            "failed_conditions" => ["program.expired == false"],
            "name" => "Warranty",
            "code" => "lincoln_warranty",
            "rank" => 4.0,
          },
        ]
      end
    end
  end
end
