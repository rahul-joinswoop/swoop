# frozen_string_literal: true

FactoryBot.define do
  factory :pcc_user, class: "PCC::User" do
    pcc_policy { nil }
    full_name { "TEST USER" }
    data do
      {
        "table" => {
          "firstName" => "KENNETH",
          "lastName" => "DIXON",
          "relation" => "H",
        },
        "modifiable" => true,
      }
    end
  end
end
