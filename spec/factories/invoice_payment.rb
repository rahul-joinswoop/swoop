# frozen_string_literal: true

FactoryBot.define do
  factory :payment, class: "Payment", parent: :invoicing_ledger_item do
    currency { Money.default_currency.iso_code }
    total_amount { BigDecimal("-10.0") }
    type { "Payment" }
    uuid { SecureRandom.hex(8) }
    payment_method { 'Credit Card' }
    mark_paid_at { Time.current }

    association :recipient, factory: :user # polymorphic, can also be Account
    association :sender, factory: :company

    trait :with_history_items do
      after(:create) do |payment, params|
        payment.target_history_items << create(:history_item, target: payment, title: HistoryItem::INVOICE_EDITED_VCC_PAYMENT, job_id: payment.job_id)
      end
    end
  end
end
