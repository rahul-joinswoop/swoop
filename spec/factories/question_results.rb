# frozen_string_literal: true

FactoryBot.define do
  factory :question_result do
    question { create :question_with_answers }
    answer { question.answers.sample }
  end
end
