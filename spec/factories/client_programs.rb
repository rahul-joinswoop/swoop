# frozen_string_literal: true

FactoryBot.define do
  factory :client_program do
    initialize_with { ClientProgram.find_or_create_by(code: code) }
    sequence(:code) { |n| "client_program_#{n}" }
    sequence(:name) { |n| "Client Program #{n}" }
  end
end
