# frozen_string_literal: true

require 'street_address'
FactoryBot.define do
  factory :location do
    association :location_type, factory: :location_type

    trait :random do
      address do
        "#{Faker::Address.street_address}, " \
        "#{Faker::Address.city}, " \
        "#{Faker::Address.state_abbr}, #{Faker::Address.zip}, USA"
      end

      lat { Float(Faker::Address.latitude) }
      lng { Float(Faker::Address.longitude) }
      street_number { StreetAddress::US.parse(address.sub(', USA', '')).number }
      street_name { StreetAddress::US.parse(address.sub(', USA', '')).street }
      state { StreetAddress::US.parse(address.sub(', USA', '')).state }
      city { StreetAddress::US.parse(address.sub(', USA', '')).city }
      street { StreetAddress::US.parse(address.sub(', USA', '')).to_s(:line1) }
      zip { StreetAddress::US.parse(address.sub(', USA', '')).postal_code }
      country { 'US' }
    end

    trait :dealership do
      location_type { create :location_type, { name: "Dealership" } }
    end

    trait :auto_body_shop do
      location_type { create :location_type, { name: "Auto Body Shop" } }
    end

    trait :new_jerzy_towing_hq do
      lat { 37.7434986 }
      lng { -122.474834 }
      address { "2381 18th Avenue, San Francisco, CA 94116" }
      street_number { "2381" }
      street_name { "18th Avenue" }
      state { "CA" }
      city { "San Francisco" }
      street { "2381 18th Avenue" }
      zip { "94116" }
    end

    trait :saint_paul_towing_hq do
      lat { 37.7720023 }
      lng { -122.4133224 }
      address { "1465 Folsom St, San Francisco, CA 94103" }
      street_number { "1465" }
      street_name { "Folsom St" }
      state { "CA" }
      city { "San Francisco" }
      street { "1465 Folsom St" }
      zip { "94103" }
    end

    trait :durand_durand_towing_hq do
      lat { 37.7737352 }
      lng { -122.4381865 }
      address { "1213 Fell St, San Francisco, CA 94117" }
      street_number { "1213" }
      street_name { "Fell St" }
      state { "CA" }
      city { "San Francisco" }
      street { "1213 Fell St" }
      zip { "94117" }
    end

    trait :mission_safeway do
      lat { 37.7432206 }
      lng { -122.422429 }
      address { "3350 Mission St, San Francisco, CA 94110" }
      street_number { "3350" }
      street_name { "Mission St" }
      street { "3350 Mission St" }
      city { "San Francisco" }
      state { "CA" }
      zip { "94110" }
    end

    trait :tiburon_presbyterian do
      lat { 37.8983205 }
      lng { -122.5021879 }
      address { "240 Tiburon Blvd, Tiburon, CA 94920" }
      street_number { "240" }
      street_name { "Tiburon Blvd" }
      street { "240 Tiburon Blvd" }
      city { "Tiburon" }
      state { "CA" }
      zip { "94920" }
    end

    trait :oakland_beer_garden do
      lat { 37.8101064 }
      lng { -122.2691908 }
      address { "2040 Telegraph Ave, Oakland, CA 94612" }
      street_number { "2040" }
      street_name { "Telegraph Ave" }
      street { "2040 Telegraph Ave" }
      city { "Oakland" }
      state { "CA" }
      zip { "94612" }
    end

    trait :round_table_pizza do
      lat { 37.4330854 }
      lng { -122.1280425 }
      address { "702 Colorado Ave, Palo Alto, CA 94303" }
      street_number { "702" }
      street_name { "Colorado Ave" }
      street { "702 Colorado Ave" }
      city { "Palo Alto" }
      state { "CA" }
      zip { "94303" }
    end

    trait :stonestown_mall do
      lat { 37.7280817 }
      lng { -122.4770808 }
      address { "3251 20th Ave, San Francisco, CA 94132" }
      street_number { "3251" }
      street_name { "20th Ave" }
      street { "3251 20th Ave" }
      city { "San Francisco" }
      state { "CA" }
      zip { "94132" }
    end

    trait :accurate_auto_body do
      lat { 37.7838366 }
      lng { -122.4152693 }
      address { "460 Eddy St, San Francisco, CA 94109" }
      street_number { "460" }
      street_name { "Eddy St" }
      state { "CA" }
      city { "San Francisco" }
      street { "460 Eddy St" }
      zip { "94109" }
    end

    trait :johns_cabin do
      address { "3222 Johns Cabin Road, Wildwood, MO, 63038" }
      lat { 38.56152 }
      lng { -90.625974 }
    end

    trait :advance_auto_repair do
      address { "265 Eddy St, San Francisco, CA 94102" }
      lat { 37.7837539 }
      lng { -122.4119065 }
    end

    trait :fell_street_auto_repair do
      address { "340 Fell St, San Francisco, CA 94102" }
      lat { 37.7761149 }
      lng { -122.4236758 }
    end

    trait :sunrise_auto_service do
      address { "2800 San Bruno Ave, San Francisco, CA 94134" }
      lat { 37.7262159 }
      lng { -122.4033221 }
    end

    trait :dr_auto_las_vegas do
      address { "6450 W Craig Rd, Las Vegas, NV 89108" }
      lat { 36.2403259 }
      lng { -115.2347431 }
    end

    trait :domi_autobody do
      address { "1455 Carroll Ave, San Francisco, CA 94124" }
      lat { 37.723195 }
      lng { -122.3907761 }
    end

    trait :fleet_demo_company do
      address { "333 Bush Street, San Francisco, CA" }
      city { "San Francisco" }
      street { "333 Bush St" }
      zip { "94104" }
      country { "US" }
      exact { true }
      lat { 37.723195 }
      lng { -122.3907761 }
      place_id { "ChIJZ7LgkYmAhYARQnfC5WvRghw" }
    end

    trait :ofarrell do
      address { "26 O'Farrell, San Francisco, CA" }
      city { "San Francisco" }
      street { "26 O'Farrell" }
      zip { "94108" }
      country { "US" }
      exact { true }
      lat { 37.7869368 }
      lng { -122.4055448 }
    end

    trait :foleys do
      address { "243 O'Farrell St, San Francisco, CA" }
      city { "San Francisco" }
      street { "243 O'Farrell" }
      zip { "94102" }
      country { "US" }
      exact { true }
      lat { 37.7859988 }
      lng { -122.4087206 }
    end
  end
end
