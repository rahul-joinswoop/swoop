# frozen_string_literal: true

FactoryBot.define do
  factory :fleet_company, parent: :company, class: "FleetCompany", aliases: [:fleet_managed_company] do
    initialize_with { FleetCompany.find_or_create_by(name: name) }

    # business "Company"
    in_house { false }
    type { "FleetCompany" }
    accounting_email { "accounting@fleet-company.com" }
    survey_template { 'api/v1/review/whitelabel/swoop_gamified' }

    trait :geico do
      name { Company::GEICO }
      accounting_email { "accounting@motorclub.com" }
      gcoapi
    end

    trait :tesla do
      name { Company::TESLA }
      uuid { "uuid_tesla" }
      in_house { true }
      support_email { "support@finish.com" }
    end

    trait :mini do
      name { 'mini' }
      uuid { "uuid_mini" }
      in_house { true }
    end

    trait :fleet_demo do
      name { "Fleet Demo" }
      uuid { "uuid_company_fleet_demo" }
    end

    trait :fleet_in_house do
      name { "Fleet In House" }
      in_house { true }
      uuid { "uuid_company_fleet_in_house" }
    end

    trait :century do
      name { Company::CENTURY }
      uuid { "uuid_company_century" }
    end

    trait :lincoln do
      name { Company::LINCOLN }
      uuid { "uuid_company_lincoln" }
    end

    trait :turo do
      name { Company::TURO }
      uuid { "uuid_company_turo" }
    end

    trait :farmers do
      name { Company::FARMERS }
      uuid { "uuid_company_farmers" }
    end

    trait :fleet_response do
      name { Company::FLEET_RESPONSE }
      uuid { "uuid_company_fleet_response" }
    end

    trait :enterprise_fleet_management do
      name { Company::ENTERPRISE_FLEET_MANAGEMENT }
      uuid { "uuid_company_enterprise_fleet_management" }
    end

    trait :agero do
      name { Company::AGERO }
      accounting_email { "agero@accounting.com" }
      ago
    end

    trait :tesla_motorclub do
      name { Company::TESLA_MOTORS_INC }
      accounting_email { "tesla_motorclub@accounting.com" }
      tsla
    end

    trait :motor_club do
      issc_client_id { 123 }
      accounting_email { "accounting@motorclub.com" }
    end

    ::FleetCompany::ISSC_CLIENT_IDS.values.each do |v|
      trait v.downcase.to_sym do
        issc_client_id { v }
        use_custom_job_status_reasons { true }
      end
    end
  end

  trait :with_account do
    after(:create) do |company|
      company.accounts << create(:account, company: company, client_company: create(:rescue_company))
    end
  end

  trait :with_ranker do
    after(:create) do |fleet_company|
      fleet_company.ranker = fleet_company.get_or_create_ranker
    end
  end

  factory :fleet_in_house_company, parent: :fleet_company, class: "FleetCompany" do
    in_house { true }
  end

  factory :fleet_demo_company, class: "FleetCompany" do
    name { Company::DEMO_FLEET_COMPANY }
    phone { "+15047229063" }
    support_email { Utils::SWOOP_OPERATIONS_EMAIL }
    accounting_email { Utils::SWOOP_OPERATIONS_EMAIL }
    type { "FleetCompany" }
    in_house { true }
    demo { true }
    location { create :location, :fleet_demo_company }
  end
end
