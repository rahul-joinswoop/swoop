# frozen_string_literal: true

FactoryBot.define do
  factory :doorkeeper_access_grant, class: "Doorkeeper::AccessGrant" do
    resource_owner_id { create(:user).id }
    application_id { create(:doorkeeper_application).id }
    token { SecureRandom.hex }
    expires_in { 600 }
    redirect_uri { Doorkeeper::Application.find(application_id).redirect_uri }
    created_at { DateTime.current }
    scopes { Doorkeeper::Application.find(application_id).scopes }
  end
end
