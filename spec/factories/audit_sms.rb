# frozen_string_literal: true

FactoryBot.define do
  factory :audit_sms do
    body { "ToFactory: RubyParser exception parsing this attribute" }
    from { "+14152148234" }
    job { any(:job) }
    to { "+12315555555" }
    user { any(:user) }

    factory :review_audit_sms, class: "ReviewAuditSms" do
      type { "ReviewAuditSms" }
      body { "Turo: Please reply with a number 1-5 (5 is best) to rate your service from On Expedite Roadside" }
    end

    factory :eta_audit_sms, class: "EtaAuditSms"
    factory :location_audit_sms, class: "LocationAuditSms"

    trait :sms_confirmation do
      type { "Sms::Confirmation" }
      body { "Fleet Demo (#1135): Request received at 1623-1699 5th Ave, San Franc... (Reply “1” to update). ETA coming soon. Support: 415-555-0278" }
    end

    trait :sms_branded_review_link do
      type { "Sms::BrandedReviewLink" }
      uuid { SecureRandom.uuid }
    end

    trait :confirm_partner_on_site do
      type { "Sms::ConfirmPartnerOnSite" }
      body { "Fleet: Has your service provider arrived? It's important to us that you're taken care of. Reply \"Y\" if yes, \"N\" if not yet." }
    end
  end
end
