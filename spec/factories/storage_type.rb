# frozen_string_literal: true

FactoryBot.define do
  factory :storage_type do
    initialize_with { StorageType.find_or_create_by(name: name, company: company) }

    company
    sequence(:name) { Faker::Company.name }
  end
end
