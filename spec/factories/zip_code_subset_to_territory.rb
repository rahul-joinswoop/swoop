# frozen_string_literal: true

FactoryBot.define do
  factory :zip_code_subset_to_territory do
    polygon_area { 'POLYGON((-91.23046875 45.460130637921, -79.8046875 49.837982453085, -88.2421875 32.694865977875, -91.23046875 45.460130637921))' }
    zip { '03062' }

    trait :random_polygon_territory do
      polygon_area { 'POLYGON((42.75170765925776 -71.4802020178214, 42.751581607789454 -71.47659712890538, 42.74981686031511 -71.47625380615148, 42.748682353262545 -71.47814208129796, 42.75170765925776 -71.4802020178214))' }
    end

    trait :random_zip_code do
      zip { '03063' }
    end
  end
end
