# frozen_string_literal: true

FactoryBot.define do
  factory :rescue_company, parent: :company, class: "RescueCompany" do
    initialize_with { RescueCompany.find_or_create_by(name: name) }
    type { "RescueCompany" }
    trait :finish_line do
      name { "Finish Line Towing" }
      uuid { "uuid_company_flt" }
      dispatch_email { "dispatch@finish.com" }
      association :location
      with_towing
    end

    trait :new_jerzy_towing do
      name { "New Jerzy Towing" }
      after(:create) do |rescue_company|
        rescue_company.sites << create(
          :site,
          company: rescue_company,
          name: 'HQ',
          location: create(:location, :new_jerzy_towing_hq)
        )
      end
    end

    trait :saint_paul_towing do
      name { "Saint Paul's Towing" }
      after(:create) do |rescue_company|
        rescue_company.sites << create(
          :site,
          company: rescue_company,
          name: 'HQ',
          location: create(:location, :saint_paul_towing_hq)
        )
      end
    end

    trait :durand_durand_towing do
      name { "Durand Durand Towing" }
      after(:create) do |rescue_company|
        rescue_company.sites << create(
          :site,
          company: rescue_company,
          name: 'HQ',
          location: create(:location, :durand_durand_towing_hq)
        )
      end
    end

    trait :with_account do
      after(:create) do |company|
        company.accounts << create(:account, company: company)
      end
    end

    trait :with_swoop_account do
      after(:create) do |company|
        company.accounts << create(:account, company: company, client_company: Company.swoop)
      end
    end

    trait :with_hq do
      after(:build) do |company, evaluator|
        company.sites << create(:site, company: company, name: 'HQ')
      end
    end

    trait :with_towing do
      after(:create) do |company|
        service_code = create :service_code, name: ServiceCode::TOW

        if !company.service_codes.include? service_code
          company.service_codes << service_code
        end
      end
    end

    trait :with_storage do
      with_sites

      after(:create) do |company|
        service_code = create :service_code, name: ServiceCode::STORAGE

        if !company.service_codes.include? service_code
          company.service_codes << service_code
        end

        feature = create :feature, name: Feature::STORAGE
        if !company.features.include? feature
          company.features << feature
        end
      end
    end
  end
end
