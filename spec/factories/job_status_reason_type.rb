# frozen_string_literal: true

FactoryBot.define do
  factory :job_status_reason_type do
    initialize_with do
      JobStatusReasonType.find_or_create_by(text: text, key: key, job_status: job_status, is_standard: is_standard)
    end
    is_standard { nil }
    # metaprogram our traits
    JobStatusReasonType::ALL_JOB_STATUS_REASON_TYPES.each do |jsrt|
      trait jsrt[:key] do
        job_status { JobStatus.find jsrt[:job_status_id] }
        key { jsrt[:key] }
        text { jsrt[:text] }
        is_standard { jsrt[:is_standard] }
      end
    end
  end
end
