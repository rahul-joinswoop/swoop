# frozen_string_literal: true

FactoryBot.define do
  factory :doorkeeper_application, class: "Doorkeeper::Application" do
    name { Faker::Hipster.unique.word }
    scopes { :fleet }
    owner { create :company }
    redirect_uri { URI(ENV['SITE_URL']).tap { |u| u.scheme = "https" } }

    trait :partner do
      scopes { :rescue }
      owner { create :rescue_company }
    end
  end
end
