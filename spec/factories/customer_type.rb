# frozen_string_literal: true

FactoryBot.define do
  factory :customer_type do
    initialize_with { CustomerType.find_or_create_by(name: name) }
    name { "Warranty" }
  end
end
