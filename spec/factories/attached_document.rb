# frozen_string_literal: true

FactoryBot.define do
  factory :attached_document do
    type { 'AttachedDocument' }
    invoice_attachment { false }
    job { create :rescue_job, :with_rescue_driver }
    user { job.rescue_driver }
    location { create :location }

    trait :with_document do
      document do
        {
          "id" => "de150e1fe4745058b05a94322367baf7",
          "storage" => "cache",
          "metadata" => {
            "size" => 799,
            "filename" => "Query Results.csv",
            "mime_type" => "text/csv",
          },
        }.to_json
      end
    end
    trait :dropoff do
      container { 'dropoff' }
    end
    trait :pickup do
      container { 'pickup' }
    end
    trait :release do
      container { 'release' }
    end
  end
  factory :signature_image, class: "AttachedDocument::SignatureImage", parent: :attached_document do
    type { 'AttachedDocument::SignatureImage' }
  end
  factory :location_image, class: "AttachedDocument::LocationImage", parent: :attached_document do
    type { 'AttachedDocument::LocationImage' }
  end
end
