# frozen_string_literal: true

FactoryBot.define do
  factory :rate_addition do
    name { "Rate Addition 1" }
    rate { create(:clean_flat_rate) }
    calc_type { "flat" }
    amount { 77.0 }
  end
end
