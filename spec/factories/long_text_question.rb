# frozen_string_literal: true

FactoryBot.define do
  factory :long_text_question, class: "Survey::LongTextQuestion" do
    survey
    ref { 'asd' }
    question { 'default question' }
  end
end
