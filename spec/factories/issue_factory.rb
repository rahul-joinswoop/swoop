# frozen_string_literal: true

FactoryBot.define do
  factory :issue do
    initialize_with { Issue.find_or_create_by(name: name) }
    name { "Sample" }
    key { Issue::ISSUE_KEY_MAP[name] || name.underscore }
    is_standard { Issue::ISSUE_KEY_MAP.include?(name) }
  end
end
