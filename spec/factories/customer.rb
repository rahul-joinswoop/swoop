# frozen_string_literal: true

FactoryBot.define do
  factory :customer do
    company
    member_number { Faker::Bank.iban }
  end
end
