# frozen_string_literal: true

FactoryBot.define do
  factory :report_result do
    transient do
      available_params { nil }
    end

    report do
      create :report, available_params: available_params
    end

    company
    user
  end
end
