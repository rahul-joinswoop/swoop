# frozen_string_literal: true

FactoryBot.define do
  factory :symptom do
    # company # is optional and is `nil` in seeds
    initialize_with { Symptom.find_or_create_by(name: name) }
    deleted_at { nil }
    name { "Other" }
    trait :random_name do
      name { Faker::Hipster.sentence }
    end
    is_standard { true }
  end
end
