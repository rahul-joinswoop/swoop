# frozen_string_literal: true

FactoryBot.define do
  factory :vehicle_type do
    initialize_with { VehicleType.find_or_create_by(name: name) }
    name { SecureRandom.hex }
  end
end
