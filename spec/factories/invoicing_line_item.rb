# frozen_string_literal: true

FactoryBot.define do
  factory :invoicing_line_item do
    creator_id { nil }
    deleted_at { nil }
    description { "Flat Tire" }
    estimated { nil }
    free { nil }
    ledger_item { create(:invoice, job: job) }
    net_amount { nil }
    notes { nil }
    order { nil }
    original_quantity { nil }
    original_unit_price { nil }
    quantity { BigDecimal("1.0") }
    tax_amount { BigDecimal("0.0") }
    tax_point { nil }
    type { nil }
    unit_price { nil }
    user_created { nil }
    uuid { SecureRandom.uuid }
    job { create :job }

    trait :estimated do
      estimated { true }
      user_created { false }
      original_quantity { quantity }
      original_unit_price { unit_price }
    end

    trait :user_created do
      estimated { false }
      user_created { true }
      original_quantity { nil }
      original_unit_price { nil }
    end

    trait :flat_rate do
      description { "Flat Rate" }
      rate { create(:flat_rate) }
    end

    trait :tax do
      description { "Tax" }
    end

    trait :storage do
      description { "Storage" }
    end
  end
end
