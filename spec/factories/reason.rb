# frozen_string_literal: true

FactoryBot.define do
  factory :reason do
    initialize_with { Reason.find_or_create_by(name: name) }

    name { "Traffic" }
  end
end
