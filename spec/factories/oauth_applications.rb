# frozen_string_literal: true

FactoryBot.define do
  factory :oauth_application, class: "Doorkeeper::Application" do
    sequence(:name) { |n| "Application #{n}" }
    initialize_with { Doorkeeper::Application.find_or_create_by(name: name) }

    scopes { owner.is_a?(RescueCompany) ? :rescue : :fleet }
    owner {}

    redirect_uri { Faker::Internet.url(scheme: 'https') }
  end
end
