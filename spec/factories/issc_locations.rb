# frozen_string_literal: true

FactoryBot.define do
  factory :issc_location do
    initialize_with do
      IsscLocation.find_or_create_by(
        fleet_company: fleet_company,
        site: site,
        locationid: locationid
      )
    end

    trait :default do
      site
      locationid { 'dispatch_provider_facility_1' }
      fleet_company
      location
    end
  end
end
