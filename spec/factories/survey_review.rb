# frozen_string_literal: true

FactoryBot.define do
  factory :survey_review, class: "Reviews::SurveyReview" do
    company
    job
    typeform_survey
    phone { "4155559999" }

    # model refactoring is probably required
    initialize_with do
      new(type: 'Reviews::SurveyReview',
          company: company,
          job: job,
          survey: typeform_survey)
    end
  end
end
