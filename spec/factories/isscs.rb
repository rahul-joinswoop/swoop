# frozen_string_literal: true

FactoryBot.define do
  factory :isscs, class: "Issc" do
    company
    site
    status { 'Unregistered' }
    deleted_at { nil }
    delete_pending_at { nil }

    trait :location do
      transient do
        fleet_company { nil }
        locationid { nil }
      end
    end

    trait :login do
      transient do
        fleet_company { nil }
      end
    end

    trait :rsc do
      system { "rsc" }
    end

    trait :with_api_access_token do
      api_access_token
    end

    factory :gco_issc do
      clientid { 'GCO' }
      contractorid { 'CA123456' }
    end

    factory :quest_issc, traits: [:location] do
      clientid { 'QUEST' }
      contractorid { 'QUEST12345' }
    end

    factory :nsd_issc do
      login
      clientid { 'NSD' }
    end

    factory :ago_issc, traits: [:location] do
      clientid { 'AGO' }
    end

    factory :tsla_issc, traits: [:location] do
      clientid { 'TSLA' }
    end

    after(:build) do |issc, params|
      #      Rails.logger.debug "issc factory params(#{params.class.name}): #{params.inspect}"
      if issc.supports_location? && params.try(:fleet_company)
        issc.issc_location = create :issc_location, site: site, fleet_company: params.fleet_company, locationid: locationid
      end

      if issc.supports_login? && params.try(:fleet_company)
        issc.issc_login = create :issc_login, site: issc.site, fleet_company: params.fleet_company, incorrect: issc.password_incorrect?
      end
    end
  end
end
