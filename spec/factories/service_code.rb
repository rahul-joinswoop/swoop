# frozen_string_literal: true

FactoryBot.define do
  factory :service_code do
    initialize_with do
      params = { name: name }
      # if we're trying to instantiate a service code that's hard-coded in
      # ServiceCode::SERVICE_CODES then ensure we find_or_create_by with the
      # correct addon value since there's a uniq key on (name, addon)
      new_addon = ServiceCode::SERVICE_CODES.dig(name, 1)
      params.merge!(addon: new_addon) if new_addon.present?
      ServiceCode.find_or_create_by!(params) do |sc|
        # if we're creating go ahead and set these attributes correctly so that
        # we don't try to update our record afterward
        sc.order = order
        sc.variable = variable
        sc.support_storage = support_storage
        sc.is_standard = is_standard
      end
    end

    # if you set addon: true and don't set a name you'll get a service which has addon: true
    addon { false }
    name { addon ? ServiceCode::DOLLY : ServiceCode::TOW }
    trait :random do
      name { ServiceCode::SERVICE_CODES.select { |k, v| v[1] == addon }.keys.sample }
    end
    order { ServiceCode::SERVICE_CODES.dig(name, 0) }
    variable { ServiceCode::SERVICE_CODES.dig(name, 2) }
    is_standard { ServiceCode::SERVICE_CODES.dig(name, 3) }

    # nil is the default value here, if we left off ||nil we'd end up updating
    # every record after initialization
    support_storage { ServiceCode::SUPPORTS_STORAGE_SERVICE_CODES.include?(name) }

    trait :addon do
      addon { true }
    end
  end
end
