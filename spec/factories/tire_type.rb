# frozen_string_literal: true

FactoryBot.define do
  factory :tire_type do
    car { "Model S" }
    extras { nil }
    position { nil }
    size { nil }
    initialize_with { TireType.find_or_create_by(attributes) }
  end
end
