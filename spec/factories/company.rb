# frozen_string_literal: true

FactoryBot.define do
  factory :company do
    name { Faker::Company.name }
    agero_vendor_id { Faker::Number.unique.number(digits: 9) }
    initialize_with { Company.find_or_create_by(name: name) }
    dispatch_email { Faker::Internet.email }
    pickup_waiver { "TODO: Remove me once Swoop company is seeded in test env" }
    dropoff_waiver { "TODO: Remove me once Swoop company is seeded in test env" }
    invoice_waiver { "TODO: Remove me once Swoop company is seeded in test env" }
    disable_review_emails { false }
    subscription_status_id { Company::DEFAULT_SUBSCRIPTION_STATUS_ID }
    currency { 'USD' }
    distance_unit { 'mi' }
    language { 'en' }

    trait :with_phone do
      phone { Faker::PhoneNumber.phone_number_e164 }
    end

    trait :with_primary_contact do
      after(:build) do |company|
        company.update! primary_contact: create(:user, company: company)
      end
    end

    trait :with_sites do
      transient do
        sites_count { 1 }
        sites_type { "PartnerSite" }
      end

      after(:build) do |company, evaluator|
        create_list :site, evaluator.sites_count, company: company, type: evaluator.sites_type
      end
    end

    trait :heavy_duty_equipment do
      features { [create(:feature, name: Feature::HEAVY_DUTY_EQUIPMENT)] }
    end

    trait :with_service_codes do
      after(:create) do |company|
        tow_service_code = create(
          :service_code,
          name: ServiceCode::TOW,
          addon: false,
          is_standard: true
        )

        custom_service_code = create(:service_code,
                                     name: "Custom service code for #{company.class.name}",
                                     addon: false,
                                     is_standard: false)

        if !company.service_codes.include? tow_service_code
          company.service_codes << tow_service_code
        end

        if !company.service_codes.include? custom_service_code
          company.service_codes << custom_service_code
        end
      end
    end

    trait :with_questions do
      after(:create) do |company|
        company.service_codes.each do |sc|
          create :companies_service, company: company, service_code: sc
          create_list(:question_with_answers, 3).each do |q|
            create :company_service_question, company: company, service_code: sc, question: q
          end
        end
      end
    end

    trait :with_addons do
      after(:create) do |company|
        accident_cleanup_addon = create(:service_code,
                                        name: ServiceCode::ACCIDENT_CLEANUP,
                                        addon: true,
                                        is_standard: true)

        custom_addon = create(:service_code,
                              name: "Custom Addon for #{company.class.name}",
                              addon: true,
                              is_standard: false)

        if !company.service_codes.include? accident_cleanup_addon
          company.service_codes << accident_cleanup_addon
        end

        if !company.service_codes.include? custom_addon
          company.service_codes << custom_addon
        end
      end
    end

    trait :with_vehicle_categories do
      after(:create) do |company|
        flatbed = create(:vehicle_category, name: VehicleCategory::FLATBED, is_standard: true)

        if !company.vehicle_categories.include? flatbed
          company.vehicle_categories << flatbed
        end

        custom_vehicle_category = create(:vehicle_category, name: "Custom VehicleCategory for #{company.class.name}")

        if !company.vehicle_categories.include? custom_vehicle_category
          company.vehicle_categories << custom_vehicle_category
        end
      end
    end

    trait :with_departments do
      after(:create) do |company|
        if company.departments.where(name: 'Sales').blank?
          create(:department, name: 'Sales', company: company)
        end
      end
    end

    trait :with_symptoms do
      after(:create) do |company|
        company.symptoms << Symptom.standard_items
      end
    end

    trait :with_location_types do
      after(:create) do |company|
        company.location_types << LocationType.find_or_create_by(name: LocationType::CLIENT_API_LOCATION_TYPES)
      end
    end

    trait :with_customer_types do
      after(:create) do |company|
        account = create(:customer_type, name: CustomerType::ACCOUNT)

        if !company.customer_types.include? account
          company.customer_types << account
        end
      end
    end
  end
end
