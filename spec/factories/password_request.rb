# frozen_string_literal: true

FactoryBot.define do
  factory :password_request do
    uuid { SecureRandom.uuid }
    user { any(:user) }
    user_input { SecureRandom.uuid }
  end
end
