# frozen_string_literal: true

FactoryBot.define do
  factory :location_type do
    name { 'Blocking Traffic' }
    initialize_with { LocationType.find_or_create_by(name: name) }
  end
end
