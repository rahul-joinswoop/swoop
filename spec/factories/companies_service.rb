# frozen_string_literal: true

FactoryBot.define do
  factory :companies_service do
    company
    service_code
  end
end
