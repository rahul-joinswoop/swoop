# frozen_string_literal: true

FactoryBot.define do
  factory :charge, class: "InvoicePaymentCharge" do
    payment
    invoice
    status { InvoicePaymentCharge::CHARGE_STATUS_RUNNING }
  end
end
