# frozen_string_literal: true

FactoryBot.define do
  factory :event_operation do
    initialize_with { EventOperation.find_or_create_by(name: name) }
    name { "create" }
  end
end
