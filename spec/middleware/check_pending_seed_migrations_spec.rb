# frozen_string_literal: true

require "rails_helper"

describe CheckPendingSeedMigrations do
  let(:env) { double("Environment") }
  let(:ware) { CheckPendingSeedMigrations.new(proc { |e| expect(e).to eq(env) }) }

  it "properly caches last migration" do
    expect(SeedMigration::Migrator).to receive(:get_last_migration_date).twice.and_call_original
    expect(SeedMigration::Migrator).to receive(:check_pending!).once

    ware.call(env)
    ware.call(env)
  end

  it "checks for new seeding" do
    expect(SeedMigration::Migrator).to receive(:check_pending!).twice

    allow(SeedMigration::Migrator).to receive(:get_last_migration_date).and_return(5)
    ware.call(env)

    # And at some later point we have another request
    allow(SeedMigration::Migrator).to receive(:get_last_migration_date).and_return(10)
    ware.call(env)
  end
end
