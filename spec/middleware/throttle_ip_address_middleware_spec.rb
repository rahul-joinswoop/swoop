# frozen_string_literal: true

require 'rails_helper'

describe ThrottleIpAddressMiddleware, freeze_time: true do
  let(:ip_address) { "192.66.22.1" }
  let(:env) { { "action_dispatch.remote_ip" => ip_address } }
  let(:app) { lambda { |env| [200, { "X-IP" => env["action_dispatch.remote_ip"] }, ["success"]] } }

  describe "not throttled" do
    it "allows requests to pass through" do
      middleware = ThrottleIpAddressMiddleware.new(app, rate_limit: 2, rate_period: 1)
      expect(middleware.call(env)).to eq [200, { "X-IP" => ip_address }, ["success"]]
    end
  end

  describe "blacklisted ip addresses" do
    it "denies requests from blacklisted ip addresses" do
      middleware = ThrottleIpAddressMiddleware.new(app, rate_limit: 2, rate_period: 1, blacklisted_ips: "192.66.22.0/16")
      expect(middleware.call(env)).to eq [503, { "Content-Type": "text/plain" }, ["Server not available"]]
    end
  end

  describe "throttled requests" do
    let(:middleware) { ThrottleIpAddressMiddleware.new(app, rate_limit: 2, rate_period: 1, whitelisted_ips: "155.22.32.0/16") }

    before :each do
      middleware.call(env)
      middleware.call(env)
    end

    it "throttles requests from the same ip address" do
      expect(middleware.call(env)).to eq [
        429,
        { "Content-Type" => "application/json", "Retry-After" => "0" },
        ['{"error":"Throttle limit reached. Retry later."}'],
      ]
    end

    it "does not throttle requests from a different ip address" do
      expect(middleware.call({ "action_dispatch.remote_ip" => "44.55.66.77" }).first).to eq 200
    end

    it "allows requests from previously throttled ip addresses" do
      Timecop.travel(2.seconds) do
        expect(middleware.call(env).first).to eq 200
      end
    end

    context "whitelisted ip address" do
      let(:ip_address) { "155.22.32.5" }

      it "does not throttle requests from a whitelisted ip address" do
        expect(middleware.call(env).first).to eq 200
      end
    end

    context "localhost" do
      let(:ip_address) { "127.0.0.1" }

      it "does not throttle requests from localhost" do
        expect(middleware.call(env).first).to eq 200
      end
    end

    context "not enabled" do
      let(:middleware) { ThrottleIpAddressMiddleware.new(app, rate_limit: 2, rate_period: 1, enabled: false) }

      it "does not throttle requests" do
        expect(middleware.call(env).first).to eq 200
      end
    end
  end
end
