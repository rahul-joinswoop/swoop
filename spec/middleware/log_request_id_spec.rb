# frozen_string_literal: true

require "rails_helper"

describe LogRequestId do
  let(:env) { { "action_dispatch.request_id" => request_id } }
  let(:request_id) { "request_id" }

  it "adds the tag to Rails.logger" do
    middleware = LogRequestId.new(proc { expect(::Rails.logger.formatter.current_tags).to include(request_id) })
    middleware.call(env)
  end
end
