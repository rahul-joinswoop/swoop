# frozen_string_literal: true

require "rails_helper"

describe ReportLongRunningRequestMiddleware do
  let(:env) { {} }
  let(:response) { [200, {}, ["OK"]] }
  let(:app) do
    lambda do |env|
      sleep(0.01)
      response
    end
  end

  it "does nothing if the request takes less than the threshold" do
    middleware = ReportLongRunningRequestMiddleware.new(app)
    expect(Rollbar).not_to receive(:warn)
    expect(middleware.call(env)).to eq(response)
  end

  it "reports the request to Rollbar if the request takes longer than the threshold" do
    middleware = ReportLongRunningRequestMiddleware.new(app, 0.001)
    expect(Rollbar).to receive(:warn).and_call_original
    expect(middleware.call(env)).to eq(response)
  end
end
