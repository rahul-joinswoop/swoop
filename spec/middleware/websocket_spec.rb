# frozen_string_literal: true

require 'rails_helper'

describe Websockets do
  let(:socket) { double(:socket) }

  describe Websockets::PartnerFleetWebsocket do
    let(:company) { create(:rescue_company) }
    let(:user) { create :user, :rescue, :driver, company: company, vehicle: vehicle }
    let!(:access_token) { create :doorkeeper_access_token, scopes: :rescue, resource_owner_id: user.id }
    let(:vehicle) { create :rescue_vehicle, company: company }
    let(:auth_data) { { 'sync' => { 'token' => access_token.token } } }

    describe "vehicle_location_update", freeze_time: true do
      subject do
        ClimateControl.modify WSUSER: 'true' do
          web_socket = Websockets::PartnerFleetWebsocket.new(socket, env)
          web_socket.authenticated(auth_data)
          web_socket.message(data)
        end
      end

      let(:lat) { 45.1 }
      let(:lng) { -110.1 }
      let(:id) { vehicle.id }
      let(:env) { { 'REQUEST_URI' => "/partner_fleet/event_company_#{company_id}" } }
      let(:data) { { 'vehicle_location_update' => { 'id' => vehicle_id, 'lat' => lat, 'lng' => lng } } }

      shared_examples "it works" do
        it "schedules a vehicle location update" do
          subject
          expect(UpdateVehicleLocationWorker).to have_enqueued_sidekiq_job(company.id, id, lat, lng, Time.now.utc.iso8601)
        end
      end

      context "with a db id" do
        let(:company_id) { company.uuid }
        let(:vehicle_id) { vehicle.id }

        it_behaves_like "it works"
        context "without auth" do
          let(:auth_data) { {} }

          it_behaves_like "it works"
        end
      end

      context "with a graphql id" do
        let(:company_id) { company.to_ssid }
        let(:vehicle_id) { vehicle.to_ssid }

        it_behaves_like "it works"

        context "when the user doesn't own the vehicle" do
          let(:user) do
            u = super()
            u.update! vehicle: nil
            u
          end

          it "does not schedule a vehicle location update" do
            subject
            expect(UpdateVehicleLocationWorker).not_to have_enqueued_sidekiq_job(company.id, id, lat, lng, Time.now.utc.iso8601)
          end
        end

        context "without auth" do
          let(:auth_data) { {} }

          it "does not schedule a vehicle location update" do
            subject
            expect(UpdateVehicleLocationWorker).not_to have_enqueued_sidekiq_job(company.id, id, lat, lng, Time.now.utc.iso8601)
          end
        end
      end
    end
  end
end
