# frozen_string_literal: true

require "rails_helper"

describe Account do
  let(:account) { create(:account) }

  it "creates account" do
    expect(account).to be_valid
  end

  describe "ElasticSearch" do
    it "has a scope to preload associations for ElasticSearch" do
      expect(Account.elastic_search_preload.to_a).to be_a(Array)
    end
  end

  describe "#rsc_enabled?" do
    subject { account.rsc_enabled? }

    it "returns true when theres an agero account and the rescue company has feature enabled" do
      account.company.features << create(:feature, name: "RSC Integration")
      account.client_company_id = Company.agero_id

      expect(subject).to eq(true)
    end

    it "returns false when the account isn't agero" do
      account.client_company = create(:fleet_company)
      account.company.features << create(:feature, name: "RSC Integration")

      expect(subject).to eq(false)
    end
  end

  describe "isscs" do
    let(:fleet_company_1) { create(:fleet_company, issc_client_id: "AGO") }
    let(:fleet_company_2) { create(:fleet_company, issc_client_id: "GCOAPI") }
    let(:rescue_company_1) { create(:rescue_company) }
    let(:rescue_company_2) { create(:rescue_company) }
    let!(:account_1) { create(:account, company: rescue_company_1, client_company: fleet_company_1) }
    let!(:account_2) { create(:account, company: rescue_company_1, client_company: fleet_company_2) }
    let!(:issc1) { create(:isscs, company: rescue_company_1, site: create(:site, company: rescue_company_1), clientid: "AGO") }
    let!(:issc2) { create(:isscs, company: rescue_company_1, site: create(:site, company: rescue_company_1), clientid: "GCOAPI") }
    let!(:issc3) { create(:isscs, company: rescue_company_2, site: create(:site, company: rescue_company_2), clientid: "AGO") }

    it "doesn't find anything if the client company is not a motor club" do
      fleet_company_1.update_column(:issc_client_id, nil)
      expect(account_1.isscs).to eq []
    end

    it "finds issc records asscociated to the account" do
      expect(account_1.isscs).to match_array [issc1]
      expect(account_2.isscs).to match_array [issc2]
    end

    it "marks all issc rows as pending deletion if the account is soft deleted" do
      account_1.soft_delete!
      expect(issc1.reload.delete_pending_at).not_to eq nil
      expect(issc2.reload.delete_pending_at).to eq nil
      expect(issc3.reload.delete_pending_at).to eq nil
    end
  end

  describe "sort_name" do
    it "returns a blank string if there is no name" do
      expect(Account.new.sort_name).to eq ""
    end

    it "returns a lowercased version of the name" do
      expect(Account.new(name: "Joe's Towing").sort_name).to eq "joe's towing"
    end

    it "transliterates non-ASCII characters" do
      expect(Account.new(name: "Über Towing").sort_name).to eq "uber towing"
    end
  end

  describe "update dependent indexes" do
    it "updates dependent indexes when the name changes" do
      account = create(:account)
      SearchIndex::AccountDependentIndexUpdateWorker.jobs.clear

      account.update!(updated_at: Time.current)
      expect(SearchIndex::AccountDependentIndexUpdateWorker.jobs.size).to eq 0

      account.update!(name: "New Name")
      expect(SearchIndex::AccountDependentIndexUpdateWorker).to have_enqueued_sidekiq_job(account.id)
    end
  end
end
