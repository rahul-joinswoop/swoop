# frozen_string_literal: true

require "rails_helper"

describe Reviews::WebReview, type: :model do
  context "on initialization" do
    subject { Reviews::WebReview.new }

    it "sets WebReview#branding to true" do
      expect(subject.branding).to eq(true)
    end
  end

  describe "#send_review_sms" do
    subject { Reviews::WebReview.new(job: job).send_review_sms }

    let(:job) { build_stubbed(:job) }

    it "sends notification via Sms::BrandedReviewLink" do
      expect(Sms::BrandedReviewLink).to receive(:send_notification)

      subject
    end
  end
end
