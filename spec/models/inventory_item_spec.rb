# frozen_string_literal: true

require "rails_helper"

describe TireType do
  let(:tire_type) { create(:tire_type) }

  it "creates tire_type" do
    expect(tire_type).to be_valid
  end
end
