# frozen_string_literal: true

require 'rails_helper'

describe FleetCompany do
  describe "auction_max_bidders" do
    it "returns the setting for 'Auction Max Bidders'" do
      company = FleetCompany.new
      company.settings.build(key: 'Auction Max Bidders', value: 1000)
      expect(company.auction_max_bidders).to eq 1000
    end

    it "returns the default max bidders" do
      company = FleetCompany.new
      expect(company.auction_max_bidders).to eq Auction::Auction.default_max_bidders
    end
  end

  describe "auction_max_distance" do
    it "returns the setting for 'Auction Max Candidate Distance'" do
      company = FleetCompany.new
      company.settings.build(key: 'Auction Max Candidate Distance', value: 5000)
      expect(company.auction_max_distance).to eq 5000
    end

    it "returns the default max candidate distance" do
      company = FleetCompany.new
      expect(company.auction_max_distance).to eq Auction::Auction.default_max_candidate_distance
    end
  end

  describe "auction_max_eta" do
    it "returns the setting for 'Auction Max Candidate ETA'" do
      company = FleetCompany.new
      company.settings.build(key: 'Auction Max Candidate ETA', value: 100)
      expect(company.auction_max_eta).to eq 100
    end

    it "returns the default max candidate distance" do
      company = FleetCompany.new
      expect(company.auction_max_eta).to eq Auction::Auction.default_max_candidate_eta
    end

    it "derives a maximum possible distance from the max eta" do
      company = FleetCompany.new
      company.settings.build(key: 'Auction Max Candidate ETA', value: 120)
      expect(company.auction_max_possible_distance).to eq 160
    end
  end
end
