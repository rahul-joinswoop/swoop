# frozen_string_literal: true

require "rails_helper"

describe Answer do
  describe "needs_info?" do
    subject { answer.needs_info? }

    context "when info is required" do
      let(:question) { build_stubbed :question, question: Answer::NEEDS_INFO.keys.sample }
      let(:answer) { build_stubbed :answer, question: question, answer: Answer::NEEDS_INFO[question.question].sample }

      it "works" do
        expect(subject).to be_truthy
      end
    end

    context "when answer_info is required" do
      let(:question) { build_stubbed :question }
      let(:answer) { build_stubbed :answer, question: question }

      it "works" do
        expect(subject).to be_falsey
      end
    end
  end
end
