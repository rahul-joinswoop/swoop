# frozen_string_literal: true

require "rails_helper"

describe APIAccessToken do
  subject { api_access_token }

  let(:api_access_token) { build(:api_access_token) }

  let(:expected_location_array) do
    [
      { "locationId": "1", "address": "123 Super St." },
    ]
  end

  describe "#location_ids_with_site" do
    include_context "basic rsc setup"

    let(:vendor_id) { "999" }
    let(:facility_id) { "1" }

    it "includes location ids that have a site" do
      issc_site.update!(dispatchable: true)

      expect(subject.location_ids_with_site.length).to eq(1)

      first_location = subject.location_ids_with_site.first

      expect(first_location["locationId"]).to eq(location.id)
      expect(first_location["address"]).to eq("123 Super St.")
    end

    it "excludes location ids that have a non-dispatchable site" do
      issc.site.update!(dispatchable: false)

      expect(subject.location_ids_with_site).to be_empty
    end

    it "does not include location ids that lack a site" do
      issc.update!(site: nil)

      expect(subject.location_ids_with_site).to eq []
    end
  end

  describe "#all_associated_location_ids" do
    include_context "basic rsc setup"

    let(:vendor_id) { "999" }
    let(:facility_id) { "1" }

    it "even includes location ids that lack a site" do
      expect(subject.all_associated_location_ids.length).to eq(1)

      first_location = subject.all_associated_location_ids.first

      expect(first_location["locationId"]).to eq(location.id)
      expect(first_location["address"]).to eq("123 Super St.")
    end
  end

  describe "#current_status" do
    subject { api_access_token.current_status }

    let(:expected_status) { :connected }

    context "when access token is connected" do
      include_context "basic rsc setup"

      let(:vendor_id) { "my-vendor-id" }
      let(:facility_id) { "1" }

      context "and all locations have sites" do
        it { is_expected.to eq(issc.status) }
      end

      context "and not all locations have sites" do
        include_context "basic rsc setup"

        let(:issc_site) { nil }
        let(:expected_status) { :needs_configured }

        it { is_expected.to eq(expected_status) }
      end

      context "but there are no isscs" do
        before do
          api_access_token.isscs.destroy_all
        end

        it { is_expected.to eq(:needs_configured) }
      end
    end

    context "when access token is not connected" do
      let(:api_access_token) do
        create(:api_access_token,
               connected: false,
               vendorid: "my-vendor-id")
      end
      let(:expected_status) { :disconnected }

      it { is_expected.to eq(expected_status) }
    end
  end

  context 'when token deleted' do
    before do
      subject.save!
      subject.deleted_at = Time.now
    end

    it 'updates access_token' do
      expect { subject.save! }.to change(subject, :access_token).from(subject.access_token).to('-')
    end

    it 'clears callback_token' do
      expect { subject.save! }.to change(subject, :callback_token).from(subject.callback_token).to(nil)
    end

    it 'marks as disconnected' do
      expect { subject.save! }.to change(subject, :connected).from(true).to(false)
    end
  end

  describe "RSC API helpers" do
    let(:api) { subject.api }

    it "returns an instance of the api with the token" do
      expect(api).to be_a(Agero::Rsc::API)
    end

    it "signs in to RSC" do
      expect(api).to receive(:sign_in)
      subject.sign_in
    end

    it "signs out from RSC" do
      expect(api).to receive(:sign_out)
      subject.sign_out
    end

    context "with a current subscription" do
      before { subject.update!(subscriptionid: "12345", callback_token: "ABCDE") }

      it "does not try to resubscribe" do
        expect(api).not_to receive(:subscribe_to_server_notifications)
        subject.subscribe_to_notifications
      end

      it "signs out of current subscription" do
        expect(api).to receive(:unsubscribe_from_server_notifications).with("12345")
        subject.unsubscribe_from_notifications
      end
    end

    context "without a current subscription" do
      before { subject.update!(subscriptionid: nil, callback_token: nil) }

      it "subscribes to notifications" do
        allow(SecureRandom).to receive(:hex).and_return("010101")
        response = Agero::Rsc::API::ResponsePayload.new("subscriptionId" => "9999")
        expect(api).to receive(:subscribe_to_server_notifications).with("010101").and_return(response)
        subject.subscribe_to_notifications
        subject.reload
        expect(subject.subscriptionid).to eq "9999"
        expect(subject.callback_token).to eq "010101"
      end

      it "does not stay subscribed if there is an error subscribing" do
        allow(SecureRandom).to receive(:hex).and_return("010101")
        expect(api).to receive(:subscribe_to_server_notifications).and_raise("fail")
        expect { subject.subscribe_to_notifications }.to raise_error("fail")
        subject.reload
        expect(subject.subscriptionid).to eq nil
        expect(subject.callback_token).to eq nil
      end

      it "does not sign out of the api" do
        expect(api).not_to receive(:unsubscribe_from_server_notifications)
        subject.unsubscribe_from_notifications
      end
    end
  end

  describe "find_job_by_dispatchid" do
    let(:job) do
      create :fleet_motor_club_job, {
        fleet_company: fleet_company,
        adjusted_created_at: Time.now,
        rescue_company: rescue_company,
      }
    end

    let!(:issc_dispatch_request) do
      create(
        :issc_dispatch_request,
        dispatchid: "1234",
        issc: issc,
        status: "new_request",
        max_eta: 90.minutes,
        job: job
      )
    end

    let(:rescue_company) { create(:rescue_company) }
    let(:fleet_company) { create(:fleet_company, :ago) }
    let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM, api_access_token: api_access_token) }
    let(:api_access_token) { create(:api_access_token) }

    it "finds a job by dispatchid" do
      expect(api_access_token.find_job_by_dispatchid("1234")).to eq job
    end

    it "does not find a job with a different dispatchid" do
      expect(api_access_token.find_job_by_dispatchid("5432")).to eq nil
    end

    it "does not find a job belonging to a different access token" do
      other_access_token = create(:api_access_token)
      expect(other_access_token.find_job_by_dispatchid(issc_dispatch_request.dispatchid)).to eq nil
    end

    it "does not find deleted jobs" do
      job.update!(deleted_at: Time.now)
      expect(api_access_token.find_job_by_dispatchid(issc_dispatch_request.dispatchid)).to eq nil
    end
  end

  describe "logout!" do
    let(:rescue_company) { create(:rescue_company) }
    let(:fleet_company) { create(:fleet_company, :agero, :ago) }
    let(:api_access_token) { create(:api_access_token, company: rescue_company) }
    let!(:issc_location) { create(:issc_location, :default) }
    let!(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM, issc_location: issc_location, api_access_token: api_access_token) }
    let!(:site_facility_map) { SiteFacilityMap.create!(company: rescue_company, fleet_company: fleet_company, facility: issc_location.location) }

    it "signs out and unsubscribe from RSC and delete issc records" do
      expect(api_access_token).to receive(:sign_out)
      expect(api_access_token).to receive(:unsubscribe_from_notifications)

      api_access_token.logout!

      expect(api_access_token.reload.deleted_at).to be_present
      expect(issc.reload.deleted_at).to be_present
      expect(issc_location.reload.deleted_at).to be_present
      expect(site_facility_map.reload.deleted_at).to be_present
    end
  end
end
