# frozen_string_literal: true

require 'rails_helper'

describe Report do
  describe "reports.sql seeds" do
    # Create a spec for each report
    Report.report_names.each do |report_name|
      it "runs the #{report_name.inspect} report" do
        report = Report.find_by!(name: report_name)
        result = report.results.build(user: create(:user), company: create(:super_company))
        result.run_query(stubbed_report_params(report))
      end
    end

    # Stub out report parameters with fake values.
    def stubbed_report_params(report)
      available_params = report.available_params || {}
      available_params = available_params.merge((report.filter_sql || {}))
      params = {}
      if report.all_params.present?
        report.all_params.each do |name|
          next unless available_params.include?(name)
          value = "1"
          if name == "start_date"
            value = "2018-05-01"
          elsif name == "end_date"
            value = "2018-05-02"
          end
          params[name] = value
        end
      end
      params
    end
  end

  describe '#transaction_type' do
    subject(:transaction_type_filter_options) { Report.new.transaction_type(nil, nil) }

    it 'returns the transaction_type filter options' do
      expect(transaction_type_filter_options).to match({
        label: "Transaction Type:",
        options: [
          { text: 'All', value: '0' },
          { text: 'Payment', value: '1' },
          { text: 'Discount', value: '2' },
          { text: 'Refund', value: '3' },
          { text: 'Write Off', value: '4' },
          { text: 'Swoop Charges', value: '5' },
        ],
        type: "options",
      })
    end
  end

  describe '#payment_status' do
    subject(:payment_status_filter_options) { Report.new.payment_status(nil, nil) }

    it 'returns the payment_status filter options' do
      expect(payment_status_filter_options).to match({
        label: "Payment Status:",
        options: [
          { text: 'All', value: '0' },
          { text: 'Charged', value: '1' },
          { text: 'Refunded', value: '2' },
        ],
        type: "options",
      })
    end
  end
end
