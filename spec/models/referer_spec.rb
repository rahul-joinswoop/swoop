# frozen_string_literal: true

require "rails_helper"

describe Referer do
  describe '.init_all' do
    before { Referer.init_all }

    it 'works' do
      expect(Referer.where(name: Referer::NAMES).pluck(:name)).to contain_exactly(*Referer::NAMES)
    end
  end
end
