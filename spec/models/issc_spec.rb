# frozen_string_literal: true

require 'rails_helper'

describe Issc do
  describe "clear delete_pending_at on save" do
    it "clears the delete_pending_at flag when deleted_at is set" do
      issc = create(:isscs)
      issc.update!(delete_pending_at: Time.current)
      issc.reload
      expect(issc.delete_pending_at).not_to eq nil
      expect(issc.deleted_at).to eq nil

      issc.update!(deleted_at: Time.current)
      issc.reload
      expect(issc.delete_pending_at).to eq nil
      expect(issc.deleted_at).not_to eq nil
    end
  end

  describe "get_login_candidates" do
    let!(:agero_account) { create(:account, company: rescue_company, client_company: agero_company) }
    let(:agero_company) { create(:fleet_company, :agero) }
    let!(:geico_account) { create(:account, company: rescue_company, client_company: geico_company) }
    let(:geico_company) { create(:fleet_company, :geico) }
    let(:rescue_company) { create(:rescue_company) }
    let!(:agero_rescue_provider) { create(:rescue_provider, site: site, company: agero_company) }
    let!(:geico_rescue_provider) { create(:rescue_provider, site: site, company: geico_company) }
    let!(:rsc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM, status: status, site: site) }
    let!(:issc) { create(:ago_issc, company: rescue_company, system: nil, status: status, site: site) }

    context "registered" do
      let(:status) { "Registered" }
      let(:site) { create(:partner_site, within_hours: true, dispatchable: true, company: rescue_company) }

      it "finds the Issc's to log in for the system" do
        expect(Issc.get_login_candidates(Issc::RSC_SYSTEM)).to eq [rsc]
        expect(Issc.get_login_candidates(Issc::ISSC_SYSTEM)).to eq [issc]
      end

      it "does not find any Issc's to log out for the system" do
        expect(Issc.get_logout_candidates(Issc::RSC_SYSTEM)).to eq []
        expect(Issc.get_logout_candidates(Issc::ISSC_SYSTEM)).to eq []
      end

      context "site is closed" do
        let(:site) { create(:partner_site, within_hours: false, dispatchable: true, company: rescue_company) }

        it "does not find any Issc's to log in for the system" do
          expect(Issc.get_login_candidates(Issc::RSC_SYSTEM)).to eq []
          expect(Issc.get_login_candidates(Issc::ISSC_SYSTEM)).to eq []
        end
      end
    end

    context "logged in" do
      let(:status) { "LoggedIn" }
      let(:site) { create(:partner_site, within_hours: false, dispatchable: true, company: rescue_company) }

      it "finds the Issc's to log out for the system" do
        expect(Issc.get_logout_candidates(Issc::RSC_SYSTEM)).to eq [rsc]
        expect(Issc.get_logout_candidates(Issc::ISSC_SYSTEM)).to eq [issc]
      end

      it "does not find any Issc's to log in for the system" do
        expect(Issc.get_login_candidates(Issc::RSC_SYSTEM)).to eq []
        expect(Issc.get_login_candidates(Issc::ISSC_SYSTEM)).to eq []
      end

      context "site is open" do
        let(:site) { create(:partner_site, within_hours: true, dispatchable: true, company: rescue_company) }

        it "does not find any Issc's to log in for the system" do
          expect(Issc.get_logout_candidates(Issc::RSC_SYSTEM)).to eq []
          expect(Issc.get_logout_candidates(Issc::ISSC_SYSTEM)).to eq []
        end
      end
    end
  end
end
