# frozen_string_literal: true

require "rails_helper"

describe PasswordRequest do
  subject(:password_request) do
    described_class.new(user: user, user_input: user_input).trigger!
  end

  let(:user) { create(:user) }
  let(:user_input) { user.email }

  describe '#trigger!' do
    it 'sets a uuid' do
      expect(password_request.uuid).not_to be_nil
    end

    it 'invalidates unused requests' do
      expect_any_instance_of(described_class).to receive(:invalidate_unused_request)

      password_request
    end

    it 'associates unused requests with current password request' do
      expect_any_instance_of(described_class).to receive(:associate_with_invalid_requests)

      password_request
    end

    it 'sets the user_input attribute' do
      expect(password_request.user_input).to eq user_input
    end

    context 'when user is nil' do
      let(:user) { nil }
      let(:user_input) { 'developer@joinswoop.co,' }

      it 'sets the user_input' do
        expect(password_request.user_input).to eq user_input
      end

      it 'sets user to nil' do
        expect(password_request.user).to be_nil
      end

      it 'sets valid_request to false' do
        expect(password_request.valid_request).to eq false
      end
    end
  end

  describe '#invalidate_unused_request' do
    subject(:invalidate_unused_request) do
      described_class.new(user: user).send(:invalidate_unused_request)
    end

    before :each do
      allow(described_class).to receive(:where).and_call_original
    end

    it 'searches for unused requests' do
      expect(described_class).to receive(:where).with({
        used_at: nil,
        valid_request: true,
        user: user,
      })

      invalidate_unused_request
    end

    it 'invalidates unused requests' do
      uuid = SecureRandom.uuid
      described_class.create!(user: user, uuid: uuid, user_input: user.email)

      expect(described_class.valid_request_for?(uuid)).to be true

      invalidate_unused_request

      expect(described_class.valid_request_for?(uuid)).to be false
    end

    context 'when there is no user' do
      let(:user) { nil }

      it 'returns nil' do
        expect(invalidate_unused_request).to be_nil
      end
    end
  end

  describe '#send_sms' do
    let(:user) { create(:user, email: nil, phone: '55555555') }
    let(:user_input) { 'tow-user' }

    before :each do
      allow(described_class).to receive(:send_sms)
    end

    it 'triggers sms job to text the user' do
      expect(described_class).to receive(:send_sms)

      password_request
    end
  end

  describe '.valid_request_for?' do
    subject(:valid_request_for?) { described_class.valid_request_for? uuid_to_validate }

    let(:uuid_to_validate) { valid_password_request.uuid }
    let(:uuid) { SecureRandom.uuid }
    let(:valid_request) { true }
    let(:created_at) { nil }

    let(:valid_password_request) do
      create(:password_request,
             {
               user: user,
               uuid: uuid,
               valid_request: valid_request,
               user_input: user.email,
               created_at: created_at,
             })
    end

    it { is_expected.to be true }

    it "fails on blank uuids" do
      expect(described_class).not_to receive(:find_by_uuid)

      expect(described_class).not_to be_valid_request_for(' ')
      expect(described_class).not_to be_valid_request_for(nil)
    end

    context 'when uuid is not found' do
      let(:uuid_to_validate) { 'in-va-li-d' }

      it { is_expected.to be false }
    end

    context 'when password_request is not a valid request' do
      let(:valid_request) { false }

      it { is_expected.to be false }
    end

    context 'when password_request has expired' do
      let(:created_at) { 10.days.ago }

      it { is_expected.to be false }
    end
  end

  describe '#invalidate!', freeze_time: true do
    it 'invalidates the described_class' do
      password_request.invalidate!
      expect(password_request).not_to be_valid_request
      expect(password_request.used_at).to eq(Time.current)
    end

    context 'when oauth access tokens exist' do
      let!(:access_token) do
        FactoryBot.create(:doorkeeper_access_token, resource_owner_id: user.id)
      end

      it 'revokes access token' do
        expect { password_request.invalidate! }.to change {
          user.access_tokens.where(revoked_at: nil).length
        }.by(-1)
      end
    end
  end
end
