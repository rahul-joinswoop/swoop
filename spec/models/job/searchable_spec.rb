# frozen_string_literal: true

require 'rails_helper'

describe Job::Searchable do
  it "has a scope to preload associations for ElasticSearch" do
    expect(Job.elastic_search_preload.to_a).to be_a(Array)
  end

  it "serializes the job into a document" do
    job = create(:job).reload
    expect(job.as_indexed_json).to eq({
      "id" => job.id,
      "created_at" => job.created_at,
      "status" => job.status,
      "notes" => nil,
      "account_id" => nil,
      "partner_notes" => nil,
      "po_number" => nil,
      "invoice_id" => nil,
      "unit_number" => nil,
      "ref_number" => nil,
      "dispatch_notes" => nil,
      "type" => nil,
      "fleet_notes" => nil,
      "driver_notes" => nil,
      "swoop_notes" => nil,
      "original_job_id" => nil,
      "storage_type_id" => nil,
      "policy_number" => job.policy_number,
      "dispatched_at" => nil,
      "completed_at" => nil,
      "stored_at" => nil,
      "invoice_states" => [],
      "paid_invoices?" => false,
      "unpaid_invoices?" => false,
      "claim_number" => nil,
      "rescue_company" => { "id" => job.rescue_company.id, "name" => job.rescue_company.name },
      "fleet_company" => { "id" => job.fleet_company.id, "name" => job.fleet_company.name },
      "customer" => { "full_name" => job.customer.full_name, "index_phone" => job.customer.index_phone },
      "stranded_vehicle" => {
        "make" => job.stranded_vehicle.make,
        "model" => job.stranded_vehicle.model,
        "license" => job.stranded_vehicle.license,
        "year" => job.stranded_vehicle.year,
        "serial_number" => job.stranded_vehicle.serial_number,
        "vin_indexed" => job.stranded_vehicle.vin_indexed,
        "vin_reverse_indexed" => job.stranded_vehicle.vin_reverse_indexed,
      },
      "service_code" => { "name" => "Tow" },
    })
  end
end
