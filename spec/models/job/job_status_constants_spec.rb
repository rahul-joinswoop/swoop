# frozen_string_literal: true

require 'rails_helper'

describe 'job status constants' do
  let(:agero) { create(:fleet_company, :agero) }

  let(:job) do
    FleetMotorClubJob.new({
      fleet_company: agero,
    })
  end

  before do
    stub_const "ALL_JOB_STATUSES", [
      Job::STATUS_PREDRAFT,
      Job::STATUS_DRAFT,
      Job::STATUS_CREATED,
      Job::STATUS_AUTO_ASSIGNING,
      Job::STATUS_PENDING,
      Job::STATUS_UNASSIGNED,
      Job::STATUS_ASSIGNED,
      Job::STATUS_ACCEPTED,
      Job::STATUS_REJECTED,
      Job::STATUS_REASSIGNED,
      Job::STATUS_DISPATCHED,
      Job::STATUS_ENROUTE,
      Job::STATUS_ONSITE,
      Job::STATUS_TOWING,
      Job::STATUS_TOWDESTINATION,
      Job::STATUS_STORED,
      Job::STATUS_RELEASED,
      Job::STATUS_COMPLETED,
      Job::STATUS_CANCELED,
      Job::STATUS_GOA,
      Job::STATUS_DELETED,
    ].freeze

    # Statuses for FleetMotorClubJobs only
    stub_const "FLEET_MOTOR_CLUB_JOB_ONLY_STATUSES", [
      Job::STATUS_SUBMITTED,
      Job::STATUS_EXPIRED,
      Job::STATUS_ETAREJECTED,
    ].freeze
  end

  it 'saves all constants' do
    (ALL_JOB_STATUSES + FLEET_MOTOR_CLUB_JOB_ONLY_STATUSES).each do |status|
      job.status = status
      expect { job.save! }.not_to raise_exception
      expect(job.reload.status).to eql(status)
    end
  end
end
