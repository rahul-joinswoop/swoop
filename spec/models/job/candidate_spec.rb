# frozen_string_literal: true

require 'rails_helper'

describe Job::Candidate do
  let(:rescue_company_sites) { build_list(:partner_site, 1) }
  let(:swoop_company) { create(:super_company) }
  let(:rescue_company) { build :rescue_company, :finish_line, sites: rescue_company_sites, name: Faker::Company.name }
  let!(:partners_swoop_account) do
    create(
      :account,
      name: 'Swoop',
      company: rescue_company,
      client_company: swoop_company
    )
  end
  let(:fleet_company) { create(:fleet_company) }
  let(:drop_location) { nil }
  let(:priority_response) { nil }
  let(:fleet_managed_job) { create(:fleet_managed_job, fleet_company: fleet_company, drop_location: drop_location, priority_response: priority_response) }
  let(:estimate) { nil }
  let(:job_candidate) { create(:job_candidate, job: fleet_managed_job, company: rescue_company, site: rescue_company_sites.first, estimate: estimate) }

  it "shows that a AutoAssign::AutoAssignCandidate is an alias of Job::Candidate" do
    expect(AutoAssign::AutoAssignCandidate).to eq(described_class)
  end

  context 'reading' do
    let(:job) { create(:rescue_job) }

    describe 'using Job::Candidate class' do
      it 'reads a Job::Candidate::Site and Job::Candidate::Truck record into a Job::Candidate instance' do
        # Step 1: Save a Job::Candidate::Site & Job::Candidate::Truck to the database
        Job::Candidate::Site.create! job: job, company: job.rescue_company
        Job::Candidate::Truck.create! job: job, company: job.rescue_company

        # Step 2: The candidate loads into a Job::Candidate instance, and
        # has the type 'Job::Candidate::Site' & 'Job::Candidate::Truck' respectively
        expect(Job::Candidate.count).to eql 2
        expect(Job::Candidate.first).to be_an_instance_of(Job::Candidate::Site)
        expect(Job::Candidate.last).to be_an_instance_of(Job::Candidate::Truck)
        expect(Job::Candidate.first.type).to eql('Job::Candidate::Site')
        expect(Job::Candidate.last.type).to eql('Job::Candidate::Truck')
      end
    end

    describe 'using AutoAssign::AutoAssignCandidate class' do
      it 'reads an AutoAssign::AutoAssignCandidate record using an AutoAssign::AutoAssignCandidate class symbol' do
        # Step 1: Save a AutoAssign::AutoAssignCandidate to the database
        ApplicationRecord.execute_query(
          "INSERT INTO auto_assign_candidates (job_id, company_id) VALUES (?,?)",
          job.id,
          job.rescue_company.id
        )
        # Step 2: The job candidate appears to be an AutoAssign::AutoAssignCandidate
        expect(AutoAssign::AutoAssignCandidate.count).to eql 1
        expect(AutoAssign::AutoAssignCandidate.first).to be_an_instance_of(AutoAssign::AutoAssignCandidate)

        # Step 3: ...but it's actually a Job::Candidate
        expect(AutoAssign::AutoAssignCandidate.first).to be_an_instance_of(Job::Candidate)
      end

      it 'reads a AutoAssign::SiteCandidate and AutoAssign::TruckCandidate record into a Job::Candidate instance' do
        # Step 1: Save a AutoAssign::SiteCandidate and AutoAssign::TruckCandidate to the database
        ApplicationRecord.execute_query(
          "INSERT INTO auto_assign_candidates (job_id, company_id, type) VALUES (?,?,?), (?,?,?)",
          job.id,
          job.rescue_company.id,
          'AutoAssign::SiteCandidate',
          job.id,
          job.rescue_company.id,
          'AutoAssign::TruckCandidate'
        )

        # Step 2: The candidate loads into a Job::Candidate instance, and
        # has the type 'AutoAssign::SiteCandidate' & 'AutoAssign::TruckCandidate' respectively
        records = Job::Candidate.all
        expect(records.count).to eql 2
        expect(records.first).to be_an_instance_of(Job::Candidate::Site)
        expect(records.last).to be_an_instance_of(Job::Candidate::Truck)

        # Check for overridden values
        expect(records.first.type).to eql('Job::Candidate::Site')
        expect(records.last.type).to eql('Job::Candidate::Truck')

        # Bypassing overridden `type`
        expect(records.first.attributes['type']).to eql('AutoAssign::SiteCandidate')
        expect(records.last.attributes['type']).to eql('AutoAssign::TruckCandidate')
      end
    end
  end

  describe 'rescue company' do
    it 'is aliased to company' do
      expect(job_candidate.company).to eql rescue_company
      expect(job_candidate.rescue_company).to eql rescue_company
    end
  end

  describe 'fleet company' do
    context 'the job has one' do
      it 'exists' do
        expect(fleet_managed_job.fleet_company).to eql fleet_company
        expect(job_candidate.fleet_company).to eql fleet_company
      end
    end
  end

  describe 'account' do
    context 'the candidate\'s rescue company is set' do
      it 'is the Rescue Company\'s Swoop company account' do
        expect(job_candidate.rescue_company).to eql rescue_company
        expect(job_candidate.account).to eql partners_swoop_account
      end
    end
  end

  describe 'ISSC contractor ID' do
    it 'is nil' do
      expect(job_candidate.issc_contractor_id).to be_nil
    end
  end

  describe 'estimate of minutes to do the job' do
    context 'the job has a drop location and the candidate\'s estimate has minutes for A->B->C->A' do
      let(:drop_location) { create(:location, :accurate_auto_body) }
      let(:estimate) { create(:estimate, job: fleet_managed_job, seconds_ab: 6 * 60, seconds_bc: 30 * 60, seconds_ca: 21 * 60, seconds_ba: nil,) }

      it 'mins_p2p is the sum of A->B->C->A + 20 minutes for time on site + 20 minutes at the drop off' do
        expect(drop_location).not_to be_nil
        expect(Job::Candidate::ESTIMATED_MINS_ON_SITE).to eql 20
        expect(Job::Candidate::ESTIMATED_MINS_AT_DROPOFF).to eql 20
        expect(job_candidate.mins_p2p).to eql 6 + 20 + 30 + 20 + 21
      end
    end

    context 'the job doesn\'t have a drop location but the candidate\'s estimate has minutes for A->B->A' do
      let(:estimate) { create(:estimate, job: fleet_managed_job, seconds_ab: 12 * 60, seconds_ba: 19 * 60, seconds_bc: nil, seconds_ca: nil) }

      it 'mins_p2p is the sum of A->B->A + 20 minutes for time on site' do
        expect(drop_location).to be_nil
        expect(Job::Candidate::ESTIMATED_MINS_ON_SITE).to eql 20
        expect(job_candidate.mins_p2p).to eql 12 + 20 + 19
      end
    end

    context 'the candidate\'s estimate does not have enough legs populated' do
      let(:estimate) { create(:estimate, job: fleet_managed_job, seconds_ab: 55 * 60, seconds_ba: nil, seconds_bc: nil, seconds_ca: nil) }

      it 'mins_p2p is nil' do
        expect(drop_location).to be_nil
        expect(job_candidate.mins_p2p).to be_nil
      end
    end
  end

  describe 'estimate of miles to do the job' do
    context 'the candidate\'s estimate has a drop location and miles for A->B->C->A' do
      let(:drop_location) { create(:location, :accurate_auto_body) }
      let(:estimate) { create(:estimate, job: fleet_managed_job, drop_location: drop_location, meters_ab: 3210, meters_bc: 11900, meters_ca: 15600, meters_ba: nil) }

      it 'miles_p2p is the sum of A->B->C->A' do
        expect(drop_location).not_to be_nil
        expect(Estimate::METERS_MILES).to eql 0.00062137
        expect(job_candidate.miles_p2p).to eql(((3210 + 11900 + 15600) * 0.00062137).round(1))
      end
    end

    context 'the candidate\'s estimate doesn\'t have a drop location but it has miles for A->B->A' do
      let(:estimate) { create(:estimate, job: fleet_managed_job, drop_location: nil, meters_ab: 4051, meters_ba: 47920, meters_bc: nil, meters_ca: nil) }

      it 'miles_p2p is the sum of A->B->A' do
        expect(drop_location).to be_nil
        expect(Estimate::METERS_MILES).to eql 0.00062137
        expect(job_candidate.miles_p2p).to eql(((4051 + 47920) * 0.00062137).round(1))
      end
    end

    context 'the candidate\'s estimate does not have enough legs populated' do
      let(:estimate) { create(:estimate, job: fleet_managed_job, drop_location: nil, meters_ab: 4051, meters_ba: nil, meters_bc: nil, meters_ca: nil) }

      it 'miles_p2p is nil' do
        expect(drop_location).to be_nil
        expect(job_candidate.miles_p2p).to be_nil
      end
    end
  end

  describe 'drop location' do
    context 'the candidates\'s job and the candidates\'s estimate both have a drop location' do
      let(:drop_location) { create(:location, :accurate_auto_body) }
      let(:estimate) { create(:estimate, job: fleet_managed_job, drop_location: drop_location) }

      it 'the candidate has the drop location' do
        expect(fleet_managed_job.drop_location).to eql drop_location
        expect(estimate.drop_location).to eql drop_location
        expect(job_candidate.drop_location).to eql drop_location
      end
    end

    context 'the job has a drop location, but the estimate doesn\'t' do
      let(:drop_location) { create(:location, :accurate_auto_body) }
      let(:estimate) { create(:estimate, job: fleet_managed_job, drop_location: nil) }

      it 'the candidate has the drop location' do
        expect(fleet_managed_job.drop_location).to eql drop_location
        expect(estimate.drop_location).to be_nil
        expect(job_candidate.drop_location).to eql drop_location
      end
    end

    context 'the estimate has a drop location, but the job doesn\'t' do
      let(:drop_location) { create(:location, :accurate_auto_body) }
      let(:fleet_managed_job) { create(:fleet_managed_job, fleet_company: fleet_company, drop_location: nil) }
      let(:estimate) { create(:estimate, job: fleet_managed_job, drop_location: drop_location) }

      it 'the candidate has the drop location' do
        expect(fleet_managed_job.drop_location).to be_nil
        expect(estimate.drop_location).to eql drop_location
        expect(job_candidate.drop_location).to eql drop_location
      end
    end

    context 'neither the job nor the estimate have a drop location' do
      let(:estimate) { create(:estimate, job: fleet_managed_job, drop_location: nil) }

      it 'the candidate has no drop location' do
        expect(fleet_managed_job.drop_location).to be_nil
        expect(estimate.drop_location).to be_nil
        expect(job_candidate.drop_location).to be_nil
      end
    end
  end

  describe 'is expected to implement' do
    let(:estimate) { create(:estimate, job: fleet_managed_job, seconds_ab: 12 * 60, seconds_ba: 19 * 60, seconds_bc: nil, seconds_ca: nil) }

    context 'Invoicable' do
      it('#fleet_company') { expect { job_candidate.fleet_company }.not_to raise_error }
      it('#rescue_company') { expect { job_candidate.rescue_company }.not_to raise_error }
      it('#site') { expect { job_candidate.site }.not_to raise_error }
      it('#account') { expect { job_candidate.account }.not_to raise_error }
      it('#mins_p2p') { expect { job_candidate.mins_p2p }.not_to raise_error }
      it('#miles_p2p') { expect { job_candidate.miles_p2p }.not_to raise_error }
      it('#estimate') { expect { job_candidate.estimate }.not_to raise_error }
      it('#drop_location') { expect { job_candidate.drop_location }.not_to raise_error }
      it('#job_id') { expect { job_candidate.job_id }.not_to raise_error }
      it('#issc_contractor_id') { expect { job_candidate.issc_contractor_id }.not_to raise_error }
    end
  end

  describe 'relationships' do
    let(:unsaved_job_candidate) { build(:job_candidate, job: fleet_managed_job, company: rescue_company, site: rescue_company_sites.first, estimate: estimate) }

    context 'job' do
      describe 'mandatory' do
        context 'is present' do
          it 'is valid' do
            expect(fleet_managed_job).to be_a Job
            expect(unsaved_job_candidate.job).to be_a Job
            expect(unsaved_job_candidate).to be_valid
          end
        end

        context 'is missing' do
          let(:fleet_managed_job) { nil }

          it 'is invalid' do
            expect(fleet_managed_job).to be_nil
            expect(unsaved_job_candidate.job).to be_nil
            expect(unsaved_job_candidate).not_to be_valid
          end
        end
      end
    end

    context 'company' do
      describe 'mandatory' do
        context 'is present' do
          it 'is valid' do
            expect(rescue_company).to be_a RescueCompany
            expect(unsaved_job_candidate).to be_valid
          end
        end

        context 'is missing' do
          let(:rescue_company) { nil }

          it 'is invalid' do
            expect(rescue_company).to be_nil
            expect(unsaved_job_candidate).not_to be_valid
          end
        end
      end

      describe 'must be a FleetCompany' do
        context 'is a rescue company' do
          it 'is valid' do
            expect(rescue_company).to be_a RescueCompany
            expect(unsaved_job_candidate).to be_valid
          end
        end

        context 'is a fleet company' do
          let(:rescue_company) { create(:fleet_company) }

          it 'is invalid' do
            expect(rescue_company).to be_a FleetCompany
            expect(unsaved_job_candidate).not_to be_valid
          end
        end
      end
    end

    context 'estimate' do
      describe 'is optional' do
        it 'valid when it exists' do
          estimate_for_the_job_candidate_to_do_the_job = create(:estimate, job: fleet_managed_job)
          job_candidate.estimate = estimate_for_the_job_candidate_to_do_the_job
          expect(job_candidate.estimate).not_to be_nil
          expect(job_candidate).to be_valid
        end
        it 'valid when it doesn\'t' do
          expect(job_candidate.estimate).to be_nil
          expect(job_candidate).to be_valid
        end
      end

      describe 'is able to indicate an estimate of time and distance to do the job' do
        it { should belong_to(:estimate) }
      end
    end
  end

  context 'scopes' do
    let!(:auto_assign_pool_site_candidates) { create_list(:site_candidate, 2, job: fleet_managed_job, company: fleet_managed_job.rescue_company, pool_type: 'auto_assign') }
    let!(:auto_assign_pool_truck_candidates) { create_list(:truck_candidate, 3, job: fleet_managed_job, company: fleet_managed_job.rescue_company, pool_type: 'auto_assign') }
    let!(:manual_assign_with_swoop_account_site_candidates) { create_list(:site_candidate, 4, job: fleet_managed_job, company: fleet_managed_job.rescue_company, pool_type: 'manual_assign_with_swoop_account') }
    let!(:manual_assign_with_swoop_account_truck_candidates) { create_list(:truck_candidate, 5, job: fleet_managed_job, company: fleet_managed_job.rescue_company, pool_type: 'manual_assign_with_swoop_account') }
    let!(:manual_assign_oon_preferred_site_candidates) { create_list(:site_candidate, 6, job: fleet_managed_job, company: fleet_managed_job.rescue_company, pool_type: 'manual_assign_oon_preferred') }
    let!(:manual_assign_oon_preferred_truck_candidates) { create_list(:truck_candidate, 7, job: fleet_managed_job, company: fleet_managed_job.rescue_company, pool_type: 'manual_assign_oon_preferred') }
    let!(:manual_assign_without_oon_preferred_without_swoop_account_site_candidates) { create_list(:site_candidate, 8, job: fleet_managed_job, company: fleet_managed_job.rescue_company, pool_type: 'manual_assign_without_oon_preferred_without_swoop_account') }
    let!(:manual_assign_without_oon_preferred_without_swoop_account_truck_candidates) { create_list(:truck_candidate, 9, job: fleet_managed_job, company: fleet_managed_job.rescue_company, pool_type: 'manual_assign_without_oon_preferred_without_swoop_account') }

    describe '#for_auto_assign' do
      it 'return all candidates for a job in the auto_assign pool' do
        expected_count = Job::Candidate.where(pool_type: 'auto_assign', job_id: fleet_managed_job.id).count

        expect(Job::Candidate.for_auto_assign(fleet_managed_job.id).count).to eq(expected_count)
        expect(Job::Candidate.for_auto_assign(fleet_managed_job.id).pluck(:pool_type).uniq).to eq(['auto_assign'])
      end
    end

    describe '#for_manual_assign_with_swoop_account' do
      it 'return all candidates for a job in the manual_assign_with_swoop_account pool' do
        expected_count = Job::Candidate.where(pool_type: 'manual_assign_with_swoop_account', job_id: fleet_managed_job.id).count

        expect(Job::Candidate.for_manual_assign_with_swoop_account(fleet_managed_job.id).count).to eq(expected_count)
        expect(Job::Candidate.for_manual_assign_with_swoop_account(fleet_managed_job.id).pluck(:pool_type).uniq).to eq(['manual_assign_with_swoop_account'])
      end
    end

    describe '#for_manual_assign_oon_preferred' do
      it 'return all candidates for a job in the manual_assign_oon_preferred pool' do
        expected_count = Job::Candidate.where(pool_type: 'manual_assign_oon_preferred', job_id: fleet_managed_job.id).count

        expect(Job::Candidate.for_manual_assign_oon_preferred(fleet_managed_job.id).count).to eq(expected_count)
        expect(Job::Candidate.for_manual_assign_oon_preferred(fleet_managed_job.id).pluck(:pool_type).uniq).to eq(['manual_assign_oon_preferred'])
      end
    end

    describe '#for_manual_assign_without_oon_preferred_without_swoop_account' do
      it 'return all candidates for a job in the manual_assign_without_oon_preferred_without_swoop_account pool' do
        expected_count = Job::Candidate.where(pool_type: 'manual_assign_without_oon_preferred_without_swoop_account', job_id: fleet_managed_job.id).count

        expect(Job::Candidate.for_manual_assign_without_oon_preferred_without_swoop_account(fleet_managed_job.id).count).to eq(expected_count)
        expect(Job::Candidate.for_manual_assign_without_oon_preferred_without_swoop_account(fleet_managed_job.id).pluck(:pool_type).uniq).to eq(['manual_assign_without_oon_preferred_without_swoop_account'])
      end
    end
  end

  context 'validations' do
    describe 'pool_type' do
      it 'cannot be initialized with an invalid pool_type' do
        expect { Job::Candidate.new(pool_type: :invalid_value) }.to raise_error(ArgumentError, /is not a valid pool_type/)
      end

      it 'does not complain if the pool type is valid, but the candidate has other validation errors' do
        invalid_job_candidate = Job::Candidate.new(pool_type: :auto_assign)
        invalid_job_candidate.valid?
        expect(invalid_job_candidate.errors.size).to be > 1
        expect(invalid_job_candidate.errors[:pool_type]).not_to include('is not included in the list')
      end
    end
  end

  describe '#pool_type' do
    let(:job) { create(:rescue_job) }

    it 'defines a default pool type of `auto_assign`' do
      expect(Job::Candidate::DEFAULT_POOL_TYPE).to eql 'auto_assign'
    end

    context 'instance initialized with explicit `pool_type`' do
      let(:pool_type) { 'manual_assign_oon_preferred' }

      it 'returns assigned `pool_type` value' do
        new_job_candidate = Job::Candidate.new(pool_type: pool_type)
        expect(new_job_candidate.pool_type).to eq(pool_type)
      end

      it 'persists assigned `pool_type` value' do
        new_job_candidate = Job::Candidate.create!(pool_type: pool_type, job: job, company: job.rescue_company)
        expect(Job::Candidate.find(new_job_candidate.id).pool_type).to eq(pool_type)
      end

      it 'persists a change in the pool type' do
        new_job_candidate = Job::Candidate.create!(pool_type: pool_type, job: job, company: job.rescue_company)
        expect(Job::Candidate.find(new_job_candidate.id).pool_type).to eq(pool_type)
        expect(ApplicationRecord.execute_query(
          "SELECT pool_type FROM auto_assign_candidates WHERE id = ?",
          new_job_candidate.id
        ).values.first.first).to eq(pool_type)

        new_job_candidate.pool_type = 'manual_assign_oon_preferred'
        new_job_candidate.save!
        expect(Job::Candidate.find(new_job_candidate.id).pool_type).to eq('manual_assign_oon_preferred')
        expect(ApplicationRecord.execute_query(
          "SELECT pool_type FROM auto_assign_candidates WHERE id = ?",
          new_job_candidate.id
        ).values.first.first).to eq('manual_assign_oon_preferred')
      end
    end

    context 'instance initialized with default `auto_assign` pool type' do
      it 'defaults new instances to the `auto_assign` pool' do
        new_job_candidate = Job::Candidate.new
        expect(new_job_candidate.pool_type).to eq('auto_assign')
      end

      it 'persists default `pool_type` value' do
        new_job_candidate = Job::Candidate.create!(job: job, company: job.rescue_company)
        expect(Job::Candidate.find(new_job_candidate.id).pool_type).to eq('auto_assign')
      end

      it 'allows `null` value for `pool_type`' do
        ApplicationRecord.execute_query(
          "INSERT INTO auto_assign_candidates (job_id, company_id, type, pool_type) VALUES (?,?,?,?)",
          job.id,
          job.rescue_company.id,
          'Job::Candidate',
          nil
        )

        expect { Job::Candidate.last }.not_to raise_error
        expect { AutoAssign::AutoAssignCandidate.last }.not_to raise_error
        expect(ApplicationRecord.execute_query(
          "SELECT pool_type FROM auto_assign_candidates WHERE job_id = ? AND company_id = ? AND type = ?",
          job.id,
          job.rescue_company.id,
          'Job::Candidate'
        ).values.first.first).to be_nil
      end
    end

    context 'existing candidate records' do
      let!(:job_candidate) do
        ApplicationRecord.execute_query(
          "INSERT INTO auto_assign_candidates (job_id, company_id, type, pool_type) VALUES (?,?,?,?)",
          job.id,
          job.rescue_company.id,
          'Job::Candidate::Site',
          nil
        )
      end

      it 'returns default `pool_type` value' do
        job_candidate = Job::Candidate.last

        expect(job_candidate.attributes['pool_type']).to be_nil
        expect(job_candidate.pool_type).to eq('auto_assign')
        expect(job_candidate.job_id).to eq(job.id)
        expect(job_candidate.company_id).to eq(job.rescue_company_id)
      end
    end
  end

  describe '#find_sites_and_vehicles_to_be_candidate_for_auction' do
    # Step 1: We have a service, 'Accident Tow'
    let(:accident_tow_service_code) { ServiceCode.find_by(name: "Accident Tow") }

    # Step 2: Job with specific pickup/service location
    let(:fleet_managed_job) do
      create(:job, service_code: accident_tow_service_code, fleet_company: fleet_company, service_location: create(:location, :mission_safeway))
    end

    # Step 3.1: We have rescue company "E" with 1 territory and the 'Accident Tow' service code with Swoop account
    # This company has:
    # 3.1.1. San Francisco area as one of it's territories
    let(:rescue_company_e) { create :rescue_company, fleet_managed_clients: [fleet_company] }
    let!(:territory_out_of_san_francisco_for_rescue_company_e) { create(:territory, company: rescue_company_e, polygon_area: 'POLYGON ((40 40, 40 50, 60 50, 60 40, 40 40))') }
    let!(:site_for_company_e) { create(:partner_site, dispatchable: true, always_open: true, location: create(:location, lat: 36.5656837, lng: -122.5047682), company: rescue_company_e).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }

    # Step 3.2: We have rescue company "D" with 1 territory and the 'Accident Tow' service code with Swoop account
    # This company has:
    # 3.2.1. San Francisco area as one of it's territories
    # 3.2.2. One site and one truck
    let(:rescue_company_d) { create :rescue_company, fleet_managed_clients: [fleet_company], live: true }
    let!(:territory_covering_san_francisco_for_rescue_company_d) { create(:territory, :san_francisco, company: rescue_company_d) }
    let!(:swoop_account_for_rescue_company_d) { create(:account, company: rescue_company_d, client_company: Company.swoop) }
    let!(:accident_tow_service_code_for_rescue_company_d) do
      create(:companies_service, company: rescue_company_d, service_code: accident_tow_service_code)
    end
    let!(:site_for_company_d_rank_16) { create(:partner_site, name: "Site D16", dispatchable: true, always_open: true, location: create(:location, lat: 36.5656838, lng: -122.5047683), company: rescue_company_d).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:truck_for_company_d_rank_17) { create(:rescue_vehicle, name: "Truck D17", company: rescue_company_d, lat: 36.565872, lng: -122.517832, location_updated_at: Time.now) }

    # Step 3.3: We have rescue company "A" with 2 territories and the 'Accident Tow' service code with Swoop account
    # This company has:
    # 3.3.1. San Francisco area as one of it's territories
    # 3.3.2. A territory other than San Francisco area
    # 3.3.3. Four sites and Four trucks
    let(:rescue_company_a) { create :rescue_company, fleet_managed_clients: [fleet_company], live: true }
    let!(:territory_for_rescue_company_a) { create :territory, company: rescue_company_a, polygon_area: 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -10.5 -10.5))' }
    let!(:territory_covering_san_francisco_for_rescue_company_a) { create(:territory, :san_francisco, company: rescue_company_a) }
    let!(:swoop_account_for_rescue_company_a) { create(:account, company: rescue_company_a, client_company: Company.swoop) }
    let!(:accident_tow_service_code_for_rescue_company_a) do
      create(:companies_service, company: rescue_company_a, service_code: accident_tow_service_code)
    end
    # Creating sites with different locations for `rescue_company_a`
    # The suffix `rank_*` denotes the sequence of the sites and trucks objects when ordered by distance from job's service location
    let!(:site_for_company_a_rank_11) { create(:partner_site, name: "Site A11", dispatchable: true, always_open: true, location: create(:location, :tiburon_presbyterian), company: rescue_company_a).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:site_for_company_a_rank_10) { create(:partner_site, name: "Site A10", dispatchable: true, always_open: true, location: create(:location, :oakland_beer_garden), company: rescue_company_a).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:site_for_company_a_rank_13) { create(:partner_site, name: "Site A13", dispatchable: true, always_open: true, location: create(:location, :round_table_pizza), company: rescue_company_a).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:site_for_company_a_rank_9) { create(:partner_site, name: "Site A9", dispatchable: true, always_open: true, location: create(:location, :stonestown_mall), company: rescue_company_a).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:site_for_company_a_rank_0_but_closed) { create(:partner_site, :closed, dispatchable: true, location: create(:location, lat: fleet_managed_job.service_location.lat, lng: fleet_managed_job.service_location.lng), company: rescue_company_a).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    # Creating rescue_vehicles with different locations for `rescue_company_a`
    let!(:truck_for_company_a_rank_7) { create(:rescue_vehicle, name: "Truck A7", company: rescue_company_a, lat: 37.783840, lng: -122.415280, location_updated_at: Time.now) }
    let!(:truck_for_company_a_rank_14) { create(:rescue_vehicle, name: "Truck A14", company: rescue_company_a, lat: 36.961425, lng: -121.625885, location_updated_at: Time.now) }
    let!(:truck_for_company_a_rank_1) { create(:rescue_vehicle, name: "Truck A1", company: rescue_company_a, lat: 37.762225, lng: -122.4170020, location_updated_at: Time.now) }
    let!(:truck_for_company_a_rank_2) { create(:rescue_vehicle, name: "Truck A2", company: rescue_company_a, lat: 37.7651545, lng: -122.4235190, location_updated_at: Time.now) }
    let!(:truck_for_company_a_not_moving) { create(:rescue_vehicle, company: rescue_company_a, lat: 37.7758320, lng: -122.4235190, location_updated_at: 30.minutes.ago) }

    # Step 3.4: We have rescue company "B" with 2 territories and the 'Accident Tow' service code with Swoop account
    # This company has:
    # 3.4.1. San Francisco area as one of it's territories
    # 3.4.2. A territory other than San Francisco area
    # 3.4.3. Three sites and two trucks
    let(:rescue_company_b) { create :rescue_company, fleet_managed_clients: [fleet_company], live: true }
    let!(:territory_for_company_b) { create :territory, company: rescue_company_b, polygon_area: 'POLYGON ((40 40, 40 50, 50 50, 50 40, 40 40))' }
    let!(:territory_covering_san_francisco_for_rescue_company_b) { create(:territory, :san_francisco, company: rescue_company_b) }
    let!(:swoop_account_for_rescue_company_b) { create(:account, company: rescue_company_b, client_company: Company.swoop) }
    let!(:accident_tow_service_code_for_rescue_company_b) do
      create(:companies_service, company: rescue_company_b, service_code: accident_tow_service_code)
    end
    # Creating sites with different locations for `rescue_company_b`
    # The suffix `rank_*` denotes the sequence of the sites and trucks objects when ordered by distance from job's service location
    let!(:site_for_company_b_rank_6) { create(:partner_site, name: "Site B6", dispatchable: true, always_open: true, location: create(:location, :accurate_auto_body), company: rescue_company_b).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:site_for_company_b_rank_15) { create(:partner_site, name: "Site B15", dispatchable: true, always_open: true, location: create(:location, lat: 36.861425, lng: -121.625885), company: rescue_company_b).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:site_for_company_b_rank_8) { create(:partner_site, name: "Site B8", dispatchable: true, always_open: true, location: create(:location, :advance_auto_repair), company: rescue_company_b).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    # Creating rescue_vehicles with different locations for `rescue_company_b`
    let!(:truck_for_company_b_rank_3) { create(:rescue_vehicle, name: "Truck B3", company: rescue_company_b, lat: 37.7637310, lng: -122.4381810, location_updated_at: Time.now) }
    let!(:truck_for_company_b_rank_12) { create(:rescue_vehicle, name: "Truck B12", company: rescue_company_b, lat: 37.898390, lng: -122.502118, location_updated_at: Time.now) }

    # Step 3.5: We have rescue company "C" with 1 territory and the 'Accident Tow' service code with Swoop account
    # This company has:
    # 3.5.1. San Francisco area as one of it's territories
    # 3.5.2. One site and one truck
    let(:rescue_company_c) { create :rescue_company, fleet_managed_clients: [fleet_company], live: true }
    let!(:territory_covering_san_francisco_for_rescue_company_c) { create(:territory, :san_francisco, company: rescue_company_c) }
    let!(:swoop_account_for_rescue_company_c) { create(:account, company: rescue_company_c, client_company: Company.swoop) }
    let!(:accident_tow_service_code_for_rescue_company_c) do
      create(:companies_service, company: rescue_company_c, service_code: accident_tow_service_code)
    end
    # The suffix `rank_*` denotes the sequence of the sites and trucks objects when ordered by distance from job's service location
    let!(:site_for_company_c_rank_4) { create(:partner_site, name: "Site C4", dispatchable: true, always_open: true, location: create(:location, :domi_autobody), company: rescue_company_c).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:site_for_company_c_not_dispatchable) { create(:partner_site, dispatchable: false, always_open: true, location: create(:location, :advance_auto_repair), company: rescue_company_c).tap { |site| create(:rescue_provider, site: site, company: fleet_company) } }
    let!(:truck_for_company_c_rank_5) { create(:rescue_vehicle, name: "Truck C5", company: rescue_company_c, lat: 37.7737310, lng: -122.4000810, location_updated_at: Time.now) }

    context 'allowed max bidder is 1' do
      it 'return eligible candidates for max (1 x 3) rescue company within 24 minute eta' do
        ClimateControl.modify AUCTION_MAX_BIDDERS_DEFAULT: '1', AUCTION_MAX_CANDIDATE_ETA_DEFAULT: '24' do
          eligible_candidates_objects = Job::Candidate.find_sites_and_vehicles_to_be_candidate_for_auction(fleet_managed_job)

          expect(eligible_candidates_objects).to eq({
            sites: [
              site_for_company_c_rank_4,
            ],
            rescue_vehicles: [
              truck_for_company_a_rank_1,
              truck_for_company_a_rank_2,
              truck_for_company_b_rank_3,
            ],
          })

          rescue_companies_ids = eligible_candidates_objects.map { |_, candidates_arr| candidates_arr.map(&:company_id) }.flatten.uniq
          expect(rescue_companies_ids.count).to eq(3)
          expect(rescue_companies_ids).to contain_exactly(rescue_company_c.id, rescue_company_b.id, rescue_company_a.id)
          expect(rescue_companies_ids).not_to include(rescue_company_d.id)
        end
      end
    end

    context 'allowed max bidder is 2' do
      it 'returns eligible candidates for max (2 x 3) rescue company' do
        ClimateControl.modify AUCTION_MAX_BIDDERS_DEFAULT: '2', AUCTION_MAX_CANDIDATE_ETA_DEFAULT: '120' do
          eligible_candidates_objects = Job::Candidate.find_sites_and_vehicles_to_be_candidate_for_auction(fleet_managed_job)

          expect(eligible_candidates_objects).to eq({
            sites: [
              site_for_company_c_rank_4,
              site_for_company_b_rank_6,
              site_for_company_b_rank_8,
              site_for_company_a_rank_9,
              site_for_company_a_rank_10,
              site_for_company_a_rank_11,
              site_for_company_a_rank_13,
              site_for_company_b_rank_15,
              site_for_company_d_rank_16,
            ],
            rescue_vehicles: [
              truck_for_company_a_rank_1,
              truck_for_company_a_rank_2,
              truck_for_company_b_rank_3,
              truck_for_company_c_rank_5,
              truck_for_company_a_rank_7,
              truck_for_company_b_rank_12,
              truck_for_company_a_rank_14,
              truck_for_company_d_rank_17,
            ],
          })

          rescue_companies_ids = eligible_candidates_objects.map { |_, candidates_arr| candidates_arr.map(&:company_id) }.flatten.uniq
          expect(rescue_companies_ids.count).to eq(4)
          expect(rescue_companies_ids).to contain_exactly(rescue_company_c.id, rescue_company_b.id, rescue_company_a.id, rescue_company_d.id)
        end
      end
    end

    context 'default allowed max bidder' do
      it 'returns eligible candidates' do
        ClimateControl.modify AUCTION_MAX_BIDDERS_DEFAULT: nil, AUCTION_MAX_CANDIDATE_ETA_DEFAULT: '120' do
          eligible_candidates_objects = Job::Candidate.find_sites_and_vehicles_to_be_candidate_for_auction(fleet_managed_job)

          expect(eligible_candidates_objects).to eq({
            sites: [
              site_for_company_c_rank_4,
              site_for_company_b_rank_6,
              site_for_company_b_rank_8,
              site_for_company_a_rank_9,
              site_for_company_a_rank_10,
              site_for_company_a_rank_11,
              site_for_company_a_rank_13,
              site_for_company_b_rank_15,
              site_for_company_d_rank_16,
            ],
            rescue_vehicles: [
              truck_for_company_a_rank_1,
              truck_for_company_a_rank_2,
              truck_for_company_b_rank_3,
              truck_for_company_c_rank_5,
              truck_for_company_a_rank_7,
              truck_for_company_b_rank_12,
              truck_for_company_a_rank_14,
              truck_for_company_d_rank_17,
            ],
          })

          rescue_companies_ids = eligible_candidates_objects.map { |_, candidates_arr| candidates_arr.map(&:company_id) }.flatten.uniq
          expect(rescue_companies_ids.count).to eq(4)
          expect(rescue_companies_ids).to contain_exactly(rescue_company_c.id, rescue_company_b.id, rescue_company_a.id, rescue_company_d.id)
        end
      end
    end

    context "edge cases" do
      it "returns an empty response if the job has no service location" do
        fleet_managed_job.update!(service_location: nil)
        eligible_candidates_objects = Job::Candidate.find_sites_and_vehicles_to_be_candidate_for_auction(fleet_managed_job)
        expect(eligible_candidates_objects).to eq(sites: [], rescue_vehicles: [])
      end

      it "returns an empty response if the max bidders is not greater than zero" do
        ClimateControl.modify AUCTION_MAX_BIDDERS_DEFAULT: '0' do
          eligible_candidates_objects = Job::Candidate.find_sites_and_vehicles_to_be_candidate_for_auction(fleet_managed_job)
          expect(eligible_candidates_objects).to eq(sites: [], rescue_vehicles: [])
        end
      end

      it "returns an empty response if there are no eligible companies" do
        fleet_managed_job.service_location.update!(lat: 0.0, lng: 0.0)
        eligible_candidates_objects = Job::Candidate.find_sites_and_vehicles_to_be_candidate_for_auction(fleet_managed_job)
        expect(eligible_candidates_objects).to eq(sites: [], rescue_vehicles: [])
      end
    end
  end
end
