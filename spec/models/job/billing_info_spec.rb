# frozen_string_literal: true

require "rails_helper"

describe Job::BillingInfo do
  describe "validations" do
    let(:job) { Job.new }
    let(:rescue_company) { RescueCompany.new }
    let(:user) { User.new }
    let(:billing_info) { job.build_billing_info(billing_info_attributes) }
    let(:billing_info_attributes) do
      { rescue_company: rescue_company, agent: user, billing_type: BillingType::VCC }
    end

    it "allows ACH but not with a vcc_amount" do
      billing_info_attributes[:billing_type] = BillingType::ACH
      expect(billing_info).to be_valid
      billing_info.vcc_amount = 100
      expect(billing_info).to have(1).error_on(:vcc_amount)
    end

    it "allows VCC with an optional vcc_amount" do
      expect(billing_info).to be_valid
      billing_info.vcc_amount = 100
      expect(billing_info).to be_valid
    end

    it "requires an agent if the vcc amount is present" do
      billing_info_attributes[:agent] = nil
      billing_info_attributes[:vcc_amount] = 100
      expect(billing_info).to have(1).error_on(:agent)
      billing_info.agent = User.new
      expect(billing_info).to be_valid
    end

    it "requires a goa agent if the goa amount is present" do
      billing_info_attributes[:agent] = nil
      billing_info_attributes[:goa_agent] = nil
      billing_info_attributes[:goa_amount] = 20
      expect(billing_info).to have(1).error_on(:goa_agent)
      billing_info.goa_agent = User.new
      expect(billing_info).to be_valid
    end

    it 'requires currency to match the rescue company' do
      billing_info_attributes[:currency] = 'EUR'
      expect(billing_info).to have(1).error_on(:currency)
    end
  end
end
