# frozen_string_literal: true

require 'rails_helper'

describe Job::Candidate::Site do
  let(:job) { create(:rescue_job) }

  describe 'renaming AutoAssign::SiteCandidate to Job::Candidate::Site' do
    before(:each) do
      # Ensure there are no site candidates in the database
      number_of_candidates_at_start = Job::Candidate.count
      expect(number_of_candidates_at_start).to eql 0
    end

    it "AutoAssign::SiteCandidate is an alias of Job::Candidate::Site" do
      expect(AutoAssign::SiteCandidate).to eq(Job::Candidate::Site)
    end

    context 'reading' do
      describe 'using Job::Candidate::Site class' do
        it 'reads a Job::Candidate::Site record into a Job::Candidate::Site instance' do
          # Step 1: Save a Job::Candidate::Site to the database
          Job::Candidate::Site.create! job: job, company: job.rescue_company

          # Step 2: The site candidate loads into a Job::Candidate::Site instance, and
          # has the type 'Job::Candidate::Site'
          expect(Job::Candidate::Site.count).to eql 1
          expect(Job::Candidate::Site.first).to be_an_instance_of(Job::Candidate::Site)
          expect(Job::Candidate::Site.first.type).to eql('Job::Candidate::Site')
        end

        it 'reads an existing AutoAssign::SiteCandidate record into a Job::Candidate::Site instance' do
          # Step 1: Save a AutoAssign::SiteCandidate to the database
          ApplicationRecord.execute_query(
            "INSERT INTO auto_assign_candidates (type, job_id, company_id) VALUES (?,?,?)",
            'AutoAssign::SiteCandidate',
            job.id,
            job.rescue_company.id
          )

          # Step 2: The site candidate loads as a Job::Candidate::Site, and now
          # has the Job::Candidate::Site type column value
          expect(Job::Candidate::Site.count).to eql 1
          expect(Job::Candidate::Site.first).to be_an_instance_of(Job::Candidate::Site)
          expect(Job::Candidate::Site.first.type).to eql('Job::Candidate::Site')
        end
      end

      describe 'using AutoAssign::SiteCandidate class' do
        it 'reads an AutoAssign::SiteCandidate record using an AutoAssign::SiteCandidate class symbol' do
          # Step 1: Save a AutoAssign::SiteCandidate to the database
          ApplicationRecord.execute_query(
            "INSERT INTO auto_assign_candidates (type, job_id, company_id) VALUES (?,?,?)",
            'AutoAssign::SiteCandidate',
            job.id,
            job.rescue_company.id
          )

          # Step 2: The site candidate appears to be an AutoAssign::SiteCandidate...
          expect(AutoAssign::SiteCandidate.count).to eql 1
          expect(AutoAssign::SiteCandidate.first).to be_an_instance_of(AutoAssign::SiteCandidate)

          # Step 3: ...but it's actually a Job::Candidate::Site
          expect(AutoAssign::SiteCandidate.first).to be_an_instance_of(Job::Candidate::Site)
          expect(AutoAssign::SiteCandidate.first.type).to eql('Job::Candidate::Site')
        end

        it 'reads a Job::Candidate::Site record using a AutoAssign::SiteCandidate class symbol' do
          # Step 1: Save a Job::Candidate::Site to the database
          Job::Candidate::Site.create! job: job, company: job.rescue_company

          # Step 2: The site candidate loads as a Job::Candidate::Site, but still
          # has the AutoAssign::SiteCandidate type column value...
          expect(AutoAssign::SiteCandidate.count).to eql 1
          expect(AutoAssign::SiteCandidate.first).to be_an_instance_of(AutoAssign::SiteCandidate)
          expect(AutoAssign::SiteCandidate.first.type).to eql('Job::Candidate::Site')

          # Step 3: ...but it's actually a Job::Candidate::Site
          expect(AutoAssign::SiteCandidate.first).to be_an_instance_of(Job::Candidate::Site)
        end
      end
    end

    context 'writing' do
      describe 'using Job::Candidate::Site class' do
        it "writes a new Job::Candidate::Site instance with a 'Job::Candidate::Site' type" do
          # Step 1: Save a Job::Candidate::Site to the database
          Job::Candidate::Site.create! job: job, company: job.rescue_company

          # Step 2: The type is 'Job::Candidate::Site'
          expect(Job::Candidate::Site.count).to eql 1
          type_of_first_record = Job::Candidate.first.type
          expect(type_of_first_record).to eql('Job::Candidate::Site')
        end
      end

      describe 'using AutoAssign::SiteCandidate class' do
        it "writes a new AutoAssign::SiteCandidate instance with a 'Job::Candidate::Site' type" do
          # Step 1: Save a AutoAssign::SiteCandidate to the database
          AutoAssign::SiteCandidate.create! job: job, company: job.rescue_company

          # Step 2: The type is 'Job::Candidate::Site'
          expect(Job::Candidate::Site.count).to eql 1
          type_of_first_record = Job::Candidate.first.type
          expect(type_of_first_record).to eql('Job::Candidate::Site')
        end

        it 'writes an existing AutoAssign::SiteCandidate back as a Job::Candidate::Site' do
          # Step 1: Save a AutoAssign::SiteCandidate to the database
          ApplicationRecord.execute_query(
            "INSERT INTO auto_assign_candidates (type, job_id, company_id) VALUES (?,?,?)",
            'AutoAssign::SiteCandidate',
            job.id,
            job.rescue_company.id
          )

          # Step 2: Load the site candidate using the AutoAssign::SiteCandidate class symbol,
          # and change some stuff
          expect(AutoAssign::SiteCandidate.count).to eql 1
          site_candidate = AutoAssign::SiteCandidate.first
          expect(site_candidate.type).to eql("Job::Candidate::Site")
          site_candidate.lat = "-123.456"
          site_candidate.lat = "78.910"

          # Step 3: Save the site candidate
          expect { site_candidate.save! }.not_to raise_exception

          # Step 4: Re-read the site candidate, and check that the type is now Job::Candidate::Site
          expect(Job::Candidate::Site.count).to eql 1
          expect(AutoAssign::SiteCandidate.first.type).to eql("Job::Candidate::Site")
        end
      end
    end

    describe 'inheritance' do
      describe 'instances created as Job::Candidate::Site' do
        let(:site_candidate) { Job::Candidate::Site.create! job: job, company: job.rescue_company }

        it 'inherits from from Job::Candidate' do
          expect(site_candidate).to be_a_kind_of(Job::Candidate)
        end

        it 'inherits from AutoAssign::Candidate' do
          expect(site_candidate).to be_a_kind_of(AutoAssign::AutoAssignCandidate)
        end
      end

      describe 'instances created as AutoAssign::SiteCandidate' do
        let(:site_candidate) { AutoAssign::SiteCandidate.create! job: job, company: job.rescue_company }

        it 'inherits from from Job::Candidate' do
          expect(site_candidate).to be_a_kind_of(Job::Candidate)
        end

        it 'inherits from AutoAssign::AutoAssignCandidate' do
          expect(site_candidate).to be_a_kind_of(AutoAssign::AutoAssignCandidate)
        end
      end
    end
  end

  context 'scopes' do
    let!(:auto_assign_pool_site_candidates) { create_list(:site_candidate, 2, job: job, company: job.rescue_company, pool_type: 'auto_assign') }
    let!(:manual_assign_with_swoop_account_site_candidates) { create_list(:site_candidate, 3, job: job, company: job.rescue_company, pool_type: 'manual_assign_with_swoop_account') }
    let!(:manual_assign_oon_preferred_site_candidates) { create_list(:site_candidate, 4, job: job, company: job.rescue_company, pool_type: 'manual_assign_oon_preferred') }
    let!(:manual_assign_without_oon_preferred_without_swoop_account_site_candidates) { create_list(:site_candidate, 5, job: job, company: job.rescue_company, pool_type: 'manual_assign_without_oon_preferred_without_swoop_account') }

    describe '#for_auto_assign' do
      it 'return all site candidates for a job in the auto_assign pool' do
        expect(Job::Candidate::Site.for_auto_assign(job.id).count).to eq(2)
        expect(Job::Candidate::Site.for_auto_assign(job.id).pluck(:pool_type).uniq).to eq(['auto_assign'])
      end
    end

    describe '#for_manual_assign_with_swoop_account' do
      it 'return all site candidates for a job in the manual_assign_with_swoop_account pool' do
        expect(Job::Candidate::Site.for_manual_assign_with_swoop_account(job.id).count).to eq(3)
        expect(Job::Candidate::Site.for_manual_assign_with_swoop_account(job.id).pluck(:pool_type).uniq).to eq(['manual_assign_with_swoop_account'])
      end
    end

    describe '#for_manual_assign_oon_preferred' do
      it 'return all site candidates for a job in the manual_assign_oon_preferred pool' do
        expect(Job::Candidate::Site.for_manual_assign_oon_preferred(job.id).count).to eq(4)
        expect(Job::Candidate::Site.for_manual_assign_oon_preferred(job.id).pluck(:pool_type).uniq).to eq(['manual_assign_oon_preferred'])
      end
    end

    describe '#for_manual_assign_without_oon_preferred_without_swoop_account' do
      it 'return all site candidates for a job in the manual_assign_without_oon_preferred_without_swoop_account pool' do
        expect(Job::Candidate::Site.for_manual_assign_without_oon_preferred_without_swoop_account(job.id).count).to eq(5)
        expect(Job::Candidate::Site.for_manual_assign_without_oon_preferred_without_swoop_account(job.id).pluck(:pool_type).uniq).to eq(['manual_assign_without_oon_preferred_without_swoop_account'])
      end
    end
  end

  it 'allows `null` value for `pool_type`' do
    ApplicationRecord.execute_query(
      "INSERT INTO auto_assign_candidates (job_id, company_id, type, pool_type) VALUES (?,?,?,?)",
      job.id,
      job.rescue_company.id,
      'Job::Candidate::Site',
      nil
    )
    expect { Job::Candidate::Site.last }.not_to raise_error
    expect { AutoAssign::SiteCandidate.last }.not_to raise_error
    expect(Job::Candidate::Site.where(job_id: job.id, company_id: job.rescue_company.id).first[:pool_type]).to be_nil
  end

  describe '#create_all_for_job_auction_from_list_of_sites' do
    let(:job) { create(:job, :with_auction) }

    context 'supplied invalid (non-array) list of sites' do
      it 'raises the corresponding exception' do
        expect do
          Job::Candidate::Site.create_all_for_job_auction_from_list_of_sites(job: job, auction: job.auction, sites: 'InvalidListOfSites')
        end.to raise_error(Job::CannotGenerateBidsBecauseSuppliedListIsInvalidError)

        expect do
          Job::Candidate::Site.create_all_for_job_auction_from_list_of_sites(job: job, auction: job.auction, sites: nil)
        end.to raise_error(Job::CannotGenerateBidsBecauseSuppliedListIsInvalidError)
      end
    end

    context 'supplied blank list of sites' do
      it 'skips further execution and returns blank array' do
        expect(Job::Candidate::Site).not_to receive(:create!)
        expect(Job::Candidate::Site).not_to receive(:create_estimates_for_candidates!)

        candidates = Job::Candidate::Site.create_all_for_job_auction_from_list_of_sites(job: job, auction: job.auction, sites: [])
        expect(candidates).to eq([])
      end
    end

    context 'supplied non-blank list of sites' do
      let(:location) { create(:location, :random) }
      let(:site) { create(:site, location: location) }

      before do
        # Avoid making calls to Google API for distance and estimates calculation
        allow(CreateEstimatesForJobCandidatesService).to receive_message_chain(:new, :call)
      end

      it 'executes required methods and creates candidates' do
        expect(Job::Candidate::Site).to receive(:create!).and_call_original
        expect(Job::Candidate::Site).to receive(:create_estimates_for_candidates!).and_call_original

        candidates = Job::Candidate::Site.create_all_for_job_auction_from_list_of_sites(job: job, auction: job.auction, sites: [site])
        expect(candidates).to eq(job.candidates)
        expect(candidates.count).to eq(1)
        expect(candidates.first.class).to eq(Job::Candidate::Site)
      end
    end
  end
end
