# frozen_string_literal: true

require "rails_helper"

describe Job::Candidate::Collector do
  it "can add partner sites and vehicles" do
    collector = Job::Candidate::Collector.new(3, 10)
    site = PartnerSite.new(company_id: 1, name: "Site")
    vehicle = RescueVehicle.new(company_id: 1, name: "Vehicle")
    collector.add(site, 1.0)
    collector.add(vehicle, 2.0)
    expect(collector.eligible_entities).to eq [site, vehicle]
  end

  it "only adds candidates up maximum number of companies" do
    collector = Job::Candidate::Collector.new(3, 10)
    site_1 = PartnerSite.new(company_id: 1, name: "Site 1")
    collector.add(site_1, 1.0)
    expect(collector.eligible_entities).to eq [site_1]

    site_2 = PartnerSite.new(company_id: 2, name: "Site 2")
    collector.add(site_2, 4.0)
    expect(collector.eligible_entities).to eq [site_1, site_2]

    site_3 = PartnerSite.new(company_id: 3, name: "Site 3")
    collector.add(site_3, 3.0)
    expect(collector.eligible_entities).to eq [site_1, site_3, site_2]

    site_4 = PartnerSite.new(company_id: 4, name: "Site 4")
    collector.add(site_4, 2.0)
    expect(collector.eligible_entities).to eq [site_1, site_4, site_3]
  end

  it "collects multiple entities per company but only ones closer than the furthest bidder" do
    # Name on these records is set to include the company id and distance to help debuggin
    site_1_1 = PartnerSite.new(company_id: 1, name: "Site 1 (1.0 miles)")
    site_1_5 = PartnerSite.new(company_id: 1, name: "Site 1 (5.0 miles)")
    site_1_10 = PartnerSite.new(company_id: 1, name: "Site 1 (10.0 miles)")
    site_1_25 = PartnerSite.new(company_id: 1, name: "Site 1 (25.0 miles)")
    site_2_3 = PartnerSite.new(company_id: 2, name: "Site 2 (3.0 miles)")
    site_2_4 = PartnerSite.new(company_id: 2, name: "Site 2 (4.0 miles)")
    site_2_6 = PartnerSite.new(company_id: 2, name: "Site 2 (6.0 miles)")
    site_2_13 = PartnerSite.new(company_id: 2, name: "Site 2 (13.0 miles)")
    site_3_8 = PartnerSite.new(company_id: 3, name: "Site 3 (8.0 miles)")
    site_3_9 = PartnerSite.new(company_id: 3, name: "Site 3 (9.0 miles)")
    site_4_12 = PartnerSite.new(company_id: 4, name: "Site 4 (12.0 miles)")
    site_5_15 = PartnerSite.new(company_id: 5, name: "Site 5 (15.0 miles)")

    # The second elment in the array represents the distance in miles that will be used.
    data = [
      [site_1_1, 1.0],
      [site_1_5, 5.0],
      [site_1_10, 10.0],
      [site_1_25, 25.0],
      [site_2_3, 3.0],
      [site_2_4, 4.0],
      [site_2_6, 6.0],
      [site_2_13, 13.0],
      [site_3_8, 8.0],
      [site_3_9, 9.0],
      [site_4_12, 12.0],
      [site_5_15, 15.0],
    ]

    # Run multiple times in random order to ensure order of operations is not important
    20.times do
      collector = Job::Candidate::Collector.new(3, 20.0)
      data.shuffle.each do |site, distance|
        collector.add(site, distance)
      end
      expect(collector.eligible_entities.map(&:name)).to eq [
        site_1_1,
        site_2_3,
        site_2_4,
        site_1_5,
        site_2_6,
        site_3_8,
      ].map(&:name)
    end

    collector = Job::Candidate::Collector.new(4, 20.0)
    data.shuffle.each do |site, distance|
      collector.add(site, distance)
    end
    expect(collector.eligible_entities.map(&:name)).to eq [
      site_1_1,
      site_2_3,
      site_2_4,
      site_1_5,
      site_2_6,
      site_3_8,
      site_3_9,
      site_1_10,
      site_4_12,
    ].map(&:name)
  end

  it "does not collect entities farther out than the max distance" do
    collector = Job::Candidate::Collector.new(3, 9)
    site = PartnerSite.new(company_id: 1, name: "Site 1")

    collector.add(site, 10.0)
    expect(collector.eligible_entities).to eq []

    collector.add(site, 4.0)
    expect(collector.eligible_entities).to eq [site]
  end
end
