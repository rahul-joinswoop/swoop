# frozen_string_literal: true

require 'rails_helper'

describe Job::Candidate::Truck do
  let(:job) { create(:rescue_job) }

  describe 'renaming AutoAssign::TruckCandidate to Job::Candidate::Truck' do
    before(:each) do
      # Ensure there are no truck candidates in the database
      number_of_candidates_at_start = Job::Candidate.count
      expect(number_of_candidates_at_start).to eql 0
    end

    it "AutoAssign::TruckCandidate is an alias of Job::Candidate::Truck" do
      expect(AutoAssign::TruckCandidate).to eq(Job::Candidate::Truck)
    end

    context 'reading' do
      describe 'using Job::Candidate::Truck class' do
        it 'reads a Job::Candidate::Truck record into a Job::Candidate::Truck instance' do
          # Step 1: Save a Job::Candidate::Truck to the database
          Job::Candidate::Truck.create! job: job, company: job.rescue_company

          # Step 2: The truck candidate loads into a Job::Candidate::Truck instance, and
          # has the type 'Job::Candidate::Truck'
          expect(Job::Candidate::Truck.count).to eql 1
          expect(Job::Candidate::Truck.first).to be_an_instance_of(Job::Candidate::Truck)
          expect(Job::Candidate::Truck.first.type).to eql('Job::Candidate::Truck')
        end

        it 'reads an existing AutoAssign::TruckCandidate record into a Job::Candidate::Truck instance' do
          # Step 1: Save a AutoAssign::TruckCandidate to the database
          ApplicationRecord.execute_query(
            "INSERT INTO auto_assign_candidates (type, job_id, company_id) VALUES (?,?,?)",
            'AutoAssign::TruckCandidate',
            job.id,
            job.rescue_company.id
          )

          # Step 2: The truck candidate loads as a Job::Candidate::Truck, and now
          # has the Job::Candidate::Truck type column value
          expect(Job::Candidate::Truck.count).to eql 1
          expect(Job::Candidate::Truck.first).to be_an_instance_of(Job::Candidate::Truck)
          expect(Job::Candidate::Truck.first.type).to eql('Job::Candidate::Truck')
        end
      end

      describe 'using AutoAssign::TruckCandidate class' do
        it 'reads an AutoAssign::TruckCandidate record using an AutoAssign::TruckCandidate class symbol' do
          # Step 1: Save a AutoAssign::TruckCandidate to the database
          ApplicationRecord.execute_query(
            "INSERT INTO auto_assign_candidates (type, job_id, company_id) VALUES (?,?,?)",
            'AutoAssign::TruckCandidate',
            job.id,
            job.rescue_company.id
          )

          # Step 2: The truck candidate appears to be an AutoAssign::TruckCandidate...
          expect(AutoAssign::TruckCandidate.count).to eql 1
          expect(AutoAssign::TruckCandidate.first).to be_an_instance_of(AutoAssign::TruckCandidate)

          # Step 3: ...but it's actually a Job::Candidate::Truck
          expect(AutoAssign::TruckCandidate.first).to be_an_instance_of(Job::Candidate::Truck)
          expect(AutoAssign::TruckCandidate.first.type).to eql('Job::Candidate::Truck')
        end

        it 'reads a Job::Candidate::Truck record using a AutoAssign::TruckCandidate class symbol' do
          # Step 1: Save a Job::Candidate::Truck to the database
          Job::Candidate::Truck.create! job: job, company: job.rescue_company

          # Step 2: The truck candidate loads as a Job::Candidate::Truck, but still
          # has the AutoAssign::TruckCandidate type column value...
          expect(AutoAssign::TruckCandidate.count).to eql 1
          expect(AutoAssign::TruckCandidate.first).to be_an_instance_of(AutoAssign::TruckCandidate)
          expect(AutoAssign::TruckCandidate.first.type).to eql('Job::Candidate::Truck')

          # Step 3: ...but it's actually a Job::Candidate::Truck
          expect(AutoAssign::TruckCandidate.first).to be_an_instance_of(Job::Candidate::Truck)
        end
      end
    end

    context 'writing' do
      describe 'using Job::Candidate::Truck class' do
        it "writes a new Job::Candidate::Truck instance with a 'Job::Candidate::Truck' type" do
          # Step 1: Save a Job::Candidate::Truck to the database
          Job::Candidate::Truck.create! job: job, company: job.rescue_company

          # Step 2: The type is 'Job::Candidate::Truck'
          expect(Job::Candidate::Truck.count).to eql 1
          type_of_first_record = Job::Candidate.first.type
          expect(type_of_first_record).to eql('Job::Candidate::Truck')
        end
      end

      describe 'using AutoAssign::TruckCandidate class' do
        it "writes a new AutoAssign::TruckCandidate instance with a 'Job::Candidate::Truck' type" do
          # Step 1: Save a AutoAssign::TruckCandidate to the database
          AutoAssign::TruckCandidate.create! job: job, company: job.rescue_company

          # Step 2: The type is 'Job::Candidate::Truck'
          expect(Job::Candidate::Truck.count).to eql 1
          type_of_first_record = Job::Candidate.first.type
          expect(type_of_first_record).to eql('Job::Candidate::Truck')
        end

        it 'writes an existing AutoAssign::TruckCandidate back as a Job::Candidate::Truck' do
          # Step 1: Save a AutoAssign::TruckCandidate to the database
          ApplicationRecord.execute_query(
            "INSERT INTO auto_assign_candidates (type, job_id, company_id) VALUES (?,?,?)",
            'AutoAssign::TruckCandidate',
            job.id,
            job.rescue_company.id
          )

          # Step 2: Load the truck candidate using the AutoAssign::TruckCandidate class symbol,
          # and change some stuff
          expect(AutoAssign::TruckCandidate.count).to eql 1
          truck_candidate = AutoAssign::TruckCandidate.first
          expect(truck_candidate.type).to eql("Job::Candidate::Truck")
          truck_candidate.lat = "-123.456"
          truck_candidate.lat = "78.910"

          # Step 3: Save the truck candidate
          expect { truck_candidate.save! }.not_to raise_exception

          # Step 4: Re-read the truck candidate, and check that the type is now Job::Candidate::Truck
          expect(Job::Candidate::Truck.count).to eql 1
          expect(AutoAssign::TruckCandidate.first.type).to eql("Job::Candidate::Truck")
        end
      end
    end

    describe 'inheritance' do
      describe 'instances created as Job::Candidate::Truck' do
        let(:truck_candidate) { Job::Candidate::Truck.create! job: job, company: job.rescue_company }

        it 'inherits from from Job::Candidate' do
          expect(truck_candidate).to be_a_kind_of(Job::Candidate)
        end

        it 'inherits from AutoAssign::Candidate' do
          expect(truck_candidate).to be_a_kind_of(AutoAssign::AutoAssignCandidate)
        end
      end

      describe 'instances created as AutoAssign::TruckCandidate' do
        let(:truck_candidate) { AutoAssign::TruckCandidate.create! job: job, company: job.rescue_company }

        it 'inherits from from Job::Candidate' do
          expect(truck_candidate).to be_a_kind_of(Job::Candidate)
        end

        it 'inherits from AutoAssign::AutoAssignCandidate' do
          expect(truck_candidate).to be_a_kind_of(AutoAssign::AutoAssignCandidate)
        end
      end
    end
  end

  context "time and distance estimates" do
    # We have a tow company called Durand Durand Towing
    subject { Job::Candidate::Truck.build_from_job_vehicle(job, auction, rescue_vehicle) }

    let(:rescue_company) { create(:rescue_company, :durand_durand_towing) }

    # There's a tow truck for Durand Durand Towing, currently
    # located at Tiburon Presbyterian
    let(:truck_starting_location) { create(:location, :tiburon_presbyterian) }
    let!(:rescue_vehicle) do
      create(:rescue_vehicle, :with_driver, {
        company: rescue_company,
        lat: truck_starting_location.lat,
        lng: truck_starting_location.lng,
        location_updated_at: Time.now,
      })
    end

    # There's a fleet-managed company
    let(:fleet_company) { create(:fleet_managed_company) }

    # There's an auction job in auto-assigning status where the service
    # location is Stonestown Mall and the dropoff is not set
    let!(:job) do
      create(:fleet_managed_job, {
        fleet_company: fleet_company,
        status: Job::AUTO_ASSIGNING,
        service_location: create(:location, :stonestown_mall),
        drop_location: nil, # There is no drop location
        rescue_company: nil, # When a job is in auto assigning status, this should be nil because the winner hasn't be assigned yet
      })
    end

    # There's an auction for the job
    let!(:auction) { create(:auction, job: job) }

    # Build the truck candidate from the rescue vehicle for this job's auction

    # ENG-8435: this estimate is correct for determing time, but incorrect for
    # determining cost (invoicing) because it implies the truck's lat/lng as the
    # 'A' waypoint
    it 'creates a time and distance estimate for the truck candidate', vcr: true do
      # A is Tiburon Presbyterian, B is Stonestown Mall
      expect(subject.google_eta).to eql 29 # 32 minutes to service location
      expect(subject.google_miles).to eql 14 # 14.9 miles to service location
      expect(subject.estimate).to be_a Estimate
      expect(subject.estimate.meters_ab).to eql 24013 # 14.9 miles to service location
      expect(subject.estimate.seconds_ab).to eql 1787 # 32 minutes to service location
      expect(subject.estimate.meters_ba).to eql 23606 # 14.7 miles back to starting location
      expect(subject.estimate.seconds_ba).to eql 2111 # 30 minutes back to starting location
      expect(subject.estimate.meters_bc).to be_nil # There's no C waypoint
      expect(subject.estimate.seconds_bc).to be_nil # There's no C waypoint
      expect(subject.estimate.meters_ca).to be_nil # There's no C waypoint
      expect(subject.estimate.seconds_ca).to be_nil # There's no C waypoint
    end

    it "assigns the company's site as the candidate's site", vcr: true do
      expect(subject.site).to eql(rescue_company.hq)
    end
  end

  context 'scopes' do
    let!(:auto_assign_pool_truck_candidates) { create_list(:truck_candidate, 2, job: job, company: job.rescue_company, pool_type: 'auto_assign') }
    let!(:manual_assign_with_swoop_account_truck_candidates) { create_list(:truck_candidate, 3, job: job, company: job.rescue_company, pool_type: 'manual_assign_with_swoop_account') }
    let!(:manual_assign_oon_preferred_truck_candidates) { create_list(:truck_candidate, 4, job: job, company: job.rescue_company, pool_type: 'manual_assign_oon_preferred') }
    let!(:manual_assign_without_oon_preferred_without_swoop_account_truck_candidates) { create_list(:truck_candidate, 5, job: job, company: job.rescue_company, pool_type: 'manual_assign_without_oon_preferred_without_swoop_account') }

    describe '#for_auto_assign' do
      it 'return all truck candidates for a job in the auto_assign pool' do
        expect(Job::Candidate::Truck.for_auto_assign(job.id).count).to eq(2)
        expect(Job::Candidate::Truck.for_auto_assign(job.id).pluck(:pool_type).uniq).to eq(['auto_assign'])
      end
    end

    describe '#for_manual_assign_with_swoop_account' do
      it 'return all truck candidates for a job in the manual_assign_with_swoop_account pool' do
        expect(Job::Candidate::Truck.for_manual_assign_with_swoop_account(job.id).count).to eq(3)
        expect(Job::Candidate::Truck.for_manual_assign_with_swoop_account(job.id).pluck(:pool_type).uniq).to eq(['manual_assign_with_swoop_account'])
      end
    end

    describe '#for_manual_assign_oon_preferred' do
      it 'return all truck candidates for a job in the manual_assign_oon_preferred pool' do
        expect(Job::Candidate::Truck.for_manual_assign_oon_preferred(job.id).count).to eq(4)
        expect(Job::Candidate::Truck.for_manual_assign_oon_preferred(job.id).pluck(:pool_type).uniq).to eq(['manual_assign_oon_preferred'])
      end
    end

    describe '#for_manual_assign_without_oon_preferred_without_swoop_account' do
      it 'return all truck candidates for a job in the manual_assign_without_oon_preferred_without_swoop_account pool' do
        expect(Job::Candidate::Truck.for_manual_assign_without_oon_preferred_without_swoop_account(job.id).count).to eq(5)
        expect(Job::Candidate::Truck.for_manual_assign_without_oon_preferred_without_swoop_account(job.id).pluck(:pool_type).uniq).to eq(['manual_assign_without_oon_preferred_without_swoop_account'])
      end
    end
  end

  it 'allows `null` value for `pool_type`' do
    ApplicationRecord.execute_query(
      "INSERT INTO auto_assign_candidates (job_id, company_id, type, pool_type) VALUES (?,?,?,?)",
      job.id,
      job.rescue_company.id,
      'Job::Candidate::Truck',
      nil
    )

    expect { Job::Candidate::Truck.last }.not_to raise_error
    expect { AutoAssign::TruckCandidate.last }.not_to raise_error
    expect(Job::Candidate::Truck.where(job_id: job.id, company_id: job.rescue_company.id).first[:pool_type]).to be_nil
  end

  describe '#create_all_for_job_auction_from_list_of_rescue_vehicles' do
    let(:job) { create(:job, :with_auction) }

    context 'supplied invalid (non-array) list of trucks' do
      it 'raises the corresponding exception' do
        expect do
          Job::Candidate::Truck.create_all_for_job_auction_from_list_of_rescue_vehicles(job: job, auction: job.auction, vehicles: 'InvalidListOfVehicles')
        end.to raise_error(Job::CannotGenerateBidsBecauseSuppliedListIsInvalidError)

        expect do
          Job::Candidate::Truck.create_all_for_job_auction_from_list_of_rescue_vehicles(job: job, auction: job.auction, vehicles: nil)
        end.to raise_error(Job::CannotGenerateBidsBecauseSuppliedListIsInvalidError)
      end
    end

    context 'supplied blank list of trucks' do
      it 'skips further execution and returns blank array' do
        expect(Job::Candidate::Truck).not_to receive(:build_from_job_vehicle)
        expect(Job::Candidate::Truck).not_to receive(:new)

        candidates = Job::Candidate::Truck.create_all_for_job_auction_from_list_of_rescue_vehicles(job: job, auction: job.auction, vehicles: [])
        expect(candidates).to eq([])
      end
    end

    context 'supplied non-blank list of trucks' do
      let(:truck) { create(:rescue_vehicle) }

      before do
        # Avoid making calls to Google API for distance and estimates calculation
        allow(CreateEstimatesForJobCandidatesService).to receive_message_chain(:new, :call) do
          allow_any_instance_of(Job::Candidate::Truck).to receive(:google_eta).and_return(10)
        end
      end

      it 'executes required methods and create candidates' do
        expect(Job::Candidate::Truck).to receive(:build_from_job_vehicle).and_call_original
        expect(Job::Candidate::Truck).to receive(:new).and_call_original

        candidates = Job::Candidate::Truck.create_all_for_job_auction_from_list_of_rescue_vehicles(job: job, auction: job.auction, vehicles: [truck])
        expect(candidates).to eq(job.candidates)
        expect(candidates.count).to eq(1)
        expect(candidates.first.class).to eq(Job::Candidate::Truck)
      end
    end
  end
end
