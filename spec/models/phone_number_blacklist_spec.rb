# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PhoneNumberBlacklist, type: :model do
  describe "#phone_number" do
    let(:phone_number) { Faker::PhoneNumber.phone_number }

    it "works with a valid phone number" do
      expect { subject.update! phone_number: phone_number }.not_to raise_error
    end

    it "fails without a phone_number" do
      expect { subject.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end

    it "fails with an invalid phone_number" do
      expect do
        subject.update!(phone_number: "asdfasdf")
      end.to raise_error(ActiveRecord::RecordInvalid)
    end

    it "formats phone_number as e164" do
      subject.update!(phone_number: phone_number)
      expect(subject.phone_number).to eq(Phonelib.parse(phone_number).e164)
    end
  end

  describe "#blacklisted?" do
    let(:bad_phone_number) { create(:phone_number_blacklist).phone_number }
    let(:good_phone_number) { Faker::PhoneNumber.phone_number }

    it "works with a blacklisted phone_number" do
      expect(PhoneNumberBlacklist.blacklisted?(bad_phone_number)).to be_truthy
    end

    it "works with a non-blacklisted phone_number" do
      expect(PhoneNumberBlacklist.blacklisted?(good_phone_number)).to be_falsey
    end
  end
end
