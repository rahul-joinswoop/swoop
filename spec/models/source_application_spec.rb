# frozen_string_literal: true

require "rails_helper"

describe SourceApplication do
  describe "instance" do
    let(:oauth_application) { create(:oauth_application, name: "Partner App") }
    let!(:web_application) { SourceApplication.create!(platform: SourceApplication::PLATFORM_WEB, source: "Swoop") }
    let!(:ios_application) { SourceApplication.create!(platform: SourceApplication::PLATFORM_IOS, source: "Swoop", version: "1.0.0") }
    let!(:partner_application) { SourceApplication.create!(platform: SourceApplication::PLATFORM_PARTNER_API, oauth_application_id: oauth_application.id) }

    context "without version" do
      it "returns an existing record" do
        application = SourceApplication.instance(platform: SourceApplication::PLATFORM_WEB, source: "Swoop")
        expect(application).to eq web_application
      end

      it "creates a new record if necessary" do
        application = SourceApplication.instance(platform: SourceApplication::PLATFORM_WEB, source: "Other")
        expect(application).not_to eq web_application
        expect(application.platform).to eq SourceApplication::PLATFORM_WEB
        expect(application.source).to eq "Other"
        expect(application.version).to eq nil
        expect(application.oauth_application_id).to eq nil
      end
    end

    context "with version" do
      it "returns an existing record" do
        application = SourceApplication.instance(platform: SourceApplication::PLATFORM_IOS, source: "Swoop", version: "1.0.0")
        expect(application).to eq ios_application
      end

      it "creates a new record if necessary" do
        application = SourceApplication.instance(platform: SourceApplication::PLATFORM_IOS, source: "Swoop", version: "1.0.1")
        expect(application).not_to eq ios_application
        expect(application.platform).to eq SourceApplication::PLATFORM_IOS
        expect(application.source).to eq "Swoop"
        expect(application.version).to eq "1.0.1"
        expect(application.oauth_application_id).to eq nil
      end
    end

    context "with oauth application" do
      it "returns an existing record" do
        application = SourceApplication.instance(platform: SourceApplication::PLATFORM_PARTNER_API, oauth_application_id: oauth_application.id)
        expect(application).to eq partner_application
      end

      it "creates a new record if necessary" do
        other_oauth_application = create(:oauth_application, name: "Another App")
        application = SourceApplication.instance(platform: SourceApplication::PLATFORM_PARTNER_API, oauth_application_id: other_oauth_application.id)
        expect(application).not_to eq partner_application
        expect(application.platform).to eq SourceApplication::PLATFORM_PARTNER_API
        expect(application.source).to eq "Another App"
        expect(application.version).to eq nil
        expect(application.oauth_application_id).to eq other_oauth_application.id
      end
    end
  end

  describe "from_authenticated_request" do
    let(:request) { ActionDispatch::TestRequest.create }
    let(:access_token) { Doorkeeper::AccessToken.create! }

    it "returns nil if not authenticated" do
      expect(SourceApplication.from_request(request)).to eq nil
    end

    it "identifies a partner API request" do
      company = create(:rescue_company)
      oauth_application = create(:oauth_application, name: "Partner App", owner: company, scopes: ["rescue"])
      access_token.update!(application_id: oauth_application.id)
      request.params[:access_token] = access_token.token
      application = SourceApplication.from_request(request)
      expect(application.platform).to eq SourceApplication::PLATFORM_PARTNER_API
      expect(application.source).to eq "Partner App"
      expect(application.version).to eq nil
      expect(application.oauth_application_id).to eq oauth_application.id
    end

    it "identifies a client API request" do
      company = create(:fleet_company)
      oauth_application = create(:oauth_application, name: "Client App", owner: company, scopes: ["fleet"])
      access_token.update!(application_id: oauth_application.id)
      request.params[:access_token] = access_token.token
      application = SourceApplication.from_request(request)
      expect(application.platform).to eq SourceApplication::PLATFORM_CLIENT_API
      expect(application.source).to eq "Client App"
      expect(application.version).to eq nil
      expect(application.oauth_application_id).to eq oauth_application.id
    end

    it "identifies an iOS app request" do
      access_token.update!(resource_owner_id: create(:user))
      request.params[:access_token] = access_token.token
      request.env["HTTP_USER_AGENT"] = "ios Swoop/1.0.0 (abcd)"
      application = SourceApplication.from_request(request)
      expect(application.platform).to eq SourceApplication::PLATFORM_IOS
      expect(application.source).to eq "Swoop"
      expect(application.version).to eq "1.0.0"
      expect(application.oauth_application_id).to eq nil
    end

    it "identifies an Android request" do
      access_token.update!(resource_owner_id: create(:user))
      request.params[:access_token] = access_token.token
      request.env["HTTP_USER_AGENT"] = "android Swoop/1.0.0 (abcd)"
      application = SourceApplication.from_request(request)
      expect(application.platform).to eq SourceApplication::PLATFORM_ANDROID
      expect(application.source).to eq "Swoop"
      expect(application.version).to eq "1.0.0"
      expect(application.oauth_application_id).to eq nil
    end

    it "identifies a Swoop web request" do
      access_token.update!(resource_owner_id: create(:user))
      request.params[:access_token] = access_token.token
      application = SourceApplication.from_request(request)
      expect(application.platform).to eq SourceApplication::PLATFORM_WEB
      expect(application.source).to eq "Swoop"
      expect(application.version).to eq nil
      expect(application.oauth_application_id).to eq nil
    end
  end

  describe "sms_application" do
    it "returns the application to use for SMS interactions" do
      application = SourceApplication.sms_application
      expect(application.platform).to eq SourceApplication::PLATFORM_SMS
      expect(application.source).to eq "Swoop"
      expect(application.version).to eq nil
      expect(application.oauth_application_id).to eq nil
    end
  end

  describe "consumer_mobile_web" do
    it "returns the application to use for customer web interactions" do
      application = SourceApplication.consumer_mobile_web
      expect(application.platform).to eq SourceApplication::PLATFORM_CONSUMER_MOBILE_WEB
      expect(application.source).to eq "Swoop"
      expect(application.version).to eq nil
      expect(application.oauth_application_id).to eq nil
    end
  end

  describe "issc_application" do
    it "returns the application to use for ISSC interactions" do
      application = SourceApplication.issc_application
      expect(application.platform).to eq SourceApplication::PLATFORM_ISSC_API
      expect(application.source).to eq "ISSC"
      expect(application.version).to eq nil
      expect(application.oauth_application_id).to eq nil
    end
  end

  describe "rsc_application" do
    it "returns the application to use for RSC interactions" do
      application = SourceApplication.rsc_application
      expect(application.platform).to eq SourceApplication::PLATFORM_RSC_API
      expect(application.source).to eq "Agero"
      expect(application.version).to eq nil
      expect(application.oauth_application_id).to eq nil
    end
  end

  describe "helpers" do
    it "identifies an application as a third party using the Swoop API" do
      application = SourceApplication.new
      expect(application.api?).to eq false
      application.platform = SourceApplication::PLATFORM_IOS
      expect(application.api?).to eq false
      application.platform = SourceApplication::PLATFORM_CLIENT_API
      expect(application.api?).to eq true
      application.platform = SourceApplication::PLATFORM_PARTNER_API
      expect(application.api?).to eq true
    end

    it "identfies the Swoop mobile applications" do
      application = SourceApplication.new
      expect(application.mobile_app?).to eq false
      application.platform = SourceApplication::PLATFORM_WEB
      expect(application.mobile_app?).to eq false
      application.platform = SourceApplication::PLATFORM_IOS
      expect(application.mobile_app?).to eq true
      application.platform = SourceApplication::PLATFORM_ANDROID
      expect(application.mobile_app?).to eq true
    end
  end
end
