# frozen_string_literal: true

require "rails_helper"

describe Payment do
  let(:invoice) { build_stubbed(:invoice, currency: 'EUR') }

  describe "#create" do
    it 'requires currency be the same as invoice' do
      expect { create(:payment, currency: 'USD', invoice: invoice) }
        .to raise_error(ActiveRecord::RecordInvalid, /Currency must match Invoice/)
    end
  end
end
