# frozen_string_literal: true

require "rails_helper"

describe Question do
  describe ".defaults_per_service_code" do
    subject(:question_defaults) do
      Question.defaults_per_service_code(service_code)
    end

    let(:service_code) do
      build_stubbed :service_code, name: ServiceCode::TRANSPORT
    end

    before do
      create :question, question: Question::KEYS_PRESENT
      create :question, question: Question::LOW_CLEARANCE
    end

    it "returns an array of question instances" do
      all_questions =
        question_defaults.all? { |question| question.is_a?(Question) }
      expect(all_questions).to be(true)
    end

    it "returns question defaults based on service_code name" do
      questions = question_defaults.map(&:question)
      expect(questions.length).to be(2)
      expect(questions).to include(Question::KEYS_PRESENT, Question::LOW_CLEARANCE)
    end

    it "returns nil when there are no defaults for a service_code" do
      service_code = build_stubbed :service_code, name: "No defaults for meeee"
      expect(Question.defaults_per_service_code(service_code)).to be_nil
    end
  end

  describe '#fetch_answer' do
    let(:question) { create :question_with_answers }

    context "with an answer" do
      let(:answer) { question.answers.first }

      it "works" do
        expect(question.fetch_answer(answer.answer)).to eq(answer)
      end
    end

    context "without an answer" do
      let(:answer) { Faker::Hipster.sentence }

      it "works" do
        expect(question.fetch_answer(answer)).to be_nil
      end
    end
  end

  describe '#fetch_answer!' do
    let(:question) { create :question_with_answers }

    context "with an answer" do
      let(:answer) { question.answers.first }

      it "works" do
        expect(question.fetch_answer!(answer.answer)).to eq(answer)
      end
    end

    context "without an answer" do
      let(:answer) { Faker::Hipster.sentence }

      it "works" do
        expect { question.fetch_answer!(answer) }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
