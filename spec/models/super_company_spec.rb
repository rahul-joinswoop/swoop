# frozen_string_literal: true

require "rails_helper"

describe SuperCompany do
  let(:super_company) { create(:super_company) }

  it "creates super_company" do
    expect(super_company).to be_valid
  end
end
