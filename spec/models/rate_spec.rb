# frozen_string_literal: true

require "rails_helper"

describe Rate, type: :model do
  describe '#calculate_line_items' do
    subject { rate.calculate_line_items(invoicable) }

    let(:invoicable) { create(:job) }

    describe 'implemented in concrete subclasses only' do
      context 'abstract parent' do
        let(:rate) { Rate.new }

        it 'raises an exception' do
          expect { subject }.to raise_error(UncallableAbstractParentMethodError)
        end
      end

      context 'concrete sublcasses' do
        context 'FlatRate' do
          let(:rate) { FlatRate.new }

          it 'does not raise an exception' do
            expect { subject }.not_to raise_error(UncallableAbstractParentMethodError)
          end
        end

        context 'HoursP2PRate' do
          let(:rate) { HoursP2PRate.new }

          it 'does not raise an exception' do
            expect { subject }.not_to raise_error(UncallableAbstractParentMethodError)
          end
        end

        context 'MilesEnRouteOnlyRate' do
          let(:rate) { MilesEnRouteOnlyRate.new }

          it 'does not raise an exception' do
            expect { subject }.not_to raise_error(UncallableAbstractParentMethodError)
          end
        end

        context 'MilesEnRouteRate' do
          let(:rate) { MilesEnRouteRate.new }

          it 'does not raise an exception' do
            expect { subject }.not_to raise_error(UncallableAbstractParentMethodError)
          end
        end

        context 'MilesP2PRate' do
          let(:rate) { MilesP2PRate.new }

          it 'does not raise an exception' do
            expect { subject }.not_to raise_error(UncallableAbstractParentMethodError)
          end
        end

        context 'MilesTowedRate' do
          let(:rate) { MilesTowedRate.new }

          it 'does not raise an exception' do
            expect { subject }.not_to raise_error(UncallableAbstractParentMethodError)
          end
        end

        context 'StorageRate' do
          let(:rate) { StorageRate.new }

          it 'does not raise an exception' do
            expect { subject }.not_to raise_error(UncallableAbstractParentMethodError)
          end
        end

        context 'TeslaRate' do
          let(:company) { Company.new(distance_unit: 'km') }
          let(:job) { Job.new }
          let(:rate) { TeslaRate.new(hourly: 31.33, miles_p2p: 0.50, company: company) }

          before do
            job.build_estimate(
              meters_ab: 12532, meters_bc: 8424, meters_ca: 11234, drop_location: Location.new
            )
          end

          it 'does not raise an exception' do
            expect { subject }.not_to raise_error(UncallableAbstractParentMethodError)
          end

          describe '#calculate_line_items' do
            it 'works' do
              expect { rate.calculate_line_items(job) }.not_to raise_error
            end
          end
        end
      end
    end
  end

  context "Miles p2p" do
    context "Round up" do
      before do
        @price = BigDecimal('1.75')
        rate = Rate.new(miles_p2p: @price)

        @miles_float = 12.345
        @li = rate._base_p2p_with_mileage(@miles_float, @price, 'description').first
      end

      it "rounds miles to 1 dp and puts in quantity" do
        quantity = BigDecimal('12.3')
        expect(@li[:quantity]).to eql(quantity)
        expect(@li[:original_quantity]).to eql(quantity)
      end

      it "copies price" do
        expect(@li[:unit_price]).to eql(@price)
        expect(@li[:original_unit_price]).to eql(@price)
      end

      it "rounds net amount up" do
        miles_1_dp = @miles_float.to_d.round(1)
        Rails.logger.debug "expected miles #{miles_1_dp} #{miles_1_dp.class}"

        # (@price * miles_1_dp).round(2)
        # $1.75 * 12.3 = $21.525
        expected = BigDecimal('21.53')

        Rails.logger.debug "expected net_amount #{expected} #{expected.class}"
        expect(@li[:net_amount]).to eql(expected)
      end
    end

    context "rounds net amount down" do
      before do
        @price = BigDecimal('1.77')
        rate = Rate.new(miles_p2p: @price)

        @miles_float = 1.2
        @li = rate._base_p2p_with_mileage(@miles_float, @price, 'description').first
      end

      it "rounds net amount down" do
        miles_1_dp = @miles_float.to_d.round(1)
        Rails.logger.debug "expected miles #{miles_1_dp} #{miles_1_dp.class}"

        # (@price * miles_1_dp).round(2)
        # $1.77 * 1.2 = $2.124
        expected = BigDecimal('2.12')

        Rails.logger.debug "expected net_amount #{expected} #{expected.class}"
        expect(@li[:net_amount]).to eql(expected)
      end
    end
  end

  context "Hours P2P" do
    context "round up" do
      before do
        @hourly_rate_dollars = 125
        rate = Rate.new(hourly: @hourly_rate_dollars)
        @mins = 56
        @li = rate._base_hours_with_mins(@mins)
      end

      it "quantity is rounded down to 2 dp" do
        # 56/60 = 0.9333333...
        expect(@li[:quantity]).to eql(BigDecimal('0.93'))
      end

      it "rounds net_amount up" do
        # 125*0.93 = 116.25
        expected = BigDecimal('116.25')
        Rails.logger.debug "net amount: #{@li[:net_amount]} class  #{@li[:net_amount].class} "
        expect(@li[:net_amount]).to eql(expected)
      end
    end
  end

  context 'finding a rate with' do
    let(:swoop_company) { create(:super_company) }

    let!(:partner_company) do
      create(:rescue_company, {
        name: 'Finish Line Towing',
      })
    end

    let(:site) do
      create(:partner_site, {
        company: partner_company,
      })
    end

    let!(:partners_swoop_account) do
      create(
        :account,
        name: 'Swoop',
        company: partner_company,
        client_company: swoop_company
      )
    end

    let!(:tow_service) do
      create(:service_code, name: 'Tow')
    end

    context 'inherit' do
      subject do
        Rate.inherit(partner_company.id,
                     partners_swoop_account.id,
                     site.id,
                     nil,
                     tow_service.id,
                     nil)
      end

      context 'no rates' do
        it 'returns a default rate' do
          expect(subject).not_to be_nil
        end

        it 'creates a default rate' do
          expect { subject }.to change(Rate, :count).from(0).to(1)
        end
      end

      context 'default rate only' do
        let!(:default_rate) do
          create(:clean_flat_rate, {
            company: partner_company,
            live: true,
          })
        end

        it 'returns default' do
          expect(subject).not_to be_nil
        end
      end
    end

    context 'inherit type' do
      subject do
        Rate.inherit_type(partner_company.id,
                          partners_swoop_account.id,
                          site.id,
                          nil,
                          tow_service.id,
                          nil,
                          'StorageRate')
      end

      context 'no rates' do
        it 'returns a default rate' do
          expect(subject).not_to be_nil
        end

        it 'creates a default rate' do
          expect { subject }.to change(Rate, :count).from(0).to(1)
        end

        it 'creates storage default rate' do
          subject
          expect(Rate.first.type).to eql('StorageRate')
        end
      end
    end
  end

  context "ported from minitest" do
    include ActionView::Helpers::NumberHelper
    fixtures :all

    def add_audit_job_statuses_to(job, stored_date = "2015-12-18 15:21:51", released_date = "2016-04-05 11:22:38 -0700")
      (job.audit_job_statuses << create(:audit_job_status, job: job, job_status: create(:job_status, :stored), last_set_dttm: stored_date))
      (job.audit_job_statuses << create(:audit_job_status, job: job, job_status: create(:job_status, :released), last_set_dttm: released_date))
    end

    it "hours" do
      job = jobs(:one)
      expect(job.invoice).to(be_truthy)
      expect(job.mins_ab).to(eq(60))
      expect(job.mins_on_site).to(eq(20))
      expect(job.mins_at_dropoff).to(eq(20))
      expect(job.mins_bc).to(eq(23))
      expect(job.mins_ca).to(eq(41))
      expect(job.mins_p2p).to(eq(164))
      rate = Rate.inherit(job.rescue_company, job.account, job.site, job.issc_contractor_id, job.service_code_id, nil)
      expect(rate.hourly).to(eq(3000))
      lis = rate.calculate_line_items(job)
      sum = 0
      lis.each { |li| sum = (sum + li[:net_amount]) }
      expect(number_with_precision(sum, precision: 2)).to(eq("8250.00"))
    end

    it "miles_p2p" do
      job = jobs(:job_two)
      expect(job.invoice).to(be_truthy)
      expect(job.estimate.miles_ab).to(eq(7.2))
      expect(job.estimate.miles_bc).to(eq(2.8))
      expect(job.estimate.miles_ca).to(eq(0.2))
      expect(job.estimate.miles_p2p).to(eq(10.2))
      rate = Rate.inherit(job.rescue_company, job.account, job.site, job.issc_contractor_id, job.service_code_id, nil)
      lis = rate.calculate_line_items(job)
      sum = 0
      lis.each { |li| sum = (sum + li[:net_amount].to_f) }
      expect(number_with_precision(sum, precision: 2)).to(eq("103.78"))
    end

    it "minimum_hourly" do
      mins = 43
      mins = HoursP2PRate.fudge(12345, mins, nil, nil, 60)
      expect(mins).to(eq(60))
    end

    it "round up 15,5 at 6 mins" do
      mins = 6
      mins = HoursP2PRate.fudge(12345, mins, 15, 5, nil)
      expect(mins).to(eq(15))
    end

    it "round down 15,5 at 4 mins" do
      mins = 4
      mins = HoursP2PRate.fudge(12345, mins, 15, 5, nil)
      expect(mins).to(eq(0))
    end

    it "round up 30,15 at 16 mins" do
      mins = 16
      mins = HoursP2PRate.fudge(12345, mins, 30, 15, nil)
      expect(mins).to(eq(30))
    end

    it "round down 30,15 at 14 mins" do
      mins = 14
      mins = HoursP2PRate.fudge(12345, mins, 30, 15, nil)
      expect(mins).to(eq(0))
    end

    it "round down 30,15 at 14 mins, minumum 25" do
      mins = 14
      mins = HoursP2PRate.fudge(12345, mins, 30, 15, 25)
      expect(mins).to(eq(25))
    end

    context '#get_qty' do
      let(:item) { create :inventory_item, :one, :with_vehicle, :undeleted }
      let(:job) { jobs(:one) }

      subject(:qty) do
        add_audit_job_statuses_to(job, item.stored_at, now.utc)
        rate.get_qty(job, [item], now.utc, rate)
      end

      context 'Free Pickup Window' do
        context 'Free storage period is 0' do
          let(:rate) { rates(:storage_no_free_pickup_24_24) }
          let(:now) { Time.parse("2016-04-05 11:22:38 -0700") }

          it "will be billed immediately" do
            expect(qty).to(eq(1))
          end
        end

        context 'There is a free storage period' do
          let(:free_pick_up_period) { 3.hours }
          let(:rate) { rates(:storage_3_hours_free_pickup_24_24) }

          context "The storage is free before the expiration of the free storage period" do
            let(:now) { item.stored_at + free_pick_up_period - 1.seconds }

            it { should eq(0) }
          end

          context "The storate is not free after the expiration of the free storage period" do
            let(:now) { item.stored_at + free_pick_up_period + 1.seconds }

            it { should eq(1) }
          end
        end
      end

      context 'Charge for 2nd Day' do
        context 'rate is set to be "After 12 hours"' do
          let(:calculate_2nd_day_after_time_period) { 12.hours }

          context 'the tricky case', timezone: 'Pacific Time (US & Canada)' do
            let(:rate) { rates(:storage_no_free_pickup_12_calendar) }

            let(:item) { create :inventory_item, :before_noon, :with_vehicle, :undeleted }

            context 'one minute before mignight it calculates 1 day since we have not passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period }

              it { should eq(1) }
            end

            # The user here is charged for the last minute as a whole day
            context 'at midnight it calculates 2 days since we have passed the end of the first period' do
              let(:now) { item.stored_at.end_of_day }

              it { should eq(2) }
            end
          end

          context 'test in isolation with free pickup window' do
            let(:rate) { rates(:storage_no_free_pickup_12_24) }

            context 'calculates 1 day since we have not passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period }

              it { should eq(1) }
            end

            context 'calculates 2 days since we have passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period + 1.seconds }

              it { should eq(2) }
            end
          end

          context 'test with pickup window' do
            let(:rate) { rates(:storage_3_hours_free_pickup_12_24) }

            context 'calculates 1 day since we have not passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period }

              it { should eq(1) }
            end

            context 'calculates 2 days since we have passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period + 1.second }

              it { should eq(2) }
            end
          end
        end

        context 'rate is set to be "After 24 hours"' do
          let(:calculate_2nd_day_after_time_period) { 24.hours }

          context 'test in isolation with free pickup window' do
            let(:rate) { rates(:storage_no_free_pickup_24_24) }

            context 'calculates 1 day since we have not passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period }

              it { should eq(1) }
            end

            context 'calculates 2 days since we have passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period + 1.seconds }

              it { should eq(2) }
            end
          end

          context 'test with pickup window' do
            let(:rate) { rates(:storage_3_hours_free_pickup_24_24) }

            context 'calculates 1 day since we have not passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period }

              it { should eq(1) }
            end

            context 'calculates 2 days since we have passed the end of the first period' do
              let(:now) { item.stored_at + calculate_2nd_day_after_time_period + 1.second }

              it { should eq(2) }
            end
          end
        end

        context 'rate is set to be "Next Calendar Day"', timezone: 'Pacific Time (US & Canada)' do
          let(:calculate_2nd_day_after_time_period) { item.stored_at.end_of_day }

          context 'test in isolation with free pickup window' do
            let(:rate) { rates(:storage_no_free_pickup_calendar_24) }

            context 'calculates 1 day since we have not passed the end of the first period' do
              let(:now) { calculate_2nd_day_after_time_period }

              it { should eq(1) }
            end

            context 'calculates 2 days since we have passed the end of the first period' do
              let(:now) { calculate_2nd_day_after_time_period + 1.seconds }

              it { should eq(2) }
            end
          end

          context 'test with pickup window' do
            let(:rate) { rates(:storage_calendar_24) }

            context 'calculates 1 day since we have not passed the end of the first period' do
              let(:now) { calculate_2nd_day_after_time_period }

              it { should eq(1) }
            end

            context 'calculates 2 days since we have passed the end of the first period' do
              let(:now) { calculate_2nd_day_after_time_period + 1.second }

              it { should eq(2) }
            end
          end
        end
      end

      context 'Charge After That' do
        context 'Per 12 Hours' do
          context '2nd day is billed at the "Next Calendar Day"', timezone: 'Pacific Time (US & Canada)' do
            let(:calculate_2nd_day_after_time_period) { item.stored_at.end_of_day }

            let(:rate) { rates(:storage_no_free_pickup_calendar_12) }

            context 'calculates 2 days since we have not passed the end of the second day' do
              let(:now) { calculate_2nd_day_after_time_period + 12.hours }

              it { should eq(2) }
            end

            context 'calculates 2.5 days since we have passed the end of the second day' do
              let(:now) { calculate_2nd_day_after_time_period + 12.hours + 2.seconds }

              it { should eq(2.5) }
            end

            context 'calculates 2.5 days since we have not passed the end of the next 12 hours' do
              let(:now) { calculate_2nd_day_after_time_period + 12.hours + 12.hours + 1.seconds }

              it { should eq(2.5) }
            end

            context 'calculates 3 day since we have passed the end of the next 12 hours' do
              let(:now) { calculate_2nd_day_after_time_period + 12.hours + 12.hours + 2.seconds }

              it { should eq(3) }
            end
          end
        end

        context 'Per 24 Hours' do
          context '2nd day is billed at the "Next Calendar Day"', timezone: 'Pacific Time (US & Canada)' do
            let(:calculate_2nd_day_after_time_period) { item.stored_at.end_of_day }

            let(:rate) { rates(:storage_no_free_pickup_calendar_24) }

            context 'calculates 2 days since we have not passed the end of the second day' do
              let(:now) { calculate_2nd_day_after_time_period + 24.hours }

              it { should eq(2) }
            end

            context 'calculates 3 days since we have passed the end of the second day' do
              let(:now) { calculate_2nd_day_after_time_period + 24.hours + 2.seconds }

              it { should eq(3) }
            end

            context 'calculates 4 days since we have passed the end of the next 24 hours' do
              let(:now) { calculate_2nd_day_after_time_period + 2 * 24.hours + 2.seconds }

              it { should eq(4) }
            end
          end
        end

        context 'Per Calendar Day', timezone: 'Pacific Time (US & Canada)' do
          context '2nd day is billed at the "Next Calendar Day"' do
            let(:rate) { rates :storage_no_free_pickup_calendar_calendar }

            context 'calculates 2 days since we have not passed the end of the second day' do
              let(:calculate_2nd_day_after_time_period) { item.stored_at.end_of_day }
              let(:now) { calculate_2nd_day_after_time_period + 1.seconds }

              it { should eq(2) }
            end

            context 'calculates 3 days since we have passed the end of the second day' do
              let(:now) { (item.stored_at + 2.days).end_of_day + 1.seconds }

              it { should eq(3) }
            end

            context 'calculates 4 days since we have passed the end of the next calendar day hours' do
              let(:now) { (item.stored_at + 3.days).end_of_day + 1.seconds }

              it { should eq(4) }
            end
          end
        end
      end

      context 'legacy specs copyed from minitest' do
        it "storage 1 day 24 hrs second day after 23 hrs" do
          rate = rates(:storage_24_24)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = (item.stored_at + 23.hours)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(1))
        end

        it "storage 24 hrs second day after 24 hrs" do
          rate = rates(:storage_24_24)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = ((item.stored_at + 24.hours) + 1.second)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(2))
        end

        it "storage 24 hrs, 24 hrs, after midnight second day" do
          rate = rates(:storage_24_24)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = ((item.stored_at + 13.hours) + 24.hours)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(2))
        end

        it "storage 24,24, after 38 hrs, after midnight third day" do
          rate = rates(:storage_24_24)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = ((item.stored_at + (3 * 24.hours)) + 1.hour)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(4))
        end

        it "storage calendar day b4 midnight" do
          rate = rates(:storage_calendar_24)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = item.stored_at.end_of_day
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(1))
        end

        it "storage calendar day,24 after midnight" do
          rate = rates(:storage_calendar_24)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          # now = item.stored_at.end_of_day + 1.second FAILS
          now = item.stored_at.end_of_day + 8.hours # PASSES
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(2))
        end

        it "storage calendar day,24 way after midnight" do
          rate = rates(:storage_calendar_24)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = ((item.stored_at + 13.hours) + 23.hours)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(2))
        end

        it "storage calendar day, 24 hrs, after midnight second day" do
          rate = rates(:storage_calendar_24)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = ((item.stored_at + 13.hours) + 24.hours)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(3))
        end

        it "storage calendar day, calander, after midnight second day" do
          rate = rates(:storage_calendar_calendar)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = (((item.stored_at + 13.hours) + 23.hours) + 30.minute)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(2))
        end

        it "storage calendar day, calander, after 24 hrs, b4 midnight second day" do
          rate = rates(:storage_calendar_calendar)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = (item.stored_at + 25.hours)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(2))
        end

        it "storage calendar day, calander, after 24 hrs, after midnight second day" do
          rate = rates(:storage_calendar_calendar)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = ((item.stored_at + 13.hours) + 24.hours)
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(3))
        end

        it "storage calendar day, calander, after 24 hrs, after midnight third day" do
          rate = rates(:storage_calendar_calendar)
          item = create :inventory_item, :one, :with_vehicle, :undeleted
          job = jobs(:one)
          now = ((item.stored_at + 13.hours) + (2 * 24.hours))
          add_audit_job_statuses_to(job, item.stored_at, now.utc)
          qty = rate.get_qty(job, [item], now.utc, rate)
          expect(qty).to(eq(4))
        end
      end
    end
  end

  describe '.build_additional_line_item_for' do
    subject(:build_additional_line_item) do
      Rate.build_additional_line_item_for(invoice: invoice, addon: addon)
    end

    shared_examples 'builds a line_item correctly' do |original_unit_price, expected_net_amount|
      it 'builds a line_item based on the existent flat_rate' do
        additional_line_item = build_additional_line_item

        expect(additional_line_item.id).to be_nil
        expect(additional_line_item.calc_type).to eq "flat"
        expect(additional_line_item.description).to eq "Dolly"
        expect(additional_line_item.job_id).to eq invoice.job_id
        expect(additional_line_item.net_amount.to_s).to eq expected_net_amount
        expect(additional_line_item.tax_amount.to_s).to eq '0.0'
        expect(additional_line_item.estimated).to be_falsey
        expect(additional_line_item.unit_price.to_s).to eq original_unit_price
        expect(additional_line_item.original_unit_price.to_s).to eq original_unit_price
        expect(additional_line_item.original_quantity.to_s).to eq '1.0'
        expect(additional_line_item.quantity.to_s).to eq '1.0'
        expect(additional_line_item.user_created).to be_truthy
        expect(additional_line_item.version).to eq Invoice::VERSION
      end
    end

    context 'when a valid invoice is passed' do
      let(:invoice) { create(:invoice) }

      context 'and a valid addon is passed' do
        let(:addon) { create(:service_code, :addon, name: ServiceCode::DOLLY) }

        context 'and there is a flat_rate corresponding to the addon' do
          let!(:rate) { create(:clean_flat_rate, company: invoice.sender, service_code: addon, flat: 20.0) }

          it_behaves_like 'builds a line_item correctly', '20.0', '20.0'
        end

        context 'and there is no corresponding flat_rate to the addon' do
          it_behaves_like 'builds a line_item correctly', '0.0', '0.0'
        end
      end

      context 'when nil addon is passed' do
        let(:addon) { nil }

        it 'raises error' do
          expect { build_additional_line_item }.to raise_error Rate::AddonCannotBeBlankError
        end
      end
    end

    context 'when nil invoice is passed' do
      let(:invoice) { nil }
      let(:addon) { create(:service_code, :addon, name: ServiceCode::DOLLY) }

      it 'raises error' do
        expect { build_additional_line_item }.to raise_error Rate::InvoiceCannotBeBlankError
      end
    end
  end

  describe "goa?" do
    it "returns true if the rate is attached to the GOA service code" do
      rate = Rate.new
      expect(rate.goa?).to eq false
      rate.service_code = ServiceCode.new(name: "Tow")
      expect(rate.goa?).to eq false
      rate.service_code = ServiceCode.new(name: "GOA")
      expect(rate.goa?).to eq true
    end
  end

  describe '#create' do
    let(:company) { build_stubbed(:company, currency: 'USD') }

    it 'must match company' do
      expect { create(:rate, currency: 'EUR', company: company) }
        .to raise_error(ActiveRecord::RecordInvalid, /Currency must match Company/)
    end
  end

  shared_examples 'it returns correct line item hashes' do
    it 'returns correct charge line item' do
      expect(subject.first.dig(:description)).to eq(description)
      expect(subject.first.dig(:net_amount)).to eq(net_amount)
      expect(subject.first.dig(:unit_price)).to eq(unit_price)
      expect(subject.first.dig(:original_unit_price)).to eq(unit_price)
      expect(subject.first.dig(:quantity)).to eq(quantity)
      expect(subject.first.dig(:original_quantity)).to eq(quantity)
    end

    it 'returns correct free line item' do
      expect(subject.last.dig(:description)).to eq(free_description)
      expect(subject.last.dig(:net_amount)).to eq(net_amount_free)
      expect(subject.last.dig(:unit_price)).to eq(unit_price_free)
      expect(subject.last.dig(:original_unit_price)).to eq(unit_price_free)
      expect(subject.last.dig(:quantity)).to eq(free_quantity)
      expect(subject.last.dig(:original_quantity)).to eq(free_quantity)
    end

    it 'includes the invoice version' do
      subject.each { |li| expect(li.dig(:version)).to eq(Invoice::VERSION) }
    end

    it 'includes estimated' do
      subject.each { |li| expect(li.dig(:estimated)).to be(true) }
    end

    shared_examples 'handles nil distance' do
      let(:distance) { nil }

      it 'sets quantity zero' do
        expect(subject.first.dig(:quantity)).to eq(0)
        expect(subject.first.dig(:original_quantity)).to eq(0)
      end

      it 'sets free to zero' do
        expect(subject.last.dig(:quantity)).to eq(0)
        expect(subject.last.dig(:original_quantity)).to eq(0)
      end
    end

    context 'when not an Invoicable' do
      let(:job) { double }

      it 'blows up' do
        expect { subject }.to raise_error(CannotCalculateRateWithoutInvoicableError)
      end
    end
  end

  shared_context 'rates setup' do
    let(:company) do
      Company.new(invoice_mileage_rounding: Company::DISTANCE_ROUND_NEAREST, distance_unit: distance_unit)
    end
    let(:rate) do
      Rate.new(miles_p2p_free: miles_free,
               miles_p2p: price_per_mile,
               miles_towed_free: miles_free,
               miles_towed: price_per_mile,
               miles_enroute_free: miles_free,
               miles_enroute: price_per_mile,
               miles_deadhead_free: miles_free,
               miles_deadhead: price_per_mile,
               company: company)
    end
    let(:estimate) { Estimate.new }
    let(:job) { Job.new(estimate: estimate) }
    let(:mi_in_km) { 1.0 / Geocoder::Calculations::KM_IN_MI }
    let(:price_per_mile) { price_per_kilometer * mi_in_km }
    let(:miles_free) { kilometers_free / mi_in_km }
    let(:distance) { 5 }
    let(:net_amount) { 2.50 }
    let(:net_amount_free) { -0.50 }
    let(:unit_price) { 0.50 }
    let(:unit_price_free) { -0.50 }
    let(:quantity) { 5 }
    let(:free_quantity) { 1 }

    before do
      # This doesn't make sense because p2p is a sum of the others
      # but we're only ever calling one of these
      allow(estimate).to receive(:miles_ca).and_return(distance)
      allow(estimate).to receive(:miles_ab).and_return(distance)
      allow(estimate).to receive(:miles_ba).and_return(distance)
      allow(estimate).to receive(:miles_bc).and_return(distance)
      allow(estimate).to receive(:miles_p2p).and_return(distance)
    end
  end

  describe '#base_tow' do
    include_context 'rates setup'

    subject { rate.base_tow(job) }

    context 'in mi' do
      let(:miles_free) { 1 }
      let(:price_per_mile) { 0.50 }
      let(:distance_unit) { 'mi' }
      let(:description) { 'Towed Per Mile' }
      let(:free_description) { 'Towed Free Miles' }

      it_behaves_like 'it returns correct line item hashes'

      context 'rounding' do
        let(:price_per_mile) { BigDecimal('7.51') }

        # disable other rounding so this can be tested
        before do
          job.fleet_company = FleetCompany.new(name: 'Tesla')
        end

        context 'down' do
          let(:miles_free) { BigDecimal('5.66') }
          let(:distance) { 55.44 }

          it "Sets Towed Quantity to 1dp and rounds down" do
            # got 55.0
            expect(subject.first[:quantity]).to eql(BigDecimal('55.4'))
          end

          it "sets towed net amount" do
            # 55.4 * $7.51 = 416.054    # getting 413.05
            expect(subject.first[:net_amount]).to eql(BigDecimal('416.05'))
          end

          it "Sets Free Quantity to 2dp" do
            # getting 5.7
            expect(subject.last[:quantity]).to eql(BigDecimal('5.66'))
          end

          it "sets free net amount rounded up" do
            # 5.66 * $7.51 =         got 42.81
            expect(subject.last[:net_amount]).to eql(BigDecimal('-42.51'))
          end
        end

        context 'rounding down' do
          let(:miles_free) { BigDecimal('3') }
          let(:distance) { 55.45 }

          it "Sets Towed Quantity to 1dp and rounds up" do
            expect(subject.first[:quantity]).to eql(BigDecimal('55.5'))
          end
        end
      end
    end

    context 'in km' do
      # User inputs
      let(:kilometers_free) { 3 }
      let(:price_per_kilometer) { 0.37 }
      # ************************
      let(:distance) { 7.38 } # miles
      let(:quantity) { 12 } # rounds to 12 kilometers
      let(:unit_price) { 0.37 }
      let(:net_amount) { 4.44 }
      let(:net_amount_free) { -1.11 }
      let(:unit_price_free) { -0.37 }
      let(:free_quantity) { 3 }
      let(:distance_unit) { 'km' }
      let(:description) { 'Towed Per Kilometer' }
      let(:free_description) { 'Towed Free Kilometers' }

      it_behaves_like 'it returns correct line item hashes'
    end
  end

  describe '#base_en_route' do
    include_context 'rates setup'

    subject { rate.base_en_route(job) }

    context 'in mi' do
      # INPUTS
      let(:miles_free) { 1 }
      let(:price_per_mile) { 0.50 }
      # **************************
      let(:distance_unit) { 'mi' }
      let(:description) { 'En Route Per Mile' }
      let(:free_description) { 'En Route Free Miles' }

      it_behaves_like 'it returns correct line item hashes'
    end

    context 'in km' do
      let(:kilometers_free) { 2 }
      let(:price_per_kilometer) { 0.10 }
      let(:distance) { 5 } # miles
      let(:quantity) { 8 }
      let(:unit_price) { 0.10 }
      let(:net_amount) { 0.80 }
      let(:net_amount_free) { -0.20 }
      let(:unit_price_free) { -0.10 }
      let(:free_quantity) { 2 }
      let(:distance_unit) { 'km' }
      let(:description) { 'En Route Per Kilometer' }
      let(:free_description) { 'En Route Free Kilometers' }

      it_behaves_like 'it returns correct line item hashes'
    end
  end

  describe '#base_deadhead' do
    include_context 'rates setup'

    subject { rate.base_deadhead(job) }

    context 'in mi' do
      # INPUTS
      let(:miles_free) { 1 }
      let(:price_per_mile) { 0.50 }
      # **************************
      let(:distance_unit) { 'mi' }
      let(:description) { 'Deadhead Per Mile' }
      let(:free_description) { 'Deadhead Free Miles' }

      it_behaves_like 'it returns correct line item hashes'
    end

    context 'in km' do
      # INPUTS
      let(:kilometers_free) { 1 }
      let(:price_per_kilometer) { 0.50 }
      let(:distance) { 5 } # miles
      let(:quantity) { 8 }
      let(:net_amount) { 4.00 }
      # **************************
      let(:distance_unit) { 'km' }
      let(:description) { 'Deadhead Per Kilometer' }
      let(:free_description) { 'Deadhead Free Kilometers' }

      it_behaves_like 'it returns correct line item hashes'
    end
  end

  describe '#base_p2p' do
    include_context 'rates setup'

    let(:kilometers_free) { 0 }

    subject { rate.base_p2p(job) }

    shared_examples 'it returns a line item hash' do
      it 'works' do
        expect(subject.first.dig(:description)).to eq(description)
        expect(subject.first.dig(:net_amount)).to eq(net_amount)
        expect(subject.first.dig(:unit_price)).to eq(unit_price)
        expect(subject.first.dig(:original_unit_price)).to eq(unit_price)
        expect(subject.first.dig(:quantity)).to eq(quantity)
        expect(subject.first.dig(:original_quantity)).to eq(quantity)
      end

      it 'includes the invoice version' do
        expect(subject.first.dig(:version)).to eq(Invoice::VERSION)
      end

      it 'includes estimated' do
        expect(subject.first.dig(:estimated)).to be(true)
      end

      context 'when not an Invoicable' do
        let(:job) { double }

        it 'blows up' do
          expect { subject }.to raise_error(CannotCalculateRateWithoutInvoicableError)
        end
      end
    end

    context 'in mi' do
      # USER INPUTS
      let(:price_per_mile) { 0.50 }
      # **************************
      let(:distance_unit) { 'mi' }
      let(:description) { 'Port To Port Miles' }

      it_behaves_like 'it returns a line item hash'
    end

    context 'in km' do
      # USER INPUTS
      let(:price_per_kilometer) { 0.23 }
      # **************************
      let(:distance_unit) { 'mi' }
      let(:distance) { 5 } # miles
      let(:quantity) { 8 }
      let(:unit_price) { 0.23 }
      let(:net_amount) { 1.84 }
      let(:distance_unit) { 'km' }
      let(:description) { 'Port To Port Kilometers' }

      it_behaves_like 'it returns a line item hash'
    end
  end
end
