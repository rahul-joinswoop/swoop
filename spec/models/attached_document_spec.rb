# frozen_string_literal: true

require "rails_helper"

describe AttachedDocument do
  describe "callbacks" do
    subject { attached_document.update! deleted_at: Time.now }

    let(:attached_document) { create :signature_image, job: job, user: user, company: company }
    let(:user) { create(:user) }

    describe "Subscribable.trigger_job_document_changed" do
      context 'when AttachedDocument.job is present' do
        let(:job) { create(:rescue_job) }
        let(:company) { nil }

        it 'schedules GraphQL::JobDocumentChangedWorker' do
          expect(GraphQL::JobDocumentChangedWorker).to receive(:perform_async).once.with(attached_document.id)

          subject
        end
      end

      context 'when AttachedDocument.job is blank' do
        let(:job) { nil }
        let(:company) { create(:company) }

        it 'does not schedule GraphQL::JobDocumentChangedWorker' do
          expect(GraphQL::JobDocumentChangedWorker).not_to receive(:perform_async)

          subject
        end
      end
    end
  end
end
