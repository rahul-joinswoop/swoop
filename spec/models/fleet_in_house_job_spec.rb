# frozen_string_literal: true

require "rails_helper"
require 'aasm/rspec'

describe FleetInHouseJob do
  let(:fleet_in_house_job) { create(:fleet_in_house_job) }

  it "creates fleet_in_house_job" do
    expect(fleet_in_house_job).to be_valid
  end

  it "has defined associations" do
    # could be redundant
    expect(fleet_in_house_job.fleet_company).to be_valid
    expect(fleet_in_house_job.service_code).to be_valid
    expect(fleet_in_house_job.rescue_company).to be_valid
    expect(fleet_in_house_job.driver).to be_valid
  end

  describe '#init_review_sms' do
    subject { create(:fleet_in_house_job, review_sms: review_sms).review_sms }

    context "with no values" do
      let(:review_sms) { nil }

      it "works" do
        expect(subject).to be_nil
      end
    end

    context "with false" do
      let(:review_sms) { false }

      it "works" do
        expect(subject).to be_falsey
      end
    end

    context "with true" do
      let(:review_sms) { true }

      it "works" do
        expect(subject).to be_truthy
      end
    end
  end

  describe 'aasm state transitions' do
    let(:fleet_manual?) { true }

    before :each do
      allow_any_instance_of(described_class).to(
        receive(:fleet_manual?)
      ).and_return fleet_manual?

      allow_any_instance_of(described_class).to(receive(:status_change))
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :dispatched
      ).on_event(:fleet_dispatched)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :enroute
      ).on_event(:fleet_enroute)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :onsite
      ).on_event(:fleet_onsite)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :towing
      ).on_event(:fleet_towing)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :towdestination
      ).on_event(:fleet_towdestination)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :completed
      ).on_event(:fleet_completed)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :dispatched
      ).on_event(:fleet_dispatched)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :enroute
      ).on_event(:fleet_enroute)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :onsite
      ).on_event(:fleet_onsite)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :towing
      ).on_event(:fleet_towing)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :towdestination
      ).on_event(:fleet_towdestination)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :completed
      ).on_event(:fleet_completed)
    end
  end

  describe "callbacks" do
    describe "Notifiable.trigger_fleet_in_house_job_assigned" do
      let!(:job) { create :fleet_in_house_job }

      it "works on a status change" do
        expect(Notifiable).to receive(:trigger_fleet_in_house_job_assigned).once.with(job)
        job.assigned!
      end

      it "works on a rescue_company change" do
        expect(Notifiable).to receive(:trigger_fleet_in_house_job_assigned).once.with(job)
        job.update! rescue_company: create(:rescue_company)
      end
    end
  end
end
