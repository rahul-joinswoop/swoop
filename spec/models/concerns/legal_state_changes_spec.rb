# frozen_string_literal: true

require 'rails_helper'
describe LegalStateChanges do
  let(:fleet_company) { create :fleet_company }
  let(:fleet_dispatcher) { create :user, :dispatcher, :fleet, company: fleet_company }
  let(:rescue_company) { create :rescue_company }
  let(:rescue_dispatcher) { create :user, :rescue, :dispatcher, company: rescue_company }
  let(:rescue_driver) { create :user, :rescue, :driver, company: rescue_company }
  let(:swoop) { create :swoop_company }
  let(:user) { create :user }
  let(:status) { :pending }
  let(:job) do
    create :fleet_motor_club_job,
           status: status,
           fleet_company: fleet_company,
           rescue_company: rescue_company,
           rescue_driver: rescue_driver,
           issc_dispatch_request: issc_dispatch_request
  end
  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
  end
  let(:issc) { create(:ago_issc, company: rescue_company) }
  let(:fleet_application) { create :oauth_application, owner: fleet_company, scopes: 'fleet' }
  let(:rescue_application) { create :oauth_application, owner: rescue_company, scopes: 'rescue' }

  describe '#legal_state_changes' do
    subject { job.legal_state_changes viewer }

    shared_examples 'it works' do
      context 'with a rescue user' do
        let(:viewer) { rescue_driver }

        it 'works' do
          expect(subject).to eq(expected_rescue_states)
        end
      end

      context 'with a rescue_application' do
        let(:viewer) { rescue_application }

        it 'works' do
          expect(subject).to eq(expected_rescue_states)
        end
      end

      context 'with a rescue company' do
        let(:viewer) { rescue_company }

        it 'works' do
          expect(subject).to eq(expected_rescue_states)
        end
      end

      context 'with a fleet user' do
        let(:viewer) { fleet_dispatcher }

        it 'works' do
          expect(subject).to eq(expected_fleet_states)
        end
      end

      context 'with a fleet application' do
        let(:viewer) { fleet_application }

        it 'works' do
          expect(subject).to eq(expected_fleet_states)
        end
      end

      context 'with a fleet company' do
        let(:viewer) { fleet_company }

        it 'works' do
          expect(subject).to eq(expected_fleet_states)
        end
      end
    end

    # TODO - this should change when the virtual_status changes are merged - we should return back
    # ['rejected', 'accepted'] at a minimum right?
    context 'with an auto_assigning job' do
      let(:job) do
        create(:fleet_managed_job,
               :with_auction,
               :with_service_location,
               :with_drop_location,
               status: :auto_assigning,
               fleet_company: fleet_company,
               bids: 3).tap do |j|
          j.auction.update! status: Auction::Auction::LIVE
          j.auction.bids << create(:bid, auction: j.auction, job: j, company: rescue_company)
        end
      end

      let(:expected_rescue_states) { [] }
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with a draft job' do
      let(:status) { :draft }
      let(:expected_rescue_states) { ['pending'] }
      let(:expected_fleet_states) { ['pending', 'canceled', 'deleted'] }

      it_behaves_like 'it works'

      # we only need to test a regular user and nil viewer once, their behavior
      # doesn't vary based on the job status
      context 'as a regular user' do
        let(:viewer) { user }

        it 'works' do
          expect(subject).to be_nil
        end
      end

      context 'without a viewer' do
        let(:viewer) { nil }

        it 'works' do
          expect(subject).to be_nil
        end
      end
    end

    context 'with a pending job' do
      let(:status) { :pending }
      let(:expected_rescue_states) { ["dispatched", "enroute", "onsite", "towing", "towdestination", "completed", "goa", "canceled"] }
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with an assigned job' do
      let(:status) { :assigned }
      let(:expected_rescue_states) { ['completed', 'rejected', 'accepted'] }
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with an accepted job' do
      let(:status) { :accepted }
      let(:expected_rescue_states) do
        ['dispatched', 'enroute', 'onsite', 'towing', 'towdestination', 'completed', 'goa', 'canceled']
      end
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with an dispatched job' do
      let(:status) { :dispatched }
      let(:expected_rescue_states) do
        ['pending', 'enroute', 'onsite', 'towing', 'towdestination', 'completed', 'goa', 'accepted', 'canceled']
      end
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with an enroute job' do
      let(:status) { :enroute }
      let(:expected_rescue_states) do
        ['pending', 'dispatched', 'onsite', 'towing', 'towdestination', 'completed', 'goa', 'accepted', 'canceled']
      end
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with an onsite job' do
      let(:status) { :onsite }
      let(:expected_rescue_states) do
        ['pending', 'dispatched', 'enroute', 'towing', 'towdestination', 'completed', 'goa', 'accepted', 'canceled']
      end
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with an towing job' do
      let(:status) { :towing }
      let(:expected_rescue_states) do
        ['pending', 'dispatched', 'enroute', 'onsite', 'towdestination', 'completed', 'goa', 'accepted', 'canceled']
      end
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with an towdestination job' do
      let(:status) { :towdestination }
      let(:expected_rescue_states) do
        ['pending', 'dispatched', 'enroute', 'onsite', 'towing', 'completed', 'goa', 'accepted', 'canceled']
      end
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with a goa job' do
      let(:status) { :goa }
      let(:expected_rescue_states) do
        ['dispatched', 'enroute', 'onsite', 'towing', 'towdestination', 'completed', 'canceled']
      end
      let(:expected_fleet_states) { ['canceled'] }

      it_behaves_like 'it works'
    end

    context 'with a completed job' do
      let(:status) { :completed }
      let(:expected_rescue_states) do
        ['pending', 'dispatched', 'enroute', 'onsite', 'towing', 'towdestination', 'goa', 'canceled']
      end
      let(:expected_fleet_states) { ['canceled', 'deleted'] }

      it_behaves_like 'it works'
    end

    context 'with a rejected job' do
      let(:status) { :rejected }
      let(:expected_rescue_states) { [] }
      let(:expected_fleet_states) { [] }

      it_behaves_like 'it works'
    end

    context 'with a stored job' do
      let(:status) { :stored }
      let(:expected_rescue_states) { ['released'] }
      let(:expected_fleet_states) { ['released'] }

      it_behaves_like 'it works'
    end
  end

  describe '#get_service_type_from_viewer' do
    subject { job.send :get_service_type_from_viewer, viewer }

    context 'oauth_application' do
      context 'fleet application' do
        let(:viewer) { fleet_application }

        it 'works' do
          expect(subject).to eq('fleet')
        end
      end

      context 'rescue application' do
        let(:viewer) { rescue_application }

        it 'works' do
          expect(subject).to eq('partner')
        end
      end

      context 'user application' do
        let(:viewer) { create :oauth_application, owner: rescue_driver, scopes: '' }

        it 'works' do
          expect(Rollbar).to receive(:error).once
          expect(subject).to eq('')
        end
      end
    end

    context 'company' do
      context 'fleet company' do
        let(:viewer) { fleet_company }

        it 'works' do
          expect(subject).to eq('fleet')
        end
      end

      context 'rescue company' do
        let(:viewer) { rescue_company }

        it 'works' do
          expect(subject).to eq('partner')
        end
      end

      context 'swoop' do
        let(:viewer) { swoop }

        it 'works' do
          expect(subject).to eq('swoop')
        end
      end
    end

    context 'user' do
      context 'fleet user' do
        let(:viewer) { create :user, :fleet_in_house, company: fleet_company }

        it 'works' do
          expect(subject).to eq('fleet')
        end
      end

      context 'rescue user' do
        let(:viewer) { create :user, :rescue, company: rescue_company }

        it 'works' do
          expect(subject).to eq('partner')
        end
      end

      context 'swoop root' do
        let(:viewer) { create :user, :root, company: swoop }

        it 'works' do
          expect(subject).to eq('swoop')
        end
      end

      context 'customer' do
        let(:viewer) { create :user }

        it 'works' do
          expect(Rollbar).to receive(:error).once
          expect(subject).to eq('')
        end
      end
    end

    context 'something else' do
      let(:viewer) { job }

      it 'works' do
        expect(Rollbar).to receive(:error).once
        expect(subject).to eq('')
      end
    end
  end
end
