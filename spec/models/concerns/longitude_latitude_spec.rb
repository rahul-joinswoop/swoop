# frozen_string_literal: true

require "rails_helper"
describe LongitudeLatitude do
  describe "lnglat" do
    let(:location) { create :location, :random }

    it "works" do
      expect(location.lnglat).to be_a(RGeo::Geographic::SphericalPointImpl)
      expect(location.lnglat.longitude).to eq(location.lng)
      expect(location.lnglat.latitude).to eq(location.lat)
    end

    context 'updates' do
      let(:lat) { Float(Faker::Address.latitude) }
      let(:lng) { Float(Faker::Address.longitude) }

      it "works" do
        location.update! lat: lat, lng: lng
        expect(location.lnglat.longitude).to eq(lng)
        expect(location.lnglat.latitude).to eq(lat)
      end
    end
  end

  describe "matches?" do
    it "is true if the locations are within 4 inches" do
      expect(Location.new(lat: 45.1234566, lng: -110.1234567).matches?(latitude: 45.1234568, longitude: -110.1234569)).to eq true
      expect(Location.new(lat: 45.123457, lng: -110.123456).matches?(latitude: 45.123456, longitude: -110.123456)).to eq false
    end

    it "is false if latitude or longitude is missing" do
      expect(Location.new(lat: nil, lng: -110.1).matches?(latitude: 45.1, longitude: -110.1)).to eq false
      expect(Location.new(lat: 45.1, lng: nil).matches?(latitude: 45.1, longitude: -110.1)).to eq false
      expect(Location.new(lat: 45.1, lng: -110.1).matches?(latitude: nil, longitude: -110.1)).to eq false
      expect(Location.new(lat: 45.1, lng: -110.1).matches?(latitude: 45.1, longitude: nil)).to eq false
    end
  end

  describe '#latlng' do
    subject { location.latlng }

    let(:location) { build :location, :random }

    it { is_expected.to eq("#{location.lat},#{location.lng}") }
  end

  describe '#has_coords?' do
    subject { location.has_coords? }

    context 'with coordinates' do
      let(:location) { build :location, :random }

      it { is_expected.to be(true) }
    end

    context 'without coordinates' do
      let(:location) { build :location }

      it { is_expected.to be(false) }
    end
  end

  describe '#timezone' do
    subject { location.timezone }

    context "without coordinates" do
      let(:location) { build :location }

      it { is_expected.to be_nil }
    end

    context "with coordinates" do
      let(:location) { build :location, :random }
      let(:tz) { ActiveSupport::TimeZone.all.sample }

      it "works" do
        expect_any_instance_of(External::GoogleTimezoneService)
          .to receive(:call).once.and_return(tz.name)
        expect(subject).to eq(tz)
      end
    end
  end
end
