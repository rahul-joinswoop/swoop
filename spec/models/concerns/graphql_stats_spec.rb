# frozen_string_literal: true

require 'rails_helper'

describe GraphQLStats do
  shared_examples 'it works' do
    let(:date_range) do
      (Date.current - (described_class::NUM_DAYS - 1))..Date.current
    end

    describe '#graphql_stats_dates' do
      it 'works' do
        expect(subject.send(:graphql_stats_dates)).to eq(date_range)
      end
    end

    describe '#graphql_stats_key' do
      it 'works' do
        expect(subject.send(:graphql_stats_key)).to eq(expected_graphql_stats_key)
      end
    end

    describe '#graphql_requests' do
      let!(:grapqhl_stats_requests) { create :graphql_stats_requests, target: subject }

      it 'works' do
        expect(subject.graphql_requests).to eq(grapqhl_stats_requests)
      end
    end

    describe '#graphql_deprecated_fields' do
      let!(:graphql_stats_deprecated_fields) { create :graphql_stats_deprecated_fields, target: subject }

      it 'works' do
        expect(subject.graphql_deprecated_fields).to eq(graphql_stats_deprecated_fields)
      end
    end
  end

  context "as an application" do
    let(:subject) { create :oauth_application }
    let(:expected_graphql_stats_key) { "application:#{subject.id}" }

    it_behaves_like 'it works'
  end

  context "as a SuperCompany" do
    let(:subject) { create :super_company }
    let(:expected_graphql_stats_key) { "all" }

    it_behaves_like 'it works'
  end
end
