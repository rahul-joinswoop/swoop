# frozen_string_literal: true

require "rails_helper"

describe DistanceToLocation do
  let(:location) { build(:location) }
  let(:sf) { Location.new(lat: 37.774929, lng: -122.419418) }
  let(:la) { Location.new(lat: 34.052235, lng: -118.243683) }
  let(:ny) { Location.new(lat: 43.000350, lng: -75.499900) }
  let(:ofarrel) { Location.new(lat: 37.786980, lng: -122.405531) }
  let(:metreon) { Location.new(lat: 37.784342, lng: -122.403410) }
  let(:foleys) { Location.new(lat: 37.786504, lng: -122.408806) }
  let(:museum) { Location.new(lat: 37.786708, lng: -122.401361) }

  before do
    stub_class 'Nonsense' do
      include DistanceToLocation
    end

    stub_const "KNOWN_METERS_SF_LA", 560_000
    stub_const "KNOWN_METERS_SF_NY", 3_963_814
    stub_const "KNOWN_METERS_LA_NY", 3_804_489
    stub_const "KNOWN_METERS_HQ_FOLEYS", 295
    stub_const "KNOWN_METERS_HQ_MUSEUM", 365
    stub_const "KNOWN_METERS_HQ_METREON", 348
  end

  # Sanity check that strategy is accurate enough for our purposes. +/- 1%.
  def expect_within_1_percent(location1, location2, correct_distance)
    distance = location1.distance_to(location2).round
    difference = (distance - correct_distance).abs
    expect(difference).to be <= (correct_distance / 100)
  end

  context '#distance_to' do
    it 'fails when passed something without coordinates' do
      expect { sf.distance_to(Object.new) }.to raise_error(TypeError).with_message(/:lat and :lng/)
    end

    it 'fails when included on object without coordinates' do
      expect { Nonsense.new.distance_to(sf) }.to raise_error(TypeError).with_message(/Nonsense/)
    end

    context 'returns the correct distance' do
      context 'when distance is great' do
        it 'from sf to la' do
          expect_within_1_percent(sf, la, KNOWN_METERS_SF_LA)
        end

        it 'from sf to ny' do
          expect_within_1_percent(sf, ny, KNOWN_METERS_SF_NY)
        end

        it 'from la to ny' do
          expect_within_1_percent(la, ny, KNOWN_METERS_LA_NY)
        end
      end

      context 'when distance is small' do
        it 'from HQ to the metreon' do
          expect_within_1_percent(ofarrel, metreon, KNOWN_METERS_HQ_METREON)
        end

        it 'from HQ to the foleys' do
          expect_within_1_percent(ofarrel, foleys, KNOWN_METERS_HQ_FOLEYS)
        end

        it 'from HQ to the museum' do
          expect_within_1_percent(ofarrel, museum, KNOWN_METERS_HQ_MUSEUM)
        end
      end
    end
  end
end
