# frozen_string_literal: true

require "rails_helper"
describe VirtualStatus do
  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company) }
  let(:job) do
    create :fleet_managed_job,
           rescue_company: rescue_company,
           fleet_company: fleet_company,
           status: :assigned
  end

  it 'works' do
    # a nil company means that we're dealing with a customer
    expect(job.virtual_status(nil)).to eq("assigned")
    expect(job.virtual_status(fleet_company)).to eq("assigned")
    expect(job.virtual_status(rescue_company)).to eq("assigned")
  end

  context "in auto_assigning" do
    context "with bid" do
      let(:job) do
        create(:fleet_managed_job,
               :with_auction,
               :with_service_location,
               :with_drop_location,
               status: :auto_assigning,
               fleet_company: fleet_company,
               bids: 1).tap do |j|
          j.auction.update! status: Auction::Auction::LIVE
          j.auction.bids << create(:bid, auction: j.auction, status: bid_status, job: j, company: rescue_company)
        end
      end

      context "with a requested bid" do
        let(:bid_status) { 'requested' }

        it "works" do
          expect(job.virtual_status(nil)).to eq('auto_assigning')
          expect(job.virtual_status(fleet_company)).to eq('auto_assigning')
          expect(job.virtual_status(rescue_company)).to eq("assigned")
          expect(job.virtual_human_status(rescue_company)).to eq("Assigned")
        end
      end

      context "with a submitted bid" do
        let(:bid_status) { 'submitted' }

        it "works" do
          expect(job.virtual_status(nil)).to eq('auto_assigning')
          expect(job.virtual_status(fleet_company)).to eq('auto_assigning')
          expect(job.virtual_status(rescue_company)).to eq("submitted")
          expect(job.virtual_human_status(rescue_company)).to eq("Submitted")
        end
      end

      context "with a rejected bid" do
        let(:bid_status) { 'provider_rejected' }

        it "works" do
          expect(job.virtual_status(nil)).to eq('auto_assigning')
          expect(job.virtual_status(fleet_company)).to eq('auto_assigning')
          expect(job.virtual_status(rescue_company)).to eq("rejected")
          expect(job.virtual_human_status(rescue_company)).to eq("Rejected")
        end
      end

      context "with a auto_rejected bid" do
        let(:bid_status) { 'auto_rejected' }

        it "works" do
          expect(job.virtual_status(nil)).to eq('auto_assigning')
          expect(job.virtual_status(fleet_company)).to eq('auto_assigning')
          expect(job.virtual_status(rescue_company)).to eq("etarejected")
          expect(job.virtual_human_status(rescue_company)).to eq("ETA Rejected")
        end
      end

      context "with a expired bid" do
        let(:bid_status) { 'expired' }

        it "works" do
          expect(job.virtual_status(nil)).to eq('auto_assigning')
          expect(job.virtual_status(fleet_company)).to eq('auto_assigning')
          expect(job.virtual_status(rescue_company)).to eq("expired")
          expect(job.virtual_human_status(rescue_company)).to eq("Expired")
        end
      end

      context "with a canceled bid" do
        let(:bid_status) { 'canceled' }

        it "works" do
          expect(job.virtual_status(nil)).to eq('auto_assigning')
          expect(job.virtual_status(fleet_company)).to eq('auto_assigning')
          expect(job.virtual_status(rescue_company)).to eq("canceled")
          expect(job.virtual_human_status(rescue_company)).to eq("Canceled")
        end
      end
    end

    context "without a bid" do
      let(:job) do
        create(:fleet_managed_job,
               :with_auction,
               :with_service_location,
               :with_drop_location,
               status: :auto_assigning,
               fleet_company: fleet_company,
               bids: 1).tap do |j|
          j.auction.update! status: Auction::Auction::LIVE
        end
      end

      it 'works' do
        expect(job.virtual_status(nil)).to eq('auto_assigning')
        expect(job.virtual_status(fleet_company)).to eq('auto_assigning')
        # we shouldn't ever get to this because a rescue_company without a bid on an auto_assigning
        # job won't have visibility into the job but this is just to verify
        expect(job.virtual_status(rescue_company)).to eq('auto_assigning')
        expect(job.virtual_human_status(rescue_company)).to eq("Auto Assigning")
      end
    end
  end
end
