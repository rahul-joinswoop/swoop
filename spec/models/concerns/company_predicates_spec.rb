# frozen_string_literal: true

require "rails_helper"
describe CompanyPredicates do
  describe "#super?" do
    context "when company type is SuperCompany" do
      let(:company) { build_stubbed :super_company }

      it "returns true" do
        expect(company.super?).to eql(true)
      end
    end
  end

  describe "#fleet_in_house?" do
    context "when company type is FleetCompany and in_house is true" do
      let(:company) { build_stubbed :fleet_company, :tesla }

      it "returns true" do
        expect(company.fleet_in_house?).to eql(true)
      end
    end
  end

  describe "#super_fleet_managed?" do
    context "when company type is FleetCompany and company is agero" do
      let(:company) { build_stubbed :fleet_company, :agero }

      it "returns true" do
        expect(company.super_fleet_managed?).to eql(true)
      end
    end
  end

  describe "#motor_club?" do
    context "with a motor_club" do
      let(:company) { build_stubbed :fleet_company, :motor_club }

      it "returns true when :issc_client_id attribute is true" do
        expect(company.motor_club?).to eql(true)
      end
    end

    context "with a regular company" do
      let(:company) { build_stubbed :company }

      it "returns false" do
        expect(company.motor_club?).to eql(false)
      end
    end
  end

  describe "has_feature?" do
    it "finds a feature by name" do
      company = Company.new
      company.features << Feature.new(name: "foo")
      company.features << Feature.new(name: "bar")
      expect(company.has_feature?("foo")).to eq true
      expect(company.has_feature(:bar)).to eq true
      expect(company.has_feature?("baz")).to eq false
    end
  end

  describe "has_product?" do
    it "finds a product by name" do
      company = Company.new
      company.products << Product.new(name: "foo")
      company.products << Product.new(name: "bar")
      expect(company.has_product?("foo")).to eq true
      expect(company.has_product(:bar)).to eq true
      expect(company.has_product?("baz")).to eq false
    end
  end
end
