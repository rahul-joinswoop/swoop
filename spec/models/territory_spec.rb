# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Territory, type: :model do
  let(:rescue_company) { create :rescue_company }

  context 'fields' do
    it { is_expected.to respond_to(:polygon_area) }
    it { is_expected.to respond_to(:deleted_at) }
    it { is_expected.to respond_to(:created_at) }
    it { is_expected.to respond_to(:updated_at) }
  end

  context 'associations' do
    it { is_expected.to belong_to(:company) }
  end

  context 'scopes' do
    context 'containing_lat_lng' do
      subject { described_class }

      let(:polygon_area_surrounding) { 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -10.5 -10.5))' }
      let(:polygon_area_far_away) { 'POLYGON ((40 40, 40 50, 50 50, 50 40, 40 40))' }
      let!(:containing_territory) { create :territory, company: rescue_company, polygon_area: polygon_area_surrounding }
      let!(:containing_territory_deleted) { create :territory, :soft_deleted, company: rescue_company, polygon_area: polygon_area_surrounding }
      let!(:non_containing_territory) { create :territory, company: rescue_company, polygon_area: polygon_area_far_away }

      it { is_expected.to respond_to(:containing_lat_lng) }

      it 'will return only the containing territory' do
        expect(subject.containing_lat_lng(0, 0).count).to eq(1)
        expect(subject.containing_lat_lng(0, 0)).to include(containing_territory)
        expect(subject.containing_lat_lng(0, 0)).not_to include(containing_territory_deleted)
        expect(subject.containing_lat_lng(0, 0)).not_to include(non_containing_territory)
      end

      it 'will not return the containing territory if it is on the edge' do
        expect(subject.containing_lat_lng(-10.5, 10.5)).not_to include(containing_territory)
      end
    end

    context 'bidding eligibility scopes' do
      # Step 1: We have a service, 'Accident Tow'
      let(:accident_tow_service_code) { ServiceCode.find_by(name: "Accident Tow") }

      # Step 2.1: We have rescue company "A" with 4 territories and service code, 'Accident Tow'
      let(:rescue_company_a) { create :rescue_company }
      let!(:territory_for_rescue_company_a_that_contains_lat_lng) { create :territory, company: rescue_company_a, polygon_area: 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -10.5 -10.5))' }
      let!(:another_territory_for_rescue_company_a_that_contains_lat_lng) { create :territory, company: rescue_company_a, polygon_area: 'POLYGON ((-11.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -11.5 -10.5))' }
      let!(:territory_for_rescue_company_a_that_does_not_contain_lat_lng) { create :territory, company: rescue_company_a, polygon_area: 'POLYGON ((40 40, 40 50, 50 50, 50 40, 40 40))' }
      let!(:territory_covering_san_francisco_for_rescue_company_a_that_contains_lat_lng) { create(:territory, :san_francisco, company: rescue_company_a) }
      let!(:swoop_account_for_rescue_company_a) { create(:account, company: rescue_company_a, client_company: Company.swoop) }
      let!(:accident_tow_service_code_for_rescue_company_a) do
        create(:companies_service, company: rescue_company_a, service_code: accident_tow_service_code)
      end

      # Step 2.2: We have rescue company "B" with 1 territory and service code, 'Accident Tow'
      let(:rescue_company_b) { create :rescue_company }
      let!(:territory_for_company_b_that_contains_lat_lng) { create :territory, company: rescue_company_b, polygon_area: 'POLYGON ((40 40, 40 50, 50 50, 50 40, 40 40))' }
      let!(:swoop_account_for_rescue_company_b) { create(:account, company: rescue_company_b, client_company: Company.swoop) }
      let!(:accident_tow_service_code_for_rescue_company_b) do
        create(:companies_service, company: rescue_company_b, service_code: accident_tow_service_code)
      end

      # Step 3: Job with specific pickup/service location
      let(:job_with_pickup_location) do
        create(:job, service_code: accident_tow_service_code, service_location: create(:location, :mission_safeway))
      end

      context 'overlapping_pickup_location' do
        context 'job having only pickup location' do
          subject(:overlapping_pickup_location) { Territory.overlapping_pickup_location(job_with_pickup_location) }

          it 'returns territories that overlap with the pickup location' do
            expect(overlapping_pickup_location.count).to eq(1)
            expect(overlapping_pickup_location.to_a).to eq([territory_covering_san_francisco_for_rescue_company_a_that_contains_lat_lng])
          end
        end

        context 'job having only dropoff location' do
          let(:job_with_dropoff_location) do
            create(:job, service_code: accident_tow_service_code, service_location: nil, drop_location: create(:location, :fleet_demo_company))
          end

          subject(:overlapping_pickup_location) { Territory.overlapping_pickup_location(job_with_dropoff_location) }

          it 'returns no territories because pickup location is missing' do
            expect(overlapping_pickup_location.count).to eq(0)
          end
        end
      end
    end
  end

  context 'validation' do
    let(:territory) { build :territory, company: rescue_company }
    let(:subject) { territory }

    context 'negative cases' do
      context 'company is not a rescue company' do
        let(:rescue_company) { create :fleet_company }

        it do
          is_expected.not_to be_valid
        end
      end

      context 'the polygon_area is missing' do
        let(:territory) { build :territory, polygon_area: nil }

        it { is_expected.not_to be_valid }
      end

      context 'the polygon_area has interior ring' do
        let(:polygon_area_with_interior_ring) { 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -10.5 -10.5), (-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 -10.5))' }
        let(:territory) { build :territory, polygon_area: polygon_area_with_interior_ring, company: rescue_company }

        it { is_expected.not_to be_valid }
      end
    end

    context 'positive cases' do
      it { is_expected.to be_valid }
    end
  end

  context 'concerns' do
    context 'soft delete' do
      context 'default scope' do
        let!(:territory_1) { create :territory, company: rescue_company }
        let!(:territory_2) { create :territory, :soft_deleted, company: rescue_company }

        it 'returns just one territory' do
          expect(Territory.all.count).to eq(1)
          expect(Territory.all.first).to eq(territory_1)
        end
      end

      context 'the deleted record is not permanently removed' do
        let!(:territory_to_be_soft_deleted) { create :territory, company: rescue_company }

        it 'returns just one territory' do
          expect do
            expect do
              territory_to_be_soft_deleted.soft_delete!
            end.to change { Territory.all.count }.by(-1)
          end.to change { Territory.unscoped.all.count }.by(0)
        end
      end
    end

    context 'polygon_area storage' do
      context 'positive cases' do
        context 'create' do
          let(:polygon_area) { 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -10.5 -10.5))' }
          let!(:territory) { create :territory, company: rescue_company, polygon_area: polygon_area }

          it 'loads the polygon_area into a model' do
            expect(territory.polygon_area).to be_present
          end

          it 'postgis adds a terminal point that closes the polygon into a solid shape' do
            first_point = territory.coordinates.first
            last_point = territory.coordinates.last
            expect(territory.coordinates.count).to eq(5)
            expect(first_point).to eq(last_point)
          end
        end

        context 'change' do
          let(:polygon_area) { 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -10.5 -10.5))' }
          let!(:territory) { create :territory, company: rescue_company }

          it 'loads the polygon_area into a model' do
            territory.polygon_area = polygon_area
            expect { territory.save }.not_to raise_error
          end

          it 'postgis adds a terminal point that closes the polygon into a solid shape' do
            territory.polygon_area = polygon_area
            territory.save!
            first_point = territory.coordinates.first
            last_point = territory.coordinates.last
            expect(territory.coordinates.size).to eq(5)
            expect(first_point).to eq(last_point)
          end
        end
      end
    end
  end

  context 'methods' do
    let(:territory) { create(:territory, company: rescue_company) }

    context 'class level methods' do
      context 'circleoid_coordinates' do
        subject do
          described_class.circleoid_coordinates(center_longitude: 1.2,
                                                center_latitude: 1.2,
                                                radius: 10)
        end

        it 'creates the right coordinates' do
          expect(subject).to eq(
            [
              { lng: 1.2, lat: 1.344927536231884 },
              { lng: 1.2909090909090908, lat: 1.3255109280847013 },
              { lng: 1.3574591643244434, lat: 1.272463768115942 },
              { lng: 1.3818181818181818, lat: 1.2 },
              { lng: 1.3574591643244434, lat: 1.127536231884058 },
              { lng: 1.290909090909091, lat: 1.0744890719152986 },
              { lng: 1.2, lat: 1.055072463768116 },
              { lng: 1.1090909090909091, lat: 1.0744890719152986 },
              { lng: 1.0425408356755566, lat: 1.1275362318840578 },
              { lng: 1.018181818181818, lat: 1.2 },
              { lng: 1.0425408356755566, lat: 1.2724637681159419 },
              { lng: 1.109090909090909, lat: 1.325510928084701 },
              { lng: 1.2, lat: 1.344927536231884 },
            ]
          )
        end
      end
    end

    context 'instance level methods' do
      it "returns polygon_ares's outer ring as the polygon's coordinates" do
        expect(territory.polygon_area.coordinates.first).to eq([
          [-10.5, 10.5],
          [10.5, 10.5],
          [10.5, -10.5],
          [-10.5, -10.5],
          [-10.5, 10.5], # Polygon closing point
        ])
        expect(territory.coordinates).to eq([
          {
            lat: -10.5,
            lng: 10.5,
          }, {
            lat: 10.5,
            lng: 10.5,
          }, {
            lat: 10.5,
            lng: -10.5,
          }, {
            lat: -10.5,
            lng: -10.5,
          }, {
            lat: -10.5,
            lng: 10.5,
          },
        ])
      end
    end

    context 'first_territory_contains_second_territory?' do
      subject { described_class }

      let(:polygon_area_surrounding) { 'POLYGON((42.75142414728736 -71.48402452468872, 42.75370887302121 -71.47555667836593, 42.7499273395579 -71.47259551961349, 42.747106795636306 -71.48006278951095, 42.75142414728736 -71.48402452468872))' }
      let(:polygon_area_inside) { 'POLYGON((42.75170765925776 -71.4802020178214, 42.751581607789454 -71.47659712890538, 42.74981686031511 -71.47625380615148, 42.748682353262545 -71.47814208129796, 42.75170765925776 -71.4802020178214))' }
      let(:polygon_area_exterior) { 'POLYGON((-91.23046875 45.460130637921, -79.8046875 49.837982453085, -88.2421875 32.694865977875, -91.23046875 45.460130637921))' }
      let(:polygon_area_overlapping) { 'POLYGON((42.75142414728736 -71.48402452468872, 42.75486814623554 -71.48477871001216, 42.75222114655179 -71.47924263060543, 42.75142414728736 -71.48402452468872))' }

      it 'returns boolean value' do
        expect(subject.first_territory_contains_second_territory?(polygon_area_surrounding, polygon_area_inside)).to eq(true)
        expect(subject.first_territory_contains_second_territory?(polygon_area_inside, polygon_area_surrounding)).to eq(false)
        expect(subject.first_territory_contains_second_territory?(polygon_area_surrounding, polygon_area_overlapping)).to eq(false)
      end

      it 'returns the containing territory if it is on the edge' do
        expect(subject.first_territory_contains_second_territory?(polygon_area_surrounding, polygon_area_exterior)).to eq(false)
      end
    end

    context 'find_exterior_polygons' do
      subject { described_class }

      let(:processing_record_for_two_polygons_with_the_same_geometry) do
        {
          "type" => "Feature",
          "geometry" => {
            "type" => "MultiPolygon",
            "coordinates" => [
              [
                [
                  [42.75142414728736, -71.48402452468872],
                  [42.75370887302121, -71.47555667836593],
                  [42.7499273395579, -71.47259551961349],
                  [42.747106795636306, -71.48006278951095],
                  [42.75142414728736, -71.48402452468872],
                ],
              ],
              [
                [
                  [42.75142414728736, -71.48402452468872],
                  [42.75370887302121, -71.47555667836593],
                  [42.7499273395579, -71.47259551961349],
                  [42.747106795636306, -71.48006278951095],
                  [42.75142414728736, -71.48402452468872],
                ],
              ],
            ],
          },
        }
      end
      let(:multi_polygon_a_and_a) { RGeo::GeoJSON.decode(processing_record_for_two_polygons_with_the_same_geometry).geometry }

      let(:processing_record_for_b_not_in_a) do
        {
          "type" => "Feature",
          "geometry" => {
            "type" => "MultiPolygon",
            "coordinates" => [
              [
                [
                  [42.75142414728736, -71.48402452468872],
                  [42.75370887302121, -71.47555667836593],
                  [42.7499273395579, -71.47259551961349],
                  [42.747106795636306, -71.48006278951095],
                  [42.75142414728736, -71.48402452468872],
                ],
              ],
              [
                [
                  [-91.23046875, 45.460130637921],
                  [-79.8046875, 49.837982453085],
                  [-88.2421875, 32.694865977875],
                  [-91.23046875, 45.460130637921],
                ],
              ],
            ],
          },
        }
      end

      let(:multi_polygon_b_not_in_a) { RGeo::GeoJSON.decode(processing_record_for_b_not_in_a).geometry }

      # TODO the description of this spec does not match what is actually does.
      # This also throws an invalid geometry error with the GEOS library installed.
      it 'returns polygon size for territory_a in territory_b criteria' do
        expect(subject.find_exterior_polygons(multi_polygon_a_and_a).size).to eq(0)
      end

      it 'returns polygon size for territory_a not in territory_b criteria' do
        expect(subject.find_exterior_polygons(multi_polygon_b_not_in_a).size).to eq(2)
      end
    end
  end
end
