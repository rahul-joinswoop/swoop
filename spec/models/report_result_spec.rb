# frozen_string_literal: true

require 'rails_helper'

describe ReportResult, type: :model do
  describe "#generate_report!" do
    let(:available_params) do
      {
        start_date: { type: "start_date" },
        end_date: { type: "end_date" },
      }
    end

    let(:submitted_params) do
      {
        start_date: 'asdfasdf', end_date: 'asdfasdf',
      }
    end

    let(:strong_params) { ActionController::Parameters.new submitted_params }

    shared_examples "handles submitted_params" do
      it 'works without submitted_params' do
        rr = create :report_result, available_params: available_params
        expect { rr.send(:generate_report!, submitted_params) }.not_to raise_error
      end

      it "works with ActionController::Parameters" do
        rr = create :report_result, available_params: available_params
        expect { rr.send(:generate_report!, submitted_params) }.not_to raise_error
      end

      it "works with hash parameters" do
        rr = create :report_result, available_params: available_params
        expect { rr.send(:generate_report!, submitted_params) }.not_to raise_error
      end
    end

    context "without available_params" do
      let(:available_params) { nil }

      it_behaves_like "handles submitted_params"
    end

    context "with available_params" do
      it_behaves_like "handles submitted_params"
    end
  end

  describe "run_query" do
    it "runs the report query with the specified parameters and save the query" do
      sql = "SELECT 1 AS count WHERE 'a' = ? AND 'b' = ?"
      report = create(:report, sql: sql, default_params: { "p1" => "b" }, all_params: ["p2", "p1"])
      result = report.results.build
      rows = result.run_query("p2" => "a")
      expect(rows[0]['count']).to eq 1
      result.reload
      expect(result.executed_sql).to eq "SELECT 1 AS count WHERE 'a' = 'a' AND 'b' = 'b'"
    end
  end

  describe '#execute_synchronously!' do
    subject(:execute_synchronously) { report_result.execute_synchronously!(submitted_params, :process_invoice_state_partner) }

    let(:root_user) { create(:user, :root) }
    let(:rescue_company) { create(:rescue_company) }

    let(:partner_and_swoop_account) do
      create(
        :account,
        name: 'Partner and Swoop',
        company: rescue_company,
        client_company: root_user.company
      )
    end

    # Create first job and it's invoice
    let(:first_partner_job) do
      create(:fleet_managed_job, :completed, :with_estimate, account: partner_and_swoop_account)
    end

    let!(:first_job_invoice) do
      create(
        :invoice,
        :with_line_items,
        job: first_partner_job,
        state: 'swoop_approved_partner',
        sender: rescue_company,
        recipient: partner_and_swoop_account,
        recipient_company: root_user.company
      )
    end

    # Create second job and it's invoice
    let(:second_partner_job) do
      create(:fleet_managed_job, :completed, :with_estimate, account: partner_and_swoop_account)
    end

    let!(:second_job_invoice) do
      create(
        :invoice,
        :with_line_items,
        job: second_partner_job,
        state: 'swoop_approved_partner',
        sender: rescue_company,
        recipient: partner_and_swoop_account,
        recipient_company: root_user.company
      )
    end

    # Find report type and create ReportResult record
    let(:report) { Report.find_by(name: 'Swoop Partner Invoice') }
    let(:report_result) { create(:report_result, report: report, user: root_user, company: root_user.company) }

    # Prepare params and execute report download code synchronously
    let(:submitted_params) do
      {
        client_company_id: root_user.company.id,
        sender_id: rescue_company.id,
        state: 'swoop_approved_partner',
        job_active: false,
      }
    end

    before do
      sql_response = nil

      allow(report_result).to receive(:process_csv) do |results, _|
        sql_response = results
      end

      # Need to stub out the reporting connection since it won't get data from
      # the transactional fixtures created for the test by the factories
      expect(ReportingDatabaseBase).to receive(:connection).and_return(ApplicationRecord.connection)

      execute_synchronously
      @rows = sql_response.values
    end

    it 'has correct data rows' do
      first_row = @rows[0]
      second_row = @rows[1]

      persisted_invoice1 = Invoice.find(first_row[11])
      persisted_invoice2 = Invoice.find(second_row[11])

      job1 = persisted_invoice1.job
      job2 = persisted_invoice2.job

      # Verify "Invoice ID"
      expect(first_row[0]).to eq("SW#{persisted_invoice1.job_id}")
      expect(second_row[0]).to eq("SW#{persisted_invoice2.job_id}")
      # Verify "Status"
      expect(first_row[3].downcase).to eq(job1.status)
      expect(second_row[3].downcase).to eq(job2.status)
      # Verify "Fleet"
      expect(first_row[4]).to eq(job1.fleet_company.name)
      expect(second_row[4]).to eq(job2.fleet_company.name)
      # Verify "Partner"
      expect(first_row[5]).to eq(persisted_invoice1.sender.name)
      expect(second_row[5]).to eq(persisted_invoice2.sender.name)
      # Verify Invoice "Amount"
      expect(first_row[7].to_f).to eq(persisted_invoice1.total_amount.to_f)
      expect(second_row[7].to_f).to eq(persisted_invoice2.total_amount.to_f)
    end

    context 'have many estimate records assocaited with job' do
      let!(:first_job_estimates) { create_list(:estimate, 5, job: first_partner_job) }
      let!(:second_job_estimates) { create_list(:estimate, 5, job: second_partner_job) }

      it 'report should display single record per job invoice' do
        expect(Estimate.where(job_id: first_partner_job.id).count).to eq(6)
        expect(Estimate.where(job_id: second_partner_job.id).count).to eq(6)

        # Verify records row count in the report
        expect(@rows.count).to eq(2)

        # Verify that each job should point to an estimate
        expect(first_partner_job.estimate.class).to eq(Estimate)
        expect(second_partner_job.estimate.class).to eq(Estimate)
      end
    end
  end
end
