# frozen_string_literal: true

require "rails_helper"

describe Job do
  let(:job) { build_stubbed :job }

  describe 'is expected to implement' do
    context 'Invoicable' do
      it('#fleet_company') { expect { job.fleet_company }.not_to raise_error }
      it('#rescue_company') { expect { job.rescue_company }.not_to raise_error }
      it('#site') { expect { job.site }.not_to raise_error }
      it('#account') { expect { job.account }.not_to raise_error }
      it('#mins_p2p') { expect { job.mins_p2p }.not_to raise_error }
      it('#miles_p2p') { expect { job.miles_p2p }.not_to raise_error }
      it('#estimate') { expect { job.estimate }.not_to raise_error }
      it('#drop_location') { expect { job.drop_location }.not_to raise_error }
      it('#priority_response') { expect { job.priority_response }.not_to raise_error }
      it('#job_id') { expect { job.job_id }.not_to raise_error }
      it('#issc_contractor_id') { expect { job.issc_contractor_id }.not_to raise_error }
    end
  end

  it "creates only 1 default ServiceCode when create 2 jobs" do
    first_job = job # create first one
    second_job = job

    expect(first_job.service_code).to eq(second_job.service_code)
  end

  describe "when :customer_type changes on save" do
    let(:job) { invoice.job }
    let(:invoice) { create :fleet_managed_job_invoice, state: "partner_sent" }

    before do
      allow(job).to receive(:invoice).and_return invoice
    end

    it "does not transition the associated invoice to :partner_new" do
      expect(invoice.state).to eql "partner_sent"
      job.customer_type = create :customer_type, name: "Other than Warranty"
      job.save!
      expect(invoice.state).not_to eql "partner_new"
    end

    context "when :customer_type changes from Customer Pay to Warranty and " \
      "invoice state is :partner_approved" do
      let(:invoice) { create :fleet_in_house_job_invoice, state: "partner_sent" }

      it "transitions the associated invoice to :partner_new" do
        invoice.partner_new
        invoice.partner_approved
        job.customer_type = create :customer_type, name: CustomerType::WARRANTY
        job.save!
        expect(invoice.reload.partner_new?).to be true
      end
    end
  end

  describe "#human_customer_phone" do
    subject { job.human_customer_phone }

    let(:customer) { create(:user, phone: "5555555555") }
    let(:job) { create(:job, customer: customer) }

    it { is_expected.to eq("+15555555555") }
  end

  describe '#dispatched_at' do
    subject do
      job.audit_job_statuses << pending_audit_job_status
      job.audit_job_statuses << submitted_audit_job_status
      job.save!
    end

    let(:job) { create(:job) }

    let(:pending_audit_job_status) do
      create(:audit_job_status, job_status_id: JobStatus::PENDING)
    end

    let(:submitted_audit_job_status) do
      create(:audit_job_status, job_status_id: JobStatus::SUBMITTED,
                                last_set_dttm: Time.zone.now)
    end

    it 'updates dispatched_at' do
      expect { subject && job.reload }.to change(job, :dispatched_at).from(nil)
    end
  end

  describe '#job_history_hash' do
    subject(:job_history) do
      job.job_history_hash(nil)
    end

    context 'when invoice is mark paid' do
      let(:job) { described_class.new(invoice: invoice) }
      let(:invoice) do
        Invoice.new.tap do |invoice|
          invoice.mark_paid
        end
      end

      before :each do
        allow_any_instance_of(Job).to receive(:audit_job_statuses_as_hash)
          .and_return({})
        allow_any_instance_of(Job).to receive(:audit_sms)
          .and_return([])
        allow_any_instance_of(Job).to receive(:audit_sms)
          .and_return([])
      end

      it 'sets Mark Paid status in the job history' do
        expect(job_history[JobStatus::ID_MAP[JobStatus::MARK_PAID]]).to(
          eq AuditJobStatus.audit_job_status_for_job_mark_paid(job)
        )
      end

      context 'when job.invoice is nil' do
        let(:invoice) { nil }

        it 'doesn\'t set Mark Paid status in the job history' do
          expect(job_history[JobStatus::ID_MAP[JobStatus::MARK_PAID]]).to eq nil
        end
      end

      context 'when job.paid is false' do
        let(:invoice) { Invoice.new }

        it 'doesn\'t set Mark Paid status in the job history' do
          expect(job_history[JobStatus::ID_MAP[JobStatus::MARK_PAID]]).to eq nil
        end
      end

      context 'when job.mark_paid_at is nil' do
        let(:invoice) { Invoice.new }

        it 'doesn\'t set Mark Paid status in the job history' do
          expect(job_history[JobStatus::ID_MAP[JobStatus::MARK_PAID]]).to eq nil
        end
      end
    end
  end

  describe "customized_rescue_company_name" do
    let(:job) { Job.new }

    before :each do
      allow_any_instance_of(Job).to receive(:rescue_company).and_return(rescue_company)
    end

    context 'when rescue_company is RescueCompany' do
      let(:rescue_company) { RescueCompany.new }

      before :each do
        allow_any_instance_of(RescueCompany).to receive(:customized_name_having)
      end

      it 'calls rescue_company.name_by_number_of_sites_having' do
        expect_any_instance_of(RescueCompany).to receive(:customized_name_having)

        job.customized_rescue_company_name
      end
    end

    context 'when rescue_company is FleetCompany' do
      let(:rescue_company) { FleetCompany.new }

      it 'returns rescue_company.name' do
        expect_any_instance_of(FleetCompany).to receive(:name)

        job.customized_rescue_company_name
      end

      it 'doesn\'t call rescue_company.name_by_number_of_sites_having' do
        expect_any_instance_of(RescueCompany).not_to receive(:customized_name_having)

        job.customized_rescue_company_name
      end
    end

    # does it happen? anyhow the spec is here (Rodrigo)
    context 'when rescue_company is SuperCompany' do
      let(:rescue_company) { SuperCompany.new }

      it 'returns rescue_company.name' do
        expect_any_instance_of(SuperCompany).to receive(:name)

        job.customized_rescue_company_name
      end

      it 'doesn\'t call rescue_company.name_by_number_of_sites_having' do
        expect_any_instance_of(RescueCompany).not_to receive(:customized_name_having)

        job.customized_rescue_company_name
      end
    end
  end

  describe ".reminder_call" do
    let(:delayed_double) { class_double("Job") }

    context "when job status is 'assigned'" do
      it "calls .call on job with associated site phone" do
        site = create :site, always_open: true, phone: "+14100000001"
        job = create :job, site: site, status: Job::STATUS_ASSIGNED

        expect(Job).to receive(:new_job_call).with("+14100000001")

        Job.reminder_call(job.id)
      end

      it "does not call .call on job with rescue_company phone without site" do
        rescue_company = create :rescue_company, phone: "+14199999991"
        job = create :job,
                     site: nil, status: Job::STATUS_ASSIGNED, rescue_company: rescue_company

        expect(Job).to receive(:new_job_call).with("+14199999991")

        Job.reminder_call(job.id)
      end
    end
  end

  describe 'auction & manual dispatch phone calls' do
    let!(:job_double) { class_double(Job) }
    let(:company) { create :rescue_company, phone: '+14199999991' }
    let!(:job) { create :job, site: job_site, status: Job::STATUS_ASSIGNED, rescue_company: company }

    describe '.call_with_new_job_if_allowed' do
      # For this block, setting a Site to always_open: true with FactoryBot is OK,
      # since Site::OpenChecker#open_right_now? will resolve the local time (this
      # avoids having to manually set a fake clock in RSpec)
      let(:company) { create :rescue_company, phone: '+14199999991' }

      before(:each) do
        Site::OpenChecker.update_open_businesses!
      end

      context 'when job alerts are enabled' do
        context 'when the job site is open for business' do
          let(:job_site) { create :site, always_open: true }

          it 'calls to say you have a new job' do
            expect(job.site.open_for_business?).to be true
            expect(Job).to receive(:new_job_call).with('+14199999991')
            Job.call_with_new_job_if_allowed(company, job.id, job_site.id, '+14199999991')
          end
        end

        context 'when the job site is after hours' do
          let(:job_site) { create :site, always_open: false }

          it 'does not call' do
            expect(job.site.open_for_business?).to be false
            expect(Job).not_to receive(:new_job_call).with('+14199999991')
            Job.call_with_new_job_if_allowed(company, job.id, job_site.id, '+14199999991')
          end
        end
      end

      context 'when job alerts are disabled' do
        let(:job_site) { create :site, always_open: true }
        let!(:disabled_job_alerts) { create(:disable_job_alerts, company: company) }

        it 'does not call' do
          expect(company.settings).to contain_exactly(disabled_job_alerts)
          expect(Job).not_to receive(:new_job_call).with('+14199999991')
          Job.call_with_new_job_if_allowed(company, job.id, job_site.id, '+14199999991')
        end
      end
    end

    describe '.won_auction_call_if_enabled' do
      let(:job_site) { create :site }

      context 'when job alerts are enabled' do
        it 'calls to say you won the job' do
          expect(company.settings).to be_empty
          expect(Job).to receive(:won_auction_call).with('+14199999991')
          Job.won_auction_call_if_enabled(company, job.id, '+14199999991')
        end
      end

      context 'when job alerts are disabled' do
        let!(:disabled_job_alerts) { create(:disable_job_alerts, company: company) }

        it 'does not call' do
          expect(company.settings).to contain_exactly(disabled_job_alerts)
          expect(Job).not_to receive(:won_auction_call).with('+14199999991')
          Job.won_auction_call_if_enabled(company, job.id, '+14199999991')
        end
      end
    end
  end

  describe '#audit_job_status_stored_or_completed' do
    subject { job.audit_job_status_stored_or_completed }

    let!(:job) { FactoryBot.create(:job) }
    let!(:audit_job_status) do
      FactoryBot.create(:audit_job_status, job_status: job_status)
    end

    before do
      job.audit_job_statuses << audit_job_status
    end

    context 'with no appropriate statuses' do
      let!(:job_status) { FactoryBot.create(:job_status, :pending) }

      it { is_expected.to eq(nil) }
    end

    context 'when a stored job status exists?' do
      let!(:job_status) { FactoryBot.create(:job_status, :stored) }

      it { is_expected.to eq(audit_job_status) }
    end

    context 'when a completed job status exists?' do
      let!(:job_status) { FactoryBot.create(:job_status, :completed) }

      it { is_expected.to eq(audit_job_status) }
    end
  end

  describe '#add_return_to_hq_to_history?' do
    subject { job.add_return_to_hq_to_history? }

    let!(:job) { FactoryBot.create(:job, invoice: invoice) }
    let!(:invoice) { FactoryBot.create(:invoice) }
    let!(:job_status) { FactoryBot.create(:job_status, :stored) }

    let(:audit_job_status) do
      FactoryBot.create(:audit_job_status, job_status: job_status)
    end
    let(:mins_ca) { nil }

    before do
      allow(job).to receive(:mins_ca).and_return(mins_ca)
    end

    context 'when audit_job_status_stored_or_completed is nil' do
      let(:mins_ca) { 40 }

      it { is_expected.to eq(false) }
    end

    context 'when mins_ca is nil' do
      let(:mins_ca) { nil }

      before do
        job.audit_job_statuses << audit_job_status
      end

      it { is_expected.to eq(false) }
    end

    context 'when conditions are appropriate' do
      let(:mins_ca) { 40 }

      before do
        job.audit_job_statuses << audit_job_status
      end

      context 'when are only inappropriate rate types' do
        let!(:rate) { FactoryBot.create(:clean_flat_rate) }

        before do
          invoice.rate_type = nil
          invoice.line_items << create(:invoicing_line_item, rate: rate)
        end

        it { is_expected.to eq(false) }
      end

      context 'when invoice has a matching rate type in its line items' do
        let!(:rate) { FactoryBot.create(:hourly_p2p) }

        before do
          invoice.line_items << create(:invoicing_line_item, rate: rate)
        end

        it { is_expected.to eq(true) }
      end

      context 'when invoice is assigned a matching rate type' do
        before do
          invoice.rate_type = HoursP2PRate.name
        end

        it { is_expected.to eq(true) }
      end
    end
  end

  describe "#site_or_rescue_company_phone" do
    context "when job has a site associated" do
      it "returns the site phone" do
        site = create :site, phone: "+14100000001"
        job = create :job, site: site

        expect(job.site_or_rescue_company_phone).to eq("+14100000001")
      end

      context "when the site has no phone" do
        it "returns the rescue_company phone" do
          site = create :site, phone: nil
          rescue_company = create :rescue_company, phone: "+14199999991"
          job = create :job, site: site, rescue_company: rescue_company

          expect(job.site_or_rescue_company_phone).to eq("+14199999991")
        end
      end

      context "when has no site or rescue_company" do
        it "returns nil" do
          job = create :job, site: nil, rescue_company: nil

          expect(job.site_or_rescue_company_phone).to eq(nil)
        end
      end
    end
  end

  describe "#dropoff_eta" do
    it "returns nil when the job does not have a drop_location" do
      job = build_stubbed :job, drop_location: nil
      expect(job.dropoff_eta).to be_nil
    end

    it "returns nil when the job has a drop_location with nil coords" do
      drop_location = build_stubbed :location, lat: nil, lng: nil
      job = build_stubbed :job, drop_location: drop_location

      allow(job)
        .to receive(:current_job_status_id).and_return(JobStatus::ACCEPTED)
      allow(job).to receive(:current_eta_time).and_return(5)
      allow(job).to receive(:mins_bc).and_return(10)

      expect(job.dropoff_eta).to be_nil
    end

    it "returns the added minutes when the job has a drop_location" do
      drop_location = build_stubbed :location, lat: 37.36, lng: -121.95
      job = build_stubbed :job, drop_location: drop_location

      allow(job)
        .to receive(:current_job_status_id).and_return(JobStatus::DISPATCHED)
      allow(job).to receive(:current_eta_time).and_return(5)
      allow(job).to receive(:mins_bc).and_return(10)

      expect(job.dropoff_eta).to eq 1805
    end
  end

  describe "completed_at" do
    it "pulls the completed at time from the list of audit_job_statuses" do
      time = Time.new(2018, 5, 15, 12)
      job = create(:job)
      expect(job.completed_at).to eq nil
      job.audit_job_statuses.create!(job_status_id: JobStatus::INITIAL, adjusted_dttm: time)
      expect(job.completed_at).to eq nil
      job.audit_job_statuses.create!(job_status_id: JobStatus::REJECTED, adjusted_dttm: time)
      expect(job.completed_at).to eq time
      job.audit_job_statuses.create!(job_status_id: JobStatus::COMPLETED, adjusted_dttm: time + 60)
      expect(job.completed_at).to eq time + 60
    end
  end

  describe "stored_at" do
    it "pulls the stored at time from the list of audit_job_statuses" do
      time = Time.new(2018, 5, 15, 12)
      job = create(:job)
      expect(job.stored_at).to eq nil
      job.audit_job_statuses.create!(job_status_id: JobStatus::INITIAL, adjusted_dttm: time)
      expect(job.stored_at).to eq nil
      job.audit_job_statuses.create!(job_status_id: JobStatus::STORED, adjusted_dttm: time)
      expect(job.stored_at).to eq time
    end
  end

  describe "dispatched_at" do
    it "is nil if the job is not dispatched" do
      job = Job.new
      expect(job.dispatched_at).to eq nil
    end

    it "is the time of dispatch if the job was dispatched" do
      job = Job.new(status: :dispatched)
      time = Time.now
      job.audit_job_statuses.build(job_status_id: JobStatus::DISPATCHED, adjusted_dttm: time)
      job.audit_job_statuses.build(job_status_id: JobStatus::TOWING, adjusted_dttm: time + 60)
      expect(job.dispatched_at).to eq time
    end

    it "is the time of the job changing from pending if it was not dispatched" do
      job = Job.new(status: :towing)
      time = Time.now
      job.audit_job_statuses.build(job_status_id: JobStatus::PENDING, adjusted_dttm: time)
      job.audit_job_statuses.build(job_status_id: JobStatus::TOWING, adjusted_dttm: time + 60)
      expect(job.dispatched_at).to eq time + 60
    end

    it "is the time of the job changing from accepted if it was not dispatched" do
      job = Job.new(status: :towing)
      time = Time.now
      job.audit_job_statuses.build(job_status_id: JobStatus::ACCEPTED, adjusted_dttm: time)
      job.audit_job_statuses.build(job_status_id: JobStatus::TOWING, adjusted_dttm: time + 60)
      expect(job.dispatched_at).to eq time + 60
    end

    it "is not the time of the job changing from pending to a non-dispatched status" do
      job = Job.new(status: :towing)
      time = Time.now
      job.audit_job_statuses.build(job_status_id: JobStatus::PENDING, adjusted_dttm: time)
      job.audit_job_statuses.build(job_status_id: JobStatus::UNASSIGNED, adjusted_dttm: time + 60)
      expect(job.dispatched_at).to eq nil
    end
  end

  describe "indexed invoice states" do
    it "returns an array of job invoice state codes" do
      job = Job.new
      expect(job.invoice_states).to eq []
      job.live_invoices.build(state: Invoice::SWOOP_SENDING_FLEET)
      expect(job.invoice_states).to eq [Invoice::SWOOP_SENDING_FLEET]
    end
  end

  describe "self_assigned?" do
    it "is true only if the partner created the job themselves" do
      partner = create(:rescue_company)
      fleet = create(:fleet_company)
      job_1 = Job.new(fleet_company: partner, rescue_company: partner)
      job_2 = Job.new(fleet_company: fleet, rescue_company: partner)
      expect(job_1.self_assigned?).to eq true
      expect(job_2.self_assigned?).to eq false
    end
  end

  describe "invoice_restrictions?" do
    let(:fleet) { create(:fleet_in_house_company) }
    let(:partner) { create(:rescue_company) }

    it "is true if the job was fleet assigned" do
      job = Job.new(fleet_company: fleet, rescue_company: partner)
      expect(job.invoice_restrictions?).to eq true
    end

    it "does not have any restrictions if the job was self assigned" do
      job = Job.new(fleet_company: partner, rescue_company: partner)
      expect(job.invoice_restrictions?).to eq false
    end

    it "does not have any restrictions if the job is a motor club job" do
      job = FleetMotorClubJob.new(fleet_company: fleet, rescue_company: partner)
      expect(job.invoice_restrictions?).to eq false
    end

    it "does not have any restrictions if the job was from a fleet in house company with no invoice restrictions" do
      job = FleetInHouseJob.new(fleet_company: fleet, rescue_company: partner)
      expect(job.invoice_restrictions?).to eq true

      fleet.features << create(:feature, name: Feature::DISABLE_INVOICE_RESTRICTIONS)
      job = FleetInHouseJob.new(fleet_company: fleet, rescue_company: partner)
      expect(job.invoice_restrictions?).to eq false
    end
  end

  describe "#location_change" do
    subject { job.save! }

    let(:old_location) { create(:location) }
    let(:new_location) { create(:location) }

    it "does not immediately create a history item" do
      job = build(:rescue_job, drop_location: old_location)

      expect { job.save! }.to avoid_changing(HistoryItem, :count)
    end

    context "when drop location changes" do
      let!(:job) { create(:rescue_job, drop_location: old_location) }

      before do
        job.status = Job::TOWING
        job.drop_location = new_location
      end

      it "creates new history item" do
        expect { subject }.to change(HistoryItem, :count).by(1)
      end
    end

    context "when service location changes" do
      let!(:job) { create(:rescue_job, service_location: old_location) }

      before do
        job.status = Job::TOWING
        job.service_location = new_location
      end

      it "creates new history item" do
        expect { subject }.to change(HistoryItem, :count).by(1)
      end
    end

    context "when both changes" do
      let!(:job) do
        create(:rescue_job,
               drop_location: old_location,
               service_location: old_location)
      end

      before do
        job.drop_location = new_location
        job.service_location = new_location
      end

      it "creates two history items" do
        expect { subject }.to change(HistoryItem, :count).by(2)
      end
    end

    context "when neither location changes" do
      let!(:job) { create(:rescue_job) }

      it "does not create any history items" do
        expect { subject }.to avoid_changing(HistoryItem, :count)
      end

      it "does not set the Job#estimate_dirty attribute to true" do
        expect(job.estimate_dirty).to eq(nil)

        subject

        expect(job.estimate_dirty).to eq(false)
      end
    end
  end

  describe "#can_auto_respond?" do
    it "returns true when job is assigned and an answer_by is present" do
      job = create(:job, status: Job::STATUS_ASSIGNED, answer_by: 15.minutes.from_now)
      expect(job.can_auto_respond?).to eq(true)
    end

    it "returns false when only job is assigned" do
      job = create(:job, status: Job::STATUS_ASSIGNED, answer_by: nil)
      expect(job.can_auto_respond?).to eq(false)
    end

    it "returns false when only answer_by is present" do
      job = create(:job, answer_by: 15.minutes.from_now)
      expect(job.can_auto_respond?).to eq(false)
    end
  end

  describe "#send_review_sms" do
    subject { job.send_review_sms }

    let!(:job) { create(:rescue_job) }

    context "when fleet company lacks the correct features" do
      it "does not send review SMS" do
        expect(ReviewSMS).not_to receive(:perform_at)

        subject
      end
    end

    context "when corresponding fleet company has the correct feature" do
      before do
        allow(job.fleet_company).to receive(:has_feature)
          .with(Feature::SMS_REVIEWS).and_return(true)
      end

      it "attempts to send review SMS" do
        expect(ReviewSMS).to receive(:perform_at)

        subject
      end
    end
  end

  describe "#draft_and_storage?" do
    subject { job.draft_and_storage? }

    let(:job) { create(:rescue_job, status: status, service_code: service_code) }

    context 'when status is Draft' do
      let(:status) { 'draft' }

      context 'and service_code is Storage' do
        let(:service_code) { create(:service_code, name: ServiceCode::STORAGE) }

        it { is_expected.to be_truthy }
      end

      context 'and service_code is not Storage' do
        let(:service_code) { create(:service_code, name: ServiceCode::TOW) }

        it { is_expected.to be_falsey }
      end

      context 'and service_code is nil' do
        let(:service_code) { nil }

        it { is_expected.to be_falsey }
      end
    end

    context 'when status is not Draft' do
      let(:status) { 'created' }

      context 'and service_code is Storage' do
        let(:service_code) { create(:service_code, name: ServiceCode::STORAGE) }

        it { is_expected.to be_falsey }
      end

      context 'and service_code is not Storage' do
        let(:service_code) { create(:service_code, name: ServiceCode::TOW) }

        it { is_expected.to be_falsey }
      end

      context 'and service_code is nil' do
        let(:service_code) { nil }

        it { is_expected.to be_falsey }
      end
    end
  end

  describe "if there is no date passed in" do
    let(:job) { create :job }

    it "returns false" do
      expect(job.create_or_update_release_status(nil)).to eql(false)
    end
  end

  describe "create/update released status in the AJS" do
    let(:job) do
      create :job, status: :completed, drop_location: create(:location, site: create(:site))
    end

    before do
      JobService::CreateStorage.call(
        job: job,
        user: job.user
      )

      # Put job into release status
      job.stored!

      job.status = Job::RELEASED

      job.log_transition
      job.save!
    end

    it "updates existing live AJS", freeze_time: true do
      starting_time = Time.now
      adjusted_time = 1.hour.from_now

      job.create_or_update_release_status(starting_time)
      job.save!

      status = job.audit_job_statuses.for_live_status(Job::RELEASED).last

      expect(status.last_set_dttm).to eql(starting_time)
      expect(status.adjusted_dttm).to eql(nil)

      job.create_or_update_release_status(adjusted_time)
      job.save!

      status.reload

      expect(status.last_set_dttm).to eql(starting_time)
      expect(status.adjusted_dttm).to eql(adjusted_time)
    end

    it "updates deleted released AJS", freeze_time: true do
      starting_time = Time.now
      adjusted_time = 1.hour.from_now

      job.create_or_update_release_status(starting_time) # creates the release AJS
      job.save!

      job.undo_release # sets the AJS deleted
      job.save!
      job.reload

      released_ajs = job.audit_job_statuses.find_by(job_status: JobStatus.for_name(Job::RELEASED))
      expect(released_ajs.deleted_at).not_to be_nil

      job.create_or_update_release_status(adjusted_time)
      job.save!

      released_ajs.reload
      expect(released_ajs.last_set_dttm).to eql(starting_time)
      expect(released_ajs.adjusted_dttm).to eql(adjusted_time)
      expect(released_ajs.deleted_at).to be_nil
    end
  end

  describe "adjusted_created_at" do
    let(:fleet_company) { create :fleet_company }
    let(:driver) { create :drive }
    let(:job) { build :job, fleet_company: fleet_company, driver: driver }
    let(:t) { Time.zone.now }

    it "uses scheduled_for as adjusted_created_at if present" do
      job.scheduled_for = t
      job.save!

      expect(job.adjusted_created_at.to_i).to eql(t.to_i)
    end

    it "uses last_set_dttm as adjusted_created_at if present" do
      job.save!

      ajs = AuditJobStatus.create! job: job, job_status_id: JobStatus::CREATED, last_set_dttm: t

      job.reload

      expect(job.adjusted_created_at.to_i).to eql(ajs.last_set_dttm.to_i)
    end

    it "uses adjusted_dttm as adjusted_created_at if present" do
      job.save!

      ajs = AuditJobStatus.create! job: job, job_status_id: JobStatus::CREATED, adjusted_dttm: t

      job.reload

      expect(job.adjusted_created_at.to_i).to eql(ajs.adjusted_dttm.to_i)
    end

    it "uses scheduled_for even if last_set_dttm is present" do
      t2 = t + 1.day
      job.scheduled_for = t

      job.audit_job_statuses.build(job_status_id: JobStatus::CREATED, last_set_dttm: t2)
      job.save!
      job.reload

      expect(job.adjusted_created_at.to_i).to eql(t.to_i)
    end

    it "uses adjusted_dttm even if last_set_dttm is present" do
      t2 = t + 1.day

      job.audit_job_statuses << AuditJobStatus.new(
        job_status_id: JobStatus::CREATED, last_set_dttm: t, adjusted_dttm: t2
      )
      job.save!

      job.reload

      expect(job.adjusted_created_at.to_i).to eql(t2.to_i)
    end

    it "uses last_set_dttm if adjusted_dttm is deleted" do
      t2 = t + 1.day

      job.audit_job_statuses.build(
        job_status_id: JobStatus::CREATED, last_set_dttm: t, adjusted_dttm: t2
      )
      job.save!

      job.reload

      ajs = job.audit_job_statuses.first
      ajs.adjusted_dttm = nil
      ajs.save!

      job.reload

      expect(job.adjusted_created_at.to_i).to eql(t.to_i)
    end

    it "uses created_at if last_set_dttm is deleted" do
      job.audit_job_statuses << AuditJobStatus.new(
        job_status_id: JobStatus::CREATED, last_set_dttm: t
      )
      job.save!

      job.reload

      ajs = job.audit_job_statuses.first
      ajs.last_set_dttm = nil
      ajs.save!

      job.reload

      expect(job.adjusted_created_at.to_i).to eql(job.created_at.to_i)
    end
  end

  describe "#perform_job_updated" do
    let(:fleet_company) { create :fleet_company }
    let(:rescue_company) { create :rescue_company }
    let(:companies) { [fleet_company, rescue_company] }
    let(:job) do
      create :job, status: :pending,
                   fleet_company: fleet_company,
                   rescue_company: rescue_company
    end

    it "works" do
      job
      expect(GraphQL::JobUpdatedWorker)
        .to have_enqueued_sidekiq_job({
          ids: [job.id],
          delta: {
            rescue_company_id: [nil, rescue_company.id],
          },
        }.deep_stringify_keys)
    end
  end

  describe "#status_reason_types_by" do
    let(:job) { create(:job) }
    let(:job_status_id) { double }

    it 'calls API::Jobs::StatusReasonTypes' do
      expect(API::Jobs::StatusReasonTypes).to receive(:new).with(
        job: job, job_status_id: job_status_id
      ).and_call_original

      job.status_reason_types_by(job_status_id)
    end
  end

  describe "#max_bidded_eta_without_explanation" do
    let(:issc_dispatch_request) { create(:issc_dispatch_request, max_eta: 45) }
    let(:job) { create(:fleet_motor_club_job, fleet_company: fleet_company, issc_dispatch_request: issc_dispatch_request) }

    context 'without a magic issc_client_id' do
      let(:fleet_company) { create :fleet_company }

      it 'works' do
        expect(job.max_bidded_eta_without_explanation).to be_nil
      end
    end

    context 'with a magic issc_client_id' do
      let(:fleet_company) { create :fleet_company, :agero }

      context 'when job.issc_dispatch_request.max_eta is nil' do
        let(:issc_dispatch_request) { create(:issc_dispatch_request, max_eta: nil) }

        it 'uses the default' do
          expect(job.max_bidded_eta_without_explanation).to eq(Issc::DEFAULT_MAX_BIDDED_ETA_WITHOUT_EXPLANATION)
        end
      end

      context 'when job.issc_dispatch_request is defined' do
        let(:issc_dispatch_request) { create(:issc_dispatch_request, max_eta: 45) }

        it 'returns issc_dispatch_request.max_eta' do
          expect(job.max_bidded_eta_without_explanation).to eq issc_dispatch_request.max_eta
        end
      end
    end
  end

  describe "#perform_job_status_changed" do
    let(:fleet_company) { create :fleet_company }
    let(:rescue_company) { create :rescue_company }
    let(:companies) { [fleet_company, rescue_company] }

    context "on create" do
      # when we create a job via CreateRescue or CreateFleet our initial status
      # is set to :pending, hence this test
      let!(:job) do
        create :job, status: :pending,
                     fleet_company: fleet_company,
                     rescue_company: rescue_company
      end

      it "works" do
        expect(GraphQL::JobStatusChangedWorker)
          .to have_enqueued_sidekiq_job(
            job.id,
            {
              status: [nil, "pending"],
              rescue_company_id: [nil, rescue_company.id],
            }
          )
      end
    end

    context "created -> pending -> canceled" do
      let!(:job) do
        create :job, status: :created,
                     fleet_company: fleet_company,
                     rescue_company: rescue_company
      end

      it "works" do
        # nothing should be called here
        expect(GraphQL::JobStatusChangedWorker)
          .not_to have_enqueued_sidekiq_job

        # go from :created -> :pending (similar to going from draft/pre_draft
        # but much easier to do manually)
        job.update! status: :pending
        expect(GraphQL::JobStatusChangedWorker)
          .to have_enqueued_sidekiq_job(job.id, status: ['created', 'pending'])

        # now go from :pending -> :canceled
        job.update! status: :canceled
        expect(GraphQL::JobStatusChangedWorker)
          .to have_enqueued_sidekiq_job(job.id, status: ['pending', 'canceled'])
      end
    end
  end

  describe "magical job simulator", freeze_time: true do
    let(:scenario) { JobSimulator.configuration.keys.first }
    let(:rescue_company) { create(:rescue_company) }
    let(:magical_string) { "Simulator Job #{scenario} #{{ partner: rescue_company.name }.to_json}" }

    around :each do |example|
      ClimateControl.modify(JOB_SIMULATOR_ENABLED: "true") { example.call }
    end

    it "kicks off the JobSimulator if one is found in the notes field when the job transitions to pending status" do
      job = create(:fleet_managed_job, status: "Draft", notes: "\n#{magical_string}")
      job.move_to_pending
      job.save!
      simulator = JobSimulator.simulator(magical_string)
      expect(JobSimulator::TransitionWorker).to have_enqueued_sidekiq_job(job.id, simulator.transitions.first, simulator.config).in(simulator.status_delay)
    end

    it "kicks off the JobSimulator if the job is already in pending and the magical string is added to the notes" do
      job = create(:fleet_managed_job, status: "Pending")
      job.update!(notes: magical_string)
      simulator = JobSimulator.simulator(magical_string)
      expect(JobSimulator::TransitionWorker).to have_enqueued_sidekiq_job(job.id, simulator.transitions.first, simulator.config).in(simulator.status_delay)
    end

    it "does not kick off the JobSimulator if it is not enabled" do
      ClimateControl.modify(JOB_SIMULATOR_ENABLED: nil) do
        job = create(:fleet_managed_job, status: "Draft", notes: "\n#{magical_string}")
        job.move_to_pending
        job.save!
      end
      expect(JobSimulator::TransitionWorker.jobs).to be_empty
    end

    it "does not kick off the JobSimulator if no magical string is found in the notes field" do
      job = create(:fleet_managed_job, status: "Draft", notes: nil)
      job.move_to_pending
      job.save!
      expect(JobSimulator::TransitionWorker.jobs).to be_empty
    end

    it "does not kick off the JobSimulator if the job is not pending when the notes are added" do
      job = create(:fleet_managed_job, status: "Pending")
      job.rescue_company = rescue_company
      job.notes = magical_string
      job.swoop_assign
      job.save!
      expect(JobSimulator::TransitionWorker.jobs).to be_empty
    end

    it "does not kick off the JobSimulator for draft jobs" do
      create(:fleet_managed_job, status: "Draft", notes: magical_string)
      expect(JobSimulator::TransitionWorker.jobs).to be_empty
    end
  end

  describe "#job_status" do
    Job.statuses.each do |k, v|
      it "works" do
        expect(Job.new(status: k).job_status.id).to eq(JobStatus::NAME_MAP[v.to_sym])
      end
    end
  end

  describe "status helpers" do
    describe "done?" do
      it "return true only if the job has a done status" do
        expect(Job.new(status: "Created").done?).to eq false
        expect(Job.new(status: "Accepted").done?).to eq false
        expect(Job.new(status: "Dispatched").done?).to eq false
        expect(Job.new(status: "En Route").done?).to eq false
        expect(Job.new(status: "On Site").done?).to eq false
        expect(Job.new(status: "Towing").done?).to eq false
        expect(Job.new(status: "Tow Destination").done?).to eq false
        expect(Job.new(status: "Completed").done?).to eq true
        expect(Job.new(status: "GOA").done?).to eq true
        expect(Job.new(status: "Rejected").done?).to eq true
        expect(Job.new(status: "Reassigned").done?).to eq true
        expect(Job.new(status: "Canceled").done?).to eq true
      end
    end
  end

  context 'issc/rsc' do
    let(:issc_issc) { create(:ago_issc, system: Issc::ISSC_SYSTEM) }
    let(:issc_rsc) { create(:ago_issc, system: Issc::RSC_SYSTEM) }
    let(:issc_dispatch_request_issc) do
      create(:issc_dispatch_request, issc: issc_issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end
    let(:issc_dispatch_request_rsc) do
      create(:issc_dispatch_request, issc: issc_rsc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end
    let(:issc_job) do
      create :fleet_motor_club_job, status: :assigned, issc_dispatch_request: issc_dispatch_request_issc
    end
    let(:rsc_job) do
      create :fleet_motor_club_job, status: :assigned, issc_dispatch_request: issc_dispatch_request_rsc
    end
    let(:other_job) { create :fleet_motor_club_job, issc_dispatch_request: nil }

    describe '#issc?' do
      it 'works' do
        expect(issc_job.issc?).to be(true)
        expect(rsc_job.issc?).to be(false)
        expect(other_job.issc?).to be(false)
      end
    end

    describe '#rsc?' do
      it 'works' do
        expect(issc_job.rsc?).to be(false)
        expect(rsc_job.rsc?).to be(true)
        expect(other_job.rsc?).to be(false)
      end
    end

    describe '#can_request_more_time?' do
      it 'works' do
        expect(issc_job.can_request_more_time?).to be(false)
        expect(rsc_job.can_request_more_time?).to be(true)
        expect(other_job.can_request_more_time?).to be(false)

        # make sure more_time_requested is checked
        rsc_job.issc_dispatch_request.update! more_time_requested: true
        expect(rsc_job.can_request_more_time?).to be(false)
      end
    end

    describe '#can_request_callback?' do
      it 'works' do
        expect(issc_job.can_request_callback?).to be(true)
        expect(rsc_job.can_request_callback?).to be(true)
        expect(other_job.can_request_callback?).to be(false)
      end
    end
  end

  describe "callbacks" do
    let!(:job) { create :job, :with_rescue_driver, :with_service_location }

    describe "Subscribable.trigger_job_status_changed" do
      it "works with a status change" do
        expect(Subscribable).to receive(:trigger_job_status_changed).once.with(job)
        job.accepted!
      end
      it "works with a rescue company change" do
        expect(Subscribable).to receive(:trigger_job_status_changed).once.with(job)
        job.update! rescue_company: create(:rescue_company)
      end
      it "works with a rescue driver change" do
        expect(Subscribable).to receive(:trigger_job_status_changed).once.with(job)
        job.update! rescue_driver: create(:user, :rescue, :driver)
      end
    end

    describe "Subscribable.trigger_job_updated" do
      it "works on all updates" do
        expect(Subscribable).to receive(:trigger_job_updated).once.with(job)
        job.accepted!
      end
    end

    describe "Notifiable.trigger_job_dispatched" do
      it "works with a status change" do
        expect(Notifiable).to receive(:trigger_job_dispatched).once.with(job)
        job.accepted!
      end
      it "works with a rescue driver change" do
        expect(Notifiable).to receive(:trigger_job_dispatched).once.with(job)
        job.update! rescue_driver: create(:user, :rescue, :driver)
      end
    end

    describe "Notifiable.trigger_job_canceled" do
      it "works with a status change" do
        expect(Notifiable).to receive(:trigger_job_canceled).once.with(job)
        job.accepted!
      end
    end

    describe "Notifiable.trigger_job_reassigned" do
      it "works with a rescue driver change" do
        expect(Notifiable).to receive(:trigger_job_reassigned).once.with(job, job.rescue_driver_id)
        job.update! rescue_driver: create(:user, :rescue, :driver)
      end
    end

    describe "Notifiable.trigger_job_service_type_changed" do
      it "works with a service_code change" do
        expect(Notifiable).to receive(:trigger_job_service_type_changed).once.with(job)
        job.update! service_code: create(:service_code, name: ServiceCode::DOLLY)
      end
    end

    describe "Notifiable.trigger_job_location_changed" do
      it "works with a service_location change" do
        expect(Notifiable).to receive(:trigger_job_location_changed).once.with(job)
        job.update! service_location: create(:location)
      end
    end
  end

  describe '#can_partner_manage_photos?' do
    subject { job.can_partner_manage_photos? }

    context 'when it is a RescueJob' do
      let(:job) { create :rescue_job }

      it { is_expected.to be_truthy }
    end

    shared_examples 'false if job.status is' do |not_allowed_statuses|
      context 'when status is not allowed' do
        not_allowed_statuses.each do |not_allowed_status|
          let(:status) { not_allowed_status }

          it { is_expected.to be_falsey }
        end
      end

      context 'when status is allowed' do
        (Job::STATUSES.keys.map(&:to_s) - not_allowed_statuses).each do |allowed_status|
          let(:status) { allowed_status }

          it { is_expected.to be_truthy }
        end
      end
    end

    context 'when it is a FleetInHouseJob' do
      let(:job) { create :fleet_in_house_job, status: status }

      it_behaves_like 'false if job.status is', ['auto_assigning', 'assigned']
    end

    context 'when it is a FleetManagedJob' do
      let(:job) { create :fleet_managed_job, status: status }

      it_behaves_like 'false if job.status is', ['auto_assigning', 'assigned']
    end

    context 'when it is a FleetMotorClubJob' do
      let(:job) { create :fleet_motor_club_job, status: status }

      it_behaves_like 'false if job.status is', ['auto_assigning', 'assigned', 'submitted']
    end
  end

  describe "#account_name" do
    subject { job.account_name }

    context "with an account" do
      let(:job) { create :fleet_motor_club_job, :with_account }

      it "works" do
        expect(subject).to eq(job.account.name)
      end
    end

    context "without an account" do
      context "with a fleet in house job" do
        let(:job) { create :fleet_in_house_job }

        it "works" do
          expect(subject).to eq(job.fleet_company.name)
        end
      end

      context "with a fleet motor club job" do
        let(:job) { create :fleet_motor_club_job }

        it "works" do
          expect(subject).to eq(job.fleet_company.name)
        end
      end

      context "with a fleet managed job" do
        let(:job) { create :fleet_managed_job }

        it "works" do
          expect(subject).to eq(Company.swoop.name)
        end
      end
    end
  end

  describe "ajs_by_status_id" do
    it "looks up the first non-deleted audit job status by id" do
      job = create(:job)
      dispatched = create(:audit_job_status, job: job, job_status_id: JobStatus::DISPATCHED)
      on_site = create(:audit_job_status, job: job, job_status_id: JobStatus::ON_SITE)
      job.reload
      expect(job.ajs_by_status_id(JobStatus::DISPATCHED)).to eq dispatched
      expect(job.ajs_by_status_id(JobStatus::ON_SITE)).to eq on_site
      expect(job.ajs_by_status_id(JobStatus::TOWING)).to eq nil

      dispatched.update!(deleted_at: Time.now)
      job.reload
      expect(job.ajs_by_status_id(JobStatus::DISPATCHED)).to eq nil
    end
  end

  describe "ajs_by_status" do
    it "looks up the first non-deleted audit job status by symbol" do
      job = create(:job)
      dispatched = create(:audit_job_status, job: job, job_status_id: JobStatus::DISPATCHED)
      on_site = create(:audit_job_status, job: job, job_status_id: JobStatus::ON_SITE)
      job.reload
      expect(job.ajs_by_status("dispatched")).to eq dispatched
      expect(job.ajs_by_status("onsite")).to eq on_site
      expect(job.ajs_by_status("towing")).to eq nil

      dispatched.update!(deleted_at: Time.now)
      job.reload
      expect(job.ajs_by_status("dispatched")).to eq nil
    end
  end

  describe "invoices_by_viewer" do
    it "returns the appropriate invoices depending on the viewing company" do
      rescue_company = create(:rescue_company)
      fleet_company = create(:fleet_company)
      super_company = create(:super_company)
      job = create(:job, rescue_company: rescue_company, fleet_company: fleet_company)
      partner_invoice = create(:invoice, job: job, sender: rescue_company)
      fleet_invoice = create(:invoice, job: job, sender: fleet_company)
      expect(job.invoices_by_viewer(rescue_company)).to eq [partner_invoice]
      expect(job.invoices_by_viewer(fleet_company)).to eq [fleet_invoice]
      expect(job.invoices_by_viewer(super_company)).to match_array [partner_invoice, fleet_invoice]
    end
  end

  describe "goa?" do
    it "returns true if the job status is GOA" do
      job = Job.new
      expect(job.goa?).to eq false
      job.status = "dispatched"
      expect(job.goa?).to eq false
      job.status = "goa"
      expect(job.goa?).to eq true
    end
  end

  describe "get_rate" do
    let(:service_code) { create(:service_code) }
    let(:goa_service_code) { create(:service_code, name: "GOA") }
    let(:rescue_company) { create(:rescue_company) }
    let(:fleet_company) { create(:fleet_company) }
    let(:account) { create(:account, company: rescue_company) }
    let!(:default_rate) { create(:flat_rate, company: rescue_company, account: account, service_code: nil, live: true, site: nil, vendor_id: nil) }
    let!(:service_code_rate) { create(:flat_rate, company: rescue_company, account: account, service_code: service_code, live: true, site: nil, vendor_id: nil) }
    let!(:goa_rate) { create(:flat_rate, company: rescue_company, account: account, service_code: goa_service_code, live: true, site: nil, vendor_id: nil) }
    let(:job) { create(:fleet_managed_job, rescue_company: rescue_company, fleet_company: fleet_company, account: account, service_code: service_code) }

    it "returns the rate for the job service code" do
      expect(job.get_rate).to eq service_code_rate
    end

    it "returns the GOA rate if the job is GOA" do
      job.status = "GOA"
      expect(job.get_rate).to eq goa_rate
    end

    context "Rate Type" do
      let(:job) { create(:job, :with_invoice, :with_account) }

      it "uses the invoice's `rate_type` db column value -- not the `invoice#rate_type` method -- \
          to determine if the rate type was overriden by the user" do
        allow(job.invoice).to receive(:attributes).and_return(job.invoice.attributes)

        expect(job.invoice).to receive(:attributes)
        expect(job.invoice).not_to receive(:rate_type)

        job.get_rate
      end

      context "The user has overridden the invoice's rate type" do
        before { job.invoice.update(rate_type: 'HoursP2PRate') }

        it "returns a rate that's the same type as the user override" do
          expect(job.invoice.rate_type).to eq('HoursP2PRate')
          expect(job.get_rate.type).to eq('HoursP2PRate')
        end
      end

      context "The user has not overridden the invoice's rate type" do
        it "returns the best rate for this job's service code, regardless of its rate type" do
          expect(job.invoice.rate_type).to be_nil
          expect(job.get_rate.type).to eq('FlatRate') # Since there are no rates set up in this test, we'll get a default FlatRate
        end
      end
    end
  end

  describe "update_invoice" do
    it "calls CreateOrUpdateInvoice synchronously with the job id" do
      job = Job.new
      job.id = 1
      worker = CreateOrUpdateInvoice.new
      expect(CreateOrUpdateInvoice).to receive(:new).and_return(worker)
      expect(worker).to receive(:perform).with(job.id)
      job.update_invoice
    end
  end

  describe "mark_invoice_goa" do
    let(:service_code) { create(:service_code) }
    let(:goa_service_code) { create(:service_code, name: "GOA") }
    let(:rescue_company) { create(:rescue_company) }
    let(:fleet_company) { create(:fleet_company) }
    let(:account) { create(:account, company: rescue_company) }
    let!(:default_rate) { create(:flat_rate, company: rescue_company, account: account, service_code: nil, live: true, site: nil, vendor_id: nil) }
    let!(:service_code_rate) { create(:flat_rate, company: rescue_company, account: account, service_code: service_code, live: true, site: nil, vendor_id: nil) }
    let!(:goa_rate) { create(:flat_rate, company: rescue_company, account: account, service_code: goa_service_code, live: true, site: nil, vendor_id: nil) }
    let(:job) { create(:fleet_managed_job, status: "Dispatched", rescue_company: rescue_company, fleet_company: fleet_company, account: account, service_code: service_code) }

    it "cannot be called if the job is not GOA" do
      expect { job.mark_invoice_goa }.to raise_error(Job::IllegalStateError)
    end

    it "creates a new GOA invoice if the invoice does not already exist" do
      job.update_columns(status: "GOA")
      expect(job.invoice).to eq nil
      job.mark_invoice_goa
      job.reload
      expect(job.invoice.line_items.size).to eq 2
      goa_item = job.invoice.line_items.reject(&:tax?).first
      expect(goa_item.rate).to eq goa_rate
    end

    it "deletes the existing line items and replaces them with a GOA invoice" do
      CreateOrUpdateInvoice.new.perform(job.id)
      job.reload.invoice
      expect(job.invoice.line_items.size).to eq 2
      job.update_columns(status: "GOA")
      job.mark_invoice_goa
      job.reload
      expect(job.invoice.line_items.size).to eq 2
      goa_item = job.invoice.line_items.reject(&:tax?).first
      expect(goa_item.rate).to eq goa_rate
    end

    it "does not remove line items if it is already a GOA invoice" do
      job.update_columns(status: "GOA")
      CreateOrUpdateInvoice.new.perform(job.id)
      job.reload
      create(:invoicing_line_item, :flat_rate, :estimated, ledger_item: job.invoice, unit_price: 60)
      expect(job.invoice.line_items.size).to eq 3
      job.mark_invoice_goa
      job.reload
      expect(job.invoice.line_items.size).to eq 3
      goa_item = job.invoice.line_items.detect { |item| item.rate&.goa? }
      expect(goa_item.rate).to eq goa_rate
    end

    it "cancels any existing fleet customer live invoices" do
      job.update_columns(status: "GOA")
      expect(job).to receive(:fleet_customer_live_invoices).and_return([:mock])
      cancel_service = InvoiceService::Cancel::CancelCustomerInvoice.new(job)
      expect(InvoiceService::Cancel::CancelCustomerInvoice).to receive(:new).with(job).and_return(cancel_service)
      expect(cancel_service).to receive(:call)
      job.mark_invoice_goa
    end
  end

  describe '#subscribers' do
    subject { job.subscribers }

    let(:rescue_company) { create :rescue_company }
    let(:job) { create :rescue_job, :with_rescue_driver, rescue_company: rescue_company }

    context "dispatchers" do
      let!(:dispatchers) { create_list :user, 3, :dispatcher, :rescue, company: rescue_company }

      it "works" do
        expect(subject).to eq(Set.new([rescue_company, *dispatchers, job.rescue_driver, job.customer]))
      end
    end

    context "oauth_application" do
      let!(:oauth_application) { create :oauth_application, owner: rescue_company }

      it "works" do
        expect(subject).to eq(Set.new([rescue_company, oauth_application, job.rescue_driver, job.customer]))
      end
    end
  end

  describe "#system_alert_swoop_if_necessary" do
    let(:job) { create(:fleet_managed_job) }

    before do
      allow(Job).to receive(:delay).and_return(FleetManagedJob)
    end

    context 'when can_system_alert_swoop? is true' do
      before do
        allow_any_instance_of(Job).to receive(:can_system_alert_swoop?).and_return(true)
      end

      context 'and there are swoop users to with alerts activated' do
        let(:swoop_user) { create(:swoopuser, phone: Faker::PhoneNumber.phone_number_e164) }

        before do
          allow_any_instance_of(SystemEmailRecipientCalculator).to receive(:calculate).and_return([swoop_user])
        end

        it 'calls send_sms with expected params' do
          expect(FleetManagedJob).to receive(:send_sms).with(
            swoop_user.phone,
            "Job Created  - #{job.swoop_summary_name}",
            user_id: swoop_user.id,
            job_id: job.id,
            type: 'JobAuditSms',
          )

          job.system_alert_swoop_if_necessary
        end

        it 'calls new_job_call_swoop_agents with expected params' do
          expect(FleetManagedJob).to receive(:new_job_call_swoop_agents).with(swoop_user.phone, job.id)

          job.system_alert_swoop_if_necessary
        end
      end
    end
  end

  describe "AssignRescueDriverVehicleToJobWorker" do
    let(:rescue_company) { create :rescue_company }
    let(:rescue_driver) { create :user, :rescue_driver, company: rescue_company }
    let!(:job) { create :job, status: :enroute, rescue_driver: nil, rescue_company: rescue_company }

    context "when rescue_driver is changed" do
      subject { job.update! rescue_driver: rescue_driver }

      it "enqueues a job" do
        subject
        expect(AssignRescueDriverVehicleToJobWorker).to have_enqueued_sidekiq_job(job.id)
      end
    end

    context "when something else is changed" do
      subject { job.update! status: :dispatched }

      it "doesn't enqueue a job" do
        subject
        expect(AssignRescueDriverVehicleToJobWorker).not_to have_enqueued_sidekiq_job(job.id)
      end
    end
  end

  describe 'Job#add_sta(sta)' do
    subject(:add_sta_to_job) { job.add_sta(20) }

    let(:job) { create(:job) }
    let(:request_context) { RequestContext.new(uuid: "abcd", user_id: 1, source_application_id: 2) }

    before do
      allow(RequestContext).to receive(:current).and_return(request_context)
    end

    it 'assigns the source_application correctly' do
      add_sta_to_job

      expect(job.sta.first.source_application_id).to eq 2
    end
  end

  describe 'Job#add_eta(sta)' do
    subject(:add_eta_to_job) { job.add_eta(20) }

    let(:job) { create(:job) }
    let(:request_context) { RequestContext.new(uuid: "abcd", user_id: 1, source_application_id: 2) }

    before do
      allow(RequestContext).to receive(:current).and_return(request_context)
    end

    it 'assigns the source_application correctly' do
      add_eta_to_job

      expect(job.eta.first.source_application_id).to eq 2
    end
  end

  describe 'Job#add_fta(sta)' do
    subject(:add_fta_to_job) { job.add_fta(20) }

    let(:job) { create(:job) }
    let(:request_context) { RequestContext.new(uuid: "abcd", user_id: 1, source_application_id: 2) }

    before do
      allow(RequestContext).to receive(:current).and_return(request_context)
    end

    it 'assigns the source_application correctly' do
      add_fta_to_job

      expect(job.fta.first.source_application_id).to eq 2
    end
  end

  describe 'Job#bid(sta)' do
    subject(:bid_job) { job.bid(20) }

    let(:job) { create(:job) }
    let(:request_context) { RequestContext.new(uuid: "abcd", user_id: 1, source_application_id: 2) }

    before do
      allow(RequestContext).to receive(:current).and_return(request_context)
    end

    it 'assigns the source_application correctly' do
      expect(bid_job.source_application_id).to eq 2
    end
  end
end
