# frozen_string_literal: true

require "rails_helper"

describe(SlackChannel, type: :model) do
  it "matches appropriate tags" do
    sc = create(:slack_channel, tag: "blah-blah", excludes: "nothing")
    channel = SlackChannel.for_tags("blah-blah").first
    expect(sc.id).to(eq(channel.id))
  end

  it "excludeds matching tags in the exclude" do
    create(:slack_channel, tag: "blah-blah", excludes: "blah%blah")
    channel = SlackChannel.for_tags("blah-blah").first
    expect(channel).to(be_nil)
  end

  it "doesn't exclude channels if exclude is nil" do
    sc = create(:slack_channel, tag: "blah-blah", excludes: nil)
    channel = SlackChannel.for_tags("blah-blah").first
    expect(sc.id).to(eq(channel.id))
  end
end
