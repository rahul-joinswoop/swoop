# frozen_string_literal: true

require "rails_helper"

# Create testing times. Defined outside
ZONE = Site::DEFAULT_TIME_ZONE
def localtime(day, hour, minute)
  Date.parse(day).in_time_zone(ZONE).change(hour: hour, minute: minute)
end

describe Site do
  let(:site) { create(:site, always_open: false, force_open: false) }

  # Pass in UTC time, otherwise postgres/rails causes problems
  let(:seven_am) { Time.utc(2018, 6, 13, 7, 0) }
  let(:nine_am) { Time.utc(2018, 6, 13, 9, 0) }
  let(:noon) { Time.utc(2018, 6, 13, 12, 0) }
  let(:five_pm) { Time.utc(2018, 6, 13, 17, 0) }
  let(:six_pm) { Time.utc(2018, 6, 13, 18, 0) }
  let(:midnight) { Time.utc(2018, 6, 14, 0, 0) }
  let(:one_am) { Time.utc(2018, 6, 14, 1, 0) }
  let(:two_am) { Time.utc(2018, 6, 14, 2, 0) }

  let(:weekdays_only) { create(:partner_site, always_open: false, force_open: false, open_time: nine_am, close_time: five_pm) }
  let(:saturday_only) { create(:partner_site, always_open: false, force_open: false, open_time_sat: nine_am, close_time_sat: five_pm) }
  let(:sunday_only) { create(:partner_site, always_open: false, force_open: false, open_time_sun: nine_am, close_time_sun: five_pm) }
  let(:always_open_site) { create(:partner_site, always_open: true) }

  let(:across_days) do
    build(
      :partner_site,
      always_open: false,
      force_open: false,
      open_time: nine_am,
      close_time: two_am,
      open_time_sat: seven_am,
      close_time_sat: midnight,
      open_time_sun: noon,
      close_time_sun: one_am
    )
  end

  it "creates site" do
    expect(site).to be_valid
  end

  it "parses times correctly" do
    # Personal sanity check b/c this is how the time gets passed in from frontend
    site = create(:partner_site, open_time: "13:37")

    open_time = Site.find_by(id: site.id).open_time
    expect(open_time.hour).to eq 13
    expect(open_time.min).to eq 37
  end

  describe "#open_for_business" do
    it "takes into account force_open", freeze_time: localtime("sunday", 12, 0) do
      expect(weekdays_only.open_for_business?).to eq false
      weekdays_only.force_open = true
      expect(weekdays_only.open_for_business?).to eq true
    end
  end

  describe "#open_right_now?" do
    it "checks the correct days", freeze_time: localtime("sunday", 12, 0) do
      expect(saturday_only.open_right_now?).to eq false
      expect(sunday_only.open_right_now?).to eq true
    end

    it "handles 24/7 businesses" do
      expect(always_open_site.open_right_now?).to eq true
      expect(site.open_right_now?).to eq false
    end

    it "is false if there is no close or open time" do
      expect(site.open_right_now?).to eq false
    end

    it "is false for time outside of range", freeze_time: localtime("tuesday", 18, 0) do
      expect(weekdays_only.open_right_now?).to eq false
    end

    it "is upper bound exclusive", freeze_time: localtime("tuesday", 17, 0) do
      expect(weekdays_only.open_right_now?).to eq false
    end

    it "is lower bound inclusive", freeze_time: localtime("tuesday", 9, 0) do
      expect(weekdays_only.open_right_now?).to eq true
    end

    it "is true for time in range", freeze_time: localtime("tuesday", 12, 0) do
      expect(weekdays_only.open_right_now?).to eq true
    end

    context "when the company closes after midnight" do
      # these example primarily relies on how here we marked sunday as being open until 1am (on monday)
      it "properly checks closing at midnight", freeze_time: localtime("saturday", 12, 0) do
        expect(across_days.open_right_now?).to eq true
      end

      it "is false if outside of wrapped range times", freeze_time: localtime("monday", 1, 30) do
        expect(across_days.open_right_now?).to eq false
      end

      it "is true if inside of wrapped range times", freeze_time: localtime("monday", 0, 30) do
        expect(across_days.open_right_now?).to eq true
      end
    end
  end

  describe "#compute_next_open_check!", freeze_time: localtime("friday", 12, 0) do
    it "sets next check to next closest time" do
      saturday_only.compute_next_open_check!
      weekdays_only.compute_next_open_check!

      expect(saturday_only.next_open_check).to eq localtime("saturday", 9, 0)
      expect(weekdays_only.next_open_check).to eq localtime("friday", 17, 0)
    end

    it "defaults the check to one day from now" do
      site.compute_next_open_check!

      expect(site.next_open_check).to eq 1.day.from_now
    end

    it "nulls the check time on always open companies" do
      always_open_site.next_open_check = Time.current
      always_open_site.compute_next_open_check!

      expect(site.next_open_check).to be_nil
    end
  end

  context "after a company has time fields updated" do
    it "forces a new check", freeze_time: localtime("tuesday", 12, 0) do
      weekdays_only.compute_next_open_check!
      expect(weekdays_only.next_open_check).not_to be_nil

      weekdays_only.close_time = six_pm
      weekdays_only.save!

      expect(weekdays_only.next_open_check).to be_nil

      Site::OpenChecker.update_open_businesses!

      expect(weekdays_only.reload.next_open_check).not_to be_nil
    end
  end

  describe "providerCompanyNames" do
    it "returns a list of super companies, fleet in house companies, and motor clubs for the site" do
      site = create(:site)

      swoop = create(:super_company)
      fleet_company = create(:fleet_company)
      motor_club = create(:fleet_company, :motor_club, issc_client_id: "AGO")
      fleet_in_house = create(:fleet_in_house_company)

      create(:rescue_provider, site: site, company: fleet_company)
      create(:rescue_provider, site: site, company: motor_club)
      create(:rescue_provider, site: site, company: swoop)
      create(:rescue_provider, site: site, company: fleet_in_house)

      expect(site.providerCompanyNames).to match_array([swoop, motor_club, fleet_in_house])
    end
  end

  describe "update dependent indexes" do
    it "updates dependent indexes when the name changes" do
      site = create(:site)
      SearchIndex::SiteDependentIndexUpdateWorker.jobs.clear

      site.update!(updated_at: Time.current)
      expect(SearchIndex::SiteDependentIndexUpdateWorker.jobs.size).to eq 0

      site.update!(name: "New Name")
      expect(SearchIndex::SiteDependentIndexUpdateWorker).to have_enqueued_sidekiq_job(site.id)
    end
  end
end
