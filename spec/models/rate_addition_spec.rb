# frozen_string_literal: true

require "rails_helper"

describe RateAddition do
  subject(:rate) { build_stubbed :rate, currency: 'USD' }

  describe '#create' do
    it 'requires currency match currency on rate' do
      rate_addition = RateAddition.new(rate: rate, name: 'MyLittleAddition', currency: 'EUR')
      expect { rate_addition.save! }
        .to raise_error(ActiveRecord::RecordInvalid, /Currency must match Rate/)
    end
  end
end
