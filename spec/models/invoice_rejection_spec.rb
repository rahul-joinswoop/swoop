# frozen_string_literal: true

require "rails_helper"

describe InvoiceRejection, type: :model do
  describe "associations" do
    it { is_expected.to belong_to :invoice_reject_reason }
    it { is_expected.to belong_to :invoice }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of :invoice_reject_reason }
    it { is_expected.to validate_presence_of :invoice }
  end

  describe "#reject_reason" do
    subject(:reject_reason) { invoice_rejection.reject_reason }

    let(:invoice_reject_reason) do
      build_stubbed :invoice_reject_reason, text: "Unapproved additional items"
    end
    let(:invoice_rejection) do
      build_stubbed :invoice_rejection,
                    invoice_reject_reason: invoice_reject_reason
    end

    it "returns the text of the associated #invoice_reject_reason" do
      expect(subject).to eql "Unapproved additional items"
    end
  end
end
