# frozen_string_literal: true

require "rails_helper"

describe AuditSms do
  describe '#ui_label' do
    subject { audit_sms.ui_label(viewing_company: nil) }

    let(:audit_sms) { create(:audit_sms, source_application: source_application) }

    context 'when source_application is set' do
      let(:source_application) do
        SourceApplication.create!(platform: SourceApplication::PLATFORM_SMS, source: SourceApplication::SOURCE_SWOOP)
      end

      it { is_expected.to eq 'Customer • SMS' }
    end

    context 'when source_application is not set' do
      let(:source_application) { nil }

      it { is_expected.to eq '' }
    end
  end

  describe '#name' do
    subject { audit_sms.name }

    let(:audit_sms) { create(:audit_sms, status: 'sent') }

    context 'when #reply_human_status is nil' do
      it { is_expected.to eq "#{AuditSms.description} #{audit_sms.human_status}" }
    end

    context 'when #reply_human_status is present' do
      before do
        allow(audit_sms).to receive(:reply_human_status).and_return('Customer Confirmed Complete')
      end

      it { is_expected.to eq 'Customer Confirmed Complete' }
    end
  end

  describe '#ui_type' do
    subject { audit_sms.ui_type }

    let!(:audit_sms) { create(:audit_sms) }

    context 'when #reply_human_status is nil' do
      it { is_expected.to eq 'SMS' }
    end

    context 'when #reply_human_status is present' do
      before do
        allow(audit_sms).to receive(:reply_human_status).and_return('Customer Confirmed Complete')
      end

      it { is_expected.to eq 'SMSReply' }
    end
  end

  describe '#adjusted_dttm' do
    subject { audit_sms.adjusted_dttm }

    let!(:audit_sms) { create(:audit_sms, response_received_at: response_received_at) }

    context 'when #reply_human_status is nil' do
      let(:response_received_at) { nil }

      it { is_expected.to eq audit_sms.sent_date }
    end

    context 'when #reply_human_status is present' do
      let(:response_received_at) { Time.now + 1.day }

      before do
        allow(audit_sms).to receive(:reply_human_status).and_return('Customer Confirmed Complete')
      end

      it { is_expected.to eq audit_sms.response_received_at }
    end
  end

  describe '#last_set_dttm' do
    subject { audit_sms.last_set_dttm }

    let!(:audit_sms) { create(:audit_sms) }

    it { is_expected.to eq audit_sms.created_at }
  end
end
