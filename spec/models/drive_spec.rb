# frozen_string_literal: true

require "rails_helper"

describe Drive do
  let(:drive) { create(:drive) }

  it "creates drive" do
    expect(drive).to be_valid
  end

  describe "description" do
    it "is the user name if no vehicle" do
      user = User.new(first_name: "Marcus", last_name: "Antonius")
      drive = Drive.new(user: user)
      expect(drive.description).to eq "Marcus Antonius"
    end

    it "is the user name and vehicle description" do
      user = User.new(first_name: "Marcus", last_name: "Antonius")
      vehicle = Vehicle.new(make: "Ford", model: "Focus", year: 2017, color: "red")
      drive = Drive.new(user: user, vehicle: vehicle)
      expect(drive.description).to eq "#{user.full_name} #{vehicle.description}"
    end

    it "is nil description without a user" do
      vehicle = Vehicle.new(make: "Ford", model: "Focus", year: 2017, color: "red")
      drive = Drive.new(vehicle: vehicle)
      expect(drive.description).to eq nil
    end
  end
end
