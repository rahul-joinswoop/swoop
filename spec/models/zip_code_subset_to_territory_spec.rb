# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ZipCodeSubsetToTerritory, type: :model do
  context 'fields' do
    it { is_expected.to respond_to(:polygon_area) }
    it { is_expected.to respond_to(:created_at) }
    it { is_expected.to respond_to(:updated_at) }
  end

  context 'zip_code_territory storage' do
    context 'positive cases' do
      context 'create' do
        let(:polygon_area) { 'POLYGON((-91.23046875 45.460130637921, -79.8046875 49.837982453085, -88.2421875 32.694865977875, -91.23046875 45.460130637921))' }
        let!(:zip_code_territory) { ZipCodeSubsetToTerritory.create(zip: "03062", polygon_area: polygon_area) }

        it 'loads the polygon_area into a model' do
          expect(zip_code_territory.polygon_area).to be_present
        end

        it 'shows that postgis adds a terminal point that closes the polygon into a solid shape' do
          first_point = zip_code_territory.coordinates.first
          last_point = zip_code_territory.coordinates.last
          expect(zip_code_territory.coordinates.count).to eq(4)
          expect(first_point).to eq(last_point)
        end
      end
    end

    context 'negative cases' do
      context 'create' do
        context 'the polygon_area is missing' do
          let!(:zip_code_territory) { ZipCodeSubsetToTerritory.create(zip: "03062", polygon_area: nil) }

          it { is_expected.not_to be_valid }
        end

        context 'the zip_code is missing' do
          let(:polygon_area) { 'POLYGON((-91.23046875 45.460130637921, -79.8046875 49.837982453085, -88.2421875 32.694865977875, -91.23046875 45.460130637921))' }
          let!(:zip_code_territory) { ZipCodeSubsetToTerritory.create(zip: nil, polygon_area: polygon_area) }

          it { is_expected.not_to be_valid }
        end
      end
    end
  end
end
