# frozen_string_literal: true

require 'rails_helper'

describe RescueProvider do
  subject(:rescue_provider) { build :rescue_provider }

  let(:rescue_company) { build :rescue_company }

  describe '#name' do
    before :each do
      allow_any_instance_of(described_class).to(
        receive(:provider)
      ).and_return(rescue_company)
      allow_any_instance_of(described_class).to(
        receive(:site)
      ).and_return(double)
      allow_any_instance_of(RescueCompany).to(
        receive(:customized_name_having)
      ).with(rescue_provider.site)
    end

    it 'calls RescueCompany#customized_name_having' do
      expect_any_instance_of(RescueCompany).to(
        receive(:customized_name_having)
      ).with(rescue_provider.site)

      rescue_provider.name
    end
  end

  describe "ElasticSearch" do
    it "has a scope to preload associations for ElasticSearch" do
      expect(Company.elastic_search_preload.to_a).to be_a(Array)
    end
  end

  describe "auto_assign_distance_miles" do
    it "returns the default if the distance has not been assigned" do
      provider = RescueProvider.new
      expect(provider.auto_assign_distance_miles).to eq RescueProvider::DEFAULT_AUTO_ASSIGN_DISTANCE_MILES
    end

    it "returns the auto_assign_distance if has been assigned" do
      provider = RescueProvider.new(auto_assign_distance: 5)
      expect(provider.auto_assign_distance_miles).to eq 5
    end
  end

  describe "sort_name" do
    it "returns a lowercased version of the name" do
      expect(RescueProvider.new(provider: RescueCompany.new(name: "Joe's Towing"), site: Site.new(name: "HQ")).sort_name).to eq "joe's towing hq"
    end

    it "transliterates non-ASCII characters" do
      expect(RescueProvider.new(provider: RescueCompany.new(name: "Über Towing"), site: Site.new(name: "HQ")).sort_name).to eq "uber towing hq"
    end
  end
end
