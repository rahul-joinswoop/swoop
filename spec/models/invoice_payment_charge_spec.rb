# frozen_string_literal: true

require 'rails_helper'

describe InvoicePaymentCharge do
  describe '#create' do
    let(:invoice) { build_stubbed(:invoice, currency: 'USD') }

    it 'must match invoice currency' do
      invoice_payment_charge = InvoicePaymentCharge.new(currency: 'EUR', invoice: invoice)
      expect { invoice_payment_charge.save! }
        .to raise_error(ActiveRecord::RecordInvalid, /Currency must match Invoice/)
    end
  end
end
