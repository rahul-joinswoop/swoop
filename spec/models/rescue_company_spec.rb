# frozen_string_literal: true

require 'rails_helper'

describe RescueCompany do
  subject(:rescue_company) do
    build :rescue_company, :finish_line, sites: rescue_company_sites, name: Faker::Company.name
  end

  let(:site_name) { 'West Towing Site' }
  let(:site) { build :partner_site, name: site_name }
  let(:rescue_company_sites) { build_list(:partner_site, 1) }

  context 'associations' do
    context 'territories' do
      it { is_expected.to have_many(:territories) }

      context 'soft deleted territories' do
        let(:rescue_company) { create :rescue_company }
        let!(:territory_1) { create :territory, company: rescue_company }
        let!(:territory_2) { create :territory, :soft_deleted, company: rescue_company }

        it 'are not included by default' do
          expect(rescue_company.territories.all.count).to eq(1)
          expect(rescue_company.territories.all.first).to eq(territory_1)
        end
      end
    end
  end

  context 'scopes' do
    let(:fleet_company) { create(:fleet_company) }

    # We have rescue company "A" with 4 territories
    let(:rescue_company_a) { create :rescue_company, fleet_managed_clients: [fleet_company], live: true }
    let!(:territory_for_rescue_company_a_that_contains_lat_lng) { create :territory, company: rescue_company_a, polygon_area: 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -10.5 -10.5))' }
    let!(:another_territory_for_rescue_company_a_that_contains_lat_lng) { create :territory, company: rescue_company_a, polygon_area: 'POLYGON ((-11.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -11.5 -10.5))' }
    let!(:territory_for_rescue_company_a_that_does_not_contain_lat_lng) { create :territory, company: rescue_company_a, polygon_area: 'POLYGON ((40 40, 40 50, 50 50, 50 40, 40 40))' }
    let!(:territory_covering_san_francisco_for_rescue_company_a_that_contains_lat_lng) { create(:territory, :san_francisco, company: rescue_company_a) }

    # We have rescue company "B" with 1 territory
    let(:rescue_company_b) { create :rescue_company, fleet_managed_clients: [fleet_company], live: true }
    let!(:territory_for_company_b_that_contains_lat_lng) { create :territory, company: rescue_company_b, polygon_area: 'POLYGON ((40 40, 40 50, 50 50, 50 40, 40 40))' }

    # We have rescue company "C" with 1 soft-deleted territory
    let(:rescue_company_with_soft_deleted_territory) { create :rescue_company, fleet_managed_clients: [fleet_company], live: true }
    let!(:territory_for_company_c_that_contains_lat_lng_but_is_deleted) { create :territory, :soft_deleted, company: rescue_company_with_soft_deleted_territory, polygon_area: 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5, -10.5 -10.5))' }

    context 'containing_lat_lng_in_its_territories' do
      subject { RescueCompany }

      it { is_expected.to respond_to(:containing_lat_lng_in_its_territories) }

      context 'calling the scope' do
        subject { RescueCompany.containing_lat_lng_in_its_territories(0, 0) }

        it 'returns rescue companies that contain the lat/lng, and are not deleted' do
          expect(subject.count).to eq(1)
          expect(subject).to include(rescue_company_a)
          expect(subject).not_to include(rescue_company_b)
          expect(subject).not_to include(rescue_company_with_soft_deleted_territory)
        end
      end
    end

    context 'bidding eligibility scopes' do
      # Step 1: We have a service, 'Accident Tow'
      let(:accident_tow_service_code) { ServiceCode.find_by(name: "Accident Tow") }

      # Step 2.1: Company 'A' can do 'Accident Tow' service jobs and has 'Swoop' account
      let!(:accident_tow_service_code_for_rescue_company_a) do
        create(:companies_service, company: rescue_company_a, service_code: accident_tow_service_code)
      end
      let!(:swoop_account_for_rescue_company_a) { create(:account, company: rescue_company_a, client_company: Company.swoop) }

      # Step 2.2: Company 'B' cannot do 'Accident Tow' service jobs but has 'Swoop' account
      let!(:swoop_account_for_rescue_company_b) { create(:account, company: rescue_company_b, client_company: Company.swoop) }

      # Step 2.3: Company 'C' can do 'Accident Tow' service jobs and doesn't has 'Swoop' account
      let!(:accident_tow_service_code_for_rescue_company_c) do
        create(:companies_service, company: rescue_company_with_soft_deleted_territory, service_code: accident_tow_service_code)
      end

      # Step 3: A fleet managed job requesting 'Accident Tow' service
      let(:job) { create(:fleet_managed_job, :in_san_francisco, rescue_company: nil, service_code: accident_tow_service_code, fleet_company: fleet_company) }

      context 'eligible_to_bid_on_job' do
        subject(:eligible_to_bid_on_job) { RescueCompany.eligible_to_bid_on_job(job) }

        it "returns all the eligible rescue companies for bidding" do
          expect(eligible_to_bid_on_job.count).to eq(1)
          expect(eligible_to_bid_on_job.to_a).to eq([rescue_company_a])
        end
      end
    end
  end

  describe ".defaults" do
    subject { rescue_company.defaults }

    let(:rescue_company) { create :rescue_company }
    let(:company_features) { rescue_company.features.map(&:name) }
    let(:company_reports) { rescue_company.reports.map(&:name) }
    let(:company_customer_types) { rescue_company.customer_types.map(&:name) }
    let(:company_location_types) { rescue_company.location_types.map(&:name) }

    before do
      RescueCompany::DEFAULT_FEATURES_FOR_NEW_COMPANY.each do |feature|
        create(:feature, name: feature)
      end

      RescueCompany::DEFAULT_REPORTS_FOR_NEW_COMPANY.each do |report|
        create(:report, name: report)
      end

      RescueCompany::DEFAULT_CUSTOMER_TYPES_FOR_NEW_COMPANY.each do |customer_type|
        create(:customer_type, name: customer_type)
      end

      RescueCompany::DEFAULT_LOCATION_TYPES_FOR_NEW_COMPANY.each do |location_type|
        create(:location_type, name: location_type)
      end

      subject
    end

    it "adds the default objects" do
      expect(company_features).to include(*RescueCompany::DEFAULT_FEATURES_FOR_NEW_COMPANY)
      expect(company_reports).to include(*RescueCompany::DEFAULT_REPORTS_FOR_NEW_COMPANY)
      expect(company_customer_types).to include(*RescueCompany::DEFAULT_CUSTOMER_TYPES_FOR_NEW_COMPANY)
      expect(company_location_types).to include(*RescueCompany::DEFAULT_LOCATION_TYPES_FOR_NEW_COMPANY)
    end
  end

  describe '#customized_name_having' do
    it 'returns rescue_company.name with site.name added' do
      expect(rescue_company.customized_name_having(site)).to(
        eq "#{rescue_company.name} #{site.name}"
      )
    end

    context "when given site name is equals to #{Site::HQ}" do
      let(:site_name) { Site::HQ }

      context 'when RescueCompany has only one site' do
        it 'returns rescue_company.name' do
          expect(rescue_company.customized_name_having(site)).to(
            eq "#{rescue_company.name} #{site.name}"
          )
        end
      end

      context 'when RescueCompany has more than one site' do
        let(:rescue_company_sites) { build_list(:partner_site, 2) }

        it 'returns rescue_company.name with site.name added' do
          expect(rescue_company.customized_name_having(site)).to(
            eq "#{rescue_company.name} #{site.name}"
          )
        end
      end
    end

    context 'when site is nil' do
      let(:site) { nil }

      it 'returns self.name' do
        expect(rescue_company.customized_name_having(site)).to eq rescue_company.name
      end
    end
  end

  describe "associated_with_phone" do
    it "finds a company associated with a normalized phone number" do
      company_1 = create(:rescue_company, phone: "7085551212")
      create(:rescue_company, phone: "7085551414")
      expect(RescueCompany.associated_with_phone("708-555-1212")).to eq company_1
    end

    it "finds a company via a site associated with a normalized phone number" do
      company = create(:rescue_company, phone: "312-555-1212")
      company.sites << create(:partner_site, phone: "7085551212")
      expect(RescueCompany.associated_with_phone("708-555-1212")).to eq company
    end

    it "only find rescue companies" do
      create(:fleet_company, phone: "7085551212")
      expect(RescueCompany.associated_with_phone("708-555-1212")).to eq nil
    end
  end

  describe "#custom_account" do
    subject(:rescue_company_custom_account) { rescue_company.custom_account&.id }

    let!(:active_custom_account) do
      create(:custom_account, company: rescue_company, upstream_account_id: 'abcde')
    end

    let!(:deleted_custom_account) do
      create(
        :custom_account,
        company: rescue_company,
        upstream_account_id: 'yyz',
        deleted_at: Time.current
      )
    end

    let(:rescue_company) { create(:rescue_company) }

    context 'when it has an active and a deleted custom_account' do
      it { is_expected.to eq active_custom_account.id }
    end

    context 'when it only contains a deleted custom_account' do
      let!(:active_custom_account) { nil }

      it { is_expected.to be_blank }
    end
  end

  describe "hq" do
    let(:rescue_company) { create(:rescue_company) }

    it "returns the first site named HQ" do
      create(:site, company: rescue_company, name: "Storage")
      create(:site, company: rescue_company, name: "HQ", deleted_at: Time.now)
      site_3 = create(:site, company: rescue_company, name: "HQ")
      create(:site, company: rescue_company, name: "Secondary")
      expect(rescue_company.hq).to eq site_3
    end

    it "returns the first site created for the company if there isn't one named HQ" do
      create(:site, company: rescue_company, name: "Storage", deleted_at: Time.now)
      site_2 = create(:site, company: rescue_company, name: "Main Office")
      create(:site, company: rescue_company, name: "Secondary")
      expect(rescue_company.hq).to eq site_2
    end
  end

  describe "billing_type" do
    it "defaults to VCC" do
      expect(RescueCompany.new.billing_type).to eq BillingType::VCC
    end

    it "comes from the settings" do
      company = RescueCompany.new
      company.settings.build(key: Setting::BILLING_TYPE, value: BillingType::ACH)
      expect(company.billing_type).to eq BillingType::ACH
    end
  end

  describe '#available_user_notification_settings' do
    let(:company) { create(:rescue_company, name: 'Tow Company') }

    it 'returns the expected available_user_notification_settings' do
      expect(company.available_user_notification_settings).to eq(
        [
          {
            notification_code: 'new_job',
            description: 'New job assigned to Tow Company',
            supported_roles: ['admin', 'dispatcher'],
            available_notification_channels: ['Call', 'Text'],
            default_notification_channels: ['Text'],
          },
          {
            notification_code: 'eta_accepted',
            description: 'ETA submission accepted',
            supported_roles: ['admin', 'dispatcher'],
            available_notification_channels: ['Call', 'Text'],
            default_notification_channels: ['Text'],
          },
          {
            notification_code: 'dispatched_to_driver',
            description: 'Job dispatched to driver',
            supported_roles: ['driver'],
            available_notification_channels: ['Call', 'Text'],
            default_notification_channels: ['Text'],
          },
        ]
      )
    end
  end

  describe "build_territories_from_zip_codes" do
    let(:rescue_company) { create(:rescue_company) }
    let!(:zip_code_territory_1) { create :zip_code_subset_to_territory }
    let!(:zip_code_territory_2) { create :zip_code_subset_to_territory, :random_polygon_territory }
    let!(:zip_code_territory_with_different_zip_code) { create :zip_code_subset_to_territory, :random_zip_code }

    it "creates a territory for the rescue company using zip" do
      expect(rescue_company.territories.size).to eq(0)
      expect { rescue_company.build_territories_from_zip_codes(['03062']) }.not_to raise_error
      expect(rescue_company.territories.size).to eq(2)
    end
  end
end
