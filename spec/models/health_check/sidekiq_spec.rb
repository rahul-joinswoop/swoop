# frozen_string_literal: true

require "rails_helper"

describe HealthCheck::Sidekiq do
  subject { described_class.new }

  let(:additional_queue_size) { 0 }

  let(:sidekiq_queue) do
    [
      double("Queue", name: "A", count: 1),
      double("Queue", name: "B", count: 1),
    ]
  end

  before do
    allow(::Sidekiq::Queue).to receive(:all).and_return(sidekiq_queue)
  end

  describe "#to_json" do
    context "when showing queues" do
      subject { described_class.new(true).to_json }

      it "adds queues" do
        expected = [
          { name: "A", size: 1 },
          { name: "B", size: 1 },
        ]

        expect(subject[:queues]).to eq(expected)
      end
    end
  end

  describe "#status" do
    it "returns green when count below 500" do
      allow(subject).to receive(:count).and_return(499)
      expect(subject.status).to eq(:green)
    end

    it "returns yellow when count between 500 and 5000" do
      allow(subject).to receive(:count).and_return(500)
      expect(subject.status).to eq(:yellow)
      allow(subject).to receive(:count).and_return(4999)
      expect(subject.status).to eq(:yellow)
    end

    it "returns red when status above 5000" do
      allow(subject).to receive(:count).and_return(5000)
      expect(subject.status).to eq(:red)
    end
  end

  describe "#count" do
    it "correctly returns sidekiq queue counts" do
      expect(subject.count).to eq(2)
    end
  end
end
