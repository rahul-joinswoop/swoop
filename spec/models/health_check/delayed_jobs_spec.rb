# frozen_string_literal: true

require "rails_helper"

describe HealthCheck::DelayedJobs do
  subject { described_class.new }

  describe "#to_json" do
    context "with verbose mode" do
      subject { described_class.new(true).to_json }

      it "adds task ids" do
        expect(subject.keys).to include(:tasks)
      end
    end
  end

  describe "#status" do
    it "returns green when < 100 jobs" do
      allow(subject).to receive(:count).and_return(99)
      expect(subject.status).to eq(:green)
    end

    it "returns yellow when >= 100 jobs and less than 500 jobs" do
      allow(subject).to receive(:count).and_return(100)
      expect(subject.status).to eq(:yellow)

      allow(subject).to receive(:count).and_return(499)
      expect(subject.status).to eq(:yellow)
    end

    it "returns red when >= 500 jobs" do
      allow(subject).to receive(:count).and_return(500)
      expect(subject.status).to eq(:red)
    end
  end

  describe "#count" do
    before do
      Delayed::Job.enqueue EmailPasswordRequest.new(1)
      Delayed::Job.enqueue EmailPasswordRequest.new(2)
      Delayed::Job.enqueue EmailPasswordRequest.new(3),
                           run_at: 10.minutes.from_now
    end

    it "only returns number of jobs running in the next minute" do
      expect(subject.count).to eq(2)
    end
  end
end
