# frozen_string_literal: true

require "rails_helper"

describe RescueJob do
  describe "#partner_deleted" do
    subject do
      rescue_job.partner_deleted
      rescue_job.save!

      rescue_job.status
    end

    let(:rescue_job) do
      create(
        :rescue_job, status: job_status, rescue_company: company, fleet_company: company
      )
    end
    let(:company) { create(:rescue_company) }

    context "when job is completed" do
      let(:job_status) { Job::COMPLETED }

      it { is_expected.to eq 'deleted' }

      context 'when company has feature Disable Delete Job' do
        let(:feature_disable_delete_job) do
          create(:feature, name: 'Disable Delete Job')
        end

        before do
          company.features << feature_disable_delete_job
        end

        it 'raises error' do
          expect { subject }.to raise_error(AASM::InvalidTransition)
        end
      end
    end
  end
end
