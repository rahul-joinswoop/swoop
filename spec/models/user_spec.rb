# frozen_string_literal: true

require "rails_helper"

describe User do
  let(:user) { create(:user) }
  let(:swoopuser) { create(:swoopuser) }

  it "creates user" do
    expect(user).to be_valid
  end

  it "creates root user / swoop user" do
    expect(swoopuser).to be_valid
  end

  describe '.oauth_access_tokens' do
    subject { user.access_tokens }

    before do
      create(:doorkeeper_access_token, resource_owner_id: user.id)
    end

    it 'returns access tokens' do
      expect(subject.length).to eq(1)
    end
  end

  describe "#publish_updates" do
    subject { user.publish_updates }

    let(:vehicle) { create(:vehicle) }

    before do
      allow(Vehicle).to receive(:find).and_return(vehicle)
      allow(user).to receive(:previous_changes).and_return(previous_changes)
    end

    context "if state changed" do
      let(:previous_changes) { { vehicle_id: [vehicle.id] } }

      it "sends an 'update' request to vehicle" do
        expect(vehicle).to receive(:base_publish)
        subject
      end
    end

    context "if state HASN'T changed" do
      let(:previous_changes) { { vehicle_id: [] } }

      it "does nothing" do
        expect(vehicle).not_to receive(:base_publish)
        subject
      end
    end
  end

  describe '#language' do
    let(:user) { User.new }

    it 'defaults to Company#language' do
      user = User.new(company: Company.new(language: 'de'))
      user.save!
      expect(user.language).to eq('de')
    end

    it 'accepts English' do
      expect(user.valid?).to be true
      user.language = Company::ENGLISH
      expect(user.valid?).to be true
    end

    it 'accepts German' do
      expect(user.valid?).to be true
      user.language = Company::GERMAN
      expect(user.valid?).to be true
    end

    it 'doesn\'t permit Spanish' do
      expect(user.valid?).to be true
      user.language = 'es'
      expect(user.valid?).to be false
    end

    it 'empty string is overwritten' do
      user.language = ''
      expect(user.valid?).to be true
      expect(user.language).to eq(ENV['DEFAULT_LANGUAGE'])
    end
  end

  describe "#copy_user" do
    subject { user.copy_user }

    let(:company) { create(:company) }
    let(:user) do
      create(:user,
             phone: "1234567890",
             uuid: "first-user-uuid",
             company: company)
    end

    it "copies the user" do
      expect(subject.class).to be User
    end

    it "copies non-unique values" do
      expect(subject.full_name).to eq user.full_name
      expect(subject.company).to eq user.company
      expect(subject.phone).to eq user.phone
    end

    it "does NOT copy unique values" do
      expect(subject.id).not_to eq user.id
      expect(subject.uuid).not_to eq user.uuid
    end
  end

  describe "with Customer" do
    subject { user }

    let(:company) { create :company }
    let(:member_number) { Faker::Bank.iban }
    let(:customer) { build(:customer, company: company, member_number: member_number) }
    let(:user) { build(:user, customer: customer) }

    it "creates a new Customer record" do
      expect { subject.save! }.not_to raise_error
      expect(subject.member_number).to eq(member_number)
      expect(subject.customer_of_company).to eq(company)
      expect(company.customers).to include(subject.customer)
    end

    it "reuses an existing Customer record" do
      user2 = create :user, customer: create(:customer, company: company, member_number: member_number)
      expect { subject.save! }.not_to raise_error
      expect(user.customer).to eq(user2.customer)
    end
  end

  describe "ElasticSearch" do
    it "has a scope to preload associations for ElasticSearch" do
      expect(User.elastic_search_preload.to_a).to be_a(Array)
    end

    it "includes all phone numbers in the phone field" do
      user = create(:user, phone: "312-555-1212", pager: "414-555-1212")
      user.valid? # Force phone coercion
      doc = user.as_indexed_json
      expect(doc["phone"]).to match_array ["+13125551212", "+14145551212"]
    end
  end

  describe "phone_numbers" do
    it "includes the primary phone and pager/secondary phone as necessary" do
      user = User.new
      expect(user.phone_numbers).to eq []

      user = create(:user, phone: "312-555-1212")
      expect(user.phone_numbers).to eq ["+13125551212"]

      user = create(:user, pager: "414-555-1212")
      expect(user.phone_numbers).to eq ["+14145551212"]

      user = create(:user, phone: "312-555-1212", pager: "414-555-1212")
      expect(user.phone_numbers).to eq ["+13125551212", "+14145551212"]
    end
  end

  describe "#driver_only?" do
    subject { user.driver_only? }

    let(:user) { create(:user, roles: roles) }

    context "when user has no rescue or driver roles" do
      let(:roles) { ["root"] }

      it { is_expected.to be_falsey }
    end

    context "when user roles only contains rescue, driver" do
      let(:roles) { ["driver", "rescue"] }

      it { is_expected.to be_truthy }
    end

    context "when user roles contain rescue, driver and dispatcher" do
      let(:roles) { ["rescue", "driver", "dispatcher"] }

      it { is_expected.to be_falsey }
    end

    context "when user roles contain rescue, driver and admin" do
      let(:roles) { ["rescue", "driver", "admin"] }

      it { is_expected.to be_falsey }
    end
  end

  describe "sort_name" do
    it "returns a blank string if there is no name" do
      expect(User.new.sort_name).to eq ""
    end

    it "returns just a first name or last name if the other is missing" do
      expect(User.new(first_name: "Joe").sort_name).to eq "joe"
      expect(User.new(last_name: "Johnson").sort_name).to eq "johnson"
    end

    it "returns a lowercased version of last name first" do
      expect(User.new(first_name: "Joe", last_name: "Johnson").sort_name).to eq "johnson, joe"
    end

    it "transliterates non-ASCII characters" do
      expect(User.new(first_name: "José", last_name: "Cüervø").sort_name).to eq "cuervo, jose"
    end
  end

  describe "#perform_user_updated" do
    let!(:user) { create :user }

    it "works" do
      user.update! first_name: Faker::Name.first_name
      expect(GraphQL::UserUpdatedWorker).to have_enqueued_sidekiq_job(user.id)
    end
  end

  describe '#grantable_roles' do
    subject { user.grantable_roles }

    let(:user) { create(:user, :rescue) }

    it 'works' do
      expect(subject).to all(be_an(Role))
    end
  end

  describe '#encode' do
    context 'username' do
      subject { user.username_key }

      let(:user) { create(:user, :rescue) }

      it 'is encoded' do
        user_key = Digest::SHA256.hexdigest(user.username)
        expect(subject).to eq(user_key)
      end
    end

    context 'email' do
      subject { user.email_key }

      let(:user) { create(:user, :rescue) }

      it 'is encoded' do
        email_key = Digest::SHA256.hexdigest(user.email)
        expect(subject).to eq(email_key)
      end
    end
  end

  describe '#lookup' do
    subject { User.lookup_by_encoded_email_or_username('xxxx') }

    context 'checks username and email' do
      it 'checks' do
        expect(User).to receive(:find_by).with(username_key: "xxxx").and_return(nil)
        expect(User).to receive(:find_by).with(email_key: "xxxx").and_return(nil)
        subject
      end

      it 'doesnt check username if email is a match' do
        expect(User).to receive(:find_by).with(email_key: "xxxx").and_return(user)
        expect(User).not_to receive(:find_by).with(username_key: "xxxx")

        subject
      end
    end
  end

  describe '#has_roles?' do
    subject { user.has_roles?(*roles_to_check) }

    let(:user) { create :user, roles: [:rescue, :admin, :dispatcher] }

    context 'when user.roles contains all roles to check' do
      let(:roles_to_check) { [:rescue, :admin, :dispatcher] }

      it { is_expected.to be_truthy }
    end

    context 'when roles to check is a subset of user.roles' do
      let(:roles_to_check) { [:rescue, :admin] }

      it { is_expected.to be_truthy }
    end

    context 'when roles to check is another subset of user.roles' do
      let(:roles_to_check) { [:rescue, :dispatcher] }

      it { is_expected.to be_truthy }
    end

    context 'when user.roles does not contain one of the role to check' do
      let(:roles_to_check) { [:rescue, :dispatcher, :dont_exist_on_user] }

      it { is_expected.to be_falsey }
    end
  end

  describe "admin?" do
    it "returns true if the user has the admin role" do
      user = User.new
      expect(user.admin?).to eq false
      user.roles << Role.new(name: "dispatcher")
      expect(user.admin?).to eq false
      user.roles << Role.new(name: "admin")
      expect(user.admin?).to eq true
    end
  end

  describe "user_site?" do
    it "returns true if the user does not have a list of sites" do
      user = User.new
      site = create(:site)
      expect(user.user_site?(site)).to eq true
    end

    it "return true if the user's sites includes the site" do
      user = User.new
      site = create(:site)
      user.sites << site
      expect(user.user_site?(site)).to eq true
    end

    it "return false if the user's sites does not include the site" do
      user = User.new
      site = create(:site)
      user.sites << create(:site)
      expect(user.user_site?(site)).to eq false
    end
  end

  describe '#subscribers' do
    subject { user.subscribers }

    let(:user) { create :user, :rescue, :driver, company: company }
    let(:company) { create :rescue_company }

    context "dispatchers" do
      let!(:dispatchers) { create_list :user, 3, :rescue, :dispatcher, company: company }

      it "works" do
        expect(subject).to eq(Set.new([Company.swoop, user, company, *dispatchers]))
      end
    end

    context "oauth_application" do
      let!(:oauth_application) { create :oauth_application, owner: company }

      it "works" do
        expect(subject).to eq(Set.new([Company.swoop, user, company, oauth_application]))
      end
    end
  end

  describe "validations" do
    it "doesn't require name if no company_id" do
      expect(User.new).to have(0).errors_on(:last_name)
      expect(User.new).to have(0).errors_on(:first_name)
    end

    context 'when company_id is present' do
      it 'requires a name' do
        expect(User.new(company_id: 1)).to have(1).errors_on(:last_name)
        expect(User.new(company_id: 1)).to have(1).errors_on(:first_name)
      end

      it "requires last name if no first name" do
        expect(User.new(company_id: 1, first_name: '')).to have(1).errors_on(:last_name)
        expect(User.new(company_id: 1, first_name: '', last_name: 'Smith')).to have(0).errors_on(:last_name)
      end

      it "requires first name if no last name" do
        expect(User.new(company_id: 1, last_name: '')).to have(1).errors_on(:first_name)
        expect(User.new(company_id: 1, first_name: 'Will', last_name: '')).to have(0).errors_on(:fist_name)
      end

      it "requires a unique first and last name" do
        company = create(:rescue_company)
        user = create(:user, company: company, first_name: "Bill", last_name: "Smith")
        create(:user, company: company, first_name: nil, last_name: "Jones")
        create(:user, company: company, first_name: "Cher", last_name: nil)

        expect(User.new).to have(0).errors_on(:first_name)
        expect(User.new(first_name: "Bill", last_name: "Smith")).to have(0).errors_on(:first_name)
        expect(User.new(company_id: company.id, last_name: "Smith")).to have(0).errors_on(:first_name)
        expect(User.new(company_id: company.id, first_name: "Larry", last_name: "Smith")).to have(0).errors_on(:first_name)
        expect(User.new(company_id: company.id + 1, first_name: "Bill", last_name: "Smith")).to have(0).errors_on(:first_name)
        expect(User.new(company_id: company.id, first_name: "Bill", last_name: "Smith")).to have(1).errors_on(:first_name)
        # TODO when ci column name is flipped
        # expect(User.new(company_id: company.id, first_name: "bill", last_name: "SMITH")).to have(1).errors_on(:first_name)

        user.update!(deleted_at: Time.current)
        expect(User.new(company_id: company.id, first_name: "Bill", last_name: "Smith")).to have(0).errors_on(:first_name)

        expect(User.new(company_id: company.id, first_name: nil, last_name: "Jones")).to have(1).errors_on(:first_name)
        expect(User.new(company_id: company.id, first_name: "Cher", last_name: nil)).to have(1).errors_on(:first_name)
      end
    end
  end

  describe "AssignRescueDriverVehicleToJobWorker" do
    let(:rescue_company) { create :rescue_company }
    let(:rescue_vehicle) { create :rescue_vehicle, company: rescue_company }
    let(:user) { create :user, :rescue_driver, company: rescue_company }
    let!(:jobs) { create_list :job, 3, status: :enroute, rescue_driver: user, rescue_company: rescue_company }

    context "when rescue_vehicle is changed" do
      subject { user.update! vehicle: rescue_vehicle }

      it "enqueues workers" do
        subject

        expect(AssignRescueDriverVehicleToJobWorker).to have_enqueued_sidekiq_job(jobs.map(&:id))
      end
    end

    context "when something else is changed" do
      subject { user.update! email: Faker::Internet.email }

      it "does not enqueue workers" do
        subject

        expect(AssignRescueDriverVehicleToJobWorker).not_to have_enqueued_sidekiq_job(jobs.map(&:id))
      end
    end
  end
end
