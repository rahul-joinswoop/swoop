# frozen_string_literal: true

require "rails_helper"

describe ServiceCode do
  let(:service_code) { create(:service_code) }

  it "creates service_code" do
    expect(service_code).to be_valid
  end

  describe "Storage" do
    it "can find the storage service code" do
      create(:service_code, name: "Tow")
      storage = create(:service_code, name: "Storage")
      expect(ServiceCode.storage).to eq storage
    end
  end

  describe "GOA" do
    it "can find the storage service code" do
      tow = create(:service_code, name: "Tow")
      goa = create(:service_code, name: "GOA")
      expect(ServiceCode.goa).to eq goa
      expect(goa.goa?).to eq true
      expect(tow.goa?).to eq false
    end
  end
end
