# frozen_string_literal: true

require "rails_helper"

describe Review do
  let(:review) do
    create(:review, company: company, user: user, job: job)
  end

  let(:company) { create(:company, name: 'CompanyFoo') }
  let(:job) { create(:job, fleet_company: company) }
  let(:response_body) { nil }
  let(:twilio_reply) { double(:twilio_reply, body: response_body) }
  let(:user) { create(:user) }

  before do
    allow(review).to receive(:save!).and_return(true)
  end

  describe '#set_review_text' do
    context 'with a rescue job' do
      let(:job) { create(:rescue_job, rescue_company: company) }

      it 'sets company_brand_name to Job#rescue_company.name' do
        review.set_review_text
        expect(review.text).to eq("#{company.name}: Please reply with a number 1-5 (5 is best) to rate your service")
      end
    end

    context 'with a fleet motor club job' do
      let(:job) { create(:fleet_motor_club_job, rescue_company: company) }

      it 'sets company_brand_name to Job#rescue_company.name' do
        review.set_review_text
        expect(review.text).to eq("#{company.name}: Please reply with a number 1-5 (5 is best) to rate your service")
      end
    end

    context 'with a fleet job' do
      let(:job) { create(:fleet_in_house_job, fleet_company: company) }

      it 'sets company_brand_name to Job#fleet_company.name' do
        review.set_review_text
        expect(review.text).to eq("#{company.name}: Please reply with a number 1-5 (5 is best) to rate your service")
      end
    end
  end

  describe '.parse_rating' do
    it 'parses string into expected integer values' do
      expect(Review.parse_rating("that was 4 star service")).to eq(4)
      expect(Review.parse_rating("that was 1.4, start service")).to eq(1)
      expect(Review.parse_rating("that was 0.1 start service")).to eq(1)
      expect(Review.parse_rating("that was 9.5 start service")).to eq(5)
      expect(Review.parse_rating("10. awesome service", max: 10)).to eq(10)
    end
  end

  describe '#process_response' do
    before do
      allow(review).to receive(:rating).and_return(rating)
    end

    let(:rating) { nil }
    let(:response_body) { "5 excellent!" }

    context 'processing a twilio response' do
      describe 'without a rating' do
        it 'calls the appropriate method' do
          expect(review)
            .to receive(:process_first_response).with(twilio_reply)

          review.process_response(twilio_reply)
        end
      end

      describe 'with a rating' do
        let(:rating) { 5 }

        it 'calls the appropriate method and sets the nps_feedback field' do
          expect(review)
            .to receive(:process_thank_you_for_feedback).with(twilio_reply)

          review.process_response(twilio_reply)
        end
      end
    end

    context 'with a parsable rating value' do
      let(:response_body) { '5, awesome service' }

      it 'sets the nps_question1 to the value of rating' do
        expect { review.process_response(twilio_reply) }
          .to change(review, :nps_question1).from(nil).to(5)
      end
    end

    context 'with an unparsable rating value' do
      let(:response_body) { 'Goobldey Goop nope!' }

      it 'does not set nps_question1 to the value of rating' do
        review.process_response(twilio_reply)
        expect(review.nps_question1).to be_nil
      end
    end
  end
end
