# frozen_string_literal: true

require 'rails_helper'

describe PartnerSite do
  let(:site) { create(:partner_site) }

  describe "rescue_provider_for_company" do
    it "identifies the rescue provider matching a fleet company" do
      fleet_company_1 = create(:fleet_company)
      fleet_company_2 = create(:fleet_company)
      rescue_provider_1 = create(:rescue_provider, site: site, company: fleet_company_1)
      rescue_provider_2 = create(:rescue_provider, site: site, company: fleet_company_2)
      expect(site.rescue_provider_for_company(fleet_company_1)).to eq rescue_provider_1
      expect(site.rescue_provider_for_company(fleet_company_2)).to eq rescue_provider_2
      expect(site.rescue_provider_for_company(FleetCompany.new)).to eq nil
    end
  end

  describe "isscs_by_fleet" do
    it "filters isscs by a fleet company client id" do
      fleet_company = create(:fleet_company, issc_client_id: "AGO")
      create(:fleet_company, issc_client_id: "GCO")
      expect(site.isscs_by_fleet(fleet_company)).to eq []

      issc = create(:ago_issc, site: site, clientid: "AGO", contractorid: "123")
      create(:gco_issc, site: site, clientid: "GCO", contractorid: "456")
      site.reload
      expect(site.isscs_by_fleet(fleet_company)).to eq [issc]
    end
  end

  describe "contractorids" do
    it "returns contractorids for a fleet company" do
      fleet_company = create(:fleet_company, issc_client_id: "AGO")
      create(:fleet_company, issc_client_id: "GCO")
      expect(site.isscs_by_fleet(fleet_company)).to eq []

      issc = create(:ago_issc, site: site, clientid: "AGO", contractorid: "123")
      create(:gco_issc, site: site, clientid: "GCO", contractorid: "456")
      site.reload
      expect(site.contractorids(fleet_company)).to eq [issc.contractorid]
    end
  end

  describe "#delete_rescue_providers" do
    let(:active_rescue_providers) { site.rescue_providers.where(deleted_at: nil) }

    it "deletes the rescue providers when the site is deleted" do
      expect(site).to receive(:delete_rescue_providers)
      site.update! deleted_at: Time.now
    end

    it "doesn't delete the rescue providers when the site is updated, but not deleted" do
      expect(site).not_to receive(:delete_rescue_providers)
      site.update! name: 'Test Site'
    end

    context 'callbacks' do
      before do
        fleet_company_1 = create(:fleet_company)
        fleet_company_2 = create(:fleet_company)
        @rescue_provider1 = create(:rescue_provider, site: site, company: fleet_company_1)
        @rescue_provider2 = create(:rescue_provider, site: site, company: fleet_company_2)
      end

      it "deletes association with rescue providers" do
        expect(active_rescue_providers.count).to eq(2)
        site.update! deleted_at: Time.now
        expect(active_rescue_providers.count).to eq(0)
      end

      it "enqueue reindexing job for each rescue provider deleted" do
        expect(Indexer).to receive(:perform_async).with(:index, RescueProvider.name, @rescue_provider1.id)
        expect(Indexer).to receive(:perform_async).with(:index, RescueProvider.name, @rescue_provider2.id)
        site.update! deleted_at: Time.now
      end
    end
  end
end
