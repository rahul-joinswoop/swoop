# frozen_string_literal: true

require "rails_helper"

describe Survey::Question do
  describe ".in_order" do
    let!(:survey) { create(:survey) }

    before do
      survey.questions << create(:survey_question, survey: survey, order: 2)
      survey.questions << create(:survey_question, survey: survey, order: 1)
    end

    it "puts survey questions in order" do
      natural_first_question = survey.questions.first
      expect(natural_first_question.order).to eq(2)

      in_order_first_question = survey.questions.in_order.first
      expect(in_order_first_question.order).to eq(1)
    end
  end
end
