# frozen_string_literal: true

require "rails_helper"

describe ModelType do
  # this will blow up if we're missing a ModelType in the db
  describe "#check!" do
    # if you get a failure here follow the instructions in the error!
    it "works" do
      ModelType.check!
    end
  end
end
