# frozen_string_literal: true

require "rails_helper"

describe ApplicationRecord do
  describe "execute_query" do
    let(:connection) { ApplicationRecord.connection }

    it "allows execution of arbitrary SQL statements with sanitized values in an array" do
      expect(connection).to receive(:execute).with("SELECT 1 AS count WHERE 'a' = 'a'").and_call_original
      result = ApplicationRecord.execute_query("SELECT 1 AS count WHERE 'a' = ?", "a")
      expect(result.first['count']).to eq 1
    end

    it "allows execution of arbitrary SQL statements with sanitized values in a hash" do
      expect(connection).to receive(:execute).with("SELECT 1 AS count WHERE 'a' = 'a'").and_call_original
      result = ApplicationRecord.execute_query("SELECT 1 AS count WHERE 'a' = :val", val: "a")
      expect(result.first['count']).to eq 1
    end
  end

  describe "#in_transaction?", skip_db_transactions: true, skip_seeds: true do
    subject { ApplicationRecord.in_transaction? }

    it "works out of a transaction" do
      expect(subject).to be_falsey
    end
    it "works in a transaction" do
      ApplicationRecord.transaction do
        expect(subject).to be_truthy
      end
    end
    it "works in a nested_transaction" do
      ApplicationRecord.transaction do
        ApplicationRecord.transaction do
          expect(subject).to be_truthy
        end
      end
    end
  end
end
