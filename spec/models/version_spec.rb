# frozen_string_literal: true

require "rails_helper"

describe Version do
  describe 'callbacks' do
    shared_examples 'schedule file upload' do
      it 'schedules ForcedVersionUploadWorker' do
        expect(ForcedVersionUploadWorker).to receive(:perform_async).once

        version.save!
      end
    end

    shared_examples 'does not schedule file upload' do
      it 'does not schedule ForcedVersionUploadWorker' do
        expect(ForcedVersionUploadWorker).not_to receive(:perform_async)

        version.save!
      end
    end

    shared_examples 'does not schedule file upload if not newer forced version' do
      context 'and db already has a newer forced version' do
        before do
          create(:version, forced: true, version: '1.0.20', name: name)
        end

        it 'does not schedule ForcedVersionUploadWorker' do
          expect(ForcedVersionUploadWorker).not_to receive(:perform_async)

          version.save!
        end
      end
    end

    shared_examples 'work as expected for new versions' do
      context 'when forced is true' do
        let(:forced) { true }

        context 'and name is android' do
          let(:name) { 'android' }

          it_behaves_like 'schedule file upload'

          it_behaves_like 'does not schedule file upload if not newer forced version'
        end

        context 'and name is ios' do
          let(:name) { 'ios' }

          it_behaves_like 'schedule file upload'

          it_behaves_like 'does not schedule file upload if not newer forced version'
        end

        context 'and name is any_but_ios_or_android' do
          let(:name) { 'any_but_ios_or_android' }

          it_behaves_like 'does not schedule file upload'
        end
      end

      context 'when forced is set to false' do
        let(:forced) { false }
        let(:name) { 'android' }

        it_behaves_like 'does not schedule file upload'
      end
    end

    shared_examples 'work as expected for already created versions' do
      context 'when forced changes to true' do
        before do
          version.forced = true
        end

        context 'and name is android' do
          let(:name) { 'android' }

          it_behaves_like 'schedule file upload'

          it_behaves_like 'does not schedule file upload if not newer forced version'
        end

        context 'and name is ios' do
          let(:name) { 'ios' }

          it_behaves_like 'schedule file upload'

          it_behaves_like 'does not schedule file upload if not newer forced version'
        end

        context 'and name is any_but_ios_or_android' do
          let(:name) { 'any_but_ios_or_android' }

          it_behaves_like 'does not schedule file upload'
        end
      end

      context 'when forced is not changed to true' do
        let(:name) { 'android' }

        before do
          version.description = '72839'
        end

        it_behaves_like 'does not schedule file upload'
      end
    end

    context 'with new version' do
      let(:version) { build :version, forced: forced, version: '1.0.10', name: name }

      it_behaves_like 'work as expected for new versions'
    end

    context 'with existent version' do
      let(:version) { create :version, forced: false, version: '1.0.10', name: name }

      it_behaves_like 'work as expected for already created versions'
    end
  end
end
