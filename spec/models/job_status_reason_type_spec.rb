# frozen_string_literal: true

require "rails_helper"

describe JobStatusReasonType do
  describe '#find_legacy' do
    subject(:find_legacy) { job_status_reason_type.find_legacy }

    context 'with an Accepted JobStatusReasonType' do
      let(:job_status_reason_type) { create(:job_status_reason_type, :accepted_event_in_area) }
      let!(:corresponding_job_eta_explanation) { create(:job_eta_explanation, text: job_status_reason_type.text) }

      it { is_expected.to eq corresponding_job_eta_explanation }
    end

    context 'with no corresponding JobETAExplanation' do
      let(:job_status_reason_type) do
        create(
          :job_status_reason_type,
          text: 'I dont exist on JobEtaExplanation',
          key: :bla,
          job_status: create(:job_status, :accepted)
        )
      end

      it 'raises error' do
        expect { find_legacy }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
