# frozen_string_literal: true

require "rails_helper"

describe User::Visit do
  describe User::Visit::SessionTracker do
    describe "track!" do
      it "only executes the block if the user/application combo is already being tracked" do
        counter = 0
        tracker = User::Visit::SessionTracker.new(1, 2)

        tracker.track! { counter += 1 }
        expect(counter).to eq 1

        tracker.track! { counter += 1 }
        expect(counter).to eq 1

        tracker.clear!
        tracker.track! { counter += 1 }
        expect(counter).to eq 2
      end
    end
  end

  describe "track", freeze_time: true do
    it "schedules a single sidekiq worker to log the visit if the session" do
      User::Visit.track(user_id: 1, source_application_id: 2)
      expect(UserVisitWorker.jobs.size).to eq 1
      expect(UserVisitWorker).to have_enqueued_sidekiq_job(Date.today.iso8601, 1, 2)
    end
  end
end
