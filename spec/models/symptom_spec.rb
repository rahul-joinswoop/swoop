# frozen_string_literal: true

require "rails_helper"

describe Symptom do
  let(:symptom) { create(:symptom) }

  it "creates symptom" do
    expect(symptom).to be_valid
  end
end
