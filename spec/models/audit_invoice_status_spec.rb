# frozen_string_literal: true

require "rails_helper"

describe AuditInvoiceStatus do
  let(:invoice) { create(:invoice, state: 'created') }

  describe 'creates audit records' do
    it 'audits the status change when an invoice is saved' do
      # Step 1: Advance an invoice from created to partner_new and save it
      expect(invoice.state).to eql('created')
      invoice.partner_new
      expect(invoice.state).to eql('partner_new')
      invoice.save!
      # Step 2: Ensure that the invoice is persisted as partner_new, and the
      # transition was logged
      expect(Invoice.find(invoice.id).state).to eq('partner_new')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id).count).to eql(1)
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[0].from_invoice_status).to eq('created')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[0].to_invoice_status).to eq('partner_new')
      # Step 3: Advance the invoice to partner_approved ('on_site_payment') in memory
      invoice.partner_approved
      expect(invoice.state).to eql 'on_site_payment'
      # Step 4: Verify that the invoice status and audit trail hasn't changed in the
      # database yet, because the invoice hasn't been saved
      expect(Invoice.find(invoice.id).state).to eq('partner_new')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id).count).to eql(1)
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[0].from_invoice_status).to eq('created')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[0].to_invoice_status).to eq('partner_new')
      # Step 5: Save the invoice
      invoice.save!
      invoice.reload
      # Step 6: Verify that the invoice status and audit trail has changed in the
      # database, and the audit trail is intact
      expect(Invoice.find(invoice.id).state).to eq('on_site_payment')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id).count).to eql(2)
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[0].from_invoice_status).to eq('created')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[0].to_invoice_status).to eq('partner_new')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[1].from_invoice_status).to eq('partner_new')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[1].to_invoice_status).to eq('on_site_payment')
      expect(AuditInvoiceStatus.where(invoice_id: invoice.id)[0].transition_happened_at).to be < AuditInvoiceStatus.where(invoice_id: invoice.id)[1].transition_happened_at
    end
  end

  describe 'invalid `*_invoice_status` values' do
    context 'rails model level exception' do
      it 'raises when `from_invoice_status` has invalid value' do
        expect do
          invoice.audit_invoice_statuses.create!(from_invoice_status: "invalid_invoice_status",
                                                 to_invoice_status: "partner_new",
                                                 transition_happened_at: Time.now)
        end.to raise_error(ArgumentError, /is not a valid from_invoice_status/)
      end

      it 'raises at db level when `to_invoice_status` has invalid value' do
        expect do
          invoice.audit_invoice_statuses.create!(from_invoice_status: "partner_new",
                                                 to_invoice_status: "invalid_invoice_status",
                                                 transition_happened_at: Time.now)
        end.to raise_error(ArgumentError, /is not a valid to_invoice_status/)
      end
    end

    context 'db level exception' do
      it 'raises when `from_invoice_status` has invalid value' do
        expect do
          ApplicationRecord.execute_query(
            "INSERT INTO audit_invoice_statuses (invoice_id, from_invoice_status, to_invoice_status) VALUES (?,?,?)",
            invoice.id,
            'invalid_invoice_status',
            'on_site_payment'
          )
        end.to raise_error(ActiveRecord::StatementInvalid, /PG::InvalidTextRepresentation: ERROR:  invalid input value for enum invoice_status: "invalid_invoice_status"/)
      end

      it 'raises at db level when `to_invoice_status` has invalid value' do
        expect do
          ApplicationRecord.execute_query(
            "INSERT INTO audit_invoice_statuses (invoice_id, from_invoice_status, to_invoice_status) VALUES (?,?,?)",
            invoice.id,
            'partner_new',
            'invalid_invoice_status'
          )
        end.to raise_error(ActiveRecord::StatementInvalid, /PG::InvalidTextRepresentation: ERROR:  invalid input value for enum invoice_status: "invalid_invoice_status"/)
      end
    end
  end

  describe 'track invoice `total_amount` for each status change' do
    let(:invoice) { create(:invoice, :with_line_items, state: 'partner_new') }

    context 'invoice line item amounts are not changed' do
      it 'stores same values for `total_amount_before_state_change` and `total_amount_after_state_change`' do
        expect(invoice.total_amount).to eq(100)
        invoice.partner_approved
        invoice.save!
        invoice.reload

        expect(invoice.audit_invoice_statuses.count).to eq(1)
        expect(invoice.audit_invoice_statuses.first.total_amount_before_state_change).to eq(100)
        expect(invoice.audit_invoice_statuses.first.total_amount_after_state_change).to eq(100)
      end
    end

    context 'invoice line item amounts are changed' do
      it 'stores updated invoice\'s `total_amount` value for `total_amount_before_state_change` and `total_amount_after_state_change`' do
        expect(invoice.total_amount).to eq(100)

        invoice.line_items.build(net_amount: 5, tax_amount: 0.25)
        invoice.partner_approved
        invoice.save!
        invoice.reload

        expect(invoice.audit_invoice_statuses.count).to eq(1)
        expect(invoice.audit_invoice_statuses.first.total_amount_before_state_change).to eq(100)
        expect(invoice.audit_invoice_statuses.first.total_amount_after_state_change).to eq(105.25)
      end
    end

    context 'invoice has no line items' do
      let(:job) { create(:job, :with_account) }
      let(:invoice) { create(:invoice, state: 'created') }

      it 'saves correct total_amount to audit records' do
        job.invoice = invoice
        job.save!

        expect(invoice.line_items.count).to eq(0)
        expect(invoice.total_amount).to eq(0)

        invoice.line_items.build(net_amount: 5, tax_amount: 0.25)
        invoice.partner_new
        invoice.save!
        invoice.reload

        expect(invoice.line_items.count).to eq(1)
        expect(invoice.total_amount).to eq(5.25)

        expect(invoice.audit_invoice_statuses.count).to eq(1)
        expect(invoice.audit_invoice_statuses.first.from_invoice_status).to eq('created')
        expect(invoice.audit_invoice_statuses.first.total_amount_before_state_change).to eq(0)
        expect(invoice.audit_invoice_statuses.first.to_invoice_status).to eq('partner_new')
        expect(invoice.audit_invoice_statuses.first.total_amount_after_state_change).to eq(5.25)
      end
    end
  end
end
