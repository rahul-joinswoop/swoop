# frozen_string_literal: true

require "rails_helper"

describe Tire do
  let(:tire) { create(:tire) }

  it "creates tire" do
    expect(tire).to be_valid
  end
end
