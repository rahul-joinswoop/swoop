# frozen_string_literal: true

require "rails_helper"
require 'aasm/rspec'

describe FleetManagedJob do
  describe 'validates' do
    let(:fleet_managed_job) { create(:fleet_managed_job) }

    context 'relationships' do
      context 'drop_location' do
        it 'valid if no drop location is set' do
          expect(fleet_managed_job.drop_location).to be_nil
          expect(fleet_managed_job).to be_valid
        end
        it 'valid if a drop location is set, and it has lat and lng' do
          fleet_managed_job.drop_location = build(:location, :random)
          expect(fleet_managed_job.drop_location).to be_a(Location)
          expect(fleet_managed_job.drop_location.lat).to be_a(Float)
          expect(fleet_managed_job.drop_location.lng).to be_a(Float)
          expect(fleet_managed_job).to be_valid
        end
        it 'invalid if a drop location is set, but does not have a lat and a lng' do
          fleet_managed_job.drop_location = build(:location, :random, lng: nil)
          expect(fleet_managed_job.drop_location).to be_a(Location)
          expect(fleet_managed_job.drop_location.lat).to be_a(Float)
          expect(fleet_managed_job.drop_location.lng).to be_nil
          expect(fleet_managed_job).not_to be_valid
        end
      end
    end
  end

  describe '#init_review_sms' do
    subject { create(:fleet_managed_job, review_sms: review_sms).review_sms }

    context "with no values" do
      let(:review_sms) { nil }

      it "works" do
        expect(subject).to be_truthy
      end
    end

    context "with false" do
      let(:review_sms) { false }

      it "works" do
        expect(subject).to be_falsey
      end
    end

    context "with true" do
      let(:review_sms) { true }

      it "works" do
        expect(subject).to be_truthy
      end
    end
  end

  describe 'aasm state transitions' do
    let(:fleet_job?) { true }

    before :each do
      allow_any_instance_of(described_class).to(
        receive(:fleet_job?)
      ).and_return fleet_job?

      allow_any_instance_of(described_class).to(receive(:status_change))
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :dispatched
      ).on_event(:swoop_dispatched)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :enroute
      ).on_event(:swoop_enroute)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :onsite
      ).on_event(:swoop_onsite)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :towing
      ).on_event(:swoop_towing)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :towdestination
      ).on_event(:swoop_towdestination)
    end

    it do
      expect(subject).to transition_from(:assigned).to(
        :completed
      ).on_event(:swoop_completed)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :dispatched
      ).on_event(:swoop_dispatched)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :enroute
      ).on_event(:swoop_enroute)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :onsite
      ).on_event(:swoop_onsite)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :towing
      ).on_event(:swoop_towing)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :towdestination
      ).on_event(:swoop_towdestination)
    end

    it do
      expect(subject).to transition_from(:accepted).to(
        :completed
      ).on_event(:swoop_completed)
    end
  end

  describe '#process_auto_assign' do
    let(:fleet_managed_job) do
      create(
        :fleet_managed_job,
        status: job_status,
        service_code: ServiceCode.find_by!(name: ServiceCode::TOW),
        scheduled_for: scheduled_for
      )
    end

    before do
      allow(StartAuctionServiceWorker).to receive(:perform_async)
    end

    context 'when job.status is auto_assigning' do
      let(:job_status) { Job::STATUS_AUTO_ASSIGNING }

      context 'and job.scheduled_for is false' do
        let(:scheduled_for) { false }

        context 'and job.fleet_company has AUTO_ASSIGN feature enabled' do
          before do
            allow(fleet_managed_job.fleet_company).to receive(:has_feature).with(
              Feature::AUTO_ASSIGN
            ).and_return(true)
          end

          context 'and job#needs_auto_assign attribute is true' do
            before do
              fleet_managed_job.needs_auto_assign = true
            end

            it 'schedules StartAuctionServiceWorker only once' do
              # calling it twice on purpose, we want to test if StartAuctionServiceWorker
              # is called only once
              fleet_managed_job.process_auto_assign
              fleet_managed_job.process_auto_assign

              expect(StartAuctionServiceWorker).to have_received(:perform_async).with(
                fleet_managed_job.id
              ).once
            end
          end
        end
      end
    end
  end

  describe "#can_auto_assign?" do
    subject { job.can_auto_assign? }

    let(:job) do
      create(
        :fleet_managed_job,
        status: status,
        scheduled_for: scheduled_for,
        fleet_company: fleet_company,
        invoice_vehicle_category: invoice_vehicle_category,
        service_code: service_code
      )
    end

    let(:fleet_company) { create(:fleet_company) }
    let!(:auto_assign_feature) { create(:feature, name: Feature::AUTO_ASSIGN) }

    let(:scheduled_for) { nil }
    let(:invoice_vehicle_category) { nil }
    let(:service_code) { create(:service_code, name: ServiceCode::BATTERY_JUMP) }

    context 'when status is draft' do
      let(:status) { 'draft' }

      it { is_expected.to be_falsey }
    end

    context 'when status is predraft' do
      let(:status) { 'predraft' }

      it { is_expected.to be_falsey }
    end

    context 'when status is predraft' do
      let(:status) { 'deleted' }

      it { is_expected.to be_falsey }
    end

    context 'when status is created' do
      let(:status) { 'created' }

      context 'and job.scheduled_for is not present' do
        context 'and job.fleet_company has AutoAssign feature' do
          let!(:company_feature) do
            create(:companies_feature, feature: auto_assign_feature, company: fleet_company)
          end

          context 'and job.invoice_vehicle_category is nil' do
            context 'and job.service_code is valid for Auction' do
              it { is_expected.to be_truthy }
            end

            context 'and job.service_code is not valid for Auction' do
              let(:service_code) { create(:service_code, name: ServiceCode::OTHER) }

              it { is_expected.to be_falsey }
            end
          end

          context 'and job.invoice_vehicle_category is valid for Auction' do
            let(:invoice_vehicle_category) do
              create(:vehicle_catetory, name: VehicleCategory::FLATBED)

              it { is_expected.to be_truthy }
            end
          end

          context 'and job.invoice_vehicle_category is not valid for Auction' do
            let(:invoice_vehicle_category) do
              create(:vehicle_catetory, name: VehicleCategory::HEAVY_DUTY)

              it { is_expected.to be_falsey }
            end
          end
        end

        context 'and job.fleet_company does not have AutoAssign feature' do
          it { is_expected.to be_falsey }
        end
      end

      context 'and job.scheduled_for is present' do
        let(:scheduled_for) { Time.now + 1.day }

        it { is_expected.to be_falsey }
      end
    end
  end

  describe "callbacks" do
    describe "Notifiable.trigger_fleet_managed_job_assigned" do
      let!(:job) { create :fleet_managed_job }

      it "works on a status change" do
        expect(Notifiable).to receive(:trigger_fleet_managed_job_assigned).once.with(job)
        job.assigned!
      end

      it "works on a rescue_company change" do
        expect(Notifiable).to receive(:trigger_fleet_managed_job_assigned).once.with(job)
        job.update! rescue_company: create(:rescue_company)
      end
    end
  end
end
