# frozen_string_literal: true

require "rails_helper"
require 'geocoder/results/google_premier'

describe Location do
  let(:location) { create(:location) }

  it "creates job" do
    expect(location).to be_valid
  end

  describe "city_state" do
    it "formats the city and state" do
      expect(Location.new(city: "Milwaukee", state: "WI").city_state).to eq "Milwaukee, WI"
    end

    it "shows just the city if there is no state" do
      expect(Location.new(city: "Milwaukee").city_state).to eq "Milwaukee"
    end

    it "shows just the state if there is no city" do
      expect(Location.new(state: "WI").city_state).to eq "WI"
    end

    it "is blank if there is no city or state" do
      expect(Location.new.city_state).to eq ""
    end
  end

  describe "street_number and street_name" do
    let(:location_with_address) { create(:location, address: "Any Address") }

    it { should validate_length_of(:street_name).is_at_most(60) }
    it { should validate_length_of(:street_number).is_at_most(12) }

    it "triggers callback" do
      expect_any_instance_of(Location).to receive(:resolve_place_id)
      expect(location.address).to be_nil
    end

    it "enqueues place resolver" do
      expect(PlacesResolver).to receive(:perform_async)
      location_with_address
    end

    it "populates street name and number" do
      result = Geocoder::Result::GooglePremier.new({
        "address_components" =>
          [
            { "long_name" => "2999", "short_name" => "2999", "types" => ["street_number"] },
            { "long_name" => "F Street", "short_name" => "F St", "types" => ["route"] },
          ],
        "formatted_address" => "2999 F St, Sacramento, CA 95816, USA",
        "geometry" =>
                                                       {
                                                         "location" => { "lat" => 38.57752079999999, "lng" => -121.4646444 },
                                                       },
        "types" => ["street_address"],
      })

      allow(Geocoder).to receive(:search).and_return([result])
      location = create(:location, :johns_cabin)

      expect(location.street_name).to be_nil
      expect(location.street_number).to be_nil

      # Process the job
      PlacesResolver.new.perform(location.id)
      location.reload

      expect(location.street_number).to eq("2999")
      expect(location.street_name).to eq("F St")
    end
  end

  describe "address_without_street_number" do
    subject { location.address_without_street_number }

    context "with a valid address" do
      let(:location) { create :location, :random }

      it "works" do
        expect(subject).to eq(location.address_without_usa.sub(/^\d+\s+/, ''))
      end
    end

    context "without a parsable address" do
      let(:location) { create :location, address: Faker::Hipster.sentence }

      it "works" do
        expect { subject }.not_to raise_error
        expect(subject).to eq(location.address)
      end
    end
  end

  describe "address_without_usa" do
    subject { location.address_without_usa }

    context "with a valid address" do
      let(:location) { create :location, :random }

      it "works" do
        expect(subject).to eq(location.address.sub(', USA', ''))
      end
    end

    context "without a parsable address" do
      let(:location) { create :location, address: Faker::Hipster.sentence }

      it "works" do
        expect { subject }.not_to raise_error
        expect(subject).to eq(location.address)
      end
    end
  end

  context 'methods' do
    context 'instance level methods' do
      context '#territories_that_contain_it' do
        subject { location }

        let(:rescue_company) { create :rescue_company }
        let(:location) { Location.new(lat: 0.5, lng: 0.5) }
        let(:polygon_area) { 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5))' }
        let!(:containing_territory) { create :territory, company: rescue_company, polygon_area: polygon_area }

        it { is_expected.to respond_to(:territories_that_contain_it) }

        it 'return the territories containing the point of location' do
          expect(location.territories_that_contain_it).to be_a(ActiveRecord::Relation)
          expect(location.territories_that_contain_it).to include(containing_territory)
        end
      end

      context '#rescue_companies_whose_teritories_contain_it' do
        let(:location) { Location.new(lat: 0, lng: 0) }

        subject { location }

        it { is_expected.to respond_to(:rescue_companies_whose_teritories_contain_it) }

        context 'calling the method' do
          let(:polygon_area) { 'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5))' }
          let(:company) { create :rescue_company }
          let!(:containing_territory) { create :territory, company: company, polygon_area: polygon_area }

          subject { location.rescue_companies_whose_teritories_contain_it }

          it 'returns one rescue company' do
            expect(subject.size).to eq(1)
            expect(subject).to include(company)
          end
        end
      end
    end
  end
end
