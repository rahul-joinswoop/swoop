# frozen_string_literal: true

require 'rails_helper'

describe IsscDispatchRequest do
  subject do
    IsscDispatchRequest.create(
      {
        dispatchid: '123456',
        status: IsscDispatchRequest::EXPIRED,
      }
    )
  end

  # ENG-7659
  it 'allows transition from expired to expired' do
    expect { subject.requester_expired }.to avoid_changing(subject, :status)
  end
end
