# frozen_string_literal: true

require "rails_helper"

describe PartnerAPIPasswordRequest do
  let(:email_verification) do
    described_class.new(user: user, client_id: client_id)
  end

  # we only test the things that are different from PasswordRequest
  let(:user) { create(:user, :phone) }
  let(:client) { create :doorkeeper_application }
  let(:client_id) { client.uid }
  let!(:access_grant) do
    if user.present? && client.present?
      create :doorkeeper_access_grant,
             resource_owner_id: user&.id,
             application_id: client&.id
    end
  end

  describe '#trigger' do
    subject { email_verification.send :trigger }

    it "works" do
      subject
      expect(Email::PartnerAPIPasswordRequestWorker)
        .to have_enqueued_sidekiq_job(email_verification.id)
    end
  end

  context "validations" do
    subject { email_verification.save! }

    describe "without access grant" do
      let!(:access_grant) { nil }

      it "fails without a valid client_id" do
        expect { subject }
          .to raise_error(
            ActiveRecord::RecordInvalid,
            "Validation failed: User must have granted access to client app"
          )
      end
    end

    describe "with revoked access grant" do
      let!(:access_grant) { super().tap { |ag| ag.update! revoked_at: DateTime.current } }

      it "fails without a valid client_id" do
        expect { subject }
          .to raise_error(
            ActiveRecord::RecordInvalid,
            "Validation failed: User must have granted access to client app"
          )
      end
    end

    describe "client_id" do
      let(:client_id) { SecureRandom.uuid }

      it "fails without a valid client_id" do
        expect { subject }
          .to raise_error(
            ActiveRecord::RecordInvalid,
            "Validation failed: Client is invalid"
          )
      end
    end

    describe "user" do
      let(:user) { nil }

      it "fails without a user" do
        expect { subject }
          .to raise_error(
            ActiveRecord::RecordInvalid,
            "Validation failed: User must exist"
          )
      end
    end

    describe "user_email" do
      let(:user) { create :user, :phone, email: nil }

      it "fails without a user_email" do
        expect { subject }
          .to raise_error(
            ActiveRecord::RecordInvalid,
            "Validation failed: User must have an email address"
          )
      end
    end
  end

  context "#trigger!" do
    subject { email_verification.trigger! }

    describe '#send_sms' do
      it 'does not trigger a sms job' do
        expect(described_class).not_to receive(:send_sms)
        subject
      end
    end

    describe '#send_sms' do
      subject { email_verification.send :send_sms }

      it 'blows up if #send_sms is called' do
        expect { subject }
          .to raise_error(NoMethodError, "#{described_class.name}#send_sms not implemented")
      end
    end

    describe '#invalidate!', freeze_time: true do
      subject { email_verification.invalidate! }

      it "invalidates the #{described_class}" do
        subject
        expect(email_verification).not_to be_valid_request
        expect(email_verification.used_at).to eq(Time.current)
      end

      context 'when oauth access tokens exist' do
        let!(:access_token) do
          FactoryBot.create(:doorkeeper_access_token, resource_owner_id: user.id)
        end

        it 'revokes access token' do
          expect { subject }
            .to change { user.access_tokens.where(revoked_at: nil).length }
            .from(1)
            .to(0)
        end
      end
    end
  end
end
