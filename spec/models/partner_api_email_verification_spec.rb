# frozen_string_literal: true

require "rails_helper"

describe PartnerAPIEmailVerification do
  let(:email_verification) do
    described_class.new(user: user,
                        user_input: user_input,
                        redirect_uri: redirect_uri,
                        client_id: client_id)
  end

  # we only test the things that are different from PasswordRequest
  let(:user) { create(:user) }
  let(:user_input) { user.email }
  let(:client) { create :doorkeeper_application }
  let(:client_id) { client.uid }
  let(:redirect_uri) { client.redirect_uri }

  describe '#trigger' do
    subject { email_verification.send :trigger }

    it "works" do
      subject
      expect(Email::PartnerAPIEmailVerificationWorker)
        .to have_enqueued_sidekiq_job(email_verification.id)
    end
  end

  context "validations" do
    subject { email_verification.save! }

    describe "client_id" do
      let(:client_id) { SecureRandom.uuid }

      it "fails without a valid client_id" do
        expect { subject }
          .to raise_error(
            ActiveRecord::RecordInvalid,
            "Validation failed: Client is invalid"
          )
      end
    end

    describe "redirect_uri" do
      let(:redirect_uri) { Faker::Internet.url }

      it "fails without a valid redirect_uri" do
        expect { subject }
          .to raise_error(
            ActiveRecord::RecordInvalid,
            "Validation failed: Redirect uri is invalid"
          )
      end
    end
  end

  context "#trigger!" do
    subject { email_verification.trigger! }

    describe '#send_sms' do
      let(:user) { create(:user, email: nil, phone: '55555555') }
      let(:user_input) { 'tow-user' }

      it 'does not trigger a sms job' do
        expect(described_class).not_to receive(:send_sms)
        subject
      end
    end

    describe '#send_sms' do
      subject { email_verification.send :send_sms }

      it 'blows up if #send_sms is called' do
        expect { subject }
          .to raise_error(NoMethodError, "#{described_class.name}#send_sms not implemented")
      end
    end

    describe '#invalidate!', freeze_time: true do
      subject { email_verification.invalidate! }

      it "invalidates the #{described_class}" do
        subject
        expect(email_verification).not_to be_valid_request
        expect(email_verification.used_at).to eq(Time.current)
      end

      context 'when oauth access tokens exist' do
        let!(:access_token) do
          FactoryBot.create(:doorkeeper_access_token, resource_owner_id: user.id)
        end

        it 'does not revoke access token' do
          expect { subject }
            .not_to change { user.access_tokens.where(revoked_at: nil).length }
        end
      end
    end
  end
end
