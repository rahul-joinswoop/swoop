# frozen_string_literal: true

require "rails_helper"

describe Department do
  let(:department) { create(:department) }

  it "creates department" do
    expect(department).to be_valid
  end

  describe "update dependent indexes" do
    it "updates dependent indexes when the name changes" do
      department = create(:department)
      SearchIndex::DepartmentDependentIndexUpdateWorker.jobs.clear

      department.update!(updated_at: Time.current)
      expect(SearchIndex::DepartmentDependentIndexUpdateWorker.jobs.size).to eq 0

      department.update!(name: "New Name")
      expect(SearchIndex::DepartmentDependentIndexUpdateWorker).to have_enqueued_sidekiq_job(department.id)
    end
  end
end
