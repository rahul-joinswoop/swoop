# frozen_string_literal: true

require "rails_helper"

describe CompanyInvoiceChargeFee, type: :model do
  describe '#create' do
    let(:company) { build_stubbed(:company, currency: 'USD') }

    it 'must have a company' do
      expect { create(:company_invoice_charge_fee, company: nil) }
        .to raise_error(ActiveRecord::RecordInvalid, /Company can't be blank/)
    end

    it 'must have a percentage' do
      expect { create(:company_invoice_charge_fee, percentage: nil) }
        .to raise_error(ActiveRecord::RecordInvalid, /Percentage can't be blank/)
    end

    it 'must have a fixed_value' do
      expect { create(:company_invoice_charge_fee, fixed_value: nil) }
        .to raise_error(ActiveRecord::RecordInvalid, /Fixed value can't be blank/)
    end

    it 'must have a payout_interval' do
      expect { create(:company_invoice_charge_fee, payout_interval: nil) }
        .to raise_error(ActiveRecord::RecordInvalid, /Payout interval can't be blank/)
    end

    it 'raises if currency doesn\'t match company' do
      expect { create(:company_invoice_charge_fee, currency: 'EUR', company: company) }
        .to raise_error(ActiveRecord::RecordInvalid, /Currency must match Company/)
    end
  end
end
