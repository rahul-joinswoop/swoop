# frozen_string_literal: true

require "rails_helper"

describe CustomerType do
  let(:customer_type) { create(:customer_type) }

  it "creates customer_type" do
    expect(customer_type).to be_valid
  end
end
