# frozen_string_literal: true

require "rails_helper"

describe AuditJobStatus do
  subject(:audit_job_status_for_job_mark_paid) do
    described_class.audit_job_status_for_job_mark_paid(job)
  end

  let(:job) { Job.new(invoice: invoice) }
  let(:invoice) do
    Invoice.new.tap do |invoice|
      invoice.mark_paid
    end
  end

  describe '#job_status_deleted?' do
    subject do
      FactoryBot.build_stubbed(:audit_job_status, job_status: job_status)
        .job_status_deleted?
    end

    context 'when job status is deleted' do
      let(:job_status) { FactoryBot.build_stubbed(:job_status, :deleted) }

      it { is_expected.to eq(true) }
    end

    context 'when job status is not deleted' do
      let(:job_status) { FactoryBot.build_stubbed(:job_status, :created) }

      it { is_expected.to eq(false) }
    end
  end

  describe '.audit_job_status_for_job_mark_paid' do
    it "sets its id to #{JobStatus::MARK_PAID}" do
      expect(audit_job_status_for_job_mark_paid.id).to eq JobStatus::MARK_PAID
    end

    it "sets its job_status with #{JobStatus.job_status_for_job_mark_paid}" do
      expect(audit_job_status_for_job_mark_paid.job_status).to(
        eq JobStatus.job_status_for_job_mark_paid
      )
    end

    it 'sets the job accordingly' do
      expect(audit_job_status_for_job_mark_paid.job).to eq job
    end

    context 'when job is nil' do
      let(:job) { nil }

      it 'returns nil' do
        expect(audit_job_status_for_job_mark_paid).to be nil
      end
    end

    context 'when invoice is nil' do
      let(:job) { nil }

      it 'returns nil' do
        expect(audit_job_status_for_job_mark_paid).to be nil
      end
    end

    context 'when invoice.mark_paid_at is nil' do
      let(:job) { nil }

      it 'returns nil' do
        expect(audit_job_status_for_job_mark_paid).to be nil
      end
    end
  end

  describe "adjusted_created_at" do
    let(:time) { Time.new(2018, 10, 25, 11, 55, 0) }
    let!(:job) { create(:job) }

    it "sets the adjusted_created_at on the job when the status is created" do
      expect_any_instance_of(AdjustedCreatedAtCalculator).to receive(:calculate).and_return(time)
      AuditJobStatus.create!(job: job, job_status_id: JobStatus::CREATED)
      expect(job.reload.adjusted_created_at).to eq time
    end

    it "does not set the adjusted_created_at on the job when the status is not created" do
      adjusted_created_at = job.reload.adjusted_created_at
      AuditJobStatus.create!(job: job, job_status_id: JobStatus::DISPATCHED)
      expect(job.reload.adjusted_created_at).to eq adjusted_created_at
    end
  end

  describe '#changed_by_company' do
    subject(:ajs) do
      build_stubbed(:audit_job_status, user: user, source_application: source_application).changed_by_company
    end

    let(:user) { nil }
    let(:source_application) { nil }

    context 'when user is set' do
      let(:user) { create(:user, :rescue) }

      it { is_expected.to eq user.company }
    end

    context 'when source_application is set' do
      let(:doorkeeper_application) { create :doorkeeper_application }
      let(:source_application) { SourceApplication.instance(platform: SourceApplication::PLATFORM_CLIENT_API, oauth_application_id: doorkeeper_application.id) }

      it { is_expected.to eq Company.find(source_application.oauth_application.owner_id) }
    end
  end

  include_context 'job history label shared context', :audit_job_status
end
