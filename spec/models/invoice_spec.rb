# frozen_string_literal: true

require "rails_helper"
require "aasm/rspec"

describe Invoice do
  subject(:invoice) { build_stubbed :invoice }

  let(:not_on_site_payment) { true }

  before :each do
    allow_any_instance_of(described_class).to(
      receive(:not_on_site_payment?)
    ).and_return not_on_site_payment
  end

  describe "#send_address" do
    context "when corresponding job has storage" do
      let!(:account) do
        build_stubbed(:account, name: "Account Name")
      end
      let!(:storage) do
        build_stubbed(
          :inventory_item, replace_invoice_info: true, released_to_account: nil
        )
      end
      let!(:job) { build_stubbed(:job, storage: storage) }
      let!(:invoice) { build_stubbed(:invoice, job: job) }

      it "does not return an address when storage is not released" do
        expect(invoice.send_address).to eq([invoice.recipient.email])
      end

      it "returns account address when storage is released to account" do
        storage.released_to_account = account

        expect(invoice.send_address).to eq(["3222 Johns Cabin Road", " Wildwood, MO, 63038"])
      end

      it "returns user address when storage is released to user" do
        user = create(:user, email: "test@tester.com",
                             first_name: "Test",
                             last_name: "Tester")

        storage.released_to_user = user

        expect(invoice.send_address).to eq(["test@tester.com"])
      end
    end
  end

  describe "#bill_to_name" do
    context "when recipient is account" do
      let!(:account) { build_stubbed(:account) }
      let!(:invoice) { build_stubbed(:invoice, recipient: account) }

      it "uses Account#name" do
        expect(invoice.bill_to_name).to eq(account.name)
      end
    end

    context "when recipient is user" do
      let!(:user) { build_stubbed(:user) }
      let!(:invoice) { build_stubbed(:invoice, recipient: user) }

      it "uses User#full_name" do
        expect(invoice.bill_to_name).to eq(user.full_name)
      end
    end

    context "when corresponding job has storage" do
      let!(:account) do
        build_stubbed(:account, name: "Account Name")
      end
      let!(:storage) do
        build_stubbed(
          :inventory_item, replace_invoice_info: true, released_to_account: nil
        )
      end
      let!(:job) { build_stubbed(:job, storage: storage) }
      let!(:invoice) { build_stubbed(:invoice, job: job) }

      it "does not return account name when storage is not released" do
        expect(invoice.bill_to_name).not_to eq("Account Name")
      end

      it "returns account name when storage is released to account" do
        storage.released_to_account = account

        expect(invoice.bill_to_name).to eq("Account Name")
      end

      it "returns user name when storage is released to user" do
        user = create(:user, first_name: "Test", last_name: "Tester")
        storage.released_to_user = user

        expect(invoice.bill_to_name).to eq("Test Tester")
      end
    end
  end

  describe '#rate_type' do
    subject { invoice.rate_type }

    let!(:invoice) { create(:invoice, rate_type: 'IExist') }

    context 'when line items exist' do
      let(:company) { create(:company) }
      let(:rate) { create(:clean_flat_rate) }
      let(:line_item) { create(:invoicing_line_item, rate: rate) }

      before do
        invoice.line_items << line_item
      end

      it { is_expected.to eq(rate.type) }
    end

    context 'when line items do not exist' do
      context 'but rate_type exists on model' do
        it { is_expected.to eq('IExist') }
      end

      context 'and rate_type does not exist on model' do
        let!(:invoice) { create(:invoice, rate_type: nil) }

        it { is_expected.to eq(nil) }
      end
    end
  end

  describe '#partner_undo_approval' do
    it do
      expect(subject).to transition_from(:partner_approved).to(
        :partner_new
      ).on_event(:partner_undo_approval)
    end
  end

  describe '#fleet_undo_approval' do
    let(:not_on_site_payment) { true }

    before :each do
      allow_any_instance_of(described_class).to(
        receive(:not_on_site_payment?)
      ).and_return not_on_site_payment
    end

    it do
      expect(subject).to transition_from(:fleet_approved).to(
        :partner_sent
      ).on_event(:fleet_undo_approval)
    end
  end

  describe '#swoop_undo_approval_partner' do
    it do
      expect(subject).to transition_from(:swoop_approved_partner).to(
        :partner_sent
      ).on_event(:swoop_undo_approval_partner)
    end

    context 'when invoice is paid' do
      let(:invoice) { create(:invoice) }

      before do
        invoice.mark_paid
      end

      it 'does not transition' do
        expect(subject).not_to allow_event(:swoop_undo_approval_partner)
      end
    end
  end

  describe '#swoop_approved_partner' do
    it do
      expect(subject).to(
        transition_from(:partner_sent)
        .to(:swoop_approved_partner)
        .on_event(:swoop_approved_partner)
      )
    end

    it do
      expect(subject).to(
        transition_from(:partner_offline_new)
        .to(:swoop_approved_partner)
        .on_event(:swoop_approved_partner)
      )
    end
  end

  describe '#swoop_downloading_partner' do
    it do
      expect(subject).to(
        transition_from(:swoop_approved_partner)
        .to(:swoop_downloading_partner)
        .on_event(:swoop_downloading_partner)
      )
    end
  end

  describe '#partner_move_back_to_new' do
    context 'when not on site payment' do
      let(:not_on_site_payment) { true }

      before :each do
        allow_any_instance_of(described_class).to(
          receive(:not_on_site_payment?)
        ).and_return not_on_site_payment
      end

      it do
        expect(subject).to transition_from(:partner_sent).to(
          :partner_approved
        ).on_event(:partner_move_back_to_new)
      end
    end

    context 'when on site payment' do
      let(:on_site_payment) { true }

      before :each do
        allow_any_instance_of(described_class).to(
          receive(:on_site_payment?)
        ).and_return on_site_payment
      end

      it do
        expect(subject).to transition_from(:on_site_payment).to(
          :partner_new
        ).on_event(:partner_move_back_to_new)
      end
    end
  end

  describe '#fleet_move_back_from_downloaded' do
    it do
      expect(subject).to transition_from(:fleet_downloaded).to(
        :fleet_approved
      ).on_event(:fleet_move_back_from_downloaded)
    end
  end

  describe '#mark_as_sent' do
    let(:invoice) { create(:invoice) }

    it 'initializes sent_at' do
      invoice.mark_as_sent

      expect(invoice.sent_at).to be_present
    end
  end

  describe '#mark_paid' do
    let(:invoice) { create(:invoice) }

    it 'sets paid as true' do
      invoice.mark_paid

      expect(invoice.paid).to be true
    end

    it 'sets mark_paid_at' do
      invoice.mark_paid

      expect(invoice.mark_paid_at).not_to be nil
    end
  end

  describe '#reset_paid' do
    it 'sets paid as false' do
      invoice.reset_paid

      expect(invoice.paid).to be false
    end

    it 'sets nil to mark_paid_at' do
      invoice.reset_paid

      expect(invoice.mark_paid_at).to be nil
    end
  end

  describe '#partner_sent' do
    it do
      expect(subject).to transition_from(:partner_sent).to(:fleet_rejected)
        .on_event(:fleet_reject)
    end

    it do
      expect(subject).to transition_from(:partner_sent).to(:swoop_rejected_partner)
        .on_event(:swoop_reject_partner)
    end
  end

  describe '#partner_approve' do
    it do
      expect(subject).to transition_from(:fleet_rejected).to(:partner_approved)
        .on_event(:partner_approved)
    end

    it do
      expect(subject).to transition_from(:swoop_rejected_partner).to(:partner_approved)
        .on_event(:partner_approved)
    end
  end

  describe "#partner_new" do
    shared_examples "transition to :partner_new and call #reset_paid" do
      it "transitions to :partner_new" do
        expect(subject).to transition_from(from_state).to(:partner_new)
          .on_event(:partner_new)
      end

      it "calls #reset_paid" do
        subject.state = "partner_sent"
        expect(subject).to receive(:reset_paid)
        invoice.partner_new
      end
    end

    context "when state is :partner_approved" do
      let(:from_state) { :partner_approved }

      it_behaves_like "transition to :partner_new and call #reset_paid"
    end

    context "when state is :partner_sent" do
      let(:from_state) { :partner_sent }

      it_behaves_like "transition to :partner_new and call #reset_paid"
    end

    context "when state is :fleet_approved" do
      let(:from_state) { :fleet_approved }

      it_behaves_like "transition to :partner_new and call #reset_paid"
    end

    context "when state is :fleet_downloaded" do
      let(:from_state) { :fleet_downloaded }

      it_behaves_like "transition to :partner_new and call #reset_paid"
    end

    context "when state is :on_site_payment" do
      let(:from_state) { :on_site_payment }

      it_behaves_like "transition to :partner_new and call #reset_paid"
    end
  end

  context 'when it is a Swoop invoice' do
    describe '#swoop_approved_fleet' do
      it do
        expect(subject).to(
          transition_from(:swoop_new)
          .to(:swoop_approved_fleet)
          .on_event(:swoop_approved_fleet)
        )
      end
    end

    describe '#swoop_undo_approval_fleet' do
      it do
        expect(subject).to transition_from(:swoop_approved_fleet).to(
          :swoop_new
        ).on_event(:swoop_undo_approval_fleet)
      end
    end

    describe '#swoop_sending_fleet' do
      it do
        expect(subject).to(
          transition_from(:swoop_approved_fleet)
          .to(:swoop_sending_fleet)
          .on_event(:swoop_sending_fleet)
        )
      end
    end

    describe '#swoop_sent_fleet' do
      it do
        expect(subject).to(
          transition_from(:swoop_sending_fleet)
          .to(:swoop_sent_fleet)
          .on_event(:swoop_sent_fleet)
        )
      end
    end

    describe '#swoop_send_fleet_error' do
      it do
        expect(subject).to(
          transition_from(:swoop_sending_fleet)
          .to(:swoop_approved_fleet)
          .on_event(:swoop_send_fleet_error)
        )
      end
    end

    describe '#any_charge_receipt_email_sent?' do
      subject(:any_charge_receipt_email_sent?) { invoice.any_charge_receipt_email_sent? }

      let(:invoice) { create(:invoice) }

      context 'when a charge had a receipt email sent' do
        let!(:charge_one) { create(:charge, invoice: invoice) }
        let!(:charge_two) { create(:charge, invoice: invoice, charge_receipt_email_sent: true) }

        it { is_expected.to be_truthy }
      end

      context 'when no charges had a receipt email sent' do
        let!(:charge_one) { create(:charge, invoice: invoice) }
        let!(:charge_two) { create(:charge, invoice: invoice) }

        it { is_expected.to be_falsey }
      end

      context 'when invoice has no charges' do
        let(:invoice) { create(:invoice) }

        it { is_expected.to be_falsey }
      end
    end

    describe '#payments_or_credits' do
      let!(:payment_3rd_item) do
        create(:payment, created_at: Time.current + 2.days, invoice: invoice)
      end

      let!(:payment_2nd_item) do
        create(:payment, created_at: Time.current + 1.days, invoice: invoice)
      end

      let!(:refund_4th_item) do
        create(:refund, created_at: Time.current + 4.days, invoice: invoice)
      end

      let!(:payment_1st_item) do
        create(:payment, created_at: Time.current, invoice: invoice)
      end

      let(:invoice) { create(:invoice) }

      it 'lists payments_or_credits in cronological order' do
        payments_or_credits = invoice.payments_or_credits

        expect(payments_or_credits.first.id).to eq payment_1st_item.id
        expect(payments_or_credits.second.id).to eq payment_2nd_item.id
        expect(payments_or_credits.third.id).to eq payment_3rd_item.id
        expect(payments_or_credits.fourth.id).to eq refund_4th_item.id
      end
    end

    describe '#rate_types' do
      let!(:job) { create(:fleet_in_house_job, invoice: invoice, fleet_company: fleet_company, rescue_company: rescue_company, account: account) }
      let(:invoice) { create(:invoice) }
      let(:rescue_company) { create(:rescue_company) }
      let!(:account) { create(:account, company: rescue_company, client_company: fleet_company) }

      before do
        RateType.init_all

        invoice.update!(job: job)

        job.reload && invoice.reload
      end

      context 'when job.fleet_company is Tesla' do
        let(:fleet_company) { create(:fleet_company, :tesla) }

        it 'returns Tesla RateType and current job RateType' do
          expect(invoice.rate_types).to match_array(
            RateType.where(type: ['FlatRate', 'TeslaRate']).to_a
          )
        end
      end

      context 'when job.fleet_company is not Tesla' do
        let(:fleet_company) { create(:fleet_company) }

        it 'returns Tesla RateType and current job RateType' do
          expect(invoice.rate_types).to match_array(RateType.standard_items.to_a)
        end
      end
    end

    describe '#class_types' do
      let!(:job) do
        create(
          :fleet_in_house_job,
          invoice: invoice,
          fleet_company: fleet_company,
          rescue_company: rescue_company,
          invoice_vehicle_category: custom_for_recipient_company
        )
      end

      let(:fleet_company) { create(:fleet_company) }
      let(:invoice) { create(:invoice, sender: rescue_company) }
      let(:rescue_company) { create(:rescue_company) }
      let(:custom_for_recipient_company) { create(:vehicle_category, name: 'Very custom one for FleetInHouse') }
      let(:custom_for_partner) { create(:vehicle_category, name: 'Very custom one for RescueCompany') }
      let(:expected_array) { ([rescue_company.vehicle_categories] + [job.invoice_vehicle_category]).flatten }

      before do
        VehicleCategory.init_all
        invoice.update!(job: job)
        fleet_company.vehicle_categories << custom_for_recipient_company
        rescue_company.vehicle_categories << custom_for_partner
        job.reload && invoice.reload
      end

      it 'fetches the expected vehicle categories' do
        expect(invoice.class_types).to match_array expected_array
      end
    end
  end

  describe '#available_addons' do
    let!(:job) do
      create(
        :fleet_in_house_job,
        invoice: invoice,
        fleet_company: fleet_company,
        rescue_company: rescue_company,
      )
    end

    let(:fleet_company) { create(:fleet_company) }
    let(:invoice) { create(:invoice, sender: rescue_company, recipient_company: recipient_company) }
    let(:rescue_company) { create(:rescue_company) }
    let(:custom_for_recipient_company) { create(:service_code, name: 'Very custom addon for FleetInHouse', addon: true) }
    let(:custom_for_partner) { create(:service_code, name: 'Very custom addon for RescueCompany', addon: true) }
    let(:expected_array) { ([rescue_company.addons] + [fleet_company.addons]).flatten }

    let!(:custom_item_addon) { create(:service_code, name: ServiceCode::CUSTOM_ITEM, addon: true) }

    let(:recipient_company) { fleet_company }

    before do
      invoice.update!(job: job)
      fleet_company.addons << custom_for_recipient_company
      rescue_company.addons << custom_for_partner
      job.reload && invoice.reload
    end

    it 'fetches the expected addons' do
      expect(invoice.available_addons).to match_array expected_array
    end

    context 'when invoice does not have a recipient_company' do
      let(:recipient_company) { nil }
      let(:expected_array) { rescue_company.addons }

      it 'fetches the expected addons' do
        expect(invoice.available_addons).to match_array expected_array
      end
    end

    it 'does not add Custom Item addon in the result' do
      expect(invoice.available_addons).not_to include custom_item_addon
    end

    context 'when recipient_company is a Motorclub' do
      let(:fleet_company) { create(:fleet_company, :motor_club) }

      it 'contains only Partner addons' do
        expect(invoice.available_addons).to include custom_for_partner
        expect(invoice.available_addons).not_to include custom_for_recipient_company
      end
    end

    context 'when recipient_company is Swoop' do
      let(:fleet_company) { create(:super_company) }

      it 'contains only Swoop addons' do
        expect(invoice.available_addons).not_to include custom_for_partner
        expect(invoice.available_addons).to include custom_for_recipient_company
      end
    end

    context 'when rescue company has Invoice Custom Items feature enabled' do
      before do
        rescue_company.features << create(:feature, name: Feature::INVOICE_CUSTOM_ITEMS)
      end

      it 'adds Custom Item addon as the first element' do
        expect(invoice.available_addons.first).to eq custom_item_addon
      end

      context 'when recipient_company is Tesla' do
        let(:fleet_company) { create(:fleet_company, :tesla) }

        it 'does not add Custom Item addon in the result' do
          expect(invoice.available_addons).not_to include custom_item_addon
        end
      end

      context 'when recipient_company is Swoop' do
        let(:fleet_company) { create(:super_company) }

        it 'does not add Custom Item addon in the result' do
          expect(invoice.available_addons).not_to include custom_item_addon
        end
      end
    end
  end

  # we test it with different cases for Dolly and Fuel addons
  describe "#available_additional_line_items" do
    let(:invoice) { create(:invoice) }
    let(:addon_dolly) { create(:service_code, :addon, name: ServiceCode::DOLLY) }
    let(:addon_fuel) { create(:service_code, :addon, name: ServiceCode::FUEL) }

    context 'when sender does not have addons' do
      it 'retuns an empty array' do
        additional_line_items = invoice.available_additional_line_items

        expect(additional_line_items.size).to eq 0
      end
    end

    context 'when invoice.sender has Dolly and Fuel addons added to the company' do
      before do
        invoice.sender.addons << addon_dolly
        invoice.sender.addons << addon_fuel
      end

      context 'when invoice.line_items does not contain Dolly line_item' do
        context 'and invoice.line_items also does not contain Fuel line_item' do
          it 'builds the additional_line_items correctly' do
            additional_line_items = invoice.available_additional_line_items

            expect(additional_line_items.size).to eq 2

            li_dolly = additional_line_items.find { |li| li.description == 'Dolly' }
            expect(li_dolly.id).to be_nil

            li_fuel = additional_line_items.find { |li| li.description == 'Fuel' }
            expect(li_fuel.id).to be_nil
          end
        end

        context 'when invoice.line_items already contains Fuel line_item' do
          before do
            invoice.line_items << create(:invoicing_line_item, description: 'Fuel')
          end

          shared_examples 'contains only Dolly addon' do
            it 'builds the additional_line_items correctly' do
              additional_line_items = invoice.available_additional_line_items

              expect(additional_line_items.size).to eq 1

              li_dolly = additional_line_items.find { |li| li.description == 'Dolly' }
              expect(li_dolly.id).to be_nil

              li_fuel = additional_line_items.find { |li| li.description == 'Fuel' }
              expect(li_fuel).to be_nil
            end
          end

          it_behaves_like 'contains only Dolly addon'

          context 'when existent Fuel in invoice.line_item is not persisted yet' do
            before do
              invoice.line_items = []
              invoice.line_items.build({ id: nil, description: 'Fuel' })
            end

            it 'does not have Fuel addon persisted' do
              # check to make sure current li is not persisted
              current_invoice_li_fuel = invoice.line_items.find { |li| li.description == 'Fuel' }
              expect(current_invoice_li_fuel.persisted?).to be_falsey

              invoice.available_additional_line_items

              # and it's still not persisted after the method call
              current_invoice_li_fuel = invoice.line_items.find { |li| li.description == 'Fuel' }
              expect(current_invoice_li_fuel.persisted?).to be_falsey
            end

            it_behaves_like 'contains only Dolly addon'
          end

          context 'and invoice.line_items also already contains Dolly line_item' do
            before do
              invoice.line_items << create(:invoicing_line_item, description: 'Fuel')
              invoice.line_items << create(:invoicing_line_item, description: 'Dolly')
            end

            it 'builds the additional_line_items correctly' do
              additional_line_items = invoice.available_additional_line_items

              expect(additional_line_items.size).to eq 0
            end

            context 'when both Fuel and Addons in invoice.line_item are not persisted yet' do
              before do
                invoice.line_items = []
                invoice.line_items.build({ id: nil, description: 'Fuel' })
                invoice.line_items.build({ id: nil, description: 'Dolly' })
              end

              it 'builds the additional_line_items correctly' do
                # check to make sure current lis are not persisted
                current_invoice_li_fuel = invoice.line_items.find { |li| li.description == 'Fuel' }
                expect(current_invoice_li_fuel.persisted?).to be_falsey
                current_invoice_li_dolly = invoice.line_items.find { |li| li.description == 'Dolly' }
                expect(current_invoice_li_dolly.persisted?).to be_falsey

                additional_line_items = invoice.available_additional_line_items

                expect(additional_line_items.size).to eq 0

                # and they're still not persisted after the method call
                current_invoice_li_fuel = invoice.line_items.find { |li| li.description == 'Fuel' }
                expect(current_invoice_li_fuel.persisted?).to be_falsey
                current_invoice_li_dolly = invoice.line_items.find { |li| li.description == 'Dolly' }
                expect(current_invoice_li_dolly.persisted?).to be_falsey
              end
            end
          end
        end
      end
    end
  end

  describe "priority response line item" do
    let(:company) { create(:company) }

    it "doesn't add Priority Response charges line item in table" do
      priority_job = create(:job, :with_invoice, priority_response: true, rescue_company: company)
      priority_line_items_count = priority_job.invoice.line_items.where(description: "Priority Response").count

      expect(priority_line_items_count).to eq(0)
    end
  end

  describe '#balance' do
    let(:invoice) { create(:invoice, :with_line_items, payments: payments) }

    shared_examples 'expected totals and balance' do |totals|
      it 'has expected total_amount' do
        expect(invoice.total_amount.to_s).to eq totals[:total_amount]
      end

      it 'has expected payments_and_credits_total_amount' do
        expect(invoice.payments_and_credits_total_amount.to_s).to eq totals[:payments_total_amount]
      end

      it 'has expected balance' do
        expect(invoice.balance.to_s).to eq totals[:balance]
      end
    end

    context 'when invoice does not have payments' do
      let(:payments) { [] }

      it_behaves_like 'expected totals and balance', total_amount: '100.0', payments_total_amount: '0.0', balance: '100.0'
    end

    context 'when invoice has payments' do
      context 'and they are persisted in the db' do
        let(:payments) { create_list(:payment, 1) }

        it 'is persisted' do
          expect(invoice.payments_or_credits.first.persisted?).to be_truthy
        end

        it_behaves_like 'expected totals and balance', total_amount: '100.0', payments_total_amount: '-10.0', balance: '90.0'
      end

      context 'and they are not persisted in the db' do
        let(:payments) { [] }

        before do
          invoice.payments_or_credits.build(build_list(:payment, 1).map(&:attributes))
        end

        it 'is not persisted' do
          expect(invoice.payments_or_credits.first.persisted?).to be_falsey
        end

        it_behaves_like 'expected totals and balance', total_amount: '100.0', payments_total_amount: '-10.0', balance: '90.0'
      end
    end
  end

  context '#subtotal_for_ui' do
    let(:invoice) { create(:invoice, line_items: line_items) }

    let(:li_flat) { create :invoicing_line_item, quantity: 1, unit_price: 20.0, description: 'Flat Rate' }
    let(:li_dolly) { create :invoicing_line_item, quantity: 1, unit_price: 50.0, description: 'Dolly' }
    let(:li_tax) { create :invoicing_line_item, tax_amount: 100.0, description: 'Tax' }

    context 'when invoice contains only rate default line_items' do
      let(:line_items) { [li_flat, li_dolly, li_tax] }

      it 'does not consider Tax items' do
        expect(invoice.subtotal_for_ui.to_s).to eq '70.0'
      end

      context 'when li_flat is deleted_at' do
        let(:li_flat) { create :invoicing_line_item, net_amount: '20.0', description: 'Flat Rate', deleted_at: Time.now }

        it 'does not consider deleted_at items' do
          expect(invoice.subtotal_for_ui.to_s).to eq '50.0'
        end
      end

      context 'when Invoice also contains a percent line_item' do
        let(:li_fuel_percent) { create :invoicing_line_item, calc_type: 'percent', net_amount: '50.0', quantity: '10.0', description: 'Fuel' }
        let(:line_items) { super() << li_fuel_percent }

        it 'does not consider percent line_items' do
          expect(invoice.subtotal_for_ui.to_s).to eq '70.0'
        end
      end
    end
  end

  describe "fix_lineitem_percentages" do
    it "updates the net amount of percentage line items based on the total of non-tax, non-storage items" do
      invoice = Invoice.new
      item_1 = invoice.line_items.build(quantity: 1, unit_price: 60)
      item_2 = invoice.line_items.build(quantity: 2, unit_price: 20)
      item_3 = invoice.line_items.build(quantity: 3, unit_price: 25, description: Rate::STORAGE)
      item_4 = invoice.line_items.build(quantity: 10, calc_type: "percent")
      invoice.line_items.build(tax_amount: 5.0, description: Rate::TAX)
      invoice.fix_lineitem_percentages
      expect(item_1.net_amount).to eq 60.0
      expect(item_2.net_amount).to eq 40.0
      expect(item_3.net_amount).to eq 75.0
      expect(item_4.net_amount).to eq 10.0
    end
  end

  describe '#derived_rate_type' do
    subject { invoice.send(:derived_rate_type) }

    # it will create an invoice with FlatRate line_items
    let(:invoice) { create(:invoice, line_items: line_items) }
    let(:line_items) { create_list(:invoicing_line_item, 2, net_amount: BigDecimal("50"), unit_price: BigDecimal("50"), rate: rate) }

    context 'when invoice is created with FlatRate items' do
      let(:rate) { create(:rate, type: 'FlatRate', flat: 20) }

      it { is_expected.to eq 'FlatRate' }

      context 'and it has a new rate set' do
        # manually simulate a rate change, in which case it marks current line_items as deleted_at,
        # and add new default ones from the new Rate
        before do
          invoice.line_items.update_all(deleted_at: Time.now)

          hours_p2p_rate = create(:hourly_p2p)

          invoice.line_items << create_list(:invoicing_line_item, 2, net_amount: BigDecimal("50"), unit_price: BigDecimal("50"), rate: hours_p2p_rate)

          invoice.reload
        end

        it { is_expected.to eq 'HoursP2PRate' }
      end
    end
  end

  describe "#friendly_type" do
    subject { invoice.friendly_rate_type }

    let!(:rate_type_flat) { create(:rate_type, :flat) }
    let!(:rate_type_hours_p2p) { create(:rate_type, :hours_port_to_port) }

    # it will create an invoice with FlatRate line_items
    let(:invoice) { create(:invoice, line_items: line_items) }
    let(:line_items) { create_list(:invoicing_line_item, 2, net_amount: BigDecimal("50"), unit_price: BigDecimal("50"), rate: rate) }
    let(:rate) { create(:rate, type: 'FlatRate', flat: 20) }

    context 'when invoice.rate_type attr is not set' do
      it { is_expected.to eq rate_type_flat } # derives from line_items.first.rate.type
    end

    context 'when invoice.rate_type is set to HoursP2P rate' do
      before do
        invoice.rate_type = rate_type_hours_p2p.type
        invoice.save!
      end

      it { is_expected.to eq rate_type_hours_p2p }
    end
  end

  describe "current_total_amount" do
    it "returns the total amount of the line items in memory" do
      invoice = Invoice.new
      invoice.line_items.build(net_amount: 5, tax_amount: 0.25)
      invoice.line_items.build(net_amount: 4, tax_amount: nil)
      invoice.line_items.build(net_amount: nil, tax_amount: 0.5)
      invoice.line_items.build(net_amount: 1, deleted_at: Time.now)
      invoice.line_items.build(net_amount: 2).mark_for_destruction
      expect(invoice.current_total_amount).to eq 9.75
      expect(invoice.total_amount).to eq nil
    end
  end

  describe "calculate_total_amount" do
    it "returns sets total amounts and returns the net amount of the line items in memory" do
      invoice = Invoice.new
      invoice.line_items.build(net_amount: 5, tax_amount: 0.25)
      invoice.line_items.build(net_amount: 4, tax_amount: nil)
      invoice.line_items.build(net_amount: nil, tax_amount: 0.5)
      invoice.line_items.build(net_amount: 1, deleted_at: Time.now)
      invoice.line_items.build(net_amount: 2).mark_for_destruction
      expect(invoice.calculate_total_amount).to eq 9
      expect(invoice.total_amount).to eq 9.75
      expect(invoice.tax_amount).to eq 0.75
      expect(invoice.fleet_ui_total_amount).to eq 9.75
    end

    it "returns sets the fleet UI total amount to zero on partner new or approved" do
      invoice = Invoice.new(state: 'partner_new')
      invoice.line_items.build(net_amount: 5, tax_amount: 0.25)
      invoice.calculate_total_amount
      expect(invoice.fleet_ui_total_amount).to eq 0
      invoice.state = 'partner_approved'
      invoice.calculate_total_amount
      expect(invoice.fleet_ui_total_amount).to eq 0
    end
  end

  describe "billing type" do
    it "sets the default billing type on fleet managed jobs when the sender is a rescue company" do
      invoice = build(:fleet_managed_job_invoice)
      expect(invoice.billing_type).to eq nil
      invoice.save!
      expect(invoice.reload.billing_type).to eq invoice.sender.billing_type
      expect(invoice.sender.billing_type).to eq BillingType::VCC
      invoice.billing_type = BillingType::ACH
      invoice.save!
      expect(invoice.reload.billing_type).to eq BillingType::ACH
    end

    it "does not set the billing type when the sender is not a rescue company" do
      invoice = build(:fleet_managed_job_invoice, sender: create(:fleet_company))
      expect(invoice.billing_type).to eq nil
      invoice.save!
      expect(invoice.reload.billing_type).to eq nil
    end

    it "does not set the billing type when the job is not a fleet managed job" do
      invoice = build(:fleet_in_house_job_invoice, sender: create(:rescue_company))
      expect(invoice.billing_type).to eq nil
      invoice.save!
      expect(invoice.reload.billing_type).to eq nil
    end
  end

  describe "editable?" do
    it "is only editable if the invoice is in an editable state" do
      job = Job.new
      invoice = Invoice.new(job: job, state: Invoice::PARTNER_NEW)
      expect(invoice.editable?).to eq true
      invoice.state = Invoice::SWOOP_APPROVED_PARTNER
      expect(invoice.editable?).to eq false
    end

    it "is always editable if the job says so" do
      job = FleetMotorClubJob.new
      expect(job.invoice_editable?).to eq true
      invoice = Invoice.new(job: job, state: Invoice::PARTNER_NEW)
      expect(invoice.editable?).to eq true
      invoice.state = Invoice::SWOOP_APPROVED_PARTNER
      expect(invoice.editable?).to eq true
    end
  end

  describe "auto approver" do
    let(:invoice) { create :invoice, :auto_approvable, state: :partner_approved }

    context "when changing to partner_sent" do
      subject { invoice.partner_sent! }

      it "works" do
        subject
        expect(InvoiceAutoApproverWorker).to have_enqueued_sidekiq_job(invoice.id)
      end
    end

    context "when changing to something else" do
      subject { invoice.partner_undo_approval! }

      it "works" do
        subject
        expect(InvoiceAutoApproverWorker).not_to have_enqueued_sidekiq_job
      end
    end
  end

  describe "#auto_approval_limit" do
    subject { invoice.auto_approval_limit }

    let(:job) { create :job, status: status }
    let(:invoice) { create :invoice, job: job }

    context "with a goa job" do
      let(:status) { :goa }

      it "works" do
        expect(subject).to eq(25.0)
      end
    end

    context "with a completed job" do
      let(:status) { :completed }

      it "works" do
        expect(subject).to eq(100.0)
      end
    end

    context "with a released job" do
      let(:status) { :released }

      it "works" do
        expect(subject).to eq(100.0)
      end
    end

    context "with an enroute job" do
      let(:status) { :enroute }

      it "works" do
        expect { subject }.to raise_error(described_class::AutoApprovalLimitError)
      end
    end
  end

  describe "#create" do
    let(:european_company) { create(:company, currency: 'EUR') }

    it 'requires currency be the same as sending company' do
      expect { create(:invoice, currency: 'USD', sender: european_company) }
        .to raise_error(ActiveRecord::RecordInvalid, /Currency must match Sender/)
    end

    it 'requires currency be the same as recipient company' do
      super_company = SuperCompany.find_by(currency: 'USD')
      expect { create(:invoice, currency: 'EUR', sender: european_company, recipient: super_company) }
        .to raise_error(ActiveRecord::RecordInvalid, /Currency must match Recipient/)
    end
  end

  describe '#find_active_fleet_managed_invoice' do
    subject { job.invoice.find_active_fleet_managed_invoice }

    let!(:job) { create :fleet_managed_job, :with_invoice }
    let!(:fleet_managed_invoice) { create :invoice, sender_id: Company.swoop_id, job_id: job.id, recipient: job.rescue_company }

    it { is_expected.to eq(fleet_managed_invoice) }

    context "without a fleet_managed_invoice" do
      let(:fleet_managed_invoice) { nil }

      it { is_expected.to be_nil }
    end
  end

  describe '#fleet_managed_invoice_approved_or_sent?' do
    subject { job.invoice.fleet_managed_invoice_approved_or_sent? }

    let!(:job) { create :fleet_managed_job, :with_invoice }

    context "with the right state" do
      let!(:fleet_managed_invoice) { create :invoice, state: Invoice::FLEET_MANGED_INVOICE_APPROVED_OR_SENT_STATES.sample, sender_id: Company.swoop_id, job_id: job.id, recipient: job.rescue_company }

      it { is_expected.to eq(true) }
    end

    context "with the wrong state" do
      let!(:fleet_managed_invoice) { create :invoice, state: :swoop_new, sender_id: Company.swoop_id, job_id: job.id, recipient: job.rescue_company }

      it { is_expected.to eq(false) }
    end

    context "without a fleet_managed_invoice" do
      let(:fleet_managed_invoice) { nil }

      it { is_expected.to eq(false) }
    end
  end
end
