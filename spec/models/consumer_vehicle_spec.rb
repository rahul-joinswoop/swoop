# frozen_string_literal: true

require "rails_helper"

describe ConsumerVehicle do
  let(:consumer_vehicle) { create(:consumer_vehicle) }

  it "creates customer_type" do
    expect(consumer_vehicle).to be_valid
  end
end
