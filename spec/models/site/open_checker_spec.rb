# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Site::OpenChecker do
  describe ".update_open_businesses!" do
    it "updates all open sites" do
      sites = create_list(:partner_site, 5, always_open: false, within_hours: true)

      Site::OpenChecker.update_open_businesses!
      sites.each { |s| expect(s.reload.within_hours).to eq(false) }
    end

    it "only updates open sites" do
      sites = create_list(:partner_site, 5, always_open: false, within_hours: true)
      special = sites.pop
      special.within_hours = false
      special.force_open = true
      special.compute_next_open_check!
      special.save!

      Site::OpenChecker.update_open_businesses!

      sites.each { |s| expect(s.reload.within_hours).to eq(false) }
      expect(special.reload.within_hours).to eq(false)
      expect(special.force_open).to eq(true)
    end
  end
end
