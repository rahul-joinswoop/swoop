# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TerritoryAuditTrail, type: :model do
  context 'fields' do
    it { is_expected.to respond_to(:action) }
  end

  context 'associations' do
    it { is_expected.to belong_to(:territory) }
    it { is_expected.to belong_to(:user) }
  end

  context 'validations' do
    it { should validate_presence_of(:action) }
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:territory) }
    it { should validate_inclusion_of(:action).in_array([:created, :destroyed]) }
  end
end
