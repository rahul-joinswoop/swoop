# frozen_string_literal: true

require "rails_helper"

describe ClientProgram do
  let(:client_company) { create(:fleet_company) }

  let!(:lincoln_for_life_client_program) do
    create(
      :client_program,
      name: 'lincoln_for_life',
      code: 'lincoln_for_life',
      coverage_notes: lincoln_for_life_coverage_notes_default,
      company: client_company,
      coverage_filename: 'covered',
    )
  end

  let(:lincoln_for_life_coverage_notes_default) { 'Default notes for this Lincoln for Life program' }

  let(:tow_service_code) { create(:service_code, name: ServiceCode::TOW) }

  describe '.manual_program' do
    subject { ClientProgram.manual_program(client_company.id) }

    context 'when there is a manual program set for the client company' do
      let!(:manual_entry) do
        create(
          :client_program,
          name: 'Manual Entry',
          company: client_company,
          coverage_filename: 'not_covered',
          coverage_notes: 'deprecated field',
        )
      end

      it { is_expected.to eq manual_entry }
    end

    context 'when there is NO manual program set for the client company' do
      it { is_expected.to be_nil }
    end
  end

  describe '.fetch_or_create_client_api_covered!' do
    subject { ClientProgram.fetch_or_create_client_api_covered!(client_company.id) }

    context 'when there is a client_api_covered set for the client company' do
      let!(:client_api_covered) do
        create(
          :client_program,
          client_identifier: ClientProgram::CLIENT_API_COVERED_CODE,
          code: ClientProgram::CLIENT_API_COVERED_CODE,
          company_id: client_company.id,
          coverage_filename: ClientProgram::COVERED_FILENAME,
          coverage_notes: ClientProgram::CLIENT_API_COVERED_NOTES,
          name: ClientProgram::CLIENT_API_COVERED,
        )
      end

      it { is_expected.to eq client_api_covered }
    end

    context 'when there is NO client_api_covered set for the client company' do
      it 'creates a new one' do
        expect { subject }.to change(ClientProgram, :count).by(1)

        client_api_covered_created = subject

        expect(client_api_covered_created.client_identifier).to eq ClientProgram::CLIENT_API_COVERED_CODE
        expect(client_api_covered_created.code).to eq ClientProgram::CLIENT_API_COVERED_CODE
        expect(client_api_covered_created.company_id).to eq client_company.id
        expect(client_api_covered_created.coverage_filename).to eq ClientProgram::COVERED_FILENAME
        expect(client_api_covered_created.coverage_notes).to eq ClientProgram::CLIENT_API_COVERED_NOTES
        expect(client_api_covered_created.name).to eq ClientProgram::CLIENT_API_COVERED
      end
    end
  end

  describe '#coverage_notes_by_filter_or_default' do
    subject { lincoln_for_life_client_program.coverage_notes_by_filter_or_default(service_code: service_code_filter) }

    context 'with service_code filter' do
      let(:service_code_filter) { tow_service_code }

      context 'when there is NO custom coverage notes for the given service_code' do
        it { is_expected.to eq lincoln_for_life_coverage_notes_default }
      end

      context 'when there is a custom coverage notes for the given service_code' do
        let!(:lincoln_for_life_coverage_notes_tow_service) do
          create(
            :client_program_service_notes,
            client_program: lincoln_for_life_client_program,
            notes: service_notes,
            service_code: tow_service_code,
          )
        end

        let(:service_notes) { 'Tow notes for Lincoln for Life program' }

        it { is_expected.to eq service_notes }
      end
    end

    context 'without service_code filter' do
      let(:service_code_filter) { nil }

      it { is_expected.to eq lincoln_for_life_coverage_notes_default }
    end
  end
end
