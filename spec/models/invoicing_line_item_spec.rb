# frozen_string_literal: true

require 'rails_helper'

describe InvoicingLineItem do
  describe "fix user input" do
    it "strips trailing decimals from the decimal fields" do
      [:quantity, :tax_amount, :unit_price, :original_unit_price, :original_quantity].each do |field|
        item = InvoicingLineItem.new
        item.send("#{field}=", nil)
        expect(item[field]).to eq nil
        item.send("#{field}=", 1.5)
        expect(item[field]).to eq 1.5
        item.send("#{field}=", "33.")
        expect(item[field]).to eq 33.0
      end
    end
  end

  describe "net_amount" do
    it "calculates the net amount when changing unit price or quantity" do
      item = InvoicingLineItem.new
      expect(item.net_amount).to eq nil
      item.quantity = 1
      expect(item.net_amount).to eq nil
      item.unit_price = 4.25
      expect(item.net_amount).to eq 4.25
      item.quantity = 2
      expect(item.net_amount).to eq 8.5
      item.unit_price = 5.0
      expect(item.net_amount).to eq 10.0
      item.quantity = nil
      expect(item.net_amount).to eq nil
    end

    it "does not override the net amount in the database until a value is changed" do
      item = create(:invoicing_line_item)
      item.update_columns(quantity: nil, unit_price: nil, net_amount: 55.55)
      item.reload
      expect(item.net_amount).to eq 55.55
      item.quantity = 2
      item.unit_price = 30
      expect(item.net_amount).to eq 60
    end

    it "correctly calculates the net amount on initialize if it is not provided" do
      item = InvoicingLineItem.new(quantity: 2, unit_price: 60)
      expect(item.net_amount).to eq 120
    end

    it "avoids a race condition when setting net_amount after unit price and quantity" do
      item = InvoicingLineItem.new(quantity: 2, unit_price: 60, net_amount: 10)
      expect(item.net_amount).to eq 120
    end

    it "avoids a race condition when setting net_amount between unit price and quantity" do
      item = InvoicingLineItem.new(quantity: 2, net_amount: 10, unit_price: 60)
      expect(item.net_amount).to eq 120
    end

    it "avoids a race condition when setting net_amount before unit price and quantity" do
      item = InvoicingLineItem.new(net_amount: 10, quantity: 2, unit_price: 60)
      expect(item.net_amount).to eq 120
    end
  end

  describe "percentage?" do
    it "returns true only if the calc type is percentage" do
      expect(InvoicingLineItem.new(calc_type: "flat").percentage?).to eq false
      expect(InvoicingLineItem.new(calc_type: "percent").percentage?).to eq true
    end

    it "returns the quantity as a percentage value only if the item is a percentage" do
      expect(InvoicingLineItem.new(quantity: 1, calc_type: "flat").percentage).to eq nil
      expect(InvoicingLineItem.new(quantity: 2, calc_type: "percent").percentage).to eq 0.02
      expect(InvoicingLineItem.new(calc_type: "percent").percentage).to eq 0.0
    end
  end

  describe "tax?" do
    it "determines if a line item is tax by description" do
      expect(InvoicingLineItem.new(description: Rate::TAX).tax?).to eq true
      expect(InvoicingLineItem.new(description: "Stuff").tax?).to eq false
    end
  end

  describe "clear_amounts" do
    it "clears the price, quantity, and tax" do
      item = InvoicingLineItem.new(quantity: 2, unit_price: 50, tax_amount: 5, net_amount: 100)
      item.clear_amounts
      expect(item.quantity).to eq 0
      expect(item.unit_price).to eq 50
      expect(item.tax_amount).to eq 0
      expect(item.net_amount).to eq 0
    end
  end

  describe '#create' do
    let(:invoice) { build_stubbed(:invoice, currency: 'EUR') }

    it 'requires currency match invoice' do
      expect { create(:invoicing_line_item, currency: 'USD', ledger_item: invoice) }
        .to raise_error(ActiveRecord::RecordInvalid, /Currency must match LedgerItem/)
    end
  end
end
