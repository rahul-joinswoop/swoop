# frozen_string_literal: true

require 'rails_helper'

describe FleetMotorClubJob do
  describe '#status_changed', freeze_time: true do
    subject(:update_digital_dispatcher_status) { job.update_digital_dispatcher_status }

    let(:job) do
      create(
        :fleet_motor_club_job,
        status: job_status,
        issc_dispatch_request: issc_dispatch_request
      )
    end

    let!(:account) do
      create(:account, company: rescue_company, client_company: fleet_company)
    end

    let(:issc_dispatch_request) do
      create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end

    let(:rescue_company) { create(:rescue_company) }
    let(:fleet_company) { create(:fleet_company, :ago) }

    context 'when job.digital_dispatcher is ISSC' do
      let(:issc) { create(:ago_issc, company: rescue_company) }

      shared_examples 'Async call Issc end to inform status change' do |issc_status|
        it 'calls schedules IsscDispatchStatus with expected params' do
          expect(IsscDispatchStatus).to receive(:perform_async).with(
            job.id, issc_status, :Manual, Time.now, Time.now
          )

          update_digital_dispatcher_status
        end
      end

      context "when rescue driver is assigned" do
        let(:driver) { create(:driver_on_duty) }
        let(:job_status) { 'Dispatched' }

        it "does not schedule an RSC worker when a driver is assigned" do
          Agero::Rsc::AssignDriverWorker.jobs.clear
          expect(NoOpWorker).to receive(:perform_async).and_call_original
          job.update!(rescue_driver: driver)
          expect(Agero::Rsc::AssignDriverWorker.jobs.size).to eq 0
        end
      end

      context 'when job.status is Dispatched' do
        let(:job_status) { 'Dispatched' }

        it_behaves_like 'Async call Issc end to inform status change', 'DriverAssigned'
      end

      context 'when job.status is En Route' do
        let(:job_status) { 'En Route' }

        it_behaves_like 'Async call Issc end to inform status change', 'Enroute'
      end

      context 'when job.status is On Site' do
        let(:job_status) { 'On Site' }

        it_behaves_like 'Async call Issc end to inform status change', 'OnScene'
      end

      context 'when job.status is Towing' do
        let(:job_status) { 'Towing' }

        it_behaves_like 'Async call Issc end to inform status change', 'EnrouteToDestination'
      end

      context 'when job.status is Tow Destination' do
        let(:job_status) { 'Tow Destination' }

        it_behaves_like 'Async call Issc end to inform status change', 'OnSceneDestination'
      end

      context 'when job.status is Stored' do
        let(:job_status) { 'Stored' }

        it_behaves_like 'Async call Issc end to inform status change', 'VehicleInStorage'
      end

      context 'when job.status is Completed' do
        let(:job_status) { 'Completed' }

        it_behaves_like 'Async call Issc end to inform status change', 'ServiceComplete'
      end

      context 'when job.status is Canceled' do
        let(:job_status) { 'Canceled' }

        it_behaves_like 'Async call Issc end to inform status change', 'ProviderCancel'
      end

      context 'when job.status is GOA' do
        let(:job_status) { 'GOA' }

        it_behaves_like 'Async call Issc end to inform status change', 'GOANeeded'
      end

      context 'when job.status is Rejected' do
        let(:job_status) { 'Rejected' }

        it 'shedules IsscDispatchResponseReject' do
          expect(IsscDispatchResponseReject).to receive(:perform_async).with(job.id)

          update_digital_dispatcher_status
        end
      end

      context 'when job.status is Accepted' do
        let(:job_status) { 'Accepted' }

        it 'does not schedule IsscDispatchStatus' do
          expect(IsscDispatchStatus).not_to receive(:perform_async).with(job.id)

          update_digital_dispatcher_status
        end

        it 'does not schedule IsscDispatchResponseAccept' do
          expect(IsscDispatchResponseAccept).not_to receive(:perform_async).with(job.id)

          update_digital_dispatcher_status
        end
      end

      context 'when job.status is Submitted' do
        let(:job_status) { 'Submitted' }

        it 'does not schedule IsscDispatchStatus' do
          expect(IsscDispatchStatus).not_to receive(:perform_async)

          update_digital_dispatcher_status
        end

        it 'schedule IsscDispatchResponseAccept' do
          expect(IsscDispatchResponseAccept).to receive(:perform_async).with(job.id)

          update_digital_dispatcher_status
        end
      end
    end

    context 'when job.digital_dispatcher is RSC' do
      let(:api_access_token) { create(:api_access_token) }
      let(:issc) do
        create(
          :ago_issc,
          company: rescue_company,
          api_access_token: api_access_token,
          system: Issc::RSC_SYSTEM
        )
      end

      shared_examples 'async calling RSC API to update job status to' do |swoop_status_id|
        if Agero::Rsc::Data.job_status_code(swoop_status_id)
          it 'schedules Agero::Rsc::ChangeDispatchStatusWorker with expected params' do
            expect(Agero::Rsc::ChangeDispatchStatusWorker).to receive(:perform_async).with(
              job.id, Agero::Rsc::Status.new(job).to_hash
            )

            update_digital_dispatcher_status
          end
        end
      end

      shared_examples 'async calling RSC API to update job cancel accordingly with' do |rsc_status_code, rsc_reason_code|
        it 'schedules Agero::Rsc::ChangeDispatchStatusWorker with expected params' do
          expect(Agero::Rsc::CancelDispatchWorker).to receive(:perform_async).with(
            job.id, rsc_status_code, rsc_reason_code
          )

          update_digital_dispatcher_status
        end
      end

      context "when rescue driver is assigned" do
        let(:driver) { create(:driver_on_duty) }
        let(:job_status) { 'Dispatched' }

        it "schedules a worker to assign the driver in RSC" do
          Agero::Rsc::AssignDriverWorker.jobs.clear
          job.update!(rescue_driver: driver)
          job_args = Agero::Rsc::AssignDriverWorker.jobs.map { |job| job["args"] }
          expect(job_args).to eq [[job.id]]
        end

        it "does not schedule a worker if the driver did not change" do
          job.update!(rescue_driver: driver)
          Agero::Rsc::AssignDriverWorker.jobs.clear
          job.update!(rescue_driver: driver)
          expect(Agero::Rsc::AssignDriverWorker.jobs.size).to eq 0
        end

        it "does not schedule a worker if there is no driver" do
          job.update!(rescue_driver: driver)
          Agero::Rsc::AssignDriverWorker.jobs.clear
          job.update!(rescue_driver: nil)
          expect(Agero::Rsc::AssignDriverWorker.jobs.size).to eq 0
        end
      end

      context 'when job.status is Accepted' do
        let(:job_status) { 'Accepted' }

        it 'does not schedule IsscDispatchStatus' do
          expect(Agero::Rsc::ChangeDispatchStatusWorker).not_to receive(:perform_async).with(job.id)

          update_digital_dispatcher_status
        end
      end

      context 'when job.status is Rejected' do
        let(:job_status) { 'Rejected' }

        it 'shedules Agero::Rsc::RejectDispatchWorker' do
          expect(Agero::Rsc::RejectDispatchWorker).to receive(:perform_async).with(job.id)

          update_digital_dispatcher_status
        end
      end

      context 'when job.status is Dispatched' do
        let(:job_status) { 'Dispatched' }

        it_behaves_like 'async calling RSC API to update job status to', JobStatus::DISPATCHED
      end

      context 'when job.status is En Route' do
        let(:job_status) { 'En Route' }

        it_behaves_like 'async calling RSC API to update job status to', JobStatus::EN_ROUTE
      end

      context 'when job.status is On Site' do
        let(:job_status) { 'On Site' }

        it_behaves_like 'async calling RSC API to update job status to', JobStatus::ON_SITE
      end

      context 'when job.status is Towing' do
        let(:job_status) { 'Towing' }

        it_behaves_like 'async calling RSC API to update job status to', JobStatus::TOWING
      end

      context 'when job.status is Tow Destination' do
        let(:job_status) { 'Tow Destination' }

        it_behaves_like 'async calling RSC API to update job status to', JobStatus::TOW_DESTINATION
      end

      context 'when job.status is Completed' do
        let(:job_status) { 'Completed' }

        it_behaves_like 'async calling RSC API to update job status to', JobStatus::COMPLETED
      end

      context 'when job.status is Canceled' do
        let(:job_status_id) { JobStatus::NAME_MAP[job_status.to_sym] }
        let(:job_status) { 'Canceled' }

        # job must have a cancel job_status_reason, required by RSC
        let!(:job_status_reason) do
          create_list(
            :job_status_reason,
            1,
            job: job,
            job_status_reason_type: job_status_reason_type,
            audit_job_status: audit_job_status,
          )
        end

        let(:job_status_reason_type) do
          create(:job_status_reason_type, key)
        end

        let(:audit_job_status) do
          create(:audit_job_status, job: job, job_status: job_status_reason_type.job_status)
        end

        context 'and job_status_reason_type.key = :cancel_prior_job_delayed' do
          let(:key) { :cancel_prior_job_delayed }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :service_provider_cancel,
            :prior_job_delayed
          )
        end

        context 'and job_status_reason_type.key = :cancel_traffic_service_vehicle_problem' do
          let(:key) { :cancel_traffic_service_vehicle_problem }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :service_provider_cancel,
            :traffic_vehicle_problem,
          )
        end

        context 'and job_status_reason_type.key = :cancel_out_of_area' do
          let(:key) { :cancel_out_of_area }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :service_provider_cancel,
            :out_of_area,
          )
        end

        context 'and job_status_reason_type.key = :cancel_another_job_priority' do
          let(:key) { :cancel_another_job_priority }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :service_provider_cancel,
            :another_job_priority,
          )
        end

        context 'and job_status_reason_type.key = :cancel_no_reason_given' do
          let(:key) { :cancel_no_reason_given }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :service_provider_cancel,
            :no_reason_given,
          )
        end

        context 'and job_status_reason_type.key = :cancel_customer_found_alternate_solution' do
          let(:key) { :cancel_customer_found_alternate_solution }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :customer_cancel,
            :found_alternate_solution,
          )
        end
      end

      context 'when job.status is GOA' do
        let(:job_status_id) { JobStatus::NAME_MAP[job_status.to_sym] }
        let(:job_status) { 'GOA' }

        # job must have a GOA job_status_reason, required by RSC
        let!(:job_status_reason) do
          create_list(
            :job_status_reason,
            1,
            job: job,
            job_status_reason_type: job_status_reason_type,
            audit_job_status: audit_job_status,
          )
        end

        let(:job_status_reason_type) do
          create(:job_status_reason_type, key)
        end

        let(:audit_job_status) do
          create(:audit_job_status, job: job, job_status: job_status_reason_type.job_status)
        end

        context 'and job_status_reason_type.key = :goa_customer_cancel_after_deadline' do
          let(:key) { :goa_customer_cancel_after_deadline }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :customer_cancel,
            :customer_cancel,
          )
        end

        context 'and job_status_reason_type.key = :goa_wrong_location_given' do
          let(:key) { :goa_wrong_location_given }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :unsuccessful_no_vehicle,
            :wrong_location_given,
          )
        end

        context 'and job_status_reason_type.key = :goa_customer_not_with_vehicle' do
          let(:key) { :goa_customer_not_with_vehicle }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :unsuccessful_no_customer,
            :customer_not_with_vehicle,
          )
        end

        context 'and job_status_reason_type.key = :goa_incorrect_service' do
          let(:key) { :goa_incorrect_service }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :service_attempt_failed,
            :incorrect_service,
          )
        end

        context 'and job_status_reason_type.key = :goa_incorrect_equipment' do
          let(:key) { :goa_incorrect_equipment }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :service_attempt_failed,
            :incorrect_equipment,
          )
        end

        context 'and job_status_reason_type.key = :goa_unsuccessful_service_attempt' do
          let(:key) { :goa_unsuccessful_service_attempt }

          it_behaves_like(
            'async calling RSC API to update job cancel accordingly with',
            :service_attempt_failed,
            :need_additional_equipment,
          )
        end
      end
    end
  end

  context 'publishing' do
    subject(:fmcj) do
      job.base_publish('update', {})
    end

    let(:job) do
      create(:fleet_motor_club_job, {
      })
    end

    it 'publishes job with invoice' do
      job.account = CreateAccountForJobService.new(job, job.fleet_company).call
      job.build_invoice(
        job: job,
        sender: job.rescue_company,
        recipient: job.account,
        currency: job.rescue_company&.currency,
      )
      job.save!
      allow(BaseModel).to receive(:publish) do |channel, json_string|
        if channel == "event_company_#{job.rescue_company.uuid}"
          args = JSON.parse(json_string)
          Rails.logger.debug "spec received: #{JSON.pretty_generate(args)}"
          expect(args["target"]["invoices"].length).to eql(1)
        end
      end
      subject
    end
  end

  context "transitions" do
    describe "issc_cancel" do
      it "allows transitioning even completed jobs back to cancelled since the external system is the source of record" do
        job = create(:fleet_motor_club_job, status: "Completed")
        job.issc_canceled!
        job.reload
        expect(job.canceled?).to eq true
      end
    end

    describe 'job completed' do
      subject(:job_status_changed) { job.job_status_change('partner_completed') }

      let!(:job) do
        create(
          :fleet_motor_club_job,
          status: 'On Site'
        )
      end

      it 'raises AASM::InvalidTransition exception' do
        expect do
          subject
          job.save!
          job.job_status_change('partner_completed')
        end.to raise_error(AASM::InvalidTransition)
      end
    end
  end

  describe "callbacks" do
    describe "Notifiable.trigger_eta_requested" do
      let!(:job) { create :fleet_motor_club_job }

      it "works on a status change" do
        expect(Notifiable).to receive(:trigger_eta_requested).once.with(job)
        job.assigned!
      end

      it "works on a rescue_company change" do
        expect(Notifiable).to receive(:trigger_eta_requested).once.with(job)
        job.update! rescue_company: create(:rescue_company)
      end
    end

    describe "update_blank_po_number" do
      context "an RSC job and the po_number is nil" do
        let!(:job) { create :fleet_motor_club_job, po_number: nil }
        let(:issc_dispatch) { double(dispatchid?: true) }

        before do
          allow(job).to receive(:issc_system).and_return('rsc')
          allow(job).to receive(:issc_dispatch_request) { issc_dispatch }
        end

        it "triggers the po number update worker" do
          expect(Agero::Rsc::DispatchJobPoNumberUpdateWorker).to receive(:perform_async).once.with(job.id)
          job.assigned!
        end

        context "the feature is turned off" do
          before do
            Flipper.enable(:block_rsc_job_populate_po_number)
          end

          it "does NOT trigger the po number update worker" do
            expect(Agero::Rsc::DispatchJobPoNumberUpdateWorker).not_to receive(:perform_async)
            job.assigned!
          end
        end
      end

      context "NOT an RSC job and the po_number is nil and this feature is on" do
        let!(:job) { create :fleet_motor_club_job, po_number: nil }

        it "does NOT trigger the po number update worker" do
          expect(Agero::Rsc::DispatchJobPoNumberUpdateWorker).not_to receive(:perform_async)
          job.assigned!
        end
      end
    end
  end
end
