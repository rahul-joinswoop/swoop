# frozen_string_literal: true

require "rails_helper"

describe TimeOfArrival do
  let(:job) { create :job, status: :accepted }
  let(:user) { create :user, company: job.fleet_company }
  let!(:email_pref) { create :job_status_email_preference, :eta_extended, user_id: user.id }
  let!(:toa) { create :time_of_arrival, job: job, time: Time.now, eba_type: TimeOfArrival::BID }

  describe "after create" do
    context 'if the eta is more than 15 minutes' do
      let(:time) { 1.hour.from_now }

      it "calls #send_eta_extension_email when creating an estimate" do
        expect(job).to receive(:send_eta_extension_email).once.and_call_original
        create(:time_of_arrival, job: job, time: time, eba_type: TimeOfArrival::SWOOP_ESTIMATE)
      end

      it "doesn't call #send_eta_extension_email for live updates" do
        expect(job).not_to receive(:send_eta_extension_email)
        create(:time_of_arrival, job: job, time: time, eba_type: TimeOfArrival::ESTIMATE)
      end

      context "without a user" do
        let!(:user) { nil }
        let!(:email_pref) { nil }

        it "doesn't call #send_eta_extension_email" do
          expect(job).not_to receive(:send_eta_extension_email)
          create(:time_of_arrival, job: job, time: time, eba_type: TimeOfArrival::SWOOP_ESTIMATE)
        end
      end
    end

    context 'if the eta is < 15 minutes' do
      let(:time) { 1.minute.from_now }

      it "doesn't call #send_eta_extension_email" do
        expect(job).not_to receive(:send_eta_extension_email)
        create(:time_of_arrival, job: job, time: time, eba_type: TimeOfArrival::SWOOP_ESTIMATE)
      end
    end
  end

  context 'methods' do
    let(:territory) { create(:territory, company: rescue_company) }

    context '#changed_by_company' do
      subject(:time_of_arrival) do
        build_stubbed(:time_of_arrival, user: user, source_application: source_application, job: job, time: Time.now, eba_type: TimeOfArrival::FLEET).changed_by_company
      end

      let!(:user) { nil }
      let!(:email_pref) { nil }
      let(:source_application) { nil }

      context 'when user is set' do
        let(:user) { create(:user, :rescue) }

        it { is_expected.to eq user.company }
      end

      context 'when source_application is set' do
        let(:doorkeeper_application) { create :doorkeeper_application }
        let(:source_application) { SourceApplication.instance(platform: SourceApplication::PLATFORM_CLIENT_API, oauth_application_id: doorkeeper_application.id) }

        it { is_expected.to eq Company.find(source_application.oauth_application.owner_id) }
      end
    end

    context '#ui_type' do
      subject(:time_of_arrival) do
        build_stubbed(:time_of_arrival, user: user, job: job, time: Time.now, eba_type: TimeOfArrival::FLEET).ui_type
      end

      context 'is set to TOA' do
        it { is_expected.to eq 'TOA' }
      end
    end

    context '#user_name' do
      subject(:time_of_arrival) do
        build_stubbed(:time_of_arrival, user: user, job: job, time: Time.now, eba_type: TimeOfArrival::FLEET).user_name
      end

      it { is_expected.to eq user.name }
    end

    context '#company' do
      subject(:time_of_arrival) do
        build_stubbed(:time_of_arrival, user: user, job: job, time: Time.now, eba_type: TimeOfArrival::FLEET).company
      end

      it { is_expected.to eq user&.company }
    end

    context '#description' do
      subject(:time_of_arrival) do
        build_stubbed(:time_of_arrival, user: user, job: job, time: Time.now + (1 * 60 * 60), eba_type: TimeOfArrival::FLEET).description
      end

      it { is_expected.to eq "ETA Entered by Fleet of 60min" }
    end

    context '#last_set_dttm' do
      subject(:time_of_arrival) do
        build_stubbed(:time_of_arrival, user: user, job: job, time: Time.now, eba_type: TimeOfArrival::FLEET).last_set_dttm
      end

      it { is_expected.to be_within(1.second).of Time.now }
    end

    context '#adjusted_dttm' do
      subject(:time_of_arrival) do
        build_stubbed(:time_of_arrival, user: user, job: job, time: Time.now, eba_type: TimeOfArrival::FLEET).adjusted_dttm
      end

      it { is_expected.to be_within(1.second).of Time.now }
    end
  end
end
