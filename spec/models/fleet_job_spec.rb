# frozen_string_literal: true

require "rails_helper"

describe FleetJob do
  describe "#partner_set_driver" do
    subject do
      fleet_job.partner_set_driver
    end

    let(:fleet_job) do
      create(:fleet_motor_club_job,
             status: job_status,
             fleet_manual: true)
    end

    context "when job is pending" do
      let(:job_status) { JobStatus::NAMES[:PENDING] }

      it "sets job status to dispatched" do
        subject

        expect(fleet_job.status).to eq(
          JobStatus::NAMES[:DISPATCHED].downcase
        )
      end
    end

    context "when job is assigned" do
      let(:job_status) { Job::ACCEPTED }

      it "sets job status to dispatched" do
        expect { subject }.to change(fleet_job, :status).to(
          JobStatus::NAMES[:DISPATCHED].downcase
        )
      end
    end
  end
end
