# frozen_string_literal: true

require "rails_helper"

describe Estimate do
  describe "run" do
    let(:job) { build(:job) }
    let(:site_location) { build(:location, lat: 45.0, lng: -110.0) }
    let(:service_location) { build(:location, lat: 45.1, lng: -110.1) }
    let(:drop_location) { build(:location, lat: 45.2, lng: -110.2) }
    let(:time) { 1.minute.from_now }

    it "calculates the AB/BA distance as site location to service location" do
      expect(External::GoogleDistanceMatrixService).to receive(:calculate).with(site_location, service_location, time).and_return([[1000, 60]])
      expect(External::GoogleDistanceMatrixService).to receive(:calculate).with(service_location, site_location, nil).and_return([[1001, 61]])
      estimate = Estimate.new(job: job, site_location: site_location, service_location: service_location, start_at: time)
      estimate.run
      expect(estimate.meters_ab).to eq 1000
      expect(estimate.seconds_ab).to eq 60
      expect(estimate.meters_ba).to eq 1001
      expect(estimate.seconds_ba).to eq 61
    end

    it "calculates the BC distance as service location to drop location" do
      expect(External::GoogleDistanceMatrixService).to receive(:calculate).with(service_location, drop_location, nil).and_return([[1000, 60]])
      estimate = Estimate.new(job: job, drop_location: drop_location, service_location: service_location, start_at: time)
      estimate.run
      expect(estimate.meters_bc).to eq 1000
      expect(estimate.seconds_bc).to eq 60
    end

    it "calculates the CA distance as drop location to site location" do
      expect(External::GoogleDistanceMatrixService).to receive(:calculate).with(drop_location, site_location, nil).and_return([[1000, 60]])
      estimate = Estimate.new(job: job, drop_location: drop_location, site_location: site_location, start_at: time)
      estimate.run
      expect(estimate.meters_ca).to eq 1000
      expect(estimate.seconds_ca).to eq 60
    end

    it "handles invalid distance calculations" do
      expect(External::GoogleDistanceMatrixService).to receive(:calculate).with(drop_location, site_location, nil).and_return([[nil, nil]])
      estimate = Estimate.new(job: job, drop_location: drop_location, site_location: site_location, start_at: time)
      estimate.run
      expect(estimate.meters_ca).to eq nil
      expect(estimate.seconds_ca).to eq nil
    end
  end
end
