# frozen_string_literal: true

require 'rails_helper'

describe IsscLocation do
  describe "sync site to issc" do
    it "syncs the site to the issc row" do
      site = create(:partner_site)
      fleet_company = create(:fleet_company)
      issc_location = create(:issc_location, site: nil, fleet_company: fleet_company, locationid: "1")
      issc = create(:isscs, issc_location: issc_location, site_id: nil)
      other_issc = create(:isscs, site_id: nil)
      issc_location.update!(site_id: site.id)
      issc.reload
      expect(issc.site_id).to eq site.id
      expect(other_issc.reload.site_id).to eq nil
    end
  end
end
