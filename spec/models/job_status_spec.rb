# frozen_string_literal: true

require "rails_helper"

describe JobStatus do
  subject(:job_status_for_job_mark_paid) do
    described_class.job_status_for_job_mark_paid
  end

  describe '.job_status_for_job_mark_paid' do
    it "sets its id to #{JobStatus::MARK_PAID}" do
      expect(job_status_for_job_mark_paid.id).to eq JobStatus::MARK_PAID
    end

    it "sets its name to #{JobStatus::ID_MAP[JobStatus::MARK_PAID]}" do
      expect(job_status_for_job_mark_paid.name).to eq JobStatus::ID_MAP[JobStatus::MARK_PAID]
    end
  end

  describe "status_id" do
    it "looks up a status id from the status code" do
      expect(JobStatus.status_id("onsite")).to eq 4
      expect(JobStatus.status_id(:pending)).to eq 1
      expect(JobStatus.status_id("Existentially Vacant")).to eq nil
      expect(JobStatus.status_id(nil)).to eq nil
    end
  end
end
