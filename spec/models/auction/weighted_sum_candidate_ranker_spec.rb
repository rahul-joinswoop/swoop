# frozen_string_literal: true

require 'rails_helper'

describe Auction::WeightedSumRanker, type: :model do
  it "raises an exception because weights must sum to zero" do
    expect { Auction::WeightedSumRanker.create!(version: 1, weights: { speed: 0.3, quality: 0, cost: 0.5 }, constraints: { max_eta: 40, max_cost: 100, min_rating: 5 }) }.to raise_exception(ActiveRecord::RecordInvalid)
  end

  skip 'calculates rank number' do
    Auction::WeightedSumRanker.create!(version: 1, weights: { speed: 0.8, quality: 0.1, cost: 0.1 }, constraints: { max_eta: 40, max_cost: 100, min_rating: 5 })
  end
end
