# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples "auction created" do
  it 'returns an auction' do
    expect(subject).to be_an Auction::Auction
  end
end

RSpec.shared_examples 'total number of ranker increasing idempotently' do |from, to|
  it 'increases the total number of rankers in the system' do
    expect { subject }.to change(Auction::WeightedSumRanker, :count).from(from).to(to)
  end
  it "calling it repeatedly doesn't create extra rankers" do
    expect { subject }.to change(Auction::WeightedSumRanker, :count).from(from).to(to)
    expect { subject }.not_to change(Auction::WeightedSumRanker, :count)
    expect { subject }.not_to change(Auction::WeightedSumRanker, :count)
  end
end

RSpec.shared_examples 'total number of rankers staying the same idempotently' do
  it 'does not increase the total number of rankers in the system' do
    expect { subject }.not_to change(Auction::WeightedSumRanker, :count)
  end
  it "calling it repeatedly doesn't create any rankers" do
    expect { subject }.not_to change(Auction::WeightedSumRanker, :count)
    expect { subject }.not_to change(Auction::WeightedSumRanker, :count)
    expect { subject }.not_to change(Auction::WeightedSumRanker, :count)
  end
end

RSpec.shared_examples "live auction ranker" do
  it 'ranker is live' do
    expect(subject.ranker.live).to eql true
  end
end

RSpec.shared_examples "non-live auction ranker" do
  it 'ranker is not live' do
    expect(subject.ranker.live).to be_falsey
  end
end

RSpec.shared_examples 'priority response job' do
  it 'the job is priority response' do
    expect(job.priority_response).to eql true
    subject # Hack to shushh expect...recieve
  end
end

RSpec.shared_examples 'non-priority response job' do
  it 'the job is not priority response' do
    expect(job.priority_response).to be_falsey
    subject # Hack to shushh expect...recieve
  end
end

RSpec.shared_examples "preexisting ranker" do
  it "returns the preexisting ranker" do
    expect(subject.ranker.name).to eq preexisting_ranker.name
    expect(subject.ranker.weights).not_to be_nil
    expect(subject.ranker.weights.keys).to contain_exactly("speed", "quality", "cost")
    expect(subject.ranker.weights["speed"]).to eql preexisting_ranker.weights["speed"]
    expect(subject.ranker.weights["quality"]).to eql preexisting_ranker.weights["quality"]
    expect(subject.ranker.weights["cost"]).to eql preexisting_ranker.weights["cost"]
    expect(subject.ranker.constraints).not_to be_nil
    expect(subject.ranker.constraints.keys).to contain_exactly("max_cost", "max_eta", "max_rating", "min_cost", "min_eta", "min_rating")
    expect(subject.ranker.constraints["min_eta"]).to eql preexisting_ranker.constraints["min_eta"]
    expect(subject.ranker.constraints["max_eta"]).to eql preexisting_ranker.constraints["max_eta"]
    expect(subject.ranker.constraints["min_cost"]).to eql preexisting_ranker.constraints["min_cost"]
    expect(subject.ranker.constraints["max_cost"]).to eql preexisting_ranker.constraints["max_cost"]
    expect(subject.ranker.constraints["min_rating"]).to eql preexisting_ranker.constraints["min_rating"]
    expect(subject.ranker.constraints["max_rating"]).to eql preexisting_ranker.constraints["max_rating"]
    expect(subject.ranker.client).to eql job.fleet_company
  end
end

RSpec.shared_examples "priority response ranker based on the preexisting ranker" do
  it "returns the priority response ranker based on the preexisting ranker" do
    expect(subject.ranker.name).to eq "Priority response"
    expect(subject.ranker.weights).not_to be_nil
    expect(subject.ranker.weights.keys).to contain_exactly("speed", "quality", "cost")
    expect(subject.ranker.weights["speed"]).to eql 0.98
    expect(subject.ranker.weights["quality"]).to eql 0.01
    expect(subject.ranker.weights["cost"]).to eql 0.01
    expect(subject.ranker.constraints).not_to be_nil
    expect(subject.ranker.constraints.keys).to contain_exactly("max_cost", "max_eta", "max_rating", "min_cost", "min_eta", "min_rating")
    expect(subject.ranker.constraints["min_eta"]).to eql preexisting_ranker.constraints["min_eta"]
    expect(subject.ranker.constraints["max_eta"]).to eql preexisting_ranker.constraints["max_eta"]
    expect(subject.ranker.constraints["min_cost"]).to eql preexisting_ranker.constraints["min_cost"]
    expect(subject.ranker.constraints["max_cost"]).to eql preexisting_ranker.constraints["max_cost"]
    expect(subject.ranker.constraints["min_rating"]).to eql preexisting_ranker.constraints["min_rating"]
    expect(subject.ranker.constraints["max_rating"]).to eql preexisting_ranker.constraints["max_rating"]
    expect(subject.ranker.client).to eql job.fleet_company
  end
end

RSpec.shared_examples "priority response ranker based on the default ranker" do
  it "returns the priority response ranker based on the default ranker" do
    expect(subject.ranker.name).to eq "Priority response"
    expect(subject.ranker.weights).not_to be_nil
    expect(subject.ranker.weights.keys).to contain_exactly("speed", "quality", "cost")
    expect(subject.ranker.weights["speed"]).to eql 0.98
    expect(subject.ranker.weights["quality"]).to eql 0.01
    expect(subject.ranker.weights["cost"]).to eql 0.01
    expect(subject.ranker.constraints).not_to be_nil
    expect(subject.ranker.constraints.keys).to contain_exactly("max_cost", "max_eta", "max_rating", "min_cost", "min_eta", "min_rating")
    expect(subject.ranker.constraints["min_eta"]).to eql 5
    expect(subject.ranker.constraints["max_eta"]).to eql 60
    expect(subject.ranker.constraints["min_cost"]).to eql 20
    expect(subject.ranker.constraints["max_cost"]).to eql 150
    expect(subject.ranker.constraints["min_rating"]).to eql 40
    expect(subject.ranker.constraints["max_rating"]).to eql 100
    expect(subject.ranker.client).to eql job.fleet_company
  end
end

RSpec.shared_examples "default ranker" do
  it 'returns a new default ranker' do
    expect(subject.ranker.name).to eq('Fully speed weighted (ETA)')
    expect(subject.ranker.weights).not_to be_nil
    expect(subject.ranker.weights.keys).to contain_exactly("speed", "quality", "cost")
    expect(subject.ranker.weights["speed"]).to eql 1.0
    expect(subject.ranker.weights["quality"]).to eql 0.0
    expect(subject.ranker.weights["cost"]).to eql 0.0
    expect(subject.ranker.constraints).not_to be_nil
    expect(subject.ranker.constraints.keys).to contain_exactly("max_cost", "max_eta", "max_rating", "min_cost", "min_eta", "min_rating")
    expect(subject.ranker.constraints["min_eta"]).to eql 5
    expect(subject.ranker.constraints["max_eta"]).to eql 60
    expect(subject.ranker.constraints["min_cost"]).to eql 20
    expect(subject.ranker.constraints["max_cost"]).to eql 150
    expect(subject.ranker.constraints["min_rating"]).to eql 40
    expect(subject.ranker.constraints["max_rating"]).to eql 100
    expect(subject.ranker.client).to eql job.fleet_company
  end
end

RSpec.shared_examples "100%-cost-based auction ranker" do
  it 'returns a 100%-cost-based ranker' do
    expect(subject.ranker.name).to eq('Fully cost weighted')
    expect(subject.ranker.weights).not_to be_nil
    expect(subject.ranker.weights.keys).to contain_exactly("speed", "quality", "cost")
    expect(subject.ranker.weights["speed"]).to eql 0.0
    expect(subject.ranker.weights["quality"]).to eql 0.0
    expect(subject.ranker.weights["cost"]).to eql 1.0
    expect(subject.ranker.constraints).not_to be_nil
    expect(subject.ranker.constraints.keys).to contain_exactly("max_cost", "max_eta", "max_rating", "min_cost", "min_eta", "min_rating")
    expect(subject.ranker.constraints["min_eta"]).to eql 5
    expect(subject.ranker.constraints["max_eta"]).not_to be_nil
    expect(subject.ranker.constraints["max_eta"]).to be_an Integer
    expect(subject.ranker.constraints["min_cost"]).to eql 20
    expect(subject.ranker.constraints["max_cost"]).to eql 150
    expect(subject.ranker.constraints["min_rating"]).to eql 40
    expect(subject.ranker.constraints["max_rating"]).to eql 100
    expect(subject.ranker.client).to eql job.fleet_company
  end
end

describe Auction::Auction, type: :model do
  describe 'UNAUCTIONABLE_CLASS_TYPES' do
    subject { Auction::Auction::UNAUCTIONABLE_CLASS_TYPE_NAMES.include?(vehicle_category&.name) }

    context 'when vehicle_category is Heavy Duty' do
      let(:vehicle_category) { create(:vehicle_category, name: VehicleCategory::HEAVY_DUTY) }

      it { is_expected.to be_truthy }
    end

    context 'when vehicle_category is Flatbad' do
      let(:vehicle_category) { create(:vehicle_category, name: VehicleCategory::FLATBED) }

      it { is_expected.to be_falsey }
    end

    context 'when vehicle_category is nil' do
      let(:vehicle_category) { nil }

      it { is_expected.to be_falsey }
    end
  end

  describe '.get_or_create_ranker' do
    subject { Auction::Auction.build_for_job(job: job) }

    let!(:exitprise_ranker) { create(:weighted_sum_ranker, :sensible_constraints, name: "Custom ranker", live: true, weights: { "cost" => 0.5, "quality" => 0.3, "speed" => 0.2 }) }
    let(:exitprise_fleet_company) { create(:fleet_company, name: "Exitprise RentAHorse", ranker: exitprise_ranker) }
    let(:job_with_existing_ranker) { create(:fleet_managed_job, fleet_company: exitprise_fleet_company) }
    let(:job_without_existing_ranker) { create(:fleet_managed_job) }
    let(:priority_response_job_with_existing_ranker) { create(:fleet_managed_job, priority_response: true, fleet_company: exitprise_fleet_company) }
    let(:priority_response_job_without_existing_ranker) { create(:fleet_managed_job, priority_response: true) }

    context 'job is priority response' do
      context 'Split experiment enabled or disabled' do
        before(:each) { expect(External::Split).not_to receive(:treatment) }

        context 'fleet has a non-priority response ranker' do
          let(:job) { priority_response_job_with_existing_ranker }
          let(:preexisting_ranker) { exitprise_ranker }

          it_behaves_like 'priority response job'
          it_behaves_like 'auction created'
          it_behaves_like 'total number of ranker increasing idempotently', 1, 2
          it_behaves_like "priority response ranker based on the preexisting ranker"
          it_behaves_like "non-live auction ranker"
        end

        context 'fleet does not have a non-priority response ranker' do
          let(:job) { priority_response_job_without_existing_ranker }
          let(:preexisting_ranker) { nil }

          it_behaves_like 'priority response job'
          it_behaves_like 'auction created'
          it_behaves_like 'total number of ranker increasing idempotently', 1, 3
          it_behaves_like "priority response ranker based on the default ranker"
          it_behaves_like "non-live auction ranker"
        end
      end
    end

    context 'job is not priority response' do
      context 'Split experiment enabled' do
        before(:each) { allow(External::Split).to receive(:treatment).and_return('on') }

        context 'fleet has a non-priority response ranker' do
          let(:job) { job_with_existing_ranker }
          let(:preexisting_ranker) { exitprise_ranker }

          it_behaves_like 'non-priority response job'
          it_behaves_like 'auction created'
          it_behaves_like 'total number of ranker increasing idempotently', 1, 2
          it_behaves_like "100%-cost-based auction ranker"
          it_behaves_like "non-live auction ranker"
        end

        context 'fleet does not have a non-priority response ranker' do
          let(:job) { job_without_existing_ranker }
          let(:preexisting_ranker) { nil }

          it_behaves_like 'non-priority response job'
          it_behaves_like 'auction created'
          it_behaves_like 'total number of ranker increasing idempotently', 1, 2
          it_behaves_like "100%-cost-based auction ranker"
          it_behaves_like "non-live auction ranker"
        end
      end

      context 'Split experiment disabled' do
        before(:each) { allow(External::Split).to receive(:treatment).and_return('off') }

        context 'fleet has a non-priority response ranker' do
          let(:job) { job_with_existing_ranker }
          let(:preexisting_ranker) { exitprise_ranker }

          it_behaves_like 'non-priority response job'
          it_behaves_like 'auction created'
          it_behaves_like 'total number of rankers staying the same idempotently'
          it_behaves_like "preexisting ranker"
          it_behaves_like "live auction ranker"
        end

        context 'fleet does not have a non-priority response ranker' do
          let(:job) { job_without_existing_ranker }
          let(:preexisting_ranker) { nil }

          it_behaves_like 'non-priority response job'
          it_behaves_like 'auction created'
          it_behaves_like 'total number of ranker increasing idempotently', 1, 2
          it_behaves_like "default ranker"
          it_behaves_like "live auction ranker"
        end
      end
    end
  end
end
