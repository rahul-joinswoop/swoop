# frozen_string_literal: true

require 'rails_helper'
describe Auction::Bid do
  describe "callbacks" do
    subject { bid.update! status: Auction::Bid::WON }

    let(:bid) { create :bid }

    describe "Subscribable.trigger_job_updated" do
      it 'works' do
        expect(Subscribable).to receive(:trigger_job_updated).once.with(bid.job)
        subject
      end
    end

    describe "Subscribable.trigger_auction_bid_status_changed" do
      it 'works' do
        expect(Subscribable).to receive(:trigger_auction_bid_status_changed).once.with(bid)
        subject
      end
    end

    describe "Notifiable.trigger_eta_requested" do
      it 'works' do
        expect(Notifiable).to receive(:trigger_eta_requested).once.with(bid)
        subject
      end
    end

    describe "Notifiable.trigger_eta_accepted" do
      it 'works' do
        expect(Notifiable).to receive(:trigger_eta_accepted).once.with(bid)
        subject
      end
    end
  end

  describe '#subscribers' do
    subject { bid.subscribers }

    let(:rescue_company) { create :rescue_company }
    let(:bid) { create :bid, company: rescue_company }

    context 'dispatchers' do
      let!(:dispatchers) { create_list :user, 3, :rescue, :dispatcher, company: rescue_company }

      it "works" do
        expect(subject).to eq(Set.new([rescue_company, *dispatchers]))
      end
    end

    context 'oauth applications' do
      let!(:oauth_application) { create :oauth_application, owner: rescue_company }

      it "works" do
        expect(subject).to eq(Set.new([rescue_company, oauth_application]))
      end
    end
  end

  describe '#job_ssid' do
    let(:bid) { create :bid }

    it 'works' do
      expect(bid.job_ssid).to eq(SomewhatSecureID.encode(bid.job, bid))
    end
  end
end
