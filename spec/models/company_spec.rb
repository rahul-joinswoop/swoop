# frozen_string_literal: true

require "rails_helper"

describe Company, type: :model do
  before :each do
    standard_services_for_spec.each do |service_name|
      ServiceCode.where(name: service_name).first_or_create
    end

    standard_addons_for_spec.each do |addon_name|
      ServiceCode.where(name: addon_name).first_or_create
    end
  end

  let(:company) { create(:company) }

  it "creates company" do
    expect(company).to be_valid
  end

  describe '.create' do
    describe '#language' do
      it 'defaults to English' do
        expect(create(:company).language).to eq('en')
      end

      it 'may be set to German' do
        expect(create(:company, language: 'de').language).to eq('de')
      end

      it 'rejects other values' do
        expect { create(:company, language: 'es') }
          .to raise_error(ActiveRecord::RecordInvalid, /es is not supported/)
      end
    end
  end

  describe "special companies" do
    it "does not allow special companies to change names" do
      Company::SPECIAL_COMPANY_NAMES.each do |name|
        company = create(:super_company, name: name)
        company.name = "New #{name}"
        expect(company).to have(1).error_on(:name)
      end

      other_company = create(:super_company, name: "Some Non-Special Name")
      other_company.name = "Changed Name"
      expect(other_company).to be_valid
    end

    it "does not allow other companies to change their name to a special company name" do
      create(:super_company, :swoop)
      company = Company.new(name: Company::SWOOP)
      expect(company).to have(1).error_on(:name)
    end

    it "finds Swoop" do
      create(:super_company, :swoop)
      expect(Company.swoop.name).to eq Company::SWOOP
      expect(Company.swoop.id).to eq Company.swoop_id
    end

    it "finds Tesla" do
      create(:super_company, name: Company::TESLA)
      expect(Company.tesla.name).to eq Company::TESLA
      expect(Company.tesla.id).to eq Company.tesla_id
    end

    it "finds Enterprise" do
      create(:super_company, name: Company::ENTERPRISE)
      expect(Company.enterprise.name).to eq Company::ENTERPRISE
      expect(Company.enterprise.id).to eq Company.enterprise_id
    end

    it "finds Turo" do
      create(:super_company, name: "Turo")
      expect(Company.turo.name).to eq "Turo"
      expect(Company.turo.id).to eq Company.turo_id
    end

    it "finds Farmers" do
      create(:super_company, name: Company::FARMERS)
      expect(Company.farmers.name).to eq Company::FARMERS
      expect(Company.farmers.id).to eq Company.farmers_id
    end
  end

  describe "#rates_by_account" do
    let(:company) { build_stubbed :company }

    it "raises ArgumentError without :account_id" do
      expect do
        company.rates_by_account(service_code_id: 123)
      end.to raise_error(ArgumentError, "Account is required")
    end
  end

  describe "#live_rates?" do
    subject(:live_rates?) do
      company.live_rates?(account: account, service_code: service_code)
    end

    let(:account) { create :account, company: company }
    let(:company) { create :company }
    let(:service_code) { create :service_code }

    it "returns true when a live rate is associated" do
      create :flat_rate,
             account: account, service_code: service_code, company: company,
             live: true
      expect(live_rates?).to be true
    end

    it "returns false when a rate associated is not live" do
      create :flat_rate,
             account: account, service_code: service_code, company: company
      expect(live_rates?).to be false
    end

    it "returns false" do
      expect(live_rates?).to be false
    end
  end

  describe "#defaults" do
    let(:company) { create :company }
    let(:vehicle_category) { create :vehicle_category }

    before do
      allow(VehicleCategory).to receive(:standard_items).and_return([vehicle_category])
      allow_any_instance_of(described_class).to receive(:add_symptoms)
    end

    it "adds 'Flatbed' as a default VehicleCategory" do
      flat_bed = VehicleCategory.find_by(name: VehicleCategory::FLATBED)
      company.defaults

      expect(company.vehicle_categories.include?(flat_bed)).to be(true)
    end
  end

  describe "#add_feature_by_name" do
    let(:company) { create :company }
    let(:feature) { create :feature, name: Feature::QUESTION }

    it "assigns Feature instances based on given Feature names" do
      expect(company.features).not_to include(feature)
      company.add_features_by_name([Feature::QUESTION])
      company.reload
      expect(company.features).to include(feature)
    end
  end

  describe "admins" do
    it "returns all non-deleted admin users" do
      company = create(:company)
      user_1 = create(:user, company: company)
      create(:user, :admin)
      admin_2 = create(:user, :admin, company: company)
      deleted_admin = create(:user, :admin, company: company, deleted_at: 1.minute.ago)
      company.users.concat([user_1, admin_2, deleted_admin])
      expect(company.admins).to match_array [admin_2]
    end
  end

  describe "ElasticSearch" do
    it "has a scope to preload associations for ElasticSearch" do
      expect(Company.elastic_search_preload.to_a).to be_a(Array)
    end
  end

  describe "setting" do
    it "returns the setting value by key" do
      company = Company.new
      company.settings.build(key: "Foo", value: "bar")
      company.settings.build(key: "other", value: "thing")
      expect(company.setting("Foo")).to eq "bar"
      expect(company.setting(:other)).to eq "thing"
      expect(company.setting("missing")).to eq nil
    end

    it "casts a value to an integer" do
      company = Company.new
      company.settings.build(key: "test", value: "10")
      expect(company.setting("test", :integer)).to eq 10
      expect(company.setting("missing", :integer)).to eq nil
    end

    it "casts a value to a float" do
      company = Company.new
      company.settings.build(key: "test", value: "1.5")
      expect(company.setting("test", :float)).to eq 1.5
      expect(company.setting("missing", :float)).to eq nil
    end

    it "casts a value to a boolean" do
      company = Company.new
      company.settings.build(key: "test", value: "1")
      expect(company.setting("test", :boolean)).to eq true
      expect(company.setting("missing", :boolean)).to eq false
    end

    it "returns a default value" do
      company = Company.new
      expect(company.setting("test", :string, :foo)).to eq "foo"
    end
  end

  describe 'after_commit' do
    context 'when a company is created' do
      let(:name) { 'Whatever' }

      it 'calls identify_on_analytics' do
        expect(Analytics).to receive(:identify).with(
          user_id: anything,
          traits: {
            company: name,
          }
        ).once

        Company.create!(name: name)
      end
    end

    context 'when a company has name change' do
      let!(:company_to_change_name) { create(:rescue_company, name: 'First Name') }
      let(:name) { 'Second Name' }

      it 'calls identify_on_analytics' do
        expect(Analytics).to receive(:identify).with(
          user_id: company_to_change_name.to_ssid,
          traits: {
            company: name,
          }
        ).once

        company_to_change_name.update!(name: name)
      end
    end

    context 'when a company has any other attr change' do
      let(:analytics_double_any_attr) { double(:analytics_double_any_attr, identify: nil) }
      let!(:company_to_change_any_attr) { create(:rescue_company, support_email: 'support@email.com', name: 'Das name') }

      it 'calls identify_on_analytics' do
        company_to_change_any_attr.update!(support_email: 'support@email.changed')

        expect(analytics_double_any_attr).not_to have_received(:identify)
      end
    end
  end

  describe "sort_name" do
    it "returns a blank string if there is no name" do
      expect(Company.new.sort_name).to eq ""
    end

    it "returns a lowercased version of the name" do
      expect(Company.new(name: "Joe's Towing").sort_name).to eq "joe's towing"
    end

    it "transliterates non-ASCII characters" do
      expect(Company.new(name: "Über Towing").sort_name).to eq "uber towing"
    end
  end

  describe "update dependent indexes" do
    it "updates dependent indexes when the name changes" do
      company = create(:rescue_company)
      SearchIndex::CompanyDependentIndexUpdateWorker.jobs.clear

      company.update!(updated_at: Time.current)
      expect(SearchIndex::CompanyDependentIndexUpdateWorker.jobs.size).to eq 0

      company.update!(name: "New Name")
      expect(SearchIndex::CompanyDependentIndexUpdateWorker).to have_enqueued_sidekiq_job(company.id)
    end
  end

  describe '#round_distance' do
    context 'when DISTANCE_ROUND_UP' do
      let(:company) { Company.new(invoice_mileage_rounding: Company::DISTANCE_ROUND_UP) }

      it 'rounds up' do
        expect(company.round_distance(BigDecimal('3.12'))).to eq(BigDecimal('4'))
      end
    end

    context 'when DISTANCE_ROUND_NEAREST' do
      let(:company) { Company.new(invoice_mileage_rounding: Company::DISTANCE_ROUND_NEAREST) }

      it 'rounds down' do
        expect(company.round_distance(BigDecimal('3.12'))).to eq(BigDecimal('3'))
      end

      it 'rounds up' do
        expect(company.round_distance(BigDecimal('5.50'))).to eq(BigDecimal('6'))
      end
    end

    context 'when DISTANCE_ROUND_NEAREST_TENTH' do
      let(:company) { Company.new(invoice_mileage_rounding: Company::DISTANCE_ROUND_NEAREST_TENTH) }

      it 'rounds down' do
        expect(company.round_distance(BigDecimal('3.12'))).to eq(BigDecimal('3.1'))
      end

      it 'rounds up' do
        expect(company.round_distance(BigDecimal('5.551'))).to eq(BigDecimal('5.6'))
      end
    end

    context 'when invoice_mileage_rounding is not set' do
      let(:company) { Company.new(invoice_mileage_rounding: nil) }

      it 'rounds down' do
        expect(company.round_distance(BigDecimal('3.12'))).to eq(BigDecimal('3.1'))
      end

      it 'rounds up' do
        expect(company.round_distance(BigDecimal('5.58'))).to eq(BigDecimal('5.6'))
      end
    end
  end
end
