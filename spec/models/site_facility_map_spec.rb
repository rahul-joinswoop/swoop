# frozen_string_literal: true

require 'rails_helper'

describe SiteFacilityMap do
  describe "sync site to issc_locations" do
    it "syncs the site with the issc location record" do
      company = create(:rescue_company)
      site = create(:partner_site, company: company)
      location = create(:location)
      fleet_company = create(:fleet_company)
      issc_location = create(:issc_location, site: nil, company: company, fleet_company: fleet_company, location: location, locationid: "1")
      other_location = create(:issc_location, site: nil, company: company, fleet_company: fleet_company, location: create(:location), locationid: "2")
      SiteFacilityMap.create!(site: site, company: company, fleet_company: fleet_company, facility: location)
      issc_location.reload
      expect(issc_location.site_id).to eq site.id
      expect(other_location.reload.site_id).to eq nil
    end
  end
end
