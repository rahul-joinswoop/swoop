# frozen_string_literal: true

require "rails_helper"

describe HistoryItem, type: :model do
  include_context 'job history label shared context', :history_item
end
