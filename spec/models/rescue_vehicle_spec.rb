# frozen_string_literal: true

require "rails_helper"

describe RescueVehicle do
  let(:company) do
    create(:rescue_company, {
      features: [
        create(:feature, {
          name: Feature::PARTNER_DISPATCH_TO_TRUCK,
        }),
      ],
    })
  end

  let!(:rescue_vehicle) do
    create(:rescue_vehicle_with_coords, {
      dedicated_to_swoop: true,
      company: company,
    })
  end

  describe ".dispatchable" do
    it "has valid model instances" do
      expect(company).to be_valid
      expect(rescue_vehicle).to be_valid
    end

    it "has_partner_dispatch_feature is not blank" do
      expect(RescueVehicle.has_partner_dispatch_feature).to exist
    end

    it "dispatchable is not blank" do
      expect(RescueVehicle.dispatchable).to exist
    end

    it "has_coords is not blank" do
      expect(RescueVehicle.has_coords).to exist
    end
  end

  describe "Subscribable" do
    context "trigger_partner_vehicle_changed" do
      context "blocking feature flag is disabled" do
        let!(:job_1) { create(:job, rescue_vehicle: rescue_vehicle, status: "completed") }
        let!(:job_2) { create(:job, rescue_vehicle: rescue_vehicle, status: "towing") }

        describe "from UpdateVehicleLocationWorker" do
          it "works" do
            GraphQL::JobUpdatedWorker.jobs.clear
            expect(Subscribable).not_to receive(:trigger_partner_vehicle_changed)
            expect(Subscribable).not_to receive(:trigger_job_updated)
            expect(Subscribable).not_to receive(:trigger_user_updated)
            UpdateVehicleLocationWorker.new.perform(rescue_vehicle.company_id, rescue_vehicle.id, 50, 123, Time.current)
            expect(GraphQL::JobUpdatedWorker).to have_enqueued_sidekiq_job(ids: [job_2.id], customer_only: true)
          end

          it "doesn't trigger if the fail safe feature flag is set" do
            Flipper.enable(:block_location_updates_to_current_jobs)
            GraphQL::JobUpdatedWorker.jobs.clear
            expect(Subscribable).not_to receive(:trigger_partner_vehicle_changed)
            expect(Subscribable).not_to receive(:trigger_job_updated)
            expect(Subscribable).not_to receive(:trigger_user_updated)
            UpdateVehicleLocationWorker.new.perform(rescue_vehicle.company_id, rescue_vehicle.id, 50, 123, Time.current)
            expect(GraphQL::JobUpdatedWorker.jobs).to be_empty
          end
        end

        describe "update!" do
          it "works" do
            GraphQL::JobUpdatedWorker.jobs.clear
            expect(Subscribable).to receive(:trigger_partner_vehicle_changed).once.with(rescue_vehicle)
            expect(Subscribable).to receive(:trigger_job_updated).once.with(rescue_vehicle.jobs)
            expect(Subscribable).to receive(:trigger_user_updated).once.with(rescue_vehicle.users)
            expect(GraphQL::JobUpdatedWorker.jobs).to be_empty
            rescue_vehicle.update! name: "asdf"
          end
        end
      end
    end
  end

  describe '#subscribers' do
    subject { rescue_vehicle.subscribers }

    context "dispatchers" do
      let!(:dispatchers) { create_list :user, 3, :rescue, :dispatcher, company: company }

      it "works" do
        expect(subject).to eq(Set.new([company, *dispatchers]))
      end
    end

    context "oauth_application" do
      let!(:oauth_application) { create :oauth_application, owner: company }

      it "works" do
        expect(subject).to eq(Set.new([company, oauth_application]))
      end
    end
  end
end
