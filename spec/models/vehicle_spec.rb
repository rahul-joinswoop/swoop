# frozen_string_literal: true

require "rails_helper"

describe Vehicle do
  let(:vehicle) { create(:vehicle) }

  it "creates vehicle" do
    expect(vehicle).to be_valid
  end

  describe "location updates" do
    it "is active if location updated in the last 12 hours" do
      vehicle = create(:rescue_vehicle)
      expect(vehicle.active?).to eq false
      vehicle.update_location!(lat: 50, lng: -119, timestamp: 13.hours.ago)
      expect(vehicle.active?).to eq false
      vehicle.update_location!(lat: 50, lng: -119, timestamp: 11.hours.ago)
      expect(vehicle.active?).to eq true
    end

    it "has live updates if location updated in the last 1 hour" do
      vehicle = create(:rescue_vehicle)
      expect(vehicle.live_updates?).to eq false
      vehicle.update_location!(lat: 50, lng: -119, timestamp: 61.minutes.ago)
      expect(vehicle.live_updates?).to eq false
      vehicle.update_location!(lat: 50, lng: -119, timestamp: 59.minutes.ago)
      expect(vehicle.live_updates?).to eq true
    end
  end
end
