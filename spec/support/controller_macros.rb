# frozen_string_literal: true

module ControllerMacros

  # Authenticate the request with the given access token.
  def authenticate_request(request, access_token)
    request.env["HTTP_AUTHORIZATION"] = "Bearer #{access_token.token}"
  end

  def stub_user_login(*traits)
    let!(:user) { create(*[:user, *traits]) }
    let!(:token) { create :doorkeeper_access_token, resource_owner_id: user.id, scopes: user.roles.map(&:name) }

    before(:each) do
      authenticate_request(controller.request, token)
    end
  end

  def stub_fleet_in_house_company_user_login(*traits)
    stub_fleet_in_house_company_login(*traits)
    let!(:user) { create(*[:user, :fleet_demo, company: company, roles: [:fleet]]) }
    stub_company_user_login
  end

  def stub_fleet_managed_company_user_login(*traits)
    stub_fleet_managed_company_login(*traits)
    let!(:user) { create(*[:user, :fleet_demo, company: company, roles: [:fleet]]) }
    stub_company_user_login
  end

  alias :stub_fleet_company_user_login :stub_fleet_managed_company_user_login

  def stub_fleet_in_house_company_login(*traits)
    let!(:company) { create(*[:fleet_in_house_company, *traits]) }
    let!(:scopes) { :fleet }
    stub_company_login
  end

  def stub_fleet_managed_company_login(*traits)
    let!(:company) { create(*[:fleet_managed_company, *traits]) }
    let!(:scopes) { :fleet }
    stub_company_login
  end

  alias :stub_fleet_company_login :stub_fleet_managed_company_login

  def stub_rescue_company_user_login(*traits)
    stub_rescue_company_login(*traits)
    let!(:user) { create(*[:user, :rescue, company: company, roles: [:rescue]]) }
    stub_company_user_login
  end

  def stub_rescue_company_login(*traits)
    let!(:company) { create(*[:rescue_company, *traits]) }
    let!(:scopes) { :rescue }
    stub_company_login
  end

  # this method expects company and scopes to already be available via let
  private def stub_company_login
    let!(:application) { create :doorkeeper_application, owner: company, scopes: scopes }
    let!(:token) { create :doorkeeper_access_token, resource_owner_id: nil, application: application, scopes: scopes }
    before(:each) do
      authenticate_request(controller.request, token)
    end
  end

  # this method expects user and scopes to already be available via let
  private def stub_company_user_login
    let!(:token) { create :doorkeeper_access_token, resource_owner_id: user.id, scopes: scopes }
  end

end

# LAME LAME LAME but doing things the right way doesn't work inside of rspec
include ControllerMacros
