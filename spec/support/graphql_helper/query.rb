# frozen_string_literal: true

# helpers to create / execute graphql queries
RSpec.shared_context "query helpers" do
  # if this is enabled then we print any errors that come back from our request execution
  let(:show_errors) { true }

  # actual method used to make queries against our schema
  def execute(context:,
              only:,
              operation_name:,
              query:,
              root_value:,
              skip_query_inliner: false,
              subscription_topic: nil,
              variables:)
    q = {
      query: query,
      context: context,
      # when variables come in from web, they come in as deserialized JSON
      # and all the keys are stringified - so we duplicate that behavior
      # here. some weird and hard to debug things happen if we use symbolized
      # keys....
      variables: variables.deep_stringify_keys,
      operation_name: operation_name,
      only: only,
      root_value: root_value,
    }

    if subscription_topic
      # don't use the query inliner for a subscription response
      q[:subscription_topic] = subscription_topic
    else
      # call our query inliner since that's what happens in production
      Utils::QueryVariableInliner.inline! q unless skip_query_inliner
    end

    # and execute our inlined query
    res = SwoopSchema.execute(q)

    # Print any errors
    if res["errors"] && show_errors
      pp res.to_h
    end
    res
  end

  # used by our only_filter below
  let(:interactor_context) { { input: { context: context } } }

  let(:query) { nil }
  let(:variables) { {} }
  # build up a random operation_name
  let(:operation_name) do
    Faker::Lorem.words(number: 3).map(&:capitalize).join.gsub(/[^a-zA-Z]/, '')
  end

  # build up our only filter
  let(:only_filter) do
    # build an interactor context and add our graphql context to it
    ctx = Interactor::Context.new(interactor_context)

    # build and return filter
    GraphQL::BuildFilter.new(ctx).call.output[:only_filter]
  end
  # allow specs to explicitly skip our query_inliner
  let(:skip_query_inliner) { false }
  # root value of our query, defaults to our api_company
  let(:root_value) { context[:api_company] }
  # if subscription_topic is specified then we go down the subscription result path of execution
  let(:subscription_topic) { nil }
  # if true, we can test flows that depend on RequestContext.current calls in our codebase
  let(:wrap_in_request_context) { false }
  # request context that we'd use if the above is true
  let(:request_context) do
    RequestContext.new(uuid: SecureRandom.hex, source_application_id: source_application.id)
  end

  # memoized helper to make requests, setup default values, etc.
  # TODO - maybe some day we merge this and #request ?
  let(:response) do
    graphql_request = -> do
      # TODO - if this order changes random specs break? LOL
      execute query: query,
              context: context,
              variables: variables,
              # TODO - this doesn't really work, why?
              # 50% of the time provide a nil operationName so we're testing this codepath
              # operation_name: rand(2) == 0 ? operation_name : nil,
              operation_name: operation_name,
              only: only_filter,
              skip_query_inliner: skip_query_inliner,
              root_value: root_value,
              subscription_topic: subscription_topic
    end
    if wrap_in_request_context
      RequestContext.use(request_context) do
        graphql_request.call
      end
    else
      graphql_request.call
    end
  end

  # shortcut to get the data from a response (and run the request implicitly)
  def response_data(*path)
    response.dig("data", *path)
  end

  let(:api_user_platform) { SourceApplication::PLATFORM_IOS }

  let(:source_application) do
    platform = nil

    if context[:api_application]
      if context[:api_application].owner.fleet?
        platform = SourceApplication::PLATFORM_CLIENT_API
      elsif context[:api_application].owner.partner?
        platform = SourceApplication::PLATFORM_PARTNER_API
      end

      SourceApplication.instance(platform: platform, oauth_application_id: context[:api_application].id)
    elsif context[:api_user]
      # Override api_user_platform in case you need to test it for Android, or anything else.
      SourceApplication.instance(platform: api_user_platform, source: SourceApplication::SOURCE_SWOOP)
    end
  end
end
