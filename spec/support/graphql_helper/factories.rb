# frozen_string_literal: true

# standard way of building various user/company/application types so that they work correctly
# with various graphql specs
RSpec.shared_context "factory helpers" do
  let(:rescue_company) do
    create :rescue_company, :with_service_codes, :with_account,
           :with_hq, :with_location_types, :with_phone
  end
  let(:rescue_application) do
    create :doorkeeper_application, owner: rescue_company, scopes: :rescue
  end
  let(:rescue_dispatcher) do
    create :user, :rescue, :dispatcher, company: rescue_company
  end
  let(:rescue_driver) do
    create :user, :rescue, :driver, company: rescue_company
  end
  let(:rescue_dispatcher_driver) do
    create :user, :rescue, :dispatcher, :driver, company: rescue_company
  end
  let(:rescue_admin) do
    create :user, :rescue, :admin, company: rescue_company
  end

  # FleetInHouseCompany - TODO we don't have any of these anymore so don't bother testing?
  let(:fleet_in_house_company) do
    create :fleet_in_house_company, :with_service_codes, :with_questions,
           :with_symptoms, :with_location_types, :with_phone
  end
  let(:fleet_in_house_application) do
    create :doorkeeper_application, owner: fleet_in_house_company, scopes: :fleet
  end

  # FleetManagedCompany
  let(:fleet_managed_company) do
    create :fleet_managed_company, :with_service_codes, :with_questions,
           :with_symptoms, :with_location_types, :with_phone
  end
  let(:fleet_managed_application) do
    create :doorkeeper_application, owner: fleet_managed_company, scopes: :fleet
  end
  let(:fleet_managed_dispatcher) do
    create :user, company: fleet_managed_company, roles: [:fleet, :dispatcher]
  end

  # FleetMotorClubCompany
  let(:fleet_motor_club_company) do
    create :fleet_company, :motor_club, :with_service_codes, :with_questions,
           :with_symptoms, :with_location_types, :with_phone
  end
  let(:fleet_motor_club_application) do
    create :doorkeeper_application, owner: fleet_motor_club_company, scopes: :fleet
  end

  # SwoopCompany
  let(:swoop) { create :super_company, :swoop }
  let(:swoop_application) do
    create :doorkeeper_application, owner: swoop, scopes: :root
  end
  let(:swoop_dispatcher) do
    create :user, company: swoop, roles: [:fleet, :dispatcher]
  end
  let(:swoop_root) do
    create :user, company: swoop, roles: [:fleet, :dispatcher, :root]
  end

  # SuperFleetCompany (aka agero)
  let(:super_fleet_managed_company) { create :fleet_company, :agero }
  let(:super_fleet_managed_application) do
    create :doorkeeper_application, owner: super_fleet_managed_company, scopes: :super_fleet_managed
  end

  # Third-party Partner Application (aka beacon/towbook)
  let(:third_party_partner_api_application) do
    create :doorkeeper_application, owner: nil, scopes: :rescue
  end
end
