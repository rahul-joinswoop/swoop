# frozen_string_literal: true

# various helpers to create valid graphql context for specs
RSpec.shared_context "context helpers" do
  # Helper method to set the api_roles for test context with the same values
  # as are set by SwoopAuthenticator.
  #
  # TODO - why aren't we using SwoopAuthenticator#scope_roles for this?
  def context_roles_for(user: nil, application: nil, company: nil, viewer: nil)
    if viewer
      case viewer
      when ::User
        user = viewer
      when ::Company
        company = viewer
      when ::Doorkeeper::Application
        application = viewer
      else
        raise ArgumentError, "Unknown viewer: #{viewer.inspect}"
      end
    end

    if user
      user.human_roles
    elsif company
      case company
      when FleetCompany
        SwoopAuthenticator::ALL_FLEET_ROLES
      when RescueCompany
        SwoopAuthenticator::ALL_RESCUE_ROLES
      else
        raise ArgumentError, "Can't find roles for company: #{company.inspect}"
      end
    elsif application
      case application.scopes.to_a
      when [SwoopAuthenticator::SUPER_FLEET_MANAGED_ROLE]
        SwoopAuthenticator::ALL_SUPER_FLEET_MANAGED_ROLES
      when [SwoopAuthenticator::FLEET_ROLE]
        SwoopAuthenticator::ALL_FLEET_ROLES
      when [SwoopAuthenticator::RESCUE_ROLE]
        SwoopAuthenticator::ALL_RESCUE_ROLES
      end
    else
      []
    end
  end

  # default values for each part of our context
  let(:api_application) { nil }
  let(:api_company) { nil }
  let(:api_roles) { context_roles_for viewer: viewer }
  let(:api_user) { nil }
  let(:headers) do
    {
      'HTTP_USER_AGENT' => 'ios SwoopMobile/9 CFNetwork/808.3 Darwin/16.3.0',
      'HTTP_REFERER' => 'http://example.com/',
    }
  end
  let(:viewer) { nil }

  # default context that everyone can use
  let(:context) do
    {
      api_application: api_application,
      api_company: api_company,
      api_roles: api_roles,
      api_user: api_user,
      headers: headers,
      viewer: viewer,
    }
  end
end
