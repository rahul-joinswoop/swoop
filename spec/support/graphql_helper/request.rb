# frozen_string_literal: true

# helpers to mock RequestContext
RSpec.shared_context "request helpers" do
  let(:api_user_platform) { SourceApplication::PLATFORM_IOS }

  let(:source_application) do
    platform = nil

    if context[:api_application]
      if context[:api_application].owner.fleet?
        platform = SourceApplication::PLATFORM_CLIENT_API
      elsif context[:api_application].owner.partner?
        platform = SourceApplication::PLATFORM_PARTNER_API
      end

      SourceApplication.instance(platform: platform, oauth_application_id: context[:api_application].id)
    elsif context[:api_user]
      # Override api_user_platform in case you need to test it for Android, or anything else.
      SourceApplication.instance(platform: api_user_platform, source: SourceApplication::SOURCE_SWOOP)
    end
  end

  # fake request context to potentially use with graphql requests
  let(:request_context) do
    RequestContext.new(uuid: SecureRandom.hex, source_application_id: source_application.id)
  end
end
