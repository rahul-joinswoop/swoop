# frozen_string_literal: true

module System
  module JobHelpers

    def open_new_job
      find('button#new-request').click
      expect(page).to have_content("New Job")
    end

    def fill_release_to_account(name = 'Jims Garage')
      find('#Containerstoragereleased_to_account_id .geosuggest__input').set(name)
      find('#Containerstoragereleased_to_account_id li', text: name).click
      page.check 'Inputstoragereplace_invoice_info'
    end

    def goto_storage
      visit "#partner/storage"
    end

    def get_job_id_from_notification
      find(".notification_list").text.scan(/\d+/).first
    end

    def find_job_row(id)
      find('.row_container:not(.table_header) .row:not(.row_header) .cell:first-child', text: id, exact_text: true)
    end

    def change_status(id, status)
      div = find_job_row(id)
      div.find(:xpath, "..").select(status, from: 'status')
    end

    def view_options(id)
      div = find_job_row(id)
      options = div.find(:xpath, "..").find("i.options")
      options.click
    end

    def select_options_item(text)
      option = find('div.optionsList li', text: text)
      option.click
    end

    def set_will_store
      find("#Inputwill_store").click
      selector = find("#Inputdrop_locationsite_id")
      selector.select(first('#Inputdrop_locationsite_id > option:nth-child(2)').text)
    end

    def set_account(name = "Cash Call")
      find('#Containeraccount input').set(name)
      find('#Containeraccount li', text: name).click
    end

    def set_user
      find('#Inputcustomerfull_name').set 'PaulTest WidOne'
      find('#Inputcustomerphone').native.send_keys '4153009663'
    end

    def set_location
      # Zooms map
      find("#Containerservice_location input").click
      find("#Containerservice_location input").set "3222 Johns Cabin Road, Wildwood Mo"
      find('#Containerservice_location input').native.send_keys(:return)

      find('input#Inputpickup_contactfull_name').set "Quinn Baetz"
      find('input#Inputpickup_contactphone').native.send_keys "4153002222"

      # Race condition if map doesn't load in time here
      # Select spot on map
      find("#Containerservice_location input").click
      find('.job_map_container').click
    end

    def set_car(customer_type_number = 1)
      find('#Containermake input').set('Tesla') if customer_type_number != 1
      find('#Containermodel input').set 'Model S'
      find('#Containeryear input').set '2015'
      find('#Containercolor input').set 'White'
      find('#Containervin input').set '1HGCM82633A004352'
    end

    def set_customer_type(item_number)
      if item_number > 0
        expect(page).to have_content('Payment Type')
        select('Warranty', from: 'Payment Type')
      end
    end

    def set_tow
      select('Tow', from: 'Inputservice')
    end

    def fill_release_to_customer
      customer = 'Customer'
      find('#Containerstoragereleased_to_account_id .geosuggest__input').set(customer)
      find('#Containerstoragereleased_to_account_id li', text: customer).click

      find('#Containerstoragereleased_to_userfull_name input').set "Release Name"
      find('#Containerstoragereleased_to_userlocation input').set "Release Address"
      find('#Containerstoragereleased_to_userphone input').set "2313357896"
      find('#Containerstoragereleased_to_email input').set "quinnbaetz@gmail.com"
      find('#Containerstoragereleased_to_userlicense_state select').select("CA")
      page.check 'Inputstoragereplace_invoice_info'
    end

    def populate_job
      # setting up customer data
      set_user

      # setting up job location
      set_location

      # setting job"s car description
      set_car

      # setting customer type
      set_tow
    end

  end
end
