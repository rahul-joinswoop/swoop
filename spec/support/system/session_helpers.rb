# frozen_string_literal: true

module System
  module SessionHelpers

    # user can be an actual User model or a hash of { username:, password: }
    def log_in(user = create(:user))
      # ENG-6415 - Clear bearer token before attempting to login.
      clear_storage

      visit "/login"
      username = user.respond_to?(:dig) ? user.dig(:username) : user.username
      password = user.respond_to?(:dig) ? user.dig(:password) : user.password
      fill_in "user_username_or_email", with: username
      fill_in "user_password", with: password
      click_button "Log In"
      wait_for_ajax
    end

    def log_out
      close_modal
      find("li.swoop-user").click
      expect(page).to have_content("LOG OUT")
      find("button.fa-power-off").click
    end

    alias_method :login, :log_in
    alias_method :logout, :log_out

    # ok - most capybara methods are blocking, ie this:
    #
    # page.has_button?("foo")
    #
    # will block for Capybara.default_max_wait_time looking for said button.
    # this is ok if you're testing a definitive flow (login, click button) since
    # you want a failure if the button never shows up. but if you're trying to
    # do something that may or may not
    #
    MAYBE_TIMEOUT = 0.25

    def maybe
      Timeout.timeout(MAYBE_TIMEOUT) do
        yield
      end
    rescue
      # noop
    end

    # there may not be a modal open to close so wrap in maybe to avoid waiting
    def close_modal
      maybe do
        page.execute_script("$('button.close, button.modal-close-button').click()")
      end
    end

    def close_all_notifications
      page.all('.close_notification').each do |notification|
        notification.click
      end
    end

    # there may not be a notification open to close so wrap in maybe to avoid
    # waiting
    def click_refresh
      maybe do
        (click_link("Refresh") && wait_for_ajax) if page.has_link?("Refresh")
      end
    end

    def wait_for_ajax
      Timeout.timeout(Capybara.default_max_wait_time) do
        loop until js_loaded? && finished_all_ajax_requests?
      end
    end

    def wait_for_loaded
      Timeout.timeout(Capybara.default_max_wait_time) do
        loop until js_loaded?
      end
    end

    def current_path_info
      current_url.sub(%r{.*?://}, "")[%r{[/\?\#].*}] || "/"
    end

    def clear_storage
      visit("")
      wait_for_loaded
      page.driver.execute_script("localStorage.clear()")
    end

    private

    def finished_all_ajax_requests?
      page.evaluate_script("jQuery.active").zero?
    end

    def js_loaded?
      page.evaluate_script("document.readyState == 'complete'")
    end

  end
end
