# frozen_string_literal: true

RSpec.shared_context "job completed shared examples", shared_context: :metadata do
  shared_examples 'sending Analytics Job Completed event' do |platform, version, referer|
    it 'instantiates AnalyticsData::JobStatus' do
      if api_user
        expect(AnalyticsData::JobStatus).to receive(:new).with(
          job: job,
          user_agent_hash: {
            platform: platform,
            version: version,
          },
          referer: referer,
          api_user: api_user,
          api_application: nil,
        ).once.and_call_original
      elsif api_application
        expect(AnalyticsData::JobStatus).to receive(:new).with(
          job: job,
          user_agent_hash: {
            platform: platform,
            version: version,
          },
          referer: referer,
          api_user: nil,
          api_application: api_application,
        ).once.and_call_original
      end

      subject
    end

    it 'calls AnalyticsData::JobStatus#push_message' do
      expect_any_instance_of(AnalyticsData::JobStatus).to receive(:push_message).once

      subject
    end
  end
end
