# frozen_string_literal: true

class LoggingFormatter

  RSpec::Core::Formatters.register self, :example_started, :example_finished

  START_STRING = "RSpec begin %s (%s:%s)"
  END_STRING = "RSpec %s (%sms) %s (%s:%s)"
  PATH_MATCHER = /\A\.\//.freeze

  def initialize(output)
    @output = output
  end

  def example_started(notification)
    ::Rails.logger.info(START_STRING % get_example_info(notification.example))
  end

  def example_finished(notification)
    example = notification.example
    execution_result = example.execution_result
    ::Rails.logger.info END_STRING % [
      execution_result.status.to_s,
      (execution_result.run_time * 1000).round(1),
      *get_example_info(example),
    ]
  end

  private def get_example_info(example)
    metadata = example.metadata
    [
      metadata[:full_description].to_s,
      metadata[:file_path].to_s.sub(PATH_MATCHER, ''),
      metadata[:line_number].to_s,
    ]
  end

end

RSpec.configure do |config|
  config.add_formatter LoggingFormatter
end
