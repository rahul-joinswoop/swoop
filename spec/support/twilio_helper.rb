# frozen_string_literal: true

RSpec.shared_examples "twilio message parsing" do
  let(:sms_alert) do
    create(:audit_sms, parsed_response: nil)
  end

  let(:twilio_response) do
    double("TwilioResponse", target: sms_alert, body: response_body)
  end

  context "when twilio response is 'Y'" do
    let(:response_body) { "Y" }

    it "logs the parsed_response as 1" do
      expect { subject && sms_alert.reload }.to change(sms_alert, :parsed_response).to(1)
    end

    it "assigns response_received_at" do
      expect { subject && sms_alert.reload }.to change(sms_alert, :response_received_at).from(nil)
    end
  end

  context "when twilio response is 'N'" do
    let(:response_body) { "N" }

    it "logs the parsed_response as 2" do
      expect { subject && sms_alert.reload }.to change(sms_alert, :parsed_response).to(2)
    end

    it "assigns response_received_at" do
      expect { subject && sms_alert.reload }.to change(sms_alert, :response_received_at).from(nil)
    end
  end
end
