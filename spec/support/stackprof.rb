# frozen_string_literal: true

if ENV['STACKPROF']
  require 'stackprof'
  RSpec.configure do |config|
    config.before :suite do
      StackProf.start(mode: :cpu, interval: 1000, out: 'tmp/stackprof-cpu-rspec.dump')
    end

    config.after :suite do
      StackProf.stop
      StackProf.results
    end
  end
end
