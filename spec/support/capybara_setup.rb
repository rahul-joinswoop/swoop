# frozen_string_literal: true

require 'capybara/rails'
require 'uri'

Capybara.register_driver :swoop_chrome_headless do |app|
  Capybara::Selenium::Driver.load_selenium
  browser_options = ::Selenium::WebDriver::Chrome::Options.new
  browser_options.args << '--headless' if ENV["SHOW_BROWSER_WINDOW"].blank?
  browser_options.args << '--disable-gpu' if Gem.win_platform?
  # this is the only change from the default (for now) and it's just a hack
  # to work around intercom covering up buttons.
  # TODO: figure out how to configure chrome to block intercom's js so that we
  # don't need this?
  browser_options.args << '--window-size=1600,1200'
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
end

Capybara.configure do |config|
  config.run_server = true
  config.server = :puma, { Silent: true }
  config.default_max_wait_time = 5
end
