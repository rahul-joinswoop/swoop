# frozen_string_literal: true

RSpec.shared_context "job status change shared examples", shared_context: :metadata do
  shared_examples 'set user_agent data to job and user' do |platform, user_agent, browser, version|
    it "sets job.platform android" do
      expect(job.platform).to be_nil

      post_complete_job

      expect(job.platform).to eq platform
    end

    it "sets user.platform android" do
      expect(controller.api_user.platform).to be_nil

      post_complete_job

      expect(controller.api_user.reload.platform).to eq platform
    end

    it 'sets user.user_agent' do
      expect(user.user_agent).to be_nil

      post_complete_job

      expect(user.user_agent).to eq user_agent
    end

    it "sets user.browser" do
      expect(controller.api_user.browser).to be_nil

      post_complete_job

      expect(controller.api_user.reload.browser).to eq browser
    end

    it "sets user.version" do
      expect(controller.api_user.reload.version).to be_nil

      post_complete_job

      expect(controller.api_user.reload.version).to eq version
    end
  end

  shared_examples 'updates job.last_touched_by when api_user is passed' do
    let(:api_user) { nil }

    context 'when api_user is passed' do
      let(:api_user) { create(:user) }

      it 'updates job.last_touched_by with the api_user' do
        expect { subject && job.reload }.to change(job, :last_touched_by).from(nil).to(api_user)
      end
    end

    context 'when no api_user is passed' do
      context 'and job.last_touched_by has been previsously set' do
        let(:previous_touched_by) { create(:user) }

        before do
          job.update! last_touched_by: previous_touched_by
        end

        it 'does not update job.last_touched_by' do
          expect { subject && job.reload }.not_to change(job, :last_touched_by)
        end
      end
    end
  end

  shared_examples 'updates job.last_touched_by with controller.api_user' do
    it 'updates job.last_touched_by with controller.api_user' do
      expect { subject && job.reload }.to change(job, :last_touched_by).from(nil).to(controller.api_user)
    end
  end
end
