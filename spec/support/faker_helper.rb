# frozen_string_literal: true

require 'faker'

# add a phone_number_e164 method to faker
module PhoneNumberE164

  def phone_number_e164
    Phonelib.parse(phone_number).e164
  end

end

Faker::PhoneNumber.extend PhoneNumberE164

module PONumber

  def po_number
    "##{Faker::Number.number(digits: 10)}"
  end

end

Faker::Company.extend PONumber

module Faker
  class Boolean

    class << self

      def boolean
        [true, false].sample
      end

      def boolean_with_nil
        [true, false, nil].sample
      end

    end

  end
end
