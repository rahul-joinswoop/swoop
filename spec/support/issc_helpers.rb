# frozen_string_literal: true

class MockConnectionManager < ISSC::ConnectionManager

  def initialize
    @f = MockFunctionCall.new
  end

  def ensure_connected(&block)
    block.call issc_client
  end

  def mock
    @f
  end

  def issc_client
    @f
  end

  def connected?
    true
  end

end

class MockHttpSuccess

  def initialize
    @got_called = false
  end

  def code
    @got_called = true
    200
  end

  def called?
    @got_called
  end

end

class MockFunctionCall

  def initialize
    @calls = {}
  end

  def method_missing(m, *args)
    @calls[m.to_sym] = args
  end

  def called?(name)
    @calls[name.to_sym].present?
  end

  def arguments(name)
    @calls[name.to_sym]
  end

end

RSpec.shared_context "issc provider setup", shared_context: :metadata do
  let(:manager) { MockConnectionManager.new }
  let!(:company) { create(:company) }
  let!(:fleet_company) { create(:fleet_company, issc_client_id: 'GCOAPI') }
  let!(:account) { create(:account, company: company, client_company: fleet_company) }
  let!(:issc) { create(:gco_issc, company: company) }
end
