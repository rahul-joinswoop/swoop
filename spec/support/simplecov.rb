# frozen_string_literal: true

# this will only work (and should only be used) in ci or locally with
# ENABLE_COVERAGE set.
if ENV['ENABLE_COVERAGE'] || ENV['CIRCLECI']
  require 'simplecov'
  require 'active_support/inflector'

  # save to CircleCI's artifacts directory if we're on CircleCI
  dir = File.join(ENV['CIRCLE_ARTIFACTS'] || 'tmp', "coverage")

  cmd = if defined?(::Rake) && ::Rake&.application&.top_level_tasks.present?
          # a rake task, figure out our command from the rake task
          ::Rake.application.top_level_tasks.join(" ")
        else
          # use our cli program name
          File.basename($PROGRAM_NAME)
        end

  # we can't have the same command_name across parallel runs or else simplecov
  # can't merge our results so we need to generate a unique name
  test_id = ENV.to_h
    .slice(
      'PARALLEL_TEST_GROUPS',
      'TEST_ENV_NUMBER',
      'CIRCLE_WORKFLOW_JOB_ID',
      'CIRCLE_NODE_INDEX'
    )
    .values.reject(&:empty?).join("-")

  if ENV['CIRCLECI']
    # disable the default html formatter on circleci - we don't use its output and it slows down our test runs
    SimpleCov.formatters = nil
  end

  SimpleCov.command_name [cmd, test_id].reject(&:empty?).join(" ")
  SimpleCov.merge_timeout 4 * 3600
  SimpleCov.use_merging true
  SimpleCov.coverage_dir(dir)
  SimpleCov.start :rails do
    add_group 'Root Libraries', '/lib'

    # don't show views or assets in our coverage report
    Dir['app/*'].map { |d| File.basename(d) }
      .reject { |d| ['views', 'assets'].include? d }
      .each { |d| add_group File.basename(d).humanize, d }

    add_filter '/spec/'
    add_filter '/test/'
    add_filter '/config/'
  end
end
