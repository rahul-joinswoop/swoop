# frozen_string_literal: true

module Requests

  module JsonHelpers

    def json
      Rails.logger.debug "Requests::JsonHelpers::json Reponse: #{response.code} #{response.body}"
      if response.body && response.body.present?
        JSON.parse(response.body)
      end
    end

  end

  module InvoiceHelpers

    def dump_invoice(invoice)
      Rails.logger.debug "INVOICE: #{invoice.inspect}"
      invoice.line_items.each do |li|
        Rails.logger.debug li.inspect
      end
    end

    def find_li(invoice, name)
      invoice.line_items.find_by(description: name)
    end

  end

end
