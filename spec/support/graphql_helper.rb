# frozen_string_literal: true

require_relative './graphql_helper/context.rb'
require_relative './graphql_helper/query.rb'
require_relative './graphql_helper/request.rb'
require_relative './graphql_helper/factories.rb'

RSpec.configure do |rspec|
  # This config option will be enabled by default on RSpec 4,
  # but for reasons of backwards compatibility, you have to
  # set it on RSpec 3.
  #
  # It causes the host group and examples to inherit metadata
  # from the shared context.
  rspec.shared_context_metadata_behavior = :apply_to_host_groups

  rspec.define_derived_metadata(file_path: Regexp.new('/spec/graphql/')) do |metadata|
    metadata[:type] = :graphql
  end
end

RSpec.shared_context "graphql helpers", shared_context: :metadata do
  include GraphQLConcerns

  include_context 'context helpers'
  include_context 'query helpers'
  include_context 'request helpers'
  include_context 'factory helpers'

  # client-related viewers to test
  shared_context "client api_application" do
    let(:api_company) { fleet_managed_company }
    let(:api_application) { fleet_managed_application }
    let(:viewer) { api_application }
    let(:viewer_company_path) { ["data", "viewer", "company"] }
  end

  shared_context "super client api_application" do
    let(:api_company) { super_fleet_managed_company }
    let(:api_application) { super_fleet_managed_application }
    let(:viewer) { api_application }
    let(:viewer_company_path) { ["data", "viewer", "company"] }
  end

  shared_context "partner api_user" do
    let(:api_company) { rescue_company }
    let(:api_user) { rescue_driver }
    let(:viewer) { api_user }
    let(:viewer_company_path) { ["data", "viewer", "company"] }
  end

  shared_context "3rd party partner api_company" do
    let(:api_company) { rescue_company }
    let(:api_application) { third_party_partner_api_application }
    let(:viewer) { api_company }
    let(:viewer_company_path) { ["data", "viewer"] }
  end

  shared_context "3rd party partner api_user" do
    let(:api_company) { rescue_company }
    let(:api_application) { third_party_partner_api_application }
    let(:viewer) { api_user }
    let(:api_user) { rescue_driver }
    let(:viewer_company_path) { ["data", "viewer", "company"] }
  end

  shared_examples "responds with error" do |*examples|
    let(:show_errors) { false }
    it "does not throw error and includes errors on response" do
      expect { response }.not_to raise_error
      # LOLWUT? this way errors_array can be a single hash (which it is 99% of the time)
      # and it can have symbolized keys and we won't have to remember to stringify them.
      # we intentionally shadow errors_array here so that the incredibly useful error /
      # comment "define errors_array in your spec" still shows the correct variable name.
      Array.wrap(errors_array).map(&:deep_stringify_keys).tap do |errors_array|
        expect(response["errors"])
          .to have(errors_array.size).items
          .and deep_include errors_array # define errors_array in your spec
      end
    end

    examples.each { |e| it_behaves_like e }
  end

  # TODO - don't use this - we end up making multiple requests, one to check here, and then one
  # to check other stuff
  shared_examples "does not raise error" do
    it "does not raise error or respond with error message" do
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
    end
  end
end

RSpec.configure do |rspec|
  rspec.include_context "graphql helpers", type: :graphql
end
