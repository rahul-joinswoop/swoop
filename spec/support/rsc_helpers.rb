# frozen_string_literal: true

RSpec.shared_context "basic rsc setup", shared_context: :metadata do
  let!(:api_access_token) do
    create(:api_access_token, company: rescue_company, vendorid: vendor_id)
  end

  let(:location) do
    create(:location, street: "123 Super St.")
  end
  let(:issc_location) do
    create(
      :issc_location,
      fleet_company: agero_company,
      locationid: facility_id,
      site: issc_site,
      location: location,
    )
  end
  let(:issc_site) { create(:partner_site, company: rescue_company) }
  let!(:agero_company) { create(:fleet_company, :agero) }
  let!(:fleet_company) { agero_company } # just an alias..
  let!(:agero_account) do
    create(:account, company: rescue_company, client_company: agero_company)
  end
  let(:rescue_company) { create(:rescue_company) }
  let!(:issc) do
    create(
      :ago_issc,
      :rsc,
      company: rescue_company,
      contractorid: vendor_id,
      issc_location: issc_location,
      api_access_token: api_access_token,
      site: issc_site
    )
  end
  let(:rsc_api) { Agero::Rsc::API.new(api_access_token.access_token) }
end
