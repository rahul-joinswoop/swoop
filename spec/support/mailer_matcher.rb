# frozen_string_literal: true

RSpec::Matchers.define :send_email do |expected|
  match do |actual|
    expect { actual.call }.to change(ActionMailer::Base.deliveries, :count).by(expected)
  end

  failure_message do |actual|
    "expected that ActionMailer::Base.deliveries count would have changed by #{expected}"
  end

  supports_block_expectations
end
