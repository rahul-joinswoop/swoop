# frozen_string_literal: true

RSpec.configure do |config|
  config.around :example, :timezone do |example|
    Time.use_zone(example.metadata[:timezone]) { example.run }
  end
end
