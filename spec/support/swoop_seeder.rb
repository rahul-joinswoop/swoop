# frozen_string_literal: true

class SwoopSeeder

  include Singleton

  class << self

    def setup_logging
      ActiveSupport::Notifications.subscribe "database_cleaner.swoop_seeder" do |name, started, finished, unique_id|
        duration_ms = "(#{(1000 * (finished - started)).round(2)}ms)"
        Rails.logger.debug "Finished DatabaseCleaner.truncate #{duration_ms}"
      end

      ActiveSupport::Notifications.subscribe "load_seed.swoop_seeder" do |name, started, finished, unique_id|
        duration_ms = "(#{(1000 * (finished - started)).round(2)}ms)"
        Rails.logger.debug "Finished Application.load_seed #{duration_ms}"
      end

      ActiveSupport::Notifications.subscribe "load_reports.swoop_seeder" do |name, started, finished, unique_id|
        duration_ms = "(#{(1000 * (finished - started)).round(2)}ms)"
        Rails.logger.debug "Finished Report.load_reports #{duration_ms}"
      end
    end

    def seed_if_necessary
      instance.seed_if_necessary
    end

    def unseeded
      instance.unseeded
    end

  end

  def initialize
    @seeded = false
  end

  def unseeded
    @seeded = false
  end

  def seed_if_necessary
    return if @seeded

    ActiveSupport::Notifications.instrument "database_cleaner.swoop_seeder" do
      DatabaseCleaner.clean_with(:truncation)
    end

    ActiveSupport::Notifications.instrument "load_seed.swoop_seeder" do
      Rails.application.load_seed
    end

    SuperCompany.create!(name: Company::SWOOP)

    ActiveSupport::Notifications.instrument "load_reports.swoop_seeder" do
      Report.load_reports
    end

    @seeded = true
  end

end
