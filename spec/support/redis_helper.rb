# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:each) do
    RedisClient.each do |redis|
      redis.flushdb
    end
  end
end
