# frozen_string_literal: true

def standard_services_for_spec
  [
    ServiceCode::ACCIDENT_TOW,
    ServiceCode::BATTERY_JUMP,
    ServiceCode::FUEL_DELIVERY,
    ServiceCode::GOA,
    ServiceCode::IMPOUND,
    ServiceCode::LOCK_OUT,
    ServiceCode::OTHER,
    ServiceCode::RECOVERY,
    ServiceCode::REPO,
    ServiceCode::SECONDARY_TOW,
    ServiceCode::STORAGE,
    ServiceCode::TIRE_CHANGE,
    ServiceCode::TOW,
    ServiceCode::TOW_IF_JUMP_FAILS,
    ServiceCode::TRANSPORT,
    ServiceCode::WINCH_OUT,
  ]
end

def standard_addons_for_spec
  [
    ServiceCode::ACCIDENT_CLEANUP,
    ServiceCode::AFTER_HOURS_FEE,
    ServiceCode::CC_PROCESSING_FEES,
    ServiceCode::DOLLY,
    ServiceCode::FUEL,
    ServiceCode::GATE_FEE,
    ServiceCode::GOJAK,
    ServiceCode::LABOR,
    ServiceCode::LIEN_FEE,
    ServiceCode::RAMPS,
    ServiceCode::REIMBURSEMENT,
    ServiceCode::RELEASE_FEES,
    ServiceCode::SECOND_TRUCK,
    ServiceCode::TIRE_SKATES,
    ServiceCode::TOLL,
    ServiceCode::WAIT_TIME,
  ]
end

def standard_vehicle_categories_for_spec
  [
    VehicleCategory::FLATBED,
    VehicleCategory::HEAVY_DUTY,
    VehicleCategory::LIGHT_DUTY,
    VehicleCategory::MEDIUM_DUTY,
    VehicleCategory::SUPER_HEAVY,
    VehicleCategory::SERVICE_TRUCK,
    VehicleCategory::WHEEL_LIFT,
    VehicleCategory::COVERED_FLATBED,
  ]
end

def standard_symptoms_for_spec
  [
    Symptom::ACCIDENT,
    Symptom::CUSTOMER_UNSAFE,
    Symptom::DEAD_BATTERY,
    Symptom::FLAT_TIRE,
    Symptom::LOCKED_OUT,
    Symptom::LONG_DISTANCE_TOW,
    Symptom::MECHANICAL_ISSUE,
    Symptom::OTHER,
    Symptom::OUT_OF_FUEL,
  ]
end
