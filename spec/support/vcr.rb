# frozen_string_literal: true

RSpec.configure do |config|
  config.around(:each) do |example|
    if example.metadata.include?(:vcr)
      example.run
    else
      VCR.turned_off { example.run }
    end
  end
end
