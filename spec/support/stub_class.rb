# frozen_string_literal: true

# StubClass provides a slightly easier work-around for
# https://www.rubydoc.info/gems/rubocop-rspec/RuboCop/Cop/RSpec/LeakyConstantDeclaration
module StubClass

  # this has to be used in a before block
  def stub_class(klass, subklass = Object, &block)
    # create a new anonymous class
    stubbed_class = Class.new(subklass)
    # run our block with the class's context
    stubbed_class.class_exec(&block) if block_given?
    # and stub out a const with the name
    stub_const klass, stubbed_class
  end

end
