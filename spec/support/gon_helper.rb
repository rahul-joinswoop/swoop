# frozen_string_literal: true

#  idea from https://github.com/gazay/gon/wiki/Testing-with-RSpec
RSpec.shared_context 'gon' do
  let(:gon) { RequestStore.store[:gon].gon }
  before { Gon.clear }
end

RSpec.configure do |rspec|
  rspec.include_context "gon", gon: true
end
