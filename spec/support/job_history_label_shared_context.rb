# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "job history label shared context" do |ajs_or_history_item_factory|
  describe 'AuditJobStatus or HistoryItem ui_label' do
    subject do
      ajs_or_history_item.ui_label(viewing_company: viewing_company)
    end

    case ajs_or_history_item_factory
    when :audit_job_status
      let(:job_status) { nil } # can be any
      let!(:ajs_or_history_item) { create(:audit_job_status, job_status: job_status, job: job, company: ajs_company, user: ajs_user) }
    when :history_item
      let!(:ajs_or_history_item) { create(:history_item, job: job, company: ajs_company, user: ajs_user, title: HistoryItem::INVOICE_EDITED) }
    end

    let(:fleet_managed_company) { create(:fleet_managed_company, name: 'Fleet Managed company name') }
    let(:fleet_in_house_company) { create(:fleet_in_house_company, name: 'Fleet In House company name') }
    let(:rescue_company) { create(:rescue_company, name: 'Rescue Company name') }
    let(:swoop) { create(:swoop_company) }

    shared_examples 'set fleet_motor_club_company.name to label' do
      case ajs_or_history_item_factory
      when :audit_job_status
        context 'and job.status is created, assigned, eta_rejected or accepted' do
          ['created', 'assigned', 'eta_rejected', 'accepted'].each do |status|
            let(:job_status) { create(:job_status, status.to_sym) }

            it { is_expected.to eq fleet_motor_club_company.name }
          end
        end
      when :history_item
        it { is_expected.to eq ajs_or_history_item.company.name }
      end
    end

    shared_examples 'return empty string if ajs.company and ajs.changed_by_company are nil' do
      context 'when ajs.company an ajs.changed_by_company are nil' do
        let(:ajs_company) { nil }
        let(:ajs_user) { nil }

        it { is_expected.to eq '' }
      end
    end

    shared_examples 'return Customer if source_application is CMW' do
      if ajs_or_history_item_factory == :history_item
        context 'when ajs is a HistoryItem instance' do
          let!(:ajs_or_history_item) { create(:history_item, job: job, company: nil, user: ajs_user, title: history_title, source_application: source_application) }
          let(:oauth_application) { nil }
          let(:ajs_user) { nil }

          context 'and history_item.source_application is present' do
            let(:source_application) do
              SourceApplication.create!(platform: platform, source: source, oauth_application: oauth_application)
            end

            context 'when ajs.source_application.platform is Platform CMW' do
              let(:platform) { SourceApplication::PLATFORM_CONSUMER_MOBILE_WEB }
              let(:source) { SourceApplication::SOURCE_SWOOP }
              let(:history_title) { 'SMS Change Location' }

              it { is_expected.to eq "Customer" }
            end
          end
        end
      end
    end

    shared_examples 'correctly set the label for a FleetInHouse job' do
      context 'and job is FleetInHouseJob' do
        let(:job) { create(:fleet_in_house_job) }

        it_behaves_like 'return Customer if source_application is CMW'

        it_behaves_like 'return empty string if ajs.company and ajs.changed_by_company are nil'

        if ajs_or_history_item_factory == :audit_job_status
          context 'when audit_job_status.source_application is present' do
            let!(:ajs_or_history_item) do
              create(:audit_job_status, job: job, company: fleet_in_house_company, source_application: source_application, user: ajs_user)
            end
            let(:source_application) do
              SourceApplication.create!(platform: platform, source: source, oauth_application: oauth_application)
            end
            let(:ajs_company) { fleet_in_house_company }
            let(:ajs_user) { create(:user, company: fleet_in_house_company) } # can be any

            context 'when ajs.source_application.platform is Client API' do
              let(:platform) { SourceApplication::PLATFORM_CLIENT_API }
              let(:source) { "Client App" }
              let(:oauth_application) { create(:oauth_application, name: source, owner: fleet_in_house_company) }

              it { is_expected.to eq "#{fleet_in_house_company.name}" }
            end

            context 'when ajs.source_application.platform is Partner API' do
              let(:platform) { SourceApplication::PLATFORM_PARTNER_API }
              let(:source) { "Partner API App" }
              let(:oauth_application) { create(:oauth_application, name: source) }

              it { is_expected.to eq "#{oauth_application.name}" }
            end
          end
        end

        context 'and ajs_user is FleetInHouse' do
          let(:ajs_company) { viewing_company }
          let(:ajs_user) { create(:user, company: fleet_in_house_company) }

          it 'formats the label as expected' do
            if viewing_company.rescue?
              expect(subject).to eq fleet_in_house_company.name
            else
              expect(subject).to eq "#{fleet_in_house_company.name} • #{ajs_or_history_item.user_name}"
            end
          end

          context 'and ajs.user is blank' do
            let(:ajs_user) { nil }

            it { is_expected.to eq viewing_company.name }
          end
        end

        context 'and ajs_user is RescueCompany' do
          let(:ajs_company) { viewing_company }
          let(:ajs_user) { create(:user, company: rescue_company) }

          it 'formats the label as expected' do
            if viewing_company.rescue?
              expect(subject).to eq "#{rescue_company.name} • #{ajs_or_history_item.user_name}"
            else
              expect(subject).to eq rescue_company.name
            end
          end

          context 'and ajs.user is blank' do
            let(:ajs_user) { nil }

            it { is_expected.to eq viewing_company.name }
          end
        end
      end
    end

    shared_examples 'format it when audit_job_status.source_application is API for FleetManaged job' do
      if ajs_or_history_item_factory == :audit_job_status
        context 'when audit_job_status.source_application is present' do
          let!(:ajs_or_history_item) do
            create(:audit_job_status, job: job, company: fleet_managed_company, source_application: source_application, user: ajs_user)
          end
          let(:source_application) do
            SourceApplication.create!(platform: platform, source: source, oauth_application: oauth_application)
          end
          let(:ajs_company) { rescue_company }
          let(:ajs_user) { create(:user, company: rescue_company) } # can be any
          let(:job) { create(:fleet_managed_job, fleet_company: fleet_managed_company) }

          context 'when ajs.source_application.platform is Client API' do
            let(:platform) { SourceApplication::PLATFORM_CLIENT_API }
            let(:source) { "Client App" }
            let(:oauth_application) { create(:oauth_application, name: source, owner: fleet_managed_company) }

            it 'formats it correctly' do
              if viewing_company.rescue?
                expect(subject).to eq "Swoop"
              else
                expect(subject).to eq "#{fleet_managed_company.name}"
              end
            end
          end

          context 'when ajs.source_application.platform is Partner API' do
            let(:platform) { SourceApplication::PLATFORM_PARTNER_API }
            let(:source) { "Partner App" }
            let(:oauth_application) { create(:oauth_application, name: source) }

            it 'formats it correctly' do
              expect(subject).to eq "#{oauth_application.name}"
            end
          end
        end
      end
    end

    shared_examples 'correctly set the label for a FleetManaged job' do |expectations|
      it_behaves_like 'return empty string if ajs.company and ajs.changed_by_company are nil'

      context 'and ajs.company is the same as viewing_company' do
        let(:ajs_company) { viewing_company }

        context 'and ajs.user is set' do
          let(:ajs_user) { create(:user) }

          it { is_expected.to eq "#{viewing_company.name} • #{ajs_or_history_item.user_name}" }
        end

        context 'and ajs.user is blank' do
          let(:ajs_user) { nil }

          it { is_expected.to eq viewing_company.name }
        end
      end

      context 'and ajs.company is FleetManaged' do
        let(:ajs_company) { fleet_managed_company }
        let(:ajs_user) { create(:user) }

        context 'and ajs_user is Swoop' do
          before do
            ajs_user.company = swoop
          end

          it 'formats the label as expected' do
            if viewing_company.rescue?
              expect(subject).to eq 'Swoop'
            elsif viewing_company.is_swoop?
              expect(subject).to eq("Swoop • #{ajs_or_history_item.user_name}")
            else
              expect(subject).to eq 'Swoop'
            end
          end
        end

        context 'and ajs_user is FleetManaged' do
          before do
            ajs_user.company = fleet_managed_company
          end

          it 'formats the label as expected' do
            if viewing_company.rescue?
              expect(subject).to eq 'Swoop'
            elsif viewing_company.is_swoop?
              expect(subject).to eq("#{fleet_managed_company.name} • #{ajs_or_history_item.user_name}")
            else
              expect(subject).to eq("#{fleet_managed_company.name} • #{ajs_or_history_item.user_name}")
            end
          end
        end

        context 'and ajs_user is RescueCompany' do
          before do
            ajs_user.company = rescue_company
          end

          it 'formats the label as expected' do
            if viewing_company.rescue?
              expect(subject).to eq("#{rescue_company.name} • #{ajs_or_history_item.user_name}")
            elsif viewing_company.is_swoop?
              expect(subject).to eq("#{rescue_company.name} • #{ajs_or_history_item.user_name}")
            else
              expect(subject).to eq rescue_company.name
            end
          end
        end

        context 'and ajs.user is blank' do
          let(:ajs_user) { nil }

          it 'formats the label as expected' do
            if viewing_company.rescue?
              expect(subject).to eq 'Swoop'
            elsif viewing_company.is_swoop?
              expect(subject).to eq fleet_managed_company.name
            else
              expect(subject).to eq fleet_managed_company.name
            end
          end
        end
      end
    end

    context 'when viewing_company is a RescueCompany' do
      let(:viewing_company) { rescue_company }

      context 'and job is RescueJob' do
        let(:job) { create(:rescue_job) }

        it_behaves_like 'return empty string if ajs.company and ajs.changed_by_company are nil'

        context 'and ajs.company is the same as viewing_company' do
          let(:ajs_company) { viewing_company }

          context 'and ajs.user is set' do
            let(:ajs_user) { create(:user) }

            it { is_expected.to eq "#{viewing_company.name} • #{ajs_or_history_item.user_name}" }
          end

          context 'and ajs.user is blank' do
            let(:ajs_user) { nil }

            it { is_expected.to eq viewing_company.name }
          end
        end
      end

      it_behaves_like 'correctly set the label for a FleetInHouse job'

      context 'and job is a FleetManagedJob' do
        let(:job) { create(:fleet_managed_job, fleet_company: fleet_managed_company) }

        it_behaves_like 'correctly set the label for a FleetManaged job'
        it_behaves_like 'format it when audit_job_status.source_application is API for FleetManaged job'

        it_behaves_like 'return Customer if source_application is CMW'
      end

      context 'and job is a FleetMotorClubJob' do
        let(:job) { create(:fleet_motor_club_job, fleet_company: fleet_motor_club_company) }
        let(:job_status) { create(:job_status, :dispatched) }
        let(:fleet_motor_club_company) { create(:fleet_company, :agero) }

        context 'and ajs.company is nil' do
          let(:ajs_company) { nil }

          context 'and ajs.user is nil' do
            let(:ajs_user) { nil }

            it { is_expected.to eq '' }
          end

          context 'and ajs.user is set' do
            let(:ajs_user) { create(:user, company: company) }
            let(:company) { create(:rescue_company) }

            it 'formats it accordingly' do
              expect(subject).to eq ajs_user.company.name
            end
          end
        end

        context 'and ajs.company is set' do
          let(:ajs_company) { create(:rescue_company) }

          context 'and ajs.user is set' do
            let(:ajs_user) { create(:user) }

            it_behaves_like 'set fleet_motor_club_company.name to label'
          end

          context 'and ajs.user is nil' do
            let(:ajs_user) { nil }

            it_behaves_like 'set fleet_motor_club_company.name to label'
          end
        end
      end
    end

    context 'when viewing_company is a FleetInHouse company' do
      let(:viewing_company) { fleet_in_house_company }

      it_behaves_like 'correctly set the label for a FleetInHouse job'
    end

    context 'when viewing_company is a FleetManaged company' do
      let(:viewing_company) { fleet_managed_company }

      let(:job) { create(:fleet_managed_job, fleet_company: fleet_managed_company, rescue_company: rescue_company) }

      it_behaves_like 'correctly set the label for a FleetManaged job'

      it_behaves_like 'return Customer if source_application is CMW'

      it_behaves_like 'format it when audit_job_status.source_application is API for FleetManaged job'
    end

    context 'when viewing_company is Swoop' do
      let(:viewing_company) { swoop }

      let(:job) { create(:fleet_managed_job, fleet_company: fleet_managed_company, rescue_company: rescue_company) }

      it_behaves_like 'correctly set the label for a FleetManaged job'

      context 'when ajs.souce_application is present' do
        let(:source_application) do
          SourceApplication.create!(platform: platform, source: source, oauth_application: oauth_application)
        end

        if ajs_or_history_item_factory == :audit_job_status
          context 'when ajs is an AuditJobStatus instance' do
            let!(:ajs_or_history_item) do
              create(:audit_job_status, job: job, company: nil, source_application: source_application, user: ajs_user)
            end

            let(:ajs_user) { nil }

            context 'when ajs.source_application.platform is Client API' do
              let(:platform) { SourceApplication::PLATFORM_CLIENT_API }
              let(:source) { "Client App" }
              let(:oauth_application) { create(:oauth_application, name: source, owner: fleet_managed_company) }

              it { is_expected.to eq "#{fleet_managed_company.name} • #{source_application.source}" }
            end

            context 'when ajs.source_application.platform is Partner API' do
              let(:platform) { SourceApplication::PLATFORM_PARTNER_API }
              let(:source) { "Partner API App" }
              let(:oauth_application) { create(:oauth_application, name: source) }

              it { is_expected.to eq "#{oauth_application.name} • #{source_application.source}" }
            end

            context 'when ajs.source_application.platform is WEB' do
              let(:platform) { SourceApplication::PLATFORM_WEB }
              let(:source) { "Swoop" }
              let(:oauth_application) { nil }
              let(:ajs_user) { create(:user, company: swoop) }

              it { is_expected.to eq "#{swoop.name} • #{ajs_user.name} • #{source_application.platform}" }
            end
          end
        end

        if ajs_or_history_item_factory == :history_item
          context 'when ajs is a HistoryItem instance' do
            let!(:ajs_or_history_item) { create(:history_item, job: job, company: nil, user: ajs_user, title: history_title, source_application: source_application) }
            let(:oauth_application) { nil }
            let(:ajs_user) { nil }

            context 'when ajs.source_application.platform is WEB' do
              let(:platform) { SourceApplication::PLATFORM_WEB }
              let(:source) { "Swoop" }
              let(:oauth_application) { nil }
              let(:ajs_user) { create(:user, company: swoop) }
              let(:history_title) { 'Invoice Edited' }

              it { is_expected.to eq "#{swoop.name} • #{ajs_user.name} • #{source_application.platform}" }
            end

            context 'when ajs.source_application.platform is Platform CMW' do
              let(:platform) { SourceApplication::PLATFORM_CONSUMER_MOBILE_WEB }
              let(:source) { SourceApplication::SOURCE_SWOOP }
              let(:history_title) { 'Change Location' }

              it { is_expected.to eq "Customer • #{source_application.platform}" }
            end
          end
        end
      end
    end
  end
end
