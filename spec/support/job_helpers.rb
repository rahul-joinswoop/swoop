# frozen_string_literal: true

RSpec.shared_examples 'validate job status change' do |from_status, to_status|
  it 'has expected status change' do
    expect { service_call }.to change(job, :status).from(from_status.to_s).to(to_status.to_s)
  end
end
