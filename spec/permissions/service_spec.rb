# frozen_string_literal: true

require 'rails_helper'

describe Permissions::Service do
  let!(:flt) do
    create :rescue_company
  end

  let!(:truck_driver) do
    create :user, {
      company: flt,
    }
  end

  let!(:truck_drivers_job) do
    create :rescue_job, {
      rescue_driver: truck_driver,
    }
  end

  let!(:another_drivers_job) do
    create :rescue_job
  end

  it 'doesnt allow any jobs' do
    expect(Permissions::Service.read(Job, truck_driver).length).to eq(0)
  end

  describe 'user permissions' do
    context 'truck driver has run_jobs role' do
      let!(:truck_driver_role) do
        Role.create!(name: 'truck_driver')
      end

      before do
        truck_driver.add_role(:truck_driver)
      end

      it 'only sees truck drivers job' do
        expect(Permissions::Service.read(Job, truck_driver).length).to eq(1)
        expect(Permissions::Service.read(Job, truck_driver).first).to eq(truck_drivers_job)
      end

      context 'biddable jobs permission' do
        let!(:biddable_job) do
          create :rescue_job
        end

        let!(:bid) do
          create(:bid, {
            job: biddable_job,
            company: flt,
            status: Auction::Bid::REQUESTED,
          })
        end

        it 'still sees truck drivers jobs' do
          expect(Permissions::Service.read(Job, truck_driver).length).to eq(2)
          expect(Permissions::Service.read(Job, truck_driver).first).to eq(truck_drivers_job)
          expect(Permissions::Service.read(Job, truck_driver).second).to eq(biddable_job)
        end
      end
    end

    describe 'rescue company role' do
      let!(:rescue_company_role) do
        Role.create!(name: 'rescue_company')
      end

      context 'company has rescue company role' do
        before do
          flt.roles << rescue_company_role
        end

        it 'sees no jobs' do
          expect(Permissions::Service.read(Job, flt).length).to eq(0)
        end

        context 'flt job exists' do
          let!(:flt_job) do
            create :rescue_job, {
              rescue_company: flt,
            }
          end

          it 'sees only jobs assigned to rescue company' do
            expect(Permissions::Service.read(Job, flt).length).to eq(1)
            expect(Permissions::Service.read(Job, flt).first).to eq flt_job
          end
        end
      end
    end

    describe 'all_jobs role' do
      let!(:all_jobs_role) do
        Role.create!(name: 'superuser')
      end

      context 'user has superuser role' do
        let!(:swoop_user) do
          create :user
        end

        before do
          swoop_user.add_role(:superuser)
        end

        it 'sees all jobs' do
          expect(Permissions::Service.read(Job, swoop_user).length).to eq(2)
          expect(Permissions::Service.read(Job, swoop_user)).to include truck_drivers_job
          expect(Permissions::Service.read(Job, swoop_user)).to include another_drivers_job
        end
      end

      context 'company has all jobs role' do
        let!(:swoop) do
          create :super_company
        end

        before do
          swoop.roles << all_jobs_role
        end

        it 'sees all jobs' do
          expect(Permissions::Service.read(Job, swoop).length).to eq(2)
          expect(Permissions::Service.read(Job, swoop)).to include truck_drivers_job
          expect(Permissions::Service.read(Job, swoop)).to include another_drivers_job
        end
      end
    end
  end
end
