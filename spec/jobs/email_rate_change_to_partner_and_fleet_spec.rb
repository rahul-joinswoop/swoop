# frozen_string_literal: true

require "rails_helper"

describe EmailRateChangeToPartnerAndFleet do
  subject(:new_email_rate) do
    described_class.new(
      company_id,
      company_id_to_be_notified,
      user_id
    )
  end

  let(:company) { build(:company, id: 10) }
  let(:company_to_be_notified) { build(:fleet_company, id: 5) }
  let(:user) { build(:user, id: 1) }

  let(:company_id) { company.id }
  let(:company_id_to_be_notified) { company_to_be_notified.id }
  let(:user_id) { user.id }

  describe '.initialize' do
    it 'sets @company_id' do
      expect(new_email_rate.instance_variable_get('@company_id'))
        .to eq company_id
    end

    it 'sets @company_id_to_be_notified' do
      expect(new_email_rate.instance_variable_get('@company_id_to_be_notified'))
        .to eq company_id_to_be_notified
    end

    it 'sets @user_id' do
      expect(new_email_rate.instance_variable_get('@user_id'))
        .to eq user_id
    end
  end

  describe '#perform' do
    before :each do
      allow(Company).to receive(:find).with(company_id)
        .and_return(company)

      allow(Company).to receive(:find).with(company_id_to_be_notified)
        .and_return(company_to_be_notified)

      allow(User).to receive(:find).and_return(user)

      allow(SystemMailer).to receive(:send_mail).and_call_original

      allow_any_instance_of(ActionMailer::MessageDelivery).to(
        receive(:deliver_now)
      ).and_return nil
    end

    it 'sets @company_that_changed_the_rate' do
      new_email_rate.perform

      expect(new_email_rate.instance_variable_get('@company_that_changed_the_rate'))
        .to eq company
    end

    it 'sets @company_to_be_notified' do
      new_email_rate.perform

      expect(new_email_rate.instance_variable_get('@company_to_be_notified'))
        .to eq company_to_be_notified
    end

    it 'sets @user_that_changed_the_rate' do
      new_email_rate.perform

      expect(new_email_rate.instance_variable_get('@user_that_changed_the_rate'))
        .to eq user
    end

    it 'prepares SystemMailer to send the email' do
      expect(SystemMailer).to receive(:send_mail)

      new_email_rate.perform
    end

    it 'sends the email' do
      expect_any_instance_of(ActionMailer::MessageDelivery).to receive(:deliver_now)

      new_email_rate.perform
    end
  end
end
