# frozen_string_literal: true

SITE_URL = 'http://mysite.com'

require "rails_helper"

describe Toa, type: :job do
  subject(:perform_toa) { Toa.new.perform }

  let(:delayed_double) { class_double("Job") } # a DelayedJob Job
  let(:job) do
    create(
      :fleet_managed_job,
      rescue_vehicle: rescue_vehicle,
      service_location: location,
      driver: driver,
      fleet_company: fleet_company
    )
  end
  let(:job_uuid) { job.uuid }
  let(:fleet_company) { create :fleet_company, :tesla }
  let(:driver) { create :drive }
  let(:location) { create :location, :johns_cabin }
  let(:rescue_vehicle) do
    create :rescue_vehicle, location_updated_at: Time.zone.now
  end
  let(:another_driver) { create :drive }
  let(:another_rescue_company) { create :rescue_company, :finish_line, name: "Best Towing" }
  let(:another_rescue_vehicle) do
    create :rescue_vehicle, location_updated_at: Time.zone.now
  end
  let(:sms_track_driver_feature) do
    create :feature, name: Feature::SMS_TRACK_DRIVER
  end
  let(:sms_args) do
    [
      driver.user.phone,
      "Tesla: Your driver is en route. Click the link to see an ETA: " \
      "#{SITE_URL}/txt/#{job_uuid} Thanks.",
      {
        user_id: driver.user.id, job_id: job.id, type: "EtaAuditSms",
        uuid: job_uuid,
      },
    ]
  end

  before do
    allow(Job).to receive(:delay).and_return(delayed_double)
  end

  describe "when job status is 'En Route'", vcr: true do
    before do
      job.move_to_pending
      job.swoop_enroute
      job.fleet_company.features << sms_track_driver_feature
    end

    context "when job is partner_sms_track_driver" do
      it "enqueues an sms with ETA link" do
        ClimateControl.modify SITE_URL: SITE_URL do
          job.update! partner_sms_track_driver: true
          expect(delayed_double).to receive(:send_sms).with(*sms_args)
          perform_toa
        end
      end
    end

    context "when job is sms_track_driver and not partner_sms_track_driver" do
      before do
        job.update sms_track_driver: true
      end

      context "when rescue_company has feature 'SMS_TRACK Driver'" do
        it "enqueues an sms with ETA link" do
          ClimateControl.modify SITE_URL: SITE_URL do
            job.rescue_company.features << sms_track_driver_feature
            expect(delayed_double).to receive(:send_sms).with(*sms_args)
            perform_toa
          end
        end
      end

      context "when rescue_company doesn't have feature 'SMS_TRACK Driver'" do
        it "does not enque an sms with ETA link" do
          expect(delayed_double).not_to receive(:send_sms).with(*sms_args)
          perform_toa
        end
      end
    end
  end

  context "when job is reassigned and status is 'En Route'", vcr: true do
    it "enqueues an sms with ETA link", vcr: { allow_playback_repeats: true } do
      ClimateControl.modify SITE_URL: SITE_URL do
        # we first verify that the original job triggers sending the sms.
        # we just need to use one of the condition branches for the purpose
        fleet_company.features << sms_track_driver_feature

        job.move_to_pending
        job.swoop_enroute
        job.partner_sms_track_driver = true
        job.save!

        expect(delayed_double).to receive(:send_sms).with(*sms_args)
        Toa.new.perform

        # we practically reassign the job, i.e. create a new copy, see
        # SwoopUpdateJobState
        job.reload.after_fleet_reasssigned
        job_copy = Job.last

        expect(job_copy.status).to eql("unassigned")

        job_copy.swoop_enroute
        job_copy.assign_attributes(
          rescue_company: another_rescue_company,
          rescue_vehicle: another_rescue_vehicle, driver: another_driver,
          partner_sms_track_driver: true
        )
        job_copy.save!

        job_copy_uuid = job_copy.uuid

        reassigned_sms_args = [
          another_driver.user.phone,
          "Tesla: Your driver is en route. Click the link to see an ETA: " \
          "#{SITE_URL}/txt/#{job_copy_uuid} Thanks.",
          {
            user_id: another_driver.user.id, job_id: job_copy.id,
            type: "EtaAuditSms", uuid: job_copy_uuid,
          },
        ]
        expect(delayed_double).to receive(:send_sms).with(*reassigned_sms_args)
        Toa.new.perform
      end
    end
  end
end
