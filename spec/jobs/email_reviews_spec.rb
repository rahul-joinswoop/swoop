# frozen_string_literal: true

require "rails_helper"

describe EmailReviews do
  describe '#perform' do
    let(:reviews) do
      [
        create(:review, :with_answers, company: rescue_company),
        create(:review, :with_rating,
               company: rescue_company,
               created_at: (Time.current - 6.hours)),
        create(:review, :with_twilio_replies, company: rescue_company),
      ]
    end

    let(:rescue_company) { create :rescue_company }

    it "works" do
      reviews.each do |r|
        expect(subject).to receive(:send_mails).with(r).and_call_original
        expect(subject)
          .to receive(:send_emails_to_users).with(r, anything).and_call_original
      end

      expect { subject.perform }.not_to raise_error

      expect { reviews.first.reload }
        .to change(reviews.first, :emailed_rating_at)

      expect { reviews.second.reload }
        .to change(reviews.second, :emailed_rating_at)

      expect { reviews.third.reload }
        .to change(reviews.third, :emailed_rating_at)
        .and change(reviews.third, :emailed_feedback_at)
    end

    context "with disable_review_emails" do
      let(:rescue_company) { create :rescue_company, disable_review_emails: true }

      it "works" do
        expect(subject)
          .to receive(:send_mails).once.with(reviews.first).and_call_original

        expect(subject).not_to receive(:send_emails_to_users)

        expect { subject.perform }.not_to raise_error
        expect { reviews.first.reload }
          .to avoid_changing(reviews.first, :emailed_rating_at)

        expect { reviews.second.reload }
          .to avoid_changing(reviews.second, :emailed_rating_at)

        expect { reviews.third.reload }
          .to avoid_changing(reviews.third, :emailed_rating_at)

        expect(reviews.third.emailed_feedback_at).to be_nil
      end
    end
  end
end
