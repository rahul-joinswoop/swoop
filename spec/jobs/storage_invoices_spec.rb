# frozen_string_literal: true

require "rails_helper"

describe StorageInvoices do
  describe '#perform' do
    let!(:job) { create :job, :stored_with_invoice }

    it 'works' do
      expect(StorageTick).to receive(:new).with(job).and_call_original
      expect_any_instance_of(StorageTick).to receive(:call).once.and_call_original
      expect(subject).to receive(:log_command).once.with(job).and_call_original
      expect_any_instance_of(Invoice).to receive(:save!).once.and_call_original
      expect { subject.perform }.not_to raise_error
    end
  end
end
