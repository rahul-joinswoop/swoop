# frozen_string_literal: true

require "rails_helper"

describe EmailFleetEnabledDispatchToSite do
  subject(:new_email_dispatch_enabled) do
    described_class.new(
      fleet_company_id,
      rescue_provider_id,
      fleet_user_id
    )
  end

  let(:fleet_company)   { build(:fleet_company,   id: 5) }
  let(:rescue_provider) { build(:rescue_provider, :default, id: 1) }
  let(:rescue_company)  { build(:rescue_company,  id: 10) }
  let(:fleet_user)      { build(:user, id: 1) }

  let(:fleet_company_id)    { fleet_company.id }
  let(:rescue_provider_id)  { rescue_provider.id }
  let(:fleet_user_id)       { fleet_user.id }

  describe '.initialize' do
    it 'sets @fleet_company_id' do
      expect(new_email_dispatch_enabled.instance_variable_get('@fleet_company_id'))
        .to eq fleet_company_id
    end

    context 'when @fleet_company_id == nil' do
      let(:fleet_company_id)    { nil }

      it 'raises ArgumentError' do
        expect { new_email_dispatch_enabled }.to raise_error(ArgumentError)
      end
    end

    it 'sets @rescue_provider_id' do
      expect(new_email_dispatch_enabled.instance_variable_get('@rescue_provider_id'))
        .to eq rescue_provider_id
    end

    context 'when @rescue_provider_id == nil' do
      let(:rescue_provider_id) { nil }

      it 'raises ArgumentError' do
        expect { new_email_dispatch_enabled }.to raise_error(ArgumentError)
      end
    end

    it 'sets @fleet_user_id' do
      expect(new_email_dispatch_enabled.instance_variable_get('@fleet_user_id'))
        .to eq fleet_user_id
    end

    context 'when @fleet_user_id == nil' do
      let(:fleet_user_id) { nil }

      it 'raises ArgumentError' do
        expect { new_email_dispatch_enabled }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#perform' do
    let(:site_name) { 'Die Website' }

    before :each do
      allow(Company).to receive(:find).with(fleet_company_id)
        .and_return(fleet_company)

      allow(RescueProvider).to receive(:find).with(rescue_provider_id)
        .and_return(rescue_provider)

      allow(rescue_provider).to receive(:name)
        .and_return(site_name)

      allow(User).to receive(:find).and_return(fleet_user)

      allow(SystemMailer).to receive(:send_mail).and_call_original

      allow_any_instance_of(ActionMailer::MessageDelivery).to(
        receive(:deliver_now)
      ).and_return nil
    end

    it 'sets @fleet_company' do
      new_email_dispatch_enabled.perform

      expect(new_email_dispatch_enabled.instance_variable_get('@fleet_company'))
        .to eq fleet_company
    end

    it 'sets @rescue_provider' do
      new_email_dispatch_enabled.perform

      expect(new_email_dispatch_enabled.instance_variable_get('@rescue_provider'))
        .to eq rescue_provider
    end

    it 'sets @fleet_user' do
      new_email_dispatch_enabled.perform

      expect(new_email_dispatch_enabled.instance_variable_get('@fleet_user'))
        .to eq fleet_user
    end

    it 'prepares SystemMailer to send the email' do
      expect(SystemMailer).to receive(:send_mail)

      new_email_dispatch_enabled.perform
    end

    it 'sends the email' do
      expect_any_instance_of(ActionMailer::MessageDelivery).to receive(:deliver_now)

      new_email_dispatch_enabled.perform
    end
  end
end
