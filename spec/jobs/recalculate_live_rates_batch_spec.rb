# frozen_string_literal: true

require "rails_helper"

describe RecalculateLiveRatesBatch, type: :job do
  subject do
    described_class.new(*args)
  end

  let(:company) { create :rescue_company }
  let!(:job) do
    j = create :job, :with_account, :with_invoice_and_line_items, rescue_company: company
    j.invoices.first.update! state: "partner_new"
    j.invoices.first.line_items.each do |li|
      li.update! rate: from_rate
    end
    j
  end
  let!(:invoicing_ledger_item) { create(:invoicing_ledger_item, state: "partner_new", job_id: job.id) }
  let(:from_rate) { create :clean_flat_rate, service_code: create(:service_code, name: ServiceCode::TOW), account: create(:account), company: company }
  let(:to_rate) { create :hourly_p2p, service_code: create(:service_code, name: ServiceCode::TOW), account: create(:account), company: company }

  shared_examples "it works" do
    describe "#initialize" do
      it "works" do
        expect { subject }.not_to raise_error
        expect(subject.instance_variable_get(:@from_rate)).to eq(from_rate)
        expect(subject.instance_variable_get(:@to_rate)).to eq(to_rate)
      end
    end

    describe "#perform" do
      it "works" do
        expect { subject.perform }.not_to raise_error
      end
    end
  end
  context "with rates" do
    let(:args) { [from_rate, to_rate] }

    it_behaves_like "it works"
  end

  context "with rate ids" do
    let(:args) { [from_rate.id, to_rate.id] }

    it_behaves_like "it works"
  end
end
