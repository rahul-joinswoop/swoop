# frozen_string_literal: true

require "rails_helper"

describe EmailDispatchableAdminNotification do
  subject(:email_dispatch_admin_notification) do
    described_class.new(
      user_ids,
      site_id: site_id,
      rescue_provider_id: rescue_provider_id
    )
  end

  let(:test_site) { create(:site, id: 1, company: create(:rescue_company), dispatchable: false) }
  let(:rescue_provider) { build(:rescue_provider, :default, id: 1) }
  let(:user) { build(:user, id: 1) }
  let(:user2) { build(:user, id: 2) }
  let(:tz) { 'America/Los_Angeles' }
  let(:email) { 'test@email.com' }

  let(:site_id) { test_site.id }
  let(:user_ids) { [user.id, user2.id] }
  let(:rescue_provider_id) { rescue_provider.id }
  let(:dispatchable) { test_site.dispatchable }

  describe '.initialize' do
    it 'sets @site_id' do
      expect(email_dispatch_admin_notification.instance_variable_get('@site_id'))
        .to eq site_id
    end

    it 'sets @user_ids' do
      expect(email_dispatch_admin_notification.instance_variable_get('@user_ids'))
        .to eq user_ids
    end

    it 'sets @rescue_provider_id' do
      expect(email_dispatch_admin_notification.instance_variable_get('@rescue_provider_id'))
        .to eq rescue_provider_id
    end
  end

  describe '#perform' do
    subject { email_dispatch_admin_notification.perform }

    before :each do
      allow(User).to receive(:find).and_return([user, user2])

      allow(Site).to receive(:find_by!).with(id: site_id, dispatchable: dispatchable)
        .and_return(test_site)
      allow(RescueProvider).to receive(:find).with(rescue_provider_id)
        .and_return(rescue_provider)

      allow(SystemMailer).to receive(:send_mail).and_call_original

      allow_any_instance_of(ActionMailer::MessageDelivery).to(
        receive(:deliver_now)
      ).and_return nil
    end

    it 'if site_id = nil and rescue_provider_id present' do
      expect(subject)

      expect(email_dispatch_admin_notification.instance_variable_get('@rescue_provider_id'))
        .to eq rescue_provider_id
    end

    it 'prepares SystemMailer to send the email' do
      expect(SystemMailer).to receive(:send_mail)

      expect(subject)
    end

    context 'when @site id is not in DB' do
      let(:site_id) { 100000 }

      before do
        allow(Site).to receive(:find_by!).with(id: site_id, dispatchable: dispatchable)
          .and_raise(ActiveRecord::RecordNotFound)
      end

      it 'raises ActiveRecord::RecordNotFound' do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    it 'doesn\'t send the email' do
      expect_any_instance_of(ActionMailer::MessageDelivery).not_to receive(:deliver_now)

      email_dispatch_admin_notification
    end
  end
end
