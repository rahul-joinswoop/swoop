# frozen_string_literal: true

require 'rails_helper'

describe EmailPasswordRequest do
  subject(:send_password_request_email) do
    EmailPasswordRequest.new(password_request.id).perform
  end

  let(:password_request) { build(:password_request, id: 1, user: user) }
  let(:user) { create(:user) }

  before :each do
    ActionMailer::Base.deliveries = []
    allow(PasswordRequest).to receive(:find).with(
      password_request.id
    ).and_return(password_request)

    allow_any_instance_of(ActionMailer::MessageDelivery).to(
      receive(:deliver_now)
    ).and_call_original

    allow(SystemMailer).to receive(:send_mail).and_call_original

    allow(AuditEmail).to receive(:create)
  end

  context 'perform works' do
    before :each do
      allow_any_instance_of(ActionMailer::MessageDelivery).to(
        receive(:deliver_now)
      ).and_return nil
    end

    describe '#perform' do
      it 'fetches the password request' do
        expect(PasswordRequest).to receive(:find).with(
          password_request.id
        ).and_return(password_request)

        send_password_request_email
      end

      it 'prepares SystemMailer to send the email' do
        expect(SystemMailer).to receive(:send_mail)

        send_password_request_email
      end

      it 'sends the email' do
        expect_any_instance_of(ActionMailer::MessageDelivery).to receive(:deliver_now)

        send_password_request_email
      end
    end
  end

  context 'Sendgrid click tracking' do
    it 'is removed' do
      send_password_request_email
      expect(ActionMailer::Base.deliveries.size).to eq 1

      mail = ActionMailer::Base.deliveries.last
      smtp_api_headers = JSON.load(mail.header['X-SMTPAPI'].to_s)
      expect(smtp_api_headers['filters']).to eq({ "clicktrack" => { "settings" => { "enable" => 0 } } })
    end
  end

  context 'when the PasswordRequest is not valid' do
    let(:password_request) do
      build(:password_request, id: 1, user: user, valid_request: false)
    end

    it 'doesn\'t send the email' do
      expect_any_instance_of(ActionMailer::MessageDelivery).not_to receive(:deliver_now)

      send_password_request_email
    end

    it 'logs a warn' do
      expect(Rails.logger).to receive(:warn)

      send_password_request_email
    end
  end
end
