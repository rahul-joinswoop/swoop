# frozen_string_literal: true

require "rails_helper"

describe EmailInvoiceReceipt do
  subject(:email_invoice_receipt) do
    EmailInvoiceReceipt.new(
      invoice_id,
      invoice_pdf_id,
      invoice_class,
      to: to_email_address,
      cc: cc_email_address,
      bcc: bcc_email_address,
      customize_for_credit_card_charge: customize_for_credit_card_charge
    )
  end

  context '#perform' do
    context 'when it is customized for credit card chage' do
      let(:customize_for_credit_card_charge) { true }
      let(:to_email_address) { 'customer@email.com' }
      let(:cc_email_address) { nil }
      let(:bcc_email_address) { nil }

      let(:invoice) { create(:invoice, sender: rescue_company) }
      let(:invoice_id) { invoice.id }
      let(:invoice_class) { Invoice }

      let(:invoice_pdf) { create(:invoice_pdf, invoice: invoice) }
      let(:invoice_pdf_id) { invoice_pdf.id }

      let(:rescue_company) { create(:rescue_company, dispatch_email: nil) }

      before do
        allow(SystemMailer).to receive(:send_mail).and_call_original
        allow(invoice_class).to receive(:find_by!).and_return(invoice)
      end

      it 'calls SystemMailer.send_mail with expected params' do
        bcc = (ENV['INVOICE_BCC_EMAIL'] ? [ENV['INVOICE_BCC_EMAIL']] : [])

        email_invoice_receipt.perform

        expect(SystemMailer).to have_received(:send_mail).with(
          {
            to: ["customer@email.com"],
            from: nil,
            sender: "operations@joinswoop.com",
            subject: "Receipt from #{invoice.sender.name} for Job ID: #{invoice.job_id}",
            template_name: "invoice",
            bcc: bcc,
          },
          invoice.job.customer,
          invoice.job,
          nil,
          'Invoice Receipt',
          invoice
        )
      end
    end
  end
end
