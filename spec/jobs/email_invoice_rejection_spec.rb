# frozen_string_literal: true

require "rails_helper"

describe EmailInvoiceRejection, type: :job do
  subject(:invoice_rejection_mail) do
    EmailInvoiceRejection.new(invoice_rejection.id).perform
    ActionMailer::Base.deliveries.last
  end

  let(:partner) do
    build_stubbed :rescue_company, :finish_line,
                  name: "Finish Line Towing", accounting_email: "accounting@partner.com"
  end
  let(:assigning_company) do
    build_stubbed :company,
                  name: "Fleet InHouse or Swoop", accounting_email: "accounting@fleet.com",
                  support_email: "support@fleet.com"
  end
  let(:account) { build_stubbed :account }
  let(:job) { build_stubbed :job, id: 123, rescue_company: partner }
  let(:invoice) do
    build_stubbed :invoice, recipient: account, job: job
  end
  let(:invoice_reject_reason) do
    build_stubbed :invoice_reject_reason, text: "Unapproved items"
  end
  let(:invoice_rejection) do
    build_stubbed :invoice_rejection,
                  invoice: invoice, invoice_reject_reason: invoice_reject_reason,
                  notes: "Approval was not granted"
  end

  before do
    allow(InvoiceRejection).to receive(:find).and_return(invoice_rejection)
    # we stub AuditEmail as it will complain about referential integrity
    allow(AuditEmail).to receive(:create).and_return(true)
    allow(job).to receive(:assigning_company).and_return(assigning_company)
  end

  describe "#perform" do
    it "has fleet#support_email as from email" do
      expect(invoice_rejection_mail.from).to eql(["billing@joinswoop.com"])
    end

    it "has Swoop from email when no fleet support_email" do
      assigning_company.support_email = nil
      expect(invoice_rejection_mail.from).to eql(["billing@joinswoop.com"])
    end

    it "includes fleet name in the subject" do
      expect(invoice_rejection_mail.subject).to eql(
        "Invoice Rejected by Fleet InHouse or Swoop for Job ID: 123"
      )
    end

    it "sends the email to partner's accounting_email" do
      expect(invoice_rejection_mail.to).to include("accounting@partner.com")
    end

    context "when no accounting_email" do
      it "sends the email to partner's dispatch_email" do
        partner.accounting_email = nil
        partner.dispatch_email = "dispatch@partner.com"
        expect(invoice_rejection_mail.to).to include("dispatch@partner.com")
      end
    end

    it "includes partner name in the email" do
      expect(invoice_rejection_mail.body).to include("Finish Line Towing")
    end

    it "sends email to fleet" do
      expect(invoice_rejection_mail.to).to include("accounting@fleet.com")
    end

    it "includes fleet name in the email" do
      expect(invoice_rejection_mail.body).to include("Fleet InHouse or Swoop")
    end

    it "includes fleet support email in the email" do
      expect(invoice_rejection_mail.body).to include("support@fleet.com")
    end

    it "includes job id in the email" do
      expect(invoice_rejection_mail.body)
        .to include("Job ID: <strong>123</strong>")
    end

    it "includes rejected reason in the email" do
      expect(invoice_rejection_mail.body).to include("Unapproved items")
    end

    it "includes rejection notes in the email" do
      expect(invoice_rejection_mail.body).to include("Approval was not granted")
    end

    it "does not include rejection notes in the email when empty" do
      invoice_rejection.notes = ""
      expect(invoice_rejection_mail.body).not_to include("Notes")
    end
  end
end
