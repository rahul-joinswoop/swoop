# frozen_string_literal: true

require "rails_helper"

describe JobStatusEmail::Accepted, type: :job do
  subject do
    described_class.new(args)
  end

  let(:args) do
    {
      job_id: job_id,
      company_id: company_id,
      recipients: recipients,
    }
  end

  let(:company_id) { nil }
  let(:from_email_address) { 'operations@joinswoop.com' }
  let(:job) { create(:job) }
  let(:job_id) { job.id }
  let(:recipients) { {} }

  context 'without a company_id' do
    describe 'instantiating an instance' do
      it 'raises an error' do
        expect { described_class.new(args) }
          .to raise_error(JobStatusEmail::MissingRecipientArgumentError)
      end
    end
  end

  describe '#perform' do
    let(:locals) do
      {
        '@original_eta_to_pickup': nil,
        '@orig_eta_mins': nil,
        '@extended_eta_mins': nil,
        '@eta_to_dropoff': job.dropoff_eta,
        '@eta_to_pickup': job.pickup_eta,
        '@job': job,
        '@scheduled_for': scheduled_for,
        '@status_for_timestamp': status_for_timestamp,
        '@status_display_name': status_display_name,
        '@title': title,
        '@tz': tz,
      }
    end

    let(:opts) do
      {
        template_name: JobStatusEmail::TEMPLATE_NAME,
        to: to_email_addresses,
        cc: cc_email_addresses,
        bcc: bcc_email_addresses,
        from: from_email_address,
        subject: email_subject,
        locals: locals,
      }
    end

    let(:bcc_email_addresses) { [] }
    let(:cc_email_addresses) { [] }
    let(:job_update_email) { 'job-updates@example.com' }
    let(:email_subject) { 'foo subject' }
    let(:logged_category) { 'Accepted' }
    let(:logged_company) { company }
    let(:logged_user) { nil }
    let(:mailer) { double(:mailer, deliver_now: true) }
    let(:scheduled_for) { nil }
    let(:status_display_name) { JobStatus::ID_MAP[status_for_timestamp] }
    let(:status_for_timestamp) { JobStatus::ACCEPTED }
    let(:subject_email_addresses) { '' }
    let(:template_name) { 'foo-template.html.erb' }
    let(:title) { 'ETA Provided' }
    let(:to_email_addresses) { [] }
    let(:tz) { 'America/Los_Angeles' }

    context 'with a company_id' do
      before do
        allow(subject)
          .to receive(:subject).and_return(email_subject)

        allow(subject)
          .to receive(:logged_user).and_return(logged_user)

        allow(subject)
          .to receive(:logged_company).and_return(logged_company)

        allow(subject)
          .to receive(:logged_category).twice.and_return(logged_category)

        allow(mailer).to receive(:deliver_now).at_least(:once)
      end

      let(:company) do
        create(:company, job_update_email: job_update_email)
      end

      let(:company_id) { company.id }

      context 'with no recipients' do
        let(:bcc_email_addresses) { [] }
        let(:cc_email_addresses) { [] }
        let(:to_email_addresses) { [company.job_update_email] }

        describe 'when building an email' do
          it 'has the correct to, cc bcc addresses' do
            expect(SystemMailer)
              .to receive(:send_mail)
              .with(
                opts,
                logged_user,
                job,
                logged_company,
                logged_category,
                job
              )
              .and_return(mailer)

            subject.perform
          end
        end
      end

      context 'with a duplicate in :to recipients' do
        let(:bcc_email_addresses) { [] }
        let(:cc_email_addresses) { [] }
        let(:to_email_addresses) { [company.job_update_email, company.dispatch_email] }

        let(:recipients) do
          { to: [company.job_update_email, company.dispatch_email] }
        end

        describe ' when building an email' do
          it 'dedupes the email addresses' do
            expect(SystemMailer)
              .to receive(:send_mail)
              .with(
                opts,
                logged_user,
                job,
                logged_company,
                logged_category,
                job
              )
              .and_return(mailer)

            subject.perform
          end
        end
      end

      context 'with a duplicate in :cc recipients' do
        let(:bcc_email_addresses) { [] }
        let(:cc_email_addresses) { [company.dispatch_email] }
        let(:to_email_addresses) { [company.job_update_email] }

        let(:recipients) do
          { cc: [company.job_update_email, company.dispatch_email] }
        end

        describe 'when building an email' do
          it 'dedupes the email addresses' do
            expect(SystemMailer)
              .to receive(:send_mail)
              .with(
                opts,
                logged_user,
                job,
                logged_company,
                logged_category,
                job
              )
              .and_return(mailer)

            subject.perform
          end
        end
      end

      context 'with a duplicate in :bcc recipients' do
        let(:bcc_email_addresses) { [company.dispatch_email] }
        let(:cc_email_addresses) { [] }
        let(:to_email_addresses) { [company.job_update_email] }

        let(:recipients) do
          { bcc: [company.dispatch_email, company.dispatch_email] }
        end

        describe ' when building an email' do
          it 'dedupes the email addresses' do
            expect(SystemMailer)
              .to receive(:send_mail)
              .with(
                opts,
                logged_user,
                job,
                logged_company,
                logged_category,
                job
              )
              .and_return(mailer)

            subject.perform
          end
        end
      end
    end
  end
end
