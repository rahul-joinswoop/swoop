# frozen_string_literal: true

require "rails_helper"

describe "AdjustedCreatedAt", type: :job, freeze_time: true do
  let!(:job) { create :fleet_managed_job, adjusted_created_at: Time.current }

  let!(:created_job_status) { create :job_status, :created }
  let!(:pending_job_status) { create :job_status, :pending }
  let!(:enroute_job_status) { create :job_status, :en_route }
  let!(:completed_job_status) { create :job_status, :completed }

  let!(:ajs_created) { create :audit_job_status, job: job, job_status: created_job_status }
  let!(:ajs_completed) { create :audit_job_status, job: job, job_status: completed_job_status }

  describe "when updating history" do
    scenario "On creation, job.adjusted_created_at is set to job.created_at" do
      expect(job.adjusted_created_at).to eql(job.created_at)
    end

    scenario "editing completed time should not update adjusted_created_at" do
      ajs_completed.adjusted_dttm = Time.current + 10.minutes
      ajs_completed.save!
      expect(job.adjusted_created_at).to eql(job.created_at)
    end

    scenario "editing the created time should update adjusted_created_at" do
      ajs_created.adjusted_dttm = Time.current + 12.minutes
      ajs_created.save!
      expect(job.adjusted_created_at).to eql(Time.current + 12.minutes)
    end
  end
end
