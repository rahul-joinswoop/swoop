# frozen_string_literal: true

require "rails_helper"

describe JobStatusEmail::EtaExtended, type: :job do
  subject(:job_status_email) do
    JobStatusEmail::EtaExtended.new(job_id: job.id, user_id: job.user_id, validate: false)
  end

  let(:job) { create(:job, status: 'accepted') }

  describe "#preference" do
    it "returns required constant" do
      expect(JobStatusEmail::EtaExtended.preference).to eq(28)
    end
  end

  describe "time/mins methods" do
    before { job_status_email.instance_variable_set(:@job, job) }

    it "returns original Pickup ETA" do
      create_list(:time_of_arrival, 2, job_id: job.id,  eba_type: 5)

      expect(job_status_email.original_eta_to_pickup).to eq('Tue, 12 Jan 2016 00:00:00 UTC +00:00')
    end

    context "have updates on ETA" do
      before(:each) do
        @latest_eta = nil

        (1..3).reverse_each do |i|
          eta = i.hours.ago
          @latest_eta = eta if i == 1
          record_datetime = (eta - 5.minutes)
          create(:time_of_arrival, job_id: job.id, eba_type: 5, time: eta, created_at: record_datetime, updated_at: record_datetime)
        end
      end

      it "returns new Pickup ETA" do
        expect(job_status_email.eta_to_pickup.to_s).to match(@latest_eta.to_s)
      end

      it "returns original ETA in minutes" do
        expect(job_status_email.orig_eta_mins).to eq(5)
      end

      it "returns extended ETA in minutes" do
        expect(job_status_email.extended_eta_mins).to eq(125)
      end
    end
  end

  describe "#title" do
    it "returns the title" do
      expect(job_status_email.title).to eq('ETA Extended')
    end
  end
end
