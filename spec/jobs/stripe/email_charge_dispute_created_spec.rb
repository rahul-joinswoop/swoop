# frozen_string_literal: true

require "rails_helper"

describe Stripe::EmailChargeDisputeCreated do
  subject(:new_email) do
    Stripe::EmailChargeDisputeCreated.new(charge.id)
  end

  let(:invoice) do
    create(
      :invoice,
      :with_line_items,
      :with_payments,
      sender: rescue_company
    )
  end

  let(:rescue_company) { create(:rescue_company, name: 'Real Tow') }

  let!(:charge) do
    create(
      :charge,
      payment: invoice.payments.first,
      upstream_charge_id: 'ch_1CkB0QJBq4iiGApq3Qqz8gMv',
      invoice: invoice,
      status: InvoicePaymentCharge::CHARGE_STATUS_SUCCESSFUL
    )
  end

  describe "#perform" do
    let(:system_mailer_double) { double(deliver_now: nil) }

    it "instantiates SystemMailer properly" do
      expect { new_email.perform }.to send_email(1)
    end
  end
end
