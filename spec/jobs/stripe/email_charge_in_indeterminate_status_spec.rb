# frozen_string_literal: true

require "rails_helper"

describe Stripe::EmailChargeInIndeterminateStatus do
  subject(:new_email) do
    Stripe::EmailChargeInIndeterminateStatus.new(charge_id)
  end

  let(:charge) do
    create(:charge, invoice: invoice)
  end
  let(:charge_id) { charge.id }
  let(:invoice) { create(:invoice) }

  describe "#perform" do
    let(:system_mailer_double) { double(deliver_now: nil) }

    before do
      allow(SystemMailer).to receive(:send_mail).and_return(system_mailer_double)

      new_email.perform
    end

    it "instantiates SystemMailer properly" do
      expect(SystemMailer).to have_received(:send_mail).with(
        {
          template_name: 'stripe_charge_issue',
          to: [Utils::STRIPE_SUPPORT_EMAIL_ADDRESS],
          from: "Swoop <operations@joinswoop.com>",
          subject: "Stripe Charge Issue - Action Required",
          locals: { :@charge => charge },
        }, nil, nil, charge.invoice.sender, "StripeChargeIssue", nil
      )
    end

    it "calls SystemMailer#deliver_now" do
      expect(system_mailer_double).to have_received(:deliver_now)
    end
  end
end
