# frozen_string_literal: true

require "rails_helper"

describe Stripe::EmailRefundFailed do
  subject(:new_email) do
    Stripe::EmailRefundFailed.new(rescue_company.id, job.id, 1529599290)
  end

  let(:rescue_company) { create(:rescue_company, accounting_email: 'acc@email.com') }
  let(:job) { create(:rescue_job, rescue_company_id: rescue_company.id) }
  let(:refund_date) { Time.current }

  describe "#perform" do
    let(:system_mailer_double) { double(deliver_now: nil) }

    it "instantiates SystemMailer properly" do
      expect { new_email.perform }.to send_email(1)
    end
  end
end
