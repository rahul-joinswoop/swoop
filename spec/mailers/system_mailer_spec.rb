# frozen_string_literal: true

require 'rails_helper'

describe SystemMailer, type: :mailer do
  subject(:send_mail) { system_mailer_instance.send_mail(args, user, job, company, "SystemMailer Spec", nil) }

  let!(:user) { create(:user) }
  let(:job) { create(:rescue_job) }
  let(:company) { create(:rescue_company) }

  let!(:system_mailer_instance) { SystemMailer.new }

  context "has all valid email addresses" do
    before(:each) do
      allow(system_mailer_instance).to receive(:mail)
      allow(system_mailer_instance).to receive(:track_email)
    end

    let(:valid_response) { { to: ["swoopuser@test.com"], cc: ["swoopusercc@test.com"], bcc: [], locals: [] } }
    let(:args) { { to: "swoopuser@test.com,", cc: "swoopusercc@test.com", locals: [] } }

    it "does not remove any emails" do
      expect(system_mailer_instance).to receive(:mail).with({
        cc: ["swoopusercc@test.com"],
        to: ["swoopuser@test.com"],
      })

      send_mail
    end

    it "does not raise a rollbar error" do
      expect(Rollbar).not_to receive(:error)

      send_mail
    end

    context "when it receives some valid email addresses" do
      let(:args) do
        { to: "swoopuser@test ; swoopuser@test.com, another@test.com ", cc: "swoopuser@test.,swoopusercc@test.com", bcc: "bcc@test", locals: [] }
      end

      it "removes invalid emails" do
        expect(system_mailer_instance).to receive(:mail).with({
          bcc: [],
          cc: ["swoopusercc@test.com"],
          to: [" swoopuser@test.com", " another@test.com "],
        })

        send_mail
      end

      it "attempts to send the email" do
        expect(system_mailer_instance).to receive(:track_email)

        send_mail
      end

      it "raises a rollbar warning" do
        expect(Rollbar).to receive(:warning).with(
          "Includes invalid email address(es)",
          invalid_emails: a_hash_including({ to: ["swoopuser@test "], cc: ["swoopuser@test."], bcc: ["bcc@test"] }),
          user_id: user.id,
          job_id: job.id,
          company_id: company.id,
          category: "SystemMailer Spec",
        )

        send_mail
      end
    end
  end

  context "when it hasn't any valid email addresses" do
    let(:args) { { to: "swoopuser@test;,", cc: "swoopuser@test.", bcc: "bcc@test" } }

    it "does not attempt to send the email" do
      expect(system_mailer_instance).not_to receive(:mail)
      expect(system_mailer_instance).not_to receive(:track_email)

      send_mail
    end

    it "raises a rollbar warning" do
      expect(Rollbar).to receive(:warning).with(
        "Includes invalid email address(es)",
        invalid_emails: a_hash_including({ to: ["swoopuser@test"], cc: ["swoopuser@test."], bcc: ["bcc@test"] }),
        user_id: user.id,
        job_id: job.id,
        company_id: company.id,
        category: "SystemMailer Spec",
      )

      send_mail
    end
  end

  context 'partner_api_email_verification' do
    let(:subject) { SystemMailer.new.send_mail(args, nil, nil, nil, 'Email Verification', email_verification) }
    let(:email_verification) { create :partner_api_email_verification }
    let(:args) do
      {
        template_name: 'partner_api_email_verification',
        to: email_verification.user_email,
        from: 'Swoop <hello@joinswoop.com>',
        subject: "Verify Swoop Email",
        locals: {
          :@email_verification => email_verification,
          :@app => email_verification.application,
        },
      }
    end

    it "works" do
      expect(subject.body)
        .to include(auth_partner_api_email_verification_url(email_verification))
    end
  end

  context 'partner_api_password_request' do
    let(:subject) { SystemMailer.new.send_mail(args, nil, nil, nil, 'Password Request', password_request) }
    let(:password_request) { create :partner_api_password_request }
    let(:args) do
      {
        template_name: 'partner_api_password_request',
        to: password_request.user_email,
        from: 'Swoop <hello@joinswoop.com>',
        subject: "Invoicing Agero (Swoop) Jobs",
        locals: {
          :@password_request => password_request,
          :@app => password_request.application,
        },
      }
    end

    it "works" do
      expect(subject.body)
        .to include(auth_partner_api_password_request_url(password_request))
        .and include("You’re now eligible for Agero (Swoop) jobs through #{ERB::Util.html_escape_once(password_request.application.name)}")
    end
  end
end
