# frozen_string_literal: true

# http://localhost:5000/rails/mailers
class SystemMailerPreview < ActionMailer::Preview

  def invoice
    #    SystemMailer.invoice(Invoice.joins(:recipient_company).where(recipient_type:"Account").where.not(companies:{accounting_email:nil}).last)
    invoice = Invoice.where(recipient_type: "Account", job_id: 24).last
    invoice_pdf = invoice.generate_pdf
    EmailInvoice.new(invoice.id, invoice_pdf.id).perform
  end

  def invoice_receipt
    invoice = Invoice.where(recipient_type: "User").last
    invoice_pdf = invoice.generate_pdf
    EmailInvoiceReceipt.new(invoice.id, invoice_pdf.id).perform
  end

  def invoice_rejection
    invoice =
      Invoice
        .joins(:job)
        .where(
          "jobs.rescue_company_id IS NOT NULL AND " \
          "jobs.type = ? AND job_id IS NOT NULL",
          "FleetInHouseJob"
        )
        .last
    invoice_reject_reason = InvoiceRejectReason.last
    invoice_rejection = InvoiceRejection.first_or_create!(
      invoice: invoice,
      invoice_reject_reason: invoice_reject_reason,
      notes: "Approval was not granted for Skates. Please remove and resubmit."
    )
    invoice.job.assigning_company
    EmailInvoiceRejection.new(invoice_rejection.id).perform
  end

  def rate_change
    EmailRateChange.new(Rate.last.id, User.with_role(:root).map(&:id)).perform
  end

  def partner_signup
    user = User.last

    EmailPartnerSignup.new(user.company_id, user.id).perform
  end

  def dispatch
    job = Job.where("fleet_company_id!=rescue_company_id").where.not(fleet_company_id: nil, rescue_company_id: nil).last
    EmailDispatch.new(job.id, job.rescue_company.id).perform
  end

  def review
    job = Job.find_by_sql("select jobs.* from jobs join reviews on jobs.id=reviews.job_id and jobs.rescue_driver_id is not null and send_sms_to_pickup is null").last
    review = job.review
    user = User.with_role(:admin).first
    EmailReview.new(review.id, user.id).perform
  end

  def review_pickup
    job = Job.find_by_sql("select jobs.* from jobs join reviews on jobs.id=reviews.job_id and jobs.rescue_driver_id is not null and send_sms_to_pickup is true").last
    review = job.review
    user = User.with_role(:admin).first
    EmailReview.new(review.id, user.id).perform
  end

  def fleet_enabled_dispatch_to_site
    fleet_company = FleetCompany.last
    rescue_provider = RescueProvider.last
    user = User.with_role(:fleet).first
    EmailFleetEnabledDispatchToSite.new(fleet_company.id, rescue_provider.id, user.id).perform
  end

  def job_accepted()
    job = Job.last
    user = User.with_role(:admin).first
    mailer = JobStatusEmail::Accepted.new(job_id: job.id, user_id: user.id, validate: false)
    acc = mailer.perform
    acc
  end

  def job_canceled()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::Canceled.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

  def job_completed()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::Completed.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

  def job_created()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::Created.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

  def job_en_route()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::EnRoute.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

  def job_on_site()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::OnSite.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

  def job_scheduled()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::Scheduled.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

  def job_tow_destionation()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::TowDestination.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

  def job_towing()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::Towing.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

  def job_dispatched()
    job = Job.last
    user = User.with_role(:admin).first
    JobStatusEmail::Dispatched.new(job_id: job.id, user_id: user.id, validate: false).perform
  end

end
