# frozen_string_literal: true

require "rails_helper"

describe ConsumerMobileWebController do
  let(:job) { create(:job, uuid: "abcdefg") }

  describe "get_location" do
    it "works" do
      get :get_location, params: { uuid: job.uuid }
      expect(response).to be_successful
      job.reload
      expect(job.get_location_clicked).to eq true
    end
  end

  describe "show_eta" do
    it "works" do
      get :show_eta, params: { uuid: job.uuid }
      expect(response).to be_successful
    end
  end

  describe "survey" do
    it "works" do
      create(:web_review, job: job)
      get :survey, params: { uuid: job.uuid }
      expect(response).to be_successful
    end
  end

  describe "user tracking" do
    it "tracks the customer using consumer mobile web", freeze_time: true do
      get :get_location, params: { uuid: job.uuid }
      customer = job.driver.user
      expect(UserVisitWorker).to have_enqueued_sidekiq_job(Date.today.iso8601, customer.id, SourceApplication.consumer_mobile_web.id)
    end
  end
end
