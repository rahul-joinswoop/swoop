# frozen_string_literal: true

require "rails_helper"

describe TwilioRepliesController, type: :controller do
  let(:test_phone) { '+14156039663' }

  describe "POST create" do
    describe "Customer confirm Partner On Site" do
      subject { post :create, params: params, as: :json }

      # I'm pretty sure this flow uses "To" and "Body" keys. I'm not sure about the others.
      let(:params) do
        {
          "ToCountry" => "US",
          "ToState" => "AL",
          "SmsMessageSid" => "SM88391eac4cacaee1d517f6b7cd1c5620",
          "NumMedia" => "0",
          "ToCity" => "GADSDEN",
          "FromZip" => "",
          "SmsSid" => "SM88391eac4cacaee1d517f6b7cd1c5620",
          "FromState" => "CA",
          "SmsStatus" => "received",
          "FromCity" => "",
          "Body" => "1",
          "FromCountry" => "US",
          "ToZip" => "35904",
          "MessageSid" => "SM88391eac4cacaee1d517f6b7cd1c5620",
          "AccountSid" => "AC6e404418519f46ded0fd769a1171cdcf",
          "ApiVersion" => "2010-04-01",
          "To" => audit_sms.to,
          "From" => audit_sms.from,
        }
      end

      let(:audit_sms) { create(:audit_sms, :confirm_partner_on_site, job: nil, to: '+1555444333', from: '+1555444333') }

      let!(:job) do
        job = create(:fleet_managed_job, status: job_status)
        job.audit_job_statuses << create(:audit_job_status, job_status: create(:job_status, ajs_status))
        job.audit_sms << audit_sms

        job.save!
        job
      end

      shared_examples 'move job.status to onsite and append SMS platform to the audit_job_status' do
        it 'moves job.status to onsite' do
          expect { subject && job.reload }.to change(job, :status).from(job_status.to_s).to('onsite')
        end

        it 'adds only one job.audit_job_status' do
          expect { subject && job.reload }.to change(job.audit_job_statuses, :count).by(1)
        end

        it 'sets job.audit_job_status.source_application_id as SMS to onsite record' do
          subject && job.reload

          on_site_ajs = job.audit_job_statuses.find_by!(job_status_id: JobStatus::ON_SITE)
          expect(on_site_ajs.source_application_id).to eq SourceApplication.sms_application.id
        end
      end

      context 'when job is dispatched' do
        let(:job_status) { :dispatched }
        let(:ajs_status) { :dispatched }

        it_behaves_like 'move job.status to onsite and append SMS platform to the audit_job_status'
      end

      context 'when job is enroute' do
        let(:job_status) { :enroute }
        let(:ajs_status) { :en_route }

        it_behaves_like 'move job.status to onsite and append SMS platform to the audit_job_status'
      end
    end
  end

  context "Unexpected message" do
    subject do
      post :create, params: {
        From: test_phone,
        Body: '5, awesome service',
      }
    end

    it "creates Twilio Reply" do
      expect { subject }.to change(TwilioReply, :count)

      expect(response.status).to eq(302)
    end

    context 'RequestContext setup' do
      let(:request_context) { RequestContext.new }

      before do
        allow(RequestContext).to receive(:current).and_return(request_context)
      end

      it 'sets RequestContext with SMS source application' do
        expect(request_context).to receive(:source_application_id=).with(SourceApplication.sms_application.id)

        subject
      end
    end
  end

  context "ReviewAuditSms sent" do
    let!(:fleet_company) { create(:fleet_company, :fleet_demo) }
    let!(:customer) do
      create(:user, {
        phone: test_phone,
      })
    end

    let(:fleet_managed_job) { create(:fleet_managed_job) }

    let!(:review) do
      create(:review, {
        phone: test_phone,
        company: fleet_company,
        user: customer,
        job: fleet_managed_job,
      })
    end

    let!(:review_audit_sms) do
      create(:review_audit_sms, {
        to: test_phone,
        job: fleet_managed_job,
        user: customer,
      })
    end

    it "updates review" do
      # Send the review
      expect do
        post :create, params: {
          From: test_phone,
          Body: '5, awesome service',
        }
      end.to change(TwilioReply, :count)

      review.reload
      expect(review.rating).to eq(5)
      expect(response.status).to eq(302)

      # We should ask for feedback
      expect(TwilioMessage.jobs.size).to eq 1
      expect(review.done?).to be false

      # Send the feedback
      expect do
        post :create, params: {
          From: test_phone,
          Body: 'GOOD FEEDBACK',
        }
      end.to change(TwilioReply, :count)

      # We should be thanked for feedback
      expect(TwilioMessage.jobs.size).to eq 2
      review.reload
      expect(review.done?).to be true

      # Send some extra bogus message
      expect do
        post :create, params: {
          From: test_phone,
          Body: "You're welcome!",
        }
      end.to change(TwilioReply, :count)

      # We shouldn't send any more texts
      expect(TwilioMessage.jobs.size).to eq 2
    end
  end

  # Expected responses to a Sms::Confirmation like:
  # Fleet Response (#1135): Request received at 1623-1699 5th Ave, San Franc... (Reply “1” to update). ETA coming soon. Support: 415-555-0278
  context "Sms::Confirmation sent" do
    let!(:customer) do
      create(:user, {
        phone: test_phone,
      })
    end

    let(:fleet_managed_job) { create(:fleet_managed_job) }

    let!(:sms_confirmation) do
      create(:audit_sms, :sms_confirmation, {
        to: test_phone,
        job: fleet_managed_job,
        user: customer,
      })
    end

    it "Queues LocationAuditSms" do
      expect do
        post :create, params: {
          From: test_phone,
          Body: '1',
        }
      end.to change(TwilioReply, :count)
        .and change(Delayed::Job, :count)

      expect(response.status).to eq(302)
    end

    it "does not Queue LocationAuditSms" do
      expect do
        post :create, params: {
          From: test_phone,
          Body: 'hi there',
        }
      end.to change(TwilioReply, :count)
        .and change(Delayed::Job, :count).by(0)

      expect(response.status).to eq(302)
    end
  end
end
