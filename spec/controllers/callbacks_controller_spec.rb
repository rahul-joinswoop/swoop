# frozen_string_literal: true

require "rails_helper"

describe CallbacksController, type: :controller do
  let(:fleet_managed_job) { create(:fleet_managed_job) }

  context "no job" do
    describe 'POST typeform' do
      it "returns :404" do
        post :review_callback,
             params: {
               form_response: {
                 "hidden": {
                   "job": "123",
                 },

               },
             }
        expect(response.status).to eq(404)
      end
    end
  end

  context "Job review" do
    before do
      stub_const "ID1", "tTey"
      stub_const "ID2", "kjzO"
      stub_const "ID3", "svaI"
      stub_const "ID4", "kuh4"
      stub_const "ID5", "53308823"

      stub_const "VAL1", 1
      stub_const "VAL2", 2
      stub_const "VAL3", 3
      stub_const "VAL4", 4
      stub_const "VAL5", "work harder"
    end

    let!(:q1) { create(:five_star_question, ref: ID1, order: 1) }
    let!(:q2) { create(:five_star_question, ref: ID2, order: 2) }
    let!(:q3) { create(:five_star_question, ref: ID3, order: 3) }
    let!(:q4) { create(:five_star_question, ref: ID4, order: 4) }
    let!(:q5) { create(:long_text_question, ref: ID5, order: 5) }

    let!(:survey) { create(:typeform_survey, questions: [q1, q2, q3, q4, q5]) }
    let!(:review) { create(:survey_review, survey: survey) }

    describe 'POST typeform' do
      context 'when expected data is received' do
        subject do
          post :review_callback,
               {
                 params: {
                   form_response: {
                     "hidden": {
                       "job": "#{review.job.id}",
                     },
                     "answers": answers,
                   },
                 },

                 as: :json,
               }
        end

        let(:answers) do
          [
            {
              "type": "number",
              "number": VAL1,
              "field": {
                "id": ID1,
                "type": "opinion_scale",
              },
            },
            {
              "type": "number",
              "number": VAL2,
              "field": {
                "id": ID2,
                "type": "opinion_scale",
              },
            },
            {
              "type": "number",
              "number": VAL3,
              "field": {
                "id": ID3,
                "type": "opinion_scale",
              },
            },
            {
              "type": "number",
              "number": VAL4,
              "field": {
                "id": ID4,
                "type": "opinion_scale",
              },
            },
            {
              "type": "text",
              "text": VAL5,
              "field": {
                "id": "53308823",
                "type": "long_text",
              },
            },

          ]
        end

        context 'RequestContext setup' do
          let(:request_context) { RequestContext.new }

          before do
            allow(RequestContext).to receive(:current).and_return(request_context)
          end

          it 'sets RequestContext with SMS source application' do
            expect(request_context).to receive(:source_application_id=).with(SourceApplication.consumer_mobile_web.id)

            subject
          end
        end

        it "Updates survey reviews" do
          subject && review.reload

          expect(response.status).to eq(200)
          expect(review.survey_results.count).to eq(5)

          result1 = Survey::Result.find_by!(job: review.job, survey_question: q1)
          result2 = Survey::Result.find_by!(job: review.job, survey_question: q2)
          result3 = Survey::Result.find_by!(job: review.job, survey_question: q3)
          result4 = Survey::Result.find_by!(job: review.job, survey_question: q4)
          result5 = Survey::Result.find_by!(job: review.job, survey_question: q5)

          expect(result1.int_value).to eq(VAL1)
          expect(result2.int_value).to eq(VAL2)
          expect(result3.int_value).to eq(VAL3)
          expect(result4.int_value).to eq(VAL4)
          expect(result5.string_value).to eq(VAL5)

          expect(review.nps_question1).to eq(VAL1)
          expect(review.nps_question2).to eq(VAL2)
          expect(review.nps_question3).to eq(VAL3)
          expect(review.nps_question4).to eq(VAL4)
          expect(review.nps_feedback).to eq(VAL5)
        end
      end

      # what are we exacly testing here?
      context 'when it returns 422' do
        subject do
          post :review_callback,
               params: {
                 form_response: {
                   "hidden": {
                     "job": "#{review.job.id}",
                   },
                   "answers": [
                     {
                       "type": "number",
                       "number": 8,
                       "field": {
                         "id": "tTey",
                         "type": "opinion_scale",
                       },
                     },
                     {
                       "type": "number",
                       "number": 7,
                       "field": {
                         "id": "kjzO",
                         "type": "opinion_scale",
                       },
                     },
                     {
                       "type": "number",
                       "number": 3,
                       "field": {
                         "id": "svaI",
                         "type": "opinion_scale",
                       },
                     },
                   ],

                 },

                 as: :json,
               }
        end

        let(:review) { create(:typeform_review) }

        it "Updates typeform reviews, 422" do
          subject

          expect(response.status).to eq(422)
        end
      end
    end
  end
end
