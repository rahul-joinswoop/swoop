# frozen_string_literal: true

require "rails_helper"

describe PartnerAPIPasswordRequestsController, type: :controller do
  render_views

  describe "#create" do
    subject { post :create, format: :json, params: params }

    let(:user) { create :user }
    let(:application) { create :doorkeeper_application }
    let(:params) { { user: { email: user.email }, application: { uid: application.uid } } }

    it "works" do
      expect(subject).to be_created
    end
  end

  describe "#update" do
    subject { patch :update, format: :json, params: params }

    let(:user) { create :user }
    let(:password) { Faker::Hipster.sentence }
    let(:password_request) { create :partner_api_password_request, user: user }
    let(:params) { { uuid: password_request.uuid, user: { password: password } } }

    it "works" do
      expect(subject).to be_ok
      # make sure our password was changed
      expect { user.reload }.to change(user, :encrypted_password)
      # and our password_request should be invalidated
      expect { password_request.reload }
        .to change(password_request, :valid_request).from(true).to(false)
    end

    context "errors" do
      let(:error) { JSON.parse(subject.body)['errors'].first }

      shared_examples "it is unprocessable" do
        it do
          expect(subject).to be_unprocessable
          expect(error).to eq(expected_error)
          # none of these errors should prevent us from reusing this password request
          expect { password_request.reload }
            .not_to change(password_request, :valid_request)
        end
      end

      context 'with a blank password' do
        let(:password) { nil }
        let(:expected_error) { "param is missing or the value is empty: password" }

        it_behaves_like "it is unprocessable"
      end

      context 'without a password param' do
        let(:params) { super().merge(user: {}) }
        let(:expected_error) { "param is missing or the value is empty: user" }

        it_behaves_like "it is unprocessable"
      end

      context 'without a user param' do
        let(:params) { super().without(:user) }
        let(:expected_error) { "param is missing or the value is empty: user" }

        it_behaves_like "it is unprocessable"
      end

      context 'with a short password' do
        let(:password) { "asdf" }
        let(:expected_error) { "Validation failed: Password is too short (minimum is 8 characters)" }

        it_behaves_like "it is unprocessable"
      end

      context 'with an bad uuid' do
        let(:params) { super().merge(uuid: SecureRandom.uuid) }

        it "is not found" do
          expect(subject).to be_not_found
        end
      end
    end
  end

  describe "#show", gon: true do
    subject { get :show, format: :html, params: params }

    let(:password_request) { create :partner_api_password_request }
    let(:params) { { uuid: password_request.uuid } }

    it "works" do
      expect(subject).to be_ok
      expect(gon['uuid']).to eq(password_request.uuid)
      expect(gon['user']).to eq({ email: password_request.user_email })
    end

    context "with a bad uuid" do
      let(:params) { { uuid: SecureRandom.uuid } }

      it "redirects" do
        expect(subject).to redirect_to('/forgotPassword')
      end
    end

    shared_examples "it returns not found" do
      it do
        expect(subject).to be_not_found
        expect(gon['error']).to eq('expired request')
      end
    end

    context "with an invalid password_request" do
      let(:password_request) do
        create :partner_api_password_request, valid_request: false
      end

      it_behaves_like "it returns not found"
    end

    context "with an expired link" do
      let(:password_request) do
        create :partner_api_password_request, created_at: 2.days.ago
      end

      it_behaves_like "it returns not found"
    end
  end
end
