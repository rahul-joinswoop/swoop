# frozen_string_literal: true

require "rails_helper"

describe GraphQLController, type: :controller do
  describe '#batch' do
    subject do
      post :batch,
           params: {
             _json: queries,
           },
           as: :json
    end

    let(:queries) do
      [
        {
          operation_name: "ViewerQuery",
          query: (
            <<~GRAPHQL
              query ViewerQuery {
                viewer {
                  ...on User {
                    roles(first: 50) {
                      edges {
                        node {
                          name
                        }
                      }
                    }
                    company {
                      features(first: 50) {
                        edges {
                          node {
                            name
                          }
                        }
                      }
                    }
                  }
                }
              }
              GRAPHQL
          ),
          variables: {},
        },
        {
          operation_name: "ViewerFeatureQuery",
          query: (
                <<~GRAPHQL
              query ViewerFeatureQuery {
                viewer {
                  ...on User {
                    company {
                      features(first: 50) {
                        edges {
                          node {
                            name
                          }
                        }
                      }
                    }
                  }
                }
              }
              GRAPHQL
              ),
          variables: {},
        },
        {
          operation_name: "ViewerRoleQuery",
          query: (
                <<~GRAPHQL
              query ViewerRoleQuery {
                viewer {
                  ...on User {
                    roles(first: 50) {
                      edges {
                        node {
                          name
                        }
                      }
                    }
                  }
                }
              }
              GRAPHQL
              ),
          variables: {},
        },
      ]
    end

    context "errors" do
      let(:operation_name) { 'getJobs' }

      context "without auth" do
        it "works" do
          expect(Rollbar)
            .to receive(:error)
            .with(GraphQLController::AUTHENTICATION_FAILURE)

          subject

          expect(response.status).to eql(401)
          parsed = JSON.parse response.body
          expect(parsed.dig('errors', 0, 'message'))
            .to eq(GraphQLController::AUTHENTICATION_FAILURE)
        end
      end

      context "pg errors" do
        stub_fleet_company_login

        let(:queries) do
          [
            *super(),
            {
              query: (
                <<~GRAPHQL
                  query #{operation_name} {
                    jobs(before: "") {
                      pageInfo {
                        endCursor
                      }
                    }
                  }
              GRAPHQL
              ),
              variables: {},
            },
          ]
        end

        it "works" do
          subject
          expect(response.status).to eql(200)
          parsed = JSON.parse response.body
          expect(parsed.last.dig('errors', 0, 'message')).to eq(GraphQLController::UNKNOWN_ERROR)
        end
      end
    end

    shared_examples "queries" do
      it "works" do
        subject
        expect(response.status).to eql(200)
        parsed = JSON.parse response.body
        parsed.each { |r| expect(r).not_to include('errors') }
      end
    end

    shared_examples "mutations" do
      let(:encoded_job_id) { job.to_ssid }
      let(:variables) do
        {
          input: {
            job: {
              id: encoded_job_id,
              status: new_status,
            },
          },
        }
      end

      let(:queries) do
        [
          *super(),
          {
            query: (
              <<~GRAPHQL
              mutation UpdateJobStatus($input: UpdateJobStatusInput!) {
                updateJobStatus(input: $input) {
                  job {
                    id
                    status
                    statusUpdatedAt
                  }
                }
              }
            GRAPHQL
            ),
            variables: variables,
          },
        ]
      end

      it "works" do
        subject
        expect(response.status).to eql(200)
        parsed = JSON.parse response.body
        parsed.each { |r| expect(r).not_to include('errors') }
        expect { job.reload }.to change(job, :status).to(new_status.downcase)
      end
    end

    context "with rescue_company" do
      let(:job) { create :rescue_job, status: :pending, rescue_company: company }
      let(:new_status) { 'Dispatched' }

      context "as a company" do
        stub_rescue_company_login :with_location_types
        it_behaves_like "queries"
        it_behaves_like "mutations"
      end

      context "as a user" do
        stub_rescue_company_user_login :with_location_types
        it_behaves_like "queries"
        it_behaves_like "mutations"
      end
    end

    context "with fleet_managed_company" do
      let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }
      let(:new_status) { 'Canceled' }

      context "as a company" do
        stub_fleet_managed_company_login :with_location_types
        it_behaves_like "queries"
        it_behaves_like "mutations"
      end

      context "as a user" do
        stub_fleet_managed_company_user_login :with_location_types
        it_behaves_like "queries"
        it_behaves_like "mutations"
      end
    end
  end

  describe '#execute' do
    subject do
      request.headers.merge! headers
      post :execute,
           params: {
             query: query,
             operation_name: operation_name,
             variables: variables,
           },
           as: :json
    end

    let(:headers) { {} }
    let(:operation_name) { '' }
    let(:encoded_company_id) { company.to_ssid }
    let(:encoded_user_id) { user.to_ssid }
    let(:variables) { {} }

    describe "error bubbling" do
      let(:company) { create :rescue_company, :with_sites, :with_location_types, :with_service_codes }
      let(:variables) { { "input": { "job": { "poNumber": "1234", "refNumber": "ref", "customer": { "fullName": "John", "phone": "+12223334444" }, "vehicle": { "make": "Audi", "model": "A5", "year": 1998, "color": "Black" }, "service": { "willStore": false, "symptom": "Battery" }, "location": { "serviceLocation": { "address": "Fisherman", "lat": 1.0, "lng": 52.0, "locationType": "Garage" }, "dropoffLocation": { "address": "Fisherman", "lat": 1.0, "lng": 52.0, "locationType": "Garage" }, "pickupContact": { "fullName": "John", "phone": "+12223334444" }, "dropoffContact": { "fullName": "John", "phone": "+12223334444" } } } } } }
      let(:operation_name) { 'CreateJobMutation' }

      let(:query) do
        <<~GRAPHQL
          mutation CreateJobMutation($input: CreateJobInput!) {
            createJob(input: $input) {
              job {
                poNumber
                refNumber
                customer {
                  fullName
                  phone
                }
                service {
                  willStore
                  name
                  symptom
                }
                location {
                  serviceLocation {
                    address
                    lat
                    lng
                    googlePlaceId
                    locationType
                  }
                  pickupContact {
                    fullName
                    phone
                  }
                  dropoffLocation {
                    address
                    lat
                    lng
                    googlePlaceId
                    locationType
                  }
                  dropoffContact {
                    fullName
                    phone
                  }
                }
                vehicle {
                  make
                  model
                  year
                  color
                  license
                  odometer
                  vin
                }
                texts {
                  sendLocation
                  sendEtaUpdates
                  sendDriverTracking
                  sendReview
                  sendTextsToPickup
                }
              }
            }
          }
        GRAPHQL
      end

      let(:user) { create :user, :rescue, :dispatcher, :driver, company: company }
      let(:token) { create :doorkeeper_access_token, resource_owner_id: user.id }
      let(:errors) { JSON.parse response.body }

      before(:each) do
        authenticate_request(controller.request, token)
      end

      context "with error bubbling enabled" do
        let(:company) do
          c = super()
          c.features << create(:feature, name: Feature::LEGACY_GRAPHQL_ERRORS)
          c.save!
          c
        end

        it "works" do
          subject
          expect(errors).to eq(
            { "errors" =>
              [{
                "message" => "Variable input of type CreateJobInput! was provided invalid value",
                "locations" => [{ "line" => 1, "column" => 28 }],
                "extensions" => {
                  "value" =>
                  { "job" =>
                    {
                      "poNumber" => "1234",
                      "refNumber" => "ref",
                      "customer" => { "fullName" => "John", "phone" => "+12223334444" },
                      "vehicle" =>
                      { "make" => "Audi", "model" => "A5", "year" => 1998, "color" => "Black" },
                      "service" => { "willStore" => false, "symptom" => "Battery" },
                      "location" =>
                      {
                        "serviceLocation" =>
                                                {
                                                  "address" => "Fisherman",
                                                  "lat" => 1.0,
                                                  "lng" => 52.0,
                                                  "locationType" => "Garage",
                                                },
                        "dropoffLocation" =>
                        {
                          "address" => "Fisherman",
                          "lat" => 1.0,
                          "lng" => 52.0,
                          "locationType" => "Garage",
                        },
                        "pickupContact" => { "fullName" => "John", "phone" => "+12223334444" },
                        "dropoffContact" => { "fullName" => "John", "phone" => "+12223334444" },
                      },
                    } },
                  "problems" => [{ "path" => [], "explanation" => "\"Garage\" is not a valid LocationType" }],
                },
              }] }
          )
        end
      end

      context "with error bubbling disabled" do
        it "works" do
          subject
          expect(errors).to eq({
            "errors" => [
              { "extensions" => { "argumentName" => "poNumber", "code" => "argumentNotAccepted", "name" => "CreateJobJobInput", "typeName" => "InputObject" }, "locations" => [{ "column" => 28, "line" => 1 }], "message" => "InputObject 'CreateJobJobInput' doesn't accept argument 'poNumber'", "path" => ["mutation CreateJobMutation", "createJob", "input", "job", "poNumber"] },
              { "extensions" => { "argumentName" => "name", "argumentType" => "ServiceCodeName!", "code" => "missingRequiredInputObjectAttribute", "inputObjectType" => "CreateJobServiceInput" }, "locations" => [{ "column" => nil, "line" => nil }], "message" => "Argument 'name' on InputObject 'CreateJobServiceInput' is required. Expected type ServiceCodeName!", "path" => ["mutation CreateJobMutation", "createJob", "input", "job", "service", "name"] },
              { "extensions" => { "argumentName" => "symptom", "code" => "argumentNotAccepted", "name" => "CreateJobServiceInput", "typeName" => "InputObject" }, "locations" => [{ "column" => 28, "line" => 1 }], "message" => "InputObject 'CreateJobServiceInput' doesn't accept argument 'symptom'", "path" => ["mutation CreateJobMutation", "createJob", "input", "job", "service", "symptom"] },
              { "extensions" => { "code" => "argumentLiteralsIncompatible", "typeName" => "CoercionError" }, "locations" => [{ "column" => nil, "line" => nil }], "message" => "\"Garage\" is not a valid LocationType", "path" => ["mutation CreateJobMutation", "createJob", "input", "job", "location", "serviceLocation", "locationType"] }, { "extensions" => { "code" => "argumentLiteralsIncompatible", "typeName" => "CoercionError" }, "locations" => [{ "column" => nil, "line" => nil }], "message" => "\"Garage\" is not a valid LocationType", "path" => ["mutation CreateJobMutation", "createJob", "input", "job", "location", "dropoffLocation", "locationType"] },
            ],
          })
        end
      end
    end

    context "errors" do
      let(:operation_name) { 'getJobs' }

      context "without auth" do
        let(:query) do
          <<~GRAPHQL
          query #{operation_name} {
		  __type(name: "LocationType") {
              name
            }
          }
          GRAPHQL
        end

        it "works" do
          expect(Rollbar)
            .to receive(:error)
            .with(GraphQLController::AUTHENTICATION_FAILURE)

          subject

          expect(response.status).to eql(401)
          parsed = JSON.parse response.body
          expect(parsed.dig('errors', 0, 'message'))
            .to eq(GraphQLController::AUTHENTICATION_FAILURE)
        end
      end

      context "pg errors" do
        stub_fleet_company_login

        let(:query) do
          <<~GRAPHQL
            query #{operation_name} {
              jobs(before: "") {
                pageInfo {
                  endCursor
                }
              }
            }
          GRAPHQL
        end

        it "works" do
          subject
          expect(response.status).to eql(200)
          parsed = JSON.parse response.body
          expect(parsed.dig('errors', 0, 'message')).to eq(GraphQLController::UNKNOWN_ERROR)
        end
      end
    end

    describe "execute" do
      describe "mutation" do
        let(:fleet_managed_job) { create :fleet_managed_job, status: :pending, fleet_company: company }
        let(:rescue_job) { create :rescue_job, status: :pending, rescue_company: company }
        let(:fleet_in_house_job) { create :fleet_in_house_job, status: :pending, fleet_company: company }

        let(:encoded_job_id) do
          job.to_ssid
        end

        let(:service_location) { create :graphql_location_input_type }
        let(:job_input) do
          {
            job: {
              id: encoded_job_id,
              location: {
                serviceLocation: service_location,
              },
            },
          }
        end

        let(:variables) do
          {
            input: job_input,
          }
        end

        shared_examples "mutations" do
          it "works" do
            subject
            expect(response.status).to eql(200)
            parsed = JSON.parse response.body
            expect(parsed).not_to include('errors')
          end
        end

        context "with an operation name" do
          let(:operation_name) { 'UpdateJob' }
          let(:query) do
            <<~GRAPHQL
            mutation #{operation_name}($input: EditJobInput!) {
              updateJob(input: $input) {
                job {
                  id
                  location {
                    serviceLocation{
                      address
                      lat
                      lng
                      locationType
                    }
                  }
                }
              }
            }
            GRAPHQL
          end

          context "with rescue_company" do
            let(:job) { rescue_job }

            context "as a user" do
              stub_rescue_company_user_login :with_location_types
              it_behaves_like "mutations"
            end
          end

          context "with fleet_managed_company" do
            let(:job) { fleet_managed_job }

            context "as a company" do
              stub_fleet_managed_company_login :with_location_types
              it_behaves_like "mutations"
            end

            context "as a user" do
              stub_fleet_managed_company_user_login :with_location_types
              it_behaves_like "mutations"
            end
          end
        end

        context "without an operation name" do
          let(:variables) { {} }
          let(:query) do
            <<~GRAPHQL
            mutation {
              updateJob(input: {
                job: {
                  id: "#{encoded_job_id}",
                  location: {
                    serviceLocation: {
                      address: "#{service_location[:address]}",
                      lat: #{service_location[:lat]},
                      lng: #{service_location[:lng]},
                      locationType: "#{service_location[:locationType]}"
                    }
                  }
                }
              }) {
                job {
                  id
                  location {
                    serviceLocation{
                      address
                      lat
                      lng
                      locationType
                    }
                  }
                }
              }
            }
            GRAPHQL
          end

          context "with rescue_company" do
            let(:job) { rescue_job }

            context "as a user" do
              stub_rescue_company_user_login :with_location_types
              it_behaves_like "mutations"
            end
          end

          context "with fleet_managed_company" do
            let(:job) { fleet_managed_job }

            context "as a company" do
              stub_fleet_managed_company_login :with_location_types
              it_behaves_like "mutations"
            end

            context "as a user" do
              stub_fleet_managed_company_user_login :with_location_types
              it_behaves_like "mutations"
            end
          end
        end
      end

      describe "query" do
        let(:operation_name) { 'getViewer' }
        let(:company_query) do
          <<~GRAPHQL
            query #{operation_name} {
              viewer {
                ...on Application {
                  company {
                    id
                    name
                  }
                }
              }
            }
          GRAPHQL
        end
        let(:user_query) do
          <<~GRAPHQL
            query #{operation_name} {
              viewer {
                ...on User {
                  id
                  fullName
                }
              }
            }
          GRAPHQL
        end

        shared_examples "query" do
          it "works" do
            subject
            expect(response.status).to eql(200)
            parsed = JSON.parse response.body
            expect(parsed).not_to include('errors')
          end
        end

        context "with rescue_company" do
          let(:job) { rescue_job }

          context "as a company" do
            let(:query) { company_query }

            stub_rescue_company_login :with_location_types
            it_behaves_like "query"
          end

          context "as a user" do
            let(:query) { user_query }

            stub_rescue_company_user_login :with_location_types
            it_behaves_like "query"
          end
        end

        context "with fleet_managed_company" do
          let(:job) { fleet_managed_job }

          context "as a company" do
            let(:query) { company_query }

            stub_fleet_managed_company_login :with_location_types
            it_behaves_like "query"
          end

          context "as a user" do
            let(:query) { user_query }

            stub_fleet_managed_company_user_login :with_location_types
            it_behaves_like "query"
          end
        end
      end

      context "subscription" do
        shared_examples "subscriptions" do
          let(:headers) do
            {
              ::Subscriptions::WebhookTransport::GRAPHQL_SUBSCRIPTION_WEBHOOK_URL => "https://foo.bar/asdf",
              ::Subscriptions::WebhookTransport::GRAPHQL_SUBSCRIPTION_WEBHOOK_URL => "asdf1234",
            }
          end

          let(:query) do
            <<~GRAPHQL.strip
              subscription JobStatusEntered {
                jobStatusEntered {
                  id
                  status
                  statusUpdatedAt
                }
              }
            GRAPHQL
          end
          it "works" do
            expect { subject }.to change(GraphQLSubscription, :count).by(1)

            expect(response.status).to eql(200)
            parsed = JSON.parse response.body
            id = parsed.dig('extensions', 'subscription', 'id')
            expect(id).to be_present
            subscription = SomewhatSecureID.load! id
            expect(subscription.query_string).to eq(query)
          end
        end

        context "with rescue_company" do
          context "as a company" do
            stub_rescue_company_login :with_location_types
            it_behaves_like "subscriptions"
          end

          context "as a user" do
            stub_rescue_company_user_login :with_location_types
            it_behaves_like "subscriptions"
          end
        end

        context "with fleet_managed_company" do
          context "as a company" do
            stub_fleet_managed_company_login :with_location_types
            it_behaves_like "subscriptions"
          end

          context "as a user" do
            stub_fleet_managed_company_user_login :with_location_types
            it_behaves_like "subscriptions"
          end
        end
      end
    end
  end

  describe "record visit" do
    let(:query) do
      <<~GRAPHQL
        query getViewerId {
          viewer {
              id
            }
          }
        }
      GRAPHQL
    end

    context "with a user" do
      stub_user_login(:rescue)

      it "records a visit" do
        Sidekiq::Testing.inline! do
          post :execute, params: { query: query, operation_name: "getViewerId", variables: {} }, as: :json
        end
        expect(response).to be_successful
        visit = user.visits.last
        expect(visit.source_application.platform).to eq SourceApplication::PLATFORM_WEB
      end
    end

    context "without a user" do
      stub_rescue_company_login

      it "does not record a visit" do
        Sidekiq::Testing.inline! do
          post :execute, params: { query: query, operation_name: "getViewerId", variables: {} }, as: :json
        end
        expect(response).to be_successful
        expect(User::Visit.count).to eq 0
      end
    end
  end
end
