# frozen_string_literal: true

require "rails_helper"

describe WelcomeController, type: :controller do
  describe "apple_app_site_association" do
    let(:association) { WelcomeController::APPLE_APP_SITE_ASSOCIATION }

    it "works" do
      get :apple_app_site_association
      expect(response.body).to eq(association)
      expect(response.headers['Content-Type']).to eq("application/json; charset=utf-8")
    end
  end

  describe "assetlinks.json" do
    let(:association) { WelcomeController::ANDROID_APP_SITE_ASSOCIATION }

    it 'is successful' do
      get :assetlinks
      expect(response.code).to eq('200')
    end

    it 'has the right body' do
      get :assetlinks
      expect(response.body).to eq(association)
    end

    it 'has the right Content-Type' do
      get :assetlinks
      expect(response.headers['Content-Type']).to eq("application/json; charset=utf-8")
    end
  end
end
