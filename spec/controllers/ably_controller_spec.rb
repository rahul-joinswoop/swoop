# frozen_string_literal: true

require "rails_helper"

describe AblyController, type: :controller do
  describe '#auth' do
    subject do
      post :auth, as: :json
    end

    context "without auth" do
      it "works" do
        expect(Rollbar)
          .to receive(:error)
          .with(GraphQLController::AUTHENTICATION_FAILURE, anything)
        expect(Swoop)
          .not_to receive(:ably_rest_client)

        subject

        expect(response.status).to eql(401)
        parsed = JSON.parse response.body
        expect(parsed.dig('message'))
          .to eq(GraphQLController::AUTHENTICATION_FAILURE)
      end
    end

    context "with auth" do
      stub_fleet_in_house_company_login

      let(:auth_response) do
        {
          "keyName" => "asdf1234",
          "timestamp" => 1547512841767,
          "nonce" => "asdf09u2309u230ru",
          "capability" => "{\"*\":[\"presence\",\"subscribe\"]}",
          "mac" => "asdfsadf+23r234lkjasdf=",
        }
      end

      it "works" do
        expect(Rollbar)
          .not_to receive(:error)
        expect(Swoop)
          .to receive_message_chain(:ably_rest_client, :auth, :create_token_request)
          .and_return(auth_response)

        subject

        expect(response.status).to eql(201)
        expect(JSON.parse(response.body)).to eq(auth_response)
      end
    end
  end
end
