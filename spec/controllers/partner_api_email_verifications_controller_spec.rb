# frozen_string_literal: true

require "rails_helper"

describe PartnerAPIEmailVerificationsController, type: :controller do
  render_views

  let(:user) { build(:user) }

  describe "#create" do
    subject { post :create, format: :json, params: params }

    before :each do
      allow(Authentication::VerifyEmailForPartnerAPI)
        .to receive(:call)
        .and_return(mocked_context)
    end

    let(:params) do
      {
        email: email,
        client_id: client_id,
        redirect_uri: redirect_uri,
      }
    end
    let(:email) { user.email }
    let(:client_id) { SecureRandom.uuid }
    let(:redirect_uri) { Faker::Internet.url }
    let(:email_verification) do
      PartnerAPIEmailVerification.new user: user, client_id: client_id, redirect_uri: redirect_uri
    end
    let(:mocked_context) do
      interactor_context = double("Interactor Context")

      allow(interactor_context)
        .to receive(:result)
        .once
        .and_return(email_verification)

      allow(interactor_context)
        .to receive(:success?)
        .once
        .and_return(true)

      interactor_context
    end

    it 'triggers email verification' do
      expect(Authentication::VerifyEmailForPartnerAPI)
        .to receive(:call)
        .and_return(mocked_context)

      subject
    end

    context 'negative cases' do
      context 'the result of the service is failure' do
        let(:error) { Faker::Hipster.sentence }

        it 'returns http status 422' do
          allow(mocked_context)
            .to receive(:success?)
            .once
            .and_return(false)

          allow(mocked_context)
            .to receive(:error)
            .once
            .and_return(error)

          expect(subject).to be_unprocessable
        end
      end
    end

    context 'positive cases' do
      context 'the result of the service is success' do
        it 'returns http status 201' do
          expect(subject).to be_created
        end
      end
    end
  end

  describe "#get" do
    subject { get :show, format: :html, params: params }

    let(:email_verification) { create :partner_api_email_verification }
    let(:params) { { uuid: email_verification.uuid } }

    it "works" do
      # we should get redirected to our oauth page
      expect(subject)
        .to redirect_to(
          oauth_authorization_url(
            client_id: email_verification.client_id,
            redirect_uri: email_verification.redirect_uri,
            scope: "company",
            response_type: "code"
          )
        )
      # and our email_verification model should have changed, but we have to
      # reload it to verify
      expect { email_verification.reload }
        .to change(email_verification, :valid_request)
        .from(true)
        .to(false)

      # and make sure we set an access_token in our session
      expect(session[:access_token]).to eq Doorkeeper::AccessToken.last.token
    end

    context "with a bad uuid" do
      let(:params) { { uuid: SecureRandom.uuid } }

      it "works" do
        # we get the generic react_application layout rendered
        expect(subject).to be_ok
        # and nothing has changed in our email_verification model
        expect { email_verification.reload }
          .not_to change { email_verification }
      end
    end
  end
end
