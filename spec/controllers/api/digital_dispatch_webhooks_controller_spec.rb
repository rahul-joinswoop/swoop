# frozen_string_literal: true

require "rails_helper"

describe API::DigitalDispatchWebhooksController, type: :controller do
  describe '#handle_rsc_event' do
    subject(:post_handle_rsc_event) do
      post :handle_rsc_event, body: event.to_json, as: :json
    end

    let(:access_token) { 'Token: 1234567890' }
    let(:callback_token) { 'Token: CALLBACK1234567890' }
    let(:header_authorization) { "Bearer #{callback_token}" }

    let!(:api_access_token) do
      create(
        :api_access_token,
        access_token: access_token, # will be used to call RSC endpoint
        callback_token: callback_token, # will be used to validate the incoming msg
        vendorid: vendor_id,
        connected: true
      )
    end

    let(:event) do
      {
        notificationEventId: notification_event_id,
        notificationCode: notification_code,
        vendorId: vendor_id,
        facilityId: facility_id,
        dispatchSource: dispatch_source,
        dispatchRequestNumber: dispatch_request_number,
        title: title,
      }.with_indifferent_access
    end

    let(:notification_event_id) { '123456789' }
    let(:vendor_id) { '50230' }
    let(:facility_id) { '1' }
    let(:dispatch_source) { 'eDispatch' }
    let(:dispatch_request_number) { 123456789 }
    let(:title) { 'New Offer' }
    let(:message) { 'You have a new job.' }

    before do
      request.headers['Authorization'] = header_authorization
    end

    context "when access token is disconnected" do
      let(:notification_code) { 1 }

      before do
        api_access_token.update!(connected: false)
      end

      it "logs Rollbar error" do
        expect(Rollbar).to receive(:error)
        post_handle_rsc_event
        expect(response.status).to eq 401
      end
    end

    context "when access token is deleted" do
      let(:notification_code) { 1 }

      before do
        api_access_token.update!(deleted_at: Time.current)
      end

      it "logs Rollbar error" do
        expect(Rollbar).to receive(:error)
        post_handle_rsc_event
        expect(response.status).to eq 401
      end
    end

    context 'when event.notificationCode is 1: NewJobOffered' do
      let(:notification_code) { 1 }

      it "schedules a worker to create a new job" do
        post_handle_rsc_event
        expect(response.status).to eq 200
        expect(Agero::Rsc::NewJobOfferedWorker).to have_enqueued_sidekiq_job(callback_token, event)
      end

      context 'when the given callback_token does not match with a vendorid in Swoop db' do
        let(:header_authorization) { 'Bearer Token: a not existent one' }

        it "does not schedule a worker to create a new job" do
          post_handle_rsc_event
          expect(response.status).to eq 401
          expect(Agero::Rsc::NewJobOfferedWorker.jobs).to be_empty
        end
      end
    end

    context 'when event.notificationCode is 8: JobApproved' do
      let(:notification_code) { 8 }

      it "schedules a worker to approve a job" do
        post_handle_rsc_event
        expect(response.status).to eq 200
        expect(Agero::Rsc::JobApprovedWorker).to have_enqueued_sidekiq_job(callback_token, event)
      end

      context 'when the given callback_token does not match with a vendorid in Swoop db' do
        let(:header_authorization) { 'Bearer Token: a not existent one' }

        it "does not schedule a worker to approve a job" do
          post_handle_rsc_event
          expect(response.status).to eq 401
          expect(Agero::Rsc::JobApprovedWorker.jobs).to be_empty
        end
      end
    end

    context 'when event.notificationCode is 7: EtaRejected' do
      let(:notification_code) { 7 }

      it "schedules a worker to approve a job" do
        post_handle_rsc_event
        expect(response.status).to eq 200
        expect(Agero::Rsc::EtaRejectedWorker).to have_enqueued_sidekiq_job(callback_token, event)
      end

      context 'when the given callback_token does not match with a vendorid in Swoop db' do
        let(:header_authorization) { 'Bearer Token: a not existent one' }

        it "does not schedule a worker to approve a job" do
          post_handle_rsc_event
          expect(response.status).to eq 401
          expect(Agero::Rsc::EtaRejectedWorker.jobs).to be_empty
        end
      end
    end

    context 'when event.notificationCode is 5: JobCancelled' do
      let(:notification_code) { 5 }

      it "schedules a worker to approve a job" do
        post_handle_rsc_event
        expect(response.status).to eq 200
        expect(Agero::Rsc::AgeroCanceledWorker).to have_enqueued_sidekiq_job(callback_token, event)
      end

      context 'when the given callback_token does not match with a vendorid in Swoop db' do
        let(:header_authorization) { 'Bearer Token: a not existent one' }

        it "does not schedule a worker to approve a job" do
          post_handle_rsc_event
          expect(response.status).to eq 401
          expect(Agero::Rsc::AgeroCanceledWorker.jobs).to be_empty
        end
      end
    end

    context 'when event.notificationCode is 23: JobCancelledByDispatcher' do
      let(:notification_code) { 23 }

      it "schedules a worker to approve a job" do
        post_handle_rsc_event
        expect(response.status).to eq 200
        expect(Agero::Rsc::AgeroCanceledWorker).to have_enqueued_sidekiq_job(callback_token, event)
      end

      context 'when the given callback_token does not match with a vendorid in Swoop db' do
        let(:header_authorization) { 'Bearer Token: a not existent one' }

        it "does not schedule a worker to approve a job" do
          post_handle_rsc_event
          expect(response.status).to eq 401
          expect(Agero::Rsc::AgeroCanceledWorker.jobs).to be_empty
        end
      end
    end

    context 'when event.notificationCode is 30: Service Timeout' do
      let(:notification_code) { 30 }

      let(:job) do
        create :fleet_motor_club_job, {
          fleet_company: fleet_company,
          adjusted_created_at: Time.now,
          rescue_company: rescue_company,
        }
      end

      let!(:issc_dispatch_request) do
        create(
          :issc_dispatch_request,
          dispatchid: dispatch_request_number,
          issc: issc,
          status: "new_request",
          max_eta: 90.minutes,
          job: job
        )
      end

      let(:rescue_company) { create(:rescue_company) }
      let(:fleet_company) { create(:fleet_company, :ago) }
      let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM, api_access_token: api_access_token) }

      it "enqueues a worker to expire the job" do
        Agero::Rsc::ExpireJobWorker.jobs.clear
        post_handle_rsc_event
        expect(response.code).to eq '200'
        expect(Agero::Rsc::ExpireJobWorker).to have_enqueued_sidekiq_job(job.id)
      end
    end

    describe "record audit data" do
      let(:notification_code) { 1 }

      it "records audit data about the request" do
        post_handle_rsc_event
        audit_isscs = AuditIssc.where(system: "rsc", incoming: true, dispatchid: dispatch_request_number)
        expect(audit_isscs.size).to eq 1
        audit_issc = audit_isscs.first
        expect(audit_issc.http_status_code).to eq 200
      end

      it "records audit data about requests with errors" do
        expect(controller).to receive(:handle_rsc_event).and_raise("boom")
        post_handle_rsc_event
        audit_isscs = AuditIssc.where(system: "rsc", incoming: true, dispatchid: dispatch_request_number)
        expect(audit_isscs.size).to eq 1
        audit_issc = audit_isscs.first
        expect(audit_issc.http_status_code).to eq 500
      end

      it "records audit data about forbidden requests" do
        request.headers['Authorization'] = nil
        post_handle_rsc_event
        audit_isscs = AuditIssc.where(system: "rsc", incoming: true, dispatchid: dispatch_request_number)
        expect(audit_isscs.size).to eq 1
        audit_issc = audit_isscs.first
        expect(audit_issc.http_status_code).to eq 401
      end
    end

    describe 'RequestContext setup' do
      let(:notification_code) { 1 } # can be any
      let(:request_context) { RequestContext.new }
      let!(:rsc_source_application) { SourceApplication.rsc_application }

      before do
        allow(RequestContext).to receive(:current).and_return(request_context)
      end

      it 'sets RequestContext with RSC source_application' do
        expect(request_context).to receive(:source_application_id=).with(rsc_source_application.id)

        post_handle_rsc_event
      end
    end
  end
end
