# frozen_string_literal: true

require "rails_helper"

describe API::IsscWebhookController, type: :controller do
  before do
    stub_class 'MockHttpSuccess' do
      def initialize
        @got_called = false
      end

      def code
        @got_called = true
        200
      end

      def called?
        @got_called
      end
    end
  end

  let(:mock_http_success) { MockHttpSuccess.new }
  let!(:connection_manager) { ISSC::ConnectionManager.new }
  let!(:company) { create(:rescue_company) }
  let!(:fleet_company) { create(:fleet_company, issc_client_id: 'USAC') }
  let!(:account) { create(:account, company: company, client_company: fleet_company) }

  shared_examples 'set RequestContext' do
    context 'RequestContext setup' do
      let(:request_context) { RequestContext.new }
      let!(:issc_source_application) { SourceApplication.issc_application }

      before do
        allow(RequestContext).to receive(:current).and_return(request_context)
      end

      it 'sets RequestContext with ISSC source_application' do
        expect(request_context).to receive(:source_application_id=).with(issc_source_application.id)

        subject
      end
    end
  end

  describe 'POST #create' do
    subject { post :create, body: envelope }

    before do
      allow(controller).to receive(:api_authorized?).and_return(true)
    end

    context "when envelope contains details" do
      let(:dispatch_request) { IsscDispatchRequest.find_by(dispatchid: "131264") }
      let(:job) do
        Job.joins(:issc_dispatch_request).find_by(issc_dispatch_requests: { dispatchid: "131264" })
      end

      let!(:issc) { create(:isscs, company: company, clientid: 'USAC', contractorid: 'AZ0123456') }

      let(:envelope) do
        [{
          "Event": "dispatch.transcript",
          "Transcript": {
            "Source": "Phone",
            "Disposition": "Accepted",
          },
          "JobID": "54e3425a-6bcf-4f1e-b720-19e72cd5",
          "AuthorizationNumber": "C7249827342",
          "MemberCallBackNumber": "999-999-9999",
          "CustomerContactMethod": "Cell",
          "Coverage": {
            "BenefitLimit": "Unlimited",
            "CashPaymentOnly": false,
            "CoPayAmount": "$50",
            "CoveredMiles": "Unlimited",
            "CoveredMilesText": "",
            "CoveredServices": "",
            "Description": "Customer covered on the service specified on this dispatch.",
            "DollarLimit": "",
            "DollarLimitText": "",
            "ExpectedTowMiles": 50,
            "FuelCoverageAmount": "",
            "NonCoveredServices": "",
            "OverMileageRate": "",
          },
          "ClientID": "USAC",
          "ContractorID": "AZ0123456",
          "DispatchID": "131264",
          "ResponseID": "2721093",
          "AccountInfo": {
            "Member": {
              "FirstName": "Emily",
              "LastName": "Martin",
            },
            "Customer": {
              "FirstName": "Abigail",
              "LastName": "Jackson",
            },
          },
          "Job": {
            "JobID": "79e33e31-8aea-445a-a933-aac6c42ae66e",
            "TimeStamp": "2017-02-21T23:49:46.8870000Z",
            "MaxETA": 90,
            "PrimaryTasks": [
              {
                "ETDRequired": false,
                "Task": "Jump Start",
              },
            ],
          },
          "Vehicle": {
            "Year": "2005",
            "Color": "Beige",
            "Make": "Chevrolet",
            "Model": "Corvette",
            "License": "LHY-4851",
            "LicenseState": "AR",
            "VIN": "WB4BP6737P4161973",
            "Odometer": 116382,
          },
          "Incident": {
            "Address1": "870 W Northwest Hwy",
            "City": "Parker",
            "State": "TX",
            "Zip": "75002",
            "CrossStreet1": "W Northwest Hwy/Cooper St.",
            "Latitude": 32.7778,
            "Longitude": -96.9567,
            "TimeZone": "CST",
            "CustomerWithVehicle": false,
          },
          "Destination": {
            "Address1": "553 Olde Forge Rd",
            "City": "Murphy",
            "State": "TX",
            "Zip": "75094",
            "Latitude": 32.9148,
            "Longitude": -96.6387,
            "Phone": "214 223-0112",
            "LocationInfo": "IMPORT CAR CENTER",
          },
        }].to_json.to_s
      end

      it "process the transcript event and make a new job with accepted status" do
        subject

        expect(response).to be_successful

        expect(dispatch_request).to be_present
        expect(dispatch_request.issc).to be_present

        expect(job).to be_present
        expect(job).to be_accepted
      end

      it_behaves_like 'set RequestContext'
    end

    context "with a successful connected message" do
      let(:envelope) do
        [{
          "Event": "connect",
          "Result": "Success",
        }].to_json.to_s
      end

      before do
        connection_manager._clear_status

        allow_any_instance_of(ISSC::DigitalDispatchClient).to receive(:connect)
          .and_return(mock_http_success)
      end

      it "connects" do
        expect(connection_manager).not_to be_connected

        subject

        expect(response).to be_successful

        expect(connection_manager).to be_connected
      end

      it_behaves_like 'set RequestContext'
    end

    context "when already connected" do
      let(:envelope) do
        [{
          "Event": "connect",
          "Result": "Failure",
          "Description": "Already connected.",
        }].to_json.to_s
      end

      before do
        connection_manager._clear_status

        allow_any_instance_of(ISSC::DigitalDispatchClient).to receive(:connect)
          .and_return(mock_http_success)
      end

      it "connects" do
        expect(connection_manager).not_to be_connected

        subject

        expect(response).to be_successful

        expect(connection_manager).to be_connected
      end

      it_behaves_like 'set RequestContext'
    end

    context "when body has body.muli" do
      let(:envelope) do
        "{\"body.multi\":[{\"ClientID\":\"USAC\",\"ContractorID\":\"CA1520543\",\"TaxID\":\"61-1803144\",\"Result\":\"Failure\",\"Description\":\"Provider ID not matched to Tax ID (105)\",\"Event\":\"preregistrations\"}]}"
      end

      it "is successful" do
        subject

        expect(response).to be_successful
      end

      it_behaves_like 'set RequestContext'
    end

    context "for unsupported events" do
      let(:envelope) do
        [{
          "Event": "dispatch.finagle",
          "Amount": 1000000,
          "Description": "Try to get a million out of them!",
        }].to_json.to_s
      end

      it "reponds okay but has an error in audit_isscs" do
        subject

        expect(response).to be_successful

        audit_issc = AuditIssc.order('created_at DESC').first
        expect(audit_issc.http_status_code).to eq(422)
      end

      it_behaves_like 'set RequestContext'
    end

    context "when the heartbeat is processed" do
      let(:envelope) do
        [{
          "Event": "heartbeat",
          "SequenceNumber": 2732,
          "UTC": "2014-05-30T14:07:21.0053Z",
        }].to_json.to_s
      end

      before do
        connection_manager._clear_status
        connection_manager._client.del('heartbeat_time')
        connection_manager._client.del('heartbeat_sequence')

        allow_any_instance_of(ISSC::DigitalDispatchClient).to receive(:connect)
          .and_return(mock_http_success)
      end

      it "sets the connection status to true" do
        expect(connection_manager).not_to be_connected

        subject
        expect(response).to be_successful

        expect(connection_manager._client.get('heartbeat_time')).to be_present
        expect(connection_manager._client.get('heartbeat_sequence').to_i).to eq(2732)
        expect(connection_manager).to be_connected
      end

      it_behaves_like 'set RequestContext'
    end

    context "when failure occurs" do
      let(:envelope) do
        [{
          "Event": "login",
          "Result": "Failure",
          "ClientID": "QUEST",
          "ContractorID": "23222",
          "Description": "Operation not allowed. Login in progress.",
        }].to_json.to_s
      end

      it "processes message and doesn't throw exception" do
        subject

        expect(response).to be_successful

        audit_issc = AuditIssc.order('created_at DESC').first
        expect(audit_issc.data).to include("Operation not allowed.")
      end

      it_behaves_like 'set RequestContext'
    end

    context "for ServiceRequestorUp and Down" do
      context 'when ServiceRequestorUp' do
        let(:envelope) do
          [{
            "Event": "servicerequester.up",
            "Name": "GEICO",
            "ClientID": "GEICO",
            "Status": "Up",
            "UpSince": "2016-11-07T00:45:29.0000000Z",
            "UpComment": "Normal operation.",
          }].to_json.to_s
        end

        it "generates an email for Up" do
          expect(SystemMailer).to receive(:send_mail)

          subject

          expect(response).to be_successful
        end

        it_behaves_like 'set RequestContext'
      end

      context 'when ServiceRequestorDown' do
        let(:envelope) do
          [{
            "Event": "servicerequester.down",
            "Name": "GEICO",
            "ClientID": "GEICO",
            "Status": "Down",
            "DownSince": "2016-11-07T00:57:15.0000000Z",
            "DownComment": "Temporarily down.",
          }].to_json.to_s
        end

        it "generates an email for Down" do
          expect(SystemMailer).to receive(:send_mail)

          subject

          expect(response).to be_successful
        end

        it_behaves_like 'set RequestContext'
      end
    end

    context "with contractor id and no location id" do
      let!(:issc) do
        create(:isscs, status: 'Registering', company: company, clientid: "USAC", contractorid: "CA300000")
      end

      let(:envelope) do
        [{
          "Event": "register",
          "Result": "Success",
          "ClientID": "USAC",
          "ContractorID": "CA300000",
          "Token": "3CCE2B67-977F4787-857DDC40-7ED01B13",
        }].to_json.to_s
      end

      it "registers the contractor id" do
        subject

        expect(response).to be_successful

        issc.reload

        expect(issc.issc_location_id).to eq(nil)
        expect(issc.status).to eq('registered')
      end

      it_behaves_like 'set RequestContext'
    end

    context "with preregistrations" do
      let(:envelope) do
        [
          {
            "preregistrations": [
              {
                "ContractorID": "TX0123456",
                "LocationID": "ab:5678",
                "LocationName": "North Brownstown",
                "Registered": true,
                "AddressLine1": "1432 River Rd.",
                "AddressLine2": "West Side",
                "City": "Akron",
                "State": "OH",
                "ZipCode": "51234",
                "POBox": "123",
                "UnitType": "Shop",
                "UnitNumber": "3A",
                "County": "Summit",
              },
              {
                "ContractorID": "TX0123456",
                "LocationID": "ab:1234",
                "LocationName": "South Brownstown",
                "Registered": false,
                "AddressLine1": "8391 Northridge Ave",
                "AddressLine2": "Section 32E",
                "City": "Boilingbrook",
                "State": "IL",
                "ZipCode": "61234",
                "POBox": "634",
                "UnitType": "Apt",
                "UnitNumber": "A834",
                "County": "Will",
              },
              {
                "ContractorID": "TX0123456",
                "LocationID": "ef:1234",
                "LocationName": "Location 1",
                "Registered": false,
                "AddressLine1": "1400 Mission St",
                "AddressLine2": "",
                "City": "Somewhere",
                "State": "CA",
                "ZipCode": "94102",
                "County": "San Francisco",
              },
              {
                "ContractorID": "TX0123456",
                "LocationID": "ef:5678",
                "LocationName": "Location 2",
                "Registered": false,
                "AddressLine1": "1500 Mission St",
                "AddressLine2": "",
                "City": "Somewhere",
                "State": "CA",
                "ZipCode": "94103",
                "County": "San Francisco",
              },
              {
                "ContractorID": "TX0123456",
                "LocationID": "ef:9012",
                "LocationName": "Location 3",
                "Registered": false,
                "AddressLine1": "1600 Mission St",
                "AddressLine2": "",
                "City": "Somewhere",
                "State": "CA",
                "ZipCode": "94104-1234",
                "County": "San Francisco",
              },
            ],
            "ClientID": "USAC",
            "ContractorID": "TX0123456",
            "TaxID": "00-1234567",
            "Result": "Success",
            "Event": "preregistrations",
          },
        ].to_json.to_s
      end

      let!(:company) { create(:rescue_company, tax_id: "00-1234567") }

      let(:prereg_site_location_1) do
        create(:location,
               lat: 37.7926185,
               lng: -122.4004241,
               address: "1400 Mission St, Somewhere, CA",
               zip: "94102")
      end

      let!(:prereg_site_location_2) do
        create(:location,
               lat: 37.7926185,
               lng: -122.4004241,
               address: "1500 Mission St, Somewhere, CA",
               zip: "94103-1234")
      end

      let!(:prereg_site_location_3) do
        create(:location,
               lat: 37.7926185,
               lng: -122.4004241,
               address: "1500 Mission St, Somewhere, CA, 94104",
               zip: "94104")
      end

      let!(:prereg_site_1) do
        create(:site,
               name: "Preregistration Site",
               location: prereg_site_location_1,
               company: company,
               always_open: true,
               dispatchable: true)
      end

      let!(:prereg_site_2) do
        create(:site,
               name: "Preregistration Site 2",
               location: prereg_site_location_2,
               company: company,
               open_time: "08:00:00",
               close_time: "17:00:00",
               dispatchable: true)
      end

      let!(:prereg_site_3) do
        create(:site,
               name: "Preregistration Site 3",
               location: prereg_site_location_3,
               company: company,
               open_time: "08:00:00",
               close_time: "09:00:00",
               dispatchable: true)
      end

      it "behaves appropriately" do
        expect { subject }.to change(Issc, :count).by(5)

        expect(response).to be_successful

        isscs = Issc.where(clientid: "USAC", contractorid: "TX0123456")
        expect(isscs.length).to eq(5)

        issc = Issc.where(clientid: "USAC", contractorid: "TX0123456", site_id: prereg_site_1.id).first
        expect(issc).not_to eq(nil)
        expect(issc.site.location_id).to eq(prereg_site_1.location_id)

        issc = Issc.where(clientid: "USAC", contractorid: "TX0123456", site_id: prereg_site_2.id).first
        expect(issc).not_to eq(nil)
        expect(issc.site.location_id).to eq(prereg_site_2.location_id)

        issc = Issc.where(clientid: "USAC", contractorid: "TX0123456", site_id: prereg_site_3.id).first
        expect(issc).not_to eq(nil)
        expect(issc.site.location.zip).to eq("94104")

        issc_locations = IsscLocation.where(locationid: ["ab:1234", "ab:5678"]).all
        expect(issc_locations.length).to eq(2)
        issc_locations.each do |loc|
          expect(loc.site_id).to eq(nil)
          expect(loc.location_id).to eq(loc.location_id)
        end

        issc_locations = IsscLocation.where(locationid: ["ef:1234", "ef:5678", "ef:9012"]).all
        expect(issc_locations.length).to eq(3)
        issc_locations.each do |loc|
          expect(loc.site_id).not_to eq(nil)
          expect(loc.location_id).not_to eq(nil)
        end
      end

      it_behaves_like 'set RequestContext'
    end
  end
end
