# frozen_string_literal: true

require "rails_helper"

describe API::V1::JobStatusReasonTypesController, type: :controller do
  describe "GET index" do
    subject(:get_index) do
      get :index, as: :json
    end

    let!(:job_status_reason_type) { create(:job_status_reason_type, :cancel_prior_job_delayed) }

    context 'when user is rescue' do
      stub_user_login :rescue

      it "returns head 200" do
        expect(get_index.status).to be 200
      end

      it 'returns serialized JobStatusReasonTypes' do
        get_index

        job_status_reason_type = json.first

        expect(job_status_reason_type).to be_present
      end
    end
  end
end
