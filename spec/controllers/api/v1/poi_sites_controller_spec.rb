# frozen_string_literal: true

require "rails_helper"

describe API::V1::PoiSitesController, type: :controller do
  # render_views

  describe "POST create" do
    subject(:create_place) do
      post :create, params: { poiSite: request_data, format: :json }
    end

    let(:request_data) do
      {
        name: "Za new place",
        location: {
          lat: 40.783060,
          lng: -73.971248,
          place_id: "ChIJYeZuBI9YwokRjMDs_IEyCwo",
          address: "Manhattan, New York",
          exact: true,
        },
        manager: {
          full_name: "Jane Doe",
        },
        # location_type_id: "234",
        phone: "+14154566288",
      }
    end

    stub_user_login(:rescue, :admin)

    it "creates a new PoiSite" do
      expect { create_place }.to change(PoiSite, :count).by(1)
    end

    it "renders a JSON serialized PoiSite" do
      create_place
      expect(response.body).to eql(PoiSiteSerializer.new(PoiSite.last).to_json)
    end

    it "creates a PoiSite associated to the logged in user's company" do
      create_place
      expect(PoiSite.last.company_id).to eq(user.company_id)
    end

    it "creates a new Location" do
      expect { create_place }.to change(Location, :count).by(1)
    end

    it "creates a new Location with given attributes" do
      create_place
      expect(Location.last).to have_attributes(
        site_id: PoiSite.last.id, lat: 40.783060, lng: -73.971248, exact: true,
        place_id: "ChIJYeZuBI9YwokRjMDs_IEyCwo", address: "Manhattan, New York"
      )
    end

    it "saves manager's :full_name as :first_name and :last_name" do
      create_place
      expect(User.last).to have_attributes(first_name: "Jane", last_name: "Doe")
    end

    context "when not admin" do
      stub_user_login

      it "returns an authorization error" do
        create_place
        expect(response.status).to be(403)
      end
    end

    context 'when duplicated name is given' do
      it "does not allow the place to be updated" do
        create :poi_site, { name: 'Za new place', company: user.company }

        create_place

        expect(response.status).to be(422)
      end
    end
  end

  describe "PATCH update" do
    let(:company_tesla) { create :fleet_company, :tesla }
    let(:place_tesla) { create :poi_site, company: company_tesla }

    stub_user_login(:tesla, :admin)

    it "updates the given attribute accordingly" do
      post :update, params: { id: place_tesla.id, poiSite: { name: "Change" }, format: :json }

      expect(response.status).to be(200)
    end

    context 'when duplicated name is given' do
      let(:another_place_tesla) do
        create :poi_site, { name: 'Different name', company: company_tesla }
      end

      it "does not allow the place to be updated" do
        create :poi_site, { name: 'Do not repeat me pls', company: company_tesla }

        post :update, params: { id: another_place_tesla.id, poiSite: { name: "Do not repeat me pls" }, format: :json }

        expect(response.status).to be(422)
      end
    end
  end
end
