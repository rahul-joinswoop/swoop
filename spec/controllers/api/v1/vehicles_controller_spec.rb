# frozen_string_literal: true

require "rails_helper"

describe API::V1::VehiclesController, type: :controller do
  stub_user_login(:root)

  describe "GET index" do
    subject(:index) do
      get :index, format: :json
    end

    it "returns head 200" do
      expect(index.status).to be 200
    end

    # there is problem with `render` because its sending empty string
  end

  describe "GET makes" do
    render_views(true)
    subject(:makes) do
      get :makes, format: :json
    end

    let!(:make) { create :vehicle_make }
    let!(:model) { create :vehicle_model, vehicle_make: make }

    it "returns 200 with the correct body" do
      expect(makes.status).to be 200
      expect(JSON.parse(makes.body)).to deep_include([
        {
          id: make.id,
          name: make.name,
          models: [{
            id: model.id,
            name: model.name,
            years: model.years,
          }],
        }.deep_stringify_keys,
      ])
    end
  end
end
