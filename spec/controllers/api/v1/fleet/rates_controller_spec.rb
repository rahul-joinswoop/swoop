# frozen_string_literal: true

require "rails_helper"

describe API::V1::Fleet::RatesController, type: :controller do
  let!(:account) { create :account, company: rescue_company, client_company: fleet_company }
  let(:fleet_company) { user.company }
  let(:rescue_company) { create :rescue_company }
  let(:rescue_provider) do
    create :rescue_provider, provider: rescue_company, company: fleet_company, site: site
  end
  let(:site) { create(:site, company: rescue_company) }

  stub_user_login(:tesla)

  describe '#post_defaults' do
    render_views

    let(:rate_default_1) { create(:hourly_p2p, company: rescue_company) }

    let(:rate_default_2) { create(:miles_p2p_rate, company: rescue_company) }

    let(:rate_account_1) { create(:hourly_p2p, :rate_account_level, company: rescue_company, account: account) }

    let(:rate_account_2) do
      create(:miles_p2p_rate, :rate_account_level, company: rescue_company, account: account)
    end

    let(:rate_account_with_service_code_id_1) do
      create(
        :miles_p2p_rate,
        :rate_account_with_service_code_level,
        account: account,
        service_code: service_code,
      )
    end

    let(:rate_account_with_service_code_id_2) do
      create(
        :miles_p2p_rate,
        :rate_account_with_service_code_level,
        account: account,
        service_code: service_code,
      )
    end

    let(:site) { create(:site, company: rescue_company) }
    let(:vendor_id) { nil }
    let(:service_code) { create(:service_code) }

    before :each do
      # create rates
      rate_default_1
      rate_default_2

      rate_account_1
      rate_account_2

      rate_account_with_service_code_id_1
      rate_account_with_service_code_id_2

      # post request
      post_defaults
    end

    context 'when account_id only is passed' do
      subject(:post_defaults) do
        post(
          :create_based_on_defaults,
          params: {
            rate: {
              provider_id: rescue_provider.id,
            },
          },
          as: :json
        )
      end

      it 'creates a new rate based on account default' do
        account_id_from_json = json.map { |rate| rate["account_id"] }.uniq.compact
        site_id_from_json = json.map { |rate| rate["site_id"] }.uniq.compact
        vendor_id_from_json = json.map { |rate| rate["vendor_id"] }.uniq.compact
        service_code_id_from_json = json.map do |rate|
          rate["service_code_id"]
        end.uniq.compact

        expect(account_id_from_json.size).to eq 1
        expect(account_id_from_json.first).to eq account.id

        expect(site_id_from_json).to be_blank
        expect(vendor_id_from_json).to be_blank
        expect(service_code_id_from_json).to be_blank
      end
    end

    context 'when account_id and service_code_id are passed' do
      subject(:post_defaults) do
        post(
          :create_based_on_defaults,
          params: {
            rate: {
              provider_id: rescue_provider.id,
              service_code_id: service_code_id,
            },
          },
          as: :json
        )
      end

      let(:account_id) { account.id }
      let(:service_code_id) { service_code.id }

      it 'creates a new rate based on service_code defaults' do
        account_id_from_json = json.map { |rate| rate["account_id"] }.uniq.compact
        site_id_from_json = json.map { |rate| rate["site_id"] }.uniq.compact
        vendor_id_from_json = json.map { |rate| rate["vendor_id"] }.uniq.compact
        service_code_id_from_json = json.map do |rate|
          rate["service_code_id"]
        end.uniq.compact

        expect(account_id_from_json.size).to eq 1
        expect(account_id_from_json.first).to eq account.id

        expect(site_id_from_json).to be_blank
        expect(vendor_id_from_json).to be_blank

        expect(service_code_id_from_json.size).to eq 1
        expect(service_code_id_from_json.first).to eq service_code_id
      end
    end
  end

  describe 'POST create_service' do
    subject(:post_create_service) do
      post(
        :create_service,
        params: {
          rescue_provider_id: rescue_provider.id,
          service_code_id: service_code.id,
        },
        as: :json
      )
    end

    let(:service_code) { create(:service_code) }

    let!(:rates) do
      create_list(
        :miles_p2p_rate,
        2,
        :rate_account_with_service_code_level,
        account: account,
        company: rescue_company,
        service_code: nil,
      )
    end

    before do
      allow(RateService::CopyRates).to receive(:new)

      allow(RescueProvider).to receive(:find_by!).with(
        company: user.company, id: rescue_provider.id.to_s
      ).and_return(rescue_provider)

      allow(rescue_provider).to receive(:provider).and_return(rescue_company)

      allow(rescue_company).to receive(:rates_by_account).with(
        account_id: account.id,
        service_code_id: nil
      ).and_return(rates)

      post_create_service
    end

    it 'calls instantiates RateService::CopyRates with expected params' do
      expect(RateService::CopyRates).to have_received(:new).with(
        rates: rates,
        company: user.company,
        new_attributes: { account_id: account.id, service_code_id: service_code.id.to_s }
      )
    end
  end
end
