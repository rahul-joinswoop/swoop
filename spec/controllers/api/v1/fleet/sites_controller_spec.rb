# frozen_string_literal: true

require "rails_helper"

describe API::V1::Fleet::SitesController, type: :controller do
  stub_user_login(:tesla)

  describe "POST search" do
    subject(:search) do
      post :search, params: { phone: "+14155552222", format: :json }
    end

    let(:site_one) { create :partner_site, phone: "+14155552222" }
    let(:site_two) { create :partner_site, phone: "+14155552222" }

    it "returns an empty JSON array when no result is found" do
      search
      expect(JSON.parse(response.body)).to eql([])
    end

    it "returns 200" do
      search
      expect(response.status).to eql(200)
    end

    context "when :phone parameter passed" do
      describe "rendered results" do
        render_views

        it "returns all phone numbers with :phone parameter" do
          site_one
          site_two

          search

          site_ids = JSON.parse(response.body).map { |site| site["id"] }
          expect(site_ids).to include(site_one.id, site_two.id)
        end
      end

      it "returns 400 when :phone is not 12 chars long" do
        post :search, params: { phone: "123" }
        expect(response.status).to eql(400)
      end
    end
  end

  describe 'POST create' do
    def created_site
      Site.find_by_name('Awesome Site')
    end

    subject(:create_site) do
      post :create, params: { site: request_data, format: :json }
    end

    let(:request_data) do
      {
        name: 'Awesome Site',
        location: {
          lat: -23.2000805,
          lng: -47.29353800000001,
          place_id: "ChIJ7frs2GVSz5QRp5SW3cXtdOc",
          address: "Salto - SP, Brasil",
          exact: true,
          location_type_id: location_type.id,
        },
      }
    end

    let(:location_type) { create(:location_type) }

    it 'creates a new Site' do
      expect { create_site }.to change(Site, :count).by(1)
    end

    it 'contains the expected location type' do
      create_site

      expect(created_site.location.location_type.id).to eq location_type.id
    end
  end

  # used Tesla type to match the Tesla user stubbed
  describe 'PATCH update' do
    def updated_site
      FleetSite.find_by_name('Fleet Site')
    end

    subject(:update_site) do
      patch :update, params: { id: fleet_site.id, site: request_data, format: :json }
    end

    let(:request_data) do
      {
        location: {
          location_type_id: location_type.id,
        },
      }
    end

    let(:location_type) { create(:location_type, name: 'Another Location') }
    let(:location) { create(:location) }
    let(:fleet_site) { create(:site, name: 'Fleet Site', type: FleetSite) }

    it 'contains the expected location type' do
      update_site

      expect(updated_site.location.location_type.name).to eq 'Another Location'
    end

    context 'when location type is null' do
      let(:request_data) do
        {
          location: {
            location_type_id: nil,
          },
        }
      end

      it 'sets site.location.location_type as null' do
        update_site

        expect(updated_site.location.location_type).to eq nil
      end
    end
  end
end
