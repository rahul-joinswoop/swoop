# frozen_string_literal: true

require "rails_helper"

describe API::V1::Fleet::JobsController, type: :controller do
  render_views

  describe "GET index" do
    subject(:index) do
      get :index, as: :json
    end

    let!(:fleet_demo) { create(:fleet_company, :fleet_demo) }

    let!(:job) do
      create(:fleet_motor_club_job, {
        fleet_company: fleet_demo,
        status: JobStatus::ID_MAP[JobStatus::PENDING],
        get_location_sms: true,
      })
    end

    stub_user_login(:fleet_demo)

    it "returns head 200" do
      expect(index.status).to be 200
      Rails.logger.debug "body: #{json}"
      expect(json["total"]).to eql(1)
    end

    context 'one fleet_demo, one mini job' do
      let!(:user) do
        create(:user, :fleet_demo)
      end

      let!(:app) do
        Doorkeeper::Application.create(name: 'swoop')
      end

      let!(:token) do
        create :doorkeeper_access_token, {
          resource_owner_id: user.id,
          scopes: user.roles.map(&:name),
          application: app,
        }
      end

      let!(:mini) { create(:fleet_company, :mini) }

      let!(:mini_job) do
        create(:fleet_motor_club_job, {
          fleet_company: mini,
          status: JobStatus::ID_MAP[JobStatus::PENDING],
          get_location_sms: true,
        })
      end

      before(:each) do
        authenticate_request(controller.request, token)
      end

      it "returns one job" do
        expect(index.status).to be 200
        expect(json["total"]).to eql(1)
      end
    end
  end

  context 'POST create' do
    subject(:post_create_job) do
      post :create, params: params, as: :json
    end

    let(:params) do
      {
        job: {
          priority_response: false,
          send_sms_to_pickup: false,
          review_sms: false,
          partner_sms_track_driver: false,
          text_eta_updates: false,
          fleet_dispatcher_id: user.id,
          customer: {
            full_name: 'Quinn Baetz',
            phone: '+12315555555',
            member_number: "asdfasdf",
          },
          make: 'Tesla',
          model: 'Model S',
          year: '2016',
          color: 'Red',
          vin: '1HGCM82633A004352',
          odometer: '1337',
          service_location: {
            lat: 37.7800769528017,
            lng: -122.438335418701,
            place_id: 'ChIJcc2VKLGAhYARFYsEkYhEE1U',
            address: '1100 Divisadero St, San Francisco, CA 94115, USA',
            exact: true,
          },
          pickup_contact: {
            full_name: 'Pickup Contact Name',
            phone: '+12315555555',
          },
          will_store: false,
          service_code_id: service_code.id,
          question_results: [],
          customer_type_id: customer_type.id,
        },
        save_as_draft: false,
      }
    end

    let(:service_code) { ServiceCode.where(name: ServiceCode::BATTERY_JUMP).first_or_create }
    let(:customer_type) { CustomerType.where(name: CustomerType::COVERED).first_or_create }
    let(:parsed_response) { JSON.parse(response.body) }
    let(:created_job) { Job.find(parsed_response['id']) }
    let(:fleet_company) { controller.api_company }
    let(:fleet_dispatcher) { controller.api_user }

    shared_examples 'created expected audit_job_status' do
      it 'works' do
        post_create_job

        audit_job_statuses = created_job.audit_job_statuses

        expect(audit_job_statuses.count).to eq expected_audit_job_statuses.length
        expect(audit_job_statuses.map { |ajs| ajs.job_status.name }).to eq(expected_audit_job_statuses)
      end
    end

    shared_examples 'creates a new Job with given attributes' do
      it 'works' do
        post_create_job

        expect(response.status).to be(201)
        expect(created_job.instance_of?(expected_job_class)).to be_truthy
        expect(created_job.driver.vehicle.make).to eq params[:job][:make]
        expect(created_job.driver.vehicle.model).to eq params[:job][:model]
        expect(created_job.driver.vehicle.year).to eq params[:job][:year].to_i
        expect(created_job.driver.vehicle.color).to eq params[:job][:color]
        expect(created_job.driver.vehicle.vin).to eq params[:job][:vin]
        expect(created_job.odometer).to eq params[:job][:odometer].to_f
        expect(created_job.driver.user).not_to be_nil
        expect(created_job.stranded_vehicle).not_to be_nil

        expect(created_job.service_location.lat).to eq params[:job][:service_location][:lat]
        expect(created_job.service_location.lng).to eq params[:job][:service_location][:lng]
        expect(created_job.service_location.place_id).to eq params[:job][:service_location][:place_id]
        expect(created_job.service_location.address).to eq params[:job][:service_location][:address]
        expect(created_job.service_location.exact).to be_truthy

        expect(created_job.service_code_id).to eq service_code.id
        expect(created_job.customer_type_id).to eq customer_type.id

        expect(created_job.pickup_contact.full_name).to eq params[:job][:pickup_contact][:full_name]
        expect(created_job.pickup_contact.phone).to eq params[:job][:pickup_contact][:phone]

        expect(created_job.fleet_company_id).to eq fleet_company.id
        expect(created_job.originating_company_id).to eq controller.api_company.id
        expect(created_job.user_id).to eq controller.api_user.id
        expect(created_job.fleet_dispatcher_id).to eq fleet_dispatcher.id
        expect(created_job.driver.user.member_number).to eq params[:job][:customer][:member_number]
        expect(parsed_response['customer']['member_number']).to eq params[:job][:customer][:member_number]
      end
    end

    shared_examples 'creates a new Job with given attributes as Swoop' do
      let(:swoop_user) { create :user, :root }
      # here these are set to the user we created in our other context - this is
      # not a swoop user but either fleetinhouse or fleetmanaged
      let(:fleet_company) { user.company }
      let(:fleet_dispatcher) { user }

      let(:params) do
        super().tap { |p| p[:job][:acts_as_company_id] = user.company.id }
      end
      let!(:token) { create :doorkeeper_access_token, resource_owner_id: swoop_user.id, scopes: swoop_user.roles.map(&:name) }
      it_behaves_like 'creates a new Job with given attributes'
    end

    context 'when its a FleetInHouse company' do
      let(:user) { User.where(company: Company.tesla).first }
      let(:expected_job_class) { FleetInHouseJob }
      let(:expected_audit_job_statuses) { ['Initial', 'Created', 'Pending'] }

      stub_user_login(:tesla)

      it_behaves_like 'creates a new Job with given attributes'
      it_behaves_like 'created expected audit_job_status'
      it_behaves_like 'creates a new Job with given attributes as Swoop'

      context 'when saving job as draft' do
        subject(:post_create_job) do
          post :create, params:
              {
                job: {
                  customer_type_id: nil,
                  fleet_site_id: nil,
                  make: 'Tesla',
                  send_sms_to_pickup: true,
                  will_store: false,
                }, save_as_draft: true,
              },
                        as: :json
        end

        let(:expected_audit_job_statuses) do
          ['Initial', 'Draft']
        end

        it 'creates a new Job with minimum attributes' do
          post_create_job

          expect(created_job.instance_of?(FleetInHouseJob)).to be_truthy

          expect(created_job.fleet_company_id).to eq controller.api_company.id
          expect(created_job.originating_company_id).to eq controller.api_company.id
          expect(created_job.user_id).to eq controller.api_user.id
          expect(created_job.fleet_dispatcher_id).to eq controller.api_user.id
          expect(created_job.draft?).to be_truthy
          expect(created_job.send_sms_to_pickup).to be_truthy
          expect(created_job.will_store).to be_falsey
          expect(created_job.driver.vehicle.make).to eq 'Tesla'
          expect(created_job.driver.user).not_to be_nil
          expect(created_job.stranded_vehicle).not_to be_nil
        end

        it_behaves_like 'created expected audit_job_status'
      end

      context 'when saving job as predraft' do
        subject(:post_create_job) do
          post :create, params:
              {
                job: {
                  customer_type_id: nil,
                  fleet_site_id: nil,
                  make: 'Tesla',
                  send_sms_to_pickup: true,
                  will_store: false,
                }, save_as_predraft: true,
              },
                        as: :json
        end

        let(:expected_audit_job_statuses) do
          ['Initial', 'Predraft']
        end

        it 'creates a new Job with minimum attributes' do
          post_create_job

          expect(created_job.instance_of?(FleetInHouseJob)).to be_truthy

          expect(created_job.fleet_company_id).to eq controller.api_company.id
          expect(created_job.originating_company_id).to eq controller.api_company.id
          expect(created_job.user_id).to eq controller.api_user.id
          expect(created_job.fleet_dispatcher_id).to eq controller.api_user.id
          expect(created_job.draft?).to be_falsey
          expect(created_job.predraft?).to be_truthy
          expect(created_job.send_sms_to_pickup).to be_truthy
          expect(created_job.will_store).to be_falsey
          expect(created_job.driver.vehicle.make).to eq 'Tesla'
          expect(created_job.driver.user).not_to be_nil
          expect(created_job.stranded_vehicle).not_to be_nil
        end

        it_behaves_like 'created expected audit_job_status'
      end
    end

    context 'when it\'s a FleetManaged company' do
      let(:user) { User.where(company: Company.find_by_name('Fleet Demo')).first }
      let(:expected_job_class) { FleetManagedJob }
      let(:expected_audit_job_statuses) { ['Initial', 'Created', 'Pending'] }

      stub_user_login(:fleet_demo)

      it_behaves_like 'creates a new Job with given attributes'
      it_behaves_like 'created expected audit_job_status'
      it_behaves_like 'creates a new Job with given attributes as Swoop'

      context 'when saving job as draft' do
        subject(:post_create_job) do
          post :create, params: {
            job: {
              partner_sms_track_driver: true,
              review_sms: true,
              send_sms_to_pickup: false,
              text_eta_updates: true,
              will_store: false,
            },
            save_as_draft: true,
          },
                        as: :json
        end

        let(:expected_audit_job_statuses) { ['Initial', 'Draft'] }

        it_behaves_like 'created expected audit_job_status'

        it 'creates a new Job with minimum attributes' do
          post_create_job

          expect(created_job.instance_of?(FleetManagedJob)).to be_truthy

          expect(created_job.fleet_company_id).to eq controller.api_company.id
          expect(created_job.originating_company_id).to eq controller.api_company.id
          expect(created_job.user_id).to eq controller.api_user.id
          expect(created_job.fleet_dispatcher_id).to eq controller.api_user.id
          expect(created_job.draft?).to be_truthy
          expect(created_job.partner_sms_track_driver).to be_truthy
          expect(created_job.review_sms).to be_truthy
          expect(created_job.send_sms_to_pickup).to be_falsey
          expect(created_job.text_eta_updates).to be_truthy
          expect(created_job.will_store).to be_falsey
          expect(created_job.driver.vehicle).not_to be_nil
          expect(created_job.driver.user).not_to be_nil
          expect(created_job.stranded_vehicle).not_to be_nil
        end
      end

      context 'when saving job as predraft' do
        subject(:post_create_job) do
          post :create, params: {
            job: {
              partner_sms_track_driver: true,
              review_sms: true,
              send_sms_to_pickup: false,
              text_eta_updates: true,
              will_store: false,
            },
            save_as_predraft: true,
          },
                        as: :json
        end

        let(:expected_audit_job_statuses) { ['Initial', 'Predraft'] }

        it_behaves_like 'created expected audit_job_status'

        it 'creates a new Job with minimum attributes' do
          post_create_job

          expect(created_job.instance_of?(FleetManagedJob)).to be_truthy

          expect(created_job.fleet_company_id).to eq controller.api_company.id
          expect(created_job.originating_company_id).to eq controller.api_company.id
          expect(created_job.user_id).to eq controller.api_user.id
          expect(created_job.fleet_dispatcher_id).to eq controller.api_user.id
          expect(created_job.draft?).to be_falsey
          expect(created_job.predraft?).to be_truthy
          expect(created_job.partner_sms_track_driver).to be_truthy
          expect(created_job.review_sms).to be_truthy
          expect(created_job.send_sms_to_pickup).to be_falsey
          expect(created_job.text_eta_updates).to be_truthy
          expect(created_job.will_store).to be_falsey
          expect(created_job.driver.vehicle).not_to be_nil
          expect(created_job.driver.user).not_to be_nil
          expect(created_job.stranded_vehicle).not_to be_nil
        end
      end
    end
  end

  describe 'PATCH update' do
    subject(:patch_update) do
      put :update,
          params: {
            id: job.id, job: {
              id: job.id,
              customer: {
                id: job.customer.id,
                full_name: 'Customer Name',
              },
            },
          },
          as: :json
    end

    def patch_update_job
      patch_update

      job.reload
    end

    before do
      create(:fleet_company, :tesla)
    end

    describe 'when it\'s a FleetInHouse company' do
      stub_user_login(:tesla)

      let(:job) do
        create(:fleet_in_house_job,
               fleet_company: Company.tesla,
               status: Job::PENDING,
               get_location_sms: true)
      end

      it 'triggers HandleToa#call' do
        allow_any_instance_of(HandleToa).to receive(:call)

        expect_any_instance_of(HandleToa).to receive(:call)

        patch_update_job
      end

      it 'calls update_history' do
        allow_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

        expect_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

        patch_update_job
      end

      it 'sets jobs.last_touched_by' do
        expect(job.last_touched_by).to be_nil

        patch_update_job

        expect(job.last_touched_by).to eq(controller.api_user)
      end

      it 'calls job.fixup_locations' do
        allow_any_instance_of(FleetInHouseJob).to receive(:fixup_locations)
        expect_any_instance_of(FleetInHouseJob).to receive(:fixup_locations)

        patch_update_job
      end

      it 'calls controller.set_platform' do
        allow_any_instance_of(API::Jobs::Update).to receive(:set_platform)
        expect_any_instance_of(API::Jobs::Update).to receive(:set_platform)

        patch_update_job
      end

      it 'calls job#send_get_location' do
        expect(job.get_location_sms).to be_truthy

        allow_any_instance_of(FleetInHouseJob).to receive(:send_get_location)
        expect_any_instance_of(FleetInHouseJob).to receive(:send_get_location)

        patch_update_job
      end

      it 'updates attributes accordingly' do
        expect(job.driver.user.name).not_to eq 'Customer Name'

        patch_update_job

        expect(job.driver.user.name).to eq 'Customer Name'
      end

      it 'doesnt call job#send_swoop_alert' do
        expect(job).not_to receive(:send_swoop_alert)

        patch_update_job
      end

      it 'doesnt call job#slack_after_create' do
        expect(job).not_to receive(:slack_after_create)

        patch_update_job
      end

      context 'when it\'s a Draft Job' do
        subject(:patch_update) do
          put :update, params: {
            id: job.id, job: {
              id: job.id,
              customer: {
                id: job.customer.id,
                full_name: 'Customer Name',
              },
            },
            save_as_draft: true,
          },
                       as: :json
        end

        let(:job) do
          create(:fleet_in_house_job,
                 fleet_company: Company.tesla,
                 status: Job::DRAFT,
                 get_location_sms: true)
        end

        it 'sets jobs.last_touched_by' do
          expect(job.last_touched_by).to be_nil

          patch_update_job

          expect(job.last_touched_by).to eq(controller.api_user)
        end

        it 'calls job.fixup_locations' do
          allow_any_instance_of(FleetInHouseJob).to receive(:fixup_locations)
          expect_any_instance_of(FleetInHouseJob).to receive(:fixup_locations)

          patch_update_job
        end

        it 'keeps the status as Draft' do
          patch_update_job

          expect(job.status).to eq Job::STATUS_DRAFT
        end

        it 'updates attributes accordingly' do
          expect(job.driver.user.name).not_to eq 'Customer Name'

          patch_update_job

          expect(job.driver.user.name).to eq 'Customer Name'
        end

        it 'calls update_history' do
          allow_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

          expect_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

          patch_update_job
        end

        it 'dont call other non-draft methods' do
          expect_any_instance_of(HandleToa).not_to receive(:call)
          expect_any_instance_of(FleetInHouseJob).not_to receive(:send_get_location)

          patch_update_job
        end

        context 'when save_as_draft param is not passed on the request' do
          subject(:patch_update) do
            put :update,
                params: {
                  id: job.id, job: {
                    id: job.id,
                    customer: {
                      id: job.customer.id,
                      full_name: 'Customer Name',
                    },
                    scheduled_for: scheduled_for,
                  },
                },
                as: :json
          end

          let(:scheduled_for) { nil }

          it 'calls job#set_service_location_user' do
            allow_any_instance_of(FleetInHouseJob).to receive(:set_service_location_user)
            expect_any_instance_of(FleetInHouseJob).to receive(:set_service_location_user)

            patch_update_job
          end

          it 'doesnt call job#scheduled_system_alert_create' do
            expect_any_instance_of(FleetInHouseJob).not_to receive(:scheduled_system_alert_create)

            patch_update_job
          end

          it 'moves the job to pending status' do
            patch_update_job

            expect(job.status).to eq Job::STATUS_PENDING
          end

          it 'calls job#send_swoop_alert' do
            allow_any_instance_of(FleetInHouseJob).to receive(:send_swoop_alert)
            expect_any_instance_of(FleetInHouseJob).to receive(:send_swoop_alert)

            patch_update_job
          end

          it 'calls job#slack_after_create' do
            allow_any_instance_of(FleetInHouseJob).to receive(:slack_after_create)
            expect_any_instance_of(FleetInHouseJob).to receive(:slack_after_create)

            patch_update_job
          end

          it 'enqueues EmailStatus::Create' do
            allow(JobStatusEmailSender).to receive(:new)
            expect(JobStatusEmailSender).to receive(:new).with(job.id, JobStatusEmail::Created.name)

            patch_update_job
          end

          context 'when job is scheduled' do
            let(:scheduled_for) { '2018-02-06T18:47:00.000Z' }

            it 'enqueues EmailStatus::Scheduled' do
              allow_any_instance_of(Job).to receive(:send_confirmation_sms)

              allow(JobStatusEmailSender).to receive(:new)
              expect(JobStatusEmailSender).to receive(:new).with(job.id, JobStatusEmail::Scheduled.name)

              patch_update_job
            end
          end
        end
      end

      context 'ETA update' do
        subject(:patch_update) do
          put :update,
              params: {
                id: job.id, job: {
                  id: job.id,
                  toa: {
                    latest: datetime,
                  },
                  customer: {
                    id: job.customer.id,
                    full_name: 'Customer Name',
                  },
                },
              },
              as: :json
        end

        before do
          create(:job_status_email_preference, :eta_extended, user_id: job.fleet_company.users.first.id)
          create(:time_of_arrival, job_id: job.id,  eba_type: 5)
        end

        context 'when new ETA delta >= 15 mins' do
          let(:datetime) { job.current_eta_time + 16.minutes }

          it 'enqueues EmailStatus::ETA_EXTENDED' do
            expect(Delayed::Job).to receive(:enqueue).with(an_instance_of(JobStatusEmail::EtaExtended))
            patch_update_job
          end
        end

        context 'when new ETA delta < 15 mins' do
          let(:datetime) { job.current_eta_time + 14.minutes }

          it 'doesn\'t enqueue EmailStatus::ETA_EXTENDED' do
            expect(Delayed::Job).not_to receive(:enqueue).with(an_instance_of(JobStatusEmail::EtaExtended))
            patch_update_job
          end
        end
      end
    end

    describe 'when it\'s a FleetManaged company' do
      stub_user_login(:fleet_demo)

      let(:job) do
        create(:fleet_managed_job,
               fleet_company: Company.find_by_name!('Fleet Demo'),
               status: Job::PENDING,
               get_location_sms: true,
               pcc_coverage_id: pcc_coverage&.id)
      end

      let(:pcc_coverage) { nil }

      it 'triggers HandleToa#call' do
        allow_any_instance_of(HandleToa).to receive(:call)

        expect_any_instance_of(HandleToa).to receive(:call)

        patch_update_job
      end

      it 'calls update_history' do
        allow_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

        expect_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

        patch_update_job
      end

      # base controller

      it 'sets jobs.last_touched_by' do
        expect(job.last_touched_by).to be_nil

        patch_update_job

        expect(job.last_touched_by).to eq(controller.api_user)
      end

      it 'calls job.fixup_locations_job' do
        allow_any_instance_of(FleetManagedJob).to receive(:fixup_locations)
        expect_any_instance_of(FleetManagedJob).to receive(:fixup_locations)

        patch_update_job
      end

      it 'calls controller.set_platform' do
        allow_any_instance_of(API::Jobs::Update).to receive(:set_platform)
        expect_any_instance_of(API::Jobs::Update).to receive(:set_platform)

        patch_update_job
      end

      it 'calls job#send_get_location' do
        expect(job.get_location_sms).to be_truthy

        allow_any_instance_of(FleetManagedJob).to receive(:send_get_location)
        expect_any_instance_of(FleetManagedJob).to receive(:send_get_location)

        patch_update_job
      end

      it 'updates attributes accordingly' do
        expect(job.driver.user.name).not_to eq 'Customer Name'

        patch_update_job

        expect(job.driver.user.name).to eq 'Customer Name'
      end

      it 'doesnt call job#send_swoop_alert' do
        expect(job).not_to receive(:send_swoop_alert)

        patch_update_job
      end

      it 'doesnt call job#slack_after_create' do
        expect(job).not_to receive(:slack_after_create)

        patch_update_job
      end

      it 'doesnt call PCC::PCCService.claim_creation' do
        expect(PCC::PCCService).not_to receive(:claim_creation)

        patch_update_job
      end

      context 'when pcc_coverage_id is passed on the request' do
        let(:pcc_coverage) { create(:pcc_coverage) }

        it 'triggers PCC::PCCService.claim_creation' do
          allow(PCC::PCCService).to receive(:claim_creation)

          expect(PCC::PCCService).to receive(:claim_creation)

          patch_update_job
        end
      end

      context 'when it\'s a Draft Job' do
        subject(:patch_update) do
          put :update,
              params: {
                id: job.id, job: {
                  id: job.id,
                  customer: {
                    id: job.customer.id,
                    full_name: 'Customer Name',
                  },
                },
                save_as_draft: true,
              },
              as: :json
        end

        let(:job) do
          create(:fleet_managed_job,
                 fleet_company: Company.find_by_name!('Fleet Demo'),
                 status: Job::DRAFT,
                 get_location_sms: true)
        end

        it 'sets jobs.last_touched_by' do
          expect(job.last_touched_by).to be_nil

          patch_update_job

          expect(job.last_touched_by).to eq(controller.api_user)
        end

        it 'calls job.fixup_locations' do
          allow_any_instance_of(FleetManagedJob).to receive(:fixup_locations)
          expect_any_instance_of(FleetManagedJob).to receive(:fixup_locations)

          patch_update_job
        end

        it 'keeps the status as Draft' do
          patch_update_job

          expect(job.status).to eq Job::STATUS_DRAFT
        end

        it 'updates attributes accordingly' do
          expect(job.driver.user.name).not_to eq 'Customer Name'

          patch_update_job

          expect(job.driver.user.name).to eq 'Customer Name'
        end

        it 'calls update_history' do
          allow_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

          expect_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

          patch_update_job
        end

        it 'dont call other non-draft methods' do
          expect_any_instance_of(HandleToa).not_to receive(:call)
          expect_any_instance_of(FleetManagedJob).not_to receive(:send_get_location)

          patch_update_job
        end

        context 'when save_as_draft param is not passed on the request' do
          subject(:patch_update) do
            put :update,
                params: {
                  id: job.id, job: {
                    id: job.id,
                    customer: {
                      id: job.customer.id,
                      full_name: 'Customer Name',
                    },
                    scheduled_for: scheduled_for,
                    text_eta_updates: text_eta_updates,
                  },
                },
                as: :json
          end

          let(:scheduled_for) { nil }

          let(:text_eta_updates) { false }

          context 'when fleet_company has SMS_CONFIRM_ON_SITE feature' do
            it 'creates a ConfirmPartnerOnSiteSmsAlert' do
              allow_any_instance_of(FleetCompany).to receive(:has_feature).with("Auto Assign").and_call_original
              allow_any_instance_of(FleetCompany).to receive(:has_feature).with("SMS_Request Location").and_call_original
              allow_any_instance_of(FleetCompany).to receive(:has_feature).with(Feature::SMS_CONFIRM_ON_SITE).and_return(true)

              allow(ConfirmPartnerOnSiteSmsAlert).to receive(:create!)
              expect(ConfirmPartnerOnSiteSmsAlert).to receive(:create!)

              patch_update_job
            end
          end

          it 'calls job#scheduled_system_alert_create' do
            allow_any_instance_of(FleetManagedJob).to receive(:scheduled_system_alert_create)
            expect_any_instance_of(FleetManagedJob).to receive(:scheduled_system_alert_create)

            patch_update_job
          end

          it 'calls job#set_service_location_user' do
            allow_any_instance_of(FleetManagedJob).to receive(:set_service_location_user)
            expect_any_instance_of(FleetManagedJob).to receive(:set_service_location_user)

            patch_update_job
          end

          it 'moves the job to pending status' do
            patch_update_job

            expect(job.status).to eq Job::STATUS_PENDING
          end

          it 'calls job#schedule_create_storage_invoice_batch' do
            allow_any_instance_of(FleetManagedJob).to receive(:schedule_create_storage_invoice_batch)
            expect_any_instance_of(FleetManagedJob).to receive(:schedule_create_storage_invoice_batch)

            patch_update_job
          end

          it 'calls job#send_swoop_alert' do
            allow_any_instance_of(FleetManagedJob).to receive(:send_swoop_alert)
            expect_any_instance_of(FleetManagedJob).to receive(:send_swoop_alert)

            patch_update_job
          end

          it 'calls job#slack_after_create' do
            allow_any_instance_of(FleetManagedJob).to receive(:slack_after_create)
            expect_any_instance_of(FleetManagedJob).to receive(:slack_after_create)

            patch_update_job
          end

          it 'enqueues EmailStatus::Create' do
            allow(JobStatusEmailSender).to receive(:new)
            expect(JobStatusEmailSender).to receive(:new).with(job.id, JobStatusEmail::Created.name)

            patch_update_job
          end

          it 'doesnt send confirmation sms' do
            expect_any_instance_of(Job).not_to receive(:send_confirmation_sms)

            patch_update_job
          end

          it 'calls job#send_get_location' do
            allow_any_instance_of(Job).to receive(:send_get_location)
            expect_any_instance_of(Job).to receive(:send_get_location)

            patch_update_job
          end

          context 'when text_eta_updates is passed on the request' do
            let(:text_eta_updates) { true }

            it 'calls job#send_confirmation_sms' do
              allow_any_instance_of(Job).to receive(:send_confirmation_sms)
              expect_any_instance_of(Job).to receive(:send_confirmation_sms)

              patch_update_job
            end
          end

          context 'when job is scheduled' do
            let(:scheduled_for) { '2018-02-06T18:47:00.000Z' }

            it 'enqueues EmailStatus::Scheduled' do
              allow_any_instance_of(Job).to receive(:send_confirmation_sms)

              allow(JobStatusEmailSender).to receive(:new)
              expect(JobStatusEmailSender).to receive(:new).with(job.id, JobStatusEmail::Scheduled.name)

              patch_update_job
            end
          end
        end
      end
    end
  end

  describe "active" do
    stub_user_login(:fleet_demo)

    let(:fleet_company) { user.company }
    let!(:job_1) { create(:fleet_managed_job, :completed, fleet_company: fleet_company) }
    let!(:job_2) { create(:fleet_managed_job, :with_service_location, fleet_company: fleet_company, status: :pending) }
    let!(:job_3) { create(:fleet_managed_job, :with_service_location, fleet_company: fleet_company, status: :pending) }

    it "returns active jobs" do
      get :active, format: :json
      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_2.id, job_3.id])
    end
  end
end
