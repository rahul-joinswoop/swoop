# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::InventoryItemsController, type: :controller do
  stub_fleet_company_user_login

  let(:user) { create(:user, :rescue, company: company) }

  let!(:inventory_item) do
    create(:inventory_item, company: company, user: user, deleted_at: nil)
  end

  describe 'DELETE #destroy' do
    subject do
      delete :destroy, params: { id: inventory_item.id }, as: :json
      response
    end

    it 'toggles the deleted_at status' do
      subject

      expect { inventory_item.reload }.to change(inventory_item, :deleted_at)
    end

    it 'calls the update service object' do
      expect(InventoryItemService::Update).to receive(:call)

      subject
    end

    it { is_expected.to have_http_status(:success) }
  end

  describe 'PATCH #update' do
    subject do
      patch :update, params: params
    end

    let(:params) do
      {
        id: inventory_item.id,
        format: :json,
        inventory_item: {
          released_at: Time.zone.now,
        },
      }
    end

    it 'successfully updates inventory item' do
      expect { subject }.to change {
        inventory_item.reload.released_at
      }.from(nil)
    end
  end
end
