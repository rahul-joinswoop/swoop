# frozen_string_literal: true

require "rails_helper"

describe API::V1::Fleet::RescueCompaniesController, type: :controller do
  render_views

  before do
    # make sure Swoop company exists for this test suite
    SuperCompany.where(name: 'Swoop').first_or_create!
    allow_any_instance_of(CompanyService::ChangeFeatures).to receive(:remove_storage_service_from_company_if_needed)
  end

  def new_rescue_provider_for_fleet
    tesla = Company.find_by(name: 'Tesla')

    site_hq = created_company.hq

    RescueProvider.find_by(
      provider_id: created_company.id,
      company_id: tesla.id,
      site_id: site_hq.id
    )
  end

  def new_rescue_provider_for_swoop
    RescueProvider.find_by(
      provider_id: created_company.id,
      company_id: Company.swoop_id,
      site_id: JSON.parse(response.body)['sites'].last['id']
    )
  end

  describe "GET show" do
    stub_user_login(:tesla)

    it "returns a company by given id" do
      company = create :rescue_company, :finish_line
      get :show, params: { id: company.id }, as: :json
      expect(response.body).to include(%("id":#{company.id}))
    end

    it "returns a 404 when a company is not found" do
      get :show, params: { id: 123 }
      expect(response.status).to eql(404)
    end
  end

  describe "POST create" do
    def created_company
      Company.find(JSON.parse(response.body)['id'])
    end

    subject(:create_partner) do
      post :create, params: { partner: request_data }, as: :json
    end

    let(:request_data) do
      {
        # company attributes
        name: "Partner Company",
        dispatch_email: "dispatch@email.com",
        phone: "+13045554000",
        accounting_email: "accounting@email.com",
        fax: "+13040000001",

        # location attributes
        location: {
          lat: -23.2000805,
          lng: -47.29353800000001,
          place_id: "ChIJ7frs2GVSz5QRp5SW3cXtdOc",
          address: "Salto - SP, Brasil",
          exact: true,
        },

        # site attributes
        tire_program: true,

        # rescue_provider attributes
        primary_contact: 'Rescue Provider Primary Contact',
        primary_phone: '+13040009999',
        primary_email: 'primary@email.com',
        status: 'WHATEVER',
        vendor_code: '0987',
        location_code: '1234',
        notes: 'Can be anything',
        tags: tesla_tag_id,
      }
    end

    let(:tesla_tag) { create(:tag, company: user.company, name: 'Primary') }
    let(:tesla_tag_id) { [tesla_tag.id] }

    let!(:admin_role) { create(:role, :admin) }
    let!(:dispatcher_role) { create(:role, :dispatcher) }
    let!(:super_root_user) do
      u = create(:user, :root)
      u
    end

    stub_user_login(:tesla)

    before :each do
      allow_any_instance_of(RescueCompany).to receive(:defaults)
    end

    it 'creates a new RescueCompany and a default Territory' do
      expect { create_partner }.to change(RescueCompany, :count).by(1)
    end

    it 'triggers RescueCompany#defaults' do
      expect_any_instance_of(RescueCompany).to receive(:defaults)

      create_partner
    end

    it 'sets RescueCompany attributes accordingly' do
      create_partner

      expect(created_company.name).to eq 'Partner Company'
      expect(created_company.dispatch_email).to eq 'dispatch@email.com'
      expect(created_company.phone).to eq '+13045554000'
      expect(created_company.accounting_email).to eq 'accounting@email.com'
      expect(created_company.fax).to eq '+13040000001'
      expect(created_company.invited_by_company_id).to eq controller.api_company.id
      expect(created_company.invited_by_user_id).to eq controller.api_user.id
    end

    it 'sets company location accordingly' do
      create_partner

      location = Location.find_by(
        lat: -23.2000805,
        lng: -47.29353800000001,
        place_id: "ChIJ7frs2GVSz5QRp5SW3cXtdOc",
        address: "Salto - SP, Brasil",
        exact: true
      )

      expect(created_company.location).to eq location
    end

    it "creates a new Site" do
      expect { create_partner }.to change(Site, :count).by(1)
    end

    it "sets Site attributes accordingly" do
      create_partner

      site = Site.where(company_id: JSON.parse(response.body)['id']).first

      expect(site.name).to eq  "HQ"
      expect(site.phone).to eq "+13045554000"
      expect(site.tire_program).to eq true
      expect(site.dispatchable).to eq true
      expect(site.company_id).to eq created_company.id
      expect(site.location_id).to eq created_company.location_id
    end

    it "creates a new rescue provider for the fleet with the expected attributes" do
      create_partner

      tesla = Company.find_by(name: 'Tesla')

      site_hq = created_company.hq

      rescue_provider_for_fleet = RescueProvider.find_by(
        provider_id: created_company.id,
        company_id: tesla.id,
        site_id: site_hq.id
      )

      expect(rescue_provider_for_fleet.primary_contact).to eq 'Rescue Provider Primary Contact'
      expect(rescue_provider_for_fleet.primary_phone).to eq '+13040009999'
      expect(rescue_provider_for_fleet.primary_email).to eq 'primary@email.com'
      expect(rescue_provider_for_fleet.status).to eq 'WHATEVER'
      expect(rescue_provider_for_fleet.vendor_code).to eq '0987'
      expect(rescue_provider_for_fleet.location_code).to eq '1234'
      expect(rescue_provider_for_fleet.notes).to eq 'Can be anything'
      expect(rescue_provider_for_fleet.tags.first.name).to eq 'Primary'
      expect(rescue_provider_for_fleet.tags.first.company.id).to eq user.company.id
    end

    context 'when no Tag is given' do
      let(:tesla_tag_id) { [nil] }

      before do
        create_partner
      end

      it 'does not add a tag for the new rescue_provider' do
        expect(new_rescue_provider_for_fleet.tags).to be_blank
      end

      it 'adds Tesla tag for the new Swoop rescue_provider' do
        expect(new_rescue_provider_for_swoop.tags).to be_present

        expect(new_rescue_provider_for_swoop.tags.first.name).to eq 'Tesla'
      end
    end

    it "creates a new rescue provider for swoop with the expected attributes" do
      create_partner

      rescue_provider_for_swoop = RescueProvider.find_by(
        provider_id: created_company.id,
        company_id: Company.swoop_id,
        site_id: JSON.parse(response.body)['sites'].last['id']
      )

      expect(rescue_provider_for_swoop.primary_contact).to eq 'Rescue Provider Primary Contact'
      expect(rescue_provider_for_swoop.primary_phone).to eq '+13040009999'
      expect(rescue_provider_for_swoop.primary_email).to eq 'primary@email.com'
      expect(rescue_provider_for_swoop.status).to eq 'WHATEVER'
      expect(rescue_provider_for_swoop.vendor_code).to eq '0987'
      expect(rescue_provider_for_swoop.location_code).to eq '1234'
      expect(rescue_provider_for_swoop.notes).to eq 'Can be anything'
    end

    it "contains rescue_provider_tag for fleet" do
      create_partner

      tesla = Company.find_by(name: 'Tesla')

      rescue_provider_for_fleet = RescueProvider.find_by(
        provider_id: created_company.id,
        company_id: tesla.id,
        site_id: created_company.hq.id
      )

      rescue_provider_tag_for_fleet = RescueProvidersTag.find_by(
        tag_id: Tag.find_by!(name: 'Primary', company: tesla).id,
        rescue_provider_id: rescue_provider_for_fleet.id
      )

      expect(rescue_provider_tag_for_fleet).not_to eq nil
    end

    it "contains rescue_provider_tag for swoop" do
      create_partner

      swoop = Company.swoop
      tesla = Company.find_by(name: 'Tesla')

      rescue_provider_for_swoop = RescueProvider.find_by(
        provider_id: created_company.id,
        company_id: swoop.id,
        site_id: created_company.hq.id
      )

      rescue_provider_tag_for_swoop = RescueProvidersTag.find_by(
        tag_id: Tag.find_by!(name: tesla.name, company: swoop).id,
        rescue_provider_id: rescue_provider_for_swoop.id
      )

      expect(rescue_provider_tag_for_swoop).not_to eq nil
    end

    it "sends EmailFleetEnabledDispatchToSite email" do
      create_partner

      expect(Delayed::Job.last.name).to eql("EmailFleetEnabledDispatchToSite")
    end

    it 'adds default invoice charge fee' do
      create_partner

      invoice_charge_fee = created_company.invoice_charge_fee

      expect(invoice_charge_fee).to be_present
      expect(invoice_charge_fee.percentage).to be_present
      expect(invoice_charge_fee.fixed_value).to be_present
      expect(invoice_charge_fee.payout_interval).to be_present
    end

    it 'adds default role permissions' do
      create_partner

      role_permissions = created_company.role_permissions

      expect(role_permissions.size).to eq 3

      charge_permissions = role_permissions.where(permission_type: 'charge')
      expect(charge_permissions.size).to eq 2

      admin_permission = charge_permissions.find_by(role_id: admin_role.id)
      expect(admin_permission).to be_present

      dispatcher_permission = charge_permissions.find_by(role_id: dispatcher_role.id)
      expect(dispatcher_permission).to be_present

      refund_permissions = role_permissions.where(permission_type: 'refund')
      expect(refund_permissions.size).to eq 1

      admin_permission = refund_permissions.find_by(role_id: admin_role.id)
      expect(admin_permission).to be_present
    end
  end

  describe "POST add_site_to_existent_partner" do
    stub_user_login(:tesla)

    def new_rescue_provider_for_fleet
      tesla = Company.find_by(name: 'Tesla')

      RescueProvider.find_by(
        provider_id: rescue_company.id,
        company_id: tesla.id,
        site_id: json['sites'].last['id']
      )
    end

    def new_rescue_provider_for_swoop
      RescueProvider.find_by(
        provider_id: rescue_company.id,
        company_id: Company.swoop_id,
        site_id: json['sites'].last['id']
      )
    end

    subject(:add_site_to_existent_partner) do
      post :add_site_to_existent_partner, params: { partner: request_data }, as: :json
    end

    let(:account) do
      create :account, company: rescue_company, client_company: fleet_company
    end

    let!(:rescue_company) { create :rescue_company, :finish_line, fax: nil }
    let(:fleet_company) { user.company }
    let(:request_data) do
      {
        # company attributes
        id: rescue_company.id,
        fax: "+13040000001",

        # location attributes
        location: {
          lat: -23.2000805,
          lng: -47.29353800000001,
          place_id: "ChIJ7frs2GVSz5QRp5SW3cXtdOc",
          address: "Salto - SP, Brasil",
          exact: true,
        },

        # site attributes
        name: 'New Site',
        phone: '+13040006661',
        tire_program: true,

        # rescue_provider attributes
        primary_contact: 'Rescue Provider Primary Contact',
        primary_phone: '+13040009999',
        primary_email: 'primary@email.com',
        status: 'WHATEVER',
        vendor_code: '0987',
        location_code: '1234',
        notes: 'Can be anything',
        tags: tesla_tag_id,
      }
    end

    let(:tesla_tag) { create(:tag, company: user.company, name: 'Primary') }
    let(:tesla_tag_id) { [tesla_tag.id] }

    it "creates a new Site" do
      expect { add_site_to_existent_partner }.to change(Site, :count).by(1)
    end

    it "sets Site attributes accordingly" do
      add_site_to_existent_partner

      site = Site.last

      expect(site.name).to eq  "New Site"
      expect(site.phone).to eq "+13040006661"
      expect(site.tire_program).to eq true
      expect(site.dispatchable).to eq true
    end

    it "sets a Location for the Site accordingly" do
      add_site_to_existent_partner

      site     = Site.find(JSON.parse(response.body)['sites'].last['id'])
      location = Location.find(JSON.parse(response.body)['sites'].last['location']['id'])

      expect(site.location).to eq location
    end

    it "updates fax attribute for the rescue company" do
      add_site_to_existent_partner

      expect(rescue_company.reload.fax).to eq '+13040000001'
    end

    it "creates a new rescue provider for the fleet with the expected attributes" do
      add_site_to_existent_partner

      tesla = Company.find_by(name: 'Tesla')

      rescue_provider_for_fleet = RescueProvider.find_by(
        provider_id: rescue_company.id,
        company_id: tesla.id,
        site_id: JSON.parse(response.body)['sites'].last['id']
      )

      expect(rescue_provider_for_fleet.primary_contact).to eq 'Rescue Provider Primary Contact'
      expect(rescue_provider_for_fleet.primary_email).to eq 'primary@email.com'
      expect(rescue_provider_for_fleet.status).to eq 'WHATEVER'
      expect(rescue_provider_for_fleet.vendor_code).to eq '0987'
      expect(rescue_provider_for_fleet.location_code).to eq '1234'
      expect(rescue_provider_for_fleet.notes).to eq 'Can be anything'
      expect(rescue_provider_for_fleet.tags.first.name).to eq 'Primary'
      expect(rescue_provider_for_fleet.tags.first.company.id).to eq user.company.id
    end

    context 'when no Tag is given' do
      let(:tesla_tag_id) { [nil] }

      before do
        add_site_to_existent_partner
      end

      it 'does not add a tag for the new rescue_provider' do
        expect(new_rescue_provider_for_fleet.tags).to be_blank
      end

      it 'adds Tesla tag for the new Swoop rescue_provider' do
        expect(new_rescue_provider_for_swoop.tags).to be_present

        expect(new_rescue_provider_for_swoop.tags.first.name).to eq 'Tesla'
      end
    end

    it "creates a new rescue provider for swoop with the expected attributes" do
      add_site_to_existent_partner

      rescue_provider_for_swoop = RescueProvider.find_by(
        provider_id: rescue_company.id,
        company_id: Company.swoop_id,
        site_id: JSON.parse(response.body)['sites'].last['id']
      )

      expect(rescue_provider_for_swoop.primary_contact).to eq 'Rescue Provider Primary Contact'
      expect(rescue_provider_for_swoop.primary_email).to eq 'primary@email.com'
      expect(rescue_provider_for_swoop.status).to eq 'WHATEVER'
      expect(rescue_provider_for_swoop.vendor_code).to eq '0987'
      expect(rescue_provider_for_swoop.location_code).to eq '1234'
      expect(rescue_provider_for_swoop.notes).to eq 'Can be anything'
    end

    it "contains rescue_provider_tag for fleet" do
      add_site_to_existent_partner

      tesla = Company.find_by(name: 'Tesla')

      rescue_provider_for_fleet = RescueProvider.find_by(
        provider_id: rescue_company.id,
        company_id: tesla.id,
        site_id: JSON.parse(response.body)['sites'].last['id']
      )

      rescue_provider_tag_for_fleet = RescueProvidersTag.find_by(
        tag_id: Tag.find_by!(name: 'Primary', company: tesla).id,
        rescue_provider_id: rescue_provider_for_fleet.id
      )

      expect(rescue_provider_tag_for_fleet).not_to eq nil
    end

    it "contains rescue_provider_tag for swoop" do
      add_site_to_existent_partner

      swoop = Company.swoop
      tesla = Company.find_by(name: 'Tesla')

      rescue_provider_for_swoop = RescueProvider.find_by(
        provider_id: rescue_company.id,
        company_id: swoop.id,
        site_id: JSON.parse(response.body)['sites'].last['id']
      )

      rescue_provider_tag_for_swoop = RescueProvidersTag.find_by(
        tag_id: Tag.find_by_name(tesla.name).id,
        rescue_provider_id: rescue_provider_for_swoop.id
      )

      expect(rescue_provider_tag_for_swoop).not_to eq nil
    end

    it "sends EmailFleetEnabledDispatchToSite email" do
      add_site_to_existent_partner

      expect(Delayed::Job.last.name).to eql("EmailFleetEnabledDispatchToSite")
    end

    it "creates a new Account when one does not exist" do
      account =
        Account.find_by(company: rescue_company, client_company: fleet_company)
      expect(account).to be_nil

      add_site_to_existent_partner

      account =
        Account.find_by(company: rescue_company, client_company: fleet_company)
      expect(account).not_to be_nil
    end

    it "does not create an Account when one exists for partner and fleet" do
      account
      add_site_to_existent_partner

      account_after_add =
        Account.find_by(company: rescue_company, client_company: fleet_company)
      expect(account).to eql(account_after_add)
    end

    it "updates an existing Account's deleted_at date to be nil" do
      account.update! deleted_at: Time.zone.now
      add_site_to_existent_partner
      expect(account.reload.deleted_at).to be_nil
    end
  end

  describe "PATCH add_existing_site" do
    def new_rescue_provider_for_fleet
      RescueProvider.find_by(
        company: fleet_company, provider: rescue_company, site: partner_site
      )
    end

    subject(:add_existing_site) do
      post :add_existing_site, params: { partner: request_data }, as: :json
    end

    let(:account) do
      create :account, company: rescue_company, client_company: fleet_company
    end
    let!(:rescue_company) { create :rescue_company, :finish_line, fax: nil }
    let(:fleet_company) { user.company }
    let!(:partner_site) do
      create :partner_site, company: rescue_company, dispatchable: true
    end
    let(:request_data) do
      {
        "fax": "+14156660000",
        "primary_contact": "Michael Scott",
        "primary_phone": "+14155550000",
        "primary_email": "michael@mytowtow.com",
        "status": "Pending Training",
        "vendor_code": "04",
        "location_code": "350",
        "tire_program": true,
        "notes": "Specially for you...",
        "id": rescue_company.id,
        "site_id": partner_site.id,
        "tags": tesla_tag_id,
      }
    end

    let(:tesla_tag) { create(:tag, company: user.company, name: 'Primary') }
    let(:tesla_tag_id) { [tesla_tag.id] }

    stub_user_login(:tesla)

    it "updates partner company fields" do
      add_existing_site
      expect(rescue_company.reload.fax).to eql("+14156660000")
    end

    it "creates a new RescueProvider" do
      expect { add_existing_site }.to change(RescueProvider, :count).by(1)
    end

    describe "when the new RescueProvider is created" do
      subject(:created_rescue_provider) { RescueProvider.last }

      before do
        add_existing_site
      end

      it "belongs to signed in Fleet and given Site" do
        expect(created_rescue_provider.site_id).to eql(partner_site.id)
        expect(created_rescue_provider.company_id).to eql(user.company_id)
      end

      it "saves RescueProvider fields" do
        expect(created_rescue_provider.primary_contact)
          .to eql("Michael Scott")
        expect(created_rescue_provider.primary_email)
          .to eql("michael@mytowtow.com")
        expect(created_rescue_provider.status).to eql("Pending Training")
        expect(created_rescue_provider.vendor_code).to eql("04")
        expect(created_rescue_provider.location_code).to eql("350")
      end
    end

    it "creates a new Account when one does not exist" do
      account =
        Account.find_by(company: rescue_company, client_company: fleet_company)
      expect(account).to be_nil

      add_existing_site

      account =
        Account.find_by(company: rescue_company, client_company: fleet_company)
      expect(account).not_to be_nil
    end

    it "does not create an Account when one exists for partner and fleet" do
      account
      add_existing_site

      account_after_add =
        Account.find_by(company: rescue_company, client_company: fleet_company)
      expect(account).to eql(account_after_add)
    end

    it "updates an existing Account's deleted_at date to be nil" do
      account.update! deleted_at: Time.zone.now
      add_existing_site
      expect(account.reload.deleted_at).to be_nil
    end

    it "creates a RescueProvidersTag for the new RescueProvider" do
      add_existing_site

      rescue_provider_tag = RescueProvidersTag.last

      rescue_provider = RescueProvider.find_by(
        company: fleet_company, provider: rescue_company, site: partner_site
      )

      expect(rescue_provider_tag.rescue_provider_id).to eql(rescue_provider.id)
      expect(rescue_provider_tag.tag).to eql(tesla_tag)
    end

    context 'when no Tag is given' do
      let(:tesla_tag_id) { [nil] }

      before do
        add_existing_site
      end

      it 'does not add a tag for the new rescue_provider' do
        expect(new_rescue_provider_for_fleet.tags).to be_blank
      end
    end

    it "makes Site#dispatchable true if false" do
      partner_site.update! dispatchable: false
      add_existing_site
      expect(partner_site.reload.dispatchable?).to eql true
    end

    it "sends an email" do
      add_existing_site
      expect(Delayed::Job.last.name).to eql("EmailFleetEnabledDispatchToSite")
    end

    it "returns 200 on success" do
      add_existing_site
      expect(response.status).to eql(200)
    end

    it "returns a Company JSON" do
      expect(add_existing_site).to render_template("api/v1/companies/show")
    end

    context "when not Fleet InHouse" do
      stub_user_login

      it "returns an authorization error" do
        add_existing_site
        expect(response.status).to eql(403)
      end
    end

    it "does not raise an error when the Site is already added to the Fleet" do
      create :rescue_provider,
             company: fleet_company,
             provider: FactoryBot.create(:rescue_company),
             site: partner_site

      add_existing_site
      expect(response.status).to eql(200)
    end
  end
end
