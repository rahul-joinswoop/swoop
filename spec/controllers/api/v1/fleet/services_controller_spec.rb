# frozen_string_literal: true

require "rails_helper"

describe API::V1::Fleet::ServicesController, type: :controller do
  render_views

  describe "GET index_by_rates_filter" do
    stub_user_login(:tesla)

    subject(:get_index_by_rates_filter) do
      get(
        :index_by_rates_filter,
        params: {
          rescue_provider_id: rescue_provider_id,
        },
        as: :json
      )
    end

    let(:rescue_company) { create(:rescue_company) }

    let(:service_code_one) { create(:service_code, name: ServiceCode::TOW) }
    let(:service_code_two) { create(:service_code, name: ServiceCode::BATTERY_JUMP) }

    let(:rescue_provider_id) { rescue_provider.id }
    let(:rescue_provider) do
      create(:rescue_provider, :default, site: create(:site, company: rescue_company), company: user.company)
    end

    let(:account_with_rates) do
      create(
        :account,
        company: rescue_company,
        client_company: user.company,
        name: 'Account with Rate'
      )
    end

    let(:rate_account_level_one) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site: nil,
        vendor_id: nil,
        service_code_id: service_code_one.id,
        hourly: 32.0
      )
    end

    let(:rate_account_level_two) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site: nil,
        vendor_id: nil,
        service_code_id: service_code_two.id,
        hourly: 32.0
      )
    end

    before do
      # add services to company
      rescue_company.services << service_code_one
      rescue_company.services << service_code_two

      # create rates
      rate_account_level_one
      rate_account_level_two
    end

    it 'responds with services bound with rates for the given rescue_company_id' do
      get_index_by_rates_filter
      expect(response).to be_successful

      services_response = JSON.parse(response.body)

      service_one_found = services_response.find do |service|
        service["id"] == service_code_one.id
      end

      service_two_found = services_response.find do |service|
        service["id"] == service_code_two.id
      end

      expect(service_one_found).to be_present
      expect(service_two_found).to be_present
    end
  end
end
