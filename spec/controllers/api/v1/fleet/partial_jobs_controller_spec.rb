# frozen_string_literal: true

require "rails_helper"

describe API::V1::Fleet::PartialJobsController do
  stub_user_login(:fleet_demo)

  let(:fleet_company) { user.company }
  let(:fleet_site) { create(:fleet_site, company: fleet_company) }
  let!(:job_1) { create(:job, :completed, fleet_company: fleet_company) }
  let!(:job_2) { create(:job, :completed, fleet_company: fleet_company, fleet_site: fleet_site) }
  let!(:job_3) { create(:job, status: :reassigned, fleet_company: fleet_company) }

  describe "without search terms" do
    it "only return jobs for the fleet company but not reassigned jobs" do
      get :index
      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_1.id, job_2.id])
    end

    it "only return jobs for whitelisted fleet sites for the user if they have any" do
      user.sites << fleet_site
      get :index
      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_2.id])
    end
  end

  describe "with search terms" do
    it "only return jobs for the fleet company but not reassigned jobs" do
      query = { must_not: { status: "reassigned" }, search_terms: { query: "foo" } }
      expect_any_instance_of(JobSearchService).to receive(:search).with(query).and_return([2, [job_1, job_2]])
      get :index, params: { search_terms: { query: "foo" } }
      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_1.id, job_2.id])
    end

    it "only return jobs for whitelisted fleet sites for the user if they have any" do
      user.sites << fleet_site
      query = { must_not: { status: "reassigned" }, filters: { fleet_site_id: [fleet_site.id] }, search_terms: { query: "foo" } }
      expect_any_instance_of(JobSearchService).to receive(:search).with(query).and_return([1, [job_2]])
      get :index, params: { search_terms: { query: "foo" } }
      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_2.id])
    end
  end

  describe "fleet in house company" do
    stub_user_login(:fleet_in_house)

    it "includes reassigned jobs" do
      get :index
      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_1.id, job_2.id, job_3.id])
    end

    it "includes reassigned jobs with a query" do
      query = { search_terms: { query: "foo" } }
      expect_any_instance_of(JobSearchService).to receive(:search).with(query).and_return([2, [job_1, job_2, job_3]])
      get :index, params: { search_terms: { query: "foo" } }
      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_1.id, job_2.id, job_3.id])
    end
  end
end
