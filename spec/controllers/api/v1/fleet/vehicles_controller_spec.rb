# frozen_string_literal: true

require "rails_helper"

describe API::V1::Fleet::VehiclesController, type: :controller do
  render_views
  context "as a fleet user" do
    stub_fleet_managed_company_user_login
    let(:other_fleet_company) { create :fleet_company }
    # vehicles connected to jobs with these statuses shouldn't show up in our
    # search results
    let(:fleet_job_bad_status) do
      create :fleet_managed_job,
             :with_rescue_vehicle,
             fleet_company: company,
             status: Job.fleet_no_publish_states.sample
    end
    # vehicles connected to these jobs should show up
    let(:fleet_jobs) do
      create_list :fleet_managed_job,
                  2,
                  :with_rescue_vehicle,
                  fleet_company: company,
                  status: Job.fleet_show_rescue_vehicle_states.sample
    end
    # this job has no rescue vehicle so they shouldn't affect anything
    let(:fleet_job_no_vehicle) do
      create :fleet_managed_job,
             fleet_company: company,
             status: Job.fleet_show_rescue_vehicle_states.sample
    end
    # these vehicles shouldn't show up because they belong to a different fleet
    # company
    let(:other_fleet_job) do
      create :fleet_managed_job,
             :with_rescue_vehicle,
             fleet_company: other_fleet_company,
             status: Job.fleet_show_rescue_vehicle_states.sample
    end

    let(:all_fleet_jobs) do
      [other_fleet_job, fleet_job_no_vehicle, fleet_job_bad_status] + fleet_jobs
    end

    before { all_fleet_jobs }

    describe "GET index" do
      subject do
        get :index, format: :json
      end

      it "returns 200" do
        expect { subject }.not_to raise_error
        expect(response).to have_http_status(:ok)
        # should only return vehicles connected to our fleet_jobs
        expect(JSON[response.body].map { |v| v['id'] })
          .to contain_exactly(*fleet_jobs.map(&:rescue_vehicle_id))
      end
    end

    describe "GET batch" do
      subject do
        get :batch, params: { vehicle_ids: all_fleet_jobs.map(&:rescue_vehicle_id) }, format: :json
      end

      it "returns 200" do
        expect { subject }.not_to raise_error
        expect(response).to have_http_status(:ok)
        # should only return vehicles connected to our fleet_jobs
        expect(JSON[response.body].map { |v| v['id'] })
          .to contain_exactly(*fleet_jobs.map(&:rescue_vehicle_id))
      end
    end
  end
end
