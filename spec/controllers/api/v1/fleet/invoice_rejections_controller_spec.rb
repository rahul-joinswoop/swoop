# frozen_string_literal: true

require "rails_helper"
require_relative "../shared_examples_for_invoice_rejections_controllers"

describe API::V1::Fleet::InvoiceRejectionsController, type: :controller do
  describe "POST create" do
    context "when user is Fleet InHouse" do
      subject(:post_reject_invoice) do
        post :create, params: params, as: :json
      end

      let!(:invoice) { create :fleet_in_house_job_invoice, state: "partner_sent" }
      let(:invoice_reject_reason) do
        create :invoice_reject_reason, company: user.company
      end
      let(:params) do
        {
          invoice_rejection: {
            invoice_id: invoice.id,
            invoice_reject_reason_id: invoice_reject_reason.id,
            notes: "Approval rejected",
          },
        }
      end

      stub_user_login(:tesla)

      it_behaves_like "API::V1::Shared::InvoiceRejections#create"

      it "moves given invoice to :fleet_rejected state" do
        expect(invoice.state).to eql("partner_sent")
        post_reject_invoice
        expect(invoice.reload.state).to eql("fleet_rejected")
      end

      context "when the invoice can't move to :fleet_rejected state" do
        it "returns an error" do
          invoice.fleet_reject!
          post_reject_invoice
          expect(response.status).to eql(422)
        end
      end
    end

    it "returns an authorization error when not Fleet InHouse" do
      post :create, as: :json
      expect(response.status).to eql(403)
    end
  end

  describe "GET reasons" do
    render_views

    let!(:company_specific_reason) do
      create :invoice_reject_reason, company: user.company, text: "Tesla reason"
    end

    stub_user_login(:tesla)

    it "returns a list of reasons associated with Swoop" do
      create :invoice_reject_reason, company: create(:super_company)

      get :reasons, as: :json

      expect(JSON.parse(response.body)).to eql([
        "id" => company_specific_reason.id,
        "text" => "Tesla reason",
      ])
    end
  end
end
