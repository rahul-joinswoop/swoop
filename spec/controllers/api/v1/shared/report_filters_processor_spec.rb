# frozen_string_literal: true

require "rails_helper"

describe API::V1::Shared::ReportFiltersProcessor do
  before do
    stub_class 'ReportsFilterImpl' do
      include API::V1::Shared::ReportFiltersProcessor

      attr_reader :company_report

      MOCK_COMPANY_REPORT = JSON.parse({
        report: { id: 1 },
      }.to_json, object_class: OpenStruct)

      def initialize(company_report: MOCK_COMPANY_REPORT)
        @company_report = company_report
      end
    end
  end

  describe '#process_params_for_payments_report' do
    subject(:process_transaction_type) do
      ReportsFilterImpl.new.process_params_for_payments_report(params)
    end

    let(:params) do
      {
        'transaction_type' => transaction_type,
        'payment_status' => payment_status,
        'invoice_payment_method' => invoice_payment_method,
      }
    end

    context 'when invoice_payment_method is nil' do
      let(:invoice_payment_method) { nil }

      context 'and payment_status is nil' do
        let(:payment_status) { nil }

        context 'and transaction_type is nil' do
          let(:transaction_type) { nil }

          it { is_expected.to match({}) }
        end

        context 'and transaction_type is 1' do
          let(:transaction_type) { 1 }

          it { is_expected.to match({ 'only_payments' => true }) }
        end
      end

      context 'and payment_status is 1' do
        let(:payment_status) { 1 }

        context 'and transaction_type is nil' do
          let(:transaction_type) { nil }

          it { is_expected.to match({ 'charged' => true }) }
        end

        context 'and transaction_type is 3' do
          let(:transaction_type) { 3 }

          it do
            expect(subject).to match({
              'charged' => true,
              'only_refunds' => true,
            })
          end
        end
      end
    end

    context 'when invoice_payment_method is 0' do
      let(:invoice_payment_method) { 0 }

      context 'and payment_status is 0' do
        let(:payment_status) { 0 }

        context 'and transaction_type is 0' do
          let(:transaction_type) { 0 }

          it { is_expected.to match({}) }
        end

        context 'and transaction_type is 1' do
          let(:transaction_type) { 1 }

          it { is_expected.to match({ 'only_payments' => true }) }
        end
      end

      context 'and payment_status is 1' do
        let(:payment_status) { 1 }

        context 'and transaction_type is 0' do
          let(:transaction_type) { 0 }

          it { is_expected.to match({ 'charged' => true }) }
        end

        context 'and transaction_type is 3' do
          let(:transaction_type) { 3 }

          it do
            expect(subject).to match({
              'charged' => true,
              'only_refunds' => true,
            })
          end
        end
      end
    end

    context 'when invoice_payment_method is 1' do
      let(:invoice_payment_method) { 1 }

      context 'and payment_status is 0' do
        let(:payment_status) { 0 }

        context 'and transaction_type is 0' do
          let(:transaction_type) { 0 }

          it { is_expected.to match({ 'invoice_payment_method' => 1 }) }
        end

        context 'and transaction_type is 1' do
          let(:transaction_type) { 1 }

          it { is_expected.to match({ 'invoice_payment_method' => 1, 'only_payments' => true }) }
        end
      end

      context 'and payment_status is 1' do
        let(:payment_status) { 1 }

        context 'and transaction_type is 0' do
          let(:transaction_type) { 0 }

          it { is_expected.to match({ 'invoice_payment_method' => 1, 'charged' => true }) }
        end

        context 'and transaction_type is 3' do
          let(:transaction_type) { 3 }

          it do
            expect(subject).to match({
              'invoice_payment_method' => 1,
              'charged' => true,
              'only_refunds' => true,
            })
          end
        end
      end
    end
  end

  describe '#process_transaction_type' do
    subject(:process_transaction_type) do
      ReportsFilterImpl.new.send(:process_transaction_type, params)
    end

    let(:params) do
      { 'transaction_type' => transaction_type }
    end

    context 'when transaction_type is nil' do
      let(:transaction_type) { nil }

      it { is_expected.to match({}) }
    end

    context 'when transaction_type is 0' do
      let(:transaction_type) { 0 }

      it { is_expected.to match({}) }
    end

    context 'when transaction_type is 1' do
      let(:transaction_type) { 1 }

      it { is_expected.to match({ 'only_payments' => true }) }
    end

    context 'when transaction_type is 2' do
      let(:transaction_type) { 2 }

      it { is_expected.to match({ 'only_discounts' => true }) }
    end

    context 'when transaction_type is 3' do
      let(:transaction_type) { 3 }

      it { is_expected.to match({ 'only_refunds' => true }) }
    end

    context 'when transaction_type is 4' do
      let(:transaction_type) { 4 }

      it { is_expected.to match({ 'only_writeoffs' => true }) }
    end

    context 'when transaction_type is 5' do
      let(:transaction_type) { 5 }

      it { is_expected.to match({ 'only_swoop_charges' => true }) }
    end
  end

  describe '#process_payment_status' do
    subject(:process_transaction_type) do
      ReportsFilterImpl.new.send(:process_payment_status, params)
    end

    let(:params) do
      { 'payment_status' => payment_status }
    end

    context 'when payment_status is nil' do
      let(:payment_status) { nil }

      it { is_expected.to match({}) }
    end

    context 'when payment_status is 0' do
      let(:payment_status) { 0 }

      it { is_expected.to match({}) }
    end

    context 'when payment_status is 1' do
      let(:payment_status) { 1 }

      it { is_expected.to match({ 'charged' => true }) }
    end

    context 'when payment_status is 2' do
      let(:payment_status) { 2 }

      it { is_expected.to match({ 'refunded' => true }) }
    end
  end
end
