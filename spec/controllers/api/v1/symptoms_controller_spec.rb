# frozen_string_literal: true

require "rails_helper"

describe API::V1::SymptomsController, type: :controller do
  render_views

  describe 'GET show' do
    context 'when user is Fleet' do
      stub_user_login(:tesla)

      subject(:get_standard_symptoms) { get :standard, format: :json }

      before :each do
        tesla = create(:fleet_company, :tesla)

        accident_symptom     = Symptom.create!(name: Symptom::ACCIDENT, is_standard: true)
        not_standard_symptom = Symptom.create!(name: 'Any not standard')

        tesla.symptoms << accident_symptom
        tesla.symptoms << not_standard_symptom
      end

      it 'responds with the standard symptoms' do
        get_standard_symptoms

        symptoms_json_response = JSON.parse(response.body)
        symptoms_response = symptoms_json_response.map do |symptom_json|
          Symptom.new(symptom_json)
        end

        accident_symptom = Symptom.find_by_name(Symptom::ACCIDENT)
        accident_symtom_from_response = symptoms_response.find do |symptom|
          symptom.id == accident_symptom.id
        end

        expect(accident_symtom_from_response.name).to eq Symptom::ACCIDENT
      end

      it 'doesnt come with non-standard symptoms' do
        get_standard_symptoms

        symptoms_json_response = JSON.parse(response.body)
        symptoms_response = symptoms_json_response.map do |symptom_json|
          Symptom.new(symptom_json)
        end

        non_standard_symptom = Symptom.find_by(name: 'Any not standard')

        expect(symptoms_response).not_to include non_standard_symptom
      end

      it 'responds with HTTP 200' do
        get_standard_symptoms

        expect(response).to have_http_status(:ok)
      end
    end
  end
end
