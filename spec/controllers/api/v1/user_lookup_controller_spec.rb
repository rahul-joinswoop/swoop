# frozen_string_literal: true

require "rails_helper"

describe API::V1::UserLookupController, type: :controller do
  let(:user) { build(:user) }

  describe '#lookup by encoded email or username' do
    subject(:get_lookup) do
      get :by_encoded_email_or_username, params: { key: 'xxxx' }
    end

    it 'expects to be authenticated' do
      expect(get_lookup.status).to be(401)
    end

    context 'authenticated' do
      stub_user_login(:root)

      it 'expects user missing' do
        expect(get_lookup.status).to be(404)
      end

      it 'looks up user key' do
        expect(User).to receive(:lookup_by_encoded_email_or_username).with('xxxx').and_return(user)
        expect(get_lookup.status).to be(200)
      end
    end
  end
end
