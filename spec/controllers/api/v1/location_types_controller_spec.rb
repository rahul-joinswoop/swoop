# frozen_string_literal: true

require "rails_helper"

describe API::V1::LocationTypesController, type: :controller do
  render_views
  stub_user_login(:tesla)

  describe 'GET show' do
    subject(:get_location_type) do
      get :show, params: { id: location_type.id, format: :json }
    end

    let(:location_type) { create(:location_type, name: 'Location Type') }

    it 'responds with the expected location type' do
      get_location_type

      json = JSON.parse(response.body)

      expect(json['name']).to eq 'Location Type'
    end

    it 'responds with HTTP 200' do
      get_location_type

      expect(response).to have_http_status(:ok)
    end
  end
end
