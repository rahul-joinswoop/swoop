# frozen_string_literal: true

require "rails_helper"

describe API::V1::AuditJobStatusesController, type: :controller do
  stub_user_login(:tesla)

  describe "POST create" do
    subject(:post_create) do
      # /api/v1/jobs/:id/audit_job_statuses
      post :create, params: {
        job_id: job.id,
        audit_job_status: {
          job_status_name: "ON_SITE",
          last_set_dttm: last_set_dttm,
        },
      }, as: :json
    end

    let(:job) { create(:fleet_in_house_job) }
    let(:last_set_dttm) { Time.current }

    it "creates adds a new audit_job_status to the job" do
      post_create

      job.audit_job_statuses.reload

      on_site_statuses = job.audit_job_statuses.where(job_status_id: JobStatus::ON_SITE)
      expect(on_site_statuses.count).to eq 1

      on_site_status = on_site_statuses.first
      expect(on_site_status.job_status.name).to eq "On Site"
      expect(on_site_status.last_set_dttm.strftime("%T")).to eq last_set_dttm.strftime("%T")
    end
  end
end
