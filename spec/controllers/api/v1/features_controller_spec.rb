# frozen_string_literal: true

require "rails_helper"

describe API::V1::FeaturesController, type: :controller do
  stub_user_login(:rescue)

  describe "GET index" do
    subject(:index) do
      get :index, as: :json
    end

    before do
      create :feature
    end

    let(:feature) { create :feature }

    it "returns head 200" do
      expect(index.status).to be 200
    end

    it "returns serialized Feature" do
      # # not yet sure why rendered json is empty string
      # expect(JSON.parse(response.body)).to eql([
      #   "id" => feature.id,
      #   "name" => feature.name,
      #   "description" => feature.description,
      # ])
    end
  end
end
