# frozen_string_literal: true

require "rails_helper"

describe API::V1::ReferersController, type: :controller do
  describe 'GET index' do
    let!(:deleted_referer) { create(:referer, name: "MySpace", deleted_at: Time.zone.now) }

    let(:returned_referers) do
      json.map { |i| i['name'] }
    end

    before do
      get :index, as: :json
    end

    it 'returns active referers alphabetically (with Other at the end)' do
      expect(returned_referers).to include(*Referer::NAMES)
    end
  end
end
