# frozen_string_literal: true

require 'rails_helper'

describe API::V1::OauthApplicationsController do
  render_views

  let(:application) { create(:oauth_application, owner: user.company) }

  shared_examples_for "access denied" do
    describe "GET index" do
      it "is denied" do
        get :index
        expect(response.status).to eq 403
      end
    end

    describe "POST create" do
      it "is denied" do
        application_count = Doorkeeper::Application.count
        post :create
        expect(response.status).to eq 403
        expect(Doorkeeper::Application.count).to eq application_count
      end
    end

    describe "DELETE destroy" do
      it "is denied" do
        delete :destroy, params: { id: application.id }
        expect(response.status).to eq 403
        expect(application.reload).to be_persisted
      end
    end
  end

  context "without an admin user" do
    stub_user_login(:fleet_demo, :driver)
    it_behaves_like "access denied"
  end

  context "without a fleet user" do
    stub_user_login(:rescue, :admin)
    it_behaves_like "access denied"
  end

  context "with a fleet admin user" do
    stub_user_login(:fleet_demo, :admin)

    describe "GET index" do
      it "returns an empty list if there are no applications" do
        get :index
        expect(response.status).to eq 200
        expect(JSON.load(response.body)).to eq []
      end

      it "returns a list of applications for the company" do
        application
        get :index
        expect(response.status).to eq 200
        expect(JSON.load(response.body)).to eq [{
          "id" => application.id,
          "name" => application.name,
          "client_id" => application.uid,
          "client_secret" => application.secret,
        }]
      end
    end

    describe "POST create" do
      it "creates a new application" do
        application_count = Doorkeeper::Application.count
        post :create
        expect(response.status).to eq 201
        expect(Doorkeeper::Application.count).to eq application_count + 1
        application = Doorkeeper::Application.last
        expect(JSON.load(response.body)).to eq({
          "id" => application.id,
          "name" => application.name,
          "client_id" => application.uid,
          "client_secret" => application.secret,
        })
        expect(application.owner).to eq user.company
      end
    end

    describe "DELETE destroy" do
      it "deletes an application and delete all tokens" do
        access_token = application.access_tokens.create!
        delete :destroy, params: { id: application.id }
        expect(response.status).to eq 200
        expect(JSON.load(response.body)).to eq({ "id" => application.id })
        expect(Doorkeeper::Application.find_by(id: application.id)).to eq nil
        expect(Doorkeeper::AccessToken.find_by(id: access_token.id)).to eq nil
      end

      it "is not able to destroy another company's application" do
        application = create(:oauth_application, owner: create(:fleet_company, name: "Other company"))
        delete :destroy, params: { id: application.id }
        expect(response.status).to eq 404
        expect(application.reload).to be_persisted
      end
    end
  end
end
