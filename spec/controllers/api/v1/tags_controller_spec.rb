# frozen_string_literal: true

require "rails_helper"

describe API::V1::TagsController, type: :controller do
  render_views

  describe 'GET index' do
    stub_user_login(:tesla)

    subject(:get_index) do
      get :index, as: :json
    end

    let(:company) { create(:fleet_company, :tesla) }

    let(:tag_one) { create(:tag, name: 'Tag One', company: company) }
    let(:tag_two) { create(:tag, name: 'Tag Two', company: company) }
    let(:tag_three) { create(:tag, name: 'Tag Three', company: company) }

    before do
      # create tags
      tag_one
      tag_two
      tag_three
    end

    it 'fetches current api_company tags' do
      get_index

      tags_as_json = JSON.parse(response.body)

      tag_one_from_json = tags_as_json.find { |tag| tag["id"] == tag_one.id }
      tag_two_from_json = tags_as_json.find { |tag| tag["id"] == tag_two.id }
      tag_three_from_json = tags_as_json.find { |tag| tag["id"] == tag_three.id }

      expect(tag_one_from_json).to be_present
      expect(tag_two_from_json).to be_present
      expect(tag_three_from_json).to be_present
    end

    context 'when a tag is marked as deleted' do
      it 'doesnt come in the response' do
        tag_one.update!(deleted_at: Time.now)

        get_index

        tags_as_json = JSON.parse(response.body)

        tag_one_from_json = tags_as_json.find { |tag| tag["id"] == tag_one.id }
        tag_two_from_json = tags_as_json.find { |tag| tag["id"] == tag_two.id }
        tag_three_from_json = tags_as_json.find { |tag| tag["id"] == tag_three.id }

        expect(tag_one_from_json).to be_blank
        expect(tag_two_from_json).to be_present
        expect(tag_three_from_json).to be_present

        tag_one.update!(deleted_at: nil)
      end
    end
  end

  describe 'POST create' do
    context 'when it is FleetInHouse' do
      stub_user_login(:tesla)

      subject(:post_create) do
        post(
          :create,
          params: {
            name: 'A brand new Tag',
            color: 'green',
          },
          as: :json
        )
      end

      let(:company) { create(:fleet_company, :tesla) }

      it 'creates a new Tag' do
        post_create

        new_tag_as_json = JSON.parse(response.body)

        expect(new_tag_as_json).to be_present
        expect(new_tag_as_json["name"]).to eq 'A brand new Tag'
        expect(new_tag_as_json["color"]).to eq 'green'
        expect(new_tag_as_json["company_id"]).to eq company.id
      end
    end

    context 'when it is Swoop' do
      stub_user_login(:root)

      subject(:post_create) do
        post(
          :create,
          params: {
            name: 'A brand new Tag for Swoop',
            color: 'green',
          },
          as: :json
        )
      end

      let(:company) { create(:super_company) }

      it 'creates a new Tag for Swoop' do
        post_create

        new_tag_as_json = JSON.parse(response.body)

        expect(new_tag_as_json).to be_present
        expect(new_tag_as_json["name"]).to eq 'A brand new Tag for Swoop'
        expect(new_tag_as_json["color"]).to eq 'green'
        expect(new_tag_as_json["company_id"]).to eq company.id
      end
    end
  end

  describe 'PATCH update' do
    context 'when it is FleetInHouse' do
      stub_user_login(:tesla)

      subject(:patch_update) do
        post(
          :update,
          params: {
            id: existent_tag.id,
            name: 'A new name for a Tesla Tag',
            color: 'grey',
          },
          as: :json
        )
      end

      let(:company) { create(:fleet_company, :tesla) }
      let(:existent_tag) do
        create(:tag, name: 'Existent Tesla Tag', color: 'blue', company: company)
      end

      it 'updates an existent Tag' do
        patch_update

        updated_tag_as_json = JSON.parse(response.body)

        expect(updated_tag_as_json).to be_present
        expect(updated_tag_as_json["name"]).to eq 'A new name for a Tesla Tag'
        expect(updated_tag_as_json["color"]).to eq 'grey'
        expect(updated_tag_as_json["company_id"]).to eq company.id
      end
    end

    context 'when it is Swoop' do
      stub_user_login(:root)

      subject(:patch_update) do
        post(
          :update,
          params: {
            id: existent_tag.id,
            name: 'A new name for a Swoop Tag',
            color: 'orange',
          },
          as: :json,
        )
      end

      let(:company) { create(:super_company) }
      let(:existent_tag) do
        create(:tag, name: 'Existent Swoop Tag', color: 'blue', company: company)
      end

      it 'updates an existent Tag' do
        patch_update

        updated_tag_as_json = JSON.parse(response.body)

        expect(updated_tag_as_json).to be_present
        expect(updated_tag_as_json["name"]).to eq 'A new name for a Swoop Tag'
        expect(updated_tag_as_json["color"]).to eq 'orange'
        expect(updated_tag_as_json["company_id"]).to eq company.id
      end
    end
  end

  describe 'PATCH update' do
    context 'when it is FleetInHouse' do
      stub_user_login(:tesla)

      subject(:delete_destroy) do
        delete(
          :destroy,
          params: {
            id: existent_tag.id,
          },
          as: :json,
        )
      end

      let(:company) { create(:fleet_company, :tesla) }
      let(:existent_tag) do
        create(:tag, name: 'Existent Tesla Tag to be deleted', color: 'blue', company: company)
      end

      it 'deletes the existent Tag' do
        delete_destroy

        deleted_tag_as_json = JSON.parse(response.body)

        expect(deleted_tag_as_json).to be_present
        expect(deleted_tag_as_json["id"]).to eq existent_tag.id
        expect(deleted_tag_as_json["company_id"]).to eq company.id
        expect(deleted_tag_as_json["deleted_at"]).to be_present
      end
    end

    context 'when it is Swoop' do
      stub_user_login(:root)

      subject(:delete_destroy) do
        delete(
          :destroy,
          params: {
            id: existent_tag.id,
          },
          as: :json
        )
      end

      let(:company) { create(:super_company) }
      let(:company) { create(:super_company) }
      let(:existent_tag) do
        create(:tag, name: 'Existent Swoop Tag to be deleted', color: 'blue', company: company)
      end

      it 'deletes the existent Tag' do
        delete_destroy

        deleted_tag_as_json = JSON.parse(response.body)

        expect(deleted_tag_as_json).to be_present
        expect(deleted_tag_as_json["id"]).to eq existent_tag.id
        expect(deleted_tag_as_json["company_id"]).to eq company.id
        expect(deleted_tag_as_json["deleted_at"]).to be_present
      end
    end
  end

  describe 'GET batch' do
    subject(:get_batch) do
      get(
        :batch,
        params: {
          tag_ids: tag_ids,
        },
        as: :json
      )
    end

    let(:tag_ids) { "#{tag_one.id},#{tag_two.id}" }

    context 'when it is FleetInHouse' do
      stub_user_login(:tesla)

      let(:company) { create(:fleet_company, :tesla) }
      let(:tag_one) { create(:tag, name: 'Tag One', company: company) }
      let(:tag_two) { create(:tag, name: 'Tag Two', company: company) }
      let(:tag_three) { create(:tag, name: 'Tag Three', company: company) }

      before do
        get_batch
      end

      it 'responds back with requested tag_ids' do
        tag_one_from_json = json.find { |tag| tag["id"] == tag_one.id }
        tag_two_from_json = json.find { |tag| tag["id"] == tag_two.id }
        tag_three_from_json = json.find { |tag| tag["id"] == tag_three.id }

        expect(tag_one_from_json).to be_present
        expect(tag_two_from_json).to be_present
        expect(tag_three_from_json).to be_blank
      end

      context 'when tag_ids is blank' do
        let(:tag_ids) { "" }

        it 'responds with empty array' do
          expect(response.code).to eq '200'

          expect(json).to eq []
        end
      end
    end

    context 'when it is Swoop' do
      stub_user_login(:root)

      let(:company) { create(:super_company) }
      let(:tag_one) { create(:tag, name: 'Tag One', company: company) }
      let(:tag_two) { create(:tag, name: 'Tag Two', company: company) }
      let(:tag_three) { create(:tag, name: 'Tag Three', company: company) }

      before do
        get_batch
      end

      it 'responds back with requested tag_ids' do
        tag_one_from_json = json.find { |tag| tag["id"] == tag_one.id }
        tag_two_from_json = json.find { |tag| tag["id"] == tag_two.id }
        tag_three_from_json = json.find { |tag| tag["id"] == tag_three.id }

        expect(tag_one_from_json).to be_present
        expect(tag_two_from_json).to be_present
        expect(tag_three_from_json).to be_blank
      end

      context 'when tag_ids is blank' do
        let(:tag_ids) { "" }

        it 'responds with empty array' do
          expect(response.code).to eq '200'

          expect(json).to eq []
        end
      end
    end
  end
end
