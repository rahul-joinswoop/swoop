# frozen_string_literal: true

require 'rails_helper'

describe API::V1::Root::CompanyInitController do
  render_views

  stub_user_login(:root)

  subject(:get_index) do
    get :index, params: { acts_as_company_id: acts_as_company_id }, as: :json
  end

  let(:fleet_managed_company) do
    create(
      :fleet_managed_company,
      :with_ranker,
      vehicle_categories: vehicle_categories,
    )
  end

  let(:acts_as_company_id) { fleet_managed_company.id }

  let(:vehicle_categories) { create_list(:vehicle_category, 1) }

  it 'returns 200 OK' do
    expect(get_index.response_code).to eq 200
  end

  it 'returns with expected vehicle_category associations' do
    get_index

    _vehicle_category = json["vehicle_categories"].first

    expect(_vehicle_category["id"]).to eq vehicle_categories.first.id
  end

  context 'when acts_as_company_id is nil' do
    let(:acts_as_company_id) { nil }

    it 'returns 422 code' do
      expect(get_index.response_code).to eq 422
    end
  end
end
