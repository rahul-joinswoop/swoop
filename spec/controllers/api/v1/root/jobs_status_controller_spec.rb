# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::JobsStatusController, type: :controller do
  stub_user_login :root
  let(:api_user) { user }

  describe "POST update" do
    let(:job) { create :fleet_managed_job, fleet_company: fleet_managed, status: "Pending" }
    let(:fleet_managed) { create(:fleet_company, :fleet_demo) }
    let(:referer) { 'http://example.com' }

    before do
      allow(Job).to receive(:find_by!).with(id: job.id.to_s).and_return(job)
      request.env['HTTP_REFERER'] = referer
    end

    context "completing a job" do
      include_context "job completed shared examples"
      include_context "job status change shared examples"

      subject(:post_complete_job) do
        post :update, params: { id: job.id, job: { status: status }, format: :json }

        job.reload
        user.reload
      end

      let(:status) { "Completed" }

      context 'when user agent is Mac Web' do
        before do
          request.env['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'Web', Swoop::VERSION, 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'mac', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'chrome', '69'
      end

      context 'when user agent is Windows Web' do
        before do
          request.env['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'Web', Swoop::VERSION, 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'windows', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'chrome', '69'
      end

      context 'when user agent is Android' do
        before do
          request.env['HTTP_USER_AGENT'] = 'android Swoop/2.0.63'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'React Android', '2.0.63', 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'android', 'android Swoop/2.0.63', 'generic', '2.0.63'
      end

      context 'when user agent is SwoopMobile' do
        before do
          request.env['HTTP_USER_AGENT'] = 'ios SwoopMobile/9 CFNetwork/808.3 Darwin/16.3.0'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'React iOS', 'SwoopMobile/9', 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'ios', 'ios SwoopMobile/9 CFNetwork/808.3 Darwin/16.3.0', 'generic', 'SwoopMobile/9'
      end

      context 'when agent is ios Swoop' do
        before do
          request.env['HTTP_USER_AGENT'] = 'ios Swoop/1.03.520'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'React iOS', '1.03.520', 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'ios', 'ios Swoop/1.03.520', 'generic', '1.03.520'
      end

      it 'calls ViewAlertService.cancel_if_necessary' do
        expect(ViewAlertService).to receive(:cancel_if_necessary).with(job, status)

        post_complete_job
      end

      it 'calls job.send_user_invoice' do
        expect(job).to receive(:send_user_invoice)

        post_complete_job
      end

      it 'calls job.send_review_sms' do
        expect(job).to receive(:send_review_sms)

        post_complete_job
      end
    end
  end

  describe 'job.last_touched_by' do
    include_context "job status change shared examples"

    subject(:update_job_status) do
      post :update, params: { id: job.id, job: { status: 'Dispatched' }, format: :json }
    end

    let(:job) { create(:fleet_managed_job, status: 'pending') }

    it_behaves_like 'updates job.last_touched_by with controller.api_user'
  end
end
