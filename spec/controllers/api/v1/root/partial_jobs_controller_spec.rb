# frozen_string_literal: true

# /api/v1/root/partial_jobs?page=2&per_page=25&fields=type,invoice_vehicle_category_id&order=last_status_changed_at,desc

require "rails_helper"

describe API::V1::Root::PartialJobsController, type: :request do
  subject do
    get "/api/v1/root/partial_jobs",
        params: { access_token: token.token, fields: 'invoice_vehicle_category_id,review' }
  end

  let!(:user) { create(:user, :root) }
  let!(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }
  let!(:company) { user.company }

  let(:vehicle_category) do
    VehicleCategory.create(name: 'foo')
  end

  let!(:job) do
    create :fleet_managed_job, {
      invoice_vehicle_category: vehicle_category,
    }
  end

  let!(:review) { create :review, job: job, nps_question1: 10, nps_feedback: 'the feedback' }

  context 'default' do
    let(:args) do
      {}
    end

    it 'returns ok' do
      subject
      expect(response).to have_http_status(200)
      expect(json["meta"]["total"]).to eql(1)
      expect(json["jobs"][0]["invoice_vehicle_category_id"]).to eql(vehicle_category.id)
    end

    it 'returns review data' do
      subject
      expect(response).to have_http_status(200)
      expect(json['jobs'][0]['review']['nps_question1']).to eql(10)
      expect(json['jobs'][0]['review']['feedback']).to eql('the feedback')
    end
  end
end
