# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::CustomerTypesController, type: :controller do
  stub_user_login(:root)

  describe "POST create" do
    subject(:create_customer_type) do
      post :create, params: { customer_type: { name: new_payment_type_name } }, as: :json
    end

    let(:new_payment_type_name) { 'New Payment Type' }

    it "creates a new Customer Type" do
      create_customer_type

      expect(CustomerType.exists?(name: new_payment_type_name)).to eq(true)
      expect(JSON.parse(response.body)["name"]).to eq(new_payment_type_name)
      expect(response.status).to eq(200)
    end

    context "when name is duplicated" do
      it 'does not duplicate the Customer Type' do
        CustomerType.where(name: new_payment_type_name).first_or_create!

        create_customer_type

        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)["name"]).to eq(['New Payment Type already exists'])
      end
    end
  end
end
