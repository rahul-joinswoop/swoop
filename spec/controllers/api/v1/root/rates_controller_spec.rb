# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::RatesController, type: :controller do
  render_views

  stub_user_login(:root)

  describe 'GET /root/rates/accounts/:account_id/services/:service_id' do
    subject(:get_account_service) do
      get :account_service, params: params, as: :json
    end

    let(:params) do
      { account_id: account.id, service_id: service.id }
    end

    let(:account) { create(:account, company: user.company, name: 'Account One') }
    let(:service) { create(:service_code, name: 'Any service') }

    let!(:rate_1) do
      create(
        :miles_p2p_rate,
        :rate_service_code_level,
        company: user.company,
        account: account,
        service_code: service,
      )
    end

    it 'fetches expected rates by account_id and service_id' do
      get_account_service

      expect(json.count).to eq 1

      rate = json.first
      expect(rate["account_id"]).to eq account.id
      expect(rate["service_code_id"]).to eq service.id
    end
  end
end
