# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::UsersController, type: :controller do
  stub_user_login(:root)

  describe 'PATCH /root/users/restore' do
    subject { patch :restore, params: params }

    let!(:deleted_user) { create(:user, :deleted) }

    context 'when username provided' do
      let(:params) do
        { username: deleted_user.username, format: :json }
      end

      it 'returns user' do
        subject

        expect(json['user']).to be_present
      end
    end

    context 'when email provided' do
      let(:params) do
        { email: deleted_user.email, format: :json }
      end

      it 'returns user' do
        subject

        expect(json['user']).to be_present
      end
    end

    context 'when user cannot be found' do
      let(:params) do
        { email: 'missing@email.com' }
      end

      it 'returns 404' do
        subject

        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
