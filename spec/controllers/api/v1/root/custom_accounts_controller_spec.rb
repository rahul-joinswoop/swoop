# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::CustomAccountsController, type: :controller do
  render_views

  stub_user_login(:root)

  describe "DELETE destroy" do
    subject(:delete_by_rescue_company_id) do
      delete :destroy, params: { rescue_company_id: rescue_company.id }, as: :json
    end

    let(:rescue_company) { create(:rescue_company) }

    before do
      allow(Stripe::DeleteCustomAccountByRescueCompanyIdWorker).to receive(:perform_async)

      delete_by_rescue_company_id
    end

    it 'returns 200 OK' do
      expect(response.status).to eq 200
    end

    it 'responds with an async_request on JSON body' do
      async_request_json = json["async_request"]

      expect(async_request_json["company_id"]).to eq user.company.id
      expect(async_request_json["user_id"]).to eq user.id
      expect(async_request_json["request"]).to eq("rescue_company_id" => rescue_company.id)
      expect(async_request_json["system"]).to eq "DeleteCustomAccount"
    end

    it 'schedules Stripe::DeleteCustomAccountByRescueCompanyIdWorker' do
      async_request = API::AsyncRequest.where(
        company_id: user.company.id,
        user_id: user.id,
        system: API::AsyncRequest::DELETE_CUSTOM_ACCOUNT
      ).first

      expect(Stripe::DeleteCustomAccountByRescueCompanyIdWorker).to have_received(:perform_async)
        .with(async_request.id, rescue_company.id)
    end
  end
end
