# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::JobsController, type: :controller do
  render_views

  stub_user_login(:root)

  describe 'INDEX' do
    subject do
      get :index, params: { job_ids: [job.id] }, as: :json
    end

    let!(:user) { create :user, :root }
    let(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }

    before(:each) do
      authenticate_request(controller.request, token)
    end

    context 'when job is FleetManagedJob' do
      let(:rescue_company) { controller.api_user.company }
      let(:fleet_company) { create(:fleet_company, :fleet_demo) }

      let!(:account) do
        create(:account, company: rescue_company, client_company: fleet_company)
      end

      let!(:job) do
        create :fleet_managed_job, {
          fleet_company: fleet_company,
          adjusted_created_at: Time.now,
          rescue_company: rescue_company,
          updated_at: '2018-07-26 18:08:04',
        }
      end

      let!(:partner_invoice) do
        create :invoice, {
          job: job,
          sender: rescue_company,
          recipient: Company.swoop,
        }
      end

      it 'returns the job' do
        subject

        first_job = json["jobs"][0]
        expect(first_job).not_to be_nil
        expect(first_job["id"]).to eql(job.id)
      end

      it 'includes an invoice' do
        subject

        first_job = json["jobs"][0]
        expect(first_job).not_to be_nil
        expect(first_job["invoices"].length).to eql(1)
        expect(first_job["invoices"][0]["uuid"]).to eql(partner_invoice.uuid)
      end

      context 'Swoop Invoice exists' do
        let!(:swoop_invoice) do
          create :invoice, {
            job: job,
            sender: Company.swoop,
            recipient: fleet_company,
            state: Invoice::SWOOP_NEW,
          }
        end

        let!(:customer) { create :user }
        let!(:customer_invoice) do
          create :invoice, {
            job: job,
            sender: fleet_company,
            recipient: customer,
            state: Invoice::FLEET_CUSTOMER_NEW,
          }
        end

        it 'includes swoop invoice' do
          subject

          first_job = json["jobs"][0]
          expect(first_job).not_to be_nil
          expect(first_job["invoices"].length).to eql(3)
          expect(first_job["invoices"][0]["uuid"]).to eql(partner_invoice.uuid)
          expect(first_job["invoices"][1]["uuid"]).to eql(swoop_invoice.uuid)
          expect(first_job["invoices"][2]["uuid"]).to eql(customer_invoice.uuid)
        end
      end
    end
  end

  describe '#update' do
    subject(:patch_update) do
      patch :update, params: params, as: :json

      job.reload

      response
    end

    let(:params) do
      {
        id: job.id,
        acts_as_company_id: fleet_managed.id,
        invoice_vehicle_category_id: vehicle_category.id,
        format: :json,
      }
    end

    let(:job) do
      create(:fleet_managed_job, invoice_vehicle_category: nil, fleet_company: fleet_managed)
    end

    let(:fleet_managed) { create(:fleet_managed_company) }

    let(:vehicle_category) { create(:vehicle_category) }

    it 'returns 200 OK' do
      expect(patch_update.code).to eq "200"
    end

    it 'updates the Job with the given params' do
      expect(job.invoice_vehicle_category_id).to eq nil

      patch_update

      expect(job.invoice_vehicle_category_id).to eq vehicle_category.id
    end
  end

  describe "active" do
    let!(:job_1) { create(:fleet_managed_job, :completed) }
    let!(:job_2) { create(:fleet_managed_job, :with_service_location, status: :pending) }
    let!(:job_3) { create(:fleet_managed_job, :with_service_location, status: :pending) }

    it "returns active jobs" do
      get :active, format: :json

      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_2.id, job_3.id])
    end

    context "when jobs have parents and some ajs activity with source_applications" do
      let!(:job_1) { create(:fleet_managed_job, :completed, :with_ajs) }
      let!(:job_2) { create(:fleet_managed_job, :with_service_location, :with_ajs, status: :pending) }
      let!(:job_3) do
        j = create(:fleet_managed_job, :with_service_location, :with_ajs, :with_child, status: :pending, scheduled_for: Time.current)
        j.audit_job_statuses << create(:audit_job_status, :with_source_application, company: j.fleet_company, job: j, job_status: create(:job_status, :assigned))
        j.history_items << create(:history_item, job: j, company: j.fleet_company, name: 'Pickup Location Edited')
        j.audit_sms << create(:audit_sms, :sms_confirmation, job: j)
        j
      end

      let!(:job_4) do
        j = job_3.child_jobs.first
        j.update! status: :unassigned
        j.audit_job_statuses << create(:audit_job_status, :with_source_application, company: j.fleet_company, job: j, job_status: create(:job_status, :assigned))
        j
      end

      it "works" do
        get :active, format: :json

        expect(response).to be_successful
      end
    end
  end

  describe "GET billing_info" do
    it "returns the job billing info" do
      company = create(:rescue_company)
      job = create(:fleet_managed_job, rescue_company: company)
      agent_1 = create(:user)
      agent_2 = create(:user)
      job.create_billing_info!(rescue_company: company, agent: agent_1, billing_type: BillingType::VCC, vcc_amount: 100, goa_agent: agent_2, goa_amount: 20)
      get :billing_info, params: { id: job.id }, as: :json
      expect(response).to be_successful
      expect(JSON.load(response.body)).to eq({
        "agent_id" => agent_1.id,
        "billing_type" => BillingType::VCC,
        "vcc_amount" => 100.0,
        "goa_agent_id" => agent_2.id,
        "goa_amount" => 20.0,
      })
    end

    it "returns empty billing info if it doesn't exist" do
      job = create(:fleet_managed_job)
      expect(job.billing_info).to eq nil
      get :billing_info, params: { id: job.id }, as: :json
      expect(response).to be_successful
      expect(JSON.load(response.body)).to eq({})
    end
  end
end
