# frozen_string_literal: true

require 'rails_helper'

describe API::V1::Root::Search::ProviderSearchController do
  render_views

  # /api/v1/root/providers/search/?page=1&per_page=50&order=id,desc&original=partner_id%3A23&search_terms%5Bpartner_id%5D=23
  describe "GET#index" do
    subject(:get_index) { get :index, params: params, as: :json }

    stub_user_login :root

    let!(:rescue_company) { create(:rescue_company) }

    let(:records_double) do
      double(:records, includes: [])
    end

    let(:params) do
      {
        page: 1,
        per_page: 50,
        order: 'id,desc',
        original: { term: rescue_company.id },
        search_terms: { term: rescue_company.id },
      }
    end

    before do
      allow_any_instance_of(
        ElasticSearchService::ProviderElasticSearchService
      ).to receive(:search).and_return([0, records_double])
    end

    # it uses elastic search and it is apparently not setup to work with specs,
    # so we just check if ProviderElasticSearchService has been called with correct params
    it "returns code 200" do
      get_index

      expect(response.status).to eq 200
    end

    it "passes all params to ProviderElasticSearchService" do
      expect_any_instance_of(ElasticSearchService::ProviderElasticSearchService)
        .to receive(:search).with(
          "search_terms" => { "term" => rescue_company.id.to_s },
          "filters" => { "company_id" => user.company.id }
        )

      get_index
    end
  end
end
