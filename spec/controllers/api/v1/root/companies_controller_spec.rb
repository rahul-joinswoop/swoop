# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::CompaniesController, type: :controller do
  render_views

  before :each do
    standard_services_for_spec.each do |service_name|
      ServiceCode.where(name: service_name).first_or_create
    end

    standard_addons_for_spec.each do |addon_name|
      ServiceCode.where(name: addon_name).first_or_create
    end

    allow_any_instance_of(CompanyService::ChangeFeatures)
      .to receive(:remove_storage_service_from_company_if_needed)
  end

  let(:settings) do
    Setting::SETTINGS_MAP.keys.reduce({}) do |memo, key|
      memo.tap { |m| m[key] = rand(10).to_s }
    end
  end

  describe '#create' do
    def created_company
      Company.find(JSON.parse(response.body)['id'])
    end

    shared_examples "create with settings_attributes" do
      context 'with settings_attributes' do
        let(:new_company_attributes) do
          {
            **super(),
            **settings,
          }
        end

        it "works" do
          post_new_company
          expect(created_company.settings.map(&:key)).to match_array(Setting::SETTINGS_MAP.values)
          expect(created_company.settings.map(&:value)).to match_array(settings.values)
        end
      end
    end

    subject(:post_new_company) do
      post :create, params: { company: new_company_attributes }, as: :json
    end

    stub_user_login(:root)

    let(:new_company_attributes) do
      {
        type: 'FleetCompany',
        in_house: in_house?,
        name: 'New FleetCompany',
        phone: '+13045551231',
        dispatch_email: 'dispatch@email.com',
        accounting_email: 'accounting@emai.com',
        location: {
          lat: -23.2639728,
          lng: -47.29970850000001,
          place_id: "ChIJhQmXWjJbz5QRZKB1RAaG7gA",
          address: 'Itu - SP, Brasil',
          exact: true,
        },
        features: features_param,
      }
    end

    let(:features_param) { [payment_type_feature.id] }
    let(:payment_type_feature) { create(:feature, name: Feature::CUSTOMER_TYPE) }
    let(:in_house?) { true }

    context 'when new company is FleetCompany' do
      before do
        create(:customer_type, name: CustomerType::CUSTOMER_PAY)
        create(:customer_type, name: CustomerType::COVERED)

        create(:feature, name: Feature::SMS_REVIEWS)
        create(:feature, name: Feature::TEXT_ETA_UPDATES)
        create(:feature, name: Feature::SMS_TRACK_DRIVER)
        create(:feature, name: Feature::SMS_REQUEST_LOCATION)
        create(:feature, name: Feature::SETTINGS)
        create(:feature, name: Feature::QUESTIONS)
        create(:feature, name: Feature::SHOW_SYMPTOMS)
        create(:feature, name: Feature::SMS_CONFIRM_ON_SITE)

        create(:symptom, name: 'StandardSymptom', is_standard: true)
        create(:service_code, name: 'StandardAddon', addon: true, is_standard: true)

        stub_const("FleetCompany::DEFAULT_REPORTS_FOR_INHOUSE", [])

        allow_any_instance_of(FleetCompany).to receive(:add_services)
      end

      context 'language' do
        it 'is set to English' do
          new_company_attributes[:language] = Company::ENGLISH
          post_new_company
          expect(response.status).to eq(201)
          expect(created_company.language).to eq(Company::ENGLISH)
        end

        it 'is set to German' do
          new_company_attributes[:language] = Company::GERMAN
          post_new_company
          expect(response.status).to eq(201)
          expect(created_company.language).to eq(Company::GERMAN)
        end

        it "is not set to Spanish" do
          new_company_attributes[:language] = 'es'
          post_new_company
          expect(response.status).to eq(422)
          expect(response.body).to match(/Language es is not supported/)
        end

        it "is created with default if not provided" do
          new_company_attributes[:language] = ''
          post_new_company
          expect(response.status).to eq(201)
        end
      end

      it_behaves_like "create with settings_attributes"

      it 'includes Customer Pay as a Payment Type' do
        post_new_company

        expect(created_company.payment_types.map(&:name)).to include(CustomerType::CUSTOMER_PAY)
      end

      it 'includes Covered as a Payment Type' do
        post_new_company

        expect(created_company.payment_types.map(&:name)).to include(CustomerType::COVERED)
      end

      context 'when company is a ManagedFleetCompany' do
        let(:in_house?) { false }

        before do
          create(:report, name: Report::FLEET_ACTIVITY)
          create(:report, name: Report::FLEET_CSAT)
          create(:report, name: Report::FLEET_ETA_VARIANCE)
        end

        it_behaves_like "create with settings_attributes"

        it 'adds SMS related Features' do
          post_new_company

          expect(created_company.features.map(&:name)).to include(Feature::SMS_REVIEWS)
          expect(created_company.features.map(&:name)).to include(Feature::TEXT_ETA_UPDATES)
          expect(created_company.features.map(&:name)).to include(Feature::SMS_TRACK_DRIVER)
          expect(created_company.features.map(&:name)).to include(Feature::SMS_REQUEST_LOCATION)
        end

        it 'includes Customer Pay as a Payment Type' do
          post_new_company

          expect(created_company.payment_types.map(&:name)).to include(CustomerType::CUSTOMER_PAY)
        end

        it 'includes Covered as a Payment Type' do
          post_new_company

          expect(created_company.payment_types.map(&:name)).to include(CustomerType::COVERED)
        end

        it 'includes Fleet Activity as a Report' do
          post_new_company

          expect(created_company.reports.map(&:name)).to include(Report::FLEET_ACTIVITY)
        end

        it 'includes Fleet CSAT as a Report' do
          post_new_company

          expect(created_company.reports.map(&:name)).to include(Report::FLEET_CSAT)
        end

        it 'includes Fleet ETA Variance as a Report' do
          post_new_company

          expect(created_company.reports.map(&:name)).to include(Report::FLEET_ETA_VARIANCE)
        end

        it 'includes standard symptoms' do
          post_new_company

          expect(created_company.symptoms.standard_items.present?).to be_truthy
        end

        it 'includes standard addons' do
          post_new_company

          expect(created_company.addons.standard_items.present?).to be_truthy
        end
      end
    end

    context 'when new company is RescueCompany' do
      let(:new_company_attributes) do
        {
          type: 'RescueCompany',
          name: 'New RescueCompany',
          phone: '+13045551231',
          dispatch_email: 'dispatch@email.com',
          accounting_email: 'accounting@emai.com',
          location: {
            lat: -23.2639728,
            lng: -47.29970850000001,
            place_id: "ChIJhQmXWjJbz5QRZKB1RAaG7gA",
            address: 'Itu - SP, Brasil',
            exact: true,
          },
          currency: Money.default_currency.iso_code,
        }
      end

      let(:rescue_company) { build(:rescue_company, id: 10) }

      let!(:admin_role) { create(:role, :admin) }
      let!(:dispatcher_role) { create(:role, :dispatcher) }

      before do
        allow_any_instance_of(Company).to receive(:add_features_by_name)
        allow_any_instance_of(Company).to receive(:add_customer_types_by_name)
        allow_any_instance_of(Company).to receive(:add_reports_by_name)
      end

      context 'currency' do
        # If this will be a required input at the API, the front end must be done first.
        it 'defaults' do
          post_new_company
          expect(response.status).to eq(201)
          expect(RescueCompany.last.currency).to eq('USD')
        end

        it 'does not accept blank' do
          new_company_attributes[:currency] = ''
          post_new_company
          expect(response.status).to eq(422)
          expect(response.body).to match(/Currency can't be blank/)
        end

        it 'accepts only EUR/USD' do
          new_company_attributes[:currency] = 'KZT'
          post_new_company
          expect(response.status).to eq(422)
          expect(response.body).to match(/Currency KZT is not supported/)
        end

        it 'is EUR' do
          new_company_attributes[:currency] = 'EUR'
          post_new_company
          expect(response.status).to eq(201)
          expect(RescueCompany.last.currency).to eq('EUR')
        end

        it 'is USD' do
          new_company_attributes[:currency] = 'USD'
          post_new_company
          expect(response.status).to eq(201)
          expect(RescueCompany.last.currency).to eq('USD')
        end

        context 'when swoop dispatcher' do
          stub_user_login(:dispatcher)

          it 'is forbidden' do
            new_company_attributes[:currency] = 'EUR'
            post_new_company
            expect(response.status).to eq(403)
          end
        end
      end

      context 'sets distance unit' do
        it "defaulting to miles" do
          new_company_attributes.delete(:distance_unit)
          post_new_company
          expect(RescueCompany.last.distance_unit).to eq(Company::MILE)
        end

        it 'does not accept blank' do
          new_company_attributes[:distance_unit] = ''
          post_new_company
          expect(response.status).to eq(422)
          expect(response.body).to match(/Distance unit can't be blank/)
        end

        it 'does not accept Furlong' do
          new_company_attributes[:distance_unit] = 'Furlong'
          post_new_company
          expect(response.status).to eq(422)
          expect(response.body).to match(/Furlong is not supported/)
        end

        it 'to Mile' do
          new_company_attributes[:distance_unit] = Company::MILE
          post_new_company
          expect(response.status).to eq(201)
          expect(RescueCompany.last&.distance_unit).to eq(Company::MILE)
        end

        it 'to Kilometer' do
          new_company_attributes[:distance_unit] = Company::KILOMETER
          post_new_company
          expect(response.status).to eq(201)
          expect(RescueCompany.last&.distance_unit).to eq(Company::KILOMETER)
        end

        context 'when swoop dispatcher' do
          stub_user_login(:dispatcher)

          it 'is forbidden' do
            new_company_attributes[:distance_unit] = Company::KILOMETER
            post_new_company
            expect(response.status).to eq(403)
          end
        end
      end

      context 'with valid input' do
        it_behaves_like "create with settings_attributes"

        it 'adds cash call account' do
          post_new_company

          cash_call = created_company.accounts.find_by_name(Account::CASH_CALL)

          expect(cash_call.name).to eq('Cash Call')
        end
      end

      it 'adds default invoice charge fee' do
        post_new_company

        invoice_charge_fee = created_company.invoice_charge_fee

        expect(invoice_charge_fee).to be_present
        expect(invoice_charge_fee.percentage).to be_present
        expect(invoice_charge_fee.fixed_value).to be_present
        expect(invoice_charge_fee.payout_interval).to be_present
      end

      it 'adds default role permissions' do
        post_new_company

        role_permissions = created_company.role_permissions

        expect(role_permissions.size).to eq 3

        charge_permissions = role_permissions.where(permission_type: 'charge')
        expect(charge_permissions.size).to eq 2

        admin_permission = charge_permissions.find_by(role_id: admin_role.id)
        expect(admin_permission).to be_present

        dispatcher_permission = charge_permissions.find_by(role_id: dispatcher_role.id)
        expect(dispatcher_permission).to be_present

        refund_permissions = role_permissions.where(permission_type: 'refund')
        expect(refund_permissions.size).to eq 1

        admin_permission = refund_permissions.find_by(role_id: admin_role.id)
        expect(admin_permission).to be_present
      end

      context 'when duplicated phone number' do
        let(:error_message) do
          "A company already exists with this phone number."
        end

        let(:where_double) { double }

        before :each do
          allow(RescueCompany).to receive(:where).and_return where_double
          allow(where_double).to receive_message_chain(:where, :not).and_return [rescue_company]
        end

        it 'returns http status 409' do
          post_new_company

          expect(response.status).to eql(409)
        end

        it 'responds with the correct error message' do
          post_new_company

          expect(JSON.parse(response.body)).to eql("phone" => [error_message])
        end

        it 'doesnt instantiate a CompanyService::Create class' do
          post_new_company

          expect(CompanyService::Create).not_to receive(:new)
        end
      end

      context "with a network manager" do
        it "sets the network manager" do
          network_manager = create(:user, :root)
          post :create, params: { company: new_company_attributes.merge(network_manager_id: network_manager.id) }, as: :json
          expect(created_company.reload.network_manager).to eq network_manager
        end
      end
    end
  end

  describe '#update' do
    def updated_company
      Company.find(JSON.parse(response.body)['id'])
    end

    stub_user_login(:root)

    shared_examples "update with settings_attributes" do
      it "works" do
        company.reload
        expect(company.settings.map(&:key)).to match_array(Setting::SETTINGS_MAP.values)
        expect(company.settings.map(&:value)).to match_array(settings.values)
      end
    end

    context 'when existent company is RescueCompany' do
      subject(:update_company) do
        patch :update, params: {
          id: rescue_company.id,
          company: updated_company_attributes,
        }, as: :json
      end

      let(:updated_company_attributes) do
        {
          id: 1,
          type: 'RescueCompany',
          phone: '+13045551231',
          invoice_charge_fee: invoice_charge_fee_params,
        }
      end

      let(:invoice_charge_fee_params) do
        {
          percentage: 10,
          fixed_value: 0.5,
          payout_interval: 'monthly',
        }
      end

      let!(:company_invoice_charge_fee) do
        create(
          :company_invoice_charge_fee,
          company: rescue_company,
          percentage: 1,
          fixed_value: 2,
          payout_interval: 'weekly'
        )
      end

      let(:rescue_company) { build(:rescue_company, name: 'Common RescueCompany') }
      let!(:custom_account) { create(:custom_account, company: rescue_company) }
      let(:duplicated_rescue_company) { build(:rescue_company, name: 'Duplicated RescueCompany') }

      context 'language' do
        it 'is set to English' do
          updated_company_attributes[:language] = Company::ENGLISH
          update_company
          expect(rescue_company.language).to eq(Company::ENGLISH)
        end

        it 'is set to German' do
          updated_company_attributes[:language] = Company::GERMAN
          update_company
          expect(rescue_company.reload.language).to eq(Company::GERMAN)
        end

        it "is not set to Spanish" do
          updated_company_attributes[:language] = 'es'
          update_company
          expect(response.status).to eq(422)
          expect(JSON.parse(response.body)).to eq({ "language" => ["es is not supported."] })
        end

        it 'does not erase valid language' do
          updated_company_attributes[:language] = ''
          update_company
          expect(response.status).to eq(422)
          expect(JSON.parse(response.body)).to eq({ "language" => ["can't be blank"] })
        end
      end

      context 'currency' do
        it 'does not accept blank' do
          updated_company_attributes[:currency] = ''
          update_company
          expect(response.status).to eq(422)
          expect(JSON.parse(response.body)).to eq({ "currency" => ["can't be blank"] })
        end

        it 'accepts only EUR/USD' do
          updated_company_attributes[:currency] = 'KZT'
          update_company
          expect(response.status).to eq(422)
          expect(JSON.parse(response.body)).to eq({ "currency" => ["KZT is not supported."] })
        end

        it 'is EUR' do
          updated_company_attributes[:currency] = 'EUR'
          update_company
          expect(response.status).to eq(200)
          expect(rescue_company.reload.currency).to eq('EUR')
        end

        it 'is USD' do
          updated_company_attributes[:currency] = 'USD'
          update_company
          expect(response.status).to eq(200)
          expect(rescue_company.reload.currency).to eq('USD')
        end

        context 'when swoop dispatcher' do
          stub_user_login(:dispatcher)

          it 'is forbidden' do
            updated_company_attributes[:currency] = 'EUR'
            update_company
            expect(response.status).to eq(403)
          end
        end
      end

      context 'sets distance unit' do
        it 'does not accept blank' do
          updated_company_attributes[:distance_unit] = ''
          update_company
          expect(response.status).to eq(422)
          expect(JSON.parse(response.body)).to eq({ "distance_unit" => ["can't be blank"] })
        end

        it 'does not accept Furlong' do
          updated_company_attributes[:distance_unit] = 'Furlong'
          update_company
          expect(response.status).to eq(422)
          expect(JSON.parse(response.body)).to eq({ "distance_unit" => ["Furlong is not supported."] })
        end

        it 'to Mile' do
          updated_company_attributes[:distance_unit] = Company::MILE
          update_company
          expect(response.status).to eq(200)
          expect(RescueCompany.last&.distance_unit).to eq(Company::MILE)
        end

        it 'to Kilometer' do
          updated_company_attributes[:distance_unit] = Company::KILOMETER
          update_company
          expect(response.status).to eq(200)
          expect(RescueCompany.last&.distance_unit).to eq(Company::KILOMETER)
        end

        context 'when swoop dispatcher' do
          stub_user_login(:dispatcher)

          it 'is forbidden' do
            updated_company_attributes[:distance_unit] = Company::KILOMETER
            update_company
            expect(response.status).to eq(403)
          end
        end
      end

      context 'with invoice_charge_fee_params' do
        before do
          update_company
        end

        def invoice_charge_fee
          updated_company.invoice_charge_fee
        end

        it 'updates invoice_charge_fee accordingly' do
          expect(invoice_charge_fee.percentage).to eq 10
          expect(invoice_charge_fee.fixed_value.to_s).to eq '0.5'
          expect(invoice_charge_fee.payout_interval).to eq 'monthly'

          expect(Stripe::UpdatePayoutScheduleWorker).to have_enqueued_sidekiq_job(rescue_company.custom_account.id, 'monthly')
        end

        context 'when invoice_charge_fee is passed as nil' do
          let(:invoice_charge_fee_params) { nil }

          it 'does not update invoice_charge_fee' do
            expect(invoice_charge_fee.percentage).to eq 1
            expect(invoice_charge_fee.fixed_value.to_s).to eq '2.0'
            expect(invoice_charge_fee.payout_interval).to eq 'weekly'

            expect(Stripe::UpdatePayoutScheduleWorker.jobs.size).to eq 0
          end
        end

        context 'when invoice_charge_fee is not passed at all' do
          let(:updated_company_attributes) do
            {
              id: 1,
              type: 'RescueCompany',
              phone: '+13045551231',
            }
          end

          it 'does not update invoice_charge_fee' do
            expect(invoice_charge_fee.percentage).to eq 1
            expect(invoice_charge_fee.fixed_value.to_s).to eq '2.0'
            expect(invoice_charge_fee.payout_interval).to eq 'weekly'

            expect(Stripe::UpdatePayoutScheduleWorker.jobs.size).to eq 0
          end
        end

        context 'when only invoice_charge_fee.percentage is passed' do
          let(:invoice_charge_fee_params) do
            {
              percentage: 3.4,
            }
          end

          it 'updates only invoice_charge_fee.percentage' do
            expect(invoice_charge_fee.percentage).to eq 3.4
            expect(invoice_charge_fee.fixed_value.to_s).to eq '2.0'
            expect(invoice_charge_fee.payout_interval).to eq 'weekly'

            expect(Stripe::UpdatePayoutScheduleWorker.jobs.size).to eq 0
          end
        end

        context 'when only invoice_charge_fee.fixed_value is passed' do
          let(:invoice_charge_fee_params) do
            {
              fixed_value: 2.78,
            }
          end

          it 'updates only invoice_charge_fee.fixed_value' do
            expect(invoice_charge_fee.percentage).to eq 1
            expect(invoice_charge_fee.fixed_value.to_s).to eq '2.78'
            expect(invoice_charge_fee.payout_interval).to eq 'weekly'

            expect(Stripe::UpdatePayoutScheduleWorker.jobs.size).to eq 0
          end
        end

        context 'when only invoice_charge_fee.payout_interval is passed' do
          let(:invoice_charge_fee_params) do
            {
              payout_interval: 'monthly',
            }
          end

          it 'updates only invoice_charge_fee.payout_interval' do
            expect(invoice_charge_fee.percentage).to eq 1
            expect(invoice_charge_fee.fixed_value.to_s).to eq '2.0'
            expect(invoice_charge_fee.payout_interval).to eq 'monthly'

            expect(Stripe::UpdatePayoutScheduleWorker).to have_enqueued_sidekiq_job(rescue_company.custom_account.id, 'monthly')
          end
        end
      end

      context "with settings_attributes" do
        let(:updated_company_attributes) do
          {
            **super(),
            **settings,
          }
        end
        let(:company) { rescue_company }

        before { update_company }

        it_behaves_like "update with settings_attributes"
      end

      context 'when duplicated phone number' do
        let(:error_message) do
          "A company already exists with this phone number."
        end

        let(:where_double) { double }

        before :each do
          allow(RescueCompany).to receive(:where).and_return where_double
          allow(where_double).to receive_message_chain(:where, :not).and_return [duplicated_rescue_company]
          allow(Company).to receive(:find_by!).and_return rescue_company
        end

        it 'returns http status 409' do
          update_company

          expect(response.status).to eql(409)
        end

        it 'responds with the correct error message' do
          update_company

          expect(JSON.parse(response.body)).to eql("phone" => [error_message])
        end
      end

      context 'when existent company is the same that is being updated' do
        let(:where_double) { double }

        before :each do
          allow(RescueCompany).to receive(:where).and_return where_double
          allow(where_double).to receive_message_chain(:where, :not).and_return []
          allow(Company).to receive(:find_by!).and_return rescue_company
          allow(rescue_company).to receive(:update).and_return true
        end

        it 'returns http status 409' do
          update_company

          expect(response.status).to eql(200)
        end
      end

      context "with a network manager" do
        it "sets the network manager" do
          network_manager = create(:user, :root)
          patch :update, params: { id: rescue_company.id, company: updated_company_attributes.merge(network_manager_id: network_manager.id) }, as: :json
          expect(rescue_company.reload.network_manager).to eq network_manager
        end
      end
    end

    context 'when existent company is a FleetManaged company' do
      subject(:update_company) do
        patch :update, params: { id: fleet_managed_company.id, company: company_attributes_to_update }, as: :json
      end

      let(:company_attributes_to_update) do
        {
          id: fleet_managed_company.id,
          dispatch_to_all_partners: dispatch_to_all_partners_toggle,
        }
      end

      let(:fleet_managed_company) { create(:fleet_company) }

      stub_user_login(:root)

      context "with settings_attributes" do
        let(:company_attributes_to_update) do
          {
            id: fleet_managed_company.id,
            **settings,
          }
        end
        let(:company) { fleet_managed_company }

        before { update_company }

        it_behaves_like "update with settings_attributes"
      end

      context 'when add_dispatch_to_all_partners is toggled on' do
        let(:dispatch_to_all_partners_toggle) { true }

        it 'adds calls dispatch_to_all_partners db function' do
          expect(ApplicationRecord).to receive(:execute_query).with("SELECT dispatch_to_all_partners(?)", fleet_managed_company.id).and_call_original
          update_company
        end
      end

      context 'when add_dispatch_to_all_partners is toggled off' do
        let(:dispatch_to_all_partners_toggle) { false }

        it 'set all own rescue_providers as deleted' do
          create(:rescue_provider, :default, { company: fleet_managed_company })
          create(:rescue_provider, :default, { company: fleet_managed_company })
          create(:rescue_provider, :default, { company: fleet_managed_company })

          deleted_rescue_providers = RescueProvider
            .where(company: fleet_managed_company)
            .where.not(deleted_at: nil)

          expect(deleted_rescue_providers.size).to eq 0

          update_company

          deleted_rescue_providers = RescueProvider
            .where(company: fleet_managed_company)
            .where.not(deleted_at: nil)

          expect(deleted_rescue_providers.size).to eq 3
        end
      end
    end

    describe "updates associated features" do
      let(:feature) { create :feature }
      let(:company) { create :company }

      it "removes features that are not passed as params for the company" do
        company.features << feature
        company.save!

        expect(company.reload.features).to include(feature)
        patch :update, params: { id: company.id, company: { features: [] } }, as: :json

        expect(company.reload.features).not_to include(feature)
      end

      it "adds new features to the company" do
        expect(company.features.size).to be(0)
        patch :update,
              params: { id: company.id, company: { features: [feature.id] } }, as: :json
        expect(company.reload.features).to include(feature)
      end
    end

    describe "updates associated customer types" do
      let(:customer_type) { create :customer_type }
      let(:company) { create :company }

      it "removes customer types that are not passed as params for the company" do
        company.customer_types << customer_type
        company.save!

        expect(company.reload.customer_types).to include(customer_type)
        patch :update, params: { id: company.id, company: { customer_type_ids: [] } }, as: :json
        expect(company.reload.customer_types).not_to include(customer_type)
      end

      it "adds new customer type to the company" do
        expect(company.customer_types.size).to be(0)

        patch :update, params: { id: company.id, company: { customer_type_ids: [customer_type.id] } }, as: :json

        expect(company.reload.customer_types).to include(customer_type)
      end
    end
  end

  describe '#show' do
    stub_user_login(:root)

    let(:response_body) { JSON.parse(response.body) }
    let(:company) { create(:company) }

    before do
      get :show, params: { id: company.id }, as: :json
    end

    it 'includes localization settings' do
      expect(response_body['distance_unit']).to eq(company.distance_unit)
      expect(response_body['currency']).to eq(company.currency)
      expect(response_body['language']).to eq(company.language)
    end
  end
end
