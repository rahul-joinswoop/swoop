# frozen_string_literal: true

require 'rails_helper'

describe API::V1::Root::AutocompleteController do
  render_views

  describe "POST root_users" do
    context "non-root user" do
      stub_user_login(:rescue)

      it "rejects requests from non-root users" do
        post :root_users, params: { autocomplete: { term: "joe" } }
        expect(response.status).to eq 403
      end
    end

    context "root user" do
      stub_user_login(:root, full_name: "Random Root")

      it "returns user id and name of root users" do
        create(:user, full_name: "Joe Alpha")
        user_2 = create(:user, :root, full_name: "Joe Bravo")
        create(:user, :root, full_name: "John Charlie")
        post :root_users, params: { autocomplete: { term: "joe" } }
        expect(response.status).to eq 200
        expect(JSON.load(response.body)).to match_array [{ "full_name" => user_2.full_name, "id" => user_2.id }]
      end
    end
  end
end
