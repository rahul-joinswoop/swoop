# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::InvoicesController, type: :controller do
  render_views

  include Requests::InvoiceHelpers

  stub_user_login(:root)
  let(:account) { create :account, company: company, client_company: fleet_company }
  let(:company) { create :rescue_company }
  let(:fleet_company) { create :fleet_company }

  let(:invoice) do
    build(
      :invoice,
      state: 'partner_new',
      sender: company,
      recipient: account,
      recipient_company: account.client_company,
    )
  end

  describe "#create" do
    subject(:create_invoice) do
      post(
        :create,
        params: {
          company_id: company.id,
          invoice: invoice,
        },
        as: :json
      )
    end

    let!(:job) do
      create(:rescue_job, {
        rescue_company: company,
        account: account,
        service_code: ServiceCode.find_by(name: 'Tow'),
      })
    end

    it "works" do
      expect(subject.status).to eq(201)
    end
  end

  describe "#update" do
    subject(:update_invoice_charge) do
      patch(
        :update,
        params: {
          company_id: company.id,
          id: invoice.id,
          invoice: {
            sender: {
              role_permissions: company.role_permissions,
              id: company.id,
            },
            job: {
              id: job.id,
              driver_notes: driver_notes,
            },
          },
        },
        as: :json
      )
    end

    let(:driver_notes) { Faker::Hipster.sentence }
    let!(:job) do
      create(:rescue_job, {
        rescue_company: company,
        invoice: invoice,
        account: account,
        service_code: create(:service_code, name: 'Tow'),
      })
    end

    before do
      invoice.job = job
      invoice.save!
    end

    it "works" do
      expect(subject.status).to eq(200)
      expect { job.reload }.to change(job, :driver_notes).to(driver_notes)
    end
  end

  describe "#download_fleet" do
    subject(:post_download_fleet) do
      post(
        :download_fleet,
        params: {
          report: {
            client_company_id: client_company.id,
            job_active: false,
          },
        },
        as: :json
      )
    end

    let(:client_company) { create(:fleet_company) }

    let!(:swoop_invoices) do
      create_list(
        :invoice,
        2,
        state: 'swoop_approved_fleet',
        sender: controller.api_company,
        recipient: swoop_and_fleet_account,
        recipient_company: swoop_and_fleet_account.client_company
      )
    end

    let(:swoop_and_fleet_account) do
      create(
        :account,
        name: 'Fleet Response',
        company: controller.api_company,
        client_company: client_company
      )
    end

    it 'moves the invoices state to swoop_sending_fleet' do
      post_download_fleet

      swoop_invoices.first.reload
      swoop_invoices.second.reload

      expect(swoop_invoices.first.state).to eq Invoice::SWOOP_SENDING_FLEET
      expect(swoop_invoices.second.state).to eq Invoice::SWOOP_SENDING_FLEET
    end

    it 'schedule InvoiceService::GenerateSwoopFleetInvoiceReportWorker' do
      expect(InvoiceService::GenerateSwoopFleetInvoiceReportWorker).to receive(:perform_async)

      post_download_fleet
    end

    it 'triggers WS send with all fetched invoices' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call)

      post_download_fleet
    end
  end

  describe "#download_all_partner" do
    subject(:post_download_all_partner) do
      post(
        :download_all_partner,
        params: {
          report: {
            client_company_id: controller.api_company.id,
            sender_id: rescue_company.id,
            job_active: false,
          },
        },
        as: :json
      )
    end

    let(:rescue_company) { create(:rescue_company) }

    let!(:partner_invoices) do
      create_list(
        :invoice,
        2,
        state: 'swoop_approved_partner',
        sender: rescue_company,
        recipient: partner_and_swoop_account,
        recipient_company: controller.api_company
      )
    end

    let(:partner_and_swoop_account) do
      create(
        :account,
        name: 'Partner and Swoop',
        company: rescue_company,
        client_company: controller.api_company
      )
    end

    it 'moves the invoices state to swoop_downloading_partner' do
      post_download_all_partner

      partner_invoices.first.reload
      partner_invoices.second.reload

      expect(partner_invoices.first.state).to eq Invoice::SWOOP_DOWNLOADING_PARTNER
      expect(partner_invoices.second.state).to eq Invoice::SWOOP_DOWNLOADING_PARTNER
    end

    it 'schedule InvoiceService::GenerateSwoopPartnerInvoiceReportWorker' do
      expect(InvoiceService::GenerateSwoopPartnerInvoiceReportWorker).to receive(:perform_async)

      post_download_all_partner
    end

    it 'triggers WS send with all fetched invoices' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call)

      post_download_all_partner
    end
  end

  describe "search" do
    it "works" do
      get :search, params: { original: 'lincoln', filters: { invoice_state: "partner_new" } }, as: :json
      expect(response).to be_successful
      payload = JSON.load(response.body)
      expect(payload["invoices"]).to eq []
      expect(payload["total"]).to eq 0
    end
  end
end
