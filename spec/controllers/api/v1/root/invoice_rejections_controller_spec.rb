# frozen_string_literal: true

require "rails_helper"
require_relative "../shared_examples_for_invoice_rejections_controllers"

describe API::V1::Root::InvoiceRejectionsController, type: :controller do
  describe "POST create" do
    context "when user is Swoop" do
      subject(:post_reject_invoice) do
        post :create, params: params, as: :json
      end

      let!(:invoice) { create :fleet_managed_job_invoice, state: "partner_sent" }
      let(:invoice_reject_reason) do
        create :invoice_reject_reason, company: user.company
      end
      let(:params) do
        {
          invoice_rejection: {
            invoice_id: invoice.id,
            invoice_reject_reason_id: invoice_reject_reason.id,
            notes: "Approval rejected",
          },
        }
      end

      stub_user_login(:root)

      it_behaves_like "API::V1::Shared::InvoiceRejections#create"

      it "moves given invoice to :swoop_reject_partner state" do
        expect(invoice.state).to eql("partner_sent")
        post_reject_invoice
        expect(invoice.reload.state).to eql("swoop_rejected_partner")
      end

      context "when the invoice can't move to :swoop_reject_partner state" do
        it "returns an error" do
          invoice.swoop_approved_partner!
          post_reject_invoice
          expect(response.status).to eql(422)
        end
      end

      it "runs within a transaction" do
        invoice.swoop_reject_partner!
        expect(invoice.reload.state).to eql("swoop_rejected_partner")
        expect { post_reject_invoice }.to avoid_changing(InvoiceRejection, :count)
      end
    end

    it "returns an authorization error when not Swoop" do
      post :create, as: :json
      expect(response.status).to eql(403)
    end
  end

  describe "GET reasons" do
    render_views

    let!(:company_specific_reason) do
      create :invoice_reject_reason, company: user.company, text: "First reason"
    end

    stub_user_login(:root)

    it "returns a list of reasons associated with Swoop" do
      create :invoice_reject_reason, company: create(:fleet_company, :tesla)

      get :reasons, as: :json

      expect(JSON.parse(response.body)).to eql([
        "id" => company_specific_reason.id,
        "text" => "First reason",
      ])
    end
  end
end
