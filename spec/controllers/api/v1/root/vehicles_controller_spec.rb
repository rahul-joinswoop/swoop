# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::VehiclesController, type: :controller do
  render_views

  context "as a root user" do
    stub_user_login(:root)
    let!(:rescue_vehicles) { create_list :rescue_vehicle, 10 }

    describe "GET index" do
      subject { get :index, format: :json, params: params }

      context "without pagination" do
        let(:params) { {} }

        it "works" do
          expect { subject }.not_to raise_error
          expect(response).to have_http_status(:ok)
          expect(JSON[response.body].map { |v| v['id'] })
            .to contain_exactly(*rescue_vehicles.map(&:id))
        end
      end

      context "with pagination" do
        let(:params) { { page: 1, per_page: 5 } }

        it "works" do
          expect { subject }.not_to raise_error
          expect(response).to have_http_status(:ok)
          expect(JSON[response.body].map { |v| v['id'] })
            .to contain_exactly(*RescueVehicle.where({ deleted_at: nil }).includes(:driver).take(5).map(&:id))
        end
      end
    end
  end
end
