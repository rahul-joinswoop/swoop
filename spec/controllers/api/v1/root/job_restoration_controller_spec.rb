# frozen_string_literal: true

require "rails_helper"

describe API::V1::Root::JobRestorationController, type: :controller do
  stub_user_login(:root)

  describe 'PATCH /root/jobs/:id/restore' do
    subject do
      patch :create, params: params
      response
    end

    let(:params) do
      { job_id: job.id, format: :json }
    end

    context 'when job exists' do
      let!(:job) { FactoryBot.create(:fleet_managed_job, status: :draft) }

      before do
        SwoopUpdateJobState.new(job, 'Canceled').call
        job.save!

        SwoopUpdateJobState.new(job, 'Deleted').call
        job.save!
      end

      it { is_expected.to have_http_status(200) }

      it 'returns job' do
        subject

        expect(json['job']).to be_present
      end
    end
  end
end
