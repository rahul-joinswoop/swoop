# frozen_string_literal: true

require "rails_helper"

describe API::V1::ProvidersController, type: :controller do
  render_views

  shared_examples 'agero vendor id is included' do
    it 'includes agero vendor id' do
      companies = json.collect { |rp| rp['company'] }
      expect(companies.flat_map(&:keys)).to include('agero_vendor_id')
      expect(companies.collect { |c| c['agero_vendor_id'] }).to include(rescue_company.agero_vendor_id)
    end
  end

  describe 'GET index' do
    subject(:get_index) { get :index, as: :json }

    let!(:rescue_company) { create(:rescue_company) }

    let!(:site) { create(:site, company: rescue_company, location: site_location) }
    let!(:site_location) { create(:location, address: '200, Cool st') }

    let!(:site_for_rp_with_location) do
      create(:site, company: rescue_company, location: site_location)
    end
    let!(:rescue_provider_location) { create(:location, address: '201, RescueProvider st') }

    context 'when it is Swoop user' do
      stub_user_login(:root)

      let!(:rescue_provider) do
        create(
          :rescue_provider,
          provider: rescue_company,
          company: user.company,
          site: site,
          location: nil
        )
      end

      let!(:rescue_provider_with_location) do
        create(
          :rescue_provider,
          provider: rescue_company,
          company: user.company,
          site: site_for_rp_with_location,
          location: rescue_provider_location
        )
      end

      before do
        # call route
        get_index
      end

      it 'fetches available RescueProviders' do
        expect(json).to be_present
        expect(response.code).to eq "200"
      end

      it 'comes with site address in the location field' do
        rescue_provider_from_json = json.find { |rp| rp["id"] == rescue_provider.id }

        expect(rescue_provider_from_json["location"]).to be_present
        expect(rescue_provider_from_json["location"]["address"]).to eq '200, Cool st'
      end

      context 'when RescueProvider has an address set' do
        it 'comes with rescue_provider address' do
          rescue_provider_from_json = json.find do |rp|
            rp["id"] == rescue_provider_with_location.id
          end

          expect(rescue_provider_from_json["location"]).to be_present
          expect(rescue_provider_from_json["location"]["address"]).to eq '201, RescueProvider st'
        end
      end

      it_behaves_like 'agero vendor id is included'
    end

    context 'when it is FleetInHouse user' do
      stub_user_login(:tesla)

      let!(:rescue_provider) do
        create(
          :rescue_provider,
          provider: rescue_company,
          company: user.company,
          site: site,
          location: nil
        )
      end

      let!(:rescue_provider_with_location) do
        create(
          :rescue_provider,
          provider: rescue_company,
          company: user.company,
          site: site_for_rp_with_location,
          location: rescue_provider_location
        )
      end

      before do
        # call route
        get_index
      end

      it 'fetches available RescueProviders' do
        expect(json).to be_present
        expect(response.code).to eq "200"
      end

      it 'comes with site address in the location field' do
        rescue_provider_from_json = json.find { |rp| rp["id"] == rescue_provider.id }

        expect(rescue_provider_from_json["location"]).to be_present
        expect(rescue_provider_from_json["location"]["address"]).to eq '200, Cool st'
      end

      context 'when RescueProvider has an address set' do
        it 'comes with rescue_provider address' do
          rescue_provider_from_json = json.find do |rp|
            rp["id"] == rescue_provider_with_location.id
          end

          expect(rescue_provider_from_json["location"]).to be_present
          expect(rescue_provider_from_json["location"]["address"]).to eq '201, RescueProvider st'
        end
      end

      it_behaves_like 'agero vendor id is included'
    end
  end
end
