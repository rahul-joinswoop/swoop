# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::SignupController, type: :controller do
  render_views

  def created_company
    Company.find(JSON.parse(response.body)['id'])
  end

  before do
    standard_services_for_spec.each do |service_name|
      ServiceCode.where(name: service_name).first_or_create
    end

    standard_addons_for_spec.each do |addon_name|
      ServiceCode.where(name: addon_name).first_or_create
    end

    SuperCompany.where(name: 'Swoop').first_or_create!
    allow_any_instance_of(CompanyService::ChangeFeatures).to receive(
      :remove_storage_service_from_company_if_needed
    )
    allow_any_instance_of(RescueCompany).to receive(:defaults)

    allow(Delayed::Job).to receive(:enqueue)

    request.accept = 'application/json'
  end

  let!(:admin_role) { create(:role, :admin) }
  let!(:dispatcher_role) { create(:role, :dispatcher) }
  let!(:super_root_user) do
    u = create(:user, :root)
    u
  end

  describe 'POST create' do
    subject(:post_company) do
      post :create, params: {
        company: {
          name: "NewCo1",
          phone: "+14155559302",
          location: {
            lat: 37.7548833,
            lng: -122.4212138,
            place_id: "ChIJcx8DAD9-j4ARClnAM476u1M",
            address: "1120 Valencia Street, San Francisco, CA",
            exact: true,
          },
        }, user: {
          first_name: "User",
          last_name: "of NewCo1",
          email: "user_of_new_co_1@joinswoop.com",
          username: "newco1_user",
          password: "123123123",
          referer: "Advertisement",
        },
      }
    end

    it 'returns 201 response code' do
      post_company
      expect(response.status).to eq(201)
    end

    it 'creates a new Company called NewCo1' do
      post_company
      json = JSON.load(response.body)
      company = Company.find(json["id"])
      expect(company.name).to eq "NewCo1"
    end

    it 'creates a new User called User of NewCo1' do
      post_company
      json = JSON.load(response.body)
      user = User.find(json["user_id"])
      expect(user.email).to eq "user_of_new_co_1@joinswoop.com"
    end

    it 'triggers async EmailPartnerSignup job' do
      expect(Delayed::Job).to receive(:enqueue)

      post_company
    end

    it 'adds default invoice charge fee' do
      post_company

      invoice_charge_fee = created_company.invoice_charge_fee

      expect(invoice_charge_fee).to be_present
      expect(invoice_charge_fee.percentage).to be_present
      expect(invoice_charge_fee.fixed_value).to be_present
      expect(invoice_charge_fee.payout_interval).to be_present
    end

    it 'adds default role permissions' do
      post_company

      role_permissions = created_company.role_permissions

      expect(role_permissions.size).to eq 3

      charge_permissions = role_permissions.where(permission_type: 'charge')
      expect(charge_permissions.size).to eq 2

      admin_permission = charge_permissions.find_by(role_id: admin_role.id)
      expect(admin_permission).to be_present

      dispatcher_permission = charge_permissions.find_by(role_id: dispatcher_role.id)
      expect(dispatcher_permission).to be_present

      refund_permissions = role_permissions.where(permission_type: 'refund')
      expect(refund_permissions.size).to eq 1

      admin_permission = refund_permissions.find_by(role_id: admin_role.id)
      expect(admin_permission).to be_present
    end

    shared_context "not creating the company and the user" do |expected_error_hash|
      it 'returns 422 response code' do
        post_company

        expect(response.status).to eq 422
      end

      it 'does not create a new company' do
        expect { post_company }.not_to change(Company, :count)
      end

      it 'does not create a new user' do
        expect { post_company }.not_to change(User, :count)
      end

      it 'responds with the error' do
        post_company

        expect(json).to match(expected_error_hash)
      end
    end

    context "when company already exists" do
      before do
        create(:rescue_company, name: "NewCo1", phone: "+14155559302")
      end

      it_behaves_like "not creating the company and the user", {
        "company.phone" => ["A company already exists with this phone number."],
      }
    end

    context "when user already exists" do
      before do
        create(:user, username: "newco1_user", email: "user_of_new_co_1@joinswoop.com")
      end

      it_behaves_like "not creating the company and the user", {
        "user.email" => ["has already been taken"],
        "user.username" => ["has already been taken"],
      }
    end
  end
end
