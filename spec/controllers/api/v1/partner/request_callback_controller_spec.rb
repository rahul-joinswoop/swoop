# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::RequestCallbackController, type: :controller do
  render_views

  stub_user_login(:rescue)

  describe "POST update", freeze_time: true do
    subject(:post_request_callback) do
      post :create, params: {
        id: job.id,
        dispatch_number: '5555555',
        secondary_number: '5555566',
        format: :json,
      }
    end

    context 'when it is a FleetMotorClubJob' do
      let!(:account) do
        create(:account, company: controller.api_company, client_company: fleet_company)
      end

      let(:job) do
        create :fleet_motor_club_job, {
          status: job_status,
          fleet_company: fleet_company,
          adjusted_created_at: Time.now,
          rescue_company: controller.api_company,
          issc_dispatch_request: issc_dispatch_request,
          answer_by: Time.now + 90,
        }
      end

      let(:issc_dispatch_request) do
        create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
      end

      context 'and job.digital_dispatcher is ISSC' do
        let(:issc) { create(:ago_issc, company: controller.api_company, api_access_token: nil) }

        context 'and fleet_company is Agero' do
          let(:fleet_company) { create(:fleet_company, :ago) }

          context 'and job status is :assigned' do
            let(:job_status) { :assigned }

            it 'responds 204 no content' do
              post_request_callback

              expect(response.code).to eq "204"
            end

            it 'changes job.partner_dispatcher from nil to api_user' do
              expect { post_request_callback && job.reload }.to change(job, :partner_dispatcher)
                .from(nil).to(controller.api_user)
            end

            it 'changes job.answer_by from Time to nil' do
              expect { post_request_callback && job.reload }.to change(job, :answer_by)
                .from(Time).to(nil)
            end

            it 'changes job.status from assigned to pending' do
              expect { post_request_callback && job.reload }.to change(job, :status)
                .from(Job::STATUS_ASSIGNED).to(Job::STATUS_PENDING)
            end

            it 'schedules a IsscDispatchResponseCallback job' do
              expect(IsscDispatchResponseCallback).to receive(:perform_async).with(
                job.id, :Automated
              )

              post_request_callback
            end
          end
        end
      end

      context 'and job.digital_dispatcher is RSC' do
        let(:api_access_token) { create(:api_access_token) }
        let(:issc) do
          create(
            :ago_issc,
            company: controller.api_company,
            api_access_token: api_access_token,
            system: Issc::RSC_SYSTEM
          )
        end

        context 'and fleet_company is Agero' do
          let(:fleet_company) { create(:fleet_company, :ago) }

          context 'and job status is :assigned' do
            let(:job_status) { :assigned }

            it 'responds 204 no content' do
              post_request_callback

              expect(response.code).to eq "204"
            end

            it 'changes job.partner_dispatcher from nil to api_user' do
              expect { post_request_callback && job.reload }.to change(job, :partner_dispatcher)
                .from(nil).to(controller.api_user)
            end

            it 'changes job.answer_by from Time to nil' do
              expect { post_request_callback && job.reload }.to change(job, :answer_by)
                .from(Time).to(nil)
            end

            it 'changes job.status from assigned to pending' do
              expect { post_request_callback && job.reload }.to change(job, :status)
                .from(Job::STATUS_ASSIGNED).to(Job::STATUS_PENDING)
            end

            it 'schedules an Agero::Rsc::RequestCallBackWorker job' do
              expect(Agero::Rsc::RequestCallBackWorker).to receive(:perform_async).with(
                job.id, { dispatch_number: '5555555', secondary_number: '5555566' }
              )

              post_request_callback
            end
          end
        end
      end
    end
  end
end
