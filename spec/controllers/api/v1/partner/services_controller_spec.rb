# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::ServicesController, type: :controller do
  render_views

  describe "GET index_by_rates_filter" do
    stub_user_login(:rescue)

    subject(:get_index_by_rates_filter) do
      get(
        :index_by_rates_filter,
        params: {
          account_id: account_id,
          site_id: site_id,
          vendor_id: vendor_id,
        },
        as: :json
      )
    end

    let(:site_for_rate) { create(:site, company: rescue_company) }

    let(:vendor_id_for_rate) { "CA12345" }
    let(:rescue_company) { user.company }

    let(:service_code_one) { create(:service_code, name: ServiceCode::TOW) }
    let(:service_code_two) { create(:service_code, name: ServiceCode::BATTERY_JUMP) }

    let(:account_id) { account_with_rates.id }
    let(:account_with_rates) do
      create(:account, company: rescue_company, name: 'Account with Rate')
    end

    let(:rate_account_level_one) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site: nil,
        vendor_id: nil,
        service_code_id: service_code_one.id,
        hourly: 32.0
      )
    end

    let(:rate_account_level_two) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site: nil,
        vendor_id: nil,
        service_code_id: service_code_two.id,
        hourly: 32.0
      )
    end

    let(:rate_site_level) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site_id: site_for_rate.id,
        vendor_id: nil,
        service_code_id: service_code_one.id,
        hourly: 32.0
      )
    end

    let(:rate_vendor_id_level) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site_id: site_for_rate.id,
        vendor_id: vendor_id_for_rate,
        service_code_id: service_code_one.id,
        hourly: 32.0
      )
    end

    before do
      # add services to company
      rescue_company.services << service_code_one
      rescue_company.services << service_code_two

      # create rates
      rate_account_level_one
      rate_account_level_two
      rate_site_level
      rate_vendor_id_level
    end

    context 'when only account_id is passed' do
      let(:site_id) { nil }
      let(:vendor_id) { nil }

      it 'responds with services bound with rates for the given account_id' do
        get_index_by_rates_filter

        services_response = JSON.parse(response.body)

        service_one_found = services_response.find do |service|
          service["id"] == service_code_one.id
        end

        service_two_found = services_response.find do |service|
          service["id"] == service_code_two.id
        end

        expect(service_one_found).to be_present
        expect(service_two_found).to be_present
      end
    end

    context 'when account_id and site_id are passed' do
      let(:site_id) { site_for_rate.id }
      let(:vendor_id) { nil }

      it 'responds with services bound with rates for the given account_id and site_id' do
        get_index_by_rates_filter

        services_response = JSON.parse(response.body)

        service_one_found = services_response.find do |service|
          service["id"] == service_code_one.id
        end

        service_two_found = services_response.find do |service|
          service["id"] == service_code_two.id
        end

        expect(service_one_found).to be_present
        expect(service_two_found).to be_blank
      end
    end

    context 'when account_id, site_id and vendor_id are passed' do
      let(:site_id) { site_for_rate.id }
      let(:vendor_id) { "CA12345" }

      it "responds with services bound with rates for the given account_id, " \
      "site_id and vendor_id" do
        get_index_by_rates_filter

        services_response = JSON.parse(response.body)

        service_one_found = services_response.find do |service|
          service["id"] == service_code_one.id
        end

        service_two_found = services_response.find do |service|
          service["id"] == service_code_two.id
        end

        expect(service_one_found).to be_present
        expect(service_two_found).to be_blank
      end
    end
  end
end
