# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::InvoiceRejectionsController, type: :controller do
  describe "GET index" do
    render_views

    let(:invoice_one) { create :fleet_managed_job_invoice, state: "partner_sent" }
    let(:invoice_two) { create :fleet_in_house_job_invoice, state: "partner_sent" }
    let!(:invoice_rejection_one) do
      create :invoice_rejection, invoice: invoice_one
    end
    let!(:invoice_rejection_two) do
      create :invoice_rejection, invoice: invoice_two
    end

    stub_user_login(:rescue)

    before do
      invoice_one.job.update! rescue_company: user.company
      invoice_two.job.update! rescue_company: user.company
    end

    it "returns an array of requested invoice rejections" do
      invoice_ids_list = [invoice_one, invoice_two].map(&:id).join(",")

      get :index, params: { invoice_ids: invoice_ids_list, format: :json }

      expect(JSON.parse(response.body)).to include(
        {
          "assigning_company_name" => "Swoop",
          "reject_reason" => "Incorrect base rate",
          "notes" => "Is not correct",
          "invoice_id" => invoice_one.id,
        },
        {
          "assigning_company_name" => "Fleet Demo",
          "reject_reason" => "Incorrect base rate",
          "notes" => "Is not correct",
          "invoice_id" => invoice_two.id,
        }
      )
    end

    it "returns an empty array when invoice rejections do not exist" do
      get :index, params: { invoice_ids: "21321", format: :json }
      expect(JSON.parse(response.body)).to eql([])
    end
  end
end
