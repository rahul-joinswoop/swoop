# frozen_string_literal: true

require 'rails_helper'

describe API::V1::Partner::AccountsController, type: :controller do
  let!(:rescue_company) { create(:rescue_company, :finish_line) }
  let!(:geico) { create(:fleet_company, :geico) }
  let!(:swoop) { create(:super_company) }
  let(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }
  let!(:user) { create :user, :rescue_with_sites }

  before(:each) do
    controller.request.env["HTTP_AUTHORIZATION"] = "Bearer #{token.token}"
  end

  describe "POST #update" do
    context "when dispatchable_sites has a new contractorid for a site" do
      let!(:account) { create :account, company: rescue_company, client_company: geico }

      let(:account_params) do
        {
          id: account.id,
          dispatchable_sites: [{
            site_id: account.company.sites[0].id,
            enabled: true,
            contractorids: ['CA123456'],
          }],
        }
      end

      it "adds a new account and creates the needed isscs" do
        dump_table(Company)

        post :update, params: { id: account.id, account: account_params }, as: :json
        expect(response).to be_successful

        issc = Issc.find_by(contractorid: 'CA123456')
        expect(issc).not_to be_nil
        expect(issc.unregistered?).to be true
        expect(issc.clientid).to eq('GCOAPI')
      end
    end

    context "with site connected to contractorid" do
      let!(:account) do
        create(:account, {
          client_company: geico,
          company: rescue_company,
        })
      end

      # let!(:customer) {create(:user,{
      #    phone:TEST_PHONE
      # })}

      let!(:issc) do
        create(
          :gco_issc,
          clientid: 'GCOAPI',
          site: rescue_company.sites[0],
          company: rescue_company,
          contractorid: 'CA123456'
        )
      end

      let(:account_params) do
        {
          dispatchable_sites: [{
            site_id: issc.site_id,
            enabled: true,
            contractorids: ['CA123457'],
          }],
        }
      end

      it "sets deleted_at for Unregistered row and adds new issc row" do
        post :update, params: { id: account.id, account: account_params }, as: :json
        expect(response).to be_successful

        issc.reload
        Rails.logger.debug "ISSC: reload - #{issc.inspect}"
        expect(issc).not_to be_nil
        expect(issc.delete_pending_at).to be_nil
        expect(issc.deleted_at).not_to be_nil

        issc = Issc.find_by(contractorid: 'CA123457')
        expect(issc).not_to be_nil
        expect(issc.unregistered?).to be true
        expect(issc.clientid).to eq('GCOAPI')
      end

      it "sets pending_delete_at and adds new issc row" do
        issc.status = :registered
        issc.save!
        post :update, params: { id: account.id, account: account_params }, as: :json
        expect(response).to be_successful

        issc.reload
        Rails.logger.debug "ISSC: reload - #{issc.inspect}"
        expect(issc).not_to be_nil
        expect(issc.delete_pending_at).not_to be_nil

        issc = Issc.find_by(contractorid: 'CA123457')
        expect(issc).not_to be_nil
        expect(issc.unregistered?).to be true
        expect(issc.clientid).to eq('GCOAPI')
      end
    end

    context "with NSD connection with incorrect password" do
      let!(:account) { create :account, fleet: :nsd }
      let!(:issc) do
        create :nsd_issc, {
          site: account.company.sites[0],
          fleet_company: account.client_company,
          company: account.company,
          contractorid: '123456',
          status: 'PasswordIncorrect',
        }
      end

      let(:account_params) do
        {
          dispatchable_sites: [{
            site_id: issc.site_id,
            enabled: true,
            contractorids: ['123456'],
            username: "username",
            password: "newpassword",
          }],
        }
      end

      it 'updates the username and password' do
        issc_login = issc.issc_login
        expect(issc_login.deleted_at).to be_nil

        post :update, params: { id: account.id, account: account_params }, as: :json
        expect(response).to be_successful

        issc_login.reload
        expect(issc_login.deleted_at).not_to be_nil

        issc.reload
        expect(issc.issc_login.id).not_to eq(issc_login.id)
        expect(issc.issc_login.password).to eq("newpassword")
      end

      it 'schedules the connection for deregistration' do
        expect do
          post :update, params: { id: account.id, account: account_params }, as: :json
        end.to change { IsscFixDeleted.jobs.count }.by(1)

        issc.reload
        expect(issc.status).to eq('deregistering')
      end
    end

    context "with NSD connection with incorrect password" do
      let!(:account) { create :account, fleet: :nsd }
      let!(:issc) do
        create :nsd_issc, {
          site: account.company.sites[0],
          fleet_company: account.client_company,
          company: account.company,
          contractorid: '123456',
          status: 'RegisterFailed',
        }
      end

      let(:account_params) do
        {
          dispatchable_sites: [{
            site_id: issc.site_id,
            enabled: true,
            contractorids: ['123457'],
            username: "username",
            password: "password",
          }],
        }
      end

      it 'marks the old issc as deleted' do
        post :update, params: { id: account.id, account: account_params }, as: :json

        issc.reload
        expect(issc.deleted_at).not_to be_nil
      end
    end
  end

  describe "GET index" do
    stub_user_login(:rescue)

    render_views

    let!(:accounts) { create_list :account, 5, company: rescue_company }
    let(:rescue_company) { user.company }

    it "works" do
      get :index, as: :json
      expect(JSON.parse(response.body).length).to eq(5)
    end

    it "works with pagination" do
      get :index, as: :json, params: { per_page: 3, page: 1 }
      expect(JSON.parse(response.body).length).to eq(3)
    end
  end

  describe "GET batch" do
    stub_user_login(:rescue)

    render_views

    let!(:accounts) { create_list :account, 5, company: rescue_company }
    let(:rescue_company) { user.company }

    it "works" do
      get :batch, as: :json, params: { account_ids: accounts.map(&:id) }
      expect(JSON.parse(response.body).length).to eq(5)
    end

    it "works with pagination" do
      get :batch, as: :json, params: { account_ids: accounts.map(&:id), per_page: 3, page: 1 }
      expect(JSON.parse(response.body).length).to eq(3)
    end
  end

  describe "GET index_structured_for_rates" do
    stub_user_login(:rescue)

    subject(:get_index_structured_for_rates) do
      get :index_structured_for_rates, as: :json
    end

    let(:account) { create(:account, company: rescue_company) }
    let(:site_for_rate) { create(:site, company: rescue_company) }
    let(:vendor_id_for_rate) { "CA12345" }

    let(:account_with_rates) do
      create(:account, company: rescue_company, name: 'Account with Rate')
    end

    let(:rescue_company) { user.company }

    let(:rate_account_level) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site: nil,
        vendor_id: nil,
        service_code: nil,
        hourly: 32.0
      )
    end

    let(:rate_site_level) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site_id: site_for_rate.id,
        vendor_id: nil,
        service_code: nil,
        hourly: 32.0
      )
    end

    let(:rate_vendor_id_level) do
      create(
        :rate,
        company: rescue_company,
        type: 'HoursP2PRate',
        account_id: account_with_rates.id,
        site_id: site_for_rate.id,
        vendor_id: vendor_id_for_rate,
        service_code: nil,
        hourly: 32.0
      )
    end

    before do
      # create rates
      rate_account_level
      rate_site_level
      rate_vendor_id_level
    end

    it 'responds with existent account' do
      get_index_structured_for_rates

      accounts_response = JSON.parse(response.body)

      account_found = accounts_response.find { |acc| acc["id"] == account_with_rates.id }

      expect(account_found).to be_present
    end

    it 'responds with existent account and site' do
      get_index_structured_for_rates

      accounts_response = JSON.parse(response.body)

      account_found = accounts_response.find      { |acc| acc["id"] == account_with_rates.id }
      site_appended = account_found["sites"].find { |site| site["id"] == site_for_rate.id }

      expect(site_appended).to be_present
    end

    it 'responds with existent account, site and vendor_id' do
      get_index_structured_for_rates

      accounts_response = JSON.parse(response.body)

      account_found = accounts_response.find      { |acc| acc["id"] == account_with_rates.id }
      site_appended = account_found["sites"].find { |site| site["id"] == site_for_rate.id }

      vendor_id_appended = site_appended["vendor_ids"].find do |vendor_id|
        vendor_id == vendor_id_for_rate
      end

      expect(vendor_id_appended).to be_present
    end
  end

  context "ported from minitest" do
    fixtures :all

    before(:each) do
      authenticate_request(controller.request, token)
    end

    let!(:token) { create :doorkeeper_access_token, resource_owner_id: user.id, scopes: user.roles.map(&:name) }

    context "eng1730" do
      let(:user) { users(:eng1730_user) }

      it "updates an NSD account with credentials and other accounts are untouched - ENG1730" do
        site_id = sites(:eng1730_site1).id
        contractorid = isscs(:eng1730_issc1).contractorid
        params = { id: accounts(:eng1730_nsd_account).id.to_i, format: :json, account: { id: accounts(:eng1730_nsd_account).id.to_i, dispatchable_sites: [{ site_id: site_id, enabled: true, locationid: nil, contractorids: [contractorid], username: "asd", password: "asd" }] } }
        patch(:update, params: params)
        assert_response(:success)
        isscs = Issc.where(contractorid: contractorid, site_id: site_id)
        Rails.logger.warn("#{JSON.pretty_generate(isscs.all.map(&:as_json))}")
        expect(isscs.length).to(eq(2))
        issc = Issc.find_by(contractorid: contractorid, clientid: "NSD", site_id: site_id)
        expect(issc.deleted_at).to(be_nil)
        expect(issc.delete_pending_at).to(be_nil)
        issc = Issc.find_by(contractorid: contractorid, clientid: "GCOAPI", site_id: site_id)
        expect(issc.deleted_at).to(be_nil)
        expect(issc.delete_pending_at).to(be_nil)
      end
    end

    context "alice_flt" do
      let(:user) { users(:alice_flt) }

      it "moves the logged in site into pending delete" do
        site = sites(:one)
        issc1 = isscs(:one)
        account = accounts(:flt_geico_account)
        issc1.status = "LoggedIn"
        issc1.save!
        new_contractorid = "AZ1123456"
        params = {
          id: account.id,
          format: :json,
          account: {
            id: account.id,
            dispatchable_sites: [{
              site_id: site.id,
              enabled: true,
              locationid: nil,
              contractorids: [new_contractorid],
            }],
          },
        }

        patch(:update, params: params)
        assert_response(:success)
        issc1 = Issc.find_by(contractorid: issc1.contractorid, clientid: "GCOAPI", site_id: site.id)
        expect(issc1.delete_pending_at).not_to(be_nil)
        Rails.logger.debug("Checking ISSC2")
        issc2 = Issc.find_by(contractorid: new_contractorid, clientid: "GCOAPI", site_id: site.id)
        expect(issc2).not_to(be_nil)
      end

      it "changes password on NSD" do
        site = sites(:one)
        nsd = isscs(:nsd)
        nsd_password_incorrect = issc_logins(:nsd_password_incorrect)
        account = accounts(:flt_nsd_account)
        expect(nsd.status).to(eq("password_incorrect"))
        params = {
          id: account.id,
          format: :json,
          account: {
            id: account.id,
            dispatchable_sites: [{
              site_id: site.id,
              enabled: true,
              locationid: nil,
              contractorids: [nsd.contractorid],
              username: nsd_password_incorrect.username,
              password: "new password",
            }],
          },
        }
        patch(:update, params: params)
        assert_response(:success)
        issc1 = Issc.includes(:issc_login).find(nsd.id)
        issc2 = Issc.includes(:issc_login).order(id: :desc).find_by(contractorid: nsd.contractorid, clientid: nsd.clientid, deleted_at: nil)
        expect(issc2.id).to(eq(issc1.id))
        expect(issc1.delete_pending_at).to(be_nil)
        expect(issc1.issc_login.incorrect).to be_falsey
      end
    end
  end
end
