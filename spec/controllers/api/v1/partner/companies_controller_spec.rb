# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::CompaniesController, type: :controller do
  render_views

  let!(:company) { create(:rescue_company, name: "Finish Line Towing") }
  let!(:user) { create(:user, :rescue, company: company) }

  let(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }

  before do
    authenticate_request(controller.request, token)
  end

  describe "PATCH #update_subcompany" do
    subject { patch :update_subcompany, params: params }

    context "when name is updated" do
      let(:params) do
        {
          id: company.id,
          company: {
            name: "Not Original Name",
          },
        }
      end

      it "attempts to kick off jobs" do
        expect(CompanyNameChange).to receive(:perform_async)

        subject
      end

      it "updates name successfully" do
        expect { subject }.to change { company.reload.name }.to("Not Original Name")
      end
    end
  end

  describe "PATCH#update_role_permissions" do
    subject(:update_role_permissions) do
      patch(
        :update_role_permissions,
        params: params,
        as: :json
      )
    end

    let(:params) do
      {
        company: {
          role_permissions: {
            permission_type: permission_type,
            role_ids: role_ids,
          },
        },
      }
    end

    let(:role_admin) { create(:role, :admin) }
    let(:role_dispatcher) { create(:role, :dispatcher) }
    let(:permission_type) { CompanyRolePermission::CHARGE }
    let(:role_ids) { [role_admin.id, role_dispatcher.id] }

    it 'update the company with the given role_permissions' do
      update_role_permissions

      role_permissions = json["role_permissions"]

      expect(role_permissions).to be_present
      expect(role_permissions.size).to eq 1

      permission_admin = company.role_permissions.where(
        permission_type: permission_type,
        role_id: role_admin.id
      )

      permission_dispatcher = company.role_permissions.where(
        permission_type: permission_type,
        role_id: role_dispatcher.id
      )

      expect(permission_admin).to be_present
      expect(permission_dispatcher).to be_present
    end

    context "when company has role_permissions already" do
      let!(:company_role_permission) do
        create(
          :company_role_permission,
          company: company,
          role: role_dispatcher
        )
      end
      let(:role_ids) { [role_admin.id] }

      it 'removes not given roles for the given type' do
        permission_dispatcher = company.role_permissions.where(
          permission_type: permission_type,
          role_id: role_dispatcher.id
        )

        expect(permission_dispatcher).to be_present

        update_role_permissions
        company = Company.find(json["id"])

        permission_dispatcher = company.role_permissions.where(
          permission_type: permission_type,
          role_id: role_dispatcher.id
        )
        permission_admin = company.role_permissions.where(
          permission_type: permission_type,
          role_id: role_admin.id
        )

        expect(permission_admin).to be_present
        expect(permission_dispatcher).to be_blank
      end
    end
  end

  describe '#show' do
    let(:response_body) { JSON.parse(response.body) }
    let(:company) { create(:company) }

    before do
      get :show, params: { id: company.id }, as: :json
    end

    it 'includes localization settings' do
      expect(response.status).to eq(200)
      expect(response_body['distance_unit']).to eq(company.distance_unit)
      expect(response_body['currency']).to eq(company.currency)
    end
  end
end
