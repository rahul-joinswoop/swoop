# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::JobsStatusController, type: :controller do
  stub_user_login :rescue
  let(:api_user) { user }

  describe "POST update", freeze_time: true do
    let(:job) { create :rescue_job, rescue_company: controller.api_company, status: "Pending" }
    let(:referer) { 'http://example.com' }

    before do
      allow(Job).to receive(:find_by!).with(id: job.id.to_s, rescue_company: controller.api_company)
        .and_return(job)
      request.env['HTTP_REFERER'] = referer
    end

    context "completing a job" do
      include_context "job completed shared examples"
      include_context "job status change shared examples"

      subject(:post_complete_job) do
        post :update, params: { id: job.id, job: { status: status }, format: :json }

        job.reload
        user.reload
      end

      let(:status) { "Completed" }

      context 'when user agent is Mac Web' do
        before do
          request.env['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'Web', Swoop::VERSION, 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'mac', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'chrome', '69'
      end

      context 'when user agent is Windows Web' do
        before do
          request.env['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'Web', Swoop::VERSION, 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'windows', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'chrome', '69'
      end

      context 'when user agent is Android' do
        before do
          request.env['HTTP_USER_AGENT'] = 'android Swoop/2.0.63'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'React Android', '2.0.63', 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'android', 'android Swoop/2.0.63', 'generic', '2.0.63'
      end

      context 'when user agent is SwoopMobile' do
        before do
          request.env['HTTP_USER_AGENT'] = 'ios SwoopMobile/9 CFNetwork/808.3 Darwin/16.3.0'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'React iOS', 'SwoopMobile/9', 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'ios', 'ios SwoopMobile/9 CFNetwork/808.3 Darwin/16.3.0', 'generic', 'SwoopMobile/9'
      end

      context 'when agent is ios Swoop' do
        before do
          request.env['HTTP_USER_AGENT'] = 'ios Swoop/1.03.520'
        end

        it_behaves_like 'sending Analytics Job Completed event', 'React iOS', '1.03.520', 'http://example.com'
        it_behaves_like 'set user_agent data to job and user', 'ios', 'ios Swoop/1.03.520', 'generic', '1.03.520'
      end

      it 'calls ViewAlertService.cancel_if_necessary' do
        expect(ViewAlertService).to receive(:cancel_if_necessary).with(job, status)

        post_complete_job
      end

      it 'calls job.send_user_invoice' do
        expect(job).to receive(:send_user_invoice)

        post_complete_job
      end

      it 'calls job.send_review_sms' do
        expect(job).to receive(:send_review_sms)

        post_complete_job
      end

      it 'calls job.store_vehicle?' do
        expect(job).to receive(:store_vehicle?)

        post_complete_job
      end
    end

    context 'when it is a FleetMotorClubJob' do
      subject(:patch_job_status_change) do
        post :update, params: {
          id: job.id,
          job: {
            status: new_status,
            job_status_reason_types: job_status_reason_types,
          },
          format: :json,
        }
      end

      let(:job_status_reason_types) { nil }

      let!(:account) do
        create(:account, company: controller.api_company, client_company: fleet_company)
      end

      let(:job) do
        create :fleet_motor_club_job, {
          status: job_status,
          fleet_company: fleet_company,
          adjusted_created_at: Time.now,
          rescue_company: controller.api_company,
          issc_dispatch_request: issc_dispatch_request,
        }
      end

      let(:issc_dispatch_request) do
        create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
      end

      let(:time_now) { Time.now }

      before do
        allow(Time).to receive(:now).and_return(time_now)
      end

      shared_examples 'update the job accordingly' do |_new_status|
        it 'sets job.status accordingly' do
          if issc.system.to_s != "rsc" || Agero::Rsc::Data.job_status_code(_new_status)
            patch_job_status_change

            job.reload

            expect(job.status).to eq _new_status
            expect(job.last_touched_by_id).to eq controller.api_user.id
          end
        end
      end

      context 'and job.digital_dispatcher is ISSC' do
        let(:issc) { create(:ago_issc, company: controller.api_company, api_access_token: nil) }

        shared_examples 'schedule a status change on Issc end' do |_new_issc_status|
          it 'schedules IsscDispatchStatus' do
            expect(IsscDispatchStatus).to receive(:perform_async).with(
              job.id, _new_issc_status, :Manual, time_now, time_now
            )

            patch_job_status_change
          end
        end

        context 'and fleet_company is Agero' do
          let(:fleet_company) { create(:fleet_company, :ago) }

          context 'and job.status is assigned' do
            let(:job_status) { Job::STATUS_ASSIGNED }

            context 'and new status passed is Accepted' do
              let(:new_status) { Job::ACCEPTED }

              it_behaves_like 'update the job accordingly', Job::STATUS_SUBMITTED

              it 'scheduled accept issc message' do
                expect(IsscDispatchResponseAccept).to receive(:perform_async).with(job.id)
                patch_job_status_change
              end
            end
          end

          context 'and job.status is accepted' do
            let(:job_status) { Job::STATUS_ACCEPTED }

            context 'and new status passed is Dispatched' do
              let(:new_status) { 'Dispatched' }

              it_behaves_like 'update the job accordingly', 'dispatched'
              it_behaves_like 'schedule a status change on Issc end', 'DriverAssigned'
            end

            context 'and new status passed is En Route' do
              let(:new_status) { 'En Route' }

              it_behaves_like 'update the job accordingly', Job::STATUS_ENROUTE
              it_behaves_like 'schedule a status change on Issc end', 'Enroute'
            end

            context 'and new status passed is On Site' do
              let(:new_status) { 'On Site' }

              it_behaves_like 'update the job accordingly', Job::STATUS_ONSITE
              it_behaves_like 'schedule a status change on Issc end', 'OnScene'
            end

            context 'and new status passed is Towing' do
              let(:new_status) { 'Towing' }

              it_behaves_like 'update the job accordingly', Job::STATUS_TOWING
              it_behaves_like 'schedule a status change on Issc end', 'EnrouteToDestination'
            end

            context 'and new status passed is Tow Destination' do
              let(:new_status) { 'Tow Destination' }

              it_behaves_like 'update the job accordingly', Job::STATUS_TOWDESTINATION
              it_behaves_like 'schedule a status change on Issc end', 'OnSceneDestination'
            end

            context 'and new status passed is Completed' do
              let(:new_status) { 'Completed' }

              it_behaves_like 'update the job accordingly', Job::STATUS_COMPLETED
              it_behaves_like 'schedule a status change on Issc end', 'ServiceComplete'
            end

            context 'and new status passed is Canceled' do
              let(:new_status) { 'Canceled' }

              it_behaves_like 'update the job accordingly', Job::STATUS_CANCELED
              it_behaves_like 'schedule a status change on Issc end', 'ProviderCancel'
            end

            context 'and new status passed is GOA' do
              let(:new_status) { 'GOA' }

              it_behaves_like 'update the job accordingly', Job::STATUS_GOA
              it_behaves_like 'schedule a status change on Issc end', 'GOANeeded'
            end
          end
        end
      end

      context 'and job.digital_dispatcher is RSC' do
        let(:api_access_token) { create(:api_access_token) }
        let(:issc) do
          create(
            :ago_issc,
            company: controller.api_company,
            api_access_token: api_access_token,
            system: Issc::RSC_SYSTEM
          )
        end

        shared_examples 'async calling RSC API to update job status to' do |swoop_status_id|
          it 'schedules Agero::Rsc::ChangeDispatchStatusWorker with expected params' do
            if Agero::Rsc::Data.job_status_code(swoop_status_id)
              expect(Agero::Rsc::ChangeDispatchStatusWorker).to receive(:perform_async).with(
                job.id, Agero::Rsc::Status.new(job, code: Agero::Rsc::Data.job_status_code(swoop_status_id)).to_hash
              )

              patch_job_status_change
            end
          end
        end

        shared_examples 'async calling RSC API to update job cancel accordingly with' do |rsc_status_code, rsc_reason_code|
          it 'schedules Agero::Rsc::ChangeDispatchStatusWorker with expected params' do
            expect(Agero::Rsc::CancelDispatchWorker).to receive(:perform_async).with(
              job.id, rsc_status_code, rsc_reason_code
            )

            patch_job_status_change
          end
        end

        shared_examples 'adding a job_status_reason to the job' do
          it 'increases JobStatusReason.count by 1' do
            expect { patch_job_status_change }.to change(JobStatusReason, :count).by(1)
          end
        end

        context 'and fleet_company is Agero' do
          let(:fleet_company) { create(:fleet_company, :ago) }

          context 'and job.status is accepted' do
            let(:job_status) { Job::STATUS_ACCEPTED }

            context 'and new status passed is Dispatched' do
              let(:new_status) { 'Dispatched' }

              it_behaves_like 'update the job accordingly', Job::STATUS_DISPATCHED
              it_behaves_like 'async calling RSC API to update job status to', JobStatus::DISPATCHED
            end

            context 'and new status passed is En Route' do
              let(:new_status) { 'En Route' }

              it_behaves_like 'update the job accordingly', Job::STATUS_ENROUTE
              it_behaves_like 'async calling RSC API to update job status to', JobStatus::EN_ROUTE
            end

            context 'and new status passed is On Site' do
              let(:new_status) { 'On Site' }

              it_behaves_like 'update the job accordingly', Job::STATUS_ONSITE
              it_behaves_like 'async calling RSC API to update job status to', JobStatus::ON_SITE
            end

            context 'and new status passed is Towing' do
              let(:new_status) { 'Towing' }

              it_behaves_like 'update the job accordingly', Job::STATUS_TOWING
              it_behaves_like 'async calling RSC API to update job status to', JobStatus::TOWING
            end

            context 'and new status passed is Tow Destination' do
              let(:new_status) { 'Tow Destination' }

              it_behaves_like 'update the job accordingly', Job::STATUS_TOWDESTINATION
              it_behaves_like(
                'async calling RSC API to update job status to',
                JobStatus::TOW_DESTINATION
              )
            end

            context 'and new status passed is Completed' do
              let(:new_status) { 'Completed' }

              it_behaves_like 'update the job accordingly', Job::STATUS_COMPLETED
              it_behaves_like 'async calling RSC API to update job status to', JobStatus::COMPLETED
            end

            context 'and new status passed is Canceled' do
              let(:new_status) { 'Canceled' }

              let(:job_status_reason_type) do
                create :job_status_reason_type, key
              end

              let(:key) { :cancel_prior_job_delayed }

              let(:job_status_reason_types) do
                [
                  { id: job_status_reason_type.id },
                ]
              end

              let(:audit_job_status) do
                create(:audit_job_status, job: job, job_status: job_status_reason_type.job_status)
              end

              it_behaves_like 'update the job accordingly', Job::STATUS_CANCELED
              it_behaves_like 'adding a job_status_reason to the job'

              context 'and job_status_reason_type.key = :cancel_prior_job_delayed' do
                let(:key) { :cancel_prior_job_delayed }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :service_provider_cancel,
                  :prior_job_delayed
                )
              end

              context 'and job_status_reason_type.key = :cancel_traffic_service_vehicle_problem' do
                let(:key) { :cancel_traffic_service_vehicle_problem }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :service_provider_cancel,
                  :traffic_vehicle_problem,
                )
              end

              context 'and job_status_reason_type.key = :cancel_out_of_area' do
                let(:key) { :cancel_out_of_area }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :service_provider_cancel,
                  :out_of_area,
                )
              end

              context 'and job_status_reason_type.key = :cancel_another_job_priority' do
                let(:key) { :cancel_another_job_priority }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :service_provider_cancel,
                  :another_job_priority,
                )
              end

              context 'and job_status_reason_type.key = :cancel_no_reason_given' do
                let(:key) { :cancel_no_reason_given }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :service_provider_cancel,
                  :no_reason_given,
                )
              end

              context 'and job_status_reason_type.key = :cancel_customer_found_alternate_solution' do
                let(:key) { :cancel_customer_found_alternate_solution }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :customer_cancel,
                  :found_alternate_solution,
                )
              end
            end

            context 'and new status passed is GOA' do
              let(:new_status) { 'GOA' }

              let(:job_status_reason_type) do
                create(:job_status_reason_type, key)
              end

              let(:key) { :goa_customer_cancel_after_deadline }

              let(:job_status_reason_types) do
                [
                  { id: job_status_reason_type.id },
                ]
              end

              let(:audit_job_status) do
                create(:audit_job_status, job: job, job_status: job_status_reason_type.job_status)
              end

              it_behaves_like 'update the job accordingly', Job::STATUS_GOA
              it_behaves_like 'adding a job_status_reason to the job'

              context 'and job_status_reason_type.key = :goa_customer_cancel_after_deadline' do
                let(:key) { :goa_customer_cancel_after_deadline }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :customer_cancel,
                  :customer_cancel,
                )
              end

              context 'and job_status_reason_type.key = :goa_wrong_location_given' do
                let(:key) { :goa_wrong_location_given }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :unsuccessful_no_vehicle,
                  :wrong_location_given,
                )
              end

              context 'and job_status_reason_type.key = :goa_customer_not_with_vehicle' do
                let(:key) { :goa_customer_not_with_vehicle }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :unsuccessful_no_customer,
                  :customer_not_with_vehicle,
                )
              end

              context 'and job_status_reason_type.key = :goa_incorrect_service' do
                let(:key) { :goa_incorrect_service }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :service_attempt_failed,
                  :incorrect_service,
                )
              end

              context 'and job_status_reason_type.key = :goa_incorrect_equipment' do
                let(:key) { :goa_incorrect_equipment }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :service_attempt_failed,
                  :incorrect_equipment,
                )
              end

              context 'and job_status_reason_type.key = :goa_unsuccessful_service_attempt' do
                let(:key) { :goa_unsuccessful_service_attempt }

                it_behaves_like(
                  'async calling RSC API to update job cancel accordingly with',
                  :service_attempt_failed,
                  :need_additional_equipment,
                )
              end
            end
          end
        end
      end
    end

    describe 'job.last_touched_by' do
      include_context "job status change shared examples"

      subject(:update_job_status) do
        post :update, params: { id: job.id, job: { status: 'Dispatched' }, format: :json }
      end

      let(:job) { create(:fleet_managed_job, status: 'pending') }

      it_behaves_like 'updates job.last_touched_by with controller.api_user'
    end
  end
end
