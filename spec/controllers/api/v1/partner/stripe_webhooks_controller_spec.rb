# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::StripeWebhooksController, type: :controller do
  describe 'POST post_account_event' do
    subject(:post_account_event) do
      post(
        :account_event,
        body: body,
        format: :json
      )
    end

    let(:body) do
      %q(
        {
          "id": "evt_1CiRq3KEsAxZwc7YHQwom7sS",
          "object": "event",
          "account": "acct_1CiOwFKEsAxZwc7Y",
          "api_version": "2018-05-21",
          "created": 1530298859,
          "data": {
              "object": {
                  "id": "py_1CiRq3KEsAxZwc7YH5Tw8LsA",
                  "object": "charge",
                  "amount": 26960,
                  "amount_refunded": 0,
              }
          },
          "type": "charge.dispute.created"
        }
      )
    end

    let(:event_handler_double) { double(handle: nil) }

    before do
      allow(StripeIntegration::EventHandlers::Base).to receive(:send).and_return(
        event_handler_double
      )

      request.env['HTTP_STRIPE_SIGNATURE'] = 'very giant stripe sig'
    end

    it 'responds 200 OK' do
      post_account_event

      expect(response.code).to eq '200'
    end

    it 'sends StripeIntegration::EventHandlers::Base the right method with expected attributes' do
      post_account_event

      expect(StripeIntegration::EventHandlers::Base).to have_received(:send).with(
        :find_account_handler_by,
        payload: body,
        sig_header: 'very giant stripe sig',
        webhook_secret: Configuration.stripe.account_webhook_secret,
      )
    end
  end

  describe 'POST post_connect_event' do
    subject(:post_connect_event) do
      post(
        :connect_event,
        body: body,
        format: :json
      )
    end

    let(:body) do
      %q(
        {
          "id": "evt_1CiRq3KEsAxZwc7YHQwom7sS",
          "object": "event",
          "account": "acct_1CiOwFKEsAxZwc7Y",
          "api_version": "2018-05-21",
          "created": 1530298859,
          "data": {
              "object": {
                  "id": "py_1CiRq3KEsAxZwc7YH5Tw8LsA",
                  "object": "charge",
                  "amount": 26960,
                  "amount_refunded": 0,
              }
          },
          "type": "account.updated"
        }
      )
    end

    let(:event_handler_double) { double(handle: nil) }

    before do
      allow(StripeIntegration::EventHandlers::Base).to receive(:send).and_return(
        event_handler_double
      )

      request.env['HTTP_STRIPE_SIGNATURE'] = 'very giant stripe sig'
    end

    it 'responds 200 OK' do
      post_connect_event

      expect(response.code).to eq '200'
    end

    it 'sends StripeIntegration::EventHandlers::Base the right method with expected attributes' do
      post_connect_event

      expect(StripeIntegration::EventHandlers::Base).to have_received(:send).with(
        :find_connect_handler_by,
        payload: body,
        sig_header: 'very giant stripe sig',
        webhook_secret: Configuration.stripe.connect_webhook_secret,
      )
    end
  end
end
