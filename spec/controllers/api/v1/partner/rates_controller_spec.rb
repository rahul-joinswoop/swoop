# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::RatesController, type: :controller do
  let(:account) { create :account, company: company, client_company: fleet_company }
  let(:company) { user.company }
  let(:fleet_company) { create :fleet_company }
  let(:site) { create(:site, company: user.company) }

  stub_user_login(:rescue)

  describe "POST live" do
    subject(:post_live_rate) do
      post :live,
           params: {
             rate: {
               type: "FlatRate",
               account_id: account.id,
               service_code_id: service_code.id,
             },
           }, as: :json
    end

    let(:miles_p2_p_rate) do
      create(:miles_p2p_rate, account: account, service_code: service_code, company: company)
    end
    let!(:flat_rate) do
      create :flat_rate,
             account: account, service_code: service_code, company: company
    end
    let(:service_code) { create :service_code }

    it "updates existing rate with passed param type to live" do
      expect(flat_rate.live).to be_nil
      post_live_rate
      expect(flat_rate.reload.live).to eql(true)
    end

    it "updates existing live rate to not be live" do
      miles_p2_p_rate.update! live: true

      expect(miles_p2_p_rate.reload.live).to eql(true)

      post_live_rate

      expect(miles_p2_p_rate.reload.live).to eql(false)
    end

    it "enqueues a new RecalculateLiveRatesBatch when new live rate" do
      miles_p2_p_rate.update! live: true

      expect(miles_p2_p_rate.reload.live).to eql(true)

      post_live_rate

      expect(Delayed::Job.last(2).map(&:name)).to include("RecalculateLiveRatesBatch")
    end

    it "sends an email notification" do
      post_live_rate
      expect(Delayed::Job.last(2).map(&:name))
        .to include("EmailRateChangeToPartnerAndFleet")
    end

    context "when the fleet company is Motor Club" do
      let(:account) do
        create :account, company: company, fleet: :gcoapi
      end

      it "does not send email notification" do
        post_live_rate
        expect(Delayed::Job.last(2).map(&:name))
          .not_to include("EmailRateChangeToPartnerAndFleet")
      end
    end

    describe "rendered results" do
      render_views

      it "returns an empty array on success" do
        post_live_rate
        expect(response.body).to eql("[]")
      end
    end
  end

  describe "DELETE delete" do
    subject(:delete_rates) do
      delete :delete, params: { rates: rates_params }, as: :json
    end

    let!(:flat_rate) do
      create :flat_rate, account: account, service_code: nil, company: company
    end
    let(:flat_rate_with_vehicle_category) do
      create :flat_rate,
             account: account, service_code: nil, company: company,
             vehicle_category_id: vehicle_category.id
    end
    let(:vehicle_category) do
      create :vehicle_category
    end
    let(:rates_params) do
      { type: "FlatRate", account_id: account.id, service_code_id: nil }
    end

    context "when :account_id, :site_id, :vendor_id and :service_code_id are given" do
      let(:vendor_id) { 112233 }

      it "marks multiple rates as deleted" do
        create :flat_rate, account: account, site_id: site.id, vendor_id: vendor_id, service_code: nil, company: company

        expect(company.rates.count).to eql(2)

        delete_rates

        expect(company.rates.count).to eql(0)
        # association default_scope is deleted_at: nil so we need to also test
        # unscoped
        expect(company.rates.unscoped.count).to eql(2)
      end

      it "marks rates with a given :vehicle_category_id as deleted" do
        flat_rate_with_vehicle_category

        delete :delete,
               params: { rates: rates_params.merge(vehicle_category_id: vehicle_category.id) },
               as: :json

        expect(company.rates.count).to eql(1) # the one created by let!
        expect(company.rates.unscoped.where("deleted_at is not null").first)
          .to eql(flat_rate_with_vehicle_category)
      end

      it "calls .schedule_all_rate_invoice_updates on Job" do
        expect(Job).to receive(:schedule_all_rate_invoice_updates)
        delete_rates
      end
    end

    context "when :account_id is not given" do
      it "does not call .schedule_all_rate_invoice_updates on Job" do
        expect(Job).not_to receive(:schedule_all_rate_invoice_updates)
        delete :delete,
               params: { rates: rates_params.merge(account_id: nil) }, as: :json
      end
    end

    it "responds with head :no_content" do
      delete_rates
      expect(response.status).to eql(204)
    end

    it "sends an email notification" do
      delete_rates
      expect(Delayed::Job.last(2).map(&:name))
        .to include("EmailRateChangeToPartnerAndFleet")
    end

    context "when the fleet company is Motor Club" do
      let(:account) do
        create :account, company: company, client_company: fleet_company
      end
      let(:fleet_company) { create :fleet_company, :motor_club }

      it "does not send email notification" do
        delete_rates
        expect(Delayed::Job.last(2).map(&:name))
          .not_to include("EmailRateChangeToPartnerAndFleet")
      end
    end

    it "does not initialize the ServiceRunner without any rates" do
      Rate.delete_all
      expect(ServiceRunner).not_to receive(:new)
      delete_rates
    end
  end

  describe '#mail_rate_change_service' do
    subject(:rates_controller) { API::V1::Partner::RatesController.new }

    before :each do
      allow(rates_controller).to receive(:account_id_from_params).and_return(1)
      allow(Account).to receive(:find_by_id).and_return account
    end

    let(:account) { build(:account, client_company: nil) }

    context 'when the Account doesnt have a FleetCompany' do
      it 'returns nil' do
        expect(rates_controller.send(:mail_rate_change_service)).to eq nil
      end

      it 'doesnt instantiate a RateService::MailRateChange' do
        expect(RateService::MailRateChange).not_to receive(:new)

        rates_controller.send(:mail_rate_change_service)
      end
    end
  end

  describe '#fetch_by_filters' do
    subject(:fetch_by_filters) do
      get :fetch_by_filters,
          params: {
            rate: {
              account_id: account_id,
              site_id: site_id,
              vendor_id: vendor_id,
              service_code_id: service_code_id,
            },
          },
          as: :json
    end

    let(:rate_default_1) { create(:hourly_p2p, company: company) }

    let(:rate_default_2) { create(:miles_p2p_rate, company: company) }

    let(:rate_account_1) { create(:hourly_p2p, :rate_account_level, company: company, account: account) }

    let(:rate_account_2) do
      create(:miles_p2p_rate, :rate_account_level, company: company, account: account)
    end

    let(:rate_site_1) do
      create(:hourly_p2p, :rate_site_level, company: company, account: account, site: site)
    end

    let(:rate_site_2) do
      create(:miles_p2p_rate, :rate_site_level, company: company, account: account, site: site)
    end

    let(:rate_vendor_id_1) do
      create(
        :miles_p2p_rate,
        :rate_vendor_id_level,
        company: company,
        account: account,
        site: site,
        vendor_id: vendor_id,
      )
    end

    let(:rate_vendor_id_2) do
      create(
        :miles_p2p_rate,
        :rate_vendor_id_level,
        company: company,
        account: account,
        site: site,
        vendor_id: vendor_id,
        hourly: 45.0,
      )
    end

    let(:rate_service_code_id_1) do
      create(
        :miles_p2p_rate,
        :rate_service_code_level,
        company: company,
        account: account,
        site: site,
        vendor_id: vendor_id,
        service_code: service_code,
      )
    end

    let(:rate_service_code_id_2) do
      create(
        :miles_p2p_rate,
        :rate_service_code_level,
        company: company,
        account: account,
        site: site,
        vendor_id: vendor_id,
        service_code: service_code,
        hourly: 55.0,
      )
    end

    let(:vendor_id) { nil }
    let(:service_code) { create(:service_code) }

    before do
      # create rates
      rate_default_1
      rate_default_2

      rate_account_1
      rate_account_2

      rate_site_1
      rate_site_2

      rate_vendor_id_1
      rate_vendor_id_2

      rate_service_code_id_1
      rate_service_code_id_2
    end

    context 'when no filter is passed' do
      let(:account_id) { nil }
      let(:site_id) { nil }
      let(:vendor_id) { nil }
      let(:service_code_id) { nil }

      it 'brings universal default rates only' do
        fetch_by_filters

        rates = JSON.parse(response.body)

        rate_1 = rates.select { |rate| rate['id'] == rate_default_1.id }.first
        rate_2 = rates.select { |rate| rate['id'] == rate_default_2.id }.first

        expect(rate_1.present?).to be_truthy
        expect(rate_2.present?).to be_truthy
      end

      it 'doesnt bring rates associated with an account' do
        fetch_by_filters

        rates = JSON.parse(response.body)

        response_rate_account_1 = rates.select { |rate| rate['id'] == rate_account_1.id }.first
        response_rate_account_2 = rates.select { |rate| rate['id'] == rate_account_2.id }.first

        expect(response_rate_account_1.present?).to be_falsey
        expect(response_rate_account_2.present?).to be_falsey
      end
    end

    context 'when account_id is passed' do
      let(:account_id) { account.id }
      let(:site_id) { nil }
      let(:vendor_id) { nil }
      let(:service_code_id) { nil }

      it 'filters by account only' do
        fetch_by_filters

        rates = JSON.parse(response.body)

        response_rate_account_1 = rates.select { |rate| rate['id'] == rate_account_1.id }.first
        response_rate_account_2 = rates.select { |rate| rate['id'] == rate_account_2.id }.first

        expect(response_rate_account_1.present?).to be_truthy
        expect(response_rate_account_2.present?).to be_truthy
      end

      it 'doesnt bring rates without account_id' do
        fetch_by_filters

        rates = JSON.parse(response.body)

        response_rate_default_1 = rates.select { |rate| rate['id'] == rate_default_1.id }.first
        response_rate_default_2 = rates.select { |rate| rate['id'] == rate_default_2.id }.first

        expect(response_rate_default_1.present?).to be_falsey
        expect(response_rate_default_2.present?).to be_falsey
      end

      it 'doesnt bring rates with site_id, vendor_id or service_code_id set' do
        fetch_by_filters

        rates = JSON.parse(response.body)

        response_rate_site_1 = rates.select { |rate| rate['id'] == rate_site_1.id }.first
        response_rate_site_2 = rates.select { |rate| rate['id'] == rate_site_2.id }.first

        response_rate_contractor_1 = rates.select { |rate| rate['id'] == rate_vendor_id_1.id }.first
        response_rate_contractor_2 = rates.select { |rate| rate['id'] == rate_vendor_id_2.id }.first

        response_rate_service_1 = rates.select { |rate| rate['id'] == rate_service_code_id_1.id }.first
        response_rate_service_2 = rates.select { |rate| rate['id'] == rate_service_code_id_2.id }.first

        expect(response_rate_site_1.present?).to be_falsey
        expect(response_rate_site_2.present?).to be_falsey

        expect(response_rate_contractor_1.present?).to be_falsey
        expect(response_rate_contractor_2.present?).to be_falsey

        expect(response_rate_service_1.present?).to be_falsey
        expect(response_rate_service_2.present?).to be_falsey
      end
    end

    context 'when account_id and site_id are passed' do
      let(:account_id) { account.id }
      let(:site_id) { site.id }
      let(:vendor_id) { nil }
      let(:service_code_id) { nil }

      it 'filters by account and site only' do
        fetch_by_filters

        rates = JSON.parse(response.body)

        response_rate_site_1 = rates.select { |rate| rate['id'] == rate_site_1.id }.first
        response_rate_site_2 = rates.select { |rate| rate['id'] == rate_site_2.id }.first

        expect(response_rate_site_1.present?).to be_truthy
        expect(response_rate_site_2.present?).to be_truthy
      end

      it 'doesnt bring rates without site_id' do
        fetch_by_filters

        rates = JSON.parse(response.body)

        response_rate_default_1 = rates.select { |rate| rate['id'] == rate_default_1.id }.first
        response_rate_default_2 = rates.select { |rate| rate['id'] == rate_default_2.id }.first

        expect(response_rate_default_1.present?).to be_falsey
        expect(response_rate_default_2.present?).to be_falsey
      end

      it 'doesnt bring rates with vendor_id or service_code_id set' do
        fetch_by_filters

        rates = JSON.parse(response.body)

        response_rate_account_1 = rates.select { |rate| rate['id'] == rate_account_1.id }.first
        response_rate_account_2 = rates.select { |rate| rate['id'] == rate_account_2.id }.first

        response_rate_service_1 = rates.select { |rate| rate['id'] == rate_service_code_id_1.id }.first
        response_rate_service_2 = rates.select { |rate| rate['id'] == rate_service_code_id_2.id }.first

        expect(response_rate_account_1.present?).to be_falsey
        expect(response_rate_account_2.present?).to be_falsey

        expect(response_rate_service_1.present?).to be_falsey
        expect(response_rate_service_2.present?).to be_falsey
      end

      context 'when account_id, site_id and vendor_id are passed' do
        let(:account_id) { account.id }
        let(:site_id) { site.id }
        let(:vendor_id) { 112233 }
        let(:service_code_id) { nil }

        it 'filters by account, site and vendor_id only' do
          fetch_by_filters

          rates = JSON.parse(response.body)

          response_rate_contractor_1 = rates.select { |rate| rate['id'] == rate_vendor_id_1.id }.first
          response_rate_contractor_2 = rates.select { |rate| rate['id'] == rate_vendor_id_2.id }.first

          expect(response_rate_contractor_1.present?).to be_truthy
          expect(response_rate_contractor_2.present?).to be_truthy
        end

        it 'doesnt bring rates without vendor_id' do
          fetch_by_filters

          rates = JSON.parse(response.body)

          response_rate_default_1 = rates.select { |rate| rate['id'] == rate_default_1.id }.first
          response_rate_default_2 = rates.select { |rate| rate['id'] == rate_default_2.id }.first

          response_rate_account_1 = rates.select { |rate| rate['id'] == rate_account_1.id }.first
          response_rate_account_2 = rates.select { |rate| rate['id'] == rate_account_2.id }.first

          response_rate_site_1 = rates.select { |rate| rate['id'] == rate_site_1.id }.first
          response_rate_site_2 = rates.select { |rate| rate['id'] == rate_site_2.id }.first

          expect(response_rate_default_1.present?).to be_falsey
          expect(response_rate_default_2.present?).to be_falsey

          expect(response_rate_account_1.present?).to be_falsey
          expect(response_rate_account_2.present?).to be_falsey

          expect(response_rate_site_1.present?).to be_falsey
          expect(response_rate_site_2.present?).to be_falsey
        end

        it 'doesnt bring rates with service_code_id set' do
          fetch_by_filters

          rates = JSON.parse(response.body)

          response_rate_service_1 = rates.select { |rate| rate['id'] == rate_service_code_id_1.id }.first
          response_rate_service_2 = rates.select { |rate| rate['id'] == rate_service_code_id_2.id }.first

          expect(response_rate_service_1.present?).to be_falsey
          expect(response_rate_service_2.present?).to be_falsey
        end
      end

      context 'when account_id, site_id, vendor_id and service_code_id are passed' do
        let(:account_id) { account.id }
        let(:site_id) { site.id }
        let(:vendor_id) { 112233 }
        let(:service_code_id) { service_code.id }

        it 'filters by account, site, vendor_id and service_code_id only' do
          fetch_by_filters

          rates = JSON.parse(response.body)

          response_rate_service_1 = rates.select { |rate| rate['id'] == rate_service_code_id_1.id }.first
          response_rate_service_2 = rates.select { |rate| rate['id'] == rate_service_code_id_2.id }.first

          expect(response_rate_service_1.present?).to be_truthy
          expect(response_rate_service_2.present?).to be_truthy
        end

        it 'doesnt bring rates without service_code_id' do
          fetch_by_filters

          rates = JSON.parse(response.body)

          response_rate_default_1 = rates.select { |rate| rate['id'] == rate_default_1.id }.first
          response_rate_default_2 = rates.select { |rate| rate['id'] == rate_default_2.id }.first

          response_rate_account_1 = rates.select { |rate| rate['id'] == rate_account_1.id }.first
          response_rate_account_2 = rates.select { |rate| rate['id'] == rate_account_2.id }.first

          response_rate_site_1 = rates.select { |rate| rate['id'] == rate_site_1.id }.first
          response_rate_site_2 = rates.select { |rate| rate['id'] == rate_site_2.id }.first

          response_rate_contractor_1 = rates.select { |rate| rate['id'] == rate_vendor_id_1.id }.first
          response_rate_contractor_2 = rates.select { |rate| rate['id'] == rate_vendor_id_2.id }.first

          expect(response_rate_default_1.present?).to be_falsey
          expect(response_rate_default_2.present?).to be_falsey

          expect(response_rate_account_1.present?).to be_falsey
          expect(response_rate_account_2.present?).to be_falsey

          expect(response_rate_site_1.present?).to be_falsey
          expect(response_rate_site_2.present?).to be_falsey

          expect(response_rate_contractor_1.present?).to be_falsey
          expect(response_rate_contractor_2.present?).to be_falsey
        end
      end
    end
  end

  describe '#post_defaults' do
    render_views

    let(:rate_default_1) { create(:hourly_p2p, company: company) }

    let(:rate_default_2) { create(:miles_p2p_rate, company: company) }

    let(:rate_account_1) { create(:hourly_p2p, :rate_account_level, company: company) }

    let(:rate_account_2) do
      create(:miles_p2p_rate, :rate_account_level, company: company, account: account)
    end

    let(:rate_site_1) do
      create(:hourly_p2p, :rate_site_level, company: company, account: account, site: site)
    end

    let(:rate_site_2) do
      create(:miles_p2p_rate, :rate_site_level, company: company, account: account, site: site)
    end

    let(:rate_vendor_id_1) do
      create(
        :miles_p2p_rate,
        :rate_vendor_id_level,
        company: company,
        account: account,
        site: site,
        vendor_id: vendor_id
      )
    end

    let(:rate_vendor_id_2) do
      create(
        :miles_p2p_rate,
        :rate_vendor_id_level,
        company: company,
        account: account,
        site: site,
        vendor_id: vendor_id,
        hourly: 45.0
      )
    end

    let(:rate_service_code_id_1) do
      create(
        :miles_p2p_rate,
        :rate_service_code_level,
        company: company,
        account: account,
        site: site,
        vendor_id: vendor_id,
        service_code: service_code,
      )
    end

    let(:rate_service_code_id_2) do
      create(
        :miles_p2p_rate,
        :rate_service_code_level,
        company: company,
        account: account,
        site: site,
        vendor_id: vendor_id,
        service_code: service_code,
        hourly: 55.0,
      )
    end

    let(:rate_account_with_service_code_id_1) do
      create(
        :miles_p2p_rate,
        :rate_account_with_service_code_level,
        account: account,
        service_code: service_code,
      )
    end

    let(:rate_account_with_service_code_id_2) do
      create(
        :miles_p2p_rate,
        :rate_account_with_service_code_level,
        account: account,
        service_code: service_code,
      )
    end

    let(:vendor_id) { nil }
    let(:service_code) { create(:service_code) }

    before :each do
      # create rates
      rate_default_1
      rate_default_2

      rate_account_1
      rate_account_2

      rate_site_1
      rate_site_2

      rate_vendor_id_1
      rate_vendor_id_2

      rate_service_code_id_1
      rate_service_code_id_2

      rate_account_with_service_code_id_1
      rate_account_with_service_code_id_2
    end

    context 'when account_id only is passed' do
      subject(:post_defaults) do
        post(
          :create_based_on_defaults,
          params: {
            rate: {
              account_id: account_id,
            },
          },
          as: :json
        )
      end

      let(:account_id) { account.id }

      it 'creates a new rate based on account default' do
        post_defaults

        new_rates_on_json = JSON.parse(response.body)

        account_id_from_json = new_rates_on_json.map { |rate| rate["account_id"] }.uniq.compact
        site_id_from_json = new_rates_on_json.map { |rate| rate["site_id"] }.uniq.compact
        vendor_id_from_json = new_rates_on_json.map { |rate| rate["vendor_id"] }.uniq.compact
        service_code_id_from_json = new_rates_on_json.map do |rate|
          rate["service_code_id"]
        end.uniq.compact

        expect(account_id_from_json.size).to eq 1
        expect(account_id_from_json.first).to eq account.id

        expect(site_id_from_json).to be_blank
        expect(vendor_id_from_json).to be_blank
        expect(service_code_id_from_json).to be_blank
      end
    end

    context 'when account_id and site_id are passed' do
      subject(:post_defaults) do
        post(
          :create_based_on_defaults,
          params: {
            rate: {
              account_id: account_id,
              site_id: site_id,
            },
          },
          as: :json
        )
      end

      let(:account_id) { account.id }
      let(:site_id) { site.id }

      it 'creates a new rate based on site defaults' do
        post_defaults

        new_rates_on_json = JSON.parse(response.body)

        account_id_from_json = new_rates_on_json.map { |rate| rate["account_id"] }.uniq.compact
        site_id_from_json = new_rates_on_json.map { |rate| rate["site_id"] }.uniq.compact
        vendor_id_from_json = new_rates_on_json.map { |rate| rate["vendor_id"] }.uniq.compact
        service_code_id_from_json = new_rates_on_json.map do |rate|
          rate["service_code_id"]
        end.uniq.compact

        expect(account_id_from_json.size).to eq 1
        expect(account_id_from_json.first).to eq account.id

        expect(site_id_from_json.size).to eq 1
        expect(site_id_from_json.first).to eq site.id

        expect(vendor_id_from_json).to be_blank
        expect(service_code_id_from_json).to be_blank
      end
    end

    context 'when account_id, site_id and vendor_id are passed' do
      subject(:post_defaults) do
        post(
          :create_based_on_defaults,
          params: {
            rate: {
              account_id: account_id,
              site_id: site_id,
              vendor_id: vendor_id,
            },
          },
          as: :json
        )
      end

      let(:account_id) { account.id }
      let(:site_id) { site.id }
      let(:vendor_id) { 'CA12345' }

      it 'creates a new rate based on vendor_id defaults' do
        post_defaults

        new_rates_on_json = JSON.parse(response.body)

        account_id_from_json = new_rates_on_json.map { |rate| rate["account_id"] }.uniq.compact
        site_id_from_json = new_rates_on_json.map { |rate| rate["site_id"] }.uniq.compact
        vendor_id_from_json = new_rates_on_json.map { |rate| rate["vendor_id"] }.uniq.compact
        service_code_id_from_json = new_rates_on_json.map do |rate|
          rate["service_code_id"]
        end.uniq.compact

        expect(account_id_from_json.size).to eq 1
        expect(account_id_from_json.first).to eq account.id

        expect(site_id_from_json.size).to eq 1
        expect(site_id_from_json.first).to eq site.id

        expect(vendor_id_from_json.size).to eq 1
        expect(vendor_id_from_json.first).to eq vendor_id

        expect(service_code_id_from_json).to be_blank
      end
    end

    context 'when account_id and service_code_id are passed' do
      subject(:post_defaults) do
        post(
          :create_based_on_defaults,
          params: {
            rate: {
              account_id: account_id,
              site_id: nil,
              vendor_id: nil,
              service_code_id: service_code_id,
            },
          },
          as: :json
        )
      end

      let(:account_id) { account.id }
      let(:service_code_id) { service_code.id }

      it 'creates a new rate based on service_code defaults' do
        post_defaults

        new_rates_on_json = JSON.parse(response.body)

        account_id_from_json = new_rates_on_json.map { |rate| rate["account_id"] }.uniq.compact
        site_id_from_json = new_rates_on_json.map { |rate| rate["site_id"] }.uniq.compact
        vendor_id_from_json = new_rates_on_json.map { |rate| rate["vendor_id"] }.uniq.compact
        service_code_id_from_json = new_rates_on_json.map do |rate|
          rate["service_code_id"]
        end.uniq.compact

        expect(account_id_from_json.size).to eq 1
        expect(account_id_from_json.first).to eq account.id

        expect(site_id_from_json).to be_blank
        expect(vendor_id_from_json).to be_blank

        expect(service_code_id_from_json.size).to eq 1
        expect(service_code_id_from_json.first).to eq service_code_id
      end
    end
  end
end
