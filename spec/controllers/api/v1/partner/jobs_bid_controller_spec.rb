# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::JobsBidController, type: :controller do
  render_views

  stub_user_login(:rescue)

  let(:rescue_company) { user.company }
  let(:job) { create(:fleet_managed_job) }
  let(:reject_reason) { create(:job_reject_reason) }
  let(:auction) { create(:auction, job: job, max_target_eta: 60) }

  let!(:bid) do
    create :bid,
           status: Auction::Bid::REQUESTED,
           eta_mins: nil,
           eta_dttm: nil,
           bid_submission_dttm: nil,
           job: job,
           company: rescue_company,
           auction: auction
  end

  describe "POST bid" do
    let(:job_candidate) { create(:job_candidate, job: job, company: rescue_company) }

    it "submits a bid" do
      expect(Auction::Auction).to receive(:slack_notification).with(job, instance_of(String))
      post :bid, params: { id: job.id, minutes: "30", candidate_id: job_candidate.id, format: :json }
      expect(response).to be_successful
      bid.reload
      expect(bid.status).to eq Auction::Bid::SUBMITTED
      expect(bid.eta_mins).to eq 30
    end
  end

  describe "POST reject" do
    context 'first time rejecting a bid' do
      it 'rejects the company\'s bid on the job' do
        expect(Auction::Auction).to receive(:slack_notification).with(job, instance_of(String))
        post :reject, params: { id: job.id, reject_reason_id: reject_reason.id, reject_reason_info: "I dont wanna do this job", format: :json }
        expect(response).to be_successful
        bid.reload
        expect(bid.status).to eq Auction::Bid::PROVIDER_REJECTED
        expect(bid.job_reject_reason_info).to eq "I dont wanna do this job"
      end
    end

    context 'second time rejecting a bid' do
      it 'looks like it rejects the company\'s bid, but nothing changes' do
        # Step 1: Reject the bid the first time
        expect(Auction::Auction).to receive(:slack_notification).with(job, instance_of(String))
        post :reject, params: { id: job.id, reject_reason_id: reject_reason.id, reject_reason_info: "I dont wanna do this job", format: :json }
        expect(response).to be_successful
        # Step 2: Reject the bid a second time
        expect(Auction::Auction).to receive(:slack_notification).with(job, instance_of(String))
        post :reject, params: { id: job.id, reject_reason_id: reject_reason.id, reject_reason_info: "I already told you I don't want to do this job", format: :json }
        expect(response).to be_successful
        bid.reload
        expect(bid.status).to eq Auction::Bid::PROVIDER_REJECTED
        expect(bid.job_reject_reason_info).to eq "I dont wanna do this job"
        expect(bid.job_reject_reason_info).not_to eq "I already told you I don't want to do this job"
      end
    end
  end
end
