# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::InvoicesController, type: :controller do
  render_views

  include Requests::InvoiceHelpers

  stub_user_login(:rescue)

  let(:account) { create :account, company: company, client_company: fleet_company }
  let(:company) { user.company }
  let(:fleet_company) { create :fleet_company }
  let!(:flat_rate) { create :clean_flat_rate, company: company, live: true }

  let!(:invoice) do
    create :invoice,
           state: 'partner_new',
           sender: company,
           recipient: account,
           recipient_company: account.client_company,
           job: job
  end

  let(:job) do
    create :rescue_job,
           rescue_company: company,
           account: account,
           service_code: ServiceCode.find_by(name: 'Tow')
  end

  describe "POST samplerate" do
    subject(:post_samplerate) do
      post(
        :samplerate,
        params: {
          id: invoice.id,
          invoice: {
            rate_type: "FlatRate",
            invoice_vehicle_category_id: nil,
          },
        },
        as: :json
      )
    end

    it "returns a valid sample rate" do
      post_samplerate

      Rails.logger.debug response.body

      expect(json["id"]).to eql(nil)
      expect(json["job_id"]).to eql(job.id)
      expect(json["balance"]).to eql('75.0')
      expect(json["recipient_id"]).to eql(account.id)
      expect(json["recipient_type"]).to eql(account.class.name)
      expect(json["total_amount"]).to eql('75.00')
      expect(json["rate_type"]).to eql('FlatRate')
      expect(json["state"]).to eql('created')

      expect(json["line_items"].length).to eql(2)
      expect(json["line_items"][0]["net_amount"]).to eql("75.00")
      expect(json["line_items"][0]["description"]).to eql("Flat Rate")
      expect(json["line_items"][0]["job_id"]).to eql(job.id)
      expect(json["line_items"][0]["rate_id"]).to eql(flat_rate.id)
    end
  end

  describe "POST charge" do
    subject(:post_invoice_charge) do
      post(
        :charge,
        params: {
          id: invoice.id,
          invoice: {
            charge: {
              token: '123456',
              amount: 100.25,
              send_recipient_to: 'customer@email.com',
              memo: 'A note for the payment',
            },
          },
        },
        as: :json
      )
    end

    before do
      post_invoice_charge

      allow(Stripe::ChargeWorker).to(receive(:perform_async))
    end

    it "returns HTTP 200" do
      expect(response.code).to eq "200"
    end

    it "returns a created api_async_request" do
      async_request = API::AsyncRequest.find(json["async_request"]["id"])

      expect(async_request.request).to eq({
        "token" => '123456',
        "amount" => 100.25,
        "send_recipient_to" => 'customer@email.com',
        "memo" => "A note for the payment",
      })
      expect(async_request.target_id).to eq invoice.id
      expect(async_request.company_id).to eq company.id
      expect(async_request.user_id).to eq user.id
      expect(async_request.system).to eq 'InvoiceChargeCard'
    end
  end

  describe "POST refund" do
    subject(:post_invoice_payment_refund) do
      post(
        :refund,
        params: {
          id: invoice_with_payment.id,
          payment_id: invoice_with_payment.payments.first.id,
        },
        as: :json
      )
    end

    let(:invoice_with_payment) do
      create(
        :invoice,
        :with_payments,
        state: 'partner_new',
        sender: company,
        recipient: account,
        recipient_company: account.client_company
      )
    end

    before do
      post_invoice_payment_refund

      allow(Stripe::RefundWorker).to(
        receive(:perform_async)
      )
    end

    it "returns HTTP 200" do
      expect(response.code).to eq "200"
    end

    it "returns a created api_async_request" do
      async_request = API::AsyncRequest.find(json["async_request"]["id"])

      expect(async_request.request).to eq({
        "payment" => { "id" => invoice_with_payment.payments.first.id },
      })
      expect(async_request.target_id).to eq invoice_with_payment.id
      expect(async_request.company_id).to eq company.id
      expect(async_request.user_id).to eq user.id
      expect(async_request.system).to eq 'InvoiceRefundCard'
    end
  end

  describe "#update" do
    subject(:update_invoice) do
      patch(
        :update,
        params: params,
        as: :json
      )
    end

    let(:driver_notes) { Faker::Hipster.sentence }

    let(:params) do
      {
        companyId: company.id,
        id: invoice.id,
        invoice: {
          sender: {
            role_permissions: company.role_permissions,
            id: company.id,
          },
          job: {
            id: job.id,
            driver_notes: driver_notes,
          },
        },
      }
    end

    it "works" do
      expect { update_invoice }.not_to raise_error
      expect(response).to be_ok
      expect { job.reload }.to change(job, :driver_notes).to(driver_notes)
    end

    context 'when a not expected invoice attribute is passed in the request' do
      let(:params) do
        {
          companyId: company.id,
          id: invoice.id,
          invoice: {
            job_id: job.id,
            job: {
              id: job.id,
              driver_notes: driver_notes,
            },
            not_permitted_attr: 'any',
          },
        }
      end

      before do
        allow(InvoiceService::EditInvoiceForm).to receive(:new).and_call_original
        allow(Invoice).to receive(:find_by!).with(
          id: invoice.id, sender_id: user.company.id
        ).and_return(invoice)
      end

      it 'does not allow it to pass' do
        expect(InvoiceService::EditInvoiceForm).to receive(:new).with(
          invoice,
          {
            job_id: job.id,
            job_attributes: {
              id: job.id,
              driver_notes: driver_notes,
            },
          }
        )

        update_invoice
        expect(response).to be_ok
      end
    end

    context 'when moving to partner_sent' do
      let!(:invoice) { create :invoice, :auto_approvable, state: :partner_approved, sender: user.company }
      let(:params) do
        {
          id: invoice.id,
          state: 'partner_sent',
        }
      end

      it "works" do
        expect { update_invoice }.not_to raise_error
        expect(response).to be_ok
        expect(InvoiceAutoApproverWorker).to have_enqueued_sidekiq_job(invoice.id)
      end
    end
  end
end
