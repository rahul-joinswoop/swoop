# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::CustomAccountsController, vcr: true, type: :controller do
  render_views

  stub_user_login(:rescue, :admin)

  describe "GET #show" do
    subject(:get_custom_account) { get :show, as: :json }

    context 'when rescue_company has a custom_account' do
      let!(:custom_account) do
        create(
          :custom_account,
          upstream_account_id: 'acct_1CkIL2JmoY5nwfCu',
          company_id: user.company.id
        )
      end

      it 'responds with HTTP 200' do
        get_custom_account

        expect(response.code).to eq '200'
      end

      it 'responds with the custom account serialized body' do
        get_custom_account

        custom_account_json = json

        expect(custom_account_json['id']).to eq custom_account.id
        expect(custom_account_json['complete']).to be_truthy
        expect(custom_account_json['verification_failure']).to be_falsey
        expect(custom_account_json['upstream_account_id']).to eq 'acct_1CkIL2JmoY5nwfCu'
        expect(custom_account_json['deleted_at']).to be_nil
        expect(custom_account_json['linked']).to be_truthy
        expect(custom_account_json['bank_account']['bank_name']).to eq 'STRIPE TEST BANK'
        expect(custom_account_json['bank_account']['last4']).to eq '6789'
      end

      context 'when rescue_company has feature DISABLE_BANK_SETTINGS set' do
        let(:rescue_company) { user.company }

        before do
          allow(RescueCompany).to receive(:find).with(rescue_company.id).and_return(rescue_company)

          allow(rescue_company).to receive(:has_feature?).with(
            Feature::DISABLE_BANK_SETTINGS
          ).and_return(true)

          get_custom_account
        end

        it 'responds with HTTP 200' do
          expect(response.code).to eq '200'
        end

        it 'responds with empty json body' do
          expect(json).to be_empty
        end
      end

      context 'when a Stripe BankAccount is not found on Stripe end' do
        it 'responds with HTTP 200' do
          get_custom_account

          expect(response.code).to eq '200'
        end

        it 'responds with empty json body' do
          get_custom_account

          expect(json).to be_empty
        end
      end

      context 'when custom_account does not have a linked bank account yet' do
        let!(:custom_account) do
          create(
            :custom_account,
            upstream_account_id: 'acct_1CkIwJBjgK148kRP',
            company_id: user.company.id
          )
        end

        before do
          get_custom_account
        end

        it 'responds with HTTP 200' do
          expect(response.code).to eq '200'
        end

        it 'responds with the custom account serialized body' do
          custom_account_json = json

          expect(custom_account_json['id']).to eq custom_account.id
          expect(custom_account_json['complete']).to be_truthy
          expect(custom_account_json['verification_failure']).to be_falsey
          expect(custom_account_json['upstream_account_id']).to eq 'acct_1CkIwJBjgK148kRP'
          expect(custom_account_json['deleted_at']).to be_nil
          expect(custom_account_json['linked']).to be_falsey
        end

        it 'reponds without nested bank_account data' do
          custom_account_json = json

          expect(custom_account_json['bank_account']).to be_nil
        end
      end
    end

    context 'when rescue_company does not have a custom_account' do
      let!(:custom_account) { nil }

      before do
        get_custom_account
      end

      it 'responds with HTTP 200' do
        expect(response.code).to eq '200'
      end

      it 'responds with empty json body' do
        expect(json).to be_empty
      end
    end
  end
end
