# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::ReportsController, type: :controller do
  render_views

  stub_user_login(:rescue)

  describe "POST run_report" do
    context "when it is Payments Report" do
      subject(:run_report) do
        post(
          :run_report,
          params: {
            id: payments_report_mock.id,
            report: report_filters,
          },
          as: :json
        )
      end

      let(:payments_report_mock) do
        create(
          :report,
          name: 'Payment Reports',
          available_params:
            {
              "start_date": { "type": "eval", "method": "start_date_yesterday_beginning_of_day" },
              "end_date": { "type": "eval", "method": "end_date_yesterday_end_of_day" },
              "account": { "type": "account" },
              "transaction_type": { "type": "eval", "method": "transaction_type" },
              "invoice_payment_method": { "type": "eval", "method": "invoice_payment_methods" },
              "payment_status": { "type": "eval", "method": "payment_status" },
            }
        )
      end
      let(:rescue_company) { user.company }
      let!(:company_report) do
        create(:company_report, company: rescue_company, report: payments_report_mock)
      end

      before do
        rescue_company.reports << payments_report_mock

        allow(CompanyReport).to receive(:find_by).with(
          company_id: rescue_company.id,
          report_id: payments_report_mock.id.to_s
        ).and_return(company_report)

        allow(company_report).to receive(:run).and_return(nil)

        run_report
      end

      context 'when only start date and end date are passed' do
        let(:report_filters) do
          {
            start_date: "2018-01-02T03:00:00.000Z",
            end_date: "2018-06-20T02:59:59.999Z",
          }
        end

        it "runs the report with the expected params" do
          expect(company_report).to have_received(:run).with(
            user,
            {
              "start_date" => "2018-01-02T03:00:00.000Z",
              "end_date" => "2018-06-20T02:59:59.999Z",
            },
            nil,
            'csv',
            nil
          )
        end
      end

      context 'when transaction_type is added as a filter' do
        let(:report_filters) do
          {
            start_date: "2018-01-02T03:00:00.000Z",
            end_date: "2018-06-20T02:59:59.999Z",
            transaction_type: 1,
          }
        end

        it "runs the report with the expected params" do
          expect(company_report).to have_received(:run).with(
            user,
            {
              "start_date" => "2018-01-02T03:00:00.000Z",
              "end_date" => "2018-06-20T02:59:59.999Z",
              "transaction_type" => 1,
            },
            nil,
            'csv',
            nil
          )
        end
      end

      context 'when payment_status is added as a filter' do
        let(:report_filters) do
          {
            start_date: "2018-01-02T03:00:00.000Z",
            end_date: "2018-06-20T02:59:59.999Z",
            payment_status: 1,
          }
        end

        it "runs the report with the expected params" do
          expect(company_report).to have_received(:run).with(
            user,
            {
              "start_date" => "2018-01-02T03:00:00.000Z",
              "end_date" => "2018-06-20T02:59:59.999Z",
              "payment_status" => 1,
            },
            nil,
            'csv',
            nil
          )
        end
      end

      context 'when account is added as a filter' do
        let(:report_filters) do
          {
            start_date: "2018-01-02T03:00:00.000Z",
            end_date: "2018-06-20T02:59:59.999Z",
            account: 1,
          }
        end

        it "runs the report with the expected params" do
          expect(company_report).to have_received(:run).with(
            user,
            {
              "start_date" => "2018-01-02T03:00:00.000Z",
              "end_date" => "2018-06-20T02:59:59.999Z",
              "account" => 1,
            },
            nil,
            'csv',
            nil
          )
        end
      end

      context 'when invoice_payment_method is added as a filter' do
        let(:report_filters) do
          {
            start_date: "2018-01-02T03:00:00.000Z",
            end_date: "2018-06-20T02:59:59.999Z",
            invoice_payment_method: 1,
          }
        end

        it "runs the report with the expected params" do
          expect(company_report).to have_received(:run).with(
            user,
            {
              "start_date" => "2018-01-02T03:00:00.000Z",
              "end_date" => "2018-06-20T02:59:59.999Z",
              "invoice_payment_method" => 1,
            },
            nil,
            'csv',
            nil
          )
        end
      end

      context 'when not allowed filter is added' do
        let(:report_filters) do
          {
            start_date: "2018-01-02T03:00:00.000Z",
            end_date: "2018-06-20T02:59:59.999Z",
            not_expected: 1,
          }
        end

        it "runs the report without not allowed params" do
          expect(company_report).to have_received(:run).with(
            user,
            {
              "start_date" => "2018-01-02T03:00:00.000Z",
              "end_date" => "2018-06-20T02:59:59.999Z",
            },
            nil,
            'csv',
            nil
          )
        end
      end

      context 'when reports filter is empty' do # currently the case for Storage report
        let(:report_filters) do
          {}
        end

        it "runs the report without any filters" do
          expect(company_report).to have_received(:run).with(
            user,
            {},
            nil,
            'csv',
            nil
          )
        end
      end
    end
  end
end
