# frozen_string_literal: true

require 'rails_helper'

describe API::V1::Partner::RejectJobController do
  render_views

  stub_user_login(:rescue)

  subject(:post_create) do
    post(
      :create,
      params: {
        id: job.id,
        job: {
          id: job.id,
          status: Job::REJECTED,
          reject_reason_id: reject_reason_id,
        },
      }, as: :json
    )
  end

  let(:job_id) { job.id }
  let!(:account) do
    create(:account, company: rescue_company, client_company: fleet_company)
  end
  let(:rescue_company) { controller.api_user.company }

  before do
    allow(Job).to receive(:find_by!).with(id: job.id.to_s, rescue_company_id: rescue_company.id)
      .and_return(job)
    allow(job).to receive(:after_partner_reject)
    allow(job).to receive(:delete_invoice)
  end

  shared_examples 'a successful HTTP 200 request' do
    it 'responds with HTTP 200 OK' do
      post_create

      expect(response.code).to eq '200'
    end

    it 'respond with the job in the body' do
      post_create

      expect(json["id"]).to eq job.id
    end
  end

  shared_examples 'a job rejected on Swoop' do
    it 'calls job.after_partner_reject' do
      expect(job).to receive(:after_partner_reject)

      post_create
    end

    it 'calls job.delete_invoice' do
      expect(job).to receive(:delete_invoice)

      post_create
    end

    it 'sets job.partner_dispatcher with controller.api_user' do
      post_create

      job.reload

      expect(job.partner_dispatcher).to eq controller.api_user
    end

    it 'sets job.last_touched_by with controller.api_user' do
      post_create

      job.reload

      expect(job.last_touched_by).to eq controller.api_user
    end

    it 'sets job.reject_reason_id' do
      post_create

      job.reload

      expect(job.reject_reason_id).to eq reject_reason_id
    end

    it 'sets job.status = rejected' do
      post_create

      job.reload

      expect(job.status).to eq Job::STATUS_REJECTED
    end
  end

  context 'when Job is FleetMotorClubJob' do
    let(:job) do
      create :fleet_motor_club_job, {
        status: job_status,
        fleet_company: fleet_company,
        adjusted_created_at: Time.now,
        rescue_company: rescue_company,
        issc_dispatch_request: issc_dispatch_request,
        updated_at: '2018-07-26 18:08:04',
      }
    end

    let(:issc_dispatch_request) do
      create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end

    # ISSC
    context 'when job.digital_dispatcher is ISSC' do
      let(:issc) { create(:ago_issc, company: rescue_company, api_access_token: nil) }

      shared_examples 'a job to be rejected on Issc' do
        it 'schedules IsscDispatchResponseReject for the Job' do
          expect(IsscDispatchResponseReject).to receive(:perform_async).with(job.id)

          post_create
        end
      end

      context 'when job.status is assigned' do
        let(:job_status) { Job::STATUS_ASSIGNED }

        context 'and fleet_company is Agero' do
          let(:fleet_company) { create(:fleet_company, :ago) }

          context 'and reject reason is PaymentIssue' do
            let(:reject_reason_id) { create(:job_reject_reason, text: 'PaymentIssue').id }

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on Issc'
          end

          context 'and reject reason is EquipmentNotAvailable' do
            let(:reject_reason_id) { create(:job_reject_reason, text: 'EquipmentNotAvailable').id }

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on Issc'
          end

          context 'and reject reason is OutOfArea' do
            let(:reject_reason_id) { create(:job_reject_reason, text: 'OutOfArea').id }

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on Issc'
          end

          context 'and reject reason is Other' do
            let(:reject_reason_id) { create(:job_reject_reason, text: 'Other').id }

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on Issc'
          end
        end
      end
    end

    # RSC
    context 'when job.digital_dispatcher is RSC' do
      let(:api_access_token) { create(:api_access_token) }

      let(:issc) do
        create(
          :ago_issc,
          company: rescue_company,
          api_access_token: api_access_token,
          system: Issc::RSC_SYSTEM
        )
      end

      shared_examples 'a job to be rejected on RSC' do
        it 'schedules Agero::Rsc::RejectDispatchWorker for the Job' do
          expect(Agero::Rsc::RejectDispatchWorker).to receive(:perform_async).with(job.id)

          post_create
        end
      end

      context 'when job.status is assigned' do
        let(:job_status) { Job::STATUS_ASSIGNED }

        context 'and fleet_company is Agero' do
          let(:fleet_company) { create(:fleet_company, :ago) }

          context "and reject reason is 'Do not accept payment type'" do
            let(:reject_reason_id) do
              create(:job_reject_reason, text: 'Do not accept payment type').id
            end

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on RSC'
          end

          context "and reject reason is 'No longer offer service'" do
            let(:reject_reason_id) do
              create(:job_reject_reason, text: 'No longer offer service').id
            end

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on RSC'
          end

          context "and reject reason is 'Out of my coverage area'" do
            let(:reject_reason_id) do
              create(:job_reject_reason, text: 'Out of my coverage area').id
            end

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on RSC'
          end

          context 'and reject reason is Other' do
            let(:reject_reason_id) { create(:job_reject_reason, text: 'Other').id }

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on RSC'
          end

          context "and reject reason is 'Proper equipment not available'" do
            let(:reject_reason_id) do
              create(:job_reject_reason, text: 'Proper equipment not available').id
            end

            it_behaves_like 'a successful HTTP 200 request'
            it_behaves_like 'a job rejected on Swoop'
            it_behaves_like 'a job to be rejected on RSC'
          end
        end
      end
    end
  end

  context 'when job is a FleetManagedJob' do
    let(:job) do
      create :fleet_managed_job, {
        status: job_status,
        fleet_company: fleet_company,
        adjusted_created_at: Time.now,
        rescue_company: rescue_company,
      }
    end

    let(:fleet_company) { create(:fleet_company) }

    context 'when job.status is assigned' do
      let(:job_status) { Job::STATUS_ASSIGNED }

      context 'and reject_reason has a reason associated' do
        let(:reject_reason_reason) { create(:reason, issue: issue) }
        let(:issue) { create(:issue, name: 'Missed ETA') }
        let(:reject_reason_id) do
          create(:job_reject_reason, text: 'Other', reason: reject_reason_reason).id
        end

        it_behaves_like 'a successful HTTP 200 request'
        it_behaves_like 'a job rejected on Swoop'

        it 'adds one explanation to job.explanations' do
          expect { post_create }.to change(job.explanations, :count).by(1)
        end
      end
    end
  end
end
