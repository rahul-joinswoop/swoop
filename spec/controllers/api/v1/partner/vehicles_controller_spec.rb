# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::VehiclesController, type: :controller do
  render_views
  stub_user_login(:rescue)

  describe "location_update" do
    it "schedules a vehicle location update if vehicle_location_update present", freeze_time: true do
      patch :location_update, params: { id: '123', vehicle: { lat: '45.1', lng: '-110.1' } }
      expect(response.status).to eq 200
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq({})
      expect(UpdateVehicleLocationWorker).to have_enqueued_sidekiq_job(user.company_id, 123, 45.1, -110.1, Time.now.utc.iso8601)
    end
  end

  describe "PATCH update" do
    let(:vehicle) { create(:rescue_vehicle) }

    before { user.update(company: vehicle.company) }

    context "driver's assignment to the vehicle" do
      let(:driver) { create(:user) }

      it "assigns requested user as a driver" do
        expect do
          patch :update, params: { id: vehicle.id, vehicle: { driver_id: driver.id }, format: :json }
        end.to change { vehicle.reload.driver }.to(driver)

        expect(response.status).to eq 200
        expect(response.content_type).to eq "application/json"
      end

      it "removes driver assignment from the vehicle" do
        vehicle.update!(driver: user)

        expect do
          patch :update, params: { id: vehicle.id, vehicle: { driver_id: nil }, format: :json }
        end.to change { vehicle.reload.driver }.to(nil)
      end

      it "keeps driver assignment unchanged" do
        vehicle.update!(driver: user)

        expect do
          patch :update, params: { id: vehicle.id, vehicle: {}, format: :json }
        end.not_to change { vehicle.reload.driver }
      end

      it "updates vehicle attributes and assign the driver" do
        make = 'Toyota'

        expect do
          patch :update, params: { id: vehicle.id, vehicle: { make: make, driver_id: driver.id }, format: :json }
        end.to change { vehicle.reload.make }.to(make)
          .and change(vehicle, :driver).to(driver)
      end

      it "schedules a worker to update the vehicle location", freeze_time: true do
        patch :update, params: { id: vehicle.id, vehicle: { lat: 45, lng: -110 }, format: :json }
        expect(response).to be_successful
        expect(UpdateVehicleLocationWorker).to have_enqueued_sidekiq_job(vehicle.company_id, vehicle.id, 45, -110, Time.now.utc.iso8601)
      end
    end
  end
end
