# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::QBController do
  stub_user_login(:rescue, :admin)

  describe "GET token" do
    subject(:get_token) do
      get :token
    end

    before :each do
      create(:qb_token, company_id: create(:user, :rescue, :admin).company.id)
    end

    let(:rescue_company) { create(:rescue_company, :finish_line) }

    it 'returns the QuickBooks token object' do
      get_token

      token = JSON.parse(response.body)

      expect(token["company_id"]).to eql(rescue_company.id)
      expect(token["token"]).not_to be_nil
    end

    context 'when rescue_company does not have a qb token' do
      it 'returns 404' do
        allow(QB::Token).to receive(:find_by_company_id).and_return(nil)

        get_token

        expect(response.status).to eq(404)
        expect(response.body).to eq("")
      end
    end
  end
end
