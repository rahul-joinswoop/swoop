# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::SitesController, type: :controller do
  let!(:user) { create :user, :rescue_with_sites }
  let(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }

  before(:each) do
    authenticate_request(controller.request, token)
  end

  describe 'POST create' do
    def created_site
      Site.find_by_name('Awesome Partner Site')
    end

    subject(:create_site) do
      post :create, params: { site: request_data, format: :json }
    end

    let(:request_data) do
      {
        name: 'Awesome Partner Site',
        location: {
          lat: -23.2000805,
          lng: -47.293538,
          place_id: "ChIJ7frs2GVSz5QRp5SW3cXtdOc",
          address: "Salto - SP, Brasil",
          exact: true,
          location_type_id: location_type.id,
        },
      }
    end

    let(:location_type) { create(:location_type) }

    it 'creates a new Site' do
      expect { create_site }.to change(Site, :count).by(1)
    end

    it 'contains the expected location type' do
      create_site

      expect(created_site.location.location_type_id).to eq location_type.id
    end

    it 'contains the expected location' do
      create_site

      site_location = created_site.location

      expect(site_location.lat).to eq(-23.2000805)
      expect(site_location.lng).to eq(-47.293538)
      expect(site_location.place_id).to eq 'ChIJ7frs2GVSz5QRp5SW3cXtdOc'
      expect(site_location.address).to eq 'Salto - SP, Brasil'
      expect(site_location.exact).to eq true
    end
  end

  describe 'PATCH update' do
    def updated_site
      Site.find_by_name('Partner Site')
    end

    subject(:update_site) do
      patch :update, params: { id: partner_site.id, site: request_data, format: :json }
    end

    let(:request_data) do
      {
        location: {
          location_type_id: location_type.id,
        },
      }
    end

    let(:location_type) { create(:location_type, name: 'Another Location') }
    let(:partner_site) { create(:partner_site, name: 'Partner Site') }

    it 'contains the expected location type' do
      update_site

      expect(updated_site.location.location_type.name).to eq 'Another Location'
    end

    context 'when location type is null' do
      let(:request_data) do
        {
          location: {
            location_type_id: nil,
          },
        }
      end

      it 'sets site.location_type as null' do
        update_site

        expect(updated_site.location.location_type).to eq nil
      end
    end
  end
end
