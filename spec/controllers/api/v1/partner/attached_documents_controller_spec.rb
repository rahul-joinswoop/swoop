# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::AttachedDocumentsController, type: :controller do
  let!(:flt) { create :rescue_company, name: "Finish Line Towing" }
  let!(:user) { create :user, :rescue, company: flt }
  let(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }
  let(:bag) { "{\"id\":\"de150e1fe4745058b05a94322367baf7\",\"storage\":\"cache\",\"metadata\":{\"size\":799,\"filename\":\"Query Results.csv\",\"mime_type\":\"text\/csv\"}}" }

  before(:each) do
    authenticate_request(controller.request, token)
    allow_any_instance_of(AttachedDocument).to receive(:finalize_document)
  end

  describe 'POST create' do
    subject(:create_attached_document) do
      @job = create(:rescue_job, fleet_company: flt, rescue_company: flt)
      # FactoryBot.attributes_for(:comment, :article_id => @article)
      post :create, params: { job_id: @job, attached_document: request_data, format: :json }
    end

    let(:request_data) do
      {

        document: bag,
        type: "AttachedDocument::SignatureImage",
        location: {
          lat: -23.2000805,
          lng: -47.293538,
          place_id: "attached_doc_place_id",
          address: "Salto - SP, Brasil",
          exact: true,
          location_type_id: location_type.id,
        },

      }
    end

    let(:location_type) { create(:location_type) }

    it 'creates Attached Document', vcr: true do
      expect { create_attached_document }.to change(AttachedDocument, :count).by(1)
      expect(Job.count).to eq 1
      job = Job.first
      attachment = job.attached_documents.first
      expect(attachment).not_to be nil
      expect(attachment.document.data).to eq JSON.load(bag)
      expect(attachment.location.place_id).to eq "attached_doc_place_id"
    end

    it 'finalizes the document on s3' do
      expect_any_instance_of(AttachedDocument).to receive(:finalize_document)

      create_attached_document
    end

    it 'returns HTTP 201' do
      create_attached_document

      expect(response.code).to eq "201"
    end
  end
end
