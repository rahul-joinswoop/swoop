# frozen_string_literal: true

require "rails_helper"

describe API::V1::Partner::JobsController, type: :controller do
  render_views

  let!(:user) { create :user, :rescue_with_sites }
  let(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }

  before(:each) do
    authenticate_request(controller.request, token)
  end

  describe "POST create" do
    before do
      create :audit_job_status, job_status: create(:job_status, :created)
      create :audit_job_status, job_status: create(:job_status, :pending)
    end

    let!(:vehicle_category) { create :vehicle_category }
    let!(:symptom) { create :symptom }
    let!(:department) { create :department }
    let!(:tire_change_service) { create :service_code, name: ServiceCode::TIRE_CHANGE }
    let(:account) { create :account, company: user.company }

    context "when an account id is not given" do
      subject(:post_create_job) do
        post :create, params: { job: job_create_params }, as: :json
      end

      let(:job_create_params) do
        attributes_for(:job).merge(account: { id: nil, name: 'New Account' })
      end

      it "does not create a new Job" do
        expect { post_create_job }.to avoid_changing(RescueJob, :count)
      end
    end

    def created_job
      Job.find(JSON.parse(response.body)['id'])
    end

    context "when an account id is given" do
      subject(:post_create_job) do
        post :create, params: { job: job_create_params }, as: :json
      end

      let(:job_create_params) do
        {
          send_sms_to_pickup: false,
          review_sms: true,
          partner_sms_track_driver: true,
          partner_dispatcher_id: user.id,
          site_id: user.company.sites.first.id,
          account: {
            id: account.id,
            name: account.name,
            accounting_email: account.accounting_email,
            primary_email: nil,
            notes: account.notes,
            phone: nil,
            fax: nil,
            contact_name: nil,
            contact_phone: nil,
            contact_email: nil,
            accounting_email_cc: account.accounting_email_cc,
            accounting_email_bcc: account.accounting_email_bcc,
            disable_qb_import: nil,
            hide_timestamps_on_invoice: nil,
            location: nil,
            physical_location: nil,
            company: nil,
            color: nil,
            po_required_at_job_creation: nil,
            auto_accept: nil,
            auto_eta: nil,
            deleted_at: nil,
          },
          partner_department_id: department.id,
          caller: {
            full_name: "Caller Name",
          },
          customer: {
            full_name: 'Customer One',
            phone: '+12315555555',
          },
          symptom_id: symptom.id,
          make: 'Tesla',
          model: 'Model S',
          year: '2016',
          color: 'Red',
          vin: '1HGCM82633A004352',
          odometer: '1337',
          service_location: {
            lat: 37.7800769528017,
            lng: -122.438335418701,
            place_id: 'ChIJcc2VKLGAhYARFYsEkYhEE1U',
            address: '1100 Divisadero St, San Francisco, CA 94115, USA',
            exact: true,
          },
          drop_location: {
            address: 'New York, NY',
            lat: 40.7127753,
            lng: -74.0059728,
            exact: true,
            place_id: 'ChIJOwg_06VPwokRYv534QaPC8g',
            site_id: user.company.sites.first.id,
          },
          pickup_contact: {
            full_name: 'Pickup Contact',
            phone: '+12315555555',
          },
          po_number: '123',
          will_store: true,
          service_code_id: tire_change_service.id,
          invoice_vehicle_category_id: vehicle_category.id,
          second_tow_scheduled_for: '2018-01-31T15:37:00.000Z',
        }
      end

      it "creates a new Job" do
        expect { post_create_job }.to change(RescueJob, :count).by(1)
      end

      it 'creates a new Job with given attributes' do
        post_create_job

        expect(created_job.instance_of?(RescueJob)).to be_truthy

        expect(created_job.driver.vehicle.make).to eq 'Tesla'
        expect(created_job.driver.vehicle.model).to eq 'Model S'
        expect(created_job.driver.vehicle.year).to eq 2016
        expect(created_job.driver.vehicle.color).to eq 'Red'
        expect(created_job.driver.vehicle.vin).to eq '1HGCM82633A004352'
        expect(created_job.odometer).to eq '1337'.to_f
        expect(created_job.driver.user.name).to eq 'Customer One'
        expect(created_job.stranded_vehicle).not_to be_nil

        expect(created_job.caller.full_name).to eq 'Caller Name'

        expect(created_job.service_location.lat).to eq 37.7800769528017
        expect(created_job.service_location.lng).to eq(-122.438335418701)
        expect(created_job.service_location.place_id).to eq 'ChIJcc2VKLGAhYARFYsEkYhEE1U'
        expect(created_job.service_location.address).to eq '1100 Divisadero St, San Francisco, CA 94115, USA'
        expect(created_job.service_location.exact).to be_truthy

        expect(created_job.service_code_id).to eq tire_change_service.id

        expect(created_job.pickup_contact.full_name).to eq 'Pickup Contact'
        expect(created_job.pickup_contact.phone).to eq '+12315555555'

        expect(created_job.po_number).to eq '123'

        expect(created_job.fleet_company_id).to eq controller.api_company.id
        expect(created_job.originating_company_id).to eq controller.api_company.id
        expect(created_job.user_id).to eq controller.api_user.id
        expect(created_job.partner_dispatcher_id).to eq controller.api_user.id

        expect(created_job.rescue_company.id).to eq controller.api_company.id
      end

      it 'created expected audit_job_status' do
        post_create_job

        audit_job_statuses = created_job.audit_job_statuses

        expect(audit_job_statuses.count).to eq 3
        expect(audit_job_statuses.first.job_status.name).to eq('Initial')
        expect(audit_job_statuses.second.job_status.name).to eq('Created')
        expect(audit_job_statuses.third.job_status.name).to eq('Pending')
      end
    end

    context 'when saving job as draft' do
      subject(:post_create_job) do
        post :create,
             params: {
               job: {
                 priority_response: false,
                 send_sms_to_pickup: false,
                 review_sms: false,
                 partner_sms_track_driver: false,
                 text_eta_updates: false,
               }, save_as_draft: true,
             },
             as: :json
      end

      it 'creates a new Job with minimum attributes' do
        post_create_job

        expect(created_job.instance_of?(RescueJob)).to be_truthy

        expect(created_job.fleet_company_id).to eq controller.api_company.id
        expect(created_job.originating_company_id).to eq controller.api_company.id
        expect(created_job.user_id).to eq controller.api_user.id
        expect(created_job.partner_dispatcher_id).to eq controller.api_user.id
        expect(created_job.draft?).to be_truthy
        expect(created_job.driver).not_to be_nil
        expect(created_job.stranded_vehicle).not_to be_nil
      end

      it 'created expected audit_job_status' do
        post_create_job

        audit_job_statuses = created_job.audit_job_statuses

        expect(audit_job_statuses.count).to eq 2
        expect(audit_job_statuses.first.job_status.name).to eq('Initial')
        expect(audit_job_statuses.second.job_status.name).to eq('Draft')
      end
    end

    context 'when saving job as predraft' do
      subject(:post_create_job) do
        post :create,
             params: {
               job: {
                 priority_response: false,
                 send_sms_to_pickup: false,
                 review_sms: false,
                 partner_sms_track_driver: false,
                 text_eta_updates: false,
               }, save_as_predraft: true,
             },
             as: :json
      end

      it 'creates a new Job with minimum attributes' do
        post_create_job

        expect(created_job.instance_of?(RescueJob)).to be_truthy

        expect(created_job.fleet_company_id).to eq controller.api_company.id
        expect(created_job.originating_company_id).to eq controller.api_company.id
        expect(created_job.user_id).to eq controller.api_user.id
        expect(created_job.partner_dispatcher_id).to eq controller.api_user.id
        expect(created_job.draft?).to be_falsey
        expect(created_job.predraft?).to be_truthy
        expect(created_job.driver).not_to be_nil
        expect(created_job.stranded_vehicle).not_to be_nil
      end

      it 'created expected audit_job_status' do
        post_create_job

        audit_job_statuses = created_job.audit_job_statuses

        expect(audit_job_statuses.count).to eq 2
        expect(audit_job_statuses.first.job_status.name).to eq('Initial')
        expect(audit_job_statuses.second.job_status.name).to eq('Predraft')
      end
    end

    context 'using a mobile example' do
      subject(:post_create_job) do
        post :create, params: {
          job: job_create_params, save_as_draft: false,
        }, as: :json
      end

      let(:account) { create(:account, name: 'AAA Non Contracted', company: user.company) }
      let(:customer_type_po) { create(:customer_type, name: 'PO') }
      let(:tire_change_service) { create(:service_code, name: 'Tire Service', addon: false) }
      let(:rescue_vehicle) { create(:rescue_vehicle, company: user.company) }
      let(:location_type_highway) { create(:location_type, name: 'Highway') }

      let(:job_create_params) do
        {
          send_sms_to_pickup: false,
          review_sms: true,
          partner_sms_track_driver: true,
          account: {
            id: account.id,
            name: account.name,
          },
          customer_type_id: customer_type_po.id,
          po_number: "13075",
          customer: {
            full_name: "Glendon Domes",
            phone: "+17196419708",
          },
          caller: {
            full_name: "Aaa ",
          },
          will_store: false,
          service_code_id: tire_change_service.id,
          invoice_vehicle_category_id: vehicle_category.id,
          # rescue_vehicle_id: rescue_vehicle.id,
          rescue_driver_id: nil,
          service_location: {
            lat: 39.1723401,
            lng: -103.9285378,
            address: "US-24, Matheson, CO 80830, USA",
            exact: true,
            location_type_id: location_type_highway.id,
          },
          make: "Chevy",
          model: "Avero ",
          color: "Red",
          year: "2005",
          partner_notes: "Has a good spare",
        }
      end

      it 'created expected audit_job_status' do
        post_create_job

        audit_job_statuses = created_job.audit_job_statuses

        expect(audit_job_statuses.count).to eq 3
        expect(audit_job_statuses.first.job_status.name).to eq('Initial')
        expect(audit_job_statuses.second.job_status.name).to eq('Created')
        expect(audit_job_statuses.third.job_status.name).to eq('Pending')
      end
    end
  end

  describe 'PATCH update' do
    subject(:patch_update) do
      put :update,
          params: {
            id: job.id, job: {
              id: job.id,
              partner_dispatcher_id: nil,
              customer: {
                id: job.customer.id,
                full_name: 'Customer Name',
              },
              rescue_driver_id: rescue_driver_id,
              bta: bta,
              get_location_sms: get_location_sms,
            },
          },
          as: :json
    end

    let(:job) do
      create(:rescue_job,
             fleet_company: user.company,
             rescue_company: user.company,
             status: JobStatus::ID_MAP[JobStatus::PENDING],
             rescue_driver: rescue_driver)
    end

    let(:rescue_driver) { user }
    let(:rescue_driver_id) { create(:user, company: user.company, roles: [:driver]).id }
    let(:bta) { nil }
    let(:get_location_sms) { false }

    def patch_update_job
      patch_update

      job.reload
    end

    context 'when job.dispatcher_id is nil' do
      it 'sets the api_user as the dispatcher_id' do
        expect(job.partner_dispatcher_id).to be_nil

        patch_update_job

        expect(job.partner_dispatcher_id).to eq controller.api_user.id
      end

      it 'sets the customers full name' do
        expect(job.customer.full_name).not_to eq 'Customer Name'

        patch_update_job

        expect(job.customer.full_name).to eq 'Customer Name'
      end
    end

    it 'sets the rescue_driver_id' do
      allow_any_instance_of(RescueJob).to receive(:partner_set_driver)

      expect(job.rescue_driver_id).to eq user.id
      expect_any_instance_of(RescueJob).to receive(:partner_set_driver)

      patch_update_job

      expect(job.rescue_driver_id).to eq rescue_driver_id
    end

    context 'when removing the rescue driver' do
      let(:rescue_driver_id) { nil }

      it 'sets the rescue_driver_id as nil' do
        allow_any_instance_of(RescueJob).to receive(:partner_remove_driver)

        expect(job.rescue_driver_id).to eq user.id
        expect_any_instance_of(RescueJob).to receive(:partner_remove_driver)

        patch_update_job

        expect(job.rescue_driver_id).to be_nil
      end
    end

    it 'triggers HandleToa#call' do
      allow_any_instance_of(HandleToa).to receive(:call)

      expect_any_instance_of(HandleToa).to receive(:call)

      patch_update_job
    end

    it 'calls update_history' do
      allow_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

      expect_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

      patch_update_job
    end

    context 'when BTA is nil' do
      it 'doesnt call bid methods on job' do
        expect_any_instance_of(RescueJob).not_to receive(:bid)
        expect_any_instance_of(RescueJob).not_to receive(:submitted_bid)

        patch_update_job
      end
    end

    context 'when rescue_driver is not changed' do
      subject(:patch_update) do
        put :update,
            params: {
              id: job.id, job: {
                id: job.id,
                partner_dispatcher_id: nil,
                customer: {
                  id: job.customer.id,
                  full_name: 'Customer Name',
                },
                bta: bta,
              },
            }, as: :json
      end

      context 'when BTA is set' do
        let(:bta) { Time.now + 20.minutes }

        it 'calls bid methods on job' do
          expect_any_instance_of(RescueJob).to receive(:bid)
          expect_any_instance_of(RescueJob).to receive(:submitted_bid)

          patch_update_job
        end

        context 'when job is FleetMotorClubJob' do
          let!(:account) do
            create(:account, company: rescue_company, client_company: fleet_company)
          end

          let!(:job) do
            create :fleet_motor_club_job, {
              fleet_company: fleet_company,
              adjusted_created_at: Time.now,
              rescue_company: rescue_company,
              issc_dispatch_request: issc_dispatch_request,
              updated_at: '2018-07-26 18:08:04',
              status: Job::ASSIGNED,
            }
          end

          let(:issc_dispatch_request) do
            create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
          end

          let(:rescue_company) { controller.api_user.company }
          let(:fleet_company) { create(:fleet_company, :ago) }

          context 'and job digital_dispatcher is Issc' do
            let(:issc) { create(:ago_issc, company: rescue_company) }

            it 'calls IsscDispatchResponseAccept.perform_async' do
              expect(IsscDispatchResponseAccept).to receive(:perform_async)

              patch_update
            end
          end

          context 'and job digital_dispatcher is RSC' do
            let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM) }

            it 'calls IsscDispatchResponseAccept.perform_async' do
              expect(Agero::Rsc::AcceptDispatchWorker).to receive(:perform_async)

              patch_update
            end
          end
        end
      end
    end

    it 'sets jobs.last_touched_by' do
      expect(job.last_touched_by).to be_nil

      patch_update_job

      expect(job.last_touched_by).to eq(controller.api_user)
    end

    context 'when get_location_sms is true' do
      let(:get_location_sms) { true }

      it 'adds +1 to job.get_location_attempts' do
        patch_update_job

        expect(job.get_location_attempts).to eq 1
      end
    end

    it 'calls job.fixup_locations_job' do
      allow_any_instance_of(RescueJob).to receive(:fixup_locations)
      expect_any_instance_of(RescueJob).to receive(:fixup_locations)

      patch_update_job
    end

    it 'calls controller.set_platform' do
      allow_any_instance_of(API::Jobs::Update).to receive(:set_platform)
      expect_any_instance_of(API::Jobs::Update).to receive(:set_platform)

      patch_update_job
    end

    context 'when it\'s a Draft Job' do
      subject(:patch_update) do
        put :update,
            params: {
              id: job.id, job: {
                id: job.id,
                partner_dispatcher_id: nil,
                customer: {
                  id: job.customer.id,
                  full_name: 'Customer Name',
                },
                rescue_driver_id: rescue_driver_id,
                bta: bta,
                get_location_sms: get_location_sms,
              }, save_as_draft: true,
            },
            as: :json
      end

      let(:job) do
        create(:rescue_job,
               fleet_company: user.company,
               rescue_company: user.company,
               status: JobStatus::ID_MAP[JobStatus::DRAFT],
               rescue_driver: rescue_driver)
      end

      before do
        JobStatus.where(id: JobStatus::CREATED, name: JobStatus::ID_MAP[JobStatus::CREATED]).first_or_create!
        JobStatus.where(id: JobStatus::DRAFT, name: JobStatus::ID_MAP[JobStatus::DRAFT]).first_or_create!
        JobStatus.where(id: JobStatus::PENDING, name: JobStatus::ID_MAP[JobStatus::PENDING]).first_or_create!
      end

      it 'sets jobs.last_touched_by' do
        expect(job.last_touched_by).to be_nil

        patch_update_job

        expect(job.last_touched_by).to eq(controller.api_user)
      end

      it 'calls job.fixup_locations' do
        allow_any_instance_of(RescueJob).to receive(:fixup_locations)
        expect_any_instance_of(RescueJob).to receive(:fixup_locations)

        patch_update_job
      end

      it 'keeps the status as Draft' do
        patch_update_job

        expect(job.status).to eq Job::STATUS_DRAFT
      end

      it 'updates attributes accordingly' do
        expect(job.driver.user.name).not_to eq 'Customer Name'

        patch_update_job

        expect(job.driver.user.name).to eq 'Customer Name'
      end

      it 'calls update_history' do
        allow_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

        expect_any_instance_of(API::Jobs::UpdateHistory).to receive(:call)

        patch_update_job
      end

      it 'doesnt call other non-draft methods' do
        expect_any_instance_of(RescueJob).not_to receive(:partner_set_driver)
        expect_any_instance_of(RescueJob).not_to receive(:partner_remove_driver)
        expect_any_instance_of(RescueJob).not_to receive(:schedule_create_estimate)
        expect_any_instance_of(RescueJob).not_to receive(:send_swoop_alert)
        expect_any_instance_of(RescueJob).not_to receive(:slack_after_create)

        patch_update_job
      end

      context 'when save_as_draft param is not passed on the request' do
        subject(:patch_update) do
          put :update,
              params: {
                id: job.id, job: {
                  id: job.id,
                  customer: {
                    id: job.customer.id,
                    full_name: 'Customer Name',
                  },
                  scheduled_for: scheduled_for,
                  text_eta_updates: text_eta_updates,
                },
              },
              as: :json
        end

        let(:scheduled_for) { nil }

        let(:text_eta_updates) { false }

        it 'calls job#set_service_location_user' do
          allow_any_instance_of(RescueJob).to receive(:set_service_location_user)
          expect_any_instance_of(RescueJob).to receive(:set_service_location_user)

          patch_update_job
        end

        it 'calls job#schedule_create_estimate' do
          allow_any_instance_of(RescueJob).to receive(:schedule_create_estimate)
          expect_any_instance_of(RescueJob).to receive(:schedule_create_estimate)

          patch_update_job
        end

        it 'calls job#schedule_create_storage_invoice_batch' do
          allow_any_instance_of(RescueJob).to receive(:schedule_create_storage_invoice_batch)
          expect_any_instance_of(RescueJob).to receive(:schedule_create_storage_invoice_batch)

          patch_update_job
        end

        context 'when job has a rescue driver' do
          it 'moves the job to dispatched status' do
            patch_update_job

            expect(job.status).to eq Job::STATUS_DISPATCHED
          end
        end

        context 'when job has no rescue driver' do
          let(:rescue_driver) { nil }
          let(:rescue_driver_id) { nil }

          it 'moves the job to pending status' do
            patch_update_job

            expect(job.status).to eq Job::STATUS_PENDING
          end
        end

        it 'calls job#send_swoop_alert' do
          allow_any_instance_of(RescueJob).to receive(:send_swoop_alert)
          expect_any_instance_of(RescueJob).to receive(:send_swoop_alert)

          patch_update_job
        end

        it 'calls job#slack_after_create' do
          allow_any_instance_of(RescueJob).to receive(:slack_after_create)
          expect_any_instance_of(RescueJob).to receive(:slack_after_create)

          patch_update_job
        end

        it 'enqueues EmailStatus::Create' do
          allow(JobStatusEmailSender).to receive(:new)
          expect(JobStatusEmailSender).to receive(:new).with(job.id, JobStatusEmail::Created.name)

          patch_update_job
        end

        it 'doesnt send confirmation sms' do
          expect_any_instance_of(RescueJob).not_to receive(:send_confirmation_sms)

          patch_update_job
        end

        it 'calls job#send_get_location' do
          allow_any_instance_of(RescueJob).to receive(:send_get_location)
          expect_any_instance_of(RescueJob).to receive(:send_get_location)

          patch_update_job
        end

        it 'doesnt call job#scheduled_system_alert_create' do
          expect_any_instance_of(RescueJob).not_to receive(:scheduled_system_alert_create)

          patch_update_job
        end

        context 'when text_eta_updates is passed on the request' do
          let(:text_eta_updates) { true }

          it 'calls job#send_confirmation_sms' do
            allow_any_instance_of(RescueJob).to receive(:send_confirmation_sms)
            expect_any_instance_of(RescueJob).to receive(:send_confirmation_sms)

            patch_update_job
          end

          it 'doesnt call job#send_get_location' do
            expect_any_instance_of(RescueJob).not_to receive(:send_get_location)

            patch_update_job
          end
        end

        context 'when job is scheduled' do
          let(:scheduled_for) { '2018-02-06T18:47:00.000Z' }

          it 'enqueues EmailStatus::Scheduled' do
            allow_any_instance_of(RescueJob).to receive(:send_confirmation_sms)

            allow(JobStatusEmailSender).to receive(:new)
            expect(JobStatusEmailSender).to receive(:new).with(job.id, JobStatusEmail::Scheduled.name)

            patch_update_job
          end
        end
      end
    end
  end

  describe "POST send_details_email" do
    subject(:post_send_details_email) do
      post :send_details_email, params: {
        id: job.id,
        email_recipients: {
          to: 'to@email.com, another_to@email.com',
          cc: 'cc@email.com, another_cc@email.com',
          bcc: 'bcc@email.com, another_bcc@email.com',
        },
      }, as: :json
    end

    let(:job) { create(:rescue_job) }

    it 'calls API::Jobs::SendDetailsEmail' do
      allow(API::Jobs::SendDetailsEmail).to receive(:call).with(
        { input:
          {
            to: 'to@email.com, another_to@email.com',
            cc: 'cc@email.com, another_cc@email.com',
            bcc: 'bcc@email.com, another_bcc@email.com',
            job_id: job.id.to_s,
            user_id: user.id,
            company_id: user.company.id,
          } }
      )

      expect(API::Jobs::SendDetailsEmail).to receive(:call).with(
        { input:
          {
            to: 'to@email.com, another_to@email.com',
            cc: 'cc@email.com, another_cc@email.com',
            bcc: 'bcc@email.com, another_bcc@email.com',
            job_id: job.id.to_s,
            user_id: user.id,
            company_id: user.company.id,
          } }
      )

      post_send_details_email
    end
  end

  describe "POST request_more_time" do
    subject(:post_request_more_time) do
      post :request_more_time, params: { id: job.id }, as: :json
    end

    context 'when job is FleetMotorClubJob' do
      let!(:account) do
        create(:account, company: rescue_company, client_company: fleet_company)
      end

      let!(:job) do
        create :fleet_motor_club_job, {
          fleet_company: fleet_company,
          adjusted_created_at: Time.now,
          rescue_company: rescue_company,
          issc_dispatch_request: issc_dispatch_request,
          updated_at: '2018-07-26 18:08:04',
        }
      end

      let(:issc_dispatch_request) do
        create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
      end

      let(:rescue_company) { controller.api_user.company }
      let(:fleet_company) { create(:fleet_company, :ago) }

      context 'and job digital_dispatcher is RSC' do
        let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM) }

        it 'responds with 200 OK' do
          post_request_more_time

          expect(response.code).to eq "200"
        end

        it 'creates an AsyncRequest' do
          expect { post_request_more_time }.to change(API::AsyncRequest, :count).by(1)
        end

        it 'responds with created AsyncRequest' do
          post_request_more_time
          async_request_from_response = json["async_request"]

          expect(async_request_from_response["company_id"]).to eq controller.api_company.id
          expect(async_request_from_response["user_id"]).to eq controller.api_user.id
          expect(async_request_from_response["target_id"]).to eq job.id
          expect(async_request_from_response["system"]).to eq 'RscRequestMoreTime'
        end
      end
    end
  end

  describe 'INDEX' do
    subject do
      get :index, params: { job_ids: [job.id] }, as: :json
    end

    context 'when job is FleetManagedJob' do
      let(:rescue_company) { controller.api_user.company }
      let(:fleet_company) { create(:fleet_company, :fleet_demo) }

      let!(:account) do
        create(:account, company: rescue_company, client_company: fleet_company)
      end

      let!(:job) do
        create :fleet_managed_job, {
          fleet_company: fleet_company,
          adjusted_created_at: Time.now,
          rescue_company: rescue_company,
          updated_at: '2018-07-26 18:08:04',
        }
      end

      let!(:partner_invoice) do
        create :invoice, {
          job: job,
          sender: rescue_company,
          recipient: Company.swoop,
        }
      end

      it 'returns the job' do
        subject

        first_job = json["jobs"][0]
        expect(first_job).not_to be_nil
        expect(first_job["id"]).to eql(job.id)
      end

      it 'includes an invoice' do
        subject

        first_job = json["jobs"][0]
        expect(first_job).not_to be_nil
        expect(first_job["invoices"].length).to eql(1)
        expect(first_job["invoices"][0]["uuid"]).to eql(partner_invoice.uuid)
      end

      context 'Swoop Invoice exists' do
        let!(:swoop_invoice) do
          create :invoice, {
            job: job,
            sender: Company.swoop,
            recipient: fleet_company,
            state: Invoice::SWOOP_NEW,
          }
        end

        let!(:customer) { create :user }
        let!(:customer_invoice) do
          create :invoice, {
            job: job,
            sender: fleet_company,
            recipient: customer,
            state: Invoice::FLEET_CUSTOMER_NEW,
          }
        end

        it 'excludes swoop and customer invoice' do
          subject

          first_job = json["jobs"][0]
          expect(first_job).not_to be_nil
          expect(first_job["invoices"].length).to eql(1)
          expect(first_job["invoices"][0]["uuid"]).to eql(partner_invoice.uuid)
        end
      end
    end
  end

  context "ported from minitest" do
    fixtures :all
    stub_user_login :rescue

    let!(:job) do
      job = jobs(:one)
      job.live_invoices.each do |inv|
        inv.recipient = job.rescue_company
        inv.save!
      end
      job.update_columns(
        status: Job::STORED,
        rescue_company_id: user.company.id,
        fleet_company_id: user.company.id
      )
      job
    end

    it "releases a vehicle and set the release time" do
      release_time = 1.hour.from_now
      params = { id: job.id, format: :json, job: { release_date: release_time } }
      patch(:release_vehicle, params: params)
      assert_response(:success)
      expect(assigns(:job)).not_to(be_nil)
      job.reload
      ajs = job.audit_job_statuses.for_live_status(Job::RELEASED).first
      expect(ajs).not_to(be_nil)
      expect(ajs.last_set_dttm.to_i).to(eql(release_time.to_i))
      expect(job.released?).to(be_truthy)
    end

    context 'existing release ajs' do
      before do
        job.audit_job_statuses.create(job_status: JobStatus.for_name(Job::RELEASED))
      end

      it "releases a vehicle and set the release time", freeze_time: true do
        release_time = 1.hour.from_now
        params = { id: job.id, format: :json, job: { release_date: release_time } }
        patch(:release_vehicle, params: params)
        assert_response(:success)
        expect(assigns(:job)).not_to(be_nil)
        job.reload
        ajs = job.audit_job_statuses.for_live_status(Job::RELEASED).first
        expect(ajs).not_to(be_nil)
        expect(ajs.adjusted_dttm).to(eql(release_time))
        expect(job.released?).to(be_truthy)
      end
    end

    it "releases a vehicle it should use the current time" do
      params = { id: job.id, format: :json, job: { email: Faker::Internet.email } }
      patch(:release_vehicle, params: params)
      assert_response(:success)
      expect(assigns(:job)).not_to(be_nil)
      job.reload
      ajs = job.audit_job_statuses.for_live_status(Job::RELEASED).first
      expect(ajs).not_to(be_nil)
      expect(ajs.last_set_dttm.to_i).to(be_within(2).of(ajs.last_set_dttm.to_i))
      expect(job.released?).to(be_truthy)
    end
  end

  describe "active" do
    stub_user_login(:rescue)

    let(:partner_company) { user.company }
    let!(:job_1) { create(:fleet_managed_job, :completed, rescue_company: partner_company) }
    let!(:job_2) { create(:fleet_managed_job, :with_service_location, rescue_company: partner_company, status: :pending) }
    let!(:job_3) { create(:fleet_managed_job, :with_service_location, rescue_company: partner_company, status: :pending) }

    it "returns active jobs" do
      get :active, format: :json
      expect(response).to be_successful
      job_ids = JSON.load(response.body)["jobs"].map { |job| job["id"] }
      expect(job_ids).to match_array([job_2.id, job_3.id])
    end
  end
end
