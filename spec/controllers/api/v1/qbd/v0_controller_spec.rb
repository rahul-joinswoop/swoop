# frozen_string_literal: true

require 'rails_helper'

describe API::V1::Qbd::V0Controller, type: :controller do
  render_views

  let(:token) { "f6c4baf2-0eca-4242-9f6f-77361ebd4fdb" }

  describe "GET accounts" do
    it "uses the HTTP Basic username as a secret token to fetch accounts" do
      set_secret(request, token)
      expect(ApplicationRecord).to receive(:execute_query).with("SELECT * FROM get_accounts(?)", token).and_call_original
      get :accounts
      expect(response).to be_successful
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq []
    end
  end

  describe "GET invoices" do
    it "uses the HTTP Basic username as a secret token to fetch invoices" do
      set_secret(request, token)
      expect(ApplicationRecord).to receive(:execute_query).with("SELECT * FROM get_invoice_headers(?)", token).and_call_original
      get :invoices
      expect(response).to be_successful
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq []
    end
  end

  describe "GET payments" do
    it "uses the HTTP Basic username as a secret token to fetch payments" do
      set_secret(request, token)
      expect(ApplicationRecord).to receive(:execute_query).with("SELECT * FROM get_payments(?)", token).and_call_original
      get :payments
      expect(response).to be_successful
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq []
    end
  end

  describe "GET lineitems" do
    it "uses the HTTP Basic username as a secret token to fetch invoice lines" do
      set_secret(request, token)
      expect(ApplicationRecord).to receive(:execute_query).with("SELECT * FROM get_invoice_lines(?)", token).and_call_original
      get :lineitems
      expect(response).to be_successful
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq []
    end
  end

  describe "GET items" do
    it "uses the HTTP Basic username as a secret token to fetch items" do
      set_secret(request, token)
      expect(ApplicationRecord).to receive(:execute_query).with("SELECT * FROM get_items(?)", token).and_call_original
      get :items
      expect(response).to be_successful
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq []
    end
  end

  describe "GET user" do
    it "uses the HTTP Basic username as a secret token to fetch the user" do
      set_secret(request, token)
      expect(ApplicationRecord).to receive(:execute_query).with("SELECT * FROM get_user(?)", token).and_call_original
      get :user
      expect(response).to be_successful
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq nil
    end
  end

  describe "PUT imported ledger ids" do
    it "uses the HTTP Basic username as a secret token to mark ledgers as imported" do
      set_secret(request, token)
      expect(ApplicationRecord).to receive(:execute_query).with("SELECT * FROM mark_imported(?, ?)", token, "1234").and_call_original
      put :imported, params: { ledger_id: 1234 }
      expect(response).to be_successful
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq({})
    end
  end

  describe "PUT onboard" do
    it "uses the HTTP Basic username as a secret token to mark the last QuickBooks import timestamp" do
      set_secret(request, token)
      expect(ApplicationRecord).to receive(:execute_query).with("SELECT * FROM set_qb_imported_at(?, ?)", token, "2018-05-01T16:58:57Z").and_call_original
      put :onboard, params: { qb_imported_at: "2018-05-01T16:58:57Z" }
      expect(response).to be_successful
      expect(response.content_type).to eq "application/json"
      expect(JSON.load(response.body)).to eq({})
    end
  end

  def set_secret(request, secret)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(secret, "")
  end
end
