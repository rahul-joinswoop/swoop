# frozen_string_literal: true

require "rails_helper"

describe API::V1::UsersController, type: :controller do
  render_views

  let(:password_request)  { build(:password_request, user: user) }
  let(:user)              { build(:user) }
  let(:response_json) { JSON.load(response.body) }

  describe "#me" do
    stub_user_login(:rescue)

    it "works" do
      get :me, format: "json"
      expect(response).to be_success
      expect(response_json["id"]).to eq user.id
      expect(response_json["language"]).to eq 'en'
    end

    # All API requests should record a visit, but this is the first endpoint called by the application
    # so we'll just test this one rather than every endpoint.
    it "records a visit" do
      Sidekiq::Testing.inline! do
        get :me, format: "json"
      end
      expect(user.visits.count).to eq 1
      visit = user.visits.first
      expect(visit.source_application.platform).to eq SourceApplication::PLATFORM_WEB
      expect(visit.source_application.source).to eq "Swoop"
    end
  end

  describe '#password_request' do
    subject(:post_user_input) do
      post :password_request, format: :json, params: { user_input: user_input }
    end

    before :each do
      allow(Authentication::ResetForgottenPassword).to receive(:call).and_return(mocked_context)
    end

    let(:user_input) { user.email }

    let(:mocked_context) do
      interactor_context = double("Interactor Context")
      allow(interactor_context).to receive(:result).and_return(PasswordRequest.new(user: user))
      allow(interactor_context).to receive(:success?).and_return(true)
      interactor_context
    end

    it 'triggers password reset' do
      expect(Authentication::ResetForgottenPassword).to receive(:call).and_return(mocked_context)

      post_user_input
    end

    context 'negative cases' do
      context 'the result of the service is failure' do
        it 'returns http status 200' do
          allow(mocked_context).to receive(:success?).and_return(false)
          expect(post_user_input.status).to be(200)
        end
      end
    end

    context 'positive cases' do
      context 'the result of the service is success' do
        it 'returns http status 201' do
          expect(post_user_input.status).to be(201)
        end
      end
    end
  end

  describe '#password_request_status' do
    subject(:get_password_request_status) do
      get :password_request_status, params: { uuid: uuid }
    end

    let(:uuid) { 'va-lid-uu-id' }
    let(:password_request_status) { true }

    before :each do
      allow(PasswordRequest).to receive(:valid_request_for?)
        .and_return(password_request_status)
      allow(PasswordRequest).to receive(:find_by_uuid)
        .and_return(password_request)
      allow_any_instance_of(PasswordRequest).to receive(:user)
        .and_return(user)
    end

    it 'validates the receipt uuid' do
      expect(PasswordRequest).to receive(:valid_request_for?)

      get_password_request_status
    end

    context 'when password_request is not valid' do
      let(:password_request_status) { false }

      it 'returns http status 404' do
        expect(get_password_request_status.status).to be(404)
      end
    end
  end

  describe '#change_password' do
    subject(:patch_change_password) do
      patch :change_password, params: { user: { password: password }, password_request: { uuid: uuid } }
    end

    let(:uuid)                    { 'va-lid-uu-id' }
    let(:password_request_status) { true }
    let(:valid_password)          { true }
    let(:password)                { '1234567890' }

    before :each do
      allow(PasswordRequest).to receive(:valid_request_for?)
        .and_return(password_request_status)
      allow(PasswordRequest).to receive(:find_by_uuid)
        .and_return(password_request)
      allow_any_instance_of(PasswordRequest).to receive(:invalidate!)
      allow_any_instance_of(User).to receive(:update_attributes)
        .and_return(valid_password)
    end

    it 'validates the receipt uuid' do
      expect(PasswordRequest).to receive(:valid_request_for?)

      patch_change_password
    end

    it 'finds PasswordRequest by uuid' do
      expect(PasswordRequest).to receive(:find_by_uuid)

      patch_change_password
    end

    it 'updates user\'s password' do
      allow_any_instance_of(User).to receive(:update_attributes).with(password)

      patch_change_password
    end

    it 'invalidates the PasswordRequest' do
      expect_any_instance_of(PasswordRequest).to receive(:invalidate!)

      patch_change_password
    end

    it 'returns head 200' do
      expect(patch_change_password.status).to be 200
    end

    context 'when the PasswordRequest is not valid' do
      let(:password_request_status) { false }

      it 'returns head 404' do
        expect(patch_change_password.status).to be 404
      end

      it 'doesn\'t invalidate the PasswordRequest' do
        expect_any_instance_of(PasswordRequest).not_to receive(:invalidate!)
      end
    end

    context 'when the password is not valid' do
      let(:valid_password) { false }

      it 'returns head 422' do
        expect(patch_change_password.status).to be 422
      end
    end
  end

  describe "POST#create" do
    stub_user_login(:root)

    subject(:post_create) { post :create, params: { user: user_params, format: :json } }

    let(:user_params) do
      {
        first_name: "User Name",
        job_status_email_frequency: "none",
        job_status_email_preferences: ["Created", "Accepted", "GOA"],
        last_name: "Surname",
        password: "123123123",
        roles: roles,
        site_ids: [],
        system_email_filtered_companies: [],
        username: "nice_username",
        company_id: company.id,
        language: Company::ENGLISH,
        custom_account_visible: custom_account_visible,
        notification_settings: [{ notification_code: 'new_job', notification_channels: ['Call', 'Text'] }],
      }
    end

    let(:roles) { ['dispatcher'] }
    let(:custom_account_visible) { nil }

    context 'when user company is rescue' do
      let(:company) { create(:rescue_company) }

      it 'return 201' do
        expect(post_create.response_code).to eq 201
      end

      it 'creates a new user' do
        expect { post_create }.to change(User, :count).by(1)
      end

      it 'sets the language' do
        user_params[:language] = 'de'
        post_create
        expect(company.language).to eq('en')
        expect(JSON.parse(response.body).dig('language')).to eq('de')
      end

      it 'adds notification_settings as expected' do
        post_create
        user = User.find(json["id"])

        expect(
          user.notification_settings.find_by(notification_code: 'new_job').notification_channels
        ).to eq ['Call', 'Text']
      end

      context "user#custom_account_visible" do
        subject do
          post_create

          User.find(json["id"]).custom_account_visible
        end

        context 'when it is passed as true' do
          let(:custom_account_visible) { true }

          it { is_expected.to be_truthy }
        end

        context 'when it is passed as false' do
          let(:custom_account_visible) { false }

          it { is_expected.to be_falsey }
        end

        context 'when it is not passed' do
          it { is_expected.to be_falsey }
        end
      end
    end

    context 'when user company is fleet' do
      let(:company) { create(:fleet_company) }

      it 'return 201' do
        expect(post_create.response_code).to eq 201
      end

      it 'creates a new user' do
        expect { post_create }.to change(User, :count).by(1)
      end
    end

    context 'when user company is Swoop' do
      let(:company) { create(:super_company) }

      it 'return 201' do
        expect(post_create.response_code).to eq 201
      end

      it 'creates a new user' do
        expect { post_create }.to change(User, :count).by(1)
      end
    end
  end

  describe "PATCH#update" do
    let(:patch_update) { patch :update, params: { id: user.id, user: user_params }, as: :json }

    shared_examples 'updates the language' do
      let(:language_from_json) { JSON.parse(response.body).dig('language') }

      it 'to English' do
        user_params[:language] = 'en'
        patch_update
        expect(language_from_json).to eq('en')
      end

      it 'to German' do
        user_params[:language] = 'de'
        patch_update
        expect(language_from_json).to eq('de')
      end

      it 'to Inherited' do
        user_params[:language] = ''
        patch_update
        expect(response.status).to eq(200)
        expect(language_from_json).to eq(ENV['DEFAULT_LANGUAGE'])
      end
    end

    context 'when root user is logged in' do
      stub_user_login(:root)

      context 'when updating a rescue user' do
        let!(:user) { create(:user, :rescue, roles: [:admin]) }
        let(:user_params) do
          {
            id: user.id,
            notification_settings: [{ notification_code: 'new_job', notification_channels: ['Text'] }],
          }
        end

        it 'updates user notification settings as expected' do
          # make sure it's empty before the update
          expect(user.notification_settings.count).to eq 0

          patch_update && user.reload

          expect(
            user.notification_settings.find_by(notification_code: 'new_job').notification_channels
          ).to eq ['Text']
        end

        it_behaves_like 'updates the language'
      end
    end

    context 'when updating a rescue driver' do
      let!(:user) { create(:user, :rescue_driver) }
      let(:user_params) { { id: user.id } }

      context 'logged in as updatee' do
        let!(:token) { create :doorkeeper_access_token, resource_owner_id: user.id, scopes: user.roles.map(&:name) }

        before(:each) do
          authenticate_request(controller.request, token)
        end

        it_behaves_like 'updates the language'
      end
    end
  end
end
