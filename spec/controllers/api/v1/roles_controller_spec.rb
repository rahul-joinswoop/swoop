# frozen_string_literal: true

require "rails_helper"

describe API::V1::RolesController, type: :controller do
  render_views
  stub_user_login(:rescue)

  describe 'GET index' do
    subject(:get_index) do
      get :index, as: :json
    end

    before do
      get_index
    end

    it 'responds with HTTP 200' do
      expect(response).to have_http_status(:ok)
    end

    it 'responds with a roles array' do
      expect(json).to be_a Array
    end

    it 'contains has same size of Role.all' do
      expect(json.size).to eq Role.all.size
    end
  end
end
