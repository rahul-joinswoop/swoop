# frozen_string_literal: true

require "rails_helper"

describe API::V1::GetLocationController, type: :controller do
  describe 'POST location' do
    subject do
      post :location, params: {
        uuid: job.uuid,
        location: {
          lat: 37.77014479999998,
          lng: -122.43849190000003,
          address: "898 Waller St, San Francisco, CA 94117, USA",
          place_id: "ChIJDS60J6mAhYARlLvg3qTbV9k",
        },
        auto: true,
      }, as: :json
    end

    let(:job) { create(:fleet_managed_job, location_updated: nil, customer: customer) } # can be any
    let(:customer) { create(:user) }
    let!(:audit_sms) { create(:audit_sms, :sms_confirmation, job: job) }

    before do
      allow(job).to receive(:slack_notification)
    end

    it 'updates job.service_location' do
      expect { subject && job.reload }.to change(job, :service_location)
    end

    it 'updates job.last_touched_by with job.customer' do
      expect { subject && job.reload }.to change(job, :last_touched_by).to(customer)
    end

    it 'sets job.location_updated accordingly' do
      expect { subject && job.reload }.to change(job, :location_updated).from(nil).to(true)
    end

    it 'trigggers a slack notification' do
      expect_any_instance_of(Job).to receive(:slack_notification)

      subject
    end

    context 'RequestContext setup' do
      let(:request_context) { RequestContext.new }

      before do
        allow(RequestContext).to receive(:current).and_return(request_context)
      end

      it 'sets RequestContext with SMS source application' do
        expect(request_context).to receive(:source_application_id=).with(SourceApplication.consumer_mobile_web.id)

        subject
      end
    end
  end
end
