# frozen_string_literal: true

require 'rails_helper'

describe API::V1::SitesController, type: :controller do
  before do
    create(
      :rescue_company,
      phone: '+14155551234',
      sites: create_list(:site, 3)
    )

    Sidekiq::Worker.clear_all
  end

  describe 'POST /api/v1/sites/search' do
    context 'with an existing company' do
      let(:phone) { '+14155551234' }

      it 'renders the json' do
        post :search, params: { phone: phone, name: 'FooBar' }

        expect(JSON.parse(response.body).count).to eq(3)
        expect(ExistingPartnerSignupAttemptWorker.jobs.count).to eq(1)
      end
    end

    context 'without an existing company' do
      let(:phone) { '+14135554321' }

      it 'renders the json' do
        post :search, params: { phone: phone, name: 'FooBar' }

        expect(JSON.parse(response.body).count).to eq(0)
        expect(ExistingPartnerSignupAttemptWorker.jobs.count).to eq(0)
      end
    end
  end
end
