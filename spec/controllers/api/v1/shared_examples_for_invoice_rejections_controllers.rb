# frozen_string_literal: true

RSpec.shared_examples "API::V1::Shared::InvoiceRejections#create" do
  it "returns 200" do
    post_reject_invoice
    expect(response).to render_template("api/v1/invoice_rejections/show")
    expect(response.status).to eql(200)
  end

  it "creates a new InvoiceRejection object" do
    expect { post_reject_invoice }
      .to change(InvoiceRejection, :count).by(1)
  end

  context "when rendering success response" do
    render_views

    it "returns an InvoiceRejection object as json" do
      post_reject_invoice
      expect(JSON.parse(response.body)).to eql(
        "assigning_company_name" => invoice.job.assigning_company.name,
        "reject_reason" => invoice_reject_reason.text,
        "notes" => "Approval rejected",
        "invoice_id" => invoice.id
      )
    end
  end

  it "returns a validation error when invalid data" do
    allow(CreateInvoiceRejection).to receive(:new).and_raise(
      CreateInvoiceRejection::InvalidInvoiceRejectionError
    )
    post_reject_invoice
    expect(response.status).to eql(400)
    expect(response.body).to include("Not a valid invoice rejection")
  end

  it "returns a 404 error when the invoice does not exist" do
    params[:invoice_rejection][:invoice_id] = 123
    post_reject_invoice
    expect(response.status).to eql(404)
  end

  it "returns a 404 error when the rejection reason does not exist" do
    params[:invoice_rejection][:invoice_reject_reason_id] = 7382
    post_reject_invoice
    expect(response.status).to eql(404)
  end

  it "returns a 403 error when the rejection reason is not allowed" do
    another_company = create :fleet_company, :fleet_demo
    invoice_reject_reason.update! company: another_company
    post_reject_invoice
    expect(response.status).to eql(403)
  end
end
