# frozen_string_literal: true

require "rails_helper"

describe API::V1::JobsBaseController, type: :controller do
  context "when an android user agent" do
    let(:job) { create(:rescue_job) }
    let(:ajs) { create(:audit_job_status) }
    let(:browser) { double("Browser", platform: 'other') }

    before do
      allow(request).to receive(:user_agent).and_return('android Swoop/2.0.49')
      allow(controller).to receive(:browser).and_return(browser)
      allow(job).to receive(:status_changed?).and_return(true)
      controller.instance_variable_set(:@job, job)
    end

    context "when job is completed" do
      before do
        job.status = Job::COMPLETED
      end

      it "has an android platform" do
        expect(controller.send(:set_platform)).to eq('android')
      end
    end

    context "when job is completed" do
      before do
        job.status = Job::PENDING
      end

      it "has an android platform" do
        expect(controller.send(:set_platform)).to eq(nil)
      end
    end
  end
end
