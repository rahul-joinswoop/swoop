# frozen_string_literal: true

require "rails_helper"

describe API::V1::AccountsController, type: :controller do
  stub_user_login(:rescue)

  describe "GET index" do
    subject(:index) do
      get :index, format: :json
    end

    before do
      create :account
    end

    it "returns head 200" do
      expect(index.status).to be 200
    end
  end
end
