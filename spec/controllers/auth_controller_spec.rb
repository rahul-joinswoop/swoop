# frozen_string_literal: true

require "rails_helper"

describe AuthController, type: :controller do
  let!(:company) { create(:company) }
  let!(:user) { create(:user, company: company) }
  let!(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }

  before do
    authenticate_request(controller.request, token)
  end

  describe "GET agero_login" do
    it "returns 403 if access_token not provided" do
      get :agero_login

      expect(response).to have_http_status(403)

      expect(response).not_to redirect_to("https://agero.example.com/login")
    end

    it "redirects to the Agero login page" do
      allow_any_instance_of(Agero::Rsc::API).to receive(:authorize_url).and_return("https://agero.example.com/login")
      get :agero_login, params: { access_token: "123" }
      expect(response).to redirect_to("https://agero.example.com/login")
    end
  end

  describe "GET agero_callback" do
    let(:payload) do
      {
        "userClaim" => {
          "vendorId" => "123",
          "profileId" => "567",
          "username" => "sample",
          "role" => "Dispatching",
        },
        "token" => {
          "access_token" => "abcdef",
          "expires_in" => "0",
          "refresh_token" => "wxyz",
          "refresh_count" => "0",
        },
      }
    end

    it "stores the access token returned from the RSC API" do
      allow_any_instance_of(Agero::Rsc::API).to receive(:get_access_token).with("1234").and_return(payload)

      expect_any_instance_of(Agero::Rsc::API).to receive(
        :subscribe_to_server_notifications
      ).and_return({})

      expect(Agero::Rsc::PullLocationsWorker).to receive(:perform_async)

      expect do
        get :agero_callback, params: { code: "1234", state: "foobar" },
                             session: {
                               auth_state: "foobar",
                               access_token: token.token,
                             }
      end.to change(APIAccessToken, :count).by(1)

      expect(response).to be_redirect
    end

    it "uses the existing access token if present" do
      allow_any_instance_of(Agero::Rsc::API).to receive(:get_access_token).with("1234").and_return(payload)
      expect_any_instance_of(Agero::Rsc::API).to receive(:subscribe_to_server_notifications).and_return({})

      create(:api_access_token,
             vendorid: "123",
             access_token: "abcdef",
             company: company,
             subscriptionid: nil,
             source: :agero)

      expect do
        get :agero_callback, params: { code: "1234", state: "foobar" },
                             session: {
                               auth_state: "foobar",
                               access_token: token.token,
                             }
      end.to avoid_changing(APIAccessToken, :count)
    end

    it "displays an error if the state param does not match the expected value" do
      get :agero_callback, params: { code: "1234", state: "foobar" },
                           session: { auth_state: "notfootbar", token: token.token }
      expect(response).to be_forbidden
    end

    it "should display an error message if the authorization code is invalid"

    it "logouts duplicate tokens" do
      allow_any_instance_of(Agero::Rsc::API).to receive(:get_access_token).with("1234").and_return(payload)
      expect_any_instance_of(Agero::Rsc::API).to receive(:subscribe_to_server_notifications).and_return({})

      company_1 = create(:rescue_company)
      company_2 = create(:rescue_company)
      company_3 = create(:rescue_company)

      token_1 = create(:api_access_token, company: company_1, vendorid: "123")
      create(:api_access_token, company: company_2, vendorid: "123", deleted_at: Time.current)
      create(:api_access_token, company: company_3, vendorid: "234")

      get :agero_callback, params: { code: "1234", state: "foobar" }, session: { auth_state: "foobar", access_token: token.token }

      expect(Agero::Rsc::DisconnectOauthWorker.jobs.size).to eq 1
      expect(Agero::Rsc::DisconnectOauthWorker).to have_enqueued_sidekiq_job(token_1.company_id, token_1.vendorid)
    end
  end

  describe "partner_authentication", gon: true do
    subject { send(method, :partner_authentication, params: params) }

    let(:method) { :get }
    let(:application) { create(:oauth_application) }
    let(:client_id) { application.uid }
    let(:redirect_uri) { application.redirect_uri }
    let(:params) { { client_id: client_id, redirect_uri: redirect_uri } }

    context "without params" do
      let(:params) { {} }

      it "fails" do
        expect(subject).to be_unprocessable
      end
    end

    context "with an incorrect client_id" do
      let(:client_id) { SecureRandom.uuid }

      it "fails" do
        expect(Rollbar).to receive(:error).once.with("AuthController: invalid client_id", anything)
        expect(subject).to be_not_found
        expect(gon['error']).to eq('invalid client_id')
      end
    end

    context "with an incorrect redirect_uri" do
      let(:redirect_uri) { Faker::Internet.url(scheme: :https) }

      it "fails" do
        expect(Rollbar).to receive(:error).once.with("AuthController: invalid redirect_uri", anything)
        expect(subject).to be_unprocessable
        expect(gon['error']).to eq('invalid redirect_uri')
      end
    end

    context "without access token parameter" do
      it "renders a login screen" do
        expect(subject).to be_successful
        expect(gon['app']).to eq({
          client_id: application.uid,
          name: application.name,
          redirect_uri: application.redirect_uri,
        })
      end
    end

    context "with access token parameter" do
      let(:params) { super().merge access_token: token.token }

      context "and a GET" do
        it "renders a login screen" do
          expect(subject).to be_successful
        end
      end

      context "and a POST" do
        let(:method) { :post }

        it "redirects to the authorize page" do
          expect(subject)
            .to redirect_to(
              oauth_authorization_url(
                **params.except(:access_token),
                scope: "company",
                response_type: "code"
              )
            )
          expect(session[:access_token]).to eq token.token
        end
      end

      context 'when a company is not live' do
        let(:method) { :post }

        it 'sets the live flag to true' do
          subject
          company.reload
          expect(company.live).to eq(true)
        end
      end
    end
  end
end
