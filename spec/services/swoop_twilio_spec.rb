# frozen_string_literal: true

require 'rails_helper'

describe SwoopTwilio do
  subject(:swoop_twilio) { SwoopTwilio.new }

  let(:from) { Faker::PhoneNumber.phone_number_e164 }
  let(:to) { Faker::PhoneNumber.phone_number_e164 }
  let(:blacklisted) { build_stubbed(:phone_number_blacklist).phone_number }
  let(:options) do
    {
      to: to,
      from: from,
      body: 'TEST',
      status_callback: 'example.com',
    }
  end
  let(:blacklist_error_code) { SwoopTwilio::TWILIO_BLACKLIST_ERROR_CODE }
  let(:invalid_error_code) { SwoopTwilio::TWILIO_INVALID_ERROR_CODE }
  let(:invalid_mobile_code) { SwoopTwilio::TWILIO_INVALID_TO_MOBILE_CODE }
  let(:other_twilio_error_code) { 11251 }

  let(:twilio_client) { double(:twilio_client, account: account) }
  let(:account) { double(:account, messages: messages, calls: calls) }
  let(:messages) { double(:messages, create: true) }
  let(:calls) { double(:calls, create: true) }
  let(:rails_logger) { double(:rails_logger) }

  before do
    allow(swoop_twilio).to receive(:client).and_return(twilio_client)
    allow(swoop_twilio).to receive(:twilio_enabled?).and_return(true)
  end

  describe '#message_create' do
    context 'with a Swoop blacklisted phone number' do
      before do
        allow(swoop_twilio).to receive(:blacklisted?).and_return(true)
      end

      it 'does not create a twilio message' do
        swoop_twilio.message_create(options)
        expect(swoop_twilio).not_to receive(:client)
      end
    end

    context 'with a twilio invalid mobile number code' do
      before do
        allow(messages).to receive(:create)
          .and_raise(Twilio::REST::RequestError.new('Foobar', invalid_mobile_code))
      end

      it 'does not raise an error and logs to debug level' do
        expect(Rails.logger).to receive(:debug).at_least(:once)
        expect { swoop_twilio.message_create(options) }
          .not_to raise_error
      end
    end

    context 'with a twilio invalid error code' do
      before do
        allow(messages).to receive(:create)
          .and_raise(
            Twilio::REST::RequestError.new('Foobar', invalid_error_code)
          )
      end

      it 'does not raise an error and logs to debug level' do
        expect(Rails.logger).to receive(:debug).at_least(:once)
        expect { swoop_twilio.message_create(options) }
          .not_to raise_error
      end
    end

    context 'with a twilio blacklist error code' do
      before do
        allow(messages).to receive(:create)
          .and_raise(
            Twilio::REST::RequestError.new('Foobar', blacklist_error_code)
          )
      end

      it 'does not raise an error and logs to debug level' do
        expect(Rails.logger).to receive(:debug).at_least(:once)
        expect { swoop_twilio.message_create(options) }
          .not_to raise_error
      end
    end

    context 'with an invalid to phone number' do
      let(:to) { '+15555551234' }

      it 'does not raise an error and logs to debug level' do
        expect(Rails.logger).to receive(:debug).at_least(:once)

        expect { swoop_twilio.message_create(options) }.not_to raise_error
      end
    end

    context 'with any other twilio error' do
      before do
        allow(messages).to receive(:create)
          .and_raise(
            Twilio::REST::RequestError.new('Foobar', other_twilio_error_code)
          )
      end

      it 'raises an error' do
        expect { swoop_twilio.message_create(options) }
          .to raise_error(Twilio::REST::RequestError)
      end
    end
  end

  describe "#call_create" do
    before do
      allow(calls).to receive(:create).and_return(true)
    end

    context 'with a valid phone number' do
      it 'creates a call' do
        expect(calls).to receive(:create)
        swoop_twilio.call_create(options)
      end
    end

    context 'with a swoop blacklisted phone number' do
      before do
        allow(swoop_twilio).to receive(:blacklisted?).and_return(true)
      end

      it 'does not create a call' do
        expect(calls).not_to receive(:create)
        swoop_twilio.call_create(options)
      end
    end
  end
end
