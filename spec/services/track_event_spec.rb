# frozen_string_literal: true

require "rails_helper"

describe TrackEvent do
  subject do
    described_class.call(event: event,
                         user: user,
                         target: target_obj,
                         additional_attrs: additional_attrs).result
  end

  let(:user) { create(:user) }
  let(:event) { :test_event }
  let(:additional_attrs) { {} }
  let(:target_obj) { nil }

  it "automatically provides user data" do
    expect(subject[:user_id]).to eq(user.to_ssid)
  end

  it "automatically provides event" do
    expect(subject[:event]).to eq(:test_event)
  end

  it "logs event" do
    expect(Analytics).to receive(:track)

    subject
  end

  it "omits an anonymous id" do
    expect(subject.keys).not_to include(:anonymous_id)
  end

  context "when an error occurs" do
    before do
      allow_any_instance_of(TrackEvent).to receive(:properties).and_raise(StandardError)
    end

    it "does not halt execution" do
      expect { subject }.not_to raise_error
    end
  end

  context "when job is target object" do
    let(:target_obj) { create(:rescue_job) }

    it "automatically provides job data" do
      expect(subject[:properties]).to eq(target_obj.render_to_hash)
    end
  end

  context "when a user is absent" do
    let(:user) { nil }

    it "lacks a user_id key" do
      expect(subject.keys).not_to include(:user_id)
    end

    it "includes an anonymous id" do
      expect(subject[:anonymous_id]).to be_present
    end
  end
end
