# frozen_string_literal: true

require "rails_helper"

describe External::Redis::Cleanup do
  it "delete RescueCompany web socket subscriptions that haven't been used in the last day" do
    redis = RedisClient[:default]
    redis.zadd("RescueCompany-1", 25.hours.ago.to_f * 1000, "foo")
    redis.zadd("RescueCompany-2", 25.hours.ago.to_f * 1000, "foo")
    redis.zadd("RescueCompany-2", 23.hours.ago.to_f * 1000, "bar")
    redis.set("RescueCompany-3", "bar")
    redis.zadd("OtherThing-3", 25.hours.ago.to_f * 1000, "foo")
    expect(redis.exists("RescueCompany-1")).to eq true

    External::Redis::Cleanup.new(:default).call

    expect(redis.exists("RescueCompany-1")).to eq false
    expect(redis.exists("RescueCompany-2")).to eq true
    expect(redis.exists("RescueCompany-3")).to eq true
    expect(redis.exists("OtherThing-3")).to eq true
  end
end
