# frozen_string_literal: true

require 'rails_helper'

describe External::Split do
  let!(:job) { create :rescue_job }
  let(:split_name) { 'job_claim_cost' }
  let(:metric_name) { 'cost_estimate' }

  context 'split enabled' do
    before do
      Rails.application.config.split_client = double
    end

    describe 'getting treatment' do
      subject do
        External::Split.treatment(job, split_name)
      end

      context 'external service returns on' do
        it 'returns on' do
          expect(Rails.application.config.split_client).to receive(:get_treatment)
            .with(job.id.to_s, split_name)
            .and_return("on")
          expect(subject).to eq("on")
        end
      end
    end

    describe 'track metrics' do
      subject do
        External::Split.track(job, 'job', metric_name, 100)
      end

      context 'external service returns on' do
        let(:value) { 100 }

        it 'calls track ok' do
          expect(Rails.application.config.split_client).to receive(:track)
            .with(job.id.to_s, 'job', metric_name, value)
            .and_return(true)

          subject
        end
      end
    end
  end

  context 'split disabled' do
    before do
      Rails.application.config.split_client = nil
    end

    describe 'getting treatment' do
      subject do
        External::Split.treatment(job, split_name)
      end

      it "returns off by default" do
        expect(subject).to eq("off")
      end
    end
  end
end
