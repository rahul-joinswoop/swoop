# frozen_string_literal: true

require 'rails_helper'

describe External::Pushwoosh do
  describe "#send" do
    subject { External::Pushwoosh.create_message title: title, msg: msg, link: link, user_ids: user_ids }

    let(:title) { Faker::Hipster.sentence }
    let(:msg) { Faker::Hipster.sentence }
    let(:user_ids) { [1, 2, 3, 4] }
    let(:link) { Faker::Hipster.sentence }

    around do |example|
      ClimateControl.modify PUSHWOOSH_PROJECT_ID: 'id', ENABLE_PUSHWOOSH: 'true', PUSHWOOSH_API_KEY: 'asdf' do
        example.call
      end
    end

    it "works" do
      stub_request(:post, External::Pushwoosh::CREATE_MESSAGE_URI)
        .to_return(status: 200,
                   headers: { "Content-Type" => "application/json; charset=utf-8" },
                   body: '{"foo": "bar"}')
      expect { subject }.not_to raise_error
    end

    it "includes custom push parameters" do
      dbl = double('HTTP::Client', post: double('Response', status: 200))
      allow(External::Pushwoosh).to receive(:http_client).and_return(dbl)
      expect(dbl).to receive(:post)
        .with(External::Pushwoosh::CREATE_MESSAGE_URI, {
          json: {
            request: {
              application: 'id',
              auth: 'asdf',
              notifications: [
                {
                  android_header: title,
                  android_root_params: { 'pw_channel' => 'bells_mono_chan' },
                  android_sound: 'bells_mono',
                  content: msg,
                  ios_sound: 'bells_mono.wav',
                  ios_title: title,
                  link: link,
                  minimize_link: 0,
                  send_date: 'now',
                  users: user_ids,
                },
              ],
            },
          },
        })
      subject
    end
  end
end
