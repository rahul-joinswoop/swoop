# frozen_string_literal: true

require 'rails_helper'
require_relative './google_directions_service_shared'

describe External::GoogleDirectionsService do
  describe '#parse_location' do
    subject { External::GoogleDirectionsService.parse_location arg }

    shared_examples "it works" do
      it 'works' do
        expect(subject).to eq(expected)
      end
    end

    context 'with a hash' do
      let(:arg) { { lat: 123.123, lng: -34.45 } }
      let(:expected) { [arg[:lat], arg[:lng]].join(',') }

      it_behaves_like 'it works'
    end

    context 'with a model that has #lat and #lng' do
      let(:arg) { create :location }
      let(:expected) { [arg.lat, arg.lng].join(',') }

      it_behaves_like 'it works'
    end

    context 'with a string' do
      let(:arg) { Faker::Address.full_address }
      let(:expected) { arg }

      it_behaves_like 'it works'
    end

    context 'with an array' do
      let(:arg) { [Faker::Address.full_address, create(:location),  { lat: 123.123, lng: -34.45 }] }
      let(:expected) { [arg[0], [arg[1].lat, arg[1].lng].join(','), [arg[2][:lat], arg[2][:lng]].join(',')].join('|') }

      it_behaves_like 'it works'
    end

    context 'with something else' do
      let(:arg) { create :user }

      it 'works' do
        expect { subject }.to raise_error(External::GoogleDirectionsService::InvalidLocationError)
      end
    end
  end

  describe '#get' do
    include_context "google directions service shared"
    subject { External::GoogleDirectionsService.get(origin, destination, waypoints) }

    it 'works' do
      stub_request(:get, "https://maps.googleapis.com/maps/api/directions/json")
        .with(query: hash_including({
          "destination" => [destination.lat, destination.lng].join(','),
          "origin" => [origin.lat, origin.lng].join(','),
          "waypoints" => waypoints.map { |waypoint| [waypoint.lat, waypoint.lng].join(',') }.join('|'),
        }))
        .to_return(status: 200,
                   headers: { "Content-Type" => "application/json; charset=utf-8" },
                   body: response.to_json)

      expect { subject }.not_to raise_error
      expect(subject).to eq(response)
    end
  end
end
