# frozen_string_literal: true

require "rails_helper"

describe External::GoogleDistanceMatrixService do
  let(:breakdown_location_1) { create(:location, { lat: 37.778511, lng: -122.478831 }) }
  let(:breakdown_location_2) { create(:location, { lat: 37.787013, lng: -122.405181 }) }
  let(:rescue_vehicle) { create(:rescue_vehicle_with_coords, { lat: 37.788511, lng: -122.468831 }) }
  let(:site_location) { create(:location, { lat: 37.778511, lng: -122.46 }) }

  context "from a single origin to a single destination", vcr: true do
    it "returns distance and time" do
      meters, seconds = External::GoogleDistanceMatrixService.calculate(breakdown_location_1, rescue_vehicle).first
      expect(meters).not_to be_nil
      expect(seconds).not_to be_nil
      expect(meters).to eql(1780)
      expect(seconds).to eql(242)
    end
  end

  context "from multiple origins to a single destination", vcr: true do
    describe "returns distances and times" do
      let(:distances_and_times) { External::GoogleDistanceMatrixService.calculate([rescue_vehicle, site_location], breakdown_location_1) }

      it 'has 2 results (2 origins x 1 destination)' do
        expect(distances_and_times.length).to eql(2)
      end
      it 'has distances for each result' do
        expect(distances_and_times.map { |dt| dt[0] }).to all(be_an(Integer))
      end
      it 'has times for each result' do
        expect(distances_and_times.map { |dt| dt[1] }).to all(be_an(Integer))
      end
      it 'has the correct values from the Google Distance Matrix API' do
        meters, seconds = distances_and_times.first
        expect(meters).to eql(1780)
        expect(seconds).to eql(258)
      end
    end
  end

  context "from a single origins to multiple destinations", vcr: true do
    describe "returns distances and times" do
      let(:distances_and_times) { External::GoogleDistanceMatrixService.calculate(rescue_vehicle, [breakdown_location_1, breakdown_location_2]) }

      it 'has 2 results (1 origin x 2 destinations)' do
        expect(distances_and_times.length).to eql(2)
      end
      it 'has distances for each result' do
        expect(distances_and_times.map { |dt| dt[0] }).to all(be_an(Integer))
      end
      it 'has times for each result' do
        expect(distances_and_times.map { |dt| dt[1] }).to all(be_an(Integer))
      end
      it 'has the correct values from the Google Distance Matrix API' do
        meters, seconds = distances_and_times.first
        expect(meters).to eql(1780)
        expect(seconds).to eql(257)
      end
    end
  end

  context "from multiple origins to multiple destinations", vcr: true do
    describe "returns distances and times" do
      let(:distances_and_times) { External::GoogleDistanceMatrixService.calculate([rescue_vehicle, site_location], [breakdown_location_1, breakdown_location_2]) }

      it 'has four results (2 origins x 2 destinations)' do
        expect(distances_and_times.length).to eql(4)
      end
      it 'has distances for each result' do
        expect(distances_and_times.map { |dt| dt[0] }).to all(be_an(Integer))
      end
      it 'has times for each result' do
        expect(distances_and_times.map { |dt| dt[1] }).to all(be_an(Integer))
      end
      it 'has the correct values from the Google Distance Matrix API' do
        meters, seconds = distances_and_times[1]
        expect(meters).to eql(6437)
        expect(seconds).to eql(999)
      end
    end
  end

  context "when arguments are nil" do
    context "destinations are not provided" do
      it "raise ArgumentError" do
        expect { External::GoogleDistanceMatrixService.calculate(rescue_vehicle, nil) }.to raise_error(ArgumentError)
      end
    end

    context "origins are not provided" do
      it "raise ArgumentError" do
        expect { External::GoogleDistanceMatrixService.calculate(nil, breakdown_location_1) }.to raise_error(ArgumentError)
      end
    end
  end

  describe "multi" do
    it "can execute multiple calculations in parallel" do
      args = []
      results = []
      20.times do |i|
        args << { origin: "origin_#{i}", destination: "destination_#{i}", start_at: "time_#{i}" }
        results << "result_#{i}"
        expect(External::GoogleDistanceMatrixService).to receive(:calculate).with("origin_#{i}", "destination_#{i}", "time_#{i}").and_return("result_#{i}")
      end
      expect(External::GoogleDistanceMatrixService.multi(args)).to eq results
    end
  end
end
