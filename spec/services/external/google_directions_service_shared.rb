# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "google directions service shared" do
  let(:origin) { create :location, :new_jerzy_towing_hq }
  let(:destination) { create :location, :saint_paul_towing_hq }
  let(:waypoints) { [create(:location, :durand_durand_towing_hq)] }

  let(:response) do
    {
      "geocoded_waypoints" =>
              [
                {
                  "geocoder_status" => "OK",
                  "place_id" => "ChIJdZF0KZJ9j4ARmbljFqhWdSE",
                  "types" => ["street_address"],
                },
                {
                  "geocoder_status" => "OK",
                  "place_id" => "ChIJEeI_q6-AhYARlBrD8aZBO1I",
                  "types" => ["premise"],
                },
                {
                  "geocoder_status" => "OK",
                  "place_id" => "ChIJMz2w0Sd-j4ART81RX7EbeOc",
                  "types" => ["premise"],
                },
              ],
      "routes" =>
      [{
        "bounds" =>
                   {
                     "northeast" => { "lat" => 37.7765647, "lng" => -122.4131672 },
                     "southwest" => { "lat" => 37.7435051, "lng" => -122.4772694 },
                   },
        "copyrights" => "Map data ©2019 Google",
        "legs" =>
         [
           {
             "distance" => { "text" => "4.3 mi", "value" => 6901 },
             "duration" => { "text" => "15 mins", "value" => 885 },
             "end_address" => "1213 Fell St, San Francisco, CA 94117, USA",
             "end_location" => { "lat" => 37.7740082, "lng" => -122.4382426 },
             "start_address" => "2381 18th Ave, San Francisco, CA 94116, USA",
             "start_location" => { "lat" => 37.7435051, "lng" => -122.4746765 },
             "steps" =>
                         [
                           {
                             "distance" => { "text" => "0.1 mi", "value" => 163 },
                             "duration" => { "text" => "1 min", "value" => 26 },
                             "end_location" => { "lat" => 37.7449707, "lng" => -122.4747736 },
                             "html_instructions" =>
                                            "Head <b>north</b> on <b>18th Ave</b> toward <b>Santiago St</b>",
                             "polyline" => { "points" => "}wjeFvyojVk@@gADkBDc@@_@@" },
                             "start_location" => { "lat" => 37.7435051, "lng" => -122.4746765 },
                             "travel_mode" => "DRIVING",
                           },
                           {
                             "distance" => { "text" => "279 ft", "value" => 85 },
                             "duration" => { "text" => "1 min", "value" => 26 },
                             "end_location" => { "lat" => 37.7449156, "lng" => -122.475738 },
                             "html_instructions" =>
                             "Turn <b>left</b> at the 1st cross street onto <b>Santiago St</b>",
                             "maneuver" => "turn-left",
                             "polyline" => { "points" => "aakeFhzojV@T@l@BnA@l@" },
                             "start_location" => { "lat" => 37.7449707, "lng" => -122.4747736 },
                             "travel_mode" => "DRIVING",
                           },
                           {
                             "distance" => { "text" => "1.4 mi", "value" => 2283 },
                             "duration" => { "text" => "4 mins", "value" => 254 },
                             "end_location" => { "lat" => 37.7654118, "lng" => -122.4772694 },
                             "html_instructions" =>
                             "Turn <b>right</b> onto <b>19th Ave</b><div style=\"font-size:0.9em\">Pass by Flyers (on the right in 0.4&nbsp;mi)</div>",
                             "maneuver" => "turn-right",
                             "polyline" =>
                             { "points" =>
                               "w`keFj`pjVK?o@ByHTuADc@@gABgADkBFkBDgGRuJXc@@k@BaHRuJXuJVc@BoIRc@BiG\\O@YBS@S@O@E?K@iBJsDLi@@eAD_FLs@BU@" },
                             "start_location" => { "lat" => 37.7449156, "lng" => -122.475738 },
                             "travel_mode" => "DRIVING",
                           },
                           {
                             "distance" => { "text" => "0.9 mi", "value" => 1485 },
                             "duration" => { "text" => "3 mins", "value" => 197 },
                             "end_location" => { "lat" => 37.7661533, "lng" => -122.4604059 },
                             "html_instructions" => "Turn <b>right</b> onto <b>Lincoln Way</b>",
                             "maneuver" => "turn-right",
                             "polyline" =>
                             { "points" =>
                               "y`oeF|ipjVAg@A{@EqBE_CAuAIuEE{CCcAIaFEcCCsAIqEEoBA]AiAIwE?KGeEEqACeCA[CiC?Y?SAICsACwBCaBAq@CeAE{ACyBEiC" },
                             "start_location" => { "lat" => 37.7654118, "lng" => -122.4772694 },
                             "travel_mode" => "DRIVING",
                           },
                           {
                             "distance" => { "text" => "0.5 mi", "value" => 871 },
                             "duration" => { "text" => "2 mins", "value" => 93 },
                             "end_location" => { "lat" => 37.7710477, "lng" => -122.4548682 },
                             "html_instructions" =>
                             "Keep <b>left</b> to continue on <b>Kezar Dr</b>",
                             "maneuver" => "keep-left",
                             "polyline" =>
                             { "points" =>
                               "meoeFp`mjVAKAIAOCEAIEKCIEIEGMOIKIMCCCCICs@aAOSIMGKIOCGCIOa@GUAISaAq@iDq@iDUiAg@kCIc@AGEMAIEIGIGEEEGCMEKCMCYCi@CQ?W@I@E@G@KBE@E@OFMDu@Zo@XSHKDUHKECAWAKCICEEKGKIGGGIU_@KS" },
                             "start_location" => { "lat" => 37.7661533, "lng" => -122.4604059 },
                             "travel_mode" => "DRIVING",
                           },
                           {
                             "distance" => { "text" => "285 ft", "value" => 87 },
                             "duration" => { "text" => "1 min", "value" => 22 },
                             "end_location" => { "lat" => 37.7713809, "lng" => -122.4539748 },
                             "html_instructions" =>
                             "Continue straight onto <b>John F Kennedy Dr</b>",
                             "maneuver" => "straight",
                             "polyline" => { "points" => "adpeF|}kjVg@mBYeA" },
                             "start_location" => { "lat" => 37.7710477, "lng" => -122.4548682 },
                             "travel_mode" => "DRIVING",
                           },
                           {
                             "distance" => { "text" => "1.0 mi", "value" => 1620 },
                             "duration" => { "text" => "3 mins", "value" => 197 },
                             "end_location" => { "lat" => 37.7733599, "lng" => -122.4357816 },
                             "html_instructions" =>
                             "Continue onto <b>Oak St</b><div style=\"font-size:0.9em\">Pass by Chase Bank (on the left in 0.9&nbsp;mi)</div>",
                             "polyline" =>
                             { "points" =>
                               "cfpeFhxkjVE[CY?Q@S@QBUBa@N}ABU?M@I?OAQASAOa@iGAYCSe@uHCSc@cHAQAQOwBWqDi@oICQK}ACk@KyAKyAAQc@}GAWi@iICa@QeCEq@AQEg@Ek@AQCa@GcA[_F" },
                             "start_location" => { "lat" => 37.7713809, "lng" => -122.4539748 },
                             "travel_mode" => "DRIVING",
                           },
                           {
                             "distance" => { "text" => "344 ft", "value" => 105 },
                             "duration" => { "text" => "1 min", "value" => 33 },
                             "end_location" => { "lat" => 37.7742917, "lng" => -122.4359679 },
                             "html_instructions" => "Turn <b>left</b> onto <b>Scott St</b>",
                             "maneuver" => "turn-left",
                             "polyline" => { "points" => "orpeFrfhjVkANgALe@F" },
                             "start_location" => { "lat" => 37.7733599, "lng" => -122.4357816 },
                             "travel_mode" => "DRIVING",
                           },
                           {
                             "distance" => { "text" => "0.1 mi", "value" => 202 },
                             "duration" => { "text" => "1 min", "value" => 37 },
                             "end_location" => { "lat" => 37.7740082, "lng" => -122.4382426 },
                             "html_instructions" =>
                             "Turn <b>left</b> at the 1st cross street onto <b>Fell St</b><div style=\"font-size:0.9em\">Destination will be on the left</div>",
                             "maneuver" => "turn-left",
                             "polyline" => { "points" => "ixpeFxghjVVhENxB@PBf@HfA" },
                             "start_location" => { "lat" => 37.7742917, "lng" => -122.4359679 },
                             "travel_mode" => "DRIVING",
                           },
                         ],
             "traffic_speed_entry" => [],
             "via_waypoint" => [],
           },
           {
             "distance" => { "text" => "1.9 mi", "value" => 3082 },
             "duration" => { "text" => "11 mins", "value" => 680 },
             "end_address" => "1465 Folsom St, San Francisco, CA 94103, USA",
             "end_location" => { "lat" => 37.772122, "lng" => -122.4131672 },
             "start_address" => "1213 Fell St, San Francisco, CA 94117, USA",
             "start_location" => { "lat" => 37.7740082, "lng" => -122.4382426 },
             "steps" =>
             [
               {
                 "distance" => { "text" => "305 ft", "value" => 93 },
                 "duration" => { "text" => "1 min", "value" => 13 },
                 "end_location" => { "lat" => 37.7738696, "lng" => -122.4392898 },
                 "html_instructions" =>
                    "Head <b>west</b> on <b>Fell St</b> toward <b>Broderick St</b>",
                 "polyline" => { "points" => "qvpeF~uhjVDh@TfD" },
                 "start_location" => { "lat" => 37.7740082, "lng" => -122.4382426 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "341 ft", "value" => 104 },
                 "duration" => { "text" => "1 min", "value" => 58 },
                 "end_location" => { "lat" => 37.7729425, "lng" => -122.4391001 },
                 "html_instructions" =>
                 "Turn <b>left</b> after Bank of America Financial Center (on the left)",
                 "maneuver" => "turn-left",
                 "polyline" => { "points" => "uupeFp|hjVf@GXEB?\\Ed@Gn@I" },
                 "start_location" => { "lat" => 37.7738696, "lng" => -122.4392898 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "1.0 mi", "value" => 1613 },
                 "duration" => { "text" => "5 mins", "value" => 296 },
                 "end_location" => { "lat" => 37.7752337, "lng" => -122.4209797 },
                 "html_instructions" =>
                 "Turn <b>left</b> onto <b>Oak St</b><div style=\"font-size:0.9em\">Pass by Chase Bank (on the left)</div>",
                 "maneuver" => "turn-left",
                 "polyline" =>
                 { "points" =>
                   "{opeFj{hjVCa@QeCEq@AQEg@Ek@AQCa@GcA[_FEk@a@}GEy@]_FCk@k@iIG{@[aFEk@AMEk@[aFEk@AOCm@]_FCk@Gk@QcCIyAGu@AQASAWCa@UcDGmAYkEO{B" },
                 "start_location" => { "lat" => 37.7729425, "lng" => -122.4391001 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "344 ft", "value" => 105 },
                 "duration" => { "text" => "1 min", "value" => 24 },
                 "end_location" => { "lat" => 37.7761668, "lng" => -122.4211769 },
                 "html_instructions" => "Turn <b>left</b> onto <b>Franklin St</b>",
                 "maneuver" => "turn-left",
                 "polyline" => { "points" => "e~peFbjejVa@Da@FYD}AR" },
                 "start_location" => { "lat" => 37.7752337, "lng" => -122.4209797 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "0.2 mi", "value" => 324 },
                 "duration" => { "text" => "2 mins", "value" => 97 },
                 "end_location" => { "lat" => 37.7765256, "lng" => -122.4175363 },
                 "html_instructions" => "Turn <b>right</b> onto <b>Fell St</b>",
                 "maneuver" => "turn-right",
                 "polyline" => { "points" => "adqeFjkejVIuA]eFAa@a@kGASAK@G?C@Q@U@Y" },
                 "start_location" => { "lat" => 37.7761668, "lng" => -122.4211769 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "0.2 mi", "value" => 391 },
                 "duration" => { "text" => "1 min", "value" => 66 },
                 "end_location" => { "lat" => 37.7740446, "lng" => -122.4143887 },
                 "html_instructions" =>
                 "Continue onto <b>10th St</b><div style=\"font-size:0.9em\">Pass by Peet's Coffee (on the right)</div>",
                 "polyline" =>
                 { "points" => "ifqeFrtdjVR]hA{Az@gAJMt@iABEX_@rAkBhAyAxAoB" },
                 "start_location" => { "lat" => 37.7765256, "lng" => -122.4175363 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "489 ft", "value" => 149 },
                 "duration" => { "text" => "1 min", "value" => 34 },
                 "end_location" => { "lat" => 37.7731024, "lng" => -122.4155943 },
                 "html_instructions" => "Turn <b>right</b> onto <b>Howard St</b>",
                 "maneuver" => "turn-right",
                 "polyline" => { "points" => "wvpeF|`djVzDnF" },
                 "start_location" => { "lat" => 37.7740446, "lng" => -122.4143887 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "0.1 mi", "value" => 193 },
                 "duration" => { "text" => "1 min", "value" => 47 },
                 "end_location" => { "lat" => 37.7718723, "lng" => -122.4140479 },
                 "html_instructions" =>
                 "Turn <b>left</b> at the 1st cross street onto <b>11th St</b>",
                 "maneuver" => "turn-left",
                 "polyline" => { "points" => "{ppeFlhdjVxAsB|AuB|@iA" },
                 "start_location" => { "lat" => 37.7731024, "lng" => -122.4155943 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "246 ft", "value" => 75 },
                 "duration" => { "text" => "1 min", "value" => 27 },
                 "end_location" => { "lat" => 37.7723502, "lng" => -122.4134489 },
                 "html_instructions" => "Turn <b>left</b> onto <b>Folsom St</b>",
                 "maneuver" => "turn-left",
                 "polyline" => { "points" => "eipeFx~cjV_BwB" },
                 "start_location" => { "lat" => 37.7718723, "lng" => -122.4140479 },
                 "travel_mode" => "DRIVING",
               },
               {
                 "distance" => { "text" => "115 ft", "value" => 35 },
                 "duration" => { "text" => "1 min", "value" => 18 },
                 "end_location" => { "lat" => 37.772122, "lng" => -122.4131672 },
                 "html_instructions" =>
                 "Turn <b>right</b> onto <b>Juniper St</b><div style=\"font-size:0.9em\">Destination will be on the right</div>",
                 "maneuver" => "turn-right",
                 "polyline" => { "points" => "elpeF`{cjVl@w@" },
                 "start_location" => { "lat" => 37.7723502, "lng" => -122.4134489 },
                 "travel_mode" => "DRIVING",
               },
             ],
             "traffic_speed_entry" => [],
             "via_waypoint" => [],
           },
         ],
        "overview_polyline" =>
         { "points" =>
           "}wjeFvyojVcGN_@@@TD|B@l@K?iJXiGP}p@nBmWr@gId@u@D}GXoBFsGPU@Ag@GmDGuE]wSYyOa@eWEaEM_JSeLIk@Qi@g@q@GGICs@aAYa@Q[_@iAyB_LiAaGGWMSc@UYGcAGy@D_@HcCbA_@NUHKE[CUGe@_@]i@KSg@mBYeAE[Ck@J}ATkCEeAmAmRaCg^iDci@c@cHsC\\e@FVhEPjCh@`IbBStAQUgDSwCuB{\\oE{q@iAsQO{Ba@D{@L}ARIuA_@gGc@sHDeA|AyBfAuAfD{EbDiEzDnFxAsBzC_E_BwBl@w@" },
        "summary" => "CA-1 N",
        "warnings" => [],
        "waypoint_order" => [0],
      }],
      "status" => "OK",
    }
  end
end
