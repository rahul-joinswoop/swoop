# frozen_string_literal: true

require "rails_helper"

describe Agero::RSA::MapDisablementReason do
  subject { described_class.call(job: job).result }

  let(:job) { build_stubbed(:job, symptom: symptom) }

  context "when symptom is 'Customer unsafe'" do
    let(:symptom) { build_stubbed(:symptom, name: "Customer unsafe") }

    it { is_expected.to eq("Tow (Not leaking fuel)") }
  end

  context "when symptom is 'Dead battery'" do
    let(:symptom) { build_stubbed(:symptom, name: "Dead battery") }

    it { is_expected.to eq("Jump Start (Did not stall while driving)") }
  end

  context "when symptom is 'Flat tire'" do
    let(:job) { create(:job, symptom: symptom) }
    let(:symptom) { build_stubbed(:symptom, name: "Flat tire") }

    let!(:tire_question) do
      create(:question, question: Question::WHICH_TIRE,
                        with_answers: [
                          "Front right",
                          "Multiple",
                        ])
    end

    let!(:spare_question) do
      create(:question, question: Question::HAVE_A_SPARE,
                        with_answers: [
                          "Yes",
                          "No",
                        ])
    end

    context "and multiple tires are not damaged" do
      let!(:tire_answer) do
        create(:question_result, job: job,
                                 question: tire_question,
                                 answer: tire_question.answers.first)
      end

      context "and customer has a spare" do
        let!(:spare_answer) do
          create(:question_result, job: job,
                                   question: spare_question,
                                   answer: spare_question.answers.first)
        end

        it { is_expected.to eq("One Flat Tire - Good spare") }
      end

      context "and customer does not have a spare" do
        let!(:spare_answer) do
          create(:question_result, job: job,
                                   question: spare_question,
                                   answer: spare_question.answers.last)
        end

        it { is_expected.to eq("Tow (Not leaking fuel)") }
      end
    end

    context "and multiple tires are damaged" do
      let!(:tire_answer) do
        create(:question_result, job: job,
                                 question: tire_question,
                                 answer: tire_question.answers.last)
      end

      it { is_expected.to eq("Multiple Flat Tires") }
    end
  end

  context "when symptom is 'Locked out'" do
    let(:job) { create(:job, symptom: symptom) }
    let(:symptom) { build_stubbed(:symptom, name: "Locked out") }

    let!(:locked_out_question) do
      create(:question, question: Question::WHERE_ARE_THE_KEYS,
                        with_answers: [
                          "Inside Vehicle",
                          "Trunk (inaccessible)",
                        ])
    end

    context "and the keys are accessible" do
      let!(:locked_out_answer) do
        create(:question_result, job: job,
                                 question: locked_out_question,
                                 answer: locked_out_question.answers.first)
      end

      it { is_expected.to eq("Lockout - Keys in car") }
    end

    context "and the keys are inaccessible" do
      let!(:locked_out_answer) do
        create(:question_result, job: job,
                                 question: locked_out_question,
                                 answer: locked_out_question.answers.last)
      end

      it { is_expected.to eq("Lockout - Keys broken/missing") }
    end
  end

  context "when symptom is 'Mechanical issue'" do
    let(:symptom) { build_stubbed(:symptom, name: "Mechanical issue") }

    it { is_expected.to eq("Tow (Not leaking fuel)") }
  end

  context "when symptom is 'No issue reported'" do
    let(:symptom) { build_stubbed(:symptom, name: "No issue reported") }

    it { is_expected.to eq("Unknown Problem") }
  end

  context "when symptom is 'Other'" do
    let(:symptom) { build_stubbed(:symptom, name: "Other") }

    it { is_expected.to eq("Unknown Problem") }
  end
end
