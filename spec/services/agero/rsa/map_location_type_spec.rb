# frozen_string_literal: true

require 'rails_helper'

describe Agero::RSA::MapLocationType do
  subject { described_class.call(location_type: location_type).result }

  context "for Auto Body Shop" do
    let(:location_type) { "Auto Body Shop" }

    it { is_expected.to eq("Residence/Work-Driveway") }
  end

  context "for Blocking Traffic" do
    let(:location_type) { "Blocking Traffic" }

    it { is_expected.to eq("Roadside-Street") }
  end

  context "for Business" do
    let(:location_type) { "Business" }

    it { is_expected.to eq("Residence/Work-Driveway") }
  end

  context "for Gated Community" do
    let(:location_type) { "Gated Community" }

    it { is_expected.to eq("Residence/Work-Driveway") }
  end

  context "for Highway" do
    let(:location_type) { "Highway" }

    it { is_expected.to eq("Roadside-Highway") }
  end

  context "for Impound Lot" do
    let(:location_type) { "Impound Lot" }

    it { is_expected.to eq("Residence/Work-Driveway") }
  end

  context "for Intersection" do
    let(:location_type) { "Intersection" }

    it { is_expected.to eq("Residence/Work-Street") }
  end

  context "for Local Roadside" do
    let(:location_type) { "Local Roadside" }

    it { is_expected.to eq("Residence/Work-Street") }
  end

  context "for Low Clearance" do
    let(:location_type) { "Low Clearance" }

    it { is_expected.to eq("Residence/Work-ParkingGarage") }
  end

  context "for Parking Garage" do
    let(:location_type) { "Parking Garage" }

    it { is_expected.to eq("GeneralParkingGarage") }
  end

  context "for Parking Lot" do
    let(:location_type) { "Parking Lot" }

    it { is_expected.to eq("GeneralParkingLot") }
  end

  context "for Point of Interest" do
    let(:location_type) { "Point of Interest" }

    it { is_expected.to eq("Residence/Work-Driveway") }
  end

  context "for Residence" do
    let(:location_type) { "Residence" }

    it { is_expected.to eq("Residence/Work-Driveway") }
  end

  context "for Storage Facility" do
    let(:location_type) { "Storage Facility" }

    it { is_expected.to eq("Residence/Work-Driveway") }
  end

  context "for Tow Company" do
    let(:location_type) { "Tow Company" }

    it { is_expected.to eq("Residence/Work-Driveway") }
  end
end
