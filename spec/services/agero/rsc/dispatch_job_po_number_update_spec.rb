# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::DispatchJobPoNumberUpdate do
  let(:job) do
    create :job, {
      po_number: swoop_po_number,
      issc_dispatch_request: issc_dispatch_request,
    }
  end
  let(:issc_dispatch_request) { create(:issc_dispatch_request) }
  let(:dispatch) { { 'poNumber' => rsc_po_number } }
  let(:rsc_api) { double(get_dispatch: dispatch) }
  let(:context) { { job: job } }

  subject(:result) { described_class.call(context) }

  context "Swoop's job PO Number is nil" do
    let(:swoop_po_number) { nil }
    let(:rsc_po_number) { '12345' }

    it "updates the job's PO Number with the RSC dispatch results" do
      expect(Agero::Rsc::API).to receive(:for_job).with(job.id).and_return(rsc_api)
      expect(result.success?).to be true
      expect(job.po_number).to eq rsc_po_number
    end
  end

  context "Swoop's job PO Number is already set" do
    let(:swoop_po_number) { '54321' }
    let(:rsc_po_number) { '12345' }

    it "maintains the original po_number and the interactor fails" do
      expect(Agero::Rsc::API).to receive(:for_job).with(job.id).and_return(rsc_api)
      expect(result.success?).to be false
      expect(job.po_number).to eq swoop_po_number
    end
  end

  context "RSC returns with empty PO Number" do
    let(:swoop_po_number) { nil }
    let(:rsc_po_number) { '' }

    it "maintains the original po_number and the interactor fails" do
      expect(Agero::Rsc::API).to receive(:for_job).with(job.id).and_return(rsc_api)
      expect(result.success?).to be false
      expect(job.po_number).to eq swoop_po_number
    end
  end
end
