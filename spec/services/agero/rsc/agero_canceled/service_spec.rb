# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::AgeroCanceled::Service do
  include_context 'basic rsc setup'

  subject(:service_call) do
    Agero::Rsc::AgeroCanceled::Service.call(
      input: { callback_token: api_access_token.callback_token, event: event }
    )

    job.reload
    issc_dispatch_request.reload
  end

  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: "Job Canceled",
      message: "Job Canceled",
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:notification_code) { 5 }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { 123 }

  let!(:agero_account) do
    create(:account, company: rescue_company, client_company: agero_company)
  end

  let(:job) do
    create :fleet_motor_club_job, {
      status: job_status,
      fleet_company: agero_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
    }
  end

  let!(:issc_dispatch_request) do
    create(
      :issc_dispatch_request,
      dispatchid: dispatch_request_number,
      status: issc_dispatch_request_status,
      issc: issc,
      max_eta: 90.minutes,
      job: job
    )
  end

  context 'when job.status is set to any before canceled or GOA' do
    let(:job_status) { Job::STATUS_ACCEPTED }
    let(:issc_dispatch_request_status) { 'accepted' }

    it 'moves job.status from accepted to canceled' do
      expect { service_call }.to change(job, :status).from(Job::STATUS_ACCEPTED).to(Job::STATUS_CANCELED)
    end

    it 'moves job.issc_dispatch_request from new_request to canceled' do
      expect { service_call }.to change(issc_dispatch_request, :status)
        .from('accepted').to('canceled')
    end
  end

  context 'when job.status is canceled' do
    let(:job_status) { Job::STATUS_CANCELED }
    let(:issc_dispatch_request_status) { 'canceled' }

    it 'does not change job.status' do
      expect { service_call }.to avoid_changing(job, :status)
    end

    it 'does not change job.issc_dispatch_request.status' do
      expect { service_call }.to avoid_changing(issc_dispatch_request, :status)
    end
  end

  context 'when job.status is goa' do
    let(:job_status) { 'goa' }
    let(:issc_dispatch_request_status) { 'canceled' }

    it 'does not change job.status' do
      expect { service_call }.to avoid_changing(job, :status)
    end

    it 'does not change job.issc_dispatch_request.status' do
      expect { service_call }.to avoid_changing(issc_dispatch_request, :status)
    end
  end
end
