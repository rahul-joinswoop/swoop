# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::SignOutVendorIds do
  subject { Agero::Rsc::SignOutVendorIds.new }

  before do
    stub_const 'CONTRACTORID_1', '9874598347'
    stub_const 'CONTRACTORID_2', '87675498'
  end

  let!(:agero) { create(:fleet_company, :ago) }
  let!(:api_access_token) do
    create(:api_access_token, {
      vendorid: CONTRACTORID_1,
      company: rescue_company,
    })
  end
  let!(:api_access_token2) do
    create(:api_access_token, {
      vendorid: CONTRACTORID_2,
      company: rescue_company,
    })
  end
  let!(:rescue_company) { create(:rescue_company) }
  let!(:account) do
    create(:account, company: rescue_company, client_company: agero)
  end
  let!(:rsc_issc1) do
    create :isscs, {
      clientid: 'AGO',
      company: rescue_company,
      api_access_token: api_access_token,
      status: Issc::LOGGED_IN,
      contractorid: CONTRACTORID_1,
    }
  end

  context 'two Isscs' do
    let!(:rsc_issc_cid_2) do
      create :isscs, {
        clientid: 'AGO',
        company: rescue_company,
        api_access_token: api_access_token2,
        status: Issc::LOGGED_IN,
        contractorid: CONTRACTORID_2,
      }
    end

    context 'two eligible, one requested' do
      it 'does sign out first' do
        expect(Agero::Rsc::API).to(receive(:new)).with(api_access_token.access_token).and_call_original
        expect_any_instance_of(Agero::Rsc::API).to(receive(:sign_out))
        expect do
          subject.perform(CONTRACTORID_1 => [rsc_issc1]) && rsc_issc1.reload
        end
          .to change(rsc_issc1, :status).from('logged_in').to('registered')
      end

      it 'does not sign out second' do
        expect(Agero::Rsc::API).to(receive(:new)).with(api_access_token.access_token).and_call_original
        expect_any_instance_of(Agero::Rsc::API).to(receive(:sign_out))
        expect do
          subject.perform(CONTRACTORID_1 => [rsc_issc1]) && rsc_issc_cid_2.reload
        end
          .to avoid_changing(rsc_issc_cid_2, :status)
      end
    end

    context 'multiple issc+locations, single contractorid' do
      let(:location2) { create(:issc_location, :default) }
      let!(:rsc_issc2) do
        create :isscs, {
          clientid: 'AGO',
          company: rescue_company,
          api_access_token: api_access_token,
          status: Issc::LOGGED_IN,
          contractorid: CONTRACTORID_1,
          issc_location: location2,
        }
      end

      context 'both logging out' do
        it 'calls signout first' do
          expect(Agero::Rsc::API).to(receive(:new)).with(api_access_token.access_token).and_call_original
          expect_any_instance_of(Agero::Rsc::API).to(receive(:sign_out))
          expect do
            subject.perform(CONTRACTORID_1 => [rsc_issc1, rsc_issc2]) && rsc_issc1.reload
          end
            .to change(rsc_issc1, :status).from('logged_in').to('registered')
        end

        it 'calls signout second' do
          expect(Agero::Rsc::API).to(receive(:new)).with(api_access_token.access_token).and_call_original
          expect_any_instance_of(Agero::Rsc::API).to(receive(:sign_out))
          expect do
            subject.perform(CONTRACTORID_1 => [rsc_issc1, rsc_issc2]) && rsc_issc2.reload
          end
            .to change(rsc_issc2, :status).from('logged_in').to('registered')
        end
      end

      context 'calls one logout' do
        it 'does not signout first' do
          expect do
            subject.perform(CONTRACTORID_1 => [rsc_issc1]) && rsc_issc1.reload
          end
            .to avoid_changing(rsc_issc1, :status)
        end
      end
    end

    context 'multiple vendorids and multiple locations/isscs for first' do
      let(:location2) { create(:issc_location, :default) }
      let!(:rsc_issc2) do
        create :isscs, {
          clientid: 'AGO',
          company: rescue_company,
          api_access_token: api_access_token,
          status: Issc::LOGGED_IN,
          contractorid: CONTRACTORID_1,
          issc_location: location2,
        }
      end

      let!(:rsc_issc_cid_2) do
        create :isscs, {
          clientid: 'AGO',
          company: rescue_company,
          api_access_token: api_access_token2,
          status: Issc::LOGGED_IN,
          contractorid: CONTRACTORID_2,
        }
      end

      context 'second eligible, both requested' do
        it 'does not sign out first - rsc_issc2 still in hours' do
          allow(Agero::Rsc::API).to(receive(:new)).with(api_access_token.access_token).and_call_original
          allow_any_instance_of(Agero::Rsc::API).to(receive(:sign_out))
          expect do
            subject.perform(CONTRACTORID_1 => [rsc_issc1], CONTRACTORID_2 => [rsc_issc_cid_2]) && rsc_issc1.reload
          end
            .to avoid_changing(rsc_issc1, :status)
        end

        it 'does sign out second' do
          allow(Agero::Rsc::API).to(receive(:new)).with(api_access_token.access_token).and_call_original
          allow_any_instance_of(Agero::Rsc::API).to(receive(:sign_out))
          expect do
            subject.perform(CONTRACTORID_1 => [rsc_issc1], CONTRACTORID_2 => [rsc_issc_cid_2]) && rsc_issc_cid_2.reload
          end
            .to change(rsc_issc_cid_2, :status).from('logged_in').to('registered')
        end
      end
    end
  end
end
