# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::NewJobOffered::Service do
  include_context "basic rsc setup"

  subject(:service_call) do
    Agero::Rsc::NewJobOffered::Service.call(
      input: { callback_token: api_access_token.callback_token, event: event }
    )
  end

  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: title,
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:notification_code) { 1 }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { '457460776' }
  let(:title) { 'New Offer' }

  let!(:audit_issc) { create(:audit_issc, dispatchid: dispatch_request_number) }

  before do
    stub_request(:get, Agero::Rsc::Client.url("/dispatches/457460776/detail"))
      .to_return(
        status: 200,
        headers: { "Content-Type" => "application/json" },
        body: rsc_api_response(:get_dispatch)
      )

    allow(Agero::Rsc::API).to receive(:new).with(api_access_token.access_token).and_return(rsc_api)
    allow(Agero::Rsc::NewJobOffered::Organizer).to receive(:call).and_call_original
  end

  it 'calls Agero::Rsc::NewJobOffered::Organizer' do
    expect(Agero::Rsc::NewJobOffered::Organizer).to receive(:call).with(
      input: {
        callback_token: api_access_token.callback_token,
        event: event,
      }
    )

    service_call
  end

  it 'increases job count by 1' do
    expect { service_call }.to change(FleetMotorClubJob, :count).by(1)
  end

  it 'updates previous AuditIssc rows with the job id' do
    expect do
      service_call
      audit_issc.reload
    end.to change(audit_issc, :job_id).from(nil).to(Integer)
  end

  describe '#kick_async_workers_after_job_created' do
    let!(:job_mock) do
      Agero::Rsc::NewJobOffered::Organizer.call(
        input: { callback_token: api_access_token.callback_token, event: event }
      ).output[:new_fleet_motor_club_job]
    end

    let(:organizer_mock) do
      double(output: { new_fleet_motor_club_job: job_mock })
    end

    let(:resolver_mock) do
      API::Jobs::ResolveLatLngIfBlank.new(job_id: job_mock.id)
    end

    let(:email_sender_mock) do
      JobStatusEmailSender.new(job_mock.id, 'JobStatusEmail::Created')
    end

    before do
      allow(Agero::Rsc::NewJobOffered::Organizer).to receive(:call).and_return(organizer_mock)
      allow(API::Jobs::ResolveLatLngIfBlank).to receive(:new).with(job_id: job_mock.id).and_return(
        resolver_mock
      )
      allow(JobStatusEmailSender).to receive(:new).with(job_mock.id, 'JobStatusEmail::Created')
        .and_return(email_sender_mock)
      allow_any_instance_of(Agero::Rsc::NewJobOffered::Service).to receive(:already_have_request?).and_return(false)
    end

    it 'schedules API::Jobs::ResolveLatLngIfBlank' do
      expect(resolver_mock).to receive(:call)

      service_call
    end

    it 'enqueues Delayed::Job with JobStatusEmailSender' do
      expect(Delayed::Job).to receive(:enqueue).with(email_sender_mock)

      service_call
    end
  end

  context 'when StandardError occurs' do
    let(:organizer_mock) do
      double(output: { new_fleet_motor_club_job: job_mock })
    end

    before do
      allow(job_mock).to receive(:assign_rescue_company).and_raise(StandardError)
      allow(Agero::Rsc::NewJobOffered::Organizer).to receive(:call).and_return(organizer_mock)
      allow_any_instance_of(Agero::Rsc::NewJobOffered::Service).to receive(:already_have_request?).and_return(false)
    end

    context 'and the Job is already saved' do
      let!(:job_mock) do
        Agero::Rsc::NewJobOffered::Organizer.call(
          input: { callback_token: api_access_token.callback_token, event: event }
        ).output[:new_fleet_motor_club_job]
      end

      it 'pushes the Job to canceled status' do
        expect { service_call }.to raise_error(StandardError)

        job_mock.reload

        expect(job_mock.status).to eq Job::STATUS_CANCELED
      end

      it 'adds canceled AJS to the job' do
        expect { service_call }.to raise_error(StandardError)

        job_mock.reload

        expect(job_mock.ajs_by_status_id(JobStatus::CANCELED)).to be_present
      end
    end

    context 'and the Job has not been not saved' do
      let!(:job_mock) { build(:fleet_motor_club_job) }

      it 'is not persisted or canceled' do
        expect { service_call }.to raise_error(StandardError)

        expect(job_mock).not_to be_persisted
        expect(job_mock.status).not_to eq Job::STATUS_CANCELED
      end
    end
  end

  describe "duplicate message" do
    it "ignores duplicate new offer messages" do
      issc.dispatch_requests.create!(dispatchid: dispatch_request_number)
      expect { service_call }.not_to change(Job, :count)
    end
  end
end
