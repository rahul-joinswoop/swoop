# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::NewJobOffered::BuildFleetMotorClubJobFromDispatchDetails do
  include_context "basic rsc setup"

  describe 'output[:new_fleet_motor_club_job]' do
    subject(:new_mc_job) do
      Agero::Rsc::NewJobOffered::BuildFleetMotorClubJobFromDispatchDetails.call(
        input: {
          callback_token: api_access_token.callback_token,
          event: event,
          dispatch_details: dispatch_details,
        }
      ).output[:new_fleet_motor_club_job]
    end

    let(:event) do
      {
        notificationEventId: notification_event_id,
        notificationCode: notification_code,
        vendorId: vendor_id,
        facilityId: facility_id,
        dispatchSource: dispatch_source,
        dispatchRequestNumber: dispatch_request_number,
        title: title,
      }
    end

    let(:notification_event_id) { '123456789' }
    let(:notification_code) { 1 }
    let(:vendor_id) { '50230' }
    let(:facility_id) { '1' }
    let(:dispatch_source) { 'eDispatch' }
    let(:dispatch_request_number) { 123 }
    let(:title) { 'New Offer' }
    let(:urgency) { 'STANDARD' }

    let(:dispatch_details) do
      dispatch_details = JSON.load(rsc_api_response(:get_dispatch))

      dispatch_details["urgency"] = urgency
      dispatch_details["referenceNumber"] = "REF_NUMBER_123"

      dispatch_details["disablementLocation"]["contactInfo"]["name"] = "Customer name"
      dispatch_details["disablementLocation"]["contactInfo"]["callbackNumber"] = "4059876543"

      dispatch_details
    end

    let(:service_tow) { create(:service_code, name: ServiceCode::TOW) }

    before :each do
      allow_any_instance_of(Agero::Rsc::NewJobOffered::BuildFleetMotorClubJobFromDispatchDetails).to receive(:dispatch_details).and_return(dispatch_details)
    end

    it { is_expected.to be_instance_of FleetMotorClubJob }

    describe 'new_mc_job.fleet_dispatcher' do
      subject(:new_mc_job_fleet_dispatcher) { new_mc_job.fleet_dispatcher }

      before do
        dispatch_details["disablementLocation"]["dispatcherContactNumber"] = "1234567890"
      end

      let!(:user) { create(:user, phone: "1234567890") }

      it { is_expected.to eq(user) }
    end

    describe 'new_mc_job.account_id' do
      subject(:new_mc_job_account_id) { new_mc_job.account_id }

      it { is_expected.to eq agero_account.id }
    end

    describe 'new_mc_job.answer_by', freeze_time: true do
      subject(:new_mc_job_answer_by) { new_mc_job.answer_by }

      it "is offset by the receivedTime" do
        dispatch_details["receivedTime"] =
          (Agero::Rsc::NewJobOffered::BuildFleetMotorClubJobFromDispatchDetails::ANSWER_BY.ago + 10.seconds).to_s

        # In the next 10 seconds
        expect(new_mc_job_answer_by).to be_within(0.1.seconds).of(10.seconds.from_now)
      end

      it "defaults to the status history statusTime if no receivedTime" do
        dispatch_details["statusHistory"] = [
          { "code" => "2", "statusTime" => 180.seconds.ago.to_s },
        ]

        # In the next 30 seconds
        expect(new_mc_job_answer_by).to be_within(0.1.seconds).of((Agero::Rsc::NewJobOffered::BuildFleetMotorClubJobFromDispatchDetails::ANSWER_BY - 180.seconds).from_now)
      end
    end

    describe 'new_mc_job.fleet_company_id' do
      subject(:new_mc_job_fleet_company_id) { new_mc_job.fleet_company_id }

      it { is_expected.to eq agero_account.client_company.id }
    end

    describe 'new_mc_job.rescue_company_id' do
      subject(:new_mc_job_rescue_company_id) { new_mc_job.rescue_company_id }

      it { is_expected.to eq agero_account.company.id }
    end

    describe 'new_mc_job.issc_dispatch_request' do
      subject(:new_mc_job_issc_dispatch_request) { new_mc_job.issc_dispatch_request }

      it 'builds the expected issc_dispatch_request' do
        expect(new_mc_job_issc_dispatch_request.dispatchid).to eq '457460776'
        expect(new_mc_job_issc_dispatch_request.club_job_type).to eq 'Digital'
        expect(new_mc_job_issc_dispatch_request.status).to eq 'new_request'
        expect(new_mc_job_issc_dispatch_request.max_eta).to eq 90 # minutes (as int value on DB)
        expect(new_mc_job_issc_dispatch_request.issc.id).to eq issc.id
      end
    end

    describe 'new_mc_job.po_number' do
      subject { new_mc_job.po_number }

      it { is_expected.to eq "115315731" }
    end

    describe 'new_mc_job.driver.user' do
      subject(:new_mc_job_customer) { new_mc_job.driver.user }

      it 'holds customer name accordingly' do
        expect(new_mc_job_customer.full_name).to eq 'Customer name'
        expect(new_mc_job_customer.phone).to eq '+14059876543'
      end
    end

    describe 'new_mc_job.driver.vehicle' do
      subject(:new_mc_job_driver_vehicle) { new_mc_job.driver.vehicle }

      it 'holds customer accordingly' do
        expect(new_mc_job_driver_vehicle.vin).to eq '123123123'
        expect(new_mc_job_driver_vehicle.year).to eq 2007
        expect(new_mc_job_driver_vehicle.make).to eq 'Hyundai'
        expect(new_mc_job_driver_vehicle.model).to eq 'SantaFe'
        expect(new_mc_job_driver_vehicle.color).to eq 'Unknown'
        expect(new_mc_job_driver_vehicle.license).to eq 'ABC1234'
      end
    end

    describe 'new_mc_job.dropoff_contact' do
      subject(:new_mc_job_dropoff_contact) { new_mc_job.dropoff_contact }

      it 'holds dropoff_contact accordingly' do
        expect(new_mc_job_dropoff_contact.full_name).to eq 'HUBHYUNDAIWEST'
        expect(new_mc_job_dropoff_contact.phone).to eq ""
      end
    end

    describe 'new_mc_job.service_code' do
      subject(:new_mc_service_code) { new_mc_job.service_code }

      it 'holds service_code accordingly' do
        expect(new_mc_service_code).to be_present
      end
    end

    describe 'new_mc_job.service_location' do
      subject(:new_mc_service_location) { new_mc_job.service_location }

      it 'holds the pickup location accordingly' do
        expect(new_mc_service_location.address).to eq "22147LeiropDr, Richmond, TX 77407"
        expect(new_mc_service_location.street).to eq '22147LeiropDr'
        expect(new_mc_service_location.city).to eq 'Richmond'
        expect(new_mc_service_location.state).to eq 'TX'
        expect(new_mc_service_location.zip).to eq '77407'
        expect(new_mc_service_location.lat).to eq 29.68697143715451
        expect(new_mc_service_location.lng).to eq 95.7644752123639
      end
    end

    describe 'new_mc_job.drop_location' do
      subject(:new_mc_drop_location) { new_mc_job.drop_location }

      it 'holds the pickup location accordingly' do
        expect(new_mc_drop_location.address).to eq '17007KATYFREEWAY, HOUSTON, TX 77094'
        expect(new_mc_drop_location.street).to eq '17007KATYFREEWAY'
        expect(new_mc_drop_location.city).to eq 'HOUSTON'
        expect(new_mc_drop_location.state).to eq 'TX'
        expect(new_mc_drop_location.zip).to eq '77094'
        expect(new_mc_drop_location.lat).to eq 29.7845
        expect(new_mc_drop_location.lng).to eq 95.6785
      end
    end

    describe 'new_mc_job.odometer' do
      subject(:new_mc_job_odometer) { new_mc_job.odometer }

      it { is_expected.to eq 100 }
    end

    context 'when dispatch_details does not come with a towDestination' do
      let(:dispatch_details) { JSON.load(rsc_api_response(:get_dispatch_winch)) }

      it 'does not raise error' do
        expect { new_mc_job }.not_to raise_error
      end
    end

    describe "site" do
      before :each do
        rescue_company.sites.each_with_index { |s, i| s.update!(name: "Site #{i}") }
        rescue_company.sites << create(:site, company: rescue_company, name: "HQ")
      end

      it "sets the site from the issc record site" do
        job = new_mc_job
        expect(job.site).not_to eq nil
        expect(job.site).to eq issc.site
      end

      it "defaults to the rescue company site if the issc site is not set" do
        issc.update!(site_id: nil)
        job = new_mc_job
        expect(job.site).not_to eq nil
        expect(job.site).to eq rescue_company.hq
      end
    end
  end
end
