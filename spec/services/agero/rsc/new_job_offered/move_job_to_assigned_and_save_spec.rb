# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::NewJobOffered::MoveJobToAssignedAndSave do
  include_context "basic rsc setup"

  subject(:service_call) do
    Agero::Rsc::NewJobOffered::MoveJobToAssignedAndSave.call(
      input: {
        callback_token: api_access_token.callback_token,
        event: event,
        new_fleet_motor_club_job: new_fleet_motor_club_job,
      }
    )
  end

  let(:new_fleet_motor_club_job) do
    Agero::Rsc::NewJobOffered::BuildFleetMotorClubJobFromDispatchDetails.call({
      input: {
        callback_token: api_access_token.callback_token,
        event: event,
      },
    }).output[:new_fleet_motor_club_job]
  end

  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: title,
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:notification_code) { 1 }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { 123 }
  let(:title) { 'New Offer' }

  before do
    stub_request(:get, Agero::Rsc::Client.url("/dispatches/123/detail"))
      .to_return(
        status: 200,
        headers: { "Content-Type" => "application/json" },
        body: rsc_api_response(:get_dispatch)
      )

    allow(Agero::Rsc::API).to receive(:new).with(api_access_token.access_token).and_return(rsc_api)
    allow(Agero::Rsc::NewJobOffered::Organizer).to receive(:call).and_call_original
  end

  it 'increases job count by 1' do
    expect { service_call }.to change(FleetMotorClubJob, :count).by(1)
  end

  it 'sets job.status to assigned' do
    job = service_call.output[:new_fleet_motor_club_job]

    expect(job.status).to eq 'assigned'
  end

  it 'calls GraphQL::JobStatusChangedWorker' do
    expect(GraphQL::JobStatusChangedWorker)
      .to receive(:perform_async)
      .once
      .with(anything, { status: [nil, 'assigned'], rescue_company_id: [nil, anything] })
    service_call
  end

  it 'calls Notification::EtaRequestedWorker' do
    expect(Notification::EtaRequestedWorker)
      .to receive(:perform_async)
      .once
      .with(anything)
    service_call
  end

  describe 'new_fleet_motor_club_job.audit_job_statuses_order_created' do
    subject(:audit_job_statuses) { job_mock.audit_job_statuses_order_created }

    let!(:job_mock) do
      Agero::Rsc::NewJobOffered::Organizer.call(
        input: { callback_token: api_access_token.callback_token, event: event }
      ).output[:new_fleet_motor_club_job]
    end

    let(:organizer_mock) do
      double(output: { new_fleet_motor_club_job: job_mock })
    end

    before do
      allow(Agero::Rsc::NewJobOffered::Organizer).to receive(:call).and_return(organizer_mock)
    end

    it 'adds expected AuditJobStatuses to the job' do
      expect(audit_job_statuses.first.job_status.id).to eq JobStatus::INITIAL
      expect(audit_job_statuses.second.job_status.id).to eq JobStatus::CREATED
      expect(audit_job_statuses.third.job_status.id).to eq JobStatus::PENDING
      expect(audit_job_statuses.fourth.job_status.id).to eq JobStatus::ASSIGNED
    end
  end
end
