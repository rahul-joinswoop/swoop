# frozen_string_literal: true

require "rails_helper"

describe Agero::Rsc::MatchLocationToSite do
  subject do
    Agero::Rsc::MatchLocationToSite.call(
      company: company,
      facility: facility,
    )
  end

  let!(:company) { create(:rescue_company) }
  let!(:facility) do
    {
      "id": "1",
      "name": "Douglas Transport-ASM",
      "address": "2040 Telegraph Ave",
      "city": "Medford",
      "state": "MA",
      "zipcode": "94612",
      "lat": "42.0123",
      "lon": "80.1153",
    }.with_indifferent_access
  end

  context "when no matches" do
    it "returns nil" do
      expect(subject.result).to eq(nil)
    end
  end

  context "when zip matches" do
    let!(:location) do
      create(:location, :oakland_beer_garden)
    end

    let!(:site) do
      create(:site, location: location, company: company)
    end

    # There's a two way association here, we need to ensure both are populated
    before do
      location.site = site
      location.save!
    end

    it "returns site" do
      expect(subject.result).to eq(site)
    end
  end
end
