# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::RequestCallBack, freeze_time: true do
  subject(:request_call_back) do
    Agero::Rsc::RequestCallBack.new(
      job: job,
      api_user: api_user,
      call_back_hash: call_back_hash,
    ).call
  end

  let(:job) do
    create :fleet_motor_club_job, {
      status: 'assigned',
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
      answer_by: Time.now + 90,
    }
  end

  let(:api_user) { create(:user, company: rescue_company) }

  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company) }

  let(:call_back_hash) do
    {
      dispatch_number: '555555555',
      secondary_number: '555555566',
    }
  end

  it 'changes job.partner_dispatcher from nil to api_user' do
    expect { request_call_back && job.reload }.to change(job, :partner_dispatcher)
      .from(nil).to(api_user)
  end

  it 'changes job.answer_by from Time to nil' do
    expect { request_call_back && job.reload }.to change(job, :answer_by)
      .from(Time).to(nil)
  end

  it 'changes job.status from assigned to pending' do
    expect { request_call_back && job.reload }.to change(job, :status)
      .from('assigned').to(Job::STATUS_PENDING)
  end

  it 'schedules an Agero::Rsc::RequestCallBackWorker job' do
    expect(Agero::Rsc::RequestCallBackWorker).to receive(:perform_async).with(
      job.id, call_back_hash
    )

    request_call_back
  end
end
