# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::SignInVendorIds do
  subject { Agero::Rsc::SignInVendorIds.new }

  before do
    stub_const 'CONTRACTORID_1', '9874598347'
    stub_const 'CONTRACTORID_2', '87675498'
  end

  let!(:agero) { create(:fleet_company, :ago) }
  let!(:api_access_token) do
    create(:api_access_token, {
      vendorid: CONTRACTORID_1,
      company: rescue_company,
    })
  end
  let!(:rescue_company) { create(:rescue_company) }
  let!(:account) do
    create(:account, company: rescue_company, client_company: agero)
  end
  let!(:rsc_issc1) do
    create :isscs, {
      clientid: 'AGO',
      company: rescue_company,
      api_access_token: api_access_token,
      status: Issc::REGISTERED,
      contractorid: CONTRACTORID_1,
    }
  end

  context 'single location' do
    let!(:api_access_token_2) do
      create(:api_access_token, {
        vendorid: CONTRACTORID_2,
        company: rescue_company,
      })
    end
    let!(:rsc_issc_cid_2) do
      create :isscs, {
        clientid: 'AGO',
        company: rescue_company,
        api_access_token: api_access_token_2,
        status: Issc::REGISTERED,
        contractorid: CONTRACTORID_2,
      }
    end

    before do
      allow(Agero::Rsc::API).to(receive(:new)).with(api_access_token.access_token).and_call_original
      allow_any_instance_of(Agero::Rsc::API).to(receive(:sign_in))
    end

    it 'calls signin' do
      expect do
        subject.perform(CONTRACTORID_1 => [rsc_issc1]) && rsc_issc1.reload
      end
        .to change(rsc_issc1, :status).from('registered').to('logged_in')
    end

    it 'does not sign in CID 2' do
      expect do
        subject.perform(CONTRACTORID_1 => [rsc_issc1]) && rsc_issc1.reload && rsc_issc_cid_2.reload
      end
        .to avoid_changing(rsc_issc_cid_2, :status)
    end

    it 'sets logged_in_at' do
      expect do
        subject.perform(CONTRACTORID_1 => [rsc_issc1]) && rsc_issc1.reload
      end
        .to change(rsc_issc1, :logged_in_at).from(nil).to(Time)
    end

    context 'multiple locations one issc' do
      let(:location2) { create(:issc_location, :default) }
      let!(:rsc_issc2) do
        create :isscs, {
          clientid: 'AGO',
          company: rescue_company,
          api_access_token: api_access_token,
          status: Issc::REGISTERED,
          contractorid: CONTRACTORID_1,
          issc_location: location2,
        }
      end

      it 'calls signin first' do
        expect do
          subject.perform(CONTRACTORID_1 => [rsc_issc1], CONTRACTORID_2 => [rsc_issc2]) && rsc_issc1.reload
        end
          .to change(rsc_issc1, :status).from('registered').to('logged_in')
      end

      it 'calls signin second' do
        expect do
          subject.perform(CONTRACTORID_1 => [rsc_issc1], CONTRACTORID_2 => [rsc_issc2]) && rsc_issc2.reload
        end
          .to change(rsc_issc2, :status).from('registered').to('logged_in')
      end
    end
  end
end
