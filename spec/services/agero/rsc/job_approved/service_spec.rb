# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::JobApproved::Service do
  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: "Job Approved",
      message: "Your job was approved",
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:notification_code) { 3 }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { 123 }

  let(:api_access_token) { create(:api_access_token, company: rescue_company, vendorid: vendor_id) }
  let(:rsc_api) { Agero::Rsc::API.new(api_access_token.access_token) }

  let!(:agero_account) do
    create(:account, company: rescue_company, client_company: agero_company)
  end
  let(:agero_company) { create(:fleet_company, :agero) }
  let(:rescue_company) { create(:rescue_company) }
  let!(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM, api_access_token: api_access_token) }

  let(:dispatch_source) { "eDispatch" }
  let(:dispatch_details) do
    dispatch_details = JSON.load(rsc_api_response(:get_dispatch))
    dispatch_details["poNumber"] = "NEWPONUMBER"
    dispatch_details["dispatchSource"] = dispatch_source
    dispatch_details
  end

  before :each do
    allow_any_instance_of(Agero::Rsc::JobApproved::Service).to receive(:dispatch_details).and_return(dispatch_details)
  end

  context "existing job" do
    let(:job) do
      create :fleet_motor_club_job, {
        status: job_status,
        fleet_company: agero_company,
        adjusted_created_at: Time.now,
        rescue_company: rescue_company,
      }
    end

    let!(:issc_dispatch_request) do
      create(
        :issc_dispatch_request,
        dispatchid: dispatch_request_number,
        issc: issc,
        max_eta: 90.minutes,
        job: job
      )
    end

    context "job is submitted" do
      let(:job_status) { "Submitted" }

      it 'moves job and issc_request status to accepted' do
        Agero::Rsc::JobApproved::Service.call(
          input: { callback_token: api_access_token.callback_token, event: event }
        )
        job.reload
        issc_dispatch_request.reload
        expect(job.status).to eq "accepted"
        expect(issc_dispatch_request.status).to eq "accepted"
        expect(job.po_number).to eq "NEWPONUMBER"
      end
    end

    context "job is not submitted" do
      let(:job_status) { "En Route" }

      it "updates just the job dispatch details" do
        Agero::Rsc::JobApproved::Service.call(
          input: { callback_token: api_access_token.callback_token, event: event }
        )
        job.reload
        issc_dispatch_request.reload
        expect(job.status).to eq "enroute"
        expect(issc_dispatch_request.status).to eq "new_request"
        expect(job.po_number).to eq "NEWPONUMBER"
      end
    end

    context "job is expired" do
      let(:job_status) { Job::EXPIRED }

      it 'moves job and issc_request status to accepted' do
        Agero::Rsc::JobApproved::Service.call(
          input: { callback_token: api_access_token.callback_token, event: event }
        )
        job.reload
        issc_dispatch_request.reload
        expect(job.status).to eq "accepted"
        expect(issc_dispatch_request.status).to eq "accepted"
        expect(job.po_number).to eq "NEWPONUMBER"
      end
    end
  end

  context "missing job" do
    let!(:issc_location) do
      create(:issc_location, :default, issc: issc, locationid: facility_id)
    end

    before :each do
      allow_any_instance_of(Agero::Rsc::JobApproved::Service).to receive(:dispatch_details).and_return(dispatch_details)
    end

    context "new phone transcript dispatch" do
      let(:dispatch_source) { "LivePhone" }

      it 'moves job.status from submitted to accepted' do
        CreateOrUpdateEstimateForJob.jobs.clear
        job_count = Job.count
        Agero::Rsc::JobApproved::Service.call(
          input: { callback_token: api_access_token.callback_token, event: event }
        )
        expect(Job.count).to eq job_count + 1
        job = Job.last
        expect(job.status).to eq "accepted"
        expect(CreateOrUpdateEstimateForJob.jobs.size).to eq 1
        expect(CreateOrUpdateEstimateForJob).to have_enqueued_sidekiq_job(job.id)
      end

      describe "site" do
        before :each do
          rescue_company.sites.each_with_index { |s, i| s.update!(name: "Site #{i}") }
          rescue_company.sites << create(:site, company: rescue_company, name: "HQ")
        end

        it "sets the site from the issc record site" do
          Agero::Rsc::JobApproved::Service.call(
            input: { callback_token: api_access_token.callback_token, event: event }
          )
          job = Job.last
          expect(job.site).not_to eq nil
          expect(job.site).to eq issc.site
        end

        it "defaults to the rescue company site if the issc site is not set" do
          issc.update!(site_id: nil)
          Agero::Rsc::JobApproved::Service.call(
            input: { callback_token: api_access_token.callback_token, event: event }
          )
          job = Job.last
          expect(job.site).not_to eq nil
          expect(job.site).to eq rescue_company.hq
        end
      end
    end

    context "missing non-phone transcript dispatch" do
      it "raises an error" do
        expect do
          Agero::Rsc::JobApproved::Service.call(
            input: { callback_token: api_access_token.callback_token, event: event }
          )
        end.to raise_error(ArgumentError)
      end
    end
  end
end
