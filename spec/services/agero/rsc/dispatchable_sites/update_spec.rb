# frozen_string_literal: true

require "rails_helper"

describe Agero::Rsc::DispatchableSites::Update do
  before do
    stub_const "CONTRACTORID", "48572387098425"
  end

  subject do
    Agero::Rsc::DispatchableSites::Update.new(rescue_company, agero, site_args).call
  end

  let!(:swoop) { create(:super_company) }
  let!(:agero) { create(:fleet_company, :agero) }
  let!(:rescue_company) { create(:rescue_company, :finish_line) }

  let!(:location) do
    Location.create(address: '123 arcacia')
  end

  let(:issc_location) do
    create :issc_location, {
      company: rescue_company,
      fleet_company: agero,
      locationid: 1,
      site: nil,
      location: location,
    }
  end

  let!(:rsc_issc1) do
    create :isscs, {
      clientid: 'AGO',
      company: rescue_company,
      status: Issc::REGISTERED,
      contractorid: CONTRACTORID,
      system: :rsc,
      issc_location: issc_location,
      site: nil,
    }
  end

  let!(:mapping) do
    SiteFacilityMap.create(company: rescue_company,
                           fleet_company: agero,
                           facility: location)
  end

  let!(:site) do
    create :site, {

    }
  end

  let(:site_args) do
    [
      {
        "site_id": site.id,
        "site_dispatchable": true,
        "enabled": true,
        "locationid": location.id,
      },
    ]
  end

  context 'no rescue provider'
  it 'creates rescue provider' do
    expect { subject }.to change(RescueProvider, :count).from(0).to(1)
    rp = RescueProvider.first
    expect(rp.company).to eql(agero)
    expect(rp.provider).to eql(rescue_company)
    expect(rp.site).to eql(site)
    expect(rp.deleted_at).to be_nil
  end

  context 'non-dispatchable' do
    let(:site_args) do
      [
        {
          "site_id": site.id,
          "site_dispatchable": true,
          "enabled": false,
          "locationid": location.id,
        },
      ]
    end

    it 'creates rescue provider and sets deleted' do
      Timecop.freeze do |time|
        expect { subject }.to change(RescueProvider, :count).from(0).to(1)
        rp = RescueProvider.first
        expect(rp.company).to eql(agero)
        expect(rp.provider).to eql(rescue_company)
        expect(rp.site).to eql(site)
        expect(rp.deleted_at.to_i).to be(time.to_i)
      end
    end
  end

  context 'existing rescue provider enabled' do
    let!(:rescue_provider) do
      RescueProvider.create(company: agero,
                            provider: rescue_company,
                            site: site)
    end
    let(:site_args) do
      [
        {
          "site_id": site.id,
          "site_dispatchable": true,
          "enabled": false,
          "locationid": location.id,
        },
      ]
    end

    it 'updates rescue provider and sets deleted' do
      Timecop.freeze do |time|
        expect { subject }.to avoid_changing(RescueProvider, :count)
        rp = RescueProvider.first
        expect(rp.company).to eql(agero)
        expect(rp.provider).to eql(rescue_company)
        expect(rp.site).to eql(site)
        expect(rp.deleted_at.to_i).to be(time.to_i)
      end
    end
  end

  context 'existing rescue provider disabled' do
    let!(:rescue_provider) do
      RescueProvider.create(company: agero,
                            provider: rescue_company,
                            site: site,
                            deleted_at: Time.now)
    end
    let(:site_args) do
      [
        {
          "site_id": site.id,
          "site_dispatchable": true,
          "enabled": true,
          "locationid": location.id,
        },
      ]
    end

    it 'updates rescue provider and sets enabled' do
      expect { subject }.to avoid_changing(RescueProvider, :count)
      rp = RescueProvider.first
      expect(rp.company).to eql(agero)
      expect(rp.provider).to eql(rescue_company)
      expect(rp.site).to eql(site)
      expect(rp.deleted_at).to be_nil
    end
  end

  it 'associates site to location' do
    expect { subject && mapping.reload }.to change(mapping, :site_id).from(nil).to(site.id)
  end

  it 'associates issc to site' do
    expect { subject && rsc_issc1.reload }.to change(rsc_issc1, :site_id).from(nil).to(site.id)
  end

  context 'multiple locations' do
    let(:location2) do
      Location.create(address: '1015 Folsom')
    end

    let(:issc_location2) do
      create :issc_location, {
        company: rescue_company,
        fleet_company: agero,
        locationid: 2,
        site: nil,
        location: location2,
      }
    end

    let!(:mapping2) do
      SiteFacilityMap.create(company: rescue_company,
                             fleet_company: agero,
                             facility: location2)
    end

    let(:site_args) do
      [
        {
          "site_id": site.id,
          "site_dispatchable": true,
          "enabled": true,
          "locationid": location2.id,
        },
      ]
    end

    before do
      rsc_issc1.issc_location = issc_location2
      rsc_issc1.save!
    end

    it 'associates site to location' do
      expect { subject && mapping2.reload }.to change(mapping2, :site_id).from(nil).to(site.id)
    end

    it 'associates issc to site' do
      expect { subject && rsc_issc1.reload }.to change(rsc_issc1, :site_id).from(nil).to(site.id)
    end
  end

  context 'move location' do
    let!(:location2) do
      Location.create(address: '1015 Folsom')
    end
    let(:issc_location2) do
      create :issc_location, {
        company: rescue_company,
        fleet_company: agero,
        locationid: 2,
        site: site,
        location: location2,
      }
    end
    let!(:rsc_issc2) do
      create :isscs, {
        clientid: 'AGO',
        company: rescue_company,
        status: Issc::REGISTERED,
        contractorid: CONTRACTORID,
        system: :rsc,
        issc_location: issc_location2,
        site: site,
      }
    end

    let!(:mapping2) do
      SiteFacilityMap.create(company: rescue_company,
                             fleet_company: agero,
                             facility: location2,
                             site: site)
    end

    let(:site_args) do
      [
        {
          "site_id": site.id,
          "site_dispatchable": true,
          "enabled": true,
          "locationid": location.id,
        },
      ]
    end

    it 'associates site to location' do
      expect { subject && mapping.reload }.to change(mapping, :site_id).from(nil).to(site.id)
    end

    it 'associates issc to site' do
      expect { subject && rsc_issc1.reload }.to change(rsc_issc1, :site_id).from(nil).to(site.id)
    end

    it 'disassociates site from location2' do
      expect { subject && mapping2.reload }.to change(mapping2, :site_id).from(site.id).to(nil)
    end

    it 'disassociates issc2 from site' do
      expect { subject && rsc_issc2.reload }.to change(rsc_issc2, :site_id).from(site.id).to(nil)
    end
  end
end
