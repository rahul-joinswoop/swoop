# frozen_string_literal: true

require "rails_helper"

describe DepartmentServices::Change do
  subject(:service_call) do
    described_class.new(
      api_company: api_company,
      departments_to_add: departments_to_add,
      department_ids_to_remove: department_ids_to_remove,
      department_to_rename: department_to_rename
    ).call
  end

  context 'when api_company is a RescueCompany' do
    let(:api_company) { create(:rescue_company, :with_departments) }

    let(:departments_to_add) do
      [{ name: 'Maintenance' }, { name: 'Recall' }]
    end

    let(:department_to_remove) { api_company.departments.first }

    let(:department_ids_to_remove) do
      [department_to_remove.id]
    end

    let(:department_to_be_renamed) do
      create(:department, name: 'Temporary Dept Name', company: api_company)
    end

    let(:department_to_rename) do
      [{ id: department_to_be_renamed.id, name: 'Oficial Dept Name' }]
    end

    it 'adds the given departments' do
      service_call

      maintenance_dept = Department.find_by(name: 'Maintenance', company: api_company)
      recall_dept      = Department.find_by(name: 'Recall',      company: api_company)

      expect(api_company.departments.live).to include(maintenance_dept, recall_dept)
    end

    it 'removes the given departments' do
      service_call

      expect(api_company.departments.live).not_to include(department_to_remove)
    end

    it 'renames the given departments' do
      service_call

      renamed_department = api_company.departments.live.find(department_to_be_renamed.id)

      expect(renamed_department.name).to eq('Oficial Dept Name')
    end
  end

  context 'when api_company is a FleetCompany' do
    let(:api_company) { create(:fleet_company, :with_departments) }

    let(:departments_to_add) do
      [{ name: 'Maintenance' }, { name: 'Recall' }]
    end

    let(:department_to_remove) { api_company.departments.first }

    let(:department_ids_to_remove) do
      [department_to_remove.id]
    end

    let(:department_to_be_renamed) do
      create(:department, name: 'Temporary Dept Name', company: api_company)
    end

    let(:department_to_rename) do
      [{ id: department_to_be_renamed.id, name: 'Oficial Dept Name' }]
    end

    it 'adds the given departments' do
      service_call

      maintenance_dept = Department.find_by(name: 'Maintenance', company: api_company)
      recall_dept      = Department.find_by(name: 'Recall',      company: api_company)

      expect(api_company.departments.live).to include(maintenance_dept, recall_dept)
    end

    it 'removes the given departments' do
      service_call

      expect(api_company.departments.live).not_to include(department_to_remove)
    end

    it 'renames the given departments' do
      service_call

      renamed_department = api_company.departments.live.find(department_to_be_renamed.id)

      expect(renamed_department.name).to eq('Oficial Dept Name')
    end
  end

  context 'when api_company is a SuperCompany' do
    let(:api_company) { create :super_company, :swoop }

    let(:departments_to_add) do
      [{ name: 'Maintenance' }, { name: 'Recall' }]
    end

    let(:department_ids_to_remove) { [] }

    let(:department_to_rename) { [] }

    it 'has none departments' do
      expect(api_company.departments).to eq([])
    end

    it 'doesnt change Swoop departments collection' do
      service_call

      expect(api_company.departments).to eq([])
    end
  end
end
