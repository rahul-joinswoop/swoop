# frozen_string_literal: true

require 'rails_helper'

describe IsscService do
  context "an geico account exists for a company" do
    let!(:account) { create :account, fleet: :gcoapi }

    let!(:issc) do
      build :gco_issc, {
        site: account.company.sites[0],
        company: account.company,
        contractorid: 'CA123456',
      }
    end

    it "changes the status from registering to registered" do
      issc.status = "registering"
      issc.save!

      event = {
        Event: "register",
        ClientID: "GCO",
        ContractorID: "CA123456",
        Token: "0000-0000-0000-0000-0000",
        Result: "Success",
      }
      issc = IsscProviderRegistered.new(event).call
      expect(issc).not_to be_nil
      expect(issc.status).to eq('registered')
      expect(issc.token).to eq(event[:Token])
    end

    it "changes the status from logging_in to logged_in" do
      issc.status = "logging_in"
      issc.save!

      event = {
        Event: "login",
        ClientID: "GCO",
        ContractorID: "CA123456",
        Result: "Success",
      }
      issc = IsscProviderLoggedIn.new(event).call
      expect(issc).not_to be_nil
      expect(issc.status).to eq('logged_in')
      expect(issc.token).to eq(event[:Token])
      expect(issc.logged_in_at).not_to be_nil
    end

    it "changes the status from logging_out to registered" do
      issc.status = "logging_out"
      issc.save!

      event = {
        Event: "logout",
        ClientID: "GCO",
        ContractorID: "CA123456",
        Result: "Success",
      }
      issc = IsscProviderLoggedOut.new(event).call
      expect(issc).not_to be_nil
      expect(issc.status).to eq('registered')
      expect(issc.token).to eq(event[:Token])
      expect(issc.logged_in_at).to be_nil
    end

    it "changes thes status from deregistering to unregistered" do
      issc.status = "deregistering"
      issc.token = "0000-0000-0000-0000-0000"
      issc.save!

      event = {
        Event: "deregister",
        ClientID: "GCO",
        ContractorID: "CA123456",
        Result: "Success",
      }
      issc = IsscProviderDeregistered.new(event).call
      expect(issc).not_to be_nil
      expect(issc.status).to eq('unregistered')
      expect(issc.token).to be_nil
    end

    it "changes thes status from deregistering and pending delete to unregistered and deleted" do
      issc.status = "deregistering"
      issc.token = "0000-0000-0000-0000-0000"
      issc.delete_pending_at = Time.now
      issc.save!

      event = {
        Event: "deregister",
        ClientID: "GCO",
        ContractorID: "CA123456",
        Result: "Success",
      }
      issc = IsscProviderDeregistered.new(event).call
      expect(issc).not_to be_nil
      expect(issc.status).to eq('unregistered')
      expect(issc.deleted_at).not_to be_nil
      expect(issc.token).to be_nil
    end
  end
end
