# frozen_string_literal: true

require "rails_helper"

describe DispatchJob do
  subject do
    DispatchJob.new(job).call
    job.save!
  end

  let(:vin) { "123456" }
  let(:vehicle) { build :vehicle, vin: vin }

  shared_examples "calling dispatch" do
    it "is valid" do
      expect(job).to be_valid
      expect(job.stranded_vehicle).to be_valid
      expect(job.stranded_vehicle.vin).to eq vin
    end

    it "updates and logs status change to pending via created" do
      expect(job.dispatched_at).to be_nil
      expect(job.status).to eq(status)
      expect { subject }.to(change(job, :status).to('pending')) &&
        change(job.audit_job_statuses, :count).by(2)
      # i don't know why we have to reload here when we've already done it above?
      expect(job.audit_job_statuses.reload.map(&:job_status).map(&:name).sort)
        .to eq(['Created', 'Pending'])
    end
  end

  shared_examples "dispatch" do
    context "from predraft" do
      let(:status) { "predraft" }

      it_behaves_like "calling dispatch"
    end

    context "from draft" do
      let(:status) { "draft" }

      it_behaves_like "calling dispatch"

      context "without a service_code" do
        let(:job) do
          j = super()
          j.update! service_code: nil
          j
        end

        it "raises an error" do
          expect { subject }.to raise_error(ArgumentError, "Job submitted with no service_code")
        end
      end
    end
  end

  context "With FleetManagedJob" do
    let(:job) { create :fleet_managed_job, stranded_vehicle: vehicle, status: status }

    it_behaves_like "dispatch"
  end

  context "With FleetInHouseJob" do
    let(:job) { create :fleet_in_house_job, stranded_vehicle: vehicle, status: status }

    it_behaves_like "dispatch"
  end

  context "with RescueJob" do
    let(:job) { create :rescue_job, stranded_vehicle: vehicle, status: status }

    it_behaves_like "dispatch"
  end
end
