# frozen_string_literal: true

require "rails_helper"

describe Territories::CreateTerritoriesForCompany do
  let!(:user) do
    u = create :user
    u.add_role :root
  end
  let!(:rescue_company) { create :rescue_company, location: (create :location, lat: 42.418560, lng: -71.106453) }

  let(:service_object) { Territories::CreateTerritoriesForCompany.new }

  subject { service_object.call(rescue_company) }

  context 'no territories should be present ' do
    it 'before the rescue company is created' do
      expect(Territory.count).to eq(0)
    end
  end

  context 'creating new territory' do
    it 'changes the territory count' do
      expect do
        subject
      end.to change(Territory, :count).by(1)
    end

    it 'for the rescue company' do
      expect do
        subject
      end.to change { rescue_company.territories.count }.by(1)
    end
  end
end
