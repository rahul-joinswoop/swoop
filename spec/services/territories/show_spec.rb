# frozen_string_literal: true

require 'rails_helper'

describe Territories::Show do
  let(:rescue_company) { create(:rescue_company) }
  let(:user) { create(:user, :rescue, company: rescue_company) }

  let!(:first_territory_for_rescue_company) do
    create(:territory, company: rescue_company)
  end

  let!(:second_territory_for_rescue_company) do
    create(:territory, company: rescue_company)
  end

  let!(:territory_for_other_rescue_company) do
    create(:territory, company: create(:rescue_company))
  end

  let(:company_id) { rescue_company.id }

  subject(:service_object_result) do
    Territories::Show.call user: user,
                           territory_id: territory_id
  end

  context "Positive Cases" do
    context "When required territory exists" do
      let(:territory_id) { first_territory_for_rescue_company.id }

      it "returns the required territory for the company" do
        expect { service_object_result }.not_to raise_error
        expect(service_object_result).to be_success
        expect(service_object_result.territory).to eq(first_territory_for_rescue_company)
      end
    end
  end

  context "Negative Cases" do
    context "When required territory doesn't exists" do
      let(:territory_id) { -1 }

      it "raises ActiveRecord::RecordNotFound" do
        expect { service_object_result }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context "Unauthorized Call" do
      let(:territory_id) { first_territory_for_rescue_company.id }

      context 'Wrong company type' do
        let(:fleet_company) { create(:fleet_company, :tesla) }
        let(:company_id) { fleet_company.id }
        let(:user) { create(:user, :fleet_demo, company: fleet_company) }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end

      context "Rescue company trying to get other's territory" do
        let(:territory_id) { territory_for_other_rescue_company.id }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end
    end
  end
end
