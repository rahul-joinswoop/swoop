# frozen_string_literal: true

require "rails_helper"

describe Territories::Create do
  let(:rescue_company) { create(:rescue_company) }
  let(:user) { create(:user, :rescue, company: rescue_company) }
  let(:company_id) { rescue_company.id }

  let(:params)  do
    {
      territory: {
        paths: [
          {
            lat: -10.5,
            lng: 10.5,
          },
          {
            lat: 10.5,
            lng: 10.5,
          },
          {
            lat: 10.5,
            lng: -10.5,
          },
          {
            lat: -10.5,
            lng: -10.5,
          },
        ],
      },
    }
  end

  subject(:service_object_result) do
    Territories::Create.call(user: user,
                             company_id: company_id,
                             polygon: params[:territory][:paths])
  end

  context 'negative cases' do
    context 'entities are not found' do
      context 'company not found' do
        let(:company_id) { -1 }

        it "raises ActiveRecord::RecordNotFound" do
          expect { service_object_result }.to raise_error(ActiveRecord::RecordNotFound)
        end
      end
    end

    context 'unauthorized' do
      context 'calling with a rescue company which is not authorized' do
        let(:unauthorized_rescue_company) { create :rescue_company }
        let(:user) { create :user, :rescue, company: unauthorized_rescue_company }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end

      context 'calling with a fleet company which is not authorized' do
        let(:unauthorized_fleet_company) { create :fleet_company }
        let(:user) { create :user, :fleet_demo, company: unauthorized_fleet_company }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end
    end
  end

  context 'positive cases' do
    it "succeeds" do
      expect(service_object_result).to be_success
      expect { service_object_result }.not_to raise_error
    end

    it "creates creates only one territory belonging to the company" do
      expect do
        expect(service_object_result.territory).to be_a(Territory)
      end.to change { rescue_company.territories.count }.by(1)
        .and change(Territory, :count).by(1)
    end

    it "creates the audit records" do
      expect { service_object_result }.to change(TerritoryAuditTrail, :count).by(1)

      expect(TerritoryAuditTrail.last.territory.coordinates).to eq(
        [
          { lat: -10.5, lng: 10.5 },
          { lat: 10.5, lng: 10.5 },
          { lat: 10.5, lng: -10.5 },
          { lat: -10.5, lng: -10.5 },
          { lat: -10.5, lng: 10.5 },
        ]
      )
      expect(TerritoryAuditTrail.last.user).to eq(user)
      expect(TerritoryAuditTrail.last.action).to eq('created')
      expect(TerritoryAuditTrail.last.territory_id).to eq(Territory.last.id)
    end
  end
end
