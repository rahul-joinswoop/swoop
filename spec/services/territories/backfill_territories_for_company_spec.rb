# frozen_string_literal: true

require "rails_helper"

describe Territories::BackfillTerritoriesForCompany do
  let!(:user) do
    u = create :user
    u.add_role :root
  end
  let!(:rescue_company) { create :rescue_company }
  let!(:site_1) { create :site, company: rescue_company, location: (create :location, lat: 1.2, lng: 1.2) }
  let!(:site_2) { create :site, company: rescue_company, location: (create :location, lat: 1.4, lng: 1.5) }
  let!(:site_3) { create :site, company: rescue_company, location: (create :location, lat: 1.6, lng: 1.7) }
  let!(:destroy) { Site.find(site_3.id).destroy }
  let!(:rescue_provider) do
    create :rescue_provider,
           company: rescue_company,
           site: site_2,
           auto_assign_distance: 7
  end
  let(:rescue_company_id) { rescue_company.id }

  let(:service_object) { Territories::BackfillTerritoriesForCompany.new }

  subject { service_object.call(rescue_company_id, simulation: false) }

  context 'negative cases' do
    context 'non existing company' do
      let(:rescue_company_id) { -1 }

      it 'returns false' do
        expect(subject).to be_falsey
      end

      it 'does not create any territories' do
        expect do
          subject
        end.not_to change(Territory, :count)
      end
    end

    context 'The Territory creation fails' do
      before(:each) do
        allow(Territory).to receive(:circleoid_coordinates).and_return(
          'POLYGON ((-10.5 -10.5, 10.5 -10.5, 10.5 10.5, -10.5 10.5))',
          'INVALID POLYGON SYNTAX'
        )
      end

      it 'returns false' do
        expect(subject).to be_falsey
      end

      context 'ACID-ification' do
        it 'does not create any territories' do
          expect do
            subject
          end.not_to change(Territory, :count)
        end
      end
    end
  end

  context 'positive cases' do
    context 'some territories already exist for the company' do
      let!(:already_existing_territory) { create :territory, company: rescue_company, polygon_area: 'POLYGON ((40 40, 40 50, 50 50, 50 40))' }

      it 'returns false' do
        expect(subject).to be_falsey
      end

      it 'the territories are kept' do
        subject
        expect(Territory.exists?(already_existing_territory.id)).to be_truthy
      end

      it 'no new territories are created' do
        expect do
          subject
        end.not_to change(Territory, :count)
      end
    end

    context 'there are no territories yet for the company' do
      it 'creates the right territories' do
        subject
        expect(rescue_company.territories.map { |x| x.coordinates }).to include(
          [
            { lng: 1.2, lat: 1.344927536231884 },
            { lng: 1.2909090909090908, lat: 1.3255109280847013 },
            { lng: 1.3574591643244434, lat: 1.272463768115942 },
            { lng: 1.3818181818181818, lat: 1.2 },
            { lng: 1.3574591643244434, lat: 1.127536231884058 },
            { lng: 1.290909090909091, lat: 1.0744890719152986 },
            { lng: 1.2, lat: 1.055072463768116 },
            { lng: 1.1090909090909091, lat: 1.0744890719152986 },
            { lng: 1.0425408356755566, lat: 1.1275362318840578 },
            { lng: 1.018181818181818, lat: 1.2 },
            { lng: 1.0425408356755566, lat: 1.2724637681159419 },
            { lng: 1.109090909090909, lat: 1.325510928084701 },
            { lng: 1.2, lat: 1.344927536231884 },
          ]
        )

        expect(rescue_company.territories.map { |x| x.coordinates }).to include(
          [
            { lng: 1.5, lat: 1.5014492753623188 },
            { lng: 1.5636363636363637, lat: 1.4878576496592908 },
            { lng: 1.6102214150271104, lat: 1.4507246376811593 },
            { lng: 1.6272727272727272, lat: 1.4 },
            { lng: 1.6102214150271104, lat: 1.3492753623188405 },
            { lng: 1.5636363636363637, lat: 1.312142350340709 },
            { lng: 1.5, lat: 1.298550724637681 },
            { lng: 1.4363636363636365, lat: 1.312142350340709 },
            { lng: 1.3897785849728896, lat: 1.3492753623188405 },
            { lng: 1.3727272727272728, lat: 1.4 },
            { lng: 1.3897785849728896, lat: 1.4507246376811593 },
            { lng: 1.4363636363636363, lat: 1.4878576496592908 },
            { lng: 1.5, lat: 1.5014492753623188 },
          ]
        )
      end

      it 'creates two territories for the company' do
        expect do
          subject
        end.to change { rescue_company.territories.count }.by(2)
      end

      context 'called with the simulation flag' do
        subject { service_object.call(rescue_company_id, simulation: true) }

        it 'does not create any territories for the company' do
          expect do
            subject
          end.not_to change { rescue_company.territories.count }
        end
      end
    end

    context 'territories for other companies' do
      let!(:other_rescue_company) { create :rescue_company }
      let!(:site_for_other_company) { create :site, company: other_rescue_company }

      it 'are not altered' do
        expect do
          subject
        end.not_to change { other_rescue_company.territories.count }
      end
    end
  end
end
