# frozen_string_literal: true

require 'rails_helper'

describe Territories::Index do
  let(:rescue_company) { create(:rescue_company) }
  let(:user) { create(:user, :rescue, company: rescue_company) }

  let!(:first_territory_for_rescue_company) do
    create(:territory, company: rescue_company)
  end

  let!(:second_territory_for_rescue_company) do
    create(:territory, company: rescue_company)
  end

  let!(:territory_for_other_rescue_company) do
    create(:territory, company: create(:rescue_company))
  end

  subject(:service_object_result) do
    Territories::Index.call user: user,
                            company_id: company_id
  end

  context "Positive Cases" do
    context "When using authorized rescue company" do
      let(:company_id) { rescue_company.id }

      context "When rescue company has territories" do
        context "Responds with all territories" do
          it "returns the required territory for the company" do
            expect { service_object_result }.not_to raise_error
            expect(service_object_result).to be_success
            expect(service_object_result.territories).to include(first_territory_for_rescue_company)
            expect(service_object_result.territories).to include(second_territory_for_rescue_company)
          end
        end

        it "does not include soft-deleted territories" do
          second_territory_for_rescue_company.update!(deleted_at: Time.now)

          expect(service_object_result.territories.count).to eq(1)
          expect(service_object_result.territories.first.deleted_at).to be_nil
          expect(service_object_result.territories).not_to include(second_territory_for_rescue_company)
        end
      end

      context "When the company doesn't have any territory" do
        let(:another_rescue_company) { create(:rescue_company) }
        let(:company_id) { another_rescue_company.id }
        let(:user) { create(:user, :rescue, company: another_rescue_company) }

        it 'returns an empty array' do
          expect(service_object_result).to be_success
          expect(service_object_result.territories.count).to eq(0)
        end
      end
    end
  end

  context "Negative Cases" do
    context "When using unauthorized company" do
      let(:unauthorized_rescue_company) { create(:rescue_company) }
      let(:company_id) { unauthorized_rescue_company.id }

      it "raises SecurityError" do
        expect { service_object_result }.to raise_error(SecurityError)
      end
    end
  end
end
