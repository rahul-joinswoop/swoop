# frozen_string_literal: true

require "rails_helper"

describe Territories::DestroyAll do
  let(:rescue_company) { create(:rescue_company) }
  let(:user) { create(:user, :rescue, company: rescue_company) }

  let(:other_rescue_company) { create :rescue_company }
  let!(:territory_belonging_to_the_other_rescue_company) do
    create(:territory, company: other_rescue_company)
  end
  let!(:territory) do
    create(:territory, company: rescue_company)
  end
  let(:company_id) { rescue_company.id }

  subject(:service_object_result) do
    Territories::DestroyAll.call(user: user,
                                 company_id: company_id)
  end

  context 'negative cases' do
    context 'entities are not found' do
      context 'company not found' do
        let(:company_id) { -1 }

        it "raises ActiveRecord::RecordNotFound" do
          expect { service_object_result }.to raise_error(ActiveRecord::RecordNotFound)
        end
      end
    end

    context 'unauthorized' do
      context 'calling with a rescue company which is not authorized' do
        let(:unauthorized_rescue_company) { create :rescue_company }
        let(:company_id) { unauthorized_rescue_company.id }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end

      context 'calling with a fleet company which is not authorized' do
        let(:unauthorized_fleet_company) { create :fleet_company }
        let(:user) { create :user, :fleet_demo, company: unauthorized_fleet_company }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end
    end
  end

  context 'positive cases' do
    it "succeeds" do
      expect(service_object_result).to be_success
      expect { service_object_result }.not_to raise_error
    end

    it "soft deletes the territories" do
      expect { service_object_result }.to change { rescue_company.territories.count }.by(-1)

      expect(rescue_company.territories.count).to eq(0)
    end

    it "doesn't hard delete the territories" do
      expect { service_object_result }.to change { rescue_company.territories.unscoped.count }.by(0)
    end

    it "doesn't delete the territories belonging to other companies" do
      expect { service_object_result }.to change { other_rescue_company.territories.count }.by(0)
    end

    it "creates the audit records" do
      expect { service_object_result }.to change(TerritoryAuditTrail, :count).by(1)

      expect(TerritoryAuditTrail.last.territory_id).to eq(territory.id)
      expect(TerritoryAuditTrail.last.user).to eq(user)
      expect(TerritoryAuditTrail.last.action).to eq('destroyed')
    end
  end
end
