# frozen_string_literal: true

require "rails_helper"

describe Territories::Destroy do
  let(:rescue_company) { create(:rescue_company) }
  let(:user) { create(:user, :rescue, company: rescue_company) }

  let(:other_rescue_company) { create :rescue_company }
  let!(:territory_belonging_to_the_other_rescue_company) do
    create(:territory, company: other_rescue_company)
  end
  let!(:territory) do
    create(:territory, company: rescue_company)
  end
  let(:company_id) { rescue_company.id }

  let!(:territory) { create(:territory, company: rescue_company) }
  let(:company_id) { rescue_company.id }
  let(:territory_id) { territory.id }

  subject(:service_object_result) do
    Territories::Destroy.call user: user,
                              territory_id: territory_id
  end

  context 'negative cases' do
    context 'entities are not found' do
      context 'territory not found' do
        let(:territory_id) { -1 }

        it "raises ActiveRecord::RecordNotFound" do
          expect { service_object_result }.to raise_error(ActiveRecord::RecordNotFound)
        end
      end
    end

    context 'unauthorized' do
      context 'calling with a user who belongs to a company which is not authorized' do
        let(:unauthorized_rescue_company) { create :rescue_company }
        let(:user) { create(:user, :rescue, company: unauthorized_rescue_company) }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end

      context 'calling with a fleet company which is not authorized' do
        let(:unauthorized_fleet_company) { create :fleet_company }
        let(:user) { create :user, :fleet_demo, company: unauthorized_fleet_company }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end

      context 'calling with a territory which belongs to other rescue company' do
        let(:another_rescue_company) { create :rescue_company }
        let(:territory_belonging_to_another_rescue_company) { create(:territory, company: another_rescue_company) }
        let(:territory_id) { territory_belonging_to_another_rescue_company.id }

        it "raises SecurityError" do
          expect { service_object_result }.to raise_error(SecurityError)
        end
      end
    end
  end

  context 'positive cases' do
    it "succeeds" do
      expect(service_object_result).to be_success
      expect { service_object_result }.not_to raise_error
    end

    it "soft deletes the territory" do
      expect { service_object_result }.to change { rescue_company.territories.count }.by(-1)
    end

    it "doesn't hard delete the territory" do
      expect { service_object_result }.to change { rescue_company.territories.unscoped.count }.by(0)
    end

    it "creates the audit records" do
      expect { service_object_result }.to change(TerritoryAuditTrail, :count).by(1)

      expect(TerritoryAuditTrail.last.territory_id).to eq(territory.id)
      expect(TerritoryAuditTrail.last.user).to eq(user)
      expect(TerritoryAuditTrail.last.action).to eq('destroyed')
    end
  end
end
