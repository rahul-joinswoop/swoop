# frozen_string_literal: true

require "rails_helper"

describe RestoreCompanySwoopAccounts do
  subject { described_class.call(company: company) }

  let!(:company) { create(:company) }

  let(:swoop_account) { create(:account, name: "Swoop") }
  let(:deleted_swoop_account) do
    create(:account, name: "Swoop", deleted_at: Time.zone.now)
  end

  context "when there are no deleted swoop accounts" do
    before do
      company.accounts << swoop_account
    end

    it "does nothing" do
      subject

      expect { swoop_account.reload }.to avoid_changing(
        swoop_account, :deleted_at
      )
    end
  end

  context "when company only has 1 deleted swoop account" do
    before do
      company.accounts << deleted_swoop_account
    end

    it "restores deleted swoop account" do
      subject

      expect { deleted_swoop_account.reload }.to change(
        deleted_swoop_account, :deleted_at
      ).to(nil)
    end
  end

  context "when company has multiple deleted swoop accounts" do
    before do
      company.accounts << swoop_account
      company.accounts << deleted_swoop_account
    end

    it "restores the deleted swoop account" do
      subject

      expect { deleted_swoop_account.reload }.to change(
        deleted_swoop_account, :deleted_at
      ).to(nil)
    end

    it "deactivates the older swoop account" do
      subject

      expect { swoop_account.reload }.to change(
        swoop_account, :deleted_at
      ).from(nil)
    end

    context "and newly-deactivated swoop account has jobs" do
      before do
        swoop_account.jobs << create_list(:job, 3, account: swoop_account)
      end

      it "migrates them to the newly restored swoop account" do
        expect(deleted_swoop_account.jobs).to be_empty

        subject

        expect(deleted_swoop_account.jobs.length).to eq(3)
      end
    end
  end
end
