# frozen_string_literal: true

require "rails_helper"

describe GenerateJobHistory do
  subject do
    described_class.call(
      job: job,
      viewing_company: viewing_company
    ).results
  end

  let(:job) { create(:job) }
  let(:viewing_company) { build_stubbed(:company) }
  let(:history_names) do
    subject.map { |k, v| [v.try(:name), v.try(:status_name)].compact.first }
  end

  context "when history items and AJS exist" do
    let!(:job_status) { create(:job_status, :completed) }
    let!(:history_item1) { create(:history_item, job: job, title: "Canceled") }
    let!(:history_item2) { create(:history_item, job: job, title: "Adjusted") }
    let!(:audit_job_status) do
      create(:audit_job_status, job_status: job_status, job: job)
    end

    it "includes all items" do
      expect(history_names).to eq(["Completed", "Canceled", "Adjusted"])
    end
  end

  context "when there are duplicate, repeatable history items" do
    let!(:history_item1) { create(:history_item, job: job, title: HistoryItem::INVOICE_EDITED) }
    let!(:history_item2) { create(:history_item, job: job, title: HistoryItem::INVOICE_EDITED) }
    let!(:history_item3) { create(:history_item, job: job, title: HistoryItem::SERVICE_EDITED) }

    it "includes all history items" do
      expect(history_names).to eq([HistoryItem::INVOICE_EDITED, HistoryItem::INVOICE_EDITED, HistoryItem::SERVICE_EDITED])
    end
  end
end
