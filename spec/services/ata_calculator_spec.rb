# frozen_string_literal: true

require "rails_helper"

describe AtaCalculator do
  subject(:calculator) { AtaCalculator.new(job.id) }

  let(:job) { build_stubbed(:job) }
  let(:oldest_parent) { build_stubbed(:job) }
  let(:eta) { [] }
  let(:onsite_status) { [] }

  context '#call' do
    before do
      allow(calculator)
        .to receive(:oldest_parent_or_job)
        .and_return(oldest_parent)
    end

    context 'without a job nor a parent' do
      describe 'calculating the ATA' do
        let(:oldest_parent) { Job.none }

        it 'returns positive value in minutes' do
          expect(calculator.call).to be(nil)
        end
      end
    end

    context 'without an eta nor an onsite status' do
      let(:eta) { [] }
      let(:onsite_status) { [] }

      describe 'calculating the ATA' do
        it 'returns nil' do
          expect(calculator)
            .to receive(:earliest_eta)
            .and_return(eta)

          expect(calculator.call).to eq(nil)
        end
      end
    end

    context 'without an eta' do
      let(:eta) { [] }
      let(:onsite_status) { build_stubbed(:job_status) }

      describe 'calculating the ATA' do
        it 'returns nil' do
          expect(calculator)
            .to receive(:earliest_eta)
            .and_return(eta)

          expect(calculator.call).to eq(nil)
        end
      end
    end

    context 'without an onsite status' do
      let(:eta) { build_stubbed(:time_of_arrival) }
      let(:onsite_status) { [] }

      describe 'calculating the ATA' do
        it 'returns nil' do
          expect(calculator)
            .to receive(:earliest_eta)
            .and_return(eta)

          expect(calculator)
            .to receive(:most_recent_onsite)
            .and_return(onsite_status)

          expect(calculator.call).to eq(nil)
        end
      end
    end

    context 'with a job or parent job' do
      let(:oldest_parent_or_job) { build_stubbed(:job) }

      context 'with an eta and an onsite status' do
        let(:eta) do
          build_stubbed(
            :time_of_arrival,
            eba_type: TimeOfArrival::BID,
            created_at: eta_created_at_dttm
          )
        end

        let(:onsite_status) do
          build_stubbed(
            :audit_job_status,
            job_status_id: JobStatus::ON_SITE,
            last_set_dttm: onsite_status_dttm
          )
        end

        context 'with an onsite dttm > eta creation date' do
          let(:eta_created_at_dttm) { 2.days.ago }
          let(:onsite_status_dttm) { 1.day.ago }

          describe 'calculating the ATA' do
            it 'returns positive value in minutes' do
              expect(calculator)
                .to receive(:earliest_eta)
                .and_return(eta)

              expect(calculator)
                .to receive(:most_recent_onsite)
                .and_return(onsite_status)

              expect(calculator.call).to be > 0
            end
          end
        end

        context 'with an onsite dttm < eta creation date' do
          let(:eta_created_at_dttm) { 1.day.ago }
          let(:onsite_status_dttm) { 2.days.ago }

          describe 'calculating the ATA' do
            it 'returns a negative value in minutes' do
              expect(calculator)
                .to receive(:earliest_eta)
                .and_return(eta)

              expect(calculator)
                .to receive(:most_recent_onsite)
                .and_return(onsite_status)

              expect(calculator.call).to be < 0
            end
          end
        end
      end
    end
  end
end
