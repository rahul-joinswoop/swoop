# frozen_string_literal: true

require "rails_helper"

describe FleetUpdateJobState do
  subject { described_class.new(job, status).execute }

  let(:job) { create(:fleet_in_house_job, :completed, :with_invoice) }

  context "when deleting" do
    let(:status) { "Deleted" }

    it "deletes job" do
      expect { subject && job.reload }.to change(job, :deleted_at).from(nil)
    end

    it "deletes invoice" do
      expect { subject && job.reload }.to change(job.invoice, :deleted_at).from(nil)
    end
  end

  context "when GOA" do
    let(:status) { "GOA" }

    it "updates the invoice" do
      expect(job).to receive(:mark_invoice_goa)
      subject
    end
  end

  context 'and new job.status is Completed' do
    include_context "job completed shared examples"

    subject do
      FleetUpdateJobState.new(
        job,
        new_status,
        api_user: api_user,
        api_application: api_application,
        user_agent_hash: user_agent_hash,
        referer: referer
      ).call

      job.save!

      job.reload
    end

    let(:job) { create(:fleet_in_house_job, :with_invoice, status: 'dispatched', fleet_manual: true) }
    let(:new_status) { 'Completed' }
    let(:api_user) { nil }
    let(:api_application) { nil }
    let(:user_agent_hash) do
      {
        platform: 'android',
        version: '1.2.3',
      }
    end
    let(:referer) { 'http://example.com' }

    context 'when api_user is passed' do
      let(:api_user) { create(:user) }

      it_behaves_like 'sending Analytics Job Completed event', 'android', '1.2.3', 'http://example.com'
    end

    context 'when api_application is passed' do
      let(:api_application) do
        double(:api_application, owner_id: company.id, owner_type: 'Company')
      end
      let(:company) { create(:rescue_company) }

      it_behaves_like 'sending Analytics Job Completed event', 'android', '1.2.3', 'http://example.com'
    end

    it 'sets job.status to completed' do
      subject

      expect(job.status).to eq Job::STATUS_COMPLETED
    end
  end

  describe 'job.last_touched_by' do
    include_context "job status change shared examples"

    subject do
      FleetUpdateJobState.new(job, "Dispatched", api_user: api_user).execute
    end

    let(:job) { create(:fleet_in_house_job, status: 'pending', fleet_manual: true) }

    it_behaves_like 'updates job.last_touched_by when api_user is passed'
  end
end
