# frozen_string_literal: true

require "rails_helper"

describe CreateAccountForJobService do
  subject { CreateAccountForJobService.new(job, fleet_company).call }

  let(:fleet_company) { create(:fleet_company) }
  let(:job) { create(:fleet_managed_job, fleet_company: fleet_company) }

  context "when job has no account" do
    it "has job w/o account" do
      expect(job.account).to be_nil
    end

    it "creates account when executed call" do
      # result normally is assigned to job
      expect(subject).to be_a(Account)
    end

    it "has same name as fleet_company" do
      # result normally is assigned to job
      expect(subject.name).to eq(fleet_company.name)
    end
  end

  context "when job already has account" do
    let(:account) { CreateAccountForJobService.new(job, fleet_company).call }
    let(:job) do
      j = super()
      j.account = account
      j.save!
      j
    end

    it "get already existing account when executed call" do
      expect(subject).to be_a(Account)
      expect(subject).to eq(account)
      expect(subject).to eq(job.account)
    end
  end

  context "when missing fleet_company" do
    subject { CreateAccountForJobService.new(job, nil).call }

    it "raise ArgumentError" do
      expect { subject }.to raise_error(ArgumentError)
    end
  end

  context "when missing job" do
    subject { CreateAccountForJobService.new(nil, fleet_company).call }

    it "raise ArgumentError" do
      expect { subject }.to raise_error(ArgumentError)
    end
  end
end
