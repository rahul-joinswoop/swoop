# frozen_string_literal: true

require 'rails_helper'

describe SiteService::Nearest, vcr: true do
  subject do
    SiteService::Nearest.new(
      FleetSite,
      origin,
      bounding_box_max_distance,
      location_type_names,
      company,
      job_id
    ).call
  end

  let(:bounding_box_max_distance) { 75 }

  let(:location_type_names) { ['Dealership'] }

  let(:company) { create :fleet_company, :lincoln }

  let(:job_id) { 999 }

  let!(:advance_auto_site) { create :fleet_site, :advance_auto_site, company: company }
  let!(:fell_street_site) { create :fleet_site, :fell_street_site, company: company }
  let!(:sunrise_auto_site) { create :fleet_site, :sunrise_auto_site, company: company }
  let!(:dr_auto_site) { create :fleet_site, :dr_auto_site, company: company }

  shared_examples 'return the expected sites' do
    context 'origin is on top of a mountain' do
      # Diablo
      let(:origin) { Location.new(lat: 37.85074341422124, lng: -121.7759878474) }

      it "returns crow flies" do
        ret = subject

        expect(ret.length).to eq(3)

        expect(ret[0]).to eq(advance_auto_site)
        expect(ret[0].distance_in_miles.round(1)).to eq(35.0)

        expect(ret[1]).to eq(sunrise_auto_site)
        expect(ret[1].distance_in_miles.round(1)).to eq(35.3)

        expect(ret[2]).to eq(fell_street_site)
        expect(ret[2].distance_in_miles.round(1)).to eq(35.7)
      end
    end

    context 'origin is downtown' do
      let(:origin) { create(:location, lat: 37.7830241, lng: -122.4249327) }

      it "returns miles" do
        ret = subject

        # expect(ret[0]).to eq(advance_auto_site)
        # expect(ret[0].distance_in_miles.round(1)).to eq(35.0)

        # expect(ret[1]).to eq(sunrise_auto_site)
        # expect(ret[1].distance_in_miles.round(1)).to eq(35.3)
        expect(ret.length).to eq(3)
        expect(ret[0]).to eq(fell_street_site)
        expect(ret[0].distance_in_miles.round(1)).to eq(0.5)

        expect(ret[1]).to eq(advance_auto_site)
        expect(ret[1].distance_in_miles.round(1)).to eq(0.7)

        expect(ret[2]).to eq(sunrise_auto_site)
        expect(ret[2].distance_in_miles.round(1)).to eq(4.1)
      end
    end
  end

  it_behaves_like 'return the expected sites'

  context 'when location_type_names is empty' do
    let(:location_type_names) { [] }

    it_behaves_like 'return the expected sites'

    context 'and there is a site without location type' do
      let!(:daly_city_wo_location_type) do
        site = create :fleet_site, :advance_auto_site, name: 'Daly City w/o Location Type', company: company
        site.location.update! location_type: nil, lat: 37.6786812, lng: -122.4704281

        site
      end

      context 'origin is downtown' do
        let(:origin) { create(:location, lat: 37.7830241, lng: -122.4249327) }

        it 'adds the site without location type in the result' do
          result = subject

          expect(result.length).to eq(4)
          expect(result[0]).to eq(fell_street_site)
          expect(result[0].distance_in_miles.round(1)).to eq(0.5)

          expect(result[1]).to eq(advance_auto_site)
          # expect(result[1].distance_in_miles.round(1)).to eq(1.5)
          expect(result[1].distance_in_miles.round(1)).to eq(0.7)

          expect(result[2]).to eq(sunrise_auto_site)
          expect(result[2].distance_in_miles.round(1)).to eq(4.1)

          expect(result[3]).to eq(daly_city_wo_location_type)
          expect(result[3].distance_in_miles.round(1)).to eq(7.6)
        end
      end
    end
  end
end
