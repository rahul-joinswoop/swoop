# frozen_string_literal: true

require 'rails_helper'

describe SiteService::Create do
  subject(:create_site) do
    SiteService::Create.new(
      site_params: {
        name: "Site 1",
        location_attributes: {
          address: "1137 Wisconsin Ave, Oak Park, IL",
        },
        dispatchable: dispatchable,
      },
      site_type: PartnerSite,
      company: partner,
    ).call.site
  end

  let!(:swoop) { create(:super_company, :swoop) }
  let(:partner) { create(:rescue_company) }
  let(:dispatchable) { nil }

  it "creates a site and location" do
    site = create_site

    expect(site.name).to eq "Site 1"
    expect(site.location.address).to eq "1137 Wisconsin Ave, Oak Park, IL"
    expect(site.location.site_id).to eq site.id
    expect(site.rescue_providers).to be_empty
  end

  context 'when HQ exists' do
    let(:open_time) { Time.now.utc }
    let(:close_time) { Time.now.utc + 2.hour }
    let(:open_time_sat) { Time.now.utc + 3.hour }
    let(:close_time_sat) { Time.now.utc + 4.hour }
    let(:open_time_sun) { Time.now.utc + 5.hour }
    let(:close_time_sun) { Time.now.utc + 6.hour }
    let(:always_open) { false }

    let!(:hq) do
      create(
        :partner_site,
        name: 'HQ',
        open_time: open_time,
        close_time: close_time,
        open_time_sat: open_time_sat,
        close_time_sat: close_time_sat,
        open_time_sun: open_time_sun,
        close_time_sun: close_time_sun,
        always_open: always_open,
        company: partner,
      )
    end

    it 'copies HQ times to new created site' do
      new_site = create_site

      expect(new_site.open_time.to_i).to eq partner.hq.open_time.to_i
      expect(new_site.close_time.to_i).to eq partner.hq.close_time.to_i
      expect(new_site.open_time_sat.to_i).to eq partner.hq.open_time_sat.to_i
      expect(new_site.close_time_sat.to_i).to eq partner.hq.close_time_sat.to_i
      expect(new_site.open_time_sun.to_i).to eq partner.hq.open_time_sun.to_i
      expect(new_site.close_time_sun.to_i).to eq partner.hq.close_time_sun.to_i
      expect(new_site.always_open).to eq partner.hq.always_open
    end

    context 'when new site is set to dispatchable' do
      let(:dispatchable) { true }

      it 'does not copy HQ times' do
        new_site = create_site

        expect(new_site.open_time).to be_nil
        expect(new_site.close_time).to be_nil
        expect(new_site.open_time_sat).to be_nil
        expect(new_site.close_time_sat).to be_nil
        expect(new_site.open_time_sun).to be_nil
        expect(new_site.close_time_sun).to be_nil
        expect(new_site.always_open).to be_falsey
        expect(new_site.dispatchable).to be_truthy
      end
    end
  end

  it "adds the partner's fleets if the site is dispatchable" do
    site_1 = partner.sites.create!(name: "Site 1")
    fleet = create(:fleet_managed_company)
    in_house = create(:fleet_in_house_company)
    fleet.rescue_providers.create!(provider: partner, site: site_1)
    in_house.rescue_providers.create!(provider: partner, site: site_1)

    service = SiteService::Create.new(
      site_params: {
        name: "Site 2",
        location_attributes: { address: "1137 Wisconsin Ave, Oak Park, IL" },
        dispatchable: true,
      },
      site_type: PartnerSite,
      company: partner,
    )
    site_2 = service.call.site
    expect(site_2.dispatchable?).to eq true
    expect(site_2.rescue_providers.map(&:company)).to match_array([swoop, fleet])
  end

  it "does not add rescue providers to non-partner sites" do
    fleet = create(:fleet_managed_company)
    service = SiteService::Create.new(
      site_params: { name: "Fleet Site 1", dispatchable: true },
      site_type: FleetSite,
      company: fleet,
    )
    site = service.call.site
    expect(site.name).to eq "Fleet Site 1"
    expect(site.rescue_providers).to be_empty
  end

  it "does not create a duplicate site name" do
    site = partner.sites.create!(name: "Site 1")
    service = SiteService::Create.new(
      site_params: { name: "Site 1", location_attributes: { address: "1137 Wisconsin Ave, Oak Park, IL" } },
      site_type: PartnerSite,
      company: partner,
    )
    service.call
    expect(service.site).to eq nil
    expect(service.duplicated_site).to eq site
    expect(service.duplicated_site?).to eq true
  end
end
