# frozen_string_literal: true

require 'rails_helper'

describe SiteService::SyncRescueProviders do
  let!(:swoop) { create(:super_company, :swoop) }
  let!(:partner) { create(:rescue_company) }
  let!(:fleet) { create(:fleet_managed_company) }
  let!(:other_fleet) { create(:fleet_managed_company) }
  let!(:in_house) { create(:fleet_in_house_company) }

  before do
    site = create(:partner_site, company: partner, dispatchable: true)
    fleet.rescue_providers.create!(provider: partner, site: site)
    in_house.rescue_providers.create!(provider: partner, site: site)
  end

  it "adds fleet company rescue providers to partner sites if dispatchable is true" do
    site = create(:partner_site, company: partner, dispatchable: true)
    SiteService::SyncRescueProviders.new(site).call
    expect(site.rescue_providers.map(&:company)).to match_array([swoop, fleet])
  end

  it "undeletes rescue providers if they were previously deleted" do
    site = create(:partner_site, company: partner, dispatchable: true)
    deleted_rescue_provider = site.rescue_providers.create!(provider: partner, company: fleet, deleted_at: Time.now)
    SiteService::SyncRescueProviders.new(site).call
    deleted_rescue_provider.reload
    expect(deleted_rescue_provider.deleted_at).to eq nil
    expect(site.rescue_providers.map(&:company)).to match_array([swoop, fleet])
  end

  it "does not add rescue providers if dispatchable is false" do
    site = create(:partner_site, company: partner, dispatchable: false)
    SiteService::SyncRescueProviders.new(site).call
    expect(site.rescue_providers.map(&:company)).to be_empty
  end

  it "does not add rescue provider to non partner sites" do
    site = create(:site, company: partner, dispatchable: true)
    SiteService::SyncRescueProviders.new(site).call
    expect(site.rescue_providers.map(&:company)).to be_empty
  end
end
