# frozen_string_literal: true

require 'rails_helper'

describe SiteService::Update do
  let!(:swoop) { create(:super_company, name: "Swoop") }
  let(:partner) { create(:rescue_company) }
  let(:site) { create(:partner_site, company: partner, name: "Site 1", description: "Description", dispatchable: false) }

  it "updates a site" do
    service = SiteService::Update.new(
      site_id: site.id,
      site_params: { name: "New Site Name", description: "New" },
      company: partner,
    )
    service.call
    site.reload
    expect(site.name).to eq "New Site Name"
    expect(site.description).to eq "New"
  end

  it "does not update a site if it would result in a duplicate name" do
    site_2 = create(:partner_site, company: partner, name: "Site 2", description: "Old")
    service = SiteService::Update.new(
      site_id: site.id,
      site_params: { name: "Site 2", description: "New" },
      company: partner,
    )
    service.call
    expect(service.duplicated_site?).to eq true
    expect(service.duplicated_site).to eq site_2
    site.reload
    site_2.reload
    expect(site.name).to eq "Site 1"
    expect(site.description).to eq "Description"
    expect(site_2.name).to eq "Site 2"
    expect(site_2.description).to eq "Old"
  end

  it "syncs rescue providers when the site is dispatchable" do
    site_1 = partner.sites.create!(name: "Site 1")
    fleet = create(:fleet_managed_company)
    in_house = create(:fleet_in_house_company)
    fleet.rescue_providers.create!(provider: partner, site: site_1)
    in_house.rescue_providers.create!(provider: partner, site: site_1)

    service = SiteService::Update.new(
      site_id: site.id,
      site_params: { dispatchable: true },
      company: partner,
    )
    service.call
    site.reload
    expect(site.dispatchable?).to eq true
    expect(site.rescue_providers.map(&:company)).to match_array([swoop, fleet])
  end

  it "does not sync rescue providers when the site is not dispatchable" do
    site_1 = partner.sites.create!(name: "Site 1")
    fleet = create(:fleet_managed_company)
    in_house = create(:fleet_in_house_company)
    fleet.rescue_providers.create!(provider: partner, site: site_1)
    in_house.rescue_providers.create!(provider: partner, site: site_1)

    service = SiteService::Update.new(
      site_id: site.id,
      site_params: { description: "New" },
      company: partner,
    )
    service.call
    site.reload
    expect(site.rescue_providers).to be_empty
  end
end
