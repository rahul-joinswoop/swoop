# frozen_string_literal: true

require "rails_helper"

describe Auction::StartAuctionService, vcr: true do
  let!(:rescue_provider_flt) do
    create(:rescue_provider, company: fleet_company, provider: flt, site: flt.sites[0])
  end

  let!(:rescue_provider_flt_closed_site) do
    create(:rescue_provider, company: fleet_company, provider: flt, site: flt_closed_for_business)
  end

  let!(:rescue_provider_bvt) do
    create(:rescue_provider, company: fleet_company, provider: bvt, site: bvt.sites[0])
  end

  let!(:rescue_provider_bvt_closed_site) do
    create(:rescue_provider, company: fleet_company, provider: bvt, site: bvt_closed_for_business)
  end

  let(:auto_assign_feature) { create(:feature, { name: Feature::AUTO_ASSIGN }) }
  let!(:swoop_company) { create(:super_company) }
  let!(:swoop_auto_dispatch_user) { create(:swoop_auto_dispatch, company: swoop_company) }

  let!(:memorex_location) do
    create(:location, {
      address: "1517 Memorex Dr, Santa Clara, CA 95050",
      lat: 37.3634351,
      lng: -121.9569983,
    })
  end

  let!(:memorex) do
    create(:partner_site, {
      location: memorex_location,
      name: 'memorex_site',
      always_open: true,
    })
  end

  let!(:flt_closed_location) do
    create(:location, {
      address: "1517 Memorex Dr, Santa Clara, CA 95050",
      lat: 37.3634351,
      lng: -121.9569983,
    })
  end

  let!(:flt_closed_for_business) do
    create(:partner_site, {
      location: flt_closed_location,
      name: 'flt_closed_site',
      always_open: false,
    })
  end

  let!(:flt) do
    create(:rescue_company, :with_swoop_account, :with_service_codes, {
      sites: [
        memorex,
        flt_closed_for_business,
      ],
    })
  end

  let!(:flt_rate) do
    create(:miles_p2p_rate,
           company: flt,
           account: flt.accounts.first,
           site: flt.sites.first,
           service_code: create(:service_code, name: ServiceCode::TOW),
           live: true)
  end

  let!(:commerce_st_location) do
    create(:location, {
      address: "2132 N Commerce St, North Las Vegas, NV 89030",
      lat: 36.1925204,
      lng: -115.1325535,
    })
  end

  let!(:commerce_st) do
    create(:partner_site, {
      location: commerce_st_location,
      name: 'commerce_st_site',
      always_open: true,
    })
  end

  let!(:bvt_closed_location) do
    create(:location, {
      address: "2132 N Commerce St, North Las Vegas, NV 89030",
      lat: 36.1925204,
      lng: -115.1325535,
    })
  end

  let!(:bvt_closed_for_business) do
    create(:partner_site, {
      location: flt_closed_location,
      name: 'flt_closed_site',
      always_open: false,
    })
  end

  let!(:bvt) do
    create(:rescue_company, :with_swoop_account, :with_service_codes, {
      sites: [
        commerce_st,
        bvt_closed_for_business,
      ],
    })
  end

  let!(:fleet_company) do
    create(:fleet_company, {
      features: [
        auto_assign_feature,
      ],
    })
  end

  let!(:sharks) do
    create(:location, {
      address: "525 W Santa Clara St, San Jose, CA 95113",
      lat: 37.3327303,
      lng: -121.9012363,
    })
  end

  let!(:job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
      service_location: sharks,
      status: Job::AUTO_ASSIGNING,
    })
  end

  let!(:existing_job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
      service_location: sharks,
      status: Job::DISPATCHED,
    })
  end

  # Rescue vehicle at Santana Row
  let!(:flt_rescue_vehicle) do
    create(:rescue_vehicle, {
      name: "flt rv 1",
      company: flt,
      lat: 37.3180705,
      lng: -121.9495058,
      location_updated_at: Time.now,
    })
  end

  let(:flt_driver_off_duty) { create(:user, company: flt, first_name: "flt (offduty)") }

  let(:flt_driver) { create(:user, :driver_on_duty, company: flt, first_name: "flt (onduty)") }

  def set_driver_in_vehicle(driver, vehicle)
    driver.vehicle = vehicle
    driver.save!
  end

  def create_rescue_providers_and_set_companies_live_and_call_service
    flt.live = true
    flt.save!
    bvt.live = true
    bvt.save!
  end

  def associate_vehicle_with_job(vehicle, job)
    job.rescue_vehicle = vehicle
    job.save!
  end

  describe ".call" do
    subject do
      Auction::StartAuctionService.new(job).call
    end

    context "No Partners" do
      ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
        it "creates no bids" do
          subject
          expect(Auction::Bid.all.count).to eq 0
        end

        it "leaves job in pending" do
          subject
          job.reload
          expect(job.status).to eq "pending"
        end

        it "leaves the candidate assignment in no_winner state" do
          subject
          job.reload
          assignment = Job::Candidate::Assignment.first
          expect(assignment).not_to be_nil
          expect(assignment.status).to eq(Job::Candidate::Assignment::MANUAL_ASSIGNMENT)
          expect(assignment.result).to eq("auction_no_winner")
        end
      end

      it 'creates one auction' do
        subject
        expect(Auction::Auction.count).to eql(1)
      end
    end

    context "RP's not live" do
      ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
        it "leaves the candidate assignment in no_winner state" do
          subject
          assignment = Job::Candidate::Assignment.first
          expect(assignment).not_to be_nil
          expect(assignment.status).to eq(Job::Candidate::Assignment::MANUAL_ASSIGNMENT)
          expect(assignment.result).to eq("auction_no_winner")
        end

        it "leaves auction in UNSUCCESSFUL state" do
          subject
          auction = Auction::Auction.first
          expect(auction).not_to be_nil
          expect(auction.result).to eq("no_live_trucks_or_sites_in_range")
          expect(auction.status).to eq(Auction::Auction::UNSUCCESSFUL)
        end
      end
    end

    context "Driver on duty" do
      before do
        flt.live = true
        flt.save!
        set_driver_in_vehicle(flt_driver, flt_rescue_vehicle)

        # Force exclusion of sites
        Site.update_all(always_open: false, force_open: false, within_hours: false)
      end

      it "leaves auction in LIVE state" do
        subject
        auction = Auction::Auction.first
        expect(auction).not_to be_nil
        expect(auction.result).to be_nil
        expect(auction.status).to eq(Auction::Auction::LIVE)
      end
    end

    context "Driver off duty" do
      before do
        flt.live = true
        flt.save!
        set_driver_in_vehicle(flt_driver_off_duty, flt_rescue_vehicle)

        # Force exclusion of sites
        Site.update_all(always_open: false, force_open: false, within_hours: false)
      end

      it "leaves the candidate assignment in no_winner state" do
        subject
        assignment = Job::Candidate::Assignment.first
        expect(assignment).not_to be_nil
        expect(assignment.status).to eq(Job::Candidate::Assignment::MANUAL_ASSIGNMENT)
        expect(assignment.result).to eq("auction_no_winner")
      end

      it "leaves auction in UNSUCCESSFUL state" do
        subject
        auction = Auction::Auction.first
        expect(auction).not_to be_nil
        expect(auction.result).to eq("no_elgible_sites_or_vehicles_in_eta")
        expect(auction.status).to eq(Auction::Auction::UNSUCCESSFUL)
      end
    end

    context "Trucks out of rescue provider range" do
      before do
        flt.live = true
        flt.save!
        set_driver_in_vehicle(flt_driver, flt_rescue_vehicle)
        site = create(:site, company: flt_rescue_vehicle.company)
        create(:rescue_provider, company: swoop_company, site: site, auto_assign_distance: 1)

        # Force exclusion of sites
        Site.update_all(always_open: false, force_open: false, within_hours: false)
      end

      it "fails to find vehicles" do
        subject

        assignment = Job::Candidate::Assignment.first
        expect(assignment).not_to be_nil
        expect(assignment.status).to eq(Job::Candidate::Assignment::MANUAL_ASSIGNMENT)
        expect(assignment.result).to eq("auction_no_winner")

        auction = Auction::Auction.first
        expect(auction).not_to be_nil
        expect(auction.result).to eq("no_live_trucks_or_sites_in_range")
        expect(auction.status).to eq(Auction::Auction::UNSUCCESSFUL)
      end
    end

    context "Candidates out of range" do
      before do
        create_rescue_providers_and_set_companies_live_and_call_service
      end

      it "leaves the candidate assignment in no_winner state" do
        ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "1" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
            subject
            assignment = Job::Candidate::Assignment.first
            expect(assignment).not_to be_nil
            expect(assignment.status).to eq(Job::Candidate::Assignment::MANUAL_ASSIGNMENT)
            expect(assignment.result).to eq("auction_no_winner")
          end
        end
      end

      it "creates no bids" do
        ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "1" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
            subject
            expect(Auction::Bid.all.count).to eq 0
          end
        end
      end

      it "leaves auction in UNSUCCESSFUL state" do
        ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "1" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
            subject
            auction = Auction::Auction.first
            expect(auction).not_to be_nil
            expect(auction.result).to eq("no_live_trucks_or_sites_in_range")
            expect(auction.status).to eq(Auction::Auction::UNSUCCESSFUL)
          end
        end
      end
    end

    context "Candidates eta too high" do
      before do
        stub_const "RescueProvider::DEFAULT_AUTO_ASSIGN_DISTANCE_MILES", 3000
        create_rescue_providers_and_set_companies_live_and_call_service
      end

      it "leaves the candidate assignment in no_winner state" do
        ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "1" do
            subject
            assignment = Job::Candidate::Assignment.first
            expect(assignment).not_to be_nil
            expect(assignment.status).to eq(Job::Candidate::Assignment::MANUAL_ASSIGNMENT)
            expect(assignment.result).to eq("auction_no_winner")
          end
        end
      end

      it "creates no bids" do
        ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "1" do
            subject
            expect(Auction::Bid.all.count).to eq 0
          end
        end
      end

      it "leaves auction in UNSUCCESSFUL state" do
        ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "300000" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "1" do
            expect(Setting.get_integer(job.fleet_company, Setting::AUCTION_MAX_DISTANCE) || Auction::Auction.default_max_candidate_distance).to eql(300000)
            dump_table(Company)
            dump_table(Site)
            dump_table(RescueProvider)
            dump_table(CompaniesService)
            subject
            auction = Auction::Auction.first
            expect(auction).not_to be_nil
            expect(auction.result).to eq("no_elgible_sites_or_vehicles_in_eta")
            expect(auction.status).to eq(Auction::Auction::UNSUCCESSFUL)
          end
        end
      end
    end

    context "ETA Only Ranker" do
      let!(:wscr) do
        create :weighted_sum_ranker, :eta_only, :loose_constraints, {
          client: fleet_company,
          live: true,
        }
      end

      context "Auction Success" do
        before do
          stub_const "RescueProvider::DEFAULT_AUTO_ASSIGN_DISTANCE_MILES", 3000
          create_rescue_providers_and_set_companies_live_and_call_service
        end

        it "has no candidate assignment" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
            ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
              subject
              assignment = Job::Candidate::Assignment.first
              expect(assignment).to be_nil
            end
          end
        end

        it "creates two bids" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
            ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
              subject
              dump_table(Site)
              dump_table(Location)
              dump_table(Job::Candidate)

              expect(Auction::Bid.all.count).to eq 2
            end
          end
        end

        it "leaves auction in LIVE state" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
            ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
              subject
              auction = Auction::Auction.first
              expect(auction).not_to be_nil
              expect(auction.result).to be_nil
              expect(auction.status).to eq(Auction::Auction::LIVE)
            end
          end
        end
        context "estimates the cost" do
          context "with feature enabled" do
            it "if there's a viable rate" do
              ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
                ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
                  ClimateControl.modify ENABLE_CANDIDATE_COST_CALCULATION: "TRUE" do
                    subject
                    expect(Job::Candidate.find_by(company: flt).cost).to eql 134
                    expect(Job::Candidate.find_by(company: flt).vehicle_eligible).to eql "eligible"
                    expect(Job::Candidate.find_by(company: bvt).cost).to eql nil
                    expect(Job::Candidate.find_by(company: bvt).vehicle_eligible).to eql "no_rate_to_calculate_cost_error"
                  end
                end
              end
            end
            it "does not calculate cost for a candidate whose best rate is a storage rate" do
              ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
                ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
                  ClimateControl.modify ENABLE_CANDIDATE_COST_CALCULATION: "TRUE" do
                    create(:storage_rate, company: bvt, account: bvt.accounts.first, site: bvt.sites.first, live: true)
                    subject
                    expect(Job::Candidate.find_by(company: flt).cost).to eql 151
                    expect(Job::Candidate.find_by(company: flt).vehicle_eligible).to eql "eligible"
                    expect(Job::Candidate.find_by(company: bvt).cost).to eql nil
                    expect(Job::Candidate.find_by(company: bvt).vehicle_eligible).to eql "cannot_calculate_cost_from_storage_rate_error"
                  end
                end
              end
            end
          end

          context "with feature disabled (default)" do
            it "sets the rate to $100" do
              ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
                ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
                  subject
                  expect(Job::Candidate.find_by(company: flt).cost).to eql 100
                  expect(Job::Candidate.find_by(company: flt).vehicle_eligible).to eql "eligible"
                  expect(Job::Candidate.find_by(company: bvt).cost).to eql 100
                  expect(Job::Candidate.find_by(company: bvt).vehicle_eligible).to eql "eligible"
                end
              end
            end
          end
        end
      end

      context "Auction Success with rescue vehicle on existing job" do
        before do
          stub_const "RescueProvider::DEFAULT_AUTO_ASSIGN_DISTANCE_MILES", 3000
          set_driver_in_vehicle(flt_driver, flt_rescue_vehicle)
          associate_vehicle_with_job(flt_rescue_vehicle, existing_job)
          create_rescue_providers_and_set_companies_live_and_call_service
        end

        it "has no candidate assignment" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
            ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
              subject
              assignment = Job::Candidate::Assignment.first
              expect(assignment).to be_nil
            end
          end
        end

        it "creates two bids" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
            ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
              subject
              dump_table(Job::Candidate)
              expect(Auction::Bid.all.count).to eq 2
            end
          end
        end

        it "Creates three candidates" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
            ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
              subject
              dump_table(Site)
              dump_table(Location)
              dump_table(Job::Candidate)
              dump_table(Job::Candidate::CandidateJob)
              dump_table(Vehicle)
              dump_table(User)
              expect(Job::Candidate.all.count).to eq 3
            end
          end
        end

        it "leaves auction in LIVE state" do
          ClimateControl.modify AUCTION_MAX_CANDIDATE_DISTANCE_DEFAULT: "3000" do
            ClimateControl.modify AUCTION_MAX_CANDIDATE_ETA_DEFAULT: "108000" do
              subject
              auction = Auction::Auction.first
              expect(auction).not_to be_nil
              expect(auction.result).to be_nil
              expect(auction.status).to eq(Auction::Auction::LIVE)
            end
          end
        end
      end
    end
  end
end
