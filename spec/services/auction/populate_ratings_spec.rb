# frozen_string_literal: true

require "rails_helper"

describe Auction::PopulateRatings do
  let!(:flt) do
    create(:rescue_company, :finish_line)
  end

  let!(:fleet_company) do
    create(:fleet_managed_company)
  end

  let!(:job) do
    create(:fleet_managed_job, {
      rescue_company: flt,
      fleet_company: fleet_company,
    })
  end

  let(:subject) do
    Auction::PopulateRatings.new(
      fleet_company.id,
      { min_num_jobs: 0, type: :nps }
    ).call
  end

  context "no reviews" do
    it "doesn't create ratings entry" do
      expect { subject }.to avoid_changing(Rating, :count)
    end
  end

  context "single review" do
    let!(:review1) do
      create(:review, {
        company: fleet_company,
        job: job,
        nps_question1: 9,
      })
    end

    it 'creates single 100 score nps rating' do
      expect { subject }.to change(Rating, :count).from(0).to(1)
      expect(Rating.first.score).to eql(100.0)
    end
  end

  context "multiple reviews" do
    let!(:review1) do
      create(:review, {
        company: fleet_company,
        job: job,
        nps_question1: 9,
      })
    end

    let!(:review2) do
      create(:review, {
        company: fleet_company,
        job: job,
        nps_question1: 7,
      })
    end

    it 'creates single 50 score nps rating' do
      expect { subject }.to change(Rating, :count).from(0).to(1)
      expect(Rating.first.score).to eql(50.0)
    end
  end

  context "multiple SP's" do
    let!(:bvt) do
      create(:rescue_company)
    end

    let!(:job2) do
      create(:fleet_managed_job, {
        rescue_company: bvt,
        fleet_company: fleet_company,
      })
    end

    let!(:review1) do
      create(:review, {
        company: fleet_company,
        job: job,
        nps_question1: 9,
      })
    end

    let!(:review2) do
      create(:review, {
        company: fleet_company,
        job: job2,
        nps_question1: 5,
      })
    end

    it 'creates two ratings' do
      expect { subject }.to change(Rating, :count).from(0).to(2)
    end

    it 'creates flt 100 nps' do
      subject
      expect(Rating.find_by(rescue_company: flt).score).to eql(100.0)
    end

    it 'creates bvt 0 nps' do
      subject
      expect(Rating.find_by(rescue_company: bvt).score).to eql(-100.0)
    end
  end
end
