# frozen_string_literal: true

require "rails_helper"

describe Auction::SubmitBidService do
  let!(:rescue_company) { create :rescue_company }
  let!(:job) { create :job, :with_service_location }
  let!(:bid) { create :bid, job: job, company: rescue_company }
  let!(:candidate) { create :job_candidate, job: job, company: rescue_company }

  let(:bid_time) { 5.minutes.from_now }
  let(:candidate_id) { candidate.id }

  context 'negative cases' do
    context 'missing params' do
      subject { Auction::SubmitBidService.new(*args) }

      shared_examples 'it raises error' do
        it do
          expect { subject }.to raise_error(expected_error)
        end
      end

      context "job is missing" do
        let(:args) { [nil, rescue_company, bid_time, candidate_id] }
        let(:expected_error) { ArgumentError }

        it_behaves_like 'it raises error'
      end

      context "rescue company is missing" do
        let(:args) { [job, nil, bid_time, candidate_id] }
        let(:expected_error) { ArgumentError }

        it_behaves_like 'it raises error'
      end

      context "bid_time" do
        context "is missing" do
          let(:args) { [job, rescue_company, nil, candidate_id] }
          let(:expected_error) { ArgumentError }

          it_behaves_like 'it raises error'
        end

        context "is not integer" do
          let(:args) { [job, rescue_company, { bid: Time.now }, candidate_id] }
          let(:expected_error) { TypeError }

          it_behaves_like 'it raises error'
        end
      end

      context "candidate_id" do
        context 'is non-existent' do
          let(:args) { [job, rescue_company, bid_time, -1] }
          let(:expected_error) { ActiveRecord::RecordNotFound }

          it_behaves_like 'it raises error'
        end

        context 'is a candidate record' do
          let(:args) { [job, rescue_company, bid_time, candidate] }
          let(:expected_error) { TypeError }

          it_behaves_like 'it raises error'
        end
      end
    end
  end

  context 'positive cases' do
    context 'candidate is not provided' do
      let!(:candidate) { create :job_candidate, job: job, company: rescue_company }

      subject { Auction::SubmitBidService.new(job, rescue_company, bid_time, nil).call }

      specify 'finds the candidate anyway' do
        expect(subject).to be_a(Auction::Bid)
        expect(subject.candidate).to eq(candidate)
      end
    end

    context 'bid time' do
      subject { Auction::SubmitBidService.new(job, rescue_company, bid_time, candidate.id).call }

      context 'relative' do
        let(:bid_time) { 35.seconds }

        specify 'The bid is calculated based on minutes' do
          now = Time.now
          Timecop.freeze(now) do
            result = subject
            expect(result).to be_a(Auction::Bid)

            bid_mins = Time.now + bid_time.minutes
            expect(result.eta_mins).to eq(bid_time)
            expect(result.eta_dttm).to eq(bid_mins)
          end
        end
      end

      context 'absolute' do
        specify 'The bid is calculated based on duration' do
          now = Time.now
          Timecop.freeze(now) do
            result = subject
            expect(result).to be_a(Auction::Bid)

            expect(result.bid_submission_dttm).to eq(now)
            expect(result.status).to eq(Auction::Bid::SUBMITTED)
            expect(result.candidate.id).to eq(candidate_id)
            expect(result.cost).to eq(candidate.cost)
            expect(result.rating).to eq(candidate.rating)

            bid_mins = ((bid_time - Time.now) / 60).round
            expect(result.eta_mins).to eq(bid_mins)
            expect(result.eta_dttm).to eq(bid_time)
          end
        end
      end
    end
  end
end
