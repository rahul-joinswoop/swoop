# frozen_string_literal: true

require 'rails_helper'

describe Auction::RejectBidService do
  let(:rescue_company) { create(:rescue_company) }
  let(:job) { create(:fleet_managed_job) }
  let(:reject_reason) { create(:job_reject_reason) }
  let(:auction) { create(:auction, job: job) }

  let!(:bid) { create(:bid, status: Auction::Bid::REQUESTED, job: job, company: rescue_company, auction: auction) }

  it "rejects the bid" do
    expect(Auction::Auction).to receive(:slack_notification).once.with(job, instance_of(String))
    result = Auction::RejectBidService.new(job, rescue_company, reject_reason.id, "I don't want to").call
    expect(result).to eq bid
    bid.reload
    expect(bid.status).to eq Auction::Bid::PROVIDER_REJECTED
    expect(bid.job_reject_reason).to eq reject_reason
    expect(bid.job_reject_reason_info).to eq "I don't want to"
    expect(Auction::ExpireAuctionTimer).to have_enqueued_sidekiq_job(auction.id)
  end

  it "selects the correct bid to reject" do
    expect(Auction::Auction).to receive(:slack_notification).once.with(job, instance_of(String))
    bid_2 = create(:bid, status: Auction::Bid::REQUESTED, job: job, company: create(:rescue_company), auction: auction)
    result = Auction::RejectBidService.new(job, rescue_company, reject_reason.id, "n/a").call
    expect(result).to eq bid
    bid_2.reload
    expect(bid_2.status).to eq Auction::Bid::REQUESTED
    expect(Auction::ExpireAuctionTimer.jobs).to be_empty
  end

  it "raises an error if the service is called twice" do
    expect(Auction::Auction).to receive(:slack_notification).twice.with(job, instance_of(String))
    expect do
      Auction::RejectBidService.new(job, rescue_company, reject_reason.id, "I don't want to").call
    end.not_to raise_error
    expect do
      Auction::RejectBidService.new(job, rescue_company, reject_reason.id, "Whoops, I did it again").call
    end.to raise_error(Auction::CannotRejectABidThatIsPastRejectionStageError)
    bid.reload
    expect(bid.status).to eq Auction::Bid::PROVIDER_REJECTED
    expect(bid.job_reject_reason_info).to eq "I don't want to"
  end

  it "raises an error if the bid status is not requested" do
    bid.update_column(:status, Auction::Bid::WON)
    expect do
      Auction::RejectBidService.new(job, rescue_company, reject_reason.id, "n/a").call
    end.to raise_error(Auction::CannotRejectABidThatIsntInRequestedStateError)
  end
end
