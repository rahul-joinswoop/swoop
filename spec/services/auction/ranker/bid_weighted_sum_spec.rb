# frozen_string_literal: true

require "rails_helper"

describe Auction::Rankers::BidWeightedSum do
  let!(:flt) do
    create(:rescue_company, :finish_line)
  end

  let!(:fleet_company) do
    create(:fleet_managed_company)
  end

  let!(:job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
    })
  end

  let!(:auction) do
    create(:auction, {
      job: job,
    })
  end

  context 'V1' do
    let(:subject) do
      Auction::Rankers::BidWeightedSum.new(
        auction.id,
        1,
        { max_eta: 90, max_cost: 100, min_rating: 40 },
        { quality: 0.2, cost: 0.6, speed: 0.2 }
      ).call
    end

    it "returns if no AAC" do
      expect(subject).to eql(nil)
    end

    context 'No bids within constraints' do
      let!(:bid1) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 91,
          company: flt,
          cost: 75,
          rating: 66,
        })
      end

      it "doesn't calculate score" do
        subject
        expect { bid1.reload }.to avoid_changing(bid1, :score)
      end
    end

    context 'single site auto assign candidate' do
      let!(:bid1) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 90,
          company: flt,
          cost: 100,
          rating: 40,
        })
      end

      it 'find calcualtes top score for the only one' do
        subject
        expect { bid1.reload }.to change(bid1, :score).to(1.0)
      end
    end

    context 'multiple site auto assign candidate' do
      let!(:bid1) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 90,
          company: flt,
          cost: 100,
          rating: 40,
        })
      end
      let!(:bid2) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 60,
          company: flt,
          cost: 100,
          rating: 40,
        })
      end

      it 'calculates scores' do
        subject
        expect { bid1.reload }.to change(bid1, :score).to(0.8)
        expect { bid2.reload }.to change(bid2, :score).to(1.0)
      end
    end
  end

  context 'V2' do
    let(:subject) do
      Auction::Rankers::BidWeightedSum.new(
        auction.id,
        2,
        { min_eta: 5, max_eta: 90, min_cost: 20, max_cost: 100, min_rating: 40, max_rating: 100 },
        { quality: 0.1, cost: 0.8, speed: 0.1 }
      ).call
    end

    it "returns if no AAC" do
      expect(subject).to eql(nil)
    end

    context 'No bids within constraints' do
      let!(:bid1) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 91,
          company: flt,
          cost: 75,
          rating: 66,
        })
      end

      it "doesn't calculate score" do
        subject
        expect { bid1.reload }.to avoid_changing(bid1, :score)
      end
    end

    context 'single site auto assign candidate' do
      let!(:bid1) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 90,
          company: flt,
          cost: 100,
          rating: 40,
        })
      end

      it 'find calcualtes top score for the only one' do
        subject
        expect { bid1.reload }.to change(bid1, :score).to(0.0)
      end
    end

    context 'multiple site auto assign candidate' do
      let!(:bid1) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 90,
          company: flt,
          cost: 100,
          rating: 40,
        })
      end
      let!(:bid2) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 60,
          company: flt,
          cost: 100,
          rating: 40,
        })
      end
      let!(:bid3) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 5,
          company: flt,
          cost: 20,
          rating: 100,
        })
      end
      let!(:bid4) do
        create(:bid, {
          job: job,
          auction: auction,
          eta_mins: 3,
          company: flt,
          cost: 5,
          rating: 100,
        })
      end

      it 'calculates scores' do
        subject
        expect { bid1.reload }.to change(bid1, :score).to(0.0)
        expect { bid2.reload }.to change(bid2, :score).to(0.035)
        expect { bid3.reload }.to change(bid3, :score).to(1.0)
        expect { bid4.reload }.to change(bid4, :score).to(1.0)
      end
    end
  end
end
