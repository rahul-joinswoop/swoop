# frozen_string_literal: true

require "rails_helper"

describe Auction::Rankers::CandidateWeightedSum do
  let!(:flt) do
    create(:rescue_company, :finish_line)
  end

  let!(:fleet_company) do
    create(:fleet_managed_company)
  end

  let!(:job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
    })
  end

  context 'V1' do
    let(:subject) do
      Auction::Rankers::CandidateWeightedSum.new(
        job.id,
        1,
        { max_eta: 90, max_cost: 100, min_rating: 40 },
        { quality: 0.2, cost: 0.6, speed: 0.2 }
      ).call
    end

    it "returns if no AAC" do
      expect(subject).to eql(nil)
    end

    context 'No aacs within constraints' do
      let!(:aac1) do
        create(:site_candidate, {
          job: job,
          google_eta: 91,
          company: flt,
          cost: 75,
          rating: 66,
          vehicle_eligible: 'eligible',
        })
      end

      it "doesn't calculate score" do
        subject
        expect { aac1.reload }.to avoid_changing(aac1, :score)
      end
    end

    context 'single site auto assign candidate' do
      let!(:aac1) do
        create(:site_candidate, {
          job: job,
          google_eta: 90,
          company: flt,
          cost: 100,
          rating: 40,
          vehicle_eligible: 'eligible',
        })
      end

      it 'find calcualtes top score for the only one' do
        subject
        expect { aac1.reload }.to change(aac1, :score).to(1.0)
      end
    end

    context 'multiple site auto assign candidate' do
      let!(:aac1) do
        create(:site_candidate, {
          job: job,
          google_eta: 90,
          company: flt,
          cost: 100,
          rating: 40,
          vehicle_eligible: 'eligible',
        })
      end
      let!(:aac2) do
        create(:site_candidate, {
          job: job,
          google_eta: 60,
          company: flt,
          cost: 100,
          rating: 40,
          vehicle_eligible: 'eligible',
        })
      end

      it 'calculates scores' do
        subject
        expect { aac1.reload }.to change(aac1, :score).to(0.8)
        expect { aac2.reload }.to change(aac2, :score).to(1.0)
      end
    end
  end

  context 'V2' do
    let(:subject) do
      Auction::Rankers::CandidateWeightedSum.new(
        job.id,
        2,
        { min_eta: 5, max_eta: 90, min_cost: 20, max_cost: 100, min_rating: 40, max_rating: 100 },
        { quality: 0.1, cost: 0.8, speed: 0.1 }
      ).call
    end

    it "returns if no AAC" do
      expect(subject).to eql(nil)
    end

    context 'No aacs within constraints' do
      let!(:aac1) do
        create(:site_candidate, {
          job: job,
          google_eta: 91,
          company: flt,
          cost: 75,
          rating: 66,
          vehicle_eligible: 'eligible',
        })
      end

      it "doesn't calculate score" do
        subject
        expect { aac1.reload }.to avoid_changing(aac1, :score)
      end
    end

    context 'single site auto assign candidate' do
      let!(:aac1) do
        create(:site_candidate, {
          job: job,
          google_eta: 90,
          company: flt,
          cost: 100,
          rating: 40,
          vehicle_eligible: 'eligible',
        })
      end

      it 'calculates lowest possible score' do
        subject
        expect { aac1.reload }.to change(aac1, :score).to(0.0)
      end
    end

    context 'multiple site auto assign candidate' do
      let!(:aac1) do
        create(:site_candidate, {
          job: job,
          google_eta: 90,
          company: flt,
          cost: 100,
          rating: 40,
          vehicle_eligible: 'eligible',
        })
      end
      let!(:aac2) do
        create(:site_candidate, {
          job: job,
          google_eta: 60,
          company: flt,
          cost: 100,
          rating: 40,
          vehicle_eligible: 'eligible',
        })
      end

      let!(:aac3) do
        create(:site_candidate, {
          job: job,
          google_eta: 5,
          company: flt,
          cost: 20,
          rating: 100,
          vehicle_eligible: 'eligible',
        })
      end

      let!(:aac4) do
        create(:site_candidate, {
          job: job,
          google_eta: 3, # below min_eta of 5, should calculate score as if it were 5
          company: flt,
          cost: 5, # below min cost of 20, should calculate score as if it were 20
          rating: 100,
          vehicle_eligible: 'eligible',
        })
      end

      it 'calculates scores' do
        subject
        expect { aac1.reload }.to change(aac1, :score).to(0.0)
        expect { aac2.reload }.to change(aac2, :score).to(0.035)
        expect { aac3.reload }.to change(aac3, :score).to(1.0)
        expect { aac4.reload }.to change(aac4, :score).to(1.0)
      end
    end
  end
end
