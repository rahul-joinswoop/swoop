# frozen_string_literal: true

require "rails_helper"

describe Auction::ExpireAuctionService, :vcr do
  let(:auto_assign_feature) { create(:feature, { name: Feature::AUTO_ASSIGN }) }
  let!(:swoop_company) { create(:super_company) }

  let(:rescue_company) do
    create(:rescue_company, {
      name: 'rescue co 1',
      sites: [
        create(:partner_site),
      ],
    })
  end

  let(:rescue_company2) do
    create(:rescue_company, {
      name: 'rescue co 2',
      sites: [
        create(:partner_site),
        create(:partner_site),
      ],
    })
  end

  let(:fleet_company) do
    create(:fleet_company, {
      features: [
        auto_assign_feature,
      ],
    })
  end

  let!(:wsr) do
    create :weighted_sum_ranker, :eta_only, :sensible_constraints, {
      client: fleet_company,
      live: true,
    }
  end

  let(:auction) do
    create :auction, :live,
           job: job,
           max_target_eta: 10,
           ranker: wsr
  end

  let!(:job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
      status: Job::AUTO_ASSIGNING,
      root_dispatcher_id: user.id,
      service_location: create(:location),
      rescue_company: nil,
    })
  end

  let(:user) { create(:user, company: swoop_company) }

  let!(:bid1) do
    create(:bid, {
      job: job,
      company: rescue_company,
      eta_mins: bid1_eta,
      status: bid_status,
      auction: auction,
      cost: 0,
      rating: 0,
    })
  end

  let(:rc2_candidate) { nil }

  let!(:bid2) do
    create(:bid, {
      job: job,
      company: rescue_company2,
      eta_mins: bid2_eta,
      status: bid_status,
      auction: auction,
      cost: 0,
      rating: 0,
      candidate: rc2_candidate,
    })
  end

  let!(:assignment) do
    create(:assignment, {
      job: job,
    })
  end

  let(:bid_status) { Auction::Bid::SUBMITTED }

  def get_service
    Auction::ExpireAuctionService.new(auction.id)
  end

  describe ".call" do
    let(:bid1_eta) { 11 }
    let(:bid2_eta) { 2 }

    before do
      @service = get_service
    end

    context "Auto Assign Success" do
      it "has result true" do
        expect(@service.call).to be true
      end

      it 'Assigns rescue company' do
        expect { @service.call && job.reload }.to change(job, :rescue_company).from(nil).to(rescue_company2)
      end

      describe 'the job is given a site' do
        context 'no specific site on the bid' do
          it 'Assigns default site' do
            expect { @service.call && job.reload }.to change(job, :site).from(nil).to(rescue_company2.sites[0])
          end
        end

        context 'bid comes in on specific site(via candidate)' do
          let(:rc2_candidate) do
            Job::Candidate::Site.create(
              job: job,
              site: rescue_company2.sites[1],
              company: rescue_company2
            )
          end

          it 'Assigns candidate site' do
            expect { @service.call && job.reload }.to change(job, :site).from(nil).to(rescue_company2.sites[1])
          end
        end
      end
    end

    context 'when there is no winner' do
      let(:bid_status) { Auction::Bid::NEW }
      let(:bid1_eta) { nil }
      let(:bid2_eta) { nil }

      it "has result true" do
        expect(@service.call).to be true
      end

      it 'removes root_dispatcher assigned to the Job' do
        expect(job.root_dispatcher_id).to be_present
        @service.call
        dump_table(Auction::Bid)

        job.reload

        expect(job.root_dispatcher_id).to be_blank
      end

      it 'sends system alert to Swoop users if necessary' do
        allow_any_instance_of(Job).to receive(:system_alert_swoop_if_necessary)

        expect_any_instance_of(Job).to receive(:system_alert_swoop_if_necessary)

        @service.call
      end
    end
  end
end
