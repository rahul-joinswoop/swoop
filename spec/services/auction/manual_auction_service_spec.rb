# frozen_string_literal: true

require "rails_helper"

describe Auction::ManualAuctionService, :vcr do
  let(:auto_assign_feature) { create(:feature, { name: Feature::AUTO_ASSIGN }) }
  let!(:swoop_company) { create(:super_company) }

  let(:rescue_company) do
    create(:rescue_company, {
      sites: [
        create(:partner_site),
      ],
    })
  end

  let(:fleet_company) do
    create(:fleet_company, {
      features: [
        auto_assign_feature,
      ],
    })
  end

  let!(:rescue_provider) do
    create(:rescue_provider, {
      company: fleet_company,
      provider: rescue_company,
      site: rescue_company.hq,
    })
  end

  let!(:wsr) do
    create :weighted_sum_ranker, :eta_only, :loose_constraints, {
      client: fleet_company,
      live: true,
    }
  end

  let(:auction) do
    create :auction, :live,
           job: job,
           max_target_eta: 10,
           ranker: wsr
  end

  let!(:job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
      status: Job::AUTO_ASSIGNING,
      root_dispatcher_id: user.id,
      service_location: create(:location),
      rescue_company: nil,
    })
  end

  let(:user) { create(:user, company: swoop_company) }

  let(:truck_candidate) do
    Job::Candidate::Truck.create!(
      job: job,
      company: rescue_company,
      site: rescue_company.hq

    )
  end

  let!(:bid1) do
    create(:bid, {
      job: job,
      company: rescue_company,
      eta_mins: 90,
      status: Auction::Bid::SUBMITTED,
      auction: auction,
      cost: 0,
      rating: 0,
      candidate: truck_candidate,
    })
  end

  def get_service
    Auction::ManualAuctionService.new(auction.id, rescue_provider.id)
  end

  describe ".call" do
    before do
      @service = get_service
    end

    context "Auto Assign Success" do
      it "has result true" do
        expect(@service.call).to be true
      end

      it 'Assigns rescue company' do
        expect { @service.call && job.reload }.to change(job, :rescue_company).from(nil).to(rescue_company)
      end

      describe 'the job is given a site' do
        it 'Assigns default site' do
          expect { @service.call && job.reload }.to change(job, :site).from(nil).to(rescue_company.hq)
        end
      end
    end
  end
end
