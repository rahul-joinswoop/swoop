# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GraphQL::BuildFilter, type: [:interactor, :graphql] do
  let(:user) { nil }
  let(:company) { nil }
  let(:roles) { nil }
  let(:context) do
    {
      api_user: user,
      api_company: company,
      api_roles: roles,
    }
  end

  describe "#call" do
    subject { GraphQL::BuildFilter.new(interactor_context).call }

    it "works" do
      expect(subject).to be_success
    end
  end

  describe "only_filter" do
    subject { GraphQL::BuildFilter.new(interactor_context).call.dig(:output, :only_filter) }

    let(:schema_member) do
      OpenStruct.new(metadata: {}.tap do |m|
        m[:required_roles] = required_roles if defined? required_roles
        m[:required_features] = required_features if defined? required_features
        m[:if_company] = if_company if defined? if_company
        m[:if_context] = if_context if defined? if_context
      end)
    end

    let(:roles) { [] }

    describe 'required_features' do
      let(:feature) { create :feature, name: Feature::INVOICING }
      let(:feature2) { create :feature, name: Feature::QUESTIONS }
      let(:feature3) { create :feature, name: Feature::SETTINGS }

      let(:company) { create :company, features: [feature, feature2] }

      shared_examples "required_features" do
        context 'without any required_features' do
          it "works" do
            expect(subject.call(schema_member, context)).to be_truthy
          end
        end

        context 'with required_features' do
          context 'with a single matching feature' do
            let(:required_features) { Feature::INVOICING }

            it "works" do
              expect(subject.call(schema_member, context)).to be_truthy
            end
          end

          context 'without a single matching role' do
            let(:required_features) { Feature::SETTINGS }

            it "works" do
              expect(subject.call(schema_member, context)).to be_falsey
            end
          end

          context 'with a set of matching features' do
            let(:required_features) { [Feature::INVOICING, Feature::QUESTIONS] }

            it "works" do
              expect(subject.call(schema_member, context)).to be_truthy
            end
          end

          context 'without a set of matching roles' do
            let(:required_features) { [Feature::INVOICING, Feature::SETTINGS] }

            it "works" do
              expect(subject.call(schema_member, context)).to be_falsey
            end
          end

          context 'with a positive proc' do
            let(:required_features) { -> (features) { features.include?(Feature::INVOICING) } }

            it "works" do
              expect(subject.call(schema_member, context)).to be_truthy
            end
          end

          context 'with a negative proc' do
            let(:required_features) { -> (features) { features.include?(Feature::CANADIAN_AP) } }

            it "works" do
              expect(subject.call(schema_member, context)).to be_falsey
            end
          end
        end
      end

      context 'with a user' do
        let(:user) { create :user, company: company }

        it_behaves_like 'required_features'
      end

      context 'with an application' do
        let(:application) do
          create :doorkeeper_application, owner: company, scopes: :rescue
        end

        it_behaves_like 'required_features'
      end
    end

    describe 'required_roles' do
      context 'without any required_roles' do
        let(:roles) { ["foo"] }

        it "works" do
          expect(subject.call(schema_member, context)).to be_truthy
        end
      end

      context 'with required_roles' do
        context 'required :any role' do
          let(:required_roles) { :any }

          context 'with a role' do
            let(:roles) { ["foo"] }

            it "works" do
              expect(subject.call(schema_member, context)).to be_truthy
            end
          end

          context 'without a role' do
            let(:roles) { [] }

            it "works" do
              expect(subject.call(schema_member, context)).to be_falsey
            end
          end
        end

        context 'with a single matching role' do
          let(:roles) { ["foo"] }
          let(:required_roles) { :foo }

          it "works" do
            expect(subject.call(schema_member, context)).to be_truthy
          end
        end

        context 'without a single matching role' do
          let(:required_roles) { :foo }

          it "works" do
            expect(subject.call(schema_member, context)).to be_falsey
          end
        end

        context 'with a set of matching roles' do
          let(:roles) { ["foo", "bar", "baz"] }
          let(:required_roles) { [:bar, :foo] }

          it "works" do
            expect(subject.call(schema_member, context)).to be_truthy
          end
        end

        context 'without a set of matching roles' do
          let(:roles) { ["foo", "bar", "baz"] }
          let(:required_roles) { [:bar, :bat] }

          it "works" do
            expect(subject.call(schema_member, context)).to be_falsey
          end
        end

        context 'with a root role' do
          let(:roles) { ["root"] }
          let(:required_roles) { [:bar, :bat] }

          it "works" do
            expect(subject.call(schema_member, context)).to be_truthy
          end
        end

        context 'with a positive proc' do
          let(:roles) { ["foo", "bar", "baz"] }
          let(:required_roles) { -> (roles) { roles.include?(:foo) } }

          it "works" do
            expect(subject.call(schema_member, context)).to be_truthy
          end
        end

        context 'with a negative proc' do
          let(:roles) { ["foo", "bar", "baz"] }
          let(:required_roles) { -> (roles) { roles.include?(:asdf) } }

          it "works" do
            expect(subject.call(schema_member, context)).to be_falsey
          end
        end
      end
    end

    describe "if_company" do
      let(:company) { create(:fleet_company) }

      context "when company method returns true" do
        let(:if_company) { :persisted? }

        it "returns true" do
          expect(subject.call(schema_member, context)).to eq true
        end
      end

      context "when some methods return false" do
        let(:if_company) { [:new_record?, :persisted?] }

        it "returns false" do
          expect(subject.call(schema_member, context)).to eq false
        end
      end

      context "whern the method doesn't exist" do
        let(:if_company) { :flurgenturm }

        it 'returns false' do
          expect(subject.call(schema_member, context)).to eq false
        end
      end

      context 'with a positive proc' do
        let(:if_company) { -> (company) { company.present? } }

        it "works" do
          expect(subject.call(schema_member, context)).to be_truthy
        end
      end

      context 'with a negative proc' do
        let(:if_company) { -> (company) { company.blank? } }

        it "works" do
          expect(subject.call(schema_member, context)).to be_falsey
        end
      end
    end

    describe "if_context" do
      context "when context method returns true" do
        let(:if_context) { :present? }

        it "returns true" do
          expect(subject.call(schema_member, context)).to be(true)
        end
      end

      context "when some methods return false" do
        let(:if_context) { [:present?, :blank?] }

        it "returns false" do
          expect(subject.call(schema_member, context)).to be(false)
        end
      end

      context "whern the method doesn't exist" do
        let(:if_context) { :flurgenturm }

        it 'returns false' do
          expect(subject.call(schema_member, context)).to be(false)
        end
      end

      context 'with a positive proc' do
        let(:if_context) { -> (ctx) { ctx.present? } }

        it "works" do
          expect(subject.call(schema_member, context)).to be(true)
        end
      end

      context 'with a negative proc' do
        let(:if_context) { -> (ctx) { ctx.blank? } }

        it "works" do
          expect(subject.call(schema_member, context)).to be(false)
        end
      end
    end
  end
end
