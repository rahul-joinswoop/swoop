# frozen_string_literal: true

require 'rails_helper'

describe GraphQL::AuthenticateClient, type: :interactor do
  subject do
    GraphQL::AuthenticateClient.call(input: { request: request })
  end

  let(:request_id) { SecureRandom.uuid }
  let(:operation_id) { SecureRandom.uuid }
  let(:company) { create(:fleet_company) }
  let(:user) { create(:user, company: company) }
  let(:access_token) do
    create :doorkeeper_access_token,
           resource_owner_id: user.id,
           scopes: user.roles.map(&:name)
  end
  let(:headers) do
    {
      'HTTP_USER_AGENT' => 'ios SwoopMobile/9 CFNetwork/808.3 Darwin/16.3.0',
      'HTTP_REFERER' => 'http://example.com/',
    }
  end

  let(:request) do
    # rubocop:disable Rails/SaveBang
    request = ActionDispatch::TestRequest.create
    # rubocop:enable Rails/SaveBang

    request.env.merge!(headers) if headers
    request.env['HTTP_AUTHORIZATION'] = "Bearer #{access_token.token}" if access_token
    request.request_id = request_id
    request.params[:operationId] = operation_id
    request
  end

  describe "authorization" do
    context "with a good token" do
      it "works" do
        expect(subject).to be_success
      end

      it 'sets access_token' do
        expect(subject.output[:context][:api_access_token]).to eq(access_token)
      end
    end

    context "without a token" do
      let(:access_token) { nil }

      it "fails" do
        expect(subject).not_to be_success
      end
    end

    context "with an expired token" do
      it "fails" do
        access_token.update!(expires_in: 20, created_at: 1.day.ago)
        expect(subject).not_to be_success
      end
    end

    context "with a revoked token" do
      it "fails" do
        access_token.update! revoked_at: Time.current
        expect(subject).not_to be_success
      end
    end

    context "with a token from an oauth application" do
      let(:access_token) do
        create :doorkeeper_access_token,
               :with_application,
               scopes: :fleet
      end

      it "works" do
        expect(subject).to be_success
      end

      context "without an owner" do
        let(:application) do
          create :doorkeeper_application,
                 name: Faker::Hipster.sentence,
                 owner: nil,
                 scopes: :fleet
        end
        let(:access_token) do
          create :doorkeeper_access_token,
                 application: application,
                 scopes: :fleet
        end

        it "fails" do
          expect(subject).not_to be_success
          expect(subject.message).to eq("invalid application")
        end
      end
    end
  end

  describe "viewer" do
    context 'with a user + company' do
      it 'works' do
        expect(subject).to be_success
        expect(subject.output[:context][:viewer]).to eq(user)
      end
    end

    context 'with a user and no company' do
      let(:company) { nil }

      it 'works' do
        expect(subject).to be_success
        expect(subject.output[:context][:viewer]).to eq(user)
      end
    end

    context 'with an application and company' do
      let(:application) { create :doorkeeper_application, owner: company }
      let(:access_token) do
        create :doorkeeper_access_token, application: application, scopes: :fleet
      end

      it 'works' do
        expect(subject).to be_success
        expect(subject.output[:context][:viewer]).to eq(application)
      end
    end
  end

  describe 'headers and request' do
    it 'sets context[:request_id] accordingly' do
      expect(subject).to be_success
      expect(subject.output[:context][:request_id]).to eq request_id
    end

    it 'sets context[:operation_id] accordingly' do
      expect(subject).to be_success
      expect(subject.output[:context][:operation_id]).to eq operation_id
    end

    it 'sets context[:headers] accordingly' do
      expect(subject).to be_success
      expect(subject.output[:context][:headers]).to match(headers)
    end

    context 'with graphql subscription headers' do
      let(:webhook_url) { "http://example.com/callback" }
      let(:webhook_secret) { "foobar" }
      let(:headers) do
        {
          ::Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_URL => webhook_url,
          ::Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET => webhook_secret,
        }
      end

      it 'adds the webhook URL and secret headers' do
        expect(subject).to be_success
        expect(subject.output[:context][:webhook_url]).to eq webhook_url
        expect(subject.output[:context][:webhook_secret]).to eq webhook_secret
      end
    end
  end
end
