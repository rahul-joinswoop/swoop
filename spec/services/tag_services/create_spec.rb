# frozen_string_literal: true

require "rails_helper"

describe TagServices::Create do
  subject(:service_call) do
    TagServices::Create.call(
      input: {
        api_company_id: company.id,
        tag_attributes: tag_attributes,
      }
    )
  end

  let!(:company) { create(:fleet_company, :tesla) }
  let(:tag_attributes) do
    {
      name: tag_name,
    }
  end

  let(:tag_name) { 'Out of Network' }

  def created_tag
    Tag.find_by!(
      company: company,
      name: tag_name
    )
  end

  it 'creates the Tag' do
    service_call

    expect(created_tag).to be_present
  end

  context 'when Tag is deleted' do
    let(:tag_name) { 'Whatever' }

    let!(:tag) do
      create(:tag, company: company, name: tag_name, deleted_at: Time.now)
    end

    it 'sets deleted_at as nil' do
      expect(created_tag.deleted_at).to be_present

      service_call

      expect(created_tag.deleted_at).to be_nil
    end

    it 'does not duplicate it' do
      service_call

      tags = Tag.where(
        company: company,
        name: tag_name
      )

      expect(tags.size).to eq 1
    end
  end
end
