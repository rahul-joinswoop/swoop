# frozen_string_literal: true

require "rails_helper"

describe RateService::FindDefaultsAndCopy do
  subject(:service_call) do
    RateService::FindDefaultsAndCopy.call(
      input: {
        api_company_id: api_company.id,
        rate_attributes_filter_hash: rate_attributes_filter_hash,
      }
    )
  end

  let(:api_company) { create(:rescue_company, :finish_line) }

  context 'when default rates exist for Default account' do
    context 'when adding a new Account on Rates table' do
      let(:rate_attributes_filter_hash) do
        {
          account_id: account.id,
        }
      end

      let(:account) { create(:account, name: 'A new account added in Rates table', company: api_company) }

      before do
        create(:rate,
               company: api_company,
               type: 'HoursP2PRate',
               account: nil,
               site: nil,
               vendor_id: nil,
               service_code: nil,
               hourly: 32.0)
      end

      it 'creates default rates for this new Account' do
        rates_for_new_service = Rate.where(
          account: account,
          site: nil,
          vendor_id: nil,
          company: api_company,
          service_code: nil,
          deleted_at: nil
        )

        expect(rates_for_new_service.size).to eq 0

        output = service_call.output

        expect(output.first.type).to eq 'HoursP2PRate'
        expect(output.first.company.id).to eq api_company.id
        expect(output.first.service_code_id).to be_nil
        expect(output.first.account_id).to eq account.id
        expect(output.first.site_id).to be_nil
        expect(output.first.vendor_id).to be_nil
        expect(output.first.hourly.to_s).to eq "32.0"
      end
    end

    context 'when adding a new rate for Default Account and a specific service' do
      let(:rate_attributes_filter_hash) do
        {
          account_id: nil,
          service_code_id: new_service.id,
        }
      end

      let(:new_service) { create(:service_code, name: 'Tire Change') }

      before do
        create(:rate,
               company: api_company,
               type: 'HoursP2PRate',
               account: nil,
               site: nil,
               vendor_id: nil,
               service_code: nil,
               hourly: 32.0)
      end

      it 'creates the default rates for the new service' do
        rates_for_new_service = Rate.where(
          account: nil,
          site: nil,
          vendor_id: nil,
          company: api_company,
          service_code: new_service,
          deleted_at: nil
        )

        expect(rates_for_new_service.size).to eq 0

        output = service_call.output

        expect(output.first.type).to eq 'HoursP2PRate'
        expect(output.first.company.id).to eq api_company.id
        expect(output.first.service_code_id).to eq new_service.id
        expect(output.first.account_id).to be_nil
        expect(output.first.site_id).to be_nil
        expect(output.first.vendor_id).to be_nil
        expect(output.first.hourly.to_s).to eq "32.0"
      end
    end

    context 'when adding a new rate for a specific Account and a specific service' do
      let(:rate_attributes_filter_hash) do
        {
          account_id: account.id,
          service_code_id: new_service.id,
        }
      end

      let(:account) { create(:account, name: 'Cash Call', company: api_company) }

      let(:new_service) { create(:service_code, name: 'Tire Change') }

      before do
        create(:rate,
               company: api_company,
               type: 'HoursP2PRate',
               account: account,
               site: nil,
               vendor_id: nil,
               service_code: nil,
               hourly: 32.0)
      end

      it 'creates default rate for the new service' do
        rates_for_new_service = Rate.where(
          account: account,
          company: api_company,
          service_code: new_service,
          deleted_at: nil
        )

        expect(rates_for_new_service.size).to eq 0

        output = service_call.output

        expect(output.first.type).to eq 'HoursP2PRate'
        expect(output.first.company.id).to eq api_company.id
        expect(output.first.service_code_id).to eq new_service.id
        expect(output.first.account_id).to eq account.id
        expect(output.first.site_id).to be_nil
        expect(output.first.vendor_id).to be_nil
      end
    end

    context 'when adding a new rate for a specific Account, a specific site and a specific service' do
      let(:rate_attributes_filter_hash) do
        {
          account_id: account.id,
          site_id: site.id,
          service_code_id: new_service.id,
        }
      end

      let(:account) { create(:account, name: 'Cash Call', company: api_company) }

      let(:site) { create(:site, company: api_company) }

      let(:new_service) { create(:service_code, name: 'Tire Change') }

      before do
        create(:rate,
               company: api_company,
               type: 'HoursP2PRate',
               account: account,
               site: site,
               vendor_id: nil,
               service_code: nil,
               hourly: 32.0)
      end

      it 'creates default rate for the new service' do
        rates_for_new_service = Rate.where(
          account: account,
          site: site,
          vendor_id: nil,
          company: api_company,
          service_code: new_service,
          deleted_at: nil
        )

        expect(rates_for_new_service.size).to eq 0

        output = service_call.output

        expect(output.first.type).to eq 'HoursP2PRate'
        expect(output.first.company.id).to eq api_company.id
        expect(output.first.service_code_id).to eq new_service.id
        expect(output.first.account_id).to eq account.id
        expect(output.first.site_id).to eq site.id
        expect(output.first.vendor_id).to be_nil
      end

      context 'when the site_id doesnt have default rates' do
        let(:rate_attributes_filter_hash) do
          {
            account_id: account.id,
            site_id: site.id,
            service_code_id: new_service.id,
          }
        end

        let(:account) { create(:account, name: 'A new account', company: api_company) }

        let(:site) { create(:site, company: api_company, name: 'A new site') }

        let(:new_service) { create(:service_code, name: 'Tire Change') }

        before do
          create(:rate,
                 company: api_company,
                 type: 'HoursP2PRate',
                 account: account,
                 site: nil,
                 vendor_id: nil,
                 service_code: nil,
                 hourly: 32.0)
        end

        it 'goes up one level and creates new rates based on account defaults' do
          rates_for_new_service = Rate.where(
            account: account,
            site: site,
            vendor_id: nil,
            company: api_company,
            service_code: new_service,
            deleted_at: nil
          )

          expect(rates_for_new_service.size).to eq 0

          output = service_call.output

          expect(output.first.type).to eq 'HoursP2PRate'
          expect(output.first.company.id).to eq api_company.id
          expect(output.first.service_code_id).to eq new_service.id
          expect(output.first.account_id).to eq account.id
          expect(output.first.site_id).to eq site.id
          expect(output.first.vendor_id).to be_nil
          expect(output.first.hourly.to_s).to eq "32.0"
        end
      end
    end

    context 'when adding a new rate for a specific Account, a specific site, a specific vendor_id and a specific service' do
      let(:rate_attributes_filter_hash) do
        {
          account_id: account.id,
          site_id: site.id,
          vendor_id: vendor_id,
          service_code_id: new_service.id,
        }
      end

      let(:account) { create(:account, name: 'Cash Call', company: api_company) }

      let(:site) { create(:site, company: api_company) }

      let(:vendor_id) { '123456' }

      let(:new_service) { create(:service_code, name: 'Tire Change') }

      before do
        create(:rate,
               company: api_company,
               type: 'HoursP2PRate',
               account: account,
               site: site,
               vendor_id: vendor_id,
               service_code: nil,
               hourly: 88.0)
      end

      it 'creates default rate for the new service' do
        rates_for_new_service = Rate.where(
          account: account,
          site: site,
          vendor_id: vendor_id,
          company: api_company,
          service_code: new_service,
          deleted_at: nil
        )

        expect(rates_for_new_service.size).to eq 0

        output = service_call.output

        expect(output.first.type).to eq 'HoursP2PRate'
        expect(output.first.company.id).to eq api_company.id
        expect(output.first.service_code_id).to eq new_service.id
        expect(output.first.account_id).to eq account.id
        expect(output.first.site_id).to eq site.id
        expect(output.first.vendor_id).to eq '123456'
        expect(output.first.hourly.to_s).to eq "88.0"
      end
    end

    context 'when the vendor_id doesnt have default rates' do
      let(:rate_attributes_filter_hash) do
        {
          account_id: account.id,
          site_id: site.id,
          vendor_id: vendor_id,
          service_code_id: new_service.id,
        }
      end

      let(:account) { create(:account, name: 'A fresh new one', company: api_company) }

      let(:site) { create(:site, company: api_company, name: 'Another again') }

      let(:vendor_id) { '334422' }

      let(:new_service) { create(:service_code, name: 'Tire Change') }

      before do
        create(:rate,
               company: api_company,
               type: 'HoursP2PRate',
               account: account,
               site: site,
               vendor_id: nil,
               service_code: nil,
               hourly: 12.0)
      end

      it 'goes up one level and creates new rates based on site defaults' do
        rates_for_new_service = Rate.where(
          account: account,
          site: site,
          vendor_id: vendor_id,
          company: api_company,
          service_code: new_service,
          deleted_at: nil
        )

        expect(rates_for_new_service.size).to eq 0

        output = service_call.output

        expect(output.first.type).to eq 'HoursP2PRate'
        expect(output.first.company.id).to eq api_company.id
        expect(output.first.service_code_id).to eq new_service.id
        expect(output.first.account_id).to eq account.id
        expect(output.first.site_id).to eq site.id
        expect(output.first.vendor_id).to eq '334422'
        expect(output.first.hourly.to_s).to eq "12.0"
      end
    end
  end

  context 'when default rates exist for a specific Account' do
    context 'when adding a new site_id for this specific Account' do
      let(:rate_attributes_filter_hash) do
        {
          account_id: account.id,
          site_id: site.id,
        }
      end

      let(:account) { create(:account, name: 'Cash Call', company: api_company) }

      let(:site) { create(:site, company: api_company) }

      before do
        create(:rate,
               company: api_company,
               type: 'HoursP2PRate',
               account: account,
               site: nil,
               vendor_id: nil,
               service_code: nil,
               hourly: 42.0)
      end

      it 'creates existent default rates for the new site_id' do
        rates_for_new_service = Rate.where(
          account: account.id,
          site: site,
          vendor_id: nil,
          company: api_company,
          service_code: nil,
          deleted_at: nil
        )

        expect(rates_for_new_service.size).to eq 0

        output = service_call.output

        expect(output.first.type).to eq 'HoursP2PRate'
        expect(output.first.company.id).to eq api_company.id
        expect(output.first.service_code_id).to be_nil
        expect(output.first.account_id).to eq account.id
        expect(output.first.site_id).to eq site.id
        expect(output.first.vendor_id).to be_nil
        expect(output.first.hourly.to_s).to eq "42.0"
      end
    end
  end

  context 'when default rates exist for a specific Account and a specific site' do
    context 'when adding a new vendor_id for this specific Account and site' do
      let(:rate_attributes_filter_hash) do
        {
          account_id: account.id,
          site_id: site.id,
          vendor_id: vendor_id,
        }
      end

      let(:account) { create(:account, name: 'Cash Call', company: api_company) }

      let(:site) { create(:site, company: api_company) }

      let(:vendor_id) { '102030' }

      before do
        create(:rate,
               company: api_company,
               type: 'HoursP2PRate',
               account: account,
               site: site,
               vendor_id: nil,
               service_code: nil,
               hourly: 42.0)
      end

      it 'creates existent default rates for the new vendor_id' do
        rates_for_new_service = Rate.where(
          account: account,
          site: site,
          vendor_id: vendor_id,
          company: api_company,
          service_code: nil,
          deleted_at: nil
        )

        expect(rates_for_new_service.size).to eq 0

        output = service_call.output

        expect(output.first.type).to eq 'HoursP2PRate'
        expect(output.first.company.id).to eq api_company.id
        expect(output.first.service_code_id).to be_nil
        expect(output.first.account_id).to eq account.id
        expect(output.first.site_id).to eq site.id
        expect(output.first.vendor_id).to eq '102030'
        expect(output.first.hourly.to_s).to eq "42.0"
      end
    end
  end
end
