# frozen_string_literal: true

require "spec_helper"
require "./app/services/swoop_service"
require "./app/services/rate_service/create_multiple"

describe RateService::CreateMultiple do
  let(:rates_attributes) do
    [
      {
        flat: "95.00",
        vehicle_category_id: "6",
        service_code_id: nil,
        account_id: 4,
        vendor_id: 'CA123456',
        type: "FlatRate",
      },
    ]
  end
  let(:company) { double "Company" }
  let(:account) { double "Account" }
  let(:rate) do
    double(
      "Rate",
      site_id: 100, account_id: 123, service_code_id: 200,
      vehicle_category_id: 300, type: "SomeRateType", vendor_id: 'CA123456'
    ).as_null_object
  end
  let(:logger) { double "Logger", debug: true }

  before do
    class_double("Rails").as_stubbed_const
    allow(Rails).to receive(:logger).and_return(logger)

    class_double("Rate").as_stubbed_const
    allow(Rate).to receive(:new).and_return(rate)
  end

  describe "#execute" do
    subject(:execute) do
      RateService::CreateMultiple.new(
        rates: rates_attributes, company: company, account: account
      ).execute
    end

    it "raises ArgumentError when a new rate is a duplicate" do
      allow(rate).to receive(:duplicate?).and_return true

      expect { execute }.to raise_error(
        ArgumentError,
        "check_unique - Found existing rate for site(100), vendor_id(CA123456), account(123), " \
        "service(200, rate(SomeRateType), vehicle_category_id(300))"
      )
    end
  end
end
