# frozen_string_literal: true

require "rails_helper"

describe RateService::MailRateChange do
  subject(:mail_rate_change) do
    described_class.new(
      company: company,
      user: user,
      company_to_be_notified: company_to_be_notified
    )
  end

  let(:user)    { build(:user, id: 1) }

  let(:company) do
    build(:company, id: 1, accounting_email: company_accounting_email)
  end

  let(:company_accounting_email) { 'company_accounting_email@email.com' }

  let(:company_to_be_notified) do
    build(
      :company,
      id: 2,
      accounting_email: company_to_be_notified_accounting_email
    )
  end

  let(:company_to_be_notified_accounting_email) { 'company_to_be_notified@email.com' }

  describe '.initialize' do
    it 'sets @company' do
      expect(mail_rate_change.instance_variable_get('@company')).to eq company
    end

    it 'sets @user' do
      expect(mail_rate_change.instance_variable_get('@user')).to eq user
    end

    it 'sets @company_to_be_notified' do
      expect(mail_rate_change.instance_variable_get('@company_to_be_notified')).to(
        eq company_to_be_notified
      )
    end
  end

  describe '#execute' do
    it 'returns nil' do
      expect(mail_rate_change.execute).to eq nil
    end
  end

  describe '#after_execution' do
    before :each do
      allow(Delayed::Job).to receive(:enqueue)
    end

    it 'schedule change_rate_email to be executed asynchronously' do
      expect(Delayed::Job).to receive(:enqueue)

      mail_rate_change.after_execution
    end

    context 'when company_to_be_notified.accounting_email.blank?' do
      let(:company_to_be_notified_accounting_email) { nil }

      before :each do
        allow_any_instance_of(ActiveSupport::Logger).to receive(:error)
      end

      it 'logs an error' do
        expect_any_instance_of(ActiveSupport::Logger). to receive(:error).with(
          I18n.t('rate.change_email_not_sent',
                 company_name: company_to_be_notified.name, company_id: company_to_be_notified.id)
        )

        mail_rate_change.after_execution
      end

      it 'returns nil' do
        expect(mail_rate_change.after_execution).to eq nil
      end

      it 'doesn\'t schedule change_rate_email to be executed asynchronously' do
        expect(Delayed::Job).not_to receive(:enqueue)

        mail_rate_change.after_execution
      end
    end

    context 'when company.accounting_email.blank?' do
      let(:company_accounting_email) { nil }

      before :each do
        allow_any_instance_of(ActiveSupport::Logger).to receive(:error)
        allow(Delayed::Job).to receive(:enqueue)
      end

      it 'logs an error' do
        expect_any_instance_of(ActiveSupport::Logger). to receive(:error).with(
          I18n.t('rate.company_with_no_accounting_email',
                 company_name: company.name, company_id: company.id)
        )

        mail_rate_change.after_execution
      end

      it 'schedule change_rate_email to be executed asynchronously' do
        expect(Delayed::Job).to receive(:enqueue)

        mail_rate_change.after_execution
      end
    end
  end
end
