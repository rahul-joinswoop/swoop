# frozen_string_literal: true

require "rails_helper"

describe InvoicePdfGenerator do
  subject { InvoicePdfGenerator.new invoice, pdf_generator: WickedPdf }

  let(:invoice) { create :fleet_in_house_job_invoice }
  let!(:audit_job_status) do
    create :audit_job_status, job_status: create(:job_status, :towing), job: invoice.job
  end
  let!(:account) do
    a = create :account, company: invoice.sender
    invoice.job.update! account: a
    a
  end

  it "works" do
    expect { subject.generate }.not_to raise_error
  end

  it "Works when customer has no phone" do
    invoice.job.customer.update!(phone: nil)
    expect { subject.generate }.not_to raise_error
  end
end
