# frozen_string_literal: true

require "rails_helper"

describe JobService::CreateStorage do
  subject do
    described_class.call(job: job, user: user)
  end

  let!(:company) { create(:rescue_company, :with_storage) }
  let!(:site) { create(:site) }
  let!(:drop_location) { create(:location, site: site) }
  let!(:job) do
    create(:job,
           :completed,
           :with_partner_new_invoice,
           :with_rescue_vehicle,
           fleet_company: company,
           rescue_company: company,
           will_store: true,
           drop_location: drop_location)
  end
  let!(:user) { create(:user) }

  context 'Adding storage first time' do
    before do
      job.save!
      job.reload
    end

    it "creates storage for job" do
      expect { subject }.to change(job, :storage).from(nil).to be_instance_of(InventoryItem)
    end

    it "Sets the job status as stored" do
      expect { subject }.to change(job, :status).from("completed").to("stored")
    end

    it 'Adds an AJS storage entry' do
      expect(job.audit_job_statuses.size).to eq(1)
      subject && job.reload
      expect(job.audit_job_statuses.size).to eq(2)
      stored_ajses = job.audit_job_statuses.where(job_status_id: JobStatus::STORED, deleted_at: nil)
      expect(stored_ajses.length).to eq(1)
    end

    it 'triggers invoice re-calculation' do
      subject
      expect(CreateOrUpdateInvoice).to have_enqueued_sidekiq_job(job.id)
    end

    context "when user is nil" do
      let(:user) { nil }

      it "raises appropriate exception" do
        expect { subject }.to raise_error(
          JobService::CreateStorage::MissingStorageUser
        )
      end
    end
  end

  context "when user removed storage from the job" do
    let(:deleted_time) { Time.now }
    let!(:deleted_storage_ajs) do
      create(:audit_job_status, {
        job: job,
        job_status_id: JobStatus::STORED,
        deleted_at: deleted_time,
      })
    end

    let!(:inventory_item) do
      create(:inventory_item, :with_vehicle, :not_released, job: job, deleted_at: Time.now)
    end

    before do
      job.reload
    end

    it "allows storing the vehicle again" do
      expect { subject }.not_to raise_error
    end

    it "does not create duplicate live storage when adding" do
      expect(InventoryItem.count).to eq(1)
      expect(job.storage).to be_nil

      subject

      job.save! && job.reload
      expect(InventoryItem.count).to eq(2)
      expect(job.storage).to be_instance_of(InventoryItem)
      expect(job.storage).not_to be(inventory_item)
    end

    it 'undeletes the AJS' do
      expect { subject && deleted_storage_ajs.reload }.to change(deleted_storage_ajs, :deleted_at).from(deleted_time).to(nil)
    end

    it 'Does not change the AJS count' do
      expect { subject }.to avoid_changing(AuditJobStatus, :count)
    end
  end
end
