# frozen_string_literal: true

require "rails_helper"

describe JobService::StatusChange do
  subject(:status_change_call) do
    described_class.call(job: job, new_status: new_status, ajs: ajs)
    job.save!
  end

  let(:job) { create(:fleet_managed_job, status: :draft) }
  let(:job_status) { create(:job_status, :canceled) }
  let(:new_status) { 'Canceled' }
  let(:ajs) { nil }

  context 'when an AuditJobStatus object provided' do
    let(:ajs) do
      create(:audit_job_status,
             last_set_dttm: 3.days.ago,
             job_status: job_status)
    end

    it 'updates the last set dttm' do
      expect { status_change_call }.to change(ajs, :last_set_dttm)
    end

    it 'calls UpdateLastUpdatedStatusOnJob service' do
      expect_any_instance_of(UpdateLastUpdatedStatusOnJob).to receive(:call)

      status_change_call
    end
  end

  context 'when an AuditJobStatus object is not provided' do
    let(:ajs) { nil }

    it 'creates a new ajs' do
      expect { status_change_call }.to change(AuditJobStatus, :count).by(1)
    end

    it 'calls UpdateLastUpdatedStatusOnJob service' do
      expect_any_instance_of(UpdateLastUpdatedStatusOnJob).to receive(:call)

      status_change_call
    end
  end

  context 'existing deleted', freeze_time: true do
    let(:ajs) do
      create(:audit_job_status,
             last_set_dttm: 3.days.ago,
             job_status: job_status,
             deleted_at: Time.current)
    end

    it 'undeletes an exisitng ajs' do
      expect { status_change_call }.to change(ajs, :deleted_at).from(Time.current).to(nil)
    end
  end
end
