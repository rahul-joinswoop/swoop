# frozen_string_literal: true

require "rails_helper"

describe JobService::FleetInhouseAssignRescueProviderService do
  subject(:service_call) do
    described_class.new(job, rescue_company, site).call
  end

  let(:fleet_in_house) { create(:fleet_in_house_company) }
  let(:job) { create(:fleet_in_house_job, fleet_company: fleet_in_house, status: 'unassigned', scheduled_for: scheduled_for) }

  let(:rescue_company) { create(:rescue_company, live: partner_live) }
  let!(:site) { create(:partner_site, company: rescue_company, always_open: site_always_open) }

  let(:partner_live) { false }
  let(:site_always_open) { false }
  let(:expected_driver_id) { nil }
  let(:scheduled_for) { nil }

  let!(:dispatch_to_truck_feature) { create(:feature, name: Feature::PARTNER_DISPATCH_TO_TRUCK) }

  shared_context "partner has Feature::PARTNER_DISPATCH_TO_TRUCK enabled", shared_context: :metadata do
    before do
      rescue_company.features << dispatch_to_truck_feature

      rescue_company.save!
    end
  end

  shared_examples 'force job.status to dispatched' do
    include_context "partner has Feature::PARTNER_DISPATCH_TO_TRUCK enabled"

    include_examples "validate job status change", :unassigned, :dispatched
  end

  shared_examples 'job is assigned to a rescue_driver' do
    it 'has expected rescue_driver' do
      expect { service_call }.to change(job, :rescue_driver_id).from(nil).to(instance_of(Integer))
    end
  end

  # here we emphasize that when partner is not live, it doesn't matter if
  # the site is opened or closed: the job.status will always land as 'dispatched'
  context 'when partner is not live' do
    context 'when site is closed' do
      include_examples "validate job status change", :unassigned, :dispatched

      context 'when partner has Feature::PARTNER_DISPATCH_TO_TRUCK enabled' do
        it_behaves_like 'force job.status to dispatched'
      end

      context 'when job is scheduled' do
        let(:scheduled_for) { Time.now + 1.day }

        include_examples "validate job status change", :unassigned, :pending
      end
    end

    context 'when site is open' do
      let(:site_always_open) { true }

      include_examples "validate job status change", :unassigned, :dispatched

      context 'when partner has Feature::PARTNER_DISPATCH_TO_TRUCK enabled' do
        it_behaves_like 'force job.status to dispatched'
      end

      context 'when job is scheduled' do
        let(:scheduled_for) { Time.now + 1.day }

        include_examples "validate job status change", :unassigned, :pending
      end
    end
  end

  context 'when partner is live' do
    let(:partner_live) { true }

    context 'when site is closed' do
      include_examples "validate job status change", :unassigned, :dispatched

      context 'when partner has Feature::PARTNER_DISPATCH_TO_TRUCK enabled' do
        it_behaves_like 'force job.status to dispatched'
      end

      context 'when job is scheduled' do
        let(:scheduled_for) { Time.now + 1.day }

        include_examples "validate job status change", :unassigned, :pending
      end
    end

    context 'when site is open' do
      let(:site_always_open) { true }

      include_examples "validate job status change", :unassigned, :assigned

      context 'when partner has Feature::PARTNER_DISPATCH_TO_TRUCK enabled' do
        it_behaves_like 'force job.status to dispatched'
      end

      context 'job is scheduled' do
        let(:scheduled_for) { Time.now + 1.day }

        include_examples "validate job status change", :unassigned, :assigned
      end
    end
  end
end
