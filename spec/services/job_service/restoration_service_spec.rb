# frozen_string_literal: true

require "rails_helper"

describe JobService::RestorationService do
  subject { described_class.call(job: job) }

  context 'when job has been deleted' do
    let!(:job) { FactoryBot.create(:fleet_managed_job, :with_invoice, status: :draft) }
    let!(:invoice) { job.invoice }

    context 'when previous job statuses exist' do
      before do
        SwoopUpdateJobState.new(job, 'Canceled').call
        job.save!

        SwoopUpdateJobState.new(job, 'Deleted').call
        job.save!
      end

      it 'nullifies Job#deleted_at' do
        expect(job.deleted_at).not_to eq(nil)

        expect { subject }.to change(job, :deleted_at).to(nil)
      end

      it 'reverts job status to previous valid state' do
        subject

        expect(job.status).to eq(Job::STATUS_CANCELED)
      end

      it 'restores invoice' do
        expect(invoice.reload.deleted_at).to be_present

        expect { subject }.to change { invoice.reload.deleted_at }.to(nil)
      end

      context 'when invoice does not exist' do
        let(:invoice) { nil }

        it 'is successful' do
          expect { subject }.to change(job, :deleted_at).to(nil)
        end
      end
    end

    context 'when previous status does not exist' do
      it 'raises exception indicating malformed job status' do
        expect { subject }.to raise_error(JobService::RestorationService::NoValidPreviousStatus)
      end
    end
  end
end
