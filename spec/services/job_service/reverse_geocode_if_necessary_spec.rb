# frozen_string_literal: true

require "rails_helper"

describe JobService::ReverseGeocodeIfNecessary do
  subject do
    JobService::ReverseGeocodeIfNecessary.new(job).call
  end

  let(:job) do
    create(:fleet_managed_job, {
      service_location: service_location,
      drop_location: drop_location,
    })
  end

  context 'no locations' do
    let(:service_location) { nil }
    let(:drop_location) { nil }

    it 'does not explode' do
      expect { subject }.not_to raise_exception
    end
  end

  context 'lat lng only' do
    let(:service_location) do
      Location.create(lat: 123, lng: 456)
    end
    let(:drop_location) do
      Location.create(lat: 78, lng: 90)
    end

    it 'queues a places resolver' do
      expect(PlacesResolver).to receive(:perform_async).with(service_location.id)
      expect(PlacesResolver).to receive(:perform_async).with(drop_location.id)
      subject
    end
  end

  context 'lat lng and address' do
    let(:service_location) do
      Location.create(lat: 123, lng: 456, address: '101 highway')
    end
    let(:drop_location) do
      Location.create(lat: 78, lng: 90, address: '123 home ave')
    end

    it 'does not queue a places resolver' do
      expect(PlacesResolver).not_to receive(:perform_async).with(service_location.id)
      expect(PlacesResolver).not_to receive(:perform_async).with(drop_location.id)
      subject
    end
  end
end
