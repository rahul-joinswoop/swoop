# frozen_string_literal: true

require 'rails_helper'

describe JobService::RemoveStorage do
  subject { described_class.call(job: job) }

  describe '.call' do
    let!(:job) { create(:rescue_job) }
    let!(:job_status) { create(:job_status, :stored) }
    let!(:ajs) { create(:audit_job_status, job: job, job_status: job_status) }

    before do
      job.audit_job_statuses << ajs
    end

    it 'updates will_store' do
      expect { subject }.to change(job, :will_store).to(false)
    end

    it 'updates status' do
      expect { subject }.to change(job, :status).to(Job::STATUS_COMPLETED)
    end

    it 'does not update job' do
      expect { subject }.to avoid_changing(job, :updated_at)
    end

    it 'assigns a deleted_at to the audit_job_status' do
      expect { subject }.to change { ajs.reload.deleted_at }.from(nil)
    end
  end
end
