# frozen_string_literal: true

require "rails_helper"

describe JobService::ChangePaymentType do
  subject(:service) do
    JobService::ChangePaymentType.new(
      job: job, old_payment_type: current_payment_type
    )
  end

  describe ".initialize" do
    let(:job) { build_stubbed :job }
    let(:old_payment_type) { build_stubbed :customer_type }

    it "raises ArgumentError without :job" do
      expect do
        JobService::ChangePaymentType.new(old_payment_type: old_payment_type)
      end.to raise_error ArgumentError
    end

    it "raises ArgumentError without :old_payment_type" do
      expect do
        JobService::ChangePaymentType.new(job: job)
      end.to raise_error ArgumentError
    end
  end

  describe "#call" do
    subject(:service_call) { service.call }

    let(:job) { build_stubbed :job }
    let(:invoice) { build :invoice }
    let(:allowed_state) { Invoice::PARTNER_APPROVED }

    before do
      allow(job).to receive(:invoice).and_return(invoice)
      allow(job).to receive(:customer_type).and_return(new_payment_type)

      allow(invoice).to receive(:partner_new!)
      allow(invoice).to receive(:state).and_return(allowed_state)
    end

    shared_examples "requires invoice transition" \
      do |new_payment_type_name, payment_types_to_require_invoice_transition|

      let(:new_payment_type) do
        build_stubbed :customer_type, name: new_payment_type_name
      end

      payment_types_to_require_invoice_transition.each do |payment_type_name|
        context "when current payment type is #{payment_type_name}" do
          let(:current_payment_type) do
            build_stubbed :customer_type, name: payment_type_name
          end

          it "calls invoice transition to new_partner" do
            expect(invoice).to receive(:partner_new!)
            service_call
          end
        end
      end

      context "when current payment type is not valid" do
        let(:current_payment_type) do
          build_stubbed :customer_type, name: "Whatever"
        end

        it "does not call #partner_new! on the associated invoice" do
          expect(invoice).not_to receive(:partner_new!)
          service_call
        end
      end

      context "when invoice transition is not allowed" do
        let(:current_payment_type) do
          build_stubbed :customer_type,
                        name: payment_types_to_require_invoice_transition[0]
        end

        before do
          allow(invoice)
            .to receive(:partner_new!).and_raise("AASM::InvalidTransition")
        end

        it "raises AASM::InvalidTransition" do
          expect { service_call }.to raise_error "AASM::InvalidTransition"
        end
      end
    end

    context "when new payment_type is #{CustomerType::PCARD}" do
      it_behaves_like(
        "requires invoice transition",
        CustomerType::PCARD,
        [CustomerType::GOODWILL, CustomerType::WARRANTY]
      )
    end

    context "when new payment_type is #{CustomerType::CUSTOMER_PAY}" do
      it_behaves_like(
        "requires invoice transition",
        CustomerType::CUSTOMER_PAY,
        [CustomerType::GOODWILL, CustomerType::WARRANTY]
      )
    end

    context "when new payment_type is #{CustomerType::GOODWILL}" do
      it_behaves_like(
        "requires invoice transition",
        CustomerType::GOODWILL,
        [CustomerType::PCARD, CustomerType::CUSTOMER_PAY]
      )
    end

    context "when new payment_type is #{CustomerType::WARRANTY}" do
      it_behaves_like(
        "requires invoice transition",
        CustomerType::WARRANTY,
        [CustomerType::PCARD, CustomerType::CUSTOMER_PAY]
      )
    end
  end
end
