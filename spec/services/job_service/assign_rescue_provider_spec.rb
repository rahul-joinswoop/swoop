# frozen_string_literal: true

require "rails_helper"

describe JobService::AssignRescueProviderService do
  describe "#call" do
    subject(:service) do
      JobService::AssignRescueProviderService.new(
        job, rescue_company, site, :swoop_assign
      ).call
    end

    let!(:fleet_company) do
      create(:fleet_company)
    end

    let(:driver) { create(:drive) }
    let!(:job) do
      create :fleet_managed_job, {
        status: :pending,
        fleet_company: fleet_company,
        adjusted_created_at: Time.now,
        driver: driver,
        rescue_company: nil,
      }
    end

    let(:site) do
      create(:site, {
        company: rescue_company,
      })
    end

    let(:rescue_company) do
      create(:rescue_company, :finish_line, {
      })
    end

    it 'works' do
      expect { subject }.not_to raise_error
    end

    it 'assigns the rescue company to the job' do
      expect { subject }.to change(job, :rescue_company).from(nil).to(rescue_company)
    end

    it 'assigns the rescue site to the job' do
      expect { subject }.to change(job, :site).from(nil).to(site)
    end

    context 'sets up account' do
      let(:department) do
        create(:department, {
          name: 'dept1',
          company: rescue_company,
        })
      end

      let(:account) do
        create(:account, {
          name: 'flt acct 1',
          company: rescue_company,
          client_company: fleet_company,
          department: department,
        })
      end

      it 'calls CreateAccountForJobService' do
        expect_any_instance_of(CreateAccountForJobService).to receive(:call)
        subject
      end

      it 'sets department' do
        expect(rescue_company).to(receive(:has_feature).with(Feature::AUTO_ASSIGN_DRIVER)).and_return(false)
        expect(rescue_company).to(receive(:has_feature).with(Feature::DEPARTMENTS)).and_return(true)
        expect_any_instance_of(CreateAccountForJobService).to(receive(:call)).and_return(account)
        expect { subject }.to change(job, :partner_department).from(nil).to(department)
      end

      it 'creates assignment' do
        expect { subject && job.save! }.to change(Assignment, :count).from(0).to(1)
        assignment = job.assignments[0]
        expect(assignment.site).to eql(site)
        expect(assignment.partner).to eql(rescue_company)
        expect(assignment.partner_live).to eql(job.partner_live)
        expect(assignment.partner_open).to eql(job.partner_open)
      end
    end
  end
end
