# frozen_string_literal: true

require "rails_helper"

describe AssignRescueDriverVehicleToJob::CheckJobForRescueDriver do
  subject { described_class.call job: job }

  context 'with no rescue_driver' do
    let(:job) { create :job }

    it "works" do
      expect(subject).to be_failure
      expect(subject.error).to eq(described_class::FAILURE_MESSAGE)
    end
  end

  context 'with rescue_driver' do
    let(:job) { create :job, :with_rescue_driver }

    it "works" do
      expect(subject).to be_success
    end
  end
end
