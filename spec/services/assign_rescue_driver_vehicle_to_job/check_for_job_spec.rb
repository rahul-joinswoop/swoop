# frozen_string_literal: true

require "rails_helper"

describe AssignRescueDriverVehicleToJob::CheckForJob do
  subject { described_class.call job: job }

  context 'with no job' do
    let(:job) { nil }

    it "fails" do
      expect(subject).to be_failure
      expect(subject.error).to eq(described_class::FAILURE_MESSAGE)
    end
  end

  context 'with job' do
    let(:job) { create :job }

    it "works" do
      expect(subject).to be_success
    end
  end
end
