# frozen_string_literal: true

require "rails_helper"

describe AssignRescueDriverVehicleToJob::AddRescueVehicleToJob do
  subject { described_class.call job: job }

  let(:job) do
    j = create :job, :with_rescue_vehicle
    j.update! rescue_vehicle: nil
    j
  end

  context 'with a valid job' do
    it "works" do
      expect { subject }
        .to change(job, :rescue_vehicle)
        .from(nil)
        .to(job.rescue_driver.vehicle)
      expect(subject).to be_success
      expect { job.reload }.not_to change(job, :rescue_vehicle)
    end
  end

  context 'with an invalid job' do
    let(:job) do
      j = super()
      # setup something that will fail a validation when we try to save
      j.fleet_company = nil
      j
    end

    it "fails" do
      expect(subject).to be_failure
      expect(subject.error)
        .to eq(described_class::FAILURE_MESSAGE % ["Validation failed: #{job.errors.full_messages.first}"])
      expect { job.reload }
        .to change(job, :rescue_vehicle)
        .to(nil)
    end
  end
end
