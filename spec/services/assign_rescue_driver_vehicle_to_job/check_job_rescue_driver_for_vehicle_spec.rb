# frozen_string_literal: true

require "rails_helper"

describe AssignRescueDriverVehicleToJob::CheckJobRescueDriverForVehicle do
  subject { described_class.call job: job }

  context 'with no rescue_driver.vehicle' do
    let(:job) { create :job, :with_rescue_driver }

    it "works" do
      expect(subject).to be_failure
      expect(subject.error).to eq(described_class::FAILURE_MESSAGE)
    end
  end

  context 'with rescue_driver.vehicle' do
    let(:job) do
      j = create :job, :with_rescue_vehicle
      j.update! rescue_vehicle: nil
      j
    end

    it "works" do
      expect(subject).to be_success
    end
  end
end
