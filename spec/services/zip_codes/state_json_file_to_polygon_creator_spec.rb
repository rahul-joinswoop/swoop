# frozen_string_literal: true

require 'rails_helper'

describe ZipCodes::StateJsonFileToPolygonCreator do
  subject { ZipCodes::StateJsonFileToPolygonCreator.new.call('nh_new_hampshire_zip_codes_geo.min.json') }

  let(:geojson_data_file_obj) { './spec/services/zip_codes/nh_new_hampshire_zip_codes_geo.min.json' }

  context 'downloads the state zipcode data file and' do
    it "invoke child workers" do
      expect_any_instance_of(S3Downloader).to receive(:download_specific_file).and_return(geojson_data_file_obj)
      expect { subject }.to change(ZipCodeSubsetToTerritory, :count).by(8)
    end
  end
end
