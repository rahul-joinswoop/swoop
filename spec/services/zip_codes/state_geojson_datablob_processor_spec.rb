# frozen_string_literal: true

require 'rails_helper'

describe ZipCodes::StateGeojsonDatablobProcessor do
  context 'method' do
    context 'call with polygon record' do
      let(:record) do
        {
          "type": "Feature",
          "properties": {
            "ZCTA5CE10": "03752",
          },
          "geometry": {
            "type": "Polygon",
            "coordinates": [
              [
                [-72.153388, 43.277677],
                [-72.15349, 43.278543],
                [-72.087543, 43.326121],
                [-72.153388, 43.277677],
              ],
            ],
          },
        }
      end

      it "increments the count of ZipCodeSubsetToTerritory by 1" do
        expect { described_class.new(record).call }.to change(ZipCodeSubsetToTerritory, :count).by(1)
      end
    end

    context 'call with multi-polygon record' do
      let(:record) do
        {
          "type": "Feature",
          "properties": {
            "ZCTA5CE10": "03752",
          },
          "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
              [
                [
                  [-71.432897, 43.691231],
                  [-71.434932, 43.692699],
                  [-71.436458, 43.691804],
                  [-71.432897, 43.691231],
                ],
              ],
              [
                [
                  [-71.519877, 43.755776],
                  [-71.51679, 43.753436],
                  [-71.516764, 43.753416],
                  [-71.519877, 43.755776],
                ],
              ],
            ],
          },
        }
      end

      it "increments the count of ZipCodeSubsetToTerritory by 2" do
        expect { described_class.new(record).call }.to change(ZipCodeSubsetToTerritory, :count).by(2)
      end
    end
  end
end
