# frozen_string_literal: true

require 'rails_helper'

describe ZipCodes::IngestGeojsonData do
  context 'method' do
    context 'call' do
      subject { ZipCodes::IngestGeojsonData.new('./spec/services/zip_codes/nh_new_hampshire_zip_codes_geo.min.json').call }

      it "returns array of objects for a provided file object " do
        expect(subject).to be_kind_of(Array)
      end
    end

    context 'open_zip_code_coordinate_data_file' do
      subject { ZipCodes::IngestGeojsonData.new('./spec/services/zip_codes/nh_new_hampshire_zip_codes_geo.min.json') }

      it "returns valid JSON object" do
        expect(subject.open_zip_code_coordinate_data_file).to have_key('features')
      end
    end
  end
end
