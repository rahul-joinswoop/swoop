# frozen_string_literal: true

require "rails_helper"

describe JobGetClosestProvidersService do
  context "when there are 3 possible RescueProvider" do
    let!(:service_location) do
      create(:location, {
        address: "325 La Casa Ave, San Mateo, CA 94403, USA",
        lat: 37.523966, # maybe it should be geocoded in future
        lng: -122.300601,
      })
    end

    let!(:job) do
      create(:job, {
        service_location: service_location,
        rescue_company: create(:rescue_company, :with_service_codes),
      })
    end

    let!(:close_location) do
      create(:location, {
        address: "500 Clark Dr, San Mateo, CA 94402, USA",
        lat: 37.566021, # maybe it should be geocoded in future
        lng: -122.348171,
      })
    end

    let!(:distant_location) do
      create(:location, {
        address: "742 Hillside Blvd, Daly City, CA 94014, USA",
        lat: 37.593966, # maybe it should be geocoded in future
        lng: -122.362141,
      })
    end

    let!(:very_distant_location) do
      create(:location, {
        address: "83 Parkview Cir, Corte Madera, CA 94925, USA",
        lat: 37.593999, # maybe it should be geocoded in future
        lng: -122.399243,
      })
    end

    let!(:viewing_company) do
      create(:fleet_company)
    end

    let!(:distant_rescue_provider) do
      create(:rescue_provider, {
        site: create(:partner_site, location: distant_location, always_open: false),
        company: viewing_company,
        provider: create(:rescue_company, :with_service_codes),
        location: nil,
      })
    end

    let!(:close_rescue_provider) do
      create(:rescue_provider, {
        site: create(:partner_site, location: close_location, always_open: false),
        company: viewing_company,
        provider: create(:rescue_company, :with_service_codes),
        location: nil,
      })
    end

    let!(:very_distant_rescue_provider) do
      create(:rescue_provider, {
        site: create(:partner_site, location: very_distant_location, always_open: false),
        company: viewing_company,
        provider: create(:rescue_company, :with_service_codes),
        location: nil,
      })
    end

    subject do
      JobGetClosestProvidersService.new(job, viewing_company)
    end

    it "uses geocoder calculates distance" do
      distance = Geocoder::Calculations.distance_between(
        [service_location.lat, service_location.lng],
        [close_location.lat, close_location.lng],
        units: :mi
      )
      expect(distance.round(5)).to eq 3.90313
    end

    describe "#call" do
      context "when job has coordinates" do
        it "returns RescueProviders sorted by job_distance" do
          result = JobGetClosestProvidersService.new(job, viewing_company).call
          expect(result).to eq [close_rescue_provider, distant_rescue_provider, very_distant_rescue_provider]
          expect(result.all?(&:job_distance)).to eq true
        end

        it "filters out RescueProviders if the distance is larger than the max allowed" do
          result = JobGetClosestProvidersService.new(job, viewing_company, max_distance: 7).call
          expect(result).to eq [close_rescue_provider, distant_rescue_provider]
        end

        it "can set a maximum number of providers" do
          result = JobGetClosestProvidersService.new(job, viewing_company, max_providers: 1).call
          expect(result).to eq [close_rescue_provider]
        end

        describe 'when filtering only by sites opened for business' do
          subject do
            JobGetClosestProvidersService.new(
              job, viewing_company, site_open_for_business: true
            ).call
          end

          it { is_expected.to eq [] }
        end

        describe "rescue provider auto assign distance" do
          before do
            stub_const("RescueProvider::DEFAULT_AUTO_ASSIGN_DISTANCE_MILES", 7)
          end

          it "filters out RescueProviders if the distance is larger than the rescue provider max allowed" do
            result = JobGetClosestProvidersService.new(job, viewing_company, for_auto_assignment: true).call
            expect(result).to eq [close_rescue_provider, distant_rescue_provider]
          end

          it "filters out RescueProviders if the distance is larger than the max allowed by the dispatch company" do
            dispatch_company = create(:super_company)
            RescueProvider.create!(
              company: dispatch_company,
              provider: close_rescue_provider.provider,
              site: close_rescue_provider.site,
              auto_assign_distance: 3
            )
            result = JobGetClosestProvidersService.new(job, viewing_company, for_auto_assignment: true, dispatch_company: dispatch_company).call
            expect(result).to eq [distant_rescue_provider]
          end

          it "does not filter out RescueProviders if not auto assigning" do
            distant_rescue_provider.update_attribute(:auto_assign_distance, 3)
            result = JobGetClosestProvidersService.new(job, viewing_company).call
            expect(result).to eq [close_rescue_provider, distant_rescue_provider, very_distant_rescue_provider]
          end
        end
      end

      context "when job has no lat/lng" do
        subject(:service_call) do
          JobGetClosestProvidersService.new(job_with_no_coordinates, viewing_company).call
        end

        let(:job_with_no_coordinates) do
          create(
            :job,
            rescue_company: create(:rescue_company, :with_service_codes)
          )
        end

        it 'returns empty array' do
          expect(service_call).to eq []
        end
      end
    end
  end
end
