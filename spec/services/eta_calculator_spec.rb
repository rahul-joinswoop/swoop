# frozen_string_literal: true

require "rails_helper"

describe EtaCalculator do
  subject(:calculator) { EtaCalculator.new(job.id) }

  let(:result) { calculator.call }
  let(:job) { create(:job) }
  let(:oldest_parent) { [] }

  before do
    allow(calculator)
      .to receive(:oldest_parent)
      .and_return(oldest_parent)

    allow(calculator)
      .to receive(:earliest_job_eta)
      .and_return(earliest_job_eta)
  end

  context 'without an eta' do
    let(:earliest_job_eta) { [] }

    describe 'calculating the ETA' do
      it 'returns nil' do
        expect(result).to eq(nil)
      end
    end
  end

  context 'the job has no parents' do
    context 'with an eta' do
      let(:earliest_job_eta) do
        build_stubbed(
          :time_of_arrival,
          eba_type: TimeOfArrival::BID,
          time: eta_estimated_at_dttm,
          created_at: eta_created_at_dttm
        )
      end

      context 'with an eta > eta creation dttm' do
        let(:eta_estimated_at_dttm) { "2018-07-17T12:31:08.610-07:00" }
        let(:eta_created_at_dttm) { "2018-07-17T11:30:41.020-07:00" }

        describe 'calculating the ETA' do
          it 'returns a positive value in minutes' do
            expect(result).to be > 0
          end
        end
      end

      context 'with an eta < eta creation dttm' do
        let(:eta_estimated_at_dttm) { "2018-07-17T11:30:41.020-07:00" }
        let(:eta_created_at_dttm) { "2018-07-17T12:31:08.610-07:00" }

        describe 'calculating the ETA' do
          it 'returns a negative value in minutes' do
            expect(result).to be < 0
          end
        end
      end
    end
  end

  context 'with a parent job' do
    let(:oldest_parent) { [create(:job)] }

    context 'with an eta' do
      let(:earliest_job_eta) do
        build_stubbed(
          :time_of_arrival,
          eba_type: TimeOfArrival::BID,
          time: eta_estimated_at_dttm,
          created_at: eta_created_at_dttm
        )
      end

      context 'with an eta > of creation' do
        let(:eta_estimated_at_dttm) { "2018-07-17T12:31:08.610-07:00" }
        let(:eta_created_at_dttm) { "2018-07-17T11:30:41.020-07:00" }

        describe 'calculating the ETA' do
          it 'returns a positive value in minutes' do
            expect(result).to be > 0
          end
        end
      end

      context 'with an eta < of creation' do
        let(:eta_estimated_at_dttm) { "2018-07-17T11:30:41.020-07:00" }
        let(:eta_created_at_dttm) { "2018-07-17T12:31:08.610-07:00" }

        describe 'calculating the ETA' do
          it 'returns a negative value in minutes' do
            expect(result).to be < 0
          end
        end
      end
    end
  end
end
