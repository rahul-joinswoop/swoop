# frozen_string_literal: true

require "rails_helper"

describe UpdateRecipientOnInvoice do
  subject { UpdateRecipientOnInvoice.new(job.id).perform }

  let(:swoop_company) { create(:super_company) }
  let(:tow_company) { create(:rescue_company, { name: 'New Jerzy Towing' }) }
  let(:in_house_fleet_a) { create(:fleet_in_house_company, name: "In-House Fleet A") }
  let(:in_house_fleet_b) { create(:fleet_in_house_company, name: "In-House Fleet B") }
  let(:tow_company_account_for_fleet_a) do
    create(
      :account,
      name: in_house_fleet_a.name,
      company: tow_company,
      client_company: in_house_fleet_a
    )
  end
  let(:tow_company_account_for_fleet_b) do
    create(
      :account,
      name: in_house_fleet_b.name,
      company: tow_company,
      client_company: in_house_fleet_b
    )
  end
  let(:job) do
    create(:fleet_in_house_job, {
      rescue_company: tow_company,
      invoice: nil,
      account: tow_company_account_for_fleet_a,
      service_code: ServiceCode.find_by(name: 'Tow'),
    })
  end

  describe 'fleet in-house invoice' do
    it 'changes the recipient and recipient company' do
      # Step 1: The job is assigned to the Tow Company, and it's account is In-House Fleet A
      expect(job.rescue_company).to eq(tow_company)
      expect(job.account).to eq(tow_company_account_for_fleet_a)
      expect(job.account.company).to eq(tow_company)
      # Step 2: We create an invoice for the job
      job.invoice = CreateInvoiceForJob.new(job).call
      job.save!
      # Step 3: The invoice is for the the Tow Company, and it's account is In-House Fleet A
      expect(job.invoice.sender).to eq(tow_company)
      expect(job.invoice.recipient).to eq(tow_company_account_for_fleet_a)
      expect(job.invoice.recipient_company).to eq(in_house_fleet_a)
      # Step 4: We change the job's account to In-House Fleet B
      job.account = tow_company_account_for_fleet_b
      job.save!
      expect(job.account).to eq(tow_company_account_for_fleet_b)
      # ... but the invoice is not yet updated to In-House Fleet B
      expect(job.invoice.sender).to eq(tow_company)
      expect(job.invoice.recipient).to eq(tow_company_account_for_fleet_a)
      expect(job.invoice.recipient_company).to eq(in_house_fleet_a)
      # Step 5: We update the invoice
      subject
      job.reload
      job.invoice.reload
      # Step 6: And now the Tow Company's invoice is updated to In-House Fleet B
      expect(job.rescue_company).to eq(tow_company)
      expect(job.account).to eq(tow_company_account_for_fleet_b)
      expect(job.account.company).to eq(tow_company)
      expect(job.invoice.sender).to eq(tow_company)
      expect(job.invoice.recipient).to eq(tow_company_account_for_fleet_b)
      expect(job.invoice.recipient_company).to eq(in_house_fleet_b)
    end
  end
end
