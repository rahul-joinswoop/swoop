# frozen_string_literal: true

require "rails_helper"

describe Slack::Config do
  let(:config) { Slack::Config.new("Test Client", "TestBot", "test_channel") }

  it "creates all client feeds" do
    config.create_client
    feeds = SlackChannel.where(bot_name: "TestBot")
    expect(feeds.size).to eq 17
    expect(feeds.map(&:tag).uniq.size).to eq feeds.size
    feed = feeds.detect { |f| f.name == "Test Client summary" }
    expect(feed.tag).to eq "test-client-summary"
    expect(feed.channel).to eq "test_channel"
  end

  it "creates review feed" do
    config.create_reviews
    feeds = SlackChannel.where(bot_name: "TestBot")
    expect(feeds.size).to eq 1
    feed = feeds.first
    expect(feed.name).to eq "Test Client review"
    expect(feed.tag).to eq "test-client-review"
    expect(feed.channel).to eq "test_channel"
  end

  it "creates ops feed" do
    config.create_ops
    feeds = SlackChannel.where(bot_name: "TestBot")
    expect(feeds.size).to eq 1
    feed = feeds.first
    expect(feed.name).to eq "Test Client Ops"
    expect(feed.tag).to eq "test-client-%"
    expect(feed.excludes).to eq "review-link"
    expect(feed.channel).to eq "test_channel"
  end

  it "does not fail if a duplicate feed already exists" do
    config.create_reviews
    config.create_reviews
    feeds = SlackChannel.where(bot_name: "TestBot")
    expect(feeds.size).to eq 1
  end
end
