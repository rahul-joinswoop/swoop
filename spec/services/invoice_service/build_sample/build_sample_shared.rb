# frozen_string_literal: true

require 'rails_helper'
RSpec.shared_context "build sample shared" do
  subject(:build_sample) do
    InvoiceService::BuildSample.new(
      original_invoice: original_invoice, # required
      rate_type: rate_type, # required
      vehicle_category: vehicle_category,
      line_items_hashes: line_items_hashes, # required
      edit_invoice_metadata: edit_invoice_metadata, # required, can be {}
    ).call.sample_invoice
  end

  def configure_job!(job_to_be_configured)
    # there are specs where original_invoice is nil, so no need to create the job
    return nil unless original_invoice

    job = job_to_be_configured
    job.rescue_company = rescue_company
    job.fleet_company = fleet_company
    job.account = account
    job.invoice = original_invoice
    job.invoice_vehicle_category = invoice_vehicle_category
    job.driver_notes = driver_notes
    job.save!

    original_invoice.job = job
    original_invoice.line_items.each do |li|
      li.job_id = job.id
      li.save!
    end

    if add_storage_to_job
      create :inventory_item, :with_vehicle, job: job, deleted_at: nil
      create :audit_job_status, job: job, job_status: create(:job_status, :stored), last_set_dttm: Time.now
    end

    original_invoice.save!

    job.reload

    job
  end

  let(:rate_type) { nil }
  let(:vehicle_category) { nil }
  let(:line_items_hashes) { nil }
  let(:edit_invoice_metadata) { {} }
  let(:previous_rate_type) { nil }
end
