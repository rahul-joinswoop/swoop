# frozen_string_literal: true

require_relative './build_sample_shared'
require_relative './valid_input_shared'

describe InvoiceService::BuildSample do
  include_context "build sample shared"

  context 'with valid input' do
    include_context "valid input shared"

    context 'when rate_type is equals to the one in the original invoice' do
      let(:rate_type) { create(:rate_type, :flat) }

      context 'and previous_rate_type is not equals to the one in the original invoice' do
        let(:previous_rate_type) { create(:rate_type, :hours_port_to_port) }

        let(:edit_invoice_metadata) do
          {
            previous_rate_type: previous_rate_type,
            previous_class_type: nil,
          }
        end

        context 'and all original_invoice.line_items are passed by line_items_hashes' do
          let(:line_items_hashes) do
            [
              original_li_flat.attributes,
              original_li_addition_dolly.attributes,
              original_li_tax.attributes,
            ]
          end

          it_behaves_like 'build the original invoice line_items in the sample invoice'
          it_behaves_like 'new expected invoice total, balance and line_item.size',
                          total_amount: '15.0', balance: '10.0', line_items_size: 3

          context 'and it also include an user_created line_item in memory' do
            let(:line_items_hashes) do
              super() << li_fuel_user_created_hash
            end

            it_behaves_like 'build values for li_fuel_user_created item accordingly'
            it_behaves_like 'new expected invoice total, balance and line_item.size',
                            total_amount: '22.0', balance: '17.0', line_items_size: 4

            context 'and the user_created line_item in memory is deleted_at' do
              let(:li_fuel_changed_deleted_at) { Time.now }

              it_behaves_like 'build the original invoice line_items in the sample invoice'
              it_behaves_like 'new expected invoice total, balance and line_item.size',
                              total_amount: '15.0', balance: '10.0', line_items_size: 3
            end
          end
        end
      end
    end

    context 'when vehicle_category is the same as the original' do
      context 'when RateType is changed' do
        let(:rate_type) { create(:rate_type, :hours_port_to_port) }

        let!(:rate_p2p) { create(:hourly_p2p, hourly: 50, company: rescue_company) }
        let(:flatbed) { create(:vehicle_category, name: VehicleCategory::FLATBED) }
        let!(:rate_p2p_for_flatbed) { create(:hourly_p2p, hourly: 60, company: rescue_company, vehicle_category: flatbed) }

        before do
          allow_any_instance_of(Job).to receive(:mins_p2p).and_return(60) # to force a line_item with values > 0
        end

        it_behaves_like 'original_invoice values correctly set'

        context 'and all original_invoice.line_items are passed by line_items_hashes' do
          let(:line_items_hashes) do
            [
              original_li_flat.attributes,
              original_li_addition_dolly.attributes,
              original_li_tax.attributes,
            ]
          end

          it_behaves_like 'build new line_items for new_rate_type accordingly',
                          net_amount: 50.0, unit_price: 50.0, quantity: 1.0

          it_behaves_like 'softly delete all previous items built by Flat Rate'

          it_behaves_like 'new expected invoice total, balance and line_item.size',
                          total_amount: '50.0', balance: '45.0', line_items_size: 5

          it_behaves_like 'not change invoices and their associations in the database'

          context 'when a user_created item is also passed in line_items_hashes' do
            let(:line_items_hashes) do
              [
                original_li_flat.attributes,
                original_li_addition_dolly.attributes,
                li_fuel_user_created_hash,
                original_li_tax.attributes,
              ]
            end

            it_behaves_like 'build values for li_fuel_user_created item accordingly'

            it_behaves_like 'softly delete all previous items built by Flat Rate'

            it_behaves_like 'new expected invoice total, balance and line_item.size',
                            total_amount: '57.0', balance: '52.0', line_items_size: 6

            it_behaves_like 'not change invoices and their associations in the database'
          end

          context 'when VehicleCategory is changed' do
            let(:vehicle_category) { flatbed }

            it_behaves_like 'softly delete all previous items built by Flat Rate'

            it_behaves_like 'not change invoices and their associations in the database'
          end
        end
      end
    end
  end
end
