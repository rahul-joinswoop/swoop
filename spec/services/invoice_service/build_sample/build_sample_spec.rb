# frozen_string_literal: true

require_relative './build_sample_shared'

describe InvoiceService::BuildSample do
  include_context "build sample shared"
  context 'when original_invoice is nil' do
    let(:original_invoice) { nil }

    it 'raises error' do
      expect { build_sample }.to raise_error InvoiceService::BuildSample::OriginalInvoiceRequiredError
    end
  end

  context 'when original_invoice is not an Invoice instance' do
    let(:original_invoice) { Job.new }

    it 'raises error' do
      expect { build_sample }.to raise_error InvoiceService::BuildSample::OriginalInvoiceIsNotAnInvoiceInstanceError
    end
  end

  context 'when original_invoice is valid' do
    let(:original_invoice) { Invoice.new }

    context 'but rate_type is nil' do
      let(:rate_type) { nil }

      it 'raises error' do
        expect { build_sample }.to raise_error InvoiceService::BuildSample::NewRateTypeRequiredError
      end
    end

    context 'but rate_type is not a RateType instance' do
      let(:rate_type) { Job.new }

      it 'raises error' do
        expect { build_sample }.to raise_error InvoiceService::BuildSample::NewRateTypeIsNotARateTypeInstanceError
      end
    end
  end

  context 'with invalid line_items_hashes hashes' do
    let(:original_invoice) { create(:invoice) }
    let(:rate_type) { create(:rate_type, :flat) }

    context 'when it is empty' do
      it 'raises error' do
        expect { build_sample }.to raise_error InvoiceService::BuildSample::LineItemsHashesNilOrEmptyError
      end
    end

    context 'when it has empty hashes' do
      let(:line_items_hashes) { [{}, {}] }

      it 'raises error' do
        expect { build_sample }.to raise_error InvoiceService::BuildSample::LineItemsHashesNilOrEmptyError
      end
    end
  end
end
