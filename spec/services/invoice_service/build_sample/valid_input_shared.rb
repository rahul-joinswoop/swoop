# frozen_string_literal: true

require 'rails_helper'
RSpec.shared_context "valid input shared" do
  #
  # The invoice used by all the specs.
  # It's created with a payment and a refund.
  #
  # We also build one more payment and one
  # more refund to it to check that the samples
  # are not persisting those (as it shouldn't).
  #
  let(:original_invoice) do
    create(
      :invoice, :with_payments, :with_refunds,
      line_items: original_lis_by_flat_rate,
      recipient_company: fleet_company,
      sender: rescue_company,
      state: 'partner_new',
      rate_type: rate_type.type,
    )
  end

  # a payment to be build into the invoice
  # (in other words, not persisted in db)
  # we check if it's still not persisted at the end of the flow
  let(:original_payment) do
    build(
      :payment,
      recipient: account,
      sender: rescue_company,
      total_amount: BigDecimal("-7.0"),
    )
  end

  # a refund to be build into the invoice
  # (in other words, not persisted in db)
  # we check if it's still not persisted at the end of the flow
  let(:original_refund) do
    build(
      :refund,
      recipient: account,
      sender: rescue_company,
      total_amount: BigDecimal("2.0"),
    )
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company) }

  let(:original_rate_type) { rate_type }
  let(:original_vehicle_category) { vehicle_category }

  let(:rate_type) { create(:rate_type, :flat) }
  let!(:rate) { create(:clean_flat_rate, flat: 10, company: rescue_company) }
  let!(:rate_addition) { create(:rate_addition, name: 'Dolly', rate: rate, amount: '5.0', calc_type: rate_addition_calc_type) }
  let(:rate_addition_calc_type) { 'flat' }

  let!(:storage_rate) { create(:storage_rate, storage_daily: 100, company: rescue_company) }

  let(:invoice_vehicle_category) { nil }
  let(:driver_notes) { nil }
  let(:account) { create(:account, company: rescue_company, client_company: fleet_company) }

  # if turned on, the job will get a storage item, and the invoice will
  # get a Storage line_item
  let(:add_storage_to_job) { false }

  let!(:job) do
    configure_job!(original_invoice.job)
  end

  # - by default, this spec simulates an Invoice with line_items originally created by a FlatRate,
  #   we start with original_lis_by_flat_rate:
  let(:original_lis_by_flat_rate) do
    if add_storage_to_job
      [original_li_flat, original_li_addition_dolly, original_li_storage, original_li_tax]
    else
      [original_li_flat, original_li_addition_dolly, original_li_tax]
    end
  end

  #
  # 'original_li_* let blocks' explained:
  #
  # all 'original_li_*' means the ones that we can add to the original_invoice to simulate
  # they were added during the Invoice creation by a FlatRate. It could be any other rate really,
  # should make no difference. Since FlatRate always adds a 'Flat Rate' item and a 'Tax Item',
  # they should always be used by original_lis_by_flat_rate to simulate a real case.
  #
  # - if original_li_addition_dolly is added, it means it is an automatic item added by FlatRate
  # - if original_li_storage is added, it means that invoice.job.storage is existent
  let(:original_li_flat) do
    create(
      :invoicing_line_item,
      description: "Flat Rate",
      estimated: true,
      net_amount: BigDecimal("10.0"),
      original_quantity: BigDecimal("1.0"),
      original_unit_price: BigDecimal("10.0"),
      quantity: BigDecimal("1.0"),
      tax_amount: BigDecimal("0.0"),
      unit_price: BigDecimal("10.0"),
      user_created: false,
      rate_id: rate.id,
    )
  end

  let(:original_li_addition_dolly) do
    create(
      :invoicing_line_item,
      description: "Dolly",
      estimated: true,
      net_amount: BigDecimal("5.0"),
      original_quantity: BigDecimal("1.0"),
      original_unit_price: BigDecimal("5.0"),
      quantity: BigDecimal("1.0"),
      tax_amount: BigDecimal("0.0"),
      unit_price: BigDecimal("5.0"),
      user_created: false,
      rate_id: rate.id,
      calc_type: rate_addition_calc_type
    )
  end

  let(:original_li_tax) do
    create(
      :invoicing_line_item,
      description: "Tax",
      estimated: true,
      net_amount: BigDecimal("0.0"),
      original_quantity: BigDecimal("0.0"),
      original_unit_price: BigDecimal("0.0"),
      quantity: BigDecimal("0.0"),
      tax_amount: BigDecimal("0.0"),
      unit_price: BigDecimal("0.0"),
      user_created: false,
      rate_id: rate.id,
    )
  end

  let(:original_li_storage) do
    create(
      :invoicing_line_item,
      description: "Storage",
      estimated: true,
      net_amount: BigDecimal("100.0"),
      original_quantity: BigDecimal("1.0"),
      original_unit_price: BigDecimal("100.0"),
      quantity: BigDecimal("1.0"),
      tax_amount: BigDecimal("0.0"),
      unit_price: BigDecimal("100.0"),
      user_created: false,
      auto_calculated_quantity: true,
      rate_id: rate.id,
    )
  end

  #
  # '*_changed or *_changed_deleted_at let blocks' explained:
  #
  # They are used to simulate changes to invoice.line_items.
  # They always dupe an original_li and changes some of its attrs
  # before calling the service.
  #
  # They simulate user UI changes.
  #
  let(:li_flat_changed_deleted_at) { nil }
  let(:li_flat_changed) do
    li = original_li_flat.dup
    li.id = original_li_flat.id
    li.quantity = BigDecimal('2.0')
    li.unit_price = BigDecimal('20.0')
    li.estimated = false
    li.deleted_at = li_flat_changed_deleted_at

    li
  end

  let(:li_dolly_unit_price_changed) { '20.0' }
  let(:li_dolly_changed_deleted_at) { nil }
  let(:li_dolly_changed) do
    li = original_li_addition_dolly.dup
    li.id = original_li_addition_dolly.id
    li.quantity = BigDecimal('2.0')
    li.unit_price = li_dolly_unit_price_changed
    li.estimated = false
    li.deleted_at = li_dolly_changed_deleted_at

    li
  end

  let(:li_storage_changed_deleted_at) { nil }
  let(:li_storage_changed) do
    li = original_li_storage.dup
    li.id = original_li_storage.id
    li.quantity = BigDecimal('2.0')
    li.unit_price = BigDecimal('120.0')
    li.estimated = false
    li.auto_calculated_quantity = false
    li.deleted_at = li_storage_changed_deleted_at

    li
  end

  let(:li_tax_changed) do
    li = original_li_tax.dup
    li.id = original_li_tax.id
    li.tax_amount = BigDecimal('20.0')
    li.estimated = false

    li
  end

  let(:li_fuel_changed_deleted_at) { nil }
  let(:li_fuel_user_created_hash) do
    {
      description: "Fuel",
      estimated: false,
      net_amount: BigDecimal("0.0"),
      original_quantity: BigDecimal("1.0"),
      original_unit_price: BigDecimal("7.0"),
      quantity: BigDecimal("1.0"),
      tax_amount: BigDecimal("0.0"),
      unit_price: BigDecimal("7.0"),
      user_created: true,
      deleted_at: li_fuel_changed_deleted_at,
    }
  end

  let(:invoices_on_db_before_build_sample) { nil }

  shared_examples 'original_invoice values correctly set' do |after_service_call|
    it 'has expected values' do
      expect(original_invoice.id).to be_present
      expect(original_invoice.rate_type).to eq 'FlatRate'
      expect(original_invoice.state).to eq 'partner_new'
      expect(original_invoice.sender.id).to eq rescue_company.id
      expect(original_invoice.recipient_company.id).to eq fleet_company.id

      expect(original_invoice.payments_or_credits.size).to eq 4 # two persisted, and two in memory
      expect(original_invoice.payments_or_credits.count).to eq 2 # check the amount of the persisted ones

      expect(original_invoice.job.id).to eq job.id
      expect(original_invoice.job.invoice_vehicle_category).to eq invoice_vehicle_category
      expect(original_invoice.job.driver_notes).to eq driver_notes

      li_flat = original_invoice.line_items.find { |li| li.description == 'Flat Rate' }
      expect(li_flat.id).to be_present
      expect(li_flat.estimated).to be_truthy
      expect(li_flat.net_amount.to_s).to eq "10.0"
      expect(li_flat.original_quantity.to_s).to eq "1.0"
      expect(li_flat.original_unit_price.to_s).to eq "10.0"
      expect(li_flat.quantity.to_s).to eq "1.0"
      expect(li_flat.tax_amount.to_s).to eq "0.0"
      expect(li_flat.unit_price.to_s).to eq "10.0"
      expect(li_flat.user_created).to be_falsey

      li_dolly = original_invoice.line_items.find { |li| li.description == 'Dolly' }
      expect(li_dolly.id).to be_present
      expect(li_dolly.estimated).to be_truthy
      expect(li_dolly.net_amount.to_s).to eq "5.0"
      expect(li_dolly.original_quantity.to_s).to eq "1.0"
      expect(li_dolly.original_unit_price.to_s).to eq "5.0"
      expect(li_dolly.quantity.to_s).to eq "1.0"
      expect(li_dolly.tax_amount.to_s).to eq "0.0"
      expect(li_dolly.unit_price.to_s).to eq "5.0"
      expect(li_dolly.user_created).to be_falsey

      li_tax = original_invoice.line_items.find { |li| li.description == 'Tax' }
      expect(li_tax.id).to be_present
      expect(li_tax.estimated).to be_truthy
      expect(li_tax.net_amount.to_s).to eq "0.0"
      expect(li_tax.original_quantity.to_s).to eq "0.0"
      expect(li_tax.original_unit_price.to_s).to eq "0.0"
      expect(li_tax.quantity.to_s).to eq "0.0"
      expect(li_tax.tax_amount.to_s).to eq "0.0"
      expect(li_tax.unit_price.to_s).to eq "0.0"
      expect(li_tax.user_created).to be_falsey

      if add_storage_to_job
        expect(original_invoice.total_amount.to_s).to eq "115.0"
        expect(original_invoice.balance.to_s).to eq "110.0"
        expect(original_invoice.line_items.count).to eq 4

        li_storage = original_invoice.line_items.find { |li| li.description == 'Storage' }
        expect(li_storage.id).to be_present
        expect(li_storage.estimated).to be_truthy
        expect(li_storage.net_amount.to_s).to eq "100.0"
        expect(li_storage.original_quantity.to_s).to eq "1.0"
        expect(li_storage.original_unit_price.to_s).to eq "100.0"
        expect(li_storage.quantity.to_s).to eq "1.0"
        expect(li_storage.tax_amount.to_s).to eq "0.0"
        expect(li_storage.unit_price.to_s).to eq "100.0"
        expect(li_storage.user_created).to be_falsey
        expect(li_storage.auto_calculated_quantity).to be_truthy
      else
        expect(original_invoice.total_amount.to_s).to eq "15.0"
        expect(original_invoice.balance.to_s).to eq "10.0"
        expect(original_invoice.line_items.count).to eq 3
      end
    end
  end

  shared_examples 'does not change invoices and their associations in the database' do
    # if we use `expect { build_sample }.to change(<model>, :all)` syntax, the specs fail,
    # so we do it manually
    it 'does not change invoices and their associations in the database' do
      invoicing_ledger_items_on_db_before_build_sample = InvoicingLedgerItem.all.to_a
      invoicing_line_items_on_db_before_build_sample = InvoicingLineItem.all.to_a
      jobs_on_db_before_build_sample = Job.all.to_a
      companies_on_db_before_build_sample = Company.all.to_a
      users_on_db_before_build_sample = User.all.to_a

      build_sample

      # InvoicingLedgerItem covers Invoice, Payment, CreditNote, Discount, WriteOff, Refund
      expect(invoicing_ledger_items_on_db_before_build_sample).to eq InvoicingLedgerItem.all.to_a
      expect(invoicing_line_items_on_db_before_build_sample).to eq InvoicingLineItem.all.to_a
      expect(jobs_on_db_before_build_sample).to eq Job.all.to_a
      expect(companies_on_db_before_build_sample).to eq Company.all.to_a
      expect(users_on_db_before_build_sample).to eq User.all.to_a
    end
  end

  shared_examples 'not change invoices and their associations in the database' do
    it_behaves_like 'does not change invoices and their associations in the database'

    context 'when job is fleet managed job' do
      let!(:job) do
        configure_job!(create(:fleet_managed_job))
      end

      it_behaves_like 'does not change invoices and their associations in the database'

      context 'when job.billing_type is VCC' do
        let(:agent) { create(:user, :dispatcher) }

        before do
          job.create_billing_info!(billing_type: BillingType::VCC, vcc_amount: 100, rescue_company: job.rescue_company, agent: agent)
        end

        it_behaves_like 'does not change invoices and their associations in the database'
      end
    end
  end

  shared_examples 'keep same original_li_flat values' do
    it 'has expected values' do
      sample_invoice = build_sample

      li_flat = sample_invoice.line_items.find { |li| li.description == 'Flat Rate' }
      expect(li_flat.id).to eq original_li_flat.id
      expect(li_flat.estimated).to be_truthy
      expect(li_flat.net_amount.to_s).to eq "10.0"
      expect(li_flat.original_quantity.to_s).to eq "1.0"
      expect(li_flat.original_unit_price.to_s).to eq "10.0"
      expect(li_flat.quantity.to_s).to eq "1.0"
      expect(li_flat.tax_amount.to_s).to eq "0.0"
      expect(li_flat.unit_price.to_s).to eq "10.0"
      expect(li_flat.user_created).to be_falsey
    end
  end

  shared_examples 'keep same original_li_storage values' do
    it 'has expected values' do
      sample_invoice = build_sample

      li_storage = sample_invoice.line_items.find { |li| li.description == 'Storage' }

      expect(li_storage.id).to eq original_li_storage.id
      expect(li_storage.estimated).to be_truthy
      expect(li_storage.net_amount.to_s).to eq "100.0"
      expect(li_storage.original_quantity.to_s).to eq "1.0"
      expect(li_storage.original_unit_price.to_s).to eq "100.0"
      expect(li_storage.quantity.to_s).to eq "1.0"
      expect(li_storage.tax_amount.to_s).to eq "0.0"
      expect(li_storage.unit_price.to_s).to eq "100.0"
      expect(li_storage.auto_calculated_quantity).to be_truthy
      expect(li_storage.user_created).to be_falsey
    end
  end

  shared_examples 'keep same original_li_addition_dolly values' do |expectations|
    it 'has expected values' do
      sample_invoice = build_sample

      li_dolly = sample_invoice.line_items.find { |li| li.description == 'Dolly' }
      expect(li_dolly.id).to eq original_li_addition_dolly.id
      expect(li_dolly.estimated).to be_truthy
      expect(li_dolly.net_amount.to_s).to eq "5.0"
      expect(li_dolly.original_quantity.to_s).to eq "1.0"
      expect(li_dolly.original_unit_price.to_s).to eq "5.0"
      expect(li_dolly.quantity.to_s).to eq "1.0"
      expect(li_dolly.tax_amount.to_s).to eq "0.0"
      expect(li_dolly.unit_price.to_s).to eq "5.0"
      expect(li_dolly.user_created).to be_falsey
    end

    context 'when li_dolly.calc_type is percent' do
      let(:rate_addition_calc_type) { 'percent' }

      it 'builds net_amount accordingly' do
        sample_invoice = build_sample
        li_dolly = sample_invoice.line_items.find { |li| li.description == 'Dolly' }

        expect(li_dolly.net_amount.to_s).to eq expectations[:value_if_percent_case].to_s
      end
    end
  end

  shared_examples 'keep same original_li_tax values' do
    it 'has expected values' do
      sample_invoice = build_sample

      li_tax = sample_invoice.line_items.find { |li| li.description == 'Tax' }
      expect(li_tax.id).to eq original_li_tax.id
      expect(li_tax.estimated).to be_truthy
      expect(li_tax.net_amount.to_s).to eq "0.0"
      expect(li_tax.original_quantity.to_s).to eq "0.0"
      expect(li_tax.original_unit_price.to_s).to eq "0.0"
      expect(li_tax.quantity.to_s).to eq "0.0"
      expect(li_tax.tax_amount.to_s).to eq "0.0"
      expect(li_tax.unit_price.to_s).to eq "0.0"
      expect(li_tax.user_created).to be_falsey
    end
  end

  shared_examples 'build new values for li_flat item accordingly' do
    it 'builds it correctly' do
      sample_invoice = build_sample
      li_flat = sample_invoice.line_items.find { |li| li.description == 'Flat Rate' }

      expect(li_flat.id).to eq original_li_flat.id
      expect(li_flat.estimated).to be_falsey
      expect(li_flat.net_amount.to_s).to eq "40.0"
      expect(li_flat.original_quantity.to_s).to eq "1.0"
      expect(li_flat.original_unit_price.to_s).to eq "10.0"
      expect(li_flat.quantity.to_s).to eq "2.0"
      expect(li_flat.tax_amount.to_s).to eq "0.0"
      expect(li_flat.unit_price.to_s).to eq "20.0"
      expect(li_flat.user_created).to be_falsey

      if li_flat_changed_deleted_at
        expect(li_flat.deleted_at).to be_present
      else
        expect(li_flat.deleted_at).to be_nil
      end
    end
  end

  shared_examples 'build values for li_fuel_user_created item accordingly' do
    it 'builds it correctly' do
      sample_invoice = build_sample

      li_fuel = sample_invoice.line_items.find { |li| li.description == 'Fuel' }

      expect(li_fuel.id).to be_nil
      expect(li_fuel.estimated).to be_falsey
      expect(li_fuel.net_amount.to_s).to eq "7.0"
      expect(li_fuel.original_quantity.to_s).to eq "1.0"
      expect(li_fuel.original_unit_price.to_s).to eq "7.0"
      expect(li_fuel.quantity.to_s).to eq "1.0"
      expect(li_fuel.tax_amount.to_s).to eq "0.0"
      expect(li_fuel.unit_price.to_s).to eq "7.0"
      expect(li_fuel.user_created).to be_truthy
    end
  end

  shared_examples 'remove deleted li_fuel_user_created from line_items_hashes because it existed in memory only' do
    it 'builds it correctly' do
      sample_invoice = build_sample
      li_fuel = sample_invoice.line_items.find { |li| li.description == 'Fuel' }

      expect(li_fuel).to be_nil
    end
  end

  shared_examples 'softly delete all previous items built by Flat Rate' do
    it 'softly deletes all previous items built by Flat Rate' do
      sample_invoice = build_sample

      previous_flat_rate_lis = sample_invoice.line_items.select { |li| li.rate_id == rate.id }

      previous_flat_rate_lis.each do |previous_flat_li|
        expect(previous_flat_li.deleted_at).to be_present
        expect(previous_flat_li.saved_changes?).to be_falsey
      end
    end
  end

  shared_examples 'build new values for li_dolly item accordingly' do |expectations|
    it 'builds it correctly' do
      sample_invoice = build_sample
      li_dolly = sample_invoice.line_items.find { |li| li.description == 'Dolly' }

      expect(li_dolly.id).to eq original_li_addition_dolly.id
      expect(li_dolly.estimated).to be_falsey
      expect(li_dolly.net_amount.to_s).to eq "40.0"
      expect(li_dolly.original_quantity.to_s).to eq "1.0"
      expect(li_dolly.original_unit_price.to_s).to eq "5.0"
      expect(li_dolly.quantity.to_s).to eq "2.0"
      expect(li_dolly.tax_amount.to_s).to eq "0.0"
      expect(li_dolly.unit_price.to_s).to eq "20.0"
      expect(li_dolly.user_created).to be_falsey

      if li_dolly_changed_deleted_at
        expect(li_dolly.deleted_at).to be_present
      else
        expect(li_dolly.deleted_at).to be_nil
      end
    end

    context 'when li_dolly.calc_type is percent' do
      let(:rate_addition_calc_type) { 'percent' }

      it 'builds net_amount accordingly' do
        sample_invoice = build_sample
        li_dolly = sample_invoice.line_items.find { |li| li.description == 'Dolly' }

        # net_amount for percentage recalc is not triggered if item is deleted_at
        # so we bypass it
        if !li_dolly_changed_deleted_at
          expect(li_dolly.net_amount.to_s).to eq expectations[:value_if_percent_case].to_s
        end
      end
    end
  end

  shared_examples 'build new values for li_storage item accordingly' do
    it 'builds it correctly' do
      sample_invoice = build_sample
      li_storage = sample_invoice.line_items.find { |li| li.description == 'Storage' }

      expect(li_storage.id).to eq original_li_storage.id
      expect(li_storage.estimated).to be_falsey
      expect(li_storage.net_amount.to_s).to eq "240.0"
      expect(li_storage.original_quantity.to_s).to eq "1.0"
      expect(li_storage.original_unit_price.to_s).to eq "100.0"
      expect(li_storage.quantity.to_s).to eq "2.0"
      expect(li_storage.tax_amount.to_s).to eq "0.0"
      expect(li_storage.unit_price.to_s).to eq "120.0"
      expect(li_storage.user_created).to be_falsey
      expect(li_storage.auto_calculated_quantity).to be_falsey

      if li_storage_changed_deleted_at
        expect(li_storage.deleted_at).to be_present
      else
        expect(li_storage.deleted_at).to be_nil
      end
    end
  end

  shared_examples 'build new line_items for new_rate_type accordingly' do |expectations|
    it 'builds only new line_items based on the new RateType' do
      sample_invoice = build_sample

      li_p2p = sample_invoice.line_items.find { |li| li.description == 'Port To Port Hours' }
      expect(li_p2p.id).to be_nil
      expect(li_p2p.persisted?).to be_falsey
      expect(li_p2p.rate_id).to eq rate_p2p.id
      expect(li_p2p.net_amount.to_s).to eq expectations[:net_amount].to_s
      expect(li_p2p.unit_price.to_s).to eq expectations[:unit_price].to_s
      expect(li_p2p.quantity.to_s).to eq expectations[:quantity].to_s
      expect(li_p2p.user_created).to be_falsey
      expect(li_p2p.estimated).to be_truthy
      expect(li_p2p.deleted_at).to be_nil

      li_tax = sample_invoice.line_items.find { |li| li.description == 'Tax' }
      expect(li_tax.id).to be_nil
      expect(li_tax.persisted?).to be_falsey
      expect(li_tax.estimated).to be_truthy
      expect(li_tax.net_amount.to_s).to eq "0.0"
      expect(li_tax.original_quantity.to_s).to eq "0.0"
      expect(li_tax.original_unit_price.to_s).to eq "0.0"
      expect(li_tax.quantity.to_s).to eq "0.0"
      expect(li_tax.tax_amount.to_s).to eq "0.0"
      expect(li_tax.unit_price.to_s).to eq "0.0"
      expect(li_tax.user_created).to be_falsey
    end
  end

  shared_examples 'build new values for li_tax item accordingly' do
    it 'builds it correctly' do
      sample_invoice = build_sample

      li_tax = sample_invoice.line_items.find { |li| li.description == 'Tax' }
      expect(li_tax.id).to eq original_li_tax.id
      expect(li_tax.estimated).to be_falsey
      expect(li_tax.net_amount.to_s).to eq "0.0"
      expect(li_tax.original_quantity.to_s).to eq "0.0"
      expect(li_tax.original_unit_price.to_s).to eq "0.0"
      expect(li_tax.quantity.to_s).to eq "0.0"
      expect(li_tax.tax_amount.to_s).to eq "20.0"
      expect(li_tax.unit_price.to_s).to eq "0.0"
      expect(li_tax.user_created).to be_falsey
    end
  end

  shared_examples 'new expected invoice total, balance and line_item.size' do |expectations|
    it 'builds it correctly' do
      sample_invoice = build_sample

      expect(sample_invoice.total_amount.to_s).to eq expectations[:total_amount]
      expect(sample_invoice.balance.to_s).to eq expectations[:balance]
      expect(sample_invoice.line_items.size).to eq expectations[:line_items_size]
    end
  end

  before do
    original_invoice.payments_or_credits.build([
      original_payment.attributes, original_refund.attributes,
    ])
  end

  shared_examples 'build the original invoice line_items in the sample invoice' do
    it 'builds original invoice line_items in the sample' do
      sample_invoice = build_sample

      expect(sample_invoice.line_items).to include(*original_invoice.line_items.to_a)
    end
  end
end
