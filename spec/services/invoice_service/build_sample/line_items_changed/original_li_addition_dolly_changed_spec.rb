# frozen_string_literal: true

require_relative '../build_sample_shared'
require_relative '../valid_input_shared'

describe InvoiceService::BuildSample do
  include_context "build sample shared"

  context 'with valid input' do
    include_context "valid input shared"
    context 'when rate_type passed is the same as the original' do
      context 'and vehicle_category is the same as the original' do
        context 'when line_items are changed by line_items_hashes' do
          it_behaves_like 'original_invoice values correctly set'

          context 'when original_li_addition_dolly is changed by line_items_hashes' do
            let(:line_items_hashes) do
              [
                original_li_flat.attributes,
                li_dolly_changed.attributes,
                original_li_tax.attributes,
              ]
            end

            it_behaves_like 'keep same original_li_flat values'
            it_behaves_like 'build new values for li_dolly item accordingly', value_if_percent_case: 0.2
            it_behaves_like 'keep same original_li_tax values'
            it_behaves_like 'new expected invoice total, balance and line_item.size',
                            total_amount: '50.0', balance: '45.0', line_items_size: 3

            it_behaves_like 'not change invoices and their associations in the database'

            context 'with deleted_at set' do
              let(:li_dolly_changed_deleted_at) { Time.now }

              it_behaves_like 'keep same original_li_flat values'
              it_behaves_like 'build new values for li_dolly item accordingly', deleted: true
              it_behaves_like 'keep same original_li_tax values'
              it_behaves_like 'new expected invoice total, balance and line_item.size',
                              total_amount: '10.0', balance: '5.0', line_items_size: 3

              it_behaves_like 'not change invoices and their associations in the database'
            end
          end
        end
      end
    end
  end
end
