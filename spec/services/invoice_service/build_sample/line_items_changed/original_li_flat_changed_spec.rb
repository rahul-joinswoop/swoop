# frozen_string_literal: true

require_relative '../build_sample_shared'
require_relative '../valid_input_shared'

describe InvoiceService::BuildSample do
  include_context "build sample shared"

  context 'with valid input' do
    include_context "valid input shared"
    context 'when rate_type passed is the same as the original' do
      context 'and vehicle_category is the same as the original' do
        context 'when line_items are changed by line_items_hashes' do
          it_behaves_like 'original_invoice values correctly set'

          context 'when only original_li_flat is changed' do
            let(:line_items_hashes) do
              [
                li_flat_changed.attributes,
                original_li_addition_dolly.attributes,
                original_li_tax.attributes,
              ]
            end

            it_behaves_like 'build new values for li_flat item accordingly'
            it_behaves_like 'keep same original_li_addition_dolly values', value_if_percent_case: 2.0
            it_behaves_like 'keep same original_li_tax values'
            it_behaves_like 'new expected invoice total, balance and line_item.size',
                            total_amount: '45.0', balance: '40.0', line_items_size: 3

            it_behaves_like 'not change invoices and their associations in the database'

            context 'with deleted_at set' do
              let(:li_flat_changed_deleted_at) { Time.now }

              it_behaves_like 'build new values for li_flat item accordingly', deleted: true
              it_behaves_like 'keep same original_li_addition_dolly values', value_if_percent_case: 0.0
              it_behaves_like 'keep same original_li_tax values'
              it_behaves_like 'new expected invoice total, balance and line_item.size',
                              total_amount: '5.0', balance: '0.0', line_items_size: 3

              it_behaves_like 'not change invoices and their associations in the database'
            end
          end
        end
      end
    end
  end
end
