# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::CreateOrUpdateFleetCustomerInvoiceIfNecessary do
  include Requests::InvoiceHelpers

  subject(:coufci) do
    InvoiceService::CreateOrUpdateFleetCustomerInvoiceIfNecessary.new(job, job.customer)
  end

  let(:swoop_company) { create(:super_company) }

  let(:fleet_company) { create(:fleet_company, :turo) }

  let(:job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
      invoice: nil,
      account: nil,
      service_code: service_code,
    })
  end

  let!(:flat_rate) do
    create(:clean_flat_rate, {
      company: fleet_company,
      live: true,
      account: nil,
    })
  end

  def find_and_check_invoice
    customer_invoices = job.fleet_customer_live_invoices
    expect(customer_invoices.length).to eql(1)
    customer_invoices[0]
  end

  context 'no customer account' do
    let(:service_code) do
      create(:service_code, name: 'Tow')
    end

    it 'does not create invoice' do
      subject.call
      expect(job.fleet_customer_live_invoices.length).to eq(0)
    end
  end

  context 'customer account' do
    let!(:client_customer_account) do
      create(
        :account,
        name: 'Customer',
        company: fleet_company,
        client_company: nil
      )
    end

    context 'no service code' do
      let(:service_code) { nil }

      it 'does not create invoice' do
        subject.call
        expect(job.fleet_customer_live_invoices.length).to eq(0)
      end
    end

    context 'service code' do
      let(:service_code) do
        create(:service_code, name: 'Tow')
      end

      context 'no invoice' do
        before do
          subject.call
        end

        def find_and_check_invoice
          customer_invoices = job.fleet_customer_live_invoices
          expect(customer_invoices.length).to eql(1)
          customer_invoices[0]
        end

        it 'Creates invoice' do
          find_and_check_invoice
        end

        it 'sets sender is fleet' do
          invoice = find_and_check_invoice
          expect(invoice.sender).to eql(fleet_company)
        end

        it 'sets customer as recipient' do
          invoice = find_and_check_invoice
          expect(invoice.recipient).to eql(job.customer)
        end

        it 'sets invoice to fleet_customer_new state' do
          invoice = find_and_check_invoice
          expect(invoice.state).to eql(Invoice::FLEET_CUSTOMER_NEW)
        end

        it 'sets job on invoice' do
          invoice = find_and_check_invoice
          expect(invoice.job).to eql(job)
        end
      end

      context 'existing invoice' do
        let!(:invoice) do
          create(
            :invoice,
            state: 'fleet_customer_new',
            sender: fleet_company,
            recipient: job.customer,
            recipient_company: nil,
            job: job
          )
        end

        before do
          subject.call
        end

        it 'keeps existing invoice' do
          new_invoice = find_and_check_invoice
          expect(new_invoice).to eql(invoice)
        end

        it 'Adds all line items' do
          invoice.reload
          expect(invoice.line_items.length).to eql(2)
          expect(find_li(invoice, "Flat Rate").net_amount).to eql(75)
        end
      end
    end
  end
end
