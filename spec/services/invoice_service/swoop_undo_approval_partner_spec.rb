# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::SwoopUndoApprovalPartner do
  subject(:service) { described_class.new(invoice_id: partner_invoice_id) }

  let(:partner_invoice_id) { 1 }

  describe '.initialize' do
    it 'sets @partner_invoice_id' do
      expect(
        service.instance_variable_get(:@partner_invoice_id)
      ).to eq partner_invoice_id
    end

    context 'when partner_invoice_id is blank' do
      let(:partner_invoice_id) { ' ' }

      it 'raises error' do
        expect { service }.to raise_error InvoiceService::MissingInvoiceError
      end
    end
  end

  describe '#call' do
    subject(:service_call) { service.call }

    let(:partner_invoice) { Invoice.new }
    let(:may_swoop_undo_approval_partner) { true }

    before :each do
      allow(Invoice).to receive(:find_by_id).and_return(partner_invoice)
      allow_any_instance_of(Invoice).to receive(:swoop_undo_approval_partner)
      allow_any_instance_of(Invoice).to receive(
        :may_swoop_undo_approval_partner?
      ).and_return may_swoop_undo_approval_partner
      allow_any_instance_of(described_class).to receive(
        :flag_fleet_managed_invoice_to_be_deleted
      ).and_return nil
    end

    it 'sets @partner_invoice' do
      service_call

      expect(
        service.instance_variable_get(:@partner_invoice)
      ).to eq partner_invoice
    end

    it 'triggers swoop_undo_approval_partner event' do
      expect_any_instance_of(Invoice).to receive(:swoop_undo_approval_partner)

      service_call
    end

    it 'pushes the partner_invoice into mutated_objects' do
      service_call

      mutated_objects = service.instance_variable_get(:@mutated_objects)

      expect(mutated_objects.first).to eq partner_invoice
    end

    it 'flags fleet managed invoice to be deleted' do
      expect_any_instance_of(described_class).to receive(:flag_fleet_managed_invoice_to_be_deleted)

      service_call
    end
  end

  describe '#flag_fleet_managed_invoice_to_be_deleted' do
    subject(:service_flag_fleet) { service.send(:flag_fleet_managed_invoice_to_be_deleted) }

    let(:partner_invoice) { Invoice.new }
    let(:fleet_managed_invoice) { Invoice.new }
    let(:may_swoop_delete_fleet) { true }

    before :each do
      service.instance_variable_set(:@partner_invoice, partner_invoice)

      allow_any_instance_of(Invoice).to(
        receive(:find_active_fleet_managed_invoice)
      ).and_return fleet_managed_invoice
      allow_any_instance_of(Invoice).to(
        receive(:may_swoop_delete_fleet?)
      ).and_return may_swoop_delete_fleet
      allow_any_instance_of(Invoice).to(receive(:swoop_delete_fleet)).and_return nil
    end

    it 'searches for the active fleet managed invoice' do
      expect_any_instance_of(Invoice).to receive(:find_active_fleet_managed_invoice)

      service_flag_fleet
    end

    it 'triggers swoop_delete_fleet event' do
      expect_any_instance_of(Invoice).to receive(:swoop_delete_fleet)

      service_flag_fleet
    end

    it 'pushes the fleet_managed_invoice into mutated_objects' do
      service_flag_fleet

      mutated_objects = service.instance_variable_get(:@mutated_objects)

      expect(mutated_objects.first).to eq fleet_managed_invoice
    end
  end
end
