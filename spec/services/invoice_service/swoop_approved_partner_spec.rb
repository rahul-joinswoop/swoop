# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::SwoopApprovedPartner do
  subject(:service) { described_class.new(invoice_id: partner_invoice&.id) }

  let(:partner_invoice) { create(:invoice) }

  describe '.initialize' do
    it 'sets @partner_invoice_id' do
      expect(
        service.instance_variable_get(:@partner_invoice_id)
      ).to eq partner_invoice.id
    end

    context 'when partner_invoice_id is blank' do
      let(:partner_invoice) { nil }

      it 'raises error' do
        expect { service }.to raise_error InvoiceService::MissingInvoiceError
      end
    end
  end

  describe '#call' do
    subject(:service_call) { service.call }

    let(:partner_invoice) { create(:invoice) }
    let(:may_swoop_approved_partner) { true }

    before :each do
      allow(Invoice).to receive(:find_by!).and_return(partner_invoice)
      allow_any_instance_of(Invoice).to receive(:swoop_approved_partner)
      allow_any_instance_of(Invoice).to receive(
        :may_swoop_approved_partner?
      ).and_return may_swoop_approved_partner
      allow_any_instance_of(described_class).to receive(
        :create_swoop_outgoing_invoice
      ).and_return nil
    end

    it 'sets @partner_invoice' do
      service_call

      expect(
        service.instance_variable_get(:@partner_invoice)
      ).to eq partner_invoice
    end

    it 'triggers swoop_approved_partner event' do
      expect_any_instance_of(Invoice).to receive(:swoop_approved_partner)

      service_call
    end

    it 'pushes the partner_invoice into mutated_objects' do
      service_call

      mutated_objects = service.instance_variable_get(:@mutated_objects)

      expect(mutated_objects.first).to eq partner_invoice
    end

    it 'triggers creations of swoop outgoing invoice' do
      expect_any_instance_of(described_class).to receive(:create_swoop_outgoing_invoice)

      service_call
    end
  end

  describe '#create_swoop_outgoing_invoice' do
    subject(:create_swoop_outgoing_invoice) { service.send(:create_swoop_outgoing_invoice) }

    let(:partner_invoice) { create(:invoice, job: job) }
    let(:swoop_outgoing_invoice) { Invoice.new }
    let(:may_swoop_delete_fleet) { true }

    let(:account) { Account.new }
    let(:account_new_record?) { false }
    let(:swoop_company) { Company.new }
    let(:job) { create(:fleet_managed_job) }

    before :each do
      service.instance_variable_set(:@partner_invoice, partner_invoice)

      allow(Account).to receive(:find_by).and_return account
      allow_any_instance_of(Account).to receive(:new_record?).and_return account_new_record?
      allow(Company).to receive(:swoop).and_return swoop_company
      allow(Invoice).to receive(:new).and_return swoop_outgoing_invoice

      allow_any_instance_of(InvoiceService::NewSwoopInvoice).to(receive(:account_attributes)).and_return nil
      allow_any_instance_of(Invoice).to(receive(:job)).and_return job
      allow_any_instance_of(Invoice).to(receive(:swoop_delete_fleet)).and_return nil
    end

    it 'does not add rate_type to the Swoop invoice' do
      create_swoop_outgoing_invoice

      mutated_objects = service.instance_variable_get(:@mutated_objects)

      expect(mutated_objects.first.rate_type).to be_nil
    end

    it 'pushes the swoop_outgoing_invoice into mutated_objects' do
      create_swoop_outgoing_invoice

      mutated_objects = service.instance_variable_get(:@mutated_objects)

      expect(mutated_objects.first).to eq swoop_outgoing_invoice
    end

    it 'adds a Tax line_item to the Swoop invoice' do
      create_swoop_outgoing_invoice

      invoice = service.instance_variable_get(:@mutated_objects).first

      expect(invoice.line_items.size).to eq 1
      expect(invoice.line_items.first.description).to eq 'Tax'
      expect(invoice.line_items.first.net_amount).to eq nil
      expect(invoice.line_items.first.tax_amount).to eq nil
    end
  end
end
