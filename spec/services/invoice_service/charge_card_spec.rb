# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::ChargeCard do
  subject(:charge_card_service_call) do
    InvoiceService::ChargeCard.call(
      input: {
        invoice_id: invoice_id,
        company_id: company_id,
        user_id: user_id,
        invoice_charge_attributes: invoice_charge_attributes,
      },
    ).output
  end

  let!(:invoice)   { create(:invoice, sender_id: company.id) }
  let(:invoice_id) { invoice.id }
  let(:company_id) { invoice.sender_id }
  let!(:company)   { create(:rescue_company) }
  let!(:user)      { create(:user, company: company) }
  let(:user_id)    { user.id }
  let(:invoice_charge_attributes) do
    {
      token: '123456',
      amount: 100.25,
      send_recipient_to: 'customer@email.com',
      memo: 'A note for the payment',
    }
  end

  before do
    allow(Stripe::ChargeWorker).to receive(:perform_async)
  end

  it { is_expected.to be_a(API::AsyncRequest) }

  it 'schedules async_charge_worker' do
    api_async_request = charge_card_service_call

    expect(Stripe::ChargeWorker).to have_received(:perform_async)
      .with(
        api_async_request.id,
        {
          token: '123456',
          amount: 100.25,
          send_recipient_to: 'customer@email.com',
          memo: 'A note for the payment',
        },
        invoice_id
      )
  end

  context 'when company_id is blank' do
    let(:company_id) { nil }

    it 'raises ArgumentError' do
      expect { charge_card_service_call }.to raise_error(ArgumentError)
    end
  end

  context 'when user_id is blank' do
    let(:user_id) { nil }

    it 'raises ArgumentError' do
      expect { charge_card_service_call }.to raise_error(ArgumentError)
    end
  end

  context 'when invoice_id is blank' do
    let(:invoice_id) { nil }

    it 'raises ArgumentError' do
      expect { charge_card_service_call }.to raise_error(ArgumentError)
    end
  end

  context 'when invoice_charge_attributes is blank' do
    let(:invoice_charge_attributes) { nil }

    it 'raises ArgumentError' do
      expect { charge_card_service_call }.to raise_error(ArgumentError)
    end
  end
end
