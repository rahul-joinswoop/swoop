# frozen_string_literal: true

require "rails_helper"

describe InvoiceService::ApplyJobBillingInfo do
  let(:swoop_company) { create(:super_company) }

  let(:partner_company) do
    create(:rescue_company, {
      name: 'Finish Line Towing',
    })
  end

  let(:partners_swoop_account) do
    create(
      :account,
      name: 'Swoop',
      company: partner_company,
      client_company: swoop_company
    )
  end

  let(:invoice) do
    create(
      :invoice,
      state: 'partner_new',
      sender: partner_company,
      recipient: partners_swoop_account,
      recipient_company: partners_swoop_account.client_company
    )
  end

  let(:job) do
    create(:fleet_managed_job, {
      rescue_company: partner_company,
      invoice: invoice,
      account: partners_swoop_account,
      service_code: create(:service_code, name: 'Tow'),
    })
  end

  let!(:flat_rate) do
    create(:clean_flat_rate, {
      company: partner_company,
      live: true,
      account: partners_swoop_account,
    })
  end

  let(:agent) { create(:user, :dispatcher) }

  describe "billing_type" do
    it "sets the billing type of the invoice" do
      expect_any_instance_of(InvoiceService::ApplyJobBillingInfo).to receive(:call).and_call_original
      job.create_billing_info!(billing_type: BillingType::ACH, rescue_company: job.rescue_company, agent: agent)
      InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
      expect(invoice.billing_type).to eq BillingType::ACH
    end
  end

  describe BillingType::VCC do
    let!(:billing_info) { job.create_billing_info!(billing_type: BillingType::VCC, vcc_amount: 100, rescue_company: job.rescue_company, agent: agent) }

    describe "line items" do
      it "changes to invoice to a single flat rate line item if the invoice amount doesn't match the payment" do
        existing_line_item = create(:invoicing_line_item, :flat_rate, :estimated, unit_price: 60)
        invoice.line_items << existing_line_item
        InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
        expect(existing_line_item.deleted_at).not_to eq nil
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 1
        line_item = invoice.line_items.reject(&:deleted_at).first
        expect(invoice.rate_type).to eq FlatRate.name
        expect(line_item.quantity).to eq 1
        expect(line_item.unit_price).to eq 100
        expect(line_item.original_unit_price).to eq nil
        expect(line_item.net_amount).to eq 100
        expect(line_item.description).to eq FlatRate::FLAT_RATE
        expect(line_item.estimated?).to eq false
        expect(line_item.user_created?).to eq false
      end

      it "updates a flat rate invoice with a zero" do
        existing_line_item = create(:invoicing_line_item, :flat_rate, :estimated, unit_price: 0)
        tax_line_item = create(:invoicing_line_item, :tax, tax_amount: 0, unit_price: 0)
        invoice.line_items << existing_line_item
        invoice.line_items << tax_line_item
        InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
        expect(invoice.rate_type).to eq FlatRate.name
        expect(existing_line_item.deleted_at).to eq nil
        expect(tax_line_item.deleted_at).to eq nil
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 2
        line_item = (invoice.line_items.reject(&:deleted_at) - [tax_line_item]).first
        expect(line_item).to eq existing_line_item
        expect(line_item.quantity).to eq 1
        expect(line_item.unit_price).to eq 100
        expect(line_item.original_unit_price).to eq nil
        expect(line_item.net_amount).to eq 100
        expect(line_item.description).to eq FlatRate::FLAT_RATE
        expect(line_item.estimated?).to eq false
        expect(line_item.user_created?).to eq false
      end

      it "does not alter the line items if the generated invoice matches the payment amount" do
        line_item = create(:invoicing_line_item, :flat_rate, :estimated, unit_price: 50, quantity: 2)
        invoice.line_items << line_item
        InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
        line_item.reload
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 1
        expect(line_item.deleted_at).to eq nil
        expect(line_item.unit_price).to eq 50
        expect(line_item.quantity).to eq 2
      end

      it "keeps the correct billing info if the invoice is recalculated" do
        flat_rate = create(:flat_rate, company: partner_company, service_code: job.service_code, flat: 20)
        UpdateInvoiceForJob.new(invoice, job, flat_rate).call
        invoice.save!
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 2
        UpdateInvoiceForJob.new(invoice, job, flat_rate).call
        invoice.save!
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 2
      end

      it "does not re-add zero balance line items" do
        flat_rate = create(:flat_rate, company: partner_company, service_code: job.service_code, flat: 0)
        UpdateInvoiceForJob.new(invoice, job, flat_rate).call
        invoice.save!
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 2
        UpdateInvoiceForJob.new(invoice, job, flat_rate).call
        invoice.save!
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 2
      end

      it "does not mess with user created items when regenerating the invoice" do
        UpdateInvoiceForJob.new(invoice, job, flat_rate).call
        invoice.save!
        user_created_line_item = create(:invoicing_line_item, :flat_rate, :user_created, unit_price: 20)
        invoice.line_items << user_created_line_item
        invoice.save!
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 3
        expect(invoice.current_total_amount).to eq 120

        UpdateInvoiceForJob.new(invoice, job, flat_rate).call

        invoice.save!
        expect(invoice.line_items.reject(&:deleted_at).size).to eq 3
        expect(invoice.current_total_amount).to eq 120
      end
    end

    describe "payment" do
      it "does not record a payment without a vcc_amount" do
        billing_info.update!(vcc_amount: nil)
        InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
        expect(invoice.billing_type).to eq BillingType::VCC
        expect(invoice.payments).to be_empty
      end

      it "records a payment with the vcc_amount", freeze_time: true do
        InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
        expect(invoice.payments.size).to eq 1
        payment = invoice.payments.first
        expect(payment.description).to eq "VCC Payment"
        expect(payment.payment_method).to eq "Credit Card"
        expect(payment.total_amount).to eq(-100)
        expect(payment.mark_paid_at).to eq Time.current
      end

      it "does not double record payments" do
        2.times { InvoiceService::ApplyJobBillingInfo.new(job, invoice).call }
        expect(invoice.payments.size).to eq 1
        expect(invoice.payments.first.total_amount).to eq(-100)
      end

      it "records the payment on the job history" do
        job
        history_item_count = job.history_items.size
        2.times { InvoiceService::ApplyJobBillingInfo.new(job, invoice).call }
        expect(job.history_items.size).to eq history_item_count + 1
        history_item = job.history_items.last
        expect(history_item.user).to eq agent
        expect(history_item.target).to eq invoice.payments.first
        expect(history_item.title).to eq HistoryItem::INVOICE_EDITED_VCC_PAYMENT
      end
    end

    describe "persistence" do
      it "persists changes to the line items and payments when the invoice is saved" do
        existing_line_item = create(:invoicing_line_item, :flat_rate, :estimated, unit_price: 60)
        invoice.line_items << existing_line_item
        another_item = invoice.line_items.build
        InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
        invoice.save!
        expect(existing_line_item.reload.deleted_at).not_to eq nil
        expect(another_item).not_to be_persisted
        expect(invoice.line_items.not_deleted.first.net_amount).to eq 100
        expect(invoice.payments.first.total_amount).to eq(-100)
        expect(invoice.payments.first.target_history_items.map(&:title)).to eq [HistoryItem::INVOICE_EDITED_VCC_PAYMENT]
      end

      it "does not implicitly persist the invoice" do
        invoice = build(:invoice, state: 'partner_new', sender: partner_company, recipient: partners_swoop_account, recipient_company: partners_swoop_account.client_company)
        existing_line_item = build(:invoicing_line_item, :flat_rate, :estimated, unit_price: 60)
        invoice.line_items << existing_line_item
        InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
        expect(invoice.persisted?).to eq false
        invoice.save!
        invoice.reload
        expect(invoice.persisted?).to eq true
        expect(invoice.line_items.not_deleted.first.net_amount).to eq 100
        expect(invoice.payments.first.total_amount).to eq(-100)
        expect(invoice.line_items.not_deleted.first.net_amount).to eq 100
        expect(invoice.payments.first.target_history_items.map(&:title)).to eq [HistoryItem::INVOICE_EDITED_VCC_PAYMENT]
      end
    end
  end

  describe "GOA" do
    let!(:billing_info) { job.create_billing_info(rescue_company: job.rescue_company, goa_amount: 35, goa_agent: agent, billing_type: BillingType::VCC, vcc_amount: 100) }
    let(:goa_service_code) { create(:service_code, name: "GOA") }
    let!(:goa_rate) { create(:flat_rate, company: partner_company, account: partners_swoop_account, service_code: goa_service_code, live: true, site: nil, vendor_id: nil) }

    it "sets the GOA amount to the job billing info GOA amount if it exists" do
      job.status = "GOA"
      invoice.line_items << build(:invoicing_line_item, :estimated, rate: goa_rate, unit_price: 10)
      InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
      goa_line_item = invoice.line_items.detect { |item| item.rate&.goa? }
      expect(goa_line_item.unit_price).to eq 35
      expect(goa_line_item.net_amount).to eq 35
    end

    it "does not set the GOA amount on non-GOA invoices" do
      job.status = "GOA"
      invoice.line_items << build(:invoicing_line_item, :flat_rate, :estimated, unit_price: 10)
      InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
      goa_line_item = invoice.line_items.detect { |item| item.rate&.goa? }
      expect(goa_line_item).to eq nil
      expect(invoice.line_items.last.unit_price).to eq 10
    end

    it "does not set the GOA amount if it has already been set and overridden" do
      job.status = "GOA"
      invoice.line_items << build(:invoicing_line_item, :estimated, rate: goa_rate, unit_price: 10)
      InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
      goa_line_item = invoice.line_items.detect { |item| item.rate&.goa? }
      goa_line_item.unit_price = 50
      InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
      expect(goa_line_item.unit_price).to eq 50
      expect(goa_line_item.net_amount).to eq 50
    end

    it "adds an item to the job history about updating the GOA invoice" do
      InvoiceService::ApplyJobBillingInfo.new(job, invoice).call
      history_item_count = job.history_items.size
      job.status = "GOA"
      invoice.line_items << build(:invoicing_line_item, :estimated, rate: goa_rate, unit_price: 10)
      2.times { InvoiceService::ApplyJobBillingInfo.new(job, invoice).call }
      expect(job.history_items.size).to eq history_item_count + 1
      history_item = job.history_items.last
      expect(history_item.user).to eq agent
      expect(history_item.target).to eq invoice
      expect(history_item.title).to eq HistoryItem::INVOICE_EDITED_GOA_COST
    end
  end
end
