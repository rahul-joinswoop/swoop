# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::Cancel::CancelInvoiceForJob do
  include Requests::InvoiceHelpers

  subject do
    InvoiceService::Cancel::CancelInvoiceForJob.new(job).call
  end

  let(:swoop_company) { create(:super_company) }
  let(:fleet_company) { create(:fleet_company) }
  let(:rescue_company) { create(:rescue_company) }

  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
      status: "Canceled",
    }
  end

  context "does not have customer invoice" do
    it "does not try to cancel customer invoice" do
      expect_any_instance_of(InvoiceService::Cancel::CancelCustomerInvoice)
        .not_to receive(:call)

      subject
    end
  end

  context 'has customer invoice' do
    let!(:invoice) do
      create(:invoice, :with_line_items, {
        state: 'fleet_customer_new',
        sender: fleet_company,
        recipient: job.customer,
        recipient_company: nil,
        job: job,
      })
    end

    it 'zeroes invoice' do
      expect { subject && invoice.reload }.to change(invoice, :total_amount).from(100).to(0)
    end

    it 'leaves line items' do
      expect { subject && invoice.reload }.to avoid_changing(invoice.line_items, :count)
    end

    context 'without a fleet company' do
      let(:fleet_company) { create(:rescue_company) }

      it 'returns without calling InvoiceService::Cancel::CancelCustomerInvoice' do
        expect_any_instance_of(InvoiceService::Cancel::CancelCustomerInvoice)
          .not_to receive(:call)
        subject
      end
    end

    context 'status is enroute' do
      before do
        job.status = 'Pending'
        job.save!
        job.swoop_assign!
        job.partner_enroute!
        job.fleet_canceled!
      end

      let!(:late_fee_service) do
        create(:service_code, {
          name: ServiceCode::LATE_CANCEL_FEE,
        })
      end

      let!(:client_customer_account) do
        create(
          :account,
          name: 'Customer',
          company: fleet_company,
          client_company: nil
        )
      end

      it 'zeroes invoice' do
        expect { subject && invoice.reload }.to change(invoice, :total_amount).from(100).to(0)
      end

      context 'cancel rate set' do
        let!(:rate) do
          create(:clean_flat_rate, {
            company: fleet_company,
            account: client_customer_account,
            service_code: late_fee_service,
            flat: 45,
            live: true,
          })
        end

        it 'Adds Late Cancel Fee' do
          expect { subject && invoice.reload }.to change(invoice, :total_amount).from(100).to(45)
        end
      end
    end

    context 'job is GOAd' do
      before do
        job.status = 'Pending'
        job.save!
        job.swoop_assign!
        job.partner_goa!
      end

      let!(:goa_fee_service) do
        create(:service_code, {
          name: ServiceCode::GOA_FEE,
        })
      end

      let!(:client_customer_account) do
        create(
          :account,
          name: 'Customer',
          company: fleet_company,
          client_company: nil
        )
      end

      it 'zeroes invoice' do
        expect { subject && invoice.reload }.to change(invoice, :total_amount).from(100).to(0)
      end

      context 'goa rate set' do
        let!(:rate) do
          create(:clean_flat_rate, {
            company: fleet_company,
            account: client_customer_account,
            service_code: goa_fee_service,
            flat: 50,
            live: true,
          })
        end

        it 'Adds Late Cancel Fee' do
          expect { subject && invoice.reload }.to change(invoice, :total_amount).from(100).to(50)
        end
      end
    end

    context 'job is Deleted' do
      before do
        job.status = Job::DRAFT
        job.save!
        job.fleet_deleted!
      end

      let!(:client_customer_account) do
        create(
          :account,
          name: 'Customer',
          company: fleet_company,
          client_company: nil
        )
      end

      it 'zeroes invoice' do
        expect { subject && invoice.reload }.to change(invoice, :total_amount).from(100).to(0)
      end
    end
  end
end
