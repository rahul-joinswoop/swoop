# frozen_string_literal: true

require "rails_helper"

describe InvoiceService::Cancel::CancelCustomerInvoice do
  subject { described_class.new(job).call }

  let(:swoop_company) { create(:super_company) }
  let(:fleet_company) { create(:fleet_company) }
  let(:rescue_company) { create(:rescue_company) }
  let(:live_invoices) { [] }

  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      live_invoices: live_invoices,
      rescue_company: rescue_company,
      status: status,
    }
  end

  context "when job is Canceled" do
    let(:status) { Job::CANCELED }

    it "does not raise error" do
      expect { subject }.not_to raise_error
    end
  end

  context "when job is GOA" do
    let(:status) { Job::GOA }

    it "does not raise error" do
      expect { subject }.not_to raise_error
    end
  end

  context "when job is Deleted" do
    let(:status) { Job::DELETED }

    it "does not raise error" do
      expect { subject }.not_to raise_error
    end
  end

  context "when job is Pending" do
    let(:status) { Job::PENDING }

    it 'raises error when not in canceled/goa' do
      expect { subject }.to raise_error(ArgumentError)
    end
  end
end
