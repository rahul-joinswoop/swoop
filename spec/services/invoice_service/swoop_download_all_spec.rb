# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::SwoopDownloadAll do
  subject(:service) do
    InvoiceService::SwoopDownloadAll.new(
      report_params: report_params,
      api_company_id: api_company_id,
      api_user_id: api_user_id,
      sender_alias: sender_alias,
    )
  end

  let(:report_params) do
    { client_company_id: client_company.id, sender_id: sender_id }
  end
  let(:swoop_company) { create(:super_company) }
  let(:swoop_user) { create(:user, company_id: swoop_company.id) }

  let(:api_company_id) { swoop_company.id }
  let(:api_user_id) { swoop_user.id }

  context 'when sender_alias is :swoop' do
    let(:sender_alias) { :swoop }
    let(:sender_id) { swoop_company.id }

    let!(:invoices) do
      create_list(
        :invoice,
        2,
        state: 'swoop_approved_fleet',
        sender: swoop_company,
        recipient: swoop_and_fleet_account,
        recipient_company: swoop_and_fleet_account.client_company
      )
    end

    let(:client_company) { create(:fleet_company, :fleet_response) }

    let(:swoop_and_fleet_account) do
      create(
        :account,
        name: 'Fleet Response',
        company: swoop_company,
        client_company: client_company
      )
    end

    it 'moves the invoices state to swoop_sending_fleet' do
      service.call

      invoices.first.reload
      invoices.second.reload

      expect(invoices.first.state).to eq Invoice::SWOOP_SENDING_FLEET
      expect(invoices.second.state).to eq Invoice::SWOOP_SENDING_FLEET
    end

    it 'schedule InvoiceService::GenerateSwoopFleetInvoiceReportWorker' do
      expect(InvoiceService::GenerateSwoopFleetInvoiceReportWorker).to receive(:perform_async)

      service.call
    end

    it 'triggers WS send with all invoices moved to swoop_sending_fleet' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call)

      service.call
    end
  end

  context 'when sender_alias is :partner' do
    let(:sender_alias) { :partner }
    let(:sender_id) { partner_company.id }
    let(:partner_company) { create(:rescue_company) }

    let!(:invoices) do
      create_list(
        :invoice,
        2,
        state: 'swoop_approved_partner',
        sender: partner_company,
        recipient: partner_and_swoop_account,
        recipient_company: partner_and_swoop_account.client_company
      )
    end

    let(:client_company) { swoop_company }

    let(:partner_and_swoop_account) do
      create(
        :account,
        name: 'Partner and Swoop',
        company: partner_company,
        client_company: swoop_company
      )
    end

    it 'moves the invoices state to swoop_downloading_partner' do
      service.call

      invoices.first.reload
      invoices.second.reload

      expect(invoices.first.state).to eq Invoice::SWOOP_DOWNLOADING_PARTNER
      expect(invoices.second.state).to eq Invoice::SWOOP_DOWNLOADING_PARTNER
    end

    it 'schedule InvoiceService::GenerateSwoopFleetInvoiceReportWorker' do
      expect(InvoiceService::GenerateSwoopPartnerInvoiceReportWorker).to receive(:perform_async)

      service.call
    end

    it 'triggers WS send with all fetched invoices' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call)

      service.call
    end
  end

  context 'when params not valid' do
    let(:client_company) { create(:fleet_company, :fleet_response) }
    let(:sender_id) { swoop_company.id }
    let(:sender_alias) { :swoop } # can be either :swoop or :partner

    context 'when report_params is blank' do
      let(:report_params) { nil }

      it 'raises error' do
        expect { service }.to raise_error(ArgumentError)
      end
    end

    context 'when report_params[:client_company_id] is blank' do
      let(:report_params) do
        {
          client_company_id: nil,
        }
      end

      it 'raises error' do
        expect { service }.to raise_error(ArgumentError)
      end
    end

    context 'when report_params[:sender_id] is blank' do
      let(:report_params) do
        {
          client_company_id: swoop_company.id, sender_id: nil,
        }
      end

      it 'raises error' do
        expect { service }.to raise_error(ArgumentError)
      end
    end

    context 'when api_company_id is blank' do
      let(:api_company_id) { nil }

      it 'raises error' do
        expect { service }.to raise_error(ArgumentError)
      end
    end

    context 'when api_company_id is blank' do
      let(:api_user_id) { nil }

      it 'raises error' do
        expect { service }.to raise_error(ArgumentError)
      end
    end

    context 'when sender_alias is invalid' do
      let(:sender_alias) { :not_recognized }

      it 'raises error' do
        expect { service }.to raise_error(ArgumentError)
      end
    end
  end
end
