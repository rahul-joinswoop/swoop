# frozen_string_literal: true

require "rails_helper"

# Stripe testing data can be obtained from:
# https://stripe.com/docs/testing#cards
describe InvoiceService::ChargeProviders::Stripe::ChargeService, vcr: true do
  subject(:service_call) do
    InvoiceService::ChargeProviders::Stripe::ChargeService.call({
      input: {
        sender_id: invoice.sender_id,
        invoice_id: invoice.id,
        user_id: user.id,
        invoice_charge_attributes: invoice_charge_attributes,
      },
    })
  end

  let!(:invoice) do
    create(
      :invoice,
      :with_line_items,
      sender: rescue_company,
      job: job,
    )
  end

  let(:job) { create(:fleet_in_house_job, account: account) }

  let(:account) { create(:account) }

  let!(:rescue_company) do
    create(:rescue_company, name: 'Real Tow "More Than" Twenty Two Characters')
  end
  let!(:user) do
    user = create(:user, company: rescue_company)
    user.roles << role_admin

    user
  end

  let!(:company_invoice_charge_fee) do
    create(:company_invoice_charge_fee, company: rescue_company)
  end
  let!(:custom_account) do
    create(:custom_account, company: rescue_company)
  end
  let!(:role_permissions) do
    create(
      :company_role_permission,
      company: rescue_company,
      role: role_admin,
      permission_type: CompanyRolePermission::CHARGE
    )
  end
  let(:role_admin) { create(:role, :admin) }

  let(:invoice_charge_attributes) do
    {
      token: stripe_test_token,
      amount: charge_amount,
      send_recipient_to: 'customer@email.com',
      memo: 'A note for the payment',
    }
  end

  let(:stripe_test_token) { 'tok_visa' } # this was taken from Stripe's testing data ^

  let(:charge_amount) { 100 }

  let(:slack_message) do
    [
      invoice.sender.name,
      invoice.recipient_type == 'Account' ? invoice.recipient.name : 'Cash Call',
      invoice.charges.last.updated_at,
      invoice.charges.last.slack_status_message,
    ].join(" | ")
  end

  let(:slack_tags) { [:payment] }
  let(:slack_args) { [slack_message, tags: slack_tags] }

  before do |example|
    allow(SlackChannel).to receive(:message)
    allow_any_instance_of(Invoice).to receive(:send_charge_card_receipt)

    unless example.metadata[:skip_before]
      allow(Stripe::Charge).to receive(:create).and_call_original

      service_call

      invoice.reload
    end
  end

  it 'calls Stripe charge API' do
    expect(Stripe::Charge).to have_received(:create).with(
      {
        amount: 10000,
        currency: invoice.currency,
        source: "tok_visa",
        statement_descriptor: "Real Tow More Than Twe", # stripe limits it into 22 chars
        description: invoice.job_id,
        metadata: {
          job_id: invoice.job_id,
          invoice_payment_charge_id: invoice.charges.first.id,
        },
        destination: {
          amount: 9680,
          # @see https://github.com/joinswoop/swoop/wiki/Stripe-Setup
          # the account value can be obtained from Stripe account, after a Bank Account is linked:
          account: "acct_1CWnkwF0C1gUdX8T",
        },
      }
    )
    expect(SlackChannel).to have_received(:message).with(*slack_args)
  end

  it 'adds one payment with charge to the invoice' do
    payment = invoice.payments.first

    expect(invoice.payments.size).to eq 1
    expect(payment.total_amount.to_s).to eq "-100.0"
    expect(payment.description).to eq 'A note for the payment'
  end

  it 'adds a respective charge to the payment' do
    payment_charge = invoice.payments.first.charge

    expect(payment_charge.last_4).to eq "4242" # VCR, data that comes from Stripe:
    expect(payment_charge.card_brand).to eq "Visa"
    expect(payment_charge.invoice_id).to eq invoice.id
    expect(payment_charge.upstream_charge_id).to be_present
    expect(payment_charge.data).to be_present
    expect(payment_charge.status).to eq 'charge_successful'
    expect(payment_charge.fee.to_s).to eq '3.2'
    expect(payment_charge.charge_user.id).to eq user.id
    expect(payment_charge.charge_receipt_email_sent).to be_truthy
  end

  it 'calls Analytics with expected params', :skip_before do
    expect(Analytics).to receive(:track).with({
      user_id: user.to_ssid,
      event: "Charge Card - Success",
      properties: {
        account: invoice.job.account.name,
        category: "Payments",
        jobId: invoice.job.id,
        service: job.service_code.name,
        value: '100.0',
      },
    })

    service_call
  end

  it 'sends expected message through slack' do
    expect(SlackChannel).to have_received(:message).with(*slack_args)
  end

  it 'triggers email receipt sending', :skip_before do
    expect_any_instance_of(Invoice).to receive(:send_charge_card_receipt).with(
      to: 'customer@email.com'
    )

    service_call
  end

  context 'when send_recipient_to is blank' do
    let(:invoice_charge_attributes) do
      {
        token: stripe_test_token,
        amount: charge_amount,
        send_recipient_to: nil,
        memo: 'A note for the payment',
      }
    end

    it 'does not trigger email receipt sending', :skip_before do
      expect_any_instance_of(Invoice).not_to receive(:send_charge_card_receipt)

      service_call
    end

    it 'does not set charge.charge_receipt_email_sent as true' do
      payment_charge = invoice.payments.first.charge

      expect(payment_charge.charge_receipt_email_sent).not_to be_truthy
    end
  end

  context 'when charge value is higher than invoice.balance' do
    let(:charge_amount) { 101 }

    before do
      allow(Stripe::Charge).to receive(:create).and_call_original
    end

    it 'sets service_call.failure with appropriate error' do
      context_result = service_call

      expect(context_result.failure?).to be_truthy
      expect(context_result.error.class.name).to eq(
        'InvoiceService::ChargeProviders::Stripe::InvalidAmountError'
      )
      expect(context_result.error.message).to eq(
        'Amount cannot exceed balance'
      )
      expect(SlackChannel).to have_received(:message).with(*slack_args)
    end

    it 'does not call Stripe endpoint' do
      expect(Stripe::Charge).not_to have_received(:create)
    end

    it 'does not add the payment to the invoice' do
      expect(invoice.payments.size).to eq 0
    end

    it 'adds an invoice_payment_charge' do
      invoice.reload

      payment_charge = invoice.charges.first

      expect(payment_charge).to be_present
      expect(payment_charge.status).to eq 'charge_error'
      expect(payment_charge.error_msg).to eq 'Amount cannot exceed balance'
    end

    it 'does not trigger email receipt sending', :skip_before do
      expect_any_instance_of(Invoice).not_to receive(:send_charge_card_receipt)

      service_call
    end
  end

  context 'when there is an error with the card on Stripe end' do
    # this was taken from Stripe's testing data, and forces a card error

    before do
      allow(Stripe::Charge).to receive(:create).and_call_original
    end

    context 'when error has no decline_code' do
      let(:stripe_test_token) { 'tok_chargeDeclinedExpiredCard' }

      it 'sets service_call.failure with appropriate error' do
        context_result = service_call

        expect(context_result.failure?).to be_truthy
        expect(context_result.error.class.name).to eq(
          'Stripe::CardError'
        )
        expect(context_result.error.message).to eq(
          'Your card has expired.'
        )
        expect(SlackChannel).to have_received(:message).with(*slack_args)
      end

      it 'does not add the payment to the invoice' do
        expect(invoice.payments.size).to eq 0
      end

      it 'adds an invoice_payment_charge' do
        invoice.reload

        payment_charge = invoice.charges.first

        expect(payment_charge).to be_present
        expect(payment_charge.status).to eq 'charge_error'
        expect(payment_charge.error_msg).to eq 'expired_card'
      end

      it 'does not trigger email receipt sending', :skip_before do
        expect_any_instance_of(Invoice).not_to receive(:send_charge_card_receipt).with(
          to: 'customer@email.com'
        )

        service_call
      end

      it 'calls Analytics with expected params', :skip_before do
        expect(Analytics).to receive(:track).with({
          user_id: user.to_ssid,
          event: "Charge Card - Failure",
          properties: {
            account: invoice.job.account.name,
            category: "Payments",
            jobId: invoice.job.id,
            label: "expired_card",
            service: job.service_code.name,
            value: '100.0',
          },
        })

        service_call
      end
    end

    context 'when error has a decline_code' do
      let(:stripe_test_token) { 'tok_chargeDeclinedFraudulent' }

      it 'sets service_call.failure with appropriate error' do
        context_result = service_call

        expect(context_result.failure?).to be_truthy
        expect(context_result.error.class.name).to eq(
          'Stripe::CardError'
        )
        expect(context_result.error.message).to eq(
          'Your card was declined.'
        )
        expect(SlackChannel).to have_received(:message).with(*slack_args)
      end

      it 'does not add the payment to the invoice' do
        expect(invoice.payments.size).to eq 0
      end

      it 'adds an invoice_payment_charge' do
        invoice.reload

        payment_charge = invoice.charges.first

        expect(payment_charge).to be_present
        expect(payment_charge.status).to eq 'charge_error'
        expect(payment_charge.error_msg).to eq 'fraudulent'
      end

      it 'does not trigger email receipt sending', :skip_before do
        expect_any_instance_of(Invoice).not_to receive(:send_charge_card_receipt).with(
          to: 'customer@email.com'
        )

        service_call
      end

      it 'calls Analytics with expected params', :skip_before do
        expect(Analytics).to receive(:track).with({
          user_id: user.to_ssid,
          event: 'Charge Card - Failure',
          properties: {
            account: invoice.job.account.name,
            category: "Payments",
            jobId: invoice.job.id,
            label: "fraudulent",
            service: invoice.job.service_code.name,
            value: "100.0",
          },
        })

        service_call
      end
    end
  end

  context 'when there is a generic error', :skip_before do
    before do
      allow(Stripe::Charge).to receive(:create).and_raise(
        Stripe::APIConnectionError, 'Stripe APIConnectionError'
      )
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
    end

    it 'sets service_call.failure with appropriate error' do
      context_result = service_call

      expect(context_result.failure?).to be_truthy
      expect(context_result.error.class.name).to eq 'Stripe::APIConnectionError'
      expect(context_result.error.message).to eq 'Stripe APIConnectionError'
      expect(SlackChannel).to have_received(:message).with(*slack_args)
    end

    it 'does not add the payment to the invoice' do
      service_call

      expect(invoice.payments.size).to eq 0
    end

    it 'adds an invoice_payment_charge' do
      service_call

      invoice.reload

      payment_charge = invoice.charges.first

      expect(payment_charge).to be_present
      expect(payment_charge.status).to eq 'charge_error'
      expect(payment_charge.error_msg).to eq 'Stripe APIConnectionError'
    end

    it 'does not trigger email receipt sending', :skip_before do
      expect_any_instance_of(Invoice).not_to receive(:send_charge_card_receipt).with(
        to: 'customer@email.com'
      )

      service_call
    end
  end

  context 'when the bank account is not linked', :skip_before do
    let(:retrivier_double) do
      double(retrieve: nil)
    end

    before do
      allow(StripeIntegration::BankAccountRetriever).to(
        receive(:new)
        .and_return(retrivier_double)
      )
    end

    it 'sets service_call.failure with appropriate error' do
      context_result = service_call

      expect(context_result.failure?).to be_truthy
      expect(context_result.error.class.name).to eq(
        'InvoiceService::ChargeProviders::Stripe::InvalidBankAccountError'
      )
      expect(context_result.error.message).to eq(
        'Bank account not linked'
      )
      expect(SlackChannel).to have_received(:message).with(*slack_args)
    end

    it 'does not add the payment to the invoice' do
      expect(invoice.payments.size).to eq 0

      service_call
    end

    it 'adds an invoice_payment_charge' do
      service_call

      invoice.reload

      payment_charge = invoice.charges.first

      expect(payment_charge).to be_present
      expect(payment_charge.status).to eq 'charge_error'
      expect(payment_charge.error_msg).to eq 'Bank account not linked'
    end

    it 'does not trigger email receipt sending', :skip_before do
      expect_any_instance_of(Invoice).not_to receive(:send_charge_card_receipt).with(
        to: 'customer@email.com'
      )

      service_call
    end
  end

  context 'when the user role is not authorized' do
    let!(:user) do
      user = create(:user, company: rescue_company)
      user.roles << role_dispatcher

      user
    end

    let(:role_dispatcher) { create(:role, :dispatcher) }

    it 'sets service_call.failure with appropriate error' do
      context_result = service_call

      expect(context_result.failure?).to be_truthy
      expect(context_result.error.class.name).to eq(
        'InvoiceService::ChargeProviders::Stripe::UserNotAuthorizedError'
      )
      expect(context_result.error.message).to eq(
        'User role not authorized'
      )
      expect(SlackChannel).to have_received(:message).with(*slack_args)
    end

    it 'does not add the payment to the invoice' do
      expect(invoice.payments.size).to eq 0
    end

    it 'adds an invoice_payment_charge' do
      invoice.reload

      payment_charge = invoice.charges.first

      expect(payment_charge).to be_present
      expect(payment_charge.status).to eq 'charge_error'
      expect(payment_charge.error_msg).to eq 'User role not authorized'
    end

    it 'does not trigger email receipt sending', :skip_before do
      expect_any_instance_of(Invoice).not_to receive(:send_charge_card_receipt).with(
        to: 'customer@email.com'
      )

      service_call
    end
  end
end
