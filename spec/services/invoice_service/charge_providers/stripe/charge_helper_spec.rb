# frozen_string_literal: true

require "rails_helper"

describe InvoiceService::ChargeProviders::Stripe::ChargeHelper do
  before do
    stub_class 'ChargeHelperImpl' do
      include InvoiceService::ChargeProviders::Stripe::ChargeHelper

      attr_reader :invoice

      def initialize(invoice_charge_attributes, invoice)
        @invoice_charge_attributes = invoice_charge_attributes
        @invoice = invoice
      end
    end
  end

  let(:invoice_charge_attributes) do
    {
      amount: charge_amount,
    }
  end

  let(:invoice) { create(:invoice, sender: rescue_company) }
  let(:rescue_company) { create(:rescue_company) }

  describe '#charge_fee' do
    subject(:calculated_charge_fee) do
      ChargeHelperImpl.new(invoice_charge_attributes, invoice).charge_fee
    end

    context 'when charge_amount is 100' do
      let(:charge_amount) { BigDecimal(100) }

      let!(:company_invoice_charge_fee) do
        create(
          :company_invoice_charge_fee,
          company: rescue_company,
          fixed_value: fixed_value,
          percentage: percentage
        )
      end

      context 'when invoice.sender.invoice_charge_fee.fixed_value is 5' do
        let(:fixed_value) { BigDecimal(5) }

        context 'and invoice.sender.invoice_charge_fee.percentage is 1' do
          let(:percentage) { BigDecimal(1) }

          it 'results in 6.0' do
            expect(calculated_charge_fee.to_s).to eq '6.0'
          end
        end

        context 'and invoice.sender.invoice_charge_fee.percentage is 0' do
          let(:percentage) { BigDecimal(0) }

          it 'results in 5.0' do
            expect(calculated_charge_fee.to_s).to eq '5.0'
          end
        end
      end

      context 'when invoice.sender.invoice_charge_fee.fixed_value is 0' do
        let(:fixed_value) { BigDecimal(0) }

        context 'and invoice.sender.invoice_charge_fee.percentage is 3.2' do
          let(:percentage) { BigDecimal(3.2, 2) }

          it 'results in 3.2' do
            expect(calculated_charge_fee.to_s).to eq '3.2'
          end
        end

        context 'and invoice.sender.invoice_charge_fee.percentage is 0' do
          let(:percentage) { BigDecimal(0) }

          it 'results in 0.0' do
            expect(calculated_charge_fee.to_s).to eq '0.0'
          end
        end
      end
    end

    # this tests rounding cases
    context 'when charge_amount is 541.78' do
      let(:charge_amount) { BigDecimal("541.78", 2) }

      let!(:company_invoice_charge_fee) do
        create(
          :company_invoice_charge_fee,
          company: rescue_company,
          fixed_value: fixed_value,
          percentage: percentage
        )
      end

      context 'when invoice.sender.invoice_charge_fee.fixed_value is 0.3' do
        let(:fixed_value) { BigDecimal("0.3", 2) }

        context 'and invoice.sender.invoice_charge_fee.percentage is 2.9' do
          let(:percentage) { BigDecimal("2.9", 2) }

          it 'results in 16.01' do
            expect(calculated_charge_fee.to_s).to eq "16.01"
          end
        end
      end
    end
  end

  # stripe receives it as integer cents, so we multiply it by 100 and cast to int
  describe '#destination_amount' do
    subject(:calculated_destination_amount) do
      ChargeHelperImpl.new(invoice_charge_attributes, invoice).destination_amount
    end

    let!(:company_invoice_charge_fee) do
      create(
        :company_invoice_charge_fee,
        company: rescue_company,
        fixed_value: fixed_value,
        percentage: percentage
      )
    end

    context 'when charge_amount is 100' do
      let(:charge_amount) { BigDecimal(100) }

      context 'when invoice.sender.invoice_charge_fee.fixed_value is 5' do
        let(:fixed_value) { BigDecimal(5) }

        context 'and invoice.sender.invoice_charge_fee.percentage is 1' do
          let(:percentage) { BigDecimal(1) }

          it 'results in 9400' do
            expect(calculated_destination_amount.to_s).to eq '9400'
          end
        end

        context 'and invoice.sender.invoice_charge_fee.percentage is 0' do
          let(:percentage) { BigDecimal(0) }

          it 'results in 9500' do
            expect(calculated_destination_amount.to_s).to eq '9500'
          end
        end
      end

      context 'when invoice.sender.invoice_charge_fee.fixed_value is 0' do
        let(:fixed_value) { BigDecimal(0) }

        context 'and invoice.sender.invoice_charge_fee.percentage is 3.2' do
          let(:percentage) { BigDecimal(3.2, 2) }

          it 'results in 9680' do
            expect(calculated_destination_amount.to_s).to eq '9680'
          end
        end

        context 'and invoice.sender.invoice_charge_fee.percentage is 0' do
          let(:percentage) { BigDecimal(0) }

          it 'results in 10000' do
            expect(calculated_destination_amount.to_s).to eq '10000'
          end
        end
      end
    end

    # this tests rounding cases
    context 'when charge_amount is 541.78' do
      let(:charge_amount) { BigDecimal("541.78", 2) }

      context 'when invoice.sender.invoice_charge_fee.fixed_value is 0.3' do
        let(:fixed_value) { BigDecimal("0.3", 2) }

        context 'and invoice.sender.invoice_charge_fee.percentage is 2.9' do
          let(:percentage) { BigDecimal("2.9", 2) }

          it 'results in 525.77' do
            expect(calculated_destination_amount.to_s).to eq '52577'
          end
        end
      end
    end
  end

  describe '#stripe_card_error_code_from_exception' do
    subject(:stripe_card_error_code_from_exception) do
      ChargeHelperImpl.new(
        invoice_charge_attributes, invoice
      ).stripe_card_error_code_from_exception(card_error)
    end

    let(:charge_amount) { BigDecimal(100) }
    let(:card_error) do
      Stripe::CardError.new(
        'message to be displayed', 'param', 422, json_body: { error: json_body_error }
      )
    end

    context 'when StripeError comes with a decline_code' do
      let(:json_body_error) do
        {
          decline_code: 'fraudulent',
          code: 'card_declined',
        }
      end

      it { is_expected.to eq 'fraudulent' }
    end

    context 'when StripeError does not come with a decline_code' do
      let(:json_body_error) do
        {
          code: 'expired_card',
        }
      end

      it { is_expected.to eq 'expired_card' }
    end

    context 'when given Exception is not an instance of Stripe::CardError' do
      let(:card_error) do
        Stripe::StripeError.new('any')
      end

      it { is_expected.to eq '' }
    end
  end
end
