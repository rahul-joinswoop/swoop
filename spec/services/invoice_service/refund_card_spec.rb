# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::RefundCard do
  subject(:refund_card_service_call) do
    InvoiceService::RefundCard.call(
      input: {
        invoice_id: invoice_id,
        payment_id: invoice_payment_id,
        company_id: company_id,
        user_id: user_id,
      },
    ).output
  end

  let!(:invoice)   { create(:invoice, :with_payments, sender_id: company.id) }
  let(:invoice_id) { invoice.id }
  let(:invoice_payment_id) { invoice.payments.first.id }
  let(:company_id) { invoice.sender_id }
  let!(:company)   { create(:rescue_company) }
  let!(:user)      { create(:user, company: company) }
  let(:user_id)    { user.id }

  before do
    allow(Stripe::RefundWorker).to receive(:perform_async)
  end

  it { is_expected.to be_a(API::AsyncRequest) }

  it 'schedules async_refund_worker' do
    api_async_request = refund_card_service_call

    expect(Stripe::RefundWorker).to(
      have_received(:perform_async).with(
        api_async_request.id,
        invoice_id,
        invoice_payment_id
      )
    )
  end

  context 'when company_id is blank' do
    let(:company_id) { nil }

    it 'raises ArgumentError' do
      expect { refund_card_service_call }.to raise_error(ArgumentError)
    end
  end

  context 'when user_id is blank' do
    let(:user_id) { nil }

    it 'raises ArgumentError' do
      expect { refund_card_service_call }.to raise_error(ArgumentError)
    end
  end

  context 'when invoice_id is blank' do
    let(:invoice_id) { nil }

    it 'raises ArgumentError' do
      expect { refund_card_service_call }.to raise_error(ArgumentError)
    end
  end

  context 'when invoice_payment_id is blank' do
    let(:invoice_payment_id) { nil }

    it 'raises ArgumentError' do
      expect { refund_card_service_call }.to raise_error(ArgumentError)
    end
  end
end
