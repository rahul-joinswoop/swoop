# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::SwoopExportAllToFleet do
  subject(:service) do
    InvoiceService::SwoopExportAllToFleet.new(
      api_company_id: swoop_company.id,
      api_user_id: swoop_user.id,
      client_company_id: fleet_managed_company.id
    )
  end

  let(:swoop_invoice_one) do
    create(
      :invoice,
      state: 'swoop_approved_fleet',
      sender: swoop_company,
      recipient: swoop_and_fleet_account,
      recipient_company: swoop_and_fleet_account.client_company,
      job: fleet_managed_job,
      export_error_msg: export_error_msg
    )
  end

  let(:swoop_invoice_two) do
    create(
      :invoice,
      state: 'swoop_approved_fleet',
      sender: swoop_company,
      recipient: swoop_and_fleet_account,
      recipient_company: swoop_and_fleet_account.client_company,
      job: fleet_managed_job,
      export_error_msg: export_error_msg
    )
  end

  let(:swoop_company) { create(:super_company) }

  let(:swoop_user) { create(:user, company_id: swoop_company.id) }

  let(:fleet_managed_company) { create(:fleet_company, :fleet_response) }

  let(:fleet_managed_job) { create(:fleet_managed_job, service_location: create(:location, state: 'CA')) }

  let(:export_error_msg) { nil }

  let(:swoop_and_fleet_account) do
    create(
      :account,
      name: 'Fleet Response',
      company: swoop_company,
      client_company: fleet_managed_company
    )
  end

  before :each do
    swoop_invoice_one
    swoop_invoice_two

    allow_any_instance_of(InvoiceService::SwoopExportAllToFleet).to(
      receive(:invoices_with_state_array).and_return([swoop_invoice_one.id, swoop_invoice_two.id])
    )
  end

  it 'creates an AsyncRequest that will be used to keep track of the whole process' do
    allow(API::AsyncRequest).to receive(:create!).and_call_original

    expect(API::AsyncRequest).to receive(:create!).with({
      company_id: swoop_company.id,
      user_id: swoop_user.id,
      request: {
        method: 'SwoopExportAllToFleet',
        params: {
          api_company_id: swoop_company.id,
          api_user_id: swoop_user.id,
          client_company_id: fleet_managed_company.id,
        },
      },
      system: API::AsyncRequest::FLEET_RESPONSE_INVOICE,
    })

    service.call
  end

  it 'moves invoice states to swoop_sending_fleet' do
    service.call

    swoop_invoice_one.reload
    swoop_invoice_two.reload

    expect(swoop_invoice_one.state).to eq Invoice::SWOOP_SENDING_FLEET
    expect(swoop_invoice_two.state).to eq Invoice::SWOOP_SENDING_FLEET
  end

  context 'when invoice has export_error_msg' do
    let(:export_error_msg) { 'An error' }

    it 'cleans the export_error_msg' do
      service.call

      swoop_invoice_one.reload
      swoop_invoice_two.reload

      expect(swoop_invoice_one.export_error_msg).to be_nil
      expect(swoop_invoice_two.export_error_msg).to be_nil
    end
  end

  it 'schedules InvoiceService::ExportToFleet::FleetResponse::ExportAllWorker async job' do
    allow(InvoiceService::ExportToFleet::FleetResponse::ExportAllWorker).to receive(:perform_async)

    expect(InvoiceService::ExportToFleet::FleetResponse::ExportAllWorker).to receive(:perform_async)

    service.call
  end

  it 'pushes WS message to FE containing the invoices found' do
    allow(Publishers::GenericWebsocketMessage).to receive(:call)

    expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
      input: {
        class_name: 'PartialObjectArray',
        target_data: {
          partial_invoices: [swoop_invoice_one.id, swoop_invoice_two.id],
        },
        company: Company.find(swoop_company.id),
      }
    )

    service.call
  end
end
