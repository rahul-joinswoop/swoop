# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::MarkPaid do
  subject(:service) do
    InvoiceService::MarkPaid.new(
      invoice_id: invoice_id,
      api_company_id: company_id,
      paid: paid
    )
  end

  let(:paid) { true }

  let(:invoice) do
    create(
      :invoice,
      state: 'swoop_approved_fleet',
      sender: swoop_company,
      recipient: swoop_and_fleet_account,
      recipient_company: swoop_and_fleet_account.client_company
    )
  end

  let(:swoop_company) { create(:super_company) }

  let(:swoop_user) { create(:user, company_id: invoice.sender_id) }

  let(:fleet_managed_company) { create(:fleet_company, :fleet_response) }
  let(:swoop_and_fleet_account) do
    create(
      :account,
      name: 'Fleet Response',
      company: swoop_company,
      client_company: fleet_managed_company
    )
  end

  let(:invoice_id) { invoice.id }

  let(:company_id) { swoop_company.id }

  it 'sets invoice as paid' do
    expect(invoice.paid).to be_falsey

    service.call.save_mutated_objects

    expect(service.invoice.paid).to be_truthy
  end

  context 'when invoice.balance > 0' do
    before :each do
      allow_any_instance_of(Invoice).to receive(:balance).and_return(BigDecimal.new("10.5"))
    end

    it 'clears the balance by setting a new payment with a negative balance value' do
      expect(invoice.balance.to_s).to eq "10.5"

      service.call.save_mutated_objects

      expect(service.invoice.payments.first.total_amount.to_s).to eq "-10.5"
    end
  end

  context 'when partner_invoice_id is blank' do
    let(:invoice_id) { nil }

    it 'raises error' do
      expect { service }.to raise_error InvoiceService::MissingInvoiceError
    end
  end

  context 'when partner_invoice_id is blank' do
    let(:company_id) { nil }

    it 'raises error' do
      expect { service }.to raise_error ArgumentError
    end
  end
end
