# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::PartnerMoveBackToNew do
  subject(:service) { described_class.new(invoice_id: partner_invoice_id) }

  let(:partner_invoice_id) { 1 }

  describe '.initialize' do
    it 'sets @partner_invoice_id' do
      expect(
        service.instance_variable_get(:@partner_invoice_id)
      ).to eq partner_invoice_id
    end

    context 'when partner_invoice_id is blank' do
      let(:partner_invoice_id) { ' ' }

      it 'raises error' do
        expect { service }.to raise_error InvoiceService::MissingInvoiceError
      end
    end
  end

  describe '#call' do
    subject(:service_call) { service.call }

    let(:partner_invoice) { Invoice.new }
    let(:may_partner_move_back_to_new) { true }

    before :each do
      allow(Invoice).to receive(:find_by_id).and_return(partner_invoice)
      allow_any_instance_of(Invoice).to receive(:partner_move_back_to_new)
      allow_any_instance_of(Invoice).to receive(:reset_paid)
      allow_any_instance_of(Invoice).to receive(
        :may_partner_move_back_to_new?
      ).and_return may_partner_move_back_to_new
    end

    it 'sets @partner_invoice' do
      service_call

      expect(
        service.instance_variable_get(:@partner_invoice)
      ).to eq partner_invoice
    end

    it 'triggers partner_move_back_to_new event' do
      expect_any_instance_of(Invoice).to receive(:partner_move_back_to_new)

      service_call
    end

    it 'pushes the partner_invoice into mutated_objects' do
      service_call

      mutated_objects = service.instance_variable_get(:@mutated_objects)

      expect(mutated_objects.first).to eq partner_invoice
    end

    it 'reset invoice paid attribute' do
      expect_any_instance_of(Invoice).to receive(:reset_paid)

      service_call
    end

    context 'when partner_move_back_to_new is not allowed' do
      let(:may_partner_move_back_to_new) { false }

      it 'raises error' do
        expect { service_call }.to raise_error InvoiceService::InvoiceStateTransitionError
      end
    end
  end
end
