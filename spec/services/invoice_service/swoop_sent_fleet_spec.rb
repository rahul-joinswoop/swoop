# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::SwoopSentFleet do
  subject(:service) do
    InvoiceService::SwoopSentFleet.new(
      invoice_id: swoop_invoice.id,
      api_company_id: swoop_invoice.sender_id,
      api_user_id: swoop_user.id
    )
  end

  let(:swoop_invoice) do
    create(
      :invoice,
      state: 'swoop_approved_fleet',
      sender: swoop_company,
      recipient: swoop_and_fleet_account,
      recipient_company: swoop_and_fleet_account.client_company
    )
  end

  let(:swoop_company) { create(:super_company) }

  let(:swoop_user) { create(:user, company_id: swoop_invoice.sender_id) }

  let(:fleet_managed_company) { create(:fleet_company, :fleet_response) }
  let(:swoop_and_fleet_account) do
    create(
      :account,
      name: 'Fleet Response',
      company: swoop_company,
      client_company: fleet_managed_company
    )
  end

  it 'moves invoice.state to swoop_sending_fleet' do
    service.call

    swoop_invoice.reload

    expect(swoop_invoice.state).to eq 'swoop_sending_fleet'
  end

  context 'when fleet company is allowed to the export invoice flow' do
    context 'when invoice has export_error_msg' do
      let(:swoop_invoice) do
        create(
          :invoice,
          state: 'swoop_approved_fleet',
          sender: swoop_company,
          recipient: swoop_and_fleet_account,
          recipient_company: swoop_and_fleet_account.client_company
        )
      end

      it 'cleans invoice.export_error_msg' do
        service.call

        swoop_invoice.reload

        expect(swoop_invoice.export_error_msg).to be_nil
      end
    end

    it 'creates a ApiAsyncRequest row on the DB and associates with the Invoice' do
      service.call

      swoop_invoice.reload

      expect(swoop_invoice.api_async_request).not_to be_nil
      expect(swoop_invoice.api_async_request.target_id).to eq swoop_invoice.id
      expect(swoop_invoice.api_async_request.company_id).to eq swoop_invoice.sender_id
      expect(swoop_invoice.api_async_request.user_id).to eq swoop_user.id
    end

    it 'calls async worker to export the invoice to the Fleet Managed' do
      module_name = InvoiceService::SwoopSentFleet::FLEETS_ALLOWED_TO_RECEIVE_INVOICE[swoop_invoice.recipient_company.name]
      worker_class = "InvoiceService::ExportToFleet::#{module_name}::Worker".constantize

      allow(worker_class).to receive(:perform_async)
      expect(worker_class).to receive(:perform_async)

      service.call
    end
  end
end
