# frozen_string_literal: true

require 'rails_helper'

describe PopulateInvoiceForJobCandidate do
  include Requests::InvoiceHelpers

  subject do
    # Step 1: Populate the candidate's invoice
    PopulateInvoiceForJobCandidate.new(
      new_invoice,
      fleet_managed_job,
      rate,
      new_jerzy_towing_site_candidate
    ).call

    # Step 2: Run validators, which calculate total_amount
    new_invoice.valid?
  end

  let(:auto_assign_feature) { create(:feature, { name: Feature::AUTO_ASSIGN }) }
  let(:fleet_company) { create(:fleet_company, { features: [auto_assign_feature] }) }
  let(:fleet_managed_job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
      service_location: create(:location, :mission_safeway),
      service_code: create(:service_code, name: 'Tow'),
      status: Job::AUTO_ASSIGNING,
      rescue_company: nil,
      invoice: nil,
      account: nil,
    })
  end
  let(:swoop_company) { create(:super_company) }
  let(:new_jerzy_towing) { create(:rescue_company, :new_jerzy_towing) }
  let(:new_jerzy_towing_swoop_account) { create(:account, name: 'Swoop', company: new_jerzy_towing, client_company: swoop_company) }
  let(:new_jerzy_towing_site_candidate) do
    create(:site_candidate,
           job: fleet_managed_job,
           company: new_jerzy_towing,
           site: new_jerzy_towing.sites.first,
           lat: nil,
           lng: nil,
           rescue_provider: nil,
           estimate: Estimate.create!(meters_ab: 10000, meters_bc: 10000, meters_ca: 20000))
  end

  let(:new_invoice) do
    create(
      :invoice,
      job: fleet_managed_job,
      sender: new_jerzy_towing,
      recipient: new_jerzy_towing_swoop_account,
      recipient_company: new_jerzy_towing_swoop_account.client_company,
      description: 'Generated for auto assign candidate',
      version: Invoice::VERSION,
      currency: new_jerzy_towing.currency,
      state: nil
    )
  end
  let(:flat_rate) do
    create(
      :clean_flat_rate,
      {
        company: new_jerzy_towing,
        account: new_jerzy_towing_swoop_account,
        live: true,
      }
    )
  end

  let(:miles_p2p_rate) do
    create(
      :miles_p2p_rate,
      :miles_enroute,
      :miles_towed,
      :miles_deadhead,
      {
        company: new_jerzy_towing,
        live: true,
      }
    )
  end

  context 'flat rate' do
    let(:rate) { flat_rate }

    it 'calculates the total amount with tax' do
      subject
      expect(new_invoice.total_amount).to eql 75
      expect(new_invoice.line_items.length).to eql(2)
      expect(new_invoice.line_items[0].description).to eql "Flat Rate"
      expect(new_invoice.line_items[1].description).to eql "Tax"
    end
  end

  context 'miles port-to-port rate' do
    let(:rate) { miles_p2p_rate }

    it 'updates the invoice when miles_ab changes' do
      subject
      expect(new_invoice.line_items.length).to eql(8)
      expect(new_invoice.line_items[0].description).to eql "Hookup"
      expect(new_invoice.line_items[0].net_amount).to eql 100
      expect(new_invoice.line_items[1].description).to eql "En Route Per Mile"
      expect(new_invoice.line_items[1].net_amount).to eql 12.4
      expect(new_invoice.line_items[2].description).to eql "En Route Free Miles"
      expect(new_invoice.line_items[2].net_amount).to eql(-12.4)
      expect(new_invoice.line_items[3].description).to eql "Towed Per Mile"
      expect(new_invoice.line_items[3].net_amount).to eql 6.2
      expect(new_invoice.line_items[4].description).to eql "Towed Free Miles"
      expect(new_invoice.line_items[4].net_amount).to eql(-5)
      expect(new_invoice.line_items[5].description).to eql "Deadhead Per Mile"
      expect(new_invoice.line_items[5].net_amount).to eql 0
      expect(new_invoice.line_items[6].description).to eql "Deadhead Free Miles"
      expect(new_invoice.line_items[6].net_amount).to eql 0
    end
  end

  context 'percentage add-ons' do
    let(:rate) { flat_rate }

    it 'calcs percentage addition' do
      flat_rate.additions.create!(
        name: 'fuel',
        calc_type: 'percent',
        amount: 10
      )
      subject
      expect(new_invoice.total_amount).to eql(75 + 7.5) # $75 for the flat rate, $7.50 for the fuel
      expect(new_invoice.line_items.length).to eql(3)
      expect(new_invoice.line_items[0].description).to eql "Flat Rate"
      expect(new_invoice.line_items[1].description).to eql "Tax"
      expect(new_invoice.line_items[2].description).to eql "fuel"
    end

    context 'addition attached to default vehicle rate' do
      let(:flatbed) { create(:vehicle_category) }
      let!(:flat_heavy_rate) do
        create(:clean_flat_rate,
               {
                 company: new_jerzy_towing,
                 live: true,
                 vehicle_category_id: flatbed.id,
                 flat: 100,
               })
      end

      it 'calculates percentage addition' do
        flat_rate.additions.create!(
          name: 'fuel',
          calc_type: 'percent',
          amount: 10
        )
        subject
        expect(new_invoice.line_items.length).to eql(3)
        expect(new_invoice.line_items[0].description).to eql "Flat Rate"
        expect(new_invoice.line_items[0].net_amount).to eql 75
        expect(new_invoice.line_items[1].description).to eql "Tax"
        expect(new_invoice.line_items[1].net_amount).to eql 0
        expect(new_invoice.line_items[2].description).to eql "fuel"
        expect(new_invoice.line_items[2].net_amount).to eql 7.5
      end
    end
  end
end
