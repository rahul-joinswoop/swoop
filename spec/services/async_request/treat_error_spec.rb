# frozen_string_literal: true

require "rails_helper"

describe AsyncRequest::TreatError do
  before do
    stub_class 'TreatErrorMock' do
      include AsyncRequest::TreatError

      attr_reader :async_request_id, :company_to_receive_msg

      def initialize(company_to_receive_msg, async_request_id)
        @async_request_id = async_request_id
        @company_to_receive_msg = company_to_receive_msg
      end

      def go_treat_this_error
        treat_error(
          ArgumentError.new('test'),
          :test_root_node,
          ['Error Message'],
          @company_to_receive_msg
        )
      end
    end
    allow(Publishers::GenericWebsocketMessage).to receive(:call)
  end

  subject(:treat_error_mock) do
    TreatErrorMock.new(
      super_company,
      '123'
    ).go_treat_this_error
  end

  let(:super_company) { create(:super_company) }

  it 'calls Publishers::GenericWebsocketMessage with expected params' do
    allow(Publishers::GenericWebsocketMessage).to receive(:call).with(
      input: {
        class_name: 'AsyncRequests',
        target_data: {
          id: 123,
          error: {
            test_root_node: ['Error Message'],
          },
        },
        company: super_company,
      }
    )

    treat_error_mock
  end
end
