# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckInvoiceSender do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    context "when the sender is a rescue company" do
      let(:invoice) { create :invoice, sender: create(:rescue_company) }

      it "works" do
        expect(context).to be_a_success
      end
    end

    context "when the sender is not a rescue company" do
      let(:invoice) { create :invoice, sender: create(:fleet_company) }

      it "fails" do
        expect(context).to be_a_failure
        expect(context.message).to eq(described_class::FAILURE_MESSAGE)
      end
    end
  end
end
