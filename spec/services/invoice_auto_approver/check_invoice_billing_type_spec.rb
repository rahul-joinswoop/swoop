# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckInvoiceBillingType do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    context "when billing_type is in VALID_BILLING_TYPES" do
      described_class::VALID_BILLING_TYPES.each do |billing_type|
        let(:invoice) { create :invoice, billing_type: billing_type }

        it "works" do
          expect(context).to be_a_success
        end
      end
    end

    context "when billing_type is not in VALID_BILLING_TYPES" do
      (BillingType::ALL_BILLING_TYPES - described_class::VALID_BILLING_TYPES).each do |billing_type|
        let(:invoice) { create :invoice, billing_type: billing_type }

        it "fails" do
          expect(context).to be_a_failure
          expect(context.message).to eq(described_class::FAILURE_MESSAGE)
        end
      end
    end
  end
end
