# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckInvoiceAmount do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    context "when amount is $0.00" do
      let(:invoice) { create :invoice, sender: create(:rescue_company), total_amount: 0.0 }

      it "fails" do
        expect(context).to be_a_failure
        expect(context.message).to eq(described_class::ZERO_FAILURE_MESSAGE)
      end
    end

    context "with line items" do
      let!(:line_items) do
        li = create :invoicing_line_item,
                    net_amount: amount,
                    unit_price: amount,
                    job: invoice.job
        invoice.line_items << li
        invoice.calculate_total_amount!
        li
      end
      let(:invoice) do
        create :invoice,
               job: create(:job, status: status),
               sender: create(:rescue_company)
      end

      [:goa, :completed, :released].each do |s|
        context "with a #{s} job" do
          let(:status) { s }

          context "with a job > auto_approval_limit" do
            let(:amount) { invoice.auto_approval_limit + 1 }

            it "fails" do
              expect(context).to be_a_failure
              expect(context.message)
                .to eq(described_class::TOO_HIGH_FAILURE_MESSAGE % [invoice.auto_approval_limit])
            end
          end

          context "with a job <= auto_approval_limit" do
            let(:amount) { invoice.auto_approval_limit - 1 }

            it "works" do
              expect(context).to be_a_success
            end
          end
        end
      end
    end
  end
end
