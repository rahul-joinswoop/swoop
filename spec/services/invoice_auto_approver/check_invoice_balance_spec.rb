# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckInvoiceBalance do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    let(:invoice) { create :invoice, sender: create(:rescue_company) }

    context "when balance is $0.00" do
      it "fails" do
        expect(context).to be_a_failure
        expect(context.message).to eq(described_class::FAILURE_MESSAGE)
      end
    end

    context "when balance is > $0.00" do
      let!(:line_items) do
        li = create :invoicing_line_item,
                    net_amount: 25.0,
                    unit_price: 25.0,
                    job: invoice.job
        invoice.line_items << li
        invoice.calculate_total_amount!
        li
      end

      it "works" do
        expect(context).to be_a_success
      end
    end
  end
end
