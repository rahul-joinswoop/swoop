# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckJobStatus do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    context "when the job is in VALID_STATES" do
      described_class::VALID_STATES.each do |status|
        let(:invoice) { create :invoice, job: create(:job, status: status) }

        it "works" do
          expect(context).to be_a_success
        end
      end
    end

    context "when the job is not in VALID_STATES" do
      let(:invoice) { create :invoice, job: create(:job, status: :pending) }

      it "fails" do
        expect(context).to be_a_failure
        expect(context.message).to eq(described_class::FAILURE_MESSAGE)
      end
    end
  end
end
