# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckForHistoryItems do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    context "when the invoice has no history items" do
      let(:invoice) { create :invoice }

      it "works" do
        expect(context).to be_a_success
      end
    end

    context "when the invoice has history items" do
      let(:invoice) { create :invoice, :with_history_items }

      it "works" do
        expect(context).to be_a_failure
        expect(context.message).to eq(described_class::FAILURE_MESSAGE)
      end
    end

    context "when the invoice has payments with history items" do
      let(:invoice) { create :invoice, :with_payments_with_history_items }

      it "works" do
        expect(context).to be_a_failure
        expect(context.message).to eq(described_class::FAILURE_MESSAGE)
      end
    end
  end
end
