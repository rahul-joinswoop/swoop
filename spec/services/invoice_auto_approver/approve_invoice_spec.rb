# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::ApproveInvoice do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    let(:invoice) { create :invoice, state: state }

    before(:each) do
      expect(InvoiceService::SwoopApprovedPartner)
        .to receive(:new)
        .once
        .with(invoice_id: invoice.id, api_company_id: Company.swoop_id)
        .and_call_original

      expect_any_instance_of(InvoiceService::SwoopApprovedPartner)
        .to receive(:call)
        .once
        .and_call_original
    end

    context "when the invoice state is partner_sent" do
      let(:state) { :partner_sent }

      it "works" do
        # in the non-error case we'll also make this call
        expect_any_instance_of(InvoiceService::SwoopApprovedPartner)
          .to receive(:save_mutated_objects)
          .once
          .and_call_original
        expect(context).to be_a_success
      end
    end

    context "when the invoice state is something else" do
      let(:state) { :fleet_approved }

      it "fails" do
        expect(context).to be_a_failure
        expect(context.message)
          .to eq("InvoiceService::InvoiceStateTransitionError")
      end
    end
  end
end
