# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckInvoiceRecipient do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    context "when swoop is the recipient" do
      let(:invoice) { create :invoice, recipient_company: create(:swoop_company) }

      it "works" do
        expect(context).to be_a_success
      end
    end

    context "when swoop is not the recipient" do
      let(:invoice) { create :invoice }

      it "fails" do
        expect(context).to be_a_failure
        expect(context.message).to eq(described_class::FAILURE_MESSAGE)
      end
    end
  end
end
