# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckInvoiceSentDate do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    let(:invoice) { create :invoice, job: create(:job, :with_ajs), sent_at: sent_at, sender: create(:rescue_company) }

    context "when sent_at < job.last_status_date_time + 90.days" do
      let(:sent_at) { DateTime.current }

      it "works" do
        expect(context).to be_a_success
      end
    end

    context "when sent_at >= job.last_status_date_time + 90.days" do
      let(:sent_at) { DateTime.current + 90.days }

      it "works" do
        expect(context).to be_a_failure
        expect(context.message).to eq(described_class::FAILURE_MESSAGE)
      end
    end
  end
end
