# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckForInvoice do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    context "with an invoice" do
      let(:invoice) { create :invoice }

      it "works" do
        expect(context).to be_a_success
        expect(context.message).to be_blank
      end
    end

    context "without an invoice" do
      let(:invoice) { nil }

      it "fails" do
        expect(context).to be_a_failure
        expect(context.message).to be(described_class::FAILURE_MESSAGE)
      end
    end

    context "without invoice context key" do
      subject(:context) { described_class.call }

      it "fails" do
        expect(context).to be_a_failure
        expect(context.message).to be(described_class::FAILURE_MESSAGE)
      end
    end
  end
end
