# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover::CheckInvoiceState do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    let(:invoice) { create :invoice, state: state }

    context "with an invoice in #{described_class::VALID_STATES}" do
      described_class::VALID_STATES.each do |s|
        let(:state) { s }

        it "works" do
          expect(context).to be_a_success
        end
      end
    end

    context "with an invoice in another state" do
      let(:state) { (Invoice.states.keys - described_class::VALID_STATES).sample }

      it "fails" do
        expect(context).to be_a_failure
        expect(context.message).to be(described_class::FAILURE_MESSAGE)
      end
    end
  end
end
