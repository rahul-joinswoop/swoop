# frozen_string_literal: true

require "rails_helper"
require 'geocoder'
require 'geocoder/results/google_premier'

# PCC Service instantiates internal classes based on the FleetCompany that's using the system.
# The pilot is "Farmers Insurance", so we're using it for the specs.
describe Geocode::Extractor do
  describe "#extract from lat/lng resut" do
    subject(:extractor) { described_class.new([result]) }

    let(:result) do
      Geocoder::Result::GooglePremier.new({
        "address_components" =>
                                                                   [
                                                                     { "long_name" => "2999", "short_name" => "2999", "types" => ["street_number"] },
                                                                     { "long_name" => "F Street", "short_name" => "F St", "types" => ["route"] },
                                                                     { "long_name" => "Central Sacramento", "short_name" => "Central Sacramento", "types" => ["neighborhood", "political"] },
                                                                     { "long_name" => "Sacramento", "short_name" => "Sacramento", "types" => ["locality", "political"] },
                                                                     { "long_name" => "Sacramento County", "short_name" => "Sacramento County", "types" => ["administrative_area_level_2", "political"] },
                                                                     { "long_name" => "California", "short_name" => "CA", "types" => ["administrative_area_level_1", "political"] },
                                                                     { "long_name" => "United States", "short_name" => "US", "types" => ["country", "political"] },
                                                                     { "long_name" => "95816", "short_name" => "95816", "types" => ["postal_code"] },
                                                                   ],
        "formatted_address" => "2999 F St, Sacramento, CA 95816, USA",
        "geometry" =>
                                                           {
                                                             "bounds" => { "northeast" => { "lat" => 38.5776615, "lng" => -121.4646505 }, "southwest" => { "lat" => 38.5775069, "lng" => -121.4652233 } },
                                                             "location" => { "lat" => 38.57752079999999, "lng" => -121.4646444 },
                                                             "location_type" => "RANGE_INTERPOLATED",
                                                             "viewport" => { "northeast" => { "lat" => 38.5789331802915, "lng" => -121.4635879197085 }, "southwest" => { "lat" => 38.5762352197085, "lng" => -121.4662858802915 } },
                                                           },
        "place_id" => "EiQyOTk5IEYgU3QsIFNhY3JhbWVudG8sIENBIDk1ODE2LCBVU0E",
        "types" => ["street_address"],
      })
    end

    it "extracts zip" do
      res = extractor.call
      expect(res[:street]).to eq('2999 F St')
      expect(res[:street_number]).to eq('2999')
      expect(res[:street_name]).to eq('F St')
      expect(res[:city]).to eq('Sacramento')
      expect(res[:state]).to eq('CA')
      expect(res[:zip]).to eq('95816')
      expect(res[:country]).to eq('US')
    end
  end
end
