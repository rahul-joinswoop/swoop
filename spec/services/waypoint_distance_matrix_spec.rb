# frozen_string_literal: true

require "rails_helper"

describe External::WaypointDistanceMatrixService do
  let(:tow_truck_driver_1) { create(:location, :mission_safeway) }
  let(:tow_truck_driver_2) { create(:location, :tiburon_presbyterian) }
  let(:tow_truck_driver_3) { create(:location, :oakland_beer_garden) }
  let(:tow_truck_driver_4) { create(:location, :round_table_pizza) }
  let(:accident_site) { create(:location, :stonestown_mall) }
  let(:body_shop) { create(:location, :accurate_auto_body) }

  describe 'from A\'s to B', vcr: true do
    describe 'Array results' do
      let(:waypoint_results) { External::WaypointDistanceMatrixService.as_to_b([tow_truck_driver_1, tow_truck_driver_2, tow_truck_driver_3], accident_site, Hash) }

      it 'returns waypoint results as a hash of arrays' do
        expect(waypoint_results).to be_instance_of(Hash)
        expect(waypoint_results).to have_key(:as_to_b)
        expect(waypoint_results).not_to have_key(:b_to_as)
        expect(waypoint_results).not_to have_key(:b_to_c)
        expect(waypoint_results).not_to have_key(:c_to_as)
        expect(waypoint_results[:as_to_b]).to be_instance_of(Array)
        waypoint_results[:as_to_b].each { |a_to_b_waypoint_result| expect(a_to_b_waypoint_result).to be_instance_of(Array) }
      end
      it 'contains the same number of results as A locations provided as inputs' do
        expect(waypoint_results[:as_to_b].size).to eql(3)
      end
      it 'contain pre-VCR-recorded km distances and second times' do
        expect(waypoint_results[:as_to_b]).to include [9937, 991]
      end
    end

    describe 'External::RoundTripPossibility results' do
      let(:waypoint_results) { External::WaypointDistanceMatrixService.as_to_b([tow_truck_driver_1, tow_truck_driver_2, tow_truck_driver_3], accident_site, External::AToBRoundTripPossibility) }

      it 'returns waypoint results as an array of External::RoundTripPossibility\'s' do
        expect(waypoint_results).to be_instance_of(Array)
        waypoint_results.each { |waypoint_result| expect(waypoint_result).to be_kind_of(External::RoundTripPossibility) }
        waypoint_results.each { |waypoint_result| expect(waypoint_result).to be_instance_of(External::AToBRoundTripPossibility) }
      end
      it 'contains the same number of results as A locations provided as inputs' do
        expect(waypoint_results.size).to eql(3)
      end
      it 'contains the locations' do
        expect(waypoint_results.map(&:a)).to contain_exactly(tow_truck_driver_1, tow_truck_driver_2, tow_truck_driver_3)
        expect(waypoint_results.map(&:b).uniq!).to contain_exactly(accident_site)
        expect { waypoint_results.map(&:c).uniq! }.to raise_error(NoMethodError)
      end
      it 'contains pre-VCR-recorded km distances and second times' do
        expect(waypoint_results.first.a_to_b_in_meters).to eql(9937)
        expect(waypoint_results.first.a_to_b_in_seconds).to eql(979)
      end
      it 'calculates the total distance' do
        expect(waypoint_results.first.a_to_b_in_meters).to eql(9937)
        expect(waypoint_results.first.total_distance_in_meters?).to eql(9937)
      end
      it 'calculates the total time' do
        expect(waypoint_results.first.a_to_b_in_seconds).to eql(994)
        expect(waypoint_results.first.total_time_in_seconds?).to eql(994)
      end
    end
  end

  describe 'from A\'s to B to A\'s', vcr: true do
    describe 'Array results' do
      let(:waypoint_results) { External::WaypointDistanceMatrixService.as_to_b_to_as([tow_truck_driver_1, tow_truck_driver_2, tow_truck_driver_3, tow_truck_driver_4], accident_site, Hash) }

      it 'returns waypoint results as a hash of arrays' do
        expect(waypoint_results).to be_instance_of(Hash)
        expect(waypoint_results).to have_key(:as_to_b)
        expect(waypoint_results).to have_key(:b_to_as)
        expect(waypoint_results).not_to have_key(:b_to_c)
        expect(waypoint_results).not_to have_key(:c_to_as)
        expect(waypoint_results[:as_to_b]).to be_instance_of(Array)
        expect(waypoint_results[:b_to_as]).to be_instance_of(Array)
        waypoint_results[:as_to_b].each { |a_to_b_waypoint_result| expect(a_to_b_waypoint_result).to be_instance_of(Array) }
        waypoint_results[:b_to_as].each { |b_to_a_waypoint_result| expect(b_to_a_waypoint_result).to be_instance_of(Array) }
      end
      it 'contains the same number of results as A locations provided as inputs' do
        expect(waypoint_results[:as_to_b].size).to eql(4)
        expect(waypoint_results[:b_to_as].size).to eql(4)
      end
      it 'contain pre-VCR-recorded km distances and second times' do
        expect(waypoint_results[:as_to_b]).to include [9937, 995]
        expect(waypoint_results[:b_to_as]).to include [9134, 841]
      end
    end

    describe 'External::RoundTripPossibility results' do
      let(:waypoint_results) { External::WaypointDistanceMatrixService.as_to_b_to_as([tow_truck_driver_1, tow_truck_driver_2, tow_truck_driver_3, tow_truck_driver_4], accident_site, External::AToBToARoundTripPossibility) }

      it 'returns waypoint results as an array of RoundTripPossibility\'s' do
        expect(waypoint_results).to be_instance_of(Array)
        waypoint_results.each { |waypoint_result| expect(waypoint_result).to be_kind_of(External::RoundTripPossibility) }
        waypoint_results.each { |waypoint_result| expect(waypoint_result).to be_instance_of(External::AToBToARoundTripPossibility) }
      end
      it 'contains the same number of results as A locations provided as inputs' do
        expect(waypoint_results.size).to eql(4)
      end
      it 'contains the locations' do
        expect(waypoint_results.map(&:a)).to contain_exactly(tow_truck_driver_1, tow_truck_driver_2, tow_truck_driver_3, tow_truck_driver_4)
        expect(waypoint_results.map(&:b).uniq!).to contain_exactly(accident_site)
        expect { waypoint_results.map(&:c).uniq! }.to raise_error(NoMethodError)
      end
      it 'contains pre-VCR-recorded km distances and second times' do
        expect(waypoint_results.first.a_to_b_in_meters).to eql(9937)
        expect(waypoint_results.first.b_to_a_in_meters).to eql(9134)
        expect(waypoint_results.first.a_to_b_in_seconds).to eql(994)
        expect(waypoint_results.first.b_to_a_in_seconds).to eql(843)
      end
      it 'calculates the total distance' do
        expect(waypoint_results.first.a_to_b_in_meters).to eql(9937)
        expect(waypoint_results.first.b_to_a_in_meters).to eql(9134)
        expect(waypoint_results.first.total_distance_in_meters?).to eql(9937 + 9134)
      end
      it 'calculates the total time' do
        expect(waypoint_results.first.a_to_b_in_seconds).to eql(976)
        expect(waypoint_results.first.b_to_a_in_seconds).to eql(842)
        expect(waypoint_results.first.total_time_in_seconds?).to eql(976 + 842)
      end
    end
  end

  describe 'from A\'s to B to C to A\'s', vcr: true do
    describe 'Array results' do
      let(:waypoint_results) { External::WaypointDistanceMatrixService.as_to_b_to_c_to_as([tow_truck_driver_1, tow_truck_driver_2], accident_site, body_shop, Hash) }

      it 'returns waypoint results as a hash of arrays' do
        expect(waypoint_results).to be_instance_of(Hash)
        expect(waypoint_results).to have_key(:as_to_b)
        expect(waypoint_results).to have_key(:b_to_c)
        expect(waypoint_results).to have_key(:c_to_as)
        expect(waypoint_results).not_to have_key(:b_to_as)
        expect(waypoint_results[:as_to_b]).to be_instance_of(Array)
        expect(waypoint_results[:b_to_c]).to be_instance_of(Array)
        expect(waypoint_results[:c_to_as]).to be_instance_of(Array)
        waypoint_results[:as_to_b].each { |a_to_b_waypoint_result| expect(a_to_b_waypoint_result).to be_instance_of(Array) }
        waypoint_results[:b_to_c].each { |b_to_c_waypoint_result| expect(b_to_c_waypoint_result).to be_instance_of(Array) }
        waypoint_results[:c_to_as].each { |c_to_a_waypoint_result| expect(c_to_a_waypoint_result).to be_instance_of(Array) }
      end
      it 'contains the same number of results as A locations provided as inputs' do
        expect(waypoint_results[:as_to_b].size).to eql(2)
        expect(waypoint_results[:b_to_c].size).to eql(1)
        expect(waypoint_results[:c_to_as].size).to eql(2)
      end
      it 'contain pre-VCR-recorded km distances and second times' do
        expect(waypoint_results[:as_to_b]).to include [9937, 995]
        expect(waypoint_results[:b_to_c]).to contain_exactly [15670, 1606]
        expect(waypoint_results[:c_to_as]).to include [6531, 1067]
      end
    end

    describe 'External::RoundTripPossibility results' do
      let(:waypoint_results) { External::WaypointDistanceMatrixService.as_to_b_to_c_to_as([tow_truck_driver_1, tow_truck_driver_2], accident_site, body_shop, External::AToBToCToARoundTripPossibility) }

      it 'returns waypoint results as an array of External::RoundTripPossibility\'s' do
        expect(waypoint_results).to be_instance_of(Array)
        waypoint_results.each { |waypoint_result| expect(waypoint_result).to be_kind_of(External::RoundTripPossibility) }
        waypoint_results.each { |waypoint_result| expect(waypoint_result).to be_instance_of(External::AToBToCToARoundTripPossibility) }
      end
      it 'contains the same number of results as A locations provided as inputs' do
        expect(waypoint_results.size).to eql(2)
      end
      it 'contains the locations' do
        expect(waypoint_results.map(&:a)).to contain_exactly(tow_truck_driver_1, tow_truck_driver_2)
        expect(waypoint_results.map(&:b).uniq!).to contain_exactly(accident_site)
        expect(waypoint_results.map(&:c).uniq!).to contain_exactly(body_shop)
      end
      it 'contains pre-VCR-recorded km distances and second times' do
        expect(waypoint_results.first.a_to_b_in_meters).to eql(9937)
        expect(waypoint_results.first.b_to_c_in_meters).to eql(15670)
        expect(waypoint_results.first.c_to_a_in_meters).to eql(6531)
        expect(waypoint_results.first.a_to_b_in_seconds).to eql(976)
        expect(waypoint_results.first.b_to_c_in_seconds).to eql(1566)
        expect(waypoint_results.first.c_to_a_in_seconds).to eql(1066)
      end
      it 'calculates the total distance' do
        expect(waypoint_results.first.a_to_b_in_meters).to eql(9937)
        expect(waypoint_results.first.b_to_c_in_meters).to eql(15670)
        expect(waypoint_results.first.c_to_a_in_meters).to eql(6531)
        expect(waypoint_results.first.total_distance_in_meters?).to eql(9937 + 15670 + 6531)
      end
      it 'calculates the total time' do
        expect(waypoint_results.first.a_to_b_in_seconds).to eql(976)
        expect(waypoint_results.first.b_to_c_in_seconds).to eql(1590)
        expect(waypoint_results.first.c_to_a_in_seconds).to eql(1058)
        expect(waypoint_results.first.total_time_in_seconds?).to eql(976 + 1590 + 1058)
      end
    end
  end

  describe 'find the best tow truck driver', vcr: true do
    let(:waypoint_results) { External::WaypointDistanceMatrixService.as_to_b_to_c_to_as([tow_truck_driver_1, tow_truck_driver_2, tow_truck_driver_3, tow_truck_driver_4], accident_site, body_shop, External::AToBToCToARoundTripPossibility) }

    it 'by shortest distance' do
      shortest_round_trip_distance = waypoint_results.sort { |a, b| a.total_distance_in_meters? <=> b.total_distance_in_meters? }.first
      expect(shortest_round_trip_distance.a).to be(tow_truck_driver_1)
    end
    it 'by fastest time' do
      fastest_round_trip_time = waypoint_results.sort { |a, b| a.total_time_in_seconds? <=> b.total_time_in_seconds? }.first
      expect(fastest_round_trip_time.a).to be(tow_truck_driver_1)
    end
  end
end
