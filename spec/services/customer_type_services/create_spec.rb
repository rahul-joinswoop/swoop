# frozen_string_literal: true

require "rails_helper"

describe CustomerTypeServices::Create do
  subject(:service_call) do
    described_class.new(customer_type_name: customer_type_name).call
  end

  let(:customer_type_name) { 'Customer Type Name' }

  it 'creates a new Customer Type' do
    service_call

    expect(CustomerType.exists?(name: customer_type_name)).to eq(true)
  end

  context 'when customer type name is duplicated' do
    before do
      CustomerType.where(name: customer_type_name.downcase).first_or_create
    end

    it 'does not create a new Customer Type' do
      allow(CustomerType).to receive(:create)

      service_call

      expect(CustomerType).not_to receive(:create)
    end
  end
end
