# frozen_string_literal: true

require "rails_helper"

describe CustomerTypeServices::Batch do
  subject(:service_call) do
    described_class.new(
      api_company: api_company,
      customer_type_ids: customer_type_ids
    ).call
  end

  context 'when api_company is a RescueCompany' do
    let(:api_company)   { create :rescue_company, :with_customer_types }
    let(:fleet_company) { create :fleet_company, :with_customer_types }

    let(:customer_type_ids) do
      api_company.customer_types.pluck(:id) + fleet_company.customer_types.pluck(:id)
    end

    let(:account) do
      create :account, company: api_company, client_company: fleet_company
    end

    it 'returns own customer types' do
      partner_customer_type = CustomerType.find(
        api_company.customer_types.first.id
      )

      expect(service_call).to include(partner_customer_type)
    end

    it 'returns custom customer types from accounts' do
      customer_type_from_account = CustomerType.find(
        account.client_company.customer_types.first.id
      )

      expect(service_call).to include(customer_type_from_account)
    end
  end

  context 'when api_company is a FleetCompany' do
    let(:api_company) { create :fleet_company, :with_customer_types }
    let(:customer_types_from_api_company) { api_company.customer_types }
    let(:any_other_customer_type) { create :customer_type, name: 'Any other Customer Type' }

    let(:any_customer_type_ids) { [any_other_customer_type.id] }

    let(:customer_type_ids) do
      api_company.customer_types.pluck(:id) + any_customer_type_ids
    end

    it 'returns all customer_types requested' do
      expect(service_call).to include(*customer_types_from_api_company, any_other_customer_type)
    end
  end

  context 'when api_company is Swoop' do
    let(:api_company) { SuperCompany.first_or_create(name: 'Swoop') }

    let(:customer_type_1) { create :customer_type, name: 'Customer Type 1' }
    let(:customer_type_2) { create :customer_type, name: 'Customer Type 2' }

    let(:customer_type_ids) do
      [customer_type_1.id, customer_type_2.id]
    end

    it 'returns all customer_types requested' do
      expect(service_call).to include(customer_type_1, customer_type_2)
    end
  end
end
