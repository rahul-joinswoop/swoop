# frozen_string_literal: true

require "rails_helper"

describe VehicleCategoryServices::Change do
  subject(:service_call) do
    described_class.new(
      api_company: api_company,
      standard_vehicle_category_ids_to_add: standard_vehicle_category_ids_to_add,
      standard_vehicle_category_ids_to_remove: standard_vehicle_category_ids_to_remove,
      custom_vehicle_categories_names_to_add: custom_vehicle_categories_names_to_add
    ).call
  end

  before :each do
    allow(PublishCompanyWorker).to receive(:perform_async)
  end

  context 'when api_company is a RescueCompany' do
    let(:api_company) { create :rescue_company, :with_vehicle_categories }

    let(:another_standard_vehicle_category) do
      create :vehicle_category, name: VehicleCategory::HEAVY_DUTY, is_standard: true
    end

    let(:standard_vehicle_category_ids_to_add) do
      [another_standard_vehicle_category.id]
    end

    let(:current_standard_vehicle_category) do
      api_company.vehicle_categories.find_by(name: VehicleCategory::FLATBED)
    end

    let(:standard_vehicle_category_ids_to_remove) do
      [current_standard_vehicle_category.id]
    end

    let(:custom_vehicle_category_name) { 'A new custom vehicle category for Rescue' }

    let(:custom_vehicle_categories_names_to_add) do
      [{ name: custom_vehicle_category_name }]
    end

    it 'adds a standard vehicle category' do
      service_call

      expect(api_company.vehicle_categories).to include(another_standard_vehicle_category)
    end

    it 'removes a standard vehicle category' do
      service_call

      expect(api_company.vehicle_categories).not_to include(current_standard_vehicle_category)
    end

    it 'adds a custom vehicle category' do
      service_call

      custom_vehicle_category = VehicleCategory.find_by(name: custom_vehicle_category_name)

      expect(api_company.vehicle_categories).to include(custom_vehicle_category)
    end
  end

  context 'when api_company is a FleetCompany' do
    let(:api_company) { create :fleet_company, :with_vehicle_categories }

    let(:another_standard_vehicle_category) do
      create :vehicle_category, name: VehicleCategory::HEAVY_DUTY, is_standard: true
    end

    let(:standard_vehicle_category_ids_to_add) do
      [another_standard_vehicle_category.id]
    end

    let(:current_standard_vehicle_category) do
      api_company.vehicle_categories.find_by(name: VehicleCategory::FLATBED)
    end

    let(:standard_vehicle_category_ids_to_remove) do
      [current_standard_vehicle_category.id]
    end

    let(:custom_vehicle_category_name) { 'A new custom vehicle category for Fleet' }

    let(:custom_vehicle_categories_names_to_add) do
      [{ name: custom_vehicle_category_name }]
    end

    it 'adds a standard vehicle category' do
      service_call

      expect(api_company.vehicle_categories).to include(another_standard_vehicle_category)
    end

    it 'removes a standard vehicle category' do
      service_call

      expect(api_company.vehicle_categories).not_to include(current_standard_vehicle_category)
    end

    it 'does not add a custom vehicle category' do
      service_call

      custom_vehicle_category = VehicleCategory.find_by(name: custom_vehicle_category_name)

      expect(api_company.vehicle_categories).not_to include(custom_vehicle_category)
    end
  end
end
