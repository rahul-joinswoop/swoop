# frozen_string_literal: true

require "rails_helper"

describe VehicleCategoryServices::Batch do
  subject(:service_call) do
    described_class.new(
      api_company: api_company,
      vehicle_category_ids: vehicle_category_ids
    ).call
  end

  context 'when api_company is a RescueCompany' do
    let(:api_company)   { create :rescue_company, :with_vehicle_categories }
    let(:fleet_company) { create :fleet_company, :with_vehicle_categories }

    let(:vehicle_category_ids) do
      (api_company.vehicle_categories.pluck(:id) + fleet_company.vehicle_categories.pluck(:id)).flatten
    end

    let(:account) do
      create :account, company: api_company, client_company: fleet_company
    end

    it 'returns own vehicle categories' do
      partner_vehicle_category = VehicleCategory.find(
        api_company.vehicle_categories.first.id
      )

      expect(service_call).to include(partner_vehicle_category)
    end

    it 'returns custom vehicle categories from accounts' do
      vehicle_category_from_account = VehicleCategory.find(
        account.client_company.vehicle_categories.first.id
      )

      expect(service_call).to include(vehicle_category_from_account)
    end
  end

  context 'when api_company is a FleetCompany' do
    let(:api_company) { create :fleet_company, :with_vehicle_categories }

    let(:vehicle_category_ids) do
      api_company.vehicle_categories.pluck(:id)
    end

    it 'returns own vehicle categories' do
      vehicle_category = VehicleCategory.find(
        api_company.vehicle_categories.first.id
      )

      expect(service_call).to include(vehicle_category)
    end

    context 'when it requires a custom vehicle category' do
      let(:custom_vehicle_category) { create(:vehicle_category, name: 'Very Custom One', is_standard: false) }
      let(:vehicle_category_ids) do
        (api_company.vehicle_categories.pluck(:id) + [custom_vehicle_category.id]).flatten
      end

      it 'does not return custom vehicle categories' do
        expect(service_call).not_to include(custom_vehicle_category)
      end
    end
  end

  context 'when api_company is Swoop' do
    let(:api_company) { create :super_company, :swoop }

    let(:vehicle_category_1) { create :vehicle_category, name: 'Vechicle Category 1' }
    let(:vehicle_category_2) { create :vehicle_category, name: 'Vechicle Category 2' }

    let(:vehicle_category_ids) do
      [vehicle_category_1.id, vehicle_category_2.id]
    end

    it 'returns all vehicle_categories requested' do
      expect(service_call).to include(vehicle_category_1, vehicle_category_2)
    end
  end
end
