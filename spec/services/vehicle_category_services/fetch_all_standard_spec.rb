# frozen_string_literal: true

require "rails_helper"

describe VehicleCategoryServices::FetchAllStandard do
  subject(:service_call) do
    described_class.new(api_company: api_company).call
  end

  context 'when api_company is a RescueCompany' do
    let(:api_company) { create :rescue_company, :with_vehicle_categories }

    it 'returns only standard services' do
      expect(service_call).to eq(VehicleCategory.standard_items)
    end
  end

  context 'when api_company is a FleetCompany' do
    let(:api_company) { create :fleet_company, :with_vehicle_categories }

    it 'returns only standard services' do
      expect(service_call).to eq(VehicleCategory.standard_items)
    end
  end

  context 'when api_company is Swoop' do
    let(:api_company) { create :super_company, :swoop }

    it 'raises ArgumentError' do
      expect { service_call }.to raise_error(ArgumentError, 'Company type not allowed: SuperCompany')
    end
  end
end
