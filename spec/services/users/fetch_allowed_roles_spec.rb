# frozen_string_literal: true

require "rails_helper"

describe Users::FetchAllowedRoles do
  describe '.call' do
    let(:api_roles) { nil }
    let(:api_user) { nil }

    shared_examples 'it includes root' do
      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(subject).to include('root')
      end
    end

    shared_examples 'it does not include root' do
      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(subject).not_to include('root')
      end
    end

    shared_examples 'it returns all fleet roles' do
      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(subject).to eq(SwoopAuthenticator::ALL_FLEET_ROLES)
      end
    end

    shared_examples 'it returns all rescue roles' do
      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(subject).to eq(SwoopAuthenticator::ALL_RESCUE_ROLES)
      end
    end

    shared_examples 'it handles new_roles' do
      subject { described_class.call(api_roles: api_roles, api_user: api_user, new_roles: new_roles) }

      let(:allowed_roles) { api_user.present? ? api_user.human_roles : api_roles }

      context 'with valid new_roles' do
        let(:new_roles) { allowed_roles }

        it 'works' do
          expect(Rollbar).not_to receive(:error)
          expect(subject).to eq(Users::FetchAllowedRoles::ALLOWED_ROLES)
        end
      end

      context 'with invalid new_roles' do
        let(:new_roles) { Users::FetchAllowedRoles::ALLOWED_ROLES - allowed_roles }

        it 'works' do
          expect(Rollbar)
            .to receive(:error)
            .once
            .with(Users::FetchAllowedRoles::NOT_ALLOWED_ROLES_ERROR,
                  api_user: api_user,
                  api_roles: api_roles.to_a,
                  not_allowed_roles: new_roles.to_a)
          expect(subject).to eq(Users::FetchAllowedRoles::ALLOWED_ROLES)
        end
      end
    end

    context 'with a user' do
      subject { described_class.call(api_user: api_user) }

      let(:swoop_root) { create(:swoopuser) }
      let(:paul) { create(:swoopuser, email: "paul@joinswoop.com") }
      let(:nonroot) { create(:user) }
      let(:fleet_non_admin) { create :user, :fleet }
      let(:rescue_non_admin) { create :user, :rescue }
      let(:fleet_admin) { create :user, :fleet, :admin }
      let(:rescue_admin) { create :user, :rescue, :admin }

      shared_examples "fleet and rescue" do
        context 'when user is fleet admin' do
          let(:api_user) { fleet_admin }

          it_behaves_like 'it returns all fleet roles'
        end

        context 'when user is fleet non-admin' do
          let(:api_user) { fleet_non_admin }

          it_behaves_like "it handles new_roles"
        end

        context 'when user is rescue admin' do
          let(:api_user) { rescue_admin }

          it_behaves_like 'it returns all rescue roles'
        end

        context 'when user is rescue non-admin' do
          let(:api_user) { rescue_non_admin }

          it_behaves_like "it handles new_roles"
        end
      end

      context 'when in production' do
        around(:each) do |example|
          ClimateControl.modify SWOOP_ENV: "production" do
            example.run
          end
        end

        context 'when user is nonroot' do
          let(:api_user) { nonroot }

          it_behaves_like 'it does not include root'
        end

        context 'when user is swoop_root' do
          let(:api_user) { swoop_root }

          it_behaves_like 'it does not include root'
        end

        context 'when user is paul' do
          let(:api_user) { paul }

          it_behaves_like 'it includes root'
        end

        it_behaves_like "fleet and rescue"
      end

      context 'when not in production' do
        context 'when user is nonroot' do
          let(:api_user) { nonroot }

          it_behaves_like 'it does not include root'
        end

        context 'when user is swoop_root' do
          let(:api_user) { swoop_root }

          it_behaves_like 'it includes root'
        end

        context 'when user is paul' do
          let(:api_user) { paul }

          it_behaves_like 'it includes root'
        end

        it_behaves_like "fleet and rescue"
      end
    end

    context 'with roles' do
      subject { described_class.call(api_roles: api_roles) }

      let(:swoop_root) { ['root'] }
      let(:nonroot) { [] }
      let(:fleet_non_admin) { ['fleet'] }
      let(:rescue_non_admin) { ['rescue'] }
      let(:fleet_admin) { ['fleet', 'admin'] }
      let(:rescue_admin) { ['rescue', 'admin'] }

      shared_examples "fleet and rescue" do
        context 'when roles are fleet admin' do
          let(:api_roles) { fleet_admin }

          it_behaves_like 'it returns all fleet roles'
        end

        context 'when roles are fleet non-admin' do
          let(:api_roles) { fleet_non_admin }

          it_behaves_like 'it handles new_roles'
        end

        context 'when roles are rescue admin' do
          let(:api_roles) { rescue_admin }

          it_behaves_like 'it returns all rescue roles'
        end

        context 'when roles are rescue non-admin' do
          let(:api_roles) { rescue_non_admin }

          it_behaves_like 'it handles new_roles'
        end
      end

      context 'when in production' do
        around(:each) do |example|
          ClimateControl.modify SWOOP_ENV: "production" do
            example.run
          end
        end

        context 'when roles are nonroot' do
          let(:api_roles) { nonroot }

          it_behaves_like 'it does not include root'
        end

        context 'when roles are swoop_root' do
          let(:api_roles) { swoop_root }

          it_behaves_like 'it does not include root'
        end

        it_behaves_like "fleet and rescue"
      end

      context 'when not in production' do
        context 'when roles are nonroot' do
          let(:api_roles) { nonroot }

          it_behaves_like 'it does not include root'
        end

        context 'when roles are swoop_root' do
          let(:api_roles) { swoop_root }

          it_behaves_like 'it includes root'
        end

        it_behaves_like "fleet and rescue"
      end
    end

    context 'with neither' do
      it 'works' do
        expect { described_class.call }.to raise_error(ArgumentError)
      end
    end
  end
end
