# frozen_string_literal: true

require "rails_helper"

describe Users::RestorationService do
  describe '.call' do
    subject { described_class.call(params) }

    let(:deleted_user) { FactoryBot.create(:user, :deleted) }

    context 'when username provided' do
      let(:params) do
        { username: deleted_user.username }
      end

      it 'undeletes user' do
        expect { subject }.to change { deleted_user.reload.deleted_at }.to(nil)
      end
    end

    context 'when email provided' do
      let(:params) do
        { email: deleted_user.email }
      end

      it 'undeletes user' do
        expect { subject }.to change { deleted_user.reload.deleted_at }.to(nil)
      end
    end

    context 'when user cannot be found' do
      let(:params) do
        { email: 'missing@email.com' }
      end

      it 'returns 404' do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
