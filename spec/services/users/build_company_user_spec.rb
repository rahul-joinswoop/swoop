# frozen_string_literal: true

require "rails_helper"

describe Users::BuildCompanyUser do
  subject(:build_company_user) do
    Users::BuildCompanyUser.new(
      user_attributes: user_attributes,
      new_roles: new_roles,
      notification_settings: notification_settings,
      api_user: api_user,
      api_company: api_company,
      api_roles: api_roles,
    ).call.user
  end

  let(:notification_settings) { [] } # only rescue company can set it
  let(:user_attributes) do
    {
      job_status_email_frequency: "none",
      username: "username_abc",
      password: "123123123",
      first_name: "FirstName",
      last_name: "And LastName",
      company_id: company_id,
    }
  end

  let(:new_roles) { ["admin"] }
  let(:api_company) { nil }
  let(:api_user) { nil }
  let(:api_roles) { nil }

  shared_examples 'build the user successfully' do |expected_company_role|
    it 'builds the user successfully' do
      user = build_company_user

      expect(user.valid?).to be_truthy
      expect(user.persisted?).to be_falsey
      expect(user.username).to eq "username_abc"
      expect(user.password).to eq "123123123"
      expect(user.first_name).to eq "FirstName"
      expect(user.last_name).to eq "And LastName"
      expect(user.company_id).to eq company_id
      expect(user.language).to eq company.language
    end

    context 'when roles are nil' do
      let(:new_roles) { nil }

      it 'builds a valid user successfully' do
        user = build_company_user

        expect(user.valid?).to be_truthy
        expect(user.roles.map(&:name)).to eq [expected_company_role.to_s]
      end
    end
  end

  shared_examples 'build the notification_settings successfully' do
    it 'builds the notification settings successfully' do
      user = build_company_user

      expect(user.notification_settings.first.notification_code).to eq "new_job"
      expect(user.notification_settings.first.notification_channels).to eq ["Text"]
    end
  end

  context "with an api_user" do
    context 'when api_user is a rescue user' do
      let(:company) { create(:rescue_company) }
      let(:company_id) { company.id }
      let(:api_user) { create(:user, roles: [:rescue, :admin], company: company) }
      let(:notification_settings) { [{ notification_code: "new_job", notification_channels: ["Text"] }] }

      it_behaves_like 'build the user successfully', :rescue
      it_behaves_like 'build the notification_settings successfully'
    end

    context 'when api_user is a fleet user' do
      let(:company) { create(:fleet_company) }
      let(:company_id) { company.id }
      let(:api_user) { create(:user, roles: [:fleet, :admin], company: company) }

      it_behaves_like 'build the user successfully', :fleet
    end

    context 'when api_user is a root user' do
      let(:company) { create(:super_company) }
      let(:api_user) { create(:user, roles: [:root], company: company) }

      context 'and a rescue user is being created' do
        let(:rescue_company) { create(:rescue_company) }
        let(:company_id) { rescue_company.id }

        it_behaves_like 'build the user successfully', :rescue
      end

      context 'and a fleet user is being created' do
        let(:fleet_company) { create(:fleet_company) }
        let(:company_id) { fleet_company.id }

        it_behaves_like 'build the user successfully', :fleet
      end
    end
  end

  context "with an api_company and api_roles" do
    context 'when api_company is a rescue company' do
      let(:company) { create(:rescue_company) }
      let(:company_id) { company.id }
      let(:api_roles) { [:rescue, :admin] }
      let(:api_company) { company }
      let(:notification_settings) { [{ notification_code: "new_job", notification_channels: ["Text"] }] }

      it_behaves_like 'build the user successfully', :rescue
      it_behaves_like 'build the notification_settings successfully'
    end

    context 'when api_company is a fleet company' do
      let(:company) { create(:fleet_company) }
      let(:company_id) { company.id }
      let(:api_roles) { [:fleet, :admin] }
      let(:api_company) { company }

      it_behaves_like 'build the user successfully', :fleet
    end

    context 'when api_company is swoop and roles include root' do
      let(:company) { create(:super_company) }
      let(:api_roles) { [:root] }
      let(:api_company) { company }

      context 'and a rescue user is being created' do
        let(:rescue_company) { create(:rescue_company) }
        let(:company_id) { rescue_company.id }

        it_behaves_like 'build the user successfully', :rescue
      end

      context 'and a fleet user is being created' do
        let(:fleet_company) { create(:fleet_company) }
        let(:company_id) { fleet_company.id }

        it_behaves_like 'build the user successfully', :fleet
      end
    end
  end
end
