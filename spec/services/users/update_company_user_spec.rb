# frozen_string_literal: true

require "rails_helper"

describe Users::UpdateCompanyUser do
  subject(:update_company_user) do
    Users::UpdateCompanyUser.new(
      user: user_to_be_updated,
      user_attributes: user_attributes,
      roles: new_roles,
      notification_settings: notification_settings,
      system_email_filtered_company_ids: system_email_filtered_company_ids,
      api_user: api_user
    ).call.user
  end

  let(:notification_settings) { [] } # only rescue company can set it
  let(:user_attributes) do
    {
      job_status_email_frequency: "none",
      username: "username_abc",
      password: "123123123",
      first_name: "FirstName",
      last_name: "And LastName",
    }
  end

  let(:user_to_be_updated) { create(:user, company_id: company_id) }

  let(:new_roles) { ["admin"] }
  let(:system_email_filtered_company_ids) { [] }

  shared_examples 'update the user successfully' do
    it 'updates the user successfully' do
      user = update_company_user

      expect(user.valid?).to be_truthy
      expect(user.persisted?).to be_truthy
      expect(user.username).to eq "username_abc"
      expect(user.password).to eq "123123123"
      expect(user.first_name).to eq "FirstName"
      expect(user.last_name).to eq "And LastName"
      expect(user.company_id).to eq company_id
    end
  end

  shared_examples 'update the notification_settings successfully' do
    it 'updates the notification settings successfully' do
      user = update_company_user

      expect(user.notification_settings.first.notification_code).to eq "new_job"
      expect(user.notification_settings.first.notification_channels).to eq ["Text"]
    end
  end

  context 'when api_user is a rescue user' do
    let(:company) { create(:rescue_company) }
    let(:company_id) { company.id }
    let(:api_user) { create(:user, roles: [:rescue, :admin], company: company) }
    let(:notification_settings) { [{ notification_code: "new_job", notification_channels: ["Text"] }] }

    it_behaves_like 'update the user successfully'
    it_behaves_like 'update the notification_settings successfully'
  end

  context 'when api_user is a fleet user' do
    let(:company) { create(:fleet_company) }
    let(:company_id) { company.id }
    let(:api_user) { create(:user, roles: [:fleet, :admin], company: company) }

    it_behaves_like 'update the user successfully'
  end

  context 'when api_user is a root user' do
    let(:company) { create(:super_company) }
    let(:api_user) { create(:user, roles: [:root], company: company) }

    context 'and a rescue user is being edited' do
      let(:rescue_company) { create(:rescue_company) }
      let(:company_id) { rescue_company.id }

      it_behaves_like 'update the user successfully'
    end

    context 'and a fleet user is being edited' do
      let(:fleet_company) { create(:fleet_company) }
      let(:company_id) { fleet_company.id }

      it_behaves_like 'update the user successfully'
    end

    context 'and a swoop user is being edited' do
      let(:company_id) { company.id }
      let(:fleet_company) { create(:fleet_company) }
      let(:system_email_filtered_company_ids) { [fleet_company.id] }

      it_behaves_like 'update the user successfully'

      it 'updates user.system_email_filtered_companies successfully' do
        expect { update_company_user && user_to_be_updated.reload }.to change(user_to_be_updated.system_email_filtered_companies, :size).from(0).to(1)
      end
    end
  end
end
