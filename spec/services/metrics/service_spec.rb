# frozen_string_literal: true

require 'rails_helper'

describe Metrics::Service do
  let!(:job) { create :rescue_job }
  let(:split_name) { 'job_claim_cost' }
  let(:metric_name) { 'cost_estimate' }

  describe 'track metrics' do
    subject do
      Metrics::Service.track(job, 'job', metric_name, 100)
    end

    context 'external service returns true' do
      let(:value) { 100 }

      before do
        allow(External::Split).to receive(:track)
          .with(job, 'job', metric_name, value)
          .and_return(true)
      end

      it 'calls track ok' do
        expect { subject }.to change(Metric, :count).from(0).to(1)
      end

      context 'job in experiement' do
        let!(:experiment) { Experiment.create!(name: 'ex 1') }
        let!(:variant) { Variant.create(experiment: experiment, name: 'variant 1') }
        let!(:target_variant) { TargetVariant.create!(target: job, variant: variant) }

        it 'calls track ok' do
          expect do
            subject
          end.to change(Metric, :count).from(0).to(1)
            .and change(ExperimentMetric, :count).from(0).to(1)

          em = ExperimentMetric.first
          expect(em.target).to eq(job)
          expect(em.variant).to eq(variant)
          expect(em.label).to eq(metric_name)
          expect(em.value).to eq(value)
        end
      end
    end
  end
end
