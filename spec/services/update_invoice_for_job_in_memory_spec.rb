# frozen_string_literal: true

require 'rails_helper'

describe UpdateInvoiceForJobInMemory do
  include Requests::InvoiceHelpers

  let(:swoop_company) { create(:super_company) }

  context 'Partner To Swoop Invoice' do
    let(:partner_company) do
      create(:rescue_company, {
        name: 'Finish Line Towing',
      })
    end

    let(:partners_swoop_account) do
      create(
        :account,
        name: 'Swoop',
        company: partner_company,
        client_company: swoop_company
      )
    end

    context 'partner_new' do
      subject(:invoice) do
        create(
          :invoice,
          state: 'partner_new',
          sender: partner_company,
          recipient: partners_swoop_account,
          recipient_company: partners_swoop_account.client_company
        )
      end

      let(:job) do
        create(:fleet_managed_job, {
          rescue_company: partner_company,
          invoice: subject,
          account: partners_swoop_account,
          service_code: create(:service_code, name: 'Tow'),
        })
      end

      context 'flat rate' do
        let!(:flat_rate) do
          create(:clean_flat_rate, {
            company: partner_company,
            live: true,
            account: partners_swoop_account,
          })
        end

        it 'Adds all line items' do
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          job.get_rate).call
          job.save!
          dump_invoice(subject)
          expect(subject.line_items.length).to eql(2)
          li = find_li(subject, "Flat Rate")
          expect(li.net_amount).to eql(75)
          expect(li.rate_id).to eql(flat_rate.id)
        end
      end

      context 'miles_p2p rate' do
        let!(:miles_p2p_rate) do
          create(:miles_p2p_rate,
                 :miles_enroute,
                 :miles_towed,
                 :miles_deadhead, {
                   company: partner_company,
                   live: true,
                 })
        end

        before do
          job.build_estimate(meters_ab: 10000, meters_bc: 10000, meters_ca: 20000)
        end

        it 'updates the invoice when miles_ab changes' do
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          job.get_rate).call
          job.save!
          dump_invoice(subject)
          expect(subject.line_items.length).to eql(8)
          expect(find_li(subject, "Hookup").net_amount).to eql(100)
          expect(find_li(subject, "En Route Per Mile").net_amount).to eql(12.4)
          expect(find_li(subject, "En Route Free Miles").net_amount).to eql(-12.4)
          expect(find_li(subject, "Towed Per Mile").net_amount).to eql(6.2)
          expect(find_li(subject, "Towed Free Miles").net_amount).to eql(-5)
          expect(find_li(subject, "Deadhead Per Mile").net_amount).to eql(0)
          expect(find_li(subject, "Deadhead Free Miles").net_amount).to eql(0)

          job.estimate.meters_ab = 100
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          job.get_rate).call
          job.save!
          expect(subject.line_items.length).to eql(8)
          expect(find_li(subject, "Hookup").net_amount).to eql(100)
          expect(find_li(subject, "En Route Per Mile").net_amount).to eql(0.2)
          expect(find_li(subject, "En Route Free Miles").net_amount).to eql(-0.2)
          expect(find_li(subject, "Towed Per Mile").net_amount).to eql(6.2)
          expect(find_li(subject, "Towed Free Miles").net_amount).to eql(-5)
          expect(find_li(subject, "Deadhead Per Mile").net_amount).to eql(0)
          expect(find_li(subject, "Deadhead Free Miles").net_amount).to eql(0)
        end
      end

      context 'manual rate override' do
        let!(:flat_rate) do
          create(:clean_flat_rate, {
            company: partner_company,
            live: true,
          })
        end

        let!(:miles_p2p_rate) do
          create(:miles_p2p_rate,
                 :miles_enroute,
                 :miles_towed,
                 :miles_deadhead, {
                   company: partner_company,
                   live: true,
                   account: partners_swoop_account,
                 })
        end

        it 'Chooses the more specific rate' do
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          job.get_rate).call
          job.save!
          expect(job.invoice.rate_type).to eql(MilesP2PRate.name)
        end

        it 'honors the override' do
          job.invoice.rate_type = 'FlatRate'
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          job.get_rate).call
          job.save!
          expect(job.invoice.rate_type).to eql(FlatRate.name)
        end
      end

      context 'percentage add-ons' do
        let!(:flat_rate) do
          create(:clean_flat_rate, {
            company: partner_company,
            live: true,
          })
        end

        it 'calcs percentage addition' do
          flat_rate.additions.create!(
            name: 'fuel',
            calc_type: 'percent',
            amount: 10
          )
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          job.get_rate).call
          job.save!
          dump_invoice(subject)
          li = find_li(subject, "Flat Rate")
          expect(li.net_amount).to eql(75)
          expect(li.rate_id).to eql(flat_rate.id)

          expect(find_li(subject, "fuel").net_amount).to eql(7.5)
        end

        context 'addition attached to default vehicle rate' do
          let(:flatbed) do
            create(:vehicle_category)
          end

          let!(:flat_heavy_rate) do
            create(:clean_flat_rate, {
              company: partner_company,
              live: true,
              vehicle_category_id: flatbed.id,
              flat: 100,
            })
          end

          it 'calcs percentage addition' do
            flat_rate.additions.create!(
              name: 'fuel',
              calc_type: 'percent',
              amount: 10
            )
            UpdateInvoiceForJobInMemory.new(subject,
                                            job,
                                            flat_heavy_rate).call
            job.save!
            dump_invoice(subject)
            li = find_li(subject, "Flat Rate")
            expect(li.net_amount).to eql(100)
            expect(li.rate_id).to eql(flat_heavy_rate.id)

            expect(find_li(subject, "fuel").net_amount).to eql(10)
          end
        end
      end

      context 'storage' do
        let!(:inventory_item) do
          create(:inventory_item, :with_vehicle, :not_released, job: job)
        end
        let!(:deleted_storage_ajs) do
          create(:audit_job_status, {
            job: job,
            job_status_id: JobStatus::STORED,
          })
        end

        it 'does not create storage line item' do
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          job.get_rate).call
          job.save!
          dump_invoice(subject)
          expect(subject.line_items.length).to eql(2)
          li = find_li(subject, "Storage")
          expect(li).to be_nil
        end
      end

      context 'deleted storage' do
        let(:deleted_time) { Time.now }
        let!(:deleted_storage_ajs) do
          create(:audit_job_status, {
            job: job,
            job_status_id: JobStatus::STORED,
            deleted_at: deleted_time,
          })
        end
        let!(:inventory_item) do
          create(:inventory_item, :with_vehicle, :not_released, job: job, deleted_at: deleted_time)
        end

        it 'Does not create storage line item' do
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          job.get_rate).call
          job.save!
          dump_invoice(subject)
          expect(subject.line_items.length).to eql(2)
          li = find_li(subject, "Storage")
          expect(li).to be_nil
        end

        context 'existing storage invoicing line item' do
          let!(:storage_line_item) do
            create :invoicing_line_item, {
              job: job,
              ledger_item: invoice,
              description: "Storage",
            }
          end

          it 'does not remove storage line item' do
            UpdateInvoiceForJobInMemory.new(subject,
                                            job,
                                            job.get_rate).call
            job.save!
            dump_invoice(subject)
            expect(subject.line_items.length).to eql(2)
            li = find_li(subject, "Storage")
            expect(li).to be_present
          end
        end
      end
    end
  end

  context 'Client to End Customer Invoice' do
    let(:fleet_company) { create(:fleet_company, :turo) }

    let(:client_customer_account) do
      create(
        :account,
        name: 'Customer',
        company: fleet_company,
        client_company: nil
      )
    end

    context 'fleet_customer_new' do
      subject(:invoice) do
        create(
          :invoice,
          state: 'fleet_customer_new',
          sender: fleet_company,
          recipient: job.customer,
          recipient_company: nil
        )
      end

      let(:job) do
        create(:fleet_managed_job, {
          fleet_company: fleet_company,
          invoice: nil,
          account: nil,
          service_code: create(:service_code, name: 'Tow'),
        })
      end

      context 'flat rate' do
        let!(:flat_rate) do
          create(:clean_flat_rate, {
            company: fleet_company,
            live: true,
            account: client_customer_account,
          })
        end

        it 'Adds all line items' do
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          flat_rate).call
          subject.save!
          dump_invoice(subject)
          expect(subject.line_items.length).to eql(2)
          expect(find_li(subject, "Flat Rate").net_amount).to eql(75)
        end

        it 'does not call storage tick' do
          expect(StorageTick).not_to receive(:new)
          UpdateInvoiceForJobInMemory.new(subject,
                                          job,
                                          flat_rate).call
        end
      end
    end
  end
end
