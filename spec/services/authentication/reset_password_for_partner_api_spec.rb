# frozen_string_literal: true

require 'rails_helper'

describe Authentication::ResetPasswordForPartnerAPI do
  subject { described_class.call(params) }

  let!(:user) { create :user }
  let(:client) { create :doorkeeper_application }
  let!(:access_grant) { create :doorkeeper_access_grant, resource_owner_id: user.id, application_id: client.id }
  let(:params) { { user: user, client: client } }

  context 'negative cases' do
    [:user, :client].each do |param|
      context "without #{param}" do
        let(:params) { super().except(param) }

        it 'is a failure' do
          expect(subject).to be_failure
          expect(subject.error).to eq("#{param} is required!")
        end
      end
    end

    context "without an access grant" do
      let!(:access_grant) { nil }

      it 'is a failure' do
        expect(subject).to be_failure
        expect(subject.error).to eq("Validation failed: User must have granted access to client app")
      end
    end

    context "with an invalid request" do
      it 'is a failure' do
        # LOLWUT - we don't have direct access to the underlying PartnerAPIPasswordRequest used
        # so we hijack the #trigger call to set valid_request: false.
        allow_any_instance_of(PartnerAPIPasswordRequest).to receive(:trigger) do |ev|
          ev.valid_request = false
          ev
        end

        expect(subject).to be_failure
        expect(subject.error).to eq("Invalid request")
      end
    end
  end

  context 'positive cases' do
    context 'common' do
      it 'is a success and the trigger method is triggered' do
        expect_any_instance_of(PartnerAPIPasswordRequest).to receive(:trigger!)
        expect(subject).to be_success
      end

      it 'the result is a PartnerAPIPasswordRequest' do
        expect(subject.result).to be_a(PartnerAPIPasswordRequest)
      end
    end
  end
end
