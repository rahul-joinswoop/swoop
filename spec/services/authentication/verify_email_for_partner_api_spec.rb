# frozen_string_literal: true

require 'rails_helper'

describe Authentication::VerifyEmailForPartnerAPI do
  subject { described_class.call(params) }

  let!(:user) { create :user, username: 'FancyUserName' }
  let(:application) { create :doorkeeper_application }
  let(:client_id) { application.uid }
  let(:redirect_uri) { application.redirect_uri }
  let(:email) { user.email }
  let(:params) do
    {
      email: email,
      client_id: client_id,
      redirect_uri: redirect_uri,
    }
  end

  context 'negative cases' do
    context 'missing params' do
      [:email, :client_id, :redirect_uri].each do |param|
        context "without #{param}" do
          let(:params) { super().except(param) }

          it 'is a failure' do
            expect(subject).to be_failure
            expect(subject.error).to eq("#{param} is required!")
          end
        end
      end
    end

    context 'there is no user with the email' do
      let(:email) { 'invalid@email.com' }

      it 'is a failure' do
        expect(subject).to be_failure
        expect(subject.user).to be_nil
      end
    end

    context 'with a bogus client_id' do
      let(:client_id) { SecureRandom.uuid }

      it 'is a failure' do
        expect(subject).to be_failure
        expect(subject.error).to eq("Validation failed: Client is invalid")
      end
    end

    context 'with a bogus redirect_uri' do
      let(:redirect_uri) { Faker::Internet.url }

      it 'is a failure' do
        expect(subject).to be_failure
        expect(subject.error).to eq("Validation failed: Redirect uri is invalid")
      end
    end

    context "with an invalid request" do
      it 'is a failure' do
        # LOLWUT - we don't have direct access to the underlying PartnerAPIEmailVerification used
        # so we hijack the #trigger call to set valid_request: false.
        allow_any_instance_of(PartnerAPIEmailVerification).to receive(:trigger) do |ev|
          ev.valid_request = false
          ev
        end

        expect(subject).to be_failure
        expect(subject.error).to eq("Invalid request")
      end
    end

    context 'with a username' do
      let(:params) { super().merge email: user.username }

      it 'is a failure' do
        expect(subject).to be_failure
      end
    end
  end

  context 'positive cases' do
    context 'common' do
      it 'is a success and the trigger method is triggered' do
        expect_any_instance_of(PartnerAPIEmailVerification).to receive(:trigger!)
        expect(subject).to be_success
      end

      it 'the result is a PartnerAPIEmailVerification' do
        expect(subject.result).to be_a(PartnerAPIEmailVerification)
      end
    end

    context 'email is in uppercase' do
      let(:params) { super().merge email: user.email.upcase }

      it 'is a success' do
        expect(subject).to be_success
      end
    end

    context 'email is with lower case letters' do
      let(:params) { super().merge email: user.email.downcase }

      it 'is a success' do
        expect(subject).to be_success
      end
    end

    context 'with a valid but not identical redirect_uri' do
      # we can add params to the redirect_uri
      let(:redirect_uri) { super() + "?foo=123" }

      it 'is a success' do
        expect(subject).to be_success
      end
    end
  end
end
