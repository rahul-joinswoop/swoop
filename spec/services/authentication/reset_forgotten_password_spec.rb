# frozen_string_literal: true

require 'rails_helper'

describe Authentication::ResetForgottenPassword do
  subject do
    Authentication::ResetForgottenPassword.call(params)
  end

  let!(:user) { create :user, username: 'FancyUserName' }
  let(:params) { { user_input: user.email } }

  let(:service_context) { subject }

  context 'negative cases' do
    context 'missing params' do
      let(:params) { super().except(:user_input) }

      it 'is a failure' do
        expect(service_context.failure?).to be true
        expect(service_context.error).to eq("user_input is required!")
      end
    end

    context 'there is no user with the email' do
      let(:params) { super().merge user_input: 'invalid@email.com' }

      it 'is a failure' do
        expect(service_context.failure?).to be true
        expect(service_context.user).to be_nil
      end
    end

    context 'there is no user with that username' do
      let(:params) { super().merge user_input: 'non_existent_username' }

      it 'is a failure' do
        expect(service_context.failure?).to be true
        expect(service_context.user).to be_nil
      end
    end
  end

  context 'positive cases' do
    context 'common' do
      it 'is a success and the trigger method is triggered' do
        expect_any_instance_of(PasswordRequest).to receive(:trigger!)
        expect(service_context.success?).to be true
      end

      it 'the result is a PasswordRequest' do
        expect(service_context.result).to be_a(PasswordRequest)
      end
    end

    context 'email is in uppercase' do
      let(:params) { super().merge user_input: user.email.upcase }

      it 'is a success' do
        expect(service_context.success?).to be true
      end
    end

    context 'input is a username' do
      context 'username is with capital letter' do
        let(:params) { super().merge user_input: user.username }

        it 'is a success' do
          expect(service_context.success?).to be true
        end
      end

      context 'username is with lower case letters' do
        let(:params) { super().merge user_input: user.username.downcase }

        it 'is a success' do
          expect(service_context.success?).to be true
        end
      end
    end
  end
end
