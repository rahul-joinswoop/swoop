# frozen_string_literal: true

require "rails_helper"

describe SymptomServices::Change do
  subject(:service_call) do
    described_class.new(
      api_company: api_company,
      standard_symptom_ids_to_add: standard_symptom_ids_to_add,
      symptom_ids_to_remove: symptom_ids_to_remove,
      custom_symptom_names_to_add: custom_symptom_names_to_add
    ).call
  end

  context 'when api_company is a RescueCompany' do
    let(:api_company) { create :rescue_company, :with_symptoms }

    let(:another_standard_symptom) do
      create :symptom, name: Symptom::CUSTOMER_UNSAFE, is_standard: true
    end

    let(:standard_symptom_ids_to_add) do
      [another_standard_symptom.id]
    end

    let(:current_standard_symptom) do
      api_company.symptoms.find_by(name: Symptom::ACCIDENT)
    end

    let(:symptom_ids_to_remove) do
      [current_standard_symptom.id]
    end

    let(:custom_symptom_name) { 'A new custom symptom for Rescue' }

    let(:custom_symptom_names_to_add) do
      [{ name: custom_symptom_name }]
    end

    it 'adds a standard symptom' do
      service_call

      expect(api_company.symptoms).to include(another_standard_symptom)
    end

    it 'removes a standard symptom' do
      service_call

      expect(api_company.symptoms).not_to include(current_standard_symptom)
    end

    it 'adds a custom symptom' do
      service_call

      custom_symptom = Symptom.find_by(name: custom_symptom_name)

      expect(api_company.symptoms).to include(custom_symptom)
    end
  end

  context 'when api_company is a FleetCompany' do
    let(:api_company) { create :fleet_company, :with_symptoms }

    let(:another_standard_symptom) do
      create :symptom, name: Symptom::CUSTOMER_UNSAFE, is_standard: true
    end

    let(:standard_symptom_ids_to_add) do
      [another_standard_symptom.id]
    end

    let(:current_standard_symptom) do
      api_company.symptoms.find_by(name: Symptom::ACCIDENT)
    end

    let(:symptom_ids_to_remove) do
      [current_standard_symptom.id]
    end

    let(:custom_symptom_name) { 'A new custom symptom for Rescue' }

    let(:custom_symptom_names_to_add) do
      [{ name: custom_symptom_name }]
    end

    it 'adds a standard symptom' do
      service_call

      expect(api_company.symptoms).to include(another_standard_symptom)
    end

    it 'removes a standard symptom' do
      service_call

      expect(api_company.symptoms).not_to include(current_standard_symptom)
    end

    it 'adds a custom symptom' do
      service_call

      custom_symptom = Symptom.find_by(name: custom_symptom_name)

      expect(api_company.symptoms).to include(custom_symptom)
    end
  end
end
