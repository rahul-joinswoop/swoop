# frozen_string_literal: true

require "rails_helper"

describe SymptomServices::Batch do
  subject(:service_call) do
    described_class.new(
      api_company: api_company,
      symptom_ids: symptom_ids
    ).call
  end

  context 'when api_company is a RescueCompany' do
    let(:api_company)   { create :rescue_company, :with_symptoms }
    let(:fleet_company) { create :fleet_company, :with_symptoms }

    let(:symptom_ids) do
      api_company.symptoms.pluck(:id) + fleet_company.symptoms.pluck(:id)
    end

    let(:account) do
      create :account, company: api_company, client_company: fleet_company
    end

    it 'returns own symptoms' do
      partner_symptom = Symptom.find(
        api_company.symptoms.first.id
      )

      expect(service_call).to include(partner_symptom)
    end

    it 'returns custom symptoms from accounts' do
      symptom_from_account = Symptom.find(
        account.client_company.symptoms.first.id
      )

      expect(service_call).to include(symptom_from_account)
    end
  end

  context 'when api_company is a FleetCompany' do
    let(:api_company) { create :fleet_company, :with_symptoms }
    let(:symptoms_from_api_company) { api_company.symptoms }
    let(:any_other_symptom) { create :symptom, name: 'Any other Symptom' }

    let(:any_symptom_ids) { [any_other_symptom.id] }

    let(:symptom_ids) do
      api_company.symptoms.pluck(:id) + any_symptom_ids
    end

    it 'returns all symptoms requested' do
      expect(service_call).to include(*symptoms_from_api_company, any_other_symptom)
    end
  end

  context 'when api_company is Swoop' do
    let(:api_company) { create :super_company, :swoop }

    let(:symptom_1) { create :symptom, name: 'Symptom 1' }
    let(:symptom_2) { create :symptom, name: 'Symptom 2' }

    let(:symptom_ids) do
      [symptom_1.id, symptom_2.id]
    end

    it 'returns all symptoms requested' do
      expect(service_call).to include(symptom_1, symptom_2)
    end
  end
end
