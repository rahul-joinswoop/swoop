# frozen_string_literal: true

require "rails_helper"

RSpec.describe UserAgentParser do
  subject(:parsed_user_agent) { UserAgentParser.new(user_agent, platform: platform).call }

  shared_examples 'matching expected values' do |platform, browser, version, client_or_swoop_version, swoop_custom_platform, build|
    it "matches to expected values" do
      expect(parsed_user_agent).to match({
        platform: platform,
        browser: browser,
        version: version,
        client_or_swoop_version: client_or_swoop_version,
        swoop_custom_platform: swoop_custom_platform,
        build: build,
      })
    end
  end

  let(:platform) { nil }

  context 'when user-agent is ios' do
    let(:user_agent) { "ios Swoop/1.15.33.265 (086e96f7d)" }

    it_behaves_like 'matching expected values', "ios", :generic, "1.15.33.265", "1.15.33.265", "React iOS", "086e96f7d"
  end

  context 'when user-agent is android' do
    let(:user_agent) { "android Swoop/2.0.63" }

    it_behaves_like 'matching expected values', "android", :generic, "2.0.63", "2.0.63", "React Android", nil
  end

  context 'when user-agent is not Swoop' do
    let(:user_agent) { "Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K)" }

    it_behaves_like 'matching expected values', "android", :generic, "0", "0", "React Android", nil
  end

  context 'when user-agent is Mac web' do
    let(:user_agent) { 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36' }

    it_behaves_like 'matching expected values', "mac", :chrome, "69", Swoop::VERSION, "Web", nil
  end

  context 'when user-agent is Windows web' do
    let(:user_agent) { 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36' }

    it_behaves_like 'matching expected values', "windows", :chrome, "69", Swoop::VERSION, "Web", nil
  end

  context 'when user-agent is broken' do
    let(:user_agent) { "kinda broken Swoop thing" }

    it_behaves_like 'matching expected values', "kinda", :generic, "broken", "broken", "broken", "woo"

    it "falls back to other types that match" do
      expect(parsed_user_agent).to match({
        platform: "kinda",
        browser: :generic,
        version: "broken",
        build: "woo",
        client_or_swoop_version: "broken",
        swoop_custom_platform: 'broken',
      })
    end
  end

  context 'when platform is set as :api' do
    let(:platform) { :api }

    context 'and user-agent is Mac (when using GraphiQL for instance)' do
      let(:user_agent) { 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36' }

      it_behaves_like 'matching expected values', "mac", :chrome, "69", Swoop::VERSION, "API", nil
    end

    context 'and user-agent is Windows (when using GraphiQL for instance)' do
      let(:user_agent) { 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36' }

      it_behaves_like 'matching expected values', "windows", :chrome, "69", Swoop::VERSION, "API", nil
    end
  end
end
