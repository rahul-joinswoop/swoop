# frozen_string_literal: true

require 'rails_helper'

describe Publishers::AsyncRequestWebsocketMessage do
  subject(:ws_message_call) do
    Publishers::AsyncRequestWebsocketMessage.call(params)
  end

  let(:params) do
    {
      input: {
        some_object: { id: 1, name: 'Im an obj attr' },
      },
      id: api_async_request.id,
    }
  end

  let(:swoop_company) { create(:super_company) }

  let(:swoop_user) { create(:user, company_id: swoop_company.id) }

  let(:api_async_request) do
    create(
      :api_async_request,
      system: 'Any',
      company: swoop_company,
      user: swoop_user
    )
  end

  it 'calls formats the WS message for the AsyncRequest accordingly' do
    allow(Publishers::GenericWebsocketMessage).to receive(:call)

    expect(Publishers::GenericWebsocketMessage).to receive(:call).with({
      input: {
        class_name: 'AsyncRequest',
        target_data: {
          id: api_async_request.id,
          data: { some_object: { id: 1, name: 'Im an obj attr' }, async_request: { id: api_async_request.id } },
        },
        company: Company.find(api_async_request.company_id),
      },
    })

    ws_message_call
  end
end
