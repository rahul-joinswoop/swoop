# frozen_string_literal: true

require 'rails_helper'

describe JobAlerts::NinetyMinutesFromSchedule do
  subject(:service_call) { JobAlerts::NinetyMinutesFromSchedule.new.call }

  context 'with a Scheduled job' do
    let(:job) { create(:fleet_managed_job, scheduled_for: Time.now + 90.minutes, status: status) }
    let!(:scheduled_system_alert) { create(:scheduled_system_alert, job: job, send_at: send_at) }

    shared_examples 'call Job.new_job_call_swoop_agents' do
      it 'calls Job.new_job_call_swoop_agents' do
        expect(Job).to receive(:new_job_call_swoop_agents).with(swoop_user.phone, job.id)

        service_call
      end
    end

    context 'with the ScheduledSystemAlert.send_at < 1 minute from now' do
      let(:send_at) { Time.now - 2.minute }

      context 'when there are swoop users with system alerts activated' do
        let(:swoop_user) { create(:swoopuser, phone: Faker::PhoneNumber.phone_number_e164) }

        before do
          allow_any_instance_of(SystemEmailRecipientCalculator).to receive(:calculate).and_return([swoop_user])
        end

        context 'and job.status is pending' do
          let(:status) { 'pending' }

          it_behaves_like 'call Job.new_job_call_swoop_agents'
        end

        context 'and job.status is dispatched' do
          let(:status) { 'pending' }

          it_behaves_like 'call Job.new_job_call_swoop_agents'
        end

        context 'and job has any other status' do
          let(:status) { 'dispatched' }

          it 'does not call Job.new_job_call_swoop_agents' do
            expect(Job).not_to receive(:new_job_call_swoop_agents)

            service_call
          end
        end
      end
    end
  end
end
