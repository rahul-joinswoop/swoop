# frozen_string_literal: true

require 'rails_helper'

describe QuestionServices::FindAnswer, :vcr do
  subject do
    described_class.call(job: job, question: question_text).result
  end

  let!(:job) { create(:job) }

  context 'when question exists' do
    let!(:question) { create(:question_with_answers) }
    let!(:question_result) do
      create(:question_result, question: question,
                               answer: question.answers.first,
                               job: job)
    end
    let(:question_text) { question.question }

    it { is_expected.to eq(question.answers.first) }
  end

  context 'when question does not exist' do
    let(:question_text) { "Does Not Exist" }

    it { is_expected.to eq(nil) }
  end
end
