# frozen_string_literal: true

require "rails_helper"

describe PartnerUpdateJobState do
  subject(:call_update_job_state) do
    PartnerUpdateJobState.new(
      job, new_status, job_status_reason_type_id: job_status_reason_type_id
    ).call

    job.save!
  end

  let!(:job) { create(:rescue_job, status: current_status) }
  let(:job_status_reason_type_id) { nil }

  context 'when current job.status is Completed' do
    let(:current_status) { 'Completed' }

    describe 'job.last_touched_by' do
      include_context "job status change shared examples"

      subject do
        PartnerUpdateJobState.new(job, 'Dispatched', api_user: api_user).call
      end

      let(:current_status) { 'Pending' } # can be any

      it_behaves_like 'updates job.last_touched_by when api_user is passed'
    end

    context 'and new_status is Deleted' do
      let(:new_status) { 'Deleted' }

      it 'sets job status to deleted' do
        call_update_job_state

        expect(job.status).to eq Job::STATUS_DELETED
      end

      it 'deletes its invoice' do
        expect_any_instance_of(Job).to receive(:delete_invoice)

        call_update_job_state
      end
    end
  end

  context 'when current job.status is Dispatched' do
    let(:current_status) { 'Dispatched' }

    context 'and new job.status is Canceled' do
      let(:new_status) { 'Canceled' }

      it 'sets job.status to canceled' do
        call_update_job_state

        expect(job.status).to eq Job::STATUS_CANCELED
      end

      context 'and a JobStatusReasonType is passed' do
        let(:job_status_reason_type_id) { create(:job_status_reason_type, :cancel_prior_job_delayed).id }

        it 'adds one job.job_status_reason' do
          expect { call_update_job_state }.to change(JobStatusReason, :count).by(1)
        end
      end
    end

    context 'and new job.status is Completed' do
      include_context "job completed shared examples"

      subject do
        PartnerUpdateJobState.new(
          job,
          new_status,
          job_status_reason_type_id: job_status_reason_type_id,
          api_user: api_user,
          api_application: api_application,
          user_agent_hash: user_agent_hash,
          referer: referer
        ).call
      end

      let(:new_status) { 'Completed' }
      let(:api_user) { nil }
      let(:api_application) { nil }
      let(:user_agent_hash) do
        {
          platform: 'android',
          version: '1.2.3',
        }
      end
      let(:referer) { 'http://example.com' }

      context 'when api_user is passed' do
        let(:api_user) { create(:user) }

        it_behaves_like 'sending Analytics Job Completed event', 'android', '1.2.3', 'http://example.com'
      end

      context 'when api_application is passed' do
        let(:api_application) do
          double(:api_application, owner_id: company.id, owner_type: 'Company')
        end
        let(:company) { create(:rescue_company) }

        it_behaves_like 'sending Analytics Job Completed event', 'android', '1.2.3', 'http://example.com'
      end

      it 'sets job.status to completed' do
        call_update_job_state

        expect(job.status).to eq Job::STATUS_COMPLETED
      end
    end
  end

  context "when GOA" do
    let(:current_status) { 'Dispatched' }
    let(:new_status) { "GOA" }

    it "updates the invoice" do
      expect(job).to receive(:mark_invoice_goa)
      subject
    end
  end
end
