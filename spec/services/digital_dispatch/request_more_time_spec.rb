# frozen_string_literal: true

require 'rails_helper'

describe DigitalDispatch::RequestMoreTime do
  subject(:request_more_time) do
    DigitalDispatch::RequestMoreTime.new(
      job_id: job_id,
      api_company_id: rescue_company.id,
      api_user_id: user.id,
    ).call
  end

  let!(:account) do
    create(:account, company: rescue_company, client_company: fleet_company)
  end

  let(:job) do
    create :fleet_motor_club_job, {
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
      updated_at: '2018-07-26 18:08:04',
    }
  end
  let(:job_id) { job.id }

  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company, :ago) }

  let(:user) { create(:user, company: rescue_company) }

  let(:api_access_token) { create(:api_access_token) }
  let(:issc) do
    create(
      :ago_issc,
      company: rescue_company,
      api_access_token: api_access_token,
      system: Issc::RSC_SYSTEM
    )
  end

  before do
    allow(Agero::Rsc::RequestMoreTimeWorker).to receive(:perform_async)
  end

  it 'creates an AsyncRequest' do
    expect { request_more_time }.to change(API::AsyncRequest, :count).by(1)
  end

  it 'schedules Agero::Rsc::RequestMoreTimeWorker' do
    async_request = request_more_time.async_request

    expect(Agero::Rsc::RequestMoreTimeWorker).to have_received(:perform_async).with(
      job.id, async_request.id
    )
  end
end
