# frozen_string_literal: true

require 'rails_helper'

describe PCC::PolicyLookup::PolicyLookupService::PolicyLookupServiceRequest do
  let(:company) { create(:fleet_company, :lincoln) }
  let(:job) { create(:job, fleet_company: company) }
  let(:service_url) { Configuration.pls_api.url }
  let(:payload) { { "foo" => "bar" } }

  before do
    company.settings << Setting.create!(key: Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE, value: 'lincoln')
  end

  it "queries for vehicle policies by vin" do
    stub_request(:get, "#{service_url}/lincoln/vehicles").with(query: { vin: "12345678" }, headers: {
      "Content-Type" => "application/json; charset=utf-8",
      "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
    }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
    context = PCC::PolicyLookup::PolicyLookupService::PolicyLookupServiceRequest.call(input: { job_id: job.id, search_params: { vin: "12345678" } })
    expect(context.output[:job_id]).to eq job.id
    expect(context.output[:response]).to eq payload
  end

  it "queries for vehicle policies by license_number" do
    stub_request(:get, "#{service_url}/lincoln/vehicles").with(query: { license_number: "LX8890" }, headers: {
      "Content-Type" => "application/json; charset=utf-8",
      "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
    }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
    context = PCC::PolicyLookup::PolicyLookupService::PolicyLookupServiceRequest.call(input: { job_id: job.id, search_params: { license_number: "LX8890" } })
    expect(context.output[:job_id]).to eq job.id
    expect(context.output[:response]).to eq payload
  end

  it "queries of customer policies by last_name and postal_code" do
    stub_request(:get, "#{service_url}/lincoln/customers").with(query: { last_name: "Doe", postal_code: "60304" }, headers: {
      "Content-Type" => "application/json; charset=utf-8",
      "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
    }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
    context = PCC::PolicyLookup::PolicyLookupService::PolicyLookupServiceRequest.call(input: { job_id: job.id, search_params: { last_name: "Doe", postal_code: "60304" } })
    expect(context.output[:job_id]).to eq job.id
    expect(context.output[:response]).to eq payload
  end

  it "queries of customer policies by phone_number and country" do
    stub_request(:get, "#{service_url}/lincoln/customers").with(query: { phone_number: "4145551212", country: "US" }, headers: {
      "Content-Type" => "application/json; charset=utf-8",
      "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
    }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
    context = PCC::PolicyLookup::PolicyLookupService::PolicyLookupServiceRequest.call(input: { job_id: job.id, search_params: { phone_number: "4145551212", country: "US" } })
    expect(context.output[:job_id]).to eq job.id
    expect(context.output[:response]).to eq payload
  end
end
