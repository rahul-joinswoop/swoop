# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "policy lookup response serializer shared" do
  let(:policy_1) do
    OpenStruct.new(
      id: policy_1_id,
      uuid: policy_1_uuid,
      policy_number: policy_1_policy_number,
      vehicles: [
        OpenStruct.new(id: vehicle_1_id, uuid: vehicle_1_uuid),
        OpenStruct.new(id: vehicle_2_id, uuid: vehicle_2_uuid),
      ],
      drivers: [
        OpenStruct.new(id: customer_1_id, uuid: customer_1_uuid),
        OpenStruct.new(id: customer_2_id, uuid: customer_2_uuid),
      ]
    )
  end

  let(:policy_1_id) { 123 }
  let(:policy_1_uuid) { "1234-abcd" }
  let(:policy_1_policy_number) { "XYZ8712" }

  # I'm still unsure how it would work for multiple policies,
  # according to the code there should be only one pii_response
  # with all vehicles and drivers for all policies.
  let(:pii_response) do
    {
      # these are for policy_1
      vehicle_1_uuid => vehicle_1,
      vehicle_2_uuid => vehicle_2,
      customer_1_uuid => customer_1,
      customer_2_uuid => customer_2,

      # these are for policy_2
      vehicle_3_uuid => vehicle_3,
      vehicle_4_uuid => vehicle_4,
      customer_3_uuid => customer_3,
      customer_4_uuid => customer_4,
    }
  end

  let(:policy_2) do
    OpenStruct.new(
      id: policy_2_id,
      uuid: policy_2_uuid,
      vehicles: [
        OpenStruct.new(id: vehicle_3_id, uuid: vehicle_3_uuid),
        OpenStruct.new(id: vehicle_4_id, uuid: vehicle_4_uuid),
      ],
      drivers: [
        OpenStruct.new(id: customer_3_id, uuid: customer_3_uuid),
        OpenStruct.new(id: customer_4_id, uuid: customer_4_uuid),
      ]
    )
  end

  let(:policy_2_id) { 456 }
  let(:policy_2_uuid) { "9876-wxyz" }

  let(:vehicle_1) do
    {
      "vehicle" => {
        "uuid" => vehicle_1_uuid,
        "vin" => "5LMTJ3DH710484384",
        "license_number" => nil,
        "license_issuer" => nil,
        "make" => "LINCOLN",
        "model" => "MKC",
        "year" => 2019,
        "color" => nil,
        "purchase_date" => "2018-12-01",
      },
    }
  end

  let(:vehicle_1_id) { "1" }
  let(:vehicle_1_uuid) { "3483935107f16de640c3274d86913c81" }

  let(:vehicle_2) do
    {
      "vehicle" => {
        "uuid" => vehicle_2_uuid,
        "vin" => "4JMTJ3DH710484333",
        "license_number" => nil,
        "license_issuer" => nil,
        "make" => "LINCOLN",
        "model" => "MKC",
        "year" => 2018,
        "color" => nil,
        "purchase_date" => "2019-09-01",
      },
    }
  end

  let(:vehicle_2_id) { "2" }
  let(:vehicle_2_uuid) { "7777935107f16de640c3274d86913c00" }

  let(:customer_1) do
    {
      "customer" => {
        "uuid" => customer_1_uuid,
        "name" => "DELORAS MCDOG",
        "street_address" => "12345 PARSER ST",
        "city" => "PUNTA MAGRA",
        "region" => "FL",
        "postal_code" => "33980",
        "country_code" => "US",
        "phone_number" => "+15668349411",
        "business" => false,
      },
    }
  end

  let(:customer_1_id) { "1" }
  let(:customer_1_uuid) { "3f196696a3da9fa05df6e292942d8a19" }

  let(:customer_2) do
    {
      "customer" => {
        "uuid" => customer_2_uuid,
        "name" => "DELORAS FERRY",
        "street_address" => "26155 KIHN UNION",
        "city" => "PUNTA GORDA",
        "region" => "FL",
        "postal_code" => "33980",
        "country_code" => "US",
        "phone_number" => "+15559549406",
        "business" => false,
      },
    }
  end

  let(:customer_2_id) { "2" }
  let(:customer_2_uuid) { "9d196696a3da9fa05df6e292942d8c20" }

  let(:vehicle_3) do
    {
      "vehicle" => {
        "uuid" => vehicle_3_uuid,
        "vin" => "2QSKJ3DH710484259",
        "license_number" => nil,
        "license_issuer" => nil,
        "make" => "LINCOLN",
        "model" => "MKC",
        "year" => 2019,
        "color" => nil,
        "purchase_date" => "2016-09-15",
      },
    }
  end

  let(:vehicle_3_id) { "3" }
  let(:vehicle_3_uuid) { "8937135107f16de640c3274d86913k77" }

  let(:vehicle_4) do
    {
      "vehicle" => {
        "uuid" => vehicle_4_uuid,
        "vin" => "9IHLJ3DH710484216",
        "license_number" => nil,
        "license_issuer" => nil,
        "make" => "LINCOLN",
        "model" => "MKC",
        "year" => 2017,
        "color" => nil,
        "purchase_date" => "2019-01-04",
      },
    }
  end

  let(:vehicle_4_id) { "4" }
  let(:vehicle_4_uuid) { "8i2e935107f16de640c3274d8691iu81" }

  let(:customer_3) do
    {
      "customer" => {
        "uuid" => customer_3_uuid,
        "name" => "DELORAS MCCAT",
        "street_address" => "17283 REGEX ST",
        "city" => "PUNTA FINA",
        "region" => "FL",
        "postal_code" => "33980",
        "country_code" => "US",
        "phone_number" => "+12348349384",
        "business" => false,
      },
    }
  end

  let(:customer_3_id) { "3" }
  let(:customer_3_uuid) { "h28d6696a3da9fa05df6e292942d8j29" }

  let(:customer_4) do
    {
      "customer" => {
        "uuid" => customer_4_uuid,
        "name" => "MARY FERRY",
        "street_address" => "26155 UNION GATE",
        "city" => "PUNTA FORTE",
        "region" => "FL",
        "postal_code" => "22980",
        "country_code" => "US",
        "phone_number" => "+15999549411",
        "business" => false,
      },
    }
  end

  let(:customer_4_id) { "4" }
  let(:customer_4_uuid) { "qq936696a3da9fa05df6e292942d9uha" }
end
