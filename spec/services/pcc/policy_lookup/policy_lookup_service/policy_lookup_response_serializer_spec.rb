# frozen_string_literal: true

require 'rails_helper'
require_relative './policy_lookup_response_serializer_shared'

describe PCC::PolicyLookup::PolicyLookupService::PolicyLookupResponseSerializer do
  include_context "policy lookup response serializer shared"

  subject(:serialize_response) do
    PCC::PolicyLookup::PolicyLookupService::PolicyLookupResponseSerializer.call(
      input: {
        job_id: 1,
        pii_response: pii_response,
        policies: [policy_1, policy_2],
      }
    ).output
  end

  it 'returns the policy with respective pcc_drivers and pcc_vehicles' do
    expect(serialize_response).to eq [
      {
        id: policy_1_id,
        uuid: policy_1_uuid,
        policy_number: policy_1_policy_number,
        pcc_drivers: [
          { id: customer_1_id }.merge(customer_1["customer"].deep_symbolize_keys),
          { id: customer_2_id }.merge(customer_2["customer"].deep_symbolize_keys),
        ],
        pcc_vehicles: [
          { id: vehicle_1_id }.merge(vehicle_1["vehicle"].deep_symbolize_keys),
          { id: vehicle_2_id }.merge(vehicle_2["vehicle"].deep_symbolize_keys),
        ],
        pcc_vehicles_and_drivers_matrix: [
          {
            pcc_vehicle: { id: vehicle_1_id }.merge(vehicle_1["vehicle"].deep_symbolize_keys),
            pcc_driver: { id: customer_1_id }.merge(customer_1["customer"].deep_symbolize_keys),
          },
          {
            pcc_vehicle: { id: vehicle_1_id }.merge(vehicle_1["vehicle"].deep_symbolize_keys),
            pcc_driver: { id: customer_2_id }.merge(customer_2["customer"].deep_symbolize_keys),
          },
          {
            pcc_vehicle: { id: vehicle_2_id }.merge(vehicle_2["vehicle"].deep_symbolize_keys),
            pcc_driver: { id: customer_1_id }.merge(customer_1["customer"].deep_symbolize_keys),
          },
          {
            pcc_vehicle: { id: vehicle_2_id }.merge(vehicle_2["vehicle"].deep_symbolize_keys),
            pcc_driver: { id: customer_2_id }.merge(customer_2["customer"].deep_symbolize_keys),
          },
        ],
      },
      {
        id: policy_2_id,
        uuid: policy_2_uuid,
        policy_number: nil,
        pcc_drivers: [
          { id: customer_3_id }.merge(customer_3["customer"].deep_symbolize_keys),
          { id: customer_4_id }.merge(customer_4["customer"].deep_symbolize_keys),
        ],
        pcc_vehicles: [
          { id: vehicle_3_id }.merge(vehicle_3["vehicle"].deep_symbolize_keys),
          { id: vehicle_4_id }.merge(vehicle_4["vehicle"].deep_symbolize_keys),
        ],
        pcc_vehicles_and_drivers_matrix: [
          {
            pcc_vehicle: { id: vehicle_3_id }.merge(vehicle_3["vehicle"].deep_symbolize_keys),
            pcc_driver: { id: customer_3_id }.merge(customer_3["customer"].deep_symbolize_keys),
          },
          {
            pcc_vehicle: { id: vehicle_3_id }.merge(vehicle_3["vehicle"].deep_symbolize_keys),
            pcc_driver: { id: customer_4_id }.merge(customer_4["customer"].deep_symbolize_keys),
          },
          {
            pcc_vehicle: { id: vehicle_4_id }.merge(vehicle_4["vehicle"].deep_symbolize_keys),
            pcc_driver: { id: customer_3_id }.merge(customer_3["customer"].deep_symbolize_keys),
          },
          {
            pcc_vehicle: { id: vehicle_4_id }.merge(vehicle_4["vehicle"].deep_symbolize_keys),
            pcc_driver: { id: customer_4_id }.merge(customer_4["customer"].deep_symbolize_keys),
          },
        ],
      },
    ]
  end
end
