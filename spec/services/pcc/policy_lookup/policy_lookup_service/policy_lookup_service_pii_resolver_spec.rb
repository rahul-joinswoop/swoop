# frozen_string_literal: true

require 'rails_helper'

describe PCC::PolicyLookup::PolicyLookupService::PolicyLookupServicePiiResolver do
  let(:response) { { "pii" => ["http://example.com/pii/1", "http://example.com/pii/2"] } }

  let(:vehicle) do
    {
      "3483935107f16de640c3274d86913c81" => {
        "vehicle" => {
          "uuid" => "3483935107f16de640c3274d86913c81",
          "vin" => "5LMTJ3DH710484384",
          "license_number" => nil,
          "license_issuer" => nil,
          "make" => "LINCOLN",
          "model" => "MKC",
          "year" => 2019,
          "color" => nil,
          "purchase_date" => "2018-12-01",
        },
      },
    }
  end

  let(:customer) do
    {
      "3f196696a3da9fa05df6e292942d8a19" => {
        "customer" => {
          "uuid" => "3f196696a3da9fa05df6e292942d8a19",
          "name" => "DELORAS FERRY",
          "street_address" => "26155 KIHN UNION",
          "city" => "PUNTA GORDA",
          "region" => "FL",
          "postal_code" => "33980",
          "country_code" => "US",
          "phone_number" => "+15559549406",
          "business" => false,
        },
      },
    }
  end

  it "returns the merged content of all PII URLs" do
    stub_request(:get, "http://example.com/pii/1").to_return(body: vehicle.to_json)
    stub_request(:get, "http://example.com/pii/2").to_return(body: customer.to_json)
    context = PCC::PolicyLookup::PolicyLookupService::PolicyLookupServicePiiResolver.call(input: { job_id: 123, response: response, client_code: 'lincoln' })
    expect(context.output[:job_id]).to eq 123
    expect(context.output[:policy_search_response]).to eq response
    expect(context.output[:pii_response]).to eq vehicle.merge(customer)
  end
end
