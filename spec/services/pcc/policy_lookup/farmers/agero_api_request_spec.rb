# frozen_string_literal: true

require "rails_helper"

describe PCC::PolicyLookup::Farmers::AgeroApiRequest, :vcr do
  subject(:policy_lookup_call) { described_class.call(input: { policy_number: "0103293256", job_id: 123 }) }

  it 'returns one policy from AgeroAPI' do
    results = policy_lookup_call.output[:response].results

    expect(results.size).to eq 1
  end

  it 'returns an OpenStruct with expected attr methods' do
    policy = policy_lookup_call.output[:response].results.first

    expect(policy.respond_to?(:policyType)).to be_truthy
    expect(policy.respond_to?(:policyNumber)).to be_truthy
    expect(policy.respond_to?(:policyOwner)).to be_truthy
    expect(policy.respond_to?(:policyStateCode)).to be_truthy
    expect(policy.respond_to?(:coverageSystemSource)).to be_truthy

    policy_owner = policy.policyOwner

    expect(policy_owner.respond_to?(:firstName)).to be_truthy
    expect(policy_owner.respond_to?(:middleName)).to be_truthy
    expect(policy_owner.respond_to?(:lastName)).to be_truthy
    expect(policy_owner.respond_to?(:contactInfo)).to be_truthy

    contact_info = policy_owner.contactInfo

    expect(contact_info.respond_to?(:address)).to be_truthy
    expect(contact_info.respond_to?(:phoneNumber)).to be_truthy

    address = contact_info.address

    expect(address.respond_to?(:zip)).to be_truthy
    expect(address.respond_to?(:state)).to be_truthy
    expect(address.respond_to?(:street)).to be_truthy
    expect(address.respond_to?(:city)).to be_truthy
    expect(address.respond_to?(:addressType)).to be_truthy
  end

  context 'when policy_number is unexistent' do
    subject(:policy_lookup_call) { described_class.call(input: { policy_number: "123456789", job_id: 123 }) }

    it 'returns no policies from AgeroAPI' do
      results = policy_lookup_call.output[:response].results

      expect(results.size).to eq 0
    end
  end

  context 'when no policy_number is passed as input' do
    subject(:policy_lookup_call) { described_class.call }

    it 'throws ArgumentError' do
      expect { policy_lookup_call }.to raise_error(ArgumentError)
    end
  end
end
