# frozen_string_literal: true

require "rails_helper"

# PCC Service instantiates internal classes based on the FleetCompany that's using the system.
# The pilot is "Farmers Insurance", so we're using it for the specs.
describe PCC::PCCService do
  shared_examples 'triggers PolicyLookup Worker accordingly' do |expected_worker_namespace|
    describe ".policy_lookup" do
      subject(:policy_lookup_call) { described_class.policy_lookup(job_id: job.id, api_user: api_user, search_terms: {}) }

      let(:job) { create(:job, fleet_company: fleet_company) }
      let(:api_user) { create(:user, company: fleet_company) }

      it "calls the correct worker" do
        worker_const = "PCC::PolicyLookup::#{expected_worker_namespace}::Worker".constantize
        allow(worker_const).to receive(:perform_async)

        expect(worker_const).to receive(:perform_async)

        policy_lookup_call
      end
    end
  end

  shared_examples 'triggers CoverageDetails Worker accordingly' do |expected_worker_namespace|
    describe ".coverage_details" do
      subject(:coverage_details_call) do
        described_class.coverage_details(
          policy_id: pcc_policy.id,
          pcc_vehicle_id: pcc_vehicle.id,
          pcc_driver_id: pcc_driver.id,
          api_user: api_user
        )
      end

      let(:pcc_policy) { create(:pcc_policy, company: fleet_company, job: job) }
      let(:pcc_vehicle) { create(:pcc_vehicle) }
      let(:pcc_driver) { create(:pcc_user) }
      let(:job) { create(:job, fleet_company: fleet_company) }
      let(:api_user) { create(:user, company: fleet_company) }

      it "calls the correct worker" do
        worker_const = "PCC::CoverageDetails::#{expected_worker_namespace}::Worker".constantize
        allow(worker_const).to receive(:perform_async)

        expect(worker_const).to receive(:perform_async)

        coverage_details_call
      end
    end
  end

  shared_examples 'triggers ClaimCreation Worker accordingly' do |expected_worker_namespace|
    describe ".claim_creation" do
      subject(:claim_creation_call) { described_class.claim_creation(job: job) }

      let(:job) { create(:job, fleet_company: fleet_company, pcc_coverage: create(:pcc_coverage)) }

      it "calls the correct worker" do
        worker_const = "PCC::ClaimCreation::#{expected_worker_namespace}::Worker".constantize
        allow(worker_const).to receive(:perform_async)

        expect(worker_const).to receive(:perform_async)

        claim_creation_call
      end

      context 'when the PCC::Coverage is not covered' do
        let(:job) { create(:job, fleet_company: fleet_company, pcc_coverage: create(:pcc_coverage, covered: false)) }

        it "doesnt call worker" do
          worker_const = "PCC::ClaimCreation::#{expected_worker_namespace}::Worker".constantize

          expect(worker_const).not_to receive(:perform_async)

          claim_creation_call
        end
      end

      context 'when the PCC::Coverage has exceeded its limits' do
        let(:job) { create(:job, fleet_company: fleet_company, pcc_coverage: create(:pcc_coverage, exceeded_service_count_limit: true)) }

        it "doesnt call the worker" do
          worker_const = "PCC::ClaimCreation::#{expected_worker_namespace}::Worker".constantize

          expect(worker_const).not_to receive(:perform_async)

          claim_creation_call
        end
      end

      context 'when job service is Winch Out' do
        it "calls PCC::ClaimCreation::Farmers::Worker accordingly" do
          worker_const = "PCC::ClaimCreation::Farmers::Worker".constantize

          expect(worker_const).to receive(:perform_async)

          claim_creation_call
        end

        context "when #{Question::WITHIN_10_FEET_OF_PAVED_SURFACE.inspect} answer is No" do
          let(:job) do
            job = create(:job, fleet_company: fleet_company, pcc_coverage: create(:pcc_coverage), service_code: create(:service_code, name: ServiceCode::WINCH_OUT))

            question = create(:question, question: Question::WITHIN_10_FEET_OF_PAVED_SURFACE)
            answer_for_job = create(:answer, question: question, answer: 'No')

            job.question_results << QuestionResult.create(
              job: job,
              question: answer_for_job.question,
              answer: answer_for_job
            )

            job
          end

          it "doesnt call PCC::ClaimCreation::Farmers::Worker" do
            expect(PCC::ClaimCreation::Farmers::Worker).not_to receive(:perform_async)

            claim_creation_call
          end
        end
      end
    end
  end

  context 'when job.fleet_company is Farmers Insurance' do
    let(:fleet_company) { create(:fleet_company, :farmers) }

    context 'and it is correctly setup through company settings for Farmers' do
      let(:pcc_service_name) { 'Farmers' }

      before do
        fleet_company.settings << Setting.where(key: Setting::PCC_SERVICE_NAME, value: pcc_service_name).first_or_create!
      end

      it_behaves_like 'triggers PolicyLookup Worker accordingly', 'Farmers'
      it_behaves_like 'triggers CoverageDetails Worker accordingly', 'Farmers'
      it_behaves_like 'triggers ClaimCreation Worker accordingly', 'Farmers'
    end
  end

  context 'when job.fleet_company is 21st Century Insurance' do
    let(:fleet_company) { create(:fleet_company, :century) }

    # 21st Century also uses Farmers endpoint
    context 'and it is correctly setup through company settings for Farmers' do
      let(:pcc_service_name) { 'Farmers' }

      before do
        fleet_company.settings << Setting.where(key: Setting::PCC_SERVICE_NAME, value: pcc_service_name).first_or_create!
      end

      it_behaves_like 'triggers PolicyLookup Worker accordingly', 'Farmers'
      it_behaves_like 'triggers CoverageDetails Worker accordingly', 'Farmers'
      it_behaves_like 'triggers ClaimCreation Worker accordingly', 'Farmers'
    end
  end

  context 'when job.fleet_company is Lincoln' do
    let(:fleet_company) { create(:fleet_company, :lincoln) }

    # Lincoln uses Swoop Policy Lookup service
    context 'and it is correctly setup through company settings for Policy Lookup service' do
      let(:pcc_service_name) { 'PolicyLookupService' }

      before do
        fleet_company.settings << Setting.where(key: Setting::PCC_SERVICE_NAME, value: pcc_service_name).first_or_create!
      end

      it_behaves_like 'triggers PolicyLookup Worker accordingly', 'PolicyLookupService'
      it_behaves_like 'triggers CoverageDetails Worker accordingly', 'PolicyLookupService'
    end
  end

  context 'when job.fleet_company is Enterprise Fleet Management' do
    let(:fleet_company) { create(:fleet_company, :enterprise_fleet_management) }

    # Enterprise Fleet Management uses Swoop Policy Lookup service
    context 'and it is correctly setup through company settings for Policy Lookup System' do
      let(:pcc_service_name) { 'PolicyLookupService' }

      before do
        fleet_company.settings << Setting.where(key: Setting::PCC_SERVICE_NAME, value: pcc_service_name).first_or_create!
      end

      it_behaves_like 'triggers PolicyLookup Worker accordingly', 'PolicyLookupService'
      it_behaves_like 'triggers CoverageDetails Worker accordingly', 'PolicyLookupService'
    end
  end

  context 'when job.fleet_company does not have Policy Lookup System setting setup' do
    let(:fleet_company) { create(:fleet_company, name: 'Any Fleet Company') }

    describe '.policy_lookup' do
      describe ".policy_lookup" do
        subject(:policy_lookup_call) { described_class.policy_lookup(job_id: job.id, api_user: api_user, search_terms: {}) }

        let(:job) { create(:job, fleet_company: fleet_company) }
        let(:api_user) { create(:user, company: fleet_company) }

        it_behaves_like 'triggers PolicyLookup Worker accordingly', 'PolicyLookupService'
      end
    end

    describe '.coverage_details' do
      describe ".coverage_details" do
        subject(:coverage_details_call) do
          PCC::PCCService.coverage_details(
            policy_id: pcc_policy.id,
            pcc_vehicle_id: pcc_vehicle.id,
            pcc_driver_id: pcc_driver.id,
            api_user: api_user
          )
        end

        let(:pcc_policy) { create(:pcc_policy, company: fleet_company, job: job) }
        let(:pcc_vehicle) { create(:pcc_vehicle) }
        let(:pcc_driver) { create(:pcc_user) }
        let(:job) { create(:job, fleet_company: fleet_company) }
        let(:api_user) { create(:user, company: fleet_company) }

        it_behaves_like 'triggers CoverageDetails Worker accordingly', 'PolicyLookupService'
      end
    end
  end
end
