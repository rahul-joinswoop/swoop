# frozen_string_literal: true

require "rails_helper"

describe PCC::NewClientSetup::AddClientCodeSetting do
  subject(:add_client_code_setting) do
    PCC::NewClientSetup::AddClientCodeSetting.call(
      company: company, policy_lookup_client_settings: policy_lookup_client_settings
    )
  end

  let(:company) { create(:fleet_managed_company) }

  context 'when Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE is present' do
    let(:policy_lookup_client_settings) do
      { Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE => 'pls_client_code' }
    end

    context 'when company does not have POLICY_LOOKUP_SERVICE_CLIENT_CODE setting yet' do
      it 'adds it to the company' do
        expect { add_client_code_setting }.to change(company.settings, :count).from(0).to(1)
      end

      it 'sets the value correctly' do
        expect(add_client_code_setting.company.settings.first.value).to eq 'pls_client_code'
      end
    end

    context 'when company already has POLICY_LOOKUP_SERVICE_CLIENT_CODE setting' do
      before do
        company.settings << Setting.new(key: Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE, value: 'old_pls_client_code')
      end

      it 'does not add a new setting to the company' do
        expect { add_client_code_setting }.not_to change(company.settings, :count)
      end

      it 'updates the value correctly' do
        expect(add_client_code_setting.company.settings.first.value).to eq 'pls_client_code'
      end
    end
  end

  context 'when Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE is nil' do
    let(:policy_lookup_client_settings) do
      { Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE => nil }
    end

    it 'flags the context.fail' do
      expect(add_client_code_setting.message).to eq "Can't set 'Policy Lookup Service client code' because its value is blank"
    end

    it 'does not add a new setting to the company' do
      expect { add_client_code_setting }.not_to change(company.settings, :count)
    end
  end
end
