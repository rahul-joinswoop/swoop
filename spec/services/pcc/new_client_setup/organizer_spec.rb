# frozen_string_literal: true

require "rails_helper"

describe PCC::NewClientSetup::Organizer do
  it 'organizes the expected interactors' do
    expect(PCC::NewClientSetup::Organizer.organized).to eq [
      PCC::NewClientSetup::AddClientCodeSetting,
      PCC::NewClientSetup::AddPolicyLookupTypesSetting,
      PCC::NewClientSetup::AddRequiredFieldsForCoverageSetting,
    ]
  end
end
