# frozen_string_literal: true

require "rails_helper"

describe PCC::NewClientSetup::AddPolicyLookupTypesSetting do
  subject(:add_client_code_setting) do
    PCC::NewClientSetup::AddPolicyLookupTypesSetting.call(
      company: company, policy_lookup_client_settings: policy_lookup_client_settings
    )
  end

  let(:company) { create(:fleet_managed_company) }

  context 'when Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES is present' do
    let(:policy_lookup_client_settings) do
      { Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => [PCC::Policy::MANUAL, PCC::Policy::VIN] }
    end

    context 'when company does not have POLICY_LOOKUP_SERVICE_LOOKUP_TYPES setting yet' do
      it 'adds it to the company' do
        expect { add_client_code_setting }.to change(company.settings, :count).from(0).to(1)
      end

      it 'sets the value correctly' do
        expect(JSON.parse(add_client_code_setting.company.settings.first.value)).to eq [PCC::Policy::MANUAL, PCC::Policy::VIN]
      end
    end

    context 'when company already has POLICY_LOOKUP_SERVICE_LOOKUP_TYPES setting' do
      before do
        company.settings << Setting.new(key: Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES, value: [PCC::Policy::MANUAL])
      end

      it 'does not add a new setting to the company' do
        expect { add_client_code_setting }.not_to change(company.settings, :count)
      end

      it 'updates the value correctly' do
        expect(JSON.parse(add_client_code_setting.company.settings.first.value)).to eq [PCC::Policy::MANUAL, PCC::Policy::VIN]
      end
    end
  end

  context 'when Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES is nil' do
    let(:policy_lookup_client_settings) do
      { Setting::POLICY_LOOKUP_SERVICE_LOOKUP_TYPES => nil }
    end

    it 'flags the context.fail' do
      expect(add_client_code_setting.message).to eq "Can't set 'Policy Lookup Service lookup types' because its value is blank"
    end

    it 'does not add a new setting to the company' do
      expect { add_client_code_setting }.not_to change(company.settings, :count)
    end
  end
end
