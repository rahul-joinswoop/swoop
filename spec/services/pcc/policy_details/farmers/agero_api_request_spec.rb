# frozen_string_literal: true

require "rails_helper"

describe PCC::PolicyDetails::Farmers::AgeroApiRequest, :vcr do
  subject(:policy_details_call) do
    described_class.call(input: { policy_id: policy.id })
  end

  let(:policy) { create :pcc_policy }

  it 'returns one policy from AgeroAPI' do
    policy = policy_details_call.output[:response].policy

    expect(policy.policyNumber).to eq "0103293256"
  end

  it 'returns an OpenStruct with expected attr methods' do
    expect(policy_details_call.output[:policy_id]).not_to be_nil

    policy_details_response = policy_details_call.output[:response]

    expect(policy_details_response.respond_to?(:transactionId)).to be_truthy
    expect(policy_details_response.respond_to?(:policy)).to be_truthy

    policy = policy_details_response.policy

    expect(policy.respond_to?(:agent)).to be_truthy
    expect(policy.respond_to?(:drivers)).to be_truthy
    expect(policy.respond_to?(:vehicles)).to be_truthy
    expect(policy.respond_to?(:policyNumber)).to be_truthy
    expect(policy.respond_to?(:policyType)).to be_truthy
    expect(policy.respond_to?(:policyStateCode)).to be_truthy
    expect(policy.respond_to?(:coverageSystemSource)).to be_truthy
    expect(policy.respond_to?(:companyCode)).to be_truthy
    expect(policy.respond_to?(:policyHolder)).to be_truthy

    policy_holder = policy.policyHolder

    expect(policy_holder.respond_to?(:firstName)).to be_truthy
    expect(policy_holder.respond_to?(:middleName)).to be_truthy
    expect(policy_holder.respond_to?(:lastName)).to be_truthy
    expect(policy_holder.respond_to?(:contactInfo)).to be_truthy

    contact_info = policy_holder.contactInfo

    expect(contact_info.respond_to?(:address)).to be_truthy
    expect(contact_info.respond_to?(:phoneNumber)).to be_truthy
    expect(contact_info.respond_to?(:faxNumber)).to be_truthy

    address = contact_info.address

    expect(address.respond_to?(:zip)).to be_truthy
    expect(address.respond_to?(:state)).to be_truthy
    expect(address.respond_to?(:street)).to be_truthy
    expect(address.respond_to?(:city)).to be_truthy

    driver = policy.drivers.first

    expect(driver.respond_to?(:firstName)).to be_truthy
    expect(driver.respond_to?(:lastName)).to be_truthy
    expect(driver.respond_to?(:relation)).to be_truthy

    vehicle = policy.vehicles.first

    expect(vehicle.respond_to?(:policyUnit)).to be_truthy
    expect(vehicle.respond_to?(:year)).to be_truthy
    expect(vehicle.respond_to?(:make)).to be_truthy
    expect(vehicle.respond_to?(:model)).to be_truthy
    expect(vehicle.respond_to?(:vin)).to be_truthy
    expect(vehicle.respond_to?(:propertyType)).to be_truthy

    agent = policy.agent

    expect(agent.respond_to?(:agentCode)).to be_truthy
    expect(agent.respond_to?(:firstName)).to be_truthy
    expect(agent.respond_to?(:lastName)).to be_truthy
    expect(agent.respond_to?(:stateCode)).to be_truthy
    expect(agent.respond_to?(:districtCode)).to be_truthy
    expect(agent.respond_to?(:contactInfo)).to be_truthy

    agent_contact_info = agent.contactInfo

    expect(agent_contact_info.respond_to?(:address)).to be_truthy
    expect(agent_contact_info.respond_to?(:phoneNumber)).to be_truthy
    expect(agent_contact_info.respond_to?(:faxNumber)).to be_truthy

    agent_contact_info_address = agent_contact_info.address

    expect(agent_contact_info_address.respond_to?(:zip)).to be_truthy
    expect(agent_contact_info_address.respond_to?(:state)).to be_truthy
    expect(agent_contact_info_address.respond_to?(:street)).to be_truthy
    expect(agent_contact_info_address.respond_to?(:city)).to be_truthy
  end

  skip do
    context 'when policy_number doesnt exist on AgeroAPI' do
      subject(:policy_details_call) do
        described_class.call(input: { policy_id: policy.id })
      end

      let(:policy) { create :pcc_policy, policy_number: "0123456789" }

      it 'does something' do
        expect { policy_details_call }.not_to raise_error
      end
    end
  end

  context 'when no policy_id is passed as input' do
    subject(:policy_details_call) do
      described_class.call(input: {})
    end

    it 'throws ArgumentError' do
      expect { policy_details_call }.to raise_error(ArgumentError)
    end
  end
end
