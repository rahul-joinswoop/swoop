# frozen_string_literal: true

require "rails_helper"

describe PCC::ClaimCreation::Farmers::CreateClaimJson do
  describe ".call" do
    subject(:create_json) { described_class.call(input: { job_id: pcc_policy.job_id }) }

    let(:pcc_policy) { create(:pcc_policy) }

    it "outputs a :create_json key" do
      create_json_output = create_json.output

      expect(create_json_output[:claim_json]).not_to be_nil
    end

    it "fills caseNumber with x's and job_id, 13 chars in total" do
      create_json_output = create_json.output

      expect(create_json_output[:claim_json][:caseNumber]).to eq(("%13.13s" % pcc_policy.job_id).gsub(' ', 'x'))
    end

    it "fills purchaseOrderNumber with x's and job_id, 9 chars in total" do
      create_json_output = create_json.output

      expect(create_json_output[:claim_json][:purchaseOrderNumber]).to eq(("%9.9s" % pcc_policy.job_id).gsub(' ', 'x'))
    end
  end

  describe "#disablement_reason_for_symtom" do
    it 'maps the symptom correctly' do
      expect(described_class.new.send(:disablement_reason_for_symtom, "Accident")).to(
        eq("Accident")
      )

      expect(described_class.new.send(:disablement_reason_for_symtom, "Flat tire")).to(
        eq("Flat Tire")
      )

      expect(described_class.new.send(:disablement_reason_for_symtom, "Inoperable Problem")).to(
        eq("Inoperable Problem")
      )

      expect(described_class.new.send(:disablement_reason_for_symtom, "Locked out")).to(
        eq("Lockout")
      )

      expect(described_class.new.send(:disablement_reason_for_symtom, "Out of fuel")).to(
        eq("Out of Fuel")
      )

      expect(described_class.new.send(:disablement_reason_for_symtom, "Specialty Vehicle/RV Disablement")).to(
        eq("Specialty Vehicle/RV Disablement")
      )

      expect(described_class.new.send(:disablement_reason_for_symtom, "Stuck")).to(
        eq("Stuck")
      )

      expect(described_class.new.send(:disablement_reason_for_symtom, "Would not Start")).to(
        eq("Would not Start")
      )

      expect(described_class.new.send(:disablement_reason_for_symtom, "any other thing not mapped")).to(
        eq("Stuck")
      )
    end
  end
end
