# frozen_string_literal: true

require 'rails_helper'

describe PCC::ClaimCreation::Farmers::AgeroApiRequest do
  let(:claim) { { id: 100, reason: "because I said so" } }

  it "posts to the Agero API to create the claim" do
    stub_request(:post, "#{AgeroApi::SERVICE_URL}/claims")
      .with(body: claim.to_json)
      .to_return(status: 201, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"status": "success"}')

    service = PCC::ClaimCreation::Farmers::AgeroApiRequest.call(input: { job_id: 100, claim_json: claim })
    expect(service.output[:job_id]).to eq 100
    expect(service.output[:response]).to eq OpenStruct.new("status" => "success")
  end

  it "raises an error if the API response is not 201 Created" do
    stub_request(:post, "#{AgeroApi::SERVICE_URL}/claims")
      .with(body: claim.to_json)
      .to_return(status: 403)

    expect do
      PCC::ClaimCreation::Farmers::AgeroApiRequest.call(input: { job_id: 100, claim_json: claim })
    end.to raise_error(PCC::ClaimCreation::Error)
  end
end
