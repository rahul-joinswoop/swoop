# frozen_string_literal: true

require 'rails_helper'

describe PCC::CoverageDetails::PolicyLookupService::ResultSorter do
  subject(:worker_perform) do
    PCC::CoverageDetails::PolicyLookupService::ResultSorter.call({
      input: {
        job: job,
        program_results: program_results,
      },
    })
  end

  let(:lincoln) { create(:fleet_company, :lincoln) }

  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: lincoln,
    }
  end

  context 'Covered > NotCovered' do
    let(:program_results) do
      [
        {
          id: 1,
          result: 'NotCovered',
          rank: 1,
        },
        {
          id: 2,
          result: 'Covered',
          rank: 2,
        },
      ]
    end

    it 'return covererd' do
      res = subject.output
      expect(res[:program_results].map { |x| x[:id] }).to eql([2, 1])
    end
  end

  context 'Covered > Failed' do
    context 'first failed, second covered' do
      let(:program_results) do
        [
          {
            id: 1,
            result: 'Failed',
            rank: 1,
          },
          {
            id: 2,
            result: 'Covered',
            rank: 2,
          },
        ]
      end

      it 'return covererd' do
        res = subject.output
        expect(res[:program_results].map { |x| x[:id] }).to eql([1, 2])
      end
    end
  end

  context 'first covered, second failed' do
    let(:program_results) do
      [
        {
          id: 1,
          result: 'Covered',
          rank: 1,
        },
        {
          id: 2,
          result: 'Failed',
          rank: 2,
        },
      ]
    end

    it 'return covererd' do
      res = subject.output
      expect(res[:program_results].map { |x| x[:id] }).to eql([1, 2])
    end
  end

  context 'results same, use rank' do
    context 'in order' do
      let(:program_results) do
        [
          {
            id: 1,
            result: 'Covered',
            rank: 1,
          },
          {
            id: 2,
            result: 'Covered',
            rank: 2,
          },
        ]
      end

      it 'return covererd' do
        res = subject.output
        expect(res[:program_results].map { |x| x[:id] }).to eql([1, 2])
      end
    end

    context 'out of order' do
      let(:program_results) do
        [
          {
            id: 1,
            result: 'Covered',
            rank: 2,
          },
          {
            id: 2,
            result: 'Covered',
            rank: 1,
          },
        ]
      end

      it 'return covererd' do
        res = subject.output
        expect(res[:program_results].map { |x| x[:id] }).to eql([2, 1])
      end
    end
  end
end
