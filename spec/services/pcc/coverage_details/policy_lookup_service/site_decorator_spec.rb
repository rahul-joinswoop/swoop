# frozen_string_literal: true

require 'rails_helper'

describe PCC::CoverageDetails::PolicyLookupService::SiteDecorator do
  subject(:worker_perform) do
    PCC::CoverageDetails::PolicyLookupService::SiteDecorator.call({
      input: {
        job: job,
        request_args: request_args,
        coverage_args: {
          dropoff: {},
        },
      },
    })
  end

  let(:lincoln) { create(:fleet_company, :lincoln) }

  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: lincoln,
    }
  end

  let!(:advance_auto_site) do
    create :fleet_site, :advance_auto_site, {
      company: lincoln,
    }
  end

  let!(:fell_street_site) do
    create :fleet_site, :fell_street_site, {
      company: lincoln,
    }
  end

  let!(:sunrise_auto_site) do
    create :fleet_site, :sunrise_auto_site, {
      company: lincoln,
    }
  end

  let!(:dr_auto_site) do
    create :fleet_site, :dr_auto_site, {
      company: lincoln,
    }
  end

  context 'no service args' do
    let(:request_args) do
      {
        "job" => {
        },
      }
    end

    it 'does not find site' do
      res = subject.output
      expect(res[:coverage_args][:dropoff][:nearest]).to be_nil
    end
  end

  context 'service in middle of nowhere' do
    let(:request_args) do
      {
        "job" => {
          "location" => {
            "service_location" => { "lat" => 34.5, "lng" => -90.5 },
          },
        },
      }
    end

    it 'does not find site' do
      res = subject.output
      expect(res[:coverage_args][:dropoff][:nearest]).to be_nil
    end
  end

  context 'service location and dealerships exist' do
    let(:request_args) do
      {
        "job" => {
          "location" => {
            "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA
          },
        },
      }
    end

    it 'finds closest site', vcr: true do
      res = subject.output
      expect(res[:coverage_args][:dropoff][:nearest]).to be_nil
    end
  end

  context 'service location and dealerships and dropoff exist' do
    context 'closest' do
      let(:request_args) do
        {
          "job" => {
            "location" => {
              "service_location" => { "lat" => 37.7830241, "lng" => -122.4249327 },
              "dropoff_location" => { "site_id" => fell_street_site.id },
            },
          },
        }
      end

      it 'sets dropoff.nearest true', vcr: true do
        res = subject.output
        expect(res[:coverage_args][:dropoff][:nearest]).to eq(true)
      end
    end

    context 'not closest' do
      let(:request_args) do
        {
          "job" => {
            "location" => {
              "service_location" => { "lat" => 37.7830241, "lng" => -122.4249327 },
              "dropoff_location" => { "site_id" => advance_auto_site.id },
            },
          },
        }
      end

      it 'sets dropoff.nearest false', vcr: true do
        res = subject.output
        expect(res[:coverage_args][:dropoff][:nearest]).to eq(false)
      end
    end
  end
end
