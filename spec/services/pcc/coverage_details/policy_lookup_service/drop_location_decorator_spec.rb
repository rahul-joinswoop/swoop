# frozen_string_literal: true

require 'rails_helper'

describe PCC::CoverageDetails::PolicyLookupService::DropLocationDecorator do
  subject(:worker_perform) do
    PCC::CoverageDetails::PolicyLookupService::DropLocationDecorator.call({
      input: {
        job: job,
        policy: nil,
        request_args: request_args,
        coverage_args: {
          vehicle: {},
          job: {},
          dealer: {},
          dropoff: {},
        },
      },
    })
  end

  let(:lincoln) { create(:fleet_company, :lincoln) }

  let!(:advance_auto_repair) { create :location, :advance_auto_repair }

  let!(:advance_auto_site) do
    create :fleet_site, {
      name: "Advance Auto Repair",
      company: lincoln,
      location: advance_auto_repair,
    }
  end

  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: lincoln,
    }
  end

  context 'has neither service location nor dropoff location' do
    let(:request_args) do
      {
        "job" => {
        },
      }
    end

    it 'does not set dropoff' do
      res = subject.output
      dropoff = res[:coverage_args][:dropoff]
      expect(dropoff).to eq({})
    end
  end

  context 'has service location' do
    let(:request_args) do
      {
        "job" => {
          "location" => {
            "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA
          },
        },
      }
    end

    it 'does not set drop location', vcr: true do
      res = subject.output
      dropoff = res[:coverage_args][:dropoff]
      expect(dropoff).to eq({})
    end
  end

  context 'has dropoff location only' do
    context 'site' do
      let(:request_args) do
        {
          "job" => {
            "location" => {
              "dropoff_location" => { "site_id" => advance_auto_site.id },
            },
          },
        }
      end

      it 'fills out drop location without distance', vcr: true do
        res = subject.output
        dropoff = res[:coverage_args][:dropoff]
        expect(dropoff).not_to be_nil
        expect(dropoff[:type]).to eq(advance_auto_site.location.location_type.name)
      end
    end

    context 'lat,lng' do
      let(:request_args) do
        {
          "job" => {
            "location" => {
              "dropoff_location" => { "lat" => 37.762277, "lng" => -122.4170051 },
            },
          },
        }
      end

      it 'fills out drop location without distance', vcr: true do
        res = subject.output
        dropoff = res[:coverage_args][:dropoff]
        expect(dropoff).to eq({})
      end
    end
  end

  context 'has service location and dropoff location' do
    let(:request_args) do
      {
        "job" => {
          "location" => {
            "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA
            "dropoff_location" => { "site_id" => advance_auto_site.id },
          },
        },
      }
    end

    it 'fills out drop location entirely', vcr: true do
      res = subject.output
      dropoff = res[:coverage_args][:dropoff]
      expect(dropoff).not_to be_nil
      expect(dropoff[:type]).to eq(advance_auto_site.location.location_type.name)
      expect(dropoff[:distance]).to eq(86.8)
    end
  end
end
