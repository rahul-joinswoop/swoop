# frozen_string_literal: true

require 'rails_helper'

describe PCC::CoverageDetails::PolicyLookupService::Organizer do
  subject(:service_call) do
    PCC::CoverageDetails::PolicyLookupService::Organizer.call({
      input: {
        request_args: {},
        job: job,
        force_client_api_coverage_override: force_client_api_coverage_override,
      },
    })
  end

  let(:force_client_api_coverage_override) { false }
  let(:fleet_company) { create(:fleet_company, :lincoln) }
  let(:job) { create :fleet_managed_job, fleet_company: fleet_company }

  context 'when @force_client_api_coverage_override is set to true' do
    let(:force_client_api_coverage_override) { true }

    it 'results in the Client API Covered program' do
      output = service_call.output
      program_results = output[:program_results][0]

      expect(program_results[:result]).to eq ClientProgram::COVERED
      expect(program_results[:name]).to eq ClientProgram::CLIENT_API_COVERED
      expect(program_results[:code]).to eq ClientProgram::CLIENT_API_COVERED_CODE
      expect(program_results[:coverage_notes]).to eq ClientProgram::CLIENT_API_COVERED_NOTES
    end
  end
end
