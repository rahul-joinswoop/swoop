# frozen_string_literal: true

require 'rails_helper'

describe PCC::CoverageDetails::PolicyLookupService::ProcessCoverageResult do
  subject(:service_call) do
    PCC::CoverageDetails::PolicyLookupService::ProcessCoverageResult.call({
      input: {
        job: job,
        policy: policy,
        request_args: { job: { service: { name: job_service&.name } } },
        coverage_args: {
          vehicle: {},
          job: {},
          dealer: {},
          dropoff: {},
        },
        force_client_api_coverage_override: force_client_api_coverage_override,
      },
    })
  end

  let(:fleet_company) { create(:fleet_company, :lincoln) }
  let(:job) { create :fleet_managed_job, fleet_company: fleet_company, status: job_status }
  let(:job_status) { 'created' }
  let(:job_service) { service_code_tow }
  let(:service_code_tow) { create(:service_code, name: ServiceCode::TOW) }
  let(:policy) { nil }
  let(:force_client_api_coverage_override) { false }

  let!(:manual_entry) do
    create(
      :client_program,
      name: 'Manual Entry',
      coverage_custom_issues: coverage_custom_issues,
      company: fleet_company,
      coverage_filename: 'not_covered',
      coverage_notes: 'Default Notes for Manual Entry',
    )
  end

  let!(:lincoln_for_life) do
    create(
      :client_program,
      name: 'lincoln_for_life',
      code: 'lincoln_for_life',
      coverage_custom_issues: coverage_custom_issues,
      company: fleet_company,
      coverage_filename: 'covered',
      coverage_notes: 'Default notes for this Lincoln for Life program',
    )
  end

  let!(:lincoln_warranty) do
    create(
      :client_program,
      name: 'lincoln_warranty',
      code: 'lincoln_warranty',
      coverage_custom_issues: coverage_custom_issues,
      company: fleet_company,
      coverage_filename: 'covered',
      coverage_notes: 'Default notes for this Lincoln Warranty program',
    )
  end

  let(:coverage_custom_issues) { nil }

  shared_examples 'select the default coverage notes' do
    it 'selects the default coverage notes' do
      coverage_notes_result = service_call.output[:program_results].first[:coverage_notes]

      expect(coverage_notes_result).to eq 'Default notes for this Lincoln for Life program'
    end
  end

  context 'when a policy is found' do
    let(:policy) { create(:pcc_policy, :lincoln, job: job) }

    context 'when there is no custom coverage notes for the job.service' do
      it_behaves_like 'select the default coverage notes'

      context 'when job.service is nil' do
        let(:job_service) { nil }

        it_behaves_like 'select the default coverage notes'
      end
    end

    context 'when there is a coverage notes customized for the job.service' do
      let!(:lincoln_for_life_coverage_notes_for_tow_service) do
        create(
          :client_program_service_notes,
          client_program: lincoln_for_life,
          service_code: service_code_tow,
          notes: 'Customized Tow notes for Lincoln for Life program',
        )
      end

      it 'selects the default coverage notes' do
        coverage_notes_result = service_call.output[:program_results].first[:coverage_notes]

        expect(coverage_notes_result).to eq 'Customized Tow notes for Lincoln for Life program'
      end
    end
  end

  context 'when policy is nil' do
    let(:policy) { nil }

    context 'and there is a "Manual Entry" client_program set to the FleetCompany' do
      let(:client_program) { manual_entry }

      context 'and coverage_custom_issues is set on the Manual Entry client_program' do
        let(:coverage_custom_issues) { ['Policy Lookup Failed'] }

        it 'appends the custom issue in the coverage issues array' do
          context = service_call

          expect(context.output[:program_results].first[:coverage_issues]).to eq(
            ['Policy Lookup Failed']
          )
        end
      end
    end
  end

  context 'when @force_client_api_coverage_override is set to true' do
    let(:force_client_api_coverage_override) { true }

    it 'results in the Client API Covered program' do
      output = service_call.output
      program_results = output[:program_results][0]

      expect(program_results[:result]).to eq ClientProgram::COVERED
      expect(program_results[:name]).to eq ClientProgram::CLIENT_API_COVERED
      expect(program_results[:code]).to eq ClientProgram::CLIENT_API_COVERED_CODE
      expect(program_results[:coverage_notes]).to eq ClientProgram::CLIENT_API_COVERED_NOTES
    end
  end
end
