# frozen_string_literal: true

require "rails_helper"

describe PCC::CoverageDetails::Farmers::AgeroApiRequest, :vcr do
  subject(:coverage_details_call) do
    described_class.call(input: {
      policy_id: policy.id,
      pcc_vehicle_id: pcc_vehicle.id,
      pcc_driver_id: pcc_driver.id,
    })
  end

  let(:policy) { create :pcc_policy }
  let(:pcc_vehicle) { create :pcc_vehicle }
  let(:pcc_driver) { create :pcc_user }

  before :each do
    allow(PCC::Utils).to receive(:id_with_13_chars).and_return("xxxxxxxxxxxx1")
  end

  it 'returns an OpenStruct with expected attr methods' do
    coverage_details = coverage_details_call.output

    expect(coverage_details[:response].respond_to?(:coverageReferenceNumber)).to be_truthy
    expect(coverage_details[:response].respond_to?(:coverageStatus)).to be_truthy
    expect(coverage_details[:response].respond_to?(:coverageLimit)).to be_truthy
    expect(coverage_details[:response].respond_to?(:coverageDescription)).to be_truthy
    expect(coverage_details[:response].respond_to?(:exceededFuelServiceCountLimit)).to be_truthy
    expect(coverage_details[:response].respond_to?(:exceededRoadsideServiceCountLimit)).to be_truthy
  end

  context 'when policy_id is unexistent' do
    subject(:coverage_details_call) do
      described_class.call(input: {
        vehicle_id: pcc_vehicle.id,
      })
    end

    let(:pcc_vehicle) { create :pcc_vehicle }

    it 'throws ArgumentError' do
      expect { coverage_details_call }.to raise_error(ArgumentError)
    end
  end

  context 'when vehicle_id is unexistent' do
    subject(:coverage_details_call) do
      described_class.call(input: {
        policy_id: policy.id,
      })
    end

    let(:policy) { create :pcc_policy }

    it 'throws ArgumentError' do
      expect { coverage_details_call }.to raise_error(ArgumentError)
    end
  end
end
