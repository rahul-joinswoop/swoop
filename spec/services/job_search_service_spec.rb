# frozen_string_literal: true

require "rails_helper"

describe JobSearchService do
  describe "company filter" do
    it 'does not filter attributes for super companies' do
      service = JobSearchService.new(company: create(:super_company), invoice_search: true)
      query = service.query(search_terms: { term: "1" })
      match_fields = query[:bool][:should].map { |q| q[:match].keys.first if q[:match] }
      restricted_fields = [
        :partner_notes,
        :dispatch_notes,
        :driver_notes,
        :"account.name",
        :"fleet_company.name",
        :swoop_notes,
        :"rescue_company.name",
        :fleet_notes,
      ]
      restricted_fields.each do |field|
        expect(match_fields).to include(field)
      end
    end

    it "searches all fleet managed jobs for super companies" do
      service = JobSearchService.new(company: create(:super_company))
      query = service.query(search_terms: { term: "1" })
      expect(query[:bool][:filter]).to eq [
        {
          term: {
            type: "fleetmanagedjob",
          },
        },
      ]
    end

    it "removes non-fleet visible fields for fleet companies" do
      service = JobSearchService.new(company: create(:fleet_company), invoice_search: true)
      query = service.query(search_terms: { term: "1" })
      match_fields = query[:bool][:should].map { |q| q[:match].keys.first if q[:match] }
      restricted_fields = [
        :dispatch_notes,
        :driver_notes,
        :"account.name",
        :"fleet_company.name",
      ]
      restricted_fields.each do |field|
        expect(match_fields).not_to include(field)
      end
    end

    it "searches only company managed jobs for fleet companies" do
      company = create(:fleet_company)
      service = JobSearchService.new(company: company)
      query = service.query(search_terms: { term: "1" })
      expect(query[:bool][:filter]).to eq [{ term: { "fleet_company.id": company.id } }]
    end

    it "removes non-parter visible fields for partner companies" do
      service = JobSearchService.new(company: create(:rescue_company), invoice_search: true)
      query = service.query(search_terms: { term: "1" })
      match_fields = query[:bool][:should].map { |q| q[:match].keys.first if q[:match] }
      restricted_fields = [
        :"rescue_company.name",
        :fleet_notes,
      ]
      restricted_fields.each do |field|
        expect(match_fields).not_to include(field)
      end
    end

    it "searches only partner company jobs for partner companies" do
      company = create(:rescue_company)
      service = JobSearchService.new(company: company)
      query = service.query(search_terms: { term: "1" })
      expect(query[:bool][:filter]).to eq [{ term: { "rescue_company.id": company.id } }]
    end
  end

  describe "invoice filter" do
    let(:company) { create(:super_company) }

    it "searches for all invoiced jobs" do
      service = JobSearchService.new(company: company, invoice_search: true)
      query = service.query(search_terms: { term: "Foo bar" })
      expect(query[:bool][:filter]).to eq [
        {
          terms: {
            status: ["canceled", "completed", "released", "goa"],
          },
        },
      ]
    end

    it "searches for all invoiced jobs in a date range" do
      service = JobSearchService.new(company: company, invoice_search: true)
      query = service.query(search_terms: { term: "Foo bar" }, filters: {
        job_after: "2018-05-10",
        job_before: "2018-05-11",
      })

      expect(query[:bool][:filter]).to eq [
        { range: { dispatched_at: {
          gte: Date.new(2018, 5, 10).beginning_of_day,
          lte: Date.new(2018, 5, 11).end_of_day,
        } } },
        { terms: { status: ["canceled", "completed", "released", "goa"] } },
      ]
    end
  end

  describe "status filter" do
    it "searches for all active jobs" do
      service = JobSearchService.new(company: create(:super_company))
      query = service.query(search_terms: { term: "Foo bar" }, filters: { active: "1" })
      expect(query[:bool][:filter]).to eq [
        {
          term: {
            type: "fleetmanagedjob",
          },
        },
        {
          terms: {
            status: ["draft", "pending", "unassigned", "assigned", "accepted", "submitted", "dispatched", "en route", "on site", "towing", "tow destination", "auto assigning"],
          },
        },
      ]
    end
  end
end
