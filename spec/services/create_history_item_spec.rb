# frozen_string_literal: true

require "rails_helper"

describe CreateHistoryItem do
  subject do
    described_class.call(job: job, target: target, event: event)
  end

  let(:job) { create(:job) }
  let(:target) { create(:audit_job_status) }
  let(:event) { 'Canceled' }

  context "when no history item exists for event" do
    it "creates a new history item" do
      expect { subject }.to change(HistoryItem, :count).by(1)
    end
  end

  context "when a history item exists for an edited event" do
    let!(:history_item) do
      create(:history_item,
             job: job,
             title: HistoryItem::DROP_OFF_LOCATION_EDITED,
             target: target,
             adjusted_created_at: 5.days.ago)
    end

    it "creates a new history item" do
      expect { subject }.to change(HistoryItem, :count).by(1)
    end
  end

  context "when a history item exists for event" do
    let!(:history_item) do
      create(:history_item,
             job: job,
             title: 'Canceled',
             target: target,
             adjusted_created_at: 5.days.ago)
    end

    it "updates the adjusted_created_at date" do
      subject

      expect { history_item.reload }.to change(
        history_item, :adjusted_created_at
      )
    end
  end

  context "when both user sources are nil" do
    let(:job) { create(:job, last_touched_by: nil) }

    it "creates a new history item" do
      expect { subject }.to change(HistoryItem, :count).by(1)
    end
  end
end
