# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService do
  describe '.transition_for' do
    subject(:invoice_service_transition_for_event) { InvoiceService.transition_for event }

    context 'when swoop_undo_approval_partner' do
      let(:event) { 'swoop_undo_approval_partner' }

      it { is_expected.to eq InvoiceService::SwoopUndoApprovalPartner }
    end

    context 'when swoop_undo_approval_partner' do
      let(:event) { 'swoop_approved_partner' }

      it { is_expected.to eq InvoiceService::SwoopApprovedPartner }
    end

    context 'when invalid' do
      let(:event) { 'not_existent' }

      it 'raises ServiceTransitionNotFoundError' do
        expect { invoice_service_transition_for_event }.to raise_error InvoiceService::ServiceTransitionNotFoundError
      end

      context 'when blank' do
        let(:event) { '  ' }

        it 'raises ArgumentError' do
          expect { invoice_service_transition_for_event }.to raise_error ArgumentError
        end
      end
    end
  end
end
