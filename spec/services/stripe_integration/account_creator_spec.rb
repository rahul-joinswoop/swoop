# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::AccountCreator, vcr: true do
  describe "#create" do
    subject(:create_account) do
      StripeIntegration::AccountCreator.new(account_attributes).create
    end

    let(:rescue_company) { create(:rescue_company, name: 'For Stripe Account') }

    let(:account_attributes) do
      {
        address_city: 'San Francisco',
        address_line1: 'Pasetta Drive',
        address_postal_code: '90506',
        address_state: 'California',
        business_name: 'Test',
        business_tax_id: 123123,
        dob_day: 16,
        dob_month: 12,
        dob_year: 1981,
        first_name: 'Partner Owner',
        last_name: 'Surname',
        personal_address_city: 'San Francisco',
        personal_address_line1: 'Pasetta Drive',
        personal_address_postal_code: '90506',
        personal_address_state: 'California',
        request_ip: '127.0.0.1',
        ssn_last_4: "0012",
        api_company: rescue_company,
      }
    end

    let!(:company_invoice_charge_fee) do
      create(
        :company_invoice_charge_fee,
        company: rescue_company
      )
    end

    it 'successfully creates the Account on Stripe end' do
      account = create_account

      expect(account.id).to be_present
      expect(account.object).to eq 'account'
      expect(account.business_name).to eq 'Test'
      expect(account.charges_enabled).to eq true
      expect(account.country).to eq 'US'
      expect(account.payout_schedule.delay_days).to eq 7
      expect(account.payout_schedule.interval).to eq 'weekly'
      expect(account.type).to eq 'custom'
    end
  end
end
