# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::PayoutScheduleUpdater, vcr: true do
  describe ".call" do
    subject(:service_call) do
      StripeIntegration::PayoutScheduleUpdater.call(
        input: {
          custom_account_id: custom_account_id,
          payout_interval: payout_interval,
        }
      )
    end

    let(:rescue_company) { create(:rescue_company) }
    let!(:custom_account) do
      create(
        :custom_account,
        company: rescue_company,
        upstream_account_id: 'acct_1CcDwkKJ3aQumLV5'
      )
    end
    let(:custom_account_id) { custom_account.id }

    context 'when it is set to daily' do
      let(:payout_interval) { 'daily' }

      it 'updates the custom_account on Stripe side' do
        custom_account_output = service_call.output

        expect(custom_account_output.payout_schedule.interval).to eq 'daily'
      end
    end

    context 'when it is set to weekly' do
      let(:payout_interval) { 'weekly' }

      it 'updates the custom_account on Stripe side' do
        custom_account_output = service_call.output

        expect(custom_account_output.payout_schedule.interval).to eq 'weekly'
        expect(custom_account_output.payout_schedule.weekly_anchor).to eq 'friday'
      end
    end

    context 'when it is set to monthly' do
      let(:payout_interval) { 'monthly' }

      it 'updates the custom_account on Stripe side' do
        custom_account_output = service_call.output

        expect(custom_account_output.payout_schedule.interval).to eq 'monthly'
        expect(custom_account_output.payout_schedule.monthly_anchor).to eq 1
      end
    end

    context 'when custom_account_id is blank' do
      let(:custom_account_id) { nil }
      let(:payout_interval) { 'weekly' }

      it 'raises ArgumentError' do
        expect { service_call }.to raise_error(ArgumentError)
      end
    end

    context 'when payout_interval is blank' do
      let(:payout_interval) { nil }

      it 'raises ArgumentError' do
        expect { service_call }.to raise_error(ArgumentError)
      end
    end
  end
end
