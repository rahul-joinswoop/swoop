# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::EventHandlers::PayoutPaid, vcr: true do
  subject(:handler_instance) do
    StripeIntegration::EventHandlers::PayoutPaid.new(
      { event: event }
    )
  end

  context 'when event is payout.paid' do
    let(:event) do
      event_hash = {
        id: "evt_1ChUI7JNvxn5tz0F6oOOzxEn",
        object: "event",
        account: "acct_1ArBIyJNvxn5tz0F",
        api_version: "2018-05-21",
        created: 1530069959,
        data: {
          object: {
            id: "po_1ChUI6JNvxn5tz0FbRXlWXzy",
            object: "payout",
            amount: 9895,
            balance_transaction: "txn_1ChUI6JNvxn5tz0FMSYZEPVR",
            created: 1530069958,
            livemode: false,
            source_type: "card",
            type: "bank_account",
          },
        },
        livemode: false,
        type: "payout.paid",
      }

      JSON.parse(event_hash.to_json, object_class: OpenStruct)
    end

    let!(:charge) do
      create(:charge, invoice: invoice, upstream_charge_id: 'ch_1CeltbAuS6RZ6q5g5jYUSahh')
    end

    let(:invoice) { create(:invoice) }

    it 'updates the charge.upstream_payout_id accordingly' do
      handler_instance.handle

      charge.reload

      expect(charge.upstream_payout_id).to eq 'po_1ChUI6JNvxn5tz0FbRXlWXzy'
    end

    it 'updates the charge.upstream_payout_created_at accordingly' do
      handler_instance.handle

      charge.reload

      expect(charge.upstream_payout_created_at.to_i).to eq 1530069958
    end

    context 'when Stripe throws Stripe::InvalidRequestError' do
      before do
        allow(Stripe::BalanceTransaction).to receive(:list).and_raise(
          Stripe::InvalidRequestError.new('Stripe::InvalidRequestError', 'Stripe::InvalidRequestError')
        )
      end

      it 'does not raises the error' do
        expect { handler_instance.handle }.not_to raise_error
      end
    end
  end
end
