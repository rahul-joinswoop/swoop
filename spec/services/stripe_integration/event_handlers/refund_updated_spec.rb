# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::EventHandlers::RefundUpdated do
  subject(:handler_instance) do
    StripeIntegration::EventHandlers::RefundUpdated.new(
      { event: event }
    )
  end

  # On Refund failure, Stripe sends 'charge.refund.updated' event.
  # So we receive it through RefundUpdated handler, and check if it's a
  # refund failure
  context 'when event is charge.refund.updated with Refund failure' do
    let(:event) do
      event_hash = {
        type: 'charge.refund.updated',
        data: {
          object: {
            created: 1529599290,
            failure_balance_transaction: "txn_1CfVqlJBq4iiGApqpzjR6c5P",
            metadata: {
              job_id: job.id,
            },
          },
        },
      }

      JSON.parse(event_hash.to_json, object_class: OpenStruct)
    end

    let(:job) { create(:rescue_job, rescue_company_id: rescue_company.id) }
    let(:rescue_company) { create(:rescue_company, name: 'Real Tow') }
    let(:email_job_double) { double }

    before do
      allow(Delayed::Job).to receive(:enqueue)
      allow(Stripe::EmailRefundFailed).to receive(:new).and_return(email_job_double)
    end

    it 'triggers email alert sending' do
      expect(Stripe::EmailRefundFailed).to receive(:new).with(
        rescue_company.id,
        job.id,
        1529599290
      ).and_return(nil)

      handler_instance.handle
    end

    it 'schedules async job to send the email' do
      expect(Delayed::Job).to receive(:enqueue).with(email_job_double)

      handler_instance.handle
    end
  end
end
