# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::EventHandlers::AccountUpdated do
  subject(:handler_instance) do
    StripeIntegration::EventHandlers::AccountUpdated.new(
      { event: event }
    )
  end

  let(:custom_account_id) { 1 }

  let(:event) do
    event_hash = {
      type: 'account.updated',
      data: {
        object: {
          id: custom_account_id,
        },
      },
    }

    JSON.parse(event_hash.to_json, object_class: OpenStruct)
  end

  it 'enqueues StripeIntegration::VerificationFieldsSynchronizer' do
    allow(StripeIntegration::VerificationFieldsSynchronizer).to receive(:enqueue)

    expect(StripeIntegration::VerificationFieldsSynchronizer).to(
      receive(:enqueue).with(custom_account_id)
    )

    handler_instance.handle
  end
end
