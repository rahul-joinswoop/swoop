# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::EventHandlers::PaymentCreated, vcr: true do
  subject(:handler_instance) do
    StripeIntegration::EventHandlers::PaymentCreated.new(
      { event: event }
    )
  end

  context 'when event is payment.created' do
    let(:event) do
      event_hash = {
        account: 'acct_1CiOwFKEsAxZwc7Y',
        type: 'payment.created',
        data: {
          object: {
            id: 'py_1CiRq3KEsAxZwc7YH5Tw8LsA',
            object: 'charge',
            source_transfer: 'tr_1CiRq2AuS6RZ6q5gYiOqsWGk',
          },
        },
      }

      JSON.parse(event_hash.to_json, object_class: OpenStruct)
    end

    let!(:charge) do
      create(:charge, invoice: invoice, upstream_charge_id: 'ch_1CiRq2AuS6RZ6q5gdrZRS29S')
    end

    let(:invoice) { create(:invoice) }

    before do
      handler_instance.handle

      charge.reload
    end

    context "when event.data.source_type is card" do
      it 'updates the charge.upstream_payment_id accordingly' do
        expect(charge.upstream_payment_id).to eq 'py_1CiRq3KEsAxZwc7YH5Tw8LsA'
      end
    end
  end
end
