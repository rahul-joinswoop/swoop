# frozen_string_literal: true

require "rails_helper"

# to trigger it on Stripe, complex:
# 1 you must have a custom account setup
# 2 setup an External Account using a specific account/routing number that will cause
#   Payout Failure: https://stripe.com/docs/connect/testing
#   e.g. acc: 000111111116 / routing: 110000000
# 3 to to Stripe Dashboard -> Connect -> Accounts -> select your Custom Account
#   then click on 'View dasboard as' button
# 4 you'll see the Dashboard as the Custom Account, go to Balance -> Payouts -> New
describe StripeIntegration::EventHandlers::PayoutFailed do
  subject(:handler_instance) do
    StripeIntegration::EventHandlers::PayoutFailed.new(
      { event: event }
    )
  end

  let!(:invoice) do
    create(
      :invoice,
      :with_line_items,
      :with_payments,
      sender: rescue_company
    )
  end
  let(:invoice_payment_id) { invoice.payments.first.id }
  let!(:payment_charge) do
    create(
      :charge,
      payment: invoice.payments.first,
      upstream_charge_id: 'ch_1CZeDgJBq4iiGApqzmT0P7D4',
      invoice: invoice,
      status: InvoicePaymentCharge::CHARGE_STATUS_SUCCESSFUL
    )
  end
  let!(:rescue_company) { create(:rescue_company, name: 'Real Tow') }
  let!(:custom_account) { create(:custom_account, company: rescue_company) }

  let(:event) do
    event_hash = {
      type: 'payout.failed',
      account: custom_account.upstream_account_id,
    }

    JSON.parse(event_hash.to_json, object_class: OpenStruct)
  end

  let(:email_job_double) { double }

  before do
    allow(Delayed::Job).to receive(:enqueue)
    allow(EmailStripePayoutFailed).to receive(:new).and_return(email_job_double)
  end

  it 'triggers email alert sending' do
    expect(EmailStripePayoutFailed).to receive(:new).with(invoice.sender.id).and_return(nil)

    handler_instance.handle
  end

  it 'schedules async job to send the email' do
    allow(Delayed::Job).to receive(:enqueue).with(email_job_double)

    handler_instance.handle
  end
end
