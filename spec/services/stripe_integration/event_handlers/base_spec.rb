# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::EventHandlers::Base do
  before(:each) do
    allow(Stripe::Webhook).to receive(:construct_event).and_return(event)
  end

  describe "find_account_handler_by" do
    subject(:find_account_handler_by) do
      StripeIntegration::EventHandlers::Base.find_account_handler_by(
        payload: double,
        sig_header: double,
        webhook_secret: double
      )
    end

    context 'when the event is charge.dispute.created' do
      let(:event) do
        OpenStruct.new(type: 'charge.dispute.created')
      end

      it { is_expected.to be_a StripeIntegration::EventHandlers::ChargeDisputeCreated }
    end

    context 'when the event is charge.refund.updated' do
      let(:event) do
        OpenStruct.new(type: 'charge.refund.updated')
      end

      it { is_expected.to be_a StripeIntegration::EventHandlers::RefundUpdated }
    end

    context 'when the event is not treated' do
      let(:event) do
        OpenStruct.new(type: 'whatever.not_treated')
      end

      it { is_expected.to be_a StripeIntegration::EventHandlers::NotHandled }
    end
  end

  describe "find_connect_handler_by" do
    subject(:find_connect_handler_by) do
      StripeIntegration::EventHandlers::Base.find_connect_handler_by(
        payload: double,
        sig_header: double,
        webhook_secret: double
      )
    end

    context 'when the event is account.updated' do
      let(:event) do
        OpenStruct.new(type: 'account.updated')
      end

      it { is_expected.to be_a StripeIntegration::EventHandlers::AccountUpdated }
    end

    context 'when the event is payout.failed' do
      let(:event) do
        OpenStruct.new(type: 'payout.failed')
      end

      it { is_expected.to be_a StripeIntegration::EventHandlers::PayoutFailed }
    end

    context 'when the event is payment.created' do
      let(:event) do
        OpenStruct.new(type: 'payment.created')
      end

      it { is_expected.to be_a StripeIntegration::EventHandlers::PaymentCreated }
    end

    context 'when the event is not treated' do
      let(:event) do
        OpenStruct.new(type: 'whatever.not_treated')
      end

      it { is_expected.to be_a StripeIntegration::EventHandlers::NotHandled }
    end
  end
end
