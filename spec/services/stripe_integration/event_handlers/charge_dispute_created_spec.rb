# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::EventHandlers::ChargeDisputeCreated do
  subject(:handler_instance) do
    StripeIntegration::EventHandlers::ChargeDisputeCreated.new(
      { event: event }
    )
  end

  let!(:invoice) do
    create(
      :invoice,
      :with_line_items,
      :with_payments,
      sender: rescue_company
    )
  end
  let(:invoice_payment_id) { invoice.payments.first.id }
  let!(:payment_charge) do
    create(
      :charge,
      payment: invoice.payments.first,
      upstream_charge_id: 'ch_1CkB0QJBq4iiGApq3Qqz8gMv',
      invoice: invoice,
      status: InvoicePaymentCharge::CHARGE_STATUS_SUCCESSFUL
    )
  end
  let!(:rescue_company) { create(:rescue_company, name: 'Real Tow') }
  let!(:custom_account) { create(:custom_account, company: rescue_company) }

  let(:event) do
    event_hash = {
      type: 'charge.dispute.created',
      account: custom_account.upstream_account_id,
      data: {
        object: {
          charge: 'ch_1CkB0QJBq4iiGApq3Qqz8gMv',
        },
      },
    }

    JSON.parse(event_hash.to_json, object_class: OpenStruct)
  end

  let(:email_job_double) { double }

  before do
    allow(Delayed::Job).to receive(:enqueue)
    allow(Stripe::EmailChargeDisputeCreated).to receive(:new).and_return(email_job_double)
  end

  it 'triggers email alert sending' do
    expect(Stripe::EmailChargeDisputeCreated).to receive(:new).with(
      payment_charge.id
    ).and_return(nil)

    handler_instance.handle
  end

  it 'schedules async job to send the email' do
    allow(Delayed::Job).to receive(:enqueue).with(email_job_double)

    handler_instance.handle
  end
end
