# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::EventHandlers::NotHandled do
  subject(:handler_instance) do
    StripeIntegration::EventHandlers::NotHandled.new(
      {
        event: OpenStruct.new({
          type: 'whatever',
        }),
      }
    )
  end

  it 'does not throw error' do
    expect { handler_instance.handle }.not_to raise_error
  end
end
