# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::AccountDestroyer, vcr: true do
  subject(:account_destroyer_service_call) do
    StripeIntegration::AccountDestroyer.call(
      input: {
        custom_account: custom_account,
      }
    )
  end

  let(:custom_account) do
    create(:custom_account, upstream_account_id: 'acct_1Cfs87H5Lo9PgCvS')
  end

  it 'deleted the account on Stripe end' do
    expect(account_destroyer_service_call).to be_present

    output = account_destroyer_service_call.output
    expect(output.id).to eq 'acct_1Cfs87H5Lo9PgCvS'
    expect(output.deleted).to eq true
  end
end
