# frozen_string_literal: true

require "rails_helper"

describe StripeIntegration::VerificationFieldsSynchronizer, vcr: true do
  subject(:synchronize) do
    StripeIntegration::VerificationFieldsSynchronizer.new(
      custom_account.upstream_account_id
    ).perform
  end

  context 'when there are fields to be verified' do
    let(:rescue_company) { create(:rescue_company) }
    let(:mail_mock) { double }

    context 'when there are fields to be verified' do
      let(:custom_account) do
        create(
          :custom_account,
          upstream_account_id: 'acct_1CfQkjIu16UyexKa',
          company: rescue_company
        )
      end

      before do
        allow(EmailStripeVerificationFields).to receive(:new).and_return(mail_mock)
        allow(Delayed::Job).to receive(:enqueue)
      end

      it 'instantiates EmailStripeVerificationFields' do
        expect(EmailStripeVerificationFields).to receive(:new).with(custom_account.id)

        synchronize
      end

      it 'enqueues the EmailStripeVerificationFields instance job on Delayed::Job' do
        expect(Delayed::Job).to receive(:enqueue).with(mail_mock)

        synchronize
      end
    end

    context 'when custom_account is deleted' do
      let(:custom_account) do
        create(
          :custom_account,
          upstream_account_id: 'acct_1CfQkjIu16UyexKa',
          company: rescue_company,
          deleted_at: Time.current
        )
      end

      before do
        allow(EmailStripeVerificationFields).to receive(:new).and_return(mail_mock)
      end

      it 'does not instantiate EmailStripeVerificationFields' do
        expect(EmailStripeVerificationFields).not_to receive(:new).with(custom_account.id)

        synchronize
      end

      it 'does not enqueue the EmailStripeVerificationFields instance job on Delayed::Job' do
        expect(Delayed::Job).not_to receive(:enqueue).with(mail_mock)

        synchronize
      end
    end
  end
end
