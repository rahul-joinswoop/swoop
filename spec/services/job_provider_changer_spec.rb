# frozen_string_literal: true

require "rails_helper"

describe JobProviderChanger do
  before do
    ## Need to get some example jobs into seeds or factories.
    ## Get an existing Job Here
    @old_rescue_company = create(:rescue_company)
    @new_rescue_company = create(:rescue_company)

    @fleet_company = create(:fleet_company)
    @driver = create(:drive)
    @job = Job.new(
      fleet_company: @fleet_company,
      driver: @driver,
      rescue_company: @old_rescue_company
    )
    @job.save!
  end

  describe "basic test" do
    it "changes a provider" do
      @provider_changer = JobProviderChanger.new(
        @job,
        @old_rescue_company.id,
        @new_rescue_company.id
      )

      result = @provider_changer.call
      # result => [Account, Job]

      job = result.select { |r| r.is_a?(Job) }
      expect(job).to be_present
    end

    it "raise exception when provide wrong old rescue company" do
      wrong_old_rescue_company = create(:rescue_company)

      @provider_changer = JobProviderChanger.new(
        @job,
        wrong_old_rescue_company.id,
        @new_rescue_company.id
      )

      # precise exception
      expect do
        @provider_changer.call
        # there is exception so nothing is returned
      end.to raise_error(JobProviderChanger::IncorrectOldCompanyError)

      result = @provider_changer.instance_variable_get(:@mutated_objects)
      expect(result).to be_empty
    end
  end
end
