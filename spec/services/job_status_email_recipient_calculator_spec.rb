# frozen_string_literal: true

require 'rails_helper'

describe JobStatusEmailRecipientCalculator do
  let(:rescue_company) { create(:rescue_company, live: true) }
  let(:fleet_company) { create(:fleet_company) }
  let!(:swoop) { create :super_company, :swoop }
  let(:job) { create(:job, rescue_company: rescue_company, fleet_company: fleet_company) }

  describe "reassigned jobs" do
    it "returns only the new rescue company if the job has been reassigned" do
      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::REASSIGNED).execute
      expect(recipients).to match_array [rescue_company]
    end
  end

  describe "company notification" do
    it "includes the rescue company" do
      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::ACCEPTED).execute
      expect(recipients).to match_array [rescue_company]
    end

    it "only includes the rescue company on GOA status" do
      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::GOA).execute
      expect(recipients).to match_array [rescue_company]
    end

    it "include no one on completed jobs" do
      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::COMPLETED).execute
      expect(recipients).to match_array []
    end
  end

  describe "company users" do
    it "includes the rescue company, fleet company, and Swoop users who have elected to receive all email" do
      partner_user_1 = create(:user, company: rescue_company, job_status_email_frequency: 'all')
      partner_user_1.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)

      partner_user_2 = create(:user, company: rescue_company, job_status_email_frequency: 'all')
      partner_user_2.job_status_email_preferences.create!(job_status_id: JobStatus::CANCELED)

      partner_user_3 = create(:user, company: rescue_company, job_status_email_frequency: 'dispatcher')
      partner_user_3.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)

      fleet_user_1 = create(:user, company: fleet_company, job_status_email_frequency: 'all')
      fleet_user_1.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)

      fleet_user_2 = create(:user, company: fleet_company, job_status_email_frequency: 'all')
      fleet_user_2.job_status_email_preferences.create!(job_status_id: JobStatus::CANCELED)

      swoop_user = create(:user, company: rescue_company, job_status_email_frequency: 'all')
      swoop_user.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)

      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::ACCEPTED).execute
      users = recipients.select { |recipient| recipient.is_a?(User) }
      expect(users).to match_array [partner_user_1, fleet_user_1, swoop_user]
    end

    it "only include fleet company users at the specified site" do
      site_1 = create(:site, company: fleet_company)
      site_2 = create(:site, company: fleet_company)
      job.update_attribute(:fleet_site, site_1)

      fleet_user_1 = create(:user, company: fleet_company, job_status_email_frequency: 'all')
      fleet_user_1.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)
      fleet_user_1.sites << site_1

      fleet_user_2 = create(:user, company: fleet_company, job_status_email_frequency: 'all')
      fleet_user_2.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)
      fleet_user_2.sites << site_2

      fleet_user_3 = create(:user, company: fleet_company, job_status_email_frequency: 'all')
      fleet_user_3.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)

      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::ACCEPTED).execute
      users = recipients.select { |recipient| recipient.is_a?(User) }
      expect(users.map(&:id)).to match_array [fleet_user_1.id, fleet_user_3.id]
    end
  end

  describe "dispatcher notification" do
    it "includes the partner dispatcher, fleet dispatcher, and root dispatcher if set to get dispatch emails" do
      partner_dispatcher = create(:user, company: rescue_company, job_status_email_frequency: 'dispatcher')
      partner_dispatcher.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)
      job.update_attribute(:partner_dispatcher, partner_dispatcher)

      fleet_dispatcher = create(:user, company: fleet_company, job_status_email_frequency: 'dispatcher')
      fleet_dispatcher.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)
      job.update_attribute(:fleet_dispatcher, fleet_dispatcher)

      root_dispatcher = create(:user, company: swoop, job_status_email_frequency: 'dispatcher')
      root_dispatcher.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)
      job.update_attribute(:root_dispatcher, root_dispatcher)

      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::ACCEPTED).execute
      users = recipients.select { |recipient| recipient.is_a?(User) }
      expect(users.map(&:id)).to match_array [partner_dispatcher.id, fleet_dispatcher.id, root_dispatcher.id]
    end

    it "includes the partner dispatcher, fleet dispatcher, and Swoop dispatcher if not set to get dispatch emails" do
      partner_dispatcher = create(:user, company: rescue_company, job_status_email_frequency: 'none')
      partner_dispatcher.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)
      job.update_attribute(:partner_dispatcher, partner_dispatcher)

      fleet_dispatcher = create(:user, company: fleet_company, job_status_email_frequency: 'all')
      job.update_attribute(:fleet_dispatcher, fleet_dispatcher)

      root_dispatcher = create(:user, company: swoop, job_status_email_frequency: 'all')
      root_dispatcher.job_status_email_preferences.create!(job_status_id: JobStatus::CANCELED)
      job.update_attribute(:root_dispatcher, root_dispatcher)

      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::ACCEPTED).execute
      users = recipients.select { |recipient| recipient.is_a?(User) }
      expect(users.map(&:id)).to be_empty
    end
  end

  describe "customized dispatcher" do
    it "includes the job dispatcher on job create" do
      dispatcher = create(:user, company: create(:super_company), job_status_email_frequency: 'dispatcher')
      dispatcher.job_status_email_preferences.create!(job_status_id: JobStatus::CREATED)
      job.update_attribute(:user, dispatcher)
      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::CREATED).execute
      users = recipients.select { |recipient| recipient.is_a?(User) }
      expect(users).to match_array [dispatcher]
    end

    it "does not include the job dispatcher on other statuses" do
      dispatcher = create(:user, company: create(:super_company), job_status_email_frequency: 'dispatcher')
      dispatcher.job_status_email_preferences.create!(job_status_id: JobStatus::ACCEPTED)
      job.update_attribute(:user, dispatcher)
      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::ACCEPTED).execute
      users = recipients.select { |recipient| recipient.is_a?(User) }
      expect(users).to be_empty
    end

    it "does not include the job dispatcher if there is none" do
      dispatcher = create(:user, company: create(:super_company), job_status_email_frequency: 'dispatcher')
      dispatcher.job_status_email_preferences.create!(job_status_id: JobStatus::CREATED)
      job.update_attribute(:fleet_dispatcher, dispatcher)
      recipients = JobStatusEmailRecipientCalculator.new(job, JobStatus::CREATED).execute
      users = recipients.select { |recipient| recipient.is_a?(User) }
      expect(users).to match_array [dispatcher]
    end
  end
end
