# frozen_string_literal: true

require "rails_helper"

describe JobGetInvoiceRecipientAndCompanyService do
  context "when there is only Job with Account with FleetCompany" do
    before do
      @job = create(:job, {
        account: create(:account, {
          client_company: create(:fleet_company),
        }),
      })
    end

    it "gets Account and FleetCompany" do
      service = JobGetInvoiceRecipientAndCompanyService.new(@job)
      recipient, recipient_company = service.call
      expect(recipient).to be_an(Account)
      expect(recipient_company).to be_an(FleetCompany)
    end
  end

  context "when @job.account == cash_account" do
    let(:rescue_company) { create(:rescue_company) }
    let(:cash_account) { create(:account, company: @rescue_company, name: Account::CASH_CALL) }
    let(:job) { create(:job, rescue_company: @rescue_company, account: cash_account) }

    it "results in User recipient" do
      service = JobGetInvoiceRecipientAndCompanyService.new(job)
      recipient, recipient_company = service.call

      expect(recipient).to be_present
      expect(recipient).to be_eql(job.customer)
      expect(recipient_company).to be_nil
    end

    context 'when Cash Call account is deleted' do
      it "results in User recipient" do
        cash_account.update!(deleted_at: Time.now)

        service = JobGetInvoiceRecipientAndCompanyService.new(job)
        recipient, recipient_company = service.call

        expect(recipient).to be_present
        expect(recipient).to be_eql(job.customer)
        expect(recipient_company).to be_nil
      end
    end
  end

  skip do
    context "when Job has vehicle_inventory_items" do
      before do
        @job = create(:job)
        @inventory_item = create(:inventory_item, {
          job: @job,
          item: create(:vehicle),
        })
      end

      it "call service" do
        # XXX require more specific condition to test
        # service = JobGetInvoiceRecipientAndCompanyService.new(@job)
        # recipient, recipient_company = service.call
      end
    end
  end
end
