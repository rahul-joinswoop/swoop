# frozen_string_literal: true

require 'rails_helper'

describe 'AnalyticsData::JobStatus' do
  subject(:push_job_status_analytics_data) do
    AnalyticsData::JobStatus.new(
      job: job,
      user_agent_hash: user_agent_hash,
      referer: referer,
      api_user: api_user,
      api_application: api_application,
    ).push_message
  end

  let(:subscription_status) { create(:subscription_status, name: 'Free Trial') }
  let(:account) { create(:account, company: rescue_company, name: 'Account Name') }
  let(:service_code) { create(:service_code, name: ServiceCode::TOW) }
  let(:rescue_company) { create(:rescue_company, subscription_status: subscription_status) }

  let(:user_agent_hash) do
    {
      platform: 'Platform name sent by FE',
      version: 'Version sent by FE',
    }
  end
  let(:referer) { 'http://www.page_before_current_one.com/index.html' }

  let(:job) do
    create(
      :fleet_in_house_job,
      last_touched_by: api_user,
      service_code: service_code,
      account: account,
      status: job_status,
    )
  end

  let(:api_application) { nil }
  let(:api_user) { nil }
  let(:human_status) { job.human_status }

  shared_examples "sending expected message to Analytics" do |company_type, user_role|
    it 'sends Analytics the expected message' do
      if api_user
        expect(Analytics).to receive(:track).with(
          user_id: api_user.to_ssid,
          event: "Job #{human_status}",
          properties: {
            company: api_user.company&.name,
            account: job.account.name,
            job_id: job.id,
            service: job.service_code.name,
            platform: user_agent_hash[:platform],
            version: user_agent_hash[:version],
            user_type: user_role.to_s.capitalize!,
            referer: 'http://www.page_before_current_one.com/index.html',
            userId: api_user.to_ssid,
            company_type: company_type,
            subscriptionStatus: api_user.company.subscription_status.name,
          }
        )
      elsif api_application
        expect(Analytics).to receive(:track).with(
          user_id: company.to_ssid,
          event: "Job #{human_status}",
          properties: {
            company: company&.name,
            account: job.account.name,
            job_id: job.id,
            service: job.service_code.name,
            platform: user_agent_hash[:platform],
            version: user_agent_hash[:version],
            user_type: 'API',
            referer: 'http://www.page_before_current_one.com/index.html',
            userId: company.to_ssid,
            company_type: company_type,
            subscriptionStatus: company.subscription_status.name,
          }
        )
      end

      push_job_status_analytics_data
    end
  end

  shared_examples 'sending Analytics message for different cases' do
    context 'and api_user is from a Partner company' do
      let(:company) { create(:rescue_company, subscription_status: subscription_status) }

      context 'and api_user.roles include Dispatcher only' do
        let(:api_user) { create(:user, :dispatcher, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Partner', :dispatcher
      end

      context 'when api_user.roles include admin' do
        let(:api_user) { create(:user, :admin, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Partner', :admin
      end

      context 'when api_user.roles only includes driver' do
        let(:api_user) { create(:user, :driver, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Partner', :driver
      end

      context 'when api_user.roles includes dispatcher and driver' do
        let(:api_user) { create(:user, :dispatcher, :driver, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Partner', :dispatcher
      end

      context 'when api_application is passed' do
        let(:api_application) do
          double(:api_application, owner_id: company.id, owner_type: 'Company')
        end

        it_behaves_like 'sending expected message to Analytics', 'Partner', 'API'
      end
    end

    context 'and api_user is from a Fleet company' do
      let(:company) { create(:fleet_company, subscription_status: subscription_status) }

      context 'and api_user.roles include Dispatcher only' do
        let(:api_user) { create(:user, :dispatcher, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'FleetCompany', :dispatcher
      end

      context 'when api_user.roles include admin' do
        let(:api_user) { create(:user, :admin, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'FleetCompany', :admin
      end

      context 'when api_application is passed' do
        let(:api_application) do
          double(:api_application, owner_id: company.id, owner_type: 'Company')
        end

        it_behaves_like 'sending expected message to Analytics', 'FleetCompany', 'API'
      end
    end

    context 'and api_user is from a Super company (aka Swoop)' do
      let(:company) { create(:super_company, subscription_status: subscription_status) }

      context 'and api_user.roles include Dispatcher only' do
        let(:api_user) { create(:user, :dispatcher, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Swoop', :dispatcher
      end

      context 'when api_user.roles include admin' do
        let(:api_user) { create(:user, :admin, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Swoop', :admin
      end
    end
  end

  context 'when job.status is Completed' do
    let(:job_status) { 'completed' }

    it_behaves_like 'sending Analytics message for different cases'
  end

  context 'when job.status is On Site' do
    let(:job_status) { 'onsite' }

    it_behaves_like 'sending Analytics message for different cases'
  end

  context 'when job.status is Accepted' do
    let(:job_status) { 'accepted' }

    it_behaves_like 'sending Analytics message for different cases'
  end

  context 'when job.status is Rejected' do
    let(:job_status) { 'rejected' }

    it_behaves_like 'sending Analytics message for different cases'
  end

  context 'when job.status is Dispatched' do
    let(:job_status) { 'dispatched' }

    it_behaves_like 'sending Analytics message for different cases'
  end

  context 'when job.status is En Route' do
    let(:job_status) { 'enroute' }

    it_behaves_like 'sending Analytics message for different cases'
  end

  context 'when job.status is not Completed and not On Site' do
    # these values can be any, should not affect spec
    let(:job_status) { 'canceled' }
    let(:api_user) { create(:user, :dispatcher, company: company, email: 'user@email.com') }
    let(:company) { create(:rescue_company, subscription_status: subscription_status) }

    it 'does not call Analytics.track' do
      expect(Analytics).not_to receive(:track)

      push_job_status_analytics_data
    end
  end

  context 'when api_user and api_application are not provided' do
    let(:job_status) { 'dispatched' }
    let(:api_user) { nil }
    let(:api_application) { nil }

    it 'raises error' do
      expect { push_job_status_analytics_data }.to raise_error(ArgumentError)
    end
  end

  context "with a status specified" do
    subject(:push_job_status_analytics_data) do
      AnalyticsData::JobStatus.new(
        job: job,
        status: human_status,
        user_agent_hash: user_agent_hash,
        referer: referer,
        api_user: api_user,
        api_application: api_application,
      ).push_message
    end

    let(:job_status) { 'auto_assigning' }
    let(:human_status) { 'Accepted' }

    it_behaves_like 'sending Analytics message for different cases'
  end
end
