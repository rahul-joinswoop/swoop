# frozen_string_literal: true

require 'rails_helper'

describe AnalyticsData::LogIn do
  subject do
    AnalyticsData::LogIn.new(
      user_agent: user_agent,
      referer: referer,
      api_user: api_user,
      api_application: api_application
    ).call
  end

  let(:application) { create :oauth_application, owner: company, scopes: :fleet }
  let(:company) { create :fleet_company }
  let(:user) { create :user, company: company }
  let(:user_agent) { "ios Swoop/1.15.33.265 (086e96f7d)" }
  let(:referer) { "http://foo.bar.com/asdf" }
  let(:api_user) { nil }
  let(:api_application) { nil }
  let(:event) { "Log In" }

  context "with an api user" do
    let(:api_user) { user }

    it "works" do
      expect(Analytics)
        .to receive(:track)
        .with({
          user_id: user.to_ssid,
          event: event,
          properties: {
            company: company.name,
            company_type: company.class.name,
            platform: "ios",
            referer: referer,
            subscriptionStatus: "Network Only",
            userId: user.to_ssid,
            user_type: "Dispatcher",
            version: "1.15.33.265",
          },

        })
      subject
    end

    context "without a company" do
      let(:user) { create :user }

      it "works" do
        expect(Analytics)
          .to receive(:track)
          .with({
            user_id: user.to_ssid,
            event: event,
            properties: {
              company: nil,
              company_type: nil,
              platform: "ios",
              referer: referer,
              subscriptionStatus: nil,
              userId: user.to_ssid,
              user_type: "Dispatcher",
              version: "1.15.33.265",
            },
          })
        subject
      end
    end
  end

  context "with an application" do
    let(:api_application) { application }

    it "works" do
      expect(Analytics)
        .to receive(:track)
        .with({
          user_id: company.to_ssid,
          event: event,
          properties: {
            company: company.name,
            company_type: company.class.name,
            platform: "ios",
            referer: referer,
            subscriptionStatus: "Network Only",
            userId: company.to_ssid,
            user_type: "API",
            version: "1.15.33.265",
          },

        })
      subject
    end
  end

  context "with nothing" do
    it "works" do
      expect(Analytics)
        .not_to receive(:track)

      expect { subject }.to raise_error(ArgumentError)
    end
  end
end
