# frozen_string_literal: true

require 'rails_helper'

describe 'AnalyticsData::Identify' do
  subject { AnalyticsData::Identify.new(user_or_company).call }

  let(:company) { create :company }
  let(:user) { create :user, company: company }

  context "with a user" do
    let(:user_or_company) { user }

    it "works" do
      expect(Analytics)
        .to receive(:identify)
        .with({ user_id: user.to_ssid, traits: { company: company.name } })
      subject
    end

    context "without a company" do
      let(:user) { create :user }

      it "works" do
        expect(Analytics)
          .to receive(:identify)
          .with({ user_id: user.to_ssid, traits: { company: nil } })
        subject
      end
    end
  end

  context "with a company" do
    let(:user_or_company) { company }

    it "works" do
      expect(Analytics)
        .to receive(:identify)
        .with({ user_id: company.to_ssid, traits: { company: company.name } })
      subject
    end
  end

  context "with nothing" do
    let(:user_or_company) { nil }

    it "works" do
      expect(Analytics)
        .not_to receive(:identify)

      expect { subject }.to raise_error(ArgumentError)
    end
  end
end
