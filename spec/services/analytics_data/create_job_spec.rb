# frozen_string_literal: true

require 'rails_helper'

describe 'AnalyticsData::CreateJob' do
  subject(:push_message) do
    AnalyticsData::CreateJob.new(
      job: job,
      user_agent_hash: user_agent_hash,
      api_company: company,
    ).push_message
  end

  let(:company) { create(:rescue_company) }

  let(:user_agent_hash) do
    {
      platform: 'Platform name sent by FE',
      version: 'Version sent by FE',
    }
  end

  let(:job) { create(:rescue_job) }

  shared_examples 'push expected message to Analytics' do |platform, version|
    it 'pushes analytics message for create job' do
      expect(Analytics).to receive(:track).with(
        user_id: company.to_ssid,
        event: "Create Job",
        properties: {
          category: 'Jobs',
          platform: platform,
          version: version,
          job_id: job.id,
          company: company.name,
        }
      )

      push_message
    end
  end

  it_behaves_like 'push expected message to Analytics', 'Platform name sent by FE', 'Version sent by FE'
end
