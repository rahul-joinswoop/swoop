# frozen_string_literal: true

require 'rails_helper'

describe AnalyticsData::Storage do
  subject(:push_storage_analytics_data) do
    AnalyticsData::Storage.new(
      job: job,
      user_agent_hash: user_agent_hash,
      referer: referer,
      api_user: api_user,
      api_application: api_application,
    ).push_message
  end

  let(:subscription_status) { create(:subscription_status, name: 'Free Trial') }
  let(:account) { create(:account, company: rescue_company, name: 'Account Name') }
  let(:service_code) { create(:service_code, name: ServiceCode::TOW) }
  let(:rescue_company) { create(:rescue_company, subscription_status: subscription_status) }

  let(:user_agent_hash) do
    {
      swoop_custom_platform: 'Platform name sent by FE',
      platform: 'Platform name sent by FE',
      client_or_swoop_version: 'Version sent by FE',
      version: 'Version sent by FE',
    }
  end
  let(:referer) { 'http://www.page_before_current_one.com/index.html' }

  let(:job) do
    create(
      :fleet_in_house_job,
      last_touched_by: api_user,
      service_code: service_code,
      account: account,
      will_store: will_store,
    )
  end

  let(:api_application) { nil }
  let(:api_user) { nil }

  shared_examples "sending expected message to Analytics" do |company_type, user_role|
    it 'sends Analytics the expected message' do
      if api_user
        expect(Analytics).to receive(:track).with(
          user_id: api_user.to_ssid,
          event: "Storage #{job.will_store ? 'Added' : 'Removed'}",
          properties: {
            company: api_user.company&.name,
            account: job.account.name,
            job_id: job.id,
            service: job.service_code.name,
            platform: user_agent_hash[:swoop_custom_platform],
            version: user_agent_hash[:client_or_swoop_version],
            user_type: user_role.to_s.capitalize!,
            referer: 'http://www.page_before_current_one.com/index.html',
            userId: api_user.to_ssid,
            company_type: company_type,
            subscriptionStatus: api_user.company.subscription_status.name,
          }
        )
      elsif api_application
        expect(Analytics).to receive(:track).with(
          user_id: company.to_ssid,
          event: "Storage #{job.will_store ? 'Added' : 'Removed'}",
          properties: {
            company: company&.name,
            account: job.account.name,
            job_id: job.id,
            service: job.service_code.name,
            platform: user_agent_hash[:swoop_custom_platform],
            version: user_agent_hash[:client_or_swoop_version],
            user_type: 'API',
            referer: 'http://www.page_before_current_one.com/index.html',
            userId: company.to_ssid,
            company_type: company_type,
            subscriptionStatus: company.subscription_status.name,
          }
        )
      end

      push_storage_analytics_data
    end
  end

  shared_examples 'sending Analytics message for different cases' do
    context 'and api_user is from a Partner company' do
      let(:company) { create(:rescue_company, subscription_status: subscription_status) }

      context 'and api_user.roles include Dispatcher only' do
        let(:api_user) { create(:user, :dispatcher, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Partner', :dispatcher
      end

      context 'when api_user.roles include admin' do
        let(:api_user) { create(:user, :admin, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Partner', :admin
      end

      context 'when api_user.roles only includes driver' do
        let(:api_user) { create(:user, :driver, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Partner', :driver
      end

      context 'when api_user.roles includes dispatcher and driver' do
        let(:api_user) { create(:user, :dispatcher, :driver, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Partner', :dispatcher
      end

      context 'when api_application is passed' do
        let(:api_application) do
          double(:api_application, owner_id: company.id, owner_type: 'Company')
        end

        it_behaves_like 'sending expected message to Analytics', 'Partner', 'API'
      end
    end

    context 'and api_user is from a Fleet company' do
      let(:company) { create(:fleet_company, subscription_status: subscription_status) }

      context 'and api_user.roles include Dispatcher only' do
        let(:api_user) { create(:user, :dispatcher, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'FleetCompany', :dispatcher
      end

      context 'when api_user.roles include admin' do
        let(:api_user) { create(:user, :admin, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'FleetCompany', :admin
      end

      context 'when api_application is passed' do
        let(:api_application) do
          double(:api_application, owner_id: company.id, owner_type: 'Company')
        end

        it_behaves_like 'sending expected message to Analytics', 'FleetCompany', 'API'
      end
    end

    context 'and api_user is from a Super company (aka Swoop)' do
      let(:company) { create(:super_company, subscription_status: subscription_status) }

      context 'and api_user.roles include Dispatcher only' do
        let(:api_user) { create(:user, :dispatcher, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Swoop', :dispatcher
      end

      context 'when api_user.roles include admin' do
        let(:api_user) { create(:user, :admin, company: company, email: 'user@email.com') }

        it_behaves_like 'sending expected message to Analytics', 'Swoop', :admin
      end
    end
  end

  context 'when will_store is true' do
    let(:will_store) { true }

    it_behaves_like 'sending Analytics message for different cases'
  end

  context 'when will_store is false' do
    let(:will_store) { false }

    it_behaves_like 'sending Analytics message for different cases'
  end

  context 'when api_user and api_application are not provided' do
    let(:will_store) { false }

    let(:api_user) { nil }
    let(:api_application) { nil }

    it 'raises error' do
      expect { push_storage_analytics_data }.to raise_error(ArgumentError)
    end
  end
end
