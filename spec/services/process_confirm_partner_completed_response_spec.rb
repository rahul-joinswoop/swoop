# frozen_string_literal: true

require "rails_helper"

describe ProcessConfirmPartnerCompletedResponse do
  subject { described_class.new(twilio_response, sms_alert).call }

  include_examples "twilio message parsing"
end
