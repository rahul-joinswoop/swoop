# frozen_string_literal: true

require 'rails_helper'

RSpec.describe WsEventLog do
  describe '.handle_error' do
    let(:error) { double("Error", backtrace: backtrace, inspect: "<my error>") }

    before do
      allow(Rollbar).to receive(:error)
      WsEventLog.handle_error(Class, "command", error)
    end

    context "when we receive a normal error" do
      let(:backtrace) { %w(my backtrace) }

      it "properly handles the backtrace" do
        expect(Rollbar).to have_received(:error).with(error)
      end
    end

    context "when we receive an error without a backtrace" do
      let(:backtrace) { nil }

      it "properly handles the backtrace" do
        expect(Rollbar).to have_received(:error).with(error)
      end
    end
  end
end
