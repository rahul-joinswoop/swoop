# frozen_string_literal: true

require 'rails_helper'

describe InvoiceAutoApprover do
  subject(:context) { described_class.call(invoice: invoice) }

  describe ".call" do
    context "with a valid invoice" do
      let(:invoice) { create :invoice, :auto_approvable }

      it "works" do
        expect(context).to be_a_success
      end
    end

    context "with an invalid invoice" do
      let(:invoice) { create :invoice }

      it "fails" do
        expect(context).to be_a_failure
      end
    end
  end
end
