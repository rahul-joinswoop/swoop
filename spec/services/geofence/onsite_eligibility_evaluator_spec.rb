# frozen_string_literal: true

require "rails_helper"

describe Geofence::OnsiteEligibilityEvaluator do
  let(:rescue_company) { create(:rescue_company) }
  let(:rescue_vehicle) do
    create(
      :rescue_vehicle,
      company: rescue_company,
      lat: 37.786342,
      lng: -122.403410,
      location_updated_at: Time.now,
    )
  end
  let(:location1) { create(:location, lat: 37.779247, lng: -122.414053) }
  let(:location2) { create(:location, lat: 37.780838, lng: -122.412007) }
  let(:location3) { create(:location, lat: 37.782441, lng: -122.409962) }

  let(:job_details) do
    {
      rescue_company: rescue_company,
      status: Job::STATUS_ENROUTE,
      rescue_vehicle: rescue_vehicle,
    }
  end

  let!(:location1_job) { create(:rescue_job, job_details.merge(service_location: location1)) }
  let!(:location2_job) { create(:rescue_job, job_details.merge(service_location: location2)) }
  let!(:location2_job_dispatched) do
    details = job_details.merge(service_location: location2, status: Job::STATUS_DISPATCHED)
    create(:rescue_job, details)
  end
  let!(:location3_job) { create(:rescue_job, job_details.merge(service_location: location3)) }

  let(:site) { create(:partner_site, company: rescue_company) }
  let(:worker_klass) { described_class::DEFAULT_WORKER_KLASS }
  let(:delay) { Geofence::OnsiteEligibilityEvaluator::TIME_THRESHOLD_SECONDS }
  let(:interactor_context) do
    {
      rescue_vehicle: rescue_vehicle,
      jobs: rescue_vehicle.current_jobs.reload,
      redis_client: RedisClient[:default],
    }
  end

  subject { Geofence::OnsiteEligibilityEvaluator }

  before do
    allow(worker_klass).to receive(:schedule)
    allow(worker_klass).to receive(:deschedule)
  end

  def move(location)
    rescue_vehicle.update_columns(lat: location.lat, lng: location.lng)
  end

  # This is easier to reason about if we say that there three locations, location1, location2, and
  # location3. Location1 is in range of location2 but not location3. Location2 is equidistant from
  # location1 and location3.
  #
  #              <---------------- 500 meters ----------->
  #  < Location1 <--- 250 m ---> Location2 <--- 250 m ---> Location3

  context '.call' do
    context 'vehicle at location1' do
      before do
        move(location1)
      end

      it 'schedules jobs in range' do
        expect(worker_klass).to receive(:schedule).with(delay, location1_job.id)
        expect(worker_klass).to receive(:schedule).with(delay, location2_job.id)
        expect(worker_klass).not_to receive(:schedule).with(delay, location3_job.id)
        subject.call interactor_context
      end

      it 'removes jobs from context' do
        returned_context = subject.call interactor_context
        expect(returned_context.jobs).not_to include(location1_job)
        expect(returned_context.jobs).not_to include(location2_job)
        expect(returned_context.jobs).to include(location3_job)
      end

      it 'scheduled dipatched jobs' do
        location2_job.update_attributes!(status: Job::STATUS_DISPATCHED)
        expect(worker_klass).to receive(:schedule).with(delay, location2_job.id)
        subject.call interactor_context
      end

      it 'ignores jobs in ineligible status' do
        location1_job.update_attributes!(status: Job::STATUS_PENDING)
        location2_job.update_attributes!(status: Job::STATUS_ONSITE)
        expect(worker_klass).not_to receive(:schedule).with(delay, location1_job.id)
        expect(worker_klass).not_to receive(:schedule).with(delay, location2_job.id)
        subject.call interactor_context
      end

      context 'schedules job and then move away' do
        before do
          expect(worker_klass).to receive(:schedule).with(delay, location1_job.id)
          expect(worker_klass).to receive(:schedule).with(delay, location2_job.id)

          subject.call interactor_context

          move(location3)
        end

        it 'deschedules jobs out of range' do
          expect(worker_klass).to receive(:deschedule).with(delay, location1_job.id)
          subject.call interactor_context
        end

        it 'keeps scheduling jobs in range' do
          expect(worker_klass).to receive(:schedule).with(delay, location2_job.id)
          subject.call interactor_context
        end

        it 'schedules job entering range' do
          expect(worker_klass).to receive(:schedule).with(delay, location3_job.id)
          subject.call interactor_context
        end
      end
    end

    it 'can handle missing location lat / lng' do
      service_location = interactor_context[:jobs].first.service_location
      service_location.lat, service_location.lng = nil, nil
      return_context = nil
      expect { return_context = subject.call interactor_context }.not_to raise_error
      expect(return_context.success?).to be(true)
    end
  end

  context '.before' do
    it 'must be passed context with redis client' do
      interactor_context[:redis_client] = ''
      expect { subject.call interactor_context }.to raise_error(ArgumentError).with_message(/RedisClient/)
    end

    it 'can handle missing jobs' do
      interactor_context[:jobs] = nil
      return_context = nil
      expect { return_context = subject.call interactor_context }.not_to raise_error
      expect(return_context.success?).to be(false)
      expect(return_context.error).to match(/called without :jobs/)
    end

    it 'can handle missing rescue_vehicle' do
      interactor_context[:rescue_vehicle] = nil
      return_context = nil
      expect { return_context = subject.call interactor_context }.not_to raise_error
      expect(return_context.success?).to be(false)
    end
  end
end
