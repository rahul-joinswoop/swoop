# frozen_string_literal: true

require "rails_helper"

describe Geofence::EligibilityEvaluator do
  let(:rescue_company) { build_stubbed(:rescue_company) }
  let(:rescue_vehicle) { build_stubbed(:rescue_vehicle, company: rescue_company, location_updated_at: Time.now) }
  let(:location1) { build_stubbed(:location, lat: 37.779247, lng: -122.414053) }
  let(:location3) { build_stubbed(:location, lat: 37.782441, lng: -122.409962) }

  let(:job_details) do
    {
      rescue_company: rescue_company,
      service_location: location1,
      drop_location: location3,
      status: Job::STATUS_ONSITE,
      rescue_vehicle: rescue_vehicle,
    }
  end

  let!(:job) { build_stubbed(:rescue_job, job_details) }
  let(:site) { build_stubbed(:partner_site, company: rescue_company) }
  let(:rescue_company) { build_stubbed(:rescue_company) }

  subject { Geofence::EligibilityEvaluator }

  it 'requires a rescue vehicle' do
    return_context = subject.call
    expect(Geofence::OnsiteEligibilityEvaluator).not_to receive(:call)
    expect(return_context.success?).to be false
    expect(return_context.error).to match(/Vehicle is required/)
  end

  it "doesn't explode" do
    expect { subject.call(rescue_vehicle: rescue_vehicle) }.not_to raise_error
  end

  it 'respects max jobs' do
    jobs = build_list(:rescue_job, 3, rescue_vehicle: rescue_vehicle, status: Job::STATUS_DISPATCHED)
    stub_const('Geofence::EligibilityEvaluator::MAX_ACTIVE_JOBS', 2)
    expect(Geofence::OnsiteEligibilityEvaluator).not_to receive(:call)
    return_context = subject.call(rescue_vehicle: rescue_vehicle, jobs: jobs)
    expect(return_context.success?).to be false
    expect(return_context.error).to match(/Aborted/)
  end

  context 'with an En Route job near service location' do
    before do
      job.status = Job::STATUS_ENROUTE
      allow(location1).to receive(:distance_to).with(rescue_vehicle).and_return(20)
    end

    it 'schedules status change' do
      expect(Geofence::OnsiteWorker).to receive(:schedule)
      subject.call(rescue_vehicle: rescue_vehicle, jobs: [job])
    end
  end

  context 'with an On Site job that left service location' do
    before do
      job.status = Job::STATUS_ONSITE
      allow(location1).to receive(:distance_to).with(rescue_vehicle).and_return(1000)
    end

    it 'schedules status change to towing' do
      expect(Geofence::TowingWorker).to receive(:schedule)
      subject.call(rescue_vehicle: rescue_vehicle, jobs: [job])
    end

    context 'and is at drop off location' do
      before do
        allow(location3).to receive(:distance_to).with(rescue_vehicle).and_return(10)
      end

      it 'only schedules Tow Destination' do
        expect(Geofence::TowDestinationWorker).to receive(:schedule)
        expect(Geofence::TowingWorker).not_to receive(:schedule)

        subject.call(rescue_vehicle: rescue_vehicle, jobs: [job])
      end
    end

    context 'with no dropoff' do
      before do
        job.drop_location = nil
      end

      it 'schedules change to completed' do
        expect(Geofence::TowingWorker).not_to receive(:schedule)
        expect(Geofence::CompletedWorker).to receive(:schedule)

        subject.call(rescue_vehicle: rescue_vehicle, jobs: [job])
      end
    end

    context 'with job in Tow Destination away from dropoff' do
      before do
        job.status = Job::STATUS_TOWDESTINATION
        allow(location1).to receive(:distance_to).with(rescue_vehicle).and_return(5000)
        allow(location3).to receive(:distance_to).with(rescue_vehicle).and_return(3000)
      end

      it 'schedules status change' do
        expect(Geofence::CompletedWorker).to receive(:schedule)
        subject.call(rescue_vehicle: rescue_vehicle, jobs: [job])
      end
    end
  end
end
