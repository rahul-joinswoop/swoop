# frozen_string_literal: true

require "rails_helper"

describe Geofence::TowingEligibilityEvaluator do
  let(:rescue_company) { build_stubbed(:rescue_company) }
  let(:rescue_vehicle) do
    build_stubbed(
      :rescue_vehicle,
      company: rescue_company,
      location_updated_at: Time.now,
    )
  end
  let(:location1) { build_stubbed(:location, lat: 37.779247, lng: -122.414053) }
  let(:location3) { build_stubbed(:location, lat: 37.782441, lng: -122.409962) }

  let(:job_details) do
    {
      rescue_company: rescue_company,
      service_location: location1,
      drop_location: location3,
      status: Job::STATUS_ONSITE,
      rescue_vehicle: rescue_vehicle,
    }
  end

  let!(:job) { build_stubbed(:rescue_job, job_details) }
  let(:site) { build_stubbed(:partner_site, company: rescue_company) }
  let(:worker_klass) { described_class::DEFAULT_WORKER_KLASS }
  let(:delay) { Geofence::TowingEligibilityEvaluator::TIME_THRESHOLD_SECONDS }
  let(:interactor_context) do
    {
      rescue_vehicle: rescue_vehicle,
      jobs: [job],
      redis_client: RedisClient[:default],
    }
  end

  subject { described_class }

  def move(location)
    rescue_vehicle.lat = location.lat
    rescue_vehicle.lng = location.lng
  end

  #              <---------------- 500 meters ----------->
  #  < Location1 <--- 250 m ---> Location2 <--- 250 m ---> Location3
  #
  shared_context 'it schedules the job' do
    it 'works' do
      expect(worker_klass).to receive(:schedule).with(delay, job.id)
      expect(worker_klass).not_to receive(:deschedule).with(delay, job.id)
      subject.call interactor_context
    end
  end

  shared_context 'it is not scheduled' do
    it 'remains unscheduled' do
      expect(worker_klass).not_to receive(:schedule).with(delay, job.id)
      subject.call interactor_context
    end
  end

  context 'never scheduled' do
    context 'vehicle at service_location' do
      before { move(location1) }

      it_behaves_like 'it is not scheduled'
    end

    context 'moves away from service location' do
      before { move(location3) }

      it_behaves_like 'it schedules the job'

      context 'without a drop location' do
        before { job.drop_location = nil }

        it_behaves_like 'it is not scheduled'
      end

      context 'service location is invalid' do
        before { job.service_location.lng = nil }

        it_behaves_like 'it is not scheduled'
      end

      Job.statuses.each do |status, human_status|
        next unless Job.active_states.include?(human_status)
        next if status == Job::STATUS_ONSITE

        context "with status of #{human_status}" do
          before { job.status = status }

          it_behaves_like 'it is not scheduled'
        end
      end

      context 'moves back to service location after scheduling' do
        before do
          subject.call interactor_context
          move(location1)
        end

        it_behaves_like 'it is not scheduled'

        it 'receives deschedule for job' do
          expect(worker_klass).to receive(:deschedule).with(delay, job.id)
          subject.call interactor_context
        end
      end
    end
  end

  context '.before' do
    it 'must be passed context with redis client' do
      interactor_context[:redis_client] = ''
      expect { subject.call interactor_context }.to raise_error(ArgumentError).with_message(/RedisClient/)
    end

    it 'can handle missing jobs' do
      interactor_context[:jobs] = nil
      return_context = nil
      expect { return_context = subject.call interactor_context }.not_to raise_error
      expect(return_context.success?).to be(false)
      expect(return_context.error).to match(/called without :jobs/)
    end

    it 'can handle missing rescue_vehicle' do
      interactor_context[:rescue_vehicle] = nil
      return_context = nil
      expect { return_context = subject.call interactor_context }.not_to raise_error
      expect(return_context.success?).to be(false)
    end
  end
end
