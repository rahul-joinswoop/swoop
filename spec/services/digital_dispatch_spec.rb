# frozen_string_literal: true

require 'rails_helper'

describe DigitalDispatch do
  describe '.strategy_for' do
    subject do
      DigitalDispatch.strategy_for(job_digital_dispatcher: job_digital_dispatcher, action: action)
    end

    context 'when job_digital_dispatcher is :issc' do
      let(:job_digital_dispatcher) { :issc }

      context 'and action is :accept' do
        let(:action) { :accept }

        it { is_expected.to eq IsscDispatchResponseAccept }
      end

      context 'and action is :reject' do
        let(:action) { :reject }

        it { is_expected.to eq IsscDispatchResponseReject }
      end

      context 'and action is :change_status' do
        let(:action) { :change_status }

        it { is_expected.to eq IsscDispatchStatus }
      end

      context 'and action is :cancel' do
        let(:action) { :cancel }

        it { is_expected.to eq IsscDispatchStatus }
      end

      context 'and action in not implemented' do
        let(:action) { :can_be_any_not_implemented }

        it 'raises error' do
          expect { subject }.to raise_error(ArgumentError)
        end
      end
    end

    context 'when job_digital_dispatcher is :rsc' do
      let(:job_digital_dispatcher) { :rsc }

      context 'and action is :accept' do
        let(:action) { :accept }

        it { is_expected.to eq Agero::Rsc::AcceptDispatchWorker }
      end

      context 'and action is :reject' do
        let(:action) { :reject }

        it { is_expected.to eq Agero::Rsc::RejectDispatchWorker }
      end

      context 'and action is :change_status' do
        let(:action) { :change_status }

        it { is_expected.to eq Agero::Rsc::ChangeDispatchStatusWorker }
      end

      context 'and action is :cancel' do
        let(:action) { :cancel }

        it { is_expected.to eq Agero::Rsc::CancelDispatchWorker }
      end

      context 'and action in not implemented' do
        let(:action) { :can_be_any_not_implemented }

        it 'raises error' do
          expect { subject }.to raise_error(ArgumentError)
        end
      end
    end
  end
end
