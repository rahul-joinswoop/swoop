# frozen_string_literal: true

require "rails_helper"

describe ServiceCodeServices::FetchAllStandard do
  subject(:service_call) do
    described_class.new(api_company: api_company, addon: addon).call
  end

  context 'when fetching non-addons' do
    let(:addon) { [nil, false] }

    context 'when api_company is a RescueCompany' do
      let(:api_company)   { create :rescue_company, :with_service_codes, :with_addons }
      let(:fleet_company) { create :fleet_company, :with_service_codes, :with_addons }

      let(:account) do
        create :account, company: api_company, client_company: fleet_company
      end

      it 'returns own standard service codes' do
        expect(service_call).to eq(
          ServiceCode.standard_items.where(addon: addon)
          .where.not(name: ServiceCode::STORAGE)
        )
      end

      it 'doesnt return own custom service codes' do
        expect(service_call).not_to include(
          *api_company.service_codes.where.not(is_standard: true)
        )
      end

      it 'returns service codes from accounts' do
        expect(service_call).to include(
          *account.client_company.service_codes.where(is_standard: true, addon: addon)
        )
      end
    end

    context 'when api_company is a FleetCompany' do
      let(:api_company) do
        create :fleet_company, :with_service_codes, :with_addons
      end

      it 'returns own standard service codes' do
        expect(service_call).to eq(
          ServiceCode.standard_items
          .where(addon: addon)
          .where.not(name: ServiceCode::STORAGE)
        )
      end

      it 'doesnt return own custom service codes' do
        expect(service_call).not_to include(
          *api_company.service_codes.where.not(is_standard: true)
        )
      end
    end
  end

  context 'when fetching addons' do
    let(:addon) { true }

    context 'when api_company is a RescueCompany' do
      let(:api_company) do
        create :rescue_company, :with_service_codes, :with_addons
      end
      let(:fleet_company) do
        create :fleet_company, :with_service_codes, :with_addons
      end

      let!(:account) do
        create :account, company: api_company, client_company: fleet_company
      end

      let!(:custom_addon_for_account) do
        create :service_code, name: 'Custom Addon for Account', addon: addon, is_standard: false
      end

      it 'returns own standard addons' do
        expect(service_call).to include(
          *api_company.service_codes.where(addon: addon, is_standard: true)
        )
      end

      it 'doesnt return own custom addons' do
        expect(service_call).not_to include(
          *api_company.service_codes.where(addon: addon, is_standard: false)
        )
      end

      it 'returns addons from accounts' do
        expect(service_call).to include(
          *account.client_company.service_codes
          .where(addon: addon, is_standard: false)
        )
      end
    end

    context 'when api_company is a FleetCompany' do
      let(:api_company) { create :fleet_company, :with_service_codes, :with_addons }

      it 'returns own standard addons' do
        expect(service_call).to include(
          *api_company.service_codes.where(addon: true, is_standard: true)
        )
      end

      it 'doesnt return own custom addons' do
        expect(service_call).not_to include(
          *api_company.service_codes.where(addon: true, is_standard: false)
        )
      end
    end
  end
end
