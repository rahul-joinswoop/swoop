# frozen_string_literal: true

require "rails_helper"

describe ServiceCodeServices::Batch do
  subject(:service_call) do
    described_class.new(
      api_company: api_company,
      service_code_ids: service_code_ids
    ).call
  end

  context 'when api_company is a RescueCompany' do
    let(:api_company)   { create :rescue_company, :with_service_codes }
    let(:fleet_company) { create :fleet_company, :with_service_codes }

    let(:service_code_ids) do
      api_company.service_codes.pluck(:id) + fleet_company.service_codes.pluck(:id)
    end

    let(:account) do
      create :account, company: api_company, client_company: fleet_company
    end

    it 'returns own service codes' do
      partner_service_code = ServiceCode.find(
        api_company.service_codes.first.id
      )

      expect(service_call).to include(partner_service_code)
    end

    it 'returns custom service codes from accounts' do
      service_code_from_account = ServiceCode.find(
        account.client_company.service_codes.first.id
      )

      expect(service_call).to include(service_code_from_account)
    end
  end

  skip do
    context 'when api_company is a FleetCompany' do
      let(:api_company) { create :fleet_company, :with_service_codes }
      let(:service_codes_from_api_company) { api_company.service_codes }
      let(:any_other_service_code) { create :service_code, name: 'Any other Service Code' }

      let(:any_service_code_ids) { [any_other_service_code.id] }

      let(:service_code_ids) do
        api_company.service_codes.pluck(:id) + any_service_code_ids
      end

      it 'returns all service_codes requested' do
        expect(service_call).to include(service_code_ids)
      end
    end
  end

  context 'when api_company is Swoop' do
    let(:api_company) { create :super_company, :swoop }

    let(:service_code_1) { create :service_code, name: 'Service Code 1' }
    let(:service_code_2) { create :service_code, name: 'Service Code 2' }

    let(:service_code_ids) do
      [service_code_1.id, service_code_2.id]
    end

    it 'returns all service_codes requested' do
      expect(service_call).to include(service_code_1, service_code_2)
    end
  end
end
