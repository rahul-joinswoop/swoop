# frozen_string_literal: true

require "rails_helper"

describe ServiceCodeServices::Change do
  context '#call' do
    subject(:service_call) do
      described_class.new(
        api_company: api_company,
        standard_services_ids_to_add: standard_services_ids_to_add,
        services_ids_to_remove: services_ids_to_remove,
        custom_service_names_to_add: custom_service_names_to_add,
        addon: addon
      ).call
    end

    before :each do
      allow(PublishCompanyWorker).to receive(:perform_async)
      allow(UpdateCustomServiceCodesToPartnersWorker).to receive(:perform_async)
    end

    context 'when it\'s a non-addon' do
      let(:addon) { false }

      context 'when api_company is a RescueCompany' do
        let(:api_company) { create :rescue_company, :with_service_codes, :with_addons }

        let(:another_standard_service) do
          create(
            :service_code,
            name: ServiceCode::BATTERY_JUMP,
            is_standard: true,
            addon: false
          )
        end

        let(:standard_services_ids_to_add) do
          [another_standard_service.id]
        end

        let(:current_standard_service) do
          api_company.service_codes.find_by(name: ServiceCode::TOW)
        end

        let(:services_ids_to_remove) do
          [current_standard_service.id]
        end

        let(:custom_service_name) { 'A new custom service for Rescue' }

        let(:custom_service_names_to_add) do
          [{ name: custom_service_name }]
        end

        it 'adds a standard service' do
          service_call

          expect(api_company.service_codes).to include(another_standard_service)
        end

        it 'removes a standard service' do
          service_call

          expect(api_company.service_codes).not_to include(current_standard_service)
        end

        it 'adds a custom service' do
          service_call

          custom_service = ServiceCode.find_by(name: custom_service_name)

          expect(api_company.service_codes).to include(custom_service)
        end

        context '#after_execution' do
          it 'calls PublishCompanyWorker asynchronously' do
            expect(PublishCompanyWorker).to receive(:perform_async).with(api_company.id, "ServiceCodeServices::Change")

            service_call
          end

          it 'doesnt calls UpdateCustomServiceCodesToPartnersWorker asynchronously' do
            expect(UpdateCustomServiceCodesToPartnersWorker).not_to receive(:perform_async)

            service_call
          end
        end
      end

      context 'when api_company is a FleetCompany' do
        let(:api_company) { create :fleet_company, :with_service_codes, :with_addons }
        let(:rescue_company) { create :rescue_company, :with_service_codes, :with_addons }
        let(:create_account) do
          @account_with_fleet ||= create :account, company: rescue_company, client_company: api_company
        end

        let(:another_standard_service) do
          create(
            :service_code,
            name: ServiceCode::BATTERY_JUMP,
            is_standard: true,
            addon: false
          )
        end

        let(:standard_services_ids_to_add) do
          [another_standard_service.id]
        end

        let(:current_standard_service) do
          api_company.service_codes.find_by(name: ServiceCode::TOW)
        end

        let(:services_ids_to_remove) do
          [current_standard_service.id]
        end

        let(:custom_service_name) { 'A new custom service for Fleet' }

        let(:custom_service_names_to_add) do
          [{ name: custom_service_name }]
        end

        it 'adds a standard service' do
          service_call

          expect(api_company.service_codes).to include(another_standard_service)
        end

        it 'removes a service' do
          service_call

          expect(api_company.service_codes).not_to include(current_standard_service)
        end

        it 'adds a custom service to fleet' do
          service_call

          custom_service = ServiceCode.find_by(name: custom_service_name)

          expect(api_company.service_codes).to include(custom_service)
        end

        context '#after_execution' do
          it 'calls PublishCompanyWorker asynchronously' do
            expect(PublishCompanyWorker).to receive(:perform_async).with(api_company.id, "ServiceCodeServices::Change")

            service_call
          end

          it 'calls UpdateCustomServiceCodesToPartnersWorker asynchronously' do
            service = service_call

            expect(UpdateCustomServiceCodesToPartnersWorker).to have_received(:perform_async)
              .with(
                api_company.id,
                service.custom_services_added.map(&:id),
                service.custom_services_removed.map(&:id),
                false
              )
          end
        end
      end

      context 'when api_company is SuperCompany' do
        let(:api_company) { create :super_company, :swoop }

        let(:standard_service) do
          create(
            :service_code,
            name: ServiceCode::TOW,
            is_standard: true,
            addon: false
          )
        end

        let(:standard_services_ids_to_add) do
          [standard_service.id]
        end

        let(:services_ids_to_remove) do
          []
        end

        let(:custom_service_name) { 'A new custom service for Super that will never get added' }

        let(:custom_service_names_to_add) do
          [{ name: custom_service_name }]
        end

        it 'has empty collection of service' do
          expect(api_company.service_codes).to eq([])
        end
      end
    end

    context 'when it\'s an addon' do
      let(:addon) { true }

      context 'when api_company is a RescueCompany' do
        let(:api_company) { create :rescue_company, :with_service_codes, :with_addons }

        let(:another_standard_addon) do
          create(
            :service_code,
            name: ServiceCode::LABOR,
            is_standard: true,
            addon: true
          )
        end

        let(:standard_services_ids_to_add) do
          [another_standard_addon.id]
        end

        let(:current_standard_addon) do
          api_company.service_codes.find_by(name: ServiceCode::ACCIDENT_CLEANUP)
        end

        let(:services_ids_to_remove) do
          [current_standard_addon.id]
        end

        let(:custom_addon_name) { 'A new custom addon for Rescue' }

        let(:custom_service_names_to_add) do
          [{ name: custom_addon_name }]
        end

        it 'adds a standard addon' do
          service_call

          expect(api_company.service_codes).to include(another_standard_addon)
        end

        it 'removes a standard addon' do
          service_call

          expect(api_company.service_codes).not_to include(current_standard_addon)
        end

        it 'adds a custom addon' do
          service_call

          custom_addon = ServiceCode.find_by(name: custom_addon_name)

          expect(api_company.service_codes).to include(custom_addon)
        end

        context '#after_execution' do
          it 'calls PublishCompanyWorker asynchronously' do
            expect(PublishCompanyWorker).to receive(:perform_async).with(api_company.id, "ServiceCodeServices::Change")

            service_call
          end

          it 'doesnt calls UpdateCustomServiceCodesToPartnersWorker asynchronously' do
            expect(UpdateCustomServiceCodesToPartnersWorker).not_to receive(:perform_async)

            service_call
          end
        end
      end

      context 'when api_company is a FleetCompany' do
        let(:api_company) { create :fleet_company, :with_service_codes, :with_addons }
        let(:rescue_company) { create :rescue_company, :with_service_codes, :with_addons }
        let(:create_account) do
          @account_with_fleet ||= create :account, company: rescue_company, client_company: api_company
        end

        let(:another_standard_addon) do
          create(
            :service_code,
            name: ServiceCode::LABOR,
            is_standard: true,
            addon: true
          )
        end

        let(:standard_services_ids_to_add) do
          [another_standard_addon.id]
        end

        let(:current_standard_addon) do
          api_company.service_codes.find_by(name: ServiceCode::ACCIDENT_CLEANUP)
        end

        let(:services_ids_to_remove) do
          [current_standard_addon.id]
        end

        let(:custom_addon_name) { 'A new custom service for Fleet' }

        let(:custom_service_names_to_add) do
          [{ name: custom_addon_name }]
        end

        it 'adds a standard addon' do
          service_call

          expect(api_company.service_codes).to include(another_standard_addon)
        end

        it 'removes a service' do
          service_call

          expect(api_company.service_codes).not_to include(current_standard_addon)
        end

        it 'adds a custom addon to fleet' do
          service_call

          custom_addon = ServiceCode.find_by(name: custom_addon_name)

          expect(api_company.service_codes).to include(custom_addon)
        end

        context '#after_execution' do
          it 'calls PublishCompanyWorker asynchronously' do
            expect(PublishCompanyWorker).to receive(:perform_async).with(api_company.id, "ServiceCodeServices::Change")

            service_call
          end

          it 'calls UpdateCustomServiceCodesToPartnersWorker asynchronously' do
            service = service_call

            expect(UpdateCustomServiceCodesToPartnersWorker).to have_received(:perform_async)
              .with(
                api_company.id,
                service.custom_services_added.map(&:id),
                service.custom_services_removed.map(&:id),
                true
              )
          end
        end
      end

      context 'when api_company is SuperCompany' do
        let(:api_company) do
          swoop_company = create(:super_company, :swoop)
          swoop_company.service_codes << create(
            :service_code,
            name: ServiceCode::ACCIDENT_CLEANUP,
            is_standard: true,
            addon: true
          )

          swoop_company
        end

        let(:rescue_company) { create :rescue_company, :with_service_codes, :with_addons }
        let(:create_account) do
          @account_with_swoop ||= create :account, company: rescue_company, client_company: api_company
        end

        let(:another_standard_addon) do
          create(
            :service_code,
            name: ServiceCode::LABOR,
            is_standard: true,
            addon: true
          )
        end

        let(:standard_services_ids_to_add) do
          [another_standard_addon.id]
        end

        let(:current_standard_addon) do
          api_company.service_codes.find_by(name: ServiceCode::ACCIDENT_CLEANUP)
        end

        let(:services_ids_to_remove) do
          [current_standard_addon.id]
        end

        let(:custom_addon_name) { 'A new custom service for Swoop' }

        let(:custom_service_names_to_add) do
          [{ name: custom_addon_name }]
        end

        it 'adds a standard addon' do
          service_call

          expect(api_company.service_codes).to include(another_standard_addon)
        end

        it 'removes a service' do
          service_call

          expect(api_company.service_codes).not_to include(current_standard_addon)
        end

        it 'adds a custom addon to Swoop' do
          service_call

          custom_addon = ServiceCode.find_by(name: custom_addon_name)

          expect(api_company.service_codes).to include(custom_addon)
        end

        context '#after_execution' do
          it 'calls PublishCompanyWorker asynchronously' do
            expect(PublishCompanyWorker).to receive(:perform_async).with(api_company.id, "ServiceCodeServices::Change")

            service_call
          end

          it 'calls UpdateCustomServiceCodesToPartnersWorker asynchronously' do
            service = service_call

            expect(UpdateCustomServiceCodesToPartnersWorker).to have_received(:perform_async)
              .with(
                api_company.id,
                service.custom_services_added.map(&:id),
                service.custom_services_removed.map(&:id),
                true
              )
          end
        end
      end
    end
  end
end
