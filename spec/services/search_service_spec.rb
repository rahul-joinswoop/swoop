# frozen_string_literal: true

require 'rails_helper'

describe SearchService do
  before do
    stub_class 'SearchService::MockService', SearchService do
      class << self

        def model_class
          Company
        end

        def power_searchable_fields
          [
            [:company_id, :integer, :"company.id"],
            [:username, :string, :username],
            [:phone_number, :phone, :phone_number],
            [:last_name, :string, :last_name],
            [:fullname, :string, [:first_name, :last_name]],
            [:things, :wildcard, :things],
            [:created_at, :date, :created_at],
          ]
        end

        def general_search_skip_fields
          [:last_name]
        end

        def filterable_fields
          [[:job_id, :"job.id"]]
        end

        def default_sort_order
          { sort_name: :asc }
        end

      end
    end
  end

  describe "query" do
    let(:service) { SearchService::MockService.new }

    describe "general searches" do
      it "searcheses all non-deleted records by default" do
        query = service.query({})
        expect(query).to eq({
          bool: { should: { match_all: {} }, filter: [], must_not: [{ exists: { field: :deleted_at } }] },
        })
      end

      it "performs a general search on the :term parameter" do
        query = service.query(search_terms: { term: "Foo bar" })
        expect(query).to eq({
          bool: {
            should: [
              { match: { username: { "query" => "foo bar", "operator" => "and" } } },
              { multi_match: {
                query: "foo bar", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
              } },
              { wildcard: { things: "*foo bar*" } },
            ],
            filter: [],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "formats number fields" do
        query = service.query(search_terms: { term: "456" })
        expect(query).to eq({
          bool: {
            should: [
              { match: { "company.id": 456 } },
              { match: { username: { "query" => "456", "operator" => "and" } } },
              { match: { phone_number: "+456" } },
              { multi_match: {
                query: "456", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
              } },
              { wildcard: { things: "*456*" } },
            ],
            filter: [],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "formats phone fields" do
        query = service.query(search_terms: { term: "708-383-2853" })
        expect(query).to eq({
          bool: {
            should: [
              { match: { username: { "query" => "708-383-2853", "operator" => "and" } } },
              { match: { phone_number: "+17083832853" } },
              { multi_match: {
                query: "708-383-2853", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
              } },
              { wildcard: { things: "*708-383-2853*" } },
            ],
            filter: [],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "formats date fields" do
        query = service.query(search_terms: { term: "05092018" })
        expect(query).to eq({
          bool: {
            should: [
              { match: { "company.id": 5092018 } },
              { match: { username: { "query" => "05092018", "operator" => "and" } } },
              { match: { phone_number: "+05092018" } },
              { multi_match: {
                query: "05092018", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
              } },
              { wildcard: { things: "*05092018*" } },
              { range: {
                created_at: {
                  gte: Date.new(2018, 5, 9).beginning_of_day,
                  lte: Date.new(2018, 5, 9).end_of_day,
                },
              } },
            ],
            filter: [],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "formats date ranges" do
        query = service.query(search_terms: { term: "05092018-05102018" })
        expect(query).to eq({
          bool: {
            should: [
              { match: { username: { "query" => "05092018-05102018", "operator" => "and" } } },
              { match: { phone_number: "+0509201805102018" } },
              { multi_match: {
                query: "05092018-05102018", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
              } },
              { wildcard: { things: "*05092018-05102018*" } },
              { range: {
                created_at: {
                  gte: Date.new(2018, 5, 9).beginning_of_day,
                  lte: Date.new(2018, 5, 10).end_of_day,
                },
              } },
            ],
            filter: [],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "is able to provide a filter" do
        query = service.query(search_terms: { term: "Foo bar" }, filters: { job_id: "123" })
        expect(query).to eq({
          bool: {
            should: [
              { match: { username: { "query" => "foo bar", "operator" => "and" } } },
              { multi_match: {
                query: "foo bar", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
              } },
              { wildcard: { things: "*foo bar*" } },
            ],
            filter: [{ term: { "job.id": "123" } }],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "normalizes string filter values by downcasing them" do
        query = service.query(search_terms: { term: "Foo bar" }, filters: { job_id: [123, "ABC"] })
        expect(query).to eq({
          bool: {
            should: [
              { match: { username: { "query" => "foo bar", "operator" => "and" } } },
              { multi_match: {
                query: "foo bar", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
              } },
              { wildcard: { things: "*foo bar*" } },
            ],
            filter: [{ terms: { "job.id": [123, "abc"] } }],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "adds a must not match filter" do
        query = service.query({
          search_terms: { term: "Foo bar" },
          must_not: { exists: :closed_at, job_id: "123" },
        })
        expect(query).to eq({
          bool: {
            should: [
              { match: { username: { "query" => "foo bar", "operator" => "and" } } },
              { multi_match: {
                query: "foo bar", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
              } },
              { wildcard: { things: "*foo bar*" } },
            ],
            filter: [],
            must_not: [
              { exists: { field: :closed_at } }, { exists: { field: :deleted_at } }, { term: { "job.id": "123" } },
            ],
          },
        })
      end
    end

    describe "power searches" do
      it "performs a power search on the power search parameters" do
        query = service.query(search_terms: { last_name: "Brown", company_id: "456" })
        expect(query).to eq({
          bool: {
            must: [
              { match: { "company.id": 456 } },
              { match: { last_name: { "query" => "brown", "operator" => "and" } } },
            ],
            filter: [],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "is able to provide a filter" do
        query = service.query({
          search_terms: { last_name: "Brown", company_id: "456" },
          filters: { job_id: "123" },
        })
        expect(query).to eq({
          bool: {
            must: [
              { match: { "company.id": 456 } },
              { match: { last_name: { "query" => "brown", "operator" => "and" } } },
            ],
            filter: [{ term: { "job.id": "123" } }],
            must_not: [{ exists: { field: :deleted_at } }],
          },
        })
      end

      it "adds a must not match filter" do
        query = service.query({
          search_terms: { last_name: "Brown", company_id: "456" },
          must_not: { exists: :closed_at, job_id: "123" },
        })
        expect(query).to eq({
          bool: {
            must: [
              { match: { "company.id": 456 } },
              { match: { last_name: { "query" => "brown", "operator" => "and" } } },
            ],
            filter: [],
            must_not: [
              { exists: { field: :closed_at } }, { exists: { field: :deleted_at } }, { term: { "job.id": "123" } },
            ],
          },
        })
      end
    end
  end

  describe "pagination validation" do
    it "defaults the page and page size" do
      service = SearchService.new
      expect(service.page).to eq 1
      expect(service.per_page).to eq 200
    end

    it "allows specifying the page and page size" do
      service = SearchService.new(page: 10, per_page: 15)
      expect(service.page).to eq 10
      expect(service.per_page).to eq 15
    end

    it "does not allow paginating past 10K rows" do
      SearchService.new(page: 200, per_page: 50)
      expect { SearchService.new(page: 201, per_page: 50) }.to raise_error(ArgumentError)
    end

    it "does not allow invalid per page values" do
      expect(SearchService.new(per_page: 0).per_page).to eq 1
      expect(SearchService.new(per_page: 10000).per_page).to eq 200
    end
  end

  describe "pagination" do
    it "paginates with a from/size if page number is given" do
      service = SearchService::MockService.new(page: 3, per_page: 20)
      expect(service).to receive(:perform_query).with(
        {
          from: 40,
          size: 20,
          min_score: 0.0001,
          query: {
            bool: {
              should: [
                { match: { username: { "query" => "foo", "operator" => "and" } } },
                { multi_match: {
                  query: "foo", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
                } },
                { wildcard: { things: "*foo*" } },
              ],
              filter: [],
              must_not: [{ exists: { field: :deleted_at } }],
            },
          },
          sort: [{ sort_name: :asc }, { id: :asc }],
        }
      )
      service.search(search_terms: { term: "foo" })
    end

    it "paginates with a search_after if after_id is given" do
      company = create(:company)
      service = SearchService::MockService.new(after_id: company.id, per_page: 20)
      expect(service).to receive(:perform_query).with(
        {
          size: 20,
          min_score: 0.0001,
          query: {
            bool: {
              should: [
                { match: { username: { "query" => "foo", "operator" => "and" } } },
                { multi_match: {
                  query: "foo", type: :cross_fields, fields: [:first_name, :last_name], operator: "and",
                } },
                { wildcard: { things: "*foo*" } },
              ],
              filter: [],
              must_not: [{ exists: { field: :deleted_at } }],
            },
          },
          search_after: [company.sort_name, company.id],
          sort: [{ sort_name: :asc }, { id: :asc }],
        }
      )
      service.search(search_terms: { term: "foo" })
    end
  end
end
