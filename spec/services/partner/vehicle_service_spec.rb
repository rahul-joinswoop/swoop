# frozen_string_literal: true

require "rails_helper"

describe Partner::VehicleService do
  describe "#update_driver" do
    let(:vehicle) { create(:rescue_vehicle) }
    let(:api_user) { create(:user) }

    it "keeps the current driver assigned" do
      driver = create(:user)
      vehicle.update!(driver: driver)

      service = Partner::VehicleService.new(api_user: api_user, vehicle: vehicle, vehicle_params: {})
      service.call

      expect(vehicle.driver.id).to eq(driver.id)
    end

    it "don't assigns current logged-in user as driver" do
      service = Partner::VehicleService.new(api_user: api_user, vehicle: vehicle, vehicle_params: {})
      service.call

      expect(vehicle.driver).to be_nil
    end

    it "reset the driver from the vehicle" do
      vehicle.update!(driver: api_user)
      service = Partner::VehicleService.new(api_user: api_user, vehicle: vehicle, vehicle_params: { driver_id: '' })
      service.call

      expect(vehicle.driver).to be_nil
    end

    it "sets the requested driver to the vehicle" do
      driver_id = create(:user).id
      service = Partner::VehicleService.new(api_user: api_user, vehicle: vehicle, vehicle_params: { driver_id: driver_id })
      service.call

      expect(vehicle.driver.id).to eq(driver_id)
    end
  end
end
