# frozen_string_literal: true

require "rails_helper"

describe Commissions::UpdateCommissionExclusionService do
  subject(:update_commission_exclusion) do
    Commissions::UpdateCommissionExclusionService.new(
      company: company,
      commission_exclusion_to_add: commission_exclusion_to_add,
      commission_exclusion_to_remove: commission_exclusion_to_remove,
      type: type
    )
  end

  let(:company) { create(:rescue_company, :with_service_codes, :with_addons) }
  let(:type) { nil }

  context 'when commission_exclusion_to_add is passed' do
    let(:commission_exclusion_to_remove) { nil }

    context 'for services type' do
      let(:commission_exclusion_to_add) do
        [
          { service_code_id: service_to_exclude.id },
        ]
      end

      let(:service_to_exclude) { company.services.first }

      let(:type) { :service }

      it 'changes company.commission_exclusions_services by 1' do
        expect { update_commission_exclusion.call }.to(
          change { company.commission_exclusions_services.count }.by(1)
        )
      end

      it 'does not change company.commission_exclusions_addons' do
        expect { update_commission_exclusion.call }.not_to(
          change { company.commission_exclusions_addons.count }
        )
      end
    end

    context 'for addons type' do
      let(:commission_exclusion_to_add) do
        [
          { service_code_id: addon_to_exclude.id },
        ]
      end

      let(:addon_to_exclude) { company.addons.first }

      let(:type) { :addon }

      it 'changes company.commission_exclusions_addons by 1' do
        expect { update_commission_exclusion.call }.to(
          change { company.commission_exclusions_addons.count }.by(1)
        )
      end

      it 'does not change company.commission_exclusions_services' do
        expect { update_commission_exclusion.call }.not_to(
          change { company.commission_exclusions_services.count }
        )
      end
    end
  end

  context 'when commission_exclusion_to_remove is passed' do
    let(:commission_exclusion_to_add) { nil }

    context 'for services type' do
      let(:commission_exclusion_to_remove) do
        [
          { service_code_id: service_to_exclude.id },
        ]
      end

      let(:service_to_exclude) { company.services.first }

      let(:type) { :service }

      before do
        ServiceCommissionsExclusions.where(
          company: company,
          service_code_id: service_to_exclude.id
        ).first_or_create
      end

      it 'changes company.commission_exclusions_services by -1' do
        expect { update_commission_exclusion.call }.to(
          change { company.commission_exclusions_services.count }.by(-1)
        )
      end

      it 'does not change company.commission_exclusions_addons' do
        expect { update_commission_exclusion.call }.not_to(
          change { company.commission_exclusions_addons.count }
        )
      end
    end

    context 'for addons type' do
      let(:commission_exclusion_to_remove) do
        [
          { service_code_id: addon_to_exclude.id },
        ]
      end

      let(:addon_to_exclude) { company.addons.first }

      let(:type) { :addon }

      before do
        CommissionExclusion.where(
          company: company,
          service_code_id: addon_to_exclude.id,
          item: addon_to_exclude.name
        ).first_or_create
      end

      it 'changes company.commission_exclusions_addons by -1' do
        expect { update_commission_exclusion.call }.to(
          change { company.commission_exclusions_addons.count }.by(-1)
        )
      end

      it 'does not change company.commission_exclusions_services' do
        expect { update_commission_exclusion.call }.not_to(
          change { company.commission_exclusions_services.count }
        )
      end
    end
  end

  context 'when both commission_exclusion_to_add and commission_exclusion_to_remove are blank' do
    let(:commission_exclusion_to_add) { nil }
    let(:commission_exclusion_to_remove) { nil }

    it 'does not throw error' do
      expect { update_commission_exclusion.call }.not_to raise_error
    end

    it 'does not change company.commission_exclusions_services' do
      expect { update_commission_exclusion.call }.to avoid_changing(
        company, :commission_exclusions_services
      )
    end

    it 'does not change company.commission_exclusions_addons' do
      expect { update_commission_exclusion.call }.to avoid_changing(
        company, :commission_exclusions_addons
      )
    end
  end

  context 'when no company is passed' do
    let(:company) { nil }
    let(:commission_exclusion_to_add) { nil }
    let(:commission_exclusion_to_remove) { nil }

    it 'raises ArgumentError' do
      expect { update_commission_exclusion }.to raise_error(ArgumentError)
    end
  end
end
