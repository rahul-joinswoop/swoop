# frozen_string_literal: true

require 'rails_helper'

describe ISSC::JobEtaExplanationMapper do
  subject(:job_explanation) do
    ISSC::JobEtaExplanationMapper.new(job_id: job_id, eta: eta).call.explanation
  end

  let(:job) do
    create(
      :fleet_motor_club_job,
      issc_dispatch_request: issc_dispatch_request,
      fleet_company: fleet_company,
      eta_explanation: job_eta_explanation,
    )
  end
  let(:job_id) { job.id }
  let(:eta) { 1 }

  before do
    allow_any_instance_of(Issc).to receive(:publish_account)
  end

  context 'job has no issc_dispatch_request' do
    let(:issc_dispatch_request) { nil }
    let(:fleet_company) { create(:fleet_company) }
    let(:job_eta_explanation) { nil }

    it { is_expected.to be_nil }
  end

  context 'when job has issc_dispatch_request' do
    let(:issc_dispatch_request) do
      create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end

    context 'and given eta is greater than issc_dispatch_request' do
      let(:eta) { 91.minutes }

      # Agero
      context 'and job is from Agero' do
        let(:fleet_company) { create(:fleet_company, :agero) }
        let(:issc) { create(:ago_issc) }

        let(:job_eta_explanation) do
          create(:job_eta_explanation, text: explanation_text)
        end

        context 'and job_eta_explanation is None' do
          let(:explanation_text) { "None" }

          it { is_expected.to eq "None" }
        end

        context 'and job_eta_explanation is Traffic' do
          let(:explanation_text) { "Traffic" }

          it { is_expected.to eq "Traffic" }
        end

        context 'and job_eta_explanation is Weather' do
          let(:explanation_text) { "Weather" }

          it { is_expected.to eq "Weather" }
        end

        context 'and job_eta_explanation is nil' do
          let(:explanation_text) { nil }

          it { is_expected.to eq "" }
        end
      end

      # Quest
      context 'and job is from Quest' do
        let(:fleet_company) { create(:fleet_company, :quest) }
        let(:issc) { create(:quest_issc, clientid: "QUEST") }

        let(:job_eta_explanation) do
          create(:job_eta_explanation, text: explanation_text)
        end

        context "and job_eta_explanation is Construction" do
          let(:explanation_text) { "Construction" }

          it { is_expected.to eq "Construction" }
        end

        context "and job_eta_explanation is Other" do
          let(:explanation_text) { "Other" }

          it { is_expected.to eq "Other" }
        end

        context "and job_eta_explanation is 'Rural Disablement'" do
          let(:explanation_text) { "Rural Disablement" }

          it { is_expected.to eq "RuralDisablement" }
        end

        context "and job_eta_explanation is 'Service Provider Issue'" do
          let(:explanation_text) { "Service Provider Issue" }

          it { is_expected.to eq "ServiceProviderIssue" }
        end

        context 'and job_eta_explanation is Traffic' do
          let(:explanation_text) { "Traffic" }

          it { is_expected.to eq "Traffic" }
        end

        context 'and job_eta_explanation is Weather' do
          let(:explanation_text) { "Weather" }

          it { is_expected.to eq "Weather" }
        end

        context 'and job_eta_explanation is nil' do
          let(:explanation_text) { nil }

          it { is_expected.to eq "" }
        end
      end
    end

    context 'and job is from Tesla Motors Inc' do
      let(:issc) { create(:tsla_issc) }
      let(:fleet_company) { create(:fleet_company, :tesla_motorclub) }

      # Tesla doesnt need explanation.
      # This also covers all other isscs in the same case, not only Tesla
      let(:job_eta_explanation) { nil }

      context 'and eta > issc_dispatch_request.max_eta' do
        let(:eta) { 91.minutes }

        it { is_expected.to eq "" }
      end

      context 'and eta < issc_dispatch_request.max_eta' do
        let(:eta) { 89.minutes }

        it { is_expected.to eq nil }
      end
    end
  end
end
