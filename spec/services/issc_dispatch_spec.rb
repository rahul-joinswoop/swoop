# frozen_string_literal: true

require "rails_helper"

describe IsscDispatchNew do
  let(:task) { "ACCSEC" }
  let(:job_hash) do
    {
      "Event": "dispatch.new",
      "ClientID": "AGO",
      "ContractorID": "CA123456",
      "DispatchID": "16691973",
      "ResponseID": "4759986",
      "AccountInfo": {
        "CallBackPhone": "540-555-6329",
        "CallFromLocation": "towyardimpoundlot",
        "Member": {
          "FirstName": "MemberFirstName",
          "LastName": "MemberSecondName",
        },
        "Customer": {
          "FirstName": "CustomerFirstName",
          "LastName": "CustomerSecondName",
        },
      },
      "Job": {
        "RequiredAcknowledgeTime": 90,
        "JobID": "4759986",
        "JobDescription": "Key Location:Under Floor Mat . Loaded Mileage: 20  Covered Mileage: 20  Over Mileage: 0 Enroute Mileage :18  ",
        "TimeStamp": "2017-10-05T14:32:34.2430000Z",
        "RequestedFutureDateTime": "2017-10-18T20:00:00.0000000Z",
        "ServiceComments": "Total Fee Owed:850.00, Fee Valid Until:10/05/2017, Payment Methods Accepted:Cash",
        "PrimaryTasks": [
          {
            "ETDRequired": false,
            "Task": task,
          },
        ],
        "ServiceQuestions": [
          {
            "Question": "Tow Type",
            "Responses": [
              "Light",
            ],
          },
          {
            "Question": "What is the drive type?",
            "Responses": [
              "Unknown",
            ],
          },
          {
            "Question": "How many passengers will ride in tow truck?",
            "Responses": [
              "0",
            ],
          },
          {
            "Question": "Is the customer in need of special accommodation?",
            "Responses": [
              "No",
            ],
          },
        ],
      },
      "Vehicle": {
        "Year": "2010",
        "Color": "Red",
        "Make": "Nissan",
        "Model": "Altima",
        "License": "QRT1024",
        "LicenseState": "CA",
        "VehicleType": "Auto",
      },
      "Incident": {
        "Address1": "2354 Sunny Lane",
        "City": "San Mateo",
        "State": "CA",
        "Zip": "94123",
        "SafeLocation": false,
        "Latitude": 38.36894,
        "Longitude": -77.75964,
        "Median": false,
        "OnOffRamp": false,
        "RightShoulder": false,
        "CustomerWithVehicle": false,
        "Comments": "Land Mark: Key Location:Under Floor Mat , Incident Location Name:Rhoadsville Towing, Contact Phone:703-555-7389, Contact Person:sat, Business Hours:Available 24/7",
      },
      "Coverage": {
        "CoveredServices": "Vehicle Release Info:Vehicle released, Vehicle Damages:Driver Side Front Door, Not Safe to Drive, Driver Lights, Entire Side Damaged, Driver Side Front Tire/Rim, Driver Side Mirror",
      },
      "Destination": {
        "Address1": "22 Wormwood Ln",
        "City": "Janderson",
        "State": "California",
        "Zip": "94725",
        "Latitude": 32.30089,
        "Longitude": -70.45914,
        "ConfidenceLatLon": "-1",
        "Phone": "703-555-7389",
        "LocationInfo": "Randall Standofrd",
      },
      "LocationID": "ab:1736375",
    }.with_indifferent_access
  end

  # job 216849
  let(:memorex_location) do
    create(:location, {
      address: "1517 Memorex Dr, Santa Clara, CA 95050",
      lat: 37.3634351,
      lng: -121.9569983,
    })
  end

  let(:memorex) do
    create(:site, {
      location: memorex_location,
    })
  end

  let(:flt) do
    create(:rescue_company, {
      features: [],
      sites: [memorex],
    })
  end

  let!(:agero) { create(:fleet_company, :agero) }

  let!(:issc_location) do
    create(:issc_location, {
      locationid: 'ab:1736375',
      site: memorex,
      fleet_company: agero,
    })
  end

  let!(:account) { create :account, company: flt, client_company: agero }

  let!(:issc) do
    create(:gco_issc, {
      issc_location: issc_location,
      company: flt,
      contractorid: 'CA123456',
      clientid: 'AGO',
    })
  end

  let(:dispatch) { described_class.new(job_hash) }

  subject { dispatch.call }

  context 'parse job' do
    it 'sets scheduled_for' do
      expect(subject.scheduled_for).to eq(Time.parse('2017-10-18T20:00:00.0000000Z'))
    end
  end

  {
    "Tow / Flatbed" => ServiceCode::TOW,
    "ACC" => ServiceCode::ACCIDENT_TOW,
    "ACCSCENE" => ServiceCode::ACCIDENT_TOW,
    "Loaner Wheel" => ServiceCode::LOANER_WHEEL,
    "Tire Change" => ServiceCode::TIRE_CHANGE,
  }.each do |issc_code, swoop_code|
    context "parses issc task '#{issc_code}' as service_code '#{swoop_code}'" do
      let(:task) { issc_code }

      it "works" do
        expect(subject.service_code.name).to eq(swoop_code)
      end
    end
  end
end
