# frozen_string_literal: true

require "rails_helper"

describe VehicleMakeService do
  subject(:service_call) do
    described_class.new(api_company: api_company).call
  end

  let!(:vehicle_make) do
    create(:vehicle_make).tap do |m|
      m.vehicle_models << create(:vehicle_model, vehicle_make: m)
    end
  end

  let!(:heavy_duty_vehicle_make) do
    create(:vehicle_make).tap do |m|
      m.vehicle_models << create(:vehicle_model, name: "heavy duty", vehicle_make: m, heavy_duty: true)
    end
  end

  shared_examples "without heavy_duty" do
    it "does not display heavy_duty makes" do
      expect(service_call).to include(vehicle_make)
      expect(service_call).not_to include(heavy_duty_vehicle_make)
    end
  end

  shared_examples "with heavy_duty" do
    it "does display heavy_duty makes" do
      expect(service_call).to include(vehicle_make)
      expect(service_call).to include(heavy_duty_vehicle_make)
    end
  end

  context 'when api_company is a RescueCompany' do
    context "without heavy_duty_equipment" do
      let(:api_company) { create :rescue_company }

      it_behaves_like "without heavy_duty"
    end

    context "with heavy_duty_equipment" do
      let(:api_company) { create :rescue_company, :heavy_duty_equipment }

      it_behaves_like "with heavy_duty"
    end
  end

  context 'when api_company is a FleetCompany' do
    context "without heavy_duty_equipment" do
      let(:api_company) { create :fleet_company }

      it_behaves_like "without heavy_duty"
    end

    context "with heavy_duty_equipment" do
      let(:api_company) { create :fleet_company, :heavy_duty_equipment }

      it_behaves_like "with heavy_duty"
    end
  end

  context 'when api_company is Swoop' do
    context "without heavy_duty_equipment" do
      let(:api_company) { create :super_company, :swoop }

      it_behaves_like "without heavy_duty"
    end

    context "with heavy_duty_equipment" do
      let(:api_company) do
        create(:super_company, :swoop).tap do |c|
          c.features << create(:feature, name: Feature::HEAVY_DUTY_EQUIPMENT)
        end
      end

      it_behaves_like "with heavy_duty"
    end
  end
end
