# frozen_string_literal: true

require 'rails_helper'

describe InventoryItemService::Update do
  let(:service) do
    described_class.call(
      inventory_item: inventory_item,
      params: params,
      event: event
    )
  end

  let(:event) { Command.new }

  describe '.call' do
    subject { service }

    let(:stranded_vehicle) { create(:stranded_vehicle) }
    let(:inventory_item) { create(:inventory_item, deleted_at: nil, item: stranded_vehicle) }

    context 'when updating released_at' do
      let(:params) do
        {
          released_at: Time.zone.now,
        }
      end

      it 'successfully updates inventory item' do
        expect { subject }.to change(inventory_item, :released_at)
      end
    end

    context 'when deleting' do
      let(:params) do
        {
          deleted_at: Time.zone.now,
        }
      end

      let!(:job_status) { create(:job_status, :stored) }
      let!(:job) { create(:rescue_job) }
      let!(:inventory_item) do
        create(:inventory_item, deleted_at: nil, job: job, item: stranded_vehicle)
      end

      before do
        job.move_to_pending
        job.save!
        job.original_store_vehicle
        job.save!
      end

      it 'successfully updates inventory item' do
        expect { subject }.to change(inventory_item, :deleted_at)
      end

      context 'and invoice is already created' do
        let!(:invoice) { create(:fleet_managed_job_invoice) }
        let!(:job) { create(:rescue_job, invoice: invoice) }
        let!(:line_item) { create(:invoicing_line_item, description: "Storage") }

        before do
          invoice.line_items << line_item
        end

        context 'when inventory item is not a vehicle' do
          let!(:inventory_item) do
            create(:inventory_item, deleted_at: nil, job: job, item_type: 'Tire')
          end

          it 'does not remove job storage' do
            expect(JobService::RemoveStorage).not_to receive(:call)

            subject
          end
        end
      end
    end
  end
end
