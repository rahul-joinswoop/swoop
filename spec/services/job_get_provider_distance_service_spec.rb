# frozen_string_literal: true

require 'rails_helper'

describe JobGetProviderDistanceService do
  let(:fleet_company) { create(:fleet_company) }
  let(:provider) { create(:rescue_company, location: nil) }

  it "sets the job_distance on the rescue provider" do
    site_location = create(:location, lat: 43.123, lng: -110.123)
    job_location = create(:location, lat: 43.456, lng: -110.456)
    site = create(:site, location: site_location)
    provider.sites << site
    rescue_provider = create(:rescue_provider, company: fleet_company, provider: provider, site: site, location: nil)
    job = create(:job, service_location: job_location)
    JobGetProviderDistanceService.new(job, rescue_provider).call
    expect(rescue_provider.job_distance.round(3)).to eq 28.458
  end

  it "defaults to max distance if the job has no location" do
    location = create(:location)
    site = create(:site)
    provider.sites << site
    rescue_provider = create(:rescue_provider, company: fleet_company, provider: provider, site: site, location: location)
    job = create(:job, service_location: nil)
    JobGetProviderDistanceService.new(job, rescue_provider).call
    expect(rescue_provider.job_distance).to eq RescueProvider::MAX_DISTANCE
  end
end
