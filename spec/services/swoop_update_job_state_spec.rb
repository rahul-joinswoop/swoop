# frozen_string_literal: true

require "rails_helper"

describe SwoopUpdateJobState do
  subject do
    SwoopUpdateJobState.new(
      job,
      new_status,
      api_user: api_user,
      api_application: api_application,
      user_agent_hash: user_agent_hash,
      referer: referer
    ).call

    job.save!

    job.reload
  end

  let(:job) { build(:fleet_managed_job, status: current_status) }
  let(:api_user) { nil }
  let(:api_application) { nil }
  let(:user_agent_hash) do
    {
      platform: 'android',
      version: '1.2.3',
    }
  end
  let(:referer) { 'http://example.com' }

  context 'when current job.status is Dispatched' do
    let(:current_status) { 'Dispatched' }

    context 'and new job.status is Completed' do
      include_context "job completed shared examples"

      let(:new_status) { 'Completed' }

      context 'when api_user is passed' do
        let(:api_user) { create(:user) }

        it_behaves_like 'sending Analytics Job Completed event', 'android', '1.2.3', 'http://example.com'
      end

      context 'when api_application is passed' do
        let(:api_application) do
          double(:api_application, owner_id: company.id, owner_type: 'Company')
        end
        let(:company) { create(:rescue_company) }

        it_behaves_like 'sending Analytics Job Completed event', 'android', '1.2.3', 'http://example.com'
      end

      it 'sets job.status to completed' do
        subject

        expect(job.status).to eq Job::STATUS_COMPLETED
      end
    end
  end

  describe 'job.last_touched_by' do
    include_context "job status change shared examples"

    subject do
      SwoopUpdateJobState.new(job, "Dispatched", api_user: api_user).execute
    end

    let(:job) { create(:fleet_managed_job, status: 'pending') }

    it_behaves_like 'updates job.last_touched_by when api_user is passed'
  end

  context "when GOA" do
    let(:current_status) { 'Dispatched' }
    let(:new_status) { "GOA" }
    let(:api_user) { create(:user) }

    it "updates the invoice" do
      expect(job).to receive(:mark_invoice_goa)
      subject
    end

    it "saves the goa_amount if present and uses it on the invoice" do
      SwoopUpdateJobState.new(
        job,
        "GOA",
        api_user: api_user,
        api_application: api_application,
        user_agent_hash: user_agent_hash,
        referer: referer,
        goa_amount: 25.0,
      ).call
      job.reload
      expect(job.billing_info.goa_amount).to eq 25.0
      expect(job.billing_info.goa_agent).to eq api_user
    end
  end
end
