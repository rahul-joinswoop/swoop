# frozen_string_literal: true

require "rails_helper"

describe CompanyService::SaveWithJobs do
  describe '.call' do
    subject { described_class.call(company: company, params: params) }

    let!(:company) { create(:fleet_company, :fleet_demo, name: 'The Company Name') }
    let!(:user) { create(:user, company: company) }
    let!(:account) do
      create(:account, client_company: company,
                       skip_account_setup: true,
                       name: 'The Company Name')
    end

    let(:params) do
      {
        phone: '555-555-5555',
      }
    end

    it 'updates the company appropriately' do
      expect { subject }.to change(company, :phone).to('+15555555555')
    end

    context 'when name provided' do
      let(:params) do
        {
          name: 'Not The Company Name',
        }
      end

      it 'enqueues the name change job' do
        expect(CompanyNameChange).to receive(:perform_async).with(company.id, 'The Company Name')

        subject
      end
    end
  end
end
