# frozen_string_literal: true

require "rails_helper"

describe CompanyService::ChangeFeatures do
  let(:company) { create :company }
  let(:feature) { create :feature }

  before do
    allow_any_instance_of(described_class).to receive(:remove_storage_service_from_company_if_needed)
  end

  describe "#initialize" do
    it "fetches features by ids when feature_ids are given" do
      expect(Feature).to receive(:where).with(id: [2, 3]).and_call_original
      CompanyService::ChangeFeatures.new(company_id: company.id, feature_ids: [2, 3])
    end

    it "fetches features by names when feature_names are given" do
      expect(Feature).to receive(:where).with(name: %w(Settings Questions)).and_call_original
      CompanyService::ChangeFeatures.new(
        company_id: company.id, feature_names: %w(Settings Questions)
      )
    end

    it "assigns company.features to overwrite_features when given" do
      expect_any_instance_of(Company).to receive(:features=).with([])
      CompanyService::ChangeFeatures.new(company_id: company.id, overwrite_features: [])
    end
  end

  describe "#execute" do
    subject do
      described_class.new(
        company_id: company.id, feature_names: feature_names, overwrite_features: []
      )
    end

    context "when adding survey features" do
      let(:feature_names) { [Feature::REVIEW_NPS_SURVEYS] }

      it "successfully adds feature" do
        expect_any_instance_of(SurveyService::SetupSmiley).to receive(:call)

        subject.execute

        expect(company.features.map(&:name)).to include(Feature::REVIEW_NPS_SURVEYS)
      end
    end

    it "adds features passed by ids to company" do
      CompanyService::ChangeFeatures.new(
        company_id: company.id, feature_ids: [feature.id]
      ).execute

      expect(company.features).to include(feature)
    end

    it "adds features passed by name to company" do
      CompanyService::ChangeFeatures.new(
        company_id: company.id, feature_names: [feature.name]
      ).execute

      expect(company.features).to include(feature)
    end

    it "returns a company" do
      expect(CompanyService::ChangeFeatures.new(company_id: company.id).execute)
        .to include(company: company)
    end

    it "calls CompanyService::CreateDefaultQuestions for QUESTIONS" do
      service_code = create(:service_code, name: ServiceCode::BATTERY_JUMP)
      feature = create(:feature, name: Feature::QUESTIONS)
      company.service_codes << service_code
      create(:question, question: Question::KEYS_PRESENT)

      CompanyService::ChangeFeatures.new(
        company_id: company.id, feature_ids: [feature.id]
      ).execute

      questions = CompanyServiceQuestion.where(company_id: company.id, service_code_id: service_code.id)
      expect(questions.count).to eq 1
    end

    describe "issues feature" do
      let!(:issues_feature) { create(:feature, name: Feature::ISSUES) }
      let!(:missed_eta) { create(:issue, name: Issue::MISSED_ETA) }
      let!(:claim) { create(:issue, name: Issue::CLAIM) }

      it "adds the standard set of issues to fleet in house companies" do
        company = create(:fleet_in_house_company)
        expect(company.issues.count).to eq 0
        CompanyService::ChangeFeatures.new(company_id: company.id, feature_ids: [issues_feature.id]).execute
        expect(company.issues.count).not_to eq 0
        expect(company.issues).to match_array(Issue.standard_items)
      end

      it "does not add issues to non-fleet in house companies" do
        company = create(:fleet_company)
        expect(company.issues.count).to eq 0
        CompanyService::ChangeFeatures.new(company_id: company.id, feature_ids: [issues_feature.id]).execute
        expect(company.issues.count).to eq 0
      end

      it "does not add issues if the company already has a customized list of issues" do
        company = create(:fleet_in_house_company)
        company.issues << missed_eta
        expect(company.issues.count).to eq 1
        CompanyService::ChangeFeatures.new(company_id: company.id, feature_ids: [issues_feature.id]).execute
        expect(company.issues).to eq [missed_eta]
      end
    end
  end
end
