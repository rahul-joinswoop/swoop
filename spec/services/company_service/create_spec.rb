# frozen_string_literal: true

require "rails_helper"

describe CompanyService::Create do
  describe '#call' do
    subject(:service_call) do
      company_service.call
    end

    let(:company_service) do
      CompanyService::Create.new(
        company_attributes: company_attributes,
        location_attributes: location_attributes,
        feature_ids: feature_ids,
        api_company: api_company,
        api_user: api_user,
      )
    end

    let(:company_attributes) do
      {
        name: "Test",
        phone: "+14056767823",
        accounting_email: "accounting@email.com",
        type: company_type,
        dispatch_email: "dispatch@email.com",
        support_email: "support@email.com",
        currency: Money.default_currency.iso_code,
      }
    end

    let(:location_attributes) do
      {
        address: "San Francisco, CA",
        lat: 37.7749295,
        lng: -122.41941550000001,
        place_id: "ChIJIQBpAG2ahYAR_6128GcTUEo",
        exact: true,
      }
    end

    let(:feature_ids) { [] }
    let(:api_company) { create(:super_company) }
    let(:api_user) { create(:user, company: api_company) }
    let!(:admin_role) { create(:role, :admin) }
    let!(:dispatcher_role) { create(:role, :dispatcher) }
    let!(:super_root_user) do
      u = create(:user, :root)
      u
    end

    before do
      allow_any_instance_of(Company).to receive(:add_features_by_name)
      allow_any_instance_of(Company).to receive(:add_customer_types_by_name)
      allow_any_instance_of(Company).to receive(:add_reports_by_name)
    end

    shared_context 'sets currency' do
      subject { service_call.company }

      it "defaults to USD" do
        company_attributes.delete(:currency)
        expect(subject.currency).to eq('USD')
      end

      it "can't be blank" do
        company_attributes[:currency] = ''
        expect { subject.currency }.to raise_error(ActiveRecord::RecordInvalid, /Currency can't be blank/)
      end

      it 'to USD' do
        company_attributes[:currency] = 'USD'
        expect(subject.currency).to eq('USD')
      end

      it 'to EUR' do
        company_attributes[:currency] = 'EUR'
        expect(subject.currency).to eq('EUR')
      end

      it 'handles an invalid value' do
        # KAZAKHSTAN Tenge
        company_attributes[:currency] = 'KZT'
        expect { subject.currency }.to raise_error(ActiveRecord::RecordInvalid, /KZT is not supported/)
      end
    end

    shared_context 'sets distance unit' do
      subject { service_call.company }

      it "defaulting to miles" do
        expect(subject.distance_unit).to eq(Company::MILE)
      end

      it 'to Mile' do
        company_attributes['distance_unit'] = Company::MILE
        expect(subject.distance_unit).to eq(Company::MILE)
      end

      it 'to Kilometer' do
        company_attributes['distance_unit'] = Company::KILOMETER
        expect(subject.distance_unit).to eq(Company::KILOMETER)
      end
    end

    context 'when creating a RescueCompany' do
      let(:company_type) { "RescueCompany" }

      it 'creates a RescueCompany successfully' do
        company = service_call.company
        expect(company).to be_present
        expect(company.name).to eq 'Test'
        expect(company.phone).to eq '+14056767823'
        expect(company.accounting_email).to eq 'accounting@email.com'
        expect(company.type).to eq 'RescueCompany'
        expect(company.dispatch_email).to eq "dispatch@email.com"
        expect(company.support_email).to eq "support@email.com"
        expect(company.location).to be_present
        expect(QB::Token.find_by(company: company).token).to be_present
        expect(company.invoice_charge_fee.percentage).to be_present
        expect(company.invoice_charge_fee.fixed_value).to be_present
        expect(company.invoice_charge_fee.payout_interval).to be_present
      end

      it 'creates the HQ site successfully' do
        company = service_call.company
        hq = company.hq

        expect(hq.name).to eq "HQ"
        expect(hq.location).to eq company.location
        expect(hq.dispatchable).to be_truthy
        expect(hq.always_open).to be_truthy
        expect(hq.phone).to eq company.phone
      end

      it "creates a Swoop account with a GOA rate" do
        company = service_call.company
        swoop_account = company.accounts.find_by(client_company_id: Company.swoop_id)
        expect(swoop_account.name).to eq "Swoop"
        goa_rate = company.rates.find_by(account: swoop_account, service_code: ServiceCode.goa)
        expect(goa_rate.flat).to eq 0
      end

      # These tests behave the same regardless of company type
      # so just testing on RescueCompany
      it_behaves_like 'sets currency'
      it_behaves_like 'sets distance unit'

      it "creates a default hq territory with a radius of 5 miles" do
        expect(Territory.count).to eq(0)
        company = service_call.company
        territory = Territory.find_by(company_id: company.id)
        expect(territory["company_id"]).to eq(company.id)
        expect(Territory.count).to eq(1)
      end
    end

    context 'when creating a FleetCompany' do
      let(:company_type) { "FleetCompany" }

      it 'won\'t call RescueCompany methods' do
        expect(company_service).not_to receive(:customize_new_rescue_company)
        expect(company_service).not_to receive(:add_qb_token_to_rescue_company)
        company = service_call.company
        expect(QB::Token.find_by(company: company)&.token).not_to be_present
      end

      it 'has a default ranker' do
        fleet_company = service_call.company
        expect(fleet_company.ranker).not_to be_nil
        expect(fleet_company.ranker).to be_kind_of Auction::Ranker
        expect(fleet_company.ranker.weights).not_to be_nil
        expect(fleet_company.ranker.constraints).not_to be_nil
      end
    end
  end
end
