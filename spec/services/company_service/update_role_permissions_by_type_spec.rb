# frozen_string_literal: true

require "rails_helper"

describe CompanyService::UpdateRolePermissionsByType do
  subject(:service_call) do
    CompanyService::UpdateRolePermissionsByType.call({
      input: {
        company: company,
        role_ids: role_ids,
        permission_type: permission_type,
      },
    })
  end

  let(:company) { create(:rescue_company) }
  let(:role_ids) { [role_admin.id, role_dispatcher.id] }
  let(:role_admin) { create(:role, :admin) }
  let(:role_dispatcher) { create(:role, :dispatcher) }
  let(:permission_type) { CompanyRolePermission::CHARGE }

  it "adds the given role_permissions by type" do
    service_call
    company.reload

    expect(company.role_permissions).to be_present
    expect(company.role_permissions.size).to eq 2

    permission_admin = company.role_permissions.where(
      permission_type: permission_type,
      role_id: role_admin.id
    )

    permission_dispatcher = company.role_permissions.where(
      permission_type: permission_type,
      role_id: role_dispatcher.id
    )

    expect(permission_admin).to be_present
    expect(permission_dispatcher).to be_present
  end

  it "triggers PublishCompany WS message" do
    expect(PublishCompanyWorker).to receive(:perform_async).with(
      company.id, 'CompanyService::UpdateRolePermissionsByType'
    ).and_return(nil)

    service_call
  end

  context "when company has role_permissions already" do
    let!(:company_role_permission) do
      create(
        :company_role_permission,
        company: company,
        role: role_dispatcher
      )
    end
    let(:role_ids) { [role_admin.id] }

    it 'removes not given roles for the given type' do
      permission_dispatcher = company.role_permissions.where(
        permission_type: permission_type,
        role_id: role_dispatcher.id
      )

      expect(permission_dispatcher).to be_present

      service_call
      company.reload

      permission_dispatcher = company.role_permissions.where(
        permission_type: permission_type,
        role_id: role_dispatcher.id
      )
      permission_admin = company.role_permissions.where(
        permission_type: permission_type,
        role_id: role_admin.id
      )

      expect(permission_admin).to be_present
      expect(permission_dispatcher).to be_blank
    end
  end
end
