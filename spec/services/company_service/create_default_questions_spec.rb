# frozen_string_literal: true

require "rails_helper"

describe CompanyService::CreateDefaultQuestions do
  subject(:create_default_questions_service) do
    CompanyService::CreateDefaultQuestions.new(
      company: company, service_codes: [service_code]
    )
  end

  let(:company) { build_stubbed :company }
  let(:question) { build_stubbed :question }
  let(:service_code) { build_stubbed :service_code }

  describe "#execute" do
    context "when there are default questions for given ServiceCodes" do
      before do
        allow(Question)
          .to receive(:defaults_per_service_code).and_return([question])
      end

      it "creates a CompanyServiceQuestion when one does not exist" do
        allow(CompanyServiceQuestion).to receive(:exists?).and_return(false)
        expect(CompanyServiceQuestion).to receive(:create).with(
          company: company, service_code: service_code, question: question
        )
        create_default_questions_service.execute
      end

      it "does not create a CompanyServiceQuestion when one exists" do
        allow(CompanyServiceQuestion).to receive(:exists?).and_return(true)
        expect(CompanyServiceQuestion).not_to receive(:create)
        create_default_questions_service.execute
      end
    end

    context "when there are no default questions for given ServiceCodes" do
      before do
        allow(Question).to receive(:defaults_per_service_code).and_return(nil)
      end

      it "does not create any CompanyServiceQuestion" do
        expect(CompanyServiceQuestion).not_to receive(:find_or_create_by)
        create_default_questions_service.execute
      end
    end
  end
end
