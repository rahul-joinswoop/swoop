# frozen_string_literal: true

require "rails_helper"

describe CompanyService::AddDefaultInvoiceChargeFeeToPartner do
  subject(:service_call) do
    CompanyService::AddDefaultInvoiceChargeFeeToPartner.call(
      input: {
        company_id: company_id,
      }
    )
  end

  let(:rescue_company) { create :rescue_company }
  let(:company_id) { rescue_company.id }
  let(:invoice_charge_fee_percentage) { 2.6 }
  let(:invoice_charge_fee_fixed_value) { 0.3 }
  let(:invoice_charge_fee_payout_interval) { "weekly" }

  before do
    config = Configuration.stripe.merge(
      "invoice_charge_fee_percentage" => invoice_charge_fee_percentage,
      "invoice_charge_fee_fixed_value" => invoice_charge_fee_fixed_value,
      "invoice_charge_fee_payout_interval" => invoice_charge_fee_payout_interval,
    )
    allow(Configuration).to receive(:stripe).and_return(config)
  end

  describe "#call" do
    it 'creates the fee and associates with the given company_id' do
      fee = service_call.output

      expect(fee.company_id).to eq rescue_company.id

      expect(fee.percentage.to_s).to eq('2.6')
      expect(fee.fixed_value.to_s).to eq('0.3')
      expect(fee.payout_interval.to_s).to eq('weekly')
    end

    context 'when Configuration.stripe.invoice_charge_fee_percentage is 0.2' do
      let(:invoice_charge_fee_percentage) { 0.2 }

      context 'and Configuration.stripe.invoice_charge_fee_fixed_value is 2' do
        let(:invoice_charge_fee_fixed_value) { 2.0 }

        it 'sets charge_fee settings accordingly' do
          fee = service_call.output

          expect(fee.percentage.to_s).to eq('0.2')
          expect(fee.fixed_value.to_s).to eq('2.0')
          expect(fee.payout_interval.to_s).to eq('weekly')
        end
      end

      context 'and Configuration.stripe.invoice_charge_fee_payout_interval is monthly' do
        let(:invoice_charge_fee_payout_interval) { "monthly" }

        it 'sets charge_fee settings accordingly' do
          fee = service_call.output

          expect(fee.percentage.to_s).to eq('0.2')
          expect(fee.fixed_value.to_s).to eq('0.3')
          expect(fee.payout_interval.to_s).to eq('monthly')
        end
      end
    end

    context 'when no company_id is given' do
      let(:company_id) { nil }

      it 'raises ArgumentError' do
        expect { service_call }.to raise_error(ArgumentError)
      end
    end
  end
end
