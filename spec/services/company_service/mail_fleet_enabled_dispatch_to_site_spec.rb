# frozen_string_literal: true

require "rails_helper"

describe CompanyService::MailFleetEnabledDispatchToSite do
  subject(:mail_dispatch_enabled) do
    described_class.new(
      fleet_company: fleet_company,
      fleet_user: fleet_user,
      rescue_provider: rescue_provider
    )
  end

  let(:fleet_user)    { build(:user, id: 1) }

  let(:fleet_company) do
    build(:company, id: 1, support_email: fleet_company_support_email)
  end

  let(:fleet_company_support_email) { 'fleet_company_support_email@email.com' }

  let(:rescue_provider) do
    build(
      :rescue_provider,
      id: 1,
      provider: rescue_company
    )
  end

  let(:rescue_company) do
    build(
      :company,
      id: 2,
      dispatch_email: rescue_company_dispatch_email
    )
  end

  let(:rescue_company_dispatch_email) { 'rescue_company_dispatch_email@email.com' }

  describe '.initialize' do
    it 'sets @fleet_company' do
      expect(mail_dispatch_enabled.instance_variable_get('@fleet_company')).to eq fleet_company
    end

    it 'sets @fleet_user' do
      expect(mail_dispatch_enabled.instance_variable_get('@fleet_user')).to eq fleet_user
    end

    it 'sets @rescue_provider' do
      expect(mail_dispatch_enabled.instance_variable_get('@rescue_provider')).to(
        eq rescue_provider
      )
    end
  end

  describe '#execute' do
    it 'returns nil' do
      expect(mail_dispatch_enabled.execute).to eq nil
    end
  end

  describe '#after_execution' do
    before :each do
      allow(Delayed::Job).to receive(:enqueue)
    end

    it 'schedule change_rate_email to be executed asynchronously' do
      expect(Delayed::Job).to receive(:enqueue)

      mail_dispatch_enabled.after_execution
    end

    context 'when rescue_company.dispatch_email.blank?' do
      let(:rescue_company_dispatch_email) { nil }

      let(:error_message) do
        "[Fleet Enabled Dispatch Email] was not sent because the rescue company #{rescue_provider.provider.name} " \
        "(id: #{rescue_provider.provider.id}) doesn't have a dispatch email"
      end

      before :each do
        allow_any_instance_of(ActiveSupport::Logger).to receive(:error)
      end

      it 'logs an error' do
        expect_any_instance_of(ActiveSupport::Logger). to receive(:error).with(error_message)

        mail_dispatch_enabled.after_execution
      end

      it 'returns nil' do
        expect(mail_dispatch_enabled.after_execution).to eq nil
      end

      it 'doesn\'t schedule change_rate_email to be executed asynchronously' do
        expect(Delayed::Job).not_to receive(:enqueue)

        mail_dispatch_enabled.after_execution
      end
    end

    context 'when company.support_email.blank?' do
      let(:fleet_company_support_email) { nil }

      let(:error_message) do
        "[Fleet Enabled Dispatch Email] was sent, but not to the the FleetInHouse (#{fleet_company.name} " \
        "id: #{fleet_company.id}), because it doesn't have a support email"
      end

      before :each do
        allow_any_instance_of(ActiveSupport::Logger).to receive(:error)
        allow(Delayed::Job).to receive(:enqueue)
      end

      it 'logs an error' do
        expect_any_instance_of(ActiveSupport::Logger). to receive(:error).with(error_message)

        mail_dispatch_enabled.after_execution
      end

      it 'schedule change_rate_email to be executed asynchronously' do
        expect(Delayed::Job).to receive(:enqueue)

        mail_dispatch_enabled.after_execution
      end
    end
  end
end
