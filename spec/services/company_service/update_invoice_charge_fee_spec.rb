# frozen_string_literal: true

require "rails_helper"

describe CompanyService::UpdateInvoiceChargeFee do
  describe ".call" do
    subject(:service_call) do
      CompanyService::UpdateInvoiceChargeFee.call(
        input: {
          company_id: rescue_company.id,
          invoice_charge_fee_attributes: invoice_charge_fee_attributes,
        },
      )
    end

    def invoice_charge_fee
      rescue_company.reload

      rescue_company.invoice_charge_fee
    end

    let(:invoice_charge_fee_attributes) do
      {
        percentage: 10,
        fixed_value: 0.5,
        payout_interval: 'daily',
      }
    end

    let(:rescue_company) { create(:rescue_company, name: 'Rescue with fee') }
    let!(:company_invoice_charge_fee) do
      create(
        :company_invoice_charge_fee,
        company: rescue_company,
        percentage: 1,
        fixed_value: 2,
        payout_interval: 'weekly'
      )
    end
    let!(:custom_account) { create(:custom_account, company: rescue_company) }

    before do |example|
      unless example.metadata[:skip_before]
        allow(Stripe::UpdatePayoutScheduleWorker).to receive(:perform_async).and_call_original

        service_call
      end
    end

    it 'updates invoice_charge_fee accordingly' do
      expect(invoice_charge_fee.percentage).to eq 10
      expect(invoice_charge_fee.fixed_value.to_s).to eq '0.5'
      expect(invoice_charge_fee.payout_interval).to eq 'daily'

      expect(Stripe::UpdatePayoutScheduleWorker).to have_received(:perform_async).with(
        rescue_company.custom_account.id, 'daily'
      )
    end

    context 'when the custom_account is deleted' do
      let!(:custom_account) do
        create(:custom_account, company: rescue_company, deleted_at: Time.current)
      end

      it 'updates invoice_charge_fee accordingly' do
        expect(invoice_charge_fee.percentage).to eq 10
        expect(invoice_charge_fee.fixed_value.to_s).to eq '0.5'
        expect(invoice_charge_fee.payout_interval).to eq 'daily'
      end

      it 'does not schedule Stripe::UpdatePayoutScheduleWorker' do
        expect(Stripe::UpdatePayoutScheduleWorker).not_to have_received(:perform_async)
      end
    end

    context 'when invoice_charge_fee is passed as nil', :skip_before do
      let(:invoice_charge_fee_attributes) { nil }

      it 'throws ArgumentError' do
        expect { service_call }.to raise_error(ArgumentError)
      end
    end

    context 'when percentage, fixed_value and payout_interval are passed as nil', :skip_before do
      let(:invoice_charge_fee_attributes) do
        {}
      end

      it 'throws ArgumentError' do
        expect { service_call }.to raise_error(ArgumentError)
      end
    end

    context 'when only invoice_charge_fee.percentage is passed' do
      let(:invoice_charge_fee_attributes) do
        {
          percentage: 3.4,
        }
      end

      it 'updates only invoice_charge_fee.percentage' do
        expect(invoice_charge_fee.percentage).to eq 3.4
        expect(invoice_charge_fee.fixed_value.to_s).to eq '2.0'
        expect(invoice_charge_fee.payout_interval).to eq 'weekly'

        expect(Stripe::UpdatePayoutScheduleWorker).not_to have_received(:perform_async)
      end
    end

    context 'when only invoice_charge_fee.fixed_value is passed' do
      let(:invoice_charge_fee_attributes) do
        {
          fixed_value: 2.78,
        }
      end

      it 'updates only invoice_charge_fee.fixed_value' do
        expect(invoice_charge_fee.percentage).to eq 1
        expect(invoice_charge_fee.fixed_value.to_s).to eq '2.78'
        expect(invoice_charge_fee.payout_interval).to eq 'weekly'

        expect(Stripe::UpdatePayoutScheduleWorker).not_to have_received(:perform_async)
      end
    end

    context 'when only invoice_charge_fee.payout_interval is passed' do
      let(:invoice_charge_fee_attributes) do
        {
          payout_interval: 'monthly',
        }
      end

      it 'updates only invoice_charge_fee.payout_interval' do
        expect(invoice_charge_fee.percentage).to eq 1
        expect(invoice_charge_fee.fixed_value.to_s).to eq '2.0'
        expect(invoice_charge_fee.payout_interval).to eq 'monthly'

        expect(Stripe::UpdatePayoutScheduleWorker).to have_received(:perform_async).with(
          rescue_company.custom_account.id, 'monthly'
        )
      end
    end
  end
end
