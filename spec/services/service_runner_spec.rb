# frozen_string_literal: true

require "rails_helper"

describe ServiceRunner do
  subject(:runner) { described_class.new }

  let(:service) do
    instance_double(
      'service',
      execute: nil,
      after_execution: nil
    )
  end

  before :each do
    allow(service).to receive(:execute)
    allow(service).to receive(:after_execution)
  end

  describe '.initialize' do
    it 'sets @services as an empty array' do
      expect(runner.instance_variable_get('@services')).to eq []
    end
  end

  describe '#<<' do
    it 'adds values into @services' do
      runner << service

      expect(runner.instance_variable_get('@services').first).to eq service
    end
  end

  describe '#call_all' do
    it 'calls service#execute' do
      runner << service

      expect(service).to receive(:execute)

      runner.call_all
    end

    it 'calls service.after_execution' do
      runner << service

      expect(service).to receive(:after_execution)

      runner.call_all
    end

    context 'when nil is added as a service' do
      it 'doesnt throw error' do
        runner << service
        runner << nil

        expect { runner.call_all }.not_to raise_error
      end
    end

    context 'when service doesn\'t respond_to #execute' do
      let(:service_with_no_execute) do
        instance_double(
          'service',
          after_execution: nil
        )
      end

      it 'throws error' do
        expect(service_with_no_execute).to receive(:execute).and_raise(NoMethodError)
        runner << service_with_no_execute

        expect { runner.call_all }.to raise_error(NoMethodError)
      end
    end

    context 'when service doesn\'t respond_to #after_execution' do
      let(:service_with_no_after_execution) do
        instance_double(
          'service',
          execute: nil
        )
      end

      it 'doesn\'t throw error' do
        runner << service_with_no_after_execution

        expect { runner.call_all }.not_to raise_error
      end
    end
  end
end
