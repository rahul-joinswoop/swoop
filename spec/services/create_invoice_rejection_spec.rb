# frozen_string_literal: true

require "spec_helper"
require "./app/services/swoop_service"
require "./app/services/create_invoice_rejection"

describe CreateInvoiceRejection do
  let(:invoice_reject_reason) { double "InvoiceRejectReason" }
  let(:invoice) { double "Invoice", next_invoice_state!: true }
  let(:invoice_state_event) { :next_invoice_state }
  let(:notes) { "Approval was not granted" }
  let(:invoice_rejection) do
    double "InvoiceRejection", valid?: true, save: true, id: 123
  end
  let(:email_invoice_rejection) { double "EmailInvoiceRejection" }
  let(:delayed_job) { double "Delayed::Job", enqueue: true }

  before do
    class_double("InvoiceRejection").as_stubbed_const
    allow(InvoiceRejection).to receive(:new).and_return(invoice_rejection)

    class_double("Delayed::Job").as_stubbed_const
    allow(Delayed::Job).to receive(:enqueue).and_return(delayed_job)

    class_double("EmailInvoiceRejection").as_stubbed_const
    allow(EmailInvoiceRejection)
      .to receive(:new).and_return(email_invoice_rejection)
  end

  describe "#call" do
    subject(:call) { CreateInvoiceRejection.new(args).call }

    let(:args) do
      {
        invoice: invoice,
        invoice_state_event: invoice_state_event,
        invoice_reject_reason: invoice_reject_reason,
        notes: notes,
      }
    end

    it "raises an error when invalid new InvoiceRejection" do
      allow(invoice_rejection).to receive(:valid?).and_return false
      expect { call }
        .to raise_error(CreateInvoiceRejection::InvalidInvoiceRejectionError)
    end

    it "calls save on new InvoiceRejection instance" do
      expect(InvoiceRejection).to receive(:new).with(
        invoice: invoice,
        invoice_reject_reason: invoice_reject_reason,
        notes: notes
      ).and_return(invoice_rejection)
      expect(invoice_rejection).to receive(:save)
      call
    end

    context "when saving new InvoiceRejection is successful" do
      it "transitions the invoice to given state" do
        expect(invoice).to receive(:send).with("next_invoice_state!")
        call
      end

      it "adds invoice rejection notification email to DelayedJob" do
        expect(Delayed::Job)
          .to receive(:enqueue).with(EmailInvoiceRejection.new(123))
        call
      end
    end

    context "when saving new InvoiceRejection is not successful" do
      before do
        allow(invoice_rejection).to receive(:save).and_return false
      end

      it "does not transition the invoice to given state" do
        expect(invoice).not_to receive(:send).with(invoice_state_event)
        call
      end
    end

    it "raises an error when the invoice can't transition to given state" do
      allow(invoice).to receive(:send).and_raise("AASM::InvalidTransition")
      expect { call }.to raise_error("AASM::InvalidTransition")
    end
  end
end
