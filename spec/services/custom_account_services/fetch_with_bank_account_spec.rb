# frozen_string_literal: true

require "rails_helper"

describe CustomAccountServices::FetchWithBankAccount, vcr: true do
  subject(:service_call) do
    CustomAccountServices::FetchWithBankAccount.call({
      input: {
        api_company_id: rescue_company.id,
      },
    })
  end

  let(:rescue_company) { create(:rescue_company) }

  context 'when rescue_company has a custom_account' do
    let!(:custom_account) do
      create(
        :custom_account,
        upstream_account_id: 'acct_1CkIL2JmoY5nwfCu',
        company_id: rescue_company.id
      )
    end

    it 'fetches its corresponding custom_account' do
      found_custom_account = service_call.output[:custom_account]

      expect(found_custom_account.id).to eq custom_account.id
    end

    it 'fetches its corresponding stripe_bank_account' do
      found_stripe_bank_account = service_call.output[:stripe_bank_account]

      expect(found_stripe_bank_account.account).to eq 'acct_1CkIL2JmoY5nwfCu'
    end

    context 'when rescue_company has feature DISABLE_BANK_SETTINGS set' do
      before do
        allow(RescueCompany).to receive(:find).with(rescue_company.id).and_return(rescue_company)

        allow(rescue_company).to receive(:has_feature?).with(
          Feature::DISABLE_BANK_SETTINGS
        ).and_return(true)
      end

      it 'sets output[:custom_account] as nil' do
        nil_custom_account = service_call.output[:custom_account]

        expect(nil_custom_account).to be_nil
      end

      it 'sets output[:stripe_bank_account] as nil' do
        nil_stripe_bank_account = service_call.output[:stripe_bank_account]

        expect(nil_stripe_bank_account).to be_nil
      end

      it 'does not call instantiate StripeIntegration::BankAccountRetriever' do
        allow(StripeIntegration::BankAccountRetriever).to receive(:new)

        expect(StripeIntegration::BankAccountRetriever).not_to receive(:new)

        service_call
      end
    end

    context 'when a Stripe BankAccount is not found on Stripe end' do
      it 'flags service_call as failed' do
        expect(service_call.failure?).to be_truthy
      end
    end
  end

  context 'when rescue_company does not have a custom_account' do
    let!(:custom_account) { nil }

    it 'sets output[:custom_account] as nil' do
      nil_custom_account = service_call.output[:custom_account]

      expect(nil_custom_account).to be_nil
    end

    it 'sets output[:stripe_bank_account] as nil' do
      nil_stripe_bank_account = service_call.output[:stripe_bank_account]

      expect(nil_stripe_bank_account).to be_nil
    end

    it 'does not call instantiate StripeIntegration::BankAccountRetriever' do
      allow(StripeIntegration::BankAccountRetriever).to receive(:new)

      expect(StripeIntegration::BankAccountRetriever).not_to receive(:new)

      service_call
    end
  end
end
