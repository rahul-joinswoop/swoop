# frozen_string_literal: true

require "rails_helper"

describe CustomAccountServices::DeleteByRescueCompanyId do
  subject(:service_call) do
    CustomAccountServices::DeleteByRescueCompanyId.call({
      input: {
        api_company_id: super_company.id,
        api_user_id: api_user.id,
        rescue_company_id: rescue_company.id,
      },
    })
  end

  let(:super_company) { create(:super_company) }
  let(:api_user) { create(:user, company: super_company) }
  let(:rescue_company) { create(:rescue_company) }

  before do
    allow(Stripe::DeleteCustomAccountByRescueCompanyIdWorker).to receive(:perform_async)
  end

  it 'creates a new Api::AsyncRequest' do
    expect { service_call }.to change { API::AsyncRequest.count }.by(1)

    async_request = API::AsyncRequest.where(
      company_id: super_company.id,
      user_id: api_user.id,
      system: API::AsyncRequest::DELETE_CUSTOM_ACCOUNT
    )

    expect(async_request.count).to eq 1
  end

  it 'schedules Stripe::DeleteCustomAccountByRescueCompanyIdWorker' do
    service_call

    async_request = API::AsyncRequest.where(
      company_id: super_company.id,
      user_id: api_user.id,
      system: API::AsyncRequest::DELETE_CUSTOM_ACCOUNT
    ).first

    expect(Stripe::DeleteCustomAccountByRescueCompanyIdWorker).to have_received(
      :perform_async
    ).with(async_request.id, rescue_company.id)
  end
end
