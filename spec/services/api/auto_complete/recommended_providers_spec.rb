# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::RecommendedProviders do
  let(:company) { create(:fleet_company) }
  let(:job) { create(:fleet_managed_job, fleet_company: company, service_code: code_1, service_location: create(:location)) }
  let(:code_1) { create(:service_code, name: "code_1") }
  let(:code_2) { create(:service_code, name: "code_2") }

  it "raises an error if the term is too short" do
    expect(lambda {
      API::AutoComplete::RecommendedProviders.new(term: "f", api_company: company, job_id: job.id, limit: 5)
    }).to raise_error(ArgumentError)
  end

  it "matches rescue companies with service codes by name and site" do
    create(:fleet_company)

    provider_1 = create(:rescue_company, name: "Joe's Towing")
    provider_1.service_codes << code_1
    provider_1.service_codes << code_2
    provider_1.sites << create(:site, name: "HQ")
    provider_1.sites << create(:site, name: "Downton")
    rescue_provider_1a = create(:rescue_provider, company: company, provider: provider_1, site: provider_1.sites.first)
    rescue_provider_1b = create(:rescue_provider, company: company, provider: provider_1, site: provider_1.sites.last)

    provider_2 = create(:rescue_company, name: "John's Towing")
    provider_2.service_codes << code_1
    provider_2.sites << create(:site, name: "Downtown")
    rescue_provider_2 = create(:rescue_provider, company: company, provider: provider_2, site: provider_2.sites.first)

    provider_3 = create(:rescue_company, name: "Mike's Towing")
    provider_3.service_codes << code_1
    provider_3.sites << create(:site, name: "HQ")
    rescue_provider_3 = create(:rescue_provider, company: company, provider: provider_3, site: provider_3.sites.first)

    provider_4 = create(:rescue_company, name: "Jo's Towing")
    provider_4.service_codes << code_2
    provider_4.sites << create(:site, name: "HQ")
    create(:rescue_provider, company: company, provider: provider_4, site: provider_4.sites.first)

    provider_5 = create(:rescue_company, name: "Joe's Old Towing", deleted_at: Time.now)
    provider_5.service_codes << code_1
    provider_5.sites << create(:site, name: "HQ")
    create(:rescue_provider, company: company, provider: provider_5, site: provider_5.sites.first)

    autocomplete = API::AutoComplete::RecommendedProviders.new(term: "JO", api_company: company, job_id: job.id)
    expect(autocomplete.call).to match_array [rescue_provider_1a, rescue_provider_1b, rescue_provider_2]

    autocomplete = API::AutoComplete::RecommendedProviders.new(term: "JOe", api_company: company, job_id: job.id)
    expect(autocomplete.call).to match_array [rescue_provider_1a, rescue_provider_1b]

    autocomplete = API::AutoComplete::RecommendedProviders.new(term: "HQ", api_company: company, job_id: job.id)
    expect(autocomplete.call).to match_array [rescue_provider_1a, rescue_provider_3]

    autocomplete = API::AutoComplete::RecommendedProviders.new(term: "joe's towing h", api_company: company, job_id: job.id)
    expect(autocomplete.call).to match_array [rescue_provider_1a]
  end

  it "should sort by distance"
end
