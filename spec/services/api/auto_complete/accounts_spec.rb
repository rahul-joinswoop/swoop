# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::Accounts do
  subject(:service_call) { API::AutoComplete::Accounts.new(term: term, api_company: company, limit: 10, **args).call }

  let(:company) { create(:rescue_company) }
  let(:other_company) { create(:rescue_company) }
  let(:fleet_1) { create(:fleet_in_house_company) }
  let(:fleet_2) { create(:fleet_in_house_company) }
  let(:fleet_3) { create(:fleet_in_house_company) }
  let!(:account_1) { create(:account, company: company, client_company: fleet_1, name: "Account Test 1") }
  let!(:account_2) { create(:account, company: other_company, client_company: fleet_1, name: "Account Test 2") }
  let!(:account_3) { create(:account, company: company, client_company: fleet_2, name: "Account Test 3") }
  let!(:account_4) { create(:account, company: company, client_company: fleet_3, name: "Account Test 4", deleted_at: Time.now) }

  let(:expected_match) { [account_1, account_3] }
  let(:args) { {} }

  shared_context 'find accounts as expected' do
    it "finds accounts as expected" do
      expect(service_call).to match_array expected_match
    end
  end

  context 'when a valid term is passed' do
    let(:term) { "test" }

    it_behaves_like 'find accounts as expected'
  end

  context 'when term is 1 char long' do
    let(:term) { 't' }

    it_behaves_like 'find accounts as expected'
  end
end
