# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::RootUsers do
  it "returns root users with names including the specified term" do
    create(:user, :rescue, full_name: "Joe Alpha")
    user_2 = create(:user, :root, full_name: "Joe Beta")
    user_3 = create(:user, :root, full_name: "Jo Epsilon")
    user_4 = create(:user, :root, full_name: "Jerry WithJoeInTheMiddle")

    expect(API::AutoComplete::RootUsers.new("joe").call).to match_array [user_2, user_4]
    expect(API::AutoComplete::RootUsers.new("jo e").call).to match_array [user_3]
  end
end
