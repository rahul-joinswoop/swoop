# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::TrailerVehicles do
  subject(:service_call) { API::AutoComplete::TrailerVehicles.new(term: term, api_company: company, limit: 10, **args).call }

  let(:company) { create(:rescue_company) }
  let(:other_company) { create(:rescue_company) }
  let(:fleet_1) { create(:fleet_in_house_company) }
  let(:fleet_2) { create(:fleet_in_house_company) }
  let(:fleet_3) { create(:fleet_in_house_company) }
  let!(:trailer_vehicle_1) { create(:trailer_vehicle, company: company, name: "Account Test 1") }
  let!(:trailer_vehicle_2) { create(:trailer_vehicle, company: other_company, name: "Account Test 2") }
  let!(:trailer_vehicle_3) { create(:trailer_vehicle, company: company, name: "Account Test 3") }
  let!(:trailer_vehicle_4) { create(:trailer_vehicle, company: company, name: "Account Test 4", deleted_at: Time.now) }

  let(:expected_match) { [trailer_vehicle_1, trailer_vehicle_3] }
  let(:args) { {} }

  shared_context 'find trailer vehicles as expected' do
    it "finds trailer vehicles as expected" do
      expect(service_call).to match_array expected_match
    end
  end

  context 'when a valid term is passed' do
    let(:term) { "test" }

    it_behaves_like 'find trailer vehicles as expected'
  end

  context 'when term is 1 char long' do
    let(:term) { 't' }

    it_behaves_like 'find trailer vehicles as expected'
  end
end
