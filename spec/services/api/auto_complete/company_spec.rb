# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::Company do
  it "raises an error if the term is too short" do
    expect(lambda {
      API::AutoComplete::Company.new("Company", "f")
    }).to raise_error(ArgumentError)
  end

  it "finds companies" do
    company_1 = create(:fleet_company, name: "Joe's Test Managed Fleet")
    company_2 = create(:fleet_in_house_company, name: "Joe's Test In House")
    company_3 = create(:rescue_company, name: "Joe's Test Provider")
    company_4 = create(:rescue_company, name: "Deleted Test Provider", deleted_at: Time.now)

    service = API::AutoComplete::Company.new("Company", "test")
    expect(service.call).to match_array [company_4, company_2, company_1, company_3]

    service = API::AutoComplete::Company.new("RescueCompany", "TEST")
    expect(service.call).to match_array [company_4, company_2, company_1, company_3]
  end
end
