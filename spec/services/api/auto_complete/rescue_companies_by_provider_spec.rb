# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::RescueCompaniesByProvider do
  let(:company) { create(:fleet_company) }

  it "raises an error if the term is too short" do
    expect(lambda {
      API::AutoComplete::RescueCompaniesByProvider.new(term: "f", api_company: company, limit: 5)
    }).to raise_error(ArgumentError)
  end

  it "returns a list of rescue companies with accounts for the searching company" do
    other_company = create(:fleet_company)

    rescue_company_1 = create(:rescue_company, name: "Jerry's Towing")
    rescue_company_1.accounts.create!(client_company: company)
    rescue_company_1.accounts.create!(client_company: other_company, name: "Other Account")

    rescue_company_2 = create(:rescue_company, name: "John's Towing")
    rescue_company_2.accounts.create!(client_company: other_company)

    rescue_company_3 = create(:rescue_company, name: "Joe's Towing")
    rescue_company_3.accounts.create!(client_company: company)

    rescue_company_4 = create(:rescue_company, name: "Floe's Towing", deleted_at: Time.now)
    rescue_company_4.accounts.create!(client_company: company)

    rescue_company_5 = create(:rescue_company, name: "Andy's Towing")
    rescue_company_5.accounts.create!(client_company: company, deleted_at: Time.now)

    rescue_company_6 = create(:rescue_company, name: "Ralph's Rescue")
    rescue_company_6.accounts.create!(client_company: company)

    rescue_company_7 = create(:rescue_company, name: "aa towing")
    rescue_company_7.accounts.create!(client_company: company)

    service = API::AutoComplete::RescueCompaniesByProvider.new(term: "tow", api_company: company, limit: 10)
    expect(service.call.map(&:id)).to eq [rescue_company_7.id, rescue_company_1.id, rescue_company_3.id]
    expect(service.call.map(&:name)).to eq ["aa towing", "Jerry's Towing", "Joe's Towing"]

    service = API::AutoComplete::RescueCompaniesByProvider.new(term: "tow", api_company: company, limit: 1)
    expect(service.call).to eq [rescue_company_7]

    service = API::AutoComplete::RescueCompaniesByProvider.new(term: "TOW", api_company: other_company, limit: 10)
    expect(service.call).to eq [rescue_company_1, rescue_company_2]
  end

  it "ensures the the names of the rescue companies are unique" do
    rescue_company_1 = create(:rescue_company, location: create(:location, city: "Chicago", state: "IL"))
    rescue_company_1.update_attribute(:name, "Joe's Towing")
    rescue_company_1.accounts.create!(client_company: company)

    rescue_company_2 = create(:rescue_company)
    rescue_company_2.update_attribute(:name, "Joe's Towing")
    rescue_company_2.accounts.create!(client_company: company)
    create(:site, company: rescue_company_2, location: create(:location, city: "Los Angeles", state: "CA"))

    rescue_company_3 = create(:rescue_company, location: create(:location, city: "Chicago", state: "IL"))
    rescue_company_3.update_attribute(:name, "Joe's Towing")
    rescue_company_3.accounts.create!(client_company: company)

    rescue_company_4 = create(:rescue_company, name: "Floe's Towing")
    rescue_company_4.accounts.create!(client_company: company)

    rescue_company_5 = create(:rescue_company)
    rescue_company_5.update_attribute(:name, "Joe's Towing")
    rescue_company_5.accounts.create!(client_company: company)

    service = API::AutoComplete::RescueCompaniesByProvider.new(term: "TOW", api_company: company, limit: 10)
    expect(service.call.map(&:name)).to match_array [
      "Floe's Towing",
      "Joe's Towing [##{rescue_company_5.id}]",
      "Joe's Towing [Chicago, IL] [##{rescue_company_1.id}]",
      "Joe's Towing [Chicago, IL] [##{rescue_company_3.id}]",
      "Joe's Towing [Los Angeles, CA]",
    ]
  end
end
