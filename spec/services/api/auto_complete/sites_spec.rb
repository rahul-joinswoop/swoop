# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::Sites do
  subject(:service_call) { API::AutoComplete::Sites.new(term: term, api_company: company, limit: 10, **args).call }

  let(:company) { create(:rescue_company) }
  let(:other_company) { create(:rescue_company) }
  let!(:site_1) { create(:site, company: company, name: "Site Test 1") }
  let!(:site_2) { create(:site, company: other_company, name: "Site Test 2") }
  let!(:site_3) { create(:site, company: company, name: "Site Test 3") }
  let!(:site_4) { create(:site, company: company, name: "Site Test 4", deleted_at: Time.now) }

  let(:expected_match) { [site_1, site_3] }
  let(:args) { {} }

  shared_context 'find sites as expected' do
    it "finds sites as expected" do
      expect(service_call).to match_array expected_match
    end
  end

  context 'when a valid term is passed' do
    let(:term) { "test" }

    it_behaves_like 'find sites as expected'
  end

  context 'when term is 1 char long' do
    let(:term) { 't' }

    it_behaves_like 'find sites as expected'
  end
end
