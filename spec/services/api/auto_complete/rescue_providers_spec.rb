# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::RescueProviders do
  let(:company) { create(:fleet_company) }

  it "raises an error if the term is too short" do
    expect(lambda {
      API::AutoComplete::RescueProviders.new(term: "f", api_company: company, limit: 5)
    }).to raise_error(ArgumentError)
  end

  it "matches rescue companies with service codes by name and site" do
    create(:fleet_company)

    provider_1 = create(:rescue_company, name: "Joe's Towing")
    provider_1.sites << create(:site, name: "HQ")
    provider_1.sites << create(:site, name: "Downton")
    rescue_provider_1a = create(:rescue_provider, company: company, provider: provider_1, site: provider_1.sites.first)
    rescue_provider_1b = create(:rescue_provider, company: company, provider: provider_1, site: provider_1.sites.last)

    provider_2 = create(:rescue_company, name: "John's Towing")
    provider_2.sites << create(:site, name: "Downtown")
    rescue_provider_2 = create(:rescue_provider, company: company, provider: provider_2, site: provider_2.sites.first)

    provider_3 = create(:rescue_company, name: "Mike's Towing")
    provider_3.sites << create(:site, name: "HQ")
    rescue_provider_3 = create(:rescue_provider, company: company, provider: provider_3, site: provider_3.sites.first)

    provider_4 = create(:rescue_company, name: "Joe's Old Towing", deleted_at: Time.now)
    provider_4.sites << create(:site, name: "HQ")
    create(:rescue_provider, company: company, provider: provider_4, site: provider_4.sites.first)

    autocomplete = API::AutoComplete::RescueProviders.new(term: "JO", api_company: company)
    expect(autocomplete.call).to match_array [rescue_provider_1a, rescue_provider_1b, rescue_provider_2]

    autocomplete = API::AutoComplete::RescueProviders.new(term: "JOe", api_company: company)
    expect(autocomplete.call).to match_array [rescue_provider_1a, rescue_provider_1b]

    autocomplete = API::AutoComplete::RescueProviders.new(term: "HQ", api_company: company)
    expect(autocomplete.call).to match_array [rescue_provider_1a, rescue_provider_3]

    autocomplete = API::AutoComplete::RescueProviders.new(term: "joe's towing h", api_company: company)
    expect(autocomplete.call).to match_array [rescue_provider_1a]
  end
end
