# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::LocationTypes do
  subject(:service_call) { API::AutoComplete::LocationTypes.new(term: term, api_company: company, limit: 10, **args).call }

  let!(:company) do
    c = create(:rescue_company)
    c.location_types << location_type_1
    c.location_types << location_type_2
    c.location_types << location_type_4
    c.save!
    c
  end

  let!(:other_company) do
    c = create(:rescue_company)
    c.location_types << location_type_3
    c.save!
    c
  end

  let!(:location_type_1) { create(:location_type, name: "Location Test 1") }
  let!(:location_type_2) { create(:location_type, name: "Location Test 2") }
  let!(:location_type_3) { create(:location_type, name: "Location Test 3") }
  let!(:location_type_4) { create(:location_type, name: "Location Test 4", deleted_at: Time.now) }

  let(:expected_match) { [location_type_1, location_type_2] }
  let(:args) { {} }

  shared_context 'find location_types as expected' do
    it "finds location_types as expected" do
      expect(service_call).to match_array expected_match
    end
  end

  context 'when a valid term is passed' do
    let(:term) { "test" }

    it_behaves_like 'find location_types as expected'
  end

  context 'when term is 1 char long' do
    let(:term) { 't' }

    it_behaves_like 'find location_types as expected'
  end
end
