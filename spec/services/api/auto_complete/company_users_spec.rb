# frozen_string_literal: true

require 'rails_helper'

describe API::AutoComplete::CompanyUsers do
  subject(:service_call) { API::AutoComplete::CompanyUsers.new(term, company_id: company.id, **args).call }

  let(:company) { create(:rescue_company) }
  let(:args) { {} }

  let!(:user1) { create(:user, full_name: "FirstName LastName", company: company) }
  let!(:user2) { create(:user, full_name: "firstname lastnamez", company: company) }
  let!(:user3) { create(:user, full_name: "firstnamez lastname", company: company) }
  let!(:user4) { create(:user, full_name: "firstnamez lastnamez", company: company, deleted_at: Time.now) }

  context "when searching for 'firstname lastname'" do
    let(:term) { 'firstname lastname' }

    it { is_expected.to match_array [user1, user2] }
  end

  context "when searching for 'firstname'" do
    let(:term) { 'firstname' }

    it { is_expected.to match_array [user1, user2, user3] }
  end

  context "when searching for 'lastname'" do
    let(:term) { 'lastname' }

    it { is_expected.to match_array [user1, user2, user3] }
  end

  context "when searching for 'e lastname'" do
    let(:term) { 'e lastname' }

    it { is_expected.to match_array [user1, user2] }
  end

  context "when searching for 'firstname l'" do
    let(:term) { 'firstname l' }

    it { is_expected.to match_array [user1, user2] }
  end

  context "when searching for 'me la'" do
    let(:term) { 'me la' }

    it { is_expected.to match_array [user1, user2] }
  end

  context "when searching for 'Firstnamez lastNAMEZ'" do
    let(:term) { 'Firstnamez lastNAMEZ' }

    it { is_expected.to be_empty }
  end

  context "when searching for 'mez la'" do
    let(:term) { 'mez la' }

    it { is_expected.to match_array [user3] }
  end

  context 'when term is 1 char long' do
    let(:term) { "a" }

    it { is_expected.to match_array [user1, user2, user3] }
  end
end
