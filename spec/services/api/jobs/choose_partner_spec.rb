# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::ChoosePartner do
  describe '#call' do
    subject(:execute_choose_partner) do
      described_class.new(job: job, api_user: api_user, choose_params: choose_params).call
    end

    let(:choose_params) do
      {
        rescue_provider_id: rescue_provider.id,
        dispatch_email: dispatch_email,
        toa: toa,
      }
    end

    let(:rescue_provider) { create(:rescue_provider, :default) }
    let(:dispatch_email) { nil }
    let(:toa) { nil }
    let(:job) { create(:fleet_in_house_job) }
    let(:api_user) { create(:user, :tesla) }

    before :each do
      allow(Auction::ManualAuctionService).to receive(:new).and_call_original
      allow_any_instance_of(Auction::ManualAuctionService).to receive(:call)

      allow(JobService::SwoopAssignRescueProviderService).to receive(:new).and_call_original
      allow_any_instance_of(JobService::SwoopAssignRescueProviderService).to receive(:call)

      allow(JobService::FleetInhouseAssignRescueProviderService).to receive(:new).and_call_original
      allow_any_instance_of(JobService::FleetInhouseAssignRescueProviderService).to receive(:call)

      allow(HandleToa).to receive(:new).and_call_original
      allow_any_instance_of(HandleToa).to receive(:call)

      allow_any_instance_of(described_class).to receive(:after_execution)
    end

    context 'when its a FleetManaged job' do
      let(:job) { create(:fleet_managed_job) }
      let(:api_user) { create(:user, :fleet_demo) }
      let(:auction) { create(:auction, job: job) }

      it 'sets job#last_touched_by with api_user' do
        allow(job).to receive(:last_touched_by=).with(api_user)
        expect(job).to receive(:last_touched_by=).with(api_user)

        execute_choose_partner
      end

      context 'when job#auction_live is true' do
        it 'calls Auction::ManualAuctionService' do
          allow(job).to receive(:auction_live).and_return(true)
          allow(job).to receive(:auction).and_return(auction)
          auction_service = Auction::ManualAuctionService.new(auction.id, rescue_provider.id)
          allow(Auction::ManualAuctionService).to receive(:new).and_return(auction_service)
          expect(auction_service).to receive(:call)

          execute_choose_partner
        end
      end

      context 'when job#auction_live is false' do
        let(:auction) { create(:auction) }

        it 'calls Auction::SwoopAssignRescueProviderService' do
          allow(job).to receive(:auction_live).and_return(false)
          allow(job).to receive(:auction).and_return(auction)
          job_service = JobService::SwoopAssignRescueProviderService.new(job, rescue_provider, create(:site))
          allow(JobService::SwoopAssignRescueProviderService).to receive(:new).and_return(job_service)
          expect(job_service).to receive(:call)

          execute_choose_partner
        end
      end

      context "with billing type" do
        let(:choose_params) do
          {
            rescue_provider_id: rescue_provider.id,
            dispatch_email: dispatch_email,
            toa: toa,
            billing_type: BillingType::VCC,
            vcc_amount: 55.50,
          }
        end

        it "adds billing info for the job" do
          execute_choose_partner
          expect(job.billing_info.billing_type).to eq BillingType::VCC
          expect(job.billing_info.vcc_amount).to eq 55.50
          expect(job.billing_info.rescue_company_id).to eq rescue_provider.provider_id
          expect(job.billing_info.agent).to eq api_user
        end
      end

      context "with goa amount" do
        let(:choose_params) do
          {
            rescue_provider_id: rescue_provider.id,
            dispatch_email: dispatch_email,
            toa: toa,
            billing_type: BillingType::VCC,
            goa_amount: 55.50,
          }
        end

        it "adds goa billing info" do
          execute_choose_partner
          expect(job.billing_info.billing_type).to eq BillingType::VCC
          expect(job.billing_info.goa_amount).to eq 55.50
          expect(job.billing_info.rescue_company_id).to eq rescue_provider.provider_id
          expect(job.billing_info.goa_agent).to eq api_user
        end
      end
    end

    context 'when its a Fleet InHouse job' do
      it 'calls JobService::FleetInhouseAssignRescueProviderService' do
        fleet_service = JobService::FleetInhouseAssignRescueProviderService.new(job, rescue_provider, create(:site))
        allow(JobService::FleetInhouseAssignRescueProviderService).to receive(:new).and_return(fleet_service)
        expect(fleet_service).to receive(:call)

        execute_choose_partner
      end
    end

    it 'calls HandleToa' do
      handle_toa = HandleToa.new(rescue_provider, api_user, job, toa)
      allow(HandleToa).to receive(:new).and_return(handle_toa)
      expect(handle_toa).to receive(:call)

      execute_choose_partner
    end
  end
end
