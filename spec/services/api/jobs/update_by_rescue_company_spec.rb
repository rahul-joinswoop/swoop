# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::UpdateByRescueCompany do
  let(:rescue_company) { create(:rescue_company, :finish_line, :with_sites) }

  let(:rescue_company_user) { create :user, :rescue }

  describe ".call" do
    subject(:updated_job) do
      mapped_params = API::MapJobParams.new(
        api_company: rescue_company,
        api_user: rescue_company_user,
        job: job,
        params: params
      ).call

      API::Jobs::UpdateByRescueCompany.new(
        company: rescue_company,
        api_user: rescue_company_user,
        job_id: job.id,
        job_history: {},
        job_storage: false,
        params: mapped_params
      ).call

      job.reload

      job
    end

    context "Managed" do
      before do
        stub_const 'ORIGINAL_PARTNER_NOTE', 'original job note'
        stub_const 'PARTNER_NOTE', 'a partner job_note'
      end

      let(:fleet_company) { create :fleet_managed_company }

      let(:job) do
        create :fleet_managed_job, {
          status: job_status,
          fleet_company: fleet_company,
          adjusted_created_at: Time.now,
          rescue_company: rescue_company,
        }
      end

      let(:params) do
        { partner_notes: PARTNER_NOTE }
      end

      let(:job_status) { :pending }

      it 'works' do
        expect { subject }.not_to raise_error
      end

      it 'stay with existing behaviour by default' do
        job[:partner_notes] = ORIGINAL_PARTNER_NOTE
        expect { updated_job }.to change(job, :partner_notes).from(
          ORIGINAL_PARTNER_NOTE
        ).to(PARTNER_NOTE)
      end

      context 'enable notes' do
        before do
          Flipper.enable(:notes)
        end

        after do
          Flipper.disable(:notes)
        end

        context 'partner_notes -> InternalPartnerNote' do
          before do
            job[:partner_notes] = ORIGINAL_PARTNER_NOTE
            job.save!
          end

          it 'migrates to partner_internal_notes' do
            expect { updated_job }.to change(job, :partner_notes).from(
              ORIGINAL_PARTNER_NOTE
            ).to(PARTNER_NOTE)

            notes = job.partner_internal_notes
            expect(notes.length).to eql(1)

            note = notes[0]
            expect(note.text).to eql(PARTNER_NOTE)
            expect(job[:partner_notes]).to eql(ORIGINAL_PARTNER_NOTE)
          end

          it 'updates job with existing partner_internal_notes' do
            job.partner_internal_notes.create!(text: 'existing')

            expect { updated_job }.to change(job, :partner_notes).from('existing').to(
              PARTNER_NOTE
            )
            expect(job[:partner_notes]).to eql(ORIGINAL_PARTNER_NOTE)
          end
        end

        context 'dispatch_notes -> PartnerDispatchNote' do
          before do
            job[:dispatch_notes] = ORIGINAL_PARTNER_NOTE
            job.save!
          end

          let(:params) do
            { dispatch_notes: PARTNER_NOTE }
          end

          it 'migrates to partner_internal_notes' do
            expect { updated_job }.to change(job, :dispatch_notes).from(
              ORIGINAL_PARTNER_NOTE
            ).to(PARTNER_NOTE)

            notes = job.partner_dispatch_notes
            expect(notes.length).to eql(1)

            note = notes[0]
            expect(note.text).to eql(PARTNER_NOTE)
            expect(job[:dispatch_notes]).to eql(ORIGINAL_PARTNER_NOTE)
          end

          it 'updates job with existing partner_dispatch_notes' do
            job.partner_dispatch_notes.create!(text: 'existing')

            expect { updated_job }.to change(job, :dispatch_notes).from('existing').to(
              PARTNER_NOTE
            )
            expect(job[:dispatch_notes]).to eql(ORIGINAL_PARTNER_NOTE)
          end
        end
      end

      context 'when job is assigned' do
        let(:job_status) { :assigned }

        context 'and BTA is set' do
          let(:params) do
            # 20.minutes should not trigger any special logic, so all good
            { bta: time_now + 20.minutes }
          end

          let(:time_now) { Time.now }

          it 'sets the job.eta' do
            expect(job.bta).to be_nil

            updated_job

            expect(job.bta).to be_present
          end

          it 'moves job.status to accepted' do
            expect { updated_job }.to change(job, :status).from(
              'assigned'
            ).to(Job::STATUS_ACCEPTED)
          end

          context 'and it is a FleetMotorClubJob' do
            let(:job) do
              create :fleet_motor_club_job, {
                status: job_status,
                fleet_company: fleet_company,
                adjusted_created_at: Time.now,
                rescue_company: rescue_company,
                issc_dispatch_request: issc_dispatch_request,
              }
            end

            let!(:account) do
              create(:account, company: rescue_company, client_company: fleet_company)
            end

            let(:issc_dispatch_request) do
              create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
            end

            let(:rescue_company) { create(:rescue_company) }
            let(:fleet_company) { create(:fleet_company, :ago) }

            context 'and the Dispatch Provider is ISSC' do
              let(:issc) { create(:ago_issc, company: rescue_company) }

              it 'schedules IsscDispatchResponseAccept worker' do
                expect(IsscDispatchResponseAccept).to receive(:perform_async).with(job.id)

                updated_job
              end

              it 'moves job.status to submitted' do
                expect { updated_job }.to change(job, :status).from(
                  'assigned'
                ).to(Job::STATUS_SUBMITTED)
              end

              it 'creates an AJS for submitted status' do
                ajs_submitted =
                  updated_job.audit_job_statuses.where(job_status_id: JobStatus::SUBMITTED).first

                expect(ajs_submitted).to be_present
                expect(ajs_submitted.user_id).to eq rescue_company_user.id
              end
            end

            context 'and the Dispatch Provider is RSC' do
              let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM) }

              it 'schedules Agero::Rsc::AcceptDispatchWorker worker' do
                expect(Agero::Rsc::AcceptDispatchWorker).to receive(:perform_async).with(job.id)

                updated_job
              end

              it 'moves job.status to submitted' do
                expect { updated_job }.to change(job, :status).from(
                  'assigned'
                ).to(Job::STATUS_SUBMITTED)
              end

              it 'creates an AJS for submitted status' do
                ajs_submitted =
                  updated_job.audit_job_statuses.where(job_status_id: JobStatus::SUBMITTED).first

                expect(ajs_submitted).to be_present
                expect(ajs_submitted.user_id).to eq rescue_company_user.id
              end
            end
          end
        end
      end

      context 'when job is accepted' do
        let(:job_status) { :accepted }

        let(:params) do
          {
            rescue_driver_id: rescue_driver_id,
            rescue_vehicle_id: rescue_vehicle_id,
          }
        end

        let(:rescue_driver_id) { nil }
        let(:rescue_vehicle_id) { nil }

        context 'and rescue_driver_id is passed' do
          let(:rescue_driver_id) { create(:user, :rescue, :driver_on_duty).id }

          it 'sets job.rescue_driver_id' do
            expect { updated_job }.to change(job, :rescue_driver_id).from(
              nil
            ).to(rescue_driver_id)
          end

          it 'moves job.status to dispatched' do
            expect { updated_job }.to change(job, :status).from(
              Job::STATUS_ACCEPTED
            ).to(Job::STATUS_DISPATCHED)
          end

          context 'but company is not live' do
            let(:rescue_company) { create(:rescue_company, :finish_line, :with_sites, live: false) }

            it 'still moves job.status to dispatched' do
              expect { updated_job }.to change(job, :status).from(
                Job::STATUS_ACCEPTED
              ).to(Job::STATUS_DISPATCHED)
            end
          end

          context 'and rescue_vehicle_id is passed' do
            let(:rescue_vehicle_id) do
              create(:rescue_vehicle_with_coords, company: rescue_company).id
            end

            it 'sets job.rescue_vehicle_id' do
              expect { updated_job }.to change(job, :rescue_vehicle_id).from(
                nil
              ).to(rescue_vehicle_id)
            end

            it 'moves job.status to dispatched' do
              expect { updated_job }.to change(job, :status).from(
                Job::STATUS_ACCEPTED
              ).to(Job::STATUS_DISPATCHED)
            end

            context 'but company is not live' do
              let(:rescue_company) { create(:rescue_company, :finish_line, :with_sites, live: false) }

              it 'still moves job.status to dispatched' do
                expect { updated_job }.to change(job, :status).from(
                  Job::STATUS_ACCEPTED
                ).to(Job::STATUS_DISPATCHED)
              end
            end
          end
        end
      end
    end
  end
end
