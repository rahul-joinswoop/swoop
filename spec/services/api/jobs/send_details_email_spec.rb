# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::SendDetailsEmail do
  let(:job) { create(:job, :completed) }
  let(:args) { { input: { to: "swoop@joinswoop.com,swoop@joinswoop", cc: "", job_id: job.id, user_id: job.user_id, company_id: 1 } } }

  describe ".call" do
    before { allow(Delayed::Job).to receive(:enqueue) }

    after { API::Jobs::SendDetailsEmail.call(args) }

    context "with invalid or blank cc/bcc" do
      it "removes `cc` from recipients hash" do
        expect(JobStatusEmail::Details).to receive(:new).with(include(recipients: hash_not_including(:cc, :bcc)))
      end

      it "removes `cc` and `bcc` from recipients hash" do
        args[:input][:bcc] = nil
        expect(JobStatusEmail::Details).to receive(:new).with(include(recipients: hash_not_including(:cc, :bcc)))
      end
    end

    context "with valid bcc" do
      it "keeps `bcc` and remove `cc` from recipients hash" do
        args[:input][:bcc] = "swoopuser@joinswoop.com"
        expect(JobStatusEmail::Details).to receive(:new).with(include(recipients: { to: anything, bcc: anything }))
      end
    end
  end
end
