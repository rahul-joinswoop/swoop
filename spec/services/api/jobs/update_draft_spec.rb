# frozen_string_literal: true

require 'rails_helper'

describe API::Jobs::UpdateDraft do
  subject(:service_call) do
    API::Jobs::UpdateDraft.new(
      job: job, params: params, api_user: api_user
    ).call

    job.reload
  end

  let(:job) { create(:rescue_job, status: Job::PREDRAFT, fleet_company: company) }
  let(:company) { create(:rescue_company) }
  let(:api_user) { create(:user, company: company) }

  context 'when only caller is passed' do
    let(:params) do
      { caller_attributes: { full_name: 'Caller Name' } }
    end

    it 'updates job with caller accordingly' do
      expect(job.caller).to be_nil

      service_call

      expect(job.caller.full_name).to eq 'Caller Name'
    end
  end
end
