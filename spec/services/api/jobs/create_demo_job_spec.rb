# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::CreateDemoJob do
  subject(:create_demo_job) do
    API::Jobs::CreateDemoJob.new(
      rescue_company_id: rescue_company.id,
      browser_platform: 'whatever',
      user_agent: 'whatever'
    ).call.job
  end

  let!(:rescue_company) { create(:rescue_company, live: rescue_company_live) }
  let!(:site) { create(:partner_site, name: 'HQ', company: rescue_company, always_open: site_always_open, location: site_location) }
  let(:site_location) { create(:location, address: "San Francisco, CA", exact: true, lat: 37.7749295, lng: -122.4194155, place_id: "ChIJIQBpAG2ahYAR_6128GcTUEo") }
  let(:site_always_open) { true }
  let(:rescue_company_live) { true }

  let!(:fleet_demo) { create :fleet_demo_company }

  # questions
  let!(:low_clearance) { create(:question, question: Question::LOW_CLEARANCE, with_answers: ['Yes']) }
  let!(:attempted_to_jump) { create(:question, question: Question::ANYONE_ATTEMPTED_TO_JUMP, with_answers: ['Yes']) }
  let!(:vehicle_stop) { create(:question, question: Question::VEHICLE_STOP_WHEN_RUNNING, with_answers: ['Yes']) }
  let!(:customer_with_vehicle) { create(:question, question: Question::CUSTOMER_WITH_VEHICLE, with_answers: ['Yes']) }
  let!(:keys_present) { create(:question, question: Question::KEYS_PRESENT, with_answers: ['Yes']) }

  it 'creates a job' do
    expect { create_demo_job }.to change(Job, :count).by(1)
  end

  describe 'Analytics' do
    it 'sends Analytics an event' do
      allow(Analytics).to receive(:track).and_call_original

      job = create_demo_job
      expect(Analytics).to have_received(:track).with(
        user_id: fleet_demo.users.first.to_ssid,
        event: 'Create Demo Job Click',
        properties: {
          category: 'Onboarding',
          job_id: job.id,
          company: fleet_demo.name,
        },
      )
    end
  end

  it 'sets the job attributes accordingly' do
    job = create_demo_job

    driver_vehicle = job.driver.vehicle
    expect(driver_vehicle.color).to eq "Red"
    expect(driver_vehicle.make).to eq "Tesla"
    expect(driver_vehicle.model).to eq "Model S"
    expect(driver_vehicle.vin).to eq "1HGCM82633A004352"
    expect(driver_vehicle.year).to eq 2016

    customer = job.customer
    expect(customer.full_name).to eq "Demo Customer"
    expect(customer.phone).to eq "+14158434288"

    service_location = job.service_location
    hq_site_location = site.location
    expect(service_location.address).to eq hq_site_location.address
    expect(service_location.exact).to eq hq_site_location.exact
    expect(service_location.lat).to eq hq_site_location.lat
    expect(service_location.lng).to eq hq_site_location.lng
    expect(service_location.place_id).to eq hq_site_location.place_id
    expect(service_location.location_type_id).to eq LocationType.find_by_name(LocationType::TOW_COMPANY).id

    expect(job.odometer).to eq 1337.0
    expect(job.send_sms_to_pickup).to be_falsey
    expect(job.service_code.name).to eq ServiceCode::BATTERY_JUMP
    expect(job.will_store).to be_falsey
    expect(job.status).to eq 'assigned'

    expect(job.text_eta_updates).to be_truthy
    expect(job.notes).to eq(
      'This is a demo job. You can accept it, assign a driver, and progress it through statuses to see how Swoop works!'
    )
    expect(job.symptom.name).to eq 'Dead battery'

    expect(job.question_results.map(&:question).map(&:question)).to match_array(
      ["Keys present?", "Customer with vehicle?", "Did the vehicle stop when running?", "Anyone attempted to jump it yet?", "Low clearance?"]
    )

    expect(job.question_results.map(&:answer).map(&:answer)).to match_array(["Yes", "Yes", "Yes", "Yes", "Yes"])
  end

  context 'when site is not always_open' do
    let(:site_always_open) { false }

    it 'forces job status to assigned' do
      job = create_demo_job

      expect(job.status).to eq 'assigned'
    end
  end

  context 'when rescue_company is not live' do
    let(:rescue_company_live) { false }

    it 'forces job status to assigned' do
      job = create_demo_job

      expect(job.status).to eq 'assigned'
    end
  end

  context 'when an Account does not exist between Fleet and Rescue' do
    it 'creates it' do
      expect { create_demo_job }.to change(Account, :count).by(1)
    end
  end

  context 'when an Account exists between Fleet and Rescue' do
    let!(:existent_account) do
      create(
        :account,
        name: 'Demo Fleet Company',
        company: rescue_company,
        client_company: fleet_demo,
        skip_account_setup: true,
      )
    end

    it 'does not duplicate it' do
      expect { create_demo_job }.not_to change(Account, :count)
    end
  end

  context 'when a RescueProvider does not exist between Fleet and Rescue' do
    it 'creates it' do
      expect { create_demo_job }.to change(RescueProvider, :count).by(1)
    end
  end

  context 'when a RescueProvider exists between Fleet and Rescue' do
    let!(:rescue_provider) { create(:rescue_provider, provider: rescue_company, company: fleet_demo, site: site) }

    it 'does not duplicate it' do
      expect { create_demo_job }.not_to change(RescueProvider, :count)
    end
  end

  context 'when Demo Fleet has no users' do
    it 'creates one to be used as the Fleet dispatcher' do
      expect { create_demo_job && fleet_demo.users.reload }.to change(fleet_demo.users, :count).by(1)
    end
  end

  context 'when Demo Fleet has users' do
    let!(:user) { create(:user, company: fleet_demo) }

    it 'does not duplicate it' do
      expect { create_demo_job && fleet_demo.users.reload }.not_to change(fleet_demo.users, :count)
    end
  end
end
