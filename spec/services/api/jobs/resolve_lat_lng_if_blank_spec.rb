# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::ResolveLatLngIfBlank do
  subject(:service_call) { API::Jobs::ResolveLatLngIfBlank.new(job_id: job_id).call }

  let(:job) do
    create(:fleet_motor_club_job, service_location: service_location, drop_location: drop_location)
  end

  let(:job_id) { job.id }

  let(:service_location) do
    create(
      :location,
      address: service_location_address,
      lat: service_location_lat,
      lng: service_location_lng
    )
  end

  let(:service_location_address) { '123, That st' }
  let(:service_location_lat) { nil }
  let(:service_location_lng) { nil }

  let(:drop_location) do
    create(
      :location,
      address: drop_location_address,
      lat: drop_location_lat,
      lng: drop_location_lng
    )
  end

  let(:drop_location_address) { '456, That st' }
  let(:drop_location_lat) { nil }
  let(:drop_location_lng) { nil }

  before do
    allow(LatLngResolver).to receive(:perform_async).and_return nil
  end

  context 'when service_location has an address' do
    context 'and it does not have lat or lng' do
      it 'schedules LatLngResolver with service_location' do
        expect(LatLngResolver).to receive(:perform_async).with(job.service_location.id).once

        service_call
      end
    end

    context 'and it has lat and lng' do
      let(:service_location_lat) { 123.4 }
      let(:service_location_lng) { 567.8 }

      it 'does not schedule LatLngResolver with service_location' do
        expect(LatLngResolver).not_to receive(:perform_async).with(job.service_location.id)

        service_call
      end
    end
  end

  context 'when service_location does not have an address' do
    let(:service_location_address) { nil }

    it 'does not schedule LatLngResolver with service_location' do
      expect(LatLngResolver).not_to receive(:perform_async).with(job.service_location.id)

      service_call
    end
  end

  context 'when drop_location has an address' do
    context 'and it does not have lat or lng' do
      it 'schedules LatLngResolver' do
        expect(LatLngResolver).to receive(:perform_async).with(job.drop_location.id).once

        service_call
      end
    end

    context 'and it has lat and lng' do
      let(:drop_location_lat) { 123.4 }
      let(:drop_location_lng) { 567.8 }

      it 'does not schedule LatLngResolver with service_location' do
        expect(LatLngResolver).not_to receive(:perform_async).with(job.drop_location.id)

        service_call
      end
    end
  end

  context 'when drop_location does not have an address' do
    let(:drop_location_address) { nil }

    it 'does not schedule LatLngResolver with service_location' do
      expect(LatLngResolver).not_to receive(:perform_async).with(job.drop_location.id)

      service_call
    end
  end

  context 'when job_id is blank' do
    let(:job_id) { nil }

    it 'raises ArgumentError' do
      expect { service_call }.to raise_error(ArgumentError)
    end
  end
end
