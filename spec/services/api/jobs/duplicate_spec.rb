# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::Duplicate do
  subject(:duplicate_job_service) do
    API::Jobs::Duplicate.new(
      current_job_id: current_job.id,
      api_company_id: api_company.id,
      api_user_id: api_user.id
    )
  end

  let(:create_estimate_double) { double }
  let(:duplicated_job) { duplicate_job_service.call.duplicated_job }

  context 'when api_company is SuperCompany' do
    let(:api_company) { create(:super_company) }
    let(:api_user)    { create(:user, :root) }
    let(:current_job) { create(:fleet_managed_job, service_code: service_code) }
    let(:service_code) { create(:service_code) }

    before :each do
      create(:audit_job_status, job_status: JobStatus.find_by_name('Initial'), job: current_job)
      create(:audit_job_status, job_status: JobStatus.find_by_name('Created'), job: current_job)
      allow(CreateOrUpdateEstimateForJob).to receive(:perform_async).and_return(create_estimate_double)
    end

    it 'creates a new job accordingly' do
      expect(duplicated_job.status).to eq(Job::STATUS_DRAFT)
      expect(duplicated_job.created_at).not_to eq(current_job.created_at)
    end

    it 'updates the adjusted_created_at date' do
      expect(duplicated_job.adjusted_created_at).not_to eq(current_job.adjusted_created_at)
    end

    it 'keeps job.fleet_company' do
      expect(duplicated_job.fleet_company_id).to eq(current_job.fleet_company.id)
    end

    it 'doesnt assign a rescue_company' do
      expect(current_job.rescue_company_id).not_to be_nil
      expect(duplicated_job.rescue_company_id).to be_nil
    end

    context 'when job does not have an estimate' do
      it 'triggers create estimate service' do
        expect(CreateOrUpdateEstimateForJob).to have_received(:perform_async).with(duplicated_job.id)
      end
    end

    context 'when service_code is nil' do
      let(:service_code) { nil }

      it 'does not raise error' do
        expect { duplicated_job }.not_to raise_error
      end
    end
  end

  context 'when api_company is FleetCompany' do
    let(:api_company) { create(:fleet_company, :tesla) }
    let(:api_user)    { create(:user, :tesla) }
    let(:current_job) do
      create(:fleet_in_house_job, fleet_company: api_company, service_code: service_code)
    end
    let(:service_code) { create(:service_code) }

    before :each do
      create(:audit_job_status, job_status: JobStatus.find_by_name('Initial'), job: current_job)
      create(:audit_job_status, job_status: JobStatus.find_by_name('Created'), job: current_job)
      allow(CreateOrUpdateEstimateForJob).to receive(:perform_async).and_return(create_estimate_double)
    end

    it 'creates a new job accordingly' do
      expect(duplicated_job.status).to eq(Job::STATUS_DRAFT)
      expect(duplicated_job.created_at).not_to eq(current_job.created_at)
    end

    it 'updates the adjusted_created_at date' do
      expect(duplicated_job.adjusted_created_at).not_to eq(current_job.adjusted_created_at)
    end

    it 'keeps job.fleet_company' do
      expect(duplicated_job.fleet_company_id).to eq(current_job.fleet_company.id)
    end

    it 'doesnt assign a rescue_company' do
      expect(current_job.rescue_company_id).not_to be_nil
      expect(duplicated_job.rescue_company_id).to be_nil
    end

    it "assign a user" do
      expect(current_job.user).not_to be_nil
      expect(duplicated_job.user).to eq api_user
    end

    context 'when job does not have an estimate' do
      it 'triggers create estimate service' do
        expect(CreateOrUpdateEstimateForJob).to have_received(:perform_async).with(duplicated_job.id)
      end
    end

    context 'when service_code is nil' do
      let(:service_code) { nil }

      it 'does not raise error' do
        expect { duplicated_job }.not_to raise_error
      end
    end
  end

  context 'when api_company is RescueCompany' do
    let(:api_company) { create(:rescue_company, :finish_line) }
    let(:api_user)    { create(:user, :rescue) }
    let(:current_job) do
      create(
        :rescue_job,
        fleet_company: api_company,
        rescue_company: api_company,
        service_code: service_code
      )
    end
    let(:service_code) { create(:service_code) }

    before :each do
      create(:audit_job_status, job_status: JobStatus.find_by_name('Initial'), job: current_job)
      create(:audit_job_status, job_status: JobStatus.find_by_name('Created'), job: current_job)
      allow(CreateOrUpdateEstimateForJob).to receive(:perform_async).and_return(create_estimate_double)
    end

    it 'creates a new job accordingly' do
      expect(duplicated_job.status).to eq(Job::STATUS_DRAFT)
      expect(duplicated_job.created_at).not_to eq(current_job.created_at)
    end

    it 'updates the adjusted_created_at date' do
      expect(duplicated_job.adjusted_created_at).not_to eq(current_job.adjusted_created_at)
    end

    it 'keeps job.fleet_company' do
      expect(duplicated_job.fleet_company_id).to eq(current_job.fleet_company.id)
    end

    it 'keeps the rescue_company' do
      expect(current_job.rescue_company_id).not_to be_nil
      expect(duplicated_job.rescue_company_id).to eq current_job.rescue_company_id
    end

    context 'when job does not have an estimate' do
      it 'triggers create estimate service' do
        expect(CreateOrUpdateEstimateForJob).to have_received(:perform_async).with(duplicated_job.id)
      end
    end

    context 'when service_code is nil' do
      let(:service_code) { nil }

      it 'does not raise error' do
        expect { duplicated_job }.not_to raise_error
      end
    end

    context 'and current_job is STORAGE' do
      let(:current_job) do
        create(
          :rescue_job,
          fleet_company: api_company,
          rescue_company: api_company,
          service_code: create(:service_code, name: ServiceCode::STORAGE)
        )
      end

      let(:released_to_user) { create(:user) }
      let(:stranded_vehicle_item) { create(:stranded_vehicle) }

      let!(:inventory_items) do
        create_list(
          :inventory_item,
          1,
          :with_vehicle,
          item_id: stranded_vehicle_item.id,
          job_id: current_job.id,
          released_at: Time.now,
          stored_at: Time.now,
          released_to_user_id: released_to_user.id
        )
      end

      it 'duplicates inventory_items as expected' do
        duplicated_inventory_item = duplicated_job.inventory_items.first

        expect(duplicated_inventory_item.item.vin).to eq inventory_items.first.item.vin
        expect(duplicated_inventory_item.released_at).to be_nil
        expect(duplicated_inventory_item.stored_at).to be_nil
        expect(duplicated_inventory_item.released_to_user_id).to be_nil
      end
    end
  end
end
