# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::StatusReasonTypes do
  subject(:service_call) do
    API::Jobs::StatusReasonTypes.new(
      job: job,
      job_status_id: job_status_id,
    ).call
  end

  let(:job) { create(:job, fleet_company: fleet_company, status: :pending, issc_dispatch_request: issc_dispatch_request) }
  let(:use_custom_job_status_reasons) { false }
  let(:fleet_company) { create :fleet_company, use_custom_job_status_reasons: use_custom_job_status_reasons }

  let(:issc_dispatch_request) { create(:issc_dispatch_request, issc: issc) }
  let(:issc) { nil }
  let(:standard_accepted_reasons) do
    JobStatusReasonType.standard_items.where(job_status_id: JobStatus::ACCEPTED)
  end
  let(:standard_rejected_reasons) do
    JobStatusReasonType.standard_items.where(job_status_id: JobStatus::REJECTED)
  end

  let(:accepted_none) { create(:job_status_reason_type, :accepted_none) }
  let(:accepted_traffic) { create(:job_status_reason_type, :accepted_traffic) }
  let(:accepted_event_in_area) { create(:job_status_reason_type, :accepted_event_in_area) }
  let(:accepted_construction) { create(:job_status_reason_type, :accepted_construction) }
  let(:accepted_rural_disablement) { create(:job_status_reason_type, :accepted_rural_disablement) }
  let(:accepted_service_provider_issue) { create(:job_status_reason_type, :accepted_service_provider_issue) }
  let(:accepted_weather) { create(:job_status_reason_type, :accepted_weather) }
  let(:accepted_other) { create(:job_status_reason_type, :accepted_other) }
  let(:rejected_tire_size) { create(:job_status_reason_type, :rejected_tire_size) }

  context 'with each JobStatus we have' do
    JobStatus.all.each do |job_status|
      let(:job_status_id) { job_status.id }

      it "does not throw error for #{job_status.name} status" do
        expect { service_call }.not_to raise_error
      end
    end
  end

  context 'when we pass it Accepted status' do
    let(:job_status_id) { JobStatus::ACCEPTED }

    let(:expected_result) do
      [
        accepted_none,
        accepted_event_in_area,
        accepted_construction,
        accepted_rural_disablement,
        accepted_service_provider_issue,
        accepted_other,
        accepted_traffic,
        accepted_weather,
      ]
    end

    it 'returns all accepted status reason types' do
      expect(service_call).to match_array expected_result
      expect(service_call.last).to eq(accepted_other)
    end

    context 'when job.fleet_company is a digital dispatcher' do
      before(:each) do
        CompanyJobStatusReasonType.init_by_client_id fleet_company.issc_client_id.to_sym
      end

      context 'and job issc_dispatcher is Agero' do
        let(:issc) { create(:ago_issc) }
        let(:expected_result) { [accepted_none, accepted_traffic, accepted_weather] }
        let(:fleet_company) { create :fleet_company, :agero }

        it 'returns expected results' do
          expect(service_call).to match_array expected_result
        end
      end

      context 'and job issc_dispatcher is Quest' do
        let(:issc) { create(:quest_issc) }
        let(:fleet_company) { create :fleet_company, :quest }

        let(:expected_result) do
          [
            accepted_construction,
            accepted_rural_disablement,
            accepted_service_provider_issue,
            accepted_other,
            accepted_traffic,
            accepted_weather,
          ]
        end

        it 'returns expected results' do
          expect(service_call).to match_array expected_result
          expect(service_call.last).to eq(accepted_other)
        end
      end
    end

    context 'when job.fleet_company.use_custom_job_status_reasons == true' do
      let(:use_custom_job_status_reasons) { true }

      context 'and it has associated custom job status reasons' do
        before do
          fleet_company.custom_job_status_reason_types << accepted_none
          fleet_company.custom_job_status_reason_types << accepted_traffic
          fleet_company.custom_job_status_reason_types << rejected_tire_size
        end

        it { is_expected.to match_array [accepted_none, accepted_traffic] }
      end

      context 'and it does not have associated custom job status reasons' do
        it { is_expected.to match_array standard_accepted_reasons }
      end
    end

    context 'when job.fleet_company.use_custom_job_status_reasons == false' do
      let(:use_custom_job_status_reasons) { false }

      it { is_expected.to match_array standard_accepted_reasons }

      context 'and it has associated custom job status reasons' do
        before do
          fleet_company.custom_job_status_reason_types << accepted_none
          fleet_company.custom_job_status_reason_types << accepted_traffic
          fleet_company.custom_job_status_reason_types << rejected_tire_size
        end

        it { is_expected.to match_array standard_accepted_reasons }
      end
    end
  end

  context 'when we pass it Rejected status' do
    let(:job_status_id) { create(:job_status, :rejected).id }

    context 'when job.fleet_company.use_custom_job_status_reasons == true' do
      let(:use_custom_job_status_reasons) { true }

      context 'and it has associated custom job status reasons' do
        before do
          fleet_company.custom_job_status_reason_types << accepted_none
          fleet_company.custom_job_status_reason_types << accepted_traffic
          fleet_company.custom_job_status_reason_types << rejected_tire_size
        end

        it { is_expected.to match_array [rejected_tire_size] }
      end

      context 'and it does not have associated custom job status reasons' do
        it { is_expected.to match_array standard_rejected_reasons }
      end
    end

    context 'when job.fleet_company.use_custom_job_status_reasons == false' do
      let(:use_custom_job_status_reasons) { false }

      it { is_expected.to match_array standard_rejected_reasons }

      context 'and it is a motor_club' do
        let(:fleet_company) { create(:fleet_managed_company, :ago, use_custom_job_status_reasons: use_custom_job_status_reasons) }

        it { is_expected.to be_empty }

        context 'and it has associated custom job status reasons' do
          before do
            fleet_company.custom_job_status_reason_types << accepted_none
            fleet_company.custom_job_status_reason_types << accepted_traffic
            fleet_company.custom_job_status_reason_types << rejected_tire_size
          end

          it { is_expected.to be_empty }
        end
      end

      context 'and it has associated custom job status reasons' do
        before do
          fleet_company.custom_job_status_reason_types << accepted_none
          fleet_company.custom_job_status_reason_types << accepted_traffic
          fleet_company.custom_job_status_reason_types << rejected_tire_size
        end

        it { is_expected.to match_array standard_rejected_reasons }
      end
    end
  end
end
