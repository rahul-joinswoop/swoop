# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::UpdateHistory do
  describe "#execute" do
    subject { API::Jobs::UpdateHistory.new(job: job, history: history) }

    let!(:job) { create(:rescue_job) }
    let!(:job_status) { create(:job_status, name: "Pending") }
    let!(:audit_job_status) do
      create(:audit_job_status, job_status: job_status, job: job)
    end

    let(:history) do
      {
        "Job Details Emailed" => job,
        "Pending" => job,
      }
    end

    it "skips history item if job status can't be found" do
      # Two history items, one method call
      expect_any_instance_of(UpdateLastUpdatedStatusOnJob).to receive(:call).once
      subject.call
    end
  end
end
