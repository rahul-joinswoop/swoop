# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::Update do
  before do
    stub_const "PLATFORM", "browser-platform"
  end

  describe ".call" do
    subject(:update_job) do
      API::Jobs::Update.new(
        job: job,
        api_user: rescue_company_user,
        browser_platform: PLATFORM
      ).call
    end

    let(:rescue_company) do
      create(:rescue_company, :finish_line, :with_sites)
    end

    let(:rescue_company_user) { create :user, :rescue }

    context "when Job is instance of FleetManagedJob" do
      let(:fleet_company) { create :fleet_managed_company }

      let!(:job) do
        create :fleet_managed_job, {
          status: job_status,
          fleet_company: fleet_company,
          adjusted_created_at: Time.now,
          rescue_company: rescue_company,
          rescue_driver_id: nil,
          rescue_vehicle_id: nil,
        }
      end

      context 'when job is accepted' do
        let(:job_status) { :accepted }

        context 'and a rescue_driver_id is associated' do
          subject(:job_instance) { Job.find(job.id) }

          let(:rescue_driver_id) { create(:user, :rescue, :driver_on_duty).id }

          it 'sets job.rescue_driver_id' do
            expect(job_instance.rescue_driver_id).to be_nil

            job.rescue_driver_id = rescue_driver_id

            update_job
            job_instance.reload

            expect(job_instance.rescue_driver_id).to eq rescue_driver_id
          end

          it 'moves job.status to dispatched' do
            expect(job_instance.status).to eq Job::STATUS_ACCEPTED

            job.rescue_driver_id = rescue_driver_id

            update_job
            job_instance.reload

            expect(job_instance.status).to eq Job::STATUS_DISPATCHED
          end

          it 'only calls process_auto_assign once' do
            expect(job).to receive(:process_auto_assign)

            job.rescue_driver_id = rescue_driver_id
            update_job
          end

          context 'and a rescue_vehicle_id is associated' do
            let(:rescue_vehicle_id) do
              create(:rescue_vehicle_with_coords, company: rescue_company).id
            end

            it 'sets job.rescue_vehicle_id' do
              expect(job_instance.rescue_vehicle_id).to be_nil

              job.rescue_vehicle_id = rescue_vehicle_id

              update_job
              job_instance.reload

              expect(job_instance.rescue_vehicle_id).to eq rescue_vehicle_id
            end

            it 'moves job.status to dispatched' do
              expect(job_instance.status).to eq Job::STATUS_ACCEPTED

              job.rescue_driver_id = rescue_driver_id
              job.rescue_vehicle_id = rescue_vehicle_id

              update_job
              job_instance.reload

              expect(job_instance.status).to eq Job::STATUS_DISPATCHED
            end
          end
        end
      end

      context 'towing' do
        let(:job_status) { :towing }

        it 'sets platform' do
          job.status = Job::STATUS_COMPLETED

          update_job
          job.reload

          expect(job.platform).to eql(PLATFORM)
        end
      end

      describe "location update SMS" do
        before :each do
          fleet_company.add_features_by_name([Feature::SMS_REQUEST_LOCATION])
          job.update!(get_location_sms: true)
        end

        context "not changing from draft" do
          let(:job_status) { :pending }

          it "sends a location update message if the location is needs updating" do
            expect(job).to receive(:send_get_location)
            subject
          end

          it "does not send a location update message if the message has already been sent" do
            job.update!(get_location_attempts: 1)
            expect(job).not_to receive(:send_get_location)
            subject
          end
        end

        context "changing from draft" do
          let(:job_status) { :draft }

          it "sends a location update message if text eta updates is false" do
            job.text_eta_updates = false
            expect(job).to receive(:send_get_location)
            expect(job).not_to receive(:send_confirmation_sms)
            subject
          end

          it "sends a location confirmation message if text eta updates is tru" do
            job.text_eta_updates = true
            expect(job).to receive(:send_confirmation_sms)
            expect(job).not_to receive(:send_get_location)
            subject
          end
        end
      end
    end

    context "when Job is instance of RescueJob" do
      let(:site) { create(:site) }
      let(:drop_location) do
        create(:location, :random, {
          site: site,
        })
      end

      let!(:job) do
        create :fleet_managed_job, {
          status: job_status,
          fleet_company: rescue_company,
          adjusted_created_at: Time.now,
          rescue_company: rescue_company,
          rescue_driver_id: nil,
          rescue_vehicle_id: nil,
          will_store: will_store,
          drop_location: drop_location,
        }
      end

      context 'when job is completed' do
        let(:job_status) { :completed }

        context 'dont store' do
          let(:will_store) { false }

          it 'does not add storage' do
            expect { subject && job.save! }.to avoid_changing(InventoryItem, :count)
          end
        end

        context 'will store' do
          let(:will_store) { true }

          it 'adds storage' do
            expect { subject && job.save! }.to change(InventoryItem, :count).from(0).to(1)
          end
        end
      end
    end
  end
end
