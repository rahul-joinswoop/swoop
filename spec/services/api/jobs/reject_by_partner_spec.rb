# frozen_string_literal: true

require 'rails_helper'

describe API::Jobs::RejectByPartner do
  subject(:service_call) do
    API::Jobs::RejectByPartner.new(
      job_id: job_id,
      api_company_id: rescue_company.id,
      api_user_id: rescue_company_user.id,
      reject_params: reject_params,
    ).call
  end

  let(:reject_params) do
    { reject_reason_id: reject_reason_id }
  end

  let(:job_id) { job.id }
  let!(:account) do
    create(:account, company: rescue_company, client_company: fleet_company)
  end
  let(:rescue_company) { create(:rescue_company) }
  let(:rescue_company_user) { create(:user, company: rescue_company) }

  before do
    allow(Job).to receive(:find_by!).with(id: job.id, rescue_company_id: rescue_company.id)
      .and_return(job)
    allow(job).to receive(:after_partner_reject)
    allow(job).to receive(:delete_invoice)
  end

  shared_examples 'a job rejected on Swoop' do
    it 'calls job.after_partner_reject' do
      expect(job).to receive(:after_partner_reject)

      service_call
    end

    it 'calls job.delete_invoice' do
      expect(job).to receive(:delete_invoice)

      service_call
    end

    it 'sets job.partner_dispatcher with given api_user_id' do
      job_from_service = service_call.job

      expect(job_from_service.partner_dispatcher_id).to eq rescue_company_user.id
    end

    it 'sets job.last_touched_by with given api_user_id' do
      job_from_service = service_call.job

      expect(job_from_service.last_touched_by_id).to eq rescue_company_user.id
    end

    it 'sets job.reject_reason_id' do
      job_from_service = service_call.job

      expect(job_from_service.reject_reason_id).to eq reject_reason_id
    end

    it 'sets job.status = rejected' do
      job_from_service = service_call.job

      expect(job_from_service.status).to eq Job::STATUS_REJECTED
    end
  end

  context 'when Job is FleetMotorClubJob' do
    let(:job) do
      create :fleet_motor_club_job, {
        status: job_status,
        fleet_company: fleet_company,
        adjusted_created_at: Time.now,
        rescue_company: rescue_company,
        issc_dispatch_request: issc_dispatch_request,
        updated_at: '2018-07-26 18:08:04',
      }
    end

    let(:issc_dispatch_request) do
      create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end

    # TODO move this inside ISSC or RSC context
    let(:issc) { create(:ago_issc, company: rescue_company, api_access_token: nil) }

    shared_examples 'a job to be rejected on Issc' do
      it 'schedules IsscDispatchResponseReject for the Job' do
        expect(IsscDispatchResponseReject).to receive(:perform_async).with(job.id)

        service_call
      end
    end

    context 'when job.status is assigned' do
      let(:job_status) { 'assigned' }

      context 'and fleet_company is Agero' do
        let(:fleet_company) { create(:fleet_company, :ago) }

        context 'and reject reason is PaymentIssue' do
          let(:reject_reason_id) { create(:job_reject_reason, text: 'PaymentIssue').id }

          it_behaves_like 'a job rejected on Swoop'
          it_behaves_like 'a job to be rejected on Issc'
        end

        context 'and reject reason is EquipmentNotAvailable' do
          let(:reject_reason_id) { create(:job_reject_reason, text: 'EquipmentNotAvailable').id }

          it_behaves_like 'a job rejected on Swoop'
          it_behaves_like 'a job to be rejected on Issc'
        end

        context 'and reject reason is OutOfArea' do
          let(:reject_reason_id) { create(:job_reject_reason, text: 'OutOfArea').id }

          it_behaves_like 'a job rejected on Swoop'
          it_behaves_like 'a job to be rejected on Issc'
        end

        context 'and reject reason is Other' do
          let(:reject_reason_id) { create(:job_reject_reason, text: 'Other').id }

          it_behaves_like 'a job rejected on Swoop'
          it_behaves_like 'a job to be rejected on Issc'
        end
      end
    end
  end

  context 'when job is a FleetManagedJob' do
    let(:job) do
      create :fleet_managed_job, {
        status: job_status,
        fleet_company: fleet_company,
        adjusted_created_at: Time.now,
        rescue_company: rescue_company,
      }
    end

    let(:fleet_company) { create(:fleet_company) }

    context 'when job.status is assigned' do
      let(:job_status) { 'assigned' }

      context 'and reject_reason has a reason associated' do
        let(:reject_reason_reason) { create(:reason, issue: issue) }
        let(:issue) { create(:issue, name: 'Missed ETA') }
        let(:reject_reason_id) do
          create(:job_reject_reason, text: 'Other', reason: reject_reason_reason).id
        end

        it_behaves_like 'a job rejected on Swoop'

        it 'adds one explanation to job.explanations' do
          expect { service_call }.to change(job.explanations, :count).by(1)
        end
      end
    end
  end
end
