# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::AddAuditJobStatus do
  subject(:add_audit_job_status) do
    API::Jobs::AddAuditJobStatus.call(
      input: {
        job_id: job.id,
        user_id: user.id,
        company_id: company.id,
        audit_job_status_attributes: {
          job_status_name: job_status_name,
          last_set_dttm: last_set_dttm,
        },
      }
    )

    job.audit_job_statuses.reload
  end

  let(:job) { create(:fleet_in_house_job) }
  let(:company) { create(:fleet_company, :tesla) }
  let(:user) { create(:user, :tesla) }
  let(:job_status_name) { "ON_SITE" }
  let(:last_set_dttm) { Time.current }

  before :each do
    allow_any_instance_of(API::Jobs::AddAuditJobStatus).to receive(:job).and_return(job)
    allow_any_instance_of(Job).to receive(:base_publish)
  end

  shared_context 'adds the audit_job_status successfully' do
    it "adds a new audit_job_status to the job" do
      expect(job.audit_job_statuses).to be_empty

      add_audit_job_status

      audit_job_status = job.audit_job_statuses.find_by(job_status_id: JobStatus::ON_SITE)

      expect(audit_job_status.job_status.name).to eq(JobStatus::ID_MAP[JobStatus::ON_SITE])
      expect(audit_job_status.last_set_dttm.strftime("%T")).to eq(last_set_dttm.strftime("%T"))
    end

    it "publishes job through WS" do
      expect_any_instance_of(Job).to receive(:base_publish)

      add_audit_job_status
    end
  end

  context "when audit_job_status does not exist" do
    it_behaves_like 'adds the audit_job_status successfully'

    context 'when JobStatus uses common string format' do
      let(:job_status_name) { "On Site" }

      it_behaves_like 'adds the audit_job_status successfully'
    end
  end

  shared_context 'does not duplicate audit_job_status' do
    it "does not add duplicated audit_job_status" do
      statuses_count = job.audit_job_statuses.count

      add_audit_job_status

      expect(job.audit_job_statuses.count).to eq statuses_count
    end
  end

  context "when audit_job_status already exists" do
    before :each do
      audit_job_statuses_attributes = {
        job_id: job.id,
        job_status_id: job_status.id,
      }

      job.audit_job_statuses.reload

      audit_job_status =
        AuditJobStatus.find_by(audit_job_statuses_attributes)

      if audit_job_status.blank?
        AuditJobStatus.create!(last_set_dttm: Time.current, **audit_job_statuses_attributes)
      end
    end

    let(:job_status) { create :job_status, :en_route }
    let(:job_status_name) { "EN_ROUTE" }

    it_behaves_like 'does not duplicate audit_job_status'

    context 'when JobStatus uses common string format' do
      let(:job_status_name) { "En Route" }

      it_behaves_like 'does not duplicate audit_job_status'
    end
  end
end
