# frozen_string_literal: true

require "rails_helper"

describe API::Jobs::CreateFleet do
  let(:fleet_user) { create :user, :fleet_demo, company: fleet_company }

  describe ".call" do
    subject do
      described_class.new(
        api_company: fleet_company,
        api_user: fleet_user,
        params: params,
        save_as_draft: save_as_draft,
      ).call
    end

    shared_examples "create fleet" do
      let(:invoice_service_double) do
        instance_double(InvoiceService::CreateOrUpdateFleetCustomerInvoiceIfNecessary)
      end

      before(:each) do
        allow(InvoiceService::CreateOrUpdateFleetCustomerInvoiceIfNecessary)
          .to receive(:new)
          .and_return(invoice_service_double)
      end

      shared_examples "it works" do
        it "works" do
          expect(invoice_service_double)
            .to receive(:call)
            .send(expected_status == 'draft' ? :once : :twice)

          expect { subject }
            .not_to raise_error

          expect(subject.job.status)
            .to eql(expected_status)
        end
      end

      context "with ActionController::Parameters" do
        let(:params) { ActionController::Parameters.new(super()) }

        it_behaves_like "it works"
      end

      context "review_sms" do
        let(:params) do
          {
            **super(),
            review_sms: false,
          }
        end

        it_behaves_like "it works"

        it "allows user to set review_sms=false" do
          expect(invoice_service_double)
            .to receive(:call)
            .send(expected_status == 'draft' ? :once : :twice)

          expect(subject.job.review_sms)
            .to eql(false)
        end
      end

      context 'has customer account' do
        before do
          Account.find_or_create_fleet_customer_account(fleet_company)
        end

        it_behaves_like "it works"
      end
    end

    shared_examples "creates draft fleet job" do
      let(:save_as_draft) { true }
      let(:params) { {} }
      let(:expected_status) { 'draft' }

      it_behaves_like "create fleet"
    end

    shared_examples "creates non-draft fleet job" do
      let(:save_as_draft) { false }
      let(:params) { { service: "Tow" } }
      let(:expected_status) { 'pending' }

      it_behaves_like "create fleet"
    end

    context "Managed" do
      let(:fleet_company) { create :fleet_managed_company }

      it_behaves_like "creates draft fleet job"
      it_behaves_like "creates non-draft fleet job"
    end

    context "In House" do
      let(:fleet_company) { create :fleet_in_house_company }

      it_behaves_like "creates draft fleet job"
      it_behaves_like "creates non-draft fleet job"
    end
  end
end
