# frozen_string_literal: true

require 'flipper'
require "rails_helper"

describe API::Jobs::CreateRescue do
  before do
    stub_const 'NOTE', 'a job note'
    stub_const 'PARTNER_NOTE', 'a partner note'
    stub_const 'PARTNER_DISPATCH_NOTE', 'a partner dispatch note'
  end

  let(:flt) do
    create(:rescue_company, :finish_line, :with_sites)
  end

  let(:flt_user) { create :user, :rescue }

  let(:params) do
    {
      service: "Tow",
    }
  end

  describe ".call" do
    subject { described_class.new(args).call }

    let(:job) { subject.job }

    let(:args) do
      {
        api_company: flt,
        api_user: flt_user,
        params: params,
      }
    end

    context "with ActionController::Parameters" do
      let(:params) { ActionController::Parameters.new(super()) }

      it "works" do
        expect { subject }.not_to raise_error
      end
    end

    context "Managed" do
      it "works" do
        expect { subject }.not_to raise_error
      end

      context "with save_as_draft: true" do
        let(:params) { {} }
        let(:args) { { **super(), save_as_draft: true } }

        it "creates a draft job" do
          expect(job.status).to eql("draft")
        end
      end

      context "with save_as_predraft: true" do
        let(:params) { {} }
        let(:args) { { **super(), save_as_predraft: true } }

        it "creates a predraft job" do
          expect(job.status).to eql("predraft")
        end
      end

      context 'adds partner note' do
        let(:params) do
          {
            service: "Tow",
            partner_notes: PARTNER_NOTE,
          }
        end

        it 'persists partner note' do
          expect(job.partner_notes).to eql(PARTNER_NOTE)
        end
      end

      context 'partner_internal_notes api adds partner job_note' do
        let(:params) do
          {
            service: "Tow",
            partner_internal_notes: [{ text: PARTNER_NOTE }],
          }
        end

        it 'persists partner internal note' do
          notes = job.partner_internal_notes
          expect(notes.length).to eql(1)
          note = notes[0]
          expect(note.text).to eql(PARTNER_NOTE)
          expect(note.company).to eql(flt)
          expect(note.user).to eql(flt_user)
          expect(job[:partner_notes]).to be_nil
        end
      end

      context 'uses existing partner_notes by default' do
        let(:params) do
          {
            service: "Tow",
            partner_notes: PARTNER_NOTE,
          }
        end

        before do
          Rails.logger.debug Flipper.adapter
        end

        it 'writes to original job.partner_notes' do
          expect(job.partner_internal_notes.length).to eql(0)
          expect(job.partner_notes).to eql(PARTNER_NOTE)
          expect(job[:partner_notes]).to eql(PARTNER_NOTE)
        end
      end

      context 'enable notes feature' do
        before do
          Flipper.enable(:notes)
        end

        def test_notes(job_actual, notes_actual, expected)
          expect(notes_actual.length).to eql(1)
          note = notes_actual[0]
          expect(note.text).to eql(expected)
          # expect(note.company).to eql(flt)
          # expect(note.user).to eql(flt_user)
          expect(job_actual).to eql(expected)
        end

        after do
          Flipper.disable(:notes)
        end

        context 'intercepts partner_notes' do
          let(:params) do
            {
              service: "Tow",
              partner_notes: PARTNER_NOTE,
            }
          end

          it 'writes to partner_internal_notes instead' do
            test_notes(job.partner_notes, job.partner_internal_notes, PARTNER_NOTE)
            expect(job[:partner_notes]).to be_nil
          end
        end

        context 'intercepts job notes' do
          let(:params) do
            {
              service: "Tow",
              notes: NOTE,
            }
          end

          it 'writes to PublicNote instead' do
            test_notes(job.notes, job.public_notes, NOTE)
            expect(job[:notes]).to be_nil
          end
        end

        context 'intercepts partner dispatch notes' do
          let(:params) do
            {
              service: "Tow",
              dispatch_notes: PARTNER_DISPATCH_NOTE,
            }
          end

          it 'writes to PartnerDispatchNote instead' do
            test_notes(job.dispatch_notes, job.partner_dispatch_notes, PARTNER_DISPATCH_NOTE)
            expect(job[:dispatch_notes]).to be_nil
          end
        end
      end
    end

    context 'job.rescue_driver' do
      context 'when api_user is driver only' do
        before do
          [:dispatcher, :admin].each { |role| flt_user.remove_role role }
          flt_user.add_role(:driver)
        end

        it 'assings api_user as job.rescue_driver' do
          expect(job.rescue_driver).to eq flt_user
        end

        it 'moves job.status to dispatched' do
          expect(job.status).to eq 'dispatched'
        end
      end

      context 'when api_user is not driver only' do
        before do
          flt_user.add_role(:driver)
          flt_user.add_role(:admin)
        end

        it 'does not assing a job.rescue_driver automatically' do
          expect(job.rescue_driver).to be_nil
        end

        it 'moves job.status to pending' do
          expect(job.status).to eq 'pending'
        end
      end

      context 'when api_user is nil' do
        let(:flt_user) { nil }

        it 'does not assing a job.rescue_driver automatically' do
          expect(job.rescue_driver).to be_nil
        end

        it 'moves job.status to pending' do
          expect(job.status).to eq 'pending'
        end
      end
    end
  end
end
