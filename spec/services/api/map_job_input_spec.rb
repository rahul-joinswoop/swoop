# frozen_string_literal: true

require "rails_helper"

describe API::MapJobInput do
  def fix_location(location)
    location
      .deep_transform_keys { |k| k.to_s.underscore.to_sym }
      .except(:location_type)
      .merge(location_type_id: location_type.id)
      .tap do |l|
      l[:place_id] = l.delete(:google_place_id) if l[:google_place_id]
    end
  end

  subject do
    API::MapJobInput.new(
      api_company: company,
      api_user: user,
      job: job,
      params: Utils::VariablesConverter.convert(params),
    ).call
  end

  # these all need to be created in the db
  let(:company) { user.company }
  let(:symptom) { create :symptom }
  let(:storage_type) { create :storage_type }
  let(:service_code) { create :service_code }
  let(:location_type) { create :location_type }
  let(:questions) { create_list(:question_with_answers, 3) }
  # ugh - our input and output all have variations on how they display this data,
  # stash away a copy here of what we actually use then assemble it below
  let(:questions_and_answers) do
    questions.map { |q| { question: q, answer: q.answers.sample } }
  end

  let(:extra) { Faker::Hipster.sentence }
  let(:answers) do
    questions_and_answers.map do |qa|
      { question: qa[:question].question, answer: qa[:answer].answer }
    end.tap { |a| a.first[:extra] = extra }
  end

  let(:question_results) do
    questions_and_answers.map do |qa|
      { question_id: qa[:question].id, answer_id: qa[:answer].id }
    end.tap { |a| a.first[:answer_info] = extra }
  end

  # these don't need to be in the db when we're creating a new job and maybe
  # don't need to be in the db if we're updating a job (need to think about this)
  # TODO - what if they are?
  let(:po_number) { Faker::Company.po_number }
  let(:ref_number) { SecureRandom.uuid }
  let(:caller) { create :graphql_caller_input_type }
  let(:customer) { create :graphql_customer_input_type }
  let(:dropoff_contact) { create :graphql_contact_input_type }
  let(:pickup_contact) { create :graphql_contact_input_type }
  let(:notes) { create :graphql_notes_input_type }
  let(:service_location) do
    create :graphql_location_input_type, locationType: location_type
  end
  let(:dropoff_location) do
    create :graphql_location_input_type, :with_google_place_id, locationType: location_type
  end
  let(:vehicle) { create :graphql_vehicle_input_type, :with_serial_number, :with_style, :with_tire_size, :with_unit_number }
  let(:texts) { create :graphql_texts_input_type }
  let(:service) do
    create :graphql_service_input_type,
           name: service_code,
           symptom: symptom,
           answers: answers,
           company: company,
           storageType: storage_type
  end

  shared_examples "it works" do
    it "does not error out and matches input" do
      expect(subject).to deep_include(expected_output)
    end
  end

  shared_examples "graphql job input mapper" do
    let(:status) { 'pending' }
    let(:save_as_draft) { false }
    let(:save_as_predraft) { false }
    let(:params) do
      {
        job: {
          caller: caller,
          status: status,
          customer: customer,
          notes: notes,
          service: service,
          vehicle: vehicle,
          location: {
            serviceLocation: service_location,
            dropoffLocation: dropoff_location,
            pickupContact: pickup_contact,
            dropoffContact: dropoff_contact,
          },
          texts: texts,
          po_number: po_number,
          ref_number: ref_number,
        },
      }
    end

    let(:expected_output) do
      {
        save_as_draft: save_as_draft,
        save_as_predraft: save_as_predraft,
        params: {
          caller: caller.deep_dup.tap { |c| c[:fullName] = c.delete(:name) if c.key?(:name) },
          send_sms_to_pickup: texts[:sendTextsToPickup],
          review_sms: texts[:sendReview],
          partner_sms_track_driver: texts[:sendDriverTracking],
          text_eta_updates: texts[:sendEtaUpdates],
          customer: customer.deep_dup.tap { |c| c[:fullName] = c.delete(:name) },
          symptom_id: symptom.id,
          will_store: service[:willStore],
          service_location: fix_location(service_location),
          drop_location: fix_location(dropoff_location),
          notes: notes[:customer],
          service_code_id: service_code.id,
          question_results: question_results,
          dropoff_contact: dropoff_contact.deep_dup.tap { |c| c[:fullName] = c.delete(:name) },
          pickup_contact: pickup_contact.deep_dup.tap { |c| c[:fullName] = c.delete(:name) },
          po_number: po_number,
          ref_number: ref_number,
          **vehicle,
        }.deep_transform_keys { |k| k.to_s.underscore.to_sym }
          .tap do |p|
            p[:params][:scheduled_for] = service[:scheduledFor] if service[:scheduledFor].present?
            p[:fleet_dispatcher_id] = user.id if user.present?
            p[:vehicle_type] = p.delete(:style)
          end,
      }.tap do |e|
        e[:api_user] = user if user.present?
        e[:api_company] = company if company.present?
        e[:params][company.internal_notes_field] = notes[:internal] if company.present?
      end
    end

    context "without a job" do
      let(:job) { nil }

      context "normal usage" do
        it_behaves_like "it works"
      end

      context "without a status" do
        # status will be populated on a addJob call but not on an editJob call
        let(:params) do
          {
            job: super().dig(:job).to_h.reject { |k, v| k == :status },
          }
        end

        it_behaves_like "it works"
      end

      context "save as draft" do
        let(:status) { Job::STATUS_DRAFT }
        let(:save_as_draft) { true }

        it_behaves_like "it works"
      end

      context "with a member_number" do
        let(:customer) { create :graphql_customer_input_type, :with_member_number, company: company }
        let(:member_number) { Faker::Bank.iban }
        let(:customer_model) { Customer.new(member_number: member_number, company_id: company.id) }
        let(:expected_output) do
          super().deeper_dup.tap do |o|
            o[:params][:customer].tap do |c|
              c.delete :member_number
              c[:customer] = customer_model
            end
          end
        end

        before(:each) do
          # two (identical) new records that haven't been saved yet are not equal
          # so we have this hack so our matchers work - we have other specs where
          # we actually do this creation so this is ok i think.
          allow(Customer).to receive(:new).and_return(customer_model)
        end

        it_behaves_like "it works"
      end
    end

    context "with a job" do
      let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }
      let(:expected_output) do
        {
          **super(),
          job: job,
        }
      end

      it_behaves_like "it works"

      context "without a status" do
        # status will be populated on a addJob call but not on an editJob call so
        let(:params) do
          {
            job: super().dig(:job).to_h.reject { |k, v| k == :status },
          }
        end

        it_behaves_like "it works"
      end

      context "assigned -> accepted with bidded eta" do
        let(:job) { super().tap { |j| j.assigned! } }
        let(:status) { Job::STATUS_ACCEPTED }
        let(:bta) { 90 }
        let(:params) { super().deeper_dup.tap { |p| p[:job][:eta] = { bidded: bta } } }
        let(:expected_output) { super().deeper_dup.tap { |e| e[:params][:bta] = bta } }

        it_behaves_like "it works"
      end

      context "with an existing caller" do
        let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company, caller: create(:user, :phone) }

        it_behaves_like "it works"

        context "with only a name" do
          let(:caller) { super().deep_dup.tap { |c| c.delete :phone } }
          let(:expected_output) { super().deeper_dup.tap { |e| e[:params][:caller][:id] = job.caller.id } }

          it_behaves_like "it works"
        end

        context "with only a phone" do
          let(:caller) { super().deep_dup.tap { |c| c.delete :name } }
          let(:expected_output) { super().deeper_dup.tap { |e| e[:params][:caller][:id] = job.caller.id } }

          it_behaves_like "it works"
        end
      end

      context "with bidded eta and current eta" do
        let(:bta) { Time.now + 30.minutes }
        let(:toa) { Time.now + 40.minutes }
        let(:params) { super().deeper_dup.tap { |p| p[:job][:eta] = { bidded: bta, current: toa } } }
        let(:expected_output) do
          super().deeper_dup.tap do |e|
            e[:params][:bta] = bta
            e[:params][:toa] = { latest: toa }
          end
        end

        it_behaves_like "it works"
      end

      context "with a draft job" do
        let(:job) { create :fleet_managed_job, status: :draft, fleet_company: company }
        let(:save_as_draft) { true }

        it_behaves_like "it works"

        describe "from a draft job to a pending job" do
          let(:save_as_draft) { false }

          let(:params) do
            {
              job: { status: 'pending' },
            }
          end

          let(:expected_output) do
            {
              save_as_draft: false,
              save_as_predraft: false,
              job: job,
            }
          end

          it_behaves_like "it works"
        end
      end
    end
  end

  context "when it's a FleetManaged company" do
    let(:user) { create :user, :fleet_demo }

    it_behaves_like "graphql job input mapper"
  end

  context "when it's a FleetInHouse company" do
    let(:user) { create :user, :tesla }

    it_behaves_like "graphql job input mapper"
  end

  context "when it's Swoop" do
    let(:user) { create :user, :root }

    it_behaves_like "graphql job input mapper"
  end

  describe "customer details" do
    let(:user) { create :user, :fleet_demo }
    let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }

    it "does nothing if there are no customer details" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {},
                                     ),)
      output = service.call
      expect(output[:params][:customer]).to eq nil
    end

    it "copies the customer fields that exist" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {
                                         customer: {
                                           full_name: "John Doe",
                                           email: "jdoe@example.com",
                                           phone: "+13125551111",
                                         },
                                       },
                                     ),)
      output = service.call
      customer = output[:params][:customer]
      expect(customer).to eq({ full_name: "John Doe", phone: "+13125551111" })
      expect(output[:params][:email]).to eq "jdoe@example.com"
    end

    it "adds the customer if there is a member number" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {
                                         customer: { member_number: "A1234" },
                                       },
                                     ),)
      output = service.call
      customer = output[:params][:customer]
      expect(customer[:member_number]).to eq("A1234")
      expect(customer[:customer].member_number).to eq("A1234")
      expect(customer[:customer].company).to eq(company)
    end
  end

  describe "class_type" do
    let(:user) { create :user, :fleet_demo }
    let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }

    it "does nothing if there is no class type" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {},
                                     ),)
      output = service.call
      expect(output[:params].include?(:invoice_vehicle_category_id)).to eq false
    end

    it "sets invoice_vehicle_category_id to the vehicle category id" do
      class_type = create(:vehicle_category, name: "Heavy Duty")
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {
                                         service: { class_type: class_type },
                                       },
                                     ),)
      output = service.call
      expect(output[:params][:invoice_vehicle_category_id]).to eq class_type.id
    end

    it "sets invoice_vehicle_category_id to nil if class type is nil" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {
                                         service: { class_type: nil },
                                       },
                                     ),)
      output = service.call
      expect(output[:params].include?(:invoice_vehicle_category_id)).to eq true
      expect(output[:params][:invoice_vehicle_category_id]).to eq nil
    end

    context "with class_type_name" do
      it "sets invoice_vehicle_category_id to the vehicle category id" do
        class_type = create(:vehicle_category, name: "Heavy Duty")
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: {
                                           service: { class_type_name: class_type },
                                         },
                                       ),)
        output = service.call
        expect(output[:params][:invoice_vehicle_category_id]).to eq class_type.id
      end

      it "sets invoice_vehicle_category_id to nil if class type is nil" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: {
                                           service: { class_type_name: nil },
                                         },
                                       ),)
        output = service.call
        expect(output[:params].include?(:invoice_vehicle_category_id)).to eq true
        expect(output[:params][:invoice_vehicle_category_id]).to eq nil
      end
    end
  end

  describe "storage_type_name" do
    let(:user) { create :user, :fleet_demo }
    let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }

    it "does nothing if there is no storage type" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {},
                                     ),)
      output = service.call
      expect(output[:params].include?(:storage_type_id)).to eq false
    end

    it "sets storage_type_id to the storage_type_id" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {
                                         service: { storage_type_name: storage_type },
                                       },
                                     ))
      output = service.call
      expect(output[:params][:storage_type_id]).to eq storage_type.id
    end
  end

  describe "payment_type" do
    let(:user) { create :user, :fleet_demo }
    let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }

    it "does nothing if there is no payment type" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: {},
                                     ),)
      output = service.call
      expect(output[:params].include?(:customer_type_id)).to eq false
    end

    it "sets customer_type_id to the payment type id" do
      payment_type = create(:customer_type, name: "Warranty")
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: { payment_type: payment_type },
                                     ),)
      output = service.call
      expect(output[:params][:customer_type_id]).to eq payment_type.id
    end

    it "sets customer_type_id to nil if payment_type is nil" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: { payment_type: nil },
                                     ),)
      output = service.call
      expect(output[:params].include?(:customer_type_id)).to eq true
      expect(output[:params][:customer_type_id]).to eq nil
    end

    context "with payment_type_name" do
      it "sets customer_type_id to the payment type id" do
        payment_type = create(:customer_type, name: "Warranty")
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { payment_type_name: payment_type },
                                       ),)
        output = service.call
        expect(output[:params][:customer_type_id]).to eq payment_type.id
      end

      it "sets customer_type_id to nil if payment_type is nil" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { payment_type_name: nil },
                                       ),)
        output = service.call
        expect(output[:params].include?(:customer_type_id)).to eq true
        expect(output[:params][:customer_type_id]).to eq nil
      end
    end
  end

  describe "client details" do
    let(:user) { create :user, :fleet_demo }
    let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }

    describe "department" do
      it "does nothing if there is no department" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { client: {} },
                                       ),)
        output = service.call
        expect(output[:params].include?(:department_id)).to eq false
      end

      it "sets department_id to the department id" do
        department = create(:department, company: company, name: "Maintenance")
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { client: { department: department } },
                                       ),)
        output = service.call
        expect(output[:params][:department_id]).to eq department.id
      end

      it "sets department_id to nil if department is nil" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { client: { department: nil } },
                                       ),)
        output = service.call
        expect(output[:params].include?(:department_id)).to eq true
        expect(output[:params][:department_id]).to eq nil
      end
    end

    describe "Site" do
      it "does nothing if there is no site" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { client: {} },
                                       ),)
        output = service.call
        expect(output[:params].include?(:fleet_site_id)).to eq false
      end

      it "sets site_id to the site id" do
        site = create(:site, company: company, name: "Maintenance")
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { client: { site: site } },
                                       ),)
        output = service.call
        expect(output[:params][:fleet_site_id]).to eq site.id
      end

      it "sets site_id to nil if site is nil" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { client: { site: nil } },
                                       ),)
        output = service.call
        expect(output[:params].include?(:fleet_site_id)).to eq true
        expect(output[:params][:fleet_site_id]).to eq nil
      end
    end
  end

  describe "Partner" do
    let(:user) { create :user, :fleet_demo }
    let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }

    describe "department" do
      let(:department) { create :department, company: company }

      it "does nothing if there is no department" do
        service = API::MapJobInput.new(api_company: company,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { department: {} } },
                                       ))
        output = service.call
        expect(output.dig(:params, :partner_department_id).to_h.key?(:id)).to be false
        expect(output.dig(:params, :partner_department_id)).to be nil
      end

      it "sets partner_department_id to the account id" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { department: { name: department.name } } },
                                       ))
        output = service.call
        expect(output.dig(:params, :partner_department_id)).to eq(department.id)
      end

      it "sets partner_department_id to nil if partner.department.name is nil" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { department: { name: nil } } },
                                       ))
        output = service.call
        expect(output[:params].key?(:partner_department_id)).to be true
        expect(output[:params][:partner_department_id]).to be nil
      end
    end

    describe "vehicle" do
      let(:vehicle) { create :rescue_vehicle, company: company }

      it "does nothing if there is no department" do
        service = API::MapJobInput.new(api_company: company,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { vehicle: {} } },
                                       ))
        output = service.call
        expect(output.dig(:params).to_h.key?(:rescue_vehicle_id)).to be false
        expect(output.dig(:params, :rescue_vehicle_id)).to be nil
      end

      it "sets rescue_vehicle_id to the vehicle id" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { vehicle: { name: vehicle.name } } },
                                       ))
        output = service.call
        expect(output.dig(:params, :rescue_vehicle_id)).to eq(vehicle.id)
      end

      it "sets rescue_vehicle_id to nil if partner.vehicle.name is nil" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { vehicle: { name: nil } } },
                                       ))
        output = service.call
        expect(output[:params].key?(:rescue_vehicle_id)).to be true
        expect(output[:params][:rescue_vehicle_id]).to be nil
      end
    end

    describe "trailer" do
      let(:trailer) { create :trailer_vehicle, company: company }

      it "does nothing if there is no trailer" do
        service = API::MapJobInput.new(api_company: company,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { trailer: {} } },
                                       ))
        output = service.call
        expect(output.dig(:params).to_h.key?(:trailer_vehicle_id)).to be false
        expect(output.dig(:params, :trailer_vehicle_id)).to be nil
      end

      it "sets trailer_vehicle_id to the vehicle id" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { trailer: { name: trailer.name } } },
                                       ))
        output = service.call
        expect(output.dig(:params, :trailer_vehicle_id)).to eq(trailer.id)
      end

      it "sets rescue_vehicle_id to nil if partner.vehicle.name is nil" do
        service = API::MapJobInput.new(api_company: company,
                                       api_user: user,
                                       job: job,
                                       params: Utils::VariablesConverter.convert(
                                         job: { partner: { trailer: { name: nil } } },
                                       ))
        output = service.call
        expect(output[:params].key?(:trailer_vehicle_id)).to be true
        expect(output[:params][:trailer_vehicle_id]).to be nil
      end
    end
  end

  describe "Account" do
    let(:user) { create :user, :fleet_demo }
    let(:job) { create :fleet_managed_job, status: :pending, fleet_company: company }
    let(:account) { create :account, company: company }

    it "does nothing if there is no department" do
      service = API::MapJobInput.new(api_company: company,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: { account: {} },
                                     ))
      output = service.call
      expect(output.dig(:params, :account).to_h.key?(:id)).to be false
      expect(output.dig(:params, :account, :id)).to be nil
    end

    it "sets account_id to the account id" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: { account: { name: account.name } },
                                     ))
      output = service.call
      expect(output.dig(:params, :account, :id)).to eq(account.id)
    end

    it "sets account_id to nil if account.name is nil" do
      service = API::MapJobInput.new(api_company: company,
                                     api_user: user,
                                     job: job,
                                     params: Utils::VariablesConverter.convert(
                                       job: { account: { name: nil } },
                                     ))
      output = service.call
      expect(output[:params][:account].key?(:id)).to be true
      expect(output[:params][:account][:id]).to be nil
    end
  end

  describe "fullName" do
    let(:user) { create :user, :fleet_demo }
    let(:job) { nil }
    let(:customer) { create :graphql_customer_input_type, :with_customer_full_name }
    let(:dropoff_contact) { create :graphql_contact_input_type, :with_full_name }
    let(:pickup_contact) { create :graphql_contact_input_type, :with_full_name }
    let(:params) do
      {
        job: {
          customer: customer,
          location: {
            pickupContact: pickup_contact,
            dropoffContact: dropoff_contact,
          },
        },
      }
    end
    let(:expected_output) do
      {
        params: {
          customer: customer.deep_dup.tap { |c| c.delete :name },
          dropoff_contact: dropoff_contact.deep_dup.tap { |c| c.delete :name },
          pickup_contact: pickup_contact.deep_dup.tap { |c| c.delete :name },
        }.deep_transform_keys { |k| k.to_s.underscore.to_sym },
      }
    end

    it_behaves_like "it works"
  end
end
