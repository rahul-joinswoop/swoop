# frozen_string_literal: true

require "rails_helper"

describe API::MapJobParams do
  subject do
    API::MapJobParams.new(api_company: company,
                          api_user: user,
                          job: job,
                          params: params).call
  end

  let(:user) { create(:user, company: company) }
  let(:job) { create(:rescue_job, rescue_company: company) }

  context "when company is not live and has rescue driver/vehicle" do
    let(:company) { create(:rescue_company, live: false) }
    let(:params) do
      {
        id: job.id,
        rescue_driver_id: user.id,
        rescue_vehicle_id: vehicle.id,
      }
    end

    let!(:vehicle) { create(:vehicle, company: company) }

    it "still maps rescue driver / vehicle successfully" do
      expect(subject).to eq(
        rescue_driver_id: user.id, rescue_vehicle_id: vehicle.id
      )
    end
  end
end
