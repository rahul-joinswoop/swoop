# frozen_string_literal: true

require "rails_helper"

describe StorageTick do
  let(:rates_attributes) do
    [
      {
        flat: "95.00",
        vehicle_category_id: "6",
        service_code_id: nil,
        account_id: 4,
        type: "FlatRate",
      },
    ]
  end

  let(:company) { double "Company" }
  let(:account) { double "Account" }

  let!(:storage_service_code) { create(:service_code, name: "Storage") }

  let!(:rate) do
    create(:storage_rate, {
      account: account,
      service_code: storage_service_code,
      company: rescue_company,
      site: nil,
    })
  end

  let(:rescue_company) { create(:rescue_company) }

  let(:fleet_company) { create(:fleet_company) }

  let(:site) { create(:site) }

  let(:location) do
    create(:location, site: site)
  end

  let(:account) { create(:account) }

  let!(:job) do
    create(:rescue_job, {
      fleet_company: rescue_company,
      rescue_company: rescue_company,
      status: Job::COMPLETED,
      drop_location: location,
      account: account,
    })
  end

  let!(:partner_invoice) do
    Invoice.new(
      job: job,
      sender: rescue_company,
      recipient: fleet_company,
      currency: rescue_company.currency
    )
  end

  let(:user) { create(:user, :rescue) }
  let!(:inventory_item) do
    create(:inventory_item, :with_vehicle, :not_released, job: job)
  end

  before do
    job.invoice = partner_invoice
    job.save!

    job.audit_job_statuses << create(:audit_job_status,
                                     job_status: create(:job_status, :stored),
                                     job: job)

    job.audit_job_statuses << create(:audit_job_status,
                                     job_status: create(:job_status, :released),
                                     job: job)
    job.reload

    dump_table(Rate)
    dump_table(ServiceCode)
    dump_table(RateAddition)
  end

  describe "Rate Addtions" do
    before do
      stub_const "FLAT_RATE_ADDITION_NAME", "Rate Addition kjrfurh"
      stub_const "FLAT_RATE_ADDITION_AMOUNT", 87.77
    end

    let!(:flat_rate_addition) do
      create(:rate_addition, {
        name: FLAT_RATE_ADDITION_NAME,
        rate: rate,
        amount: FLAT_RATE_ADDITION_AMOUNT,
      })
    end

    it "Add additional rate items" do
      Rails.logger.debug "Calling StorageTick with #{job}"

      expect(job.invoice.line_items.length).to eq(0)

      StorageTick.new(job).call

      Rails.logger.debug "Job li's: #{job.invoice.line_items.inspect}"
      expect(job.invoice.line_items.length).to eq(2)

      addition = job.invoice.find_lineitem_by_job_and_description(job, FLAT_RATE_ADDITION_NAME)
      expect(addition.net_amount).to eq(FLAT_RATE_ADDITION_AMOUNT)
    end

    it "is idempotent" do
      StorageTick.new(job).call
      StorageTick.new(job).call

      expect(job.invoice.line_items.length).to eq(2)

      addition = job.invoice.find_lineitem_by_job_and_description(job, FLAT_RATE_ADDITION_NAME)
      expect(addition.net_amount).to eq(FLAT_RATE_ADDITION_AMOUNT)
    end
  end

  it "Doesn't add additions if they dont exist" do
    StorageTick.new(job).call

    expect(job.invoice.line_items.length).to eq(1)
  end
end
