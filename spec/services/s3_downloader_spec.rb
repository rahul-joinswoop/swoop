# frozen_string_literal: true

require 'rails_helper'

describe Service::S3Downloader do
  subject { described_class.new }

  let(:data) { '{"type": "FeatureCollection", "features": [{"type": "Feature", "properties": {"STATEFP10": "33", "ZCTA5CE10": "03752", "GEOID10": "3303752", "CLASSFP10": "B5", "MTFCC10": "G6350", "FUNCSTAT10": "S", "ALAND10": 58230518, "AWATER10": 238931, "INTPTLAT10": "+43.2722954", "INTPTLON10": "-072.1004854", "PARTFLG10": "N"}, "geometry": {"type": "Polygon", "coordinates": [[[-72.153388, 43.277677], [-72.15349, 43.278543], [-72.153906, 43.281623], [-72.159651, 43.280907], [-72.160076, 43.282892], [-72.161057, 43.287475], [-72.16261, 43.294728], [-72.163962, 43.301211], [-72.165513, 43.308276], [-72.162596, 43.307617], [-72.161616, 43.309389], [-72.16107, 43.310402], [-72.160845, 43.310791], [-72.158916, 43.314395], [-72.157153, 43.314019], [-72.155795, 43.313668], [-72.123421, 43.249397], [-72.124529, 43.249539], [-72.130962, 43.249569], [-72.149919, 43.249682], [-72.150377, 43.253666], [-72.151411, 43.262094], [-72.151639, 43.263505], [-72.151753, 43.264403], [-72.151892, 43.265537], [-72.153312, 43.277042], [-72.153388, 43.277677]]]}}]}' }
  let(:fake_s3) { {} }
  let(:client) do
    client = Aws::S3::Client.new(stub_responses: true)
    client.stub_responses(
      :create_bucket, ->(context) {
        name = context.params[:bucket]
        if fake_s3[name]
          'BucketAlreadyExists'
        else
          fake_s3[name] = {}
        end
      }
    )
    client.stub_responses(
      :list_objects, ->(context) {
        bucket = context.params[:bucket]
        bucket_contents = fake_s3[bucket]
        if bucket_contents
          obj = { key: bucket_contents.keys.first }
          { contents: [obj] }
        else
          'NoSuchBucket'
        end
      }
    )
    client.stub_responses(
      :get_object, ->(context) {
        bucket = context.params[:bucket]
        key = context.params[:key]
        bucket_contents = fake_s3[bucket]
        if bucket_contents
          obj = bucket_contents[key]
          if obj
            { body: obj }
          else
            'NoSuchKey'
          end
        else
          'NoSuchBucket'
        end
      }
    )
    client.stub_responses(
      :put_object, ->(context) {
        bucket = context.params[:bucket]
        key = context.params[:key]
        body = context.params[:body]
        bucket_contents = fake_s3[bucket]
        if bucket_contents
          bucket_contents[key] = body
          {}
        else
          'NoSuchBucket'
        end
      }
    )
    client
  end

  context "bucket operations" do
    before do
      client.create_bucket(bucket: "geojson_data")
    end

    it "can write and retrieve an object" do
      client.put_object(bucket: "geojson_data", key: "geojson_data/nh_new_hampshire_zip_codes_geo.min.json", body: data)
      obj = client.get_object(bucket: "geojson_data", key: "geojson_data/nh_new_hampshire_zip_codes_geo.min.json")
      expect(obj.body.read).to eq(data)
    end

    it "can list all objects" do
      client.put_object(bucket: "geojson_data", key: "geojson_data/test.min.json", body: data)
      keys = client.list_objects({ bucket: "geojson_data", prefix: "geojson_data" })[:contents].collect(&:key)
      expect(keys).to be_kind_of(Array)
      expect(keys).to have(1).items
      expect(keys).to include("geojson_data/test.min.json")
      expect(keys).not_to include("geojson_data/nh_new_hampshire_zip_codes_geo.min.json")
    end

    it "raises the appropriate exception when a bucket doesn't exist" do
      expect do
        client.put_object(bucket: "geojson_data_dummy", key: "somerandom.mini.json", body: "Invalid String")
      end.to raise_error(Aws::S3::Errors::NoSuchBucket)
      expect(client.api_requests.size).to eq(2)
    end

    it "raises the appropriate exception when an object doesn't exist" do
      expect do
        client.get_object(bucket: "geojson_data", key: "404NoSuchKey")
      end.to raise_error(Aws::S3::Errors::NoSuchKey)
      expect(client.api_requests.size).to eq(2)
    end
  end
end
