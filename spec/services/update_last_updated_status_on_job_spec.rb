# frozen_string_literal: true

require "rails_helper"

describe UpdateLastUpdatedStatusOnJob do
  let(:service) { described_class.new(job, audit_job_status, previous) }
  let(:job) do
    FactoryBot.build_stubbed(:job, last_status_changed_at: 7.days.ago)
  end
  let(:previous) { 3.days.ago }

  describe '.call' do
    subject { service.call }

    context 'when audit_job_status has adjusted datetime' do
      let(:audit_job_status) do
        FactoryBot.build_stubbed(:audit_job_status, adjusted_dttm: 3.days.ago)
      end

      context 'and conditions are met' do
        it 'updates last_status_changed_at with adjusted_dttm' do
          expect { subject }.to change(job, :last_status_changed_at)
            .to(audit_job_status.adjusted_dttm)
        end
      end

      context 'and conditions are not met' do
        let(:job) do
          FactoryBot.build_stubbed(:job, last_status_changed_at: 30.minutes.ago)
        end

        it 'does nothing' do
          expect { subject }.to avoid_changing(job, :last_status_changed_at)
        end
      end
    end

    context 'when job status is deleted' do
      let(:job_status) { FactoryBot.build_stubbed(:job_status, :deleted) }
      let(:audit_job_status) do
        FactoryBot.build_stubbed(:audit_job_status,
                                 adjusted_dttm: 3.days.ago,
                                 job_status: job_status)
      end

      it 'does not change last_status_changed_at' do
        expect { subject }.to avoid_changing(job, :last_status_changed_at)
      end
    end

    context 'when audit_job_status lacks adjusted date time' do
      let(:audit_job_status) do
        FactoryBot.build_stubbed(:audit_job_status,
                                 adjusted_dttm: nil,
                                 last_set_dttm: 3.days.ago)
      end

      context 'and conditions are met' do
        it 'updates last_status_changed_at with last_set_dttm' do
          expect { subject }.to change(job, :last_status_changed_at)
            .to(audit_job_status.last_set_dttm)
        end
      end

      context 'and conditions are not met' do
        let(:job) do
          FactoryBot.build_stubbed(:job, last_status_changed_at: 30.minutes.ago)
        end

        it 'does nothing' do
          expect { subject }.to avoid_changing(job, :last_status_changed_at)
        end
      end
    end
  end
end
