# frozen_string_literal: true

require "rails_helper"

describe AssignRescueDriverVehicleToJob do
  subject { described_class.call job: job }

  context 'with a valid job' do
    let(:job) do
      j = create :job, :with_rescue_vehicle
      j.update! rescue_vehicle: nil
      j
    end

    it "works" do
      expect(subject).to be_success
    end
  end

  context 'with an invalid job' do
    let(:job) { nil }

    it "works" do
      expect(subject).to be_failure
    end
  end
end
