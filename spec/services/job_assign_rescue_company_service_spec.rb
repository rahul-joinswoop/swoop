# frozen_string_literal: true

require "rails_helper"

describe JobAssignRescueCompanyService do
  let!(:swoop_company) { create(:super_company) }
  let!(:rescue_vehicle) do
    rv = create(:rescue_vehicle_with_coords,
                dedicated_to_swoop: true,
                company: rescue_company,
                lat: 37.778511,
                lng: -122.478831,
                driver: build(:user, :driver_on_duty),
                location_updated_at: Time.now)
    rescue_company.update! auto_truck: rv
    rv
  end
  let!(:rescue_company) do
    rc = create(:rescue_company, live: true)
    rc.sites << create(:partner_site, company: rc)
    rc.auto_driver = create(:user)
    rc.save!
    rc
  end
  let!(:fleet_company) do
    create(:fleet_company,
           features: [
             create(:feature, {
               name: Feature::AUTO_ASSIGN,
             }),
           ])
  end
  # where tow happens
  let!(:service_location) { create(:location, lat: 37.781745, lng: -122.490367) }
  let(:service) { JobAssignRescueCompanyService.new(job, rescue_company.sites[0]) }
  let(:job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
      service_location: service_location,
      status: "pending",
    })
  end

  describe "#call" do
    subject { service.call }

    it "sets partner_live to true" do
      expect { subject }.to change(job, :partner_live?).from(false).to(true)
    end

    it "sets account" do
      expect(job.account).to be_nil
      subject
      expect(job.account).to be_present
    end

    it "sets company_assigned to true" do
      expect(job.company_assigned).to be_falsey
      subject
      expect(job.company_assigned).to be_truthy
    end

    context "rescue company has AUTO_ASSIGN_DRIVER feature" do
      let(:rescue_company) do
        rc = super()
        rc.features << create(:feature, { name: Feature::AUTO_ASSIGN_DRIVER })
        rc.save!
        rc
      end

      before { subject }

      it "has rescue_driver" do
        expect(job.rescue_driver).to be_present
      end

      it "has rescue_vehicle" do
        expect(job.rescue_vehicle).to be_present
      end

      it "has service assigned driver" do
        expect(service.instance_variable_get(:@is_assigning_driver)).to be true
      end

      it "has status changed to dispatched" do
        # status is lowercase
        expect(job.status.downcase).to be_eql(Job::STATUS_DISPATCHED)
      end
    end

    context "rescue company has not AUTO_ASSIGN_DRIVER feature" do
      it "has rescue_driver" do
        expect(job.rescue_driver).not_to be_present
      end

      it "has rescue_vehicle" do
        expect(job.rescue_vehicle).not_to be_present
      end
    end

    context "rescue company has DEPARTMENTS feature" do
      let(:rescue_company) do
        rc = super()
        rc.features << create(:feature, { name: Feature::DEPARTMENTS })
        rc.save!
        rc
      end

      let!(:account) do
        # job.fleet_managed_job? is true so Account will be assigned to Swoop
        create(:account, {
          client_company: Company.swoop, # see comment above
          company: rescue_company,
          department: create(:department),
        })
      end

      before { subject }

      it "set partner_department" do
        expect(job.partner_department).to be_present
      end
    end

    context "rescue_company is live and has phone" do
      let(:rescue_company) do
        rc = super()
        rc.live = true
        rc.phone = "1-234-567-890"
        rc.save!
        rc
      end

      before { subject }

      it "execute call and set call result" do
        expect(service.call_result).to be true
      end
    end

    context "rescue_company is live and has NO phone - error is logged" do
      let(:rescue_company) do
        rc = super()
        rc.live = true
        rc.phone = nil
        rc.sites[0].phone = nil
        rc.sites[0].save!
        rc.save!
        rc
      end

      before { subject }

      it "execute call and set call result" do
        expect(service.call_result).to be false
      end
    end

    context "rescue_company is NOT live and has phone" do
      let(:rescue_company) do
        rc = super()
        rc.live = false
        rc.phone = "1-234-567-890"
        rc.save!
        rc
      end

      before { subject }

      it "execute call and set call result" do
        expect(service.call_result).to be_falsey
      end
    end

    context "rescue_company is NOT live and has phone" do
      let(:rescue_company) do
        rc = super()
        rc.live = false
        rc.phone = "1-234-567-890"
        rc.save!
        rc
      end

      before { subject }

      it "execute call and set call result" do
        expect(service.call_result).to be_falsey
      end
    end

    context "when Job#partner_live_and_open?" do
      let(:site) { create(:partner_site, force_open: true, company: rescue_company) }
      let(:service) { JobAssignRescueCompanyService.new(job, site) }

      let(:rescue_company) do
        rc = super()
        rc.live = true
        rc.phone = "1-234-567-890"
        rc.save!
        job.save!
        rc
      end

      it "has Job#partner_live_and_open?" do
        expect { subject }.to change(job, :partner_live_and_open?).from(false).to(true)
      end

      it "execute call and set call result to true" do
        subject
        expect(service.call_result).to be true
      end
    end

    context "when Job is not in fleet_inhouse_job, fleet_motor_club_job, fleet_managed_job" do
      let(:service) { JobAssignRescueCompanyService.new(job, rescue_company.sites[0]) }
      let(:job) do
        j = super()
        j.type = "Job"
        j.save!
        Job.find(job.id)
      end

      it "raise ArgumentError exception" do
        expect { service.call }.to raise_error(ArgumentError)
      end
    end
  end
end
