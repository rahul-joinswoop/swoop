# frozen_string_literal: true

require "rails_helper"

describe CreateEstimatesForJobCandidatesService do
  subject do
    ::CreateEstimatesForJobCandidatesService.new(
      candidates: candidates,
      auction: auction,
      job: fleet_managed_job
    ).call
  end

  let(:fleet_company) { create(:fleet_company) }
  let(:fleet_managed_job) { create(:fleet_managed_job, fleet_company: fleet_company, service_location: service_location, drop_location: drop_location) }
  let(:auction) { create(:auction, job: fleet_managed_job) }
  let(:auto_assign_feature) { create(:feature, { name: Feature::AUTO_ASSIGN }) }
  let(:rescue_companies) do
    [
      create(:rescue_company, :new_jerzy_towing),
      create(:rescue_company, :saint_paul_towing),
      create(:rescue_company, :durand_durand_towing),
    ]
  end
  let(:rescue_vehicles) do
    rescue_companies.map do |rescue_company|
      create(:rescue_vehicle, :with_driver, company: rescue_company, lat: rescue_company.sites.first.location.lat, lng: rescue_company.sites.first.location.lng)
    end
  end
  let(:site_candidates) do
    rescue_companies.map do |rescue_company|
      create(:site_candidate,
             job: fleet_managed_job,
             company: rescue_company,
             site: rescue_company.sites.first,
             lat: nil,
             lng: nil,
             rescue_provider: nil,
             estimate: nil)
    end
  end
  let(:site_candidate_for_new_jerzy_towing) { site_candidates[0] }
  let(:site_candidate_for_saint_paul_towing) { site_candidates[1] }
  let(:site_candidate_for_durand_durand_towing) { site_candidates[2] }
  let(:truck_candidates) do
    rescue_vehicles.map do |rescue_vehicle|
      create(:truck_candidate,
             job: fleet_managed_job,
             vehicle: rescue_vehicle,
             has_auto_dispatchable_job: true,
             company: rescue_vehicle.company,
             site: rescue_vehicle.company.sites.first,
             location_updated_at: nil,
             driver: nil,
             lat: nil,
             lng: nil,
             estimate: nil)
    end
  end
  let(:truck_candidate_for_new_jerzy_towing) { truck_candidates[0] }
  let(:truck_candidate_for_saint_paul_towing) { truck_candidates[1] }
  let(:truck_candidate_for_durand_durand_towing) { truck_candidates[2] }

  context 'job has a service location but not a drop location (the tow will be A->B->A)', vcr: true do
    let(:service_location) { create(:location, :mission_safeway) }
    let(:drop_location) { nil }

    describe 'each candidate has an estimate (with AB meters, AB seconds, BA meters, and BA seconds), google_eta, and google_miles' do
      describe 'site candidates' do
        let(:candidates) { site_candidates }

        it 'for New Jerzy Towing (starting location) to Mission Safeway (service location) and back to New Jerzy Towing (starting location)' do
          # A is 2381 18th Avenue, B is 3350 Mission St
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location).to be_nil
          expect(site_candidate_for_new_jerzy_towing.company.name).to eql 'New Jerzy Towing'
          expect(site_candidate_for_new_jerzy_towing.site.location.address).to eql '2381 18th Avenue, San Francisco, CA 94116'
          expect(site_candidate_for_new_jerzy_towing.starting_location.address).to eql '2381 18th Avenue, San Francisco, CA 94116'
          expect(site_candidate_for_new_jerzy_towing.starting_location.lat).to eql 37.7434986
          expect(site_candidate_for_new_jerzy_towing.starting_location.lng).to eql(-122.474834)
          expect(site_candidate_for_new_jerzy_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(site_candidate_for_new_jerzy_towing.estimate.meters_ab).to eql 5651 # 3.5 miles there
          expect(site_candidate_for_new_jerzy_towing.google_miles).to eql 3 # 3.5 miles there
          expect(site_candidate_for_new_jerzy_towing.estimate.seconds_ab).to eql 747 # 12 minutes there
          expect(site_candidate_for_new_jerzy_towing.google_eta).to eql 12 # 12 minutes there
          expect(site_candidate_for_new_jerzy_towing.estimate.meters_ba).to eql 5829 # 3.6 miles back
          expect(site_candidate_for_new_jerzy_towing.estimate.seconds_ba).to eql 790 # 13 minutes back
          expect(site_candidate_for_new_jerzy_towing.estimate.meters_bc).to be_nil
          expect(site_candidate_for_new_jerzy_towing.estimate.seconds_bc).to be_nil
          expect(site_candidate_for_new_jerzy_towing.estimate.meters_ca).to be_nil
          expect(site_candidate_for_new_jerzy_towing.estimate.seconds_ca).to be_nil
          # The site is eligible
          expect(site_candidate_for_new_jerzy_towing.vehicle_eligible).to eql 'eligible'
        end
        it 'for Saint Paul Towing (starting location) to Mission Safeway (service location) and back to Saint Paul Towing (starting location)' do
          # A is 1465 Folsom St, B is 3350 Mission St
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location).to be_nil
          expect(site_candidate_for_saint_paul_towing.company.name).to eql "Saint Paul's Towing"
          expect(site_candidate_for_saint_paul_towing.site.location.address).to eql '1465 Folsom St, San Francisco, CA 94103'
          expect(site_candidate_for_saint_paul_towing.starting_location.address).to eql '1465 Folsom St, San Francisco, CA 94103'
          expect(site_candidate_for_saint_paul_towing.starting_location.lat).to eql 37.7720023
          expect(site_candidate_for_saint_paul_towing.starting_location.lng).to eql(-122.4133224)
          expect(site_candidate_for_saint_paul_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(site_candidate_for_saint_paul_towing.estimate.meters_ab).to eql 5101 # 3.2 miles there
          expect(site_candidate_for_saint_paul_towing.google_miles).to eql 3 # 3.2 miles there
          expect(site_candidate_for_saint_paul_towing.estimate.seconds_ab).to eql 580 # 9 minutes there
          expect(site_candidate_for_saint_paul_towing.google_eta).to eql 9 # 9 minutes there
          expect(site_candidate_for_saint_paul_towing.estimate.meters_ba).to eql 3947 # 2.4 miles back
          expect(site_candidate_for_saint_paul_towing.estimate.seconds_ba).to eql 613 # 10 minutes back
          expect(site_candidate_for_saint_paul_towing.estimate.meters_bc).to be_nil
          expect(site_candidate_for_saint_paul_towing.estimate.seconds_bc).to be_nil
          expect(site_candidate_for_saint_paul_towing.estimate.meters_ca).to be_nil
          expect(site_candidate_for_saint_paul_towing.estimate.seconds_ca).to be_nil
          # The site is eligible
          expect(site_candidate_for_saint_paul_towing.vehicle_eligible).to eql 'eligible'
        end
        it 'for Durand Durand Towing (starting location) to Mission Safeway (service location) and back to Durand Durand Towing (starting location)' do
          # A is 1213 Fell St, B is 3350 Mission St
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location).to be_nil
          expect(site_candidate_for_durand_durand_towing.company.name).to eql 'Durand Durand Towing'
          expect(site_candidate_for_durand_durand_towing.site.location.address).to eql '1213 Fell St, San Francisco, CA 94117'
          expect(site_candidate_for_durand_durand_towing.starting_location.address).to eql '1213 Fell St, San Francisco, CA 94117'
          expect(site_candidate_for_durand_durand_towing.starting_location.lat).to eql 37.7737352
          expect(site_candidate_for_durand_durand_towing.starting_location.lng).to eql(-122.4381865)
          expect(site_candidate_for_durand_durand_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(site_candidate_for_durand_durand_towing.estimate.meters_ab).to eql 5149 # 3.1 miles there
          expect(site_candidate_for_durand_durand_towing.google_miles).to eql 3 # 3.1 miles there
          expect(site_candidate_for_durand_durand_towing.estimate.seconds_ab).to eql 882 # 14 minutes there
          expect(site_candidate_for_durand_durand_towing.google_eta).to eql 14 # 14 minutes there
          expect(site_candidate_for_durand_durand_towing.estimate.meters_ba).to eql 4835 # 3 miles back
          expect(site_candidate_for_durand_durand_towing.estimate.seconds_ba).to eql 773 # 13 minutes back
          expect(site_candidate_for_durand_durand_towing.estimate.meters_bc).to be_nil
          expect(site_candidate_for_durand_durand_towing.estimate.seconds_bc).to be_nil
          expect(site_candidate_for_durand_durand_towing.estimate.meters_ca).to be_nil
          expect(site_candidate_for_durand_durand_towing.estimate.seconds_ca).to be_nil
          # The site is eligible
          expect(site_candidate_for_durand_durand_towing.vehicle_eligible).to eql 'eligible'
        end
      end

      describe 'truck candidates' do
        let(:candidates) { truck_candidates }

        it "for New Jerzy Towing's tow truck (starting location) to Mission Safeway (service location) and back to New Jerzy Towing's tow truck (starting location)" do
          # A is 2381 18th Avenue, B is 3350 Mission St
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location).to be_nil
          expect(truck_candidate_for_new_jerzy_towing.company.name).to eql 'New Jerzy Towing'
          expect(truck_candidate_for_new_jerzy_towing.vehicle).to be_instance_of RescueVehicle
          expect(truck_candidate_for_new_jerzy_towing.has_auto_dispatchable_job).to be true
          expect(truck_candidate_for_new_jerzy_towing.vehicle.lat).to eql 37.7434986
          expect(truck_candidate_for_new_jerzy_towing.vehicle.lng).to eql(-122.474834)
          expect(truck_candidate_for_new_jerzy_towing.starting_location.lat).to eql 37.7434986
          expect(truck_candidate_for_new_jerzy_towing.starting_location.lng).to eql(-122.474834)
          expect(truck_candidate_for_new_jerzy_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(truck_candidate_for_new_jerzy_towing.estimate.meters_ab).to eql 5651 # 3.5 miles there
          expect(truck_candidate_for_new_jerzy_towing.google_miles).to eql 3 # 3.5 miles there
          expect(truck_candidate_for_new_jerzy_towing.estimate.seconds_ab).to eql 751 # 12 minutes there
          expect(truck_candidate_for_new_jerzy_towing.google_eta).to eql 12 # 12 minutes there
          expect(truck_candidate_for_new_jerzy_towing.estimate.meters_ba).to eql 5829 # 3.6 miles back
          expect(truck_candidate_for_new_jerzy_towing.estimate.seconds_ba).to eql 778 # 13 minutes back
          expect(truck_candidate_for_new_jerzy_towing.estimate.meters_bc).to be_nil
          expect(truck_candidate_for_new_jerzy_towing.estimate.seconds_bc).to be_nil
          expect(truck_candidate_for_new_jerzy_towing.estimate.meters_ca).to be_nil
          expect(truck_candidate_for_new_jerzy_towing.estimate.seconds_ca).to be_nil
          # The truck is eligible
          expect(truck_candidate_for_new_jerzy_towing.vehicle_eligible).to eql 'eligible'
        end
        it "for Saint Paul Towing's tow truck (starting location) to Mission Safeway (service location) and back to Saint Paul Towing's tow truck (starting location)" do
          # A is 1465 Folsom St, B is 3350 Mission St
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location).to be_nil
          expect(truck_candidate_for_saint_paul_towing.company.name).to eql "Saint Paul's Towing"
          expect(truck_candidate_for_saint_paul_towing.vehicle).to be_instance_of RescueVehicle
          expect(truck_candidate_for_saint_paul_towing.has_auto_dispatchable_job).to be true
          expect(truck_candidate_for_saint_paul_towing.vehicle.lat).to eql 37.7720023
          expect(truck_candidate_for_saint_paul_towing.vehicle.lng).to eql(-122.4133224)
          expect(truck_candidate_for_saint_paul_towing.starting_location.lat).to eql 37.7720023
          expect(truck_candidate_for_saint_paul_towing.starting_location.lng).to eql(-122.4133224)
          expect(truck_candidate_for_saint_paul_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(truck_candidate_for_saint_paul_towing.estimate.meters_ab).to eql 5101 # 3.2 miles there
          expect(truck_candidate_for_saint_paul_towing.google_miles).to eql 3 # 3.2 miles there
          expect(truck_candidate_for_saint_paul_towing.estimate.seconds_ab).to eql 576 # 9 minutes there
          expect(truck_candidate_for_saint_paul_towing.google_eta).to eql 9 # 9 minutes there
          expect(truck_candidate_for_saint_paul_towing.estimate.meters_ba).to eql 3947 # 2.4 miles back
          expect(truck_candidate_for_saint_paul_towing.estimate.seconds_ba).to eql 613 # 10 minutes back
          expect(truck_candidate_for_saint_paul_towing.estimate.meters_bc).to be_nil
          expect(truck_candidate_for_saint_paul_towing.estimate.seconds_bc).to be_nil
          expect(truck_candidate_for_saint_paul_towing.estimate.meters_ca).to be_nil
          expect(truck_candidate_for_saint_paul_towing.estimate.seconds_ca).to be_nil
          # The truck is eligible
          expect(truck_candidate_for_saint_paul_towing.vehicle_eligible).to eql 'eligible'
        end
        it "for Durand Durand Towing's tow truck (starting location) to Mission Safeway (service location) and back to Durand Durand Towing's tow truck (starting location)" do
          # A is 1213 Fell St, B is 3350 Mission St
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location).to be_nil
          expect(truck_candidate_for_durand_durand_towing.company.name).to eql 'Durand Durand Towing'
          expect(truck_candidate_for_durand_durand_towing.vehicle).to be_instance_of RescueVehicle
          expect(truck_candidate_for_durand_durand_towing.has_auto_dispatchable_job).to be true
          expect(truck_candidate_for_durand_durand_towing.vehicle.lat).to eql 37.7737352
          expect(truck_candidate_for_durand_durand_towing.vehicle.lng).to eql(-122.4381865)
          expect(truck_candidate_for_durand_durand_towing.starting_location.lat).to eql 37.7737352
          expect(truck_candidate_for_durand_durand_towing.starting_location.lng).to eql(-122.4381865)
          expect(truck_candidate_for_durand_durand_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(truck_candidate_for_durand_durand_towing.estimate.meters_ab).to eql 5149 # 3.1 miles there
          expect(truck_candidate_for_durand_durand_towing.google_miles).to eql 3 # 3.1 miles there
          expect(truck_candidate_for_durand_durand_towing.estimate.seconds_ab).to eql 886 # 14 minutes there
          expect(truck_candidate_for_durand_durand_towing.google_eta).to eql 14 # 14 minutes there
          expect(truck_candidate_for_durand_durand_towing.estimate.meters_ba).to eql 4835 # 3 miles back
          expect(truck_candidate_for_durand_durand_towing.estimate.seconds_ba).to eql 771 # 12 minutes back
          expect(truck_candidate_for_durand_durand_towing.estimate.meters_bc).to be_nil
          expect(truck_candidate_for_durand_durand_towing.estimate.seconds_bc).to be_nil
          expect(truck_candidate_for_durand_durand_towing.estimate.meters_ca).to be_nil
          expect(truck_candidate_for_durand_durand_towing.estimate.seconds_ca).to be_nil
          # The truck is eligible
          expect(truck_candidate_for_durand_durand_towing.vehicle_eligible).to eql 'eligible'
        end
      end
    end
  end

  context 'job has a service location and a drop location (the tow will be A->B->C->A)', vcr: true do
    let(:service_location) { create(:location, :mission_safeway) }
    let(:drop_location) { create(:location, :stonestown_mall) }

    describe 'each candidate has an estimate (with AB meters, AB seconds, BC meters, BC seconds, CA meters, CA seconds), google_eta, and google_miles' do
      describe 'site candidates' do
        let(:candidates) { site_candidates }

        it 'for New Jerzy Towing (starting location) to Mission Safeway (service location) to Stonestown Mall (drop location) and back to New Jerzy Towing (starting location)' do
          # A is 2381 18th Avenue, B is 3350 Mission St, C is 3251 20th Ave
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location.address).to eql '3251 20th Ave, San Francisco, CA 94132'
          expect(site_candidate_for_new_jerzy_towing.company.name).to eql 'New Jerzy Towing'
          expect(site_candidate_for_new_jerzy_towing.site.location.address).to eql '2381 18th Avenue, San Francisco, CA 94116'
          expect(site_candidate_for_new_jerzy_towing.starting_location.address).to eql '2381 18th Avenue, San Francisco, CA 94116'
          expect(site_candidate_for_new_jerzy_towing.starting_location.lat).to eql 37.7434986
          expect(site_candidate_for_new_jerzy_towing.starting_location.lng).to eql(-122.474834)
          expect(site_candidate_for_new_jerzy_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(site_candidate_for_new_jerzy_towing.estimate.meters_ab).to eql 5651 # 3.5 miles to pickup
          expect(site_candidate_for_new_jerzy_towing.google_miles).to eql 3 # 3.5 miles to pickup
          expect(site_candidate_for_new_jerzy_towing.estimate.seconds_ab).to eql 767 # 12 minutes to pickup
          expect(site_candidate_for_new_jerzy_towing.google_eta).to eql 12 # 12 minutes to pickup
          expect(site_candidate_for_new_jerzy_towing.estimate.meters_ba).to be_nil
          expect(site_candidate_for_new_jerzy_towing.estimate.seconds_ba).to be_nil
          expect(site_candidate_for_new_jerzy_towing.estimate.meters_bc).to eql 9908 # 6.1 miles to dropoff
          expect(site_candidate_for_new_jerzy_towing.estimate.seconds_bc).to eql 796 # 13 minutes to dropoff
          expect(site_candidate_for_new_jerzy_towing.estimate.meters_ca).to eql 2323 # 1.4 miles back to starting location
          expect(site_candidate_for_new_jerzy_towing.estimate.seconds_ca).to eql 385 # 6 minutes back to starting location
          # The site is eligible
          expect(site_candidate_for_new_jerzy_towing.vehicle_eligible).to eql 'eligible'
        end
        it 'for Saint Paul Towing (starting location) to Mission Safeway (service location) to Stonestown Mall (drop location) and back to Saint Paul Towing (starting location)' do
          # A is 1465 Folsom St, B is 3350 Mission St, C is 3251 20th Ave
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location.address).to eql '3251 20th Ave, San Francisco, CA 94132'
          expect(site_candidate_for_saint_paul_towing.company.name).to eql "Saint Paul's Towing"
          expect(site_candidate_for_saint_paul_towing.site.location.address).to eql '1465 Folsom St, San Francisco, CA 94103'
          expect(site_candidate_for_saint_paul_towing.starting_location.address).to eql '1465 Folsom St, San Francisco, CA 94103'
          expect(site_candidate_for_saint_paul_towing.starting_location.lat).to eql 37.7720023
          expect(site_candidate_for_saint_paul_towing.starting_location.lng).to eql(-122.4133224)
          expect(site_candidate_for_saint_paul_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(site_candidate_for_saint_paul_towing.estimate.meters_ab).to eql 5101 # 3.2 miles to pickup
          expect(site_candidate_for_saint_paul_towing.google_miles).to eql 3 # 3.2 miles to pickup
          expect(site_candidate_for_saint_paul_towing.estimate.seconds_ab).to eql 524 # 8 minutes to pickup
          expect(site_candidate_for_saint_paul_towing.google_eta).to eql 8 # 8 minutes to pickup
          expect(site_candidate_for_saint_paul_towing.estimate.meters_ba).to be_nil
          expect(site_candidate_for_saint_paul_towing.estimate.seconds_ba).to be_nil
          expect(site_candidate_for_saint_paul_towing.estimate.meters_bc).to eql 9908 # 6.1 miles to dropoff
          expect(site_candidate_for_saint_paul_towing.estimate.seconds_bc).to eql 788 # 13 minutes to dropoff
          expect(site_candidate_for_saint_paul_towing.estimate.meters_ca).to eql 14880 # 9.2 miles back to starting location
          expect(site_candidate_for_saint_paul_towing.estimate.seconds_ca).to eql 1083 # 18 minutes back to starting location
          # The site is eligible
          expect(site_candidate_for_saint_paul_towing.vehicle_eligible).to eql 'eligible'
        end
        it 'for Durand Durand Towing (starting location) to Mission Safeway (service location) to Stonestown Mall (drop location) and back to Durand Durand Towing (starting location)' do
          # A is 1213 Fell St, B is 3350 Mission St, C is 3251 20th Ave
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location.address).to eql '3251 20th Ave, San Francisco, CA 94132'
          expect(site_candidate_for_durand_durand_towing.company.name).to eql 'Durand Durand Towing'
          expect(site_candidate_for_durand_durand_towing.site.location.address).to eql '1213 Fell St, San Francisco, CA 94117'
          expect(site_candidate_for_durand_durand_towing.starting_location.address).to eql '1213 Fell St, San Francisco, CA 94117'
          expect(site_candidate_for_durand_durand_towing.starting_location.lat).to eql 37.7737352
          expect(site_candidate_for_durand_durand_towing.starting_location.lng).to eql(-122.4381865)
          expect(site_candidate_for_durand_durand_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(site_candidate_for_durand_durand_towing.estimate.meters_ab).to eql 5149 # 3.1 miles to pickup
          expect(site_candidate_for_durand_durand_towing.google_miles).to eql 3 # 3.1 miles to pickup
          expect(site_candidate_for_durand_durand_towing.estimate.seconds_ab).to eql 873 # 14 minutes to pickup
          expect(site_candidate_for_durand_durand_towing.google_eta).to eql 14 # 14 minutes to pickup
          expect(site_candidate_for_durand_durand_towing.estimate.meters_ba).to be_nil
          expect(site_candidate_for_durand_durand_towing.estimate.seconds_ba).to be_nil
          expect(site_candidate_for_durand_durand_towing.estimate.meters_bc).to eql 9908 # 6.1 miles to dropoff
          expect(site_candidate_for_durand_durand_towing.estimate.seconds_bc).to eql 789 # 13 minutes to dropoff
          expect(site_candidate_for_durand_durand_towing.estimate.meters_ca).to eql 9048 # 5.6 miles back to starting location
          expect(site_candidate_for_durand_durand_towing.estimate.seconds_ca).to eql 1015 # 17 minutes back to starting location
          # The site is eligible
          expect(site_candidate_for_durand_durand_towing.vehicle_eligible).to eql 'eligible'
        end
      end

      describe 'truck candidates' do
        let(:candidates) { truck_candidates }

        it "for New Jerzy Towing's tow truck (starting location) to Mission Safeway (service location) to Stonestown Mall (drop location) and back to New Jerzy Towing's tow truck (starting location)" do
          # A is 2381 18th Avenue, B is 3350 Mission St, C is 3251 20th Ave
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location.address).to eql '3251 20th Ave, San Francisco, CA 94132'
          expect(truck_candidate_for_new_jerzy_towing.company.name).to eql 'New Jerzy Towing'
          expect(truck_candidate_for_new_jerzy_towing.vehicle).to be_instance_of RescueVehicle
          expect(truck_candidate_for_new_jerzy_towing.has_auto_dispatchable_job).to be true
          expect(truck_candidate_for_new_jerzy_towing.vehicle.lat).to eql 37.7434986
          expect(truck_candidate_for_new_jerzy_towing.vehicle.lng).to eql(-122.474834)
          expect(truck_candidate_for_new_jerzy_towing.starting_location.lat).to eql 37.7434986
          expect(truck_candidate_for_new_jerzy_towing.starting_location.lng).to eql(-122.474834)
          expect(truck_candidate_for_new_jerzy_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(truck_candidate_for_new_jerzy_towing.estimate.meters_ab).to eql 5651 # 3.5 miles to pickup
          expect(truck_candidate_for_new_jerzy_towing.google_miles).to eql 3 # 3.5 miles to pickup
          expect(truck_candidate_for_new_jerzy_towing.estimate.seconds_ab).to eql 739 # 12 minutes to pickup
          expect(truck_candidate_for_new_jerzy_towing.google_eta).to eql 12 # 12 minutes to pickup
          expect(truck_candidate_for_new_jerzy_towing.estimate.meters_ba).to be_nil
          expect(truck_candidate_for_new_jerzy_towing.estimate.seconds_ba).to be_nil
          expect(truck_candidate_for_new_jerzy_towing.estimate.meters_bc).to eql 9908 # 6.1 miles to dropoff
          expect(truck_candidate_for_new_jerzy_towing.estimate.seconds_bc).to eql 790 # 13 minutes to dropoff
          expect(truck_candidate_for_new_jerzy_towing.estimate.meters_ca).to eql 2323 # 1.4 miles back to starting location
          expect(truck_candidate_for_new_jerzy_towing.estimate.seconds_ca).to eql 396 # 6 minutes back to starting location
          # The truck is eligible
          expect(truck_candidate_for_new_jerzy_towing.vehicle_eligible).to eql 'eligible'
        end
        it "for Saint Paul Towing's tow truck (starting location) to Mission Safeway (service location) to Stonestown Mall (drop location) and back to Saint Paul Towing's tow truck (starting location)" do
          # A is 1465 Folsom St, B is 3350 Mission St, C is 3251 20th Ave
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location.address).to eql '3251 20th Ave, San Francisco, CA 94132'
          expect(truck_candidate_for_saint_paul_towing.company.name).to eql "Saint Paul's Towing"
          expect(truck_candidate_for_saint_paul_towing.vehicle).to be_instance_of RescueVehicle
          expect(truck_candidate_for_saint_paul_towing.has_auto_dispatchable_job).to be true
          expect(truck_candidate_for_saint_paul_towing.vehicle.lat).to eql 37.7720023
          expect(truck_candidate_for_saint_paul_towing.vehicle.lng).to eql(-122.4133224)
          expect(truck_candidate_for_saint_paul_towing.starting_location.lat).to eql 37.7720023
          expect(truck_candidate_for_saint_paul_towing.starting_location.lng).to eql(-122.4133224)
          expect(truck_candidate_for_saint_paul_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(truck_candidate_for_saint_paul_towing.estimate.meters_ab).to eql 5101 # 3.2 miles to pickup
          expect(truck_candidate_for_saint_paul_towing.google_miles).to eql 3 # 3.2 miles to pickup
          expect(truck_candidate_for_saint_paul_towing.estimate.seconds_ab).to eql 495 # 8 minutes to pickup
          expect(truck_candidate_for_saint_paul_towing.google_eta).to eql 8 # 8 minutes to pickup
          expect(truck_candidate_for_saint_paul_towing.estimate.meters_ba).to be_nil
          expect(truck_candidate_for_saint_paul_towing.estimate.seconds_ba).to be_nil
          expect(truck_candidate_for_saint_paul_towing.estimate.meters_bc).to eql 9908 # 6.1 miles to dropoff
          expect(truck_candidate_for_saint_paul_towing.estimate.seconds_bc).to eql 780 # 13 minutes to dropoff
          expect(truck_candidate_for_saint_paul_towing.estimate.meters_ca).to eql 14880 # 9.2 miles back to starting location
          expect(truck_candidate_for_saint_paul_towing.estimate.seconds_ca).to eql 1077 # 18 minutes back to starting location
          # The truck is eligible
          expect(truck_candidate_for_saint_paul_towing.vehicle_eligible).to eql 'eligible'
        end
        it "for Durand Durand Towing's tow truck (starting location) to Mission Safeway (service location) to Stonestown Mall (drop location) and back to Durand Durand Towing's tow truck (starting location)" do
          # A is 1213 Fell St, B is 3350 Mission St, C is 3251 20th Ave
          expect(fleet_managed_job.service_location.address).to eql '3350 Mission St, San Francisco, CA 94110'
          expect(fleet_managed_job.drop_location.address).to eql '3251 20th Ave, San Francisco, CA 94132'
          expect(truck_candidate_for_durand_durand_towing.company.name).to eql 'Durand Durand Towing'
          expect(truck_candidate_for_durand_durand_towing.vehicle).to be_instance_of RescueVehicle
          expect(truck_candidate_for_durand_durand_towing.has_auto_dispatchable_job).to be true
          expect(truck_candidate_for_durand_durand_towing.vehicle.lat).to eql 37.7737352
          expect(truck_candidate_for_durand_durand_towing.vehicle.lng).to eql(-122.4381865)
          expect(truck_candidate_for_durand_durand_towing.starting_location.lat).to eql 37.7737352
          expect(truck_candidate_for_durand_durand_towing.starting_location.lng).to eql(-122.4381865)
          expect(truck_candidate_for_durand_durand_towing.estimate).to be_nil

          subject

          # The estimates are filled in
          expect(truck_candidate_for_durand_durand_towing.estimate.meters_ab).to eql 5149 # 3.1 miles to pickup
          expect(truck_candidate_for_durand_durand_towing.google_miles).to eql 3 # 3.1 miles to pickup
          expect(truck_candidate_for_durand_durand_towing.estimate.seconds_ab).to eql 835 # 13 minutes to pickup
          expect(truck_candidate_for_durand_durand_towing.google_eta).to eql 13 # 13 minutes to pickup
          expect(truck_candidate_for_durand_durand_towing.estimate.meters_ba).to be_nil
          expect(truck_candidate_for_durand_durand_towing.estimate.seconds_ba).to be_nil
          expect(truck_candidate_for_durand_durand_towing.estimate.meters_bc).to eql 9908 # 6.1 miles to dropoff
          expect(truck_candidate_for_durand_durand_towing.estimate.seconds_bc).to eql 777 # 13 minutes to dropoff
          expect(truck_candidate_for_durand_durand_towing.estimate.meters_ca).to eql 9048 # 5.6 miles back to starting location
          expect(truck_candidate_for_durand_durand_towing.estimate.seconds_ca).to eql 976 # 16 minutes back to starting location
          # The truck is eligible
          expect(truck_candidate_for_durand_durand_towing.vehicle_eligible).to eql 'eligible'
        end
      end
    end
  end

  context "job doesn't have a service location" do
    let(:service_location) { nil }
    let(:drop_location) { nil }
    let(:candidates) { [site_candidates, truck_candidates].sample }

    it 'raises an exception' do
      expect { subject }.to raise_error CannotCreateEstimatesForJobCandidatesWithoutJobServiceLocationError
    end
  end
end
