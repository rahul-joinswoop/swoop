# frozen_string_literal: true

require "rails_helper"

describe TireTypeServices::Batch do
  subject(:service_call) do
    described_class.new(
      tire_type_ids: @tire_types.map(&:id).join(',')
    ).call
  end

  def create_tire_types
    tire_type_one = create(:tire_type, car: 'Model S', size: '245/45R19')
    tire_type_two = create(:tire_type, car: 'Model S', size: '245/35R21')

    [tire_type_one, tire_type_two]
  end

  before :each do
    @tire_types ||= create_tire_types
  end

  it 'returns all tire_types requested' do
    tire_type_one = @tire_types.first
    tire_type_two = @tire_types.second

    expect(service_call).to include(tire_type_one, tire_type_two)
  end
end
