# frozen_string_literal: true

require "rails_helper"

RSpec.describe SitesHelper, type: :helper do
  describe "#dispatchable_by_fleet" do
    subject(:dispatchable_by_fleet) do
      helper.dispatchable_by_fleet(fleet_rescue_provider)
    end

    let(:fleet_rescue_provider) { double "RescueProvider", deleted_at: nil }

    it "returns true when the RescueProvider is not deleted" do
      expect(dispatchable_by_fleet).to eql(true)
    end

    it "returns false when the RescueProvider is nil" do
      expect(helper.dispatchable_by_fleet(nil)).to eql(false)
    end

    it "returns false when the RescueProvider is deleted" do
      allow(fleet_rescue_provider)
        .to receive(:deleted_at).and_return(Time.zone.now)
      expect(dispatchable_by_fleet).to eql(false)
    end
  end
end
