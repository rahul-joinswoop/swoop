# frozen_string_literal: true

require "rails_helper"

describe AuctionHelper, :vcr do
  let!(:swoop_auto_dispatch_user) { create(:swoop_auto_dispatch) }
  let(:user) { create(:user, company: create(:super_company)) }
  let(:auto_assign_feature) { create(:feature, { name: Feature::AUTO_ASSIGN }) }
  let!(:rescue_companies) { create_list(:rescue_company, 10) }
  let(:rescue_company_who_is_bidding) { rescue_companies.first }
  let(:rescue_company_who_is_bidding_bid) { rescue_company_who_is_bidding.bids.first }
  let(:candidate_id_who_is_bidding) { rescue_company_who_is_bidding_bid.candidate.id }
  let(:fleet_company) { create(:fleet_company, { features: [auto_assign_feature] }) }
  let(:auction) { create(:auction, :live, job: job, max_target_eta: 30) }
  let(:job) do
    create(:fleet_managed_job, {
      fleet_company: fleet_company,
      status: Job::AUTO_ASSIGNING,
      root_dispatcher_id: user.id,
      service_location: create(:location),
    })
  end

  before(:each) do
    Sidekiq::Worker.clear_all
  end

  describe 'all bids are pending' do
    before(:each) do
      # Make some pending bids
      rescue_companies.each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::REQUESTED, auction: auction })
      end
    end

    describe 'a bid is submitted' do
      it 'does not call the expire auction timer to end the auction' do
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
        Auction::SubmitBidService.new(job, rescue_company_who_is_bidding, rand(5..45), candidate_id_who_is_bidding).call
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
      end
    end
  end

  describe 'some bids are pending (>1) and some are submitted' do
    before(:each) do
      # Make some pending bids
      rescue_companies[0..4].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::REQUESTED, auction: auction })
      end
      # Make some submitted bids
      rescue_companies[5..9].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::SUBMITTED, auction: auction })
      end
    end

    describe 'a bid is submitted' do
      it 'does not call the expire auction timer to end the auction' do
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
        Auction::SubmitBidService.new(job, rescue_company_who_is_bidding, rand(5..45), candidate_id_who_is_bidding).call
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
      end
    end
  end

  describe 'some bids are pending, some are submitted, and some are provider rejected' do
    before(:each) do
      # Make some pending bids
      rescue_companies[0..2].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::REQUESTED, auction: auction })
      end
      # Make some submitted bids
      rescue_companies[3..5].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::SUBMITTED, auction: auction })
      end
      # Make some provider rejected bids
      rescue_companies[6..9].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::PROVIDER_REJECTED, auction: auction })
      end
    end

    describe 'a bid is submitted' do
      it 'does not call the expire auction timer to end the auction' do
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
        Auction::SubmitBidService.new(job, rescue_company_who_is_bidding, rand(5..45), candidate_id_who_is_bidding).call
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
      end
    end
  end

  describe 'some bids are pending, some are submitted, but one is won' do
    before(:each) do
      # Make some pending bids
      rescue_companies[0..4].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::REQUESTED, auction: auction })
      end
      # Make some submitted bids
      rescue_companies[5..8].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::SUBMITTED, auction: auction })
      end
      # Make 1 won bid
      create(:bid, { candidate: create(:job_candidate, { job: job, company: rescue_companies[9] }), job: job, company: rescue_companies[9], eta_mins: rand(5..45), status: Auction::Bid::WON, auction: auction })
    end

    describe 'a bid is submitted' do
      it 'does not call the expire auction timer to end the auction' do
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
        expect do
          Auction::SubmitBidService.new(job, rescue_company_who_is_bidding, rand(5..45), candidate_id_who_is_bidding).call
        end.to raise_error(AuctionHelper::CannotExpireAuctionImmediatelyIfAuctionHasAnyTerminalBidsError)
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
      end
    end
  end

  describe 'all bids are auto rejected, except for one which is pending' do
    before(:each) do
      # Make 1 pending bid
      create(:bid, { candidate: create(:job_candidate, { job: job, company: rescue_companies[0] }), job: job, company: rescue_companies[0], eta_mins: rand(5..45), status: Auction::Bid::REQUESTED, auction: auction })
      # Make some auto rejected bids
      rescue_companies[1..9].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::AUTO_REJECTED, auction: auction })
      end
    end

    describe 'a bid is submitted' do
      it 'does not call the expire auction timer to end the auction' do
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
        expect do
          Auction::SubmitBidService.new(job, rescue_company_who_is_bidding, rand(5..45), candidate_id_who_is_bidding).call
        end.to raise_error(AuctionHelper::CannotExpireAuctionImmediatelyIfAuctionHasAnyTerminalBidsError)
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
      end
    end
  end

  describe 'all bids are submitted or provider rejected, except for one which is pending' do
    before(:each) do
      # Make 1 pending bid
      create(:bid, { candidate: create(:job_candidate, { job: job, company: rescue_companies[0] }), job: job, company: rescue_companies[0], eta_mins: rand(5..45), status: Auction::Bid::REQUESTED, auction: auction })
      # Make some submitted bids
      rescue_companies[1..4].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::SUBMITTED, auction: auction })
      end
      # Make some provider rejected bids
      rescue_companies[5..9].each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::PROVIDER_REJECTED, auction: auction })
      end
    end

    describe 'a bid is submitted' do
      it 'calls the expire auction timer to end the auction' do
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
        Auction::SubmitBidService.new(job, rescue_company_who_is_bidding, rand(5..45), candidate_id_who_is_bidding).call
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(1)
      end
    end

    describe 'a bid is rejected by the provider' do
      it 'calls the expire auction timer to end the auction' do
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
        Auction::RejectBidService.new(job, rescue_company_who_is_bidding, 10, "The tenderloin? Are you crazy?!?").call
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(1)
      end
    end
  end

  describe 'all bids are new' do
    before(:each) do
      # Make some new bids
      rescue_companies.each do |rc|
        create(:bid, { candidate: create(:job_candidate, { job: job, company: rc }), job: job, company: rc, eta_mins: rand(5..45), status: Auction::Bid::NEW, auction: auction })
      end
    end

    describe 'a caller tries to expire the auction immediately' do
      it 'does not call the expire auction timer to end the auction' do
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
        expect do
          Object.new.extend(AuctionHelper).expire_auction_immediately_if_no_bidders_remain(rescue_company_who_is_bidding_bid, job)
        end.to raise_error(AuctionHelper::CannotExpireAuctionImmediatelyWhereAllBidsAreNewError)
        expect(Auction::ExpireAuctionTimer.jobs.size).to eql(0)
      end
    end
  end

  # Required to flush out Auction::ExpireAuctionTimer.jobs.size
  # between tests, or you'll get occasional failures due to a race
  # condition where RSpec is moving faster than Sidekiq organically
  # purges jobs
end
