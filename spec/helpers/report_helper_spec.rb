# frozen_string_literal: true

require "rails_helper"
RSpec.describe ReportHelper, type: :model do
  include ReportHelper

  it "calculate the correct number of minutes" do
    seconds = duration_to_seconds("01:00:00")
    expect(seconds).to eq(1.hour.to_f)
    seconds = duration_to_seconds("00:01:00")
    expect(seconds).to eq(1.minute.to_f)
  end

  it "calculates the correct duration for negative numbers" do
    seconds = duration_to_seconds("-01:00:00")
    expect(seconds).to eq(-1.hours.to_f)
  end

  it "shows the correct duration" do
    duration = seconds_to_duration(1.day)
    expect(duration).to eq("24:00")
    duration = seconds_to_duration(1.hour)
    expect(duration).to eq("01:00")
    duration = seconds_to_duration(1.minute)
    expect(duration).to eq("00:01")
  end

  it "shows the correct duration format with negative" do
    duration = seconds_to_duration(-1.day)
    expect(duration).to eq("-24:00")
    duration = seconds_to_duration(-1.hour)
    expect(duration).to eq("-01:00")
    duration = seconds_to_duration(-1.minute)
    expect(duration).to eq("-00:01")
  end
end
