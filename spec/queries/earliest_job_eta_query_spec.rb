# frozen_string_literal: true

require 'rails_helper'

describe EarliestJobEtaQuery do
  subject(:result) { EarliestJobEtaQuery.call(job_id) }

  before do
    allow(EarliestJobEtaQuery)
      .to receive(:job_or_eldest_parent)
      .with(job_id)
      .and_return(job)
  end

  context 'when a job or parent job is found' do
    let(:job_id) { job.id }

    let(:job) do
      create(
        :job,
        time_of_arrivals: toas
      )
    end

    let(:toas) do
      [
        expected_eta,
        create(:time_of_arrival,
               eba_type: TimeOfArrival::ESTIMATE),
        create(:time_of_arrival,
               eba_type: TimeOfArrival::ACTUAL),
        create(:time_of_arrival,
               eba_type: TimeOfArrival::SWOOP_ESTIMATE),
        create(:time_of_arrival,
               eba_type: TimeOfArrival::FLEET),
        create(:time_of_arrival,
               eba_type: TimeOfArrival::PARTNER),
      ]
    end

    let(:earliest_creation_date) { Time.now - 1.days }

    let(:expected_eta) do
      create(
        :time_of_arrival,
        eba_type: TimeOfArrival::BID,
        created_at: earliest_creation_date
      )
    end

    context 'with several types of etas' do
      it 'only selects BID, PARTNER, or SWOOP_ESTIMATE types' do
        expect(
          [
            TimeOfArrival::BID,
            TimeOfArrival::SWOOP_ESTIMATE,
            TimeOfArrival::PARTNER,
          ]
        ).to include(result.eba_type)
      end

      it 'selects the most recently created' do
        expect(result.id).to eq(expected_eta.id)
      end
    end
  end

  context 'when no job is found' do
    let(:job) { nil }
    let(:job_id) { 'nope' }

    it 'returns nil' do
      expect(result).to eq(nil)
    end
  end
end
