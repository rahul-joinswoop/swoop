# frozen_string_literal: true

require 'rails_helper'

describe MostRecentOnsiteJobStatusQuery do
  subject(:result) { MostRecentOnsiteJobStatusQuery.call(job.id) }

  let(:job) do
    create(
      :job,
      audit_job_statuses: audit_job_statuses
    )
  end

  context 'with zero ON_SITE AuditJobStatus' do
    let(:audit_job_statuses) do
      [create(:audit_job_status, job_status_id: JobStatus::ASSIGNED)]
    end

    describe 'querying for job_id' do
      it 'returns an empty relation' do
        expect(result).to eq([])
      end
    end
  end

  context 'with one ON_SITE AuditJobStatus' do
    let(:audit_job_statuses) do
      [
        create(:audit_job_status, job_status_id: JobStatus::ON_SITE),
        create(:audit_job_status, job_status_id: JobStatus::ASSIGNED),
      ]
    end

    describe 'querying for job_id' do
      it 'returns the AuditJobStatus record' do
        expect(result.job_status_id).to eq(JobStatus::ON_SITE)
      end
    end
  end

  context 'with multiple ON_SITE AuditJobStatus' do
    let(:most_recent_creation_time) { Time.now + 1.day }

    let(:expected_audit_job_status) do
      create(
        :audit_job_status,
        job_status_id: JobStatus::ON_SITE,
        created_at: most_recent_creation_time
      )
    end

    let(:audit_job_statuses) do
      [
        expected_audit_job_status,
        create(:audit_job_status, job_status_id: JobStatus::ON_SITE),
      ]
    end

    describe 'querying for job_id' do
      it 'returns the most recent AuditJobStatus record' do
        expect(result).to eq(expected_audit_job_status)
      end
    end
  end
end
