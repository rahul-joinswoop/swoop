# frozen_string_literal: true

require 'rails_helper'

describe JobAncestorsWithStatusesQuery do
  let(:statuses) { [] }

  context 'without a job' do
    describe 'querying for job ancestors' do
      let(:job_id) { 'nope' }

      it 'returns an empty relation' do
        result = JobAncestorsWithStatusesQuery.call(job_id, statuses: statuses)
        expect(result).to eq([])
      end
    end
  end

  context 'with a job having no parents' do
    subject(:result) do
      JobAncestorsWithStatusesQuery.call(job_id, statuses: statuses)
    end

    let(:job) do
      create(
        :job,
        status: Job::REASSIGNED
      )
    end

    let(:job_id) { job.id }

    context 'when no statuses are passed in' do
      context 'a job matches on id' do
        it 'returns an empty relation' do
          expect(result).to eq([])
        end
      end

      context 'no job matches on id' do
        it 'returns an empty relation' do
          result =
            JobAncestorsWithStatusesQuery.call('nope', statuses: statuses)
          expect(result).to eq([])
        end
      end

      context 'a job matches on statuses' do
        let(:statuses) { [Job::UNASSIGNED] }

        it 'returns an empty relation' do
          expect(result).to eq([])
        end
      end

      context 'when no job matches on statuses' do
        let(:statuses) { [Job::STATUS_CANCELED] }

        it 'returns an empty relation' do
          expect(result).to eq([])
        end
      end
    end
  end

  context 'with a job having less than n parents' do
    let(:job_with_parents) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent1
      )
    end

    let(:parent1) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent2
      )
    end

    let(:parent2) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent3
      )
    end

    let(:parent3) do
      create(
        :job,
        status: Job::UNASSIGNED,
      )
    end

    context 'a job matches on statuses' do
      let(:statuses) do
        [Job::STATUS_UNASSIGNED, Job::STATUS_REASSIGNED]
      end

      it 'returns all parents with the selected statuses' do
        result =
          JobAncestorsWithStatusesQuery
            .call(job_with_parents.id, statuses: statuses)

        expect(result.count).to eq(3)
      end
    end

    context 'no job matches on statuses' do
      let(:statuses) { [Job::STATUS_CANCELED] }

      it 'returns an empty relation' do
        result =
          JobAncestorsWithStatusesQuery
            .call(job_with_parents.id, statuses: statuses)

        expect(result).to eq([])
      end
    end
  end

  # Query object doesn't recurse up past RECURSION_LIMIT.
  context 'with n + 1 parents' do
    let(:job_with_parents) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent1
      )
    end

    let(:parent1) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent2
      )
    end

    let(:parent2) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent3
      )
    end

    let(:parent3) do
      create(
        :job,
        status: Job::UNASSIGNED,
        parent: parent4
      )
    end

    let(:parent4) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent5
      )
    end

    let(:parent5) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent6
      )
    end

    let(:parent6) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent7
      )
    end

    let(:parent7) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent8
      )
    end

    let(:parent8) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent9
      )
    end

    let(:parent9) do
      create(
        :job,
        status: Job::REASSIGNED,
        parent: parent10
      )
    end

    let(:parent10) do
      create(
        :job,
        status: Job::REASSIGNED
      )
    end

    context 'a job matches on statuses' do
      let(:statuses) do
        [Job::STATUS_REASSIGNED, Job::STATUS_UNASSIGNED]
      end

      it 'returns the nth ancestor' do
        result =
          JobAncestorsWithStatusesQuery
            .call(job_with_parents.id, statuses: statuses)

        expect(result.count).to eq(9)
      end
    end
  end
end
