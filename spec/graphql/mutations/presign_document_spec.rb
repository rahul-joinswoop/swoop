# frozen_string_literal: true

require 'rails_helper'

describe Mutations::PresignDocument do
  shared_examples "presignDocument" do
    let(:query) do
      <<~GRAPHQL
        mutation #{operation_name} {
          presignDocument(input: {}) {
            presignedDocument {
              id
              storage
              url
            }
          }
        }
      GRAPHQL
    end

    it "works" do
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
      presigned = response.dig('data', 'presignDocument', 'presignedDocument')
      expect(presigned['id']).to be_present
      expect(presigned['storage']).to eq('cache')
      expect(presigned['url']).to be_present
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    it_behaves_like "presignDocument"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    it_behaves_like "presignDocument"
  end
end
