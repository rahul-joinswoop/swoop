# frozen_string_literal: true

require 'rails_helper'

describe Mutations::CreateUser do
  let(:name) { Faker::Name.name }
  let(:phone) { Faker::PhoneNumber.phone_number_e164 }
  let(:email) { Faker::Internet.email }
  let(:user_response) { response.dig('data', 'createUser', 'user') }
  let(:query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: CreateUserInput!) {
        createUser(input: $input) {
          user {
            id
            name
            email
            phone
            roles {
              edges {
                node {
                  name
                }
              }
            }
          }
        }
      }
    GRAPHQL
  end

  shared_examples 'createUser' do
    shared_examples "executing create user" do
      it "creates the User accordingly" do
        expect { response }.to change(User, :count).by(1)
          .and avoid_raising_error
        expect(response).not_to include('errors')
      end
    end

    context "with name" do
      let(:variables) do
        {
          input: {
            user: {
              name: name,
            },
          },
        }
      end

      it_behaves_like "executing create user"

      it "works" do
        user = SomewhatSecureID.load! user_response.dig('id')
        expect(user.name).to eq(name)
      end
    end

    context "with phone, email, etc" do
      let(:variables) do
        {
          input: {
            user: {
              name: name,
              phone: phone,
              email: email,
            },
          },
        }
      end

      it_behaves_like "executing create user"

      it "works" do
        user = SomewhatSecureID.load! user_response.dig('id')
        expect(user.phone).to eq(phone)
        expect(user.email).to eq(email)
      end
    end

    context "roles" do
      let(:variables) do
        {
          input: {
            user: {
              name: name,
              roles: roles,
            },
          },
        }
      end

      context "with no roles" do
        let(:roles) { nil }

        it_behaves_like "executing create user"

        it "works" do
          user = SomewhatSecureID.load! user_response.dig('id')
          expect(user.human_roles).to eq(['rescue'])
        end
      end

      context "with roles" do
        let(:roles) { ['admin', 'dispatcher', 'driver'] }

        it_behaves_like "executing create user"

        it "works" do
          user = SomewhatSecureID.load! user_response.dig('id')
          expect(user.human_roles).to contain_exactly(*roles, 'rescue')
        end
      end

      context "with bad roles" do
        let(:roles) { Faker::Hipster.words }
        let(:errors_array) do
          {
            message: Analyzers::CreateUserAnalyzer::INVALID_ROLES_MESSAGE,
            extensions: {
              problems: roles.map { |r| Analyzers::CreateUserAnalyzer::INVALID_ROLE_PROBLEM % [r] },
            },
            path: ["mutation #{operation_name}", "createUser", "input", "user", "roles"],
            locations: [{ line: 2, column: 3 }],
          }
        end

        let(:show_errors) { false }

        it_behaves_like "responds with error"

        it 'does not create a user' do
          expect { response }.not_to change(User, :count)
        end
      end
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    let!(:api_user) { rescue_admin }

    it_behaves_like "createUser"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    let!(:api_user) { rescue_admin }

    it_behaves_like "createUser"
  end
end
