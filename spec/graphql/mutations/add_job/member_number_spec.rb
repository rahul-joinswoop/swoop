# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"

  shared_examples "member_number" do
    context "with Feature::JOB_MEMBER_NUMBER" do
      let(:job_input) { create(:graphql_job_input_type, :with_member_number, company: api_company) }
      let(:query) do
        qs = super().dup
        # insert memberNumber into our query
        qs.insert(qs.index(/^\s+?phone/), "memberNumber\n")
        qs
      end

      it_behaves_like "creates a job"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "member_number"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "member_number"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "member_number"
  end
end
