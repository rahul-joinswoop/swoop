# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"

  shared_examples 'fleet response' do
    context 'with a client.department' do
      let(:job_input) { create(:graphql_job_input_type, :with_client_department, company: api_company) }
      let(:query) do
        qs = super().dup
        # insert memberNumber into our query
        qs.insert(qs.index('service{'), "client { department { name } }\n")
        qs
      end

      let(:job_output) do
        super().deep_dup.tap do |j|
          j["client"]["department"] = { "name" => j["client"]["department"] }
        end
      end

      it_behaves_like "creates a job"
    end

    context 'with a client.site' do
      let(:job_input) { create(:graphql_job_input_type, :with_client_site, company: api_company) }
      let(:query) do
        qs = super().dup
        # insert memberNumber into our query
        qs.insert(qs.index('service{'), "client { site { name } }\n")
        qs
      end

      let(:job_output) do
        super().deep_dup.tap do |j|
          j["client"]["site"] = { "name" => j["client"]["site"] }
        end
      end

      it_behaves_like "creates a job"
    end

    # TODO - something is broken in this spec, when i add a context for paymentTypeName which is identical
    # to the below the specs always fail with unrelated errors (extra: nil showing up in question, exact: nil
    # showing up in addresses). see also the skip above, the same thing happened there. we should split this spec
    # up into smaller pieces also because it's so slow...
    context 'with a paymentType' do
      let(:job_input) { create(:graphql_job_input_type, :with_payment_type, company: api_company) }
      let(:query) do
        qs = super().dup
        # insert memberNumber into our query
        qs.insert(qs.index('service{'), "paymentType { name } \n")
        qs
      end

      let(:job_output) do
        super().deep_dup.tap do |j|
          j["paymentType"] = { "name" => j["paymentType"] }
        end
      end

      it_behaves_like "creates a job"
    end

    context 'with a priorityResponse' do
      let(:job_input) { create(:graphql_job_input_type, :with_priority_response, company: api_company) }
      let(:query) do
        qs = super().dup
        # insert memberNumber into our query
        qs.insert(qs.index('service{'), "priorityResponse\n")
        qs
      end

      let(:job_output) do
        super().deep_dup
        # .tap do |j|
        #   j["paymentType"]= { "name" => j["paymentType"] }
        # end
      end

      it_behaves_like "creates a job"
    end
  end

  # this spec is for Fleet Response who is a fleet motor club company?
  # TODO - should we be testing this type of company everywhere?
  context "as a client api_application" do
    let(:api_company) { fleet_motor_club_company }
    let(:api_application) { fleet_motor_club_application }
    let(:viewer) { api_application }

    it_behaves_like "fleet response"
  end
end
