# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"
  shared_examples 'classType' do
    let(:job_input) { create(:graphql_job_input_type, :with_class_type, company: api_company) }
    let(:query) do
      qs = super().dup
      # insert memberNumber into our query
      qs.insert(qs.index(/^\s+?willStore/), "classType { name }\n")
      qs
    end

    context "with a good class type" do
      let(:job_output) do
        super().deep_dup.tap do |j|
          j["service"]["classType"] = { "name" => j["service"]["classType"] }
        end
      end

      it_behaves_like "creates a job"
    end

    context "with a bad class type" do
      let(:classType) { Faker::Hipster.sentence }
      let(:job_input) { super().deep_dup.tap { |j| j[:service][:classType] = classType } }
      let(:errors_array) do
        {
          message: "#{classType.inspect} is not a valid VehicleClassTypeType",
          locations: [{ line: nil, column: nil }],
          path: ["mutation #{operation_name}", "addJob", "input", "job", "service", "classType"],
          extensions: {
            code: "argumentLiteralsIncompatible",
            typeName: "CoercionError",
          },
        }
      end

      it_behaves_like "responds with error"
    end
  end

  # only testing rescue company here for now - partner / swoop all behave differently depending on whether or not
  # they have the feature enabled, and i don't feel like writing out the whole spec while production is broken :/
  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "classType"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "classType"
  end
end
