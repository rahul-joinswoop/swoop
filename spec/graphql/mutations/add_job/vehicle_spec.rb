# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"
  describe "vehicle" do
    shared_examples "overflowing odometer value" do
      # make sure we handle odometer values > 32bits
      let(:odometer) { 3900067695.0 }
      let(:job_input) do
        create(:graphql_job_input_type, company: api_company).tap do |j|
          j[:vehicle][:odometer] = odometer
        end
      end

      it 'works' do
        expect(response).not_to include('errors')
        expect(response.dig('data', 'addJob', 'job', 'vehicle', 'odometer')).to eq(odometer.to_i.to_s)
      end
    end

    context "as a client api_application" do
      include_context "client api_application"
      it_behaves_like "overflowing odometer value"
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "overflowing odometer value"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "overflowing odometer value"
    end

    context "without make, model color" do
      let(:job_input) do
        job_input = create :graphql_job_input_type, company: api_company
        job_input[:vehicle].delete :make
        job_input[:vehicle].delete :model
        job_input[:vehicle].delete :color
        job_input
      end

      context "as a client api_application" do
        include_context "client api_application"
        let(:errors_array) do
          [:make, :model, :color].map do |m|
            {
              extensions: {
                problems: [{ explanation: Analyzers::Validations::Vehicle::ERROR_MESSAGE % [m.to_s.inspect] }],
              },
              locations: [{ line: 2, column: 3 }],
              message: Analyzers::Validations::Vehicle::ERROR_MESSAGE % [m.to_s.inspect],
              path: ["mutation #{operation_name}", "addJob", "input", "job", "vehicle"],
            }
          end
        end

        it_behaves_like "responds with error"
      end

      context "as a partner api_user" do
        include_context "partner api_user"
        it_behaves_like "creates a job"
      end

      context "as a 3rd party partner api_user" do
        include_context "3rd party partner api_user"
        it_behaves_like "creates a job"
      end
    end
  end
end
