# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"

  shared_examples "features" do
    context 'when Feature::JOB_PO_NUMBER is enabled' do
      before do
        api_company.features << create(:feature, name: Feature::JOB_PO_NUMBER)
      end

      context "with a po_number" do
        let(:job_input) { create :graphql_job_input_type, :with_po_number, company: api_company }
        let(:query) do
          qs = super().dup
          # insert poNumber into our query as a sibling of id
          qs.insert(qs.index(/^\s+?id/), "poNumber\n")
          qs
        end

        it_behaves_like "creates a job"
      end
    end

    context 'when Feature::JOB_REFERENCE_NUMBER is enabled' do
      before do
        api_company.features << create(:feature, name: Feature::JOB_REFERENCE_NUMBER)
      end

      context "with a ref_number" do
        let(:job_input) { create :graphql_job_input_type, refNumber: "A1234", company: api_company }
        let(:query) do
          qs = super().dup
          # insert refNumber into our query as a sibling of id
          qs.insert(qs.index(/^\s+?id/), "refNumber\n")
          qs
        end

        it_behaves_like "creates a job"
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "features"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "features"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "features"
  end
end
