# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"
  shared_examples "location" do
    let(:texts) { create :graphql_texts_input_type, sendEtaUpdates: false }
    let(:job_input) { create(:graphql_job_input_type, company: api_company, texts: texts) }

    context "bad location_type" do
      let(:location_type) { Faker::Hipster.sentence }
      let(:job_input) do
        super().deep_dup.tap { |j| j[:location][:serviceLocation][:locationType] = location_type }
      end
      let(:errors_array) do
        {
          extensions: { code: "argumentLiteralsIncompatible", typeName: "CoercionError" },
          locations: [{ column: nil, line: nil }],
          message: "#{location_type.inspect} is not a valid LocationType",
          path: [
            "mutation #{operation_name}",
            "addJob",
            "input",
            "job",
            "location",
            "serviceLocation",
            "locationType",
          ],
        }
      end

      it_behaves_like "responds with error"
    end

    context "with lat/lng and google id" do
      it_behaves_like "creates a job"
    end

    shared_examples "it works" do
      context "with lat/lng" do
        let(:job_input) { super().deeper_dup.tap { |i| i[:location][loc].delete :googlePlaceId } }

        it_behaves_like "creates a job"
      end

      context "with google_place_id" do
        let(:job_input) do
          super().deeper_dup.tap do |i|
            i[:location][loc].delete :lat
            i[:location][loc].delete :lng
          end
        end

        it_behaves_like "creates a job"
      end

      context "without google_place_id or lat/lng" do
        let(:job_input) do
          super().deeper_dup.tap do |i|
            i[:location][loc].delete :lat
            i[:location][loc].delete :lng
            i[:location][loc].delete :googlePlaceId
          end
        end
        let(:errors_array) do
          [{
            message: Analyzers::Validations::LocationInfo::EITHER_OR_ERROR_MESSAGE % [loc],
            extensions: {
              problems: [{
                explanation: Analyzers::Validations::LocationInfo::EITHER_OR_ERROR_MESSAGE % [loc],
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "addJob", "input", "job", "location", loc.to_s],
          }]
        end

        it_behaves_like "responds with error"
      end
    end

    context "dropoffLocation" do
      let(:loc) { :dropoffLocation }

      it_behaves_like "it works"
    end

    context "serviceLocation" do
      let(:loc) { :serviceLocation }

      it_behaves_like "it works"
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "location"

    context "without serviceLocation.locationType" do
      let(:job_input) { super().deeper_dup.tap { |i| i[:location][:serviceLocation].delete :locationType } }

      it_behaves_like "creates a job"
    end
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "location"

    context "without serviceLocation.locationType" do
      let(:job_input) { super().deeper_dup.tap { |i| i[:location][:serviceLocation].delete :locationType } }

      it_behaves_like "creates a job"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    # YUCK - have to duplicate this here because a few cases are slightly different and i couldn't figure out
    # an elegant way to share the same specs everywhere
    [:dropoffLocation, :serviceLocation].each do |loc|
      context "location" do
        let(:texts) { create :graphql_texts_input_type, sendEtaUpdates: false }
        let(:job_input) { create(:graphql_job_input_type, company: api_company, texts: texts) }

        context "bad location_type" do
          let(:location_type) { Faker::Hipster.sentence }
          let(:job_input) do
            super().deep_dup.tap { |j| j[:location][:serviceLocation][:locationType] = location_type }
          end
          let(:errors_array) do
            {
              extensions: { code: "argumentLiteralsIncompatible", typeName: "CoercionError" },
              locations: [{ column: nil, line: nil }],
              message: "#{location_type.inspect} is not a valid LocationType",
              path: [
                "mutation #{operation_name}",
                "addJob",
                "input",
                "job",
                "location",
                "serviceLocation",
                "locationType",
              ],
            }
          end

          it_behaves_like "responds with error"
        end

        context "with lat/lng and google id" do
          it_behaves_like "creates a job"
        end

        context "it works" do
          context "with lat/lng" do
            let(:job_input) { super().deeper_dup.tap { |i| i[:location][loc].delete :googlePlaceId } }

            it_behaves_like "creates a job"
          end

          context "with google_place_id" do
            let(:job_input) do
              super().deeper_dup.tap do |i|
                i[:location][loc].delete :lat
                i[:location][loc].delete :lng
              end
            end
            let(:errors_array) do
              [{
                message: Analyzers::Validations::LocationInfo::FLEET_MANAGED_ERROR_MESSAGE,
                extensions: {
                  problems: [{
                    explanation: Analyzers::Validations::LocationInfo::FLEET_MANAGED_ERROR_MESSAGE,
                  }],
                },
                locations: [{ column: 3, line: 2 }],
                path: ["mutation #{operation_name}", "addJob", "input", "job", "location", loc.to_s],
              }]
            end

            if loc == :serviceLocation
              it_behaves_like "creates a job"
            else
              it_behaves_like "responds with error"
            end
          end

          context "without google_place_id or lat/lng" do
            let(:job_input) do
              super().deeper_dup.tap do |i|
                i[:location][loc].delete :lat
                i[:location][loc].delete :lng
                i[:location][loc].delete :googlePlaceId
              end
            end

            if loc == :serviceLocation
              let(:errors_array) do
                [{
                  message: Analyzers::Validations::LocationInfo::EITHER_OR_ERROR_MESSAGE % [loc],
                  extensions: {
                    problems: [{
                      explanation: Analyzers::Validations::LocationInfo::EITHER_OR_ERROR_MESSAGE % [loc],
                    }],
                  },
                  locations: [{ column: 3, line: 2 }],
                  path: ["mutation #{operation_name}", "addJob", "input", "job", "location", loc.to_s],
                }]
              end
            else
              let(:errors_array) do
                [{
                  message: Analyzers::Validations::LocationInfo::FLEET_MANAGED_ERROR_MESSAGE,
                  extensions: {
                    problems: [{
                      explanation: Analyzers::Validations::LocationInfo::FLEET_MANAGED_ERROR_MESSAGE,
                    }],
                  },
                  locations: [{ column: 3, line: 2 }],
                  path: ["mutation #{operation_name}", "addJob", "input", "job", "location", loc.to_s],
                }]
              end
            end

            it_behaves_like "responds with error"
          end
        end
      end
    end

    context "without serviceLocation.locationType" do
      let(:job_input) { super().deeper_dup.tap { |i| i[:location][:serviceLocation].delete :locationType } }
      let(:errors_array) do
        [{
          message: Analyzers::Validations::ServiceLocationType::ERROR_MESSAGE,
          extensions: {
            problems: [{
              explanation: Analyzers::Validations::ServiceLocationType::ERROR_MESSAGE,
            }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "addJob", "input", "job", "location", "serviceLocation"],
        }]
      end

      it_behaves_like "responds with error"
    end
  end
end
