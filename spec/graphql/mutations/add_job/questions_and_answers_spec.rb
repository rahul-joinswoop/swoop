# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"

  shared_examples 'questions and answers' do
    before do
      api_company.features << create(:feature, name: Feature::QUESTIONS)
    end

    let(:job_input) { create :graphql_job_input_type, :with_answers, company: api_company }
    let(:variables) { { input: { job: job_input } } }

    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: AddJobInput!) {
        addJob(input: $input) {
          job {
            id
            service{
              answers{
                edges {
                  node {
                    question
                    answer
                    extra
                  }
                }
              }
            }
          }
        }
      }
      GRAPHQL
    end

    let(:questions_and_answers_output) do
      { edges: job_input.dig(:service, :answers).map { |a| { node: a } } }.with_indifferent_access
    end

    context 'with questions and answers' do
      it 'works' do
        expect { job_response }.to change(Job, :count).by(1)
        expect(job_response.dig("service", "answers")).to deep_include(questions_and_answers_output)
      end
    end

    context 'when required questions are missing' do
      let(:job_input) { create :graphql_job_input_type, company: api_company }
      let(:problems) do
        api_company
          .questions_for_service_code(ServiceCode.find_by(name: job_input[:service][:name]))
          .map { |q| { explanation: Analyzers::Validations::ServiceQuestions::ERROR_EXPLANATION % [q.question.inspect] } }
      end

      let(:errors_array) do
        {
          extensions: {
            problems: problems,
          },
          locations: [{ column: 3, line: 2 }],
          message: Analyzers::Validations::ServiceQuestions::ERROR_MESSAGE % ['Questions', job_input[:service][:name].inspect],
          path: ["mutation #{operation_name}", "addJob", "input", "job", "service", "answers"],
        }
      end

      it_behaves_like "responds with error"

      context 'with data from Root Insurance' do
        # Root Insurance did a few different interesting things that are worth testing:
        # 1. they named their mutation in their GraphQL but didn't actually provide an operationName query parameter (my GraphQL client adds operationName automatically)
        # 2. they used sub-input types under job instead of AddJobInputType - in the end this was fine but it's still worth having a spec
        #    for this case
        let(:query) do
          <<~GRAPHQL
          mutation SwoopIntegration__Graphql__RealCommunicator__ADD_JOB_MUTATION($customer: JobCustomerInput, $notes: NotesInput, $service: AddJobServiceInput!, $vehicle: VehicleInput, $location: AddJobJobLocationInput!, $texts: JobTextsInput) {
            addJob(input: { job: { customer: $customer, notes: $notes, service: $service, vehicle: $vehicle, location: $location, texts: $texts } }) {
              job { id notes { customer internal } service { name symptom answers { edges { node { question answer extra } } } } vehicle { make model year color license odometer vin } location { serviceLocation { address lat lng locationType } dropoffLocation { address lat lng locationType googlePlaceId } pickupContact { fullName phone } dropoffContact { fullName phone } } texts { sendLocation sendEtaUpdates sendDriverTracking sendReview sendTextsToPickup } }
            }
          }
          GRAPHQL
        end

        let(:variables) do
          {
            customer: job_input[:customer],
            notes: job_input[:notes],
            service: job_input[:service],
            vehicle: job_input[:vehicle],
            location: job_input[:location],
            texts: job_input[:texts],
          }
        end

        let(:service_input) do
          create :graphql_service_input_type,
                 :with_symptom,
                 willStore: false,
                 storageType: nil,
                 company: api_company
        end
        let(:operation_name) { nil }
        let(:job_input) do
          i = create :graphql_job_input_type, company: api_company, service: service_input
          i[:service].delete :willStore
          i[:service].delete :storageType
          i[:texts].delete :sendLocation
          i[:texts].delete :sendTextsToPickup
          i[:location][:serviceLocation].delete :googlePlaceId
          i[:location][:serviceLocation].delete :exact
          i[:location][:dropoffLocation].delete :exact
          i[:customer][:fullName] = i[:customer].delete :name
          i[:location][:pickupContact][:fullName] = i[:location][:pickupContact].delete :name
          i[:location][:dropoffContact][:fullName] = i[:location][:dropoffContact].delete :name
          i
        end

        let(:errors_array) do
          {
            extensions: {
              problems: problems,
            },
            locations: [{ column: 3, line: 2 }],
            message: Analyzers::Validations::ServiceQuestions::ERROR_MESSAGE % ['Questions', job_input[:service][:name].inspect],
            path: ["mutation SwoopIntegration__Graphql__RealCommunicator__ADD_JOB_MUTATION", "addJob", "input", "job", "service", "answers"],
          }
        end

        before do
          api_company.features << create(:feature, name: Feature::SHOW_SYMPTOMS)
          api_company.features << create(:feature, name: Feature::LEGACY_GRAPHQL_ERRORS)
        end

        it_behaves_like "responds with error"
      end
    end

    context 'with invalid input' do
      context "when the given question doesn't exist in the db" do
        let(:question) { Faker::Hipster.sentence }
        let(:job_input) do
          super().deep_dup.tap do |j|
            # lol, i don't know of an easier way to do this?
            @old_question = j[:service][:answers].first[:question]
            j[:service][:answers].first[:question] = question
          end
        end

        let(:errors_array) do
          [
            {
              extensions: {
                code: "argumentLiteralsIncompatible",
                typeName: "CoercionError",
              },
              locations: [{ column: 3, line: 2 }],
              message: Analyzers::Validations::Question::ERROR_MESSAGE % question.inspect,
              path: [
                "mutation #{operation_name}",
                "addJob",
                "input",
                "job",
                "service",
                "answers",
                0,
              ],
            },
            {
              extensions: {
                problems: [{
                  explanation: Analyzers::Validations::ServiceQuestions::ERROR_EXPLANATION % @old_question.inspect,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              message: Analyzers::Validations::ServiceQuestions::ERROR_MESSAGE % ['Question', job_input[:service][:name].inspect],
              path: [
                "mutation #{operation_name}",
                "addJob",
                "input",
                "job",
                "service",
                "answers",
              ],
            },
          ]
        end

        before(:each) do
          expect(Rollbar).not_to receive(:error)
        end

        it_behaves_like "responds with error"
      end

      context "when required questions or answers are not passed" do
        let(:job_input) { super().deep_dup }
        let!(:answers) { job_input[:service].delete(:answers) }
        let(:errors_array) do
          {
            extensions: {
              problems: answers.map { |qa| { explanation: Analyzers::Validations::ServiceQuestions::ERROR_EXPLANATION % [qa[:question].inspect] } },
            },
            locations: [{ column: 3, line: 2 }],
            message: Analyzers::Validations::ServiceQuestions::ERROR_MESSAGE % ['Questions', job_input[:service][:name].inspect],
            path: [
              "mutation #{operation_name}",
              "addJob",
              "input",
              "job",
              "service",
              "answers",
            ],
          }
        end

        it_behaves_like "responds with error"
      end

      context "when the given answer doesn't exist in the db" do
        let(:answer) { Faker::Hipster.sentence }
        let(:job_input) do
          super().deep_dup.tap { |j| j[:service][:answers].second[:answer] = answer }
        end

        let(:errors_array) do
          [
            {
              extensions: {
                code: "argumentLiteralsIncompatible",
                typeName: "CoercionError",
              },
              locations: [{ column: 3, line: 2 }],
              message: Analyzers::Validations::Answer::ERROR_MESSAGE % answer.inspect,
              path: [
                "mutation #{operation_name}",
                "addJob",
                "input",
                "job",
                "service",
                "answers",
                1,
              ],
            },
            {
              extensions: {
                problems: [{
                  explanation: Analyzers::Validations::ServiceQuestions::ERROR_EXPLANATION % job_input[:service][:answers].second[:question].inspect,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              message: Analyzers::Validations::ServiceQuestions::ERROR_MESSAGE % ['Question', job_input[:service][:name].inspect],
              path:
              [
                "mutation #{operation_name}",
                "addJob",
                "input",
                "job",
                "service",
                "answers",
              ],
            },
          ]
        end

        before(:each) do
          expect(Rollbar).not_to receive(:error)
        end

        it_behaves_like "responds with error"
      end
    end

    describe 'Tire Change' do
      let(:service_code) { create(:service_code, name: ServiceCode::TIRE_CHANGE) }
      let(:api_company) do
        c = super()
        c.features << create(:feature, name: Feature::QUESTIONS)
        c.companies_services << create(:companies_service, company: c, service_code: service_code)
        c
      end
      let(:job_input) { create :graphql_job_input_type, company: api_company, service: service_input }
      let(:variables) { { input: { job: job_input } } }
      let(:query) do
        <<~GRAPHQL
      mutation #{operation_name}($input: AddJobInput!) {
        addJob(input: $input) {
          job {
            id
          }
        }
      }
      GRAPHQL
      end

      let(:spare_question) { create :question, question: ::Question::HAVE_A_SPARE }
      let!(:answer_no) { create :answer, answer: ::Answer::NO, question: spare_question }
      let!(:answer_yes) { create :answer, answer: ::Answer::YES, question: spare_question }

      let(:tire_question) { create :question, question: ::Question::WHICH_TIRE }
      let!(:answer_multiple) { create :answer, answer: ::Answer::DAMAGED_TIRE_MULTIPLE, question: tire_question }
      let!(:answer_front_driver) { create :answer, answer: ::Answer::DAMAGED_TIRE_FRONT_DRIVER, question: tire_question }

      context "spare tire" do
        describe "no" do
          let(:service_input) do
            s = create(:graphql_service_input_type, company: api_company, service_code: service_code)
            s[:answers] = [{
              question: spare_question.question,
              answer: answer_no.answer,
            }]
            s
          end

          let(:errors_array) do
            {
              message: Analyzers::Validations::TireChangeService::ERROR_MESSAGE,
              locations: [{ line: 2, column: 3 }],
              path: ["mutation #{operation_name}", "addJob", "input", "job", "service", "name"],
              extensions: {
                problems: [{
                  explanation: Analyzers::Validations::TireChangeService::NO_SPARE_EXPLANATION,
                }],
              },
            }
          end

          it_behaves_like "responds with error"
        end

        describe "yes" do
          let(:service_input) do
            s = create(:graphql_service_input_type, company: api_company, service_code: service_code)
            s[:answers] = [{
              question: spare_question.question,
              answer: answer_yes.answer,
            }]
            s
          end

          it_behaves_like "does not raise error"
        end
      end

      context "multiple flat tires" do
        describe "yes" do
          let(:service_input) do
            create(:graphql_service_input_type, company: api_company, service_code: service_code)
              .merge(answers: [{
                question: tire_question.question,
                answer: answer_multiple.answer,
              }])
          end

          let(:errors_array) do
            {
              message: Analyzers::Validations::TireChangeService::ERROR_MESSAGE,
              locations: [{ line: 2, column: 3 }],
              path: ["mutation #{operation_name}", "addJob", "input", "job", "service", "name"],
              extensions: {
                problems: [{
                  explanation: Analyzers::Validations::TireChangeService::MULTI_FLAT_EXPLANATION,
                }],
              },
            }
          end

          it_behaves_like "responds with error"
        end

        describe "no" do
          let(:service_input) do
            create(:graphql_service_input_type, company: api_company, service_code: service_code)
              .merge(answers: [{
                question: tire_question.question,
                answer: answer_front_driver.answer,
              }])
          end

          it_behaves_like "does not raise error"
        end
      end

      context "no spare tire and multiple flat tires" do
        let(:service_input) do
          create(:graphql_service_input_type, company: api_company, service_code: service_code)
            .merge(answers: [
              {
                question: spare_question.question,
                answer: answer_no.answer,
              },
              {
                question: tire_question.question,
                answer: answer_multiple.answer,
              },
            ])
        end

        let(:errors_array) do
          {
            message: Analyzers::Validations::TireChangeService::ERROR_MESSAGE,
            locations: [{ line: 2, column: 3 }],
            path: ["mutation #{operation_name}", "addJob", "input", "job", "service", "name"],
            extensions: {
              problems: [
                {
                  explanation: Analyzers::Validations::TireChangeService::NO_SPARE_EXPLANATION,
                },
                {
                  explanation: Analyzers::Validations::TireChangeService::MULTI_FLAT_EXPLANATION,
                },
              ],
            },
          }
        end

        it_behaves_like "responds with error"
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like 'questions and answers'
  end
end
