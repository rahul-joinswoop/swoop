# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"

  shared_examples "service" do
    describe "scheduledFor" do
      let(:job_input) do
        super().deep_dup.tap { |j| j[:service][:scheduledFor] = scheduled_for.iso8601 }
      end

      context "with an invalid scheduled_for" do
        let(:scheduled_for) { Time.current }
        let(:errors_array) do
          {
            extensions: { problems: [{ explanation: Analyzers::Validations::ServiceScheduledFor::ERROR_MESSAGE }] },
            locations: [{ column: 3, line: 2 }],
            message: Analyzers::Validations::ServiceScheduledFor::ERROR_MESSAGE,
            path: [
              "mutation #{operation_name}",
              "addJob",
              "input",
              "job",
              "service",
              "scheduledFor",
            ],
          }
        end

        it_behaves_like "responds with error"
      end

      context "with a valid scheduled_for" do
        let(:scheduled_for) { Time.current + 3.hours }

        it_behaves_like "creates a job"
      end
    end

    # give a service that's not in the db
    describe "service" do
      let(:service) { Faker::Hipster.sentence }
      let(:job_input) do
        super().deep_dup.tap { |j| j[:service][:name] = service }
      end
      let(:errors_array) do
        {
          extensions: { code: "argumentLiteralsIncompatible", typeName: "CoercionError" },
          locations: [{ column: nil, line: nil }],
          message: "#{service.inspect} is not a valid Service",
          path: ["mutation #{operation_name}", "addJob", "input", "job", "service", "name"],
        }
      end

      it_behaves_like "responds with error"
    end

    describe "when storageType does not exist in the DB" do
      let(:storage_type) { Faker::Hipster.sentence }
      let(:job_input) do
        super().deep_dup.tap { |j| j[:service][:storageType] = storage_type }
      end
      let(:errors_array) do
        {
          extensions: { code: "argumentLiteralsIncompatible", typeName: "CoercionError" },
          locations: [{ column: nil, line: nil }],
          message: "#{storage_type.inspect} is not a valid StorageType",
          path: [
            "mutation #{operation_name}",
            "addJob",
            "input",
            "job",
            "service",
            "storageType",
          ],
        }
      end

      it_behaves_like "responds with error"
    end

    # don't provide a dropoffLocation when the service requires one
    describe "dropoffLocation" do
      let!(:service_code) do
        create :service_code, name: (api_company.is_a?(RescueCompany) ? ServiceCode::RESCUE_DROP_LOCATION_SERVICE_CODES : ServiceCode::FLEET_DROP_LOCATION_SERVICE_CODES).sample
      end
      let!(:companies_service) do
        create :companies_service, company: api_company, service_code: service_code
      end

      let(:job_input) do
        super().deep_dup.tap do |j|
          j[:service][:name] = service_code.name
          j[:location].delete :dropoffLocation
        end
      end

      let(:errors_array) do
        {
          extensions: { problems: [{ explanation: Analyzers::Validations::DropLocation::ERROR_EXPLANATION % [service_code.name.inspect] }] },
          locations: [{ column: 3, line: 2 }],
          message: Analyzers::Validations::DropLocation::ERROR_MESSAGE % [service_code.name.inspect],
          path: [
            "mutation #{operation_name}",
            "addJob",
            "input",
            "job",
            "location",
          ],
        }
      end

      it_behaves_like "responds with error"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "service"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "service"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "service"
  end
end
