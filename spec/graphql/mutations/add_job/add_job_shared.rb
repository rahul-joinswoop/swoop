# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "add job shared" do
  let(:user) { create :user }
  let(:job_input) { create :graphql_job_input_type, company: api_company }
  let(:variables) { { input: { job: job_input } } }
  # when a rescue company creates a non-draft job it automatically goes to dispatched
  let(:status) { api_company.is_a?(RescueCompany) ? 'Dispatched' : 'Pending' }
  let(:query) do
    <<~GRAPHQL
    mutation #{operation_name}($input: AddJobInput!) {
      addJob(input: $input) {
        job {
          id
          status
          statusUpdatedAt
          customer {
            id
            fullName
            name
            phone
          }

          notes{
            customer
            internal
          }

          service{
            name
            scheduledFor
            answerBy
            willStore
            storageType
          }

          partner {
            driver {
              name
            }
          }

          vehicle{
            make
            model
            year
            color
            license
            odometer
            vin
          }
          location{
            serviceLocation{
              address
              lat
              lng
              locationType
              googlePlaceId
              exact
            }
            dropoffLocation{
              address
              lat
              lng
              locationType
              googlePlaceId
              exact
            }
            pickupContact {
              fullName
              name
              phone
            }
            dropoffContact {
              fullName
              name
              phone
            }
          }

          texts{
            sendLocation
            sendEtaUpdates
            sendDriverTracking
            sendReview
            sendTextsToPickup
          }
        }
      }
    }
    GRAPHQL
  end
  let(:job_output) do
    job_input.deep_dup.tap do |j|
      # j[:service][:classType] = { name: j[:service][:classType]  }
      # see comment below
      [:serviceLocation, :dropoffLocation].each do |k|
        j[:location][k][:address] = j[:location][k][:address].clone.gsub(/\, USA/, '')
      end

      [:pickupContact, :dropoffContact].each do |k|
        j[:location][k][:fullName] = j[:location][k][:name].clone
      end

      j[:vehicle][:odometer] = j[:vehicle][:odometer].to_i.to_s
      # customer can be nil (see JobType)
      if j[:customer]
        j[:customer][:fullName] = j[:customer][:name]
      end
    end.deep_stringify_keys
  end

  # check job output
  let(:job_response) { response.dig('data', 'addJob', 'job') }

  before(:each) do
    # mock this, otherwise we end up doing lookups for the timezone of our
    # randomly generated lat/lng coordinates
    allow_any_instance_of(External::GoogleTimezoneService).to receive(:call).and_return("UTC")
  end

  shared_examples "creates a job" do
    it "works" do
      create_analytics_sent = false

      expect(Analytics).to receive(:track) do |args|
        # yuck, so hard to filter out unwanted events. in this case we only want
        # to match 'add job'
        if args[:event] == 'Create Job'
          create_analytics_sent = true
          expect(args).to deep_include({
            # we can't test for job_id here because we don't have it yet
            user_id: api_company.to_ssid,
            event: 'Create Job',
            properties: {
              category: 'Jobs',
              platform: 'React iOS',
              company: api_company.name,
            },
          })
        end
      end.at_least(:once)
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
      expect(create_analytics_sent).to be_truthy

      id = job_response.dig('id')

      j = SomewhatSecureID.load!(id)
      expect(j).to be_present
      # initial state should be pending
      expect(j.status).to eq(status.downcase)

      # sometimes our addresses get the 'USA' portion dropped. i don't know why
      # this is happening and i haven't been able to track it down so for now
      # just strip that piece off so the comparisons will always work
      ['serviceLocation', 'dropoffLocation'].each do |k|
        job_response['location'][k]['address'] = job_response['location'][k]['address'].to_s.clone.gsub(/\, USA/, '')
      end

      expect(job_response.without(['id'])).to deep_include(job_output)
    end
  end
end
