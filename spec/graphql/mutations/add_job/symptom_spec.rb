# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"
  describe "symptom" do
    let(:job_input) do
      create :graphql_job_input_type,
             company: api_company,
             service: create(:graphql_service_input_type, :with_symptom, company: api_company)
    end
    let(:query) do
      qs = super().dup
      # insert symptom into our query as a sibling of scheduledFor
      qs.insert(qs.index(/^\s+?scheduledFor/), "symptom\n ")
      qs
    end

    before do
      api_company.features << create(:feature, name: Feature::SHOW_SYMPTOMS)
    end

    shared_examples "it works" do
      context 'and a valid symptom is passed' do
        it_behaves_like "creates a job"
      end

      context "when the symptom does not exist" do
        let(:symptom) { Faker::Hipster.sentence }
        let(:job_input) do
          super().deep_dup.tap { |j| j[:service][:symptom] = symptom }
        end
        let(:errors_array) do
          {
            extensions: { code: "argumentLiteralsIncompatible", typeName: "CoercionError" },
            locations: [{ column: nil, line: nil }],
            message: "#{symptom.inspect} is not a valid Symptom",
            path: ["mutation #{operation_name}", "addJob", "input", "job", "service", "symptom"],
          }
        end

        it_behaves_like "responds with error"
      end
    end

    context "as a client api_application" do
      include_context "client api_application"

      it_behaves_like "it works"

      context "without a symptom" do
        let(:job_input) { super().deeper_dup.tap { |i| i[:service].delete :symptom } }
        let(:errors_array) do
          {
            extensions: { problems: [{ explanation: Analyzers::Validations::ServiceSymptom::ERROR_MESSAGE }] },
            locations: [{ column: 3, line: 2 }],
            message: Analyzers::Validations::ServiceSymptom::ERROR_MESSAGE,
            path: ["mutation #{operation_name}", "addJob", "input", "job", "service"],
          }
        end

        it_behaves_like "responds with error"
      end
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "it works"
      context "without a symptom" do
        let(:job_input) { super().deeper_dup.tap { |i| i[:service].delete :symptom } }

        it_behaves_like "creates a job"
      end
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "it works"
      context "without a symptom" do
        let(:job_input) { super().deeper_dup.tap { |i| i[:service].delete :symptom } }

        it_behaves_like "creates a job"
      end
    end
  end
end
