# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"

  shared_examples 'caller' do
    let(:job_input) do
      input = create(:graphql_job_input_type, company: api_company)

      # have no idea but create(:graphql_job_input_type, :with_caller) is not appending
      # caller into the job_input (and it should!), so let's do it manually
      input[:caller] = create(:graphql_caller_input_type)

      input
    end

    context 'with a name' do
      let(:query) do
        qs = super().deep_dup
        # insert caller into our query
        qs.insert(qs.index(/^\s+?status/), "caller { name }\n")
        qs
      end
      let(:job_input) { super().deep_dup.tap { |j| j[:caller].delete :phone } }

      it_behaves_like "creates a job"
    end

    context 'with a name and a phone' do
      let(:query) do
        qs = super().deep_dup
        # insert caller into our query
        qs.insert(qs.index(/^\s+?status/), "caller { name phone }\n")
        qs
      end

      it_behaves_like "creates a job"
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "caller"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "caller"
  end
end
