# frozen_string_literal: true

require_relative './add_job_shared'

describe Mutations::AddJob do
  include_context "add job shared"

  shared_examples "storageSite" do
    let(:with_storage_site) { true }
    let(:job_response) { super().deep_dup.tap { |r| r["location"]["dropoffLocation"] = {} } }
    let(:job_output) { super().deep_dup.tap { |j| j["location"]["dropoffLocation"] = {} } }
    let(:storage_site) { create(:site, company: api_company, location: create(:location, :random)) }
    let(:job_input) do
      create(:graphql_job_input_type, :with_storage_site, company: api_company, storage_site: storage_site)
    end
    let(:query) do
      qs = super().dup
      # insert storageSite into our query
      qs.insert(qs.index(/^\s+?storageType/), "storageSite\n")
      qs
    end

    it_behaves_like "creates a job"

    it 'sets job.service.dropoffLocation with storageSite.location' do
      job_response = response.dig('data', 'addJob', 'job')

      job = SomewhatSecureID.load! job_response['id']
      expect(job.drop_location.site_id).to eq storage_site.id

      location_attrs = [:address, :lat, :lng, :exact, :place_id, :location_type_id]
      expect(job.drop_location.attributes.symbolize_keys.slice(location_attrs)).to match(
        storage_site.location.attributes.symbolize_keys.slice(location_attrs)
      )
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "storageSite"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "storageSite"
  end
end
