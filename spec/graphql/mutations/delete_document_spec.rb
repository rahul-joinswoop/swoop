# frozen_string_literal: true

require 'rails_helper'

describe Mutations::DeleteDocument, freeze_time: true do
  shared_examples 'deleteDocument' do
    let(:job) { create :job, rescue_company: api_company, fleet_company: fleet_managed_company }
    let(:document) { create :location_image, job: job, user: api_user }

    let(:variables) do
      {
        input: {
          document: {
            id: document.to_ssid,
          },
        },
      }
    end

    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: DeleteDocumentInput!) {
        deleteDocument(input: $input) {
          document {
            id
            deletedAt
          }
        }
      }
      GRAPHQL
    end

    it "deletes the Document" do
      expect { response && document.reload }.to avoid_raising_error
        .and change(document, :deleted_at).from(nil).to(Time.now)
      expect(response).not_to include('errors')
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    it_behaves_like "deleteDocument"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    it_behaves_like "deleteDocument"
  end
end
