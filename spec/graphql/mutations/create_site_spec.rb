# frozen_string_literal: true

require 'rails_helper'

describe Mutations::CreateSite do
  shared_examples "createSite" do
    let(:site_name) { Faker::Hipster.sentence }
    let(:variables) do
      {
        input: {
          site: {
            name: site_name,
            location: location,
          },
        },
      }
    end

    let(:site_response) { response.dig('data', 'createSite', 'site') }

    shared_examples "executing create site" do
      it "creates the Site accordingly" do
        expect { response }.to change(Site, :count).by(1)
          .and avoid_raising_error
        expect(response).not_to include('errors')
      end
    end

    context "with lat/lng" do
      let(:location) { create :graphql_location_input_type, :with_lat_and_lng_only }
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: CreateSiteInput!) {
          createSite(input: $input) {
            site {
              id
              name
              location {
                lat
                lng
              }
            }
          }
        }
        GRAPHQL
      end

      it_behaves_like "executing create site"

      it "works" do
        site = SomewhatSecureID.load! site_response.dig('id')
        expect(site.location.lat).to eq(location[:lat])
        expect(site.location.lng).to eq(location[:lng])
        expect(site.name).to eq(site_name)
      end
    end

    context "with googlePlaceId" do
      let(:location) { create :graphql_location_input_type, :with_google_place_id_only }
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: CreateSiteInput!) {
          createSite(input: $input) {
            site {
              id
              name
              location {
                googlePlaceId
              }
            }
          }
        }
        GRAPHQL
      end

      it_behaves_like "executing create site"

      it "works" do
        site = SomewhatSecureID.load! site_response.dig('id')
        expect(site.location.place_id).to eq(location[:googlePlaceId])
        expect(site.name).to eq(site_name)
      end

      context "with address" do
        let(:location) { create :graphql_location_input_type, :with_address_only }
        let(:query) do
          <<~GRAPHQL
          mutation #{operation_name}($input: CreateSiteInput!) {
            createSite(input: $input) {
              site {
                id
                name
                location {
                  address
                }
              }
            }
          }
          GRAPHQL
        end

        it_behaves_like "executing create site"

        it "works" do
          site = SomewhatSecureID.load! site_response.dig('id')
          expect(site.location.address).to eq(location[:address])
          expect(site.name).to eq(site_name)
        end
      end

      context "with full location input" do
        let(:location) { create :graphql_location_input_type, :with_google_place_id }
        let(:query) do
          <<~GRAPHQL
          mutation #{operation_name}($input: CreateSiteInput!) {
            createSite(input: $input) {
              site {
                id
                name
                location {
                  address
                  lat
                  lng
                  googlePlaceId
                }
              }
            }
          }
          GRAPHQL
        end

        it_behaves_like "executing create site"

        it "works" do
          site = SomewhatSecureID.load! site_response.dig('id')
          expect(site.location.address).to eq(location[:address])
          expect(site.location.lat).to eq(location[:lat])
          expect(site.location.lng).to eq(location[:lng])
          expect(site.location.place_id).to eq(location[:googlePlaceId])
          expect(site.name).to eq(site_name)
        end
      end

      context "with empty location input" do
        let(:location) { {} }
        let(:query) do
          <<~GRAPHQL
          mutation #{operation_name}($input: CreateSiteInput!) {
            createSite(input: $input) {
              site {
                id
                name
                location {
                  address
                  lat
                  lng
                  googlePlaceId
                }
              }
            }
          }
          GRAPHQL
        end

        let(:errors_array) do
          {
            message: "One of lat/lng, googlePlaceId or address is required",
            locations: [{ line: 2, column: 3 }],
            path: ["mutation #{operation_name}", "createSite", "input", "site", "location"],
            extensions: {
              problems: [{ explanation: "One of lat/lng, googlePlaceId or address is required" }],
            },
          }
        end

        it_behaves_like "responds with error"
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    let(:api_user) { rescue_admin }
    let!(:api_company) { rescue_company }

    it_behaves_like "createSite"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_admin }
    let!(:api_company) { rescue_company }

    it_behaves_like "createSite"
  end
end
