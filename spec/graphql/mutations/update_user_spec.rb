# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdateUser do
  shared_examples 'updateUser' do
    let(:show_errors) { false }
    let(:encoded_job_id) { job.to_ssid }
    let(:user) { create(:user, company: api_company, roles: roles, on_duty: nil) }
    let(:user_id) { user.to_ssid }

    let(:onDuty) { nil }

    let(:variables) do
      {
        input: {
          user: {
            id: user_id,
            onDuty: onDuty,
          },
        },
      }
    end

    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: UpdateUserInput!) {
        updateUser(input: $input) {
          user {
            onDuty
            vehicle {
              id
            }
          }
        }
      }
      GRAPHQL
    end

    describe "vehicle" do
      let(:roles) { [:rescue, :driver] }
      let(:api_user) { user }
      let(:vehicle) { create :rescue_vehicle, company: api_company }

      context "vehicle" do
        let(:user) { super().tap { |u| u.update! vehicle: vehicle } }

        context "with a vehicle that belongs to another company" do
          let(:vehicle2) { create :rescue_vehicle }

          context "with an id" do
            context "changing a vehicle" do
              let(:variables) { super().deep_dup.tap { |v| v[:input][:user][:vehicle] = { id: vehicle2.to_ssid } } }

              it "works" do
                expect(response).to include('errors')
                expect(response.dig('errors', 0, 'message')).to eq("Couldn't find PartnerVehicle with id: #{vehicle2.to_ssid.inspect}")
                expect { user.reload }.not_to change(user, :vehicle)
              end
            end
          end

          context "with a name" do
            context "changing a vehicle" do
              let(:variables) { super().deep_dup.tap { |v| v[:input][:user][:vehicle] = { name: vehicle2.name } } }

              it "works" do
                expect(response.dig('errors', 0, 'message')).to eq("Couldn't find PartnerVehicle with name: #{vehicle2.name.inspect}")
                expect { user.reload }.not_to change(user, :vehicle)
              end
            end
          end
        end

        context "with a vehicle that belongs to the user's company" do
          let(:vehicle2) { create :rescue_vehicle, company: api_company }

          context "with an id" do
            context "changing a vehicle" do
              let(:variables) { super().deep_dup.tap { |v| v[:input][:user][:vehicle] = { id: vehicle2.to_ssid } } }
              let!(:jobs) { create_list :job, 3, status: :enroute, rescue_driver: user }

              it "works" do
                expect { response && user.reload }.to change(user, :vehicle).from(vehicle).to(vehicle2)
                expect(AssignRescueDriverVehicleToJobWorker).to have_enqueued_sidekiq_job(jobs.map(&:id))
              end
            end

            context "clearing a vehicle" do
              let(:variables) { super().deep_dup.tap { |v| v[:input][:user][:vehicle] = { id: nil } } }

              it "works" do
                expect { response && user.reload }.to change(user, :vehicle).from(vehicle).to(nil)
              end
            end
          end

          context "with a name" do
            context "changing a vehicle" do
              let(:variables) { super().deep_dup.tap { |v| v[:input][:user][:vehicle] = { name: vehicle2.name } } }

              it "works" do
                expect { response && user.reload }.to change(user, :vehicle).from(vehicle).to(vehicle2)
              end
            end

            context "clearing a vehicle" do
              let(:variables) { super().deep_dup.tap { |v| v[:input][:user][:vehicle] = { name: nil } } }

              it "works" do
                expect { response && user.reload }.to change(user, :vehicle).from(vehicle).to(nil)
              end
            end
          end

          context "with a name and an id" do
            let(:variables) do
              super().deep_dup.tap { |v| v[:input][:user][:vehicle] = { id: vehicle2.to_ssid, name: vehicle2.name } }
            end

            it "works" do
              expect(response.dig('errors', 0, 'message')).to eq(Analyzers::UpdateUserAnalyzer::VEHICLE_ID_AND_NAME_ERROR_MESSAGE)
              expect { user.reload }.not_to change(user, :vehicle)
            end
          end
        end
      end
    end

    shared_examples "executing partner user update" do |initial_on_duty, final_on_duty|
      it "does not throw error" do
        expect { response }.not_to raise_error
      end

      it "does not include errors on response" do
        expect(response).not_to include('errors')
      end

      it "updates on_duty accordingly" do
        expect(user.on_duty).to eq(initial_on_duty)

        user_from_response = response.dig('data', 'updateUser', 'user').with_indifferent_access
        user.reload

        expect(user_from_response[:onDuty]).to eq(final_on_duty)
        expect(user.on_duty).to eq(final_on_duty)
      end
    end

    context "when partner users are logged in" do
      let(:roles) { [:rescue, :driver] }

      context 'and they are updating themselves' do
        let(:api_user) { user }

        context 'and onDuty is passed as true' do
          let(:onDuty) { true }

          it_behaves_like "executing partner user update", nil, true
        end

        context 'and onDuty is passed as false' do
          let(:onDuty) { false }

          it_behaves_like "executing partner user update", nil, false
        end
      end

      context 'but they are trying to update another user' do
        let(:api_user) { create(:user, full_name: 'Another Driver', company: api_company, roles: roles, on_duty: nil) }
        let(:errors_array) do
          [
            {
              message: Analyzers::UpdateUserAnalyzer::USER_NOT_AUTHORIZED_ERROR_MESSAGE,
              extensions: {
                problems: [{
                  explanation: "Viewer is not authorized to update this User",
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "updateUser", "input", "user", "id"],
            },
          ]
        end

        it_behaves_like "responds with error"
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    context "as a driver" do
      let(:api_user) { rescue_driver }

      it_behaves_like "updateUser"
    end

    context "as an admin" do
      let(:api_user) { rescue_admin }

      it_behaves_like "updateUser"
    end

    context "as a dispatcher" do
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "updateUser"
    end
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    context "as a driver" do
      let(:api_user) { rescue_driver }

      it_behaves_like "updateUser"
    end

    context "as an admin" do
      let(:api_user) { rescue_admin }

      it_behaves_like "updateUser"
    end

    context "as a dispatcher" do
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "updateUser"
    end
  end
end
