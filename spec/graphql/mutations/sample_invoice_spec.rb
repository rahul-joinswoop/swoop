# frozen_string_literal: true

require 'rails_helper'

describe Mutations::SampleInvoice do
  shared_examples 'sampleInvoice' do
    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: SampleInvoiceInput!) {
        sampleInvoice(input: $input) {
          invoice {
            id
            balance
            classType
            currency
            job {
              id
            }
            lineItems {
              edges {
                node {
                  id
                  autoCalculatedQuantity
                  calcType
                  currency
                  deletedAt
                  description
                  estimated
                  netAmount
                  originalQuantity
                  originalUnitPrice
                  quantity
                  rate {
                    id
                  }
                  job {
                    id
                  }
                  taxAmount
                  unitPrice
                  userCreated
                }
              }
            }
            notes
            paid
            payments {
              edges {
                node {
                  id
                  amount
                  currency
                  memo
                  paymentMethod
                  type
                }
              }
            }
            rateType {
              name
              type
            }
            status
            totalAmount
          }
        }
      }
      GRAPHQL
    end

    let(:variables) do
      {
        input: {
          invoice: {
            id: original_invoice.id,
            balance: original_invoice.balance.to_s,
            classType: new_class_type,
            job: {
              id: job_ssid,
            },
            notes: "Driver notes changed by user",
            lineItems: line_items_input,
            payments: [
              {
                id: original_invoice.payments_or_credits.first.to_ssid,
                amount: original_invoice.payments_or_credits.first.total_amount.to_s,
                deletedAt: nil,
                markPaidAt: nil,
                memo: original_invoice.payments_or_credits.first.description,
                paymentMethod: original_invoice.payments_or_credits.first.payment_method,
                type: original_invoice.payments_or_credits.first.type,
              },
            ],
            rateType: { type: new_hours_p2p_rate_type&.friendly_type },
            subtotal: original_invoice.subtotal.to_s,
            totalAmount: original_invoice.total_amount.to_s,
          },
        },
      }
    end

    let(:line_items_input) { original_line_items_input }

    let(:original_line_items_input) do
      [
        {
          id: original_li_flat.to_ssid,
          autoCalculatedQuantity: false,
          calcType: "Flat",
          deletedAt: nil,
          description: original_li_flat.description,
          estimated: false,
          job: {
            id: job.to_ssid,
          },
          originalQuantity: original_li_flat.original_quantity.to_s,
          originalUnitPrice: original_li_flat.original_unit_price.to_s,
          quantity: "2.00", # simulate quantity change
          rate: { id: flat_rate.to_ssid },
          taxAmount: "0.00",
          unitPrice: "2000.00", # simulate unitPrice change
          netAmount: "4000.00", # it will update the netAmount, so let's check it
          userCreated: false,
        },
        {
          id: original_li_addition_dolly.to_ssid,
          autoCalculatedQuantity: false,
          calcType: "Flat",
          deletedAt: nil,
          description: original_li_addition_dolly.description,
          estimated: false,
          job: {
            id: job.to_ssid,
          },
          originalQuantity: original_li_addition_dolly.original_quantity.to_s,
          originalUnitPrice: original_li_addition_dolly.original_unit_price.to_s,
          quantity: original_li_addition_dolly.quantity.to_s,
          rate: { id: flat_rate.to_ssid },
          taxAmount: original_li_addition_dolly.tax_amount.to_s,
          unitPrice: original_li_addition_dolly.unit_price.to_s,
          netAmount: original_li_addition_dolly.net_amount.to_s,
          userCreated: false,
        },
        {
          id: original_li_tax.to_ssid,
          autoCalculatedQuantity: false,
          calcType: "Flat",
          deletedAt: nil,
          description: original_li_tax.description,
          estimated: false,
          job: {
            id: job.to_ssid,
          },
          originalQuantity: original_li_tax.original_quantity.to_s,
          originalUnitPrice: original_li_tax.original_unit_price.to_s,
          quantity: original_li_tax.quantity.to_s,
          rate: { id: flat_rate.to_ssid },
          taxAmount: original_li_tax.tax_amount.to_s,
          unitPrice: original_li_tax.unit_price.to_s,
          netAmount: original_li_tax.net_amount.to_s,
          userCreated: false,
        },
      ]
    end

    let(:job_ssid) { job.to_ssid }

    let(:original_invoice) do
      create(
        :invoice, :with_payments, :with_refunds,
        currency: 'USD',
        line_items: [original_li_flat, original_li_addition_dolly, original_li_tax],
        recipient_company: fleet_company,
        sender: rescue_company,
        state: 'partner_new',
        rate_type: rate_type.type,
      )
    end

    let!(:job) do
      job = original_invoice.job
      job.rescue_company = rescue_company
      job.fleet_company = fleet_company
      job.account = account
      job.invoice = original_invoice
      job.invoice_vehicle_category = invoice_vehicle_category
      job.driver_notes = 'Original Driver Notes'

      original_invoice.job = job
      original_invoice.line_items.each do |li|
        li.job_id = job.id
        li.save!
      end

      original_invoice.save!
      job.save!

      original_invoice.reload
      job.reload

      job
    end

    let(:original_li_flat) do
      create(
        :invoicing_line_item,
        description: "Flat Rate",
        estimated: true,
        net_amount: BigDecimal("10.0"),
        original_quantity: BigDecimal("1.0"),
        original_unit_price: BigDecimal("10.0"),
        quantity: BigDecimal("1.0"),
        tax_amount: BigDecimal("0.0"),
        unit_price: BigDecimal("10.0"),
        user_created: false,
        rate_id: flat_rate.id,
      )
    end

    let(:original_li_addition_dolly) do
      create(
        :invoicing_line_item,
        description: "Dolly",
        estimated: true,
        net_amount: BigDecimal("5.0"),
        original_quantity: BigDecimal("1.0"),
        original_unit_price: BigDecimal("5.0"),
        quantity: BigDecimal("1.0"),
        tax_amount: BigDecimal("0.0"),
        unit_price: BigDecimal("5.0"),
        user_created: false,
        rate_id: flat_rate.id,
        calc_type: 'Flat'
      )
    end

    let(:original_li_tax) do
      create(
        :invoicing_line_item,
        description: "Tax",
        estimated: true,
        net_amount: BigDecimal("0.0"),
        original_quantity: BigDecimal("0.0"),
        original_unit_price: BigDecimal("0.0"),
        quantity: BigDecimal("0.0"),
        tax_amount: BigDecimal("0.0"),
        unit_price: BigDecimal("0.0"),
        user_created: false,
        rate_id: flat_rate.id,
      )
    end

    let(:invoice_vehicle_category) { create(:vehicle_category, name: VehicleCategory::FLATBED) }

    let(:fleet_company) { fleet_managed_company }

    let!(:account) { create(:account, company: api_company, client_company: fleet_company) }
    let!(:flat_rate) { create(:clean_flat_rate, company: api_company, account: account) }
    let(:rate_type) { create(:rate_type, :flat) }

    let(:new_class_type) { super_heavy_class_type.name }

    let!(:new_hours_p2p_super_heavy_rate) { create(:hourly_p2p, company: api_company, account: account, vehicle_category: super_heavy_class_type) }
    let(:new_hours_p2p_rate_type) { create(:rate_type, :hours_port_to_port) }
    let!(:super_heavy_class_type) { create(:vehicle_category, name: "Super Heavy") }

    before do
      api_company.vehicle_categories << super_heavy_class_type
    end

    shared_examples 'build sample invoice as expected' do |expectations|
      it 'builds a sample invoice' do
        invoice = response_data("sampleInvoice", "invoice")

        expect(invoice["id"]).to be_kind_of String
        expect(invoice["id"]).not_to eq original_invoice.id
        expect(invoice["balance"]).to eq expectations[:balance]
        expect(invoice["classType"]).to eq "Super Heavy"
        expect(invoice["notes"]).to eq "Driver notes changed by user"
        expect(invoice["notes"]).to eq "Driver notes changed by user"
        expect(original_invoice.reload.job.driver_notes).to eq "Original Driver Notes"
        expect(invoice.dig("job", "id")).to eq job.to_ssid
        expect(invoice.dig("rateType", "type")).to eq "HoursPortToPort"
        expect(invoice.dig("rateType", "name")).to eq "Hourly (Port to Port)"
        expect(invoice["status"]).to eq original_invoice.state
        expect(invoice["totalAmount"]).to eq expectations[:total_amount]

        line_items = invoice.dig("lineItems", "edges").map(&:values).map(&:first)

        expect(line_items.size).to eq expectations[:line_items_size]

        new_line_item_p2p = line_items.find { |li| li["description"] == 'Port To Port Hours' && li.dig("rate", "id") == new_hours_p2p_super_heavy_rate.to_ssid }

        expect(new_line_item_p2p["id"]).to be_kind_of String
        expect(new_line_item_p2p["deletedAt"]).to be_nil
        expect(new_line_item_p2p["unitPrice"]).to eq "50.00"
        expect(new_line_item_p2p["quantity"]).to eq "0.00"
        expect(new_line_item_p2p["netAmount"]).to eq "0.00"
        expect(new_line_item_p2p.dig("job", "id")).to eq original_invoice.job.to_ssid

        new_line_item_tax = line_items.find { |li| li["description"] == 'Tax' && li.dig("rate", "id") == new_hours_p2p_super_heavy_rate.to_ssid }

        expect(new_line_item_tax["id"]).to be_kind_of String
        expect(new_line_item_tax["deletedAt"]).to be_nil
        expect(new_line_item_tax["unitPrice"]).to eq "0.00"
        expect(new_line_item_tax["quantity"]).to eq "0.00"
        expect(new_line_item_tax["netAmount"]).to eq "0.00"
        expect(new_line_item_tax.dig("job", "id")).to eq original_invoice.job.to_ssid

        line_item_flat = line_items.find { |li| li["description"] == 'Flat Rate' && li.dig("rate", "id") == flat_rate.to_ssid }

        expect(line_item_flat["id"]).to eq original_invoice.line_items.find_by_description('Flat Rate').to_ssid
        expect(line_item_flat["deletedAt"]).to be_present
        expect(line_item_flat["unitPrice"]).to eq "2000.00"
        expect(line_item_flat["quantity"]).to eq "2.00"
        expect(line_item_flat["netAmount"]).to eq "4000.00"
        expect(line_item_flat.dig("job", "id")).to eq original_invoice.job.to_ssid

        line_item_dolly = line_items.find { |li| li["description"] == 'Dolly' && li.dig("rate", "id") == flat_rate.to_ssid }
        expect(line_item_dolly["id"]).to eq original_invoice.line_items.find_by_description('Dolly').to_ssid
        expect(line_item_flat["deletedAt"]).to be_present
        expect(line_item_dolly["unitPrice"]).to eq "5.00"
        expect(line_item_dolly["quantity"]).to eq "1.00"
        expect(line_item_dolly["netAmount"]).to eq "5.00"
        expect(line_item_dolly.dig("job", "id")).to eq original_invoice.job.to_ssid

        line_item_tax = line_items.find { |li| li["description"] == 'Tax' && li.dig("rate", "id") == flat_rate.to_ssid }
        expect(line_item_tax["id"]).to eq original_invoice.line_items.find_by_description('Tax').to_ssid
        expect(line_item_tax["unitPrice"]).to eq "0.00"
        expect(line_item_tax["quantity"]).to eq "0.00"
        expect(line_item_tax["netAmount"]).to eq "0.00"
        expect(line_item_tax.dig("job", "id")).to eq original_invoice.job.to_ssid

        payments_or_credits = invoice.dig("payments", "edges")
        payment = payments_or_credits.first.dig("node")
        expect(payment["id"]).to eq original_invoice.payments_or_credits.find_by_type('Payment').to_ssid
        expect(payment["amount"]).to eq "-10.00"
        expect(payment["memo"]).to be_nil
        expect(payment["paymentMethod"]).to eq "Credit Card"
        expect(payment["type"]).to eq "Payment"

        credit = payments_or_credits.second.dig("node")
        expect(credit["id"]).to eq original_invoice.payments_or_credits.find_by_type('Refund').to_ssid
        expect(credit["amount"]).to eq "10.00"
        expect(credit["memo"]).to be_nil
        expect(credit["paymentMethod"]).to eq "Credit Card"
        expect(credit["type"]).to eq "Refund"
      end
    end

    context 'when it is Partner user' do
      let(:api_company) { rescue_company }
      let(:api_user) { create(:user, company: api_company, roles: [:rescue, :dispatcher]) }

      it_behaves_like 'build sample invoice as expected', line_items_size: 5, balance: "0.00", total_amount: "0.00"

      context 'when new_class_type is nil' do
        let(:new_class_type) { nil }

        it 'returns classType nil' do
          expect(response_data("invoiceSample", "invoice", "classType")).to be_nil
        end

        it 'does not change it for persisted job' do
          response

          expect(job.reload.invoice_vehicle_category).to eq invoice_vehicle_category
        end
      end

      context 'when user adds an additional item' do
        let(:user_created_item) do
          {
            id: 'fake_by_a_previous_sample',
            autoCalculatedQuantity: false,
            calcType: "Flat",
            deletedAt: nil,
            description: 'Fuel',
            estimated: false,
            job: {
              id: job.to_ssid,
            },
            originalQuantity: '1.00',
            originalUnitPrice: '0.00',
            quantity: "10.00",
            rate: nil,
            taxAmount: "0.00",
            unitPrice: "5.00",
            netAmount: "50.00",
            userCreated: true,
          }
        end

        let(:line_items_input) { super() << user_created_item }

        it_behaves_like 'build sample invoice as expected', line_items_size: 6, balance: "50.00", total_amount: "50.00"

        it 'keeps the Fuel additional item without a rate_id' do
          line_items = response_data("sampleInvoice", "invoice").dig("lineItems", "edges").map(&:values).map(&:first)
          fuel_item = line_items.find { |li| li["description"] == 'Fuel' && li.dig("rate", "id").nil? }

          expect(fuel_item).to be_present
        end
      end
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    it_behaves_like "sampleInvoice"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    it_behaves_like "sampleInvoice"
  end
end
