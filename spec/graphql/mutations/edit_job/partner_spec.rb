# frozen_string_literal: true

require_relative "./edit_job_shared"

describe Mutations::EditJob do
  include_context "edit job shared"

  shared_examples "partner" do
    let!(:job) do
      create :fleet_managed_job, status: :pending, fleet_company: fleet_managed_company, rescue_company: api_company
    end

    # lolwut? assiging a driver / vehicle to an accepted job will also change the status
    context "dispatching" do
      let!(:job) do
        create :fleet_managed_job, status: :accepted, fleet_company: fleet_managed_company, rescue_company: api_company
      end

      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: EditJobInput!) {
          editJob(input: $input) {
            job {
              id
              status
            }
          }
        }
        GRAPHQL
      end
      let(:driver) { create(:user, roles: [:rescue, :driver, :dispatcher], company: rescue_company) }
      let(:vehicle) { create(:rescue_vehicle, company: rescue_company) }
      let(:variables) { { input: { job: { id: encoded_job_id, partner: { driver: { name: driver.name }, vehicle: { name: vehicle.name } } } } } }
      let(:referer) { 'http://example.com/' }

      it "works" do
        expect(AnalyticsData::JobStatus).to receive(:new).with(
          # can't match against job because it is changed in our mutation
          job: anything,
          user_agent_hash: {
            platform: 'React iOS',
            version: 'SwoopMobile/9',
          },
          referer: referer,
          api_user: api_user,
          api_application: api_application,
        ).once.and_call_original

        expect(response).not_to include('errors')

        expect { job.reload }
          .to change(job, :rescue_driver).to(driver)
          .and change(job, :rescue_vehicle).to(vehicle)
          .and change(job, :status).to('dispatched')

        # make sure our mutation resulted in a AssignRescueDriverVehicleToJobWorker being enqueued
        expect(AssignRescueDriverVehicleToJobWorker).to have_enqueued_sidekiq_job(job.id)
      end
    end

    context "partner.department.name" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: EditJobInput!) {
          editJob(input: $input) {
            job {
              id
              partner {
                department {
                  name
                }
              }
            }
          }
        }
        GRAPHQL
      end

      let(:department) { create(:department, company: api_company) }
      let(:variables) { { input: { job: { partner: { department: { name: department.name } }, id: encoded_job_id } } } }

      it "works" do
        expect(response).not_to include('errors')
        job_response = response.dig('data', 'editJob', 'job')
        expect { job.reload }.to change(job, :partner_department_id).from(nil).to(department.id)
        expect(job_response).to include({
          id: encoded_job_id,
          partner: {
            department: {
              name: department.name,
            },
          },
        }.deep_stringify_keys)

        # since we're not updating job.rescue_driver make sure we didn't enqueue AssignRescueDriverVehicleToJobWorker
        # (we only need to test this once)
        expect(AssignRescueDriverVehicleToJobWorker).not_to have_enqueued_sidekiq_job(job.id)
      end

      # test the editable response - only need to do this once
      context "editable" do
        let(:job) do
          j = super()
          j.assigned!
          j
        end

        let(:errors_array) do
          [
            {
              message: Analyzers::Validations::JobEditable::ERROR_MESSAGE,
              extensions: {
                problems: [{
                  explanation: Analyzers::Validations::JobEditable::ERROR_MESSAGE,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "editJob", "input", "job"],
            }.deep_stringify_keys,
          ]
        end

        it_behaves_like 'responds with error'
      end

      context "with a bad name" do
        let(:variables) { { input: { job: { partner: { department: { name: Faker::Name.name } }, id: encoded_job_id } } } }
        let(:errors_array) do
          [
            {
              message: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['department'],
              extensions: {
                problems: [{
                  explanation: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['department'],
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "editJob", "input", "job", "partner", "department"],
            }.deep_stringify_keys,
          ]
        end

        it_behaves_like 'responds with error'
      end
    end

    context "partner.vehicle.name" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: EditJobInput!) {
          editJob(input: $input) {
            job {
              id
              partner {
                vehicle {
                  name
                }
              }
            }
          }
        }
        GRAPHQL
      end

      let(:vehicle) { create(:rescue_vehicle, company: api_company) }
      let(:variables) { { input: { job: { partner: { vehicle: { name: vehicle.name } }, id: encoded_job_id } } } }

      it "works" do
        expect(response).not_to include('errors')
        job_response = response.dig('data', 'editJob', 'job')
        expect { job.reload }.to change(job, :rescue_vehicle_id).from(nil).to(vehicle.id)
        expect(job_response).to include({
          id: encoded_job_id,
          partner: {
            vehicle: {
              name: vehicle.name,
            },
          },
        }.deep_stringify_keys)
      end
      context "with a bad name" do
        let(:variables) { { input: { job: { partner: { vehicle: { name: Faker::Name.name } }, id: encoded_job_id } } } }
        let(:errors_array) do
          [
            {
              message: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['vehicle'],
              extensions: { problems: [{
                explanation: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['vehicle'],
              }] },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "editJob", "input", "job", "partner", "vehicle"],
            }.deep_stringify_keys,
          ]
        end

        it_behaves_like 'responds with error'
      end
    end

    context "partner.trailer.name" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: EditJobInput!) {
          editJob(input: $input) {
            job {
              id
              partner {
                trailer {
                  name
                }
              }
            }
          }
        }
        GRAPHQL
      end

      let(:trailer) { create(:trailer_vehicle, company: api_company) }
      let(:variables) { { input: { job: { partner: { trailer: { name: trailer.name } }, id: encoded_job_id } } } }

      it "works" do
        expect(response).not_to include('errors')
        job_response = response.dig('data', 'editJob', 'job')
        expect { job.reload }.to change(job, :trailer_vehicle_id).from(nil).to(trailer.id)
        expect(job_response).to include({
          id: encoded_job_id,
          partner: {
            trailer: {
              name: trailer.name,
            },
          },
        }.deep_stringify_keys)
      end

      context "with a bad name" do
        let(:variables) { { input: { job: { partner: { trailer: { name: Faker::Name.name } }, id: encoded_job_id } } } }
        let(:errors_array) do
          [
            {
              message: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['trailer'],
              extensions: {
                problems: [{
                  explanation: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['trailer'],
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "editJob", "input", "job", "partner", "trailer"],
            }.deep_stringify_keys,
          ]
        end

        it_behaves_like 'responds with error'
      end
    end

    context "partner.driver.name" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: EditJobInput!) {
          editJob(input: $input) {
            job {
              id
              partner {
                driver {
                  name
                }
              }
            }
          }
        }
        GRAPHQL
      end

      let!(:driver) { create(:user, :rescue, :driver, company: api_company) }
      let(:variables) { { input: { job: { partner: { driver: { name: driver.name } }, id: encoded_job_id } } } }

      it "works" do
        expect(response).not_to include('error')
        job_response = response.dig('data', 'editJob', 'job')
        expect { job.reload }.to change(job, :rescue_driver_id).from(nil).to(driver.id)
        expect(job_response).to include({
          id: encoded_job_id,
          partner: {
            driver: {
              name: driver.name,
            },
          },
        }.deep_stringify_keys)
      end

      context "with a bad name" do
        let(:variables) { { input: { job: { partner: { driver: { name: Faker::Name.name } }, id: encoded_job_id } } } }
        let(:errors_array) do
          [
            {
              message: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['driver'],
              extensions: {
                problems: [{
                  explanation: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['driver'],
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "editJob", "input", "job", "partner", "driver"],
            }.deep_stringify_keys,
          ]
        end

        it_behaves_like 'responds with error'
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    let(:api_user) { rescue_dispatcher_driver }

    it_behaves_like "partner"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_dispatcher_driver }

    it_behaves_like "partner"
  end
end
