# frozen_string_literal: true

require_relative "./edit_job_shared"

describe Mutations::EditJob do
  include_context "edit job shared"

  shared_examples "account.name" do
    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: EditJobInput!) {
        editJob(input: $input) {
          job {
            id
            account {
              name
            }
          }
        }
      }
      GRAPHQL
    end

    let!(:job) do
      create :fleet_managed_job, status: :pending, fleet_company: fleet_managed_company, rescue_company: rescue_company
    end

    let(:account) { create(:account, company: rescue_company) }
    let(:variables) { { input: { job: { account: { name: account.name }, id: encoded_job_id } } } }

    it_behaves_like 'does not raise error'

    it "works" do
      job_response = response.dig('data', 'editJob', 'job')
      expect { job.reload }.to change(job, :account_id).from(nil).to(account.id)
      expect(job_response).to include({
        id: encoded_job_id,
        account: { name: account.name },
      }.deep_stringify_keys)
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "account.name"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "account.name"
  end
end
