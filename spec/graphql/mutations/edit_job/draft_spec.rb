# frozen_string_literal: true

require_relative "./edit_job_shared"

describe Mutations::EditJob do
  include_context "edit job shared"

  describe 'draft job' do
    let(:status) { 'Draft' }

    context "as a client api_application" do
      include_context "client api_application"
      it_behaves_like 'with a FleetManagedJob', 'updates a job'
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like 'with a FleetManagedJob', 'updates a job'
      it_behaves_like 'with a RescueJob', 'updates a job'
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like 'with a FleetManagedJob', 'updates a job'
      it_behaves_like 'with a RescueJob', 'updates a job'
    end
  end
end
