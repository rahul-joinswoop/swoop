# frozen_string_literal: true

require_relative "./edit_job_shared"

describe Mutations::EditJob do
  include_context "edit job shared"

  shared_examples 'location' do
    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: EditJobInput!) {
        editJob(input: $input) {
          job {
            id
            location {
              serviceLocation{
                address
                lat
                lng
                locationType
                exact
              }
              dropoffLocation{
                address
                lat
                lng
                locationType
                exact
              }
            }
          }
        }
      }
      GRAPHQL
    end

    let(:status) { 'dispatched' }
    let(:location) do
      create(
        :location,
        lat: 37.4567,
        lng: 38.9876,
        address: 'San Francisco',
        exact: true,
        place_id: 'place_id'
      )
    end

    let(:location_type) { create(:location_type, name: "Auto Body Shop") }

    before do
      api_company.location_types << location_type
    end

    shared_examples 'changes only location_type' do |service_or_drop_location|
      it 'changes only location_type' do
        location_response = if service_or_drop_location == :service_location
                              response_data('editJob', 'job', 'location', 'serviceLocation')
                            else
                              response_data('editJob', 'job', 'location', 'dropoffLocation')
                            end

        job_location = job.reload.send(service_or_drop_location)

        if location_type_name
          expect(job_location.location_type_id).to eq location_type.id
          expect(location_response["locationType"]).to eq location_type.name
        else
          expect(job_location.location_type_id).to be_nil
          expect(location_response["locationType"]).to be_nil
        end

        expect(location_response["lat"]).to eq location.lat
        expect(location_response["lng"]).to eq location.lng
        expect(location_response["address"]).to eq location.address
        expect(location_response["exact"]).to eq location.exact

        expect(job_location.lat).to eq location.lat
        expect(job_location.lng).to eq location.lng
        expect(job_location.address).to eq location.address
        expect(job_location.exact).to eq location.exact
      end
    end

    shared_examples 'changes only address data' do |service_or_drop_location|
      it 'changes only address data' do
        location_response = if service_or_drop_location == :service_location
                              response_data('editJob', 'job', 'location', 'serviceLocation')
                            else
                              response_data('editJob', 'job', 'location', 'dropoffLocation')
                            end

        job_location = job.reload.send(service_or_drop_location)

        expect(job_location.location_type_id).to eq job_location_type.id
        expect(location_response["locationType"]).to eq job_location_type.name

        expect(location_response["lat"]).to eq 37.998877
        expect(location_response["lng"]).to eq 38.334455
        expect(location_response["address"]).to eq '865, New Address st - SF'
        expect(location_response["exact"]).to eq false

        expect(job_location.lat).to eq 37.998877
        expect(job_location.lng).to eq 38.334455
        expect(job_location.address).to eq '865, New Address st - SF'
        expect(job_location.exact).to eq false
      end
    end

    context 'with job.service_location is already set' do
      before do
        job.update! service_location_id: location.id
      end

      context 'when only service_location.location_type is changed' do
        let(:variables) do
          {
            input: {
              job: {
                id: encoded_job_id,
                location: {
                  serviceLocation: {
                    locationType: location_type_name,
                  },
                },
              },
            },
          }
        end

        shared_examples 'changes only service_location.location_type and keep original address' do
          it_behaves_like 'changes only location_type', :service_location
        end

        context 'when given locationType is valid' do
          let(:location_type_name) { location_type.name }

          it_behaves_like "changes only service_location.location_type and keep original address"
        end

        context 'when given locationType is null' do
          let(:location_type_name) { nil }

          context 'and job has a service_location.location_type set' do
            let(:job_service_location_type) { create(:location_type, name: 'Any Service Location') }

            before do
              api_company.location_types << job_service_location_type
              job.service_location.update! location_type_id: job_service_location_type.id
            end

            it_behaves_like "changes only service_location.location_type and keep original address"
          end
        end
      end

      context 'when only service_location address data is changed' do
        let(:variables) do
          {
            input: {
              job: {
                id: encoded_job_id,
                location: {
                  serviceLocation: {
                    address: '865, New Address st - SF',
                    lat: 37.998877,
                    lng: 38.334455,
                    exact: false,
                  },
                },
              },
            },
          }
        end

        context 'and job.service_location.location_type is present' do
          let(:job_location_type) { create(:location_type, name: 'Any Service Location') }

          before do
            api_company.location_types << job_location_type
            job.service_location.update! location_type_id: job_location_type.id
          end

          shared_examples 'changes only service_location address data and keep original location_type' do
            it_behaves_like 'changes only address data', :service_location
          end

          it_behaves_like 'changes only service_location address data and keep original location_type'
        end
      end
    end

    context 'with job.drop_location is already set' do
      before do
        job.update! drop_location_id: location.id
      end

      context 'when only drop_location.location_type is changed' do
        let(:variables) do
          {
            input: {
              job: {
                id: encoded_job_id,
                location: {
                  dropoffLocation: {
                    locationType: location_type_name,
                  },
                },
              },
            },
          }
        end

        shared_examples 'changes only drop_location.location_type and keep original address' do
          it_behaves_like 'changes only location_type', :drop_location
        end

        context 'when given locationType is valid' do
          let(:location_type_name) { location_type.name }

          it_behaves_like "changes only drop_location.location_type and keep original address"
        end

        context 'when given locationType is null' do
          let(:location_type_name) { nil }

          context 'and job has a drop_location.location_type set' do
            let(:job_drop_location_type) { create(:location_type, name: 'Any Drop Location') }

            before do
              api_company.location_types << job_drop_location_type
              job.drop_location.update! location_type_id: job_drop_location_type.id
            end

            it_behaves_like "changes only drop_location.location_type and keep original address"
          end
        end
      end

      context 'when only drop_location address data is changed' do
        let(:variables) do
          {
            input: {
              job: {
                id: encoded_job_id,
                location: {
                  dropoffLocation: {
                    address: '865, New Address st - SF',
                    lat: 37.998877,
                    lng: 38.334455,
                    exact: false,
                  },
                },
              },
            },
          }
        end

        context 'and job.drop_location.location_type is present' do
          let(:job_location_type) { create(:location_type, name: 'Any Drop Location') }

          before do
            api_company.location_types << job_location_type
            job.drop_location.update! location_type_id: job_location_type.id
          end

          shared_examples 'changes only drop_location address data and keep original location_type' do
            it_behaves_like 'changes only address data', :drop_location
          end

          it_behaves_like 'changes only drop_location address data and keep original location_type'
        end
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like 'with a FleetManagedJob', 'location'
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like 'with a FleetManagedJob', 'location'
    it_behaves_like 'with a RescueJob', 'location'
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like 'with a FleetManagedJob', 'location'
    it_behaves_like 'with a RescueJob', 'location'
  end
end
