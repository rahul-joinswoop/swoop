# frozen_string_literal: true

require_relative "./edit_job_shared"

describe Mutations::EditJob do
  include_context "edit job shared"

  shared_examples "eta", freeze_time: true do
    let!(:job) do
      create :fleet_managed_job, status: :pending, fleet_company: fleet_managed_company, rescue_company: api_company
    end
    let(:current) { Time.now + 45.minutes }

    context "eta.current" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: EditJobInput!) {
          editJob(input: $input) {
            job {
              id
              eta {
                bidded
                current
              }
            }
          }
        }
        GRAPHQL
      end

      let(:variables) do
        {
          input: {
            job: {
              id: encoded_job_id,
              eta: {
                current: current.utc.to_formatted_s(:iso8601),
              },
            },
          },
        }
      end

      it_behaves_like 'does not raise error'

      it "works" do
        expect { response && job.reload }.to change { job.bta&.time }.to(current)
        job_response = response.dig('data', 'editJob', 'job')
        expect(job_response).to include({
          id: encoded_job_id,
          eta: {
            bidded: current.utc.to_formatted_s(:iso8601),
            current: current.utc.to_formatted_s(:iso8601),
          },
        }.deep_stringify_keys)
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "eta"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "eta"
  end
end
