# frozen_string_literal: true

require_relative "./edit_job_shared"

describe Mutations::EditJob do
  include_context "edit job shared"

  shared_examples "caller" do
    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: EditJobInput!) {
        editJob(input: $input) {
          job {
            id
            caller {
              name
              phone
            }
          }
        }
      }
      GRAPHQL
    end
    let(:user) { create(:user, :phone) }
    let!(:job) do
      create :rescue_job, status: :pending, fleet_company: rescue_company, rescue_company: rescue_company, caller: user
    end

    let(:caller) { create :graphql_caller_input_type }
    let(:variables) { { input: { job: { caller: caller, id: encoded_job_id } } } }
    let(:job_response) { response.dig('data', 'editJob', 'job') }

    context "with a name" do
      let(:query) do
        <<~GRAPHQL
          mutation #{operation_name}($input: EditJobInput!) {
            editJob(input: $input) {
              job {
                id
                caller {
                  name
                }
              }
            }
          }
          GRAPHQL
      end

      let(:caller) { super().deep_dup.tap { |c| c.delete :phone } }

      it_behaves_like 'does not raise error'

      it "works" do
        expect { response && job.reload }
          .to change { job.caller.full_name.strip }
          .to(caller[:name].strip)
          .and not_change { job.caller.phone }

        expect(job_response).to include({
          id: encoded_job_id,
          caller: caller,
        }.deep_stringify_keys)
      end
    end

    context "with a phone" do
      let(:query) do
        <<~GRAPHQL
          mutation #{operation_name}($input: EditJobInput!) {
            editJob(input: $input) {
              job {
                id
                caller {
                  phone
                }
              }
            }
          }
          GRAPHQL
      end

      let(:caller) { super().deep_dup.tap { |c| c.delete :name } }

      it_behaves_like 'does not raise error'

      it "works" do
        expect { response && job.reload }
          .to not_change { job.caller.full_name }
          .and change { job.caller.phone }
          .to(caller[:phone])

        expect(job_response).to include({
          id: encoded_job_id,
          caller: caller,
        }.deep_stringify_keys)
      end
    end

    context "with a phone and name" do
      it_behaves_like 'does not raise error'

      it "works" do
        expect { response && job.reload }
          .to change { job.caller.full_name.strip }
          .to(caller[:name].strip)
          .and change { job.caller.phone }
          .to(caller[:phone])

        expect(job_response).to include({
          id: encoded_job_id,
          caller: caller,
        }.deep_stringify_keys)
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "caller"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "caller"
  end
end
