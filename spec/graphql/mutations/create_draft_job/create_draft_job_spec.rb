# frozen_string_literal: true

require 'rails_helper'

describe Mutations::CreateDraftJob do
  let(:variables) { { input: { job: job_input } } }
  let(:job_input) { {} }
  let(:query) do
    <<~GRAPHQL
    mutation #{operation_name}($input: CreateDraftJobInput!) {
      createDraftJob(input: $input) {
        job {
          id
          status
        }
      }
    }
    GRAPHQL
  end
  let(:job_output) { { 'status' => status } }
  let(:status) { 'Draft' }
  let(:job_response) { response.dig('data', 'createDraftJob', 'job') }

  before(:each) do
    # mock this, otherwise we end up doing lookups for the timezone of our
    # randomly generated lat/lng coordinates
    allow_any_instance_of(External::GoogleTimezoneService).to receive(:call).and_return("UTC")
  end

  shared_examples "creates a draft job" do
    it "works" do
      create_analytics_sent = false

      expect(Analytics).to receive(:track) do |args|
        # yuck, so hard to filter out unwanted events. in this case we only want
        # to match 'Create Job'
        if args[:event] == 'Create Job'
          create_analytics_sent = true
          expect(args).to deep_include({
            # we can't test for job_id here because we don't have it yet
            user_id: api_company.to_ssid,
            event: 'Create Job',
            properties: {
              category: 'Jobs',
              platform: 'React iOS',
              company: api_company.name,
            },
          })
        end
      end.at_least(:once)
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
      expect(create_analytics_sent).to be_truthy

      id = job_response.dig('id')

      j = SomewhatSecureID.load!(id)
      expect(j).to be_present
      # initial state should be pending
      expect(j.status).to eq(status.downcase)

      # sometimes our addresses get the 'USA' portion dropped. i don't know why
      # this is happening and i haven't been able to track it down so for now
      # just strip that piece off so the comparisons will always work
      ['serviceLocation', 'dropoffLocation'].each do |k|
        if job_response.dig('location', k, 'address').present?
          job_response['location'][k]['address'] = job_response['location'][k]['address'].to_s.clone.gsub(/\, USA/, '')
        end
      end

      expect(job_response.without(['id'])).to deep_include(job_output)
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "creates a draft job"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "creates a draft job"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "creates a draft job"
  end
end
