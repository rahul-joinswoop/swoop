# frozen_string_literal: true

require_relative './create_draft_job_shared'

describe Mutations::CreateDraftJob do
  include_context "create draft job shared"

  shared_examples 'Tire Change' do
    let(:service_code) { create(:service_code, name: ServiceCode::TIRE_CHANGE) }
    let(:api_company) do
      c = super()
      c.features << create(:feature, name: Feature::QUESTIONS)
      c.companies_services << create(:companies_service, company: c, service_code: service_code)
      c
    end
    let(:job_input) { { service: service_input } }
    let(:variables) { { input: { job: job_input } } }

    let(:spare_question) { create :question, question: ::Question::HAVE_A_SPARE }
    let!(:answer_no) { create :answer, answer: ::Answer::NO, question: spare_question }
    let!(:answer_yes) { create :answer, answer: ::Answer::YES, question: spare_question }

    let(:tire_question) { create :question, question: ::Question::WHICH_TIRE }
    let!(:answer_multiple) { create :answer, answer: ::Answer::DAMAGED_TIRE_MULTIPLE, question: tire_question }
    let!(:answer_front_driver) { create :answer, answer: ::Answer::DAMAGED_TIRE_FRONT_DRIVER, question: tire_question }

    context "spare tire" do
      describe "no" do
        let(:service_input) do
          create(:graphql_service_input_type, company: api_company, service_code: service_code)
            .merge(answers: [{
              question: spare_question.question,
              answer: answer_no.answer,
            }])
        end

        let(:errors_array) do
          [{
            message: Analyzers::Validations::TireChangeService::ERROR_MESSAGE,
            locations: [{ line: 2, column: 3 }],
            path: ["mutation #{operation_name}", "createDraftJob", "input", "job", "service", "name"],
            extensions: {
              problems: [{
                explanation: Analyzers::Validations::TireChangeService::NO_SPARE_EXPLANATION,
              }],
            },
          }]
        end

        it_behaves_like "responds with error"
      end

      describe "yes" do
        let(:service_input) do
          create(:graphql_service_input_type, company: api_company, service_code: service_code)
            .merge(answers: [{
              question: spare_question.question,
              answer: answer_yes.answer,
            }])
        end

        it_behaves_like "does not raise error"
      end
    end

    context "multiple flat tires" do
      describe "yes" do
        let(:service_input) do
          create(:graphql_service_input_type, company: api_company, service_code: service_code)
            .merge(answers: [{
              question: tire_question.question,
              answer: answer_multiple.answer,
            }])
        end

        let(:errors_array) do
          [{
            message: Analyzers::Validations::TireChangeService::ERROR_MESSAGE,
            locations: [{ line: 2, column: 3 }],
            path: ["mutation #{operation_name}", "createDraftJob", "input", "job", "service", "name"],
            extensions: {
              problems: [{
                explanation: Analyzers::Validations::TireChangeService::MULTI_FLAT_EXPLANATION,
              }],
            },
          }]
        end

        it_behaves_like "responds with error"
      end

      describe "no" do
        let(:service_input) do
          create(:graphql_service_input_type, company: api_company, service_code: service_code)
            .merge(answers: [{
              question: tire_question.question,
              answer: answer_front_driver.answer,
            }])
        end

        it_behaves_like "does not raise error"
      end
    end

    context "no spare tire and multiple flat tires" do
      let(:service_input) do
        create(:graphql_service_input_type, company: api_company, service_code: service_code)
          .merge(answers: [
            {
              question: spare_question.question,
              answer: answer_no.answer,
            },
            {
              question: tire_question.question,
              answer: answer_multiple.answer,
            },
          ])
      end

      let(:errors_array) do
        [{
          message: Analyzers::Validations::TireChangeService::ERROR_MESSAGE,
          locations: [{ line: 2, column: 3 }],
          path: ["mutation #{operation_name}", "createDraftJob", "input", "job", "service", "name"],
          extensions: {
            problems: [
              {
                explanation: Analyzers::Validations::TireChangeService::NO_SPARE_EXPLANATION,
              },
              {
                explanation: Analyzers::Validations::TireChangeService::MULTI_FLAT_EXPLANATION,
              },
            ],
          },
        }]
      end

      it_behaves_like "responds with error"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "Tire Change"
  end
end
