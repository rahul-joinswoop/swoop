# frozen_string_literal: true

require_relative './create_draft_job_shared'

describe Mutations::CreateDraftJob do
  include_context "create draft job shared"

  shared_examples "service" do
    let(:query) do
      qs = super().dup
      # insert service.scheduledFor into our query
      qs.insert(qs.index(/^\s+?id/), "service { scheduledFor }\n")
      qs
    end

    describe "scheduledFor" do
      let(:job_input) { { service: { scheduledFor: scheduled_for.iso8601 } } }

      context "with an invalid scheduled_for" do
        let(:scheduled_for) { Time.current }
        let(:errors_array) do
          {
            message: Analyzers::Validations::ServiceScheduledFor::ERROR_MESSAGE,
            locations: [{ line: 2, column: 3 }],
            path: ["mutation #{operation_name}", "createDraftJob", "input", "job", "service", "scheduledFor"],
            extensions: {
              problems: [{
                explanation: Analyzers::Validations::ServiceScheduledFor::ERROR_MESSAGE,
              }],
            },
          }
        end

        it_behaves_like "responds with error"
      end

      context "with a valid scheduled_for" do
        let(:scheduled_for) { Time.current + 3.hours }

        it_behaves_like "creates a draft job"
      end
    end

    # give a service that's not in the db
    describe "service" do
      let(:service) { Faker::Hipster.sentence }
      let(:job_input) { { service: { name: service } } }
      let(:errors_array) do
        {
          message: "#{service.inspect} is not a valid Service",
          locations: [{ line: nil, column: nil }],
          path: ["mutation #{operation_name}", "createDraftJob", "input", "job", "service", "name"],
          extensions: { code: "argumentLiteralsIncompatible", typeName: "CoercionError" },
        }
      end

      it_behaves_like "responds with error"
    end

    describe "when storageType does not exist in the DB" do
      let(:storage_type) { Faker::Hipster.sentence }
      let(:job_input) { { service: { storageType: storage_type } } }
      let(:errors_array) do
        {
          message: "#{storage_type.inspect} is not a valid StorageType",
          locations: [{ line: nil, column: nil }],
          path: ["mutation #{operation_name}", "createDraftJob", "input", "job", "service", "storageType"],
          extensions: { code: "argumentLiteralsIncompatible", typeName: "CoercionError" },
        }
      end

      it_behaves_like "responds with error"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "service"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "service"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "service"
  end
end
