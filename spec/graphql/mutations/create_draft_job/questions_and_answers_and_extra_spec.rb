# frozen_string_literal: true

require_relative './create_draft_job_shared'

describe Mutations::CreateDraftJob do
  include_context "create draft job shared"

  shared_examples "questions and answers and extra" do
    before do
      api_company.features << create(:feature, name: Feature::QUESTIONS)
    end

    let(:job_input) do
      ji = { service: create(:graphql_service_input_type, :with_answers, company: api_company) }
      ji[:service].delete(:service_code)
      ji[:service].delete(:name)
      ji
    end

    let!(:company_service_question) do
      question = create :question, question: Question::LOW_CLEARANCE, with_answers: Answer::DEFAULT_ANSWERS_PER_QUESTION[Question::LOW_CLEARANCE]
      csq = api_company.company_service_questions.order(:id).first
      csq.update! question: question
      api_company.company_service_questions.reload
      csq
    end

    let!(:question_answer) do
      job_input[:service][:answers]
        .find { |qa| qa[:question] == Question::LOW_CLEARANCE }
        .tap do |qa|
          qa.delete(:extra)
          qa[:answer] = answer
        end
    end

    context 'when extra is required' do
      let(:answer) { Answer::YES }
      let(:errors_array) do
        {
          locations: [{ column: 3, line: 2 }],
          message: Analyzers::Validations::Extra::ERROR_MESSAGE % [company_service_question.question.question.inspect],
          path: [
            "mutation #{operation_name}",
            "createDraftJob",
            "input",
            "job",
            "service",
            "answers",
          ],
        }
      end

      it_behaves_like "responds with error"
    end

    context 'when extra is not required' do
      let(:answer) { Answer::NO }

      it "works" do
        allow_any_instance_of(External::GoogleTimezoneService).to receive(:call).and_return("UTC")
        expect { response }.not_to raise_error
        expect(response['errors']).to be_blank
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "questions and answers and extra"
  end
end
