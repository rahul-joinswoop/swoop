# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "create draft job shared" do
  let(:job_input) { {} }
  let(:variables) { { input: { job: job_input } } }
  let(:status) { 'Draft' }
  let(:query) do
    <<~GRAPHQL
    mutation #{operation_name}($input: CreateDraftJobInput!) {
      createDraftJob(input: $input) {
        job {
          id
          status
        }
      }
    }
    GRAPHQL
  end
  let(:job_output) do
    job_input.deep_dup.tap do |j|
      # j[:service][:classType] = { name: j[:service][:classType]  }
      # see comment below
      [:serviceLocation, :dropoffLocation].each do |k|
        if j.dig(:location, k, :address).present?
          j[:location][k][:address] = j[:location][k][:address].clone.gsub(/\, USA/, '')
        end
      end

      [:pickupContact, :dropoffContact].each do |k|
        if j.dig(:location, k, :name).present?
          j[:location][k][:fullName] = j[:location][k][:name].clone
        end
      end

      if j.dig(:vehicle, :odometer).present?
        j[:vehicle][:odometer] = j[:vehicle][:odometer].to_i.to_s
      end
      # customer can be nil (see JobType)
      if j.dig(:customer, :name).present?
        j[:customer][:fullName] = j[:customer][:name]
      end
    end.deep_stringify_keys
  end

  # check job output
  let(:job_response) { response.dig('data', 'createDraftJob', 'job') }

  before(:each) do
    # mock this, otherwise we end up doing lookups for the timezone of our
    # randomly generated lat/lng coordinates
    allow_any_instance_of(External::GoogleTimezoneService).to receive(:call).and_return("UTC")
  end

  shared_examples "creates a draft job" do
    it "works" do
      create_analytics_sent = false

      expect(Analytics).to receive(:track) do |args|
        # yuck, so hard to filter out unwanted events. in this case we only want
        # to match 'Create Job'
        if args[:event] == 'Create Job'
          create_analytics_sent = true
          expect(args).to deep_include({
            # we can't test for job_id here because we don't have it yet
            user_id: api_company.to_ssid,
            event: 'Create Job',
            properties: {
              category: 'Jobs',
              platform: 'React iOS',
              company: api_company.name,
            },
          })
        end
      end.at_least(:once)
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
      expect(create_analytics_sent).to be_truthy

      id = job_response.dig('id')

      j = SomewhatSecureID.load!(id)
      expect(j).to be_present
      # initial state should be pending
      expect(j.status).to eq(status.downcase)

      # sometimes our addresses get the 'USA' portion dropped. i don't know why
      # this is happening and i haven't been able to track it down so for now
      # just strip that piece off so the comparisons will always work

      ['serviceLocation', 'dropoffLocation'].each do |k|
        if job_response.dig('location', k, 'address').present?
          job_response['location'][k]['address'] = job_response['location'][k]['address'].to_s.clone.gsub(/\, USA/, '')
        end
      end

      expect(job_response.without(['id'])).to deep_include(job_output)
    end
  end
end
