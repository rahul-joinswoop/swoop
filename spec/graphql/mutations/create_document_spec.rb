# frozen_string_literal: true

require 'rails_helper'

describe Mutations::CreateDocument, freeze_time: true do
  shared_examples 'createDocument' do
    let(:job) { create :rescue_job, rescue_company: api_company }
    let(:location) { create :graphql_location_input_type }
    let(:type) { 'SignatureImage' }
    let(:document_document) do
      {
        "id" => "de150e1fe4745058b05a94322367baf7",
        "storage" => "cache",
        "metadata" => {
          "size" => 799,
          "filename" => "Query Results.csv",
          "mime_type" => "text/csv",
        },
      }
    end

    let(:created_at) { (Time.current - 5.minutes).iso8601 }
    let(:variables) do
      {
        input: {
          document: {
            createdAt: created_at,
            type: type,
            location: location,
            document: document_document,
            job: {
              id: job.to_ssid,
            },
          },
        },
      }
    end

    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: CreateDocumentInput!) {
        createDocument(input: $input) {
          document {
            id
            createdAt
            company {
              id
            }
            container
            invoiceAttachment
            job {
              id
            }
            location {
              lat
              lng
            }
            originalFilename
            type
            url
            user {
              id
            }
          }
        }
      }
      GRAPHQL
    end

    before do
      allow_any_instance_of(AttachedDocument).to receive(:finalize_document)
    end

    it "does not throw error" do
      expect { response }.not_to raise_error
    end

    it "does not include errors on response", vcr: true do
      expect(response).not_to include('errors')
    end

    it "creates the Document accordingly", vcr: true do
      expect_any_instance_of(AnalyticsData::Document).to receive(:push_message).once.and_call_original
      expect(Analytics).to receive(:track).once
      expect { response }.to change(AttachedDocument, :count).by(1)
      attachment = job.attached_documents.first
      expect(attachment).not_to be nil
      expect(attachment.location.place_id).to eq location[:placeId]
      expect(attachment.document.data).to eq(document_document)
      expect(attachment.created_at.iso8601).to eq(created_at)
    end

    it 'finalizes the document on s3' do
      expect_any_instance_of(AttachedDocument).to receive(:finalize_document)

      response
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "createDocument"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "createDocument"
  end
end
