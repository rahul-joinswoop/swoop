# frozen_string_literal: true

require_relative './create_job_shared'

describe Mutations::CreateJob do
  include_context "create job shared"

  shared_examples 'partner' do
    context "partner.department.name" do
      let(:query) do
        qs = super().dup
        # insert partner.department.name into our query
        qs.insert(qs.index(/^\s+?id/), "partner{ department { name } }\n")
        qs
      end

      context "with a valid partner.department.name" do
        let(:job_input) { create(:graphql_job_input_type, :with_partner_department, company: api_company) }

        it_behaves_like "creates a job"
      end

      context "with an invalid partner.department.name" do
        let(:department_name) { Faker::Name.name }
        let(:job_input) { create(:graphql_job_input_type, company: api_company).tap { |i| i[:partner] = { department: { name: department_name } } } }
        let(:errors_array) do
          [
            message: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['department'],
            extensions: {
              problems: [{
                explanation: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['department'],
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "createJob", "input", "job", "partner", "department"],
          ]
        end

        it_behaves_like "responds with error"
      end
    end

    context "partner.vehicle.name" do
      let(:query) do
        qs = super().dup
        # insert partner.vehicle.name into our query
        qs.insert(qs.index(/^\s+?id/), "partner{ vehicle { name } }\n")
        qs
      end

      context "with a valid partner.vehicle.name" do
        let(:job_input) { create(:graphql_job_input_type, :with_partner_vehicle, company: api_company) }

        it_behaves_like "creates a job"
      end

      context "with an invalid partner.vehicle.name" do
        let(:vehicle_name) { Faker::Name.name }
        let(:job_input) { create(:graphql_job_input_type, company: api_company).tap { |i| i[:partner] = { vehicle: { name: vehicle_name } } } }
        let(:errors_array) do
          [
            message: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['vehicle'],
            extensions: {
              problems: [{
                explanation: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['vehicle'],
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "createJob", "input", "job", "partner", "vehicle"],
          ]
        end

        it_behaves_like "responds with error"
      end
    end

    context "partner.trailer.name" do
      let(:query) do
        qs = super().dup
        # insert partner.trailer.name into our query
        qs.insert(qs.index(/^\s+?id/), "partner{ trailer { name } }\n")
        qs
      end

      context "with a valid partner.trailer.name" do
        let(:job_input) { create(:graphql_job_input_type, :with_partner_trailer, company: api_company) }

        it_behaves_like "creates a job"
      end

      context "with an invalid partner.trailer.name" do
        let(:trailer_name) { Faker::Name.name }
        let(:job_input) { create(:graphql_job_input_type, company: api_company).tap { |i| i[:partner] = { trailer: { name: trailer_name } } } }
        let(:errors_array) do
          [
            message: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['trailer'],
            extensions: {
              problems: [{
                explanation: Analyzers::Validations::PartnerInfo::ERROR_MESSAGE % ['trailer'],
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "createJob", "input", "job", "partner", "trailer"],
          ]
        end

        it_behaves_like "responds with error"
      end
    end

    context 'other partner fields' do
      let(:job_input) { create :graphql_job_input_type, :with_partner_vehicle_fields, company: api_company }
      let(:query) do
        qs = super().dup
        # insert symptom into our query as a sibling of make inside of vehicle
        qs.insert(qs.index(/^\s+?make/), "serialNumber\nstyle\ntireSize\nunitNumber\n ")
        qs
      end

      it_behaves_like "creates a job"
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "partner"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "partner"
  end
end
