# frozen_string_literal: true

require_relative './create_job_shared'

describe Mutations::CreateJob do
  include_context "create job shared"

  shared_examples 'fullName vs name' do
    let(:job_input) do
      super().tap do |j|
        j[:customer][:fullName] = j[:customer].delete(:name)
        [:pickupContact, :dropoffContact].each do |contact|
          j[:location][contact][:fullName] = j[:location][contact].delete(:name)
        end
      end
    end
    let(:job_output) do
      super().deep_dup.tap do |j|
        ['pickupContact', 'dropoffContact'].each do |k|
          j['location'][k]['fullName'] = j['location'][k]['name'] = job_input[:location][k.to_sym][:fullName]
        end
        j['customer']['fullName'] = j['customer']['name'] = job_input[:customer][:fullName]
      end
    end

    it_behaves_like "creates a job"
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "fullName vs name"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "fullName vs name"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "fullName vs name"
  end
end
