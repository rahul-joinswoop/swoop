# frozen_string_literal: true

require_relative './create_job_shared'

describe Mutations::CreateJob do
  include_context "create job shared"

  shared_examples "creating a draft job" do
    let(:job_input) { create(:graphql_job_input_type, :draft, company: api_company) }
    let(:status) { "Draft" }
    let(:job_output) { super().deep_dup.tap { |j| j['status'] = status } }

    it_behaves_like  "creates a job"
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "creating a draft job"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "creating a draft job"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "creating a draft job"
  end
end
