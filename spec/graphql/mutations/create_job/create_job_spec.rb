# frozen_string_literal: true

require_relative './create_job_shared'

describe Mutations::CreateJob do
  include_context "create job shared"

  shared_examples "sendTextsToPickup without pickupContact.phone" do
    let(:job_input) do
      super().deep_dup.tap do |j|
        j[:location][:pickupContact].delete :phone
        j[:texts][:sendTextsToPickup] = true
      end
    end
    let(:errors_array) do
      {
        extensions: {
          problems: [{
            explanation: Analyzers::Validations::Texts::ERROR_MESSAGE,
          }],
        },
        locations: [{ column: 3, line: 2 }],
        message: Analyzers::Validations::Texts::ERROR_MESSAGE,
        path: ["mutation #{operation_name}", "createJob", "input", "job", "pickupContact"],
      }
    end

    it_behaves_like "responds with error"
  end

  shared_examples 'Client API Coverage Override' do
    before do
      api_company.features << create(:feature, name: Feature::CLIENT_API_COVERAGE_OVERRIDE)
    end

    it_behaves_like "creates a job"

    it 'does not create a coverage to the job' do
      job_ssid = response_data("createJob", "job", "id")

      job = SomewhatSecureID.load! job_ssid

      expect(job.pcc_coverage_id).to be_nil
    end

    context 'and Request.current.source_application.client_api? is true' do
      let(:wrap_in_request_context) { true }

      it_behaves_like "creates a job"

      it 'creates a Covered PCC::Coverage to the job' do
        job_ssid = response_data("createJob", "job", "id")
        job = SomewhatSecureID.load! job_ssid

        pcc_coverage_data = job.pcc_coverage.data[0]

        expect(pcc_coverage_data['result']).to eq ClientProgram::COVERED
        expect(pcc_coverage_data['name']).to eq ClientProgram::CLIENT_API_COVERED
        expect(pcc_coverage_data['code']).to eq ClientProgram::CLIENT_API_COVERED_CODE
        expect(pcc_coverage_data['coverage_notes']).to eq ClientProgram::CLIENT_API_COVERED_NOTES
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "creates a job"
    it_behaves_like "sendTextsToPickup without pickupContact.phone"
    it_behaves_like 'Client API Coverage Override'
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "creates a job"
    it_behaves_like "sendTextsToPickup without pickupContact.phone"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "creates a job"
    it_behaves_like "sendTextsToPickup without pickupContact.phone"
  end
end
