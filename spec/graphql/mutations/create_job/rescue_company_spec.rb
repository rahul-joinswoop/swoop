# frozen_string_literal: true

require_relative './create_job_shared'

describe Mutations::CreateJob do
  include_context "create job shared"

  shared_examples 'rescue company' do
    context 'with api_user' do
      context 'when api_user is driver only' do
        let(:api_user) { rescue_driver }

        it 'automatically assigns the api_user as the job.rescue_driver' do
          expect(job_response.dig('partner', 'driver', 'name')).to eq api_user.full_name
        end

        it 'sets job.status to Dispatched' do
          expect(job_response.dig('status')).to eq 'Dispatched'
        end
      end

      context 'when api_user is not driver only' do
        let(:api_user) { rescue_dispatcher_driver }

        it 'does not assign a job.rescue_driver' do
          expect(job_response.dig('partner', 'driver', 'name')).to be_nil
        end

        it 'sets job.status to Pending' do
          expect(job_response.dig('status')).to eq 'Pending'
        end
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "rescue company"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "rescue company"
  end
end
