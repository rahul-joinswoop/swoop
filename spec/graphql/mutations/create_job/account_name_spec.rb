# frozen_string_literal: true

require_relative './create_job_shared'

describe Mutations::CreateJob do
  include_context "create job shared"

  shared_examples "account.name" do
    let(:query) do
      qs = super().dup
      # insert account.name into our query
      qs.insert(qs.index(/^\s+?id/), "account{ name }\n")
      qs
    end

    context "with a valid account.name" do
      let(:job_input) { create(:graphql_job_input_type, :with_account, company: api_company) }

      it_behaves_like "creates a job"
    end

    context "with an invalid account.name" do
      let(:account_name) { Faker::Name.name }
      let(:job_input) { create(:graphql_job_input_type, company: api_company).tap { |i| i[:account] = { name: account_name } } }
      let(:errors_array) do
        {
          message: Analyzers::Validations::AccountName::ERROR_MESSAGE % [account_name.inspect],
          extensions: {
            problems: [{
              explanation: Analyzers::Validations::AccountName::ERROR_MESSAGE % [account_name.inspect],
            }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "createJob", "input", "job", "account", "name"],
        }
      end

      it_behaves_like "responds with error"
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "account.name"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "account.name"
  end
end
