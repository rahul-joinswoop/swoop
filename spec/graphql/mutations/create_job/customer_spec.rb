# frozen_string_literal: true

require_relative './create_job_shared'

describe Mutations::CreateJob do
  include_context "create job shared"

  shared_examples 'customer' do
    describe "customer.name" do
      context "when api_company does not have 'Don't require customer name and phone' enabled" do
        let(:job_input) do
          super().deep_dup.tap { |j| j[:customer][:name] = nil }
        end

        let(:errors_array) do
          {
            extensions: { problems: [{ explanation: Analyzers::Validations::CustomerInfo::CUSTOMER_NAME_ERROR_MESSAGE }] },
            locations: [{ column: 3, line: 2 }],
            message: Analyzers::Validations::CustomerInfo::CUSTOMER_NAME_ERROR_MESSAGE,
            path: ["mutation #{operation_name}", "createJob", "input", "job", "customer"],
          }
        end

        it_behaves_like "responds with error"
      end
    end

    describe "phone.number" do
      let(:phone) { "asdasdf adf asdf asdf" }
      let(:job_input) do
        super().deep_dup.tap { |j| j[:customer][:phone] = phone }
      end

      let(:errors_array) do
        {
          extensions: { code: "argumentLiteralsIncompatible", typeName: "CoercionError" },
          locations: [{ column: nil, line: nil }],
          message: "#{phone.inspect} is not a valid phone number",
          path: [
            "mutation #{operation_name}",
            "createJob",
            "input",
            "job",
            "customer",
            "phone",
          ],
        }
      end

      it_behaves_like "responds with error"

      context "when api_company does not have 'Don't require customer name and phone' enabled" do
        let(:job_input) do
          super().deep_dup.tap { |j| j[:customer][:phone] = nil }
        end

        let(:errors_array) do
          {
            extensions: { problems: [{ explanation: Analyzers::Validations::CustomerInfo::CUSTOMER_PHONE_ERROR_MESSAGE }] },
            locations: [{ column: 3, line: 2 }],
            message: Analyzers::Validations::CustomerInfo::CUSTOMER_PHONE_ERROR_MESSAGE,
            path: ["mutation #{operation_name}", "createJob", "input", "job", "customer"],
          }
        end

        it_behaves_like "responds with error"
      end
    end

    context "when api_company has 'Don't require customer name and phone'" do
      let!(:dont_require_customer_feature) { create(:feature, name: "Don't require customer name and phone") }

      before do
        api_company.features << dont_require_customer_feature
      end

      context 'and customer is filled' do
        let(:job_input) do
          super().deep_dup.tap { |j| j[:customer] = { name: 'William Shakespeare', phone: '4058743876' } }
        end

        let(:job_output) do
          super().deep_dup.tap { |j| j["customer"]["phone"] = '+14058743876' }
        end

        it_behaves_like "creates a job"
      end

      context 'and customer is nil' do
        let(:job_input) do
          super().deep_dup.tap { |j| j.delete(:customer) }
        end

        let(:job_output) do
          super().deep_dup.tap { |j| j["customer"] = { "name" => "", "phone" => nil } }
        end

        it_behaves_like "creates a job"
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "customer"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "customer"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "customer"
  end
end
