# frozen_string_literal: true

require_relative './create_job_shared'

describe Mutations::CreateJob do
  include_context "create job shared"

  shared_examples "eta.current" do
    let(:query) do
      qs = super().dup
      # insert eta.bidded into our query
      qs.insert(qs.index(/^\s+?id/), "eta{ current bidded }\n")
      qs
    end
    let(:current) { Time.now + 30.minutes }
    let(:job_input) { super().deep_dup.tap { |j| j[:eta] = { current: current.utc.to_formatted_s(:iso8601) } } }
    let(:job_output) do
      super().deep_dup.tap do |j|
        j['eta']['current'] = j['eta']['bidded'] = j['eta'].delete('current')
      end
    end

    it_behaves_like "creates a job"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "eta.current"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "eta.current"
  end
end
