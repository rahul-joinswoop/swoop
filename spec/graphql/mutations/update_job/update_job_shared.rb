# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "update job shared" do
  let(:encoded_job_id) { job.to_ssid }

  shared_examples "with a FleetManagedJob" do |example|
    let(:job) do
      create :fleet_managed_job, fleet_company: fleet_managed_company, rescue_company: rescue_company, status: status
    end

    it_behaves_like example
  end

  shared_examples "with a RescueJob" do |example|
    let(:job) do
      create :rescue_job, fleet_company: rescue_company, rescue_company: rescue_company, status: status
    end

    it_behaves_like example
  end

  shared_examples "updates a job" do
    context "location" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobInput!) {
          updateJob(input: $input) {
            job {
              id
              location {
                serviceLocation{
                  address
                  lat
                  lng
                  locationType
                  exact
                }
              }
            }
          }
        }
        GRAPHQL
      end
      let(:service_location) { create :graphql_location_input_type }
      let(:variables) do
        {
          input: {
            job: {
              id: encoded_job_id,
              location: {
                serviceLocation: service_location,
              },
            },
          },
        }
      end

      context "with errors" do
        let(:encoded_job_id) { Job.new(id: 123123123).to_ssid }
        let(:errors_array) { { message: "invalid Job #{encoded_job_id.inspect}" } }

        it_behaves_like "responds with error"
      end

      it "works" do
        # we didn't blow up
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')

        job_response = response.dig('data', 'updateJob', 'job')

        # if we didn't submit an exact: value then we remove "exact: nil" from
        # our job_response so our comparison passes
        if service_location[:exact].nil? && job_response.dig('location', 'serviceLocation', 'exact').nil?
          job_response['location']['serviceLocation'].delete 'exact'
        end

        job.reload

        # and our job actually changed - here it's really the service_location
        # that we have to verify
        sl = job.service_location

        # our job_output isn't in a let block because we have to make sure job
        # is reloaded before we build it
        expect(job_response).to include({
          id: encoded_job_id,
          location: {
            serviceLocation: service_location,
          },
        }.deep_stringify_keys)

        # job status shouldn't change
        expect(job.status).to eq(status.downcase)

        # deal with some floating point rounding errors
        [:lat, :lng].each do |k|
          expect(sl[k]).to be_within(0.0000000001).of(service_location[k])
        end
        expect(sl[:address]).to eq(service_location[:address])
        expect(sl[:place_id]).to eq(service_location[:googlePlaceId])
        expect(sl.location_type.name).to eq(service_location[:locationType])
      end
    end

    context "po_number" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobInput!) {
          updateJob(input: $input) {
            job {
              id
              poNumber
            }
          }
        }
        GRAPHQL
      end
      let(:po_number) { Faker::Company.po_number }
      let(:variables) { { input: { job: { poNumber: po_number, id: encoded_job_id } } } }

      context 'when company has Feature::JOB_PO_NUMBER' do
        before do
          api_company.features << create(:feature, name: Feature::JOB_PO_NUMBER)
        end

        it "works" do
          # we didn't blow up
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          job_response = response.dig('data', 'updateJob', 'job')

          # and our job actually changed
          expect(job.reload).to have_attributes(
            po_number: po_number,
          )

          # our job_output isn't in a let block because we have to make sure job
          # is reloaded before we build it
          expect(job_response).to include({
            id: encoded_job_id,
            poNumber: po_number,
          }.deep_stringify_keys)
        end
      end
    end

    context 'when company has Feature::SHOW_SYMPTOMS enabled' do
      before do
        api_company.features << create(:feature, name: Feature::SHOW_SYMPTOMS)
      end

      context "when symptom is being changed" do
        let(:query) do
          <<~GRAPHQL
          mutation #{operation_name}($input: UpdateJobInput!) {
            updateJob(input: $input) {
              job {
                id
                service {
                  symptom
                }
              }
            }
          }
          GRAPHQL
        end

        let(:symptom) { create(:symptom) }
        let(:variables) { { input: { job: { service: { symptom: symptom_name }, id: encoded_job_id } } } }

        shared_examples 'update the symptom accordingly' do
          it "works" do
            # we didn't blow up
            expect { response }.not_to raise_error
            expect(response).not_to include('errors')
            job_response = response.dig('data', 'updateJob', 'job')

            # and our job actually changed
            if symptom_name.present?
              expect(job.reload).to have_attributes(symptom_id: symptom.id)
            else
              expect(job.reload).to have_attributes(symptom_id: nil)
            end

            # our job_output isn't in a let block because we have to make sure job
            # is reloaded before we build it
            expect(job_response).to include({
              id: encoded_job_id,
              service: { symptom: symptom_name },
            }.deep_stringify_keys)
          end
        end

        context 'when a valid symptom is passed' do
          let(:symptom_name) { symptom.name }

          it_behaves_like 'update the symptom accordingly'
        end

        context 'when symptom is passed as null' do
          let(:symptom_name) { nil }

          it_behaves_like 'update the symptom accordingly'
        end

        context 'and there is no symptom key' do
          let(:variables) { { input: { job: { id: encoded_job_id } } } }

          it_behaves_like 'does not raise error'
        end

        context "when the symptom does not exist" do
          let(:symptom) { Faker::Hipster.sentence }
          let(:variables) { { input: { job: { service: { symptom: symptom }, id: encoded_job_id } } } }
          let(:errors_array) do
            {
              message: "#{symptom.inspect} is not a valid Symptom",
              locations: [{ line: nil, column: nil }],
              path: ["mutation #{operation_name}", "updateJob", "input", "job", "service", "symptom"],
              extensions: {
                code: "argumentLiteralsIncompatible",
                typeName: "CoercionError",
              },
            }
          end

          it_behaves_like "responds with error"
        end
      end
    end

    context "notes" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobInput!) {
          updateJob(input: $input) {
            job {
              id
              notes{
                customer
                internal
              }
            }
          }
        }
        GRAPHQL
      end
      let(:notes) { create :graphql_notes_input_type }
      let(:variables) { { input: { job: { notes: notes, id: encoded_job_id } } } }

      it "works" do
        # we didn't blow up
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        job_response = response.dig('data', 'updateJob', 'job')

        # and our job actually changed
        expected_attributes = {
          notes: notes[:customer],
          status: status.downcase,
        }

        expected_attributes[api_company.internal_notes_field] = notes[:internal]

        expect(job.reload).to have_attributes(expected_attributes)

        # our job_output isn't in a let block because we have to make sure job
        # is reloaded before we build it
        expect(job_response).to include({
          id: encoded_job_id,
          notes: {
            customer: job.notes,
            internal: job.send(api_company.internal_notes_field),
          },
        }.deep_stringify_keys)
      end
    end

    context "storageType" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobInput!) {
          updateJob(input: $input) {
            job {
              id
              service {
                storageType
              }
            }
          }
        }
        GRAPHQL
      end
      let(:storage_type) { create :storage_type, company: api_company }
      let(:variables) { { input: { job: { service: { storageType: storage_type.name }, id: encoded_job_id } } } }

      it_behaves_like 'does not raise error'

      it "works" do
        job_response = response.dig('data', 'updateJob', 'job')

        # and our job actually changed
        expect(job.reload).to have_attributes(
          storage_type_id: storage_type.id,
          status: status.downcase,
        )

        # our job_output isn't in a let block because we have to make sure job
        # is reloaded before we build it
        expect(job_response).to include({
          id: encoded_job_id,
          service: { storageType: storage_type.name },
        }.deep_stringify_keys)
      end

      describe "removing storage type" do
        let(:variables) { { input: { job: { service: { storageType: nil }, id: encoded_job_id } } } }
        let(:job) do
          j = super()
          j.update! storage_type: storage_type
          j
        end

        it "works" do
          job_response = response.dig('data', 'updateJob', 'job')

          # and our job actually changed
          expect(job.reload).to have_attributes(
            storage_type_id: nil,
          )

          # our job_output isn't in a let block because we have to make sure job
          # is reloaded before we build it
          expect(job_response).to include({
            id: encoded_job_id,
            service: { storageType: nil },
          }.deep_stringify_keys)
        end
      end
    end
  end
end
