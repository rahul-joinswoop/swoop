# frozen_string_literal: true

require_relative "./update_job_shared"

describe Mutations::UpdateJob do
  include_context "update job shared"

  shared_examples 'vehicle' do
    let(:job) { create :rescue_job, rescue_company: api_company }

    describe 'odometer' do
      # make sure we handle odometer values > 32bits
      let(:odometer) { 3900067695.0 }

      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobInput!) {
          updateJob(input: $input) {
            job {
              id
              vehicle {
                odometer
              }
            }
          }
        }
        GRAPHQL
      end

      let(:variables) do
        {
          input: {
            job: {
              id: encoded_job_id,
              vehicle: {
                odometer: odometer,
              },
            },
          },
        }
      end

      it 'works' do
        expect(response).not_to include('errors')
        expect(response.dig('data', 'updateJob', 'job', 'vehicle', 'odometer')).to eq(odometer.to_i.to_s)
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"

    it_behaves_like "vehicle"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"

    it_behaves_like "vehicle"
  end
end
