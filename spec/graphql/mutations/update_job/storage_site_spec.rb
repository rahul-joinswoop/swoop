# frozen_string_literal: true

require_relative "./update_job_shared"

describe Mutations::UpdateJob do
  include_context "update job shared"

  shared_examples "storageSite" do
    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: UpdateJobInput!) {
        updateJob(input: $input) {
          job {
            id
            service {
              storageSite
              willStore
            }
          }
        }
      }
      GRAPHQL
    end

    let!(:job) do
      create :fleet_managed_job, status: :pending, fleet_company: fleet_managed_company, rescue_company: api_company
    end
    let(:storage_site) { create(:site, company: api_company, location: create(:location, :random, lat: 80.9302174, lng: 103.155902)) }
    let(:variables) { { input: { job: { service: { storageSite: storage_site.name, willStore: will_store }, id: encoded_job_id } } } }
    let(:will_store) { true }

    it_behaves_like 'does not raise error'

    shared_examples "it works" do
      it "works" do
        expect_any_instance_of(AnalyticsData::Storage).to receive(:push_message).once
        job_response = response.dig('data', 'updateJob', 'job')
        storage_site_location = storage_site.location
        expect(job.reload.drop_location).to have_attributes(
          site_id: storage_site.id,
          address: storage_site_location.address,
          lat: 80.9302174,
          lng: 103.155902,
          exact: storage_site_location.exact,
          place_id: storage_site_location.place_id,
          location_type_id: storage_site_location.location_type_id,
        )

        expect(job.will_store).to eq(will_store)

        expect(job_response).to include({
          id: encoded_job_id,
          service: { storageSite: storage_site.name, willStore: will_store },
        }.deep_stringify_keys)
      end
    end

    it_behaves_like "it works"

    context "when will_store is false" do
      let(:will_store) { false }

      it_behaves_like "it works"
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"

    it_behaves_like "storageSite"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"

    it_behaves_like "storageSite"
  end
end
