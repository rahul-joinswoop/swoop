# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdateInvoice do
  shared_examples 'updateInvoice' do
    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: UpdateInvoiceInput!) {
        updateInvoice(input: $input) {
          invoice {
            id
            classType
            currency
            job {
              id
            }
            lineItems {
              edges {
                node {
                  id
                  autoCalculatedQuantity
                  calcType
                  currency
                  deletedAt
                  description
                  estimated
                  netAmount
                  originalQuantity
                  originalUnitPrice
                  quantity
                  rate {
                    id
                  }
                  taxAmount
                  unitPrice
                  userCreated
                }
              }
            }
            payments {
              edges {
                node {
                  id
                  amount
                  currency
                  memo
                  paymentMethod
                  type
                }
              }
            }
            rateType {
              name
              type
            }
          }
        }
      }
      GRAPHQL
    end

    let(:variables) do
      {
        input: {
          invoice: {
            id: original_invoice.id, # it can be a sample id, it does not matter at the end
            balance: original_invoice.balance.to_s,
            classType: new_class_type,
            lineItems: line_items_input,
            notes: new_notes,
            payments: [
              {
                id: original_invoice.payments_or_credits.first.to_ssid,
                amount: original_invoice.payments_or_credits.first.total_amount.to_s,
                deletedAt: nil,
                markPaidAt: nil,
                memo: original_invoice.payments_or_credits.first.description,
                paymentMethod: original_invoice.payments_or_credits.first.payment_method,
                type: original_invoice.payments_or_credits.first.type,
              },
            ],
            job: {
              id: job_ssid,
            },
            rateType: { type: new_rate_type&.friendly_type },
            subtotal: original_invoice.subtotal.to_s,
            totalAmount: original_invoice.total_amount.to_s,
          },
        },
      }
    end

    let(:line_items_input) do
      [
        {
          id: original_li_flat.to_ssid,
          autoCalculatedQuantity: false,
          calcType: "Flat",
          deletedAt: li_flat_changed_deleted_at,
          description: original_li_flat.description,
          estimated: false,
          job: {
            id: job.to_ssid,
          },
          originalQuantity: original_li_flat.original_quantity.to_s,
          originalUnitPrice: original_li_flat.original_unit_price.to_s,
          quantity: "2.00",
          netAmount: "4000.00",
          rate: { id: rate.to_ssid },
          taxAmount: "0.00",
          unitPrice: "2000.00",
          userCreated: false,
        },
        {
          id: original_li_addition_dolly.to_ssid,
          autoCalculatedQuantity: false,
          calcType: "Flat",
          deletedAt: nil,
          description: original_li_addition_dolly.description,
          estimated: false,
          job: {
            id: job.to_ssid,
          },
          originalQuantity: original_li_addition_dolly.original_quantity.to_s,
          originalUnitPrice: original_li_addition_dolly.original_unit_price.to_s,
          quantity: "3.00",
          netAmount: "15.00",
          rate: { id: rate.to_ssid },
          taxAmount: "0.00",
          unitPrice: "5.00",
          userCreated: false,
        },
        {
          id: "a_fake_one_generated_by_sample_flow",
          autoCalculatedQuantity: false,
          calcType: "Flat",
          deletedAt: nil,
          description: "Fuel",
          estimated: false,
          job: {
            id: job.to_ssid,
          },
          originalQuantity: "0.00",
          originalUnitPrice: "8.00",
          quantity: "1.00",
          netAmount: "9.00",
          rate: { id: nil },
          taxAmount: "0.00",
          unitPrice: "9.00",
          userCreated: true,
        },
        {
          id: original_li_tax.to_ssid,
          autoCalculatedQuantity: false,
          calcType: "Flat",
          deletedAt: nil,
          description: original_li_tax.description,
          estimated: false,
          job: {
            id: job.to_ssid,
          },
          originalQuantity: original_li_tax.original_quantity.to_s,
          originalUnitPrice: original_li_tax.original_unit_price.to_s,
          quantity: "0.00",
          netAmount: "0.00",
          rate: { id: rate.to_ssid },
          taxAmount: "9.00",
          unitPrice: "0.00",
          userCreated: false,
        },
      ]
    end

    let(:job_ssid) { job.to_ssid }

    let(:original_invoice) do
      create(
        :invoice, :with_payments, :with_refunds,
        currency: 'USD',
        line_items: [original_li_flat, original_li_addition_dolly, original_li_tax],
        recipient_company: fleet_company,
        sender: rescue_company,
        state: 'partner_new',
        rate_type: rate_type,
      )
    end

    let!(:job) do
      job = original_invoice.job
      job.rescue_company = rescue_company
      job.fleet_company = fleet_company
      job.account = account
      job.invoice = original_invoice
      job.invoice_vehicle_category = invoice_vehicle_category
      job.driver_notes = 'Original Driver Notes'

      original_invoice.job = job
      original_invoice.line_items.each do |li|
        li.job_id = job.id
        li.save!
      end

      original_invoice.save!
      job.save!

      original_invoice.reload
      job.reload

      job
    end

    let(:original_li_flat) do
      create(
        :invoicing_line_item,
        currency: 'USD',
        description: "Flat Rate",
        estimated: true,
        net_amount: BigDecimal("10.0"),
        original_quantity: BigDecimal("1.00"),
        original_unit_price: BigDecimal("10.00"),
        quantity: BigDecimal("1.0"),
        tax_amount: BigDecimal("0.0"),
        unit_price: BigDecimal("10.0"),
        user_created: false,
        rate_id: rate.id,
      )
    end

    let(:li_flat_changed_deleted_at) { nil }

    let(:original_li_addition_dolly) do
      create(
        :invoicing_line_item,
        currency: 'USD',
        description: "Dolly",
        estimated: true,
        net_amount: BigDecimal("5.0"),
        original_quantity: BigDecimal("1.0"),
        original_unit_price: BigDecimal("5.0"),
        quantity: BigDecimal("1.0"),
        tax_amount: BigDecimal("0.0"),
        unit_price: BigDecimal("5.0"),
        user_created: false,
        rate_id: rate.id,
        calc_type: 'Flat'
      )
    end

    let(:original_li_tax) do
      create(
        :invoicing_line_item,
        currency: 'USD',
        description: "Tax",
        estimated: true,
        net_amount: BigDecimal("0.0"),
        original_quantity: BigDecimal("0.0"),
        original_unit_price: BigDecimal("0.0"),
        quantity: BigDecimal("0.0"),
        tax_amount: BigDecimal("0.0"),
        unit_price: BigDecimal("0.0"),
        user_created: false,
        rate_id: rate.id,
        calc_type: 'Flat'
      )
    end

    let(:new_class_type) { class_type.name }
    let(:new_notes) { 'This is the new notes' }
    let(:invoice_vehicle_category) { create(:vehicle_category, name: VehicleCategory::FLATBED) }

    let(:fleet_company) { fleet_managed_company }
    let!(:account) { create(:account, company: api_company, client_company: fleet_company) }
    let!(:rate) { create(:clean_flat_rate, company: api_company, account: account) }
    let!(:new_rate) { create(:hourly_p2p, company: api_company, account: account) }
    let(:rate_type) { create(:rate_type, :flat) }

    let!(:class_type) { create(:vehicle_category, name: "Super Heavy") }

    let(:new_rate_type) { rate_type }

    before do
      api_company.vehicle_categories << class_type
    end

    it 'updates the invoice accordingly' do
      invoice = response_data("updateInvoice", "invoice")

      expect(invoice["id"]).to eq original_invoice.to_ssid
      expect(invoice.dig("rateType", "name")).to eq "Flat"
      expect(invoice.dig("rateType", "type")).to eq "Flat"
      if new_class_type
        expect(invoice["classType"]).to eq "Super Heavy"
      else
        expect(invoice["classType"]).to be_nil
      end

      expect(invoice.dig("job", "id")).to eq job.to_ssid

      line_items = invoice.dig("lineItems", "edges").map { |li| li["node"] }

      li_flat_rate_returned = line_items.find { |li| li["description"] == "Flat Rate" }
      expect(li_flat_rate_returned["id"]).to eq original_li_flat.to_ssid
      expect(li_flat_rate_returned["unitPrice"]).to eq "2000.00"
      expect(li_flat_rate_returned["quantity"]).to eq "2.00"
      expect(li_flat_rate_returned["netAmount"]).to eq "4000.00"
      expect(li_flat_rate_returned["originalUnitPrice"]).to eq "10.00"
      expect(li_flat_rate_returned["originalQuantity"]).to eq "1.00"

      li_dolly_returned = line_items.find { |li| li["description"] == "Dolly" }
      expect(li_dolly_returned["id"]).to eq original_li_addition_dolly.to_ssid
      expect(li_dolly_returned["unitPrice"]).to eq "5.00"
      expect(li_dolly_returned["quantity"]).to eq "3.00"
      expect(li_dolly_returned["netAmount"]).to eq "15.00"
      expect(li_dolly_returned["originalUnitPrice"]).to eq "5.00"
      expect(li_dolly_returned["originalQuantity"]).to eq "1.00"

      li_fuel_returned = line_items.find { |li| li["description"] == "Fuel" }

      original_invoice.reload

      if new_notes
        expect(original_invoice.job.driver_notes).to eq 'This is the new notes'
      else
        expect(original_invoice.job.driver_notes).to be_nil
      end

      if new_class_type
        expect(original_invoice.job.invoice_vehicle_category.name).to eq new_class_type
      else
        expect(original_invoice.job.invoice_vehicle_category).to be_nil
      end

      fuel_li_in_db = original_invoice.line_items.find_by_description("Fuel")

      expect(li_fuel_returned["id"]).to eq fuel_li_in_db.to_ssid
      expect(li_fuel_returned["unitPrice"]).to eq "9.00"
      expect(li_fuel_returned["quantity"]).to eq "1.00"
      expect(li_fuel_returned["netAmount"]).to eq "9.00"
      expect(li_fuel_returned["originalUnitPrice"]).to eq "8.00"
      expect(li_fuel_returned["originalQuantity"]).to eq "0.00"

      line_item_tax = line_items.find { |li| li["description"] == "Tax" }

      expect(line_item_tax["id"]).to eq original_li_tax.to_ssid
      expect(line_item_tax["unitPrice"]).to eq "0.00"
      expect(line_item_tax["quantity"]).to eq "0.00"
      expect(line_item_tax["netAmount"]).to eq "0.00"
      expect(line_item_tax["taxAmount"]).to eq "9.00"
      expect(line_item_tax["originalUnitPrice"]).to eq "0.00"
      expect(line_item_tax["originalQuantity"]).to eq "0.00"

      payments_or_credits = invoice.dig("payments", "edges")

      payment = payments_or_credits.first.dig("node")
      expect(payment["id"]).to be_kind_of String
      expect(payment["amount"]).to eq "-10.00"
      expect(payment["memo"]).to be_nil
      expect(payment["paymentMethod"]).to eq "Credit Card"
      expect(payment["type"]).to eq "Payment"

      credit = payments_or_credits.second.dig("node")
      expect(credit["id"]).to be_kind_of String
      expect(credit["amount"]).to eq "10.00"
      expect(credit["memo"]).to be_nil
      expect(credit["paymentMethod"]).to eq "Credit Card"
      expect(credit["type"]).to eq "Refund"
    end

    it "creates #{HistoryItem::INVOICE_EDITED.inspect} history item" do
      expect(CreateHistoryItem).to receive(:call).with(
        job: original_invoice.job,
        target: original_invoice,
        user: context[:api_user], # CreateHistoryItem treats it in case it's null
        event: HistoryItem::INVOICE_EDITED,
      )

      response
    end

    context "when li_flat_changed has deleted_at set" do
      let(:li_flat_changed_deleted_at) { Time.now.to_s }

      it "sets it as deleted in the db as well" do
        # make sure it's correctly set before calling the mutation
        flat_items = original_invoice.line_items.where(description: 'Flat Rate')
        expect(flat_items.size).to eq 1

        flat_item = flat_items.first
        expect(flat_item.deleted_at).to be_nil

        invoice = response_data("updateInvoice", "invoice") # call the mutation

        flat_items.reload
        flat_item.reload
        expect(flat_items.size).to eq 1
        expect(flat_item.deleted_at).to be_present

        line_items_returned = invoice.dig("lineItems", "edges").map { |li| li["node"] }

        li_flat_rate_deleted = line_items_returned.find { |li| li["description"] == "Flat Rate" && li.dig("rate", "id") == rate.to_ssid }
        expect(li_flat_rate_deleted).to be_nil

        li_dolly_rate_returned = line_items_returned.find { |li| li["description"] == "Dolly" && li.dig("rate", "id") == rate.to_ssid }
        expect(li_dolly_rate_returned).to be_present

        li_fuel_rate_returned = line_items_returned.find { |li| li["description"] == "Fuel" }
        expect(li_fuel_rate_returned).to be_present

        li_tax_rate_returned = line_items_returned.find { |li| li["description"] == "Tax" }
        expect(li_tax_rate_returned).to be_present
      end
    end

    context 'when RateType is changed' do
      let(:new_rate_type) { create(:rate_type, :hours_port_to_port) }

      it 'updates it accordingly' do
        invoice = response_data("updateInvoice", "invoice")

        expect(invoice.dig("rateType", "type")).to eq "HoursPortToPort"
      end
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    let(:api_user) { rescue_admin }

    it_behaves_like "updateInvoice"
    context 'when new_class_type is nil' do
      let(:new_class_type) { nil }

      it_behaves_like 'updateInvoice'
    end

    context 'when new_notes is nil' do
      let(:new_notes) { nil }

      it_behaves_like 'updateInvoice'
    end
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_admin }

    it_behaves_like "updateInvoice"
    context 'when new_class_type is nil' do
      let(:new_class_type) { nil }

      it_behaves_like 'updateInvoice'
    end

    context 'when new_notes is nil' do
      let(:new_notes) { nil }

      it_behaves_like 'updateInvoice'
    end
  end
end
