# frozen_string_literal: true

shared_context "create invoice", shared_context: :metadata do
  let(:invoice_created_with_flat_rate) do
    create(
      :invoice, :with_payments, :with_refunds,
      line_items: original_line_items,
      recipient_company: fleet_company,
      sender: rescue_company,
      state: 'partner_new',
      rate_type: original_flat_rate_type.type,
    )
  end

  let(:account) { create(:account, company: rescue_company, client_company: fleet_company) }
  let!(:original_flat_rate) { create(:clean_flat_rate, company: rescue_company, account: account, vehicle_category: original_flatbad_class_type) }
  let(:original_flat_rate_type) { create(:rate_type, :flat) }

  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company) }

  let(:original_flatbad_class_type) { create(:vehicle_category, name: VehicleCategory::FLATBED) }

  let!(:job) do
    job = invoice_created_with_flat_rate.job
    job.rescue_company = rescue_company
    job.fleet_company = fleet_company
    job.account = account
    job.invoice = invoice_created_with_flat_rate
    job.invoice_vehicle_category = original_flatbad_class_type
    job.driver_notes = 'Original Driver Notes'

    invoice_created_with_flat_rate.job = job
    invoice_created_with_flat_rate.line_items.each do |li|
      li.job_id = job.id
      li.save!
    end

    invoice_created_with_flat_rate.save!
    job.save!

    invoice_created_with_flat_rate.reload
    job.reload

    job
  end

  let(:original_line_items) { [original_li_flat, original_li_addition_dolly, original_li_tax] }

  let(:original_li_flat) do
    create(
      :invoicing_line_item,
      description: "Flat Rate",
      estimated: true,
      net_amount: BigDecimal("10.0"),
      original_quantity: BigDecimal("1.00"),
      original_unit_price: BigDecimal("10.00"),
      quantity: BigDecimal("1.0"),
      tax_amount: BigDecimal("0.0"),
      unit_price: BigDecimal("10.0"),
      user_created: false,
      rate_id: original_flat_rate.id,
    )
  end

  let(:li_flat_changed_deleted_at) { nil }

  let(:original_li_addition_dolly) do
    create(
      :invoicing_line_item,
      description: "Dolly",
      estimated: true,
      net_amount: BigDecimal("5.0"),
      original_quantity: BigDecimal("1.0"),
      original_unit_price: BigDecimal("5.0"),
      quantity: BigDecimal("1.0"),
      tax_amount: BigDecimal("0.0"),
      unit_price: BigDecimal("5.0"),
      user_created: false,
      rate_id: original_flat_rate.id,
      calc_type: 'Flat'
    )
  end

  let(:original_li_tax) do
    create(
      :invoicing_line_item,
      description: "Tax",
      estimated: true,
      net_amount: BigDecimal("0.0"),
      original_quantity: BigDecimal("0.0"),
      original_unit_price: BigDecimal("0.0"),
      quantity: BigDecimal("0.0"),
      tax_amount: BigDecimal("0.0"),
      unit_price: BigDecimal("0.0"),
      user_created: false,
      rate_id: original_flat_rate.id,
      calc_type: 'Flat'
    )
  end

  before do
    rescue_company.vehicle_categories << original_flatbad_class_type
  end
end
