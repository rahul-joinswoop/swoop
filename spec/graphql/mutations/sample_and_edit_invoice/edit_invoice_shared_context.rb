# frozen_string_literal: true

shared_context "edit invoice" do
  def run_edit_invoice_mutation_with(sample_invoice)
    new_rate_type_from_sample = sample_invoice.dig("rateType", "type")
    new_class_type_from_sample = sample_invoice.dig("classType")
    new_line_items_from_sample = sample_invoice.dig("lineItems").dig("edges").map { |li| li["node"] }
    new_notes_from_sample = sample_invoice.dig("notes")

    edit_invoice_variables = {
      input: {
        invoice: {
          id: 'whatever',
          balance: '0.0', # can be any
          rateType: {
            type: new_rate_type_from_sample,
          },
          classType: new_class_type_from_sample,
          totalAmount: '0.0', # can be any
          subtotal: '0.0', # can be any
          lineItems: new_line_items_from_sample,
          notes: new_notes_from_sample,
          payments: [
            {
              id: invoice_created_with_flat_rate.payments_or_credits.first.to_ssid,
              amount: invoice_created_with_flat_rate.payments_or_credits.first.total_amount.to_s,
              deletedAt: nil,
              markPaidAt: nil,
              memo: invoice_created_with_flat_rate.payments_or_credits.first.description,
              paymentMethod: invoice_created_with_flat_rate.payments_or_credits.first.payment_method,
              type: invoice_created_with_flat_rate.payments_or_credits.first.type,
            },
          ],
          job: {
            id: job.to_ssid,
          },
        },
      },
    }

    SwoopSchema.execute(
      query: edit_invoice_query,
      context: context,
      variables: edit_invoice_variables,
      operation_name: operation_name,
      only: only_filter,
      root_value: context[:api_company],
    ).dig("data", "updateInvoice", "invoice")
  end

  let(:edit_invoice_query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: UpdateInvoiceInput!) {
        updateInvoice(input: $input) {
          invoice {
            id
            balance
            totalAmount
            subtotal
            notes
            rateType {
              type
            }
            classType
            job {
              id
            }
            lineItems {
              edges {
                node {
                  id
                  autoCalculatedQuantity
                  calcType
                  deletedAt
                  description
                  estimated
                  netAmount
                  originalQuantity
                  originalUnitPrice
                  quantity
                  rate {
                    id
                  }
                  job {
                    id
                  }
                  taxAmount
                  unitPrice
                  userCreated
                }
              }
            }
            payments {
              edges {
                node {
                  id
                  amount
                  memo
                  paymentMethod
                  type
                }
              }
            }
          }
        }
      }
    GRAPHQL
  end

  shared_examples 'build sample and persist changes to the invoice correctly' do |expectations|
    it 'builds sample and edits the invoice correctly' do
      sample_invoice = run_sample_invoice_mutation

      invoice_response = run_edit_invoice_mutation_with(sample_invoice)

      check_invoice_from_response(invoice_response, expectations)
      check_invoice_from_db(expectations)
    end
  end

  def check_invoice_from_response(invoice_response, expectations)
    expect(invoice_response.dig("job", "id")).to eq job.to_ssid
    expect(invoice_response.dig("balance")).to eq expectations[:balance] + "0"
    expect(invoice_response.dig("totalAmount")).to eq expectations[:total_amount] + "0"
    expect(invoice_response.dig("subtotal")).to eq expectations[:subtotal] + "0"

    li_response = invoice_response.dig("lineItems").dig("edges").map { |li| li["node"] }

    expect(li_response.reject { |li| li["deletedAt"] }.size).to eq expectations[:line_items].size

    expectations[:line_items] do |expected_li|
      li = li_response.find { |lir| lir["description"] == expected_li[:description] && lir["deleted_at"].nil? }

      expect(li["unit_price"]).to eq expected_li[:unit_price].to_s + "0"
      expect(li["quantity"]).to eq expected_li[:quantity].to_s + "0"
      expect(li["unit_price"]).to eq expected_li[:net_amount].to_s + "0"
      expect(li["unit_price"]).to eq expected_li[:tax_amount].to_s + "0"
    end
  end

  def check_invoice_from_db(expectations)
    invoice = Invoice.find(invoice_created_with_flat_rate.id)

    expect(invoice.attributes["rate_type"]).to eq expectations[:rate_type]
    expect(invoice.job.invoice_vehicle_category&.name).to eq expectations[:class_type]
    expect(invoice.job.driver_notes).to eq expectations[:notes]

    expect(invoice.job_id).to eq invoice_created_with_flat_rate.job_id
    expect(invoice.balance.to_s).to eq expectations[:balance]
    expect(invoice.total_amount.to_s).to eq expectations[:total_amount]
    expect(invoice.subtotal.to_s).to eq expectations[:subtotal]

    expect(invoice.line_items.not_deleted.count).to eq expectations[:line_items].size

    expectations[:line_items].each do |expected_li|
      li = invoice.line_items.find_by(description: expected_li[:description], deleted_at: nil)

      expect(li.unit_price.to_s).to eq expected_li[:unit_price].to_s
      expect(li.quantity.to_s).to eq expected_li[:quantity].to_s
      expect(li.net_amount.to_s).to eq expected_li[:net_amount].to_s
      expect(li.tax_amount.to_s).to eq expected_li[:tax_amount].to_s
    end
  end
end
