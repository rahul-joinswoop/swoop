# frozen_string_literal: true

require 'rails_helper'
require_relative 'create_invoice_shared_context'
require_relative 'build_sample_invoice_shared_context'
require_relative 'edit_invoice_shared_context'

# This relies on lots of setup and expecations that are imported by include_context:
#
# - include_context 'create invoice' - creates the original invoice
# - include_context 'build sample invoice' - has all helpers to to kick buildSample mutation
# - include_context 'edit invoice' - has all helpers to kick updateInvoice mutation
#
# 'edit invoice' context is the responsible to kick both sampleInvoice call followed by
# an updateInvoice call, it simulates user changing something in the invoice in UI and saving it.
# The simulation is triggered by 'build sample and persist changes to the invoice correctly' shared_example,
# and it is used by all cases in this file.
#
describe 'Build Sample and Edit Invoice' do
  shared_examples 'build sample and edit invoice' do
    include_context 'create invoice'
    include_context 'build sample invoice'
    include_context 'edit invoice'

    context 'when there is no changes to Invoice.rate_type' do
      context 'and all line items have changes' do
        let(:li_flat_quantity_changed) { "2.0" }
        let(:li_flat_unit_price_changed) { "20.0" }

        let(:li_dolly_quantity_changed) { "3.0" }
        let(:li_dolly_unit_price_changed) { "30.0" }

        let(:li_flat_tax_tax_amount_changed) { "7.0" }

        it_behaves_like 'build sample and persist changes to the invoice correctly',
                        rate_type: 'FlatRate', class_type: 'Flatbed', balance: "137.0", total_amount: "137.0", subtotal: "130.0",
                        line_items: [
                          { description: "Flat Rate", unit_price: "20.0", quantity: "2.0", net_amount: "40.0", tax_amount: "0.0" },
                          { description: "Dolly", unit_price: "30.0", quantity: "3.0", net_amount: "90.0", tax_amount: "0.0" },
                          { description: "Tax", unit_price: "0.0", quantity: "0.0", net_amount: "0.0", tax_amount: "7.0" },
                        ]

        context 'and new additional item is added' do
          let(:fuel_additional_item_for_sample) do
            {
              id: 'a_fake_one',
              autoCalculatedQuantity: false,
              calcType: "Flat",
              deletedAt: nil,
              description: 'Fuel',
              estimated: false,
              job: {
                id: job.to_ssid,
              },
              originalQuantity: '1.0',
              originalUnitPrice: '0.0',
              quantity: '4.5',
              rate: nil,
              taxAmount: '0.0',
              unitPrice: '2.2',
              netAmount: '9.9',
              userCreated: true,
            }
          end

          let(:line_items_for_sample) { super() << fuel_additional_item_for_sample }

          it_behaves_like 'build sample and persist changes to the invoice correctly',
                          rate_type: 'FlatRate', class_type: 'Flatbed', balance: "146.9", total_amount: "146.9", subtotal: "139.9",
                          line_items: [
                            { description: "Flat Rate", unit_price: "20.0", quantity: "2.0", net_amount: "40.0", tax_amount: "0.0" },
                            { description: "Dolly", unit_price: "30.0", quantity: "3.0", net_amount: "90.0", tax_amount: "0.0" },
                            { description: "Fuel", unit_price: "2.2", quantity: "4.5", net_amount: "9.9", tax_amount: "0.0" },
                            { description: "Tax", unit_price: "0.0", quantity: "0.0", net_amount: "0.0", tax_amount: "7.0" },
                          ]

          context 'when Dolly line item is deleted' do
            let(:li_dolly_for_sample_deleted_at) { Time.now.to_s }

            it_behaves_like 'build sample and persist changes to the invoice correctly',
                            rate_type: 'FlatRate', class_type: 'Flatbed', balance: "56.9", total_amount: "56.9", subtotal: "49.9",
                            line_items: [
                              { description: "Flat Rate", unit_price: "20.0", quantity: "2.0", net_amount: "40.0", tax_amount: "0.0" },
                              { description: "Fuel", unit_price: "2.2", quantity: "4.5", net_amount: "9.9", tax_amount: "0.0" },
                              { description: "Tax", unit_price: "0.0", quantity: "0.0", net_amount: "0.0", tax_amount: "7.0" },
                            ]
          end
        end
      end
    end

    context "when rate_type and class_type are changed" do
      let!(:hourly_p2p_rate) { create(:hourly_p2p, company: rescue_company, account: account, hourly: 50.0, vehicle_category: nil) }

      let(:new_rate_type) { create(:rate_type, :hours_port_to_port) }
      let(:new_class_type) { nil }

      context 'and original_invoice also has an additional item' do
        let(:fuel_additional_item) do
          create(
            :invoicing_line_item,
            description: "Fuel",
            estimated: false,
            net_amount: BigDecimal("22.0"),
            original_quantity: BigDecimal("1.0"),
            original_unit_price: BigDecimal("11.0"),
            quantity: BigDecimal("2.0"),
            tax_amount: BigDecimal("0.0"),
            unit_price: BigDecimal("11.0"),
            user_created: true,
            rate_id: nil,
            calc_type: 'Flat'
          )
        end

        let(:original_line_items) { super() << fuel_additional_item }

        it_behaves_like 'build sample and persist changes to the invoice correctly',
                        rate_type: 'HoursP2PRate', class_type: nil, balance: "22.0", total_amount: "22.0", subtotal: "22.0",
                        line_items: [
                          { description: "Port To Port Hours", unit_price: "50.0", quantity: "0.0", net_amount: "0.0", tax_amount: "0.0" },
                          { description: "Fuel", unit_price: "11.0", quantity: "2.0", net_amount: "22.0", tax_amount: "0.0" },
                          { description: "Tax", unit_price: "0.0", quantity: "0.0", net_amount: "0.0", tax_amount: "0.0" },
                        ]
      end
    end

    context 'when notes are changed' do
      let(:new_notes) { 'Customer paid by cash and credit card' }

      it_behaves_like 'build sample and persist changes to the invoice correctly',
                      rate_type: 'FlatRate', class_type: 'Flatbed', balance: "15.0", total_amount: "15.0", subtotal: "15.0",
                      notes: 'Customer paid by cash and credit card',
                      line_items: [
                        { description: "Flat Rate", unit_price: "10.0", quantity: "1.0", net_amount: "10.0", tax_amount: "0.0" },
                        { description: "Dolly", unit_price: "5.0", quantity: "1.0", net_amount: "5.0", tax_amount: "0.0" },
                        { description: "Tax", unit_price: "0.0", quantity: "0.0", net_amount: "0.0", tax_amount: "0.0" },
                      ]
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    it_behaves_like "build sample and edit invoice"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    it_behaves_like "build sample and edit invoice"
  end
end
