# frozen_string_literal: true

shared_context "build sample invoice" do
  def run_sample_invoice_mutation
    SwoopSchema.execute(
      query: build_sample_query,
      context: context,
      variables: build_sample_variables,
      operation_name: operation_name,
      only: only_filter,
      root_value: context[:api_company],
    ).dig("data", "sampleInvoice", "invoice")
  end

  let(:build_sample_query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: SampleInvoiceInput!) {
        sampleInvoice(input: $input) {
          invoice {
            id
            rateType {
              type
            }
            classType
            notes
            job {
              id
            }
            lineItems {
              edges {
                node {
                  id
                  autoCalculatedQuantity
                  calcType
                  deletedAt
                  description
                  estimated
                  netAmount
                  originalQuantity
                  originalUnitPrice
                  quantity
                  rate {
                    id
                  }
                  job {
                    id
                  }
                  taxAmount
                  unitPrice
                  userCreated
                }
              }
            }
            payments {
              edges {
                node {
                  id
                  amount
                  memo
                  paymentMethod
                  type
                }
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let(:build_sample_variables) do
    {
      input: {
        invoice: {
          id: invoice_created_with_flat_rate.to_ssid,
          balance: invoice_created_with_flat_rate.balance.to_s,
          notes: new_notes,
          rateType: {
            type: new_rate_type.friendly_type,
          },
          classType: new_class_type&.name,
          totalAmount: invoice_created_with_flat_rate.total_amount.to_s,
          subtotal: invoice_created_with_flat_rate.subtotal_for_ui.to_s,
          lineItems: line_items_for_sample,
          payments: [
            {
              id: invoice_created_with_flat_rate.payments_or_credits.first.to_ssid,
              amount: invoice_created_with_flat_rate.payments_or_credits.first.total_amount.to_s,
              deletedAt: nil,
              markPaidAt: nil,
              memo: invoice_created_with_flat_rate.payments_or_credits.first.description,
              paymentMethod: invoice_created_with_flat_rate.payments_or_credits.first.payment_method,
              type: invoice_created_with_flat_rate.payments_or_credits.first.type,
            },
          ],
          job: {
            id: job.to_ssid,
          },
        },
      },
    }
  end

  let(:line_items_for_sample) { [li_flat_rate_for_sample, li_dolly_for_sample, li_flat_tax_for_sample] }

  let(:li_flat_rate_for_sample) do
    {
      id: original_li_flat.to_ssid,
      autoCalculatedQuantity: false,
      calcType: "Flat",
      deletedAt: nil,
      description: original_li_flat.description,
      estimated: false,
      job: {
        id: job.to_ssid,
      },
      originalQuantity: original_li_flat.original_quantity.to_s,
      originalUnitPrice: original_li_flat.original_unit_price.to_s,
      quantity: li_flat_quantity_changed,
      rate: { id: original_flat_rate.to_ssid },
      taxAmount: "0.00",
      unitPrice: li_flat_unit_price_changed,
      netAmount: original_li_flat.net_amount.to_s,
      userCreated: false,
    }
  end

  let(:li_dolly_for_sample) do
    {
      id: original_li_addition_dolly.to_ssid,
      autoCalculatedQuantity: false,
      calcType: "Flat",
      deletedAt: li_dolly_for_sample_deleted_at,
      description: original_li_addition_dolly.description,
      estimated: false,
      job: {
        id: job.to_ssid,
      },
      originalQuantity: original_li_addition_dolly.original_quantity.to_s,
      originalUnitPrice: original_li_addition_dolly.original_unit_price.to_s,
      quantity: li_dolly_quantity_changed,
      rate: { id: original_flat_rate.to_ssid },
      taxAmount: original_li_addition_dolly.tax_amount.to_s,
      unitPrice: li_dolly_unit_price_changed,
      netAmount: original_li_addition_dolly.net_amount.to_s,
      userCreated: false,
    }
  end

  let(:li_flat_tax_for_sample) do
    {
      id: original_li_tax.to_ssid,
      autoCalculatedQuantity: false,
      calcType: "Flat",
      deletedAt: nil,
      description: original_li_tax.description,
      estimated: false,
      job: {
        id: job.to_ssid,
      },
      originalQuantity: original_li_tax.original_quantity.to_s,
      originalUnitPrice: original_li_tax.original_unit_price.to_s,
      quantity: '0.0',
      rate: { id: original_flat_rate.to_ssid },
      taxAmount: li_flat_tax_tax_amount_changed,
      unitPrice: original_li_tax.unit_price.to_s,
      netAmount: original_li_tax.net_amount.to_s,
      userCreated: false,
    }
  end

  let(:new_rate_type) { original_flat_rate_type }
  let(:new_class_type) { original_flatbad_class_type }

  let(:li_flat_quantity_changed) { original_li_flat.quantity.to_s }
  let(:li_flat_unit_price_changed) { original_li_flat.unit_price.to_s }

  let(:li_dolly_quantity_changed) { original_li_addition_dolly.quantity.to_s }
  let(:li_dolly_unit_price_changed) { original_li_addition_dolly.unit_price.to_s }
  let(:li_dolly_for_sample_deleted_at) { nil }

  let(:li_flat_tax_tax_amount_changed) { original_li_tax.tax_amount.to_s }

  let(:new_notes) { nil }
end
