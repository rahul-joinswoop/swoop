# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdatePartnerVehicle do
  let(:partner_vehicle) do
    create(:rescue_vehicle, company: api_company, driver: driver, lat: nil, lng: nil)
  end

  let(:driver) { api_user }
  let(:partner_vehicle_ssid) { partner_vehicle.to_ssid }

  let(:lat) { 37.7786953 }
  let(:lng) { -122.4112873 }

  let(:variables) do
    {
      input: {
        partnerVehicle: {
          id: partner_vehicle_ssid,
          location: {
            lat: lat,
            lng: lng,
          },
        },
      },
    }
  end

  let(:query) do
    <<~GRAPHQL
    mutation #{operation_name}($input: UpdatePartnerVehicleInput!) {
      updatePartnerVehicle(input: $input) {
        partnerVehicle {
          driver {
            id
            name
            onDuty
          }
          lat
          lng
        }
      }
    }
    GRAPHQL
  end

  shared_examples "updatePartnerVehicle" do
    it "updates attributes accordingly", freeze_time: true do
      # vehicle location updates happen async - we can sort of fake it by forcing
      # the worker call to run inline which makes our tests on the model pass. it
      # doesn't force the response to show our location updates though but i that's ok -
      # our clients aren't depending on the data to come back matching what they sent.
      Sidekiq::Testing.inline! do
        allow(PlacesResolver).to receive(:perform_async)

        expect(UpdateVehicleLocationWorker)
          .to receive(:perform_async)
          .once
          .with(context[:api_company].id, partner_vehicle.id, lat, lng, Time.now.utc.iso8601)
          .and_call_original

        expect(response).not_to include('errors')

        expect { partner_vehicle.reload }
          .to change(partner_vehicle, :lat).from(nil).to(lat)
          .and change(partner_vehicle, :lng).from(nil).to(lng)
          .and change(partner_vehicle, :location_updated_at).from(nil).to(DateTime.current)

        expect { partner_vehicle.driver.reload }.not_to change(partner_vehicle, :driver)

        partner_vehicle_from_response = response.dig('data', 'updatePartnerVehicle', 'partnerVehicle').with_indifferent_access
        expect(partner_vehicle_from_response.dig(:driver, :id)).to eq(driver.to_ssid)
      end
    end
  end

  shared_examples 'it works' do
    it_behaves_like 'updatePartnerVehicle'

    context "and the given partner_vehicle ssid does not exist" do
      let(:errors_array) do
        [
          {
            message: Analyzers::UpdatePartnerVehicleAnalyzer::NOT_FOUND_ERROR_MESSAGE,
            extensions: {
              problems: [{
                explanation: Analyzers::UpdatePartnerVehicleAnalyzer::NOT_FOUND_ERROR_MESSAGE,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "updatePartnerVehicle", "input", "partnerVehicle", "id"],
          },
        ]
      end

      context "because partner_vehicle ssid is from another partner's vehicle" do
        let(:partner_vehicle) { create :rescue_vehicle }

        it_behaves_like "responds with error"
      end

      context "because partner_vehicle ssid simply does not exist" do
        let(:partner_vehicle_ssid) { "1234567" }

        it_behaves_like "responds with error"
      end
    end

    context "but he is is trying to update a partner_vehicle for another user" do
      let(:another_driver) { create(:user, company: api_company, roles: [:rescue, :driver]) }
      let(:partner_vehicle) { super().tap { |v| v.update! driver: another_driver } }
      let(:errors_array) do
        [
          {
            message: Analyzers::UpdatePartnerVehicleAnalyzer::DRIVER_NOT_AUTHORIZED_ERROR_MESSAGE,
            extensions: {
              problems: [{
                explanation: Analyzers::UpdatePartnerVehicleAnalyzer::DRIVER_NOT_AUTHORIZED_ERROR_MESSAGE,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "updatePartnerVehicle", "input", "partnerVehicle", "id"],
          },
        ]
      end

      it_behaves_like "responds with error"
    end

    context "and partner_vehicle is already assigned to this driver" do
      let(:original_assigned_driver) { driver }

      context "and no driver node is passed in the mutation" do
        it_behaves_like "updatePartnerVehicle"
      end

      context "with the deprecated input structure" do
        let(:variables) do
          {
            input: {
              partnerVehicle: {
                id: partner_vehicle_ssid,
                lat: lat,
                lng: lng,
              },
            },
          }
        end

        it_behaves_like "updatePartnerVehicle"
      end
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    it_behaves_like 'it works'
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    it_behaves_like 'it works'
  end
end
