# frozen_string_literal: true

require 'rails_helper'

describe Mutations::EditJobStatus do
  let(:query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: EditJobStatusInput!) {
        editJobStatus(input: $input) {
          job {
            id
            status
            statusReasons {
              edges {
                node {
                  id
                  status
                  reason
                }
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let(:job_status) { response.dig('data', 'editJobStatus', 'job', 'status') }

  let(:variables) do
    {
      input: {
        job: {
          id: job.to_ssid,
          status: 'GOA',
        },
      },
    }
  end

  let!(:status_reason_type) { create(:job_status_reason_type, :goa_customer_cancel_after_deadline) }
  let(:statusReason) { nil }
  let(:agero) do
    c = create :fleet_managed_company, :agero, use_custom_job_status_reasons: true
    CompanyJobStatusReasonType.init_by_client_id(:AGO)
    c
  end
  let!(:job) { create(:fleet_motor_club_job, status: :pending, fleet_company: fleet_managed_company, rescue_company: rescue_company) }

  shared_examples "changes job status" do
    it "changes job.status" do
      if job.is_a?(FleetMotorClubJob)
        expect_any_instance_of(FleetMotorClubJob).to receive(:update_digital_dispatcher_status).once
      end
      expect { response && job.reload }
        .to change(job, :status)
        .from('pending')
        .to('goa')
      expect(job_status).to eq("GOA")
    end
  end

  shared_examples 'does not change job status' do
    it 'does not change job status' do
      expect { response && job.reload }
        .not_to change(job, :status)
        .from('pending')
    end
  end

  shared_examples "changes job_status_reason_types" do
    it 'updates job.job_status_reasons accordingly' do
      expect { response && job.reload }
        .to change(job, :job_status_reason_types)
        .to([status_reason_type])
    end
  end

  shared_examples "does not change job_status_reason_types" do
    it 'updates job.job_status_reasons accordingly' do
      expect { response && job.reload }
        .not_to change { job.job_status_reason_types.count }
    end
  end

  shared_examples 'it works' do
    let(:variables) { super().tap { |v| v[:input][:job][:reason] = statusReason } }
    let(:show_errors) { false }

    context "with an agero job" do
      let(:fleet_managed_company) { agero }

      context "with a reason" do
        let(:statusReason) { status_reason_type.text }

        it_behaves_like "does not raise error"
        it_behaves_like "changes job status"
        it_behaves_like "changes job_status_reason_types"
      end

      context "without a reason" do
        let(:errors_array) do
          msg = Analyzers::Validations::JobStatusReasonType::REASON_REQUIRED_ERROR_MESSAGE % ['GOA']
          [{
            message: msg,
            extensions: {
              problems: [{
                explanation: msg,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "editJobStatus", "input", "job", "reason"],
          }]
        end

        it_behaves_like "responds with error"
        it_behaves_like "does not change job status"
        it_behaves_like "does not change job_status_reason_types"
      end
    end

    context "with a non-agero job" do
      context "with a reason" do
        let(:statusReason) { status_reason_type.text }
        let(:errors_array) do
          msg = Analyzers::Validations::JobStatusReasonType::INVALID_STATUS_REASON_FOR_JOB_MESSAGE
          [{
            message: msg,
            extensions: {
              problems: [{
                explanation: msg,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "editJobStatus", "input", "job", "reason"],
          }]
        end

        it_behaves_like "responds with error"
        it_behaves_like "does not change job status"
        it_behaves_like "does not change job_status_reason_types"
      end

      context "without a reason" do
        it_behaves_like "does not raise error"
        it_behaves_like "changes job status"
        it_behaves_like "does not change job_status_reason_types"
      end

      context "with a fleet managed job" do
        let!(:job) { create(:fleet_managed_job, status: :pending, fleet_company: fleet_managed_company, rescue_company: rescue_company) }

        let(:errors_array) do
          msg = Analyzers::Validations::JobStatusGoa::PARTNER_CANNOT_GOA_FLEET_MANAGED_JOB_MESSAGE
          [{
            extensions: {
              problems: [{
                explanation: msg,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            message: msg,
            path: ["mutation #{operation_name}", "editJobStatus", "input", "job", "status"],
          }]
        end

        it_behaves_like "responds with error"
        it_behaves_like "does not change job status"
        it_behaves_like "does not change job_status_reason_types"
      end
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    it_behaves_like "it works"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    it_behaves_like "it works"
  end
end
