# frozen_string_literal: true

require 'rails_helper'

# NOTE: Accepted, Rejected, Canceled and GOA have such specific cases
# that they are specd separately in these files:
# edit_job_status_accepted_spec, edit_job_status_rejected_spec and edit_job_status_canceled_and_goa_spec

describe Mutations::EditJobStatus do
  let(:encoded_job_id) { job.to_ssid }

  shared_examples 'values from request headers' do
    subject(:mutation) do
      Mutations::EditJobStatus.new(object: {}, context: context)
    end

    let(:context) do
      super().merge!(
        request_id: '123',
        headers: headers,
      )
    end

    shared_examples 'grabbing expected value from headers' do |attribute, expected_value|
      subject(:user_agent) { mutation.send attribute }

      it { is_expected.to eq expected_value }
    end

    context 'when HTTP_USER_AGENT is passed as a request header' do
      context 'and client is iOS' do
        let(:headers) do
          {
            'HTTP_USER_AGENT' => 'ios SwoopMobile/9 CFNetwork/808.3 Darwin/16.3.0',
            'HTTP_REFERER' => 'http://example.com',
          }
        end

        it_behaves_like 'grabbing expected value from headers', :version, 'SwoopMobile/9'
        it_behaves_like 'grabbing expected value from headers', :client_or_swoop_version, 'SwoopMobile/9'
        it_behaves_like 'grabbing expected value from headers', :swoop_custom_platform, 'React iOS'
        it_behaves_like 'grabbing expected value from headers', :referer, 'http://example.com'
      end

      context 'and client is Android' do
        let(:headers) do
          {
            'HTTP_USER_AGENT' => 'android Swoop/2.0.63',
            'HTTP_REFERER' => 'http://example.com',
          }
        end

        it_behaves_like 'grabbing expected value from headers', :version, '2.0.63'
        it_behaves_like 'grabbing expected value from headers', :client_or_swoop_version, '2.0.63'
        it_behaves_like 'grabbing expected value from headers', :swoop_custom_platform, 'React Android'
        it_behaves_like 'grabbing expected value from headers', :referer, 'http://example.com'
      end

      context 'and client is app specific, but from mac platform (e.g GraphiQL)' do
        let(:headers) do
          {
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
            'HTTP_REFERER' => 'http://example.com',
          }
        end

        it_behaves_like 'grabbing expected value from headers', :version, '69'
        it_behaves_like 'grabbing expected value from headers', :client_or_swoop_version, Swoop::VERSION
        it_behaves_like 'grabbing expected value from headers', :swoop_custom_platform, 'API'
        it_behaves_like 'grabbing expected value from headers', :referer, 'http://example.com'
      end

      context 'and client is app specific, but from windows platform (e.g GraphiQL)' do
        let(:headers) do
          {
            'HTTP_USER_AGENT' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
            'HTTP_REFERER' => 'http://example.com',
          }
        end

        it_behaves_like 'grabbing expected value from headers', :version, '69'
        it_behaves_like 'grabbing expected value from headers', :client_or_swoop_version, Swoop::VERSION
        it_behaves_like 'grabbing expected value from headers', :swoop_custom_platform, 'API'
        it_behaves_like 'grabbing expected value from headers', :referer, 'http://example.com'
      end
    end

    context 'when HTTP_USER_AGENT is NOT passed as a request header' do
      let(:headers) do
        {}
      end

      it_behaves_like 'grabbing expected value from headers', :version, 'API - version not provided by client'
      it_behaves_like 'grabbing expected value from headers', :platform, 'API - platform not provided by client'
      it_behaves_like 'grabbing expected value from headers', :client_or_swoop_version, 'API - version not provided by client'
      it_behaves_like 'grabbing expected value from headers', :swoop_custom_platform, 'API - platform not provided by client'
      it_behaves_like 'grabbing expected value from headers', :referer, 'API - referer not provided by client'
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "values from request headers"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "values from request headers"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "values from request headers"
  end
end
