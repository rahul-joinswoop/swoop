# frozen_string_literal: true

require 'rails_helper'

describe Mutations::DeleteSubscription, freeze_time: true do
  shared_examples 'deleteSubscription' do
    let!(:subscription) { create :graphql_subscription, context: context, viewer: viewer }

    let(:variables) do
      {
        input: {
          subscription: {
            id: subscription.to_ssid,
          },
        },
      }
    end

    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: DeleteSubscriptionInput!) {
        deleteSubscription(input: $input) {
          subscription {
            id
          }
        }
      }
      GRAPHQL
    end

    it "works" do
      expect { response }.to avoid_raising_error
        .and change(GraphQLSubscription, :count).from(1).to(0)
      expect(response).not_to include('errors')
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    it_behaves_like "deleteSubscription"
  end

  context '3rd party partner api_company' do
    include_context "3rd party partner api_company"
    it_behaves_like "deleteSubscription"
  end
end
