# frozen_string_literal: true

require 'rails_helper'

describe Mutations::RequestCallback, freeze_time: true do
  shared_examples 'requestCallback' do
    let(:fleet_company) { create(:fleet_company, :ago) }
    let(:dispatchPhone) { nil }
    let(:secondaryPhone) { nil }

    let(:variables) do
      {
        input: {
          job: {
            id: job.to_ssid,
          },
          dispatchPhone: dispatchPhone,
          secondaryPhone: secondaryPhone,
        },
      }
    end

    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: RequestCallbackInput!) {
        requestCallback(input: $input) {
          job {
            id
          }
        }
      }
      GRAPHQL
    end

    let!(:account) do
      create(:account, company: api_company, client_company: fleet_company)
    end

    let(:job) do
      create :fleet_motor_club_job, {
        status: :assigned,
        fleet_company: fleet_company,
        adjusted_created_at: Time.now,
        rescue_company: api_company,
        issc_dispatch_request: issc_dispatch_request,
        answer_by: Time.now + 90,
      }
    end

    let(:issc_dispatch_request) do
      create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end

    shared_examples "requests callback" do
      it_behaves_like "does not raise error"
      it 'works' do
        expect { response && job.reload }
          .to change(job, :partner_dispatcher)
          .from(nil).to(context[:api_user])
          .and change(job, :answer_by)
          .from(Time).to(nil)
          .and change(job, :status)
          .from(Job::STATUS_ASSIGNED).to(Job::STATUS_PENDING)
      end
    end

    context 'ISSC job' do
      let(:issc) { create(:ago_issc, company: api_company, api_access_token: nil, system: Issc::ISSC_SYSTEM) }

      it_behaves_like "requests callback"

      it 'schedules a IsscDispatchResponseCallback job' do
        expect(IsscDispatchResponseCallback)
          .to receive(:perform_async)
          .once
          .with(job.id, :Automated)
        response
      end
    end

    context 'RSC job' do
      let(:api_access_token) { create(:api_access_token) }
      let(:issc) do
        create(
          :ago_issc,
          company: api_company,
          api_access_token: api_access_token,
          system: Issc::RSC_SYSTEM
        )
      end

      shared_examples "schedules an Agero::Rsc::RequestCallBackWorker job" do
        it 'works' do
          expect(Agero::Rsc::RequestCallBackWorker)
            .to receive(:perform_async)
            .once
            .with(job.id, { dispatch_number: dispatchPhone || job.site_or_rescue_company_phone, secondary_number: secondaryPhone })

          response
        end
      end

      it_behaves_like "requests callback"
      it_behaves_like "schedules an Agero::Rsc::RequestCallBackWorker job"

      context 'with a dispatchPhone' do
        let(:dispatchPhone) { Faker::PhoneNumber.phone_number_e164 }

        it_behaves_like "requests callback"
        it_behaves_like "schedules an Agero::Rsc::RequestCallBackWorker job"

        context 'and a secondaryPhone' do
          let(:secondaryPhone) { Faker::PhoneNumber.phone_number_e164 }

          it_behaves_like "requests callback"
          it_behaves_like "schedules an Agero::Rsc::RequestCallBackWorker job"
        end
      end

      context 'with a secondaryPhone' do
        let(:secondaryPhone) { Faker::PhoneNumber.phone_number_e164 }

        it_behaves_like "requests callback"
        it_behaves_like "schedules an Agero::Rsc::RequestCallBackWorker job"
      end
    end

    context 'other job' do
      let(:job) { create :fleet_motor_club_job, rescue_company: api_company }
      let(:errors_array) do
        [
          {
            message: Analyzers::RequestCallbackAnalyzer::RSC_ISSC_JOBS_ONLY_ERROR_MESSAGE,
            extensions: {
              problems: [{
                explanation: Analyzers::RequestCallbackAnalyzer::RSC_ISSC_JOBS_ONLY_ERROR_MESSAGE,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "requestCallback", "input", "job", "id"],
          }.deep_stringify_keys,
        ]
      end

      it_behaves_like "responds with error"
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "requestCallback"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "requestCallback"
  end
end
