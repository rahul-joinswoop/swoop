# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdateJobStatus do
  let(:query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: UpdateJobStatusInput!) {
        updateJobStatus(input: $input) {
          job {
            id
            status
            statusReasons {
              edges {
                node {
                  reason
                }
              }
            }
            eta {
              bidded
            }
          }
        }
      }
    GRAPHQL
  end

  let(:variables) do
    {
      input: {
        job: {
          id: job.to_ssid,
          status: 'Accepted',
          reason: statusReason,
          eta: {
            bidded: bidded_eta,
          },
        },
      },
    }
  end

  let(:statusReason) { nil }

  let(:bidded_eta) { nil }
  let(:job_status) { response.dig('data', 'updateJobStatus', 'job', 'status') }

  shared_examples 'set job accepted' do |reason_expected|
    it "changes job status, updates jon.bta#{" and job.eta_explanation" if reason_expected}" do
      expect_any_instance_of(AnalyticsData::JobStatus).to receive(:push_message).once
      expect { response && job.reload }
        .to change(job, :status)
        .from('assigned')
        .to(job.is_a?(FleetMotorClubJob) ? 'submitted' : 'accepted')
        .and change { job&.bta&.time&.iso8601 }
        .to(bidded_eta)

      if reason_expected
        expect(job&.eta_explanation&.text).to eq(status_reason_type.text)
      end

      expect(job_status).to eq(job.is_a?(FleetMotorClubJob) ? 'Submitted' : 'Accepted')
    end
  end

  shared_examples 'does not change job attributes' do
    it 'does not change job.bta or status' do
      expect { response && job.reload }
        .to not_change(job, :bta)
        .and not_change(job, :status).from('assigned')
    end
  end

  shared_examples 'Job Accepted cases' do
    let(:issc_dispatch_request) { create(:issc_dispatch_request, max_eta: 90) }
    shared_examples "it works" do
      context 'with a company that does not support max_eta_without_reason' do
        let(:fleet_managed_company) { create :fleet_company }

        context 'with scheduled_for set' do
          let(:job) { super().tap { |j| j.update! scheduled_for: DateTime.current + 1.hours } }

          context "it doesn't require bta" do
            it_behaves_like 'does not raise error'
            it 'sets job to accepted' do
              if job.is_a?(FleetMotorClubJob)
                expect_any_instance_of(FleetMotorClubJob).to receive(:update_digital_dispatcher_status).once
              end
              expect { response && job.reload }
                .to change(job, :status)
                .from('assigned')
                .to(job.is_a?(FleetMotorClubJob) ? 'submitted' : 'accepted')

              expect(job_status).to eq(job.is_a?(FleetMotorClubJob) ? 'Submitted' : 'Accepted')
            end
          end
        end

        context 'with an existing bta' do
          let(:job) do
            super().tap do |j|
              j.time_of_arrivals << create(:time_of_arrival, time: Time.current + 1.hours, eba_type: TimeOfArrival::BID)
              j
            end
          end

          context "it doesn't require bta" do
            it_behaves_like 'does not raise error'
            it 'sets job to accepted' do
              if job.is_a?(FleetMotorClubJob)
                expect_any_instance_of(FleetMotorClubJob).to receive(:update_digital_dispatcher_status).once
              end
              expect { response && job.reload }
                .to change(job, :status)
                .from('assigned')
                .to(job.is_a?(FleetMotorClubJob) ? 'submitted' : 'accepted')

              expect(job_status).to eq(job.is_a?(FleetMotorClubJob) ? 'Submitted' : 'Accepted')
            end
          end
        end

        context 'with an existing accepted ajs' do
          let(:job) do
            super().tap do |j|
              j.audit_job_statuses << create(:audit_job_status, job_status: create(:job_status, :accepted))
              j
            end
          end

          context "it doesn't require bta" do
            it_behaves_like 'does not raise error'
            it 'sets job to accepted' do
              if job.is_a?(FleetMotorClubJob)
                expect_any_instance_of(FleetMotorClubJob).to receive(:update_digital_dispatcher_status).once
              end
              expect { response && job.reload }
                .to change(job, :status)
                .from('assigned')
                .to(job.is_a?(FleetMotorClubJob) ? 'submitted' : 'accepted')

              expect(job_status).to eq(job.is_a?(FleetMotorClubJob) ? 'Submitted' : 'Accepted')
            end
          end
        end

        context 'with no 1 or status reason' do
          let(:errors_array) do
            msg = Analyzers::Validations::JobStatusAccepted::ACCEPTED_WITHOUT_BTA_ERROR_MSG
            [
              {
                message: msg,
                extensions: {
                  problems: [{
                    explanation: msg,
                  }],
                },
                locations: [{ column: 3, line: 2 }],
                path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "eta", "bidded"],
              },
            ]
          end

          it_behaves_like 'responds with error', 'does not change job attributes'
        end

        context 'with a bidded_eta' do
          let(:bidded_eta) { (job.created_at + 35.minutes).iso8601 }

          it_behaves_like 'does not raise error'
          it_behaves_like 'set job accepted', false

          context 'and a statusReason' do
            let!(:status_reason_type) { create(:job_status_reason_type, :accepted_event_in_area) }
            let(:statusReason) { status_reason_type.text }

            it_behaves_like 'does not raise error'
            it_behaves_like 'set job accepted', true
          end
        end
      end

      context 'with a company that supports max_eta_without_reason' do
        let(:fleet_managed_company) { create :fleet_company, :agero }

        context 'with a valid bidded_eta' do
          let(:bidded_eta) { (job.created_at + 35.minutes).iso8601 }

          it_behaves_like 'does not raise error'

          it_behaves_like 'set job accepted', false
        end

        context 'with an existing bta' do
          let(:job) do
            super().tap do |j|
              j.time_of_arrivals << create(:time_of_arrival, time: Time.current + 1.hours, eba_type: TimeOfArrival::BID)
              j
            end
          end

          context "it doesn't require bta" do
            it_behaves_like 'does not raise error'
            it 'sets job to accepted' do
              expect { response && job.reload }
                .to change(job, :status)
                .from('assigned')
                .to(job.is_a?(FleetMotorClubJob) ? 'submitted' : 'accepted')

              expect(job_status).to eq(job.is_a?(FleetMotorClubJob) ? 'Submitted' : 'Accepted')
            end
          end
        end

        context 'when no bidded_eta is passed' do
          let(:errors_array) do
            msg = Analyzers::Validations::JobStatusAccepted::ACCEPTED_WITHOUT_BTA_ERROR_MSG
            [
              {
                message: msg,
                extensions: {
                  problems: [{
                    explanation: msg,
                  }],
                },
                locations: [{ column: 3, line: 2 }],
                path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "eta", "bidded"],
              },
            ]
          end

          it_behaves_like 'responds with error', 'does not change job attributes'
        end

        context 'when given bidded_eta > job.max_bidded_eta_without_explanation' do
          let(:bidded_eta) { (job.adjusted_created_at + 100.minutes).iso8601 }
          let!(:status_reason_type) { create(:job_status_reason_type, :accepted_event_in_area) }
          let!(:job_eta_explanation) { create(:job_eta_explanation, text: status_reason_type.text) }

          context 'with a statusReason' do
            let(:statusReason) { status_reason_type.text }

            it_behaves_like 'does not raise error'
            it_behaves_like 'set job accepted', true
          end

          context 'when statusReason.job_status is not Accepted' do
            let!(:invalid_status_reason_type) { create(:job_status_reason_type, :cancel_prior_job_delayed) }
            let(:statusReason) { invalid_status_reason_type.text }

            let(:errors_array) do
              msg = Analyzers::Validations::JobStatusReasonType::INVALID_REASON_ERROR_MESSAGE % ['Accepted']
              [
                {
                  message: msg,
                  extensions: {
                    problems: [{
                      explanation: msg,
                    }],
                  },
                  locations: [{ column: 3, line: 2 }],
                  path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
                },
              ]
            end

            it_behaves_like 'responds with error', 'does not change job attributes'
          end

          context 'when job.fleet_company.use_custom_job_status_reasons? is true' do
            let(:fleet_managed_company) { create :fleet_company, :agero, use_custom_job_status_reasons: true }

            let!(:custom_status_reason_type) { create(:job_status_reason_type, job_status: JobStatus.find(JobStatus::ACCEPTED), text: 'Rural Disablemesadfnt', key: :accepted_rural_disablementasdf) }
            let!(:job_reject_reason) { create(:job_reject_reason, text: custom_status_reason_type.text) }

            before do
              fleet_managed_company.custom_job_status_reason_types << custom_status_reason_type
            end

            context 'and a reason not supported by job.fleet_company is given' do
              let(:statusReason) { status_reason_type.text }

              let(:errors_array) do
                msg = Analyzers::Validations::JobStatusReasonType::INVALID_STATUS_REASON_FOR_JOB_MESSAGE
                [
                  {
                    message: msg,
                    extensions: {
                      problems: [{
                        explanation: msg,
                      }],
                    },
                    locations: [{ column: 3, line: 2 }],
                    path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
                  },
                ]
              end

              it_behaves_like 'responds with error', 'does not change job attributes'
            end

            context 'and a reason supported by job.fleet_company is given' do
              let(:statusReason) { custom_status_reason_type.text }

              let!(:status_reason_type) { custom_status_reason_type }

              it_behaves_like 'does not raise error', 'set job accepted'
            end
          end

          context 'without a statusReason' do
            let(:errors_array) do
              msg = Analyzers::Validations::JobStatusAccepted::BTA_WITHOUT_REASON_ERROR_MSG
              [
                {
                  message: msg,
                  extensions: {
                    problems: [{
                      explanation: msg,
                    }],
                  },
                  locations: [{ column: 3, line: 2 }],
                  path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
                },
              ]
            end

            it_behaves_like 'responds with error', 'does not change job attributes'
          end
        end
      end
    end

    context 'with a FleetManaged job' do
      let!(:job) do
        create :fleet_managed_job,
               status: :assigned,
               fleet_company: fleet_managed_company,
               rescue_company: rescue_company,
               issc_dispatch_request: issc_dispatch_request
      end

      it_behaves_like 'it works'
    end

    context 'with a FleetMotorClub job' do
      let!(:job) do
        create :fleet_motor_club_job,
               status: :assigned,
               fleet_company: fleet_managed_company,
               rescue_company: rescue_company,
               issc_dispatch_request: issc_dispatch_request
      end

      it_behaves_like 'it works'
    end
  end

  shared_examples 'not authorized to Accept a job' do
    let(:query) do
      <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobStatusInput!) {
          updateJobStatus(input: $input) {
            job {
              id
              status
            }
          }
        }
      GRAPHQL
    end

    let(:variables) do
      {
        input: {
          job: {
            id: job.to_ssid,
            status: 'Accepted',
          },
        },
      }
    end

    let(:errors_array) do
      msg = Analyzers::Validations::UpdateJobStatus.error_message(api_company, ::Job.statuses[job.status], 'Accepted')
      [
        {
          message: msg,
          extensions: {
            problems: [{ explanation: msg }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "status"],
        },
      ]
    end

    it_behaves_like 'responds with error', 'does not change job attributes'
  end

  context "as a client api_application" do
    include_context "client api_application"
    let!(:job) { create(:fleet_managed_job, status: :assigned, fleet_company: fleet_managed_company) }

    it_behaves_like 'not authorized to Accept a job'
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like 'Job Accepted cases'
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like 'Job Accepted cases'
  end
end
