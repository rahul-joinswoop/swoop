# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdateJobStatus do
  let(:query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: UpdateJobStatusInput!) {
        updateJobStatus(input: $input) {
          job {
            id
            status
          }
        }
      }
    GRAPHQL
  end

  let(:job_status) { response.dig('data', 'updateJobStatus', 'job', 'status') }

  let(:variables) do
    {
      input: {
        job: {
          id: job.to_ssid,
          status: 'OnSite',
        },
      },
    }
  end

  let!(:job) { create(:fleet_managed_job, status: :enroute, fleet_company: fleet_managed_company, rescue_company: rescue_company) }

  shared_examples "authorized to OnSite a job" do
    it_behaves_like 'does not raise error'
    it 'updates status' do
      expect { response && job.reload }.to change(job, :status).to('onsite')
      expect(job_status).to eq('OnSite')
    end
  end

  shared_examples 'not authorized to OnSite a job' do
    let(:errors_array) do
      [
        {
          locations: [{ column: 3, line: 2 }],
          message: Analyzers::Validations::UpdateJobStatus.error_message(api_company, job.human_status, 'On Site'),
          path: ["updateJobStatus"],
        },
      ]
    end
    let(:show_errors) { false }
    it_behaves_like 'responds with error'
    it 'does not change job status' do
      expect { response && job.reload }.not_to change(job, :status)
    end
  end

  context 'as a partner company' do
    shared_examples 'it works' do
      it_behaves_like 'authorized to OnSite a job'
      context 'with an invalid transition' do
        let(:job) { super().tap { |j| j.assigned! } }

        it_behaves_like 'not authorized to OnSite a job'
      end
    end

    context 'with a partner api_user' do
      include_context "partner api_user"
      it_behaves_like "it works"
    end

    context '3rd party partner api_user' do
      include_context "3rd party partner api_user"
      it_behaves_like "it works"
    end
  end

  # client companies can't do this unless #fleet_manual? is true - i don't know how to do this and i
  # don't really care for the purpose of this spec, it's mostly testing the error messages anyway. as
  # long as we test a single positive case we should be ok.
  context 'as a client api_application' do
    include_context "client api_application"

    let!(:job) { create(:fleet_managed_job, status: :enroute, fleet_company: api_company) }

    it_behaves_like 'not authorized to OnSite a job'
  end
end
