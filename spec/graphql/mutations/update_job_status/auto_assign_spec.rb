# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdateJobStatus, freeze_time: true do
  shared_examples 'auto_assign' do
    let(:query) do
      <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobStatusInput!) {
          updateJobStatus(input: $input) {
            job {
              id
              status
              auction {
                bids {
                  edges {
                    node {
                      eta
                      status
                      createdAt
                      updatedAt
                    }
                  }
                }
              }
            }
          }
        }
      GRAPHQL
    end

    let(:variables) do
      {
        input: {
          job: {
            id: job.to_ssid,
            status: status,
            reason: statusReason,
            eta: {
              bidded: bidded_eta,
            },
          },
        },
      }
    end

    let(:statusReason) { nil }

    let(:job) { create(:fleet_managed_job, status: :auto_assigning) }
    let(:auction) { create(:auction, job: job, max_target_eta: 60) }
    let!(:job_candidate) { create(:job_candidate, job: job, company: api_company) }
    let(:bidded_eta) { nil }

    let!(:bid) do
      create :bid,
             status: Auction::Bid::REQUESTED,
             eta_mins: nil,
             eta_dttm: nil,
             bid_submission_dttm: nil,
             job: job,
             company: api_company,
             auction: auction
    end

    let(:job_status) { response.dig('data', 'updateJobStatus', 'job', 'status') }

    describe 'creating bid' do
      let(:status) { 'Accepted' }

      context "with a bidded eta" do
        let(:bidded_eta) { (Time.current + 30.minutes).iso8601 }

        it 'works' do
          expect_any_instance_of(AnalyticsData::JobStatus).to receive(:push_message).once

          expect(Auction::Auction)
            .to receive(:slack_notification)
            .with(job, instance_of(String))

          expect { response && job.reload }
            .not_to change { job }

          expect { bid.reload }
            .to change(bid, :status)
            .from(Auction::Bid::REQUESTED)
            .to(Auction::Bid::SUBMITTED)
            .and change(bid, :eta_mins)
            .from(nil)
            .to(30)
            .and change(bid, :eta_dttm)
            .from(nil)
            .to(Time.current + 30.minutes)
            .and change(bid, :bid_submission_dttm)
            .from(nil)
            .to(Time.current)

          expect(job_status).to eq('Submitted')
        end
      end

      context 'without a bidded eta' do
        let(:errors_array) do
          msg = Analyzers::Validations::JobStatusAccepted::ACCEPTED_WITHOUT_BTA_ERROR_MSG
          [
            {
              message: msg,
              extensions: {
                problems: [{
                  explanation: msg,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "eta", "bidded"],
            },
          ]
        end

        it_behaves_like "responds with error"
      end
    end

    describe 'rejecting a bid' do
      let(:status) { 'Rejected' }
      let!(:status_reason_type) { create(:job_status_reason_type, :rejected_no_drivers_available) }
      let!(:job_reject_reason) { create(:job_reject_reason, text: status_reason_type.text) }

      before do
        job.audit_job_statuses.create!(job_status: JobStatus.find(JobStatus::CREATED), last_set_dttm: job.created_at, company: api_company)
      end

      context 'with a statusReason' do
        let(:statusReason) { status_reason_type.text }

        it 'works' do
          expect_any_instance_of(AnalyticsData::JobStatus).to receive(:push_message).once

          expect(Auction::Auction)
            .to receive(:slack_notification)
            .with(job, instance_of(String))

          expect { response && job.reload }
            .not_to change { job }

          expect { bid.reload }
            .to change(bid, :status)
            .from(Auction::Bid::REQUESTED)
            .to(Auction::Bid::PROVIDER_REJECTED)
            .and change(bid, :job_reject_reason_id)
            .from(nil)
            .to(job_reject_reason.id)

          expect(job_status).to eq('Rejected')
        end

        context "and a notes.rejected" do
          let(:reject) { Faker::Hipster.sentence }
          let(:variables) do
            super().tap { |i| i[:input][:job][:notes] = { rejected: reject } }
          end

          it 'works' do
            expect_any_instance_of(AnalyticsData::JobStatus).to receive(:push_message).once

            expect(Auction::Auction)
              .to receive(:slack_notification)
              .with(job, instance_of(String))

            expect { response && job.reload }
              .not_to change { job }

            expect { bid.reload }
              .to change(bid, :status)
              .from(Auction::Bid::REQUESTED)
              .to(Auction::Bid::PROVIDER_REJECTED)
              .and change(bid, :job_reject_reason_id)
              .from(nil)
              .to(job_reject_reason.id)
              .and change(bid, :job_reject_reason_info)
              .from(nil)
              .to(reject)

            expect(job_status).to eq('Rejected')
          end
        end
      end

      context 'without a statusReason' do
        let(:errors_array) do
          msg = Analyzers::Validations::JobStatusReasonType::REASON_REQUIRED_ERROR_MESSAGE % ['Rejected']
          [
            {
              message: msg,
              extensions: {
                problems: [{
                  explanation: msg,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
            },
          ]
        end

        it_behaves_like "responds with error"
      end
    end

    describe 'doing anything else' do
      let(:status) { 'Towing' }
      let(:errors_array) do
        msg = Analyzers::Validations::UpdateJobStatus.error_message(api_company, ::Job.statuses[job.status], status)
        [
          {
            message: msg,
            extensions: {
              problems: [{
                explanation: msg,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "status"],
          },
        ]
      end

      it_behaves_like "responds with error"
    end
  end

  context 'with a partner api_user' do
    include_context "partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "auto_assign"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "auto_assign"
  end
end
