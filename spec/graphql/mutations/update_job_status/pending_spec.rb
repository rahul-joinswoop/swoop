# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdateJobStatus do
  let(:query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: UpdateJobStatusInput!) {
        updateJobStatus(input: $input) {
          job {
            id
            status
          }
        }
      }
    GRAPHQL
  end

  let(:job_status) { response.dig('data', 'updateJobStatus', 'job', 'status') }
  let(:status) { 'Pending' }
  let(:variables) do
    {
      input: {
        job: {
          id: job.to_ssid,
          status: status,
        },
      },
    }
  end

  shared_examples "changes job status" do
    it "changes job.status" do
      expect { response && job.reload }
        .to change(job, :status)
        .from('draft')
        .to(status.downcase)

      expect(job_status).to eq(status)
    end
  end

  shared_examples 'does not change job status' do
    it 'does not change job status' do
      expect { response && job.reload }
        .not_to change(job, :status)
        .from('draft')
    end
  end

  context 'as a Partner company' do
    shared_examples "it works" do
      context 'with missing service code' do
        let!(:job) do
          create :rescue_job,
                 :with_service_location,
                 status: :draft,
                 fleet_company: fleet_managed_company,
                 rescue_company: api_company,
                 service_code: nil
        end

        let(:errors_array) do
          msg = Analyzers::Validations::ServiceName::ERROR_MESSAGE
          [{
            message: msg,
            extensions: {
              problems: [{
                explanation: msg,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "updateJobStatus", "job", "service"],
          }]
        end

        it_behaves_like "responds with error", "does not change job status"
      end

      context 'with missing service location' do
        let!(:job) do
          create :rescue_job,
                 status: :draft,
                 fleet_company: fleet_managed_company,
                 rescue_company: api_company
        end

        let(:errors_array) do
          msg = Analyzers::Validations::ServiceLocation::ERROR_MESSAGE
          [{
            message: msg,
            extensions: {
              problems: [{
                explanation: msg,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "updateJobStatus", "job", "location"],
          }]
        end

        it_behaves_like "responds with error", "does not change job status"
      end

      context 'with dropLocation and a serviceCode that requires it' do
        let(:service_code) { create :service_code, name: ServiceCode::RESCUE_DROP_LOCATION_SERVICE_CODES.sample }
        let!(:job) do
          create :rescue_job,
                 :with_service_location,
                 status: :draft,
                 fleet_company: fleet_managed_company,
                 rescue_company: api_company,
                 service_code: service_code
        end

        let(:errors_array) do
          [{
            message: Analyzers::Validations::DropLocation::ERROR_MESSAGE % service_code.name.inspect,
            extensions: {
              problems: [{
                explanation: Analyzers::Validations::DropLocation::ERROR_EXPLANATION % service_code.name.inspect,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "updateJobStatus", "job", "location"],
          }]
        end

        it_behaves_like "responds with error", "does not change job status"
      end
    end

    context 'with a partner api_user' do
      include_context "partner api_user"
      it_behaves_like "it works"
    end

    context '3rd party partner api_user' do
      include_context "3rd party partner api_user"
      it_behaves_like "it works"
    end
  end

  context 'as a Client company' do
    include_context "client api_application"

    context 'with missing serviceLocation.locationType' do
      let(:service_location) { create :location, :random, location_type: nil }
      let!(:job) do
        create :fleet_managed_job,
               :with_drop_location,
               status: :draft,
               fleet_company: api_company,
               service_location: service_location
      end

      let(:errors_array) do
        msg = Analyzers::Validations::ServiceLocationType::ERROR_MESSAGE
        [{
          message: msg,
          extensions: {
            problems: [{
              explanation: msg,
            }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "updateJobStatus", "job", "location", "serviceLocation"],
        }]
      end

      it_behaves_like "responds with error", "does not change job status"
    end

    context 'with customer fields missing' do
      let!(:job) do
        create :fleet_managed_job,
               :with_drop_location,
               :with_service_location,
               status: :draft,
               fleet_company: api_company,
               customer: customer
      end

      let(:errors_array) do
        [{
          message: msg,
          extensions: {
            problems: [{
              explanation: msg,
            }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "updateJobStatus", "job", "customer"],
        }]
      end

      context 'name' do
        let(:customer) { create :user, :phone, first_name: nil, last_name: nil }
        let(:msg) { Analyzers::Validations::CustomerInfo::CUSTOMER_NAME_ERROR_MESSAGE }

        it_behaves_like "responds with error", "does not change job status"
      end

      context 'phone' do
        let(:customer) { create :user }
        let(:msg) { Analyzers::Validations::CustomerInfo::CUSTOMER_PHONE_ERROR_MESSAGE }

        it_behaves_like "responds with error", "does not change job status"
      end
    end

    context 'with missing service.symptom' do
      let(:api_company) do
        c = super()
        c.features << create(:feature, name: Feature::SHOW_SYMPTOMS)
        c
      end

      let!(:job) do
        create :fleet_managed_job,
               :with_drop_location,
               :with_service_location,
               status: :draft,
               fleet_company: api_company,
               symptom: nil
      end

      let(:errors_array) do
        msg = Analyzers::Validations::ServiceSymptom::ERROR_MESSAGE
        [{
          message: msg,
          extensions: {
            problems: [{
              explanation: msg,
            }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "updateJobStatus", "job", "service"],
        }]
      end

      it_behaves_like "responds with error", "does not change job status"
    end

    context 'with texts.sendTextsToPickup and missing pickupContact.phone' do
      let(:pickup_contact) { create :user }
      let!(:job) do
        create :fleet_managed_job,
               :with_drop_location,
               :with_service_location,
               status: :draft,
               fleet_company: api_company,
               send_sms_to_pickup: true,
               pickup_contact: pickup_contact
      end

      let(:errors_array) do
        msg = Analyzers::Validations::Texts::ERROR_MESSAGE
        [{
          message: msg,
          extensions: {
            problems: [{
              explanation: msg,
            }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "updateJobStatus", "job", "pickupContact"],
        }]
      end

      it_behaves_like "responds with error", "does not change job status"
    end

    context 'with vehicle fields missing' do
      let!(:job) do
        create :fleet_managed_job,
               :with_drop_location,
               :with_service_location,
               status: :draft,
               fleet_company: api_company,
               driver: create(:drive, vehicle: vehicle)
      end

      let(:errors_array) do
        msg = Analyzers::Validations::Vehicle::ERROR_MESSAGE % [field.inspect]
        [{
          message: msg,
          extensions: {
            problems: [{
              explanation: msg,
            }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "updateJobStatus", "job", "vehicle"],
        }]
      end

      context 'make' do
        let(:vehicle) { create :stranded_vehicle, make: nil }
        let(:field) { 'make' }

        it_behaves_like "responds with error", "does not change job status"
      end

      context 'model' do
        let(:vehicle) { create :stranded_vehicle, model: nil }
        let(:field) { 'model' }

        it_behaves_like "responds with error", "does not change job status"
      end

      context 'color' do
        let(:vehicle) { create :stranded_vehicle, color: nil }
        let(:field) { 'color' }

        it_behaves_like "responds with error", "does not change job status"
      end
    end

    context 'with correct data' do
      let(:api_company) do
        super().tap do |c|
          c.features << create(:feature, name: Feature::QUESTIONS)
          c.features << create(:feature, name: Feature::SHOW_SYMPTOMS)
        end
      end

      let!(:job) do
        j = create :fleet_managed_job,
                   :with_drop_location,
                   :with_service_location,
                   status: :draft,
                   fleet_company: api_company,
                   send_sms_to_pickup: true

        api_company.questions_for_service_code(j.service_code).each do |q|
          j.question_results << create(:question_result, question: q)
        end
        j
      end

      it_behaves_like "changes job status"

      context 'when client company has "Client API Coverage Override" feature' do
        before do
          api_company.features << create(:feature, name: Feature::CLIENT_API_COVERAGE_OVERRIDE)
        end

        it_behaves_like "changes job status"

        it 'does not create a coverage to the job' do
          response && job.reload

          expect(job.pcc_coverage_id).to be_nil
        end

        context 'and Request.current.source_application.client_api? is true' do
          let(:wrap_in_request_context) { true }

          it_behaves_like "changes job status"

          it 'creates a Covered PCC::Coverage to the job' do
            expect(job.pcc_coverage_id).to eq nil
            expect { response && job.reload }.to change(job, :pcc_coverage_id).from(nil)

            pcc_coverage_data = job.pcc_coverage.data[0]

            expect(pcc_coverage_data['result']).to eq ClientProgram::COVERED
            expect(pcc_coverage_data['name']).to eq ClientProgram::CLIENT_API_COVERED
            expect(pcc_coverage_data['code']).to eq ClientProgram::CLIENT_API_COVERED_CODE
            expect(pcc_coverage_data['coverage_notes']).to eq ClientProgram::CLIENT_API_COVERED_NOTES
          end

          context 'and job.pcc_coverage is present' do
            before do
              coverage = create(:pcc_coverage, :lincoln)
              job.update! pcc_coverage_id: coverage.id
            end

            it 'does not change the job.pcc_coverage_id' do
              expect { response && job.reload }.not_to change(job, :pcc_coverage_id)
            end
          end
        end
      end
    end

    context 'questions' do
      let(:api_company) do
        super().tap do |c|
          c.features << create(:feature, name: Feature::QUESTIONS)
        end
      end

      let!(:job) do
        create :fleet_managed_job,
               :with_drop_location,
               :with_service_location,
               status: :draft,
               fleet_company: api_company
      end

      let(:errors_array) do
        msg = Analyzers::Validations::ServiceQuestions::ERROR_MESSAGE % ['Questions', job.service_code.name.inspect]
        problems = api_company
          .questions_for_service_code(job.service_code)
          .map { |q| { explanation: Analyzers::Validations::ServiceQuestions::ERROR_EXPLANATION % [q.question.inspect] } }
        [{
          message: msg,
          extensions: {
            problems: problems,
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "updateJobStatus", "job", "service", "answers"],
        }]
      end

      it_behaves_like "responds with error", "does not change job status"
    end
  end
end
