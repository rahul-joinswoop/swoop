# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdateJobStatus do
  let(:query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: UpdateJobStatusInput!) {
        updateJobStatus(input: $input) {
          job {
            id
            status
            statusReasons {
              edges {
                node {
                  id
                  status
                  reason
                }
              }
            }
            #{"eta { bidded }" if api_company.rescue?}
          }
        }
      }
    GRAPHQL
  end

  let(:job_status) { response.dig('data', 'updateJobStatus', 'job', 'status') }

  let(:variables) do
    {
      input: {
        job: {
          id: job.to_ssid,
          status: 'Rejected',
          reason: statusReason,
          notes: {
            rejected: notes,
          },
        },
      },
    }
  end

  let(:statusReason) { nil }
  let(:notes) { nil }

  shared_examples 'set job rejected' do
    it 'changes job.status to rejected' do
      expect_any_instance_of(AnalyticsData::JobStatus).to receive(:push_message).once
      if job.is_a?(FleetMotorClubJob)
        expect_any_instance_of(FleetMotorClubJob).to receive(:update_digital_dispatcher_status).once
      end
      expect { response && job.reload }.to change(job, :status).from('assigned').to('rejected')
      expect(job_status).to eq("Rejected")
    end

    it 'updates job.reject_reason accordingly' do
      expect { response && job.reload }
        .to change { job&.reject_reason&.text }
        .from(nil)
        .to(status_reason_type.text)
    end

    it 'updates job.reject_reason_info accordingly' do
      if notes.present?
        expect { response && job.reload }
          .to change(job, :reject_reason_info)
          .from(nil)
          .to(notes)
      end
    end
  end

  shared_examples 'does not change job attributes' do
    it 'does not change job status' do
      expect { response && job.reload }.not_to change(job, :status).from('assigned')
    end

    it 'does not change job.reject_reason' do
      expect { response && job.reload }.not_to change(job, :reject_reason)
    end
  end

  shared_examples 'Job Rejected cases' do
    context 'with a FleetManaged job' do
      let!(:job) do
        create :fleet_managed_job, status: :assigned, fleet_company: fleet_managed_company, rescue_company: rescue_company
      end

      let!(:status_reason_type) { create(:job_status_reason_type, :rejected_no_drivers_available) }
      let!(:job_reject_reason) { create(:job_reject_reason, text: status_reason_type.text) }

      before do
        job.audit_job_statuses.create! job_status: JobStatus.find(JobStatus::CREATED),
                                       last_set_dttm: job.created_at,
                                       company: rescue_company
      end

      context 'with a statusReason' do
        let(:statusReason) { status_reason_type.text }

        it_behaves_like 'does not raise error'
        it_behaves_like 'set job rejected'

        context 'with job.notes' do
          let(:notes) { 'This is Reject notes' }

          it_behaves_like 'does not raise error'
          it_behaves_like 'set job rejected'
        end
      end

      context 'with a statusReason of other' do
        let!(:status_reason_type) { create(:job_status_reason_type, :rejected_other) }
        let(:statusReason) { status_reason_type.text }
        let(:errors_array) do
          msg = Analyzers::Validations::JobStatusRejected::NOTES_REQUIRED_FOR_REJECTED_OTHER_MESSAGE
          [
            {
              message: msg,
              extensions: {
                problems: [{
                  explanation: msg,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "notes", "rejected"],
            },
          ]
        end

        it_behaves_like 'responds with error', 'does not change job attributes'

        context 'with job.notes' do
          let(:notes) { 'This is Reject notes' }

          it_behaves_like 'does not raise error', 'set job rejected'
        end
      end

      context 'when statusReason.job_status is not Rejected' do
        let(:statusReason) { create(:job_status_reason_type, :cancel_another_job_priority).text }
        let(:errors_array) do
          msg = Analyzers::Validations::JobStatusReasonType::INVALID_REASON_ERROR_MESSAGE % ['Rejected']
          [
            {
              message: msg,
              extensions: {
                problems: [{
                  explanation: msg,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
            },
          ]
        end

        it_behaves_like 'responds with error', 'does not change job attributes'
      end

      context 'when job.fleet_company.use_custom_job_status_reasons? is true' do
        let(:fleet_managed_company) { create :fleet_managed_company, use_custom_job_status_reasons: true }

        let!(:custom_status_reason_type) do
          create :job_status_reason_type,
                 job_status: JobStatus.find(JobStatus::REJECTED),
                 text: 'Not Interested asdf',
                 key: :rejected_not_interested_asdf
        end
        let!(:job_reject_reason) { create(:job_reject_reason, text: custom_status_reason_type.text) }

        before do
          fleet_managed_company.custom_job_status_reason_types << custom_status_reason_type
        end

        context 'and a reason not supported by job.fleet_company is given' do
          let(:statusReason) { status_reason_type.text }
          let(:errors_array) do
            msg = Analyzers::Validations::JobStatusReasonType::INVALID_STATUS_REASON_FOR_JOB_MESSAGE
            [
              {
                message: msg,
                extensions: {
                  problems: [{
                    explanation: msg,
                  }],
                },
                locations: [{ column: 3, line: 2 }],
                path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
              },
            ]
          end

          it_behaves_like 'responds with error', 'does not change job attributes'
        end

        context 'and a reason supported by job.fleet_company is given' do
          let(:statusReason) { custom_status_reason_type.text }
          let!(:status_reason_type) { custom_status_reason_type }

          it_behaves_like 'does not raise error'
          it_behaves_like 'set job rejected'
        end
      end

      context 'without a statusReason' do
        let(:errors_array) do
          msg = Analyzers::Validations::JobStatusReasonType::REASON_REQUIRED_ERROR_MESSAGE % ['Rejected']
          [
            {
              message: msg,
              extensions: {
                problems: [{
                  explanation: msg,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
            },
          ]
        end

        it_behaves_like 'responds with error', 'does not change job attributes'
      end
    end
  end

  shared_examples 'not authorized to Reject a job' do
    let(:query) do
      <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobStatusInput!) {
          updateJobStatus(input: $input) {
            job {
              id
              status
            }
          }
        }
      GRAPHQL
    end

    let(:variables) do
      {
        input: {
          job: {
            id: job.to_ssid,
            status: 'Rejected',
          },
        },
      }
    end
    let(:errors_array) do
      msg = Analyzers::Validations::UpdateJobStatus.error_message(api_company, ::Job.statuses[job.status], 'Rejected')
      [
        {
          message: msg,
          extensions: {
            problems: [{
              explanation: msg,
            }],
          },
          locations: [{ column: 3, line: 2 }],
          path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "status"],
        },
      ]
    end
    it_behaves_like 'responds with error', 'does not change job attributes'
  end

  context 'when api_company is a Partner company' do
    context 'with a partner api_user' do
      include_context "partner api_user"
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "Job Rejected cases"
    end

    context '3rd party partner api_user' do
      include_context "3rd party partner api_user"
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "Job Rejected cases"
    end
  end

  context 'when api_company is a FleetManaged company' do
    context 'as a client api_application' do
      include_context "client api_application"
      let!(:job) { create(:fleet_managed_job, status: :assigned, fleet_company: api_company) }

      it_behaves_like 'not authorized to Reject a job'
    end
  end
end
