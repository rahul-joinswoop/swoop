# frozen_string_literal: true

require 'rails_helper'

describe Mutations::UpdateJobStatus do
  let(:query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: UpdateJobStatusInput!) {
        updateJobStatus(input: $input) {
          job {
            id
            status
            statusReasons {
              edges {
                node {
                  id
                  status
                  reason
                }
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let(:job_status) { response.dig('data', 'updateJobStatus', 'job', 'status') }

  let(:variables) do
    {
      input: {
        job: {
          id: job.to_ssid,
          status: 'Canceled',
        },
      },
    }
  end

  let!(:status_reason_type) { create(:job_status_reason_type, :cancel_another_job_priority) }
  let(:statusReason) { nil }
  let(:agero) do
    c = create :fleet_managed_company, :agero, use_custom_job_status_reasons: true
    CompanyJobStatusReasonType.init_by_client_id(:AGO)
    c
  end

  let!(:job) do
    create :fleet_motor_club_job,
           status: status,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end

  shared_examples "changes job status" do
    it "changes job.status" do
      expect { response && job.reload }
        .to change(job, :status)
        .from(status.to_s)
        .to('canceled')
      expect(job_status).to eq('Canceled')
    end
  end

  shared_examples 'does not change job status' do
    it 'does not change job status' do
      expect { response && job.reload }
        .not_to change(job, :status)
        .from(status.to_s)
    end
  end

  shared_examples "changes job_status_reason_types" do
    it 'updates job.job_status_reasons accordingly' do
      expect { response && job.reload }
        .to change(job, :job_status_reason_types)
        .to([status_reason_type])
    end
  end

  shared_examples "does not change job_status_reason_types" do
    it 'updates job.job_status_reasons accordingly' do
      expect { response && job.reload }
        .not_to change { job.job_status_reason_types.count }
    end
  end

  context 'from draft' do
    let(:status) { :draft }

    context 'as a Partner company' do
      let(:variables) { super().tap { |v| v[:input][:job][:reason] = statusReason } }

      shared_examples 'it works' do
        context "with fleet managed job" do
          let(:errors_array) do
            [{
              locations: [{ column: 3, line: 2 }],
              message: Analyzers::Validations::UpdateJobStatus.error_message(api_company, job.human_status, 'Canceled'),
              path: ["updateJobStatus"],
            }]
          end
          let(:show_errors) { false }

          it_behaves_like "responds with error"
          it_behaves_like "does not change job status"
          it_behaves_like "does not change job_status_reason_types"
        end

        context "with a rescue job" do
          let!(:job) do
            create :rescue_job,
                   status: status,
                   fleet_company: rescue_company,
                   rescue_company: rescue_company
          end

          it_behaves_like "does not raise error"
          it_behaves_like "changes job status"
          it_behaves_like "does not change job_status_reason_types"
        end
      end

      context 'with a partner api_user' do
        include_context "partner api_user"
        it_behaves_like "it works"
      end

      context '3rd party partner api_user' do
        include_context "3rd party partner api_user"
        it_behaves_like "it works"
      end
    end

    context 'as a Client company' do
      include_context "client api_application"

      context "with an agero job" do
        let(:api_company) { agero }
        let(:api_application) { create(:doorkeeper_application, owner: api_company, scopes: :fleet) }
        let(:fleet_managed_company) { agero }

        context "without a reason" do
          it_behaves_like "does not raise error"
          it_behaves_like "changes job status"
          it_behaves_like "does not change job_status_reason_types"
        end
      end

      context "with a non-agero job" do
        let(:api_company) { fleet_managed_company }
        let(:api_application) { create(:doorkeeper_application, owner: api_company, scopes: :fleet) }

        context "without a reason" do
          it_behaves_like "does not raise error"
          it_behaves_like "changes job status"
          it_behaves_like "does not change job_status_reason_types"
        end
      end
    end
  end

  context 'from pending' do
    let(:status) { :pending }

    context 'as a Partner company' do
      let(:variables) { super().tap { |v| v[:input][:job][:reason] = statusReason } }
      let(:show_errors) { false }

      shared_examples 'it works' do
        context "with an agero job" do
          let(:fleet_managed_company) { agero }

          context "with a reason" do
            let(:statusReason) { status_reason_type.text }

            it_behaves_like "does not raise error"
            it_behaves_like "changes job status"
            it_behaves_like "changes job_status_reason_types"
          end

          context "without a reason" do
            let(:errors_array) do
              msg = Analyzers::Validations::JobStatusReasonType::REASON_REQUIRED_ERROR_MESSAGE % ['Canceled']
              [{
                message: msg,
                extensions: {
                  problems: [{
                    explanation: msg,
                  }],
                },
                locations: [{ column: 3, line: 2 }],
                path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
              }]
            end

            it_behaves_like "responds with error"
            it_behaves_like "does not change job status"
            it_behaves_like "does not change job_status_reason_types"
          end
        end

        context "with a non-agero job" do
          context "with a reason" do
            let(:statusReason) { status_reason_type.text }
            let(:errors_array) do
              msg = Analyzers::Validations::JobStatusReasonType::INVALID_STATUS_REASON_FOR_JOB_MESSAGE
              [{
                message: msg,
                extensions: {
                  problems: [{
                    explanation: msg,
                  }],
                },
                locations: [{ column: 3, line: 2 }],
                path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "reason"],
              }]
            end

            it_behaves_like "responds with error"
            it_behaves_like "does not change job status"
            it_behaves_like "does not change job_status_reason_types"
          end

          context "without a reason" do
            it_behaves_like "does not raise error"
            it_behaves_like "changes job status"
            it_behaves_like "does not change job_status_reason_types"
          end

          context "with a fleet managed job" do
            let!(:job) { create(:fleet_managed_job, status: :pending, fleet_company: fleet_managed_company, rescue_company: rescue_company) }

            let(:errors_array) do
              msg = Analyzers::Validations::JobStatusCanceled::PARTNER_CANNOT_CANCEL_FLEET_MANAGED_JOB_MESSAGE
              [{
                extensions: {
                  problems: [{
                    explanation: msg,
                  }],
                },
                locations: [{ column: 3, line: 2 }],
                message: msg,
                path: ["mutation #{operation_name}", "updateJobStatus", "input", "job", "status"],
              }]
            end

            it_behaves_like "responds with error"
            it_behaves_like "does not change job status"
            it_behaves_like "does not change job_status_reason_types"
          end
        end
      end

      context 'with a partner api_user' do
        include_context "partner api_user"
        it_behaves_like "it works"
      end

      context '3rd party partner api_user' do
        include_context "3rd party partner api_user"
        it_behaves_like "it works"
      end
    end

    context 'as a Client company' do
      include_context "client api_application"
      context "with an agero job" do
        let(:api_company) { agero }
        let(:api_application) { create(:doorkeeper_application, owner: api_company, scopes: :fleet) }
        let(:fleet_managed_company) { agero }

        context "without a reason" do
          it_behaves_like "does not raise error"
          it_behaves_like "changes job status"
          it_behaves_like "does not change job_status_reason_types"
        end
      end

      context "with a non-agero job" do
        let(:api_company) { fleet_managed_company }
        let(:api_application) { create(:doorkeeper_application, owner: api_company, scopes: :fleet) }

        context "without a reason" do
          it_behaves_like "does not raise error"
          it_behaves_like "changes job status"
          it_behaves_like "does not change job_status_reason_types"
        end
      end
    end
  end
end
