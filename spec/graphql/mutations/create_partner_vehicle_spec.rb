# frozen_string_literal: true

require 'rails_helper'

describe Mutations::CreatePartnerVehicle do
  let(:vehicle_name) { 'Super rescue vehicle' }

  let(:variables) do
    {
      input: {
        partnerVehicle: {
          name: vehicle_name,
        },
      },
    }
  end

  let(:query) do
    <<~GRAPHQL
      mutation #{operation_name}($input: CreatePartnerVehicleInput!) {
        createPartnerVehicle(input: $input) {
          partnerVehicle {
            id
            name
          }
        }
      }
    GRAPHQL
  end

  shared_examples "createPartnerVehicle" do
    it "does not throw error" do
      expect { response }.not_to raise_error
    end

    it "does not include errors on response" do
      expect(response).not_to include('errors')
    end

    it "creates the RescueVehicle accordingly" do
      expect { response }.to change(RescueVehicle, :count).by(1)
    end

    it "returns the created RescueVehicle" do
      vehicle_from_response = response.dig('data', 'createPartnerVehicle', 'partnerVehicle').with_indifferent_access

      expect(vehicle_from_response[:id]).to be_present
      expect(vehicle_from_response[:name]).to eq(vehicle_name)
    end
  end

  shared_examples "it works" do
    context 'as a driver' do
      let(:api_user) { rescue_driver }

      it_behaves_like 'createPartnerVehicle'

      # To be implemented in ENG-11015
      skip 'when partner company has disabled driver truck creation' do
        it_behaves_like 'responds with error'
      end
    end

    context 'as a dispatcher' do
      let(:api_user) { rescue_dispatcher }

      it_behaves_like 'createPartnerVehicle'
    end

    context 'as a driver dispatcher' do
      let(:api_user) { rescue_dispatcher_driver }

      it_behaves_like 'createPartnerVehicle'
    end

    context 'as someone else' do
      let(:api_user) { create :user, :rescue, company: rescue_company }
      let(:errors_array) do
        [
          {
            message: Analyzers::CreatePartnerVehicleAnalyzer::NOT_AUTHORIZED_ERROR_MESSAGE,
            extensions: {
              problems: [
                {
                  explanation: Analyzers::CreatePartnerVehicleAnalyzer::NOT_AUTHORIZED_ERROR_MESSAGE,
                },
              ],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "createPartnerVehicle"],
          },
        ]
      end

      it_behaves_like 'responds with error'
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "it works"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "it works"
  end
end
