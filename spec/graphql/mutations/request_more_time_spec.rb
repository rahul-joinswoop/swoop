# frozen_string_literal: true

require 'rails_helper'

describe Mutations::RequestMoreTime, freeze_time: true do
  shared_examples 'requestMoreTime' do
    let(:fleet_company) { create(:fleet_company, :ago) }

    let(:variables) do
      {
        input: {
          job: {
            id: job.to_ssid,
          },
        },
      }
    end

    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name}($input: RequestMoreTimeInput!) {
        requestMoreTime(input: $input) {
          job {
            id
            partner {
              canRequestMoreTime
            }
          }
        }
      }
      GRAPHQL
    end

    let!(:account) do
      create(:account, company: api_company, client_company: fleet_company)
    end

    let(:job) do
      create :fleet_motor_club_job, {
        status: :assigned,
        fleet_company: fleet_company,
        adjusted_created_at: Time.now,
        rescue_company: api_company,
        issc_dispatch_request: issc_dispatch_request,
        answer_by: Time.now + 90,
      }
    end

    let(:issc_dispatch_request) do
      create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end

    context 'ISSC job' do
      let(:issc) { create(:ago_issc, company: api_company, api_access_token: nil) }
      let(:errors_array) do
        [
          {
            message: Analyzers::RequestMoreTimeAnalyzer::RSC_JOBS_ONLY_ERROR_MESSAGE,
            extensions: {
              problems: [{
                explanation: Analyzers::RequestMoreTimeAnalyzer::RSC_JOBS_ONLY_ERROR_MESSAGE,
              }],
            },
            locations: [{ column: 3, line: 2 }],
            path: ["mutation #{operation_name}", "requestMoreTime", "input", "job", "id"],
          }.deep_stringify_keys,
        ]
      end

      it_behaves_like "responds with error"
    end

    context 'RSC job' do
      let(:api_access_token) { create(:api_access_token) }
      let(:issc) do
        create(
          :ago_issc,
          company: api_company,
          api_access_token: api_access_token,
          system: Issc::RSC_SYSTEM
        )
      end

      it_behaves_like "does not raise error"

      it 'works' do
        allow(Agero::Rsc::RequestMoreTimeWorker)
          .to receive(:perform_async)

        expect { response && job.reload }
          .to change(API::AsyncRequest, :count)
          .by(1)

        req = API::AsyncRequest.last
        expect(req.user_id).to eq(context[:api_user].id)
        expect(req.company_id).to eq(context[:api_company].id)
        expect(req.target_id).to eq(job.id)
        expect(req.system).to eq('RscRequestMoreTime')

        expect(Agero::Rsc::RequestMoreTimeWorker)
          .to have_received(:perform_async)
          .with(job.id, req.id)

        expect(response.dig('data', 'requestMoreTime', 'job', 'partner', 'canRequestMoreTime')).to be(false)
      end

      context 'when more time has already been requested' do
        let(:issc_dispatch_request) do
          idr = super()
          idr.update! more_time_requested: true
          idr
        end

        let(:errors_array) do
          [
            {
              message: Analyzers::RequestMoreTimeAnalyzer::RSC_JOBS_ONLY_ERROR_MESSAGE,
              extensions: {
                problems: [{
                  explanation: Analyzers::RequestMoreTimeAnalyzer::RSC_JOBS_ONLY_ERROR_MESSAGE,
                }],
              },
              locations: [{ column: 3, line: 2 }],
              path: ["mutation #{operation_name}", "requestMoreTime", "input", "job", "id"],
            }.deep_stringify_keys,
          ]
        end

        it_behaves_like "responds with error"
      end
    end
  end
  context 'with a partner api_user' do
    include_context "partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "requestMoreTime"
  end

  context '3rd party partner api_user' do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "requestMoreTime"
  end
end
