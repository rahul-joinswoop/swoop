# frozen_string_literal: true

require 'rails_helper'

describe Mutations::CreateDemoJob, freeze_time: true do
  shared_examples 'createDemoJob' do
    let(:query) do
      <<~GRAPHQL
      mutation #{operation_name} {
        createDemoJob(input: {}) {
          job {
            id
          }
        }
      }
      GRAPHQL
    end

    before do
      create :fleet_demo_company
      create :question, question: Question::LOW_CLEARANCE, with_answers: ['Yes']
      create :question, question: Question::ANYONE_ATTEMPTED_TO_JUMP, with_answers: ['Yes']
      create :question, question: Question::VEHICLE_STOP_WHEN_RUNNING, with_answers: ['Yes']
      create :question, question: Question::CUSTOMER_WITH_VEHICLE, with_answers: ['Yes']
      create :question, question: Question::KEYS_PRESENT, with_answers: ['Yes']
    end

    it "works" do
      expect { response }.to change(Job, :count).by(1)
      expect(response).not_to include('errors')
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "createDemoJob"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "createDemoJob"
  end
end
