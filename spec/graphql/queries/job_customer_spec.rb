# frozen_string_literal: true

require 'rails_helper'

describe "Job Customer Query" do
  let(:customer) { create :customer, company: fleet_managed_company }
  let(:job) do
    create :fleet_motor_club_job,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end

  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        job(id: $id) {
          id
          customer {
            name
            phone
            email
          }
        }
      }
    GRAPHQL
  end

  let(:email_response) { response.dig('data', 'job', 'customer', 'email') }
  let(:customer_response) { response.dig('data', 'job', 'customer') }
  let(:phone_response) { response.dig('data', 'job', 'customer', 'phone') }
  let(:name_response) { response.dig('data', 'job', 'customer', 'name') }

  let(:variables) { { id: job.to_ssid } }

  shared_examples "customer is hidden" do
    it "works" do
      expect(customer_response).to be_nil
    end
  end

  shared_examples "customer is present" do
    it "works" do
      expect(phone_response).to eq(job.customer.phone)
      expect(name_response).to eq(job.customer.name)
    end
  end

  shared_examples "email is hidden" do
    context "when only customer has an email" do
      it "works" do
        expect(email_response).to be_nil
      end
    end

    context "when only job has an email" do
      let(:job) do
        j = super()
        j.customer.update! email: nil
        j.update! email: Faker::Internet.email
        j
      end

      it "works" do
        expect(email_response).to be_nil
      end
    end

    context "when job + customer has an email" do
      let(:job) do
        j = super()
        j.update! email: Faker::Internet.email
        j
      end

      it "works" do
        expect(email_response).to be_nil
      end
    end
  end
  shared_examples "email is present" do
    context "when only customer has an email" do
      it "works" do
        expect(email_response).to eq(job.customer.email)
      end
    end

    context "when only job has an email" do
      let(:job) do
        j = super()
        j.customer.update! email: nil
        j.update! email: Faker::Internet.email
        j
      end

      it "works" do
        expect(email_response).to eq(job.email)
      end
    end

    context "when job + customer has an email" do
      let(:job) do
        j = super()
        j.update! email: Faker::Internet.email
        j
      end

      it "works" do
        expect(email_response).to eq(job.customer.email)
      end
    end
  end

  context "as a client company" do
    context 'as an api_application' do
      include_context "client api_application"

      context "with a FleetMotorClubJob" do
        let(:job) do
          create :fleet_motor_club_job,
                 fleet_company: fleet_managed_company,
                 rescue_company: rescue_company
        end

        it_behaves_like "email is present"
        it_behaves_like "customer is present"
      end

      context "with a FleetManagedJob" do
        let(:job) do
          create :fleet_managed_job,
                 fleet_company: fleet_managed_company,
                 rescue_company: rescue_company
        end

        it_behaves_like "email is present"
        it_behaves_like "customer is present"
      end
    end
  end

  context "as a partner company" do
    shared_examples 'it works' do
      context "with a FleetMotorClubJob" do
        let(:job) do
          create :fleet_motor_club_job,
                 fleet_company: fleet_managed_company,
                 rescue_company: rescue_company
        end

        it_behaves_like "customer is present"

        context "when only customer has an email" do
          it "works" do
            expect(email_response).to eq(job.customer.email)
          end
        end

        context "when only job has an email" do
          let(:job) do
            j = super()
            j.customer.update! email: nil
            j.update! email: Faker::Internet.email
            j
          end

          it "works" do
            expect(email_response).to eq(job.email)
          end
        end

        context "when job + customer has an email" do
          let(:job) do
            j = super()
            j.update! email: Faker::Internet.email
            j
          end

          it "works" do
            expect(email_response).to eq(job.customer.email)
          end
        end

        context "and job is auto-assigning" do
          let(:job) do
            create :fleet_motor_club_job,
                   fleet_company: fleet_managed_company,
                   rescue_company: rescue_company,
                   status: :auto_assigning
          end

          it_behaves_like "email is hidden"
          it_behaves_like "customer is hidden"
        end
      end

      context "with a FleetManagedJob" do
        let(:job) do
          create :fleet_managed_job,
                 fleet_company: fleet_managed_company,
                 rescue_company: rescue_company
        end

        it_behaves_like "email is hidden"
        it_behaves_like "customer is present"
      end
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "it works"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "it works"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "it works"
    end
  end
end
