# frozen_string_literal: true

require 'rails_helper'

describe "StorageTypes Query" do
  let(:query) do
    <<~GRAPHQL
      fragment companyFields on Company {
        storageTypes {
          edges {
            node {
              id
              name
            }
          }
        }
      }

      query #{operation_name} {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
    GRAPHQL
  end

  let(:another_company) { create(:rescue_company) }
  let!(:another_company_storage_types) { create_list :storage_type, 3, company: another_company }

  shared_context "storageTypes" do
    let!(:storage_types) { create_list :storage_type, 3, company: api_company }
    let!(:deleted_storage_types) { create :storage_type, company: api_company, deleted_at: Time.now }

    it 'returns expected storage types' do
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
      expected_result = storage_types.map { |st| { "id" => st.to_ssid, "name" => st.name } }

      query_result = response.dig(*viewer_company_path, "storageTypes", "edges")
      nodes = query_result.to_a.map { |r| r['node'] }

      expect(nodes.size).to eq expected_result.size
      expect(nodes).to match_array expected_result
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "storageTypes"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "storageTypes"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "storageTypes"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "storageTypes"
  end
end
