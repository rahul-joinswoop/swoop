# frozen_string_literal: true

require 'rails_helper'

describe "GraphQLSubscriptions Query" do
  let(:query) do
    <<~GRAPHQL
      fragment SubscriptionsFragment on WebhookSubscriptionConnection {
        edges {
          node {
            id
            queryString
            webhookUrl
            variables
          }
        }
      }
      query #{operation_name} {
        viewer {
          ...on User {
            subscriptions {
              ...SubscriptionsFragment
            }
          }
          ...on Application {
            subscriptions {
              ...SubscriptionsFragment
            }
          }
          ...on Company {
            subscriptions {
              ...SubscriptionsFragment
            }
          }
        }
      }
    GRAPHQL
  end

  let!(:subscriptions) { create_list :graphql_subscription, 3, viewer: viewer }
  let(:ids) { response.dig('data', 'viewer', 'subscriptions', 'edges').pluck('node').pluck('id') }

  shared_examples "it works" do
    it "works" do
      expect(ids).to include(*subscriptions.map(&:to_ssid))
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "it works"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "it works"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "it works"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "it works"
  end
end
