# frozen_string_literal: true

require 'rails_helper'

describe "Visibility" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name} {
	      __type(name: #{field_name.inspect}) {
          name
          fields {
            name
          }
        }
      }
    GRAPHQL
  end

  let(:fields) { response.dig("data", "__type", "fields")&.map { |f| f["name"] } }
  let(:features) { [] }

  shared_examples "fields are present" do
    it 'works' do
      expect(response).not_to include('errors')
      feature_fields.each do |field|
        expect(fields).to include(field)
      end
    end
  end

  shared_examples "fields are present with features" do
    let(:api_company) do
      super().tap do |c|
        features.each do |f|
          c.features << create(:feature, name: f)
        end
      end
    end
    it 'works' do
      expect(response).not_to include('errors')
      feature_fields.each do |field|
        expect(fields).to include(field)
      end
    end
  end

  shared_examples "fields are not present" do
    it 'works' do
      expect(response).not_to include('errors')
      feature_fields.each do |field|
        expect(fields).not_to include(field)
      end
    end
  end

  shared_examples 'rescue company' do
    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "fields are present"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "fields are present"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "fields are present"
    end
  end

  shared_examples "fleet_company" do
    context 'with a client api_application' do
      include_context "client api_application"
      it_behaves_like "fields are not present"
      it_behaves_like "fields are present with features"
    end
  end

  context "Job" do
    let(:field_name) { 'Job' }
    let(:feature_fields) { ['paymentType', 'poNumber', 'refNumber'] }
    let(:features) { [Feature::CUSTOMER_TYPE, Feature::JOB_PO_NUMBER, Feature::JOB_REFERENCE_NUMBER] }

    it_behaves_like 'rescue company'
    it_behaves_like 'fleet_company'
  end

  context "JobClient" do
    let(:field_name) { 'JobClient' }
    let(:feature_fields) { ['department'] }
    let(:features) { [Feature::DEPARTMENTS] }

    it_behaves_like 'rescue company'
    it_behaves_like 'fleet_company'
  end

  context "JobPartner" do
    let(:field_name) { 'JobPartner' }
    let(:feature_fields) { ['trailer'] }
    let(:features) { [Feature::TRAILERS] }

    it_behaves_like 'rescue company'
    it_behaves_like 'fleet_company'
  end

  context "JobService" do
    let(:field_name) { 'JobService' }
    let(:feature_fields) { ['classType', 'symptom'] }
    let(:features) { [Feature::FLEET_CLASS_TYPE, Feature::SHOW_SYMPTOMS] }

    it_behaves_like 'rescue company'
    it_behaves_like 'fleet_company'
  end

  context "StrandedVehicle" do
    let(:field_name) { 'StrandedVehicle' }
    let(:feature_fields) { ['style', 'serialNumber', 'unitNumber'] }
    let(:features) { [Feature::HEAVY_DUTY_EQUIPMENT, Feature::SERIAL_NUMBER, Feature::JOB_UNIT_NUMBER] }

    it_behaves_like 'rescue company'
    it_behaves_like 'fleet_company'
  end
end
