# frozen_string_literal: true

require 'rails_helper'
describe "Job vehicle.style Query" do
  shared_examples 'job.vehicle.style' do
    let!(:job) do
      create :job,
             :with_driver_vehicle_style,
             fleet_company: fleet_managed_company,
             rescue_company: rescue_company
    end

    let(:query) do
      <<~GRAPHQL
        query #{operation_name} {
          job(id: "#{job.to_ssid}") {
            id
            vehicle {
              style
              category {
                name
              }
              classType {
                name
              }
            }
          }
        }
      GRAPHQL
    end

    it 'works' do
      expect(response).not_to include('errors')
      job_response = response.dig("data", "job")
      expect(job_response["id"]).to eq job.to_ssid
      expect(job_response.dig("vehicle", "style")).to eq job.driver.vehicle.vehicle_type
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "job.vehicle.style"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "job.vehicle.style"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "job.vehicle.style"
  end

  context "as a client api_application" do
    include_context "client api_application"
    before { api_company.features << create(:feature, name: Feature::HEAVY_DUTY_EQUIPMENT) }

    it_behaves_like "job.vehicle.style"
  end
end
