# frozen_string_literal: true

require 'rails_helper'

describe "Job Location Query" do
  shared_examples 'job.location' do
    let(:job) do
      create :fleet_managed_job,
             :with_service_location,
             :with_drop_location,
             fleet_company: fleet_managed_company,
             rescue_company: rescue_company
    end

    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            location {
              dropoffLocation {
                address
                street
                city
                state
                postalCode
                country
              }
              serviceLocation {
                address
                street
                city
                state
                postalCode
                country
              }
            }
          }
        }
      GRAPHQL
    end

    let(:variables) { { id: job.to_ssid } }
    let(:location) { response.dig('data', 'job', 'location') }
    let(:expected_location) do
      {
        dropoffLocation: {
          address: job.drop_location.address,
          street: job.drop_location.street,
          city: job.drop_location.city,
          state: job.drop_location.state,
          postalCode: job.drop_location.zip,
          country: job.drop_location.country,
        },
        serviceLocation: {
          address: job.service_location.address,
          street: job.service_location.street,
          city: job.service_location.city,
          state: job.service_location.state,
          postalCode: job.service_location.zip,
          country: job.service_location.country,
        },
      }.deep_stringify_keys
    end
    it "works" do
      expect(location).to eq(expected_location)
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "job.location"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "job.location"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "job.location"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "job.location"
  end
end
