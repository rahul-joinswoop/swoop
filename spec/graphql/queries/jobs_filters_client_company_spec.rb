# frozen_string_literal: true

require 'rails_helper'

# have to freeze time with a #round call, otherwise the milliseconds value
# ends up breaking our queries to postgres
describe "Jobs Query Filtered on clientCompany.id" do
  let(:variables) { { id: id } }
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        jobs(filters: { client: { company: { id: $id } }}) {
          edges {
            node {
              id
            }
          }
        }
      }
    GRAPHQL
  end
  let!(:fleet_jobs) do
    create_list(:fleet_managed_job,
                2,
                fleet_company: fleet_managed_company,
                status: :unassigned) +
    create_list(:fleet_managed_job,
                2,
                fleet_company: fleet_motor_club_company,
                status: :unassigned)
  end

  shared_examples "clientCompany.id filters" do
    shared_examples "it works" do
      it "works" do
        # we didn't blow up
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        response_ids = response.dig("data", "jobs", "edges")&.map { |e| e.dig("node", "id") }
        expect(response_ids).to contain_exactly(*expected_jobs.map(&:to_ssid))
      end
    end

    context "without filters.clientCompany.id" do
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            jobs {
              edges {
                node {
                  id
                }
              }
            }
          }
        GRAPHQL
      end
      let(:variables) { {} }
      let(:expected_jobs) { fleet_jobs }

      it_behaves_like "it works"
    end

    context "with a valid id" do
      let(:id) { fleet_managed_company.to_ssid }
      let(:expected_jobs) { fleet_jobs.slice(0, 2) }

      it_behaves_like "it works"
    end

    context "with an invalid id" do
      let(:id) { "asdf1234" }
      let(:expected_jobs) { [] }

      # just silently drop the invalid id and return nothing
      it_behaves_like "it works"
    end
  end

  context "super client api_application" do
    include_context "super client api_application"

    it_behaves_like "clientCompany.id filters"
  end
end
