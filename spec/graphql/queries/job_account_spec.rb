# frozen_string_literal: true

require 'rails_helper'

describe "Job Account Query" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        job(id: $id) {
          id
          account {
            id
            name
          }
        }
      }
    GRAPHQL
  end

  let(:fleet_company) { fleet_managed_company }
  let(:job) do
    create(:fleet_managed_job, :with_auction, fleet_company: fleet_company, bids: 3).tap do |j|
      j.auction.update! status: Auction::Auction::LIVE
      j.auction.bids << create(:bid, auction: j.auction, job: j, company: rescue_company)
    end
  end
  let(:variables) { { id: job.to_ssid } }
  let(:job_response) { response.dig('data', 'job') }
  let(:account) { nil }

  shared_examples "job.account" do
    context "job with no account" do
      let(:job) do
        j = super()
        j.update! account_id: nil
        j
      end

      context "company with a swoop account" do
        let!(:account) do
          create :account, skip_account_setup: true, name: "Swoop", company: rescue_company, client_company: create(:super_company)
        end

        it 'works' do
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(job_response).to eq({
            id: job.auction.bids.last.job_ssid,
            account: {
              id: account.to_ssid,
              name: account.name,
            },
          }.deep_stringify_keys)
        end
      end

      context "company without a swoop account" do
        it 'works' do
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(job_response).to eq({ id: job.auction.bids.last.job_ssid, account: nil }.deep_stringify_keys)
        end
      end
    end

    context "job with account" do
      let(:job) do
        j = super()
        j.update! account: create(:account, company: rescue_company)
        j
      end

      context "company with a swoop account" do
        let!(:account) do
          create :account, skip_account_setup: true, name: "Swoop", company: rescue_company, client_company: create(:super_company)
        end

        it 'works' do
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(job_response).to eq({
            id: job.auction.bids.last.job_ssid,
            account: {
              id: job.account.to_ssid,
              name: job.account.name,
            },
          }.deep_stringify_keys)
        end
      end

      context "company without a swoop account" do
        it 'works' do
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(job_response).to eq({
            id: job.auction.bids.last.job_ssid,
            account: {
              id: job.account.to_ssid,
              name: job.account.name,
            },
          }.deep_stringify_keys)
        end
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "job.account"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "job.account"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "job.account"
  end
end
