# frozen_string_literal: true

require 'rails_helper'

describe "Jobs Query" do
  let(:fleet_company) { fleet_managed_company }
  let(:auto_assigning_job) do
    create(:fleet_managed_job,
           :with_auction,
           :with_service_location,
           :with_drop_location,
           status: :auto_assigning,
           fleet_company: fleet_company,
           bids: 3).tap do |j|
      j.auction.update! status: Auction::Auction::LIVE
      j.auction.bids << create(:bid, auction: j.auction, job: j, company: rescue_company)
    end
  end
  let(:enroute_job) do
    create :fleet_managed_job,
           :with_service_location,
           :with_drop_location,
           status: :enroute,
           rescue_company: rescue_company,
           fleet_company: fleet_company
  end
  let(:query) do
    <<~GRAPHQL
      query #{operation_name} {
        job(id: #{job.to_ssid.inspect}) {
          id
          customer {
            fullName
            phone
          }
          vehicle {
            license
          }
          location {
            serviceLocation {
              address
            }
            dropoffLocation {
              address
            }
            pickupContact {
              fullName
              phone
            }
            dropoffContact {
              fullName
              phone
            }
          }
        }
      }
    GRAPHQL
  end
  let(:full_response) do
    {
      job: {
        id: job.to_ssid,
        customer: {
          fullName: job.customer.full_name,
          phone: job.customer.phone,
        },
        vehicle: {
          license: job&.stranded_vehicle.license,
        },
        location: {
          serviceLocation: {
            address: job.service_location.address,
          },
          dropoffLocation: {
            address: job.drop_location.address,
          },
          pickupContact: {
            fullName: job.pickup_contact.name,
            phone: job.pickup_contact.phone,
          },
          dropoffContact: {
            fullName: job.dropoff_contact.name,
            phone: job.dropoff_contact.phone,
          },
        },
      },
    }.deep_stringify_keys
  end

  context "as a client company" do
    context 'as a client api_application' do
      include_context "client api_application"

      context 'with an enroute job' do
        let(:job) { enroute_job }

        it 'works' do
          expect(response['data']).to eq(full_response)
        end
      end

      context 'with an auto_assigning job' do
        let(:job) { auto_assigning_job }

        it 'works' do
          expect(response['data']).to eq(full_response)
        end
      end
    end
  end

  context "as a partner company" do
    shared_examples 'it works' do
      context 'with an enroute job' do
        let(:job) { enroute_job }

        it 'works' do
          expect(response['data']).to eq(full_response)
        end
      end

      context 'with an auto_assigning job' do
        let(:job) { auto_assigning_job }
        let(:full_response) do
          {
            job: {
              id: job.auction.bids.last.job_ssid,
              customer: nil,
              vehicle: {
                license: '',
              },
              location: {
                serviceLocation: {
                  address: job.service_location.address_without_street_number,
                },
                dropoffLocation: {
                  address: job.drop_location.address_without_street_number,
                },
                pickupContact: nil,
                dropoffContact: nil,
              },
            },
          }.deep_stringify_keys
        end

        it 'works' do
          expect(response['data']).to eq(full_response)
        end
      end
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "it works"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "it works"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "it works"
    end
  end
end
