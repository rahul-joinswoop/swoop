# frozen_string_literal: true

require 'rails_helper'

describe 'Invoices query' do
  let(:invoices_query) do
    <<~INVOICES_QUERY
      #{query_field} {
        edges {
          node {
            id
            balance
            currency
            totalAmount
            createdAt
            paid
            lineItems {
              edges {
                node {
                  id
                  currency
                  description
                  netAmount
                  unitPrice
                  quantity
                  taxAmount
                }
              }
            }
            payments {
              edges {
                node {
                  id
                  amount
                  currency
                  markPaidAt
                  memo
                  type
                }
              }
            }
          }
        }
      }
    INVOICES_QUERY
  end

  let(:job_query) { false }

  let(:invoice) { create(:invoice, sender: invoice_sender, recipient_company: invoice_recipient, state: invoice_state) }
  let!(:invoice_line_item) do
    create(
      :invoicing_line_item,
      ledger_item_id: invoice.id,
      description: 'Flat Rate',
      net_amount: '12.72',
      unit_price: '5.3',
      quantity: '2.4',
      tax_amount: '4.63'
    )
  end

  let!(:account) { create(:account, company: account_company, client_company: account_client_company) }
  let(:invoice_state) { :partner_new }
  let(:invoice_sender) { rescue_company }

  before do
    RateType.init_all

    job.reload

    invoice.update!(recipient: job.fleet_company, job: job)
  end

  shared_examples 'fetch invoices' do
    shared_context 'append rateTypes query' do
      let(:query) do
        qs = super().dup
        # insert rateTypes into our query as a sibling of createdAt
        qs.insert(qs.index(/^\s+?createdAt/), "rateTypes {\n edges {\n node {\n name\n type \n }\n }\n }\n ")

        qs
      end
    end

    shared_context 'append classTypes query' do
      let(:query) do
        qs = super().dup
        # insert classTypes into our query as a sibling of createdAt
        qs.insert(qs.index(/^\s+?createdAt/), "classTypes {\n edges {\n node {\n name \n }\n }\n }\n ")

        qs
      end
    end

    shared_context 'append availableAdditionalLineItems query' do
      let(:query) do
        qs = super().dup
        # insert classTypes into our query as a sibling of createdAt
        qs.insert(qs.index(/^\s+?createdAt/), "availableAdditionalLineItems {\n edges {\n node {\n description \n }\n }\n }\n ")

        qs
      end
    end

    shared_examples 'does not fetch any invoice' do
      it 'does not fetch any invoice' do
        invoices_hash = nil

        expect(response).not_to include('errors')

        if job_query
          invoices_hash = response.dig("data", "job", "jobInvoices", "edges")
        else
          invoices_hash = response.dig("data", "invoices", "edges")
        end

        expect(invoices_hash.size).to eq 0
      end
    end

    shared_examples 'fetch invoices attributes correctly' do
      it 'fetches invoices correctly' do
        invoice_hash = nil

        expect(response).not_to include('errors')

        if job_query
          invoice_hash = response.dig("data", "job", "jobInvoices", "edges").first.dig("node")
        else
          invoice_hash = response.dig("data", "invoices", "edges").first.dig("node")
        end

        expect(invoice_hash["id"]).to eq invoice.to_ssid
        expect(invoice_hash["balance"]).to eq "%.2f" % invoice.balance.truncate(2)
        expect(invoice_hash["currency"]).to eq invoice.currency
        expect(invoice_hash["totalAmount"]).to eq "%.2f" % invoice.total_amount.truncate(2)
        expect(invoice_hash["createdAt"]).to eq invoice.created_at.utc.to_formatted_s(:iso8601)
        expect(invoice_hash["paid"]).to eq invoice.paid

        line_item_hash = invoice_hash.dig('lineItems', 'edges').first.dig('node')

        expect(line_item_hash["id"]).to eq invoice_line_item.to_ssid
        expect(line_item_hash["currency"]).to eq invoice_line_item.currency
        expect(line_item_hash["description"]).to eq invoice_line_item.description
        expect(line_item_hash["netAmount"]).to eq "%.2f" % invoice_line_item.net_amount.truncate(2)
        expect(line_item_hash["unitPrice"]).to eq "%.2f" % invoice_line_item.unit_price.truncate(2)
        expect(line_item_hash["quantity"]).to eq "%.2f" % invoice_line_item.quantity.truncate(2)
        expect(line_item_hash["taxAmount"]).to eq "%.2f" % invoice_line_item.tax_amount.truncate(2)
      end
    end

    shared_examples 'fetch expected rate_types' do
      include_context 'append rateTypes query'

      it 'fetches the expected rate_types' do
        invoice = nil

        expect(response).not_to include('errors')

        if job_query
          invoice = response.dig("data", "job", "jobInvoices", "edges").first.dig("node")
        else
          invoice = response.dig("data", "invoices", "edges").first.dig("node")
        end

        rate_types = invoice.dig("rateTypes", "edges")

        expect(rate_types.count).to eq RateType.standard_items.count
      end
    end

    shared_examples 'not authorized to see rateTypes' do
      include_context 'append rateTypes query'

      let(:errors_array) do
        if job_query
          [
            {
              path: ["query #{operation_name}", "job", "jobInvoices", "edges", "node", "rateTypes"],
              locations: [{ column: 1, line: 10 }],
              message: "Field 'rateTypes' doesn't exist on type 'Invoice'",
              extensions: { code: "undefinedField", fieldName: "rateTypes", typeName: "Invoice" },
            },
          ]
        else
          [
            {
              path: ["query #{operation_name}", "invoices", "edges", "node", "rateTypes"],
              locations: [{ column: 1, line: 9 }],
              message: "Field 'rateTypes' doesn't exist on type 'Invoice'",
              extensions: { code: "undefinedField", fieldName: "rateTypes", typeName: "Invoice" },
            },
          ]
        end
      end

      it_behaves_like 'responds with error'
    end

    shared_examples 'not authorized to see classTypes' do
      include_context 'append classTypes query'

      let(:errors_array) do
        if job_query
          [
            {
              path: ["query #{operation_name}", "job", "jobInvoices", "edges", "node", "classTypes"],
              locations: [{ column: 1, line: 10 }],
              message: "Field 'classTypes' doesn't exist on type 'Invoice'",
              extensions: { code: "undefinedField", fieldName: "classTypes", typeName: "Invoice" },
            },
          ]
        else
          [
            {
              path: ["query #{operation_name}", "invoices", "edges", "node", "classTypes"],
              locations: [{ column: 1, line: 9 }],
              message: "Field 'classTypes' doesn't exist on type 'Invoice'",
              extensions: { code: "undefinedField", fieldName: "classTypes", typeName: "Invoice" },
            },
          ]
        end
      end

      it_behaves_like 'responds with error'
    end

    shared_examples 'fetch expected class_types' do
      include_context 'append classTypes query'

      it 'fetches the expected class_types' do
        invoice = nil

        expect(response).not_to include('errors')

        if job_query
          invoice = response.dig("data", "job", "jobInvoices", "edges").first.dig("node")
        else
          invoice = response.dig("data", "invoices", "edges").first.dig("node")
        end

        class_types = invoice.dig("classTypes", "edges")

        expect(class_types.count).to eq job.rescue_company.vehicle_categories.count
      end
    end

    shared_examples 'fetch expected availableAdditionalLineItems' do
      include_context 'append availableAdditionalLineItems query'

      it 'fetches the expected availableAdditionalLineItems' do
        invoice_hash = nil

        expect(response).not_to include('errors')

        if job_query
          invoice_hash = response.dig("data", "job", "jobInvoices", "edges").first.dig("node")
        else
          invoice_hash = response.dig("data", "invoices", "edges").first.dig("node")
        end

        available_additional_line_items = invoice_hash.dig("availableAdditionalLineItems", "edges")

        expect(available_additional_line_items.count).to eq invoice.available_additional_line_items.count
      end
    end

    shared_examples 'not authorized to see availableAdditionalLineItems' do
      include_context 'append availableAdditionalLineItems query'

      let(:errors_array) do
        if job_query
          [
            {
              path: ["query #{operation_name}", "job", "jobInvoices", "edges", "node", "availableAdditionalLineItems"],
              locations: [{ column: 1, line: 10 }],
              message: "Field 'availableAdditionalLineItems' doesn't exist on type 'Invoice'",
              extensions: { code: "undefinedField", fieldName: "availableAdditionalLineItems", typeName: "Invoice" },
            },
          ]
        else
          [
            {
              path: ["query #{operation_name}", "invoices", "edges", "node", "availableAdditionalLineItems"],
              locations: [{ column: 1, line: 9 }],
              message: "Field 'availableAdditionalLineItems' doesn't exist on type 'Invoice'",
              extensions: { code: "undefinedField", fieldName: "availableAdditionalLineItems", typeName: "Invoice" },
            },
          ]
        end
      end

      it_behaves_like 'responds with error'
    end

    context 'when api_company is a Partner company' do
      let(:invoice_sender) { api_company }
      let(:invoice_recipient) { fleet_in_house_company }
      let(:invoice_state) { :partner_new }
      let(:account_company) { api_company }
      let(:account_client_company) { fleet_in_house_company }

      let(:job) do
        create(
          :fleet_in_house_job,
          fleet_company: fleet_in_house_company,
          rescue_company: api_company,
          invoice: invoice,
          account: account,
        )
      end

      shared_examples 'invoice partner' do
        let(:api_user) { rescue_admin }

        it_behaves_like 'fetch invoices attributes correctly'
        it_behaves_like 'fetch expected rate_types'
        it_behaves_like 'fetch expected class_types'
        it_behaves_like 'fetch expected availableAdditionalLineItems'

        context 'when api_user is driver only' do
          let(:api_user) { rescue_driver }

          it_behaves_like 'fetch invoices attributes correctly'

          context "and company has feature 'Disable driver view invoice' enabled" do
            before do
              api_company.features << create(:feature, name: Feature::DISABLE_DRIVER_VIEW_INVOICE)
            end

            it_behaves_like 'does not fetch any invoice'
          end
        end
      end

      context "as a partner api_user" do
        include_context "partner api_user"
        it_behaves_like "invoice partner"
      end

      context "as a 3rd party partner api_user" do
        include_context "3rd party partner api_user"
        it_behaves_like "invoice partner"
      end

      context "as a 3rd party partner api_company" do
        include_context "3rd party partner api_company"
        it_behaves_like 'fetch invoices attributes correctly'
        it_behaves_like 'fetch expected rate_types'
        it_behaves_like 'fetch expected class_types'
        it_behaves_like 'fetch expected availableAdditionalLineItems'
      end
    end

    context 'when api_company is a FleetManaged company' do
      context 'and an api_application is logged in' do
        include_context "client api_application"

        let(:invoice_sender) { swoop }
        let(:invoice_recipient) { api_company }
        let(:account_company) { rescue_company }
        let(:account_client_company) { swoop }
        let(:invoice_state) { :swoop_sent_fleet }

        let!(:job) do
          create(
            :fleet_managed_job,
            fleet_company: api_company,
            rescue_company: rescue_company,
            invoice: invoice,
            account: account,
          )
        end

        context 'with rateTypes added into the query' do
          it_behaves_like 'not authorized to see rateTypes'
        end

        context 'with classTypes added into the query' do
          it_behaves_like 'not authorized to see classTypes'
        end

        context 'with availableAdditionalLineItems added into the query' do
          it_behaves_like 'not authorized to see availableAdditionalLineItems'
        end
      end
    end
  end

  context 'when querying through company' do
    let(:query_field) { 'invoices' }
    let(:query) do
      <<~GRAPHQL
        query #{operation_name} {
          #{invoices_query}
        }
      GRAPHQL
    end

    it_behaves_like 'fetch invoices'
  end

  context 'when querying through job' do
    let(:variables) { { id: job.to_ssid } }
    let(:job_query) { true }
    let(:query_field) { 'jobInvoices' }

    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            #{invoices_query}
          }
        }
      GRAPHQL
    end

    it_behaves_like 'fetch invoices'
  end
end
