# frozen_string_literal: true

require 'rails_helper'

describe "LocationTypes Query" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($query: String) {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
      fragment companyFields on Company {
        locationTypes(query: $query) {
          edges {
            node {
              id
              name
            }
          }
        }
      }
    GRAPHQL
  end

  let(:query_by_name) { nil }
  let(:variables) { { query: query_by_name } }
  let!(:location_type_1) { create(:location_type, name: "Location Type Test 1") }
  let!(:location_type_2) { create(:location_type, name: "Location Type Test 2") }
  let!(:location_type_3) { create(:location_type, name: "Location Type Test 3") }
  let!(:location_type_4) { create(:location_type, name: "Location Type Test 4", deleted_at: Time.current) }
  let!(:other_company) { fleet_motor_club_company.tap { |c| c.location_types << location_type_2 } }

  shared_examples "locationTypes" do
    before do
      api_company.location_types << location_type_1
      api_company.location_types << location_type_3
      api_company.location_types << location_type_4
    end

    shared_examples "lists expected location_types" do
      it "has expected results" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expected_response = expected_location_types || {}
        sites_result = response.dig(*viewer_company_path, "locationTypes", "edges")

        expected_response_attributes = expected_response.map do |site|
          {
            id: site.to_ssid,
            name: site.name,
          }.deep_stringify_keys
        end
        location_type_nodes = sites_result.to_a.map { |r| r['node'] }

        expect(location_type_nodes.size).to eq expected_response_attributes.size
        expect(location_type_nodes).to match_array expected_response_attributes
      end
    end

    let(:expected_location_types) { api_company.location_types.not_deleted }

    it_behaves_like "lists expected location_types"

    context "when querying for locationTypes" do
      context "by 'test'" do
        let(:query_by_name) { 'test' }
        let(:expected_location_types) { [location_type_1, location_type_3] }

        it_behaves_like "lists expected location_types"
      end

      context "by a deleted account name" do
        let(:query_by_name) { 'Location Type Test 4' }
        let(:expected_location_types) { [] }

        it_behaves_like "lists expected location_types"
      end

      context "when query term is empty" do
        let(:query_by_name) { " " }
        let(:expected_location_types) { [] }

        it_behaves_like "lists expected location_types"
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    before do
      api_company.features << create(:feature, name: Feature::CLIENT_SITES)
    end

    it_behaves_like "locationTypes"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "locationTypes"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "locationTypes"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "locationTypes"
  end
end
