# frozen_string_literal: true

require 'rails_helper'

# have to freeze time with a #round call, otherwise the milliseconds value
# ends up breaking our queries to postgres
describe "Jobs Query Filtered on createdAt", freeze_time: Time.current.round do
  let!(:now) { Time.current }
  let!(:one_day_ago) { now - 1.days }
  let!(:two_days_ago) { now - 2.days }
  let!(:three_days_ago) { now - 3.days }
  let(:from) { nil }
  let(:to) { nil }
  let(:variables) { { from: from, to: to } }
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($from: DateTime, $to: DateTime) {
        jobs(filters: { createdAt: { from: $from, to: $to } })  {
          edges {
            node {
              id
            }
          }
        }
      }
    GRAPHQL
  end
  let!(:fleet_jobs) do
    jobs = create_list :job,
                       4,
                       :with_rescue_vehicle,
                       fleet_company: fleet_managed_company,
                       status: :unassigned
    jobs[0].update! created_at: now
    jobs[1].update! created_at: one_day_ago
    jobs[2].update! created_at: two_days_ago
    jobs[3].update! created_at: three_days_ago
    jobs
  end

  shared_examples "createdAt filters" do
    shared_examples "it works" do
      it "works" do
        # we didn't blow up
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        response_ids = response.dig("data", "jobs", "edges")&.map { |e| e.dig("node", "id") }
        expect(response_ids).to contain_exactly(*expected_jobs.map(&:to_ssid))
      end
    end

    context "without filters.createdAt:" do
      let(:expected_jobs) { fleet_jobs }
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            jobs {
              edges {
                node {
                  id
                }
              }
            }
          }
        GRAPHQL
      end

      it_behaves_like "it works"
    end

    context "without from: or to:" do
      let(:expected_jobs) { fleet_jobs }

      it_behaves_like "it works"
    end

    context "with from:" do
      let(:from) { two_days_ago.iso8601 }
      let(:expected_jobs) { fleet_jobs.slice(0, 3) }

      it_behaves_like "it works"
    end

    context "with to:" do
      let(:to) { one_day_ago.iso8601 }
      let(:expected_jobs) { fleet_jobs.slice(1, 3) }

      it_behaves_like "it works"
    end

    context "with from: and to:" do
      let(:from) { two_days_ago.iso8601 }
      let(:to) { one_day_ago.iso8601 }
      let(:expected_jobs) { fleet_jobs.slice(1, 2) }

      it_behaves_like "it works"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"

    it_behaves_like "createdAt filters"
  end
end
