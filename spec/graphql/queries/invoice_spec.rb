# frozen_string_literal: true

require 'rails_helper'

describe 'Invoices query' do
  let(:query) do
    <<~INVOICE_QUERY
      query #{operation_name}($id: ID!) {
        invoice(id: $id) {
          id
          currency
          availableAdditionalLineItems {
            edges {
              node {
                id
                autoCalculatedQuantity
                calcType
                currency
                deletedAt
                description
                estimated
                job {
                  id
                }
                netAmount
                originalQuantity
                originalUnitPrice
                quantity
                rate {
                  id
                  currency
                }
                taxAmount
                unitPrice
                userCreated
              }
            }
          }
          createdAt
          balance
          notes
          rateType {
            name
            type
          }
          lineItems {
            edges {
              node {
                id
                calcType
                currency
                description
                netAmount
                originalQuantity
                originalUnitPrice
                unitPrice
                quantity
                taxAmount
              }
            }
          }
          paid
          payments {
            edges {
              node {
                id
                amount
                currency
                markPaidAt
                memo
                paymentMethod
                type
              }
            }
          }
          totalAmount
        }
      }
    INVOICE_QUERY
  end

  let(:variables) { { id: invoice.to_ssid } }
  let(:invoice_sender) { rescue_company }
  let(:invoice_recipient) { fleet_managed_company }
  let(:invoice_state) { :partner_new }
  let(:account_company) { rescue_company }
  let(:account_client_company) { fleet_managed_company }
  let(:job) do
    create(
      :fleet_in_house_job,
      fleet_company: fleet_managed_company,
      rescue_company: rescue_company,
      invoice: invoice,
      account: account,
    )
  end
  let(:invoice) { create(:invoice, :with_payments, sender: invoice_sender, recipient_company: invoice_recipient, state: invoice_state) }
  let!(:invoice_line_item) do
    create(
      :invoicing_line_item,
      description: 'Flat Rate',
      ledger_item_id: invoice.id,
      net_amount: '12.72',
      original_quantity: nil, # there are cases where this is nil, let's make sure it works
      original_unit_price: nil, # there are cases where this is nil, let's make sure it works
      quantity: '2.4',
      rate_id: rate.id,
      unit_price: '5.3',
      tax_amount: '0.00'
    )
  end

  let!(:invoice_line_item_deleted) do
    create(
      :invoicing_line_item,
      description: 'Fuel',
      ledger_item_id: invoice.id,
      net_amount: '10.00',
      original_quantity: nil, # there are cases where this is nil, let's make sure it works
      original_unit_price: nil, # there are cases where this is nil, let's make sure it works
      quantity: '1.0',
      rate_id: rate.id,
      unit_price: '10.0',
      tax_amount: '0.00',
      deleted_at: Time.now,
    )
  end

  let!(:rate) { create(:clean_flat_rate, company: invoice_sender, account: account) } # account level
  let!(:account) { create(:account, company: account_company, client_company: account_client_company) }
  let(:invoice_state) { :partner_new }
  let(:invoice_sender) { rescue_company }

  before do
    RateType.init_all

    job.reload

    invoice.update!(recipient: job.fleet_company, job: job)

    # to test availableAdditionalLineItems
    invoice.sender.addons << create(:service_code, :addon, name: ServiceCode::DOLLY)
  end

  shared_examples 'invoice' do
    it 'fetches the invoice correctly' do
      expect(response).not_to include('errors')
      invoice_hash = response.dig("data", "invoice")

      expect(invoice_hash["id"]).to eq invoice.to_ssid
      expect(invoice_hash["currency"]).to eq invoice.currency
      expect(invoice_hash["balance"]).to eq "%.2f" % invoice.balance.truncate(2)
      expect(invoice_hash["createdAt"]).to eq invoice.created_at.utc.to_formatted_s(:iso8601)
      expect(invoice_hash["classType"]).to eq invoice.job.invoice_vehicle_category
      expect(invoice_hash["notes"]).to eq invoice.job.driver_notes
      expect(invoice_hash["paid"]).to eq invoice.paid
      expect(invoice_hash.dig("rateType", "name")).to eq "Flat"
      expect(invoice_hash.dig("rateType", "type")).to eq "Flat"
      expect(invoice_hash["totalAmount"]).to eq "%.2f" % invoice.total_amount.truncate(2)

      expect(invoice_hash.dig('lineItems', 'edges').size).to eq 1

      line_item_hash = invoice_hash.dig('lineItems', 'edges').first.dig('node')

      expect(line_item_hash["id"]).to eq invoice_line_item.to_ssid
      expect(line_item_hash["currency"]).to eq invoice_line_item.currency
      expect(line_item_hash["description"]).to eq invoice_line_item.description
      expect(line_item_hash["calcType"]).to eq invoice_line_item.calc_type.capitalize # cos it's Enum value in GraphQL
      expect(line_item_hash["netAmount"]).to eq "%.2f" % invoice_line_item.net_amount.truncate(2)
      expect(line_item_hash["originalQuantity"]).to eq '0.00'
      expect(line_item_hash["originalUnitPrice"]).to eq '0.00'
      expect(line_item_hash["quantity"]).to eq "%.2f" % invoice_line_item.quantity.truncate(2)
      expect(line_item_hash["taxAmount"]).to eq "%.2f" % invoice_line_item.tax_amount.truncate(2)
      expect(line_item_hash["unitPrice"]).to eq "%.2f" % invoice_line_item.unit_price.truncate(2)

      payment_hash = invoice_hash.dig('payments', 'edges').first.dig('node')
      expect(payment_hash["id"]).to eq invoice.payments.first.to_ssid
      expect(payment_hash["amount"]).to eq "-10.00"
      expect(payment_hash["currency"]).to eq invoice.currency
      expect(payment_hash["deletedAt"]).to be_nil
      expect(payment_hash["markPaidAt"]).not_to be_nil
      expect(payment_hash["memo"]).to be_nil
      expect(payment_hash["paymentMethod"]).to eq 'Credit Card'
      expect(payment_hash["type"]).to eq 'Payment'

      available_additional_li = invoice_hash.dig(
        'availableAdditionalLineItems', 'edges'
      ).first.dig('node')

      expect(available_additional_li["id"]).to be_present
      expect(available_additional_li["calcType"]).to eq "Flat"
      expect(available_additional_li["currency"]).to eq invoice.currency
      expect(available_additional_li["description"]).to eq "Dolly"
      expect(available_additional_li.dig("job", "id")).to eq invoice.job.to_ssid
      expect(available_additional_li["netAmount"]).to eq '0.00'
      expect(available_additional_li["taxAmount"]).to eq '0.00'
      expect(available_additional_li["estimated"]).to be_falsey
      expect(available_additional_li["unitPrice"]).to eq '0.00'
      expect(available_additional_li["originalUnitPrice"]).to eq '0.00'
      expect(available_additional_li["originalQuantity"]).to eq '1.00'
      expect(available_additional_li["quantity"]).to eq '1.00'
      expect(available_additional_li["userCreated"]).to be_truthy
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "invoice"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "invoice"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "invoice"
  end
end
