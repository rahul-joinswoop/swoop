# frozen_string_literal: true

require 'rails_helper'

describe "Job Partner Driver Query" do
  let(:status) { :pending }
  let(:job) do
    create :fleet_managed_job,
           :with_rescue_driver,
           :with_rescue_vehicle,
           status: status,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end
  let(:variables) { { id: job.to_ssid } }
  let(:id) { response.dig('data', 'job', 'id') }

  shared_examples "it works" do
    context "as a rescue company" do
      shared_examples "partner" do
        it 'works' do
          expect(id).to be_present
          expect(driver_name).to eq(job.rescue_driver.name)
          expect(driver_first_name).to eq(job.rescue_driver.first_name)
          expect(driver_last_name).to eq(job.rescue_driver.last_name)
          expect(driver_phone).to eq(job.rescue_driver.phone)
        end
      end
      context "as a partner api_user" do
        include_context "partner api_user"
        it_behaves_like "partner"
      end

      context "as a 3rd party partner api_user" do
        include_context "3rd party partner api_user"
        it_behaves_like "partner"
      end

      context "as a 3rd party partner api_company" do
        include_context "3rd party partner api_company"
        it_behaves_like "partner"
      end
    end

    context "as a client api_application" do
      include_context "client api_application"

      context "with status: pending" do
        it "works" do
          expect(id).to be_present
          expect(driver_name).to be_nil
          expect(driver_first_name).to be_nil
          expect(driver_last_name).to be_nil
        end
      end

      context "with status: enroute" do
        let(:status) { :enroute }

        it "works" do
          expect(id).to be_present
          expect(driver_name).to eq(job.rescue_driver.first_name)
          expect(driver_first_name).to eq(job.rescue_driver.first_name)
          expect(driver_last_name).to be_nil
        end
      end
    end
  end

  context "with current query" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            partner {
              driver {
                name
                firstName
                lastName
                phone
              }
            }
          }
        }
      GRAPHQL
    end
    let(:driver_name) { response.dig('data', 'job', 'partner', 'driver', 'name') }
    let(:driver_first_name) { response.dig('data', 'job', 'partner', 'driver', 'firstName') }
    let(:driver_last_name) { response.dig('data', 'job', 'partner', 'driver', 'lastName') }
    let(:driver_phone) { response.dig('data', 'job', 'partner', 'driver', 'phone') }

    it_behaves_like "it works"
  end

  context "with deprecated query" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            rescueVehicle {
              driver {
                name
                firstName
                lastName
                phone
              }
            }
          }
        }
      GRAPHQL
    end
    let(:driver_name) { response.dig('data', 'job', 'rescueVehicle', 'driver', 'name') }
    let(:driver_first_name) { response.dig('data', 'job', 'rescueVehicle', 'driver', 'firstName') }
    let(:driver_last_name) { response.dig('data', 'job', 'rescueVehicle', 'driver', 'lastName') }
    let(:driver_phone) { response.dig('data', 'job', 'rescueVehicle', 'driver', 'phone') }

    it_behaves_like "it works"
  end
end
