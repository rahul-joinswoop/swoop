# frozen_string_literal: true

require 'rails_helper'

describe "Settings Query" do
  let(:query) do
    <<~GRAPHQL
      fragment companyFields on Company {
        settings {
          edges {
            node {
              id
              name
              value
            }
          }
        }
      }

      query #{operation_name} {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
    GRAPHQL
  end

  shared_examples 'settings' do
    shared_examples "lists settings" do
      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expected_response = api_company.settings
        settings_result = response.dig(*viewer_company_path, "settings", "edges")

        expected_response_attributes = expected_response.map do |setting|
          {
            id: setting.to_ssid,
            name: setting.key,
            value: setting.value,
          }.deep_stringify_keys
        end
        settings_nodes = settings_result.to_a.map { |r| r['node'] }

        expect(settings_nodes.size).to eq expected_response_attributes.size
        expect(settings_nodes).to match_array expected_response_attributes
      end
    end

    context 'when Company has settings' do
      before do
        api_company.settings << create_list(:setting, 3)
      end

      it_behaves_like 'lists settings'
    end

    context 'when Company does not have settings' do
      it_behaves_like 'lists settings'
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "settings"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "settings"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "settings"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "settings"
  end
end
