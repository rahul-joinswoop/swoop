# frozen_string_literal: true

require 'rails_helper'

describe "Symptoms Query" do
  let(:query) do
    <<~GRAPHQL
      fragment companyFields on Company {
        symptoms {
          edges {
            node {
              id
              name
            }
          }
        }
      }

      query #{operation_name} {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
    GRAPHQL
  end

  let(:query_by_name) { nil }
  let(:variables) { { query: query_by_name } }

  shared_examples "symptoms" do
    before do
      api_company.features << create(:feature, name: Feature::SHOW_SYMPTOMS)
    end

    it "has expected results" do
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
      expected_response = expected_symptoms
      symptoms_result = response.dig(*viewer_company_path, "symptoms", "edges")

      expected_response_attributes = expected_response.map do |symptom|
        {
          id: symptom.to_ssid,
          name: symptom.name,
        }.deep_stringify_keys
      end
      symptom_nodes = symptoms_result.to_a.map { |r| r['node'] }

      expect(symptom_nodes.size).to eq expected_response_attributes.size
      expect(symptom_nodes).to match_array expected_response_attributes
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    let(:expected_symptoms) { Symptom.standard_items }

    it_behaves_like "symptoms"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    let(:expected_symptoms) { Symptom.standard_items }

    it_behaves_like "symptoms"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    let(:expected_symptoms) { Symptom.standard_items }

    it_behaves_like "symptoms"
  end

  context "as a client api_application" do
    include_context "client api_application"
    let(:expected_symptoms) { api_company.symptoms }

    before do
      api_company.symptoms << create(:symptom, name: 'Custom symptom', is_standard: false)
    end

    it_behaves_like "symptoms"
  end
end
