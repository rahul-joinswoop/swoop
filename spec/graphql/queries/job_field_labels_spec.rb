# frozen_string_literal: true

require 'rails_helper'

describe "Job Field Labels Query" do
  shared_examples 'fieldLabels' do
    let(:job) { create :fleet_managed_job, fleet_company: fleet_managed_company, rescue_company: rescue_company }
    let(:rescue_company) { create :rescue_company }
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            fieldLabels
          }
        }
      GRAPHQL
    end

    let(:variables) { { id: job.to_ssid } }

    context "with an enterprise job" do
      let(:fleet_managed_company) { create :fleet_company, name: 'Enterprise' }

      it "works" do
        expect(response.dig('data', 'job', 'fieldLabels')).to eq(Resolvers::FieldLabels::ENTERPRISE)
      end
    end

    context "with an enterprise fleet management job" do
      let(:fleet_managed_company) { create :fleet_company, name: 'Enterprise Fleet Management' }

      it "works" do
        expect(response.dig('data', 'job', 'fieldLabels')).to eq(Resolvers::FieldLabels::ENTERPRISE_FLEET_MANAGEMENT)
      end
    end

    context "with a turo job" do
      let(:fleet_managed_company) { create :fleet_company, name: 'Turo' }

      it "works" do
        expect(response.dig('data', 'job', 'fieldLabels')).to eq(Resolvers::FieldLabels::TURO)
      end
    end

    context "with another job" do
      let(:fleet_managed_company) { create :fleet_company }

      it "works" do
        expect(response.dig('data', 'job', 'fieldLabels')).to be nil
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "fieldLabels"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "fieldLabels"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "fieldLabels"
  end
end
