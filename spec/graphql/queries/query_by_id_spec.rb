# frozen_string_literal: true

require 'rails_helper'

describe "Query By ID" do
  let(:variables) { { id: "asdfasdf" } }
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        #{field}(id: $id) {
        id
      }
    }
    GRAPHQL
  end

  shared_examples 'query by id' do
    shared_examples 'no errors' do
      it 'works' do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
      end
    end

    context 'when company has Feature::SHOW_SYMPTOMS enabled' do
      before do
        api_company.features << create(:feature, name: Feature::SHOW_SYMPTOMS)
      end

      describe 'symptom' do
        let(:field) { 'symptom' }

        it_behaves_like 'no errors'
      end
    end

    describe 'service' do
      let(:field) { 'service' }

      it_behaves_like 'no errors'
    end

    describe 'location_type' do
      let(:field) { 'locationType' }

      it_behaves_like 'no errors'
    end

    describe 'feature' do
      let(:field) { 'feature' }

      it_behaves_like 'no errors'
    end

    describe 'job' do
      let(:field) { 'job' }

      it_behaves_like 'no errors'
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "query by id"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "query by id"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "query by id"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "query by id"
  end
end
