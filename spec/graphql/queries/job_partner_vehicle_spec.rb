# frozen_string_literal: true

require 'rails_helper'

describe "Job Partner Vehicle Query" do
  let(:status) { :pending }
  let!(:rescue_vehicle) do
    rv = job.rescue_vehicle
    rv.update! vehicle_category: create(:vehicle_category)
    rv
  end
  let(:job) do
    create :fleet_managed_job,
           :with_rescue_driver,
           :with_rescue_vehicle,
           status: status,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end
  let(:variables) { { id: job.to_ssid } }
  let(:id) { response.dig('data', 'job', 'id') }

  shared_examples "it works" do
    context "partner company" do
      shared_examples 'partner vehicle' do
        it "works" do
          expect(id).to be_present
          expect(vehicle_category_name).to eq(job.rescue_vehicle.vehicle_category.name)
          expect(vehicle_location_lat).to be_within(0.0000000001).of(job.rescue_vehicle.lat)
          expect(vehicle_location_lng).to be_within(0.0000000001).of(job.rescue_vehicle.lng)
        end
      end
      context "as a partner api_user" do
        include_context "partner api_user"
        it_behaves_like "partner vehicle"
      end

      context "as a 3rd party partner api_user" do
        include_context "3rd party partner api_user"
        it_behaves_like "partner vehicle"
      end

      context "as a 3rd party partner api_company" do
        include_context "3rd party partner api_company"
        it_behaves_like "partner vehicle"
      end
    end

    context "client company" do
      context "as a client api_application" do
        include_context "client api_application"

        context "with status: pending" do
          it "works" do
            expect(id).to be_present
            expect(vehicle_category_name).to be_nil
            expect(vehicle_location_lat).to be_nil
            expect(vehicle_location_lng).to be_nil
          end
        end

        context "with status: enroute" do
          let(:status) { :enroute }

          it "works" do
            expect(id).to be_present
            expect(vehicle_category_name).to eq(job.rescue_vehicle.vehicle_category.name)
            expect(vehicle_location_lat).to be_within(0.0000000001).of(job.rescue_vehicle.lat)
            expect(vehicle_location_lng).to be_within(0.0000000001).of(job.rescue_vehicle.lng)
          end
        end
      end
    end
  end

  context "with current query" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            partner {
              vehicle {
                category {
                  name
                }
                location {
                  lat
                  lng
                }
              }
            }
          }
        }
      GRAPHQL
    end
    let(:vehicle_category_name) { response.dig('data', 'job', 'partner', 'vehicle', 'category', 'name') }
    let(:vehicle_location_lat) { response.dig('data', 'job', 'partner', 'vehicle', 'location', 'lat') }
    let(:vehicle_location_lng) { response.dig('data', 'job', 'partner', 'vehicle', 'location', 'lng') }

    it_behaves_like "it works"
  end

  context "with deprecated query" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            rescueVehicle {
              category {
                name
              }
              location {
                lat
                lng
              }
            }
          }
        }
      GRAPHQL
    end
    let(:vehicle_category_name) { response.dig('data', 'job', 'rescueVehicle', 'category', 'name') }
    let(:vehicle_location_lat) { response.dig('data', 'job', 'rescueVehicle', 'location', 'lat') }
    let(:vehicle_location_lng) { response.dig('data', 'job', 'rescueVehicle', 'location', 'lng') }

    it_behaves_like "it works"
  end
end
