# frozen_string_literal: true

require 'rails_helper'

describe "Partner Drivers Query" do
  let(:expected_users) { nil }
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($query: String, $roles: [RoleType!]) {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
      fragment companyFields on Company {
        users(query: $query, roles: $roles) {
          edges {
            node {
              id
              fullName
              phone
              company {
                id
                name
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let(:query_by_name) { nil }
  let(:query_roles) { nil }
  let(:variables) { { query: query_by_name, roles: query_roles } }

  before do
    # TODO is there any other way to enable ElasticSearch on tests?
    allow_any_instance_of(UserSearchService).to receive(:search).with(
      { search_terms: { term: query_by_name }, filters: { "company_id" => rescue_company.id } }
    ).and_return([
      expected_users.reject(&:blank?).size,
      double(:records, records: User.where(id: expected_users.reject(&:blank?)&.map(&:id))),
    ])
  end

  shared_examples "successfully listing expected users" do
    it "has expected results" do
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
      expected_response = expected_users.reject(&:blank?)
      drivers_result = response.dig(*viewer_company_path, "users", "edges")

      expected_response_attributes = expected_response.map do |driver|
        {
          id: driver.to_ssid,
          fullName: driver.full_name,
          phone: driver.phone,
          company: {
            id: driver.company.to_ssid,
            name: driver.company.name,
          },
        }.deep_stringify_keys
      end
      driver_nodes = drivers_result.to_a.map { |r| r['node'] }

      expect(driver_nodes.size).to eq expected_response_attributes.size
      expect(driver_nodes).to match_array expected_response_attributes
    end
  end

  shared_examples "company.users" do
    context "when rescue_company does not have more drivers" do
      let(:expected_users) { [api_user] }

      it_behaves_like "successfully listing expected users"
    end

    context "when rescue_company has more 2 drivers" do
      let(:company_drivers) do
        [
          create(:user, roles: [:rescue, :driver], company: rescue_company, full_name: "Driver Name"),
          create(:user, roles: [:rescue, :driver], company: rescue_company, full_name: "Other Driver"),
        ]
      end
      let(:expected_users) { company_drivers + [api_user] }

      it_behaves_like "successfully listing expected users"

      context "and it also has a dispatcher" do
        let(:company_dispatcher) { create(:user, roles: [:rescue, :dispatcher], company: rescue_company, full_name: "Dispatcher Name") }
        let(:expected_users) { company_drivers + [company_dispatcher, api_user] }

        it_behaves_like "successfully listing expected users"

        context "and it also has an admin" do
          let(:company_admin) { create(:user, roles: [:rescue, :admin], company: rescue_company, full_name: "Admin Name") }
          let(:expected_users) { company_drivers + [company_dispatcher, company_admin, api_user] }

          it_behaves_like "successfully listing expected users"
        end
      end
    end

    context "when searching for users" do
      let(:first_driver) { create(:user, company: rescue_company, roles: [:rescue, :driver], full_name: 'King Duck') }
      let(:second_driver) { create(:user, company: rescue_company, roles: [:rescue, :driver], full_name: 'Donkey Kong') }
      let(:third_driver) { create(:user, company: rescue_company, roles: [:rescue, :driver], full_name: 'King Kong') }

      let!(:all_drivers) { [first_driver, second_driver, third_driver] }

      let(:king_drivers) { [first_driver, third_driver] }
      let(:kong_drivers) { [second_driver, third_driver] }

      let!(:king_dispatcher) { create(:user, company: rescue_company, roles: [:rescue, :dispatcher], full_name: 'King Duck2') }
      let!(:king_kong_admin) { create(:user, company: rescue_company, roles: [:rescue, :admin], full_name: 'King Kong2') }

      let(:another_rescue_company) { create :rescue_company }
      let!(:another_rescue_company_driver) { create(:user, company: another_rescue_company, roles: [:rescue, :driver], full_name: 'King Duck3') }
      let!(:another_rescue_company_dispatcher) { create(:user, company: another_rescue_company, roles: [:rescue, :driver], full_name: 'Donkey Kong2') }
      let!(:another_rescue_company_admin) { create(:user, company: another_rescue_company, roles: [:rescue, :admin], full_name: 'King Kong3') }

      context "by 'King'" do
        let(:query_by_name) { 'King' }
        let(:expected_users) { king_drivers + [king_dispatcher, king_kong_admin] }

        it_behaves_like "successfully listing expected users"

        context "and roles are passed as ['driver']" do
          let(:query_roles) { ["Driver"] }
          let(:expected_users) { king_drivers }

          it_behaves_like "successfully listing expected users"
        end

        context "and roles are passed as ['dispatcher']" do
          let(:query_roles) { ["Dispatcher"] }
          let(:expected_users) { [king_dispatcher] }

          it_behaves_like "successfully listing expected users"
        end

        context "and roles are passed as ['admin']" do
          let(:query_roles) { ["Admin"] }
          let(:expected_users) { [king_kong_admin] }

          it_behaves_like "successfully listing expected users"
        end

        context "and roles are passed as ['Driver', 'Dispatcher']" do
          let(:query_roles) { ["Driver", "Dispatcher"] }
          let(:expected_users) { king_drivers + [king_dispatcher] }

          it_behaves_like "successfully listing expected users"
        end
      end

      context "by 'king' lower case" do
        let(:query_by_name) { 'king' }
        let(:expected_users) { king_drivers + [king_dispatcher, king_kong_admin] }

        it_behaves_like "successfully listing expected users"
      end

      context "by 'king Ko'" do
        let(:query_by_name) { 'king Ko' }
        let(:expected_users) { [third_driver, king_kong_admin] }

        it_behaves_like "successfully listing expected users"
      end

      context "by 'ng ko'" do
        let(:query_by_name) { 'ng ko' }
        let(:expected_users) { [third_driver, king_kong_admin] }

        it_behaves_like "successfully listing expected users"
      end

      context "by 'nG KoNg'" do
        let(:query_by_name) { 'nG KoNg' }
        let(:expected_users) { [third_driver, king_kong_admin] }

        it_behaves_like "successfully listing expected users"
      end

      context "by 'Kong'" do
        let(:query_by_name) { 'Kong' }
        let(:expected_users) { kong_drivers + [king_kong_admin] }

        it_behaves_like "successfully listing expected users"
      end

      context "by 'kong' lower case" do
        let(:query_by_name) { 'kong' }
        let(:expected_users) { kong_drivers + [king_kong_admin] }

        it_behaves_like "successfully listing expected users"
      end

      context "by unexistent user" do
        let(:query_by_name) { 'unexistent will have no result' }
        let(:expected_users) { [] }

        it_behaves_like "successfully listing expected users"
      end

      context "when query term is empty" do
        let(:query_by_name) { " " }

        let(:expected_users) { [] }

        it_behaves_like "successfully listing expected users"
      end

      context "when search term has 1 char" do
        let(:query_by_name) { "u" }

        let(:expected_users) { [api_user, first_driver, king_dispatcher] }

        it_behaves_like "successfully listing expected users"
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "company.users"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "company.users"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "company.users"
  end
end
