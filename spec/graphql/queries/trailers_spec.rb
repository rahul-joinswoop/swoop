# frozen_string_literal: true

require 'rails_helper'

describe "Trailers Query" do
  let(:other_rescue_company) { create :rescue_company }
  let(:other_fleet_company) { create :fleet_managed_company }

  context "trailers" do
    # vehicles connected to jobs with these statuses shouldn't show up in our
    # search results
    let(:fleet_job_bad_status) do
      create :fleet_managed_job,
             :with_rescue_vehicle,
             :with_trailer_vehicle,
             fleet_company: fleet_managed_company,
             rescue_company: rescue_company,
             status: Job.fleet_no_publish_states.sample
    end
    # vehicles connected to these jobs should show up
    let(:fleet_jobs) do
      create_list :fleet_managed_job,
                  2,
                  :with_rescue_vehicle,
                  :with_trailer_vehicle,
                  fleet_company: fleet_managed_company,
                  rescue_company: rescue_company,
                  status: Job.fleet_show_rescue_vehicle_states.sample
    end
    # this job has no rescue vehicle so they shouldn't affect anything
    let(:fleet_job_no_vehicle) do
      create :fleet_managed_job,
             fleet_company: fleet_managed_company,
             rescue_company: rescue_company,
             status: Job.fleet_show_rescue_vehicle_states.sample
    end
    # these vehicles shouldn't show up because they belong to a different fleet
    # company
    let(:other_fleet_job) do
      create :fleet_managed_job,
             :with_rescue_vehicle,
             :with_trailer_vehicle,
             fleet_company: swoop,
             rescue_company: other_rescue_company,
             status: Job.fleet_show_rescue_vehicle_states.sample
    end

    # these vehicles shouldn't show up because they belong to a different fleet
    # company
    let(:other_fleet_job_bad_status) do
      create :fleet_managed_job,
             :with_rescue_vehicle,
             :with_trailer_vehicle,
             fleet_company: swoop,
             rescue_company: rescue_company,
             status: Job.fleet_no_publish_states.sample
    end

    let!(:all_fleet_jobs) do
      [other_fleet_job, other_fleet_job_bad_status, fleet_job_no_vehicle, fleet_job_bad_status] + fleet_jobs
    end

    shared_examples "trailers" do
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            viewer {
              ...on Application {
                company {
                  ...companyFields
                }
              }
              ...on User {
                company {
                  ...companyFields
                }
              }
              ...on Company {
                ...companyFields
              }
            }
          }
          fragment companyFields on Company {
            trailers {
              edges {
                node {
                  id
                  name
                  make
                  model
                  category {
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expected_response = expected_vehicles.map do |v|
          {
            id: v.to_ssid,
            name: v.name,
            make: v.make,
            model: v.model,
            category: {
              name: v.vehicle_category.name,
            },
          }.deep_stringify_keys
        end
        trailers_response = response.dig(*viewer_company_path, 'trailers', 'edges').to_a.map { |r| r['node'] }

        # everything else should match
        expect(trailers_response.length).to eq(expected_response.length)
        expect(trailers_response).to contain_exactly(*expected_response)
      end
    end

    shared_examples "trailer node" do
      let(:query) do
        <<~GRAPHQL
        query #{operation_name} {
          node(id: #{node.to_ssid.inspect}) {
            id
          }
        }
      GRAPHQL
      end

      context "with a valid node" do
        let(:node) { expected_vehicles.sample }

        it "works" do
          skip "no valid nodes" if node.blank?
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(response.dig("data", "node", "id")).to eq(node.to_ssid)
        end
      end

      context "with an invalid node" do
        let(:node) { (all_fleet_jobs.map(&:trailer_vehicle).select(&:present?) - expected_vehicles).sample }

        it "works" do
          skip "no invalid nodes" if node.blank?
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(response.dig("data", "node", "id")).to be_blank
        end
      end
    end

    context "as a client api_application" do
      include_context "client api_application"
      let(:expected_vehicles) { [] }

      before do
        api_company.features << create(:feature, name: Feature::TRAILERS)
      end

      it_behaves_like "trailers"
      it_behaves_like "trailer node"
    end

    context 'partner' do
      let(:expected_vehicles) do
        (fleet_jobs + [fleet_job_bad_status, other_fleet_job_bad_status]).map(&:trailer_vehicle)
      end

      context "as a partner api_user" do
        include_context "partner api_user"
        it_behaves_like "trailers"
        it_behaves_like "trailer node"
      end

      context "as a 3rd party partner api_user" do
        include_context "3rd party partner api_user"
        it_behaves_like "trailers"
        it_behaves_like "trailer node"
      end

      context "as a 3rd party partner api_company" do
        include_context "3rd party partner api_company"
        it_behaves_like "trailers"
        it_behaves_like "trailer node"
      end
    end
  end

  context "search" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($query: String) {
          viewer {
            ...on Application {
              company {
                ...trailerFields
              }
            }
            ...on User {
              company {
                ...trailerFields
              }
            }
            ...on Company {
              ...trailerFields
            }

          }
        }
        fragment trailerFields on Company {
          trailers(query: $query) {
            edges {
              node {
                id
                name
              }
            }
          }
        }
      GRAPHQL
    end
    let(:user) { create(:user, company: rescue_company, roles: [:rescue, :driver], full_name: "RescueCompany User Name") }
    let(:query_by_name) { nil }
    let(:variables) { { query: query_by_name } }
    let!(:trailer_swoop) { create(:trailer_vehicle, company: rescue_company, name: 'Swoop') }
    let!(:trailer_cash_call) { create(:trailer_vehicle, company: rescue_company, name: 'Cash Call') }
    let!(:trailer_fleet) { create(:trailer_vehicle, company: rescue_company, name: 'Fleet Company') }
    let!(:trailer_pacoca) { create(:trailer_vehicle, company: rescue_company, name: 'Paçoca') }
    let!(:trailer_deleted) { create(:trailer_vehicle, company: rescue_company, name: 'Deleted Account', deleted_at: Time.now) }

    let(:expected_trailers) { [trailer_swoop, trailer_cash_call, trailer_fleet, trailer_pacoca] }

    shared_examples 'trailer search' do
      shared_examples "successfully listing expected trailers" do
        it "has expected results" do
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')

          expected_response = expected_trailers || {}
          trailers_result = response.dig(*viewer_company_path, "trailers", "edges")

          expected_response_attributes = expected_response.map do |trailer|
            {
              id: trailer.to_ssid,
              name: trailer.name,
            }.deep_stringify_keys
          end
          trailer_nodes = trailers_result.to_a.map { |r| r['node'] }

          expect(trailer_nodes.size).to eq expected_response_attributes.size
          expect(trailer_nodes).to match_array expected_response_attributes
        end
      end

      it_behaves_like "successfully listing expected trailers"

      context "when querying for trailers" do
        context "by 'Cash'" do
          let(:query_by_name) { 'Cash' }
          let(:expected_trailers) { [trailer_cash_call] }

          it_behaves_like "successfully listing expected trailers"
        end

        context "by 'cash' lower case" do
          let(:query_by_name) { 'cash' }
          let(:expected_trailers) { [trailer_cash_call] }

          it_behaves_like "successfully listing expected trailers"
        end

        context "by 'Swoop'" do
          let(:query_by_name) { 'Swoop' }
          let(:expected_trailers) { [trailer_swoop] }

          it_behaves_like "successfully listing expected trailers"
        end

        context "by 'swoop' lower case" do
          let(:query_by_name) { 'swoop' }
          let(:expected_trailers) { [trailer_swoop] }

          it_behaves_like "successfully listing expected trailers"
        end

        context "by 'ç' lower case" do
          let(:query_by_name) { 'ç' }
          let(:expected_trailers) { [trailer_pacoca] }

          it_behaves_like "successfully listing expected trailers"
        end

        context "by a deleted trailer name" do
          let(:query_by_name) { 'Deleted' }
          let(:expected_trailers) { [] }

          it_behaves_like "successfully listing expected trailers"
        end

        context "when query term is empty" do
          let(:query_by_name) { " " }

          let(:expected_trailers) { [] }

          it_behaves_like "successfully listing expected trailers"
        end

        context "when querying for a deleted trailer" do
          let(:query_by_name) { trailer_deleted.name }

          let(:expected_trailers) { [] }

          it_behaves_like "successfully listing expected trailers"
        end
      end
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "trailer search"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "trailer search"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "trailer search"
    end
  end
end
