# frozen_string_literal: true

require 'rails_helper'

describe "Jobs Query Resolver sync with Type", freeze_time: Time.current.round do
  let(:query) do
    <<-GRAPHQL
      fragment JobsFields on JobConnection {
        edges {
          node {
            id
             __typename
          }
          cursor
        }
      }

      query #{operation_name}($filters: JobsFiltersInput = {states: Active}) {
        viewer {
          __typename
          ...on User {
            company {
              id
              jobs(filters: $filters) {
                ...JobsFields
              }
            }
          }
          ...on Application {
            company {
              id
              jobs(filters: $filters) {
                ...JobsFields
              }
            }
          }
          ...on Company {
            id
            jobs(filters: $filters) {
              ...JobsFields
            }
          }
        }
      }
    GRAPHQL
  end

  let(:variables) { { filters: { states: 'Done' } } }
  let(:other_rescue_company) { create(:rescue_company) }
  let(:job) do
    create :rescue_job,
           :completed,
           rescue_company: other_rescue_company,
           fleet_company: fleet_managed_company,
           bids: 3
  end
  let(:auction) do
    create :auction, {
      expires_at: 20.minutes.ago,
      created_at: 21.minutes.ago,
      status: Auction::Auction::SUCCESSFUL,
      job: job,
    }
  end
  let!(:bid) do
    create :bid, {
      job: job,
      company: rescue_company,
      status: Auction::Bid::SUBMITTED,
      auction: auction,
    }
  end
  let(:job_edges) { response.to_h.dig(*viewer_company_path, 'jobs', 'edges') }

  # These tests are intended to check that there is some synchronization between the logic in the
  # jobs resolver and JobType.authorized?

  shared_examples 'returns the job' do
    it 'works' do
      expect(job_edges.present?).to be true
      expect(job_edges.pluck('node').any?(&:nil?)).to be false
      expect(job_edges.first.dig('node', 'id')).to eq(bid.job_ssid)
    end
  end

  shared_examples "doesn't return the job" do
    it 'has no edge' do
      expect(job_edges.present?).to be false
    end
  end

  shared_examples 'old bid' do
    context 'for a driver' do
      let(:api_user) { rescue_driver }

      it_behaves_like "doesn't return the job"
    end

    context 'for an admin' do
      let(:api_user) { rescue_admin }

      it_behaves_like "doesn't return the job"
    end

    context 'for a disatcher' do
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "doesn't return the job"
    end
  end

  shared_examples 'recent bid' do
    before { auction.update_columns(expires_at: 10.minutes.ago) }

    context 'for a driver' do
      let(:api_user) { rescue_driver }

      it_behaves_like "doesn't return the job"
    end

    context 'for an admin' do
      let(:api_user) { rescue_admin }

      it_behaves_like 'returns the job'
    end

    context 'for a disatcher' do
      let(:api_user) { rescue_dispatcher }

      it_behaves_like 'returns the job'
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "old bid"
    it_behaves_like "recent bid"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "old bid"
    it_behaves_like "recent bid"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"

    context "old bid" do
      it_behaves_like "doesn't return the job"
    end

    context 'recent bid' do
      before { auction.update_columns(expires_at: 10.minutes.ago) }

      it_behaves_like 'returns the job'
    end
  end
end
