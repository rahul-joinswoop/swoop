# frozen_string_literal: true

require 'rails_helper'

describe "Job Partner Query" do
  let(:job) do
    create :fleet_managed_job,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end

  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        job(id: $id) {
          id
          partner {
            name
            phone
            company {
              name
              phone
            }
          }
        }
      }
    GRAPHQL
  end

  let(:variables) { { id: job.to_ssid } }
  let(:deprecated_partner_name) { response.dig('data', 'job', 'partner', 'name') }
  let(:deprecated_partner_phone) { response.dig('data', 'job', 'partner', 'phone') }
  let(:partner_name) { response.dig('data', 'job', 'partner', 'company', 'name') }
  let(:partner_phone) { response.dig('data', 'job', 'partner', 'company', 'phone') }
  let(:id) { response.dig('data', 'job', 'id') }

  context "as an assigned partner company" do
    shared_examples 'partner fields' do
      it "works" do
        expect(id).to be_present
        expect(partner_name).to be_present
        expect(partner_phone).to be_present
        expect(deprecated_partner_name).to be_present
        expect(deprecated_partner_phone).to be_present
      end
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "partner fields"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "partner fields"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "partner fields"
    end
  end

  context "as a losing bidding partner company" do
    shared_examples "hidden partner fields" do
      let(:another_rescue_company) { create :rescue_company }
      let(:job) do
        create(:fleet_managed_job,
               :with_auction,
               :with_service_location,
               :with_drop_location,
               status: :assigned,
               fleet_company: fleet_managed_company,
               rescue_company: another_rescue_company,
               bids: 3).tap do |j|
          j.auction.update! status: Auction::Auction::LIVE
          j.auction.bids << create(:bid, auction: j.auction, job: j, company: rescue_company)
        end
      end

      it "works" do
        # we can see the job because we still have some permissions on it but we can't actually see anything in job.partner
        expect(id).to be_present
        expect(partner_name).to be_blank
        expect(partner_phone).to be_blank
        expect(deprecated_partner_name).to be_blank
        expect(deprecated_partner_phone).to be_blank
      end
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "hidden partner fields"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      let(:api_user) { rescue_dispatcher }

      it_behaves_like "hidden partner fields"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "hidden partner fields"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"

    it "works" do
      expect(id).to be_present
      expect(partner_name).to be_present
      expect(partner_phone).to be_present
      expect(deprecated_partner_name).to be_present
      expect(deprecated_partner_phone).to be_present
    end
  end
end
