# frozen_string_literal: true

require 'rails_helper'

describe "User Jobs Query" do
  let(:other_rescue_driver) { create :user, :rescue, :driver, company: rescue_company }
  let!(:job1) { create :job, :with_rescue_vehicle, rescue_driver: rescue_driver, rescue_company: rescue_company, status: :unassigned }
  let!(:job2) { create :job, :with_rescue_vehicle, rescue_driver: rescue_dispatcher_driver, rescue_company: rescue_company, status: :unassigned }
  let!(:job3) { create :job, :with_rescue_vehicle, rescue_driver: other_rescue_driver, rescue_company: rescue_company, status: :unassigned }

  let(:query) do
    <<~GRAPHQL
      query #{operation_name} {
        node(id: #{query_user.to_ssid.inspect}) {
          ...on User {
            jobs {
              edges {
                node {
                  id
                }
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let(:response_ids) do
    response.dig("data", "node", "jobs", "edges")&.map { |e| e.dig("node", "id") }
  end

  shared_examples "it returns jobs" do
    it "works" do
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
      expect(response_ids).to eq(expected_job_ids)
    end
  end

  shared_examples "it works" do
    context "querying on rescue_driver" do
      let(:query_user) { rescue_driver }
      let(:expected_job_ids) { expected_rescue_driver_job_ids }

      it_behaves_like "it returns jobs"
    end

    context "querying on other_rescue_driver" do
      let(:query_user) { other_rescue_driver }
      let(:expected_job_ids) { expected_other_rescue_driver_job_ids }

      it_behaves_like "it returns jobs"
    end

    context "querying on rescue_dispatcher_driver" do
      let(:query_user) { rescue_dispatcher_driver }
      let(:expected_job_ids) { expected_rescue_dispatcher_driver_job_ids }

      it_behaves_like "it returns jobs"
    end

    context "querying on rescue_admin" do
      let(:query_user) { rescue_admin }
      let(:expected_job_ids) { expected_rescue_admin_job_ids }

      it_behaves_like "it returns jobs"
    end
  end

  shared_examples "as a rescue user" do
    context "with rescue_driver" do
      let(:api_user) { rescue_driver }
      let(:expected_rescue_driver_job_ids) { [job1].map(&:to_ssid) }
      let(:expected_rescue_dispatcher_driver_job_ids) { [] }
      let(:expected_other_rescue_driver_job_ids) { [] }
      let(:expected_rescue_admin_job_ids) { [] }

      it_behaves_like "it works"
    end

    context "with rescue_dispatcher_driver" do
      let(:api_user) { rescue_dispatcher_driver }
      let(:expected_rescue_driver_job_ids) { [job1].map(&:to_ssid) }
      let(:expected_rescue_dispatcher_driver_job_ids) { [job2].map(&:to_ssid) }
      let(:expected_other_rescue_driver_job_ids) { [job3].map(&:to_ssid) }
      let(:expected_rescue_admin_job_ids) { [] }

      it_behaves_like "it works"
    end

    context "with rescue_admin" do
      let(:api_user) { rescue_admin }
      let(:expected_rescue_driver_job_ids) { [job1].map(&:to_ssid) }
      let(:expected_rescue_dispatcher_driver_job_ids) { [job2].map(&:to_ssid) }
      let(:expected_other_rescue_driver_job_ids) { [job3].map(&:to_ssid) }
      let(:expected_rescue_admin_job_ids) { [] }

      it_behaves_like "it works"
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "as a rescue user"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "as a rescue user"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    let(:expected_rescue_driver_job_ids) { [job1].map(&:to_ssid) }
    let(:expected_rescue_dispatcher_driver_job_ids) { [job2].map(&:to_ssid) }
    let(:expected_other_rescue_driver_job_ids) { [job3].map(&:to_ssid) }
    let(:expected_rescue_admin_job_ids) { [] }

    it_behaves_like "it works"
  end
end
