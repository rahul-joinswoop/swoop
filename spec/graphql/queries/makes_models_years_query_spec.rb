# frozen_string_literal: true

require 'rails_helper'

describe "Makes Models Years Queries" do
  shared_examples "makes models years" do
    context "by id" do
      let(:makes_query) do
        <<~GRAPHQL
          query #{operation_name}{
            vehicle {
              makes {
                edges {
                  node {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      let(:models_query) do
        <<~GRAPHQL
          query #{operation_name}($makeId: ID){
            vehicle {
              make(id: $makeId) {
                name
                models {
                  edges {
                    node {
                      id
                      name
                    }
                  }
                }
              }
            }
          }
          GRAPHQL
      end

      let(:years_query) do
        <<~GRAPHQL
          query #{operation_name}($makeId: ID, $modelId: ID){
            vehicle {
              make(id: $makeId) {
                name
                model(id: $modelId) {
                  name
                  years {
                    edges {
                      node {
                        year
                      }
                    }
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      let!(:makes) { create_list :vehicle_make, 3, :random, :with_models }

      it "works" do
        # we didn't blow up
        makes_response = execute query: makes_query,
                                 context: context,
                                 variables: variables,
                                 operation_name: operation_name,
                                 only: only_filter,
                                 root_value: context[:api_company]
        expect(makes_response).not_to include('errors')
        make_id = makes_response.dig('data', 'vehicle', 'makes', 'edges', 0, 'node', 'id')

        models_response = execute query: models_query,
                                  context: context,
                                  variables: { makeId: make_id },
                                  operation_name: operation_name,
                                  only: only_filter,
                                  root_value: context[:api_company]
        expect(models_response).not_to include('errors')
        model_id = models_response.dig('data', 'vehicle', 'make', 'models', 'edges', 0, 'node', 'id')

        years_response = execute query: years_query,
                                 context: context,
                                 variables: { makeId: make_id, modelId: model_id },
                                 operation_name: operation_name,
                                 only: only_filter,
                                 root_value: context[:api_company]
        expect(years_response).not_to include('errors')
      end
    end

    context "by name" do
      let(:makes_query) do
        <<~GRAPHQL
          query #{operation_name}{
            vehicle {
              makes {
                edges {
                  node {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      let(:models_query) do
        <<~GRAPHQL
          query #{operation_name}($makeName: String){
            vehicle {
              make(name: $makeName) {
                name
                models {
                  edges {
                    node {
                      id
                      name
                    }
                  }
                }
              }
            }
          }
          GRAPHQL
      end

      let(:years_query) do
        <<~GRAPHQL
          query #{operation_name}($makeName: String, $modelName: String){
            vehicle {
              make(name: $makeName) {
                name
                model(name: $modelName) {
                  name
                  years {
                    edges {
                      node {
                        year
                      }
                    }
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      let!(:makes) { create_list :vehicle_make, 3, :random, :with_models }

      it "works" do
        # we didn't blow up
        makes_response = execute query: makes_query,
                                 context: context,
                                 variables: variables,
                                 operation_name: operation_name,
                                 only: only_filter,
                                 root_value: context[:api_company]
        expect(makes_response).not_to include('errors')
        make_name = makes_response.dig('data', 'vehicle', 'makes', 'edges', 0, 'node', 'name')

        models_response = execute query: models_query,
                                  context: context,
                                  variables: { makeName: make_name },
                                  operation_name: operation_name,
                                  only: only_filter,
                                  root_value: context[:api_company]
        expect(models_response).not_to include('errors')
        model_name = models_response.dig('data', 'vehicle', 'make', 'models', 'edges', 0, 'node', 'name')

        years_response = execute query: years_query,
                                 context: context,
                                 variables: { makeName: make_name, modelName: model_name },
                                 operation_name: operation_name,
                                 only: only_filter,
                                 root_value: context[:api_company]
        expect(years_response).not_to include('errors')
      end
    end

    context "with query string" do
      let(:makes_query) do
        <<~GRAPHQL
          query #{operation_name}($query: String){
            vehicle{
              makes(query: $query)  {
                edges {
                  node {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      let(:models_query) do
        <<~GRAPHQL
          query #{operation_name}($makeName: String, $query: String){
            vehicle {
              make(name: $makeName) {
                name
                models(query: $query) {
                  edges {
                    node {
                      id
                      name
                    }
                  }
                }
              }
            }
          }
          GRAPHQL
      end

      let(:years_query) do
        <<~GRAPHQL
          query #{operation_name}($makeName: String, $modelName: String, $query: String){
            vehicle {
              make(name: $makeName) {
                name
                model(name: $modelName) {
                  name
                  years(query: $query) {
                    edges {
                      node {
                        year
                      }
                    }
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      let!(:makes) { create_list :vehicle_make, 3, :random, :with_models }

      it "works" do
        # we didn't blow up
        makes_response = execute query: makes_query,
                                 context: context,
                                 variables: { query: makes.first.name.slice(0..2) },
                                 operation_name: operation_name,
                                 only: only_filter,
                                 root_value: context[:api_company]

        expect(makes_response).not_to include('errors')
        make_name = makes_response.dig('data', 'vehicle', 'makes', 'edges', 0, 'node', 'name')

        models_response = execute query: models_query,
                                  context: context,
                                  variables: { makeName: make_name, query: makes.first.vehicle_models.first.name.slice(0..2) },
                                  operation_name: operation_name,
                                  only: only_filter,
                                  root_value: context[:api_company]

        expect(models_response).not_to include('errors')
        model_name = models_response.dig('data', 'vehicle', 'make', 'models', 'edges', 0, 'node', 'name')

        years_response = execute query: years_query,
                                 context: context,
                                 variables: { makeName: make_name, modelName: model_name, query: makes.first.vehicle_models.first.years.first.to_s.slice(0..2) },
                                 operation_name: operation_name,
                                 only: only_filter,
                                 root_value: context[:api_company]

        expect(years_response).not_to include('errors')
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "makes models years"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "makes models years"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "makes models years"
  end
end
