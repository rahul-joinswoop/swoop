# frozen_string_literal: true

require 'rails_helper'

describe "Viewer Query" do
  context "viewer" do
    def build_roles_edges(roles)
      roles.map do |r|
        {
          node: {
            id: r.to_ssid,
            name: r.name,
            description: Role::NAME_TO_DESCRIPTION_MAP[Role::STATUS_TO_NAME_MAP[r.name.to_sym]],
          },
        }
      end
    end
    let(:encoded_user_id) { api_user.to_ssid }
    let(:encoded_company_id) { api_company.to_ssid }

    shared_examples 'application' do
      let(:grantable_roles) { build_roles_edges(Role.where(name: SwoopAuthenticator::ALL_FLEET_ROLES)) }
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            viewer {
              ...on Application {
                grantableRoles {
                  edges {
                    node {
                      id
                      name
                      description
                    }
                  }
                }
                company {
                  id
                  name
                  type
                  location {
                    address
                    lat
                    lng
                    locationType
                    googlePlaceId
                  }
                  logoUrl
                  phone
                  fax
                  primaryContact {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      it "matches output" do
        expect(response).not_to include('errors')
        viewer_response = response.dig('data', 'viewer')
        expect(viewer_response).to deep_include({
          grantable_roles: {
            edges: grantable_roles,
          },
          company: {
            id: encoded_company_id,
            fax: api_company.fax,
            logo_url: api_company.logo_url,
            name: api_company.name,
            phone: api_company.phone,
            location: api_company.location,
            type: api_company.graphql_type,
          },
        }.deep_transform_keys { |k| k.to_s.camelize(:lower) })
      end
    end
    shared_examples 'user' do
      let!(:vehicle) { create :vehicle, company: api_company }
      let!(:api_user) { super().tap { |u| u.update! vehicle: vehicle } }
      let(:user_roles_edges) { build_roles_edges api_user.roles }
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            viewer {
              ...on User {
                id
                firstName
                lastName
                fullName
                userName
                email
                phone
                deletedAt
                language
                onDuty
                vehicle {
                  id
                }
                grantableRoles {
                  edges {
                    node {
                      id
                      name
                      description
                    }
                  }
                }
                roles {
                  edges {
                    node {
                      id
                      name
                      description
                    }
                  }
                }
                company {
                  id
                  dropoffWaiver
                  pickupWaiver
                }
              }
            }
          }
        GRAPHQL
      end

      it "matches output" do
        expect(response).not_to include('errors')
        viewer_response = response.dig('data', 'viewer')
        # the id that comes back should be our user we called with
        expect(viewer_response).to deep_include({
          id: api_user.to_ssid,
          full_name: api_user.full_name,
          first_name: api_user.first_name,
          last_name: api_user.last_name,
          user_name: api_user.username,
          email: api_user.email,
          phone: api_user.phone,
          deleted_at: api_user.deleted_at,
          language: api_user.language,
          on_duty: api_user.on_duty,
          company: {
            id: api_user.company.to_ssid,
            dropoff_waiver: api_company.inherited_dropoff_waiver,
            pickup_waiver: api_company.inherited_pickup_waiver,
          },
          vehicle: {
            id: api_user.vehicle.to_ssid,
          },
          roles: {
            edges: user_roles_edges,
          },
          grantable_roles: {
            edges: build_roles_edges(Role.where(name: SwoopAuthenticator::ALL_RESCUE_ROLES)),
          },
        }.deep_transform_keys { |k| k.to_s.camelize(:lower) })
      end
    end

    shared_examples "company" do
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            viewer {
              ...on Company {
                id
                name
                type
                location {
                  address
                  lat
                  lng
                  locationType
                  googlePlaceId
                }
                logoUrl
                currency
                distanceUnit
                language
                phone
                fax
                primaryContact {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end

      it "matches output" do
        expect(response).not_to include('errors')
        viewer_response = response.dig('data', 'viewer')
        expect(viewer_response).to deep_include({
          id: api_company.to_ssid,
          currency: api_company.currency,
          distance_unit: api_company.distance_unit,
          language: api_company.language,
          fax: api_company.fax,
          logo_url: api_company.logo_url,
          name: api_company.name,
          phone: api_company.phone,
          location: api_company.location,
          type: api_company.graphql_type,
        }.deep_transform_keys { |k| k.to_s.camelize(:lower) })
      end
    end

    context "as a client api_application" do
      include_context "client api_application"
      it_behaves_like 'application'
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      let(:api_user) { rescue_admin }

      it_behaves_like "user"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      let(:api_user) { rescue_admin }

      it_behaves_like "user"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "company"
    end
  end

  context "services -> questions -> answer tree" do
    let(:services_query) do
      <<~GRAPHQL
        query #{operation_name}{
          services {
            edges {
              node {
                id
                name
              }
            }
          }
        }
        GRAPHQL
    end
    let(:questions_query) do
      <<~GRAPHQL
        query #{operation_name}($serviceId: ID!){
          service(id: $serviceId) {
            name
            questions {
              edges {
                node {
                  id
                  question
                }
              }
            }
          }
        }
        GRAPHQL
    end

    let(:answers_query) do
      <<~GRAPHQL
        query #{operation_name}($serviceId: ID!, $questionId: ID!){
          service(id: $serviceId) {
            name
            question(id: $questionId) {
              question
              answers {
                edges {
                  node {
                    answer
                    needsExtra
                  }
                }
              }
            }
          }
        }
        GRAPHQL
    end

    let(:show_errors) { false }

    shared_examples "service question answer" do
      it "works" do
        # we didn't blow up
        services_response = execute(
          query: services_query,
          context: context,
          variables: variables,
          operation_name: operation_name,
          only: only_filter,
          root_value: api_company
        )
        expect(services_response).not_to include('errors')

        service_id = services_response.dig('data', 'services', 'edges', 0, 'node', 'id')
        questions_response = execute(
          query: questions_query,
          context: context,
          variables: { serviceId: service_id },
          operation_name: operation_name,
          only: only_filter,
          root_value: api_company
        )
        expect(questions_response).not_to include('errors')

        question_id = questions_response.dig('data', 'service', 'questions', 'edges', 0, 'node', 'id')
        answers_response = execute(
          query: answers_query,
          context: context,
          variables: { serviceId: service_id, questionId: question_id },
          operation_name: operation_name,
          only: only_filter,
          root_value: api_company
        )

        # but we don't support questions to rescue_companies, so we bypass the validation.
        if !api_company.rescue?
          expect(answers_response).not_to include('errors')
        end
      end
    end

    context "as a client api_application" do
      include_context "client api_application"
      it_behaves_like "service question answer"
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "service question answer"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "service question answer"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "service question answer"
    end
  end

  describe "stats" do
    let(:query) do
      <<~GRAPHQL
        #{stats_fragment}
        #{application_fragment}
        query #{operation_name} {
          viewer {
            ...on Application {
              ...ApplicationFragment
            }
          }
        }
      GRAPHQL
    end

    shared_examples 'it works' do
      it 'works' do
        expect(response).not_to include('errors')
        expect(response.dig(*path)).to eq(expected_edges)
      end
    end

    describe 'deprecatedFields' do
      let!(:deprecated_fields) { create :graphql_stats_deprecated_fields, target: target }
      let(:expected_edges) do
        deprecated_fields.map do |date, values|
          {
            node: {
              date: date.iso8601,
              fields: {
                edges: values.map do |field, count|
                  {
                    node: {
                      field: field,
                      reason: SwoopSchema.deprecation_reason_for(field),
                      count: count.to_i,
                    },
                  }
                end,
              },
            },
          }.deep_stringify_keys
        end
      end

      let(:stats_fragment) do
        <<~GRAPHQL
          fragment StatsFragment on DeprecatedFieldsStats {
            daily(first: 30) {
              edges {
                node {
                  date
                  fields {
                    edges {
                      node {
                        field
                        reason
                        count
                      }
                    }
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      context "on an application" do
        include_context "client api_application"

        let(:application_fragment) do
          <<~GRAPHQL
            fragment ApplicationFragment on Application {
              stats {
                deprecatedFields {
                  ...StatsFragment
                }
              }
            }
          GRAPHQL
        end

        let(:target) { api_application }
        let(:path) { ['data', 'viewer', 'stats', 'deprecatedFields', 'daily', 'edges'] }

        it_behaves_like "it works"
      end

      context "on Swoop" do
        let(:application_fragment) do
          <<~GRAPHQL
            fragment ApplicationFragment on Application {
              company {
                globalStats {
                  deprecatedFields {
                    ...StatsFragment
                  }
                }
              }
            }
          GRAPHQL
        end

        let(:api_company) { swoop }
        # TODO - why doesn't swoop_application work here?
        let(:api_application) { create :oauth_application, owner: api_company }
        let(:viewer) { api_application }
        let(:target) { api_company }
        let(:path) { ['data', 'viewer', 'company', 'globalStats', 'deprecatedFields', 'daily', 'edges'] }

        it_behaves_like "it works"

        describe "mobile app" do
          let(:application_fragment) do
            <<~GRAPHQL
              fragment ApplicationFragment on Application {
                company {
                  mobileApp {
                    stats {
                      deprecatedFields {
                        ...StatsFragment
                      }
                    }
                  }
                }
              }
            GRAPHQL
          end
          let(:path) { ['data', 'viewer', 'company', 'mobileApp', 'stats', 'deprecatedFields', 'daily', 'edges'] }
          let(:target) { SuperCompany::IGNORE_ME_I_AM_A_FAKE_MOBILE_APP }

          it_behaves_like "it works"
        end
      end
    end

    describe 'requests' do
      let!(:requests) { create :graphql_stats_requests, target: target }
      let(:expected_edges) do
        requests.map do |date, count|
          {
            node: {
              date: date.iso8601,
              requests: count.to_i,
            },
          }.deep_stringify_keys
        end
      end

      let(:stats_fragment) do
        <<~GRAPHQL
          fragment StatsFragment on RequestStats {
            daily(first: 30) {
              edges {
                node {
                  date
                  requests
                }
              }
            }
          }
        GRAPHQL
      end

      before do
        # if we let this run in our schema then our stats will increase for the request we're making now
        # which makes comparisons more difficult
        allow(Utils::Stats).to receive(:call).and_return(true)
      end

      context "on an application" do
        include_context 'client api_application'

        let(:application_fragment) do
          <<~GRAPHQL
            fragment ApplicationFragment on Application {
              stats {
                requests {
                  ...StatsFragment
                }
              }
            }
          GRAPHQL
        end

        let(:target) { api_application }
        let(:path) { ['data', 'viewer', 'stats', 'requests', 'daily', 'edges'] }

        it_behaves_like "it works"
      end

      context "on Swoop" do
        let(:application_fragment) do
          <<~GRAPHQL
            fragment ApplicationFragment on Application {
              company {
                globalStats {
                  requests {
                    ...StatsFragment
                  }
                }
              }
            }
          GRAPHQL
        end

        let(:api_company) { swoop }
        # TODO - why doesn't swoop_application work here?
        let(:api_application) { create :oauth_application, owner: api_company }
        let(:viewer) { api_application }
        let(:target) { api_company }
        let(:path) { ['data', 'viewer', 'company', 'globalStats', 'requests', 'daily', 'edges'] }

        it_behaves_like "it works"

        describe 'mobile app' do
          let(:application_fragment) do
            <<~GRAPHQL
              fragment ApplicationFragment on Application {
                company {
                  mobileApp {
                    stats {
                      requests {
                        ...StatsFragment
                      }
                    }
                  }
                }
              }
            GRAPHQL
          end

          let(:path) { ['data', 'viewer', 'company', 'mobileApp', 'stats', 'requests', 'daily', 'edges'] }
          let(:target) { SuperCompany::IGNORE_ME_I_AM_A_FAKE_MOBILE_APP }

          it_behaves_like "it works"
        end
      end
    end
  end
end
