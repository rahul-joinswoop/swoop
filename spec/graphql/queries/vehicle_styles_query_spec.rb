# frozen_string_literal: true

require 'rails_helper'

describe "VehicleStyleTypes Query" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name} {
        vehicle {
          styles {
            edges {
              node {
                id
                name
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let!(:styles) { create_list :vehicle_type, 3 }

  let(:variables) { {} }

  shared_context "styles" do
    it_behaves_like "does not raise error"

    it 'returns expected class types' do
      expected_result = styles.map { |st| { "id" => st.to_ssid, "name" => st.name } }

      query_result = response.dig("data", "vehicle", "styles", "edges")
      nodes = query_result.to_a.map { |r| r['node'] }

      expect(nodes).to match_array expected_result
    end
    context "with a query variable" do
      let(:variables) { { query: styles.first.name.slice(0..2) } }
      let(:query) do
        <<~GRAPHQL
          query #{operation_name}($query: String) {
            vehicle {
              styles(query: $query) {
                edges {
                  node {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      it 'returns expected class types' do
        expected_result = { "id" => styles.first.to_ssid, "name" => styles.first.name }
        query_result = response.dig("data", "vehicle", "styles", "edges")
        nodes = query_result.to_a.map { |r| r['node'] }
        expect(nodes).to include(expected_result)
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "styles"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "styles"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "styles"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "styles"
  end
end
