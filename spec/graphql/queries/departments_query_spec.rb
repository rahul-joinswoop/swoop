# frozen_string_literal: true

require 'rails_helper'

describe "Departments Query" do
  let(:query) do
    <<~GRAPHQL
      fragment companyFields on Company {
        departments {
          edges {
            node {
              id
              name
            }
          }
        }
      }

      query #{operation_name} {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
    GRAPHQL
  end
  let!(:departments) { create_list :department, 3, company: api_company }
  let(:another_company) { create(:rescue_company) }
  let!(:another_company_departments) { create_list :department, 3, company: another_company }
  let(:variables) { {} }

  shared_examples "departments" do
    before do
      api_company.features << create(:feature, name: Feature::DEPARTMENTS)
    end

    it 'returns expected departments' do
      expect(response).not_to include('errors')
      expected_result = departments.map { |st| { "id" => st.to_ssid, "name" => st.name } }
      path = [*viewer_company_path, "departments", "edges"]
      query_result = response.dig(*path)
      nodes = query_result.to_a.map { |r| r['node'] }

      expect(nodes.size).to eq expected_result.size
      expect(nodes).to match_array expected_result
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "departments"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "departments"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "departments"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "departments"
  end
end
