# frozen_string_literal: true

require 'rails_helper'

describe "Pagination Query" do
  shared_examples 'pagination' do
    before do
      api_company.services << services
    end

    let(:services) do
      # we want 3 pages of service codes when we're paginating with our max value (which by default is 20).
      sc_count = SwoopSchema.default_max_page_size * 2 + 1

      ServiceCode::SERVICE_CODES.keys
        .first(sc_count)
        .map { |s| create(:service_code, name: s) }
    end

    def query(args = '')
      <<~GRAPHQL
        query #{operation_name} {
          services#{args} {
            totalCount
            pageInfo {
              hasNextPage
              hasPreviousPage
              startCursor
              endCursor
            }
            edges {
              cursor
              node {
                id
                name
              }
            }
          }
        }
      GRAPHQL
    end

    def get_page_info(response)
      response.dig('data', 'services', 'pageInfo').symbolize_keys
    end

    def get_total_count(response)
      response.dig('data', 'services', 'totalCount').to_i
    end

    def get_services_response(response)
      response.dig('data', 'services', 'edges').map(&:deep_symbolize_keys)
    end

    # make sure we have enough values for our spec to work - this could fail
    # if default_max_page_size or ServiceCode::SERVICE_CODES are changed
    it "has enough records to paginate through" do
      expect(services.size).to eq(SwoopSchema.default_max_page_size * 2 + 1)
    end

    # this tests our ImplicitFirstOnConnections monkeypatch
    it "works without first or last" do
      # first make a request and get the first page back
      response = execute(
        query: query,
        context: context,
        variables: {},
        operation_name: operation_name,
        only: only_filter,
        root_value: api_company
      )
      total_count = get_total_count response
      expect(total_count).to eq(api_company.companies_services_excluding_addons.length)

      services_response = get_services_response response
      expect(services_response.length).to eq(SwoopSchema.default_max_page_size)

      page_info = get_page_info response
      expect(page_info[:hasNextPage]).to be true
      expect(page_info[:hasPreviousPage]).to be false
      expect(page_info[:startCursor]).to eq(services_response.first[:cursor])
      expect(page_info[:endCursor]).to eq(services_response.last[:cursor])

      # now ask for the next page
      response = execute(
        query: query("(after: #{page_info[:endCursor].inspect})"),
        context: context,
        variables: {},
        operation_name: operation_name,
        only: only_filter,
        root_value: api_company
      )
      total_count = get_total_count response
      expect(total_count).to eq(api_company.companies_services_excluding_addons.length)

      services_response = get_services_response response
      expect(services_response.length).to eq(SwoopSchema.default_max_page_size)

      page_info = get_page_info response
      expect(page_info[:hasNextPage]).to be true
      expect(page_info[:hasPreviousPage]).to be true
      expect(page_info[:startCursor]).to eq(services_response.first[:cursor])
      expect(page_info[:endCursor]).to eq(services_response.last[:cursor])

      # now ask for the last page
      response = execute(
        query: query("(after: #{page_info[:endCursor].inspect})"),
        context: context,
        variables: {},
        operation_name: operation_name,
        only: only_filter,
        root_value: api_company
      )

      total_count = get_total_count response
      expect(total_count).to eq(api_company.companies_services_excluding_addons.length)

      services_response = get_services_response response
      # here our response should be less than the max
      expect(services_response.length).to eq(api_company.companies_services_excluding_addons.length % SwoopSchema.default_max_page_size)

      page_info = get_page_info response
      expect(page_info[:hasNextPage]).to be false
      expect(page_info[:hasPreviousPage]).to be true
      expect(page_info[:startCursor]).to eq(services_response.first[:cursor])
      expect(page_info[:endCursor]).to eq(services_response.last[:cursor])

      # we don't/can't support reverse paging without last: so we're done here
    end

    # this tests the normal usage of pagination
    it "works with first/last" do
      # first make a request and get the first page back
      page_size = SwoopSchema.default_max_page_size

      response = execute(
        query: query("(first: #{page_size})"),
        context: context,
        variables: {},
        operation_name: operation_name,
        only: only_filter,
        root_value: api_company
      )

      total_count = get_total_count response
      expect(total_count).to eq(api_company.companies_services_excluding_addons.length)

      services_response = get_services_response response
      expect(services_response.length).to eq(page_size)

      page_info = get_page_info response
      expect(page_info[:hasNextPage]).to be true
      expect(page_info[:hasPreviousPage]).to be false
      expect(page_info[:startCursor]).to eq(services_response.first[:cursor])
      expect(page_info[:endCursor]).to eq(services_response.last[:cursor])

      # now ask for the next page
      response = execute(
        query: query("(first: #{page_size}, after: #{page_info[:endCursor].inspect})"),
        context: context,
        variables: {},
        operation_name: operation_name,
        only: only_filter,
        root_value: api_company
      )

      total_count = get_total_count response
      expect(total_count).to eq(api_company.companies_services_excluding_addons.length)

      services_response = get_services_response response
      expect(services_response.length).to eq(page_size)

      page_info = get_page_info response
      expect(page_info[:hasNextPage]).to be true
      expect(page_info[:hasPreviousPage]).to be true
      expect(page_info[:startCursor]).to eq(services_response.first[:cursor])
      expect(page_info[:endCursor]).to eq(services_response.last[:cursor])

      # now ask for the last page
      response = execute(
        query: query("(first: #{page_size}, after: #{page_info[:endCursor].inspect})"),
        context: context,
        variables: {},
        operation_name: operation_name,
        only: only_filter,
        root_value: api_company
      )

      total_count = get_total_count response
      expect(total_count).to eq(api_company.companies_services_excluding_addons.length)

      services_response = get_services_response response
      # here our response should be less than the max
      expect(services_response.length).to eq(api_company.companies_services_excluding_addons.length % page_size)

      page_info = get_page_info response
      expect(page_info[:hasNextPage]).to be false
      expect(page_info[:hasPreviousPage]).to be true
      expect(page_info[:startCursor]).to eq(services_response.first[:cursor])
      expect(page_info[:endCursor]).to eq(services_response.last[:cursor])

      # now go backwards
      response = execute(
        query: query("(last: #{page_size}, before: #{page_info[:startCursor].inspect})"),
        context: context,
        variables: {},
        operation_name: operation_name,
        only: only_filter,
        root_value: api_company
      )

      total_count = get_total_count response
      expect(total_count).to eq(api_company.companies_services_excluding_addons.length)

      services_response = get_services_response response
      expect(services_response.length).to eq(page_size)

      page_info = get_page_info response

      expect(page_info[:hasNextPage]).to be true
      expect(page_info[:hasPreviousPage]).to be true
      expect(page_info[:startCursor]).to eq(services_response.first[:cursor])
      expect(page_info[:endCursor]).to eq(services_response.last[:cursor])

      # and backwards one more time - we should now end up on the first page
      response = execute(
        query: query("(last: #{page_size}, before: #{page_info[:startCursor].inspect})"),
        context: context,
        variables: {},
        operation_name: operation_name,
        only: only_filter,
        root_value: api_company
      )

      total_count = get_total_count response
      expect(total_count).to eq(api_company.companies_services_excluding_addons.length)

      services_response = get_services_response response
      expect(services_response.length).to eq(page_size)

      page_info = get_page_info response

      expect(page_info[:hasNextPage]).to be true
      expect(page_info[:hasPreviousPage]).to be false
      expect(page_info[:startCursor]).to eq(services_response.first[:cursor])
      expect(page_info[:endCursor]).to eq(services_response.last[:cursor])
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "pagination"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "pagination"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "pagination"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "pagination"
  end
end
