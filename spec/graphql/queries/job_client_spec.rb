# frozen_string_literal: true

require 'rails_helper'

describe "Job Client Query" do
  let(:job) do
    create :fleet_managed_job,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end

  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        job(id: $id) {
          id
          client {
            name
            phone
            company {
              name
              phone
            }
          }
        }
      }
    GRAPHQL
  end

  let(:variables) { { id: job.to_ssid } }
  let(:client) { response.dig('data', 'job', 'client') }
  let(:deprecated_client_name) { client.dig('name') }
  let(:deprecated_client_phone) { client.dig('phone') }
  let(:client_name) { client.dig('company', 'name') }
  let(:client_phone) { client.dig('company', 'phone') }
  let(:id) { response.dig('data', 'job', 'id') }

  context "as a client company" do
    context 'as an api_application' do
      include_context "client api_application"

      it "works" do
        expect(id).to be_present
        expect(client_name).to be_present
        expect(client_phone).to be_present
        expect(deprecated_client_name).to be_present
        expect(deprecated_client_phone).to be_present
      end
    end
  end

  context "as a super client company" do
    context 'as an api_application' do
      include_context "super client api_application"

      it "works" do
        expect(id).to be_present
        expect(client_name).to be_present
        expect(client_phone).to be_present
        expect(deprecated_client_name).to be_present
        expect(deprecated_client_phone).to be_present
      end
    end
  end
end
