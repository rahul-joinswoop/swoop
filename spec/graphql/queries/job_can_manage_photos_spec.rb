# frozen_string_literal: true

require 'rails_helper'

describe "job.canManagePhotos" do
  shared_examples 'job.canManagePhotos' do
    let!(:job) { create :job, rescue_company: rescue_company }
    let(:query) do
      <<~GRAPHQL
        query #{operation_name} {
          job(id: "#{job.to_ssid}") {
            id
            canManagePhotos
          }
        }
      GRAPHQL
    end

    it 'has expected attributes' do
      expect(response).not_to include('errors')
      job_response = response_data.dig("job")

      expect(job_response["id"]).to eq job.to_ssid
      # LOLWUT? there are better specs around this in job_spec
      expect(job_response["canManagePhotos"]).to be_in([true, false])
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "job.canManagePhotos"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "job.canManagePhotos"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "job.canManagePhotos"
  end
end
