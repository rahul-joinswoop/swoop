# frozen_string_literal: true

require 'rails_helper'

describe "Job Coverage Query" do
  let(:fleet_company) { fleet_managed_company }
  let!(:pcc_policy) { create :pcc_policy, :lincoln, company: fleet_company, job: job }
  let(:job) { create :fleet_managed_job, fleet_company: fleet_company, pcc_coverage: pcc_coverage }
  let(:variables) { { id: job.to_ssid } }
  let(:job_response) { response.dig('data', 'job') }

  shared_examples "job.coverage" do
    shared_examples "matches expected output" do
      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expect(job_response).to eq(expected_response)
      end
    end

    context "with pcc_coverage" do
      context "with full coverage" do
        let(:pcc_coverage) { create :pcc_coverage, :lincoln }

        it_behaves_like "matches expected output"
      end

      context "with partial coverage" do
        let(:pcc_coverage) { create :pcc_coverage, :lincoln_partially_covered }

        it_behaves_like "matches expected output"
      end

      context "with no coverage" do
        let(:pcc_coverage) { create :pcc_coverage, :lincoln_not_covered }

        it_behaves_like "matches expected output"
      end
    end

    context "without pcc_coverage" do
      let(:pcc_coverage) { nil }
      let(:pcc_policy) { nil }
      let(:expected_response) do
        {
          id: job.to_ssid,
          coverage: nil,
        }.deep_stringify_keys
      end

      it_behaves_like "matches expected output"
    end
  end

  context "as a super client api_application" do
    include_context "super client api_application"
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            coverage {
              id
              policy {
                id
                number
                swcid
              }
              program {
                code
                endAt
                name
                startAt
                swcid
              }
              result
              issues {
                edges {
                  node {
                    issue
                  }
                }
              }
            }
          }
        }
      GRAPHQL
    end
    let(:expected_response) do
      {
        id: job.to_ssid,
        coverage: {
          id: pcc_coverage.to_ssid,
          policy: {
            id: pcc_policy.to_ssid,
            number: pcc_policy.policy_number,
            swcid: pcc_policy.id,
          },
          program: {
            code: pcc_coverage.data.first["code"],
            endAt: pcc_policy.data.dig("programs").find { |p| p["program_name"] == pcc_coverage.data.first["name"] }['end_date'],
            name: pcc_coverage.data.first["name"],
            startAt: pcc_policy.data.dig("programs").find { |p| p["program_name"] == pcc_coverage.data.first["name"] }['start_date'],
            swcid: pcc_coverage.client_program_id,
          },
          issues: pcc_coverage.data.first["coverage_issues"]&.reduce({ edges: [] }) do |memo, cur|
            memo[:edges] << { node: { issue: cur } }
            memo
          end,
          result: pcc_coverage.data.first["result"],
        },
      }.deep_stringify_keys
    end

    it_behaves_like "job.coverage"
  end

  context "as a client api_application" do
    include_context "client api_application"
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            coverage {
              id
              policy {
                id
                number
                swcid
              }
              program {
                name
                swcid
              }
              result
            }
          }
        }
      GRAPHQL
    end

    let(:expected_response) do
      {
        id: job.to_ssid,
        coverage: {
          id: pcc_coverage.to_ssid,
          policy: {
            id: pcc_policy.to_ssid,
            number: pcc_policy.policy_number,
            swcid: pcc_policy.id,
          },
          program: {
            name: pcc_coverage.data.first["name"],
            swcid: pcc_coverage.client_program_id,
          },
          result: pcc_coverage.data.first["result"],
        },
      }.deep_stringify_keys
    end

    it_behaves_like "job.coverage"
  end
end
