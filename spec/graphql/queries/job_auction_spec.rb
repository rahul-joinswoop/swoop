# frozen_string_literal: true

require 'rails_helper'

describe "Job Auction Query" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        job(id: $id) {
          id
          status
          expiresAt
          auction {
            expiresAt
            status
            bids {
              edges {
                node {
                  id
                  status
                  eta
                }
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let(:fleet_company) { fleet_managed_company }
  let(:job) do
    create(:fleet_managed_job, :with_auction, status: :auto_assigning, fleet_company: fleet_company, bids: 3).tap do |j|
      j.auction.update! status: Auction::Auction::LIVE
    end
  end
  let(:variables) { { id: job.to_ssid } }
  let(:job_response) { response.dig('data', 'job') }
  let(:status) { SwoopSchema.types['JobStatus'].values.detect { |k, v| v.value == job.virtual_status(context[:api_company]) }[0] }
  let(:expected) do
    {
      status: status,
      id: job.to_ssid,
      expiresAt: job.auction.expires_at.utc.iso8601,
      auction: {
        expiresAt: job.auction.expires_at.utc.iso8601,
        status: Auction::Auction::STATUS_TO_NAME_MAP[job.auction.status],
        bids: {
          edges: [{
            node: {}.tap do |n|
              bid = job.auction.bids.where(company: rescue_company).first
              n[:id] = bid.to_ssid
              n[:status] = Auction::Bid::STATUS_TO_ENUM_NAME_MAP[bid.status]
              n[:eta] = bid.eta_dttm.utc.iso8601
            end,
          }],
        },
      },
    }.deep_stringify_keys
  end

  shared_examples "auction" do
    let(:expected) do
      super().tap do |e|
        e['id'] = job.auction.bids.last.job_ssid
      end
    end

    context "with a job we're not a part of" do
      # we shouldn't be able to see this job
      it 'works' do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expect(job_response).to be_nil
      end
    end

    context "with bidding active and a bid" do
      # we should be able to see this job
      let(:job) do
        super().tap do |j|
          j.auction.bids << create(:bid, auction: j.auction, job: j, company: rescue_company)
        end
      end

      it 'works' do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expect(job_response).to eq(expected)
      end
    end

    context "with a losing bid" do
      context "and an auction in progress" do
        let!(:job) do
          super().tap do |j|
            j.auction.bids << create(:bid, auction: j.auction, job: j, company: rescue_company)
            j.auction.bids.update_all status: Auction::Bid::AUTO_REJECTED
          end
        end

        it 'works' do
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(job_response).to eq(expected)
        end
      end

      context "and a completed auction" do
        let!(:job) do
          super().tap do |j|
            j.auction.bids << create(:bid, auction: j.auction, job: j, company: rescue_company)
            j.auction.bids.update_all status: Auction::Bid::AUTO_REJECTED
            j.auction.bids.first.update! status: Auction::Bid::WON
            j.auction.update! status: Auction::Auction::SUCCESSFUL, expires_at: Time.current
            j.update! rescue_company: j.auction.bids.first.company
          end
        end

        context "within 15 minutes" do
          # we should be able to see this job
          it 'works' do
            expect { response }.not_to raise_error
            expect(response).not_to include('errors')
            expect(job_response).to eq(expected)
          end
        end

        context "after 15 minutes" do
          # we shouldn't be able to see this job
          it 'works' do
            Timecop.travel(Time.current + Auction::Auction.get_duration_secs(job).seconds + 15.minutes) do
              expect { response }.not_to raise_error
              expect(response).not_to include('errors')
              expect(job_response).to be_nil
            end
          end
        end
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "auction"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "auction"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "auction"
  end
end
