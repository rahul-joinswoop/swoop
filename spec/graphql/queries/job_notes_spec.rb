# frozen_string_literal: true

require 'rails_helper'

describe "Job Notes Query" do
  describe "internal notes" do
    let(:fleet_notes) { Faker::Hipster.sentence }
    let(:partner_notes) { Faker::Hipster.sentence }
    let(:swoop_notes) { Faker::Hipster.sentence }
    let(:dispatch_notes) { Faker::Hipster.sentence }
    let(:job) do
      create :fleet_managed_job,
             fleet_company: fleet_managed_company,
             rescue_company: rescue_company,
             fleet_notes: fleet_notes,
             partner_notes: partner_notes,
             swoop_notes: swoop_notes,
             dispatch_notes: dispatch_notes
    end
    let(:dispatcher) { false }
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            notes {
              internal
              #{'dispatcher' if dispatcher}
            }
          }
        }
      GRAPHQL
    end

    let(:internal_notes) { response.dig('data', 'job', 'notes', 'internal') }
    let(:variables) { { id: job.to_ssid, partner: false } }

    context "as a client api_application" do
      include_context "client api_application"

      it "works" do
        expect(internal_notes).to eq(fleet_notes)
      end
    end

    context 'partner' do
      let(:dispatcher) { true }

      shared_examples "partner notes" do
        context 'as a driver' do
          let!(:api_user) { rescue_driver }

          it "works" do
            expect(response.dig('data', 'job', 'notes', 'dispatcher')).to be_nil
          end
        end

        context 'as a dispatcher' do
          let!(:api_user) { rescue_dispatcher }

          it "works" do
            expect(response.dig('data', 'job', 'notes', 'dispatcher')).to eq(dispatch_notes)
          end
        end

        context 'as an admin' do
          let!(:api_user) { rescue_admin }

          it "works" do
            expect(response.dig('data', 'job', 'notes', 'dispatcher')).to eq(dispatch_notes)
          end
        end
      end

      context "as a partner api_user" do
        include_context "partner api_user"
        it_behaves_like "partner notes"
      end

      context "as a 3rd party partner api_user" do
        include_context "3rd party partner api_user"
        it_behaves_like "partner notes"
      end

      context "as a 3rd party partner api_company" do
        include_context "3rd party partner api_company"

        it "works" do
          expect(internal_notes).to eq(partner_notes)
          # check the actual reading of job.notes.dispatcher here, check the visibility below
          expect(response.dig('data', 'job', 'notes', 'dispatcher')).to eq(dispatch_notes)
        end
      end
    end
  end
end
