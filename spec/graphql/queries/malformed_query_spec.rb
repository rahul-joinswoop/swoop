# frozen_string_literal: true

require 'rails_helper'

describe "Malformed Query" do
  let(:query) { '# #' }

  it "doesn't explode" do
    expect { response }.not_to raise_error
  end
end
