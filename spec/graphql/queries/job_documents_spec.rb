# frozen_string_literal: true

require 'rails_helper'

describe "Job Documents Query" do
  let(:job) do
    create :fleet_managed_job,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company,
           rescue_driver: create(:user, :rescue, :driver, company: rescue_company)
  end

  let(:location_images) do
    [
      create(:location_image, :dropoff, job: job, user: job.rescue_driver),
      create(:location_image, :pickup, job: job, user: job.rescue_driver),
      create(:location_image, :release, job: job, user: job.rescue_driver),
    ]
  end

  let(:signature_images) do
    [
      create(:signature_image, :dropoff, job: job, user: job.rescue_driver),
      create(:signature_image, :pickup, job: job, user: job.rescue_driver),
      create(:signature_image, :release, job: job, user: job.rescue_driver),
    ]
  end

  let(:attached_documents) do
    [
      create(:attached_document, :dropoff, job: job, user: job.rescue_driver),
      create(:attached_document, :pickup, job: job, user: job.rescue_driver),
      create(:attached_document, :release, job: job, user: job.rescue_driver),
    ]
  end

  let(:dropoff_documents) do
    [
      location_images.first,
      signature_images.first,
      attached_documents.first,
    ]
  end

  let!(:all_documents) { [*attached_documents, *signature_images, *location_images] }

  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!, $type: DocumentTypeType, $container: DocumentContainerType ) {
        job(id: $id) {
          id
          documents(type: $type, container: $container) {
            edges {
              node {
                id
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let(:documents) { response.dig('data', 'job', 'documents', 'edges')&.map { |v| v.dig('node', 'id') } }
  let(:container) { nil }
  let(:type) { nil }
  let(:variables) { { id: job.to_ssid, type: type, container: container } }

  context "as a client api_application" do
    include_context "client api_application"

    it "works" do
      expect(documents).to match_array(all_documents.map(&:to_ssid))
    end

    context 'with a container set' do
      let(:container) { 'Dropoff' }

      it "works" do
        expect(documents).to match_array(dropoff_documents.map(&:to_ssid))
      end
    end

    context 'with a type set' do
      let(:type) { 'LocationImage' }

      it "works" do
        expect(documents).to match_array(location_images.map(&:to_ssid))
      end
    end

    context 'with a type and container set' do
      let(:type) { 'LocationImage' }
      let(:container) { 'Dropoff' }

      it "works" do
        expect(documents).to match_array([location_images.first].map(&:to_ssid))
      end
    end
  end

  context "as a partner" do
    context "as a partner api_user" do
      include_context "partner api_user"
      it "works" do
        expect(documents).to match_array(all_documents.map(&:to_ssid))
      end
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it "works" do
        expect(documents).to match_array(all_documents.map(&:to_ssid))
      end
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it "works" do
        expect(documents).to match_array(all_documents.map(&:to_ssid))
      end
    end
  end
end
