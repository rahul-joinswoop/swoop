# frozen_string_literal: true

require 'rails_helper'

describe "Job History Query" do
  shared_examples 'job.history' do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            history {
              edges {
                node {
                  name
                  type
                  lastSet
                }
              }
            }
          }
        }
      GRAPHQL
    end

    let(:variables) { { id: job.to_ssid } }
    let!(:job) do
      # set scheduled_for to force scheduled_for history row in the result (it will be an OpenStruct)
      parent_job = create(:fleet_managed_job, fleet_company: api_company, status: Job::STATUS_REASSIGNED, scheduled_for: Time.current)
      parent_job.audit_job_statuses << create(:audit_job_status, company: api_company, job_status: create(:job_status, :reassigned))
      parent_job.history_items << create(:history_item, target: parent_job, job: parent_job, title: "Invoice Edited", history_type: "History")
      parent_job.audit_sms << create(:audit_sms, :sms_confirmation)

      # set scheduled_for to force scheduled_for history row in the result (it will be an OpenStruct)
      job = create(:fleet_managed_job, fleet_company: api_company, parent: parent_job, scheduled_for: Time.current)
      job.history_items << create(:history_item, target: job, job: job, title: "", history_type: "")
      job.history_items << create(:history_item, target: job, job: job, title: "A Title", history_type: "A Type")
      job.audit_job_statuses << create(:audit_job_status, company: api_company, job_status: create(:job_status, :assigned))
      job.audit_job_statuses << create(:audit_job_status, company: api_company, job_status: create(:job_status, :en_route))
      job.audit_sms << create(:audit_sms, :sms_confirmation)
      job
    end

    let(:nodes) { response.dig('data', 'job', 'history', 'edges').map { |n| n['node'] } }

    it 'works' do
      expect(response).not_to include('errors')
      # eight statuses in the db,
      # and two created dynamically for the scheduled_for value (for for the parent, the other for the child)
      expect(nodes.length).to eq(10)
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "job.history"
  end
end
