# frozen_string_literal: true

require 'rails_helper'

describe "Job Caller Query" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        job(id: $id) {
          id
          caller {
            name
            phone
          }
        }
      }
    GRAPHQL
  end

  let(:variables) { { id: job.to_ssid } }
  let(:caller) { response.dig('data', 'job', 'caller') }

  shared_examples 'job.caller' do
    context 'with a FleetInHouseJob' do
      let(:job) { create :fleet_in_house_job, rescue_company: api_company }

      it 'works' do
        expect(response).not_to include('errors')
        expect(caller['name']).to eq(job.fleet_dispatcher.name)
        expect(caller['phone']).to eq(job.fleet_dispatcher.phone)
      end
    end

    context 'with a FleetMotorClubJob' do
      let(:job) { create :fleet_motor_club_job, rescue_company: api_company }

      it 'works' do
        expect(response).not_to include('errors')
        expect(caller['name']).to eq(job.fleet_dispatcher.name)
        expect(caller['phone']).to eq(job.fleet_dispatcher.phone)
      end

      context 'with an ISSC job' do
        it 'works' do
          allow_any_instance_of(Job).to receive(:issc?).and_return(true)
          expect(response).not_to include('errors')
          expect(caller).to be_nil
        end
      end

      context 'with an RSC job' do
        it 'works' do
          allow_any_instance_of(Job).to receive(:rsc?).and_return(true)
          expect(response).not_to include('errors')
          expect(caller).to be_nil
        end
      end
    end

    context 'with a FleetManagedJob' do
      let(:job) { create :fleet_managed_job, rescue_company: api_company }

      it 'works' do
        expect(response).not_to include('errors')
        expect(caller['name']).to eq(job.root_dispatcher.name)
        expect(caller['phone']).to eq(Types::JobCallerType::DISPATCH_NUMBER)
      end
    end

    context 'with a RescueJob' do
      context "without a caller" do
        let(:job) { create :rescue_job, rescue_company: api_company, fleet_company: api_company, caller: nil }

        it 'works' do
          expect(response).not_to include('errors')
          expect(caller['name']).to eq(nil)
          expect(caller['phone']).to eq(nil)
        end
      end

      context "with a caller" do
        let(:job) { create :rescue_job, rescue_company: api_company, caller: create(:user) }

        it 'works' do
          expect(response).not_to include('errors')
          expect(caller['name']).to eq(job.caller.name)
          expect(caller['phone']).to eq(job.caller.phone)
        end
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "job.caller"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "job.caller"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "job.caller"
  end
end
