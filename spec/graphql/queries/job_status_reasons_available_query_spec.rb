# frozen_string_literal: true

require 'rails_helper'

describe "JobStatusReasonsAvailable Query" do
  let(:agero) do
    c = create :fleet_managed_company, :agero, use_custom_job_status_reasons: true
    CompanyJobStatusReasonType.init_by_client_id(:AGO)
    c
  end

  let(:fleet_managed_company) { agero }
  let!(:job) do
    create :fleet_motor_club_job,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end

  let(:query) do
    <<~GRAPHQL
      query #{operation_name} {
        job(id: "#{job.to_ssid}") {
          id
          statusReasonsAvailable(status: #{status}) {
            edges {
              node {
                id
                reason
                status
                notesRequired
              }
            }
          }
        }
      }
    GRAPHQL
  end
  let!(:expected_reason_types) do
    JobStatusReasonType.where(key: [CompanyJobStatusReasonType::KEYS_RESTRICTED_BY_COMPANY[:AGO][status.upcase.to_sym]])
  end

  shared_examples 'statusReasonsAvailable' do
    shared_context "fetching expected job status reason types" do
      it 'returns expected results' do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        job_result = response.dig("data", "job")
        expect(job_result["id"]).to eq job.to_ssid
        status_reason_type_ids = job_result.dig("statusReasonsAvailable", "edges").map { |e| e.dig("node", "id") }
        expect(status_reason_type_ids).to include(*expected_reason_types.map(&:to_ssid))
      end
    end

    context 'when status is Accepted' do # but really, can be any
      let(:status) { 'Accepted' }

      it_behaves_like "fetching expected job status reason types"
    end

    context 'when status is Rejected' do # but really, can be any
      let(:status) { 'Rejected' }

      it_behaves_like "fetching expected job status reason types"

      it "sets notesRequired correctly" do
        reasons = response.dig("data", "job", "statusReasonsAvailable", "edges").map { |e| e['node'] }
        expect(reasons.select { |r| r['reason'] == 'Other' }.all? { |r| r['notesRequired'] == true }).to be(true)
        expect(reasons.reject { |r| r['reason'] == 'Other' }.all? { |r| r['notesRequired'] == false }).to be(true)
      end
    end

    context 'when status is GOA' do # but really, can be any
      let(:status) { 'GOA' }

      it_behaves_like "fetching expected job status reason types"
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "statusReasonsAvailable"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "statusReasonsAvailable"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "statusReasonsAvailable"
  end
end
