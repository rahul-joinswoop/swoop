# frozen_string_literal: true

require 'rails_helper'

describe "job.vehicle.odometer" do
  shared_examples "odometer" do
    # make sure we can handle odometer values > 32bits
    let(:odometer) { 3900067695.0 }
    let(:job) do
      create :job,
             rescue_company: rescue_company,
             fleet_company: fleet_managed_company,
             odometer: odometer
    end
    let(:query) do
      <<~GRAPHQL
        query #{operation_name} {
          job(id: "#{job.to_ssid}") {
            id
            vehicle {
              odometer
            }
          }
        }
      GRAPHQL
    end

    it 'works' do
      expect(response).not_to include('errors')
      expect(response.dig('data', 'job', 'vehicle', 'odometer')).to eq(odometer.to_i.to_s)
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "odometer"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "odometer"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "odometer"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "odometer"
  end
end
