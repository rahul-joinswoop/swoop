# frozen_string_literal: true

require 'rails_helper'

describe "Partner Vehicles Query" do
  let(:other_rescue_company) { create :rescue_company }

  context "rescueVehicles" do
    # vehicles connected to jobs with these statuses shouldn't show up in our
    # search results
    let(:fleet_job_bad_status) do
      create :fleet_managed_job,
             :with_rescue_vehicle,
             fleet_company: fleet_managed_company,
             rescue_company: rescue_company,
             status: Job.fleet_no_publish_states.sample
    end
    # vehicles connected to these jobs should show up
    let(:fleet_jobs) do
      create_list :fleet_managed_job,
                  2,
                  :with_rescue_vehicle,
                  fleet_company: fleet_managed_company,
                  rescue_company: rescue_company,
                  status: Job.fleet_show_rescue_vehicle_states.sample
    end
    # this job has no rescue vehicle so they shouldn't affect anything
    let(:fleet_job_no_vehicle) do
      create :fleet_managed_job,
             fleet_company: fleet_managed_company,
             rescue_company: rescue_company,
             status: Job.fleet_show_rescue_vehicle_states.sample
    end
    # these vehicles shouldn't show up because they belong to a different fleet
    # company
    let(:other_fleet_job) do
      create :fleet_managed_job,
             :with_rescue_vehicle,
             fleet_company: swoop,
             rescue_company: other_rescue_company,
             status: Job.fleet_show_rescue_vehicle_states.sample
    end

    # these vehicles shouldn't show up because they belong to a different fleet
    # company
    let(:other_fleet_job_bad_status) do
      create :fleet_managed_job,
             :with_rescue_vehicle,
             fleet_company: swoop,
             rescue_company: rescue_company,
             status: Job.fleet_no_publish_states.sample
    end

    let!(:all_fleet_jobs) do
      [other_fleet_job, other_fleet_job_bad_status, fleet_job_no_vehicle, fleet_job_bad_status] + fleet_jobs
    end

    shared_examples "rescueVehicles" do
      let(:query) do
        <<~GRAPHQL
          fragment companyFields on Company {
            rescueVehicles {
              edges {
                node {
                  id
                  location {
                    lat
                    lng
                  }
                  driver {
                    name
                    phone
                  }
                }
              }
            }
          }
          query #{operation_name} {
            viewer {
              ...on Application {
                company {
                  ...companyFields
                }
              }
              ...on User {
                company {
                  ...companyFields
                }
              }
              ...on Company {
                  ...companyFields
              }

            }
          }
        GRAPHQL
      end

      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expected_response = expected_vehicles.map do |v|
          {
            id: v.to_ssid,
            location: {
              lat: v.lat,
              lng: v.lng,
            },
            driver: {
              phone: v.driver.phone,
              name: api_company.is_a?(::FleetCompany) ? v.driver.first_name : v.driver.full_name,
            },
          }.deep_stringify_keys
        end
        rescue_vehicles_response = response.dig(*viewer_company_path, 'rescueVehicles', 'edges').to_a.map { |r| r['node'] }

        # yuck, check and remove the lat/lng here since pg rounds them
        expected_response.each do |er|
          r = rescue_vehicles_response.find { |rvr| rvr['id'] == er['id'] }
          expect(r).to be_present
          ['lat', 'lng'].each do |k|
            expect(r['location'].delete(k)).to be_within(0.0000000001).of(er['location'].delete(k))
          end
        end

        # everything else should match
        expect(rescue_vehicles_response.length).to eq(expected_response.length)
        expect(rescue_vehicles_response).to contain_exactly(*expected_response)
      end
    end

    shared_examples "rescueVehicle node" do
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            node(id: #{node.to_ssid.inspect}) {
              id
            }
          }
        GRAPHQL
      end

      context "with a valid node" do
        let(:node) { expected_vehicles.sample }

        it "works" do
          skip "no valid nodes" if node.blank?
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(response.dig("data", "node", "id")).to eq(node.to_ssid)
        end
      end

      context "with an invalid node" do
        let(:node) { (all_fleet_jobs.map(&:rescue_vehicle).select(&:present?) - expected_vehicles).sample }

        it "works" do
          skip "no invalid nodes" if node.blank?
          expect { response }.not_to raise_error
          expect(response).not_to include('errors')
          expect(response.dig("data", "node", "id")).to be_blank
        end
      end
    end

    context "as a client api_application" do
      include_context "client api_application"
      let(:expected_vehicles) { fleet_jobs.map(&:rescue_vehicle) }

      it_behaves_like "rescueVehicles"
      it_behaves_like "rescueVehicle node"
    end

    context 'partner' do
      let(:expected_vehicles) { (fleet_jobs + [fleet_job_bad_status, other_fleet_job_bad_status]).map(&:rescue_vehicle) }

      context "as a partner api_user" do
        include_context "partner api_user"

        it_behaves_like "rescueVehicles"
        it_behaves_like "rescueVehicle node"
      end

      context "as a 3rd party partner api_user" do
        include_context "3rd party partner api_user"

        it_behaves_like "rescueVehicles"
        it_behaves_like "rescueVehicle node"
      end

      context "as a 3rd party partner api_company" do
        include_context "3rd party partner api_company"

        it_behaves_like "rescueVehicles"
        it_behaves_like "rescueVehicle node"
      end
    end
  end

  shared_examples "search" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($query: String) {
          viewer {
            ...on Application {
              company {
                ...partnerVehicleFields
              }
            }
            ...on User {
              company {
                ...partnerVehicleFields
              }
            }
            ...on Company {
              ...partnerVehicleFields
            }
          }
        }
        fragment partnerVehicleFields on Company {
          partnerVehicles(query: $query) {
            edges {
              node {
                id
                name
              }
            }
          }
        }
      GRAPHQL
    end
    let(:query_by_name) { nil }
    let(:variables) { { query: query_by_name } }
    let!(:vehicle_swoop) { create(:rescue_vehicle, company: rescue_company, name: 'Swoop') }
    let!(:vehicle_cash_call) { create(:rescue_vehicle, company: rescue_company, name: 'Cash Call') }
    let!(:vehicle_fleet) { create(:rescue_vehicle, company: rescue_company, name: 'Fleet Company') }
    let!(:vehicle_pacoca) { create(:rescue_vehicle, company: rescue_company, name: 'Paçoca') }
    let!(:vehicle_deleted) { create(:rescue_vehicle, company: rescue_company, name: 'Deleted Account', deleted_at: Time.now) }

    let(:expected_vehicles) { [vehicle_swoop, vehicle_cash_call, vehicle_fleet, vehicle_pacoca] }

    shared_examples "successfully listing expected vehicles" do
      it "has expected results" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expected_response = expected_vehicles || {}
        vehicles_result = response.dig(*viewer_company_path, "partnerVehicles", "edges")

        expected_response_attributes = expected_response.map do |vehicle|
          {
            id: vehicle.to_ssid,
            name: vehicle.name,
          }.deep_stringify_keys
        end
        vehicle_nodes = vehicles_result.to_a.map { |r| r['node'] }

        expect(vehicle_nodes.size).to eq expected_response_attributes.size
        expect(vehicle_nodes).to match_array expected_response_attributes
      end
    end

    it_behaves_like "successfully listing expected vehicles"

    context "when querying for vehicles" do
      context "by 'Cash'" do
        let(:query_by_name) { 'Cash' }
        let(:expected_vehicles) { [vehicle_cash_call] }

        it_behaves_like "successfully listing expected vehicles"
      end

      context "by 'cash' lower case" do
        let(:query_by_name) { 'cash' }
        let(:expected_vehicles) { [vehicle_cash_call] }

        it_behaves_like "successfully listing expected vehicles"
      end

      context "by 'Swoop'" do
        let(:query_by_name) { 'Swoop' }
        let(:expected_vehicles) { [vehicle_swoop] }

        it_behaves_like "successfully listing expected vehicles"
      end

      context "by 'swoop' lower case" do
        let(:query_by_name) { 'swoop' }
        let(:expected_vehicles) { [vehicle_swoop] }

        it_behaves_like "successfully listing expected vehicles"
      end

      context "by 'ç' lower case" do
        let(:query_by_name) { 'ç' }
        let(:expected_vehicles) { [vehicle_pacoca] }

        it_behaves_like "successfully listing expected vehicles"
      end

      context "by a deleted vehicle name" do
        let(:query_by_name) { 'Deleted' }
        let(:expected_vehicles) { [] }

        it_behaves_like "successfully listing expected vehicles"
      end

      context "when query term is empty" do
        let(:query_by_name) { " " }

        let(:expected_vehicles) { [] }

        it_behaves_like "successfully listing expected vehicles"
      end

      context "when querying for a deleted vehicle" do
        let(:query_by_name) { vehicle_deleted.name }

        let(:expected_vehicles) { [] }

        it_behaves_like "successfully listing expected vehicles"
      end
    end
  end

  shared_examples "filters" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($noDriver: Boolean, $offDutyDriver: Boolean) {
          viewer {
            ...on Application {
              company {
                ...partnerVehicleFields
              }
            }
            ...on User {
              company {
                ...partnerVehicleFields
              }
            }
            ...on Company {
              ...partnerVehicleFields
            }
          }
        }
        fragment partnerVehicleFields on Company {
          partnerVehicles(filters: { noDriver: $noDriver, offDutyDriver: $offDutyDriver }) {
            edges {
              node {
                id
              }
            }
          }
        }
      GRAPHQL
    end
    let(:query_by_name) { nil }
    let(:variables) { { noDriver: noDriver, offDutyDriver: offDutyDriver } }
    let!(:vehicle) { create(:rescue_vehicle, company: rescue_company) }
    let!(:vehicle_with_on_duty_driver) { create(:rescue_vehicle, :with_driver, company: rescue_company) }
    let!(:vehicle_with_off_duty_driver) { create(:rescue_vehicle, driver: create(:user, :phone), company: rescue_company) }
    let(:expected_vehicles) { [vehicle, vehicle_with_on_duty_driver, vehicle_with_off_duty_driver] }

    shared_examples "successfully listing expected vehicles by filter" do
      it "has expected results" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expected_response = expected_vehicles || {}
        vehicles_result = response.dig(*viewer_company_path, "partnerVehicles", "edges")

        expected_response_attributes = expected_response.map do |vehicle|
          {
            id: vehicle.to_ssid,
          }.deep_stringify_keys
        end
        vehicle_nodes = vehicles_result.to_a.map { |r| r['node'] }

        expect(vehicle_nodes.size).to eq expected_response_attributes.size
        expect(vehicle_nodes).to match_array expected_response_attributes
      end
    end

    context "defaults" do
      let(:noDriver) { false }
      let(:offDutyDriver) { false }

      it_behaves_like "successfully listing expected vehicles by filter"
    end

    context "noDriver=true" do
      let(:noDriver) { true }
      let(:offDutyDriver) { false }
      let(:expected_vehicles) { [vehicle_with_on_duty_driver, vehicle_with_off_duty_driver] }

      it_behaves_like "successfully listing expected vehicles by filter"
    end

    context "offDutyDriver=true" do
      let(:offDutyDriver) { true }
      let(:noDriver) { false }
      let(:expected_vehicles) { [vehicle, vehicle_with_on_duty_driver] }

      it_behaves_like "successfully listing expected vehicles by filter"
    end

    context "noDriver=true, offDutyDriver=true" do
      let(:noDriver) { true }
      let(:offDutyDriver) { true }
      let(:expected_vehicles) { [vehicle_with_on_duty_driver] }

      it_behaves_like "successfully listing expected vehicles by filter"
    end
  end

  shared_examples "area" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($query: String, $lat1: Float, $lat2: Float, $lng1: Float, $lng2: Float) {
          viewer {
            ...on Application {
              company {
                ...partnerVehicleFields
              }
            }
            ...on User {
              company {
                ...partnerVehicleFields
              }
            }
            ...on Company {
              ...partnerVehicleFields
            }
          }
        }
        fragment partnerVehicleFields on Company {
          partnerVehicles(query: $query, area: { from: { lat: $lat1, lng: $lng1 }, to: { lat: $lat2, lng: $lng2 } }) {
            edges {
              node {
                id
                name
              }
            }
          }
        }
      GRAPHQL
    end
    let(:query_by_name) { nil }
    let(:variables) { { lat1: lat1, lng1: lng1, lat2: lat2, lng2: lng2 } }

    let!(:vehicle_swoop) { create(:rescue_vehicle, company: rescue_company, name: 'Swoop', lat: 0, lng: 0) }
    let!(:vehicle_cash_call) { create(:rescue_vehicle, company: rescue_company, name: 'Cash Call', lat: 100, lng: 100) }
    let!(:vehicle_fleet) { create(:rescue_vehicle, company: rescue_company, name: 'Fleet Company', lat: -100, lng: -100) }
    let!(:vehicle_pacoca) { create(:rescue_vehicle, company: rescue_company, name: 'Paçoca', lat: 125, lng: 125) }

    let(:expected_vehicles) { [vehicle_swoop, vehicle_cash_call, vehicle_fleet, vehicle_pacoca] }

    shared_examples "successfully listing expected vehicles by area" do
      it "has expected results" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expected_response = expected_vehicles || {}
        vehicles_result = response.dig(*viewer_company_path, "partnerVehicles", "edges")

        expected_response_attributes = expected_response.map do |vehicle|
          {
            id: vehicle.to_ssid,
            name: vehicle.name,
          }.deep_stringify_keys
        end
        vehicle_nodes = vehicles_result.to_a.map { |r| r['node'] }

        expect(vehicle_nodes.size).to eq expected_response_attributes.size
        expect(vehicle_nodes).to match_array expected_response_attributes
      end
    end

    context "when querying by area" do
      context "-100, -100 to 100, 100" do
        let(:lat1) { -100 }
        let(:lng1) { -100 }
        let(:lat2) { 100 }
        let(:lng2) { 100 }
        let(:expected_vehicles) { [vehicle_swoop, vehicle_cash_call, vehicle_fleet] }

        it_behaves_like "successfully listing expected vehicles by area"
      end

      context "0,0 to 100, 100" do
        let(:lat1) { 0 }
        let(:lng1) { 0 }
        let(:lat2) { 100 }
        let(:lng2) { 100 }
        let(:expected_vehicles) { [vehicle_swoop, vehicle_cash_call] }

        it_behaves_like "successfully listing expected vehicles by area"
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"

    it_behaves_like "search"
    it_behaves_like "area"
    it_behaves_like "filters"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"

    it_behaves_like "search"
    it_behaves_like "area"
    it_behaves_like "filters"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"

    it_behaves_like "search"
    it_behaves_like "area"
    it_behaves_like "filters"
  end
end
