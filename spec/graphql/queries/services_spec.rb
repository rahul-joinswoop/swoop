# frozen_string_literal: true

require 'rails_helper'

describe "Services Query" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($filter: ServiceFilter!) {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
      fragment companyFields on Company {
        services(filter: $filter) {
          edges {
            node {
              id
              name
            }
          }
        }
      }


    GRAPHQL
  end

  let(:variables) { {} }

  shared_examples "services" do
    let!(:service_accident_tow) { create :service_code, name: ServiceCode::ACCIDENT_TOW }
    let!(:service_tow) { create :service_code, name: ServiceCode::TOW }
    let!(:service_goa) { create :service_code, name: ServiceCode::GOA }
    let!(:service_storage) { create :service_code, name: ServiceCode::STORAGE }
    let!(:expected_services) do
      services = [service_accident_tow, service_tow, service_goa, service_storage]
      api_company.services << services
      api_company.companies_services
    end

    shared_examples "has expected results" do
      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')

        expected_response = expected_services || {}
        services_result = response.dig(*viewer_company_path, "services", "edges")

        expected_response_attributes = expected_response.map do |service|
          {
            id: service.to_ssid,
            name: service.service_code.name,
          }.deep_stringify_keys
        end
        service_nodes = services_result.to_a.map { |r| r['node'] }
        expect(service_nodes).to match_array expected_response_attributes
      end
    end

    context 'without filter' do
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            viewer {
              ...on Application {
                company {
                  ...companyFields
                }
              }
              ...on User {
                company {
                  ...companyFields
                }
              }
              ...on Company {
                ...companyFields
              }
            }
          }
          fragment companyFields on Company {
            services {
              edges {
                node {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end

      it_behaves_like "has expected results"
    end

    context "with filter: 'Create'" do
      let(:variables) { { filter: 'Create' } }
      let(:expected_services) { super().reject { |s| ['GOA', 'Storage'].include?(s.service_code.name) } }

      it_behaves_like "has expected results"
    end

    context "with filter: 'All'" do
      let(:variables) { { filter: 'All' } }

      it_behaves_like "has expected results"
    end

    context "with filter: 'Storage'" do
      let(:variables) { { filter: 'Storage' } }
      let(:expected_services) { super().select { |s| s.service_code.support_storage? } }

      it_behaves_like "has expected results"
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "services"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "services"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "services"
  end
end
