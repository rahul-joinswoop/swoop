# frozen_string_literal: true

require 'rails_helper'

describe "VehicleClassTypes Query" do
  let(:query) do
    <<~GRAPHQL
      fragment companyFields on Company {
        classTypes {
          edges {
            node {
              id
              name
            }
          }
        }
      }

      query #{operation_name} {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
    GRAPHQL
  end

  shared_examples "classTypes" do
    let(:class_type_1) { create(:vehicle_category, name: VehicleCategory::FLATBED) }
    let(:class_type_2) { create(:vehicle_category, name: VehicleCategory::WHEEL_LIFT) }
    let(:class_types) { [class_type_1, class_type_2] }

    let(:variables) { {} }

    before do
      api_company.vehicle_categories << class_types
    end

    it 'returns expected class types' do
      expect(response).not_to include('errors')
      expected_result = class_types.map { |st| { "id" => st.to_ssid, "name" => st.name } }

      query_result = response.dig(*viewer_company_path, "classTypes", "edges")
      nodes = query_result.to_a.map { |r| r['node'] }

      expect(nodes.size).to eq expected_result.size
      expect(nodes).to match_array expected_result
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    before do
      # client companies can't see this field without this feature
      api_company.features << create(:feature, name: Feature::FLEET_CLASS_TYPE)
    end

    it_behaves_like "classTypes"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "classTypes"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "classTypes"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "classTypes"
  end
end
