# frozen_string_literal: true

require 'rails_helper'

describe "Jobs Query Filtered on States", freeze_time: Time.current.round do
  let(:states) { nil }
  let(:other_rescue_company) { create :rescue_company }
  let(:query) do
    <<~GRAPHQL
      query #{operation_name} {
        jobs#{"(filters: { states: #{states} })" if states} {
          edges {
            node {
              id
            }
          }
        }
      }
    GRAPHQL
  end
  let(:active_fleet_job) do
    create :job,
           fleet_company: fleet_managed_company,
           status: :unassigned
  end
  let(:active_fleet_managed_job) do
    create :fleet_managed_job,
           fleet_company: swoop,
           status: :unassigned
  end
  let(:active_other_job) do
    create :job,
           fleet_company: fleet_motor_club_company,
           rescue_company: other_rescue_company,
           status: :unassigned
  end
  let(:active_rescue_job) do
    create :job,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company,
           status: :unassigned
  end
  let(:done_fleet_job) do
    create :job,
           :completed,
           fleet_company: fleet_managed_company
  end
  let(:done_fleet_managed_job) do
    create :fleet_managed_job,
           :completed,
           fleet_company: swoop
  end
  let(:done_other_job) do
    create :job,
           :completed,
           fleet_company: fleet_motor_club_company,
           rescue_company: other_rescue_company
  end
  let(:done_rescue_job) do
    create :job,
           :completed,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end
  let(:draft_fleet_job) do
    create :job,
           fleet_company: fleet_managed_company,
           status: :draft
  end
  let(:draft_rescue_job) do
    create :job,
           fleet_company: rescue_company,
           rescue_company: rescue_company,
           status: :draft
  end
  let(:map_fleet_job) do
    create :job,
           fleet_company: fleet_managed_company,
           status: :pending
  end
  let(:map_fleet_managed_job) do
    create :fleet_managed_job,
           fleet_company: swoop,
           status: :pending
  end
  let(:map_other_job) do
    create :job,
           fleet_company: fleet_motor_club_company,
           rescue_company: other_rescue_company,
           status: :pending
  end
  let(:map_rescue_job) do
    create :job,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company,
           status: :pending
  end
  let(:predraft_fleet_job) do
    create :job,
           fleet_company: fleet_managed_company,
           status: :predraft
  end
  let(:predraft_fleet_managed_job) do
    create :fleet_managed_job,
           fleet_company: swoop,
           status: :predraft
  end
  let(:predraft_rescue_job) do
    create :job,
           fleet_company: rescue_company,
           rescue_company: rescue_company,
           status: :predraft
  end

  shared_examples "jobs" do
    before do
      jobs = [
        active_fleet_job,
        active_fleet_managed_job,
        active_other_job,
        active_rescue_job,
        done_fleet_job,
        done_fleet_managed_job,
        done_other_job,
        done_rescue_job,
        draft_fleet_job,
        draft_rescue_job,
        map_fleet_job,
        map_fleet_managed_job,
        map_other_job,
        map_rescue_job,
        predraft_fleet_job,
        predraft_fleet_managed_job,
        predraft_rescue_job,
      ]
      # update the timestamps we potentially sort on so that the results
      # from states='Done' will be different from the states
      jobs.each_with_index do |j, i|
        time = DateTime.current + i
        time2 = DateTime.current + (jobs.size * 2) - i
        j.created_at = time
        j.updated_at = time2
        j.adjusted_created_at = time
        j.last_status_changed_at = time2
        j.save!
      end
    end

    shared_examples "it works" do
      it "works" do
        # we didn't blow up
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        response_ids = response.dig("data", "jobs", "edges")&.map { |e| e.dig("node", "id") }
        sorted = expected_jobs.sort do |a, b|
          if states == 'Done'
            b.last_status_changed_at <=> a.last_status_changed_at
          else
            b.adjusted_created_at <=> a.adjusted_created_at
          end
        end.map(&:to_ssid)

        expect(response_ids).to eq(sorted)
      end
    end

    context "with states: Active" do
      let(:states) { 'Active' }
      let(:expected_jobs) { expected_active_jobs }

      it_behaves_like "it works"

      context "with deprecated states: argument" do
        let(:query) do
          <<~GRAPHQL
            query #{operation_name} {
              jobs#{"(states: #{states})" if states} {
                edges {
                  node {
                    id
                  }
                }
              }
            }
          GRAPHQL
        end

        it_behaves_like "it works"
      end
    end

    context "with states: Draft" do
      let(:states) { 'Draft' }
      let(:expected_jobs) { expected_draft_jobs }

      it_behaves_like "it works"
    end

    context "with states: All" do
      let(:states) { 'All' }
      let(:expected_jobs) { expected_all_jobs }

      it_behaves_like "it works"
    end

    context "with states: Done" do
      let(:states) { 'Done' }
      let(:expected_jobs) { expected_done_jobs }

      it_behaves_like "it works"

      context 'when called with states at the top level (deprecated)' do
        let(:query) do
          <<~GRAPHQL
            query #{operation_name} {
              jobs#{"(states: #{states})" if states} {
                edges {
                  node {
                    id
                  }
                }
              }
            }
          GRAPHQL
        end

        it_behaves_like "it works"
      end
    end

    context "with states: Map" do
      let(:states) { 'Map' }
      let(:expected_jobs) { expected_map_jobs }

      it_behaves_like "it works"
    end

    context "without states" do
      let(:states) { nil }
      let(:expected_jobs) { expected_active_jobs }

      it_behaves_like "it works"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"

    let(:expected_all_jobs) do
      expected_active_jobs + expected_done_jobs + expected_draft_jobs
    end
    let(:expected_active_jobs) { [active_fleet_job, active_rescue_job, map_fleet_job, map_rescue_job] }
    let(:expected_draft_jobs) { [draft_fleet_job, predraft_fleet_job] }
    let(:expected_done_jobs) { [done_fleet_job, done_rescue_job] }
    let(:expected_map_jobs) { [map_fleet_job, map_rescue_job] }

    it_behaves_like "jobs"
  end

  context "partner" do
    let(:expected_active_jobs) { [active_rescue_job, map_rescue_job] }
    let(:expected_draft_jobs) { [draft_rescue_job, predraft_rescue_job] }
    let(:expected_done_jobs) { [done_rescue_job] }
    let(:expected_all_jobs) { expected_active_jobs + expected_done_jobs + expected_draft_jobs }
    let(:expected_map_jobs) { [map_rescue_job] }

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "jobs"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "jobs"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "jobs"
    end
  end
end
