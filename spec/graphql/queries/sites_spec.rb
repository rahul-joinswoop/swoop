# frozen_string_literal: true

require 'rails_helper'

describe "Sites Query" do
  let(:rescue_company) { create :rescue_company }

  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($query: String) {
        viewer {
          ...on Application {
            company {
              ...companyFields
            }
          }
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
      fragment companyFields on Company {
        sites(query: $query) {
          edges {
            node {
              id
              name
            }
          }
        }
      }
    GRAPHQL
  end

  let(:query_by_name) { nil }
  let(:variables) { { query: query_by_name } }
  let(:other_company) { create :rescue_company }

  shared_examples "sites" do
    shared_examples "it works" do
      let!(:site_1) { create(:site, company: api_company, name: "Site Test 1") }
      let!(:site_2) { create(:site, company: other_company, name: "Site Test 2") }
      let!(:site_3) { create(:site, company: api_company, name: "Site Test 3") }
      let!(:site_4) { create(:site, company: api_company, name: "Site Test 4", deleted_at: Time.current) }

      it "has expected results" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expected_response = expected_sites || {}
        sites_result = response.dig(*viewer_company_path, "sites", "edges")

        expected_response_attributes = expected_response.map do |site|
          {
            id: site.to_ssid,
            name: site.name,
          }.deep_stringify_keys
        end
        site_nodes = sites_result.to_a.map { |r| r['node'] }

        expect(site_nodes.size).to eq expected_response_attributes.size
        expect(site_nodes).to match_array expected_response_attributes
      end
    end

    let(:expected_sites) { [site_1, site_3] }

    it_behaves_like "it works"

    context "when querying for sites" do
      context "by 'test'" do
        let(:query_by_name) { 'test' }

        it_behaves_like "it works"
      end

      context "by a deleted account name" do
        let(:query_by_name) { 'Site Test 4' }
        let(:expected_sites) { [] }

        it_behaves_like "it works"
      end

      context "when query term is empty" do
        let(:query_by_name) { " " }
        let(:expected_sites) { [] }

        it_behaves_like "it works"
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    before do
      api_company.features << create(:feature, name: Feature::CLIENT_SITES)
    end

    it_behaves_like "sites"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "sites"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "sites"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "sites"
  end
end
