# frozen_string_literal: true

require 'rails_helper'

describe "Job Service Query" do
  describe "service display name" do
    let(:job) do
      create :fleet_managed_job,
             fleet_company: fleet_managed_company,
             rescue_company: rescue_company
    end
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            service {
              name
              displayName
            }
          }
        }
      GRAPHQL
    end

    let(:name) { response.dig('data', 'job', 'service', 'name') }
    let(:display_name) { response.dig('data', 'job', 'service', 'displayName') }
    let(:variables) { { id: job.to_ssid } }

    shared_examples "service" do
      it "works" do
        expect(name).to eq(job.service_code.name)
        expect(display_name).to eq(job.service_code.name)
      end

      context "with will_store set" do
        let(:job) do
          j = super()
          j.update! will_store: true
          j
        end

        it "works" do
          expect(name).to eq(job.service_code.name)
          expect(display_name).to eq("#{job.service_code.name}, Storage")
        end
      end
    end

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "service"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "service"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "service"
    end

    context "as a client api_application" do
      include_context "client api_application"
      it_behaves_like "service"
    end
  end
end
