# frozen_string_literal: true

require 'rails_helper'

describe "Job ETA Query" do
  let(:bidded) { Time.current + 1.hours }
  let(:current) { Time.current + 2.hours }
  let(:live) { Time.current + 3.hours }
  let(:job) do
    job = create :fleet_managed_job,
                 fleet_company: fleet_managed_company,
                 rescue_company: rescue_company

    job.time_of_arrivals << create(:time_of_arrival, time: bidded, eba_type: TimeOfArrival::BID)
    job.time_of_arrivals << create(:time_of_arrival, time: current, eba_type: TimeOfArrival::PARTNER)
    job.time_of_arrivals << create(:time_of_arrival, time: live, eba_type: TimeOfArrival::ESTIMATE)
    job
  end

  let(:variables) { { id: job.to_ssid } }
  let(:eta_bidded) { response.dig('data', 'job', 'eta', 'bidded') }
  let(:eta_current) { response.dig('data', 'job', 'eta', 'current') }
  let(:id) {  response.dig('data', 'job', 'id') }

  shared_examples "partner" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            eta {
              bidded
              current
            }
          }
        }
      GRAPHQL
    end

    it "works" do
      expect(eta_bidded).to eq(bidded.iso8601)
      expect(eta_current).to eq(live.iso8601)
    end
  end

  shared_examples "client" do
    let(:query) do
      <<~GRAPHQL
        query #{operation_name}($id: ID!) {
          job(id: $id) {
            id
            eta {
              current
            }
          }
        }
      GRAPHQL
    end

    it "works" do
      expect(eta_current).to eq(live.iso8601)
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "client"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "partner"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "partner"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "partner"
  end
end
