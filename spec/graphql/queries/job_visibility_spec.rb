# frozen_string_literal: true

require 'rails_helper'

describe "Job Visibility Query" do
  let(:other_fleet_company) { create :fleet_company }
  let(:other_rescue_company) { create :rescue_company }

  # TODO - this really needs to be refactored so it's clearer WTF is going on.
  let(:active_other_job) do
    create :job,
           :with_rescue_vehicle,
           fleet_company: other_fleet_company,
           rescue_company: other_rescue_company,
           status: :unassigned
  end

  let(:active_fleet_motor_club_job) do
    create :fleet_motor_club_job,
           :with_rescue_vehicle,
           fleet_company: fleet_managed_company,
           status: :unassigned,
           issc_dispatch_request: create(:issc_dispatch_request, max_eta: 90)
  end

  let(:active_rescue_job) do
    create :rescue_job,
           :with_rescue_vehicle,
           :with_storage_site,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company,
           status: :unassigned
  end

  let(:other_rescue_job) do
    create :rescue_job,
           :with_rescue_vehicle,
           :with_storage_site,
           fleet_company: other_fleet_company,
           rescue_company: other_rescue_company,
           status: :unassigned
  end

  let(:active_fleet_managed_job) do
    create :fleet_managed_job,
           :with_rescue_vehicle,
           fleet_company: swoop,
           status: :unassigned
  end

  let(:expected_job_id) { job_id }
  let(:expected_other_job_id) { nil }
  let(:other_job) { active_other_job }

  shared_examples "job" do
    let(:variables) do
      {
        id: job_id,
      }
    end

    shared_examples "with a job from a different company" do
      let(:job_id) { other_job.to_ssid }
      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expect(response_id).to eq(expected_other_job_id)
      end
    end

    shared_examples "with a valid id" do
      let(:job_id) { job&.to_ssid }

      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expect(response_id).to eq(expected_job_id)
      end
    end

    shared_examples "invalid id" do
      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')
        expect(response_id).to eq(nil)
      end
    end

    shared_examples "with an unparsable id" do
      # this will raise an error in SomewhatSecureID
      let(:job_id) { 1231234 }

      it_behaves_like "invalid id"
    end

    shared_examples "with an non-existent id" do
      # this will raise an ActiveRecord::RecordNotFound
      let(:job_id) { Job.new(id: 123123).to_ssid }

      it_behaves_like "invalid id"
    end

    context "node" do
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            node(id: "#{job_id}") {
              ...on Job {
                id
              }
            }
          }
        GRAPHQL
      end
      let(:response_id) do
        response.dig("data", "node", "id")
      end

      it_behaves_like "with a valid id"
      it_behaves_like "with an unparsable id"
      it_behaves_like "with an non-existent id"
      it_behaves_like "with a job from a different company"
    end

    context "job" do
      let(:query) do
        <<~GRAPHQL
          query #{operation_name} {
            job(id: "#{job_id}") {
              id
            }
          }
        GRAPHQL
      end

      let(:response_id) do
        response.dig("data", "job", "id")
      end

      it_behaves_like "with a valid id"
      it_behaves_like "with an unparsable id"
      it_behaves_like "with an non-existent id"
      it_behaves_like "with a job from a different company"
    end
  end

  context "as a super client managed company" do
    context 'as a super client_api application' do
      include_context "super client api_application"

      context "on a fleet job" do
        # this is someone else's fleet managed job, we should be able to see it
        let(:job) { active_fleet_managed_job }
        # this is someone else's fleet job motor club job, we should not be able to see this
        let(:active_other_job) { active_fleet_motor_club_job }
        let(:expected_other_job_id) { nil }
        let(:other_job) { active_other_job }

        it_behaves_like "job"
      end

      context "on a rescue job" do
        # can't see any rescue jobs
        let(:job) { active_rescue_job }
        let(:expected_job_id) { nil }
        let(:other_job) { other_rescue_job }
        let(:expected_other_job_id) { nil }

        it_behaves_like "job"
      end
    end
  end

  context "as a client" do
    context 'as a client_api application' do
      include_context "client api_application"

      # we can see jobs that we're the fleet_company on
      let(:job) { active_fleet_motor_club_job }

      it_behaves_like "job"
    end
  end

  context "as a partner" do
    # we can see jobs that we're the rescue_company on
    let(:job) { active_rescue_job }

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "job"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "job"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "job"
    end
  end
end
