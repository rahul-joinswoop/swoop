# frozen_string_literal: true

require 'rails_helper'

describe "Accounts Query" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($query: String) {
        viewer {
          ...on User {
            company {
              ...companyFields
            }
          }
          ...on Company {
            ...companyFields
          }
        }
      }
      fragment companyFields on Company {
        accounts(query: $query) {
          edges {
            node {
              id
              name
            }
          }
        }
      }
    GRAPHQL
  end

  let(:query_by_name) { nil }
  let(:variables) { { query: query_by_name } }

  shared_examples "accounts" do
    let!(:account_null_client_company) { api_company.accounts.find_or_create_by(company: api_company, client_company: nil) }
    let!(:account_swoop) { create(:account, company: api_company, client_company: swoop, name: 'Swoop') }
    let!(:account_cash_call) { create(:account, company: api_company, name: 'Cash Call') }
    let!(:account_fleet) { create(:account, company: api_company, client_company: fleet_managed_company, name: 'Fleet Company') }
    let!(:account_pacoca) { create(:account, company: api_company, name: 'Paçoca') }
    let!(:account_deleted) { create(:account, company: api_company, name: 'Deleted Account', deleted_at: Time.now) }
    let(:expected_accounts) { [] }
    shared_examples "successfully listing expected accounts" do
      it "works" do
        expect { response }.not_to raise_error
        expect(response).not_to include('errors')

        expected_response = expected_accounts
        accounts_result = response.dig(*viewer_company_path, "accounts", "edges")

        expected_response_attributes = expected_response.map do |account|
          {
            id: account.to_ssid,
            name: account.name,
          }.deep_stringify_keys
        end
        account_nodes = accounts_result.to_a.map { |r| r['node'] }

        expect(account_nodes.size).to eq expected_response_attributes.size
        expect(account_nodes).to match_array expected_response_attributes
      end
    end

    context 'with no filter' do
      let(:expected_accounts) { [account_swoop, account_cash_call, account_fleet, account_pacoca, account_null_client_company] }

      it_behaves_like "successfully listing expected accounts"
    end

    context "with filter: 'Create'" do
      let(:variables) { super().tap { |v| v[:filter] = 'Create' } }
      let(:expected_accounts) { [account_swoop, account_cash_call, account_fleet, account_pacoca, account_null_client_company] }

      it_behaves_like "successfully listing expected accounts"
    end

    context "with filter: 'All'" do
      let(:expected_accounts) { [account_swoop, account_cash_call, account_fleet, account_pacoca, account_null_client_company] }

      let(:variables) { super().tap { |v| v[:filter] = 'All' } }

      it_behaves_like "successfully listing expected accounts"
    end

    context "when querying for accounts" do
      context "by 'Cash'" do
        let(:query_by_name) { 'Cash' }
        let(:expected_accounts) { [account_cash_call] }

        it_behaves_like "successfully listing expected accounts"
      end

      context "by 'cash' lower case" do
        let(:query_by_name) { 'cash' }
        let(:expected_accounts) { [account_cash_call] }

        it_behaves_like "successfully listing expected accounts"
      end

      context "by 'Swoop'" do
        let(:query_by_name) { 'Swoop' }
        let(:expected_accounts) { [account_swoop] }

        it_behaves_like "successfully listing expected accounts"
      end

      context "by 'swoop' lower case" do
        let(:query_by_name) { 'swoop' }
        let(:expected_accounts) { [account_swoop] }

        it_behaves_like "successfully listing expected accounts"
      end

      context "by 'ç' lower case" do
        let(:query_by_name) { 'ç' }
        let(:expected_accounts) { [account_pacoca] }

        it_behaves_like "successfully listing expected accounts"
      end

      context "by a deleted account name" do
        let(:query_by_name) { 'Deleted' }
        let(:expected_accounts) { [] }

        it_behaves_like "successfully listing expected accounts"
      end

      context "when query term is empty" do
        let(:query_by_name) { " " }

        let(:expected_accounts) { [] }

        it_behaves_like "successfully listing expected accounts"
      end

      context "when querying for a deleted account" do
        let(:query_by_name) { account_deleted.name }

        let(:expected_accounts) { [] }

        it 'exists' do
          expect(api_company.accounts.find(account_deleted.id)).to be_present
          expect(api_company.accounts.find(account_deleted.id).deleted_at).to be_present
        end

        it_behaves_like "successfully listing expected accounts"
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "accounts"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "accounts"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "accounts"
  end
end
