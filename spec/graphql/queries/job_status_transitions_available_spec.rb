# frozen_string_literal: true

require 'rails_helper'

describe "Job Status Transitions Available Query" do
  let(:query) do
    <<~GRAPHQL
      query #{operation_name}($id: ID!) {
        job(id: $id) {
          id
          statusTransitionsAvailable {
            edges {
              node
            }
          }
        }
      }
    GRAPHQL
  end

  let(:status) { :pending }
  let!(:job) { create :fleet_motor_club_job, status: status, fleet_company: fleet_managed_company, rescue_company: rescue_company }
  let(:variables) { { id: job.to_ssid } }
  let(:job_response) { response.dig('data', 'job') }
  let(:status_transitions_available) { job_response.dig('statusTransitionsAvailable', 'edges').map { |e| e.dig('node') } }

  shared_examples "statusTransitionsAvailable" do
    context 'with an assigned job' do
      let(:status) { :assigned }

      it "works" do
        expect(status_transitions_available).to eq(expected_assigned)
      end
    end

    context 'with a rejected job' do
      let(:status) { :rejected }

      it "works" do
        expect(status_transitions_available).to eq(expected_rejected)
      end
    end

    context 'with a stored job' do
      let(:status) { :stored }

      it "works" do
        expect(status_transitions_available).to eq(expected_stored)
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    let(:expected_assigned) { ["Canceled"] }
    let(:expected_rejected) { [] }
    let(:expected_stored) { ['Released'] }

    it_behaves_like "statusTransitionsAvailable"
  end

  context "partner" do
    let(:expected_assigned) { ['Completed', 'Rejected', 'Accepted'] }
    let(:expected_rejected) { [] }
    let(:expected_stored) { ['Released'] }

    context "as a partner api_user" do
      include_context "partner api_user"
      it_behaves_like "statusTransitionsAvailable"
    end

    context "as a 3rd party partner api_user" do
      include_context "3rd party partner api_user"
      it_behaves_like "statusTransitionsAvailable"
    end

    context "as a 3rd party partner api_company" do
      include_context "3rd party partner api_company"
      it_behaves_like "statusTransitionsAvailable"
    end
  end
end
