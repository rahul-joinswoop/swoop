# frozen_string_literal: true

require 'rails_helper'

describe 'default if_context filters' do
  shared_examples 'mutations' do |visibility|
    # createJob is visible to everyone before if_context filters run
    subject { response.dig('data', '__type', 'fields').any? { |f| f['name'] == 'createJob' } }

    let(:query) do
      <<~GRAPHQL
      query #{operation_name} {
        __type(name: "Mutation") {
          name
          fields {
            name
          }
        }
      }
      GRAPHQL
    end

    it 'works' do
      expect(subject).to eq(visibility)
    end
  end

  shared_examples 'subscriptions' do |visibility|
    # activeJobsFeed is visible to everyone before if_context filters run
    subject { response.dig('data', '__type', 'fields').any? { |f| f['name'] == 'activeJobsFeed' } }

    let(:query) do
      <<~GRAPHQL
      query #{operation_name} {
        __type(name: "Subscription") {
          name
          fields {
            name
          }
        }
      }
      GRAPHQL
    end

    it "works" do
      expect(subject).to eq(visibility)
    end
  end

  shared_examples 'queries' do |visibility|
    # jobs is visible to everyone before if_context filters run
    subject { response.dig('data', '__type', 'fields').any? { |f| f['name'] == 'jobs' } }

    let(:query) do
      <<~GRAPHQL
      query #{operation_name} {
        __type(name: "Query") {
          name
          fields {
            name
          }
        }
      }
      GRAPHQL
    end

    it "works" do
      expect(subject).to eq(visibility)
    end
  end

  context 'as a partner company' do
    let(:api_company) { rescue_company }

    context 'as an api_user' do
      let(:api_user) { create :user, :rescue, :driver, company: api_company }
      let(:viewer) { api_user }

      it_behaves_like 'queries', true
      it_behaves_like 'mutations', true
      it_behaves_like 'subscriptions', true
    end

    context 'as a third party partner api client' do
      let(:api_application) { third_party_partner_api_application }
      let(:api_company) { rescue_company }

      context 'as an api_company' do
        let(:viewer) { api_company }

        it_behaves_like 'queries', true
        it_behaves_like 'mutations', false
        it_behaves_like 'subscriptions', true
      end

      context 'as an api_user' do
        let(:api_user) { create :user, :rescue, :driver, company: api_company }
        let(:viewer) { api_user }

        it_behaves_like 'queries', true
        it_behaves_like 'mutations', true
        it_behaves_like 'subscriptions', false
      end
    end
  end

  context 'as a client company' do
    let(:api_company) { fleet_managed_company }

    context 'as an api_application' do
      let(:api_application) { fleet_managed_application }
      let(:viewer) { api_application }

      it_behaves_like 'queries', true
      it_behaves_like 'mutations', true
      it_behaves_like 'subscriptions', true
    end
  end
end
