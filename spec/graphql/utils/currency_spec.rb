# frozen_string_literal: true

require 'rails_helper'

describe Utils::Currency do
  include Utils::Currency

  let(:decimal_places) { nil }

  describe '.currency_value_or_nil' do
    shared_examples 'format the value as expected' do |expected_value|
      it 'formats it correctly' do
        if decimal_places
          expect(currency_value_or_nil(value, decimal_places: decimal_places)).to eq expected_value
        else
          expect(currency_value_or_nil(value)).to eq expected_value
        end
      end
    end

    context 'when a value is passed' do
      let(:value) { '2' }

      it_behaves_like 'format the value as expected', '2.00'

      context 'when decimal_places is also passed' do
        let(:decimal_places) { 1 }

        it_behaves_like 'format the value as expected', '2.0'
      end
    end

    context 'when nil value is passed' do
      let(:value) { nil }

      it_behaves_like 'format the value as expected', nil

      context 'when decimal_places is also passed' do
        let(:decimal_places) { 1 }

        it_behaves_like 'format the value as expected', nil
      end
    end
  end

  describe '.currency_value_or_zero' do
    shared_examples 'format the value as expected' do |expected_value|
      it 'formats it correctly' do
        if decimal_places
          expect(currency_value_or_zero(value, decimal_places: decimal_places)).to eq expected_value
        else
          expect(currency_value_or_zero(value)).to eq expected_value
        end
      end
    end

    context 'when a value is passed' do
      let(:value) { '2' }

      it_behaves_like 'format the value as expected', '2.00'

      context 'when decimal_places is also passed' do
        let(:decimal_places) { 1 }

        it_behaves_like 'format the value as expected', '2.0'
      end
    end

    context 'when nil value is passed' do
      let(:value) { nil }

      it_behaves_like 'format the value as expected', '0.00'

      context 'when decimal_places is also passed' do
        let(:decimal_places) { 1 }

        it_behaves_like 'format the value as expected', '0.0'
      end
    end
  end
end
