# frozen_string_literal: true

require 'rails_helper'

describe Utils::QueryVariableInliner do
  include Utils::QueryVariableInliner
  let(:id) { create(:job).to_ssid }

  context 'subscriptions' do
    subject { inline!(query) }

    let(:subscription_inline_query) do
      <<~GRAPHQL.chomp
        subscription JobListPageJobUpdatedSubscription {
          jobUpdated(id: #{id.to_json}) {
            id
            status
          }
        }
      GRAPHQL
    end
    let(:subscription_variable_query) do
      <<~GRAPHQL.chomp
        subscription JobListPageJobUpdatedSubscription($id: ID!) {
          jobUpdated(id: $id) {
            id
            status
          }
        }
      GRAPHQL
    end

    context "with variables" do
      let(:query) do
        {
          query: subscription_variable_query,
          variables: { id: id }.deep_stringify_keys!,
          context: {},
        }
      end

      it 'works' do
        expect { subject }
          .to change { query[:document]&.to_query_string }.from(nil).to(subscription_inline_query)
          .and change { query[:query] }.to(nil)
          .and change { query[:variables] }.to({})
      end
    end

    context "without variables" do
      let(:query) do
        {
          query: subscription_inline_query,
          variables: {},
          context: {},
        }
      end

      it 'works' do
        expect { subject }
          .to avoid_changing { query[:document]&.to_query_string }
          .and avoid_changing { query[:query] }
          .and avoid_changing { query[:variables] }
      end
    end
  end

  context "job mutations" do
    let!(:job_input) { create :graphql_job_input_type, :with_answers }
    let(:status) { "Assigned" }
    let(:input) { { job: job_input } }

    #  createJob and updateJob both share the gigantic input structure
    let(:customer) do
      GraphQL::Language::Nodes::Argument.new(
        name: "customer",
        value: GraphQL::Language::Nodes::InputObject.new(
          arguments: [
            GraphQL::Language::Nodes::Argument.new(name: 'name', value: job_input[:customer][:name]),
            GraphQL::Language::Nodes::Argument.new(name: 'phone', value: job_input[:customer][:phone]),
          ]
        )
      )
    end
    let(:notes) do
      GraphQL::Language::Nodes::Argument.new(
        name: 'notes',
        value: GraphQL::Language::Nodes::InputObject.new(
          arguments: [
            GraphQL::Language::Nodes::Argument.new(name: 'customer', value: job_input[:notes][:customer]),
            GraphQL::Language::Nodes::Argument.new(name: 'internal', value: job_input[:notes][:internal]),
          ]
        )
      )
    end
    let(:answers) do
      GraphQL::Language::Nodes::Argument.new(
        name: 'answers',
        value: job_input[:service][:answers].map do |a|
                 GraphQL::Language::Nodes::InputObject.new(
                   arguments: [:answer, :question, :extra].map do |k|
                     if a[k].present?
                       GraphQL::Language::Nodes::Argument.new(name: k.to_s, value: a[k])
                     else
                       nil
                     end
                   end.reject(&:blank?)
                 )
               end
      )
    end
    let(:service) do
      GraphQL::Language::Nodes::Argument.new(
        name: 'service',
        value: GraphQL::Language::Nodes::InputObject.new(
          arguments: [
            *[:name, :willStore, :storageType].map do |k|
              GraphQL::Language::Nodes::Argument.new(name: k.to_s, value: job_input[:service][k])
            end,
            answers,
          ]
        )
      )
    end
    let(:vehicle) do
      GraphQL::Language::Nodes::Argument.new(
        name: "vehicle",
        value: GraphQL::Language::Nodes::InputObject.new(
          arguments: [:make, :model, :year, :color, :license, :odometer, :vin].map do |a|
            GraphQL::Language::Nodes::Argument.new(name: a.to_s, value: job_input[:vehicle][a])
          end
        )
      )
    end
    let(:locations) do
      [:serviceLocation, :dropoffLocation].map do |l|
        GraphQL::Language::Nodes::Argument.new(
          name: l.to_s,
          value: GraphQL::Language::Nodes::InputObject.new(
            arguments: [:address, :lat, :lng, :locationType, :exact, :googlePlaceId].map do |k|
                         if k == :exact && job_input[:location][l][k].nil?
                           nil
                         else
                           GraphQL::Language::Nodes::Argument.new(
                             name: k.to_s,
                             value: job_input[:location][l][k]
                           )
                         end
                       end.reject(&:blank?)
          )
        )
      end
    end
    let(:contacts) do
      [:pickupContact, :dropoffContact].map do |c|
        GraphQL::Language::Nodes::Argument.new(
          name: c.to_s,
          value: GraphQL::Language::Nodes::InputObject.new(
            arguments: [:name, :phone].map do |k|
              GraphQL::Language::Nodes::Argument.new(name: k.to_s, value: job_input[:location][c][k])
            end
          )
        )
      end
    end
    let(:location) do
      GraphQL::Language::Nodes::Argument.new(
        name: 'location',
        value: GraphQL::Language::Nodes::InputObject.new(
          arguments: [
            *locations,
            *contacts,
          ]
        )
      )
    end
    let(:texts) do
      GraphQL::Language::Nodes::Argument.new(
        name: 'texts',
        value: GraphQL::Language::Nodes::InputObject.new(
          arguments: [:sendLocation, :sendEtaUpdates, :sendDriverTracking, :sendReview, :sendTextsToPickup].map do |k|
            GraphQL::Language::Nodes::Argument.new(name: k.to_s, value: job_input[:texts][k])
          end
        )
      )
    end
    let(:job) do
      GraphQL::Language::Nodes::InputObject.new(
        arguments: [
          GraphQL::Language::Nodes::Argument.new(
            name: 'job',
            value: GraphQL::Language::Nodes::InputObject.new(
              arguments: job_arguments
            )
          ),
        ]
      )
    end

    shared_examples_for '#value_to_arguments' do
      shared_examples_for 'it works' do
        it 'works' do
          args = subject.value_to_arguments(input, klass, line, col)
          expect(args).to eql(job)
          expect(args.arguments.first.position).to eq([line, col])
        end
      end

      context "with a location" do
        let(:line) { 3 }
        let(:col) { 2 }

        it_behaves_like 'it works'
      end

      context "without a location" do
        let(:line) { nil }
        let(:col) { nil }

        it_behaves_like 'it works'
      end
    end

    shared_examples_for '#inline!' do
      subject { inline!(query) }

      context 'with good query variables' do
        let(:query) do
          {
            query: variable_query,
            variables: { input: input.deeper_dup }.deep_stringify_keys!,
            context: {},
          }
        end

        it 'works' do
          expect(Rollbar).not_to receive(:error)

          expect { subject }
            .to change { query[:document]&.to_query_string }
            .from(nil).to(inline_query)
            .and change { query[:query] }
            .from(variable_query).to(nil)
            .and change { query[:variables] }
            .from(query[:variables]).to({})

          expect(subject).to eq(true)
        end
      end

      shared_examples_for 'handles errors' do
        it 'works' do
          # we have to #deeper_dup query here because otherwise we end up with the modified version
          # here in the context of our expectation (even though after our second expectation query
          # is correct - i don't understand why this is happening...)
          expect(Rollbar).to receive(:error)
            .once.with(anything, query.slice(:query, :variables).deeper_dup)

          expect { subject }
            .to avoid_changing { query[:document]&.to_query_string }
            .and avoid_changing { query[:query] }
            .and avoid_changing { query[:variables] }

          expect(subject).to eq(false)
        end
      end

      context 'with bad query variables' do
        # if our query has extra variables we can't do anything with it since we don't
        # know the type so we bail and avoid making any changes to our query. we then
        # let graphql handle this (TODO - will this result in the undesirable error messages?)
        let(:query) do
          {
            query: variable_query,
            variables: { input: { **input.deeper_dup, foo: { bar: 1 } } }.deep_stringify_keys!,
            context: {},
          }
        end

        it_behaves_like 'handles errors'
      end

      context 'with bad query' do
        let(:query) do
          {
            query: "asdfasdf#{variable_query}",
            variables: { input: input }.deep_stringify_keys!,
            context: {},
          }
        end

        it_behaves_like 'handles errors'
      end

      shared_examples_for "doesn't modify query" do
        it 'works' do
          expect(Rollbar).not_to receive(:error)

          expect { subject }
            .to avoid_changing { query[:document]&.to_query_string }
            .and avoid_changing { query[:query] }
            .and avoid_changing { query[:variables] }

          expect(subject).to eq(nil)
        end
      end

      context 'without query variables' do
        let(:query) do
          {
            query: inline_query,
            context: {},
          }
        end

        it_behaves_like "doesn't modify query"
      end

      context 'with Feature::LEGACY_GRAPHQL_ERRORS enabled' do
        let(:company) do
          c = create(:company)
          c.features << create(:feature, name: Feature::LEGACY_GRAPHQL_ERRORS)
          c
        end
        let(:query) do
          {
            query: variable_query,
            variables: { input: input.deeper_dup }.deep_stringify_keys!,
            context: {
              api_company: company,
              api_roles: [],
            },
          }
        end

        it_behaves_like "doesn't modify query"
      end
    end

    context 'createJob' do
      let(:variable_query) do
        <<~GRAPHQL.strip
          mutation CreateJob($input: CreateJobInput!) {
            createJob(input: $input) {
              job {
                id
              }
            }
          }
        GRAPHQL
      end
      let(:inline_query) do
        <<~GRAPHQL.strip
          mutation CreateJob {
            createJob(input: #{job.to_query_string}) {
              job {
                id
              }
            }
          }
        GRAPHQL
      end

      let(:klass) { SwoopSchema.types['CreateJobInput'] }
      let(:job_arguments) do
        [
          customer,
          notes,
          service,
          vehicle,
          location,
          texts,
        ]
      end

      it_behaves_like '#value_to_arguments'
      it_behaves_like '#inline!'
    end

    context "UpdateJob" do
      let(:variable_query) do
        <<~GRAPHQL.strip
          mutation UpdateJob($input: UpdateJobInput!) {
            updateJob(input: $input) {
              job {
                id
              }
            }
          }
        GRAPHQL
      end
      let(:inline_query) do
        <<~GRAPHQL.strip
          mutation UpdateJob {
            updateJob(input: #{job.to_query_string}) {
              job {
                id
              }
            }
          }
        GRAPHQL
      end

      let(:job_input) { super().deeper_dup.tap { |j| j[:id] = id } }

      let(:klass) { SwoopSchema.types['UpdateJobInput'] }
      let(:job_arguments) do
        [
          customer,
          notes,
          service,
          vehicle,
          location,
          texts,
          GraphQL::Language::Nodes::Argument.new(
            name: 'id',
            value: id
          ),
        ]
      end

      it_behaves_like '#value_to_arguments'
      it_behaves_like '#inline!'
    end

    context "UpdateJobStatus" do
      let(:variable_query) do
        <<~GRAPHQL.strip
          mutation UpdateJobStatus($input: UpdateJobStatusInput!) {
            updateJobStatus(input: $input) {
              job {
                id
              }
            }
          }
        GRAPHQL
      end
      let(:inline_query) do
        <<~GRAPHQL.strip
          mutation UpdateJobStatus {
            updateJobStatus(input: #{job.to_query_string}) {
              job {
                id
              }
            }
          }
        GRAPHQL
      end

      let(:job_input) { { id: id, status: status } }

      let(:klass) { SwoopSchema.types['UpdateJobStatusInput'] }
      let(:job_arguments) do
        [
          GraphQL::Language::Nodes::Argument.new(name: 'id', value: id),
          GraphQL::Language::Nodes::Argument.new(
            name: 'status',
            value: GraphQL::Language::Nodes::Enum.new(name: status)
          ),
        ]
      end

      it_behaves_like '#value_to_arguments'
      it_behaves_like '#inline!'
    end
  end

  context "invoice mutations" do
    subject { inline!(query) }

    context "updateInvoice" do
      let(:invoice_variable_query) do
        <<~GRAPHQL.chomp
          mutation InvoiceScreenSaveMutation($input: UpdateInvoiceInput!) {
            updateInvoice(input: $input) {
              invoice {
                id
                rateType {
                  type
                  name
                }
                classType
                lineItems {
                  edges {
                    node {
                      description
                      netAmount
                      quantity
                      unitPrice
                      deletedAt
                      id
                      autoCalculatedQuantity
                      calcType
                      estimated
                      originalQuantity
                      originalUnitPrice
                      rate {
                        id
                      }
                      userCreated
                      taxAmount
                    }
                  }
                }
                subtotal
                totalAmount
                payments {
                  edges {
                    node {
                      memo
                      paymentMethod
                      amount
                      id
                      type
                    }
                  }
                }
                balance
                notes
                job {
                  id
                  account {
                    name
                    id
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      let(:invoice_inline_query) do
        <<~GRAPHQL.chomp
        mutation InvoiceScreenSaveMutation {
          updateInvoice(input: {invoice: {id: "3DmH9uTylZ6UZJc31m8sgk", classType: "Heavy Duty", lineItems: [{calcType: Flat, description: "Flat Rate", quantity: "1.00", unitPrice: "105.00", deletedAt: null, id: "7C0yNlfMIbS0pI2mVd8ozb", autoCalculatedQuantity: true, netAmount: "105.00", estimated: true, originalQuantity: "1.00", originalUnitPrice: "105.00", rate: {id: "6GYo16ibVJcDxAQEinShk8"}, userCreated: false, taxAmount: "0.00"}, {calcType: Flat, description: "Tax", quantity: "0.00", unitPrice: "0.00", deletedAt: null, id: "4iIgxuhE0bLFoIxaD581YY", autoCalculatedQuantity: true, netAmount: "0.00", estimated: true, originalQuantity: "0.00", originalUnitPrice: "0.00", rate: {id: "6GYo16ibVJcDxAQEinShk8"}, userCreated: false, taxAmount: "0.00"}, {calcType: Flat, description: "Labor", quantity: "1.00", unitPrice: "75.00", deletedAt: null, id: "9eeeb283a54084e9e78086f01133439d", autoCalculatedQuantity: true, netAmount: "75.00", estimated: false, originalQuantity: "1.00", originalUnitPrice: "0.00", rate: null, userCreated: true, taxAmount: "0.00"}], subtotal: "180.00", totalAmount: "180.00", payments: [], balance: "180.00", notes: "Includes Drive time and First hour ", job: {id: "4TNFQUDroF8cYan501FVXV"}, rateType: {type: Flat}}}) {
            invoice {
              id
              rateType {
                type
                name
              }
              classType
              lineItems {
                edges {
                  node {
                    description
                    netAmount
                    quantity
                    unitPrice
                    deletedAt
                    id
                    autoCalculatedQuantity
                    calcType
                    estimated
                    originalQuantity
                    originalUnitPrice
                    rate {
                      id
                    }
                    userCreated
                    taxAmount
                  }
                }
              }
              subtotal
              totalAmount
              payments {
                edges {
                  node {
                    memo
                    paymentMethod
                    amount
                    id
                    type
                  }
                }
              }
              balance
              notes
              job {
                id
                account {
                  name
                  id
                }
              }
            }
          }
        }
        GRAPHQL
      end

      let(:variables) do
        {
          input: {
            invoice: {
              id: "3DmH9uTylZ6UZJc31m8sgk",
              classType: "Heavy Duty",
              lineItems: [
                {
                  calcType: "Flat",
                  description: "Flat Rate",
                  quantity: "1.00",
                  unitPrice: "105.00",
                  deletedAt: nil,
                  id: "7C0yNlfMIbS0pI2mVd8ozb",
                  autoCalculatedQuantity: true,
                  netAmount: "105.00",
                  estimated: true,
                  originalQuantity: "1.00",
                  originalUnitPrice: "105.00",
                  rate: { id: "6GYo16ibVJcDxAQEinShk8" },
                  userCreated: false,
                  taxAmount: "0.00",
                }, {
                  calcType: "Flat",
                  description: "Tax",
                  quantity: "0.00",
                  unitPrice: "0.00",
                  deletedAt: nil,
                  id: "4iIgxuhE0bLFoIxaD581YY",
                  autoCalculatedQuantity: true,
                  netAmount: "0.00",
                  estimated: true,
                  originalQuantity: "0.00",
                  originalUnitPrice: "0.00",
                  rate: { id: "6GYo16ibVJcDxAQEinShk8" },
                  userCreated: false,
                  taxAmount: "0.00",
                }, {
                  calcType: "Flat",
                  description: "Labor",
                  quantity: "1.00",
                  unitPrice: "75.00",
                  deletedAt: nil,
                  id: "9eeeb283a54084e9e78086f01133439d",
                  autoCalculatedQuantity: true,
                  netAmount: "75.00",
                  estimated: false,
                  originalQuantity: "1.00",
                  originalUnitPrice: "0.00",
                  rate: nil,
                  userCreated: true,
                  taxAmount: "0.00",
                },
              ],
              subtotal: "180.00",
              totalAmount: "180.00",
              payments: [],
              balance: "180.00",
              notes: "Includes Drive time and First hour ",
              job: { id: "4TNFQUDroF8cYan501FVXV" },
              rateType: { type: "Flat" },
            },
          },
        }
      end

      context "with variables" do
        let(:query) do
          {
            query: invoice_variable_query,
            variables: variables.deep_stringify_keys,
            context: {},
          }
        end

        it 'works' do
          expect { subject }
            .to change { query[:document]&.to_query_string }.from(nil).to(invoice_inline_query)
            .and change { query[:query] }.to(nil)
            .and change { query[:variables] }.to({})
        end
      end

      context "without variables" do
        let(:query) do
          {
            query: invoice_inline_query,
            variables: {},
            context: {},
          }
        end

        it 'works' do
          expect { subject }
            .to avoid_changing { query[:document]&.to_query_string }
            .and avoid_changing { query[:query] }
            .and avoid_changing { query[:variables] }
        end
      end
    end

    context "sampleInvoice" do
      let(:invoice_variable_query) do
        <<~GRAPHQL.chomp
        mutation InvoiceScreenRecalcMutation($input: SampleInvoiceInput!) {
          sampleInvoice(input: $input) {
            invoice {
              id
              rateType {
                type
                name
              }
              classType
              lineItems {
                edges {
                  node {
                    description
                    netAmount
                    quantity
                    unitPrice
                    deletedAt
                    id
                    autoCalculatedQuantity
                    calcType
                    estimated
                    originalQuantity
                    originalUnitPrice
                    rate {
                      id
                    }
                    userCreated
                    taxAmount
                  }
                }
              }
              subtotal
              totalAmount
              payments {
                edges {
                  node {
                    memo
                    paymentMethod
                    amount
                    id
                    type
                  }
                }
              }
              balance
              notes
              job {
                id
                account {
                  name
                  id
                }
              }
            }
          }
        }
        GRAPHQL
      end

      let(:invoice_inline_query) do
        <<~GRAPHQL.chomp
          mutation InvoiceScreenRecalcMutation {
            sampleInvoice(input: {invoice: {id: "6HlgUZpPRTlIYohjk0vDe", classType: "Heavy Duty", lineItems: [{calcType: Flat, description: "Hookup", quantity: "1.00", unitPrice: "125.00", deletedAt: null, id: "5VkXUFIinEdHuMUZBTeV4h", autoCalculatedQuantity: true, netAmount: "125.00", estimated: true, originalQuantity: "1.00", originalUnitPrice: "125.00", rate: {id: "46eP41iSeosNr9uKwROohx"}, userCreated: false, taxAmount: "0.00"}, {calcType: Flat, description: "Towed Per Mile", quantity: "4.00", unitPrice: "5.90", deletedAt: null, id: "2npXlRHGIE1OBRQeGOWXoT", autoCalculatedQuantity: true, netAmount: "23.60", estimated: true, originalQuantity: "4.00", originalUnitPrice: "5.90", rate: {id: "46eP41iSeosNr9uKwROohx"}, userCreated: false, taxAmount: "0.00"}, {calcType: Flat, description: "Towed Free Miles", quantity: "0.00", unitPrice: "-5.90", deletedAt: "2019-04-18T21:29:02.124Z", id: "1BY7W6dWvxV8Wnwh0P9KoW", autoCalculatedQuantity: true, netAmount: "0.00", estimated: true, originalQuantity: "0.00", originalUnitPrice: "-5.90", rate: {id: "46eP41iSeosNr9uKwROohx"}, userCreated: false, taxAmount: "0.00"}, {calcType: Flat, description: "Tax", quantity: "0.00", unitPrice: "0.00", deletedAt: null, id: "7T3LSqq5lVO975Mc2OQDQX", autoCalculatedQuantity: true, netAmount: "0.00", estimated: true, originalQuantity: "0.00", originalUnitPrice: "0.00", rate: {id: "46eP41iSeosNr9uKwROohx"}, userCreated: false, taxAmount: "0.00"}], subtotal: "148.60", totalAmount: "148.60", payments: [], balance: "148.60", job: {id: "tPtBWV059BkWeQsKmr3o"}, rateType: {type: MileageTowed}, notes: ""}}) {
              invoice {
                id
                rateType {
                  type
                  name
                }
                classType
                lineItems {
                  edges {
                    node {
                      description
                      netAmount
                      quantity
                      unitPrice
                      deletedAt
                      id
                      autoCalculatedQuantity
                      calcType
                      estimated
                      originalQuantity
                      originalUnitPrice
                      rate {
                        id
                      }
                      userCreated
                      taxAmount
                    }
                  }
                }
                subtotal
                totalAmount
                payments {
                  edges {
                    node {
                      memo
                      paymentMethod
                      amount
                      id
                      type
                    }
                  }
                }
                balance
                notes
                job {
                  id
                  account {
                    name
                    id
                  }
                }
              }
            }
          }
        GRAPHQL
      end

      let(:variables) do
        {
          input: {
            invoice: {
              id: "6HlgUZpPRTlIYohjk0vDe",
              classType: "Heavy Duty",
              lineItems: [
                {
                  calcType: "Flat",
                  description: "Hookup",
                  quantity: "1.00",
                  unitPrice: "125.00",
                  deletedAt: nil,
                  id: "5VkXUFIinEdHuMUZBTeV4h",
                  autoCalculatedQuantity: true,
                  netAmount: "125.00",
                  estimated: true,
                  originalQuantity: "1.00",
                  originalUnitPrice: "125.00",
                  rate: { id: "46eP41iSeosNr9uKwROohx" },
                  userCreated: false,
                  taxAmount: "0.00",
                }, {
                  calcType: "Flat",
                  description: "Towed Per Mile",
                  quantity: "4.00",
                  unitPrice: "5.90",
                  deletedAt: nil,
                  id: "2npXlRHGIE1OBRQeGOWXoT",
                  autoCalculatedQuantity: true,
                  netAmount: "23.60",
                  estimated: true,
                  originalQuantity: "4.00",
                  originalUnitPrice: "5.90",
                  rate: { id: "46eP41iSeosNr9uKwROohx" },
                  userCreated: false,
                  taxAmount: "0.00",
                }, {
                  calcType: "Flat",
                  description: "Towed Free Miles",
                  quantity: "0.00",
                  unitPrice: "-5.90",
                  deletedAt: "2019-04-18T21:29:02.124Z",
                  id: "1BY7W6dWvxV8Wnwh0P9KoW",
                  autoCalculatedQuantity: true,
                  netAmount: "0.00",
                  estimated: true,
                  originalQuantity: "0.00",
                  originalUnitPrice: "-5.90",
                  rate: { id: "46eP41iSeosNr9uKwROohx" },
                  userCreated: false,
                  taxAmount: "0.00",
                }, {
                  calcType: "Flat",
                  description: "Tax",
                  quantity: "0.00",
                  unitPrice: "0.00",
                  deletedAt: nil,
                  id: "7T3LSqq5lVO975Mc2OQDQX",
                  autoCalculatedQuantity: true,
                  netAmount: "0.00",
                  estimated: true,
                  originalQuantity: "0.00",
                  originalUnitPrice: "0.00",
                  rate: { id: "46eP41iSeosNr9uKwROohx" },
                  userCreated: false,
                  taxAmount: "0.00",
                },
              ],
              subtotal: "148.60",
              totalAmount: "148.60",
              payments: [],
              balance: "148.60",
              job: { id: "tPtBWV059BkWeQsKmr3o" },
              rateType: { type: "MileageTowed" },
              notes: "",
            },
          },
        }
      end

      context "with variables" do
        let(:query) do
          {
            query: invoice_variable_query,
            variables: variables.deep_stringify_keys,
            context: {},
          }
        end

        it 'works' do
          expect { subject }
            .to change { query[:document]&.to_query_string }.from(nil).to(invoice_inline_query)
            .and change { query[:query] }.to(nil)
            .and change { query[:variables] }.to({})
        end
      end

      context "without variables" do
        let(:query) do
          {
            query: invoice_inline_query,
            variables: {},
            context: {},
          }
        end

        it 'works' do
          expect { subject }
            .to avoid_changing { query[:document]&.to_query_string }
            .and avoid_changing { query[:query] }
            .and avoid_changing { query[:variables] }
        end
      end
    end
  end

  skip "queries" do
    subject { inline!(query) }

    context "with a fragment" do
      let(:query) do
        {
          query: fragment_query,
          variables: { 'filter': 'Create' },
          context: {},
        }
      end

      let(:fragment_inline_query) do
        <<~GRAPHQL
          query FooQuery($filter: Create) {
            viewer {
              ...on Application {
                company {
                  ...companyFields
                }
              }
              ...on User {
                company {
                  ...companyFields
                }
              }
            }
          }
          fragment companyFields on Company {
            services(filter: $filter) {
              edges {
                node {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end

      let(:fragment_query) do
        <<~GRAPHQL
          query FooQuery($filter: ServiceFilter) {
            viewer {
              ...on Application {
                company {
                  ...companyFields
                }
              }
              ...on User {
                company {
                  ...companyFields
                }
              }
            }
          }

          fragment companyFields on Company {
            services(filter: $filter) {
              edges {
                node {
                  id
                  name
                }
              }
            }
          }


        GRAPHQL
      end

      it "works" do
        expect { subject }
          .to change { query[:document]&.to_query_string }.from(nil).to(fragment_inline_query)
          .and change { query[:query] }.to(nil)
          .and change { query[:variables] }.to({})
      end
    end
  end
end
