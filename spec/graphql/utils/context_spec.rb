# frozen_string_literal: true

require "rails_helper"

describe Utils::Context do
  let(:query) do
    <<~GRAPHQL
      query {
        __type(name: "__Type"){
          name
        }
      }
    GRAPHQL
  end
  let(:result) { SwoopSchema.execute(query: query, context: context) }
  let(:partner) { create :rescue_company }
  let(:client) { create :fleet_company }
  let(:super_client_managed) { create :fleet_company, :agero }
  let(:swoop) { create :super_company, :swoop }
  let(:context) { {} }

  describe "#has_role?" do
    context "when the context does not have any api_roles" do
      it "works" do
        expect(result.context.has_role?(:root)).to be false
      end
    end

    context "when the context has any of the defined roles" do
      let(:context) { { api_roles: ["rescue", "dispatcher"] } }

      it "works" do
        expect(result.context.has_role?(:root)).to be false
        expect(result.context.has_role?(:dispatcher)).to be true
        expect(result.context.has_role?("dispatcher")).to be true
        expect(result.context.has_role?(:rescue)).to be true
      end
    end
  end

  describe "#has_any_role?" do
    context "when the context does not have any api_roles" do
      it "works" do
        expect(result.context.has_any_role?(:root)).to be false
      end
    end

    context "when the context has any of the defined roles" do
      let(:context) { { api_roles: ["rescue", "dispatcher"] } }

      it "works" do
        expect(result.context.has_any_role?(:root)).to be false
        expect(result.context.has_any_role?(:dispatcher)).to be true
        expect(result.context.has_any_role?("dispatcher", "admin")).to be true
        expect(result.context.has_any_role?(:rescue)).to be true
      end
    end
  end

  describe "#has_all_roles?" do
    context "when the context does not have any api_roles" do
      it "works" do
        expect(result.context.has_all_roles?(:root)).to be false
      end
    end

    context "when the context has any of the defined roles" do
      let(:context) { { api_roles: ["rescue", "dispatcher"] } }

      it "works" do
        expect(result.context.has_all_roles?(:root)).to be false
        expect(result.context.has_all_roles?(:dispatcher)).to be true
        expect(result.context.has_all_roles?("dispatcher", "rescue")).to be true
        expect(result.context.has_all_roles?("dispatcher", "driver", "rescue")).to be false
        expect(result.context.has_all_roles?("dispatcher", "fleet")).to be false
      end
    end
  end

  describe "#is_partner?" do
    subject { result.context.is_partner? }

    context "without an api_company" do
      it { is_expected.to be(false) }
    end

    context "with a partner company" do
      let(:context) { { api_company: partner } }

      it { is_expected.to be(true) }
    end

    context "with a client company" do
      let(:context) { { api_company: client } }

      it { is_expected.to be(false) }
    end
  end

  describe "#is_client?" do
    subject { result.context.is_client? }

    let(:partner) { create :rescue_company }
    let(:client) { create :fleet_company }

    context "without an api_company" do
      it { is_expected.to be(false) }
    end

    context "with a partner company" do
      let(:context) { { api_company: partner } }

      it { is_expected.to be(false) }
    end

    context "with a client company" do
      let(:context) { { api_company: client } }

      it { is_expected.to be(true) }
    end
  end

  describe "is_swoop?" do
    subject { result.context.is_swoop? }

    context "when the context does not have any api_roles" do
      it { is_expected.to be(false) }
    end

    context "when the company is not swoop company" do
      let(:context) { { api_company: client } }

      it { is_expected.to be(false) }
    end

    context "when the company is a super_fleet_managed company " do
      let(:context) { { api_company: swoop } }

      it { is_expected.to be(true) }
    end
  end

  describe "is_super_client_managed?" do
    subject { result.context.is_super_client_managed? }

    context "when the context does not have any api_roles" do
      it { is_expected.to be(false) }
    end

    context "when the company is not a super_fleet_managed company" do
      let(:context) { { api_company: client } }

      it { is_expected.to be(false) }
    end

    context "when the company is a super_fleet_managed company " do
      let(:context) { { api_company: super_client_managed } }

      it { is_expected.to be(false) }

      context "and api_roles includes super_fleet_managed" do
        let(:context) { super().deeper_dup.tap { |c| c[:api_roles] = ['super_fleet_managed'] } }

        it { is_expected.to be(true) }
      end
    end
  end

  describe "is_third_party_api_client?" do
    subject { result.context.is_third_party_api_client? }

    context 'without an api_company' do
      let(:context) { { api_application: third_party_partner_api_application } }

      it { is_expected.to be(false) }
    end

    context 'without an api_application' do
      let(:context) { { api_company: rescue_company } }

      it { is_expected.to be(false) }
    end

    context 'with a related api_application' do
      let(:context) { { api_company: rescue_company, api_application: rescue_application } }

      it { is_expected.to be(false) }
    end

    context 'without a related api_application' do
      let(:context) { { api_company: rescue_company, api_application: third_party_partner_api_application } }

      it { is_expected.to be(true) }
    end
  end

  describe "is_valid_subscription_viewer?" do
    subject { result.context.is_valid_subscription_viewer? }

    let(:api_user) { create :user, :rescue, :driver, company: rescue_company }

    context 'with a third-party api client' do
      let(:context) { { api_application: third_party_partner_api_application, api_company: rescue_company, viewer: viewer } }

      context 'with an api_user viewer' do
        let(:viewer) { api_user }

        it { is_expected.to be(false) }
      end

      context 'with an api_company viewer' do
        let(:viewer) { rescue_company }

        it { is_expected.to be(true) }
      end
    end

    context 'without a third-party api client' do
      let(:context) { { api_user: api_user, api_company: rescue_company, viewer: api_user } }

      it { is_expected.to be(true) }
    end
  end

  describe "is_valid_mutation_viewer?" do
    subject { result.context.is_valid_mutation_viewer? }

    let(:api_user) { create :user, :rescue, :driver, company: rescue_company }

    context 'with a third-party api client' do
      let(:context) { { api_application: third_party_partner_api_application, api_company: rescue_company, viewer: viewer } }

      context 'with an api_user viewer' do
        let(:viewer) { api_user }

        it { is_expected.to be(true) }
      end

      context 'with an api_company viewer' do
        let(:viewer) { rescue_company }

        it { is_expected.to be(false) }
      end
    end

    context 'without a third-party api client' do
      let(:context) { { api_user: api_user, api_company: rescue_company, viewer: api_user } }

      it { is_expected.to be(true) }
    end
  end
end
