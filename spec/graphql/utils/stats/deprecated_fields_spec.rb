# frozen_string_literal: true

require_relative "./stats_shared"

describe Utils::Stats::DeprecatedFields, freeze_time: true do
  include_context "stats shared"

  let(:query) do
    <<~GRAPHQL
      query {
        service(id: #{company.service_codes.first.to_ssid.inspect}) {
          id
          name
        }
        viewer {
          ...on User {
            fullName
          }
        }
        rescueVehicles {
          edges {
            node {
              driver {
                fullName
              }
            }
          }
        }
      }
    GRAPHQL
  end

  let(:deprecated_fields) do
    [
      "Query.rescueVehicles",
      "Query.service",
      "User.fullName",
    ]
  end

  let(:first_range) { deprecated_fields.map { |k| [k, 1.0] } }
  let(:second_range) { deprecated_fields.map { |k| [k, 2.0] } }

  shared_examples 'it increments keys' do
    it "works" do
      # the first time we run our query it will implicitly create our key and sorted sets
      # and set our sorted sets to 1
      expect { execute_query }
        .to change { redis.ttl(all_key) }
        .from(-2)
        .to(described_class::TTL)
        .and change { redis.ttl(application_key) }
        .from(-2)
        .to(described_class::TTL)
        .and change { redis.zrange(all_key, 0, 10, with_scores: true) }
        .from([])
        .to(first_range)
        .and change { redis.zrange(application_key, 0, 10, with_scores: true) }
        .from([])
        .to(first_range)

      # the second time we run the query it should increment each of our sorted sets
      # to 2.0 (there's no real way to test the ttl update because while we can fool
      # time in ruby during specs we can't do the same for redis)
      expect { execute_query }
        .to change { redis.zrange(all_key, 0, 10, with_scores: true) }
        .from(first_range)
        .to(second_range)
        .and change { redis.zrange(application_key, 0, 10, with_scores: true) }
        .from(first_range)
        .to(second_range)
    end
  end

  it_behaves_like "with and without application"
end
