# frozen_string_literal: true

require_relative "./stats_shared"

describe Utils::Stats::Queries, freeze_time: true do
  include_context "stats shared"

  let(:query) do
    <<~GRAPHQL
      query {
        jobs {
          edges {
            node {
              id
            }
          }
        }
      }
    GRAPHQL
  end

  shared_examples 'it increments keys' do
    it "works" do
      # the first time we run our query it will implicitly create our key
      # and set it to 1
      expect { execute_query }
        .to change { redis.get(all_key) }
        .from(nil)
        .to('1')
        .and change { redis.get(application_key) }
        .from(nil)
        .to('1')
        .and change { redis.ttl(all_key) }
        .from(-2)
        .to(described_class::TTL)
        .and change { redis.ttl(application_key) }
        .from(-2)
        .to(described_class::TTL)

      # the second time we run the query it should increment each of keys
      # (there's no real way to test the ttl update because while we can fool
      # time in ruby during specs we can't do the same for redis)
      expect { execute_query }
        .to change { redis.get(all_key) }
        .from('1')
        .to('2')
        .and change { redis.get(application_key) }
        .from('1')
        .to('2')
    end
  end

  it_behaves_like "with and without application"
end
