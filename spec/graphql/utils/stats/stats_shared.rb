# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "stats shared" do
  def execute_query
    SwoopSchema.execute(query: query, context: context, root_value: company)
  end

  let(:redis) { RedisClient[:default] }
  let(:date) { Date.current.strftime('%Y%m%d') }
  let(:company) { fleet_managed_company }
  let(:application) { create :doorkeeper_application, owner: company, scopes: :fleet }
  let(:user) { create :user, company: company, roles: [:fleet] }
  let(:no_application_context) do
    {
      api_company: company,
      api_user: user,
      api_roles: context_roles_for(user: user),
      headers: headers,
      viewer: user,
    }
  end
  let(:application_context) do
    {
      api_company: company,
      api_application: application,
      api_roles: context_roles_for(application: application),
      headers: headers,
      viewer: application,
    }
  end
  let(:instance) { described_class.new }
  let(:all_key) { instance.build_all_key date }

  shared_examples "with and without application" do
    context "without application" do
      let(:context) { no_application_context }
      let(:application_key) { instance.build_application_key date, described_class::MOBILE_APP_ID }

      it_behaves_like "it increments keys"
    end

    context "with application" do
      let(:context) { application_context }
      let(:application_key) { instance.build_application_key date, application.id }

      it_behaves_like "it increments keys"
    end
  end
end
