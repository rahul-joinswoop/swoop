# frozen_string_literal: true

require 'rails_helper'

describe Resolvers::Editable do
  subject { Resolvers::Editable.new(object: job, context: context) }

  let(:job) do
    create :fleet_managed_job,
           status: status,
           fleet_company: fleet_managed_company,
           rescue_company: api_company,
           rescue_driver: rescue_driver
  end

  shared_examples 'editable' do
    let(:good_args) { { id: 123, customer: { email: Faker::Internet.email } } }
    let(:bad_args) { { id: 123, customer: { name: Faker::Name.name, email: Faker::Internet.email } } }
    let(:partner_driver_update_args) do
      {
        id: 123,
        partner: {
          driver: { name: Faker::Name.name },
          vehicle: { name: Faker::Beer.hop },
        },
      }
    end

    context 'en route job' do
      let(:status) { :enroute }

      context '#has_other_keys' do
        it 'when extra keys at top level' do
          expect(subject.send(:has_other_keys?, { a: 1, b: 2, c: 3 }, { a: 1 })).to be true
        end
        it 'when no extra keys regardless of value' do
          expect(subject.send(:has_other_keys?, { a: 2 }, { a: 1 })).to be false
        end
        it 'when search has is deeper than filter' do
          expect(subject.send(:has_other_keys?, { b: { a: 1 } }, { b: 1 })).to be true
        end
        it 'for a nested hash with matching keys' do
          expect(subject.send(:has_other_keys?, { b: { a: 1 } }, { b: { a: 3 } })).to be false
        end
        it 'with extra keys nested' do
          expect(subject.send(:has_other_keys?, { b: { a: 1, b: 2 } }, { b: { a: 3 } })).to be true
        end
        it 'if there are keys and not filtered with a hash' do
          expect(subject.send(:has_other_keys?, { b: { a: 1, b: 2 } }, 1)).to be true
        end
        it 'is true if there are keys and not filtered with a hash' do
          expect(subject.send(:has_other_keys?, {}, 1)).to be false
        end
      end

      context "without any features" do
        it 'works' do
          expect(subject.resolve).to be(true)
        end
        it 'works when editing email' do
          expect(subject.resolve(args: good_args)).to be(true)
        end
        it 'works when editing more than email' do
          expect(subject.resolve(args: bad_args)).to be(true)
        end
      end

      context "with #{Feature::DISABLE_APP_EDIT_JOBS}" do
        let(:api_company) { super().tap { |c| c.features << create(:feature, name: Feature::DISABLE_APP_EDIT_JOBS) } }

        it 'works' do
          expect(subject.resolve).to be(false)
        end
        it 'works when editing email' do
          expect(subject.resolve(args: good_args)).to be(true)
        end
        it 'permits editing partner driver' do
          expect(subject.resolve(args: partner_driver_update_args)).to be(true)
        end
        it 'works when editing more than email' do
          expect(subject.resolve(args: bad_args)).to be(false)
        end

        context 'and a non-mobile ua' do
          let(:headers) { { 'HTTP_USER_AGENT' => 'mozilla/compatible alkasjasdf' } }

          it 'works' do
            expect(subject.resolve).to be(true)
          end
        end
      end

      context "with #{Feature::DISABLE_APP_DRIVERS_EDIT_JOBS}" do
        let(:api_company) { super().tap { |c| c.features << create(:feature, name: Feature::DISABLE_APP_DRIVERS_EDIT_JOBS) } }

        it 'works' do
          expect(subject.resolve).to be(true)
        end
        it 'works when editing email' do
          expect(subject.resolve(args: good_args)).to be(true)
        end
        it 'works when editing more than email' do
          expect(subject.resolve(args: bad_args)).to be(true)
        end

        context 'and a driver api_user' do
          let(:api_user) { rescue_driver }

          it 'works' do
            expect(subject.resolve).to be(false)
          end
          it 'works when editing email' do
            expect(subject.resolve(args: good_args)).to be(true)
          end
          it 'works when editing more than email' do
            expect(subject.resolve(args: bad_args)).to be(false)
          end
          it "doesn't permit editing partner driver" do
            expect(subject.resolve(args: partner_driver_update_args)).to be(false)
          end
        end

        context 'and a non-mobile ua' do
          let(:headers) { { 'HTTP_USER_AGENT' => 'mozilla/compatible alkasjasdf' } }

          it 'works' do
            expect(subject.resolve).to be(true)
          end
        end
      end
    end

    context 'submitted job' do
      let(:status) { :submitted }
      let(:job) do
        create :fleet_motor_club_job,
               status: status,
               fleet_company: fleet_managed_company,
               rescue_company: api_company
      end

      it 'works' do
        expect(subject.resolve).to be(false)
      end
    end

    context 'auto_assigning job' do
      let(:status) { :auto_assigning }

      it 'works' do
        expect(subject.resolve).to be(false)
      end
    end

    context 'assigned job' do
      let(:status) { :assigned }

      it 'works' do
        expect(subject.resolve).to be(false)
      end
    end
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "editable"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    let(:api_user) { rescue_dispatcher }

    it_behaves_like "editable"
  end

  context "as a 3rd party partner api_company" do
    # currently we don't allow 3rd party api_companies to access
    # mutations so this should always return false
    include_context "3rd party partner api_company"

    context 'en route job' do
      let(:status) { :enroute }

      context "without any features" do
        it 'works' do
          expect(subject.resolve).to be(false)
        end
      end

      context "with #{Feature::DISABLE_APP_EDIT_JOBS}" do
        let(:api_company) { super().tap { |c| c.features << create(:feature, name: Feature::DISABLE_APP_EDIT_JOBS) } }

        it 'works' do
          expect(subject.resolve).to be(false)
        end

        context 'and a non-mobile ua' do
          let(:headers) { { 'HTTP_USER_AGENT' => 'mozilla/compatible alkasjasdf' } }

          it 'works' do
            expect(subject.resolve).to be(false)
          end
        end
      end

      context "with #{Feature::DISABLE_APP_DRIVERS_EDIT_JOBS}" do
        let(:api_company) { super().tap { |c| c.features << create(:feature, name: Feature::DISABLE_APP_DRIVERS_EDIT_JOBS) } }

        it 'works' do
          expect(subject.resolve).to be(false)
        end

        context 'and a non-mobile ua' do
          let(:headers) { { 'HTTP_USER_AGENT' => 'mozilla/compatible alkasjasdf' } }

          it 'works' do
            expect(subject.resolve).to be(false)
          end
        end
      end
    end

    context 'submitted job' do
      let(:status) { :submitted }
      let(:job) do
        create :fleet_motor_club_job,
               status: status,
               fleet_company: fleet_managed_company,
               rescue_company: api_company
      end

      it 'works' do
        expect(subject.resolve).to be(false)
      end
    end

    context 'auto_assigning job' do
      let(:status) { :auto_assigning }

      it 'works' do
        expect(subject.resolve).to be(false)
      end
    end

    context 'assigned job' do
      let(:status) { :assigned }

      it 'works' do
        expect(subject.resolve).to be(false)
      end
    end
  end
end
