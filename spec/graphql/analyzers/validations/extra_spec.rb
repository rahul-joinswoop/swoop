# frozen_string_literal: true

require_relative "./validations_shared"

describe Analyzers::Validations::Extra do
  include_context "validations shared"

  subject do
    described_class.call(
      irep_node: irep_node,
      memo: memo,
      visit_type: visit_type,
      input: input,
      index: index,
      question: question,
      answer: answer
    )
  end

  let(:index) { 1 }
  let(:input) { { extra: extra } }
  let(:successful_interactor_context) { { extra: extra } }
  let(:answer) { create :answer, answer: Answer::YES, question: question }

  context "when answer needs_info?" do
    let(:question) { create :question, question: QuestionDefinitions::LOW_CLEARANCE }

    context "with extra" do
      let(:extra) { Faker::Hipster.sentence }

      it_behaves_like "it succeeds"
    end

    context "without extra" do
      let(:extra) { nil }
      let(:interactor_error) { 'missing required extra' }
      let(:memo_errors) do
        {
          message: Analyzers::Validations::Extra::ERROR_MESSAGE % question.question.inspect,
          path: [*irep_path, "input", "job", "service", "answers", index],
        }
      end

      it_behaves_like "it fails with error"
    end
  end

  context "when answer does not needs_info?" do
    let(:question) { create :question, question: QuestionDefinitions::CUSTOMER_BEEN_CONTACTED }

    context "with extra" do
      let(:extra) { Faker::Hipster.sentence }

      it_behaves_like "it succeeds"
    end

    context "without extra" do
      let(:extra) { nil }

      it_behaves_like "it succeeds"
    end
  end
end
