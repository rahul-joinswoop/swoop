# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "validations shared" do
  subject do
    described_class.call irep_node: irep_node,
                         memo: memo,
                         visit_type: visit_type,
                         **context_args
  end

  let(:context_args) { {} }
  let(:memo_errors) { [] }
  let(:interactor_error) { nil }

  shared_examples "it fails with error" do
    it "fails" do
      # check our interactor context
      expect(subject).to be_failure
      expect(subject.error).to eq(interactor_error) # define interactor_error in your spec

      # if we have memo_errors defined, then verify the expected errors
      # against what our memo actually contains
      Array.wrap(memo_errors).map(&:deep_stringify_keys).tap do |memo_errors|
        expect(memo[:errors].map(&:to_h))
          .to have(memo_errors.size).items
          .and deep_include memo_errors # define memo_errors in your spec
      end
    end
  end

  # LOLWUT? this is to test interactors that populate memo[:errors] but don't actually
  # fail the interactor context.
  shared_examples "it succeeds with error" do
    it "succeeds" do
      # check our interactor context
      expect(subject).to be_success

      # if we have memo_errors defined, then verify the expected errors
      # against what our memo actually contains
      Array.wrap(memo_errors).map(&:deep_stringify_keys).tap do |memo_errors|
        expect(memo[:errors].map(&:to_h))
          .to have(memo_errors.size).items
          .and deep_include memo_errors # define memo_errors in your spec
      end
    end
  end

  # define any interactor context variables you need to check when your interactor
  # succeeds
  let(:successful_interactor_context) { {} }
  shared_examples "it succeeds" do
    it "works" do
      expect(subject).to be_success
      successful_interactor_context.each do |k, v|
        expect(subject.send(k)).to eq(v)
      end
    end
  end

  # these are all things that need to work/exist on our irep_node
  let(:irep_node) { double("irep node") }
  let(:visit_type) { 'VisitType' }
  let(:irep_owner_type) { 'ownertype' }
  let(:irep_parent) { 'parent' }
  let(:irep_name) { 'name' }

  # helper to build the beginning of a path for an error
  let(:irep_path) { ["#{irep_owner_type} #{irep_parent}", irep_name] }

  # our memo that's passed in
  let(:memo) { { errors: [], context: context } }

  let(:context) { double("context") }
  # key/value pairs of methods that will be mocked on memo[:context]
  let(:graphql_context_messages) { {} }

  before do
    allow(irep_node).to receive_message_chain(:owner_type, :name, :downcase) { irep_owner_type }
    allow(irep_node).to receive_message_chain(:parent, :name) { irep_parent }
    allow(irep_node).to receive(:name).and_return(irep_name)
    allow(irep_node).to receive(:ast_node)

    # mock messages on context via context_messages, see
    # https://relishapp.com/rspec/rspec-mocks/v/3-8/docs/basics/allowing-messages#messages-can-be-allowed-in-bulk-using-%60receive-messages%60
    allow(context).to receive_messages(graphql_context_messages)
  end
end
