# frozen_string_literal: true

require_relative "./validations_shared"

describe Analyzers::Validations::QuestionAnswerExtra do
  include_context "validations shared"

  subject do
    described_class.call(
      irep_node: irep_node,
      memo: memo,
      visit_type: visit_type,
      input: input,
      index: index,
    )
  end

  let(:index) { 1 }

  context "with valid questions and answers" do
    let(:question) { create :question, question: QuestionDefinitions::LOW_CLEARANCE }
    let(:answer) { create :answer, answer: Answer::YES, question: question }
    let(:extra) { Faker::Hipster.sentence }
    let(:input) do
      {
        question: question.question,
        answer: answer.answer,
        extra: extra,
      }
    end

    it_behaves_like "it succeeds"
  end

  context "with invalid question" do
    # just testing one failure case
    let(:question) { Faker::Hipster.sentence }
    let(:answer) { Faker::Hipster.sentence }
    let(:extra) { Faker::Hipster.sentence }
    let(:input) do
      {
        question: question,
        answer: answer,
        extra: extra,
      }
    end
    let(:interactor_error) { 'invalid question' }
    let(:memo_errors) do
      {
        message: Analyzers::Validations::Question::ERROR_MESSAGE % question.inspect,
        path: [*irep_path, "input", "job", "service", "answers", index],
      }
    end

    it_behaves_like "it fails with error"
  end
end
