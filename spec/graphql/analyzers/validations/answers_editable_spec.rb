# frozen_string_literal: true

require_relative "./validations_shared"

describe Analyzers::Validations::AnswersEditable do
  include_context "validations shared"

  let(:is_partner) { false }
  let(:is_client) { false }
  let(:has_questions) { false }
  let(:interactor_error) { described_class::ERROR_MESSAGE }
  let(:graphql_context_messages) do
    {
      is_partner?: is_partner,
      is_client?: is_client,
      has_feature?: has_questions,
    }
  end

  context "with a partner" do
    let(:is_partner) { true }

    it_behaves_like "it fails with error"
  end

  context "with a client" do
    let(:is_client) { true }

    context "without questions feature" do
      it_behaves_like "it fails with error"
    end

    context "with questions feature" do
      let(:has_questions) { true }

      it_behaves_like "it succeeds"
    end
  end
end
