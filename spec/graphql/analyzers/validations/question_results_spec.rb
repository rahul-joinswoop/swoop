# frozen_string_literal: true

require_relative "./validations_shared"

describe Analyzers::Validations::QuestionResults do
  include_context "validations shared"

  let(:count) { 3 }
  let(:questions) { create_list(:question_with_answers, count) }
  let(:extras) { count.times { Faker::Hipster.sentence } }
  let(:input) do
    questions.map.with_index do |q, i|
      {
        question: q.question,
        answer: q.answers.first.answer,
        extra: extras[i],
      }
    end
  end
  let(:is_partner) { false }
  let(:is_client) { true }
  let(:has_questions) { true }
  let(:graphql_context_messages) do
    {
      is_partner?: is_partner,
      is_client?: is_client,
      has_feature?: has_questions,
    }
  end

  before do
    allow(irep_node).to receive_message_chain(:arguments, :dig) { input }
  end

  shared_examples "it works" do
    context "with valid input" do
      it_behaves_like "it succeeds"
    end

    context "with invalid input" do
      let(:invalid_question) { Faker::Hipster.sentence }
      let(:invalid_answer) { Faker::Hipster.sentence }
      let(:input) do
        super().tap do |i|
          i[1][:question] = invalid_question
          i[2][:answer] = invalid_answer
        end
      end

      let(:memo_errors) do
        [
          {
            extensions: {
              code: "argumentLiteralsIncompatible",
              typeName: "CoercionError",
            },
            message: Analyzers::Validations::Question::ERROR_MESSAGE % invalid_question.inspect,
            path: [*irep_path, "input", "job", "service", "answers", 1],
          },
          {
            extensions: {
              code: "argumentLiteralsIncompatible",
              typeName: "CoercionError",
            },
            message: Analyzers::Validations::Answer::ERROR_MESSAGE % invalid_answer.inspect,
            path: [*irep_path, "input", "job", "service", "answers", 2],
          },
        ]
      end

      it_behaves_like "it succeeds with error"
    end
  end

  context "without a job" do
    let(:successful_interactor_context) do
      {
        question_results: questions.map.with_index do |q, i|
          {
            question: q,
            answer: q.answers.first,
            extra: extras[i],
          }
        end,
      }
    end

    it_behaves_like "it works"
  end

  context "with a job" do
    let(:job) do
      create(:job).tap do |j|
        j.question_results << create_list(:question_result, 3)
      end
    end
    let(:context_args) { { job: job } }
    let(:successful_interactor_context) do
      {
        question_results: job.question_results.to_a +
                          questions.map.with_index do |q, i|
                            {
                              question: q,
                              answer: q.answers.first,
                              extra: extras[i],
                            }
                          end,
      }
    end

    it_behaves_like "it works"
  end
end
