# frozen_string_literal: true

require_relative "./validations_shared"

describe Analyzers::Validations::Question do
  include_context "validations shared"

  subject do
    described_class.call(
      irep_node: irep_node,
      memo: memo,
      visit_type: visit_type,
      input: input,
      index: index
    )
  end

  let(:valid_question) { create(:question) }
  let(:invalid_question) { { question: Faker::Lorem.question } }
  let(:index) { 1 }

  context "with valid question" do
    let(:input) { { question: valid_question.question } }
    let(:successful_interactor_context) { { question: valid_question } }

    it_behaves_like "it succeeds"
  end

  context "with invalid questions" do
    let(:input) { invalid_question }
    let(:interactor_error) { "invalid question" }
    let(:memo_errors) do
      {
        extensions: {
          code: "argumentLiteralsIncompatible",
          typeName: "CoercionError",
        },
        message: described_class::ERROR_MESSAGE % invalid_question[:question].inspect,
        path: [*irep_path, "input", "job", "service", "answers", index],
      }
    end

    it_behaves_like "it fails with error"
  end
end
