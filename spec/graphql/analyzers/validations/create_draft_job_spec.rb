# frozen_string_literal: true

require 'rails_helper'

describe Analyzers::Validations::CreateDraftJob do
  describe '#organized' do
    let(:create_job_organized) do
      Set.new Analyzers::Validations::CreateJob.deep_organized
    end

    let(:create_draft_job_organized) do
      Set.new described_class.deep_organized
    end

    it 'works' do
      # CreateDraftJob should always be a subset of CreateJob
      expect(create_draft_job_organized.subset?(create_job_organized)).to be true
    end
  end
end
