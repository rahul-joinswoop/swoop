# frozen_string_literal: true

require_relative "./validations_shared"

describe Analyzers::Validations::ServiceQuestions do
  include_context "validations shared"

  let(:company) { create :company, :with_service_codes, :with_questions }
  let(:service_code) { company.service_codes.sample }
  let(:questions) { company.questions_for_service_code service_code }
  let(:count) { 3 }
  let(:extras) { count.times { Faker::Hipster.sentence } }

  let(:is_partner) { false }
  let(:is_client) { true }
  let(:has_questions) { true }
  let(:graphql_context_messages) do
    {
      is_partner?: is_partner,
      is_client?: is_client,
      has_feature?: has_questions,
    }
  end

  before do
    allow(context).to receive(:[]).with(:api_company).and_return(company)
  end

  context "without a job" do
    let(:input) { service_code }

    before do
      allow(irep_node).to receive_message_chain(:arguments, :dig) { { name: input } }
    end

    context "with all required questions" do
      let(:context_args) do
        {
          question_results: questions.map do |q|
            {
              question: q,
              answer: q.answers.sample,
              extra: Faker::Hipster.sentence,
            }
          end,
        }
      end

      it_behaves_like "it succeeds"
    end

    context "without all required questions" do
      let(:context_args) do
        {
          question_results: questions.take(1).map do |q|
            {
              question: q,
              answer: q.answers.sample,
              extra: Faker::Hipster.sentence,
            }
          end,
        }
      end
      let(:memo_errors) do
        {
          extensions: {
            problems: questions[1..-1].map do |q|
              { explanation: described_class::ERROR_EXPLANATION % q.question.inspect }
            end,
          },
          message: described_class::ERROR_MESSAGE % ["Questions", service_code.name.inspect],
          path: [*irep_path, "input", "job", "service", "answers"],
        }
      end

      it_behaves_like "it succeeds with error"
    end
  end

  context "with a job" do
    let(:job) do
      create(:job, service_code: service_code).tap do |j|
        company.questions_for_service_code(service_code).each.with_index do |q, i|
          j.question_results << QuestionResult.new(question: q, answer: q.answers.sample)
        end
      end
    end

    let(:context_args) do
      {
        job: job,
        question_results: job.question_results.includes(:question).to_a,
      }
    end

    context "with all required questions" do
      it_behaves_like "it succeeds"
    end

    context "without all required questions" do
      let!(:missing_question) do
        q = job.question_results.last.question
        job.question_results.last.delete
        q
      end

      let(:memo_errors) do
        {
          extensions: {
            problems: [{ explanation: described_class::ERROR_EXPLANATION % missing_question.question.inspect }],
          },
          message: described_class::ERROR_MESSAGE % ["Question", service_code.name.inspect],
          path: [*irep_path, "job", "service", "answers"],
        }
      end

      it_behaves_like "it succeeds with error"

      context "but with missing question in input" do
        let(:context_args) do
          question_results = job.question_results.includes(:question).to_a
          question_results << {
            question: missing_question,
            answer: missing_question.answers.sample,
            extra: Faker::Hipster.sentence,
          }
          {
            job: job,
            question_results: question_results,
          }
        end

        it_behaves_like "it succeeds"
      end
    end
  end
end
