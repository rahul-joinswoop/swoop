# frozen_string_literal: true

require_relative "./validations_shared"

describe Analyzers::Validations::Answer do
  include_context "validations shared"

  subject do
    described_class.call(
      irep_node: irep_node,
      memo: memo,
      visit_type: visit_type,
      input: input,
      index: index,
      question: question
    )
  end

  let(:question) { create(:question_with_answers) }
  let(:valid_answer) { question.answers.first }
  let(:invalid_answer) { { question: Faker::Lorem.question } }
  let(:index) { 1 }

  context "with valid question" do
    let(:input) { { answer: valid_answer.answer } }
    let(:successful_interactor_context) { { answer: valid_answer } }

    it_behaves_like "it succeeds"
  end

  context "with invalid answer" do
    let(:input) { invalid_answer }
    let(:interactor_error) { "invalid answer" }
    let(:memo_errors) do
      {
        extensions: {
          code: "argumentLiteralsIncompatible",
          typeName: "CoercionError",
        },
        message: described_class::ERROR_MESSAGE % invalid_answer[:answer].inspect,
        path: [*irep_path, "input", "job", "service", "answers", index],
      }
    end

    it_behaves_like "it fails with error"
  end
end
