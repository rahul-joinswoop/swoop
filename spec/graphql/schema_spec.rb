# frozen_string_literal: true

require 'rails_helper'

describe "GraphQL Schema" do
  describe "artifacts" do
    it "has up to date artifacts" do
      artifacts_path = File.join("app", "assets", "javascripts", "graphql")
      fail_message = "does not match the schema definition; run `bin/rake graphql:schema:dump` to update it."

      schema_hash = JSON.parse(SwoopSchema.to_json)

      json_path = File.join(artifacts_path, "schema.json")
      artifact_hash = JSON.parse((Rails.root + json_path).read)

      artifact_hash.delete("extensions")

      unless schema_hash == artifact_hash
        fail "#{json_path} #{fail_message}"
      end

      graphql = SwoopSchema.to_definition
      graphql_path = File.join(artifacts_path, "schema.graphql")
      artifact_graphql = (Rails.root + graphql_path).read
      unless graphql == artifact_graphql
        fail "#{graphql_path} #{fail_message}"
      end
    end
  end
end
