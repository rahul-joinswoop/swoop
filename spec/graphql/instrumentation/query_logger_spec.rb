# frozen_string_literal: true

require 'rails_helper'

describe Instrumentation::QueryLogger do
  let(:job) { create :fleet_managed_job, status: :pending, fleet_company: fleet_managed_company, rescue_company: rescue_company }
  let(:encoded_job_id) { job.to_ssid }
  let(:show_errors) { false }
  let(:service_location) { create :graphql_location_input_type }

  let(:operation_name) { 'UpdateJob' }

  shared_examples "calls querylogger" do
    it "works" do
      expect(Utils::QueryLogger).to receive(:log_query).once.and_call_original
      expect { response }.not_to raise_error
    end
  end

  shared_examples "no input" do
    let(:response) do
      SwoopSchema.execute
    end

    it_behaves_like "calls querylogger"
  end

  shared_examples "query" do
    let(:variables) { {} }
    let(:query) do
      <<~GRAPHQL
          query #{operation_name} {
            viewer {
              ...on Application {
                company {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
    end

    it_behaves_like "calls querylogger"
  end

  shared_examples "mutation" do
    context "with an operation name and variables" do
      let(:query) do
        <<~GRAPHQL
          mutation #{operation_name}($input: UpdateJobInput!) {
            updateJob(input: $input) {
              job {
                id
                location {
                  serviceLocation{
                    address
                    lat
                    lng
                    locationType
                  }
                }
              }
            }
          }
          GRAPHQL
      end

      it_behaves_like "calls querylogger"

      context "with an error" do
        let(:operation_name) { "UpdateJobStatus" }
        let(:query) do
          <<~GRAPHQL
            mutation UpdateJobStatus($input: UpdateJobStatusInput!) {
              updateJobStatus(input: $input) {
                job {
                  id
                  status
                }
              }
            }
            GRAPHQL
        end

        let(:variables) do
          {
            input: {
              job: {
                id: encoded_job_id,
                status: "EnRoute",
              },
            },
          }
        end

        it_behaves_like "calls querylogger"
      end

      context "with an invalid location_type" do
        let(:service_location) { create(:graphql_location_input_type).tap { |sl| sl[:locationType] = "asdfasdf" } }

        it_behaves_like "calls querylogger"
      end
    end

    context "with an operation name and no variables" do
      let(:variables) { {} }
      let(:query) do
        <<~GRAPHQL
          mutation #{operation_name} {
            updateJob(input: {
              job: {
                id: "#{encoded_job_id}",
                location: {
                  serviceLocation: {
                    address: "#{service_location[:address]}",
                    lat: #{service_location[:lat]},
                    lng: #{service_location[:lng]},
                    locationType: "#{service_location[:locationType]}"
                  }
                }
              }
            }) {
              job {
                id
                location {
                  serviceLocation{
                    address
                    lat
                    lng
                    locationType
                  }
                }
              }
            }
          }
          GRAPHQL
      end

      it_behaves_like "calls querylogger"
    end

    context "without an operation name or variables" do
      let(:variables) { {} }
      let(:operation_name) {}
      let(:query) do
        <<~GRAPHQL
          mutation {
            updateJob(input: {
              job: {
                id: "#{encoded_job_id}",
                location: {
                  serviceLocation: {
                    address: "#{service_location[:address]}",
                    lat: #{service_location[:lat]},
                    lng: #{service_location[:lng]},
                    locationType: "#{service_location[:locationType]}"
                  }
                }
              }
            }) {
              job {
                id
                location {
                  serviceLocation{
                    address
                    lat
                    lng
                    locationType
                  }
                }
              }
            }
          }
          GRAPHQL
      end

      it_behaves_like "calls querylogger"
    end

    context "without an operation name, with variables" do
      let(:operation_name) {}
      let(:query) do
        <<~GRAPHQL
          mutation #{operation_name}($input: UpdateJobInput!) {
            updateJob(input: $input) {
              job {
                id
                location {
                  serviceLocation{
                    address
                    lat
                    lng
                    locationType
                  }
                }
              }
            }
          }
          GRAPHQL
      end

      it_behaves_like "calls querylogger"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like "no input"
    it_behaves_like "query"
    it_behaves_like "mutation"
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like "no input"
    it_behaves_like "query"
    it_behaves_like "mutation"
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like "no input"
    it_behaves_like "query"
    it_behaves_like "mutation"
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like "no input"
    it_behaves_like "query"
  end
end
