# frozen_string_literal: true

require 'rails_helper'

describe Instrumentation::AuditLogger do
  # TODO - add specs for inline queries too
  let(:skip_query_inliner) { true }

  let(:encoded_job_id) { job.to_ssid }
  let(:job) do
    create :fleet_managed_job,
           status: :pending,
           fleet_company: fleet_managed_company,
           rescue_company: rescue_company
  end

  let(:service_location) { create :graphql_location_input_type }
  let(:job_input) do
    {
      job: {
        id: encoded_job_id,
        location: {
          serviceLocation: service_location,
        },
      },
    }
  end

  let(:variables) { { input: job_input } }

  let(:show_errors) { false }

  shared_examples "query" do
    let(:variables) { {} }
    let(:query) do
      <<~GRAPHQL
        query #{operation_name} {
          viewer {
            ...on Application {
              company {
                id
                name
              }
            }
          }
        }
      GRAPHQL
    end

    it "works" do
      expect { response }.to avoid_changing(Command, :count)
      expect(response).not_to include('errors')
    end
  end

  shared_examples "mutation" do
    let(:lat_lng) do
      {
        lat: Float(Faker::Address.latitude).round(13),
        lng: Float(Faker::Address.longitude).round(13),
      }
    end

    shared_examples "mutations" do
      let(:service_location) { create :graphql_location_input_type, lat_lng }

      it "works" do
        expect { response }.to change(Command, :count).by(1)
        expect(response).not_to include('errors')
        cmd = Command.last
        expect(cmd.event_operation.name).to eq(EventOperation::MUTATION)
        expect(JSON.parse(cmd.json)).to deep_include({ query: query, variables: variables }.deep_stringify_keys)
        expect(cmd.company).to eq(api_company)
        expect(cmd.request).to eq(context[:request_id])
        expect(cmd.applied).to be_truthy
      end
    end
    context "with an operation name and variables" do
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name}($input: UpdateJobInput!) {
          updateJob(input: $input) {
            job {
              id
              location {
                serviceLocation {
                  address
                  lat
                  lng
                  locationType
                }
              }
            }
          }
        }
        GRAPHQL
      end

      it_behaves_like "mutations"

      # only need to test this once, make sure a provided uuid is passed through
      context "with a request id" do
        let(:context) { super().merge(request_id: SecureRandom.uuid) }

        it_behaves_like "mutations"
      end

      context "with an error" do
        let(:query) do
          <<~GRAPHQL
          mutation #{operation_name}($input: UpdateJobStatusInput!) {
            updateJobStatus(input: $input) {
              job {
                id
                status
              }
            }
          }
          GRAPHQL
        end
        let(:variables) do
          {
            input: {
              job: {
                id: encoded_job_id,
                status: "asdf",
              },
            },
          }
        end

        it "works" do
          expect { response }.to change(Command, :count).by(1)
          expect(response).to include('errors')
          cmd = Command.last
          expect(cmd.event_operation.name).to eq(EventOperation::MUTATION)
          expect(JSON.parse(cmd.json)).to deep_include({ query: query, variables: variables }.deep_stringify_keys)
          expect(cmd.company).to eq(api_company)
          expect(cmd.request).to eq(context[:request_id])
          expect(cmd.applied).to be_falsey
        end
      end

      context "with an invalid location_type" do
        let(:service_location) { create(:graphql_location_input_type, lat_lng).tap { |sl| sl[:locationType] = "asdfasdf" } }

        it "works" do
          expect { response }.to change(Command, :count).by(1)
          expect(response).to include('errors')
          cmd = Command.last
          expect(cmd.event_operation.name).to eq(EventOperation::MUTATION)
          expect(JSON.parse(cmd.json)).to deep_include({ query: query, variables: variables }.deep_stringify_keys)
          expect(cmd.company).to eq(api_company)
          expect(cmd.request).to eq(context[:request_id])
          expect(cmd.applied).to be_falsey
        end
      end
    end

    context "with an operation name and no variables" do
      let(:variables) { {} }
      let(:query) do
        <<~GRAPHQL
        mutation #{operation_name} {
          updateJob(input: {
            job: {
              id: "#{encoded_job_id}",
              location: {
                serviceLocation: {
                  address: "#{service_location[:address]}",
                  lat: #{service_location[:lat]},
                  lng: #{service_location[:lng]},
                  locationType: "#{service_location[:locationType]}"
                }
              }
            }
          }) {
            job {
              id
              location {
                serviceLocation {
                  address
                  lat
                  lng
                  locationType
                }
              }
            }
          }
        }
        GRAPHQL
      end

      it_behaves_like "mutations"
    end

    context "without an operation name or variables" do
      let(:variables) { {} }
      let(:operation_name) {}
      let(:query) do
        <<~GRAPHQL
        mutation {
          updateJob(input: {
            job: {
              id: "#{encoded_job_id}",
              location: {
                serviceLocation: {
                  address: "#{service_location[:address]}",
                  lat: #{service_location[:lat]},
                  lng: #{service_location[:lng]},
                  locationType: "#{service_location[:locationType]}"
                }
              }
            }
          }) {
            job {
              id
              location {
                serviceLocation {
                  address
                  lat
                  lng
                  locationType
                }
              }
            }
          }
        }
        GRAPHQL
      end

      it_behaves_like "mutations"
    end

    context "without an operation name, with variables" do
      let(:operation_name) {}
      let(:query) do
        <<~GRAPHQL
        mutation($input: UpdateJobInput!) {
          updateJob(input: $input) {
            job {
              id
              location {
                serviceLocation {
                  address
                  lat
                  lng
                  locationType
                }
              }
            }
          }
        }
        GRAPHQL
      end

      it_behaves_like "mutations"
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like 'query'
    it_behaves_like 'mutation'
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    it_behaves_like 'query'
    it_behaves_like 'mutation'
  end

  context "as a 3rd party partner api_user" do
    include_context "3rd party partner api_user"
    it_behaves_like 'query'
    it_behaves_like 'mutation'
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    it_behaves_like 'query'
    # we can't do mutations here except for deleteSubscription :shrug:
  end
end
