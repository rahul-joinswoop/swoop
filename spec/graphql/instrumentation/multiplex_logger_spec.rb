# frozen_string_literal: true

require 'rails_helper'

describe Instrumentation::MultiplexLogger do
  shared_examples "multiplex logger" do
    let(:show_errors) { false }
    let(:response) do
      SwoopSchema.multiplex(queries)
    end

    shared_examples "calls querylogger" do
      it "works" do
        expect(Utils::QueryLogger).to receive(:log_multiplex).once.and_call_original
        response
      end
    end

    context "with no input" do
      let(:queries) { [] }

      it "works" do
        expect(Utils::QueryLogger).to receive(:log_multiplex).once.and_call_original
        expect { response }.not_to raise_error
      end
    end

    context "with queries" do
      let(:queries) do
        [
          {
            operation_name: "ViewerQuery",
            query: (
              <<~GRAPHQL
            query ViewerQuery {
              viewer {
                ...on User {
                  roles(first: 50) {
                    edges {
                      node {
                        name
                      }
                    }
                  }
                  company {
                    features(first: 50) {
                      edges {
                        node {
                          name
                        }
                      }
                    }
                  }
                }
              }
            }
            GRAPHQL
            ),
            variables: {},
            context: context,
            only: only_filter,
          },
          {
            operation_name: "ViewerFeatureQuery",
            query: (
              <<~GRAPHQL
            query ViewerFeatureQuery {
              viewer {
                ...on User {
                  company {
                    features(first: 50) {
                      edges {
                        node {
                          name
                        }
                      }
                    }
                  }
                }
              }
            }
            GRAPHQL
            ),
            variables: {},
            context: context,
            only: only_filter,
          },
          {
            operation_name: "ViewerRoleQuery",
            query: (
              <<~GRAPHQL
            query ViewerRoleQuery {
              viewer {
                ...on User {
                  roles(first: 50) {
                    edges {
                      node {
                        name
                      }
                    }
                  }
                }
              }
            }
            GRAPHQL
            ),
            variables: {},
            context: context,
            only: only_filter,
          },
        ]
      end

      it_behaves_like "calls querylogger"

      context "with a mutation" do
        context "with an operation name and variables" do
          let(:job_input) do
            {
              job: {
                id: encoded_job_id,
                location: {
                  serviceLocation: service_location,
                },
              },
            }
          end
          let(:job) { create :fleet_managed_job, status: :pending, fleet_company: api_company, rescue_company: rescue_company }
          let(:encoded_job_id) { job.to_ssid }
          let(:service_location) { create :graphql_location_input_type }

          let(:variables) do
            {
              input: job_input,
            }
          end

          let(:operation_name) { 'UpdateJob' }

          let(:queries) do
            [
              *super(),
              {
                query: (
                  <<~GRAPHQL
                  mutation #{operation_name}($input: UpdateJobInput!) {
                    updateJob(input: $input) {
                      job {
                        id
                        location {
                          serviceLocation{
                            address
                            lat
                            lng
                            locationType
                          }
                        }
                      }
                    }
                  }
                GRAPHQL
                ),
                variables: variables,
                operation_name: operation_name,
                context: context,
                only: only_filter,
              },
            ]
          end

          it_behaves_like "calls querylogger"
        end
      end
    end
  end

  context "as a client api_application" do
    include_context "client api_application"
    it_behaves_like 'multiplex logger'
  end

  context 'as a partner api_user' do
    include_context "partner api_user"
    it_behaves_like 'multiplex logger'
  end

  context 'as a 3rd party partner api_company' do
    include_context "3rd party partner api_company"
    it_behaves_like 'multiplex logger'
  end

  context 'as a 3rd party partner api_user' do
    include_context "3rd party partner api_user"
    it_behaves_like 'multiplex logger'
  end
end
