# frozen_string_literal: true

require 'rails_helper'

describe DeprecationReasonFor do
  subject { SwoopSchema.deprecation_reason_for path }

  shared_examples "it works" do
    it "works" do
      expect(subject).to eq(expected_path)
    end
  end

  context 'with a root query path' do
    let(:path) { "Query.service" }
    let(:expected_path) { "Please use the node interface instead" }

    it_behaves_like "it works"
  end

  context 'with a mutation path' do
    let(:path) { "Mutation.addJob" }
    let(:expected_path) { "Please use createJob instead" }

    it_behaves_like "it works"
  end

  context 'with a non-root query path' do
    let(:path) { "Job.invoices" }
    let(:expected_path) { "Please use Job.jobInvoices field instead" }

    it_behaves_like "it works"
  end

  context 'with a bogus type' do
    let(:path) { "JobASF.invoices" }
    let(:expected_path) { nil }

    it_behaves_like "it works"
  end

  context 'with a bogus field' do
    let(:path) { "Job.invoicesasfasdf" }
    let(:expected_path) { nil }

    it_behaves_like "it works"
  end
end
