# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Subscriptions::PostgresqlStorage do
  subject do
    Subscriptions::SwoopWebhookSubscriptions.new(schema: SwoopSchema).instance_variable_get :@storage
  end

  let(:query_string) do
    <<~GRAPHQL.strip
      subscription #{operation_name} {
        jobStatusLeft(states: Active) {
          id
        }
      }
    GRAPHQL
  end
  let(:provided_variables) { { a: 1, b: 2 } }
  let(:context) do
    {
      c: 1,
      d: 2,
      request_id: SecureRandom.uuid,
      viewer: create(:user),
    }
  end
  let(:query) do
    OpenStruct.new(
      query_string: query_string,
      provided_variables: provided_variables,
      context: context,
      operation_name: operation_name
    )
  end
  let(:topics) { Faker::Hipster.sentences(number: 3).map { |t| OpenStruct.new(topic: t) } }

  describe '#write_subscription' do
    it "works" do
      expect { subject.write_subscription(query, topics) }
        .to change(GraphQLSubscription, :count).by(1)
        .and change(GraphQLTopic, :count).by(3)
      expect(GraphQLSubscription.last.graphql_topics.length).to eq(3)
    end
  end

  describe '#read_subscription' do
    let!(:subscription) { create :graphql_subscription }

    it "works" do
      expect(subject.read_subscription(subscription.id)).to eq({
        query_string: subscription.query_string,
        variables: subscription.variables,
        context: subscription.context,
        operation_name: subscription.operation_name,
      })
    end
  end

  describe '#each_subscription_id' do
    let(:topic) { create :graphql_topic }
    let(:event) { OpenStruct.new(topic: topic.name) }
    let!(:subscriptions) do
      s = create_list :graphql_subscription, 3
      topic.graphql_subscriptions << s
      s
    end

    it "works" do
      expect { |b| subject.each_subscription_id(event, &b) }
        .to yield_successive_args(*subscriptions.map(&:id))
    end
  end

  describe '#delete_subscription' do
    let(:topic) { create :graphql_topic }
    let!(:subscription) { create :graphql_subscription }
    let!(:subscription_with_topic) do
      s = create :graphql_subscription
      topic.graphql_subscriptions << s
      s
    end

    it "works" do
      expect { subject.delete_subscription(subscription_with_topic.id) }
        .to change(GraphQLSubscription, :count).from(2).to(1)
        .and change(GraphQLTopic, :count).from(1).to(0)
    end
  end

  skip '#clear'

  describe '#topics' do
    let!(:topics) do
      t = create_list :graphql_topic, 5
      t.each do |_t|
        _t.graphql_subscriptions << create_list(:graphql_subscription, rand(5))
      end
      t
    end

    it "works" do
      expect(subject.topics(limit: 2, offset: 0)).to eq([
        topics.first(2).map do |t|
          OpenStruct.new(
            name: t.name,
            subscriptions_count: t.graphql_subscriptions.count,
          )
        end,
        5,
      ])

      expect(subject.topics(limit: 2, offset: 2)).to eq([
        topics.slice(2, 2).map do |t|
          OpenStruct.new(
            name: t.name,
            subscriptions_count: t.graphql_subscriptions.count,
          )
        end,
        5,
      ])

      expect(subject.topics(limit: 2, offset: 4)).to eq([
        topics.slice(4, 2).map do |t|
          OpenStruct.new(
            name: t.name,
            subscriptions_count: t.graphql_subscriptions.count,
          )
        end,
        5,
      ])
    end
  end
end
