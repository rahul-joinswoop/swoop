# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Subscriptions::SwoopMultiSubscriptions do
  subject do
    Subscriptions::SwoopMultiSubscriptions.new(
      schema: SwoopSchema,
      redis: Swoop.graphql_redis_client,
      ably: Swoop.ably_rest_client,
    )
  end

  let(:event) { { a: 1 } }
  let(:object) { { b: 2 } }

  describe "#initialize" do
    it "works" do
      expect(subject.swoop_ably_subscriptions).to be_a(Subscriptions::SwoopAblySubscriptions)
      expect(subject.swoop_webhook_subscriptions).to be_a(Subscriptions::SwoopWebhookSubscriptions)
    end
  end

  describe "#trigger" do
    let(:event_name) { "Foo" }
    let(:args) { { a: 1 } }
    let(:scope) { "asdf1234" }

    it "works" do
      expect(subject.swoop_ably_subscriptions).to receive(:trigger).with(event_name, args, object, scope: scope).once
      expect(subject.swoop_webhook_subscriptions).to receive(:trigger).with(event_name, args, object, scope: scope).once
      subject.trigger(event_name, args, object, scope: scope)
    end
  end

  describe "#execute" do
    context "with a uuid" do
      let(:subscription_id) { SecureRandom.uuid }

      it "works" do
        expect(subject.swoop_ably_subscriptions).to receive(:execute).once.with(subscription_id, event, object)
        expect(subject.swoop_webhook_subscriptions).not_to receive(:execute)
        subject.execute(subscription_id, event, object)
      end
    end

    context "with a non-uuid" do
      let(:subscription_id) { SecureRandom.hex }

      it "works" do
        expect(subject.swoop_ably_subscriptions).not_to receive(:execute)
        expect(subject.swoop_webhook_subscriptions).to receive(:execute).once.with(subscription_id, event, object)
        subject.execute(subscription_id, event, object)
      end
    end
  end

  describe "#execute_all" do
    it "works" do
      expect(subject.swoop_ably_subscriptions).to receive(:execute_all).once.with(event, object)
      expect(subject.swoop_webhook_subscriptions).to receive(:execute_all).once.with(event, object)
      subject.execute_all(event, object)
    end
  end

  describe "#each_subscription_id" do
    it "works" do
      expect(subject.swoop_ably_subscriptions).to receive(:each_subscription_id).once.with(event)
      expect(subject.swoop_webhook_subscriptions).to receive(:each_subscription_id).once.with(event)
      subject.each_subscription_id(event)
    end
  end

  describe "#read_subscription" do
    context "with a uuid" do
      let(:subscription_id) { SecureRandom.uuid }

      it "works" do
        expect(subject.swoop_ably_subscriptions).to receive(:read_subscription).once.with(subscription_id)
        expect(subject.swoop_webhook_subscriptions).not_to receive(:read_subscription)
        subject.read_subscription(subscription_id)
      end
    end

    context "with a non-uuid" do
      let(:subscription_id) { SecureRandom.hex }

      it "works" do
        expect(subject.swoop_ably_subscriptions).not_to receive(:read_subscription)
        expect(subject.swoop_webhook_subscriptions).to receive(:read_subscription).once.with(subscription_id)
        subject.read_subscription(subscription_id)
      end
    end
  end

  describe "#deliver" do
    let(:result) { { a: 1, b: 2 } }

    context "with a uuid" do
      let(:subscription_id) { SecureRandom.uuid }

      it "works" do
        expect(subject.swoop_ably_subscriptions).to receive(:deliver).once.with(subscription_id, result, true)
        expect(subject.swoop_webhook_subscriptions).not_to receive(:deliver)
        subject.deliver(subscription_id, result)
      end
    end

    context "with a non-uuid" do
      let(:subscription_id) { SecureRandom.hex }

      it "works" do
        expect(subject.swoop_ably_subscriptions).not_to receive(:deliver)
        expect(subject.swoop_webhook_subscriptions).to receive(:deliver).once.with(subscription_id, result, true)
        subject.deliver(subscription_id, result)
      end
    end
  end

  describe "#write_subscription" do
    let(:events) { [] }

    context "with webhook_url" do
      let(:query) do
        OpenStruct.new(context: { webhook_url: 1 })
      end

      it "works" do
        expect(subject.swoop_ably_subscriptions).not_to receive(:write_subscription)
        expect(subject.swoop_webhook_subscriptions).to receive(:write_subscription).once.with(query, events)

        subject.write_subscription(query, events)
      end
    end

    context "without webhook_url" do
      let(:query) do
        OpenStruct.new(context: {})
      end

      it "works" do
        expect(subject.swoop_ably_subscriptions).to receive(:write_subscription).once.with(query, events)
        expect(subject.swoop_webhook_subscriptions).not_to receive(:write_subscription)
        subject.write_subscription(query, events)
      end
    end
  end

  describe "#delete_subscription" do
    context "with a uuid" do
      let(:subscription_id) { SecureRandom.uuid }

      it "works" do
        expect(subject.swoop_ably_subscriptions).to receive(:delete_subscription).once.with(subscription_id)
        expect(subject.swoop_webhook_subscriptions).not_to receive(:delete_subscription)
        subject.delete_subscription(subscription_id)
      end
    end

    context "with a non-uuid" do
      let(:subscription_id) { SecureRandom.hex }

      it "works" do
        expect(subject.swoop_ably_subscriptions).not_to receive(:delete_subscription)
        expect(subject.swoop_webhook_subscriptions).to receive(:delete_subscription).once.with(subscription_id)
        subject.delete_subscription(subscription_id)
      end
    end
  end
end
