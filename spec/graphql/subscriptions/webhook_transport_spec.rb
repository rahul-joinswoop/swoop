# frozen_string_literal: true

require 'rails_helper'
RSpec.describe ::Subscriptions::WebhookTransport do
  subject { ::Subscriptions::WebhookTransport.new }

  describe "#deliver" do
    let(:webhook_url) { Faker::Internet.url }
    let(:id) { SecureRandom.uuid }
    let(:webhook_secret) { SecureRandom.hex }
    let(:subscription) do
      s = create :graphql_subscription
      s.context[:webhook_url] = webhook_url
      s.context[:webhook_secret] = webhook_secret
      s.save!
      s
    end
    let(:result) { { id: id } }
    let(:more) { true }
    let(:body) { { result: result, more: more } }
    let(:headers) do
      {
        "Accept" => "application/json",
        "Content-Type" => "application/json; charset=utf-8",
        "Accept-Encoding" => "gzip",
        ::Subscriptions::WebhookTransport::GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET => webhook_secret,
        ::Subscriptions::WebhookTransport::GRAPHQL_SUBSCRIPTION_ID => subscription.to_ssid,
        ::Subscriptions::WebhookTransport::GRAPHQL_SUBSCRIPTION_VIEWER => subscription.viewer.to_ssid,
      }
    end

    it "works" do
      stub_request(:post, webhook_url).with(body: body.to_json, headers: headers)
      subject.deliver(subscription.id, result)
      expect(WebMock).to have_requested(:post, webhook_url).with(body: body.to_json, headers: headers).once
    end

    context "with more: false" do
      let(:result) { nil }
      let(:more) { false }

      it "works" do
        stub_request(:post, webhook_url).with(body: body.to_json, headers: headers)

        subject.deliver(subscription.id, result, more)
        expect(WebMock).to have_requested(:post, webhook_url).with(body: body.to_json, headers: headers).once
      end
    end

    context "with webhook errors" do
      let(:response_body) { Faker::Hipster.sentence }
      let(:response_headers) { { 'Custom-Header' => "123" } }

      it 'works' do
        stub_request(:post, webhook_url)
          .with(body: body.to_json, headers: headers)
          .to_return(body: response_body, status: 422, headers: response_headers)
        expect(Rollbar).to receive(:error).once do |error, **args|
          expect(args[:response]).to be_a(Hash)
          expect(args[:response]).to include({ "status" => 422, "body" => [response_body], "headers" => response_headers.to_a })
          expect(args[:payload]).to eq(body)
        end
        expect { subject.deliver(subscription.id, result) }.to raise_error(Subscriptions::WebhookTransport::Error)
        expect(WebMock).to have_requested(:post, webhook_url).with(body: body.to_json, headers: headers).once
      end
    end

    context "with graphql errors" do
      let(:result) do
        {
          errors: [
            { message: 'Cannot return null for non-nullable field' },
            { message: 'Some other error' },
          ],
        }.deep_stringify_keys
      end

      it "works" do
        expect(Rollbar).to receive(:error).once do |error, e|
          expect(error).to be_a(Subscriptions::WebhookTransport::SubscriptionError)
          expect(error.message).to eq(result.dig('errors', 1, 'message'))
          expect(e).to eq(result.dig('errors', 1))
        end

        subject.deliver(subscription.id, result)
      end
    end
  end
end
