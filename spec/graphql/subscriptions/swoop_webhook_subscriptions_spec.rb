# frozen_string_literal: true

require 'rails_helper'
RSpec.describe ::Subscriptions::SwoopWebhookSubscriptions do
  subject do
    ::Subscriptions::SwoopWebhookSubscriptions.new schema: SwoopSchema
  end

  describe "#deliver" do
    let(:id) { SecureRandom.uuid }
    let(:result) { { id: id } }
    let(:more) { true }

    it "works" do
      expect_any_instance_of(Subscriptions::WebhookTransport)
        .to receive(:deliver)
        .once
        .with(id, result, more)

      subject.deliver(id, result, more)
    end

    context "with more: false" do
      let(:result) { nil }
      let(:more) { false }

      it "works" do
        expect_any_instance_of(Subscriptions::WebhookTransport)
          .to receive(:deliver)
          .once
          .with(id, result, more)

        expect_any_instance_of(Subscriptions::PostgresqlStorage)
          .to receive(:delete_subscription)
          .once
          .with(id)

        subject.deliver(id, result, more)
      end
    end
  end
end
