# frozen_string_literal: true

require 'rails_helper'

describe Subscriptions::SwoopSubscriptionExecute do
  subject do
    SwoopSchema.subscriptions.trigger('jobUpdated', { id: job.to_ssid }, job, scope: rescue_driver)
  end

  let(:rescue_company) { create :rescue_company }
  let(:rescue_driver) { create :user, :rescue, :driver, company: rescue_company }
  let(:job) { create :rescue_job, rescue_company: rescue_company, rescue_driver: rescue_driver }
  let(:variables) { { id: job.to_ssid }.deep_stringify_keys }
  let(:doorkeeper_token) { create :doorkeeper_access_token, scopes: '', resource_owner_id: rescue_driver.id }
  let(:webhook_url) { "http://example.com/callback" }
  let(:webhook_secret) { "foobar" }
  let(:headers) do
    {
      ::Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_URL => webhook_url,
      ::Subscriptions::WebhookTransport::HTTP_GRAPHQL_SUBSCRIPTION_WEBHOOK_SECRET => webhook_secret,
    }
  end
  let(:auth_result) { GraphQL::Authenticate.call(input: { request: request }) }
  let(:query) do
    q = {
      query: subscription,
      variables: variables,
      context: auth_result.dig(:output, :context),
      operation_name: operation_name,
      only: auth_result.dig(:output, :only_filter),
      root_value: auth_result.dig(:output, :context, :api_company),
    }
    Utils::QueryVariableInliner.inline! q
    q
  end
  let(:request) do
    # rubocop:disable Rails/SaveBang
    request = ActionDispatch::TestRequest.create
    # rubocop:enable Rails/SaveBang
    request.env.merge!(headers) if headers
    request.env['HTTP_AUTHORIZATION'] = "Bearer #{doorkeeper_token.token}" if doorkeeper_token
    request.request_id = SecureRandom.uuid
    request.params[:operationId] = SecureRandom.uuid
    request
  end
  let(:subscription) do
    <<~GRAPHQL
      subscription #{operation_name}($id: ID!) {
        jobUpdated(id: $id) {
          id
          status
          # editable is selected here because we need a field that has some required_roles
          # on it, otherwise we won't trigger a failure.
          editable
        }
      }
    GRAPHQL
  end
  let(:response) { SwoopSchema.execute(query) }

  it 'works' do
    # create a webhook subscription
    expect(response.subscription?).to be(true)

    # stub out our deliver call to make sure that we're not getting any errors
    # passed in
    expect_any_instance_of(Subscriptions::WebhookTransport)
      .to receive(:deliver) { |_, _, result| expect(result.dig('errors')).to be_blank }
      .once

    # and trigger a subscription
    subject
  end

  it "doesn't work if the access token is expired" do
    # create a webhook subscription
    expect(response.subscription?).to be(true)

    doorkeeper_token.revoke

    expect_any_instance_of(Subscriptions::WebhookTransport)
      .to receive(:deliver) do |_, _, result, more|
        expect(result).to be_nil
        expect(more).to be_falsey
      end
      .once

    expect { subject }.not_to raise_error
  end

  context 'without :api_roles in our serialized context' do
    it 'works' do
      # create a webhook subscription
      expect(response.subscription?).to be(true)

      # load up our subscription and remove the api_roles from its serialized context
      subscription = SomewhatSecureID.load! response.context[:subscription_id]
      subscription.context.delete :api_roles
      subscription.save!

      # stub out our deliver call to make sure that we're not getting any errors
      # passed in
      expect_any_instance_of(Subscriptions::WebhookTransport)
        .to receive(:deliver) { |_, _, result| expect(result.dig('errors')).to be_blank }
        .once

      # and trigger a subscription
      subject
    end
  end

  context 'without :api_access_token in our serialized context' do
    it 'works' do
      # create a webhook subscription
      expect(response.subscription?).to be(true)

      # load up our subscription and remove the api_roles from its serialized context
      subscription = SomewhatSecureID.load! response.context[:subscription_id]
      subscription.context.delete :api_access_token
      subscription.save!

      # stub out our deliver call to make sure that we're not getting any errors
      # passed in
      expect_any_instance_of(Subscriptions::WebhookTransport)
        .to receive(:deliver) { |_, _, result| expect(result.dig('errors')).to be_blank }
        .once

      # and trigger a subscription
      subject
    end
  end
end
