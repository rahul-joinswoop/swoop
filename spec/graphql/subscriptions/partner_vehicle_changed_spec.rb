# frozen_string_literal: true

require 'rails_helper'
RSpec.describe "partnerVehicleChanged" do
  # this spec only checks the that the subscription parses and is valid - we're
  # not actually checking where or what data is being sent back to the client
  # (that's currently done in individual models but maybe it shouldn't be)
  let(:query) do
    <<~GRAPHQL
        subscription #{operation_name} {
          partnerVehicleChanged {
            id
          }
        }
      GRAPHQL
  end

  context "as a partner api_user" do
    include_context "partner api_user"
    let(:user) { api_user }

    it "works" do
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
    end
  end

  context "as a 3rd party partner api_company" do
    include_context "3rd party partner api_company"
    let(:user) { rescue_driver }

    it "works" do
      expect { response }.not_to raise_error
      expect(response).not_to include('errors')
    end
  end
end
