# frozen_string_literal: true

require 'rails_helper'
RSpec.describe ::Subscriptions::SwoopAblySubscriptions do
  subject do
    ::Subscriptions::SwoopAblySubscriptions.new schema: SwoopSchema,
                                                ably: ably,
                                                redis: redis
  end

  let(:ably) { double('ably') }
  let(:ably_channel) { double('ably_channel') }
  let(:redis) { double('redis') }

  describe "#deliver" do
    let(:id) { SecureRandom.uuid }
    let(:result) { { id: id } }

    it "works" do
      expect(ably)
        .to receive(:channel)
        .and_return(ably_channel)

      expect(ably_channel)
        .to receive(:publish)
        .with("update", { more: true, result: result })

      subject.deliver(id, result)
    end

    context "with more: false" do
      let(:result) { nil }
      let(:more) { false }

      it "works" do
        expect(ably)
          .to receive(:channel)
          .and_return(ably_channel)

        expect(ably_channel)
          .to receive(:publish)
          .with("update", { more: more, result: result })

        expect_any_instance_of(::Subscriptions::SwoopAblySubscriptions)
          .to receive(:delete_subscription)
          .once
          .with(id)

        subject.deliver(id, result, more)
      end
    end

    context "with errors" do
      let(:result) do
        {
          errors: [
            { message: 'Cannot return null for non-nullable field' },
            { message: 'Some other error' },
          ],
        }.deep_stringify_keys
      end

      it "works" do
        expect(Rollbar).to receive(:error).once do |error, e|
          expect(error).to be_a(Subscriptions::SwoopAblySubscriptions::SubscriptionError)
          expect(error.message).to eq(result.dig('errors', 1, 'message'))
          expect(e).to eq(result.dig('errors', 1))
        end

        subject.deliver(id, result)
      end
    end
  end
end
