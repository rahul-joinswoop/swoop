# frozen_string_literal: true

require 'rails_helper'

describe Subscriptions::Debouncer do
  context 'as a partner' do
    include_context "partner api_user"

    let(:subscription_id) { graphql_subscription.id }
    let(:object) { create :user }
    let(:doorkeeper_access_token) do
      create :doorkeeper_access_token, scopes: :rescue, resource_owner_id: api_user.id
    end
    let(:context) { super().merge(api_access_token: doorkeeper_access_token) }
    let(:job) do
      create :rescue_job, status: :assigned, rescue_company: api_company, rescue_driver: api_user
    end
    let(:variables) { { id: job.to_ssid } }
    let!(:graphql_subscription) do
      s = create :graphql_subscription,
                 viewer: api_user,
                 operation_name: operation_name,
                 query_string: query,
                 variables: variables,
                 context: context
      s.graphql_topics << graphql_topic
      s
    end
    let(:subscription_topic) do
      GraphQL::Subscriptions::Event.serialize(
        'jobUpdated',
        variables.deep_stringify_keys,
        SwoopSchema.get_field(SwoopSchema.subscription, 'jobUpdated'),
        scope: api_user
      )
    end
    let(:graphql_topic) { create :graphql_topic, name: subscription_topic }
    let(:query) do
      <<~GRAPHQL
        subscription #{operation_name}($id: ID!) {
          jobUpdated(id: $id) {
            id
            status
          }
        }
      GRAPHQL
    end
    let(:run_query) do
      -> {
        execute(
          query: query,
          context: context,
          operation_name: operation_name,
          variables: variables,
          root_value: job,
          only: only_filter,
          subscription_topic: subscription_topic,
        )
      }
    end

    context 'with debouncing disabled' do
      it "works" do
        ClimateControl.modify GRAPHQL_DEBOUNCING: "" do
          # trigger our subscription and verify the response
          result = run_query.call
          expect(result.to_h.dig('data', 'jobUpdated', 'status')).to eq('Assigned')
          described_class.debounce!(graphql_subscription.id, result, job, api_user)

          # trigger our subscription again and verify the identical response
          result = run_query.call
          expect(result.to_h.dig('data', 'jobUpdated', 'status')).to eq('Assigned')
          described_class.debounce!(graphql_subscription.id, result, job, api_user)

          # update our job
          job.update! status: :enroute

          # and trigger our subscription and verify response changed
          result = run_query.call
          expect(result.to_h.dig('data', 'jobUpdated', 'status')).to eq('EnRoute')
          described_class.debounce!(graphql_subscription.id, result, job, api_user)
        end
      end
    end

    context 'with debouncing enabled' do
      it "works" do
        ClimateControl.modify GRAPHQL_DEBOUNCING: "true" do
          # trigger our subscription and verify the response
          result = run_query.call
          expect(result.to_h.dig('data', 'jobUpdated', 'status')).to eq('Assigned')
          described_class.debounce!(graphql_subscription.id, result, job, api_user)

          # trigger our subscription again - since the response will be identical we
          # should raise an error
          result = run_query.call
          expect(result.to_h.dig('data', 'jobUpdated', 'status')).to eq('Assigned')
          expect do
            described_class.debounce!(graphql_subscription.id, result, job, api_user)
          end.to raise_error(GraphQL::Schema::Subscription::NoUpdateError)

          # update our job
          job.update! status: :enroute

          # trigger our subscription again and verify response changed (and no error raised)
          result = run_query.call
          expect(result.to_h.dig('data', 'jobUpdated', 'status')).to eq('EnRoute')
          described_class.debounce!(graphql_subscription.id, result, job, api_user)
        end
      end
    end
  end
end
