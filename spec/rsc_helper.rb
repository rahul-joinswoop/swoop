# frozen_string_literal: true

require 'rails_helper'

# Load canned response copied from Agero RSC API docs.
def rsc_api_response(name)
  (Rails.root + "spec" + "fixtures" + "agero_rsc_api" + "#{name}.json").read
end
