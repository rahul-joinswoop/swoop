# frozen_string_literal: true

require "rails_helper"

feature "Release To", js: true, vcr: true do
  include System::JobHelpers
  let(:company) { create :rescue_company, :finish_line, :with_storage, :with_account }
  let(:account) { company.accounts.first }
  let(:user) { create :user, :rescue, company: company }

  skip do
    context "customer" do
      it "works" do
        login user

        create_job do
          set_account account.name
          set_will_store
        end

        job_id = get_job_id_from_notification
        change_status job_id, "En Route"
        change_status job_id, "Completed"
        expect(page).to have_content('has been completed.')
        close_all_notifications

        goto_storage

        edit_job(job_id) do
          fill_release_to_customer
        end

        expect(page).to have_content('has been edited.')
        close_all_notifications

        edit_job(job_id) do
          expect(find('#Containerstoragereleased_to_account_id input').value).to eq "Customer"
          expect(find('#Containerstoragereleased_to_userfull_name input').value).to eq "Release Name"
          expect(find('#Inputstoragereplace_invoice_info').value).to eq "true"
        end

        expect(page).to have_content('has been edited.')
        close_all_notifications

        view_options(job_id)

        # TODO: need to fix these as the invoice is now a pdf https://gist.github.com/joemasilotti/6045144
        # select_options_item("View Invoice")
        # new_window=page.driver.browser.window_handles.last
        # page.within_window new_window do
        #  check_customer
        #  closeTab
        # end
        logout
      end
    end

    context "account" do
      it "works" do
        login user

        create_job do
          set_account user.company.accounts.first.name
          set_will_store
        end

        job_id = get_job_id_from_notification
        change_status job_id, "En Route"
        change_status job_id, "Completed"

        expect(page).to have_content('has been completed.')
        close_all_notifications

        goto_storage

        edit_job(job_id) do
          fill_release_to_account account.name
          expect(page).not_to have_selector('#Containerstoragereleased_to_userfull_name input')
        end

        expect(page).to have_content('has been edited.')
        close_all_notifications

        edit_job(job_id) do
          expect(find('#Containerstoragereleased_to_account_id input').value).to eq account.name
          expect(page).not_to have_selector('#Containerstoragereleased_to_userfull_name input')
          expect(find('#Inputstoragereplace_invoice_info')).to be_checked
        end

        expect(page).to have_content('has been edited.')
        close_all_notifications

        view_options(job_id)

        # TODO: need to fix these as the invoice is now a pdf https://gist.github.com/joemasilotti/6045144
        # select_options_item("View Invoice")
        # new_window=page.driver.browser.window_handles.last
        # page.within_window new_window do
        #  check_account
        #  closeTab
        # end
        logout
      end
    end
  end
end
