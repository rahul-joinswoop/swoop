# frozen_string_literal: true

require "rails_helper"

feature "Root can list users", js: true do
  context "with valid credentials" do
    let(:admin) { create(:user, :root) }

    before do
      log_in admin
    end

    scenario "users list is available" do
      visit "#user/settings"

      find('.qnav a', text: 'Users').click

      expect(page).to have_content("Create User")
    end
  end
end
