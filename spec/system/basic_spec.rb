# frozen_string_literal: true

require "rails_helper"
feature "Basic test", js: true do
  include System::JobHelpers

  skip do
    context "fleet_managed" do
      let!(:company) { create :fleet_company, :fleet_demo, :with_service_codes }
      let!(:user) { create :user, :fleet_demo, company: company }

      scenario "create_job" do
        log_in user
        create_job
        log_out
      end
    end

    context "tesla" do
      let!(:company) { create :fleet_company, :tesla, :with_service_codes }
      let!(:user) { create :user, :tesla, company: company }

      scenario "create_job" do
        log_in user
        create_job
        log_out
      end
    end

    context "fleet" do
      let!(:company) { create :fleet_company, :fleet_demo, :with_service_codes }
      let!(:user) { create :user, :fleet_demo, company: company }

      scenario "create_job" do
        log_in user
        create_job
        log_out
      end
    end
  end
end
