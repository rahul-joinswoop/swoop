# frozen_string_literal: true

require "rails_helper"

feature "User", js: true, vcr: true do
  feature "Login" do
    let(:company_name) { user.company.name }

    # defined like this  since the wrapping logic is mostly identical but the
    # middle section is potentially different
    def login_flow
      login user
      expect(page).to have_css('.userName', text: user.full_name)
      expect(page).to have_css('.companyName', text: company_name)

      yield if block_given?

      log_out
      expect(page).not_to have_css('.userName', text: user.full_name)
      expect(page).not_to have_css('.companyName', text: company_name)
      expect(current_path_info).to eq("/login")
      expect(page).to have_content("LOG IN")
      expect(page).to have_button("Log In")
    end

    context "as admin" do
      let(:user) { create :user, :root }
      let(:company_name) { "Administrator" }

      scenario "login flow" do
        login_flow do
          expect(current_path_info).to eq("/partner/dispatch")
          expect(page).to have_content("DASHBOARD")
          # expect(page).to have_content("MAP")
          expect(page).to have_content("REPORTING")
        end
      end

      scenario "with an invalid password" do
        log_in username: user.username, password: "invalid password"
        wait_for_ajax
        expect(page).to have_content("Please enter a valid email and password")
      end
    end

    context "as rescue" do
      let(:user) { create :user, :rescue }

      scenario "login flow" do
        login_flow
      end
    end

    context "as a fleet in house" do
      let(:user) { create :user, :tesla }

      scenario "login flow" do
        login_flow
      end
    end

    context "as a fleet managed" do
      let(:user) { create :user, :fleet_demo }

      scenario "login flow" do
        login_flow
      end
    end
  end
end

# TODO this is commented because UserList on FE now relies on ES search mechanism
# context "within settings tab" do
#   let!(:user) do
#     create(:user, :root,
#       roles: [:driver],
#       email: "asd@asd.asd",
#       username: "asd"
#     )
#   end
#
#   scenario "can list other users" do
#     # FIXME
#     # User list is probably cached at this point, without reloading
#     # there's still only one user visible
#     users = User.all
#     visit "#user/settings"
#     find('.qnav a', text: "Users", exact: true).click
#     expect(page).to have_content(admin.first_name)
#     expect(page).to have_content(admin.last_name)
#     expect(page).to have_content(admin.email)
#     # FIXME caching problems
#     visit "#user/settings"
#     find('.qnav a', text: "Users", exact: true).click
#     expect(page).to have_content(user.first_name)
#     expect(page).to have_content(user.last_name)
#     expect(page).to have_content(user.email)
#   end
# end
# end
