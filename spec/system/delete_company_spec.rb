# frozen_string_literal: true

require "rails_helper"

feature "Admin deletes Company", js: true, vcr: true do
  let(:admin) do
    root_user = create(:user, :root)
    root_user.company.add_features_by_name(["Settings"])
    root_user
  end

  let!(:company) { create :company, name: "VeryRandomCompany" }

  # there is problem with fixtures and JS spec
  before(:each) do
    allow_any_instance_of(CompanyService::ChangeFeatures).to receive(:remove_storage_service_from_company_if_needed)
    log_in admin
  end

  scenario "is logged in" do
    # after sign-in
    expect(current_path_info).to eq("/partner/dispatch")

    # settings gear icon
    find('.settings-cog').click
    # wait_for_ajax

    # companies list
    find('a', text: "Companies").click
    # wait_for_ajax
  end
end
