# frozen_string_literal: true

require "rails_helper"
RSpec.describe "Admin performs basic tasks", js: true do
  include System::JobHelpers

  let(:admin) { create(:user, roles: roles, company: company) }

  before(:each) { log_in admin }

  after(:each) { log_out }

  context "for a SuperCompany" do
    let(:company) { create(:super_company) }
    let(:roles) { [:root, :admin] }
    let(:user) { create(:user, roles: roles, company: company) }

    context "within dispatch panel" do
      before(:each) do
        clear_storage

        visit "/#login"
        username = user.respond_to?(:dig) ? user.dig(:username) : user.username
        password = user.respond_to?(:dig) ? user.dig(:password) : user.password
        fill_in "user_username_or_email", with: username
        fill_in "user_password", with: password
        click_button "Log In"
        wait_for_ajax
        visit("/#partner/dispatch")
      end

      it "does NOT see a new job button" do
        expect(page).to have_content("PENDING")
        expect(page).to have_content("IN-PROGRESS")
        expect(page).to have_button("New Job")
      end
    end
  end

  context "for Tesla" do
    let(:company) { create(:fleet_in_house_company, :tesla, :with_service_codes) }
    let(:roles) { [:admin, :fleet] }

    context "within dispatch panel" do
      before(:each) do
        log_in admin
        visit("/#partner/dispatch")
      end

      it "sees a new job button" do
        expect(page).to have_content("PENDING")
        expect(page).to have_content("IN-PROGRESS")
        expect(page).to have_button("New Job")
      end

      context "manually creating a new create job" do
        before do
          click_button("New Job")
          Company.last.reload
          populate_job
        end

        it "is not able to change car make" do
          expect(page).not_to have_css("#Containermake input")
        end
      end
    end
  end

  context "for a RescueCompany" do
    let(:company) { create(:rescue_company, :finish_line) }
    let(:roles) { [:admin, :rescue] }

    context "within dispatch panel" do
      before(:each) { visit("/#partner/dispatch") }

      it "sees a new job button" do
        expect(page).to have_content("PENDING")
        expect(page).to have_content("IN-PROGRESS")
        expect(page).to have_button("New Job")
      end
    end
  end
end
