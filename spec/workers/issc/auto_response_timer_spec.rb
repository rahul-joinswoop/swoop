# frozen_string_literal: true

require "rails_helper"

describe ISSC::AutoResponseTimer do
  include_context "basic rsc setup"

  subject { described_class.new.perform(job_id) }

  let(:vendor_id) { "1" }
  let(:facility_id) { "2" }

  let(:job) { create(:job, account: agero_account) }
  let(:job_id) { job.id }

  let(:job_double) do
    double("FleetMotorClubJob",
           id: job.id,
           account: agero_account,
           rescue_company: rescue_company,
           digital_dispatcher: :issc,
           can_auto_respond?: true)
  end

  before do
    allow(Job).to receive(:find).and_return(job_double)
  end

  context "when account doesn't have RSC enabled but has auto accept enabled" do
    before do
      allow(agero_account).to receive(:rsc_enabled?).and_return(false)
      agero_account.auto_accept = true
    end

    it "attempts to accept job via ISSC" do
      expect(IsscAutoAcceptJob).to receive(:perform_async)

      subject
    end
  end

  context "when account has RSC and auto_accept enabled" do
    before do
      allow(job_double).to receive(:digital_dispatcher).and_return(:rsc)
      allow(agero_account).to receive(:rsc_enabled?).and_return(true)

      agero_account.auto_accept = true
    end

    it "attempts to accept job" do
      expect { subject }.to raise_error(ArgumentError)
    end
  end
end
