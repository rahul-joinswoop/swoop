# frozen_string_literal: true

require 'rails_helper'

describe AssignRescueDriverVehicleToJobWorker do
  subject { described_class.new.perform(job_ids) }

  context 'with a single job' do
    let(:job) { create :job }
    let(:job_ids) { job.id }

    it 'works' do
      expect(AssignRescueDriverVehicleToJob).to receive(:call).once.with(job: job)
      subject
    end
  end

  context 'with multiple jobs' do
    let(:jobs) { create_list :job, 3 }
    let(:job_ids) { jobs.map(&:id) }

    it 'works' do
      jobs.each do |job|
        expect(AssignRescueDriverVehicleToJob).to receive(:call).once.with(job: job)
      end
      subject
    end
  end
end
