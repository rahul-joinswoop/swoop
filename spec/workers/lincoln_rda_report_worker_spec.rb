# frozen_string_literal: true

require 'rails_helper'

describe LincolnRdaReportWorker do
  let(:report) { LincolnRdaReportWorker.new }
  let(:lincoln) { create(:fleet_company, :lincoln) }
  let(:warranty_program) { create(:client_program, company: lincoln, code: "lincoln_warranty") }

  describe "generate_report" do
    it "generates a report for current jobs and yield it as an unread stream", freeze_time: Time.parse("2019-01-21T12:01:05-0500") do
      job_1 = Timecop.travel(8.days.ago) { create(:job, :completed, fleet_company: lincoln, email: "user@example.com") }
      job_2 = Timecop.travel(1.day.ago) { create(:job, :completed, fleet_company: lincoln, email: "user@example.com") }
      job_3 = create(:job, :completed, fleet_company: lincoln, email: "user@example.com")
      job_4 = create(:job, :completed, fleet_company: create(:fleet_company, name: "Other"), email: "user@example.com")
      job_5 = create(:job, :goa, fleet_company: lincoln, email: "user@example.com")
      job_6 = Timecop.travel(25.hours.from_now) { create(:job, :completed, fleet_company: lincoln, email: "user@example.com") }
      job_7 = create(:job, :completed, fleet_company: lincoln, email: "user@example.com")
      job_8 = create(:job, :completed, fleet_company: lincoln, email: "user@example.com", service_code: create(:service_code, name: ServiceCode::REIMBURSEMENT))
      create(:job, :completed, fleet_company: lincoln, email: nil)

      [job_1, job_2, job_4, job_5, job_6, job_8].each do |job|
        job.pcc_coverage = build(:pcc_coverage, :lincoln, result: PCC::Coverage::COVERED_RESULT)
        job.save!
      end
      job_3.pcc_coverage = build(:pcc_coverage, :lincoln, result: PCC::Coverage::PARTIAL_COVERAGE_RESULT)
      job_3.save!
      job_3.pcc_coverage.pcc_policy = create(:pcc_policy, :lincoln, job: job_3, company: lincoln, uuid: "123456")
      job_7.pcc_coverage = build(:pcc_coverage, :lincoln, result: PCC::Coverage::NOT_COVERED_RESULT)
      job_7.save!

      policy_customer = { "last_name" => "Smith", "first_name" => "Joe", "street_address" => "123 Main St.", "city" => "Oak Park", "region" => "Illinois", "postal_code" => "60304" }
      expect_any_instance_of(PolicyLookupService::Client).to receive(:policy).with("123456").and_return("customers" => [policy_customer])

      data = []
      report.generate_report(Time.now.to_date) do |stream|
        CSV.new(stream, headers: true).each do |row|
          data << row
        end
      end

      job_ids = data.map { |row| row["purch_order_id"].to_i }
      expect(job_ids).to match_array([job_2.id, job_3.id])

      str_data = data.flatten.join
      expect(str_data).to include "123 Main St."
      expect(str_data).to include "Oak Park"
      expect(str_data).to include "Illinois"
      expect(str_data).to include "60304"
    end
  end

  describe "perform" do
    it "generates and send the report to an sftp server" do
      stream = StringIO.new("data")
      expect(report).to receive(:generate_report).with(Date.new(2019, 1, 21)).and_yield(stream)
      expect_any_instance_of(SftpServer).to receive(:upload).with(stream, "incoming/LincolnRDAReport.2019-01-21.csv")

      ClimateControl.modify("LINCOLN_SFTP_SERVER_URI": "sftp://user@localhost", "LINCOLN_SFTP_PASSWORD": "foo") do
        report.perform("2019-01-21")
      end
    end
  end
end
