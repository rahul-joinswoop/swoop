# frozen_string_literal: true

require 'rails_helper'
require 'sidekiq/scheduled'

class NonsenseWorker

  include Sidekiq::Worker
  include SidekiqUniqueScheduledWorkerManagement

  def self.nonsense_called(*args)
    puts ".called"
  end

  def call(*args)
    self.class.nonsense_called(*args)
  end

end

describe SidekiqUniqueScheduledWorkerManagement do
  subject { NonsenseWorker }

  around(:each) do |example|
    Sidekiq::Testing.disable! do
      example.run
    end
  end

  def run_scheduled_jobs
    Sidekiq::Testing.inline! do
      enq = Sidekiq::Scheduled::Enq.new
      enq.enqueue_jobs(10.minutes.from_now.to_f.to_s)
    end
  end

  shared_examples 'it works' do
    context '.schedule' do
      it 'queues one job' do
        expect(subject).to receive(:nonsense_called).with(1).once
        subject.schedule(180, 1)
        run_scheduled_jobs
      end
    end

    context '.deschedule' do
      it 'causes a no-op' do
        expect(subject).not_to receive(:nonsense_called)
        subject.deschedule(180, 1)
        run_scheduled_jobs
      end
    end
  end

  context 'has nothing scheduled' do
    it_behaves_like 'it works'
  end

  context 'has a job already scheduled' do
    before do
      subject.schedule(180, 1)
    end

    it_behaves_like 'it works'
  end

  context 'has a job descheduled' do
    before do
      subject.schedule(180, 1)
      subject.deschedule(180, 1)
    end

    it 'has the no-op key set' do
      key = 'NonsenseWorker__1'
      expect(RedisClient[:default].get(key)).to eq key
    end

    it_behaves_like 'it works'
  end
end
