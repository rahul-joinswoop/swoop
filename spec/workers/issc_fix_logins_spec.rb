# frozen_string_literal: true

require "rails_helper"

describe IsscFixLogins do
  include_context "issc provider setup"

  subject do
    IsscFixLogins.new(connection_manager: connection_manager).perform
  end

  let(:contractorid) { "32498734581987" }

  let(:issc1) { create(:ago_issc, contractorid: contractorid, status: 'LoggedIn', company: company, site: issc1_site) }
  let(:issc2) { create(:ago_issc, contractorid: contractorid, status: 'LoggedIn', company: company, site: issc2_site) }

  let(:connection_manager) do
    double("ConnectionManager")
  end

  context "when two contractorids with different locations" do
    before do
      issc1.save!
      issc2.save!

      allow(connection_manager).to receive(:connected?).and_return(true)
    end

    context "when both out of hours" do
      let(:issc1_site) { create(:site, within_hours: false, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, dispatchable: true) }

      it "logouts" do
        expect(IsscLogoutProvider).to receive(:perform_async).with([issc1.id, issc2.id])

        subject
      end

      context 'rsc db row exists' do
        let!(:rsc_issc) do
          create(:ago_issc, {
            contractorid: contractorid,
            status: 'LoggedIn',
            company: company,
            site: issc2_site,
            system: Issc::RSC_SYSTEM,
          })
        end

        it "logouts only the issc ones" do
          expect(IsscLogoutProvider).to receive(:perform_async).with([issc1.id, issc2.id])

          subject
        end
      end
    end

    context "when two agero sites are in a group with force open" do
      let(:issc1_site) { create(:site, within_hours: false, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, force_open: true, dispatchable: true) }

      it "remains logged in" do
        subject

        expect(issc1.status).to eql('logged_in')
        expect(issc2.status).to eql('logged_in')
      end
    end

    context "when one agero site is in hours" do
      let(:issc1_site) { create(:site, within_hours: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, dispatchable: true) }

      it "logs out both" do
        expect(IsscLogoutProvider).to receive(:perform_async).with([issc1.id, issc2.id])

        subject
      end
    end

    context "when one agero site is in hours and one is force open" do
      let(:issc1_site) { create(:site, within_hours: true, force_open: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, dispatchable: true) }

      it "both remain logged in" do
        subject

        expect(issc1.status).to eql('logged_in')
        expect(issc2.status).to eql('logged_in')
      end
    end

    context "when same contractorids on different sites" do
      let(:issc3) { create(:ago_issc, status: 'LoggingOut', company: company, site: issc3_site) }

      let(:issc1_site) { create(:site, within_hours: false, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, dispatchable: true) }
      let(:issc3_site) { create(:site, within_hours: true, dispatchable: true) }

      before do
        issc3.contractorid = contractorid
        issc3.save!
      end

      it "logs out sites 1 and 2 ignores the one already in LoggingOut" do
        expect(IsscLogoutProvider).to receive(:perform_async).with([issc1.id, issc2.id])

        subject
      end
    end
  end

  context 'disconnected' do
    let!(:rsc_issc1)  do
      create(:ago_issc, {
        contractorid: contractorid,
        status: 'LoggedIn',
        company: company,
        system: Issc::RSC_SYSTEM,
      })
    end
    let(:issc1_site) { create(:site, within_hours: false, dispatchable: true) }
    let(:issc2_site) { create(:site, within_hours: false, dispatchable: true) }

    before do
      issc1.save!
      issc2.save!
    end

    it 'logs all ISSC isscs out, leaves RSC' do
      expect(connection_manager).to receive(:connected?).and_return(false)
      expect(rsc_issc1.reload.status).to eql('logged_in')
      subject
      expect(issc1.reload.status).to eql('registered')
      expect(issc2.reload.status).to eql('registered')
    end
  end
end
