# frozen_string_literal: true

require 'rails_helper'

describe UpdateVehicleLocationWorker do
  let(:rescue_company) { create(:rescue_company) }
  let(:vehicle) { create(:rescue_vehicle, :with_driver, company: rescue_company, lat: nil, lng: nil, location_updated_at: nil) }
  let(:time) { Time.new(2018, 10, 23, 14, 15, 30) }

  it "updates the location of the vehicle", freeze_time: true do
    UpdateVehicleLocationWorker.new.perform(rescue_company.id, vehicle.id, 45.1, -110.1, time.utc.iso8601)
    vehicle.reload

    expect(vehicle.lat).to eq(45.1)
    expect(vehicle.lng).to eq(-110.1)
    expect(vehicle.location_updated_at).to eq time

    expect(vehicle.current_location.lat).to eq(45.1)
    expect(vehicle.current_location.lng).to eq(-110.1)
    expect(vehicle.current_location.timestamp).to eq time
  end

  it "raises an error if the vehicle company doesn't match the authenticated company" do
    other_company = create(:rescue_company)
    expect do
      UpdateVehicleLocationWorker.new.perform(other_company.id, vehicle.id, 45.1, -110.1, time.utc.iso8601)
    end
    vehicle.reload
    expect(vehicle.lat).not_to eq(45.1)
    expect(vehicle.current_location).to eq nil
  end

  context '#run_geofencing' do
    subject { UpdateVehicleLocationWorker.new }

    let(:action) { subject.run_geofencing(vehicle) }

    before do
      allow(subject).to receive(:geofencing_enabled?).and_return(true)
    end

    it 'runs EligiblityEvaluator' do
      expect(Geofence::EligibilityEvaluator).to receive(:call)
      action
    end

    it 'does nothing if geofencing is disabled' do
      allow(subject).to receive(:geofencing_enabled?).and_return(false)
      expect(Geofence::EligibilityEvaluator).not_to receive(:call)
      action
    end

    it 'does nothing if driver is not on duty' do
      allow(vehicle.driver).to receive(:on_duty?).and_return(false)
      expect(Geofence::EligibilityEvaluator).not_to receive(:call)
      action
    end
  end
end
