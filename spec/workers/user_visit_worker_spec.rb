# frozen_string_literal: true

require "rails_helper"

describe UserVisitWorker do
  let(:user) { create(:user) }
  let(:date) { Date.today }
  let(:application) { SourceApplication.create!(platform: SourceApplication::PLATFORM_WEB, source: "Swoop") }

  it "creates a visit record" do
    UserVisitWorker.new.perform(date.iso8601, user.id, application.id)
    visit = User::Visit.find_by(visit_date: date, user_id: user.id, source_application_id: application.id)
    expect(visit.session_count).to eq 1
  end

  it "increments the session count on a visit" do
    visit = User::Visit.create!(visit_date: date, user_id: user.id, source_application_id: application.id, session_count: 2)
    UserVisitWorker.new.perform(date.iso8601, user.id, application.id)
    visit.reload
    expect(visit.session_count).to eq 3
  end
end
