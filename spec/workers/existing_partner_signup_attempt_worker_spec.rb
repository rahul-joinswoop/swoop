# frozen_string_literal: true

require 'rails_helper'

describe ExistingPartnerSignupAttemptWorker do
  subject(:worker) do
    ExistingPartnerSignupAttemptWorker.new
  end

  before do
    allow(AuditEmail).to receive(:create).and_return(true)
    allow(Company).to receive(:find).and_return(company)
  end

  let(:company) do
    double(:company,
           id: 1,
           dispatch_email: 'ronnie@example.com',
           location: location,
           name: 'Foo',
           phone: '+14155551234',
           primary_contact: primary_contact)
  end
  let(:location) do
    double(:location, address: '123 Street San Francisco, CA 94116')
  end
  let(:primary_contact) { nil }

  context 'with a primary contact' do
    let(:primary_contact) do
      double(:primary_contact, name: 'Jackie', email: 'jswoop@example.com')
    end

    it 'sends an email with primary contact info' do
      expect(worker).to receive(:send_email_notification)
        .with(company,
              company.primary_contact.email,
              company.primary_contact.name)
      worker.perform(company.id)
    end
  end

  context 'without a primary contact' do
    it 'sends an email with primary contact info' do
      expect(worker).to receive(:send_email_notification)
        .with(company,
              company.dispatch_email,
              'Unknown')
      worker.perform(company.id)
    end
  end
end
