# frozen_string_literal: true

require 'rails_helper'

describe CheckSiteTimesWorker do
  it "updates the open status of sites and publish changed sites" do
    create(:site, always_open: false, within_hours: true)
    expect(Site::OpenChecker).to receive(:update_open_businesses!).and_call_original

    Sidekiq::Testing.inline! do
      CheckSiteTimesWorker.perform_async
    end
  end
end
