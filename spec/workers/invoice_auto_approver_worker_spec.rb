# frozen_string_literal: true

require "rails_helper"

describe InvoiceAutoApproverWorker do
  subject { described_class.new.perform(invoice.id) }

  let(:invoice) { create(:invoice) }

  it "works" do
    expect(InvoiceAutoApprover).to receive(:call).once.with(invoice: invoice)
    subject
  end
end
