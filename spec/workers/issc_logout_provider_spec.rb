# frozen_string_literal: true

require "rails_helper"

describe IsscLogoutProvider do
  include_context "issc provider setup"

  let(:provider) do
    described_class.new(connection_manager: manager)
  end

  it "allows provider logout" do
    issc.status = "LoggedIn"
    issc.save!

    provider.perform(issc.id)

    expect(manager.issc_client.called?(:logout)).to eq(true)

    logout_args = manager.issc_client.arguments(:logout)[0].length

    expect(logout_args).to eq(1)
  end
end
