# frozen_string_literal: true

require "rails_helper"

describe CreateOrUpdateEstimateForJob do
  let!(:job) { create(:job) }

  describe 'finds or creates an estimate for the job' do
    describe 'finds an estimate if one exists' do
      before(:each) do
        job.estimate = create(:estimate, job: job)
        job.save!
      end

      describe 'given' do
        it 'the job has an estimate' do
          expect(job.estimate).to be_instance_of(Estimate)
        end
      end

      it 'finds one' do
        pre_existing_job_estimate = job.estimate
        subject.perform(job.id)
        job.reload
        expect(job.estimate.id).to eql(pre_existing_job_estimate.id)
      end
    end

    describe 'creates an estimate if one doesn\t exist' do
      describe 'given' do
        it 'the job doesn\'t have an estimate' do
          expect(job.estimate).to be_nil
        end
      end

      it 'creates one' do
        subject.perform(job.id)
        job.reload
        expect(job.estimate).to be_instance_of(Estimate)
      end
    end
  end

  it 'runs the estimate' do
    job.estimate = create(:estimate, job: job)
    allow(Job).to receive(:find_by).with(id: job.id).and_return(job)
    allow(job.estimate).to receive(:run)
    subject.perform(job.id)
    expect(job.estimate).to have_received(:run).once
  end

  describe 'creates or updates an invoice if necessary' do
    describe 'if an estimate was created and the job belongs to an account' do
      before(:each) do
        job.account = create(:account)

        job.save!
      end

      describe 'given' do
        it 'the job doesn\'t have an estimate' do
          expect(job.estimate).to be_nil
        end

        it 'the job belongs to an account' do
          expect(job.account).to be_instance_of(Account)
        end
      end

      it 'calls CreateOrUpdateInvoice once' do
        expect(CreateOrUpdateInvoice).to receive(:perform_async).with(job.id).once

        subject.perform(job.id)
      end

      context 'and job.status is Draft' do
        let(:status) { 'Draft' }

        let(:job) do
          create(
            :rescue_job,
            service_code: service_code,
            status: status
          )
        end

        context 'and current_job.service_code is STORAGE' do
          let(:service_code) { create(:service_code, name: ServiceCode::STORAGE) }

          it 'does not call CreateOrUpdateInvoice' do
            expect(CreateOrUpdateInvoice).not_to receive(:perform_async)

            subject.perform(job.id)
          end
        end

        context 'and current_job.service_code is not STORAGE' do
          let(:service_code) { create(:service_code, name: ServiceCode::TOW) }

          it 'calls CreateOrUpdateInvoice once' do
            expect(CreateOrUpdateInvoice).to receive(:perform_async).with(job.id).once

            subject.perform(job.id)
          end
        end
      end
    end

    describe 'if an estimate was updated and the job doesn\'t have an invoice and can create one' do
      before(:each) do
        job.estimate = create(:estimate, job: job)
        job.site = create(:site)
        job.save!
      end

      describe 'given' do
        it 'doesnt have an invoice' do
          expect(job.invoice).to be_nil
        end
        it 'is allowed to create an invoice' do
          expect(job.rescue_company).to be_instance_of(RescueCompany)
          expect(job.site).to be_instance_of(Site)
        end
      end

      it 'calls CreateOrUpdateInvoice once' do
        allow(CreateOrUpdateInvoice).to receive(:perform_async).with(an_instance_of(Integer))
        subject.perform(job.id)
        expect(CreateOrUpdateInvoice).to have_received(:perform_async).with(job.id).once
      end
    end

    describe 'if an estimate was updated and the job\'s invoice is in a new state' do
      before(:each) do
        job.estimate = create(:estimate, job: job)
        job.invoice = create(:invoice, job: job)
        job.invoice.state = 'partner_new'
        job.save!
      end

      describe 'given' do
        it 'has an invoice' do
          expect(job.invoice).to be_instance_of(Invoice)
        end
      end

      it 'calls CreateOrUpdateInvoice once' do
        allow(CreateOrUpdateInvoice).to receive(:perform_async).with(an_instance_of(Integer))
        subject.perform(job.id)
        expect(CreateOrUpdateInvoice).to have_received(:perform_async).with(job.id).once
      end
    end

    describe 'doesn\'t if those 3 scenarios weren\'t met' do
      describe 'given' do
        it 'the job doesn\'t have an account' do
          expect(job.account).to be_nil
        end
        it 'doesn\'t call CreateOrUpdateInvoice once' do
          allow(CreateOrUpdateInvoice).to receive(:perform_async)
          subject.perform(job.id)
          expect(CreateOrUpdateInvoice).not_to have_received(:perform_async)
        end
      end
    end
  end
end
