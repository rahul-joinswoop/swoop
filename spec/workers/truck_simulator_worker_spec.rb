# frozen_string_literal: true

require 'rails_helper'

describe TruckSimulatorWorker do
  subject { TruckSimulatorWorker.new.perform(truck.id, directions_id) }

  let(:truck) { create :rescue_vehicle }
  let(:directions_id) { SecureRandom.uuid }
  let(:redis) { RedisClient[:default] }

  context 'with directions' do
    let(:coordinates) do
      [
        [Faker::Address.latitude, Faker::Address.longitude],
      ]
    end

    it 'works' do
      coordinates.each { |c| redis.rpush(directions_id, MultiJson.dump(c)) }
      expect(UpdateVehicleLocationWorker)
        .to receive(:perform_async)
        .once
        .with(truck.company_id,
              truck.id,
              # LOLWUT our directions were stored as json in redis and the float values
              # may have been truncated
              JSON.load(JSON.dump(coordinates[0][0])),
              JSON.load(JSON.dump(coordinates[0][1])),
              anything)

      expect(TruckSimulatorWorker)
        .to receive(:perform_in)
        .once
        .with(TruckSimulator::UPDATE_FREQUENCY_SECONDS, truck.id, directions_id)

      subject
    end
  end

  context 'with empty directions' do
    it 'works' do
      # create an empty list by pushing then popping- shouldn't have to do this
      # but ruby doesn't accept LPUSH/RPUSH without the second arg even though
      # redis does :/
      redis.rpush(directions_id, 123)
      redis.lpop(directions_id)

      expect(UpdateVehicleLocationWorker)
        .not_to receive(:perform_async)

      expect(TruckSimulatorWorker)
        .not_to receive(:perform_in)

      subject

      # our list should have been deleted
      expect(redis.exists(directions_id)).to be false
    end
  end

  context 'without directions' do
    it 'works' do
      # we shouldn't have a list
      expect(redis.exists(directions_id)).to be false

      expect(UpdateVehicleLocationWorker)
        .not_to receive(:perform_async)

      expect(TruckSimulatorWorker)
        .not_to receive(:perform_in)

      subject

      # we still shouldn't have a list
      expect(redis.exists(directions_id)).to be false
    end
  end
end
