# frozen_string_literal: true

require 'rails_helper'

describe SmsAlertWorker, freeze_time: Time.current.round do
  subject { described_class.new.perform }

  describe '#delete_or_cancel_alert_if_necessary' do
    subject { described_class.new.send(:delete_or_cancel_alert_if_necessary, alert) }

    let!(:alert) { create :confirm_partner_completed_sms_alert, job: job, send_at: 5.minutes.ago }

    context "with a valid job" do
      let(:job) { create :job, status: :dispatched }

      it { is_expected.to be(false) }
    end

    context "with a job_status not in VALID_FROM_STATES" do
      let(:job) { create :job, status: :pending }

      it "works" do
        expect { subject }.to change(alert, :canceled_at).to(Time.current)
        expect(subject).to be(true)
      end
    end

    context "with a deleted Job" do
      let(:job) { create :job, status: :dispatched, deleted_at: Time.current }

      it "works" do
        expect { subject }.to change(alert, :canceled_at).to(Time.current)
        expect(subject).to be(true)
      end
    end
  end

  describe '#process_confirm_partner_on_site_alert' do
    subject { described_class.new.send :process_confirm_partner_on_site_alert }

    let(:job) { create :job }
    let!(:toa) { create :time_of_arrival, job: job, time: 2.minutes.ago }
    let(:audit_sms) { create :audit_sms, job: job }
    let!(:alert) { create :confirm_partner_on_site_sms_alert, job: job }

    context "with a good alert" do
      it "works" do
        expect_any_instance_of(SmsAlertWorker)
          .to receive(:delete_or_cancel_alert_if_necessary)
          .once
          .with(alert)
          .and_return(true)

        subject

        expect { alert.reload }
          .not_to change { alert }
      end
    end

    context "with a bad alert" do
      it "works" do
        expect_any_instance_of(SmsAlertWorker)
          .to receive(:delete_or_cancel_alert_if_necessary)
          .once
          .with(alert)
          .and_return(false)

        expect(Sms::ConfirmPartnerOnSite)
          .to receive(:send_notification)
          .once
          .with(job)
          .and_return(audit_sms)

        subject

        expect { alert.reload }
          .to change(alert, :audit_sms).to(audit_sms)
          .and change(alert, :sent_at).to(Time.current)
      end
    end
  end

  describe '#process_confirm_partner_on_site_follow_up_alert' do
    subject { described_class.new.send :process_confirm_partner_on_site_follow_up_alert }

    let(:job) { create :job }
    let(:audit_sms) { create :audit_sms, job: job, created_at: 6.minutes.ago, response: nil, followed_up_at: nil }
    let!(:alert) { create :confirm_partner_on_site_sms_alert, audit_sms: audit_sms, job: job, sent_at: 5.minutes.ago }

    context "with a good alert" do
      it "works" do
        expect_any_instance_of(SmsAlertWorker)
          .to receive(:delete_or_cancel_alert_if_necessary)
          .once
          .with(alert)
          .and_return(true)

        subject

        expect { audit_sms.reload }
          .not_to change { alert }
      end
    end

    context "with a bad alert" do
      it "works" do
        expect_any_instance_of(SmsAlertWorker)
          .to receive(:delete_or_cancel_alert_if_necessary)
          .once
          .with(alert)
          .and_return(false)

        expect(Sms::ConfirmPartnerOnSiteFollowUp)
          .to receive(:send_notification)
          .once
          .with(job)

        subject

        expect { audit_sms.reload }
          .to change(audit_sms, :followed_up_at).to(Time.current)
      end
    end
  end

  describe '#process_confirm_partner_completed_alert' do
    subject { described_class.new.send :process_confirm_partner_completed_alert }

    let(:job) { create :job }
    let!(:alert) { create :confirm_partner_completed_sms_alert, job: job, send_at: 5.minutes.ago }
    let(:audit_sms) { create :audit_sms, job: job }

    context "with a good alert" do
      it "works" do
        expect_any_instance_of(SmsAlertWorker)
          .to receive(:delete_or_cancel_alert_if_necessary)
          .with(alert)
          .and_return(true)

        subject

        expect { alert.reload }
          .not_to change { alert }
      end
    end

    context "with a bad alert" do
      it "works" do
        expect_any_instance_of(SmsAlertWorker)
          .to receive(:delete_or_cancel_alert_if_necessary)
          .with(alert)
          .and_return(false)

        expect(Sms::ConfirmPartnerCompleted)
          .to receive(:send_notification)
          .with(job)
          .and_return(audit_sms)

        subject

        expect { alert.reload }
          .to change(alert, :audit_sms).to(audit_sms)
          .and change(alert, :sent_at).to(Time.current)
      end
    end
  end
end
