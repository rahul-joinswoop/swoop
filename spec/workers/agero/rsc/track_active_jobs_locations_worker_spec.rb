# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::TrackActiveJobsLocationsWorker do
  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company, :ago) }
  let(:api_access_token) { create(:api_access_token) }

  let(:rsc_issc) { create(:ago_issc, :with_api_access_token, api_access_token: api_access_token, system: "rsc", company: rescue_company) }
  let(:gco_issc) { create(:gco_issc, system: "issc", company: rescue_company) }

  let(:dispatch_1) { create(:issc_dispatch_request, issc: rsc_issc, jobid: "j1", dispatchid: "d1") }
  let(:dispatch_2) { create(:issc_dispatch_request, issc: rsc_issc, jobid: "j2", dispatchid: "d2") }
  let(:dispatch_3) { create(:issc_dispatch_request, issc: gco_issc, jobid: "j3", dispatchid: "d3") }
  let(:dispatch_4) { create(:issc_dispatch_request, issc: gco_issc, jobid: "j4", dispatchid: "d4") }
  let(:dispatch_5) { create(:issc_dispatch_request, issc: rsc_issc, jobid: "j5", dispatchid: "d5") }

  let(:driver) { create(:driver_on_duty) }
  let(:vehicle) { create(:rescue_vehicle, lat: 45.0, lng: -118.0, location_updated_at: Time.now) }
  let(:not_moving_vehicle) { create(:rescue_vehicle, lat: 45.0, lng: -118.0, location_updated_at: 10.minutes.ago) }

  let!(:job_1) { create(:fleet_motor_club_job, status: "Towing", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, rescue_driver: driver, issc_dispatch_request: dispatch_1) }
  let!(:job_2) { create(:fleet_motor_club_job, status: "En Route", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: not_moving_vehicle, rescue_driver: driver, issc_dispatch_request: dispatch_2) }
  let!(:job_3) { create(:fleet_motor_club_job, status: "Towing", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, rescue_driver: driver, issc_dispatch_request: dispatch_3) }
  let!(:job_4) { create(:fleet_motor_club_job, status: "En Route", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, rescue_driver: driver, issc_dispatch_request: dispatch_4) }
  let!(:job_5) { create(:fleet_motor_club_job, status: "Completed", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, rescue_driver: driver, issc_dispatch_request: dispatch_5) }

  it "finds active RSC jobs with rescue vehicle locations in the Towing or En Route status" do
    Agero::Rsc::TrackVehicleLocationWorker.jobs.clear
    Agero::Rsc::TrackActiveJobsLocationsWorker.new.perform
    expect(Agero::Rsc::TrackVehicleLocationWorker.jobs.size).to eq(2)
    job_args = Agero::Rsc::TrackVehicleLocationWorker.jobs.map { |j| j["args"] }
    expect(job_args).to include [job_1.id, job_1.rescue_driver_id, vehicle.lat, vehicle.lng, vehicle.location_updated_at.iso8601]
    expect(job_args).to include [job_2.id, job_2.rescue_driver_id, not_moving_vehicle.lat, not_moving_vehicle.lng, not_moving_vehicle.location_updated_at.iso8601]
  end

  it "does not enqueue jobs for clients with invalid access tokens" do
    api_access_token.update!(deleted_at: Time.now)
    Agero::Rsc::TrackVehicleLocationWorker.jobs.clear
    Agero::Rsc::TrackActiveJobsLocationsWorker.new.perform
    expect(Agero::Rsc::TrackVehicleLocationWorker.jobs.size).to eq(0)
  end
end
