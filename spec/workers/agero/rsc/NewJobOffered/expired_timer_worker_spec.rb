# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::NewJobOffered::ExpiredTimerWorker do
  let(:job) do
    create :fleet_motor_club_job, {
      status: Job::ASSIGNED,
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
      account: account,
      answer_by: 1.second.ago,
    }
  end

  let!(:issc_dispatch_request) do
    create(
      :issc_dispatch_request,
      dispatchid: "1234",
      issc: issc,
      status: "new_request",
      max_eta: 90.minutes,
      job: job
    )
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company, :ago) }
  let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM) }
  let(:account) { create(:account, client_company: fleet_company, company: rescue_company) }

  around :each do |example|
    ClimateControl.modify(ISSC_AUTORESPONSE: "true") do
      example.run
    end
  end

  it "expires a job" do
    Agero::Rsc::NewJobOffered::ExpiredTimerWorker.new.perform(job.id)
    job.reload
    expect(job.expired?).to eq true
    expect(Agero::Rsc::RejectDispatchWorker.jobs).to be_empty
    expect(Agero::Rsc::NewJobOffered::ExpiredTimerWorker.jobs).to be_empty
  end

  it "reschedules itself if the job approve_by is in the future", freeze_time: true do
    time = 1.minute.from_now
    job.update!(answer_by: time)
    Agero::Rsc::NewJobOffered::ExpiredTimerWorker.new.perform(job.id)
    job.reload
    expect(job.expired?).to eq false
    expect(Agero::Rsc::RejectDispatchWorker.jobs).to be_empty
    expect(Agero::Rsc::NewJobOffered::ExpiredTimerWorker).to have_enqueued_sidekiq_job(job.id).at(time)
  end

  it "rejects a job if the company feature flag is enabled" do
    rescue_company.features << create(:feature, name: Feature::MC_JOB_AUTO_REJECT_OVERRIDE)
    Agero::Rsc::NewJobOffered::ExpiredTimerWorker.new.perform(job.id)
    expect(Agero::Rsc::RejectDispatchWorker).to have_enqueued_sidekiq_job(job.id)
  end

  it "accepts a job if the company is set to auto accept", freeze_time: true do
    account.update!(auto_eta: 20, auto_accept: true)
    expect(Agero::Rsc::AcceptDispatch).to receive(:call).with(job: job)
    Agero::Rsc::NewJobOffered::ExpiredTimerWorker.new.perform(job.id)
    job.reload
    expect(job.submitted?).to eq true
    expect(job.bta.time).to eq 20.minutes.from_now
    expect(Agero::Rsc::RejectDispatchWorker.jobs).to be_empty
  end

  it "does nothing if the job is not in the assigned state" do
    job.update_column(:status, Job::TOWING)
    Agero::Rsc::NewJobOffered::ExpiredTimerWorker.new.perform(job.id)
    job.reload
    expect(job.towing?).to eq true
  end

  it "does not auto accept a job if the feature is globally disabled" do
    ClimateControl.modify ISSC_AUTORESPONSE: nil do
    end
  end
end
