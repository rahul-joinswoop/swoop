# frozen_string_literal: true

require "rsc_helper"

describe Agero::Rsc::PullLocationsWorker do
  subject(:worker_perform) do
    described_class.new.perform(api_access_token.id)
  end

  before do
    stub_const "VENDORID1", "vendor-1"
    allow_any_instance_of(Agero::Rsc::API).to receive(:facility_list).and_return(payload)
    allow_any_instance_of(Agero::Rsc::API).to receive(:sign_in)
  end

  let!(:agero) { create(:fleet_company, :agero) }
  let!(:rescue_company) { create(:rescue_company) }
  let!(:account) { create(:account, client_company: agero, company: rescue_company) }
  let!(:api_access_token) { create(:api_access_token, vendorid: VENDORID1, company: rescue_company) }

  let(:payload) { JSON.load(rsc_api_response("facility_list")) }

  #   {
  #     "totalCount": 2,
  #     "offset": 0,
  #     "limit": 1,
  #     "results": [
  #       {
  #         "id": "1",
  #         "name": "Oakland Beer Garden",
  #         "address": "2040 Telegraph Ave",
  #         "city": "Oakland",
  #         "state": "CA",
  #         "zipcode": "94612",
  #         "lat": "37.810100",
  #         "lon": "-122.269155"
  #       },
  #       {
  #         "id": "2",
  #         "name": "Douglas Transport-VIMS",
  #         "address": "100 Main Street",
  #         "city": "Malden",
  #         "state": "MA",
  #         "zipcode": "02155",
  #         "lat": "42.0123",
  #         "lon": "80.1153"
  #       }
  #     ]
  #   }

  context "when API shows no facilities" do
    let(:payload) { {} }

    it "does not throw an error" do
      expect { worker_perform }.not_to raise_error
    end
  end

  context "when rescue company lacks agero as an account" do
    let(:account) { nil }

    it "adds agero as an account" do
      expect { worker_perform }.to change(
        rescue_company.accounts, :count
      ).by(1)
    end

    it "correctly assigns Agero as the name" do
      worker_perform

      agero_account = rescue_company.accounts.last

      expect(agero_account.name).to eq("Agero")
    end
  end

  context 'rsc setup already' do
    let!(:issc_location) do
      create(:issc_location, {
        locationid: "1",
        company: rescue_company,
        fleet_company: agero,
        site: nil,
      })
    end

    let!(:issc) do
      create(:ago_issc,
             site: nil,
             contractorid: VENDORID1,
             issc_location: issc_location,
             company: rescue_company,
             system: "rsc")
    end

    context "when Issc record exists with existing site" do
      let!(:site_location) { create(:location, :oakland_beer_garden, site: site) }
      let!(:site) { create(:site, company: rescue_company) }

      it "does not update issc site" do
        expect(issc.site).not_to eq(site)

        worker_perform

        issc_location.reload

        expect(issc.site).not_to eq(site)
      end
    end

    context "when Issc object exists with nil site" do
      let!(:site_location) { create(:location, :oakland_beer_garden, site: site) }
      let!(:site) { create(:site, company: rescue_company) }

      # Two-way binding, required
      before do
        site.location = site_location
        site.save!
      end

      it "updates issc record" do
        expect(issc.site).not_to eq(site)

        worker_perform

        issc.reload

        expect(issc.site).to eq(site)
      end

      context 'site is already bound to another issc' do
        let!(:issc_location2) do
          create(:issc_location, {
            locationid: "2",
            company: rescue_company,
            fleet_company: agero,
            site: site,
          })
        end
        let!(:issc2) do
          create(:ago_issc,
                 site: site,
                 contractorid: VENDORID1,
                 issc_location: issc_location2,
                 company: rescue_company,
                 system: "rsc")
        end

        it 'updates the site' do
          worker_perform
          issc.reload
          expect(issc.site).to eq site
        end
      end
    end
  end

  context "when no Issc objects exist" do
    it "creates corresponding Issc records" do
      expect { worker_perform }.to change(Issc, :count).by(2)
    end

    it "creates corresponding swoop Location records" do
      expect { worker_perform }.to change(Location, :count).by(2)
    end

    it "creates corresponding IsscLocation records" do
      expect { worker_perform }.to change(IsscLocation, :count).by(2)
    end

    it "attempts to automatically match the new records" do
      expect(Agero::Rsc::MatchLocationToSite).to receive(:call)
        .twice.and_call_original

      worker_perform
    end
  end

  context 'when multiple companies have same vendorid' do
    let!(:rescue_company2) { create(:rescue_company) }
    let!(:issc_location2) do
      create(:issc_location, {
        locationid: "1",
        company: rescue_company2,
        fleet_company: agero,
        site: nil,
      })
    end
    let!(:issc) do
      create(:ago_issc,
             site: nil,
             contractorid: VENDORID1,
             issc_location: issc_location2,
             company: rescue_company2,
             system: "rsc")
    end

    it 'creates new issc' do
      expect { worker_perform }.to change(Issc, :count).by(2)
    end
  end

  context '3 facilities' do
    let(:payload) { JSON.load(rsc_api_response("3_facility_list")) }

    let!(:site_location) { create(:location, site: site, zip: '77777') }
    let!(:site) { create(:site, company: rescue_company) }

    let!(:site_location2) { create(:location, site: site2, zip: '02886') }
    let!(:site2) { create(:site, company: rescue_company) }

    before do
      site.location = site_location
      site.save!
      site2.location = site_location2
      site.save!
    end

    it 'creates location3 properly' do
      worker_perform
      loc_list = IsscLocation.where(locationid: 3)
      expect(loc_list.size).to eq(1)
      loc = loc_list.first
      expect(loc.company).to eq(rescue_company)
    end

    it 'creates location2 properly' do
      worker_perform
      loc_list = IsscLocation.where(locationid: 2)
      expect(loc_list.size).to eq(1)
      loc = loc_list.first
      expect(loc.company).to eq(rescue_company)
    end

    it 'creates location1 properly' do
      worker_perform
      locationid1_list = IsscLocation.where(locationid: 1)
      expect(locationid1_list.size).to eq(1)
      loc1 = locationid1_list.first
      expect(loc1.company).to eq(rescue_company)
    end

    it 'creates issc 3 properly' do
      worker_perform
      issc_list = Issc.joins(:issc_location).where(
        company: rescue_company,
        contractorid: VENDORID1,
        clientid: 'AGO',
        issc_locations: {
          locationid: 3,
        }
      )
      expect(issc_list.size).to eq(1)
      issc = issc_list.first
      expect(issc.status).to eq('registered')
    end

    it "creates 3 locations" do
      expect { worker_perform }.to change(IsscLocation, :count).from(0).to(3)
    end

    it "creates 3 isscs" do
      expect { worker_perform }.to change(Issc, :count).from(0).to(3)
    end
  end
end
