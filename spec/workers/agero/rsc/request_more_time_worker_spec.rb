# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::RequestMoreTimeWorker, freeze_time: true do
  include_context "basic rsc setup"

  subject(:worker_perform) do
    Agero::Rsc::RequestMoreTimeWorker.new.perform(job_id, async_request.id)
  end

  let(:job) do
    create :fleet_motor_club_job, {
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
      answer_by: Time.now,
    }
  end

  let(:job_id) { job.id }

  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
  end

  let(:vendor_id) { '1234' }
  let(:facility_id) { '10' }

  let(:async_request) do
    create(
      :api_async_request,
      company_id: rescue_company.id,
      user_id: user.id,
      target_id: job.id,
      system: API::AsyncRequest::RSC_REQUEST_MORE_TIME
    )
  end

  let(:user) { create(:user, company: rescue_company) }

  context 'when Agero accepts more time' do
    before do
      stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/requestExtension")
        .to_return(status: 200, body: "", headers: {})
    end

    it 'sets job.issc_dispatch_request.more_time_requested = true' do
      worker_perform
      job.issc_dispatch_request.reload
      expect(job.issc_dispatch_request.more_time_requested).to be_truthy
    end

    it 'updates job.answer_by by 1 minute' do
      worker_perform
      job.reload
      expect(job.answer_by).to eq(1.minute.from_now)
    end

    it 'updates async_request.external_response_body' do
      worker_perform
      async_request.reload
      expect(async_request.external_response_body).to eq 'More Time Request Accepted by RSC'
    end

    it 'pushes WS message with AsyncRequest' do
      expect(Publishers::AsyncRequestWebsocketMessage).to receive(:call).with(
        {
          input: {
            charge: {
              status: 'success',
            },
          },
          id: async_request.id,
        }
      )

      worker_perform
    end
  end

  context 'when Agero API returns 403' do
    before do
      stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/requestExtension")
        .to_return(status: 403, body: "", headers: {})
    end

    it 'sets job.issc_dispatch_request.more_time_requested = true' do
      worker_perform

      job.issc_dispatch_request.reload

      expect(job.issc_dispatch_request.more_time_requested).to be_truthy
    end

    it 'updates async_request.external_response_body with the error' do
      worker_perform

      async_request.reload

      expect(async_request.external_response_body).to eq 'More Time Request Rejected by Agero'
    end

    it 'pushes WS message with error' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        {
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                request_more_time: ['More Time Request Rejected by Agero'],
              },
            },
            company: rescue_company,
          },
        }
      )

      worker_perform
    end
  end

  # @see 5.6 https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
  # 400 - Invalid dispatch request - will likely not happen, but may be an error on Agero side
  #       only, so we should be able to retry it
  context 'when Agero API returns error 400' do
    before do
      stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/requestExtension")
        .to_return(status: 400, body: "", headers: {})
    end

    it 'does not set job.issc_dispatch_request.more_time_requested = true' do
      worker_perform

      job.issc_dispatch_request.reload

      expect(job.issc_dispatch_request.more_time_requested).to be_falsey
    end

    it 'updates async_request.external_response_body with the error' do
      worker_perform

      async_request.reload

      expect(async_request.external_response_body).to eq 'More Time Request Error - 400 Bad Request'
    end

    it 'pushes WS message with error' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        {
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                request_more_time: ['More time request failed, please try again'],
              },
            },
            company: rescue_company,
          },
        }
      )

      worker_perform
    end
  end

  # @see 5.6 https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
  # 401 - A valid OAuth token was not provided - it may be an error on Agero side only,
  #       so we should be able to retry it
  context 'when Agero API returns error 401' do
    before do
      stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/requestExtension")
        .to_return(status: 401, body: "", headers: {})
    end

    it 'does not set job.issc_dispatch_request.more_time_requested = true' do
      worker_perform

      job.issc_dispatch_request.reload

      expect(job.issc_dispatch_request.more_time_requested).to be_falsey
    end

    it 'updates async_request.external_response_body with the error' do
      worker_perform

      async_request.reload

      expect(async_request.external_response_body).to eq 'More Time Request Error - 401 Unauthorized'
    end

    it 'pushes WS message with error' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        {
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                request_more_time: ['More time request failed, please try again'],
              },
            },
            company: rescue_company,
          },
        }
      )

      worker_perform
    end
  end

  # @see 5.6 https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
  # 404 - Job Not Found - will likely not happen, but may be an error on Agero side only
  #       so we should be able to retry it (don't know how it differs from error 400 ^)
  context 'when Agero API returns error 404' do
    before do
      stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/requestExtension")
        .to_return(status: 404, body: "", headers: {})
    end

    it 'does not set job.issc_dispatch_request.more_time_requested = true' do
      worker_perform

      job.issc_dispatch_request.reload

      expect(job.issc_dispatch_request.more_time_requested).to be_falsey
    end

    it 'updates async_request.external_response_body with the error' do
      worker_perform

      async_request.reload

      expect(async_request.external_response_body).to eq 'More Time Request Error - 404 Not Found'
    end

    it 'pushes WS message with error' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        {
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                request_more_time: ['More time request failed, please try again'],
              },
            },
            company: rescue_company,
          },
        }
      )

      worker_perform
    end
  end

  # @see 5.6 https://apiportal.agero.com/sites/default/files/digitaldispatchapiforpartnerslive.html
  # 500 - Unknown Error - unknown, we should be able to retry it
  context 'when Agero API returns error 500' do
    before do
      stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/requestExtension")
        .to_return(status: 500, body: "", headers: {})
    end

    it 'does not set job.issc_dispatch_request.more_time_requested = true' do
      worker_perform

      job.issc_dispatch_request.reload

      expect(job.issc_dispatch_request.more_time_requested).to be_falsey
    end

    it 'updates async_request.external_response_body with the error' do
      worker_perform

      async_request.reload

      expect(async_request.external_response_body).to eq(
        'More Time Request Error - 500 Internal Server Error'
      )
    end

    it 'pushes WS message with error' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        {
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                request_more_time: ['More time request failed, please try again'],
              },
            },
            company: rescue_company,
          },
        }
      )

      worker_perform
    end
  end
end
