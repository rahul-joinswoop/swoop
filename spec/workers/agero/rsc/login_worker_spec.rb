# frozen_string_literal: true

require "rsc_helper"

describe Agero::Rsc::LoginWorker do
  subject { Agero::Rsc::LoginWorker.new }

  let(:contractorid) { "32498734581987" }

  let!(:rescue_company) { create(:rescue_company) }

  let(:issc1) do
    create(:ago_issc, {
      contractorid: contractorid,
      status: Issc::REGISTERED,
      company: rescue_company,
      site: issc1_site,
    })
  end

  let(:issc1_site) { create(:site, within_hours: true, dispatchable: true) }

  context 'RSC_SYSTEM' do
    before do
      issc1.system = Issc::RSC_SYSTEM
      issc1.save!
    end

    it 'finds the issc' do
      expect_any_instance_of(Agero::Rsc::SignInVendorIds).to receive(:perform).with(contractorid => [issc1])
      subject.perform
    end
  end

  context 'ISSC_SYSTEM' do
    before do
      issc1.system = Issc::ISSC_SYSTEM
      issc1.save!
    end

    it 'does not find the issc' do
      expect_any_instance_of(Agero::Rsc::SignInVendorIds).not_to receive(:perform)
      subject.perform
    end
  end
end
