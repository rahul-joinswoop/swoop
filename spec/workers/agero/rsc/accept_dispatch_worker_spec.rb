# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::AcceptDispatchWorker do
  include_context "basic rsc setup"

  subject(:worker_perform) { Agero::Rsc::AcceptDispatchWorker.new.perform(job_id) }

  let(:job) do
    create :fleet_motor_club_job, {
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
      updated_at: '2018-07-26 18:08:04',
      bta: bta,
      eta_explanation_id: eta_explanation_id,
    }
  end

  let(:bta) do
    create(
      :time_of_arrival,
      time: bta_time,
      created_at: time_now,
      updated_at: time_now
    )
  end

  let(:time_now) { Time.now }

  let(:job_id) { job.id }
  let(:vendor_id) { '1234' }
  let(:facility_id) { '10' }

  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
  end

  before do
    stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/acceptDispatchRequest")
      .to_return(status: 200, body: "", headers: {})
  end

  # MAX ETA accepted by Agero w/o explanation is 90.minutes
  context 'when ETA < MAX ETA allowed of 90 minutes' do
    let(:bta_time) { time_now + 89.minutes }
    let(:eta_explanation_id) { nil }

    it 'calls rsc_api#accept_dispatch with the expected params' do
      expect_any_instance_of(Agero::Rsc::API).to receive(:accept_dispatch).with(
        job.issc_dispatch_request.dispatchid, eta: job.bta.duration, reason: nil
      )

      worker_perform
    end

    it 'calls rsc_client#post! with the expected params' do
      expect_any_instance_of(Agero::Rsc::Client).to receive(:post!).with(
        "/dispatches/#{job.issc_dispatch_request.dispatchid}/acceptDispatchRequest", {
          params: {
            eta: 89,
          },
        }
      )

      worker_perform
    end

    context 'but Agero returns error' do
      before do
        stub_request(
          :post,
          "https://agero.example.com/v1/dispatches/123123123/acceptDispatchRequest"
        ).to_return(status: status_error, body: "", headers: {})
      end

      context 'and error is 403' do
        let(:status_error) { 403 }

        it 'marks the job as expired' do
          worker_perform
          job.reload
          expect(job.status).to eq "expired"
        end
      end

      context 'and error is not 403' do
        let(:status_error) { 401 }

        it 'raises error' do
          expect { worker_perform }.to raise_error(Agero::Rsc::Error)
        end
      end
    end
  end

  # MAX ETA accepted by Agero w/o explanation is 90.minutes
  context 'when ETA > MAX ETA allowed of 90 minutes' do
    let(:bta_time) { time_now + 91.minutes }

    context 'and job_eta_explanation is None' do
      let(:eta_explanation_id) { create(:job_eta_explanation, text: 'None').id }

      it 'calls rsc_api#accept_dispatch with the expected explanation reason' do
        expect_any_instance_of(Agero::Rsc::API).to receive(:accept_dispatch).with(
          job.issc_dispatch_request.dispatchid, eta: job.bta.duration, reason: :other
        )

        worker_perform
      end

      it 'calls rsc_client#post! with the expected params' do
        expect_any_instance_of(Agero::Rsc::Client).to receive(:post!).with(
          "/dispatches/#{job.issc_dispatch_request.dispatchid}/acceptDispatchRequest", {
            params: {
              eta: 91,
              reasonCode: 0,
              reason: "None",
            },
          }
        )

        worker_perform
      end
    end

    context 'and job_eta_explanation is Traffic' do
      let(:eta_explanation_id) { create(:job_eta_explanation, text: 'Traffic').id }

      it 'calls rsc_api#accept_dispatch with the expected explanation reason' do
        expect_any_instance_of(Agero::Rsc::API).to receive(:accept_dispatch).with(
          job.issc_dispatch_request.dispatchid, eta: job.bta.duration, reason: :traffic
        )

        worker_perform
      end

      it 'calls rsc_client#post! with the expected params' do
        expect_any_instance_of(Agero::Rsc::Client).to receive(:post!).with(
          "/dispatches/#{job.issc_dispatch_request.dispatchid}/acceptDispatchRequest", {
            params: {
              eta: 91,
              reasonCode: 403,
              reason: "Extreme traffic in the area",
            },
          }
        )

        worker_perform
      end
    end

    context 'and job_eta_explanation is Weather' do
      let(:eta_explanation_id) { create(:job_eta_explanation, text: 'Weather').id }

      it 'calls rsc_api#accept_dispatch with the expected explanation reason' do
        expect_any_instance_of(Agero::Rsc::API).to receive(:accept_dispatch).with(
          job.issc_dispatch_request.dispatchid, eta: job.bta.duration, reason: :weather
        )

        worker_perform
      end

      it 'calls rsc_client#post! with the expected params' do
        expect_any_instance_of(Agero::Rsc::Client).to receive(:post!).with(
          "/dispatches/#{job.issc_dispatch_request.dispatchid}/acceptDispatchRequest", {
            params: {
              eta: 91,
              reasonCode: 401,
              reason: "Weather Emergency",
            },
          }
        )

        worker_perform
      end
    end

    context 'and job_eta_explanation does not match an Agero one' do
      let(:eta_explanation_id) { create(:job_eta_explanation, text: 'An unexpected new one').id }

      it 'calls rsc_api#accept_dispatch with the expected explanation reason' do
        expect_any_instance_of(Agero::Rsc::API).to receive(:accept_dispatch).with(
          job.issc_dispatch_request.dispatchid, eta: job.bta.duration, reason: :other
        )

        worker_perform
      end

      it 'calls rsc_client#post! with the expected params' do
        expect_any_instance_of(Agero::Rsc::Client).to receive(:post!).with(
          "/dispatches/#{job.issc_dispatch_request.dispatchid}/acceptDispatchRequest", {
            params: {
              eta: 91,
              reasonCode: 0,
              reason: "None",
            },
          }
        )

        worker_perform
      end
    end
  end

  context 'when job.bta is nil' do
    let(:bta) { nil }
    let(:eta_explanation_id) { nil }

    it 'raises error' do
      expect { worker_perform }.to raise_error(ArgumentError)
    end
  end
end
