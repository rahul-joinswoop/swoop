# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::AssignDriverWorker do
  let(:job) do
    create :fleet_motor_club_job, {
      status: "Dispatched",
      fleet_company: fleet_company,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
      rescue_driver: rescue_driver,
      rescue_vehicle: rescue_vehicle,
    }
  end

  let(:api_access_token) { create(:api_access_token) }
  let(:issc) { create(:ago_issc, company: rescue_company, api_access_token: api_access_token) }
  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company, :ago) }
  let(:rescue_vehicle) { create(:rescue_vehicle) }

  context "with a driver" do
    let(:rescue_driver) { create(:driver_on_duty) }

    it "posts to assign external driver in RSC" do
      expect_any_instance_of(Agero::Rsc::API).to receive(:assign_external_driver).with(
        job.issc_dispatch_request.dispatchid,
        driver: job.rescue_driver,
        vehicle: job.rescue_vehicle,
        site: job.site,
      )
      Agero::Rsc::AssignDriverWorker.new.perform(job.id)
    end

    it "handle forbidden errors" do
      expect_any_instance_of(Agero::Rsc::API).to receive(:assign_external_driver).and_raise(Agero::Rsc::ForbiddenError)
      expect { Agero::Rsc::AssignDriverWorker.new.perform(job.id) }.not_to raise_error
    end
  end

  context "without a driver" do
    let(:rescue_driver) { nil }

    it "does not post to assign external driver in RSC" do
      expect_any_instance_of(Agero::Rsc::API).not_to receive(:assign_external_driver)
      Agero::Rsc::AssignDriverWorker.new.perform(job.id)
    end
  end
end
