# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::RequestCallBackWorker do
  include_context "basic rsc setup"

  subject(:worker_perform) do
    Agero::Rsc::RequestCallBackWorker.new.perform(job_id, call_back_hash)
  end

  let(:job) do
    create :fleet_motor_club_job, {
      fleet_company: agero_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
      updated_at: '2018-07-26 18:08:04',
      partner_dispatcher: partner_dispatcher,
      site: site,
    }
  end

  let(:job_id) { job.id }
  let(:partner_dispatcher) { create(:user, company: rescue_company) }
  let(:vendor_id) { '776655' }
  let(:facility_id) { 1 }
  let(:site) { create(:partner_site, phone: '+14051234567') }

  let(:issc_dispatch_request) do
    create(
      :issc_dispatch_request,
      issc: issc,
      jobid: "ISSC_JOB_123",
      max_eta: 90.minutes,
      dispatchid: '123456',
    )
  end

  let(:call_back_hash) do
    {
      dispatch_number: '+15556668888',
      secondary_number: '+14050060010',
      name: job.partner_dispatcher.full_name,
    }
  end

  it 'calls Rsc API with expected params' do
    expect_any_instance_of(Agero::Rsc::API).to receive(:request_phone_call).with(
      '123456',
      callback_phone_number: '5556668888',
      name: job.partner_dispatcher.full_name,
      alternate_phone: '4050060010'
    )

    worker_perform
  end

  it 'creates associated history item' do
    expect_any_instance_of(Agero::Rsc::API).to receive(:request_phone_call).with(
      '123456',
      callback_phone_number: '5556668888',
      name: job.partner_dispatcher.full_name,
      alternate_phone: '4050060010'
    )

    expect_any_instance_of(Job).to receive(:base_publish).with("update").and_call_original

    expect { worker_perform }.to change(HistoryItem, :count).by(1)
  end

  context 'when call_back_hash[:callback_phone_number] is blank' do # currently covers mobile
    let(:call_back_hash) do
      {
        dispatch_number: nil,
        secondary_number: nil,
        name: job.partner_dispatcher.full_name,
      }
    end

    it 'fallbacks to us job.site.phone (without +1)' do
      expect_any_instance_of(Agero::Rsc::API).to receive(:request_phone_call).with(
        '123456',
        callback_phone_number: '4051234567',
        name: job.partner_dispatcher.full_name,
        alternate_phone: nil
      )

      worker_perform
    end
  end

  context 'when Agero::Rsc::API throws Agero::Rsc::ForbiddenError' do
    before do
      allow_any_instance_of(Agero::Rsc::API).to receive(:request_phone_call)
        .and_raise(Agero::Rsc::ForbiddenError)
    end

    it 'doesnt raise the error' do
      expect { worker_perform }.not_to raise_error
    end
  end
end
