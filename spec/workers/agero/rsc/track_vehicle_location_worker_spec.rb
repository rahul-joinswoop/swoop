# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::TrackVehicleLocationWorker do
  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", dispatchid: "1234", max_eta: 90.minutes)
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company, :ago) }
  let(:api_access_token) { create(:api_access_token) }

  let(:issc) { create(:ago_issc, :with_api_access_token, system: "rsc", api_access_token: api_access_token, company: rescue_company) }

  let(:job) do
    create(
      :fleet_motor_club_job,
      status: "Towing",
      fleet_company: fleet_company,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
    )
  end

  let(:status) { Agero::Rsc::Status.new(job).to_hash }

  it "calls the RSC API to update the vehicle location with the specified status" do
    mock_api = double(:api)
    expect(Agero::Rsc::API).to receive(:new).with(api_access_token.access_token).and_return(mock_api)
    time = Time.at(Time.now.to_i)
    expect(mock_api).to receive(:update_driver_locations).with(driver_id: 123, latitude: 45.1, longitude: -110.3, timestamp: time)
    Agero::Rsc::TrackVehicleLocationWorker.new.perform(job.id, 123, 45.1, -110.3, time.iso8601)
  end
end
