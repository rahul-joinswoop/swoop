# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::ChangeDispatchStatusWorker do
  include_context "basic rsc setup"

  subject(:worker_perform) do
    Agero::Rsc::ChangeDispatchStatusWorker.new.perform(job.id, rsc_status)
  end

  let(:job) do
    create :fleet_motor_club_job, {
      status: "On Site",
      fleet_company: fleet_company,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
    }
  end

  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
  end

  let(:vendor_id) { '1234' }
  let(:facility_id) { '10' }

  let(:rsc_status) { Agero::Rsc::Status.new(job).to_hash }

  describe "schedule_for" do
    it "schedules a job" do
      Agero::Rsc::ChangeDispatchStatusWorker.jobs.clear
      Agero::Rsc::ChangeDispatchStatusWorker.schedule_for(job, JobStatus::TOWING)
      expect(Agero::Rsc::ChangeDispatchStatusWorker.jobs.size).to eq 1
      expect(Agero::Rsc::ChangeDispatchStatusWorker.jobs.first["args"]).to eq [
        job.id,
        Agero::Rsc::Status.new(job, code: :tow_in_progress).to_hash,
      ]
    end

    it "does not schedule a job if the RSC status is not :assigned" do
      Agero::Rsc::ChangeDispatchStatusWorker.jobs.clear

      Agero::Rsc::ChangeDispatchStatusWorker.schedule_for(job, JobStatus::ASSIGNED)
      expect(Agero::Rsc::ChangeDispatchStatusWorker.jobs.size).to eq 0

      Agero::Rsc::ChangeDispatchStatusWorker.schedule_for(job, JobStatus::DISPATCHED)
      expect(Agero::Rsc::ChangeDispatchStatusWorker.jobs.size).to eq 0
    end
  end

  describe "perform" do
    it "posts the status to the RSC API" do
      rsc_status = Agero::Rsc::Status.new(job).to_hash
      expect_any_instance_of(Agero::Rsc::API).to receive(:change_status).with(
        job.issc_dispatch_request.dispatchid, status: rsc_status
      )
      Agero::Rsc::ChangeDispatchStatusWorker.new.perform(job.id, rsc_status)
    end
  end

  context 'when Agero returns error' do
    before do
      stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/status")
        .to_return(status: status_error, body: "", headers: {})
    end

    context 'and error is 403' do
      let(:status_error) { 403 }

      it 'does not raise error' do
        expect { worker_perform }.not_to raise_error
      end
    end

    context 'and error is 400' do
      let(:status_error) { 400 }

      it 'raises error' do
        expect { worker_perform }.to raise_error(Agero::Rsc::Error)
      end
    end

    context 'and error is 401' do
      let(:status_error) { 401 }

      it 'raises error' do
        expect { worker_perform }.to raise_error(Agero::Rsc::Error)
      end
    end

    context 'and error is 404' do
      let(:status_error) { 404 }

      it 'raises error' do
        expect { worker_perform }.to raise_error(Agero::Rsc::Error)
      end
    end

    context 'and error is 500' do
      let(:status_error) { 500 }

      it 'raises error' do
        expect { worker_perform }.to raise_error(Agero::Rsc::Error)
      end
    end
  end
end
