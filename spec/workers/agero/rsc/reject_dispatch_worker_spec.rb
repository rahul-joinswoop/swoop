# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::RejectDispatchWorker do
  include_context "basic rsc setup"

  subject(:worker_perform) { Agero::Rsc::RejectDispatchWorker.new.perform(job_id) }

  let(:job) do
    create :fleet_motor_club_job, {
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
      updated_at: '2018-07-26 18:08:04',
      reject_reason_id: job_reject_reason_id,
    }
  end

  let(:job_id) { job.id }

  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
  end

  let(:vendor_id) { '1234' }
  let(:facility_id) { '10' }

  before do
    stub_request(:post, "https://agero.example.com/v1/dispatches/123123123/refuseDispatchRequest")
      .to_return(status: 200, body: "", headers: {})
  end

  shared_examples 'RSC Rejection sync' do |expected_rsc_reason|
    it 'calls Rsc::API#refuse_dispatch with expected params' do
      expect_any_instance_of(Agero::Rsc::API).to receive(:refuse_dispatch).with(
        job.issc_dispatch_request.dispatchid, reason: expected_rsc_reason
      )

      worker_perform
    end
  end

  context "when job_reject_reason_id maps to 'Do not accept payment type'" do
    let(:job_reject_reason_id) do
      create(:job_reject_reason, text: 'Do not accept payment type').id
    end

    it_behaves_like 'RSC Rejection sync', :payment_type
  end

  context "when job_eta_explanation_id maps to 'No longer offer service'" do
    let(:job_reject_reason_id) { create(:job_reject_reason, text: 'No longer offer service').id }

    it_behaves_like 'RSC Rejection sync', :no_longer_offering_service
  end

  context "when job_eta_explanation_id maps to 'Out of my coverage area'" do
    let(:job_reject_reason_id) { create(:job_reject_reason, text: 'Out of my coverage area').id }

    it_behaves_like 'RSC Rejection sync', :out_of_coverage_area
  end

  context 'when job_eta_explanation_id maps to Other' do
    let(:job_reject_reason_id) { create(:job_reject_reason, text: 'Other').id }

    it_behaves_like 'RSC Rejection sync', :other
  end

  context "when job_eta_explanation_id maps to 'Proper equipment not available'" do
    let(:job_reject_reason_id) do
      create(:job_reject_reason, text: 'Proper equipment not available').id
    end

    it_behaves_like 'RSC Rejection sync', :equipment_not_available
  end

  context 'when job_eta_explanation_id is nil' do
    let(:job_reject_reason_id) { nil }

    it_behaves_like 'RSC Rejection sync', :other
  end

  context 'when job_eta_explanation_id does not match an Agero one' do
    let(:job_reject_reason_id) do
      create(:job_reject_reason, text: 'An unexpected new reject reason').id
    end

    it_behaves_like 'RSC Rejection sync', :other
  end

  context 'when Agero returns error' do
    before do
      stub_request(
        :post,
        "https://agero.example.com/v1/dispatches/123123123/refuseDispatchRequest"
      ).to_return(status: status_error, body: "", headers: {})
    end

    # can be any job_reject_reason_id
    let(:job_reject_reason_id) do
      create(:job_reject_reason, text: 'Do not accept payment type').id
    end

    context 'and error is 403' do
      let(:status_error) { 403 }

      it 'does not raise error' do
        expect { worker_perform }.not_to raise_error
      end
    end

    context 'and error is 401' do
      let(:status_error) { 401 }

      it 'raises error' do
        expect { worker_perform }.to raise_error(Agero::Rsc::Error)
      end
    end

    context 'and error is 404' do
      let(:status_error) { 404 }

      it 'raises error' do
        expect { worker_perform }.to raise_error(Agero::Rsc::Error)
      end
    end

    context 'and error is 500' do
      let(:status_error) { 500 }

      it 'raises error' do
        expect { worker_perform }.to raise_error(Agero::Rsc::Error)
      end
    end
  end
end
