# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::CancelDispatchWorker do
  include_context "basic rsc setup"

  let(:job) do
    create :fleet_motor_club_job, {
      status: job_status,
      fleet_company: agero_company,
      rescue_company: rescue_company,
      issc_dispatch_request: issc_dispatch_request,
    }
  end

  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
  end

  let(:vendor_id) { "999" }
  let(:facility_id) { "1" }

  context '.perform' do
    let(:job_status) { 'Canceled' }

    it 'calls Agero::Rsc::API#cancel_dispatch with expected params' do
      expect_any_instance_of(Agero::Rsc::API).to receive(:cancel_dispatch).with(
        job.issc_dispatch_request.dispatchid,
        status: 'whatever rsc_status',
        reason: 'whatever rsc_reason'
      )

      Agero::Rsc::CancelDispatchWorker.new.perform(
        job.id, 'whatever rsc_status', 'whatever rsc_reason'
      )
    end

    it "gracefully handles 403 response indicating the dispatch is already cancelled" do
      expect_any_instance_of(Agero::Rsc::API).to receive(:cancel_dispatch).and_raise(Agero::Rsc::ForbiddenError)
      expect do
        Agero::Rsc::CancelDispatchWorker.new.perform(job.id, 'whatever rsc_status', 'whatever rsc_reason')
      end.not_to raise_error
    end
  end

  context '.schedule_for' do
    shared_examples 'async scheduling Agero::Rsc::CancelDispatchWorker' do |rsc_status, rsc_reason|
      it 'calls Agero::Rsc::CancelDispatchWorker with expected params' do
        expect(Agero::Rsc::CancelDispatchWorker).to receive(:perform_async).with(
          job.id, rsc_status, rsc_reason
        )

        Agero::Rsc::CancelDispatchWorker.schedule_for(job, job_status_id)
      end
    end

    context "when job.status is Canceled" do
      let(:job_status) { 'Canceled' }
      let(:job_status_id) { JobStatus::CANCELED }

      context 'and job has a job_status_reason_type' do
        let!(:job_status_reason) do
          create_list(
            :job_status_reason,
            1,
            job: job,
            job_status_reason_type: job_status_reason_type,
            audit_job_status: audit_job_status,
          )
        end

        let(:job_status_reason_type) do
          create(:job_status_reason_type, key)
        end

        let(:audit_job_status) do
          create(:audit_job_status, job: job, job_status: job_status_reason_type.job_status)
        end

        context 'and job_status_reason_type.key = :cancel_prior_job_delayed' do
          let(:key) { :cancel_prior_job_delayed }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :service_provider_cancel, :prior_job_delayed,
          )
        end

        context 'and job_status_reason_type.key = :cancel_traffic_service_vehicle_problem' do
          let(:key) { :cancel_traffic_service_vehicle_problem }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :service_provider_cancel, :traffic_vehicle_problem,
          )
        end

        context 'and job_status_reason_type.key = :cancel_out_of_area' do
          let(:key) { :cancel_out_of_area }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :service_provider_cancel, :out_of_area,
          )
        end

        context 'and job_status_reason_type.key = :cancel_another_job_priority' do
          let(:key) { :cancel_another_job_priority }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :service_provider_cancel, :another_job_priority,
          )
        end

        context 'and job_status_reason_type.key = :cancel_no_reason_given' do
          let(:key) { :cancel_no_reason_given }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :service_provider_cancel, :no_reason_given,
          )
        end

        context 'and job_status_reason_type.key = :cancel_customer_found_alternate_solution' do
          let(:key) { :cancel_customer_found_alternate_solution }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :customer_cancel, :found_alternate_solution,
          )
        end
      end

      context 'and job does not have a reason type' do # currently covers mobile
        it_behaves_like(
          'async scheduling Agero::Rsc::CancelDispatchWorker',
          :service_provider_cancel, :no_reason_given,
        )
      end
    end

    context "when job.status is GOA" do
      let(:job_status) { 'GOA' }
      let(:job_status_id) { JobStatus::GOA }

      context 'and job has a job_status_reason_type' do
        let!(:job_status_reason) do
          create_list(
            :job_status_reason,
            1,
            job: job,
            job_status_reason_type: job_status_reason_type,
            audit_job_status: audit_job_status,
          )
        end

        let(:job_status_reason_type) do
          create(:job_status_reason_type, key)
        end

        let(:audit_job_status) do
          create(:audit_job_status, job: job, job_status: job_status_reason_type.job_status)
        end

        context 'and job_status_reason_type.key = :goa_customer_cancel_after_deadline' do
          let(:key) { :goa_customer_cancel_after_deadline }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :customer_cancel, :customer_cancel,
          )
        end

        context 'and job_status_reason_type.key = :goa_wrong_location_given' do
          let(:key) { :goa_wrong_location_given }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :unsuccessful_no_vehicle, :wrong_location_given,
          )
        end

        context 'and job_status_reason_type.key = :goa_customer_not_with_vehicle' do
          let(:key) { :goa_customer_not_with_vehicle }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :unsuccessful_no_customer, :customer_not_with_vehicle,
          )
        end

        context 'and job_status_reason_type.key = :goa_incorrect_service' do
          let(:key) { :goa_incorrect_service }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :service_attempt_failed, :incorrect_service,
          )
        end

        context 'and job_status_reason_type.key = :goa_incorrect_equipment' do
          let(:key) { :goa_incorrect_equipment }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :service_attempt_failed, :incorrect_equipment,
          )
        end

        context 'and job_status_reason_type.key = :goa_unsuccessful_service_attempt' do
          let(:key) { :goa_unsuccessful_service_attempt }

          it_behaves_like(
            'async scheduling Agero::Rsc::CancelDispatchWorker',
            :service_attempt_failed, :need_additional_equipment,
          )
        end
      end

      context 'and job does not have a reason type' do # currently covers mobile
        it_behaves_like(
          'async scheduling Agero::Rsc::CancelDispatchWorker',
          :service_attempt_failed, :incorrect_service
        )
      end
    end
  end
end
