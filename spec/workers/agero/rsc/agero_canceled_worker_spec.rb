# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::AgeroCanceledWorker do
  subject(:worker_perform) do
    Agero::Rsc::AgeroCanceledWorker.new.perform(api_access_token.callback_token, event)

    job.reload
    issc_dispatch_request.reload
  end

  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: "Job Canceled",
      message: "Job Canceled",
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:notification_code) { 5 }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { 123 }

  let(:api_access_token) { create(:api_access_token, company: rescue_company, vendorid: vendor_id) }
  let(:rsc_api) { Agero::Rsc::API.new(api_access_token.access_token) }

  let!(:agero_account) do
    create(:account, company: rescue_company, client_company: agero_company)
  end
  let(:agero_company) { create(:fleet_company, :agero) }
  let(:rescue_company) { create(:rescue_company) }

  let(:job) do
    create :fleet_motor_club_job, {
      status: job_status,
      fleet_company: agero_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
    }
  end

  let!(:issc_dispatch_request) do
    create(
      :issc_dispatch_request,
      dispatchid: dispatch_request_number,
      issc: issc,
      status: issc_dispatch_request_status,
      max_eta: 90.minutes,
      job: job
    )
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM, api_access_token: api_access_token) }

  context 'when job.status is set to any before canceled or GOA' do
    let(:job_status) { Job::STATUS_ACCEPTED }
    let(:issc_dispatch_request_status) { 'accepted' }

    it 'moves job.status from accepted to canceled' do
      expect { worker_perform }.to change(job, :status).from(Job::STATUS_ACCEPTED).to(Job::STATUS_CANCELED)
    end

    it 'moves job.issc_dispatch_request from new_request to canceled' do
      expect { worker_perform }.to change(issc_dispatch_request, :status)
        .from('accepted').to('canceled')
    end
  end

  context 'when job.status is canceled' do
    let(:job_status) { Job::STATUS_CANCELED }
    let(:issc_dispatch_request_status) { 'canceled' }

    it 'does not change job.status' do
      expect { worker_perform }.to avoid_changing(job, :status)
    end

    it 'does not change job.issc_dispatch_request.status' do
      expect { worker_perform }.to avoid_changing(issc_dispatch_request, :status)
    end
  end

  context 'when job.status is goa' do
    let(:job_status) { 'goa' }
    let(:issc_dispatch_request_status) { 'canceled' }

    it 'does not change job.status' do
      expect { worker_perform }.to avoid_changing(job, :status)
    end

    it 'does not change job.issc_dispatch_request.status' do
      expect { worker_perform }.to avoid_changing(issc_dispatch_request, :status)
    end
  end
end
