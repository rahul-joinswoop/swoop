# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::EtaRejectedWorker do
  subject(:worker_perform) do
    Agero::Rsc::EtaRejectedWorker.new.perform(api_access_token.callback_token, event)

    job.reload
    issc_dispatch_request.reload
  end

  # {
  #   "notificationEventId": 1827911069,
  #   "notificationCode": 7,
  #   "vendorId": "123",
  #   "facilityId": "1",
  #   "dispatchSource": "eDispatch",
  #   "dispatchRequestNumber": 1541739811,
  #   "title": "Job Rejected",
  #   "message": "Your ETA has been rejected"
  # }
  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: "Job Rejected",
      message: "Your ETA has been rejected",
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:notification_code) { 7 }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { 123 }

  let(:api_access_token) { create(:api_access_token, company: rescue_company, vendorid: vendor_id) }
  let(:rsc_api) { Agero::Rsc::API.new(api_access_token.access_token) }

  let!(:agero_account) do
    create(:account, company: rescue_company, client_company: agero_company)
  end
  let(:agero_company) { create(:fleet_company, :agero) }
  let(:rescue_company) { create(:rescue_company) }

  let(:job) do
    create :fleet_motor_club_job, {
      status: Job::STATUS_SUBMITTED,
      fleet_company: agero_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
    }
  end

  let!(:issc_dispatch_request) do
    create(
      :issc_dispatch_request,
      dispatchid: dispatch_request_number,
      issc: issc,
      max_eta: 90.minutes,
      job: job
    )
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM, api_access_token: api_access_token) }

  it 'moves job.status from submitted to etarejected' do
    expect { worker_perform }.to change(job, :status).from(Job::STATUS_SUBMITTED).to(Job::STATUS_ETAREJECTED)
  end

  it 'moves job.issc_dispatch_request from new_request to rejected' do
    expect { worker_perform }.to change(issc_dispatch_request, :status)
      .from('new_request').to('rejected')
  end
end
