# frozen_string_literal: true

require "rails_helper"

describe Agero::Rsc::DisconnectOauthWorker do
  include_context "basic rsc setup"

  subject(:worker_perform) do
    described_class.new.perform(rescue_company.id, "1")
  end

  let(:vendor_id) { "1" }
  let(:facility_id) { "2" }

  it "doesn't call on deleted tokens" do
    api_access_token.deleted_at = Time.now
    api_access_token.save!
    expect { worker_perform }.to avoid_changing(api_access_token.isscs, :count)
  end

  context 'success' do
    before do
      allow_any_instance_of(::Agero::Rsc::API).to receive(:unsubscribe_from_server_notifications).and_return(nil)
      allow_any_instance_of(::Agero::Rsc::API).to receive(:sign_out).and_return(nil)
    end

    it "sets isscs deleted_at" do
      expect { worker_perform }.to change(api_access_token.isscs, :count).by(-1)
    end

    it "sets tokens deleted_at" do
      expect { worker_perform && api_access_token.reload }.to change(api_access_token, :deleted_at).from(nil)
    end
  end
end
