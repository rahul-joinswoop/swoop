# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::ExpireJobWorker do
  let(:job) do
    create :fleet_motor_club_job, {
      status: Job::ASSIGNED,
      fleet_company: fleet_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
    }
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company, :ago) }

  it "expires a job" do
    Agero::Rsc::ExpireJobWorker.new.perform(job.id)
    job.reload
    expect(job.expired?).to eq true
    expect(Agero::Rsc::RejectDispatchWorker.jobs).to be_empty
  end
end
