# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::NewJobOfferedWorker do
  include_context "basic rsc setup"

  subject(:worker_perform) do
    Agero::Rsc::NewJobOfferedWorker.new.perform(api_access_token.callback_token, event)
  end

  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: title,
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:notification_code) { 1 }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { 123456789 }
  let(:title) { 'New Offer' }

  before do
    allow(Agero::Rsc::NewJobOffered::Service).to receive(:call).and_call_original

    stub_request(:get, Agero::Rsc::Client.url("/dispatches/123456789/detail"))
      .to_return(
        status: 200,
        headers: { "Content-Type" => "application/json" },
        body: rsc_api_response(:get_dispatch)
      )
  end

  it 'calls Agero::Rsc::NewJobOffered::Service' do
    expect(Agero::Rsc::NewJobOffered::Service).to receive(:call).with(
      input: { callback_token: api_access_token.callback_token, event: event }
    )

    worker_perform
  end

  it 'fires Segment event' do
    expect(TrackEvent).to receive(:call)

    worker_perform
  end
end
