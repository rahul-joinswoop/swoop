# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::JobCompletedWorker do
  # {
  #   "notificationEventId": 1827911069,
  #   "notificationCode": 29,
  #   "vendorId": "123",
  #   "facilityId": "1",
  #   "dispatchSource": "eDispatch",
  #   "dispatchRequestNumber": 1541739811,
  #   "title": "Job Approved",
  #   "message": "Your job was approved"
  # }
  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: "Job Complete",
      message: "Job complete",
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:notification_code) { 29 }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { 123 }

  let(:api_access_token) { create(:api_access_token, company: rescue_company, vendorid: vendor_id) }
  let(:rsc_api) { Agero::Rsc::API.new(api_access_token.access_token) }

  let!(:agero_account) do
    create(:account, company: rescue_company, client_company: agero_company)
  end
  let(:agero_company) { create(:fleet_company, :agero) }
  let(:rescue_company) { create(:rescue_company) }

  let(:job) do
    create :fleet_motor_club_job, {
      status: Job::STATUS_ACCEPTED,
      fleet_company: agero_company,
      adjusted_created_at: Time.now,
      rescue_company: rescue_company,
    }
  end

  let!(:issc_dispatch_request) do
    create(
      :issc_dispatch_request,
      dispatchid: dispatch_request_number,
      issc: issc,
      max_eta: 90.minutes,
      job: job,
      status: IsscDispatchRequest::ACCEPTED
    )
  end

  let(:rescue_company) { create(:rescue_company) }
  let(:issc) { create(:ago_issc, company: rescue_company, system: Issc::RSC_SYSTEM, api_access_token: api_access_token) }

  let(:dispatch_details) do
    dispatch_details = JSON.load(rsc_api_response(:get_dispatch))
    dispatch_details["poNumber"] = "NEWPONUMBER"
    dispatch_details["dispatchSource"] = dispatch_source
    dispatch_details
  end

  before :each do
    allow_any_instance_of(Agero::Rsc::JobCompleted::Service).to receive(:dispatch_details).and_return(dispatch_details)
  end

  context 'moves job status from accepted to completed' do
    it 'works' do
      expect(Agero::Rsc::JobCompleted::Service)
        .to receive(:call).with({
          input: { callback_token: api_access_token.callback_token, event: event },
        }).and_call_original

      Agero::Rsc::JobCompletedWorker.new.perform(api_access_token.callback_token, event)
      job.reload
      issc_dispatch_request.reload

      expect(job.status).to eq('completed')
      expect(issc_dispatch_request.status).to eq('completed')
    end
  end
end
