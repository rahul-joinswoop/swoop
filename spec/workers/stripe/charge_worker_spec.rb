# frozen_string_literal: true

require "rails_helper"

# Stripe testing data can be obtained from:
# https://stripe.com/docs/testing#cards
describe Stripe::ChargeWorker, vcr: true do
  subject(:worker_perform) do
    Stripe::ChargeWorker.new
      .perform(async_request.id, invoice_charge_attributes, invoice.id)
  end

  let!(:invoice) do
    create(
      :invoice,
      :with_line_items,
      sender: rescue_company,
      job: job,
    )
  end

  let(:job) { create(:fleet_in_house_job, account: account) }

  let(:account) { create(:account) }

  let!(:rescue_company) { create(:rescue_company, name: 'Real Tow') }
  let!(:user) do
    user = create(:user, company: rescue_company)
    user.roles << role_admin

    user
  end
  let!(:role_permissions) do
    create(
      :company_role_permission,
      company: rescue_company,
      role: role_admin,
      permission_type: CompanyRolePermission::CHARGE
    )
  end
  let(:role_admin) { create(:role, :admin) }
  let!(:company_invoice_charge_fee) do
    create(:company_invoice_charge_fee, company: rescue_company)
  end
  let!(:custom_account) do
    create(:custom_account, company: rescue_company)
  end

  let(:invoice_charge_attributes) do
    {
      token: stripe_test_token,
      amount: charge_amount,
      send_recipient_to: 'customer@email.com',
      memo: 'A note for the payment',
    }
  end

  let(:stripe_test_token) { 'tok_visa' } # this was taken from Stripe's testing data ^

  let(:async_request) do
    create(
      :api_async_request,
      company_id: rescue_company.id,
      user_id: user.id,
      target_id: invoice.id,
      system: API::AsyncRequest::INVOICE_CHARGE_CARD
    )
  end
  let(:charge_amount) { 99.06 }

  before do |example|
    unless example.metadata[:skip_before]
      allow(Stripe::Charge).to receive(:create).and_call_original
      allow(Publishers::AsyncRequestWebsocketMessage).to receive(:call)

      worker_perform

      invoice.reload

      async_request.reload
    end
  end

  it 'call Stripe charge API' do
    expect(Stripe::Charge).to have_received(:create).with(
      {
        amount: 9906,
        currency: invoice.currency,
        source: "tok_visa",
        statement_descriptor: "Real Tow",
        description: invoice.job_id,
        metadata: {
          job_id: invoice.job_id,
          invoice_payment_charge_id: invoice.charges.first.id,
        },
        destination: {
          amount: 9589,
          # @see https://github.com/joinswoop/swoop/wiki/Stripe-Setup
          # the account value can be obtained from Stripe account, after a Bank Account is linked:
          account: "acct_1CWnkwF0C1gUdX8T",
        },
      }
    )
  end

  it 'adds one payment with charge to the invoice' do
    payment = invoice.payments.first
    payment_charge = payment.charge

    expect(invoice.payments.size).to eq 1
    expect(payment.total_amount.to_s).to eq "-99.06"
    expect(payment.description).to eq 'A note for the payment'

    # VCR, data that comes from Stripe:
    expect(payment_charge.last_4).to eq "4242"
    expect(payment_charge.card_brand).to eq "Visa"
    expect(payment_charge.invoice_id).to eq invoice.id
    expect(payment_charge.upstream_charge_id).to be_present
    expect(payment_charge.data).to be_present
    expect(payment_charge.status).to eq 'charge_successful'
  end

  it 'sets api_async_request.request in JSON format' do
    json = JSON.parse async_request.external_request_body

    expect(json).to be_present
  end

  it 'sets api_async_request.response' do
    expect(async_request.external_response_body).to be_present
  end

  it 'publishes the async_request result through WS' do
    expect(Publishers::AsyncRequestWebsocketMessage).to have_received(:call).with({
      input: {
        charge: {
          status: 'success',
        },
      },
      id: async_request.id,
    })
  end

  context 'when charge value is higher than invoice.balance' do
    let(:charge_amount) { 101 }

    before do
      allow(Stripe::Charge).to receive(:create).and_call_original
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
    end

    it 'does not call Stripe endpoint' do
      expect(Stripe::Charge).not_to have_received(:create)
    end

    it 'does not add the payment to the invoice' do
      expect(invoice.payments.size).to eq 0
    end

    it 'pushes WS message with error.amount appended', :skip_before do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              'amount' => ['Amount cannot exceed balance'],
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )

      worker_perform
    end
  end

  context 'when Charge is declined' do
    before do
      allow(Stripe::Charge).to receive(:create).and_call_original
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
    end

    context 'by expired card' do
      let(:stripe_test_token) { 'tok_chargeDeclinedExpiredCard' }

      it 'does not add the payment to the invoice' do
        expect(invoice.payments.size).to eq 0
      end

      it 'pushes WS message with card error appended', :skip_before do
        expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                'card' => ['Card expired'],
              },
            },
            company: Company.find(invoice.sender_id),
          }
        )

        worker_perform
      end
    end

    context 'by fraud suspicion' do
      # this was taken from Stripe's testing data, and forces a card error
      let(:stripe_test_token) { 'tok_chargeDeclinedFraudulent' }

      it 'does not add the payment to the invoice' do
        expect(invoice.payments.size).to eq 0
      end

      it 'pushes WS message with error.amount appended', :skip_before do
        expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                'card' => ['Blocked for fraud risk. Contact Swoop for approval: (866) 219-8136.'],
              },
            },
            company: Company.find(invoice.sender_id),
          }
        )

        worker_perform
      end
    end

    context 'by incorrect CVC' do
      # this was taken from Stripe's testing data, and forces a card error
      let(:stripe_test_token) { 'tok_chargeDeclinedIncorrectCvc' }

      it 'does not add the payment to the invoice' do
        expect(invoice.payments.size).to eq 0
      end

      it 'pushes WS message with error.amount appended', :skip_before do
        expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                'card' => ['Incorrect CVC'],
              },
            },
            company: Company.find(invoice.sender_id),
          }
        )

        worker_perform
      end
    end

    context 'by insuficient funds' do
      # this was taken from Stripe's testing data, and forces a card error
      let(:stripe_test_token) { 'tok_chargeDeclinedInsufficientFunds' }

      it 'does not add the payment to the invoice' do
        expect(invoice.payments.size).to eq 0
      end

      it 'pushes WS message with error.amount appended', :skip_before do
        expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                'card' => ['Insufficient funds. Contact card issuer.'],
              },
            },
            company: Company.find(invoice.sender_id),
          }
        )

        worker_perform
      end
    end

    context 'by processing error' do
      # this was taken from Stripe's testing data, and forces a card error
      let(:stripe_test_token) { 'tok_chargeDeclinedProcessingError' }

      it 'does not add the payment to the invoice' do
        expect(invoice.payments.size).to eq 0
      end

      it 'pushes WS message with error.amount appended', :skip_before do
        expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
          input: {
            class_name: 'AsyncRequest',
            target_data: {
              id: async_request.id,
              error: {
                'card' => ['Processing error. Please try again.'],
              },
            },
            company: Company.find(invoice.sender_id),
          }
        )

        worker_perform
      end
    end
  end

  context 'when there is a generic error', :skip_before do
    before do
      allow(Stripe::Charge).to receive(:create).and_raise(Stripe::APIConnectionError)
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
    end

    it 'does not add the payment to the invoice' do
      worker_perform

      expect(invoice.payments.size).to eq 0
    end

    it 'pushes WS message with error.amount appended' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              'charge' => ['Charge failed. Please try again.'],
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )

      worker_perform
    end
  end

  context 'when the bank account is not linked', :skip_before do
    before do
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
      allow(StripeIntegration::BankAccountRetriever).to(
        receive(:new)
        .and_raise(InvoiceService::ChargeProviders::Stripe::InvalidBankAccountError)
      )
    end

    it 'does not add the payment to the invoice' do
      expect(invoice.payments.size).to eq 0

      worker_perform
    end

    it 'pushes WS message with error.amount appended' do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              'charge' => ['Charge failed. Please try again.'],
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )

      worker_perform
    end
  end

  context 'when the user role is not authorized' do
    let!(:user) do
      user = create(:user, company: rescue_company)
      user.roles << role_dispatcher

      user
    end

    let(:role_dispatcher) { create(:role, :dispatcher) }

    before do
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
    end

    it 'does not add the payment to the invoice' do
      expect(invoice.payments.size).to eq 0
    end

    it 'pushes WS message with error.amount appended', :skip_before do
      expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              'charge' => ['Charge failed. Please try again.'],
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )

      worker_perform
    end
  end
end
