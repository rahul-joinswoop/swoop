# frozen_string_literal: true

require "rails_helper"

describe Stripe::UpdatePayoutScheduleWorker, vcr: true do
  describe ".call" do
    subject(:worker_perform) do
      Stripe::UpdatePayoutScheduleWorker.new.perform(
        rescue_company.custom_account.id, payout_interval
      )
    end

    let(:rescue_company) { create(:rescue_company) }
    let(:payout_interval) { 'monthly' }
    let!(:custom_account) do
      create(
        :custom_account,
        company: rescue_company,
        upstream_account_id: 'acct_1CcDwkKJ3aQumLV5'
      )
    end

    it 'calls StripeIntegration::PayoutScheduleUpdater with expected values' do
      expect(StripeIntegration::PayoutScheduleUpdater).to receive(:call).with(
        input: {
          custom_account_id: custom_account.id,
          payout_interval: payout_interval,
        }
      ).and_call_original

      worker_perform
    end

    it 'updates the custom_account on Stripe side' do
      custom_account_output = worker_perform

      expect(custom_account_output.payout_schedule.interval).to eq 'monthly'
    end
  end
end
