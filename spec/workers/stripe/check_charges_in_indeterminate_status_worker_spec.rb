# frozen_string_literal: true

require 'rails_helper'

describe Stripe::CheckChargesInIndeterminateStatusWorker do
  subject(:worker) { Stripe::CheckChargesInIndeterminateStatusWorker.new }

  let!(:old_charge) do
    create(:charge, invoice: invoice, created_at: 5.minutes.ago)
  end

  let(:old_charge_id) { old_charge.id }

  let!(:new_charge) do
    create(:charge, invoice: invoice, created_at: Time.current)
  end
  let(:new_charge_id) { new_charge.id }

  let(:invoice) { create(:invoice) }
  let(:system_mailer_double) { double(new: nil) }

  before do
    allow(Delayed::Job).to receive(:enqueue)
    allow(Stripe::EmailChargeInIndeterminateStatus).to receive(:new)
      .and_return(system_mailer_double)

    worker.perform

    old_charge.reload
    new_charge.reload
  end

  it "updates old_charge.alert_sent to true" do
    expect(old_charge.alert_sent).to be_truthy
  end

  it "keeps new_charge.alert_sent as false" do
    expect(new_charge.alert_sent).to be_falsey
  end

  it "instantiates Stripe::EmailChargeInIndeterminateStatus with the found charge" do
    expect(Stripe::EmailChargeInIndeterminateStatus).to have_received(:new).with(
      old_charge_id
    )
  end

  it "does not instantiate Stripe::EmailChargeInIndeterminateStatus with the found charge" do
    expect(Stripe::EmailChargeInIndeterminateStatus).not_to have_received(:new).with(
      new_charge_id
    )
  end

  it "schedules a Delayed::Job with the Stripe::EmailChargeInIndeterminateStatus" do
    expect(Delayed::Job).to have_received(:enqueue).with(system_mailer_double)
  end
end
