# frozen_string_literal: true

require "rails_helper"

# Stripe testing data can be obtained from:
# https://stripe.com/docs/testing#cards
describe Stripe::RefundWorker, vcr: true do
  subject(:worker_perform) do
    Stripe::RefundWorker.new
      .perform(async_request.id, invoice.id, invoice_payment_id)
  end

  let!(:invoice) do
    create(
      :invoice,
      :with_line_items,
      :with_payments,
      sender: rescue_company,
    )
  end
  let(:invoice_payment_id) { invoice.payments.first.id }
  let!(:payment_charge) do
    create(
      :charge,
      payment: invoice.payments.first,
      upstream_charge_id: 'ch_1CZddeJBq4iiGApqyN8xfgMi',
      invoice: invoice,
      status: InvoicePaymentCharge::CHARGE_STATUS_SUCCESSFUL
    )
  end
  let!(:rescue_company) { create(:rescue_company, name: 'Real Tow') }
  let!(:user) do
    user = create(:user, company: rescue_company)
    user.roles << role_admin

    user
  end
  let!(:role_permissions) do
    create(
      :company_role_permission,
      company: rescue_company,
      role: role_admin,
      permission_type: CompanyRolePermission::REFUND
    )
  end
  let(:role_admin) { create(:role, :admin) }
  let!(:custom_account) do
    create(:custom_account, company: rescue_company)
  end

  let(:async_request) do
    create(
      :api_async_request,
      company_id: rescue_company.id,
      user_id: user.id,
      target_id: invoice.id,
      system: API::AsyncRequest::INVOICE_CHARGE_CARD
    )
  end

  before do |example|
    unless example.metadata[:skip_before]
      allow(Stripe::Refund).to receive(:create).and_call_original
      allow(Publishers::AsyncRequestWebsocketMessage).to receive(:call)

      worker_perform

      invoice.reload

      async_request.reload
    end
  end

  it 'call Stripe charge API' do
    expect(Stripe::Refund).to have_received(:create).with(
      {
        charge: payment_charge.upstream_charge_id,
        metadata: {
          job_id: invoice.job_id,
          invoice_payment_charge_id: payment_charge.id,
        },
        reverse_transfer: true,
      }
    )
  end

  it 'adds one refund for the respective payment to the invoice' do
    refund = invoice.refunds.first
    payment_charge = refund.charge

    expect(invoice.refunds.size).to eq 1
    expect(refund.total_amount.to_s).to eq "10.0"
    expect(payment_charge).to be_present
  end

  it 'sets api_async_request.response in JSON format' do
    json = JSON.parse async_request.external_response_body

    expect(json).to be_present
  end

  it 'publishes the async_request result through WS' do
    refund_hash = RefundSerializer.new(invoice.refunds.first).serializable_hash

    expect(Publishers::AsyncRequestWebsocketMessage).to have_received(:call).with({
      input: {
        refund: refund_hash,
      },
      id: async_request.id,
    })
  end

  context 'when there is a generic error', :skip_before do
    before do
      allow(Stripe::Charge).to receive(:retrieve).and_raise(Stripe::APIConnectionError)
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
    end

    it 'raises Stripe::APIConnectionError' do
      expect { worker_perform }.to raise_error(Stripe::APIConnectionError)
    end

    it 'does not add the refund to the invoice' do
      expect { worker_perform }.to raise_error(Stripe::APIConnectionError)

      expect(invoice.refunds.size).to eq 0
    end

    it 'pushes WS message with error.amount appended' do
      expect { worker_perform }.to raise_error(Stripe::APIConnectionError)

      expect(Publishers::GenericWebsocketMessage).to have_received(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              'refund' => ['Refund failed. Please try again.'],
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )
    end
  end

  context 'when the bank account is not linked', :skip_before do
    before do
      allow(Publishers::GenericWebsocketMessage).to receive(:call)

      # couldn't find a better way to stub this
      allow_any_instance_of(InvoiceService::ChargeProviders::Stripe::RefundService).to(
        receive(:validate_linked_bank_account)
        .and_raise(InvoiceService::ChargeProviders::Stripe::InvalidBankAccountError)
      )
    end

    it 'raises Stripe::APIConnectionError' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::InvalidBankAccountError
      )
    end

    it 'does not add the refund to the invoice' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::InvalidBankAccountError
      )

      expect(invoice.refunds.size).to eq 0
    end

    it 'pushes WS message with error.amount appended' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::InvalidBankAccountError
      )

      expect(Publishers::GenericWebsocketMessage).to have_received(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              'refund' => ['Refund failed. Please try again.'],
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )
    end
  end

  context 'when the user role is not authorized', :skip_before do
    let!(:user) do
      user = create(:user, company: rescue_company)
      user.roles << role_dispatcher

      user
    end

    let(:role_dispatcher) { create(:role, :dispatcher) }

    before do
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
    end

    it 'raises Stripe::UserNotAuthorizedError' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::UserNotAuthorizedError
      )
    end

    it 'does not add a refund to the invoice' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::UserNotAuthorizedError
      )

      expect(invoice.refunds.size).to eq 0
    end

    it 'pushes WS message with error appended' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::UserNotAuthorizedError
      )

      expect(Publishers::GenericWebsocketMessage).to have_received(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              'refund' => ['Refund failed. Please try again.'],
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )
    end
  end

  context 'when the payment has already been refundedd', :skip_before do
    let!(:invoice) do
      create(
        :invoice,
        :with_line_items,
        :with_payments,
        :with_refunds,
        sender: rescue_company,
      )
    end

    let!(:payment_charge) do
      create(
        :charge,
        payment: invoice.payments.first,
        refund: invoice.refunds.first,
        upstream_charge_id: 'ch_1CZeDgJBq4iiGApqzmT0P7D4',
        invoice: invoice,
        status: InvoicePaymentCharge::CHARGE_STATUS_SUCCESSFUL
      )
    end

    let(:invoice_payment_id) { invoice.payments.first.id }

    before do
      allow(Publishers::GenericWebsocketMessage).to receive(:call)
    end

    it 'raises Stripe::PaymentAlreadyRefundedError' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::PaymentAlreadyRefundedError
      )
    end

    it 'does not add a new refund to the invoice' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::PaymentAlreadyRefundedError
      )

      expect(invoice.refunds.size).to eq 1
    end

    it 'pushes WS message with error appended' do
      expect { worker_perform }.to raise_error(
        InvoiceService::ChargeProviders::Stripe::PaymentAlreadyRefundedError
      )

      expect(Publishers::GenericWebsocketMessage).to have_received(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              'refund' => ['Refund failed. Please try again.'],
            },
          },
          company: Company.find(invoice.sender_id),
        }
      )
    end
  end
end
