# frozen_string_literal: true

require "rails_helper"

describe Stripe::DeleteCustomAccountByRescueCompanyIdWorker, vcr: true do
  subject(:account_destroyer_worker_perform) do
    Stripe::DeleteCustomAccountByRescueCompanyIdWorker.new.perform(
      async_request.id, rescue_company.id
    )
  end

  let(:async_request) do
    create(:api_async_request, company_id: super_company.id, user_id: user.id)
  end

  let(:super_company) { create(:super_company) }
  let(:user) { create(:user, company: super_company) }
  let(:rescue_company) { create(:rescue_company) }

  let(:stripe_response_mock) do
    double(output: {
      id: 'acct_1Cfs87H5Lo9PgCvS',
      deleted: true,
    })
  end

  context 'when account is successfully deleted' do
    before do
      allow(StripeIntegration::AccountDestroyer).to receive(:call).and_return(
        stripe_response_mock
      )
    end

    let!(:custom_account) do
      create(:custom_account, company: rescue_company, upstream_account_id: 'acct_1Cfs87H5Lo9PgCvS')
    end

    it 'updates the async_request with request and response' do
      account_destroyer_worker_perform

      async_request.reload
      custom_account.reload

      expect(async_request.external_request_body).to eq(
        "{\"upstream_account_id\":\"acct_1Cfs87H5Lo9PgCvS\"}"
      )

      expect(async_request.external_response_body).to eq(
        "{:id=>\"acct_1Cfs87H5Lo9PgCvS\", :deleted=>true}"
      )
    end

    it 'calls StripeIntegration::AccountDestroyer' do
      account_destroyer_worker_perform

      expect(StripeIntegration::AccountDestroyer).to have_received(:call).with(
        input: { custom_account: custom_account }
      )
    end

    it 'sets custom_account.deleted_at' do
      account_destroyer_worker_perform
      custom_account.reload

      expect(custom_account.deleted_at).to be_present
    end

    it 'sends success message through WS' do
      expect(Publishers::AsyncRequestWebsocketMessage).to receive(:call).with(
        input: {
          custom_account: {
            status: 'success',
          },
        },
        id: async_request.id
      )

      account_destroyer_worker_perform
    end
  end

  context 'when RescueCompany does not have a CustomAccount' do
    before do
      allow(Publishers::GenericWebsocketMessage).to receive(:call)

      account_destroyer_worker_perform
    end

    let!(:custom_account) do
      create(
        :custom_account,
        company: rescue_company,
        upstream_account_id: 'acct_1Cfs87H5Lo9PgCvS',
        deleted_at: Time.current
      )
    end

    it 'pushes the error to FE' do
      expect(Publishers::GenericWebsocketMessage).to have_received(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              custom_account: ['No Stripe Connect account exists for this company'],
            },
          },
          company: async_request.company,
        }
      )
    end
  end

  context 'when a generic error occurs' do
    let!(:custom_account) do
      create(:custom_account, company: rescue_company, upstream_account_id: 'acct_1Cfs87H5Lo9PgCvS')
    end

    before do
      allow(StripeIntegration::AccountDestroyer).to receive(:call).and_raise(StandardError)
      allow(Publishers::GenericWebsocketMessage).to receive(:call)

      account_destroyer_worker_perform
    end

    it 'pushes the error to FE' do
      expect(Publishers::GenericWebsocketMessage).to have_received(:call).with(
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            id: async_request.id,
            error: {
              custom_account: ['Stripe Connect account deletion failed'],
            },
          },
          company: async_request.company,
        }
      )
    end
  end
end
