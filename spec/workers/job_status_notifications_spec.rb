# frozen_string_literal: true

require "rails_helper"

describe JobStatusNotifications do
  subject(:job_status_notifications) { JobStatusNotifications.new.perform }

  it 'calls JobAlerts::NinetyMinutesFromSchedule service' do
    expect_any_instance_of(JobAlerts::NinetyMinutesFromSchedule).to receive(:call)

    subject
  end
end
