# frozen_string_literal: true

require 'rails_helper'

describe IsscDispatchStatusGps, freeze_time: true do
  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company, :ago) }

  let(:rsc_issc) { create(:ago_issc, system: "rsc", company: rescue_company) }
  let(:gco_issc) { create(:gco_issc, system: nil, company: rescue_company) }

  let(:dispatch_1) { create(:issc_dispatch_request, issc: gco_issc, jobid: "j1", dispatchid: "d1") }
  let(:dispatch_2) { create(:issc_dispatch_request, issc: gco_issc, jobid: "j2", dispatchid: "d2") }
  let(:dispatch_3) { create(:issc_dispatch_request, issc: rsc_issc, jobid: "j3", dispatchid: "d3") }
  let(:dispatch_4) { create(:issc_dispatch_request, issc: rsc_issc, jobid: "j4", dispatchid: "d4") }
  let(:dispatch_5) { create(:issc_dispatch_request, issc: gco_issc, jobid: "j5", dispatchid: "d5") }

  let(:vehicle) { create(:rescue_vehicle, lat: 45.0, lng: -118.0, location_updated_at: Time.now) }
  let(:not_moving_vehicle) { create(:rescue_vehicle, lat: 46.0, lng: -119.0, location_updated_at: 10.minutes.ago) }

  let!(:job_1) { create(:fleet_motor_club_job, status: "Towing", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, issc_dispatch_request: dispatch_1) }
  let!(:job_2) { create(:fleet_motor_club_job, status: "En Route", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, issc_dispatch_request: dispatch_2) }
  let!(:job_3) { create(:fleet_motor_club_job, status: "Towing", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, issc_dispatch_request: dispatch_3) }
  let!(:job_4) { create(:fleet_motor_club_job, status: "En Route", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, issc_dispatch_request: dispatch_4) }
  let!(:job_5) { create(:fleet_motor_club_job, status: "On Site", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, issc_dispatch_request: dispatch_5) }

  it "finds active RSC jobs with rescue vehicle locations in the Towing or En Route status" do
    connection_manager = ISSC::ConnectionManager.new
    client = ISSC::DigitalDispatchClient.new('ISSC_AUTH_TOKEN')
    allow(connection_manager).to receive(:ensure_connected).and_yield(client)
    expect(client).to receive(:dispatchstatus).with(nil, [
      {
        ClientID: "GCO",
        DispatchID: "d1",
        ContractorID: gco_issc.contractorid,
        EventTimeUTC: vehicle.updated_at,
        SentTimeUTC: vehicle.location_updated_at,
        StatusType: "GPS",
        Source: "Automated",
        Latitude: 45.0,
        Longitude: -118.0,
        GPSLastUpdateTimeUTC: vehicle.location_updated_at,
      },
      {
        ClientID: "GCO",
        DispatchID: "d2",
        ContractorID: gco_issc.contractorid,
        EventTimeUTC: vehicle.updated_at,
        SentTimeUTC: vehicle.location_updated_at,
        StatusType: "GPS",
        Source: "Automated",
        Latitude: 45.0,
        Longitude: -118.0,
        GPSLastUpdateTimeUTC: vehicle.location_updated_at,
      },
    ])
    IsscDispatchStatusGps.new(connection_manager: connection_manager).perform
  end
end
