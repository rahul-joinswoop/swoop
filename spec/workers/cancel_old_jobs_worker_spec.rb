# frozen_string_literal: true

require "rails_helper"

describe CancelOldJobsWorker do
  subject { described_class.new.perform }

  # In the tests below, setting the created_at date also sets the
  # adjusted_created_at date (if the data is created with
  # the adjusted_created_at then it gets reset to current time
  # and the test fails)
  context 'when adjusted_created_at older than 24 hours' do
    let!(:job) { create(:fleet_managed_job, created_at: 72.hours.ago, status: Job::STATUS_DRAFT) }

    it 'works with fleet managed jobs' do
      ClimateControl.modify CANCEL_OLD_JOBS: 'true' do
        subject
        expect(job.reload.status).to eq Job::STATUS_CANCELED
      end
    end
  end

  context 'when adjusted_created_at older than 24 hours' do
    let!(:job) { create(:rescue_job, created_at: 72.hours.ago, status: Job::STATUS_DRAFT) }

    it 'works with rescue jobs' do
      ClimateControl.modify CANCEL_OLD_JOBS: 'true' do
        subject
        expect(job.reload.status).to eq Job::STATUS_CANCELED
      end
    end
  end

  context 'when adjusted_created_at more recent than 24 hours' do
    let!(:job) { create(:fleet_managed_job, created_at: DateTime.current, status: Job::STATUS_DRAFT) }

    it 'works' do
      ClimateControl.modify CANCEL_OLD_JOBS: '' do
        subject
        expect(job.reload.status).not_to eq 'canceled'
      end
    end
  end

  context 'when adjusted_created_at older than 24 hours and status is completed' do
    let!(:job) { create(:fleet_managed_job, created_at: 25.hours.ago, status: 'completed') }

    it 'works' do
      ClimateControl.modify CANCEL_OLD_JOBS: '' do
        subject
        expect(job.reload.status).not_to eq 'canceled'
      end
    end
  end

  context 'when SWOOP_ENV is production' do
    it 'does not run' do
      ClimateControl.modify CANCEL_OLD_JOBS: '', SWOOP_ENV: 'production' do
        expect(SwoopUpdateJobState).not_to receive(:new)
        expect(PartnerUpdateJobState).not_to receive(:new)
        subject
      end
    end
  end

  context 'when CANCEL_OLD_JOBS setting is not present' do
    it 'does not run' do
      ClimateControl.modify SWOOP_ENV: 'production' do
        expect(SwoopUpdateJobState).not_to receive(:new)
        expect(PartnerUpdateJobState).not_to receive(:new)
        subject
      end
    end
  end
end
