# frozen_string_literal: true

require "rails_helper"

describe IsscConnect do
  subject do
    IsscConnect.new(connection_manager: connection_manager).perform
  end

  let(:connection_manager) { double(ISSC::ConnectionManager) }

  let(:issc_client) { double(ISSC::DigitalDispatchClient) }

  let!(:agero) { create(:fleet_company, :agero) }

  let!(:rescue_company) { create(:rescue_company, :finish_line) }

  let!(:issc) do
    Issc.create(company: rescue_company,
                clientid: Issc::AGERO_CLIENTID,
                system: nil,
                status: 'LoggedIn')
  end

  let!(:rsc_issc) do
    Issc.create(company: rescue_company,
                clientid: Issc::AGERO_CLIENTID,
                system: Issc::RSC_SYSTEM,
                status: 'LoggedIn')
  end

  it 'calls connect' do
    ClimateControl.modify ISSC_AUTH_TOKEN: "ISSC-TOKEN" do
      ClimateControl.modify ISSC_WEBHOOK_URL: "http://url" do
        expect(connection_manager).to receive(:issc_client).and_return(issc_client)
        expect(issc_client).to receive(:connect).and_return(OpenStruct.new({ code: 200 }))
        subject
        expect(issc.reload.status).to eql('registered')
        expect(rsc_issc.reload.status).to eql('logged_in')
      end
    end
  end
end
