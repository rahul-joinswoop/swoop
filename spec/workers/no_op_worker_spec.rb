# frozen_string_literal: true

require 'rails_helper'

describe NoOpWorker do
  it "is not enqueued" do
    NoOpWorker.jobs.clear

    NoOpWorker.perform_async(:blah, :foo, :bar)
    expect(NoOpWorker.jobs.size).to eq 0

    NoOpWorker.perform_in(0, 1)
    expect(NoOpWorker.jobs.size).to eq 0

    NoOpWorker.perform_in(Time.now, 1)
    expect(NoOpWorker.jobs.size).to eq 0
  end

  it "does not do anything if it is enqueued" do
    expect(NoOpWorker.new.perform).to eq nil
    expect(NoOpWorker.new.perform(:blah, :foo, :bar)).to eq nil
  end
end
