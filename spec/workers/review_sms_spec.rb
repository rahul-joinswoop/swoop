# frozen_string_literal: true

require 'rails_helper'

describe ReviewSMS do
  let(:subject) { ReviewSMS.new.perform(job.id) }
  let(:job) { create :job, fleet_company: fleet_company }
  let(:fleet_company) { create :fleet_company }

  context "without any specific features" do
    it 'works' do
      expect_any_instance_of(Review).to receive(:send_review_sms).once
      expect_any_instance_of(Job).to receive(:track).once.with(Metric::SENT, "sms_review", 1)
      expect { subject }.to change(Review, :count).by(1)
    end

    shared_examples "it does nothing" do
      it 'works' do
        expect_any_instance_of(Review).not_to receive(:send_review_sms)
        expect_any_instance_of(Job).not_to receive(:track).once.with(Metric::SENT, "sms_review", 1)
        expect { subject }.not_to change(Review, :count)
      end
    end

    context "without a review_sms" do
      let(:job) { create :job, fleet_company: fleet_company, review_sms: nil }

      it_behaves_like 'it does nothing'
    end

    context "without a rescue_company" do
      let(:job) { create :job, fleet_company: fleet_company, rescue_company: nil }

      it_behaves_like 'it does nothing'
    end

    context "with a review" do
      let!(:review) { create :review, job: job }

      it_behaves_like 'it does nothing'
    end

    context "with tesla" do
      let(:fleet_company) { create :fleet_company, :tesla }

      it_behaves_like 'it does nothing'
    end
  end

  context "with Feature::REVIEW_4_TEXTS" do
    let(:fleet_company) do
      c = super()
      c.features << create(:feature, name: Feature::REVIEW_4_TEXTS)
      c.save!
      c
    end

    it 'works' do
      expect_any_instance_of(NpsReview).to receive(:send_review_sms).once
      expect_any_instance_of(Job).to receive(:track).once.with(Metric::SENT, "sms_review", 1)
      expect { subject }.to change(NpsReview, :count).by(1)
    end
  end

  context "with Feature::REVIEW_CHOOSE" do
    let(:fleet_company) do
      c = super()
      c.features << create(:feature, name: Feature::REVIEW_CHOOSE)
      c.save!
      c
    end

    it 'works' do
      expect_any_instance_of(NpsChooseReview).to receive(:send_review_sms).once
      expect_any_instance_of(Job).to receive(:track).once.with(Metric::SENT, "sms_review", 1)
      expect { subject }.to change(NpsChooseReview, :count).by(1)
    end
  end

  context "with Feature::REVIEW_NPS_SURVEYS" do
    let(:fleet_company) do
      c = super()
      c.features << create(:feature, name: Feature::REVIEW_NPS_SURVEYS)
      c.save!
      c
    end

    it 'works' do
      expect_any_instance_of(Reviews::SurveyReview).to receive(:send_review_sms).once
      expect_any_instance_of(Job).to receive(:track).once.with(Metric::SENT, "sms_review", 1)
      expect { subject }.to change(Reviews::SurveyReview, :count).by(1)
    end
  end

  context "with Feature::REVIEW_PURE_NPS" do
    let(:fleet_company) do
      c = super()
      c.features << create(:feature, name: Feature::REVIEW_PURE_NPS)
      c.save!
      c
    end

    it 'works' do
      expect_any_instance_of(NpsPureReview).to receive(:send_review_sms).once
      expect_any_instance_of(Job).to receive(:track).once.with(Metric::SENT, "sms_review", 1)
      expect { subject }.to change(NpsPureReview, :count).by(1)
    end
  end
end
