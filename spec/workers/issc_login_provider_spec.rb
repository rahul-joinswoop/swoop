# frozen_string_literal: true

require "rails_helper"

describe IsscLoginProvider do
  include_context "issc provider setup"

  let(:provider) do
    described_class.new(connection_manager: manager)
  end

  it "allows provider login" do
    provider.perform([issc.id])

    expect(manager.issc_client.called?(:login)).to eq(true)

    login_args = manager.issc_client.arguments(:login)[0].length

    expect(login_args).to eq(1)
  end
end
