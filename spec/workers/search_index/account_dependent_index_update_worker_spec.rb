# frozen_string_literal: true

require 'rails_helper'

describe SearchIndex::AccountDependentIndexUpdateWorker do
  it "updates the associated RescueProvider index" do
    rescue_company = create(:rescue_company)
    account = create(:account, company: rescue_company)
    other_account = create(:account, company: rescue_company)
    job_1 = create(:job, account: account, rescue_company: rescue_company)
    job_2 = create(:job, account: account, rescue_company: rescue_company)
    create(:job, account: other_account, rescue_company: rescue_company)
    Indexer.jobs.clear

    SearchIndex::AccountDependentIndexUpdateWorker.new.perform(account.id)

    expect(Indexer.jobs.size).to eq 2
    expect(Indexer).to have_enqueued_sidekiq_job("index", "Job", job_1.id)
    expect(Indexer).to have_enqueued_sidekiq_job("index", "Job", job_2.id)
  end
end
