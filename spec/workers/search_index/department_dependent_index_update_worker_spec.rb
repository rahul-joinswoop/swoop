# frozen_string_literal: true

require 'rails_helper'

describe SearchIndex::DepartmentDependentIndexUpdateWorker do
  it "updates the associated RescueProvider index" do
    fleet_company = create(:fleet_company)
    department = create(:department, company: fleet_company)
    job_1 = create(:job, department: department, fleet_company: fleet_company)
    job_2 = create(:job, department: department, fleet_company: fleet_company)
    create(:job, department: create(:department, company: fleet_company), fleet_company: fleet_company)

    SearchIndex::DepartmentDependentIndexUpdateWorker.new.perform(department.id)

    expect(Indexer).to have_enqueued_sidekiq_job("index", "Job", job_1.id)
    expect(Indexer).to have_enqueued_sidekiq_job("index", "Job", job_2.id)
  end
end
