# frozen_string_literal: true

require 'rails_helper'

describe SearchIndex::CompanyDependentIndexUpdateWorker do
  it "updates the associated RescueProvider index" do
    company = create(:rescue_company)
    other_company = create(:rescue_company)
    user_1 = create(:user, company: company)
    create(:user, company: other_company)

    rescue_provider_1 = create(:rescue_provider, provider: company, site: create(:site, company: company))
    create(:rescue_provider, provider: other_company, site: create(:site, company: other_company))

    job_1 = create(:job, rescue_company: company)
    job_2 = create(:job, fleet_company: company)
    create(:job, rescue_company: other_company)

    Indexer.jobs.clear

    SearchIndex::CompanyDependentIndexUpdateWorker.new.perform(company.id)

    expect(Indexer.jobs.size).to eq 4
    expect(Indexer).to have_enqueued_sidekiq_job("index", "User", user_1.id)
    expect(Indexer).to have_enqueued_sidekiq_job("index", "RescueProvider", rescue_provider_1.id)
    expect(Indexer).to have_enqueued_sidekiq_job("index", "Job", job_1.id)
    expect(Indexer).to have_enqueued_sidekiq_job("index", "Job", job_2.id)
  end
end
