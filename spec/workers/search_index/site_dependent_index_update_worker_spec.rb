# frozen_string_literal: true

require 'rails_helper'

describe SearchIndex::SiteDependentIndexUpdateWorker do
  it "updates the associated RescueProvider index" do
    rescue_company = create(:rescue_company)
    site = create(:site, company: rescue_company)
    rescue_provider_1 = create(:rescue_provider, site: site, provider: rescue_company)
    rescue_provider_2 = create(:rescue_provider, site: site, provider: rescue_company)
    create(:rescue_provider, site: create(:site, company: rescue_company), provider: rescue_company)
    Indexer.jobs.clear

    SearchIndex::SiteDependentIndexUpdateWorker.new.perform(site.id)

    expect(Indexer.jobs.size).to eq 2
    expect(Indexer).to have_enqueued_sidekiq_job("index", "RescueProvider", rescue_provider_1.id)
    expect(Indexer).to have_enqueued_sidekiq_job("index", "RescueProvider", rescue_provider_2.id)
  end
end
