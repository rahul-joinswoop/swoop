# frozen_string_literal: true

require 'rails_helper'
RSpec.describe GraphQL::UserUpdatedWorker, type: :worker do
  subject { GraphQL::UserUpdatedWorker.new }

  before do
    # if we don't do this and one of our tests fail we overflow the output
    # with a deep dump of our SwoopSchema. Inspector (see spec/support/inspector)
    # is a kludge to override how a class is #inspect'd and we use it here
    # specifically because a failure here will break ci.
    SwoopSchema.prepend Inspector
  end

  let(:rescue_company) { create :rescue_company }
  let(:swoop_company) { create :super_company }

  shared_examples "called with companies" do
    it "works" do
      companies.each do |c|
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('userUpdated', { id: user.to_ssid }, user, { scope: c })
          .once
      end

      expect(SwoopSchema.subscriptions)
        .to receive(:trigger)
        .with('userUpdated', { id: user.to_ssid }, user, { scope: user })
        .once

      subject.perform user.id
    end
  end

  # TODO this is really just testing User#listeners, we should
  # just test that directly
  context "with a rescue company" do
    let(:user) { create :user, :rescue, company: rescue_company }
    let(:companies) { [rescue_company, swoop_company] }

    it_behaves_like "called with companies"
  end
end
