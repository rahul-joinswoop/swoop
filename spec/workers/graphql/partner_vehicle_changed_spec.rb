# frozen_string_literal: true

require 'rails_helper'
RSpec.describe GraphQL::PartnerVehicleChangedWorker, type: :worker do
  subject { GraphQL::PartnerVehicleChangedWorker.new }

  before do
    # if we don't do this and one of our tests fail we overflow the output
    # with a deep dump of our SwoopSchema. Inspector (see spec/support/inspector)
    # is a kludge to override how a class is #inspect'd and we use it here
    # specifically because a failure here will break ci.
    SwoopSchema.prepend Inspector
  end

  let(:rescue_company) { create :rescue_company }
  let(:rescue_vehicle) { create :rescue_vehicle, company: rescue_company }
  let(:swoop_company) { create :super_company }

  shared_examples "called with companies" do
    it "works" do
      companies.each do |c|
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('partnerVehicleChanged', {}, rescue_vehicle, { scope: c })
          .once
      end

      subject.perform rescue_vehicle.id
    end
  end

  context "with a rescue vehicle" do
    let(:companies) { [rescue_company] }

    it_behaves_like "called with companies"
  end
end
