# frozen_string_literal: true

require 'rails_helper'
RSpec.describe GraphQL::JobStatusChangedWorker, type: :worker do
  subject { GraphQL::JobStatusChangedWorker.new }

  before do
    # if we don't do this and one of our tests fail we overflow the output
    # with a deep dump of our SwoopSchema. Inspector (see spec/support/inspector)
    # is a kludge to override how a class is #inspect'd and we use it here
    # specifically because a failure here will break ci.
    SwoopSchema.prepend Inspector
  end

  let(:fleet_company) { create :fleet_company }
  let(:rescue_company) { create :rescue_company }
  let(:companies) { [fleet_company, rescue_company] }

  context "adding a rescue_company" do
    let(:job) do
      create :rescue_job, status: :assigned, rescue_company: rescue_company
    end
    let!(:admin) { create :user, :admin, :rescue, company: rescue_company }
    let!(:dispatcher) { create :user, :dispatcher, :rescue, company: rescue_company }
    let!(:rescue_driver) { create :user, :driver, :rescue, company: rescue_company }

    it "works" do
      [admin, dispatcher].each do |u|
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :active_states }, anything, { scope: u })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: u })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :map_states }, anything, { scope: u })
          .once
      end

      subject.perform job.id, rescue_company_id: [nil, rescue_company.id]
    end
  end

  context "rescue_driver" do
    let(:job) do
      create :rescue_job, rescue_driver: rescue_driver, status: :assigned, rescue_company: rescue_company
    end
    let(:rescue_driver) { create :user, :driver, :rescue, company: rescue_company }
    let(:rescue_driver2) { create :user, :driver, :rescue, company: rescue_company }

    context "adding" do
      it "works" do
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :active_states }, anything, { scope: job.rescue_driver })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: job.rescue_driver })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :map_states }, anything, { scope: job.rescue_driver })
          .once

        subject.perform job.id, rescue_driver_id: [nil, job.rescue_driver_id]
      end

      context "when the rescue driver is a dispatcher" do
        let(:rescue_driver) { super().tap { |u| u.roles << create(:role, :dispatcher) } }

        let(:job) { create :rescue_job, rescue_driver: rescue_driver, status: :accepted, rescue_company: rescue_company }

        it 'works' do
          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobStatusEntered', { states: :active_states }, anything, { scope: rescue_driver })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('activeJobsFeed', {}, anything, { scope: rescue_driver })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobStatusEntered', { states: :map_states }, anything, { scope: rescue_driver })
            .once

          subject.perform job.id, rescue_driver_id: [nil, job.rescue_driver_id]
        end
      end
    end

    context "removing" do
      let(:job) { super().tap { |j| j.update! rescue_driver_id: nil } }

      it "works" do
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusLeft', { states: :active_states }, anything, { scope: rescue_driver })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: rescue_driver })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusLeft', { states: :map_states }, anything, { scope: rescue_driver })
          .once

        subject.perform job.id, rescue_driver_id: [rescue_driver.id, nil]
      end

      context "when the rescue driver is a dispatcher" do
        let(:rescue_driver) { super().tap { |u| u.roles << create(:role, :dispatcher) } }

        let(:job) { super().tap { |j| j.update! rescue_driver_id: nil } }

        it 'works' do
          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobStatusLeft', { states: :active_states }, anything, { scope: rescue_driver })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('activeJobsFeed', {}, anything, { scope: rescue_driver })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobStatusLeft', { states: :map_states }, anything, { scope: rescue_driver })
            .once

          subject.perform job.id, rescue_driver_id: [rescue_driver.id, nil]
        end
      end
    end

    context "changing" do
      let!(:job) do
        super().tap { |j| j.update! rescue_driver: rescue_driver2 }
      end

      it "works" do
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusLeft', { states: :active_states }, anything, { scope: rescue_driver })
          .once
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusLeft', { states: :map_states }, anything, { scope: rescue_driver })
          .once
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: rescue_driver })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :active_states }, anything, { scope: rescue_driver2 })
          .once
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :map_states }, anything, { scope: rescue_driver2 })
          .once
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: rescue_driver2 })
          .once

        subject.perform job.id, rescue_driver_id: [rescue_driver.id, rescue_driver2.id]
      end
    end
  end

  context "on create" do
    let(:job) do
      create :job, status: :pending,
                   fleet_company: fleet_company,
                   rescue_company: rescue_company
    end

    it "works" do
      companies.each do |c|
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :active_states }, anything, { scope: c })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: c })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :map_states }, anything, { scope: c })
          .once
      end

      subject.perform job.id, status: [nil, "pending"]
    end
  end

  context "created -> pending -> canceled" do
    let(:job) do
      create :job, status: :created,
                   fleet_company: fleet_company,
                   rescue_company: rescue_company
    end

    it "works" do
      # nothing should be called here
      subject.perform job.id, status: [nil, 'created']

      # go from :created -> :pending (similar to going from draft/pre_draft
      # but much easier to do manually)
      companies.each do |c|
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :active_states }, anything, { scope: c })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: c })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :map_states }, anything, { scope: c })
          .once
      end

      job.update! status: :pending
      subject.perform job.id, status: ['created', 'pending']

      # now go from :pending -> :canceled
      companies.each do |c|
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusLeft', { states: :active_states }, anything, { scope: c })
          .once
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: c })
          .once
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusLeft', { states: :map_states }, anything, { scope: c })
          .once
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :done_states }, anything, { scope: c })
          .once
      end

      job.update! status: :canceled
      subject.perform job.id, status: ['pending', 'canceled']
    end
  end
end
