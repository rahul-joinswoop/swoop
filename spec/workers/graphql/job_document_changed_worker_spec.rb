# frozen_string_literal: true

require 'rails_helper'
RSpec.describe GraphQL::JobDocumentChangedWorker, type: :worker do
  subject(:worker_perform) { GraphQL::JobDocumentChangedWorker.new.perform(document.id) }

  before do
    # if we don't do this and one of our tests fail we overflow the output
    # with a deep dump of our SwoopSchema. Inspector (see spec/support/inspector)
    # is a kludge to override how a class is #inspect'd and we use it here
    # specifically because a failure here will break ci.
    SwoopSchema.prepend Inspector

    allow_any_instance_of(Job).to receive(:subscribers).and_return([rescue_company])
  end

  let(:document) { create(:signature_image, :pickup, job: job, user: user) }
  let(:rescue_company) { create :rescue_company }
  let(:job) do
    create :job, rescue_company: rescue_company
  end
  let(:user) { create :user, company: rescue_company }

  it 'triggers jobDocumentChanged subscription' do
    expect(SwoopSchema.subscriptions)
      .to receive(:trigger)
      .with(
        'jobDocumentChanged',
        { type: document.type, container: document.container, job_id: document.job.to_ssid },
        document,
        { scope: rescue_company }
      ).once

    worker_perform
  end
end
