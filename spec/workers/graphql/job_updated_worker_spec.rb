# frozen_string_literal: true

require 'rails_helper'
RSpec.describe GraphQL::JobUpdatedWorker, type: :worker do
  subject { GraphQL::JobUpdatedWorker.new }

  before do
    # if we don't do this and one of our tests fail we overflow the output
    # with a deep dump of our SwoopSchema. Inspector (see spec/support/inspector)
    # is a kludge to override how a class is #inspect'd and we use it here
    # specifically because a failure here will break ci.
    SwoopSchema.prepend Inspector
  end

  let(:fleet_company) { create :fleet_company }
  let(:rescue_company) { create :rescue_company }
  let(:companies) { [fleet_company, rescue_company] }
  let(:job) do
    create :job, status: :pending,
                 fleet_company: fleet_company,
                 rescue_company: rescue_company
  end

  it "works with an array of ids" do
    companies.each do |c|
      expect(SwoopSchema.subscriptions)
        .to receive(:trigger)
        .with('jobUpdated', { id: job.to_ssid }, anything, { scope: c })
        .once
      expect(SwoopSchema.subscriptions)
        .to receive(:trigger)
        .with('activeJobsFeed', {}, anything, { scope: c })
        .once
    end

    expect(SwoopSchema.subscriptions)
      .to receive(:trigger)
      .with('jobUpdated', { id: job.to_ssid }, anything, { scope: job.customer })
      .once

    subject.perform({ ids: [job.id] }.deep_stringify_keys)
  end

  it "works without an array of ids" do
    companies.each do |c|
      expect(SwoopSchema.subscriptions)
        .to receive(:trigger)
        .with('jobUpdated', { id: job.to_ssid }, anything, { scope: c })
        .once
      expect(SwoopSchema.subscriptions)
        .to receive(:trigger)
        .with('activeJobsFeed', {}, anything, { scope: c })
        .once
    end

    expect(SwoopSchema.subscriptions)
      .to receive(:trigger)
      .with('jobUpdated', { id: job.to_ssid }, anything, { scope: job.customer })
      .once

    subject.perform({ ids: job.id }.deep_stringify_keys)
  end

  context 'with customer_only: true' do
    it 'works' do
      expect(SwoopSchema.subscriptions)
        .to receive(:trigger)
        .with('jobUpdated', { id: job.to_ssid }, anything, { scope: job.customer })
        .once

      subject.perform({ ids: job.id, customer_only: true }.deep_stringify_keys)
    end
  end

  context 'auctions' do
    context 'with an active auction' do
      let!(:job) { create :fleet_motor_club_job, :with_auction, bids: 3, fleet_company: fleet_company }

      it "works" do
        job.auction.bids.each do |bid|
          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobUpdated', { id: bid.job_ssid }, anything, { scope: bid.company })
            .once
          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobUpdated', { id: job.to_ssid }, anything, { scope: bid.company })
            .once
          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('activeJobsFeed', {}, anything, { scope: bid.company })
            .once
        end

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: job.to_ssid }, anything, { scope: fleet_company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: fleet_company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: job.to_ssid }, anything, { scope: job.customer })
          .once

        subject.perform({ ids: job.id }.deep_stringify_keys)
      end
    end

    context 'with a completed auction' do
      let(:rescue_driver) { create :user, :rescue, :driver, company: job.rescue_company }
      let!(:job) do
        j = create :fleet_motor_club_job, :with_auction, bids: 3, fleet_company: fleet_company
        j.auction.bids.update_all status: Auction::Bid::AUTO_REJECTED
        j.auction.bids.first.update! status: Auction::Bid::WON
        j.auction.update! status: Auction::Auction::SUCCESSFUL, expires_at: Time.current
        j.update! issc_dispatch_request: create(:issc_dispatch_request, job: j),
                  rescue_company: j.auction.bids.first.company,
                  rescue_driver: create(:user, :rescue, :driver, company: j.auction.bids.first.company)
        j
      end

      it "works" do
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: job.auction.bids.first.job_ssid }, anything, { scope: job.rescue_company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: job.to_ssid }, anything, { scope: job.rescue_company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: job.rescue_company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: job.auction.bids.first.job_ssid }, anything, { scope: job.rescue_driver })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: job.to_ssid }, anything, { scope: job.rescue_driver })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: job.rescue_driver })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: job.to_ssid }, anything, { scope: fleet_company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, anything, { scope: fleet_company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: job.to_ssid }, anything, { scope: job.customer })
          .once

        subject.perform({ ids: job.id }.deep_stringify_keys)
      end
    end
  end
end
