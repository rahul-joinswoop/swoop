# frozen_string_literal: true

require 'rails_helper'
RSpec.describe GraphQL::AuctionBidStatusChangedWorker, type: :worker do
  subject { GraphQL::AuctionBidStatusChangedWorker.new }

  before do
    # if we don't do this and one of our tests fail we overflow the output
    # with a deep dump of our SwoopSchema. Inspector (see spec/support/inspector)
    # is a kludge to override how a class is #inspect'd and we use it here
    # specifically because a failure here will break ci.
    SwoopSchema.prepend Inspector
  end

  let!(:job) { create :fleet_motor_club_job, :with_auction, bids: 3 }
  let(:fleet_company) { job.fleet_company }
  let(:rescue_company) { create :rescue_company }
  let(:bids) { job.auction.bids }
  let(:companies) { job.auction.bids.map(&:company) }

  context "starting auction" do
    it "works" do
      # all of our bids are created in status Auction::Bid::REQUESTED so we just pretend we made that transition manually and pass
      # that diff through below
      bids.each do |bid|
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusEntered', { states: :active_states }, bid.job, { scope: bid.company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: bid.job.to_ssid }, bid.job, { scope: bid.company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: bid.job_ssid }, bid.job, { scope: bid.company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, bid.job, { scope: bid.company })
          .once

        expect(SwoopSchema.subscriptions)
          .not_to receive(:trigger)
          .with('jobStatusLeft', anything, bid.job, { scope: bid.company })

        subject.perform bid.id, status: [Auction::Bid::NEW, Auction::Bid::REQUESTED]
      end
    end
  end

  context "auction ended successfully" do
    it 'works' do
      bids.each do |bid|
        if bid == bids.first
          # our winner!
          bid.update! status: ::Auction::Bid::WON

          # winner generates a jobStatusEntered notification for :map_states only
          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobStatusEntered', { states: :map_states }, bid.job, { scope: bid.company })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobUpdated', { id: bid.job.to_ssid }, bid.job, { scope: bid.company })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobUpdated', { id: bid.job_ssid }, bid.job, { scope: bid.company })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('activeJobsFeed', {}, bid.job, { scope: bid.company })
            .once

          expect(SwoopSchema.subscriptions)
            .not_to receive(:trigger)
            .with('jobStatusEntered', { states: :active_states }, bid.job, { scope: bid.company })

          # winner generates no jobStatusLeft notifications
          expect(SwoopSchema.subscriptions)
            .not_to receive(:trigger)
            .with('jobStatusLeft', anything, bid.job, { scope: bid.company })

          subject.perform bid.id, status: [Auction::Bid::REQUESTED, ::Auction::Bid::WON]
        else
          # loser
          bid.update! status: ::Auction::Bid::AUTO_REJECTED

          # loser generates a jobStatusLeft for :active_states only
          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobStatusLeft', { states: :active_states }, bid.job, { scope: bid.company })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobUpdated', { id: bid.job.to_ssid }, bid.job, { scope: bid.company })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('jobUpdated', { id: bid.job_ssid }, bid.job, { scope: bid.company })
            .once

          expect(SwoopSchema.subscriptions)
            .to receive(:trigger)
            .with('activeJobsFeed', {}, bid.job, { scope: bid.company })
            .once

          expect(SwoopSchema.subscriptions)
            .not_to receive(:trigger)
            .with('jobStatusLeft', { states: :map_states }, bid.job, { scope: bid.company })

          # loser generates no jobStatusEntered notifications
          expect(SwoopSchema.subscriptions)
            .not_to receive(:trigger)
            .with('jobStatusEntered', anything, bid.job, anything)

          subject.perform bid.id, status: [Auction::Bid::REQUESTED, ::Auction::Bid::AUTO_REJECTED]
        end
      end
    end
  end

  context "auction expired" do
    it 'works' do
      bids.each do |bid|
        # everyone's a loser
        bid.update! status: ::Auction::Bid::EXPIRED
        # generate a jobStatusLeft for :active_states only
        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobStatusLeft', { states: :active_states }, bid.job, { scope: bid.company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('jobUpdated', { id: bid.job.to_ssid }, bid.job, { scope: bid.company })
          .once

        expect(SwoopSchema.subscriptions)
          .to receive(:trigger)
          .with('activeJobsFeed', {}, bid.job, { scope: bid.company })
          .once

        expect(SwoopSchema.subscriptions)
          .not_to receive(:trigger)
          .with('jobStatusLeft', { states: :map_states }, bid.job, { scope: bid.company })

        # generate no jobStatusEntered notifications
        expect(SwoopSchema.subscriptions)
          .not_to receive(:trigger)
          .with('jobStatusEntered', anything, bid.job, anything)

        subject.perform bid.id, status: [Auction::Bid::REQUESTED, ::Auction::Bid::EXPIRED]
      end
    end
  end
end
