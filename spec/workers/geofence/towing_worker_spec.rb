# frozen_string_literal: true

require 'rails_helper'

describe Geofence::TowingWorker do
  subject { described_class.new }

  let(:action) { subject.perform(job.id) }

  before do
    expect(subject).to receive(:find_job).with(job.id).and_return(job)
  end

  shared_examples 'sets source application' do
    let(:driver) { create(:user, :rescue_driver) }

    it 'sets platform GPS' do
      action
      expect(audit_job_status.job_status.name).to eq(job.human_status)
      expect(audit_job_status.source_application&.platform).to eq('GPS')
    end

    it 'sets history item user to rescue driver even if touched by admin' do
      job.update_columns(rescue_driver_id: driver.id)
      action
      expect(job.rescue_driver).not_to be_nil
      expect(audit_job_status.user).to eq(job.rescue_driver)
    end
  end

  shared_examples 'it works' do
    let(:audit_job_status) { job.audit_job_statuses.last }

    it 'advances to towing' do
      action
      expect(job.reload.status).to eq(Job::STATUS_TOWING)
    end

    it 'updates the digital dispatcher status' do
      expect(job).to receive(:update_digital_dispatcher_status)
      action
    end

    it 'sends a push' do
      expect(Notifiable).to receive(:trigger_geofence_towing).with(job).once
      action
    end

    context 'with no request context' do
      it_behaves_like 'sets source application'
    end

    context 'with existing request context', with_request_context: true do
      it_behaves_like 'sets source application'

      it 'does not wipe out an existing context' do
        hash = RequestContext.current.hash
        action
        expect(RequestContext.current.hash).to eq(hash)
      end
    end
  end

  shared_examples 'it works in different statuses' do
    context "in status 'On Site'" do
      let(:job) { create(job_type, status: Job::STATUS_ONSITE) }

      it_behaves_like 'it works'
    end

    context "receiving an 'On Site' job" do
      let(:job) { create(job_type, status: Job::STATUS_TOWING) }

      it 'does nothing' do
        expect { action }.not_to raise_error
        expect(job.reload.status).to eq(Job::STATUS_TOWING)
        expect(subject).not_to receive(:advance_to_towing)
      end
    end
  end

  context 'with a RescueJob' do
    let(:job_type) { :rescue_job }

    it_behaves_like 'it works in different statuses'
  end
end
