# frozen_string_literal: true

require 'rails_helper'

describe Geofence::CompletedWorker do
  subject { described_class.new }

  let(:action) { subject.perform(job.id) }

  before do
    expect(subject).to receive(:find_job).with(job.id).and_return(job)
  end

  shared_examples 'sets source application' do
    it 'sets platform GPS' do
      action
      expect(audit_job_status.job_status.name).to eq(job.human_status)
      expect(audit_job_status.source_application&.platform).to eq('GPS')
    end
  end

  shared_examples 'it does nothing' do
    it 'does no work' do
      expect(subject).not_to receive(:advance_to_completed)
      expect(Notifiable).not_to receive(:trigger_geofence_completed)
      before_status = job.status
      expect { action }.not_to raise_error
      expect(job.reload.status).to eq(before_status)
    end
  end

  shared_examples 'it works' do
    let(:audit_job_status) { job.audit_job_statuses.last }

    it 'advances to completed' do
      action
      expect(job.reload.status).to eq(Job::STATUS_COMPLETED)
    end

    it 'updates the digital dispatcher status' do
      expect(job).to receive(:update_digital_dispatcher_status)
      action
    end

    context 'with no request context' do
      it_behaves_like 'sets source application'
    end

    context 'with existing request context', with_request_context: true do
      it_behaves_like 'sets source application'

      it 'does not wipe out an existing context' do
        hash = RequestContext.current.hash
        action
        expect(RequestContext.current.hash).to eq(hash)
      end
    end
  end

  shared_examples 'it works in different statuses' do
    context "in status 'On Site'" do
      let(:job) { create(job_type, status: Job::STATUS_ONSITE) }

      it_behaves_like 'it works'

      it 'sends a push' do
        expect(Notifiable).to receive(:trigger_geofence_completed_without_dropoff).with(job).once
        expect(Notifiable).not_to receive(:trigger_geofence_completed_with_dropoff)
        action
      end
    end

    context "in status 'Towing'" do
      let(:job) { create(job_type, status: Job::STATUS_TOWING) }

      it_behaves_like 'it does nothing'
    end

    context "receiving a 'Dispatched' job" do
      let(:job) { create(job_type, status: Job::STATUS_DISPATCHED) }

      it_behaves_like 'it does nothing'
    end

    context "receiving a 'Tow Destination' job" do
      let(:job) { create(job_type, :with_drop_location, status: Job::STATUS_TOWDESTINATION) }

      it_behaves_like 'it works'

      it 'sends a push' do
        expect(Notifiable).to receive(:trigger_geofence_completed_with_dropoff).with(job).once
        expect(Notifiable).not_to receive(:trigger_geofence_completed_without_dropoff)
        action
      end
    end

    context "receiving a 'En Route' job" do
      let(:job) { create(job_type, status: Job::STATUS_ENROUTE) }

      it_behaves_like 'it does nothing'
    end

    context "receiving a 'Completed' job" do
      let(:job) { create(job_type, status: Job::STATUS_COMPLETED) }

      it_behaves_like 'it does nothing'
    end
  end

  context 'with a RescueJob' do
    let(:job_type) { :rescue_job }

    it_behaves_like 'it works in different statuses'
  end
end
