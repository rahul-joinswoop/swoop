# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notification::JobReassignedWorker do
  let(:rescue_company) { create :rescue_company }
  let(:rescue_drivers) { create_list :user, 2, :rescue, :driver, company: rescue_company }
  let(:user_ids) { [rescue_drivers.first.to_ssid].sort }
  let(:title) { Notifiable::JobReassigned.build_title(job) }
  let(:msg) { Notifiable::JobReassigned.build_msg }
  let(:link) { Notifiable::JobReassigned.build_link(job) }

  shared_examples "it works" do
    it "calls External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .to receive(:create_message)
        .with(user_ids: user_ids, title: title, msg: msg, link: link)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .not_to receive(:create_message)
      subject
    end
  end

  context "with a job and a rescue_driver" do
    subject do
      Notification::JobReassignedWorker.new.perform(job.id, rescue_drivers.first.id)
    end

    let!(:job) do
      create :rescue_job,
             :with_account,
             rescue_company: rescue_company,
             rescue_driver: rescue_drivers.last
    end

    it_behaves_like "it works"
  end
end
