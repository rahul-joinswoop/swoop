# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notification::JobLocationChangedWorker do
  let(:title) { Notifiable::JobLocationChanged.build_title(job) }
  let(:msg) { Notifiable::JobLocationChanged.build_msg }
  let(:link) { Notifiable::JobLocationChanged.build_link(job) }
  let(:user_ids) { [job.rescue_driver.to_ssid].sort }

  shared_examples "it works" do
    it "calls External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .to receive(:create_message)
        .with(user_ids: user_ids, title: title, msg: msg, link: link)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .not_to receive(:create_message)
      subject
    end
  end

  context "with a job" do
    subject { Notification::JobLocationChangedWorker.new.perform(job.id) }

    context "with a LocationChanged job and a driver" do
      let!(:job) do
        create :rescue_job,
               :with_rescue_driver,
               :with_account,
               service_location: create(:location)
      end

      it_behaves_like "it works"

      context "without a driver" do
        let!(:job) do
          create :rescue_job,
                 :with_account,
                 service_location: create(:location)
        end

        it_behaves_like "it does nothing"
      end
    end
  end
end
