# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context "users for notification settings" do
  # admins with notification settings setup
  let!(:admin_new_job_phone_sms_enabled) { create :user, roles: [:rescue, :admin], phone: '+1405111225222', company: rescue_company, notification_settings: [{ new_job: ['Text'] }] }
  let!(:admin_new_job_phone_call_and_sms_enabled) { create :user, roles: [:rescue, :admin], phone: '+1405111225709', company: rescue_company, notification_settings: [{ new_job: ['Call', 'Text'] }] }

  let!(:admin_eta_accepted_phone_sms_enabled) { create :user, roles: [:rescue, :admin], phone: '+1405111225782', company: rescue_company, notification_settings: [{ eta_accepted: ['Text'] }] }
  let!(:admin_eta_accepted_phone_call_and_sms_enabled) { create :user, roles: [:rescue, :admin], phone: '+1405111225786', company: rescue_company, notification_settings: [{ eta_accepted: ['Call', 'Text'] }] }

  # dispatchers with notification settings setup
  let!(:dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled) do
    create :user, roles: [:rescue, :dispatcher], phone: '+1405111245367', company: rescue_company, notification_settings: [{ new_job: ['Call'] }, { eta_accepted: ['Text', 'Call'] }]
  end
  let!(:dispatcher_2_new_job_phone_call_enabled) { create :user, roles: [:rescue, :dispatcher], phone: '+1405111222332', company: rescue_company, notification_settings: [{ new_job: ['Call'] }] }
  let!(:dispatcher_new_job_phone_sms_enabled) { create :user, roles: [:rescue, :dispatcher], phone: '+1405111225443', company: rescue_company, notification_settings: [{ new_job: ['Text'] }] }
  let!(:dispatcher_2_new_job_phone_sms_enabled) { create :user, roles: [:rescue, :dispatcher], phone: '+1405111222123', company: rescue_company, notification_settings: [{ new_job: ['Text'] }] }
  let!(:dispatcher_without_phone_new_job_call_enabled) { create :user, roles: [:rescue, :dispatcher], phone: nil, company: rescue_company, notification_settings: [{ new_job: ['Call'] }] }
  let!(:dispatcher_without_phone_new_job_sms_enabled) { create :user, roles: [:rescue, :dispatcher], phone: nil, company: rescue_company, notification_settings: [{ new_job: ['Text'] }] }
  let!(:dispatcher_deleted_new_job_phone_call_enabled) { create :user, roles: [:rescue, :dispatcher], phone: '+140511122872', company: rescue_company, notification_settings: [{ new_job: ['Call'] }], deleted_at: Time.now }
  let!(:dispatcher_deleted_new_job_phone_sms_enabled) { create :user, roles: [:rescue, :dispatcher], phone: '+1405111228385', company: rescue_company, notification_settings: [{ new_job: ['Text'] }], deleted_at: Time.now }
  let!(:broken_ex_dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled) do
    ex_dispatcher = create(:user, roles: [:rescue, :dispatcher], phone: '+1405111222337', company: rescue_company, notification_settings: [{ new_job: ['Call'] }, { eta_accepted: ['Text'] }])

    ex_dispatcher.add_role :driver
    ex_dispatcher.remove_role(:dispatcher)

    ex_dispatcher
  end

  let!(:dispatcher_eta_accepted_sms_enabled) do
    create :user, roles: [:rescue, :dispatcher], phone: '+1405111245819', company: rescue_company, notification_settings: [{ eta_accepted: ['Text'] }]
  end

  # all roles without notification settings
  let!(:admin_with_phone_notifications_disabled) { create :user, roles: [:rescue, :admin], phone: '+1405111222726', company: rescue_company }
  let!(:dispatcher_with_phone_notifications_disabled) { create :user, roles: [:rescue, :dispatcher], phone: '+1405111222726', company: rescue_company }
  let!(:driver_with_phone_notifications_disabled) { create :user, roles: [:rescue, :driver], phone: '+1405111222726', company: rescue_company }

  let(:all_admins) do
    [
      admin_new_job_phone_sms_enabled, admin_new_job_phone_call_and_sms_enabled,
      admin_eta_accepted_phone_call_and_sms_enabled, admin_eta_accepted_phone_sms_enabled,
    ]
  end

  let(:all_dispatchers) do
    [
      dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled, dispatcher_2_new_job_phone_call_enabled,
      dispatcher_new_job_phone_sms_enabled, dispatcher_2_new_job_phone_sms_enabled,
      dispatcher_without_phone_new_job_call_enabled, dispatcher_without_phone_new_job_sms_enabled,
      dispatcher_deleted_new_job_phone_call_enabled, dispatcher_deleted_new_job_phone_sms_enabled,
      broken_ex_dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled,
      dispatcher_eta_accepted_sms_enabled,
    ]
  end

  let(:users_without_notification_settings) do
    [
      admin_with_phone_notifications_disabled,
      dispatcher_with_phone_notifications_disabled,
      driver_with_phone_notifications_disabled,
    ]
  end

  let(:rescue_company) { create :rescue_company, name: 'ABC Towing', users: [] }
  let(:rescue_company_users_check) { true }

  # make sure all expected users are associated with the company before running our specs;
  # can be disabled if rescue_company_users_check == false
  it 'creates all users for rescue_company' do
    if rescue_company_users_check
      rescue_company.reload

      expect(rescue_company.users).to match_array(all_admins + all_dispatchers + users_without_notification_settings)
    end
  end
end
