# frozen_string_literal: true

require_relative './users_for_notification_settings'

RSpec.describe Notification::JobAssignedWorker do
  include_context "users for notification settings"

  subject(:run_worker) do
    Notification::JobAssignedWorker.new.perform(job.id)
  end

  let(:job) { create :fleet_motor_club_job, status: :assigned, service_code: service_code, account: account, rescue_company: rescue_company }
  let(:service_code) { create(:service_code, name: ServiceCode::BATTERY_JUMP) }
  let(:account) { create(:account, name: 'MotorClub Demo', company: rescue_company) }

  it 'triggers phone notifications with the expected amount of times' do
    expect(Job).to receive(:new_job_call).exactly(3).times
    expect(Job).to receive(:send_sms).exactly(4).times

    run_worker
  end

  it 'triggers the correct phone notifications for the correct users' do
    allow(Job).to receive(:new_job_call)
    allow(Job).to receive(:send_sms)

    run_worker

    expect(Job).to have_received(:new_job_call).with(admin_new_job_phone_call_and_sms_enabled.phone)
    expect(Job).to have_received(:new_job_call).with(dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled.phone)
    expect(Job).to have_received(:new_job_call).with(dispatcher_2_new_job_phone_call_enabled.phone)

    expect(Job).to have_received(:send_sms).with(
      admin_new_job_phone_sms_enabled.phone,
      Notifiable::JobAssigned.build_sms_msg(job),
      user_id: admin_new_job_phone_sms_enabled.id,
      job_id: job.id,
      type: 'JobAuditSms'
    )

    expect(Job).to have_received(:send_sms).with(
      admin_new_job_phone_call_and_sms_enabled.phone,
      Notifiable::JobAssigned.build_sms_msg(job),
      user_id: admin_new_job_phone_call_and_sms_enabled.id,
      job_id: job.id,
      type: 'JobAuditSms'
    )

    expect(Job).to have_received(:send_sms).with(
      dispatcher_new_job_phone_sms_enabled.phone,
      Notifiable::JobAssigned.build_sms_msg(job),
      user_id: dispatcher_new_job_phone_sms_enabled.id,
      job_id: job.id,
      type: 'JobAuditSms'
    )

    expect(Job).to have_received(:send_sms).with(
      dispatcher_2_new_job_phone_sms_enabled.phone,
      Notifiable::JobAssigned.build_sms_msg(job),
      user_id: dispatcher_2_new_job_phone_sms_enabled.id,
      job_id: job.id,
      type: 'JobAuditSms'
    )
  end
end
