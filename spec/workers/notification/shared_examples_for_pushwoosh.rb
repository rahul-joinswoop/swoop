# frozen_string_literal: true

# Must implement title, msg, link, user_ids in requiring spec
RSpec.shared_examples "sends to pushwoosh" do
  it "calls External::Pushwoosh" do
    expect(Rollbar).not_to receive(:error)
    expect(External::Pushwoosh)
      .to receive(:create_message)
      .with(user_ids: user_ids.sort, title: title, msg: msg, link: link)
      .once
    subject
  end
end

RSpec.shared_examples "doesn't send to pushwoosh" do
  it "doesn't call External::Pushwoosh" do
    expect(Rollbar).not_to receive(:error)
    expect(External::Pushwoosh)
      .not_to receive(:create_message)
    subject
  end
end
