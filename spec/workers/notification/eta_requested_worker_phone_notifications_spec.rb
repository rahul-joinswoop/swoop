# frozen_string_literal: true

require_relative './users_for_notification_settings'

RSpec.describe Notification::EtaRequestedWorker do
  describe "send_phone_notification" do
    include_context "users for notification settings"

    let(:service_code) { create(:service_code, name: ServiceCode::BATTERY_JUMP) }

    shared_examples 'triggers the correct notifications to the correct users' do |expected_client_company_name|
      it 'triggers phone notifications the expected amount of times' do
        expect(Job).to receive(:new_job_call).exactly(3).times
        expect(Job).to receive(:send_sms).exactly(4).times

        run_worker
      end

      it 'triggers the correct phone notifications for the correct users' do
        allow(Job).to receive(:new_job_call)
        allow(Job).to receive(:send_sms)

        run_worker

        expect(Job).to have_received(:new_job_call).with(admin_new_job_phone_call_and_sms_enabled.phone)
        expect(Job).to have_received(:new_job_call).with(dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled.phone)
        expect(Job).to have_received(:new_job_call).with(dispatcher_2_new_job_phone_call_enabled.phone)

        expect(Job).to have_received(:send_sms).with(
          admin_new_job_phone_sms_enabled.phone,
          Notifiable::EtaRequested.build_sms_msg(job),
          user_id: admin_new_job_phone_sms_enabled.id,
          job_id: job.id,
          type: 'JobAuditSms'
        )

        expect(Job).to have_received(:send_sms).with(
          admin_new_job_phone_call_and_sms_enabled.phone,
          Notifiable::EtaRequested.build_sms_msg(job),
          user_id: admin_new_job_phone_call_and_sms_enabled.id,
          job_id: job.id,
          type: 'JobAuditSms'
        )

        expect(Job).to have_received(:send_sms).with(
          dispatcher_new_job_phone_sms_enabled.phone,
          Notifiable::EtaRequested.build_sms_msg(job),
          user_id: dispatcher_new_job_phone_sms_enabled.id,
          job_id: job.id,
          type: 'JobAuditSms'
        )

        expect(Job).to have_received(:send_sms).with(
          dispatcher_2_new_job_phone_sms_enabled.phone,
          Notifiable::EtaRequested.build_sms_msg(job),
          user_id: dispatcher_2_new_job_phone_sms_enabled.id,
          job_id: job.id,
          type: 'JobAuditSms'
        )
      end
    end

    context 'when it is a MotorClub job' do
      subject(:run_worker) do
        Notification::EtaRequestedWorker.new.perform(job.id)
      end

      let(:job) { create(:fleet_motor_club_job, service_code: service_code, rescue_company: rescue_company) }

      it_behaves_like 'triggers the correct notifications to the correct users', 'MotorClub Demo'
    end

    context 'when it is a FleetManaged job' do
      context 'when job has auction' do
        subject(:run_worker) do
          Notification::EtaRequestedWorker.new.perform(nil, job.bids.first.id)
        end

        let(:job) do
          job = create(:fleet_managed_job, :with_auction, bids: 2, service_code: service_code, rescue_company: nil)

          job.bids.first.update! company: rescue_company

          job
        end

        it_behaves_like 'triggers the correct notifications to the correct users', 'Swoop'
      end

      context 'when job does not have auction' do
        subject(:run_worker) { Notification::EtaRequestedWorker.new.perform(job.id) }

        let(:job) { create(:fleet_managed_job, service_code: service_code, rescue_company: rescue_company) }

        it_behaves_like 'triggers the correct notifications to the correct users', 'Swoop'
      end
    end
  end
end
