# frozen_string_literal: true

require 'rails_helper'
require_relative "./shared_examples_for_pushwoosh"

RSpec.describe Notification::GeofenceCompletedWorker do
  let(:title) { Notifiable::GeofenceCompleted.build_title(job) }
  let(:msg) { Notifiable::GeofenceCompleted.build_msg(departed_location) }
  let(:link) { Notifiable::GeofenceCompleted.build_link(job) }
  let(:user_ids) { [job.rescue_driver.to_ssid] }

  shared_context "with a job" do
    subject { described_class.new.perform(job.id, departed_location) }

    context "with a driver" do
      let!(:job) { create :rescue_job, :with_rescue_driver, :with_account }

      it_behaves_like "sends to pushwoosh"

      context "without a driver" do
        let!(:job) { create :rescue_job, :with_account }

        it_behaves_like "doesn't send to pushwoosh"
      end
    end
  end

  context 'leaving Drop off' do
    let(:departed_location) { 'Drop Off' }
    let(:msg) { 'Job marked Completed because you left Drop Off' }

    it_behaves_like 'with a job'
  end

  context 'leaving Pickup' do
    let(:departed_location) { 'Pickup' }
    let(:msg) { 'Job marked Completed because you left Pickup' }

    it_behaves_like 'with a job'
  end
end
