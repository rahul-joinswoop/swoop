# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notification::JobDispatchedWorker do
  let(:title) { Notifiable::JobDispatched.build_title(job) }
  let(:msg) { Notifiable::JobDispatched.build_msg }
  let(:link) { Notifiable::JobDispatched.build_link(job) }
  let(:user_ids) { [job.rescue_driver.to_ssid].sort }

  shared_examples "send push notification" do
    it "calls External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .to receive(:create_message)
        .with(user_ids: user_ids, title: title, msg: msg, link: link)
        .once

      run_worker
    end
  end

  shared_examples 'send phone notification' do
    before do
      allow(Job).to receive(:new_job_call)
      allow(Job).to receive(:send_sms)
    end

    shared_examples 'trigger sms texting to user phone and secondary phone' do
      it "triggers the correct phone notification" do
        run_worker

        expect(Job).to have_received(:send_sms).with(
          rescue_driver.phone,
          Notifiable::JobDispatched.build_sms_msg(job),
          user_id: rescue_driver.id,
          job_id: job.id,
          type: 'JobAuditSms'
        )

        expect(Job).to have_received(:send_sms).with(
          rescue_driver.pager,
          Notifiable::JobDispatched.build_sms_msg(job),
          user_id: rescue_driver.id,
          job_id: job.id,
          type: 'JobAuditSms'
        )
      end
    end

    shared_examples 'do not trigger sms texting to user phone or secondary phone' do
      it "triggers the correct phone notification" do
        run_worker

        expect(Job).not_to have_received(:send_sms).with(
          rescue_driver.phone,
          Notifiable::JobDispatched.build_sms_msg(job),
          user_id: rescue_driver.id,
          job_id: job.id,
          type: 'JobAuditSms'
        )

        expect(Job).not_to have_received(:send_sms).with(
          rescue_driver.pager,
          Notifiable::JobDispatched.build_sms_msg(job),
          user_id: rescue_driver.id,
          job_id: job.id,
          type: 'JobAuditSms'
        )
      end
    end

    shared_examples 'trigger phone call to user phone and secondary phone' do
      it "triggers the correct phone notification" do
        run_worker

        expect(Job).to have_received(:new_job_call).with(rescue_driver.phone)
        expect(Job).to have_received(:new_job_call).with(rescue_driver.pager)
      end
    end

    shared_examples 'do not trigger phone call to user phone or secondary phone' do
      it "triggers the correct phone notification" do
        run_worker

        expect(Job).not_to have_received(:new_job_call).with(rescue_driver.phone)
        expect(Job).not_to have_received(:new_job_call).with(rescue_driver.pager)
      end
    end

    context 'with Call and Text enabled' do
      let(:dispatched_to_driver) { ['Call', 'Text'] }

      it_behaves_like 'trigger sms texting to user phone and secondary phone'
      it_behaves_like 'trigger phone call to user phone and secondary phone'
    end

    context 'with only Call enabled' do
      let(:dispatched_to_driver) { ['Call'] }

      it_behaves_like 'trigger phone call to user phone and secondary phone'
      it_behaves_like 'do not trigger sms texting to user phone or secondary phone'
    end

    context 'with only Text enabled' do
      let(:dispatched_to_driver) { ['Text'] }

      it_behaves_like 'trigger sms texting to user phone and secondary phone'
      it_behaves_like 'do not trigger phone call to user phone or secondary phone'
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .not_to receive(:create_message)
      run_worker
    end
  end

  context "with a job" do
    subject(:run_worker) { Notification::JobDispatchedWorker.new.perform(job.id) }

    context "with a dispatched job" do
      let(:job) { create :rescue_job, rescue_company: rescue_company, rescue_driver: rescue_driver, status: :dispatched }
      let(:rescue_company) { create :rescue_company }

      context 'with a driver' do
        let(:rescue_driver) do
          create(
            :user,
            roles: [:rescue, :driver],
            phone: '+1405111225222',
            pager: '+1409444762999',
            notification_settings: [{ dispatched_to_driver: dispatched_to_driver }],
            company: rescue_company
          )
        end

        let(:dispatched_to_driver) { [] }

        it_behaves_like "send push notification"
        it_behaves_like "send phone notification"
      end

      context "without a driver" do
        let!(:job) { create :rescue_job, status: :dispatched }

        it_behaves_like "it does nothing"
      end
    end
  end
end
