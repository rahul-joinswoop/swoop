# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notification::JobServiceTypeChangedWorker do
  let(:service_code) { create :service_code, name: ServiceCode::DOLLY }
  let(:user_ids) { [job.rescue_driver.to_ssid].sort }
  let(:title) { Notifiable::JobServiceTypeChanged.build_title(job) }
  let(:msg) { Notifiable::JobServiceTypeChanged.build_msg }
  let(:link) { Notifiable::JobServiceTypeChanged.build_link(job) }

  shared_examples "it works" do
    it "calls External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .to receive(:create_message)
        .with(user_ids: user_ids, title: title, msg: msg, link: link)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .not_to receive(:create_message)
      subject
    end
  end

  context "with a job" do
    subject { Notification::JobServiceTypeChangedWorker.new.perform(job.id) }

    context "with a job, service_code, and a driver" do
      let!(:job) do
        create :rescue_job,
               :with_rescue_driver,
               :with_account,
               service_code: service_code
      end

      it_behaves_like "it works"

      context "without a driver" do
        let!(:job) do
          create :rescue_job,
                 :with_account,
                 service_code: service_code
        end

        it_behaves_like "it does nothing"
      end
    end
  end
end
