# frozen_string_literal: true

##
#
# note: specs to phone notification (sms and call) are found in eta_requested_worker_phone_notifications_spec file
#
##
require 'rails_helper'
RSpec.describe Notification::EtaRequestedWorker do
  let(:rescue_company) { create :rescue_company }
  let(:driver) { job.rescue_driver }
  let(:msg) { Notifiable::EtaRequested.build_msg }
  let(:title) { Notifiable::EtaRequested.build_title(job) }
  let(:link) { Notifiable::EtaRequested.build_link(job) }

  shared_examples "it works" do
    it "calls External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .to receive(:create_message)
        .with(user_ids: user_ids, title: title, msg: msg, link: link)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .not_to receive(:create_message)
      subject
    end
  end

  context "with a job" do
    subject { Notification::EtaRequestedWorker.new.perform(job.id) }

    let!(:dispatcher) { create :user, :dispatcher, company: rescue_company }
    let!(:admin) { create :user, :admin, company: rescue_company }
    let(:user_ids) { [dispatcher.to_ssid, admin.to_ssid].sort }

    context "with an accepted job and a rescue company" do
      let!(:job) do
        create :fleet_motor_club_job,
               :with_account,
               status: :accepted,
               rescue_company: rescue_company
      end

      it_behaves_like "it works"

      context "without a dispatcher or admin" do
        let!(:admin)   { nil }
        let!(:dispatcher) { nil }

        it_behaves_like "it does nothing"
      end
    end
  end

  context "with an auction" do
    subject { Notification::EtaRequestedWorker.new.perform(nil, bid.id) }

    let!(:dispatcher) { create :user, :dispatcher, company: rescue_company }
    let!(:admin) { create :user, :admin, company: rescue_company }
    let(:user_ids) { [dispatcher.to_ssid, admin.to_ssid].sort }

    let(:job) do
      create :fleet_managed_job,
             :with_auction,
             status: :assigned,
             bids: 3
    end

    let!(:bid) do
      bid = create :bid, job: job, company: rescue_company, status: Auction::Bid::REQUESTED
      job.auction.bids << bid
      job.auction.update! status: Auction::Auction::LIVE
      bid
    end

    it_behaves_like "it works"
  end
end
