# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notification::FleetInHouseJobAssignedWorker do
  let(:rescue_company) { create :rescue_company }
  let(:driver) { job.rescue_driver }
  let!(:dispatcher) { create :user, :dispatcher, company: rescue_company }
  let!(:admin) { create :user, :admin, company: rescue_company }
  let(:user_ids) { [dispatcher.to_ssid, admin.to_ssid].sort }
  let(:title) { Notifiable::FleetInHouseJobAssigned.build_title(job) }
  let(:msg) { Notifiable::FleetInHouseJobAssigned.build_msg }
  let(:link) { Notifiable::FleetInHouseJobAssigned.build_link(job) }

  shared_examples "it works" do
    it "calls External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .to receive(:create_message)
        .with(user_ids: user_ids.sort, title: title, msg: msg, link: link)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .not_to receive(:create_message)
      subject
    end
  end

  context "with a job" do
    subject { Notification::FleetInHouseJobAssignedWorker.new.perform(job.id) }

    context "with an accepted job and a rescue company" do
      let!(:job) do
        create :fleet_in_house_job,
               :with_account,
               status: :assigned,
               rescue_company: rescue_company
      end

      it_behaves_like "it works"

      context "without a dispatcher or admin" do
        let!(:admin)   { nil }
        let!(:dispatcher) { nil }

        it_behaves_like "it does nothing"
      end
    end
  end
end
