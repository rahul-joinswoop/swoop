# frozen_string_literal: true

require 'rails_helper'
require_relative "./shared_examples_for_pushwoosh"

RSpec.describe Notification::GeofenceOnsiteWorker do
  let(:title) { Notifiable::GeofenceOnsite.build_title(job) }
  let(:msg) { Notifiable::GeofenceOnsite.build_msg }
  let(:link) { Notifiable::GeofenceOnsite.build_link(job) }
  let(:user_ids) { [job.rescue_driver.to_ssid] }

  context "with a job" do
    subject { described_class.new.perform(job.id) }

    context "with a driver" do
      let!(:job) { create :rescue_job, :with_rescue_driver, :with_account }

      it_behaves_like "sends to pushwoosh"

      context "without a driver" do
        let!(:job) { create :rescue_job, :with_account }

        it_behaves_like "doesn't send to pushwoosh"
      end
    end
  end
end
