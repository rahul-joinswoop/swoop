# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notification::JobCanceledWorker do
  let(:rescue_company) { create :rescue_company }
  let(:rescue_driver) { create :user, :rescue, :driver, company: rescue_company }
  let!(:dispatchers) { create_list :user, 3, :rescue, :dispatcher, company: rescue_company }
  let(:user_ids) { [job.rescue_driver, *dispatchers].uniq.reject(&:blank?).map(&:to_ssid).sort }
  let(:title) { Notifiable::JobCanceled.build_title(job) }
  let(:msg) { Notifiable::JobCanceled.build_msg }
  let(:dispatch_jobs_link) { Notifiable::JobCanceled.dispatch_jobs_link }
  let(:my_jobs_link) { Notifiable::JobCanceled.my_jobs_link }

  def expect_push_with(options)
    defaults = { title: title, msg: msg }
    expect(External::Pushwoosh).to receive(:create_message).with(defaults.merge(options))
  end

  shared_examples "it works" do
    it "calls External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)

      rescue_ssids = Array(rescue_driver.to_ssid).sort
      dispatcher_ssids = (dispatchers.map(&:to_ssid).sort - rescue_ssids).sort

      expect_push_with(user_ids: rescue_ssids, link: my_jobs_link).once

      if dispatcher_ssids.present?
        expect_push_with(user_ids: dispatcher_ssids, link: dispatch_jobs_link).once
      end

      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .not_to receive(:create_message)
      subject
    end
  end

  context "with a job" do
    subject { Notification::JobCanceledWorker.new.perform(job.id) }

    context "that is canceled and a rescue company" do
      let!(:job) do
        create :rescue_job,
               :with_account,
               rescue_driver: rescue_driver,
               rescue_company: rescue_company,
               status: :canceled
      end

      it_behaves_like "it works"

      context "when a dispatcher is the driver" do
        let(:rescue_driver) { dispatchers.last }
        let!(:dispatchers) do
          d = create_list :user, 3, :rescue, :dispatcher, company: rescue_company
          d.last.roles << create(:role, name: Role::DRIVER)
          d
        end

        it_behaves_like "it works"

        it 'sends a my_jobs link to that driver' do
          driver_ssids = [rescue_driver.to_ssid].sort
          dispatcher_ssids = (dispatchers.map(&:to_ssid).sort - driver_ssids).sort

          expect_push_with(user_ids: driver_ssids, link: my_jobs_link)
          expect_push_with(user_ids: dispatcher_ssids, link: dispatch_jobs_link)

          subject
        end
      end

      context "without a driver, dispatcher or admin" do
        let!(:dispatchers) { [] }
        let!(:rescue_driver) { nil }

        it_behaves_like "it does nothing"
      end

      context "without a rescue_company" do
        let!(:job) do
          create :job,
                 :with_account,
                 rescue_driver: nil,
                 rescue_company: nil,
                 status: :canceled
        end

        it_behaves_like "it does nothing"
      end
    end
  end
end
