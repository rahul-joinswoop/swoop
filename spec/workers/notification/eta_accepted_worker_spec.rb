# frozen_string_literal: true

require_relative './users_for_notification_settings'

RSpec.describe Notification::EtaAcceptedWorker do
  include_context "users for notification settings"

  let(:user_ids) { rescue_company.dispatchers.map(&:to_ssid).sort }
  let(:msg) { Notifiable::EtaAccepted.build_msg }
  let(:title) { Notifiable::EtaAccepted.build_title(job) }
  let(:link) { Notifiable::EtaAccepted.build_link(job) }

  shared_examples "it works" do
    it "calls External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .to receive(:create_message)
        .with(user_ids: user_ids, title: title, msg: msg, link: link)
        .once
      subject
    end

    it 'triggers phone notifications the expected amount of times' do
      expect(Job).to receive(:new_job_call).twice
      expect(Job).to receive(:send_sms).exactly(4).times

      run_worker
    end

    it 'triggers the correct phone notifications for the correct users' do
      allow(Job).to receive(:new_job_call)
      allow(Job).to receive(:send_sms)

      run_worker

      expect(Job).to have_received(:new_job_call).with(admin_eta_accepted_phone_call_and_sms_enabled.phone)
      expect(Job).to have_received(:new_job_call).with(dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled.phone)

      expect(Job).to have_received(:send_sms).with(
        admin_eta_accepted_phone_sms_enabled.phone,
        Notifiable::EtaAccepted.build_sms_msg(job),
        user_id: admin_eta_accepted_phone_sms_enabled.id,
        job_id: job.id,
        type: 'JobAuditSms'
      )

      expect(Job).to have_received(:send_sms).with(
        admin_eta_accepted_phone_call_and_sms_enabled.phone,
        Notifiable::EtaAccepted.build_sms_msg(job),
        user_id: admin_eta_accepted_phone_call_and_sms_enabled.id,
        job_id: job.id,
        type: 'JobAuditSms'
      )

      expect(Job).to have_received(:send_sms).with(
        dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled.phone,
        Notifiable::EtaAccepted.build_sms_msg(job),
        user_id: dispatcher_new_job_phone_call_enabled_and_eta_accepted_call_and_sms_enabled.id,
        job_id: job.id,
        type: 'JobAuditSms'
      )

      expect(Job).to have_received(:send_sms).with(
        dispatcher_eta_accepted_sms_enabled.phone,
        Notifiable::EtaAccepted.build_sms_msg(job),
        user_id: dispatcher_eta_accepted_sms_enabled.id,
        job_id: job.id,
        type: 'JobAuditSms'
      )
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call External::Pushwoosh" do
      expect(Rollbar).not_to receive(:error)
      expect(External::Pushwoosh)
        .not_to receive(:create_message)
      subject
    end

    it "does not send sms notification" do
      expect(Job).not_to receive(:new_job_call)
    end

    it "does not send call notification" do
      expect(Job).not_to receive(:send_sms)
    end
  end

  context "with a job" do
    subject(:run_worker) { Notification::EtaAcceptedWorker.new.perform(job.id) }

    context "with an accepted job and a rescue company" do
      let!(:job) { create :fleet_motor_club_job, account: account, status: :accepted, rescue_company: rescue_company }
      let(:account) { create(:account, name: 'MotorClub Demo', company: rescue_company) }

      it_behaves_like "it works"

      context "without an account" do
        let!(:job) { create :fleet_motor_club_job, account: nil, status: :accepted, rescue_company: rescue_company }

        it_behaves_like "it works"
      end

      context "without users" do
        before do
          rescue_company.users.delete_all
        end

        it_behaves_like "it does nothing"
      end
    end
  end
end
