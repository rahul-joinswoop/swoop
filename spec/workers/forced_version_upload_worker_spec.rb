# frozen_string_literal: true

require 'rails_helper'

describe ForcedVersionUploadWorker do
  subject(:worker_perform) do
    ForcedVersionUploadWorker.new.perform(version.id)
  end

  let(:version) do
    create :version, description: '1.2.3', name: 'android', version: '665', forced: true
  end

  it 'uploads it to S3' do
    expect_any_instance_of(S3Uploader).to receive(:upload).with(version.new_forced_version_hash.to_json)

    worker_perform
  end
end
