# frozen_string_literal: true

require 'rails_helper'

describe Fmc360ReportWorker do
  let(:report) { Fmc360ReportWorker.new }
  let(:lincoln) { create(:fleet_company, :lincoln) }
  let(:warranty_program) { create(:client_program, company: lincoln, code: "lincoln_warranty") }
  let(:esp_program) { create(:client_program, company: lincoln, code: "lincoln_esp_00001") }
  let(:lincoln_for_life_program) { create(:client_program, company: lincoln, code: "lincoln_for_life") }
  let(:black_label_program) { create(:client_program, company: lincoln, code: "lincoln_black_label") }

  describe "service_code_id" do
    it "classifies jobs with the correct service codes" do
      expect(report.service_code_id(Job.new(service_code: ServiceCode.new(name: ServiceCode::LOCK_OUT)))).to eq "CCM003"
      expect(report.service_code_id(Job.new(service_code: ServiceCode.new(name: ServiceCode::BATTERY_JUMP)))).to eq "CCM004"
      expect(report.service_code_id(Job.new(service_code: ServiceCode.new(name: ServiceCode::TIRE_CHANGE)))).to eq "CCM005"
      expect(report.service_code_id(Job.new(service_code: ServiceCode.new(name: ServiceCode::FUEL_DELIVERY)))).to eq "CCM006"
      expect(report.service_code_id(Job.new(service_code: ServiceCode.new(name: ServiceCode::WINCH_OUT)))).to eq "CCM007"

      wheel_lift = RescueVehicle.new(vehicle_category: VehicleCategory.new(name: VehicleCategory::WHEEL_LIFT))
      expect(report.service_code_id(Job.new(service_code: ServiceCode.new(name: ServiceCode::TOW), rescue_vehicle: wheel_lift))).to eq "CCM002"
      expect(report.service_code_id(Job.new(service_code: ServiceCode.new(name: ServiceCode::ACCIDENT_TOW)))).to eq "CCM001"
      expect(report.service_code_id(Job.new(service_code: ServiceCode.new(name: ServiceCode::ACCIDENT_TOW), rescue_vehicle: wheel_lift))).to eq "CCM002"
    end
  end

  describe "program_code_id" do
    it "classifies jobs with the correct program code id's" do
      job_1 = create(:job, :completed, fleet_company: lincoln)
      job_1.pcc_coverage = build(:pcc_coverage, :lincoln, client_program: warranty_program)
      expect(report.program_code_id(job_1)).to eq "1415"

      job_2 = create(:job, :completed, fleet_company: lincoln)
      job_2.pcc_coverage = build(:pcc_coverage, :lincoln, client_program: esp_program)
      expect(report.program_code_id(job_2)).to eq "1416"

      job_3 = create(:job, :completed, fleet_company: lincoln)
      job_3.pcc_coverage = build(:pcc_coverage, :lincoln, client_program: warranty_program)
      expect(report.program_code_id(job_3)).to eq "1415"

      job_4 = create(:job, :completed, fleet_company: lincoln)
      job_4.pcc_coverage = build(:pcc_coverage, :lincoln, client_program: black_label_program)
      expect(report.program_code_id(job_4)).to eq "1415"
    end
  end

  describe "generate_report" do
    it "generates a report for current jobs and yield it as an unread stream", freeze_time: Time.parse("2019-01-21T12:01:05-0500") do
      job_1 = Timecop.travel(25.hours.ago) { create(:job, :completed, fleet_company: lincoln) }
      job_2 = create(:job, :completed, fleet_company: lincoln)
      job_3 = create(:job, :goa, fleet_company: lincoln)
      job_4 = create(:job, :completed, fleet_company: create(:fleet_company, name: "Other"))
      job_5 = create(:job, status: "Towing", fleet_company: lincoln)
      job_6 = Timecop.travel(25.hours.from_now) { create(:job, :completed, fleet_company: lincoln) }
      job_7 = create(:job, :completed, fleet_company: lincoln)

      [job_1, job_2, job_4, job_5, job_6].each do |job|
        job.pcc_coverage = build(:pcc_coverage, :lincoln, result: PCC::Coverage::COVERED_RESULT)
        job.save!
      end
      job_3.pcc_coverage = build(:pcc_coverage, :lincoln, result: PCC::Coverage::PARTIAL_COVERAGE_RESULT)
      job_3.save!
      job_7.pcc_coverage = build(:pcc_coverage, :lincoln, result: PCC::Coverage::NOT_COVERED_RESULT)
      job_7.save!

      data = nil
      report.generate_report(Time.now.to_date) do |stream|
        data = stream.read.chomp
      end
      lines = data.split("\n")
      expect(lines.shift).to eq "HSWOOP               2019-01-2112:01:05  0000000001"
      expect(lines.pop).to eq "TSWOOP               0000000002"
      expect(lines.map { |line| line[0, 10].to_i }).to match_array([job_2.id, job_3.id])
    end
  end

  describe "perform" do
    it "generates and send the report to an sftp server" do
      stream = StringIO.new("data")
      expect(report).to receive(:generate_report).with(Date.new(2019, 1, 21)).and_yield(stream)
      expect_any_instance_of(SftpServer).to receive(:upload).with(stream, "incoming/Agero-LincolnRoadSideAssistance.2019-01-21.txt")

      ClimateControl.modify("LINCOLN_SFTP_SERVER_URI": "sftp://user@localhost", "LINCOLN_SFTP_PASSWORD": "foo") do
        report.perform("2019-01-21")
      end
    end
  end
end
