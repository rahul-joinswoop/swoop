# frozen_string_literal: true

require 'rails_helper'

describe AuditChangedCompanySubscriptionStatusWorker, type: :worker do
  let(:company) do
    create(:company, subscription_status_id: company_subscription_status_id)
  end

  let(:company_subscription_status_id) do
    Company::DEFAULT_SUBSCRIPTION_STATUS_ID
  end

  let(:audit_subscription_status_id) do
    Company::DEFAULT_SUBSCRIPTION_STATUS_ID
  end

  let(:company_id) { company.id }

  describe '#perform' do
    context 'with a company' do
      context 'without an audit record' do
        it 'creates a new audit record' do
          expect do
            AuditChangedCompanySubscriptionStatusWorker
              .new
              .perform(company_id)
          end.to change(AuditCompanySubscriptionStatus, :count).by(1)
        end
      end

      context 'with a nil company.subscription_status_id' do
        let(:company_subscription_status_id) { nil }

        it 'does not create a new audit record' do
          expect do
            AuditChangedCompanySubscriptionStatusWorker
              .new
              .perform(company_id)
          end.to change(AuditCompanySubscriptionStatus, :count).by(0)
        end
      end

      context 'with an audit record' do
        before do
          create(
            :audit_company_subscription_status,
            company_id: company_id,
            subscription_status_id: audit_subscription_status_id
          )
        end

        context 'with a nil company.subscription_status_id' do
          let(:company_subscription_status_id) { nil }

          it 'does not create a new audit record' do
            expect do
              AuditChangedCompanySubscriptionStatusWorker
                .new
                .perform(company_id)
            end.to change(AuditCompanySubscriptionStatus, :count).by(0)
          end
        end

        context 'the company subscription changed' do
          let!(:company_subscription_status_id) { 2 }

          it 'creates a new audit record' do
            expect do
              AuditChangedCompanySubscriptionStatusWorker
                .new
                .perform(company_id)
            end.to change(AuditCompanySubscriptionStatus, :count).by(1)
          end
        end

        context 'the company subscription has not changed' do
          it 'does not create a new audit record' do
            expect do
              AuditChangedCompanySubscriptionStatusWorker
                .new
                .perform(company_id)
            end.to change(AuditCompanySubscriptionStatus, :count).by(0)
          end
        end
      end
    end
  end
end
