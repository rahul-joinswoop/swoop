# frozen_string_literal: true

require 'rails_helper'

describe LatLngResolver do
  subject(:worker_perform) { described_class.new.perform(location.id) }

  let(:location) do
    create(
      :location,
      address: address,
      lat: nil,
      lng: nil
    )
  end

  let(:address) { '130 Pasetta Drive, Santa Clara, Santa Clara County, California 95050-3245' }

  it 'adds fetched lat/lng from google api to the given location', vcr: true do
    worker_perform

    location.reload

    expect(location.lat).not_to be_nil
    expect(location.lng).not_to be_nil
  end

  context 'when google returns no results' do
    let(:address) { '2 asdfasdfasdf ave' }

    it 'keeps lat/lng as null', vcr: true do
      worker_perform

      location.reload

      expect(location.lat).to be_nil
      expect(location.lng).to be_nil
    end
  end
end
