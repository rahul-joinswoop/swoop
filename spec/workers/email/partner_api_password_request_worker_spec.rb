# frozen_string_literal: true

require 'rails_helper'

describe Email::PartnerAPIPasswordRequestWorker do
  subject { described_class.new.perform(password_request.id) }

  let(:password_request) { create :partner_api_password_request }
  let(:args) do
    {
      template_name: 'partner_api_password_request',
      to: password_request.user_email,
      from: 'Swoop <hello@joinswoop.com>',
      subject: "Invoicing Agero (Swoop) Jobs",
      locals: {
        :@ageroSwoopLogo => true,
        :@password_request => password_request,
        :@app => password_request.application,
        :@smtp_api_values => { "filters.clicktrack.settings.enable" => 0 },
      },
    }
  end

  it "sends email" do
    expect(SystemMailer)
      .to receive(:send_mail)
      .once
      .with(args, nil, nil, nil, 'Password Request', password_request)
      .and_call_original

    expect { subject }
      .to change { ActionMailer::Base.deliveries.count }
      .by(1)
  end

  shared_examples "it doesn't send email" do
    it "works" do
      expect(SystemMailer)
        .not_to receive(:send_mail)

      expect { subject }
        .not_to change { ActionMailer::Base.deliveries.count }
    end
  end

  context 'with an invalid request' do
    let(:password_request) { create :partner_api_password_request, valid_request: false }

    it_behaves_like "it doesn't send email"
  end
end
