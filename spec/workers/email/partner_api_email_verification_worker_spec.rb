# frozen_string_literal: true

require 'rails_helper'

describe Email::PartnerAPIEmailVerificationWorker do
  subject { described_class.new.perform(email_verification.id) }

  let(:email_verification) { create :partner_api_email_verification }
  let(:args) do
    {
      template_name: 'partner_api_email_verification',
      to: email_verification.user_email,
      from: 'Swoop <hello@joinswoop.com>',
      subject: "Verify Swoop Email",
      locals: {
        :@ageroSwoopLogo => true,
        :@email_verification => email_verification,
        :@app => email_verification.application,
        :@smtp_api_values => { "filters.clicktrack.settings.enable" => 0 },
      },
    }
  end

  it "sends email" do
    expect(SystemMailer)
      .to receive(:send_mail)
      .once
      .with(args, nil, nil, nil, 'Email Verification', email_verification)
      .and_call_original

    expect { subject }
      .to change { ActionMailer::Base.deliveries.count }
      .by(1)
  end

  shared_examples "it doesn't send email" do
    it "works" do
      expect(SystemMailer)
        .not_to receive(:send_mail)

      expect { subject }
        .not_to change { ActionMailer::Base.deliveries.count }
    end
  end

  context 'without a user_email' do
    let(:email_verification) do
      create :partner_api_email_verification, user: create(:user, email: nil)
    end

    it_behaves_like "it doesn't send email"
  end

  context 'with an invalid request' do
    let(:email_verification) { create :partner_api_email_verification, valid_request: false }

    it_behaves_like "it doesn't send email"
  end
end
