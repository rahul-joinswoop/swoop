# frozen_string_literal: true

require "rails_helper"

describe PublishCompanyWorker do
  describe "#perform" do
    subject(:publish_worker_perform) { PublishCompanyWorker.new.perform(company_id, 'Test') }

    let(:company_id) { company.id }
    let(:company) { create(:rescue_company) }

    before do
      allow(Company).to receive_message_chain(:includes, :find_by).with(id: company_id).and_return(company)

      allow_any_instance_of(Company).to receive(:base_publish)
    end

    it 'calls Company#base_publish' do
      publish_worker_perform

      expect(company).to have_received(:base_publish).with('update')
    end

    context 'when the company_id is null' do
      let(:company_id) { nil }
      let(:company) { nil }

      it 'does not call Company#base_publish' do
        expect_any_instance_of(Company).not_to receive(:base_publish).with('update')

        publish_worker_perform
      end
    end
  end
end
