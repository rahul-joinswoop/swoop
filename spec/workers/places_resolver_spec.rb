# frozen_string_literal: true

require "rails_helper"

# PCC Service instantiates internal classes based on the FleetCompany that's using the system.
# The pilot is "Farmers Insurance", so we're using it for the specs.
describe PlacesResolver, :vcr do
  describe "#extract from lat/lng resut" do
    subject(:resolver) { described_class.new }

    context "using highway with place id" do
      let(:location) do
        create(:location, {
          lat: nil,
          lng: nil,
          address: nil,
          place_id: 'EhlGIFN0LCBTYWNyYW1lbnRvLCBDQSwgVVNB',
        })
      end

      it "resolves location fully" do
        resolver.perform(location.id)
        location.reload
        expect(location[:street]).to eq('F St')
        expect(location[:street_number]).to eq('2999')
        expect(location[:street_name]).to eq('F St')
        expect(location[:city]).to eq('Sacramento')
        expect(location[:state]).to eq('CA')
        expect(location[:zip]).to eq('95816')
        expect(location[:country]).to eq('US')
        expect(location[:lat]).to eq(38.5775069)
        expect(location[:lng]).to eq(-121.4646505)
      end
    end

    context "using residential address" do
      let(:location) do
        create(:location, {
          lat: nil,
          lng: nil,
          address: nil,
          place_id: 'EjIxMTIxIEtpbGxhcm5leSBMYW5lLCBCdXJsaW5nYW1lLCBDQSwgVW5pdGVkIFN0YXRlcw',
        })
      end

      it "resolves location fully" do
        resolver.perform(location.id)
        location.reload
        expect(location[:street]).to eq('1121 Killarney Ln')
        expect(location[:street_name]).to eq('Killarney Ln')
        expect(location[:street_number]).to eq('1121')
        expect(location[:city]).to eq('Burlingame')
        expect(location[:state]).to eq('CA')
        expect(location[:zip]).to eq('94010')
        expect(location[:country]).to eq('US')
        expect(location[:lat]).to eq(37.5929781)
        expect(location[:lng]).to eq(-122.3788261)
      end
    end

    context "using cross roads" do
      let(:location) do
        create(:location, {
          lat: 33.1690852,
          lng: -117.21719310000003,
          address: 'Sycamore Ave & CA-78, Vista, CA',
        })
      end

      it "resolves location fully" do
        resolver.perform(location.id)
        location.reload
        expect(location[:street]).to eq('CA-78') # This is what google returns, doesn't breakout intersections
        expect(location[:street_number]).to be_nil
        expect(location[:street_name]).to eq('CA-78')
        expect(location[:city]).to eq('Vista')
        expect(location[:state]).to eq('CA')
        expect(location[:zip]).to eq('92083')
        expect(location[:country]).to eq('US')
        expect(location[:lat]).to eq(33.1690852)
        expect(location[:lng]).to eq(-117.2171931)
      end
    end

    context "using crossroads with place" do
      let(:location) do
        create(:location, {
          lat: 37.62808,
          lng: -122.41633999999999,
          address: 'El Camino Real & San Bruno Ave, San Bruno, CA',
          place_id: 'ChIJW0iSD9t5j4AR22daU8cFHlA',
        })
      end

      it "resolves location fully" do
        resolver.perform(location.id)
        location.reload
        expect(location[:street]).to eq('713 El Camino Real')
        expect(location[:street_number]).to eq('713')
        expect(location[:street_name]).to eq('El Camino Real')
        expect(location[:city]).to eq('San Bruno')
        expect(location[:state]).to eq('CA')
        expect(location[:zip]).to eq('94066')
        expect(location[:country]).to eq('US')
        expect(location[:lat]).to eq(37.62808)
        expect(location[:lng]).to eq(-122.41634)
      end
    end

    context "Unknown place_id" do
      let(:location) do
        create(:location, {
          lat: 33.67398,
          lng: -117.882377,
          address: '3020 Pullman Street, Costa Mesa, CA',
          place_id: 'ChIJ4cpcNQPf3IARF6HKVbjbZ-4',
        })
      end

      it "resolves location fully" do
        resolver.perform(location.id)
        location.reload
        expect(location[:street]).to eq('3020 Pullman St')
        expect(location[:street_name]).to eq('Pullman St')
        expect(location[:street_number]).to eq('3020')
        expect(location[:city]).to eq('Costa Mesa')
        expect(location[:state]).to eq('CA')
        expect(location[:zip]).to eq('92626')
        expect(location[:country]).to eq('US')
        expect(location[:lat]).to eq(33.67398)
        expect(location[:lng]).to eq(-117.882377)
      end
    end
  end
end
