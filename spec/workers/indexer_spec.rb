# frozen_string_literal: true

require 'rails_helper'

describe Indexer do
  it "indexes a record if it exist" do
    company = create(:company)
    expect(Company.__elasticsearch__.client).to receive(:index).with({
      index: Company.index_name,
      type: Company.document_type,
      id: company.id,
      body: company.as_indexed_json,
    })
    Indexer.new.perform("index", "Company", company.id)
  end

  it "does not do anything if the record does not exist" do
    expect(Company.__elasticsearch__.client).not_to receive(:index)
    Indexer.new.perform("index", "Company", 0)
  end

  it "deletes a record" do
    expect(Company.__elasticsearch__.client).to receive(:delete).with({
      index: Company.index_name,
      type: Company.document_type,
      id: 0,
    })
    Indexer.new.perform("delete", "Company", 0)
  end
end
