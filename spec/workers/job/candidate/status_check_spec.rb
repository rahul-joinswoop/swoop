# frozen_string_literal: true

require 'rails_helper'

describe Job::Candidate::StatusCheck do
  it "AutoAssign::StatusCheck is an alias of Job::Candidate::StatusCheck" do
    expect(AutoAssign::StatusCheck).to eq(described_class)
  end
end
