# frozen_string_literal: true

require "rails_helper"

describe IsscFixLogouts do
  include_context "issc provider setup"

  subject do
    IsscFixLogouts.new(connection_manager: connection_manager)
  end

  let(:contractorid) { "32498734581987" }

  let(:issc1) { create(:ago_issc, contractorid: contractorid, status: 'Registered', company: company, site: issc1_site) }
  let(:issc2) { create(:ago_issc, contractorid: contractorid, status: 'Registered', company: company, site: issc2_site) }

  let(:connection_manager) do
    double("ConnectionManager")
  end

  context "when locked" do
    it "bails" do
      expect(connection_manager).to receive(:connected?).and_return(true)
      expect(connection_manager).to receive(:logging_in_lock).and_return(true)

      subject.perform
    end
  end

  context "when two contractorids with different locations" do
    before do
      issc1.save!
      issc2.save!

      allow(connection_manager).to receive(:connected?).and_return(true)
      allow(connection_manager).to receive(:logging_in_lock).and_return(false)
      allow(connection_manager).to receive(:set_logging_in_lock).and_return(true)
    end

    context "when both in hours" do
      let(:issc1_site) { create(:site, within_hours: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: true, dispatchable: true) }

      it "logins" do
        expect(IsscLoginProvider).to receive(:perform_async).with([issc1.id, issc2.id])

        subject.perform

        expect(issc1.reload.last_login_attempted_at).not_to be_nil
        expect(issc2.reload.last_login_attempted_at).not_to be_nil
      end

      context 'rsc db row exists' do
        let!(:rsc_issc) do
          create(:ago_issc, {
            contractorid: contractorid,
            status: 'Registered',
            company: company,
            site: issc2_site,
            system: Issc::RSC_SYSTEM,
          })
        end

        it "logs in only the issc ones" do
          expect(IsscLoginProvider).to receive(:perform_async).with([issc1.id, issc2.id])

          subject.perform
        end
      end
    end

    context "when two agero sites are in a group with force open" do
      let(:issc1_site) { create(:site, within_hours: false, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, force_open: true, dispatchable: true) }

      it "logins" do
        expect(IsscLoginProvider).to receive(:perform_async).with([issc1.id, issc2.id])

        subject.perform

        expect(issc1.reload.last_login_attempted_at).not_to be_nil
        expect(issc2.reload.last_login_attempted_at).not_to be_nil
      end
    end

    context "when one agero site is in hours" do
      let(:issc1_site) { create(:site, within_hours: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, dispatchable: true) }

      it "doesn't login" do
        expect(IsscLoginProvider).not_to receive(:perform_async)

        subject.perform
        expect(issc1.reload.last_login_attempted_at).to be_nil
        expect(issc2.reload.last_login_attempted_at).to be_nil
      end
    end

    context "when one agero site is in hours and one is force open" do
      let(:issc1_site) { create(:site, within_hours: true, force_open: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, dispatchable: true) }

      it "logs both in" do
        expect(IsscLoginProvider).to receive(:perform_async).with([issc1.id, issc2.id])

        subject.perform
      end
    end

    context "when zero agero sites" do
      let(:issc1_site) { create(:site, within_hours: true) }
      let(:issc2_site) { create(:site, within_hours: false) }

      it "logins" do
        subject.perform
      end
    end

    context "when same contractorids on different sites" do
      let(:issc3) { create(:ago_issc, status: 'LoggingIn', company: company, site: issc3_site) }

      let(:issc1_site) { create(:site, within_hours: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: true, dispatchable: true) }
      let(:issc3_site) { create(:site, within_hours: true, dispatchable: true) }

      before do
        issc3.contractorid = contractorid
        issc3.save!
      end

      it "logs in sites 1 and 2 ignores the logging_in" do
        expect(IsscLoginProvider).to receive(:perform_async).with([issc1.id, issc2.id])

        subject.perform

        expect(issc1.reload.last_login_attempted_at).not_to be_nil
        expect(issc2.reload.last_login_attempted_at).not_to be_nil
      end
    end

    context "when two agero sites are in a group" do
      let(:issc1_site) { create(:site, within_hours: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: false, force_open: true, dispatchable: true) }

      it "logins" do
        expect(Issc).to receive(:find_by_sql).and_return [issc1, issc2]
        expect(Issc).to receive(:find_by_sql).and_return [issc1, issc2]
        expect(IsscLoginProvider).to receive(:perform_async).with([issc1.id, issc2.id])

        subject.perform
      end
    end

    context "recently attempted log in" do
      let(:issc1_site) { create(:site, within_hours: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: true, dispatchable: true) }

      before do
        issc1.last_login_attempted_at = Time.now
        issc1.save!
        issc2.last_login_attempted_at = Time.now
        issc2.save!
      end

      it "does not login" do
        expect(IsscLoginProvider).not_to receive(:perform_async)

        subject.perform
      end

      context 'had previously logged in' do
        before do
          issc1.logged_in_at = Time.now - 5.minutes
          issc1.save!
        end

        it 'logs in' do
          dump_table(Issc)
          expect(IsscLoginProvider).to receive(:perform_async)

          subject.perform
        end
      end
    end

    context "not recently attempted log in" do
      let(:issc1_site) { create(:site, within_hours: true, dispatchable: true) }
      let(:issc2_site) { create(:site, within_hours: true, dispatchable: true) }

      before do
        issc1.last_login_attempted_at = Time.now - 4.hours - 1.minute
        issc1.save!
        issc2.last_login_attempted_at = Time.now - 4.hours - 1.minute
        issc2.save!
      end

      it "does login" do
        expect(IsscLoginProvider).to receive(:perform_async)

        subject.perform
      end
    end
  end
end
