# frozen_string_literal: true

require 'rails_helper'

describe IsscDispatchStatus, freeze_time: true do
  let(:rescue_company) { create(:rescue_company) }
  let(:fleet_company) { create(:fleet_company, :ago) }
  let(:gco_issc) { create(:gco_issc, system: nil, company: rescue_company) }
  let(:dispatch) { create(:issc_dispatch_request, issc: gco_issc, jobid: "j1", dispatchid: "d1") }
  let(:vehicle) { create(:rescue_vehicle, lat: 45.0, lng: -118.0, location_updated_at: location_updated_at) }
  let!(:job) { create(:fleet_motor_club_job, status: "Towing", fleet_company: fleet_company, rescue_company: rescue_company, rescue_vehicle: vehicle, issc_dispatch_request: dispatch) }

  let(:status) { 'NA' }
  let(:source) { 'Automated' }
  let(:latitude) { 37.7749 }
  let(:longitude) { 122.4194 }
  let(:result_latitude) { latitude }
  let(:result_longitude) { longitude }
  let(:updated_at) { "2019-07-24T17:17:16-00:00" }
  let(:result_updated_at) { updated_at }
  let(:location_updated_at) { Time.now }
  let(:event_at) { "2019-07-24T17:17:17-00:00" }
  let(:sent_at) { "2019-07-24T17:17:18-00:00" }
  let(:others) { { lat: latitude, lng: longitude, gps_updated_at: updated_at } }
  let(:connection_manager) { ISSC::ConnectionManager.new }
  let(:client) { ISSC::DigitalDispatchClient.new('ISSC_AUTH_TOKEN') }
  let(:payload) do
    {
      ClientID: "GCO",
      DispatchID: "d1",
      ContractorID: gco_issc.contractorid,
      EventTimeUTC: DateTime.parse(event_at).utc,
      SentTimeUTC: DateTime.parse(sent_at).utc,
      StatusType: status,
      Source: source,
      Latitude: result_latitude,
      Longitude: result_longitude,
      GPSLastUpdateTimeUTC: result_updated_at,
    }
  end

  before(:each) do
    allow(connection_manager).to receive(:ensure_connected).and_yield(client)
    expect(client).to receive(:dispatchstatus).with(job, payload.compact)
  end

  context "when status is NOT 'GPS'" do
    let(:result_latitude) { job.rescue_vehicle[:lat] }
    let(:result_longitude) { job.rescue_vehicle[:lng] }
    let(:result_updated_at) { job.rescue_vehicle[:location_updated_at] }

    it "sends the rescue vehicle latitude, longitude & location_updated_at to payload" do
      IsscDispatchStatus.new(connection_manager: connection_manager).perform(job.id, status, source, event_at, sent_at, others)
    end

    context "when #job.rescue_vehicle.location_updated_at is nil" do
      let(:result_latitude) { nil }
      let(:result_longitude) { nil }
      let(:result_updated_at) { nil }
      let(:location_updated_at) { nil }

      it "does NOT set the latitude, longitude & location_updated_at" do
        IsscDispatchStatus.new(connection_manager: connection_manager).perform(job.id, status, source, event_at, sent_at, others)
      end
    end
  end

  context "when status is set to 'GPS'" do
    let(:status) { 'GPS' }

    it "sends 'others' options to payload" do
      IsscDispatchStatus.new(connection_manager: connection_manager).perform(job.id, status, source, event_at, sent_at, others)
    end
  end
end
