# frozen_string_literal: true

require 'rails_helper'

# TODO this spec will be deleted soon
# https://swoopme.atlassian.net/browse/ENG-12487
describe Sites::Dropoff::Lincoln::Worker, vcr: true do
  subject(:worker_perform) do
    Sites::Dropoff::Lincoln::Worker.new.perform(async_request.id, job.id, subject_args)
  end

  let(:async_request) { create(:api_async_request, company: lincoln, user: user) }
  let(:lincoln) { create(:fleet_company, :lincoln) }
  let(:user) { create(:user, company: lincoln) }
  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: lincoln,
      service_location: service_location,
    }
  end

  let!(:service_location) do
    create :location, {
      lat: 37.924513,
      lng: -121.230777,
    }
  end

  let!(:advance_auto_repair) { create :location, :advance_auto_repair, :dealership }
  let!(:fell_street_auto_repair) { create :location, :fell_street_auto_repair, :dealership }
  let!(:sunrise_auto_service) { create :location, :sunrise_auto_service, :dealership }
  let!(:dr_auto_las_vegas) { create :location, :dr_auto_las_vegas, :dealership }

  let!(:advance_auto_site) do
    create :fleet_site, {
      name: "Advance Auto Repair",
      company: lincoln,
      location: advance_auto_repair,
    }
  end

  let!(:fell_street_site) do
    create :fleet_site, {
      name: "Fell Street Auto Repair",
      company: lincoln,
      location: fell_street_auto_repair,
    }
  end

  let!(:sunrise_auto_site) do
    create :fleet_site, {
      name: "Sunrise Auto Service",
      company: lincoln,
      location: sunrise_auto_service,
    }
  end

  let!(:dr_auto_site) do
    create :fleet_site, {
      name: "Dr Auto Las vegas",
      company: lincoln,
      location: dr_auto_las_vegas,
    }
  end

  context "has sites" do
    describe "good args" do
      let(:subject_args) do
        {
          "job" => {
            "service" => { "name" => ServiceCode::ACCIDENT_TOW },
            "location" => {
              "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA
            },
          },
        }
      end

      let(:dropoff_sites_return) do
        {
          async_request_id: async_request.id,
          data: {
            dropoff_sites: [],
          },
        }
      end

      it 'returns sites' do
        expect(PCC::Publishers::Websocket).to receive(:call) do |args|
          dropoff_sites = args[:input][:dropoff_sites]
          expect(dropoff_sites.length).to eq(3)
          expect(dropoff_sites[0][:site][:id]).to eq(advance_auto_site.id)
          expect(dropoff_sites[1][:site][:id]).to eq(fell_street_site.id)
          expect(dropoff_sites[2][:site][:id]).to eq(sunrise_auto_site.id)
        end

        worker_perform
      end
    end
  end
end
