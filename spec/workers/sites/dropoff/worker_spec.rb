# frozen_string_literal: true

require 'rails_helper'

describe Sites::Dropoff::Worker, vcr: true do
  subject(:worker_perform) do
    Sites::Dropoff::Worker.new.perform(async_request.id, job.id, subject_args)
  end

  let(:async_request) { create(:api_async_request, company: company, user: user) }
  let(:user) { create(:user, company: company) }
  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: company,
      service_location: service_location,
    }
  end

  let!(:service_location) do
    create :location, {
      lat: 37.924513,
      lng: -121.230777,
    }
  end

  let!(:advance_auto_repair) { create :location, :advance_auto_repair, :dealership }
  let!(:fell_street_auto_repair) { create :location, :fell_street_auto_repair, :dealership }
  let!(:sunrise_auto_service) { create :location, :sunrise_auto_service, :dealership }
  let!(:dr_auto_las_vegas) { create :location, :dr_auto_las_vegas, :dealership }

  let!(:advance_auto_site) do
    create :fleet_site, {
      name: "Advance Auto Repair",
      company: company,
      location: advance_auto_repair,
    }
  end

  let!(:fell_street_site) do
    create :fleet_site, {
      name: "Fell Street Auto Repair",
      company: company,
      location: fell_street_auto_repair,
    }
  end

  let!(:sunrise_auto_site) do
    create :fleet_site, {
      name: "Sunrise Auto Service",
      company: company,
      location: sunrise_auto_service,
    }
  end

  let!(:dr_auto_site) do
    create :fleet_site, {
      name: "Dr Auto Las vegas",
      company: company,
      location: dr_auto_las_vegas,
    }
  end

  let!(:daly_city_wo_location_type) do
    site = create :fleet_site, :advance_auto_site, name: 'Daly City w/o Location Type', company: company
    site.location.update! location_type: nil, lat: 37.6786812, lng: -122.4704281

    site
  end

  let(:company) { create(:fleet_company) }

  let(:subject_args) do
    {
      "job" => {
        "service" => { "name" => ServiceCode::ACCIDENT_TOW },
        "location" => {
          "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA
        },
      },
    }
  end

  it 'returns the expected sites' do
    expect(PCC::Publishers::Websocket).to receive(:call) do |args|
      dropoff_sites = args[:input][:dropoff_sites]
      expect(dropoff_sites.length).to eq(4)
      expect(dropoff_sites[0][:site][:id]).to eq(advance_auto_site.id)
      expect(dropoff_sites[1][:site][:id]).to eq(fell_street_site.id)
      expect(dropoff_sites[2][:site][:id]).to eq(sunrise_auto_site.id)
      expect(dropoff_sites[3][:site][:id]).to eq(daly_city_wo_location_type.id)
    end

    worker_perform
  end

  context 'when company is Lincoln' do
    let(:company) { create(:fleet_company, :lincoln) }

    it 'returns only sites with location type Dealership' do
      expect(PCC::Publishers::Websocket).to receive(:call) do |args|
        dropoff_sites = args[:input][:dropoff_sites]
        expect(dropoff_sites.length).to eq(3)
        expect(dropoff_sites[0][:site][:id]).to eq(advance_auto_site.id)
        expect(dropoff_sites[1][:site][:id]).to eq(fell_street_site.id)
        expect(dropoff_sites[2][:site][:id]).to eq(sunrise_auto_site.id)
      end

      worker_perform
    end
  end
end
