# frozen_string_literal: true

require "rails_helper"

describe InvoicePdfWorker do
  subject(:worker_perform) do
    InvoicePdfWorker.new.perform(invoice.id, invoice_pdf.id)
  end

  let(:invoice) { create(:invoice) }
  let(:invoice_pdf) { create(:invoice_pdf, public_url: nil) }

  let(:invoice_generator_double) { double(:invoice_generator, generate: pdf_double) }
  let(:pdf_double) { double(:pdf_double) }
  let(:uploader_double) { double(:uploader_double, upload: nil) }

  context 'when it successfully uploads the S3 file' do
    before do
      allow(Invoice).to receive(:find).and_return(invoice)
      allow(InvoicePdf).to receive(:find).and_return(invoice_pdf)
      allow_any_instance_of(InvoicePdf).to receive(:s3_public_url).and_return invoice_pdf.public_url

      allow(InvoicePdfGenerator).to receive(:new).and_return(invoice_generator_double)
      allow(S3Uploader).to receive(:new).and_return(uploader_double)
    end

    it 'uploads the new generated pdf into S3' do
      worker_perform

      expect(uploader_double).to have_received(:upload).with(pdf_double)
    end

    it 'populates the invoice_pdf.public_url with invoice_pdf.s3_public_url' do
      expect(invoice_pdf.public_url).to be nil

      worker_perform

      invoice_pdf.reload

      expect(invoice_pdf.public_url).to eq invoice_pdf.s3_public_url
    end
  end

  # this covers the transactional race condition case
  # where a job will try to access a database record that has not committed yet.
  context 'when the worker is triggered from inside a db transactional context' do
    let(:invoice_pdf) do
      invoice_pdf = create(:invoice_pdf)

      # we simulate it by taking the next id from the invoice_pdf sequence,
      # since the id will not be associated with a row in the DB yet.
      invoice_pdf.id =
        ActiveRecord::Base.connection.execute(
          "SELECT nextval('invoice_pdfs_id_seq')"
        )[0]["nextval"]

      invoice_pdf
    end

    context 'and the invoice_pdf has not yet been committed into the DB' do
      it 'raises ActiveRecord::RecordNotFound' do
        expect { worker_perform }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
