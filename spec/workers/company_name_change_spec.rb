# frozen_string_literal: true

require 'rails_helper'

describe CompanyNameChange do
  subject { described_class.new.perform(company.id, old_name) }

  let!(:old_name) { 'The Old Company Name' }
  let!(:company) { create(:fleet_company, :fleet_demo, name: 'The New Company Name') }

  context 'when account names match the old company name' do
    let!(:account) do
      create(:account, skip_account_setup: true,
                       client_company: company,
                       name: 'The Old Company Name')
    end

    it 'updates the account names' do
      expect { subject }.to change { account.reload.name }.to('The New Company Name')
    end
  end

  context 'when account names do not match the old company name' do
    let!(:account) do
      create(:account, client_company: company,
                       skip_account_setup: true,
                       name: 'Some Other Custom Name')
    end

    it 'does not update account names' do
      expect { subject }.to avoid_changing(account, :name)
    end
  end
end
