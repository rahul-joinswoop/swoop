# frozen_string_literal: true

require "rails_helper"

describe IsscAutoAcceptJob do
  subject(:worker_perform) do
    IsscAutoAcceptJob.new.perform(job.id)
  end

  let(:account) { create(:account, auto_accept: auto_accept) }
  let(:job) { create(:rescue_job, account: account) }

  context "when job is not auto-acceptable/rejectable" do
    let(:auto_accept) { false }

    it "does nothing" do
      expect_any_instance_of(ISSC::RejectJob).not_to receive(:execute)
      expect_any_instance_of(ISSC::AcceptJob).not_to receive(:execute)

      subject
    end
  end

  context "when job has MC_JOB_AUTO_REJECT_OVERRIDE feature" do
    let(:auto_accept) { false }

    before do
      job.rescue_company.features << create(:feature, name: Feature::MC_JOB_AUTO_REJECT_OVERRIDE)
    end

    it "rejects job" do
      expect_any_instance_of(ISSC::RejectJob).to receive(:execute)

      subject
    end
  end

  context "when job is auto-acceptable" do
    let(:auto_accept) { true }

    it "accepts job" do
      expect_any_instance_of(ISSC::AcceptJob).to receive(:execute)

      subject
    end
  end
end
