# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::ExportToFleet::FleetResponse::Worker, vcr: true do
  subject(:worker_perform) do
    InvoiceService::ExportToFleet::FleetResponse::Worker.new
      .perform(swoop_invoice.id, swoop_invoice.api_async_request.id)
  end

  let(:swoop_invoice) do
    create(
      :invoice,
      state: 'swoop_sending_fleet',
      sender: swoop_company,
      recipient: swoop_and_fleet_account,
      recipient_company: swoop_and_fleet_account.client_company,
      api_async_request: api_async_request,
      job: fleet_managed_job
    )
  end

  let(:swoop_company) { create(:super_company) }

  let(:swoop_user) { create(:user, company_id: swoop_company.id) }

  let(:fleet_managed_company) { create(:fleet_company, :fleet_response) }

  let(:fleet_managed_job) { create(:fleet_managed_job, service_location: create(:location, state: 'CA')) }

  let(:swoop_and_fleet_account) do
    create(
      :account,
      name: 'Fleet Response',
      company: swoop_company,
      client_company: fleet_managed_company
    )
  end

  let(:api_async_request) do
    create(
      :api_async_request,
      system: API::AsyncRequest::FLEET_RESPONSE_INVOICE,
      company: swoop_company,
      user: swoop_user
    )
  end

  before :each do
    allow_any_instance_of(FleetManagedJob).to receive(:completed_at).and_return(Time.now)
    allow_any_instance_of(InvoicePdfGenerator).to receive(:generate).and_return('a nice pdf invoice')
  end

  it 'sets swoop_invoice state to swoop_sent_fleet' do
    worker_perform

    swoop_invoice.reload

    expect(swoop_invoice.state).to eq Invoice::SWOOP_SENT_FLEET
  end

  it 'does not add swoop_invoice.export_error_msg' do
    worker_perform

    swoop_invoice.reload

    expect(swoop_invoice.export_error_msg).to be_nil
  end

  it 'sets request and response data onto swoop_invoice.api_async_request' do
    worker_perform

    swoop_invoice.reload

    expect(swoop_invoice.api_async_request.external_request_url).not_to be_nil
    expect(swoop_invoice.api_async_request.external_request_headers).not_to be_nil
    expect(swoop_invoice.api_async_request.external_request_body).not_to be_nil

    expect(swoop_invoice.api_async_request.external_response_body).not_to be_nil
    expect(swoop_invoice.api_async_request.external_response_headers).not_to be_nil
    expect(swoop_invoice.api_async_request.external_response_code).not_to be_nil
  end

  context 'when response from FleetResponse API is not Accepted' do
    let(:error_code) { "Not Found" }

    it 'moves swoop_invoice state back to swoop_approved_fleet' do
      worker_perform

      swoop_invoice.reload

      expect(swoop_invoice.state).to eq Invoice::SWOOP_APPROVED_FLEET
    end

    context 'when error_code is Not Found' do
      it 'sets invoice.export_error_msg accordingly' do
        worker_perform

        swoop_invoice.reload

        expect(swoop_invoice.export_error_msg).to eq 'Fleet Response Claim not found or did not have record of Swoop service on that claim'
      end
    end

    context 'when error_code is Error' do
      it 'sets invoice.export_error_msg accordingly' do
        worker_perform

        swoop_invoice.reload

        expect(swoop_invoice.export_error_msg).to eq 'Fleet Response was unable to process the transaction and Fleet Response has been notified'
      end
    end

    context 'when error_code is Tax Exempt' do
      it 'sets invoice.export_error_msg accordingly' do
        worker_perform

        swoop_invoice.reload

        expect(swoop_invoice.export_error_msg).to eq 'Client is tax exempt but is being charged tax, invoice is rejected and not saved'
      end
    end

    context 'when we have generic error' do
      around(:all) do |example|
        ClimateControl.modify FLEET_RESPONSE_INVOICE_ENDPOINT_TIMEOUT: '0.1' do
          example.run
        end
      end

      it 'sets invoice.export_error_msg accordingly' do
        worker_perform

        swoop_invoice.reload

        expect(swoop_invoice.export_error_msg).to eq 'Error with the export invoice process. Please try again or report to an admin.'
      end
    end
  end
end
