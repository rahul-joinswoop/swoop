# frozen_string_literal: true

require 'rails_helper'

describe InvoiceService::GenerateSwoopFleetInvoiceReportWorker, vcr: true do
  subject(:worker_perform) do
    InvoiceService::GenerateSwoopFleetInvoiceReportWorker.new.perform(
      report_params,
      swoop_user.id,
      [swoop_invoice_one.id, swoop_invoice_two.id],
      api_async_request.id
    )
  end

  let(:report_params) do
    {
      "client_company_id" => fleet_managed_company.id,
    }
  end

  let(:swoop_invoice_one) do
    create(
      :invoice,
      state: 'swoop_sending_fleet',
      sender: swoop_company,
      recipient: swoop_and_fleet_account,
      recipient_company: swoop_and_fleet_account.client_company,
      api_async_request: api_async_request,
      job: fleet_managed_job
    )
  end

  let(:swoop_invoice_two) do
    create(
      :invoice,
      state: 'swoop_sending_fleet',
      sender: swoop_company,
      recipient: swoop_and_fleet_account,
      recipient_company: swoop_and_fleet_account.client_company,
      api_async_request: api_async_request,
      job: fleet_managed_job
    )
  end

  let(:swoop_company) { create(:super_company) }

  let(:swoop_user) { create(:user, company_id: swoop_company.id) }

  let(:fleet_managed_company) { create(:fleet_company, :fleet_response) }

  let(:fleet_managed_job) do
    create(:fleet_managed_job, service_location: create(:location, state: 'CA'))
  end

  let(:swoop_and_fleet_account) do
    create(
      :account,
      name: 'Swoop and Fleet',
      company: swoop_company,
      client_company: fleet_managed_company
    )
  end

  let(:api_async_request) do
    create(
      :api_async_request,
      system: API::AsyncRequest::SWOOP_FLEET_INVOICE_DOWNLOAD,
      company: swoop_company,
      user: swoop_user
    )
  end

  let(:report_result) do
    report_result = create(:report_result,
                           report: Report.first,
                           company: swoop_company,
                           user_id: swoop_user.id,
                           format: 'CSV',
                           collated: false,
                           s3_filename: 'https://s3.amazonaws.com/joinswoop.stagingreports/report.csv')

    report_result.update!(state: ReportResult::FINISHED)

    report_result
  end

  before do
    unless Report.where(name: Report::SWOOP_FLEET_INVOICE).exists?
      create(:report, name: Report::SWOOP_FLEET_INVOICE)
    end

    allow_any_instance_of(CompanyReport).to receive(:run).and_return(report_result)

    allow(Publishers::GenericWebsocketMessage).to receive(:call)

    allow(User).to receive(:find).with(swoop_user.id).and_return(swoop_user)
  end

  it 'moves invoices to swoop_sent_fleet state' do
    worker_perform

    swoop_invoice_one.reload
    swoop_invoice_two.reload

    expect(swoop_invoice_one.state).to eq Invoice::SWOOP_SENT_FLEET
    expect(swoop_invoice_two.state).to eq Invoice::SWOOP_SENT_FLEET
  end

  it 'pushes WS with invoices updated' do
    allow_any_instance_of(
      InvoiceService::GenerateSwoopFleetInvoiceReportWorker
    ).to receive(:publish_report_result_through_ws)

    partial_invoices = [swoop_invoice_one, swoop_invoice_two].map do |invoice|
      { id: invoice.id, state: Invoice::SWOOP_SENT_FLEET }
    end

    expect(Publishers::GenericWebsocketMessage).to receive(:call).with({
      input: {
        class_name: 'PartialObjectArray',
        target_data: {
          partial_invoices: partial_invoices,
        },
        company: Company.swoop,
      },
    })

    worker_perform
  end

  it 'pushes WS with report result' do
    allow_any_instance_of(
      InvoiceService::GenerateSwoopFleetInvoiceReportWorker
    ).to receive(:publish_partial_invoices_through_ws)

    expect(Publishers::AsyncRequestWebsocketMessage).to receive(:call).with({
      input: {
        report_result: ReportResultSerializer.new(report_result).serializable_hash,
      },
      id: api_async_request.id,
    })

    worker_perform
  end

  context 'when FleetManaged is Century' do
    let(:fleet_managed_company) { create(:fleet_company, :century) }
    let(:century_invoice_report) do
      Report.where(name: Report::CENTURY_INVOICE).first ||
        create(:report, name: Report::CENTURY_INVOICE)
    end

    before do
      allow(Report).to receive(:century_invoice).and_return(century_invoice_report)
      allow(Company).to receive(:swoop).and_return(swoop_company)
      allow(CompanyReport).to receive(:new).and_call_original
    end

    it 'generates the report using Report::CENTURY' do
      expect(CompanyReport).to receive(:new).with(
        company_id: swoop_company.id, report: century_invoice_report
      )

      worker_perform
    end

    it 'passes the right params to CompanyReport.run' do
      allow_any_instance_of(CompanyReport).to receive(:run).with(
        swoop_user,
        { invoice_ids: array_including([swoop_invoice_one.id, swoop_invoice_two.id]) },
        nil,
        nil,
        nil,
        true
      )
    end
  end

  context "when company_report_result.state is != 'Finished'" do
    before do
      allow_any_instance_of(ReportResult).to receive(:state).and_return(ReportResult::FAILED)
    end

    it 'pushes invoices back to swoop_approved_fleet state' do
      expect { worker_perform }.to raise_error(InvoiceService::ReportResultFailed)

      swoop_invoice_one.reload
      swoop_invoice_two.reload

      expect(swoop_invoice_one.state).to eq Invoice::SWOOP_APPROVED_FLEET
      expect(swoop_invoice_two.state).to eq Invoice::SWOOP_APPROVED_FLEET
    end
  end

  context 'when error occurs during the worker process' do
    before :each do
      allow_any_instance_of(
        InvoiceService::GenerateSwoopFleetInvoiceReportWorker
      ).to receive(:generate_report).and_raise("boom")
    end

    it 'pushes invoices back to swoop_approved_fleet state' do
      expect { worker_perform }.to raise_error(RuntimeError)

      swoop_invoice_one.reload
      swoop_invoice_two.reload

      expect(swoop_invoice_one.state).to eq Invoice::SWOOP_APPROVED_FLEET
      expect(swoop_invoice_two.state).to eq Invoice::SWOOP_APPROVED_FLEET
    end

    it 'pushes WS with invoices updated with error' do
      allow_any_instance_of(
        InvoiceService::GenerateSwoopFleetInvoiceReportWorker
      ).to receive(:publish_report_result_error_through_ws)

      partial_invoices = [swoop_invoice_one, swoop_invoice_two].map do |invoice|
        { id: invoice.id, state: Invoice::SWOOP_APPROVED_FLEET }
      end

      expect(Publishers::GenericWebsocketMessage).to receive(:call).with({
        input: {
          class_name: 'PartialObjectArray',
          target_data: {
            partial_invoices: array_including(partial_invoices),
            error: {
              message: 'There was a problem with the report generation. ' \
                       'Please try again or call a system admin.',
            },
          },
          company: Company.swoop,
        },
      })

      expect { worker_perform }.to raise_error(RuntimeError)
    end

    it 'pushes WS with report result with error' do
      allow_any_instance_of(
        InvoiceService::GenerateSwoopFleetInvoiceReportWorker
      ).to receive(:publish_partial_invoices_with_error_through_ws)

      expect(Publishers::GenericWebsocketMessage).to receive(:call).with({
        input: {
          class_name: 'AsyncRequest',
          target_data: {
            error: {
              code: 500,
              message: 'There was a problem with the report generation. ' \
                       'Please try again or call a system admin.',
            },
            id: api_async_request.id,
          },
          company: swoop_company,
        },
      })

      expect { worker_perform }.to raise_error(RuntimeError)
    end
  end
end
