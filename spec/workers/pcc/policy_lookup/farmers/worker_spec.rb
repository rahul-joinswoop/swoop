# frozen_string_literal: true

require 'rails_helper'

describe PCC::PolicyLookup::Farmers::Worker, vcr: true do
  subject(:worker_perform) do
    PCC::PolicyLookup::Farmers::Worker.new.perform(async_request.id, job.id, { "policy_number": policy_number })
  end

  let(:async_request) { create(:api_async_request, company: company, user: user) }
  let(:company) { create(:fleet_company, :century) }
  let(:user) { create(:user, company: company) }
  let(:policy_number) { '0103293256' }
  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: company,
    }
  end

  before do
    allow(Company).to receive(:find).with(
      async_request.company_id
    ).and_return(company)
  end

  it 'calls publishers with expected params' do
    msg = {
      class: "AsyncRequest",
      operation: "update",
      target: {
        id: async_request.id,
        data: { "policies": [
          {
            "id": 1,
            "policy_number": "0103293256",
            "full_name": "KENNETH DIXON",
            "address": "20052 PINEVILLE CT # 60, YORBA LINDA, CA",
          },
        ] },
      },
    }
    expect(AsyncRedis.instance).to receive(:publish) do |channel, message|
      expect(channel).to eq("event_#{company.channel_name}")
      msg[:target][:data][:policies][0].delete(:id)
      actual = JSON.parse(message)
      actual[:target][:data][:policies][0].delete(:id)
      expect(actual).to eq(msg)
    end

    worker_perform
  end
end
