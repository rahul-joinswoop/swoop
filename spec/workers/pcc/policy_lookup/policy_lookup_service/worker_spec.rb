# frozen_string_literal: true

require 'rails_helper'

describe PCC::PolicyLookup::PolicyLookupService::Worker do
  subject(:worker_perform) do
    PCC::PolicyLookup::PolicyLookupService::Worker.new.perform(async_request.id, job.id, { "vin" => vin })
  end

  let!(:company) { create(:fleet_company, :lincoln) }
  let!(:async_request) { create(:api_async_request, company: company, user: user) }
  let(:user) { create(:user, company: company) }
  let(:vin) { '5TFMU4FNXFX073781' }
  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: company,
    }
  end

  let(:pls_return) do
    <<~HEREJSON
    {
      "policies": [
        {
          "uuid": "cf7351c1ed822596051f7ce49af9840a",
          "start_date": "2018-12-01",
          "end_date": "2024-12-01",
          "cancel_date": null,
          "client_code": "lincoln",
          "client_name": "Lincoln Motor Company",
          "program_code": "lincoln_warranty",
          "program_name": "Lincoln Warranty",
          "details": {
            "dealer_code": "L25261"
          },
          "expired": false,
          "vehicles": [
            {
              "uuid": "4e3fd6db976aeac5e8ec08fb75eebbc3"
            }
          ],
          "customers": [
            {
              "uuid": "9a3c49ff6dc964d2202df6ea766ff8d2"
            }
          ]
        }
      ],
      "pii": [
        "http://localhost:7500/lincoln/pii/V-4e3fd6db976aeac5e8ec08fb75eebbc3/C-9a3c49ff6dc964d2202df6ea766ff8d2?auth_token=pk212w-4flps4otej3qv2glkm5lqvs8ji6a8ozzrjew5lxght3p8sp0n1"
      ]
    }
    HEREJSON
  end

  let(:policy_uuid) { "cf7351c1ed822596051f7ce49af9840a" }

  let(:pcc_policy) do
    create :pcc_policy, {
      job: job,
      uuid: policy_uuid,
    }
  end

  let(:policy_search_return) do
    {
      async_request_id: async_request.id,
      input: {
        policies: [
          {
            id: pcc_policy.id,
            uuid: policy_uuid,
            pcc_vehicles: [
              {
                uuid: "4e3fd6db976aeac5e8ec08fb75eebbc3",
                vin: "5LMTJ3DH710484384",
                license_number: "LX8890",
                license_issuer: "WI",
                make: "LINCOLN",
                model: "MKC",
                year: 2019,
                color: "BLAC",
                purchase_date: "2018-12-01",
              },
            ],
            pcc_drivers: [
              {
                uuid: "9a3c49ff6dc964d2202df6ea766ff8d2",
                name: "DELORAS FERRY",
                street_address: "26155 KIHN UNION",
                city: "PUNTA GORDA",
                region: "FL",
                postal_code: "33980",
                country_code: "US",
                phone_number: "+15559549406",
                business: false,
              },
            ],
            pcc_vehicles_and_drivers_matrix: [
              {
                pcc_driver: {
                  business: false,
                  city: "PUNTA GORDA",
                  country_code: "US",
                  name: "DELORAS FERRY",
                  phone_number: "+15559549406",
                  postal_code: "33980",
                  region: "FL",
                  street_address: "26155 KIHN UNION",
                  uuid: "9a3c49ff6dc964d2202df6ea766ff8d2",
                },
                pcc_vehicle: {
                  color: "BLAC",
                  license_issuer: "WI",
                  license_number: "LX8890",
                  make: "LINCOLN",
                  model: "MKC",
                  purchase_date: "2018-12-01",
                  uuid: "4e3fd6db976aeac5e8ec08fb75eebbc3",
                  vin: "5LMTJ3DH710484384",
                  year: 2019,
                },
              },
            ],
            policy_number: "0103293256",
          },
        ],
      },
    }
  end

  let(:pii_return) do
    <<~HEREJSON
    {
      "4e3fd6db976aeac5e8ec08fb75eebbc3": {
        "vehicle": {
          "uuid": "4e3fd6db976aeac5e8ec08fb75eebbc3",
          "vin": "5LMTJ3DH710484384",
          "license_number": "LX8890",
          "license_issuer": "WI",
          "make": "LINCOLN",
          "model": "MKC",
          "year": 2019,
          "color": "BLAC",
          "purchase_date": "2018-12-01"
        }
      },
      "9a3c49ff6dc964d2202df6ea766ff8d2": {
        "customer": {
          "uuid": "9a3c49ff6dc964d2202df6ea766ff8d2",
          "name": "DELORAS FERRY",
          "street_address": "26155 KIHN UNION",
          "city": "PUNTA GORDA",
          "region": "FL",
          "postal_code": "33980",
          "country_code": "US",
          "phone_number": "+15559549406",
          "business": false
        }
      }
    }
    HEREJSON
  end

  before do
    company.settings << Setting.create!(key: Setting::POLICY_LOOKUP_SERVICE_CLIENT_CODE, value: 'lincoln')
  end

  it 'calls publishers with expected params' do
    expect(PCC::Policy).to receive(:create!).and_return(pcc_policy)

    stub_request(:get, "#{Configuration.pls_api.url}/lincoln/vehicles").with(query: { vin: vin }).to_return(body: pls_return)
    stub_request(:get, "http://localhost:7500/lincoln/pii/V-4e3fd6db976aeac5e8ec08fb75eebbc3/C-9a3c49ff6dc964d2202df6ea766ff8d2?auth_token=pk212w-4flps4otej3qv2glkm5lqvs8ji6a8ozzrjew5lxght3p8sp0n1").to_return(body: pii_return)

    expect(PCC::Publishers::Websocket).to receive(:call) do |args|
      pcc_policy.reload
      policy_search_return[:input][:policies][0][:pcc_vehicles][0][:id] = pcc_policy.vehicles[0].id
      policy_search_return[:input][:policies][0][:pcc_drivers][0][:id] = pcc_policy.drivers[0].id
      policy_search_return[:input][:policies][0][:pcc_vehicles_and_drivers_matrix][:pcc_vehicles][0][:id] = pcc_policy.vehicles[0].id
      policy_search_return[:input][:policies][0][:pcc_vehicles_and_drivers_matrix][:pcc_drivers][0][:id] = pcc_policy.vehicles[0].id
      expect(args).to eql(policy_search_return)
    end

    worker_perform
  end
end
