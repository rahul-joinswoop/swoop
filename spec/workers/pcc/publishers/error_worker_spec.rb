# frozen_string_literal: true

require 'rails_helper'

describe PCC::Publishers::ErrorWorker do
  subject(:worker_perform) do
    PCC::Publishers::ErrorWorker.new.perform(async_request.id)
  end

  let(:async_request) { create(:api_async_request, company: company, user: user) }
  let(:company) { create(:fleet_company, :century) }
  let(:user) { create(:user, company: company) }

  before do
    allow(Company).to receive(:find).with(
      async_request.company_id
    ).and_return(company)
  end

  it 'calls Publishers::GenericWebsocketMessage with expected params' do
    expect(Publishers::GenericWebsocketMessage).to receive(:call).with(
      input: {
        class_name: 'AsyncRequest',
        target_data: {
          error: {
            code: 500,
            message: 'AgeroAPI has failed, please see the logs',
          },
          id: async_request.id,
        },
        company: company,
      }
    )

    worker_perform
  end
end
